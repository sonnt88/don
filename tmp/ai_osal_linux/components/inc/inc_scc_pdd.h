/************************************************************************
| FILE:         inc_scc_pdd.h
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Definitions for Inter Node Communication
|               SCC PDD (persistent device driver)
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 13.03.13  | Initial revision           | bta2hi
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#ifndef INC_SCC_PDD_H
#define INC_SCC_PDD_H

/* message id*/
#define SCC_PD_NET_C_COMPONENT_STATUS_MSGID	              0x20
#define SCC_PD_NET_R_COMPONENT_STATUS_MSGID               0x21
#define SCC_PD_NET_R_REJECT_MSGID	                      0x0B
#define SCC_PD_NET_C_READ_DATA_POOL               	      0x34
#define SCC_PD_NET_R_READ_DATA_POOL                       0x35
#define SCC_PD_NET_C_WRITE_DATA_POOL                      0x44
#define SCC_PD_NET_R_WRITE_DATA_POOL                      0x45

/* maximun lenght of data bytes  */
#define SCC_PD_NET_MAX_LENGTH_DATA_BYTES                  500

/* other message define*/
#define SCC_PD_NET_TP_MARKER                        0x00
#define SCC_PD_NET_APPLICATION_STATUS_ACTIVE        0x01
#define SCC_PD_NET_APPLICATION_STATUS_INACTIVE      0x02
#define SCC_PD_NET_VERSION                          0x02
/*reject reason*/
#define SCC_PD_NET_REJ_NO_REASON                    0x00
#define SCC_PD_NET_REJ_UNKNOWN_MESSAGE              0x01
#define SCC_PD_NET_REJ_INVALID_PARA                 0x02
#define SCC_PD_NET_REJ_TMP_UNAVAIL                  0x03
#define SCC_PD_NET_REJ_VERSION_MISMATCH             0x04

#endif /* INC_SCC_PDD_H */
