/************************************************************************
| FILE:         dgram_service.h
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Datagram service to be used on TCP stream socket.
|
|               Written data is prepended with a header which is used to find
|               message boundaries on receive.
|
|               Each call to dgram_send will result in one call to dgram_recv
|               returning.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 15.01.13  | Initial revision           | Andreas Pape
| 21.02.13  | Fix cpp compiler issues    | Matthias Thomae
| 06.02.13  | Move functions to .c file  | Matthias Thomae
| 07.10.16	| Support for TCP/IP		 | Mai Daftedar
|			| based Gen4 INC			 |
| 17.10.16	| Fix RADAR and lint issues	 | Venkatesh Parthasarathy
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#ifndef __DGRAM_SERVICE__H__
#define __DGRAM_SERVICE__H__

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OSAL_GEN4
#define MSG_PROTOCOL
#endif

#ifdef MSG_PROTOCOL
#define DEFAULT_MSGSZ 0x6000
#endif

#ifdef DG_ENABLE_DEBUG
#define DG_DBG(a, ...) fprintf(stdout,a, ## __VA_ARGS__)
#else
#define DG_DBG(a,...)
#endif

#define DG_ERR(a, ...) fprintf(stderr,a, ## __VA_ARGS__)


#define UNUSED_VARIABLE(VAR) VAR

/*standard header for datagram service*/
typedef struct {
   unsigned short dglen;   /*2byte-length of dgram*/
   #ifdef MSG_PROTOCOL
   unsigned short msgid;   /*uniques identifer for each message sent*/
   #endif
}dgram_header_std;

/*maximum size of datagram*/
#define DGRAM_MAX (0xffff-sizeof(dgram_header_std))

typedef struct {
   int sk;     /*socket filedescriptor*/
   char* buf;  /*receive buffer*/
   int len;    /*len of receive buffer*/
   int proto;  /*used protocol*/
   int hlen;   /*size of used dgram header*/
   int received;  /*received bytes*/
   dgram_header_std h; /*copy of received header*/
}sk_dgram;

sk_dgram* dgram_init(int sk, int dgram_max, void *options);
int dgram_exit(sk_dgram *skd);
int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen);
int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen);

#ifdef __cplusplus
}
#endif

#endif /*__DGRAM_SERVICE__H__*/
