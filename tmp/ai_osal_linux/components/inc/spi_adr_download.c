/******************************************************************************
 *
 * \file          spi_adr_download.c
 * \brief         This file implements the test code for verifying the SPI
 *                communication by accessing /dev/spidev_adr3 and /dev/sr0 
 *                interfaces.
 *
 * \project       IMX6 (ADIT GEN3) JAC & MIB
 *
 * \authors       dhs2cob
 *
 * COPYRIGHT      (c) 2014 Bosch CarMultimedia GmbH
 *
 * HISTORY      :
 *------------------------------------------------------------------------------
 * Date         |        Modification           | Author & comments
 *--------------|-------------------------------|---------------------------
 * 22.Aug.2014  |  Initial s32Version           | Suresh Dhandapani (RBEI/ECF5)
 * -----------------------------------------------------------------------------
|-----------------------------------------------------------------------*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <poll.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <malloc.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netdb.h>
#include "inc.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "inc_spi_share_test.h"

//#define ADR3CTRL_VERBOSE_TRACE

static tS32 s32SpiDevFd = 0;
#ifdef ADR3CTRL_VERBOSE_TRACE
static tS32 s32WriteCnt = 0;
#endif
tU32 u32ADR3ReadTimeout=2000;
struct pollfd rPollFdSet[1] = {0};
extern struct ADR rAdr_global;
static tS32 s32Init_setup(void);

/******************************************************************************
* FUNCTION     : s32ReadNetDevName
*
* PARAMETER    : ifname - pointer to buffer data
*                u32bufSize - buffer size
*
* RETURNVALUE  : tS32 - success/failure
*
*
* DESCRIPTION  :  This function checks the inc-adr3 interface is available.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32ReadNetDevName(tU8 * ifname,
                                        tU32   u32bufSize)
{
   tS32 s32RetVal = 0;
   tS32 s32fd;
   tS32 s32Len = 0;

   s32fd = open(ADR_IFNAME_DEVICE_TREE_PATH, O_RDONLY);
   if (s32fd < 0)
   {
      printf("\n%s:Open failed", __func__);
      s32RetVal = s32fd;
   }
   else
   {
      s32Len = read(s32fd, ifname, u32bufSize);
      if (s32Len < 0)
      {
         printf("\n%s:Read failed:%d", __func__, errno);
         s32RetVal = ADR_ERROR;
      }
      if(close(s32fd) < 0 )
      {
         printf("\n%s:close failed:%d", __func__, errno);
         s32RetVal = ADR_ERROR;
      }
   }
   
   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : s32GetGpioState
*
* PARAMETER    : s32fd - file descriptor
*                pValue - pointer to the buffer data
*
* RETURNVALUE  : tS32 - read data/failure
*
*
* DESCRIPTION  :  This function reads and updates the GPIO state into buffer.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32GetGpioState(const tS32 s32fd,
                                     tS32 *      pValue)
{
   tS32 s32Len;
   tS32 s32RetVal = 0;
   tU8 u8Value[INT_MAX_DECIMALS];

   s32Len = read(s32fd, u8Value, INT_MAX_DECIMALS);
   if (s32Len < 0)
   {
      printf("\n%s:Read failed:%d", __func__, errno);
      s32RetVal = ADR_ERROR;
   }
   *pValue = atoi(u8Value);

   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : s32SetGpioEdge
*
* PARAMETER    : path - pointer to device path
*                pu8Str - pointer to the string data
*
* RETURNVALUE  : tS32 - success/failure
*
*
* DESCRIPTION  :  This function updates the string in the mentioned path.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32SetGpioEdge(const tU8 * path,
                                               const tU8 * pu8Str)
{
   tS32 s32RetVal = 0;
   tS32 s32fd;
   tS32 s32Len = 0;

   s32fd = open(path, O_RDWR);
   if (s32fd < 0)
   {
      s32RetVal = s32fd;
      printf("\n%s:Open failed", __func__);
   }
   else
   {
      s32Len = write(s32fd, pu8Str, strlen(pu8Str));
      if (s32Len <= 0)
      {
         s32RetVal = ADR_ERROR;
         printf("\n%s:Write failed:%d", __func__, errno);
      }

      if(close(s32fd) < 0 )
      {
         printf("\n%s:close failed:%d", __func__, errno);
         s32RetVal = ADR_ERROR;
      }
   }

   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : s32SetGpioState
*
* PARAMETER    : path - pointer to device path
*                value - data - 0/1
*
* RETURNVALUE  : tS32 - success/failure
*
*
* DESCRIPTION  :  This function updates the value in the mentioned path.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32SetGpioState(const tU8 * path,
                                               const tS32 value)
{
   tS32 s32RetVal = 0;
   tS32 s32fd;
   tS32 s32Len = 0;
   tU8 u8ID[U32_MAX_DECIMALS];

   s32fd = open(path, O_RDWR);
   if (s32fd < 0)
   {
      s32RetVal = s32fd;
      printf("\n%s:Open failed", __func__);
   }
   else
   {
      s32Len = snprintf(u8ID, U32_MAX_DECIMALS, "%lu", value);
      if (s32Len <= 0 || s32Len >= U32_MAX_DECIMALS)
      {
         s32RetVal = ADR_ERROR;
         printf("\n%s:data length not matched", __func__);
      }
      else
      {
         s32Len = write(s32fd, u8ID, strlen(u8ID));
         if (s32Len <= 0)
         {
            s32RetVal = ADR_ERROR;
            printf("\n%s:Write failed:%d", __func__, errno);
         }
         if(close(s32fd) < 0 )
         {
            printf("\n%s:close failed:%d", __func__, errno);
            s32RetVal = ADR_ERROR;
         }
      }
   }
   
   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : s32SetNetDevState
*
* PARAMETER    : activate - enable/disable - 1/0
*
* RETURNVALUE  : tS32 - success/failure
*
*
* DESCRIPTION  :  This function enables/disables the INC communication.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32SetNetDevState(tS32 activate)
{
   tS32 s32RetVal;
   tS32 s32fd = -1;
   struct ifreq ifr;
   tU8 ifname[IFNAMSIZ];

   memset(&ifr, 0, sizeof(struct ifreq));
   s32RetVal = s32ReadNetDevName(ifname, IFNAMSIZ);
   if (!s32RetVal)
   {
      s32fd = socket(AF_BOSCH_INC_ADR, (tS32)SOCK_STREAM, 0);
      if (s32fd < 0)
      {
         s32RetVal = s32fd;
         printf("\n%s:Socket Open failed", __func__);
      }
      else
      {
         strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
         if (ioctl(s32fd, SIOCGIFFLAGS, &ifr) == -1)
         {
            close(s32fd);
            printf("\n%s:IOCTL-1 SIOCGIFFLAGS failed:%d", __func__, errno);
            s32RetVal = ADR_ERROR;
         }
      }
   }
   if (!s32RetVal)
   {
      if (activate)
      {
         if (ifr.ifr_flags & IFF_UP)
         {
            close(s32fd);
            printf("\n%s:INC Activation failed", __func__);
            s32RetVal = ADR_ERROR;
         }
         ifr.ifr_flags |= IFF_UP;
      }
      else
      {
         if (!(ifr.ifr_flags & IFF_UP))
         {
            close(s32fd);
            printf("\n%s:INC Deactivation failed", __func__);
            s32RetVal = ADR_ERROR;
         }
         ifr.ifr_flags &= ~IFF_UP;
      }
   }
   if (!s32RetVal)
   {
      if (ioctl(s32fd, SIOCSIFFLAGS, &ifr) == -1)
      {
         printf("\n%s:IOCTL-2 SIOCGIFFLAGS failed:%d", __func__, errno);
         s32RetVal = ADR_ERROR;
      }
      if(close(s32fd) < 0 )
      {
         printf("\n%s:close failed:%d", __func__, errno);
         s32RetVal = ADR_ERROR;
      }
   }

   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : s32InitDnl
*
* PARAMETER    : void
*
* RETURNVALUE  : tS32 - success/failure
*
*
* DESCRIPTION  :  This function sets the ADR to download mode.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32InitDnl(void)
{
   tS32 s32RetVal;

   s32RetVal = s32SetNetDevState(0);
   if (s32RetVal != 0)
   {
      s32RetVal = ADR_ERROR;
      rAdr_global.u32Adr_err_count |= 1 << 0;
      printf("\n%s:Deactivate INC failed", __func__);
   }
   else
   {
      /* TODO: use new interface for named GPIOs once available */
      s32RetVal = system(CMD_REQ_GPIO_EXPORT);
      if (s32RetVal != 0)
      {
         s32RetVal = ADR_ERROR;
         rAdr_global.u32Adr_err_count |= 1 << 1;
         printf("\n%s:GPIO export failed", __func__);
      }
      else
      {
         s32RetVal = system(CMD_SPIDEV_START);
         if (s32RetVal != 0)
         {
            s32RetVal = ADR_ERROR;
            rAdr_global.u32Adr_err_count |= 1 << 2;
            printf("\n%s:Load SPIDEV module failed", __func__);
         }
      }
   }
   if (!s32RetVal)
   {
      s32RetVal = s32Init_setup();
      if (s32RetVal)
      {
         printf("\n%s:Setting up failed\n", __func__);
         s32RetVal = ADR_ERROR;
      }
   }

   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : s32Init_setup
*
* PARAMETER    : void
*
* RETURNVALUE  : tS32 - success/failure
*
*
* DESCRIPTION  :  This function clears the SPI line and the poll event.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32Init_setup(void)
{

   tU8 u8DummyByte = 0;
   tS32 s32Len;
   tS32 s32RetVal = 0;
   tS32 s32ReqVal;
   
   rPollFdSet[0].fd = open(ADR_REQ_GPIO_VALUE_PATH, O_RDONLY);
   if (rPollFdSet[0].fd < 0)
   {
      s32RetVal = ADR_ERROR;
      rAdr_global.u32Adr_err_count |= 1 << 3;
      printf("\n%s:GPIO req line open failed:%d", __func__, errno);
   }
   else
   {
      rPollFdSet[0].events = POLLPRI;
      /* read the current value; this prevents poll from returning again
       * without an actual event having occurred */
      s32RetVal = s32GetGpioState(rPollFdSet[0].fd, &s32ReqVal);
      if (s32RetVal)
      {
         rAdr_global.u32Adr_err_count |= 1 << 28;
         s32RetVal = ADR_ERROR;
         close(rPollFdSet[0].fd);
      }
   }
   if (!s32RetVal)
   {
      s32SpiDevFd = open(SPI_DEVICE_PATH, O_RDWR);
      if (s32SpiDevFd < 0)
      {         
         rAdr_global.u32Adr_err_count |= 1 << 4;
         close(rPollFdSet[0].fd);
         printf("\n%s:SPIDEV open failed:%d", __func__, errno);
      }
      else
      {
         /* write dummy byte to clear communication channel
          * TODO: analyze why this is necessary */
         s32Len = write(s32SpiDevFd, &u8DummyByte, 1);
         if (s32Len <= 0)
         {            
            rAdr_global.u32Adr_err_count |= 1 << 5;
            close(s32SpiDevFd);
            close(rPollFdSet[0].fd);
            printf("\n%s:SPIDEV write failed:%d", __func__, errno);
         }
      }
   }
   
   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : s32ExitDnl
*
* PARAMETER    : void
*
* RETURNVALUE  : tS32 - success/failure
*
*
* DESCRIPTION  :  This function sets ADR into normal mode.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32ExitDnl(void)
{
   tS32 s32RetVal = 0;

   if (close(rPollFdSet[0].fd) < 0)
   {
      rAdr_global.u32Adr_err_count |= 1 << 6;
      printf("\n%s:GPIO Req line close failed:%d", __func__, errno);
      s32RetVal = ADR_ERROR;
   }
   if (close(s32SpiDevFd) < 0)
   {
      rAdr_global.u32Adr_err_count |= 1 << 7;
      printf("\n%s:SPIDEV close failed:%d", __func__, errno);
      s32RetVal = ADR_ERROR;
   }   
   /* TODO: use new interface for named GPIOs once available */
   if (system(CMD_REQ_GPIO_UNEXPORT) != 0)
   {
      rAdr_global.u32Adr_err_count |= 1 << 9;
      printf("\n%s:GPIO unexport Req line failed:%d", __func__, errno);
      s32RetVal = ADR_ERROR;
   }
   if (s32SetNetDevState(1) != 0)
   {
      rAdr_global.u32Adr_err_count |= 1 << 10;
      printf("\n%s:Activate INC failed", __func__);
      s32RetVal = ADR_ERROR;
   }
   
   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : s32ResetADR
*
* PARAMETER    : mode - normal/download mode
*
* RETURNVALUE  : tS32 - success/failure
*
*
* DESCRIPTION  :  This function sets the ADR into normal/download mode and does 
*                 reset.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
tS32 s32ResetADR(enAdrBootMode mode)
{

   tS32 s32RetVal = 0;
   
   if (mode == BOOT_MODE_SPI)
      s32RetVal = s32InitDnl();
   if (mode == BOOT_MODE_NORMAL)
      s32RetVal = s32ExitDnl();
   if (s32RetVal != 0)
   {
      rAdr_global.u32Adr_err_count |= 1 << 11;
      printf("\n%s:Init/Deinit failed for mode:%d", __func__, mode);
      s32RetVal = ADR_ERROR;
   }
   else
   {
     /* set boot mode */
      if (mode == BOOT_MODE_SPI) 
      {
         s32RetVal = s32SetGpioState(ADR_BOOTSEL0_GPIO_VALUE_PATH, 1);
      } 
      else 
      {
         s32RetVal = s32SetGpioState(ADR_BOOTSEL0_GPIO_VALUE_PATH, 0);
      }
      if (s32RetVal != 0)
      {
         rAdr_global.u32Adr_err_count |= 1 << 12;
         printf("\n%s:BOOTSEL0 set failed for mode: %d", __func__, mode);
         s32RetVal = ADR_ERROR;
      }
      else
      {
         s32RetVal = s32SetGpioState(ADR_BOOTSEL1_GPIO_VALUE_PATH, 0);
         if (s32RetVal != 0)
         {
            rAdr_global.u32Adr_err_count |= 1 << 13;
            printf("\n%s:BOOTSEL1 set failed for mode: %d", __func__, mode);
            s32RetVal = ADR_ERROR;
         }
      }
   }
   if (!s32RetVal)
   {
      /* activate reset */
      s32RetVal = s32SetGpioState(ADR_RESET_GPIO_VALUE_PATH, 1);
      if (s32RetVal != 0)
      {
         rAdr_global.u32Adr_err_count |= 1 << 14;
         printf("\n%s:Reset pin set-1 failed for mode: %d", __func__, mode);
         s32RetVal = ADR_ERROR;
      }
      else
      {
         s32RetVal = s32SetGpioState(ADR_RESET_GPIO_VALUE_PATH, 0);
         if (s32RetVal != 0)
         {
            rAdr_global.u32Adr_err_count |= 1 << 15;
            printf("\n%s:Reset pin set-0 failed for mode: %d", __func__, mode);
            s32RetVal = ADR_ERROR;
         }
      }
   }   
   if (!s32RetVal)
   {
      /* ADR needs to be at least 5 msec in reset */
      usleep(10000);
      /* enable request trigger edge */
      if (mode == BOOT_MODE_SPI)
      {
         s32RetVal = s32SetGpioEdge(ADR_REQ_GPIO_EDGE_PATH, "falling");
         if (s32RetVal != 0)
         {
            rAdr_global.u32Adr_err_count |= 1 << 16;
            printf("\n%s:GPIO edge set failed for mode: %d", __func__, mode);
            s32RetVal = ADR_ERROR;
         }
      }
      if (!s32RetVal)
      {
         /* release reset */
         s32RetVal = s32SetGpioState(ADR_RESET_GPIO_VALUE_PATH, 1);
         if(s32RetVal != 0)
         {
            rAdr_global.u32Adr_err_count |= 1 << 17;
            printf("\n%s:Reset pin release failed for mode: %d", 
                                                      __func__, mode);
            s32RetVal = ADR_ERROR;
         }
         if((!s32RetVal) && (mode == BOOT_MODE_NORMAL))
         {
            usleep(1500000);/*delay required to ensure ADR boot in normal mode*/
         }
         else if((!s32RetVal) && (mode == BOOT_MODE_SPI))
         {
            usleep(30000);/*delay required to ensure ADR boot in download mode*/
         }
      }
   }
   
   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : s32IOReadADR
*
* PARAMETER    : pBuffer - pointer to the buffer data
*                u32Size - contains the size to read.
*
* RETURNVALUE  : tS32 - total size of the data read/failure
*
*
* DESCRIPTION  :  This function reads the data from the ADR upto the size and 
*                 updates it into the buffer.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
tS32 s32IOReadADR(tU8 * pBuffer, tU32  u32Size)
{
   tS32  s32RetVal = 0;
   tS32  s32Len;
   tS32  s32ReqVal;
   tU8   u8Len_bytes_to_read[2]={0,0};
   tU16  u16Length = 0;
   tU8   u8Data[C_MAX_STATUS_LENGTH+2];
   
   /*
    * TODO: required to unblock the poll call on reset?
    */
   rPollFdSet[0].revents = 0;
   s32Len = poll(rPollFdSet, 1, (tS32)u32ADR3ReadTimeout);
   if (s32Len < 0)
   {
      rAdr_global.u32Adr_err_count |= 1 << 18;
      printf("\n%s:poll failed:%d", __func__, errno);
      s32RetVal = ADR_ERROR;
   }
   if (s32Len == 0)
   {
      rAdr_global.u32Adr_err_count |= 1 << 19;
      printf("\n%s:poll Timeout", __func__);
      s32RetVal = ADR_ERROR;
   }
   else
   {
      if (rPollFdSet[0].revents & POLLPRI) 
      {
         s32Len = lseek(rPollFdSet[0].fd, 0, SEEK_SET);
         if (s32Len < 0)
         {
            rAdr_global.u32Adr_err_count |= 1 << 20;
            printf("\n%s:lseek failed:%d", __func__, errno);
            s32RetVal = ADR_ERROR;
         }
         if (!s32RetVal)
         {
            s32Len = s32GetGpioState(rPollFdSet[0].fd, &s32ReqVal);
            if (s32Len != 0)
            {
               rAdr_global.u32Adr_err_count |= 1 << 21;
               printf("\n%s:Read GPIO state failed", __func__);
               s32RetVal = ADR_ERROR;
            }
         }
      }
   }
   if (!s32RetVal)
   {
      /*
       * read frame size
       */
      s32Len = read(s32SpiDevFd, u8Len_bytes_to_read, 2);
      if (s32Len <= 0)
      {
         rAdr_global.u32Adr_err_count |= 1 << 22;
         printf("\n%s:read length SPIDEV failed:%d", __func__, errno);
         s32RetVal = ADR_ERROR;
      }
      else
      {
         u16Length = (u8Len_bytes_to_read[0]<<8) | u8Len_bytes_to_read[1];
         if (u16Length == 0)
         {
            rAdr_global.u32Adr_err_count |= 1 << 23;
            printf("\n%s:read length is 0", __func__);
            s32RetVal = ADR_ERROR;
         }
      }
   }
   if (!s32RetVal)
   {
      if (u16Length > C_MAX_STATUS_LENGTH)
      {
         u16Length = C_MAX_STATUS_LENGTH;
      }
      /*
       * read actual data
       */
      s32Len = read(s32SpiDevFd, u8Data, u16Length-2);
      if (s32Len <= 0)
      {
         rAdr_global.u32Adr_err_count |= 1 << 24;
         printf("\n%s:read data SPIDEV failed:%d", __func__, errno);
         s32RetVal = ADR_ERROR;
      }
      else
      {
         /* limit data length to application buffer size */
         if (u16Length > u32Size)
         {
            u16Length = (tU16)u32Size;
         }
         pBuffer[0] = u8Len_bytes_to_read[0];
         pBuffer[1] = u8Len_bytes_to_read[1];
         memcpy(&pBuffer[2], u8Data, u16Length-2);   
#ifdef ADR3CTRL_VERBOSE_TRACE
         tS32  s32Count;
         fprintf(stderr, "read buffer:");
         for (s32Count = 0; s32Count < u16Length; s32Count++)
            fprintf(stderr, " %02x", (tU8)pBuffer[s32Count]);
         fprintf(stderr, "\n");
#endif
         s32RetVal = u16Length;
      }
   }
   
   return(s32RetVal);
}

/******************************************************************************
* FUNCTION     : s32IOWriteADR
*
* PARAMETER    : pBuffer - pointer to the buffer data
*                u32Size - contains the size to write.
*
* RETURNVALUE  : tS32 - total size of the data written/failure
*
*
* DESCRIPTION  :  This function writes the data from the buffer upto the size  
*                 into the ADR.
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
tS32 s32IOWriteADR(const tU8 * pcs8Buffer, tU32  u32Size)
{
   tS32  s32RetVal = 0;
   tS32  s32Len;

#ifdef ADR3CTRL_VERBOSE_TRACE
   tS32  s32Count;
   if(s32WriteCnt % 100 == 0)
      printf( "size=%d cnt=%d", u32Size, s32WriteCnt);
   s32WriteCnt++;  
   if(u32Size < 100) 
   {
      fprintf(stderr, "write buffer:");
      for (s32Count = 0; s32Count < u32Size; s32Count++)
         fprintf(stderr, " %02x", (tU8)pcs8Buffer[s32Count]);
      fprintf(stderr, "\n");
   }
#endif
   s32Len = write(s32SpiDevFd, pcs8Buffer, u32Size);
   if (s32Len <= 0)
   {
      rAdr_global.u32Adr_err_count |= 1 << 25;
      printf("\n%s:write SPIDEV failed:%d", __func__, errno);
      s32RetVal = ADR_ERROR;
   }
   else
   {
      s32RetVal = s32Len;
   }
   
   return(s32RetVal);
}
