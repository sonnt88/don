/************************************************************************
| FILE:         inc_demo.h
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  PC tool for simulating INC messages
|				and demonstration of Gen4 INC.
|				Refer Readme.txt for usage of INC PC tool
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2016 Robert Bosch GmbH| HISTORY:
| Date      | Modification               | Author
| 24.01.13  | Initial revision           | Mai Daftedar
| 17.10.16	| Fix RADAR and lint issues	 | Venkatesh Parthasarathy
| --.--.--  | ----------------           | -------, -----
|*****************************************************************************/
#ifndef INC_DEMO_H
#define INC_DEMO_H

#ifdef __cplusplus
extern "C" {
#endif

#define USE_DGRAM_SERVICE
#define DEMO_ENABLE_DEBUG



#ifdef OSAL_GEN4

#include <sys/epoll.h>	

#ifdef USE_DGRAM_SERVICE
#include "dgram_service.h"
/*Used for clients, where we have only one dgram and we connect to one server*/
sk_dgram * dgram;
#endif


#define AF_BOSCH_INC_ADR      AF_INET

#define AF_BOSCH_INC_AUTOSAR  AF_BOSCH_INC_LINUX /* AF_INC */
#define AF_BOSCH_INC_LINUX    AF_INET

#define NONBLOCKING_SOCKET 15
#define BLOCKING_SOCKET    16

#define MIN_PORT_NUM	0xC700
#define MAX_PORT_NUM  	0xC7FF

#define DEFAULT_LEN 	400
#define TEST_FAIL  		-1
#define TEST_SUCCESS 	 0

#define FIXED_PATTERN  		"AA"
#define ALTERNATE_PATTERN   "AAAEBEBEBAAA"
#define PATTERN_LENGTH		256


/*Test modes*/
#define CONTINOUSTEST    	1
#define NONCONTINOUSTEST  	2

#define MAX_NUM_CLIENT 10

#define MAX_NUM_EVENTS  20

#define PR_ERR(fmt, args...) \
      { fprintf(stderr, " %s: " fmt, __func__, ## args);}

#ifdef DEMO_ENABLE_DEBUG
#define PR_MSG(fmt, args...) \
      { fprintf(stdout, " %s: " fmt, __func__, ## args);}
#else
#define PR_MSG(fmt, args...)
#endif




/*GEN4 Test Modes*/
typedef enum
{
	FIXED=1,
	ALTERNATE,
	PREDEFINED

} enuTxPattern;

typedef enum
{
	SENDER=10,
	RECV,
	DEFAULT
} enuTxRxSpeed;

typedef enum
{
	CLIENT=0,
	SERVER
}enuOperationMode;

typedef enum
{
	ER_INVALID_PORT = -20,
	ER_PORT_INUSE,
	ER_REMOTE_HOST_FAIL,
	ER_LOCAL_HOST_FAIL,
	ER_DGRAM_INIT,
	ER_EXCEED_NUM_CLIENTS,
	/*Socket closed while data was being sent/received*/
	ER_BAD_FD,
	ER_EXCEED_SIZE
}enuIncErrors;

typedef enum
{
	IDLE=0,
	CONNECTED,
	DISCONNECTED
}enuStates;
typedef struct
{
	int client_sk;
	/*Pointer to the skdgram struct for the maximum number of sockets allowed*/
	sk_dgram *dgram;
	enuStates state;
}listenerlst;

typedef struct
{
	int sockfd;
	int portno;
	int multiclient;
	int epfd;	
	int socktype;
}serverparam;

typedef struct
{
	int sockfd;
	int portno;
	int localportno;
	int socktype;
	char *pattern;
	int mode;
}clientparam;

typedef struct
{
	int epfd;
    /*epoll structure that will contain the current network socket and event when epoll wakes up*/
    struct epoll_event *events;
}epollstruct;

/*Mainitaining a list of all the clients we are listening to*/
listenerlst strlistenerlst[MAX_NUM_CLIENT];




/*Global socket id for either operationmodes(client or Server)*/
char buffer[DEFAULT_MSGSZ+1];
char pattern[PATTERN_LENGTH];
char local_name[PATTERN_LENGTH];
char intf_name[PATTERN_LENGTH];
int client_number;
enuStates clientstate;
enuOperationMode operationMode;



int startserver(serverparam* strstructparam);
int clientstart(clientparam * strclientparam,int epfd);
int inc_echotest(int sockid, int buflen,int mode,char * pattern,int loopflag,sk_dgram *skdgram);
int inc_delaysendrecv(int commFd,int buflen,char * buff,int delay,int testmode,int loopflag,sk_dgram *skdgram);
int inc_verifypattern(int buflen,char* buff,int ptrnlen,char * pattern);
void eventhandle(epollstruct* strpoll);
void inc_handleblockmode(int sockid, sk_dgram *socketdgram);
#ifdef __cplusplus
}
#endif


#endif /* OSAL_GEN4 */

#endif /* INC_DEMO_H */
