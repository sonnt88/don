/************************************************************************
| FILE:         inc_sim_adc.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Test program for simulating a remote ADC port extender for INC.
|
|               Setup for test with ADC_INC kernel module and fake device:
|
|               modprobe dev-fake
|               /opt/bosch/base/bin/inc_sim_adc_out.out
|               modprobe adc_inc
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2012 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 10.02.13  | Initial revision           | ant3kor
| 11.03.13  | Use datagram service	     | ant3kor
| 01.04.13  | Use host name, update desc | ant3kor
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#define USE_DGRAM_SERVICE

#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_port_extender_adc.h"
#ifdef USE_DGRAM_SERVICE
#include "dgram_service.h"
#endif

/*Reject reasons not added*/

#define ADC_INC_STATUS_ACTIVE         0x01
#define ADC_INC_RESOLUTION_10BIT      0x0a
#define ADC_INC_MAX_CHANNELS          256

/*As of now below defines are not used*/
/*#define ADC_INC_STATUS_INACTIVE       0x02 */
/* #define ADC_INC_RESOLUTION_12BIT      0x0c */

#define false 0
#define true 1

struct bytewidelimit
{
    uint8_t lowbyte;
    uint8_t highbyte;
};
typedef union stlimit
{
    uint16_t limit;
    struct bytewidelimit lpart;
} threshold;

struct adcTh
{
    threshold upperlimit;
    threshold lowerlimit;
};
struct adcTh AdcThreshold[ADC_INC_MAX_CHANNELS];

typedef enum
{
    en_adc_upperthreshold = 1,
    en_adc_lowerthreshold
}encomparision;

struct adcsetThreshold
{
    encomparision compare;
    threshold thval;
};
struct adcsetThreshold adcsetTh[ADC_INC_MAX_CHANNELS];

int adc_is_threshod_set = false;

void *tx_thread(void *arg)
{
    int n, fd;
    char sendbuf[1024] = {0};
    int adcid = 0, sendflag = 0;

    /*To make lint happy*/
    fd = 0;

#ifdef USE_DGRAM_SERVICE
   sk_dgram *dgram = (sk_dgram *) arg;
#else
   int fd = (int) arg;
#endif

    sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_THRESHOLD_HIT_MSGID;

    do
    {
        usleep(100000);
        sendflag = 0;
        if(1 == adcsetTh[adcid].compare) {
            sendbuf[1] = adcid;
            sendbuf[2] = 0x01;
            sendbuf[3] = 0x00;
            sendflag = 1;
        }
        else if(2 == adcsetTh[adcid].compare) {
            sendbuf[1] = adcid;
            sendbuf[3] = 0x01;
            sendbuf[2] = 0x00;
            sendflag = 1;
        }else {;}
        adcid++;
        if(adcid > ADC_INC_MAX_CHANNELS)
        {
            adcid = 0;
        }
        if(sendflag)
        {
#ifdef USE_DGRAM_SERVICE
         n = dgram_send(dgram, sendbuf, 4);
#else
         n = send(fd, sendbuf, 4, 0);
#endif
            if (n == -1)
            {
                fprintf(stderr, "INC_sendto failed: %d\n", n);
                break;
            }
            fprintf(stderr, "<- R_THRESHOLD_HIT chid: %d adcid = %d\n", sendbuf[1],adcid);
        }

    } while (1);
   return 0;
}

int main()
{
    int m, n, fd, ret, adcid;
    unsigned char sendbuf[1024];
    unsigned char recvbuf[1024];
    pthread_t tid;
   /* struct sockaddr_in local, remote; */
#ifdef USE_DGRAM_SERVICE
   sk_dgram *dgram;
#endif
   struct hostent *local, *remote;
   struct sockaddr_in local_addr, remote_addr;

   local = gethostbyname("fake1-local");
   if (local == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(fake1-local) failed: %d\n", errsv);
      return 1;
   }
   local_addr.sin_family = AF_INET;
   memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);   
   /*local.sin_addr.s_addr = inet_addr("192.168.4.1");*/
   local_addr.sin_port = htons(PORT_EXTENDER_ADC_PORT); /*From inc_ports.h*/
	remote = gethostbyname("fake1");
   	if (remote == NULL)
   	{
    	int errsv = errno;
      	fprintf(stderr, "gethostbyname(fake1) failed: %d\n", errsv);
      	return 1;
   	}
   remote_addr.sin_family = AF_INET;
   memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
   /*remote.sin_addr.s_addr = inet_addr("192.168.4.2");*/
   remote_addr.sin_port = htons(PORT_EXTENDER_ADC_PORT); /* from inc_ports.h */

    fprintf(stderr, "local %s:%d\n", inet_ntoa(local_addr.sin_addr), ntohs(local_addr.sin_port));
    fprintf(stderr, "remote %s:%d\n", inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));

    fd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
    if (fd == -1)
    {
        int errsv = errno;
        fprintf(stderr, "create socket failed: %d\n", errsv);
        perror("CreateSocket");
		      return 1;
	}
#ifdef USE_DGRAM_SERVICE
   dgram = dgram_init(fd, DGRAM_MAX, NULL);
   if (dgram == NULL)
   {
      fprintf(stderr, "dgram_init failed\n");
      return 1;
   }
#endif
//lint -e64
   ret = bind(fd, (struct sockaddr *) &local_addr, sizeof(local_addr));
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "bind failed: %d\n", errsv);
      return 1;
   }

   ret = connect(fd, (struct sockaddr *) &remote_addr, sizeof(remote_addr));
    if (ret < 0)
    {
        int errsv = errno;
        fprintf(stderr, "connect failed: %d\n", errsv);
        perror("Connect failed:");
        return 1;
    }

#ifdef USE_DGRAM_SERVICE
   if (pthread_create(&tid, NULL, tx_thread, (void *) dgram) == -1)
      fprintf(stderr, "pthread_create failed\n");
#else
   if (pthread_create(&tid, NULL, tx_thread, (void *) fd) == -1)
      fprintf(stderr, "pthread_create failed\n");
#endif

   do {
      do {
#ifdef USE_DGRAM_SERVICE
         n = dgram_recv(dgram, recvbuf, sizeof(recvbuf));
#else
         n = recv(fd, recvbuf, sizeof(recvbuf), 0);
#endif
      } while(n < 0 && errno == EAGAIN);
      if (n < 0)
      {
         int errsv = errno;
         fprintf(stderr, "recv failed: %d\n", errsv);
         return 1;
      }

        fprintf(stderr, "recv: %d\n", n);

        if (n == 0)
        {
            fprintf(stderr, "invalid msg size: %d\n", n);
            return 1;
        }

        switch (recvbuf[0])
        {
        case SCC_PORT_EXTENDER_ADC_C_COMPONENT_STATUS_MSGID:
            if (n != 2) {
                fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
                return 1;
            }
            if (recvbuf[1] != ADC_INC_STATUS_ACTIVE) {
                fprintf(stderr, "invalid status: %d\n", recvbuf[1]);
                return 1;
            }
            fprintf(stderr, "-> C_COMPONENT_STATUS: %d\n", recvbuf[1]);

            sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_COMPONENT_STATUS_MSGID;
            sendbuf[1] = ADC_INC_STATUS_ACTIVE;
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 2);
#else
            m = send(fd, sendbuf, 2, 0);
#endif
            if (m == -1)
            {
                int errsv = errno;
                fprintf(stderr, "send failed: %d\n", errsv);
                return 1;
            }
            fprintf(stderr, "<- R_COMPONENT_STATUS: %d\n", sendbuf[1]);
            break;

        case SCC_PORT_EXTENDER_ADC_C_GET_SAMPLE_MSGID:
            if (n != 1) {
                fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
                return 1;
            }
            fprintf(stderr, "-> C_GET_SAMPLE: %d\n", recvbuf[1]);

            sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_GET_SAMPLE_MSGID;
            sendbuf[1] = 0x05;
            sendbuf[2] = 0x01;	/*ADC 0 sample*/
            sendbuf[3] = 0x23;
            sendbuf[4] = 0x04;	/*ADC 1 sample*/
            sendbuf[5] = 0x05;    
            sendbuf[6] = 0x06;	/*ADC 2 sample*/
            sendbuf[7] = 0x07;
            sendbuf[8] = 0x08;	/*ADC 3 sample*/
            sendbuf[9] = 0x11;
            sendbuf[10] = 0x00;	/*ADC 4 sample*/
            sendbuf[11] = 0xAA;
			
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 12);
#else
         m = send(fd, sendbuf, 12, 0);
#endif

           if (m == -1)
            {
                int errsv = errno;
                fprintf(stderr, "send failed: %d\n", errsv);
                return 1;
            }
            fprintf(stderr, "<- R_GET_SAMPLE: %d\n", sendbuf[1]);

            /*ADC_is_active = 1;*/
            break;

        case SCC_PORT_EXTENDER_ADC_C_SET_THRESHOLD_MSGID:
            adc_is_threshod_set = true;
            if(n != 5)
            {
                fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);	  
            }
            fprintf(stderr, "-> C_SET_THRESHOLD channelid: %d\n", recvbuf[1]);
            adcid = recvbuf[1];

            adcsetTh[adcid].compare = (encomparision)recvbuf[2];  /*Copy limit id*/
            adcsetTh[adcid].thval.lpart.lowbyte = recvbuf[3]; /*Copy lower threshold byte*/
            adcsetTh[adcid].thval.lpart.highbyte = recvbuf[4]; /*Copy higher threshold byte*/
            
            /*Storing data for threshold hit*/
            if(recvbuf[2] == 1){
                AdcThreshold[adcid].upperlimit.lpart.lowbyte = recvbuf[3];
                AdcThreshold[adcid].upperlimit.lpart.highbyte = recvbuf[4];
                AdcThreshold[adcid].lowerlimit.limit = 0;
            }else if(recvbuf[2] == 2){
                AdcThreshold[adcid].upperlimit.limit = 0;
                AdcThreshold[adcid].lowerlimit.lpart.lowbyte = recvbuf[3];
                AdcThreshold[adcid].lowerlimit.lpart.highbyte = recvbuf[4];
            }else {
                fprintf(stderr,"Wrong enum for Threshold comparision\n");
                AdcThreshold[adcid].upperlimit.limit = 0;
                AdcThreshold[adcid].upperlimit.limit = 0;
            }

            fprintf(stderr,"Receive buf : %x %x %x %x %x \n",\
                recvbuf[0],recvbuf[1],recvbuf[2],recvbuf[3],recvbuf[4]);

            fprintf(stderr,"adc id : %d upperlimit: %d lower limit %d \n",\
                adcid,AdcThreshold[adcid].upperlimit.limit,AdcThreshold[adcid].lowerlimit.limit);

            sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_SET_THRESHOLD_MSGID;
            sendbuf[1] = adcid;
            sendbuf[2] = adcsetTh[adcid].compare ;
            sendbuf[3] = adcsetTh[adcid].thval.lpart.lowbyte;
            sendbuf[4] = adcsetTh[adcid].thval.lpart.highbyte;

            fprintf(stderr,"Send buf : %x %x %x %x %x %x \n",\
                sendbuf[0],sendbuf[1],sendbuf[2],sendbuf[3],sendbuf[4]);

#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 5);
#else
         m = send(fd, sendbuf, 5, 0);
#endif

            if (m == -1){
                int errsv = errno;
                fprintf(stderr, "send failed: %d\n", errsv);
                return 1;
            }
            fprintf(stderr, "<- R_SET_THRESHOLD: channelid %d\n", sendbuf[1]);

            break;
        case SCC_PORT_EXTENDER_ADC_C_GET_CONFIG_MSGID:
            if(n != 1)
            {
                fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
            }
            fprintf(stderr, "-> C_GET_CONFIG: %d\n", recvbuf[1]);

            sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_GET_CONFIG_MSGID;
            sendbuf[1] = ADC_INC_RESOLUTION_10BIT;

#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 2);
#else
         m = send(fd, sendbuf, 2, 0);
#endif

            if(-1 == m){
                fprintf(stderr, "INC_sendto failed: %d\n", m);	  
            }

            fprintf(stderr, "<- R_GET_CONFIG: %d\n", sendbuf[1]);
            break;
        default:
            fprintf(stderr, "invalid msgid: 0x%02X\n", recvbuf[0]);
            return 1;
        }

    } while (n >= 0);

#ifdef USE_DGRAM_SERVICE
   ret = dgram_exit(dgram);
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "dgram_exit failed: %d\n", errsv);
      return 1;
   }
#endif

   close(fd);

   return 0;
}
