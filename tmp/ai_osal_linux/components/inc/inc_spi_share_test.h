/******************************************************************************
 *
 * \file          inc_spi_share_test.h
 * \brief         This file has the macro definition and the structure 
 *                declaration to be used by the spi share stress test app.
 *
 * \project       IMX6 (ADIT GEN3) JAC & MIB
 *
 * \authors       dhs2cob
 *
 * COPYRIGHT      (c) 2014 Bosch CarMultimedia GmbH
 *
 * HISTORY      :
 *------------------------------------------------------------------------------
 * Date         |        Modification           | Author & comments
 *--------------|-------------------------------|---------------------------
 * 22.Aug.2014  |  Initial s32Version           | Suresh Dhandapani (RBEI/ECF5)
 * -----------------------------------------------------------------------------
|-----------------------------------------------------------------------*/

#ifndef __SPI_SHARE__H__
#define __SPI_SHARE__H__

#include <semaphore.h>

#ifdef __cplusplus
extern "C" {
#endif

#define ADR_IFNAME_DEVICE_TREE_PATH                "/proc/device-tree/board-configuration/inc/adr3/interface-name"
#define SPI_DEVICE_PATH                            "/dev/spidev_adr3"
#define ADR_RESET_GPIO_VALUE_PATH                  "/sys/devices/virtual/boardcfg/boardcfg/embeddedradio-gpio-adr-reset/value"
#define ADR_BOOTSEL0_GPIO_VALUE_PATH               "/sys/devices/virtual/boardcfg/boardcfg/embeddedradio-gpio-adr-boot-select0/value"
#define ADR_BOOTSEL1_GPIO_VALUE_PATH               "/sys/devices/virtual/boardcfg/boardcfg/embeddedradio-gpio-adr-boot-select1/value"
#define ADR_REQ_GPIO_VALUE_PATH                    "/sys/class/gpio/gpio24/value"
#define ADR_REQ_GPIO_EDGE_PATH                     "/sys/class/gpio/gpio24/edge"
#define CMD_REQ_GPIO_EXPORT                        "echo 24 > /sys/class/gpio/export"
#define CMD_REQ_GPIO_UNEXPORT                      "echo 24 > /sys/class/gpio/unexport"
#define CMD_SPIDEV_START                           "/sbin/modprobe -r -q spidev; /sbin/modprobe spidev bufsiz=20480"
#define U32_MAX_DECIMALS                           11 /* max nof decimals of tS32 + 1 */
#define INT_MAX_DECIMALS                           10 /* max nof decimals of tS32 + 1 */
#define C_MAX_STATUS_LENGTH                        1000

/*#define AMFMTUNER_PORT   0xC714 //AMFMtuner */
#define AMPLIFIER_PORT     0xC716 /* Amplifier */
#define CD_MASCA_DEVICE    "/dev/sr0"
#define ADR_POLL_TIMEOUT   2000
#define ADR_ERROR          -1
#define CD_ERROR           -1
#define ADR_SUCCESS        0
#define CD_SUCCESS         0
#define SPI_SHARE_SUCCESS  0
#define SPI_SHARE_FAILURE  -1

/*Ensure CD test runs along with ADR till end*/
#define SYNC_WITH_ADR      105
#define SYNC_WITH_CD       200
#define LOOP_CNT_DEF       50
#define ADR_SUCCESS_BYTE   0x00
#define ADR_IN_PROCESS_1   0x01
#define ADR_IN_PROCESS_2   0x02

struct CMD
{
   tU8 u8Cmd[50];
   size_t u32Size;
   tU32 u32Reply_len;
};

struct ADR
{
   tU32 u32Adr_err_count;/*check error in ADR*/
   tU32 u32Adr_tst_cnt;/*loop s32Count*/
   tS32 s32Adr_poll_timeout;/*To ensure startup msg is received*/
   tS32 s32Terminate;/*flag to exit receiver thread*/
   pthread_t tid_adr_snd;
   pthread_t tid_adr_rcv;
   pthread_t tid_adr_down;
   sem_t sem_adr;
   tS32 s32Check_adr;/*holds a byte data to be checked with response bytes*/
   tS32 s32Total_cmd_adr;/*calculate total sending commands to adr*/
   struct CMD rAdr_ctrl[50];/*holds actual msg data send to ADR*/
};

struct CD
{
   tU32 u32Cd_err_count;/*check error in CD*/
   tU32 u32Cd_tst_cnt;/*loop s32Count*/
   tS32 s32Cd_available;/*At present it is 0, may require in future*/
   pthread_t tid_cd;
   struct CMD rCd_ctrl[50];/*holds actual msg data send to CD*/
};

typedef enum
{
   BOOT_MODE_NORMAL = 0, /* Normal mode */
   BOOT_MODE_SPI = 1 /* Download mode */
} enAdrBootMode;

#ifdef __cplusplus
}
#endif

#endif /*__SPI_SHARE__H__*/
