/************************************************************************
| FILE:         inc_sim_input.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Test program for simulating remote input devices for INC.
|
|               Setup for test with INPUT_INC kernel module and fake device:
|
|               modprobe dev-fake
|               /opt/bosch/base/bin/inc_sim_input_out.out
|               rmmod input-inc
|               modprobe input-inc of_id=rbcm,input-inc-test
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 11.03.13  | Initial revision           | tma2hi
| 21.03.13  | Added devices              | gs77hi
| 27.06.13  | Add support for protocol v2| tma2hi
| 19.06.14  | Add MT type B device       | tma2hi
|
|*****************************************************************************/

#define USE_DGRAM_SERVICE

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_input_device.h"
#include "linux/input.h"

#ifdef USE_DGRAM_SERVICE
#include "dgram_service.h"
#endif

#define INPUT_INC_STATUS_ACTIVE        0x01
#define INPUT_INC_PROTOCOL_VERSION_CUR 2
#define INPUT_INC_PROTOCOL_VERSION_MIN 1

int input_is_active = 0;

void *tx_thread(void *arg)
{
   int m0, m1, m2, m3, cycle = 0;
   char sendbuf_0[1024];
   char sendbuf_1[1024];
   char sendbuf_2[1024];
   char sendbuf_3[1024];

#ifdef USE_DGRAM_SERVICE
   sk_dgram *dgram = (sk_dgram *) arg;
#else
   int fd = (int) arg;
#endif

   /******************* Events from Dev.ID = 0 ****************/
   sendbuf_0[0] = SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID;
   sendbuf_0[1] = 1; 					/*NumEvents*/
   *(unsigned short *) (sendbuf_0 + 2) = EV_KEY;  	/*Type*/
   *(unsigned short *) (sendbuf_0 + 4) = KEY_OK;  	/*Code*/
   *(signed int *)     (sendbuf_0 + 6) = 1; 		/*Value*/

   /******************* Events from Dev.ID = 1 ****************/
   sendbuf_1[0] = SCC_INPUT_DEVICE_R_EVENT_DEV1_MSGID;
   sendbuf_1[1] = 5; 					/*NumEvents*/
   *(unsigned short *) (sendbuf_1 + 2) = EV_REL;  	/*Type*/
   *(unsigned short *) (sendbuf_1 + 4) = REL_X;  	/*Code*/
   *(signed int *)     (sendbuf_1 + 6) = 500; 		/*Value*/
   *(unsigned short *) (sendbuf_1 + 10) = EV_SYN;  	/*Type*/
   *(unsigned short *) (sendbuf_1 + 12) = SYN_REPORT;	/*Code*/
   *(signed int *)     (sendbuf_1 + 14) = 0; 		/*Value*/
   *(unsigned short *) (sendbuf_1 + 18) = EV_REL;  	/*Type*/
   *(unsigned short *) (sendbuf_1 + 20) = REL_Y;  	/*Code*/
   *(signed int *)     (sendbuf_1 + 22) = -500; 	/*Value*/
   *(unsigned short *) (sendbuf_1 + 26) = EV_SYN;  	/*Type*/
   *(unsigned short *) (sendbuf_1 + 28) = SYN_REPORT;	/*Code*/
   *(signed int *)     (sendbuf_1 + 30) = 0; 		/*Value*/
   *(unsigned short *) (sendbuf_1 + 34) = EV_REL;  	/*Type*/
   *(unsigned short *) (sendbuf_1 + 36) = REL_Z;  	/*Code*/
   *(signed int *)     (sendbuf_1 + 38) = 2; 		/*Value*/

   /******************* Events from Dev.ID = 2 ****************/
   sendbuf_2[0] = SCC_INPUT_DEVICE_R_EVENT_DEV2_MSGID;
   sendbuf_2[1] = 4; 					/*NumEvents*/
   *(unsigned short *) (sendbuf_2 + 2) = EV_ABS;  	/*Type*/
   *(unsigned short *) (sendbuf_2 + 4) = ABS_X;  	/*Code*/
   *(signed int *)     (sendbuf_2 + 6) = 200; 		/*Value*/
   *(unsigned short *) (sendbuf_2 + 10) = EV_ABS;  	/*Type*/
   *(unsigned short *) (sendbuf_2 + 12) = ABS_Y;	/*Code*/
   *(signed int *)     (sendbuf_2 + 14) = -200; 	/*Value*/
   *(unsigned short *) (sendbuf_2 + 18) = EV_SYN;  	/*Type*/
   *(unsigned short *) (sendbuf_2 + 20) = SYN_MT_REPORT;/*Code*/
   *(signed int *)     (sendbuf_2 + 22) = 0; 		/*Value*/
   *(unsigned short *) (sendbuf_2 + 26) = EV_KEY;  	/*Type*/
   *(unsigned short *) (sendbuf_2 + 28) = BTN_TOUCH;	/*Code*/
   *(signed int *)     (sendbuf_2 + 30) = 1; 		/*Value*/

   /******************* Events from Dev.ID = 3 ****************/
   sendbuf_3[0] = SCC_INPUT_DEVICE_R_EVENT_DEV3_MSGID;
   sendbuf_3[1] = 6; 						/*NumEvents*/
   *(unsigned short *) (sendbuf_3 + 2) = EV_ABS;  		/*Type*/
   *(unsigned short *) (sendbuf_3 + 4) = ABS_MT_SLOT;  		/*Code*/
   *(signed int *)     (sendbuf_3 + 6) = 0 | 1 << 8 | 0 << 16; 	/*Value*/
   *(unsigned short *) (sendbuf_3 + 10) = EV_ABS;  		/*Type*/
   *(unsigned short *) (sendbuf_3 + 12) = ABS_MT_POSITION_X;  	/*Code*/
   *(signed int *)     (sendbuf_3 + 14) = 200; 			/*Value*/
   *(unsigned short *) (sendbuf_3 + 18) = EV_ABS;  		/*Type*/
   *(unsigned short *) (sendbuf_3 + 20) = ABS_MT_POSITION_Y;	/*Code*/
   *(signed int *)     (sendbuf_3 + 22) = -200; 		/*Value*/
   *(unsigned short *) (sendbuf_3 + 26) = EV_ABS;  		/*Type*/
   *(unsigned short *) (sendbuf_3 + 28) = ABS_MT_SLOT;  	/*Code*/
   *(signed int *)     (sendbuf_3 + 30) = 1 | 1 << 8 | 0 << 16; /*Value*/
   *(unsigned short *) (sendbuf_3 + 34) = EV_ABS;  		/*Type*/
   *(unsigned short *) (sendbuf_3 + 36) = ABS_MT_POSITION_X;  	/*Code*/
   *(signed int *)     (sendbuf_3 + 38) = 400; 			/*Value*/
   *(unsigned short *) (sendbuf_3 + 42) = EV_ABS;  		/*Type*/
   *(unsigned short *) (sendbuf_3 + 44) = ABS_MT_POSITION_Y;	/*Code*/
   *(signed int *)     (sendbuf_3 + 46) = -400; 		/*Value*/


   usleep(5000000);

   do
   {
      usleep(1000000);

      if (input_is_active) 
      {
         *(signed int *)     (sendbuf_0 + 6)  ^= 1; 	/*Value*/
         *(signed int *)     (sendbuf_1 + 6)  -= 1; 	/*Value*/
         *(signed int *)     (sendbuf_1 + 22) += 1; 	/*Value*/
         *(signed int *)     (sendbuf_1 + 38) += 1; 	/*Value*/
         *(signed int *)     (sendbuf_2 + 6)  -= 1; 	/*Value*/
         *(signed int *)     (sendbuf_2 + 14) += 1; 	/*Value*/
         *(signed int *)     (sendbuf_2 + 30) ^= 1; 	/*Value*/
         if (cycle % 2 == 0)
        	 *(signed int *)     (sendbuf_3 + 6)  ^= 1 << 8;
         if (cycle % 3 == 0)
        	 *(signed int *)     (sendbuf_3 + 30) ^= 1 << 8;
         *(signed int *)     (sendbuf_3 + 14) -= 1; 	/*Value*/
         *(signed int *)     (sendbuf_3 + 22) += 1; 	/*Value*/
         *(signed int *)     (sendbuf_3 + 38) -= 1; 	/*Value*/
         *(signed int *)     (sendbuf_3 + 46) += 1; 	/*Value*/
#ifdef USE_DGRAM_SERVICE
         m0 = dgram_send(dgram, sendbuf_0, 10);
         m1 = dgram_send(dgram, sendbuf_1, 42);
         m2 = dgram_send(dgram, sendbuf_2, 34);
         m3 = dgram_send(dgram, sendbuf_3, 50);
#else
         m0 = send(fd, sendbuf_0, 10, 0);
         m1 = send(fd, sendbuf_1, 42, 0);
         m2 = send(fd, sendbuf_2, 34, 0);
         m3 = send(fd, sendbuf_3, 50, 0);
#endif
         if ((m0 == -1))
         {
            fprintf(stderr, "INC_sendto failed: %d\n", m0);
            break;
         }
         if ((m1 == -1))
         {
            fprintf(stderr, "INC_sendto failed: %d\n", m1);
            break;
         }
         if ((m2 == -1))
         {
            fprintf(stderr, "INC_sendto failed: %d\n", m2);
            break;
         }
         if ((m3 == -1))
         {
            fprintf(stderr, "INC_sendto failed: %d\n", m3);
            break;
         }
	
         /******************* Events from Dev.ID = 0 ****************/
         fprintf(stderr, "<- R_EVENT.DeviceID: %d\n", 0);
         fprintf(stderr, "<- R_EVENT.NumEvents: %d\n", sendbuf_0[1]);
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_0 + 2));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_0 + 4));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_0 + 6));
         fprintf(stderr, "--------------------\n");

         /******************* Events from Dev.ID = 1 ****************/
         fprintf(stderr, "<- R_EVENT.DeviceID: %d\n", 1);
         fprintf(stderr, "<- R_EVENT.NumEvents: %d\n", sendbuf_1[1]);
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_1 + 2));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_1 + 4));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_1 + 6));
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_1 + 10));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_1 + 12));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_1 + 14));
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_1 + 18));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_1 + 20));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_1 + 22));
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_1 + 26));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_1 + 28));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_1 + 30));
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_1 + 34));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_1 + 36));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_1 + 38));
         fprintf(stderr, "--------------------\n");

         /******************* Events from Dev.ID = 2 ****************/
         fprintf(stderr, "<- R_EVENT.DeviceID: %d\n", 2);
         fprintf(stderr, "<- R_EVENT.NumEvents: %d\n", sendbuf_2[1]);
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_2 + 2));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_2 + 4));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_2 + 6));
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_2 + 10));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_2 + 12));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_2 + 14));
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_2 + 18));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_2 + 20));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_2 + 22));
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_2 + 26));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_2 + 28));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_2 + 30));
         fprintf(stderr, "--------------------\n\n");

         /******************* Events from Dev.ID = 3 ****************/
         fprintf(stderr, "<- R_EVENT.DeviceID: %d\n", 3);
         fprintf(stderr, "<- R_EVENT.NumEvents: %d\n", sendbuf_3[1]);
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_3 + 2));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_3 + 4));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_3 + 6));
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_3 + 10));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_3 + 12));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_3 + 14));
         fprintf(stderr, "<- R_EVENT.Type: %d\n", *(unsigned short *) (sendbuf_3 + 18));
         fprintf(stderr, "<- R_EVENT.Code: %d\n", *(unsigned short *) (sendbuf_3 + 20));
         fprintf(stderr, "<- R_EVENT.Value: %d\n", *(signed int *) (sendbuf_3 + 22));
         fprintf(stderr, "--------------------\n\n");
}

      cycle++;
   } while (1);

   return 0;
}

int main()
{
   int m, n, fd, ret;
   char sendbuf[1024];
   char recvbuf[1024];
   pthread_t tid;
#ifdef USE_DGRAM_SERVICE
   sk_dgram *dgram;
#endif
   struct hostent *local, *remote;
   struct sockaddr_in local_addr, remote_addr;

   local = gethostbyname("fake1-local"); /*read IP address from /etc/hosts */
   if (local == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(fake1-local) failed: %d\n", errsv);
      return 1;
   }   
   local_addr.sin_family = AF_INET;
   memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);
   local_addr.sin_port = htons(INPUT_DEVICE_PORT); /* from inc_ports.h (50964) */

   remote = gethostbyname("fake1");
   if (remote == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(fake1) failed: %d\n", errsv);
      return 1;
   }
   remote_addr.sin_family = AF_INET;
   memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
   remote_addr.sin_port = htons(INPUT_DEVICE_PORT); /* from inc_ports.h (50964) */

   fprintf(stderr, "local %s:%d\n", inet_ntoa(local_addr.sin_addr), ntohs(local_addr.sin_port));
   fprintf(stderr, "remote %s:%d\n", inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));

   fd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
   if (fd == -1)
   {
      int errsv = errno;
      fprintf(stderr, "create socket failed: %d\n", errsv);
      return 1;
   }

#ifdef USE_DGRAM_SERVICE
   dgram = dgram_init(fd, DGRAM_MAX, NULL);
   if (dgram == NULL)
   {
      fprintf(stderr, "dgram_init failed\n");
      return 1;
   }
#endif
//lint -e64
   ret = bind(fd, (struct sockaddr *) &local_addr, sizeof(local_addr));
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "bind failed: %d\n", errsv);
      return 1;
   }

   ret = connect(fd, (struct sockaddr *) &remote_addr, sizeof(remote_addr));
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "connect failed: %d\n", errsv);
      return 1;
   }

#ifdef USE_DGRAM_SERVICE
   if (pthread_create(&tid, NULL, tx_thread, (void *) dgram) == -1)
      fprintf(stderr, "pthread_create failed\n");
#else
   if (pthread_create(&tid, NULL, tx_thread, (void *) fd) == -1)
      fprintf(stderr, "pthread_create failed\n");
#endif

   do {
      do {
#ifdef USE_DGRAM_SERVICE
         n = dgram_recv(dgram, recvbuf, sizeof(recvbuf));
#else
         n = recv(fd, recvbuf, sizeof(recvbuf), 0);
#endif
      } while(n < 0 && errno == EAGAIN);
      if (n < 0)
      {
         int errsv = errno;
         fprintf(stderr, "recv failed: %d\n", errsv);
         return 1;
      }

      fprintf(stderr, "recv: %d\n", n);

      if (n == 0)
      {
         fprintf(stderr, "invalid msg size: %d\n", n);
         return 1;
      }

      switch (recvbuf[0])
      {
      case SCC_INPUT_DEVICE_C_COMPONENT_STATUS_MSGID:
         if (n != 3) 
         {
            fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
            return 1;
         }
         if (recvbuf[1] != INPUT_INC_STATUS_ACTIVE) 
         {
            fprintf(stderr, "invalid status: %d\n", recvbuf[1]);
            return 1;
         }
         if (recvbuf[2] > INPUT_INC_PROTOCOL_VERSION_CUR ||
               recvbuf[2] < INPUT_INC_PROTOCOL_VERSION_MIN)
         {
            fprintf(stderr, "invalid protocol version: %d\n", recvbuf[2]);
            return 1;
         }
         fprintf(stderr, "-> C_COMPONENT_STATUS.HostAppStatus: %d\n", recvbuf[1]);
         fprintf(stderr, "-> C_COMPONENT_STATUS.HostAppVersion: %d\n", recvbuf[2]);
         fprintf(stderr, "--------------------\n");

         sendbuf[0] = SCC_INPUT_DEVICE_R_COMPONENT_STATUS_MSGID;
         sendbuf[1] = INPUT_INC_STATUS_ACTIVE;
         sendbuf[2] = INPUT_INC_PROTOCOL_VERSION_CUR;
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 3);
#else
         m = send(fd, sendbuf, 3, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_COMPONENT_STATUS.SCCAppStatus: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_COMPONENT_STATUS.SCCAppVersion: %d\n", sendbuf[2]);
         fprintf(stderr, "--------------------\n");

         input_is_active = 1;
         break;
      case SCC_INPUT_DEVICE_C_CONFIG_START_MSGID:
         if (n != 1) 
         {
            fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
            return 1;
         }
         fprintf(stderr, "-> C_CONFIG_START\n");
         fprintf(stderr, "--------------------\n");

	 /********************** Config Start *************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_START_MSGID;
         sendbuf[1] = 4;  					/*NumDevices*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 2);
#else
         m = send(fd, sendbuf, 2, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_START.NumDevices: %d\n", sendbuf[1]);
         fprintf(stderr, "--------------------\n");
	
         /**************************** Key device, Dev.ID = 0 *********************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID;
         sendbuf[1] = 0; 					/*Dev.ID*/
         *(unsigned short *) (sendbuf + 2) = EV_KEY; 		/*Type*/
         sendbuf[4] = 11; 					/*NumCodes*/
         *(unsigned short *) (sendbuf + 5)  = KEY_A; 		/*Code*/
         *(unsigned short *) (sendbuf + 7)  = 0x3fe; 		/*Code*/
         *(unsigned short *) (sendbuf + 9)  = KEY_ESC; 		/*Code*/
         *(unsigned short *) (sendbuf + 11) = BTN_MIDDLE; 	/*Code*/
         *(unsigned short *) (sendbuf + 13) = KEY_TUNER; 	/*Code*/
         *(unsigned short *) (sendbuf + 15) = KEY_PLAYER; 	/*Code*/
         *(unsigned short *) (sendbuf + 17) = KEY_TV; 		/*Code*/
         *(unsigned short *) (sendbuf + 19) = KEY_OK; 		/*Code*/
         *(unsigned short *) (sendbuf + 21) = KEY_BACK; 	/*Code*/
         *(unsigned short *) (sendbuf + 23) = KEY_FORWARD; 	/*Code*/
         *(unsigned short *) (sendbuf + 25) = KEY_HOME; 	/*Code*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 27);
#else
         m = send(fd, sendbuf, 27, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Type: %d\n", *(unsigned short *) (sendbuf + 2));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.NumCodes: %d\n", sendbuf[4]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 5));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 7));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 9));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 11));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 13));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 15));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 17));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 19));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 21));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 23));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 25));
         fprintf(stderr, "--------------------\n");

         /**************************** 3 Encoder, Dev.ID = 1 *********************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID;
         sendbuf[1] = 1; 					/*Dev.ID*/
         *(unsigned short *) (sendbuf + 2) = EV_REL; 		/*Type*/
         sendbuf[4] = 3; 					/*NumCodes*/
         *(unsigned short *) (sendbuf + 5)  = REL_X; 		/*Code*/
         *(unsigned short *) (sendbuf + 7)  = REL_Y; 		/*Code*/
         *(unsigned short *) (sendbuf + 9)  = REL_Z; 		/*Code*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 11);
#else
         m = send(fd, sendbuf, 11, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Type: %d\n", *(unsigned short *) (sendbuf + 2));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.NumCodes: %d\n", sendbuf[4]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 5));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 7));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 9));
         fprintf(stderr, "--------------------\n");

         /************************* Touch device (proto A) Key, Dev.ID = 2 ****************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID;
         sendbuf[1] = 2; 					/*Dev.ID*/
         *(unsigned short *) (sendbuf + 2) = EV_KEY; 		/*Type*/
         sendbuf[4] = 1; 					/*NumCodes*/
         *(unsigned short *) (sendbuf + 5)  = BTN_TOUCH; 	/*Code*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 7);
#else
         m = send(fd, sendbuf, 7, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Type: %d\n", *(unsigned short *) (sendbuf + 2));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.NumCodes: %d\n", sendbuf[4]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 5));
         fprintf(stderr, "--------------------\n");

         /************************* Touch device (proto A) Abs, Dev.ID = 2 ****************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID;
         sendbuf[1] = 2; 					/*Dev.ID*/
         *(unsigned short *) (sendbuf + 2) = EV_ABS; 		/*Type*/
         sendbuf[4] = 2; 					/*NumCodes*/
         *(unsigned short *) (sendbuf + 5)  = ABS_X; 		/*Code*/
         *(unsigned short *) (sendbuf + 7)  = ABS_Y; 		/*Code*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 9);
#else
         m = send(fd, sendbuf, 9, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Type: %d\n", *(unsigned short *) (sendbuf + 2));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.NumCodes: %d\n", sendbuf[4]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 5));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 7));
         fprintf(stderr, "--------------------\n");

         /********************** Touch device (proto A) Abs, ABS_X Dev.ID = 2 *************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_ABS_MSGID;
         sendbuf[1] = 2; 					/*Dev.ID*/
         *(unsigned short *) (sendbuf + 2)  = ABS_X; 		/*Type*/
         *(signed int *)     (sendbuf + 4)  = -444; 		/*Min*/
         *(signed int *)     (sendbuf + 8)  = 200000; 		/*Max*/
         *(signed int *)     (sendbuf + 12) = 0; 		/*Fuzz*/
         *(signed int *)     (sendbuf + 16) = 0; 		/*Flat*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 20);
#else
         m = send(fd, sendbuf, 20, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 2));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Min: %d\n", *(signed int *) (sendbuf + 4));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Max: %d\n", *(signed int *) (sendbuf + 8));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Fuzz: %d\n", *(signed int *) (sendbuf + 12));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Flat: %d\n", *(signed int *) (sendbuf + 16));
         fprintf(stderr, "--------------------\n");

         /********************** Touch device (proto A) Abs, ABS_Y Dev.ID = 2 *************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_ABS_MSGID;
         sendbuf[1] = 2; 					/*Dev.ID*/
         *(unsigned short *) (sendbuf + 2)  = ABS_Y; 		/*Type*/
         *(signed int *)     (sendbuf + 4)  = -2000000000; 	/*Min*/
         *(signed int *)     (sendbuf + 8)  = 2000000000; 	/*Max*/
         *(signed int *)     (sendbuf + 12) = 0; 		/*Fuzz*/
         *(signed int *)     (sendbuf + 16) = 0; 		/*Flat*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 20);
#else
         m = send(fd, sendbuf, 20, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 2));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Min: %d\n", *(signed int *) (sendbuf + 4));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Max: %d\n", *(signed int *) (sendbuf + 8));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Fuzz: %d\n", *(signed int *) (sendbuf + 12));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Flat: %d\n", *(signed int *) (sendbuf + 16));
         fprintf(stderr, "--------------------\n");

         /************************* Touch device (proto B) Abs, Dev.ID = 3 ****************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID;
         sendbuf[1] = 3; 					/*Dev.ID*/
         *(unsigned short *) (sendbuf + 2) = EV_ABS; 		/*Type*/
         sendbuf[4] = 2; 					/*NumCodes*/
         *(unsigned short *) (sendbuf + 5)  = ABS_MT_POSITION_X;/*Code*/
         *(unsigned short *) (sendbuf + 7)  = ABS_MT_POSITION_Y;/*Code*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 9);
#else
         m = send(fd, sendbuf, 9, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Type: %d\n", *(unsigned short *) (sendbuf + 2));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.NumCodes: %d\n", sendbuf[4]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 5));
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 7));
         fprintf(stderr, "--------------------\n");

         /************************* Touch device (proto B) Abs, Slots Dev.ID = 3 ****************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_MT_SLOTS_MSGID;
         sendbuf[1] = 3; 					/*Dev.ID*/
         sendbuf[2] = 2; 					/*NumSlots*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 3);
#else
         m = send(fd, sendbuf, 3, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_TYPE.Dev.NumSlots: %d\n", sendbuf[2]);
         fprintf(stderr, "--------------------\n");

         /********************** Touch device (proto B) Abs, ABS_MT_POSITION_X Dev.ID = 3 *************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_ABS_MSGID;
         sendbuf[1] = 3; 					/*Dev.ID*/
         *(unsigned short *) (sendbuf + 2)  = ABS_MT_POSITION_X;/*Type*/
         *(signed int *)     (sendbuf + 4)  = -444; 		/*Min*/
         *(signed int *)     (sendbuf + 8)  = 200000; 		/*Max*/
         *(signed int *)     (sendbuf + 12) = 0; 		/*Fuzz*/
         *(signed int *)     (sendbuf + 16) = 0; 		/*Flat*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 20);
#else
         m = send(fd, sendbuf, 20, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 2));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Min: %d\n", *(signed int *) (sendbuf + 4));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Max: %d\n", *(signed int *) (sendbuf + 8));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Fuzz: %d\n", *(signed int *) (sendbuf + 12));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Flat: %d\n", *(signed int *) (sendbuf + 16));
         fprintf(stderr, "--------------------\n");

         /********************** Touch device (proto B) Abs, ABS_MT_POSITION_Y Dev.ID = 3 *************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_ABS_MSGID;
         sendbuf[1] = 3; 					/*Dev.ID*/
         *(unsigned short *) (sendbuf + 2)  = ABS_MT_POSITION_Y;/*Type*/
         *(signed int *)     (sendbuf + 4)  = -2000000000; 	/*Min*/
         *(signed int *)     (sendbuf + 8)  = 2000000000; 	/*Max*/
         *(signed int *)     (sendbuf + 12) = 0; 		/*Fuzz*/
         *(signed int *)     (sendbuf + 16) = 0; 		/*Flat*/
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 20);
#else
         m = send(fd, sendbuf, 20, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.ID: %d\n", sendbuf[1]);
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 2));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Min: %d\n", *(signed int *) (sendbuf + 4));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Max: %d\n", *(signed int *) (sendbuf + 8));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Fuzz: %d\n", *(signed int *) (sendbuf + 12));
         fprintf(stderr, "<- R_CONFIG_ABS.Dev.Flat: %d\n", *(signed int *) (sendbuf + 16));
         fprintf(stderr, "--------------------\n");

         /********************** Config End *************************/
         sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_END_MSGID;
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 1);
#else
         m = send(fd, sendbuf, 1, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_CONFIG_END\n");
         fprintf(stderr, "--------------------\n\n");

         break;

      default:
         fprintf(stderr, "invalid msgid: 0x%02X\n", recvbuf[0]);
         return 1;
      }

   } while (n > 0);

#ifdef USE_DGRAM_SERVICE
   ret = dgram_exit(dgram);
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "dgram_exit failed: %d\n", errsv);
      return 1;
   }
#endif

   close(fd);

   return 0;
}
