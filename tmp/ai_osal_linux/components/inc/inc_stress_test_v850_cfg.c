/*
    header file for inc_stress_test1.c
*/

/***************************************************************************************************
**                                   Includes                                                     **
***************************************************************************************************/
#include "inc_stress_test_v850.h"

/***************************************************************************************************
**                                         CONSTANTS                                              **
***************************************************************************************************/
const lunData luns[MAX_LUN_NUM] =
{
    /*Port*/    /*lunName*/    /*messageType*/    /*testMsgName*/     /*C_compStatSize*/        /*C_compStatBuff*/    /*R_compStatSize*/     /*R_compStatBuff*/      /*C_testMsgSize*/          /*C_testMsgBuff*/       /*R_testMsgId*/    
    {0xc703,     "ADC",            "EVENT",       "COMPONENT_STATUS",        0x02,            {0x20,0x01,0x00,0x00},      0x02,            {0x21,0x01,0x00,0x00},          0x02,              {0x20,0x01,0x00,0x00},         0x21,      }, 
    {0xc702,     "GPIO",           "EVENT",       "COMPONENT_STATUS",        0x02,            {0x20,0x01,0x00,0x00},      0x02,            {0x21,0x01,0x00,0x00},          0x02,              {0x20,0x01,0x00,0x00},         0x21,      },  
    {0xc70A,     "PWM",            "EVENT",       "COMPONENT_STATUS",        0x02,            {0x20,0x01,0x00,0x00},      0x02,            {0x21,0x01,0x00,0x00},          0x02,              {0x20,0x01,0x00,0x00},         0x21,      },    
    {0xc705,     "DIA_UDD",        "EVENT",       "COMPONENT_STATUS",        0x04,            {0x20,0x01,0x01,0x01},      0x04,            {0x21,0x01,0x01,0x00},          0x04,              {0x20,0x01,0x01,0x01},         0x21,      },  
    {0xc728,     "RTC",            "EVENT",       "COMPONENT_STATUS",        0x03,            {0x20,0x01,0x01,0x00},      0x03,            {0x21,0x01,0x01,0x00},          0x03,              {0x20,0x01,0x01,0x00},         0x21,      },    
    {0xc70D,     "SUPPLYMGMT",     "EVENT",       "COMPONENT_STATUS",        0x03,            {0x20,0x01,0x01,0x00},      0x03,            {0x21,0x01,0x01,0x00},          0x03,              {0x20,0x01,0x01,0x00},         0x21,      },    
    {0xc71B,     "DIMMING",        "EVENT",       "COMPONENT_STATUS",        0x03,            {0x20,0x01,0x01,0x00},      0x03,            {0x21,0x01,0x01,0x00},          0x03,              {0x20,0x01,0x01,0x00},         0x21,      },  
    {0xc704,     "PD_NET",          "TP",         "READ_DATA_POOL",          0x03,            {0x20,0x01,0x02,0x00},      0x03,            {0x21,0x01,0x02,0x00},          0x02,              {0x34,0x01,0x00,0x00},         0x35,      },
};

/*
END OF FILE
*/