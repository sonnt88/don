/**************************************************************************
| FILE:         inc_dlt_adapter.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  DLT adapter program for sending and receiving INC messages from V850 & iMX.
|
|               This adapter will receive V850 messages via INC LUN port(0xC707) and it forwards this 
|               messages to Active DLT through local loopback in which lib passive node have waited in it.
|               App1(dltinc): bind to 192.168.3.1:50951(local), connect to 192.168.3.2:50951(remote)
|               /opt/bosch/base/bin/inc_dlt_adapter_out.out 
|
|               App2(local loopback): bind to 127.0.0.1:3491(local loopback) to connect to libdltpassiveNode.so
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2012 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 13.01.14  | Initial revision           | jov1kor
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <semaphore.h>
#include <netinet/tcp.h>
#include "inc.h"
#include "inc_ports.h"
#include "dgram_service.h"

#ifdef __cplusplus
extern "C" {
#endif

#define DLT_MAX_PACKET_SIZE   1024
#define FALSE   0
#define TRUE    1
#define TCP_HEADER_SIZE   4	

/* Configuration  */
typedef struct
{
   int s32DgrmSocFD;  /* Handle to socket  */
   int s32TcpSocFD;  /* Handle to socket  */
   int s32ConSocFD;
   sk_dgram* hldSocDgram; /*datagram handle*/
   char u8RecvBuffer[DLT_MAX_PACKET_SIZE]; /* Buffer to receive data from socket */
   char u8SendBuffer[DLT_MAX_PACKET_SIZE]; /* Buffer to receive data from socket */
   unsigned int u32RecvdMsgSize;  /* Size of latest received message */
   unsigned int u32SendMsgSize;
   unsigned int bResetRequired;
   sem_t tSemHandle;
   pthread_mutex_t tmutexptr;
   pthread_mutexattr_t tmutexattr;    
   pthread_t tidTxThread;
   pthread_t tidRxThread;
}trPassiveDLTInfo;

int connect_datagram( void );

/*-----------------------------------------------------------------------
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/

static trPassiveDLTInfo rPassiveDLTInfo; 

/* 
* Receive thread
* Receive V850 DLT messages via INC and send to local loopback port  
*/
void* rx_thread(void *arg)
{
   int n = 0;
   int ret = 0;
   char recvbuf[1024] = {0};
   unsigned char bContinue = FALSE;
   struct sockaddr_in serv_addr; 
   trPassiveDLTInfo* pstDLTInfo  = (trPassiveDLTInfo *) arg;
   
   do
   {
      (void)pthread_mutex_lock(&rPassiveDLTInfo.tmutexptr);
      rPassiveDLTInfo.bResetRequired = FALSE;
      (void)pthread_mutex_unlock(&rPassiveDLTInfo.tmutexptr); 
     
	  /*Create Socket*/
      pstDLTInfo->s32TcpSocFD = socket(AF_INET, (int)SOCK_STREAM, 0);
      if (pstDLTInfo->s32TcpSocFD == -1)
      {
        int errsv = errno;
        fprintf(stderr, "create socket failed: %d\n", errsv);
        return (void*)1;
      }
      memset(&serv_addr, '0', sizeof(serv_addr));

      serv_addr.sin_family = AF_INET;
      serv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
      serv_addr.sin_port = htons(3491); 
   
      int optval = 1;
      if(setsockopt(pstDLTInfo->s32TcpSocFD, SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval)) < 0) 
      {
         int errsv = errno;
         fprintf(stderr, "setsockopt failed: %d\n", errsv);
         return (void*)1;
      }
   
      /* bind local loopback socket/address */
//lint -e64	  
      if(bind(pstDLTInfo->s32TcpSocFD, (const struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
      {
         int errsv = errno;
         fprintf(stderr, "bind failed: %d\n", errsv);
         return (void*)1;
      }
      
      /* Go listen mode until client are connected*/
      if(listen(pstDLTInfo->s32TcpSocFD, 3) < 0)
      {
         int errsv = errno;
         fprintf(stderr, "listen failed: %d\n", errsv);
         return (void*)1;
      }
      
      /* accept connection from client*/
      pstDLTInfo->s32ConSocFD = accept(pstDLTInfo->s32TcpSocFD, (struct sockaddr*)NULL, NULL);
      if(pstDLTInfo->s32ConSocFD < 0)
      {
         int errsv = errno;
         fprintf(stderr, "accept failed: %d\n", errsv);
         return (void*)1;
      }

      /* wake tx thread to receive messages from iMX DLT*/
      sem_post(&pstDLTInfo->tSemHandle);
      do {
          do{
             /* Receive message using datagram service */
             n = dgram_recv(pstDLTInfo->hldSocDgram, 
                            recvbuf, 
                            sizeof(recvbuf));
             if(n > 0)
             {
               pstDLTInfo->u32RecvdMsgSize =  (unsigned int)n;
               /*send only required bytes */
               memcpy(pstDLTInfo->u8RecvBuffer,recvbuf,pstDLTInfo->u32RecvdMsgSize);
               send(pstDLTInfo->s32ConSocFD,
                    pstDLTInfo->u8RecvBuffer, 
                    pstDLTInfo->u32RecvdMsgSize, 
                    0);
             }
           }while(n < 0 && errno == EAGAIN);
            if (n < 0)
            {
              bContinue = TRUE;
              (void)pthread_mutex_lock(&rPassiveDLTInfo.tmutexptr);
              rPassiveDLTInfo.bResetRequired = TRUE;
              (void)pthread_mutex_unlock(&rPassiveDLTInfo.tmutexptr);
            }
            else
            {
               if (n == 0)
               {  
                  fprintf(stderr, "invalid msg size: %d\n", n);
               }
               bContinue = FALSE;
            }
        } while((n > 0)||(FALSE == rPassiveDLTInfo.bResetRequired));

        /*shutdown socket if required*/
        shutdown(pstDLTInfo->s32ConSocFD,SHUT_RDWR);
        /*close client socket*/
        close(pstDLTInfo->s32ConSocFD);
        /*close server socket*/
        close(pstDLTInfo->s32TcpSocFD);
        if(TRUE == rPassiveDLTInfo.bResetRequired)
        {
           /* connect datagram */
           ret = (int)connect_datagram();
           if (ret != 0) 
           {
             int errsv = errno;
             fprintf(stderr, "connect_datagram failed: %d\n", errsv);
             return (void*)0;
           }
        }
   }while(TRUE == rPassiveDLTInfo.bResetRequired);
   
   bContinue = bContinue;
   return (void*)0;
}

/* 
* Transmit thread
* Transmit iMX DLT messages from active daemon and send to passive daemon via INC  
*/
void* tx_thread(void *arg)
{
   int ret = 0;
   char recvbuf[1024] = {0}, recvbuf_previous[4] = {0}, recvbuf_final[1024] = {0};
   unsigned int i=0,back_up=0;
   ssize_t szRecvBytes;
   unsigned char bContinue = FALSE;
   
   trPassiveDLTInfo* pstDLTInfo  = (trPassiveDLTInfo *) arg;
 
   do{
        /*wait till client is connected*/
        sem_wait(&pstDLTInfo->tSemHandle);
        (void)pthread_mutex_lock(&rPassiveDLTInfo.tmutexptr);
        rPassiveDLTInfo.bResetRequired = FALSE;
        (void)pthread_mutex_unlock(&rPassiveDLTInfo.tmutexptr);
   
        do{
            szRecvBytes = recv(pstDLTInfo->s32ConSocFD, recvbuf, sizeof(recvbuf), 0);
            if (szRecvBytes < 0) 
            {
               int errsv = errno;
               /*Below are known recoverable error in socket programming. If anything else we have to add accordingly*/
               if((ECONNABORTED == errsv)||(ECONNRESET == errsv)||(ETIMEDOUT == errsv)||(ECONNREFUSED == errsv))
               {
                  bContinue = TRUE;
                  /*Reset in case of Recv() error*/
                  (void)pthread_mutex_lock(&rPassiveDLTInfo.tmutexptr);
                  rPassiveDLTInfo.bResetRequired = TRUE;
                  (void)pthread_mutex_unlock(&rPassiveDLTInfo.tmutexptr);
               }
               else
               {
                  bContinue = FALSE;
               }
           }
           else if(szRecvBytes > 0)
           {
               /* Send message using datagram service */
               pstDLTInfo->u32SendMsgSize = (unsigned int)szRecvBytes;
			   //Messages from DLT are sent in 3 times: pre-header, then DLT header, then data
			   if(pstDLTInfo->u32SendMsgSize == TCP_HEADER_SIZE)
				{
					strcpy(recvbuf_previous,recvbuf);
					back_up = TCP_HEADER_SIZE;										
					continue;
				}

				strcpy(recvbuf_final,recvbuf_previous);				
				recvbuf_previous[0] = '\0';
				// this case is for recieving only tcp header
				if(back_up == TCP_HEADER_SIZE)
				{						
					for(i=0; i < pstDLTInfo->u32SendMsgSize;i++)
						recvbuf_final[i+TCP_HEADER_SIZE] = recvbuf[i];						
					pstDLTInfo->u32SendMsgSize += TCP_HEADER_SIZE;
					back_up =0;																	
				}
				//this case for recieving only data (no tcp header)
				else	
					strcpy(recvbuf_final,recvbuf);														
				memcpy(pstDLTInfo->u8SendBuffer,recvbuf_final,pstDLTInfo->u32SendMsgSize);
				ret = dgram_send( pstDLTInfo->hldSocDgram,
                               pstDLTInfo->u8SendBuffer,
                               pstDLTInfo->u32SendMsgSize );
				if(ret == -1)
				{
					int errsv = errno;
					fprintf(stderr, "dgram_send failed: %d\n", errsv);
				}

			}
           else
           {
             /*socket closed in client side*/
             bContinue = TRUE;
            /*Reset in case of Recv() error*/
            (void)pthread_mutex_lock(&rPassiveDLTInfo.tmutexptr);
            rPassiveDLTInfo.bResetRequired = TRUE;
            (void)pthread_mutex_unlock(&rPassiveDLTInfo.tmutexptr);
        }
     }while(szRecvBytes > 0);
   
    }while((bContinue == TRUE)||(TRUE == rPassiveDLTInfo.bResetRequired));

   return (void*)0;
}

int connect_datagram( void )
{
   int ret = 0 ;
   struct hostent *local, *remote;
   struct sockaddr_in local_addr, remote_addr;

   if(TRUE == rPassiveDLTInfo.bResetRequired)
   {
     /* exit datagram service */
     ret = dgram_exit(rPassiveDLTInfo.hldSocDgram);
     if (ret < 0)
     {
      int errsv = errno;
      fprintf(stderr, "dgram_exit failed: %d\n", errsv);
      return 1;
     }
     shutdown(rPassiveDLTInfo.s32DgrmSocFD,SHUT_RDWR);
     close(rPassiveDLTInfo.s32DgrmSocFD);
     memset(rPassiveDLTInfo.u8RecvBuffer, 0, sizeof(rPassiveDLTInfo.u8RecvBuffer));
     memset(rPassiveDLTInfo.u8SendBuffer, 0, sizeof(rPassiveDLTInfo.u8SendBuffer));
   }
   
   /* Get local and remote addresses and ports */
   local = gethostbyname("scc-local");
   if (local == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "Reset gethostbyname(scc-local) failed: %d\n", errsv);
      return 1;
   }
   memset(&local_addr, '0', sizeof(local_addr));
   
   local_addr.sin_family = AF_INET;
   memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, (unsigned int)local->h_length);
   local_addr.sin_port = htons(DLT_PORT);/* from inc_ports.h */

   remote = gethostbyname("scc");
   if (remote == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(scc) failed: %d\n", errsv);
      return 1;
   }
   
   memset(&remote_addr, '0', sizeof(remote_addr));
   remote_addr.sin_family = AF_INET;
   memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, (unsigned int)remote->h_length);
   remote_addr.sin_port = htons(DLT_PORT);/* from inc_ports.h */
   
   /*create socket*/
   rPassiveDLTInfo.s32DgrmSocFD = socket(AF_BOSCH_INC_AUTOSAR, (int)SOCK_STREAM, 0);
   if (rPassiveDLTInfo.s32DgrmSocFD == -1)
   {
      int errsv = errno;
      fprintf(stderr, "create socket failed: %d\n", errsv);
      return 1;
   }
   /* Initialize datagram service */
   rPassiveDLTInfo.hldSocDgram = dgram_init(rPassiveDLTInfo.s32DgrmSocFD, DGRAM_MAX, NULL);
   if (rPassiveDLTInfo.hldSocDgram == NULL)
   {
      int errsrv = errno;
      fprintf(stderr, "dgram_init failed %d\n",errsrv);
      return 1;
   }
   /* Bind socket to local address/port */
   ret = bind(rPassiveDLTInfo.s32DgrmSocFD, (const struct sockaddr *) &local_addr, sizeof(local_addr));
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "INC bind failed: %d\n", errsv);
      return 1;
   }
   /* Connect to remote address/port */
   ret = connect(rPassiveDLTInfo.s32DgrmSocFD, (const struct sockaddr *) &remote_addr, sizeof(remote_addr));
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "INC connect failed: %d\n", errsv);
      return 1;
   }
   return 0;
}

/**
 * Adapter entry point 
 */
int main(void)
{
   int ret = 0 ;
   
   /* init semaphore */
   ret = sem_init(&rPassiveDLTInfo.tSemHandle, 0, 0);
   if (ret != 0) 
   {
      int errsv = errno;
      fprintf(stderr, "sem init failed: %d\n", errsv);
      return 1;
   }
   
   /* initialize a mutex to its default value */ 
   ret = pthread_mutex_init(&rPassiveDLTInfo.tmutexptr, NULL);
   if (ret != 0) 
   {
      int errsv = errno;
      fprintf(stderr, "mutex init failed: %d\n", errsv);
      return 1;
   }
   
   /* initialize a mutex attribute */ 
   ret = pthread_mutexattr_init(&rPassiveDLTInfo.tmutexattr);
   if (ret != 0) 
   {
      int errsv = errno;
      fprintf(stderr, "mutex_attr init failed: %d\n", errsv);
      return 1;
   }
   
   /* connect datagram */
   do
   {
     ret = connect_datagram();
     if (ret != 0) 
     {
       int errsv = errno;
       fprintf(stderr, "connect_datagram failed: %d\n", errsv);
     }
   }while(FALSE != ret);
   
   /*create tx and rx threads to receive and send V850&iMX DLT messages */
   if (pthread_create(&rPassiveDLTInfo.tidTxThread, NULL, tx_thread,  &rPassiveDLTInfo) == -1)
   fprintf(stderr, "pthread_create failed\n");

   if (pthread_create(&rPassiveDLTInfo.tidRxThread, NULL, rx_thread,  &rPassiveDLTInfo) == -1)
   fprintf(stderr, "pthread_create failed\n");

   memset(rPassiveDLTInfo.u8RecvBuffer,0,sizeof(rPassiveDLTInfo.u8RecvBuffer));

   pthread_join(rPassiveDLTInfo.tidTxThread,NULL);
   pthread_join(rPassiveDLTInfo.tidRxThread,NULL);
   /* exit datagram service */
   ret = dgram_exit(rPassiveDLTInfo.hldSocDgram);
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "dgram_exit failed: %d\n", errsv);
      return 1;
   }
   
   /*Destroy semaphore*/
   sem_destroy(&rPassiveDLTInfo.tSemHandle);
   
   /* destroy mutex */
   ret = pthread_mutex_destroy(&rPassiveDLTInfo.tmutexptr);
   if (ret != 0) 
   {
      int errsv = errno;
      fprintf(stderr, "mutex destroy failed: %d\n", errsv);
      return 1;
   }
   close(rPassiveDLTInfo.s32DgrmSocFD);
   
   return 0;
}
