/******************************************************************************
 *
 * \file          inc_cd_test.c
 * \brief         This file implements the test code for verifying the SPI
 *                communication by accessing inc,spidev and /dev/sr0 interfaces.
 *
 * \project       IMX6 (ADIT GEN3) JAC & MIB
 *
 * \authors       dhs2cob
 *
 * COPYRIGHT      (c) 2014 Bosch CarMultimedia GmbH
 *
 * HISTORY      :
 *------------------------------------------------------------------------------
 * Date         |        Modification           | Author & comments
 *--------------|-------------------------------|---------------------------
 * 22.May.2014  |  Initial Version              | Suresh Dhandapani (RBEI/ECF5)
 * 22.Aug.2014  |  Added verification test for  | Suresh Dhandapani (RBEI/ECF5)
 *              |  ADR in download mode with CD |
 * -----------------------------------------------------------------------------
|-----------------------------------------------------------------------*/

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <errno.h>
#include <scsi/sg.h> /* take care: fetches glibc's /usr/include/scsi/sg.h */
#include <signal.h>
#include <stdbool.h>
#include <pthread.h>
#include "inc.h"
#include "dgram_service.h"
#include "inc_ports.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "inc_spi_share_test.h"

#ifdef __cplusplus
extern "C" {
#endif

static void vSig_handler(tS32 s32Sig);
static tS32 s32Initialise_cd(tS32 *ps32Sg_fd);
static void *pvCd_thread(void *pvArg);
static tS32 s32Initialise_adr(tS32 *ps32Sockfd, sk_dgram **dgram);
static void *pvInc_snd_thread(void *pvArg);
static void *pvInc_recv_thread(void *pvArg);
static tS32 s32Check_adr_buffer(const tU8 * pu8Buffer);
static tS32 s32Init_socket(tS32 *ps32Sockfd, sk_dgram **dgram);
static tS32 s32Bind_socket(tS32 s32Ret, const tS32 *ps32Sockfd, 
                     struct sockaddr_in *rServ_addr, struct hostent **prLhost);
static tS32 s32Connect_socket(tS32 s32Ret, tS32 const *ps32Sockfd,
                  struct sockaddr_in *rRemote_addr, struct hostent **prServer);
static tS32 s32Create_threads(tS32 s32Ret, const tS32 *ps32Sockfd, 
                              sk_dgram * const *dgram);
static void s32TestCase1(void);
static void s32TestCase2(const tU8 *pu8FileName);
static void *pvADR_thread_down(void *pvArg);
static tS32 s32Initialise_adr_down(const tU8 * pu8FileName);
static tS32 s32GetChecksum(const tU8 *pu8Buff, tU32 u32len);
extern tS32 s32IOWriteADR(const tU8 * pcs8Buffer, tU32  u32Size);
extern tS32 s32IOReadADR(tU8 * pBuffer, tU32  u32Size);
extern tS32 s32ResetADR(enAdrBootMode mode);
struct ADR rAdr_global;
struct CD rCd_global;
/*flag used to exit at point in time independent of loop cnt*/
tS32 s32Unctrl_exit;
sem_t sem_start_test;
tU32 u32Loop_cnt_bk = 0; /*loop s32Count value backup*/
/******************************************************************************
* FUNCTION     : main
*
* PARAMETER    : ps8Argv - gets the command line arguments containing loop
*                          s32Count
*                s32Argc - Total number of arguments passed.
*
* RETURNVALUE  : tS32 - Error/Success
*
*
* DESCRIPTION  : This function checks the user input and executes the testcases 
*                1 and 2.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-----------------------------
* 22.May.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
tS32 main(tS32 s32Argc, tU8 * const ps8Argv[])
{   
   tS32 s32Ret = SPI_SHARE_SUCCESS;
   tU32 u32Count;
   tS8 s8ValidInput = FALSE;
   FILE *ps32Bin_fd;   
   tU8 *pu8Bin_file = NULL;   

   /*Add user signal to this process which will be added to the threads*/
   sigset_t sigs_to_block;
   sigemptyset(&sigs_to_block);
   sigaddset(&sigs_to_block, SIGUSR1);
   pthread_sigmask(SIG_BLOCK, &sigs_to_block, NULL);
   
   if(sem_init(&sem_start_test, 0, 0))
   {
      s32Ret = SPI_SHARE_FAILURE;
      printf("Sem start init failed\n");
   }
   else
   {
      /*check for input data is available*/
      switch(s32Argc)
      {
         case 1:/*No input provided*/
         {
            printf("\nPlease provide input data as mentioned in below format");
            printf("\nFormat1:'%s <Test Count> <bin file path with name>'", 
                                                               ps8Argv[0]);
            printf("\nor");
            printf("\nFormat2:'%s <bin file path with name>'", 
                                                               ps8Argv[0]);
            printf("\nNote: Format2 takes default Test count as 50\n");
            s32Ret = SPI_SHARE_FAILURE;
            break;
         }
         case 2:/*Input provided - complete bin file path*/
         {
            ps32Bin_fd = fopen(ps8Argv[1],"r");
            if(ps32Bin_fd == NULL)
            {
               printf("\nPlease provide correct path with bin file name\n");
               printf("Format:'%s <bin file path with name>'\n", ps8Argv[0]);
               s32Ret = SPI_SHARE_FAILURE;
            }
            else
            {
               fclose(ps32Bin_fd);
               s8ValidInput = TRUE;
               printf("\nNo User input Test Count data\n");
               printf("default Test Count : 50\n");
               u32Loop_cnt_bk = LOOP_CNT_DEF;
               pu8Bin_file = ps8Argv[1];
            }
            break;
         }
         case 3:/*Input provided - Test count and complete bin file path*/
         {
            ps32Bin_fd = fopen(ps8Argv[2],"r");
            if(ps32Bin_fd == NULL)
            {
               printf("\nPlease provide correct path with bin file name\n");
               printf("Format:'%s <Test Count> <bin file path with name>'\n", 
                                                               ps8Argv[0]);
               s32Ret = SPI_SHARE_FAILURE;
            }
            else
            {
               fclose(ps32Bin_fd);
               u32Count = 0;
               while (u32Count < strlen(ps8Argv[1]))
               {
                  if (!isdigit(ps8Argv[1][u32Count]))
                  {
                     break;
                  }
                  u32Count++;
               }
               if (u32Count == strlen(ps8Argv[1]))
                  s8ValidInput = TRUE;
               else
               {
                  s32Ret = SPI_SHARE_FAILURE;
                  printf("\nPlease provide proper Test count data\n");
                  printf("Format:'%s <Test Count> <bin file path with name>'\n", 
                                                               ps8Argv[0]);
               }
            }
            if (s8ValidInput)
            {
               if (atoi(ps8Argv[1]) <= 0)
               {
                  printf("Please provide proper Test Count data\n");
                  printf("Format:'%s <Test Count> <bin file path with name>'\n", 
                                                               ps8Argv[0]);
                  s8ValidInput = FALSE;
                  s32Ret = SPI_SHARE_FAILURE;
               }
               else
               {
                  u32Loop_cnt_bk = (tU32)atoi(ps8Argv[1]);
                  pu8Bin_file = ps8Argv[2];
               }
            }
            break;
         }
         default:
         {
            printf("\nPlease provide input data as mentioned in below format");
            printf("\nFormat1:'%s <Test Count> <bin file path with name>'", 
                                                               ps8Argv[0]);
            printf("\nor");
            printf("\nFormat2:'%s <bin file path with name>'", 
                                                               ps8Argv[0]);
            printf("\nNote: Format2 takes default Test count as 50\n");
            s32Ret = SPI_SHARE_FAILURE;
            break;
         }         
      }
      if(!s32Ret && s8ValidInput)
      {     
         s32TestCase1();
         s32TestCase2(pu8Bin_file);         
      }
   }   
   return s32Ret;
}

/******************************************************************************
* FUNCTION     : s32TestCase1
*
* PARAMETER    : void
*
* RETURNVALUE  : void
*
*
* DESCRIPTION  :  This test function will test both CD masca and ADR in normal
*                 mode in parallel and displays the test result at the end.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static void s32TestCase1(void)
{
   tS32 s32Sockfd = -1;
   sk_dgram *dgram;
   tS32 s32Sg_fd;
   
      /*clear memory*/
   memset(&rCd_global, 0 , sizeof(struct CD));
   memset(&rAdr_global, 0 , sizeof(struct ADR));
   rCd_global.u32Cd_tst_cnt = u32Loop_cnt_bk;
   rAdr_global.u32Adr_tst_cnt = u32Loop_cnt_bk;
   printf("\nSelected loop Count:%lu", rAdr_global.u32Adr_tst_cnt);
   printf("\nNote: Press ctrl+c if you want to stop the test");

   rCd_global.s32Cd_available = 0; /*CD not required*/
   if(!s32Initialise_cd(&s32Sg_fd))
   {
      signal(SIGINT, vSig_handler);/*handler for ctrl+c command*/
      if(!s32Initialise_adr(&s32Sockfd, &dgram))
      {
         /*wait for startup message */
         sem_wait(&sem_start_test);
         sem_destroy(&sem_start_test);
         /*signals the threads which waits for to send data*/
         pthread_kill(rCd_global.tid_cd, SIGUSR1);
         pthread_kill(rAdr_global.tid_adr_snd, SIGUSR1);
         pthread_join(rAdr_global.tid_adr_snd, NULL);
         pthread_join(rAdr_global.tid_adr_rcv, NULL);
         if (dgram_exit(dgram) < 0)
         {
            printf("dgram_exit failed");
         }
         close(s32Sockfd);
         pthread_join(rCd_global.tid_cd, NULL);
         close(s32Sg_fd);
      }
      else
      {
         s32Unctrl_exit = 1;
         pthread_kill(rCd_global.tid_cd, SIGUSR1);
         printf("\n ADR initialisation failed");
         pthread_join(rCd_global.tid_cd, NULL);
         close(s32Sg_fd);
      }
   }
   else
   {
      printf("\n CD initialisation failed");
   }
   printf("\n**************** Test case-1 Result **************************\n");
   if(!rAdr_global.u32Adr_err_count && !rCd_global.u32Cd_err_count &&
   (rAdr_global.u32Adr_tst_cnt != u32Loop_cnt_bk) &&
   (rCd_global.u32Cd_tst_cnt != u32Loop_cnt_bk))
   {
      printf("\nSUCCESS: SPI shared functionality is ok so far :)\n");
      printf("\nADR Test rounds remaining: %lu", 
                                          rAdr_global.u32Adr_tst_cnt);
      printf("\nCD Test rounds remaining: %lu\n", 
                                          rCd_global.u32Cd_tst_cnt);
   }
   else if((rAdr_global.u32Adr_tst_cnt == u32Loop_cnt_bk) &&
               (rCd_global.u32Cd_tst_cnt == u32Loop_cnt_bk))
   {
      printf("\n SPI shared functionality is unknown to me!!! :( or :) \n");
      printf("\nADR err Count:%lu", rAdr_global.u32Adr_err_count);
      printf("\nADR Test rounds remaining: %lu", 
                                          rAdr_global.u32Adr_tst_cnt);
      printf("\n\nCD err Count:%lu\n", rCd_global.u32Cd_err_count);
      printf("\nCD Test rounds remaining: %lu\n", 
                                          rCd_global.u32Cd_tst_cnt);
   }
   else
   {
      printf("\nFAILED: SPI shared functionality is not ok :(\n");
      printf("\nADR err Count:%lu", rAdr_global.u32Adr_err_count);
      printf("\nTest rounds remaining: %lu", 
                                          rAdr_global.u32Adr_tst_cnt);
      printf("\n\nCD err Count:%lu\n", rCd_global.u32Cd_err_count);
      printf("Test rounds remaining: %lu\n", rCd_global.u32Cd_tst_cnt);
   }
   printf("\n************* Test case-1 END Result *************************\n");
}

/******************************************************************************
* FUNCTION     : s32TestCase2
*
* PARAMETER    : pu8FileName - pointer to the xl_flasher662.bin file name
*
* RETURNVALUE  : void
*
*
* DESCRIPTION  :  This test function will test both CD masca and ADR in download
*                 mode in parallel and displays the test result at the end.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static void s32TestCase2(const tU8 *pu8FileName)
{
   tS32 s32Sg_fd;
   
   if(pu8FileName == NULL)
   {
      printf("\n%s:File name is NULL\n", __func__);
   }
   else
   {
         /*clear memory*/
      memset(&rCd_global, 0 , sizeof(struct CD));
      memset(&rAdr_global, 0 , sizeof(struct ADR));
      rCd_global.u32Cd_tst_cnt = u32Loop_cnt_bk;
      rAdr_global.u32Adr_tst_cnt = u32Loop_cnt_bk;
      printf("\nSelected loop Count:%lu", rAdr_global.u32Adr_tst_cnt);
      printf("\nNote: Press ctrl+c if you want to stop the test");
      rCd_global.s32Cd_available = 0; /*CD not required*/
      if(!s32Initialise_cd(&s32Sg_fd))
      {
         signal(SIGINT, vSig_handler);/*handler for ctrl+c command*/
         if(!s32Initialise_adr_down(pu8FileName))
         {
            /*signals the threads which waits for to send data*/
            pthread_kill(rCd_global.tid_cd, SIGUSR1);
            pthread_kill(rAdr_global.tid_adr_down, SIGUSR1);
            pthread_join(rAdr_global.tid_adr_down, NULL);
            pthread_join(rCd_global.tid_cd, NULL);
            close(s32Sg_fd);
         }
         else
         {
            s32Unctrl_exit = 1;
            pthread_kill(rCd_global.tid_cd, SIGUSR1);
            printf("\n ADR initialisation failed");
            pthread_join(rCd_global.tid_cd, NULL);
            close(s32Sg_fd);
         }
      }
      else
      {
         printf("\n CD initialisation failed");
      }
      printf("\n************* Test case-2 Result **************************\n");
      if(!rAdr_global.u32Adr_err_count && !rCd_global.u32Cd_err_count &&
      (rAdr_global.u32Adr_tst_cnt != u32Loop_cnt_bk) &&
      (rCd_global.u32Cd_tst_cnt != u32Loop_cnt_bk))
      {
         printf("\nSUCCESS: SPI shared functionality is ok so far :)\n");
         printf("\nADR Test rounds remaining: %lu", 
                                             rAdr_global.u32Adr_tst_cnt);
         printf("\nCD Test rounds remaining: %lu\n", 
                                             rCd_global.u32Cd_tst_cnt);
      }
      else if((rAdr_global.u32Adr_tst_cnt == u32Loop_cnt_bk) &&
                  (rCd_global.u32Cd_tst_cnt == u32Loop_cnt_bk))
      {
         printf("\n SPI shared functionality is unknown to me!!! :( or :) \n");
         printf("\nADR err Count:%lu", rAdr_global.u32Adr_err_count);
         printf("\nADR Test rounds remaining: %lu", 
                                             rAdr_global.u32Adr_tst_cnt);
         printf("\n\nCD err Count:%lu\n", rCd_global.u32Cd_err_count);
         printf("\nCD Test rounds remaining: %lu\n", 
                                             rCd_global.u32Cd_tst_cnt);
      }
      else
      {
         printf("\nFAILED: SPI shared functionality is not ok :(\n");
         printf("\nADR err Count:%lu", rAdr_global.u32Adr_err_count);
         printf("\nTest rounds remaining: %lu", 
                                             rAdr_global.u32Adr_tst_cnt);
         printf("\n\nCD err Count:%lu\n", rCd_global.u32Cd_err_count);
         printf("Test rounds remaining: %lu\n", rCd_global.u32Cd_tst_cnt);
      }
      printf("\n************* Test case-2 END Result **********************\n");
   }
}

/******************************************************************************
* FUNCTION     : s32Initialise_adr_down
*
* PARAMETER    : pu8FileName - pointer to the xl_flasher662.bin file name
*
* RETURNVALUE  : tS32 - success/failure 
*
*
* DESCRIPTION  :  This function will initialize the ADR to download mode and 
*                 makes the bin file data readily available. Once all the setup 
*                 is done, thread will be created to start the test.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32Initialise_adr_down(const tU8 * pu8FileName)
{
   tS32 s32Ret = 0;   
   tU8 *pu8Buff = NULL;
   tU32 u32Len;   
   FILE *fp = NULL;
   tU32 file_len = 0;   
   
   if(pu8FileName == NULL)
   {
      printf("\n%s:File name is NULL\n", __func__);
      s32Ret = ADR_ERROR;
   }
   else
   {
      fp = fopen(pu8FileName,"r"); /*open xl_flasher662.bin*/
      if(fp == NULL)
      {
         printf("\n %s:Bin File open error", __func__);
         s32Ret = ADR_ERROR;
      }
      else
      {
         if(fseek(fp, 0L, SEEK_END) != 0)
         {
            s32Ret = ADR_ERROR;
            fclose(fp);
            fp = NULL;
            printf("\n%s:fseek failed", __func__);
         }
         else
         {
            file_len = (tU32)ftell(fp);
         }
      }
      if(!s32Ret && fp)
      {
         if(fseek(fp, 0L, SEEK_SET) != 0)
         {
            s32Ret = ADR_ERROR;
            fclose(fp);
            fp = NULL;
            printf("\n%s:fseek failed", __func__);
         }
         else if(file_len == 0)
         {
            s32Ret = ADR_ERROR;
            fclose(fp);
            fp = NULL;
            printf("\n%s:file size is zero", __func__);
         }
      }
   }
   if(!s32Ret && fp)
   {
      printf("\n File size is :%lu" ,file_len);
      pu8Buff = (tU8 *)malloc(file_len);
      if(pu8Buff == NULL)
      {
         s32Ret = ADR_ERROR;
         fclose(fp);
         fp = NULL;
         printf("\n%s:No memory available:%d\n", __func__, errno);
      }
   }
   if (!s32Ret && fp && pu8Buff)
   {
      s32Ret = s32ResetADR(BOOT_MODE_SPI); /*change ADR into download mode*/
      if(!s32Ret)
      {
         u32Len = fread(pu8Buff,1,file_len,fp);
         fclose(fp);
         if(u32Len != file_len)
         {
            if (s32ResetADR(BOOT_MODE_NORMAL) != 0) /*change ADR into normal */
            {
               printf("\n%s:ADR normal mode set failed\n", __func__);
            }
            s32Ret = ADR_ERROR;
            free(pu8Buff);
            printf("\n%s:File data read failed", __func__);
         }
         else
         {     

            u32Len = (tU32)s32IOWriteADR(pu8Buff, file_len);
            if(u32Len != file_len)
            {
               if (s32ResetADR(BOOT_MODE_NORMAL) != 0)
               {
                  printf("\n%s:ADR normal mode set failed\n", __func__);
               }
               free(pu8Buff);
               rAdr_global.u32Adr_err_count |= (tU32)1 << 31;
               s32Unctrl_exit = 1;
               printf("\n write XL flasher failed:%lu",u32Len);
            }
            else
            {
               usleep(30000); /*min 30ms delay is required*/
               free(pu8Buff); /*free the allocated memory for pu8Buff*/
               /*Read ADR flash content command*/
               rAdr_global.rAdr_ctrl[0].u32Size = 12;
               memcpy(&rAdr_global.rAdr_ctrl[0].u8Cmd[0],
               "\x00\x0C\x48\x00\x00\x00", 6);
               memcpy(&rAdr_global.rAdr_ctrl[0].u8Cmd[6],
               "\x00\x00\x00\x00\x04\x58", 6);
               if(ADR_ERROR == pthread_create(&rAdr_global.tid_adr_down, NULL,
                                 pvADR_thread_down, NULL))
               {
                  if (s32ResetADR(BOOT_MODE_NORMAL) != 0)
                  {
                     printf("\n%s:ADR normal mode set failed\n", __func__);
                  }
                  printf("%s:Failed to create a thread", __func__);
                  rAdr_global.u32Adr_err_count |= 1 << 26;
                  s32Ret = ADR_ERROR;
               }
            }
         }
      }
      else
      {
         free(pu8Buff);
         s32Ret = ADR_ERROR;
         fclose(fp);
         printf("\n%s:ADR Download mode set failed\n", __func__);
      }
   }
   else
   {
      if(pu8Buff)
         free(pu8Buff);
   }
   
   return s32Ret;
}

/******************************************************************************
* FUNCTION     : pvADR_thread_down
*
* PARAMETER    : void
*
* RETURNVALUE  : void 
*
*
* DESCRIPTION  :  This thread function will write the xl_flasher662.bin data and 
*                 proceeds to read flash content upto the loop count.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static void *pvADR_thread_down(void *pvArg)
{
   sigset_t signals_to_catch;
   tS32 s32Caught;
   tS32 s32Count = 0;
   tS32 s32Len;
   tU8 u8Buffer[12];
   tU8 u8CheckSum = 0;
   tS32 s32Count_upto_cd = SYNC_WITH_CD;
   
   (void)pvArg;
   sigemptyset(&signals_to_catch);
   sigaddset(&signals_to_catch, SIGUSR1);
   /*calculate total commands*/
   while (rAdr_global.rAdr_ctrl[s32Count].u32Size)
   {
      s32Count++;
   }
   rAdr_global.s32Total_cmd_adr = s32Count;
   (void)rAdr_global.s32Total_cmd_adr; /*might be useful in future*/
  
     /* Wait for SIGUSR1 signal*/ 
   sigwait(&signals_to_catch, &s32Caught);
   printf("\nADR Test Running....\n");
#if 0 /*This dead code might be useful in future samples with HW fix*/
   if(!s32Unctrl_exit) /*Ensure that user has not cancelled test case before*/
   {
      /* Write the xl_flasher662.bin file data*/
      s32Len = s32IOWriteADR(rFile_info_t->pu8FileData,
                             rFile_info_t->u32File_len);
      if((tU32)s32Len != rFile_info_t->u32File_len)
      {
         rAdr_global.u32Adr_err_count |= (tU32)1 << 31;
         s32Unctrl_exit = 1;
         printf("\n write XL flasher failed:%d",s32Len);
      }
   }
#endif
   while (rAdr_global.u32Adr_tst_cnt && !s32Unctrl_exit)
   {
      s32Count = 0;
      /*Ensure ADR test runs along with CD till end*/
      while(s32Count < s32Count_upto_cd && !s32Unctrl_exit)
      {
         s32Len = s32IOWriteADR(rAdr_global.rAdr_ctrl[0].u8Cmd,
                             rAdr_global.rAdr_ctrl[0].u32Size);
         if((tU32)s32Len != rAdr_global.rAdr_ctrl[0].u32Size)
         {
            rAdr_global.u32Adr_err_count |= 1 << 29;
            s32Unctrl_exit = 1;
         }
         else
         {
            s32Len = s32IOReadADR(u8Buffer,12);
            if(s32Len > 0 )
            {   /*calculate the checksum with the received data*/         
               u8CheckSum = (tU8)s32GetChecksum(u8Buffer, (tU32)s32Len);
               /*validate the data received*/
               if((u8Buffer[2] != 0x41) || (u8Buffer[s32Len-1] != u8CheckSum) || 
               (u8CheckSum == 0))
               {
                  rAdr_global.u32Adr_err_count |= 1 << 27;
                  s32Unctrl_exit = 1;
               }
            }
            else
            {
               printf("\n%s:No response from ADR3\n", __func__);
               rAdr_global.u32Adr_err_count |= 1 << 30;
               s32Unctrl_exit = 1;
            }
            memset(u8Buffer, 0, sizeof(u8Buffer));
         }    
         s32Count++;
      }
      rAdr_global.u32Adr_tst_cnt--;
   }    
   printf("\nADR Test completed\n"); 
   if (s32ResetADR(BOOT_MODE_NORMAL) != 0) /*change ADR into normal mode*/
   {
      printf("\n%s:ADR normal mode set failed\n", __func__);
   }
   
   return ADR_SUCCESS;
}

/******************************************************************************
* FUNCTION     : s32GetChecksum
*
* PARAMETER    : pu8Buff - Received buffer data
*                u32len -  Length of the data received
*
* RETURNVALUE  : tS32 - returns the calculated checksum value.
*
*
* DESCRIPTION  :  This function will create checksum for the received data upto 
*                 the length provided.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.Aug.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32GetChecksum(const tU8 *pu8Buff, tU32 u32len)
{
   tU32 u32Count;
   tS32 s32RetVal = 0;
   
   for(u32Count=0; u32Count<(u32len-1); u32Count++)
   {
      s32RetVal = s32RetVal + pu8Buff[u32Count];
   }
   s32RetVal = s32RetVal % 256;
   
   return s32RetVal;
}

/******************************************************************************
* FUNCTION     : vSig_handler
*
* PARAMETER    : s32Sig - signal information(ctrl+c)
*
* RETURNVALUE  : void
*
*
* DESCRIPTION  :  This function will set the flag s32Unctrl_exit for exiting the
*             threads in proper manner.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.May.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static void vSig_handler(tS32 s32Sig)
{
   s32Unctrl_exit = 1;
   (void)s32Sig; /*lint*/
   printf("\nShutting down test case, Please wait...do not touch\n");
}

/******************************************************************************
* FUNCTION     : s32Initialise_cd
*
* PARAMETER    : ps32Sg_fd - parameter for MASCA file descriptor
*
* RETURNVALUE  : tS32 - Error/Success
*
*
* DESCRIPTION  :  This function will initialise the CD commands and creates
*             thread for execution
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.May.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* 22.Aug.2014  |  Remove CD media dependency   |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32Initialise_cd(tS32 *ps32Sg_fd)
{
   tS32 s32Version;
   tS32 s32Ret = CD_SUCCESS;

   /*inquiry command*/
   rCd_global.rCd_ctrl[0].u32Size = 6;
   memcpy(&rCd_global.rCd_ctrl[0].u8Cmd[0],
       "\x12\x00\x00\x00\x24\x00", rCd_global.rCd_ctrl[0].u32Size);
   rCd_global.rCd_ctrl[0].u32Reply_len = 36;

   *ps32Sg_fd = open(CD_MASCA_DEVICE, O_RDONLY | O_NONBLOCK);
   if(*ps32Sg_fd < 0)
   {
      /* Note that most SCSI commands require the O_RDWR flag to
         be set */
      perror("\nerror opening /dev/sr0");
      rCd_global.u32Cd_err_count |= 1 << 0;
      s32Ret = CD_ERROR;
   }
   else
   {
      /* It is prudent to check we have a sg device by trying an ioctl */
      if((ioctl(*ps32Sg_fd, SG_GET_VERSION_NUM, &s32Version) < 0) ||
       (s32Version < 30000))
      {
         rCd_global.u32Cd_err_count |= 1 << 1;
         perror("error getting s32Version details");
         s32Ret = CD_ERROR;
      }
      else
      {
         if(CD_ERROR == pthread_create(&rCd_global.tid_cd, NULL,
                        pvCd_thread, (void *)ps32Sg_fd))
         {
            close(*ps32Sg_fd);
            printf("Failed to create a thread");
            rCd_global.u32Cd_err_count |= 1 << 2;
            s32Ret = CD_ERROR;
         }
      }
   }
   return s32Ret;
}

/******************************************************************************
* FUNCTION     : pvCd_thread
*
* PARAMETER    : pvArg - parameter containing MASCA file descriptor
*
* RETURNVALUE  : void
*
*
* DESCRIPTION  :  This thread will executes the CD commands till the user
*       provided loop / ctrl+c.
*
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|--------------------------------
* 22.May.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* ------------------------------------------------------------------------------
*******************************************************************************/
static void *pvCd_thread(void *pvArg)
{
   tS32 s32Cmd_count;
   tS32 s32Count = CD_SUCCESS;
   tS32 s32Cur_cmd_count = CD_SUCCESS;
   tS32 s32Count_upto_adr = SYNC_WITH_ADR;
   tS32 *ps32Sg_fd = (tS32 *)pvArg;
   tU8 u8Logsensebuff[50];
   tU8 u8Sense_buffer[32];
   sg_io_hdr_t rIo_hdr;
   tU8 u8Temp[80];   
   sigset_t signals_to_catch;
   tS32 s32Caught;


   sigemptyset(&signals_to_catch);
   sigaddset(&signals_to_catch, SIGUSR1);
   /*clear memory*/
   memset(u8Sense_buffer, 0, sizeof(u8Sense_buffer));
   memset(u8Logsensebuff, 0, sizeof(u8Logsensebuff));
   memset(u8Temp, 0, sizeof(u8Temp));
   /*calculate total commands*/
   if(rCd_global.s32Cd_available)
   {
      while (rCd_global.rCd_ctrl[s32Count].u32Size)
      {
         s32Count++;
      }
      s32Cmd_count = s32Count;
   }
   else
   {
      s32Cmd_count = 1;
   }
   (void)s32Cmd_count;
   /* Wait for SIGUSR1 signal*/
   sigwait(&signals_to_catch, &s32Caught);
   printf("\nCD Test Running....\n");   
   while(rCd_global.u32Cd_tst_cnt && !s32Unctrl_exit)
   {
      s32Count = 0;
      /*Ensure CD test runs along with ADR till end*/
      while(s32Count < s32Count_upto_adr && !s32Unctrl_exit)
      {
         /* Prepare INQUIRY command */
         memset(&rIo_hdr, 0, sizeof(sg_io_hdr_t));
         rIo_hdr.interface_id = 'S';
         rIo_hdr.cmd_len = (tU8)rCd_global.rCd_ctrl[s32Cur_cmd_count].u32Size;
         /* rIo_hdr.iovec_count = 0; */
         /* memset takes care of this */
         rIo_hdr.mx_sb_len = sizeof(u8Sense_buffer);
         rIo_hdr.dxfer_direction = SG_DXFER_FROM_DEV;
         rIo_hdr.dxfer_len =
         rCd_global.rCd_ctrl[s32Cur_cmd_count].u32Reply_len;
         rIo_hdr.dxferp = u8Logsensebuff;
         rIo_hdr.cmdp = rCd_global.rCd_ctrl[s32Cur_cmd_count].u8Cmd;
         rIo_hdr.sbp = u8Sense_buffer;
         /* 20000 millisecs == 20 seconds */
         rIo_hdr.timeout = 20000;
         /* take defaults: indirect IO, etc */
         /* rIo_hdr.flags = 0; */
         /* rIo_hdr.pack_id = 0; */
         /* rIo_hdr.usr_ptr = NULL; */

         if(ioctl(*ps32Sg_fd, SG_IO, &rIo_hdr) < 0)
         {
            perror("sg_simple0: Inquiry SG_IO ioctl error");
            rCd_global.u32Cd_err_count |= 1 << 3;
            s32Unctrl_exit = 1;
         }            /*check for error*/
         else if(rIo_hdr.sb_len_wr != CD_SUCCESS)
         {
            rCd_global.u32Cd_err_count |= 1 << 4;
            printf("\nErr - u8Sense_buffer:%s", u8Sense_buffer);
            printf("\n TST s32Count:%lu", rCd_global.u32Cd_tst_cnt);
            s32Unctrl_exit = 1;
         }
         /*validate received data*/
         if(!s32Cur_cmd_count)
         {
            memcpy(u8Temp, u8Logsensebuff + 8, 8);
            u8Temp[8] = '\0';
            if((strncmp("PIONEER", u8Temp, 7) != 0) &&
               (strncmp("KENWOOD", u8Temp, 7) != 0) &&
               (strncmp("TANASHIN", u8Temp, 8) != 0))
            {
               printf("\nCD drive error");
               printf("\nSAT Vendor ID: %s\n", u8Temp);
               rCd_global.u32Cd_err_count |= 1 << 5;
               s32Unctrl_exit = 1;
            }
         }
         memset(u8Sense_buffer, 0, sizeof(u8Sense_buffer));
         memset(u8Logsensebuff, 0, sizeof(u8Logsensebuff));
         memset(u8Temp, 0, sizeof(u8Temp));
         s32Count++;
         
      }
      rCd_global.u32Cd_tst_cnt--;
   } 
   printf("\nCD Test completed\n");
   return CD_SUCCESS;
}

/******************************************************************************
* FUNCTION     : s32Create_threads
*
* PARAMETER    : ps32Sockfd - input parameter containing socket file descriptor
*                dgram - input parameter containing dgram file
*                descriptor
*                s32Ret -input parameter containing return value of the
*                s32Connect_socket function
*
* RETURNVALUE  : tS32 - Success/Failure
*
*
* DESCRIPTION  :  This function will creates ADR send and receive thread.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.May.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32Create_threads(tS32 s32Ret, const tS32 *ps32Sockfd, 
                              sk_dgram * const *dgram)
{
   if(!s32Ret)
   {
      /*initialise semaphore used to sync between send
         and receive thread*/
      if(sem_init(&rAdr_global.sem_adr, 0, 0))
      {
         printf("\nADR:SEM init failed\n");
         rAdr_global.u32Adr_err_count |= 1 << 7;
         s32Ret = ADR_ERROR;
      }
      if(!s32Ret)
      {  /* create ADR command sending thread*/
         if(ADR_ERROR == pthread_create(&rAdr_global.tid_adr_snd, NULL,
                                       pvInc_snd_thread, (void *)*dgram))
         {
            close(*ps32Sockfd);
            printf("Failed to create a thread");
            rAdr_global.u32Adr_err_count |= 1 << 8;
            s32Ret = ADR_ERROR;
         }/*create ADR response receiving thread*/
         else if(ADR_ERROR == pthread_create(&rAdr_global.tid_adr_rcv, NULL,
                                       pvInc_recv_thread, (void *)*dgram))
         {
            s32Unctrl_exit = 1;
            pthread_kill(rAdr_global.tid_adr_snd, SIGUSR1);
            pthread_join(rAdr_global.tid_adr_snd, NULL);
            close(*ps32Sockfd);
            printf("\nFailed to create a thread");
            rAdr_global.u32Adr_err_count |= 1 << 9;
            s32Ret = ADR_ERROR;
         }
      }
   }
   return s32Ret;
}

/******************************************************************************
* FUNCTION     : s32Connect_socket
*
* PARAMETER    : ps32Sockfd - input parameter containing socket file descriptor
*                rRemote_addr - parameter containing remote client info
*                prServer - parameter containing host ip addr info
*                s32Ret -input parameter containing return value of the
*                s32Bind_socket function
*
* RETURNVALUE  : tS32 - Success/Failure
*
*
* DESCRIPTION  :  This function will connects to the remote ADR port.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.May.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32Connect_socket(tS32 s32Ret, tS32 const *ps32Sockfd,
               struct sockaddr_in *rRemote_addr, struct hostent **prServer)
{
   tS32 s32Portno = AMPLIFIER_PORT; /*Amplifier*/
   if(!s32Ret)
   {
      /**
      * Get host information ... (Note to developer > Check adr3/v850 is
      * present is present in /etc/hosts, if not present add it)
      */
      *prServer = gethostbyname("adr3");

      if(*prServer == NULL)
      {
         close(*ps32Sockfd);
         printf("\nFailed, no such host adr3...");
         rAdr_global.u32Adr_err_count |= 1 << 5;
         s32Ret = ADR_ERROR;;
      }
      else
      {
         memcpy((tS8 *)&rRemote_addr->sin_addr.s_addr,
               (tS8 *)(*prServer)->h_addr, 
               (tU32)(*prServer)->h_length);
         /*Address received from gethostbyname function*/
         /**
         * Connect to port of the ADR3/v850.
         * Note to Developer > Configure remote client and prServer by
         * following commands, or else connect call will fail
         */
         rRemote_addr->sin_family = (unsigned short)AF_INET;/*Protol*/
         /*Portnumber to listen to*/
         rRemote_addr->sin_port = htons((unsigned short)s32Portno);
         /*lint -e{740}*/
         /* Lint Info 740 cannot be fixed, please refer
            http://man7.org/linux/man-pages/man2/connect.2.html
            http://man7.org/linux/man-pages/man2/bind.2.html 
            http://man7.org/linux/man-pages/man7/ip.7.html */
//lint -e64			
         if(connect(*ps32Sockfd, (struct sockaddr *)rRemote_addr,
                     sizeof(struct sockaddr_in)) < 0)
         {
            close(*ps32Sockfd);
            printf("\nconnecting socket Failed");
            rAdr_global.u32Adr_err_count |= 1 << 6;
            s32Ret = ADR_ERROR;
         }
      }
   }
   return s32Ret;
}

/******************************************************************************
* FUNCTION     : s32Bind_socket
*
* PARAMETER    : ps32Sockfd - input parameter containing socket file descriptor
*                rServ_addr - parameter containing local client info
*                prLhost - parameter containing local ip addr info
*                s32Ret -input parameter containing return value of the
*                s32Init_socket function
*
* RETURNVALUE  : tS32 - Success/Failure
*
*
* DESCRIPTION  :  This function will binds to the local port.
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.May.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32Bind_socket(tS32 s32Ret, const tS32 *ps32Sockfd, 
                        struct sockaddr_in *rServ_addr,
                        struct hostent **prLhost)
{   
   tS32 s32Localportno = AMPLIFIER_PORT; /*Amplifier*/
   if(!s32Ret)
   {
      *prLhost = gethostbyname("adr3-local");

      if(*prLhost == NULL)
      {
         close(*ps32Sockfd);
         rAdr_global.u32Adr_err_count |= 1 << 3;
         printf("\nFailed, no such host adr3-local...");
         s32Ret = ADR_ERROR;
      }
      else
      {
         memcpy((tS8 *)&rServ_addr->sin_addr.s_addr, 
               (tS8 *)(*prLhost)->h_addr,
               (tU32)(*prLhost)->h_length);
         /*Address received from gethostbyname function*/

         /**
         * Bind to a local port to recieve response
         */

         rServ_addr->sin_family = (unsigned short)AF_INET;
         rServ_addr->sin_port = htons((unsigned short)s32Localportno);
         /*lint -e{740}*/
         /* Lint Info 740 cannot be fixed, please refer
            http://man7.org/linux/man-pages/man2/connect.2.html
            http://man7.org/linux/man-pages/man2/bind.2.html 
            http://man7.org/linux/man-pages/man7/ip.7.html */
         if(bind(*ps32Sockfd, (struct sockaddr *)rServ_addr,
                  sizeof(struct sockaddr_in)) < 0)
         {
            close(*ps32Sockfd);
            printf("\nBind protocol Failed");
            rAdr_global.u32Adr_err_count |= 1 << 4;
            s32Ret = ADR_ERROR;;
         }
      }
   }
   return s32Ret;
}

/******************************************************************************
* FUNCTION     : s32Init_socket
*
* PARAMETER    : ps32Sockfd - input parameter containing socket file descriptor
*                 dgram - input parameter containing dgram file
*                descriptor
*
* RETURNVALUE  : tS32 - Success/Failure
*
*
* DESCRIPTION  :  This function will initialise the socket and data gram service
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification           | Author & comments
*--------------|-------------------------------|-------------------------------
* 22.May.2014  |  initial  Modification        |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
******************************************************************************/
static tS32 s32Init_socket(tS32 *ps32Sockfd, sk_dgram **dgram)
{
   tS32 s32Ret = ADR_SUCCESS;
   tS32 s32On = 1;
   tS32 s32Rc = ADR_SUCCESS;
   
   /**
   * Create a socket
   */
   *ps32Sockfd = socket(AF_BOSCH_INC_ADR, (tS32)SOCK_STREAM, 0);

   if(*ps32Sockfd < 0)
   {
      printf("\nSocket creation Failed");
      rAdr_global.u32Adr_err_count |= 1 << 0;
      s32Ret = ADR_ERROR;
   }
   else
   {
      *dgram = dgram_init(*ps32Sockfd, DGRAM_MAX, NULL);
      if(*dgram == NULL)
      {
         printf("dgram_init failed");
         close(*ps32Sockfd);
         rAdr_global.u32Adr_err_count |= 1 << 1;
         s32Ret = ADR_ERROR;
      }
      else
      {
         s32Rc = ioctl(*ps32Sockfd, FIONBIO, (tS8 *)&s32On);
         if(s32Rc < 0)
         {
            printf("\nioctl() failed");
            close(*ps32Sockfd);
            rAdr_global.u32Adr_err_count |= 1 << 2;
            s32Ret = ADR_ERROR;
         }
      }
   }
   return s32Ret;
}

/*****************************************************************************
* FUNCTION     : s32Initialise_adr
*
* PARAMETER    : ps32Sockfd - input parameter containing socket file descriptor
*            dgram - input parameter containing dgram file
*                descriptor
*
* RETURNVALUE  : tS32 - Error/Success
*
*
* DESCRIPTION  :  This function intialises communication interface to ADR and
*             creates two threads(send and receive).
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date         |        Modification          | Author & comments
*--------------|------------------------------|-------------------------------
* 22.May.2014  |  initial  Modification       |  Suresh Dhandapani (RBEI/ECF5)
* ----------------------------------------------------------------------------
*****************************************************************************/
static tS32 s32Initialise_adr(tS32 *ps32Sockfd, sk_dgram **dgram)
{
   struct sockaddr_in rServ_addr, rRemote_addr;
   struct hostent *prServer, *prLhost;
   tS32 s32Ret;

   rAdr_global.s32Adr_poll_timeout = 1;

   /*volume step commands*/
   /*Please refer the concept and specification document*/
   /*These commands are used to change the volume*/
   rAdr_global.rAdr_ctrl[0].u32Size = 18;
   memcpy(&rAdr_global.rAdr_ctrl[0].u8Cmd[0],
   "\x00\x10\x00\x91\x01\x0f\x20\x02\x00", 9);
   memcpy(&rAdr_global.rAdr_ctrl[0].u8Cmd[9],
   "\x08\x01\x1E\xFE\xE4\x00\x0a\x01\x90", 9);

   rAdr_global.rAdr_ctrl[1].u32Size = 18;
   memcpy(&rAdr_global.rAdr_ctrl[1].u8Cmd[0],
   "\x00\x10\x00\x91\x01\x0f\x20\x02\x00", 9);
   memcpy(&rAdr_global.rAdr_ctrl[1].u8Cmd[9],
   "\x08\x01\x21\xFE\xFc\x00\x0a\x01\x90", 9);

   rAdr_global.rAdr_ctrl[2].u32Size = 18;
   memcpy(&rAdr_global.rAdr_ctrl[2].u8Cmd[0],
   "\x00\x10\x00\x91\x01\x0f\x20\x02\x00" , 9);
   memcpy(&rAdr_global.rAdr_ctrl[2].u8Cmd[9],
   "\x08\x01\x23\xff\x08\x00\x0a\x01\x90", 9);

   rAdr_global.rAdr_ctrl[3].u32Size = 18;
   memcpy(&rAdr_global.rAdr_ctrl[3].u8Cmd[0],
   "\x00\x10\x00\x91\x01\x0f\x20\x02\x00", 9);
   memcpy(&rAdr_global.rAdr_ctrl[3].u8Cmd[9],
   "\x08\x01\x28\xff\x30\x00\x0a\x01\x90", 9);

   rAdr_global.rAdr_ctrl[4].u32Size = 18;
   memcpy(&rAdr_global.rAdr_ctrl[4].u8Cmd[0],
   "\x00\x10\x00\x91\x01\x0f\x20\x02\x00", 9);
   memcpy(&rAdr_global.rAdr_ctrl[4].u8Cmd[9],
   "\x08\x01\x2D\xff\x58\x00\x0a\x01\x90", 9);

   rAdr_global.rAdr_ctrl[5].u32Size = 18;
   memcpy(&rAdr_global.rAdr_ctrl[5].u8Cmd[0],
   "\x00\x10\x00\x91\x01\x0f\x20\x02\x00", 9);
   memcpy(&rAdr_global.rAdr_ctrl[5].u8Cmd[9],
   "\x08\x01\x32\xff\x80\x00\x0a\x01\x90", 9);

   rAdr_global.rAdr_ctrl[6].u32Size = 18;
   memcpy(&rAdr_global.rAdr_ctrl[6].u8Cmd[0],
   "\x00\x10\x00\x91\x01\x0f\x20\x02\x00", 9);
   memcpy(&rAdr_global.rAdr_ctrl[6].u8Cmd[9],
   "\x08\x01\x37\xff\xA8\x00\x0a\x01\x90", 9);

   rAdr_global.rAdr_ctrl[7].u32Size = 18;
   memcpy(&rAdr_global.rAdr_ctrl[7].u8Cmd[0],
   "\x00\x10\x00\x91\x01\x0f\x20\x02\x00", 9);
   memcpy(&rAdr_global.rAdr_ctrl[7].u8Cmd[9],
   "\x08\x01\x3C\xff\xD0\x00\x0a\x01\x90", 9);

   s32Ret = s32Init_socket(ps32Sockfd, dgram);
   s32Ret = s32Bind_socket(s32Ret, ps32Sockfd, &rServ_addr, &prLhost);
   s32Ret = s32Connect_socket(s32Ret, ps32Sockfd, &rRemote_addr, &prServer);
   s32Ret = s32Create_threads(s32Ret, ps32Sockfd, dgram);

   return s32Ret;
}

/****************************************************************************
* FUNCTION     : pvInc_snd_thread
*
* PARAMETER    : pvArg - input parameter containing dgram file descriptor
*
* RETURNVALUE  : void
*
*
* DESCRIPTION  :  This thread will sends data to ADR and waits for the response
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |        Modification          | Author & comments
*--------------|------------------------------|--------------------------------
* 22.May.2014  |  initial  Modification       |  Suresh Dhandapani (RBEI/ECF5)
* -----------------------------------------------------------------------------
*******************************************************************************/
static void *pvInc_snd_thread(void *pvArg)
{
   tU32 u32Len = ADR_SUCCESS;
   tS32 s32Count = ADR_SUCCESS;
   sigset_t signals_to_catch;
   tS32 s32Caught;
   sk_dgram *pdgram = (sk_dgram *)pvArg;

   sigemptyset(&signals_to_catch);
   sigaddset(&signals_to_catch, SIGUSR1);
   /*calculate total commands*/
   while (rAdr_global.rAdr_ctrl[s32Count].u32Size)
   {
      s32Count++;
   }

   rAdr_global.s32Total_cmd_adr = s32Count;
     /* Wait for SIGUSR1 signal*/ 
   sigwait(&signals_to_catch, &s32Caught);
   printf("\nADR Test Running....\n");

   while (rAdr_global.u32Adr_tst_cnt && !s32Unctrl_exit)
   {
   /*Store the volume step byte into s32Check_adr variable,
     so it will be used to check with the response bytes from the ADR in
      the receiving thread*/
      s32Count = 0;
      while (s32Count < rAdr_global.s32Total_cmd_adr && !s32Unctrl_exit)
      {
         rAdr_global.s32Check_adr = rAdr_global.rAdr_ctrl[s32Count].u8Cmd[11];
         u32Len = (tU32)dgram_send(pdgram, 
                           &rAdr_global.rAdr_ctrl[s32Count].u8Cmd[0],
                           rAdr_global.rAdr_ctrl[s32Count].u32Size);
         /*check for actual length with written length*/
         if (u32Len != rAdr_global.rAdr_ctrl[s32Count].u32Size)
         {
            printf("\nSending ADR command failed");
            rAdr_global.u32Adr_err_count |= 1 << 10;
            s32Unctrl_exit = 1;
         }
         else
         {/*if write is success then wait for the response message*/
            sem_wait(&rAdr_global.sem_adr);
         }
         s32Count++;
      }
      rAdr_global.u32Adr_tst_cnt--;
   }
   /*setting this flag will terminate the receive thread*/
   rAdr_global.s32Terminate = 1;
   printf("\nADR Test completed\n"); 
   return ADR_SUCCESS;
}


/******************************************************************************
* FUNCTION     : pvInc_recv_thread
*
* PARAMETER    : pvArg - input parameter containing dgram file descriptor
*
* RETURNVALUE  : void
*
*
* DESCRIPTION  :  This thread will wait and receives data from ADR, then
*          validates it.
*
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |        Modification          | Author & comments
*--------------|------------------------------|---------------------------------
* 22.May.2014  |  initial  Modification       |  Suresh Dhandapani (RBEI/ECF5)
* 22.Aug.2014  |  Update data type for buffer |  Suresh Dhandapani (RBEI/ECF5)
* ------------------------------------------------------------------------------
*******************************************************************************/
static void *pvInc_recv_thread(void *pvArg)
{
   sk_dgram *pdgram = (sk_dgram *)pvArg;
   tS32 u32Pfd = pdgram->sk;
   tS32 s32Rc = -1;
   struct pollfd rFds[1];
   tU8 u8Buffer[500];
   tS32 s32Status;
   tS32 s32Count;

   rFds[0].fd = u32Pfd;
   rFds[0].events = POLLIN;
   /*clear memory*/
   memset(u8Buffer, 0, sizeof(u8Buffer));
   /*check for the terminate flag*/
   while (!rAdr_global.s32Terminate)
   {
      s32Rc = poll(rFds, 1, ADR_POLL_TIMEOUT);
      if (s32Rc < 0)
      {
         printf("Poll Failed");
         rAdr_global.u32Adr_err_count |= 1 << 11;
         s32Unctrl_exit = 1;
         rAdr_global.s32Adr_poll_timeout = 0;
         sem_post(&rAdr_global.sem_adr);
         rAdr_global.s32Terminate = 1;
      }
      else if (s32Rc == 0)
      {/*check whether timeout happens not because of the missing startup 
         message */
         if (!rAdr_global.s32Adr_poll_timeout && !rAdr_global.s32Terminate)
         {
            printf("\nADR not responding... exiting test");
            rAdr_global.u32Adr_err_count |= 1 << 12;
            s32Unctrl_exit = 1;
            sem_post(&rAdr_global.sem_adr);
            rAdr_global.s32Terminate = 1;
         }/*check for the startup message receive timeout*/
         else if(rAdr_global.s32Adr_poll_timeout)
         {/*It is to ensure all startup messages are received*/
            rAdr_global.s32Adr_poll_timeout = 0;
            sem_post(&sem_start_test);
         }
      }
      else if (rFds[0].revents & POLLIN)
      {
         s32Rc = dgram_recv(pdgram, u8Buffer, sizeof(u8Buffer));
         if(s32Rc > 0)
         {
            s32Status = s32Check_adr_buffer(u8Buffer);
            if (s32Status < 0)
            {
               printf("\n Err - ADR receive u8Buffer");
               for (s32Count = 0; s32Count < s32Rc; s32Count++)
                  printf(" 0x%x", u8Buffer[s32Count]);
               rAdr_global.u32Adr_err_count |= 1 << 13;
               s32Unctrl_exit = 1;
            }
            else if (!rAdr_global.s32Adr_poll_timeout && !s32Status)
            {/*ready to receive next message, initiate next command to ADR*/
               sem_post(&rAdr_global.sem_adr);
            }
         }
         else
         {
            s32Unctrl_exit = 1;
            sem_post(&rAdr_global.sem_adr);
            printf("Error during receive");
            rAdr_global.u32Adr_err_count |= 1 << 13;
         }
         memset(u8Buffer, 0, sizeof(u8Buffer));/*Clear data*/
      }
   }
   sem_destroy(&rAdr_global.sem_adr);
   return ADR_SUCCESS;
}

/******************************************************************************
* FUNCTION     : s32Check_adr_buffer
*
* PARAMETER    : pu8Buffer - input parameter containing ADR response pu8Buffer
*
* RETURNVALUE  : tS32 - -1 in error case
*        0 or 1 or 2 in success case
*
*
* DESCRIPTION  :  This function will validates the pu8Buffer received from the
*                 ADR.
*
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |        Modification          | Author & comments
*--------------|------------------------------|---------------------------------
* 22.May.2014  |  initial  Modification       |  Suresh Dhandapani (RBEI/ECF5)
* 22.Aug.2014  |  Update data type for buffer |  Suresh Dhandapani (RBEI/ECF5)
* ------------------------------------------------------------------------------
*******************************************************************************/
static tS32 s32Check_adr_buffer(const tU8 * pu8Buffer)
{  
   tS32 s32Ret = ADR_SUCCESS;
   /*pu8Buffer[11] has the volume step info and pu8Buffer[7] says the response  
      s32Status whether ok or not, pu8Buffer[14] gives the information about  
      whether volume change is done or in progress*/
   if ((pu8Buffer[11] != rAdr_global.s32Check_adr) || (pu8Buffer[7] != 0x0c))
   {
      printf("\n ERROR: Received ADR :%x", pu8Buffer[11]);
      rAdr_global.u32Adr_err_count |= 1 << 15;
      s32Ret = ADR_ERROR;;
   }
   else
   {
      switch (pu8Buffer[14])
      {
         case ADR_SUCCESS_BYTE:
         break;
         case ADR_IN_PROCESS_1:
            s32Ret = 1;
         break;
         case ADR_IN_PROCESS_2:
            s32Ret = 2;
         break;
         default:
            s32Ret = -1;
         break;
      }
   }
   return s32Ret;
}

#ifdef __cplusplus
}
#endif
