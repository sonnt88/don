/************************************************************************
  | FILE:         inc_perf_common.c
  | PROJECT:      platform
  | SW-COMPONENT: INC
  |------------------------------------------------------------------------------
  | DESCRIPTION:   INC performance measurement common file 
  |                File contains the common functions and global data shared between   
  |                INC performance measurement commmand line apps & OEDT's 
  |------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2014 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification                               | Author
  | 11.07.14  | Initial revision                           | Shashikant Suguni 
  | 12.11.15  | INC SCC(v850) Throughput threshold update  | dus5cob 
  | 03.03.16  | Throughput update for new kernel changes   | Madhu Sudhan Swargam (RBEI/ECF5)
  | --.--.--  | ---------------------                      | -------, -----
  |
  |*****************************************************************************/
#include <signal.h>
#include <sys/signal.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include "linux/input.h"
#include "dgram_service.h"
#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_input_device.h"
#include "inc_perf_common.h"

#define PR_ERR(fmt, args...) \
{ fprintf(stderr, "inc_perf_common: %s: " fmt, __func__, ## args); }

#define EXPCT_EQ(val1, val2, ret, fmt, args...) \
    if (val1 != val2) { \
        PR_ERR(fmt, ## args); \
        return ret; }

#define EXPCT_NE(val1, val2, ret, fmt, args...) \
    if (val1 == val2) { \
        PR_ERR(fmt, ## args); \
        return ret; }

#define MAX_STATS                       4
#define INIT                            0
#define PREVIOUS                        1
#define PRESENT                         2
#define AVERAGE                         3

#define MAX_LUN                         256

/* Values are synced with kernel files print sequence */
#define LUN                             0
#define RX_BYTES                        1
#define RX_PACKETS                      2
#define TX_BYTES                        3
#define TX_PACKETS                      4

#define ERROR_INDEX                     4
#define RX_CHK_ERR                      5
#define RX_SEQ_ERR                      6
#define RX_XOFF                         7
#define RX_SYN_ERR                      8
#define TX_CHK_ERR                      9
#define TX_SEQ_ERR                      10
#define TX_XOFF                         11
//#define TX_HW_XOFF                      12
#define TX_SYN_ERR                      13

#define MAX_ERROR                       ((TX_SYN_ERR - RX_CHK_ERR) + 1)

/*INC ADR3 Throughput threshold values*/

#define TH_RX_BYTES_ADR3                170 
#define TH_TX_BYTES_ADR3                310
#define TH_RX_PACKETS_ADR3              12
#define TH_TX_PACKETS_ADR3              32

/* Updated INC SCC(v850) Throughput threshold values.
   The INC performance figures are reduced by 13%, This is due to migration from
   linux kernel 3.8 to 3.14.
*/

#define TH_RX_BYTES_SCC                 54593 
#define TH_TX_BYTES_SCC                 109191  
#define TH_RX_PACKETS_SCC               909
#define TH_TX_PACKETS_SCC               1819 

/* Structure to hold the througput figures */
typedef struct _throughput{
    bool bLunActv;
    unsigned long rx_packets;
    unsigned long tx_packets;
    unsigned long rx_bytes;
    unsigned long tx_bytes;
}throughput;

throughput stats[MAX_STATS][MAX_LUN] = {0};
unsigned long error_stats[MAX_LUN][MAX_ERROR+1]={0};

/*
 * Function to tokenise and fetch the net device statistics
 */
int netdev_stats_handler(char *host,throughput* device_stats)
{
    FILE *fp ;
    char stat_path[100],path[1035];
    unsigned long bytes[20];    
    int i = 0 , err_index=0;
    int errsv;
    char *p;
    throughput* lun_stats;
    EXPCT_NE(device_stats, NULL, -1,
             "Invalid argument, device_stats\n"
            );
    EXPCT_NE(host, NULL, -1, "Invalid argument, host\n");

    memset (bytes,0,sizeof(bytes));
    memset (path,0,sizeof(path));
    memset (stat_path,0,sizeof(stat_path));
    strncat(stat_path, COMMON_PATH, sizeof(COMMON_PATH));
    if (strcmp(host,"inc-adr3") == 0)
    {
        strncat(stat_path, "inc-adr3", sizeof("inc-adr3"));
    }
    else if (strcmp(host,"inc-scc") == 0)
    {
        strncat(stat_path, "inc-scc", sizeof("inc-scc"));
    }
    else
    {
        PR_ERR("ERROR Host not valid=%s\n",host);
    }
    strncat(stat_path, STATS_NAME, sizeof(STATS_NAME));
    fp = fopen(stat_path, "r");
    errsv = errno;
    EXPCT_NE(fp, NULL, -1, "fopen failed: %d\n", errsv);
    lun_stats = device_stats;
    while (fgets(path, sizeof(path)-1, fp) != NULL) 
    {
        i=0;
        p = strtok(path, " ");
        while(p != NULL)
        {
            bytes[i] = (unsigned long)atol(p);
            i++;
            p = strtok(NULL, " ");
        }
        err_index = 0;
        /* Update the lun_stats structure to the lun value 
           containing array position  
        */
        lun_stats = device_stats + bytes[LUN];
        if(lun_stats <= (device_stats + MAX_LUN))
        {
            lun_stats->bLunActv = true; 
            lun_stats->rx_packets = bytes[RX_PACKETS];
            lun_stats->tx_packets = bytes[TX_PACKETS];
            lun_stats->rx_bytes = bytes[RX_BYTES];
            lun_stats->tx_bytes = bytes[TX_BYTES];
        }
        else
        {
            printf("LUN value:%d exceed the Max value:%d", 
                   (int)bytes[LUN], MAX_LUN
                  );
            return -1;
        }
        /* Check the LUN value*/
        if(bytes[LUN] < MAX_LUN)
        {
            error_stats[bytes[LUN]][err_index] = bytes[LUN];
            for(err_index = 1; err_index < (MAX_ERROR+1); err_index++)
            {
                /* Update the error statistics array */
                error_stats[bytes[LUN]][err_index] = \
                bytes[err_index + RX_CHK_ERR - 1];
            }
        }
        else
        {
            printf("LUN value:%d exceed the Max value:%d",
                    (int)bytes[LUN], MAX_LUN
                   );
            return -1;
        }
    }
    fclose(fp);
    return 0;
}

/* computes INC throughput figures */
int throughput_handler(char *host,unsigned long interval,
                       unsigned long interval_count)
{
    char path[1035];
    unsigned long bytes[5];    
    throughput device_stats[MAX_LUN]={0};    
    int ret,indx;
    
    EXPCT_NE(host, NULL, -1, "Invalid argument, host\n");

    memset (bytes,0,sizeof(bytes));
    memset (path,0,sizeof(path));
    ret = netdev_stats_handler(host,&device_stats[0]);
    if (ret != 0)
    {
        PR_ERR("ERROR netdev_stats_handler failed\n");
        return -1;
    }
    if (interval_count == 0)
    {
        for(indx = 0 ; indx < MAX_LUN; indx++ )
        {
            if(device_stats[indx].bLunActv ==  true)
            {
                stats[INIT][indx] = device_stats[indx];
            }
        }
    }
    else 
    {
        /* Update the previous values */
        for(indx = 0 ; indx < MAX_LUN; indx++ )
        {
            if(stats[PRESENT][indx].bLunActv ==  true)
            {
                stats[PREVIOUS][indx] = stats[PRESENT][indx];
            }
        }
        /* Update the present values */
        for(indx = 0 ; indx < MAX_LUN; indx++ )
        {
            if(device_stats[indx].bLunActv ==  true) 
            {
                stats[PRESENT][indx].bLunActv = true;
                stats[PRESENT][indx].rx_packets = device_stats[indx].rx_packets - \
                                               stats[INIT][indx].rx_packets;
                stats[PRESENT][indx].tx_packets = device_stats[indx].tx_packets - \
                                               stats[INIT][indx].tx_packets;
                stats[PRESENT][indx].rx_bytes = device_stats[indx].rx_bytes - \
                                             stats[INIT][indx].rx_bytes;
                stats[PRESENT][indx].tx_bytes = device_stats[indx].tx_bytes - \
                                             stats[INIT][indx].tx_bytes;
                stats[AVERAGE][indx].bLunActv = true;
                stats[AVERAGE][indx].tx_packets = (stats[PRESENT][indx].tx_packets) / \
                                               (interval_count * interval);
                stats[AVERAGE][indx].rx_packets = (stats[PRESENT][indx].rx_packets) / \
                                               (interval_count * interval);
                stats[AVERAGE][indx].tx_bytes   = (stats[PRESENT][indx].tx_bytes) /  \
                                               (interval_count * interval);
                stats[AVERAGE][indx].rx_bytes   = (stats[PRESENT][indx].rx_bytes) / \
                                               (interval_count * interval);
            }
        }
    }
    return 0;
}

/* Prints throughput*/
void print_throughput(unsigned long interval,unsigned long count, 
                      bool per_lun_prnt_actv, bool avg_lun_prnt_actv)
{

    unsigned long tx_bytes_l,rx_bytes_l,tx_pckts_l,rx_pckts_l;
    unsigned long tx_bytes_s,rx_bytes_s,tx_pckts_s,rx_pckts_s;
    unsigned long total_tx_bytes_l,total_rx_bytes_l,\
                  total_tx_pckts_l,total_rx_pckts_l;
    unsigned long total_tx_bytes_s,total_rx_bytes_s, \
                  total_tx_pckts_s,total_rx_pckts_s;
    int total_error_count;
    unsigned long Total_error_stats[MAX_ERROR+1]= {0};
    int indx, err_index; 
    unsigned long actv_lun_count=0;
    memset (Total_error_stats,0,sizeof(Total_error_stats));
    if (count == 0 || interval == 0)
        return;
    total_tx_bytes_l = total_rx_bytes_l = 0;
    total_tx_pckts_l = total_rx_pckts_l = 0;
    total_tx_bytes_s = total_rx_bytes_s = 0;
    total_tx_pckts_s = total_rx_pckts_s = 0;
    for(indx = 0 ; indx < MAX_LUN; indx++ )
    {
        if(stats[PRESENT][indx].bLunActv ==  true) 
        {
            actv_lun_count++;
            /* Update the present packets received*/
            tx_pckts_l = (stats[PRESENT][indx].tx_packets - \
                          stats[PREVIOUS][indx].tx_packets)/interval;
            rx_pckts_l = (stats[PRESENT][indx].rx_packets - \
                          stats[PREVIOUS][indx].rx_packets)/interval;
            tx_bytes_l = (stats[PRESENT][indx].tx_bytes - \
                          stats[PREVIOUS][indx].tx_bytes)/interval;
            rx_bytes_l = (stats[PRESENT][indx].rx_bytes - \
                          stats[PREVIOUS][indx].rx_bytes)/interval;
            total_tx_bytes_l += tx_bytes_l;
            total_rx_bytes_l += rx_bytes_l;
            total_tx_pckts_l += tx_pckts_l;
            total_rx_pckts_l += rx_pckts_l;
            /* Update the present packets received per second */
            tx_pckts_s = (stats[PRESENT][indx].tx_packets) / (count * interval);
            rx_pckts_s = (stats[PRESENT][indx].rx_packets) / (count * interval);
            tx_bytes_s = (stats[PRESENT][indx].tx_bytes) / (count * interval);
            rx_bytes_s = (stats[PRESENT][indx].rx_bytes) / (count * interval);
            total_tx_bytes_s += tx_bytes_s;
            total_rx_bytes_s += rx_bytes_s;
            total_tx_pckts_s += tx_pckts_s;
            total_rx_pckts_s += rx_pckts_s;
            total_error_count = 0;
            for (err_index = 1;err_index<(MAX_ERROR+1);  err_index ++)
            {
                Total_error_stats[err_index] += error_stats[indx][err_index];
                total_error_count += (int)error_stats[indx][err_index];
            }
            /* Print if per lun is active
            */
            if(true == per_lun_prnt_actv)
            {
                printf ("%3d\t%3d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\n",
                        (int)(count * interval),indx,(int)rx_pckts_l,
                        (int)tx_pckts_l,
                        (int)(tx_pckts_l+rx_pckts_l),(int)rx_bytes_l,
                        (int)tx_bytes_l,
                        (int)(tx_bytes_l+rx_bytes_l),(int)rx_pckts_s,
                        (int)tx_pckts_s,
                        (int)(tx_pckts_s+rx_pckts_s),(int)rx_bytes_s,
                        (int)tx_bytes_s,
                        (int)(tx_bytes_s+rx_bytes_s),
                        (int)error_stats[indx][RX_CHK_ERR - ERROR_INDEX],
                        (int)error_stats[indx][RX_SEQ_ERR - ERROR_INDEX],
                        (int)error_stats[indx][RX_XOFF - ERROR_INDEX],
                        (int)error_stats[indx][RX_SYN_ERR - ERROR_INDEX],
                        (int)error_stats[indx][TX_CHK_ERR - ERROR_INDEX],
                        (int)error_stats[indx][TX_SEQ_ERR - ERROR_INDEX],
                        (int)error_stats[indx][TX_XOFF - ERROR_INDEX ],
                        (int)error_stats[indx][TX_SYN_ERR - ERROR_INDEX],
                        total_error_count
                     );
            }
        }
    }
    if(true == avg_lun_prnt_actv && actv_lun_count)
    {
        total_error_count = 0;
        for (err_index = 1;err_index<(MAX_ERROR+1);  err_index ++)
        {
            total_error_count +=  (int)Total_error_stats[err_index];
        }
         printf ("%3d\t%3s\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\t%7d\n",
                 (int)(count * interval),"Avg",(int)(total_rx_pckts_l/actv_lun_count),
                 (int)(total_tx_pckts_l/actv_lun_count),
                 (int)((total_tx_pckts_l+total_rx_pckts_l)/actv_lun_count),
                 (int)(total_rx_bytes_l/actv_lun_count),
                 (int)(total_tx_bytes_l/actv_lun_count),
                 (int)((total_tx_bytes_l+total_rx_bytes_l)/actv_lun_count),
                 (int)(total_rx_pckts_s/actv_lun_count),
                 (int)(total_tx_pckts_s/actv_lun_count),
                 (int)((total_tx_pckts_s+total_rx_pckts_s)/actv_lun_count),
                 (int)(total_rx_bytes_s/actv_lun_count),
                 (int)(total_tx_bytes_s/actv_lun_count),
                 (int)((total_tx_bytes_s+total_rx_bytes_s)/actv_lun_count),
                 (int)(Total_error_stats[RX_CHK_ERR - ERROR_INDEX]/actv_lun_count),
                 (int)(Total_error_stats[RX_SEQ_ERR - ERROR_INDEX]/actv_lun_count),
                 (int)(Total_error_stats[RX_XOFF - ERROR_INDEX]/actv_lun_count),
                 (int)(Total_error_stats[RX_SYN_ERR - ERROR_INDEX]/actv_lun_count),
                 (int)(Total_error_stats[TX_CHK_ERR - ERROR_INDEX]/actv_lun_count),
                 (int)(Total_error_stats[TX_SEQ_ERR - ERROR_INDEX]/actv_lun_count),
                 (int)(Total_error_stats[TX_XOFF - ERROR_INDEX ]/actv_lun_count),
                 (int)(Total_error_stats[TX_SYN_ERR - ERROR_INDEX]/actv_lun_count),
                 (total_error_count/(int)actv_lun_count)
                );
    }
}
int print_msgpool_info( char *host)
{
    char chr, msgpool_path[128]={0};
    int cur, min, errsv;
    FILE *fp ;
    EXPCT_NE(host, NULL, -1, "Invalid argument, host\n");
    strncat(msgpool_path, COMMON_PATH, sizeof(COMMON_PATH));
    if (strcmp(host,"inc-adr3") == 0)
    {
        strncat(msgpool_path, "inc-adr3", sizeof("inc-adr3"));
    }
    else if (strcmp(host,"inc-scc") == 0)
    {
        strncat(msgpool_path, "inc-scc", sizeof("inc-scc"));
    }
    else
    {
        PR_ERR("ERROR Host not valid=%s\n",host);
    }
    strncat(msgpool_path, MSGPOOL_NAME, sizeof(MSGPOOL_NAME));
    fp = fopen(msgpool_path, "r");
    errsv = errno;
    EXPCT_NE(fp, NULL, -1, "fopen failed: %d\n", errsv);
    /* path has values in current and minimum order*/
    fscanf (fp, "%s", &chr);
    cur = atol(&chr);
    fscanf (fp, "%s", &chr);
    min = atol(&chr);
    fclose(fp);
    printf("message pool size: current %d minimum %d\n", cur, min);
    return 0;
}

/*Initialises the INC communication*/
int inc_communication_init(char * host,inc_comm *inccom)
{
    int ret,optval;
    int errsv,sockfd;
    int localportno,portno;
    struct hostent *local, *remote;
    struct sockaddr_in local_addr, remote_addr;
    char local_name[256];
    sk_dgram *dgram;

    EXPCT_NE(host, NULL, -1, "Invalid argument, host\n");
    EXPCT_NE(inccom, NULL, -1, "Invalid argument, inccom\n");

    if (strcmp(host,"adr3") == 0)
    {
        portno = localportno = 0xC716;
    }
    else
    {
        portno = localportno = 0xC700;
    }

    sprintf(local_name, "%s-local", host);
    local = gethostbyname(local_name);
    errsv = errno;
    EXPCT_NE(local, NULL, 1, "gethostbyname() failed: %d\n", errsv);

    local_addr.sin_family = AF_INET;
    memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);
    local_addr.sin_port = htons(localportno);

    remote = gethostbyname(host);
    errsv = errno;
    EXPCT_NE(remote, NULL, 2, "gethostbyname (remote) failed: %d\n", errsv);

    remote_addr.sin_family = AF_INET;
    memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
    remote_addr.sin_port = htons(portno); 

    sockfd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
    errsv = errno;
    EXPCT_NE(sockfd, -1, 3, "create socket failed: %d\n", errsv);

    dgram = dgram_init(sockfd, DGRAM_MAX, NULL);
    EXPCT_NE(dgram, NULL, 4, "dgram_init failed\n");

    //lint -e64
    optval = 1; 
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));
    errsv = errno;
    EXPCT_NE(sockfd, -1, 5, "setsockopt failed: %d\n", errsv);

    ret = bind(sockfd, (struct sockaddr *) &local_addr, sizeof(local_addr));
    errsv = errno;
    EXPCT_EQ(ret, 0, 6, "bind failed: %d\n", errsv);

    ret = connect(sockfd, (struct sockaddr *) &remote_addr, sizeof(remote_addr));
    errsv = errno;
    EXPCT_EQ(ret, 0, 7, "connect failed: %d\n", errsv);

    inccom->dgram = dgram; 
    inccom->sockfd = sockfd;
    return 0;
}

/* Function to disconnect the INC connection*/
int inc_communication_exit(inc_comm *inccom)
{
    int ret, errsv;

    EXPCT_NE(inccom, NULL, -1, "Invalid argument, inccom\n");

    ret = dgram_exit(inccom->dgram);
    errsv = errno;
    EXPCT_EQ(ret, 0, 1, "dgram_exit failed: %d\n", errsv);

    close(inccom->sockfd);

    return 0;
}

/*Evaluates the throughput with threshold values */
int inc_threshold_evaluate(char *host)
{

    unsigned long tx_bytes = 0,rx_bytes = 0,tx_pckts = 0,rx_pckts = 0;
    int i,res = 0;

    EXPCT_NE(host, NULL, -1, "Invalid argument, host\n");

    for(i = 0 ; i < MAX_LUN; i++ )
    {
        if(true == stats[AVERAGE][i].bLunActv)
        {
            tx_pckts += stats[AVERAGE][i].tx_packets;
            rx_pckts += stats[AVERAGE][i].rx_packets;
            tx_bytes += stats[AVERAGE][i].tx_bytes;
            rx_bytes += stats[AVERAGE][i].rx_bytes;
        }
    }
    if (strcmp (host,"inc-adr3") == 0)
    {     
        if ((tx_pckts < TH_TX_PACKETS_ADR3) || (rx_pckts < TH_RX_PACKETS_ADR3) || (rx_bytes < TH_RX_BYTES_ADR3) || (tx_bytes < TH_TX_BYTES_ADR3))
        {
            res = 100;
        }
    }
    else
    {
        if ((tx_pckts < TH_TX_PACKETS_SCC) || (rx_pckts < TH_RX_PACKETS_SCC) || (rx_bytes < TH_RX_BYTES_SCC) || (tx_bytes < TH_TX_BYTES_SCC))
        {
            res = 200;
        }
    }
    return res;
}
