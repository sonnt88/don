/************************************************************************
| FILE:         inc_scc_port_extender_gpio.h
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Definitions for Inter Node Communication
|               SCC GPIO Port Extender service
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 04.02.13  | Initial revision           | tma2hi
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#ifndef INC_SCC_PORT_EXTENDER_GPIO_H
#define INC_SCC_PORT_EXTENDER_GPIO_H

#define SCC_PORT_EXTENDER_GPIO_C_COMPONENT_STATUS_MSGID	0x20
#define SCC_PORT_EXTENDER_GPIO_R_COMPONENT_STATUS_MSGID	0x21
#define SCC_PORT_EXTENDER_GPIO_R_REJECT_MSGID		0x0B
#define SCC_PORT_EXTENDER_GPIO_C_GET_STATE_MSGID	0x30
#define SCC_PORT_EXTENDER_GPIO_R_GET_STATE_MSGID	0x31
#define SCC_PORT_EXTENDER_GPIO_C_SET_STATE_MSGID	0x32
#define SCC_PORT_EXTENDER_GPIO_R_SET_STATE_MSGID	0x33

#endif /* INC_SCC_PORT_EXTENDER_GPIO_H */
