/************************************************************************
| FILE:         inc.h
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for the INC user-space interface. It
|               shall be included by all user-space INC applications.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2012 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 17.12.12  | Initial revision           | tma2hi
| 07.10.16	| Support for TCP/IP		 | Venkatesh Parthasarathy
|			| based Gen4 INC			 |
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#ifndef INC_H
#define INC_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef OSAL_GEN3_SIM
#define AF_BOSCH_INC_ADR      AF_INET
#else
#define AF_BOSCH_INC_ADR      41 /* AF_INC */
#endif

#ifdef OSAL_GEN4
#define AF_BOSCH_INC_AUTOSAR  AF_INET
#else
#define AF_BOSCH_INC_AUTOSAR  41 /* AF_INC */
#endif

#define AF_BOSCH_INC_LINUX    AF_INET

#ifdef __cplusplus
}
#endif

#endif /* INC_H */
