/************************************************************************
  | FILE:         inc_perf_common.h
  | PROJECT:      platform
  | SW-COMPONENT: INC
  |------------------------------------------------------------------------------
  | DESCRIPTION:   INC performance measurement common header file 
  |                File contains the common function prototypes  and global data shared 
  |                between INC performance measurement commmand line apps & OEDT's 
  |------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2014 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification               | Author
  | 11.07.14  | Initial revision           | Shashikant Suguni
  | 03.03.16  | Update print_throughput()  | Madhu Sudhan Swargam (RBEI/ECF5)
  | --.--.--  | ----------------           | -------, -----
  |
  |*****************************************************************************/
#ifndef INC_PERF_COMMON_H
#define INC_PERF_COMMON_H

#include "dgram_service.h"
#include <stdbool.h>
typedef struct _inccomm{
    sk_dgram *dgram;
    int sockfd;
}inc_comm;

#define COMMAND "/proc/net/dev"
#define COMMON_PATH "/sys/kernel/debug/ssi32/"
#define MSGPOOL_NAME "/msgpool"
#define STATS_NAME "/stats"

int throughput_handler(char *host,unsigned long interval,unsigned long interval_count);
void print_throughput(unsigned long interval,unsigned long count, 
                      bool per_lun_prnt_actv, bool avg_lun_prnt_actv);
int inc_communication_init(char * host,inc_comm *inccom);
int print_msgpool_info( char *host);
int inc_communication_exit(inc_comm *inccom);
int inc_threshold_evaluate(char *host);
#endif /* INC_PERF_COMMON_H */
