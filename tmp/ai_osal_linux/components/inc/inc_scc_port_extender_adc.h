/************************************************************************
| FILE:         inc_scc_port_extender_adc.h
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Definitions for Inter Node Communication
|               SCC ADC Port Extender service
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 08.04.13  | Initial revision           | ant3kor
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#ifndef INC_SCC_PORT_EXTENDER_ADC_H
#define INC_SCC_PORT_EXTENDER_ADC_H

#define SCC_PORT_EXTENDER_ADC_C_COMPONENT_STATUS_MSGID	 0x20
#define SCC_PORT_EXTENDER_ADC_R_COMPONENT_STATUS_MSGID   0x21
#define SCC_PORT_EXTENDER_ADC_R_REJECT_MSGID		 0x0B
#define SCC_PORT_EXTENDER_ADC_C_GET_SAMPLE_MSGID 	 0x40
#define SCC_PORT_EXTENDER_ADC_R_GET_SAMPLE_MSGID         0x41
#define SCC_PORT_EXTENDER_ADC_C_SET_THRESHOLD_MSGID	 0x42
#define SCC_PORT_EXTENDER_ADC_R_SET_THRESHOLD_MSGID	 0x43
#define SCC_PORT_EXTENDER_ADC_R_THRESHOLD_HIT_MSGID	 0x45
#define SCC_PORT_EXTENDER_ADC_C_GET_CONFIG_MSGID	 0x46
#define SCC_PORT_EXTENDER_ADC_R_GET_CONFIG_MSGID	 0x47
 
#endif /*INC_SCC_PORT_EXTENDER_ADC_H*/
