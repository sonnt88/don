/************************************************************************
| FILE:         inc_basic_test.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Basic test program for sending and receiving INC messages.
|
|               Setup for test with fake device (add to /etc/hosts):
|               modprobe dev-fake
|
|               App1(loopback): bind to 192.168.104.1:51195, connect to 192.168.104.2:51194
|               /opt/bosch/base/bin/inc_basic_test_out.out -l -p51194 -b51195 fake1-local fake1 &
|
|               App2(sender): bind to 192.168.103.1:51194, connect to 192.168.103.2:51195
|               /opt/bosch/base/bin/inc_basic_test_out.out -p51195 -b51194 fake0-local fake0 &
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2012 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 17.12.12  | Initial revision           | Andreas Pape
| 28.01.13  | Use datagram service       | tma2hi
| 06.02.13  | Bind to local address      | tma2hi
| 19.03.13  | Update description         | tma2hi
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#define USE_DGRAM_SERVICE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/time.h>
#include <time.h>
#include <netinet/tcp.h>
#include "inc.h"

#ifdef USE_DGRAM_SERVICE
#include "dgram_service.h"
#endif

#define PR_ERR(fmt, args...) \
      { fprintf(stderr, "inc_basic_test: %s: " fmt, __func__, ## args); \
      return -1; }

static uint32_t time_diff_us(struct timeval *new, struct timeval *old)
{
	return ((new->tv_sec - old->tv_sec) * 1000*1000) + (new->tv_usec - old->tv_usec );
}

#define DEFAULT_MSGSZ 240
int main(int argc, char *argv[])
{
	int sockfd, portno=0,localportno=0, wr,rd,rd_tmp, i, message_size = DEFAULT_MSGSZ, no_of_packets=0, loop =0;
	struct sockaddr_in serv_addr, local_addr;
	struct hostent *server, *local;
	char buffer[DEFAULT_MSGSZ+1];
	uint32_t actualBytes_sent=0;
	int opt, optmsk;
	int rtt_min=0x7fffffff,rtt_max =0,rtt_avg =0,rtt_sum =0,rtt_val;
#ifdef USE_DGRAM_SERVICE
   sk_dgram *dgram;
#endif

	struct timeval  rtt_start, rtt_stop;

	for (optmsk = opterr = 0; (opt = getopt(argc, argv, "ln:p:b:")) != -1;) {
		switch (opt) {
		case 'n':
			no_of_packets = atoi(optarg);
			break;
		case 'p':
			portno = atoi(optarg);
			break;
		case 'b':
			localportno = atoi(optarg);
			break;
		case 'l':
			loop = 1;/*reply received data*/
			break;

		default:
			PR_ERR("invalid option\n");
		}
	}
	if(portno == 0)
		PR_ERR("invalid port - use option -p\n");

	sockfd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);

	if (sockfd < 0) 
		PR_ERR("ERROR opening socket\n");

#ifdef USE_DGRAM_SERVICE
   dgram = dgram_init(sockfd, DGRAM_MAX, NULL);
   if (dgram == NULL)
      PR_ERR("dgram_init failed\n");
#endif

   if (optind >= argc)
      PR_ERR("ERROR local host missing\n");
   local = gethostbyname(argv[optind]);
	if (local == NULL)
		PR_ERR("ERROR, no such local host\n");
   local_addr.sin_family = AF_INET;
   memcpy((char *)&local_addr.sin_addr.s_addr, (char *)local->h_addr, local->h_length);
   local_addr.sin_port = htons(localportno);
   optind++;

   if (optind >= argc)
      PR_ERR("ERROR remote host missing\n");
   server = gethostbyname(argv[optind]);
   if (server == NULL)
      PR_ERR("ERROR, no such remote host\n");
   serv_addr.sin_family = AF_INET;
   memcpy((char *)&serv_addr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
   serv_addr.sin_port = htons(portno);
//lint -e64
	if (bind(sockfd, (struct sockaddr *) &local_addr, sizeof(local_addr)) < 0)
		PR_ERR("ERROR on binding\n");

	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
		PR_ERR("ERROR connecting\n");

	memset(buffer, 0, sizeof(buffer));
	i = 0;


	/*send data to server  */
	while((i<no_of_packets) ||(no_of_packets==0))
	{
		wr=0;
		rd=0;

		if (!loop)
		{

			wr += sprintf(buffer+wr,"MSG %04d port %04d", i, portno);
			wr += sprintf(buffer+wr,"---INC TEST LOOP---");
			buffer[DEFAULT_MSGSZ]='\0';
			printf("SEND    : %s", buffer);

			gettimeofday(&rtt_start, NULL);
#ifdef USE_DGRAM_SERVICE
         wr = dgram_send(dgram, buffer, message_size);
#else
         wr = write(sockfd,buffer,message_size);
#endif

			if (wr < 0) 
				PR_ERR("ERROR writing to socket\n");
			actualBytes_sent += wr;
			i++;

			/*read until full message received*/
			while(rd < message_size) {
#ifdef USE_DGRAM_SERVICE
			   rd_tmp = dgram_recv(dgram, buffer+rd, message_size-rd);
#else
				rd_tmp = read(sockfd, buffer+rd, message_size-rd);
#endif
				if (rd_tmp < 0)
					PR_ERR("ERROR reading from socket\n");
				rd+= rd_tmp;
			}

			gettimeofday(&rtt_stop, NULL);

			buffer[DEFAULT_MSGSZ]='\0';
			printf("RECEIVED: %s\n", buffer);

			rtt_val = time_diff_us(&rtt_stop, &rtt_start);
			if(rtt_val < rtt_min)
				rtt_min = rtt_val;
			if(rtt_val > rtt_max)
				rtt_max = rtt_val;
			rtt_sum += rtt_val;
			rtt_avg = rtt_sum/i;
			printf("RTT %d/%d bytes: %dus  min %dus max %dus avg %dus\n",wr,rd, rtt_val,rtt_min,rtt_max,rtt_avg);
		} else {
			/*read until full message received*/
			while(rd < message_size) {
				rd_tmp = read(sockfd, buffer+rd, message_size-rd);
				if (rd_tmp < 0)
					PR_ERR("ERROR reading from socket\n");
				rd+= rd_tmp;
			}
			buffer[DEFAULT_MSGSZ]='\0';
			printf("LOOP: %s", buffer);
#ifdef USE_DGRAM_SERVICE
         wr = dgram_send(dgram, buffer, message_size);
#else
			wr = write(sockfd,buffer,message_size);
#endif

			if (wr < 0) 
				PR_ERR("ERROR writing to socket\n");
			actualBytes_sent += wr;
			i++;


		}
		usleep(500000);
	}

	printf("Number of packets sent %d\n",
	      (actualBytes_sent / (uint32_t) message_size));

#ifdef USE_DGRAM_SERVICE
   if (dgram_exit(dgram) < 0)
      PR_ERR("dgram_exit failed\n");
#endif

	close(sockfd);

	return 0;
}



