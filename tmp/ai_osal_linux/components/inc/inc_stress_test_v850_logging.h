/*
    header file for inc_stress_test_logging.c
*/

#ifndef INC_STRESS_TEST_LOGGING_H
#define INC_STRESS_TEST_LOGGING_H

/***************************************************************************************************
**                                    MACROS                                                      **
***************************************************************************************************/

/* below macro uses a buffer 'TimeStamp'. It is declared in inc_stress_test_logging.c 
   and an extern reference is present in this file. */
#define LOG_MESSAGE(format, args...) \
{   fprintf(flog, "(%s) %s, line %d, %s() : ", GetTimeStamp(TimeStamp), __FILE__, __LINE__, __func__); \
    fprintf(flog, format, ##args); }
  
#define TIME_STAMP_SIZE 50

/***************************************************************************************************
**                            USER-DEFINED DATA TYPES                                             **
***************************************************************************************************/
typedef struct time_struct {
			int days;
			int hours;
			int minutes;
			int seconds;
}TimeStruct;

/***************************************************************************************************
**                                   Variables                                                    **
***************************************************************************************************/
extern FILE *flog;
extern char TimeStamp[];

/***************************************************************************************************
**                              FUNCTION DECLARATIONS                                             **
***************************************************************************************************/
extern void GetErrorMessage(int err, char *buffer, size_t size);         
extern int OpenLogFile(void);
extern void CloseLogFile(void);
extern int ClearLog(void);
extern char *GetTimeStamp(char *Date__Time);
extern void convert(time_t val, TimeStruct *ts);
extern void FreeCharBuffers(int num,...);

#endif          //INC_STRESS_TEST_LOGGING_H

/*
END OF FILE
*/
