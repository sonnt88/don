/************************************************************************
| FILE: inc-sec-populate.c
| PROJECT: Platform
| SW-COMPONENT: INC
|
| DESCRIPTION:	INC Security Implementation
|		Reads the LUN, group name and security flag entries from 
|		/etc/inc/security/01-inc-scc-sec.conf file. Retreives the 
|		group id from group name and write the LUN, group id and 
|		security flag to /sys/class/net/inc-scc/inc_sec_lun_gid_mapping file.
|		Reads the LUN, group name and security flag entries from 
|		/etc/inc/security/02-inc-adr3-sec.conf file. Retreives the 
|		group id from group name and write the LUN, group id and 
|		security flag to /sys/class/net/inc-adr3/inc_sec_lun_gid_mapping file.
|		
| COPYRIGHT: (c) 2015 Robert Bosch GmbH
|
| HISTORY:
| Date		| Modification		| Author
| 17/11/2015	| Initial Rev.		| Venkatesh Parthasarathy (RBEI/ECF5)
| 05/01/2015	| Review Rework - Fix 	| Venkatesh Parthasarathy (RBEI/ECF5)
|		| lint warnings		
|
************************************************************************/


#include <stdio.h>
/*@=skipposixheaders@*/
#include <fcntl.h>
#include <grp.h>
#include <pwd.h>
#include <errno.h>
#include <unistd.h>

#define PR_ERR(fmt, args...) \
      { fprintf(stderr, "inc_sec_populate: %s: " fmt, __func__, ## args); }

#define BUF_LEN 12

static int inc_sec_write2sysfs(char *config_file, char *sys_file)
{
	FILE *fp_config, *fp_sysfs;
	char  gname[32], lun[2], buf[BUF_LEN];
	int sec_flag, ret;
	struct group *grp;

	if (access(sys_file, F_OK) < 0) {
		PR_ERR("File %s not exists\n", sys_file);
		return 0;
	}

	fp_config = fopen(config_file, "r");
	if (!fp_config) {
		PR_ERR("Config file %s open error\n", config_file);
		return -1;
	}

	fp_sysfs = fopen(sys_file, "w");
	if (!fp_sysfs) {
		PR_ERR("sysfs file %s open error\n", sys_file);
		(void) fclose(fp_config);
		return -1;
	}

	setbuf(fp_sysfs, NULL);

	while (fscanf(fp_config, "%s %s %d", lun, gname, &sec_flag) != EOF ) {

		grp = getgrnam(gname);
		if (grp) {
			(void) snprintf(buf, BUF_LEN, "%s %05d %d\n", lun, grp->gr_gid, sec_flag);
			ret = fprintf(fp_sysfs,"%s", buf);
			if (ret <= 0) {
				PR_ERR("Write to sysfs fail. Err: %d\n", errno);
				(void) fclose(fp_sysfs);
				(void) fclose(fp_config);
				return -1;
			}
		} else {
			PR_ERR("Group %s not found\n", gname);
			(void) fclose(fp_sysfs);
			(void) fclose(fp_config);
			return -1;
		}
	}

	(void) fclose(fp_sysfs);
	(void) fclose(fp_config);
	return 0;
}

int main()
{
	if (inc_sec_write2sysfs("/etc/inc/security/01-inc-sec-scc.conf", "/sys/class/net/inc-scc/inc_sec_lun_gid_mapping") < 0)
		return -1;
	if (inc_sec_write2sysfs("/etc/inc/security/02-inc-sec-adr3.conf", "/sys/class/net/inc-adr3/inc_sec_lun_gid_mapping") < 0)
		return -1;
	return 0;
}
