/************************************************************************
| FILE:         inc_sim_gpio.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Test program for simulating a remote GPIO port extender for INC.
|
|               Setup for test with GPIO_INC kernel module and fake device:
|
|               modprobe dev-fake
|               /opt/bosch/base/bin/inc_sim_gpio_out.out
|               modprobe gpio-inc
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2012 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 17.12.12  | Initial revision           | tma2hi
| 28.01.13  | Use datagram service       | tma2hi
|           | and new include for msg id |
| 19.03.13  | Use host name, update desc | tma2hi
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#define USE_DGRAM_SERVICE

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_port_extender_gpio.h"

#ifdef USE_DGRAM_SERVICE
#include "dgram_service.h"
#endif

#define GPIO_INC_STATUS_ACTIVE   0x01

int gpio_is_active = 0;

void *tx_thread(void *arg)
{
   int n, cycle = 0;
   char sendbuf[1024];

#ifdef USE_DGRAM_SERVICE
   sk_dgram *dgram = (sk_dgram *) arg;
#else
   int fd = (int) arg;
#endif

   sendbuf[0] = SCC_PORT_EXTENDER_GPIO_R_GET_STATE_MSGID;
   sendbuf[1] = 13;
   sendbuf[2] = 0xE2;
   sendbuf[3] = 0x00;
   sendbuf[4] = 0xE2;
   sendbuf[5] = 0xFF;

   do
   {
      usleep(1000000);

      if (gpio_is_active) {

//         sendbuf[2] = sendbuf[2] >> 1;
//         sendbuf[4] = sendbuf[4] >> 1;
         sendbuf[2] = sendbuf[2] ^ 0xFF;

#ifdef USE_DGRAM_SERVICE
         n = dgram_send(dgram, sendbuf, 6);
#else
         n = send(fd, sendbuf, 6, 0);
#endif
         if (n == -1)
         {
            fprintf(stderr, "INC_sendto failed: %d\n", n);
            break;
         }
         fprintf(stderr, "<- R_GET_STATE: %d\n", sendbuf[1]);
      }

      cycle++;
   } while (1);

   return 0;
}

int main()
{
   int m, n, fd, ret;
   char sendbuf[1024];
   char recvbuf[1024];
   pthread_t tid;
#ifdef USE_DGRAM_SERVICE
   sk_dgram *dgram;
#endif
   struct hostent *local, *remote;
   struct sockaddr_in local_addr, remote_addr;

   local = gethostbyname("fake1-local");
   if (local == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(fake1-local) failed: %d\n", errsv);
      return 1;
   }
   local_addr.sin_family = AF_INET;
   memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);
   local_addr.sin_port = htons(PORT_EXTENDER_GPIO_PORT); /* from inc_ports.h */

   remote = gethostbyname("fake1");
   if (remote == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(fake1) failed: %d\n", errsv);
      return 1;
   }
   remote_addr.sin_family = AF_INET;
   memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
   remote_addr.sin_port = htons(PORT_EXTENDER_GPIO_PORT); /* from inc_ports.h */

   fprintf(stderr, "local %s:%d\n", inet_ntoa(local_addr.sin_addr), ntohs(local_addr.sin_port));
   fprintf(stderr, "remote %s:%d\n", inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));

   fd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
   if (fd == -1)
   {
      int errsv = errno;
      fprintf(stderr, "create socket failed: %d\n", errsv);
      return 1;
   }

#ifdef USE_DGRAM_SERVICE
   dgram = dgram_init(fd, DGRAM_MAX, NULL);
   if (dgram == NULL)
   {
      fprintf(stderr, "dgram_init failed\n");
      return 1;
   }
#endif
//lint -e64
   ret = bind(fd, (struct sockaddr *) &local_addr, sizeof(local_addr));
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "bind failed: %d\n", errsv);
      return 1;
   }

   ret = connect(fd, (struct sockaddr *) &remote_addr, sizeof(remote_addr));
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "connect failed: %d\n", errsv);
      return 1;
   }

#ifdef USE_DGRAM_SERVICE
   if (pthread_create(&tid, NULL, tx_thread, (void *) dgram) == -1)
      fprintf(stderr, "pthread_create failed\n");
#else
   if (pthread_create(&tid, NULL, tx_thread, (void *) fd) == -1)
      fprintf(stderr, "pthread_create failed\n");
#endif

   do {
      do {
#ifdef USE_DGRAM_SERVICE
         n = dgram_recv(dgram, recvbuf, sizeof(recvbuf));
#else
         n = recv(fd, recvbuf, sizeof(recvbuf), 0);
#endif
      } while(n < 0 && errno == EAGAIN);
      if (n < 0)
      {
         int errsv = errno;
         fprintf(stderr, "recv failed: %d\n", errsv);
         return 1;
      }

      fprintf(stderr, "recv: %d\n", n);

      if (n == 0)
      {
         fprintf(stderr, "invalid msg size: %d\n", n);
         return 1;
      }

      switch (recvbuf[0])
      {
      case SCC_PORT_EXTENDER_GPIO_C_COMPONENT_STATUS_MSGID:
         if (n != 2) {
            fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
            return 1;
         }
         if (recvbuf[1] != GPIO_INC_STATUS_ACTIVE) {
            fprintf(stderr, "invalid status: %d\n", recvbuf[1]);
            return 1;
         }
         fprintf(stderr, "-> C_COMPONENT_STATUS: %d\n", recvbuf[1]);

         sendbuf[0] = SCC_PORT_EXTENDER_GPIO_R_COMPONENT_STATUS_MSGID;
         sendbuf[1] = GPIO_INC_STATUS_ACTIVE;
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 2);
#else
         m = send(fd, sendbuf, 2, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_COMPONENT_STATUS: %d\n", sendbuf[1]);
         break;

      case SCC_PORT_EXTENDER_GPIO_C_GET_STATE_MSGID:
         if (n != 1) {
            fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
            return 1;
         }
         fprintf(stderr, "-> C_GET_STATE: %d\n", recvbuf[1]);

         sendbuf[0] = SCC_PORT_EXTENDER_GPIO_R_GET_STATE_MSGID;
         sendbuf[1] = 13;
         sendbuf[2] = 0xE2;
         sendbuf[3] = 0x00;
         sendbuf[4] = 0xE2;
         sendbuf[5] = 0xFF;
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 6);
#else
         m = send(fd, sendbuf, 6, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_GET_STATE: %d\n", sendbuf[1]);

         gpio_is_active = 1;
         break;

      case SCC_PORT_EXTENDER_GPIO_C_SET_STATE_MSGID:
         if (n != 3) {
            fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
            return 1;
         }
         fprintf(stderr, "-> C_SET_STATE: %d %d\n", recvbuf[1], recvbuf[2]);

//         usleep(3000000);

         sendbuf[0] = SCC_PORT_EXTENDER_GPIO_R_SET_STATE_MSGID;
         sendbuf[1] = recvbuf[1];
         sendbuf[2] = recvbuf[2];
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 3);
#else
         m = send(fd, sendbuf, 3, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_SET_STATE: %d %d\n", sendbuf[1], sendbuf[2]);
         break;

      default:
         fprintf(stderr, "invalid msgid: 0x%02X\n", recvbuf[0]);
         return 1;
      }

   } while (n > 0);

#ifdef USE_DGRAM_SERVICE
   ret = dgram_exit(dgram);
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "dgram_exit failed: %d\n", errsv);
      return 1;
   }
#endif

   close(fd);

   return 0;
}
