/************************************************************************
| FILE:         inc_scc_adr3.h
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Definitions for Inter Node Communication
|               SCC ADR3 
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 01.10.13  | Initial revision           | swm2kor
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#ifndef INC_SCC_ADR3_H
#define INC_SCC_ADR3_H

/* message id*/
#define SCC_ADR3CTRL_C_COMPONENT_STATUS      0x20
#define SCC_ADR3CTRL_R_COMPONENT_STATUS      0x21
#define SCC_ADR3CTRL_R_REJECT                0x0B
#define SCC_ADR3CTRL_C_SET_RESET             0x30
#define SCC_ADR3CTRL_R_SET_RESET             0x31
#define ADR3_STATUS_ACTIVE                   0x01
#define ADR3_STATUS_INACTIVE                 0x02
#define ADR3_RESET_STATUS_ACTIVE             0x01
#define ADR3_RESET_STATUS_RELEASE            0x00
#define ADR3_FIRST_VERSION                   0x01
#define ADR3_MODE_NORMAL                     0x00
#define ADR3_MODE_DOWNLOAD                   0x01
 
#endif /* INC_SCC_ADR3_H */