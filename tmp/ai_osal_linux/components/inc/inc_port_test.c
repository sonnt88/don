#define USE_DGRAM_SERVICE

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/tcp.h>
#include "inc.h"
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>

#ifdef USE_DGRAM_SERVICE
#include "dgram_service.h"
sk_dgram *dgram;
#endif


int main(int argc, char *argv[]) {
	struct sockaddr_in servAddr, clientAddr;
	struct hostent *server, *local;
	socklen_t addr_size;
	int sockfd, port, remotePort = 0, startPort = 0xC700, endPort = 0xC7FF;
	int status, enable = 1, n;
	char str[INET_ADDRSTRLEN];
	int opt, optmsk, mode;

	strcpy(intf_name,"eth0");

	for (optmsk = opterr = 0; (opt = getopt(argc, argv, "p:b:I:S:O:L:M:P:V:C:D:T:G")) != -1;) {
		switch (opt) {
		case 'I':
			strcpy(intf_name,optarg);
			break;
		case 'S':
			startPort = atoi(optarg);
			break;
		case 'E':
			endPort = atoi(optarg);
			break;
		case 'p':
			remotePort = atoi(optarg);
			break;
		default:
			PR_ERR("invalid option Err:%d Argc:%d Arg:%c\n",opterr,argc,opt);
			break;
		}
	}

	if(remotePort == 0 || startPort < 0xC700 || endPort > 0xC7FF) {
                status = -20;
                PR_ERR("invalid port usage\n");
        }

        PR_MSG("Remote port %d, Start port %d, End port %d\n", remotePort, startPort, endPort);	


	server = gethostbyname(intf_name);
	if (server == NULL)
		PR_ERR("ERROR, no such remote host\n");

	servAddr.sin_family = AF_INET;
	memcpy((char *)&servAddr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
	servAddr.sin_port = htons(remotePort);

	sprintf(local_name, "%s-local", intf_name);
	local = gethostbyname(local_name);
	if (local == NULL)
		PR_ERR("ERROR, no such local host\n");

	for (port = startPort; port <= endPort; port++ ) {

		PR_MSG("Test portno : %d\n", port);

		clientAddr.sin_family = AF_INET;
		memcpy((char *)&clientAddr.sin_addr.s_addr, (char *)local->h_addr, local->h_length);
		clientAddr.sin_port = htons(port);

		sockfd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
		if (sockfd < 0) {
			status = errno;
			PR_ERR("ERROR opening socket: %d %s\n", status, strerror(status));
		}

#ifdef USE_DGRAM_SERVICE
		dgram = dgram_init(sockfd, DGRAM_MAX, NULL);
		if (dgram == NULL)
			PR_ERR("dgram_init failed\n");
#endif


		addr_size = sizeof(clientAddr);
		status = bind(sockfd, (struct sockaddr *) &clientAddr, addr_size);
		if (status < 0) {
			status = errno;
			PR_MSG("ERROR binding: %d %s\n", status, strerror(status));
			goto ERR;
		}

		addr_size = sizeof(servAddr);
		status = connect(sockfd,(struct sockaddr *) &servAddr,addr_size);
		if(status < 0) {
			status = errno;
			PR_MSG("ERROR connecting: %d %s\n", status, strerror(status));
			goto ERR;
		}

                strcpy(buffer,"porttest");
#ifdef USE_DGRAM_SERVICE
                n = dgram_send(dgram, buffer, 9);
#else
                send(fd, buffer, 6, 0);
#endif

#ifdef USE_DGRAM_SERVICE
                n = dgram_recv(dgram, buffer, 100);
#else
                recv(fd, buffer, 100, 0);
#endif
	}



ERR:
#ifdef USE_DGRAM_SERVICE
	if (dgram_exit(dgram) < 0)
		PR_ERR("dgram_exit failed\n");
#endif

	if(sockfd)
		close(sockfd);

	return status;
}

