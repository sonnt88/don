/*****************************************************************************************************************************************************************************/
/*****************************************************************************************************************************************************************************/
/*****************************************************************************************************************************************************************************/
/*****************************************************************************************************************************************************************************/

------------------------------------------------------------------README-------------------------------------------------------------------------------------------------------

/*****************************************************************************************************************************************************************************/
/*****************************************************************************************************************************************************************************/
/*****************************************************************************************************************************************************************************/

How to run the testcases implemented for socket interfaces

Compilation

PC TOOL:

gcc inc_main.c inc_demo.c dgram_service.c -o pctool -lpthread


TARGET:
Path to crosscompiler: Example if it is /opt/tooling/codesourcery/MGC-2014.05-46-arm-gcc-4.8.3/Sourcery_CodeBench_for_ARM_Embedded/bin/


/opt/tooling/codesourcery/MGC-2014.05-46-arm-gcc-4.8.3/Sourcery_CodeBench_for_ARM_Embedded/bin/arm-none-linux-gnueabi-gcc dgram_service.c inc_main.c inc_demo.c -o inc_send


To run the server mode you could use the following command:

./pctool -O server -p 50942 -S 1




To run the client mode you could use the following command:
./inc_send -I eth0 -p 50942 -b 50943-O client -L 30 - M 1
Note: to allow multiple clients to run on the same machine you have to use the same server port but different server port numbers

Options to run the application:

/*I: Interface, define the interface that the test will operate with*/
Example: - T eth0

/*O: Opernation Mode, C or S for client or server respectively*/
Example: -O client 
         -O server
/*p: Server port number*/
Example: -p 50942 


/*p: Client port number*/
Example: -b 50943 


/*S: MultiClient mode enabled*/
Example: -S 1
To enable the multiclient mode, the default that it is disabled

/*L: Define the length of the buffer to be sent out*/
Example: -L 300
Define the length of the buffers to be sent out

/*M: Mode, to choose between three modes, fixed pattern, alternate pattern, and Pre-Defined Pattern */
/*P:Pattern, to define the pattern that will be used for the test mode 3, this option can't be used with the two options*/
Example:
Mode 1, sends out a single character, Mode 2, sends a fixed longer pattern  and Mode 3 allows the user to define their own pattern sequence
 -M 2
 -M 3 -P HJAHSJJASASSSA 

/*V: Verification flag to verify the data being sent*/
Example:
-V 1

/*D: Delay for fast/slow sending and receiving*/
-D defines the delay for the slow sending

/*C: Category for speed testing, slow/fast receiving and sending*/
Two options are eithe slowsend or slowrecv
Example: 
-C slowsend

/*T: Socket Type: Blocking (B) or NonBlocking(NB)*/

Example:
Two options are -T NB or -T B
Default is blocking sockets