/************************************************************************
  | FILE:         inc_analyse_g4.c
  | PROJECT:      platform
  | SW-COMPONENT: INC
  |------------------------------------------------------------------------------
  | DESCRIPTION:   INC performance application 
  |                The app shall read out the existing netdevice statistics at a 
  |                configurable interval, compute the throughput and print it to stdout.
  |                Throughput figures: Average for last interval, average since start of app, 
  |                Total / RX / TX, in bytes/second and packets/second
  |                Command line parameters: Hostname, measurement interval [seconds]
  |
  |
  |                Execution:
  |                /opt/bosch/base/bin/inc_analyse_g4_out.out hostname interval 
  |                
  |                Eg. For host INC-SCC(V850), interval 1sec
  |                /opt/bosch/base/bin/inc_analyse_g4_out.out inc-scc 1 
  |
  |------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2014 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification               | Author
  | 06.10.16  | Initial revision           | Shahida Mohammed Ashraf
  | --.--.--  | ----------------           | -------, -----
  |
  |*****************************************************************************/
#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "inc_perf_common_g4.h"

#define PR_ERR(fmt, args...) \
{ fprintf(stderr, "inc_analyse_g4: %s: " fmt, __func__, ## args); \
    exit(EXIT_FAILURE); }

/* Prints the INC statistics banner */
void print_incstats_banner(void)
{
    printf ("inc_analyse_out.out: measure throughput of INC communication\n");
    printf ("glossary:\n");
    printf ("\ttime:      elapsed time since start of measurement\n");
    printf ("\tlast:      average throughput for last interval only\n");
    printf ("\tcomplete:  average throughput since start of measurement\n");
    printf ("|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n");
    printf ("|--time-|------------------------------------------------last---------------------------------------|------------------------------------------------complete-------------------------------------|\n");
    printf ("|--sec--|---------------packets/sec-----------------|------------------bytes/sec--------------------|----------------packets/sec-----------------|-------------------bytes/sec--------------------|\n");
    printf ("\t RX\t\tTX\t\tTOT\t\tRX\t\tTX\t\tTOT\t\tRX\t\tTX\t\tTOT\t\tRX\t\tTX\t\tTOT\n");
}

int main(int argc, char *argv[])
{

    unsigned long interval,count = 0;
    char host[20]; 
    int ret;

    memset (host,0,sizeof (host));

    if (argc != 3) {
        PR_ERR("Application needs two params \"Hostname\" & \"Interval\", \n Hostname -> inc-scc for V850 \nUSAGE: %s Hostname Interval\n",argv[0])
    }

    interval = atol(argv[2]); 
    strcpy (host,argv[1]); 
    
    /*Print the banner*/
    print_incstats_banner ();

    while (1)
    {
        /* Compute the throughtput per interval */
        ret = throughput_handler(host,interval,count);
        if (ret !=0)
        {
            PR_ERR("throughput_handler failed\n")
        }

        /*Appends the throughput figures to console*/
        print_throughput(interval,count);

        count ++;
        sleep (interval);

    }

}
