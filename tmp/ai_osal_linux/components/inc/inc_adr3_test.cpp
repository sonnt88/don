/************************************************************************
| FILE:         inc_adr3_test.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Test program for ADR3.
|
|               Execution:
|               /opt/bosch/base/bin/inc_adr3_test_out.out
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 15.12.12  | Initial revision           | Ranjit Katuri
| 15.02.13  | Use datagram service       | tma2hi
|           | Bind to local address      |
| 19.03.13  | Update description         | tma2hi
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/time.h>
#include <time.h>
#include <netinet/tcp.h>
#include "inc.h"
#include "dgram_service.h"

#include <limits>
#include <iostream>
using namespace std;

bool bTerminate = false;
//Function to send commands based on user choice
void vMainLoopIterate(int choice, sk_dgram *dgram);
//Function to run a loop to receive messages
void* vRxThread(void* arg);
//Helper function to send test options
void vPrintTestOptions(void);

int main()
{
	cout<<"Start ..."<<endl;
	int sockfd=-1, portno=0xC716,localportno=0xC716;
	struct sockaddr_in serv_addr;
	struct hostent *server,*lhost;
	sk_dgram *dgram;

	/**
	* Create a socket
	*/
	cout<<"Creating Socket with Address Family AF_BOSCH_INC_AUTOSAR ..";
	sockfd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);

	if (sockfd < 0) 
	{
		cout<<"Failed"<<endl;
		bTerminate = true;
	}
	else
	{
		cout<<"OK"<<endl;
	}
	cout<<"SockFD Created - "<<sockfd<<endl;

	dgram = dgram_init(sockfd, DGRAM_MAX, NULL);
	if (dgram == NULL)
	{
	   cout<<"dgram_init failed"<<endl;
      bTerminate = true;
	}

	int on =1;
	int rc = ioctl(sockfd, FIONBIO, (char *)&on);
	if (rc < 0)
	{
		cout<<"ioctl() failed";
		close(sockfd);
		exit(-1);
	}

	cout<<"Get information about localinc0 .. ";
	lhost = gethostbyname("adr3-local");

	if (lhost == NULL)
	{
		cout<<"Failed, no such host adr3-local..."<<endl;
		bTerminate = true;
	}
	else
	{
		cout<<"OK"<<endl;
      memcpy((char *)&serv_addr.sin_addr.s_addr, (char *)lhost->h_addr, lhost->h_length);//Address received from gethostbyname function
   }

   /**
   * Bind to a local port to recieve response
   */
   cout<<"Binding to Port .. "<<localportno<<"..";
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_port = htons(localportno);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		cout<<" Failed"<<endl;
		bTerminate = true;
	}
	else
	{
		cout<<" OK"<<endl;
	}

	/**
	* Get host information ... (Note to developer > Check adr3/v850 is present is present in /etc/hosts, if not present add it)
	*/
	cout<<"Get information about ADR .. ";
	server = gethostbyname("adr3");

	if (server == NULL)
	{
		cout<<"Failed, no such host adr3..."<<endl;
		bTerminate = true;
	}
	else
	{
		cout<<"OK"<<endl;
      memcpy((char *)&serv_addr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);//Address received from gethostbyname function
      /**
      * Connect to port of the ADR3/v850.
      * Note to Developer > Configure remote client and server by following commands, or else connect call will fail
      */
      cout<<"Trying to Connect to .. "<<string(server->h_name)<<":"<<portno<<"...";
   }

   serv_addr.sin_family = AF_INET;//Protol
   serv_addr.sin_port = htons(portno);//Portnumber to listen to

	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
	{
		cout<<" Failed"<<endl;
		bTerminate = true;
	}
	else
		cout<<" OK"<<endl;

	if(!bTerminate)
	{
		pthread_t tid;
		if ( -1 == pthread_create(&tid, NULL, vRxThread, (void*)dgram) )
		{
			cout<<"Failed to create a thread"<<endl;
		}
		else
		{
			while(!bTerminate)
			{
				int choice = 0;
				vPrintTestOptions();//Print test options
				cin >> choice;

				if (cin.fail())
				{
					cout << "ERROR -- You did not enter an integer";
					// get rid of failure state
					cin.clear();
					// discard 'bad' character(s)
					cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					continue;
				}

				if(choice == 0)
				{
					bTerminate = true;
				}
				else
				{
					vMainLoopIterate(choice, dgram);
				}
			}
		}
	}
   if (dgram_exit(dgram) < 0)
      cout<<"dgram_exit failed"<<endl;

   cout<<"Closing Socket for shutdown"<<endl;
	close(sockfd);

	return 0;
}

/**
* Entry function of the worker thread created to receive messages from INC device
*/
void* vRxThread(void* arg)
{
	if(arg == NULL)
		return NULL;
	cout<<endl<<"Rx Loop Started"<<endl;

	sk_dgram* pdgram = (static_cast<sk_dgram*>(arg));
	int pfd = pdgram->sk;
	int rc = -1;
	struct pollfd fds[1];
	fds[0].fd = pfd;
	fds[0].events = POLLIN;
	//Setting a timeout of 2 seconds
	int timeout = 200000;
	char buffer[500];
	memset(buffer,0,500);

	cout<<"Polling on fd "<<pfd<<endl;

	while(!bTerminate)
	{
		rc = poll(fds, 1, timeout);
		if(rc < 0)
		{
			cout<<"Poll Failed"<<endl;
			break;
		}
		if(rc == 0)
		{
			cout<<"Poll Timed out"<<endl;//No need to trace
			continue;
		}
		if(fds[0].revents & POLLIN)
		{
			rc = dgram_recv(pdgram, buffer, sizeof(buffer));
			if(rc > 0)
			{
				cout<<"<RECV> Bytes Received = "<<rc<<", Data : ";
				for(int i = 0; i< rc ; i++)
				{
					cout<<" "<< hex <<(int)buffer[i];//Printing bytes received
				}
			}
			else
			{
				cout<<"Error during receive"<<endl;
			}
			memset(buffer,0,500);//Clear data
		}
	}
	cout<<endl<<"Rx Loop exit"<<endl;

	return NULL;
}

void vMainLoopIterate(int choice, sk_dgram *dgram)
{
	switch(choice)
	{

	case 1:
		{

			char cmd_Fkt_Get[9] = {0x00,0x91, 0x01, 0x0f,0x02, 0x02, 0x00,0x01,0x21};
			//Sending command
			cout<<"<SEND> Bytes - ";
			for(int i = 0; i< 9 ; i++)
			{
				cout<< hex << uppercase << (int)cmd_Fkt_Get[i]<<",";//Printing bytes received
			}	
			cout<< dec << nouppercase;

			int wr = dgram_send(dgram,cmd_Fkt_Get,9);
			if(wr != 9)
			{
				cout<<"Error sending data, response  - "<<wr<<endl;
			}
			else
			{
				cout<<"Data sent successfully "<<endl;
			}
		}
		break;
	case 2:
		{
			cout<<"Change Source command "<<endl;//This is only for sound settings but not route
			uint8_t uiCmdAudioRoute[12]      =    {0x00,0x10, 0x00,0x91, 0x01, 0x0f,0x13, 0x02, 0x00,0x02, 0x01,0x01};
			//Get stream type
			cout<<"Enter stream type "<<endl;
			cout<<"1 Entertain1"<<endl;
			cout<<"2 Infotain1"<<endl;
			cout<<"3 Infotain2"<<endl;
			cout<<"4 Infotain3"<<endl;
			cout<<"5 Infotain4"<<endl;
			cout<<"6 Infotain5"<<endl;
			cout<<"7 Entertain2"<<endl;
			int a1;
			cin >> a1;
			uiCmdAudioRoute[10] = a1;
			//Get Source type
			cout<<"Enter Source type "<<endl;
			cout<<"0 NoSource"<<endl;
			cout<<"1 FM"<<endl;
			cout<<"2 AM"<<endl;
			cout<<"3 Media"<<endl;
			cout<<"4 CDA"<<endl;
			cout<<"5 AUX"<<endl;
			cout<<"6 DAB"<<endl;
			cout<<"7 Phone1"<<endl;
			cout<<"8 Phone2"<<endl;
			cout<<"9 AUX2"<<endl;
			cout<<"10 XM"<<endl;
			cout<<"14 FM_TA"<<endl;
			cout<<"15 DAB_TA"<<endl;
			cout<<"16 Speech"<<endl;
			cout<<"17 Cues"<<endl;
			cout<<"32 SoundGenerator1"<<endl;
			cout<<"33 SoundGenerator2"<<endl;
			cin >> a1;
			uiCmdAudioRoute[11] = a1;

			//Sending command
			cout<<"<SEND> Bytes - ";
			for(int i = 0; i< 12 ; i++)
			{
				cout<< hex << uppercase << (int)uiCmdAudioRoute[i]<<",";//Printing bytes received
			}	
			cout<< dec << nouppercase;

			int wr = dgram_send(dgram,uiCmdAudioRoute,12);
			if(wr != 12)
			{
				cout<<"Error sending data, response  - "<<wr<<endl;
			}
			else
			{
				cout<<"Data sent successfully "<<endl;
			}
		}
		break;
	case 3:
		{
			cout<<"Change Route command "<<endl;
			uint8_t uiCmdAudioRoute[12]      =    {0x00,0x10, 0x00,0x91, 0x01, 0x0f,0x15, 0x02, 0x00,0x02, 0x01,0x01};
			//Get stream type
			cout<<"Enter stream type "<<endl;
			cout<<"1 Entertain1"<<endl;
			cout<<"2 Infotain1"<<endl;
			cout<<"3 Infotain2"<<endl;
			cout<<"4 Infotain3"<<endl;
			cout<<"5 Infotain4"<<endl;
			cout<<"6 Infotain5"<<endl;
			cout<<"7 Entertain2"<<endl;
			int a1;
			cin >> a1;
			uiCmdAudioRoute[10] = a1;
			//Get Source type
			cout<<"Enter Source type "<<endl;
			cout<<"0 NoSource"<<endl;
			cout<<"1 TUN1"<<endl;
			cout<<"2 TUN2"<<endl;
			cout<<"3 DAB"<<endl;
			cout<<"4 Media"<<endl;
			cout<<"5 CDA"<<endl;
			cout<<"6 AUX"<<endl;
			cout<<"7 Phone1"<<endl;
			cout<<"8 Phone2"<<endl;
			cout<<"9 TP_Memo"<<endl;
			cout<<"10 XM"<<endl;
			cout<<"11 AUX2"<<endl;
			cout<<"16 Speech"<<endl;
			cout<<"17 Cues"<<endl;
			cout<<"32 SoundGenerator1"<<endl;
			cout<<"33 SoundGenerator2"<<endl;
			cin >> a1;
			uiCmdAudioRoute[11] = a1;


			//Sending command
			cout<<"<SEND> Bytes - ";
			for(int i = 0; i< 12 ; i++)
			{
				cout<< hex << uppercase << (int)uiCmdAudioRoute[i]<<",";//Printing bytes received
			}	
			cout<< dec << nouppercase;

			int wr = dgram_send(dgram,uiCmdAudioRoute,12);
			if(wr != 12)
			{
				cout<<"Error sending data, response  - "<<wr<<endl;
			}
			else
			{
				cout<<"Data sent successfully "<<endl;
			}
		}
		break;
	case 4:
		{
			cout<<"Volume command "<<endl;
			uint8_t uiCmdVolumeStep[18]         =    {0x00,0x10, 0x00,0x91, 0x01, 0x0f,0x20, 0x02, 0x00,0x08, 0x01,0x00,0x00,0x00, 0x00,0x0a, 0x01,0x90};

			int16_t volume = 0;
			int16_t volume_conv = 0;
			cout<<"Enter volume ... 0 - 40 : ";
			cin >> volume;

			volume_conv = (volume * 10) - 400; 
			cout<<"Converted volume = "<<volume_conv<<endl;

			uiCmdVolumeStep[12] = (uint8_t)((volume_conv>>8)& 0x00FF);
			uiCmdVolumeStep[13] = (uint8_t)(volume_conv& 0x00FF);

			cout<<"<SEND> Bytes - ";
			for(int i = 0; i< 18 ; i++)
			{
				cout<< hex << uppercase << (int)uiCmdVolumeStep[i]<<",";//Printing bytes received
			}	
			cout<< dec << nouppercase;


			int wr = dgram_send(dgram,uiCmdVolumeStep,18);
			if(wr != 18)
			{
				cout<<"Error sending data, response  - "<<wr<<endl;
			}
			else
			{
				cout<<"Data sent successfully "<<endl;
			}
		}
		break;
	case 5:
		{
			cout<<"Mute command for ENT stream"<<endl;
			 
			uint8_t uiCmdMute[16]    =    {0x00,0x10, 0x00,0x91, 0x01, 0x0f,0x60, 0x02, 0x00,0x06, 0x01,0x00 /*Mute action*/,0x00,0x64, 0x00,0x0a}; //De-Mute

			//Get Mute Action
			cout<<"Enter Mute Action 0 - De-mute, 1 - Mute "<<endl;
			int a1;
			cin >> a1;
			uiCmdMute[11] = a1;
			
			cout<<"<SEND> Bytes - ";
			for(int i = 0; i< 16 ; i++)
			{
				cout<< hex << uppercase << (int)uiCmdMute[i]<<",";//Printing bytes received
			}	
			cout<< dec << nouppercase;


			int wr = dgram_send(dgram,uiCmdMute,16);
			if(wr != 16)
			{
				cout<<"Error sending data, response  - "<<wr<<endl;
			}
			else
			{
				cout<<"Data sent successfully "<<endl;
			}
		}
		break;
		

		
	default: /* satisfy lint */
      break;

	}
}
void vPrintTestOptions(void)
{
	sleep(1);
	cout<<endl<<"INC Socket Handling Tester"<<endl;
	cout<<"1. Ping "<<endl;
	cout<<"2. Change Source "<<endl;
	cout<<"3. Change Route "<<endl;
	cout<<"4. Change Volume "<<endl;
	cout<<"5. Mute ENT stream"<<endl;
	cout<<"Enter choice (0 to exit) : "<<endl;
}
