/************************************************************************
  | FILE:         inc_stress_test.c
  | PROJECT:      platform
  | SW-COMPONENT: INC
  |----------------------------------------------------------------------------------------------------------------------------------
  | DESCRIPTION:  This Application opens one transmit thread and one receive thread for each of the LUN configured.
  |               It then starts sending the configured test messages by transmit thread and
  |               receives the responses by the receive thread.
  |                
  |               There is one display thread which keeps statistics on the console.
  |                
  |               The application accepts :
  |               1) the delay between two transmissions in milliseconds as the first argument,
  |               2) the run-time for the program in seconds as the second argument,
  |               3) the screen refreshing rate in seconds as the third argument and
  |               4) the number of required LUNs as the fourth argument.
  |                  This argument is optional, if not provided then by default all the LUNs would be considered.
  |
  |               Usage : inc_stress_test_out.out delay run_time refresh_rate [required_lun]
  |
  |                 e.g. inc_stress_test_out.out 11 60 5 [5]
  |
  |------------------------------------------------------------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2014 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification               | Author
  | 01.20.16  | Initial revision           | Prabhat Kumar 
  | --.--.--  | ----------------           | -------, -----
  |
  |***********************************************************************************************************************************/

/***************************************************************************************************
**                                   Includes                                                     **
***************************************************************************************************/
#include "inc_stress_test_v850.h"
#include "inc_stress_test_v850_cfg.h"
#include "inc_stress_test_v850_logging.h"

/***************************************************************************************************
**                                    MACROS                                                      **
***************************************************************************************************/

/***************************************************************************************************
**                                   Constants                                                    **
***************************************************************************************************/
const char *reportFileName = "inc_stress_test_report";

/***************************************************************************************************
**                                   Variables                                                    **
***************************************************************************************************/

/* network requirements */
inc_comm inccom_channel[MAX_LUN_NUM];   /*array for inccom channels to be acquired for each LUN*/

/* thread requirements */
pthread_t tid_tx[MAX_LUN_NUM];          /* array for transmit threads respective thread ids */
pthread_t tid_rx[MAX_LUN_NUM];          /* array for receive threads respective thread ids */
pthread_t disp_tid;                     /* display thread id */

tx_arg TxArg[MAX_LUN_NUM];              /* array for transmit threads args */
rx_arg RxArg[MAX_LUN_NUM];              /* array for receive threads args */

/* signal handler requirements */
struct sigaction act;                   /* signal handler structure */

/* timing requirements */
int ms_delay;                               /* delay between two transmissions */
int s_execTime;                             /* execution time for the application */
int s_refreshTime;                          /* screen refresh time */
int requiredLuns;                           /*number of required LUNs to be considered */
int timerExpiredFlag = FALSE;               /*flag to indicate the execution time has expired */
int dispThreadAliveFlag = FALSE;            /*flag to check the status of dispThread */
time_t iStartTime;                          /*start time of the test in seconds */
char startTime[TIME_STAMP_SIZE] = {0};      /*start time of the test in human understandable format */
char currentTime[TIME_STAMP_SIZE] = {0};    /*current time of the test in human understandable format */
char endTime[TIME_STAMP_SIZE] = {0};        /*end time of the test in human understandable format */
TimeStruct testRunTime;                     /*to store converted version of number of seconds into days,hours,minutes and seconds */

/*error logging requirements */
char ErrMsgBuff[ERROR_MESSAGE_SIZE] = {0};  /*buffer to store error message obtained from "perror" */

/***************************************************************************************************
**                                   FUNCTIONS                                                    **
***************************************************************************************************/

/***********************************************************************************************************************
 * int main(int argc, char *argv[]) description
 *
 * \main thread
 *
 * \param : int argc, char *argv[]
 *
 * \return : int
***********************************************************************************************************************/                                   
int main(int argc, char *argv[])
{
    int err;
    if(argc < 4)
    {
        fprintf(stderr, "Fatal Error : Insufficient number of arguments.\n");
        fprintf(stderr, "Please enter the delay between two transmissions in milliseconds as the first argument,\n");
        fprintf(stderr, "the run-time for the program in seconds as the second argument,\n");
        fprintf(stderr, "the screen refreshing rate in seconds as the third argument and\n");
        fprintf(stderr, "the number of required LUNs as the fourth argument. This argument is optional, if not provided then by default all the LUNs would be considered.\n\n");
        fprintf(stderr, "Usage : inc_stress_test_out.out delay run_time refresh_rate [required_lun]\n");
        fprintf(stderr, "e.g. inc_stress_test_out.out 11 60 5 [5]\n\n");
        return -1;
    }
     
    /* getting the command line arguments */
    ms_delay = atol(argv[1]);
    s_execTime = atol(argv[2]);
    s_refreshTime = atol(argv[3]);
    if(argc < 5)
    {/* fifth argument is not provided */
        requiredLuns = MAX_LUN_NUM;
    }
    else
    {
        requiredLuns = atol(argv[4]);
    }
    
    /* checking the validity of provided arguments */
    if((ms_delay <= 0) || (s_execTime <= 0) || (s_refreshTime <= 0) || (requiredLuns <= 0))
    {
        fprintf(stderr, "All the arguments should have a value greater than zero.\n");
        return -1;
    }
    
    if(requiredLuns > MAX_LUN_NUM)
    {
        fprintf(stderr,"required_lun(fourth argument) can not exceed the number of LUNs configured which is %d.\n\n", MAX_LUN_NUM);
        return -1;
    }
    
    /* setting the error logging environment */
    err = ClearLog();        /* clear the logfile or create a new one */
    if(err == -1)
    {
        fprintf(stderr, "Terminating because of logfile creation failure...\n\n");
        return -1;
    }
    
    err = OpenLogFile();     /* open the logfile for error logging */
    if(err == -1)
    {
        fprintf(stderr, "Terminating because of logfile opening failure...\n\n");
        return -1;
    }
   
    /* setting the signal handler */
    act.sa_handler = sigalrmHandle;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    err = sigaction(SIGALRM, &act, NULL);
    if(err < 0)
    {
        fprintf(stderr, "setting up of the signal handler failed.\n");
        perror("main");
        GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
        LOG_MESSAGE("ERROR : setting up of the signal handler failed.\nPossible Cause : %s\n", ErrMsgBuff);
        fprintf(stderr, "\nTerminating...\n");
        return -1;
    }
    
    /* beginning the operations */
    int iIndex;
    int duplicateIndex = -1;
    
    /* acquire inccom_channel for each LUN and preparing the data structure for each LUN and corresponding thread */
    for(iIndex = 0; iIndex < requiredLuns; iIndex++)
    {      
        /* check if a port is already assigned to this Lun */
        duplicateIndex = checkDuplicacyOfLun(luns[iIndex].lunID, iIndex);
        if(duplicateIndex == -1)
        {/* this lun has not been assigned an inccom_channel yet...assign one */
            if(0 != inc_communication_sock_init(&inccom_channel[iIndex], luns[iIndex].lunID))
            {
                fprintf(stderr, "inc_communication_sock_init failed for the port : %x, at iIndex : %d.\n", luns[iIndex].lunID, iIndex);
                GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
                LOG_MESSAGE("ERROR : inc_communication_sock_init failed for the port : %x, at iIndex : %d.\nPossible Cause : %s\n", luns[iIndex].lunID, iIndex, ErrMsgBuff);
                LOG_MESSAGE("ERROR : Because of the channel assignment failure, this LUN(%x) at the iIndex %d, would not be considered for the test.\nThe following commands can be helpful in removing this error : \n'modprobe -r -v boardcfg'\n'modprobe -r -v <module_name>'\n", luns[iIndex].lunID, iIndex);
                LOG_MESSAGE("INFO : If the Even the above commands fail to rewmove the binding error. Use 'netstat' to know the processes using this particular port or contact the Component owner.");
            }
            else
            {/* channel assigned successfully */
                TxArg[iIndex].channelAssigned = TRUE;
            }
        }
        else if(duplicateIndex >= iIndex)
        {/* possible error in checkDuplicacyOfLun()...as this can't be possible */
            fprintf(stderr, "checkDuplicacyOfLun() has returned a false result(%d) for the port : %x, ignoring this LUN.",duplicateIndex, luns[iIndex].lunID);
            LOG_MESSAGE("ERROR : Returned a false result(%d) for the port : %x, ignoring this LUN.\n", duplicateIndex, luns[iIndex].lunID);
        }
        else
        {/* duplicate candidate found at 'duplicateIndex' */
            fprintf(stdout, "An inccom_channel is already assigned to this LUN(%d)which is present at iIndex : %d\n", luns[iIndex].lunID, duplicateIndex);
            LOG_MESSAGE("ERROR : An inccom_channel is already assigned to this LUN(%d) which is present at iIndex : %d.\nNo channel can be acquired for this iIndex(%d).\n", luns[iIndex].lunID, duplicateIndex, iIndex);
        }
        
        /* prepare the 'TxArg' for the transmit thread */
        TxArg[iIndex].lun            = (const lunData *)&luns[iIndex];               /* assigning the port number */
        TxArg[iIndex].inccom_channel = &inccom_channel[iIndex];                             /* assigning the channel*/
        TxArg[iIndex].sentMsgCnt     = 0;
        TxArg[iIndex].readyForComm   = FALSE;
        TxArg[iIndex].alive      = FALSE;
        
        // prepare the 'RxArg' for the receive thread
        RxArg[iIndex].lun            = (const lunData *)&luns[iIndex];              /*assigning the port number*/
        RxArg[iIndex].inccom_channel = &inccom_channel[iIndex];                            /*assigning the channel*/
        RxArg[iIndex].rcvdMsgCnt     = 0;
        RxArg[iIndex].readyForComm   = FALSE;
        RxArg[iIndex].improperResponse = 0;
        RxArg[iIndex].alive      = FALSE;
    }
    
    /* set the start time of the test */
    GetTimeStamp(startTime);
    iStartTime = time(0);
    
     /* spawn threads for each LUN */
    for(iIndex = 0; iIndex < requiredLuns; iIndex++)
    {
        if(TxArg[iIndex].channelAssigned == TRUE)
        {
            /* Create the transmit thread */
            err = pthread_create(&tid_tx[iIndex], NULL, vEventTxThread, (void*)&TxArg[iIndex]);
            if (err != 0)
            {/* Tx thread creation failed */
                fprintf(stderr, "ERROR, TX THREAD CREATION FAILED for port : %x.\n", luns[iIndex].lunID);
                perror("main");
                GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
                LOG_MESSAGE("ERROR : TX THREAD CREATION FAILED for port : %x at iIndex : %d.\nPossible Cause : %s\n", luns[iIndex].lunID, iIndex, ErrMsgBuff);
                continue;       /* continue the loop, no need to create the corresponding recieve thread */
            }
            else
            {/* Tx thread created successfully */
                TxArg[iIndex].alive = TRUE;
                RxArg[iIndex].correspondingTxthreadIndex = iIndex;     /* Rx thread should know its corresponding Tx counterpart */
            }
            
            /* Create the receive thread */
            err = pthread_create(&tid_rx[iIndex], NULL, vEventRxThread,(void*)&RxArg[iIndex]);
            if (err != 0)
            {/* Rx thread creation failed */
                fprintf(stderr, "ERROR,Rx Thread creation Failed for port : %x\n\n", luns[iIndex].lunID);
                perror("main");
                GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
                LOG_MESSAGE("ERROR : ERROR,Rx Thread creation Failed for port : %x at iIndex : %d.\nPossible Cause : %s\n", luns[iIndex].lunID, iIndex, ErrMsgBuff);
            }
            else
            {/* Rx thread created successfully */
                RxArg[iIndex].alive = TRUE;
            }
        } /* if(TxArg[iIndex].channelAssigned == TRUE) */
    } /* end of for loop */
    
    /* creating the display thread */
    err = pthread_create(&disp_tid, NULL, vDisplayThread, NULL);
    if (err != 0)
    {/* dispThread creation failed */
        fprintf(stderr, "ERROR : DISPLAY THREAD CREATION FAILED\n\n");
        perror("main");
        GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
        LOG_MESSAGE("ERROR : DISPLAY THREAD CREATION FAILED.\nPossible Cause : %s\n", ErrMsgBuff);
    }
    else
    {/* dispThread created successfully */
        dispThreadAliveFlag = TRUE;
    }
    
    /* setting up the alarm */
    fprintf(stdout, "setting the execution time as %d seconds.\n", s_execTime);
    err = alarm(s_execTime);
    if(err != 0)
    {
        LOG_MESSAGE("INFO : There is an existing alarm to be delivered after '%d' seconds; it has been cancelled.\n\n", err);
    }
    LOG_MESSAGE("INFO : Execution time set successfully.\nShunting the main thread...\n");
    
    /* joining all the threads to main thread */
    for(iIndex = 0; iIndex < requiredLuns; iIndex++)
    {
        if(TxArg[iIndex].channelAssigned == TRUE)
        {
            /* Tx thread */
            if(0 != pthread_join(tid_tx[iIndex], NULL))
            {
                GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
                LOG_MESSAGE("ERROR : TX THREAD JOINING FAILED for port : %x at iIndex : %d.\nPossible Cause : %s\n", luns[iIndex].lunID, iIndex, ErrMsgBuff);
            }
           
            /* Rx thread */
            if(0 != pthread_join(tid_rx[iIndex], NULL))
            {
                GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
                LOG_MESSAGE("ERROR : RX THREAD JOINING FAILED for port : %x at iIndex : %d.\nPossible Cause : %s\n", luns[iIndex].lunID, iIndex, ErrMsgBuff);
            }
        }
    }
    
    /* joinig disp thread to main thread */
    if(0 != pthread_join(disp_tid, NULL))
    {
        GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
        LOG_MESSAGE("ERROR : DISP THREAD JOINING FAILED : %s.\n", ErrMsgBuff);
    }
   
    pthread_exit(0);
}

/***********************************************************************************************************************
 * void* vEventTxThread(void* arg) description
 *
 * \Transmit thread
 *
 * \param : void* arg
 *
 * \return : void*
***********************************************************************************************************************/ 
void* vEventTxThread(void* arg)
{   
    int ret = 0;
    if (arg == NULL)
    {/* can't proceed as the thread should know the corresponding LUN data */
        fprintf(stderr, "ERROR,vEventTxThread is called with NULL argument. Terminating the thread\n\n");
        LOG_MESSAGE("ERROR : one of the Tx threads is called with NULL argument. Terminating the thread...\nTis could create potential problem as the thread is dead but the alive flag could not be made FALSE...\n");
        return NULL;
    }
    
    /* setting the cancellation info */
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    
    /* get the argument */
    tx_arg *parg = (tx_arg*)arg;
    
    /* send the component status for the LUN */   
    ret = dgram_send((sk_dgram*)(parg->inccom_channel)->dgram, (char *)(parg->lun)->C_compStatBuff,(parg->lun)->C_compStatSize);
    if(ret > 0)
    {/* successful */
        LOG_MESSAGE("INFO : component status sent for the port : %x, %x.\n",(parg->lun)->lunID, (parg->lun)->C_compStatBuff[0]);
        parg->readyForComm = TRUE;
    }
    else
    {/* failure */
        GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
        LOG_MESSAGE("ERROR : component status could not be sent for the port : %x.\nPossible Cause : %s, for the returned value : %d.\n",(parg->lun)->lunID, ErrMsgBuff, ret);
        parg->readyForComm = FALSE;
        parg->alive = FALSE;        /* killing the thread as the component is not active */
        return NULL;
    }
    
    while(timerExpiredFlag != TRUE)
    {/* run until the timer expires */
        /* send the test message */
        ret = dgram_send((sk_dgram*)(parg->inccom_channel)->dgram,(char *)(parg->lun)->C_testMsgBuff,(parg->lun)->C_testMsgSize);
        if(ret > 0)
        {
            parg->sentMsgCnt++;
            ret = -1;
        }
        else
        {
            GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
            LOG_MESSAGE("ERROR : send failure for the port : %x while sending C_testMsgBuff.\nPossible Cause : %s, for the returned value : %d.\n", (parg->lun)->lunID, ErrMsgBuff, ret);
        }
        usleep(ms_delay*1000);
    }  
    
    parg->alive = FALSE;
    return NULL;
}

/***********************************************************************************************************************
 * void* vEventRxThread(void* arg) description
 *
 * \Receive thread
 *
 * \param : void* arg
 *
 * \return : void*
***********************************************************************************************************************/ 
void* vEventRxThread(void* arg)
{
    char buffer[SIZE] = {0};
    //int i = 0;
    int ret = 0;
    if(arg == NULL)
    {/* can't proceed as the thread should know the corresponding LUN data */
        fprintf(stderr, "ERROR,vEventRxThread is called with NULL argument. Terminating the thread\n\n");
        LOG_MESSAGE("ERROR : one of the Rx threads is called with NULL argument. Terminating the thread...\nTis could create potential problem as the thread is dead but the alive flag could not be made FALSE...\n");
        return NULL;
    }
    
    /* setting the cancellation info */
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    
    /* get the argument */
    rx_arg *parg = (rx_arg*)arg;
    
    /* receive the component status */
    ret = dgram_recv((sk_dgram*)(parg->inccom_channel)->dgram, buffer, sizeof(buffer));
    if(ret > 0)
    {/* success */
        if (0 != memcmp(buffer, (parg->lun)->R_compStatBuff, (parg->lun)->R_compStatSize))
        {/* ACTIVE status not received */
            LOG_MESSAGE("ERROR : LUN : %x has not returned ACTIVE. Buffer[0] = %x. Terminating the Tx and Rx threads for this LUN...\n", (parg->lun)->lunID, buffer[0]);
            if(TxArg[parg->correspondingTxthreadIndex].alive == TRUE)
            {
                pthread_cancel(tid_tx[parg->correspondingTxthreadIndex]);
                TxArg[parg->correspondingTxthreadIndex].alive = FALSE;
                LOG_MESSAGE("INFO : Txthread for the port : %x at iIndex : %d, cancelled successfully.\n", TxArg[parg->correspondingTxthreadIndex].lun->lunID, parg->correspondingTxthreadIndex);
            }
            else
            {
                LOG_MESSAGE("INFO : No alive Txthread found for the port : %x at iIndex : %d.\n", TxArg[parg->correspondingTxthreadIndex].lun->lunID, parg->correspondingTxthreadIndex);
            }
            
            /* kill the thread */
            parg->alive = FALSE;       /* declare itself dead */
            LOG_MESSAGE("INFO : Rxthread for the port : %x, cancelled successfully.\n", parg->lun->lunID);
            return NULL;
        }
        else
        {/* component ACTIVE */
            parg->readyForComm = TRUE;
            LOG_MESSAGE("INFO : LUN : %x ACTIVE. Commencing the tests for this LUN.\n", (parg->lun)->lunID);
        }
    }
    else if(ret == 0)
    {/* socket closed by the host */
        LOG_MESSAGE("ERROR : socket closed by the remote site for the port : %x while waiting on component status. Terminating the thread...\n", (parg->lun)->lunID);
        parg->readyForComm = FALSE;
        parg->alive = FALSE;
        return NULL; 
    }
    else
    {/* failure */  
        LOG_MESSAGE("ERROR : recv failure for the port : %x while waiting on component status. Terminating the thread...\n", (parg->lun)->lunID);
        parg->readyForComm = FALSE;
        parg->alive = FALSE;
        return NULL;
    }
    
    while(timerExpiredFlag != TRUE)
    {/* run until timer expires */
        memset(buffer, '\0', sizeof(buffer));
        
        /* receive the response for the test message sent by the corresponding TX thread */
        ret = dgram_recv((sk_dgram*)(parg->inccom_channel)->dgram, buffer, sizeof(buffer));
        if(ret > 0)
        {/* receive successful */
            /* validate whether the received message is valid or not */
            if(buffer[0] == (parg->lun)->R_testMsgId)
            {
                parg->rcvdMsgCnt++;
            }
            else
            {
                LOG_MESSAGE("ERROR : LUN : %x has not given a valid response. Response id = %x.\n", (parg->lun)->lunID, buffer[0]);
                parg->improperResponse++;
            }
            ret = -1;   /* my paranoia */
        }
        else
        {/* receive failure */
            GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
            LOG_MESSAGE("ERROR : recv failure for the port : %x while receiving R_testMsgBuff.\nPossible Cause : %s, for the returned value : %d.\n", (parg->lun)->lunID, ErrMsgBuff, ret);
        }
    }
    
    parg->alive = FALSE;
    return NULL;
}

/***********************************************************************************************************************
 * void* vDisplayThread(void* arg) description
 *
 * \Display thread
 *
 * \param : void* arg
 *
 * \return : void*
***********************************************************************************************************************/ 
void* vDisplayThread(void* arg)
{   
    /*lint prio 2 */
    arg = NULL;
    
    /* setting the cancellation info */
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    
    while(timerExpiredFlag != TRUE)
    {/* run until timer expires  */      
        system("clear");
        fprintf(stdout, "Start Time   : %s\n", startTime);
        fprintf(stdout, "Current Time : %s\n", GetTimeStamp(currentTime)); 
        memset(&testRunTime, 0, sizeof(testRunTime));
        convert(time(0) - iStartTime, &testRunTime);
        fprintf(stdout, "Test Running for : %2d:%2d:%2d:%2d\n", testRunTime.days, testRunTime.hours, testRunTime.minutes, testRunTime.seconds);
        displayLunTable(stdout);
        sleep(s_refreshTime);
    }
    
    dispThreadAliveFlag = FALSE;
    return NULL;
}

/***********************************************************************************************************************
 *void sigalrmHandle(int signum) description
 *
 * \Signal handler for SIGALRM
 *
 * \param : int signum : signal number
 *
 * \return : void
***********************************************************************************************************************/ 
void sigalrmHandle(int signum)
{
    /*lint prio 2*/
    (void)signum;
    
    fprintf(stdout, "timer expired...cancelling all the threads...\n");
    LOG_MESSAGE("INFO : timer expired...cancelling all the threads...\n");
    
    timerExpiredFlag = TRUE;    /* informing the threads to complete the current cycle and exit */
    GetTimeStamp(endTime);      /* setting up the end time */    
    
    /* Give some time to the threads to exit after the timer has expired */
    sleep(2);   
    
    /* cancel the threads which did not exit by themselves */
    int iIndex = 0, err = 0;
    for(iIndex = 0; iIndex < requiredLuns; iIndex++)
    {
        if(TxArg[iIndex].channelAssigned == TRUE)  /* filtering 'iIndex' */
        {
            if(TxArg[iIndex].alive == TRUE)   /* killing only running threads to avoid 'thread not existing error' */
            {
                err = pthread_cancel(tid_tx[iIndex]);
                if(err < 0)
                {
                    GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
                    fprintf(stderr, "Txthread for the port : %x at iIndex : %d,can't be cancelled successfully.\n", TxArg[iIndex].lun->lunID, iIndex);
                    perror("sigalrmHandle");
                    LOG_MESSAGE("ERROR : Txthread for the port : %x at iIndex : %d,can't be cancelled successfully.\nPossible Cuase : %s\n", TxArg[iIndex].lun->lunID, iIndex, ErrMsgBuff);
                }
                LOG_MESSAGE("INFO : Txthread for the port : %x at iIndex : %d, cancelled successfully.\n", TxArg[iIndex].lun->lunID, iIndex);
            }  /* if(TxArg[iIndex].alive == TRUE) */
            if(RxArg[iIndex].alive == TRUE)    /* killing only running threads to avoid thread not existing error */
            {
                err = pthread_cancel(tid_rx[iIndex]);
                if(err < 0)
                {
                    GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
                    fprintf(stderr, "Rxthread for the port : %x at iIndex : %d,can't be cancelled successfully.\n", RxArg[iIndex].lun->lunID, iIndex);
                    perror("sigalrmHandle");
                    LOG_MESSAGE("ERROR : Rxthread for the port : %x at iIndex : %d,can't be cancelled successfully.\nPossible Cuase : %s\n", RxArg[iIndex].lun->lunID, iIndex, ErrMsgBuff);    
                }
                LOG_MESSAGE("INFO : Rxthread for the port : %x at iIndex : %d, cancelled successfully.\n", RxArg[iIndex].lun->lunID, iIndex);
            } /* if(RxArg[iIndex].alive == TRUE) */
        } /* if(TxArg[iIndex].channelAssigned == TRUE) */
        
    } /* end of for loop */
    
    /* cancelling disp thread if it has not exited by itself */
    if(dispThreadAliveFlag == TRUE)    /* killing only when dispThread is running to avoid thread not existing error */
    {
        err = pthread_cancel(disp_tid);
        if(err < 0)
        {
            GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
            fprintf(stderr, "Display thread can't be cancelled successfully.\n");
            perror("sigalrmHandle");
            LOG_MESSAGE("ERROR : Display thread can't be cancelled successfully.\nPossible Cuase : %s\n", ErrMsgBuff);
        }
    }
    
    /* releasing the inc_com channel for all the luns */
    fprintf(stdout, "Cleaning up the network resources used...\n");
    LOG_MESSAGE("INFo : Cleaning up the network resources used...\n");
    for(iIndex = 0; iIndex < requiredLuns; iIndex++)
    {    
        if(TxArg[iIndex].channelAssigned == TRUE)          /* filter 'iIndex' */
        {
            if (inc_communication_sock_exit(&inccom_channel[iIndex]) != 0)
            {
                GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
                fprintf(stderr, "dgram_exit failed for port : %x\n\n",luns[iIndex].lunID);
                perror("sigalrmHandle");
                LOG_MESSAGE("ERROR : dgram_exit failed for port : %x at iIndex : %d.\nPossible Cause : %s\n", luns[iIndex].lunID, iIndex,  ErrMsgBuff);
            }
        }
    }
    
    LOG_MESSAGE("INFo : Generating the Summary...\n");
    
    /* display the last snapshot of the LUN table */
    system("clear");
    
    fprintf(stdout, "Generating the Summary...\n\n");
    fprintf(stdout, "\n*******************************************************************************************************************************************************************************\n");
    fprintf(stdout, "**************************************************************** Test Report Summary for INC STRESS TEST ***********************************************************************\n");
    fprintf(stdout, "********************************************************************************************************************************************************************************\n\n");
    
    fprintf(stdout, "TEST START TIME   : %s\n", startTime);
    fprintf(stdout, "TEST END TIME     : %s\n", endTime);
    memset(&testRunTime, 0, sizeof(testRunTime));
    convert(s_execTime, &testRunTime);
    
    fprintf(stdout, "TEST RUNNING TIME : %2d:%2d:%2d:%2d (dd:hh:mm:ss)\n\n", testRunTime.days, testRunTime.hours, testRunTime.minutes, testRunTime.seconds);
    fprintf(stdout, "DELAY BETWEEN TWO TRANSMISSIONS : %dms\n\n", ms_delay);
    
    /* display the ignored LUNs report */
    displayIgnoredLuns(stdout);
    
    /* display Lun Table */
    displayLunTable(stdout);
    
    fprintf(stdout, "\n********************************************************************************************************************************************************************************\n");
    fprintf(stdout, "*************************************************************************** END OF REPORT ***************************************************************************************\n");
    fprintf(stdout, "*********************************************************************************************************************************************************************************\n\n");
    fflush(stdout);
    
    /* writing the report in the report file */
    err = summariseTestReport();
    if(err != 0)
    {
        fprintf(stderr, "\nSummary could not be exported...\n\n");
        LOG_MESSAGE("ERROR : Summary could not be exported...\n");
    }
    else
    {
        fprintf(stdout, "Summary successfully exported to '%s/%s'.\n\n", getcwd(NULL, 100), reportFileName);
        LOG_MESSAGE("INFO : Summary successfully exported to '%s/%s'.\n", getcwd(NULL, 100), reportFileName);
    }
    
    CloseLogFile(); /* closing the logfile */
    fprintf(stdout, "Exiting the process...\n");
    exit(0);
}

/***********************************************************************************************************************
 *int summariseTestReport() description
 *
 * \Summarise the test report and write into the report file
 *
 * \param : NA
 *
 * \return : 0 : success, -1 : failure
***********************************************************************************************************************/ 
int summariseTestReport()
{
    FILE *fp;
    //int iIndex = 0;
    
    fprintf(stdout, "\nExporting the summary to a report file...\n\n");
    
    /* open the report file */
    fp = fopen(reportFileName, "w");
    if(fp == NULL)
    {
        GetErrorMessage(errno, ErrMsgBuff, sizeof(ErrMsgBuff));
        fprintf(stderr, "'%s' could not be opened/created.\n", reportFileName);
        perror("summariseTestReport");
        LOG_MESSAGE("INFO : '%s' could not be opened/created.\nPossible Cause : %s\n", reportFileName, ErrMsgBuff);
        return -1;
    }
     
    /* write the test report */
    fprintf(fp, "\n*******************************************************************************************************************************************************************************\n");
    fprintf(fp, "**************************************************************** Test Report Summary for INC STRESS TEST ***********************************************************************\n");
    fprintf(fp, "********************************************************************************************************************************************************************************\n\n");
    
    fprintf(fp, "TEST START TIME   : %s\n", startTime);
    fprintf(fp, "TEST END TIME     : %s\n", endTime);
    memset(&testRunTime, 0, sizeof(testRunTime));
    convert(s_execTime, &testRunTime);
    fprintf(fp, "TEST RUNNING TIME : %2d:%2d:%2d:%2d (dd:hh:mm:ss)\n\n", testRunTime.days, testRunTime.hours, testRunTime.minutes, testRunTime.seconds);
    fprintf(fp, "DELAY BETWEEN TWO TRANSMISSIONS : %dms\n\n", ms_delay);
    
    /* write the ignored LUNs report */
    displayIgnoredLuns(fp);
    
    /* display the LUN table */
    displayLunTable(fp);
    
    fprintf(fp, "\n********************************************************************************************************************************************************************************\n");
    fprintf(fp, "*************************************************************************** END OF REPORT ***************************************************************************************\n");
    fprintf(fp, "*********************************************************************************************************************************************************************************\n\n");
    fflush(fp);
    
    close(fileno(fp));
    return 0;
}

/***********************************************************************************************************************
 * void displayIgnoredLuns(FILE *fHandle) description
 *
 * \displays the LUNs not considered for the test
 *
 * \param : FILE *fHandle
 *
 * \return : NA
***********************************************************************************************************************/ 
void displayIgnoredLuns(FILE *fHandle)
{
    int iIndex = 0;
    int flag = FALSE;
    
    if(fHandle == NULL)
    {
        fprintf(stderr, "ERROR : NULL PTR passed to displayIgnoredLuns\n\n");
        LOG_MESSAGE("ERROR : NULL PTR passed.\n");
        return;
    }
    
    for(iIndex = 0; iIndex < requiredLuns; iIndex++)
    {    
        if(TxArg[iIndex].channelAssigned != TRUE)
        {
            flag = TRUE;
            if(iIndex == 0)
            {
                fprintf(fHandle, "\nNOTE : FOLLOWING LUNS WERE NOT CONSIDERED FOR THE TEST.\n\n");
            }
            fprintf(fHandle, "PORT : %d, NAME : %s\n\n", luns[iIndex].lunID, luns[iIndex].lunName);
        }
    }
    if(flag == TRUE)
    {
        fprintf(fHandle, "REASON : CHANNEL ACQUIRING FAILED.\n");
        fprintf(fHandle, "BELOW COMMANDS CAN BE HELPFUL FOR POSSIBLE SOLUTIONS.\n");
        fprintf(fHandle,"'modprobe -r -v boardcfg'\n'modprobe -r -v <module_name>'\n");
        fprintf(fHandle, "IF THE PROBLEM STILL PERSISTS, USE 'NETSTAT' TO GET IDEA ABOUT THE PROCESS USING THIS PORT.\n\n\n");  
    }
}

/***********************************************************************************************************************
 * void displayLunTable(FILE *fHandle) description
 *
 * \Monitores the current status of all the LUNs and flushes into the file pointed by the fHandle
 *
 * \param : FILE *fHandle
 *
 * \return : NA
***********************************************************************************************************************/ 
void displayLunTable(FILE *fHandle)
{
    int iIndex = 0;
    unsigned long long int totalTxMsgCnt = 0;
    unsigned long long int totalRxMsgCnt = 0;
    unsigned long long int totalImproperResponses = 0;
    unsigned long long int totalUntracked = 0;
    
    if(fHandle == NULL)
    {
        fprintf(stderr, "ERROR : NULL PTR passed to displayLunTable\n\n");
        LOG_MESSAGE("ERROR : NULL PTR passed.\n");
        return;
    }
    
    fprintf(fHandle, "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
    fprintf(fHandle, "|    LUN_NAME     | PORT | TYPE |         MESSAGE NAME         |          SENT          |          RECEIVED         |     IMPROPER RESPONSES     |          UNTRACKED          |\n");
    fprintf(fHandle, "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
    for(iIndex = 0; iIndex < requiredLuns; iIndex++)
    {
        if((TxArg[iIndex].readyForComm == TRUE) && (RxArg[iIndex].readyForComm == TRUE))
        {
            fprintf(fHandle, "|%-15s| %4x | %-6s |%-30s|  %20llu  |    %20llu   |    %20llu    |    %20llu    |\n", luns[iIndex].lunName, luns[iIndex].lunID, luns[iIndex].messageType, luns[iIndex].testMsgName, TxArg[iIndex].sentMsgCnt, RxArg[iIndex].rcvdMsgCnt, RxArg[iIndex].improperResponse, TxArg[iIndex].sentMsgCnt-RxArg[iIndex].rcvdMsgCnt);
            fprintf(fHandle, "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
            totalTxMsgCnt += TxArg[iIndex].sentMsgCnt;
            totalRxMsgCnt += RxArg[iIndex].rcvdMsgCnt;
            totalImproperResponses += RxArg[iIndex].improperResponse;
            totalUntracked += (TxArg[iIndex].sentMsgCnt-RxArg[iIndex].rcvdMsgCnt);
        }
    }
    fprintf(fHandle, "|%-62s|  %20llu  |    %20llu   |    %20llu    |    %20llu    |\n", "TOTAL", totalTxMsgCnt, totalRxMsgCnt, totalImproperResponses, totalUntracked);
    fprintf(fHandle, "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");    
}

/***********************************************************************************************************************
 * int checkDuplicacyOfLun(int port, int limit) description
 *
 * \checks whether the 'port' is present in the Lun_data table till the 'limit'  
 *
 * \param : int port, int limit
 *
 * \return : int : iIndex at which the duplicate entry for 'port' is found, -1 : if no duplicate entry found for 'port'
***********************************************************************************************************************/ 
int checkDuplicacyOfLun(int port, int limit)
{
    unsigned int iIndex = 0;
    if(limit <= 0)
    {
        return -1;
    }
    
    for(iIndex = 0; iIndex < (unsigned)limit; iIndex++)
    {
        if(port == luns[iIndex].lunID)
        {/* duplicate found */
            return iIndex;
        }
    }
    
    /* no duplicacy found */
    return -1;
}

/***********************************************************************************************************************
 * static int inc_communication_sock_init(inc_comm *inccom, int portno)) description
 *
 * \acquires the inccomchannel for the provided 'portno'  
 *
 * \param : inc_comm *inccom, int portno
 *
 * \return : 0 : success, -1 : failure
***********************************************************************************************************************/
static int inc_communication_sock_init(inc_comm *inccom, int portno)
{
    int ret,optval;
    int errsv,sockfd;
    int localportno;
    struct hostent *local, *remote;
    struct sockaddr_in local_addr, remote_addr;
    char local_name[256];
    char * host = "scc";
    sk_dgram *dgram;
   
    EXPCT_NE(inccom, NULL, -1, "Invalid argument, inccom\n");
    
    localportno = portno;

    sprintf(local_name, "%s-local", host);
    local = gethostbyname(local_name);
    errsv = errno;
    EXPCT_NE(local, NULL, 1, "gethostbyname() failed: %d\n", errsv);

    local_addr.sin_family = AF_INET;
    memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);
    local_addr.sin_port = htons(localportno);

    remote = gethostbyname(host);
    errsv = errno;
    EXPCT_NE(remote, NULL, 2, "gethostbyname (remote) failed: %d\n", errsv);

    remote_addr.sin_family = AF_INET;
    memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
    remote_addr.sin_port = htons(portno); 

    sockfd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
    errsv = errno;
    EXPCT_NE(sockfd, -1, 3, "create socket failed: %d\n", errsv);

    dgram = dgram_init(sockfd, DGRAM_MAX, NULL);
    EXPCT_NE(dgram, NULL, 4, "dgram_init failed\n");

    //lint -e64
    optval = 1; 
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));
    errsv = errno;
    EXPCT_NE(sockfd, -1, 5, "setsockopt failed: %d\n", errsv);
    
    ret = bind(sockfd, (struct sockaddr *) &local_addr, sizeof(local_addr));
    errsv = errno;
    EXPCT_EQ(ret, 0, 6, "bind failed: %d\n", errsv);

    ret = connect(sockfd, (struct sockaddr *) &remote_addr, sizeof(remote_addr));
    errsv = errno;
    EXPCT_EQ(ret, 0, 7, "connect failed: %d\n", errsv);

    inccom->dgram = dgram; 
    inccom->sockfd = sockfd;
    return 0;
}

/***********************************************************************************************************************
 * static int inc_communication_sock_exit(inc_comm *inccom) description
 *
 * \release the inccom channel and disconnect  
 *
 * \param : inc_comm *inccom
 *
 * \return : 0 : success, -1 : failure
***********************************************************************************************************************/
static int inc_communication_sock_exit(inc_comm *inccom)
{
    int ret, errsv;

    EXPCT_NE(inccom, NULL, -1, "Invalid argument, inccom\n");

    ret = dgram_exit(inccom->dgram);
    errsv = errno;
    EXPCT_EQ(ret, 0, 1, "dgram_exit failed: %d\n", errsv);

    close(inccom->sockfd);

    return 0;
}


/*
END OF FILE
*/