/************************************************************************
  | FILE:         inc_stress_test_g4.c
  | PROJECT:      platform
  | SW-COMPONENT: INC
  |------------------------------------------------------------------------------
  | DESCRIPTION:  Test application for INC stress test.
  |
  |               The application opens a socket on LUN 0 and send INC packets as 
  |               fast as possible in a loop.
  |               The app shall send dummy data with configurable size to the V850. 
  |               In a separate thread the application receives the responses.
  |
  |               Command line parameters: Hostname, packet size [bytes]
  |
  |               Execution:
  |               /opt/bosch/base/bin/inc_stress_test_g4_out.out hostname size
  |
  |               Eg. To send 50 bytes to SCC(V850)
  |               /opt/bosch/base/bin/inc_stress_test_g4_out.out scc 50
  |
  |------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2014 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification               | Author
  | 06.10.16  | Initial revision           | Shahida Mohammed Ashraf
  | --.--.--  | ----------------           | -------, -----
  |
  |*****************************************************************************/
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/time.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include "inc.h"
#include "dgram_service.h"
#include "inc_ports.h"
#include "inc_perf_common_g4.h"

#define SIZE (1024 * 64)

#define PR_ERR(fmt, args...) \
{ fprintf(stderr, "inc_stress_test_g4: %s: " fmt, __func__, ## args); \
    return -1; }

void* vRxThread(void* arg);

int main(int argc, char *argv[])
{
    int err;
    char buffer[SIZE+1];
    long i,size;
    void *ptr;
    pthread_t tid;
    inc_comm inccom;

    memset(buffer,0,sizeof (buffer));

    if (argc != 3) {
        PR_ERR("Application needs two params \"Hostname\" & \"packetsize\", \n Hostname -> scc for V850 \nUSAGE: %s Hostname packet_size\n",argv[0])
    }

    size = atol(argv[2]);
    if (size > SIZE)
    {
        PR_ERR("Please enter the size less than %d \n",SIZE)
    }

    /* Prepare the dummy data to be sent to SCC(v850) node*/
    for (i = 0; i< size; i++)
    {
	    buffer[i] = 's';
    }
    buffer[i] = '\0';
    
    /*Init Inc Communication*/
    err = inc_communication_init(argv[1],&inccom);
    if (err != 0)
        PR_ERR("inc_communication_init failed\n")

    ptr = (void*)(inccom.dgram);

    /* Create the reciever thread*/
    if ( -1 == pthread_create(&tid, NULL, vRxThread,ptr) )
    {
        if (inc_communication_exit(&inccom) != 0)
        {
            PR_ERR("dgram_exit failed\n")
		}
        PR_ERR("ERROR,Thread creation Failed\n\n")
    }
    else
    {
        while(1)
        {
            dgram_send(inccom.dgram,buffer,size);
        }
    }
	
    return 0;
}

/*
 * Entry function of the worker thread created to receive messages from INC device
 */
void* vRxThread(void* arg)
{
    char buffer[SIZE];

    if(arg == NULL)
        return NULL;

    sk_dgram* pdgram = (sk_dgram*)arg;
    memset(buffer,0,SIZE);

    while(1)
    {
        dgram_recv(pdgram, buffer, sizeof(buffer));
    }
}
