/************************************************************************
  | FILE:         inc_perf_common_g4.h
  | PROJECT:      platform
  | SW-COMPONENT: INC
  |------------------------------------------------------------------------------
  | DESCRIPTION:   INC performance measurement common header file 
  |                File contains the common function prototypes  and global data shared 
  |                between INC performance measurement commmand line apps & OEDT's 
  |------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2014 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification               | Author
  | 06.10.16  | Initial revision           | Shahida Mohammed Ashraf 
  | --.--.--  | ----------------           | -------, -----
  |
  |*****************************************************************************/
#ifndef INC_PERF_COMMON_G4_H
#define INC_PERF_COMMON_G4_H

#include "dgram_service.h"

typedef struct _inccomm{
    sk_dgram *dgram;
    int sockfd;
}inc_comm;

#define COMMAND "/proc/net/dev"

int throughput_handler(char *host,unsigned long interval,unsigned long interval_count);
void print_throughput(unsigned long interval,unsigned long count);
int inc_communication_init(char * host,inc_comm *inccom);
int inc_communication_exit(inc_comm *inccom);
int inc_threshold_evaluate(char *host);
#endif /* INC_PERF_COMMON_G4H */