/************************************************************************
| FILE:         inc_sim_thermalmgmt.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Test program for simulating vcc thermal management.
|
|               Setup for test with fake device:
|
|               modprobe dev-fake
|               /opt/bosch/base/bin/inc_sim_thermalmgmt_out.out
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2012 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 14.06.13  | Initial revision           | bkr6kor
||*****************************************************************************/

#define USE_DGRAM_SERVICE

#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <errno.h>
#include "inc.h"
#include "inc_ports.h"
//#include "inc_scc_port_extender_gpio.h"

#ifdef USE_DGRAM_SERVICE
#include "dgram_service.h"
#endif

//#define GPIO_INC_STATUS_ACTIVE   0x01
#define SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS_MSGID 0x20
#define SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS_MSGID 0x21
#define SCC_THERMAL_MANAGEMENT_R_REJECT_MSGID 0x0B
#define SCC_THERMAL_MANAGEMENT_C_GET_STATE_MSGID 0x30
#define SCC_THERMAL_MANAGEMENT_R_GET_STATE_MSGID 0x31
//#define SCC_THERMAL_MANAGEMENT_C_SET_STATE_MSGID 0x32
//#define SCC_THERMAL_MANAGEMENT_R_SET_STATE_MSGID 0x33
//#define SCC_THERMAL_MANAGEMENT_C_REQ_FAN_MSGID 0x34
//#define SCC_THERMAL_MANAGEMENT_R_REQ_FAN_MSGID 0x35

//int gpio_is_active = 0;

//void *tx_thread(void *arg)
//{
//   int n, cycle = 0;
//   char sendbuf[1024];
//
//#ifdef USE_DGRAM_SERVICE
//   sk_dgram *dgram = (sk_dgram *) arg;
//#else
//   int fd = (int) arg;
//#endif
//
//   sendbuf[0] = SCC_PORT_EXTENDER_GPIO_R_GET_STATE_MSGID;
//   sendbuf[1] = 13;
//   sendbuf[2] = 0xE2;
//   sendbuf[3] = 0x00;
//   sendbuf[4] = 0xE2;
//   sendbuf[5] = 0xFF;
//
//   do
//   {
//      usleep(1000000);
//
//      if (gpio_is_active) {
//
////         sendbuf[2] = sendbuf[2] >> 1;
////         sendbuf[4] = sendbuf[4] >> 1;
//         sendbuf[2] = sendbuf[2] ^ 0xFF;
//
//#ifdef USE_DGRAM_SERVICE
//         n = dgram_send(dgram, sendbuf, 6);
//#else
//         n = send(fd, sendbuf, 6, 0);
//#endif
//         if (n == -1)
//         {
//            fprintf(stderr, "INC_sendto failed: %d\n", n);
//            break;
//         }
//         fprintf(stderr, "<- R_GET_STATE: %d\n", sendbuf[1]);
//      }
//
//      cycle++;
//   } while (1);
//
//   return 0;
//}

int main(int argc, char *argv[])
{
   int m, n, fd, ret;
   char sendbuf[1024];
   char recvbuf[1024];
   pthread_t tid;
#ifdef USE_DGRAM_SERVICE
   sk_dgram *dgram;
#endif
   struct hostent *local, *remote;
   struct sockaddr_in local_addr, remote_addr;
   int clilen, new_sd;

   local = gethostbyname("fake1-local");
   if (local == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(fake1-local) failed: %d\n", errsv);
      return 1;
   }
   local_addr.sin_family = AF_INET;
   memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);
   local_addr.sin_port = htons(THERMAL_MANAGEMENT_PORT);/* from inc_ports.h */

   remote = gethostbyname("fake1");
   if (remote == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(fake1) failed: %d\n", errsv);
      return 1;
   }
   remote_addr.sin_family = AF_INET;
   memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
   remote_addr.sin_port = htons(THERMAL_MANAGEMENT_PORT);/* from inc_ports.h */

   fprintf(stderr, "local %s:%d\n", inet_ntoa(local_addr.sin_addr), ntohs(local_addr.sin_port));
   fprintf(stderr, "remote %s:%d\n", inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));

   fd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
   if (fd == -1)
   {
      int errsv = errno;
      fprintf(stderr, "create socket failed: %d\n", errsv);
      return 1;
   }

  fprintf(stderr, "socket fd: %d\n", fd);
#ifdef USE_DGRAM_SERVICE
   dgram = dgram_init(fd, DGRAM_MAX, NULL);
   if (dgram == NULL)
   {
      int errsrv = errno;
      fprintf(stderr, "dgram_init failed %d\n",errsrv);
      return 1;
   }
#endif

   ret = bind(fd, (struct sockaddr *) &local_addr, sizeof(local_addr));
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "bind failed: %d\n", errsv);
      return 1;
   }

   ret = connect(fd, (struct sockaddr *) &remote_addr, sizeof(remote_addr));
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "connect failed: %d\n", errsv);
      return 1;
   }

 
//#ifdef USE_DGRAM_SERVICE
//   if (pthread_create(&tid, NULL, tx_thread, (void *) dgram) == -1)
//      fprintf(stderr, "pthread_create failed\n");
//#else
//   if (pthread_create(&tid, NULL, tx_thread, (void *) fd) == -1)
//      fprintf(stderr, "pthread_create failed\n");
//#endif

   do {
      do {
#ifdef USE_DGRAM_SERVICE
         n = dgram_recv(dgram, recvbuf, sizeof(recvbuf));
#else
         n = recv(fd, recvbuf, sizeof(recvbuf), 0);
#endif
      } while(n < 0 && errno == EAGAIN);
      if (n < 0)
      {
         int errsv = errno;
         fprintf(stderr, "recv failed: %d\n", errsv);
         return 1;
      }

      fprintf(stderr, "recv: %d\n", n);

      if (n == 0)
      {
         fprintf(stderr, "invalid msg size: %d\n", n);
         return 1;
      }

      switch (recvbuf[0])
      {
        //Modified the case for handling SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS message
        case SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS_MSGID:
         if (n != 2) {
            fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
            return 1;
         }
         if (recvbuf[1] != 0x01) {
           fprintf(stderr, "invalid status: %d\n", recvbuf[1]);
           return 1;
        }
        fprintf(stderr, "-> C_COMPONENT_STATUS: %d\n", recvbuf[1]);

         sendbuf[0] = SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS_MSGID;
         sendbuf[1] = 0x01;
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 2);
#else
         m = send(fd, sendbuf, 2, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_COMPONENT_STATUS: %d\n", sendbuf[1]);
         break;

         
        case SCC_THERMAL_MANAGEMENT_C_GET_STATE_MSGID:
         if (n != 1) {
            fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
            return 1;
         }
         fprintf(stderr, "-> C_GET_STATE: %d\n", recvbuf[1]);

         sendbuf[0] = SCC_THERMAL_MANAGEMENT_R_GET_STATE_MSGID;
         sendbuf[1] = 0x01; //GOOD -1 BAD - 2 POOR - 3
#ifdef USE_DGRAM_SERVICE
         //m = dgram_send(dgram, sendbuf, 6);
         m = dgram_send(dgram, sendbuf, 2);
#else
         //m = send(fd, sendbuf, 6, 0);
         m = send(fd, sendbuf, 2, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            return 1;
         }
         fprintf(stderr, "<- R_GET_STATE: %d\n", sendbuf[1]);

         //gpio_is_active = 1;
         break;

//      case SCC_PORT_EXTENDER_GPIO_C_SET_STATE_MSGID:
//         if (n != 3) {
//            fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
//            return 1;
//         }
//         fprintf(stderr, "-> C_SET_STATE: %d %d\n", recvbuf[1], recvbuf[2]);
//
////         usleep(3000000);
//
//         sendbuf[0] = SCC_PORT_EXTENDER_GPIO_R_SET_STATE_MSGID;
//         sendbuf[1] = recvbuf[1];
//         sendbuf[2] = recvbuf[2];
//#ifdef USE_DGRAM_SERVICE
//         m = dgram_send(dgram, sendbuf, 3);
//#else
//         m = send(fd, sendbuf, 3, 0);
//#endif
//         if (m == -1)
//         {
//            int errsv = errno;
//            fprintf(stderr, "send failed: %d\n", errsv);
//            return 1;
//         }
//         fprintf(stderr, "<- R_SET_STATE: %d %d\n", sendbuf[1], sendbuf[2]);
//         break;

      default:
         fprintf(stderr, "invalid msgid: 0x%02X\n", recvbuf[0]);
         return 1;
      }

   } while (n > 0);

#ifdef USE_DGRAM_SERVICE
   ret = dgram_exit(dgram);
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "dgram_exit failed: %d\n", errsv);
      return 1;
   }
#endif

   close(fd);

   return 0;
}
