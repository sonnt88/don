/************************************************************************
| FILE:         inc_sim_adr3_scc.cpp
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Test program for ADR3_LINUX.
|               Explanation and usage :
|                 1) adr_scc simulator main thread process the request from adr3_driver
|                 2) Send the respective responses to the driver 
|                 3) Apart from main thread there is a Manual response thread
|                 4) The manual thread takes the manual choices and send the reponse to driver
|                 5) While manual thread is responding for the choices made the 
|                    main thread process is suspended till the counter choice is enterd
|                 6) Example: For low voltage active choice the main thread response is suspended
|                    till the low voltage recovery choice provided 
|                 7) Manual thread choice suspending main thread is to provide the better test scenarios
|               Execution:
|               modprobe dev-fake
|               /opt/bosch/base/bin/inc_sim_adr3_scc_out.out
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 27.09.13  | Initial revision           | SWM2KOR
| 14.09.13  | Lint removal               | SWM2KOR
|
|*****************************************************************************/
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <iostream>
#include <limits>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/time.h>
#include <netinet/tcp.h>
#include "inc.h"
#include "inc_ports.h"
#include "dgram_service.h"
#include "inc_scc_adr3ctrl.h"
using namespace std;
#define ADR3_SCC_COMM_BUFFR_LENGHT                 3  
sk_dgram *dgram;
/* Variable used to suspend the main thread response*/
bool bManualChoiceActive= false;
//Function to send commands based on user choice
void vADR3Simlator_Choiceprocessing(int choice);
//Thread to receive the user choice and process them
void* vADR3Simlator_ManualSocketRspnsThrd(void* arg);
//Helper choice printer
void vADR3Simlator_PrintTestOptions(void);
/* Socket setup function*/
bool bADR3Simlator_SCC_Comm_SocketSetup(int *psockfd)
{
   int fd,ret,errsv;
   bool bSocketSetup = false;
   struct hostent *local, *remote;
   struct sockaddr_in local_addr ={0}, remote_addr={0};
   /* Get the host details for fake1-local*/
   local = gethostbyname("fake1-local");
   if(local == NULL)
   {
      errsv = errno;
      fprintf(stderr, "gethostbyname(fake1-local) failed: %d\n", errsv);
   }
   else
   {
      local_addr.sin_family = AF_INET;
      memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr,(unsigned int)local->h_length);
      local_addr.sin_port = htons(ADR3CTRL_PORT); /* from inc_ports.h */
   }     
   /* Get the host details for fake1*/
   remote = gethostbyname("fake1");
   if(remote == NULL)
   {
      errsv = errno;
      fprintf(stderr, "gethostbyname(fake1) failed: %d\n", errsv);
   }
   else
   {
      remote_addr.sin_family = AF_INET;
      memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr,(unsigned int)remote->h_length);
      remote_addr.sin_port = htons(ADR3CTRL_PORT); /* from inc_ports.h */
   }
   /* Socket creation*/
   fd = socket(AF_BOSCH_INC_AUTOSAR,(int)SOCK_STREAM, 0);
   if(fd == -1)
   {
      errsv = errno;
      fprintf(stderr, "create socket failed: %d\n", errsv);
   }
    /* Dgram Intialization*/
   else if((dgram = dgram_init(fd, DGRAM_MAX, NULL)) == NULL)
   {
      fprintf(stderr, "dgram_init failed\n");
      close(fd);       
   }
      /** Binding to local address*/
   else if((ret = bind(fd,(struct sockaddr *)&local_addr,sizeof(local_addr))) < 0)
   {
      errsv = errno;
      fprintf(stderr, "bind failed: %d\n", errsv);
      dgram_exit(dgram);
      close(fd);
   }
   /* Connecting to the fake-1*/
   else if((ret = connect(fd,(struct sockaddr *)&remote_addr,sizeof(remote_addr))) < 0)
   {
      errsv = errno;
      fprintf(stderr, "connect failed: %d\n", errsv);
      dgram_exit(dgram);
      close(fd);
   }
   else
   {
      bSocketSetup = true;
      fprintf(stderr, " Socket set-up is Sucessful \n");
   }
   if(bSocketSetup == true)
   {
      *psockfd = fd;
   }
   else
   {
      *psockfd  = -1;
   }
   return bSocketSetup;
}
int main()
{
   bool bSCC_Scoket_initOkay ;
   int sockfd = -1;
   int n;
   /* Used to recieve the command form ADR3_LINUX*/
   char recvbuf[ADR3_SCC_COMM_BUFFR_LENGHT]={0};
   /* Used to send the command form ADR3_LINUX*/
   char sendbuf[ADR3_SCC_COMM_BUFFR_LENGHT]={0};
   cout<<".... ADR3_SCC Simulator Start ...."<<endl;
   bSCC_Scoket_initOkay = bADR3Simlator_SCC_Comm_SocketSetup(&sockfd);
   if(bSCC_Scoket_initOkay == true)
   {
      pthread_t tid;
      /* Create the thread for manual responses */
      if(0 != pthread_create(&tid, NULL, vADR3Simlator_ManualSocketRspnsThrd, (void*)NULL))
      {
         cout<<"Failed to create a Manual socket response thread"<<endl;
      }
      do {
         do {
               n = dgram_recv(dgram, recvbuf, sizeof(recvbuf));

            } while(n < 0 && errno == EAGAIN);
         fprintf(stderr, "recv: %d\n", n);
         if(n < 0)
         {
            fprintf(stderr, "recv failed: %d\n", errno);
         }
         else if(n == 0)
         {
            fprintf(stderr, "invalid msg size: %d\n", n);            
         } 
         else  if(n != 3) 
         {
            fprintf(stderr, "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
         }
         /* Process the recv buf only if there are no manual 
            responses which are pending. please see vADR3Simlator_ManualSocketRspnsThrd */
         else if(bManualChoiceActive == false)
         {
            switch (recvbuf[0])
            {
               case SCC_ADR3CTRL_C_COMPONENT_STATUS:
                  if((recvbuf[1] != ADR3_STATUS_ACTIVE) && (recvbuf[2] != ADR3_FIRST_VERSION)) 
                  {
                     fprintf(stderr, "invalid status: %d\n", recvbuf[1]);
                     sendbuf[0] = SCC_ADR3CTRL_R_REJECT ;
                     sendbuf[1] = 0Xff ;
                     sendbuf[2] = 0xff ;
                  }
                  else
                  {
                     sendbuf[0] = SCC_ADR3CTRL_R_COMPONENT_STATUS ;
                     sendbuf[1] = ADR3_STATUS_ACTIVE ;
                     sendbuf[2] = ADR3_FIRST_VERSION ;
                     sleep(2);
                     if(dgram_send(dgram,sendbuf,ADR3_SCC_COMM_BUFFR_LENGHT) != ADR3_SCC_COMM_BUFFR_LENGHT)
                     {
                       cout<<"send failed: " <<errno<<endl;
                     }
                     else
                     {
                        fprintf(stderr, 
                                "command form ADR3_SCC MsgID = %x ,status =%x, Mode =%x\n", 
                                sendbuf[0],
                                sendbuf[1],
                                sendbuf[2]
                                );
                     }
                     memset(sendbuf, 0, sizeof(sendbuf));
                     sendbuf[0] = SCC_ADR3CTRL_R_SET_RESET ;
                     sendbuf[1] = ADR3_RESET_STATUS_RELEASE ;
                     sendbuf[2] = ADR3_MODE_NORMAL ;
                  }
                  break;
               case SCC_ADR3CTRL_C_SET_RESET :
                  sendbuf[0] = SCC_ADR3CTRL_R_SET_RESET ;
                  /* Send the same response  
                     for reset active send active 
                     same for reset release ,norml , dwnl mode*/
                  sendbuf[1] = recvbuf[1] ; 
                  sendbuf[2] = recvbuf[2] ;
                  break;
               default:
                  sendbuf[0] = SCC_ADR3CTRL_R_REJECT   ;
                  sendbuf[1] = 0Xff ;
                  sendbuf[2] = 0xff ;
                  break;
            }
            sleep(2);
            /* Send the buffer to driver*/
            fprintf(stderr, 
                                "Recived form ADR3_Linux MsgID = %x ,status =%x, Mode =%x\n", 
                                recvbuf[0],
                                recvbuf[1],
                                recvbuf[2]
                                );
            if(dgram_send(dgram,sendbuf,ADR3_SCC_COMM_BUFFR_LENGHT) != ADR3_SCC_COMM_BUFFR_LENGHT)
            {
              cout<<"send failed: " <<errno<<endl;
            }
            else
            {
               fprintf(stderr, 
                       "command form ADR3_SCC MsgID = %x ,status =%x, Mode =%x\n", 
                       sendbuf[0],
                       sendbuf[1],
                       sendbuf[2]
                       );
            }
            memset(sendbuf, 0, sizeof(sendbuf));
         }
         memset(recvbuf, 0, sizeof(recvbuf));
      } while (n >= 0);
      if(dgram_exit(dgram) < 0)
      {
         int errsv = errno;
         fprintf(stderr, "dgram_exit failed: %d\n", errsv);
      }
      close(sockfd);
   }
    cout<<"Simulator exit" <<endl;
   return 0;
}

/**
   Manual response to ADR3_linux to produce different test scenario
   This thread Send the manual response to ADR3_linux to create Low voltage and 
   application reset request on V850.
*/
void* vADR3Simlator_ManualSocketRspnsThrd(void* arg)
{
   bool bTerminate = false;
   (void) arg;
   while(!bTerminate)
   {
      int choice = 0;
      vADR3Simlator_PrintTestOptions();//Print test options
      cin >> choice;
      if(cin.fail())
      {
         cout << "ERROR -- You did not enter an integer";
         // get rid of failure state
         cin.clear();
         // discard 'bad' character(s)
         cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
         continue;
      }
      if(choice == 0)
      {
         bTerminate = true;
      }
      else
      {
         vADR3Simlator_Choiceprocessing(choice);
      }
   }
   cout << "Manual Socket Reponse thread exit"<<endl;
   return NULL;
}
/**
   Send the response to ADR3 driver based on the choice
   */
void vADR3Simlator_Choiceprocessing(int choice)
{
   char command[ADR3_SCC_COMM_BUFFR_LENGHT]={0};
   bool bValidCommand = true;
   switch(choice)
   {  
      /* 1. Start low voltage */
      case 1:
      {
         command[0]= SCC_ADR3CTRL_R_SET_RESET;
         command[1]= ADR3_RESET_STATUS_ACTIVE;
         command[2]= ADR3_MODE_NORMAL;
      /** Block the main thread response to ADR3 linux until choice 2 is given 
          Just like a Low voltage occurnce in the system which response only 
          after Low voltage recovery*/
         bManualChoiceActive = true;
         fprintf(stderr, "Untill choice 2 is provided simulator will not respond to ADR3_linux commands\n");
      }
      break;
      /* "2. End low voltage " */
      case 2:
      {
         command[0]= SCC_ADR3CTRL_R_SET_RESET;
         command[1]= ADR3_RESET_STATUS_RELEASE;
         command[2]= ADR3_MODE_NORMAL;
         /* Start the main thread responding to ADR3_Linux*/
         bManualChoiceActive = false;
      }
      break;
      /* "3. reset active normal " */
      case 3:
      {
         command[0]= SCC_ADR3CTRL_R_SET_RESET;
         command[1]= ADR3_RESET_STATUS_ACTIVE;
         command[2]= ADR3_MODE_NORMAL;
         bManualChoiceActive = true;
      /** Block the main thread response to ADR3 linux until choice 4 is given 
          Just like a early and real audio app reset request */
          fprintf(stderr, "Untill choice 4 is provided simulator will not respond to ADR3_linux commands\n");
      }
      break;
      /* 4. reset release normal */
      case 4:
      {
         command[0]= SCC_ADR3CTRL_R_SET_RESET; 
         command[1]= ADR3_RESET_STATUS_RELEASE;
         command[2]= ADR3_MODE_NORMAL;
        /* Start the main thread responding to ADR3_Linux*/
         bManualChoiceActive = false;
      }
      break;
      /* 5. reset active  download */
      case 5:
      {
         command[0]= SCC_ADR3CTRL_R_SET_RESET;
         command[1]= ADR3_RESET_STATUS_ACTIVE;
         command[2]= ADR3_MODE_DOWNLOAD;
         bManualChoiceActive = true;
      /** Block the main thread response to ADR3 linux until choice 4 is given 
          Just like testing scenario*/
         fprintf(stderr, "Untill choice 6 is provided simulator will not respond to ADR3_linux commands\n");
      }
      break;
      /* 6. reset release  download*/
      case 6:
      {
         command[0]= SCC_ADR3CTRL_R_SET_RESET;
         command[1]= ADR3_RESET_STATUS_RELEASE;
         command[2]= ADR3_MODE_DOWNLOAD;
          /* Start the main thread responding to ADR3_Linux*/
         bManualChoiceActive = false;
      }
      break;
      /* 7. ADR3 active */
      case 7:
      {
         command[0]= SCC_ADR3CTRL_R_COMPONENT_STATUS;
         command[1]= ADR3_STATUS_ACTIVE;
         command[2]= ADR3_FIRST_VERSION;
      }
      break;
      /* 8. ADR3 Inactive  */
      case 8:
      {
         command[0]= SCC_ADR3CTRL_R_COMPONENT_STATUS;
         command[1]= ADR3_STATUS_INACTIVE;
         command[2]= ADR3_FIRST_VERSION;
      }
      break;
      default:
      {
          bValidCommand = false;
      }
      break;
   }
   if(bValidCommand == true)
   {
      if(dgram_send(dgram,command,ADR3_SCC_COMM_BUFFR_LENGHT) != ADR3_SCC_COMM_BUFFR_LENGHT)
      {
        cout<<"send failed: " <<errno<<endl;
      }
      else
      {
         fprintf(stderr, "command form ADR3_SCC MsgID = %x ,status =%x, Mode =%x\n", command[0],command[1],command[2]);
      }
   }
}
void vADR3Simlator_PrintTestOptions()
{
   sleep(2);
   cout<<endl<<" ADR3 V850 Responses "<<endl;
   cout<<"1. Start low voltage "<<endl;
   cout<<"2. End low voltage "<<endl;
   cout<<"3. Reset active normal "<<endl;
   cout<<"4. Reset release normal "<<endl;
   cout<<"5. Reset active  download "<<endl;
   cout<<"6. Reset release  download "<<endl;
   cout<<"7. ADR3 active "<<endl;
   cout<<"8. ADR3 Inactive "<<endl;
   cout<<"Enter choice (0 to exit) : "<<endl;
}
