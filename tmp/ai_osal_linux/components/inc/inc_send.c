/************************************************************************
| FILE:         inc_send.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Test program for sending arbitrary INC messages.
|
|               Example:
|               /opt/bosch/base/bin/inc_send_out.out -p 50944 -b 50944 -r scc 01-02-03
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 24.01.13  | Initial revision           | Andreas Pape
| 28.01.13  | Use datagram service       | tma2hi
| 06.02.13  | Use local hostname         | tma2hi
| 06.02.13  | Integrate Lothar's SPM ext | tma2hi
| 19.03.13  | Update description         | tma2hi
| 19.06.14  | Text error msgs, recv only | tma2hi
|
|*****************************************************************************/

#define USE_DGRAM_SERVICE

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/time.h>
#include <time.h>
#include <netinet/tcp.h>
#include "inc.h"

#ifdef USE_DGRAM_SERVICE
#include "dgram_service.h"
sk_dgram *dgram;
#endif

int sockfd;

#define PR_ERR(fmt, args...) \
      { fprintf(stderr, "inc_send: %s: " fmt, __func__, ## args); \
      return -1; }

static void hexdump(const char *intro, char *buffer, int len)
{
int i;
	fprintf(stdout,"%s:",intro);
	for (i=0;i<len;i++)
		fprintf(stdout," 0x%02x", buffer[i]);
	fprintf(stdout,"\n");
}

//input: payload as hex-ASCII string: e.g: a9-de-56-03
static int arg2payload(char *buffer, int buflen, char* string)
{
	char *pos, *end;
	long result;
	int len = 0;
	if(!string)
		return -1;
	pos = string;
	end = string;
	while(*end != '\0' && (len < buflen)) {
		result = strtol(pos, &end ,16);
		if(result > 255)
			return -1;
		if(result < 0)
			return -1;
		buffer[len++] = result;
		if(*end != '\0') {
			if(*end == '-')
				end++;
			else
				return -1;
		}
		pos = end;
	}
	return len;
}

#define DEFAULT_MSGSZ 1500

int incsend( char* string )
{
   int wr, len;
   char buffer[DEFAULT_MSGSZ+1];

   len = arg2payload(buffer,DEFAULT_MSGSZ, string );  // start message
   hexdump("SEND", buffer , len);
#ifdef USE_DGRAM_SERVICE
      wr = dgram_send(dgram, buffer, len);
#else
      wr = write(sockfd,buffer,len);
#endif
   if (wr < 0) 
      PR_ERR("ERROR writing to socket\n");
   return wr;
}

int main(int argc, char *argv[])
{
	int portno = 0, localportno = 0, wr,rd, receive = 0, spm = 0, spmseq = 0,
	      len = 0;
	struct sockaddr_in serv_addr, local_addr;
   struct hostent *server;
   struct hostent *local;
	char buffer[DEFAULT_MSGSZ+1];
	char local_name[256];
	int opt, optmsk;

	for (optmsk = opterr = 0; (opt = getopt(argc, argv, "rp:b:s:")) != -1;) {
		switch (opt) {
		case 'p':
			portno = atoi(optarg);
			break;
		case 'b':
			localportno = atoi(optarg);
			break;
		case 'r':
			receive = 1;/*received data after sending*/
			break;
		case 's':
			spm    = 1; /* spm test mode active */
			spmseq = atoi(optarg);
			portno      = 0xC701;  /* LUN 1 */
			localportno = 0xC701;  /* LUN 1 */
			break;
		default:
			PR_ERR("invalid option\n");
		}
	}
	if(portno == 0)
		PR_ERR("invalid port - use option -p\n");

	if(optind>=argc)
		PR_ERR("ERROR host missing\n");

   server = gethostbyname(argv[optind]);
   if (server == NULL)
      PR_ERR("ERROR, no such remote host\n");
   serv_addr.sin_family = AF_INET;
   memcpy((char *)&serv_addr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
   serv_addr.sin_port = htons(portno);

   sprintf(local_name, "%s-local", argv[optind]);
   local = gethostbyname(local_name);
   if (local == NULL)
      PR_ERR("ERROR, no such local host\n");
   local_addr.sin_family = AF_INET;
   memcpy((char *)&local_addr.sin_addr.s_addr, (char *)local->h_addr, local->h_length);
   local_addr.sin_port = htons(localportno);

   optind++;
   if( spm == 0 ) /* no payload, if sequences are send/received */
   {
      if(optind>=argc) {
         len = 0;
      } else {
         len = arg2payload(buffer,DEFAULT_MSGSZ,argv[optind]);
         if(len<0)
            PR_ERR("ERROR on parsing payload\n");
         optind++;
      }
   }

	if(optind<argc)
		PR_ERR("ERROR unexpected argument\n");

	sockfd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
	if (sockfd < 0) {
      int errsv = errno;
      PR_ERR("ERROR opening socket: %d %s\n", errsv, strerror(errsv));
   }

#ifdef USE_DGRAM_SERVICE
   dgram = dgram_init(sockfd, DGRAM_MAX, NULL);
   if (dgram == NULL)
      PR_ERR("dgram_init failed\n");
#endif
//lint -e64
	if (bind(sockfd, (struct sockaddr *) &local_addr, sizeof(local_addr)) < 0) {
	   int errsv = errno;
      PR_ERR("ERROR binding: %d %s\n", errsv, strerror(errsv));
	}

	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
      int errsv = errno;
      PR_ERR("ERROR connecting: %d %s\n", errsv, strerror(errsv));
   }

   if( spm == 0 ) /* simple send receive */
   {
      if (len > 0) {
         hexdump("SEND", buffer, len);
         /*send data*/
#ifdef USE_DGRAM_SERVICE
         wr = dgram_send(dgram, buffer, len);
#else
         wr = write(sockfd,buffer,len);
#endif
         if (wr < 0) {
              int errsv = errno;
              PR_ERR("ERROR writing to socket: %d %s\n", errsv, strerror(errsv));
           }
     }

      /*read*/
      if (receive) {
#ifdef USE_DGRAM_SERVICE
         rd = dgram_recv(dgram, buffer, DEFAULT_MSGSZ);
#else
         rd = read(sockfd, buffer, DEFAULT_MSGSZ);
#endif
         if (rd < 0) {
            int errsv = errno;
            PR_ERR("ERROR reading from socket: %d %s\n", errsv, strerror(errsv));
         }
         hexdump("RECEIVED", buffer, rd);
      }
   }
   else
   {
      if( spmseq == 0 ) /* startup sequence warmboot */
      {
         incsend("c1-00"); // Startup message R_RESET_UC 00:State_Normal, 01 State_download, 02 State_testmanager
      }
      do
      {
#ifdef USE_DGRAM_SERVICE
         rd = dgram_recv(dgram, buffer, DEFAULT_MSGSZ);
#else
         rd = read(sockfd, buffer, DEFAULT_MSGSZ);
#endif
         if (rd < 0)
            PR_ERR("ERROR reading from socket\n");
         hexdump("RECEIVED", buffer, rd);

         switch( buffer[0] )
         {
            case 0xe8:
               switch( buffer[1] )
               {
                  case 0xC2:  // C_Startup   buffer[2] = 00: Coldstart, 01: restart, 02: warmstart
                     incsend("e8-c3-00"); // R_STARTUP - STATE_NORMAL
                     break;

                  case 0x62:  // C_watchdog
                     incsend("e8-63"); // R_Watchdog
                     break;

                  default:
                     break;
               }
               break;
            default:
               break;
         }
      }while( rd > 0 );
   }
#ifdef USE_DGRAM_SERVICE
   if (dgram_exit(dgram) < 0)
      PR_ERR("dgram_exit failed\n");
#endif

	close(sockfd);
	return 0;
}



