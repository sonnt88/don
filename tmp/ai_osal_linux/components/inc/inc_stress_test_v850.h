/*
    header file for inc_stress_test1.c
*/

#ifndef INC_STRESS_TEST_H
#define INC_STRESS_TEST_H

/***************************************************************************************************
**                                   Includes                                                     **
***************************************************************************************************/
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <sys/time.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <signal.h>
#include <sys/signal.h>
#include "inc.h"
#include "dgram_service.h"
#include "inc_ports.h"
#include "inc_perf_common.h"

/***************************************************************************************************
**                                    MACROS                                                      **
***************************************************************************************************/
#define SIZE (1024 * 64) 
#define MAX_LUN_NUM (8)
#define FALSE (0)
#define TRUE (1)
#define ERROR_MESSAGE_SIZE 100

#define PR_ERR(fmt, args...) \
{ fprintf(stderr, "inc_stress_test: %s: " fmt, __func__, ## args); \
    return -1; }
    
#define EXPCT_EQ(val1, val2, ret, fmt, args...) \
    if (val1 != val2) { \
        LOG_MESSAGE(fmt, ## args); \
        return ret; }

#define EXPCT_NE(val1, val2, ret, fmt, args...) \
    if (val1 == val2) { \
        LOG_MESSAGE(fmt, ## args); \
        return ret; }

        
/***************************************************************************************************
**                            USER-DEFINED DATA TYPES                                             **
***************************************************************************************************/        
typedef struct{
    int lunID;
    const char *lunName;            /*refrain from going beyond 15 characters*/
    const char *messageType;        /*refrain from going beyond 10 characters*/
    const char *testMsgName;        /*refrain from going beyond 30 characters*/
    int C_compStatSize;
    char C_compStatBuff[4];
    unsigned int R_compStatSize;
    char R_compStatBuff[4];
    unsigned int C_testMsgSize;
    char C_testMsgBuff[4];
    int R_testMsgId;   
}lunData;
        
typedef struct{
    const lunData *lun;
    const inc_comm *inccom_channel;
    unsigned long long int sentMsgCnt;
    int readyForComm;
    int channelAssigned;
    int alive;
}tx_arg;

typedef struct{
    int correspondingTxthreadIndex;
    const lunData *lun;
    const inc_comm *inccom_channel;
    unsigned long long int rcvdMsgCnt;
    unsigned long long int improperResponse;
    int readyForComm;
    int alive;
}rx_arg;

/***************************************************************************************************
**                              FUNCTION DECLARATIONS                                             **
***************************************************************************************************/
void *vEventRxThread(void* arg);
void *vEventTxThread(void *arg);
void* vDisplayThread(void* arg);
void sigalrmHandle(int signum);
int summariseTestReport(void);
int checkDuplicacyOfLun(int port, int limit);
void displayLunTable(FILE *fHandle);
void displayIgnoredLuns(FILE *fHandle);
static int inc_communication_sock_init(inc_comm *inccom, int port);
static int inc_communication_sock_exit(inc_comm *inccom);

#endif   /*INC_STRESS_TEST_H*/

/*
END OF FILE
*/
