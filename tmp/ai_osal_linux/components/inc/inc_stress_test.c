/************************************************************************
  | FILE:         inc_stress_test.c
  | PROJECT:      platform
  | SW-COMPONENT: INC
  |------------------------------------------------------------------------------
  | DESCRIPTION:  Test application for INC stress test.
  |
  |               The application opens a socket on LUN 0 and send INC packets as 
  |               fast as possible in a loop. Depending on the hostname the app 
  |               shall either send dummy data with configurable size to the 
  |               (V850) or send a “ping” command (ADR3). 
  |               In a separate thread the application receives the responses.
  |
  |               Command line parameters: Hostname, packet size [bytes]
  |
  |               Execution:
  |               /opt/bosch/base/bin/inc_stress_test_out.out hostname size
  |
  |               Eg. To send 50 bytes to SCC(V850)
  |               /opt/bosch/base/bin/inc_stress_test_out.out scc 50
  |
  |               Eg. To send PING command to ADR3 
  |               /opt/bosch/base/bin/inc_stress_test_out.out adr3 1 
  |
  |------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2014 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification               | Author
  | 11.07.14  | Initial revision           | Shashikant Suguni 
  | --.--.--  | ----------------           | -------, -----
  |
  |*****************************************************************************/
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/time.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include "inc.h"
#include "dgram_service.h"
#include "inc_ports.h"
#include "inc_perf_common.h"

#define SIZE (1024 * 64)

#define PR_ERR(fmt, args...) \
{ fprintf(stderr, "inc_stress_test: %s: " fmt, __func__, ## args); \
    return -1; }

void* vRxThread(void* arg);

int main(int argc, char *argv[])
{
    int err, host_flag = 0;
    char buffer[SIZE];
    long i,size;
    void *ptr;
    pthread_t tid;
    inc_comm inccom;
    
    /*PING command to ADR3*/
    char cmd_Fkt_Get[9] = {0x00,0x91, 0x01, 0x0f,0x02, 0x02, 0x00,0x01,0x21};

    memset(buffer,0,sizeof (buffer));

    if (argc != 3) {
        PR_ERR("Application needs two params \"Hostname\" & \"packetsize\", \n Hostname -> scc for V850 , adr3 for ADR3 \nUSAGE: %s Hostname packet_size\n",argv[0])
    }

    size = atol(argv[2]);
    if (size > SIZE)
    {
        PR_ERR("Please enter the size less than %d \n",SIZE)
    }

    if (strcmp(argv[1],"adr3") == 0)
    {
        host_flag = 1;
    }
    else
    {
        /* Prepare the dummy data to be sent to SCC(v850) node*/
        for (i = 0; i< size; i++)
        {
            buffer[i] = 's';
        }
        buffer[i] = '\0';
    }

    /*Init Inc Communication*/
    err = inc_communication_init(argv[1],&inccom);
    if (err != 0)
        PR_ERR("inc_communication_init failed\n")

    ptr = (void*)(inccom.dgram);

    /* Create the reciever thread*/
    if ( -1 == pthread_create(&tid, NULL, vRxThread,ptr) )
    {
        PR_ERR("ERROR,Thread creation Failed\n\n")
    }
    else
    {
        while(1)
        {
            /*If host is ADR3 send the ping command */
            if (host_flag == 1)
            {
                /*Sending the command to ADR3*/
                dgram_send(inccom.dgram,cmd_Fkt_Get,9);
            }
            /*If host is SCC(v850) send the dummy data of configurable size */
            else
            {
                dgram_send(inccom.dgram,buffer,size);
            }
        }
    }

    if (inc_communication_exit(&inccom) != 0)
    {
        PR_ERR("dgram_exit failed\n")
    }

    return 0;
}

/*
 * Entry function of the worker thread created to receive messages from INC device
 */
void* vRxThread(void* arg)
{
    char buffer[SIZE];

    if(arg == NULL)
        return NULL;

    sk_dgram* pdgram = (sk_dgram*)arg;
    memset(buffer,0,SIZE);

    while(1)
    {
        dgram_recv(pdgram, buffer, sizeof(buffer));
    }
}
