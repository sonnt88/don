/************************************************************************
  | FILE:         inc_analyse.c
  | PROJECT:      platform
  | SW-COMPONENT: INC
  |------------------------------------------------------------------------------
  | DESCRIPTION:   INC performance application 
  |                The app shall read out the existing netdevice statistics at a 
  |                configurable interval, compute the throughput and print it to stdout.
  |                Throughput figures: Average for last interval, average since start of app, 
  |                Total / RX / TX, in bytes/second and packets/second
  |                Command line parameters: Hostname, measurement interval [seconds]
  |
  |                 3 modes: default, per LUN, full
  |                 default: over all LUNs
  |                 per LUN
  |                 full: including message pool size
  |                 Options:
  |                 	-i: interval <>
  |                  -p: per-lun
  |                  -a: avg-lun ie average lun
  |                  -m: msg-pool
  |                  -h: help
  |                  -a -p: per lun and average lun
  |
  |                Execution:
  |                /opt/bosch/base/bin/inc_analyse_out.out hostname <options> 
  |                
  |                Eg. For host INC-SCC(V850), interval 1sec
  |                /opt/bosch/base/bin/inc_analyse_out.out inc-scc  
  |
  |                Eg. For host INC-ADR3(ADR3),interval 1sec
  |                /opt/bosch/base/bin/inc_analyse_out.out inc-adr3  
  |
  |                Eg. For host INC-SCC(V850), interval 3sec
  |                /opt/bosch/base/bin/inc_analyse_out.out inc-scc  -i 3
  |
  |                Eg. For host INC-SCC(V850), msg pool only option
  |                /opt/bosch/base/bin/inc_analyse_out.out inc-scc -m
  |
  |                Eg. For host INC-SCC(V850), per lun option
  |                /opt/bosch/base/bin/inc_analyse_out.out inc-scc -p
  |
  |                Eg. For host INC-SCC(V850), avg lun option
  |                /opt/bosch/base/bin/inc_analyse_out.out inc-scc -a
  |
  |                Eg. For host INC-SCC(V850), avg lun  per lun option
  |                /opt/bosch/base/bin/inc_analyse_out.out inc-scc -p -a
  |
  |------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2014 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification                        | Author
  | 11.07.14  | Initial revision                    | Shashikant Suguni
  | 02.03.16  | Update the user options and usage   | Madhu Sudhan Swargam (RBEI/ECF5)  
  | --.--.--  | ----------------                    | -------, -----
  |
  |*****************************************************************************/
#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "inc_perf_common.h"

#define PR_ERR(fmt, args...) \
{ fprintf(stderr, "inc_analyse: %s: " fmt, __func__, ## args); \
    exit(EXIT_FAILURE); }

unsigned long interval_value=1;
bool msg_pool_prnt_actv = true;
/* Variable used to print per lun and all lun data*/
bool per_lun_prnt_actv = true, avg_lun_prnt_actv = true;

#define RX "RX"
#define TX "TX"
#define TOT "TOT"

/* Prints the INC statistics banner */
void print_incstats_banner(void)
{
    printf ("inc_analyse_out.out: measure throughput of INC communication\n");
    printf ("glossary:\n");
    printf ("\ttime:      elapsed time since start of measurement\n");
    printf ("\tlast:      average throughput for last interval only\n");
    printf ("\tcomplete:  average throughput since start of measurement\n");
    printf ("|----------------------------------------------------------------------------------------------------------------|---------------------------------ERRORS---------------------------------|\n");
    printf ("|-time-|----------------------last-------------------------------|----------------complete-----------------------|--------------RX---------------|-----------------TX-------------|--TOT--|\n");
    printf ("|-sec -|--LUN--|-------packets/sec-------|------bytes/sec--------|-----packets/sec-------|------bytes/sec--------|--CHK--|--SEQ--|--XOFF-|--SYNC-|--CHK--|--SEQ--|--XOFF--|--SYNC-|--TOT--|\n");
    printf ("\t \t%7s\t%7s\t%7s\t%7s\t%7s\t%7s\t%7s\t%7s\t%7s\t%7s\t%7s\t%7s\n",RX,TX,TOT,RX,TX,TOT,RX,TX,TOT,RX,TX,TOT);
}

/* Prints the usage of inc_analysis application */
void print_usage_options()
{
    printf ("\nUsage: /opt/bosch/base/bin/inc_analyse_out.out <host> <options>\n\n");
    printf ("List of Hosts\n");
    printf ("\tinc-scc or inc-adr3\n\n");
    printf ("List of options\n");
    printf ("\tdefault                          interval: 1sec, default statistics: per lun average lun msg-pool \n");
    printf ("\t-i <interval value>              :non-default interval: 1sec, default statistics: per lun average lun msg-pool  \n");
    printf ("\t-m                               :only msg-pool\n");
    printf ("\t-p                               :only per-lun\n");
    printf ("\t-p -a                            :per-lun + average-lun\n");
    printf ("\t-a                               :only average-lun\n\n\n");
}

/* Updates the user option */
void get_user_option(char* const* opt)
{
    int opt_ind =0;

    while (opt[opt_ind] != NULL)
    {
        if (strstr(opt[opt_ind],"-i"))
        {
            opt_ind++;
            if(opt[opt_ind] != NULL)
            {
                interval_value = (unsigned long)atol(opt[opt_ind]);
            }
            else
            {
               printf("Wrong usage of application \n");
               exit(EXIT_SUCCESS);
            }
            break;
        }
        else if (strstr(opt[opt_ind],"-m"))
        {
            avg_lun_prnt_actv = false;
            per_lun_prnt_actv = false;
            break;
        }
        else if (strstr(opt[opt_ind],"-p"))
        {
            msg_pool_prnt_actv = false;
            avg_lun_prnt_actv = false;
            opt_ind++;
            if((opt[opt_ind] != NULL) && strstr(opt[opt_ind],"-a"))
            {
                avg_lun_prnt_actv = true;
            }
            break;
        }
        else if (strstr(opt[opt_ind],"-a"))
        {
            msg_pool_prnt_actv = false;
            per_lun_prnt_actv = false;
            opt_ind++;
            if((opt[opt_ind] != NULL) && strstr(opt[opt_ind],"-p"))
            {
                 per_lun_prnt_actv = true;
            }
            break;
        }
        else if (strstr(opt[opt_ind],"-h"))
        {
            /* Help print is done already */
            exit(EXIT_SUCCESS);
        }
        else
        {
            break;
        }
    }
    printf("User options: Interval = %lu, msg-pool=%s, per-lun=%s, All-lun =%s\n\n",
           interval_value, 
          (msg_pool_prnt_actv == true)? "Active": "InActive",
          (per_lun_prnt_actv == true)? "Active": "InActive",
          (avg_lun_prnt_actv == true)? "Active": "InActive"
         );
}

int main(int argc, char * const argv[])
{

    unsigned long count = 0;
    char host[20];
    bool loop_actv = true;

    memset (host,0,sizeof (host));
    if (argc > 5 || argc < 2) 
    {
        PR_ERR("Application needs proper parameters. Use %s --help\n",argv[0])
    }
    if(strstr(argv[1],"-h"))
    {
        print_usage_options();
        exit(EXIT_SUCCESS);
    }
    else
    {
      print_usage_options();
    }
    /* Get the user options */
    get_user_option(&argv[2]); 

    strcpy (host,argv[1]); 

    /*Print the banner*/
    print_incstats_banner();
    while (loop_actv)
    {
        int ret;
        /* Compute the throughtput per interval */
        ret = throughput_handler(host,interval_value,count);
        if (ret !=0)
        {
            PR_ERR("throughput_handler failed\n")
        }

        /*Appends the throughput figures to console*/
        print_throughput(interval_value,count,
                          per_lun_prnt_actv, avg_lun_prnt_actv);
        if(count && msg_pool_prnt_actv)
        {
            print_msgpool_info(host);
        }
        if(count)
        {
            printf("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
        }
        count ++;
        sleep (interval_value);
    }
}
