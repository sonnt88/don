/************************************************************************
| FILE:         dgram_service.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Datagram service to be used on TCP stream socket.
|
|               Written data is prepended with a header which is used to find
|               message boundaries on receive.
|
|               Each call to dgram_send will result in one call to dgram_recv
|               returning.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 15.01.13  | Initial revision           | Andreas Pape
| 21.02.13  | Fix cpp compiler issues    | Matthias Thomae
| 06.02.13  | Move functions to .c file  | Matthias Thomae
| 25.06.15  | LintFix in dgram_init()    | Shahida Mohammed Ashraf(ECF5)
|           | CFG3-1266                  |
| 07.10.16	| Support for TCP/IP		 | Mai Daftedar
|			| based Gen4 INC			 |
| 17.10.16	| Fix RADAR and lint issues	 | Venkatesh Parthasarathy
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "dgram_service.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef AF_INC
#define AF_INC 41
#endif


/*****************************************************************
sk_dgram* dgram_init(int sk, int dgram_max, void *options)
* PURPOSE : init datagram servive on given socket
*
* RETURN :  Pointer to the dgram structure initialized
*          
* Author :  

*****************************************************************/
sk_dgram* dgram_init(int sk, int dgram_max, void *options)
{
   sk_dgram *skd;
   struct sockaddr_in addr = {0}; //lint fix CFG3-1266
   socklen_t len = sizeof(struct sockaddr);
//lint -e64

   if(getsockname(sk, (struct sockaddr *)&addr, &len)<0)
      return NULL;
  
   DG_DBG("dgram init on socket af %d\n",addr.sin_family);

   if(dgram_max > (int) DGRAM_MAX) 
   { /* satisfy lint */
      DG_ERR("exceed maxsize (%d>%ld)\n",dgram_max, DGRAM_MAX);
      return NULL;
   }

    switch(addr.sin_family) 
	{
      case AF_INET:
      case AF_INC:
         break;
      default:
         DG_ERR("not supported on AF %d\n",addr.sin_family);
         return NULL;
	}
   
   skd = (sk_dgram *) malloc(sizeof(sk_dgram));
	
	if(!skd)
	{
      DG_ERR("failed allocating memory\n");
      return NULL;
	}
   
	memset(skd, 0,sizeof(*skd));
	skd->proto = addr.sin_family;
	skd->hlen = sizeof(dgram_header_std);
	skd->len = dgram_max;

   /*Not needed for now for GEN4: MESSAGE_PROTOCOL is defined*/
   #ifndef MESSAGE_PROTOCOL
   skd->buf = (char *) malloc(skd->len+skd->hlen);

   if(!skd->buf) {
      DG_ERR("failed allocating rcv buffer\n");
      free(skd);
      return NULL;
   }
   #endif
   
   skd->sk = sk;
   /*ignore options*/
   UNUSED_VARIABLE(options);

   return skd;
}

/*****************************************************************
int dgram_exit(sk_dgram *skd)

* PURPOSE : Freeing the dgram structure created
*
* RETURN :  Error code, if sk_dgram is invalid or success otherwise
*          
* Author :  

*****************************************************************/
int dgram_exit(sk_dgram *skd)
{
	if(!skd) 
	{
		DG_ERR("invalid handle\n");
		errno = EINVAL;
		return -1;
	}
   
 
    /*Not needed for now for GEN4: MESSAGE_PROTOCOL is defined*/
	#ifndef MESSAGE_PROTOCOL
 	free(skd->buf);
	skd->buf = NULL;
	#endif
   
	free(skd);
 	return 0;
}

/*****************************************************************
int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen)

* PURPOSE : Send message as datagram, just prepend header,
* for GEN4 use 
*
* RETURN :  
*                       
*
* NOTES :   
*          
* Author :  

*****************************************************************/
int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen)
{
   struct msghdr msg;
   struct iovec iov[2];
   dgram_header_std h;
   int ret;
      
   if(skd->proto != AF_INET)
   {
      return send(skd->sk, ubuf, ulen, 0);
   }
   
   if((int) ulen > skd->len) { /* satisfy lint */
      DG_ERR("send: dgram exceeds bufsize (%ld>%d)\n", ulen, skd->len);
      errno = EMSGSIZE;
      return -1;
   }
   
   memset(&msg, 0, sizeof(msg));
   msg.msg_iovlen = 2;
   msg.msg_iov = iov;
   
   #ifdef MSG_PROTOCOL
   /*Header to include the digital Id identifer and the message length*/
   h.dglen = ulen;
   h.msgid = htons(ulen);
   #else
   h.dglen = htons(ulen);
   #endif
   
   iov[0].iov_base = &h;
   iov[0].iov_len = sizeof(h);
   iov[1].iov_base = ubuf;
   iov[1].iov_len = ulen;
  
   ret = sendmsg(skd->sk, &msg, 0);
   if(ret >= (int) sizeof(h)) /* satisfy lint */
      ret -= sizeof(h);

   return ret;
}
/*****************************************************************
int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen)

* PURPOSE : Messaging protocol to return the atomic message data
* based on the size added in the message header. For GEN4 it is not required
* to loop to obtain the full sized data required.
*
* RETURN :  
*                       
* NOTES :   
*          
* Author :  

*****************************************************************/
int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen)
{
	int totalrd = 0;	

	#ifdef MSG_PROTOCOL
	dgram_header_std *h;
	int rd, size;
	/*Use two system calls to receive the stream of data in Packets*/
	 rd = recv(skd->sk , ubuf , sizeof(dgram_header_std) , 0);
	 h = (dgram_header_std *)ubuf;
	 
	 DG_DBG("Size of Message : %d\n",h->dglen);
	 
	 size = h->dglen;
	 if(size > (int)DGRAM_MAX || size > (int)ulen)
	 {
		DG_ERR("Maximum size of message exceeded \n");
		errno = EMSGSIZE;
		return -1;
	 }
	 do
	 {
		 rd = recv(skd->sk, ((char *)ubuf)+rd, size , 0);
		 size -= rd;
		 
		 totalrd += rd;
		 
		 DG_DBG("Receive Bytes:%d\n",rd);
		 
		 if(rd < 0) 
		 {
			if(errno == EAGAIN)
			{
				DG_DBG("NONBLOCKING SOCKETS used, all data is received \n");
				goto ERR;
			}
			else
			{
				DG_DBG("Receive failed with err%d \n", errno);
				rd = errno;
			}
			break;
		 }
	 }while(size > 0);
	 
   #else
   
   if(skd->proto != AF_INET)
      return recv(skd->sk, ubuf, ulen, 0);


   if((skd->received < skd->hlen) || ((skd->received >= skd->hlen) && (skd->received < (skd->h.dglen+skd->hlen))))
   {
      totalrd = recv(skd->sk, skd->buf+skd->received, (skd->len + skd->hlen)-skd->received, 0);
      if(totalrd <= 0)
	  {
         DG_DBG("receive failed with err %d %d \n",totalrd, errno);
         return totalrd;
      }
      skd->received += totalrd;
   }

   if(skd->received < skd->hlen){
      errno = EAGAIN;
      return -1;
   }

   /*complete header available*/
   memcpy((char*)&skd->h, skd->buf, skd->hlen);
   skd->h.dglen = ntohs(skd->h.dglen);

   if(skd->received < (skd->h.dglen+skd->hlen ))
   {
      errno = EAGAIN;
      return -1;
   }

   /*full dgram available*/
   if(ulen < skd->h.dglen) 
   {
      DG_ERR("recv: dgram exceeds bufsize (%d>%d)\n", skd->h.dglen, ulen);
      errno = EMSGSIZE;
      return -1;
   }

   memcpy(ubuf, skd->buf+skd->hlen, skd->h.dglen);
   skd->received-= (skd->h.dglen+skd->hlen);
   totalrd = skd->h.dglen;
   if(skd->received > 0 ) 
   {
      memmove(skd->buf, skd->buf+skd->hlen+skd->h.dglen, skd->received);
      if(skd->received >= skd->hlen)
	  {
         memcpy((char*)&skd->h, skd->buf, skd->hlen);
         skd->h.dglen = ntohs(skd->h.dglen);
      }
   }
   DG_DBG("finished DGRAM of len %d\n", totalrd);
   
   #endif
   
ERR:
   return totalrd;

}

#ifdef __cplusplus
}
#endif
