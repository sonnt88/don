/************************************************************************
| FILE:         inc_demo.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  PC tool for simulating INC messages
|				and demonstration of Gen4 INC.
|				Refer Readme.txt for usage of INC PC tool
|


|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2016 Robert Bosch GmbH| HISTORY:
| Date      | Modification               | Author
| 24.01.13  | Initial revision           | Mai Daftedar
| 17.10.16	| Fix RADAR and lint issues	 | Venkatesh Parthasarathy
| --.--.--  | ----------------           | -------, -----
|*****************************************************************************/

#include "inc_demo.h"

#ifdef OSAL_GEN4
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/time.h>
#include <time.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
   

extern int bufflen;
extern int closesocket;

/*******************************************************************************
* static void hexdump(const char *intro, char *buffer, int len)
*
* PURPOSE : Priniting the buffer passed in in Hex Format
*
* RETURN :  NONE
*          
* Author :  
*
******************************************************************************/
static void hexdump(const char *intro, char *buffer, int len)
{
	int i;
	fprintf(stdout,"%s:",intro);
	for (i=0;i<len;i++)
		fprintf(stdout," 0x%02x", buffer[i]);
	fprintf(stdout,"\n");
}

/*******************************************************************************
*static int arg2payload(char *buffer, int buflen, char* string)
*
* PURPOSE : Helper function to convert from string to HEx Format
*	    input: payload as hex-ASCII string: e.g: a9-de-56-03
* RETURN :  The length of the output buffer
*                        
* Author :  
*
******************************************************************************/

static int arg2payload(char *buffer, int buflen, char* string)
{
	char *pos, *end;
	long result;
	int len = 0;
	if(!string)
		return -1;
	pos = string;
	end = string;
	while(*end != '\0' && (len < buflen)) {
		result = strtol(pos, &end ,16);
		if(result > 255)
			return -1;
		if(result < 0)
			return -1;
		buffer[len++] = result;
		if(*end != '\0') 
		{
			if(*end == '-')
				end++;
			else
				return -1;
		}
		pos = end;
	}
	return len;
}
/****************************************************************************************
* int inc_send(int sockid, int buflen,int mode,char * pattern,int loopflag,sk_dgram *skdgram)
*
* PURPOSE : Simple echo test example,
*			Sending different formates of data and receiving it back.
*			Data can be of different sizes based on the length passed in
* RETURN :  Returns the status of the global errno of the recv or send operation
*          
* Author :  
*
***************************************************************************************/
static int inc_send(int sockid, int buflen,int mode,char * pattern,int loopflag,sk_dgram *skdgram)
{
	int i,err=0, bytesent,append =0,rd;
	int loop;
	
	
	switch(mode)
	{
	
		case FIXED:
		for(i=0;i<buflen;i++)
		{
			append += sprintf(buffer+append,"%s",FIXED_PATTERN);
		}
		break;
		case ALTERNATE:
		for(i=0;i<buflen;i++)
		{
			append += sprintf(buffer+append,"%s",ALTERNATE_PATTERN);
		}
		break;
		case PREDEFINED:
		for(i=0;i<buflen;i++)
		{
			append += sprintf(buffer+append,"%s",pattern);
		}
		break;
		
	}
	
	hexdump("SEND", buffer,buflen);
	 
	/*Flag defines either a continous test scneario or a noncontinous mode*/
	if(loopflag == CONTINOUSTEST)
		loop = 1;
	else
		loop = 0;
	do
	{
		#ifdef USE_DGRAM_SERVICE
        bytesent = dgram_send(skdgram, buffer, buflen);
		#else
        bytesent = write(sockid,buffer,buflen);
		#endif
	
		PR_MSG("DGRAM sent %d\n",bytesent);
	
		if (bytesent < 0 && (errno!= EAGAIN)) 
		{
		
           err = errno;
           PR_ERR("ERROR writing to socket: %d %s\n", err, strerror(err));
		   goto ERR;
		}
		else
		{
			PR_MSG("DGRAM sent successfully\n");
		}
    }while(loop);
	
	
ERR: 
	return err;
}

/****************************************************************************************
* int inc_echotest(int sockid, int buflen,int mode,char * pattern,int loopflag,sk_dgram *skdgram)
*
* PURPOSE : Simple echo test example,
*			Sending different formates of data and receiving it back.
*			Data can be of different sizes based on the length passed in
* RETURN :  Returns the status of the global errno of the recv or send operation
*          
* Author :  
*
***************************************************************************************/
int inc_echotest(int sockid, int buflen,int mode,char * pattern,int loopflag,sk_dgram *skdgram)
{
	int i,err=0, bytesent,append =0,rd;
	int loop;
	
	
	switch(mode)
	{
	
		case FIXED:
		for(i=0;i<buflen;i++)
		{
			append += sprintf(buffer+append,"%s",FIXED_PATTERN);
		}
		break;
		case ALTERNATE:
		for(i=0;i<buflen;i++)
		{
			append += sprintf(buffer+append,"%s",ALTERNATE_PATTERN);
		}
		break;
		case PREDEFINED:
		for(i=0;i<buflen;i++)
		{
			append += sprintf(buffer+append,"%s",pattern);
		}
		break;
		
	}
	
	hexdump("SEND", buffer,buflen);
	 
	/*Flag defines either a continous test scneario or a noncontinous mode*/
	if(loopflag == CONTINOUSTEST)
		loop = 1;
	else
		loop = 0;
	do
	{
		#ifdef USE_DGRAM_SERVICE
        bytesent = dgram_send(skdgram, buffer, buflen);
		#else
        bytesent = write(sockid,buffer,buflen);
		#endif
	
		PR_MSG("DGRAM sent %d\n",bytesent);
	
		if (bytesent < 0 && (errno!= EAGAIN)) 
		{
		
           err = errno;
           PR_ERR("ERROR writing to socket: %d %s\n", err, strerror(err));
		   goto ERR;
		}
		else
		{
			PR_MSG("DGRAM sent successfully\n");
		}

		#ifdef USE_DGRAM_SERVICE
         rd = dgram_recv(skdgram, buffer, buflen);
		#else
         rd = read(sockid, buffer, buflen);
		#endif
	
		if (rd < 0 && (errno!= EAGAIN))
		{
			err = errno;
			PR_ERR("ERROR reading from socket: %d %s\n", err, strerror(err));
			goto ERR;
		}
		else
		{
			PR_MSG("Bytes received %d\n",rd); 
			hexdump("RECEIVED", buffer, rd);
		}
    }while(loop);
	
	
ERR: 
	return err;
}

/*************************************************************************************
*int inc_delaysendrecv
*(int commFd,int buflen,char * buff,int delay,int testmode,int loopflag,sk_dgram *skdgram)
*
* PURPOSE : Continous sending or receiving with configurable delays based on the 
*	    passed in parameters, default mode waits for data reception and sends
*	   the data back.
*
* RETURN :   Returns the status of the global errno for the different socket function 
*	calls
*                       
*          
* Author :  
*
***************************************************************************************/
int inc_delaysendrecv(int commFd,int buflen,char * buff,int delay,int testmode,int loopflag,sk_dgram *skdgram) 
{
	int i, bytesent,rd;
	int loop;
    int err=0;
	
	
	if(loopflag == CONTINOUSTEST)
		loop = 1;
	else
		loop = 0;
	
	switch(testmode)
	{	
	case SENDER:		
		/*Continously sending data with configurable delay*/
		/* Prepare the dummy data to be sent to SCC(v850) node*/
        	for (i = 0; i< buflen; i++) 
		{
			buff[i] = 'c';
		}
			buff[i] = '\0';

			i = 0;
			
		while(loopflag) 
		{
			
			#ifdef USE_DGRAM_SERVICE
			bytesent = dgram_send(skdgram, buff, buflen);
			#else
			bytesent = send(commFd, buff, buflen, 0);
			#endif
			if (bytesent < 0 && (errno!= EAGAIN))
			{
				err = errno;
				PR_ERR("ERROR writing to socket: %d %s\n", err, strerror(err));
				goto ERR;
			}
			i++;
			PR_MSG("Send : Loop %d, Data: %s\n", i, buff);
			usleep(delay);
		}
		break;
	case RECV:		/*Continously receiving data with configurable delay*/
		i = 0;
		PR_MSG("SLOW receiving starting\n");
		
		while(loopflag) 
		{
			#ifdef USE_DGRAM_SERVICE
			rd = dgram_recv(skdgram, buff, buflen);
			#else
			rd = recv(commFd, buff, buflen, 0);
			#endif
			if (rd < 0 && (errno!= EAGAIN))
			{
				err = errno;
				PR_ERR("ERROR reading from socket: %d %s\n", err, strerror(err));
				goto ERR;
			}
			i++;
			PR_MSG("Recv : Loop %d, Data: %s\n", i, buff);
			usleep(delay);
		}
		
		break;
	case DEFAULT: /*the default mode, sends back the data received */
	
			
		PR_MSG("Receive data and send it back\n");
		do
		{
			#ifdef USE_DGRAM_SERVICE
			rd = dgram_recv(skdgram, buff, buflen);
			#else
			PR_MSG(" Receiving data from socket %d\n",commFd);
			rd = recv(commFd, buff, buflen, 0);
			#endif

			PR_MSG("Bytes received %d\n",rd);
			hexdump("Received", buff, rd);
			if(rd <= 0)
			{
				if(errno!= EAGAIN)
				{
					err = errno;
					PR_ERR("ERROR reading from socket: %d %s\n", err, strerror(err));
					goto ERR;
				}
			}
			else 
			{
			PR_MSG("Sending back the data sent\n");
			#ifdef USE_DGRAM_SERVICE
			bytesent = dgram_send(skdgram, buff, strlen(buff));
			#else
			bytesent = send(commFd,buff,strlen(buff),0);
			#endif
			if (bytesent < 0) 
			{
				if(errno!= EAGAIN)
				{
					err = errno;
					PR_ERR("ERROR writing to socket: %d\n", bytesent);
					goto ERR;
				}
			}
			hexdump("SENT", buff , bytesent);
			}
			
		}while(loop);
		
		break;
	}
ERR:
 return 0;
}


/*************************************************************************************
*int inc_verifypattern(int buflen,char* buff,int ptrnlen,char * pattern)
*
* PURPOSE : Function to verify the contents of the buffer based on a certain
* expected format
*
* RETURN :  Verification status
*                       
*          
* Author :  
*************************************************************************************/
int inc_verifypattern(int buflen,char* buff,int ptrnlen,char * pattern)
{
	int err = TEST_SUCCESS;
	int i=0,j=0;
	char *ptr_char;
	
	for(i=0;i<buflen;i+=(ptrnlen))
	{
		ptr_char = pattern;
		for(j=i;j<i+(ptrnlen);j++,ptr_char++)
		{
			if(j==buflen)
				break;
			if(!(buffer[j] == *ptr_char))
			{
				PR_ERR("Character Failed\n");
				break;
			}
		}
	}

	PR_MSG("\t************TEST VERIFICATION************\t\n");
	PR_MSG("\tTEST VERIFICATION STATUS:%s\t\n",(err)?"Fail":"Success");
	
	return err;	
	
}


/*************************************************************************************
* static int inc_socket_nonblocking(int sfd,int *epfd,int operationmode,int listener_numb)
* PURPOSE : Register a defined number of events to be monitored
*	EPOLLRDHUP : This flag is especially useful for writing simple 
*	code to detect peer shutdown when using Edge Triggered monitoring.
*	EPOLLERR : Error condition happened on the associated file descriptor. 
*	epoll_wait(2) will always wait for this event; it is not necessary to 
*	set it in events.
*	EPOLLIN : The associated file is available for read(2) operations.
*	EPOLLET	: Using Edge Triggered monitoring.
*
* RETURN :  NONE
*                       
*          
* Author :  
***************************************************************************************/
static int inc_socket_nonblocking(int sfd,int epfd,int operationmode,int listener_numb)
{
	int flags, status;
	int err=0;
	/*epoll struct that contains the list of epoll events attached to a network socket*/
	static struct epoll_event Edgvent;
	
	
	flags = fcntl (sfd, F_GETFL, 0);
	if (flags == -1) 
	{
		err = errno;
		PR_ERR("ERROR: fcntl get flags : %d %s\n", err, strerror(err));
		goto ERR;
	}

	flags |= O_NONBLOCK;
	status = fcntl (sfd, F_SETFL, flags);
	if (status == -1) 
	{
		err = errno;
		PR_ERR("ERROR: fcntl set flags : %d %s\n", err, strerror(err));
		goto ERR;
	}

	Edgvent.events =  EPOLLRDHUP | EPOLLIN | EPOLLERR | EPOLLET;
	
	if(operationmode == CLIENT)
	{
		
		Edgvent.data.fd = sfd;
		 /*Add the socket to the epoll file descriptors*/
		if(epoll_ctl(epfd, EPOLL_CTL_ADD, sfd, &Edgvent) != 0)
		{
			err = errno;
			PR_ERR("ERROR: epoll add FDs : %d %s\n", err, strerror(err));
			goto ERR;
		}
	}
	else if(operationmode == SERVER)
	{
		Edgvent.data.fd = listener_numb;
		/*Add the socket to the epoll file descriptors*/
		if(epoll_ctl(epfd, EPOLL_CTL_ADD, sfd, &Edgvent) != 0)
		{
			err = errno;
			PR_ERR("ERROR: epoll add FDs : %d %s\n", err, strerror(err));
			goto ERR;
		}
		
	}
   
ERR:	
	return err;
}
/****************************************************************************
* int clientstart(clientparam * strclientparam,int *epfd)
* PURPOSE : Starting client and  configuring the 
*			event handling functions for the nonblocking socket type  
*
* RETURN :   Errors from different socket functions is returned 
*                       
*          
* Author :  
****************************************************************************/
int clientstart(clientparam * strclientparam,int epfd)
{
	int status;
	struct sockaddr_in servAddr, clientAddr;
    struct hostent *server;
    struct hostent *local;
	socklen_t addr_size;
	int enable = 1;
	int err=0;

	
					
	/*set socket to non blocking and allow port reuse*/
	if ( (setsockopt(strclientparam->sockfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(enable))) == -1)
   	{
		err = errno;
		PR_ERR("ERROR: Set socket opts : %d %s\n", err, strerror(err));
		goto ERR;
   	}

	server = gethostbyname(intf_name);
	if (server == NULL)
	{
		PR_ERR("ERROR, no such remote host\n");
		err = ER_REMOTE_HOST_FAIL;
		goto ERR;
	}
	
	
	servAddr.sin_family = AF_INET;
	memcpy((char *)&servAddr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
	servAddr.sin_port = htons(strclientparam->portno);

	sprintf(local_name, "%s-local", intf_name);
	local = gethostbyname(local_name);
  
	if (local == NULL)
	{
		PR_ERR("ERROR, Local host failure\n");
		err = ER_LOCAL_HOST_FAIL;
		goto ERR;
	}

	clientAddr.sin_family = AF_INET;
	memcpy((char *)&clientAddr.sin_addr.s_addr, (char *)local->h_addr, local->h_length);
	clientAddr.sin_port = htons(strclientparam->localportno);
		
	addr_size = sizeof(clientAddr);
	status = bind(strclientparam->sockfd, (struct sockaddr *) &clientAddr, addr_size);
	if (status < 0)
	{ 
		err = errno;
		PR_ERR("ERROR binding: %d %s\n", err, strerror(err));
		goto ERR;
	}

	addr_size = sizeof(servAddr);
	status = connect(strclientparam->sockfd,(struct sockaddr *) &servAddr,addr_size);
	if(status < 0 && errno != EINPROGRESS)
	{
		err = errno;
		PR_MSG("ERROR connecting: %d %s\n", err, strerror(err));
		goto ERR;
	}
	#ifdef USE_DGRAM_SERVICE
	dgram = dgram_init(strclientparam->sockfd, DGRAM_MAX, NULL);
	if (dgram == NULL)
	{
		err = ER_DGRAM_INIT;
		PR_MSG("ERROR: dgram init fail %d\n",err);
		goto ERR;
	}
	#endif
	clientstate = CONNECTED;
	if(strclientparam->socktype == NONBLOCKING_SOCKET)
	{
		inc_socket_nonblocking(strclientparam->sockfd,epfd,CLIENT,0);
		inc_send(strclientparam->sockfd,bufflen,strclientparam->mode,pattern,NONCONTINOUSTEST,dgram);
	}
	
ERR:
	return err;
}	
/*****************************************************************
*int startserver(serverparam* strstructparam)
*
* PURPOSE : Starting server and configuring the event handling 
*			functions for the nonblocking socket type  
*
*
* RETURN :  Errors from different socket functions is returned 
*                       
*
* Author :  
*****************************************************************/
int startserver(serverparam* srvstructparam)
{
	/*Variable to identify the test number and flag for the automatic test cases to stress test*/
	int client_fd,err =0;	
	struct sockaddr_in servAddr, clientAddr;
	socklen_t addr_size;
	char str[INET_ADDRSTRLEN];
	int status = 0;
	int enable = 1;

	
	addr_size = sizeof(servAddr);
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = INADDR_ANY;
	servAddr.sin_port = htons(srvstructparam->portno);
     
	
	if (setsockopt(srvstructparam->sockfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT , &enable, sizeof(int)) < 0)
	{
		err = errno;
		PR_ERR("ERROR: Set socket opts : %d %s\n", err, strerror(err));
		goto ERR;
	}
	
	status = bind(srvstructparam->sockfd, (struct sockaddr *) &servAddr, addr_size);
	if(status < 0) 
	{
		err = errno;
		PR_MSG("ERROR binding: %d %s\n", err, strerror(err));
		goto ERR;
	}

	if(listen(srvstructparam->sockfd, MAX_NUM_CLIENT) == 0)
	{
		PR_MSG("Listening\n");
	}
	else
	{
		err = errno;
		PR_MSG("ERROR listening: %d %s\n", err, strerror(err));
		goto ERR;
	}

	addr_size = sizeof(clientAddr);

	do
	{
		PR_MSG("Waiting to accept clients\n");
		client_fd = accept(srvstructparam->sockfd, (struct sockaddr *)&clientAddr,&addr_size);
		if (client_fd < 0 && errno != EAGAIN) 
		{
			err = errno;
			PR_MSG("ERROR: Accept: %d %s\n", err, strerror(err));
			goto ERR;
		}
			
		inet_ntop(AF_INET, &clientAddr.sin_addr, str, INET6_ADDRSTRLEN);
		PR_MSG("Connection accepted with address: %s\n", str);
	
		
		#ifdef USE_DGRAM_SERVICE
		strlistenerlst[client_number].client_sk = client_fd; 
		strlistenerlst[client_number].dgram = dgram_init(client_fd, DGRAM_MAX, NULL);
		if(strlistenerlst[client_number].dgram == NULL)
		{
			err = ER_DGRAM_INIT;
			PR_MSG("ERROR: dgram init fail %d\n",err);
			goto ERR;
		}
		
			
		if(srvstructparam->socktype == NONBLOCKING_SOCKET)
		{
			inc_socket_nonblocking(client_fd,srvstructparam->epfd,SERVER,client_number);
		}
		
		#endif
		strlistenerlst[client_number].state = CONNECTED;
		if(srvstructparam->socktype == BLOCKING_SOCKET)
			inc_handleblockmode(strlistenerlst[client_number].client_sk,strlistenerlst[client_number].dgram);
		
		client_number ++;
		
		usleep(600);
	}while(srvstructparam->multiclient);
		
ERR:
	if(err)
	{
		/*we should shutdown, if an error occured in the system calls*/
		closesocket = 1;
	}
	return err;	
}


/*****************************************************************
* void eventhandle(epollstruct* strpoll)
* PURPOSE : This function runs in a thread to continously wait for
* events received by the epoll signaling system and handles the events 
* accordingly.
* Client and server use a ping pong data exchange, when an event is
* recevived to indicate data reception, the sama data is sent back
*
* RETURN :  NONE
*                       
*          
* Author :  
*****************************************************************/
void eventhandle(epollstruct* strpoll)
{
 int i,count;
 int status = 0;
 int clientnumb;
  do
   {
     
	  PR_MSG("EPOLL WAITING\n");
	 
      count = epoll_wait(strpoll->epfd, strpoll->events, MAX_NUM_EVENTS, -1);
	   
	   PR_MSG("Number of events received: %d Events:%d \n",count,strpoll->events[i].events);
      for(i=0;i<count;i++)
      {
         if( (strpoll->events[i].events & EPOLLRDHUP) ||
		   ( strpoll->events[i].events & EPOLLHUP) ) 
        {
			PR_MSG("SOCKET: close \n");
			
			if(operationMode == CLIENT)
			{
				/*Removing socket from the epoll control handlers*/
				epoll_ctl(strpoll->epfd, EPOLL_CTL_DEL, strpoll->events[i].data.fd, NULL);
				PR_MSG("Closing socket with ID:%d\n",strpoll->events[i].data.fd);
				
				status = close(strpoll->events[i].data.fd);
				if(status != 0)
				{
					PR_MSG("Error while closing socket with ID:%d Status:%d\n",strpoll->events[i].data.fd,errno);
					break;
				}
			}
			else
			{
				/*For server side, using the client number*/
				clientnumb = strpoll->events[i].data.fd;
				if(strlistenerlst[clientnumb].client_sk == -1)
				{
					PR_MSG("This socket is already closed wait for other events\n");
					continue;
				}
				/*socket is closed, remove the socket from epoll and create a new one*/
				epoll_ctl(strpoll->epfd, EPOLL_CTL_DEL,strlistenerlst[clientnumb].client_sk,NULL);
			
				PR_MSG("Closing socket%d\n",strlistenerlst[clientnumb].client_sk);
				PR_MSG("Invalidate the list listener entry ClientNumb%d\n",clientnumb);
				
				status = close(strlistenerlst[clientnumb].client_sk);
				if(status != 0)
				{
					PR_MSG("ERROR: Closing socket with ID:%d Error:%d\n",strlistenerlst[clientnumb].client_sk,errno);
					break;
				}
				strlistenerlst[clientnumb].client_sk = -1;
			}
           
         }
		
	if (strpoll->events[i].events & EPOLLIN)/*Event Reception signaling*/
         {
			memset(buffer,0,DEFAULT_MSGSZ+1);
			
			
			if(operationMode == CLIENT)
			{
				inc_delaysendrecv(strpoll->events[i].data.fd,bufflen,buffer,0,DEFAULT,NONCONTINOUSTEST,dgram);
			}
			else if(operationMode == SERVER)
			{
				clientnumb = strpoll->events[i].data.fd;
				PR_MSG("Client number sending the data %d\n",clientnumb);
				
				if(strlistenerlst[clientnumb].client_sk == -1)
				{
					PR_MSG("Client connection LOST reconnect\n");
					break;
				}
				
				inc_delaysendrecv(strlistenerlst[clientnumb].client_sk,bufflen,buffer,0,DEFAULT,NONCONTINOUSTEST,strlistenerlst[clientnumb].dgram);
			}
		  }
		}
	   usleep(600);
   } while(1);  
}

#endif
