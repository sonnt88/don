/*
inc_stress_test log file operations and time logging
*/

/***************************************************************************************************
**                                   Includes                                                     **
***************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdarg.h>
#include <errno.h>

#include "inc_stress_test_v850_logging.h"

/***************************************************************************************************
**                                    MACROS                                                      **
***************************************************************************************************/


/***************************************************************************************************
**                                   Constants                                                    **
***************************************************************************************************/
static const char *LogFileName = "inc_stress_test_log";   /* path of the log file */

/***************************************************************************************************
**                                   Variables                                                    **
***************************************************************************************************/
char TimeStamp[TIME_STAMP_SIZE] = {0};
FILE *flog;

/***************************************************************************************************
**                                   FUNCTIONS                                                    **
***************************************************************************************************/

/***********************************************************************************************************************
 * int OpenLogFile(void) description
 *
 * \Opens the log file
 *
 * \param : NA
 *
 * \return : 0 : success, -1 : failure
***********************************************************************************************************************/
int OpenLogFile(void)
{
	flog = fopen(LogFileName, "a");
    if(flog == NULL)
    {
        fprintf(stdout, "%s could not be opened/created.\n", LogFileName);
        perror("OpenLogFile");
        return -1;
    }
    
    fprintf(flog, "\n*****************************************************************************************************************************************************\n");
    fprintf(flog, "*********************************************************** LOG FILE for INC STRESS TEST **************************************************************\n");
    fprintf(flog, "*****************************************************************************************************************************************************\n\n");
    
	GetTimeStamp(TimeStamp);
    fprintf(flog, "%s : %s\n\n", "Time Of Creation", TimeStamp);
     
    return 0;
}

/***********************************************************************************************************************
 * void CloseLogFile(void) description
 *
 * \Closes the log file
 *
 * \param : NA
 *
 * \return : NA
***********************************************************************************************************************/
void CloseLogFile(void)
{
	GetTimeStamp(TimeStamp);
    
    fprintf(flog, "%s : %s\n\n", "Last Access Time", TimeStamp);
    fprintf(flog, "\n*****************************************************************************************************************************************************\n");
    fprintf(flog, "******************************************************************** END OF FILE **********************************************************************\n");
    fprintf(flog, "*****************************************************************************************************************************************************\n\n");
    fflush(flog);
    
    close(fileno(flog));
}

/***********************************************************************************************************************
 * \void GetErrorMessage(int err, char *buffer, size_t size) description
 *
 * \fetches the error string meant for the errno 'err' and store in 'buffer'
 * \Undefined behaviour if the 'size' is not sufficient to store the message. 
 *
 * \param :(int err, char *buffer, size_t size
 *
 * \return : NA
***********************************************************************************************************************/
void GetErrorMessage(int err, char *buffer, size_t size)
{   
    #ifdef _GNU_SOURCE 
    strerror_r(err, buffer, size); 
    #endif 
    
    #if((_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && ! _GNU_SOURCE) 
    if(0!= strerror_r(err, buffer, size)) 
    { 
        strcpy(buffer, "Unknown Error"); 
    } 
    #endif 
}

/***********************************************************************************************************************
 *int ClearLog(void) description
 *
 * \clears the log file
 *
 * \param : NA
 *
 * \return : 0 : Success, -1 : failure
***********************************************************************************************************************/
int ClearLog(void)
{
	FILE *fp;
    fp = fopen(LogFileName,"w");
    if(fp == NULL)
    {
        fprintf(stderr, "LogFile creation failed...\n");
        perror("ClearLog");
        return -1;
    }
    
    close(fileno(fp));
    return 0;
}

/***********************************************************************************************************************
 * \char *GetTimeStamp(char *Date__Time) description
 *
 * \Get the current date and time and stores them in one string in the format Date__Time.
 *
 * \param : char *Date__Time_Stamp
 *
 * \return : NA
***********************************************************************************************************************/
char *GetTimeStamp(char *Date__Time)
{	
	time_t CurrentTime;				/* for holding the time since epoch */
	struct tm *tmptr;
	char DateString[20] = {'\0'};			/* for holding the Date */
	char TimeString[20] = {'\0'};			/* for holding the time */

	CurrentTime = time(0);				/* obtainig the current time */

	/* converting it into human understandable format */ 
	tmptr = localtime(&CurrentTime);		
	
	strftime(DateString, 20, "%F", tmptr);
	strftime(TimeString, 20, "%T", tmptr);

	/* Combining data and time to obtain a format Date__Time*/
	strcpy(Date__Time, DateString);
	strcat(Date__Time, "__");
	strcat(Date__Time, TimeString);
	return Date__Time;
}

/***********************************************************************************************************************
 * \void convert(time_t val, TimeStruct *ts) description
 *
 * \converts the number of seconds into days, hours, minutes and seconds.
 *
 * \param : time_t val, TimeStruct *ts
 *
 * \return : NA
***********************************************************************************************************************/
void convert(time_t val, TimeStruct *ts)
{
	ts->days = val/86400;  
	val = val%86400;
	ts->hours = val/3600;
	val = val%3600;
	ts->minutes = val/60;
	val = val%60;
	ts->seconds = val;  
}

/***********************************************************************************************************************
 * \void FreeCharBuffers(int num,...) description
 *
 * \frees the character buffers. after freeing those pointers are set to NULL
 *
 * \param : variable argument list. First has to be the number of arguments followed after it and the rest should be the
 * \        pointers to the character buffers.
 *
 * \return : NA
***********************************************************************************************************************/
void FreeCharBuffers(int num,...)
{
    va_list valist;
    int i = 0;
	char **str = NULL;
    /* initialize valist for num number of arguments */
    va_start(valist, num);

    /* access all the arguments assigned to valist */
    for (i = 0; i < num; i++)
    {
       str = (va_arg(valist, char **));
       free(*str);
	(*str) = NULL;	/*free and set it to NULL to avoid dangling pointers*/
    }
    /* clean memory reserved for valist */
    va_end(valist);
}

/*
END OF FILE
*/
