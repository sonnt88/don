/************************************************************************
  | FILE:         inc_perf_common_g4.c
  | PROJECT:      platform
  | SW-COMPONENT: INC
  |------------------------------------------------------------------------------
  | DESCRIPTION:   INC performance measurement common file 
  |                File contains the common functions and global data shared between   
  |                INC performance measurement commmand line apps & OEDT's 
  |------------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2014 Robert Bosch GmbH
  | HISTORY:
  | Date      | Modification                             | Author
  | 06.10.16  | Initial revision                         | Shahida Mohammed Ashraf 
  | --.--.--  | ---------------------                    | -------, -----
  |
  |*****************************************************************************/
#include <signal.h>
#include <sys/signal.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include "linux/input.h"
#include "dgram_service.h"
#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_input_device.h"
#include "inc_perf_common_g4.h"

#define PR_ERR(fmt, args...) \
{ fprintf(stderr, "inc_perf_common_g4: %s: " fmt, __func__, ## args); }

#define EXPCT_EQ(val1, val2, ret, fmt, args...) \
    if (val1 != val2) { \
        PR_ERR(fmt, ## args); \
        return ret; }

#define EXPCT_NE(val1, val2, ret, fmt, args...) \
    if (val1 == val2) { \
        PR_ERR(fmt, ## args); \
        return ret; }

#define MAX_STATS                       4
#define INIT                            0
#define PREVIOUS                        1
#define PRESENT                         2
#define AVERAGE                         3

#define RX_BYTES                        1
#define RX_PACKETS                      2 
#define TX_BYTES                        9
#define TX_PACKETS                      10

/*Threshhold values for x86 to x86 */
#define TH_RX_BYTES_SCC                 22151150
#define TH_TX_BYTES_SCC                 36446650  
#define TH_RX_PACKETS_SCC               10000
#define TH_TX_PACKETS_SCC               10000 

/* Structure to hold the througput figures */
typedef struct _throughput{
    unsigned long rx_packets;
    unsigned long tx_packets;
    unsigned long rx_bytes;
    unsigned long tx_bytes;
}throughput;

throughput stats[MAX_STATS] = {
    {0,0,0,0},
    {0,0,0,0},
    {0,0,0,0},
    {0,0,0,0}
};

/*
 * Function to tokenise and fetch the net device statistics
 */
int netdev_stats_handler(char *host,throughput* device_stats)
{
    FILE *fp ;
    char path[1035];
    unsigned long bytes[20];    
    int i = 0 ,flag = 0;
    int errsv;

    EXPCT_NE(device_stats, NULL, -1, "Invalid argument, device_stats\n");
	EXPCT_NE(host, NULL, -1, "Invalid argument, host\n");

    memset (bytes,0,sizeof(bytes));
    memset (path,0,sizeof(path));

    fp = fopen(COMMAND, "r");
    errsv = errno;
    EXPCT_NE(fp, NULL, -1, "fopen failed: %d\n", errsv);

    /* To check whether the host is present or not*/
    if (fgets(path, sizeof(path)-1, fp) == NULL)
    {
        PR_ERR("ERROR Host not found\n");
        return -1;
    }
    while (fgets(path, sizeof(path)-1, fp) != NULL) 
    {
        if(strstr(path,host) != NULL) 
        {
            flag = 1;
            char *p = strtok(path, " ");
            while(p != NULL) {
                bytes[i++] = strtol(p, NULL, 10);
                p = strtok(NULL, " ");
            }
        }
    }
    if (flag == 0)
    {
        PR_ERR("ERROR No such Host\n");
        return -1;
    }
    fclose(fp);

    device_stats->rx_packets = bytes[RX_PACKETS] ;
    device_stats->tx_packets = bytes[TX_PACKETS] ;
    device_stats->rx_bytes = bytes[RX_BYTES] ;
    device_stats->tx_bytes = bytes[TX_BYTES] ;
    return 0;
}

/* computes INC throughput figures */
int throughput_handler(char *host,unsigned long interval,unsigned long interval_count)
{ 
    throughput device_stats;    
    int ret;

    EXPCT_NE(host, NULL, -1, "Invalid argument, host\n");

    ret = netdev_stats_handler(host,&device_stats);
    if (ret != 0)
    {
        PR_ERR("ERROR netdev_stats_handler failed\n");
        return -1;
    }

    if (interval_count == 0)
    {
        stats[INIT] = device_stats;
    }
    else 
    {    
        stats[PREVIOUS] = stats[PRESENT];

        stats[PRESENT].rx_packets = device_stats.rx_packets - stats[INIT].rx_packets;
        stats[PRESENT].tx_packets = device_stats.tx_packets - stats[INIT].tx_packets;
        stats[PRESENT].rx_bytes = device_stats.rx_bytes - stats[INIT].rx_bytes;
        stats[PRESENT].tx_bytes = device_stats.tx_bytes - stats[INIT].tx_bytes;

        stats[AVERAGE].tx_packets = (stats[PRESENT].tx_packets) / (interval_count * interval);
        stats[AVERAGE].rx_packets = (stats[PRESENT].rx_packets) / (interval_count * interval);
        stats[AVERAGE].tx_bytes   = (stats[PRESENT].tx_bytes) / (interval_count * interval);
        stats[AVERAGE].rx_bytes   = (stats[PRESENT].rx_bytes) / (interval_count * interval);
    }
    return 0;
}

/* Prints throughput*/
void print_throughput(unsigned long interval,unsigned long count)
{

    unsigned long tx_bytes_l,rx_bytes_l,tx_pckts_l,rx_pckts_l;
    unsigned long tx_bytes_s,rx_bytes_s,tx_pckts_s,rx_pckts_s;

    if (count == 0 || interval == 0)
        return;

    tx_bytes_l = rx_bytes_l = tx_pckts_l = rx_pckts_l = 0;
    tx_bytes_s = rx_bytes_s = tx_pckts_s = rx_pckts_s = 0;

    tx_pckts_l = (stats[PRESENT].tx_packets - stats[PREVIOUS].tx_packets)/interval;
    rx_pckts_l = (stats[PRESENT].rx_packets - stats[PREVIOUS].rx_packets)/interval;
    tx_bytes_l = (stats[PRESENT].tx_bytes - stats[PREVIOUS].tx_bytes)/interval;
    rx_bytes_l = (stats[PRESENT].rx_bytes - stats[PREVIOUS].rx_bytes)/interval;

    tx_pckts_s = (stats[PRESENT].tx_packets) / (count * interval);
    rx_pckts_s = (stats[PRESENT].rx_packets) / (count * interval);
    tx_bytes_s = (stats[PRESENT].tx_bytes) / (count * interval);
    rx_bytes_s = (stats[PRESENT].rx_bytes) / (count * interval);

    printf ("%4ld\t%08ld\t%08ld\t%08ld\t%08ld\t%08ld\t%08ld\t%08ld\t%08ld\t%08ld\t%08ld\t%08ld\t%08ld\n",
            count,rx_pckts_l,tx_pckts_l,(tx_pckts_l+rx_pckts_l),rx_bytes_l,tx_bytes_l,(tx_bytes_l+rx_bytes_l),
            rx_pckts_s,tx_pckts_s,(tx_pckts_s+rx_pckts_s),rx_bytes_s,tx_bytes_s,(tx_bytes_s+rx_bytes_s));

}

/*Initialises the INC communication*/
int inc_communication_init(char * host,inc_comm *inccom)
{
    int ret,optval;
    int errsv,sockfd;
    int localportno,portno;
    struct hostent *local, *remote;
    struct sockaddr_in local_addr, remote_addr;
    char local_name[256];
    sk_dgram *dgram;

    EXPCT_NE(host, NULL, -1, "Invalid argument, host\n");
    EXPCT_NE(inccom, NULL, -1, "Invalid argument, inccom\n");

    /*For v850*/
    portno = localportno = 0xC700;

    sprintf(local_name, "%s-local", host);
    local = gethostbyname(local_name);
    errsv = errno;
    EXPCT_NE(local, NULL, 1, "gethostbyname() failed: %d\n", errsv);

    local_addr.sin_family = AF_INET;
    memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);
    local_addr.sin_port = htons(localportno);

    remote = gethostbyname(host);
    errsv = errno;
    EXPCT_NE(remote, NULL, 2, "gethostbyname (remote) failed: %d\n", errsv);

    remote_addr.sin_family = AF_INET;
    memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
    remote_addr.sin_port = htons(portno); 

    sockfd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
    errsv = errno;
    EXPCT_NE(sockfd, -1, 3, "create socket failed: %d\n", errsv);

    dgram = dgram_init(sockfd, DGRAM_MAX, NULL);
    EXPCT_NE(dgram, NULL, 4, "dgram_init failed\n");

    //lint -e64
    optval = 1; 
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));
    errsv = errno;
    EXPCT_NE(sockfd, -1, 5, "setsockopt failed: %d\n", errsv);

    ret = bind(sockfd, (struct sockaddr *) &local_addr, sizeof(local_addr));
    errsv = errno;
    EXPCT_EQ(ret, 0, 6, "bind failed: %d\n", errsv);

    ret = connect(sockfd, (struct sockaddr *) &remote_addr, sizeof(remote_addr));
    errsv = errno;
    EXPCT_EQ(ret, 0, 7, "connect failed: %d\n", errsv);

    inccom->dgram = dgram; 
    inccom->sockfd = sockfd;
    return 0;
}

/* Function to disconnect the INC connection*/
int inc_communication_exit(inc_comm *inccom)
{
    int ret, errsv;

    EXPCT_NE(inccom, NULL, -1, "Invalid argument, inccom\n");

    ret = dgram_exit(inccom->dgram);
    errsv = errno;
    EXPCT_EQ(ret, 0, 1, "dgram_exit failed: %d\n", errsv);

    close(inccom->sockfd);

    return 0;
}

/*Evaluates the throughput with threshold values */
int inc_threshold_evaluate(char *host)
{

    unsigned long tx_bytes,rx_bytes,tx_pckts,rx_pckts;
    int res = 0;

    EXPCT_NE(host, NULL, -1, "Invalid argument, host\n");

    tx_pckts = stats[AVERAGE].tx_packets;
    rx_pckts = stats[AVERAGE].rx_packets;
    tx_bytes = stats[AVERAGE].tx_bytes;
    rx_bytes = stats[AVERAGE].rx_bytes; 

	/* For V850*/
    if ((tx_pckts < TH_TX_PACKETS_SCC) || (rx_pckts < TH_RX_PACKETS_SCC) || (rx_bytes < TH_RX_BYTES_SCC) || (tx_bytes < TH_TX_BYTES_SCC))
    {
        res = 200;
    }
	
    return res;
}
