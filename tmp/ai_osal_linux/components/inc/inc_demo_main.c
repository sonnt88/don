/************************************************************************
| FILE:         inc_demo_main.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  PC tool for simulating INC messages
|				and demonstration of Gen4 INC.
|				Refer Readme.txt for usage of INC PC tool
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2016 Robert Bosch GmbH| HISTORY:
| Date      | Modification               | Author
| 24.01.13  | Initial revision           | Mai Daftedar
| 17.10.16	| Fix RADAR and lint issues	 | Venkatesh Parthasarathy
| --.--.--  | ----------------           | -------, -----
|*****************************************************************************/

#include "inc_demo.h"



#ifdef OSAL_GEN4

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/time.h>
#include <time.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>


/*Global Variables*/
int bufflen = DEFAULT_LEN;
int closesocket = 0;

/*server structure to define the parameters defined*/
serverparam str_serverparam;
clientparam str_clientparam;

enuTxRxSpeed category;
int stress_test=0,autotest=0,verflag=0,speedflag=0,delay=0,mode=0;
/***********************************************************************************************
*void shutdowninterrupt(int sig)
*
* PURPOSE : Interrupt to be processed when shutdown signal is received
*
* RETURN :  None
*                       
* Author :  
***********************************************************************************************/
void shutdowninterrupt(int sig)
{
	closesocket = sig;  
}

/***********************************************************************************************
*void inc_handleblockmode(int sockid, sk_dgram *socketdgram)
*
* PURPOSE : Handling of different test cases for the blocking socket types
*
* RETURN :  None
*                       
* Author :  
***********************************************************************************************/
void inc_handleblockmode(int sockid, sk_dgram *socketdgram)
{
	
	if(stress_test == 1)
	{
		PR_MSG("RUNNING: Running Stress test for all connected clients\n");
		inc_delaysendrecv(sockid,bufflen,buffer,delay,category,CONTINOUSTEST,socketdgram);		
	}
	if(autotest == 1)
	{	
		PR_MSG("RUNNING: ECHO TEST\n");
		inc_echotest(sockid,bufflen,mode,pattern,CONTINOUSTEST,socketdgram);	
	}
	if(verflag == 1)
	{
		PR_MSG("RUNNING: Verification of data recieved\n");
		/*call verification function based on the mode of the test running*/	
		switch(mode)
		{
			case FIXED:
			inc_verifypattern(bufflen,buffer,strlen(FIXED_PATTERN),FIXED_PATTERN);
			break;
			case ALTERNATE:
			inc_verifypattern(bufflen,buffer,strlen(ALTERNATE_PATTERN),ALTERNATE_PATTERN);
			break;
			case PREDEFINED:
			inc_verifypattern(bufflen,buffer,strlen(pattern),pattern);
			break;
		}
	}
	if(speedflag)
	{
		PR_MSG("RUNNING: Running speed test\n");
		inc_delaysendrecv(sockid,bufflen,buffer,delay,category,CONTINOUSTEST,socketdgram);	
	}
	
}

/***********************************************************************************************
*int main(int argc, char *argv[])
*
* PURPOSE : This is the main process to handle all the command line user parameters
*
* RETURN :  
*                       
*
* NOTES :   
*          
* Author :  
***********************************************************************************************/
int main(int argc, char *argv[])
{
	int portno = 0,localportno,socktype,multiclient=0;
	/*Variable to identify the test number and flag for the automatic test cases to stress test*/
	int sockfd;
    	int opt, optmsk;
	int errsv;
    	int i;
	/*Default testing parameter setting*/
	mode = FIXED;
	memset(pattern,0,PATTERN_LENGTH);
	memset(buffer,0,DEFAULT_MSGSZ+1);
	operationMode = CLIENT;
	socktype = BLOCKING_SOCKET;
	/*Initialize client incrementor*/
	client_number = 0;
	clientstate = IDLE;
	category = DEFAULT;
	/*Signaction used when shutting down the running application*/
	struct sigaction sa;
	
	/*Defining the threads used for testing*/
	pthread_t serverthread;
	pthread_t eventchkthread;
	
	/*epoll structure*/
	epollstruct strepollstruct;

	 /*Assume that eth0 is the interface name added with the server address in the etc/hosts and the eth0-local is the 
	 the local address with local-address
	 Not needed when in server mode
	 */
	strcpy(intf_name,"eth0");
	
	/*Parsing input arguments*/
	/*b: Defining a client port number*/
	/*I: Interface, define the interface that the test will operate with*/
	/*O: Opernation Mode, C or S for client or server respectively*/
	/*p: Client port number*/
	/*S: MultiClient mode enabled*/
	/*L: Define the length of the buffer to be sent out*/
	/*M: Mode, to choose between three modes, fixed pattern, alternate pattern, and Pre-Defined Pattern */
	/*P: Pattern, define the sequence that is required to be sent*/
	/*V: Verification flag to verify the data being sent*/
	/*D: Delay for fast/slow sending and receiving*/
	/*C: Category for speed testing, slow/fast receiving and sending*/
	/*T: Socket Type: Blocking (B) or NonBlocking(NB)*/

	for (optmsk = opterr = 0; (opt = getopt(argc, argv, "p:b:I:S:O:L:M:P:V:C:D:T:G:")) != -1;) {
		switch (opt) {
		case 'I':
			strcpy(intf_name,optarg);
			break;
		case 'S':
			PR_MSG("Server mode supported\n");
			multiclient = atoi(optarg);
			break;
		case 'O':
			if(!strcmp(optarg,"client"))
			{
				operationMode = CLIENT;
			}
			else if(!strcmp(optarg,"server"))
			{
				operationMode = SERVER;
			}
			else
			{
				PR_ERR("Invalid Operational mode please specify either client or server modes only\n");
				goto ERR;
			}
			break;
		case 'p':
			portno = atoi(optarg);
			break;
			/*Defining a client portnumber will be usefull to define a unique connection with a multiclient
			server conenction*/
		case 'b':
			localportno = atoi(optarg);
			break;
		/*Define Echo test with the length of data and type of Mode*/
		case 'L':
			bufflen = atoi(optarg);
			break;
		 /*Mode: 1-FIXED PATTERN 2-ALTERNATE PATTERN, 3-PRE_DEFINED PATTERN*/
		case 'M':
			 autotest = 1;
			 PR_MSG("Running Echo Test\n");
			 mode =  atoi(optarg);
		    break;
		case 'P':
			/*Maximum allowed size is */
			if(mode == PREDEFINED)
			{
				strcpy(pattern,optarg);
			}
			else
				PR_MSG("Choose Mode: 3 to use the predefined pattern\n");
			 break;
		case 'V':
			verflag = 1;
			break;
		case'D':
			delay = atoi(optarg);
			break;
		case 'C':
			speedflag = 1;
			if(!strcmp(optarg,"send"))
			{
				PR_MSG("SENDER with possible delay\n");
				category = SENDER;
			}
			else if(!strcmp(optarg,"recv"))
			{
				PR_MSG("RECV with possible delay\n");
				category = RECV;
			}
			else
			{
				PR_MSG("Default\n");
				category = DEFAULT;
			}
			break;
		case 'T':
			PR_MSG("Handling Blocking/NonBlocking Sockets\n");
			if(!strcmp(optarg,"NB"))
			{
				PR_MSG("Nonblocking\n");
				socktype = NONBLOCKING_SOCKET;
			}
			if(!strcmp(optarg,"B"))
			{
				PR_MSG("Blocking\n");
				socktype = BLOCKING_SOCKET;
			}
			break;
		case 'G': /*Stress Test*/
			stress_test = 1;
			PR_MSG("Running Stress Test\n");
			bufflen = atoi(optarg);
			break;
		default:
			PR_ERR("invalid option Err:%d Argc:%d Arg:%c\n",opterr,argc,opt);
		}
	}
	
	/*Checking parameters passed in are within the expected range*/
	if(portno == 0 || portno < MIN_PORT_NUM || portno > MAX_PORT_NUM)
	{
		errsv = ER_INVALID_PORT;
		PR_ERR("invalid port - use option -p for localport and -b for remoteport number\n");
		goto ERR;
	}
	
	if(bufflen > DEFAULT_MSGSZ)
	{
		errsv = ER_EXCEED_SIZE;
		PR_ERR("Exceed maximum buffer length\n");
		goto ERR;
	}

	PR_MSG("***************************************************************\n");
	PR_MSG("Mode %s, port %d, delay %d, size %d\n", operationMode?"server":"client", portno, delay, bufflen);
	if(socktype == NONBLOCKING_SOCKET)
	{
		PR_MSG("SocketType: Nonblocking\n");
	}
	else
	{
		PR_MSG("SocketType: Blocking\n");
	}

	PR_MSG("Verification: %s\n", verflag?"Enabled":"Disabled");
	PR_MSG("***************************************************************\n");

	/*Register interrupt if Ctrl-C is used to close the running application*/
	memset(&sa, 0, sizeof(struct sigaction));
    	sa.sa_handler = shutdowninterrupt;
    	sa.sa_flags = 0;
    	sigemptyset (&(sa.sa_mask));
	
	if(sigaction (SIGINT, &sa, NULL)!= 0)
    	{
     		PR_ERR("ERROR: sigaction failed");
      		goto ERR;
	}

	/*Socket creation*/
	sockfd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
	if (sockfd < 0) 
	{
		errsv = errno;
		PR_ERR("ERROR opening socket: %d %s\n", errsv, strerror(errsv));
		goto ERR;
	}
	
	if(socktype == NONBLOCKING_SOCKET)
	{
		/* Creating a shared epoll file descriptors for the clients connected*/
		strepollstruct.epfd = epoll_create(MAX_NUM_CLIENT);
   
		/* Allocate enough memory to store all the events in the "events" structure*/
		if (NULL == (strepollstruct.events = calloc(MAX_NUM_CLIENT, sizeof(struct epoll_event))))
		{
			errsv = errno;
			PR_ERR("ERROR allocating sockets: %d %s\n", errsv, strerror(errsv));
			goto ERR;
		}
		/*Create two threads one for error checking and one for send and receiving data for now
		till we use the epoll subsystem*/
		if( 0!= pthread_create (&eventchkthread, NULL, (void *)eventhandle, &strepollstruct) )
		{
			errsv = errno;
			PR_ERR("Event Handling Thread create error  %d %s\n", errsv, strerror(errsv));
			goto ERR;
		}
		
	}
	if(operationMode == SERVER)
	{
		/*Prepare server parameters */
		str_serverparam.sockfd = sockfd;
		str_serverparam.portno = portno;
		str_serverparam.multiclient = multiclient;
		str_serverparam.socktype = socktype;
		
		
		if(socktype == NONBLOCKING_SOCKET)
		{
			str_serverparam.epfd = strepollstruct.epfd;
		
			if(0!= pthread_create(&serverthread, NULL, (void *)startserver, &str_serverparam))
			{
				errsv = errno;
				PR_ERR("Server start thread create error  %d %s\n", errsv, strerror(errsv));
				goto ERR;
			}
		}
		else
		{
			startserver(&str_serverparam);
		}
	}
	else 
	{	

		/*Prepare client paramaters*/
		str_clientparam.sockfd = sockfd;
		str_clientparam.portno = portno;
		str_clientparam.localportno = localportno;
		str_clientparam.socktype = socktype;
		str_clientparam.mode = mode;
		str_clientparam.pattern = pattern;
		
		
		
		if(socktype == NONBLOCKING_SOCKET)
		{
			errsv = clientstart(&str_clientparam,strepollstruct.epfd);
		}
		else
		{
			errsv = clientstart(&str_clientparam,0);
		}
		if(errsv)
		{
			PR_MSG("Error in starting client: %i\n",errsv);
			goto ERR;
		}
		
	}
   
	while(!closesocket && socktype == BLOCKING_SOCKET)
	{
		if(operationMode == CLIENT && clientstate == CONNECTED)
		{
			inc_handleblockmode(str_clientparam.sockfd,dgram);
			
		
		}
	}
	while(socktype == NONBLOCKING_SOCKET && !closesocket)
	{
		pause();/* Suspend program until signal received */
	}
	
ERR:
	PR_MSG("Shuting down \n");
	#ifdef USE_DGRAM_SERVICE
	/*If dgram has been initialized then call dgram_exit, if dgram hasn't been initialized, there would not be any need to 
	go through this*/
	if (operationMode == CLIENT)
	{
		if (dgram != NULL)
		{
			if (dgram_exit(dgram) < 0)
				PR_ERR("dgram_exit failed\n");
		}
		clientstate = DISCONNECTED;
	}
	else if (operationMode == SERVER)
	{
		
		for (i = 0;i < client_number;i++)
		{
			if (strlistenerlst[i].dgram != NULL)
			{
				if (dgram_exit(strlistenerlst[i].dgram) < 0)
				PR_ERR("dgram_exit failed\n");
			}
			strlistenerlst[client_number].state = DISCONNECTED;
		}
		
		
	}
	#endif
	
	if(sockfd)
	{
		close(sockfd);
		
	}

	return errsv;
}

#endif
