/************************************************************************
| FILE:         inc_scc_input_device.h
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Definitions for Inter Node Communication
|               SCC Input Device service
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 08.03.13  | Initial revision           | tma2hi
| 27.06.13  | Add support for protocol v2| tma2hi
| 06.06.14  | Add MT_SLOTS message id    | tma2hi
|
|*****************************************************************************/
#ifndef INC_SCC_INPUT_DEVICE_H
#define INC_SCC_INPUT_DEVICE_H

#define SCC_INPUT_DEVICE_C_COMPONENT_STATUS_MSGID  0x20
#define SCC_INPUT_DEVICE_R_COMPONENT_STATUS_MSGID  0x21
#define SCC_INPUT_DEVICE_R_REJECT_MSGID            0x0B
#define SCC_INPUT_DEVICE_C_CONFIG_START_MSGID      0x30
#define SCC_INPUT_DEVICE_R_CONFIG_START_MSGID      0x31
#define SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID       0x33
#define SCC_INPUT_DEVICE_R_CONFIG_ABS_MSGID        0x35
#define SCC_INPUT_DEVICE_R_CONFIG_MT_SLOTS_MSGID   0x39
#define SCC_INPUT_DEVICE_R_CONFIG_END_MSGID        0x37
#define SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID        0x51
#define SCC_INPUT_DEVICE_R_EVENT_DEV1_MSGID        0x53
#define SCC_INPUT_DEVICE_R_EVENT_DEV2_MSGID        0x55
#define SCC_INPUT_DEVICE_R_EVENT_DEV3_MSGID        0x57
#define SCC_INPUT_DEVICE_R_EVENT_DEV4_MSGID        0x59
#define SCC_INPUT_DEVICE_R_EVENT_DEV5_MSGID        0x5B
#define SCC_INPUT_DEVICE_R_EVENT_DEV6_MSGID        0x5D
#define SCC_INPUT_DEVICE_R_EVENT_DEV7_MSGID        0x5F
#define SCC_INPUT_DEVICE_R_EVENT_DEV8_MSGID        0x61
#define SCC_INPUT_DEVICE_R_EVENT_DEV9_MSGID        0x63
#define SCC_INPUT_DEVICE_R_EVENT_DEV10_MSGID       0x65
#define SCC_INPUT_DEVICE_R_EVENT_DEV11_MSGID       0x67
#define SCC_INPUT_DEVICE_R_EVENT_DEV12_MSGID       0x69
#define SCC_INPUT_DEVICE_R_EVENT_DEV13_MSGID       0x6B

#endif /* INC_SCC_INPUT_DEVICE_H */
