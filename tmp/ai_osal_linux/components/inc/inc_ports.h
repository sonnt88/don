/************************************************************************
| FILE:         inc_ports.h
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Definitions for Inter Node Communication - ports
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 04.02.13  | Initial revision           | tma2hi
| 21.02.13  | Add new ports 0x09..0x14   | tma2hi
| 17.07.13  | Sync to IncCom spec v1.30  | tma2hi
| 11.09.13  | Sync to IncCom spec v1.57  | tma2hi
| 14.10.13  | Sync to IncCom spec v1.60  | tma2hi
| 20.01.16  | Bugfix for CMG3GB-2898     | Shahida Mohammed Ashraf
| 15.02.16  | Sync to IncCom spec v2.29  | Shahida Mohammed Ashraf
| --.--.--  | ----------------           | ------------
|
|*****************************************************************************/
#ifndef INC_PORTS_H
#define INC_PORTS_H

#define PORT_OFFSET        0xC700

#define ECHO_PORT                     (0x00 | PORT_OFFSET)
#define SPM_PORT                      (0x01 | PORT_OFFSET)
#define PORT_EXTENDER_GPIO_PORT       (0x02 | PORT_OFFSET)
#define PORT_EXTENDER_ADC_PORT        (0x03 | PORT_OFFSET)
#define PD_NET_PORT                   (0x04 | PORT_OFFSET)
#define DIA_UDD_PORT                  (0x05 | PORT_OFFSET)
#define SENSORS_PORT                  (0x06 | PORT_OFFSET)
#define DLT_PORT                      (0x07 | PORT_OFFSET)
#define GNSS_PORT                     (0x08 | PORT_OFFSET)
#define WDG_PORT                      (0x09 | PORT_OFFSET)
#define PORT_EXTENDER_PWM_PORT        (0x0A | PORT_OFFSET)
#define DOWNLOAD_PORT                 (0x0B | PORT_OFFSET)
#define THERMAL_MANAGEMENT_PORT       (0x0C | PORT_OFFSET)
#define SUPPLY_MANGEMENT_PORT         (0x0D | PORT_OFFSET)
#define NET_CTRL_PORT                 (0x0E | PORT_OFFSET)
#define NET_BROADCAST_PORT            (0x0F | PORT_OFFSET)
#define NET_TP0_PORT                  (0x10 | PORT_OFFSET)
#define NET_TP1_PORT                  (0x11 | PORT_OFFSET)
#define NET_TP2_PORT                  (0x12 | PORT_OFFSET)
#define NET_TP3_PORT                  (0x13 | PORT_OFFSET)
#define INPUT_DEVICE_PORT             (0x14 | PORT_OFFSET)
#define DIA_EVENTMEMORY_PORT          (0x15 | PORT_OFFSET)
#define PRJ_COMP2_PORT                (0x16 | PORT_OFFSET)
#define SYSTEM_STATEMACHINE_PORT      (0x17 | PORT_OFFSET)
#define DIMMING_PORT                  (0x1B | PORT_OFFSET)
#define NET_TP4_PORT			      (0x1C | PORT_OFFSET)
#define NET_TP5_PORT                  (0x1D | PORT_OFFSET)
#define NET_TP6_PORT                  (0x1E | PORT_OFFSET)
#define NET_TP7_PORT                  (0x1F | PORT_OFFSET)
#define BAP_00_PORT                   (0x20 | PORT_OFFSET)
#define BAP_01_PORT                   (0x21 | PORT_OFFSET)
#define PRJ_COMP3_PORT				  (0x22 | PORT_OFFSET)
#define EARLY_AUDIO_PORT              (0x24 | PORT_OFFSET)
#define ADR3CTRL_PORT                 (0x25 | PORT_OFFSET)
#define TTFIS_PORT                    (0x26 | PORT_OFFSET)
#define SECURITY_PORT                 (0x27 | PORT_OFFSET)
#define RTC_PORT                      (0x28 | PORT_OFFSET)
#define PRJ_COMP_PORT                 (0x29 | PORT_OFFSET)
#define GNSS_FW_UPDATE_PORT           (0x2A | PORT_OFFSET)
#define EARLY_APP_PORT                (0x2B | PORT_OFFSET)
#define ERROR_MEMORY_PORT             (0x2C | PORT_OFFSET)
#define ENGINEERING_MENU_PORT         (0x2D | PORT_OFFSET)
#endif /* INC_PORTS_H */
