//#define SSI_OLD_INTERFACE
/*******************************************************************************
 * \file          ssi_if.h
 * \brief         SSI interface header
 *
 * \see           \\bosch.com\dfsrb\DfsDE\DIV\CM\DI\Projects\Common\IPCA\SSI_Spec\Valid\SSI_2v00.pdf
 *
 * \project       GEN2 platform
 *
 * \authors       Markus Pr�hl
 *
 * COPYRIGHT      (c) 2012 Bosch Car Multimedia GmbH
 *
 * \history_begin
 * see ClearCase
 * \history_end
 *
*/
#ifndef SSI_IF_HEADER
#define SSI_IF_HEADER

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*******************************************************************************
 * includes:
 ******************************************************************************/
/*-- none --*/

/*******************************************************************************
 * defines and macros (scope: file local)
 ******************************************************************************/
#define SSI_C_NO_ERROR        ((tS32)( 0))
#define SSI_C_GENERAL_ERROR   ((tS32)(-1))

/*******************************************************************************
 * typedefs (scope: file local)
 ******************************************************************************/
typedef enum
{
  SSI_INSTANCE_ADR3,
  SSI_NUMBER_OF_INSTANCES

}enSsiInstance; /**< number of used SPI interfaces */

/* ------------------------------------------------------------------------- */
typedef enum
{
  SSI_C_CONF_FAULT,             /**<  negativ data confirmation */
  SSI_C_CONF_OKAY               /**<  positiv data confirmation */
}enSsiConfState;  /**< SSI data confirmation state */

/* ------------------------------------------------------------------------- */
typedef enum
{
  SSI_C_RECEIVE_READY,          /**<  Receiver state is receive ready */
  SSI_C_RECEIVE_NOT_READY       /**<  Receiver state is receive not ready */
}enSsiReceiverState;  /**< SSI data confirmation state */

/* ------------------------------------------------------------------------- */
// declare a typedef for a function pointer
typedef void (*ssi_tPVFNDataCon) ( void *pvHandle, enSsiConfState enState);
typedef void (*ssi_tPVFNDataInd) ( void *pvHandle, tU8 *pu8Data, tU32 u32Length);

/* ------------------------------------------------------------------------- */
typedef struct
{
  enSsiInstance         enInstance;         /**< e.g. ADR3, FGS, ...*/
  tU8                   u8Lun;              //LUN
  ssi_tPVFNDataCon      pvfnCallbackDataCon;//Data confirmation callback
  ssi_tPVFNDataInd      pvfnCallbackDataInd;//Data indication callck
  tU32                  u32MaxTxLength;     //length of transmit buffer
  tU32                  u32MaxRxLength;     //lenght of receive buffer
  tBool                 fXoffByApplicationDefault;// TRUE-> After SSI_s32OpenChannel() the channel is in Xoff state 
}ssi_tChannelConfig;

/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */

#ifdef SSI_OLD_INTERFACE
typedef enum
{
  LLD_SSI_INSTANCE_ADR3,
  LLD_SSI_NUMBER_OF_INSTANCES

}enLldSsiInstance; /**< number of used SPI interfaces */

/* ------------------------------------------------------------------------- */
typedef enum
{
  LLD_SSI_C_CONF_FAULT,             /**<  negativ data confirmation */
  LLD_SSI_C_CONF_OKAY               /**<  positiv data confirmation */
}enLldSsiConfState;  /**< SSI data confirmation state */

/* ------------------------------------------------------------------------- */
// declare a typedef for a function pointer
typedef void (*lld_ssi_tPVFNDataCon) ( void *pvHandle, enLldSsiConfState enState);
typedef void (*lld_ssi_tPVFNDataInd) ( void *pvHandle, tU8 *pu8Data, tU32 u32Length);

/* ------------------------------------------------------------------------- */
typedef struct
{
  enLldSsiInstance      enInstance;         /**< e.g. ADR3, FGS, ...*/
  tU8                   u8Lun;              //LUN
  lld_ssi_tPVFNDataCon  pvfnCallbackDataCon;//Data confirmation callback
  lld_ssi_tPVFNDataInd  pvfnCallbackDataInd;//Data indication callck
  tU32                  u32MaxTxLength;     //length of transmit buffer
  tU32                  u32MaxRxLength;     //lenght of receive buffer
  tBool                 fXoffByApplicationDefault;// TRUE-> After SSI_s32OpenChannel() the channel is in Xoff state 
}lld_ssi_tChannelConfig;
#endif /*SSI_OLD_INTERFACE*/
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */

/*******************************************************************************
 * variable definition (scope: file local)
 ******************************************************************************/
/*-- none --*/

/*******************************************************************************
 * function prototypes (scope: global)
 ******************************************************************************/
tS32 SSI_s32OpenChannel( void **pvHandle, ssi_tChannelConfig *prChannelConfig);
tS32 SSI_s32CloseChannel( void **pvHandle);
tS32 SSI_s32DataReq( void *pvHandle, tU8 *pu8Data, tU32 u32Length);
tS32 SSI_s32DataIndProcessed( void *pvHandle);
tS32 SSI_s32SetReceiverState( void *pvHandle, enSsiReceiverState rSsiReceiverState);

#ifdef SSI_OLD_INTERFACE
tS32 lld_ssi_s32OpenChannel( void **pvHandle, lld_ssi_tChannelConfig *prChannelConfig);
tS32 lld_ssi_s32CloseChannel( void **pvHandle);
tS32 lld_ssi_s32DataReq( void *pvHandle, tU8 *pu8Data, tU32 u32Length);
tS32 lld_ssi_s32DataIndProcessed( void *pvHandle);
#endif /*SSI_OLD_INTERFACE*/

/*******************************************************************************
 * variable definition (scope: global)
 ******************************************************************************/
/*-- none --*/

/*******************************************************************************
 * constants (scope: file local)
 ******************************************************************************/
/*-- none --*/

/*******************************************************************************
 * function implementation (scope: file local = static)
 ******************************************************************************/
/*-- none --*/

/*******************************************************************************
 * function implementation (scope: global)
 ******************************************************************************/
/*-- none --*/

#ifdef __cplusplus
}  /* extern "C" */
#endif /* __cplusplus */

#endif /*SSI_IF_HEADER */
