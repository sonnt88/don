#include "OsalConf.h"

#define SYSTEM_S_IMPORT_INTERFACE_MAP
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include <stl_pif.h>
using namespace bpstl;
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"


#include "Linux_osal.h"

#include <ctype.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>


//#include "dev_tcp.h"
#include "ssi_if.h"  // ...\ai_projects\components\devices\dev_ssi\....

#define INVALID_SOCKET        -1
#define SOCKET_ERROR          -1

#define SSI_MAX_NAME_LEN 100
#define ADR3_SOCK_ASSERT_RETURN(expr)           \
    if (!(expr)) {                              \
        while(1) {}                             \
        NORMAL_M_ASSERT_ALWAYS();               \
        return;                                 \
    }

#define ADR3_SOCK_ASSERT_RETURN_VAL(expr, retval)     \
    if (!(expr)) {                              \
        while(1) {}                             \
        NORMAL_M_ASSERT_ALWAYS();               \
        return (retval);                        \
    }


static void  *_pvHandleAdr;	//handle for connction
static bool bIsTcpOpen = false;
void adr3_sock_dummyFn() {
};

class adr3_sem {
 public:
    adr3_sem(tVoid) {
        _hSem = OSAL_C_INVALID_HANDLE;
    }

    ~adr3_sem(tVoid) {
        vClose();
        _hSem = OSAL_C_INVALID_HANDLE;
    }

	tBool bIsOpen() {
      return _hSem != OSAL_C_INVALID_HANDLE;
	}

    tVoid vOpen(char const *sName, tU32 u32InitVal=0) {
        _oSemName = sName;
        if (_hSem != OSAL_C_INVALID_HANDLE) {
            return;
        }
        if(OSAL_ERROR  == OSAL_s32SemaphoreCreate(/*tCString*/        sName,
                                                  /*OSAL_tSemHandle*/ &_hSem,
                                                  /*uCount*/          (tU32)u32InitVal))
        {
            printf("adr3_sem:vOpen: OSAL_s32SemaphoreCreate (%s) ERROR", sName);
            _hSem = OSAL_C_INVALID_HANDLE;	
        }
    }
    
    tVoid vClose(tVoid) {
        if (_hSem == OSAL_C_INVALID_HANDLE) {
            return;
        }
        
        if(OSAL_ERROR  == OSAL_s32SemaphoreClose(_hSem))
        {
            
            printf("adr3_sem:vClose:OSAL_s32SemaphoreClose (%s) ERROR", _oSemName.c_str());
            _hSem = OSAL_C_INVALID_HANDLE;
            return;
        }
        _hSem = OSAL_C_INVALID_HANDLE;

        if(OSAL_ERROR  == OSAL_s32SemaphoreDelete(_oSemName.c_str()))
        {               
            printf("adr3_sem:vClose:OSAL_s32SemaphoreDelete (%s) ERROR", _oSemName.c_str());
            
        }
    }

    tVoid vGet() {
        if (_hSem == OSAL_C_INVALID_HANDLE) {
            return;
        }
        tS32 s32Ret = OSAL_s32SemaphoreWait( _hSem, OSAL_C_U32_INFINITE );
        if (OSAL_OK != s32Ret) {
            printf("adr3_sem:vGet: failed for Sem %s", _oSemName.c_str());
        }
    }

    tVoid vPost() {
        if (_hSem == OSAL_C_INVALID_HANDLE) {
            return;
        }
        tS32 s32Ret = OSAL_s32SemaphorePost( _hSem );
        if (OSAL_OK != s32Ret) {
            printf("adr3_sem:vPost: failed for Sem %s", _oSemName.c_str());
        }     
    }
    
private:
    OSAL_tSemHandle _hSem;
    string _oSemName;
};


#define ADR3_SOCK_MAX_RX_LEN 2048

struct trChnData    {
    tU32                     _u32Fd;
    ssi_tChannelConfig   _rSsiCfg;
    adr3_sem _hRxSem;
    adr3_sem _hTxSem;
    adr3_sem _hTermSem;
    adr3_sem _hLockSem;
    OSAL_tThreadID _hRxThreadId;
    OSAL_tThreadID _hTxThreadId;
    tBool _bRxReady;
    typedef enum {
        enState_Open,
        enState_Running,
        enState_Terminating,
        enState_Terminated,
        enState_Null,
        enState_Invalid
    } tenState;

    tenState _enState;

    tenState enGetState() {
        return _enState;
    }

    tBool bIsRunning() {
        return (_enState==enState_Open || _enState==enState_Running);
    }

    tU8 _au8RxBuf[ADR3_SOCK_MAX_RX_LEN]; // bin
    tU32 _u32RxBufLen;
    tChar _szTxBuf[ADR3_SOCK_MAX_RX_LEN*2]; // char
    tU32 _u32TxBufLen;

    int  _sock;
    char    _szHost[256];
    tU16    _Port;
    char    _szAutoStartupMsg[256];

    trChnData(tU32 u32Fd=0, ssi_tChannelConfig rSsiCfg=ssi_tChannelConfig()) :
        _u32Fd(u32Fd),
        _rSsiCfg(rSsiCfg),
        _bRxReady(FALSE),
        _enState(enState_Null),
        _u32RxBufLen(0),
        _u32TxBufLen(0),
        _Port(0)
    {
        _sock = INVALID_SOCKET;
        _szHost[0]=0;
        _szAutoStartupMsg[0]=0;
        char szSemName[SSI_MAX_NAME_LEN];
        _hRxSem.vOpen(szGetName(szSemName, "rxSem"));
        _hTxSem.vOpen(szGetName(szSemName, "txSem"));
        _hTermSem.vOpen(szGetName(szSemName, "termSem"));
        _hLockSem.vOpen(szGetName(szSemName, "lockSem"), 1);
        if (!bGetConfig()) {
            return;
        }
        struct hostent *ho1 = NULL;
        unsigned long hostip;
        if ((hostip=inet_addr(_szHost)) == INADDR_NONE) {
            return;
        }
        _sock = socket(AF_INET, SOCK_STREAM, 0);
        if (_sock == INVALID_SOCKET)
            return;
        
        struct sockaddr_in sin;
        sin.sin_addr.s_addr = hostip;
        sin.sin_family      = (short)AF_INET;
        sin.sin_port        = htons((tU16)_Port);;
        // socket verbinden
        if (connect(_sock, (struct sockaddr *)&sin, sizeof(sin)) == SOCKET_ERROR)
        {
            close(_sock);
            _sock = INVALID_SOCKET;
        }
        _enState=enState_Open;
    }

    ~trChnData() {
        _enState=enState_Terminating;
        close(_sock);
        _hRxSem.vPost();
        _hTermSem.vGet();
        _hTxSem.vPost();
        _hTermSem.vGet();


        _hRxSem.vClose();
        _hTxSem.vClose();
        _hTermSem.vClose();
        _hLockSem.vClose();
        _enState=enState_Terminated;
        
    }

    tBool bStart() {
        if (enGetState()!=enState_Open) {
            return FALSE;
        }
        _enState=enState_Running;

        tChar *pBuf=_szTxBuf;
        pBuf+=sprintf(pBuf, "SSI32.open CH=0 LUN=%d RXLEN=%d\r", u8GetLun(), _rSsiCfg.u32MaxRxLength);
        _u32TxBufLen=(tU32)strlen((char *)_szTxBuf);
    
        _hTxSem.vPost();

        if (_szAutoStartupMsg[0]!=0) {
            // client-id=0xFFFF
            // fblock-id=0x0043
            // u8InstanceId=0x01
            // u16FktId=0x0f01
            // opType=0x0c
            // meca-Len=0x0000
            tU32 u32MsgLen=(tU32)strlen(_szAutoStartupMsg)+1;
            bStoreRxPkt(_szAutoStartupMsg, &u32MsgLen);
            _bRxReady=TRUE;
            _rSsiCfg.pvfnCallbackDataInd((tVoid *)u8GetLun(), _au8RxBuf, _u32RxBufLen);

        }
        else {
            _hRxSem.vPost();
        }
        //        _rSsiCfg.pvfnCallbackDataCon((tVoid *)u8GetLun(), SSI_C_CONF_OKAY);
        return TRUE;
    }
    tU8 u8GetLun() {
        return _rSsiCfg.u8Lun;
    }

    tBool bGetConfig() {
        tChar szArgs[200];
        const char *szHome=getenv("HOME");
        strcpy(szArgs,szHome);
        strcat(szArgs,"/osal_pure_device_args.ini");
        tBool bFoundHost=FALSE;
        tBool bFoundPort=FALSE;
        FILE *fp = fopen(szArgs,"r");
        if (fp==NULL) {
            return FALSE;
        }
       char buf[500]; char *s;
       while(fgets(buf,sizeof(buf),fp) != NULL) {
           char *szToken=buf;
           while (*szToken) {
               if (*szToken=='\t') {
                   *szToken=' ';
               }
               if (*szToken=='\r' || *szToken=='\n') {
                   *szToken=0;
                   break;
               }
               szToken++;
           }
           szToken=buf;
           while(*szToken == ' ')
               szToken++;
            if (*szToken == '#')
                continue;
            szToken = strstr(buf,"/dev/adr3");
            if (szToken==NULL) {
                continue;
            }
            szToken = strstr(buf,":");
            if (szToken==NULL) {
                continue;
            }
			szToken++;
            while (*szToken==' ') {
                szToken++;
            }
            char *pArg=strstr(szToken, "host=");
            if (pArg != NULL) {
                pArg+=strlen("host=");
                tChar *szHost=_szHost;
                while(*pArg!=0 && *pArg!=' ') {
                    *szHost++=*pArg++;
                }
                *szHost='\0';
                bFoundHost=TRUE;
                if (strcmp(_szHost,"localhost") == 0)                     // to prevent proxy problems
                    strcpy (_szHost,"127.0.0.1");
            }
            pArg=strstr(szToken, "port=");
            if (pArg != NULL) {
                pArg+=strlen("port=");
                char* stop;
                _Port= (tU16)strtoul(pArg,&stop,10);
                if (_Port == 0) {
                    return FALSE;
                }
                bFoundPort=TRUE;

            }
            char szStartupInd[50];
            sprintf(szStartupInd, "%s%02x=\"", "startup_msg_0x", u8GetLun());
            pArg=strstr(szToken, szStartupInd);
            if (pArg != NULL) {
                pArg+=strlen(szStartupInd);
                tChar *szStartUpMsg=_szAutoStartupMsg;
                szStartUpMsg+=sprintf(szStartUpMsg, "SSI32.rx MSG=");
                while(*pArg!=0 && *pArg!='\"') {
                    *szStartUpMsg++=*pArg++;
                }
                *szStartUpMsg='\0';
                // bStoreRxPkt("SSI32.rx MSG=FFFF0043010f010c0000\n");
            }
			if (bFoundHost && bFoundPort) {
                return TRUE;
            }


       }
       return FALSE;
    }
    tVoid vStartThreads();
    static tVoid vRxThreadStarter(tVoid* pvArg);
    tVoid vRxThreadFn();
    static tVoid vTxThreadStarter(tVoid* pvArg);
    tVoid vTxThreadFn();

    char *szGetName(tString szDest, tCString szBegin, tU32 u32MaxLen=SSI_MAX_NAME_LEN) {
        OSALUTIL_s32SaveNPrintFormat(szDest, u32MaxLen, "%s_%02x", szBegin, u8GetLun());
        return szDest;
    };
    tBool bStoreRxPkt(tChar *szRxBuf, tU32 *pu32RxBufLen);
    tBool bParseRxPkt(tChar *szRxBuf, tU8 *pu8OutBuf, tU32 *pu32OutBufLen);
    tU32 u32Tx2Ascii(tU8 *pu8Buffer, tU32 nbytes);
    tBool bDataReq(tU8 *pu8Data, tU32 u32Length);


} ;

class cAdrChannelMngr
{
public:
    cAdrChannelMngr() {
    }

    ~cAdrChannelMngr() {
        _hMapSem.vClose();
        // todo: delete all elements in map
    }


    map<tU8, trChnData*> _aChnMap;
    adr3_sem _hMapSem;

    trChnData *prGetChnData(tU8 u8Lun) {
		vCallFirst();
        _hMapSem.vGet();
        for (map<tU8, trChnData*>::iterator iter=_aChnMap.begin(); 
             iter!=_aChnMap.end();
             ++iter) {
            if (iter->second->u8GetLun() == u8Lun) {
                _hMapSem.vPost();
                return iter->second;
            }
        }
        _hMapSem.vPost();
        return OSAL_NULL;
    }


    bool bIsChnActive(tU8 u8Lun)
    {
        return prGetChnData(u8Lun) ? TRUE: FALSE;
    }

    tBool bAddChn(tU32 u32Fd, ssi_tChannelConfig const &rSsiCfg) {
	    vCallFirst();
        tU8 u8Lun=rSsiCfg.u8Lun;
        trChnData *prChnData=prGetChnData(u8Lun);
        if (OSAL_NULL != prChnData) {
            return FALSE;
        }
        prChnData=OSAL_NEW trChnData(u32Fd, rSsiCfg);
        _hMapSem.vGet();
        _aChnMap[u8Lun]=prChnData;
        _hMapSem.vPost();
        prChnData->vStartThreads();
        return prChnData->_enState==trChnData::enState_Open || prChnData->_enState==trChnData::enState_Running;
    }

    void vDelDevice(tU8 u8Lun) {
		vCallFirst();
        // todo: better deactivate but leave in map
        trChnData *prChnData=prGetChnData(u8Lun);
        if (OSAL_NULL != prChnData) {
            // close osal-device
            OSAL_DELETE prChnData; 
            _hMapSem.vGet();
            _aChnMap.erase(u8Lun);
            _hMapSem.vPost();
        }
    }

private:
    tVoid vCallFirst() {
        if (!_hMapSem.bIsOpen()) {
            _hMapSem.vOpen("adr3MapSem", 1);
        }
    }


};

static cAdrChannelMngr m_ChnMngr;


tVoid trChnData::vStartThreads() {

    // start rx-thread
    char szRxThreadName[SSI_MAX_NAME_LEN];
    OSAL_trThreadAttribute    rRxThreadAttribute 
        = {
        szGetName(szRxThreadName, "rxThrd"),
        100, // PRIO
        10000,
        vRxThreadStarter,
        this      
    };
    _hRxThreadId=OSAL_ThreadSpawn ( &rRxThreadAttribute );
    ADR3_SOCK_ASSERT_RETURN(OSAL_ERROR != _hRxThreadId);



    

    // start tx-thread
    char szTxThreadName[SSI_MAX_NAME_LEN];
    OSAL_trThreadAttribute    rTxThreadAttribute 
        = {
        szGetName(szTxThreadName, "txThrd"),
        100, // PRIO
        10000,
        vTxThreadStarter,
        this      
    };
    _hTxThreadId=OSAL_ThreadSpawn ( &rTxThreadAttribute );
    ADR3_SOCK_ASSERT_RETURN(OSAL_ERROR != _hTxThreadId);

}

tVoid trChnData::vRxThreadStarter(tVoid* pvArg) {
    trChnData *prChnData=(trChnData *)pvArg;
    ADR3_SOCK_ASSERT_RETURN(OSAL_NULL != prChnData);
    prChnData->vRxThreadFn();
};


tVoid trChnData::vTxThreadStarter(tVoid* pvArg) {
    trChnData *prChnData=(trChnData *)pvArg;
    ADR3_SOCK_ASSERT_RETURN(OSAL_NULL != prChnData);

    prChnData->vTxThreadFn();
};


tBool trChnData::bParseRxPkt(tChar *szRxBuf, tU8 *pu8OutBuf, tU32 *pu32OutBufLen) {
    char *pMatch = szRxBuf;
    *pu32OutBufLen=0;
    pMatch = strstr((char*)(pMatch),"SSI32.rx"); 
    if (!pMatch) {
        return FALSE;
    }

    pMatch = strstr((char*)(pMatch),"MSG="); 
    if (!pMatch) {
        return FALSE;
    }
    pMatch+=strlen("MSG=");
    tU32 u32AsciiLen=strlen(pMatch);
    if (u32AsciiLen%2) {
        return FALSE;
    }

    tChar *pEnd=pMatch+(u32AsciiLen*2);
    char s[3];
    s[2]=0;
    
    while(pMatch<pEnd){         // calculate ascii-char into hex-values
        s[0]=*pMatch++;
        s[1]=*pMatch++;
        sscanf(s, "%02X", pu8OutBuf);
        pu8OutBuf++;
    }
    *pu32OutBufLen=u32AsciiLen/2;
    return TRUE;

};

tBool trChnData::bStoreRxPkt(tChar *szRxBuf, tU32 *pu32RxBufLen) {
    tBool bRes=FALSE;
    tU32 i=0;
    tU32 u32RxLen=*pu32RxBufLen;
    char *pCurPkt=szRxBuf;
    char *pEnd=szRxBuf+u32RxLen;
    tBool bMsgEmpty=TRUE;

    for (i=0; i<u32RxLen; i++) {
        if (szRxBuf[i]=='\r' || szRxBuf[i]=='\n'|| szRxBuf[i]==0) {
        	szRxBuf[i]=0;

            // copy complete packet
        	if (!bMsgEmpty) {
              bRes=bParseRxPkt(pCurPkt, _au8RxBuf, &_u32RxBufLen);
              bMsgEmpty=TRUE;
        	}
            // set start-address of next packet
        	pCurPkt=&szRxBuf[i+1];
            if (bRes) {
                break;
            }

         } else {
        	bMsgEmpty=FALSE;
         }
    }

    // copy remainder
    if (pCurPkt==pEnd) {
    	// we consumed all bytes, no remainder
    	*pu32RxBufLen=0;
    }
    else if (pCurPkt != szRxBuf) {
    	// we consumed some but not all bytes
        tU32 u32RemainderLen=u32RxLen +szRxBuf -pCurPkt;
        memcpy(szRxBuf, pCurPkt, u32RemainderLen);
        *pu32RxBufLen=u32RemainderLen;
    }
    // else we consumed nothing

    return bRes;
};

tVoid trChnData::vRxThreadFn() {
    tChar *szRxBufLocal= OSAL_NEW tChar[4*ADR3_SOCK_MAX_RX_LEN] ;
    tU32 u32RxBufLenLocal=0;
    while (bIsRunning()) {
        // we get the sem when someone wants us to reade
        _hRxSem.vGet();
        tS32 s32IOReadLen=0;
        tBool bPktComplete=bStoreRxPkt(szRxBufLocal, &u32RxBufLenLocal);
        while (!bPktComplete && bIsRunning() && _sock != INVALID_SOCKET ) {
            s32IOReadLen=read(_sock, &szRxBufLocal[u32RxBufLenLocal], ADR3_SOCK_MAX_RX_LEN*2);
            //            TCP_s32IORead(u8GetLun(), _u32Fd, (tS8*)&szRxBufLocal[u32RxBufLenLocal], ADR3_SOCK_MAX_RX_LEN*2, &s32IOReadLen);
            // todo: error-handling if read return 0
            if (s32IOReadLen != -1 && s32IOReadLen != 0) {
                u32RxBufLenLocal+=s32IOReadLen;
            szRxBufLocal[u32RxBufLenLocal]=0;
            bPktComplete=bStoreRxPkt(szRxBufLocal, &u32RxBufLenLocal);
        }
        }
        if (bPktComplete) {
            // data confirmation to application (audio/tuner/dab)
            _bRxReady=TRUE;
            _rSsiCfg.pvfnCallbackDataInd((tVoid *)u8GetLun(), _au8RxBuf, _u32RxBufLen);
        }
    }

    _hTermSem.vPost();
    OSAL_DELETE[] szRxBufLocal;


};



tVoid trChnData::vTxThreadFn() {
    while (bIsRunning() && _sock != INVALID_SOCKET ) {
        // we get the sem when someone wants us to write

        _hTxSem.vGet();

        tU32 u32TxLenRes=0;

        u32TxLenRes=send(_sock, _szTxBuf, _u32TxBufLen, 0);
        // data confirmation to application (audio/tuner/dab)
        enSsiConfState enSsiRes=(u32TxLenRes == _u32TxBufLen) ? SSI_C_CONF_OKAY : SSI_C_CONF_FAULT;
        _u32TxBufLen=0;
        _rSsiCfg.pvfnCallbackDataCon((tVoid *)u8GetLun(), enSsiRes);
    }
    _hTermSem.vPost();

}

/****************************************************************************
 * FUNCTION:     function to write DAB-messages and add header information   *
 *               like "SSI32.TX CH=0 LUN=18 MSG="                            *
 *               Additional the meca-msg) will be added.                     *
 ****************************************************************************/

// todo needs to be adapted for lun-handling (at the moment hard coded "LUN=18")

// just copied from tcpraw device
tU32 trChnData::u32Tx2Ascii(tU8 *pu8Buffer, tU32 nbytes)
{
    tChar *pBuf=_szTxBuf;

    pBuf+=sprintf(pBuf, "SSI32.TX CH=0 LUN=%d MSG=", u8GetLun());

    for (tU32 j = 0; j< nbytes; j++)
    {
        pBuf+=sprintf(pBuf,"%02x", pu8Buffer[j]);

    }
    pBuf+=sprintf(pBuf,"\r");
    return (tU32)strlen((char *)_szTxBuf);
};

tBool trChnData::bDataReq(tU8 *pu8Data, tU32 u32Length) {
    if (_u32TxBufLen) {
        // sending still busy
        return FALSE;
    }
    // store data to be sent
    _u32TxBufLen=u32Tx2Ascii(pu8Data, u32Length);
    
    _hTxSem.vPost();
    return TRUE;
};




extern "C" {

/*******************************************************************************
 * function    SSI_s32OpenChannel
 * \doxydocu
 * \brief      This function opens the connection to a LUN
 *
 * \param[in]  pvHandle
 *              pointer to handle. The handle identifies the connection.
 * \param[in]  prChannelConfig
 *              Pointer to the configuration data for the connection.
 *              
 * \return     Error code. Possible values are SSI_C_NO_ERROR and SSI_C_GENERAL_ERROR.
 *
 * \reentrant  no
 *
 ******************************************************************************/
tS32 SSI_s32OpenChannel(void **pvHandle, ssi_tChannelConfig *prChannelConfig)
{
    tU32 u32Fd=0; // handle

    trChnData *prChnData=m_ChnMngr.prGetChnData(prChannelConfig->u8Lun);
    *pvHandle=(tVoid *)OSAL_NULL;
    if (OSAL_NULL != prChnData) {
        // lun is already opened
        return SSI_C_GENERAL_ERROR;
    }
    tS32 s32OsalHanlde;


    if (!m_ChnMngr.bAddChn(u32Fd, *prChannelConfig)) {
        return SSI_C_GENERAL_ERROR;
    }

    *pvHandle=(tVoid *)prChannelConfig->u8Lun;

    return SSI_C_NO_ERROR;
}

/*******************************************************************************
 * function    SSI_s32CloseChannel
 * \doxydocu
 * \brief      This function opens the connection to a LUN
 *
 * \param[in]  pvHandle
 *              pointer to handle. The handle identifies the connection.
 *
 * \return     Error code. Possible values are SSI_C_NO_ERROR and SSI_C_GENERAL_ERROR.
 *
 * \reentrant  no
 *
 ******************************************************************************/
tS32 SSI_s32CloseChannel(void **pvHandle)
{
    tS32 s32RetVal = SSI_C_GENERAL_ERROR;
    tU8 u8Lun = (tU8)(tU32)(*pvHandle);
    trChnData *prChnData=m_ChnMngr.prGetChnData(u8Lun);
    if (OSAL_NULL != prChnData) {
        return SSI_C_GENERAL_ERROR;
    }


    tU32 u32Fd=prChnData->_u32Fd;
    m_ChnMngr.vDelDevice(u8Lun);

    return SSI_C_NO_ERROR;
}

/*******************************************************************************
 * function    SSI_s32DataReq
 * \doxydocu
 * \brief      Request to send a message to SSI
 *
 * \param[in]  pvHandle
 *              pointer to handle. The handle identifies the connection.
 * \param[in]  pu8Data
 *              Pointer to data which shall be send.
 * \param[in]  u32Length
 *              Number of data bytes which shall be send.
 *              
 * \return     Error code. Possible values are SSI_C_NO_ERROR and SSI_C_GENERAL_ERROR.
 *
 * \reentrant  no
 *
 ******************************************************************************/
tS32 SSI_s32DataReq(void *pvHandle,tU8 *pu8Data, tU32 u32Length)
{
    tS32 s32RetVal = SSI_C_GENERAL_ERROR;
    tU8 u8Lun = (tU8)(tU32)pvHandle;
    trChnData *prChnData=m_ChnMngr.prGetChnData(u8Lun);

    if (OSAL_NULL == prChnData) {
        return s32RetVal;
    }

    return prChnData->bDataReq(pu8Data, u32Length) == TRUE ? SSI_C_NO_ERROR : SSI_C_GENERAL_ERROR;
}

/*******************************************************************************
 * function    SSI_s32DataIndProcessed
 * \doxydocu
 * \brief      The received and indicated data are processed.
 *
 *             This function is called from application after YYY_vDataInd()
 *             to indicate that the data is processed. The SSI frees the Rx buffer.
 *
 * \param[in]  pvHandle
 *              pointer to handle. The handle identifies the connection.
 *              
 * \return     Error code. Possible values are SSI_C_NO_ERROR and SSI_C_GENERAL_ERROR.
 *
 * \reentrant  no
 *
 ******************************************************************************/
tS32
SSI_s32DataIndProcessed(void *pvHandle)
{
    tU8 u8Lun = (tU8)(tU32)pvHandle;
    trChnData *prChnData=m_ChnMngr.prGetChnData(u8Lun);
    if (prChnData==OSAL_NULL) {
        return SSI_C_GENERAL_ERROR;
    }
    if (!prChnData->_bRxReady) {
        return SSI_C_GENERAL_ERROR;
    }
    prChnData->_bRxReady=FALSE;
    // inform reader-thread
    prChnData->_hRxSem.vPost();

    return SSI_C_NO_ERROR;
}



tS32 SSI_s32SetReceiverState( void *pvHandle, enSsiReceiverState rSsiReceiverState) {
    tU8 u8Lun = (tU8)(tU32)pvHandle;
    trChnData *prChnData=m_ChnMngr.prGetChnData(u8Lun);
 
    if (prChnData==OSAL_NULL) {
        return SSI_C_GENERAL_ERROR;
    }

    if( rSsiReceiverState == SSI_C_RECEIVE_NOT_READY) {

    }
    else {
        if (!prChnData->bStart()) {
            return SSI_C_GENERAL_ERROR;
        }
    }
    return SSI_C_NO_ERROR;
}

tS32 SSI_s32StartChannel( void *pvHandle) {
        tU8 u8Lun = (tU8)(tU32)pvHandle;
    trChnData *prChnData=m_ChnMngr.prGetChnData(u8Lun);

    if (prChnData==OSAL_NULL) {
        return SSI_C_GENERAL_ERROR;
    }
    if (!prChnData->bStart()) {
        return SSI_C_GENERAL_ERROR;
    }
    return SSI_C_NO_ERROR;
}

} // extern "C"
