/*******************************************************************************
*
* FILE:         dev_wup_inc.h
*
* SW-COMPONENT: Device Wake-Up
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Local header with Inter-Node-Communication (INC) related
*               definitions.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_WUP_INC_H_
#define _DEV_WUP_INC_H_

/******************************************************************************/
/*                                                                            */
/* DEFINES                                                                    */
/*                                                                            */
/******************************************************************************/

/* -------------------------------------------------------------------------- */
/* INC message related defines  (little endian, least significant byte first) */
/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_U8_INC_MSG_CAT_MAJOR_VERSION_NUMBER                          2
#define DEV_WUP_C_U8_INC_MSG_CAT_MINOR_VERSION_NUMBER                         12

#define DEV_WUP_C_U8_INC_MSG_SEND                                           0x01
#define DEV_WUP_C_U8_INC_MSG_RECEIVE                                        0x02

// Message codes and lengths

#define DEV_WUP_C_U8_INC_MSGID_C_GET_DATA                                   0x20

#define DEV_WUP_C_U8_INC_MSGID_R_REJECT                                     0x21
#define DEV_WUP_C_U8_INC_MSGLEN_R_REJECT                                    0x03

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_CTRL_RESET_EXECUTION              0x30
                                                                                
#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_CTRL_RESET_EXECUTION              0x31
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_CTRL_RESET_EXECUTION             0x01

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_PROC_RESET_REQUEST                0x32
                                                                                
#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_PROC_RESET_REQUEST                0x33
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_PROC_RESET_REQUEST               0x02

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_STARTUP_FINISHED                  0x34

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_STARTUP_FINISHED                  0x35
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_STARTUP_FINISHED                 0x01

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_EXTEND_POWER_OFF_TIMEOUT          0x36

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT          0x37
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT         0x03

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_AP_SUPERVISION_ERROR              0x39
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_AP_SUPERVISION_ERROR             0x02

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_WAKEUP_EVENT_ACK                  0x42
                                                                                
#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_EVENT_ACK                  0x43
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_EVENT_ACK                 0x03

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_WAKEUP_STATE_ACK                  0x44
                                                                                
#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE_ACK                  0x45
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_STATE_ACK                 0x03

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE_VECTOR               0x47
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_STATE_VECTOR              0x05

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE                      0x49
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_STATE                     0x05

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_EVENT                      0x4D
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_EVENT                     0x04

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_REASON                     0x4F
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_REASON                    0x02

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_SHUTDOWN_IN_PROGRESS              0x50
                                                                                
#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_SHUTDOWN_IN_PROGRESS              0x51
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_SHUTDOWN_IN_PROGRESS             0x01

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_SET_WAKEUP_CONFIG                 0x52

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_SET_WAKEUP_CONFIG                 0x53
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_SET_WAKEUP_CONFIG                0x05

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_INDICATE_CLIENT_APP_STATE         0xC0
                                                                                
#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE         0xC1
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE        0x06

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_STARTUP_INFO                      0xC3
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_STARTUP_INFO                     0x03

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_REQ_CLIENT_APP_STATE              0xC5
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_REQ_CLIENT_APP_STATE             0x02

#define DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_REQ_CLIENT_BOOT_MODE              0xC6

#define DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_REQ_CLIENT_BOOT_MODE              0xC7
#define DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_REQ_CLIENT_BOOT_MODE             0x02

#define DEV_WUP_C_U8_INC_MSGID_INVALID                                      0xFF

// Message parameters

#define DEV_WUP_C_U8_GET_DATA_MODE_ALL_DATA                                 0x00
#define DEV_WUP_C_U8_GET_DATA_MODE_AS_SPECIFIED                             0x10

#define DEV_WUP_C_U8_RESET_REASON_AP_INTENDED                               0x00
#define DEV_WUP_C_U8_RESET_REASON_AP_EXCEPTIONAL                            0x01

#define DEV_WUP_C_U8_SHUTDOWN_NOT_ALLOWED                                   0x00
#define DEV_WUP_C_U8_SHUTDOWN_ALLOWED                                       0x01
#define DEV_WUP_C_U8_SHUTDOWN_CANCELED                                      0x02


#define DEV_WUP_C_U8_REJECT_OK                                              0xFF
#define DEV_WUP_C_U8_REJECT_NO_REASON                                       0x00
#define DEV_WUP_C_U8_REJECT_UNKNOWN_MESSAGE                                 0x01
#define DEV_WUP_C_U8_REJECT_INVALID_PARAMETER                               0x02
#define DEV_WUP_C_U8_REJECT_TEMPORARY_UNAVAILABLE                           0x03
#define DEV_WUP_C_U8_REJECT_VERSION_MISMATCH                                0x04
#define DEV_WUP_C_U8_REJECT_NOT_SUPPORTED                                   0x05
#define DEV_WUP_C_U8_REJECT_SEQUENCE_ERROR                                  0x06

#define DEV_WUP_C_U8_MSG_CAT_VERSION_CHECK_RESULT_OK                        0x01
#define DEV_WUP_C_U8_MSG_CAT_VERSION_CHECK_RESULT_MISMATCH                  0x04

#define DEV_WUP_C_U8_BOOT_MODE_EMMC                                         0x00
#define DEV_WUP_C_U8_BOOT_MODE_USB                                          0x01

#define DEV_WUP_C_U8_PREVENT_VCC_RESET_BITMASK_ALL                          0x01
#define DEV_WUP_C_U8_PREVENT_VCC_RESET_BITMASK_COM_WD                       0x02

/* -------------------------------------------------------------------------- */

#endif //_DEV_WUP_INC_H_
