/*******************************************************************************
*
* FILE:         dev_wup_rootdaemon.h
*
* SW-COMPONENT: Device Wake-Up
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  TBD
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2015 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_WUP_ROOTDAEMON_H_
#define _DEV_WUP_ROOTDAEMON_H_

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

#include "RootDaemonTypes.h"

/******************************************************************************/
/*                                                                            */
/* DEFINES                                                                    */
/*                                                                            */
/******************************************************************************/

typedef enum
{
	RESET_APPLICATION_PROCESSOR,
	EMMC_REMOUNT_READ_ONLY
} tenRootDaemonCommands;

/******************************************************************************/
/*                                                                            */
/* FUNCTION DECLARATIONS                                                      */
/*                                                                            */
/******************************************************************************/

CmdData DEV_WUP_ROOTDAEMON_CALLER_rPerformRootOp(const char * clientName, const tenRootDaemonCommands cmdNum, const char * args);

/******************************************************************************/

#endif //_DEV_WUP_ROOTDAEMON_H_
