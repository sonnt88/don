/*******************************************************************************
*
* FILE:         dev_volt_if.h
*
* SW-COMPONENT: Device Wake-Up
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Public interface header.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifdef DEV_WUP_IMPORT_INTERFACE_GENERIC
  #ifndef DEV_WUP_ALREADY_INCLUDE_GENERIC
    #define DEV_WUP_ALREADY_INCLUDE_GENERIC
    #include "dev_wup_osal.h"
    #include "dev_wup_inc.h"
  #endif 
#endif 

#ifdef DEV_WUP_IMPORT_INTERFACE_INC
  #ifndef DEV_WUP_ALREADY_INCLUDE_INC
    #define DEV_WUP_ALREADY_INCLUDE_INC
    #include "dev_wup_inc.h"
  #endif 
#endif 

#ifdef DEV_WUP_IMPORT_INTERFACE_OSAL
  #ifndef DEV_WUP_ALREADY_INCLUDE_OSAL
    #define DEV_WUP_ALREADY_INCLUDE_OSAL
    #include "dev_wup_osal.h"
  #endif 
#endif 

#if !(defined DEV_WUP_IMPORT_INTERFACE_GENERIC || \
      defined DEV_WUP_IMPORT_INTERFACE_INC     || \
      defined DEV_WUP_IMPORT_INTERFACE_OSAL      )
 #error "Please define an interface when you include dev_wup_if.h"
#endif
