/*******************************************************************************
*
* FILE:         dev_wup_rootdaemon_client.cpp
*
* SW-COMPONENT: Device Wake-Up
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  This file contains the puplic interface for the root daemon
*               to execute those actions which need root access to be 
*               performed properly.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2015 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>
#include <mntent.h>
#include <fcntl.h>
#include <string.h>
#include <string>

#include "IClientSharedlib.h"

#include "dev_wup_rootdaemon.h"

/******************************************************************************/
/*                                                                            */
/* DEFINITIONS                                                                */
/*                                                                            */
/******************************************************************************/

#define DEV_WUP_ROOT_EMMC_REMOUNT_READ_ONLY_TIMEOUT_MS                       300

extern "C"
{

/******************************************************************************/
/*                                                                            */
/* LOCAL FUNCTIONS                                                            */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Perform a reset of the application processor via the sysrq trigger.
*
*******************************************************************************/
static int DEV_WUP_ROOT_nResetApplicationProcessor(void)
{
	int  nFd;

	char chSysrq              = '1';
	char chSysrqTriggerReboot = 'b';

	nFd = open("/proc/sys/kernel/sysrq", O_RDWR);

	if (nFd == -1)
		return errno;

	if (write(nFd, &chSysrq, sizeof(char)) == -1) {
		close(nFd);
		return errno;
	}

	close(nFd);

	nFd = open("/proc/sysrq-trigger", O_RDWR);

	if (nFd == -1)
		return errno;

	if (write(nFd, &chSysrqTriggerReboot, sizeof(char)) == -1) {
		close(nFd);
		return errno;
	}

	close(nFd);

	return 0;
}

/*******************************************************************************
*
* Trigger an emergency remount read-only for all devices and wait until all
* eMMC related devices have reached the read-only state. The check for the
* reached read-only state of the eMMC related devices is limited by a timeout.
*
*******************************************************************************/
static int DEV_WUP_ROOT_nEmmcRemountReadOnly(void)
{
	int             nFd;
	long            lStartTimeMs;
	long            lCurrentTimeMs;

	bool            bSuccess                = false;
	struct mntent*  prMntent                = { 0 };
	FILE*           prFile                  = NULL;
	char            chSysrq                 = '1';
	char            chSysrqTriggerRemountRO = 'u';
	struct timespec rStartTime              = { 0 };
	struct timespec rCurrentTime            = { 0 };

	nFd = open("/proc/sys/kernel/sysrq", O_RDWR);

	if (nFd == -1)
		return errno;

	if (write(nFd, &chSysrq, sizeof(char)) == -1) {
		close(nFd);
		return errno;
	}

	close(nFd);

	nFd = open("/proc/sysrq-trigger", O_RDWR);

	if (nFd == -1)
		return errno;

	if (write(nFd, &chSysrqTriggerRemountRO, sizeof(char)) == -1) {
		close(nFd);
		return errno;
	}

	close(nFd);

	if (clock_gettime(CLOCK_REALTIME, &rStartTime) != -1)
		lStartTimeMs = (rStartTime.tv_sec * 1000) + ((long)(rStartTime.tv_nsec / 1.0e6));
	else
		return errno;

	while (!bSuccess) {

		usleep(10);

		bSuccess = true;

		prFile = setmntent("/proc/mounts", "r");

		if (prFile == NULL)
			return errno;

		while (NULL != (prMntent = getmntent(prFile))) {
			if ((strcmp(
				(const char*)prMntent->mnt_fsname,
				"/dev/mmc") == 0) &&
			    (strcmp(
				(const char*)prMntent->mnt_opts,
				"ro") != 0)) {
				bSuccess = false;
				break;
			}
		}

		endmntent(prFile);

		if (clock_gettime(CLOCK_REALTIME, &rCurrentTime) != -1)
			lCurrentTimeMs = (rCurrentTime.tv_sec * 1000) + ((long)(rCurrentTime.tv_nsec / 1.0e6));
		else
			return errno;

		if ((lCurrentTimeMs - lStartTimeMs) > DEV_WUP_ROOT_EMMC_REMOUNT_READ_ONLY_TIMEOUT_MS)
			return -ETIME;
	}

	return 0;
}

/*******************************************************************************
*
* Central function which triggers the execution of all the operations which
* require root permission.
*
*******************************************************************************/
static CmdData DEV_WUP_ROOTDAEMON_CLIENT_rPerformRootOp(const int cmdNum, std::string args)
{
	CmdData rCmdData = { 0 };
	int     nResult  = 0;

	(void)args; // Unused parameter.

	switch(cmdNum) {
	case RESET_APPLICATION_PROCESSOR: {
		nResult = DEV_WUP_ROOT_nResetApplicationProcessor();
		
		strcpy(rCmdData.message, nResult ? "FAILURE" : "SUCCESS");

		rCmdData.errorNo = ERR_NONE;
		break;
	}
	case EMMC_REMOUNT_READ_ONLY: {
		nResult = DEV_WUP_ROOT_nEmmcRemountReadOnly();
		
		strcpy(rCmdData.message, nResult ? "FAILURE" : "SUCCESS");

		rCmdData.errorNo = ERR_NONE;
		break;
	}
	default:
		rCmdData.errorNo = ERR_UNKNOWN_CMD;
		break;
	}

	return rCmdData;
}

/******************************************************************************/
/*                                                                            */
/* PUBLIC FUNCTIONS                                                           */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Common root daemon interface function which returns a string with the
* name of this client.
*
*******************************************************************************/
const char * getClientName()
{
	return "dev_wup";
}

/*******************************************************************************
*
* Common root daemon interface function which returns a string with the
* name of the related Linux file system access control GROUP.
*
*******************************************************************************/
const char * getClientGroupName()
{
	return "eco_osal";
}

/*******************************************************************************
*
* Common root daemon interface function which returns a string with the
* name of the related Linux file system access control USER.
*
*******************************************************************************/
const char * getClientUserName()
{
	// The wakeup device doesn't belong to a concrete user, therefore
	// don't return a user name.
	return NULL;
}

/*******************************************************************************
*
* Common root daemon interface to execute a passed command.
*
*******************************************************************************/
CmdData command(const int cmdNum, std::string args)
{
	return (DEV_WUP_ROOTDAEMON_CLIENT_rPerformRootOp(cmdNum, args));
}

/*******************************************************************************
*
* Local wrapper function to directly execute root operations without the help
* of the root-daemon while the wake-up device is itself being executed as root.
*
*******************************************************************************/
CmdData DEV_WUP_ROOTDAEMON_CLIENT_rPerformRootOpAsRoot(const int cmdNum, std::string args)
{
	return (DEV_WUP_ROOTDAEMON_CLIENT_rPerformRootOp(cmdNum, args));
}

/******************************************************************************/

} // extern "C"
