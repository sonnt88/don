/*******************************************************************************
*
* FILE:         dev_wup.c
*
* SW-COMPONENT: Device Wake-Up
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  The wake-up device is connected to the power-master application
*               of the system communication controller via an INC communication
*               channel and covers most of its functionalities via this INC 
*               message exchange.

*               The core functionalities of the wake-up device are :
*
*               - Detect and offer the systems start-type.
*               - Detect and offer the initial wake-up reason.
*               - Continuously check for new switch-on/off reasons and notify
*                 registered clients about any changes.
*               - Trigger the shut-down of the application processor.
*               - Configure which of the possible wake-up reasons shall be
*                 considered to wake-up the system.
*               - Perform self-resets of the application processor, resets of
*                 the system communication controller and other ECUs which are
*                 under control of the system communication controller.
*		- Control if and how the application processor is being
*                 supervised by the system communcation controller.
*               - Extend the time the application processor is allowed to stay
*                 switched on from the point of view of the system
*                 communication controller.
*               - Maintain and offer the latest reset reason of the application
*                 processor and the system communication controller.
*               - Offer an interface for the system communication controller to
*                 write into the error-memory.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <stdio.h>
#include <dirent.h>
#include <mntent.h>
#include <arpa/inet.h>

#include "inc.h"
#include "inc_ports.h"
#include "dgram_service.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"

#define DEV_WUP_IMPORT_INTERFACE_GENERIC
#include "dev_wup_if.h"

#include "dev_wup_rootdaemon.h"

#ifdef DEV_WUP_UNIT_TEST
  #include "./utest/dev_wup_glibc_mock.h"
  #define FOREVER 0
#else
  #define FOREVER 1
#endif

/******************************************************************************/
/*                                                                            */
/* DEFINES                                                                    */
/*                                                                            */
/******************************************************************************/

#define DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS                         ((tU8)5)

#define DEV_WUP_CONF_C_U32_INC_MSG_RECEIVED_TIMEOUT_S                  ((tU32)2)

#define DEV_WUP_CONF_C_EN_GPIO_CPU_PWR_OFF                   OSAL_EN_CPU_PWR_OFF

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_STRING_DOWNLOAD_MODE_1_FILE_NAME   "/dev/root/etc/bosch_rootfs_initramfs.id"
#define DEV_WUP_C_STRING_DOWNLOAD_MODE_2_FILE_NAME   "/dev/root/etc/bosch_rootfs_initramfs-v2.id"

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_U32_CREATOR_CONTEXT_MAGIC                   ((tU32)0xDEADBEEF)
#define DEV_WUP_C_U32_CONNECT_TO_FAKE_INC_DEVICE_MAGIC        ((tU32)0xDEADBEEF)

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_U8_ERROR                                              ((tU8)0)
#define DEV_WUP_C_U8_OK                                                 ((tU8)1)

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_U8_ONOFF_REASON_TYPE_EVENT                            ((tU8)0)
#define DEV_WUP_C_U8_ONOFF_REASON_TYPE_STATE                            ((tU8)1)

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_U8_TRACE_TYPE_STRING                               ((tU8)0x01)
#define DEV_WUP_C_U8_TRACE_TYPE_INC_MESSAGE                          ((tU8)0x02)
#define DEV_WUP_C_U8_TRACE_TYPE_INTERNAL_DATA                        ((tU8)0x03)
#define DEV_WUP_C_U8_TRACE_TYPE_IO_CONTROL                           ((tU8)0x04)
#define DEV_WUP_C_U8_TRACE_TYPE_ONOFF_REASON_BUFFER                  ((tU8)0x05)
#define DEV_WUP_C_U8_TRACE_TYPE_NOTIFICATION_EVENT                   ((tU8)0x06)
#define DEV_WUP_C_U8_TRACE_TYPE_IO_CONTROL_LIST_ELEMENTS             ((tU8)0x07)
#define DEV_WUP_C_U8_TRACE_TYPE_REGISTERED_CLIENT_DATA               ((tU8)0x08)
#define DEV_WUP_C_U8_TRACE_TYPE_DROPPED_WAKEUP_EVENT                 ((tU8)0x09)

#define DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH                         ((tU8)254)
#define DEV_WUP_C_U8_TACE_CALLBACK_BUFFER_LENGTH                       ((tU8)64)

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_STRING_INC_HOST_LOCAL_FAKE                       "fake0-local"
#define DEV_WUP_C_STRING_INC_HOST_REMOTE_FAKE                            "fake0"
#define DEV_WUP_C_STRING_INC_HOST_LOCAL                              "scc-local"
#define DEV_WUP_C_STRING_INC_HOST_REMOTE                                   "scc"

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_STRING_SHARED_MEM_NAME                            "DevWupData"

#define DEV_WUP_C_STRING_SEM_DATA_NAME                              "DevWupData"

#define DEV_WUP_C_U32_SEM_DATA_TIMEOUT_MS                           ((tU32)2000)

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME        "DevWupStartupMsg"

#define DEV_WUP_C_STRING_SEM_WAKEUP_MSG_RECEIVED_NAME          "DevWupWakeupMsg"

#define DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME     "DevWupCtrlResMsg"

#define DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME     "DevWupProcResMsg"

#define DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME         "DevWupPwrOffMsg"

#define DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME          "DevWupAppMsg"

#define DEV_WUP_C_STRING_SEM_ONOFF_EVENT_ACK_RECEIVED_NAME   "DevWupEventAckMsg"

#define DEV_WUP_C_STRING_SEM_ONOFF_STATE_ACK_RECEIVED_NAME   "DevWupStateAckMsg"

#define DEV_WUP_C_STRING_SEM_WAKEUP_CFG_MSG_RECEIVED_NAME   "DevWupWakeupCfgMsg"

#define DEV_WUP_C_STRING_SEM_START_FINISH_MSG_RECEIVED_NAME  "DevWupStFinishMsg"

#define DEV_WUP_C_STRING_SEM_SHUTDOWN_MSG_RECEIVED_NAME      "DevWupShutdownMsg"

#define DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS               ((tU32)2000)

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_U8_THREAD_NOT_INSTALLED                               ((tU8)0)
#define DEV_WUP_C_U8_THREAD_RUNNING                                     ((tU8)1)
#define DEV_WUP_C_U8_THREAD_SHUTTING_DOWN                               ((tU8)2)
#define DEV_WUP_C_U8_THREAD_OFF                                         ((tU8)3)

#define DEV_WUP_C_U32_SEM_THREAD_TIMEOUT_MS                         ((tU32)2000)

/* -------------------------------------------------------------------------- */

// Free client event bitmasks for /dev/wup starting at 0x00010000 (osioctrl.h)
#define DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD                  ((tU32)0x00010000)

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_STRING_INC_MSG_THREAD_NAME                         "DevWupInc"
#define DEV_WUP_C_S32_INC_MSG_THREAD_STK_SIZE                      ((tS32)10000)
#define DEV_WUP_C_U32_INC_MSG_THREAD_PRIO                     ((tU32)0x0000004A)

#define DEV_WUP_C_STRING_INC_MSG_THREAD_EVENT_NAME                   "DevWupInc"

#define DEV_WUP_C_U32_INC_MSG_THREAD_EVENT_MASK_ALL \
        (DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD)

#define DEV_WUP_C_STRING_SEM_INC_MSG_THREAD_NAME                     "DevWupInc"

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_STRING_OSAL_MSG_THREAD_NAME                       "DevWupOsal"
#define DEV_WUP_C_S32_OSAL_MSG_THREAD_STK_SIZE                     ((tS32)10000)
#define DEV_WUP_C_U32_OSAL_MSG_THREAD_PRIO                    ((tU32)0x0000004A)

#define DEV_WUP_C_STRING_OSAL_MSG_QUEUE_NAME                        "DevWupOsal"
#define DEV_WUP_C_U32_OSAL_MSG_QUEUE_MAX_MSGS                         ((tU32)32)
#define DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN                          ((tU32)64)

#define DEV_WUP_C_U8_OSAL_MSG_STOP_THREAD                               ((tU8)1)
#define DEV_WUP_C_U8_OSAL_MSG_TRACE_CALLBACK_COMMAND_RECEIVED           ((tU8)2)
#define DEV_WUP_C_U8_OSAL_MSG_SEND_INC_MSG                              ((tU8)3)

#define DEV_WUP_C_STRING_SEM_OSAL_MSG_THREAD_NAME                   "DevWupOsal"

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_STRING_CLIENT_THREAD_NAME                       "DevWupClient"
#define DEV_WUP_C_S32_CLIENT_THREAD_STK_SIZE                       ((tS32)10000)
#define DEV_WUP_C_U32_CLIENT_THREAD_PRIO                      ((tU32)0x0000004A)

#define DEV_WUP_C_U32_CLIENT_THREAD_EVENT_MASK_ALL \
        (DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY | \
         DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY | \
         DEV_WUP_C_U32_EVENT_MASK_AP_SUPERVISION_ERROR_CHANGED_NOTIFY | \
         DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD)

#define DEV_WUP_C_STRING_SEM_CLIENT_THREAD_NAME                   "DevWupClient"

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_U8_INC_MSG_BUFFER_LENGTH                             ((tU8)64)

/* -------------------------------------------------------------------------- */

#define DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN                 ((tU8)128)

#define DEV_WUP_C_CHAR_SET_CPU_RUN_INACTIVE                                  '0'
#define DEV_WUP_C_CHAR_SET_CPU_RUN_ACTIVE                                    '1'
#define DEV_WUP_C_CHAR_TRIGGER_EMMC_POWER_OFF_AND_SET_CPU_RUN_INACTIVE       '4'
//#define DEV_WUP_C_CHAR_TRIGGER_EMMC_POWER_OFF_AND_SET_CPU_RUN_ACTIVE       '5'

/******************************************************************************/
/*                                                                            */
/* TYPE DEFINITIONS                                                           */
/*                                                                            */
/******************************************************************************/

typedef struct
{
  tU32                  u32ClientId;
  tChar                 szNotificationEventName[DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH];
  tU8                   u8OnOffReasonChangedNotificationMode;
  tBool                 bIsSystemMaster;
  tU32                  u32IndexOnOffEventRead;
  tU16                  u16LatestAckOnOffEventMsgHandle;
  tBool                 bNotifyAPSupervisionError;
} trClientSpecificData;

typedef struct
{
  DEV_WUP_trOnOffEvent  arOnOffEvent[DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS];
  tU32                  u32IndexOnOffEventHead;
  tU32                  u32IndexOnOffEventTail;
  tU8                   u8NumberOfUsedElements;
} trOnOffEventRingBuffer;

typedef struct
{
  tU16 u16LatestAcknowledgedOnOffEventMsgHandle;
  tU16 u16ResetReasonAP;
  tU8  u8ResetClassificationAP;
  tU8  auReservedAndFillUpTo32Bytes[27];
} trPersistentRamData; // PRAM data entry configured with 32 bytes

typedef struct
{
  tU8                                    u8NumberOfClients;
  tU8                                    u8NumberOfRegisteredClients;
  tU8                                    u8NumberOfOnOffReasonChangedRegistrations;
  tU32                                   u32LatestAssignedClientId;
  tU8                                    u8Starttype;
  tU8                                    u8ApplicationModeSCC;
  tU8                                    u8ApplicationModeAP;
  tBool                                  bRStartupInfoReceived;
  tU8                                    u8WupReason;
  tBool                                  bRWupReasonReceived;
  tBool                                  bRIndicateClientAppStateReceived;
  tU32                                   u32AcknowledgedWakeupReasonsMask;
  DEV_WUP_trOnOffStates                  rOnOffStates;
  trOnOffEventRingBuffer                 rOnOffEventRingBuffer;
  trClientSpecificData                   arClientSpecificData[DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS];
  tU8                                    u8ResetReasonSCC;
  tU8                                    u8LatestResetProcessor;
  tU16                                   u16ReportedPowerOffTimeoutS;
  tU16                                   u16OnOffEventMsgHandleOfLatestAckResponse;
  tU16                                   u16OnOffStateMsgHandleOfLatestAckResponse;
  trPersistentRamData                    rPramMirror;
  trPersistentRamData                    rPramLocal;
  tBool                                  bSystemShutdownSuccess;
  tU8                                    u8APSupervisionError;
} trGlobalData;

typedef struct
{
  tU32                                      u32CreatorContextMagic;
  OSAL_tMQueueHandle                        hMsgQueueOsalThread;
  OSAL_tEventHandle                         hEventIncMsgThread;
  OSAL_tEventHandle                         hEventClientThread;
  OSAL_tSemHandle                           hSemDataAccess;
  OSAL_tSemHandle                           hSemIncMsgThread;
  OSAL_tSemHandle                           hSemOsalMsgThread;
  OSAL_tSemHandle                           hSemClientThread;
  OSAL_tSemHandle                           hSemWakeupReasonMsgReceived;
  OSAL_tSemHandle                           hSemStartupMsgReceived;
  OSAL_tSemHandle                           hSemCtrlResetMsgReceived;
  OSAL_tSemHandle                           hSemProcResetMsgReceived;
  OSAL_tSemHandle                           hSemPwrOffMsgReceived;
  OSAL_tSemHandle                           hSemAppStateMsgReceived;
  OSAL_tSemHandle                           hSemOnOffEventAckMsgReceived;
  OSAL_tSemHandle                           hSemOnOffStateAckMsgReceived;
  OSAL_tSemHandle                           hSemSetWakeupConfigMsgReceived;
  OSAL_tSemHandle                           hSemStartupFinishedMsgReceived;
  OSAL_tSemHandle                           hSemShutdownMsgReceived;
  OSAL_tShMemHandle                         hSharedMemory;
  OSAL_tIODescriptor                        rTraceIODescriptor;
  OSAL_tIODescriptor                        rWupIODescriptor;
  trGlobalData*                             prGlobalData;
  sk_dgram*                                 pDatagramSocketDescriptor;
  tInt                                      nSocketFileDescriptorInc;
  const tChar*                              coszIncHostLocal;
  const tChar*                              coszIncHostRemote;
  tU8                                       au8TraceCallbackBuffer[DEV_WUP_C_U8_TACE_CALLBACK_BUFFER_LENGTH];
  tU8                                       au8IncRcvBuffer[DEV_WUP_C_U8_INC_MSG_BUFFER_LENGTH];
  tU8                                       u8IncMsgThreadState;
  tU8                                       u8OsalMsgThreadState;
  tU8                                       u8ClientThreadState;
  tBool                                     bIncMsgCatVersionMatches;
  tU8                                       u8IncMsgCatMajorVersionServer;
  tU8                                       u8IncMsgCatMinorVersionServer;
  tU8                                       u8LatestReceivedOnOffEvent;
  tU16                                      u16LatestReceivedOnOffEventMsgHandle;
  OSAL_tEventHandle                         ahClientEventHandle[DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS];
  tBool                                     bGpioCpuRunAvailable;
  tBool                                     bGpioCpuPwfOffAvailable;
  tU32                                      u32ConnectToFakeIncDeviceMagic;
  tBool                                     bSccIncCommunicationEstablished;
} trModuleData;

typedef tU8 (*tpfu8IncMsgHandler)();

typedef struct
{
  tU8                u8MsgId;
  tU32               u32MsgLen;
  tBool              bRejectIfMsgCatVersionDoesntMatch;
  tpfu8IncMsgHandler pfu8IncMsgHandler;
} trIncMsgHandler;

/******************************************************************************/
/*                                                                            */
/* LOCAL FUNCTION DECLARATIONS                                                */
/*                                                                            */
/******************************************************************************/

/* -------------------------------------------------------------------------- */
/* Calling context : Creator process                                          */
/* -------------------------------------------------------------------------- */

/* ----------------------------- Initialization ----------------------------- */

static tU32 DEV_WUP_u32Init(tVoid);
static tU32 DEV_WUP_u32Deinit(tVoid);

static tVoid DEV_WUP_vInitModuleData(tVoid);
static tVoid DEV_WUP_vInitGlobalData(tVoid);

static tVoid DEV_WUP_vReadRegistryValues(tVoid);

static tVoid DEV_WUP_vReadPersistentRamData(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tVoid DEV_WUP_vWritePersistentRamData(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);

static tVoid DEV_WUP_vDetermineApplicationMode(tVoid);

static tU32  DEV_WUP_u32CreateClientNotificationEvents(tVoid);
static tVoid DEV_WUP_vDeleteClientNotificationEvents(tVoid);

static tVoid DEV_WUP_vRegisterTraceCallback(tVoid);
static tVoid DEV_WUP_vUnregisterTraceCallback(tVoid);

static tBool DEV_WUP_bEstablishSccIncCom(tVoid);
static tBool DEV_WUP_bReleaseSccIncCom(tVoid);

static tU32  DEV_WUP_u32InstallIncMsgThread(tVoid);
static tU32  DEV_WUP_u32UninstallIncMsgThread(tVoid);

static tU32  DEV_WUP_u32InstallOsalMsgThread(tVoid);
static tU32  DEV_WUP_u32UninstallOsalMsgThread(tVoid);

static tU32  DEV_WUP_u32InstallClientThread(tU8 u8NotificationMode);
static tU32  DEV_WUP_u32UninstallClientThread(tVoid);

/* --------------------------------- Trace ---------------------------------- */

static tVoid DEV_WUP_vTraceCallback(const tU8 * const pu8Data);
static tVoid DEV_WUP_vEvaluateTraceCallbackCommand(tVoid);

static tVoid DEV_WUP_vSimulateIncMsgReceived(const tU8 * const pu8RcvBuffer, tU32 u32MsgLen);

static tVoid DEV_WUP_vTraceInternalState(TR_tenTraceLevel enTraceLevel);
static tVoid DEV_WUP_vTraceOnOffEventBuffer(TR_tenTraceLevel enTraceLevel);
static tVoid DEV_WUP_vTraceRegisteredClientData(TR_tenTraceLevel enTraceLevel);
static tVoid DEV_WUP_vTraceNotificatonEvent(tU32 u32ClientId, tString szNotificationEventName, OSAL_tEventMask rEventMask);
static tVoid DEV_WUP_vTraceIncMsg(tU8 u8Direction, const tU8 * const pu8IncMsgBuffer, tU32 u32MessageLength);
static tVoid DEV_WUP_vTraceDroppedWakeupEvent(tU8 u8OnOffEvent, tU16 u16OnOffEventMsgHandle);

/* -------------------- INC message processing --------------------- */

static tVoid DEV_WUP_vIncMsgThread(tPVoid pvArg);
static tBool DEV_WUP_bEvaluateReceivedIncMsg(tU32 u32NumOfBytesReceived);
static tVoid DEV_WUP_vOnOsalEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemIncMsgThread);

/* -------------------- OSAL message processing --------------------- */

static tVoid DEV_WUP_vOsalMsgThread(tPVoid pvArg);
static tVoid DEV_WUP_vOnOsalMsgReceived(const tU8 * const pau8MsqQueueBuffer, OSAL_tSemHandle hSemIncMsgThread);

/* --------------------------------- Test Client ---------------------------- */

static tVoid DEV_WUP_vClientThread(tPVoid pvArg);
static tVoid DEV_WUP_vOnClientEventReceived(DEV_WUP_trClientRegistration* rClientRegistration, OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemClientThread);

/* --------------- send and evaluate received INC messages ------------------ */

static tVoid DEV_WUP_vSendIncMsgFromCreatorContext(const tU8 * const pu8IncSndBuffer, tU32 u32MessageLength);
static tVoid DEV_WUP_vSendIncMsgTransferedFromOtherContext(const tU8 * const pau8MsqQueueBuffer);

static tVoid DEV_WUP_vIncSend_CGetData(tU8 u8DataMode, tU8 u8MsgId);
static tVoid DEV_WUP_vIncSend_RReject(tU8 u8RejectReason, tU8 u8MsgId);
static tVoid DEV_WUP_vIncSend_CProcResetRequest(tU8 u8ProcId, tU16 u16ResetDurationMs);
static tVoid DEV_WUP_vIncSend_CCtrlResetExecution(tU8 u8ResetControlBitmask);
static tVoid DEV_WUP_vIncSend_CExtendPowerOffTimeout(tU16 u16TimeoutS);
static tVoid DEV_WUP_vIncSend_CStartupFinished(tVoid);
static tVoid DEV_WUP_vIncSend_CWakeupEventAck(tU8 u8Event, tU16 u16MsgHandle);
static tVoid DEV_WUP_vIncSend_CWakeupStateAck(tU16 u16MsgHandle);
static tVoid DEV_WUP_vIncSend_CShutdownInProgress(tVoid);
static tVoid DEV_WUP_vIncSend_CSetWakeupConfig(tU32 u32WakeupConfig);
static tVoid DEV_WUP_vIncSend_CIndicateClientAppState(tU8 u8ApplicationModeAP, tU8 u8ResetClassificationAP);
static tVoid DEV_WUP_vIncSend_CReqClientBootMode(tU8 u8BootMode);

static tU8 DEV_WUP_u8HandleIncMsg_RReject(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RCtrlResetExecution(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RProcResetRequest(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RStartupFinished(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RExtendPowerOffTimeout(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RAPSupervisionError(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupEventAck(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupStateAck(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupStateVector(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupState(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupEvent(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupReason(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RShutdownInProgress(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RSetWakeupConfig(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RReqClientAppState(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RStartupInfo(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RIndicateClientAppState(tVoid);
static tU8 DEV_WUP_u8HandleIncMsg_RReqClientBootMode(tVoid);

/* -------------------- General processing --------------------- */

static tVoid DEV_WUP_vStoreOnOffEvent(tU8 u8OnOffEvent, tU16 u16OnOffEventMsgHandle);
static tU32  DEV_WUP_u32NotifyOnOffReasonChanged(tU8 u8OnOffReasonType);
static tU32  DEV_WUP_u32NotifyAPSupervisionErrorChanged(tVoid);
static tBool DEV_WUP_bExecuteSystemShutdown(tVoid);
static tBool DEV_WUP_bGetUndervoltageDeviceName(tChar* pchDeviceName);
static tBool DEV_WUP_bSetGpioCpuPwrOff(tS32 s32GpioControl);

/* -------------------------------------------------------------------------- */
/* Calling context : Client processes                                         */
/* -------------------------------------------------------------------------- */

static tU32 DEV_WUP_u32Open(tVoid);
static tU32 DEV_WUP_u32Close(tVoid);
static tU32 DEV_WUP_u32Control(tS32 s32Fun, intptr_t pnArg);
static tU32 DEV_WUP_u32AddClient(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32RemoveClient(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32RegisterClient(DEV_WUP_trClientRegistration* prClientRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32UnregisterClient(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32RegisterOnOffReasonChanged(DEV_WUP_trOnOffReasonChangedRegistration* prOnOffReasonChangedRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32UnregisterOnOffReasonChanged(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32GetOnOffEvents(DEV_WUP_trOnOffEventHistory* prOnOffEventHistory, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32AcknowledgeOnOffEvent(DEV_WUP_trOnOffEventAcknowledge* prOnOffEventAcknowledge, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32AcknowledgeOnOffState(DEV_WUP_trOnOffStateAcknowledge* prOnOffStateAcknowledge, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32GetStarttype(tPU8 pu8Starttype, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32GetWakeupReason(tPU8 pu8WakeupReason, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32GetLatestResetReason(DEV_WUP_trLatestResetReason* prLatestResetReason, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32ExtendPowerOffTimeout(tU16* pu16TimeoutMs, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32ControlResetMasterSupervision(DEV_WUP_trResetMasterSupervision* prResetMasterSupervision);
static tU32 DEV_WUP_u32ConfigureWakeupReasons(tU32 u32WakeupReasonsMask, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32IndicateStartupFinished(tVoid);
static tU32 DEV_WUP_u32TriggerSystemShutdown(tU8 u8ShutdownType, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32GetApplicationMode(tPU8 pu8ApplicationModeAP, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32GetAPSupervisionError(tPU8 pu8APSupervisionError, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32RegisterAPSupervisionErrorChanged(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32 DEV_WUP_u32UnregisterAPSupervisionErrorChanged(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);

static tVoid DEV_WUP_vTransferIncMsgToCreatorContext(const tU8 * const pu8IncSndBuffer, tU32 u32MessageLength);

/* -------------------------------------------------------------------------- */
/* Calling context : Creator process and/or Client processes                  */
/* -------------------------------------------------------------------------- */

static tU32  DEV_WUP_u32MapGlobalData(OSAL_tShMemHandle* phDevWupShMem, trGlobalData** pprGlobalData);
static tU32  DEV_WUP_u32UnmapGlobalData(OSAL_tShMemHandle hDevWupShMem, trGlobalData* prGlobalData);

static tU32  DEV_WUP_u32DataLock(OSAL_tSemHandle hSemDataAccess);
static tVoid DEV_WUP_vDataUnlock(OSAL_tSemHandle hSemDataAccess);

static tVoid DEV_WUP_vWriteErrorMemoryFormatted(const tChar * const coszFormatString, ...);

static tVoid DEV_WUP_vTraceFormatted(TR_tenTraceLevel enTraceLevel, const tChar * const coszFormatString, ...);

static tVoid DEV_WUP_vTraceIOControl(tS32 s32Fun, intptr_t pnArg, tU8* pu8TmpArgBuffer, tU32 u32OsalErrorCode);

static tVoid DEV_WUP_vSendIncMsg(const tU8 * const pu8IncSndBuffer, tU32 u32MessageLength);

static tU32  DEV_WUP_u32ResetProcessor(DEV_WUP_trResetProcessorInfo* prResetProcessorInfo, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_WUP_u32ResetApplicationProcessor(DEV_WUP_trResetProcessorInfo* prResetProcessorInfo, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_WUP_u32RequestProcessorResetViaSCC(DEV_WUP_trResetProcessorInfo* prResetProcessorInfo, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tBool DEV_WUP_bExecuteApplicationProcessorReset(tVoid);
static tVoid DEV_WUP_vDetermineAndHandleIntendedResetReason(DEV_WUP_trResetProcessorInfo* prResetProcessorInfo, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_WUP_u32DecreaseApplicationProcessorResetCounter(tVoid);

static tBool DEV_WUP_bEmmcRemountReadOnly(tVoid);
static tBool DEV_WUP_bTriggerEmmcPowerOff(tChar chCommand);
static tBool DEV_WUP_bSetGpioCpuRun(tS32 s32GpioControl);


/******************************************************************************/
/*                                                                            */
/* GLOBAL CONSTANTS                                                           */
/*                                                                            */
/******************************************************************************/

static const trIncMsgHandler g_carIncMsgHandlerList[] =
{
  { 
    DEV_WUP_C_U8_INC_MSGID_R_REJECT,
    DEV_WUP_C_U8_INC_MSGLEN_R_REJECT,
    FALSE,
    DEV_WUP_u8HandleIncMsg_RReject
  },
  {
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_CTRL_RESET_EXECUTION,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_CTRL_RESET_EXECUTION,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RCtrlResetExecution
  },
  {
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_PROC_RESET_REQUEST,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_PROC_RESET_REQUEST,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RProcResetRequest
  },
  {
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_STARTUP_FINISHED,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_STARTUP_FINISHED,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RStartupFinished
  },
  {
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RExtendPowerOffTimeout
  },
  {
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_AP_SUPERVISION_ERROR,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_AP_SUPERVISION_ERROR,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RAPSupervisionError
  },
  { 
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_EVENT_ACK,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_EVENT_ACK,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RWakeupEventAck
  },
  { 
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE_ACK,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_STATE_ACK,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RWakeupStateAck
  },
  { 
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE_VECTOR,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_STATE_VECTOR,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RWakeupStateVector
  },
  { 
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_STATE,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RWakeupState
  },
  { 
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_EVENT,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_EVENT,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RWakeupEvent
  },
  { 
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_REASON,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_REASON,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RWakeupReason
  },
  { 
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_SHUTDOWN_IN_PROGRESS,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_SHUTDOWN_IN_PROGRESS,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RShutdownInProgress
  },
  { 
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_SET_WAKEUP_CONFIG,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_SET_WAKEUP_CONFIG,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RSetWakeupConfig
  },
  { 
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE,
    FALSE,
    DEV_WUP_u8HandleIncMsg_RIndicateClientAppState
  },
  {
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_STARTUP_INFO,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_STARTUP_INFO,
    FALSE,
    DEV_WUP_u8HandleIncMsg_RStartupInfo
  },
  {
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_REQ_CLIENT_APP_STATE,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_REQ_CLIENT_APP_STATE,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RReqClientAppState
  },
  {
    DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_REQ_CLIENT_BOOT_MODE,
    DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_REQ_CLIENT_BOOT_MODE,
    TRUE,
    DEV_WUP_u8HandleIncMsg_RReqClientBootMode
  }
};

/******************************************************************************/
/*                                                                            */
/* GLOBAL VARIABLES                                                           */
/*                                                                            */
/******************************************************************************/

static trModuleData g_rModuleData;

/******************************************************************************/
/*                                                                            */
/* LOCAL FUNCTIONS                                                            */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Initialize the module local variables with default values.
*
*******************************************************************************/
static tVoid DEV_WUP_vInitModuleData(tVoid)
{
	tU32 u32Index;

	g_rModuleData.u32CreatorContextMagic = DEV_WUP_C_U32_CREATOR_CONTEXT_MAGIC;

	g_rModuleData.hMsgQueueOsalThread            = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hEventIncMsgThread             = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hEventClientThread             = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemDataAccess                 = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemIncMsgThread               = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemOsalMsgThread              = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemClientThread               = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemWakeupReasonMsgReceived    = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemStartupMsgReceived         = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemCtrlResetMsgReceived       = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemProcResetMsgReceived       = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemPwrOffMsgReceived          = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemAppStateMsgReceived        = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemOnOffEventAckMsgReceived   = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemOnOffStateAckMsgReceived   = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemSetWakeupConfigMsgReceived = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemStartupFinishedMsgReceived = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemShutdownMsgReceived        = OSAL_C_INVALID_HANDLE;

	g_rModuleData.hSharedMemory      = OSAL_ERROR;
	g_rModuleData.rTraceIODescriptor = OSAL_ERROR;
	g_rModuleData.rWupIODescriptor   = OSAL_ERROR;

	g_rModuleData.prGlobalData              = NULL;
	g_rModuleData.pDatagramSocketDescriptor = NULL;
	g_rModuleData.nSocketFileDescriptorInc  = -1;

	g_rModuleData.coszIncHostLocal  = DEV_WUP_C_STRING_INC_HOST_LOCAL;
	g_rModuleData.coszIncHostRemote = DEV_WUP_C_STRING_INC_HOST_REMOTE;

	OSAL_pvMemorySet(
		g_rModuleData.au8TraceCallbackBuffer,
		0,
		sizeof(g_rModuleData.au8TraceCallbackBuffer));

	OSAL_pvMemorySet(
		g_rModuleData.au8IncRcvBuffer,
		0,
		sizeof(g_rModuleData.au8IncRcvBuffer));

	g_rModuleData.u8IncMsgThreadState  = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;
	g_rModuleData.u8OsalMsgThreadState = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;
	g_rModuleData.u8ClientThreadState  = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++)
		g_rModuleData.ahClientEventHandle[u32Index] = OSAL_C_INVALID_HANDLE;

	g_rModuleData.bIncMsgCatVersionMatches      = FALSE;
	g_rModuleData.u8IncMsgCatMajorVersionServer = 0x00;
	g_rModuleData.u8IncMsgCatMinorVersionServer = 0x00;

	g_rModuleData.u8LatestReceivedOnOffEvent           = DEV_WUP_C_U8_ONOFF_EVENT_NO_WAKEUP_EVENT;
	g_rModuleData.u16LatestReceivedOnOffEventMsgHandle = 0;

	g_rModuleData.bGpioCpuRunAvailable = FALSE;
	g_rModuleData.bGpioCpuPwfOffAvailable = FALSE;

	g_rModuleData.u32ConnectToFakeIncDeviceMagic = 0x00000000;

	g_rModuleData.bSccIncCommunicationEstablished = FALSE;
}

/*******************************************************************************
*
* Initialize the global variables which are located in the shared-memory with 
* default values.
*
*******************************************************************************/
static tVoid DEV_WUP_vInitGlobalData(tVoid)
{
	tU32 u32Index;

	g_rModuleData.prGlobalData->u8NumberOfClients                         = 0;
	g_rModuleData.prGlobalData->u8NumberOfRegisteredClients               = 0;
	g_rModuleData.prGlobalData->u8NumberOfOnOffReasonChangedRegistrations = 0;
	g_rModuleData.prGlobalData->u32LatestAssignedClientId                 = 0;

	g_rModuleData.prGlobalData->u8Starttype           = DEV_WUP_C_U8_STARTTYPE_UNKNOWN;
	g_rModuleData.prGlobalData->bRStartupInfoReceived = FALSE;

	g_rModuleData.prGlobalData->u8ApplicationModeSCC = DEV_WUP_C_U8_APPLICATION_MODE_UNKNOWN;
	g_rModuleData.prGlobalData->u8ApplicationModeAP  = DEV_WUP_C_U8_APPLICATION_MODE_NORMAL;

	g_rModuleData.prGlobalData->u8WupReason         = DEV_WUP_C_U8_WAKEUP_REASON_UNKNOWN;

	g_rModuleData.prGlobalData->bRWupReasonReceived = FALSE;
	g_rModuleData.prGlobalData->bRIndicateClientAppStateReceived = FALSE;

	g_rModuleData.prGlobalData->u32AcknowledgedWakeupReasonsMask = 0x00000000;

	g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 = 0;
	g_rModuleData.prGlobalData->rOnOffStates.u16MsgHandle     = 0;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS;
	     u32Index++) {
		g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32Index].u8Event      = DEV_WUP_C_U8_ONOFF_EVENT_NO_WAKEUP_EVENT;
		g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32Index].u16MsgHandle = 0;
		g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32Index].u32Timestamp = 0;
	}

	g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead = 0;
	g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail = 0;
	g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u8NumberOfUsedElements = 0;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId                          = DEV_WUP_C_U32_CLIENT_ID_INVALID;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName[0]           = '\0';
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u8OnOffReasonChangedNotificationMode = DEV_WUP_C_U8_NOTIFY_NONE;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].bIsSystemMaster                      = FALSE;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead               = 0;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u16LatestAckOnOffEventMsgHandle      = 0;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].bNotifyAPSupervisionError            = FALSE;
	}

	g_rModuleData.prGlobalData->u8ResetReasonSCC = DEV_WUP_C_U8_RESET_REASON_POR;

	g_rModuleData.prGlobalData->u8LatestResetProcessor = DEV_WUP_C_U8_INVALID_PROCESSOR;

	g_rModuleData.prGlobalData->u16ReportedPowerOffTimeoutS = 0;

	g_rModuleData.prGlobalData->u16OnOffEventMsgHandleOfLatestAckResponse = 0;
	g_rModuleData.prGlobalData->u16OnOffStateMsgHandleOfLatestAckResponse = 0;
	
	g_rModuleData.prGlobalData->rPramMirror.u16LatestAcknowledgedOnOffEventMsgHandle = 0;
	g_rModuleData.prGlobalData->rPramMirror.u16ResetReasonAP                         = DEV_WUP_C_U16_RESET_REASON_POWER_ON;
	g_rModuleData.prGlobalData->rPramMirror.u8ResetClassificationAP                  = DEV_WUP_C_U8_RESET_REASON_AP_INTENDED;
	OSAL_pvMemorySet(
		g_rModuleData.prGlobalData->rPramMirror.auReservedAndFillUpTo32Bytes,
		0,
		27);

	g_rModuleData.prGlobalData->rPramLocal.u16LatestAcknowledgedOnOffEventMsgHandle = 0;
	g_rModuleData.prGlobalData->rPramLocal.u16ResetReasonAP                         = DEV_WUP_C_U16_RESET_REASON_POWER_ON;
	g_rModuleData.prGlobalData->rPramLocal.u8ResetClassificationAP                  = DEV_WUP_C_U8_RESET_REASON_AP_INTENDED;
	OSAL_pvMemorySet(
		g_rModuleData.prGlobalData->rPramLocal.auReservedAndFillUpTo32Bytes,
		0,
		27);

	g_rModuleData.prGlobalData->bSystemShutdownSuccess = FALSE;

	g_rModuleData.prGlobalData->u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_NONE;
}

/*******************************************************************************
*
* Read device related configuration values from the OSAL registry.
*
*******************************************************************************/
static tVoid DEV_WUP_vReadRegistryValues(tVoid)
{
	OSAL_tIODescriptor    rRegistryIODescriptor = OSAL_ERROR;
	OSAL_trIOCtrlRegistry rIOCtrlRegistry       = { 0 };
	tU32                  u32RegistryValue      = 0x00000000;

	rRegistryIODescriptor = OSAL_IOOpen(
					OSAL_C_STRING_DEVICE_REGISTRY"/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/DEVICES/DEV_WUP/",
					OSAL_EN_READWRITE);

	if (rRegistryIODescriptor == OSAL_ERROR)
		return;

	rIOCtrlRegistry.pcos8Name = (tCS8*)"CONNECT_TO_FAKE";
	rIOCtrlRegistry.ps8Value  = (tU8*)&u32RegistryValue;
	rIOCtrlRegistry.u32Size   = sizeof(tS32);

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK)
			g_rModuleData.u32ConnectToFakeIncDeviceMagic = u32RegistryValue;

	if (OSAL_s32IOClose(rRegistryIODescriptor) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32ReadRegistryValues() => OSAL_IOClose(DEVICE_REGISTRY) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
}

/*******************************************************************************
*
* Read data from persistent RAM.
*
*******************************************************************************/
static tVoid DEV_WUP_vReadPersistentRamData(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	OSAL_tIODescriptor rIODescriptor = OSAL_ERROR;

	if (DEV_WUP_u32DataLock(hSemDataAccess) != OSAL_E_NOERROR)
		return;

	rIODescriptor = OSAL_IOOpen(
				OSAL_C_STRING_DEVICE_PRAM"/dev_wup",
				OSAL_EN_READWRITE);

	if (rIODescriptor == OSAL_ERROR) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_vReadPersistentRamData() => OSAL_IOOpen(DEVICE_PRAM) for 'dev_wup' failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		goto error_io_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32IOControl(
		rIODescriptor,
		OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK,
		0) == OSAL_ERROR) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vReadPersistentRamData() => OSAL_s32IOControl(DEV_PRAM_SEEK) for 'dev_wup' failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		goto error_pram_access; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	// Reading of persistant RAM data could have failed because they are invalidated due to a longer disconnection from power supply.
	if (OSAL_s32IORead(
		rIODescriptor,
		(tPS8)&prGlobalData->rPramMirror,
		sizeof(trPersistentRamData)) == OSAL_ERROR) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_SYSTEM_MIN,
			"Lost persistent RAM data due to disconnection from power supply. Restart with default values.",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		
		// Write default values to the persistent RAM data to make it valid again.
		if (OSAL_s32IOWrite(
			rIODescriptor,
			(tPCS8)&prGlobalData->rPramMirror,
			sizeof(trPersistentRamData)) == OSAL_ERROR) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vReadPersistentRamData() => OSAL_s32IOWrite(DEVICE_PRAM) for 'dev_wup' failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
			goto error_pram_access; /*lint !e801, authorized LINT-deactivation #<71> */
		}

		if (OSAL_s32IOControl(
			rIODescriptor,
			OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK,
			0) == OSAL_ERROR) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vReadPersistentRamData() => OSAL_s32IOControl(DEV_PRAM_SEEK) for 'dev_wup' failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
			goto error_pram_access; /*lint !e801, authorized LINT-deactivation #<71> */
		}

		// Now the read access MUST succeed because we have just written the data into the persistent RAM.
		if (OSAL_s32IORead(
			rIODescriptor,
			(tPS8)&prGlobalData->rPramMirror,
			sizeof(trPersistentRamData)) == OSAL_ERROR) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vReadPersistentRamData() => OSAL_s32IORead(DEVICE_PRAM) for 'dev_wup' failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
			goto error_pram_access; /*lint !e801, authorized LINT-deactivation #<71> */
		}
	}

	// Transfer content of PRAM-Mirror to PRAM-Local.
	prGlobalData->rPramLocal.u16LatestAcknowledgedOnOffEventMsgHandle = prGlobalData->rPramMirror.u16LatestAcknowledgedOnOffEventMsgHandle;
	prGlobalData->rPramLocal.u16ResetReasonAP                         = prGlobalData->rPramMirror.u16ResetReasonAP;
	prGlobalData->rPramLocal.u8ResetClassificationAP                  = prGlobalData->rPramMirror.u8ResetClassificationAP;

	// The persistent RAM stayed valid over the reset, so we must set the AP reset reason to UNKNOWN
	// and EXCEPTIONAL in the persistent RAM to be able to properly determine upcoming unknown resets.
	prGlobalData->rPramMirror.u16ResetReasonAP        = DEV_WUP_C_U16_RESET_REASON_UNKNOWN;
	prGlobalData->rPramMirror.u8ResetClassificationAP = DEV_WUP_C_U8_RESET_REASON_AP_EXCEPTIONAL;

	if (OSAL_s32IOControl(
		rIODescriptor,
		OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK,
		0) == OSAL_ERROR) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vReadPersistentRamData() => OSAL_s32IOControl(DEV_PRAM_SEEK) for 'dev_wup' failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		goto error_pram_access; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32IOWrite(
		rIODescriptor,
		(tPCS8)&prGlobalData->rPramMirror,
		sizeof(trPersistentRamData)) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vReadPersistentRamData() => OSAL_s32IOWrite(DEVICE_PRAM) for 'dev_wup' failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_pram_access:

	if (OSAL_s32IOClose(rIODescriptor) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vReadPersistentRamData() => OSAL_IOClose(DEVICE_PRAM) for 'dev_wup' failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_io_open:

	DEV_WUP_vDataUnlock(hSemDataAccess);
}

/*******************************************************************************
*
* Write data to persistent RAM.
*
*******************************************************************************/
static tVoid DEV_WUP_vWritePersistentRamData(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	OSAL_tIODescriptor rIODescriptor = OSAL_ERROR;

	if (DEV_WUP_u32DataLock(hSemDataAccess) != OSAL_E_NOERROR)
		return;

	rIODescriptor = OSAL_IOOpen(
				OSAL_C_STRING_DEVICE_PRAM"/dev_wup",
				OSAL_EN_READWRITE);

	if (rIODescriptor == OSAL_ERROR) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_vWritePersistentRamData() => OSAL_IOOpen(DEVICE_PRAM) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		goto error_io_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32IOControl(
		rIODescriptor,
		OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK,
		0) == OSAL_ERROR) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vWritePersistentRamData() => OSAL_s32IOControl(DEV_PRAM_SEEK) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		goto error_pram_access; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32IOWrite(
		rIODescriptor,
		(tPCS8)&prGlobalData->rPramMirror,
		sizeof(trPersistentRamData)) != OSAL_ERROR) {
			// Transfer content of PRAM-Mirror to PRAM-Local.
			// Consider special use case for UNKNOWN and EXCEPTIONAL reset where the content of the mirror local PRAM may differ.
			prGlobalData->rPramLocal.u16LatestAcknowledgedOnOffEventMsgHandle = prGlobalData->rPramMirror.u16LatestAcknowledgedOnOffEventMsgHandle;

			if (prGlobalData->rPramMirror.u16ResetReasonAP != DEV_WUP_C_U16_RESET_REASON_UNKNOWN)
				prGlobalData->rPramLocal.u16ResetReasonAP = prGlobalData->rPramMirror.u16ResetReasonAP;

			if (prGlobalData->rPramMirror.u8ResetClassificationAP != DEV_WUP_C_U8_RESET_REASON_AP_EXCEPTIONAL)
				prGlobalData->rPramLocal.u8ResetClassificationAP = prGlobalData->rPramMirror.u8ResetClassificationAP;
	} else {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vWritePersistentRamData() => OSAL_s32IOWrite(DEVICE_PRAM) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

error_pram_access:

	if (OSAL_s32IOClose(rIODescriptor) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vWritePersistentRamData() => OSAL_IOClose(DEVICE_PRAM) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_io_open:

	DEV_WUP_vDataUnlock(hSemDataAccess);
}

/*******************************************************************************
*
* Determine the possible application modes NORMAL or DOWNLOAD of the application
* processor by checking the existance of some files in the filesystem.
*
*******************************************************************************/
static tVoid DEV_WUP_vDetermineApplicationMode(tVoid)
{
	tU32               u32OsalErrorCode = OSAL_E_NOERROR;
	OSAL_tIODescriptor hIODescriptor    = OSAL_ERROR;

	// Default application mode = NORMAL
	g_rModuleData.prGlobalData->u8ApplicationModeAP = DEV_WUP_C_U8_APPLICATION_MODE_NORMAL;

	hIODescriptor = OSAL_IOOpen(DEV_WUP_C_STRING_DOWNLOAD_MODE_1_FILE_NAME, OSAL_EN_READONLY);

	if (hIODescriptor == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		if (u32OsalErrorCode != OSAL_E_DOESNOTEXIST) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vDetermineApplicationMode() => Access to file '%s' failed with error code = '%s'",
				DEV_WUP_C_STRING_DOWNLOAD_MODE_1_FILE_NAME,
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
			return;
		}

		hIODescriptor = OSAL_IOOpen(DEV_WUP_C_STRING_DOWNLOAD_MODE_2_FILE_NAME, OSAL_EN_READONLY);
		if (hIODescriptor == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			if (u32OsalErrorCode != OSAL_E_DOESNOTEXIST) {
				DEV_WUP_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_WUP_vDetermineApplicationMode() => Access to file '%s' failed with error code = '%s'",
					DEV_WUP_C_STRING_DOWNLOAD_MODE_2_FILE_NAME,
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
			}
			return;
		} else {
			(tVoid)OSAL_s32IOClose(hIODescriptor);
		}
	} else {
		(tVoid)OSAL_s32IOClose(hIODescriptor);
	}

	g_rModuleData.prGlobalData->u8ApplicationModeAP = DEV_WUP_C_U8_APPLICATION_MODE_DOWNLOAD;
}

/*******************************************************************************
*
* Initialization function of the OSAL device which creates the necessary OSAL
* resources like semaphores, events, message-queues and threads and brings the
* device into an operational (initialized) state.
*
*******************************************************************************/
static tU32 DEV_WUP_u32Init(tVoid)
{
	tU32 u32OsalErrorCode    = OSAL_E_NOERROR;
	tU32 u32TmpOsalErrorCode = OSAL_E_NOERROR;

	OSAL_trProcessControlBlock rProcessControlBlock;

	DEV_WUP_vInitModuleData();

	DEV_WUP_vReadRegistryValues();

	if (OSAL_s32ProcessControlBlock(
		OSAL_ProcessWhoAmI(),
		&rProcessControlBlock) == OSAL_ERROR)
		return OSAL_u32ErrorCode();

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_1, 
		"DEV_WUP_u32Init() => Called from process '%.15s' (%u)",
		rProcessControlBlock.szName,
		rProcessControlBlock.id);

	// Determine if the INC fake device should be used instead of the real INC communication to the
	// System-Communication-Controller (V850) depending on the build environment and other configurations.
	#if (defined (GEN3ARM) || defined (GEN4ARM) || defined (GEN4INTEL)) // GEN3ARM = iMX, GEN4ARM = RCar, GEN4INTEL = Broxton
		// Check for integration tests with inital process name = 'dev_wup_client_process'
		// and INC fake device configuration via the OSAL registry.
		if ((OSAL_s32StringNCompare(
			rProcessControlBlock.szName,
			"dev_wup_client_",
			15) == 0) || (g_rModuleData.u32ConnectToFakeIncDeviceMagic == DEV_WUP_C_U32_CONNECT_TO_FAKE_INC_DEVICE_MAGIC)) {
			g_rModuleData.coszIncHostLocal  = DEV_WUP_C_STRING_INC_HOST_LOCAL_FAKE;
			g_rModuleData.coszIncHostRemote = DEV_WUP_C_STRING_INC_HOST_REMOTE_FAKE;
		}
	#elif (defined (LinuxX86Make) || defined (_LINUXX86_64_)) // LinuxX86Make or _LINUXX86_64_= Ubuntu Virtual Box (Unit-Test or Real target code execution on host machine)
		if (g_rModuleData.u32ConnectToFakeIncDeviceMagic == DEV_WUP_C_U32_CONNECT_TO_FAKE_INC_DEVICE_MAGIC) {
			g_rModuleData.coszIncHostLocal  = DEV_WUP_C_STRING_INC_HOST_LOCAL_FAKE;
			g_rModuleData.coszIncHostRemote = DEV_WUP_C_STRING_INC_HOST_REMOTE_FAKE;
		}
	#elif defined (GEN3X86) // GEN3X86 = LSim (Gen3 as well as Gen4)
		g_rModuleData.coszIncHostLocal  = DEV_WUP_C_STRING_INC_HOST_LOCAL_FAKE;
		g_rModuleData.coszIncHostRemote = DEV_WUP_C_STRING_INC_HOST_REMOTE_FAKE;
	#else
		#error "/dev/wup only supports environments Gen3ArmMake, Gen4Broxton, Gen4RCar, LinuxX86Make, LinuxX86_64, Gen3X86Make or Gen4Lsim"
	#endif

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_1,
		"DEV_WUP_u32Init() => Used INC socket address names : Local = '%s' and Remote = '%s'",
		g_rModuleData.coszIncHostLocal,
		g_rModuleData.coszIncHostRemote);

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_DATA_NAME,
		&g_rModuleData.hSemDataAccess,
		(tU32) 1) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	g_rModuleData.hSharedMemory = OSAL_SharedMemoryCreate(
					DEV_WUP_C_STRING_SHARED_MEM_NAME,
					OSAL_EN_READWRITE, 
					sizeof(trGlobalData));

	if (OSAL_ERROR == g_rModuleData.hSharedMemory) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_shared_memory_create; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.prGlobalData = (trGlobalData*) OSAL_pvSharedMemoryMap(
							g_rModuleData.hSharedMemory,
							OSAL_EN_READWRITE,
							sizeof(trGlobalData),
							0);

	if (NULL == g_rModuleData.prGlobalData) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_shared_memory_map; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_WUP_vInitGlobalData();

	DEV_WUP_vReadPersistentRamData(g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess);

	DEV_WUP_vDetermineApplicationMode();

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_WAKEUP_MSG_RECEIVED_NAME,
		&g_rModuleData.hSemWakeupReasonMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_wakeup; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME,
		&g_rModuleData.hSemStartupMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_startup; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME,
		&g_rModuleData.hSemCtrlResetMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_ctrl_reset; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME,
		&g_rModuleData.hSemProcResetMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_proc_reset; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME,
		&g_rModuleData.hSemPwrOffMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_pwr_off; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME,
		&g_rModuleData.hSemAppStateMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_app_state; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_ONOFF_EVENT_ACK_RECEIVED_NAME,
		&g_rModuleData.hSemOnOffEventAckMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_onoff_event; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_ONOFF_STATE_ACK_RECEIVED_NAME,
		&g_rModuleData.hSemOnOffStateAckMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_onoff_state; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_WAKEUP_CFG_MSG_RECEIVED_NAME,
		&g_rModuleData.hSemSetWakeupConfigMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_wakeup_config; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_START_FINISH_MSG_RECEIVED_NAME,
		&g_rModuleData.hSemStartupFinishedMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_startup_finished; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_SHUTDOWN_MSG_RECEIVED_NAME,
		&g_rModuleData.hSemShutdownMsgReceived,
		(tU32) 0) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_sem_create_shutdown; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	u32OsalErrorCode =  DEV_WUP_u32CreateClientNotificationEvents();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_create_notify_events; /*lint !e801, authorized LINT-deactivation #<71> */

	g_rModuleData.bSccIncCommunicationEstablished = DEV_WUP_bEstablishSccIncCom();

	u32OsalErrorCode = DEV_WUP_u32InstallOsalMsgThread();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_install_osal_msg_thread; /*lint !e801, authorized LINT-deactivation #<71> */

	u32OsalErrorCode = DEV_WUP_u32InstallIncMsgThread();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_install_inc_msg_thread; /*lint !e801, authorized LINT-deactivation #<71> */

	g_rModuleData.bGpioCpuRunAvailable = DEV_WUP_bSetGpioCpuRun(OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE);

	g_rModuleData.bGpioCpuPwfOffAvailable = DEV_WUP_bSetGpioCpuPwrOff(OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE);

	DEV_WUP_vRegisterTraceCallback();

	DEV_WUP_vIncSend_CIndicateClientAppState(
		g_rModuleData.prGlobalData->u8ApplicationModeAP,
		g_rModuleData.prGlobalData->rPramLocal.u8ResetClassificationAP);

	DEV_WUP_vIncSend_CGetData(DEV_WUP_C_U8_GET_DATA_MODE_ALL_DATA, 0);

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_1,
		"DEV_WUP_u32Init() left with success");

	return OSAL_E_NOERROR;

error_install_inc_msg_thread:

	u32TmpOsalErrorCode = DEV_WUP_u32UninstallOsalMsgThread();

	if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => DEV_WUP_u32UninstallOsalMsgThread() failed with error code = %s",
			OSAL_coszErrorText(u32TmpOsalErrorCode));

error_install_osal_msg_thread:

	if (DEV_WUP_bReleaseSccIncCom() == FALSE)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => DEV_WUP_bReleaseSccIncCom() failed");

	DEV_WUP_vDeleteClientNotificationEvents();

error_create_notify_events:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemShutdownMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_SHUTDOWN_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemShutdownMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(SHUTDOWN_MSG_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(StartupFinishedMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_shutdown:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemStartupFinishedMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_START_FINISH_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemStartupFinishedMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(START_FINISH_MSG_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(StartupFinishedMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_startup_finished:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemSetWakeupConfigMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_WAKEUP_CFG_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemSetWakeupConfigMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(WAKEUP_CFG_MSG_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(SetWakeupConfigMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_wakeup_config:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemOnOffStateAckMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_ONOFF_STATE_ACK_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemOnOffStateAckMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(ONOFF_STATE_ACK_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(OnOffStateAckMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_onoff_state:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemOnOffEventAckMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_ONOFF_EVENT_ACK_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemOnOffEventAckMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(ONOFF_EVENT_ACK_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(OnOffEventAckMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_onoff_event:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemAppStateMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemAppStateMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(APP_STATE_MSG_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(AppStateMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_app_state:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemPwrOffMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemPwrOffMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(PWR_OFF_MSG_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(PwrOffMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_pwr_off:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemProcResetMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemProcResetMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(PROC_RESET_MSG_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(ProcResetMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_proc_reset:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemCtrlResetMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemCtrlResetMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(CTRL_RESET_MSG_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(CtrlResetMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_ctrl_reset:


	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemStartupMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemStartupMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(STARTUP_MSG_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(StartupMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_startup:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemWakeupReasonMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_WAKEUP_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemWakeupReasonMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(WAKEUP_MSG_RECEIVED) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(WakeupReasonMsgReceived) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_wakeup:

	if (OSAL_s32SharedMemoryUnmap(g_rModuleData.prGlobalData, sizeof(trGlobalData)) == OSAL_OK)
		g_rModuleData.prGlobalData = NULL;
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SharedMemoryUnmap() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_shared_memory_map:

	if (OSAL_s32SharedMemoryClose(g_rModuleData.hSharedMemory) == OSAL_OK)
		if (OSAL_s32SharedMemoryDelete(DEV_WUP_C_STRING_SHARED_MEM_NAME) == OSAL_OK)
			g_rModuleData.hSharedMemory = OSAL_ERROR;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SharedMemoryDelete() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SharedMemoryClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_shared_memory_create:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemDataAccess) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_DATA_NAME) == OSAL_OK)
			g_rModuleData.hSemDataAccess = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32Init() => OSAL_s32SemaphoreDelete(DATA) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Init() => OSAL_s32SemaphoreClose(DataAccess) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* De-Initialization function of the OSAL device which deletes all OSAL
* resources like semaphores, events, message-queues and threads and again brings
* the device into a non operational (de-initialzed) state.
*
*******************************************************************************/
static tU32 DEV_WUP_u32Deinit(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_FATAL,
		"DEV_WUP_u32Deinit() entered. Creator context magic = 0x%08X",
		g_rModuleData.u32CreatorContextMagic);

	if (g_rModuleData.hSemDataAccess == OSAL_C_INVALID_HANDLE)
		return OSAL_E_NOTINITIALIZED;

	DEV_WUP_vUnregisterTraceCallback();

	u32OsalErrorCode = DEV_WUP_u32UninstallClientThread();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = DEV_WUP_u32UninstallIncMsgThread();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = DEV_WUP_u32UninstallOsalMsgThread();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if (DEV_WUP_bReleaseSccIncCom() == FALSE)
		return OSAL_E_UNKNOWN;

	DEV_WUP_vDeleteClientNotificationEvents();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemStartupMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemStartupMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemWakeupReasonMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_WAKEUP_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemWakeupReasonMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemCtrlResetMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemCtrlResetMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemProcResetMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemProcResetMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemPwrOffMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemPwrOffMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemAppStateMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemAppStateMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemOnOffEventAckMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_ONOFF_EVENT_ACK_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemOnOffEventAckMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemOnOffStateAckMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_ONOFF_STATE_ACK_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemOnOffStateAckMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemSetWakeupConfigMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_WAKEUP_CFG_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemSetWakeupConfigMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemStartupFinishedMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_START_FINISH_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemStartupFinishedMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemShutdownMsgReceived) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_SHUTDOWN_MSG_RECEIVED_NAME) == OSAL_OK)
			g_rModuleData.hSemShutdownMsgReceived = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SharedMemoryUnmap(g_rModuleData.prGlobalData, sizeof(trGlobalData)) == OSAL_OK)
		g_rModuleData.prGlobalData = NULL;
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SharedMemoryClose(g_rModuleData.hSharedMemory) == OSAL_OK)
		if (OSAL_s32SharedMemoryDelete(DEV_WUP_C_STRING_SHARED_MEM_NAME) == OSAL_OK)
			g_rModuleData.hSharedMemory = OSAL_ERROR;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemDataAccess) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_DATA_NAME) == OSAL_OK)
			g_rModuleData.hSemDataAccess = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_1,
		"DEV_WUP_u32Deinit() left with success");

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* Open function of the OSAL device.
*
*******************************************************************************/
static tU32 DEV_WUP_u32Open(tVoid)
{
	tU32                       u32OsalErrorCode     = OSAL_E_NOERROR;
	tU32                       u32TmpOsalErrorCode  = OSAL_E_NOERROR;
	trGlobalData*              prGlobalData         = NULL;
	OSAL_tShMemHandle          hDevWupShMem         = OSAL_ERROR;
	OSAL_tSemHandle            hSemDataAccess       = OSAL_C_INVALID_HANDLE;
	OSAL_trThreadControlBlock  rThreadControlBlock;
	OSAL_trProcessControlBlock rProcessControlBlock;

	if (OSAL_s32ProcessControlBlock(
		OSAL_ProcessWhoAmI(),
		&rProcessControlBlock) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32ThreadControlBlock(
		OSAL_ThreadWhoAmI(),
		&rThreadControlBlock) == OSAL_ERROR) {
		// I can be a valid use case that a thread control block is not yet available if DEV_WUP_u32Open() is called from a process main() function.
		// With the second call of the same function the pseudo thread control block should be created by OSAL. Try for a second time ...
		if (OSAL_s32ThreadControlBlock(
			OSAL_ThreadWhoAmI(),
			&rThreadControlBlock) == OSAL_ERROR)
				return OSAL_u32ErrorCode();
	}

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_1,
		"DEV_WUP_u32Open() => Called from process '%.15s' (%u) and thread '%.15s' (%u)",
		rProcessControlBlock.szName,
		rProcessControlBlock.id,
		rThreadControlBlock.szName,
		rThreadControlBlock.id);

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_DATA_NAME,
		&hSemDataAccess) == OSAL_ERROR) {
		
		u32OsalErrorCode = OSAL_u32ErrorCode();
		
		// If this central semaphore doesn't exist, then the device wasn't initialized successfully.
		if (u32OsalErrorCode == OSAL_E_DOESNOTEXIST)
			u32OsalErrorCode = OSAL_E_NOTINITIALIZED;

		return u32OsalErrorCode;
	}

	u32OsalErrorCode = DEV_WUP_u32MapGlobalData(
		&hDevWupShMem,
		&prGlobalData);
		
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_map_global_data; /*lint !e801, authorized LINT-deactivation #<71> */

	u32OsalErrorCode = DEV_WUP_u32AddClient(prGlobalData, hSemDataAccess);

	u32TmpOsalErrorCode = DEV_WUP_u32UnmapGlobalData(hDevWupShMem,prGlobalData);
	
	if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Open() => DEV_WUP_u32UnmapGlobalData() failed with error code = %s",
			OSAL_coszErrorText(u32TmpOsalErrorCode));

error_map_global_data:

	if (OSAL_s32SemaphoreClose(hSemDataAccess) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Open() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return (u32OsalErrorCode);
}

/*******************************************************************************
*
* Close function of the OSAL device.
*
*******************************************************************************/
static tU32 DEV_WUP_u32Close(tVoid)
{
	tU32              u32OsalErrorCode    = OSAL_E_NOERROR;
	tU32              u32TmpOsalErrorCode = OSAL_E_NOERROR;
	trGlobalData*     prGlobalData        = NULL;
	OSAL_tShMemHandle hDevWupShMem        = OSAL_ERROR;
	OSAL_tSemHandle   hSemDataAccess      = OSAL_C_INVALID_HANDLE;

	OSAL_trThreadControlBlock  rThreadControlBlock;
	OSAL_trProcessControlBlock rProcessControlBlock;

	if (OSAL_s32ProcessControlBlock(
		OSAL_ProcessWhoAmI(),
		&rProcessControlBlock) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32ThreadControlBlock(
		OSAL_ThreadWhoAmI(),
		&rThreadControlBlock) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_1,
		"DEV_WUP_u32Close() => Called from process '%.15s' (%u) and thread '%.15s' (%u)",
		rProcessControlBlock.szName,
		rProcessControlBlock.id,
		rThreadControlBlock.szName,
		rThreadControlBlock.id);

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_DATA_NAME,
		&hSemDataAccess) == OSAL_ERROR) {
		
		u32OsalErrorCode = OSAL_u32ErrorCode();
		
		// If this central semaphore doesn't exist, then the device wasn't initialized successfully.
		if (u32OsalErrorCode == OSAL_E_DOESNOTEXIST)
			u32OsalErrorCode = OSAL_E_NOTINITIALIZED;

		return u32OsalErrorCode;
	}

	u32OsalErrorCode = DEV_WUP_u32MapGlobalData(
		&hDevWupShMem,
		&prGlobalData);
		
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_map_global_data; /*lint !e801, authorized LINT-deactivation #<71> */

	u32OsalErrorCode = DEV_WUP_u32RemoveClient(prGlobalData, hSemDataAccess);

	u32TmpOsalErrorCode = DEV_WUP_u32UnmapGlobalData(hDevWupShMem,prGlobalData);
	
	if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Open() => DEV_WUP_u32UnmapGlobalData() failed with error code = %s",
			OSAL_coszErrorText(u32TmpOsalErrorCode));

error_map_global_data:

	if (OSAL_s32SemaphoreClose(hSemDataAccess) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Close() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return (u32OsalErrorCode);
}

/*******************************************************************************
*
* IO-control function of the OSAL device.
*
*******************************************************************************/
static tU32 DEV_WUP_u32Control(tS32 s32Fun, intptr_t pnArg)
{
	tU32               u32OsalErrorCode    = OSAL_E_NOERROR;
	tU32               u32TmpOsalErrorCode = OSAL_E_NOERROR;
	trGlobalData*      prGlobalData        = NULL;
	OSAL_tShMemHandle  hDevWupShMem        = OSAL_ERROR;
	OSAL_tSemHandle    hSemDataAccess      = OSAL_C_INVALID_HANDLE;
	tU8                u8TmpArgBuffer[2]   = {0, 0};

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_DATA_NAME, 
		&hSemDataAccess) == OSAL_ERROR) {
		
		u32OsalErrorCode = OSAL_u32ErrorCode();
		
		// If this central semaphore doesn't exist, then the device wasn't initialized successfully.
		if (u32OsalErrorCode == OSAL_E_DOESNOTEXIST)
			u32OsalErrorCode = OSAL_E_NOTINITIALIZED;

		return u32OsalErrorCode;
	}

	u32OsalErrorCode = DEV_WUP_u32MapGlobalData(
		&hDevWupShMem,
		&prGlobalData);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_shared_memory_map; /*lint !e801, authorized LINT-deactivation #<71> */

	switch (s32Fun) {
	case OSAL_C_S32_IOCTRL_WUP_SHUTDOWN : {
		tU8 u8ShutdownType = (tU8)pnArg;

		u32OsalErrorCode = DEV_WUP_u32TriggerSystemShutdown(
					u8ShutdownType,
					prGlobalData,
					hSemDataAccess);
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_STARTTYPE : {
		tPU8 pu8Starttype = (tPU8)pnArg;

		if (pu8Starttype != NULL) {
		#if defined (GEN3X86) // GEN3X86 = LSim
			u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
	
			if (u32OsalErrorCode == OSAL_E_NOERROR) {
				prGlobalData->u8Starttype = DEV_WUP_C_U8_STARTTYPE_COLDSTART;
				*pu8Starttype = prGlobalData->u8Starttype;
				DEV_WUP_vDataUnlock(hSemDataAccess);
			}
		#else
			u32OsalErrorCode = DEV_WUP_u32GetStarttype(
						pu8Starttype,
						prGlobalData,
						hSemDataAccess);
		#endif
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_WAKEUP_REASON : {
		tPU8 pu8WakeupReason = (tPU8)pnArg;

		if (pu8WakeupReason != NULL) {

		#if defined (GEN3X86) // GEN3X86 = LSim
			u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
	
			if (u32OsalErrorCode == OSAL_E_NOERROR) {
				prGlobalData->u8WupReason = DEV_WUP_C_U8_WAKEUP_REASON_IGN_PIN;
				*pu8WakeupReason = prGlobalData->u8WupReason;
				DEV_WUP_vDataUnlock(hSemDataAccess);
			}
		#else
			u32OsalErrorCode = DEV_WUP_u32GetWakeupReason(
						pu8WakeupReason,
						prGlobalData,
						hSemDataAccess);
		#endif
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT: {
		DEV_WUP_trClientRegistration* prClientRegistration = (DEV_WUP_trClientRegistration*)pnArg;

		if (prClientRegistration != NULL)
			u32OsalErrorCode = DEV_WUP_u32RegisterClient(
						prClientRegistration,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT: {
		tU32 u32ClientId = (tU32)pnArg;

		u32OsalErrorCode = DEV_WUP_u32UnregisterClient(
					u32ClientId, 
					prGlobalData,
					hSemDataAccess);
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_CONFIGURE_WAKEUP_REASONS : {
		tU32 u32WakeupReasonsMask = (tU32)pnArg;

		u32OsalErrorCode = DEV_WUP_u32ConfigureWakeupReasons(
					u32WakeupReasonsMask,
					prGlobalData,
					hSemDataAccess);
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_REGISTER_ONOFF_REASON_CHANGED_NOTIFICATION : {
		DEV_WUP_trOnOffReasonChangedRegistration* prOnOffReasonChangedRegistration = (DEV_WUP_trOnOffReasonChangedRegistration*)pnArg;

		if (prOnOffReasonChangedRegistration != NULL)
			u32OsalErrorCode = DEV_WUP_u32RegisterOnOffReasonChanged(
						prOnOffReasonChangedRegistration,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_UNREGISTER_ONOFF_REASON_CHANGED_NOTIFICATION : {
		tU32 u32ClientId = (tU32)pnArg;

		u32OsalErrorCode = DEV_WUP_u32UnregisterOnOffReasonChanged(
			u32ClientId,
			prGlobalData,
			hSemDataAccess);
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_RESET_PROCESSOR : {
		DEV_WUP_trResetProcessorInfo* prResetProcessorInfo = (DEV_WUP_trResetProcessorInfo*)pnArg;

		if (prResetProcessorInfo != NULL) {
			u32OsalErrorCode = DEV_WUP_u32ResetProcessor(
				prResetProcessorInfo,
				prGlobalData,
				hSemDataAccess);
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_EVENTS : {
		DEV_WUP_trOnOffEventHistory* prOnOffEventHistory = (DEV_WUP_trOnOffEventHistory*)pnArg;

		if (prOnOffEventHistory != NULL) {
			u32OsalErrorCode = DEV_WUP_u32GetOnOffEvents(
						prOnOffEventHistory,
						prGlobalData,
						hSemDataAccess);
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_ACKNOWLEDGE_ONOFF_EVENT : {
		DEV_WUP_trOnOffEventAcknowledge* prOnOffEventAcknowledge = (DEV_WUP_trOnOffEventAcknowledge*)pnArg;

		if (prOnOffEventAcknowledge != NULL) {
			u32OsalErrorCode = DEV_WUP_u32AcknowledgeOnOffEvent(
						prOnOffEventAcknowledge,
						prGlobalData,
						hSemDataAccess);
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_ACKNOWLEDGE_ONOFF_STATE : {
		DEV_WUP_trOnOffStateAcknowledge* prOnOffStateAcknowledge = (DEV_WUP_trOnOffStateAcknowledge*)pnArg;

		u32OsalErrorCode = DEV_WUP_u32AcknowledgeOnOffState(
					prOnOffStateAcknowledge,
					prGlobalData,
					hSemDataAccess);
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_STATES : {
		DEV_WUP_trOnOffStates* prOnOffStates = (DEV_WUP_trOnOffStates*)pnArg;

		u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
		
		if (OSAL_E_NOERROR == u32OsalErrorCode) {

			#if defined (GEN3X86) // GEN3X86 = LSim
			prGlobalData->rOnOffStates.uOnOffStates.u32 = 0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_IGN_PIN;
			prGlobalData->rOnOffStates.uOnOffStates.u32 |= 0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_S_CONTACT;
			prGlobalData->rOnOffStates.u16MsgHandle     = 0;
			#endif

			prOnOffStates->uOnOffStates.u32 = prGlobalData->rOnOffStates.uOnOffStates.u32;
			prOnOffStates->u16MsgHandle     = prGlobalData->rOnOffStates.u16MsgHandle;
			DEV_WUP_vDataUnlock(hSemDataAccess);
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_LATEST_RESET_REASON : {
		DEV_WUP_trLatestResetReason* prLatestResetReason = (DEV_WUP_trLatestResetReason*)pnArg;

		if (prLatestResetReason != NULL) {
			#if defined (GEN3X86) // GEN3X86 = LSim
			prGlobalData->u8ResetReasonSCC            = DEV_WUP_C_U8_RESET_REASON_POR;
			prGlobalData->rPramLocal.u16ResetReasonAP = DEV_WUP_C_U16_RESET_REASON_POWER_ON;
			prLatestResetReason->u8ResetReasonSCC     = prGlobalData->u8ResetReasonSCC;
			prLatestResetReason->u16ResetReasonAP     = prGlobalData->rPramLocal.u16ResetReasonAP;
			#else
			u32OsalErrorCode = DEV_WUP_u32GetLatestResetReason(
						prLatestResetReason,
						prGlobalData,
						hSemDataAccess);
			#endif
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_INDICATE_STARTUP_FINISHED : {

		u32OsalErrorCode = DEV_WUP_u32IndicateStartupFinished();
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_EXTEND_POWER_OFF_TIMEOUT : {
		tU16* pu16TimeoutMs = (tU16*)pnArg;

		u8TmpArgBuffer[0] = (tU8)(((*pu16TimeoutMs) & 0x00FF) >>  0);
		u8TmpArgBuffer[1] = (tU8)(((*pu16TimeoutMs) & 0xFF00) >>  8);

		if (pu16TimeoutMs != NULL)
			u32OsalErrorCode = DEV_WUP_u32ExtendPowerOffTimeout(
						pu16TimeoutMs,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_CONTROL_RESET_MASTER_SUPERVISION : {
		DEV_WUP_trResetMasterSupervision* prResetMasterSupervision = (DEV_WUP_trResetMasterSupervision*)pnArg;

		if (prResetMasterSupervision != NULL)
			u32OsalErrorCode = DEV_WUP_u32ControlResetMasterSupervision(
						prResetMasterSupervision);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_APPLICATION_MODE : {
		tPU8 pu8ApplicationModeAP = (tPU8)pnArg;

		if (pu8ApplicationModeAP != NULL) {
			u32OsalErrorCode = DEV_WUP_u32GetApplicationMode(
						pu8ApplicationModeAP,
						prGlobalData,
						hSemDataAccess);
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_AP_SUPERVISION_ERROR : {
		tPU8 pu8APSupervisionError = (tPU8)pnArg;

		if (pu8APSupervisionError != NULL) {
			u32OsalErrorCode = DEV_WUP_u32GetAPSupervisionError(
						pu8APSupervisionError,
						prGlobalData,
						hSemDataAccess);
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_REGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION : {
		tU32 u32ClientId = (tU32)pnArg;

		u32OsalErrorCode = DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			u32ClientId,
			prGlobalData,
			hSemDataAccess);
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_UNREGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION : {
		tU32 u32ClientId = (tU32)pnArg;

		u32OsalErrorCode = DEV_WUP_u32UnregisterAPSupervisionErrorChanged(
			u32ClientId,
			prGlobalData,
			hSemDataAccess);
		break;
	}
	default:
		u32OsalErrorCode = OSAL_E_NOTSUPPORTED;
		break;
	}

	if (OSAL_E_NOTSUPPORTED == u32OsalErrorCode)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_u32Control() => Passed function s32Fun = 0x%08X is not supported ", s32Fun);
	else if (s32Fun != OSAL_C_S32_IOCTRL_WUP_RESET_PROCESSOR)
		DEV_WUP_vTraceIOControl(s32Fun, pnArg, u8TmpArgBuffer, u32OsalErrorCode);

	u32TmpOsalErrorCode = DEV_WUP_u32UnmapGlobalData(hDevWupShMem,prGlobalData);
	
	if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Control() => DEV_WUP_u32UnmapGlobalData() failed with error code = %s",
			OSAL_coszErrorText(u32TmpOsalErrorCode));

error_shared_memory_map:

	if (OSAL_s32SemaphoreClose(hSemDataAccess) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32Control() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return (u32OsalErrorCode);
}

/*******************************************************************************
*
* This functions maps the global data of the device to the shared-memory.
*
*******************************************************************************/
static tU32 DEV_WUP_u32MapGlobalData(OSAL_tShMemHandle* phDevWupShMem, trGlobalData** pprGlobalData)
{
	*phDevWupShMem = OSAL_SharedMemoryOpen(DEV_WUP_C_STRING_SHARED_MEM_NAME, OSAL_EN_READWRITE);

	if (*phDevWupShMem == OSAL_ERROR)
		return OSAL_u32ErrorCode();

	*pprGlobalData = (trGlobalData*) OSAL_pvSharedMemoryMap(
		*phDevWupShMem, 
		OSAL_EN_READWRITE, 
		sizeof(trGlobalData), 
		0);

	if (*pprGlobalData == NULL)
	{
		OSAL_s32SharedMemoryClose(*phDevWupShMem);

		return OSAL_u32ErrorCode();
	}

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This functions un-maps the devices global data from the shared-memory.
*
*******************************************************************************/
static tU32 DEV_WUP_u32UnmapGlobalData(OSAL_tShMemHandle hDevWupShMem, trGlobalData* prGlobalData)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (OSAL_s32SharedMemoryUnmap(prGlobalData, sizeof(trGlobalData)) == OSAL_ERROR)
		u32OsalErrorCode = OSAL_u32ErrorCode();

	if (OSAL_s32SharedMemoryClose(hDevWupShMem) == OSAL_ERROR)
		u32OsalErrorCode = OSAL_u32ErrorCode();

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function locks the devices central data via a semaphore.
*
*******************************************************************************/
static tU32 DEV_WUP_u32DataLock(OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (OSAL_s32SemaphoreWait(
		hSemDataAccess,
		DEV_WUP_C_U32_SEM_DATA_TIMEOUT_MS) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32DataLock() => OSAL_s32SemaphoreWait() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function unlocks the devices central data via a semaphore.
*
*******************************************************************************/
static tVoid DEV_WUP_vDataUnlock(OSAL_tSemHandle hSemDataAccess)
{
	if (OSAL_s32SemaphorePost(hSemDataAccess) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vDataUnlock() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
}

/*******************************************************************************
*
* This functions adds another anonymous client to the device.
*
*******************************************************************************/
static tU32 DEV_WUP_u32AddClient(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if (prGlobalData->u8NumberOfClients < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS)
		prGlobalData->u8NumberOfClients++;
	else
		u32OsalErrorCode = OSAL_E_BUSY;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions removes an anonymous client from the device.
*
*******************************************************************************/
static tU32 DEV_WUP_u32RemoveClient(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if (prGlobalData->u8NumberOfClients > 0)
		prGlobalData->u8NumberOfClients--;
	else
		u32OsalErrorCode = OSAL_E_DOESNOTEXIST;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}


/*******************************************************************************
*
* This function registers a so far anonymous client at the device.
*
*******************************************************************************/
static tU32 DEV_WUP_u32RegisterClient(DEV_WUP_trClientRegistration* prClientRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode            = OSAL_E_NOERROR;
	tU32  u32Index;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if (prGlobalData->u8NumberOfRegisteredClients < prGlobalData->u8NumberOfClients) {

		u32OsalErrorCode = OSAL_E_UNKNOWN;

		for (u32Index = 0;
		     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
		     u32Index++) {

			if (DEV_WUP_C_U32_CLIENT_ID_INVALID == prGlobalData->arClientSpecificData[u32Index].u32ClientId) {

				prGlobalData->u8NumberOfRegisteredClients++;
				prGlobalData->u32LatestAssignedClientId++;

				prGlobalData->arClientSpecificData[u32Index].u32ClientId = prGlobalData->u32LatestAssignedClientId;

				(tVoid) OSALUTIL_szSaveStringNCopy(
					prClientRegistration->szNotificationEventName,
					prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
					sizeof(prClientRegistration->szNotificationEventName));

				prClientRegistration->u32ClientId = prGlobalData->arClientSpecificData[u32Index].u32ClientId;

				u32OsalErrorCode = OSAL_E_NOERROR;
				break;
			}
		}
	} else {
		u32OsalErrorCode = OSAL_E_MAXFILES;
	}

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function un-registers an registered client from the device.
*
*******************************************************************************/
static tU32 DEV_WUP_u32UnregisterClient(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;

	tU32  u32Index;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = OSAL_E_DOESNOTEXIST;
	
	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == u32ClientId) {
			prGlobalData->u8NumberOfRegisteredClients--;

			prGlobalData->arClientSpecificData[u32Index].u32ClientId = DEV_WUP_C_U32_CLIENT_ID_INVALID;

			u32OsalErrorCode =  OSAL_E_NOERROR;
			break;
		}
	}

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions registers a callback function at the TTFIS trace device.
*
*******************************************************************************/
static tVoid DEV_WUP_vRegisterTraceCallback(tVoid)
{
	g_rModuleData.rTraceIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE, OSAL_EN_READWRITE);

	if (g_rModuleData.rTraceIODescriptor != OSAL_ERROR) {
		OSAL_trIOCtrlLaunchChannel rIOCtrlLaunchChannel;

		rIOCtrlLaunchChannel.pCallback      = (OSAL_tpfCallback)DEV_WUP_vTraceCallback;
		rIOCtrlLaunchChannel.enTraceChannel = (TR_tenTraceChan) TR_TTFIS_DEV_WUP;

		if (OSAL_s32IOControl(
			g_rModuleData.rTraceIODescriptor,
			OSAL_C_S32_IOCTRL_CALLBACK_REG, 
			(intptr_t) &rIOCtrlLaunchChannel) == OSAL_ERROR) {

				DEV_WUP_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_WUP_vRegisterTraceCallback() => OSAL_s32IOControl(OSAL_C_S32_IOCTRL_CALLBACK_REG) failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

				if (OSAL_s32IOClose(g_rModuleData.rTraceIODescriptor) == OSAL_OK)
					g_rModuleData.rTraceIODescriptor = OSAL_ERROR;
				else
					DEV_WUP_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_WUP_vRegisterTraceCallback() => OSAL_s32IOClose failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	} else {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vRegisterTraceCallback() => OSAL_IOOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}
}

/*******************************************************************************
*
* This functions un-registers the callback function from the TTFIS trace device.
*
*******************************************************************************/
static tVoid DEV_WUP_vUnregisterTraceCallback(tVoid)
{
	if (g_rModuleData.rTraceIODescriptor != OSAL_ERROR) {
		OSAL_trIOCtrlLaunchChannel rIOCtrlLaunchChannel;

		rIOCtrlLaunchChannel.pCallback      = (OSAL_tpfCallback)DEV_WUP_vTraceCallback;
		rIOCtrlLaunchChannel.enTraceChannel = (TR_tenTraceChan) TR_TTFIS_DEV_WUP;

		if (OSAL_s32IOControl(
			g_rModuleData.rTraceIODescriptor,
			OSAL_C_S32_IOCTRL_CALLBACK_UNREG,
			(intptr_t) &rIOCtrlLaunchChannel) == OSAL_OK) {
				if (OSAL_s32IOClose(g_rModuleData.rTraceIODescriptor) == OSAL_OK)
					g_rModuleData.rTraceIODescriptor = OSAL_ERROR;
				else
					DEV_WUP_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_WUP_vUnregisterTraceCallback() => OSAL_s32IOClose failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
		} else {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vUnregisterTraceCallback() => OSAL_s32IOControl(OSAL_C_S32_IOCTRL_CALLBACK_UNREG) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}
}


/*******************************************************************************
*
* This function represents the callback function for the TTFIS trace device.
*
*******************************************************************************/
static tVoid DEV_WUP_vTraceCallback(const tU8 * const pu8Data)
{
	tU8 au8MsqQueueBuffer[DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN];

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_FATAL,
		"DEV_WUP_vTraceCallback() => Received command 0x%02X with size %u",
		pu8Data[1],
		pu8Data[0]);

	if (pu8Data[0] <= sizeof(g_rModuleData.au8TraceCallbackBuffer)) {
		(tVoid) OSAL_pvMemoryCopy(
				g_rModuleData.au8TraceCallbackBuffer,
				pu8Data,
				pu8Data[0]+1U);

		au8MsqQueueBuffer[0] = DEV_WUP_C_U8_OSAL_MSG_TRACE_CALLBACK_COMMAND_RECEIVED;

		if (OSAL_s32MessageQueuePost(
			g_rModuleData.hMsgQueueOsalThread,
			au8MsqQueueBuffer,
			DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN,
			OSAL_C_U32_MQUEUE_PRIORITY_LOWEST) == OSAL_ERROR)
				DEV_WUP_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_WUP_vTraceCallback() => OSAL_s32MessageQueuePost() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
	} else {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vTraceCallback() => Size %u of command 0x%02X too long (max = %u). Command not executed",
			pu8Data[0],
			pu8Data[1],
			sizeof(g_rModuleData.au8TraceCallbackBuffer));
	}
}

/*******************************************************************************
*
* This function evaluates the received commands which are received via the
* TTFIS trac callback and performs the requested actions.
*
*******************************************************************************/
static tVoid DEV_WUP_vEvaluateTraceCallbackCommand(tVoid)
{
	tBool bCommandFailed = FALSE;

	switch (g_rModuleData.au8TraceCallbackBuffer[1]) {
	case 0x00: { // Command = DEV_WUP_GET_INTERNAL_STATE
		DEV_WUP_vTraceInternalState(TR_LEVEL_FATAL);
		break;
	}
	case 0x01: { // Command = DEV_WUP_GET_REGISTERED_CLIENT_DATA
		DEV_WUP_vTraceRegisteredClientData(TR_LEVEL_FATAL);
		break;
	}
	case 0x02: { // Command = DEV_WUP_GET_ONOFF_EVENT_BUFFER
		DEV_WUP_vTraceOnOffEventBuffer(TR_LEVEL_FATAL);
	break;
	}
	case 0x40: {// Command = DEV_WUP_TRIGGER_SYSTEM_SHUTDOWN
		DEV_WUP_vTraceIOControl(
			(tS32)OSAL_C_S32_IOCTRL_WUP_SHUTDOWN,
			(intptr_t)DEV_WUP_SHUTDOWN_NORMAL,
			NULL,
			OSAL_E_NOERROR);

		OSAL_s32ThreadWait(100); // Give trace output some time to be printed before shut-down is performed.

		DEV_WUP_vIncSend_CShutdownInProgress();
		break;
	}
	case 0x41: { // Command = DEV_WUP_RESET_PROCESSOR
		DEV_WUP_trResetProcessorInfo rResetProcessorInfo;

		rResetProcessorInfo.u8Processor        = g_rModuleData.au8TraceCallbackBuffer[2];
		rResetProcessorInfo.u8ResetMode        = g_rModuleData.au8TraceCallbackBuffer[3];
		rResetProcessorInfo.u16ResetReason     = (tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[5]) << 8) |
								 ((tU16)g_rModuleData.au8TraceCallbackBuffer[4])        );

		rResetProcessorInfo.u16ResetDurationMs = (tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[7]) << 8) |
								 ((tU16)g_rModuleData.au8TraceCallbackBuffer[6])        );

		if (DEV_WUP_u32ResetProcessor(
			&rResetProcessorInfo,
			g_rModuleData.prGlobalData,
			g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
				bCommandFailed = TRUE;
		break;
	}
	case 0x42: { // Command = DEV_WUP_CONFIGURE_WAKEUP_REASONS
		tU32 u32WakeupConfig = ((((tU32) g_rModuleData.au8TraceCallbackBuffer[2]) <<  0) |
					(((tU32) g_rModuleData.au8TraceCallbackBuffer[3]) <<  8) |
					(((tU32) g_rModuleData.au8TraceCallbackBuffer[4]) << 16) |
					(((tU32) g_rModuleData.au8TraceCallbackBuffer[5]) << 24)  );

		DEV_WUP_vIncSend_CSetWakeupConfig(u32WakeupConfig);
		break;
	}
	case 0x43: { // Command = DEV_WUP_INDICATE_STARTUP_FINISHED
		DEV_WUP_vIncSend_CStartupFinished();
		break;
	}
	case 0x44: { // Command = DEV_WUP_EXTEND_POWER_OFF_TIMEOUT
		tU16 u16TimeoutS = (tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[3]) << 8) |
					   ((tU16)g_rModuleData.au8TraceCallbackBuffer[2])        );

		DEV_WUP_vIncSend_CExtendPowerOffTimeout(u16TimeoutS);
		break;
	}
	case 0x45: { // Command = DEV_WUP_CONTROL_RESET_MASTER_SUPERVISION
		tU8 u8ResetControlBitmask = g_rModuleData.au8TraceCallbackBuffer[3];

		if (g_rModuleData.au8TraceCallbackBuffer[2] == TRUE)
			u8ResetControlBitmask |= DEV_WUP_C_U8_PREVENT_VCC_RESET_BITMASK_ALL;

		DEV_WUP_vIncSend_CCtrlResetExecution(u8ResetControlBitmask);
		break;
	}
	case 0x80: { // Command = DEV_WUP_SIMULATE_RECEIVED_R_INDICATE_CLIENT_APP_STATE
		tU8 u8RcvBuffer[6];

		u8RcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE;
		u8RcvBuffer[1] = DEV_WUP_C_U8_APPLICATION_MODE_NORMAL;
		u8RcvBuffer[2] = DEV_WUP_C_U8_RESET_REASON_POR;
		u8RcvBuffer[3] = 2;
		u8RcvBuffer[4] = 8;
		u8RcvBuffer[5] = DEV_WUP_C_U8_MSG_CAT_VERSION_CHECK_RESULT_OK;

		DEV_WUP_vSimulateIncMsgReceived(u8RcvBuffer, 6);
		break;
	}
	case 0x81: { // Command = DEV_WUP_SIMULATE_RECEIVED_R_SHUTDOWN_IN_PROGRESS
		tU8 u8RcvBuffer[1];

		u8RcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_SHUTDOWN_IN_PROGRESS;

		DEV_WUP_vSimulateIncMsgReceived(u8RcvBuffer, 1);
		break;
	}
	case 0x82: { // Command = DEV_WUP_SIMULATE_RECEIVED_R_WAKEUP_REASON
		tU8 u8RcvBuffer[2];

		u8RcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_REASON;
		u8RcvBuffer[1] = g_rModuleData.au8TraceCallbackBuffer[2];

		DEV_WUP_vSimulateIncMsgReceived(u8RcvBuffer, 2);
		break;
	}
	case 0x83: { // Command = DEV_WUP_SIMULATE_RECEIVED_R_WAKEUP_EVENT
		tU8 u8RcvBuffer[4];

		u8RcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_EVENT;
		u8RcvBuffer[1] = g_rModuleData.au8TraceCallbackBuffer[2];
		u8RcvBuffer[2] = g_rModuleData.au8TraceCallbackBuffer[3];
		u8RcvBuffer[3] = g_rModuleData.au8TraceCallbackBuffer[4];

		DEV_WUP_vSimulateIncMsgReceived(u8RcvBuffer, 4);
		break;
	}
	case 0x84: { // Command = DEV_WUP_SIMULATE_RECEIVED_R_WAKEUP_STATE
		tU8 u8RcvBuffer[5];

		u8RcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE;
		u8RcvBuffer[1] = g_rModuleData.au8TraceCallbackBuffer[2];
		u8RcvBuffer[2] = g_rModuleData.au8TraceCallbackBuffer[3];
		u8RcvBuffer[3] = g_rModuleData.au8TraceCallbackBuffer[4];
		u8RcvBuffer[4] = g_rModuleData.au8TraceCallbackBuffer[5];

		DEV_WUP_vSimulateIncMsgReceived(u8RcvBuffer, 5);
		break;
	}

	case 0x85: { // Command = DEV_WUP_SIMULATE_RECEIVED_R_STARTUP_INFO
		tU8 u8RcvBuffer[3];

		u8RcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_STARTUP_INFO;
		u8RcvBuffer[1] = DEV_WUP_C_U8_STARTTYPE_COLDSTART;
		u8RcvBuffer[2] = 0;

		DEV_WUP_vSimulateIncMsgReceived(u8RcvBuffer, 3);
		break;
	}
	case 0x86: { // Command = R_AP_SUPERVISION_ERROR
		tU8 u8RcvBuffer[2];

		u8RcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_AP_SUPERVISION_ERROR;
		u8RcvBuffer[1] = g_rModuleData.au8TraceCallbackBuffer[2];

		DEV_WUP_vSimulateIncMsgReceived(u8RcvBuffer, 2);
		break;
	}
	case 0xC0: { // Command = DEV_WUP_INVOKE_CLIENT_THREAD
		tU8 u8NotificationMode = g_rModuleData.au8TraceCallbackBuffer[2];

		if (DEV_WUP_u32InstallClientThread(u8NotificationMode) != OSAL_E_NOERROR)
			bCommandFailed = TRUE;
		break;
	}
	case 0xC1: { // Command = DEV_WUP_EMMC_REMOUNT_READ_ONLY
		(tVoid)DEV_WUP_bEmmcRemountReadOnly();
		break;
	}
	case 0xC2: { // Command = DEV_WUP_TRIGGER_EMMC_POWER_OFF
		tChar chCommand = g_rModuleData.au8TraceCallbackBuffer[2];

		if (DEV_WUP_bTriggerEmmcPowerOff(chCommand) == FALSE)
			bCommandFailed = TRUE;
		break;
	}
	default:
		bCommandFailed = TRUE;
		break;
	}

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_FATAL,
		"DEV_WUP_vEvaluateTraceCallbackCommand() => Command 0x%02X %s",
		g_rModuleData.au8TraceCallbackBuffer[1],
		(bCommandFailed == TRUE) ? "failed" : "executed");
}

/*******************************************************************************
*
* This function prints a formatted string on TTFIS.
*
*******************************************************************************/
static tVoid DEV_WUP_vWriteErrorMemoryFormatted(const tChar * const coszFormatString, ...)
{
	tChar acErrorMemoryBuffer[ERRMEM_MAX_ENTRY_LENGTH];

	OSAL_tIODescriptor rErrmemIODescriptor = OSAL_ERROR;
	trErrmemEntry      rErrmemEntry        = {0};
	tU16               u16MessageLength    = 0;

	OSAL_tVarArgList   ArgList;

	rErrmemIODescriptor = OSAL_IOOpen(
				OSAL_C_STRING_DEVICE_ERRMEM,
				OSAL_EN_WRITEONLY);

	if (rErrmemIODescriptor == OSAL_ERROR) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vWriteErrorMemoryFormatted() => OSAL_IOOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		return;
	}

	OSAL_VarArgStart(ArgList, coszFormatString); //lint !e530
	OSAL_s32VarNPrintFormat(
		acErrorMemoryBuffer,
		sizeof(acErrorMemoryBuffer),
		coszFormatString,
		ArgList); //lint !e530
	OSAL_VarArgEnd(ArgList);

	u16MessageLength = (tU16)OSAL_u32StringLength(acErrorMemoryBuffer);
	
	(tVoid) OSAL_pvMemoryCopy(
			rErrmemEntry.au8EntryData,
			acErrorMemoryBuffer,
			u16MessageLength);

	(tVoid) OSAL_s32ClockGetTime(&rErrmemEntry.rEntryTime);
	rErrmemEntry.eEntryType = eErrmemEntryNormal;
	rErrmemEntry.u16EntryLength = u16MessageLength;

	if (OSAL_s32IOWrite(
		rErrmemIODescriptor,
		(tPCS8)&rErrmemEntry,
		sizeof(rErrmemEntry)) == OSAL_ERROR) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vWriteErrorMemoryFormatted() => OSAL_s32IOWrite() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	if (OSAL_s32IOClose(rErrmemIODescriptor) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vWriteErrorMemoryFormatted() => OSAL_s32IOClose() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
}

/*******************************************************************************
*
* This function prints a formatted string on TTFIS.
*
*******************************************************************************/
static tVoid DEV_WUP_vTraceFormatted(TR_tenTraceLevel enTraceLevel, const tChar * const coszFormatString, ...)
{
	tU8 au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (LLD_bIsTraceActive(TR_CLASS_DEV_WUP, enTraceLevel) != FALSE) {
		au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_STRING;

		OSAL_tVarArgList ArgList; 
		OSAL_VarArgStart(ArgList, coszFormatString); //lint !e530
		OSAL_s32VarNPrintFormat(
			(char*)(&(au8TraceSendBuffer[1])),
			sizeof(au8TraceSendBuffer) - 1,
			coszFormatString,
			ArgList); //lint !e530
		OSAL_VarArgEnd(ArgList);

		LLD_vTrace(
			TR_CLASS_DEV_WUP,
			enTraceLevel,
			au8TraceSendBuffer,
			(tU32)(OSAL_u32StringLength(au8TraceSendBuffer) + 1U));
	}
}

/*******************************************************************************
*
* This function prints the internal state of the device on TTFIS.
*
*******************************************************************************/
static tVoid DEV_WUP_vTraceInternalState(TR_tenTraceLevel enTraceLevel)
{
	tU8 au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];

	tU32               u32ResetCounter = 0xDEADBEEF;
	OSAL_tIODescriptor rIODescriptor   = OSAL_ERROR;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	if (LLD_bIsTraceActive(TR_CLASS_DEV_WUP, enTraceLevel) != FALSE) {
		au8TraceSendBuffer[0]  = DEV_WUP_C_U8_TRACE_TYPE_INTERNAL_DATA;

		// Trace in PRAM located application processor reset counter value
		rIODescriptor = OSAL_IOOpen(
					OSAL_C_STRING_DEVICE_PRAM"/reset_counter",
					OSAL_EN_READWRITE);

		if (rIODescriptor != OSAL_ERROR) {
			if (OSAL_s32IOControl(
				rIODescriptor,
				OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK,
				0) == OSAL_OK)
					(tVoid)OSAL_s32IORead(
						rIODescriptor,
						(tPS8)&u32ResetCounter,
						sizeof(u32ResetCounter));
			(tVoid)OSAL_s32IOClose(rIODescriptor);
		}

		au8TraceSendBuffer[1] = (tU8)(((u32ResetCounter) & 0x000000FF) >>  0);
		au8TraceSendBuffer[2] = (tU8)(((u32ResetCounter) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[3] = (tU8)(((u32ResetCounter) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[4] = (tU8)(((u32ResetCounter) & 0xFF000000) >> 24);
		au8TraceSendBuffer[5] = (tU8)((u32ResetCounter >>  8) & 0x000000FF);
		au8TraceSendBuffer[6]  = g_rModuleData.bGpioCpuRunAvailable;
		au8TraceSendBuffer[7]  = g_rModuleData.bGpioCpuPwfOffAvailable;
		au8TraceSendBuffer[8]  = g_rModuleData.u8IncMsgThreadState;
		au8TraceSendBuffer[9]  = g_rModuleData.u8OsalMsgThreadState;
		au8TraceSendBuffer[10]  = g_rModuleData.u8ClientThreadState;
		au8TraceSendBuffer[11]  = DEV_WUP_C_U8_INC_MSG_CAT_MAJOR_VERSION_NUMBER;
		au8TraceSendBuffer[12]  = DEV_WUP_C_U8_INC_MSG_CAT_MINOR_VERSION_NUMBER;
		au8TraceSendBuffer[13]  = g_rModuleData.u8IncMsgCatMajorVersionServer;
		au8TraceSendBuffer[14]  = g_rModuleData.u8IncMsgCatMinorVersionServer;
		au8TraceSendBuffer[15] = (tU8)(((g_rModuleData.u32ConnectToFakeIncDeviceMagic) & 0x000000FF) >>  0);
		au8TraceSendBuffer[16] = (tU8)(((g_rModuleData.u32ConnectToFakeIncDeviceMagic) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[17] = (tU8)(((g_rModuleData.u32ConnectToFakeIncDeviceMagic) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[18] = (tU8)(((g_rModuleData.u32ConnectToFakeIncDeviceMagic) & 0xFF000000) >> 24);
		au8TraceSendBuffer[19] = g_rModuleData.bSccIncCommunicationEstablished;
		au8TraceSendBuffer[20] = g_rModuleData.prGlobalData->u8Starttype;
		au8TraceSendBuffer[21] = (tU8)(((g_rModuleData.prGlobalData->rPramLocal.u16ResetReasonAP) & 0x00FF) >>  0);
		au8TraceSendBuffer[22] = (tU8)(((g_rModuleData.prGlobalData->rPramLocal.u16ResetReasonAP) & 0xFF00) >>  8);
		au8TraceSendBuffer[23] = g_rModuleData.prGlobalData->rPramLocal.u8ResetClassificationAP;
		au8TraceSendBuffer[24] = g_rModuleData.prGlobalData->u8ResetReasonSCC;
		au8TraceSendBuffer[25] = g_rModuleData.prGlobalData->u8ApplicationModeAP;
		au8TraceSendBuffer[26] = g_rModuleData.prGlobalData->u8ApplicationModeSCC;
		au8TraceSendBuffer[27] = g_rModuleData.prGlobalData->u8WupReason;
		au8TraceSendBuffer[28] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.CAN);
		au8TraceSendBuffer[29] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.FLEXRAY);
		au8TraceSendBuffer[30] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.MOST);
		au8TraceSendBuffer[31] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.LIN);
		au8TraceSendBuffer[32] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.IGN_PIN);
		au8TraceSendBuffer[33] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.TELEPHONE_MUTE);
		au8TraceSendBuffer[34] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.MOST_RBD);
		au8TraceSendBuffer[35] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.MOST_ECL);
		au8TraceSendBuffer[36] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.MOST_UNDEF);
		au8TraceSendBuffer[37] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.S_CONTACT);
		au8TraceSendBuffer[38] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.CAN2);
		au8TraceSendBuffer[39] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.CAN3);
		au8TraceSendBuffer[40] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.CAN4);
		au8TraceSendBuffer[41] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.ODOMETER);
		au8TraceSendBuffer[42] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.CELLNETWORK);
		au8TraceSendBuffer[43] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.EXTERNAL_PIN);
		au8TraceSendBuffer[44] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.USB);
		au8TraceSendBuffer[45] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.CD_CLAMP);
		au8TraceSendBuffer[46] = (tU8)(g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.rBitfield.ILLUMINATION);
		au8TraceSendBuffer[47] = (tU8)(((g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32) & 0x000000FF) >>  0);
		au8TraceSendBuffer[48] = (tU8)(((g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[49] = (tU8)(((g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[50] = (tU8)(((g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32) & 0xFF000000) >> 24);
		au8TraceSendBuffer[51] = (tU8)(((g_rModuleData.prGlobalData->rOnOffStates.u16MsgHandle) & 0x00FF) >>  0);
		au8TraceSendBuffer[52] = (tU8)(((g_rModuleData.prGlobalData->rOnOffStates.u16MsgHandle) & 0xFF00) >>  8);
		au8TraceSendBuffer[53] = g_rModuleData.prGlobalData->u8APSupervisionError;
		au8TraceSendBuffer[54] = g_rModuleData.u8LatestReceivedOnOffEvent;
		au8TraceSendBuffer[55] = (tU8)(((g_rModuleData.prGlobalData->rPramLocal.u16LatestAcknowledgedOnOffEventMsgHandle) & 0x00FF) >>  0);
		au8TraceSendBuffer[56] = (tU8)(((g_rModuleData.prGlobalData->rPramLocal.u16LatestAcknowledgedOnOffEventMsgHandle) & 0xFF00) >>  8);
		au8TraceSendBuffer[57] = g_rModuleData.prGlobalData->u8NumberOfOnOffReasonChangedRegistrations;
		au8TraceSendBuffer[58] = g_rModuleData.prGlobalData->u8NumberOfClients;
		au8TraceSendBuffer[59] = g_rModuleData.prGlobalData->u8NumberOfRegisteredClients;
		au8TraceSendBuffer[60] = (tU8)(((g_rModuleData.prGlobalData->u32LatestAssignedClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[61] = (tU8)(((g_rModuleData.prGlobalData->u32LatestAssignedClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[62] = (tU8)(((g_rModuleData.prGlobalData->u32LatestAssignedClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[63] = (tU8)(((g_rModuleData.prGlobalData->u32LatestAssignedClientId) & 0xFF000000) >> 24);

		LLD_vTrace(
			TR_CLASS_DEV_WUP,
			enTraceLevel,
			au8TraceSendBuffer,
			64);
	}

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This functions prints information of each registered client on TTFIS.
*
*******************************************************************************/
static tVoid DEV_WUP_vTraceRegisteredClientData(TR_tenTraceLevel enTraceLevel)
{
	tU8  au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];
	tU32 u32Index;
	tU32 u32Index2;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	if (LLD_bIsTraceActive(TR_CLASS_DEV_WUP, enTraceLevel) != FALSE) {
		for (u32Index2 = 0;
		     u32Index2 < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
		     u32Index2++) {

			if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].u32ClientId != DEV_WUP_C_U32_CLIENT_ID_INVALID) {
				tU32 u32IndexOnOffEventRead               = g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].u32IndexOnOffEventRead;
				tU16 u16OnOffEventMsgHandleAtReadPosition = g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32IndexOnOffEventRead].u16MsgHandle;

				au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_REGISTERED_CLIENT_DATA;
				au8TraceSendBuffer[1] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].u32ClientId) & 0x000000FF) >>  0);
				au8TraceSendBuffer[2] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].u32ClientId) & 0x0000FF00) >>  8);
				au8TraceSendBuffer[3] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].u32ClientId) & 0x00FF0000) >> 16);
				au8TraceSendBuffer[4] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].u32ClientId) & 0xFF000000) >> 24);

				for (u32Index = 0;
				     u32Index < DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH;
				     u32Index++)
					au8TraceSendBuffer[5+u32Index] = g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].szNotificationEventName[u32Index];

				au8TraceSendBuffer[5+DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH]  = g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].u8OnOffReasonChangedNotificationMode;
				au8TraceSendBuffer[6+DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH]  = g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].bIsSystemMaster;
				au8TraceSendBuffer[7+DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH]  = (tU8)(((u16OnOffEventMsgHandleAtReadPosition) & 0x00FF) >>  0);
				au8TraceSendBuffer[8+DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH]  = (tU8)(((u16OnOffEventMsgHandleAtReadPosition) & 0xFF00) >>  8);
				au8TraceSendBuffer[9+DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH]  = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].u16LatestAckOnOffEventMsgHandle) & 0x00FF) >>  0);
				au8TraceSendBuffer[10+DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].u16LatestAckOnOffEventMsgHandle) & 0xFF00) >>  8);
				au8TraceSendBuffer[11+DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH] = g_rModuleData.prGlobalData->arClientSpecificData[u32Index2].bNotifyAPSupervisionError;

				LLD_vTrace(
					TR_CLASS_DEV_WUP,
					enTraceLevel,
					au8TraceSendBuffer,
					12 + DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH);
			}
		}
	}

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function prints the IO-control accesses to the device on TTFIS.
*
*******************************************************************************/
static tVoid DEV_WUP_vTraceIOControl(tS32 s32Fun, intptr_t pnArg, tU8* pu8TmpArgBuffer, tU32 u32OsalErrorCode)
{
	tU8 au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];

	tU8              u8BufferLength = 0;
	TR_tenTraceLevel enTraceLevel   = TR_LEVEL_USER_1;

	tU8 u8Index;

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		enTraceLevel = TR_LEVEL_ERRORS;

	if (LLD_bIsTraceActive(TR_CLASS_DEV_WUP, enTraceLevel) == FALSE)
		return;

	au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_IO_CONTROL;
	au8TraceSendBuffer[1] = (tU8)(((tU32)s32Fun & 0x000000FF) >>  0);
	au8TraceSendBuffer[2] = (tU8)(((tU32)s32Fun & 0x0000FF00) >>  8);
	au8TraceSendBuffer[3] = (tU8)(((tU32)s32Fun & 0x00FF0000) >> 16);
	au8TraceSendBuffer[4] = (tU8)(((tU32)s32Fun & 0xFF000000) >> 24);

	if (u32OsalErrorCode == OSAL_E_NOERROR)
		au8TraceSendBuffer[5] = DEV_WUP_C_U8_OK;
	else
		au8TraceSendBuffer[5] = DEV_WUP_C_U8_ERROR;

	au8TraceSendBuffer[6] = (tU8)(((u32OsalErrorCode) & 0x000000FF) >>  0);
	au8TraceSendBuffer[7] = (tU8)(((u32OsalErrorCode) & 0x0000FF00) >>  8);
	au8TraceSendBuffer[8] = (tU8)(((u32OsalErrorCode) & 0x00FF0000) >> 16);
	au8TraceSendBuffer[9] = (tU8)(((u32OsalErrorCode) & 0xFF000000) >> 24);

	switch(s32Fun) {
	case OSAL_C_S32_IOCTRL_WUP_SHUTDOWN : {
		tU8 u8ShutdownType = (tU8)pnArg;

		au8TraceSendBuffer[10] = u8ShutdownType;

		u8BufferLength = 11;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_STARTTYPE : {
		tPU8 pu8Starttype = (tPU8)pnArg;

		au8TraceSendBuffer[10] = *pu8Starttype;

		u8BufferLength = 11;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_WAKEUP_REASON : {
		tPU8 pu8WakeupReason = (tPU8)pnArg;

		au8TraceSendBuffer[10] = *pu8WakeupReason;

		u8BufferLength = 11;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT : {
		DEV_WUP_trClientRegistration* prClientRegistration = (DEV_WUP_trClientRegistration*)pnArg;

		au8TraceSendBuffer[10]   = (tU8)(((prClientRegistration->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11]   = (tU8)(((prClientRegistration->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12]   = (tU8)(((prClientRegistration->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13]   = (tU8)(((prClientRegistration->u32ClientId) & 0xFF000000) >> 24);

		for (u8Index = 0;
		     u8Index < DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH;
		     u8Index++)
			au8TraceSendBuffer[14 + u8Index] = prClientRegistration->szNotificationEventName[u8Index];

		u8BufferLength = 14 + DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT : {
		tU32 u32ClientId = (tU32)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((u32ClientId) & 0xFF000000) >> 24);

		u8BufferLength = 14;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_REGISTER_ONOFF_REASON_CHANGED_NOTIFICATION : {
		DEV_WUP_trOnOffReasonChangedRegistration* prOnOffReasonChangedRegistration = (DEV_WUP_trOnOffReasonChangedRegistration*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prOnOffReasonChangedRegistration->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prOnOffReasonChangedRegistration->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((prOnOffReasonChangedRegistration->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((prOnOffReasonChangedRegistration->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[14] = prOnOffReasonChangedRegistration->u8NotificationMode;
		au8TraceSendBuffer[15] = prOnOffReasonChangedRegistration->bIsSystemMaster;

		u8BufferLength = 16;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_UNREGISTER_ONOFF_REASON_CHANGED_NOTIFICATION : {
		tU32 u32ClientId = (tU32)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((u32ClientId) & 0xFF000000) >> 24);

		u8BufferLength = 14;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_CONFIGURE_WAKEUP_REASONS : {
		tU32 u32WakeupConfiguration = (tU32)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((u32WakeupConfiguration) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((u32WakeupConfiguration) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((u32WakeupConfiguration) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((u32WakeupConfiguration) & 0xFF000000) >> 24);

		u8BufferLength = 14;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_RESET_PROCESSOR : {
		DEV_WUP_trResetProcessorInfo* prResetProcessorInfo = (DEV_WUP_trResetProcessorInfo*)pnArg;

		au8TraceSendBuffer[10] = prResetProcessorInfo->u8Processor;
		au8TraceSendBuffer[11] = prResetProcessorInfo->u8ResetMode;
		au8TraceSendBuffer[12] = (tU8)(((prResetProcessorInfo->u16ResetReason) & 0x00FF) >>  0);
		au8TraceSendBuffer[13] = (tU8)(((prResetProcessorInfo->u16ResetReason) & 0xFF00) >>  8);
		au8TraceSendBuffer[14] = (tU8)(((prResetProcessorInfo->u16ResetDurationMs) & 0x00FF) >>  0);
		au8TraceSendBuffer[15] = (tU8)(((prResetProcessorInfo->u16ResetDurationMs) & 0xFF00) >>  8);

		u8BufferLength = 16;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_EVENTS : {
		DEV_WUP_trOnOffEventHistory* prOnOffEventHistory = (DEV_WUP_trOnOffEventHistory*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prOnOffEventHistory->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prOnOffEventHistory->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((prOnOffEventHistory->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((prOnOffEventHistory->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[14] = prOnOffEventHistory->u8NumberOfOnOffEvents;

		u8BufferLength = 15;

		LLD_vTrace(
			TR_CLASS_DEV_WUP, 
			enTraceLevel, 
			au8TraceSendBuffer,
			u8BufferLength);

		for (u8Index = 0;
		     u8Index < prOnOffEventHistory->u8NumberOfOnOffEvents;
		     u8Index++) {

			au8TraceSendBuffer[0]  = DEV_WUP_C_U8_TRACE_TYPE_IO_CONTROL_LIST_ELEMENTS;
			au8TraceSendBuffer[10] = u8Index;
			au8TraceSendBuffer[11] = prOnOffEventHistory->arOnOffEvent[u8Index].u8Event;
			au8TraceSendBuffer[12] = (tU8)(((prOnOffEventHistory->arOnOffEvent[u8Index].u16MsgHandle) & 0x00FF) >>  0);
			au8TraceSendBuffer[13] = (tU8)(((prOnOffEventHistory->arOnOffEvent[u8Index].u16MsgHandle) & 0xFF00) >>  8);
			au8TraceSendBuffer[14] = (tU8)(((prOnOffEventHistory->arOnOffEvent[u8Index].u32Timestamp) & 0x000000FF) >>  0);
			au8TraceSendBuffer[15] = (tU8)(((prOnOffEventHistory->arOnOffEvent[u8Index].u32Timestamp) & 0x0000FF00) >>  8);
			au8TraceSendBuffer[16] = (tU8)(((prOnOffEventHistory->arOnOffEvent[u8Index].u32Timestamp) & 0x00FF0000) >> 16);
			au8TraceSendBuffer[17] = (tU8)(((prOnOffEventHistory->arOnOffEvent[u8Index].u32Timestamp) & 0xFF000000) >> 24);

			u8BufferLength = 18;

			LLD_vTrace(
				TR_CLASS_DEV_WUP, 
				enTraceLevel, 
				au8TraceSendBuffer,
				u8BufferLength);
		}

		u8BufferLength = 0;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_ACKNOWLEDGE_ONOFF_EVENT : {
		DEV_WUP_trOnOffEventAcknowledge* prOnOffEventAcknowledge = (DEV_WUP_trOnOffEventAcknowledge*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prOnOffEventAcknowledge->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prOnOffEventAcknowledge->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((prOnOffEventAcknowledge->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((prOnOffEventAcknowledge->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[14] = (tU8)(((prOnOffEventAcknowledge->u16OnOffEventMsgHandle) & 0x00FF) >>  0);
		au8TraceSendBuffer[15] = (tU8)(((prOnOffEventAcknowledge->u16OnOffEventMsgHandle) & 0xFF00) >>  8);

		u8BufferLength = 16;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_STATES : {
		DEV_WUP_trOnOffStates* prOnOffStates = (DEV_WUP_trOnOffStates*)pnArg;

		au8TraceSendBuffer[10] = (tU8)prOnOffStates->uOnOffStates.rBitfield.CAN;
		au8TraceSendBuffer[11] = (tU8)prOnOffStates->uOnOffStates.rBitfield.FLEXRAY;
		au8TraceSendBuffer[12] = (tU8)prOnOffStates->uOnOffStates.rBitfield.MOST;
		au8TraceSendBuffer[13] = (tU8)prOnOffStates->uOnOffStates.rBitfield.LIN;
		au8TraceSendBuffer[14] = (tU8)prOnOffStates->uOnOffStates.rBitfield.IGN_PIN;
		au8TraceSendBuffer[15] = (tU8)prOnOffStates->uOnOffStates.rBitfield.TELEPHONE_MUTE;
		au8TraceSendBuffer[16] = (tU8)prOnOffStates->uOnOffStates.rBitfield.MOST_RBD;
		au8TraceSendBuffer[17] = (tU8)prOnOffStates->uOnOffStates.rBitfield.MOST_ECL;
		au8TraceSendBuffer[18] = (tU8)prOnOffStates->uOnOffStates.rBitfield.MOST_UNDEF;
		au8TraceSendBuffer[19] = (tU8)prOnOffStates->uOnOffStates.rBitfield.S_CONTACT;
		au8TraceSendBuffer[20] = (tU8)prOnOffStates->uOnOffStates.rBitfield.CAN2;
		au8TraceSendBuffer[21] = (tU8)prOnOffStates->uOnOffStates.rBitfield.CAN3;
		au8TraceSendBuffer[22] = (tU8)prOnOffStates->uOnOffStates.rBitfield.CAN4;
		au8TraceSendBuffer[23] = (tU8)prOnOffStates->uOnOffStates.rBitfield.ODOMETER;
		au8TraceSendBuffer[24] = (tU8)prOnOffStates->uOnOffStates.rBitfield.CELLNETWORK;
		au8TraceSendBuffer[25] = (tU8)prOnOffStates->uOnOffStates.rBitfield.EXTERNAL_PIN;
		au8TraceSendBuffer[26] = (tU8)prOnOffStates->uOnOffStates.rBitfield.USB;
		au8TraceSendBuffer[27] = (tU8)prOnOffStates->uOnOffStates.rBitfield.CD_CLAMP;
		au8TraceSendBuffer[28] = (tU8)prOnOffStates->uOnOffStates.rBitfield.ILLUMINATION;
		au8TraceSendBuffer[29] = (tU8)(((prOnOffStates->uOnOffStates.u32) & 0x000000FF) >>  0);
		au8TraceSendBuffer[30] = (tU8)(((prOnOffStates->uOnOffStates.u32) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[31] = (tU8)(((prOnOffStates->uOnOffStates.u32) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[32] = (tU8)(((prOnOffStates->uOnOffStates.u32) & 0xFF000000) >> 24);
		au8TraceSendBuffer[33] = (tU8)(((prOnOffStates->u16MsgHandle) & 0x00FF) >>  0);
		au8TraceSendBuffer[34] = (tU8)(((prOnOffStates->u16MsgHandle) & 0xFF00) >>  8);

		u8BufferLength = 35;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_LATEST_RESET_REASON : {
		DEV_WUP_trLatestResetReason* prLatestResetReason = (DEV_WUP_trLatestResetReason*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prLatestResetReason->u16ResetReasonAP) & 0x00FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prLatestResetReason->u16ResetReasonAP) & 0xFF00) >>  8);
		au8TraceSendBuffer[12] = prLatestResetReason->u8ResetReasonSCC;

		u8BufferLength = 13;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_INDICATE_STARTUP_FINISHED : {
		u8BufferLength = 10;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_EXTEND_POWER_OFF_TIMEOUT : {
		tU16* pu16TimeoutMs = (tU16*)pnArg;

		au8TraceSendBuffer[10] = *pu8TmpArgBuffer;                          // low byte requested timeout
		au8TraceSendBuffer[11] = *(pu8TmpArgBuffer+1);			    // high byte requested timeout
		au8TraceSendBuffer[12] = (tU8)(((*pu16TimeoutMs) & 0x00FF) >>  0);  // low byte reported timeout
		au8TraceSendBuffer[13] = (tU8)(((*pu16TimeoutMs) & 0xFF00) >>  8);  // high byte repoted timeout

		u8BufferLength = 14;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_CONTROL_RESET_MASTER_SUPERVISION : {
		DEV_WUP_trResetMasterSupervision* prResetMasterSupervision = (DEV_WUP_trResetMasterSupervision*)pnArg;

		au8TraceSendBuffer[10] = prResetMasterSupervision->bSwitchOff;
		au8TraceSendBuffer[11] = prResetMasterSupervision->u8ExceptionBitmask;

		u8BufferLength = 12;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_ACKNOWLEDGE_ONOFF_STATE : {
		DEV_WUP_trOnOffStateAcknowledge* prOnOffStateAcknowledge = (DEV_WUP_trOnOffStateAcknowledge*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prOnOffStateAcknowledge->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prOnOffStateAcknowledge->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((prOnOffStateAcknowledge->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((prOnOffStateAcknowledge->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[14] = (tU8)(((prOnOffStateAcknowledge->u16OnOffStateMsgHandle) & 0x00FF) >>  0);
		au8TraceSendBuffer[15] = (tU8)(((prOnOffStateAcknowledge->u16OnOffStateMsgHandle) & 0xFF00) >>  8);

		u8BufferLength = 16;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_APPLICATION_MODE : {
		tPU8 pu8ApplicationModeAP = (tPU8)pnArg;

		au8TraceSendBuffer[10] = *pu8ApplicationModeAP;

		u8BufferLength = 11;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_GET_AP_SUPERVISION_ERROR : {
		tPU8 pu8APSupervisionError = (tPU8)pnArg;

		au8TraceSendBuffer[10] = *pu8APSupervisionError;

		u8BufferLength = 11;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_REGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION : {
		tU32 u32ClientId = (tU32)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((u32ClientId) & 0xFF000000) >> 24);

		u8BufferLength = 14;
		break;
	}
	case OSAL_C_S32_IOCTRL_WUP_UNREGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION : {
		tU32 u32ClientId = (tU32)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((u32ClientId) & 0xFF000000) >> 24);

		u8BufferLength = 14;
		break;
	}
	default:
		// Intentionally do nothing
		break;
	}

	if (u8BufferLength != 0)
		LLD_vTrace(
			TR_CLASS_DEV_WUP, 
			enTraceLevel, 
			au8TraceSendBuffer,
			u8BufferLength);
}

/*******************************************************************************
*
* This function prints the content of the switch-on/off event ring buffer on 
* TTFIS.
*
*******************************************************************************/
static tVoid DEV_WUP_vTraceOnOffEventBuffer(TR_tenTraceLevel enTraceLevel)
{
	tU8 au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];
	tU8 u8Index;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	if (LLD_bIsTraceActive(TR_CLASS_DEV_WUP, enTraceLevel) != FALSE) {
		if (g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u8NumberOfUsedElements > 0) {
			au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_ONOFF_REASON_BUFFER;
			au8TraceSendBuffer[1] = 0xFF; // Heading with elements (not empty)
			au8TraceSendBuffer[2] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead) & 0x000000FF) >>  0);
			au8TraceSendBuffer[3] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead) & 0x0000FF00) >>  8);
			au8TraceSendBuffer[4] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead) & 0x00FF0000) >> 16);
			au8TraceSendBuffer[5] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead) & 0xFF000000) >> 24);
			au8TraceSendBuffer[6] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail) & 0x000000FF) >>  0);
			au8TraceSendBuffer[7] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail) & 0x0000FF00) >>  8);
			au8TraceSendBuffer[8] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail) & 0x00FF0000) >> 16);
			au8TraceSendBuffer[9] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail) & 0xFF000000) >> 24);

			LLD_vTrace(
				TR_CLASS_DEV_WUP,
				enTraceLevel,
				au8TraceSendBuffer,
				10);

			for (u8Index = 0;
			     u8Index < g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u8NumberOfUsedElements;
			     u8Index++) {

				au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_ONOFF_REASON_BUFFER;
				au8TraceSendBuffer[1] = u8Index;
				au8TraceSendBuffer[2] = g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u8Index].u8Event;
				au8TraceSendBuffer[3] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u8Index].u16MsgHandle) & 0x00FF) >>  0);
				au8TraceSendBuffer[4] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u8Index].u16MsgHandle) & 0xFF00) >>  8);
				au8TraceSendBuffer[5] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u8Index].u32Timestamp) & 0x000000FF) >>  0);
				au8TraceSendBuffer[6] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u8Index].u32Timestamp) & 0x0000FF00) >>  8);
				au8TraceSendBuffer[7] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u8Index].u32Timestamp) & 0x00FF0000) >> 16);
				au8TraceSendBuffer[8] = (tU8)(((g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u8Index].u32Timestamp) & 0xFF000000) >> 24);

				LLD_vTrace(
					TR_CLASS_DEV_WUP,
					enTraceLevel,
					au8TraceSendBuffer,
					9);
			}
		} else {
			au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_ONOFF_REASON_BUFFER;
			au8TraceSendBuffer[1] = 0xFE; // Heading without elements (empty)

			LLD_vTrace(
				TR_CLASS_DEV_WUP,
				enTraceLevel,
				au8TraceSendBuffer,
				2);
		}
	}

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* FUNCTION: DEV_WUP_vTraceNotificatonEvent()
*
* DESCRIPTION: 
*
* PARAMETER: None.
*
* GLOBALS: None.
*
* RETURNVALUE: 
*
*******************************************************************************/
static tVoid DEV_WUP_vTraceNotificatonEvent(tU32 u32ClientId, tString szNotificationEventName, OSAL_tEventMask rEventMask)
{
	tU32 u32Index;
	tU8  au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (LLD_bIsTraceActive(TR_CLASS_DEV_WUP, TR_LEVEL_USER_3) != FALSE) {
		au8TraceSendBuffer[0]  = DEV_WUP_C_U8_TRACE_TYPE_NOTIFICATION_EVENT;
		au8TraceSendBuffer[1] = (tU8)(((rEventMask) & 0xFF000000) >> 24);
		au8TraceSendBuffer[2] = (tU8)(((rEventMask) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[3] = (tU8)(((rEventMask) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[4] = (tU8)(((rEventMask) & 0x000000FF) >>  0);
		au8TraceSendBuffer[5] = (tU8)(((u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[6] = (tU8)(((u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[7] = (tU8)(((u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[8] = (tU8)(((u32ClientId) & 0xFF000000) >> 24);

		for (u32Index = 0;
		     u32Index < DEV_WUP_C_U8_NOTIFICATION_EVENT_NAME_LENGTH;
		     u32Index++)
			au8TraceSendBuffer[9+u32Index] = szNotificationEventName[u32Index];

		LLD_vTrace(
			TR_CLASS_DEV_WUP,
			TR_LEVEL_USER_3,
			au8TraceSendBuffer,
			9 + u32Index - 1);
	}
}

/*******************************************************************************
*
* This functions traces received or transmitted INC messages on TTFIS.
*
*******************************************************************************/
static tVoid DEV_WUP_vTraceIncMsg(tU8 u8Direction, const tU8 * const pu8IncMsgBuffer, tU32 u32MessageLength)
{
	tU8 au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (LLD_bIsTraceActive(TR_CLASS_DEV_WUP, TR_LEVEL_USER_2) != FALSE) {
		au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_INC_MESSAGE;
		au8TraceSendBuffer[1] = u8Direction;

		(tVoid) OSAL_pvMemoryCopy(
				(&(au8TraceSendBuffer[2])),
				pu8IncMsgBuffer,
				u32MessageLength);

		LLD_vTrace(
			TR_CLASS_DEV_WUP,
			TR_LEVEL_USER_2,
			au8TraceSendBuffer,
			u32MessageLength + 2);
	}
}

/*******************************************************************************
*
* This function prints the switch-on/off event which was received from the 
* system communication controller but dropped by the device due a multiple 
* reception.
*
*******************************************************************************/
static tVoid DEV_WUP_vTraceDroppedWakeupEvent(tU8 u8OnOffEvent, tU16 u16OnOffEventMsgHandle)
{
	tU8 au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (LLD_bIsTraceActive(TR_CLASS_DEV_WUP, TR_LEVEL_SYSTEM_MIN) != FALSE) {
		au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_DROPPED_WAKEUP_EVENT;
		au8TraceSendBuffer[1] = u8OnOffEvent;
		au8TraceSendBuffer[2] = (tU8)(((u16OnOffEventMsgHandle) & 0x00FF) >>  0);
		au8TraceSendBuffer[3] = (tU8)(((u16OnOffEventMsgHandle) & 0xFF00) >>  8);

		LLD_vTrace(
			TR_CLASS_DEV_WUP, 
			TR_LEVEL_SYSTEM_MIN, 
			au8TraceSendBuffer,
			4);
	}
}

/*******************************************************************************
*
* This function creates client individual notification events which are used to 
* notify registered clients about property changes.
*
*******************************************************************************/
static tU32 DEV_WUP_u32CreateClientNotificationEvents()
{
	tU32 u32Index;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (OSALUTIL_s32SaveNPrintFormat(
			g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
			sizeof(g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName),
			DEV_WUP_C_STRING_NOTIFICATION_EVENT_POSTFIX_FORMAT,
			DEV_WUP_C_STRING_NOTIFICATION_EVENT_PREFIX,
			u32Index+1) < 0)
				return OSAL_E_UNKNOWN;

		if (OSAL_s32EventCreate(
			g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
			&g_rModuleData.ahClientEventHandle[u32Index]) == OSAL_ERROR)
				return OSAL_u32ErrorCode();

	}

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This function deletes the client individual notification events.
*
*******************************************************************************/
static tVoid DEV_WUP_vDeleteClientNotificationEvents()
{
	tU32 u32Index;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (g_rModuleData.ahClientEventHandle[u32Index] != OSAL_C_INVALID_HANDLE) {
			if (OSAL_s32EventClose(g_rModuleData.ahClientEventHandle[u32Index]) == OSAL_OK)
				if (OSAL_s32EventDelete(g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName) == OSAL_OK) {
					g_rModuleData.ahClientEventHandle[u32Index] = OSAL_C_INVALID_HANDLE;
					g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName[0] = '\0';
				} else {
					DEV_WUP_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_WUP_vDeleteClientNotificationEvents() => OSAL_s32EventDelete() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
				}
			else
				DEV_WUP_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_WUP_vDeleteClientNotificationEvents() => OSAL_s32EventClose() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}
}

/*******************************************************************************
*
* This function establishes the INC comunication to the system communication
* controller for channel SPM_PORT.
*
*******************************************************************************/
tBool DEV_WUP_bEstablishSccIncCom(tVoid)
{
	g_rModuleData.nSocketFileDescriptorInc = socket(AF_BOSCH_INC_AUTOSAR, (tInt)SOCK_STREAM, 0);

	if (g_rModuleData.nSocketFileDescriptorInc == -1) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_bEstablishSccIncCom() => Call of socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0) failed with errno = %d",
			errno);
		goto error_socket; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	struct timeval rSocketReceiveTimeval;

	rSocketReceiveTimeval.tv_sec  = DEV_WUP_CONF_C_U32_INC_MSG_RECEIVED_TIMEOUT_S;
	rSocketReceiveTimeval.tv_usec = 0;

	if (setsockopt(
		g_rModuleData.nSocketFileDescriptorInc,
		SOL_SOCKET, SO_RCVTIMEO,
		(char*)&rSocketReceiveTimeval,
		sizeof(rSocketReceiveTimeval)) == -1) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_bEstablishSccIncCom() => Call of setsockopt(..., SO_RCVTIMEO, ...) failed with errno = %d",
				errno);
			goto error_setsockopt; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.pDatagramSocketDescriptor = dgram_init(g_rModuleData.nSocketFileDescriptorInc, DGRAM_MAX, NULL);

	if (g_rModuleData.pDatagramSocketDescriptor == NULL) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_bEstablishSccIncCom() => Call of dgram_init(..., DGRAM_MAX, NULL) failed with errno = %d",
			errno);
		goto error_dgram_init; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	struct hostent *prHostEntLocal;
	struct hostent *prHostEntRemote;
	struct sockaddr_in rSockAddrLocal;
	struct sockaddr_in rSockAddrRemote;

	prHostEntLocal = gethostbyname(g_rModuleData.coszIncHostLocal);

	if (prHostEntLocal == NULL) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_bEstablishSccIncCom() => Call of gethostbyname('%s') failed with errno = %d",
			g_rModuleData.coszIncHostLocal,
			errno);
		goto error_gethostbyname_local; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rSockAddrLocal.sin_family = AF_INET;

	memcpy((char *) &rSockAddrLocal.sin_addr.s_addr,
	       (char *) prHostEntLocal->h_addr,
	       (size_t) prHostEntLocal->h_length);

	rSockAddrLocal.sin_port = (tU16)htons(SPM_PORT);

	prHostEntRemote = gethostbyname(g_rModuleData.coszIncHostRemote);

	if (prHostEntRemote == NULL) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_bEstablishSccIncCom() => Call of gethostbyname('%s') failed with errno = %d",
			g_rModuleData.coszIncHostRemote,
			errno);
		goto error_gethostbyname_remote; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rSockAddrRemote.sin_family = AF_INET;

	memcpy((char *) &rSockAddrRemote.sin_addr.s_addr,
	       (char *) prHostEntRemote->h_addr,
	       (size_t) prHostEntRemote->h_length);

	rSockAddrRemote.sin_port = (tU16)htons(SPM_PORT);

	if (bind(
		g_rModuleData.nSocketFileDescriptorInc,
		(struct sockaddr *) &rSockAddrLocal, /*lint !e64, authorized LINT-deactivation #<408> */
		sizeof(rSockAddrLocal)) == -1) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_bEstablishSccIncCom() => Call of bind(address local = '%s') failed with errno = %d",
				inet_ntoa(rSockAddrLocal.sin_addr),
				errno);
			goto error_bind; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (connect(
		g_rModuleData.nSocketFileDescriptorInc, 
		(struct sockaddr *) &rSockAddrRemote, /*lint !e64, authorized LINT-deactivation #<408> */
		sizeof(rSockAddrRemote)) == -1) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_bEstablishSccIncCom() => Call of connect(address remote = '%s') failed with errno = %d",
				inet_ntoa(rSockAddrRemote.sin_addr),
				errno);
			goto error_connect; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	return TRUE;
	
error_connect: // FALLTROUGH
error_bind: // FALLTROUGH
error_gethostbyname_remote: // FALLTROUGH
error_gethostbyname_local:

	dgram_exit(g_rModuleData.pDatagramSocketDescriptor);

error_dgram_init: // FALLTROUGH
error_setsockopt:

	close(g_rModuleData.nSocketFileDescriptorInc);

error_socket:

	return FALSE;
}

/*******************************************************************************
*
* This function releases the INC communication to the system communication
* controller for channel SPM_PORT.
*
*******************************************************************************/
static tBool DEV_WUP_bReleaseSccIncCom(tVoid)
{
	tBool bResult = TRUE;

	if (g_rModuleData.nSocketFileDescriptorInc == -1)
		return FALSE;

	if (g_rModuleData.pDatagramSocketDescriptor == NULL)
		return FALSE;

	if (dgram_exit(g_rModuleData.pDatagramSocketDescriptor) == -1)
		bResult = FALSE;

	if (close(g_rModuleData.nSocketFileDescriptorInc) == -1)
		bResult = FALSE;

	return bResult;
}

/*******************************************************************************
*
* This function installs the thread which handles the reception of INC messages 
* from the system communication controller.
*
*******************************************************************************/
static tU32 DEV_WUP_u32InstallIncMsgThread(tVoid)
{
	tU32                   u32OsalErrorCode  = OSAL_E_NOERROR;
	OSAL_trThreadAttribute rThreadAttribute;

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_INC_MSG_THREAD_NAME,
		&g_rModuleData.hSemIncMsgThread,
		(tU32) 0) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32EventCreate(
		DEV_WUP_C_STRING_INC_MSG_THREAD_EVENT_NAME,
		&g_rModuleData.hEventIncMsgThread) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_event_create; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rThreadAttribute.szName       = (tString)DEV_WUP_C_STRING_INC_MSG_THREAD_NAME;
	rThreadAttribute.s32StackSize = DEV_WUP_C_S32_INC_MSG_THREAD_STK_SIZE;
	rThreadAttribute.u32Priority  = DEV_WUP_C_U32_INC_MSG_THREAD_PRIO;
	rThreadAttribute.pfEntry      = (OSAL_tpfThreadEntry)DEV_WUP_vIncMsgThread;
	rThreadAttribute.pvArg        = NULL;

	DEV_WUP_vTraceFormatted(TR_LEVEL_USER_4, "DEV_WUP_u32InstallIncMsgThread() => Try to spawn thread ...");

	if (OSAL_ThreadSpawn(&rThreadAttribute) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();

		goto error_thread_spawn; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		g_rModuleData.hSemIncMsgThread,
		DEV_WUP_C_U32_SEM_THREAD_TIMEOUT_MS) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();

		goto error_semaphore_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (g_rModuleData.u8IncMsgThreadState != DEV_WUP_C_U8_THREAD_RUNNING) {

		u32OsalErrorCode = OSAL_E_UNKNOWN;

		DEV_WUP_vTraceFormatted(TR_LEVEL_FATAL, "DEV_WUP_u32InstallIncMsgThread() => ... failed to install thread");

		goto error_thread_state; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_WUP_vTraceFormatted(TR_LEVEL_USER_4, "DEV_WUP_u32InstallIncMsgThread() => ... properly installed thread");

	return OSAL_E_NOERROR;

error_thread_state:
error_semaphore_wait:
error_thread_spawn:

	if (OSAL_s32EventClose(g_rModuleData.hEventIncMsgThread) == OSAL_OK)
		if (OSAL_s32EventDelete(DEV_WUP_C_STRING_INC_MSG_THREAD_EVENT_NAME) == OSAL_OK)
			g_rModuleData.hEventIncMsgThread = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32InstallIncMsgThread() => OSAL_s32EventDelete() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32InstallIncMsgThread() => OSAL_s32EventClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_event_create:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemIncMsgThread) == OSAL_OK) {
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_INC_MSG_THREAD_NAME) == OSAL_OK)
			g_rModuleData.hSemIncMsgThread = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32InstallIncMsgThread() => OSAL_s32SemaphoreDelete() failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));
	} else {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32InstallIncMsgThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function un-installs the thread which handles the reception of INC 
* messages from the system communication controller.
*
*******************************************************************************/
static tU32 DEV_WUP_u32UninstallIncMsgThread(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (OSAL_s32EventPost(
		g_rModuleData.hEventIncMsgThread,
		DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD, 
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_WUP_u32UninstallIncMsgThread() => Sent event STOP_THREAD to leave thread ...");

	if (OSAL_s32SemaphoreWait(
		g_rModuleData.hSemIncMsgThread,
		DEV_WUP_C_U32_SEM_THREAD_TIMEOUT_MS) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (g_rModuleData.u8IncMsgThreadState != DEV_WUP_C_U8_THREAD_SHUTTING_DOWN) {
			u32OsalErrorCode = OSAL_E_UNKNOWN;
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}
	
	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_WUP_u32UninstallIncMsgThread() => Thread acknowledged shutdown request ... wait 1 second ...");

	OSAL_s32ThreadWait(1000); // Give thread 1 second to leave its own thread context.

	g_rModuleData.u8IncMsgThreadState = DEV_WUP_C_U8_THREAD_OFF;

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_WUP_u32UninstallIncMsgThread() => ... 1 second expired and thread should be gone");

error_out:

	if (OSAL_s32EventClose(g_rModuleData.hEventIncMsgThread) == OSAL_OK)
		if (OSAL_s32EventDelete(DEV_WUP_C_STRING_INC_MSG_THREAD_EVENT_NAME) == OSAL_OK)
			g_rModuleData.hEventIncMsgThread = OSAL_C_INVALID_HANDLE;
		else
			u32OsalErrorCode = OSAL_u32ErrorCode();
	else
		u32OsalErrorCode = OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemIncMsgThread) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_INC_MSG_THREAD_NAME) == OSAL_OK)
			g_rModuleData.hSemIncMsgThread = OSAL_C_INVALID_HANDLE;
		else
			u32OsalErrorCode = OSAL_u32ErrorCode();
	else
		u32OsalErrorCode = OSAL_u32ErrorCode();

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function represents the thread which handles the reception of INC 
* messages from the system communication controller.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncMsgThread(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	tU32            u32OsalErrorCode    = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemIncMsgThread    = OSAL_C_INVALID_HANDLE;
	tInt            nNumOfBytesReceived = 0;
	OSAL_tEventMask rEventMaskResult    = 0;

	g_rModuleData.u8IncMsgThreadState = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_INC_MSG_THREAD_NAME, 
		&hSemIncMsgThread) == OSAL_ERROR) {
		
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vIncMsgThread() => OSAL_s32SemaphoreOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.u8IncMsgThreadState = DEV_WUP_C_U8_THREAD_RUNNING;

	if (OSAL_s32SemaphorePost(hSemIncMsgThread) == OSAL_ERROR) {
		g_rModuleData.u8IncMsgThreadState = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vIncMsgThread() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	while (DEV_WUP_C_U8_THREAD_RUNNING == g_rModuleData.u8IncMsgThreadState) {
		if (OSAL_s32EventWait(
			g_rModuleData.hEventIncMsgThread,
			DEV_WUP_C_U32_INC_MSG_THREAD_EVENT_MASK_ALL,
			OSAL_EN_EVENTMASK_OR,
			OSAL_C_TIMEOUT_NOBLOCKING,
			&rEventMaskResult) == OSAL_OK) {

			DEV_WUP_vOnOsalEventReceived(rEventMaskResult, hSemIncMsgThread);

		} else {
			u32OsalErrorCode = OSAL_u32ErrorCode();

			if (u32OsalErrorCode == OSAL_E_TIMEOUT) {
				if (g_rModuleData.bSccIncCommunicationEstablished == TRUE) {
					nNumOfBytesReceived = dgram_recv(
								g_rModuleData.pDatagramSocketDescriptor,
								g_rModuleData.au8IncRcvBuffer,
								sizeof(g_rModuleData.au8IncRcvBuffer));

					if (nNumOfBytesReceived == -1) {
						if (errno != EWOULDBLOCK)
							DEV_WUP_vTraceFormatted(
								TR_LEVEL_FATAL,
								"DEV_WUP_vIncMsgThread() => socket recv() failed due to errno = %s",
								strerror(errno));
					} else {
						(tVoid)DEV_WUP_bEvaluateReceivedIncMsg((tU32)nNumOfBytesReceived);
					}
				} else {
					// If the INC communication to the SCC can't be established, we at least need to
					// put this thread to sleep for a while to avoid to let it running continuously.
					OSAL_s32ThreadWait(DEV_WUP_CONF_C_U32_INC_MSG_RECEIVED_TIMEOUT_S * 1000);
				}
			} else {
				DEV_WUP_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_WUP_vIncMsgThread() => OSAL_s32EventWait() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
			}
		}
	}

	if (OSAL_s32SemaphoreClose(hSemIncMsgThread) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vIncMsgThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_out:

	if (DEV_WUP_C_U8_THREAD_NOT_INSTALLED == g_rModuleData.u8IncMsgThreadState)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vIncMsgThread() => Immediately left due to setup failure");
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_USER_3,
			"DEV_WUP_vIncMsgThread() => Left after controlled shutdown");
}

/*******************************************************************************
*
* This function evaluates the INC messages which are received from the system 
* communication controller within the context of the thread function 
* DEV_WUP_vIncMsgThread().
*
*******************************************************************************/
static tBool DEV_WUP_bEvaluateReceivedIncMsg(tU32 u32NumOfBytesReceived)
{
	tU8   u8MsgId         = g_rModuleData.au8IncRcvBuffer[0];
	tBool bUnknownMessage = TRUE;
	tU8   u8RejectReason  = DEV_WUP_C_U8_REJECT_OK;
	tBool bResult         = FALSE;

	tU32  u32Index;

	for (u32Index = 0;
	     u32Index < (sizeof(g_carIncMsgHandlerList) / sizeof(trIncMsgHandler));
	     u32Index++) {

		if (g_carIncMsgHandlerList[u32Index].u8MsgId == u8MsgId) {

			bUnknownMessage = FALSE;

			if ((TRUE == g_rModuleData.bIncMsgCatVersionMatches)                             ||
			    (FALSE == g_carIncMsgHandlerList[u32Index].bRejectIfMsgCatVersionDoesntMatch)  ) {

				if (g_carIncMsgHandlerList[u32Index].u32MsgLen == u32NumOfBytesReceived) {

					if (g_carIncMsgHandlerList[u32Index].pfu8IncMsgHandler != NULL) {

						DEV_WUP_vTraceIncMsg(
							DEV_WUP_C_U8_INC_MSG_RECEIVE, 
							g_rModuleData.au8IncRcvBuffer,
							u32NumOfBytesReceived);

							u8RejectReason = g_carIncMsgHandlerList[u32Index].pfu8IncMsgHandler(); /*lint !e746, authorized LINT-deactivation #<74> */

							if (u8RejectReason != DEV_WUP_C_U8_REJECT_OK)
								DEV_WUP_vIncSend_RReject(u8RejectReason, u8MsgId);
							else
								bResult = TRUE;
					} else {
						DEV_WUP_vTraceFormatted(
							TR_LEVEL_ERRORS,
							"DEV_WUP_bEvaluateReceivedIncMsg() => Received message ID = 0x%02X is not yet supported",
							g_carIncMsgHandlerList[u32Index].u8MsgId);
					}
				} else {
					DEV_WUP_vTraceFormatted(
						TR_LEVEL_ERRORS, 
						"DEV_WUP_bEvaluateReceivedIncMsg() => Wrong length of received message ID = 0x%02X. Expected = %d, Received = %d",
						g_carIncMsgHandlerList[u32Index].u8MsgId,
						g_carIncMsgHandlerList[u32Index].u32MsgLen,
						u32NumOfBytesReceived);
				}
			} else {

				DEV_WUP_vTraceFormatted(
					TR_LEVEL_ERRORS,
					"Received message >> 0x%02X  ... reject due to catalogue version mismatch",
					u8MsgId);

				DEV_WUP_vTraceFormatted(
					TR_LEVEL_ERRORS,
					"   - Catalogue requested => Major = %u, Minor = %u",
					DEV_WUP_C_U8_INC_MSG_CAT_MAJOR_VERSION_NUMBER,
					DEV_WUP_C_U8_INC_MSG_CAT_MINOR_VERSION_NUMBER);
        
				if (g_rModuleData.u8IncMsgCatMajorVersionServer != 0x00)
					DEV_WUP_vTraceFormatted(
						TR_LEVEL_ERRORS,
						"   - Catalogue offered => Major = %u, Minor = %u",
						g_rModuleData.u8IncMsgCatMajorVersionServer,
						g_rModuleData.u8IncMsgCatMinorVersionServer);
				else
					DEV_WUP_vTraceFormatted(
						TR_LEVEL_ERRORS,
						"   - Catalogue offered => Major = UNKNOWN, Minor = UNKNOWN");

				DEV_WUP_vIncSend_RReject(DEV_WUP_C_U8_REJECT_VERSION_MISMATCH, u8MsgId);
			}
			break;
		}
	}

	if (TRUE == bUnknownMessage) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"Received message >> Unknown message ID = 0x%02X",
			u8MsgId);

		DEV_WUP_vIncSend_RReject(DEV_WUP_C_U8_REJECT_UNKNOWN_MESSAGE, u8MsgId);
	}

	return bResult;
}

/*******************************************************************************
*
* This function evaluates the OSAL events which are received within the context
* of the thread function DEV_WUP_vIncMsgThread().
*
*******************************************************************************/
static tVoid DEV_WUP_vOnOsalEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemIncMsgThread)
{
	if (OSAL_s32EventPost(
		g_rModuleData.hEventIncMsgThread,
		~rEventMaskResult, 
		OSAL_EN_EVENTMASK_AND) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vOnOsalEventReceived() => OSAL_s32EventPost() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));

	if (rEventMaskResult & DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_WUP_vOnOsalEventReceived() => Event STOP_THREAD received, thread is shutting down ...");

		g_rModuleData.u8IncMsgThreadState = DEV_WUP_C_U8_THREAD_SHUTTING_DOWN;

		if (OSAL_s32SemaphorePost(hSemIncMsgThread) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vOnOsalEventReceived() => OSAL_s32SemaphorePost() for thread shutdown failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD);
	}

	if (rEventMaskResult)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vOnOsalEventReceived() => OSAL_s32EventWait() received unknown event = 0x%08X",
			rEventMaskResult);
}

/*******************************************************************************
*
* This function installs the thread which is used to perform device internal 
* actions which are initiated by use of OSAL messages.
*
*******************************************************************************/
static tU32 DEV_WUP_u32InstallOsalMsgThread(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	OSAL_trThreadAttribute rThreadAttribute;

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_OSAL_MSG_THREAD_NAME,
		&g_rModuleData.hSemOsalMsgThread,
		(tU32) 0) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32MessageQueueCreate(
		DEV_WUP_C_STRING_OSAL_MSG_QUEUE_NAME, 
		DEV_WUP_C_U32_OSAL_MSG_QUEUE_MAX_MSGS,
		DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN,
		OSAL_EN_READWRITE, 
		&g_rModuleData.hMsgQueueOsalThread) == OSAL_ERROR) {
		
		u32OsalErrorCode = OSAL_u32ErrorCode();

		goto error_message_queue_create; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rThreadAttribute.szName       = (tChar*)DEV_WUP_C_STRING_OSAL_MSG_THREAD_NAME;
	rThreadAttribute.s32StackSize = DEV_WUP_C_S32_OSAL_MSG_THREAD_STK_SIZE;
	rThreadAttribute.u32Priority  = DEV_WUP_C_U32_OSAL_MSG_THREAD_PRIO;
	rThreadAttribute.pfEntry      = (OSAL_tpfThreadEntry) DEV_WUP_vOsalMsgThread;
	rThreadAttribute.pvArg        = NULL;

	DEV_WUP_vTraceFormatted(TR_LEVEL_USER_4, "DEV_WUP_u32InstallOsalMsgThread() => Try to spawn thread ...");

	if (OSAL_ThreadSpawn(&rThreadAttribute) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();

		goto error_thread_spawn; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		g_rModuleData.hSemOsalMsgThread,
		DEV_WUP_C_U32_SEM_THREAD_TIMEOUT_MS) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();

		goto error_semaphore_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (g_rModuleData.u8OsalMsgThreadState != DEV_WUP_C_U8_THREAD_RUNNING) {

		u32OsalErrorCode = OSAL_E_UNKNOWN;

		DEV_WUP_vTraceFormatted(TR_LEVEL_FATAL, "DEV_WUP_u32InstallOsalMsgThread() => ... failed to install thread");

		goto error_thread_state; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_WUP_vTraceFormatted(TR_LEVEL_USER_4, "DEV_WUP_u32InstallOsalMsgThread() => ... properly installed thread");

	return OSAL_E_NOERROR;
	
error_thread_state:
error_semaphore_wait:
error_thread_spawn:

	if (OSAL_s32MessageQueueClose(g_rModuleData.hMsgQueueOsalThread) == OSAL_OK) {
		if (OSAL_s32MessageQueueDelete(DEV_WUP_C_STRING_OSAL_MSG_QUEUE_NAME) == OSAL_OK)
			g_rModuleData.hMsgQueueOsalThread = OSAL_C_INVALID_HANDLE;
		else
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32InstallOsalMsgThread() => OSAL_s32MessageQueueDelete() failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));
	} else {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32InstallOsalMsgThread() => OSAL_s32MessageQueueClose() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));
	}

error_message_queue_create:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemOsalMsgThread) == OSAL_OK) {
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_OSAL_MSG_THREAD_NAME) == OSAL_OK) {
			g_rModuleData.hSemOsalMsgThread = OSAL_C_INVALID_HANDLE;
		} else {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32InstallOsalMsgThread() => OSAL_s32SemaphoreDelete() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	} else {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32InstallOsalMsgThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function un-installs the thread which is used to perform device internal 
* actions which are initiated by use of OSAL messages.
*
*******************************************************************************/
static tU32 DEV_WUP_u32UninstallOsalMsgThread(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	tU8 au8MsqQueueBuffer[DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN];

	au8MsqQueueBuffer[0] = DEV_WUP_C_U8_OSAL_MSG_STOP_THREAD;

	if (OSAL_s32MessageQueuePost(
		g_rModuleData.hMsgQueueOsalThread,
		au8MsqQueueBuffer, 
		DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN,
		OSAL_C_U32_MQUEUE_PRIORITY_LOWEST) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_WUP_u32UninstallOsalMsgThread() => Sent message STOP_THREAD to leave thread ...");

	if (OSAL_s32SemaphoreWait(
		g_rModuleData.hSemOsalMsgThread,
		DEV_WUP_C_U32_SEM_THREAD_TIMEOUT_MS) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (g_rModuleData.u8OsalMsgThreadState != DEV_WUP_C_U8_THREAD_SHUTTING_DOWN) {
			u32OsalErrorCode = OSAL_E_UNKNOWN;
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_WUP_u32UninstallOsalMsgThread() => Thread acknowledged shutdown request ... wait 1 second ...");

	OSAL_s32ThreadWait(1000); // Give thread DEV_WUP_vIncMsgThread() 1 second to leave its own thread context.

	g_rModuleData.u8OsalMsgThreadState = DEV_WUP_C_U8_THREAD_OFF;

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_WUP_u32UninstallOsalMsgThread() => ... 1 second expired and thread should be gone");

error_out:

	if (OSAL_s32MessageQueueClose(g_rModuleData.hMsgQueueOsalThread) == OSAL_OK)
		if (OSAL_s32MessageQueueDelete(DEV_WUP_C_STRING_OSAL_MSG_QUEUE_NAME) == OSAL_OK)
			g_rModuleData.hMsgQueueOsalThread = OSAL_C_INVALID_HANDLE;
		else
			u32OsalErrorCode = OSAL_u32ErrorCode();
	else
		u32OsalErrorCode = OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemOsalMsgThread) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_OSAL_MSG_THREAD_NAME) == OSAL_OK)
			g_rModuleData.hSemOsalMsgThread = OSAL_C_INVALID_HANDLE;
		else
			u32OsalErrorCode = OSAL_u32ErrorCode();
	else
		u32OsalErrorCode = OSAL_u32ErrorCode();

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function represents the thread which handles the OSAL messages which are
* used to perform internal actions of this device.
*
*******************************************************************************/
static tVoid DEV_WUP_vOsalMsgThread(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	OSAL_tSemHandle hSemOsalMsgThread = OSAL_C_INVALID_HANDLE;
	tU32            u32MsgPrio        = OSAL_C_U32_MQUEUE_PRIORITY_LOWEST;

	tU8 au8MsqQueueBuffer[DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN];

	g_rModuleData.u8OsalMsgThreadState = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_OSAL_MSG_THREAD_NAME, 
		&hSemOsalMsgThread) == OSAL_ERROR) {
		
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vOsalMsgThread() => OSAL_s32SemaphoreOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.u8OsalMsgThreadState = DEV_WUP_C_U8_THREAD_RUNNING;

	if (OSAL_s32SemaphorePost(hSemOsalMsgThread) == OSAL_ERROR) {
		g_rModuleData.u8OsalMsgThreadState = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vOsalMsgThread() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	while (DEV_WUP_C_U8_THREAD_RUNNING == g_rModuleData.u8OsalMsgThreadState) {
		if (OSAL_s32MessageQueueWait(
			g_rModuleData.hMsgQueueOsalThread,
			au8MsqQueueBuffer,
			DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN,
			&u32MsgPrio,
			OSAL_C_TIMEOUT_FOREVER) > 0) {
			DEV_WUP_vOnOsalMsgReceived(au8MsqQueueBuffer, hSemOsalMsgThread);
		} else {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vOsalMsgThread() => OSAL_s32MessageQueueWait() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}

	if (OSAL_s32SemaphoreClose(hSemOsalMsgThread) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vOsalMsgThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_out:

	if (DEV_WUP_C_U8_THREAD_NOT_INSTALLED == g_rModuleData.u8OsalMsgThreadState)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vOsalMsgThread() => Immediately left due to setup failure");
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_USER_3,
			"DEV_WUP_vOsalMsgThread() => Left after controlled shutdown");
}

/*******************************************************************************
*
* This function evaluates the OSAL messages which are received within the context
* of the thread function DEV_WUP_vOsalMsgThread().
*
*******************************************************************************/
static tVoid DEV_WUP_vOnOsalMsgReceived(const tU8 * const pau8MsqQueueBuffer, OSAL_tSemHandle hSemOsalMsgThread)
{
	tU8 u8OsalMsgCode = *pau8MsqQueueBuffer;

	if (DEV_WUP_C_U8_OSAL_MSG_TRACE_CALLBACK_COMMAND_RECEIVED == u8OsalMsgCode) {

		DEV_WUP_vEvaluateTraceCallbackCommand();

	} else if (DEV_WUP_C_U8_OSAL_MSG_STOP_THREAD == u8OsalMsgCode) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_WUP_vOsalMsgThread() => ... message STOP_INC_MSG_HANDLER received, thread is shutting down ...");

		g_rModuleData.u8OsalMsgThreadState = DEV_WUP_C_U8_THREAD_SHUTTING_DOWN;

		if (OSAL_s32SemaphorePost(hSemOsalMsgThread) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vOsalMsgThread() => OSAL_s32SemaphorePost() for thread shutdown failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));

	} else if (DEV_WUP_C_U8_OSAL_MSG_SEND_INC_MSG == u8OsalMsgCode) {

		DEV_WUP_vSendIncMsgTransferedFromOtherContext(pau8MsqQueueBuffer);

	} else {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vOsalMsgThread() => OSAL_s32MessageQueueWait() received unknown message = %d",
			u8OsalMsgCode);
	}
}

/*******************************************************************************
*
* This function determines the real name of the undervoltage Linux kernel 
* device. The name of the device is not fix as it contains a numerical counting
* value ## which might change.
*
* /sys/bus/platform/drivers/undervoltage/undervoltage.##/trigger 
*
*******************************************************************************/
static tBool DEV_WUP_bGetUndervoltageDeviceName(tChar* szDeviceName)
{
	const tChar* copcUndervoltageDevicePath = "/sys/bus/platform/drivers/undervoltage";
	DIR*         dir_ptr                    = opendir(copcUndervoltageDevicePath);
	tBool        bResult = FALSE;

	struct dirent* dir_entry;

	if (!dir_ptr) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_bGetUndervoltageDeviceName() => opendir('%s') failed with errno = %d",
			copcUndervoltageDevicePath,
			errno);
		return FALSE;
	}

	do {
		errno = 0;
		dir_entry = readdir(dir_ptr);

		if (errno != 0) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_bGetUndervoltageDeviceName() => readdir('%s') failed with errno = %d",
				copcUndervoltageDevicePath,
				errno);
			break;
		}

		if ((dir_entry != NULL) && (dir_entry->d_type == DT_LNK)) {
			if (OSALUTIL_s32SaveNPrintFormat(
				szDeviceName,
				DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN,
				"%s/%s",
				copcUndervoltageDevicePath,
				dir_entry->d_name) > 0)
					bResult = TRUE;
			break;
		}
	} while (dir_entry != NULL);

	if (bResult == FALSE)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_bGetUndervoltageDeviceName() => missing link under '%s'",
			copcUndervoltageDevicePath);

	closedir(dir_ptr);

	return bResult;
}

/*******************************************************************************
*
* This function sets the GPIO pin CPU_RUN to ouput HIGH or LOW via the
* undervoltage Linux kernel device.
*
*******************************************************************************/
static tBool DEV_WUP_bSetGpioCpuRun(tS32 s32GpioControl)
{
	tU32             u32OsalErrorCode  = OSAL_E_UNKNOWN;
	tInt             nFd               = -1;
	tChar            chCommand         = (s32GpioControl == OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE) ? DEV_WUP_C_CHAR_SET_CPU_RUN_ACTIVE : DEV_WUP_C_CHAR_SET_CPU_RUN_INACTIVE;

	tChar            szUndervoltageDeviceName[DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN];
	tChar            szUndervoltageTrigger[DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN];

	szUndervoltageDeviceName[0] = '\0';
	szUndervoltageTrigger[0]    = '\0';

	if (DEV_WUP_bGetUndervoltageDeviceName(szUndervoltageDeviceName) == FALSE)
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */

	if (OSALUTIL_s32SaveNPrintFormat(
		szUndervoltageTrigger,
		DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN,
		"%s/%s",
		szUndervoltageDeviceName,
		"trigger") <= 0)
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */

	nFd = open(szUndervoltageTrigger, O_WRONLY);

	if (nFd == -1) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"open('%s') => failed with errno = %d",
			szUndervoltageTrigger,
			errno);
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (write(nFd, &chCommand, sizeof(tChar)) != -1)
		u32OsalErrorCode = OSAL_E_NOERROR;

	close(nFd);

error_out:

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_bSetGpioCpuRun(%s) => failed with errno = %d",
			(s32GpioControl == OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE) ? "ACTIVE = LOW" : "INACTIVE = HIGH",
			errno);
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_WUP_bSetGpioCpuRun(%s) => SUCCESS",
			(s32GpioControl == OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE) ? "ACTIVE = LOW" : "INACTIVE = HIGH");

	return (u32OsalErrorCode == OSAL_E_NOERROR);
}

/*******************************************************************************
*
* This function triggers the undervoltage Linux kernel device to power off the
* eMMC and finally set the CPU run line to inactive.
*
*******************************************************************************/
static tBool DEV_WUP_bTriggerEmmcPowerOff(tChar chCommand)
{
	tInt  nFd                    = -1;
	tChar chQuiescentState       = '0';
	tChar chLocalCommand         = chCommand;
	tBool bSuccess               = FALSE;
	tU8   u8PeriodicDelayCounter = 0;

	tChar szUndervoltageDeviceName[DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN];
	tChar szUndervoltageTrigger[DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN];
	tChar szUndervoltageQuiescentState[DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN];

	szUndervoltageDeviceName[0]     = '\0';
	szUndervoltageTrigger[0]        = '\0';
	szUndervoltageQuiescentState[0] = '\0';

	if (DEV_WUP_bGetUndervoltageDeviceName(szUndervoltageDeviceName) == FALSE)
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */

	if (OSALUTIL_s32SaveNPrintFormat(
		szUndervoltageTrigger,
		DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN,
		"%s/%s",
		szUndervoltageDeviceName,
		"trigger") <= 0)
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */

	if (OSALUTIL_s32SaveNPrintFormat(
		szUndervoltageQuiescentState,
		DEV_WUP_C_U8_MAX_UNDERVOLTAGE_DEVICE_NAME_LEN,
		"%s/%s",
		szUndervoltageDeviceName,
		"quiescent_state") <= 0)
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */

	nFd = open(szUndervoltageTrigger, O_WRONLY);

	if (nFd == -1)
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */

	if (write(nFd, &chLocalCommand, sizeof(tChar)) == -1) {
		close(nFd);
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	close(nFd);

	nFd = open(szUndervoltageQuiescentState, O_RDONLY);

	for (u8PeriodicDelayCounter = 20; u8PeriodicDelayCounter > 0; u8PeriodicDelayCounter--) {
		OSAL_s32ThreadWait(50);

		if (read(nFd, &chQuiescentState, sizeof(tChar)) == -1) {
			close(nFd);
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}

		if (chQuiescentState == '1') {
			bSuccess = TRUE;
			break;
		}

		// Reposition the file offset to 0 for the next read() call.
		if (lseek(nFd, 0, SEEK_SET) == -1) {
			close(nFd);
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}
	}

	close(nFd);

error_out:

	DEV_WUP_vTraceFormatted(
		(bSuccess == TRUE) ? TR_LEVEL_USER_4 : TR_LEVEL_FATAL,
		"DEV_WUP_bTriggerEmmcPowerOff() => %s",
		(bSuccess == TRUE) ? "SUCCESS" : "FAILED");

	return bSuccess;
}

/*******************************************************************************
*
* Perform an eMMC remount read-only via a root daemon call.
*
*******************************************************************************/
static tBool DEV_WUP_bEmmcRemountReadOnly(tVoid)
{
	CmdData rCmdData = { 0 };
	
	rCmdData = DEV_WUP_ROOTDAEMON_CALLER_rPerformRootOp(
			"dev_wup",
			EMMC_REMOUNT_READ_ONLY,
			"");

	if (rCmdData.errorNo != ERR_NONE) {
		DEV_WUP_vWriteErrorMemoryFormatted(
			"DEV_WUP_bEmmcRemountReadOnly() failed with Root-Daemon error = %d",
			rCmdData.errorNo);
		return FALSE;
	}

	if (OSAL_s32StringCompare(rCmdData.message, "SUCCESS") != 0) {
		DEV_WUP_vWriteErrorMemoryFormatted(
			"DEV_WUP_bEmmcRemountReadOnly() => Root-Daemon command EMMC_REMOUNT_READ_ONLY failed");
		return FALSE;
	}

	return TRUE;
}

/*******************************************************************************
*
* This function sets the GPIO pin CPU_PWR_OFF to ouput HIGH or LOW via
* GPIO device.
*
*******************************************************************************/
static tBool DEV_WUP_bSetGpioCpuPwrOff(tS32 s32GpioControl)
{
	tU32               u32OsalErrorCode  = OSAL_E_NOERROR;
	OSAL_tIODescriptor rGpioIODescriptor = OSAL_ERROR;
	OSAL_trGPIOData    rGPIOData         = { 0 };

	rGpioIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, OSAL_EN_READWRITE);

	if (rGpioIODescriptor == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();	
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rGPIOData.tId = (OSAL_tGPIODevID) DEV_WUP_CONF_C_EN_GPIO_CPU_PWR_OFF;

	if (OSAL_s32IOControl(
		rGpioIODescriptor, 
		s32GpioControl,
		(intptr_t)&rGPIOData) == OSAL_ERROR)
			u32OsalErrorCode = OSAL_u32ErrorCode();	

	if (OSAL_s32IOClose(rGpioIODescriptor) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_bSetGpioCpuPwrOff() => OSAL_IOClose(DEVICE_GPIO) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_out:

	DEV_WUP_vTraceFormatted(
		(u32OsalErrorCode == OSAL_E_NOERROR) ? TR_LEVEL_USER_4 : TR_LEVEL_FATAL,
		"DEV_WUP_bSetGpioCpuPwrOff(%s) => %s",
		(s32GpioControl == OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE) ? "ACTIVE = LOW" : "INACTIVE = HIGH",
		(u32OsalErrorCode == OSAL_E_NOERROR) ? "SUCCESS" : "FAILED");

	return (u32OsalErrorCode == OSAL_E_NOERROR);
}

/*******************************************************************************
*
* This function returns the start-type of the system.
*
*******************************************************************************/
static tU32 DEV_WUP_u32GetStarttype(tPU8 pu8Starttype, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32            u32OsalErrorCode      = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore            = OSAL_C_INVALID_HANDLE;
	tBool           bRStartupInfoReceived = FALSE;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	bRStartupInfoReceived = prGlobalData->bRStartupInfoReceived;
	*pu8Starttype         = prGlobalData->u8Starttype;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	if (FALSE == bRStartupInfoReceived) {
		if (OSAL_s32SemaphoreOpen(
			DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME,
			&hSemaphore) == OSAL_ERROR)
				return OSAL_u32ErrorCode();
			
		if (OSAL_s32SemaphoreWait(
			hSemaphore,
			DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_OK) {

			u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
	
			if (u32OsalErrorCode == OSAL_E_NOERROR) {
				*pu8Starttype = prGlobalData->u8Starttype;
				DEV_WUP_vDataUnlock(hSemDataAccess);
			}
		} else {
			u32OsalErrorCode = OSAL_u32ErrorCode();
		}

		if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32GetStarttype() => OSAL_s32SemaphoreClose() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function returns the wake-up reason of the system.
*
*******************************************************************************/
static tU32 DEV_WUP_u32GetWakeupReason(tPU8 pu8WakeupReason, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32            u32OsalErrorCode    = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore          = OSAL_C_INVALID_HANDLE;
	tBool           bRWupReasonReceived = FALSE;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	bRWupReasonReceived = prGlobalData->bRWupReasonReceived;
	*pu8WakeupReason    = prGlobalData->u8WupReason;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	if (FALSE == bRWupReasonReceived) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_u32GetWakeupReason() => No yet received a valid R_WAKEUP_REASON message. Retry once and again request via C_GET_DATA",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		DEV_WUP_vIncSend_CGetData(DEV_WUP_C_U8_GET_DATA_MODE_AS_SPECIFIED, DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_REASON);

		if (OSAL_s32SemaphoreOpen(
			DEV_WUP_C_STRING_SEM_WAKEUP_MSG_RECEIVED_NAME,
			&hSemaphore) == OSAL_ERROR)
				return OSAL_u32ErrorCode();

		if (OSAL_s32SemaphoreWait(
			hSemaphore,
			DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_OK) {

			u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
	
			if (u32OsalErrorCode == OSAL_E_NOERROR) {
				*pu8WakeupReason = prGlobalData->u8WupReason;
				DEV_WUP_vDataUnlock(hSemDataAccess);
			}
		} else {
			u32OsalErrorCode = OSAL_u32ErrorCode();
		}

		if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32GetStarttype() => OSAL_s32SemaphoreClose() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function returns the latest reset reasons of the application processor
* and the system communication controller..
*
*******************************************************************************/
static tU32 DEV_WUP_u32GetLatestResetReason(DEV_WUP_trLatestResetReason* prLatestResetReason, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32            u32OsalErrorCode                 = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore                       = OSAL_C_INVALID_HANDLE;
	tBool           bRIndicateClientAppStateReceived = FALSE;
	tU8             u8ApplicationModeAP              = prGlobalData->u8ApplicationModeAP;
	tU8             u8ResetClassificationAP          = prGlobalData->rPramLocal.u8ResetClassificationAP;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	bRIndicateClientAppStateReceived      = prGlobalData->bRIndicateClientAppStateReceived;
	prLatestResetReason->u8ResetReasonSCC = prGlobalData->u8ResetReasonSCC;
	prLatestResetReason->u16ResetReasonAP = prGlobalData->rPramLocal.u16ResetReasonAP;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	if (FALSE == bRIndicateClientAppStateReceived) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_u32GetLatestResetReason() => No yet received a valid R_INDICATE_CLIENT_APP_STATE message. Retry once and again send C_INDICATE_CLIENT_APP_STATE",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		DEV_WUP_vIncSend_CIndicateClientAppState(u8ApplicationModeAP, u8ResetClassificationAP);

		if (OSAL_s32SemaphoreOpen(
			DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME,
			&hSemaphore) == OSAL_ERROR)
				return OSAL_u32ErrorCode();

		if (OSAL_s32SemaphoreWait(
			hSemaphore,
			DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_OK) {

			u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
	
			if (u32OsalErrorCode == OSAL_E_NOERROR) {
				prLatestResetReason->u8ResetReasonSCC = prGlobalData->u8ResetReasonSCC;
				DEV_WUP_vDataUnlock(hSemDataAccess);
			}
		} else {
			u32OsalErrorCode = OSAL_u32ErrorCode();
		}

		if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32GetStarttype() => OSAL_s32SemaphoreClose() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function returns the mode of the application processor.
*
*******************************************************************************/
static tU32 DEV_WUP_u32GetApplicationMode(tPU8 pu8ApplicationModeAP, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	*pu8ApplicationModeAP = prGlobalData->u8ApplicationModeAP;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function returns the state of the application processor supervision error.
*
*******************************************************************************/
static tU32 DEV_WUP_u32GetAPSupervisionError(tPU8 pu8APSupervisionError, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	*pu8APSupervisionError = prGlobalData->u8APSupervisionError;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function simulates the reception of an INC message by injecting it via 
* TTFIS.
*
*******************************************************************************/
static tVoid DEV_WUP_vSimulateIncMsgReceived(const tU8 * const pu8RcvBuffer, tU32 u32MsgLen)
{
	(tVoid) OSAL_pvMemoryCopy(
			g_rModuleData.au8IncRcvBuffer,
			pu8RcvBuffer,
			u32MsgLen);

	(tVoid)DEV_WUP_bEvaluateReceivedIncMsg(u32MsgLen);
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_REJECT.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RReject(tVoid)
{
	tU8 u8RejectReason  = g_rModuleData.au8IncRcvBuffer[1];
	tU8 u8RejectedMsgId = g_rModuleData.au8IncRcvBuffer[2];

	(tVoid)u8RejectReason; // Actually unused.

	if (u8RejectedMsgId == DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_CTRL_RESET_EXECUTION) {

		if (OSAL_s32SemaphorePost(g_rModuleData.hSemCtrlResetMsgReceived) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u8HandleIncMsg_RReject() => OSAL_s32SemaphorePost() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	return DEV_WUP_C_U8_REJECT_OK;  // Reject messages are never rejected
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_CTRL_RESET_EXECUTION.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RCtrlResetExecution(tVoid)
{
	if (OSAL_s32SemaphorePost(g_rModuleData.hSemCtrlResetMsgReceived) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RCtrlResetExecution() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_PROC_RESET_REQUEST.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RProcResetRequest(tVoid)
{
	tU8 u8ProcId = g_rModuleData.au8IncRcvBuffer[1];

	if ((u8ProcId != DEV_WUP_C_U8_ENTIRE_SYSTEM)                          &&
	    (u8ProcId != DEV_WUP_C_U8_ENTIRE_SYSTEM_WITH_POWER_DISCONNECTION) &&
	    (u8ProcId != DEV_WUP_C_U8_SYSTEM_COMMUNICATION_CONTROLLER)        &&
	    (u8ProcId != DEV_WUP_C_U8_BLUETOOTH_PROCESSOR)                    &&
	    (u8ProcId != DEV_WUP_C_U8_WIRELESS_LAN_PROCESSOR)                 &&
	    (u8ProcId != DEV_WUP_C_U8_GNSS_PROCESSOR)                         &&
	    (u8ProcId != DEV_WUP_C_U8_XM_PROCESSOR)                           &&
	    (u8ProcId != DEV_WUP_C_U8_IPOD_PROCESSOR)                         &&
	    (u8ProcId != DEV_WUP_C_U8_ADR1_PROCESSOR)                         &&
	    (u8ProcId != DEV_WUP_C_U8_ADR2_PROCESSOR)                         &&
	    (u8ProcId != DEV_WUP_C_U8_VIDEO_PROCESSOR)                          )
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->u8LatestResetProcessor = u8ProcId;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (OSAL_s32SemaphorePost(g_rModuleData.hSemProcResetMsgReceived) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RProcResetRequest() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_STARTUP_FINISHED.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RStartupFinished(tVoid)
{
	if (OSAL_s32SemaphorePost(g_rModuleData.hSemStartupFinishedMsgReceived) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RStartupFinished() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message 
* R_EXTEND_POWER_OFF_TIMEOUT.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RExtendPowerOffTimeout(tVoid)
{
	tU16 u16TimeoutS = (tU16)((((tU16)g_rModuleData.au8IncRcvBuffer[2]) << 8) |
				   ((tU16)g_rModuleData.au8IncRcvBuffer[1])        );

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->u16ReportedPowerOffTimeoutS = u16TimeoutS;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (OSAL_s32SemaphorePost(g_rModuleData.hSemPwrOffMsgReceived) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RExtendPowerOffTimeout() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message 
* R_AP_SUPERVISION_ERROR.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RAPSupervisionError(tVoid)
{
	tU8 u8APSupervisionError = g_rModuleData.au8IncRcvBuffer[1];

	if ((u8APSupervisionError != DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN) &&
	    (u8APSupervisionError != DEV_WUP_C_U8_AP_SUPERVISION_ERROR_THERMAL_SHUTDOWN)                      )
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->u8APSupervisionError = u8APSupervisionError;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	(tVoid)DEV_WUP_u32NotifyAPSupervisionErrorChanged();

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_WAKEUP_EVENT_ACK.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupEventAck(tVoid)
{
	tU8  u8RejectReason = DEV_WUP_C_U8_REJECT_OK;
	tU16 u16MsgHandle   = (tU16)((((tU16)g_rModuleData.au8IncRcvBuffer[2]) << 8) |
				      ((tU16)g_rModuleData.au8IncRcvBuffer[1])        );

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->u16OnOffEventMsgHandleOfLatestAckResponse = u16MsgHandle;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (OSAL_s32SemaphorePost(g_rModuleData.hSemOnOffEventAckMsgReceived) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RWakeupEventAck() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u8RejectReason;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_WAKEUP_STATE_ACK.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupStateAck(tVoid)
{
	tU8  u8RejectReason = DEV_WUP_C_U8_REJECT_OK;
	tU16 u16MsgHandle   = (tU16)((((tU16)g_rModuleData.au8IncRcvBuffer[2]) << 8) |
				      ((tU16)g_rModuleData.au8IncRcvBuffer[1])        );

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->u16OnOffStateMsgHandleOfLatestAckResponse = u16MsgHandle;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (OSAL_s32SemaphorePost(g_rModuleData.hSemOnOffStateAckMsgReceived) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RWakeupStateAck() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u8RejectReason;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_WAKEUP_STATE_VECTOR.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupStateVector(tVoid)
{
	tU8  u8RejectReason = DEV_WUP_C_U8_REJECT_OK;
	tU32 u32OnOffStates = (((tU32)g_rModuleData.au8IncRcvBuffer[1]      ) |
			       ((tU32)g_rModuleData.au8IncRcvBuffer[2] << 8 ) |
			       ((tU32)g_rModuleData.au8IncRcvBuffer[3] << 16) |
			       ((tU32)g_rModuleData.au8IncRcvBuffer[4] << 24)  );

	if (u32OnOffStates >= (0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_MAX_VALUE_PLUS_ONE))
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 = u32OnOffStates;
	g_rModuleData.prGlobalData->rOnOffStates.u16MsgHandle     = 0;

	g_rModuleData.prGlobalData->u16OnOffStateMsgHandleOfLatestAckResponse = 0;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	return u8RejectReason;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_WAKEUP_STATE.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupState(tVoid)
{
	tU8   u8OnOffState     = g_rModuleData.au8IncRcvBuffer[1];
	tU8   u8IsActive       = g_rModuleData.au8IncRcvBuffer[2];
	tU16 u16MsgHandle      = (tU16)((((tU16)g_rModuleData.au8IncRcvBuffer[4]) << 8) |
					 ((tU16)g_rModuleData.au8IncRcvBuffer[3])        );
	tU32  u32Bitmask       = ((tU32)0x00000001) << u8OnOffState;

	tBool bStateChanged    = FALSE;
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;

	if (u8OnOffState >= DEV_WUP_C_U8_ONOFF_STATE_MAX_VALUE_PLUS_ONE)
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if ((u8IsActive != 0) && (u8IsActive != 1))
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	if (1 == u8IsActive) {
		if ((g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 & u32Bitmask) == 0) {
			g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 |= u32Bitmask;
			bStateChanged  = TRUE;
		}
	} else {
		if ((g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 & u32Bitmask) != 0) {
			g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 &= ~u32Bitmask;
			bStateChanged  = TRUE;
		}
	}

	g_rModuleData.prGlobalData->rOnOffStates.u16MsgHandle = u16MsgHandle;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (TRUE == bStateChanged) {
		u32OsalErrorCode = DEV_WUP_u32NotifyOnOffReasonChanged(DEV_WUP_C_U8_ONOFF_REASON_TYPE_STATE);
		if (u32OsalErrorCode != OSAL_E_NOERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u8HandleIncMsg_RWakeupState() => DEV_WUP_u32NotifyOnOffReasonChanged() failed with OSAL error code = '%s'",
				OSAL_coszErrorText(u32OsalErrorCode));
	}

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_WAKEUP_EVENT.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupEvent(tVoid)
{
	tU8  u8OnOffEvent     = g_rModuleData.au8IncRcvBuffer[1];
	tU16 u16MsgHandle     = (tU16)((((tU16)g_rModuleData.au8IncRcvBuffer[3]) << 8) |
					((tU16)g_rModuleData.au8IncRcvBuffer[2])        );
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	tU16 u16LatestAcknowledgedOnOffEventMsgHandle = 0;

	if ((u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_POWER_ON_RESET)        &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_CSD_HOME_PRESSED)      &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_DRIVERS_DOOR_OPENED)   &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_DEBUG_WAKEUP)          &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_RTC_WAKEUP)            &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_ON_TIPPER_PRESS)       &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_IGNITION_PIN)          &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_CD_INSERT_DETECTED)    &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_CD_EJECT_DETECTED)     &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_MOST_ACTIVE_DETECTED)  &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_MOST_RBD_DETECTED)     &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_MOST_ECL_STG_DETECTED) &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_MOST_UNDEF_DETECTED)   &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_UART)                  &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_BLUETOOTH)             &&
	    (u8OnOffEvent != DEV_WUP_C_U8_ONOFF_EVENT_NO_WAKEUP_EVENT)         )
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	u16LatestAcknowledgedOnOffEventMsgHandle = g_rModuleData.prGlobalData->rPramLocal.u16LatestAcknowledgedOnOffEventMsgHandle;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	if ((u16MsgHandle > g_rModuleData.u16LatestReceivedOnOffEventMsgHandle) &&
	    (u16MsgHandle > u16LatestAcknowledgedOnOffEventMsgHandle)             )
	{
		g_rModuleData.u8LatestReceivedOnOffEvent           = u8OnOffEvent;
		g_rModuleData.u16LatestReceivedOnOffEventMsgHandle = u16MsgHandle;
	
		DEV_WUP_vStoreOnOffEvent(u8OnOffEvent, u16MsgHandle);

		u32OsalErrorCode = DEV_WUP_u32NotifyOnOffReasonChanged(DEV_WUP_C_U8_ONOFF_REASON_TYPE_EVENT);
		if (u32OsalErrorCode != OSAL_E_NOERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u8HandleIncMsg_RWakeupEvent() => DEV_WUP_u32NotifyOnOffReasonChanged() failed with OSAL error code = '%s'",
				OSAL_coszErrorText(u32OsalErrorCode));
	} else {
		DEV_WUP_vTraceDroppedWakeupEvent(u8OnOffEvent, u16MsgHandle);
	}

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_WAKEUP_REASON.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RWakeupReason(tVoid)
{
	tU8 u8WupReason = g_rModuleData.au8IncRcvBuffer[1];

	if ((u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_UNKNOWN)             &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_POWER_ON_RESET)      &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_ON_TIPPER)           &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_CAN)                 &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_MOST)                &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_IGN_PIN)             &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_TEL_MUTE)            &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_DEBUG_WAKEUP)        &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_RTC_WAKEUP)          &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_CD_INSERT_DETECTED)  &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_CD_EJECT_DETECTED)   &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_S_CONTACT_WAKEUP)    &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_CELLNETWORK_WAKEUP)  &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_CAN2_WAKEUP)         &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_CAN3_WAKEUP)         &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_CAN4_WAKEUP)         &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_ODOMETER_WAKEUP)     &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_LIN_WAKEUP)          &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_EXTERNAL_PIN_WAKEUP) &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_UART)                &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_UART2)               &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_USB)                 &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_BLUETOOTH)           &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_ACCELERATOR_SENSOR)  &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_EXTERNAL_GPIO2)      &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_EXTERNAL_GPIO3)      &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_EXTERNAL_GPIO4)      &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_EXTERNAL_GPIO5)      &&
	    (u8WupReason != DEV_WUP_C_U8_WAKEUP_REASON_ILLUMINATION)          )
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->u8WupReason         = u8WupReason;
	g_rModuleData.prGlobalData->bRWupReasonReceived = TRUE;

	if (OSAL_s32SemaphorePost(g_rModuleData.hSemWakeupReasonMsgReceived) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RWakeupReason() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_SHUTDOWN_IN_PROGRESS.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RShutdownInProgress(tVoid)
{
	tBool bSystemShutdownSuccess = FALSE;

	bSystemShutdownSuccess = DEV_WUP_bExecuteSystemShutdown();

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->bSystemShutdownSuccess = bSystemShutdownSuccess;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (OSAL_s32SemaphorePost(g_rModuleData.hSemShutdownMsgReceived) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RShutdownInProgress() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_SET_WAKEUP_CONFIG.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RSetWakeupConfig(tVoid)
{
	tU32 u32WakeupReasonsMask = (((tU32)g_rModuleData.au8IncRcvBuffer[1] <<  0) |
				     ((tU32)g_rModuleData.au8IncRcvBuffer[2] <<  8) |
				     ((tU32)g_rModuleData.au8IncRcvBuffer[3] << 16) |
				     ((tU32)g_rModuleData.au8IncRcvBuffer[4] << 24)  );

	if (u32WakeupReasonsMask > (DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ON_TIPPER           |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CAN                 |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_MOST                |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_IGN_PIN             |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_TEL_MUTE            |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_DEBUG_WAKEUP        |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_RTC_WAKEUP          |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CD_INSERT_DETECTED  |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CD_EJECT_DETECTED   |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_S_CONTACT_WAKEUP    |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CELLNETWORK_WAKEUP  |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CAN2_WAKEUP         |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CAN3_WAKEUP         |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CAN4_WAKEUP         |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ODOMETER_WAKEUP     |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_LIN_WAKEUP          |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_UART                |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_UART2               |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_USB                 |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_BLUETOOTH           |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ACCELERATOR_SENSOR  |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_GPIO2      |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_GPIO3      |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_GPIO4      |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_GPIO5      |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ILLUMINATION         ))
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->u32AcknowledgedWakeupReasonsMask = u32WakeupReasonsMask;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (OSAL_s32SemaphorePost(g_rModuleData.hSemSetWakeupConfigMsgReceived) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RSetWakeupConfig() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_REQ_CLIENT_APP_STATE.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RReqClientAppState(tVoid)
{
	tU8  u8RejectReason             = DEV_WUP_C_U8_REJECT_OK;
	tU8  u8RequestedApplicationMode = g_rModuleData.au8IncRcvBuffer[1];
	tU8  u8ApplicationModeAP        = g_rModuleData.prGlobalData->u8ApplicationModeAP;
	tU8  u8ResetClassificationAP    = g_rModuleData.prGlobalData->rPramLocal.u8ResetClassificationAP;

	if (DEV_WUP_C_U8_APPLICATION_MODE_NORMAL == u8RequestedApplicationMode) {
		if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
			return DEV_WUP_C_U8_REJECT_NO_REASON;

		DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

		DEV_WUP_vIncSend_CIndicateClientAppState(u8ApplicationModeAP, u8ResetClassificationAP);
	} else if ((DEV_WUP_C_U8_APPLICATION_MODE_DOWNLOAD    == u8RequestedApplicationMode) ||
		   (DEV_WUP_C_U8_APPLICATION_MODE_TESTMANAGER == u8RequestedApplicationMode)   ) {
		// Switch to DOWNLOAD or TESTMANAGER application mode ...
		// Actually not yet implemented, so reject with DEV_WUP_C_U8_REJECT_NO_REASON.
		u8RejectReason = DEV_WUP_C_U8_REJECT_NO_REASON;
	} else {
		u8RejectReason = DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;
	}

	return u8RejectReason;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_STARTUP_INFO.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RStartupInfo(tVoid)
{
	tU8  u8Starttype             = g_rModuleData.au8IncRcvBuffer[1];
	tU8  u8DisconnectInfo        = g_rModuleData.au8IncRcvBuffer[2];
	tU8  u8ApplicationModeAP     = g_rModuleData.prGlobalData->u8ApplicationModeAP;
	tU8  u8ResetClassificationAP = g_rModuleData.prGlobalData->rPramLocal.u8ResetClassificationAP;

	(tVoid)u8DisconnectInfo; // Actually unused.

	if ((DEV_WUP_C_U8_STARTTYPE_COLDSTART   == u8Starttype) ||
	    (DEV_WUP_C_U8_STARTTYPE_RESTART_VCC == u8Starttype) ||
	    (DEV_WUP_C_U8_STARTTYPE_WARMSTART   == u8Starttype) ||
	    (DEV_WUP_C_U8_STARTTYPE_RESTART_CPU == u8Starttype)   ) {

		if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
			return DEV_WUP_C_U8_REJECT_NO_REASON;

		g_rModuleData.prGlobalData->u8Starttype           = u8Starttype;
		g_rModuleData.prGlobalData->bRStartupInfoReceived = TRUE;

		DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

		if (OSAL_s32SemaphorePost(g_rModuleData.hSemStartupMsgReceived) == OSAL_ERROR) {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u8HandleIncMsg_RStartupInfo() => OSAL_s32SemaphorePost() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
			return DEV_WUP_C_U8_REJECT_NO_REASON;
		}

		if (DEV_WUP_C_U8_STARTTYPE_RESTART_VCC == u8Starttype) {
			DEV_WUP_vIncSend_CIndicateClientAppState(u8ApplicationModeAP, u8ResetClassificationAP);
			DEV_WUP_vIncSend_CGetData(DEV_WUP_C_U8_GET_DATA_MODE_ALL_DATA, 0);
		}
	} else {
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;
	}

	// HWA => Here a re-synchronization for all INC clients must be triggered
	//        be propagating the just executed restart of the SCC.
	//
	//        Which component or driver needs to be informed to propagate the 
	//        restart of the SCC to all INC clients is not yet clear.

	return DEV_WUP_C_U8_REJECT_OK;
}

/*******************************************************************************
*
* This function handles the reception of the INC message 
* R_INDICATE_CLIENT_APP_STATE.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RIndicateClientAppState(tVoid)
{
	tU8 u8RejectReason                = DEV_WUP_C_U8_REJECT_OK;
	tU8 u8ApplicationModeSCC          = g_rModuleData.au8IncRcvBuffer[1];
	tU8 u8ResetReasonSCC              = g_rModuleData.au8IncRcvBuffer[2];
	tU8 u8IncMsgCatMajorVersionServer = g_rModuleData.au8IncRcvBuffer[3];
	tU8 u8IncMsgCatMinorVersionServer = g_rModuleData.au8IncRcvBuffer[4];
	tU8 u8Result                      = g_rModuleData.au8IncRcvBuffer[5];

	if ((DEV_WUP_C_U8_MSG_CAT_VERSION_CHECK_RESULT_OK       != u8Result) &&
	    (DEV_WUP_C_U8_MSG_CAT_VERSION_CHECK_RESULT_MISMATCH != u8Result)   )
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if ((DEV_WUP_C_U8_APPLICATION_MODE_NORMAL      != u8ApplicationModeSCC) &&
	    (DEV_WUP_C_U8_APPLICATION_MODE_DOWNLOAD    != u8ApplicationModeSCC) &&
	    (DEV_WUP_C_U8_APPLICATION_MODE_TESTMANAGER != u8ApplicationModeSCC)   )
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if ((DEV_WUP_C_U8_RESET_REASON_HW_WATCHDOG    != u8ResetReasonSCC) &&
	    (DEV_WUP_C_U8_RESET_REASON_POR            != u8ResetReasonSCC) &&
	    (DEV_WUP_C_U8_RESET_REASON_COLDSTART      != u8ResetReasonSCC) &&
	    (DEV_WUP_C_U8_RESET_REASON_APPMODE_CHANGE != u8ResetReasonSCC) &&
	    (DEV_WUP_C_U8_RESET_REASON_DURING_LPW     != u8ResetReasonSCC) &&
	    (DEV_WUP_C_U8_RESET_REASON_PLL_OSZ        != u8ResetReasonSCC) &&
	    (DEV_WUP_C_U8_RESET_REASON_SW             != u8ResetReasonSCC) &&
	    (DEV_WUP_C_U8_RESET_REASON_WARMSTART      != u8ResetReasonSCC) &&
	    (DEV_WUP_C_U8_RESET_REASON_USER_RESET     != u8ResetReasonSCC)   )
		return DEV_WUP_C_U8_REJECT_INVALID_PARAMETER;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return DEV_WUP_C_U8_REJECT_NO_REASON;

	g_rModuleData.prGlobalData->bRIndicateClientAppStateReceived = TRUE;

	g_rModuleData.u8IncMsgCatMajorVersionServer = u8IncMsgCatMajorVersionServer;
	g_rModuleData.u8IncMsgCatMinorVersionServer = u8IncMsgCatMinorVersionServer;

	if (DEV_WUP_C_U8_MSG_CAT_VERSION_CHECK_RESULT_OK == u8Result)
		g_rModuleData.bIncMsgCatVersionMatches = TRUE;
	else
		g_rModuleData.bIncMsgCatVersionMatches = FALSE;

	g_rModuleData.prGlobalData->u8ApplicationModeSCC = u8ApplicationModeSCC;

	g_rModuleData.prGlobalData->u8ResetReasonSCC = u8ResetReasonSCC;

	if (OSAL_s32SemaphorePost(g_rModuleData.hSemAppStateMsgReceived) == OSAL_ERROR) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u8HandleIncMsg_RIndicateClientAppState() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		u8RejectReason = DEV_WUP_C_U8_REJECT_NO_REASON;
	}

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	return u8RejectReason;
}

/*******************************************************************************
*
* This function handles the reception of the INC message R_REQ_CLIENT_BOOT_MODE.
*
*******************************************************************************/
static tU8 DEV_WUP_u8HandleIncMsg_RReqClientBootMode(tVoid)
{
	tU8 u8RejectReason   = DEV_WUP_C_U8_REJECT_OK;
	tU8 u8BootMode       = g_rModuleData.au8IncRcvBuffer[1];

	(tVoid)u8BootMode; // Actually unused.

	return u8RejectReason;
}

/*******************************************************************************
*
* This function checks of an INC message is tried to be send to the system
* communication controller from the devices creator context or from another
* context. Depending on the context the INC message is either immediately send
* or transfered to the creator context to be send from there.
*
*******************************************************************************/
static tVoid DEV_WUP_vSendIncMsg(const tU8 * const pu8IncSndBuffer, tU32 u32MessageLength)
{
	if (g_rModuleData.u32CreatorContextMagic == DEV_WUP_C_U32_CREATOR_CONTEXT_MAGIC)
		DEV_WUP_vSendIncMsgFromCreatorContext(pu8IncSndBuffer, u32MessageLength);
	else
		DEV_WUP_vTransferIncMsgToCreatorContext(pu8IncSndBuffer, u32MessageLength);
}

/*******************************************************************************
*
* This function sends an INC message to the system communication controller.
*
*******************************************************************************/
static tVoid DEV_WUP_vSendIncMsgFromCreatorContext(const tU8 * const pu8IncSndBuffer, tU32 u32MessageLength)
{
	DEV_WUP_vTraceIncMsg(
		DEV_WUP_C_U8_INC_MSG_SEND,
		pu8IncSndBuffer,
		u32MessageLength);

	if (g_rModuleData.bSccIncCommunicationEstablished == TRUE) {
		if (dgram_send(
			g_rModuleData.pDatagramSocketDescriptor,
			(tVoid*)pu8IncSndBuffer,
			u32MessageLength) == -1)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vSendIncMsgFromCreatorContext() => Call of dgram_send() failed due to errno = %s",
				strerror(errno));
	}
}

/*******************************************************************************
*
* This function transfers to content of an INC message which shall be send to
* the system communication controller to the devices creator context to be send
* from there.
*
*******************************************************************************/
static tVoid DEV_WUP_vTransferIncMsgToCreatorContext(const tU8 * const pu8IncSndBuffer, tU32 u32MessageLength)
{
	OSAL_tMQueueHandle hMsgQueueOsalThread = OSAL_C_INVALID_HANDLE;

	tU8 au8MsqQueueBuffer[DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN];

	if (OSAL_s32MessageQueueOpen(
		DEV_WUP_C_STRING_OSAL_MSG_QUEUE_NAME,
		OSAL_EN_READWRITE, 
		&hMsgQueueOsalThread) == OSAL_ERROR) {
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vTransferIncMsgToCreatorContext() => OSAL_s32MessageQueueOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	au8MsqQueueBuffer[0] = DEV_WUP_C_U8_OSAL_MSG_SEND_INC_MSG;
	au8MsqQueueBuffer[1] = (tU8)(((u32MessageLength) & 0x000000FF) >>  0);
	au8MsqQueueBuffer[2] = (tU8)(((u32MessageLength) & 0x0000FF00) >>  8);
	au8MsqQueueBuffer[3] = (tU8)(((u32MessageLength) & 0x00FF0000) >> 16);
	au8MsqQueueBuffer[4] = (tU8)(((u32MessageLength) & 0xFF000000) >> 24);

	(tVoid) OSAL_pvMemoryCopy(
			&au8MsqQueueBuffer[5],
			pu8IncSndBuffer,
			u32MessageLength);

	if (OSAL_s32MessageQueuePost(
		hMsgQueueOsalThread,
		au8MsqQueueBuffer,
		DEV_WUP_C_U32_OSAL_MSG_QUEUE_MSG_LEN,
		OSAL_C_U32_MQUEUE_PRIORITY_LOWEST) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vTransferIncMsgToCreatorContext() => OSAL_s32MessageQueuePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_out:

	if (OSAL_s32MessageQueueClose(hMsgQueueOsalThread) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vTransferIncMsgToCreatorContext() => OSAL_s32MessageQueueClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
}

/*******************************************************************************
*
* This function sends an INC message to the system communication controller
* which has just been transfered from another context to the devices creator
* context.
*
*******************************************************************************/
static tVoid DEV_WUP_vSendIncMsgTransferedFromOtherContext(const tU8 * const pau8MsqQueueBuffer)
{
	tU32 u32MessageLength = (((tU32)(*(pau8MsqQueueBuffer + 1)) <<  0) |
				 ((tU32)(*(pau8MsqQueueBuffer + 2)) <<  8) |
				 ((tU32)(*(pau8MsqQueueBuffer + 3)) << 16) |
				 ((tU32)(*(pau8MsqQueueBuffer + 4)) << 24)  );

	DEV_WUP_vSendIncMsg(pau8MsqQueueBuffer + 5, u32MessageLength);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_GET_DATA.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CGetData(tU8 u8DataMode, tU8 u8MsgId)
{
	tU8 au8IncSndBuffer[3];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_C_GET_DATA;
	au8IncSndBuffer[1] = u8DataMode;
	au8IncSndBuffer[2] = u8MsgId;

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 3);
}

/*******************************************************************************
*
* This function assembles and sends the INC message R_REJECT.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_RReject(tU8 u8RejectReason, tU8 u8MsgId)
{
	tU8 au8IncSndBuffer[3];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_R_REJECT;
	au8IncSndBuffer[1] = u8RejectReason;
	au8IncSndBuffer[2] = u8MsgId;

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 3);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_PROC_RESET_REQUEST.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CProcResetRequest(tU8 u8ProcId, tU16 u16ResetDurationMs)
{
	tU8 au8IncSndBuffer[4];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_PROC_RESET_REQUEST;
	au8IncSndBuffer[1] = u8ProcId;
	au8IncSndBuffer[2] = (tU8)((u16ResetDurationMs & 0x00FF) >> 0);
	au8IncSndBuffer[3] = (tU8)((u16ResetDurationMs & 0xFF00) >> 8);

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 4);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_CTRL_RESET_EXECUTION.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CCtrlResetExecution(tU8 u8ResetControlBitmask)
{
	tU8 au8IncSndBuffer[2];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_CTRL_RESET_EXECUTION;
	au8IncSndBuffer[1] = u8ResetControlBitmask;

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 2);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_CTRL_RESET_EXECUTION.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CExtendPowerOffTimeout(tU16 u16TimeoutS)
{
	tU8 au8IncSndBuffer[3];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_EXTEND_POWER_OFF_TIMEOUT;
	au8IncSndBuffer[1] = (tU8)((u16TimeoutS & 0x00FF) >> 0);
	au8IncSndBuffer[2] = (tU8)((u16TimeoutS & 0xFF00) >> 8);

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 3);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_STARTUP_FINISHED.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CStartupFinished(tVoid)
{
	tU8 au8IncSndBuffer[1];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_STARTUP_FINISHED;

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 1);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_WAKEUP_EVENT_ACK.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CWakeupEventAck(tU8 u8Event, tU16 u16MsgHandle)
{
	tU8 au8IncSndBuffer[4];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_WAKEUP_EVENT_ACK;
	au8IncSndBuffer[1] = u8Event;
	au8IncSndBuffer[2] = (tU8)((u16MsgHandle & 0x00FF) >> 0);
	au8IncSndBuffer[3] = (tU8)((u16MsgHandle & 0xFF00) >> 8);

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 4);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_WAKEUP_STATE_ACK.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CWakeupStateAck(tU16 u16MsgHandle)
{
	tU8 au8IncSndBuffer[3];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_WAKEUP_STATE_ACK;
	au8IncSndBuffer[1] = (tU8)((u16MsgHandle & 0x00FF) >> 0);
	au8IncSndBuffer[2] = (tU8)((u16MsgHandle & 0xFF00) >> 8);

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 3);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_SHUTDOWN_IN_PROGRESS.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CShutdownInProgress(tVoid)
{
	tU8 au8IncSndBuffer[1];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_SHUTDOWN_IN_PROGRESS;

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 1);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_SET_WAKEUP_CONFIG.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CSetWakeupConfig(tU32 u32WakeupConfig)
{
	tU8 au8IncSndBuffer[5];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_SET_WAKEUP_CONFIG;
	au8IncSndBuffer[1] = (tU8)(((u32WakeupConfig) & 0x000000FF) >>  0);  
	au8IncSndBuffer[2] = (tU8)(((u32WakeupConfig) & 0x0000FF00) >>  8);  
	au8IncSndBuffer[3] = (tU8)(((u32WakeupConfig) & 0x00FF0000) >> 16);  
	au8IncSndBuffer[4] = (tU8)(((u32WakeupConfig) & 0xFF000000) >> 24);  

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 5);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_INDICATE_CLIENT_APP_STATE.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CIndicateClientAppState(tU8 u8ApplicationModeAP, tU8 u8ResetClassificationAP)
{
	tU8 au8IncSndBuffer[5];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_INDICATE_CLIENT_APP_STATE;
	au8IncSndBuffer[1] = u8ApplicationModeAP;
	au8IncSndBuffer[2] = u8ResetClassificationAP;
	au8IncSndBuffer[3] = DEV_WUP_C_U8_INC_MSG_CAT_MAJOR_VERSION_NUMBER;
	au8IncSndBuffer[4] = DEV_WUP_C_U8_INC_MSG_CAT_MINOR_VERSION_NUMBER;

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 5);
}

/*******************************************************************************
*
* This function assembles and sends the INC message C_REQ_CLIENT_BOOT_MODE.
*
*******************************************************************************/
static tVoid DEV_WUP_vIncSend_CReqClientBootMode(tU8 u8BootMode)
{
	tU8 au8IncSndBuffer[19];

	au8IncSndBuffer[0]  = DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_REQ_CLIENT_BOOT_MODE;
	au8IncSndBuffer[1]  = 0xAC;
	au8IncSndBuffer[2]  = 0xAC;
	au8IncSndBuffer[3]  = 0xAC;
	au8IncSndBuffer[4]  = 0xAC;
	au8IncSndBuffer[5]  = 0xCA;
	au8IncSndBuffer[6]  = 0xCA;
	au8IncSndBuffer[7]  = 0xCA;
	au8IncSndBuffer[8]  = 0xCA;
	au8IncSndBuffer[9]  = u8BootMode;
	au8IncSndBuffer[10] = 0x0F;
	au8IncSndBuffer[11] = 0x0F;
	au8IncSndBuffer[12] = 0x0F;
	au8IncSndBuffer[13] = 0x0F;
	au8IncSndBuffer[14] = 0xF0;
	au8IncSndBuffer[15] = 0xF0;
	au8IncSndBuffer[16] = 0xF0;
	au8IncSndBuffer[17] = 0xF0;
	au8IncSndBuffer[18] = (tU8)(~u8BootMode);

	DEV_WUP_vSendIncMsg(au8IncSndBuffer, 19);
}

/*******************************************************************************
*
* This functions registers a client to be notified about changes of
* switch-on/off reasons.
*
*******************************************************************************/
static tU32 DEV_WUP_u32RegisterOnOffReasonChanged(DEV_WUP_trOnOffReasonChangedRegistration* prOnOffReasonChangedRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32              u32OsalErrorCode        = OSAL_E_NOERROR;
	OSAL_tEventHandle hEventClient            = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask   rEventMask              = 0;
	tBool             bRegistrationEntryAdded = FALSE;

	tU32 u32Index = 0;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if ((prOnOffReasonChangedRegistration->u8NotificationMode < DEV_WUP_C_U8_NOTIFY_STATES_ONLY)                        ||
	    (prOnOffReasonChangedRegistration->u8NotificationMode > DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITHOUT_PAST_ONES)  ) {

		u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (prOnOffReasonChangedRegistration->bIsSystemMaster == TRUE) {
		for (u32Index = 0;
		     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
		     u32Index++)
			if ((prGlobalData->arClientSpecificData[u32Index].u32ClientId     != DEV_WUP_C_U32_CLIENT_ID_INVALID) &&
			    (prGlobalData->arClientSpecificData[u32Index].bIsSystemMaster == TRUE)                              ) {
				u32OsalErrorCode = OSAL_E_BUSY;
				goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
			}
	} else if (prOnOffReasonChangedRegistration->bIsSystemMaster != FALSE) {
		u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prOnOffReasonChangedRegistration->u32ClientId == prGlobalData->arClientSpecificData[u32Index].u32ClientId) {

			if (prGlobalData->arClientSpecificData[u32Index].u8OnOffReasonChangedNotificationMode != DEV_WUP_C_U8_NOTIFY_NONE) {
				u32OsalErrorCode = OSAL_E_ALREADYEXISTS;
				goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
			}

			prGlobalData->arClientSpecificData[u32Index].u8OnOffReasonChangedNotificationMode = prOnOffReasonChangedRegistration->u8NotificationMode;
			prGlobalData->arClientSpecificData[u32Index].bIsSystemMaster                      = prOnOffReasonChangedRegistration->bIsSystemMaster;

			if ((DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITHOUT_PAST_ONES == prGlobalData->arClientSpecificData[u32Index].u8OnOffReasonChangedNotificationMode) ||
			    (DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITHOUT_PAST_ONES       == prGlobalData->arClientSpecificData[u32Index].u8OnOffReasonChangedNotificationMode)   ) {
				tU32 u32IndexOnOffEventTail = prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail;

				prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead = u32IndexOnOffEventTail;
				prGlobalData->arClientSpecificData[u32Index].u16LatestAckOnOffEventMsgHandle = prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32IndexOnOffEventTail].u16MsgHandle;
			} else {
				prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead = prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead;
				prGlobalData->arClientSpecificData[u32Index].u16LatestAckOnOffEventMsgHandle = 0;
			}

			prGlobalData->u8NumberOfOnOffReasonChangedRegistrations++;
			bRegistrationEntryAdded = TRUE;
			break;
		}
	}

	if (u32Index >= DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS) {
		u32OsalErrorCode = OSAL_E_DOESNOTEXIST;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventOpen(prGlobalData->arClientSpecificData[u32Index].szNotificationEventName, &hEventClient) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	// One time on/off reason changed notifications after registration => ON-OFF EVENTS
	if ((prGlobalData->rOnOffEventRingBuffer.u8NumberOfUsedElements > 0)                                                  &&
	    ((DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITH_PAST_ONES == prOnOffReasonChangedRegistration->u8NotificationMode)      ||
	     (DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES == prOnOffReasonChangedRegistration->u8NotificationMode)  )  )
		rEventMask |= DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY;

	// One time on/off reason changed notifications after registration => ON-OFF STATES
	if ((prOnOffReasonChangedRegistration->u8NotificationMode != DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITH_PAST_ONES)   &&
	    (prOnOffReasonChangedRegistration->u8NotificationMode != DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITHOUT_PAST_ONES)  )
		rEventMask |= DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY;

	if (OSAL_s32EventPost(
		hEventClient,
		rEventMask,
		OSAL_EN_EVENTMASK_OR) == OSAL_OK) {
			DEV_WUP_vTraceNotificatonEvent(
				prGlobalData->arClientSpecificData[u32Index].u32ClientId,
				prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
				rEventMask);
	} else {
		u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	if (OSAL_s32EventClose(hEventClient) == OSAL_ERROR)
		u32OsalErrorCode = OSAL_u32ErrorCode();

error_out:

	if ((u32OsalErrorCode != OSAL_E_NOERROR)                &&
	    (bRegistrationEntryAdded == TRUE)                   &&
	    (u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS)  ) { // Last check is reduntant. Remove LINT warning 'Possible access of out-of-bounds pointer'.
		prGlobalData->arClientSpecificData[u32Index].u8OnOffReasonChangedNotificationMode = DEV_WUP_C_U8_NOTIFY_NONE;
		prGlobalData->arClientSpecificData[u32Index].bIsSystemMaster                      = FALSE;
		prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead               = prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead;
		prGlobalData->arClientSpecificData[u32Index].u16LatestAckOnOffEventMsgHandle      = 0;
		prGlobalData->u8NumberOfOnOffReasonChangedRegistrations--;
	}

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions un-registers a client to be notified about changes of
* switch-on/off reasons.
*
*******************************************************************************/
static tU32 DEV_WUP_u32UnregisterOnOffReasonChanged(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;
	tU32  u32Index;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = OSAL_E_DOESNOTEXIST;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {
		if ((prGlobalData->arClientSpecificData[u32Index].u32ClientId == u32ClientId)                                        &&
		    (prGlobalData->arClientSpecificData[u32Index].u8OnOffReasonChangedNotificationMode   != DEV_WUP_C_U8_NOTIFY_NONE)  ) {
			prGlobalData->arClientSpecificData[u32Index].u8OnOffReasonChangedNotificationMode = DEV_WUP_C_U8_NOTIFY_NONE;
			prGlobalData->arClientSpecificData[u32Index].bIsSystemMaster                      = FALSE;
			prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead               = prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead;
			prGlobalData->arClientSpecificData[u32Index].u16LatestAckOnOffEventMsgHandle      = 0;

			prGlobalData->u8NumberOfOnOffReasonChangedRegistrations--;

			u32OsalErrorCode = OSAL_E_NOERROR;
			break;
		}
	}

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions registers a client to be notified about changes of
* the application processor supervision error state.
*
*******************************************************************************/
static tU32 DEV_WUP_u32RegisterAPSupervisionErrorChanged(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32              u32OsalErrorCode = OSAL_E_NOERROR;
	OSAL_tEventHandle hEventClient     = OSAL_C_INVALID_HANDLE;

	tU32 u32Index = 0;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {
		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == u32ClientId) {
			if (prGlobalData->arClientSpecificData[u32Index].bNotifyAPSupervisionError == TRUE) {
				u32OsalErrorCode = OSAL_E_ALREADYEXISTS;
				goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
			}

			prGlobalData->arClientSpecificData[u32Index].bNotifyAPSupervisionError = TRUE;
			break;
		}
	}

	if (u32Index >= DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS) {
		u32OsalErrorCode = OSAL_E_DOESNOTEXIST;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventOpen(prGlobalData->arClientSpecificData[u32Index].szNotificationEventName, &hEventClient) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (prGlobalData->u8APSupervisionError != DEV_WUP_C_U8_AP_SUPERVISION_ERROR_NONE) {
		if (OSAL_s32EventPost(
			hEventClient,
			DEV_WUP_C_U32_EVENT_MASK_AP_SUPERVISION_ERROR_CHANGED_NOTIFY,
			OSAL_EN_EVENTMASK_OR) == OSAL_OK) {
				DEV_WUP_vTraceNotificatonEvent(
					prGlobalData->arClientSpecificData[u32Index].u32ClientId,
					prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
					DEV_WUP_C_U32_EVENT_MASK_AP_SUPERVISION_ERROR_CHANGED_NOTIFY);
		} else {
			u32OsalErrorCode = OSAL_u32ErrorCode();
		}
	}

	if ((OSAL_s32EventClose(hEventClient) == OSAL_ERROR) && (u32OsalErrorCode == OSAL_E_NOERROR))
		u32OsalErrorCode = OSAL_u32ErrorCode();

error_out:

	if ((u32OsalErrorCode != OSAL_E_NOERROR)                &&
	    (u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS)  ) // Last check is reduntant. Remove LINT warning 'Possible access of out-of-bounds pointer'.
			prGlobalData->arClientSpecificData[u32Index].bNotifyAPSupervisionError = FALSE;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions un-registers a client to be notified about changes of
* the application processor supervision error state.
*
*******************************************************************************/
static tU32 DEV_WUP_u32UnregisterAPSupervisionErrorChanged(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;
	tU32  u32Index;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = OSAL_E_DOESNOTEXIST;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {
		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == u32ClientId) {
			prGlobalData->arClientSpecificData[u32Index].bNotifyAPSupervisionError = FALSE;
			u32OsalErrorCode = OSAL_E_NOERROR;
			break;
		}
	}

	DEV_WUP_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function stores a just from the system communication controller
* received switch-on/off event into a ring buffer. For each registered client
* the ring buffer maintains separate read and write index to be able to
* individually handle the acknowledgement and re-transmission process for each
* client. If a client doesn't retrieve its switch-on/off events until the ring
* buffer overwrites not yet accessed switch-on/off events, then these 
* switch-on/off events will get lost for the respective client.
*
*******************************************************************************/
static tVoid DEV_WUP_vStoreOnOffEvent(tU8 u8Event, tU16 u16MsgHandle)
{
	tU32 u32Index;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	// Only increase the tail index if it is NOT the very first on/off event written into the ring buffer.
	// After writing the very first on/off event the tail index shall reference exact this element and is therefore not increased.
	// For the very first on/off event the head and tail index both have a value of 0 and reference the same element.
	if (g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u8NumberOfUsedElements > 0) {
		// Increase tail index to store the on/off event at a new position.
		g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail++;

		// Tail index wrap around check.
		if (g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail >= DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS)
			g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail = 0;

		// Tail <=> Head collision check => Buffer full => Move head forward ... cycling.
		if (g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail == g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead) {
			g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead++;

			// Head index wrap around check.
			if (g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead >= DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS)
				g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventHead = 0;

			// Check clients to move on/off event read index.
			for (u32Index = 0;
			     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
			     u32Index++) {

				if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId != DEV_WUP_C_U32_CLIENT_ID_INVALID) {
					if (g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail == g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead) {
						g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead++;

						// Wrap around check.
						if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead >= DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS)
							g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead = 0;

						// TRACE DATA LOSS => HWA
					}
				}
			}
		}
	}

	g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail].u8Event      = u8Event;
	g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail].u16MsgHandle = u16MsgHandle;
	g_rModuleData.prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail].u32Timestamp = (tU32)OSAL_ClockGetElapsedTime();

	if (g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u8NumberOfUsedElements < DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS)
		g_rModuleData.prGlobalData->rOnOffEventRingBuffer.u8NumberOfUsedElements++;

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function notifies registered clients about changed switch-on/off events 
* or switch-on/off states via an OSAL event.
*
*******************************************************************************/
static tU32 DEV_WUP_u32NotifyOnOffReasonChanged(tU8 u8OnOffReasonType)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	tU32 u32Index;

	u32OsalErrorCode = DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId != DEV_WUP_C_U32_CLIENT_ID_INVALID) {
			OSAL_tEventMask rEventMask = 0;
			tU8             u8NotificationMode = g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u8OnOffReasonChangedNotificationMode;

			if (DEV_WUP_C_U8_ONOFF_REASON_TYPE_EVENT == u8OnOffReasonType) {
				if (DEV_WUP_C_U8_NOTIFY_STATES_ONLY != u8NotificationMode)
					rEventMask = DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY; 
			} else if (DEV_WUP_C_U8_ONOFF_REASON_TYPE_STATE == u8OnOffReasonType) {
				if ((u8NotificationMode != DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITHOUT_PAST_ONES) &&
				    (u8NotificationMode != DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITH_PAST_ONES)      )
					rEventMask = DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY; 
			} else {
				u32OsalErrorCode = OSAL_E_INVALIDVALUE;
				break;
			}

			if (rEventMask != 0) {
				if (OSAL_s32EventPost(
					g_rModuleData.ahClientEventHandle[u32Index],
					rEventMask,
					OSAL_EN_EVENTMASK_OR) == OSAL_OK) {
						DEV_WUP_vTraceNotificatonEvent(
							g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId,
							g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
							rEventMask);
				} else {
					u32OsalErrorCode = OSAL_u32ErrorCode();
					break;
				}
			}
		}
	}

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function notifies registered clients about changes of the application
* processor supervision error state.
*
*******************************************************************************/
static tU32 DEV_WUP_u32NotifyAPSupervisionErrorChanged(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	tU32 u32Index;

	u32OsalErrorCode = DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if ((g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId != DEV_WUP_C_U32_CLIENT_ID_INVALID) &&
		    (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].bNotifyAPSupervisionError == TRUE)                ) {
			if (OSAL_s32EventPost(
				g_rModuleData.ahClientEventHandle[u32Index],
				DEV_WUP_C_U32_EVENT_MASK_AP_SUPERVISION_ERROR_CHANGED_NOTIFY,
				OSAL_EN_EVENTMASK_OR) == OSAL_OK) {
					DEV_WUP_vTraceNotificatonEvent(
						g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId,
						g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
						DEV_WUP_C_U32_EVENT_MASK_AP_SUPERVISION_ERROR_CHANGED_NOTIFY);
			} else {
				u32OsalErrorCode = OSAL_u32ErrorCode();
				break;
			}
		}
	}

	DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32NotifyAPSupervisionErrorChanged() failed with OSAL error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function returns the client individual buffer of all not yet 
* acknowlegded switch-on/off events.
*
*******************************************************************************/
static tU32 DEV_WUP_u32GetOnOffEvents(DEV_WUP_trOnOffEventHistory* prOnOffEventHistory, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode         = OSAL_E_NOERROR;
	tBool bRegistrationEntryFound  = FALSE;
	tU32  u32IndexOnOffEventRead   = 0;
	tU8   u8NumberOfOnOffEvents    = 0;

	tU32 u32Index;
	tU32 u32Index2;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	for (u32Index = 0; 
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS; 
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == prOnOffEventHistory->u32ClientId) {
			bRegistrationEntryFound = TRUE;

			if (prGlobalData->rOnOffEventRingBuffer.u8NumberOfUsedElements > 0) {
				// Start copying of onoff events at actual client individual read index.
				u32IndexOnOffEventRead = prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead;

				for (u32Index2 = 0;
				     u32Index2 < DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS;
				     u32Index2++) {

					if (prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32IndexOnOffEventRead].u16MsgHandle > prGlobalData->arClientSpecificData[u32Index].u16LatestAckOnOffEventMsgHandle) {
						prOnOffEventHistory->arOnOffEvent[u8NumberOfOnOffEvents].u8Event      = prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32IndexOnOffEventRead].u8Event;
						prOnOffEventHistory->arOnOffEvent[u8NumberOfOnOffEvents].u16MsgHandle = prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32IndexOnOffEventRead].u16MsgHandle;
						prOnOffEventHistory->arOnOffEvent[u8NumberOfOnOffEvents].u32Timestamp = prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32IndexOnOffEventRead].u32Timestamp;

						u8NumberOfOnOffEvents++;

						// Clients which are NO system master get the on/off events immediately set as acknowledged, not to get them more than once.
						if (prGlobalData->arClientSpecificData[u32Index].bIsSystemMaster == FALSE) {
							prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead = u32IndexOnOffEventRead;
							prGlobalData->arClientSpecificData[u32Index].u16LatestAckOnOffEventMsgHandle = prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32IndexOnOffEventRead].u16MsgHandle;
						}
					}

					// If tail index of ring buffer is reached, then all events are copied => leave loop.
					if (u32IndexOnOffEventRead == prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail)
						break;

					// Step to next event entry by increasing the read index and check for wrap around.
					u32IndexOnOffEventRead++;

					if (u32IndexOnOffEventRead >= DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS)
						u32IndexOnOffEventRead = 0;
				}
			}

			prOnOffEventHistory->u8NumberOfOnOffEvents = u8NumberOfOnOffEvents;
			break;
		}
	}

	DEV_WUP_vDataUnlock(hSemDataAccess);

	if (FALSE == bRegistrationEntryFound)
		u32OsalErrorCode = OSAL_E_INVALIDVALUE;

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function performs the client individual acknowledgement of a 
* switch-on/off event. Each acknowledged switch-on/off event is no more 
* considered for all further accesses of the client individual list of 
* switch-on/off events (see function DEV_WUP_u32GetOnOffEvents()). The
* switch-on/off event is also acknowledged at the system communication 
* controller via an INC message.
*
*******************************************************************************/
static tU32 DEV_WUP_u32AcknowledgeOnOffEvent(DEV_WUP_trOnOffEventAcknowledge* prOnOffEventAcknowledge, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32            u32OsalErrorCode       = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore             = OSAL_C_INVALID_HANDLE;
	tU8             u8OnOffEvent           = DEV_WUP_C_U8_ONOFF_EVENT_NO_WAKEUP_EVENT;
	tU32            u32IndexOnOffEventRead = 0;

	tU32 u32Index;
	tU32 u32Index2;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	// Assume client not found.
	u32OsalErrorCode = OSAL_E_INVALIDVALUE;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {
		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == prOnOffEventAcknowledge->u32ClientId) {
			// Client found.
			u32OsalErrorCode = OSAL_E_NOERROR;
			if (prGlobalData->arClientSpecificData[u32Index].bIsSystemMaster == FALSE) {
				// Only the system master client is allowed to acknowledge on/off events.
				u32OsalErrorCode = OSAL_E_NOPERMISSION;
				break;
			}
			if (prGlobalData->rOnOffEventRingBuffer.u8NumberOfUsedElements > 0) {
				// Start event message handle lookup at the actual client individual read index.
				u32IndexOnOffEventRead = prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead;

				for (u32Index2 = 0;
				     u32Index2 < DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS;
				     u32Index2++) {
					// Check if we have found the event which relates to the passed event message handle.
					if (prOnOffEventAcknowledge->u16OnOffEventMsgHandle == prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32IndexOnOffEventRead].u16MsgHandle) {
						// Take over locally modified u32IndexOnOffEventRead into global client specific structure.
						prGlobalData->arClientSpecificData[u32Index].u32IndexOnOffEventRead = u32IndexOnOffEventRead;

						// Update the value of the message handle for the latest acknowledged on/off event.
						prGlobalData->arClientSpecificData[u32Index].u16LatestAckOnOffEventMsgHandle = prOnOffEventAcknowledge->u16OnOffEventMsgHandle;

						// Get the to be acknowledged wake-up event.
						u8OnOffEvent = prGlobalData->rOnOffEventRingBuffer.arOnOffEvent[u32IndexOnOffEventRead].u8Event;
						break;
					}

					// If tail index of ring buffer is reached, then all events are checked => leave loop.
					if (u32IndexOnOffEventRead == prGlobalData->rOnOffEventRingBuffer.u32IndexOnOffEventTail) {
						// No matching event message handle found (is an error) and no wake-up event can be acknowledged.
						u32OsalErrorCode = OSAL_E_INVALIDVALUE;
						break;
					}

					// Step to next event entry by increasing the read index and check for wrap around.
					u32IndexOnOffEventRead++;

					if (u32IndexOnOffEventRead >= DEV_WUP_CONF_C_U8_MAX_NUM_OF_BUFFERED_ONOFF_EVENTS)
						u32IndexOnOffEventRead = 0;
				}
			} else {
				u32OsalErrorCode = OSAL_E_INVALIDVALUE;
			}
			break;
		}
	}

	prGlobalData->u16OnOffEventMsgHandleOfLatestAckResponse = 0;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	if (u32OsalErrorCode == OSAL_E_NOERROR) {

		u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
	
		if (u32OsalErrorCode != OSAL_E_NOERROR)
			return u32OsalErrorCode;

		prGlobalData->rPramMirror.u16LatestAcknowledgedOnOffEventMsgHandle = prOnOffEventAcknowledge->u16OnOffEventMsgHandle;

		DEV_WUP_vDataUnlock(hSemDataAccess);

		DEV_WUP_vWritePersistentRamData(
			prGlobalData,
			hSemDataAccess);
	
		DEV_WUP_vIncSend_CWakeupEventAck(u8OnOffEvent, prOnOffEventAcknowledge->u16OnOffEventMsgHandle);

		if (OSAL_s32SemaphoreOpen(
			DEV_WUP_C_STRING_SEM_ONOFF_EVENT_ACK_RECEIVED_NAME,
			&hSemaphore) == OSAL_ERROR)
				return OSAL_u32ErrorCode();

		if (OSAL_s32SemaphoreWait(
			hSemaphore,
			DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_OK) {

			u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

			if (u32OsalErrorCode == OSAL_E_NOERROR) {
				if (prGlobalData->u16OnOffEventMsgHandleOfLatestAckResponse < prOnOffEventAcknowledge->u16OnOffEventMsgHandle)
					u32OsalErrorCode = OSAL_E_UNKNOWN;
				DEV_WUP_vDataUnlock(hSemDataAccess);
			}
		} else {
			u32OsalErrorCode = OSAL_u32ErrorCode();
		}

		if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32AcknowledgeOnOffEvent() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function performs the acknowledgement of a switch-on/off state by
* sending the respective INC message to the system communication controller.
*
*******************************************************************************/
static tU32 DEV_WUP_u32AcknowledgeOnOffState(DEV_WUP_trOnOffStateAcknowledge* prOnOffStateAcknowledge, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32            u32OsalErrorCode = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore       = OSAL_C_INVALID_HANDLE;

	tU32 u32Index;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	// Assume client not found.
	u32OsalErrorCode = OSAL_E_INVALIDVALUE;

	for (u32Index = 0;
	     u32Index < DEV_WUP_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {
		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == prOnOffStateAcknowledge->u32ClientId) {
			// Client found.
			u32OsalErrorCode = OSAL_E_NOERROR;
			if (prGlobalData->arClientSpecificData[u32Index].bIsSystemMaster == FALSE) {
				// Only the system master client is allowed to acknowledge on/off states.
				u32OsalErrorCode = OSAL_E_NOPERMISSION;
				break;
			}
		}
	}

	prGlobalData->u16OnOffStateMsgHandleOfLatestAckResponse = 0;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	DEV_WUP_vIncSend_CWakeupStateAck(prOnOffStateAcknowledge->u16OnOffStateMsgHandle);

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_ONOFF_STATE_ACK_RECEIVED_NAME,
		&hSemaphore) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreWait(
		hSemaphore,
		DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_OK) {

		u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

		if (u32OsalErrorCode == OSAL_E_NOERROR) {
			if (prGlobalData->u16OnOffStateMsgHandleOfLatestAckResponse < prOnOffStateAcknowledge->u16OnOffStateMsgHandle)
				u32OsalErrorCode = OSAL_E_UNKNOWN;
			DEV_WUP_vDataUnlock(hSemDataAccess);
		}
	} else {
		u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32AcknowledgeOnOffState() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function performs a controlled shutdown of the application processor.
*
*******************************************************************************/
static tBool DEV_WUP_bExecuteSystemShutdown(tVoid)
{
	tBool bSuccess = TRUE;

	if (DEV_WUP_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {
		g_rModuleData.prGlobalData->rPramMirror.u16ResetReasonAP        = DEV_WUP_C_U16_RESET_REASON_POWER_OFF;
		g_rModuleData.prGlobalData->rPramMirror.u8ResetClassificationAP = DEV_WUP_C_U8_RESET_REASON_AP_INTENDED;

		DEV_WUP_vDataUnlock(g_rModuleData.hSemDataAccess);
	} else {
		bSuccess = FALSE;
	}

	DEV_WUP_vWritePersistentRamData(
		g_rModuleData.prGlobalData,
		g_rModuleData.hSemDataAccess);

	if (DEV_WUP_bEmmcRemountReadOnly() == FALSE)
		bSuccess = FALSE;

	if (DEV_WUP_bTriggerEmmcPowerOff(DEV_WUP_C_CHAR_TRIGGER_EMMC_POWER_OFF_AND_SET_CPU_RUN_INACTIVE) == FALSE)
		bSuccess = FALSE;

	DEV_WUP_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_WUP_vExecuteSystemShutdown() => Shutdown was executed and iMX expects to be switched off or reset by SCC power master ...");

	return bSuccess;
}

/*******************************************************************************
*
* This function handles the reset of a processor.
*
*******************************************************************************/
static tU32 DEV_WUP_u32ResetProcessor(DEV_WUP_trResetProcessorInfo* prResetProcessorInfo, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if ((prResetProcessorInfo->u8ResetMode != DEV_WUP_C_U8_RESET_MODE_UNLOGGED) &&
	    (prResetProcessorInfo->u8ResetMode != DEV_WUP_C_U8_RESET_MODE_LOGGED)     ) {
		u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (prResetProcessorInfo->u16ResetDurationMs > 1000) {
		u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if ((prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_APPLICATION_PROCESSOR)                  &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_SYSTEM_COMMUNICATION_CONTROLLER)        &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_ENTIRE_SYSTEM)                          &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_ENTIRE_SYSTEM_WITH_POWER_DISCONNECTION) &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_BLUETOOTH_PROCESSOR)                    &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_WIRELESS_LAN_PROCESSOR)                 &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_GNSS_PROCESSOR)                         &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_XM_PROCESSOR)                           &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_IPOD_PROCESSOR)                         &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_ADR1_PROCESSOR)                         &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_ADR2_PROCESSOR)                         &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_VIDEO_PROCESSOR)                          ) {
		u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if ((prResetProcessorInfo->u16ResetReason != DEV_WUP_C_U16_RESET_REASON_UNSPECIFIED)           &&
	    (prResetProcessorInfo->u16ResetReason != DEV_WUP_C_U16_RESET_REASON_SW_DOWNLOAD)           &&
	    (prResetProcessorInfo->u16ResetReason != DEV_WUP_C_U16_RESET_REASON_LATE_WAKEUP)           &&
	    (prResetProcessorInfo->u16ResetReason != DEV_WUP_C_U16_RESET_REASON_DIAGNOSIS)             &&
	    (prResetProcessorInfo->u16ResetReason != DEV_WUP_C_U16_RESET_REASON_SWITCH_BOOT_MODE_EMMC) &&
	    (prResetProcessorInfo->u16ResetReason != DEV_WUP_C_U16_RESET_REASON_SWITCH_BOOT_MODE_USB)    ) {
		u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (((prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_ENTIRE_SYSTEM)                          ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_ENTIRE_SYSTEM_WITH_POWER_DISCONNECTION) ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_SYSTEM_COMMUNICATION_CONTROLLER)        ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_BLUETOOTH_PROCESSOR)                    ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_WIRELESS_LAN_PROCESSOR)                 ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_GNSS_PROCESSOR)                         ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_XM_PROCESSOR)                           ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_IPOD_PROCESSOR)                         ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_ADR1_PROCESSOR)                         ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_ADR2_PROCESSOR)                         ||
	     (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_VIDEO_PROCESSOR)                          ) &&
	     (prResetProcessorInfo->u16ResetReason != DEV_WUP_C_U16_RESET_REASON_UNSPECIFIED)               &&
	     (prResetProcessorInfo->u16ResetReason != DEV_WUP_C_U16_RESET_REASON_SW_DOWNLOAD)               &&
	     (prResetProcessorInfo->u16ResetReason != DEV_WUP_C_U16_RESET_REASON_DIAGNOSIS)                   ) {
		u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_WUP_vDetermineAndHandleIntendedResetReason(
		prResetProcessorInfo,
		prGlobalData,
		hSemDataAccess);

	if (prResetProcessorInfo->u8ResetMode == DEV_WUP_C_U8_RESET_MODE_LOGGED) {
		// Do nothing as the logging feature was removed.
	}

	DEV_WUP_vTraceIOControl(
		(tS32)OSAL_C_S32_IOCTRL_WUP_RESET_PROCESSOR,
		(intptr_t)prResetProcessorInfo,
		NULL,
		OSAL_E_NOERROR);

	 // Give trace output some time to be printed before eventual application processor reset is performed.
	OSAL_s32ThreadWait(100);

	if (prResetProcessorInfo->u8Processor == DEV_WUP_C_U8_APPLICATION_PROCESSOR) {
		u32OsalErrorCode = DEV_WUP_u32ResetApplicationProcessor(
			prResetProcessorInfo,
			prGlobalData,
			hSemDataAccess);
	} else {
		u32OsalErrorCode = DEV_WUP_u32RequestProcessorResetViaSCC(
			prResetProcessorInfo,
			prGlobalData,
			hSemDataAccess);
	}

error_out:

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceIOControl(
			(tS32)OSAL_C_S32_IOCTRL_WUP_RESET_PROCESSOR,
			(intptr_t)prResetProcessorInfo,
			NULL,
			u32OsalErrorCode);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function handles a reset of the application processor.
*
*******************************************************************************/
static tU32 DEV_WUP_u32ResetApplicationProcessor(DEV_WUP_trResetProcessorInfo* prResetProcessorInfo, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	prGlobalData->rPramMirror.u16ResetReasonAP = DEV_WUP_C_U16_RESET_REASON_SELF;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	DEV_WUP_vWritePersistentRamData(
		prGlobalData,
		hSemDataAccess);

	if ((prResetProcessorInfo->u16ResetReason == DEV_WUP_C_U16_RESET_REASON_UNSPECIFIED) ||
	    (prResetProcessorInfo->u16ResetReason == DEV_WUP_C_U16_RESET_REASON_SW_DOWNLOAD) ||
	    (prResetProcessorInfo->u16ResetReason == DEV_WUP_C_U16_RESET_REASON_LATE_WAKEUP) ||
	    (prResetProcessorInfo->u16ResetReason == DEV_WUP_C_U16_RESET_REASON_DIAGNOSIS)     ) {

		// The data access semaphore is intentionally locked to block any further IO-control calls.
		(tVoid)DEV_WUP_u32DataLock(hSemDataAccess);

		(tVoid)DEV_WUP_bEmmcRemountReadOnly();

		(tVoid)DEV_WUP_bExecuteApplicationProcessorReset();

		// Wait forever until iMX self-reset happens.
		while (FOREVER) {
			OSAL_s32ThreadWait(500);

			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32ResetApplicationProcessor() => Wait for just triggerd iMX self-reset to happen ...");
		}
	}

	if ((prResetProcessorInfo->u16ResetReason == DEV_WUP_C_U16_RESET_REASON_SWITCH_BOOT_MODE_EMMC) || 
	    (prResetProcessorInfo->u16ResetReason == DEV_WUP_C_U16_RESET_REASON_SWITCH_BOOT_MODE_USB)    ) {

		tU8 u8BootMode = DEV_WUP_C_U8_BOOT_MODE_EMMC;

		if (prResetProcessorInfo->u16ResetReason == DEV_WUP_C_U16_RESET_REASON_SWITCH_BOOT_MODE_USB)
			u8BootMode = DEV_WUP_C_U8_BOOT_MODE_USB;

		DEV_WUP_vIncSend_CReqClientBootMode(u8BootMode);
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function handles a reset via an INC message which is send to the 
* system communication controller which finally executes the reset.
*
*******************************************************************************/
static tU32 DEV_WUP_u32RequestProcessorResetViaSCC(DEV_WUP_trResetProcessorInfo* prResetProcessorInfo, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32            u32OsalErrorCode = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore       = OSAL_C_INVALID_HANDLE;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	prGlobalData->u8LatestResetProcessor = DEV_WUP_C_U8_INVALID_PROCESSOR;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	DEV_WUP_vIncSend_CProcResetRequest(prResetProcessorInfo->u8Processor, prResetProcessorInfo->u16ResetDurationMs);

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME,
		&hSemaphore) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreWait(
		hSemaphore,
		DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_OK) {
			u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

			if (u32OsalErrorCode == OSAL_E_NOERROR) {
				if (prGlobalData->u8LatestResetProcessor != prResetProcessorInfo->u8Processor)
					u32OsalErrorCode = OSAL_E_UNKNOWN;

				DEV_WUP_vDataUnlock(hSemDataAccess);
			}
	} else {
		u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32RequestProcessorResetViaSCC() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* Perform a reset of the application processor via a root daemon call.
*
*******************************************************************************/
static tBool DEV_WUP_bExecuteApplicationProcessorReset(tVoid)
{
	CmdData rCmdData = { 0 };

	rCmdData = DEV_WUP_ROOTDAEMON_CALLER_rPerformRootOp(
			"dev_wup",
			RESET_APPLICATION_PROCESSOR,
			"");

	if (rCmdData.errorNo != ERR_NONE) {
		DEV_WUP_vWriteErrorMemoryFormatted(
			"DEV_WUP_bExecuteApplicationProcessorReset() failed with Root-Daemon error = %d",
			rCmdData.errorNo);
		return FALSE;
	}

	if (OSAL_s32StringCompare(rCmdData.message, "SUCCESS") != 0) {
		DEV_WUP_vWriteErrorMemoryFormatted(
			"DEV_WUP_bExecuteApplicationProcessorReset() => Root-Daemon command RESET_APPLICATION_PROCESSOR failed");
		return FALSE;
	}

	return TRUE;
}

/*******************************************************************************
*
* This function checks if the reset reason is a one for which the resulting
* reset of the application processor shall not be counted. In case of such a
* not to be counted reset reason, the respective intended-AP-reset flag is set
* in the PRAM and the AP reset counter is decreased by one.
*
*******************************************************************************/
static tVoid DEV_WUP_vDetermineAndHandleIntendedResetReason(DEV_WUP_trResetProcessorInfo* prResetProcessorInfo, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU8 u8ResetClassificationAP = DEV_WUP_C_U8_RESET_REASON_AP_EXCEPTIONAL;

	// Only handle intended reset reasons for the below subset of processors.
	// Currently the AP processor is also reset with each reset of the SCC (this might change in the future).
	// Therefore the below list also considers the SCC as a reset where the AP reset counter is decreased.
	if ((prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_ENTIRE_SYSTEM)                          &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_ENTIRE_SYSTEM_WITH_POWER_DISCONNECTION) &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_APPLICATION_PROCESSOR)                  &&
	    (prResetProcessorInfo->u8Processor != DEV_WUP_C_U8_SYSTEM_COMMUNICATION_CONTROLLER)          )
		return;

	// Determine INTENDED or EXCEPTIONAL reset depending on set or not set MSB of u16ResetReason.
	if (prResetProcessorInfo->u16ResetReason & 0x8000)
		u8ResetClassificationAP = DEV_WUP_C_U8_RESET_REASON_AP_INTENDED;

	(tVoid)DEV_WUP_u32DataLock(hSemDataAccess);

	prGlobalData->rPramMirror.u8ResetClassificationAP = u8ResetClassificationAP;

	DEV_WUP_vDataUnlock(hSemDataAccess);

	DEV_WUP_vWritePersistentRamData(
		prGlobalData,
		hSemDataAccess);

	// If the AP reset is INTENDED, then the reset shall not be counted and the AP reset counter is decremented by one.
	if (u8ResetClassificationAP == DEV_WUP_C_U8_RESET_REASON_AP_INTENDED)
		(tVoid)DEV_WUP_u32DecreaseApplicationProcessorResetCounter();
}

/*******************************************************************************
*
* Decrease the reset counter of the application processor which resides in
* the persistent RAM by one.
*
*******************************************************************************/
static tU32 DEV_WUP_u32DecreaseApplicationProcessorResetCounter(tVoid)
{
	tU32               u32OsalErrorCode       = OSAL_E_NOERROR;
	OSAL_tIODescriptor rIODescriptor          = OSAL_ERROR;
	tU32               u32ResetCounter        = 0;
	tU8                u8ResetCounter         = 0;
	tU8                u8ResetCounterInverted = 0;

	rIODescriptor = OSAL_IOOpen(
				OSAL_C_STRING_DEVICE_PRAM"/reset_counter",
				OSAL_EN_READWRITE);

	if (rIODescriptor == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_u32DecreaseApplicationProcessorResetCounter() => OSAL_IOOpen(DEVICE_PRAM) for 'reset_counter' failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));
		return u32OsalErrorCode;
	}

	if (OSAL_s32IOControl(
		rIODescriptor,
		OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK,
		0) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32DecreaseApplicationProcessorResetCounter() => First OSAL_s32IOControl(DEV_PRAM_SEEK) for 'reset_counter' failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32IORead(
		rIODescriptor,
		(tPS8)&u32ResetCounter,
		sizeof(u32ResetCounter)) != OSAL_ERROR) {

		if ((u32ResetCounter & 0xFF0000FF) == 0xCC0000AA) {
			u8ResetCounter         = (tU8)((u32ResetCounter >>  8) & 0x000000FF);
			u8ResetCounterInverted = (tU8)((u32ResetCounter >> 16) & 0x000000FF);
			
			if (u8ResetCounter > 0) {
				if (u8ResetCounterInverted == (0xFF - u8ResetCounter)) {
					u8ResetCounter--;
					u32ResetCounter = 0xCC0000AA;
					u32ResetCounter = u32ResetCounter | (((tU32)u8ResetCounter) << 8);
					u32ResetCounter = u32ResetCounter | (((tU32)(0xFF - u8ResetCounter)) << 16);

					if (OSAL_s32IOControl(
						rIODescriptor,
						OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK,
						0) == OSAL_ERROR) {
						u32OsalErrorCode = OSAL_u32ErrorCode();
						DEV_WUP_vTraceFormatted(
							TR_LEVEL_FATAL,
							"DEV_WUP_u32DecreaseApplicationProcessorResetCounter() => Second OSAL_s32IOControl(DEV_PRAM_SEEK) for 'reset_counter' failed with error code = '%s'",
							OSAL_coszErrorText(u32OsalErrorCode));
						goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
					}

					if (OSAL_s32IOWrite(
						rIODescriptor,
						(tPCS8)&u32ResetCounter,
						sizeof(u32ResetCounter)) == OSAL_ERROR) {
						u32OsalErrorCode = OSAL_u32ErrorCode();
						DEV_WUP_vTraceFormatted(
							TR_LEVEL_FATAL,
							"DEV_WUP_u32DecreaseApplicationProcessorResetCounter() => OSAL_s32IOWrite(DEVICE_PRAM) for 'reset_counter' failed with error code = '%s'",
							OSAL_coszErrorText(u32OsalErrorCode));
					} else {
						DEV_WUP_vTraceFormatted(
							TR_LEVEL_USER_3,
							"DEV_WUP_u32DecreaseApplicationProcessorResetCounter() => Application processor reset counter decreased by one. New magic = 0x%08X",
							u32ResetCounter);
					}
				} else {
					u32OsalErrorCode = OSAL_E_UNKNOWN;
					DEV_WUP_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_WUP_u32DecreaseApplicationProcessorResetCounter() => Can't decrease application processor reset counter = 0x%08X because the inverted counter value 0x%02X is invalid",
						u32ResetCounter,
						u8ResetCounterInverted);
				}
			}
		} else {
			u32OsalErrorCode = OSAL_E_UNKNOWN;
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32DecreaseApplicationProcessorResetCounter() => Can't decrease application processor reset counter = 0x%08X because it is NOT enclosed by magic 0xCC....AA.",
				u32ResetCounter);
		}
	} else {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32DecreaseApplicationProcessorResetCounter() => OSAL_s32IORead(DEVICE_PRAM) for 'reset_counter' failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));
	}

error_out:

	if (OSAL_s32IOClose(rIODescriptor) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32DecreaseApplicationProcessorResetCounter() => OSAL_IOClose(DEVICE_PRAM) failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function extends the power off timeout by sending the respective INC 
* message to the system communication controller.
*
*******************************************************************************/
static tU32 DEV_WUP_u32ExtendPowerOffTimeout(tU16* pu16TimeoutS, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32            u32OsalErrorCode      = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore            = OSAL_C_INVALID_HANDLE;

	if (*pu16TimeoutS == 0)
		return OSAL_E_INVALIDVALUE;

	DEV_WUP_vIncSend_CExtendPowerOffTimeout(*pu16TimeoutS);

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME,
		&hSemaphore) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreWait(
		hSemaphore,
		DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_OK) {

		u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

		if (u32OsalErrorCode == OSAL_E_NOERROR) {
			*pu16TimeoutS = prGlobalData->u16ReportedPowerOffTimeoutS;
			DEV_WUP_vDataUnlock(hSemDataAccess);
		}
	} else {
		u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32ExtendPowerOffTimeout() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;

}

/*******************************************************************************
*
* This function controls if and how the reset master - the system communication
* controller - supervises the application processor by sending the 
* respective INC message.
*
*******************************************************************************/
static tU32 DEV_WUP_u32ControlResetMasterSupervision(DEV_WUP_trResetMasterSupervision* prResetMasterSupervision)
{
	tU32            u32OsalErrorCode      = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore            = OSAL_C_INVALID_HANDLE;
	tU8             u8ResetControlBitmask = 0;

	if (prResetMasterSupervision->u8ExceptionBitmask & ~DEV_WUP_C_U8_BITMASK_SUPERVISION_METHOD_COM_WD)
		return OSAL_E_INVALIDVALUE;

	if (prResetMasterSupervision->bSwitchOff == TRUE)
		u8ResetControlBitmask |= DEV_WUP_C_U8_PREVENT_VCC_RESET_BITMASK_ALL;

	if (prResetMasterSupervision->u8ExceptionBitmask & DEV_WUP_C_U8_BITMASK_SUPERVISION_METHOD_COM_WD)
		u8ResetControlBitmask |= DEV_WUP_C_U8_PREVENT_VCC_RESET_BITMASK_COM_WD;

	DEV_WUP_vIncSend_CCtrlResetExecution(u8ResetControlBitmask);

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME,
		&hSemaphore) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreWait(
		hSemaphore,
		DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_ERROR)
			u32OsalErrorCode = OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32ControlResetMasterSupervision() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function configures which wake-up reasons shall be considered for the
* initial wake-up of the system by sending the respective INC message.
*
*******************************************************************************/
static tU32 DEV_WUP_u32ConfigureWakeupReasons(tU32 u32WakeupReasonsMask, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32            u32OsalErrorCode = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore       = OSAL_C_INVALID_HANDLE;

	if (u32WakeupReasonsMask > (DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ON_TIPPER           |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CAN                 |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_MOST                |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_IGN_PIN             |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_TEL_MUTE            |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_DEBUG_WAKEUP        |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_RTC_WAKEUP          |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CD_INSERT_DETECTED  |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CD_EJECT_DETECTED   |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_S_CONTACT_WAKEUP    |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CELLNETWORK_WAKEUP  |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CAN2_WAKEUP         |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CAN3_WAKEUP         |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_CAN4_WAKEUP         |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ODOMETER_WAKEUP     |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_LIN_WAKEUP          |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_UART                |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_UART2               |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_USB                 |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_BLUETOOTH           |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ACCELERATOR_SENSOR  |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_GPIO2      |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_GPIO3      |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_GPIO4      |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_GPIO5      |
				    DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ILLUMINATION         ))
		return OSAL_E_INVALIDVALUE;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode == OSAL_E_NOERROR) {
		prGlobalData->u32AcknowledgedWakeupReasonsMask = 0x00000000;
		DEV_WUP_vDataUnlock(hSemDataAccess);
	} else {
		return u32OsalErrorCode;
	}

	DEV_WUP_vIncSend_CSetWakeupConfig(u32WakeupReasonsMask);

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_WAKEUP_CFG_MSG_RECEIVED_NAME,
		&hSemaphore) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreWait(
		hSemaphore,
		DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_OK) {

		u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

		if (u32OsalErrorCode == OSAL_E_NOERROR) {
			if (prGlobalData->u32AcknowledgedWakeupReasonsMask != u32WakeupReasonsMask)
				u32OsalErrorCode = OSAL_E_UNKNOWN;

			DEV_WUP_vDataUnlock(hSemDataAccess);
		}
	} else {
		u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32ConfigureWakeupReasons() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function indicates a finished startup of the system by sending the 
* respective INC message.
*
*******************************************************************************/
static tU32 DEV_WUP_u32IndicateStartupFinished(tVoid)
{
	tU32            u32OsalErrorCode = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore       = OSAL_C_INVALID_HANDLE;

	DEV_WUP_vIncSend_CStartupFinished();

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_START_FINISH_MSG_RECEIVED_NAME,
		&hSemaphore) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreWait(
		hSemaphore,
		DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_ERROR)
			u32OsalErrorCode = OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32IndicateStartupFinished() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function triggers the shutdown of the system by sending the respective
* INC message.
*
*******************************************************************************/
static tU32 DEV_WUP_u32TriggerSystemShutdown(tU8 u8ShutdownType, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32            u32OsalErrorCode = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemaphore       = OSAL_C_INVALID_HANDLE;

	if (u8ShutdownType != DEV_WUP_SHUTDOWN_NORMAL)
		return OSAL_E_INVALIDVALUE;

	u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode == OSAL_E_NOERROR) {
		prGlobalData->bSystemShutdownSuccess = FALSE;
		DEV_WUP_vDataUnlock(hSemDataAccess);
	} else {
		return u32OsalErrorCode;
	}

	DEV_WUP_vIncSend_CShutdownInProgress();

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_SHUTDOWN_MSG_RECEIVED_NAME,
		&hSemaphore) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreWait(
		hSemaphore,
		DEV_WUP_C_U32_SEM_INC_MSG_RECEIVED_TIMEOUT_MS) == OSAL_OK) {

		u32OsalErrorCode = DEV_WUP_u32DataLock(hSemDataAccess);

		if (u32OsalErrorCode == OSAL_E_NOERROR) {
			if (prGlobalData->bSystemShutdownSuccess == FALSE)
				u32OsalErrorCode = OSAL_E_UNKNOWN;
			DEV_WUP_vDataUnlock(hSemDataAccess);
		}
	} else {
		u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	if (OSAL_s32SemaphoreClose(hSemaphore) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_u32TriggerSystemShutdown() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function installs the thread which operates as a test client for this
* device.
*
*******************************************************************************/
static tU32 DEV_WUP_u32InstallClientThread(tU8 u8NotificationMode)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	OSAL_trThreadAttribute rThreadAttribute;
	OSAL_tIODescriptor     rWupIODescriptor;

	if (g_rModuleData.rWupIODescriptor != OSAL_ERROR)
		return OSAL_E_ALREADYEXISTS;

	rWupIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_WUP, OSAL_EN_READWRITE);

	if (rWupIODescriptor == OSAL_ERROR)
		return OSAL_u32ErrorCode();

	g_rModuleData.rWupIODescriptor = rWupIODescriptor;

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_C_STRING_SEM_CLIENT_THREAD_NAME,
		&g_rModuleData.hSemClientThread,
		(tU32) 0) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();

		goto error_semaphore_create; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rThreadAttribute.szName       = (tChar*)DEV_WUP_C_STRING_CLIENT_THREAD_NAME;
	rThreadAttribute.s32StackSize = DEV_WUP_C_S32_CLIENT_THREAD_STK_SIZE;
	rThreadAttribute.u32Priority  = DEV_WUP_C_U32_CLIENT_THREAD_PRIO;
	rThreadAttribute.pfEntry      = (OSAL_tpfThreadEntry) DEV_WUP_vClientThread;
	rThreadAttribute.pvArg        = &u8NotificationMode;

	DEV_WUP_vTraceFormatted(TR_LEVEL_USER_4, "DEV_WUP_u32InstallClientThread() => Try to spawn thread ...");

	if (OSAL_ThreadSpawn(&rThreadAttribute) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();

		goto error_thread_spawn; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		g_rModuleData.hSemClientThread,
		DEV_WUP_C_U32_SEM_THREAD_TIMEOUT_MS) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();

		goto error_semaphore_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (g_rModuleData.u8ClientThreadState != DEV_WUP_C_U8_THREAD_RUNNING) {

		u32OsalErrorCode = OSAL_E_UNKNOWN;

		DEV_WUP_vTraceFormatted(TR_LEVEL_FATAL, "DEV_WUP_u32InstallClientThread() => ... failed to install thread");

		goto error_thread_state; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_WUP_vTraceFormatted(TR_LEVEL_USER_4, "DEV_WUP_u32InstallClientThread() => ... properly installed thread");

	return OSAL_E_NOERROR;
	
error_thread_state:
error_semaphore_wait:
error_thread_spawn:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemClientThread) == OSAL_OK) {
		if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_CLIENT_THREAD_NAME) == OSAL_OK) {
			g_rModuleData.hSemClientThread = OSAL_C_INVALID_HANDLE;
		} else {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32InstallClientThread() => OSAL_s32SemaphoreDelete() failed with error code = '%s'",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	} else {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32InstallClientThread() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

error_semaphore_create:

	if (OSAL_s32IOClose(g_rModuleData.rWupIODescriptor) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_u32InstallClientThread() => OSAL_s32IOClose() failed with error code = '%s'",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		g_rModuleData.rWupIODescriptor = OSAL_ERROR;

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function un-installs the thread which operates as a test client for this
* device.
*
*******************************************************************************/
static tU32 DEV_WUP_u32UninstallClientThread(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (g_rModuleData.u8ClientThreadState != DEV_WUP_C_U8_THREAD_RUNNING)
		return OSAL_E_NOERROR;

	DEV_WUP_vTraceFormatted(TR_LEVEL_USER_4, "DEV_WUP_u32UninstallClientThread() => Sent event STOP_THREAD to leave thread ...");

	if (g_rModuleData.hEventClientThread == OSAL_C_INVALID_HANDLE) {
		u32OsalErrorCode = OSAL_E_DOESNOTEXIST;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}
	
	if (OSAL_s32EventPost(
		g_rModuleData.hEventClientThread,
		DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (g_rModuleData.hSemClientThread == OSAL_C_INVALID_HANDLE) {
		u32OsalErrorCode = OSAL_E_DOESNOTEXIST;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		g_rModuleData.hSemClientThread,
		DEV_WUP_C_U32_SEM_THREAD_TIMEOUT_MS) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (g_rModuleData.u8ClientThreadState != DEV_WUP_C_U8_THREAD_SHUTTING_DOWN) {

		u32OsalErrorCode = OSAL_E_UNKNOWN;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

        DEV_WUP_vTraceFormatted(TR_LEVEL_USER_4, "DEV_WUP_u32UninstallClientThread() => Thread acknowledged shutdown request ... wait 1 second ...");

        OSAL_s32ThreadWait(1000); // Give thread 1 second to leave its own thread context.

        g_rModuleData.u8ClientThreadState = DEV_WUP_C_U8_THREAD_OFF;

        DEV_WUP_vTraceFormatted(TR_LEVEL_USER_4, "DEV_WUP_u32UninstallClientThread() => ... 1 second expired and thread should be gone");

error_out:

	if (g_rModuleData.hSemClientThread != OSAL_C_INVALID_HANDLE) {
		if (OSAL_s32SemaphoreClose(g_rModuleData.hSemClientThread) == OSAL_OK)
			if (OSAL_s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_CLIENT_THREAD_NAME) == OSAL_OK)
				g_rModuleData.hSemClientThread = OSAL_C_INVALID_HANDLE;
			else
				u32OsalErrorCode = OSAL_u32ErrorCode();
		else
			u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	if (g_rModuleData.rWupIODescriptor != OSAL_ERROR) {
		if (OSAL_s32IOClose(g_rModuleData.rWupIODescriptor) == OSAL_OK)
			g_rModuleData.rWupIODescriptor = OSAL_ERROR;
		else
			u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function represents the thread which handles the OSAL events which are
* used to perform actions of the test client.
*
*******************************************************************************/
static tVoid DEV_WUP_vClientThread(tPVoid pvArg)
{
	tU32            u32OsalErrorCode    = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemClientThread    = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask rEventMaskResult    = 0;
	tPU8            pu8NotificationMode = (tPU8)pvArg;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	g_rModuleData.u8ClientThreadState = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_C_STRING_SEM_CLIENT_THREAD_NAME,
		&hSemClientThread) == OSAL_ERROR) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => OSAL_s32SemaphoreOpen() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		goto error_semaphore_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	u32OsalErrorCode = DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => DEV_WUP_u32RegisterClient() failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));
	
		goto error_register_client; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventOpen(
		rClientRegistration.szNotificationEventName,
		&g_rModuleData.hEventClientThread) == OSAL_ERROR) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => OSAL_s32EventOpen() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.u8NotificationMode = *pu8NotificationMode;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = FALSE;

	u32OsalErrorCode = DEV_WUP_u32RegisterOnOffReasonChanged(
		&rOnOffReasonChangedRegistration,
		g_rModuleData.prGlobalData,
		g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => DEV_WUP_u32RegisterOnOffReasonChanged() failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));
	
		goto error_register_onoff_reason; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	u32OsalErrorCode = DEV_WUP_u32RegisterAPSupervisionErrorChanged(
		rClientRegistration.u32ClientId,
		g_rModuleData.prGlobalData,
		g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => DEV_WUP_u32RegisterAPSupervisionErrorChanged() failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));
	
		goto error_register_ap_supervision_error; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.u8ClientThreadState = DEV_WUP_C_U8_THREAD_RUNNING;

	if (OSAL_s32SemaphorePost(hSemClientThread) == OSAL_ERROR) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => OSAL_s32SemaphorePost failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		g_rModuleData.u8ClientThreadState = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;
	}

	while (DEV_WUP_C_U8_THREAD_RUNNING == g_rModuleData.u8ClientThreadState) {
		if (OSAL_s32EventWait(
			g_rModuleData.hEventClientThread,
			DEV_WUP_C_U32_CLIENT_THREAD_EVENT_MASK_ALL,
			OSAL_EN_EVENTMASK_OR,
			OSAL_C_TIMEOUT_FOREVER,
			&rEventMaskResult) == OSAL_OK) {

			if (OSAL_s32EventPost(
				g_rModuleData.hEventClientThread,
				~rEventMaskResult, 
				OSAL_EN_EVENTMASK_AND) == OSAL_ERROR)
					DEV_WUP_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_WUP_vClientThread() => OSAL_s32EventPost() failed with error code = '%s'",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
				
				DEV_WUP_vOnClientEventReceived(
					&rClientRegistration,
					rEventMaskResult,
					hSemClientThread);
		} else {
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vClientThread() => OSAL_s32EventWait() failed with error code = '%s'",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}

	u32OsalErrorCode = DEV_WUP_u32UnregisterOnOffReasonChanged(
				rOnOffReasonChangedRegistration.u32ClientId,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => DEV_WUP_u32UnregisterOnOffReasonChanged() failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));

error_register_ap_supervision_error:

	u32OsalErrorCode = DEV_WUP_u32UnregisterAPSupervisionErrorChanged(
				rClientRegistration.u32ClientId,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => DEV_WUP_u32UnregisterAPSupervisionErrorChanged() failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));

error_register_onoff_reason:

	if (OSAL_s32EventClose(g_rModuleData.hEventClientThread) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => OSAL_s32EventClose() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_event_open:

	u32OsalErrorCode = DEV_WUP_u32UnregisterClient(
		rClientRegistration.u32ClientId,
		g_rModuleData.prGlobalData,
		g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => DEV_WUP_u32UnregisterClient() failed with error code = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));

error_register_client:

	if (OSAL_s32SemaphoreClose(hSemClientThread) == OSAL_ERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => OSAL_s32SemaphoreClose() failed with error code = '%s'",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_semaphore_open:

	if (DEV_WUP_C_U8_THREAD_NOT_INSTALLED == g_rModuleData.u8ClientThreadState)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vClientThread() => Immediately left due to setup failure");
	else
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_USER_3,
			"DEV_WUP_vClientThread() => Left after controlled shutdown");
}

/*******************************************************************************
*
* This function evaluates the OSAL events which are received within the context
* of the thread function DEV_WUP_vClientThread().
*
*******************************************************************************/
static tVoid DEV_WUP_vOnClientEventReceived(DEV_WUP_trClientRegistration* rClientRegistration, OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemClientThread)
{
	if (rEventMaskResult & DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD) {

		DEV_WUP_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_WUP_vOnClientEventReceived() => ... event STOP_THREAD received, thread is shutting down ...");

		g_rModuleData.u8ClientThreadState = DEV_WUP_C_U8_THREAD_SHUTTING_DOWN;

		if (OSAL_s32SemaphorePost(hSemClientThread) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_WUP_vOnClientEventReceived() => OSAL_s32SemaphorePost() for thread shutdown failed with error code = '%s'",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD);
	}

	if (rEventMaskResult & DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY) {

		DEV_WUP_trOnOffEventHistory rOnOffEventHistory;

		rOnOffEventHistory.u32ClientId = rClientRegistration->u32ClientId;

		if (OSAL_s32IOControl(
			g_rModuleData.rWupIODescriptor,
			OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_EVENTS,
			(intptr_t)&rOnOffEventHistory) == OSAL_ERROR)
			DEV_WUP_vTraceFormatted(
				TR_LEVEL_ERRORS,
				"DEV_WUP_vOnClientEventReceived() => OSAL_s32IOControl(OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_EVENTS) failed with error code = '%s'",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY);
	}

	if (rEventMaskResult & DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY) {

		DEV_WUP_trOnOffStates rOnOffStates;

		if (OSAL_s32IOControl(
			g_rModuleData.rWupIODescriptor,
			OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_STATES,
			(intptr_t)&rOnOffStates) == OSAL_ERROR)
				DEV_WUP_vTraceFormatted(
					TR_LEVEL_ERRORS,
					"DEV_WUP_vOnClientEventReceived() => OSAL_s32IOControl(OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_STATES) failed with error code = '%s'",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY);
	}

	if (rEventMaskResult & DEV_WUP_C_U32_EVENT_MASK_AP_SUPERVISION_ERROR_CHANGED_NOTIFY) {
		tU8 u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_NONE;

		if (OSAL_s32IOControl(
			g_rModuleData.rWupIODescriptor,
			OSAL_C_S32_IOCTRL_WUP_GET_AP_SUPERVISION_ERROR,
			(intptr_t)&u8APSupervisionError) == OSAL_ERROR)
				DEV_WUP_vTraceFormatted(
					TR_LEVEL_ERRORS,
					"DEV_WUP_vOnClientEventReceived() => OSAL_s32IOControl(OSAL_C_S32_IOCTRL_WUP_GET_AP_SUPERVISION_ERROR) failed with error code = '%s'",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_WUP_C_U32_EVENT_MASK_AP_SUPERVISION_ERROR_CHANGED_NOTIFY);
	}

	if (rEventMaskResult)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_WUP_vOnClientEventReceived() => OSAL_s32EventWait() received unknown event = 0x%08X",
			rEventMaskResult);
}

/******************************************************************************/
/*                                                                            */
/* PUBLIC FUNCTIONS                                                           */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* This function initializes the /dev/wup device.
*
*******************************************************************************/
tU32 DEV_WUP_OsalIO_u32Init(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32Init();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_OsalIO_u32Init() left with error code = 0x%08X = '%s'",
			u32OsalErrorCode,
			OSAL_coszErrorText(u32OsalErrorCode));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function de-initializes the /dev/wup device.
*
*******************************************************************************/
tU32 DEV_WUP_OsalIO_u32Deinit(tVoid)
{
	tU32 u32OsalErrorCode  = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32Deinit();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_OsalIO_u32Deinit() left with error code = 0x%08X = '%s'",
			u32OsalErrorCode,
			OSAL_coszErrorText(u32OsalErrorCode));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function opens the /dev/wup device.
*
*******************************************************************************/
tU32 DEV_WUP_OsalIO_u32Open(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32Open();

	if (u32OsalErrorCode != OSAL_E_NOERROR) 
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_OsalIO_u32Open() left with error code = 0x%08X = '%s'",
			u32OsalErrorCode,
			OSAL_coszErrorText(u32OsalErrorCode));

	return (u32OsalErrorCode);
}

/*******************************************************************************
*
* This function closes the /dev/wup device.
*
*******************************************************************************/
tU32 DEV_WUP_OsalIO_u32Close(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32Close();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_OsalIO_u32Close() left with error code = 0x%08X = '%s'",
			u32OsalErrorCode,
			OSAL_coszErrorText(u32OsalErrorCode));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function is the IO-control interface of the /dev/wup device.
*
*******************************************************************************/
tU32 DEV_WUP_OsalIO_u32Control(tS32 s32Fun, intptr_t pnArg)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_WUP_u32Control(s32Fun, pnArg);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_WUP_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_WUP_OsalIO_u32Control() left with error code = 0x%08X = '%s'",
			u32OsalErrorCode,
			OSAL_coszErrorText(u32OsalErrorCode));

	return (u32OsalErrorCode);
}

/*******************************************************************************
*
* This function is a wrapper to open the /dev/wup device.
*
*******************************************************************************/
tS32 wup_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
	(tVoid) s32Id;    // Unused parameter
	(tVoid) szName;   // Unused parameter
	(tVoid) enAccess; // Unused parameter
	(tVoid) pu32FD;   // Unused parameter
	(tVoid) app_id;   // Unused parameter

	return ((tS32)DEV_WUP_OsalIO_u32Open());
}

/*******************************************************************************
*
* This function is a wrapper to close the /dev/wup device.
*
*******************************************************************************/
tS32 wup_drv_io_close(tS32 s32ID, uintptr_t u32FD)
{
	(tVoid) s32ID; // Unused parameter
	(tVoid) u32FD; // Unused parameter

	return ((tS32)DEV_WUP_OsalIO_u32Close());
}

/*******************************************************************************
*
* This function is a wrapper for the IO-control interface of the /dev/wup device.
*
*******************************************************************************/
tS32 wup_drv_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t pnArg)
{
	(tVoid) s32ID; // Unused parameter
	(tVoid) u32FD; // Unused parameter

	return ((tS32)DEV_WUP_OsalIO_u32Control(s32fun, pnArg));
}

/******************************************************************************/
