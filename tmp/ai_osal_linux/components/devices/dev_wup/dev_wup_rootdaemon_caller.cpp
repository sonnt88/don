/*******************************************************************************
*
* FILE:         dev_wup_rootdaemon_caller.cpp
*
* SW-COMPONENT: Device Wake-Up
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  This file contains the C binding wrapper function to call the
*               C++ method for root daemon operations.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2015 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

#include "RootDaemonHelper.h"

extern "C"
{
#include "dev_wup_rootdaemon.h"

/******************************************************************************/
/*                                                                            */
/* FUNCTION DECLARATIONS                                                      */
/*                                                                            */
/******************************************************************************/

// Function DEV_WUP_ROOTDAEMON_CLIENT_rPerformRootOpAsRoot() resides in file dev_wup_rootdaemon_client.cpp
extern CmdData DEV_WUP_ROOTDAEMON_CLIENT_rPerformRootOpAsRoot(const int cmdNum, std::string args);

/******************************************************************************/
/*                                                                            */
/* FUNCTIONS                                                                  */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Function with C binding to call the static C++ method performRootOp() from
* the C based wake-up device.
*
*******************************************************************************/
CmdData DEV_WUP_ROOTDAEMON_CALLER_rPerformRootOp(const char * clientName, const tenRootDaemonCommands cmdNum, const char * args)
{
	// When user is root then perform the root operation directly, otherwise
	// perform the root operation via the root daemon.
	if(geteuid() == 0)
		return DEV_WUP_ROOTDAEMON_CLIENT_rPerformRootOpAsRoot(cmdNum, std::string(args));
	else
		return RootDaemonHelper::performRootOp(
			clientName,
			cmdNum,
			std::string(args));
}

/******************************************************************************/

} // extern "C"
