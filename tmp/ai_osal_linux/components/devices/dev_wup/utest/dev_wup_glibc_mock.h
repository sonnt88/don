/*******************************************************************************
*
* FILE:         dev_wup_glibc_mock.h
* 
* SW-COMPONENT: Device Wakeup
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  GLIBC mocks for unit testing
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_WUP_GLIBC_MOCK_H_
#define _DEV_WUP_GLIBC_MOCK_H_

#define open(a,b)                  open_mock(a,b)              // /usr/include/fcntl.h
#define close(a)                   close_mock(a)               // /usr/include/unistd.h
#define write(a,b,c)               write_mock(a,b,c)           // /usr/include/unistd.h
#define socket(a,b,c)              socket_mock(a,b,c)          // /usr/include/i386-linux-gnu/sys/socket.h
#define setsockopt(a,b,c,d,e)      setsockopt_mock(a,b,c,d,e)  // /usr/include/i386-linux-gnu/sys/socket.h
#define bind(a,b,c)                bind_mock(a,b,c)            // /usr/include/i386-linux-gnu/sys/socket.h
#define connect(a,b,c)             connect_mock(a,b,c)         // /usr/include/i386-linux-gnu/sys/socket.h
#define gethostbyname(a)           gethostbyname_mock(a)       // /usr/include/netdb.h
#define opendir(a)                 opendir_mock(a)             // /usr/include/dirent.h
#define closedir(a)                closedir_mock(a)            // /usr/include/dirent.h
#define readdir(a)                 readdir_mock(a)             // /usr/include/dirent.h

extern int             open_mock          (const char*, int);
extern int             close_mock         (int);
extern ssize_t         write_mock         (int, const void*, size_t);
extern int             socket_mock        (int , int , int);
extern int             setsockopt_mock    (int , int , int, const void*, socklen_t);
extern int             bind_mock          (int , const struct sockaddr*, socklen_t);
extern int             connect_mock       (int , const struct sockaddr*, socklen_t);
extern struct hostent* gethostbyname_mock (const char*);
extern DIR*            opendir_mock       (const char *name);
extern int             closedir_mock      (DIR *dirp);
extern struct dirent*  readdir_mock       (DIR *dirp);

#endif // _DEV_WUP_GLIBC_MOCK_H_

/******************************************************************************/
