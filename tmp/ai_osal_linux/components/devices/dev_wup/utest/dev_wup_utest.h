/*******************************************************************************
*
* FILE:         dev_wup_utest.h
* 
* SW-COMPONENT: Device Wakeup
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Unit tests
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_WUP_UNIT_TEST_H_
#define _DEV_WUP_UNIT_TEST_H_

class DevWupTest : public ::testing::Test 
{
	public:
		DevWupTest() {}
		virtual ~DevWupTest() {}

	protected:
		virtual void SetUp();
		virtual void TearDown();

		OSAL_trProcessControlBlock m_rPCB;
		OSAL_trThreadControlBlock  m_rTCB;

		tChar m_acIncLocalAddress[4];
		tChar* m_apIncLocalAddressList[1];

		tChar m_acIncRemoteAddress[4];
		tChar* m_apIncRemoteAddressList[1];

		sk_dgram m_rsk_dgram;

		struct hostent m_rhostent_IncLocal;
		struct hostent m_rhostent_IncRemote;

		struct dirent m_rdirent_Undervoltage;

//		DIR* m_prDir_Undervoltage;

		trGlobalData m_rGlobalData;

		OsalMock m_oOsalMock;
		DevWupMock m_oDevWupMock;
};

class DevWupTestIOControl : public DevWupTest
{
	public:
		DevWupTestIOControl() {}
		virtual ~DevWupTestIOControl() {}

	protected:
		virtual void SetUp();
		virtual void TearDown();
};

class DevWupTestInternal : public DevWupTest
{
	public:
		DevWupTestInternal() {}
		virtual ~DevWupTestInternal() {}

	protected:
		virtual void SetUp();
		virtual void TearDown();
};

#endif //_DEV_WUP_UNIT_TEST_H

/******************************************************************************/
