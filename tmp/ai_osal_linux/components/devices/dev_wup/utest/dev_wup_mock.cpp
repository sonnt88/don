/*******************************************************************************
*
* FILE:         dev_wup_mock.cpp
*
* SW-COMPONENT: Wakeup Device
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Mocks for unit testing
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdio.h>
#include <dirent.h>

#include "system_types.h"
#include "system_definition.h"
#include "dgram_service.h"
#include "RootDaemonTypes.h"

#include "dev_wup_mock.h"

extern "C"
{
  /******************************************************************************/
  /*                                   GLIBC                                    */
  /******************************************************************************/

  int open_mock(const char *pathname, int flags)
  {
	return DevWupMock::GetDelegatee().open_mock(pathname, flags);
  }

  int close_mock(int fd)
  {
	return DevWupMock::GetDelegatee().close_mock(fd);
  }

  ssize_t write_mock(int fd, const void *buf, size_t count)
  {
	return DevWupMock::GetDelegatee().write_mock(fd, buf, count);
  }

  int socket_mock(int domain, int type, int protocol)
  {
	return DevWupMock::GetDelegatee().socket_mock(domain, type, protocol);
  }

  int setsockopt_mock(int sockfd, int level, int optname, const void *optval, socklen_t optlen)
  {
	return DevWupMock::GetDelegatee().setsockopt_mock(sockfd, level, optname, optval, optlen);
  }

  int bind_mock(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
  {
	return DevWupMock::GetDelegatee().bind_mock(sockfd, addr, addrlen);
  }

  int connect_mock(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
  {
	return DevWupMock::GetDelegatee().connect_mock(sockfd, addr, addrlen);

  }

  struct hostent * gethostbyname_mock(const char *name)
  {
	return DevWupMock::GetDelegatee().gethostbyname_mock(name);
  }

  DIR* opendir_mock(const char *name)
  {
	return DevWupMock::GetDelegatee().opendir_mock(name);
  }

  int closedir_mock(DIR *dirp)
  {
	return DevWupMock::GetDelegatee().closedir_mock(dirp);
  }

  struct dirent* readdir_mock(DIR *dirp)
  {
	return DevWupMock::GetDelegatee().readdir_mock(dirp);
  }

  /******************************************************************************/
  /*                                    LLD                                     */
  /******************************************************************************/

  tBool LLD_bIsTraceActive(tU32 u32Class, tU32 u32Level)
  {
	return TRUE;
//  	return DevWupMock::GetDelegatee().LLD_bIsTraceActive(u32Class, u32Level);
  } 

  tVoid LLD_vTrace(tU32 u32Class, tU32 u32Level, const void* pvData, tU32 u32Length)
  {
#if 0
	char* chData = (char*) pvData;

	if (chData[0] == 0x01) // 0x01 = DEV_WUP_C_U8_TRACE_TYPE_STRING
		printf("LLD_vTrace : %s\n", &chData[1]);
#endif
//  	return DevWupMock::GetDelegatee().LLD_vTrace(u32Class, u32Level, pvData, u32Length);
  } 

  /******************************************************************************/
  /*                                DGRAM_SERVICE                               */
  /******************************************************************************/

  sk_dgram* dgram_init(int sk, int dgram_max, void *options)
  {
	return DevWupMock::GetDelegatee().dgram_init(sk, dgram_max, options);
  }

  int dgram_exit(sk_dgram *skd)
  {
	return DevWupMock::GetDelegatee().dgram_exit(skd);
  }

  int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen)
  {
	return DevWupMock::GetDelegatee().dgram_send(skd, ubuf, ulen);
  }

  int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen)
  {
	return DevWupMock::GetDelegatee().dgram_recv(skd, ubuf, ulen);
  }

  /******************************************************************************/
  /*                          DEV WUP / ROOT DAEMON CALLER                      */
  /******************************************************************************/

  CmdData DEV_WUP_ROOTDAEMON_CALLER_rPerformRootOp(const char * clientName, const int cmdNum, const char * args)
  {
	CmdData rCmdData = { 0, 0, ERR_NONE, "SUCCESS" };

	return rCmdData;
  }
} // extern "C"
