/*******************************************************************************
*
* FILE:         dev_wup_mock.h
* 
* SW-COMPONENT: Device Wakeup
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Mocks for unit testing
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_WUP_MOCK_H_
#define _DEV_WUP_MOCK_H_

#include "gmock/gmock.h"
#include "MockDelegatee.h"

class DevWupMock : public MockDelegatee<DevWupMock>
{
  public:
	static inline const char *GetMockName() { return "DevWupMock"; }

	static DevWupMock &GetDelegatee()
	{
		return MockDelegatee<DevWupMock>::GetDelegatee();
	};

	/* GLIBC */
	MOCK_METHOD2(open_mock,             int (const char*, int));
	MOCK_METHOD1(close_mock,            int (int));
	MOCK_METHOD3(write_mock,            ssize_t (int, const void*, size_t));
	MOCK_METHOD3(socket_mock,           int (int , int , int));
	MOCK_METHOD5(setsockopt_mock,       int (int , int , int, const void*, socklen_t));
	MOCK_METHOD3(bind_mock,             int (int , const struct sockaddr*, socklen_t));
	MOCK_METHOD3(connect_mock,          int (int , const struct sockaddr*, socklen_t));
	MOCK_METHOD1(gethostbyname_mock,    struct hostent* (const char*));
	MOCK_METHOD1(opendir_mock,          DIR* (const char *name));
	MOCK_METHOD1(closedir_mock,         int (DIR *dirp));
	MOCK_METHOD1(readdir_mock,          struct dirent* (DIR *dirp));

	/* LLD */
	MOCK_METHOD2(LLD_bIsTraceActive,    tBool (tU32, tU32));
	MOCK_METHOD4(LLD_vTrace,            tVoid (tU32, tU32, const void*, tU32));

	/* DGRAM_SERVICE */
	MOCK_METHOD3(dgram_init,            sk_dgram* (int, int, void*));
	MOCK_METHOD1(dgram_exit,            int (sk_dgram*));
	MOCK_METHOD3(dgram_send,            int (sk_dgram*, void*, size_t));
	MOCK_METHOD3(dgram_recv,            int (sk_dgram*, void*, size_t));
};

#endif // _DEV_WUP_MOCK_H_

/******************************************************************************/
