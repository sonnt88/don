/*******************************************************************************
*
* FILE:         dev_wup_utest.cpp
*
* SW-COMPONENT: Wakeup Device
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Unit tests
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#include "gmock/gmock.h"
#include "gtest/gtest.h"

extern "C"
{
  #include "../dev_wup.c"
}

#include "dev_wup_mock.h"

#include "osal_if_mock.h"

#include "dev_wup_utest.h"

/******************************************************************************/

using ::testing::Return;
using ::testing::ReturnNull;
using ::testing::ReturnArg;
using ::testing::ReturnPointee;
using ::testing::SetArgPointee;
using ::testing::DoAll;
using ::testing::_;
using ::testing::Invoke;
using ::testing::WithArg;
using ::testing::WithArgs;
using ::testing::Pointee;
using ::testing::StrEq;
using ::testing::ResultOf;
using ::testing::MatcherCast;
using ::testing::SaveArgPointee;
using ::testing::SetArrayArgument;

/******************************************************************************/

#define DEV_WUP_C_S32_OSAL_PROCESS_ID                                 0x00000100
#define DEV_WUP_C_S32_OSAL_THREAD_ID                                  0x00000200

/******************************************************************************/

static tU32 g_u32OsalErrorCode = OSAL_E_NOERROR;
static tU32 g_u32ResetCounter  = 0xFFFFFFFF;

/******************************************************************************/

int main(int argc, char** argv)
{
	::testing::InitGoogleMock(&argc, argv);
	return RUN_ALL_TESTS();
}
/******************************************************************************/

OSAL_tThreadID My_OSAL_ThreadSpawn(const OSAL_trThreadAttribute* pcorAttr)
{
	g_rModuleData.u8IncMsgThreadState  = DEV_WUP_C_U8_THREAD_RUNNING;
	g_rModuleData.u8OsalMsgThreadState = DEV_WUP_C_U8_THREAD_RUNNING;
	g_rModuleData.u8ClientThreadState  = DEV_WUP_C_U8_THREAD_NOT_INSTALLED;

	return 0x00000001;
}

/* -------------------------------------------------------------------------- */

tVoid My_OSAL_vSetErrorCode(tU32 u32ErrorCode)
{
	g_u32OsalErrorCode = u32ErrorCode;
}

/* -------------------------------------------------------------------------- */

tU32 My_OSAL_u32ErrorCode(tVoid)
{
	return g_u32OsalErrorCode;
}

/* -------------------------------------------------------------------------- */

tString szGetRegistryValueName(intptr_t arg)
{
	return (tString)((OSAL_trIOCtrlRegistry*)arg)->pcos8Name;
}

/* -------------------------------------------------------------------------- */

tVoid vSetResetCounterTo1(tPS8 ps8ResetCounter)
{
	*((tPU32)ps8ResetCounter) = 0xCCFE01AA;
}

/* -------------------------------------------------------------------------- */

tVoid vGetResetCounter(tPCS8 pcs8ResetCounter)
{
	g_u32ResetCounter = *((tPU32)pcs8ResetCounter);
}

/******************************************************************************/

ACTION_P(My_OSAL_vSetErrorCode_Action, u32ErrorCode)
{
	My_OSAL_vSetErrorCode(u32ErrorCode);
}

/* -------------------------------------------------------------------------- */

ACTION_P(DevWupTest_vSetThreadStates, u8ThreadState)
{
	g_rModuleData.u8IncMsgThreadState  = u8ThreadState;
	g_rModuleData.u8OsalMsgThreadState = u8ThreadState;
	g_rModuleData.u8ClientThreadState  = u8ThreadState;
}

/* -------------------------------------------------------------------------- */

ACTION_P(DevWupTest_vSetSystemShutdownSuccess, bState)
{
	g_rModuleData.prGlobalData->bSystemShutdownSuccess = bState;
}

/* -------------------------------------------------------------------------- */

ACTION_P(DevWupTest_vSetAcknowledgedWakeupReasonsMask, u32AcknowledgedWakeupReasonsMask)
{
	g_rModuleData.prGlobalData->u32AcknowledgedWakeupReasonsMask = u32AcknowledgedWakeupReasonsMask;
}

/* -------------------------------------------------------------------------- */

ACTION_P(DevWupTest_vSetConnectToFakeIncDeviceMagic, u32ConnectToFakeIncDeviceMagic)
{
	g_rModuleData.u32ConnectToFakeIncDeviceMagic = u32ConnectToFakeIncDeviceMagic;
}

/* -------------------------------------------------------------------------- */

ACTION_P(DevWupTest_vSetLatestResetProcessor, u8LatestResetProcessor)
{
	g_rModuleData.prGlobalData->u8LatestResetProcessor = u8LatestResetProcessor;
}

/******************************************************************************/

void DevWupTest::SetUp()
{
	m_rPCB.id          = DEV_WUP_C_S32_OSAL_PROCESS_ID;
	m_rPCB.szName      = (tString)"dev_wup_utest_process";
	m_rPCB.startTime   = 0;
	m_rPCB.runningTime = 0;

	m_rTCB.id          = DEV_WUP_C_S32_OSAL_THREAD_ID;
	m_rTCB.szName      = (tString)"dev_wup_utest_thread";

	m_acIncLocalAddress[0] = 192;
	m_acIncLocalAddress[1] = 168;
	m_acIncLocalAddress[2] = 223;
	m_acIncLocalAddress[3] = 1;
	
	m_apIncLocalAddressList[0] = m_acIncLocalAddress;

	m_rhostent_IncLocal.h_name      = NULL;
	m_rhostent_IncLocal.h_aliases   = NULL;
	m_rhostent_IncLocal.h_addrtype  = AF_INET;
	m_rhostent_IncLocal.h_length    = 4;
	m_rhostent_IncLocal.h_addr_list = m_apIncLocalAddressList;

	m_acIncRemoteAddress[0] = 192;
	m_acIncRemoteAddress[1] = 168;
	m_acIncRemoteAddress[2] = 223;
	m_acIncRemoteAddress[3] = 2;
	
	m_apIncRemoteAddressList[0] = m_acIncRemoteAddress;

	m_rhostent_IncRemote.h_name      = NULL;
	m_rhostent_IncRemote.h_aliases   = NULL;
	m_rhostent_IncRemote.h_addrtype  = AF_INET;
	m_rhostent_IncRemote.h_length    = 4;
	m_rhostent_IncRemote.h_addr_list = m_apIncRemoteAddressList;

	m_rdirent_Undervoltage.d_type = DT_LNK;

	strncpy(
		m_rdirent_Undervoltage.d_name,
		"/sys/bus/platform/drivers/undervoltage/undervoltage.22/trigger",
		sizeof(m_rdirent_Undervoltage.d_name));

	memset(
		&m_rGlobalData,
		0,
		sizeof(m_rGlobalData));

	g_rModuleData.prGlobalData = &m_rGlobalData;

	// Expect calls for m_oOsalMock

	EXPECT_CALL(m_oOsalMock, ClockGetElapsedTime())
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oOsalMock, s32ClockGetTime(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, ProcessWhoAmI())
		.WillRepeatedly(Return(DEV_WUP_C_S32_OSAL_PROCESS_ID));

	EXPECT_CALL(m_oOsalMock, s32ProcessControlBlock(_, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(m_rPCB), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(_, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SharedMemoryClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SharedMemoryDelete(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreOpen(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreDelete(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SemaphorePost(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, SharedMemoryCreate(_, _, _))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, pvSharedMemoryMap(_, _, _, _))
		.WillRepeatedly(Return((tVoid*)&m_rGlobalData));

	EXPECT_CALL(m_oOsalMock, s32SharedMemoryUnmap(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, SharedMemoryOpen(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IOClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32IORead(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32IOWrite(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SaveNPrintFormat(_, _, _))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, szSaveStringNCopy(_, _, _))
		.WillRepeatedly(WithArgs<0,1,2>(Invoke(strncpy)));

	EXPECT_CALL(m_oOsalMock, s32EventCreate(_, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32EventOpen(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32EventClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32EventDelete(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, coszErrorText(_))
		.WillRepeatedly(Return("DUMMY ERROR STRING"));

	EXPECT_CALL(m_oOsalMock, s32MessageQueueCreate(_, _, _, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32MessageQueueClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32MessageQueueDelete(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32MessageQueueOpen(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32MessageQueuePost(_, _, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, ThreadSpawn(_))
		.WillRepeatedly(Invoke(My_OSAL_ThreadSpawn));

	EXPECT_CALL(m_oOsalMock, ThreadWhoAmI())
		.WillRepeatedly(Return(DEV_WUP_C_S32_OSAL_THREAD_ID));

	EXPECT_CALL(m_oOsalMock, s32ThreadControlBlock(_, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(m_rTCB), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32IOControl(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32IOControl(_, OSAL_C_S32_IOCTRL_REGGETVALUE, ResultOf(szGetRegistryValueName, StrEq("CONNECT_TO_FAKE"))))
		.WillRepeatedly(DoAll(DevWupTest_vSetConnectToFakeIncDeviceMagic(DEV_WUP_C_U32_CONNECT_TO_FAKE_INC_DEVICE_MAGIC), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, vSetErrorCode(_))
		.WillRepeatedly(WithArg<0>(Invoke(My_OSAL_vSetErrorCode)));

	EXPECT_CALL(m_oOsalMock, u32ErrorCode())
		.WillRepeatedly(Invoke(My_OSAL_u32ErrorCode));

	EXPECT_CALL(m_oOsalMock, s32ThreadWait(_))
		.WillRepeatedly(Return(OSAL_OK));

	// Expect calls for m_oDevWupMock

	EXPECT_CALL(m_oDevWupMock, open_mock(_, _))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevWupMock, close_mock(_))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevWupMock, write_mock(_, _, _))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevWupMock, opendir_mock("/sys/bus/platform/drivers/undervoltage"))
		.WillRepeatedly(Return((DIR*)1));

	EXPECT_CALL(m_oDevWupMock, closedir_mock(_))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevWupMock, readdir_mock((DIR*)1))
		.WillRepeatedly(Return(&m_rdirent_Undervoltage));

	EXPECT_CALL(m_oDevWupMock, socket_mock(_, _, _))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevWupMock, setsockopt_mock(_, _, _, _, _))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevWupMock, bind_mock(_, _, _))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevWupMock, connect_mock(_, _, _))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevWupMock, gethostbyname_mock("fake0-local"))
		.WillRepeatedly(Return(&m_rhostent_IncLocal));

	EXPECT_CALL(m_oDevWupMock, gethostbyname_mock("fake0"))
		.WillRepeatedly(Return(&m_rhostent_IncRemote));

	EXPECT_CALL(m_oDevWupMock, gethostbyname_mock("scc-local"))
		.WillRepeatedly(Return(&m_rhostent_IncLocal));

	EXPECT_CALL(m_oDevWupMock, gethostbyname_mock("scc"))
		.WillRepeatedly(Return(&m_rhostent_IncRemote));

	EXPECT_CALL(m_oDevWupMock, dgram_init(_, _, _))
		.WillRepeatedly(Return(&m_rsk_dgram));

	EXPECT_CALL(m_oDevWupMock, dgram_exit(_))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevWupMock, dgram_send(_, _, _))
		.WillRepeatedly(Return(1));
}

/* -------------------------------------------------------------------------- */

void DevWupTest::TearDown()
{


}

/* -------------------------------------------------------------------------- */

void DevWupTestIOControl::SetUp()
{
	DevWupTest::SetUp();

	DEV_WUP_OsalIO_u32Init();

	DEV_WUP_OsalIO_u32Open();
}

/* -------------------------------------------------------------------------- */

void DevWupTestIOControl::TearDown()
{
	DEV_WUP_OsalIO_u32Close();

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD, _))
		.WillRepeatedly(DoAll(DevWupTest_vSetThreadStates(DEV_WUP_C_U8_THREAD_SHUTTING_DOWN), Return(OSAL_OK)));

	DEV_WUP_OsalIO_u32Deinit();

	DevWupTest::TearDown();
}

/* -------------------------------------------------------------------------- */

void DevWupTestInternal::SetUp()
{
	DevWupTest::SetUp();

	DEV_WUP_OsalIO_u32Init();
}

/* -------------------------------------------------------------------------- */

void DevWupTestInternal::TearDown()
{
	EXPECT_CALL(m_oOsalMock, s32EventPost(_, DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD, _))
		.WillRepeatedly(DoAll(DevWupTest_vSetThreadStates(DEV_WUP_C_U8_THREAD_SHUTTING_DOWN), Return(OSAL_OK)));

	DEV_WUP_OsalIO_u32Deinit();

	DevWupTest::TearDown();
}

/******************************************************************************/
/******************************************************************************/
/*                                DevWupTest                                  */
/******************************************************************************/
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/* DEV_WUP_OsalIO_u32Init()                                                   */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_E_NOERROR)
{
	EXPECT_EQ(OSAL_E_NOERROR, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__UseIncFakeDevice__OSAL_E_NOERROR)
{
	DEV_WUP_OsalIO_u32Init();

	EXPECT_EQ(DEV_WUP_C_STRING_INC_HOST_LOCAL_FAKE, g_rModuleData.coszIncHostLocal);
	EXPECT_EQ(DEV_WUP_C_STRING_INC_HOST_REMOTE_FAKE, g_rModuleData.coszIncHostRemote);
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__UseRealIncDevice__OSAL_E_NOERROR)
{
	EXPECT_CALL(m_oOsalMock, s32IOControl(_, OSAL_C_S32_IOCTRL_REGGETVALUE, ResultOf(szGetRegistryValueName, StrEq("CONNECT_TO_FAKE"))))
		.WillRepeatedly(DoAll(DevWupTest_vSetConnectToFakeIncDeviceMagic(0x00000000), Return(OSAL_ERROR)));

	DEV_WUP_OsalIO_u32Init();

	EXPECT_EQ(DEV_WUP_C_STRING_INC_HOST_LOCAL,  g_rModuleData.coszIncHostLocal);
	EXPECT_EQ(DEV_WUP_C_STRING_INC_HOST_REMOTE, g_rModuleData.coszIncHostRemote);
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32ProcessControlBlock_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32ProcessControlBlock(_, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(m_rPCB), My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreCreate_DATA_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_DATA_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_SharedMemoryCreate_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, SharedMemoryCreate(_, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_pvSharedMemoryMap_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, pvSharedMemoryMap(_, _, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), ReturnNull()));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreCreate_WAKEUP_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_WAKEUP_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreCreate_STARTUP_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreCreate_CTRL_RESET_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreCreate_PROC_RESET_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreCreate_PWR_OFF_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreCreate_APP_STATE_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__DEV_WUP_u32CreateClientNotificationEvents_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SaveNPrintFormat(_, _, _))
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__DEV_WUP_bEstablishSccIncCom_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oDevWupMock, socket_mock(_, _, _))
		.WillRepeatedly(Return(-1));

	g_rModuleData.bSccIncCommunicationEstablished = TRUE;

	EXPECT_EQ(OSAL_E_NOERROR, DEV_WUP_OsalIO_u32Init());

	EXPECT_EQ(FALSE, g_rModuleData.bSccIncCommunicationEstablished);
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__DEV_WUP_u32InstallOsalMsgThread_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_OSAL_MSG_THREAD_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__DEV_WUP_u32InstallIncMsgThread_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_INC_MSG_THREAD_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32MessageQueuePost(_, Pointee(DEV_WUP_C_U8_OSAL_MSG_STOP_THREAD), _, _))
		.WillRepeatedly(DoAll(DevWupTest_vSetThreadStates(DEV_WUP_C_U8_THREAD_SHUTTING_DOWN), Return(OSAL_OK)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__DEV_WUP_u32UninstallOsalMsgThread_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_INC_MSG_THREAD_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32MessageQueuePost(_, Pointee(DEV_WUP_C_U8_OSAL_MSG_STOP_THREAD), _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__DEV_WUP_bReleaseSccIncCom_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_OSAL_MSG_THREAD_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oDevWupMock, dgram_exit(_))
		.WillRepeatedly(Return(-1));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreClose_AppStateMsgReceived_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_ONOFF_EVENT_ACK_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(2), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreClose(2))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreDelete_APP_STATE_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_ONOFF_EVENT_ACK_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreClose_PwrOffMsgReceived_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(2), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreClose(2))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreDelete_PWR_OFF_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_APP_STATE_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreClose_ProcResetMsgReceived_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(2), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreClose(2))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreDelete_PROC_RESET_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_PWR_OFF_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}


TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreClose_CtrlResetMsgReceived_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(2), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreClose(2))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreDelete_CTRL_RESET_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreClose_StartupMsgReceived_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(2), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreClose(2))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreDelete_STARTUP_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_CTRL_RESET_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreClose_WakeupReasonMsgReceived_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_WAKEUP_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(2), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreClose(2))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

TEST_F(DevWupTest, DEV_WUP_OsalIO_u32Init__OSAL_s32SemaphoreDelete_WAKEUP_MSG_RECEIVED_failed__OSAL_E_UNKNOWN)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(DEV_WUP_C_STRING_SEM_STARTUP_MSG_RECEIVED_NAME, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreDelete(DEV_WUP_C_STRING_SEM_WAKEUP_MSG_RECEIVED_NAME))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_UNKNOWN, DEV_WUP_OsalIO_u32Init());
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32Deinit()                                                        */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTest, DEV_WUP_u32Deinit__OSAL_E_NOERROR)
{
	DEV_WUP_OsalIO_u32Init();

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, DEV_WUP_C_U32_EVENT_MASK_STOP_THREAD, _))
		.WillRepeatedly(DoAll(DevWupTest_vSetThreadStates(DEV_WUP_C_U8_THREAD_SHUTTING_DOWN), Return(OSAL_OK)));

	EXPECT_EQ(OSAL_E_NOERROR, DEV_WUP_OsalIO_u32Deinit());
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32Open()                                                          */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTest, DEV_WUP_u32Open__OSAL_E_NOERROR)
{
	DEV_WUP_OsalIO_u32Init();

	EXPECT_EQ(OSAL_E_NOERROR, DEV_WUP_OsalIO_u32Open());
}

TEST_F(DevWupTest, DEV_WUP_u32Open__OSAL_E_NOTINITIALIZED)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreOpen(DEV_WUP_C_STRING_SEM_DATA_NAME, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_DOESNOTEXIST), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_NOTINITIALIZED, DEV_WUP_OsalIO_u32Open());
}

/******************************************************************************/
/******************************************************************************/
/*                           DevWupTestIOControl                              */
/******************************************************************************/
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32Control()                                                       */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__SHUTDOWN__OSAL_E_NOERROR)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreOpen(DEV_WUP_C_STRING_SEM_SHUTDOWN_MSG_RECEIVED_NAME, _))
		.WillRepeatedly(DoAll(DevWupTest_vSetSystemShutdownSuccess(TRUE), Return(OSAL_OK)));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_SHUTDOWN,
			DEV_WUP_SHUTDOWN_NORMAL));
}

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__GET_STARTTYPE__OSAL_E_NOERROR)
{
	tU8 u8Starttype;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_GET_STARTTYPE,
			(intptr_t)&u8Starttype));
}

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__GET_WAKEUP_REASON__OSAL_E_NOERROR)
{
	tU8 u8WakeupReason = DEV_WUP_C_U8_WAKEUP_REASON_UNKNOWN;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_GET_WAKEUP_REASON,
			(intptr_t)&u8WakeupReason));
}

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__REGISTER_CLIENT__OSAL_E_NOERROR)
{
	DEV_WUP_trClientRegistration rClientRegistration;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
			(intptr_t)&rClientRegistration));
}

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__UNREGISTER_CLIENT__OSAL_E_NOERROR)
{
	DEV_WUP_trClientRegistration rClientRegistration;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
			(intptr_t)&rClientRegistration));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT,
			(intptr_t)rClientRegistration.u32ClientId));
}

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__UNREGISTER_CLIENT__OSAL_E_DOESNOTEXIST)
{
	tU32 u32ClientId = 1;

	EXPECT_EQ(
		OSAL_E_DOESNOTEXIST,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT,
			(intptr_t)u32ClientId));
}

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__GET_APPLICATION_MODE__OSAL_E_NOERROR)
{
	tU8 u8ApplicationMode;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_GET_APPLICATION_MODE,
			(intptr_t)&u8ApplicationMode));
}

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__GET_AP_SUPERVISION_ERROR__OSAL_E_NOERROR)
{
	tU8 u8APSupervisionError;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_GET_AP_SUPERVISION_ERROR,
			(intptr_t)&u8APSupervisionError));
}

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__REGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION__OSAL_E_NOERROR)
{
	DEV_WUP_trClientRegistration rClientRegistration;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
			(intptr_t)&rClientRegistration));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_REGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION,
			(intptr_t)rClientRegistration.u32ClientId));
}

TEST_F(DevWupTestIOControl, DEV_WUP_u32Control__UNREGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION__OSAL_E_NOERROR)
{
	DEV_WUP_trClientRegistration rClientRegistration;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
			(intptr_t)&rClientRegistration));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_REGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION,
			(intptr_t)rClientRegistration.u32ClientId));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Control(
			OSAL_C_S32_IOCTRL_WUP_UNREGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION,
			(intptr_t)rClientRegistration.u32ClientId));
}

/******************************************************************************/
/******************************************************************************/
/*                             DevWupTestInternal                            */
/******************************************************************************/
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/* DEV_WUP_vDetermineApplicationMode()                                        */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_vDetermineApplicationMode__NeitherFirstNorSecondFileExist__NORMAL)
{
	g_rModuleData.prGlobalData->u8ApplicationModeAP = DEV_WUP_C_U8_APPLICATION_MODE_UNKNOWN;

	EXPECT_CALL(m_oOsalMock, IOOpen("/dev/root/etc/bosch_rootfs_initramfs.id", OSAL_EN_READONLY))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_DOESNOTEXIST), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, IOOpen("/dev/root/etc/bosch_rootfs_initramfs-v2.id", OSAL_EN_READONLY))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_DOESNOTEXIST), Return(OSAL_ERROR)));

	DEV_WUP_vDetermineApplicationMode();

	EXPECT_EQ(g_rModuleData.prGlobalData->u8ApplicationModeAP, DEV_WUP_C_U8_APPLICATION_MODE_NORMAL);
}

TEST_F(DevWupTestInternal, DEV_WUP_vDetermineApplicationMode__FirstFileExistsButAccessReportsAnError__NORMAL)
{
	g_rModuleData.prGlobalData->u8ApplicationModeAP = DEV_WUP_C_U8_APPLICATION_MODE_UNKNOWN;

	EXPECT_CALL(m_oOsalMock, IOOpen("/dev/root/etc/bosch_rootfs_initramfs.id", OSAL_EN_READONLY))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	DEV_WUP_vDetermineApplicationMode();

	EXPECT_EQ(g_rModuleData.prGlobalData->u8ApplicationModeAP, DEV_WUP_C_U8_APPLICATION_MODE_NORMAL);
}

TEST_F(DevWupTestInternal, DEV_WUP_vDetermineApplicationMode__FirstFileDoesNotExistAndSecondFileExistsButAccessReportsAnError__NORMAL)
{
	g_rModuleData.prGlobalData->u8ApplicationModeAP = DEV_WUP_C_U8_APPLICATION_MODE_UNKNOWN;

	EXPECT_CALL(m_oOsalMock, IOOpen("/dev/root/etc/bosch_rootfs_initramfs.id", OSAL_EN_READONLY))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_DOESNOTEXIST), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, IOOpen("/dev/root/etc/bosch_rootfs_initramfs-v2.id", OSAL_EN_READONLY))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	DEV_WUP_vDetermineApplicationMode();

	EXPECT_EQ(g_rModuleData.prGlobalData->u8ApplicationModeAP, DEV_WUP_C_U8_APPLICATION_MODE_NORMAL);
}

TEST_F(DevWupTestInternal, DEV_WUP_vDetermineApplicationMode__FirstFileExistsButSecondFileNot__DOWNLOAD)
{
	g_rModuleData.prGlobalData->u8ApplicationModeAP = DEV_WUP_C_U8_APPLICATION_MODE_UNKNOWN;

	EXPECT_CALL(m_oOsalMock, IOOpen("/dev/root/etc/bosch_rootfs_initramfs.id", OSAL_EN_READONLY))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, IOOpen("/dev/root/etc/bosch_rootfs_initramfs-v2.id", OSAL_EN_READONLY))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_DOESNOTEXIST), Return(OSAL_ERROR)));

	DEV_WUP_vDetermineApplicationMode();

	EXPECT_EQ(g_rModuleData.prGlobalData->u8ApplicationModeAP, DEV_WUP_C_U8_APPLICATION_MODE_DOWNLOAD);
}

TEST_F(DevWupTestInternal, DEV_WUP_vDetermineApplicationMode__SecondFileExistsButFirstFileNot__DOWNLOAD)
{
	g_rModuleData.prGlobalData->u8ApplicationModeAP = DEV_WUP_C_U8_APPLICATION_MODE_UNKNOWN;

	EXPECT_CALL(m_oOsalMock, IOOpen("/dev/root/etc/bosch_rootfs_initramfs.id", OSAL_EN_READONLY))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_DOESNOTEXIST), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, IOOpen("/dev/root/etc/bosch_rootfs_initramfs-v2.id", OSAL_EN_READONLY))
		.WillRepeatedly(Return(1));

	DEV_WUP_vDetermineApplicationMode();

	EXPECT_EQ(g_rModuleData.prGlobalData->u8ApplicationModeAP, DEV_WUP_C_U8_APPLICATION_MODE_DOWNLOAD);
}
/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32RegisterClient()                                                */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterClient__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterClient__PermanentSemaphoreLock__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, DEV_WUP_C_U32_SEM_DATA_TIMEOUT_MS))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterClient__DoubleRegistration__OSAL_E_MAXFILES)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	EXPECT_EQ(
		OSAL_E_MAXFILES,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32UnregisterClient()                                              */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32UnregisterClient__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32UnregisterClient(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32UnregisterClient__PermanentSemaphoreLock__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;
	tU32            u32ClientId    = 1;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, DEV_WUP_C_U32_SEM_DATA_TIMEOUT_MS))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_WUP_u32UnregisterClient(
			u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32UnregisterClient__WithoutPrecedingRegistration__OSAL_E_DOESNOTEXIST)
{
	OSAL_tSemHandle hSemDataAccess = 1;
	tU32            u32ClientId    = 1;

	EXPECT_EQ(
		OSAL_E_DOESNOTEXIST,
		DEV_WUP_u32UnregisterClient(
			u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_bEvaluateReceivedIncMsg()                                          */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_bEvaluateReceivedIncMsg__R_WAKEUP_REASON__TRUE)
{
	g_rModuleData.au8IncRcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_REASON;
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_WAKEUP_REASON_IGN_PIN;

	g_rModuleData.bIncMsgCatVersionMatches = TRUE;

	EXPECT_EQ(
		TRUE,
		DEV_WUP_bEvaluateReceivedIncMsg(DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_REASON));
}

TEST_F(DevWupTestInternal, DEV_WUP_bEvaluateReceivedIncMsg__UnknownMessage__FALSE)
{
	g_rModuleData.au8IncRcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_INVALID;

	EXPECT_EQ(
		FALSE,
		DEV_WUP_bEvaluateReceivedIncMsg(0xFF));
}

TEST_F(DevWupTestInternal, DEV_WUP_bEvaluateReceivedIncMsg__CatalogueVersionMismatch_UnknownVersion__FALSE)
{
	g_rModuleData.au8IncRcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_REASON;
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_WAKEUP_REASON_IGN_PIN;

	EXPECT_EQ(
		FALSE,
		DEV_WUP_bEvaluateReceivedIncMsg(DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_REASON));
}

TEST_F(DevWupTestInternal, DEV_WUP_bEvaluateReceivedIncMsg__CatalogueVersionMismatch_WrongVersion__FALSE)
{
	g_rModuleData.au8IncRcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_REASON;
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_WAKEUP_REASON_IGN_PIN;

	g_rModuleData.u8IncMsgCatMajorVersionServer = 255;
	g_rModuleData.u8IncMsgCatMinorVersionServer = 255;

	EXPECT_EQ(
		FALSE,
		DEV_WUP_bEvaluateReceivedIncMsg(DEV_WUP_C_U8_INC_MSGLEN_SPM_SPMS_R_WAKEUP_REASON));
}

TEST_F(DevWupTestInternal, DEV_WUP_bEvaluateReceivedIncMsg__WrongMessageLength__FALSE)
{
	g_rModuleData.au8IncRcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_REASON;
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_WAKEUP_REASON_IGN_PIN;

	g_rModuleData.bIncMsgCatVersionMatches = TRUE;

	EXPECT_EQ(
		FALSE,
		DEV_WUP_bEvaluateReceivedIncMsg(0xFF));
}

TEST_F(DevWupTestInternal, DEV_WUP_bEvaluateReceivedIncMsg__MessageNotYetSupported__FALSE)
{
	g_rModuleData.au8IncRcvBuffer[0] = DEV_WUP_C_U8_INC_MSGID_INVALID;

	g_rModuleData.bIncMsgCatVersionMatches = TRUE;

	EXPECT_EQ(
		FALSE,
		DEV_WUP_bEvaluateReceivedIncMsg(1));
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u8HandleIncMsg_RWakeupStateVector()                                */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupStateVector__DEV_WUP_C_U8_REJECT_OK)
{
	tU32 u32OnOffStates = ((0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_CAN) | (0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN));

	g_rModuleData.au8IncRcvBuffer[1] = (tU8)(u32OnOffStates >>  0);
	g_rModuleData.au8IncRcvBuffer[2] = (tU8)(u32OnOffStates >>  8);
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)(u32OnOffStates >> 16);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)(u32OnOffStates >> 24);

	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_OK,
		DEV_WUP_u8HandleIncMsg_RWakeupStateVector());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupStateVector__InvalidWakeupStateVector__DEV_WUP_C_U8_REJECT_INVALID_PARAMETER)
{
	tU32 u32OnOffStates = (0x00000001 << (DEV_WUP_C_U8_ONOFF_STATE_ILLUMINATION + 1));

	g_rModuleData.au8IncRcvBuffer[1] = (tU8)(u32OnOffStates >>  0);
	g_rModuleData.au8IncRcvBuffer[2] = (tU8)(u32OnOffStates >>  8);
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)(u32OnOffStates >> 16);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)(u32OnOffStates >> 24);

	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_INVALID_PARAMETER,
		DEV_WUP_u8HandleIncMsg_RWakeupStateVector());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupStateVector__PermanentLockFailure__DEV_WUP_C_U8_REJECT_NO_REASON)
{
	tU32 u32OnOffStates = ((0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_CAN) | (0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN));

	g_rModuleData.au8IncRcvBuffer[1] = (tU8)(u32OnOffStates >>  0);
	g_rModuleData.au8IncRcvBuffer[2] = (tU8)(u32OnOffStates >>  8);
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)(u32OnOffStates >> 16);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)(u32OnOffStates >> 24);

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_NO_REASON,
		DEV_WUP_u8HandleIncMsg_RWakeupStateVector());
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u8HandleIncMsg_RWakeupState()                                      */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupState__ChangedToActive__DEV_WUP_C_U8_REJECT_OK)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN;
	g_rModuleData.au8IncRcvBuffer[2] = 1;
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)((0x0001 & 0x00FF) >> 0);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)((0x0001 & 0xFF00) >> 8);

	g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 = 0x00000000;

	if (DEV_WUP_u8HandleIncMsg_RWakeupState() != DEV_WUP_C_U8_REJECT_OK) {
		ADD_FAILURE();
		return;
	}

	EXPECT_GT(
		g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 | (0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN),
		0);
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupState__ChangedToInactive__DEV_WUP_C_U8_REJECT_OK)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN;
	g_rModuleData.au8IncRcvBuffer[2] = 0;
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)((0x0001 & 0x00FF) >> 0);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)((0x0001 & 0xFF00) >> 8);

	g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 = (0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN);

	if (DEV_WUP_u8HandleIncMsg_RWakeupState() != DEV_WUP_C_U8_REJECT_OK) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(
		g_rModuleData.prGlobalData->rOnOffStates.uOnOffStates.u32 & (0x00000001 << DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN),
		0);
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupState__InvalidWakeupState__DEV_WUP_C_U8_REJECT_INVALID_PARAMETER)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_ONOFF_STATE_ILLUMINATION + 1;
	g_rModuleData.au8IncRcvBuffer[2] = 1;
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)((0x0001 & 0x00FF) >> 0);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)((0x0001 & 0xFF00) >> 8);
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_INVALID_PARAMETER,
		DEV_WUP_u8HandleIncMsg_RWakeupState());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupState__InvalidIsActiveState__DEV_WUP_C_U8_REJECT_INVALID_PARAMETER)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN;
	g_rModuleData.au8IncRcvBuffer[2] = 2;
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)((0x0001 & 0x00FF) >> 0);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)((0x0001 & 0xFF00) >> 8);
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_INVALID_PARAMETER,
		DEV_WUP_u8HandleIncMsg_RWakeupState());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupState__PermanentLockFailure__DEV_WUP_C_U8_REJECT_NO_REASON)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN;
	g_rModuleData.au8IncRcvBuffer[2] = 1;
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)((0x0001 & 0x00FF) >> 0);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)((0x0001 & 0xFF00) >> 8);

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_NO_REASON,
		DEV_WUP_u8HandleIncMsg_RWakeupState());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupState__NotifyOnOffReasonChangedFailure__DEV_WUP_C_U8_REJECT_OK)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_ONOFF_STATE_EXTERNAL_PIN;
	g_rModuleData.au8IncRcvBuffer[2] = 1;
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)((0x0001 & 0x00FF) >> 0);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)((0x0001 & 0xFF00) >> 8);

	ASSERT_EQ(OSAL_E_NOERROR, DEV_WUP_OsalIO_u32Open());

	ASSERT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_INVALIDVALUE), Return(OSAL_ERROR)));

	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_OK,
		DEV_WUP_u8HandleIncMsg_RWakeupState());
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u8HandleIncMsg_RWakeupReason()                                     */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupReason__DEV_WUP_C_U8_REJECT_OK)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_WAKEUP_REASON_EXTERNAL_PIN_WAKEUP;
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_OK,
		DEV_WUP_u8HandleIncMsg_RWakeupReason());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupReason__PermanentLockFailure__DEV_WUP_C_U8_REJECT_NO_REASON)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_NO_REASON,
		DEV_WUP_u8HandleIncMsg_RWakeupReason());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RWakeupReason__InvalidWakeupReason__DEV_WUP_C_U8_REJECT_INVALID_PARAMETER)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_WAKEUP_REASON_ILLUMINATION + 1;
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_INVALID_PARAMETER,
		DEV_WUP_u8HandleIncMsg_RWakeupReason());
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u8HandleIncMsg_RSetWakeupConfig()                                  */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RSetWakeupConfig__DEV_WUP_C_U8_REJECT_OK)
{
	g_rModuleData.au8IncRcvBuffer[1] = (tU8)((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP & 0x000000FF) >>  0);
	g_rModuleData.au8IncRcvBuffer[2] = (tU8)((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP & 0x0000FF00) >>  8);
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP & 0x00FF0000) >> 16);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP & 0xFF000000) >> 24);
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_OK,
		DEV_WUP_u8HandleIncMsg_RSetWakeupConfig());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RSetWakeupConfig__InvalidWakeupReasonIsTooLarge__DEV_WUP_C_U8_REJECT_INVALID_PARAMETER)
{
	g_rModuleData.au8IncRcvBuffer[1] = (tU8)(((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ILLUMINATION << 1) & 0x000000FF) >>  0);
	g_rModuleData.au8IncRcvBuffer[2] = (tU8)(((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ILLUMINATION << 1) & 0x0000FF00) >>  8);
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)(((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ILLUMINATION << 1) & 0x00FF0000) >> 16);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)(((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ILLUMINATION << 1) & 0xFF000000) >> 24);
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_INVALID_PARAMETER,
		DEV_WUP_u8HandleIncMsg_RSetWakeupConfig());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RSetWakeupConfig__PermanentLockFailure__DEV_WUP_C_U8_REJECT_NO_REASON)
{
	g_rModuleData.au8IncRcvBuffer[1] = (tU8)((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP & 0x000000FF) >>  0);
	g_rModuleData.au8IncRcvBuffer[2] = (tU8)((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP & 0x0000FF00) >>  8);
	g_rModuleData.au8IncRcvBuffer[3] = (tU8)((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP & 0x00FF0000) >> 16);
	g_rModuleData.au8IncRcvBuffer[4] = (tU8)((DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP & 0xFF000000) >> 24);

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_NO_REASON,
		DEV_WUP_u8HandleIncMsg_RSetWakeupConfig());
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u8HandleIncMsg_RAPSupervisionError()                               */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RAPSupervisionError__DEV_WUP_C_U8_REJECT_OK)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN;
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_OK,
		DEV_WUP_u8HandleIncMsg_RAPSupervisionError());

	EXPECT_EQ(
		DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN,
		g_rModuleData.prGlobalData->u8APSupervisionError);
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RAPSupervisionError__InvalidSupervisionError__DEV_WUP_C_U8_REJECT_INVALID_PARAMETER)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_NONE;
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_INVALID_PARAMETER,
		DEV_WUP_u8HandleIncMsg_RAPSupervisionError());
}

TEST_F(DevWupTestInternal, DEV_WUP_u8HandleIncMsg_RAPSupervisionError__PermanentLockFailure__DEV_WUP_C_U8_REJECT_NO_REASON)
{
	g_rModuleData.au8IncRcvBuffer[1] = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));
	
	EXPECT_EQ(
		DEV_WUP_C_U8_REJECT_NO_REASON,
		DEV_WUP_u8HandleIncMsg_RAPSupervisionError());
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32RegisterOnOffReasonChanged()                                    */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__StatesAndEventsWithPastOnes__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;
	DEV_WUP_trOnOffEventHistory              rOnOffEventHistory;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	DEV_WUP_vStoreOnOffEvent(1, 1);
	DEV_WUP_vStoreOnOffEvent(2, 2);

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	My_OSAL_vSetErrorCode(OSAL_E_UNKNOWN);

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY | DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_NOERROR), Return(OSAL_OK)));

	if (DEV_WUP_u32RegisterOnOffReasonChanged(
		&rOnOffReasonChangedRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	if (OSAL_u32ErrorCode() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	DEV_WUP_vStoreOnOffEvent(3, 3);

	rOnOffEventHistory.u32ClientId = rClientRegistration.u32ClientId;

	DEV_WUP_u32GetOnOffEvents(
		&rOnOffEventHistory,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	if (rOnOffEventHistory.u8NumberOfOnOffEvents != 3) {
			ADD_FAILURE();
			return;
	}

	if (rOnOffEventHistory.arOnOffEvent[0].u8Event != 1) {
			ADD_FAILURE();
			return;
	}

	if (rOnOffEventHistory.arOnOffEvent[0].u16MsgHandle != 1) {
			ADD_FAILURE();
			return;
	}

	if (rOnOffEventHistory.arOnOffEvent[1].u8Event != 2) {
			ADD_FAILURE();
			return;
	}

	if (rOnOffEventHistory.arOnOffEvent[1].u16MsgHandle != 2) {
			ADD_FAILURE();
			return;
	}

	if (rOnOffEventHistory.arOnOffEvent[2].u8Event != 3) {
			ADD_FAILURE();
			return;
	}

	if (rOnOffEventHistory.arOnOffEvent[2].u16MsgHandle != 3) {
			ADD_FAILURE();
			return;
	}
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__StatesAndEventsWithoutPastOnes__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;
	DEV_WUP_trOnOffEventHistory              rOnOffEventHistory;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	DEV_WUP_vStoreOnOffEvent(1, 1);
	DEV_WUP_vStoreOnOffEvent(2, 2);

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITHOUT_PAST_ONES;

	My_OSAL_vSetErrorCode(OSAL_E_UNKNOWN);

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_NOERROR), Return(OSAL_OK)));

	if (DEV_WUP_u32RegisterOnOffReasonChanged(
		&rOnOffReasonChangedRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	if (OSAL_u32ErrorCode() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	DEV_WUP_vStoreOnOffEvent(3, 3);

	rOnOffEventHistory.u32ClientId = rClientRegistration.u32ClientId;

	DEV_WUP_u32GetOnOffEvents(
		&rOnOffEventHistory,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	if (rOnOffEventHistory.u8NumberOfOnOffEvents != 1) {
			ADD_FAILURE();
			return;
	}

	if (rOnOffEventHistory.arOnOffEvent[0].u8Event != 3) {
			ADD_FAILURE();
			return;
	}

	if (rOnOffEventHistory.arOnOffEvent[0].u16MsgHandle != 3) {
			ADD_FAILURE();
			return;
	}
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__PermanentSemaphoreLock__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, DEV_WUP_C_U32_SEM_DATA_TIMEOUT_MS))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__InvalidNotificationModeValue__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = 255;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__DoubleSystemMasterRegistration__OSAL_E_BUSY)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration1;
	DEV_WUP_trClientRegistration             rClientRegistration2;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration1,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration2,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration1.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	if (DEV_WUP_u32RegisterOnOffReasonChanged(
		&rOnOffReasonChangedRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration2.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	EXPECT_EQ(
		OSAL_E_BUSY,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__InvalidSystemMasterValue__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = 2;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__WithUnknownClientId__OSAL_E_DOESNOTEXIST)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	rOnOffReasonChangedRegistration.u32ClientId        = 1;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	EXPECT_EQ(
		OSAL_E_DOESNOTEXIST,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__DoubleOnOffReasonChangedRegistration_OSAL_E_ALREADYEXISTS)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = FALSE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	EXPECT_EQ(
		OSAL_E_ALREADYEXISTS,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__EventOpenFailed__OSAL_E_UNKNOWN)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;
	DEV_WUP_trOnOffEventHistory              rOnOffEventHistory;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	EXPECT_CALL(m_oOsalMock, s32EventOpen(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__EventPostFailed__OSAL_E_UNKNOWN)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;
	DEV_WUP_trOnOffEventHistory              rOnOffEventHistory;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterOnOffReasonChanged__EventCloseFailed__OSAL_E_UNKNOWN)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;
	DEV_WUP_trOnOffEventHistory              rOnOffEventHistory;

	if (DEV_WUP_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_WUP_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	EXPECT_CALL(m_oOsalMock, s32EventClose(_))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32UnregisterOnOffReasonChanged()                                  */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32UnregisterOnOffReasonChanged__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	ASSERT_EQ(OSAL_E_NOERROR, DEV_WUP_OsalIO_u32Open());

	ASSERT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	ASSERT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32UnregisterOnOffReasonChanged(
			rOnOffReasonChangedRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32UnregisterOnOffReasonChanged__PermanentSemaphoreLock__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, DEV_WUP_C_U32_SEM_DATA_TIMEOUT_MS))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_WUP_u32UnregisterOnOffReasonChanged(
			1,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32UnregisterOnOffReasonChanged__NotRegistered__OSAL_E_DOESNOTEXIST)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	ASSERT_EQ(OSAL_E_NOERROR, DEV_WUP_OsalIO_u32Open());

	ASSERT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_DOESNOTEXIST,
		DEV_WUP_u32UnregisterOnOffReasonChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32NotifyOnOffReasonChanged()                                      */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32NotifyOnOffReasonChanged__OnOffReasonEvent__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	ASSERT_EQ(OSAL_E_NOERROR, DEV_WUP_OsalIO_u32Open());

	ASSERT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	ASSERT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32NotifyOnOffReasonChanged(DEV_WUP_C_U8_ONOFF_REASON_TYPE_EVENT));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32NotifyOnOffReasonChanged__OnOffReasonState__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration             rClientRegistration;
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration;

	ASSERT_EQ(OSAL_E_NOERROR, DEV_WUP_OsalIO_u32Open());

	ASSERT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES;

	ASSERT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterOnOffReasonChanged(
			&rOnOffReasonChangedRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_INVALIDVALUE), Return(OSAL_ERROR)));

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32NotifyOnOffReasonChanged(DEV_WUP_C_U8_ONOFF_REASON_TYPE_STATE));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32NotifyOnOffReasonChanged__PermanentSemaphoreLock__OSAL_E_TIMEOUT)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, DEV_WUP_C_U32_SEM_DATA_TIMEOUT_MS))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_WUP_u32NotifyOnOffReasonChanged(DEV_WUP_C_U8_ONOFF_REASON_TYPE_STATE));
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32ResetProcessor()                                                */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32ResetProcessor__ENTIRE_SYSTEM_with_RESET_REASON_DIAGNOSIS__OSAL_E_NOERROR)
{
	OSAL_tSemHandle              hSemDataAccess      = 1;
	DEV_WUP_trResetProcessorInfo rResetProcessorInfo = {
							     DEV_WUP_C_U8_ENTIRE_SYSTEM,
							     DEV_WUP_C_U8_RESET_MODE_LOGGED,
							     DEV_WUP_C_U16_RESET_REASON_DIAGNOSIS,
							     10
							   };

	g_u32ResetCounter = 0xFFFFFFFF;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreOpen(DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME, _))
		.WillRepeatedly(DoAll(DevWupTest_vSetLatestResetProcessor(DEV_WUP_C_U8_ENTIRE_SYSTEM), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32IORead(_, _, sizeof(tU32)))
		.WillRepeatedly(DoAll(WithArg<1>(Invoke(vSetResetCounterTo1)) , Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32IOWrite(_, _, sizeof(tU32)))
		.WillRepeatedly(DoAll(WithArg<1>(Invoke(vGetResetCounter)) , Return(OSAL_OK)));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32ResetProcessor(
			&rResetProcessorInfo,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		0xCCFF00AA,
		g_u32ResetCounter);
}

TEST_F(DevWupTestInternal, DEV_WUP_u32ResetProcessor__SYSTEM_COMMUNICATION_CONTROLLER_with_RESET_REASON_DIAGNOSIS__OSAL_E_NOERROR)
{
	OSAL_tSemHandle              hSemDataAccess      = 1;
	DEV_WUP_trResetProcessorInfo rResetProcessorInfo = {
							     DEV_WUP_C_U8_SYSTEM_COMMUNICATION_CONTROLLER,
							     DEV_WUP_C_U8_RESET_MODE_LOGGED,
							     DEV_WUP_C_U16_RESET_REASON_DIAGNOSIS,
							     10
							   };

	g_u32ResetCounter = 0xFFFFFFFF;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreOpen(DEV_WUP_C_STRING_SEM_PROC_RESET_MSG_RECEIVED_NAME, _))
		.WillRepeatedly(DoAll(DevWupTest_vSetLatestResetProcessor(DEV_WUP_C_U8_SYSTEM_COMMUNICATION_CONTROLLER), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32IORead(_, _, sizeof(tU32)))
		.WillRepeatedly(DoAll(WithArg<1>(Invoke(vSetResetCounterTo1)) , Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32IOWrite(_, _, sizeof(tU32)))
		.WillRepeatedly(DoAll(WithArg<1>(Invoke(vGetResetCounter)) , Return(OSAL_OK)));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32ResetProcessor(
			&rResetProcessorInfo,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		0xCCFF00AA,
		g_u32ResetCounter);
}

TEST_F(DevWupTestInternal, DEV_WUP_u32ResetProcessor__APPLICATION_PROCESSOR_with_RESET_REASON_DIAGNOSIS__OSAL_E_NOERROR)
{
	OSAL_tSemHandle              hSemDataAccess      = 1;
	DEV_WUP_trResetProcessorInfo rResetProcessorInfo = {
							     DEV_WUP_C_U8_APPLICATION_PROCESSOR,
							     DEV_WUP_C_U8_RESET_MODE_LOGGED,
							     DEV_WUP_C_U16_RESET_REASON_DIAGNOSIS,
							     10
							   };

	g_u32ResetCounter = 0xFFFFFFFF;

	EXPECT_CALL(m_oOsalMock, s32IORead(_, _, sizeof(tU32)))
		.WillRepeatedly(DoAll(WithArg<1>(Invoke(vSetResetCounterTo1)) , Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32IOWrite(_, _, sizeof(tU32)))
		.WillRepeatedly(DoAll(WithArg<1>(Invoke(vGetResetCounter)) , Return(OSAL_OK)));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32ResetProcessor(
			&rResetProcessorInfo,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		0xCCFF00AA,
		g_u32ResetCounter);
}

TEST_F(DevWupTestInternal, DEV_WUP_u32ResetProcessor__APPLICATION_PROCESSOR_with_RESET_REASON_UNSPECIFIED__OSAL_E_NOERROR)
{
	OSAL_tSemHandle              hSemDataAccess      = 1;
	DEV_WUP_trResetProcessorInfo rResetProcessorInfo = {
							     DEV_WUP_C_U8_APPLICATION_PROCESSOR,
							     DEV_WUP_C_U8_RESET_MODE_LOGGED,
							     DEV_WUP_C_U16_RESET_REASON_UNSPECIFIED,
							     10
							   };

	g_u32ResetCounter = 0xFFFFFFFF;

	EXPECT_CALL(m_oOsalMock, s32IORead(_, _, sizeof(tU32)))
		.WillRepeatedly(DoAll(WithArg<1>(Invoke(vSetResetCounterTo1)) , Return(OSAL_OK)));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32ResetProcessor(
			&rResetProcessorInfo,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		0xFFFFFFFF,
		g_u32ResetCounter);
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32DecreaseApplicationProcessorResetCounter()                        */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32DecreaseApplicationProcessorResetCounter__OSAL_E_NOERROR)
{
	tU8 au8ValidResetCounterWithValue1[] = { 0xAA, 0x01, 0xFE, 0xCC };

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(m_oOsalMock, IOOpen(OSAL_C_STRING_DEVICE_PRAM"/reset_counter", OSAL_EN_READWRITE))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IORead(1, _, _))
		.WillRepeatedly(DoAll(SetArrayArgument<1>(au8ValidResetCounterWithValue1, au8ValidResetCounterWithValue1 + sizeof(au8ValidResetCounterWithValue1)), Return(sizeof(au8ValidResetCounterWithValue1))));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32DecreaseApplicationProcessorResetCounter());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32DecreaseApplicationProcessorResetCounter__IOOpenFailed__OSAL_E_DOESNOTEXIST)
{
	EXPECT_CALL(m_oOsalMock, IOOpen(OSAL_C_STRING_DEVICE_PRAM"/reset_counter", OSAL_EN_READWRITE))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_DOESNOTEXIST), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_DOESNOTEXIST,
		DEV_WUP_u32DecreaseApplicationProcessorResetCounter());
}


TEST_F(DevWupTestInternal, DEV_WUP_u32DecreaseApplicationProcessorResetCounter__FirstIOControlPRAMSeekFailed__OSAL_E_UNKNOWN)
{
	tU8 au8ValidResetCounterWithValue1[] = { 0xAA, 0x01, 0xFE, 0xCC };

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(m_oOsalMock, IOOpen(OSAL_C_STRING_DEVICE_PRAM"/reset_counter", OSAL_EN_READWRITE))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IORead(1, _, _))
		.WillRepeatedly(DoAll(SetArrayArgument<1>(au8ValidResetCounterWithValue1, au8ValidResetCounterWithValue1 + sizeof(au8ValidResetCounterWithValue1)), Return(sizeof(au8ValidResetCounterWithValue1))));

	EXPECT_CALL(m_oOsalMock, s32IOControl(1, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 0))
		.Times(1)
		.WillOnce(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32DecreaseApplicationProcessorResetCounter());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32DecreaseApplicationProcessorResetCounter__SecondIOControlPRAMSeekFailed__OSAL_E_UNKNOWN)
{
	tU8 au8ValidResetCounterWithValue1[] = { 0xAA, 0x01, 0xFE, 0xCC };

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(m_oOsalMock, IOOpen(OSAL_C_STRING_DEVICE_PRAM"/reset_counter", OSAL_EN_READWRITE))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IORead(1, _, _))
		.WillRepeatedly(DoAll(SetArrayArgument<1>(au8ValidResetCounterWithValue1, au8ValidResetCounterWithValue1 + sizeof(au8ValidResetCounterWithValue1)), Return(sizeof(au8ValidResetCounterWithValue1))));

	EXPECT_CALL(m_oOsalMock, s32IOControl(1, OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK, 0))
		.Times(2)
		.WillOnce(Return(OSAL_OK))
		.WillOnce(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32DecreaseApplicationProcessorResetCounter());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32DecreaseApplicationProcessorResetCounter__ReadResetCounterFailed__OSAL_E_UNKNOWN)
{
	tU8 au8ValidResetCounterWithValue1[] = { 0xAA, 0x01, 0xFE, 0xCC };

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(m_oOsalMock, IOOpen(OSAL_C_STRING_DEVICE_PRAM"/reset_counter", OSAL_EN_READWRITE))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IORead(1, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32DecreaseApplicationProcessorResetCounter());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32DecreaseApplicationProcessorResetCounter__InvalidEnclosingMagic__OSAL_E_UNKNOWN)
{
	tU8 au8InvalidResetCounterWithWrongEnclosingMagic[] = { 0xAB, 0x00, 0x00, 0xCD };

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(m_oOsalMock, IOOpen(OSAL_C_STRING_DEVICE_PRAM"/reset_counter", OSAL_EN_READWRITE))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IORead(1, _, _))
		.WillRepeatedly(DoAll(SetArrayArgument<1>(au8InvalidResetCounterWithWrongEnclosingMagic, au8InvalidResetCounterWithWrongEnclosingMagic + sizeof(au8InvalidResetCounterWithWrongEnclosingMagic)), Return(sizeof(au8InvalidResetCounterWithWrongEnclosingMagic))));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32DecreaseApplicationProcessorResetCounter());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32DecreaseApplicationProcessorResetCounter__InvalidInvertedResetCounterValue__OSAL_E_UNKNOWN)
{
	tU8 au8InvalidResetCounterWithWrongInvertedCounterValue[] = { 0xAA, 0x01, 0xFD, 0xCC };

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(m_oOsalMock, IOOpen(OSAL_C_STRING_DEVICE_PRAM"/reset_counter", OSAL_EN_READWRITE))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IORead(1, _, _))
		.WillRepeatedly(DoAll(SetArrayArgument<1>(au8InvalidResetCounterWithWrongInvertedCounterValue, au8InvalidResetCounterWithWrongInvertedCounterValue + sizeof(au8InvalidResetCounterWithWrongInvertedCounterValue)), Return(sizeof(au8InvalidResetCounterWithWrongInvertedCounterValue))));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32DecreaseApplicationProcessorResetCounter());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32DecreaseApplicationProcessorResetCounter__WriteResetCounterFailed__OSAL_E_UNKNOWN)
{
	tU8 au8ValidResetCounterWithValue1[] = { 0xAA, 0x01, 0xFE, 0xCC };

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(m_oOsalMock, IOOpen(OSAL_C_STRING_DEVICE_PRAM"/reset_counter", OSAL_EN_READWRITE))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IORead(1, _, _))
		.WillRepeatedly(DoAll(SetArrayArgument<1>(au8ValidResetCounterWithValue1, au8ValidResetCounterWithValue1 + sizeof(au8ValidResetCounterWithValue1)), Return(sizeof(au8ValidResetCounterWithValue1))));

	EXPECT_CALL(m_oOsalMock, s32IOWrite(1, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32DecreaseApplicationProcessorResetCounter());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32DecreaseApplicationProcessorResetCounter__IOCloseFailed__OSAL_E_UNKNOWN)
{
	tU8 au8ValidResetCounterWithValue1[] = { 0xAA, 0x01, 0xFE, 0xCC };

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(m_oOsalMock, IOOpen(OSAL_C_STRING_DEVICE_PRAM"/reset_counter", OSAL_EN_READWRITE))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IORead(1, _, _))
		.WillRepeatedly(DoAll(SetArrayArgument<1>(au8ValidResetCounterWithValue1, au8ValidResetCounterWithValue1 + sizeof(au8ValidResetCounterWithValue1)), Return(sizeof(au8ValidResetCounterWithValue1))));

	EXPECT_CALL(m_oOsalMock, s32IOClose(1))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32DecreaseApplicationProcessorResetCounter());
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32ConfigureWakeupReasons()                                        */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32ConfigureWakeupReasons__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	tU32 u32WakeupConfiguration = DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ON_TIPPER | DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(DevWupTest_vSetAcknowledgedWakeupReasonsMask(u32WakeupConfiguration), Return(OSAL_OK)));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32ConfigureWakeupReasons(
			u32WakeupConfiguration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32ConfigureWakeupReasons__InvalidWakeupReasonIsTooLarge__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	tU32 u32WakeupConfiguration = DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ILLUMINATION << 1;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_WUP_u32ConfigureWakeupReasons(
			u32WakeupConfiguration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32ConfigureWakeupReasons__PermanentLockFailure__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	tU32 u32WakeupConfiguration = DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ON_TIPPER | DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_WUP_u32ConfigureWakeupReasons(
			u32WakeupConfiguration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32ConfigureWakeupReasons__SemaphoreOpenFailed__OSAL_E_UNKNOWN)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	tU32 u32WakeupConfiguration = DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ON_TIPPER | DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreOpen(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32ConfigureWakeupReasons(
			u32WakeupConfiguration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32ConfigureWakeupReasons__SemaphoreWaitFailed__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	tU32 u32WakeupConfiguration = DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ON_TIPPER | DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_WUP_u32ConfigureWakeupReasons(
			u32WakeupConfiguration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32ConfigureWakeupReasons__NonMatchingResponseFromScc__OSAL_E_UNKNOWN)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	tU32 u32WakeupConfiguration = DEV_WUP_C_U32_WAKEUP_REASONS_MASK_ON_TIPPER | DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP;

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32ConfigureWakeupReasons(
			u32WakeupConfiguration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32NotifyAPSupervisionErrorChanged()                               */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32NotifyAPSupervisionErrorChanged__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, DEV_WUP_C_U32_EVENT_MASK_AP_SUPERVISION_ERROR_CHANGED_NOTIFY, _))
		.Times(1)
		.WillOnce(Return(OSAL_OK));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32NotifyAPSupervisionErrorChanged());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32NotifyAPSupervisionErrorChanged__NotRegistered_OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, _, _))
		.Times(0);

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32NotifyAPSupervisionErrorChanged());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32NotifyAPSupervisionErrorChanged__PermanentLockFailure_OSAL_E_TIMEOUT)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_WUP_u32NotifyAPSupervisionErrorChanged());
}

TEST_F(DevWupTestInternal, DEV_WUP_u32NotifyAPSupervisionErrorChanged__EvenPostFailed_OSAL_E_UNKNOWN)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, DEV_WUP_C_U32_EVENT_MASK_AP_SUPERVISION_ERROR_CHANGED_NOTIFY, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32NotifyAPSupervisionErrorChanged());
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32GetAPSupervisionError()                                         */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32GetAPSupervisionError__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess       = 1;
	tU8             u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_NONE;

	g_rModuleData.prGlobalData->u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN;
	
	DEV_WUP_u32GetAPSupervisionError(
		&u8APSupervisionError,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	EXPECT_EQ(
		DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN,
		u8APSupervisionError);
}

TEST_F(DevWupTestInternal, DEV_WUP_u32GetAPSupervisionError__PermanentLockFailure__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess       = 1;
	tU8             u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_NONE;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_WUP_u32GetAPSupervisionError(
			&u8APSupervisionError,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32RegisterAPSupervisionErrorChanged()                             */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterAPSupervisionErrorChanged__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	g_rModuleData.prGlobalData->u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterAPSupervisionErrorChanged__DoubleRegistration__OSAL_E_ALREADYEXISTS)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	g_rModuleData.prGlobalData->u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_ALREADYEXISTS,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterAPSupervisionErrorChanged__UnknownClientId__OSAL_E_DOESNOTEXIST)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	tU32 u32ClientId = 1;

	g_rModuleData.prGlobalData->u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN;

	EXPECT_EQ(
		OSAL_E_DOESNOTEXIST,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterAPSupervisionErrorChanged__EventOpenFailed__OSAL_E_UNKNOWN)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	g_rModuleData.prGlobalData->u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_CALL(m_oOsalMock, s32EventOpen(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterAPSupervisionErrorChanged__EventPostFailed__OSAL_E_UNKNOWN)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	g_rModuleData.prGlobalData->u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, _, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32RegisterAPSupervisionErrorChanged__EventCloseFailed__OSAL_E_UNKNOWN)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	g_rModuleData.prGlobalData->u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_SYSTEM_ON_WITHOUT_A_REASON_SHUTDOWN;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_CALL(m_oOsalMock, s32EventClose(_))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_UNKNOWN), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_UNKNOWN,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_WUP_u32UnregisterAPSupervisionErrorChanged()                           */
/*----------------------------------------------------------------------------*/

TEST_F(DevWupTestInternal, DEV_WUP_u32UnregisterAPSupervisionErrorChanged__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_WUP_trClientRegistration rClientRegistration;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_OsalIO_u32Open());

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32RegisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_WUP_u32UnregisterAPSupervisionErrorChanged(
			rClientRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevWupTestInternal, DEV_WUP_u32UnregisterAPSupervisionErrorChanged__UnknownClientId__OSAL_E_DOESNOTEXIST)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	tU32 u32ClientId = 1;

	EXPECT_EQ(
		OSAL_E_DOESNOTEXIST,
		DEV_WUP_u32UnregisterAPSupervisionErrorChanged(
			u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/******************************************************************************/
