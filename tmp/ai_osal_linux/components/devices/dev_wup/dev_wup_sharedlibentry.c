/*******************************************************************************
*
* FILE:         dev_wup_sharedlibentry.h
*
* SW-COMPONENT: Device Wakeu-Up
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Process attach and detach functions to let the /dev/wup
*               operate as a shared library.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#define DEV_WUP_IMPORT_INTERFACE_OSAL
#include "dev_wup_if.h"

/******************************************************************************/
/*                                                                            */
/* DEFINES                                                                    */
/*                                                                            */
/******************************************************************************/

#define DEV_WUP_C_U8_TRACE_TYPE_STRING                                      0x01

#define DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH                                128

/******************************************************************************/
/*                                                                            */
/* GLOBAL VARIABLES                                                           */
/*                                                                            */
/******************************************************************************/

static sem_t* g_hSemLock = SEM_FAILED;
static tS32   g_hShMem = -1;
static tS32*  g_ps32CreatorProcessId = MAP_FAILED;

/******************************************************************************/
/*                                                                            */
/* LOCAL FUNCTIONS                                                            */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Shared library attach function which is called by OSAL for each process at
* the first call of OSAL_IOOpen() for /dev/wup.
*
*******************************************************************************/
void __attribute__ ((constructor)) wup_process_attach(void)
{
	tU8 au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];

	tBool bIsCreatorProcess = FALSE;

	OSAL_trProcessControlBlock rProcessControlBlock;

	rProcessControlBlock.szName = NULL;
	rProcessControlBlock.id     = 0;

	OSAL_s32ProcessControlBlock(OSAL_ProcessWhoAmI(), &rProcessControlBlock);

	g_hSemLock = sem_open("WUP_INIT",O_EXCL | O_CREAT, 0660, 0);

	if (g_hSemLock != SEM_FAILED) {
		if(s32OsalGroupId) {
			if(chown("/dev/shm/sem.WUP_INIT",(uid_t)-1,(gid_t)s32OsalGroupId) == -1)
				vWritePrintfErrmem("wup_process_attach  -> chown error %d \n",errno);

			if(chmod("/dev/shm/sem.WUP_INIT", OSAL_ACCESS_RIGTHS) == -1)
				vWritePrintfErrmem("wup_process_attach chmod -> fchmod error %d \n",errno);
		}
		
		g_hShMem = shm_open("WUP_INIT", O_EXCL|O_RDWR|O_CREAT|O_TRUNC, OSAL_ACCESS_RIGTHS);

		if (g_hShMem == -1) {
			FATAL_M_ASSERT_ALWAYS();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}

		if (s32OsalGroupId) {
			if (fchown(g_hShMem, (uid_t)-1, (gid_t)s32OsalGroupId) == -1)
				vWritePrintfErrmem("Attach shm_open -> fchown error %d",errno);
			// umask (022) is overwriting the permissions on creation, so we have to set it again
			if (fchmod(g_hShMem, OSAL_ACCESS_RIGTHS) == -1)
				vWritePrintfErrmem("Attach shm_open -> fchmod error %d",errno);
		}

		if (ftruncate(g_hShMem, sizeof(tU32)) == -1) {
			FATAL_M_ASSERT_ALWAYS();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}

		g_ps32CreatorProcessId = (tS32*) mmap(
			NULL,
			sizeof(tU32),
			PROT_READ | PROT_WRITE,
			MAP_SHARED,
			g_hShMem,
			0);

		if (g_ps32CreatorProcessId == MAP_FAILED) {
			FATAL_M_ASSERT_ALWAYS();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}

		bIsCreatorProcess = TRUE;
	} else {
		g_hSemLock = sem_open("WUP_INIT", 0);

		if (g_hSemLock == SEM_FAILED) {
			FATAL_M_ASSERT_ALWAYS();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}

		sem_wait(g_hSemLock);

		g_hShMem = shm_open("WUP_INIT", O_RDWR ,0);

		if (g_hShMem  == -1) {
			FATAL_M_ASSERT_ALWAYS();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}

		g_ps32CreatorProcessId = (tS32*) mmap(
			NULL,
			sizeof(tU32),
			PROT_READ | PROT_WRITE,
			MAP_SHARED,
			g_hShMem,
			0);

		if (g_ps32CreatorProcessId == MAP_FAILED) {
			FATAL_M_ASSERT_ALWAYS();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}
	}

	au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_STRING;

	(tVoid)OSALUTIL_s32SaveNPrintFormat(
		(char*)(&(au8TraceSendBuffer[1])),
		sizeof(au8TraceSendBuffer)-1,
		"wup_process_attach() => Library attached to process '%.15s' (#%d).%s",
		(rProcessControlBlock.szName != NULL) ? rProcessControlBlock.szName : "UNKNOWN",
		rProcessControlBlock.id,
		bIsCreatorProcess ? " (INIT)" : "");

	LLD_vTrace(
		TR_CLASS_DEV_WUP,
		TR_LEVEL_USER_4,
		au8TraceSendBuffer,
		(tU32)(OSAL_u32StringLength(au8TraceSendBuffer) + 1U));

	if (bIsCreatorProcess == TRUE) {
		*g_ps32CreatorProcessId = rProcessControlBlock.id;
		DEV_WUP_OsalIO_u32Init();
	}

error_out:

	if (g_hSemLock != SEM_FAILED)
		sem_post(g_hSemLock);
}

/*******************************************************************************
*
* Shared library detach function which is called by OSAL at the time the last
* registered client of the /dev/wup has called the related OSAL_IOClose()
* function.
*
*******************************************************************************/
void __attribute__((destructor)) wup_process_detach(void)
{
	tU8 au8TraceSendBuffer[DEV_WUP_C_U8_TRACE_SEND_BUFFER_LENGTH];

	OSAL_trProcessControlBlock rProcessControlBlock;

	if ((g_ps32CreatorProcessId == MAP_FAILED) ||
	    (g_hShMem  == -1)                      ||
	    (g_hSemLock == SEM_FAILED)               ) {
		FATAL_M_ASSERT_ALWAYS();
		return;
	}

	rProcessControlBlock.szName = NULL;
	rProcessControlBlock.id     = 0;

	OSAL_s32ProcessControlBlock(OSAL_ProcessWhoAmI(), &rProcessControlBlock);

	au8TraceSendBuffer[0] = DEV_WUP_C_U8_TRACE_TYPE_STRING;

	(tVoid)OSALUTIL_s32SaveNPrintFormat(
		(char*)(&(au8TraceSendBuffer[1])),
		sizeof(au8TraceSendBuffer)-1,
		"wup_process_detach() => Library detached from process '%.15s' (#%d).%s",
		(rProcessControlBlock.szName != NULL) ? rProcessControlBlock.szName : "UNKNOWN",
		rProcessControlBlock.id,
		((*g_ps32CreatorProcessId) == rProcessControlBlock.id) ? " (DE-INIT)" : "");

	LLD_vTrace(
		TR_CLASS_DEV_WUP,
		TR_LEVEL_USER_4,
		au8TraceSendBuffer,
		(tU32)(OSAL_u32StringLength(au8TraceSendBuffer) + 1U));

	if ((*g_ps32CreatorProcessId) == rProcessControlBlock.id) {

		DEV_WUP_OsalIO_u32Deinit();

		munmap(g_ps32CreatorProcessId, sizeof(tU32));
		shm_unlink("WUP_INIT");

		sem_close(g_hSemLock);
		sem_unlink("WUP_INIT");
	}
}

/******************************************************************************/

