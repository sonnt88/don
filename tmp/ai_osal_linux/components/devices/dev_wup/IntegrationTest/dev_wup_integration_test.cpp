/*******************************************************************************
*
* FILE:         dev_wup_integration_test.cpp
*
* SW-COMPONENT: Device Wakeup
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Integration tests.
*
* AUTHOR:       CM-AI/PJ-CF33-Kalms
*
* COPYRIGHT:    (c) 2013 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

/* ---------------------------- Linux standard ------------------------------ */
#include <errno.h>
#include <stdio.h>
/* ------------------ Linux socket, dgram-service and INC ------------------- */
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "inc.h"
#include "inc_ports.h"
#include "dgram_service.h"
/* ------------------------------ Google-Test ------------------------------- */
#include "gmock/gmock.h"
#include "gtest/gtest.h"
/* ---------------------------------- OSAL ---------------------------------- */
extern "C"
{
  #undef __cplusplus 
    #define OSAL_S_IMPORT_INTERFACE_GENERIC
    #include "osal_if.h"
  #define __cplusplus
}
/* ------------------------------ Wakeup Device ----------------------------- */
extern "C"
{
  #undef __cplusplus 
    #define DEV_WUP_IMPORT_INTERFACE_GENERIC
    #include "../dev_wup_if.h"
  #define __cplusplus
}
/* ---------------------------------- Self ---------------------------------- */
#include "dev_wup_integration_test.h"

/******************************************************************************/
/*                                                                            */
/* DEFINES                                                                    */
/*                                                                            */
/******************************************************************************/

#define DEV_WUP_CONF_C_U32_INC_MESSAGE_PROCESS_TIME_MS                       100
#define DEV_WUP_CONF_C_U32_IO_CONTROL_PROCESS_TIME_MS                        100

/* --------------------------------------------------------------------------- */

#define DEV_WUP_TEST_C_STRING_CLIENT_PROCESS_PATH\
		"./dev_wup_client_process_out.out"
#define DEV_WUP_TEST_C_STRING_CLIENT_PROCESS_NAME\
		"dev_wup_client_process_out.out"

#define DEV_WUP_C_U32_SEM_CLIENT_PROCESS_TIMEOUT_MS                         1000

/* --------------------------------------------------------------------------- */

#define DEV_WUP_TEST_C_U8_INC_MSG_CAT_MAJOR_VERSION_NUMBER_SCC              0x02
#define DEV_WUP_TEST_C_U8_INC_MSG_CAT_MINOR_VERSION_NUMBER_SCC              0x08

#define DEV_WUP_TEST_C_U8_INC_MSG_BUFFER_LENGTH                               64

/* --------------------------------------------------------------------------- */

#define DEV_WUP_TEST_C_STRING_INC_HOST_LOCAL_FAKE                  "fake1-local"
#define DEV_WUP_TEST_C_STRING_INC_HOST_REMOTE_FAKE                       "fake1"

#define DEV_WUP_TEST_C_U32_INC_MSG_RECEIVED_TIMEOUT_S                          1
#define DEV_WUP_TEST_C_U32_INC_MSG_RECEIVED_TIMEOUT_US                         0
/* --------------------------------------------------------------------------- */

#define DEV_WUP_TEST_C_STRING_MSG_HANDLER_THREAD_NAME               "DevWupTest"
#define DEV_WUP_TEST_C_S32_MSG_HANDLER_THREAD_STK_SIZE                     10000
#define DEV_WUP_TEST_C_U32_MSG_HANDLER_THREAD_PRIO                          0x4A

/* --------------------------------------------------------------------------- */

#define DEV_WUP_TEST_C_STRING_MSG_HANDLER_THREAD_EVENT_NAME         "DevWupTest"

#define DEV_WUP_TEST_C_STRING_MSG_HANDLER_THREAD_QUEUE_NAME         "DevWupTest"
#define DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MAX_MSGS                            32
#define DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MSG_LEN                             64

#define DEV_WUP_TEST_C_U8_OSAL_MSG_STOP_THREAD                              0x01
#define DEV_WUP_TEST_C_U8_OSAL_MSG_ACTIVATE_SINGLE_INC_MSG_RECEPTION        0x02

/* --------------------------------------------------------------------------- */

#define DEV_WUP_TEST_C_STRING_SEM_MSG_HANDLER_THREAD_NAME      "DevWupTestThread"

#define DEV_WUP_TEST_C_U32_SEM_THREAD_TIMEOUT_MS                             2000

/* --------------------------------------------------------------------------- */

#define DEV_WUP_TEST_C_STRING_SEM_FIRST_MSGS_SYNC_NAME           "DevWupTestMsgs"

#define DEV_WUP_TEST_C_U32_SEM_FIRST_MSGS_SYNC_TIMEOUT_MS                    4000

/* --------------------------------------------------------------------------- */

#define DEV_WUP_TEST_C_U8_THREAD_NOT_INSTALLED                                 0
#define DEV_WUP_TEST_C_U8_THREAD_RUNNING                                       1
#define DEV_WUP_TEST_C_U8_THREAD_SHUTTING_DOWN                                 2
#define DEV_WUP_TEST_C_U8_THREAD_OFF                                           3

/******************************************************************************/
/*                                                                            */
/* GLOBAL VARIABLES                                                           */
/*                                                                            */
/******************************************************************************/

static DevWupEnvironment* g_poDevWupEnvironment;

/******************************************************************************/
/*                                                                            */
/* PUBLIC FUNCTIONS                                                           */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Main function which attaches a global test environment to be able to execute
* SetUp() and TearDown() methods at the beginning and at the end of all tests
* and which starts the execution of the tests via the RUN_ALL_TESTS() macro.
*
*******************************************************************************/
extern "C"
{
	void vStartApp(int argc, char **argv)
	{
		g_poDevWupEnvironment = new DevWupEnvironment;

		::testing::AddGlobalTestEnvironment(g_poDevWupEnvironment);

		::testing::InitGoogleMock(&argc, argv);
		
		RUN_ALL_TESTS();
    }
} // extern "C"

/******************************************************************************/
/*                                                                            */
/* METHODS                                                                    */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Constructor for class DevWupFixture.
*
*******************************************************************************/
DevWupFixture::DevWupFixture(): 
	m_hIODescWupDriver(OSAL_ERROR),
	m_poDevWupEnvironment(g_poDevWupEnvironment)
{
}

/*******************************************************************************
*
* Destructor for class DevWupFixture.
*
*******************************************************************************/
DevWupFixture::~DevWupFixture()
{
}

/*******************************************************************************
*
* Constructor for class DevWupEnvironment.
*
*******************************************************************************/
DevWupEnvironment::DevWupEnvironment() :
	m_nSocketFileDescriptorInc(-1),
	m_pDatagramSocketDescriptor(NULL),
	m_hSemMsgHandlerThread(OSAL_C_INVALID_HANDLE),
	m_hSemFirstMsgsSync(OSAL_C_INVALID_HANDLE),
	m_hSemClientProcessStart(OSAL_C_INVALID_HANDLE),
	m_hSemClientProcessStop(OSAL_C_INVALID_HANDLE),
	m_hEventMsgHandlerThread(OSAL_C_INVALID_HANDLE),
	m_hQueueMsgHandlerThread(OSAL_C_INVALID_HANDLE),
	m_u8MsgHandlerThreadState(DEV_WUP_TEST_C_U8_THREAD_NOT_INSTALLED),
	m_bSccIncComEstablished(FALSE),
	m_u16OnOffEventMsgHandle(0),
	m_u16OnOffStateMsgHandle(0)
{
	OSAL_pvMemorySet(
		m_achIncRcvBuffer,
		0,
		sizeof(m_achIncRcvBuffer));
}

/*******************************************************************************
*
* Destructor for class DevWupEnvironment.
*
*******************************************************************************/
DevWupEnvironment::~DevWupEnvironment()
{
}

/*******************************************************************************
*
* Constructor for class DevWupFixtureOsal.
*
*******************************************************************************/
DevWupFixtureOsal::DevWupFixtureOsal()
{
}

/*******************************************************************************
*
* Destructor for class DevWupFixtureOsal.
*
*******************************************************************************/
DevWupFixtureOsal::~DevWupFixtureOsal()
{
}

/*******************************************************************************
*
* SetUp method for the entire test environment, which is executed once before
* all tests.
*
*******************************************************************************/
tVoid DevWupEnvironment::SetUp(tVoid)
{
	OSAL_trProcessAttribute rProcessAttribute = { 0 };
	tInt                    nIncMsgLength     = 0;

	rProcessAttribute.szAppName   = (tString)DEV_WUP_TEST_C_STRING_CLIENT_PROCESS_PATH;
	rProcessAttribute.szName      = (tString)DEV_WUP_TEST_C_STRING_CLIENT_PROCESS_NAME;
	rProcessAttribute.u32Priority = 100;

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_TEST_C_STRING_SEM_CLIENT_PROCESS_START_NAME,
		&m_hSemClientProcessStart,
		(tU32) 0) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	if ((m_bSccIncComEstablished = bInitSccIncCom()) == FALSE) {
		ADD_FAILURE();
		return;
	}

	if (bInstallMsgHandlerThread() == FALSE) {
		ADD_FAILURE();
		return;
	}

	// Invalidate the PRAM of the /dev/wup to set the value of the latest acknowledged
	// on/off event message handle to 0 to allow the function bIncSend_RWakeupEvent()
	// to again start with a message handle of 1 if the test is repeated.
	if (bInvalidateDevWupPRAM() == FALSE) {
		ADD_FAILURE();
		return;
	}

	if (OSAL_ProcessSpawn(&rProcessAttribute) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	if (OSAL_s32SemaphoreWait(
			m_hSemClientProcessStart,
			DEV_WUP_C_U32_SEM_CLIENT_PROCESS_TIMEOUT_MS) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_TEST_C_STRING_SEM_CLIENT_PROCESS_STOP_NAME,
		&m_hSemClientProcessStop) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	nIncMsgLength = dgram_recv(
		m_pDatagramSocketDescriptor,
		m_achIncRcvBuffer,
		sizeof(m_achIncRcvBuffer));

	if ((nIncMsgLength        == -1)                                                         ||
	    (m_achIncRcvBuffer[0] != DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_INDICATE_CLIENT_APP_STATE)  ) {
		ADD_FAILURE();
		return;
	}

	nIncMsgLength = dgram_recv(
		m_pDatagramSocketDescriptor,
		m_achIncRcvBuffer,
		sizeof(m_achIncRcvBuffer));

	if ((nIncMsgLength        == -1)                                 ||
	    (m_achIncRcvBuffer[0] != DEV_WUP_C_U8_INC_MSGID_C_GET_DATA)  ) {
		ADD_FAILURE();
		return;
	}

	if (bIncSend_RIndicateClientAppState(
		DEV_WUP_C_U8_APPLICATION_MODE_NORMAL,
		DEV_WUP_C_U8_RESET_REASON_COLDSTART,
		DEV_WUP_TEST_C_U8_INC_MSG_CAT_MAJOR_VERSION_NUMBER_SCC,
		DEV_WUP_TEST_C_U8_INC_MSG_CAT_MINOR_VERSION_NUMBER_SCC,
		DEV_WUP_C_U8_MSG_CAT_VERSION_CHECK_RESULT_OK) == FALSE) {
		ADD_FAILURE();
		return;
	}

	if (bIncSend_RStartupInfo(DEV_WUP_C_U8_STARTTYPE_COLDSTART) == FALSE)
		ADD_FAILURE();

	return;
}

/*******************************************************************************
*
* TearDown method for the entire test environment, which is executed once after
* all tests.
*
*******************************************************************************/
tVoid DevWupEnvironment::TearDown(tVoid)
{
	if (m_u8MsgHandlerThreadState == DEV_WUP_TEST_C_U8_THREAD_RUNNING)
		EXPECT_TRUE(bUninstallMsgHandlerThread());

	if (m_bSccIncComEstablished == TRUE)
		EXPECT_TRUE(bExitSccIncCom());

	if (m_hSemClientProcessStart != OSAL_C_INVALID_HANDLE) {
		if (OSAL_s32SemaphoreClose(m_hSemClientProcessStart) == OSAL_OK) {
			if (OSAL_s32SemaphoreDelete(DEV_WUP_TEST_C_STRING_SEM_CLIENT_PROCESS_START_NAME) == OSAL_ERROR)
				ADD_FAILURE();
		} else {
			ADD_FAILURE();
		}
	}

	if (m_hSemClientProcessStop != OSAL_C_INVALID_HANDLE) {
		if (OSAL_s32SemaphorePost(m_hSemClientProcessStop) == OSAL_ERROR)
			ADD_FAILURE();

		if (OSAL_s32SemaphoreClose(m_hSemClientProcessStop) == OSAL_ERROR)
			ADD_FAILURE();
	}

	// Wait until the 'client' process is terminated BEFORE this 'integration_test'
	// process terminates because the OSAL will drop its core resources when the
	// process which initially loaded the OSAL library is terminated. The termination
	// of the 'client' process will lead to a de-initialization and termination of
	// the /dev/wup.

	// The duration for the delay must be longer than 3000 ms because the /dev/wup
	// has an active 1000 ms delay inside the shutdown sequence of the two central
	// threads 'OsalMsgThread' and 'IncMsgThread'. 
	OSAL_s32ThreadWait(3000);
}

/*******************************************************************************
*
* Method to establish the INC communication with the /dev/wup.
*
*******************************************************************************/
tBool DevWupEnvironment::bInitSccIncCom(tVoid)
{
	tBool bResult = FALSE;

	m_nSocketFileDescriptorInc = socket(AF_BOSCH_INC_AUTOSAR, (tInt)SOCK_STREAM, 0);

	if (m_nSocketFileDescriptorInc == -1)
		return FALSE;

	struct timeval rSocketReceiveTimeval;
	rSocketReceiveTimeval.tv_sec  = DEV_WUP_TEST_C_U32_INC_MSG_RECEIVED_TIMEOUT_S;
	rSocketReceiveTimeval.tv_usec = DEV_WUP_TEST_C_U32_INC_MSG_RECEIVED_TIMEOUT_US;

	if (setsockopt(m_nSocketFileDescriptorInc, SOL_SOCKET, SO_RCVTIMEO, (char*)&rSocketReceiveTimeval, sizeof(rSocketReceiveTimeval)) == -1) {
		ADD_FAILURE();
		goto error_set_sock_opt; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	m_pDatagramSocketDescriptor = dgram_init(m_nSocketFileDescriptorInc, DGRAM_MAX, NULL);

	if (m_pDatagramSocketDescriptor == NULL) {
		ADD_FAILURE();
		goto error_dgram_init; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	struct hostent *prHostEntLocal;
	struct hostent *prHostEntRemote;
	struct sockaddr_in rSockAddrLocal;
	struct sockaddr_in rSockAddrRemote;

	prHostEntLocal = gethostbyname(DEV_WUP_TEST_C_STRING_INC_HOST_LOCAL_FAKE);

	if (prHostEntLocal == NULL) {
		ADD_FAILURE();
		goto error_get_host_by_name_local; /*lint !e801, authorized LINT-deactivation #<71> */
	}
	
	rSockAddrLocal.sin_family = AF_INET;
	memcpy((char *) &rSockAddrLocal.sin_addr.s_addr, (char *) prHostEntLocal->h_addr, (size_t)prHostEntLocal->h_length);
	rSockAddrLocal.sin_port = (tU16)htons(SPM_PORT);

	prHostEntRemote = gethostbyname(DEV_WUP_TEST_C_STRING_INC_HOST_REMOTE_FAKE);

	if (prHostEntRemote == NULL) {
		ADD_FAILURE();
		goto error_get_host_by_name_remote; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rSockAddrRemote.sin_family = AF_INET;
	memcpy((char *) &rSockAddrRemote.sin_addr.s_addr, (char *) prHostEntRemote->h_addr, (size_t)prHostEntRemote->h_length);
	rSockAddrRemote.sin_port = (tU16)htons(SPM_PORT);

	if (bind(
		m_nSocketFileDescriptorInc,
		(struct sockaddr *) &rSockAddrLocal, 
		sizeof(rSockAddrLocal)) == -1) {
		ADD_FAILURE();
		goto error_bind; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (connect(
		m_nSocketFileDescriptorInc,
		(struct sockaddr *) &rSockAddrRemote, 
		sizeof(rSockAddrRemote)) == -1) {
		ADD_FAILURE();
		goto error_connect; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	return TRUE;

error_connect:
error_bind:
error_get_host_by_name_remote:
error_get_host_by_name_local:
	(tVoid) dgram_exit(m_pDatagramSocketDescriptor);

error_dgram_init:
error_set_sock_opt:
	(tVoid) close(m_nSocketFileDescriptorInc);

	return FALSE;
}

/*******************************************************************************
*
* Method to release the INC communication with the /dev/wup.
*
*******************************************************************************/
tBool DevWupEnvironment::bExitSccIncCom(tVoid)
{
	tBool bResult = TRUE;

	if (dgram_exit(m_pDatagramSocketDescriptor) == -1) {
		ADD_FAILURE();
		bResult = FALSE;
	}

	if (close(m_nSocketFileDescriptorInc) == -1) {
		ADD_FAILURE();
		bResult = FALSE;
	}

	return bResult;
}

/*******************************************************************************
*
* Method to install the message handler thread for the INC communication with
* the /dev/wup.
*
*******************************************************************************/
tBool DevWupEnvironment::bInstallMsgHandlerThread(tVoid)
{
	OSAL_trThreadAttribute rThreadAttribute;

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_TEST_C_STRING_SEM_MSG_HANDLER_THREAD_NAME,
		&m_hSemMsgHandlerThread,
		(tU32) 0) == OSAL_ERROR) {
		ADD_FAILURE();
		return FALSE;
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_WUP_TEST_C_STRING_SEM_FIRST_MSGS_SYNC_NAME,
		&m_hSemFirstMsgsSync,
		(tU32) 0) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_sem_create; /*lint !e801, authorized LINT-deactivation #<71> */
	}


	if (OSAL_s32MessageQueueCreate(
		DEV_WUP_TEST_C_STRING_MSG_HANDLER_THREAD_QUEUE_NAME,
		DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MAX_MSGS,
		DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MSG_LEN,
		OSAL_EN_READWRITE, 
		&m_hQueueMsgHandlerThread) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_queue_create; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rThreadAttribute.szName       = (tString)DEV_WUP_TEST_C_STRING_MSG_HANDLER_THREAD_NAME;
	rThreadAttribute.s32StackSize = DEV_WUP_TEST_C_S32_MSG_HANDLER_THREAD_STK_SIZE;
	rThreadAttribute.u32Priority  = DEV_WUP_TEST_C_U32_MSG_HANDLER_THREAD_PRIO;
	rThreadAttribute.pfEntry      = (OSAL_tpfThreadEntry)vMsgHandlerThread;
	rThreadAttribute.pvArg        = (tPVoid)this;

	if (OSAL_ThreadSpawn(&rThreadAttribute) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_thread_spawn; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		m_hSemMsgHandlerThread,
		DEV_WUP_TEST_C_U32_SEM_THREAD_TIMEOUT_MS) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_sem_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (m_u8MsgHandlerThreadState != DEV_WUP_TEST_C_U8_THREAD_RUNNING) {
		ADD_FAILURE();
		goto error_thread_state; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	return TRUE;

error_thread_state:
error_sem_wait:
error_thread_spawn:
	if (OSAL_s32MessageQueueClose(m_hQueueMsgHandlerThread) == OSAL_OK)
		(tVoid)OSAL_s32MessageQueueDelete(DEV_WUP_TEST_C_STRING_MSG_HANDLER_THREAD_QUEUE_NAME);

error_queue_create:
	if (OSAL_s32SemaphoreClose(m_hSemFirstMsgsSync) == OSAL_OK)
		(tVoid)OSAL_s32SemaphoreDelete(DEV_WUP_TEST_C_STRING_SEM_FIRST_MSGS_SYNC_NAME);

error_sem_create:
	if (OSAL_s32SemaphoreClose(m_hSemMsgHandlerThread) == OSAL_OK)
		(tVoid)OSAL_s32SemaphoreDelete(DEV_WUP_TEST_C_STRING_SEM_MSG_HANDLER_THREAD_NAME);

	return FALSE;
}

/*******************************************************************************
*
* Method to un-install the message handler thread for the INC communication
* with the /dev/wup.
*
*******************************************************************************/
tBool DevWupEnvironment::bUninstallMsgHandlerThread(tVoid)
{
	tU8 au8MsqQueueBuffer[DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MSG_LEN];

	tBool bResult = TRUE;

	au8MsqQueueBuffer[0] = DEV_WUP_TEST_C_U8_OSAL_MSG_STOP_THREAD;

	if (OSAL_s32MessageQueuePost(
		m_hQueueMsgHandlerThread,
		au8MsqQueueBuffer, 
		DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MSG_LEN,
		OSAL_C_U32_MQUEUE_PRIORITY_LOWEST) == OSAL_ERROR) {
		ADD_FAILURE();
		bResult = FALSE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		m_hSemMsgHandlerThread,
		DEV_WUP_TEST_C_U32_SEM_THREAD_TIMEOUT_MS) == OSAL_ERROR) {
		ADD_FAILURE();
		bResult = FALSE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (m_u8MsgHandlerThreadState != DEV_WUP_TEST_C_U8_THREAD_SHUTTING_DOWN) {
		ADD_FAILURE();
		bResult = FALSE;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	OSAL_s32ThreadWait(1000); // Give thread 1 second to leave its own thread context.

	m_u8MsgHandlerThreadState = DEV_WUP_TEST_C_U8_THREAD_OFF;

error_out:

	if (OSAL_s32MessageQueueClose(m_hQueueMsgHandlerThread) == OSAL_OK)
		if (OSAL_s32MessageQueueDelete(DEV_WUP_TEST_C_STRING_MSG_HANDLER_THREAD_QUEUE_NAME) == OSAL_OK)
			m_hQueueMsgHandlerThread = OSAL_C_INVALID_HANDLE;

	if (OSAL_s32SemaphoreClose(m_hSemFirstMsgsSync) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_TEST_C_STRING_SEM_FIRST_MSGS_SYNC_NAME) == OSAL_OK)
			m_hSemFirstMsgsSync = OSAL_C_INVALID_HANDLE;

	if (OSAL_s32SemaphoreClose(m_hSemMsgHandlerThread) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_WUP_TEST_C_STRING_SEM_MSG_HANDLER_THREAD_NAME) == OSAL_OK)
			m_hSemMsgHandlerThread = OSAL_C_INVALID_HANDLE;

	return bResult;
}

/*******************************************************************************
*
* Method which represents the thread which handles the evaluation of received
* INC messages from the /dev/wup.
*
*******************************************************************************/
tVoid DevWupEnvironment::vMsgHandlerThread(tVoid* pvArg)
{
	tU8 au8MsqQueueBuffer[DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MSG_LEN];

	OSAL_tSemHandle    hSemMsgHandlerThread = OSAL_C_INVALID_HANDLE;
	tU32               u32MsgPrio           = OSAL_C_U32_MQUEUE_PRIORITY_LOWEST;
	DevWupEnvironment* poDevWupEnvironment  = (DevWupEnvironment*) pvArg;

	poDevWupEnvironment->m_u8MsgHandlerThreadState = DEV_WUP_TEST_C_U8_THREAD_NOT_INSTALLED;

	if (OSAL_s32SemaphoreOpen(
		DEV_WUP_TEST_C_STRING_SEM_MSG_HANDLER_THREAD_NAME,
		&hSemMsgHandlerThread) == OSAL_ERROR)
			return;

	poDevWupEnvironment->m_u8MsgHandlerThreadState = DEV_WUP_TEST_C_U8_THREAD_RUNNING;

	if (OSAL_s32SemaphorePost(hSemMsgHandlerThread) == OSAL_ERROR)
		poDevWupEnvironment->m_u8MsgHandlerThreadState = DEV_WUP_TEST_C_U8_THREAD_NOT_INSTALLED;

	while (poDevWupEnvironment->m_u8MsgHandlerThreadState == DEV_WUP_TEST_C_U8_THREAD_RUNNING) {
		if (OSAL_s32MessageQueueWait(
			poDevWupEnvironment->m_hQueueMsgHandlerThread,
			au8MsqQueueBuffer,
			DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MSG_LEN,
			&u32MsgPrio,
			OSAL_C_TIMEOUT_FOREVER) > 0)
				poDevWupEnvironment->vOnOsalMsgReceived(au8MsqQueueBuffer);
	}

	(tVoid) OSAL_s32SemaphoreClose(hSemMsgHandlerThread);
}

/*******************************************************************************
*
* Method which reacts to received OSAL messages.
*
*******************************************************************************/
tVoid DevWupEnvironment::vOnOsalMsgReceived(tU8* pu8MsqQueueBuffer)
{
	tU8  u8OsalMsgCode = *pu8MsqQueueBuffer;
	tInt nIncMsgLength = 0;

	if (DEV_WUP_TEST_C_U8_OSAL_MSG_STOP_THREAD == u8OsalMsgCode) {
		m_u8MsgHandlerThreadState = DEV_WUP_TEST_C_U8_THREAD_SHUTTING_DOWN;

		(tVoid)OSAL_s32SemaphorePost(m_hSemMsgHandlerThread);

	} else if (DEV_WUP_TEST_C_U8_OSAL_MSG_ACTIVATE_SINGLE_INC_MSG_RECEPTION == u8OsalMsgCode) {
			nIncMsgLength = dgram_recv(
				m_pDatagramSocketDescriptor,
				m_achIncRcvBuffer,
				sizeof(m_achIncRcvBuffer));

			if (nIncMsgLength != -1)
				vOnIncMsgReceived(nIncMsgLength);
	}
}

/*******************************************************************************
*
* Method which reacts to received INC messages.
*
*******************************************************************************/
tVoid DevWupEnvironment::vOnIncMsgReceived(tInt nIncMsgLength)
{
	switch (m_achIncRcvBuffer[0]) {
	case DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_SET_WAKEUP_CONFIG: {
		tU32 u32WakeupConfig = ((((tU32) m_achIncRcvBuffer[1]) <<  0) |
					(((tU32) m_achIncRcvBuffer[2]) <<  8) |
					(((tU32) m_achIncRcvBuffer[3]) << 16) |
					(((tU32) m_achIncRcvBuffer[4]) << 24)  );
		(tVoid)bIncSend_RSetWakeupConfig(u32WakeupConfig);
		break;
	}
	case DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_CTRL_RESET_EXECUTION: {
		(tVoid)bIncSend_RCtrlResetExecution();
		break;
	}
	case DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_STARTUP_FINISHED: {
		(tVoid)bIncSend_RStartupFinished();
		break;
	}
	case DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_EXTEND_POWER_OFF_TIMEOUT: {
		tU16 u16TimeoutS = ((((tU16)m_achIncRcvBuffer[1]) << 0) |
				    (((tU16)m_achIncRcvBuffer[2]) << 8)  );
		(tVoid)bIncSend_RExtendPowerOffTimeout(u16TimeoutS);
		break;
	}
	case DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_SHUTDOWN_IN_PROGRESS: {
		(tVoid)bIncSend_RShutdownInProgress();
		break;
	}
	case DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_PROC_RESET_REQUEST: {
		tU8 u8ProcId = m_achIncRcvBuffer[1];
		(tVoid)bIncSend_RProcResetRequest(u8ProcId);
		break;
	}
	case DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_WAKEUP_EVENT_ACK: {
		tU16 u16OnOffEventMsgHandle = ((((tU16)m_achIncRcvBuffer[2]) << 0) |
					       (((tU16)m_achIncRcvBuffer[3]) << 8)  );
		(tVoid)bIncSend_RWakeupEventAck(u16OnOffEventMsgHandle);
		break;
	}
	case DEV_WUP_C_U8_INC_MSGID_SPMS_SPM_C_WAKEUP_STATE_ACK: {
		tU16 u16OnOffStateMsgHandle = ((((tU16)m_achIncRcvBuffer[1]) << 0) |
					       (((tU16)m_achIncRcvBuffer[2]) << 8)  );
		(tVoid)bIncSend_RWakeupStateAck(u16OnOffStateMsgHandle);
		break;
	}
	default :
		/* Intentionally do nothing */
		break;
	}
}

/*******************************************************************************
*
* Method which sends the passed INC message to the /dev/wup.
*
*******************************************************************************/
tBool DevWupEnvironment::bSendIncMsg(tU8* pu8IncSndBuffer, tU32 u32MessageLength)
{
	if (dgram_send(
		m_pDatagramSocketDescriptor,
		pu8IncSndBuffer,
		u32MessageLength) == -1)
			return FALSE;
	return TRUE;
}

/*******************************************************************************
*
* Method which sends the INC message R_WAKEUP_EVENT.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RWakeupEvent(tU8 u8OnOffEvent)
{
	static tU16 u16OnOffEventMsgHandle = 1;

	tU8 au8IncSndBuffer[4];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_EVENT;
	au8IncSndBuffer[1] = u8OnOffEvent;
	au8IncSndBuffer[2] = (tU8)((u16OnOffEventMsgHandle & 0x00FF) >> 0);
	au8IncSndBuffer[3] = (tU8)((u16OnOffEventMsgHandle & 0xFF00) >> 8);

	u16OnOffEventMsgHandle++;

	return (bSendIncMsg(au8IncSndBuffer, 4));
}

/*******************************************************************************
*
* Method which sends the INC message R_WAKEUP_STATE.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RWakeupState(tU8 u8OnOffState, tBool bIsActive)
{
	static tU16 u16OnOffStateMsgHandle = 1;

	tU8 au8IncSndBuffer[5];
	
	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE;
	au8IncSndBuffer[1] = u8OnOffState;
	au8IncSndBuffer[2] = bIsActive;
	au8IncSndBuffer[3] = (tU8)((u16OnOffStateMsgHandle & 0x00FF) >> 0);
	au8IncSndBuffer[4] = (tU8)((u16OnOffStateMsgHandle & 0xFF00) >> 8);

	u16OnOffStateMsgHandle++;

	return (bSendIncMsg(au8IncSndBuffer, 5));
}

/*******************************************************************************
*
* Method which sends the INC message R_INDICATE_CLIENT_APP_STATE.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RIndicateClientAppState(tU8 u8ApplicationMode, tU8 u8ResetReasonScc, tU8 u8MsgCatMajorVersion, tU8 u8MsgCatMinorVersion, tU8 u8VerionCheckResult)
{
	tU8 au8IncSndBuffer[6];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE;
	au8IncSndBuffer[1] = u8ApplicationMode;
	au8IncSndBuffer[2] = u8ResetReasonScc;
	au8IncSndBuffer[3] = u8MsgCatMajorVersion;
	au8IncSndBuffer[4] = u8MsgCatMinorVersion;
	au8IncSndBuffer[5] = u8VerionCheckResult;

	return (bSendIncMsg(au8IncSndBuffer, 6));
}

/*******************************************************************************
*
* Method which sends the INC message R_SET_WAKEUP_CONFIG.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RSetWakeupConfig(tU32 u32WakeupConfig)
{
	tU8 au8IncSndBuffer[5];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_SET_WAKEUP_CONFIG;
	au8IncSndBuffer[1] = (tU8)(((u32WakeupConfig) & 0x000000FF) >>  0);
	au8IncSndBuffer[2] = (tU8)(((u32WakeupConfig) & 0x0000FF00) >>  8);
	au8IncSndBuffer[3] = (tU8)(((u32WakeupConfig) & 0x00FF0000) >> 16);
	au8IncSndBuffer[4] = (tU8)(((u32WakeupConfig) & 0xFF000000) >> 24);

	return (bSendIncMsg(au8IncSndBuffer, 5));
}

/*******************************************************************************
*
* Method which sends the INC message R_WAKEUP_REASON.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RWakeupReason(tU8 u8WakeupReason)
{
	tU8 au8IncSndBuffer[2];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_REASON;
	au8IncSndBuffer[1] = u8WakeupReason;

	return (bSendIncMsg(au8IncSndBuffer, 2));
}

/*******************************************************************************
*
* Method which sends the INC message R_WAKEUP_STATE_VECTOR.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RWakeupStateVector(tVoid)
{
	tU8 au8IncSndBuffer[5];

	DEV_WUP_trOnOffStates rOnOffStates;
	
	rOnOffStates.uOnOffStates.rBitfield.CAN            = 1;
	rOnOffStates.uOnOffStates.rBitfield.LIN            = 1;
	rOnOffStates.uOnOffStates.rBitfield.TELEPHONE_MUTE = 1;

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE_VECTOR;
	au8IncSndBuffer[1] = (tU8)((rOnOffStates.uOnOffStates.u32 & 0x000000FF) >>  0);
	au8IncSndBuffer[2] = (tU8)((rOnOffStates.uOnOffStates.u32 & 0x0000FF00) >>  8);
	au8IncSndBuffer[3] = (tU8)((rOnOffStates.uOnOffStates.u32 & 0x00FF0000) >> 16);
	au8IncSndBuffer[4] = (tU8)((rOnOffStates.uOnOffStates.u32 & 0xFF000000) >> 24);

	return (bSendIncMsg(au8IncSndBuffer, 5));
}

/*******************************************************************************
*
* Method which sends the INC message R_STARTUP_INFO.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RStartupInfo(tU8 u8StartType)
{
	tU8 au8IncSndBuffer[3];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_STARTUP_INFO;
	au8IncSndBuffer[1] = u8StartType;
	au8IncSndBuffer[2] = 0; // u8DisconnectInfo

	return (bSendIncMsg(au8IncSndBuffer, 3));
}

/*******************************************************************************
*
* Method which sends the INC message R_SHUTDOWN_IN_PROGRESS.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RShutdownInProgress(tVoid)
{
	tU8 au8IncSndBuffer[1];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_SHUTDOWN_IN_PROGRESS;

	return (bSendIncMsg(au8IncSndBuffer, 1));
}

/*******************************************************************************
*
* Method which sends the INC message R_PROC_RESET_REQUEST.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RProcResetRequest(tU8 u8ProcId)
{
	tU8 au8IncSndBuffer[2];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_PROC_RESET_REQUEST;
	au8IncSndBuffer[1] = u8ProcId;

	return(bSendIncMsg(au8IncSndBuffer, 2));
}

/*******************************************************************************
*
* Method which sends the INC message R_CTRL_RESET_EXECUTION.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RCtrlResetExecution(tVoid)
{
	tU8 au8IncSndBuffer[1];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_CTRL_RESET_EXECUTION;

	return (bSendIncMsg(au8IncSndBuffer, 1));
}

/*******************************************************************************
*
* Method which sends the INC message R_EXTEND_POWER_OFF_TIMEOUT.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RExtendPowerOffTimeout(tVoid)
{
	tU8 au8IncSndBuffer[3];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT;
	au8IncSndBuffer[1] = 0x3C;
	au8IncSndBuffer[2] = 0x00;

	return (bSendIncMsg(au8IncSndBuffer, 3));
}

/*******************************************************************************
*
* Method which sends the INC message R_WAKEUP_EVENT_ACK.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RWakeupEventAck(tU16 u16OnOffEventMsgHandle)
{
	tU8 au8IncSndBuffer[3];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_EVENT_ACK;
	au8IncSndBuffer[1] = (tU8)(((u16OnOffEventMsgHandle) & 0x00FF) >>  0);
	au8IncSndBuffer[2] = (tU8)(((u16OnOffEventMsgHandle) & 0xFF00) >>  8);

	return (bSendIncMsg(au8IncSndBuffer, 3));
}

/*******************************************************************************
*
* Method which sends the INC message R_WAKEUP_STATE_ACK.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RWakeupStateAck(tU16 u16OnOffStateMsgHandle)
{
	tU8 au8IncSndBuffer[3];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_WAKEUP_STATE_ACK;
	au8IncSndBuffer[1] = (tU8)(((u16OnOffStateMsgHandle) & 0x00FF) >>  0);
	au8IncSndBuffer[2] = (tU8)(((u16OnOffStateMsgHandle) & 0xFF00) >>  8);

	return (bSendIncMsg(au8IncSndBuffer, 3));
}

/*******************************************************************************
*
* Method which sends the INC message R_STARTUP_FINISHED.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RStartupFinished(tVoid)
{
	tU8 au8IncSndBuffer[1];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_STARTUP_FINISHED;

	return (bSendIncMsg(au8IncSndBuffer, 1));
}

/*******************************************************************************
*
* Method which sends the INC message R_EXTEND_POWER_OFF_TIMEOUT.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RExtendPowerOffTimeout(tU16 u16TimeoutS)
{
	tU8 au8IncSndBuffer[3];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT;
	au8IncSndBuffer[1] = (tU8)((u16TimeoutS & 0x00FF) >> 0);
	au8IncSndBuffer[2] = (tU8)((u16TimeoutS & 0xFF00) >> 8);

	return (bSendIncMsg(au8IncSndBuffer, 3));
}

/*******************************************************************************
*
* Method which sends the INC message R_AP_SUPERVISION_ERROR.
*
*******************************************************************************/
tBool DevWupEnvironment::bIncSend_RAPSupervisionError(tU8 u8APSupervisionError)
{
	tU8 au8IncSndBuffer[2];

	au8IncSndBuffer[0] = DEV_WUP_C_U8_INC_MSGID_SPM_SPMS_R_AP_SUPERVISION_ERROR;
	au8IncSndBuffer[1] = u8APSupervisionError;

	return (bSendIncMsg(au8IncSndBuffer, 2));
}

/*******************************************************************************
*
* Method which prepares the INC messge handler thread to receive and evaluate
* one INC message.
*
*******************************************************************************/
tBool DevWupEnvironment::bActivateSingleAsyncIncMsgReception(tVoid)
{
	tU8 au8MsqQueueBuffer[DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MSG_LEN];

	au8MsqQueueBuffer[0] = DEV_WUP_TEST_C_U8_OSAL_MSG_ACTIVATE_SINGLE_INC_MSG_RECEPTION;

	if (OSAL_s32MessageQueuePost(
		m_hQueueMsgHandlerThread,
		au8MsqQueueBuffer, 
		DEV_WUP_TEST_C_U32_OSAL_MSG_QUEUE_MSG_LEN,
		OSAL_C_U32_MQUEUE_PRIORITY_LOWEST) == OSAL_ERROR)
		return FALSE;

	return TRUE;
}

/*******************************************************************************
*
* Invalidate the PRAM data of the /dev/wup.
*
*******************************************************************************/
tBool DevWupEnvironment::bInvalidateDevWupPRAM(tVoid)
{
	tBool bResult = TRUE;

	OSAL_tIODescriptor rIODescriptor = OSAL_ERROR;

	rIODescriptor = OSAL_IOOpen(
				OSAL_C_STRING_DEVICE_PRAM"/dev_wup",
				OSAL_EN_READWRITE);

	if (rIODescriptor == OSAL_ERROR)
		return FALSE;

	if (OSAL_s32IOControl(
		rIODescriptor,
		OSAL_C_S32_IOCTRL_DEV_PRAM_CLEAR,
		0) == OSAL_ERROR)
			bResult = FALSE;

	OSAL_s32IOClose(rIODescriptor);

	return bResult;
}

/******************************************************************************/
/*                                                                            */
/* TESTS                                                                      */
/*                                                                            */
/******************************************************************************/

TEST_F(DevWupFixtureCloseDevice, OpenDevice)
{
	m_hIODescWupDriver = OSAL_IOOpen(
				OSAL_C_STRING_DEVICE_WUP,
				OSAL_EN_READWRITE);

	EXPECT_NE(OSAL_ERROR, m_hIODescWupDriver);
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOpenedDevice, CloseDevice)
{
	EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(m_hIODescWupDriver)); 
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControlGetStarttype)
{
	tU8 u8Starttype = DEV_WUP_C_U8_STARTTYPE_UNKNOWN;

	if (m_poDevWupEnvironment->bIncSend_RStartupInfo(DEV_WUP_C_U8_STARTTYPE_COLDSTART) == FALSE) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_WUP_CONF_C_U32_INC_MESSAGE_PROCESS_TIME_MS);

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_GET_STARTTYPE,
		(intptr_t)&u8Starttype) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	if (u8Starttype != DEV_WUP_C_U8_STARTTYPE_COLDSTART) {
		return;
		ADD_FAILURE();
	}
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControlGetWakeupReason)
{
	tU8 u8WakeupReason = DEV_WUP_C_U8_WAKEUP_REASON_UNKNOWN;

	if (m_poDevWupEnvironment->bIncSend_RWakeupReason(DEV_WUP_C_U8_WAKEUP_REASON_EXTERNAL_PIN_WAKEUP) == FALSE) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_WUP_CONF_C_U32_INC_MESSAGE_PROCESS_TIME_MS);

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_GET_WAKEUP_REASON,
		(intptr_t)&u8WakeupReason) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	if (u8WakeupReason != DEV_WUP_C_U8_WAKEUP_REASON_EXTERNAL_PIN_WAKEUP) {
		ADD_FAILURE();
		return;
	}
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_ConfigureWakeupReasons)
{
	tU32 u32WakeupConfig = DEV_WUP_C_U32_WAKEUP_REASONS_MASK_EXTERNAL_PIN_WAKEUP;

	if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver,
				OSAL_C_S32_IOCTRL_WUP_CONFIGURE_WAKEUP_REASONS,
				(intptr_t)u32WakeupConfig));
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_ControlResetMasterSupervision)
{
	DEV_WUP_trResetMasterSupervision rResetMasterSupervision = { TRUE, 0 };

	if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver, 
				OSAL_C_S32_IOCTRL_WUP_CONTROL_RESET_MASTER_SUPERVISION,
				(intptr_t)&rResetMasterSupervision));
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_GetLatestResetReason)
{
	DEV_WUP_trLatestResetReason rLatestResetReason;

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver,
				OSAL_C_S32_IOCTRL_WUP_GET_LATEST_RESET_REASON,
				(intptr_t)&rLatestResetReason));

	if (((rLatestResetReason.u16ResetReasonAP != DEV_WUP_C_U16_RESET_REASON_POWER_ON) &&
	     (rLatestResetReason.u16ResetReasonAP != DEV_WUP_C_U16_RESET_REASON_POWER_OFF)  ) ||
	     (rLatestResetReason.u8ResetReasonSCC != DEV_WUP_C_U8_RESET_REASON_COLDSTART)   )
		ADD_FAILURE();
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_IndicateStartupFinished)
{
	if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver,
				OSAL_C_S32_IOCTRL_WUP_INDICATE_STARTUP_FINISHED,
				(intptr_t)0));
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_ExtendPowerOffTimeout)
{
	tU16 u16TimeoutS = 60;

	if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver, 
				OSAL_C_S32_IOCTRL_WUP_EXTEND_POWER_OFF_TIMEOUT,
				(intptr_t)&u16TimeoutS));

	EXPECT_EQ(u16TimeoutS, 60);
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, DISABLED_IOControl_Shutdown)
{
	if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver,
				OSAL_C_S32_IOCTRL_WUP_SHUTDOWN,
				(intptr_t)DEV_WUP_SHUTDOWN_NORMAL));
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_ResetProcessor_Bluetooth_Unspecified)
{
	DEV_WUP_trResetProcessorInfo rResetProcessorInfo =
	{                               
		DEV_WUP_C_U8_BLUETOOTH_PROCESSOR,
		DEV_WUP_C_U8_RESET_MODE_LOGGED,
		DEV_WUP_C_U16_RESET_REASON_UNSPECIFIED,
		10
	};

	if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver,
				OSAL_C_S32_IOCTRL_WUP_RESET_PROCESSOR,
				(intptr_t)&rResetProcessorInfo));
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_ResetProcessor_EntireSystemWithPowerDisconnection_Unspecified)
{
	DEV_WUP_trResetProcessorInfo rResetProcessorInfo =
	{                               
		DEV_WUP_C_U8_ENTIRE_SYSTEM_WITH_POWER_DISCONNECTION,
		DEV_WUP_C_U8_RESET_MODE_LOGGED,
		DEV_WUP_C_U16_RESET_REASON_UNSPECIFIED,
		10
	};

	if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver,
				OSAL_C_S32_IOCTRL_WUP_RESET_PROCESSOR,
				(intptr_t)&rResetProcessorInfo));
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_RegisterAndUnregisterClient)
{
	DEV_WUP_trClientRegistration rClientRegistration = { 0 };

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver,
				OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
				(intptr_t)&rClientRegistration));

	EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(
				m_hIODescWupDriver,
				OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT,
				(intptr_t)rClientRegistration.u32ClientId));
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_RegisterAndUnregisterOnOffReasonChangedNotification)
{
	DEV_WUP_trClientRegistration             rClientRegistration             = { 0 };
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration = { 0 };
	tS32                                     s32OsalResult                   = OSAL_ERROR;
	tU32                                     u32Index                        = 0;

	struct rRegistration {
		tU8  u8NotificationMode;
		tS32 s32ExpectedOsalResult;
	};

	const struct rRegistration arRegistration[] =
	{
		{
			DEV_WUP_C_U8_NOTIFY_STATES_ONLY,
			OSAL_OK
		},
		{
			DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITH_PAST_ONES,
			OSAL_OK
		},
		{
			DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITHOUT_PAST_ONES,
			OSAL_OK
		},
		{
			DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITH_PAST_ONES,
			OSAL_OK
		},
		{
			DEV_WUP_C_U8_NOTIFY_STATES_AND_EVENTS_WITHOUT_PAST_ONES,
			OSAL_OK
		},
		{
			DEV_WUP_C_U8_NOTIFY_NONE,
			OSAL_ERROR
		}
	};

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	for (u32Index = 0;
	     u32Index < (sizeof(arRegistration) / sizeof(struct rRegistration));
	     u32Index ++) {
		rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
		rOnOffReasonChangedRegistration.u8NotificationMode = arRegistration[u32Index].u8NotificationMode;
		rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;
	
		s32OsalResult = OSAL_s32IOControl(
					m_hIODescWupDriver,
					OSAL_C_S32_IOCTRL_WUP_REGISTER_ONOFF_REASON_CHANGED_NOTIFICATION,
					(intptr_t)&rOnOffReasonChangedRegistration);

		if (s32OsalResult == OSAL_OK)
			OSAL_s32IOControl(
				m_hIODescWupDriver,
				OSAL_C_S32_IOCTRL_WUP_UNREGISTER_ONOFF_REASON_CHANGED_NOTIFICATION,
				(intptr_t)rOnOffReasonChangedRegistration.u32ClientId);

		if (s32OsalResult != arRegistration[u32Index].s32ExpectedOsalResult) {
				ADD_FAILURE();
				break;
		}
	}

error_register_onoff:
	OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT,
		(intptr_t)rClientRegistration.u32ClientId);
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_RegisterAndUnregisterApSupervisionErrorChangedNotification_OSAL_OK)
{
	DEV_WUP_trClientRegistration rClientRegistration = { 0 };

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_REGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION,
		(intptr_t)rClientRegistration.u32ClientId) == OSAL_OK) {

		if (OSAL_s32IOControl(
			m_hIODescWupDriver,
			OSAL_C_S32_IOCTRL_WUP_UNREGISTER_AP_SUPERVISION_ERROR_CHANGED_NOTIFICATION,
			(intptr_t)rClientRegistration.u32ClientId) == OSAL_ERROR)
				ADD_FAILURE();
	} else {
		ADD_FAILURE();
	}		

error_register_onoff:

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT,
		(intptr_t)rClientRegistration.u32ClientId) == OSAL_ERROR)
			ADD_FAILURE();
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_GetAPSupervisionError_ThermalShutdown_OSAL_OK)
{
	tU8 u8APSupervisionError = DEV_WUP_C_U8_AP_SUPERVISION_ERROR_NONE;

	if (m_poDevWupEnvironment->bIncSend_RAPSupervisionError(DEV_WUP_C_U8_AP_SUPERVISION_ERROR_THERMAL_SHUTDOWN) == FALSE) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_WUP_CONF_C_U32_INC_MESSAGE_PROCESS_TIME_MS);

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_GET_AP_SUPERVISION_ERROR,
		(intptr_t)&u8APSupervisionError) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	if (u8APSupervisionError != DEV_WUP_C_U8_AP_SUPERVISION_ERROR_THERMAL_SHUTDOWN)
		ADD_FAILURE();
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_GetOnOffEvents_EventsOnlyWithPastOnes)
{
	OSAL_tEventHandle                        hEventHandle                    = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask                          rEventMaskResult                = 0;
	DEV_WUP_trClientRegistration             rClientRegistration             = { 0 };
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration = { 0 };

	if (m_poDevWupEnvironment->bIncSend_RWakeupEvent(DEV_WUP_C_U8_ONOFF_EVENT_ON_TIPPER_PRESS) == FALSE) {
		ADD_FAILURE();
		return;
	}

	if (m_poDevWupEnvironment->bIncSend_RWakeupEvent(DEV_WUP_C_U8_ONOFF_EVENT_IGNITION_PIN) == FALSE) {
		ADD_FAILURE();
		return;
	}

	if (m_poDevWupEnvironment->bIncSend_RWakeupEvent(DEV_WUP_C_U8_ONOFF_EVENT_CD_INSERT_DETECTED) == FALSE) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_WUP_CONF_C_U32_INC_MESSAGE_PROCESS_TIME_MS);

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	if (OSAL_s32EventOpen(
		rClientRegistration.szNotificationEventName,
		&hEventHandle) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITH_PAST_ONES;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_REGISTER_ONOFF_REASON_CHANGED_NOTIFICATION,
		(intptr_t)&rOnOffReasonChangedRegistration) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_register_onoff; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventWait(
		hEventHandle,
		DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventPost(
		hEventHandle, 
		~rEventMaskResult, 
		OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_post; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (rEventMaskResult & DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY) {
		DEV_WUP_trOnOffEventHistory rOnOffEventHistory;

		rOnOffEventHistory.u32ClientId = rOnOffReasonChangedRegistration.u32ClientId;

		if (OSAL_s32IOControl(
			m_hIODescWupDriver,
			OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_EVENTS,
			(intptr_t)&rOnOffEventHistory) == OSAL_OK) {
			
			DEV_WUP_trOnOffEventAcknowledge rOnOffEventAcknowledge;
			tU8                             u8Index;

			if (rOnOffEventHistory.u8NumberOfOnOffEvents != 3)
				ADD_FAILURE();

			rOnOffEventAcknowledge.u32ClientId = rOnOffReasonChangedRegistration.u32ClientId;

			for (u8Index = 0; u8Index < rOnOffEventHistory.u8NumberOfOnOffEvents; u8Index++) {
				rOnOffEventAcknowledge.u16OnOffEventMsgHandle = rOnOffEventHistory.arOnOffEvent[u8Index].u16MsgHandle;

				if ((u8Index == 0)                                                                                &&
				    (rOnOffEventHistory.arOnOffEvent[u8Index].u8Event != DEV_WUP_C_U8_ONOFF_EVENT_ON_TIPPER_PRESS)  ) {
					ADD_FAILURE();
					break;
				}

				if ((u8Index == 1)                                                                             &&
				    (rOnOffEventHistory.arOnOffEvent[u8Index].u8Event != DEV_WUP_C_U8_ONOFF_EVENT_IGNITION_PIN)  ) {
					ADD_FAILURE();
					break;
				}

				if ((u8Index == 2)                                                                                   &&
				    (rOnOffEventHistory.arOnOffEvent[u8Index].u8Event != DEV_WUP_C_U8_ONOFF_EVENT_CD_INSERT_DETECTED)  ) {
					ADD_FAILURE();
					break;
				}

				if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE) {
						ADD_FAILURE();
						break;
				}

				if (OSAL_s32IOControl(
					m_hIODescWupDriver,
					OSAL_C_S32_IOCTRL_WUP_ACKNOWLEDGE_ONOFF_EVENT,
					(intptr_t)&rOnOffEventAcknowledge) == OSAL_ERROR) {
						ADD_FAILURE();
						break;
				}
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

error_event_post:
error_event_wait:

	OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_UNREGISTER_ONOFF_REASON_CHANGED_NOTIFICATION,
		(intptr_t)rOnOffReasonChangedRegistration.u32ClientId);

error_register_onoff:
	OSAL_s32EventClose(hEventHandle);

error_event_open:
	OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT,
		(intptr_t)rClientRegistration.u32ClientId);
}

/* --------------------------------------------------------------------------- */


TEST_F(DevWupFixtureOsal, IOControl_GetOnOffEvents_EventsOnlyWithoutPastOnes)
{
	OSAL_tEventHandle                        hEventHandle                    = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask                          rEventMaskResult                = 0;
	DEV_WUP_trClientRegistration             rClientRegistration             = { 0 };
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration = { 0 };

	if (m_poDevWupEnvironment->bIncSend_RWakeupEvent(DEV_WUP_C_U8_ONOFF_EVENT_ON_TIPPER_PRESS) == FALSE) {
		ADD_FAILURE();
		return;
	}

	if (m_poDevWupEnvironment->bIncSend_RWakeupEvent(DEV_WUP_C_U8_ONOFF_EVENT_IGNITION_PIN) == FALSE) {
		ADD_FAILURE();
		return;
	}

	if (m_poDevWupEnvironment->bIncSend_RWakeupEvent(DEV_WUP_C_U8_ONOFF_EVENT_CD_INSERT_DETECTED) == FALSE) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_WUP_CONF_C_U32_INC_MESSAGE_PROCESS_TIME_MS);

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	if (OSAL_s32EventOpen(
		rClientRegistration.szNotificationEventName,
		&hEventHandle) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_EVENTS_ONLY_WITHOUT_PAST_ONES;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_REGISTER_ONOFF_REASON_CHANGED_NOTIFICATION,
		(intptr_t)&rOnOffReasonChangedRegistration) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_register_onoff; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (m_poDevWupEnvironment->bIncSend_RWakeupEvent(DEV_WUP_C_U8_ONOFF_EVENT_RTC_WAKEUP) == FALSE) {
		ADD_FAILURE();
		goto error_inc_send; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	OSAL_s32ThreadWait(DEV_WUP_CONF_C_U32_INC_MESSAGE_PROCESS_TIME_MS);

	if (OSAL_s32EventWait(
		hEventHandle,
		DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventPost(
		hEventHandle, 
		~rEventMaskResult, 
		OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_post; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (rEventMaskResult & DEV_WUP_C_U32_EVENT_MASK_ONOFF_EVENT_CHANGED_NOTIFY) {
		DEV_WUP_trOnOffEventHistory rOnOffEventHistory;

		rOnOffEventHistory.u32ClientId = rOnOffReasonChangedRegistration.u32ClientId;

		if (OSAL_s32IOControl(
			m_hIODescWupDriver,
			OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_EVENTS,
			(intptr_t)&rOnOffEventHistory) == OSAL_OK) {
			
			DEV_WUP_trOnOffEventAcknowledge rOnOffEventAcknowledge;
			tU8                             u8Index;

			if (rOnOffEventHistory.u8NumberOfOnOffEvents != 1)
				ADD_FAILURE();

			rOnOffEventAcknowledge.u32ClientId = rOnOffReasonChangedRegistration.u32ClientId;

			for (u8Index = 0; u8Index < rOnOffEventHistory.u8NumberOfOnOffEvents; u8Index++) {
				rOnOffEventAcknowledge.u16OnOffEventMsgHandle = rOnOffEventHistory.arOnOffEvent[u8Index].u16MsgHandle;

				if ((u8Index == 0)                                                                                &&
				    (rOnOffEventHistory.arOnOffEvent[u8Index].u8Event != DEV_WUP_C_U8_ONOFF_EVENT_RTC_WAKEUP)  ) {
					ADD_FAILURE();
					break;
				}

				if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE) {
						ADD_FAILURE();
						break;
				}

				if (OSAL_s32IOControl(
					m_hIODescWupDriver,
					OSAL_C_S32_IOCTRL_WUP_ACKNOWLEDGE_ONOFF_EVENT,
					(intptr_t)&rOnOffEventAcknowledge) == OSAL_ERROR) {
						ADD_FAILURE();
						break;
				}
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

error_event_post:
error_event_wait:

	OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_UNREGISTER_ONOFF_REASON_CHANGED_NOTIFICATION,
		(intptr_t)rOnOffReasonChangedRegistration.u32ClientId);

error_inc_send:
error_register_onoff:
	OSAL_s32EventClose(hEventHandle);

error_event_open:
	OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT,
		(intptr_t)rClientRegistration.u32ClientId);
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControl_GetOnOffStates)
{
	OSAL_tEventHandle                        hEventHandle                    = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask                          rEventMaskResult                = 0;
	DEV_WUP_trClientRegistration             rClientRegistration             = { 0 };
	DEV_WUP_trOnOffReasonChangedRegistration rOnOffReasonChangedRegistration = { 0 };

	if (m_poDevWupEnvironment->bIncSend_RWakeupState(DEV_WUP_C_U8_ONOFF_STATE_CAN, 1) == FALSE) {
		ADD_FAILURE();
		return;
	}

	if (m_poDevWupEnvironment->bIncSend_RWakeupState(DEV_WUP_C_U8_ONOFF_STATE_LIN, 1) == FALSE) {
		ADD_FAILURE();
		return;
	}

	if (m_poDevWupEnvironment->bIncSend_RWakeupState(DEV_WUP_C_U8_ONOFF_STATE_S_CONTACT, 1) == FALSE) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_WUP_CONF_C_U32_INC_MESSAGE_PROCESS_TIME_MS);

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	if (OSAL_s32EventOpen(
		rClientRegistration.szNotificationEventName,
		&hEventHandle) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rOnOffReasonChangedRegistration.u32ClientId        = rClientRegistration.u32ClientId;
	rOnOffReasonChangedRegistration.u8NotificationMode = DEV_WUP_C_U8_NOTIFY_STATES_ONLY;
	rOnOffReasonChangedRegistration.bIsSystemMaster    = TRUE;

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_REGISTER_ONOFF_REASON_CHANGED_NOTIFICATION,
		(intptr_t)&rOnOffReasonChangedRegistration) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_register_onoff; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventWait(
		hEventHandle,
		DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventPost(
		hEventHandle, 
		~rEventMaskResult, 
		OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_post; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (rEventMaskResult & DEV_WUP_C_U32_EVENT_MASK_ONOFF_STATE_CHANGED_NOTIFY) {
		DEV_WUP_trOnOffStates rOnOffStates;

		if (OSAL_s32IOControl(
			m_hIODescWupDriver,
			OSAL_C_S32_IOCTRL_WUP_GET_ONOFF_STATES,
			(intptr_t)&rOnOffStates) == OSAL_OK) {
				DEV_WUP_trOnOffStateAcknowledge rOnOffStateAcknowledge;

				if ((rOnOffStates.uOnOffStates.rBitfield.CAN       != 1) ||
				    (rOnOffStates.uOnOffStates.rBitfield.LIN       != 1) || 
				    (rOnOffStates.uOnOffStates.rBitfield.S_CONTACT != 1)   )
					ADD_FAILURE();
				    
				rOnOffStateAcknowledge.u32ClientId            = rOnOffReasonChangedRegistration.u32ClientId;
				rOnOffStateAcknowledge.u16OnOffStateMsgHandle = rOnOffStates.u16MsgHandle;

				if (m_poDevWupEnvironment->bActivateSingleAsyncIncMsgReception() == FALSE)
					ADD_FAILURE();

				if (OSAL_s32IOControl(
					m_hIODescWupDriver,
					OSAL_C_S32_IOCTRL_WUP_ACKNOWLEDGE_ONOFF_STATE,
					(intptr_t)&rOnOffStateAcknowledge) == OSAL_ERROR)
						ADD_FAILURE();
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

error_event_post:
error_event_wait:
	OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_UNREGISTER_ONOFF_REASON_CHANGED_NOTIFICATION,
		(intptr_t)rOnOffReasonChangedRegistration.u32ClientId);

error_inc_send:
error_register_onoff:
	OSAL_s32EventClose(hEventHandle);

error_event_open:
	OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_UNREGISTER_CLIENT,
		(intptr_t)rClientRegistration.u32ClientId);
}

/* --------------------------------------------------------------------------- */

TEST_F(DevWupFixtureOsal, IOControlGetApplicationMode)
{
	tU8 u8ApplicationModeAP = DEV_WUP_C_U8_APPLICATION_MODE_UNKNOWN;

	if (OSAL_s32IOControl(
		m_hIODescWupDriver,
		OSAL_C_S32_IOCTRL_WUP_GET_APPLICATION_MODE,
		(intptr_t)&u8ApplicationModeAP) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	if (u8ApplicationModeAP != DEV_WUP_C_U8_APPLICATION_MODE_NORMAL) {
		ADD_FAILURE();
		return;
	}
}

/******************************************************************************/
