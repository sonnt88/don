/*******************************************************************************
*
* FILE:         dev_wup_integration_test.h
*
* SW-COMPONENT: Device Wakeup
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Integration tests.
*
* AUTHOR:       CM-AI/PJ-CF33-Kalms
*
* COPYRIGHT:    (c) 2013 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_WUP_INTEGRATION_TEST_H_
#define _DEV_WUP_INTEGRATION_TEST_H_

/******************************************************************************/

#define DEV_WUP_TEST_C_STRING_SEM_CLIENT_PROCESS_START_NAME        "DevWupStart"

#define DEV_WUP_TEST_C_STRING_SEM_CLIENT_PROCESS_STOP_NAME          "DevWupStop"

/******************************************************************************/
/*                                                                            */
/* CLASS DECLARATION                                                          */
/*                                                                            */
/******************************************************************************/

#ifdef __cplusplus
class DevWupEnvironment : public testing::Environment
{
	private:

		tInt               m_nSocketFileDescriptorInc;
		sk_dgram*          m_pDatagramSocketDescriptor;
		OSAL_tSemHandle    m_hSemMsgHandlerThread;
		OSAL_tSemHandle    m_hSemFirstMsgsSync;
		OSAL_tSemHandle    m_hSemClientProcessStart;
		OSAL_tSemHandle    m_hSemClientProcessStop;
		OSAL_tEventHandle  m_hEventMsgHandlerThread;
		OSAL_tMQueueHandle m_hQueueMsgHandlerThread;
		tU8                m_u8MsgHandlerThreadState;
		tChar              m_achIncRcvBuffer[64];
		tBool              m_bSccIncComEstablished;
		tU16               m_u16OnOffEventMsgHandle;
		tU16               m_u16OnOffStateMsgHandle;

	public:

		DevWupEnvironment();
		~DevWupEnvironment();

		virtual tVoid SetUp(tVoid);
		virtual tVoid TearDown(tVoid);

		tBool bInitSccIncCom(tVoid);
		tBool bExitSccIncCom(tVoid);

		tBool bInstallMsgHandlerThread(tVoid);
		tBool bUninstallMsgHandlerThread(tVoid);

		static tVoid vMsgHandlerThread(tVoid* pvArg);

		tVoid vOnIncMsgReceived(tInt nIncMsgLength);
		tVoid vOnOsalMsgReceived(tU8* pu8MsqQueueBuffer);

		tBool bActivateSingleAsyncIncMsgReception(tVoid);

		tBool bSendIncMsg(tU8* pu8IncSndBuffer, tU32 u32MessageLength);

		tBool bIncSend_RIndicateClientAppState(tU8 u8ApplicationMode, tU8 u8ResetReasonScc, tU8 u8MsgCatMajorVersion, tU8 u8MsgCatMinorVersion, tU8 u8VerionCheckResult);
		tBool bIncSend_RStartupInfo(tU8 u8StartType);
		tBool bIncSend_RWakeupReason(tU8 u8WakeupReason);
		tBool bIncSend_RSetWakeupConfig(tU32 u32WakeupConfig);
		tBool bIncSend_RWakeupStateVector(tVoid);
		tBool bIncSend_RShutdownInProgress(tVoid);
		tBool bIncSend_RProcResetRequest(tU8 u8ProcId);
		tBool bIncSend_RCtrlResetExecution(tVoid);
		tBool bIncSend_RWakeupEvent(tU8 u8OnOffEvent);
		tBool bIncSend_RWakeupState(tU8 u8OnOffState, tBool bIsActive);
		tBool bIncSend_RExtendPowerOffTimeout(tVoid);
		tBool bIncSend_RWakeupEventAck(tU16 u16OnOffEventMsgHandle);
		tBool bIncSend_RWakeupStateAck(tU16 u16OnOffStateMsgHandle);
		tBool bIncSend_RStartupFinished(tVoid);
		tBool bIncSend_RExtendPowerOffTimeout(tU16 u16TimeoutS);
		tBool bIncSend_RAPSupervisionError(tU8 u8APSupervisionError);
		
		tBool bInvalidateDevWupPRAM(tVoid);
};

/* -------------------------------------------------------------------------- */

class DevWupFixture : public testing::Test
{
	protected:
  
		OSAL_tIODescriptor m_hIODescWupDriver;
		DevWupEnvironment* m_poDevWupEnvironment;

	public:

		DevWupFixture();
		~DevWupFixture();
};

/* -------------------------------------------------------------------------- */

class DevWupFixtureOpenedDevice : public DevWupFixture
{
	protected:

		virtual void SetUp()
		{
			m_hIODescWupDriver = 
				OSAL_IOOpen(
					OSAL_C_STRING_DEVICE_WUP,
					OSAL_EN_READWRITE);
		}
};

/* -------------------------------------------------------------------------- */

class DevWupFixtureCloseDevice : public DevWupFixture
{
	protected:

		virtual void TearDown()
		{
			OSAL_s32IOClose(m_hIODescWupDriver);  
		}
};

/* -------------------------------------------------------------------------- */

class DevWupFixtureOsal : public DevWupFixture
{
	private:

	protected:

		virtual void SetUp()
		{
			m_hIODescWupDriver =
				OSAL_IOOpen(
					OSAL_C_STRING_DEVICE_WUP,
					OSAL_EN_READWRITE);
		}

		virtual void TearDown()
		{
			OSAL_s32IOClose(m_hIODescWupDriver);  
		}

	public:

		DevWupFixtureOsal();
		~DevWupFixtureOsal();
};

/* -------------------------------------------------------------------------- */
#endif /* __cplusplus */

#endif //_DEV_WUP_INTEGRATION_TEST_H_
