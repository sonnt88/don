/******************************************************************************
* FILE            : dev_odometer.c
*
* DESCRIPTION     : This file simulates the odometer driver for LSIM.
*                   A parsing application will parse the trip file and send the data to 
*                   Network data dispatcher module via Net link socket.
*                   This odometer driver receives this data from network dispatcher
*                   via a Osal Message queue interface.
*
* AUTHOR(s)       : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY         :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 26.OCT.2012|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

/************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
/* To access osal interfaces */
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"

#include "dev_odometer.h"
#include "odometer_rngbuffer.h"

/************************************************************************
* Macro declaration (scope: Global)
*-----------------------------------------------------------------------*/


#define ODOMETER_C_S32_IO_VERSION (tS32)0x00000103   /* Read as v1.03 */


#define OSALCORE_C_U32_STACK_SIZE_ODOMETER          (2048)
#define OSALCORE_C_U32_PRIORITY_ODOMETER            (15)

/*! \brief number of the ringbuffer entries in \ref
 *   ODOMETER_trCtrlRingBuff.rOdoData */
#define ODOMETER_C_U16_RINGBUFF_ENTRIES  (512)

/*! \brief value for the timeout in \ref ODOMETER_s32IORead() for the
 *   OSAL_s32EventWait() timeout parameter */
#define ODOMETER_IOREAD_DEFAULT_TIMEOUT_IN_MS (300)


/*! \def ODOMETER_OVC_CSM_SAMPLING_RATE_IN_NS return value for ioctrl call \ref
 * OSAL_C_S32_IOCTRL_ODOMETER_GETCYCLETIME odovcan from CSM sampling rate in
 * ns */
#define  ODOMETER_OVC_CSM_SAMPLING_RATE_IN_NS  (100000000)   

/*!\brief macro for ioctrl call \ref
 * OSAL_C_S32_IOCTRL_ODOMETER_GET_TIMEOUT_VALUE to convert time in ms from \ref
 * ODOMETER_IOREAD_DEFAULT_TIMEOUT_IN_MS to seconds */
#define ODOMETER_CONVERT_MS_INTO_SECONDS(time_in_milliseconds)          \
   (tU32)(((tU32)(time_in_milliseconds))/((tU32)1000))


/*!\brief macro for ioctrl call 
 * OSAL_C_S32_IOCTRL_ODOMETER_GETRESOLUTION to convert time in nano seconds from 
 * ODOMETER_OVC_CSM_SAMPLING_RATE_IN_NS to   milliseconds */
#define ODOMETER_CONVERT_NS_INTO_MS(time_in_nanoseconds)          \
   (tU32)(((tU32)(time_in_nanoseconds))/((tU32)1000000))


/*! circumfernce of the wheel  */
#define ABS_WHEELCIRCUMFERENCE_IN_METER (2)

/*! Pulses per revolution of the wheel */
#define ABS_PPR 100

/*! K value derived from DistanceCalibrationValue in pulses/m */
#define ODO_K_VALUE_INIT  (ABS_PPR / ABS_WHEELCIRCUMFERENCE_IN_METER)



#define ODOMETER_DATA_SOURCE_MSG_QUE "ODOMETER_MSG_QUEUE_LSIM"

/*Odometer resolution/sampling rate presently is 100ms.
   It is better to buffer data sufficient atlease for next 3 seconds in message queue.
   This is needed because of some random delays with net link socket communication*/
#define ODOMETER_DATA_SOURCE_MSG_QUE_LENGTH (30) 

#define ODOMETER_MAIN_THREAD_NAME "ODOMETER_THREAD_MAIN"

#define ODOMETER_TIMEOUT_HANDLING_THREAD "ODOMETER_TIMEOUT_THREAD"

#define LSIM_ODOMETER_TIMEOUT_EVENT_NAME "LSIM_ODOMETER_TIMEOUT_EVENT"

#define LSIM_ODO_TIMEOUT_EVENT  (0x01)
#define LSIM_ODO_SHUTDOWN_EVENT (0x02)


/* This value is derived from Tengine driver of odometer*/
#define ODOMETER_DATA_TIMEOUT_VALUE (300) /* 100*3 milliseconds */

/* Timeout message attributes */
#define OSAL_C_U8_ODO_STATUS_UNKNOWN (0xFF)
#define OSAL_C_U32_ODO_WHEEL_COUNTER_UNKNOWN (0xFFFF)
#define ODOMETER_DATA_TIMEOUT_MESSAGE_PRIORITY (6)
#define ODOMETER_TIMEOUT_RECORD {OSAL_NULL,\
                                 OSAL_C_U32_ODO_WHEEL_COUNTER_UNKNOWN,\
                                 OSAL_EN_RFS_UNKNOWN,\
                                 OSAL_NULL,\
                                 OSAL_C_U8_ODO_STATUS_UNKNOWN,0,0,0,0,0,0}

/* These macros are used to release the resources acquired.*/

#define ODO_MSG_QUEUE     (1)
#define ODO_RING_BUFFER   (2)
#define ODO_DATA_TIMER    (3)
#define ODO_TIMEOUT_EVENT (4)

/************************************************************************
* Drver specific structure definition
*-----------------------------------------------------------------------*/
/* Format of data from Odometer Message queue  */
typedef struct
{
   tS32 s32ResHld;
   tU32 u32MsgSize;
   OSAL_trIOCtrlOdometerData rOdoSensorData;
}trOdoMsgQueData;

/* All global variables are embedded in this structure*/
typedef struct
{
   trRngBufferHandle hr_odoRngBufHandle;
   OSAL_tMQueueHandle hDataSourceMsgQue;
   OSAL_tTimerHandle hTimerDataTimeout;
   tBool bOdoDevCreateFlag;
   tBool bOdoDevOpenFlag;
   tU32 u32OdoDataErrCnt;
   tU16 u16Kvalue;
   tBool bShutDownFlag;
   OSAL_tEventHandle hEventOdoTimeout;

}trOdoLsimInfo;

/************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/
/* This structure holds all global variables used in this file */

trOdoLsimInfo rOdoLSimInfo = {RNGBUF_C_INIT_HANDLE,\
                              0,\
                              0,\
                              FALSE,\
                              FALSE,\
                              0,\
                              ODO_K_VALUE_INIT,\
                              FALSE,\
                              0};

/* This structure is used to store odometer data */
static OSAL_trIOCtrlOdometerData rOdoDataBuffer[ODOMETER_C_U16_RINGBUFF_ENTRIES];


/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
static tVoid LsimOdo_vDataTimeoutCallback(tPVoid pvDummyArg);
static tVoid LsimOdo_vThreadMain(tPVoid pvDummyArg);
static tVoid LsimOdo_vTimeOutThread(tPVoid pvDummyArg);
static tVoid LsimOdo_vReleaseResource(tU32 u32ResourceID);
static tVoid LsimOdo_vTraceOut(  tU32 u32Level,const tChar *pcFormatString,... );

/********************************************************************************
* FUNCTION        : ODOMETER_s32CreateOdoDevice 

* PARAMETER       : NONE
*                                                      
* RETURNVALUE     : OSAL_OK  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : This is initialization function for odometer driver in LSIM. 
*                   This is called from devinit.c at startup.
*                   1:It creates message queue which is odometer data source in LSIM.
*                   2: Initialize a Ring buffer.
*                   3: Creates a timer. This timer is used to monitor if data is 
*                      received at proper intervals.
*                   4: Creates two threads and a Event.
*
* HISTORY         : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 ODOMETER_s32CreateOdoDevice(tVoid)
{
   tS32 s32RetVal=OSAL_ERROR;
   OSAL_trThreadAttribute rThreadAttr;
   tuRngBufDataPtr u_odoDataBuffer;

   /* Pointer to memory for ring buffer */
   u_odoDataBuffer.pOdoData = rOdoDataBuffer;

   /* Initialize ring buffer to store odometer data. */
   if( OSAL_OK == 
       ODOMETER_s32InitRngBuf(&rOdoLSimInfo.hr_odoRngBufHandle,  /* Handle to Ring buffer */
                              ODO_RNG_BUF,                       /*Type of Data stored in Ring Buffer*/
                              sizeof(OSAL_trIOCtrlOdometerData), /*Size of each entry*/
                              ODOMETER_C_U16_RINGBUFF_ENTRIES,   /*Number of entries*/
                              u_odoDataBuffer ))                 /*Pointer to memory for ring buffer */
   {
      /* Network dispatcher will receive data from odometer parser and fill this message queue.
         This is the virtual source of data for odometer in LSIM.*/
      if(OSAL_OK == (OSAL_s32MessageQueueCreate((tCString)ODOMETER_DATA_SOURCE_MSG_QUE,
                                                ODOMETER_DATA_SOURCE_MSG_QUE_LENGTH,
                                                sizeof(trOdoMsgQueData),
                                                (OSAL_tenAccess)OSAL_EN_READWRITE,
                                                &rOdoLSimInfo.hDataSourceMsgQue)))
      {
         /* Call back of this timer is used to send error message to VD sensor
            if odometer data is not received within timeout specified.*/
         s32RetVal = OSAL_s32TimerCreate(LsimOdo_vDataTimeoutCallback,
                                         OSAL_NULL,
                                         &rOdoLSimInfo.hTimerDataTimeout);
         if(OSAL_OK != s32RetVal)
         {
            LsimOdo_vTraceOut(TR_LEVEL_FATAL,
                             "Data timeout Timer creation failed. Error code %lu ",
                             OSAL_u32ErrorCode());
            LsimOdo_vReleaseResource(ODO_RING_BUFFER);
            LsimOdo_vReleaseResource(ODO_MSG_QUEUE);
         }
         /*If timer creation passed, start the timer*/
         else if(OSAL_OK != OSAL_s32TimerSetTime(rOdoLSimInfo.hTimerDataTimeout,
                                                 ODOMETER_DATA_TIMEOUT_VALUE,
                                                 ODOMETER_DATA_TIMEOUT_VALUE))
         {
            LsimOdo_vTraceOut(TR_LEVEL_ERROR,
                             "Failed OSAL_s32TimerSetTime. Err Code %lu",
                             OSAL_u32ErrorCode());
         }
      }
      else
      {
         LsimOdo_vTraceOut(TR_LEVEL_FATAL,
                          "ODOMETER_DATA_SOURCE_MSG_QUE creation failed. Error code %lu ",
                          OSAL_u32ErrorCode());
         LsimOdo_vReleaseResource(ODO_RING_BUFFER);
      }
   }
   else
   {
      LsimOdo_vTraceOut(TR_LEVEL_FATAL,"Ring Buff Init Failed");
   }

   /* If there is no error till now */
   if(OSAL_OK == s32RetVal)
   {
      /* This event is used to intimate ODOMETER_TIMEOUT_HANDLING_THREAD regarding timeout */
      if(OSAL_OK != OSAL_s32EventCreate((tCString) LSIM_ODOMETER_TIMEOUT_EVENT_NAME,&(rOdoLSimInfo.hEventOdoTimeout)))
      {
         LsimOdo_vTraceOut(TR_LEVEL_FATAL,"LSIM_ODOMETER_TIMEOUT_EVENT create Failed");
         s32RetVal = OSAL_ERROR;
         LsimOdo_vReleaseResource(ODO_RING_BUFFER);
         LsimOdo_vReleaseResource(ODO_MSG_QUEUE);
         LsimOdo_vReleaseResource(ODO_DATA_TIMER);
      }
   }

   /* This thread is used to post error record into odometer ring buffer in case of timeout event
      from hTimerDataTimeout timer. As posting to ring buffer involves acquiring a semaphore, this
      cannot be done in timer callback context.Hence this thread is used.*/
   if(OSAL_OK == s32RetVal)
   {
      rThreadAttr.szName       = (tString)ODOMETER_TIMEOUT_HANDLING_THREAD;
      rThreadAttr.s32StackSize = OSALCORE_C_U32_STACK_SIZE_ODOMETER;
      rThreadAttr.u32Priority  = OSALCORE_C_U32_PRIORITY_ODOMETER;
      rThreadAttr.pfEntry      = LsimOdo_vTimeOutThread;
      rThreadAttr.pvArg        = (tPVoid)OSAL_NULL;

      if(OSAL_ERROR == (OSAL_ThreadSpawn(&rThreadAttr)))
      {
         LsimOdo_vTraceOut(TR_LEVEL_FATAL,"ODOMETER timeout thread creation failed.Error code %lu",
                                           OSAL_u32ErrorCode());
         s32RetVal = OSAL_ERROR;
         LsimOdo_vReleaseResource(ODO_RING_BUFFER);
         LsimOdo_vReleaseResource(ODO_MSG_QUEUE);
         LsimOdo_vReleaseResource(ODO_DATA_TIMER);
         LsimOdo_vReleaseResource(ODO_TIMEOUT_EVENT);
      }
   }

   
   /*This is the solo thread for Odometer driver in LSIM. This receives data from 
     message queue and adds it to ring buffer.*/
   if(OSAL_OK == s32RetVal)
   {

      rThreadAttr.szName       = (tString)ODOMETER_MAIN_THREAD_NAME;
      rThreadAttr.s32StackSize = OSALCORE_C_U32_STACK_SIZE_ODOMETER;
      rThreadAttr.u32Priority  = OSALCORE_C_U32_PRIORITY_ODOMETER;
      rThreadAttr.pfEntry      = LsimOdo_vThreadMain;
      rThreadAttr.pvArg        = (tPVoid)OSAL_NULL;

      if(OSAL_ERROR == (OSAL_ThreadSpawn(&rThreadAttr)))
      {
         LsimOdo_vTraceOut(TR_LEVEL_FATAL,"ODOMETER Main thread creation failed.Error code %lu",
                                           OSAL_u32ErrorCode());
         LsimOdo_vReleaseResource(ODO_RING_BUFFER);
         LsimOdo_vReleaseResource(ODO_MSG_QUEUE);
         LsimOdo_vReleaseResource(ODO_DATA_TIMER);
         LsimOdo_vReleaseResource(ODO_TIMEOUT_EVENT);
      }
      else
      {
         rOdoLSimInfo.bOdoDevCreateFlag = TRUE;
      }
   }
   return s32RetVal;
}

/********************************************************************************
* FUNCTION        : ODOMETER_IOOpen 

* PARAMETER       : NONE
*                                                      
* RETURNVALUE     : OSAL_E_NOERROR  on sucess
*                   OSAL_E_ALREADYOPENED or OSAL_E_NOTINITIALIZED on Failure
*
* DESCRIPTION     : This is the open call for driver. 
*                   This is called by application before using any other interfaces of odmeter.
*                   1:Check for open and device create flag.
*                   2: Only one open is allowed presently.
*
*
* HISTORY         : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 ODOMETER_IOOpen(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;
   /* Check if driver is initialized */
   if(TRUE == rOdoLSimInfo.bOdoDevCreateFlag)
   {
      /* Check if driver is already opened*/
      if(rOdoLSimInfo.bOdoDevOpenFlag == FALSE)
      {
         s32RetVal = OSAL_E_NOERROR;
         rOdoLSimInfo.bOdoDevOpenFlag = TRUE;
      }
      else
      {
         s32RetVal = OSAL_E_ALREADYOPENED;
         LsimOdo_vTraceOut(TR_LEVEL_ERROR, "Multiple open not supported");
      }
   }
   else
   {
      s32RetVal = OSAL_E_NOTINITIALIZED;
      LsimOdo_vTraceOut(TR_LEVEL_ERROR, "Odo device in not created");
   }

   return s32RetVal;
}

/********************************************************************************
* FUNCTION        : ODOMETER_s32IOClose 

* PARAMETER       :   NONE
*                                                      
* RETURNVALUE     : OSAL_E_NOERROR
*                             
*
* DESCRIPTION     : This resets the open flag set during ODOMETER_IOOpen. 
*
*
* HISTORY         : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 ODOMETER_s32IOClose(void)
{

   rOdoLSimInfo.bOdoDevOpenFlag = FALSE;
   
   return (OSAL_E_NOERROR);
}


/********************************************************************************
* FUNCTION        : ODOMETER_s32IOControl 

* PARAMETER       : s32fun : Function identificator
*                   s32arg :  Argument to execute the IO_control.
*                                                      
* RETURNVALUE     : OSAL_E_NOERROR  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : This handles all  IO controls to Odometer driver for lsim.
*                   Most IO controls supported here are getting and setting some configuration values.
*                   Unlike actual driver, most of values are hardcoded. This function is designed 
*                   to behave in a similar manner as odometer driver in Tengine hardware
*                   in application point of view.
*
* HISTORY         : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 ODOMETER_s32IOControl(tS32 s32fun, tS32 s32arg)
{
   tS32 s32RetVal = OSAL_ERROR;
   tPS32 ps32Arg = (tPS32)s32arg;
   
   switch(s32fun)
   {
   case ( OSAL_C_S32_IOCTRL_ODOMETER_FLUSH ):
      {
         /*This is not implemented in actual odometer driver.*/
         s32RetVal = OSAL_E_NOERROR;
         break;
      }

   case ( OSAL_C_S32_IOCTRL_ODOMETER_GETCNT ):
      {
         /*This is not implemented in actual odometer driver.*/
         s32RetVal = OSAL_E_NOERROR;

         break;
      }

   case ( OSAL_C_S32_IOCTRL_ODOMETER_GET_TIMEOUT_VALUE ):
      {
         if( ps32Arg != OSAL_NULL )
         {
            (*ps32Arg) = ODOMETER_CONVERT_MS_INTO_SECONDS( ODOMETER_IOREAD_DEFAULT_TIMEOUT_IN_MS );
            s32RetVal = OSAL_E_NOERROR;
         }
         break;
      }

   case ( OSAL_C_S32_IOCTRL_ODOMETER_GETRESOLUTION ):
      {
         if( ps32Arg != OSAL_NULL )
         {
            (*ps32Arg) = ODOMETER_CONVERT_NS_INTO_MS( ODOMETER_OVC_CSM_SAMPLING_RATE_IN_NS );
            s32RetVal = OSAL_E_NOERROR;
         }
         break;
      }

   case ( OSAL_C_S32_IOCTRL_ODOMETER_GETCYCLETIME ):
      {
         if( ps32Arg != OSAL_NULL )
         {
            (*ps32Arg) = ODOMETER_OVC_CSM_SAMPLING_RATE_IN_NS;
            s32RetVal = OSAL_E_NOERROR;
         }
         break;
      }
   case (OSAL_C_S32_IOCTRL_ODOMETER_SET_K_VALUE):
      {
         if( ps32Arg != OSAL_NULL )
         {
            rOdoLSimInfo.u16Kvalue = (tU16)(*ps32Arg);
            s32RetVal = OSAL_E_NOERROR;
         }
         break;
      }
   case (OSAL_C_S32_IOCTRL_ODOMETER_PULSES_PER_REVOLUTION):

      {
         if( ps32Arg != OSAL_NULL )
         {
            (*ps32Arg) = ABS_PPR;
            s32RetVal = OSAL_E_NOERROR;
         }
         break;
      }
      
   case ( OSAL_C_S32_IOCTRL_ODOMETER_WHEEL_CIRCUMFERENCE ):

      {
         if( ps32Arg != OSAL_NULL )
         {
            (*ps32Arg) = ABS_WHEELCIRCUMFERENCE_IN_METER * 1000 / ABS_PPR;
            s32RetVal = OSAL_E_NOERROR;
         }
         break;
      }
   case ( OSAL_C_S32_IOCTRL_VERSION ):
     {

      if(ps32Arg != OSAL_NULL)
      {
         (*ps32Arg) = ODOMETER_C_S32_IO_VERSION;
          s32RetVal = OSAL_E_NOERROR;
      }
      break;
     }
      /* not supported */
   case ( OSAL_C_S32_IOCTRL_ODOMETER_RESET ):
   case ( OSAL_C_S32_IOCTRL_ODOMETER_INIT ):
   case ( OSAL_C_S32_IOCTRL_ODOMETER_GETDIRECTION ):
   case ( OSAL_C_S32_IOCTRL_ODOMETER_SETDIRECTION ):
   case ( OSAL_C_S32_IOCTRL_ODOMETER_SET_TIMEOUT_VALUE ):
   case ( OSAL_C_S32_IOCTRL_ODOMETER_GET_WHEELCOUNTER ):
   case ( OSAL_C_S32_IOCTRL_ODOMETER_INIT_OVC ):
   case ( OSAL_C_S32_IOCTRL_ODOMETER_INACTIVATE_OIC ):
   case ( OSAL_C_S32_IOCTRL_ODOMETER_ACTIVATE_OIC ):

   default:
      {
         s32RetVal = OSAL_E_NOTSUPPORTED;
         break;
      }
   }
   return s32RetVal;
}

/********************************************************************************
* FUNCTION        : ODOMETER_s32IORead 

* PARAMETER       : prOdoData: pointer to store odometer data.
*                   u32maxbytes: Number of bytes of data to be read.
*                                                      
* RETURNVALUE     : Number of bytes read  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : Osal read call will be routed to this function. This reads integral
*                   number of odometer samples into memory pointed by prIOOdoData.
*
* HISTORY         : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 ODOMETER_s32IORead(OSAL_trIOCtrlOdometerData* prOdoData, tU32 u32maxbytes)
{
   /* Calculate the number of odometer records to be read */
   tU32 u32NumberOfEntries = u32maxbytes / sizeof(OSAL_trIOCtrlOdometerData);
   static tuRngBufDataPtr uDataBuff;
   tS32 s32RetVal = OSAL_ERROR;
   tU32 u32retNumBufs;

   if((OSAL_NULL != prOdoData) && ( 0 < u32NumberOfEntries ))
   {
      uDataBuff.pOdoData = prOdoData;
      
      u32retNumBufs = ODOMETER_u32ReadRngBuf( &rOdoLSimInfo.hr_odoRngBufHandle,
                                              u32NumberOfEntries,
                                              uDataBuff,
                                              0,
                                              &s32RetVal );
      /*If read from ring buffer is sucessfull, update return value to number of bytes read.*/
      if( OSAL_OK == s32RetVal )
      {
         s32RetVal = u32retNumBufs;
      }
   }
   else
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   return s32RetVal;
}
/********************************************************************************
* FUNCTION        : ODOMETER_s32DestroyOdoDevice 

* PARAMETER       :   None
*                                                      
* RETURNVALUE     : OSAL_OK
*
* DESCRIPTION     : This is called from devinit.c during shutdown.
*                   This sets shutdown flag and clears device create flag.
*                   Then releases all the resources created during startup.
*
* HISTORY         : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 ODOMETER_s32DestroyOdoDevice(tVoid)
{

   rOdoLSimInfo.bShutDownFlag = TRUE;
   rOdoLSimInfo.bOdoDevCreateFlag = FALSE;

   /*During shutdown, LsimOdo_vThreadMain may be waiting for a message.
      This should come out of wait state. So post a dummy message. This can be done by 
      calling timer callback instead of implementing a new function.*/
   LsimOdo_vDataTimeoutCallback((tPVoid)OSAL_NULL);
   LsimOdo_vReleaseResource(ODO_DATA_TIMER);
   LsimOdo_vReleaseResource(ODO_RING_BUFFER);
   LsimOdo_vReleaseResource(ODO_MSG_QUEUE);
   LsimOdo_vReleaseResource(ODO_TIMEOUT_EVENT);

   return OSAL_OK;
}
/********************************************************************************
* FUNCTION        : LsimOdo_vDataTimeoutCallback 

* PARAMETER       : pvDummyArg : Not used
*                                                      
* RETURNVALUE     : None
*
* DESCRIPTION     : This is timer callback function. This is used to mointor if data is received
*                   before timeout.
*
* HISTORY         : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

static tVoid LsimOdo_vDataTimeoutCallback(tPVoid pvDummyArg)
{

   /* Adding to ring buffer involves acquiring locks. This cant be done from a callback.
   Hence we post a event and LsimOdo_vTimeOutThread will add timeout record to ringbuffeer. */
   if(OSAL_OK != OSAL_s32EventPost(rOdoLSimInfo.hEventOdoTimeout,
                                   LSIM_ODO_TIMEOUT_EVENT,
                                   OSAL_EN_EVENTMASK_OR))
   {
      LsimOdo_vTraceOut(TR_LEVEL_ERROR, "OSAL_s32EventPost failed %u", __LINE__);
   }

}
/********************************************************************************
* FUNCTION        : LsimOdo_vThreadMain 

* PARAMETER       : pvDummyArg : Not used
*                                                      
* RETURNVALUE     : None
*
* DESCRIPTION     : This is the main and solo thread for odometer driver in LSIM. 
*                   This gets data from network dispatcher via mesage queue.
*                   Waits for the time specified in timestamp feild of odometer record.
*                   Updates the timestamp of odometer record to present system time
*                   using aux clock. Then it adds the record to Ring buffer.
*
* HISTORY         : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tVoid LsimOdo_vThreadMain(tPVoid pvDummyArg)
{
   tS32 s32ErrChk = OSAL_ERROR;
   trOdoMsgQueData rOdoDataRecord;
   tuRngBufDataPtr puRngBufDataPtr;
   OSAL_trIOCtrlAuxClockTicks rAuxClkTime;
   tS32 s32WaitTime = 0;
   puRngBufDataPtr.pOdoData = &rOdoDataRecord.rOdoSensorData;

   (tVoid)memset(&rOdoDataRecord,sizeof(rOdoDataRecord),0);

   /* Exit from the loop during shutdown */
   while(rOdoLSimInfo.bShutDownFlag != TRUE)
   {
      if(sizeof(trOdoMsgQueData) == OSAL_s32MessageQueueWait(rOdoLSimInfo.hDataSourceMsgQue,(tPU8)&rOdoDataRecord,sizeof(trOdoMsgQueData),
               OSAL_NULL,(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER))
      {
         /* We received data. So reset the timer */
         if(OSAL_OK != OSAL_s32TimerSetTime(rOdoLSimInfo.hTimerDataTimeout,
                                            ODOMETER_DATA_TIMEOUT_VALUE,
                                            ODOMETER_DATA_TIMEOUT_VALUE))
         {
            LsimOdo_vTraceOut(TR_LEVEL_ERROR,
            "Failed OSAL_s32TimerSetTime. Err Code %lu", OSAL_u32ErrorCode());
         }
         /*We got a odometer data. Reset Error counter */
         rOdoLSimInfo.u32OdoDataErrCnt = 0;
         /* Read from AUX clock for the latest timestamp */
         if(sizeof(rAuxClkTime) == AUXCLOCK_s32IORead(&rAuxClkTime, sizeof(rAuxClkTime)))
         { 
            /* Wait time is calculated and updated in  u32TimeStamp by odometer parser*/
            s32WaitTime = (tS32)rOdoDataRecord.rOdoSensorData.u32TimeStamp;

            if(s32WaitTime >= 0)
            {
               /*Update the timestamp of the record*/
               rOdoDataRecord.rOdoSensorData.u32TimeStamp =  rAuxClkTime.u32Low + (tU32)s32WaitTime;
               /*Wait for time specified by parser. This is needed to sync all sensor drivers.
                  (ODOMETER, GPS, GYRO, ABS)*/
               LsimOdo_vTraceOut(TR_LEVEL_USER_4,"Wait time %d  TS %lu", s32WaitTime,rAuxClkTime.u32Low);
               OSAL_s32ThreadWait((OSAL_tMSecond)s32WaitTime);
               /*Write data to ring buffer*/
               ODOMETER_u32WriteRngBuf( &rOdoLSimInfo.hr_odoRngBufHandle, puRngBufDataPtr );
            }
            else
            {
               LsimOdo_vTraceOut(TR_LEVEL_ERROR,
               "Invalid Timestamp from Parser.TS: %lu",
               rOdoDataRecord.rOdoSensorData.u32TimeStamp);
            }
         }
         else
         {
            LsimOdo_vTraceOut(TR_LEVEL_ERROR, "AUXCLOCK_s32IORead failed line %u", __LINE__);
         }
      }
      else
      {
         LsimOdo_vTraceOut(TR_LEVEL_ERROR, "Message que wait failed line %u", __LINE__);
      }
   }   
}

/********************************************************************************
* FUNCTION        : LsimOdo_vTimeOutThread 

* PARAMETER       : pvDummyArg : Not used
*
* RETURNVALUE     : None
*
* DESCRIPTION     : This thread is used to add timeout record into ring buffer. 
*                   A timer call back function will post timeout event to this thread.
*
*
* HISTORY         : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

static tVoid LsimOdo_vTimeOutThread(tPVoid pvDummyArg)
{
   tS32 s32ErrChk = OSAL_ERROR;
   OSAL_tEventMask u32EventResultMask = 0;

   /*This is the timeout record added to ringbuffe. This record is 
     updated to indicate appication regarding data timeout event.*/
   static OSAL_trIOCtrlOdometerData rOdoTimeoutRecord = ODOMETER_TIMEOUT_RECORD;
   tuRngBufDataPtr puRngBufDataPtr;
   puRngBufDataPtr.pOdoData = &rOdoTimeoutRecord;

   while(rOdoLSimInfo.bShutDownFlag != TRUE)
   {
      s32ErrChk = OSAL_s32EventWait(rOdoLSimInfo.hEventOdoTimeout,
                                   (OSAL_tEventMask)LSIM_ODO_TIMEOUT_EVENT|LSIM_ODO_SHUTDOWN_EVENT,
                                   OSAL_EN_EVENTMASK_OR,
                                   (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER,
                                    &u32EventResultMask);

      if(s32ErrChk == OSAL_OK)
      {
         /*Clear the event */
         if(OSAL_OK != OSAL_s32EventPost(rOdoLSimInfo.hEventOdoTimeout,~(u32EventResultMask),OSAL_EN_EVENTMASK_AND))
         {
            LsimOdo_vTraceOut(TR_LEVEL_ERROR, "OSAL_s32EventPost failed %u", __LINE__);
         }

         switch(u32EventResultMask)
         {
         case LSIM_ODO_TIMEOUT_EVENT:
            {
               /*Increment Error counter for every timer. This will be reset in 
               LsimOdo_vThreadMain when data is received.*/
               rOdoLSimInfo.u32OdoDataErrCnt++;

               LsimOdo_vTraceOut(TR_LEVEL_USER_4,"Timeout callback expaired");

               /* Update u16ErrorCounter entry of odometer record with  u32OdoDataErrCnt*/
               rOdoTimeoutRecord.u16ErrorCounter = rOdoLSimInfo.u32OdoDataErrCnt;

               /* Add timeout record to ring buffer */
               ODOMETER_u32WriteRngBuf( &rOdoLSimInfo.hr_odoRngBufHandle, puRngBufDataPtr);
               break;
            }
         case LSIM_ODO_SHUTDOWN_EVENT:
            {
               LsimOdo_vTraceOut(TR_LEVEL_USER_4,"Shut down event received");
               OSAL_vThreadExit();
            }
         default:
            {
               LsimOdo_vTraceOut(TR_LEVEL_ERROR,"Default in switch line %u %s " , __LINE__, __FUNCTION__);
               break;
            }
         }
      }
      else
      {
         LsimOdo_vTraceOut(TR_LEVEL_ERROR,"Event wait failed in LsimOdo_vTimeOutThread");
      }
   }
}

/********************************************************************************
* FUNCTION      : LsimOdo_vReleaseResource 

* PARAMETER     : u32ResourceID : Which resource has to be released.
*                                                      
* RETURNVALUE   : NONE.
*
* DESCRIPTION   : This Funcrion releases the resources specified.
*
* HISTORY       : 26.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tVoid LsimOdo_vReleaseResource(tU32 u32ResourceID)
{
   tU8 u8LoopCnt = 0;
   
   switch(u32ResourceID)
   {

      /* Closes all opened message queues. */
   case ODO_MSG_QUEUE:
      {
         if(OSAL_NULL != rOdoLSimInfo.hDataSourceMsgQue)
         {
            if(OSAL_OK == OSAL_s32MessageQueueClose(rOdoLSimInfo.hDataSourceMsgQue))
            {
               if(OSAL_OK != OSAL_s32MessageQueueDelete(ODOMETER_DATA_SOURCE_MSG_QUE))
               {
                  LsimOdo_vTraceOut(TR_LEVEL_ERROR, "MSG QUE Delete FAILED Err Code %lu",OSAL_u32ErrorCode());
               }
            }
            else
            {
               LsimOdo_vTraceOut(TR_LEVEL_ERROR, "MSG QUE Close FAILED Err Code %lu",OSAL_u32ErrorCode());
            }
         }
         break;
      }

      /* Closes and delete connection count semaphore. */
   case ODO_DATA_TIMER:
      {
         if(OSAL_OK != OSAL_s32TimerDelete(rOdoLSimInfo.hTimerDataTimeout))
         {
            LsimOdo_vTraceOut(TR_LEVEL_ERROR, "Timer Delete FAILED Err Code %lu",OSAL_u32ErrorCode());
         }
         break;
      }
      /* Closes and delete Driver response semaphore. */
   case ODO_RING_BUFFER:
      {
         if(OSAL_OK != ODOMETER_s32deInitRngBuf(&rOdoLSimInfo.hr_odoRngBufHandle))
         {
            LsimOdo_vTraceOut(TR_LEVEL_ERROR, "Ring Buffer Deinit FAILED");
         }
         break;
      }
      /* Send shutdown event and delete the event */
   case ODO_TIMEOUT_EVENT:
      {
         /*This post is needed to kill LsimOdo_vTimeOutThread */
         if(OSAL_OK != OSAL_s32EventPost(rOdoLSimInfo.hEventOdoTimeout,LSIM_ODO_SHUTDOWN_EVENT,OSAL_EN_EVENTMASK_OR))
         LsimOdo_vTraceOut(TR_LEVEL_ERROR, "OSAL_s32EventPost FAILED in line %u", __LINE__);
         
         if(OSAL_OK != OSAL_s32EventClose(rOdoLSimInfo.hEventOdoTimeout))
         LsimOdo_vTraceOut(TR_LEVEL_ERROR, "OSAL_s32EventClose FAILED in line %u", __LINE__);
         
         if(OSAL_OK != OSAL_s32EventDelete((tCString)LSIM_ODOMETER_TIMEOUT_EVENT_NAME))
         LsimOdo_vTraceOut(TR_LEVEL_ERROR, "OSAL_s32EventDelete FAILED in line %u", __LINE__);

         break;
      }
   default:
      {
         LsimOdo_vTraceOut(TR_LEVEL_FATAL,
         "Default reached in LsimOdo_vReleaseResource. There is some bug in code");
         break;
      }
   }

}

/********************************************************************************
* FUNCTION     : LsimOdo_vTraceOut

* PARAMETER    : u32Level - trace level, pcFormatString - Trace string

* RETURNVALUE  : tU32 error codes

* DESCRIPTION  : Trace out function for LSIM Odometer.

* HISTORY      : 26.SEP.2012| Initial Version    |Madhu Kiran Ramachandra (RBEI/ECF5)      
**********************************************************************************/
static tVoid LsimOdo_vTraceOut(  tU32 u32Level,const tChar *pcFormatString,... )
{
   if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_ODO, u32Level) != FALSE)
   {
      /*
      Parameter to hold the argument for a function, specified the format
      string in pcFormatString
      defined as:
      typedef char* va_list in stdarg.h
      */
      va_list argList = {0};
      /*
      vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure
      */
      tS32 s32Size ;
      /*
      Buffer to hold the string to trace out
      */
      tS8 u8Buffer[MAX_TRACE_SIZE];
      /*
      Position in buffer from where the format string is to be
      concatenated
      */
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /*
      Flush the String
      */
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /*
      Copy the String to indicate the trace is from the RTC device
      */

      /*
      Initialize the argList pointer to the beginning of the variable
      arguement list
      */
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
      Collect the format String's content into the remaining part of
      the Buffer
      */
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,
                  sizeof(u8Buffer),
                  pcFormatString,
                  argList ) ) )
      {
         return;
      }

      /*
      Trace out the Message to TTFis
      */
      LLD_vTrace( OSAL_C_TR_CLASS_DEV_ODO,
      u32Level,
      u8Buffer,
      (tU32)s32Size );   /* Send string to Trace*/
      /*
      Performs the appropiate actions to facilitate a normal return by a
      function that has used the va_list object
      */
      va_end(argList);
   }
}

/*End of file*/
