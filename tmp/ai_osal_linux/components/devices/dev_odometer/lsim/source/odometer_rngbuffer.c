/*******************************************************************************
 * Copyright (C) Blaupunkt GmbH, 2007
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 *******************************************************************************/
/*!
 *\file     
 *\brief    [Short description] This implements ring buffer feature for
 *                              ODodmeter driver in LSIM.
 *          [detailed description]
 *
 *\author   CM-DI/PJ-CF14 Joachim Frie�
 *
 *\par Copyright: (C) 2007 Blaupunkt GmbH
 *
 *\par History: [history] 
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 26.NOV.2012|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
* This part of the code is ported from Gen2 Tengine Odometer Driver.
* -----------------------------------------------------------------------
 *******************************************************************************/
 
/*******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local
|------------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "odometer_rngbuffer.h"

/*! \addtogroup sensor */
/*! @{*/

/*******************************************************************************
| defines and macros (scope: modul-local)
|------------------------------------------------------------------------------*/

/*******************************************************************************
| typedefs (scope: modul-local)
|------------------------------------------------------------------------------*/

/*******************************************************************************
| variable definition (scope: global)
|------------------------------------------------------------------------------*/

/*******************************************************************************
| variable definition (scope: modul-local)
|------------------------------------------------------------------------------*/

/*******************************************************************************
| function prototype (scope: modul-local)
|------------------------------------------------------------------------------*/

/*******************************************************************************
| function implementation (scope: modul-local)
|------------------------------------------------------------------------------*/


/******************************************************************************/
/*! 
 * \brief initializes the ring buffer for use
 * 
 * the init function will set all the variables of the buffer control structure
 *
 * \author CM-DI/PJ-CF14 Joachim Frie�
 *
 * \par History:  
 *
 ******************************************************************************/
tS32
ODOMETER_s32InitRngBuf( trRngBufferHandle *hr_rngBufferHandle,
                        tenRngBufferType eRngBufferType,
                        tU32 u32_sizeOfEntry,
                        tU32 u32_numElements ,
                        tuRngBufDataPtr u_bufPtr )
{
   static tU8 u8_rngBufNumber=0;
   tCString cosz_rngBufName = hr_rngBufferHandle->c_rngBufName;
   tU8 u8_numRemainder = 0;
   tS32 s32_return = OSAL_OK ;

   /* convert rngBufNumber to char */
   hr_rngBufferHandle->c_rngBufName[5] = u8_rngBufNumber / 100 + 0x30 ;
   u8_numRemainder = u8_rngBufNumber % 100;
   hr_rngBufferHandle->c_rngBufName[6] = u8_numRemainder / 10 + 0x30;
   u8_numRemainder = u8_rngBufNumber % 10;
   hr_rngBufferHandle->c_rngBufName[7] = u8_numRemainder + 0x30;

   /* create semaphore */
   if ( OSAL_OK 
        != 
        OSAL_s32SemaphoreCreate( cosz_rngBufName,
                                 &hr_rngBufferHandle->h_sem,
                                 1 ))
   {
      s32_return = OSAL_ERROR;
   }

   /* create events */
   if (OSAL_OK 
       != 
       OSAL_s32EventCreate( cosz_rngBufName,
                            &hr_rngBufferHandle->h_event ))
   {
      s32_return = OSAL_ERROR;
   }

   /* get semaphore */
   if (OSAL_OK 
       != 
       OSAL_s32SemaphoreWait( hr_rngBufferHandle->h_sem,
                              OSAL_C_TIMEOUT_FOREVER ))
   {
      s32_return = OSAL_ERROR;
   }   
   /* init datafields */
   hr_rngBufferHandle->u32_readIndex = 0;
   hr_rngBufferHandle->u32_writeIndex = 0;
   hr_rngBufferHandle->u32_numEntries = 0 ;
   hr_rngBufferHandle->u32_numMaxEntries = u32_numElements;
   hr_rngBufferHandle->u32_sizeOfBufferEntry = u32_sizeOfEntry; 
   hr_rngBufferHandle->en_typeOfBuffer = eRngBufferType;
   
   if (OSAL_NULL != u_bufPtr.pUndefined)
   {
      switch (eRngBufferType)
      {
      case ODO_RNG_BUF: 
         hr_rngBufferHandle->u_BufStartPtr.pOdoData = 
            u_bufPtr.pOdoData;
         break;
      case STEERING_RNG_BUF: 
         hr_rngBufferHandle->u_BufStartPtr.pSteeringData = 
            u_bufPtr.pSteeringData;
         break;
      case ABS_RNG_BUF: 
         hr_rngBufferHandle->u_BufStartPtr.pAbsData = 
            u_bufPtr.pAbsData;
         break ;
      case UNDEFINED_RNG_BUF: hr_rngBufferHandle->u_BufStartPtr.pUndefined = 
            u_bufPtr.pUndefined;
         break;
      default: 
         ;
      }
   }

   /* post semaphore */
   if (OSAL_OK != OSAL_s32SemaphorePost( hr_rngBufferHandle->h_sem ))
   {
      s32_return = OSAL_ERROR;
   }   

   u8_rngBufNumber++;
   return( s32_return );
}

/******************************************************************************/
/*! 
 * \brief destroys the ringbuffer
 * 
 * removes the semaphore and the eventgroup and resets the variables.
 *
 * \author CM-AI/PJ-CF31 Joachim Frie�
 *
 * \par History:  
 *
 ******************************************************************************/
tS32
ODOMETER_s32deInitRngBuf( trRngBufferHandle *hr_rngBufferHandle )
{
   tCString cosz_rngBufName = hr_rngBufferHandle->c_rngBufName;
   tS32 s32Ret = OSAL_ERROR;
   tS32 s32Return = OSAL_OK; 

   OSAL_s32SemaphoreWait( hr_rngBufferHandle->h_sem,
                          OSAL_C_TIMEOUT_FOREVER );

   hr_rngBufferHandle->u32_readIndex=0;
   hr_rngBufferHandle->u32_writeIndex=0;
   hr_rngBufferHandle->u32_numEntries=0;
   hr_rngBufferHandle->u32_reqEntries=0;
   hr_rngBufferHandle->en_typeOfBuffer=UNDEFINED_RNG_BUF;
   hr_rngBufferHandle->u32_sizeOfBufferEntry=0;
   hr_rngBufferHandle->u32_numMaxEntries=0;
   hr_rngBufferHandle->u_BufStartPtr.pUndefined = OSAL_NULL;

   /* delete semaphore */
   s32Ret = OSAL_s32SemaphoreDelete( cosz_rngBufName );

   if( OSAL_ERROR == s32Ret )
   {
      s32Return = OSAL_ERROR; 
   }

   hr_rngBufferHandle->h_sem = NULL;

   /* delete event */
   s32Ret = OSAL_s32EventDelete( cosz_rngBufName );
   if( OSAL_ERROR == s32Ret )
   {
      s32Return = OSAL_ERROR; 
   }

   hr_rngBufferHandle->h_event = NULL;

   return (s32Return);
}


/******************************************************************************/
/*! 
 * \brief writes one set of data to the ringbuffer 
 * 
 * [detailed discription]
 *
 * \author CM-DI/PJ-CF14 Joachim Frie�
 *
 * \par History:  
 *
 ******************************************************************************/
tU32 
ODOMETER_u32WriteRngBuf( trRngBufferHandle *hr_rngBufferHandle,
                         tuRngBufDataPtr pvSrc )
{
/*    tS32 s32_return = OSAL_OK ; */
   tS32 s32_OSALreturn = OSAL_OK;
   OSAL_tEventMask f_recievedEvent;

   if ( OSAL_OK 
        != 
        OSAL_s32SemaphoreWait( hr_rngBufferHandle->h_sem,
                               OSAL_C_TIMEOUT_FOREVER ))
   {
      /* error handling */
      /* s32_return = OSAL_u32ErrorCode(); */
   }

   /* if buffer is full */
   if ( hr_rngBufferHandle->u32_numEntries == hr_rngBufferHandle->u32_numMaxEntries )
   {
      /*  well ...  we have to throw away the oldest buffer entry */
      hr_rngBufferHandle->u32_readIndex++;
      hr_rngBufferHandle->u32_readIndex %= hr_rngBufferHandle->u32_numMaxEntries;
      hr_rngBufferHandle->u32_numEntries--;
   }

   /* write data to buffer */
   switch ( hr_rngBufferHandle->en_typeOfBuffer )
   {
   case ODO_RNG_BUF: 
      *( hr_rngBufferHandle->u_BufStartPtr.pOdoData 
         + 
         hr_rngBufferHandle->u32_writeIndex ) = *(pvSrc.pOdoData);
      break;
   case STEERING_RNG_BUF: 
      *( hr_rngBufferHandle->u_BufStartPtr.pSteeringData 
         + 
         hr_rngBufferHandle->u32_writeIndex ) = *(pvSrc.pSteeringData);
      break;
   case ABS_RNG_BUF: 
      *( hr_rngBufferHandle->u_BufStartPtr.pAbsData 
         + 
         hr_rngBufferHandle->u32_writeIndex ) = *(pvSrc.pAbsData);
      break;
   case UNDEFINED_RNG_BUF: 
      /* not implemented yet */
      break;
   default:
      ;
      /* there is no default action  */
   }
   /* adjust write index and number of entries */
   hr_rngBufferHandle->u32_writeIndex++;
   hr_rngBufferHandle->u32_writeIndex %= hr_rngBufferHandle->u32_numMaxEntries;
   hr_rngBufferHandle->u32_numEntries++;

   /* check pending data requests (after every buffer write) */
   s32_OSALreturn = OSAL_s32EventWait( hr_rngBufferHandle->h_event,
                                       RNGBUF_C_UN_EVENT_READ_REQ,
                                       OSAL_EN_EVENTMASK_OR,
                                       OSAL_C_TIMEOUT_NOBLOCKING,
                                       &f_recievedEvent );

   if (OSAL_OK == s32_OSALreturn)
   {
      if (RNGBUF_C_UN_EVENT_READ_REQ == (f_recievedEvent & RNGBUF_C_UN_EVENT_READ_REQ) )
      {
   
         if ( hr_rngBufferHandle->u32_numEntries 
              >= 
              hr_rngBufferHandle->u32_reqEntries )
         {
            /* clear request event */
            s32_OSALreturn = OSAL_s32EventPost( hr_rngBufferHandle->h_event,
                                                ~RNGBUF_C_UN_EVENT_READ_REQ,
                                                OSAL_EN_EVENTMASK_AND);

            /* send ready event */
            s32_OSALreturn = OSAL_s32EventPost( hr_rngBufferHandle->h_event,
                                                RNGBUF_C_UN_EVENT_READ_RDY,
                                                OSAL_EN_EVENTMASK_OR);
         }
      }
   }

   /* release semaphore */
   if ( OSAL_OK != OSAL_s32SemaphorePost( hr_rngBufferHandle->h_sem ))
   {
      /* error handling */
      /* s32_return = OSAL_u32ErrorCode(); */
   }

   return(  hr_rngBufferHandle->u32_sizeOfBufferEntry );
}


/******************************************************************************/
/*! 
 * \brief reads data elements from the buffer
 * 
 * If there are enough entries available they are simply copied to the
 * destination. Otherwise the read will wait until the requested
 * number of data packets has arrived. The timeout can be controlled
 * with each read.
 *
 * \author CM-DI/PJ-CF14 Joachim Frie�
 *
 * \par History:  
 * \par Todo: the request datafield has to be protected to avoid manipulation
 ******************************************************************************/
tU32
ODOMETER_u32ReadRngBuf( trRngBufferHandle *hr_rngBufferHandle, 
                        tU32 u32_reqBuffers,
                        tuRngBufDataPtr pvDest,
                        OSAL_tMSecond timeOut,
                        tS32 *s32RetError )
{
   OSAL_tEventMask rEventResultMask;
   tS32 s32_OSALreturn = OSAL_OK ;
   tU32 u32_cnt = 0;

   /* at this point everything is okay */
   *s32RetError=OSAL_OK ;
   /* number of requestet buffer is larger than the buffer size ->
      ERROR.  */   
   if ( ( u32_reqBuffers
          > 
          hr_rngBufferHandle->u32_numMaxEntries )
        ||
        ( u32_reqBuffers == 0 ) )
   {
      return( 0 );
   }

   /* check if there are enough entries in the buffer otherwise wait
      for signal data ready */

   if ( OSAL_OK 
        != 
        OSAL_s32SemaphoreWait( hr_rngBufferHandle->h_sem,
                               OSAL_C_TIMEOUT_FOREVER ))
   {
      *s32RetError = OSAL_ERROR;
   }


      /* if we want the data immediatly we get as much as there is available */
   if (0 == timeOut)
   {
      if (hr_rngBufferHandle->u32_numEntries < u32_reqBuffers)
      {
         hr_rngBufferHandle->u32_reqEntries = 
            hr_rngBufferHandle->u32_numEntries;
      }
      else
      {
         hr_rngBufferHandle->u32_reqEntries = u32_reqBuffers;
      }
      /* wait at least for one buf entry (this is needed by the
         vd_sensor logic!) with timeout
         RNGBUF_C_NOTIMOUT_TIMEOUT */
      if (0 == hr_rngBufferHandle->u32_numEntries )
      {
         hr_rngBufferHandle->u32_reqEntries = 1;
         timeOut = RNGBUF_C_NOTIMOUT_TIMEOUT;
      }
   }
   else
   {
      /* write number of elements requested to control structure */
      hr_rngBufferHandle->u32_reqEntries = u32_reqBuffers;
   }

   /* if the number of buffers is there get it otherwise wait for them */
   if ( hr_rngBufferHandle->u32_reqEntries 
        >
        hr_rngBufferHandle->u32_numEntries )
   {
      if (OSAL_OK != OSAL_s32SemaphorePost( hr_rngBufferHandle->h_sem ))
      {
         *s32RetError = OSAL_ERROR;
      }
   
      /* send request data event */
      if ( OSAL_OK 
           != 
           OSAL_s32EventPost( hr_rngBufferHandle->h_event,
                              RNGBUF_C_UN_EVENT_READ_REQ,
                              OSAL_EN_EVENTMASK_OR))
      {
         *s32RetError = OSAL_ERROR;
      }
   
      /* wait for data ready event */
      s32_OSALreturn = OSAL_s32EventWait( hr_rngBufferHandle->h_event,
                                          RNGBUF_C_UN_EVENT_READ_RDY,
                                          OSAL_EN_EVENTMASK_AND,
                                          timeOut,
                                          &rEventResultMask );

      /* was there a ready event? or was it a timeout */
      /* clear ready event */
      if (OSAL_OK == s32_OSALreturn)
      {
         if (RNGBUF_C_UN_EVENT_READ_RDY == (rEventResultMask & RNGBUF_C_UN_EVENT_READ_RDY) )
         {
            /* remove ready signal */
      s32_OSALreturn = OSAL_s32EventPost( hr_rngBufferHandle->h_event,
                                          ~RNGBUF_C_UN_EVENT_READ_RDY,
                                          OSAL_EN_EVENTMASK_AND);
         }
      }
      else
      {
         if (OSAL_E_TIMEOUT != OSAL_u32ErrorCode())
         {
            *s32RetError = OSAL_ERROR;            
         }
      
      }
      /* if there was an error we return with it */
      if ( (OSAL_OK != s32_OSALreturn) || (OSAL_ERROR == *s32RetError) )
      {
         return( 0 );
      }   
   
      /* get semaphore again */
      if ( OSAL_OK 
           != 
           OSAL_s32SemaphoreWait( hr_rngBufferHandle->h_sem,
                                  OSAL_C_TIMEOUT_FOREVER ))
      {
         *s32RetError = OSAL_ERROR;
      }
      /* Check if the number of buffers now matches the requested
         number. In case of a timeout there are probably less then the
         requested buffers available */
      if ( hr_rngBufferHandle->u32_numEntries 
           < 
           hr_rngBufferHandle->u32_reqEntries )
      {
         /* get as much buffers as there are available */
         hr_rngBufferHandle->u32_reqEntries = 
            hr_rngBufferHandle->u32_numEntries;
      }
   }
   
   /* copy data */   
   for ( u32_cnt=0; u32_cnt < hr_rngBufferHandle->u32_reqEntries ; u32_cnt++ )
   {
      switch ( hr_rngBufferHandle->en_typeOfBuffer )
      {
      case ODO_RNG_BUF: 
         *(pvDest.pOdoData + u32_cnt) = 
            *( hr_rngBufferHandle->u_BufStartPtr.pOdoData 
               + 
               hr_rngBufferHandle->u32_readIndex );
         break;
      case STEERING_RNG_BUF: 
         *(pvDest.pSteeringData + u32_cnt) = 
            *( hr_rngBufferHandle->u_BufStartPtr.pSteeringData 
               + 
               hr_rngBufferHandle->u32_readIndex );
         break;
      case ABS_RNG_BUF: 
         *(pvDest.pAbsData + u32_cnt) = 
            *( hr_rngBufferHandle->u_BufStartPtr.pAbsData 
               + 
               hr_rngBufferHandle->u32_readIndex );
         break;
      case UNDEFINED_RNG_BUF:
         /* not implemented yet */
         break;
      default:
         /* there is no default action */
         ;
      }
      /* adjust control structure */
      hr_rngBufferHandle->u32_readIndex++ ;
      hr_rngBufferHandle->u32_readIndex %= hr_rngBufferHandle->u32_numMaxEntries;
      hr_rngBufferHandle->u32_numEntries--;
   }

   if ( OSAL_OK != OSAL_s32SemaphorePost( hr_rngBufferHandle->h_sem ))
   {
      *s32RetError = OSAL_ERROR;
   }

   if( *s32RetError == OSAL_OK )
   {
      /* everything is fine return number of bytes copied */
      return( u32_cnt * hr_rngBufferHandle->u32_sizeOfBufferEntry );
   }
   else
   {
      return( 0 );
   }
}


/******************************************************************************/
/*! 
 * \brief flush the ringbuffer
 * 
 * This function erases all entries in the buffer
 *
 * \author CM-DI/PJ-CF14 Joachim Frie�
 *
 * \par History:  
 *
 ******************************************************************************/
tS32
ODOMETER_s32flushRngBuf( trRngBufferHandle *hr_rngBufferHandle )
{
   tS32 s32_return = OSAL_OK ;
/*    tS32 s32_OSALreturn ; */

   if ( OSAL_OK 
        != 
        OSAL_s32SemaphoreWait( hr_rngBufferHandle->h_sem,
                               OSAL_C_TIMEOUT_FOREVER ))
   {
      s32_return = OSAL_ERROR;
   }
   hr_rngBufferHandle->u32_readIndex = 0;
   hr_rngBufferHandle->u32_writeIndex = 0;
   hr_rngBufferHandle->u32_numEntries = 0;


   if ( OSAL_OK != OSAL_s32SemaphorePost( hr_rngBufferHandle->h_sem ))
   {
      s32_return = OSAL_ERROR;
   }

   return ( s32_return );
}


/******************************************************************************/
/*! 
 * \brief returns number of entries in the buffer (fill level)
 * 
 * This function returns the fill level of the buffer.
 *
 * \author CM-DI/PJ-CF14 Joachim Frie�
 *
 * \par History:  
 *
 ******************************************************************************/
tU32
ODOMETER_u32getLevelRngBuf( const trRngBufferHandle *hr_rngBufferHandle )
{
   tU32 u32_return = 0 ;
   /*    tS32 s32_OSALreturn ; */

   OSAL_s32SemaphoreWait( hr_rngBufferHandle->h_sem,
                          OSAL_C_TIMEOUT_FOREVER );

   u32_return = hr_rngBufferHandle->u32_numEntries;
   
   OSAL_s32SemaphorePost( hr_rngBufferHandle->h_sem );

   return (u32_return);
} 
 
