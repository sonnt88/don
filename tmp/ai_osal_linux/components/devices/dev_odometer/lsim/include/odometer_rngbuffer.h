/*******************************************************************************
* Copyright (C) Blaupunkt GmbH, 2007
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
*******************************************************************************/
/*!
*\file     
*\brief header file for the ring buffer implementation used in odometer
*       [detailed description]
*
*\author CM-DI/PJ-CF14 Joachim Frie�
*
*\par Copyright: (C) 2007 Blaupunkt GmbH
*
*\par History: [history] 
* 
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 26.NOV.2012|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
* This part of the code is ported from Gen2 Tengine Odometer Driver.
* -----------------------------------------------------------------------
 *******************************************************************************/
#ifndef ODOMETER_RNGBUFFER_H
#define ODOMETER_RNGBUFFER_H

/*******************************************************************************
| defines and macros (scope: global)
|------------------------------------------------------------------------------*/
#define RNGBUF_C_UN_EVENT_READ_REQ 0x01
#define RNGBUF_C_UN_EVENT_READ_RDY 0x02
#define RNGBUF_C_NAME "ODORB###"

#define RNGBUF_C_INIT_HANDLE { (OSAL_tSemHandle)NULL, \
                               (OSAL_tEventHandle)NULL,\
                               0,\
                               0,\
                               0,\
                               0,\
                               UNDEFINED_RNG_BUF,\
                               0,\
                               0,\
                               {(void *)NULL},\
                               RNGBUF_C_NAME\
}

#define RNGBUF_C_NOTIMOUT_TIMEOUT 1000

/*******************************************************************************
| typedefs (scope: global)
|------------------------------------------------------------------------------*/
typedef enum enRngBufferType
{
   ODO_RNG_BUF,
   STEERING_RNG_BUF,
   ABS_RNG_BUF,
   UNDEFINED_RNG_BUF
} tenRngBufferType;

typedef union uRngBufferPtr
{
   OSAL_trIOCtrlOdometerData *pOdoData;
   OSAL_trSteeringData *pSteeringData;
   OSAL_trAbsData *pAbsData;
   void *pUndefined;
} tuRngBufDataPtr;

typedef struct
{
   OSAL_tSemHandle h_sem;
   OSAL_tEventHandle h_event;
   tU32 u32_readIndex;
   tU32 u32_writeIndex;
   tU32 u32_numEntries;
   tU32 u32_reqEntries;
   tenRngBufferType en_typeOfBuffer;
   tU32 u32_sizeOfBufferEntry;
   tU32 u32_numMaxEntries;
   tuRngBufDataPtr u_BufStartPtr;
   char c_rngBufName[9];
} trRngBufferHandle;

/*******************************************************************************
| variable declaration (scope: global)
|------------------------------------------------------------------------------*/

/*******************************************************************************
| function prototypes (scope: global)
|------------------------------------------------------------------------------*/

tS32 ODOMETER_s32InitRngBuf( trRngBufferHandle *hr_RngBufferHandle,
                             tenRngBufferType enRngBufferType,
                             tU32 u32_sizeOfEntry,
                             tU32 u32_numElements,
                             tuRngBufDataPtr u_dataSrc );

tS32 ODOMETER_s32deInitRngBuf( trRngBufferHandle *hr_rngBufferHandle );

tU32 ODOMETER_u32WriteRngBuf( trRngBufferHandle *hr_rngBufferHandle,
                              tuRngBufDataPtr pvSrc );

tU32 ODOMETER_u32ReadRngBuf( trRngBufferHandle *hr_rngBufferHandle, 
                             tU32 u32_numElements,
                             tuRngBufDataPtr u_dataDest,
                             OSAL_tMSecond timeOut,
                             tS32 *s32RetError );

tS32 ODOMETER_s32flushRngBuf( trRngBufferHandle *hr_rngBufferHandle );

tU32 ODOMETER_u32getLevelRngBuf( const trRngBufferHandle *hr_rngBufferHandle );

#else
#error "odometer_rngbuffer.h included twice"
#endif  /* ODOMETER_RNGBUFFER_H */
