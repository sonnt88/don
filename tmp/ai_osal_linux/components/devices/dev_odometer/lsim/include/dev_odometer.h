/******************************************************************************
* FILE         : dev_odometer.h


* DESCRIPTION  : This is the header file for dev_odometer.c

* AUTHOR(s)    : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 17.OCT.2012|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#ifndef ODOMETER_HEADER_INCLUDE
#define ODOMETER_HEADER_INCLUDE

tS32 ODOMETER_s32CreateOdoDevice(tVoid);

tS32 ODOMETER_IOOpen(void);
tS32 ODOMETER_s32IOClose(void);
tS32 ODOMETER_s32IOControl(tS32 s32fun, tS32 s32arg);
tS32 ODOMETER_s32IORead(OSAL_trIOCtrlOdometerData* prIOOdoData, tU32 u32maxbytes);

tS32 ODOMETER_s32DestroyOdoDevice(tVoid);


#endif
