/*******************************************************************************
 *
 *
 * FILE:
 *     fd_device_ctrl.c
 *
 * REVISION:
 *      1.2
 *
 * AUTHOR:
 *     (c) 2010, Robert Bosch Gmbh., Ravindran P (RBEI CM-AI/PJ-CF31)
 *                                                   
 *
 * CREATED:
 *     23/12/2010 - Ravindran P 
 *
 * DESCRIPTION:
 *     This file contains the function definitions  
 *     for the module FD_device_ctrl
 *
 * NOTES:
 *
 * MODIFIED:
 *   DATE        |  AUTHOR        |   MODIFICATION
 *  23/12/2010   |  Ravindran P   |   Initial version
 *  05/04/2011   |  Anooj Gopi    |   Added wrapper function to read CID
 *  13/03/2012   |  Anooj Gopi    |   Added support to read CID from multiple devices.
 *  26/03/2012   |  MadhuSudhan   |   Added Traces 
 *  19/11/2012   | Ravikiran G    |  Added fix for nikai-435 and added new functions
 *                                |  to differenciate whether the SD Card is through
 *                                |  USB hub interface or direct Sd card interface
 *  19/02/2012   | Swathi Bolar   |  Added fix for nikai-2546 and updated the HW write
 *               |                |  protection status and Card size
 *  02/05/2013   | Selvan Krishnan  | Removed the hardcoded the drive name . 
                                 |Added new API for getting the drive name dynamically.
*  23/04/2014   | Padmashri Kudari  |Added fix for CFG3-606
                                 |Added check for OSAL_EN_DEVID_DEV_MEDIA in 
                                  fd_device_ctrl_u32SDCardInfo function.                             
*  20/06/2014   | Padmashri Kudari| FIx for SUZUKI Ticket CFG3-742 Improvement to
*                                 | make the IOCTLs OSAL_C_S32_IOCTRL_CARD_STATE
*                                 | and OSAL_C_S32_IOCTRL_FIOGET_CID thread safe 
* 28/10/2014    | SJA3KOR         | Added fix for SUZUKI-19311
*               |                 |(send read CID command only to SMSC card reader).  
* 04/11/2014   |Modified fix for SUZUKI-19311                       |SJA3KOR
*              |(avoided sync and creation of file). Added new      |
*              |function u32Sysfs_Get_Vendorid_Info to get vendorID |
* 19/12/2014   | removed compiler warning LONG_MAX redefined        | vew5kor
*
******************************************************************************/

/*****************************************************************
  | includes of component-internal interfaces
  | (scope: component-local)
  |--------------------------------------------------------------*/

/* OSAL Interface */
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include"fd_device_ctrl_trace.h"
#include "fd_device_ctrl_private.h"

#include <sys/stat.h>
#include <scsi/sg.h>
#include <sys/ioctl.h>

/*flags to enable/disable features*/	
#undef DEBUG_SIGNATURE_VERFICATION
#undef SIGN_VERI_SUCCESS_SIMULATION

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

#ifndef LONG_MAX
#define LONG_MAX 2147483647L
#define LONG_MIN (-LONG_MAX - 1L)
#endif

#define BASE_DECIMAL 10
#define BASE_HEX 16
#define SMSC_VENDER_ID "0424"     //VENDER_ID of SMSC USB SD CARD Reader.


/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/




/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-external)
|----------------------------------------------------------------*/
/*****************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/


/*****************************************************************
| function implementation (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/
extern tU32 u32CardDeviceName(tU8 *p8deviceName);

/******************************************************************************
 * FUNCTION:      fd_device_ctrl_u32UsbSdCardInfo
 *
 * DESCRIPTION:   Reads the card related data from via the GM USB hub
 *
 * PARAMETER:     prIOCtrlCardState - Card State structure
 *
 * PARAMETER:     fd - file descripter
 *                prIOCtrlCardState - Card State structure
 *
 * RETURNVALUE:   tU32 Error Code
 *
 * HISTORY:
 * Date        |   Modification                          | Authors
 * 23.12.10    |   Initial revision                      | Ravindran P
 * 12.03.12    |   Support added to read CID from -      |
 *                 multiple devices                      | Anooj Gopi (agn2cob)
 * 26.03.12    |   Trace Functions are added             
 *                 and update from 11.9 to 12 Label      | MadhuSudhan(swm2kor)
 * 19.11.2012  |   fix for nikai-435                     | Ravikiran Gundeti
 ******************************************************************************/
tU32 fd_device_ctrl_u32UsbSdCardInfo( tS32 fd, OSAL_trIOCtrlCardState* prIOCtrlCardState )
{
    tU8 cmd[6] = {0};
    tS32 len ;
    tU8 u8buffer[30];
    tPU8 pu8cid;
    tU8 u8_resp[MAX_RESP_SIZE] = {0};
    tU32  u32ErrorCode = OSAL_E_NOERROR;

    /* set initial scsi cdb content */
    cmd[0] = 0xCF;  /* 0: operation code */
    /* 1: vendor specific function code */
    cmd[2] = 0;	 /* 2: reserved */
    cmd[3] = 0;	 /* 3: reserved */
    /* 4: transfer length */
    cmd[5] = 0;	 /* 5: reserved */
    len = 6;     /* cdb length */

    cmd[1] = 0x18;
    cmd[4] = 16;
    if (s32Do_Card_Command(fd, cmd, len, u8_resp) == 0)
    {
        prIOCtrlCardState->u8ManufactureId = u8_resp[0];
        prIOCtrlCardState->u32SerialNumber = ((tU32)u8_resp[9] << 24) |
                                  ((tU32)u8_resp[10] << 16) |
                                      ((tU32)u8_resp[11] << 8)	|
                                      ((tU32)u8_resp[12]);
        OSAL_pvMemoryCopy(prIOCtrlCardState->u8CIDRegister, u8_resp, 16);
        fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                   "CID Register Details", 0, 0, 0, 0);
        pu8cid = prIOCtrlCardState->u8CIDRegister;
        OSAL_s32PrintFormat(u8buffer,"CID1:%x:%x:%x:%x:%x:%x:%x:%x",pu8cid[0],pu8cid[1],pu8cid[2],
                             pu8cid[3],pu8cid[4],pu8cid[5],pu8cid[6],pu8cid[7]);
        
        fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                   u8buffer,0,0,0,0);
        
        OSAL_s32PrintFormat(u8buffer,"CID2:%x:%x:%x:%x:%x:%x:%x:%x",pu8cid[8],pu8cid[9],pu8cid[10],
                             pu8cid[11],pu8cid[12],pu8cid[13],pu8cid[14],pu8cid[15]);
        fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                   u8buffer,0,0,0,0);
    }
    else
    {
        u32ErrorCode = OSAL_E_NOTSUPPORTED ;
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                   "SD Card command(0x18) failed", 0, 0, 0, 0);
    }
    cmd[1] = 0x1A;
    cmd[4] = 16;
    if (s32Do_Card_Command(fd, cmd, len, u8_resp) == 0)
    {
                /* check csd struct version */
        if (((u8_resp[0] >> 7) & 1) == 0)
        {
                /* get both permanent and temporary write protect */
            if (((u8_resp[14] >> 4) & 3) == 0)
            {
                prIOCtrlCardState->bSW_WriteProtected = 0;
                fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                            "Card - SW write Not Protected", 0, 0, 0, 0);
            }
            else
            {
                prIOCtrlCardState->bSW_WriteProtected = 1;
                fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                            "Card - SW write Protected", 0, 0, 0, 0);
            }
        }
        else
        {
            u32ErrorCode = OSAL_E_NOTSUPPORTED ;
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                      "CSD struct version Not Matched ", 0, 0, 0, 0);
        }
    }
    else
    {
        u32ErrorCode = OSAL_E_NOTSUPPORTED ;
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "SD Card command(0x1A) failed", 0, 0, 0, 0);
    }

    cmd[1] = 0x1B;
    cmd[4] = 8;
    if (s32Do_Card_Command(fd, cmd, len, u8_resp) == 0)
    {
            /* check scr struct version */
        if (((u8_resp[0] >> 4) & 0x0F) == 0)
        {
            /* get scr spec version */
            prIOCtrlCardState->u8SDCardSpecVersion = u8_resp[0] & 0x0F;
        }
        else
        {
            u32ErrorCode = OSAL_E_NOTSUPPORTED ;
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                      "Unable to get Card spec Version ", 0, 0, 0, 0);
        }
    }
    else
    {
        u32ErrorCode = OSAL_E_NOTSUPPORTED ;
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "SD Card command(0X1B) failed", 0, 0, 0, 0);
    }
    
    cmd[1] = 0xC1;
    cmd[4] = 65;
    if (s32Do_Card_Command(fd, cmd, len, u8_resp) == 0)
    {
        prIOCtrlCardState->bHW_WriteProtected = (u8_resp[64] >> 7) & 1;
        if(prIOCtrlCardState->bHW_WriteProtected == 0)
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                        "Card -  HW wite Not protected ", 0, 0, 0, 0);
        }
        else
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                        "Card - HW wite protected ", 0, 0, 0, 0);
        }
    }
    else
    {
        u32ErrorCode = OSAL_E_NOTSUPPORTED ;
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_USB_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "SD Card command(0XC1) failed", 0, 0, 0, 0);
    }

    return u32ErrorCode;
}

/******************************************************************************
 * FUNCTION:      fd_device_ctrl_u32DirectSDCardInfo
 *
 * DESCRIPTION:   Reads the card related data from direct SD card (Nissan LCN2 Kai).
 *
 * PARAMETER:     prIOCtrlCardState - Card State structure
 *
 * RETURNVALUE:   tU32 Error Code
 *
 * HISTORY:
 * Date        |   Modification                          | Authors
 * 19.11.2012  |   Initial revision                      | Ravikiran Gundeti
 * 19.11.2012  |   fix for nikai-435                     | Ravikiran Gundeti
 * 19.02.2013  |   fix for nikai-2546                    | Swathi Bolar
 * 02.05.2013  |   fix for nikai-734                    | Selvan Krishnan
 ******************************************************************************/

tU32 fd_device_ctrl_u32DirectSDCardInfo(OSAL_trIOCtrlCardState* prIOCtrlCardState )
{
    tU32    u32cid[4]={0};
    tU32    u32csd[4]={0};
    tU32    u32scr[2]={0};
    tU32    u32manfid = 0;
    tU32    u32serial[2]={0};
    tU8     u8index1 = 0;
    tU8     u8index2 = 0;
    tU8     u8index3 = 0;
    tU8     u8buffer[32]={0};
    tPU8    pu8cid;
    tU32    u32size;
    tU32    u32ErrorCode = OSAL_E_NOERROR;
    tBool   bPerm_sw_write_protect;
    tBool   bTmp_sw_write_protect;
    tU8     coszDevPath[50]="/sys/block/";
    tU8     DevPathLength; 
    tU32    u32Cardsize = 0;
    tU32    u32Hw_writeprotect = 0;
    
    memset(u8buffer,0,sizeof(u8buffer));

    u32CardDeviceName(&coszDevPath[strlen("/sys/block/")]);
    DevPathLength = (tU8)strlen ( coszDevPath);
 
    /*get the cid from sysfs*/ 
    strcpy(&coszDevPath[ DevPathLength],"/device/cid");

    //coszDevPath = "/sys/block/mmcblk1/device/cid";
    u32size = 32;
    if(OSAL_E_NOERROR != u32Sysfs_Get_Card_Info((tCString)coszDevPath, u32size, u32cid, BASE_HEX))
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "(cid)u32Sysfs_Get_Card_Info FAILED", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_NOACCESS;
    }
    else
    {
        /*
        we are getting the CID register content from the kernel in the following format.
        _________________________________________________________________________________
        |	3	2	1	0	|	7	6	5	4	|	11	10	9	8	|	15	14	13	12	|
        _________________________________________________________________________________
                u32cid[0] 	  		u32cid[1]			u32cid[2] 			u32cid[3]
    
        we are making it into prIOCtrlCardState->u8CIDRegister (below) format		   
        __________________________________________________________________________________
        | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |  10  |  11  |  12  |	13	|  14  |  15 | 
        __________________________________________________________________________________			  
        */
        for (u8index1 = 0, u8index2 = 3; u8index1 <= 15; u8index1++, u8index2--)
        {
            if((u8index1%4) == 0 )
                u8index3 = u8index1;
        
            prIOCtrlCardState->u8CIDRegister[u8index1] = (tU8) *(((char *) u32cid)+ u8index3 + u8index2 );
            
            if(u8index2 == 0)
                u8index2 = 4;
        }
        
        fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                "CID Register Details", 0, 0, 0, 0);
        pu8cid = prIOCtrlCardState->u8CIDRegister;
        OSAL_s32PrintFormat(u8buffer,"CID1:%x:%x:%x:%x:%x:%x:%x:%x",pu8cid[0],pu8cid[1],pu8cid[2],
                            pu8cid[3],pu8cid[4],pu8cid[5],pu8cid[6],pu8cid[7]);
        
        fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                   u8buffer,0,0,0,0);
        
        OSAL_s32PrintFormat(u8buffer,"CID2:%x:%x:%x:%x:%x:%x:%x:%x",pu8cid[8],pu8cid[9],pu8cid[10],
                             pu8cid[11],pu8cid[12],pu8cid[13],pu8cid[14],pu8cid[15]);
        fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                   u8buffer,0,0,0,0);
    }

    /*get the csd from sysfs*/
    //coszDevPath = "/sys/block/mmcblk1/device/csd";
    strcpy(&coszDevPath[ DevPathLength],"/device/csd");

    u32size = 32;
    if(OSAL_E_NOERROR != u32Sysfs_Get_Card_Info((tCString)coszDevPath, u32size, u32csd, BASE_HEX))
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                          "(csd)u32Sysfs_Get_Card_Info FAILED", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_NOACCESS;
    }
    else
    {
        /* check csd struct version */
        if (((u32csd[0] >> 7) & 0x00000001) == 0)
        {
                    /* get permanent SW write protect */
            bPerm_sw_write_protect = (tBool) (((u32csd[3] & 0x00002000)) >> 13);
                    /* get temporary SW write protect */
            bTmp_sw_write_protect = (tBool) (((u32csd[3] & 0x00001000)) >> 12);

            prIOCtrlCardState->bSW_WriteProtected = (bPerm_sw_write_protect || bTmp_sw_write_protect);
            if(prIOCtrlCardState->bSW_WriteProtected)
            {
                fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                            "Card - SW write Protected", 0, 0, 0, 0);
            }
            else
            {
                fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                            "Card - SW write Not Protected", 0, 0, 0, 0);
            }
        }
        else
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "CSD struct version Not Matched ", 0, 0, 0, 0);
            u32ErrorCode = OSAL_E_NOTSUPPORTED ;
        }
    }

    /*get the scr from sysfs*/
    //coszDevPath = "/sys/block/mmcblk1/device/scr";
    strcpy(&coszDevPath[ DevPathLength],"/device/scr");
    
    u32size = 16;
    if(OSAL_E_NOERROR != u32Sysfs_Get_Card_Info((tCString)coszDevPath, u32size, u32scr, BASE_HEX))
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "(scr)u32Sysfs_Get_Card_Info FAILED", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_NOACCESS;
    }
    else
    {
            /* check scr struct version */
        if (((u32scr[0] >> 4) & 0x0000000F) == 0)
        {
                    /* get scr spec version */
            prIOCtrlCardState->u8SDCardSpecVersion = (tU8) (((u32scr[0] & 0x0F000000)) >> 24);
        }
        else
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "Unable to get Card spec Version ", 0, 0, 0, 0);
            u32ErrorCode = OSAL_E_NOTSUPPORTED ;
        }
    }

    /*get the manufacture ID from sysfs*/
    //coszDevPath = "/sys/block/mmcblk1/device/manfid";
    strcpy(&coszDevPath[ DevPathLength],"/device/manfid");
 
    u32size = 8;
    if(OSAL_E_NOERROR != u32Sysfs_Get_Card_Info((tCString)coszDevPath, u32size, &u32manfid, BASE_HEX))
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "(manufacture ID)u32Sysfs_Get_Card_Info FAILED", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_NOACCESS;
    }
    else
    {
        prIOCtrlCardState->u8ManufactureId = (tU8)u32manfid;
    }

        /*get the serial number from sysfs*/
    //coszDevPath = "/sys/block/mmcblk1/device/serial";
    strcpy(&coszDevPath[ DevPathLength],"/device/serial");
 
    u32size = 16;//round of to the nearest multiple of 8
    if (OSAL_E_NOERROR != u32Sysfs_Get_Card_Info((tCString)coszDevPath, u32size,  u32serial, BASE_HEX))
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "(Serial Number)u32Sysfs_Get_Card_Info FAILED", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_NOACCESS;
    }
    else
    {
        prIOCtrlCardState->u32SerialNumber = ((u32serial[0]<<8)|(u32serial[1]));
    }
    /*get the hw write protection status from sysfs*/
    //coszDevPath = "/sys/block/mmcblk1/ro";
    strcpy(&coszDevPath[ DevPathLength],"/ro");

    u32size = 8; //round of to the nearest multiple of 8
    if (OSAL_E_NOERROR != u32Sysfs_Get_Card_Info((tCString)coszDevPath, u32size, &u32Hw_writeprotect, BASE_DECIMAL))
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "(HW_write protection)u32Sysfs_Get_Card_Info FAILED", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_NOACCESS;
    }
    else
    {
        prIOCtrlCardState->bHW_WriteProtected = (tBool)u32Hw_writeprotect;
        if (prIOCtrlCardState->bHW_WriteProtected)
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                    "Card - HW write Protected", 0, 0, 0, 0);
        }
        else
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_INFO,
                                    "Card - HW write Not Protected", 0, 0, 0, 0);
        }
    }
    //get the sd card size from sys fs
    //coszDevPath = "/sys/block/mmcblk1/size";
    strcpy(&coszDevPath[ DevPathLength],"/size");
   
    u32size = 8;
     if (OSAL_E_NOERROR != u32Sysfs_Get_Card_Info((tCString)coszDevPath, u32size, &u32Cardsize, BASE_DECIMAL))
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "(Card Size)u32Sysfs_Get_Card_Info FAILED", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_NOACCESS;
    }
    else
    {
        prIOCtrlCardState->u64CardSize =  ((tU64)u32Cardsize) * 512; //converting sectors into Bytes
    }
    prIOCtrlCardState->bMounted = 1;// This is true as FS is automount unlike paramount
    prIOCtrlCardState->u16UncleanUnmountCnt = 0; // Paramount specific members are made 0
    prIOCtrlCardState->u16UncleanWriteCnt = 0;
    prIOCtrlCardState->u16UncleanReadCnt = 0;
    return u32ErrorCode;
}

/******************************************************************************
 * FUNCTION:      fd_device_ctrl_u32SDCardInfo
 *
 * DESCRIPTION:   Reads the card related data from SD card
 *
 * PARAMETER:
 *                prIOCtrlCardState - Card State structure
 *                s32DeviceIdx -Device Index
 *
 * RETURNVALUE:   tU32 Error Code
 *
 * HISTORY:
 * Date        |   Modification                          | Authors
 * 23.12.10    |   Initial revision                      | Ravindran P
 * 12.03.12    |   Support added to read CID from -      |
 *                 multiple devices                      | Anooj Gopi (agn2cob)
 * 19.11.2012  |   fix for nikai-435                     | Ravikiran Gundeti
 * 23/04/2014 | fix for CFG3-606         | Padmashri Kudari(pmh5kor)
 			  added check for OSAL_EN_DEVID_DEV_MEDIA
* 20.06.14  |FIx for SUZUKI Ticket CFG3-742 Improvement to make the IOCTLs  |
*           |OSAL_C_S32_IOCTRL_CARD_STATE and OSAL_C_S32_IOCTRL_FIOGET_CID  |
*           |thread safe                                                    |pmh5kor
* 28/10/2014   |Added fix for SUZUKI-19311                       |SJA3KOR
*              |(send read CID command only to SMSC card reader).|
* 04/11/2014   |Modified fix for SUZUKI-19311                       |SJA3KOR
*              |(avoided sync and creation of file).                |
 ******************************************************************************/
 
tU32 fd_device_ctrl_u32SDCardInfo( OSAL_trIOCtrlCardState* prIOCtrlCardState,tS32 s32DeviceIdx )
{
    tS32 fd;
    tU32  u32ErrorCode = OSAL_E_NOERROR;
    tS32  s32CardIdx = s32DeviceIdx;
    tCString coszDevName = OSAL_NULL;
    tS8     venderid[10] = {0};
   
  /* Get the USB index for the OSAL device 
    if(s32DeviceId == (tS32)OSAL_EN_DEVID_DEV_MEDIA)
    {
        s32CardIdx = s32GetUsbIdx();
    }
    Get the Crypt index for crypt device
    else if(s32DeviceId == (tS32)OSAL_EN_DEVID_CRYPT_CARD)
    {
        s32CardIdx = s32GetCryptIdx();
    }
    else if (s32DeviceId == (tS32)OSAL_EN_DEVID_CARD2)
    {
        s32CardIdx = pOsalData->s32UsbIdxForCard2;
    }*/

    /* Get the Linux device name from USB index */
    if( s32CardIdx == FD_DEVICE_CTRL_EN_CARDIDX_SDA )
    {
        coszDevName = "/dev/sda";
    }
    else if( s32CardIdx == FD_DEVICE_CTRL_EN_CARDIDX_SDB )
    {
        coszDevName = "/dev/sdb";
    }
    else if( s32CardIdx == FD_DEVICE_CTRL_EN_CARDIDX_SDC)
    {
        coszDevName = "/dev/sdc";
    }
    else if( s32CardIdx == FD_DEVICE_CTRL_EN_CARDIDX_SDD)
    {
        coszDevName = "/dev/sdd";
    }
    else if( s32CardIdx == FD_DEVICE_CTRL_EN_CARDIDX_SDE )
    {
        coszDevName = "/dev/sde";
    }
    else if( s32CardIdx == FD_DEVICE_CTRL_EN_CARDIDX_SDF)
    {
        coszDevName = "/dev/sdf";
    }
    else if( s32CardIdx == FD_DEVICE_CTRL_EN_CARDIDX_SDG)
    {
        coszDevName = "/dev/sdg";
    }
    else if( s32CardIdx == FD_DEVICE_CTRL_EN_CARDIDX_SDH)
    {
        coszDevName = "/dev/sdh";
    }

	if( s32CardIdx >= FD_DEVICE_CTRL_EN_CARDIDX_MMCBLK1)//the index can be > 8 in case if the previous eject of the card was done without properly unmounting the file system.
	{
		if(OSAL_E_NOERROR != fd_device_ctrl_u32DirectSDCardInfo(prIOCtrlCardState))
		{
			fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                   	                     "FAILED fd_device_ctrl_u32DirectSDCardInfoe FAILED", 0, 0, 0, 0);
            u32ErrorCode = OSAL_E_NOACCESS;
		}
	}
    /* Linux devices from sda to sdh */
    else if ( coszDevName )
    {
        fd = open(coszDevName, O_RDONLY|O_NONBLOCK);
        if (fd < 0)
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "Opening of linux device FAILED", 0, 0, 0, 0);
            u32ErrorCode = OSAL_E_NOACCESS;
        }
        else
        {
            u32ErrorCode = u32Sysfs_Get_Vendorid_Info(coszDevName ,(tChar*)&venderid[0]);
            if(u32ErrorCode == OSAL_E_NOERROR)
            {
             /*Send SCSI commands only to SMSC usb sdCARD reader.
                VENDER ID of SMSC usb sdCARD reader is 0424*/
                if(strncmp(SMSC_VENDER_ID,(tChar*)&venderid[0],strlen(SMSC_VENDER_ID))== 0)
                {
                    u32ErrorCode = fd_device_ctrl_u32UsbSdCardInfo(fd, prIOCtrlCardState);
                } 
                else
                {
                    
                    fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "NOT a SMSC-USB SDcard reader",0, 0, 0, 0);

                    u32ErrorCode = OSAL_E_NOTSUPPORTED;
                }
            }
            else
            {
                fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "Get Vendorid Failed", 0, 0, 0, 0);
            }
            close(fd);
        }
       
    }
    else
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_GET_SDCARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "Unknown Linux Device", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_NOTSUPPORTED;
    }

    return u32ErrorCode;
}
/******************************************************************************
 * FUNCTION:      s32Do_Card_Command
 *
 * DESCRIPTION:   SCSI commands specific to GM USB hub manufacturer
                             Ported from the test code given by Martin Mueller
 *
 * PARAMETER:
 *
 *
 * RETURNVALUE:   tS32 Error Code
 *
 * HISTORY:
 * Date        |   Modification                         | Authors
 * 23.12.10  |   Initial revision                      | Ravindran P
 * 23.3.12   |   Addtion of trace funtions             | MadhuSudhan(swm2kor)   
 ******************************************************************************/
static tS32 s32Do_Card_Command(tS32 fd, tU8*cmd, tS32 len, tU8* u8_resp) 
{
   sg_io_hdr_t io;
   tS32 s32RetVal = 0;

   OSAL_pvMemorySet(u8_resp, 0, MAX_RESP_SIZE);

   OSAL_pvMemorySet(&io, 0, sizeof(io));
   io.interface_id = 'S';
   io.cmd_len = (tU8) len;
   io.mx_sb_len = 0;
   io.dxfer_direction = SG_DXFER_FROM_DEV;
   io.dxfer_len = MAX_RESP_SIZE;
   io.dxferp = u8_resp;
   io.cmdp = cmd;
   io.flags |= SG_FLAG_LUN_INHIBIT;

   if (ioctl(fd, SG_IO, &io) < 0)
   {
      fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_DO_CARD_COMMAND, TR_DEVICE_CTRL_MSG_ERROR,
                                 "ioctl() failed with error no:",errno, 0, 0, 0);
      s32RetVal = -1;
   }
   else if ((io.info & SG_INFO_OK_MASK) != SG_INFO_OK)
   {
      fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_DO_CARD_COMMAND, TR_DEVICE_CTRL_MSG_ERROR,
                                 "ioctl()passed but SG_info Not matched", 0, 0, 0, 0);
      s32RetVal = -1;
   }

   return(s32RetVal);
}
/******************************************************************************
 * FUNCTION:      fd_device_ctrl_vCIDPatternAdapt
 *
 * DESCRIPTION:   Adapts the read CID as per the signature generated.
 *
 * PARAMETER: tU8* pu8CIDRegister - pointer for CID register
 *                     Ported from Gen1 Implementation
 *
 * RETURNVALUE:   tVoid
 *
 * HISTORY:
 * Date        |   Modification                         | Authors
 * 23.12.10  |   Initial revision                      | Ravindran P
 ******************************************************************************/
tVoid fd_device_ctrl_vCIDPatternAdapt(tU8* pu8CIDRegister)
{
    
     tU8 u8NumDataBits = NUM_CID_STREAM_BITS;
     tU8 u8ShiftReg = 0x00; 
     tU8 u8BitMask = 0x80;
     tU8 u8Position = 0;
     tU8 u8Cnt = 0;
     tU8 u8LimitCheck = 0;
     tU8 u8NextBit = 0;
     tS8 s8CntReverse;
     tU8 u8CRC7;
     tU8 u8CIDReg[CID_LEN] = {0};

     fd_device_ctrl_vTraceInfo(TR_LEVEL_USER_4, TR_DEVICE_CTRL_CID_PATTERN_ADAPT, TR_DEVICE_CTRL_MSG_ERROR,
                                "Enter CID Pattern adapt ", 0, 0, 0, 0);
    /*Reverse the sequence of bytes to compensate for the reversal of bytes made by SD card 
    data read via USB*/

     for( s8CntReverse=15;s8CntReverse>=0;s8CntReverse--)
     {
          u8CIDReg[s8CntReverse]= pu8CIDRegister[u8Cnt++] ; 
     }

     /*Compute CRC7.Logic taken from SD wrappers*/
     u8CRC7 = 0x00;	// Must be 0 to get correct CRC7
     u8CIDReg[0] = 0x01;

    
     for(u8Cnt = u8NumDataBits; u8Cnt > 0; u8Cnt--)
     {
          u8Position = (u8Cnt >> 3);
          u8LimitCheck = (u8Cnt % 8);
          if(u8LimitCheck == 7)
       {
           u8BitMask = 0x80;
       }

          /* Get next bit from data stream */	
          if((u8CIDReg[u8Position] & u8BitMask) > 0)
          { 	
	       u8NextBit = 1;			
	   }
	   else	
          {
               u8NextBit = 0;
          } 
          /* Polynom division */
          u8ShiftReg = (tU8)(((tU16)u8ShiftReg << 1) | u8NextBit); 	
          if(u8ShiftReg & 0x80)		// Test highest bit of SR
          {
               u8ShiftReg = (u8ShiftReg ^ GP_CRC7);
           }
	   
	    u8BitMask >>= 1;
	} 

	
       u8ShiftReg &= 0x7F;
       u8ShiftReg <<= 1;
       u8CRC7 |= (u8ShiftReg | 0x01);

       u8CIDReg[0] = u8CRC7; 

       (tVoid)OSAL_pvMemoryCopy( pu8CIDRegister,u8CIDReg,sizeof(u8CIDReg) );
    
	
}

/*****************************************************************************
 * FUNCTION:
 *     fd_device_ctrl_u32ReadCid
 *
 * DESCRIPTION:
 *     Reads the CID from SD card connected to GM Hub.
 *
 * PARAMETERS:
 *      
 *      pu8Cid:    (Out)  Pointer to CID data (tU8 au8CID[CID_LEN])
 *      s32DeviceIndx: (In) Device Index returned by the PRM
 *
 * RETURNVALUE:
 *     OSAL_E_NOERROR     - Success
 *     OSAL_E_IOERROR     - Error while reading CID
 *
 * HISTORY:
 *   DATE      |  AUTHOR     |         MODIFICATION
 *   24/02/11  | Anooj Gopi  |          Created
 *   12/03/12  | Anooj Gopi  |  Added support for multiple devices
 *   26/03/12  | MadhuSudhan |  Added trace functions 
 * 19.11.2012  | Ravikiran G |  changed function name (fix for nikai-435)
 *  20.06.14   |Padmashri K  | FIx for SUZUKI Ticket CFG3-742 
 *                           |updated the function with one more argument s32DeviceIndx
 *                           |and removed an argument s32DeviceId
 *****************************************************************************/
tU32 fd_device_ctrl_u32ReadCid( tPU8 pu8Cid , tS32 s32DeviceIndx)
{
   OSAL_trIOCtrlCardState trIOCtrlCardState;
   tU32  u32ErrorCode = OSAL_E_IOERROR;

   /* Get the SD card info via USB interface */
   OSAL_pvMemorySet( &trIOCtrlCardState, 0, sizeof(OSAL_trIOCtrlCardState) );
   
   if ( OSAL_E_NOERROR == fd_device_ctrl_u32SDCardInfo( &trIOCtrlCardState ,s32DeviceIndx ) )
   {
      /* Adapt the CID pattern as expected by the signature verification function */
      fd_device_ctrl_vCIDPatternAdapt( trIOCtrlCardState.u8CIDRegister );
      OSAL_pvMemoryCopy( pu8Cid, trIOCtrlCardState.u8CIDRegister, CID_LEN);
      u32ErrorCode = OSAL_E_NOERROR;
   }
   else
   {
      fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_READ_CID, TR_DEVICE_CTRL_MSG_ERROR,
                                 "Card Information Not Avaliable", 0, 0, 0, 0);
   }

   return u32ErrorCode;
}

/******************************************************************************
 * FUNCTION:      u32Sysfs_Get_Card_Info
 *
 * DESCRIPTION:  get the card related data from sysfs
 *
 * PARAMETER:   coszDevPath - path to get the required info like cid,csd etc..
 *              u32size - size of bytes to be read. 
 *              u32_resp - response
 *
 * RETURNVALUE:   tU32 Error Code
 *
 * HISTORY:
 * Date         |       Modification                      | Authors
 * 29.11.2012   |   Initial revision                      | Ravikiran Gundeti
 * 19.02.2013   |   Retry mechanism for incomplete read   | Swathi Bolar
 ******************************************************************************/

static tU32 u32Sysfs_Get_Card_Info(tCString coszDevPath, tU32 u32size, tPU32 u32_resp, tU8 u8base)
{
    tS32    s32fd ;
    tU8     u8buffer[32] = {0};
    tU8     u8index = 0;
    tU8     u8temp[9] = {0};
    tU32    u32conval = 0;
    tU32    u32ErrorCode = OSAL_E_NOERROR;
    tU32    u32Read = 0; //indicates the number of characters read
    tU8    u8Retryflag = 1; //to retry reading incase of incomplete read
    OSAL_pvMemorySet(u32_resp, 0, (u32size/8));
    if (coszDevPath == OSAL_NULL)
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "Unknown Path", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_NOACCESS;
    }
    else
    {
        s32fd = open(coszDevPath,O_RDONLY);
        if (s32fd < 0)
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "Opening of coszDevPath FAILED", 0, 0, 0, 0);
            u32ErrorCode = OSAL_E_NOACCESS;
        }
        else if (u32size>32)
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "Buffer size exceeded- Read Failed", 0, 0, 0, 0);
            u32ErrorCode = OSAL_E_NOACCESS;
        }
        else
        {
             do
            {
                memset(u8buffer,0,sizeof(u8buffer));
                u32Read = (tU32)read(s32fd, u8buffer, u32size) ;
                if ((tS32)u32Read <= 0)
                {
                    fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                                "read FAILED", 0, 0, 0, 0);
                    u32ErrorCode = OSAL_E_NOACCESS;
                    break; 
                }
                if (u32Read < u32size)
                {
                    if (u8Retryflag == 1)
                    {
                        if (lseek(s32fd, 0, SEEK_SET) == -1)
                        {
                            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                                      "seek FAILED", 0, 0, 0, 0);
                            u32ErrorCode = OSAL_E_NOACCESS;
                            break;
                        }
                        continue;
                    }
                    else
                    {
                        u32Read = u32size; //No of characters in the file is less than the actual size expected
                    }
                }
                if (u32Read == u32size)
                {
                    for(u8index = 0; u8index <(u32size/8); u8index++)
                    {
                        errno = 0;
                        memset(u8temp,0,sizeof(u8temp));
                        strncpy(u8temp, &u8buffer[8*u8index], 8);
                        u32conval=strtoul(u8temp, NULL, u8base);
                        /* Check for various possible errors for strtoul */
                        if ((errno == ERANGE && (u32conval == (tU32)LONG_MAX || u32conval == (tU32)LONG_MIN))
                                    || (errno != 0 && u32conval == 0))
                        {
                            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                                                        "Converted Value out of range (strtoul)", 0, 0, 0, 0);
                            u32ErrorCode = OSAL_E_NOACCESS;
                        }
                        else
                            u32_resp[u8index] = u32conval;
                    }
                    break;
                }
            }while(u8Retryflag--);
            close(s32fd);
        }   
    }
    return u32ErrorCode;
}
/******************************************************************************
 * FUNCTION:      u32Sysfs_Get_Vendorid_Info
 *
 * DESCRIPTION:  get the vendorId from sysfs
 *
 * PARAMETER:   coszDevPath - path to get VendorId.
 *            	    SVendorid - Pointer to store VenderId string.
 * RETURNVALUE:   tU32 Error Code
 *
 * HISTORY:
 * Date         |       Modification                      | Authors
 * 04.11.2014   |   Initial revision                      | sja3kor
 ******************************************************************************/
static tU32 u32Sysfs_Get_Vendorid_Info(tCString coszDevPath, tString SVendorid )
{
    tS32    s32fd ;
    tS8     s8buffer[30] = {0};
    tU32    u32ErrorCode = OSAL_E_NOERROR;
    tU32    u32Read = 0; //indicates the number of characters read
    tU32    u32BytesToRead= 4;
    tU8     u8Retryflag = 1; //to retry reading incase of incomplete read
    tS8     coszDevName[20]={0} ;
    tS8     s8snbuf[256]={0};
    tS8     s8fnbuf[256]={0};
    tU32    i;
    struct stat st;

    if (coszDevPath == OSAL_NULL)
    {
        fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                    "Unknown Path", 0, 0, 0, 0);
        u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
    else
    {
        //refer device first partition to get the vendorid info..
        strncpy((tChar*)&coszDevName[0],coszDevPath,strlen(coszDevPath));
        strncat((tChar*)&coszDevName[0],"1",strlen("1"));

        if(stat((tChar*)&coszDevName[0], &st) == 0)
        {
            //Get the major and minor no. from device partition.
            snprintf((tChar*)&s8snbuf[0],255, "/sys/dev/block/%d:%d",major(st.st_rdev),minor(st.st_rdev));
            for (i = 0; i < 10; ++i) 
            {
                strcpy((tChar*)&s8fnbuf[0],(tChar*)&s8snbuf[0]);
                strcat((tChar*)&s8fnbuf[0], "/idVendor");
                if (stat((tChar*)&s8fnbuf[0], &st) == 0) 
                {
                    break;
                }
                strcat((tChar*)&s8snbuf[0], "/..");
            }
        }
        else
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "stat for coszDevName Failed", 0, 0, 0, 0);
            return OSAL_E_UNKNOWN ;
        }
        
        if(i>=10)
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "File idVendor Not Found", 0, 0, 0, 0);
            return OSAL_E_UNKNOWN ;
        }

        s32fd = open((tChar*)&s8fnbuf[0],O_RDONLY);
        if (s32fd < 0)
        {
            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                        "Opening of file idVendor FAILED", 0, 0, 0, 0);
            u32ErrorCode = OSAL_E_INVALIDVALUE;
        }
        else
        {
             do
            {
                u32Read = (tU32)read(s32fd,&s8buffer[0], u32BytesToRead) ;
                if ((tS32)u32Read <= 0)
                {
                    fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                                "VendorId read FAILED", 0, 0, 0, 0);
                    u32ErrorCode = OSAL_E_UNKNOWN;
                    break; 
                }
                if (u32Read < u32BytesToRead)
                {
                    if (u8Retryflag == 1)
                    {
                        if (lseek(s32fd, 0, SEEK_SET) == -1)
                        {
                            fd_device_ctrl_vTraceInfo(TR_LEVEL_ERRORS, TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO, TR_DEVICE_CTRL_MSG_ERROR,
                                                      "seek FAILED", 0, 0, 0, 0);
                            u32ErrorCode = OSAL_E_UNKNOWN;
                            break;
                        }
                        continue;
                    }
                    else
                    {
                        u32Read = u32BytesToRead; //No of characters in the file is less than the actual size expected
                    }
                }
                if (u32Read == u32BytesToRead)
                {
                   //copy vendorid..
                    u8Retryflag = 0; //make the retryflag 0 in case of (u32Read == u32BytesToRead)
                    strncpy(SVendorid,(tChar*)&s8buffer[0],u32Read);
                }
            }while(u8Retryflag--);
            close(s32fd);
        }   
    }
    return u32ErrorCode;
}
