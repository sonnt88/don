/*******************************************************************************
*
* FILE:
*     fd_device_ctrl_trace.c
*
* REVISION:
*     1.0
*
* AUTHOR:
*     (c) 2012, Robert Bosch India Ltd., ECM/ECF5,MadhuSudhan Swargam, 
*                                                 MadhuSudhan.Swargam@in.bosch.com
*
* CREATED:
*     21/03/2012 - MadhuSudhan Swargam 
*
* DESCRIPTION:
*     This file contains the FD DEVICE CTRL Driver Trace Messages Code
*
* NOTES:
*
* MODIFIED:
*	  
*
*******************************************************************************/

/**************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|----------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"

/************************************************************************
| includes of interfaces from external components (scope: global)
|-----------------------------------------------------------------------*/

#include "fd_device_ctrl_trace.h"		  

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototype (scope: module-global)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:
*     lld_mmc_vTraceInfo
*
* DESCRIPTION:
*     This function creates the trace message and sends it to PC
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
* HISTORY:
* Date        |   Modification                          | Authors
* 21.03.12    |   Initial revision                      | MadhuSudhan S
*
*****************************************************************************/
tVoid fd_device_ctrl_vTraceInfo(TR_tenTraceLevel enTraceLevel,
                         tenFdDeviceCtrlTraceFunction enFunction,
                         tenFdDeviceCtrlTraceMsg enLldMmcTraceMsg,
                         tPCChar copchDescription,
                         tS32 s32Par1, 
                         tS32 s32Par2, 
                         tS32 s32Par3, 
                         tS32 s32Par4)
{
   if(LLD_bIsTraceActive((tU32)FD_DEVICE_CTRL_TRACE_CLASS,(tU32)enTraceLevel) == TRUE)
   {
      tU8 au8Buf[3 * sizeof(tU8) + 30 * sizeof(tChar) + 5 * sizeof(tS32)];
      tU32 u32ThreadID;
      tChar ch;
      tInt i;

      u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
      OSAL_M_INSERT_T8(&au8Buf[0], (tU8)enLldMmcTraceMsg);
      OSAL_M_INSERT_T32(&au8Buf[1], u32ThreadID);
      OSAL_M_INSERT_T8(&au8Buf[5], (tU8)enFunction);
      for (i = 0; i < 29; ++i)
      {
         ch = copchDescription[i];
         OSAL_M_INSERT_T8(&au8Buf[6 + i], (tU8) ch);
         if (ch == '\0') break;
      }
      OSAL_M_INSERT_T8(&au8Buf[35], (tU8) '\0');
      OSAL_M_INSERT_T32(&au8Buf[36], (tU32)s32Par1);
      OSAL_M_INSERT_T32(&au8Buf[40], (tU32)s32Par2);
      OSAL_M_INSERT_T32(&au8Buf[44], (tU32)s32Par3);
      OSAL_M_INSERT_T32(&au8Buf[48], (tU32)s32Par4);

      LLD_vTrace((tU32)FD_DEVICE_CTRL_TRACE_CLASS,
                 (tU32)enTraceLevel,
                 &au8Buf[0], 
                 sizeof(au8Buf));
   }

}
