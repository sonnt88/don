/*******************************************************************************
*
* FILE:
*     fd_device_ctrl_trace.h
*
* REVISION:
*     1.
*
* AUTHOR:
*     (c) 2012, Robert Bosch India Ltd., ECM/ECF5, MadhuSudhan Swargam, 
*                                                  MadhuSudhan.Swargam@in.bosch.com
*
* CREATED:
*     21/03/2012 - MadhuSudhan Swargam 
*
* DESCRIPTION:
*     This file contains all the definitions, macros, and types that 
*     are private to the fd_device_ctrl_trace.c
*
* NOTES:
*
* MODIFIED:
*   DATE      |  AUTHOR     |         MODIFICATION
* 19.11.2012  | Ravikiran G |   Added new trace classes for newly added functions
*******************************************************************************/
#ifndef _FD_DEVICE_CTRL_TRACE_H
#define _FD_DEVICE_CTRL_TRACE_H
#define  FD_DEVICE_CTRL_TRACE_CLASS OSAL_C_TR_CLASS_FD_DEVICE_CTRL

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/* Trace messages */  
typedef enum 
{
   TR_DEVICE_CTRL_MSG_SUCCESS,
   TR_DEVICE_CTRL_MSG_ERROR,
   TR_DEVICE_CTRL_MSG_INFO,
   TR_DEVICE_CTRL_MSG_STRING,
   TR_DEVICE_CTRL_MSG_FNER

} tenFdDeviceCtrlTraceMsg;

/* Trace functions enumeration */
typedef enum
{
   TR_DEVICE_CTRL_GET_USB_SDCARD_INFO,
   TR_DEVICE_CTRL_DO_CARD_COMMAND,
   TR_DEVICE_CTRL_CID_PATTERN_ADAPT,
   TR_DEVICE_CTRL_READ_CID,
   TR_DEVICE_CTRL_GET_SDCARD_INFO,
   TR_DEVICE_CTRL_GET_DIRECT_SDCARD_INFO,
   TR_DEVICE_CTRL_SYSFS_GET_CARD_INFO
} tenFdDeviceCtrlTraceFunction;


/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/
 
/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:
*     fd_device_ctrl_vTraceInfo
*
* DESCRIPTION:
*     This function creates the trace message and sends it to PC*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None	       
*
* HISTORY:
*
*****************************************************************************/
tVoid fd_device_ctrl_vTraceInfo(TR_tenTraceLevel enTraceLevel,
                         tenFdDeviceCtrlTraceFunction enFunction,
                         tenFdDeviceCtrlTraceMsg enLldMmcTraceMsg,
                         tPCChar copchDescription,
                         tS32 s32Par1, 
                         tS32 s32Par2, 
                         tS32 s32Par3, 
                         tS32 s32Par4);


#endif /* #ifndef _FD_DEVICE_CTRL_TRACE_H */ 
