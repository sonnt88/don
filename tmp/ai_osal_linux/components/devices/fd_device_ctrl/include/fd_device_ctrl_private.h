/*******************************************************************************
 *
 * FILE:
 *     fd_device_ctrl_private.h
 *
 * REVISION:
 *	   1.1
 *
 * AUTHOR:
 *     (c) 2010, Robert Bosch Gmbh., Ravindran P (RBEI CM-AI/PJ-CF31)
*                                                   
 *
 * CREATED:
 *     23/12/2010 - Ravindran P 
 *
 * DESCRIPTION:
 *     This file contains the private API functions & data elements 
 *     for the module FD_device_ctrl
 *
 * NOTES:
 *
 * MODIFIED:
 *   DATE       |  AUTHOR        |         MODIFICATION
 *  23/12/2010  |  Ravindran P   |   Initial version
 *  19.11.2012  |  Ravikiran G   |   Added new function and enums for card index values
 *  19.02.2012  |  Swathi Bolar  |   Introduced new parameter for base conversion
 *              |                |    in function u32Sysfs_Get_Card_Info 
 *  04.11.2014 |  sja3kor |   Added new function prototye for "u32Sysfs_Get_Vendorid_Info"
 ******************************************************************************/

#ifndef _FD_DEVICE_CTRL_PRIVATE_H
#define _FD_DEVICE_CTRL_PRIVATE_H

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|----------------------------------------------------------------*/
			

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

static tS32 s32Do_Card_Command(tS32 fd, tU8*cmd, tS32 len, tU8* u8_resp);
static tU32 u32Sysfs_Get_Card_Info(tCString coszDevPath, tU32 u32size, tPU32 u8_resp, tU8 u8base);
static tU32 u32Sysfs_Get_Vendorid_Info(tCString coszDevPath,tString SVendorid );



/* enum for card index values*/
enum
{
	FD_DEVICE_CTRL_EN_CARDIDX_UNKNOWN = -1,
	FD_DEVICE_CTRL_EN_CARDIDX_SDA = 0,
	FD_DEVICE_CTRL_EN_CARDIDX_SDB,
	FD_DEVICE_CTRL_EN_CARDIDX_SDC,
	FD_DEVICE_CTRL_EN_CARDIDX_SDD,
	FD_DEVICE_CTRL_EN_CARDIDX_SDE,
	FD_DEVICE_CTRL_EN_CARDIDX_SDF,
	FD_DEVICE_CTRL_EN_CARDIDX_SDG,
	FD_DEVICE_CTRL_EN_CARDIDX_SDH,
	FD_DEVICE_CTRL_EN_CARDIDX_MMCBLK1

};



/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define MAX_RESP_SIZE   512
#define NUM_CID_STREAM_BITS		127		
#define GP_CRC7					0x89	// CRC7 Generator-Polynom
/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/


   
/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/





#ifdef __cplusplus
}
#endif


#endif /* #ifndef _FD_DEVICE_CTRL_H */
