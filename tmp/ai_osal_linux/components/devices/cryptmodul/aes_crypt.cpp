/*****************************************************************************
| FILE:         aes_crypt.cpp
| PROJECT:      
| SW-COMPONENT: Crypt modul
|-----------------------------------------------------------------------------
| DESCRIPTION:  
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 12.10.10  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#include "OsalConf.h"

#ifdef AES_ENCRYPTION_AVAILABLE

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#define ENCRYPTION_S_IMPORT_READER
#include "encryption_if.h"


extern "C" {

tU32 s32ReadAesFile(tCString szFileName, tPU8 pu8Data,tU32 u32Size)
{
   AESReader oReader;
   tU32 u32CryptSize = 0;
   tPU8 pu8Ptr = NULL;

   if(oReader.bDecryptFile(szFileName,&u32CryptSize,&pu8Ptr))
   {
       memcpy(pu8Data,pu8Ptr,u32Size);
       delete[] pu8Ptr; // heap memory allocated by oReader.bDecryptFile
   }
   return u32CryptSize;
}

};
#endif //AES_ENCRYPTION_AVAILABLE
/******************************************************************************
|end of file aes_crypt.cpp
|-----------------------------------------------------------------------*/
