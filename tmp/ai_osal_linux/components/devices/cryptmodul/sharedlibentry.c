/* OSAL Interface */
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "fd_crypt.h"
#include "fd_device_ctrl.h"


OSAL_DECL tU32 crypt_ctrl_u32SDCardInfo( void* prIOCtrlCardState, tS32 s32DeviceIndx )
{
   return fd_device_ctrl_u32SDCardInfo( (OSAL_trIOCtrlCardState*) prIOCtrlCardState, s32DeviceIndx );
}
OSAL_DECL tU32 crypt_ctrl_u32ReadCid( void* pu8Cid,tS32 s32DeviceIndx )
{
   return fd_device_ctrl_u32ReadCid( (tPU8) pu8Cid, s32DeviceIndx  );
}

OSAL_DECL tS32 sign_verify_signaturefile( tCString coszCertPath )
{
   return fd_crypt_verify_signaturefile(coszCertPath);
}

OSAL_DECL tS32 sign_get_signature_verify_status(tCString coszCertPath )
{
   return fd_crypt_get_signature_verify_status(coszCertPath);
}

OSAL_DECL tS32 sign_get_signaturefile_type( tCString coszCertPath )
{
   return fd_crypt_get_signaturefile_type(coszCertPath );
}

#ifdef AES_ENCRYPTION_AVAILABLE
extern tU32 s32ReadAesFile(tCString szFileName, tPU8 pu8Data,tU32 u32Size);

OSAL_DECL tU32 crypt_read_aes_file(tCString szFileName, void* vpData,tU32 u32Size)
{
   return s32ReadAesFile(szFileName,(tPU8)vpData,u32Size);
}
#endif

