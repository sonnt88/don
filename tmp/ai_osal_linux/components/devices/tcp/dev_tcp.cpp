#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#if OSAL_OS==OSAL_TENGINE
  #include "OsalmpSubSystem.h"
#endif
#include "Linux_osal.h"

#include <ctype.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
// #include "dev_tcp.h" dispatcher_device.h

#include "cClientTcpSocket.h"
#include "cTcpDevice.h"

// =======================================================================================================
// =======================================================================================================

#ifndef TABSIZE
#define TABSIZE(a) (sizeof(a)/sizeof(a[0]))
#endif

class cTcpDeviceMngr
{
  public:
   struct t_DrvInstanceTable
   {
     tS32         s32Id;
     cTcpDevice * pDevice;    // same as u32FD
   } aDrvInstanceTable[10];
   
   
   cTcpDevice *GetDevice(tS32 s32Id, tU32 u32FD)
   {
      for (int i=0; i < TABSIZE(aDrvInstanceTable); i++)
      {
         if (aDrvInstanceTable[i].s32Id == s32Id)
         {
            if(aDrvInstanceTable[i].pDevice)
               return aDrvInstanceTable[i].pDevice;
         } 
      }
      return NULL;
   }
   
   bool AddDevice(tS32 s32Id, tU32 *pu32FD, cTcpDevice *pDevice)
   {
      for (int i=0; i < TABSIZE(aDrvInstanceTable); i++)
      {
         if (aDrvInstanceTable[i].s32Id == 0)
         {
            aDrvInstanceTable[i].pDevice = pDevice;
            aDrvInstanceTable[i].s32Id = s32Id;
            *pu32FD = (tU32)(pDevice);   /*lint !e571 */  
            return true;
         } 
      }
   }
   
   void DelDevice(tS32 s32Id, tU32 u32FD)
   {
      for (int i=0; i < TABSIZE(aDrvInstanceTable); i++)
      {
         if (aDrvInstanceTable[i].s32Id == s32Id)
         {
            aDrvInstanceTable[i].pDevice =0;
            aDrvInstanceTable[i].s32Id=0;
         } 
      }
   }
};

static cTcpDeviceMngr m_DevMngr;

/************************************************************************
* DESCRIPTION                                                           *
*      Createa the TCP-Device                                          *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
************************************************************************/
extern "C" tS32 TCP_s32IODeviceInit(tVoid)
{
    return OSAL_E_NOERROR;
}

/************************************************************************
* DESCRIPTION                                                           *
*      Destroy the ram-copy or something elese                          *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
************************************************************************/
extern "C" tS32 TCP_s32IODeviceRemove(tVoid)
{
    return OSAL_E_NOERROR;
}

/************************************************************************
* DESCRIPTION                                                           *
*     return true                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
***************************************************   *********************/
extern "C" tS32 TCP_IOOpen(tS32 s32Id, tU32 *u32FD, tCString szName)
{

   cTcpDevice *pDevice = new cTcpDevice(szName,"");
   // ASSERT(pDevice);
   //aDrvInstanceTable[curnum++] = pDevice;
   m_DevMngr.AddDevice(s32Id,u32FD,pDevice);
   return OSAL_E_NOERROR;
}

/************************************************************************
* DESCRIPTION                                                           *
*     return true                                                       *
* OUTPUTS                                                               *
*      tS32: Error code                                                 *
************************************************************************/
extern "C" tS32 TCP_s32IOClose( tS32 s32Id, tU32 u32FD )
{
   cTcpDevice *pDevice = m_DevMngr.GetDevice(s32Id, u32FD);
   if(pDevice == NULL)
      return OSAL_ERROR;
   m_DevMngr.DelDevice(s32Id,u32FD);
   delete pDevice;
   return OSAL_E_NOERROR;
}

/************************************************************************
* DESCRIPTION                                                           *
*  Call a control function                                              *
*  Following controls are supported and Ps32arg has to be of the        *
*  described type:                                                      *
*                                                                       *
*  Control (Ps32fun)                          Type of Ps32arg           *
*  OSAL_C_S32_IOCTRL_VERSION                 tPS32                      *
*                                                                       *
* INPUTS                                                                *
*       tS32 Ps32fun:              Function identificator               *
*       tS32 Ps32arg:              Argument to be passed to function    *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error code                                                 *
************************************************************************/
extern "C" tS32 TCP_s32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{
   cTcpDevice *pDevice = m_DevMngr.GetDevice(s32ID, u32FD);
   if(pDevice == NULL)
      return OSAL_ERROR;
   return pDevice->u32IOCtrlFunction((tU32)s32fun,(tU32)s32arg);
}

/************************************************************************
* DESCRIPTION                                                           *
*    write a entry data to RAM buffer                                   *
* INPUTS                                                                *
*       tPCS32 Pps32Buffer:      Pointer to data buffer                 *
*       tU32   Pu32nbytes:       Number of bytes in buffer              *
* OUTPUTS                                                               *
*      tS32: Error code                                                 *
************************************************************************/
extern "C" tS32 TCP_s32IOWrite(tS32 s32ID, tU32 u32FD, tPCS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   int j;
   cTcpDevice *pDevice = m_DevMngr.GetDevice(s32ID, u32FD);

   if(pDevice == NULL)
      return OSAL_ERROR;
   
   printf("TCP_s32IOWrite(%d): ", u32Size);
   for(j=0; j < 8; j++)
   {
      printf("%02x ",pBuffer[j]);
   }
   printf("\n");

   return pDevice->u32WriteFunction(pBuffer,u32Size);
}

/************************************************************************
* DESCRIPTION                                                           *
*    read a entry data from RAM buffer                                  *
* INPUTS                                                                *
*       tPCS32 Pps8Buffer:      Pointer to data buffer                  *
*       tU32   Pu32nbytes:      Number of bytes in buffer               *
* OUTPUTS                                                               *
*      tS32: Error code                                                 *
************************************************************************/
extern "C" tS32 TCP_s32IORead(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   cTcpDevice *pDevice = m_DevMngr.GetDevice(s32ID, u32FD);
   if(pDevice == NULL)
      return OSAL_ERROR;
   return pDevice->u32ReadFunction(pBuffer, u32Size, ret_size);
}

/* End of File dev_tcp.cpp                                                     */



