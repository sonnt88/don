#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#if OSAL_OS==OSAL_TENGINE
  #include "OsalmpSubSystem.h"
#endif
#include "Linux_osal.h"

#include <ctype.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>

#include "cClientTcpSocket.h"
#include "cTcpDevice.h"


/*************************************************************************
* drivers constructor
*************************************************************************/
cTcpDevice::cTcpDevice( tCString sczDevName, const tChar* szMountPoint)
: _bIsEnabled( true ), _poSocket(NULL)
{
   extern void adr3_sock_dummyFn();
  // adr3_sock_dummyFn();

   // init socket params
   _Port = 0;
   enum cClientTcpSocket::enProt  prot = cClientTcpSocket::RAW;
   strcpy (_szHost, "localhost");

   _bIsConfiguredAsServer = false;
    // host=10.34.211.60 port=13213 type=client prot=ESC"

   printf("cTcpDevice::cTcpDevice: DevName: '%s'\n",sczDevName);
   tChar szArgs[200];
   const char *szHome=getenv("HOME");
   strcpy(szArgs,szHome);
   strcat(szArgs,"/osal_pure_device_args.ini");

   //
   // at first process arguments for host,port,protocol and sockettype
   // configured as default in drivers target table definition or in
   // a local configuration file, that overwrites the parameters

   FILE *fp = fopen(szArgs,"r");

   if (fp == NULL){
      fp = fopen("/opt/bosch/nfs/osal_pure_device_args.ini","r");
   }

   if (fp != NULL)
   {
       char buf[500]; char *s;
       while(fgets(buf,sizeof(buf),fp) != NULL)
       {
            while(buf[0] == ' ' || szArgs[0] == '\t')
                strcpy(buf,buf+1);
            if (buf[0] == '#')
                continue;
            const char *szToken = strstr(buf,sczDevName);
            if (szToken != NULL)
            {
                szToken = strstr(buf,":");
                if (szToken != NULL)
                {
                    strcpy(szArgs,szToken+1);
                    if ((s=strchr(szArgs,'\r')) != NULL)
                        *s='\0';
                    if ((s=strchr(szArgs,'\n')) != NULL)
                        *s='\0';
                    break;
                }
            }
       }
       fclose(fp);
   }


   if(szArgs[0] != '\0')
   {
       while(szArgs[0] == ' ' || szArgs[0] == '\t')
           strcpy(szArgs,szArgs+1);
      char* argument = strtok (szArgs, " ");
      while (argument != NULL)
      {
         if ( strncmp(argument,"host=",5) == 0 )
         {
            snprintf(_szHost,sizeof(_szHost), "%s", argument+5);
         }
         if ( strncmp(argument,"port=",5) == 0 )
         {
            char* stop;
            unsigned port= strtoul(argument+5,&stop,10);
            if (port == 0)
              ;
              // WOS_Printf("OSAL_TCPRAW: unknown port (%s)\n", argument);
            else
              _Port = port;
         }
         if ( strncmp(argument,"prot=",5) == 0 )
         {
            if (strncmp(argument+5,"ESC",3)==0)
               prot = cClientTcpSocket::ESC;
            else if (strncmp(argument+5,"DAB",3)==0)
               prot = cClientTcpSocket::DAB;
            else if (strncmp(argument+5,"RAW",3)==0)
               prot = cClientTcpSocket::RAW;
            else
               prot = cClientTcpSocket::RAW;
         }
         if ( strncmp(argument,"type=",5) == 0 )
         {  // type=server type=client
            _bIsConfiguredAsServer = strstr(argument+5,"server") != NULL;
            if (_bIsConfiguredAsServer)
            {
              ; //WOS_Printf("OSAL_TCPRAW: type=%s : currently only 'client' supported.\n", argument);
            }
         }
         argument = strtok (NULL, " ");
      }
   }

   if (strcmp(_szHost,"localhost") == 0)                     // to prevent proxy problems
      strcpy (_szHost,"127.0.0.1");

   if (_bIsEnabled && _poSocket == NULL)
   {
      _poSocket = new cClientTcpSocket();
      if (_poSocket != NULL)
      {
         if (_poSocket->bInit(_szHost, _Port, prot) != OSAL_OK)
         {
            delete _poSocket;
            _poSocket = NULL;
         }
      }
   }
}


static void vHandleDabWriteMessage( tPCS8 pBuffer, tU32 nbytes, cClientTcpSocket * poSocket)
{
   char *pch = strstr((char*)pBuffer,"SSI32.Open");

   if ( pch == NULL )
   {
      char msg[MAX_DAB_MSGS_SIZE];
      strcpy(msg, "SSI32.TX CH=0 LUN=18 MSG=");

      for (tU32 j = 0; j< nbytes; j++)
      {
         char _lastBuffer[12]; /* temp _lastBuffer for itoa conversion */
         sprintf((char *)_lastBuffer,"%x", pBuffer[j]);
         int n = strlen(_lastBuffer);
         if ( n == 1)
            strcat(msg, "0");
         else if (n > 2)
         {
            for (int i=0; i<=2; i++)
            {
               _lastBuffer [i] = _lastBuffer[n-2+i];
            }
         }

         strcat(msg, (const char*)_lastBuffer);
      }
      strcat(msg, "\r");
      poSocket->s32Send((const char*)msg, (int)strlen(msg),0);
   }
   else
   {
      poSocket->s32Send((const char*)pBuffer, (int)nbytes,0);
   }
}

static void vHandleEscWriteMessage( tPCS8 buf, tU32 l, cClientTcpSocket * poSocket)
{
   // Paket wegschreiben
   tU8 sbuf[1400];
   int cnt,x=l;

   cnt=0;
   memset(sbuf,0xE5,sizeof(sbuf));
   sbuf[cnt++] = FR_ESC;
   sbuf[cnt++] = TR_START;
   while (l > 0)
   {
      sbuf[cnt++] = *buf;
      if (*buf == FR_ESC)
         sbuf[cnt++] = TR_ESC;
      l--;
      buf++;
      if (cnt >= sizeof(sbuf)-3) {
         // Cache voll
         poSocket->s32Send((const char*)sbuf,(int)cnt,0);
         cnt=0;
      }
   }
   // und den Rest auch raus bitte
   sbuf[cnt++] = FR_ESC;
   sbuf[cnt++] = TR_END;
   poSocket->s32Send((const char*)sbuf,(int)cnt,0);
}


/*************************************************************************
* drivers Write function
*------------------------------------------------------------------------
* PARAMETER:  | (I) tU32 Param1: output _lastBuffer
*             | (I) tU32 Param2: output _lastBuffer size
* RETURN      | OSAL error code: OSAL_E_NOERROR in case of success
*************************************************************************/
tU32 cTcpDevice::u32WriteFunction( const tS8* pBuffer, int u32BuffLen)
{
   if (pBuffer == NULL || u32BuffLen == 0)
      return OSAL_E_INVALIDVALUE;

   if (_poSocket != NULL)
   {
      if ( _poSocket->_prot == cClientTcpSocket::DAB )
      {
          vHandleDabWriteMessage(pBuffer, u32BuffLen, _poSocket);
      }
      else if ( _poSocket->_prot == cClientTcpSocket::ESC )
      {
          vHandleEscWriteMessage(pBuffer, u32BuffLen, _poSocket);
      }
      else  // _poSocket->_prot == cClientTcpSocket::RAW
      {
         printf("cTcpDevice::u32WriteFunction(): ");

         for(int j=0; j < u32BuffLen; j++)
         {
            printf("%02x ",pBuffer[j]);
            if ((j %8)==0)
               printf("\n");
         }
         printf("snd ->\n");
         if (_poSocket->s32Send ((const char*)pBuffer, u32BuffLen, 0) == SOCKET_ERROR)
         {
            delete _poSocket;
            _poSocket = NULL;
            return OSAL_E_UNKNOWN;
         }
      }
      return OSAL_E_NOERROR;
   }
   return OSAL_E_DOESNOTEXIST;
}

/*************************************************************************
* drivers Read function, called from the application
*------------------------------------------------------------------------
* RETURN      | OSAL error code: OSAL_E_NOERROR in case of success
*************************************************************************/
tU32 cTcpDevice::u32ReadFunction( tPVoid pBuffer, tU32 maxbytes, tU32 *pReadBytes )
{
   int nbrReads = 0;

   if( _poSocket == NULL)
   {
       return OSAL_E_INVALIDVALUE;
   }

   _poSocket->EnterCriticalSection();

   if ( _poSocket->_prot == cClientTcpSocket::DAB )
   {
       nbrReads = _poSocket->readDAB((tS8*)pBuffer, maxbytes);
   }
   else if ( _poSocket->_prot == cClientTcpSocket::ESC )
   {
       nbrReads = _poSocket->readESC((tS8*)pBuffer, maxbytes);
   }
   else // ( _poSocket->_prot == cClientTcpSocket::RAW )
   {
       nbrReads = _poSocket->readRAW((char*)pBuffer, maxbytes);
   }

   _poSocket->LeaveCriticalSection();

   if (nbrReads < 0)
      nbrReads = 0;
   if(pReadBytes)
      *pReadBytes = nbrReads;

   printf("cTcpDevice::u32ReadFunction(): number bytes read: %d\n",nbrReads);
   int i=0;
   int iMaxByte = 100;
   tU8* pBuf = (tU8*)pBuffer;
   if (nbrReads < 100) {
      iMaxByte = nbrReads;
   }
   for (i=0;i<iMaxByte;i++) printf("0x%02x, ",pBuf[i]);
   printf("\n");

   return OSAL_E_NOERROR;
}

/*************************************************************************
* drivers IOCtrl Function
*------------------------------------------------------------------------
* PARAMETER:  | (I) tU32 dwCode: Code
*             | (I) tU32 Param: Parameter
* RETURN      | OSAL error code: OSAL_E_NOERROR in case of success
*************************************************************************/
tU32 cTcpDevice::u32IOCtrlFunction(tU32 dwCode, tU32 Param)
{
   switch (dwCode)
   {
   case OSAL_C_S32_IOCTRL_VERSION:
      *((int *) Param) = TCP_C_S32_IO_VERSION;
      return OSAL_E_NOERROR;

   case OSAL_C_S32_IOCTRL_CALLBACK_REG:
      if (_poSocket != NULL)
      {
         _poSocket->_pFktCallback = (pfIpnRxCallback) Param;
         if (_poSocket->_prot == cClientTcpSocket::DAB)
         {  // open ADR3
            char msg[100];
            strcpy(msg, "SSI32.Open CH=0 LUN=18 RXLEN=256");
            strcat(msg, "\r\0");
            return u32WriteFunction((const tS8*)msg, strlen(msg));
         }
         return OSAL_E_NOERROR;
      }
      break;

   case OSAL_C_S32_IOCTRL_CALLBACK_UNREG:
      if (_poSocket != NULL)
      {
         _poSocket->_pFktCallback = NULL;
         return OSAL_E_NOERROR;
      }
      break;

   case OSAL_C_S32_IOCTRL_ISACTIVE:
      {
         if (_poSocket != NULL)
         {
             // dataptr = (void *)Param; ...
             return OSAL_E_NOERROR;
         }
      }
      break;
   default:
      break;
   }
   return OSAL_E_INVALIDVALUE;
}


