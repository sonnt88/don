// =======================================================================================================
// =======================================================================================================
// =======================================================================================================


class cTcpDevice 
{
  public:
   cTcpDevice();
   virtual ~cTcpDevice()
   {
      if( _poSocket != NULL )
      {
         delete _poSocket;
         _poSocket = NULL;
      }
   }
   cTcpDevice(  tCString  sczDevName, const tChar* szMountPath);

   tU32 u32ReadFunction  ( tPVoid pBuffer, tU32 maxbytes, tU32 *pReadBytes );
   tU32 u32WriteFunction ( const tS8* pBuffer, int u32BuffLen );
   tU32 u32IOCtrlFunction( tU32 Param1, tU32 Param2 );
  private:
   bool                    _bIsEnabled;
   bool                    _bIsConfiguredAsServer;
   long                    _Port;
   char                    _szHost[256];
   class cClientTcpSocket *_poSocket;
};


