#ifndef _DEV_TCP_H
#define _DEV_TCP_H

#ifdef __cplusplus
 extern "C" {

tS32 TCP_s32IODeviceInit(tVoid);
tS32 TCP_s32IODeviceRemove(tVoid);

tS32 TCP_IOOpen(tS32 s32Id, tU32 *pu32FD, tCString szName);
tS32 TCP_s32IOClose(tS32 s32ID, tU32 u32FD);
tS32 TCP_s32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg);
tS32 TCP_s32IOWrite(tS32 s32ID, tU32 u32FD, tPCS8 pBuffer, tU32 u32Size, tU32 *ret_size);
tS32 TCP_s32IORead(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size);

}
#endif

#else
#error dev_tcp.h included several times
#endif
