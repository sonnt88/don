
typedef int SOCKET;

#define INVALID_SOCKET        -1
#define SOCKET_ERROR          -1
#define closesocket           close
#define TCP_C_S32_IO_VERSION  1

extern "C" { typedef void (*pfIpnRxCallback) (void); };


// =======================================================================================================
// =======================================================================================================
// =======================================================================================================
// =======================================================================================================
// =======================================================================================================
// =======================================================================================================

#define MAX_DAB_MSGS_SIZE 1024

// Frame Escape Codes
#define FR_ESC    0xF0      // n�chstes Zeichen ist ein Kommando
#define TR_START  0xF1      // Paket Start
#define TR_END    0xF2      // Paket Stop
#define TR_ESC    0xF3      // Zeichen ist Escape-Zeichen selbst


class cClientTcpSocket 
{
   public:
      enum enProt { RAW, DAB, ESC };

   public:
      cClientTcpSocket();
      virtual ~cClientTcpSocket();
      tS32 bInit(const char *hostname, int portnum, enProt eProtocol);
      void vExit ();
      int  OnDataReceived(unsigned char *buf, int size);
      int  s32Send (const char *buf, int len, int flags);
      int  ThreadReceiveFunc();
      bool ThreadWorkProc();
      tS32 readRAW(char *readBuffer, int sizeOfReadBuffer);
      tS32 readDAB( tPS8 pBuffer, int sizeOfReadBuffer);
      tS32 readESC( tPS8 readBuffer, int sizeOfReadBuffer);
      void EnterCriticalSection() { pthread_mutex_lock( &_critical_section ); }
      void LeaveCriticalSection() { pthread_mutex_unlock( &_critical_section); }
      
   private:  // ThreadProc
      static void* m_threadproc(void* classpointer)
      {   // callback Thread-Funktion
        ((cClientTcpSocket *)(classpointer))->ThreadWorkProc();
        return NULL;
      }
      bool CreateReceiveThread();

   private:
      pthread_mutex_t   _critical_section;
      pthread_t         _dwThreadID;  
      SOCKET            _sock;
      char             *_lastBuffer;
      int               _lastBufferLen;
      struct pack_info
      {
           unsigned        rlen,rcnt,resc;
           unsigned char   rbuf[1024];             // Emfangsbuffer
           void init()
           {
               rlen=sizeof(rbuf);
               resc=0;
               rcnt=0;
               memset(rbuf,0,sizeof(rbuf));
           }
      } _packetbuf;

  public:
      pfIpnRxCallback   _pFktCallback; // callback funtionpointer
      enum enProt       _prot;
};



