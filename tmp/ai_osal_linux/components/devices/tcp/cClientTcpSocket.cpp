#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#if OSAL_OS==OSAL_TENGINE
  #include "OsalmpSubSystem.h"
#endif
#include "Linux_osal.h"

#include <ctype.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>

#include "cClientTcpSocket.h" 

/************************************************************************
* FUNCTION:     bInit
*----------------------------------------------------------------------
* PARAMETER:    (I) szServerHost: host (e.g.: 10.85.0.213 )
* PARAMETER:    (I) portnum
* DESCRIPTION:  creates tcp socket and receive thread
* RETURNVALUE:  OSAL_OK or OSAL_ERROR
*************************************************************************/
tS32 cClientTcpSocket::bInit(const char * szServerHost, int portnum, enProt eProtocol)
{
   // create socket
   _sock = socket(AF_INET, SOCK_STREAM, 0);
   if (_sock == INVALID_SOCKET)
      return OSAL_ERROR;

   struct hostent *ho1 = NULL;
   unsigned long hostip;

   _prot = eProtocol;

   if (strcmp(szServerHost,"localhost") == 0)                     // proxy problems
       szServerHost = "127.0.0.1";

   printf("cClientTcpSocket::bInit(): Server: %s, port: %d\n", szServerHost, portnum);

   if ((hostip=inet_addr(szServerHost)) == INADDR_NONE)           // "127.0.0.1", ...
   {
        if ((ho1=gethostbyname(szServerHost)) == NULL)            // "localhost", ...
                    return OSAL_ERROR;							                  // "Can't lookup"!
        hostip = *((unsigned long*)ho1->h_addr_list[0]);
                memcpy((void *)&hostip,(void *)ho1->h_addr, sizeof(struct in_addr));
   }

   struct {				                  // connection information
     union {
         struct sockaddr_in  in;
         struct sockaddr     so;
     } hostadr;
   } ho;
   memset(&ho,0,sizeof(ho));

   ho.hostadr.in.sin_addr.s_addr = hostip;
   ho.hostadr.in.sin_family      = (short)AF_INET;
   ho.hostadr.in.sin_port        = htons((unsigned short)portnum);;

   // socket verbinden
   if (connect(_sock, &ho.hostadr.so, sizeof(ho.hostadr.in)) == SOCKET_ERROR)
   {
      closesocket(_sock);
      _sock = INVALID_SOCKET;
      return OSAL_ERROR;
   }

   if(CreateReceiveThread() == false)
   {
      closesocket(_sock);
      _sock = INVALID_SOCKET;
      return OSAL_ERROR;
   }
   return OSAL_OK;
}

/************************************************************************
* FUNCTION:     cClientTcpSocket
*----------------------------------------------------------------------
* DESCRIPTION:  ctor
*************************************************************************/
cClientTcpSocket::cClientTcpSocket()
{
    _lastBuffer = NULL;
    _lastBufferLen = 0;
//    _critical_section = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutexattr_t attr;
    pthread_mutexattr_init( &attr );
    pthread_mutexattr_settype( &attr, PTHREAD_MUTEX_NORMAL );
    pthread_mutex_init( &_critical_section, &attr );
    pthread_mutexattr_destroy( &attr );
    _sock = INVALID_SOCKET;
    _packetbuf.init();
   _pFktCallback = NULL;
}


cClientTcpSocket::~cClientTcpSocket()
{
   if (_dwThreadID != 0)
      pthread_join(_dwThreadID,0); // pthread_detach(_dwThreadID);, pthread_exit(NULL); 
   _dwThreadID=0;
   vExit();
}

bool cClientTcpSocket::ThreadWorkProc()
{
   while (ThreadReceiveFunc()>0)
      ;
   return true;
}

bool cClientTcpSocket::CreateReceiveThread()
{
   pthread_attr_t  attr;
   pthread_attr_init(&attr);
   pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
   //   pthread_attr_setstacksize(&attr, nStackSize);
   int rc = pthread_create(&_dwThreadID,&attr,m_threadproc,this);
   //if ( rc == EPERM )
   //    rc = pthread_create(&_dwThreadID,NULL,m_threadproc,this);
   pthread_attr_destroy(&attr);
   return rc == 0;
}
   


/************************************************************************
* FUNCTION:     write data to server
* RETURNVALUE:  size of written data
*************************************************************************/
int cClientTcpSocket::s32Send (const char *buf, int len, int flags)
{
   return send (_sock, buf, len, flags);
}


/************************************************************************
* FUNCTION:     vExit
*----------------------------------------------------------------------
* DESCRIPTION:  closes the socket and the connection
* RETURNVALUE:  OSAL_OK
*************************************************************************/
void cClientTcpSocket::vExit()
{
   _pFktCallback = NULL;
   if (_sock != INVALID_SOCKET)
   {
      closesocket(_sock);
      _sock = INVALID_SOCKET;
   }
}

#define GET_U16(pu8) ((tU16)(((tU16)((pu8)[0])<<8) + (pu8)[1]))

/****************************************************************************
* FUNCTION:     function to readRAW DAB-messages and remove header information *
*               like "SSI32.rx T=25 CH=0 LUN=0x12 LEN=11 MSG="              *
*               Additional the ascii-text(which includes the meca-msg)      *
*               will be converted into hex-values.                          *
****************************************************************************/
tS32 cClientTcpSocket::readDAB( tPS8 readBuffer, int sizeOfReadBuffer)
{
    char msg[MAX_DAB_MSGS_SIZE];
    char * pCheckBuffer = NULL;
    char * pch5 = NULL;
    char * pch6 = NULL;
    tU8 u8Count = 0;
    tU8 length = 0;
    int countOfShift = sizeOfReadBuffer;

    if ((_lastBufferLen==0)||( _lastBuffer == NULL ))
    {   // nothing to deliver
        return 0;
    }

    if (sizeOfReadBuffer > _lastBufferLen)
    {
        sizeOfReadBuffer = _lastBufferLen;
    }

    memcpy(readBuffer, _lastBuffer, sizeOfReadBuffer);
    readBuffer[sizeOfReadBuffer]= 0;

    pCheckBuffer = strstr((char*)(readBuffer),"SSI32.rx"); // check if more than one msg is in _lastBuffer

    if (pCheckBuffer != NULL) {
        do{
            pCheckBuffer = strstr(pCheckBuffer+8,"SSI32.rx");
            u8Count++;
        }while (pCheckBuffer != NULL);
    }

    if (u8Count)
    {
        pch5 = strstr((char*)(readBuffer+4),"MSG="); //handle stream : readRAW only the first msg and trigger readRAW again

        if (pch5 != NULL) {
            countOfShift = pch5 - (char*)readBuffer;

            pch6 = strstr((char*)(pch5+7),"SSI32.rx");  // check bis 1. Nachricht zu Ende

            if ( pch6 != NULL){             // additional rx-msg are in _lastBuffer
                length = pch6 - pch5-6;     // calculate length of first rx Nachricht
            }
            else{                           // only one rx-msg in _lastBuffer
                length = strlen(pch5+4);
            }

            countOfShift += length+4;       //calculate count to shift
            //sizeOfReadBuffer = length;
        }

        sprintf(msg, "%s", (char*)pch5+4);        // copy rx-msg in temp msg-_lastBuffer
        msg[length] = '\0';                 // Null-termination

        length = strlen(msg);               // get size of ascii - msg
        sizeOfReadBuffer += length;

        tU8 j,k = 0;
        char s[2];

        for(j = 0; j< length; j++){         // calculate ascii-char into hex-values
            s[0] = msg[j];
            j++;
            s[1] = msg[j];
            unsigned int *pp = (unsigned int *)((char*)(readBuffer+k));
            sscanf(s, "%02X", pp);
            k++;
        }

        if ( j > 0 ) {
            readBuffer[j] = '\0';
            length = --k;
        }
    }

    _lastBufferLen -= countOfShift;
    if (_lastBufferLen <= 0)
    {
      delete _lastBuffer;
      _lastBuffer = NULL;
      _lastBufferLen = 0;
    }
    else
    {
      char * tempBuffer = new char[_lastBufferLen];
      if ( tempBuffer != NULL)
      {
          memcpy(tempBuffer, _lastBuffer+countOfShift, _lastBufferLen);
          delete _lastBuffer;
          _lastBuffer = tempBuffer;
        }
    }
    return (0x0a +GET_U16(&readBuffer[8])); //(MECA-HEADER)+(MECA-MSG-LENGTH)
}

tS32 cClientTcpSocket::readESC( tPS8 readBuffer, int sizeOfReadBuffer)
{
   if ((_lastBufferLen==0)||( _lastBuffer == NULL ))
   {   // nothing to deliver
       return 0;
   }

   if (sizeOfReadBuffer > _lastBufferLen)
   {
      sizeOfReadBuffer = _lastBufferLen;
   }

   memcpy(readBuffer, _lastBuffer, sizeOfReadBuffer);
   _lastBufferLen -= sizeOfReadBuffer;
   if (_lastBufferLen == 0 && _lastBuffer)
   {
      delete [] _lastBuffer;
      _lastBuffer = NULL;
      _lastBufferLen = 0;
   }
   else
   {
      char * tempBuffer = new char[_lastBufferLen];
      if ( tempBuffer != NULL)
      {
          memcpy(tempBuffer, _lastBuffer+sizeOfReadBuffer, _lastBufferLen);
          delete [] _lastBuffer;
          _lastBuffer = tempBuffer;
      }
   }
   return sizeOfReadBuffer;
}

/************************************************************************
* FUNCTION:     readRAW data send from server
* RETURNVALUE:  size of readRAW data return in readBuffer
*************************************************************************/
tS32 cClientTcpSocket::readRAW(char *readBuffer, int sizeOfReadBuffer)
{
   if ((_lastBufferLen==0)||( _lastBuffer == NULL ))
   {   // nothing to deliver
       return 0;
   }

   if (sizeOfReadBuffer > _lastBufferLen)
   {
      sizeOfReadBuffer = _lastBufferLen;
   }

   memcpy(readBuffer, _lastBuffer, sizeOfReadBuffer);
   _lastBufferLen -= sizeOfReadBuffer;
   if (_lastBufferLen == 0 && _lastBuffer)
   {
      delete [] _lastBuffer;
      _lastBuffer = NULL;
      _lastBufferLen = 0;
   }
   else
   {
      char * tempBuffer = new char[_lastBufferLen];
      if ( tempBuffer != NULL)
      {
          memcpy(tempBuffer, _lastBuffer+sizeOfReadBuffer, _lastBufferLen);
          delete [] _lastBuffer;
          _lastBuffer = tempBuffer;
          // _lastBufferLen -= sizeOfReadBuffer; is already set
      }
   }
   return sizeOfReadBuffer;
}

int cClientTcpSocket::OnDataReceived(unsigned char *buf, int nbrs)
{
   EnterCriticalSection();

   char *tempBuffer = new char[_lastBufferLen+nbrs];
   if ( NULL == tempBuffer )
   {
      LeaveCriticalSection();
      return OSAL_ERROR;
   }
   if (_lastBufferLen > 0)
   {
         memcpy(tempBuffer, _lastBuffer, _lastBufferLen);
         delete [] _lastBuffer;
   }
   memcpy(tempBuffer+_lastBufferLen, buf, nbrs);
   _lastBuffer = tempBuffer;
   _lastBufferLen += nbrs;

   LeaveCriticalSection();

   return nbrs;
}

int cClientTcpSocket::ThreadReceiveFunc()
{
   char buf[1500];

   int nbrs = recv(_sock, buf, sizeof(buf), 0);
   if (nbrs <= 0)
   {
      return nbrs;
   }

   // -----------------------------------------------------
   if ( _prot == cClientTcpSocket::DAB )
   {
      const char * pch1 = strstr((char*)buf,"SSI32.tx");
      const char * pch2 = strstr((char*)buf,"SSI32.TX");
      if (( pch1 != NULL )||( pch2 != NULL ))
      {
          return nbrs;
      }
      pch1 = strstr((char*)buf,"SSI32.open");
      if ( pch1 != NULL ){ // send startup
          unsigned char msg[10] = {0xFF, 0xFF, 0x00, 0x43, 0x01, 0x0f, 0x01, 0x02, 0x00, 0x00};
          memcpy(buf, msg,10);
          buf[10] = 0;
          nbrs = 10;
      }
      nbrs = OnDataReceived((unsigned char *)buf,nbrs);
   }
   // -----------------------------------------------------
   else if ( _prot == cClientTcpSocket::ESC )
   {
      pack_info *inf = &_packetbuf;
      int l,i;
      unsigned char *cmdbuf = (unsigned char*)buf;
        for (i=0;i<l;i++)
        {
            if (inf->resc)
            {   // escapesequence beenden
                inf->resc=0;
                if (cmdbuf[i]==TR_START)
                {
                    inf->rcnt=0;
                    continue;
                }
                if (cmdbuf[i]==TR_END)
                {
                     nbrs = OnDataReceived(inf->rbuf,inf->rcnt);
                    inf->rcnt=0;
                    continue;
                }
                if (cmdbuf[i]==TR_ESC)
                {
                    if (inf->rcnt < inf->rlen)
                        inf->rbuf[inf->rcnt++]=FR_ESC;
                    continue;
                }
                continue;

            } else if (cmdbuf[i]==FR_ESC) {
                inf->resc = 1;
                continue;
            }
            // normale Daten
            if (inf->rcnt < inf->rlen)
                inf->rbuf[inf->rcnt++]=cmdbuf[i];
        } // for
   }
   else // if ( _prot == cClientTcpSocket::RAW )
   {
      nbrs = OnDataReceived((unsigned char *)buf,nbrs);
   }

   if(nbrs > 0 && this->_pFktCallback)
        {
      this->_pFktCallback();
        }
   return nbrs;
}





