/****************************************************************************
* FILE                      [ $Workfile:   eollib_if.h  $ ]
* PROJECT (1st APPLICATION) [ GMGE ]
* ---------------------------------------------------------------------------
* EXPLANATION
* Header file to import Datapool interfaces of EOL Calibration modules
*
* ---------------------------------------------------------------------------
* Last Review:
* ---------------------------------------------------------------------------
* COPYRIGHT (c) 2014 Robert Bosch GmbH
* AUTHOR    [ Yuvaraj Krishnaraj (yuv2cob) ]
* ---------------------------------------------------------------------------
* 
****************************************************************************/
#ifndef EOLLIB_DP_HEADER
#define EOLLIB_DP_HEADER

#ifdef DIAG_EOL_LIB_S_IMPORT_INTERFACE_DP

#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"

#define EOL_MAX_DATA_LENGTH							28
#define SCC_EOL_MAX_PARAMETER_NBR					24
#define EOL_MAX_CAL_BLOCK_TRNSF_TO_SCC				2

#define EOL_VALID_BYTES_ACTUALLY_TRNSF_TO_SCC		25
#define EOL_TO_SCC_UNUSED							(EOL_MAX_DATA_LENGTH - EOL_VALID_BYTES_ACTUALLY_TRNSF_TO_SCC)

//CAN EOL DP Offset
#define	CANEOL_DP_OFFSET_BUS_WAKEUP_DELAY_TIME													0
#define	CANEOL_DP_OFFSET_HS_BUS_OFF_DTC_FAILURE_CRITERIA_X										1
#define	CANEOL_DP_OFFSET_HS_BUS_OFF_DTC_FAILURE_CRITERIA_Y										2
#define	CANEOL_DP_OFFSET_HSGMLAN_TX_MESSAGE_MASK_BYTE_1											3
#define	CANEOL_DP_OFFSET_HSGMLAN_TX_MESSAGE_MASK_BYTE_2											4
#define	CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_1											5
#define	CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_2											6
#define	CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_3											7
#define	CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_4											8
#define	CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_5											9
#define	CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_6											10
#define	CANEOL_DP_OFFSET_LSGMLAN_RX_BYTE_DTC_TRIGGER											11
#define	CANEOL_DP_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_MINIMUM_UPDATE_TIME				12
#define	CANEOL_DP_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_PERIODIC_RATE					13
#define	CANEOL_DP_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_DELAY_FOR_FIRST_PERIODIC_RATE	14
#define	CANEOL_DP_OFFSET_TX_DTC_TRIGGERED_788_MINIMUM_UPDATE_TIME								15
#define	CANEOL_DP_OFFSET_TX_DTC_TRIGGERED_788_PERIODIC_RATE										16
#define	CANEOL_DP_OFFSET_TX_DTC_TRIGGERED_788_DELAY_FOR_FIRST_PERIODIC_RATE						17
#define	EOL_DP_OFFSET_SYSTEM_CONFIGURATION														18
#define	EOL_DP_OFFSET_MAIN_DISP_FAILSOFT														19
#define	CANEOL_DP_OFFSET_DTC_MASK_BYTE1															21
#define	CANEOL_DP_OFFSET_DTC_MASK_BYTE2															22
#define	CANEOL_DP_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE1											23
#define	CANEOL_DP_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE2											24

/* EOL table and offset definitions */
#include "EOLLib.h"

	typedef struct
	{
		tU8     u8TableId;
		tU8		Header[14]; 
		tU16    u16Length;
	} tsDiagEOLBlockHeader;

#endif

#endif //EOLLIB_DP_HEADER

