/****************************************************************************
* FILE                      [ $Workfile:   EOLLib.h  $ ]
* PROJECT (1st APPLICATION) [ GMGE ]
* ---------------------------------------------------------------------------
* EXPLANATION
* Definition of Tables and Offsets for the EOL Calibration Storage Device
*
* ---------------------------------------------------------------------------
* Last Review:
* ---------------------------------------------------------------------------
* COPYRIGHT (c) 2007 Blaupunkt GmbH, Hildesheim
* AUTHOR    [ TMS Schmidt ]
* ---------------------------------------------------------------------------
* HISTORY AT THE END OF THE FILE
****************************************************************************/
#ifndef EOLLIB_HEADER
   #define EOLLIB_HEADER

#define EOL_BLOCK_NBR                       10  
#define U8_EOLLIB_MAX_BLOCK_LENGTH          0x2000
#define EOLLIB_INIT_MAGIC                   0xac
#define EOLLIB_T_ENGINE_DRIVER_INITIATED    0x01

#define OSAL_C_S32_IOCTRL_DIAGEOL_SYNC_SCC_DATAPOOL			  0x103
/**************************************************************************
** #include
**************************************************************************/

/**************************************************************************
** typedef
**************************************************************************/

typedef struct __attribute__ ((packed))
{
  unsigned short CHECKSUM;
  unsigned short MODULE_ID;
  unsigned short HFI;
  unsigned long PART_NUMBER;
  char DLS[2];
  unsigned short Cal_Form_ID;
} EOLLib_CalDsHeader;

typedef struct 
{
   tU32 offset;
   const tVoid* address;
   tU32 size;
} EOLLib_Entry;

extern tS32 EOLLib_Read(OSAL_trDiagEOLEntry* pEntry);
extern tS32 EOLLib_Write(OSAL_trDiagEOLEntry* pEntry);

/**************************************************************************
** Macro #define
**************************************************************************/

/**************************************************************************
** #define TABLE
**************************************************************************/

#define EOLLIB_TABLE_ID_SYSTEM               2
#define EOLLIB_TABLE_ID_DISPLAY_INTERFACE    3
#define EOLLIB_TABLE_ID_BLUETOOTH            4
#define EOLLIB_TABLE_ID_NAVIGATION_SYSTEM    5
#define EOLLIB_TABLE_ID_NAVIGATION_ICON      6
#define EOLLIB_TABLE_ID_BRAND                7
#define EOLLIB_TABLE_ID_COUNTRY              8
#define EOLLIB_TABLE_ID_SPEECH_RECOGNITION   9
#define EOLLIB_TABLE_ID_HAND_FREE_TUNING     10
#define EOLLIB_TABLE_ID_REAR_VISION_CAMERA   11

#define EOLLIB_TABLE_NAME_SYSTEM             "System.EOL"
#define EOLLIB_TABLE_NAME_DISPLAY_INTERFACE  "Display.EOL"
#define EOLLIB_TABLE_NAME_BLUETOOTH          "Bluetooth.EOL"
#define EOLLIB_TABLE_NAME_NAVIGATION_SYSTEM  "NavSystem.EOL"
#define EOLLIB_TABLE_NAME_NAVIGATION_ICON    "NavIcon.EOL"
#define EOLLIB_TABLE_NAME_BRAND              "Brand.EOL"
#define EOLLIB_TABLE_NAME_COUNTRY            "Country.EOL"
#define EOLLIB_TABLE_NAME_SPEECH_RECOGNITION "Speech.EOL"
#define EOLLIB_TABLE_NAME_HAND_FREE_TUNING   "HFTuning.EOL"
#define EOLLIB_TABLE_NAME_REAR_VISION_CAMERA "Rvc.EOL"

#define EOLLIB_TABLE_ADDRESS_SYSTEM             0x1000
#define EOLLIB_TABLE_ADDRESS_DISPLAY_INTERFACE  0x2000
#define EOLLIB_TABLE_ADDRESS_BLUETOOTH          0x3000
#define EOLLIB_TABLE_ADDRESS_NAVIGATION_SYSTEM  0x4000
#define EOLLIB_TABLE_ADDRESS_NAVIGATION_ICON    0x5000
#define EOLLIB_TABLE_ADDRESS_BRAND              0x6000
#define EOLLIB_TABLE_ADDRESS_COUNTRY            0x7000
#define EOLLIB_TABLE_ADDRESS_SPEECH_RECOGNITION 0x8000
#define EOLLIB_TABLE_ADDRESS_HAND_FREE_TUNING   0xA000
#define EOLLIB_TABLE_ADDRESS_REAR_VISION_CAMERA 0xB000

/**************************************************************************
** #define OFFSET
** BASIS:  GIS-344-Delivery V4.1.1
**************************************************************************/

//SYSTEM
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_HMI_SYSTEMS_CAL                             0   // BYTE (Boolean)
#define EOLLIB_OFFSET_ACT_REQ_HOLD_OFF_TIME                                                  1   // BYTE (8-Bit)
#define EOLLIB_OFFSET_APINFO1_CONSECUTIVE_READS                                              2   // BYTE (8-Bit)
#define EOLLIB_OFFSET_AUDIBLE_FEEDBACK_HOLD                                                  3   // BYTE (8-Bit)
#define EOLLIB_OFFSET_BUS_WAKEUP_DELAY_TIME                                                  4   // BYTE (8-Bit)
#define EOLLIB_OFFSET_CONSENT_PROCEED_TIMER                                                  5   // BYTE (8-Bit)
#define EOLLIB_OFFSET_CPID_MASK_BYTE1                                                        6   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_CPID_MASK_BYTE2                                                        7   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_CSHMI_TOUCH_EVENT_BUFFER_TIME                                          8   // BYTE (8-Bit)
#define EOLLIB_OFFSET_CSHMI_AUX_TOUCH_EVENT_BUFFER_TIME                                      9   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEREGISTER_DURATION                                                    10   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DISPLAY_POWER_ON_TIME                                                  11   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DISPLAY_VARIANT_ID                                                     12   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DTC_CRANK_DELAY_TIME                                                   13   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DTC_IGN_CYCLE_CTR_MAX                                                  14   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DTC_MASK_BYTE1                                                         15   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DTC_MASK_BYTE2                                                         16   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DTC_MASK_BYTE3                                                         17   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DTC_MASK_BYTE4                                                         18   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DTC_MASK_BYTE6                                                         19   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_3G2                                       20   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_3GP                                       21   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_AA                                        22   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_AAC                                       23   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_AIFF                                      24   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_APPLELOSSLESS                             25   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_MP3                                       26   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_MP4                                       27   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_M4A                                       28   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_OGG                                       29   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_WAV                                       30   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_ENCODING_FORMAT_WMA                                       31   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_BOSCH_SPECIFIC_HMI_UUDT_USDT                                    32   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_FAVORITES_BACKUP_MANAGER                                        33   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_DIVX_DIVX                                 34   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_FLASHVIDEO_FLV                            35   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_H263_3GP                                  36   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_H264_MP4                                  37   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_H264_M4V                                  38   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_MPEG1_MPG                                 39   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_MPEG1_MPEG                                40   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_MPEG1_MP4                                 41   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_MPEG2_MP4                                 42   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_MPEG4_AVC_M4V                             43   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_MPEG4_AVC_MP4                             44   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_MPEG4_MP4                                 45   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_NERO_MP4                                  46   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_WMV_WMV                                   47   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VIDEO_ENCODING_FORMAT_WMV_AVI                                   48   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENHANCED_FAN_HANDLING_ON_OFF                                           49   // BYTE (8-Bit)
#define EOLLIB_OFFSET_FBLOCK_MESSAGE_MASK_BYTE_1                                             50   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_FBLOCK_MESSAGE_MASK_BYTE_2                                             51   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_FBLOCK_MESSAGE_MASK_BYTE_3                                             52   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_FBLOCK_MESSAGE_MASK_BYTE_4                                             53   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_FBLOCK_MESSAGE_MASK_BYTE_5                                             54   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_FBLOCK_MESSAGE_MASK_BYTE_6                                             55   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_FBLOCK_MESSAGE_MASK_BYTE_7                                             56   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_GMBC                                                                   58   // BYTE (8-Bit) as ARRAY
#define EOLLIB_OFFSET_GMBVI_VERIFICATION_INITIAL                                             62   // BYTE (8-Bit) as ARRAY
#define EOLLIB_OFFSET_GMBVI_YEAR                                                             64   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GMBVI_MONTH                                                            65   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GMBVI_DAY                                                              66   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GMTV_MODEL_YEAR                                                        67   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GMTV_VEHICLE_MAKE                                                      68   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GMTV_VEHILCE_CARLINE                                                   69   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GMTV_ENGINE_RPO                                                        70   // BYTE (8-Bit) as ARRAY
#define EOLLIB_OFFSET_HMI_MODULE_DISPLAY_CONFIG                                              72   // BYTE (8-Bit)
#define EOLLIB_OFFSET_HS_BUS_OFF_DTC_FAILURE_CRITERIA_X                                      73   // BYTE (8-Bit)
#define EOLLIB_OFFSET_HS_BUS_OFF_DTC_FAILURE_CRITERIA_Y                                      74   // BYTE (8-Bit)
#define EOLLIB_OFFSET_HSGMLAN_TX_MESSAGE_MASK_BYTE_1                                         75   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_HSGMLAN_TX_MESSAGE_MASK_BYTE_2                                         76   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_1                                         77   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_2                                         78   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_3                                         79   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_4                                         80   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_5                                         81   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_6                                         82   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_IFAVORTEPROVIDER_MASK_BYTE1                                            83   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_INTERNET_FRAMEWORK_MONITORING_APPS                                     84   // BYTE (8-Bit)
#define EOLLIB_OFFSET_INTERNET_SR_SESSION_TIMEOUT                                            85   // BYTE (8-Bit)
#define EOLLIB_OFFSET_IR_PAUSE_CONNECTION_TIMEOUT                                            86   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ISERVICEFAVORITESPROVIDER_REGISTRATION_MASK_BYTE1                      87   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_LSGMLAN_RX_BYTE_DTC_TRIGGER                                            88   // BYTE (Boolean)
#define EOLLIB_OFFSET_LVDS_LEGNTH_MODE                                                       89   // BYTE (8-Bit)
#define EOLLIB_OFFSET_LVDS_DEEMPHASIS                                                        90   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MANIFEST_NOTICE                                                        91   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_CACHE_SIZE                                                         92   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_ICON_NAME_LENGTH                                                   93   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_PROG_FILE_SIZE_IPC                                                 94   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_PROG_FILE_SIZE_SBX                                                 98   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_PROG_FILE_SIZE_HMI                                                 102   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_PROG_FILE_SIZE_RSE                                                 106   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_PROG_FILE_SIZE_CD                                                  110   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_PROG_FILE_SIZE_AMP                                                 114   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_OPERATIONAL_PROG_FILE_SIZE_IPC                                     118   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_OPERATIONAL_PROG_FILE_SIZE_SBX                                     122   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_OPERATIONAL_PROG_FILE_SIZE_HMI                                     126   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_OPERATIONAL_PROG_FILE_SIZE_RSE                                     130   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_OPERATIONAL_PROG_FILE_SIZE_CD                                      134   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_OPERATIONAL_PROG_FILE_SIZE_AMP                                     138   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_SPP_MESSAGE_LENGTH                                                 142   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MEC                                                                    143   // BYTE (8-Bit)
#define EOLLIB_OFFSET_NO_SWITCH_PROG_MODE                                                    144   // BYTE (8-Bit)
#define EOLLIB_OFFSET_NOTIFICATION_DELAY_TIMER                                               145   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ODI_CUST_FU40_IND01_ENABLED                                            146   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU40_IND02_ENABLED                                            147   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU40_IND03_ENABLED                                            148   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU40_IND04_ENABLED                                            149   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU40_IND05_ENABLED                                            150   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU40_IND06_ENABLED                                            151   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU40_IND07_ENABLED                                            152   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU40_IND08_ENABLED                                            153   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU41_IND01_ENABLED                                            154   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU41_IND02_ENABLED                                            155   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU41_IND03_ENABLED                                            156   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU41_IND04_ENABLED                                            157   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU41_IND05_ENABLED                                            158   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU41_IND06_ENABLED                                            159   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND01_ENABLED                                            160   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND02_ENABLED                                            161   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND03_ENABLED                                            162   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND04_ENABLED                                            163   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND05_ENABLED                                            164   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND06_ENABLED                                            165   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND07_ENABLED                                            166   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND08_ENABLED                                            167   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND09_ENABLED                                            168   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND10_ENABLED                                            169   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU42_IND11_ENABLED                                            170   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU43_IND01_ENABLED                                            171   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU43_IND02_ENABLED                                            172   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU43_IND03_ENABLED                                            173   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND01_ENABLED                                            174   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND02_ENABLED                                            175   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND03_ENABLED                                            176   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND04_ENABLED                                            177   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND05_ENABLED                                            178   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND06_ENABLED                                            179   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND07_ENABLED                                            180   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND08_ENABLED                                            181   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND09_ENABLED                                            182   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND10_ENABLED                                            183   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND11_ENABLED                                            184   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND12_ENABLED                                            185   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND13_ENABLED                                            186   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU44_IND14_ENABLED                                            187   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU45_IND01_ENABLED                                            188   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU45_IND02_ENABLED                                            189   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU45_IND03_ENABLED                                            190   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU45_IND04_ENABLED                                            191   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU45_IND05_ENABLED                                            192   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU45_IND06_ENABLED                                            193   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU45_IND07_ENABLED                                            194   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU46_IND01_ENABLED                                            195   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU46_IND02_ENABLED                                            196   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU46_IND03_ENABLED                                            197   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU46_IND04_ENABLED                                            198   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU47_IND01_ENABLED                                            199   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU47_IND02_ENABLED                                            200   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU48_IND01_ENABLED                                            201   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU48_IND02_ENABLED                                            202   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU50_IND01_ENABLED                                            203   // BYTE (Boolean)
#define EOLLIB_OFFSET_ODI_CUST_FU51_IND01_ENABLED                                            204   // BYTE (Boolean)
#define EOLLIB_OFFSET_OSS_LINK                                                               206   // WORD (16-Bit) as ARRAY
#define EOLLIB_OFFSET_PROGRAMMING_COMPLETE                                                   286   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PROGRAMMING_ENABLE                                                     287   // BYTE (Boolean)
#define EOLLIB_OFFSET_PROGRAMMING_MASTER                                                     288   // BYTE (Boolean)
#define EOLLIB_OFFSET_PROGRAMMING_RESET_TIMER                                                289   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PROGRAMMING_RETRY_COUNT                                                290   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PWM_FAN_LEVEL1                                                         291   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PWM_FAN_LEVEL2                                                         292   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PWM_FAN_LEVEL3                                                         293   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PWM_FAN_LEVEL4                                                         294   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PWM_FAN_LEVEL5                                                         295   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PWM_FAN_LEVEL6                                                         296   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SNOET                                                                  298   // BYTE (8-Bit) as ARRAY
#define EOLLIB_OFFSET_SPP_BUFFER_SIZE                                                        317   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE1                                          318   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE2                                          319   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_SYSSUPPID_SYSTEMSUPPLIERIDENTIFIER                                     322   // BYTE (8-Bit) as ARRAY
#define EOLLIB_OFFSET_SYSSUPPID_SYSTEMGROUPID                                                330   // BYTE (8-Bit) as ARRAY
#define EOLLIB_OFFSET_SYSSUPPID_RESERVED                                                     334   // BYTE (8-Bit) as ARRAY
#define EOLLIB_OFFSET_SYSTEM_CONFIGURATION                                                   336   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_SYSTEM_OFFERING_BYTE1                                                  337   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_T_SENSOR_MAX_DTC_HOT                                                   338   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_ENTRY_LEVEL_1                                                339   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_ENTRY_LEVEL_2                                                340   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_ENTRY_LEVEL_3                                                341   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_ENTRY_LEVEL_4                                                342   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_ENTRY_LEVEL_5                                                343   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_ENTRY_LEVEL_6                                                344   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_ENTRY_LEVEL_1                                                345   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_ENTRY_LEVEL_2                                                346   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_ENTRY_LEVEL_3                                                347   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_ENTRY_LEVEL_4                                                348   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_ENTRY_LEVEL_5                                                349   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_ENTRY_LEVEL_6                                                350   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_EXIT_LEVEL_1                                                 351   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_EXIT_LEVEL_2                                                 352   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_EXIT_LEVEL_3                                                 353   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_EXIT_LEVEL_4                                                 354   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_EXIT_LEVEL_5                                                 355   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR1_EXIT_LEVEL_6                                                 356   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_EXIT_LEVEL_1                                                 357   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_EXIT_LEVEL_2                                                 358   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_EXIT_LEVEL_3                                                 359   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_EXIT_LEVEL_4                                                 360   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_EXIT_LEVEL_5                                                 361   // BYTE (8-Bit)
#define EOLLIB_OFFSET_T_SENSOR2_EXIT_LEVEL_6                                                 362   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TLIN_MIN_SYS_COMMAND_UPDATE_TIME                                       363   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_MINIMUM_UPDATE_TIME             364   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_PERIODIC_RATE                   365   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_DELAY_FOR_FIRST_PERIODIC_RATE   366   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TX_DTC_TRIGGERED_788_MINIMUM_UPDATE_TIME                               367   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TX_DTC_TRIGGERED_788_PERIODIC_RATE                                     368   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TX_DTC_TRIGGERED_788_DELAY_FOR_FIRST_PERIODIC_RATE                     369   // BYTE (8-Bit)
#define EOLLIB_OFFSET_U0029_DELAY_TIMER                                                      370   // BYTE (8-Bit)
#define EOLLIB_OFFSET_U_HYSTERESIS                                                           371   // BYTE (8-Bit)
#define EOLLIB_OFFSET_U_TH_LOW                                                               372   // BYTE (8-Bit)
#define EOLLIB_OFFSET_U_TH_CRITICAL                                                          373   // BYTE (8-Bit)
#define EOLLIB_OFFSET_U_TH_SUPER                                                             374   // BYTE (8-Bit)
#define EOLLIB_OFFSET_USB_VOLTAGE_RECOVERY_AUTOPLAY_DELAY_TIME                               375   // BYTE (8-Bit)
#define EOLLIB_OFFSET_USB_1_HUB_ENABLE                                                       376   // BYTE (Boolean)
#define EOLLIB_OFFSET_USB_1_HUB_MAX_PORTS                                                    377   // BYTE (8-Bit)
#define EOLLIB_OFFSET_USB_2_HUB_ENABLE                                                       378   // BYTE (Boolean)
#define EOLLIB_OFFSET_USB_2_HUB_MAX_PORTS                                                    379   // BYTE (8-Bit)
#define EOLLIB_OFFSET_WAIT_USER_LOGIN                                                        380   // BYTE (8-Bit)
#define EOLLIB_OFFSET_WAIT_TO_END                                                            381   // BYTE (8-Bit)
#define EOLLIB_OFFSET_WAITING_FOR_FILE                                                       382   // BYTE (8-Bit)
#define EOLLIB_OFFSET_WAITING_FOR_PARKING                                                    383   // BYTE (8-Bit)
#define EOLLIB_OFFSET_WAITING_ON_CLIENT                                                      384   // BYTE (8-Bit)
#define EOLLIB_OFFSET_WAITING_ON_GMLAN                                                       385   // BYTE (8-Bit)
#define EOLLIB_OFFSET_XM_INITIAL_DATA_SID_REQUEST                                            386   // WORD (16-Bit)
#define EOLLIB_OFFSET_RESERVED_BYTE_1_SYSTEM                                                 388   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_SYSTEM                                                 389   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_SYSTEM                                                 390   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_SYSTEM                                                 391   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_SYSTEM                                                 392   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_SYSTEM                                                 393   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_SYSTEM                                                 394   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_SYSTEM                                                 395   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_SYSTEM                                                 396   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_SYSTEM                                                397   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_SYSTEM                                                 398   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_SYSTEM                                                 400   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_SYSTEM                                                 402   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_SYSTEM                                                 404   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_SYSTEM                                                 406   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_SYSTEM                                                 408   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_SYSTEM                                                 410   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_SYSTEM                                                 412   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_SYSTEM                                                 414   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_SYSTEM                                                416   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_SYSTEM                                                418   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_SYSTEM                                                422   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_SYSTEM_END                                                     1485  // max. limit

//DISPLAY INTERFACE
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_HMI_F_B_CAL                                                   0   // BYTE (Boolean)
#define EOLLIB_OFFSET_ADJUSTINGTONESETTINGS_SLEW_RATE                                                          1   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ADJUSTINGTONESETTINGS_RATEOFCHANGE                                                       2   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ATC_MENU_OPTION_ENABLE                                                                   3   // BYTE (Boolean)
#define EOLLIB_OFFSET_ALERT_AUTODISMISSFADEOUTTIME                                                             4   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ALERTWINDOWELEMENT_DELAYEDACTIONPRESENTATIONTIME                                         5   // BYTE (8-Bit)
#define EOLLIB_OFFSET_APPLICATION_REVEALONAPPROACHTIME                                                         6   // BYTE (8-Bit)
#define EOLLIB_OFFSET_CANCLETIME                                                                               7   // BYTE (8-Bit)
#define EOLLIB_OFFSET_CLUSTER_SWIPE_APPLICATION_EVENT_MAX_NOTIFY_RATE                                          8   // BYTE (8-Bit)
#define EOLLIB_OFFSET_D_LONG_AUTO_ACK                                                                          9   // BYTE (8-Bit)
#define EOLLIB_OFFSET_D_MEDIUM_AUTO_ACK                                                                        10   // BYTE (8-Bit)
#define EOLLIB_OFFSET_D_SHORT_AUTO_ACK                                                                         11   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEFAULT_EMAIL_SETTINGS_VIEW_SENT_EMAIL_SETTING                                           12   // BYTE (Boolean)
#define EOLLIB_OFFSET_DEFAULT_MASK_DISPLAY_SETTINGS                                                            13   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEFAULT_SENT_TEXT_MESSAGE_SETTING                                                        14   // BYTE (Boolean)
#define EOLLIB_OFFSET_DTC_MASK_BYTE_MIC2                                                                       15   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_AUDIO                                                                 16   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_PHONE                                                                 17   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_RSE                                                                   18   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_SETTINGS                                                              19   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_AUDIO                                                             20   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_PHONE                                                             21   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_RSE                                                               22   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIONOWPLAYING_SENDTOCLUSTER                                                     23   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_BLUETOOTHSOURCEMODE                                                               24   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_CLIMATECONTROL_QUICK_STATUS_PANE                                                  25   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_FADE                                                                              26   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_NOISE_COMP_TEXT_STRING_OVERRIDE                                                   27   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_POP_UPS_WIDGET_AUTOMATICDISMISSAL                                                 28   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RENAMINGFAVORITES                                                                 29   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_ALBUMS                                                           30   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORTIES_APPLICATION_SELECTION                                            31   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_ARTISTS                                                          32   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_AUDIO_SOURCE_SELECTION                                           33   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_AUDIOBOOKS                                                       34   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_BROADCASTAUDIO                                                   35   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_DIAL_PHONE_NUMBER_WITH_NO_CONTACT_INFO_SELECTION                 36   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_DIAL_VOICEMAIL_SELECTION_ENABLE                                  37   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_GENERATE_BLANK_EMAIL_TO_EMAIL_ADDRESS_SELECTION_ENABLE           38   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_GENERATE_COMPOSED_EMAIL_TO_EMAIL_ADDRESS_SELECTION_ENABLE        39   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_GENERATE_BLANK_SMS_TO_PHONE_NUMBER_CONTACT_SELECTION_ENABLE      40   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_GENERATE_COMPOSED_SMS_TO_PHONE_NUMBER_CONTACT_SELECTION_ENABLE   41   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_GENRES                                                           42   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_INTERNETRADIOSTATIONS                                            43   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_PANDORARADIOSTATIONS                                             44   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_PLAYLISTS                                                        45   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_PODCASTS                                                         46   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_SHUFFLE_TOGGLE_ENABLE                                            47   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_SONGS                                                            48   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_TONESETTINGS                                                     49   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_TUNESELECTALERTMENU                                              50   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_STORINGFAVORITES_VIDEOS                                                           51   // BYTE (Boolean)
#define EOLLIB_OFFSET_F_B_ENABLE_MASK_BYTE_1                                                                   52   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_F_B_ENABLE_MASK_BYTE_2                                                                   53   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_FRONT_TEMPERATURE_ADJUSTMENT_SLEW_RATE                                                   54   // BYTE (8-Bit)
#define EOLLIB_OFFSET_FRONT_FAN_SPEED_ADJUSTMENT_SLEW_RATE                                                     55   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_DECISION_TIMER                                                                   56   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_DOUBLE_TAP_TIME                                                                  57   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_FLING_GAIN                                                                       58   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_FLING_FRICTION                                                                   59   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_FLING_MAX_SPEED                                                                  60   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_FLING_MIN_DISTANCE                                                               61   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_FLING_MIN_SPEED                                                                  62   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_NUDGE_GAIN                                                                       63   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_NUDGE_MIN_DISTANCE                                                               64   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_PINCH_GAIN                                                                       65   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_PINCH_MAX_FACTOR                                                                 66   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_PRESS_HOLD_MAX_DISTANCE                                                          67   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_PRESS_HOLD_TIME_1                                                                68   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_TAP_MAX_DISTANCE                                                                 69   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_TAP_MAX_TIME                                                                     70   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_SPREAD_GAIN                                                                      71   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_SPREAD_MIN_FACTOR                                                                72   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GESTURE_SPREAD_PINCH_DOUBLETOUCH_TIME                                                    73   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAIN_GROUP_MAX_VOLUME                                                                    74   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_AUDIO_TEXT_EMAIL_RECORD_TIME                                                         75   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_CHIME_VOL_STEPS                                                                      76   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_NEW_PREDEFINED_TEXT_EMAIL_MSG_RESPONSES                                              77   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_NEW_TEXT_EMAIL_MESSAGE_RESPONSE_CHARACTERS                                           78   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_SURROUND_LEVEL                                                                       79   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MEDIA_SOURCE_CHANGE_TO_NOW_PLAYING_TIMEOUT                                               80   // BYTE (8-Bit)
#define EOLLIB_OFFSET_METADATA_FULLSCREEN_PAGETIME                                                             81   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MICROPHONE_2_CONFIG                                                                      82   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MID_EQ_ENABLE                                                                            83   // BYTE (Boolean)
#define EOLLIB_OFFSET_MINACTIVITYINDICATORENABLETIME                                                           84   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MINACTIVITYINDICATORACTIVETIME                                                           85   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MOVINGFAVORITE_ROWADVANCMENTSLEWRATE                                                     86   // BYTE (8-Bit)
#define EOLLIB_OFFSET_NOISE_COMP_TEXT_STRING                                                                   90   // WORD (16-Bit) as ARRAY
#define EOLLIB_OFFSET_OAT_DISPLAY_ENABLE                                                                       152   // BYTE (Boolean)
#define EOLLIB_OFFSET_POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_1                                                  153   // BYTE (8-Bit)
#define EOLLIB_OFFSET_POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_2                                                  154   // BYTE (8-Bit)
#define EOLLIB_OFFSET_POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_3                                                  155   // BYTE (8-Bit)
#define EOLLIB_OFFSET_POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_4                                                  156   // BYTE (8-Bit)
#define EOLLIB_OFFSET_POPUPWIDGET_AUTODISMISSFADEOUTTIME                                                       157   // BYTE (8-Bit)
#define EOLLIB_OFFSET_QUICKSTATUSPANEWIDGET_AUTOMATICDISMISSALTIME                                             158   // BYTE (8-Bit)
#define EOLLIB_OFFSET_REARRANGING_APP_ICON_TIMEOUT                                                             159   // BYTE (8-Bit)
#define EOLLIB_OFFSET_REAR_TEMPERATURE_ADJUSTMENT_SLEW_RATE                                                    160   // BYTE (8-Bit)
#define EOLLIB_OFFSET_REAR_FAN_SPEED_ADJUSTMENT_SLEW_RATE                                                      161   // BYTE (8-Bit)
#define EOLLIB_OFFSET_REVEALONAPPROACHFADEOUTTIME                                                              162   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACCELERATION_LISTPART_THRESHOLD                                                   163   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_1                                               164   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACEELERATION_DELTACOUNT_SKIP_1                                                    165   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_2                                               166   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACEELERATION_DELTACOUNT_SKIP_2                                                    167   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_3                                               168   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACEELERATION_DELTACOUNT_SKIP_3                                                    169   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_4                                               170   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACEELERATION_DELTACOUNT_SKIP_4                                                    171   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_5                                               172   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACEELERATION_DELTACOUNT_SKIP_5                                                    173   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACCELERATION_START_DELAY                                                          174   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROTARY_ACCELERATION_CLEAR_TIMER                                                          175   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RSE_FRONT_VIDEO_ENABLED                                                                  176   // BYTE (Boolean)
#define EOLLIB_OFFSET_SCROLLBAR_PAGEUPDOWN_SLEWRATE                                                            177   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SIMULATED_SURROUND_DISPLAY_TEXT_ENABLE                                                   178   // BYTE (Boolean)
#define EOLLIB_OFFSET_SIMULATED_SURROUND_DISPLAY_TEXT_STRING                                                   182   // WORD (16-Bit) as ARRAY
#define EOLLIB_OFFSET_SOURCECHANGETIMEOUT                                                                      204   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SPEECH_SETTING_MENU_MASK_BYTE_1                                                          205   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_TIME_SHIFT_ENABLE                                                                        206   // BYTE (Boolean)
#define EOLLIB_OFFSET_VALET_MASK_MENU                                                                          207   // BYTE (Boolean)
#define EOLLIB_OFFSET_VALET_COMBO_FLASH_FREQUENCY                                                              208   // BYTE (8-Bit)
#define EOLLIB_OFFSET_VALET_COMBO_FLASH_TIME                                                                   209   // BYTE (8-Bit)
#define EOLLIB_OFFSET_VALET_COMBO_DIGITS_SUPPORTED                                                             210   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RESERVED_BYTE_1_DISPLAY                                                                  211   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_DISPLAY                                                                  212   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_DISPLAY                                                                  213   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_DISPLAY                                                                  214   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_DISPLAY                                                                  215   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_DISPLAY                                                                  216   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_DISPLAY                                                                  217   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_DISPLAY                                                                  218   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_DISPLAY                                                                  219   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_DISPLAY                                                                 220   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_DISPLAY                                                                  222   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_DISPLAY                                                                  224   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_DISPLAY                                                                  226   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_DISPLAY                                                                  228   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_DISPLAY                                                                  230   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_DISPLAY                                                                  232   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_DISPLAY                                                                  234   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_DISPLAY                                                                  236   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_DISPLAY                                                                  238   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_DISPLAY                                                                 240   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_DISPLAY                                                                 242   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_DISPLAY                                                                 246   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_F_B_END                                                                          485   // max. limit

//BLUETOOTH
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_BLUETOOTH_CAL          0   // BYTE (Boolean)
#define EOLLIB_OFFSET_BLUETOOTH_CALL_INITIATION_DELAY_TIME              1   // BYTE (8-Bit)
#define EOLLIB_OFFSET_BT_AUDIO_VOICE_CONNECTION_TYPE                    2   // BYTE (8-Bit)
#define EOLLIB_OFFSET_BT_CHAUFFER_MODE_ENABLED                          3   // BYTE (Boolean)
#define EOLLIB_OFFSET_BT_CONNECTION_REQUEST_TIMER                       4   // BYTE (8-Bit)
#define EOLLIB_OFFSET_BT_DISCOVERABLE_MODE_TIMER                        5   // BYTE (8-Bit)
#define EOLLIB_OFFSET_BT_IN_BAND_RINGING                                6   // BYTE (Boolean)
#define EOLLIB_OFFSET_BT_RECORDING_TIME_LIMIT                           7   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEFAULT_BT_UPDATE_INBOX_POLLING_PERIOD            8   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ENABLE_ADVANCED_AUDIO_DISTRIBUTION_PROFILE        9   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUDIO_VIDEO_REMOTE_CONTROL_PROFILE         10   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_BLUETOOTH                                  11   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_CALL_TIMERS                                12   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_DIAL_UP_NETWORKING_PROFILE                 13   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_GENERIC_AUDIO_VIDEO_DISTRIBUTION_PROFILE   14   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_GENERIC_PICTURE_ICON                       15   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_HANDS_FREE_PROFILE                         16   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MESSAGING_ACCESS_PROFILE                   17   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MTP_DEVICES                                18   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_PERSONAL_AREA_NETWORK_PROFILE              19   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_PHONE_AUTOCOMPLETE                         20   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_PHONE_BOOK_ACCESS_PROFILE                  21   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_PHONE_KEYPAD_AUTOCOMPLET_DROPDOWN          22   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_PHONE_STATUS_REGION_BATTERY_LEVEL          23   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_PHONE_STATUS_REGION_ROAMING_INDICATOR      24   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_PHONE_STATUS_REGION_SIGNAL_STRENGTH        25   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_PORTABLE_NAVIGATION_PROFILE                26   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_SERIAL_PORT_PROFILE                        27   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_THREEWAY_CALL_UNMERGE                      28   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_V_CARD_PICTURE_ICON                        29   // BYTE (Boolean)
#define EOLLIB_OFFSET_FAVORITE_PHONE_CALL_ENDED_TIMEOUT                 30   // BYTE (8-Bit)
#define EOLLIB_OFFSET_IGNITION_OFF_IN_CALL_VIEW_DELAY                   31   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_IN_PHONE_SPEECH_REC_ENABLE                        32   // BYTE (Boolean)
#define EOLLIB_OFFSET_LOCAL_PHONE_TIMEOUT                               33   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHONE_CALL_ENDED_INFORMATION_TIMEOUT              34   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHONE_INTERACTION_SELECTOR_IN_CALL_VIEW_TIMEOUT   35   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TELEPHONE_FBLOCK_ALERT_TONE_COUNT                 36   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TELEPHONE_FBLOCK_PAUSE_DURATION                   37   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TELEPHONE_FBLOCK_RETRY_COUNT                      38   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TELEPHONEFBLOCK_ALERT_TONE_DURATION               39   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TELEPHONEFBLOCK_PERIODIC_WAIT_TIME                40   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RESERVED_BYTE_1_WIRELESS                          41   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_WIRELESS                          42   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_WIRELESS                          43   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_WIRELESS                          44   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_WIRELESS                          45   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_WIRELESS                          46   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_WIRELESS                          47   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_WIRELESS                          48   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_WIRELESS                          49   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_WIRELESS                         50   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_WIRELESS                          52   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_WIRELESS                          54   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_WIRELESS                          56   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_WIRELESS                          58   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_WIRELESS                          60   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_WIRELESS                          62   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_WIRELESS                          64   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_WIRELESS                          66   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_WIRELESS                          68   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_WIRELESS                         70   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_WIRELESS                         74   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_WIRELESS                         78   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END                             285  // max. limit

//NAVSYSTEM
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_HMI_NAV_SYSTEM_CAL                              0   // BYTE (Boolean)
#define EOLLIB_OFFSET__3D_BLDG_ICON_DISPLAY_ZOOM_LEVEL                                           2   // DWORD (32-Bit)
#define EOLLIB_OFFSET_ADASIS_SUPPORT                                                             6   // BYTE (8-Bit)
#define EOLLIB_OFFSET_AH_ITERATIONS_STEPS                                                        7   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_AH_ITERATIONS_LENGHT                                                       8   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_AUTOZOOM_LEVEL                                                             10   // DWORD (32-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_BUTTON_PRESS_VOICE_VOLUME_DELTA                                            14   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEFAULT_WEATHER_MENUE_WEATHER_ALERT_SETTING                                15   // BYTE (Boolean)
#define EOLLIB_OFFSET_DESTINATION_DETAIL_UNIQUE_LOCATION_ZOOM_LEVEL                              18   // DWORD (32-Bit)
#define EOLLIB_OFFSET_DESTINATION_DETAIL_STREET_CITY_LOCATION_ZOOM_LEVEL                         22   // DWORD (32-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_DESTINATION_DETAIL_CITY_LOCATION_ZOOM_LEVEL                                26   // DWORD (32-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_DESTINATION_DISPLAY                                                        30   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DS_TRAILING_LENGTH                                                         32   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_DS_TRAILING_PERCENTAGE                                                     34   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_AGP_ARRIVING_AT_DESTINATION_SPECIALIZED_PROMPT                      35   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_APPLICATION_WEATHER                                                 36   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_ETA_TO_MANEUVER                                                     37   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_GOOGLE_POI_SEARCH_RESULTS                                           38   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_GUIDANCE_WHILE_ON_PHONE_CALL                                        39   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ENABLE_HMI_FOR_NAV_WITHOUT_MAP_DB                                          40   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_FAST_FOOD                            41   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_GAS_STATION                          42   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_HOTELS                               43   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_TRAFFIC_LIGHTS                       44   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_2                         45   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_3                         46   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_4                         47   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_5                         48   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_AVOID_TRAFFIC                    49   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_AVOID_TUNNELS                    50   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_CARPOOL_LANES                    51   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_FASTEST                          52   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_FERRIES                          53   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_GREENEST                         54   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_HIGHWAY                          55   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_SEASONAL                         56   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_SHORTEST                         57   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_TOLL_ROADS                       58   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_VOICE_PREFERENCES_PROMPT_STYLE_LANDMARK_GUIDANCE   59   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RELATIVE_DIRECTION_TO_DESTINATION                                   60   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_SCENIC_ROUTE                                                        61   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VEHICLE_ADDRESS_CALLOUT                                             62   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_NAV_FEATURE_POI_ALERT                                               63   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_NAV_ME_OFFROAD_PACKAGE                                              64   // BYTE (Boolean)
#define EOLLIB_OFFSET_HIST_TRAFFIC_ENABLE                                                        65   // BYTE (Boolean)
#define EOLLIB_OFFSET_INITIAL_DEFAULT_FORECAST_INFORMATION                                       66   // BYTE (8-Bit)
#define EOLLIB_OFFSET_INITIAL_HORIZON_TIME                                                       67   // BYTE (8-Bit)
#define EOLLIB_OFFSET_KEYBOARD_AUTOCOMPLETE_HISTORY                                              68   // BYTE (8-Bit)
#define EOLLIB_OFFSET_LANDMARK_GUIDANCE_POI_QUALIFIER                                            69   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MANEUVER_CRITICALITY_DISTANCE_MEDIUM                                       70   // DWORD (32-Bit)
#define EOLLIB_OFFSET_MAX_LENGTH_OF_TRANSMITTED_PATH                                             74   // WORD (16-Bit)
#define EOLLIB_OFFSET_MAX_SINGLE_DESTINATION                                                     76   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_OFFSET_RANGE                                                           78   // WORD (16-Bit)
#define EOLLIB_OFFSET_MIN_PATH_INDICES_REUSE_DISTANCE                                            80   // WORD (16-Bit)
#define EOLLIB_OFFSET_MINIMUM_AH_DRIVETIME                                                       82   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MINIMUM_MLP_BUILDUP_TIME                                                   83   // BYTE (8-Bit)
#define EOLLIB_OFFSET_NO_GPS_ICON_TIMER                                                          84   // BYTE (8-Bit)
#define EOLLIB_OFFSET_NORTH_UP_MODE_CHANGE_ZOOM_LEVEL                                            86   // DWORD (32-Bit)
#define EOLLIB_OFFSET_OFFSET_RANGE_LIMITATION_METHOD                                             90   // BYTE (8-Bit)
#define EOLLIB_OFFSET_POI_EXPANDED_LIST_MAX                                                      91   // BYTE (8-Bit)
#define EOLLIB_OFFSET_POI_HOURS_OF_OPERATION                                                     92   // BYTE (8-Bit)
#define EOLLIB_OFFSET_POI_LIST_INSTANCE_MAX                                                      93   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PROFILE_INTERPOLATION_TYPE                                                 94   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RADIO_MOREFEATURE_DAB_LINK                                                 95   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_RADIO_MOREFEATURE_L_BAND                                                   96   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_RETRANSMIT_HORIZON_TIME                                                    97   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ROUTE_OVERVIEW_TIME                                                        98   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RT_GAS_PRICES_ENABLE                                                       99   // BYTE (Boolean)
#define EOLLIB_OFFSET_RT_MOVIE_THEATER_INFO_ENABLE                                               100   // BYTE (Boolean)
#define EOLLIB_OFFSET_RT_TRAFFIC_ENABLE                                                          101   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_RT_WEATHER_ENABLE                                                          102   // BYTE (Boolean)
#define EOLLIB_OFFSET_RT_WEATHER_STORMTRACKER_ENABLE                                             103   // BYTE (Boolean)
#define EOLLIB_OFFSET_RT_HORIZON_DISPLAY                                                         104   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TRAFFIC_REROUTE_ALERT_DISTANCE_GUIDANCE                                    106   // DWORD (32-Bit)
#define EOLLIB_OFFSET_TRAFFIC_REROUTE_ALERT_TIME_GUIDANCE                                        110   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TURN_LIST_DISTANCE_TO_MANEUVER_OPTION                                      111   // BYTE (8-Bit)
#define EOLLIB_OFFSET_WARNING_SCREEN_FREQUENCY                                                   112   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ZOOM_INDICATOR_TIMEOUT                                                     113   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RESERVED_BYTE_1_NAV_SYSTEM                                                 114   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_NAV_SYSTEM                                                 115   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_NAV_SYSTEM                                                 116   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_NAV_SYSTEM                                                 117   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_NAV_SYSTEM                                                 118   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_NAV_SYSTEM                                                 119   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_NAV_SYSTEM                                                 120   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_NAV_SYSTEM                                                 121   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_NAV_SYSTEM                                                 122   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_NAV_SYSTEM                                                123   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_NAV_SYSTEM                                                 124   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_NAV_SYSTEM                                                 126   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_NAV_SYSTEM                                                 128   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_NAV_SYSTEM                                                 130   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_NAV_SYSTEM                                                 132   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_NAV_SYSTEM                                                 134   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_NAV_SYSTEM                                                 136   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_NAV_SYSTEM                                                 138   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_NAV_SYSTEM                                                 140   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_NAV_SYSTEM                                                142   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_NAV_SYSTEM                                                146   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_NAV_SYSTEM                                                150   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END                                                    485   // max. limit

//NAVICON
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_HMI_NAV_ICON_CAL   0   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_ACUE_RUN_CUE                        1   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_ACUE_STOP_CUE                       2   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_SHUTDOWN_ENABLE                     3   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_STARTUP_ENABLE                      4   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_WELCOME_ENABLE                      5   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_RESERVED_3                          6   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_RESERVED_4                          7   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_RESERVED_5                          8   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_RESERVED_6                          9   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_RESERVED_7                          10   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUDIO_CUE_RESERVED_8                          11   // BYTE (Boolean)
#define EOLLIB_OFFSET_DEFAULT_POI_FUEL_STATION                      12   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEPARTURETIME_SLEWRATE                        13   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEPARTURETIME_BUTTONHOLDTIME                  14   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEPARTURETIME_HOURINCREMENTDECREMENTRATE      15   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEPARTURETIME_MINUTEINCREMENTDECREMENTRATE    16   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ECO_ROUTE_CURVE                               17   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_HYBRID                     18   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_HYBRID                 19   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENERGYSUMMARY_POPUPREQUEST_WAITTIMER          20   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ENERGYSUMMARY_POPUP_DISPLAYTIMER              21   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ENERGYSUMMARY_MINIMUMPOPUP_DISPLAYTIMER       22   // BYTE (8-Bit)
#define EOLLIB_OFFSET_HYBRID_SYSTEM_HMI                             23   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_INVALID_DATA_POPUP_TIMER                      24   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SCHEDULE_UPDATE_TIMEOUT_TIMER                 25   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SETTINGS_MASK_BYTE_1                          26   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_MAXALLOWED_FUELECONOMY_DISPLAY                28   // WORD (16-Bit)
#define EOLLIB_OFFSET_MM_SCREEN_POPUP_TIMER                         30   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RESERVED_BYTE_1_NAV_MAP_DB                    31   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_NAV_MAP_DB                    32   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_NAV_MAP_DB                    33   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_NAV_MAP_DB                    34   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_NAV_MAP_DB                    35   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_NAV_MAP_DB                    36   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_NAV_MAP_DB                    37   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_NAV_MAP_DB                    38   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_NAV_MAP_DB                    39   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_NAV_MAP_DB                   40   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_NAV_MAP_DB                    42   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_NAV_MAP_DB                    44   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_NAV_MAP_DB                    46   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_NAV_MAP_DB                    48   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_NAV_MAP_DB                    50   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_NAV_MAP_DB                    52   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_NAV_MAP_DB                    54   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_NAV_MAP_DB                    56   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_NAV_MAP_DB                    58   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_NAV_MAP_DB                   60   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_NAV_MAP_DB                   62   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_NAV_MAP_DB                   66   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END                          85   // max. limit

//BRAND
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_HMI_BRAND_CAL                    0   // BYTE (Boolean)
#define EOLLIB_OFFSET_BLUETOOTH_MODULE_DEVICE_NAME_STRING                         2   // BYTE (8-Bit) as ARRAY
#define EOLLIB_OFFSET_CHARACTER_RECOGNITION_ON_FRONT_TOUCHSCREEN_DISPLAY_ENABLE   35   // BYTE (Boolean)
#define EOLLIB_OFFSET_CHEVY_APPS_TRAY_INACTIVITY_TIMER                            36   // BYTE (8-Bit)
#define EOLLIB_OFFSET_CHEVY_THEMES_AVAILABLE_BYTE                                 37   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DEFAULT_CHEVY_GRAPHICAL_THEME                               38   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEFAULT_AUDIO_TOUCH_FEEDBACK_SETTING                        39   // BYTE (Boolean)
#define EOLLIB_OFFSET_DEFAULT_MIRRORLINK_NOTIFICATION_SETTING                     40   // BYTE (Boolean)
#define EOLLIB_OFFSET_DMC3_VISUALIZATION_SCREEN_AUTO_MODE_ENABLE                  41   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_DMC3_VISUALIZATION_SCREEN_SPORT_MODE_ENABLE                 42   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_DTC_MASK_USB1                                               43   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DTC_MASK_USB2                                               44   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DTC_LVDS                                                    45   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_DIGITAL_IPOD_OUT                         46   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_APPLICATION_DMC3                                     47   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_APPLICATION_INTERIOR_LIGHTING                        48   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_MIRRORLINK                               49   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_APPLICATION_PROJECTION                               50   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_CLIMATECONTROL                       51   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_PROJECTION                           52   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_DMC3                                 53   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_INTERIOR_LIGHTING                    54   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_REAR_CLIMATECONTROL                  55   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_NAVIGATION                           56   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_BOSE_PANARAY_LOGO                                    57   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_ALL_TERRAIN                                58   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_COMFORT                                    59   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_ECO                                        60   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_NORMAL                                     61   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_OFF_ROAD                                   62   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_SNOW                                       63   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_SNOW_ICE                                   64   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_SPORT                                      65   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_TOUR                                       66   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_TRACK                                      67   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_TOW_HAUL                                   68   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_DMC3_MODE_WEATHER                                    69   // BYTE (Boolean) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_ENABLE_FACEPLATE_VOLUME_TAP                                 70   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_FACEPLATE_VOLUME_PRESS_HOLD                          71   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MIRRORLINK_NOTIFICATION                              72   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_REVEALONAPPROACH                                     73   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_REVEALONAPPROACHFADEOUT                              74   // BYTE (Boolean)
#define EOLLIB_OFFSET_FACEPLATE_FLING_MIN_SLIDER_POSITIONS                        75   // BYTE (8-Bit)
#define EOLLIB_OFFSET_FACEPLATE_TAP_MAX_TIME                                      76   // BYTE (8-Bit)
#define EOLLIB_OFFSET_FACEPLATE_VOLUME_STEP_PER_SLIDER_SENSOR                     78   // WORD (16-Bit)
#define EOLLIB_OFFSET_FACEPLATE_VOLUME_TAP_VOLUME_STEP_DELTA                      80   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GM_VEHICLE_BRAND                                            81   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GYRO_OFFSET_X                                               82   // WORD (16-Bit)
#define EOLLIB_OFFSET_GYRO_OFFSET_Y                                               84   // WORD (16-Bit)
#define EOLLIB_OFFSET_GYRO_OFFSET_Z                                               86   // WORD (16-Bit)
#define EOLLIB_OFFSET_HANDWRITING_AREA_CLEAR_TIMER                                88   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_HAPTIC_FEEDBACK_ON_FRONT_TOUCHSCREEN_DISPLAY_ENABLE         89   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_HVAC_RADIO_DISPLAY_ENABLE                                   90   // BYTE (Boolean)
#define EOLLIB_OFFSET_K_UPA_TYPE                                                  91   // BYTE (8-Bit)
#define EOLLIB_OFFSET_L_REAR_CROSS_TRAFFIC_U_PIXEL                                92   // WORD (16-Bit)
#define EOLLIB_OFFSET_L_REAR_CROSS_TRAFFIC_V_PIEXEL                               94   // WORD (16-Bit)
#define EOLLIB_OFFSET_MAIN_DISP_FAILSOFT                                          96   // WORD (16-Bit)
#define EOLLIB_OFFSET_MAN_MODE                                                    98   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAX_FACEPLATE_VOLUME_SLIDER_SENSORS                         99   // BYTE (8-Bit)
#define EOLLIB_OFFSET_R_REAR_CROSS_TRAFFIC_U_PIXEL                                100   // WORD (16-Bit)
#define EOLLIB_OFFSET_R_REAR_CROSS_TRAFFIC_V_PIXEL                                102   // WORD (16-Bit)
#define EOLLIB_OFFSET_REAR_CROSS_TRAFFIC_ALERT_FLASHING_HZ                        104   // BYTE (8-Bit)
#define EOLLIB_OFFSET_REAR_HVAC_RADIO_DISPLAY_ENABLE                              105   // BYTE (Boolean)
#define EOLLIB_OFFSET_REAR_HVAC_RADIO_DISPLAY_TYPE                                106   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_REAR_MAN_MODE                                               107   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_1                                    108   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_1                                    110   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_2                                    112   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_2                                    114   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_3                                    116   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_3                                    118   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_4                                    120   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_4                                    122   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_5                                    124   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_5                                    126   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_6                                    128   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_6                                    130   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_7                                    132   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_7                                    134   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_8                                    136   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_8                                    138   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_9                                    140   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_9                                    142   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_10                                   144   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_10                                   146   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_11                                   148   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_11                                   150   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_U_PIXEL_12                                   152   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_UPA_CENTER_V_PIXEL_12                                   154   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_STEER_REAR_AXLE_OFFSET                                  156   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_STEER_INVERSE_FRONT_RATIO                               158   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_STEER_INVERSE_REAR_RATIO                                160   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_STEER_WHEELBASE                                         162   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_STEER_SATURATION_ANGLE                                  164   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_STEERING_WHEEL_DELTA                                    165   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_OVLAY_TRACK_WIDTH                                       166   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_OVLAY_DELTA_DIST_MARKS                                  167   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_OVLAY_NUM_DIST_MARKS                                    168   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_ROTATION_X                                       170   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_ROTATION_Y                                       172   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_ROTATION_Z                                       174   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_OFFSET_X                                         176   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_OFFSET_Y                                         177   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_OFFSET_Z                                         178   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_FOCAL_LENGTH_U                                   180   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_FOCAL_LENGTH_V                                   182   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_DISTORT_COEF_1                                   184   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_DISTORT_COEF_2                                   186   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_OPTICAL_OFFSET_U                                 188   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_RVS_CAMERA_OPTICAL_OFFSET_V                                 189   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_RVS_CAMERA_DELTA_PITCH                                      190   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_DELTA_ROLL                                       191   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_DELTA_YAW                                        192   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_CAMERA_DISP_BORDER_WIDTH                                193   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_RVS_ON_HYSTERESIS                                           194   // WORD (16-Bit)
#define EOLLIB_OFFSET_RVS_OFF_HYSTERESIS                                          196   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_DISABLE_HYSTERESIS_SPEED                                197   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SETTINGS_MASK_BYTE_3                                        198   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_TOUCHPAD_AUDIO_FEEDBACK_KEYBOARD                            199   // BYTE (Boolean)
#define EOLLIB_OFFSET_TOUCHPAD_AUDIO_FEEDBACK_LIST                                200   // BYTE (Boolean)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_1                    201   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CARPLAY_ENABLE                                     202   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_2                    203   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_3                    204   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_1                     205   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_2                     206   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_3                     207   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_1                     208   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_2                     209   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_3                     210   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SETTING_DEFAULT              211   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_FASTER                212   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_DEFAULT               213   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_SLOWER                214   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_SETTING_DEFAULT       215   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHARACTER_RECOGNITION_ON_TOUCHPAD_ENABLE           216   // BYTE (Boolean)
#define EOLLIB_OFFSET_TOUCHPAD_CHARACTER_RECOGNITION_PANE_TIMEOUT                 217   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_DISPLAY_SCALING_FACTOR_X                           218   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_DISPLAY_SCALING_FACTOR_Y                           219   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_FBS_ENABLE_MASK_BYTE_1                             220   // BYTE (8-Bit-Mask) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_TOUCHPAD_FBS_ENABLE_MASK_BYTE_2                             221   // BYTE (8-Bit-Mask) --> MOVED from DISPLAY INTERFACE Block
#define EOLLIB_OFFSET_TOUCHPAD_FBS_ENABLE_MASK_BYTE_3                             222   // BYTE (8-Bit-Mask) --> MOVED from DISPLAY INTERFACE Block and MODIFIED
#define EOLLIB_OFFSET_TOUCHPAD_FOCUS_CHANGE_HYSTERESIS                            223   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_DOUBLE_TAP_TIME                            224   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_PRESS_HOLD_TIME_1                          225   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_PRESS_HOLD_TIME_2                          226   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_PRESS_HOLD_TIME_3                          227   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_SPREAD_MIN_FACTOR                          228   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_PINCH_MAX_FACTOR                           229   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_FLING_MIN_DISTANCE                         230   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_FLING_MIN_SPEED                            231   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_SPREAD_GAIN                                232   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_PINCH_GAIN                                 233   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_NUDGE_GAIN                                 234   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_FLING_GAIN                                 235   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_FLING_FRICTION                             236   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_TAP_MAX_DISTANCE                           237   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_GLIDE_MIN_DISTANCE                         238   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_PINCHSPREAD_MAX_DISTANCE                   239   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_DECISION_TIMER                             240   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_FLING_MAX_SPEED                            241   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_GLIDE_GAIN                                 242   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_GLIDE_LIST_ACCELERATION_COEFF                      246   // DWORD (32-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_HIGHLIGHT_TIMEOUT                                  250   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_TOUCHPAD_MIRRORLINK_ENABLE                                  251   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_NAV_MENU_DELAY                                     252   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_TIME_TO_RECOGNIZE_A_DOUBLE_TAP                     253   // BYTE (8-Bit)
#define EOLLIB_OFFSET_UPA_LINE_WIDTH                                              254   // BYTE (8-Bit)
#define EOLLIB_OFFSET_VEHICLE_ANIMATION_SHUTDOWN_TYPE                             255   // BYTE (8-Bit)
#define EOLLIB_OFFSET_VEHICLE_ANIMATION_STARTUP_TYPE                              256   // BYTE (8-Bit)
#define EOLLIB_OFFSET_VEHICLE_ANIMATION_WELCOME_TYPE                              257   // BYTE (8-Bit)
#define EOLLIB_OFFSET_VEHICLE_AUDIOCUE_SHUTDOWN_TYPE                              258   // BYTE (8-Bit)
#define EOLLIB_OFFSET_VEHICLE_AUDIOCUE_STARTUP_TYPE                               259   // BYTE (8-Bit)
#define EOLLIB_OFFSET_VEHICLE_AUDIOCUE_WELCOME_TYPE                               260   // BYTE (8-Bit)
#define EOLLIB_OFFSET_VEHICLE_ICON_BODYSTYLE                                      261   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_VOLUME_PRESS_HOLD_VOLUMESLEWRATE                            262   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RESERVED_BYTE_1_BRAND                                       263   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_BRAND                                       264   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_BRAND                                       265   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_BRAND                                       266   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_BRAND                                       267   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_BRAND                                       268   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_BRAND                                       269   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_BRAND                                       270   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_BRAND                                       271   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_BRAND                                      272   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_BRAND                                       274   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_BRAND                                       276   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_BRAND                                       278   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_BRAND                                       280   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_BRAND                                       282   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_BRAND                                       284   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_BRAND                                       286   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_BRAND                                       288   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_BRAND                                       290   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_BRAND                                      292   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_BRAND                                      294   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_BRAND                                      298   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_BRAND_END                                           485   // max. limit

//COUNTRY
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_COUNTRY_CAL                        0   // BYTE (Boolean)
#define EOLLIB_OFFSET_AM_CATEGORIES_SETTING_PRESENT                                 1   // BYTE (Boolean)
#define EOLLIB_OFFSET_AUTOSTART_ENABLED                                             2   // BYTE (Boolean)
#define EOLLIB_OFFSET_BT_VEHICLE_MOVING_OVERIDE                                     3   // BYTE (Boolean)
#define EOLLIB_OFFSET_DAB_TUNING_MSG_TIME                                           4   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_US_ENGILSH                         5   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_UK_ENGLISH                         6   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_FRENCH                             7   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_FRENCH_CANADIAN                    8   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_SPANISH                            9   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_SPANISH_LATIN                      10   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_PORTUGUESE                         11   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_BRAZILIAN_PORTUGUESE               12   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_BULGARIAN                          13   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_CROATIAN                           14   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_CZECH                              15   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_DANISH                             16   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_DUTCH                              17   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_ESTONIAN                           18   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_FINNISH                            19   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_GERMAN                             20   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_GREEK                              21   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_HUNGARIAN                          22   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_ITALIAN                            23   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_LATVIAN                            24   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_LITHUANIAN                         25   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_MACEDONIAN                         26   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_NORWEGIAN                          27   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_POLISH                             28   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_ROMANIAN                           29   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_RUSSIAN                            30   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_SLOVAK                             31   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_SLOVENIAN                          32   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_SWEDISH                            33   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_TURKISH                            34   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_UKRAINIAN                          35   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_UZBEK                              36   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_ARABIC                             37   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_THAI                               38   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_KOREAN                             39   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_JAPANESE                           40   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_STANDARD_CHINESE                   41   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMAT_MASK_SEPERATOR_TRADITIONAL_CHINESE                42   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_US_ENGLISH              43   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_UK_ENGLISH              44   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_FRENCH                  45   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_FRENCH_CANADIAN         46   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SPANISH                 47   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SPANISH_LATIN           48   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_PORTUGUESE              49   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_BRAZILIAN_PORTUGUESE    50   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_BULGARIAN               51   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_CROATIAN                52   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_CZECH                   53   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_DANISH                  54   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_DUTCH                   55   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ESTONIAN                56   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_FINNISH                 57   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_GERMAN                  58   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_GREEK                   59   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_HUNGARIAN               60   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ITAILIAN                61   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_LATVIAN                 62   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_LITHUANIAN              63   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_MACEDONIAN              64   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_NORWEGIAN               65   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_POLISH                  66   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ROMANIAN                67   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_RUSSIAN                 68   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SLOVAK                  69   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SLOVENIAN               70   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SWEDISH                 71   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_TURKISH                 72   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_UKRAINIAN               73   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_UZBEK                   74   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ARABIC                  75   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_THAI                    76   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_KOREAN                  77   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_JAPANESE                78   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_STANDARD_CHINESE        79   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_TRADITIONAL_CHINESE     80   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEFAULT_3D_BUILDINGS_SETTING                                  81   // BYTE (Boolean)
#define EOLLIB_OFFSET_DEFAULT_AUTOZOOM_SETTING                                      82   // BYTE (Boolean)
#define EOLLIB_OFFSET_DEFAULT_DOPPLER_MAP_SCALE                                     83   // BYTE (8-Bit)
#define EOLLIB_OFFSET_DEFAULT_PREFERRED_PHONEBOOK_SORT_ORDER                        84   // BYTE (Boolean)
#define EOLLIB_OFFSET_DEFAULT_MASK_BT_SETTINGS                                      85   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DIRECT_TUNE_AM_FM_DAB_METHOD                                  86   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_DIRECT_TUNE_CLEAR_FREQ_TIMEOUT                                87   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_DIRECT_TUNE_XM_TIMEOUT                                        88   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_DOPPLER_MAP_DISPLAY                                           89   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DRIVE_SIDE                                                    90   // BYTE (Boolean)
#define EOLLIB_OFFSET_DTC_INTERNET_RESETS                                           91   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_DTC_MASK_BYTE_MIC1                                            92   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_AM_FM_MASK_BYTE                                           93   // BYTE (8-Bit-Mask) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_DWL_OWNERS_MANUAL_MASK_BYTE                                   94   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_XM_DAB_DMB_MASK_BYTE                                      95   // BYTE (8-Bit-Mask) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_DWL_INTERNET_RADIO_MASK_BYTE1                                 96   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_PANDORA_MASK_BYTE1                                        97   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_ALL_NAVIGATION_MASK_BYTE                                  98   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_NAV_WEATHER_MAP_SCROLLING                                 99   // BYTE (Boolean)
#define EOLLIB_OFFSET_DWL_NAV_PREVIOUS_DEST                                         100   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_NAV_CONTACTS                                              101   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_NAV_POI_LISTS                                             102   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_NAV_MAP_MENU                                              103   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_NAV_TURN_LISTS                                            104   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_MEDIA                                                     105   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_RSE                                                       106   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_CUSTOMIZATION_SETTINGS_BYTE1                              107   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_CUSTOMIZATION_SETTINGS_BYTE2                              108   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_PHONE_MASK_BYTE_1                                         109   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_MMS_SMS_MASK_BYTE1                                        110   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_EMAIL_MASK_BYTE1                                          111   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_WEATHER_MASK_BYTE1                                        112   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_MULTI_WAY_SEAT_DISPLAY_LOCKOUT                            113   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_SEAT_PANE_LOCKOUT                                         114   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DWL_ENERGY_LOCKOUT                                            115   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_DWL_MIRRORLINK                                                116   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_DWL_TEEN_SEATBELT_RESTRICTION_MODE                            117   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_12_24_CLOCK                                            118   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_3D_BUILDINGS                                           119   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AM_AUDIOMOREFEATURE_TRAFFICINFORMATION_JAPAN           120   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AM_FM_AUDIOMOREFEATURE_HD_SWITCHING_RADIO_ID           121   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AM_FM_DAB_AUDIOMOREFEATURE_UPDATESTATIONLIST           122   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_DMB                                        123   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_HANDSET_VR                                 124   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_APPLICATION_NAVIGATION_LVM                             125   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_ONSTAR                                     126   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_APPLICATION_ONSTAR_TBT                                 127   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_ENABLE_APPLICATION_PANDORA                                    128   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_APPLICATION_PERFORMANCE_DATA_RECORDER                  129   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_TEXT_SMS                                   130   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_XM                                         131   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_DMB                                    132   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_HANDSET_VR                             133   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_INTERNET_APP_6                         134   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_NAVIGATION_LVM                         135   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_PERFORMANCE_DATA_RECORDER              136   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_TEXT_MMS                               137   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_TEXT_SMS                               138   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_TRAFFIC                                139   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_WEATHER                                140   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPTRAY_NAV_MODE_QSP                                   141   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUTO_SET_BUTTON                                        142   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUTO_SET_MENU_SELECTION_CELL_NETWORK                   143   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUTO_SET_MENU_SELECTION_RDS                            144   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_AUTOZOOM                                               145   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_BAND_SOURCE_INTERNET_RADIO                             146   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_CITY_MODELS_2D                                         147   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_CITY_MODELS_3D                                         148   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_GLONAS_AS_GPS_DEFAULT                                  149   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MANUAL_CVP_SWITCH_FOR_ELEVATED_ROAD_DETECTION          150   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_ME_OFFROADS         151   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_NAV_ME_OFFROAD_PACKAGE_MENU_SELECTION                  152   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_ONROUTE_ALERT                                          153   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_ONSTAR_REGIONAL_BRANDING_STRING                        154   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RFD_UPDATES_FOR_FUEL_PRICES                            155   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RFD_UPDATES_FOR_MOVIE_TIMES                            156   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RFD_UPDATES_FOR_TABULAR_WEATHER                        157   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RFD_UPDATES_FOR_WEATHER_ALERTS                         158   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_SCENIC_ROUTE_DEPARTURE_THRESHOLD                       159   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_SPEECHRECOGNITIONDOMAIN_NAVIGATION                     160   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_SPEECHRECOGNITIONDOMAIN_ONSTARHANDOFF                  161   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_TRAFFIC_AHEAD_ALERT                                    162   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_VOICEMAIL_VIEW_BUTTON                                  163   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_WEATHER_ALERTS                                         164   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_XM_AUDIOMOREFEATURE_TUNESELECT                         165   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_XM_LIST_VIEW_ARTISTVIEW                                166   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_XM_LIST_VIEW_CHANNELVIEW                               167   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_XM_LIST_VIEW_SONGVIEW                                  168   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_15                                169   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_14                                170   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_13                                171   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_12                                172   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_11                                173   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_10                                174   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_9                                 175   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_8                                 176   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_7                                 177   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_6                                 178   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_5                                 179   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_4                                 180   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_3                                 181   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_2                                 182   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_1                                 183   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_RESERVED_APPLICATION_0                                 184   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_SPEECHRECOGNITIONDOMAIN_DAB                            185   // BYTE (Boolean) --> MOVED from NAV-SYS Block
#define EOLLIB_OFFSET_ENABLE_SPEECHRECOGNITIONDOMAIN_FM_RDS                         186   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_ENABLE_SPEECHRECOGNITIONDOMAIN_XM_AUDIO                       187   // BYTE (Boolean) --> MOVED from NAV-SYS Block
#define EOLLIB_OFFSET_FBLOCK_MESSAGE_MASK_ENABLE_INTERNETBROWSER_FBLOCK             188   // BYTE (Boolean)
#define EOLLIB_OFFSET_FBLOCK_MESSAGE_MASK_ENABLE_LVMINTERFACE_FBLOCK                189   // BYTE (Boolean)
#define EOLLIB_OFFSET_FM_ARTIST_EXPERIENCE_HD_IMAGE_CHANGE_TIMEOUT                  190   // BYTE (8-Bit)
#define EOLLIB_OFFSET_FM_CATEGORIES_SETTING_PRESENT                                 191   // BYTE (Boolean)
#define EOLLIB_OFFSET_FM_RDS_DAB_INFORMATION_REGION                                 192   // BYTE (8-Bit) --> Parameter name CORRECTED and MODIFIED
#define EOLLIB_OFFSET_FM_RDS_SEEK_TUNING_MSG_TIME                                   193   // BYTE (8-Bit)
#define EOLLIB_OFFSET_GM_BACK_OFFICE_BASEURL                                        194   // WORD (16-Bit) as ARRAY
#define EOLLIB_OFFSET_HD_ARTIST_EXPERIENCE_ENABLED_SETTING_PRESENT                  274   // BYTE (Boolean)
#define EOLLIB_OFFSET_HD_DISPLAY_DIGITAL_ACQ                                        276   // WORD (16-Bit)
#define EOLLIB_OFFSET_INTERNET_BROWSER_FBLOCK_MANAGER_MASK_BYTE_1                   278   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_INTERNET_RADIO_MASK_BYTE_1                                    279   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_JAVASCRIPTAPI_MASK_BYTE_0                                     280   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_JAVASCRIPTAPI_MASK_BYTE_1                                     281   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_JAVASCRIPTAPI_MASK_BYTE_2                                     282   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_JAVASCRIPTAPI_MASK_BYTE_3                                     283   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_JAVASCRIPTAPI_MASK_BYTE_4                                     284   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_JAVASCRIPTAPI_MASK_BYTE_5                                     285   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_JAVASCRIPTAPI_MASK_BYTE_6                                     286   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_JAVASCRIPTAPI_MASK_BYTE_7                                     287   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_LANGUAGE_ENABLE_MASK_BYTE_1                                   288   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_LANGUAGE_ENABLE_MASK_BYTE_2                                   289   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_LANGUAGE_ENABLE_MASK_BYTE_3                                   290   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_LANGUAGE_ENABLE_MASK_BYTE_4                                   291   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_LANGUAGE_ENABLE_MASK_BYTE_5                                   292   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_LANGUAGE_ENABLE_MASK_BYTE_6                                   293   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_LOCKOUT_PRNDL_SPEED                                           294   // BYTE (8-Bit)
#define EOLLIB_OFFSET_LOCKOUT_SPEED                                                 295   // BYTE (8-Bit)
#define EOLLIB_OFFSET_LVM_1                                                         296   // BYTE (8-Bit)
#define EOLLIB_OFFSET_LVM_2                                                         297   // BYTE (8-Bit)
#define EOLLIB_OFFSET_LVM_3                                                         298   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MARKETING_REGION                                              299   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_MICROPHONE_1_CONFIG                                           300   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MULTIWAYSEAT_STATUS_PANE_AUTO_DISMISS_TIMER                   301   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MULTIWAYSEAT_STATUS_PANE_MINI_MODE_TIMER                      302   // BYTE (8-Bit)
#define EOLLIB_OFFSET_NETWORK_CONNECTION_MANAGEMENT_MASK_BYTE_1                     303   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_ONSTAR_REGIONAL_BRANDING_STRING                               306   // WORD (16-Bit) as ARRAY
#define EOLLIB_OFFSET_ONSTAR_ICON_ONSTAR_NA                                         326   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_ONSTAR_ICON_CHINA_STAR                                        327   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_ONSTAR_ICON_EUROPE_STAR                                       328   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_ONSTAR_ICON_CHEVY_STAR                                        329   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_ONSTAR_EMG_CALL_END_BUTTON                                    330   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ONSTAR_REGIONAL_BRAND_HMI_LOGO                                331   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PREVIOUSLY_RUNNING_APPS_RESTART_ENABLED                       332   // BYTE (Boolean)
#define EOLLIB_OFFSET_RSC_SCREEN_LOCKOUT                                            333   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RT_TRAFFIC_SOURCE                                             334   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SETTINGS_MASK_BYTE_4                                          335   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_SIGNING_CERTIFICATE_ISSUER                                    338   // WORD (16-Bit) as ARRAY
#define EOLLIB_OFFSET_SPELLER_CHINESE_SOGU_KEYBOARD_ENABLE                          466   // BYTE (Boolean)
#define EOLLIB_OFFSET_SUPPLEMENTAL_MICROPHONE_DELAY                                 467   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SUPPRESS_SERVICE_INFO                                         468   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_TEEN_DRIVER_KEY_REGISTRATION_LOCKOUT                          469   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TEEN_DRIVER_LOCKOUT_SPEED                                     470   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TEEN_DRIVER_SETTINGS_LOCKOUTS                                 471   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TEEN_DRIVER_VOLUME_LIMIT_MENU_ENABLE                          472   // BYTE (Boolean)
#define EOLLIB_OFFSET_TEEN_MODE_CODE_FLASH_FREQUENCY                                474   // WORD (16-Bit)
#define EOLLIB_OFFSET_TEEN_MODE_CODE_FLASH_TIME                                     476   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TEEN_MODE_SEATBELT_POPUP_AUTODISMISS_TIME                     477   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TODC_VOLTAGE_DISPLAY                                          478   // BYTE (8-Bit)
#define EOLLIB_OFFSET_TOUCHPAD_CHAR_RECOG_BEST_MATCH_LIST_TIMEOUT                   479   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_CHARACTER_RECOGNITION_VEHICLE_SPEED_LOCKOUT_ENABLE   480   // BYTE (Boolean)
#define EOLLIB_OFFSET_TOUCHPAD_CURSOR_TRACKING_SPEED_SLOW                           481   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_CURSOR_TRACKING_SPEED_NORMAL                         482   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_CURSOR_TRACKING_SPEED_FAST                           483   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_CURSOR_SHORT_DISTANCE_SNAP_TIME                      484   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_CURSOR_LONG_DISTANCE_SNAP_TIME                       485   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_ENABLE_TAP_TO_ENTER                                  486   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_ENABLE_TAP_TO_ENTER_DEFAULT_SETTING                  487   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_FAST_SCROLL_SCALING                                  488   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_FAST_SCROLL_TIMEOUT                                  489   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_DOUBLE_CLICK_TIME                            490   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_INDICATION_TIMER                             491   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_GESTURE_TRIPLE_CLICK_TIME                            492   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_HAPTIC_FEEDBACK_ENABLE                               493   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_HAPTIC_FEEDBACK_ENABLE_DEFAULT_SETTING               494   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_HOVER_ANIMATION_TIME_SHORT                           495   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_HOVER_ANIMATION_TIME_LONG                            496   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_HOVER_ANIMATION_TIME_CHAIN                           497   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_HOVER_ANIMATION_DISTANCE_THRESHOLD                   498   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_INTERNETAPPS_ENABLE                                  499   // BYTE (Boolean) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_REACTIVATION_COUNTER                                 500   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TOUCHPAD_REACTIVATION_TIME                                    501   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_TRAFFIC_ROUTING_PREFERENCE_DEFAULT                            502   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_UNLOCK_DWL_LOCKOUTS_BYTE1                                     503   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_VALET_MODE_LOCKOUT_SPEED                                      504   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_VALET_MODE_SETTINGS_LOCKOUTS                                  505   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_VIN_RELEARN_EN                                                506   // BYTE (Boolean)
#define EOLLIB_OFFSET_XM_ADVISORY_MSG_TIMEOUT                                       507   // BYTE (8-Bit)
#define EOLLIB_OFFSET_XM_NO_CAT_FOUND_MSG_TIMEOUT                                   508   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RESERVED_APPLICATION_MASK_BYTE_0                              509   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_RESERVED_BYTE_1_COUNTRY                                       510   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_COUNTRY                                       511   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_COUNTRY                                       512   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_COUNTRY                                       513   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_COUNTRY                                       514   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_COUNTRY                                       515   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_COUNTRY                                       516   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_COUNTRY                                       517   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_COUNTRY                                       518   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_COUNTRY                                      519   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_COUNTRY                                       520   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_COUNTRY                                       522   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_COUNTRY                                       524   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_COUNTRY                                       526   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_COUNTRY                                       528   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_COUNTRY                                       530   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_COUNTRY                                       532   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_COUNTRY                                       534   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_COUNTRY                                       536   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_COUNTRY                                      538   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_COUNTRY                                      542   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_COUNTRY                                      546   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_COUNTRY_END                                           1985  // max. limit

//SPEECHREC
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_SPEECH_REC_CAL              0   // BYTE (Boolean)
#define EOLLIB_OFFSET_CT_COEF_1_US_ENGILSH                                   2   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_US_ENGILSH                                   4   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_US_ENGILSH                                   6   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_US_ENGILSH                                   8   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_US_ENGILSH                                   10   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_US_ENGILSH                                   12   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_US_ENGILSH                                   14   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_US_ENGILSH                                   16   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_US_ENGILSH                                   18   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_US_ENGILSH                                  20   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_US_ENGILSH                                  22   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_US_ENGILSH                                  24   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_US_ENGILSH                                  26   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_US_ENGILSH                                  28   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_US_ENGILSH                                  30   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_US_ENGILSH                                  32   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_US_ENGILSH                                  34   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_US_ENGILSH                                  36   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_US_ENGILSH                                  38   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_US_ENGILSH                                  40   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_US_ENGILSH                                  42   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_US_ENGILSH                                  44   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_US_ENGILSH                                  46   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_US_ENGILSH                                  48   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_US_ENGILSH                                  50   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_US_ENGILSH                                  52   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_US_ENGILSH                                  54   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_US_ENGILSH                                  56   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_US_ENGILSH                                  58   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_US_ENGILSH                                  60   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_US_ENGILSH                                  62   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_US_ENGILSH                                  64   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_US_ENGILSH                                  66   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_US_ENGILSH                                  68   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_US_ENGILSH                                  70   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_US_ENGILSH                                  72   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_US_ENGILSH                                  74   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_US_ENGILSH                                  76   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_US_ENGILSH                                  78   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_US_ENGILSH                                  80   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_US_ENGILSH                                  82   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_US_ENGILSH                                  84   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_US_ENGILSH                                  86   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_US_ENGILSH                                  88   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_US_ENGILSH                                  90   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_US_ENGILSH                                  92   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_US_ENGILSH                                  94   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_US_ENGILSH                                  96   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_US_ENGILSH                                  98   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_US_ENGILSH                                  100   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_US_ENGILSH                                  102   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_US_ENGILSH                                  104   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_US_ENGILSH                                  106   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_US_ENGILSH                                  108   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_US_ENGILSH                                  110   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_US_ENGILSH                                  112   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_US_ENGILSH                                  114   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_US_ENGILSH                                  116   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_US_ENGILSH                                  118   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_US_ENGILSH                                  120   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_US_ENGILSH                                  122   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_62_US_ENGILSH                                  124   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_63_US_ENGILSH                                  126   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_64_US_ENGILSH                                  128   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_65_US_ENGILSH                                  130   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_66_US_ENGILSH                                  132   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_67_US_ENGILSH                                  134   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_68_US_ENGILSH                                  136   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_69_US_ENGILSH                                  138   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_70_US_ENGILSH                                  140   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_71_US_ENGILSH                                  142   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_72_US_ENGILSH                                  144   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_73_US_ENGILSH                                  146   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_74_US_ENGILSH                                  148   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_75_US_ENGILSH                                  150   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_76_US_ENGILSH                                  152   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_77_US_ENGILSH                                  154   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_78_US_ENGILSH                                  156   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_79_US_ENGILSH                                  158   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_80_US_ENGILSH                                  160   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_81_US_ENGILSH                                  162   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_82_US_ENGILSH                                  164   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_83_US_ENGILSH                                  166   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_84_US_ENGILSH                                  168   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_85_US_ENGILSH                                  170   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_86_US_ENGILSH                                  172   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_87_US_ENGILSH                                  174   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_88_US_ENGILSH                                  176   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_89_US_ENGILSH                                  178   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_90_US_ENGILSH                                  180   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_91_US_ENGILSH                                  182   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_92_US_ENGILSH                                  184   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_93_US_ENGILSH                                  186   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_94_US_ENGILSH                                  188   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_95_US_ENGILSH                                  190   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_96_US_ENGILSH                                  192   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_97_US_ENGILSH                                  194   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_98_US_ENGILSH                                  196   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_99_US_ENGILSH                                  198   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_100_US_ENGILSH                                 200   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_101_US_ENGILSH                                 202   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_102_US_ENGILSH                                 204   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_103_US_ENGILSH                                 206   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_104_US_ENGILSH                                 208   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_105_US_ENGILSH                                 210   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_106_US_ENGILSH                                 212   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_107_US_ENGILSH                                 214   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_108_US_ENGILSH                                 216   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_109_US_ENGILSH                                 218   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_110_US_ENGILSH                                 220   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_111_US_ENGILSH                                 222   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_112_US_ENGILSH                                 224   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_113_US_ENGILSH                                 226   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_114_US_ENGILSH                                 228   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_115_US_ENGILSH                                 230   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_116_US_ENGILSH                                 232   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_117_US_ENGILSH                                 234   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_118_US_ENGILSH                                 236   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_119_US_ENGILSH                                 238   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_120_US_ENGILSH                                 240   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_121_US_ENGILSH                                 242   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_122_US_ENGILSH                                 244   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_123_US_ENGILSH                                 246   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_124_US_ENGILSH                                 248   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_125_US_ENGILSH                                 250   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_126_US_ENGILSH                                 252   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_127_US_ENGILSH                                 254   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_128_US_ENGILSH                                 256   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_129_US_ENGILSH                                 258   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_130_US_ENGILSH                                 260   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_131_US_ENGILSH                                 262   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_132_US_ENGILSH                                 264   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_133_US_ENGILSH                                 266   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_134_US_ENGILSH                                 268   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_135_US_ENGILSH                                 270   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_136_US_ENGILSH                                 272   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_137_US_ENGILSH                                 274   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_138_US_ENGILSH                                 276   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_139_US_ENGILSH                                 278   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_140_US_ENGILSH                                 280   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_141_US_ENGILSH                                 282   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_142_US_ENGILSH                                 284   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_143_US_ENGILSH                                 286   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_144_US_ENGILSH                                 288   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_145_US_ENGILSH                                 290   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_146_US_ENGILSH                                 292   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_147_US_ENGILSH                                 294   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_148_US_ENGILSH                                 296   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_149_US_ENGILSH                                 298   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_150_US_ENGILSH                                 300   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_UK_ENGLISH                                   302   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_UK_ENGLISH                                   304   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_UK_ENGLISH                                   306   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_UK_ENGLISH                                   308   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_UK_ENGLISH                                   310   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_UK_ENGLISH                                   312   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_UK_ENGLISH                                   314   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_UK_ENGLISH                                   316   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_UK_ENGLISH                                   318   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_UK_ENGLISH                                  320   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_UK_ENGLISH                                  322   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_UK_ENGLISH                                  324   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_UK_ENGLISH                                  326   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_UK_ENGLISH                                  328   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_UK_ENGLISH                                  330   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_UK_ENGLISH                                  332   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_UK_ENGLISH                                  334   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_UK_ENGLISH                                  336   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_UK_ENGLISH                                  338   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_UK_ENGLISH                                  340   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_UK_ENGLISH                                  342   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_UK_ENGLISH                                  344   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_UK_ENGLISH                                  346   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_UK_ENGLISH                                  348   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_UK_ENGLISH                                  350   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_UK_ENGLISH                                  352   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_UK_ENGLISH                                  354   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_UK_ENGLISH                                  356   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_UK_ENGLISH                                  358   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_UK_ENGLISH                                  360   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_UK_ENGLISH                                  362   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_UK_ENGLISH                                  364   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_UK_ENGLISH                                  366   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_UK_ENGLISH                                  368   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_UK_ENGLISH                                  370   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_UK_ENGLISH                                  372   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_UK_ENGLISH                                  374   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_UK_ENGLISH                                  376   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_UK_ENGLISH                                  378   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_UK_ENGLISH                                  380   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_UK_ENGLISH                                  382   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_UK_ENGLISH                                  384   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_UK_ENGLISH                                  386   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_UK_ENGLISH                                  388   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_UK_ENGLISH                                  390   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_UK_ENGLISH                                  392   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_UK_ENGLISH                                  394   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_UK_ENGLISH                                  396   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_UK_ENGLISH                                  398   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_UK_ENGLISH                                  400   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_UK_ENGLISH                                  402   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_UK_ENGLISH                                  404   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_UK_ENGLISH                                  406   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_UK_ENGLISH                                  408   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_UK_ENGLISH                                  410   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_UK_ENGLISH                                  412   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_UK_ENGLISH                                  414   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_UK_ENGLISH                                  416   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_UK_ENGLISH                                  418   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_UK_ENGLISH                                  420   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_UK_ENGLISH                                  422   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_UK_ENGLISH                                  424   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_UK_ENGLISH                                  426   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_UK_ENGLISH                                  428   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_UK_ENGLISH                                  430   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_UK_ENGLISH                                  432   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_UK_ENGLISH                                  434   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_UK_ENGLISH                                  436   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_UK_ENGLISH                                  438   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_UK_ENGLISH                                  440   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_UK_ENGLISH                                  442   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_UK_ENGLISH                                  444   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_UK_ENGLISH                                  446   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_UK_ENGLISH                                  448   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_UK_ENGLISH                                  450   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_UK_ENGLISH                                  452   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_UK_ENGLISH                                  454   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_UK_ENGLISH                                  456   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_UK_ENGLISH                                  458   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_UK_ENGLISH                                  460   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_UK_ENGLISH                                  462   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_UK_ENGLISH                                  464   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_UK_ENGLISH                                  466   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_UK_ENGLISH                                  468   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_UK_ENGLISH                                  470   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_UK_ENGLISH                                  472   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_UK_ENGLISH                                  474   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_UK_ENGLISH                                  476   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_UK_ENGLISH                                  478   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_UK_ENGLISH                                  480   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_AUS_ENGLISH                                  482   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_AUS_ENGLISH                                  484   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_AUS_ENGLISH                                  486   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_AUS_ENGLISH                                  488   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_AUS_ENGLISH                                  490   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_AUS_ENGLISH                                  492   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_AUS_ENGLISH                                  494   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_AUS_ENGLISH                                  496   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_AUS_ENGLISH                                  498   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_AUS_ENGLISH                                 500   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_AUS_ENGLISH                                 502   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_AUS_ENGLISH                                 504   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_AUS_ENGLISH                                 506   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_AUS_ENGLISH                                 508   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_AUS_ENGLISH                                 510   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_AUS_ENGLISH                                 512   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_AUS_ENGLISH                                 514   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_AUS_ENGLISH                                 516   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_AUS_ENGLISH                                 518   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_AUS_ENGLISH                                 520   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_AUS_ENGLISH                                 522   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_AUS_ENGLISH                                 524   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_AUS_ENGLISH                                 526   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_AUS_ENGLISH                                 528   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_AUS_ENGLISH                                 530   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_AUS_ENGLISH                                 532   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_AUS_ENGLISH                                 534   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_AUS_ENGLISH                                 536   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_AUS_ENGLISH                                 538   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_AUS_ENGLISH                                 540   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_AUS_ENGLISH                                 542   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_AUS_ENGLISH                                 544   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_AUS_ENGLISH                                 546   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_AUS_ENGLISH                                 548   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_AUS_ENGLISH                                 550   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_AUS_ENGLISH                                 552   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_AUS_ENGLISH                                 554   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_AUS_ENGLISH                                 556   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_AUS_ENGLISH                                 558   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_AUS_ENGLISH                                 560   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_AUS_ENGLISH                                 562   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_AUS_ENGLISH                                 564   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_AUS_ENGLISH                                 566   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_AUS_ENGLISH                                 568   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_AUS_ENGLISH                                 570   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_AUS_ENGLISH                                 572   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_AUS_ENGLISH                                 574   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_AUS_ENGLISH                                 576   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_AUS_ENGLISH                                 578   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_AUS_ENGLISH                                 580   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_AUS_ENGLISH                                 582   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_AUS_ENGLISH                                 584   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_AUS_ENGLISH                                 586   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_AUS_ENGLISH                                 588   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_AUS_ENGLISH                                 590   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_AUS_ENGLISH                                 592   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_AUS_ENGLISH                                 594   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_AUS_ENGLISH                                 596   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_AUS_ENGLISH                                 598   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_AUS_ENGLISH                                 600   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_AUS_ENGLISH                                 602   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_AUS_ENGLISH                                 604   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_AUS_ENGLISH                                 606   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_AUS_ENGLISH                                 608   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_AUS_ENGLISH                                 610   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_AUS_ENGLISH                                 612   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_AUS_ENGLISH                                 614   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_AUS_ENGLISH                                 616   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_AUS_ENGLISH                                 618   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_AUS_ENGLISH                                 620   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_AUS_ENGLISH                                 622   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_AUS_ENGLISH                                 624   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_AUS_ENGLISH                                 626   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_AUS_ENGLISH                                 628   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_AUS_ENGLISH                                 630   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_AUS_ENGLISH                                 632   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_AUS_ENGLISH                                 634   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_AUS_ENGLISH                                 636   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_AUS_ENGLISH                                 638   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_AUS_ENGLISH                                 640   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_AUS_ENGLISH                                 642   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_AUS_ENGLISH                                 644   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_AUS_ENGLISH                                 646   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_AUS_ENGLISH                                 648   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_AUS_ENGLISH                                 650   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_AUS_ENGLISH                                 652   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_AUS_ENGLISH                                 654   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_AUS_ENGLISH                                 656   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_AUS_ENGLISH                                 658   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_AUS_ENGLISH                                 660   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_SAFRICA_ENGLISH                              662   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_SAFRICA_ENGLISH                              664   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_SAFRICA_ENGLISH                              666   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_SAFRICA_ENGLISH                              668   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_SAFRICA_ENGLISH                              670   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_SAFRICA_ENGLISH                              672   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_SAFRICA_ENGLISH                              674   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_SAFRICA_ENGLISH                              676   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_SAFRICA_ENGLISH                              678   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_SAFRICA_ENGLISH                             680   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_SAFRICA_ENGLISH                             682   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_SAFRICA_ENGLISH                             684   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_SAFRICA_ENGLISH                             686   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_SAFRICA_ENGLISH                             688   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_SAFRICA_ENGLISH                             690   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_SAFRICA_ENGLISH                             692   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_SAFRICA_ENGLISH                             694   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_SAFRICA_ENGLISH                             696   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_SAFRICA_ENGLISH                             698   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_SAFRICA_ENGLISH                             700   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_SAFRICA_ENGLISH                             702   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_SAFRICA_ENGLISH                             704   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_SAFRICA_ENGLISH                             706   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_SAFRICA_ENGLISH                             708   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_SAFRICA_ENGLISH                             710   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_SAFRICA_ENGLISH                             712   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_SAFRICA_ENGLISH                             714   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_SAFRICA_ENGLISH                             716   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_SAFRICA_ENGLISH                             718   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_SAFRICA_ENGLISH                             720   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_SAFRICA_ENGLISH                             722   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_SAFRICA_ENGLISH                             724   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_SAFRICA_ENGLISH                             726   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_SAFRICA_ENGLISH                             728   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_SAFRICA_ENGLISH                             730   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_SAFRICA_ENGLISH                             732   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_SAFRICA_ENGLISH                             734   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_SAFRICA_ENGLISH                             736   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_SAFRICA_ENGLISH                             738   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_SAFRICA_ENGLISH                             740   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_SAFRICA_ENGLISH                             742   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_SAFRICA_ENGLISH                             744   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_SAFRICA_ENGLISH                             746   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_SAFRICA_ENGLISH                             748   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_SAFRICA_ENGLISH                             750   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_SAFRICA_ENGLISH                             752   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_SAFRICA_ENGLISH                             754   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_SAFRICA_ENGLISH                             756   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_SAFRICA_ENGLISH                             758   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_SAFRICA_ENGLISH                             760   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_SAFRICA_ENGLISH                             762   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_SAFRICA_ENGLISH                             764   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_SAFRICA_ENGLISH                             766   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_SAFRICA_ENGLISH                             768   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_SAFRICA_ENGLISH                             770   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_SAFRICA_ENGLISH                             772   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_SAFRICA_ENGLISH                             774   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_SAFRICA_ENGLISH                             776   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_SAFRICA_ENGLISH                             778   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_SAFRICA_ENGLISH                             780   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_SAFRICA_ENGLISH                             782   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_SAFRICA_ENGLISH                             784   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_SAFRICA_ENGLISH                             786   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_SAFRICA_ENGLISH                             788   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_SAFRICA_ENGLISH                             790   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_SAFRICA_ENGLISH                             792   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_SAFRICA_ENGLISH                             794   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_SAFRICA_ENGLISH                             796   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_SAFRICA_ENGLISH                             798   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_SAFRICA_ENGLISH                             800   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_SAFRICA_ENGLISH                             802   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_SAFRICA_ENGLISH                             804   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_SAFRICA_ENGLISH                             806   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_SAFRICA_ENGLISH                             808   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_SAFRICA_ENGLISH                             810   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_SAFRICA_ENGLISH                             812   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_SAFRICA_ENGLISH                             814   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_SAFRICA_ENGLISH                             816   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_SAFRICA_ENGLISH                             818   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_SAFRICA_ENGLISH                             820   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_SAFRICA_ENGLISH                             822   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_SAFRICA_ENGLISH                             824   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_SAFRICA_ENGLISH                             826   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_SAFRICA_ENGLISH                             828   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_SAFRICA_ENGLISH                             830   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_SAFRICA_ENGLISH                             832   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_SAFRICA_ENGLISH                             834   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_SAFRICA_ENGLISH                             836   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_SAFRICA_ENGLISH                             838   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_SAFRICA_ENGLISH                             840   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_FRENCH                                       842   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_FRENCH                                       844   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_FRENCH                                       846   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_FRENCH                                       848   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_FRENCH                                       850   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_FRENCH                                       852   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_FRENCH                                       854   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_FRENCH                                       856   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_FRENCH                                       858   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_FRENCH                                      860   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_FRENCH                                      862   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_FRENCH                                      864   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_FRENCH                                      866   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_FRENCH                                      868   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_FRENCH                                      870   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_FRENCH                                      872   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_FRENCH                                      874   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_FRENCH                                      876   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_FRENCH                                      878   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_FRENCH                                      880   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_FRENCH                                      882   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_FRENCH                                      884   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_FRENCH                                      886   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_FRENCH                                      888   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_FRENCH                                      890   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_FRENCH                                      892   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_FRENCH                                      894   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_FRENCH                                      896   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_FRENCH                                      898   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_FRENCH                                      900   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_FRENCH                                      902   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_FRENCH                                      904   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_FRENCH                                      906   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_FRENCH                                      908   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_FRENCH                                      910   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_FRENCH                                      912   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_FRENCH                                      914   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_FRENCH                                      916   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_FRENCH                                      918   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_FRENCH                                      920   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_FRENCH                                      922   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_FRENCH                                      924   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_FRENCH                                      926   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_FRENCH                                      928   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_FRENCH                                      930   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_FRENCH                                      932   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_FRENCH                                      934   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_FRENCH                                      936   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_FRENCH                                      938   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_FRENCH                                      940   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_FRENCH                                      942   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_FRENCH                                      944   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_FRENCH                                      946   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_FRENCH                                      948   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_FRENCH                                      950   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_FRENCH                                      952   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_FRENCH                                      954   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_FRENCH                                      956   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_FRENCH                                      958   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_FRENCH                                      960   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_FRENCH                                      962   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_FRENCH                                      964   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_FRENCH                                      966   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_FRENCH                                      968   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_FRENCH                                      970   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_FRENCH                                      972   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_FRENCH                                      974   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_FRENCH                                      976   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_FRENCH                                      978   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_FRENCH                                      980   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_FRENCH                                      982   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_FRENCH                                      984   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_FRENCH                                      986   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_FRENCH                                      988   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_FRENCH                                      990   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_FRENCH                                      992   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_FRENCH                                      994   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_FRENCH                                      996   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_FRENCH                                      998   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_FRENCH                                      1000   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_FRENCH                                      1002   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_FRENCH                                      1004   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_FRENCH                                      1006   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_FRENCH                                      1008   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_FRENCH                                      1010   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_FRENCH                                      1012   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_FRENCH                                      1014   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_FRENCH                                      1016   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_FRENCH                                      1018   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_FRENCH                                      1020   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_FRENCH_CANADIAN                              1022   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_FRENCH_CANADIAN                              1024   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_FRENCH_CANADIAN                              1026   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_FRENCH_CANADIAN                              1028   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_FRENCH_CANADIAN                              1030   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_FRENCH_CANADIAN                              1032   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_FRENCH_CANADIAN                              1034   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_FRENCH_CANADIAN                              1036   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_FRENCH_CANADIAN                              1038   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_FRENCH_CANADIAN                             1040   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_FRENCH_CANADIAN                             1042   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_FRENCH_CANADIAN                             1044   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_FRENCH_CANADIAN                             1046   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_FRENCH_CANADIAN                             1048   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_FRENCH_CANADIAN                             1050   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_FRENCH_CANADIAN                             1052   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_FRENCH_CANADIAN                             1054   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_FRENCH_CANADIAN                             1056   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_FRENCH_CANADIAN                             1058   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_FRENCH_CANADIAN                             1060   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_FRENCH_CANADIAN                             1062   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_FRENCH_CANADIAN                             1064   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_FRENCH_CANADIAN                             1066   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_FRENCH_CANADIAN                             1068   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_FRENCH_CANADIAN                             1070   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_FRENCH_CANADIAN                             1072   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_FRENCH_CANADIAN                             1074   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_FRENCH_CANADIAN                             1076   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_FRENCH_CANADIAN                             1078   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_FRENCH_CANADIAN                             1080   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_FRENCH_CANADIAN                             1082   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_FRENCH_CANADIAN                             1084   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_FRENCH_CANADIAN                             1086   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_FRENCH_CANADIAN                             1088   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_FRENCH_CANADIAN                             1090   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_FRENCH_CANADIAN                             1092   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_FRENCH_CANADIAN                             1094   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_FRENCH_CANADIAN                             1096   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_FRENCH_CANADIAN                             1098   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_FRENCH_CANADIAN                             1100   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_FRENCH_CANADIAN                             1102   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_FRENCH_CANADIAN                             1104   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_FRENCH_CANADIAN                             1106   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_FRENCH_CANADIAN                             1108   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_FRENCH_CANADIAN                             1110   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_FRENCH_CANADIAN                             1112   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_FRENCH_CANADIAN                             1114   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_FRENCH_CANADIAN                             1116   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_FRENCH_CANADIAN                             1118   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_FRENCH_CANADIAN                             1120   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_FRENCH_CANADIAN                             1122   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_FRENCH_CANADIAN                             1124   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_FRENCH_CANADIAN                             1126   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_FRENCH_CANADIAN                             1128   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_FRENCH_CANADIAN                             1130   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_FRENCH_CANADIAN                             1132   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_FRENCH_CANADIAN                             1134   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_FRENCH_CANADIAN                             1136   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_FRENCH_CANADIAN                             1138   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_FRENCH_CANADIAN                             1140   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_FRENCH_CANADIAN                             1142   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_FRENCH_CANADIAN                             1144   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_FRENCH_CANADIAN                             1146   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_FRENCH_CANADIAN                             1148   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_FRENCH_CANADIAN                             1150   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_FRENCH_CANADIAN                             1152   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_FRENCH_CANADIAN                             1154   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_FRENCH_CANADIAN                             1156   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_FRENCH_CANADIAN                             1158   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_FRENCH_CANADIAN                             1160   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_FRENCH_CANADIAN                             1162   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_FRENCH_CANADIAN                             1164   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_FRENCH_CANADIAN                             1166   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_FRENCH_CANADIAN                             1168   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_FRENCH_CANADIAN                             1170   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_FRENCH_CANADIAN                             1172   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_FRENCH_CANADIAN                             1174   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_FRENCH_CANADIAN                             1176   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_FRENCH_CANADIAN                             1178   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_FRENCH_CANADIAN                             1180   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_FRENCH_CANADIAN                             1182   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_FRENCH_CANADIAN                             1184   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_FRENCH_CANADIAN                             1186   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_FRENCH_CANADIAN                             1188   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_FRENCH_CANADIAN                             1190   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_FRENCH_CANADIAN                             1192   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_FRENCH_CANADIAN                             1194   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_FRENCH_CANADIAN                             1196   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_FRENCH_CANADIAN                             1198   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_FRENCH_CANADIAN                             1200   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_SPANISH                                      1202   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_SPANISH                                      1204   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_SPANISH                                      1206   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_SPANISH                                      1208   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_SPANISH                                      1210   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_SPANISH                                      1212   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_SPANISH                                      1214   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_SPANISH                                      1216   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_SPANISH                                      1218   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_SPANISH                                     1220   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_SPANISH                                     1222   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_SPANISH                                     1224   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_SPANISH                                     1226   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_SPANISH                                     1228   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_SPANISH                                     1230   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_SPANISH                                     1232   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_SPANISH                                     1234   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_SPANISH                                     1236   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_SPANISH                                     1238   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_SPANISH                                     1240   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_SPANISH                                     1242   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_SPANISH                                     1244   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_SPANISH                                     1246   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_SPANISH                                     1248   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_SPANISH                                     1250   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_SPANISH                                     1252   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_SPANISH                                     1254   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_SPANISH                                     1256   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_SPANISH                                     1258   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_SPANISH                                     1260   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_SPANISH                                     1262   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_SPANISH                                     1264   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_SPANISH                                     1266   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_SPANISH                                     1268   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_SPANISH                                     1270   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_SPANISH                                     1272   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_SPANISH                                     1274   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_SPANISH                                     1276   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_SPANISH                                     1278   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_SPANISH                                     1280   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_SPANISH                                     1282   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_SPANISH                                     1284   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_SPANISH                                     1286   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_SPANISH                                     1288   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_SPANISH                                     1290   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_SPANISH                                     1292   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_SPANISH                                     1294   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_SPANISH                                     1296   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_SPANISH                                     1298   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_SPANISH                                     1300   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_SPANISH                                     1302   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_SPANISH                                     1304   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_SPANISH                                     1306   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_SPANISH                                     1308   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_SPANISH                                     1310   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_SPANISH                                     1312   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_SPANISH                                     1314   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_SPANISH                                     1316   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_SPANISH                                     1318   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_SPANISH                                     1320   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_SPANISH                                     1322   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_SPANISH                                     1324   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_SPANISH                                     1326   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_SPANISH                                     1328   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_SPANISH                                     1330   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_SPANISH                                     1332   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_SPANISH                                     1334   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_SPANISH                                     1336   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_SPANISH                                     1338   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_SPANISH                                     1340   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_SPANISH                                     1342   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_SPANISH                                     1344   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_SPANISH                                     1346   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_SPANISH                                     1348   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_SPANISH                                     1350   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_SPANISH                                     1352   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_SPANISH                                     1354   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_SPANISH                                     1356   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_SPANISH                                     1358   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_SPANISH                                     1360   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_SPANISH                                     1362   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_SPANISH                                     1364   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_SPANISH                                     1366   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_SPANISH                                     1368   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_SPANISH                                     1370   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_SPANISH                                     1372   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_SPANISH                                     1374   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_SPANISH                                     1376   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_SPANISH                                     1378   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_SPANISH                                     1380   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_SPANISH_LATIN                                1382   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_SPANISH_LATIN                                1384   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_SPANISH_LATIN                                1386   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_SPANISH_LATIN                                1388   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_SPANISH_LATIN                                1390   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_SPANISH_LATIN                                1392   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_SPANISH_LATIN                                1394   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_SPANISH_LATIN                                1396   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_SPANISH_LATIN                                1398   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_SPANISH_LATIN                               1400   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_SPANISH_LATIN                               1402   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_SPANISH_LATIN                               1404   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_SPANISH_LATIN                               1406   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_SPANISH_LATIN                               1408   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_SPANISH_LATIN                               1410   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_SPANISH_LATIN                               1412   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_SPANISH_LATIN                               1414   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_SPANISH_LATIN                               1416   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_SPANISH_LATIN                               1418   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_SPANISH_LATIN                               1420   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_SPANISH_LATIN                               1422   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_SPANISH_LATIN                               1424   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_SPANISH_LATIN                               1426   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_SPANISH_LATIN                               1428   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_SPANISH_LATIN                               1430   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_SPANISH_LATIN                               1432   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_SPANISH_LATIN                               1434   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_SPANISH_LATIN                               1436   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_SPANISH_LATIN                               1438   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_SPANISH_LATIN                               1440   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_SPANISH_LATIN                               1442   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_SPANISH_LATIN                               1444   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_SPANISH_LATIN                               1446   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_SPANISH_LATIN                               1448   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_SPANISH_LATIN                               1450   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_SPANISH_LATIN                               1452   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_SPANISH_LATIN                               1454   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_SPANISH_LATIN                               1456   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_SPANISH_LATIN                               1458   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_SPANISH_LATIN                               1460   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_SPANISH_LATIN                               1462   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_SPANISH_LATIN                               1464   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_SPANISH_LATIN                               1466   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_SPANISH_LATIN                               1468   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_SPANISH_LATIN                               1470   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_SPANISH_LATIN                               1472   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_SPANISH_LATIN                               1474   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_SPANISH_LATIN                               1476   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_SPANISH_LATIN                               1478   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_SPANISH_LATIN                               1480   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_SPANISH_LATIN                               1482   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_SPANISH_LATIN                               1484   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_SPANISH_LATIN                               1486   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_SPANISH_LATIN                               1488   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_SPANISH_LATIN                               1490   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_SPANISH_LATIN                               1492   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_SPANISH_LATIN                               1494   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_SPANISH_LATIN                               1496   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_SPANISH_LATIN                               1498   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_SPANISH_LATIN                               1500   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_SPANISH_LATIN                               1502   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_SPANISH_LATIN                               1504   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_SPANISH_LATIN                               1506   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_SPANISH_LATIN                               1508   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_SPANISH_LATIN                               1510   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_SPANISH_LATIN                               1512   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_SPANISH_LATIN                               1514   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_SPANISH_LATIN                               1516   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_SPANISH_LATIN                               1518   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_SPANISH_LATIN                               1520   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_SPANISH_LATIN                               1522   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_SPANISH_LATIN                               1524   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_SPANISH_LATIN                               1526   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_SPANISH_LATIN                               1528   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_SPANISH_LATIN                               1530   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_SPANISH_LATIN                               1532   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_SPANISH_LATIN                               1534   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_SPANISH_LATIN                               1536   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_SPANISH_LATIN                               1538   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_SPANISH_LATIN                               1540   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_SPANISH_LATIN                               1542   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_SPANISH_LATIN                               1544   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_SPANISH_LATIN                               1546   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_SPANISH_LATIN                               1548   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_SPANISH_LATIN                               1550   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_SPANISH_LATIN                               1552   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_SPANISH_LATIN                               1554   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_SPANISH_LATIN                               1556   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_SPANISH_LATIN                               1558   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_SPANISH_LATIN                               1560   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_PORTUGUESE                                   1562   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_PORTUGUESE                                   1564   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_PORTUGUESE                                   1566   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_PORTUGUESE                                   1568   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_PORTUGUESE                                   1570   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_PORTUGUESE                                   1572   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_PORTUGUESE                                   1574   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_PORTUGUESE                                   1576   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_PORTUGUESE                                   1578   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_PORTUGUESE                                  1580   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_PORTUGUESE                                  1582   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_PORTUGUESE                                  1584   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_PORTUGUESE                                  1586   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_PORTUGUESE                                  1588   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_PORTUGUESE                                  1590   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_PORTUGUESE                                  1592   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_PORTUGUESE                                  1594   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_PORTUGUESE                                  1596   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_PORTUGUESE                                  1598   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_PORTUGUESE                                  1600   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_PORTUGUESE                                  1602   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_PORTUGUESE                                  1604   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_PORTUGUESE                                  1606   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_PORTUGUESE                                  1608   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_PORTUGUESE                                  1610   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_PORTUGUESE                                  1612   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_PORTUGUESE                                  1614   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_PORTUGUESE                                  1616   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_PORTUGUESE                                  1618   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_PORTUGUESE                                  1620   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_PORTUGUESE                                  1622   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_PORTUGUESE                                  1624   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_PORTUGUESE                                  1626   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_PORTUGUESE                                  1628   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_PORTUGUESE                                  1630   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_PORTUGUESE                                  1632   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_PORTUGUESE                                  1634   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_PORTUGUESE                                  1636   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_PORTUGUESE                                  1638   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_PORTUGUESE                                  1640   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_PORTUGUESE                                  1642   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_PORTUGUESE                                  1644   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_PORTUGUESE                                  1646   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_PORTUGUESE                                  1648   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_PORTUGUESE                                  1650   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_PORTUGUESE                                  1652   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_PORTUGUESE                                  1654   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_PORTUGUESE                                  1656   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_PORTUGUESE                                  1658   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_PORTUGUESE                                  1660   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_PORTUGUESE                                  1662   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_PORTUGUESE                                  1664   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_PORTUGUESE                                  1666   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_PORTUGUESE                                  1668   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_PORTUGUESE                                  1670   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_PORTUGUESE                                  1672   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_PORTUGUESE                                  1674   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_PORTUGUESE                                  1676   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_PORTUGUESE                                  1678   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_PORTUGUESE                                  1680   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_PORTUGUESE                                  1682   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_PORTUGUESE                                  1684   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_PORTUGUESE                                  1686   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_PORTUGUESE                                  1688   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_PORTUGUESE                                  1690   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_PORTUGUESE                                  1692   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_PORTUGUESE                                  1694   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_PORTUGUESE                                  1696   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_PORTUGUESE                                  1698   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_PORTUGUESE                                  1700   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_PORTUGUESE                                  1702   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_PORTUGUESE                                  1704   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_PORTUGUESE                                  1706   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_PORTUGUESE                                  1708   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_PORTUGUESE                                  1710   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_PORTUGUESE                                  1712   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_PORTUGUESE                                  1714   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_PORTUGUESE                                  1716   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_PORTUGUESE                                  1718   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_PORTUGUESE                                  1720   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_PORTUGUESE                                  1722   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_PORTUGUESE                                  1724   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_PORTUGUESE                                  1726   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_PORTUGUESE                                  1728   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_PORTUGUESE                                  1730   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_PORTUGUESE                                  1732   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_PORTUGUESE                                  1734   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_PORTUGUESE                                  1736   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_PORTUGUESE                                  1738   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_PORTUGUESE                                  1740   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_BRAZILIAN_PORTUGUESE                         1742   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_BRAZILIAN_PORTUGUESE                         1744   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_BRAZILIAN_PORTUGUESE                         1746   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_BRAZILIAN_PORTUGUESE                         1748   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_BRAZILIAN_PORTUGUESE                         1750   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_BRAZILIAN_PORTUGUESE                         1752   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_BRAZILIAN_PORTUGUESE                         1754   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_BRAZILIAN_PORTUGUESE                         1756   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_BRAZILIAN_PORTUGUESE                         1758   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_BRAZILIAN_PORTUGUESE                        1760   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_BRAZILIAN_PORTUGUESE                        1762   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_BRAZILIAN_PORTUGUESE                        1764   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_BRAZILIAN_PORTUGUESE                        1766   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_BRAZILIAN_PORTUGUESE                        1768   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_BRAZILIAN_PORTUGUESE                        1770   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_BRAZILIAN_PORTUGUESE                        1772   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_BRAZILIAN_PORTUGUESE                        1774   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_BRAZILIAN_PORTUGUESE                        1776   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_BRAZILIAN_PORTUGUESE                        1778   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_BRAZILIAN_PORTUGUESE                        1780   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_BRAZILIAN_PORTUGUESE                        1782   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_BRAZILIAN_PORTUGUESE                        1784   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_BRAZILIAN_PORTUGUESE                        1786   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_BRAZILIAN_PORTUGUESE                        1788   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_BRAZILIAN_PORTUGUESE                        1790   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_BRAZILIAN_PORTUGUESE                        1792   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_BRAZILIAN_PORTUGUESE                        1794   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_BRAZILIAN_PORTUGUESE                        1796   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_BRAZILIAN_PORTUGUESE                        1798   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_BRAZILIAN_PORTUGUESE                        1800   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_BRAZILLIAN_PORTUGUESE                       1802   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_BRAZILLIAN_PORTUGUESE                       1804   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_BRAZILLIAN_PORTUGUESE                       1806   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_BRAZILLIAN_PORTUGUESE                       1808   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_BRAZILLIAN_PORTUGUESE                       1810   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_BRAZILLIAN_PORTUGUESE                       1812   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_BRAZILLIAN_PORTUGUESE                       1814   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_BRAZILLIAN_PORTUGUESE                       1816   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_BRAZILLIAN_PORTUGUESE                       1818   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_BRAZILLIAN_PORTUGUESE                       1820   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_BRAZILLIAN_PORTUGUESE                       1822   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_BRAZILLIAN_PORTUGUESE                       1824   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_BRAZILLIAN_PORTUGUESE                       1826   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_BRAZILLIAN_PORTUGUESE                       1828   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_BRAZILLIAN_PORTUGUESE                       1830   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_BRAZILLIAN_PORTUGUESE                       1832   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_BRAZILLIAN_PORTUGUESE                       1834   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_BRAZILLIAN_PORTUGUESE                       1836   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_BRAZILLIAN_PORTUGUESE                       1838   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_BRAZILLIAN_PORTUGUESE                       1840   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_BRAZILLIAN_PORTUGUESE                       1842   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_BRAZILLIAN_PORTUGUESE                       1844   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_BRAZILLIAN_PORTUGUESE                       1846   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_BRAZILLIAN_PORTUGUESE                       1848   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_BRAZILLIAN_PORTUGUESE                       1850   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_BRAZILLIAN_PORTUGUESE                       1852   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_BRAZILLIAN_PORTUGUESE                       1854   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_BRAZILLIAN_PORTUGUESE                       1856   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_BRAZILLIAN_PORTUGUESE                       1858   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_BRAZILLIAN_PORTUGUESE                       1860   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_BRAZILLIAN_PORTUGUESE                       1862   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_BRAZILLIAN_PORTUGUESE                       1864   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_BRAZILLIAN_PORTUGUESE                       1866   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_BRAZILLIAN_PORTUGUESE                       1868   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_BRAZILLIAN_PORTUGUESE                       1870   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_BRAZILLIAN_PORTUGUESE                       1872   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_BRAZILLIAN_PORTUGUESE                       1874   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_BRAZILLIAN_PORTUGUESE                       1876   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_BRAZILLIAN_PORTUGUESE                       1878   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_BRAZILLIAN_PORTUGUESE                       1880   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_BRAZILLIAN_PORTUGUESE                       1882   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_BRAZILLIAN_PORTUGUESE                       1884   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_BRAZILLIAN_PORTUGUESE                       1886   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_BRAZILLIAN_PORTUGUESE                       1888   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_BRAZILLIAN_PORTUGUESE                       1890   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_BRAZILLIAN_PORTUGUESE                       1892   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_BRAZILLIAN_PORTUGUESE                       1894   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_BRAZILLIAN_PORTUGUESE                       1896   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_BRAZILLIAN_PORTUGUESE                       1898   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_BRAZILLIAN_PORTUGUESE                       1900   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_BRAZILLIAN_PORTUGUESE                       1902   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_BRAZILLIAN_PORTUGUESE                       1904   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_BRAZILLIAN_PORTUGUESE                       1906   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_BRAZILLIAN_PORTUGUESE                       1908   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_BRAZILLIAN_PORTUGUESE                       1910   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_BRAZILLIAN_PORTUGUESE                       1912   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_BRAZILLIAN_PORTUGUESE                       1914   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_BRAZILLIAN_PORTUGUESE                       1916   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_BRAZILLIAN_PORTUGUESE                       1918   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_BRAZILLIAN_PORTUGUESE                       1920   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_BULGARIAN                                    1922   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_BULGARIAN                                    1924   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_BULGARIAN                                    1926   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_BULGARIAN                                    1928   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_BULGARIAN                                    1930   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_BULGARIAN                                    1932   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_BULGARIAN                                    1934   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_BULGARIAN                                    1936   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_BULGARIAN                                    1938   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_BULGARIAN                                   1940   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_BULGARIAN                                   1942   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_BULGARIAN                                   1944   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_BULGARIAN                                   1946   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_BULGARIAN                                   1948   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_BULGARIAN                                   1950   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_BULGARIAN                                   1952   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_BULGARIAN                                   1954   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_BULGARIAN                                   1956   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_BULGARIAN                                   1958   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_BULGARIAN                                   1960   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_BULGARIAN                                   1962   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_BULGARIAN                                   1964   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_BULGARIAN                                   1966   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_BULGARIAN                                   1968   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_BULGARIAN                                   1970   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_BULGARIAN                                   1972   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_BULGARIAN                                   1974   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_BULGARIAN                                   1976   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_BULGARIAN                                   1978   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_BULGARIAN                                   1980   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_BULGARIAN                                   1982   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_BULGARIAN                                   1984   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_BULGARIAN                                   1986   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_BULGARIAN                                   1988   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_BULGARIAN                                   1990   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_BULGARIAN                                   1992   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_BULGARIAN                                   1994   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_BULGARIAN                                   1996   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_BULGARIAN                                   1998   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_BULGARIAN                                   2000   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_BULGARIAN                                   2002   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_BULGARIAN                                   2004   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_BULGARIAN                                   2006   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_BULGARIAN                                   2008   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_BULGARIAN                                   2010   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_BULGARIAN                                   2012   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_BULGARIAN                                   2014   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_BULGARIAN                                   2016   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_BULGARIAN                                   2018   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_BULGARIAN                                   2020   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_BULGARIAN                                   2022   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_BULGARIAN                                   2024   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_BULGARIAN                                   2026   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_BULGARIAN                                   2028   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_BULGARIAN                                   2030   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_BULGARIAN                                   2032   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_BULGARIAN                                   2034   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_BULGARIAN                                   2036   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_BULGARIAN                                   2038   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_BULGARIAN                                   2040   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_BULGARIAN                                   2042   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_BULGARIAN                                   2044   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_BULGARIAN                                   2046   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_BULGARIAN                                   2048   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_BULGARIAN                                   2050   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_BULGARIAN                                   2052   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_BULGARIAN                                   2054   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_BULGARIAN                                   2056   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_BULGARIAN                                   2058   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_BULGARIAN                                   2060   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_BULGARIAN                                   2062   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_BULGARIAN                                   2064   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_BULGARIAN                                   2066   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_BULGARIAN                                   2068   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_BULGARIAN                                   2070   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_BULGARIAN                                   2072   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_BULGARIAN                                   2074   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_BULGARIAN                                   2076   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_BULGARIAN                                   2078   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_BULGARIAN                                   2080   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_BULGARIAN                                   2082   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_BULGARIAN                                   2084   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_BULGARIAN                                   2086   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_BULGARIAN                                   2088   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_BULGARIAN                                   2090   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_BULGARIAN                                   2092   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_BULGARIAN                                   2094   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_BULGARIAN                                   2096   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_BULGARIAN                                   2098   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_BULGARIAN                                   2100   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_CROATIAN                                     2102   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_CROATIAN                                     2104   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_CROATIAN                                     2106   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_CROATIAN                                     2108   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_CROATIAN                                     2110   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_CROATIAN                                     2112   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_CROATIAN                                     2114   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_CROATIAN                                     2116   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_CROATIAN                                     2118   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_CROATIAN                                    2120   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_CROATIAN                                    2122   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_CROATIAN                                    2124   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_CROATIAN                                    2126   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_CROATIAN                                    2128   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_CROATIAN                                    2130   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_CROATIAN                                    2132   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_CROATIAN                                    2134   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_CROATIAN                                    2136   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_CROATIAN                                    2138   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_CROATIAN                                    2140   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_CROATIAN                                    2142   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_CROATIAN                                    2144   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_CROATIAN                                    2146   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_CROATIAN                                    2148   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_CROATIAN                                    2150   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_CROATIAN                                    2152   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_CROATIAN                                    2154   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_CROATIAN                                    2156   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_CROATIAN                                    2158   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_CROATIAN                                    2160   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_CROATIAN                                    2162   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_CROATIAN                                    2164   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_CROATIAN                                    2166   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_CROATIAN                                    2168   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_CROATIAN                                    2170   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_CROATIAN                                    2172   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_CROATIAN                                    2174   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_CROATIAN                                    2176   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_CROATIAN                                    2178   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_CROATIAN                                    2180   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_CROATIAN                                    2182   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_CROATIAN                                    2184   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_CROATIAN                                    2186   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_CROATIAN                                    2188   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_CROATIAN                                    2190   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_CROATIAN                                    2192   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_CROATIAN                                    2194   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_CROATIAN                                    2196   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_CROATIAN                                    2198   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_CROATIAN                                    2200   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_CROATIAN                                    2202   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_CROATIAN                                    2204   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_CROATIAN                                    2206   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_CROATIAN                                    2208   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_CROATIAN                                    2210   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_CROATIAN                                    2212   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_CROATIAN                                    2214   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_CROATIAN                                    2216   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_CROATIAN                                    2218   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_CROATIAN                                    2220   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_CROATIAN                                    2222   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_CROATIAN                                    2224   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_CROATIAN                                    2226   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_CROATIAN                                    2228   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_CROATIAN                                    2230   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_CROATIAN                                    2232   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_CROATIAN                                    2234   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_CROATIAN                                    2236   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_CROATIAN                                    2238   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_CROATIAN                                    2240   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_CROATIAN                                    2242   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_CROATIAN                                    2244   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_CROATIAN                                    2246   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_CROATIAN                                    2248   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_CROATIAN                                    2250   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_CROATIAN                                    2252   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_CROATIAN                                    2254   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_CROATIAN                                    2256   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_CROATIAN                                    2258   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_CROATIAN                                    2260   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_CROATIAN                                    2262   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_CROATIAN                                    2264   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_CROATIAN                                    2266   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_CROATIAN                                    2268   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_CROATIAN                                    2270   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_CROATIAN                                    2272   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_CROATIAN                                    2274   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_CROATIAN                                    2276   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_CROATIAN                                    2278   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_CROATIAN                                    2280   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_CZECH                                        2282   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_CZECH                                        2284   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_CZECH                                        2286   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_CZECH                                        2288   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_CZECH                                        2290   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_CZECH                                        2292   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_CZECH                                        2294   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_CZECH                                        2296   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_CZECH                                        2298   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_CZECH                                       2300   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_CZECH                                       2302   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_CZECH                                       2304   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_CZECH                                       2306   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_CZECH                                       2308   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_CZECH                                       2310   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_CZECH                                       2312   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_CZECH                                       2314   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_CZECH                                       2316   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_CZECH                                       2318   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_CZECH                                       2320   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_CZECH                                       2322   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_CZECH                                       2324   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_CZECH                                       2326   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_CZECH                                       2328   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_CZECH                                       2330   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_CZECH                                       2332   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_CZECH                                       2334   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_CZECH                                       2336   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_CZECH                                       2338   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_CZECH                                       2340   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_CZECH                                       2342   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_CZECH                                       2344   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_CZECH                                       2346   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_CZECH                                       2348   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_CZECH                                       2350   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_CZECH                                       2352   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_CZECH                                       2354   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_CZECH                                       2356   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_CZECH                                       2358   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_CZECH                                       2360   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_CZECH                                       2362   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_CZECH                                       2364   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_CZECH                                       2366   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_CZECH                                       2368   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_CZECH                                       2370   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_CZECH                                       2372   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_CZECH                                       2374   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_CZECH                                       2376   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_CZECH                                       2378   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_CZECH                                       2380   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_CZECH                                       2382   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_CZECH                                       2384   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_CZECH                                       2386   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_CZECH                                       2388   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_CZECH                                       2390   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_CZECH                                       2392   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_CZECH                                       2394   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_CZECH                                       2396   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_CZECH                                       2398   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_CZECH                                       2400   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_CZECH                                       2402   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_CZECH                                       2404   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_CZECH                                       2406   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_CZECH                                       2408   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_CZECH                                       2410   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_CZECH                                       2412   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_CZECH                                       2414   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_CZECH                                       2416   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_CZECH                                       2418   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_CZECH                                       2420   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_CZECH                                       2422   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_CZECH                                       2424   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_CZECH                                       2426   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_CZECH                                       2428   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_CZECH                                       2430   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_CZECH                                       2432   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_CZECH                                       2434   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_CZECH                                       2436   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_CZECH                                       2438   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_CZECH                                       2440   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_CZECH                                       2442   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_CZECH                                       2444   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_CZECH                                       2446   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_CZECH                                       2448   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_CZECH                                       2450   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_CZECH                                       2452   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_CZECH                                       2454   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_CZECH                                       2456   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_CZECH                                       2458   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_CZECH                                       2460   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_DANISH                                       2462   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_DANISH                                       2464   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_DANISH                                       2466   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_DANISH                                       2468   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_DANISH                                       2470   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_DANISH                                       2472   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_DANISH                                       2474   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_DANISH                                       2476   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_DANISH                                       2478   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_DANISH                                      2480   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_DANISH                                      2482   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_DANISH                                      2484   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_DANISH                                      2486   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_DANISH                                      2488   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_DANISH                                      2490   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_DANISH                                      2492   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_DANISH                                      2494   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_DANISH                                      2496   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_DANISH                                      2498   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_DANISH                                      2500   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_DANISH                                      2502   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_DANISH                                      2504   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_DANISH                                      2506   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_DANISH                                      2508   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_DANISH                                      2510   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_DANISH                                      2512   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_DANISH                                      2514   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_DANISH                                      2516   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_DANISH                                      2518   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_DANISH                                      2520   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_DANISH                                      2522   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_DANISH                                      2524   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_DANISH                                      2526   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_DANISH                                      2528   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_DANISH                                      2530   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_DANISH                                      2532   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_DANISH                                      2534   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_DANISH                                      2536   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_DANISH                                      2538   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_DANISH                                      2540   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_DANISH                                      2542   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_DANISH                                      2544   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_DANISH                                      2546   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_DANISH                                      2548   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_DANISH                                      2550   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_DANISH                                      2552   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_DANISH                                      2554   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_DANISH                                      2556   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_DANISH                                      2558   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_DANISH                                      2560   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_DANISH                                      2562   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_DANISH                                      2564   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_DANISH                                      2566   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_DANISH                                      2568   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_DANISH                                      2570   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_DANISH                                      2572   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_DANISH                                      2574   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_DANISH                                      2576   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_DANISH                                      2578   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_DANISH                                      2580   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_DANISH                                      2582   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_DANISH                                      2584   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_DANISH                                      2586   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_DANISH                                      2588   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_DANISH                                      2590   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_DANISH                                      2592   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_DANISH                                      2594   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_DANISH                                      2596   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_DANISH                                      2598   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_DANISH                                      2600   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_DANISH                                      2602   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_DANISH                                      2604   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_DANISH                                      2606   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_DANISH                                      2608   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_DANISH                                      2610   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_DANISH                                      2612   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_DANISH                                      2614   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_DANISH                                      2616   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_DANISH                                      2618   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_DANISH                                      2620   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_DANISH                                      2622   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_DANISH                                      2624   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_DANISH                                      2626   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_DANISH                                      2628   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_DANISH                                      2630   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_DANISH                                      2632   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_DANISH                                      2634   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_DANISH                                      2636   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_DANISH                                      2638   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_DANISH                                      2640   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_DUTCH                                        2642   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_DUTCH                                        2644   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_DUTCH                                        2646   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_DUTCH                                        2648   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_DUTCH                                        2650   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_DUTCH                                        2652   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_DUTCH                                        2654   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_DUTCH                                        2656   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_DUTCH                                        2658   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_DUTCH                                       2660   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_DUTCH                                       2662   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_DUTCH                                       2664   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_DUTCH                                       2666   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_DUTCH                                       2668   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_DUTCH                                       2670   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_DUTCH                                       2672   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_DUTCH                                       2674   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_DUTCH                                       2676   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_DUTCH                                       2678   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_DUTCH                                       2680   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_DUTCH                                       2682   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_DUTCH                                       2684   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_DUTCH                                       2686   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_DUTCH                                       2688   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_DUTCH                                       2690   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_DUTCH                                       2692   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_DUTCH                                       2694   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_DUTCH                                       2696   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_DUTCH                                       2698   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_DUTCH                                       2700   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_DUTCH                                       2702   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_DUTCH                                       2704   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_DUTCH                                       2706   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_DUTCH                                       2708   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_DUTCH                                       2710   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_DUTCH                                       2712   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_DUTCH                                       2714   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_DUTCH                                       2716   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_DUTCH                                       2718   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_DUTCH                                       2720   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_DUTCH                                       2722   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_DUTCH                                       2724   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_DUTCH                                       2726   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_DUTCH                                       2728   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_DUTCH                                       2730   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_DUTCH                                       2732   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_DUTCH                                       2734   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_DUTCH                                       2736   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_DUTCH                                       2738   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_DUTCH                                       2740   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_DUTCH                                       2742   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_DUTCH                                       2744   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_DUTCH                                       2746   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_DUTCH                                       2748   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_DUTCH                                       2750   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_DUTCH                                       2752   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_DUTCH                                       2754   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_DUTCH                                       2756   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_DUTCH                                       2758   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_DUTCH                                       2760   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_DUTCH                                       2762   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_DUTCH                                       2764   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_DUTCH                                       2766   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_DUTCH                                       2768   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_DUTCH                                       2770   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_DUTCH                                       2772   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_DUTCH                                       2774   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_DUTCH                                       2776   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_DUTCH                                       2778   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_DUTCH                                       2780   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_DUTCH                                       2782   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_DUTCH                                       2784   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_DUTCH                                       2786   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_DUTCH                                       2788   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_DUTCH                                       2790   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_DUTCH                                       2792   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_DUTCH                                       2794   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_DUTCH                                       2796   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_DUTCH                                       2798   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_DUTCH                                       2800   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_DUTCH                                       2802   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_DUTCH                                       2804   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_DUTCH                                       2806   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_DUTCH                                       2808   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_DUTCH                                       2810   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_DUTCH                                       2812   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_DUTCH                                       2814   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_DUTCH                                       2816   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_DUTCH                                       2818   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_DUTCH                                       2820   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_GERMAN                                       2822   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_GERMAN                                       2824   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_GERMAN                                       2826   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_GERMAN                                       2828   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_GERMAN                                       2830   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_GERMAN                                       2832   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_GERMAN                                       2834   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_GERMAN                                       2836   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_GERMAN                                       2838   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_GERMAN                                      2840   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_GERMAN                                      2842   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_GERMAN                                      2844   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_GERMAN                                      2846   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_GERMAN                                      2848   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_GERMAN                                      2850   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_GERMAN                                      2852   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_GERMAN                                      2854   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_GERMAN                                      2856   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_GERMAN                                      2858   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_GERMAN                                      2860   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_GERMAN                                      2862   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_GERMAN                                      2864   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_GERMAN                                      2866   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_GERMAN                                      2868   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_GERMAN                                      2870   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_GERMAN                                      2872   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_GERMAN                                      2874   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_GERMAN                                      2876   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_GERMAN                                      2878   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_GERMAN                                      2880   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_GERMAN                                      2882   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_GERMAN                                      2884   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_GERMAN                                      2886   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_GERMAN                                      2888   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_GERMAN                                      2890   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_GERMAN                                      2892   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_GERMAN                                      2894   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_GERMAN                                      2896   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_GERMAN                                      2898   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_GERMAN                                      2900   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_GERMAN                                      2902   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_GERMAN                                      2904   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_GERMAN                                      2906   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_GERMAN                                      2908   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_GERMAN                                      2910   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_GERMAN                                      2912   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_GERMAN                                      2914   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_GERMAN                                      2916   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_GERMAN                                      2918   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_GERMAN                                      2920   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_GERMAN                                      2922   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_GERMAN                                      2924   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_GERMAN                                      2926   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_GERMAN                                      2928   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_GERMAN                                      2930   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_GERMAN                                      2932   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_GERMAN                                      2934   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_GERMAN                                      2936   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_GERMAN                                      2938   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_GERMAN                                      2940   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_GERMAN                                      2942   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_GERMAN                                      2944   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_GERMAN                                      2946   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_GERMAN                                      2948   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_GERMAN                                      2950   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_GERMAN                                      2952   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_GERMAN                                      2954   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_GERMAN                                      2956   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_GERMAN                                      2958   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_GERMAN                                      2960   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_GERMAN                                      2962   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_GERMAN                                      2964   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_GERMAN                                      2966   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_GERMAN                                      2968   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_GERMAN                                      2970   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_GERMAN                                      2972   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_GERMAN                                      2974   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_GERMAN                                      2976   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_GERMAN                                      2978   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_GERMAN                                      2980   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_GERMAN                                      2982   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_GERMAN                                      2984   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_GERMAN                                      2986   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_GERMAN                                      2988   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_GERMAN                                      2990   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_GERMAN                                      2992   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_GERMAN                                      2994   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_GERMAN                                      2996   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_GERMAN                                      2998   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_GERMAN                                      3000   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_GREEK                                        3002   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_GREEK                                        3004   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_GREEK                                        3006   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_GREEK                                        3008   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_GREEK                                        3010   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_GREEK                                        3012   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_GREEK                                        3014   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_GREEK                                        3016   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_GREEK                                        3018   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_GREEK                                       3020   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_GREEK                                       3022   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_GREEK                                       3024   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_GREEK                                       3026   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_GREEK                                       3028   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_GREEK                                       3030   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_GREEK                                       3032   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_GREEK                                       3034   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_GREEK                                       3036   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_GREEK                                       3038   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_GREEK                                       3040   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_GREEK                                       3042   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_GREEK                                       3044   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_GREEK                                       3046   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_GREEK                                       3048   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_GREEK                                       3050   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_GREEK                                       3052   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_GREEK                                       3054   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_GREEK                                       3056   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_GREEK                                       3058   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_GREEK                                       3060   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_GREEK                                       3062   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_GREEK                                       3064   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_GREEK                                       3066   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_GREEK                                       3068   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_GREEK                                       3070   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_GREEK                                       3072   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_GREEK                                       3074   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_GREEK                                       3076   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_GREEK                                       3078   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_GREEK                                       3080   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_GREEK                                       3082   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_GREEK                                       3084   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_GREEK                                       3086   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_GREEK                                       3088   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_GREEK                                       3090   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_GREEK                                       3092   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_GREEK                                       3094   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_GREEK                                       3096   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_GREEK                                       3098   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_GREEK                                       3100   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_GREEK                                       3102   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_GREEK                                       3104   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_GREEK                                       3106   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_GREEK                                       3108   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_GREEK                                       3110   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_GREEK                                       3112   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_GREEK                                       3114   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_GREEK                                       3116   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_GREEK                                       3118   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_GREEK                                       3120   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_GREEK                                       3122   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_GREEK                                       3124   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_GREEK                                       3126   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_GREEK                                       3128   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_GREEK                                       3130   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_GREEK                                       3132   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_GREEK                                       3134   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_GREEK                                       3136   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_GREEK                                       3138   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_GREEK                                       3140   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_GREEK                                       3142   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_GREEK                                       3144   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_GREEK                                       3146   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_GREEK                                       3148   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_GREEK                                       3150   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_GREEK                                       3152   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_GREEK                                       3154   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_GREEK                                       3156   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_GREEK                                       3158   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_GREEK                                       3160   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_GREEK                                       3162   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_GREEK                                       3164   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_GREEK                                       3166   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_GREEK                                       3168   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_GREEK                                       3170   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_GREEK                                       3172   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_GREEK                                       3174   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_GREEK                                       3176   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_GREEK                                       3178   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_GREEK                                       3180   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_HUNGARIAN                                    3182   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_HUNGARIAN                                    3184   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_HUNGARIAN                                    3186   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_HUNGARIAN                                    3188   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_HUNGARIAN                                    3190   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_HUNGARIAN                                    3192   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_HUNGARIAN                                    3194   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_HUNGARIAN                                    3196   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_HUNGARIAN                                    3198   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_HUNGARIAN                                   3200   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_HUNGARIAN                                   3202   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_HUNGARIAN                                   3204   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_HUNGARIAN                                   3206   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_HUNGARIAN                                   3208   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_HUNGARIAN                                   3210   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_HUNGARIAN                                   3212   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_HUNGARIAN                                   3214   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_HUNGARIAN                                   3216   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_HUNGARIAN                                   3218   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_HUNGARIAN                                   3220   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_HUNGARIAN                                   3222   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_HUNGARIAN                                   3224   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_HUNGARIAN                                   3226   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_HUNGARIAN                                   3228   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_HUNGARIAN                                   3230   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_HUNGARIAN                                   3232   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_HUNGARIAN                                   3234   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_HUNGARIAN                                   3236   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_HUNGARIAN                                   3238   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_HUNGARIAN                                   3240   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_HUNGARIAN                                   3242   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_HUNGARIAN                                   3244   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_HUNGARIAN                                   3246   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_HUNGARIAN                                   3248   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_HUNGARIAN                                   3250   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_HUNGARIAN                                   3252   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_HUNGARIAN                                   3254   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_HUNGARIAN                                   3256   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_HUNGARIAN                                   3258   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_HUNGARIAN                                   3260   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_HUNGARIAN                                   3262   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_HUNGARIAN                                   3264   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_HUNGARIAN                                   3266   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_HUNGARIAN                                   3268   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_HUNGARIAN                                   3270   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_HUNGARIAN                                   3272   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_HUNGARIAN                                   3274   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_HUNGARIAN                                   3276   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_HUNGARIAN                                   3278   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_HUNGARIAN                                   3280   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_HUNGARIAN                                   3282   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_HUNGARIAN                                   3284   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_HUNGARIAN                                   3286   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_HUNGARIAN                                   3288   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_HUNGARIAN                                   3290   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_HUNGARIAN                                   3292   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_HUNGARIAN                                   3294   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_HUNGARIAN                                   3296   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_HUNGARIAN                                   3298   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_HUNGARIAN                                   3300   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_HUNGARIAN                                   3302   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_HUNGARIAN                                   3304   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_HUNGARIAN                                   3306   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_HUNGARIAN                                   3308   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_HUNGARIAN                                   3310   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_HUNGARIAN                                   3312   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_HUNGARIAN                                   3314   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_HUNGARIAN                                   3316   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_HUNGARIAN                                   3318   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_HUNGARIAN                                   3320   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_HUNGARIAN                                   3322   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_HUNGARIAN                                   3324   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_HUNGARIAN                                   3326   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_HUNGARIAN                                   3328   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_HUNGARIAN                                   3330   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_HUNGARIAN                                   3332   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_HUNGARIAN                                   3334   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_HUNGARIAN                                   3336   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_HUNGARIAN                                   3338   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_HUNGARIAN                                   3340   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_HUNGARIAN                                   3342   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_HUNGARIAN                                   3344   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_HUNGARIAN                                   3346   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_HUNGARIAN                                   3348   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_HUNGARIAN                                   3350   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_HUNGARIAN                                   3352   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_HUNGARIAN                                   3354   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_HUNGARIAN                                   3356   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_HUNGARIAN                                   3358   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_HUNGARIAN                                   3360   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_ITALIAN                                      3362   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_ITALIAN                                      3364   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_ITALIAN                                      3366   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_ITALIAN                                      3368   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_ITALIAN                                      3370   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_ITALIAN                                      3372   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_ITALIAN                                      3374   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_ITALIAN                                      3376   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_ITALIAN                                      3378   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_ITALIAN                                     3380   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_ITALIAN                                     3382   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_ITALIAN                                     3384   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_ITALIAN                                     3386   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_ITALIAN                                     3388   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_ITALIAN                                     3390   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_ITALIAN                                     3392   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_ITALIAN                                     3394   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_ITALIAN                                     3396   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_ITALIAN                                     3398   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_ITALIAN                                     3400   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_ITALIAN                                     3402   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_ITALIAN                                     3404   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_ITALIAN                                     3406   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_ITALIAN                                     3408   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_ITALIAN                                     3410   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_ITALIAN                                     3412   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_ITALIAN                                     3414   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_ITALIAN                                     3416   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_ITALIAN                                     3418   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_ITALIAN                                     3420   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_ITALIAN                                     3422   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_ITALIAN                                     3424   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_ITALIAN                                     3426   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_ITALIAN                                     3428   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_ITALIAN                                     3430   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_ITALIAN                                     3432   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_ITALIAN                                     3434   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_ITALIAN                                     3436   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_ITALIAN                                     3438   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_ITALIAN                                     3440   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_ITALIAN                                     3442   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_ITALIAN                                     3444   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_ITALIAN                                     3446   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_ITALIAN                                     3448   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_ITALIAN                                     3450   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_ITALIAN                                     3452   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_ITALIAN                                     3454   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_ITALIAN                                     3456   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_ITALIAN                                     3458   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_ITALIAN                                     3460   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_ITALIAN                                     3462   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_ITALIAN                                     3464   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_ITALIAN                                     3466   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_ITALIAN                                     3468   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_ITALIAN                                     3470   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_ITALIAN                                     3472   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_ITALIAN                                     3474   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_ITALIAN                                     3476   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_ITALIAN                                     3478   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_ITALIAN                                     3480   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_ITALIAN                                     3482   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_ITALIAN                                     3484   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_ITALIAN                                     3486   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_ITALIAN                                     3488   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_ITALIAN                                     3490   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_ITALIAN                                     3492   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_ITALIAN                                     3494   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_ITALIAN                                     3496   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_ITALIAN                                     3498   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_ITALIAN                                     3500   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_ITALIAN                                     3502   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_ITALIAN                                     3504   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_ITALIAN                                     3506   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_ITALIAN                                     3508   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_ITALIAN                                     3510   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_ITALIAN                                     3512   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_ITALIAN                                     3514   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_ITALIAN                                     3516   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_ITALIAN                                     3518   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_ITALIAN                                     3520   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_ITALIAN                                     3522   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_ITALIAN                                     3524   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_ITALIAN                                     3526   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_ITALIAN                                     3528   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_ITALIAN                                     3530   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_ITALIAN                                     3532   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_ITALIAN                                     3534   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_ITALIAN                                     3536   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_ITALIAN                                     3538   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_ITALIAN                                     3540   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_NORWEGIAN                                    3542   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_NORWEGIAN                                    3544   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_NORWEGIAN                                    3546   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_NORWEGIAN                                    3548   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_NORWEGIAN                                    3550   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_NORWEGIAN                                    3552   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_NORWEGIAN                                    3554   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_NORWEGIAN                                    3556   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_NORWEGIAN                                    3558   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_NORWEGIAN                                   3560   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_NORWEGIAN                                   3562   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_NORWEGIAN                                   3564   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_NORWEGIAN                                   3566   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_NORWEGIAN                                   3568   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_NORWEGIAN                                   3570   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_NORWEGIAN                                   3572   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_NORWEGIAN                                   3574   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_NORWEGIAN                                   3576   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_NORWEGIAN                                   3578   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_NORWEGIAN                                   3580   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_NORWEGIAN                                   3582   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_NORWEGIAN                                   3584   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_NORWEGIAN                                   3586   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_NORWEGIAN                                   3588   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_NORWEGIAN                                   3590   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_NORWEGIAN                                   3592   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_NORWEGIAN                                   3594   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_NORWEGIAN                                   3596   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_NORWEGIAN                                   3598   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_NORWEGIAN                                   3600   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_NORWEGIAN                                   3602   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_NORWEGIAN                                   3604   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_NORWEGIAN                                   3606   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_NORWEGIAN                                   3608   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_NORWEGIAN                                   3610   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_NORWEGIAN                                   3612   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_NORWEGIAN                                   3614   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_NORWEGIAN                                   3616   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_NORWEGIAN                                   3618   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_NORWEGIAN                                   3620   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_NORWEGIAN                                   3622   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_NORWEGIAN                                   3624   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_NORWEGIAN                                   3626   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_NORWEGIAN                                   3628   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_NORWEGIAN                                   3630   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_NORWEGIAN                                   3632   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_NORWEGIAN                                   3634   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_NORWEGIAN                                   3636   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_NORWEGIAN                                   3638   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_NORWEGIAN                                   3640   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_NORWEGIAN                                   3642   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_NORWEGIAN                                   3644   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_NORWEGIAN                                   3646   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_NORWEGIAN                                   3648   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_NORWEGIAN                                   3650   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_NORWEGIAN                                   3652   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_NORWEGIAN                                   3654   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_NORWEGIAN                                   3656   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_NORWEGIAN                                   3658   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_NORWEGIAN                                   3660   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_NORWEGIAN                                   3662   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_NORWEGIAN                                   3664   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_NORWEGIAN                                   3666   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_NORWEGIAN                                   3668   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_NORWEGIAN                                   3670   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_NORWEGIAN                                   3672   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_NORWEGIAN                                   3674   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_NORWEGIAN                                   3676   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_NORWEGIAN                                   3678   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_NORWEGIAN                                   3680   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_NORWEGIAN                                   3682   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_NORWEGIAN                                   3684   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_NORWEGIAN                                   3686   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_NORWEGIAN                                   3688   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_NORWEGIAN                                   3690   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_NORWEGIAN                                   3692   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_NORWEGIAN                                   3694   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_NORWEGIAN                                   3696   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_NORWEGIAN                                   3698   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_NORWEGIAN                                   3700   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_NORWEGIAN                                   3702   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_NORWEGIAN                                   3704   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_NORWEGIAN                                   3706   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_NORWEGIAN                                   3708   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_NORWEGIAN                                   3710   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_NORWEGIAN                                   3712   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_NORWEGIAN                                   3714   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_NORWEGIAN                                   3716   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_NORWEGIAN                                   3718   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_NORWEGIAN                                   3720   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_POLISH                                       3722   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_POLISH                                       3724   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_POLISH                                       3726   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_POLISH                                       3728   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_POLISH                                       3730   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_POLISH                                       3732   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_POLISH                                       3734   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_POLISH                                       3736   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_POLISH                                       3738   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_POLISH                                      3740   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_POLISH                                      3742   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_POLISH                                      3744   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_POLISH                                      3746   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_POLISH                                      3748   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_POLISH                                      3750   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_POLISH                                      3752   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_POLISH                                      3754   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_POLISH                                      3756   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_POLISH                                      3758   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_POLISH                                      3760   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_POLISH                                      3762   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_POLISH                                      3764   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_POLISH                                      3766   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_POLISH                                      3768   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_POLISH                                      3770   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_POLISH                                      3772   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_POLISH                                      3774   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_POLISH                                      3776   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_POLISH                                      3778   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_POLISH                                      3780   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_POLISH                                      3782   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_POLISH                                      3784   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_POLISH                                      3786   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_POLISH                                      3788   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_POLISH                                      3790   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_POLISH                                      3792   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_POLISH                                      3794   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_POLISH                                      3796   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_POLISH                                      3798   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_POLISH                                      3800   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_POLISH                                      3802   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_POLISH                                      3804   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_POLISH                                      3806   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_POLISH                                      3808   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_POLISH                                      3810   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_POLISH                                      3812   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_POLISH                                      3814   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_POLISH                                      3816   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_POLISH                                      3818   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_POLISH                                      3820   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_POLISH                                      3822   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_POLISH                                      3824   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_POLISH                                      3826   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_POLISH                                      3828   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_POLISH                                      3830   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_POLISH                                      3832   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_POLISH                                      3834   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_POLISH                                      3836   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_POLISH                                      3838   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_POLISH                                      3840   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_POLISH                                      3842   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_POLISH                                      3844   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_POLISH                                      3846   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_POLISH                                      3848   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_POLISH                                      3850   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_POLISH                                      3852   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_POLISH                                      3854   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_POLISH                                      3856   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_POLISH                                      3858   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_POLISH                                      3860   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_POLISH                                      3862   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_POLISH                                      3864   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_POLISH                                      3866   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_POLISH                                      3868   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_POLISH                                      3870   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_POLISH                                      3872   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_POLISH                                      3874   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_POLISH                                      3876   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_POLISH                                      3878   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_POLISH                                      3880   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_POLISH                                      3882   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_POLISH                                      3884   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_POLISH                                      3886   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_POLISH                                      3888   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_POLISH                                      3890   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_POLISH                                      3892   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_POLISH                                      3894   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_POLISH                                      3896   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_POLISH                                      3898   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_POLISH                                      3900   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_ROMANIAN                                     3902   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_ROMANIAN                                     3904   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_ROMANIAN                                     3906   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_ROMANIAN                                     3908   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_ROMANIAN                                     3910   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_ROMANIAN                                     3912   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_ROMANIAN                                     3914   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_ROMANIAN                                     3916   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_ROMANIAN                                     3918   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_ROMANIAN                                    3920   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_ROMANIAN                                    3922   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_ROMANIAN                                    3924   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_ROMANIAN                                    3926   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_ROMANIAN                                    3928   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_ROMANIAN                                    3930   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_ROMANIAN                                    3932   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_ROMANIAN                                    3934   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_ROMANIAN                                    3936   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_ROMANIAN                                    3938   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_ROMANIAN                                    3940   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_ROMANIAN                                    3942   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_ROMANIAN                                    3944   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_ROMANIAN                                    3946   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_ROMANIAN                                    3948   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_ROMANIAN                                    3950   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_ROMANIAN                                    3952   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_ROMANIAN                                    3954   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_ROMANIAN                                    3956   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_ROMANIAN                                    3958   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_ROMANIAN                                    3960   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_ROMANIAN                                    3962   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_ROMANIAN                                    3964   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_ROMANIAN                                    3966   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_ROMANIAN                                    3968   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_ROMANIAN                                    3970   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_ROMANIAN                                    3972   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_ROMANIAN                                    3974   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_ROMANIAN                                    3976   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_ROMANIAN                                    3978   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_ROMANIAN                                    3980   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_ROMANIAN                                    3982   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_ROMANIAN                                    3984   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_ROMANIAN                                    3986   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_ROMANIAN                                    3988   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_ROMANIAN                                    3990   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_ROMANIAN                                    3992   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_ROMANIAN                                    3994   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_ROMANIAN                                    3996   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_ROMANIAN                                    3998   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_ROMANIAN                                    4000   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_ROMANIAN                                    4002   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_ROMANIAN                                    4004   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_ROMANIAN                                    4006   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_ROMANIAN                                    4008   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_ROMANIAN                                    4010   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_ROMANIAN                                    4012   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_ROMANIAN                                    4014   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_ROMANIAN                                    4016   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_ROMANIAN                                    4018   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_ROMANIAN                                    4020   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_ROMANIAN                                    4022   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_ROMANIAN                                    4024   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_ROMANIAN                                    4026   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_ROMANIAN                                    4028   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_ROMANIAN                                    4030   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_ROMANIAN                                    4032   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_ROMANIAN                                    4034   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_ROMANIAN                                    4036   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_ROMANIAN                                    4038   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_ROMANIAN                                    4040   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_ROMANIAN                                    4042   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_ROMANIAN                                    4044   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_ROMANIAN                                    4046   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_ROMANIAN                                    4048   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_ROMANIAN                                    4050   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_ROMANIAN                                    4052   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_ROMANIAN                                    4054   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_ROMANIAN                                    4056   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_ROMANIAN                                    4058   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_ROMANIAN                                    4060   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_ROMANIAN                                    4062   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_ROMANIAN                                    4064   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_ROMANIAN                                    4066   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_ROMANIAN                                    4068   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_ROMANIAN                                    4070   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_ROMANIAN                                    4072   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_ROMANIAN                                    4074   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_ROMANIAN                                    4076   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_ROMANIAN                                    4078   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_ROMANIAN                                    4080   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_RUSSIAN                                      4082   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_RUSSIAN                                      4084   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_RUSSIAN                                      4086   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_RUSSIAN                                      4088   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_RUSSIAN                                      4090   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_RUSSIAN                                      4092   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_RUSSIAN                                      4094   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_RUSSIAN                                      4096   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_RUSSIAN                                      4098   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_RUSSIAN                                     4100   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_RUSSIAN                                     4102   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_RUSSIAN                                     4104   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_RUSSIAN                                     4106   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_RUSSIAN                                     4108   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_RUSSIAN                                     4110   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_RUSSIAN                                     4112   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_RUSSIAN                                     4114   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_RUSSIAN                                     4116   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_RUSSIAN                                     4118   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_RUSSIAN                                     4120   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_RUSSIAN                                     4122   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_RUSSIAN                                     4124   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_RUSSIAN                                     4126   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_RUSSIAN                                     4128   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_RUSSIAN                                     4130   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_RUSSIAN                                     4132   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_RUSSIAN                                     4134   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_RUSSIAN                                     4136   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_RUSSIAN                                     4138   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_RUSSIAN                                     4140   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_RUSSIAN                                     4142   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_RUSSIAN                                     4144   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_RUSSIAN                                     4146   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_RUSSIAN                                     4148   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_RUSSIAN                                     4150   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_RUSSIAN                                     4152   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_RUSSIAN                                     4154   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_RUSSIAN                                     4156   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_RUSSIAN                                     4158   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_RUSSIAN                                     4160   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_RUSSIAN                                     4162   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_RUSSIAN                                     4164   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_RUSSIAN                                     4166   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_RUSSIAN                                     4168   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_RUSSIAN                                     4170   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_RUSSIAN                                     4172   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_RUSSIAN                                     4174   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_RUSSIAN                                     4176   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_RUSSIAN                                     4178   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_RUSSIAN                                     4180   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_RUSSIAN                                     4182   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_RUSSIAN                                     4184   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_RUSSIAN                                     4186   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_RUSSIAN                                     4188   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_RUSSIAN                                     4190   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_RUSSIAN                                     4192   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_RUSSIAN                                     4194   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_RUSSIAN                                     4196   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_RUSSIAN                                     4198   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_RUSSIAN                                     4200   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_RUSSIAN                                     4202   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_RUSSIAN                                     4204   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_RUSSIAN                                     4206   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_RUSSIAN                                     4208   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_RUSSIAN                                     4210   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_RUSSIAN                                     4212   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_RUSSIAN                                     4214   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_RUSSIAN                                     4216   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_RUSSIAN                                     4218   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_RUSSIAN                                     4220   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_RUSSIAN                                     4222   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_RUSSIAN                                     4224   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_RUSSIAN                                     4226   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_RUSSIAN                                     4228   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_RUSSIAN                                     4230   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_RUSSIAN                                     4232   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_RUSSIAN                                     4234   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_RUSSIAN                                     4236   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_RUSSIAN                                     4238   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_RUSSIAN                                     4240   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_RUSSIAN                                     4242   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_RUSSIAN                                     4244   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_RUSSIAN                                     4246   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_RUSSIAN                                     4248   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_RUSSIAN                                     4250   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_RUSSIAN                                     4252   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_RUSSIAN                                     4254   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_RUSSIAN                                     4256   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_RUSSIAN                                     4258   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_RUSSIAN                                     4260   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_SLOVAK                                       4262   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_SLOVAK                                       4264   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_SLOVAK                                       4266   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_SLOVAK                                       4268   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_SLOVAK                                       4270   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_SLOVAK                                       4272   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_SLOVAK                                       4274   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_SLOVAK                                       4276   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_SLOVAK                                       4278   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_SLOVAK                                      4280   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_SLOVAK                                      4282   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_SLOVAK                                      4284   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_SLOVAK                                      4286   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_SLOVAK                                      4288   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_SLOVAK                                      4290   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_SLOVAK                                      4292   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_SLOVAK                                      4294   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_SLOVAK                                      4296   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_SLOVAK                                      4298   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_SLOVAK                                      4300   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_SLOVAK                                      4302   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_SLOVAK                                      4304   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_SLOVAK                                      4306   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_SLOVAK                                      4308   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_SLOVAK                                      4310   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_SLOVAK                                      4312   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_SLOVAK                                      4314   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_SLOVAK                                      4316   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_SLOVAK                                      4318   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_SLOVAK                                      4320   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_SLOVAK                                      4322   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_SLOVAK                                      4324   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_SLOVAK                                      4326   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_SLOVAK                                      4328   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_SLOVAK                                      4330   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_SLOVAK                                      4332   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_SLOVAK                                      4334   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_SLOVAK                                      4336   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_SLOVAK                                      4338   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_SLOVAK                                      4340   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_SLOVAK                                      4342   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_SLOVAK                                      4344   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_SLOVAK                                      4346   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_SLOVAK                                      4348   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_SLOVAK                                      4350   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_SLOVAK                                      4352   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_SLOVAK                                      4354   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_SLOVAK                                      4356   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_SLOVAK                                      4358   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_SLOVAK                                      4360   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_SLOVAK                                      4362   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_SLOVAK                                      4364   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_SLOVAK                                      4366   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_SLOVAK                                      4368   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_SLOVAK                                      4370   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_SLOVAK                                      4372   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_SLOVAK                                      4374   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_SLOVAK                                      4376   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_SLOVAK                                      4378   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_SLOVAK                                      4380   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_SLOVAK                                      4382   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_SLOVAK                                      4384   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_SLOVAK                                      4386   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_SLOVAK                                      4388   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_SLOVAK                                      4390   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_SLOVAK                                      4392   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_SLOVAK                                      4394   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_SLOVAK                                      4396   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_SLOVAK                                      4398   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_SLOVAK                                      4400   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_SLOVAK                                      4402   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_SLOVAK                                      4404   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_SLOVAK                                      4406   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_SLOVAK                                      4408   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_SLOVAK                                      4410   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_SLOVAK                                      4412   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_SLOVAK                                      4414   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_SLOVAK                                      4416   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_SLOVAK                                      4418   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_SLOVAK                                      4420   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_SLOVAK                                      4422   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_SLOVAK                                      4424   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_SLOVAK                                      4426   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_SLOVAK                                      4428   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_SLOVAK                                      4430   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_SLOVAK                                      4432   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_SLOVAK                                      4434   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_SLOVAK                                      4436   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_SLOVAK                                      4438   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_SLOVAK                                      4440   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_SLOVENIAN                                    4442   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_SLOVENIAN                                    4444   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_SLOVENIAN                                    4446   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_SLOVENIAN                                    4448   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_SLOVENIAN                                    4450   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_SLOVENIAN                                    4452   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_SLOVENIAN                                    4454   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_SLOVENIAN                                    4456   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_SLOVENIAN                                    4458   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_SLOVENIAN                                   4460   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_SLOVENIAN                                   4462   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_SLOVENIAN                                   4464   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_SLOVENIAN                                   4466   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_SLOVENIAN                                   4468   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_SLOVENIAN                                   4470   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_SLOVENIAN                                   4472   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_SLOVENIAN                                   4474   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_SLOVENIAN                                   4476   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_SLOVENIAN                                   4478   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_SLOVENIAN                                   4480   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_SLOVENIAN                                   4482   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_SLOVENIAN                                   4484   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_SLOVENIAN                                   4486   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_SLOVENIAN                                   4488   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_SLOVENIAN                                   4490   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_SLOVENIAN                                   4492   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_SLOVENIAN                                   4494   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_SLOVENIAN                                   4496   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_SLOVENIAN                                   4498   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_SLOVENIAN                                   4500   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_SLOVENIAN                                   4502   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_SLOVENIAN                                   4504   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_SLOVENIAN                                   4506   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_SLOVENIAN                                   4508   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_SLOVENIAN                                   4510   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_SLOVENIAN                                   4512   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_SLOVENIAN                                   4514   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_SLOVENIAN                                   4516   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_SLOVENIAN                                   4518   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_SLOVENIAN                                   4520   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_SLOVENIAN                                   4522   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_SLOVENIAN                                   4524   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_SLOVENIAN                                   4526   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_SLOVENIAN                                   4528   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_SLOVENIAN                                   4530   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_SLOVENIAN                                   4532   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_SLOVENIAN                                   4534   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_SLOVENIAN                                   4536   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_SLOVENIAN                                   4538   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_SLOVENIAN                                   4540   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_SLOVENIAN                                   4542   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_SLOVENIAN                                   4544   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_SLOVENIAN                                   4546   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_SLOVENIAN                                   4548   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_SLOVENIAN                                   4550   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_SLOVENIAN                                   4552   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_SLOVENIAN                                   4554   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_SLOVENIAN                                   4556   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_SLOVENIAN                                   4558   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_SLOVENIAN                                   4560   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_SLOVENIAN                                   4562   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_SLOVENIAN                                   4564   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_SLOVENIAN                                   4566   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_SLOVENIAN                                   4568   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_SLOVENIAN                                   4570   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_SLOVENIAN                                   4572   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_SLOVENIAN                                   4574   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_SLOVENIAN                                   4576   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_SLOVENIAN                                   4578   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_SLOVENIAN                                   4580   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_SLOVENIAN                                   4582   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_SLOVENIAN                                   4584   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_SLOVENIAN                                   4586   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_SLOVENIAN                                   4588   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_SLOVENIAN                                   4590   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_SLOVENIAN                                   4592   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_SLOVENIAN                                   4594   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_SLOVENIAN                                   4596   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_SLOVENIAN                                   4598   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_SLOVENIAN                                   4600   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_SLOVENIAN                                   4602   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_SLOVENIAN                                   4604   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_SLOVENIAN                                   4606   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_SLOVENIAN                                   4608   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_SLOVENIAN                                   4610   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_SLOVENIAN                                   4612   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_SLOVENIAN                                   4614   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_SLOVENIAN                                   4616   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_SLOVENIAN                                   4618   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_SLOVENIAN                                   4620   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_SWEDISH                                      4622   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_SWEDISH                                      4624   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_SWEDISH                                      4626   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_SWEDISH                                      4628   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_SWEDISH                                      4630   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_SWEDISH                                      4632   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_SWEDISH                                      4634   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_SWEDISH                                      4636   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_SWEDISH                                      4638   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_SWEDISH                                     4640   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_SWEDISH                                     4642   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_SWEDISH                                     4644   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_SWEDISH                                     4646   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_SWEDISH                                     4648   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_SWEDISH                                     4650   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_SWEDISH                                     4652   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_SWEDISH                                     4654   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_SWEDISH                                     4656   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_SWEDISH                                     4658   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_SWEDISH                                     4660   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_SWEDISH                                     4662   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_SWEDISH                                     4664   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_SWEDISH                                     4666   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_SWEDISH                                     4668   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_SWEDISH                                     4670   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_SWEDISH                                     4672   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_SWEDISH                                     4674   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_SWEDISH                                     4676   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_SWEDISH                                     4678   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_SWEDISH                                     4680   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_SWEDISH                                     4682   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_SWEDISH                                     4684   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_SWEDISH                                     4686   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_SWEDISH                                     4688   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_SWEDISH                                     4690   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_SWEDISH                                     4692   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_SWEDISH                                     4694   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_SWEDISH                                     4696   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_SWEDISH                                     4698   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_SWEDISH                                     4700   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_SWEDISH                                     4702   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_SWEDISH                                     4704   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_SWEDISH                                     4706   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_SWEDISH                                     4708   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_SWEDISH                                     4710   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_SWEDISH                                     4712   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_SWEDISH                                     4714   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_SWEDISH                                     4716   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_SWEDISH                                     4718   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_SWEDISH                                     4720   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_SWEDISH                                     4722   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_SWEDISH                                     4724   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_SWEDISH                                     4726   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_SWEDISH                                     4728   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_SWEDISH                                     4730   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_SWEDISH                                     4732   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_SWEDISH                                     4734   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_SWEDISH                                     4736   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_SWEDISH                                     4738   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_SWEDISH                                     4740   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_SWEDISH                                     4742   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_SWEDISH                                     4744   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_SWEDISH                                     4746   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_SWEDISH                                     4748   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_SWEDISH                                     4750   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_SWEDISH                                     4752   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_SWEDISH                                     4754   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_SWEDISH                                     4756   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_SWEDISH                                     4758   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_SWEDISH                                     4760   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_SWEDISH                                     4762   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_SWEDISH                                     4764   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_SWEDISH                                     4766   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_SWEDISH                                     4768   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_SWEDISH                                     4770   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_SWEDISH                                     4772   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_SWEDISH                                     4774   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_SWEDISH                                     4776   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_SWEDISH                                     4778   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_SWEDISH                                     4780   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_SWEDISH                                     4782   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_SWEDISH                                     4784   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_SWEDISH                                     4786   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_SWEDISH                                     4788   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_SWEDISH                                     4790   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_SWEDISH                                     4792   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_SWEDISH                                     4794   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_SWEDISH                                     4796   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_SWEDISH                                     4798   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_SWEDISH                                     4800   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_TURKISH                                      4802   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_TURKISH                                      4804   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_TURKISH                                      4806   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_TURKISH                                      4808   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_TURKISH                                      4810   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_TURKISH                                      4812   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_TURKISH                                      4814   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_TURKISH                                      4816   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_TURKISH                                      4818   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_TURKISH                                     4820   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_TURKISH                                     4822   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_TURKISH                                     4824   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_TURKISH                                     4826   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_TURKISH                                     4828   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_TURKISH                                     4830   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_TURKISH                                     4832   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_TURKISH                                     4834   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_TURKISH                                     4836   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_TURKISH                                     4838   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_TURKISH                                     4840   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_TURKISH                                     4842   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_TURKISH                                     4844   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_TURKISH                                     4846   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_TURKISH                                     4848   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_TURKISH                                     4850   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_TURKISH                                     4852   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_TURKISH                                     4854   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_TURKISH                                     4856   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_TURKISH                                     4858   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_TURKISH                                     4860   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_TURKISH                                     4862   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_TURKISH                                     4864   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_TURKISH                                     4866   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_TURKISH                                     4868   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_TURKISH                                     4870   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_TURKISH                                     4872   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_TURKISH                                     4874   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_TURKISH                                     4876   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_TURKISH                                     4878   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_TURKISH                                     4880   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_TURKISH                                     4882   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_TURKISH                                     4884   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_TURKISH                                     4886   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_TURKISH                                     4888   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_TURKISH                                     4890   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_TURKISH                                     4892   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_TURKISH                                     4894   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_TURKISH                                     4896   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_TURKISH                                     4898   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_TURKISH                                     4900   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_TURKISH                                     4902   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_TURKISH                                     4904   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_TURKISH                                     4906   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_TURKISH                                     4908   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_TURKISH                                     4910   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_TURKISH                                     4912   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_TURKISH                                     4914   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_TURKISH                                     4916   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_TURKISH                                     4918   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_TURKISH                                     4920   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_TURKISH                                     4922   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_TURKISH                                     4924   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_TURKISH                                     4926   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_TURKISH                                     4928   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_TURKISH                                     4930   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_TURKISH                                     4932   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_TURKISH                                     4934   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_TURKISH                                     4936   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_TURKISH                                     4938   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_TURKISH                                     4940   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_TURKISH                                     4942   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_TURKISH                                     4944   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_TURKISH                                     4946   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_TURKISH                                     4948   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_TURKISH                                     4950   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_TURKISH                                     4952   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_TURKISH                                     4954   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_TURKISH                                     4956   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_TURKISH                                     4958   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_TURKISH                                     4960   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_TURKISH                                     4962   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_TURKISH                                     4964   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_TURKISH                                     4966   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_TURKISH                                     4968   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_TURKISH                                     4970   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_TURKISH                                     4972   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_TURKISH                                     4974   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_TURKISH                                     4976   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_TURKISH                                     4978   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_TURKISH                                     4980   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_UKRAINIAN                                    4982   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_UKRAINIAN                                    4984   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_UKRAINIAN                                    4986   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_UKRAINIAN                                    4988   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_UKRAINIAN                                    4990   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_UKRAINIAN                                    4992   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_UKRAINIAN                                    4994   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_UKRAINIAN                                    4996   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_UKRAINIAN                                    4998   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_UKRAINIAN                                   5000   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_UKRAINIAN                                   5002   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_UKRAINIAN                                   5004   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_UKRAINIAN                                   5006   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_UKRAINIAN                                   5008   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_UKRAINIAN                                   5010   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_UKRAINIAN                                   5012   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_UKRAINIAN                                   5014   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_UKRAINIAN                                   5016   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_UKRAINIAN                                   5018   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_UKRAINIAN                                   5020   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_UKRAINIAN                                   5022   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_UKRAINIAN                                   5024   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_UKRAINIAN                                   5026   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_UKRAINIAN                                   5028   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_UKRAINIAN                                   5030   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_UKRAINIAN                                   5032   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_UKRAINIAN                                   5034   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_UKRAINIAN                                   5036   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_UKRAINIAN                                   5038   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_UKRAINIAN                                   5040   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_UKRAINIAN                                   5042   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_UKRAINIAN                                   5044   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_UKRAINIAN                                   5046   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_UKRAINIAN                                   5048   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_UKRAINIAN                                   5050   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_UKRAINIAN                                   5052   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_UKRAINIAN                                   5054   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_UKRAINIAN                                   5056   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_UKRAINIAN                                   5058   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_UKRAINIAN                                   5060   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_UKRAINIAN                                   5062   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_UKRAINIAN                                   5064   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_UKRAINIAN                                   5066   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_UKRAINIAN                                   5068   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_UKRAINIAN                                   5070   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_UKRAINIAN                                   5072   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_UKRAINIAN                                   5074   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_UKRAINIAN                                   5076   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_UKRAINIAN                                   5078   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_UKRAINIAN                                   5080   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_UKRAINIAN                                   5082   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_UKRAINIAN                                   5084   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_UKRAINIAN                                   5086   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_UKRAINIAN                                   5088   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_UKRAINIAN                                   5090   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_UKRAINIAN                                   5092   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_UKRAINIAN                                   5094   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_UKRAINIAN                                   5096   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_UKRAINIAN                                   5098   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_UKRAINIAN                                   5100   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_UKRAINIAN                                   5102   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_UKRAINIAN                                   5104   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_UKRAINIAN                                   5106   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_UKRAINIAN                                   5108   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_UKRAINIAN                                   5110   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_UKRAINIAN                                   5112   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_UKRAINIAN                                   5114   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_UKRAINIAN                                   5116   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_UKRAINIAN                                   5118   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_UKRAINIAN                                   5120   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_UKRAINIAN                                   5122   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_UKRAINIAN                                   5124   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_UKRAINIAN                                   5126   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_UKRAINIAN                                   5128   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_UKRAINIAN                                   5130   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_UKRAINIAN                                   5132   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_UKRAINIAN                                   5134   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_UKRAINIAN                                   5136   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_UKRAINIAN                                   5138   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_UKRAINIAN                                   5140   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_UKRAINIAN                                   5142   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_UKRAINIAN                                   5144   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_UKRAINIAN                                   5146   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_UKRAINIAN                                   5148   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_UKRAINIAN                                   5150   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_UKRAINIAN                                   5152   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_UKRAINIAN                                   5154   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_UKRAINIAN                                   5156   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_UKRAINIAN                                   5158   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_UKRAINIAN                                   5160   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_ARABIC                                       5162   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_ARABIC                                       5164   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_ARABIC                                       5166   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_ARABIC                                       5168   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_ARABIC                                       5170   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_ARABIC                                       5172   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_ARABIC                                       5174   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_ARABIC                                       5176   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_ARABIC                                       5178   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_ARABIC                                      5180   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_ARABIC                                      5182   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_ARABIC                                      5184   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_ARABIC                                      5186   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_ARABIC                                      5188   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_ARABIC                                      5190   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_ARABIC                                      5192   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_ARABIC                                      5194   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_ARABIC                                      5196   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_ARABIC                                      5198   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_ARABIC                                      5200   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_ARABIC                                      5202   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_ARABIC                                      5204   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_ARABIC                                      5206   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_ARABIC                                      5208   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_ARABIC                                      5210   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_ARABIC                                      5212   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_ARABIC                                      5214   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_ARABIC                                      5216   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_ARABIC                                      5218   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_ARABIC                                      5220   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_ARABIC                                      5222   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_ARABIC                                      5224   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_ARABIC                                      5226   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_ARABIC                                      5228   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_ARABIC                                      5230   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_ARABIC                                      5232   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_ARABIC                                      5234   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_ARABIC                                      5236   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_ARABIC                                      5238   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_ARABIC                                      5240   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_ARABIC                                      5242   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_ARABIC                                      5244   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_ARABIC                                      5246   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_ARABIC                                      5248   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_ARABIC                                      5250   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_ARABIC                                      5252   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_ARABIC                                      5254   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_ARABIC                                      5256   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_ARABIC                                      5258   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_ARABIC                                      5260   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_ARABIC                                      5262   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_ARABIC                                      5264   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_ARABIC                                      5266   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_ARABIC                                      5268   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_ARABIC                                      5270   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_ARABIC                                      5272   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_ARABIC                                      5274   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_ARABIC                                      5276   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_ARABIC                                      5278   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_ARABIC                                      5280   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_ARABIC                                      5282   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_ARABIC                                      5284   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_ARABIC                                      5286   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_ARABIC                                      5288   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_ARABIC                                      5290   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_ARABIC                                      5292   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_ARABIC                                      5294   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_ARABIC                                      5296   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_ARABIC                                      5298   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_ARABIC                                      5300   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_ARABIC                                      5302   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_ARABIC                                      5304   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_ARABIC                                      5306   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_ARABIC                                      5308   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_ARABIC                                      5310   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_ARABIC                                      5312   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_ARABIC                                      5314   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_ARABIC                                      5316   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_ARABIC                                      5318   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_ARABIC                                      5320   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_ARABIC                                      5322   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_ARABIC                                      5324   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_ARABIC                                      5326   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_ARABIC                                      5328   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_ARABIC                                      5330   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_ARABIC                                      5332   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_ARABIC                                      5334   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_ARABIC                                      5336   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_ARABIC                                      5338   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_ARABIC                                      5340   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_THAI                                         5342   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_THAI                                         5344   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_THAI                                         5346   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_THAI                                         5348   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_THAI                                         5350   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_THAI                                         5352   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_THAI                                         5354   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_THAI                                         5356   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_THAI                                         5358   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_THAI                                        5360   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_THAI                                        5362   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_THAI                                        5364   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_THAI                                        5366   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_THAI                                        5368   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_THAI                                        5370   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_THAI                                        5372   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_THAI                                        5374   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_THAI                                        5376   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_THAI                                        5378   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_THAI                                        5380   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_THAI                                        5382   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_THAI                                        5384   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_THAI                                        5386   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_THAI                                        5388   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_THAI                                        5390   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_THAI                                        5392   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_THAI                                        5394   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_THAI                                        5396   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_THAI                                        5398   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_THAI                                        5400   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_THAI                                        5402   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_THAI                                        5404   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_THAI                                        5406   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_THAI                                        5408   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_THAI                                        5410   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_THAI                                        5412   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_THAI                                        5414   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_THAI                                        5416   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_THAI                                        5418   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_THAI                                        5420   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_THAI                                        5422   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_THAI                                        5424   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_THAI                                        5426   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_THAI                                        5428   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_THAI                                        5430   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_THAI                                        5432   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_THAI                                        5434   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_THAI                                        5436   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_THAI                                        5438   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_THAI                                        5440   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_THAI                                        5442   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_THAI                                        5444   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_THAI                                        5446   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_THAI                                        5448   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_THAI                                        5450   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_THAI                                        5452   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_THAI                                        5454   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_THAI                                        5456   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_THAI                                        5458   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_THAI                                        5460   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_THAI                                        5462   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_THAI                                        5464   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_THAI                                        5466   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_THAI                                        5468   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_THAI                                        5470   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_THAI                                        5472   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_THAI                                        5474   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_THAI                                        5476   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_THAI                                        5478   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_THAI                                        5480   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_THAI                                        5482   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_THAI                                        5484   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_THAI                                        5486   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_THAI                                        5488   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_THAI                                        5490   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_THAI                                        5492   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_THAI                                        5494   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_THAI                                        5496   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_THAI                                        5498   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_THAI                                        5500   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_THAI                                        5502   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_THAI                                        5504   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_THAI                                        5506   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_THAI                                        5508   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_THAI                                        5510   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_THAI                                        5512   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_THAI                                        5514   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_THAI                                        5516   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_THAI                                        5518   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_THAI                                        5520   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_KOREAN                                       5522   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_KOREAN                                       5524   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_KOREAN                                       5526   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_KOREAN                                       5528   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_KOREAN                                       5530   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_KOREAN                                       5532   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_KOREAN                                       5534   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_KOREAN                                       5536   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_KOREAN                                       5538   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_KOREAN                                      5540   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_KOREAN                                      5542   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_KOREAN                                      5544   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_KOREAN                                      5546   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_KOREAN                                      5548   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_KOREAN                                      5550   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_KOREAN                                      5552   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_KOREAN                                      5554   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_KOREAN                                      5556   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_KOREAN                                      5558   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_KOREAN                                      5560   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_KOREAN                                      5562   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_KOREAN                                      5564   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_KOREAN                                      5566   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_KOREAN                                      5568   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_KOREAN                                      5570   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_KOREAN                                      5572   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_KOREAN                                      5574   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_KOREAN                                      5576   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_KOREAN                                      5578   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_KOREAN                                      5580   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_KOREAN                                      5582   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_KOREAN                                      5584   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_KOREAN                                      5586   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_KOREAN                                      5588   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_KOREAN                                      5590   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_KOREAN                                      5592   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_KOREAN                                      5594   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_KOREAN                                      5596   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_KOREAN                                      5598   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_KOREAN                                      5600   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_KOREAN                                      5602   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_KOREAN                                      5604   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_KOREAN                                      5606   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_KOREAN                                      5608   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_KOREAN                                      5610   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_KOREAN                                      5612   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_KOREAN                                      5614   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_KOREAN                                      5616   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_KOREAN                                      5618   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_KOREAN                                      5620   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_KOREAN                                      5622   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_KOREAN                                      5624   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_KOREAN                                      5626   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_KOREAN                                      5628   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_KOREAN                                      5630   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_KOREAN                                      5632   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_KOREAN                                      5634   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_KOREAN                                      5636   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_KOREAN                                      5638   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_KOREAN                                      5640   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_KOREAN                                      5642   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_KOREAN                                      5644   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_KOREAN                                      5646   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_KOREAN                                      5648   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_KOREAN                                      5650   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_KOREAN                                      5652   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_KOREAN                                      5654   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_KOREAN                                      5656   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_KOREAN                                      5658   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_KOREAN                                      5660   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_KOREAN                                      5662   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_KOREAN                                      5664   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_KOREAN                                      5666   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_KOREAN                                      5668   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_KOREAN                                      5670   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_KOREAN                                      5672   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_KOREAN                                      5674   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_KOREAN                                      5676   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_KOREAN                                      5678   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_KOREAN                                      5680   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_KOREAN                                      5682   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_KOREAN                                      5684   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_KOREAN                                      5686   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_KOREAN                                      5688   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_KOREAN                                      5690   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_KOREAN                                      5692   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_KOREAN                                      5694   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_KOREAN                                      5696   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_KOREAN                                      5698   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_KOREAN                                      5700   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_JAPANESE                                     5702   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_JAPANESE                                     5704   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_JAPANESE                                     5706   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_JAPANESE                                     5708   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_JAPANESE                                     5710   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_JAPANESE                                     5712   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_JAPANESE                                     5714   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_JAPANESE                                     5716   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_JAPANESE                                     5718   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_JAPANESE                                    5720   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_JAPANESE                                    5722   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_JAPANESE                                    5724   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_JAPANESE                                    5726   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_JAPANESE                                    5728   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_JAPANESE                                    5730   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_JAPANESE                                    5732   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_JAPANESE                                    5734   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_JAPANESE                                    5736   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_JAPANESE                                    5738   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_JAPANESE                                    5740   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_JAPANESE                                    5742   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_JAPANESE                                    5744   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_JAPANESE                                    5746   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_JAPANESE                                    5748   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_JAPANESE                                    5750   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_JAPANESE                                    5752   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_JAPANESE                                    5754   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_JAPANESE                                    5756   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_JAPANESE                                    5758   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_JAPANESE                                    5760   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_JAPANESE                                    5762   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_JAPANESE                                    5764   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_JAPANESE                                    5766   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_JAPANESE                                    5768   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_JAPANESE                                    5770   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_JAPANESE                                    5772   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_JAPANESE                                    5774   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_JAPANESE                                    5776   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_JAPANESE                                    5778   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_JAPANESE                                    5780   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_JAPANESE                                    5782   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_JAPANESE                                    5784   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_JAPANESE                                    5786   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_JAPANESE                                    5788   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_JAPANESE                                    5790   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_JAPANESE                                    5792   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_JAPANESE                                    5794   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_JAPANESE                                    5796   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_JAPANESE                                    5798   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_JAPANESE                                    5800   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_JAPANESE                                    5802   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_JAPANESE                                    5804   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_JAPANESE                                    5806   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_JAPANESE                                    5808   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_JAPANESE                                    5810   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_JAPANESE                                    5812   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_JAPANESE                                    5814   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_JAPANESE                                    5816   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_JAPANESE                                    5818   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_JAPANESE                                    5820   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_JAPANESE                                    5822   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_JAPANESE                                    5824   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_JAPANESE                                    5826   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_JAPANESE                                    5828   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_JAPANESE                                    5830   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_JAPANESE                                    5832   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_JAPANESE                                    5834   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_JAPANESE                                    5836   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_JAPANESE                                    5838   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_JAPANESE                                    5840   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_JAPANESE                                    5842   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_JAPANESE                                    5844   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_JAPANESE                                    5846   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_JAPANESE                                    5848   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_JAPANESE                                    5850   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_JAPANESE                                    5852   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_JAPANESE                                    5854   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_JAPANESE                                    5856   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_JAPANESE                                    5858   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_JAPANESE                                    5860   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_JAPANESE                                    5862   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_JAPANESE                                    5864   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_JAPANESE                                    5866   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_JAPANESE                                    5868   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_JAPANESE                                    5870   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_JAPANESE                                    5872   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_JAPANESE                                    5874   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_JAPANESE                                    5876   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_JAPANESE                                    5878   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_JAPANESE                                    5880   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_STANDARD_CHINESE_MANDERIN                    5882   // WORD (16-Bit) --> Parameter name CORRECTED
#define EOLLIB_OFFSET_CT_COEF_2_STANDARD_CHINESE_MANDERIN                    5884   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_STANDARD_CHINESE_MANDERIN                    5886   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_STANDARD_CHINESE_MANDERIN                    5888   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_STANDARD_CHINESE_MANDERIN                    5890   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_STANDARD_CHINESE_MANDERIN                    5892   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_STANDARD_CHINESE_MANDERIN                    5894   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_STANDARD_CHINESE_MANDERIN                    5896   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_STANDARD_CHINESE_MANDERIN                    5898   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_STANDARD_CHINESE_MANDERIN                   5900   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_STANDARD_CHINESE_MANDERIN                   5902   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_STANDARD_CHINESE_MANDERIN                   5904   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_STANDARD_CHINESE_MANDERIN                   5906   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_STANDARD_CHINESE_MANDERIN                   5908   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_STANDARD_CHINESE_MANDERIN                   5910   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_STANDARD_CHINESE_MANDERIN                   5912   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_STANDARD_CHINESE_MANDERIN                   5914   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_STANDARD_CHINESE_MANDERIN                   5916   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_STANDARD_CHINESE_MANDERIN                   5918   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_STANDARD_CHINESE_MANDERIN                   5920   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_STANDARD_CHINESE_MANDERIN                   5922   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_STANDARD_CHINESE_MANDERIN                   5924   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_STANDARD_CHINESE_MANDERIN                   5926   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_STANDARD_CHINESE_MANDERIN                   5928   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_STANDARD_CHINESE_MANDERIN                   5930   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_STANDARD_CHINESE_MANDERIN                   5932   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_STANDARD_CHINESE_MANDERIN                   5934   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_STANDARD_CHINESE_MANDERIN                   5936   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_STANDARD_CHINESE_MANDERIN                   5938   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_STANDARD_CHINESE_MANDERIN                   5940   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_STANDARD_CHINESE_MANDERIN                   5942   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_STANDARD_CHINESE_MANDERIN                   5944   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_STANDARD_CHINESE_MANDERIN                   5946   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_STANDARD_CHINESE_MANDERIN                   5948   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_STANDARD_CHINESE_MANDERIN                   5950   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_STANDARD_CHINESE_MANDERIN                   5952   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_STANDARD_CHINESE_MANDERIN                   5954   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_STANDARD_CHINESE_MANDERIN                   5956   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_STANDARD_CHINESE_MANDERIN                   5958   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_STANDARD_CHINESE_MANDERIN                   5960   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_STANDARD_CHINESE_MANDERIN                   5962   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_STANDARD_CHINESE_MANDERIN                   5964   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_STANDARD_CHINESE_MANDERIN                   5966   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_STANDARD_CHINESE_MANDERIN                   5968   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_STANDARD_CHINESE_MANDERIN                   5970   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_STANDARD_CHINESE_MANDERIN                   5972   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_STANDARD_CHINESE_MANDERIN                   5974   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_STANDARD_CHINESE_MANDERIN                   5976   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_STANDARD_CHINESE_MANDERIN                   5978   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_STANDARD_CHINESE_MANDERIN                   5980   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_STANDARD_CHINESE_MANDERIN                   5982   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_STANDARD_CHINESE_MANDERIN                   5984   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_STANDARD_CHINESE_MANDERIN                   5986   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_STANDARD_CHINESE_MANDERIN                   5988   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_STANDARD_CHINESE_MANDERIN                   5990   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_STANDARD_CHINESE_MANDERIN                   5992   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_STANDARD_CHINESE_MANDERIN                   5994   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_STANDARD_CHINESE_MANDERIN                   5996   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_STANDARD_CHINESE_MANDERIN                   5998   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_STANDARD_CHINESE_MANDERIN                   6000   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_STANDARD_CHINESE_MANDERIN                   6002   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_STANDARD_CHINESE_MANDERIN                   6004   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_STANDARD_CHINESE_MANDERIN                   6006   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_STANDARD_CHINESE_MANDERIN                   6008   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_STANDARD_CHINESE_MANDERIN                   6010   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_STANDARD_CHINESE_MANDERIN                   6012   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_STANDARD_CHINESE_MANDERIN                   6014   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_STANDARD_CHINESE_MANDERIN                   6016   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_STANDARD_CHINESE_MANDERIN                   6018   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_STANDARD_CHINESE_MANDERIN                   6020   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_STANDARD_CHINESE_MANDERIN                   6022   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_STANDARD_CHINESE_MANDERIN                   6024   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_STANDARD_CHINESE_MANDERIN                   6026   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_STANDARD_CHINESE_MANDERIN                   6028   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_STANDARD_CHINESE_MANDERIN                   6030   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_STANDARD_CHINESE_MANDERIN                   6032   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_STANDARD_CHINESE_MANDERIN                   6034   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_STANDARD_CHINESE_MANDERIN                   6036   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_STANDARD_CHINESE_MANDERIN                   6038   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_STANDARD_CHINESE_MANDERIN                   6040   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_STANDARD_CHINESE_MANDERIN                   6042   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_STANDARD_CHINESE_MANDERIN                   6044   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_STANDARD_CHINESE_MANDERIN                   6046   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_STANDARD_CHINESE_MANDERIN                   6048   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_STANDARD_CHINESE_MANDERIN                   6050   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_STANDARD_CHINESE_MANDERIN                   6052   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_STANDARD_CHINESE_MANDERIN                   6054   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_STANDARD_CHINESE_MANDERIN                   6056   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_STANDARD_CHINESE_MANDERIN                   6058   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_STANDARD_CHINESE_MANDERIN                   6060   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_TRADITIONAL_CHINESE_MANDERIN                 6062   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_TRADITIONAL_CHINESE_MANDERIN                 6064   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_TRADITIONAL_CHINESE_MANDERIN                 6066   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_TRADITIONAL_CHINESE_MANDERIN                 6068   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_TRADITIONAL_CHINESE_MANDERIN                 6070   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_TRADITIONAL_CHINESE_MANDERIN                 6072   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_TRADITIONAL_CHINESE_MANDERIN                 6074   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_TRADITIONAL_CHINESE_MANDERIN                 6076   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_TRADITIONAL_CHINESE_MANDERIN                 6078   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_TRADITIONAL_CHINESE_MANDERIN                6080   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_TRADITIONAL_CHINESE_MANDERIN                6082   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_TRADITIONAL_CHINESE_MANDERIN                6084   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_TRADITIONAL_CHINESE_MANDERIN                6086   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_TRADITIONAL_CHINESE_MANDERIN                6088   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_TRADITIONAL_CHINESE_MANDERIN                6090   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_TRADITIONAL_CHINESE_MANDERIN                6092   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_TRADITIONAL_CHINESE_MANDERIN                6094   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_TRADITIONAL_CHINESE_MANDERIN                6096   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_TRADITIONAL_CHINESE_MANDERIN                6098   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_TRADITIONAL_CHINESE_MANDERIN                6100   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_TRADITIONAL_CHINESE_MANDERIN                6102   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_TRADITIONAL_CHINESE_MANDERIN                6104   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_TRADITIONAL_CHINESE_MANDERIN                6106   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_TRADITIONAL_CHINESE_MANDERIN                6108   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_TRADITIONAL_CHINESE_MANDERIN                6110   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_TRADITIONAL_CHINESE_MANDERIN                6112   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_TRADITIONAL_CHINESE_MANDERIN                6114   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_TRADITIONAL_CHINESE_MANDERIN                6116   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_TRADITIONAL_CHINESE_MANDERIN                6118   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_TRADITIONAL_CHINESE_MANDERIN                6120   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_TRADITIONAL_CHINESE_MANDERIN                6122   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_TRADITIONAL_CHINESE_MANDERIN                6124   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_TRADITIONAL_CHINESE_MANDERIN                6126   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_TRADITIONAL_CHINESE_MANDERIN                6128   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_TRADITIONAL_CHINESE_MANDERIN                6130   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_TRADITIONAL_CHINESE_MANDERIN                6132   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_TRADITIONAL_CHINESE_MANDERIN                6134   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_TRADITIONAL_CHINESE_MANDERIN                6136   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_TRADITIONAL_CHINESE_MANDERIN                6138   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_TRADITIONAL_CHINESE_MANDERIN                6140   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_TRADITIONAL_CHINESE_MANDERIN                6142   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_TRADITIONAL_CHINESE_MANDERIN                6144   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_TRADITIONAL_CHINESE_MANDERIN                6146   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_TRADITIONAL_CHINESE_MANDERIN                6148   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_TRADITIONAL_CHINESE_MANDERIN                6150   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_TRADITIONAL_CHINESE_MANDERIN                6152   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_TRADITIONAL_CHINESE_MANDERIN                6154   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_TRADITIONAL_CHINESE_MANDERIN                6156   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_TRADITIONAL_CHINESE_MANDERIN                6158   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_TRADITIONAL_CHINESE_MANDERIN                6160   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_TRADITIONAL_CHINESE_MANDERIN                6162   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_TRADITIONAL_CHINESE_MANDERIN                6164   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_TRADITIONAL_CHINESE_MANDERIN                6166   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_TRADITIONAL_CHINESE_MANDERIN                6168   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_TRADITIONAL_CHINESE_MANDERIN                6170   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_TRADITIONAL_CHINESE_MANDERIN                6172   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_TRADITIONAL_CHINESE_MANDERIN                6174   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_TRADITIONAL_CHINESE_MANDERIN                6176   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_TRADITIONAL_CHINESE_MANDERIN                6178   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_TRADITIONAL_CHINESE_MANDERIN                6180   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_TRADITIONAL_CHINESE_MANDERIN                6182   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_TRADITIONAL_CHINESE_MANDERIN                6184   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_TRADITIONAL_CHINESE_MANDERIN                6186   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_TRADITIONAL_CHINESE_MANDERIN                6188   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_TRADITIONAL_CHINESE_MANDERIN                6190   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_TRADITIONAL_CHINESE_MANDERIN                6192   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_TRADITIONAL_CHINESE_MANDERIN                6194   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_TRADITIONAL_CHINESE_MANDERIN                6196   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_TRADITIONAL_CHINESE_MANDERIN                6198   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_TRADITIONAL_CHINESE_MANDERIN                6200   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_TRADITIONAL_CHINESE_MANDERIN                6202   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_TRADITIONAL_CHINESE_MANDERIN                6204   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_TRADITIONAL_CHINESE_MANDERIN                6206   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_TRADITIONAL_CHINESE_MANDERIN                6208   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_TRADITIONAL_CHINESE_MANDERIN                6210   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_TRADITIONAL_CHINESE_MANDERIN                6212   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_TRADITIONAL_CHINESE_MANDERIN                6214   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_TRADITIONAL_CHINESE_MANDERIN                6216   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_TRADITIONAL_CHINESE_MANDERIN                6218   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_TRADITIONAL_CHINESE_MANDERIN                6220   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_TRADITIONAL_CHINESE_MANDERIN                6222   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_TRADITIONAL_CHINESE_MANDERIN                6224   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_TRADITIONAL_CHINESE_MANDERIN                6226   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_TRADITIONAL_CHINESE_MANDERIN                6228   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_TRADITIONAL_CHINESE_MANDERIN                6230   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_TRADITIONAL_CHINESE_MANDERIN                6232   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_TRADITIONAL_CHINESE_MANDERIN                6234   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_TRADITIONAL_CHINESE_MANDERIN                6236   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_TRADITIONAL_CHINESE_MANDERIN                6238   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_TRADITIONAL_CHINESE_MANDERIN                6240   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_STANDARD_CHINESE_CANTONESE                   6242   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_STANDARD_CHINESE_CANTONESE                   6244   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_STANDARD_CHINESE_CANTONESE                   6246   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_STANDARD_CHINESE_CANTONESE                   6248   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_STANDARD_CHINESE_CANTONESE                   6250   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_STANDARD_CHINESE_CANTONESE                   6252   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_STANDARD_CHINESE_CANTONESE                   6254   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_STANDARD_CHINESE_CANTONESE                   6256   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_STANDARD_CHINESE_CANTONESE                   6258   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_STANDARD_CHINESE_CANTONESE                  6260   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_STANDARD_CHINESE_CANTONESE                  6262   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_STANDARD_CHINESE_CANTONESE                  6264   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_STANDARD_CHINESE_CANTONESE                  6266   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_STANDARD_CHINESE_CANTONESE                  6268   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_STANDARD_CHINESE_CANTONESE                  6270   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_STANDARD_CHINESE_CANTONESE                  6272   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_STANDARD_CHINESE_CANTONESE                  6274   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_STANDARD_CHINESE_CANTONESE                  6276   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_STANDARD_CHINESE_CANTONESE                  6278   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_STANDARD_CHINESE_CANTONESE                  6280   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_STANDARD_CHINESE_CANTONESE                  6282   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_STANDARD_CHINESE_CANTONESE                  6284   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_STANDARD_CHINESE_CANTONESE                  6286   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_STANDARD_CHINESE_CANTONESE                  6288   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_STANDARD_CHINESE_CANTONESE                  6290   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_STANDARD_CHINESE_CANTONESE                  6292   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_STANDARD_CHINESE_CANTONESE                  6294   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_STANDARD_CHINESE_CANTONESE                  6296   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_STANDARD_CHINESE_CANTONESE                  6298   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_STANDARD_CHINESE_CANTONESE                  6300   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_STANDARD_CHINESE_CANTONESE                  6302   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_STANDARD_CHINESE_CANTONESE                  6304   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_STANDARD_CHINESE_CANTONESE                  6306   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_STANDARD_CHINESE_CANTONESE                  6308   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_STANDARD_CHINESE_CANTONESE                  6310   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_STANDARD_CHINESE_CANTONESE                  6312   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_STANDARD_CHINESE_CANTONESE                  6314   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_STANDARD_CHINESE_CANTONESE                  6316   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_STANDARD_CHINESE_CANTONESE                  6318   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_STANDARD_CHINESE_CANTONESE                  6320   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_STANDARD_CHINESE_CANTONESE                  6322   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_STANDARD_CHINESE_CANTONESE                  6324   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_STANDARD_CHINESE_CANTONESE                  6326   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_STANDARD_CHINESE_CANTONESE                  6328   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_STANDARD_CHINESE_CANTONESE                  6330   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_STANDARD_CHINESE_CANTONESE                  6332   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_STANDARD_CHINESE_CANTONESE                  6334   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_STANDARD_CHINESE_CANTONESE                  6336   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_STANDARD_CHINESE_CANTONESE                  6338   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_STANDARD_CHINESE_CANTONESE                  6340   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_STANDARD_CHINESE_CANTONESE                  6342   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_STANDARD_CHINESE_CANTONESE                  6344   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_STANDARD_CHINESE_CANTONESE                  6346   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_STANDARD_CHINESE_CANTONESE                  6348   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_STANDARD_CHINESE_CANTONESE                  6350   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_STANDARD_CHINESE_CANTONESE                  6352   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_STANDARD_CHINESE_CANTONESE                  6354   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_STANDARD_CHINESE_CANTONESE                  6356   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_STANDARD_CHINESE_CANTONESE                  6358   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_STANDARD_CHINESE_CANTONESE                  6360   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_STANDARD_CHINESE_CANTONESE                  6362   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_STANDARD_CHINESE_CANTONESE                  6364   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_STANDARD_CHINESE_CANTONESE                  6366   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_STANDARD_CHINESE_CANTONESE                  6368   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_STANDARD_CHINESE_CANTONESE                  6370   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_STANDARD_CHINESE_CANTONESE                  6372   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_STANDARD_CHINESE_CANTONESE                  6374   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_STANDARD_CHINESE_CANTONESE                  6376   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_STANDARD_CHINESE_CANTONESE                  6378   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_STANDARD_CHINESE_CANTONESE                  6380   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_STANDARD_CHINESE_CANTONESE                  6382   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_STANDARD_CHINESE_CANTONESE                  6384   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_STANDARD_CHINESE_CANTONESE                  6386   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_STANDARD_CHINESE_CANTONESE                  6388   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_STANDARD_CHINESE_CANTONESE                  6390   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_STANDARD_CHINESE_CANTONESE                  6392   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_STANDARD_CHINESE_CANTONESE                  6394   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_STANDARD_CHINESE_CANTONESE                  6396   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_STANDARD_CHINESE_CANTONESE                  6398   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_STANDARD_CHINESE_CANTONESE                  6400   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_STANDARD_CHINESE_CANTONESE                  6402   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_STANDARD_CHINESE_CANTONESE                  6404   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_STANDARD_CHINESE_CANTONESE                  6406   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_STANDARD_CHINESE_CANTONESE                  6408   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_STANDARD_CHINESE_CANTONESE                  6410   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_STANDARD_CHINESE_CANTONESE                  6412   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_STANDARD_CHINESE_CANTONESE                  6414   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_STANDARD_CHINESE_CANTONESE                  6416   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_STANDARD_CHINESE_CANTONESE                  6418   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_STANDARD_CHINESE_CANTONESE                  6420   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_1_TRADITIONAL_CHINESE_CANTONESE                6422   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_2_TRADITIONAL_CHINESE_CANTONESE                6424   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_3_TRADITIONAL_CHINESE_CANTONESE                6426   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_4_TRADITIONAL_CHINESE_CANTONESE                6428   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_5_TRADITIONAL_CHINESE_CANTONESE                6430   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_6_TRADITIONAL_CHINESE_CANTONESE                6432   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_7_TRADITIONAL_CHINESE_CANTONESE                6434   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_8_TRADITIONAL_CHINESE_CANTONESE                6436   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_9_TRADITIONAL_CHINESE_CANTONESE                6438   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_10_TRADITIONAL_CHINESE_CANTONESE               6440   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_11_TRADITIONAL_CHINESE_CANTONESE               6442   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_12_TRADITIONAL_CHINESE_CANTONESE               6444   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_13_TRADITIONAL_CHINESE_CANTONESE               6446   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_14_TRADITIONAL_CHINESE_CANTONESE               6448   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_15_TRADITIONAL_CHINESE_CANTONESE               6450   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_16_TRADITIONAL_CHINESE_CANTONESE               6452   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_17_TRADITIONAL_CHINESE_CANTONESE               6454   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_18_TRADITIONAL_CHINESE_CANTONESE               6456   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_19_TRADITIONAL_CHINESE_CANTONESE               6458   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_20_TRADITIONAL_CHINESE_CANTONESE               6460   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_21_TRADITIONAL_CHINESE_CANTONESE               6462   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_22_TRADITIONAL_CHINESE_CANTONESE               6464   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_23_TRADITIONAL_CHINESE_CANTONESE               6466   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_24_TRADITIONAL_CHINESE_CANTONESE               6468   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_25_TRADITIONAL_CHINESE_CANTONESE               6470   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_26_TRADITIONAL_CHINESE_CANTONESE               6472   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_27_TRADITIONAL_CHINESE_CANTONESE               6474   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_28_TRADITIONAL_CHINESE_CANTONESE               6476   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_29_TRADITIONAL_CHINESE_CANTONESE               6478   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_30_TRADITIONAL_CHINESE_CANTONESE               6480   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_31_TRADITIONAL_CHINESE_CANTONESE               6482   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_32_TRADITIONAL_CHINESE_CANTONESE               6484   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_33_TRADITIONAL_CHINESE_CANTONESE               6486   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_34_TRADITIONAL_CHINESE_CANTONESE               6488   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_35_TRADITIONAL_CHINESE_CANTONESE               6490   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_36_TRADITIONAL_CHINESE_CANTONESE               6492   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_37_TRADITIONAL_CHINESE_CANTONESE               6494   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_38_TRADITIONAL_CHINESE_CANTONESE               6496   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_39_TRADITIONAL_CHINESE_CANTONESE               6498   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_40_TRADITIONAL_CHINESE_CANTONESE               6500   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_41_TRADITIONAL_CHINESE_CANTONESE               6502   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_42_TRADITIONAL_CHINESE_CANTONESE               6504   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_43_TRADITIONAL_CHINESE_CANTONESE               6506   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_44_TRADITIONAL_CHINESE_CANTONESE               6508   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_45_TRADITIONAL_CHINESE_CANTONESE               6510   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_46_TRADITIONAL_CHINESE_CANTONESE               6512   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_47_TRADITIONAL_CHINESE_CANTONESE               6514   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_48_TRADITIONAL_CHINESE_CANTONESE               6516   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_49_TRADITIONAL_CHINESE_CANTONESE               6518   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_50_TRADITIONAL_CHINESE_CANTONESE               6520   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_51_TRADITIONAL_CHINESE_CANTONESE               6522   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_52_TRADITIONAL_CHINESE_CANTONESE               6524   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_53_TRADITIONAL_CHINESE_CANTONESE               6526   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_54_TRADITIONAL_CHINESE_CANTONESE               6528   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_55_TRADITIONAL_CHINESE_CANTONESE               6530   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_56_TRADITIONAL_CHINESE_CANTONESE               6532   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_57_TRADITIONAL_CHINESE_CANTONESE               6534   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_58_TRADITIONAL_CHINESE_CANTONESE               6536   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_59_TRADITIONAL_CHINESE_CANTONESE               6538   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_60_TRADITIONAL_CHINESE_CANTONESE               6540   // WORD (16-Bit)
#define EOLLIB_OFFSET_CT_COEF_61_TRADITIONAL_CHINESE_CANTONESE               6542   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_62_TRADITIONAL_CHINESE_CANTONESE               6544   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_63_TRADITIONAL_CHINESE_CANTONESE               6546   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_64_TRADITIONAL_CHINESE_CANTONESE               6548   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_65_TRADITIONAL_CHINESE_CANTONESE               6550   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_66_TRADITIONAL_CHINESE_CANTONESE               6552   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_67_TRADITIONAL_CHINESE_CANTONESE               6554   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_68_TRADITIONAL_CHINESE_CANTONESE               6556   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_69_TRADITIONAL_CHINESE_CANTONESE               6558   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_70_TRADITIONAL_CHINESE_CANTONESE               6560   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_71_TRADITIONAL_CHINESE_CANTONESE               6562   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_72_TRADITIONAL_CHINESE_CANTONESE               6564   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_73_TRADITIONAL_CHINESE_CANTONESE               6566   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_74_TRADITIONAL_CHINESE_CANTONESE               6568   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_75_TRADITIONAL_CHINESE_CANTONESE               6570   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_76_TRADITIONAL_CHINESE_CANTONESE               6572   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_77_TRADITIONAL_CHINESE_CANTONESE               6574   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_78_TRADITIONAL_CHINESE_CANTONESE               6576   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_79_TRADITIONAL_CHINESE_CANTONESE               6578   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_80_TRADITIONAL_CHINESE_CANTONESE               6580   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_81_TRADITIONAL_CHINESE_CANTONESE               6582   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_82_TRADITIONAL_CHINESE_CANTONESE               6584   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_83_TRADITIONAL_CHINESE_CANTONESE               6586   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_84_TRADITIONAL_CHINESE_CANTONESE               6588   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_85_TRADITIONAL_CHINESE_CANTONESE               6590   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_86_TRADITIONAL_CHINESE_CANTONESE               6592   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_87_TRADITIONAL_CHINESE_CANTONESE               6594   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_88_TRADITIONAL_CHINESE_CANTONESE               6596   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_89_TRADITIONAL_CHINESE_CANTONESE               6598   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_CT_COEF_90_TRADITIONAL_CHINESE_CANTONESE               6600   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_ONE_SHOT_DESTINATION_LEADING_SILENCE                   6602   // WORD (16-Bit) --> Parameter name CORRECTED
#define EOLLIB_OFFSET_ONE_SHOT_DESTINATION_TRAILING_SILENCE                  6604   // WORD (16-Bit)
#define EOLLIB_OFFSET_ONE_SHOT_DESTINATION_MAXIMUM_USER_SPEECH_TIME          6606   // WORD (16-Bit)
#define EOLLIB_OFFSET_NOSPEECHTIMEOUT                                        6608   // WORD (16-Bit)
#define EOLLIB_OFFSET_NOSPEECHTIMEOUT_EXTENDED                               6610   // WORD (16-Bit)
#define EOLLIB_OFFSET_VOICE_KEYPAD_CONT_DIGITS_TIMEOUT                       6612   // WORD (16-Bit)
#define EOLLIB_OFFSET_VOICE_KEYPAD_CONT_DIGITS_TIMEOUT_EXTENDED              6614   // WORD (16-Bit)
#define EOLLIB_OFFSET_NL_EOU_TIMEOUT                                         6616   // BYTE (8-Bit)
#define EOLLIB_OFFSET_CONTDIGTIMEOUT                                         6617   // BYTE (8-Bit)
#define EOLLIB_OFFSET_SPEECH_MANUAL_INTERACTION_TIMEOUT                      6618   // WORD (16-Bit)
#define EOLLIB_OFFSET_FSG_END_OF_UTTERANCE_TIMEOUT                           6620   // BYTE (8-Bit)
#define EOLLIB_OFFSET_MAXIMUM_USER_SPEECH_TIME                               6622   // WORD (16-Bit)
#define EOLLIB_OFFSET_DEFAULT_SPEECHRECOGNITION_PROMPTMODE                   6624   // BYTE (8-Bit)
#define EOLLIB_OFFSET_ENABLE_UNSUPERVISED_SPEAKER_ADAPTION_PERSISTANCE       6625   // BYTE (Boolean)
#define EOLLIB_OFFSET_REGIONAL_PHONE_NUMBER_GROUPING_PLAYBACK_PAUSE_LENGTH   6626   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RESERVED_BYTE_1_SPEECH_REC                             6627   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_SPEECH_REC                             6628   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_SPEECH_REC                             6629   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_SPEECH_REC                             6630   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_SPEECH_REC                             6631   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_SPEECH_REC                             6632   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_SPEECH_REC                             6633   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_SPEECH_REC                             6634   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_SPEECH_REC                             6635   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_SPEECH_REC                            6636   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_SPEECH_REC                             6638   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_SPEECH_REC                             6640   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_SPEECH_REC                             6642   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_SPEECH_REC                             6644   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_SPEECH_REC                             6646   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_SPEECH_REC                             6648   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_SPEECH_REC                             6650   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_SPEECH_REC                             6652   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_SPEECH_REC                             6654   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_SPEECH_REC                            6656   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_SPEECH_REC                            6658   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_SPEECH_REC                            6662   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END                                 8177   // max. limit

// HAND-FREE TUNING
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_HF_TUNING_CAL   0   // BYTE (Boolean)
#define EOLLIB_OFFSET_PHFTR_CS_COEF1                             1   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF2                             2   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF3                             3   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF4                             4   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF5                             5   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF6                             6   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF7                             7   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF8                             8   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF9                             9   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF10                            10   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF11                            11   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF12                            12   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF13                            13   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF14                            14   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF15                            15   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF16                            16   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF17                            17   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF18                            18   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF19                            20   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF20                            22   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF21                            23   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF22                            24   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF23                            26   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF24                            28   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF25                            30   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF26                            32   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF27                            34   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF28                            36   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF29                            38   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF30                            40   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF31                            42   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF32                            44   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF33                            46   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF34                            48   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF35                            50   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF36                            52   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF37                            54   // BYTE (8-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_CS_COEF38                            56   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF39                            58   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF40                            60   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF41                            62   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF42                            64   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF43                            66   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF44                            68   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_CS_COEF45                            70   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF1                            72   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF2                            74   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF3                            76   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF4                            78   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF5                            80   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF6                            82   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF7                            84   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF8                            86   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF9                            88   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF10                           90   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF11                           92   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF12                           94   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF13                           95   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF14                           96   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF15                           98   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF16                           100   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF17                           102   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF18                           104   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF19                           106   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF20                           108   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF21                           110   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF22                           112   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF23                           114   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF24                           116   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF25                           118   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF26                           120   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF27                           122   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF28                           124   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF29                           126   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF30                           128   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF31                           130   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF32                           132   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF33                           134   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF34                           136   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF35                           138   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF36                           140   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF37                           142   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF38                           144   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF39                           146   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF40                           148   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF41                           150   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF42                           152   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF43                           154   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF44                           156   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF45                           158   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF46                           160   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF47                           162   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF48                           164   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF49                           166   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF50                           168   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF51                           170   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF52                           172   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF53                           174   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF54                           176   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF55                           178   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF56                           180   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF57                           182   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF58                           184   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF59                           186   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF60                           188   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF61                           190   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF62                           192   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF63                           194   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF64                           196   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF65                           198   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF66                           200   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF67                           202   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF68                           204   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF69                           206   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF70                           208   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP1_COEF71                           210   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF72                           212   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF73                           214   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF74                           216   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF75                           218   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF76                           220   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF77                           222   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF78                           224   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF79                           226   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP1_COEF80                           228   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF1                            230   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF2                            232   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF3                            234   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF4                            236   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF5                            238   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF6                            240   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF7                            242   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF8                            244   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF9                            246   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF10                           248   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF11                           250   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF12                           252   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF13                           253   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF14                           254   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF15                           256   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF16                           258   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF17                           260   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF18                           262   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF19                           264   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF20                           266   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF21                           268   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF22                           270   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF23                           272   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF24                           274   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF25                           276   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF26                           278   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF27                           280   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF28                           282   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF29                           284   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF30                           286   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF31                           288   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF32                           290   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF33                           292   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF34                           294   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF35                           296   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF36                           298   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF37                           300   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF38                           302   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF39                           304   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF40                           306   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF41                           308   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF42                           310   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF43                           312   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF44                           314   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF45                           316   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF46                           318   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF47                           320   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF48                           322   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF49                           324   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF50                           326   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF51                           328   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF52                           330   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF53                           332   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF54                           334   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF55                           336   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF56                           338   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF57                           340   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF58                           342   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF59                           344   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF60                           346   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF61                           348   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF62                           350   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF63                           352   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF64                           354   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF65                           356   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF66                           358   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF67                           360   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF68                           362   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF69                           364   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF70                           366   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP2_COEF71                           368   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF72                           370   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF73                           372   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF74                           374   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF75                           376   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF76                           378   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF77                           380   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF78                           382   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF79                           384   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP2_COEF80                           386   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF1                            388   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF2                            390   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF3                            392   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF4                            394   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF5                            396   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF6                            398   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF7                            400   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF8                            402   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF9                            404   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF10                           406   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF11                           408   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF12                           410   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF13                           411   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF14                           412   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF15                           414   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF16                           416   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF17                           418   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF18                           420   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF19                           422   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF20                           424   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF21                           426   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF22                           428   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF23                           430   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF24                           432   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF25                           434   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF26                           436   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF27                           438   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF28                           440   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF29                           442   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF30                           444   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF31                           446   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF32                           448   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF33                           450   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF34                           452   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF35                           454   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF36                           456   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF37                           458   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF38                           460   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF39                           462   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF40                           464   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF41                           466   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF42                           468   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF43                           470   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF44                           472   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF45                           474   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF46                           476   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF47                           478   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF48                           480   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF49                           482   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF50                           484   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF51                           486   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF52                           488   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF53                           490   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF54                           492   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF55                           494   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF56                           496   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF57                           498   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF58                           500   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF59                           502   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF60                           504   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF61                           506   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF62                           508   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF63                           510   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF64                           512   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF65                           514   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF66                           516   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF67                           518   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF68                           520   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF69                           522   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF70                           524   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP3_COEF71                           526   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF72                           528   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF73                           530   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF74                           532   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF75                           534   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF76                           536   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF77                           538   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF78                           540   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF79                           542   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP3_COEF80                           544   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF1                            546   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF2                            548   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF3                            550   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF4                            552   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF5                            554   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF6                            556   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF7                            558   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF8                            560   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF9                            562   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF10                           564   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF11                           566   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF12                           568   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF13                           569   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF14                           570   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF15                           572   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF16                           574   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF17                           576   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF18                           578   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF19                           580   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF20                           582   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF21                           584   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF22                           586   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF23                           588   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF24                           590   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF25                           592   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF26                           594   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF27                           596   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF28                           598   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF29                           600   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF30                           602   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF31                           604   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF32                           606   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF33                           608   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF34                           610   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF35                           612   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF36                           614   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF37                           616   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF38                           618   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF39                           620   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF40                           622   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF41                           624   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF42                           626   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF43                           628   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF44                           630   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF45                           632   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF46                           634   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF47                           636   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF48                           638   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF49                           640   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF50                           642   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF51                           644   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF52                           646   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF53                           648   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF54                           650   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF55                           652   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF56                           654   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF57                           656   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF58                           658   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF59                           660   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF60                           662   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF61                           664   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF62                           666   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF63                           668   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF64                           670   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF65                           672   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF66                           674   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF67                           676   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF68                           678   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF69                           680   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF70                           682   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP4_COEF71                           684   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF72                           686   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF73                           688   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF74                           690   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF75                           692   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF76                           694   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF77                           696   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF78                           698   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF79                           700   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP4_COEF80                           702   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF1                            704   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF2                            706   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF3                            708   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF4                            710   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF5                            712   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF6                            714   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF7                            716   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF8                            718   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF9                            720   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF10                           722   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF11                           724   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF12                           726   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF13                           727   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF14                           728   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF15                           730   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF16                           732   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF17                           734   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF18                           736   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF19                           738   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF20                           740   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF21                           742   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF22                           744   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF23                           746   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF24                           748   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF25                           750   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF26                           752   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF27                           754   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF28                           756   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF29                           758   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF30                           760   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF31                           762   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF32                           764   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF33                           766   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF34                           768   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF35                           770   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF36                           772   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF37                           774   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF38                           776   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF39                           778   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF40                           780   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF41                           782   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF42                           784   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF43                           786   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF44                           788   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF45                           790   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF46                           792   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF47                           794   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF48                           796   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF49                           798   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF50                           800   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF51                           802   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF52                           804   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF53                           806   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF54                           808   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF55                           810   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF56                           812   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF57                           814   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF58                           816   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF59                           818   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF60                           820   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF61                           822   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF62                           824   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF63                           826   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF64                           828   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF65                           830   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF66                           832   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF67                           834   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF68                           836   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF69                           838   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF70                           840   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP5_COEF71                           842   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF72                           844   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF73                           846   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF74                           848   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF75                           850   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF76                           852   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF77                           854   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF78                           856   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF79                           858   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP5_COEF80                           860   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF1                            862   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF2                            864   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF3                            866   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF4                            868   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF5                            870   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF6                            872   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF7                            874   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF8                            876   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF9                            878   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF10                           880   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF11                           882   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF12                           884   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF13                           885   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF14                           886   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF15                           888   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF16                           890   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF17                           892   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF18                           894   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF19                           896   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF20                           898   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF21                           900   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF22                           902   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF23                           904   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF24                           906   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF25                           908   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF26                           910   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF27                           912   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF28                           914   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF29                           916   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF30                           918   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF31                           920   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF32                           922   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF33                           924   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF34                           926   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF35                           928   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF36                           930   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF37                           932   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF38                           934   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF39                           936   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF40                           938   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF41                           940   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF42                           942   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF43                           944   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF44                           946   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF45                           948   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF46                           950   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF47                           952   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF48                           954   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF49                           956   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF50                           958   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF51                           960   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF52                           962   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF53                           964   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF54                           966   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF55                           968   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF56                           970   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF57                           972   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF58                           974   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF59                           976   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF60                           978   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF61                           980   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF62                           982   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF63                           984   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF64                           986   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF65                           988   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF66                           990   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF67                           992   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF68                           994   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF69                           996   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF70                           998   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP6_COEF71                           1000   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF72                           1002   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF73                           1004   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF74                           1006   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF75                           1008   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF76                           1010   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF77                           1012   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF78                           1014   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF79                           1016   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP6_COEF80                           1018   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF1                            1020   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF2                            1022   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF3                            1024   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF4                            1026   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF5                            1028   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF6                            1030   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF7                            1032   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF8                            1034   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF9                            1036   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF10                           1038   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF11                           1040   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF12                           1042   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF13                           1043   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF14                           1044   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF15                           1046   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF16                           1048   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF17                           1050   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF18                           1052   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF19                           1054   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF20                           1056   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF21                           1058   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF22                           1060   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF23                           1062   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF24                           1064   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF25                           1066   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF26                           1068   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF27                           1070   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF28                           1072   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF29                           1074   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF30                           1076   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF31                           1078   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF32                           1080   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF33                           1082   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF34                           1084   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF35                           1086   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF36                           1088   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF37                           1090   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF38                           1092   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF39                           1094   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF40                           1096   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF41                           1098   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF42                           1100   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF43                           1102   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF44                           1104   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF45                           1106   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF46                           1108   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF47                           1110   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF48                           1112   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF49                           1114   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF50                           1116   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF51                           1118   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF52                           1120   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF53                           1122   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF54                           1124   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF55                           1126   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF56                           1128   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF57                           1130   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF58                           1132   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF59                           1134   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF60                           1136   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF61                           1138   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF62                           1140   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF63                           1142   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF64                           1144   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF65                           1146   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF66                           1148   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF67                           1150   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF68                           1152   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF69                           1154   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF70                           1156   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP7_COEF71                           1158   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF72                           1160   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF73                           1162   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF74                           1164   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF75                           1166   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF76                           1168   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF77                           1170   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF78                           1172   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF79                           1174   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP7_COEF80                           1176   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF1                            1178   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF2                            1180   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF3                            1182   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF4                            1184   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF5                            1186   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF6                            1188   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF7                            1190   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF8                            1192   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF9                            1194   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF10                           1196   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF11                           1198   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF12                           1200   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF13                           1201   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF14                           1202   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF15                           1204   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF16                           1206   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF17                           1208   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF18                           1210   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF19                           1212   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF20                           1214   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF21                           1216   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF22                           1218   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF23                           1220   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF24                           1222   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF25                           1224   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF26                           1226   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF27                           1228   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF28                           1230   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF29                           1232   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF30                           1234   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF31                           1236   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF32                           1238   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF33                           1240   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF34                           1242   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF35                           1244   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF36                           1246   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF37                           1248   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF38                           1250   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF39                           1252   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF40                           1254   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF41                           1256   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF42                           1258   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF43                           1260   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF44                           1262   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF45                           1264   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF46                           1266   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF47                           1268   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF48                           1270   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF49                           1272   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF50                           1274   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF51                           1276   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF52                           1278   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF53                           1280   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF54                           1282   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF55                           1284   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF56                           1286   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF57                           1288   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF58                           1290   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF59                           1292   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF60                           1294   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF61                           1296   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF62                           1298   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF63                           1300   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF64                           1302   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF65                           1304   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF66                           1306   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF67                           1308   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF68                           1310   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF69                           1312   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF70                           1314   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP8_COEF71                           1316   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF72                           1318   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF73                           1320   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF74                           1322   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF75                           1324   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF76                           1326   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF77                           1328   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF78                           1330   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF79                           1332   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP8_COEF80                           1334   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF1                            1336   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF2                            1338   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF3                            1340   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF4                            1342   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF5                            1344   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF6                            1346   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF7                            1348   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF8                            1350   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF9                            1352   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF10                           1354   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF11                           1356   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF12                           1358   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF13                           1359   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF14                           1360   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF15                           1362   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF16                           1364   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF17                           1366   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF18                           1368   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF19                           1370   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF20                           1372   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF21                           1374   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF22                           1376   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF23                           1378   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF24                           1380   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF25                           1382   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF26                           1384   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF27                           1386   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF28                           1388   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF29                           1390   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF30                           1392   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF31                           1394   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF32                           1396   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF33                           1398   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF34                           1400   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF35                           1402   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF36                           1404   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF37                           1406   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF38                           1408   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF39                           1410   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF40                           1412   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF41                           1414   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF42                           1416   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF43                           1418   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF44                           1420   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF45                           1422   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF46                           1424   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF47                           1426   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF48                           1428   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF49                           1430   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF50                           1432   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF51                           1434   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF52                           1436   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF53                           1438   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF54                           1440   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF55                           1442   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF56                           1444   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF57                           1446   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF58                           1448   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF59                           1450   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF60                           1452   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF61                           1454   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF62                           1456   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF63                           1458   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF64                           1460   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF65                           1462   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF66                           1464   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF67                           1466   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF68                           1468   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF69                           1470   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF70                           1472   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP9_COEF71                           1474   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF72                           1476   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF73                           1478   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF74                           1480   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF75                           1482   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF76                           1484   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF77                           1486   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF78                           1488   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF79                           1490   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP9_COEF80                           1492   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF1                           1494   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF2                           1496   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF3                           1498   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF4                           1500   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF5                           1502   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF6                           1504   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF7                           1506   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF8                           1508   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF9                           1510   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF10                          1512   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF11                          1514   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF12                          1516   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF13                          1517   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF14                          1518   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF15                          1520   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF16                          1522   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF17                          1524   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF18                          1526   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF19                          1528   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF20                          1530   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF21                          1532   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF22                          1534   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF23                          1536   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF24                          1538   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF25                          1540   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF26                          1542   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF27                          1544   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF28                          1546   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF29                          1548   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF30                          1550   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF31                          1552   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF32                          1554   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF33                          1556   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF34                          1558   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF35                          1560   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF36                          1562   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF37                          1564   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF38                          1566   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF39                          1568   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF40                          1570   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF41                          1572   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF42                          1574   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF43                          1576   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF44                          1578   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF45                          1580   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF46                          1582   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF47                          1584   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF48                          1586   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF49                          1588   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF50                          1590   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF51                          1592   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF52                          1594   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF53                          1596   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF54                          1598   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF55                          1600   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF56                          1602   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF57                          1604   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF58                          1606   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF59                          1608   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF60                          1610   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF61                          1612   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF62                          1614   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF63                          1616   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF64                          1618   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF65                          1620   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF66                          1622   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF67                          1624   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF68                          1626   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF69                          1628   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF70                          1630   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_PP10_COEF71                          1632   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF72                          1634   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF73                          1636   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF74                          1638   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF75                          1640   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF76                          1642   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF77                          1644   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF78                          1646   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF79                          1648   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_PP10_COEF80                          1650   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF1                        1652   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF2                        1654   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF3                        1656   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF4                        1658   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF5                        1660   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF6                        1662   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF7                        1664   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF8                        1666   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF9                        1668   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF10                       1670   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF11                       1672   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF12                       1674   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF13                       1675   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF14                       1676   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF15                       1678   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF16                       1680   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF17                       1682   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF18                       1684   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF19                       1686   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF20                       1688   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF21                       1690   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF22                       1692   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF23                       1694   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF24                       1696   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF25                       1698   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF26                       1700   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF27                       1702   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF28                       1704   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF29                       1706   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF30                       1708   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF31                       1710   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF32                       1712   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF33                       1714   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF34                       1716   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF35                       1718   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF36                       1720   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF37                       1722   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF38                       1724   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF39                       1726   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF40                       1728   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF41                       1730   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF42                       1732   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF43                       1734   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF44                       1736   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF45                       1738   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF46                       1740   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF47                       1742   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF48                       1744   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF49                       1746   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF50                       1748   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF51                       1750   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF52                       1752   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF53                       1754   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF54                       1756   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF55                       1758   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF56                       1760   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF57                       1762   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF58                       1764   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF59                       1766   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF60                       1768   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF61                       1770   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF62                       1772   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF63                       1774   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF64                       1776   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF65                       1778   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF66                       1780   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF67                       1782   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF68                       1784   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF69                       1786   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF70                       1788   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF71                       1790   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF72                       1792   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF73                       1794   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF74                       1796   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF75                       1798   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF76                       1800   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF77                       1802   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF78                       1804   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF79                       1806   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_DEFAULT_COEF80                       1808   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF1                       1810   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF2                       1812   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF3                       1814   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF4                       1816   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF5                       1818   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF6                       1820   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF7                       1822   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF8                       1824   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF9                       1826   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF10                      1828   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF11                      1830   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF12                      1832   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF13                      1833   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF14                      1834   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF15                      1836   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF16                      1838   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF17                      1840   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF18                      1842   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF19                      1844   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF20                      1846   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF21                      1848   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF22                      1850   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF23                      1852   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF24                      1854   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF25                      1856   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF26                      1858   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF27                      1860   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF28                      1862   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF29                      1864   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF30                      1866   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF31                      1868   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF32                      1870   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF33                      1872   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF34                      1874   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF35                      1876   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF36                      1878   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF37                      1880   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF38                      1882   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF39                      1884   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF40                      1886   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF41                      1888   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF42                      1890   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF43                      1892   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF44                      1894   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF45                      1896   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF46                      1898   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF47                      1900   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF48                      1902   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF49                      1904   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF50                      1906   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF51                      1908   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF52                      1910   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF53                      1912   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF54                      1914   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF55                      1916   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF56                      1918   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF57                      1920   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF58                      1922   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF59                      1924   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF60                      1926   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF61                      1928   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF62                      1930   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF63                      1932   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF64                      1934   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF65                      1936   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF66                      1938   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF67                      1940   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF68                      1942   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF69                      1944   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF70                      1946   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF71                      1948   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF72                      1950   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF73                      1952   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF74                      1954   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF75                      1956   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF76                      1958   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF77                      1960   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF78                      1962   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF79                      1964   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK1_COEF80                      1966   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF1                       1968   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF2                       1970   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF3                       1972   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF4                       1974   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF5                       1976   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF6                       1978   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF7                       1980   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF8                       1982   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF9                       1984   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF10                      1986   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF11                      1988   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF12                      1990   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF13                      1991   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF14                      1992   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF15                      1994   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF16                      1996   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF17                      1998   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF18                      2000   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF19                      2002   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF20                      2004   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF21                      2006   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF22                      2008   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF23                      2010   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF24                      2012   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF25                      2014   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF26                      2016   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF27                      2018   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF28                      2020   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF29                      2022   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF30                      2024   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF31                      2026   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF32                      2028   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF33                      2030   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF34                      2032   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF35                      2034   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF36                      2036   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF37                      2038   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF38                      2040   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF39                      2042   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF40                      2044   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF41                      2046   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF42                      2048   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF43                      2050   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF44                      2052   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF45                      2054   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF46                      2056   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF47                      2058   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF48                      2060   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF49                      2062   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF50                      2064   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF51                      2066   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF52                      2068   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF53                      2070   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF54                      2072   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF55                      2074   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF56                      2076   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF57                      2078   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF58                      2080   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF59                      2082   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF60                      2084   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF61                      2086   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF62                      2088   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF63                      2090   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF64                      2092   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF65                      2094   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF66                      2096   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF67                      2098   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF68                      2100   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF69                      2102   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF70                      2104   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF71                      2106   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF72                      2108   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF73                      2110   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF74                      2112   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF75                      2114   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF76                      2116   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF77                      2118   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF78                      2120   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF79                      2122   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK2_COEF80                      2124   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF1                       2126   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF2                       2128   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF3                       2130   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF4                       2132   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF5                       2134   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF6                       2136   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF7                       2138   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF8                       2140   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF9                       2142   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF10                      2144   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF11                      2146   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF12                      2148   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF13                      2149   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF14                      2150   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF15                      2152   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF16                      2154   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF17                      2156   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF18                      2158   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF19                      2160   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF20                      2162   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF21                      2164   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF22                      2166   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF23                      2168   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF24                      2170   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF25                      2172   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF26                      2174   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF27                      2176   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF28                      2178   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF29                      2180   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF30                      2182   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF31                      2184   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF32                      2186   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF33                      2188   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF34                      2190   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF35                      2192   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF36                      2194   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF37                      2196   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF38                      2198   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF39                      2200   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF40                      2202   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF41                      2204   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF42                      2206   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF43                      2208   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF44                      2210   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF45                      2212   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF46                      2214   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF47                      2216   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF48                      2218   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF49                      2220   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF50                      2222   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF51                      2224   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF52                      2226   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF53                      2228   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF54                      2230   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF55                      2232   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF56                      2234   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF57                      2236   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF58                      2238   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF59                      2240   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF60                      2242   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF61                      2244   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF62                      2246   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF63                      2248   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF64                      2250   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF65                      2252   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF66                      2254   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF67                      2256   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF68                      2258   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF69                      2260   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF70                      2262   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF71                      2264   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF72                      2266   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF73                      2268   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF74                      2270   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF75                      2272   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF76                      2274   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF77                      2276   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF78                      2278   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF79                      2280   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_NETWORK3_COEF80                      2282   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF1                  2284   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF2                  2286   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF3                  2288   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF4                  2290   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF5                  2292   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF6                  2294   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF7                  2296   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF8                  2298   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF9                  2300   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF10                 2302   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF11                 2304   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF12                 2306   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF13                 2307   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF14                 2308   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF15                 2310   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF16                 2312   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF17                 2314   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF18                 2316   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF19                 2318   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF20                 2320   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF21                 2322   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF22                 2324   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF23                 2326   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF24                 2328   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF25                 2330   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF26                 2332   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF27                 2334   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF28                 2336   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF29                 2338   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF30                 2340   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF31                 2342   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF32                 2344   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF33                 2346   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF34                 2348   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF35                 2350   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF36                 2352   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF37                 2354   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF38                 2356   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF39                 2358   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF40                 2360   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF41                 2362   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF42                 2364   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF43                 2366   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF44                 2368   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF45                 2370   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF46                 2372   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF47                 2374   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF48                 2376   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF49                 2378   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF50                 2380   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF51                 2382   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF52                 2384   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF53                 2386   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF54                 2388   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF55                 2390   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF56                 2392   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF57                 2394   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF58                 2396   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF59                 2398   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF60                 2400   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF61                 2402   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF62                 2404   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF63                 2406   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF64                 2408   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF65                 2410   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF66                 2412   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF67                 2414   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF68                 2416   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF69                 2418   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF70                 2420   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF71                 2422   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF72                 2424   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF73                 2426   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF74                 2428   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF75                 2430   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF76                 2432   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF77                 2434   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF78                 2436   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF79                 2438   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER1_COEF80                 2440   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF1                  2442   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF2                  2444   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF3                  2446   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF4                  2448   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF5                  2450   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF6                  2452   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF7                  2454   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF8                  2456   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF9                  2458   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF10                 2460   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF11                 2462   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF12                 2464   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF13                 2465   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF14                 2466   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF15                 2468   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF16                 2470   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF17                 2472   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF18                 2474   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF19                 2476   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF20                 2478   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF21                 2480   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF22                 2482   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF23                 2484   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF24                 2486   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF25                 2488   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF26                 2490   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF27                 2492   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF28                 2494   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF29                 2496   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF30                 2498   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF31                 2500   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF32                 2502   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF33                 2504   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF34                 2506   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF35                 2508   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF36                 2510   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF37                 2512   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF38                 2514   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF39                 2516   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF40                 2518   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF41                 2520   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF42                 2522   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF43                 2524   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF44                 2526   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF45                 2528   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF46                 2530   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF47                 2532   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF48                 2534   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF49                 2536   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF50                 2538   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF51                 2540   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF52                 2542   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF53                 2544   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF54                 2546   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF55                 2548   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF56                 2550   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF57                 2552   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF58                 2554   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF59                 2556   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF60                 2558   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF61                 2560   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF62                 2562   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF63                 2564   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF64                 2566   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF65                 2568   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF66                 2570   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF67                 2572   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF68                 2574   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF69                 2576   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF70                 2578   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF71                 2580   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF72                 2582   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF73                 2584   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF74                 2586   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF75                 2588   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF76                 2590   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF77                 2592   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF78                 2594   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF79                 2596   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER2_COEF80                 2598   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF1                  2600   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF2                  2602   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF3                  2604   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF4                  2606   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF5                  2608   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF6                  2610   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF7                  2612   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF8                  2614   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF9                  2616   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF10                 2618   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF11                 2620   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF12                 2622   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF13                 2623   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF14                 2624   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF15                 2626   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF16                 2628   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF17                 2630   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF18                 2632   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF19                 2634   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF20                 2636   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF21                 2638   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF22                 2640   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF23                 2642   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF24                 2644   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF25                 2646   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF26                 2648   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF27                 2650   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF28                 2652   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF29                 2654   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF30                 2656   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF31                 2658   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF32                 2660   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF33                 2662   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF34                 2664   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF35                 2666   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF36                 2668   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF37                 2670   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF38                 2672   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF39                 2674   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF40                 2676   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF41                 2678   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF42                 2680   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF43                 2682   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF44                 2684   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF45                 2686   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF46                 2688   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF47                 2690   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF48                 2692   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF49                 2694   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF50                 2696   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF51                 2698   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF52                 2700   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF53                 2702   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF54                 2704   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF55                 2706   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF56                 2708   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF57                 2710   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF58                 2712   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF59                 2714   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF60                 2716   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF61                 2718   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF62                 2720   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF63                 2722   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF64                 2724   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF65                 2726   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF66                 2728   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF67                 2730   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF68                 2732   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF69                 2734   // BYTE (8-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF70                 2736   // WORD (16-Bit) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF71                 2738   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF72                 2740   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF73                 2742   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF74                 2744   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF75                 2746   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF76                 2748   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF77                 2750   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF78                 2752   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF79                 2754   // WORD (16-Bit)
#define EOLLIB_OFFSET_PHFTR_VOICE_SERVER3_COEF80                 2756   // WORD (16-Bit)
#define EOLLIB_OFFSET_RESERVED_BYTE_1_HF_TUNING                  2758   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_HF_TUNING                  2759   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_HF_TUNING                  2760   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_HF_TUNING                  2761   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_HF_TUNING                  2762   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_HF_TUNING                  2763   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_HF_TUNING                  2764   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_HF_TUNING                  2765   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_HF_TUNING                  2766   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_HF_TUNING                 2767   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_HF_TUNING                  2768   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_HF_TUNING                  2770   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_HF_TUNING                  2772   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_HF_TUNING                  2774   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_HF_TUNING                  2776   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_HF_TUNING                  2778   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_HF_TUNING                  2780   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_HF_TUNING                  2782   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_HF_TUNING                  2784   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_HF_TUNING                 2786   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_HF_TUNING                 2790   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_HF_TUNING                 2794   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END                      2985   // max. limit

//RVC
#define EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_FILE_HMI_REAR_VISION_CAL   0   // BYTE (Boolean)
#define EOLLIB_OFFSET_RVS_PRESENT_STATUS                               1   // BYTE (Boolean) --> Content of attributes was MODIFIED
#define EOLLIB_OFFSET_RVS_TEXT_LOCATION                                2   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_FEATURE_AVAIL_GUIDELINES                     3   // BYTE (8-Bit)
#define EOLLIB_OFFSET_RVS_FEATURE_AVAIL_UPA_SYMBOLS                    4   // BYTE (8-Bit)
#define EOLLIB_OFFSET_REAR_CAMERA_SETTING_MASK                         5   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_DTC_MASK_BYTE_REAR_CAMERA                        6   // BYTE (8-Bit-Mask)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_FRONT_CAMERA                  7   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_FRONT_CAMERA              8   // BYTE (Boolean)
#define EOLLIB_OFFSET_ENABLE_APPLICATION_VIDEO_RECORDER                9   // BYTE (Boolean) --> MOVED from COUNTRY Block
#define EOLLIB_OFFSET_ENABLE_APPLICATIONTRAY_VIDEO_RECORDER            10   // BYTE (Boolean) --> MOVED from COUNTRY Block
#define EOLLIB_OFFSET_RESERVED_BYTE_1_REAR_VIEW_CAMERA                 11   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_2_REAR_VIEW_CAMERA                 12   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_3_REAR_VIEW_CAMERA                 13   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_4_REAR_VIEW_CAMERA                 14   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_5_REAR_VIEW_CAMERA                 15   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_6_REAR_VIEW_CAMERA                 16   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_7_REAR_VIEW_CAMERA                 17   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_8_REAR_VIEW_CAMERA                 18   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_9_REAR_VIEW_CAMERA                 19   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_BYTE_10_REAR_VIEW_CAMERA                20   // BYTE (8-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_1_REAR_VIEW_CAMERA                 22   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_2_REAR_VIEW_CAMERA                 24   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_3_REAR_VIEW_CAMERA                 26   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_4_REAR_VIEW_CAMERA                 28   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_5_REAR_VIEW_CAMERA                 30   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_6_REAR_VIEW_CAMERA                 32   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_7_REAR_VIEW_CAMERA                 34   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_8_REAR_VIEW_CAMERA                 36   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_9_REAR_VIEW_CAMERA                 38   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_WORD_10_REAR_VIEW_CAMERA                40   // WORD (16-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_1_REAR_VIEW_CAMERA                42   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_RESERVED_DWORD_2_REAR_VIEW_CAMERA                46   // DWORD (32-Bit) --> NEW
#define EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END                     185  // max. limit

#define EOLLIB_DEFAULT_ENTRY(a) {EOLLIB_OFFSET_ ## a, &a, sizeof(a)}

extern const EOLLib_Entry EOLLib_arSystem[];
extern const EOLLib_Entry EOLLib_arDisplay[];
extern const EOLLib_Entry EOLLib_arBluetooth[];
extern const EOLLib_Entry EOLLib_arNavSystem[];
extern const EOLLib_Entry EOLLib_arNavIcon[];
extern const EOLLib_Entry EOLLib_arBrand[];
extern const EOLLib_Entry EOLLib_arCountry[];
extern const EOLLib_Entry EOLLib_arSpeechRec[];
extern const EOLLib_Entry EOLLib_arHfTuning[];
extern const EOLLib_Entry EOLLib_arRVC[];


#endif

