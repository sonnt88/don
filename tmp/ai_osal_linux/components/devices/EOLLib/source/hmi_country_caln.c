
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#include <EOLLib.h>

/*
MOD ID 08 - HMI COUNTRY CALN
*/

/*
*| hmi_country_caln.HEADER_COUNTRY {
*|      : is_calconst;
*|      : description = "CALDS Header";
*| } 
*/ 
const EOLLib_CalDsHeader HEADER_COUNTRY = {0x0000, EOLLIB_TABLE_ID_COUNTRY << 8, 0x0000, 0x00000000, {0x41, 0x41}, 0x0401};

/*
*| hmi_country_caln.DRIVE_SIDE {
*|      : is_calconst;
*|      : description = "DRIVE_SIDE, to comprehend vehicle configuration of Right Hand Drive (RHD) versus Left Hand Drive (LHD) with respect to GMLAN message location of driver front versus passenger front. ";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DRIVE_SIDE = 0x00;

/*
*| hmi_country_caln.MARKETING_REGION {
*|      : is_calconst;
*|      : description = "The contents of the Audio More Features are based on Regions.";
*|      : units = "Enumeration";
*|      : type = fixed.MARKETING_REGION;
*| } 
*/ 
const char MARKETING_REGION = 0x00;

/*
*| hmi_country_caln.FM_RDS_INFORMATION_REGION {
*|      : is_calconst;
*|      : description = "Documentation update for global calibrator clarity until a future FCAT communicates RDS type and presence to HMI.\
FM_RDS_INFORMATION_REGION\
Add description for calibrations.\
$0 = North America {USA, Canada, Mexico, Australia}\
$1 = Europe {European countries}\
$2 - OFF { CHINA, JAPAN, KOREA, MIDDLE EAST, RUSSIA, ISRAEL, AFRICA, CENTRAL AMERICA, SOUTH AMERICA}\
$3 - $F = Undefined (default to North America)\
\
refer to FBS, 9.3.43.2 vs. 9.3.43.4\
how will HMI get to know the different sorting of list elements\
should be added to select EU or NAR variant, refer to GIS-068-102B PTY Table\
\
Also reference GIS-338-101A.  (Program service name) display is based on this calibration per MY2014 Workshop.  \
Also determines RDS PTY station grouping - GIS-338-102B PTY Table:  \
\
Note that tuner and HMI coordination is required per the design.\
\
Files on GDM (Global Document Management system) - See Link on General GM Training page for GDM.\
GMW16273 - GMW16273Apr2013.pdf - RDS ...";
*|      : units = "Enum";
*|      : type = fixed.FM_RDS_INFORMATION_REGION;
*| } 
*/ 
const char FM_RDS_INFORMATION_REGION = 0x00;

/*
*| hmi_country_caln.ENABLE_AM_FM_XM_AUDIOMOREFEATURE_TAGSONG {
*|      : is_calconst;
*|      : description = "AudioMoreFeatures\
This calibration enables or disables the respective HMI Settings menu and associated sub-menus.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AM_FM_XM_AUDIOMOREFEATURE_TAGSONG = 0x01;

/*
*| hmi_country_caln.ENABLE_AM_FM_DAB_XM_AUDIOMOREFEATURE_TIMESHIFT {
*|      : is_calconst;
*|      : description = "AudioMoreFeatures\
This calibration enables or disables the respective HMI Settings menu and associated sub-menus.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AM_FM_DAB_XM_AUDIOMOREFEATURE_TIMESHIFT = 0x01;

/*
*| hmi_country_caln.ENABLE_AM_FM_AUDIOMOREFEATURE_HD_SWITCHING_RADIO_ID {
*|      : is_calconst;
*|      : description = "This calibration REMOVES the HD RADIO ID and HD Switching ON/OFF from the SETTING MENU.  Calibration must be enabled when vehicle contains a HD hardware in the Tuner module.  Synchronization of RPO release strings is required with the TUNER hardware.\
\
AudioMoreFeatures\
This calibration enables or disables the respective HMI Settings menu and associated sub-menus.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AM_FM_AUDIOMOREFEATURE_HD_SWITCHING_RADIO_ID = 0x01;

/*
*| hmi_country_caln.ENABLE_AM_FM_DAB_AUDIOMOREFEATURE_UPDATESTATIONLIST {
*|      : is_calconst;
*|      : description = "AudioMoreFeatures\
This calibration enables or disables the respective HMI Settings menu and associated sub-menus.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AM_FM_DAB_AUDIOMOREFEATURE_UPDATESTATIONLIST = 0x01;

/*
*| hmi_country_caln.ENABLE_AM_AUDIOMOREFEATURE_TRAFFICINFORMATION_JAPAN {
*|      : is_calconst;
*|      : description = "AudioMoreFeatures\
This calibration enables or disables the respective HMI Settings menu and associated sub-menus.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AM_AUDIOMOREFEATURE_TRAFFICINFORMATION_JAPAN = 0x00;

/*
*| hmi_country_caln.ENABLE_XM_AUDIOMOREFEATURE_TUNESELECT {
*|      : is_calconst;
*|      : description = "AudioMoreFeatures\
This calibration enables or disables the respective HMI Settings menu and associated sub-menus.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_XM_AUDIOMOREFEATURE_TUNESELECT = 0x01;

const char COUNTRY_DUMMY_1[3] =
 {
  0x00, 0x00, 0x00
 }
;

/*
*| hmi_country_caln.MICROPHONE_1_CONFIG {
*|      : is_calconst;
*|      : description = "Primary microphone input.  Use in DTC Failure Criteria and to control the mic configuration\
\
Examples:\
Microphone 1 = $01 for OnStar configuration (mic directly connects to OnStar then passthru to HMI)\
Microphone 1 = $03 for non-OnStar (mic directly connected to HMI module for speech)";
*|      : units = "ENUM";
*|      : type = fixed.MICROPHONE_1_CONFIG;
*| } 
*/ 
const char MICROPHONE_1_CONFIG = 0x01;

const char COUNTRY_DUMMY_2 = 0x00;

/*
*| hmi_country_caln.TODC_VOLTAGE_DISPLAY {
*|      : is_calconst;
*|      : description = "TODC_Voltage_Display \
 $0 � 120V & 240V  North America\
 $1 � 120V only N/A\
 $2 � 220V only China, Korea\
 $3 � 230V only Europe\
 $4 � 240V only Australia, New Zealand\
 $5 - Text Only\
\
The charge start and end times shall vary by region and supply voltage. The silverbox shall use the calibration TODC_Voltage_Display to determine the voltage that should be displayed for each region. \
Calibration Name Enumeration Region";
*|      : units = "Enumeration";
*|      : type = fixed.TODC_VOLTAGE_DISPLAY;
*| } 
*/ 
const char TODC_VOLTAGE_DISPLAY = 0x00;

/*
*| hmi_country_caln.RT_TRAFFIC_SOURCE {
*|      : is_calconst;
*|      : description = "Real Time Traffic data type/source";
*|      : units = "Enumeration";
*|      : type = fixed.RT_TRAFFIC_SOURCE;
*| } 
*/ 
const char RT_TRAFFIC_SOURCE = 0x01;

const char COUNTRY_DUMMY_37 = 0x00;

const char COUNTRY_DUMMY_3 = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATION_ONSTAR {
*|      : is_calconst;
*|      : description = "HomeScreen Applications\
Dual purpose calibration:  Determines OnStar icon for 8\" systems AND also is part of the logic to determine when to show the NAV icon on the home screen.\
For OnStar icon on Home screen:\
The 4.2� HMI will NOT show an Onstar icon but the 8� HMIs will show an icon per this cal.  \
\
For NAV icon on Home screen:\
[SYSTEM_CONFIGURATION = NAV] \
OR \
[ENABLE_APPLICATON_ONSTAR=Enable\
AND SYSTEM_CONFIGURATION=COLOR or CONNECTIVITY]";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_ONSTAR = 0x01;

/*
*| hmi_country_caln.ENABLE_BAND_SOURCE_INTERNET_RADIO {
*|      : is_calconst;
*|      : description = "The List of all available bradcast audio sources can be cycled through by pressing the BAND button on the interaction selector. The sources will rotate in the following order: AM, FM, XM, DAB (when Applicable), Internet Radio.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_BAND_SOURCE_INTERNET_RADIO = 0x00;

/*
*| hmi_country_caln.XM_ADVISORY_MSG_TIMEOUT {
*|      : is_calconst;
*|      : description = "�.this Advisor is displayed for 2 seconds and thent he user is returned to the previous channel. If the Previous channel is not available, the system will tune to XM Channel 1";
*|      : units = "ms";
*|      : transform = fixed.XM_ADVISORY_MSG_TIMEOUT;
*| } 
*/ 
const char XM_ADVISORY_MSG_TIMEOUT = 0x14;

const char COUNTRY_DUMMY_4 = 0x00;

/*
*| hmi_country_caln.XM_NO_CAT_FOUND_MSG_TIMEOUT {
*|      : is_calconst;
*|      : description = "The Metadata field will display \"CAT Not Found\" when there are no channels available for a selected category. This information will timeout after 3 seconds (Calibratble) or disapear with user input.";
*|      : units = "ms";
*|      : transform = fixed.XM_NO_CAT_FOUND_MSG_TIMEOUT;
*| } 
*/ 
const char XM_NO_CAT_FOUND_MSG_TIMEOUT = 0x1E;

/*
*| hmi_country_caln.ENABLE_XM_LIST_VIEW_CHANNELVIEW {
*|      : is_calconst;
*|      : description = "The XM Tuning List can be viewed in one of three different ways based on the the user's selection of the VIEW button.  This Calibration enables or disables a particular View.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_XM_LIST_VIEW_CHANNELVIEW = 0x01;

/*
*| hmi_country_caln.ENABLE_XM_LIST_VIEW_ARTISTVIEW {
*|      : is_calconst;
*|      : description = "The XM Tuning List can be viewed in one of three different ways based on the the user's selection of the VIEW button.  This Calibration enables or disables a particular View.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_XM_LIST_VIEW_ARTISTVIEW = 0x01;

/*
*| hmi_country_caln.ENABLE_XM_LIST_VIEW_SONGVIEW {
*|      : is_calconst;
*|      : description = "The XM Tuning List can be viewed in one of three different ways based on the the user's selection of the VIEW button.  This Calibration enables or disables a particular View.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_XM_LIST_VIEW_SONGVIEW = 0x01;

/*
*| hmi_country_caln.ENABLE_SCENIC_ROUTE_DEPARTURE_THRESHOLD {
*|      : is_calconst;
*|      : description = "Cal to define the scenic route ideparture threshold";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_SCENIC_ROUTE_DEPARTURE_THRESHOLD = 0x01;

const char COUNTRY_DUMMY_11 = 0x00;

/*
*| hmi_country_caln.FM_RDS_SEEK_TUNING_MSG_TIME {
*|      : is_calconst;
*|      : description = "we will need to add a FM_RDS_SEEK_TUNING_MSG_TIME calibration, 0 to 5 seconds, to all HMI to display a tuning message for RDS tuning for EU markets";
*|      : units = "ms";
*|      : transform = fixed.FM_RDS_SEEK_TUNING_MSG_TIME;
*| } 
*/ 
const char FM_RDS_SEEK_TUNING_MSG_TIME = 0x96;

const char COUNTRY_DUMMY_5[6] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_country_caln.SUPPLEMENTAL_MICROPHONE_DELAY {
*|      : is_calconst;
*|      : description = "This is the microphone delay time between either a MOST Microphone or the analog Mic2 to MIC1";
*|      : units = "ms";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SUPPLEMENTAL_MICROPHONE_DELAY = 0x00;

const char COUNTRY_DUMMY_12 = 0x00;

/*
*| hmi_country_caln.VIN_RELEARN_EN {
*|      : is_calconst;
*|      : description = "A shown in the diagram on the previous page, the internal ModuleTheftLockedFlag within a component may only be cleared upon receipt of the evClearVINCPID event.  This event is generated in response to the VIN clearing CPID $FE that is sent to the Silverbox from a service tool and gated onto the GMLAN network by the GMLANReceiveGateway.  \
Per the diagram above, note that clearing back to a NoVIN condition is guarded by each components Internal Manufacturing Enable Counter or MEC as well as a VIN_RELEARN_EN calibration that is used to bypass VIN learning altogether in particular regions where Theftlock is not required but the same end model part number is used for a component. \
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char VIN_RELEARN_EN = 0x01;

/*
*| hmi_country_caln.LANGUAGE_ENABLE_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LANGUAGE_ENABLE_MASK_BYTE_1 = 0x80;

/*
*| hmi_country_caln.LANGUAGE_ENABLE_MASK_BYTE_2 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LANGUAGE_ENABLE_MASK_BYTE_2 = 0x00;

/*
*| hmi_country_caln.LANGUAGE_ENABLE_MASK_BYTE_3 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LANGUAGE_ENABLE_MASK_BYTE_3 = 0x00;

/*
*| hmi_country_caln.LOCKOUT_SPEED {
*|      : is_calconst;
*|      : description = "The system shall be capable of making specific manual controls, both touch screen and faceplate, inoperable within the user interface (e.g., �gray out�, ignore face�plate/SWC switch input, etc.) based on vehicle speed, transmission status, or park brake status. ";
*|      : units = "kph";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LOCKOUT_SPEED = 0x08;

/*
*| hmi_country_caln.LOCKOUT_PRNDL_SPEED {
*|      : is_calconst;
*|      : description = "The system shall be capable of making specific manual controls, both touch screen and faceplate, inoperable within the user interface (e.g., �gray out�, ignore face�plate/SWC switch input, etc.) based on vehicle speed, transmission status, or park brake status.   When this calibration is set to $0 Unlock in PARK, the System shall enable dwl lockouts when the shifter is out of PARK gear.  If the Calibration is set to $1 Unlock in Drive, then the sysetem shall look at the speed lockout calibration to determine at which speed should be used to enable the DWL lockouts.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char LOCKOUT_PRNDL_SPEED = 0x01;

/*
*| hmi_country_caln.UNLOCK_DWL_LOCKOUTS_BYTE1 {
*|      : is_calconst;
*|      : description = "Lockouts could be disabled when the following condi�tions are true or have occurred. The system must ac�count for these conditions through calibrations.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char UNLOCK_DWL_LOCKOUTS_BYTE1 = 0x84;

/*
*| hmi_country_caln.DWL_AM_FM_MASK_BYTE {
*|      : is_calconst;
*|      : description = "Driver Workload for AM & FM Tuner pages";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_AM_FM_MASK_BYTE = 0x00;

/*
*| hmi_country_caln.DWL_OWNERS_MANUAL_MASK_BYTE {
*|      : is_calconst;
*|      : description = "Driver Workload for Owners_Manual pages";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_OWNERS_MANUAL_MASK_BYTE = 0x00;

/*
*| hmi_country_caln.DWL_XM_DAB_DMB_MASK_BYTE {
*|      : is_calconst;
*|      : description = "Driver Workload for XM, DAB, DMB Tuner pages";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_XM_DAB_DMB_MASK_BYTE = 0x00;

/*
*| hmi_country_caln.DWL_INTERNET_RADIO_MASK_BYTE1 {
*|      : is_calconst;
*|      : description = "Driver Workload for Internet Radio pages";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_INTERNET_RADIO_MASK_BYTE1 = 0x00;

/*
*| hmi_country_caln.DWL_PANDORA_MASK_BYTE1 {
*|      : is_calconst;
*|      : description = "Driver Workload for Calendar pages";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_PANDORA_MASK_BYTE1 = 0x00;

/*
*| hmi_country_caln.DWL_ALL_NAVIGATION_MASK_BYTE {
*|      : is_calconst;
*|      : description = "Navigation Menu Lock Outs";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_ALL_NAVIGATION_MASK_BYTE = 0x60;

/*
*| hmi_country_caln.DWL_NAV_WEATHER_MAP_SCROLLING {
*|      : is_calconst;
*|      : description = "Map Scrolling Limits";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DWL_NAV_WEATHER_MAP_SCROLLING = 0x01;

const char COUNTRY_DUMMY_6 = 0x00;

/*
*| hmi_country_caln.DWL_NAV_PREVIOUS_DEST {
*|      : is_calconst;
*|      : description = "Previous Dest Limits";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_NAV_PREVIOUS_DEST = 0x1C;

/*
*| hmi_country_caln.DWL_NAV_CONTACTS {
*|      : is_calconst;
*|      : description = "Contacts Dest Limits";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_NAV_CONTACTS = 0x60;

/*
*| hmi_country_caln.DWL_NAV_POI_LISTS {
*|      : is_calconst;
*|      : description = "POI List Destination Limits";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_NAV_POI_LISTS = 0x7C;

const char COUNTRY_DUMMY_7[2] =
 {
  0x00, 0x00
 }
;

/*
*| hmi_country_caln.DWL_NAV_MAP_MENU {
*|      : is_calconst;
*|      : description = "Navigation prefrences Limits";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_NAV_MAP_MENU = 0x00;

/*
*| hmi_country_caln.DWL_NAV_TURN_LISTS {
*|      : is_calconst;
*|      : description = "Navigation Turn List Limits";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_NAV_TURN_LISTS = 0x00;

/*
*| hmi_country_caln.DWL_MEDIA {
*|      : is_calconst;
*|      : description = "Driver Workload for Optical Drives, Discrete USB/SD, Discrete BT Devices, Bluetooth Music, and My Media";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_MEDIA = 0x01;

/*
*| hmi_country_caln.DWL_RSE {
*|      : is_calconst;
*|      : description = "Driver Workload for RSE.  The Global CALIBRATIN \" LOCKOUT_PRNDL_SPEED\" Shall NOT be utilized for RSE DWL.  RSE DWL Shall ONLY use the PARK information.  There is a USA regulatroy requirement for RSE Video / TV to be shown while Driving at any speed. ";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_RSE = 0x01;

/*
*| hmi_country_caln.DWL_CUSTOMIZATION_SETTINGS_BYTE1 {
*|      : is_calconst;
*|      : description = "Customization Driver Workload Lockouts";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_CUSTOMIZATION_SETTINGS_BYTE1 = 0x0A;

/*
*| hmi_country_caln.DWL_CUSTOMIZATION_SETTINGS_BYTE2 {
*|      : is_calconst;
*|      : description = "Customization Driver Workload Lockouts";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_CUSTOMIZATION_SETTINGS_BYTE2 = 0x00;

/*
*| hmi_country_caln.DWL_PHONE_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "Phone Driver Workload Lockouts: Either Bluetooth or Onstar";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_PHONE_MASK_BYTE_1 = 0x41;

/*
*| hmi_country_caln.DWL_MMS_SMS_MASK_BYTE1 {
*|      : is_calconst;
*|      : description = "Messaging Driver Workload Lockouts\
Greys out the Text Message menu list item in the Phone Home screen.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_MMS_SMS_MASK_BYTE1 = 0x80;

/*
*| hmi_country_caln.DWL_EMAIL_MASK_BYTE1 {
*|      : is_calconst;
*|      : description = "Messaging Driver Workload Lockouts";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_EMAIL_MASK_BYTE1 = 0x80;

const char COUNTRY_DUMMY_13 = 0x00;

/*
*| hmi_country_caln.DWL_WEATHER_MASK_BYTE1 {
*|      : is_calconst;
*|      : description = "Driver Workload for Weather pages";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_WEATHER_MASK_BYTE1 = 0x5C;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_US_ENGILSH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_US_ENGILSH = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_GERMAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_GERMAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_ITALIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_ITALIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_SWEDISH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_SWEDISH = 0x02;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_FRENCH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_FRENCH = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_SPANISH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_SPANISH = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_DUTCH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_DUTCH = 0x02;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_PORTUGUESE {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_PORTUGUESE = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_NORWEGIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_NORWEGIAN = 0x02;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_FINNISH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_FINNISH = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_DANISH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_DANISH = 0x02;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_GREEK {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_GREEK = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_JAPANESE {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_JAPANESE = 0x02;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_ARABIC {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_ARABIC = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_STANDARD_CHINESE {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_STANDARD_CHINESE = 0x02;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_POLISH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_POLISH = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_TURKISH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_TURKISH = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_KOREAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_KOREAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_TRADITIONAL_CHINESE {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_TRADITIONAL_CHINESE = 0x02;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_UK_ENGLISH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_UK_ENGLISH = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_HUNGARIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_HUNGARIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_CZECH {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_CZECH = 0x02;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_SLOVAK {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_SLOVAK = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_RUSSIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_RUSSIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_BRAZILIAN_PORTUGUESE {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_BRAZILIAN_PORTUGUESE = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_THAI {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_THAI = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_BULGARIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_BULGARIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_ROMANIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_ROMANIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_SLOVENIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_SLOVENIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_CROATIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_CROATIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_UKRAINIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_UKRAINIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_FRENCH_CANADIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_FRENCH_CANADIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_SPANISH_LATIN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_SPANISH_LATIN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_US_ENGLISH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_US_ENGLISH = 0x01;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_GERMAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_GERMAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ITAILIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ITAILIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SWEDISH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SWEDISH = 0x02;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_FRENCH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_FRENCH = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SPANISH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SPANISH = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_DUTCH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_DUTCH = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_PORTUGUESE {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_PORTUGUESE = 0x00;

/*
*| hmi_country_caln.ENABLE_12_24_CLOCK {
*|      : is_calconst;
*|      : description = "There needs to be a Calibration that enables or disables the Clock completely from the HMI.  This is when there is an analog clock installed in the vehicle and the Program Teams don't want the digital clock shown in the HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_12_24_CLOCK = 0x00;

const char COUNTRY_DUMMY_14 = 0x00;

const char COUNTRY_DUMMY_15 = 0x00;

const char COUNTRY_DUMMY_16 = 0x00;

/*
*| hmi_country_caln.DOPPLER_MAP_DISPLAY {
*|      : is_calconst;
*|      : description = "The North-up Doppler map shows cloud cover for the current location with state and city information as shown in the navigation map. If any weather advisories are in effect, they are shown on the map\
\
\
If the user pans the map to another location, the fore�cast information below is updated to reflect that loca�tion";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DOPPLER_MAP_DISPLAY = 0x00;

/*
*| hmi_country_caln.DEFAULT_DOPPLER_MAP_SCALE {
*|      : is_calconst;
*|      : description = "If the user has panned the map to a location away from the current vehicle location, the map reset button is shown and allows the user to reset the map to the cur�rent vehicle location and at a set zoom level of 16 miles";
*|      : units = "Scale _Setting";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DEFAULT_DOPPLER_MAP_SCALE = 0x0A;

const char COUNTRY_DUMMY_8 = 0x00;

/*
*| hmi_country_caln.ENABLE_ONSTAR_REGIONAL_BRANDING_STRING {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_ONSTAR_REGIONAL_BRANDING_STRING = 0x01;

const char COUNTRY_DUMMY_9[10] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_country_caln.ONSTAR_EMG_CALL_END_BUTTON {
*|      : is_calconst;
*|      : description = "may remove End when it is an emergency call with the airbag deployed";
*|      : units = "Enumeration";
*|      : type = fixed.ONSTAR_EMG_CALL_END_BUTTON;
*| } 
*/ 
const char ONSTAR_EMG_CALL_END_BUTTON = 0x00;

/*
*| hmi_country_caln.ONSTAR_REGIONAL_BRAND_HMI_LOGO {
*|      : is_calconst;
*|      : description = "The alert shows the incoming call is from the OnStar Advisor along with the OnStar icon. The OnStar symbol (calibratible) may be different based upon the region.";
*|      : units = "ENM";
*|      : type = fixed.ONSTAR_REGIONAL_BRAND_HMI_LOGO;
*| } 
*/ 
const char ONSTAR_REGIONAL_BRAND_HMI_LOGO = 0x01;

/*
*| hmi_country_caln.ONSTAR_ICON_ONSTAR_NA {
*|      : is_calconst;
*|      : description = "Logo Graphics for Onstar - North America Systems";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ONSTAR_ICON_ONSTAR_NA = 0xF8;

/*
*| hmi_country_caln.ONSTAR_ICON_CHINA_STAR {
*|      : is_calconst;
*|      : description = "Logo Graphics for Onstar - China Based Systems";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ONSTAR_ICON_CHINA_STAR = 0xF8;

/*
*| hmi_country_caln.ONSTAR_ICON_EUROPE_STAR {
*|      : is_calconst;
*|      : description = "Logo Graphics for OnStar - Europe Based Systems";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ONSTAR_ICON_EUROPE_STAR = 0xF8;

/*
*| hmi_country_caln.ONSTAR_ICON_CHEVY_STAR {
*|      : is_calconst;
*|      : description = "Logo Graphics for OnStar - Chevy Star Based System";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ONSTAR_ICON_CHEVY_STAR = 0xF8;

const char COUNTRY_DUMMY_10 = 0x00;

/*
*| hmi_country_caln.LVM_1 {
*|      : is_calconst;
*|      : description = "Low Volume Module 1";
*|      : units = "Enumeration";
*|      : type = fixed.LVM;
*| } 
*/ 
const char LVM_1 = 0x00;

/*
*| hmi_country_caln.LVM_2 {
*|      : is_calconst;
*|      : description = "Low Volume Module 2";
*|      : units = "Enumeration";
*|      : type = fixed.LVM;
*| } 
*/ 
const char LVM_2 = 0x00;

/*
*| hmi_country_caln.LVM_3 {
*|      : is_calconst;
*|      : description = "Low Volume Module 3";
*|      : units = "Enumeration";
*|      : type = fixed.LVM;
*| } 
*/ 
const char LVM_3 = 0x00;

/*
*| hmi_country_caln.SWMI_CALIBRATION_DATA_FILE_COUNTRY_CAL {
*|      : is_calconst;
*|      : description = "The NoCalibration state is defined in order to make sure that Infotainment subsystem components have been updated with calibrations after a service event such as replacing one or more modules or upgrading software.  The mechanism for NoCalibration determination is the same as that for Theftlock and NoVIN determination.  That is, at initialization a module detects whether or not it has a valid calibration.  If it does not, it notifies the SystemState FBlock via the SystemState.SetNoCalibrationModuleState method.  Once a valid calibration is received, the module calls the SystemState.SetNoCalibrationModuleState method to clear the condition.\
The SystemState FBlock shall assume all module calibrations are valid upon each Sleep cycle.  Modules that already have a valid calibration or do not support NoCalibration detection (determined by each module CTS) do not need to report the NoCalibration clear condition at each initialization.\
Note: each of these Calibrations should be ...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SWMI_CALIBRATION_DATA_FILE_COUNTRY_CAL = 0x00;

/*
*| hmi_country_caln.ENABLE_AUTO_SET_BUTTON {
*|      : is_calconst;
*|      : description = "The System indicates AutoSet On by displaying the Auto Set Indicator Button As The Selected State. Tapping on the Auto Set Button will bring up the Auto Set Menu where the user can select an Auto Set On Source or Manually turn AutoSet Off.  The menu should display only those sources calibrated as available.\
\
26JUN2013 - This cal is only enabled for certain countries.  If the vehicle is made in the U.S. or if the country of sale is indeterminate, this cal shall be disabled. If the country of sale is known, this cal shall be turned on for countries that get this feature per GMW16273.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUTO_SET_BUTTON = 0x01;

/*
*| hmi_country_caln.ENABLE_AUTO_SET_MENU_SELECTION_CELL_NETWORK {
*|      : is_calconst;
*|      : description = "The System indicates AutoSet On by displaying the Auto Set Indicator Button As The Selected State. Tapping on the Auto Set Button will bring up the Auto Set Menu where the user can select an Auto Set On Source or Manually turn AutoSet Off.  The menu shoudl display only those sources calibrated as available.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUTO_SET_MENU_SELECTION_CELL_NETWORK = 0x01;

/*
*| hmi_country_caln.ENABLE_AUTO_SET_MENU_SELECTION_RDS {
*|      : is_calconst;
*|      : description = "The System indicates AutoSet On by displaying the Auto Set Indicator Button As The Selected State. Tapping on the Auto Set Button will bring up the Auto Set Menu where the user can select an Auto Set On Source or Manually turn AutoSet Off.  The menu should display only those sources calibrated as available.\
\
26JUN2013 - This cal is only enabled for certain countries.  If the vehicle is made in the U.S. or if the country of sale is indeterminate, this cal shall be disabled. If the country of sale is known, this cal shall be turned on for countries that get this feature per GMW16273.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUTO_SET_MENU_SELECTION_RDS = 0x00;

const char COUNTRY_DUMMY_17 = 0x00;

/*
*| hmi_country_caln.TEEN_MODE_CODE_FLASH_TIME {
*|      : is_calconst;
*|      : description = "Upon entering a 4�digit code and tapping �SET TEEN�, the PIN will flash for 3 seconds at a 1 hertz rate and then display the PIN solid ON. Upon exiting the Infotainment Teen Mode screen the PIN display shall be cleared and be blank the next time the Infotainment Teen Mode screen is entered.";
*|      : units = "ms";
*|      : transform = fixed.TEEN_MODE_CODE_FLASH_TIME;
*| } 
*/ 
const char TEEN_MODE_CODE_FLASH_TIME = 0x1E;

/*
*| hmi_country_caln.TEEN_MODE_CODE_FLASH_FREQUENCY {
*|      : is_calconst;
*|      : description = "Upon entering a 4�digit code and tapping �SET TEEN�, the PIN will flash for 3 seconds at a 1 hertz rate and then display the PIN solid ON. Upon exiting the Infotainment Teen Mode screen the PIN display shall be cleared and be blank the next time the Infotainment Teen Mode screen is entered. Flash frequency for successfully entered code.";
*|      : units = "Hz";
*|      : transform = fixed.TEEN_MODE_CODE_FLASH_FREQUENCY;
*| } 
*/ 
const unsigned short TEEN_MODE_CODE_FLASH_FREQUENCY = 0x000A;

const char COUNTRY_DUMMY_18 = 0x00;

const char COUNTRY_DUMMY_19 = 0x00;

const char COUNTRY_DUMMY_34 = 0x00;

const char COUNTRY_DUMMY_20 = 0x00;

const char COUNTRY_DUMMY_21 = 0x00;

/*
*| hmi_country_caln.LANGUAGE_ENABLE_MASK_BYTE_4 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LANGUAGE_ENABLE_MASK_BYTE_4 = 0x01;

/*
*| hmi_country_caln.LANGUAGE_ENABLE_MASK_BYTE_5 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LANGUAGE_ENABLE_MASK_BYTE_5 = 0x80;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_NORWEGIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_NORWEGIAN = 0x02;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_FINNISH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_FINNISH = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_DANISH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_DANISH = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_GREEK {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_GREEK = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_JAPANESE {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_JAPANESE = 0x02;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ARABIC {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ARABIC = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_STANDARD_CHINESE {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_STANDARD_CHINESE = 0x02;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_POLISH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_POLISH = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_TURKISH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_TURKISH = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_KOREAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_KOREAN = 0x02;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_TRADITIONAL_CHINESE {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_TRADITIONAL_CHINESE = 0x02;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_UK_ENGLISH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_UK_ENGLISH = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_HUNGARIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_HUNGARIAN = 0x02;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_CZECH {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_CZECH = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SLOVAK {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SLOVAK = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_RUSSIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_RUSSIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_BRAZILIAN_PORTUGUESE {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_BRAZILIAN_PORTUGUESE = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_THAI {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_THAI = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_BULGARIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_BULGARIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ROMANIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ROMANIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SLOVENIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SLOVENIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_CROATIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_CROATIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_UKRAINIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_UKRAINIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_FRENCH_CANADIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_FRENCH_CANADIAN = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SPANISH_LATIN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_SPANISH_LATIN = 0x00;

/*
*| hmi_country_caln.TOUCHPAD_CHARACTER_RECOGNITION_VEHICLE_SPEED_LOCKOUT_ENABLE {
*|      : is_calconst;
*|      : description = "Vehicle Speed lockouts enable/disable for character recognition (both touchPad and Front touchscreen display)";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char TOUCHPAD_CHARACTER_RECOGNITION_VEHICLE_SPEED_LOCKOUT_ENABLE = 0x01;

/*
*| hmi_country_caln.DAB_TUNING_MSG_TIME {
*|      : is_calconst;
*|      : description = " to display a minimum display time for the DAB for EU markets.  Similar function as FM_RDS_SEEK_TUNING_MSG_TIME but for DAB.";
*|      : units = "ms";
*|      : transform = fixed.DAB_TUNING_MSG_TIME;
*| } 
*/ 
const char DAB_TUNING_MSG_TIME = 0x64;

/*
*| hmi_country_caln.ONSTAR_REGIONAL_BRANDING_STRING {
*|      : is_calconst;
*|      : description = "Onstar Regional brand text display for the Phone Main pages.  Max 10 characters.";
*|      : units = "characters";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short ONSTAR_REGIONAL_BRANDING_STRING[10] =
 {
  0x004F, 0x006E, 0x0053, 0x0074, 0x0061, 0x0072, 0x0000, 0x0000,
  0x0000, 0x0000
 }
;

/*
*| hmi_country_caln.INTERNET_BROWSER_FBLOCK_MANAGER_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "Application files are stored by the HMI module.  Before any file is stored by the HMI module, it must be verified to have come from a trusted source (i.e. GM Back Office).\
An �application� consists of locally and/or remotely stored files, scripts, and data.  The locally stored components include source HTML files and static external resources, such as images, JavaScript scripts, and cascading style sheets. The files stored locally on the HMI module access off-board data through asynchronous JavaScript requests.\
";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char INTERNET_BROWSER_FBLOCK_MANAGER_MASK_BYTE_1 = 0xDD;

/*
*| hmi_country_caln.GM_BACK_OFFICE_BASEURL {
*|      : is_calconst;
*|      : description = "The base URL of the GM Back Office shall be calibrateable and is specified in GIS-344 Infotainment Subsystem EOL Calibration Specification.  ";
*|      : units = "UTF-16";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short GM_BACK_OFFICE_BASEURL[40] =
 {
  0x0073, 0x0066, 0x0070, 0x002E, 0x006F, 0x0062, 0x006F, 0x0073,
  0x0065, 0x0072, 0x0076, 0x0069, 0x0063, 0x0065, 0x0073, 0x002E,
  0x006D, 0x006F, 0x0062, 0x0069, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

const char COUNTRY_DUMMY_27 = 0x00;

const char COUNTRY_DUMMY_28 = 0x00;

const char COUNTRY_DUMMY_29 = 0x00;

const char COUNTRY_DUMMY_30 = 0x00;

/*
*| hmi_country_caln.NETWORK_CONNECTION_MANAGEMENT_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "The HMI Module shall be able to connect to an external network using all of the following connection methods ";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char NETWORK_CONNECTION_MANAGEMENT_MASK_BYTE_1 = 0x0F;

/*
*| hmi_country_caln.INTERNET_RADIO_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char INTERNET_RADIO_MASK_BYTE_1 = 0x80;

const unsigned short COUNTRY_DUMMY_22[40] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

/*
*| hmi_country_caln.SIGNING_CERTIFICATE_ISSUER {
*|      : is_calconst;
*|      : description = "ASCII String representing the certificate per GIS-358\
default values:�CN=GM Code Signature Server, OU=GM NA INFOTAINMENT, O=GM�";
*|      : units = "UTF-16";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short SIGNING_CERTIFICATE_ISSUER[64] =
 {
  0x0043, 0x004E, 0x003D, 0x0047, 0x004D, 0x0020, 0x0043, 0x006F,
  0x0064, 0x0065, 0x0020, 0x0053, 0x0069, 0x0067, 0x006E, 0x0061,
  0x0074, 0x0075, 0x0072, 0x0065, 0x0020, 0x0053, 0x0065, 0x0072,
  0x0076, 0x0065, 0x0072, 0x002C, 0x0020, 0x004F, 0x0055, 0x003D,
  0x0047, 0x004D, 0x0020, 0x004E, 0x0041, 0x0020, 0x0049, 0x004E,
  0x0046, 0x004F, 0x0054, 0x0041, 0x0049, 0x004E, 0x004D, 0x0045,
  0x004E, 0x0054, 0x002C, 0x0020, 0x004F, 0x003D, 0x0047, 0x004D,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

const unsigned short COUNTRY_DUMMY_23[192] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

/*
*| hmi_country_caln.SPELLER_CHINESE_SOGU_KEYBOARD_ENABLE {
*|      : is_calconst;
*|      : description = "Keyboard support for the Sogu Chinese Speller";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SPELLER_CHINESE_SOGU_KEYBOARD_ENABLE = 0x00;

const char COUNTRY_DUMMY_24 = 0x00;

/*
*| hmi_country_caln.BT_VEHICLE_MOVING_OVERIDE {
*|      : is_calconst;
*|      : description = "The vehicle moving value (in mph/kph) shall be set via calibration";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char BT_VEHICLE_MOVING_OVERIDE = 0x01;

/*
*| hmi_country_caln.DTC_MASK_BYTE_MIC1 {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_BYTE_MIC1 = 0x00;

/*
*| hmi_country_caln.ENABLE_SPEECHRECOGNITIONDOMAIN_ONSTARHANDOFF {
*|      : is_calconst;
*|      : description = "The user is able to command the system by using\
speech commands. The following domains are available\
for control using speech.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_SPEECHRECOGNITIONDOMAIN_ONSTARHANDOFF = 0x01;

const char COUNTRY_DUMMY_25 = 0x00;

/*
*| hmi_country_caln.FBLOCK_MESSAGE_MASK_ENABLE_INTERNETBROWSER_FBLOCK {
*|      : is_calconst;
*|      : description = "$0 = Disables FBLOCK $CA Internet\
$1 = Enables FBLOCK $CA Internet\
Similar to GMAN requriment of all Rx / Tx having a Enable / Disable Calibration";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char FBLOCK_MESSAGE_MASK_ENABLE_INTERNETBROWSER_FBLOCK = 0x01;

/*
*| hmi_country_caln.FBLOCK_MESSAGE_MASK_ENABLE_LVMINTERFACE_FBLOCK {
*|      : is_calconst;
*|      : description = "$0 = Disables FBLOCK 0xD3 - LVMInterface\
$1 = Enables FBLOCK 0xD3 - LVMInterface\
Similar to GMAN requriment of all Rx / Tx having a Enable / Disable Calibration";
*|      : units = "Boolean";
*|      : type = fixed.LVM;
*| } 
*/ 
const char FBLOCK_MESSAGE_MASK_ENABLE_LVMINTERFACE_FBLOCK = 0x00;

const char COUNTRY_DUMMY_26 = 0x00;

/*
*| hmi_country_caln.ENABLE_MANUAL_CVP_SWITCH_FOR_ELEVATED_ROAD_DETECTION {
*|      : is_calconst;
*|      : description = "China:  Cal to control the manual swich of the CVP between the elevated roads";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MANUAL_CVP_SWITCH_FOR_ELEVATED_ROAD_DETECTION = 0x00;

/*
*| hmi_country_caln.ENABLE_RFD_UPDATES_FOR_TABULAR_WEATHER {
*|      : is_calconst;
*|      : description = "Cal to control the RFD update feature for the XM Tabular weather data services";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RFD_UPDATES_FOR_TABULAR_WEATHER = 0x00;

/*
*| hmi_country_caln.ENABLE_RFD_UPDATES_FOR_WEATHER_ALERTS {
*|      : is_calconst;
*|      : description = "Cal to control the RFD update feature for the XM weather alerts data services";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RFD_UPDATES_FOR_WEATHER_ALERTS = 0x00;

/*
*| hmi_country_caln.ENABLE_RFD_UPDATES_FOR_FUEL_PRICES {
*|      : is_calconst;
*|      : description = "Cal to control the RFD update feature for the XM Fuel Prices data services";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RFD_UPDATES_FOR_FUEL_PRICES = 0x00;

/*
*| hmi_country_caln.ENABLE_RFD_UPDATES_FOR_MOVIE_TIMES {
*|      : is_calconst;
*|      : description = "Cal to control the RFD update feature for the XM Movie times data services";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RFD_UPDATES_FOR_MOVIE_TIMES = 0x00;

/*
*| hmi_country_caln.ENABLE_ONROUTE_ALERT {
*|      : is_calconst;
*|      : description = "Cal to control the display of traffic alerts on the screen when there is an active route guidance";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_ONROUTE_ALERT = 0x00;

/*
*| hmi_country_caln.ENABLE_TRAFFIC_AHEAD_ALERT {
*|      : is_calconst;
*|      : description = "Cal to control the display of traffic alerts on the screen when there is no active route guidance";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_TRAFFIC_AHEAD_ALERT = 0x00;

/*
*| hmi_country_caln.ENABLE_WEATHER_ALERTS {
*|      : is_calconst;
*|      : description = "Cal to control the display of all weather alerts on the screen";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_WEATHER_ALERTS = 0x00;

/*
*| hmi_country_caln.ENABLE_CITY_MODELS_2D {
*|      : is_calconst;
*|      : description = "Allows the display of the city models on the 2D map mode";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_CITY_MODELS_2D = 0x00;

/*
*| hmi_country_caln.ENABLE_CITY_MODELS_3D {
*|      : is_calconst;
*|      : description = "Allows the display of the city models on the 3D map mode";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_CITY_MODELS_3D = 0x00;

/*
*| hmi_country_caln.GPS_DEFAULT_SATSYS {
*|      : is_calconst;
*|      : description = "DESC";
*|      : units = "ENUM";
*|      : type = fixed.GPS_DEFAULT_SATSYS;
*| } 
*/ 
const char GPS_DEFAULT_SATSYS = 0x00;

/*
*| hmi_country_caln.ENABLE_SPEECHRECOGNITIONDOMAIN_NAVIGATION {
*|      : is_calconst;
*|      : description = "This calibration will allow for the dynamic method to remove Navigation domain speech recognition grammar and HMI. \
Added new calibration ENABLE_SPEECHRECOGNITIONDOMAIN_NAVIGATION to the Country Calibration block to support speech recognition delete for Navigation functions related with compatibility issues with map database and regional compliance.\
HMI functionality and SWC functionality will be described in GIS-400 and speech grammar functionality will be documented in GIS-382.\
\
The user is able to command the system by using speech commands. The following domains are available for control using speech.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_SPEECHRECOGNITIONDOMAIN_NAVIGATION = 0x00;

/*
*| hmi_country_caln.DEFAULT_PREFERRED_PHONEBOOK_SORT_ORDER {
*|      : is_calconst;
*|      : description = "This calibration determines the default phonebook sorting order.  Even though language is the primary factor that influences the phonebook sort order, to clarify complexity, this calibration applies to all langauges that are currently enabled.  Factory default sort order will apply to all available languages within a specific vehicle configuration.\
\
Calibrator hints on how to set:\
USA, Canada, Mexico\
    $00 - firstname_preferred_sortorder\
\
China, Japan, Korea region:\
   $01 - lastname_preferred_sortorder";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DEFAULT_PREFERRED_PHONEBOOK_SORT_ORDER = 0x00;

/*
*| hmi_country_caln.HD_ARTIST_EXPERIENCE_ENABLED_SETTING_PRESENT {
*|      : is_calconst;
*|      : description = "Added new calibration HD_ARTIST_EXPERIENCE_ENABLED_SETTING_PRESENT  to the 208 Country calblock.  This calibration determines when the HD Artist Experience Menu option should be shown in the list. \
\
Boolean, Default = FALSE\
When True, show HDArtistExperienceEnabled in menu, false, hide it.\
";
*|      : units = "enum";
*|      : type = fixed.HD_ARTIST_EXPERIENCE_ENABLED_SETTING_PRESENT;
*| } 
*/ 
const char HD_ARTIST_EXPERIENCE_ENABLED_SETTING_PRESENT = 0x00;

/*
*| hmi_country_caln.JAVASCRIPTAPI_MASK_BYTE_4 {
*|      : is_calconst;
*|      : description = "The following sections outline all JavaScript APIs that shall be available in the HMI module.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char JAVASCRIPTAPI_MASK_BYTE_4 = 0xDE;

/*
*| hmi_country_caln.JAVASCRIPTAPI_MASK_BYTE_5 {
*|      : is_calconst;
*|      : description = "The following sections outline all JavaScript APIs that shall be available in the HMI module.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char JAVASCRIPTAPI_MASK_BYTE_5 = 0xF9;

/*
*| hmi_country_caln.JAVASCRIPTAPI_MASK_BYTE_6 {
*|      : is_calconst;
*|      : description = "The following sections outline all JavaScript APIs that shall be available in the HMI module.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char JAVASCRIPTAPI_MASK_BYTE_6 = 0x00;

/*
*| hmi_country_caln.JAVASCRIPTAPI_MASK_BYTE_7 {
*|      : is_calconst;
*|      : description = "The following sections outline all JavaScript APIs that shall be available in the HMI module.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char JAVASCRIPTAPI_MASK_BYTE_7 = 0x98;

/*
*| hmi_country_caln.RESERVED_APPLICATION_MASK_BYTE_0 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char RESERVED_APPLICATION_MASK_BYTE_0 = 0x1F;

/*
*| hmi_country_caln.LANGUAGE_ENABLE_MASK_BYTE_6 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LANGUAGE_ENABLE_MASK_BYTE_6 = 0x00;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_LITHUANIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_LITHUANIAN = 0x02;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_ESTONIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_ESTONIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_LATVIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_LATVIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_MACEDONIAN {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_MACEDONIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMAT_MASK_SEPERATOR_UZBEK {
*|      : is_calconst;
*|      : description = "Mapping of Date Format to language type";
*|      : units = "Enum";
*|      : type = fixed.DATE_FORMAT_MASK_SEPERATOR;
*| } 
*/ 
const char DATE_FORMAT_MASK_SEPERATOR_UZBEK = 0x00;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_LITHUANIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_LITHUANIAN = 0x02;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ESTONIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_ESTONIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_LATVIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_LATVIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_MACEDONIAN {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_MACEDONIAN = 0x01;

/*
*| hmi_country_caln.DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_UZBEK {
*|      : is_calconst;
*|      : description = "Display Date Formats";
*|      : units = "\"S\" Seperator";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DATE_FORMATS_DISPLAY_S_SEPERATOR_TYPE_UZBEK = 0x01;

/*
*| hmi_country_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_ME_OFFROADS {
*|      : is_calconst;
*|      : description = "To set the OFFROAD_Trail feature for Middle East (to enable/disable the feature for MiddleEast).";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_ME_OFFROADS = 0x00;

/*
*| hmi_country_caln.ENABLE_NAV_ME_OFFROAD_PACKAGE_MENU_SELECTION {
*|      : is_calconst;
*|      : description = "To enable/disable OFFROAD_Trail_menu selection feature for Middle East (to set the MENU, if the OFFROAD feature is enabled).";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ENABLE_NAV_ME_OFFROAD_PACKAGE_MENU_SELECTION = 0x00;

/*
*| hmi_country_caln.TEEN_MODE_SEATBELT_POPUP_AUTODISMISS_TIME {
*|      : is_calconst;
*|      : description = "Timout after Seatbelt not Attached Popup is auto dismissed.  ";
*|      : units = "mSec";
*|      : transform = fixed.TEEN_MODE_SEATBELT_POPUP_AUTODISMISS_TIME;
*| } 
*/ 
const char TEEN_MODE_SEATBELT_POPUP_AUTODISMISS_TIME = 0x0014;

/*
*| hmi_country_caln.DWL_TEEN_SEATBELT_RESTRICTION_MODE {
*|      : is_calconst;
*|      : description = "Seatbelt Restriction:\
 Setting Available";
*|      : units = "enum";
*|      : type = fixed.DWL_TEEN_SEATBELT_RESTRICTION_MODE;
*| } 
*/ 
const char DWL_TEEN_SEATBELT_RESTRICTION_MODE = 0x01;

/*
*| hmi_country_caln.HD_DISPLAY_DIGITAL_ACQ {
*|      : is_calconst;
*|      : description = "Metadata is kept for this time after HD signal loss.";
*|      : units = "mSec";
*|      : transform = fixed.HD_DISPLAY_DIGITAL_ACQ;
*| } 
*/ 
const unsigned short HD_DISPLAY_DIGITAL_ACQ = 0x0014;

/*
*| hmi_country_caln.HD_RADIO_AVAILABLE {
*|      : is_calconst;
*|      : description = "System includes a HD Radio Tuner";
*|      : units = "enum";
*|      : type = fixed.HD_RADIO_AVAILABLE;
*| } 
*/ 
const char HD_RADIO_AVAILABLE = 0x01;

const char COUNTRY_DUMMY_35 = 0x00;

const char COUNTRY_DUMMY_36 = 0x00;

/*
*| hmi_country_caln.ENABLE_APPTRAY_NAV_MODE_QSP {
*|      : is_calconst;
*|      : description = "Disbales the QSP after navi map mode change";
*|      : units = "enum";
*|      : type = fixed.ENABLE_APPTRAY_NAV_MODE_QSP;
*| } 
*/ 
const char ENABLE_APPTRAY_NAV_MODE_QSP = 0x00;

/*
*| hmi_country_caln.FM_ARTIST_EXPERIENCE_HD_IMAGE_CHANGE_TIMEOUT {
*|      : is_calconst;
*|      : description = "The Metadata field will display \"CAT Not Found\" when there are no channels available for a selected category. This information will timeout after 3 seconds (Calibratble) or disapear with user input.";
*|      : units = "ms";
*|      : transform = fixed.FM_ARTIST_EXPERIENCE_HD_IMAGE_CHANGE_TIMEOUT;
*| } 
*/ 
const char FM_ARTIST_EXPERIENCE_HD_IMAGE_CHANGE_TIMEOUT = 0x1E;

/*
*| hmi_country_caln.TEEN_DRIVER_VOLUME_LIMIT_MENU_ENABLE {
*|      : is_calconst;
*|      : description = "1.4.2.2 Radio Volume\
WHEN  Teen Mode = Active and K_TEEN_RADIO_VOLUME_LIMIT = Enabled - (Range: Enabled / Disabled, Set Point: Enabled , Resolution: N/A)\
\
THEN Limit Radio Volume to a fixed value of K_Teen_Radio_Volume_Setting - Range: 50%-100%, \
Set Point: 50%, Resolution: 1%.\
note: this calibration should be the max Settable Volume Step.  This calibration may end up residing in the Silverbox.  But HMI module could easily prevent setting Volume above the Volume step just as well.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char TEEN_DRIVER_VOLUME_LIMIT_MENU_ENABLE = 0x01;

/*
*| hmi_country_caln.MULTIWAYSEAT_STATUS_PANE_AUTO_DISMISS_TIMER {
*|      : is_calconst;
*|      : description = "MULTIWAYSEAT_STATUS_PANE_AUTO_DISMISS_TIMER - calibration to set the time period to wait before auto-dismissing  the Seat Status Pane, due to inactivity with the seat switch controls.  Type - ENM, Range = 0-255 Sec, Resolution = 1 Sec, Default = 3 Sec.\
";
*|      : units = "Sec";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MULTIWAYSEAT_STATUS_PANE_AUTO_DISMISS_TIMER = 0x03;

/*
*| hmi_country_caln.MULTIWAYSEAT_STATUS_PANE_MINI_MODE_TIMER {
*|      : is_calconst;
*|      : description = "MULTIWAYSEAT_STATUS_PANE_MINI_MODE_TIMER - calibration to set the time period to wait before switching the dominant view (i.e.  Driver OR Passenger) in the Miniature Seat Status Pane (Mini Mode).  Type - ENM, Range = 0-255 Sec, Resolution = 1 Sec, Default = 3 Sec.";
*|      : units = "Sec";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MULTIWAYSEAT_STATUS_PANE_MINI_MODE_TIMER = 0x03;

const char COUNTRY_DUMMY_31 = 0x00;

const char COUNTRY_DUMMY_32 = 0x00;

/*
*| hmi_country_caln.DWL_MULTI_WAY_SEAT_DISPLAY_LOCKOUT {
*|      : is_calconst;
*|      : description = "MULTI_WAY_SEAT: Driver Workload Lockouts";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_MULTI_WAY_SEAT_DISPLAY_LOCKOUT = 0x80;

/*
*| hmi_country_caln.DWL_SEAT_PANE_LOCKOUT {
*|      : is_calconst;
*|      : description = "DWL_SEAT_PANE: Driver Workload Lockouts";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DWL_SEAT_PANE_LOCKOUT = 0x00;

/*
*| hmi_country_caln.GPS_DATA_SOURCE {
*|      : is_calconst;
*|      : description = "Cal to define the source of the GPS data";
*|      : units = "Enumeration";
*|      : type = fixed.GPS_DATA_SOURCE;
*| } 
*/ 
const char GPS_DATA_SOURCE = 0x02;

/*
*| hmi_country_caln.ENABLE_GLONAS_AS_GPS_DEFAULT {
*|      : is_calconst;
*|      : description = "This calibration provides the factory default for the GLONAS  menu setting.  Reference SETTINGS_MASK_BYTE_1_BIT6 to show the setting menu item for GLONAS.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_GLONAS_AS_GPS_DEFAULT = 0x00;

/*
*| hmi_country_caln.SETTINGS_MASK_BYTE_4 {
*|      : is_calconst;
*|      : description = "Menu Configuration Settings";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SETTINGS_MASK_BYTE_4 = 0x00;

/*
*| hmi_country_caln.DEFAULT_MASK_BT_SETTINGS {
*|      : is_calconst;
*|      : description = "The user taps Bluetooth in the Vehicle Settings interac�tion selector to view the Bluetooth settings menu. The user may add a new Bluetooth device, manage the currently paired Bluetooth devices, change ringtones, or change voice mail phone number.:";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DEFAULT_MASK_BT_SETTINGS = 0xF0;

/*
*| hmi_country_caln.ENABLE_VOICEMAIL_VIEW_BUTTON {
*|      : is_calconst;
*|      : description = "Voicemail is a regional requirement.\
$00 - disable for China applciations\
$01 - enabled for all non-China applications\
\
11.2.7~MY13 No Voice Mail Functionality\
There is no voice mail functionality for China in the Phone application. The Interaction Selector for Phone views does not include the Voice Mail button. The content of the Interaction Selector for China is Speech Recognition, Contacts, Recent, Keypad, and Phone Source.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VOICEMAIL_VIEW_BUTTON = 0x01;

/*
*| hmi_country_caln.ENABLE_APPLICATION_TEXT_SMS {
*|      : is_calconst;
*|      : description = "HomeScreen Applications\
Updated description text to match clarified GIS-40X specifications:\
ENABLE_APPLICATION_TEXT_SMS\
Documentation:  this controls the following:\
1)  Home screen icon for Text SMS\
2) Settings->Bluetooth->Text message alerts settings\
3) Quick text icons in the contacts for a phone number\
ENABLE_APPLICATION_TEXT_MMS\
Documentation  this controls the following:\
1) Remove the one list item on 9.7.23.2 �Record an audio message�.  Basically, TEXT_MMS cal prevents the creation of MMS type messages.  Viewing of MMS will still be comprehended.\
\
\
ENABLE_APPLICATION_TEXT_SMS cal was intended for both enabling the SMS functionality AND showing the icon in the home screen, but 4.2\" does not have an icon for SMS in the home screen by design (see GIS-402). However, SMS is accessible via the Phone Home Screen.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_TEXT_SMS = 0x01;

const char COUNTRY_DUMMY_33 = 0x00;

/*
*| hmi_country_caln.PREVIOUSLY_RUNNING_APPS_RESTART_ENABLED {
*|      : is_calconst;
*|      : description = "This calibration is to globally enable / disable internet Applications restarting if they had been running at shutdown.\
Turned off in MY2014 development to improve system startup performance.  Need to change default to $0 to match expected MY2015/16 behavior.";
*|      : units = "Apps";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PREVIOUSLY_RUNNING_APPS_RESTART_ENABLED = 0x00;

/*
*| hmi_country_caln.AUTOSTART_ENABLED {
*|      : is_calconst;
*|      : description = "This calibration is to globally enable / disable internet Application Autostart.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUTOSTART_ENABLED = 0x01;

/*
*| hmi_country_caln.JAVASCRIPTAPI_MASK_BYTE_0 {
*|      : is_calconst;
*|      : description = "The following sections outline all JavaScript APIs that shall be available in the HMI module.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char JAVASCRIPTAPI_MASK_BYTE_0 = 0xED;

/*
*| hmi_country_caln.JAVASCRIPTAPI_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "The following sections outline all JavaScript APIs that shall be available in the HMI module.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char JAVASCRIPTAPI_MASK_BYTE_1 = 0xBF;

/*
*| hmi_country_caln.JAVASCRIPTAPI_MASK_BYTE_2 {
*|      : is_calconst;
*|      : description = "The following sections outline all JavaScript APIs that shall be available in the HMI module.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char JAVASCRIPTAPI_MASK_BYTE_2 = 0x3F;

/*
*| hmi_country_caln.JAVASCRIPTAPI_MASK_BYTE_3 {
*|      : is_calconst;
*|      : description = "The following sections outline all JavaScript APIs that shall be available in the HMI module.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char JAVASCRIPTAPI_MASK_BYTE_3 = 0xF8;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_15 {
*|      : is_calconst;
*|      : description = "Disables internet Application 15 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_15 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_14 {
*|      : is_calconst;
*|      : description = "Disables internet Application 14 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_14 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_13 {
*|      : is_calconst;
*|      : description = "Disables internet Application 13 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_13 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_12 {
*|      : is_calconst;
*|      : description = "Disables internet Application 12 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_12 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_11 {
*|      : is_calconst;
*|      : description = "Disables internet Application 11 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_11 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_10 {
*|      : is_calconst;
*|      : description = "Disables internet Application 10 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_10 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_9 {
*|      : is_calconst;
*|      : description = "Disables internet Application 9 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_9 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_8 {
*|      : is_calconst;
*|      : description = "Disables internet Application 8 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_8 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_7 {
*|      : is_calconst;
*|      : description = "Disables internet Application 7 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_7 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_6 {
*|      : is_calconst;
*|      : description = "Disables internet Application 6 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_6 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_5 {
*|      : is_calconst;
*|      : description = "Disables internet Application 5 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_5 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_4 {
*|      : is_calconst;
*|      : description = "Disables internet Application 4 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_4 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_3 {
*|      : is_calconst;
*|      : description = "Disables internet Application 3 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_3 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_2 {
*|      : is_calconst;
*|      : description = "Disables internet Application 2 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_2 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_1 {
*|      : is_calconst;
*|      : description = "Disables internet Application 1 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_1 = 0x00;

/*
*| hmi_country_caln.ENABLE_RESERVED_APPLICATION_0 {
*|      : is_calconst;
*|      : description = "Disables internet Application 0 on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RESERVED_APPLICATION_0 = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATION_DMB {
*|      : is_calconst;
*|      : description = "Disables DMB+ (LVM) Application on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_DMB = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_DMB {
*|      : is_calconst;
*|      : description = "Shows DMB+ (LVM) Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_DMB = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATION_HANDSET_VR {
*|      : is_calconst;
*|      : description = "Disables Handset VR Application on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_HANDSET_VR = 0x01;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_HANDSET_VR {
*|      : is_calconst;
*|      : description = "Shows Handset VR Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_HANDSET_VR = 0x01;

/*
*| hmi_country_caln.ENABLE_APPLICATION_NAVIGATION_LVM {
*|      : is_calconst;
*|      : description = "Disables Navigation (LVM) Application on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.LVM;
*| } 
*/ 
const char ENABLE_APPLICATION_NAVIGATION_LVM = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_NAVIGATION_LVM {
*|      : is_calconst;
*|      : description = "Shows Navigation (LVM) Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.LVM;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_NAVIGATION_LVM = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATION_PANDORA {
*|      : is_calconst;
*|      : description = "Disables Pandora Application on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_PANDORA = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_WEATHER {
*|      : is_calconst;
*|      : description = "Shows Weather Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_WEATHER = 0x01;

/*
*| hmi_country_caln.ENABLE_APPLICATION_PERFORMANCE_DATA_RECORDER {
*|      : is_calconst;
*|      : description = "Disables Performance Data Recorder Application on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_PERFORMANCE_DATA_RECORDER = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_PERFORMANCE_DATA_RECORDER {
*|      : is_calconst;
*|      : description = "Shows Performance Data Recorder Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_PERFORMANCE_DATA_RECORDER = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_TEXT_SMS {
*|      : is_calconst;
*|      : description = "Shows Text Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_TEXT_SMS = 0x01;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_TEXT_MMS {
*|      : is_calconst;
*|      : description = "Shows Text Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_TEXT_MMS = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATION_VIDEO_RECORDER {
*|      : is_calconst;
*|      : description = "Disables Video Recorder on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_VIDEO_RECORDER = 0x01;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_VIDEO_RECORDER {
*|      : is_calconst;
*|      : description = "Shows Video Recorder Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_VIDEO_RECORDER = 0x00;

/*
*| hmi_country_caln.AM_CATEGORIES_SETTING_PRESENT {
*|      : is_calconst;
*|      : description = "The user can toggle the AM Categories on or off by tapping this list item. The Default is ON\
\
There are two independent cals for AM and FM band that will allow the SW to enable or disable the CAT (PTY Categories) HMI button that is presented to the users. The purpose for change is to be able to isolate PTY categories button away from RDS functionality just in case RDS exists in a region however the RDS Categories are not available all the time. \
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AM_CATEGORIES_SETTING_PRESENT = 0x01;

/*
*| hmi_country_caln.FM_CATEGORIES_SETTING_PRESENT {
*|      : is_calconst;
*|      : description = "The user can toggle the FM Categories on or off by tapping this list item. The Default is ON\
\
There are two independent cals for AM and FM band that will allow the SW to enable or disable the CAT (PTY Categories) HMI button that is presented to the users. The purpose for change is to be able to isolate PTY categories button away from RDS functionality just in case RDS exists in a region however the RDS Categories are not available all the time. \
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char FM_CATEGORIES_SETTING_PRESENT = 0x01;

/*
*| hmi_country_caln.ENABLE_AUTOZOOM {
*|      : is_calconst;
*|      : description = "The user is able to configure whether or not they would\
like Autozoom to be enabled for maneuvers. The default\
is ON and if the user turns this off, the zoom level\
will never be adjusted automatically by the system and\
always left at the user�s last zoom level.\
\
18th Sep 2013: This cal enables/disables the Autozoom feature being placed on the menu.  DEFAULT_AUTOZOOM_SETTING is the cal that sets the default autozoom setting.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUTOZOOM = 0x00;

/*
*| hmi_country_caln.ENABLE_3D_BUILDINGS {
*|      : is_calconst;
*|      : description = "The user is able to configure whether or not 3D buildings\
are displayed or not. If they are turned off, the\
system does not display any 3D landmarks or 3D city\
models. The default is On and this setting is retained\
over ignition cycles.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_3D_BUILDINGS = 0x00;

/*
*| hmi_country_caln.DEFAULT_AUTOZOOM_SETTING {
*|      : is_calconst;
*|      : description = "The user can toggle Autozoom on or off by tapping this list item. The Default is OFF\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DEFAULT_AUTOZOOM_SETTING = 0x00;

/*
*| hmi_country_caln.DEFAULT_3D_BUILDINGS_SETTING {
*|      : is_calconst;
*|      : description = "The user can toggle 3D Building on or off by tapping this list item. The Default is OFF\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DEFAULT_3D_BUILDINGS_SETTING = 0x00;

/*
*| hmi_country_caln.DTC_INTERNET_RESETS {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.  Reference DTC B126A_00";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DTC_INTERNET_RESETS = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATION_TRAFFIC {
*|      : is_calconst;
*|      : description = "Disables Traffic Application on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_TRAFFIC = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_TRAFFIC {
*|      : is_calconst;
*|      : description = "Shows Traffic Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_TRAFFIC = 0x00;

/*
*| hmi_country_caln.ENABLE_APPLICATIONTRAY_INTERNET_APP_6 {
*|      : is_calconst;
*|      : description = "Shows Internet Application 6 on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_INTERNET_APP_6 = 0x00;

/*
*| hmi_country_caln.CAL_HMI_COUNTRY_END {
*|      : is_calconst;
*|      : description = "END OF CAL BLOCK";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CAL_HMI_COUNTRY_END = 0x00;
