
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#include <EOLLib.h>

/*
MOD ID 0B - HMI RR VIEW CAMERA CALN
*/

/*
*| hmi_rr_view_camera_caln.HEADER_RVC {
*|      : is_calconst;
*|      : description = "CALDS Header";
*| } 
*/ 
const EOLLib_CalDsHeader HEADER_RVC = {0x0000, EOLLIB_TABLE_ID_REAR_VISION_CAMERA << 8, 0x0000, 0x00000000, {0x41, 0x41}, 0x0401};

/*
*| hmi_rr_view_camera_caln.RVS_PRESENT_STATUS {
*|      : is_calconst;
*|      : description = "Switch between �No RVC�, �RVC� and �Camera processing module�. Refer to GIS-40 state charts and documentation in �Rear Vision System - Calibration flowpath - rev 4.pdf� for details.\
NOTE for MY16: When RVS_PRESENT_STATUS=2, HMIM software shall ignore rear camera cals - VPM controls camera features.";
*|      : units = "Bool";
*|      : type = fixed.UB0;
*| } 
*/ 
const char RVS_PRESENT_STATUS = 0x01;

const unsigned short RVS_DUMMY_61 = 0x0000;

const char RVS_DUMMY_62 = 0x00;

const char RVS_DUMMY_63 = 0x00;

const char RVS_DUMMY_4 = 0x00;

/*
*| hmi_rr_view_camera_caln.RVS_FEATURE_AVAIL_GUIDELINES {
*|      : is_calconst;
*|      : description = "Guideline On/Off On/Off Option.";
*|      : units = "Enum";
*|      : type = fixed.RVS_FEATURE_AVAIL_GUIDELINES;
*| } 
*/ 
const char RVS_FEATURE_AVAIL_GUIDELINES = 0x02;

/*
*| hmi_rr_view_camera_caln.RVS_TEXT_LOCATION {
*|      : is_calconst;
*|      : description = "Drives messages Location. \
GIS-40-023";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char RVS_TEXT_LOCATION = 0x01;

/*
*| hmi_rr_view_camera_caln.RVS_FEATURE_AVAIL_UPA_SYMBOLS {
*|      : is_calconst;
*|      : description = "Rear Parking Assist On/Off Option. ";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char RVS_FEATURE_AVAIL_UPA_SYMBOLS = 0x01;

const char RVS_DUMMY_59 = 0x00;

const char RVS_DUMMY_60 = 0x00;

const unsigned short RVS_DUMMY_43 = 0x0000;

const unsigned short RVS_DUMMY_44 = 0x0000;

const unsigned short RVS_DUMMY_45 = 0x0000;

const char RVS_DUMMY_1 = 0x00;

const char RVS_DUMMY_3 = 0x00;

const char RVS_DUMMY_46 = 0x00;

const char RVS_DUMMY_47 = 0x00;

const char RVS_DUMMY_48 = 0x00;

const unsigned short RVS_DUMMY_49 = 0x0000;

const unsigned short RVS_DUMMY_50 = 0x0000;

const unsigned short RVS_DUMMY_51 = 0x0000;

const unsigned short RVS_DUMMY_52 = 0x0000;

const char RVS_DUMMY_53 = 0x00;

const char RVS_DUMMY_54 = 0x00;

const char RVS_DUMMY_55 = 0x00;

const char RVS_DUMMY_56 = 0x00;

const char RVS_DUMMY_57 = 0x00;

const char RVS_DUMMY_58 = 0x00;

const char RVS_DUMMY_40 = 0x00;

const char RVS_DUMMY_41 = 0x00;

const char RVS_DUMMY_42 = 0x00;

const char RVS_DUMMY_35 = 0x00;

const unsigned short RVS_DUMMY_36 = 0x0000;

const unsigned short RVS_DUMMY_37 = 0x0000;

const unsigned short RVS_DUMMY_38 = 0x0000;

const char RVS_DUMMY_39 = 0x00;

const unsigned short RVS_DUMMY_11 = 0x0000;

const unsigned short RVS_DUMMY_12 = 0x0000;

const unsigned short RVS_DUMMY_13 = 0x0000;

const unsigned short RVS_DUMMY_14 = 0x0000;

const unsigned short RVS_DUMMY_15 = 0x0000;

const unsigned short RVS_DUMMY_16 = 0x0000;

const unsigned short RVS_DUMMY_17 = 0x0000;

const unsigned short RVS_DUMMY_18 = 0x0000;

const unsigned short RVS_DUMMY_19 = 0x0000;

const unsigned short RVS_DUMMY_20 = 0x0000;

const unsigned short RVS_DUMMY_21 = 0x0000;

const unsigned short RVS_DUMMY_22 = 0x0000;

const unsigned short RVS_DUMMY_23 = 0x0000;

const unsigned short RVS_DUMMY_24 = 0x0000;

const unsigned short RVS_DUMMY_25 = 0x0000;

const unsigned short RVS_DUMMY_26 = 0x0000;

const unsigned short RVS_DUMMY_27 = 0x0000;

const unsigned short RVS_DUMMY_28 = 0x0000;

const unsigned short RVS_DUMMY_29 = 0x0000;

const unsigned short RVS_DUMMY_30 = 0x0000;

const unsigned short RVS_DUMMY_31 = 0x0000;

const unsigned short RVS_DUMMY_32 = 0x0000;

const unsigned short RVS_DUMMY_33 = 0x0000;

const unsigned short RVS_DUMMY_34 = 0x0000;

/*
*| hmi_rr_view_camera_caln.REAR_CAMERA_SETTING_MASK {
*|      : is_calconst;
*|      : description = "The Settings Application Rear Camera Settings view pro�vides the following settings: ";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char REAR_CAMERA_SETTING_MASK = 0x7F;

const char RVS_DUMMY_10 = 0x00;

const unsigned short RVS_DUMMY_2 = 0x0000;

const unsigned short RVS_DUMMY_5 = 0x0000;

const unsigned short RVS_DUMMY_6 = 0x0000;

const unsigned short RVS_DUMMY_7 = 0x0000;

const unsigned short RVS_DUMMY_8 = 0x0000;

const char RVS_DUMMY_9 = 0x00;

/*
*| hmi_rr_view_camera_caln.SWMI_CALIBRATION_DATA_FILE_HMI_REAR_VISION_CAL {
*|      : is_calconst;
*|      : description = "The NoCalibration state is defined in order to make sure that Infotainment subsystem components have been updated with calibrations after a service event such as replacing one or more modules or upgrading software.  The mechanism for NoCalibration determination is the same as that for Theftlock and NoVIN determination.  That is, at initialization a module detects whether or not it has a valid calibration.  If it does not, it notifies the SystemState FBlock via the SystemState.SetNoCalibrationModuleState method.  Once a valid calibration is received, the module calls the SystemState.SetNoCalibrationModuleState method to clear the condition.\
The SystemState FBlock shall assume all module calibrations are valid upon each Sleep cycle.  Modules that already have a valid calibration or do not support NoCalibration detection (determined by each module CTS) do not need to report the NoCalibration clear condition at each initialization.\
Note: each of these Calibrations should be ...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SWMI_CALIBRATION_DATA_FILE_HMI_REAR_VISION_CAL = 0x00;

/*
*| hmi_rr_view_camera_caln.DTC_MASK_BYTE_REAR_CAMERA {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_BYTE_REAR_CAMERA = 0x01;

/*
*| hmi_rr_view_camera_caln.ENABLE_APPLICATION_FRONT_CAMERA {
*|      : is_calconst;
*|      : description = "FRONT_CAMERA ICON on home screen,to enable/disable the front camera icon/button on the home screen ";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_FRONT_CAMERA = 0x00;

/*
*| hmi_rr_view_camera_caln.ENABLE_APPLICATIONTRAY_FRONT_CAMERA {
*|      : is_calconst;
*|      : description = "Shows Front Camera Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_FRONT_CAMERA = 0x00;

/*
*| hmi_rr_view_camera_caln.CAL_HMI_REAR_VIEW_CAMERA_END {
*|      : is_calconst;
*|      : description = "END OF CAL BLOCK";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CAL_HMI_REAR_VIEW_CAMERA_END = 0x00;
