
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#include <EOLLib.h>

/*
MOD ID 02 - HMI SYS CALN
*/

/*
*| hmi_sys_caln.HEADER_SYSTEM {
*|      : is_calconst;
*|      : description = "CALDS Header";
*| } 
*/ 
const EOLLib_CalDsHeader HEADER_SYSTEM = {0x0000, EOLLIB_TABLE_ID_SYSTEM << 8, 0x0000, 0x00000000, {0x41, 0x41}, 0x0401};

/*
*| hmi_sys_caln.ACT_REQ_HOLD_OFF_TIME {
*|      : is_calconst;
*|      : description = "Sometimes the request for activation of a Logical AV Channel is returned with a RequestResult of RR_DENIED or RR_SUSPENDED.  In this case, the IAVConnectionRequester may request the same channel again after a specified time period or when the ChannelStatus transitions to CS_INACTIVE or CS_SUSPENDED. \
The IAVConnectionRequester�s requirements regarding retransmission of a RequestAVActivation.StartResultAck message for a channel for which the received RequestAVActivation.ResultAck had a (RequestResult == RR_DENIED) or (RequestResult == RR_SUSPENDED) are as follows:\
1.The IAVConnectionRequester shall wait until one of the following conditions occur before retransmitting the RequestAVActivation.StartResultAck:\
a. The ChannelStatus of the channel transitions to CS_INACTIVE or CS_SUSPENDED from any other value.\
b. ACT_REQ_HOLD_OFF_TIME, a calibratable value defined in GIS-344, has transpired since the previous request was denied or suspended.";
*|      : units = "ms";
*|      : transform = fixed.ACT_REQ_HOLD_OFF_TIME;
*| } 
*/ 
const char ACT_REQ_HOLD_OFF_TIME = 0x19;

/*
*| hmi_sys_caln.U_TH_SUPER {
*|      : is_calconst;
*|      : description = "The threshold levels in the diagram above are specified using the rising transition on the left side of the diagram as follows:\
\
UTh_Super =  16.0 V\
UTh_Critical  =  9.0 V\
UTh_Low =  6.5 V\
\
The corresponding falling voltage thresholds are defined by subtracting the UHysteresis voltage delta from the rising thresholds specified above.  The value of UHysteresis is 0.5 V for all components.\
The tolerance on all voltage levels shall be �0.5V\
";
*|      : units = "Volts";
*|      : transform = fixed.U_TH_SUPER;
*| } 
*/ 
const char U_TH_SUPER = 0xa0;

/*
*| hmi_sys_caln.U_TH_CRITICAL {
*|      : is_calconst;
*|      : description = "The threshold levels in the diagram above are specified using the rising transition on the left side of the diagram as follows:\
\
UTh_Super =  16.0 V\
UTh_Critical  =  9.0 V\
UTh_Low =  6.5 V\
\
The corresponding falling voltage thresholds are defined by subtracting the UHysteresis voltage delta from the rising thresholds specified above.  The value of UHysteresis is 0.5 V for all components.\
The tolerance on all voltage levels shall be �0.5V\
";
*|      : units = "Volts";
*|      : transform = fixed.U_TH_CRITICAL;
*| } 
*/ 
const char U_TH_CRITICAL = 0x5a;

/*
*| hmi_sys_caln.U_TH_LOW {
*|      : is_calconst;
*|      : description = "The threshold levels in the diagram above are specified using the rising transition on the left side of the diagram as follows:\
\
UTh_Super =  16.0 V\
UTh_Critical  =  9.0 V\
UTh_Low =  6.5 V\
\
The corresponding falling voltage thresholds are defined by subtracting the UHysteresis voltage delta from the rising thresholds specified above.  The value of UHysteresis is 0.5 V for all components.\
The tolerance on all voltage levels shall be �0.5V\
";
*|      : units = "Volts";
*|      : transform = fixed.U_TH_LOW;
*| } 
*/ 
const char U_TH_LOW = 0x41;

/*
*| hmi_sys_caln.U_HYSTERESIS {
*|      : is_calconst;
*|      : description = "The threshold levels in the diagram above are specified using the rising transition on the left side of the diagram as follows:\
\
UTh_Super =  16.0 V\
UTh_Critical  =  9.0 V\
UTh_Low =  6.5 V\
\
The corresponding falling voltage thresholds are defined by subtracting the UHysteresis voltage delta from the rising thresholds specified above.  The value of UHysteresis is 0.5 V for all components.\
The tolerance on all voltage levels shall be �0.5V\
";
*|      : units = "Volts";
*|      : transform = fixed.U_HYSTERESIS;
*| } 
*/ 
const char U_HYSTERESIS = 0x05;

/*
*| hmi_sys_caln.DISPLAY_POWER_ON_TIME {
*|      : is_calconst;
*|      : description = "In order to coordinate initial display illumination, the Silverbox, Remote Faceplate, and Remote Display, and HMI Module shall execute the power-on sequence on exit from the Sleep state as described below";
*|      : units = "ms";
*|      : transform = fixed.DISPLAY_POWER_ON_TIME;
*| } 
*/ 
const char DISPLAY_POWER_ON_TIME = 0x0A;

/*
*| hmi_sys_caln.AUDIBLE_FEEDBACK_HOLD {
*|      : is_calconst;
*|      : description = "revised description:\
GIS-318-1635 AudibleFeedback Hold Time\
As shown in the AudioCues FBlock internal state logic on the following page, the AudioCues FBlock shall implement a hold timer after the completion of playback for a single audible feedback request before requesting deactivating the LC_SOFT_ALERT_TONE channel in the AVManager.  \
\
\
original description prior to GIS-344 V3.0\
AudioCues FBlock that holds the audible feedback requests to avoid jitter on successive button requests.  It was defined in the 1.7 release of GIS-318 last month";
*|      : units = "ms";
*|      : transform = fixed.AUDIBLE_FEEDBACK_HOLD;
*| } 
*/ 
const char AUDIBLE_FEEDBACK_HOLD = 0x0A;

const char SYSTEM_DUMMY_1[11] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_sys_caln.IFAVORTEPROVIDER_MASK_BYTE1 {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char IFAVORTEPROVIDER_MASK_BYTE1 = 0x40;

/*
*| hmi_sys_caln.SYSSUPPID_SYSTEMSUPPLIERIDENTIFIER {
*|      : is_calconst;
*|      : description = "SystemSupplierId information which identifies the ECU hardware supplier and the system type (System Group Identifier).";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SYSSUPPID_SYSTEMSUPPLIERIDENTIFIER[5] =
 {
  0x42, 0x4F, 0x53, 0x43, 0x48
 }
;

/*
*| hmi_sys_caln.SYSSUPPID_SYSTEMGROUPID {
*|      : is_calconst;
*|      : description = "SystemSupplierId information which identifies the ECU hardware supplier and the system type (System Group Identifier).";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SYSSUPPID_SYSTEMGROUPID[2] =
 {
  0x30, 0x34
 }
;

/*
*| hmi_sys_caln.SYSSUPPID_RESERVED {
*|      : is_calconst;
*|      : description = "SystemSupplierId information which identifies the ECU hardware supplier and the system type (System Group Identifier).";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SYSSUPPID_RESERVED[2] =
 {
  0x30, 0x30
 }
;

/*
*| hmi_sys_caln.SNOET {
*|      : is_calconst;
*|      : description = "SystemNameOrEngineType which identifies the electronic system name (e.g. Automatic Transmission with 2 Ltr engine: GS820 X20XEV) or engine type (X30XE) installed.  The data shall always be of type ASCII.  The length is variable (depends on length of string) but shall not exceed 20 characters.";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SNOET[19] =
 {
  0x4E, 0x45, 0x58, 0x54, 0x20, 0x47, 0x45, 0x4E, 0x20, 0x48, 0x4D, 0x49, 0x20, 0x4D, 0x4F, 0x44, 0x55, 0x4C, 0x45
 }
;

/*
*| hmi_sys_caln.DTC_CRANK_DELAY_TIME {
*|      : is_calconst;
*|      : description = "Seconday condition.  All DTC's should not set during a Crank Operation and for x ms after the crank operation.";
*|      : units = "ms";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_CRANK_DELAY_TIME = 0x32;

/*
*| hmi_sys_caln.MEC {
*|      : is_calconst;
*|      : description = "the MEC which is used when determining the current status of ECU security.";
*|      : units = "Count";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MEC = 0xfe;

/*
*| hmi_sys_caln.GMBVI_VERIFICATION_INITIAL {
*|      : is_calconst;
*|      : description = "contains 5 bytes of data used to provide information about bench verification during the development process.  ";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GMBVI_VERIFICATION_INITIAL[2] =
 {
  0x53, 0x4C
 }
;

/*
*| hmi_sys_caln.GMBVI_YEAR {
*|      : is_calconst;
*|      : description = "contains 5 bytes of data used to provide information about bench verification during the development process.  ";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GMBVI_YEAR = 0x6e;

/*
*| hmi_sys_caln.GMBVI_MONTH {
*|      : is_calconst;
*|      : description = "contains 5 bytes of data used to provide information about bench verification during the development process.  ";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GMBVI_MONTH = 0x04;

/*
*| hmi_sys_caln.GMBVI_DAY {
*|      : is_calconst;
*|      : description = "contains 5 bytes of data used to provide information about bench verification during the development process.  ";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GMBVI_DAY = 0x0C;

/*
*| hmi_sys_caln.GMTV_MODEL_YEAR {
*|      : is_calconst;
*|      : description = "contains 5 bytes of data used to identify the vehicle that the ECU is targeted for";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GMTV_MODEL_YEAR = 0x23;

/*
*| hmi_sys_caln.GMTV_VEHICLE_MAKE {
*|      : is_calconst;
*|      : description = "contains 5 bytes of data used to identify the vehicle that the ECU is targeted for";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GMTV_VEHICLE_MAKE = 0x23;

/*
*| hmi_sys_caln.GMTV_VEHILCE_CARLINE {
*|      : is_calconst;
*|      : description = "contains 5 bytes of data used to identify the vehicle that the ECU is targeted for";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GMTV_VEHILCE_CARLINE = 0x23;

/*
*| hmi_sys_caln.GMTV_ENGINE_RPO {
*|      : is_calconst;
*|      : description = "contains 5 bytes of data used to identify the vehicle that the ECU is targeted for";
*|      : units = "UTF-8";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GMTV_ENGINE_RPO[2] =
 {
  0x2A, 0x2A
 }
;

/*
*| hmi_sys_caln.GMBC {
*|      : is_calconst;
*|      : description = "contains 4 alpha characters which represent the ECU broadcast code.  The broadcast code is unique for each combination of hardware, software, and calibrations for an ECU";
*|      : units = "ASCII";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GMBC[4] =
 {
  0x41, 0x42, 0x43, 0x44
 }
;

/*
*| hmi_sys_caln.U0029_DELAY_TIMER {
*|      : is_calconst;
*|      : description = "Countdown timer related with U0029 MOST Bus DTC.";
*|      : units = "ms";
*|      : transform = fixed.U0029_DELAY_TIMER;
*| } 
*/ 
const char U0029_DELAY_TIMER = 0x64;

const unsigned short SYSTEM_DUMMY_15 = 0x0000;

const unsigned short SYSTEM_DUMMY_16 = 0x0000;

/*
*| hmi_sys_caln.DTC_IGN_CYCLE_CTR_MAX {
*|      : is_calconst;
*|      : description = "DTC�s with corresponding DTC Status and Failure Type information shall be cleared after a calibratable: DTC_IGN_CYCLE_CTR_MAX ignition on/off cycles with at least one testPass in each ignition cycle and no test fail result. ";
*|      : units = "Cycles";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_IGN_CYCLE_CTR_MAX = 0x32;

/*
*| hmi_sys_caln.HS_BUS_OFF_DTC_FAILURE_CRITERIA_X {
*|      : is_calconst;
*|      : description = "The handler notifies the application by using a callback function. This callback function is launched by the handler whenever a bus off condition is reported by the CAN-controller. A routine () checks every 1000ms if a Bus Off condition occurred. If the number of Bus Off events exceeds a Calibratable threshold of a calibratable number of consecutive tests, i.e. an X of Y algorithm, then the Bus Off fault code is set. The X of Y algorithm shall be started and the counter for Bus Off events shall go from 0 to 1 when the application is notified by the handler that a Bus Off has occurred. Setting of Lost Comm DTCs shall be inhibited while the counter for Bus Off events is greater than 0. The HMI Module shall make provisions for these 2 calibration variables. For high speed buses the default value for Bus Off events shall be 5, and the number of consecutive tests shall be set to 5, i.e. x=5 and y=5.";
*|      : units = "UNM";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HS_BUS_OFF_DTC_FAILURE_CRITERIA_X = 0x05;

/*
*| hmi_sys_caln.HS_BUS_OFF_DTC_FAILURE_CRITERIA_Y {
*|      : is_calconst;
*|      : description = "The handler notifies the application by using a callback function. This callback function is launched by the handler whenever a bus off condition is reported by the CAN-controller. A routine () checks every 1000ms if a Bus Off condition occurred. If the number of Bus Off events exceeds a Calibratable threshold of a calibratable number of consecutive tests, i.e. an X of Y algorithm, then the Bus Off fault code is set. The X of Y algorithm shall be started and the counter for Bus Off events shall go from 0 to 1 when the application is notified by the handler that a Bus Off has occurred. Setting of Lost Comm DTCs shall be inhibited while the counter for Bus Off events is greater than 0. The HMI Module shall make provisions for these 2 calibration variables. For high speed buses the default value for Bus Off events shall be 5, and the number of consecutive tests shall be set to 5, i.e. x=5 and y=5.";
*|      : units = "UNM";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HS_BUS_OFF_DTC_FAILURE_CRITERIA_Y = 0x05;

/*
*| hmi_sys_caln.DTC_MASK_BYTE1 {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_BYTE1 = 0xFD;

/*
*| hmi_sys_caln.DTC_MASK_BYTE2 {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_BYTE2 = 0xF9;

/*
*| hmi_sys_caln.DTC_MASK_BYTE3 {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_BYTE3 = 0xBF;

/*
*| hmi_sys_caln.DTC_MASK_BYTE4 {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_BYTE4 = 0xDF;

const char SYSTEM_DUMMY_40 = 0x00;

/*
*| hmi_sys_caln.DTC_MASK_BYTE6 {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_BYTE6 = 0xF3;

/*
*| hmi_sys_caln.CPID_MASK_BYTE1 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CPID_MASK_BYTE1 = 0xFF;

/*
*| hmi_sys_caln.CPID_MASK_BYTE2 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CPID_MASK_BYTE2 = 0xC0;

/*
*| hmi_sys_caln.ISERVICEFAVORITESPROVIDER_REGISTRATION_MASK_BYTE1 {
*|      : is_calconst;
*|      : description = "To enable the Favorites Manager architecture to be �plug and play� the IServiceFavoritesProviders interface includes a RegisterFavoritesProvider function. This function is utilized by every FBlock with an IFavoritesProvider interface to register as a Favorites Provider.  Once a Favorites Provider is registered, the Favorites Manager will register for notification of changes to favorite information stored within that Favorites Provider.  In order for a device to be notified of changes to FBlock properties, it must register for notification via the Notification (0x001) property of the FBlock containing the properties for which notification of changes is required.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ISERVICEFAVORITESPROVIDER_REGISTRATION_MASK_BYTE1 = 0xEF;

/*
*| hmi_sys_caln.ENABLE_FAVORITES_BACKUP_MANAGER {
*|      : is_calconst;
*|      : description = "The Favorites Backup Manager is expected to reside in the module containing a user-accessible USB port.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_FAVORITES_BACKUP_MANAGER = 0x01;

const char SYSTEM_DUMMY_2 = 0x00;

const unsigned short SYSTEM_DUMMY_3[40] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

const char SYSTEM_DUMMY_18 = 0x00;

const char SYSTEM_DUMMY_4[3] =
 {
  0x00, 0x00, 0x00
 }
;

const unsigned short SYSTEM_DUMMY_17[40] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

const char SYSTEM_DUMMY_19 = 0x00;

const char SYSTEM_DUMMY_20 = 0x00;

const char SYSTEM_DUMMY_21 = 0x00;

const char SYSTEM_DUMMY_22 = 0x00;

/*
*| hmi_sys_caln.BUS_WAKEUP_DELAY_TIME {
*|      : is_calconst;
*|      : description = "The suppliers shall copy the calibration definitions in the GMLCal.c file into their calibration files that have the application defined calibrations. The calibration program editor shall feed these files into the applicable calibration program. The GMLAN Handler is capable of using the new value that was programmed into the flash memory instead of the hard coded default value in the GMLAN Hander.";
*|      : units = "ms";
*|      : transform = fixed.BUS_WAKEUP_DELAY_TIME;
*| } 
*/ 
const char BUS_WAKEUP_DELAY_TIME = 0x0A;

/*
*| hmi_sys_caln.HSGMLAN_TX_MESSAGE_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "The transmitting ECU GMLAN Handler software shall provide calibrations to enable or disable the transmit processing for each transmitted GMLAN message. The default values for these calibrations shall be �enabled�.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HSGMLAN_TX_MESSAGE_MASK_BYTE_1 = 0xFF;

/*
*| hmi_sys_caln.HSGMLAN_TX_MESSAGE_MASK_BYTE_2 {
*|      : is_calconst;
*|      : description = "The transmitting ECU GMLAN Handler software shall provide calibrations to enable or disable the transmit processing for each transmitted GMLAN message. The default values for these calibrations shall be �enabled�.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HSGMLAN_TX_MESSAGE_MASK_BYTE_2 = 0xFF;

const char SYSTEM_DUMMY_23[3] =
 {
  0x00, 0x00, 0x00
 }
;

const char SYSTEM_DUMMY_24[3] =
 {
  0x00, 0x00, 0x00
 }
;

const char SYSTEM_DUMMY_25[3] =
 {
  0x00, 0x00, 0x00
 }
;

const char SYSTEM_DUMMY_26[3] =
 {
  0x00, 0x00, 0x00
 }
;

const char SYSTEM_DUMMY_27[3] =
 {
  0x00, 0x00, 0x00
 }
;

const char SYSTEM_DUMMY_28[3] =
 {
  0x00, 0x00, 0x00
 }
;

const char SYSTEM_DUMMY_35[3] =
 {
  0x00, 0x00, 0x00
 }
;

const char SYSTEM_DUMMY_36[3] =
 {
  0x00, 0x00, 0x00
 }
;

const char SYSTEM_DUMMY_5[3] =
 {
  0x00, 0x00, 0x00
 }
;

/*
*| hmi_sys_caln.TX_DTC_TRIGGERED_788_MINIMUM_UPDATE_TIME {
*|      : is_calconst;
*|      : description = "Calibrations related with the HSGMLAN message DTC_Triggered_788 message transmit criteria.";
*|      : units = "ms";
*|      : transform = fixed.TX_DTC_TRIGGERED_788_MINIMUM_UPDATE_TIME;
*| } 
*/ 
const char TX_DTC_TRIGGERED_788_MINIMUM_UPDATE_TIME = 0x00;

/*
*| hmi_sys_caln.TX_DTC_TRIGGERED_788_PERIODIC_RATE {
*|      : is_calconst;
*|      : description = "Calibrations related with the HSGMLAN message DTC_Triggered_788 message transmit criteria.";
*|      : units = "ms";
*|      : transform = fixed.TX_DTC_TRIGGERED_788_PERIODIC_RATE;
*| } 
*/ 
const char TX_DTC_TRIGGERED_788_PERIODIC_RATE = 0x32;

/*
*| hmi_sys_caln.TX_DTC_TRIGGERED_788_DELAY_FOR_FIRST_PERIODIC_RATE {
*|      : is_calconst;
*|      : description = "Calibrations related with the HSGMLAN message DTC_Triggered_788 message transmit criteria.";
*|      : units = "ms";
*|      : transform = fixed.TX_DTC_TRIGGERED_788_DELAY_FOR_FIRST_PERIODIC_RATE;
*| } 
*/ 
const char TX_DTC_TRIGGERED_788_DELAY_FOR_FIRST_PERIODIC_RATE = 0x00;

const char SYSTEM_DUMMY_6[18] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_sys_caln.HSGMLAN_RX_MESSAGE_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "The receiving ECU GMLAN Handler software shall provide calibrations to enable or disable the receive processing for each received GMLAN message. The default values for these calibrations shall be �enabled�.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HSGMLAN_RX_MESSAGE_MASK_BYTE_1 = 0xFF;

/*
*| hmi_sys_caln.HSGMLAN_RX_MESSAGE_MASK_BYTE_2 {
*|      : is_calconst;
*|      : description = "The receiving ECU GMLAN Handler software shall provide calibrations to enable or disable the receive processing for each received GMLAN message. The default values for these calibrations shall be �enabled�.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HSGMLAN_RX_MESSAGE_MASK_BYTE_2 = 0xFF;

/*
*| hmi_sys_caln.HSGMLAN_RX_MESSAGE_MASK_BYTE_3 {
*|      : is_calconst;
*|      : description = "DTC Trigger byte based on HS-GMLAN";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HSGMLAN_RX_MESSAGE_MASK_BYTE_3 = 0xFF;

/*
*| hmi_sys_caln.FBLOCK_MESSAGE_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "Similar to GMAN requriment of all Rx / Tx having a Enable / Disable Calibration";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char FBLOCK_MESSAGE_MASK_BYTE_1 = 0xFF;

/*
*| hmi_sys_caln.FBLOCK_MESSAGE_MASK_BYTE_2 {
*|      : is_calconst;
*|      : description = "Similar to GMAN requriment of all Rx / Tx having a Enable / Disable Calibration";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char FBLOCK_MESSAGE_MASK_BYTE_2 = 0xFF;

/*
*| hmi_sys_caln.FBLOCK_MESSAGE_MASK_BYTE_3 {
*|      : is_calconst;
*|      : description = "Similar to GMAN requriment of all Rx / Tx having a Enable / Disable Calibration";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char FBLOCK_MESSAGE_MASK_BYTE_3 = 0xFF;

/*
*| hmi_sys_caln.FBLOCK_MESSAGE_MASK_BYTE_4 {
*|      : is_calconst;
*|      : description = "Similar to GMAN requriment of all Rx / Tx having a Enable / Disable Calibration";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char FBLOCK_MESSAGE_MASK_BYTE_4 = 0xFF;

/*
*| hmi_sys_caln.FBLOCK_MESSAGE_MASK_BYTE_5 {
*|      : is_calconst;
*|      : description = "Similar to GMAN requriment of all Rx / Tx having a Enable / Disable Calibration";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char FBLOCK_MESSAGE_MASK_BYTE_5 = 0xFF;

/*
*| hmi_sys_caln.FBLOCK_MESSAGE_MASK_BYTE_6 {
*|      : is_calconst;
*|      : description = "Similar to GMAN requriment of all Rx / Tx having a Enable / Disable Calibration";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char FBLOCK_MESSAGE_MASK_BYTE_6 = 0xFF;

/*
*| hmi_sys_caln.FBLOCK_MESSAGE_MASK_BYTE_7 {
*|      : is_calconst;
*|      : description = "Similar to GMAN requriment of all Rx / Tx having a Enable / Disable Calibration";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char FBLOCK_MESSAGE_MASK_BYTE_7 = 0xFF;

/*
*| hmi_sys_caln.SYSTEM_CONFIGURATION {
*|      : is_calconst;
*|      : description = "This section defines the system offerings that make up the Next-Generation Infotainment Architecture lineup.\
This calibration is the MAIN calibration used for system setup and configurations.  For example, the TLIN is enabled for 8\" and 10.2\" touch display systems whereas TLIN is disabled for 4.2\" configurations.\
\
For 4.2 inch the TLIN will never be on, because the SYSTEM_CONFIGURATION is used to determine if TLIN should be used or not. For 4.2 inch SYSTEM_CONFIGURATION is set to 1, which also disables TLIN. If SYSTEM_CONFIGURATION is set to 2, 3, 4 or 5 TLIN will be available.";
*|      : units = "ENM";
*|      : type = fixed.SYSTEM_CONFIGURATION;
*| } 
*/ 
const char SYSTEM_CONFIGURATION = 0x02;

/*
*| hmi_sys_caln.ODI_CUST_FU40_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU40_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU40_IND02_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU40_IND02_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU40_IND03_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU40_IND03_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU40_IND04_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU40_IND04_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU40_IND05_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU40_IND05_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU40_IND06_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU40_IND06_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU40_IND07_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU40_IND07_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU40_IND08_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU40_IND08_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU41_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU41_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU41_IND02_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU41_IND02_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU41_IND03_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU41_IND03_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU41_IND04_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU41_IND04_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU41_IND05_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU41_IND05_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU41_IND06_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU41_IND06_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND02_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND02_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND03_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND03_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND04_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND04_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND05_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND05_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND06_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND06_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND07_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND07_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND08_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND08_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND09_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND09_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND10_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND10_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU42_IND11_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU42_IND11_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU43_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU43_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU43_IND02_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU43_IND02_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU43_IND03_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU43_IND03_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND02_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND02_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND03_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND03_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND04_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND04_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND05_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND05_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND06_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND06_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND07_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND07_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND08_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND08_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND09_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND09_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND10_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND10_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND11_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND11_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND12_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND12_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND13_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND13_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU44_IND14_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU44_IND14_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU45_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU45_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU45_IND02_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU45_IND02_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU45_IND03_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU45_IND03_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU45_IND04_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU45_IND04_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU45_IND05_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU45_IND05_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU45_IND06_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU45_IND06_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU45_IND07_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU45_IND07_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU46_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU46_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU46_IND02_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU46_IND02_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU46_IND03_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU46_IND03_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU46_IND04_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU46_IND04_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU47_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU47_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU47_IND02_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU47_IND02_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU48_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU48_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU48_IND02_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU48_IND02_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU50_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU50_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.ODI_CUST_FU51_IND01_ENABLED {
*|      : is_calconst;
*|      : description = "Display of each customization setting shall be enabled/disabled using ODIIndications or dedicated GMLAN signals from the remote module settings owners.  The Infotainment HMI software shall process these enable flags as follows:\
1. The HMIModule or Base Radio Silverbox shall receive and store the enable flags.\
2. All stored Indication flags shall be cleared upon shut down of the Infotainment Virtual Network.\
3. The HMIModule or Base Radio Silverbox  shall implement Flash ROM calibrations to override the visibility of each customization setting.  These calibrations are defined in GIS-344 in the System Calibration Block requirement.  Each flag is named as:\
ODI_CUST_FUxx_INDyy_ENABLED, where XX is the FUClass ID and YY is the number of the specific Indication for that setting defined in the respective FUClass reports.\
5. The HMIModule or Base Radio Software HMI software shall show or hide each customization setting based on the state of the following expression (evaluated...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ODI_CUST_FU51_IND01_ENABLED = 0x01;

/*
*| hmi_sys_caln.DEREGISTER_DURATION {
*|      : is_calconst;
*|      : description = "GIS-358-7100- Constants\
Parameters not affecting user interaction. \
";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DEREGISTER_DURATION = 0x01;

/*
*| hmi_sys_caln.PROGRAMMING_MASTER {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations \
Parameter affecting user interaction.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PROGRAMMING_MASTER = 0x01;

/*
*| hmi_sys_caln.PWM_FAN_LEVEL1 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PWM_FAN_LEVEL1 = 0x1e;

/*
*| hmi_sys_caln.PWM_FAN_LEVEL2 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PWM_FAN_LEVEL2 = 0x2C;

/*
*| hmi_sys_caln.PWM_FAN_LEVEL3 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PWM_FAN_LEVEL3 = 0x64;

/*
*| hmi_sys_caln.PWM_FAN_LEVEL4 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PWM_FAN_LEVEL4 = 0x64;

/*
*| hmi_sys_caln.PWM_FAN_LEVEL5 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PWM_FAN_LEVEL5 = 0x64;

/*
*| hmi_sys_caln.PWM_FAN_LEVEL6 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PWM_FAN_LEVEL6 = 0x64;

/*
*| hmi_sys_caln.T_SENSOR1_ENTRY_LEVEL_1 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_ENTRY_LEVEL_1 = 0x4a;

/*
*| hmi_sys_caln.T_SENSOR1_ENTRY_LEVEL_2 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_ENTRY_LEVEL_2 = 0x4c;

/*
*| hmi_sys_caln.T_SENSOR1_ENTRY_LEVEL_3 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_ENTRY_LEVEL_3 = 0x4e;

/*
*| hmi_sys_caln.T_SENSOR1_ENTRY_LEVEL_4 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_ENTRY_LEVEL_4 = 0x4e;

/*
*| hmi_sys_caln.T_SENSOR1_ENTRY_LEVEL_5 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_ENTRY_LEVEL_5 = 0x4e;

/*
*| hmi_sys_caln.T_SENSOR1_ENTRY_LEVEL_6 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_ENTRY_LEVEL_6 = 0x4e;

/*
*| hmi_sys_caln.T_SENSOR1_EXIT_LEVEL_1 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_EXIT_LEVEL_1 = 0x47;

/*
*| hmi_sys_caln.T_SENSOR1_EXIT_LEVEL_2 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_EXIT_LEVEL_2 = 0x49;

/*
*| hmi_sys_caln.T_SENSOR1_EXIT_LEVEL_3 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_EXIT_LEVEL_3 = 0x4b;

/*
*| hmi_sys_caln.T_SENSOR1_EXIT_LEVEL_4 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_EXIT_LEVEL_4 = 0x4b;

/*
*| hmi_sys_caln.T_SENSOR1_EXIT_LEVEL_5 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_EXIT_LEVEL_5 = 0x4b;

/*
*| hmi_sys_caln.T_SENSOR1_EXIT_LEVEL_6 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR1_EXIT_LEVEL_6 = 0x4b;

/*
*| hmi_sys_caln.T_SENSOR2_ENTRY_LEVEL_1 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_ENTRY_LEVEL_1 = 0x43;

/*
*| hmi_sys_caln.T_SENSOR2_ENTRY_LEVEL_2 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_ENTRY_LEVEL_2 = 0x45;

/*
*| hmi_sys_caln.T_SENSOR2_ENTRY_LEVEL_3 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_ENTRY_LEVEL_3 = 0x47;

/*
*| hmi_sys_caln.T_SENSOR2_ENTRY_LEVEL_4 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_ENTRY_LEVEL_4 = 0x47;

/*
*| hmi_sys_caln.T_SENSOR2_ENTRY_LEVEL_5 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_ENTRY_LEVEL_5 = 0x47;

/*
*| hmi_sys_caln.T_SENSOR2_ENTRY_LEVEL_6 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_ENTRY_LEVEL_6 = 0x47;

/*
*| hmi_sys_caln.T_SENSOR2_EXIT_LEVEL_1 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_EXIT_LEVEL_1 = 0x40;

/*
*| hmi_sys_caln.T_SENSOR2_EXIT_LEVEL_2 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_EXIT_LEVEL_2 = 0x42;

/*
*| hmi_sys_caln.T_SENSOR2_EXIT_LEVEL_3 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_EXIT_LEVEL_3 = 0x44;

/*
*| hmi_sys_caln.T_SENSOR2_EXIT_LEVEL_4 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_EXIT_LEVEL_4 = 0x44;

/*
*| hmi_sys_caln.T_SENSOR2_EXIT_LEVEL_5 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_EXIT_LEVEL_5 = 0x44;

/*
*| hmi_sys_caln.T_SENSOR2_EXIT_LEVEL_6 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR2_EXIT_LEVEL_6 = 0x44;

/*
*| hmi_sys_caln.T_SENSOR_MAX_DTC_HOT {
*|      : is_calconst;
*|      : description = "";
*|      : units = "degree";
*|      : type = fixed.UB0;
*| } 
*/ 
const char T_SENSOR_MAX_DTC_HOT = 0x4b;

/*
*| hmi_sys_caln.ENHANCED_FAN_HANDLING_ON_OFF {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Fan Control Levels";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ENHANCED_FAN_HANDLING_ON_OFF = 0x02;

/*
*| hmi_sys_caln.XM_INITIAL_DATA_SID_REQUEST {
*|      : is_calconst;
*|      : description = "Initial Data SID to request Data Services from XM/Sirrus Receiver\
Note: \
HCMOS90 - Max SID ID is 255\
Next Gen Module will have max SID ID of 1024";
*|      : units = "SID ID";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short XM_INITIAL_DATA_SID_REQUEST = 0x00D1;

/*
*| hmi_sys_caln.SWMI_CALIBRATION_DATA_FILE_HMI_SYSTEMS_CAL {
*|      : is_calconst;
*|      : description = "The NoCalibration state is defined in order to make sure that Infotainment subsystem components have been updated with calibrations after a service event such as replacing one or more modules or upgrading software.  The mechanism for NoCalibration determination is the same as that for Theftlock and NoVIN determination.  That is, at initialization a module detects whether or not it has a valid calibration.  If it does not, it notifies the SystemState FBlock via the SystemState.SetNoCalibrationModuleState method.  Once a valid calibration is received, the module calls the SystemState.SetNoCalibrationModuleState method to clear the condition.\
The SystemState FBlock shall assume all module calibrations are valid upon each Sleep cycle.  Modules that already have a valid calibration or do not support NoCalibration detection (determined by each module CTS) do not need to report the NoCalibration clear condition at each initialization.\
Note: each of these Calibrations should be ...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SWMI_CALIBRATION_DATA_FILE_HMI_SYSTEMS_CAL = 0x00;

/*
*| hmi_sys_caln.TLIN_MIN_SYS_COMMAND_UPDATE_TIME {
*|      : is_calconst;
*|      : description = "There's a min update time value in GIS-309 for the System Command Message that carries brightness updates to the display. This min update was hard-coded to 56 ms in earlier versions of the spec, but in newer versions of GIS 309 this shall be a HMIModule cal instead with a 56 ms default.  It's a timer within the HMIModule that holds off System Command Messages in order to avoid excessive disruption to the position, velocity, and Haptic command updates from/to the display on the TLIN interface.\
";
*|      : units = "ms";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TLIN_MIN_SYS_COMMAND_UPDATE_TIME = 0x38;

/*
*| hmi_sys_caln.ENABLE_BOSCH_SPECIFIC_HMI_UUDT_USDT {
*|      : is_calconst;
*|      : description = "The HMI Module shall implement a Calibration �ENABLE_BOSCH_SPECIFIC_HMI_UUDT_USDT� to enable and disable all $548 UUDT and $648 USDT responses to either a $248 or $101 Request. Currently, $248/$548/$648 are defined as free in the GMLAN Data Dictionary, but if GM at a later date defines a new module to those ID�s then the HMI Module must calibrate the �ENABLE_BOSCH_SPECIFIC_HMI_UUDT_USDT� to DISABLED for Production";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_BOSCH_SPECIFIC_HMI_UUDT_USDT = 0x00;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_MP3 {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_MP3 = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_WMA {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_WMA = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_MP4 {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_MP4 = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_M4A {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_M4A = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_AAC {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_AAC = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_AA {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_AA = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_APPLELOSSLESS {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_APPLELOSSLESS = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_OGG {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_OGG = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_WAV {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_WAV = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_3GP {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_3GP = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_3G2 {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_3G2 = 0x01;

/*
*| hmi_sys_caln.ENABLE_AUDIO_ENCODING_FORMAT_AIFF {
*|      : is_calconst;
*|      : description = "The HMI module shall support the following audio encoding formats:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_ENCODING_FORMAT_AIFF = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_MPEG1_MPG {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_MPEG1_MPG = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_MPEG1_MPEG {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_MPEG1_MPEG = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_MPEG1_MP4 {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_MPEG1_MP4 = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_MPEG2_MP4 {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_MPEG2_MP4 = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_MPEG4_MP4 {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_MPEG4_MP4 = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_H264_MP4 {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_H264_MP4 = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_H264_M4V {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_H264_M4V = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_MPEG4_AVC_MP4 {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_MPEG4_AVC_MP4 = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_MPEG4_AVC_M4V {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_MPEG4_AVC_M4V = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_WMV_WMV {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_WMV_WMV = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_WMV_AVI {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_WMV_AVI = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_H263_3GP {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_H263_3GP = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_DIVX_DIVX {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_DIVX_DIVX = 0x00;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_NERO_MP4 {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_NERO_MP4 = 0x01;

/*
*| hmi_sys_caln.ENABLE_VIDEO_ENCODING_FORMAT_FLASHVIDEO_FLV {
*|      : is_calconst;
*|      : description = "The following video encoding formats shall be supported:";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VIDEO_ENCODING_FORMAT_FLASHVIDEO_FLV = 0x01;

/*
*| hmi_sys_caln.PROGRAMMING_ENABLE {
*|      : is_calconst;
*|      : description = "GIS-358-7200 ";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PROGRAMMING_ENABLE = 0x01;

/*
*| hmi_sys_caln.SPP_BUFFER_SIZE {
*|      : is_calconst;
*|      : description = "It is for the number of messages from the phone that the Framework will buffer for the SPP Aggregator app";
*|      : units = "Messages";
*|      : transform = fixed.SPP_BUFFER_SIZE;
*| } 
*/ 
const char SPP_BUFFER_SIZE = 0x02;

/*
*| hmi_sys_caln.MAX_SPP_MESSAGE_LENGTH {
*|      : is_calconst;
*|      : description = " the maximum number of bytes in an SPP message. ";
*|      : units = "Bytes";
*|      : transform = fixed.MAX_SPP_MESSAGE_LENGTH;
*| } 
*/ 
const char MAX_SPP_MESSAGE_LENGTH = 0x19;

/*
*| hmi_sys_caln.MAX_ICON_NAME_LENGTH {
*|      : is_calconst;
*|      : description = "the maximum length of an application's name on the home screen";
*|      : units = "Characters";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MAX_ICON_NAME_LENGTH = 0x1E;

/*
*| hmi_sys_caln.NOTIFICATION_DELAY_TIMER {
*|      : is_calconst;
*|      : description = "GIS-307-1187 Sequencing Registration for Notification\
In order to avoid excessive control message traffic, and hence an increase in the probability of LLR�s  at network startup/restart, registration for properties prior to the expiration of the NotificationDelayTimer is limited.\
General properties for  which registration for notification is allowed prior to the expiration of the NotificationDelayTimer are indicated in the table below.\
Furthermore, prior to the expiration of the NotificationDelayTimer, all controllers shall monitor the XXXChannelStatus notifications from the AVManager to find out the restored source.   Once controllers see an active source on the MAIN, REAR_MAIN, LVM or HEADPHONE_1/2 channels in the XXXChannelStatus.Status messages, the controllers may immediately register for all Properties and/or call any methods on the FBlock implementing that souce.  Controllers shall delay notification requests from all other inactive sources until after the expirat...";
*|      : units = "Bytes";
*|      : transform = fixed.NOTIFICATION_DELAY_TIMER;
*| } 
*/ 
const char NOTIFICATION_DELAY_TIMER = 0x19;

/*
*| hmi_sys_caln.CSHMI_TOUCH_EVENT_BUFFER_TIME {
*|      : is_calconst;
*|      : description = "To minimize the message load inside the HMIModule, this parameter gives the time in which touch events are buffered before they will be sent to the HMI thread. The proximity event or the first touch event (start of a gesture) are sent immediately. A release event will trigger the sending of the events buffered so far even if the time for buffering has not ended.";
*|      : units = "ms";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CSHMI_TOUCH_EVENT_BUFFER_TIME = 0x32;

/*
*| hmi_sys_caln.CSHMI_AUX_TOUCH_EVENT_BUFFER_TIME {
*|      : is_calconst;
*|      : description = "To minimize the message load on the MOST network in case the RSE is active on the CenterStackDisplay, this parameter gives the time in which touch events are buffered before they will be sent to the RSE-System. The proximity event or the first touch event (start of a gesture) are sent immediately. A release event will trigger the sending of the events buffered so far even if the time for buffering has not ended.";
*|      : units = "ms";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CSHMI_AUX_TOUCH_EVENT_BUFFER_TIME = 0x64;

const char SYSTEM_DUMMY_7 = 0x00;

const unsigned short SYSTEM_DUMMY_8[40] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

/*
*| hmi_sys_caln.CONSENT_PROCEED_TIMER {
*|      : is_calconst;
*|      : description = "GIS-358-7100- Constants";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CONSENT_PROCEED_TIMER = 0x1E;

/*
*| hmi_sys_caln.PROGRAMMING_RESET_TIMER {
*|      : is_calconst;
*|      : description = "GIS-358-7100- Constants";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PROGRAMMING_RESET_TIMER = 0x0A;

/*
*| hmi_sys_caln.WAIT_TO_END {
*|      : is_calconst;
*|      : description = "GIS-358-7100- Constants";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char WAIT_TO_END = 0x02;

/*
*| hmi_sys_caln.WAITING_FOR_FILE {
*|      : is_calconst;
*|      : description = "GIS-358-7100- Constants";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char WAITING_FOR_FILE = 0xFF;

/*
*| hmi_sys_caln.MAX_PROG_FILE_SIZE_IPC {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_PROG_FILE_SIZE_IPC = 0x00007D00;

/*
*| hmi_sys_caln.MAX_PROG_FILE_SIZE_SBX {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_PROG_FILE_SIZE_SBX = 0x00001F40;

/*
*| hmi_sys_caln.MAX_PROG_FILE_SIZE_HMI {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_PROG_FILE_SIZE_HMI = 0x003D0900;

/*
*| hmi_sys_caln.MAX_PROG_FILE_SIZE_RSE {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_PROG_FILE_SIZE_RSE = 0x00007D00;

/*
*| hmi_sys_caln.MAX_PROG_FILE_SIZE_CD {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_PROG_FILE_SIZE_CD = 0x000001F4;

/*
*| hmi_sys_caln.MAX_PROG_FILE_SIZE_AMP {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_PROG_FILE_SIZE_AMP = 0x00007D00;

/*
*| hmi_sys_caln.MANIFEST_NOTICE {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "ms";
*|      : transform = fixed.MANIFEST_NOTICE;
*| } 
*/ 
const char MANIFEST_NOTICE = 0xFF;

/*
*| hmi_sys_caln.PROGRAMMING_RETRY_COUNT {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "Integer";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PROGRAMMING_RETRY_COUNT = 0x01;

/*
*| hmi_sys_caln.MAX_OPERATIONAL_PROG_FILE_SIZE_IPC {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_OPERATIONAL_PROG_FILE_SIZE_IPC = 0x00007D00;

/*
*| hmi_sys_caln.MAX_OPERATIONAL_PROG_FILE_SIZE_SBX {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_OPERATIONAL_PROG_FILE_SIZE_SBX = 0x00001F40;

/*
*| hmi_sys_caln.MAX_OPERATIONAL_PROG_FILE_SIZE_HMI {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_OPERATIONAL_PROG_FILE_SIZE_HMI = 0x003D0900;

/*
*| hmi_sys_caln.MAX_OPERATIONAL_PROG_FILE_SIZE_RSE {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_OPERATIONAL_PROG_FILE_SIZE_RSE = 0x00007D00;

/*
*| hmi_sys_caln.MAX_OPERATIONAL_PROG_FILE_SIZE_CD {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_OPERATIONAL_PROG_FILE_SIZE_CD = 0x000001F4;

/*
*| hmi_sys_caln.MAX_OPERATIONAL_PROG_FILE_SIZE_AMP {
*|      : is_calconst;
*|      : description = "GIS-358-7200 - Calibrations";
*|      : units = "KBytes";
*|      : type = fixed.UL0;
*| } 
*/ 
const unsigned long MAX_OPERATIONAL_PROG_FILE_SIZE_AMP = 0x00007D00;

/*
*| hmi_sys_caln.USB_VOLTAGE_RECOVERY_AUTOPLAY_DELAY_TIME {
*|      : is_calconst;
*|      : description = "From the changes that we are going to make for the USB to account for the 1.0 Amp of charge.  If and when the voltage goes below the support threshold for this operation.  We are going to have to input a autoplay feature to resume audio playback of the USB devices.  This is a delay time after the voltage returns to normal that the USB Autoplay feature will be delayed.";
*|      : units = "ms";
*|      : transform = fixed.USB_VOLTAGE_RECOVERY_AUTOPLAY_DELAY_TIME;
*| } 
*/ 
const char USB_VOLTAGE_RECOVERY_AUTOPLAY_DELAY_TIME = 0x3C;

/*
*| hmi_sys_caln.IR_PAUSE_CONNECTION_TIMEOUT {
*|      : is_calconst;
*|      : description = "Whenever a user changes sources or mutes the audio for an internet radio station we enter the \"pause\" state. But, if the internet radio station is a broadcast station, which they often are, then the audio is actually muted, not stopped. As such the data from the broadcast continues to be streamed down to the vehicle eating up data. This is OK if the user is muted for a few minutes, such as for a short phone call. But, it is probably undesirable for this data to continue for a long time, such as if the user changes to FM without disconnecting the radio station. So, the timeout is the length of time before the Internet Radio station connection is broken and will have to be re-established.\
";
*|      : units = "Enumeration";
*|      : type = fixed.IR_PAUSE_CONNECTION_TIMEOUT;
*| } 
*/ 
const char IR_PAUSE_CONNECTION_TIMEOUT = 0x04;

/*
*| hmi_sys_caln.WAITING_ON_GMLAN {
*|      : is_calconst;
*|      : description = "GIS-358-7100";
*|      : units = "ms";
*|      : transform = fixed.WAITING_ON_GMLAN;
*| } 
*/ 
const char WAITING_ON_GMLAN = 0x08;

/*
*| hmi_sys_caln.WAITING_ON_CLIENT {
*|      : is_calconst;
*|      : description = "If programming master does not receive any request while it is waiting for the client to advance the following protocol elements within WAITING-ON-CLIENT duration, it shall continue processing that manifest file. - See GIS-358 for additional details.\
Typical values range from 1 second to 5 seconds per GIS-358-7100";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char WAITING_ON_CLIENT = 0x32;

/*
*| hmi_sys_caln.WAITING_FOR_PARKING {
*|      : is_calconst;
*|      : description = "GIS-358-7100 ";
*|      : units = "ms";
*|      : transform = fixed.WAITING_FOR_PARKING;
*| } 
*/ 
const char WAITING_FOR_PARKING = 0x64;

/*
*| hmi_sys_caln.PROGRAMMING_COMPLETE {
*|      : is_calconst;
*|      : description = "GIS-358-7100 ";
*|      : units = "ms";
*|      : transform = fixed.PROGRAMMING_COMPLETE;
*| } 
*/ 
const char PROGRAMMING_COMPLETE = 0x3C;

const char SYSTEM_DUMMY_29 = 0x00;

/*
*| hmi_sys_caln.WAIT_USER_LOGIN {
*|      : is_calconst;
*|      : description = "It is a timer for how long to wait to see if a user logs in before merely keeping the previously logged in user as the user";
*|      : units = "Enumeration";
*|      : type = fixed.WAIT_USER_LOGIN;
*| } 
*/ 
const char WAIT_USER_LOGIN = 0x00;

const char SYSTEM_DUMMY_9[6] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_sys_caln.APINFO1_CONSECUTIVE_READS {
*|      : is_calconst;
*|      : description = "Period of time to check the LVDS-Bit in the status byte before setting the DTC 127E.";
*|      : units = "mS";
*|      : transform = fixed.APINFO1_CONSECUTIVE_READS;
*| } 
*/ 
const char APINFO1_CONSECUTIVE_READS = 0x64;

const unsigned short SYSTEM_DUMMY_10[256] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

/*
*| hmi_sys_caln.HSGMLAN_RX_MESSAGE_MASK_BYTE_4 {
*|      : is_calconst;
*|      : description = "DTC Trigger byte based on HS-GMLAN";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HSGMLAN_RX_MESSAGE_MASK_BYTE_4 = 0xFF;

/*
*| hmi_sys_caln.HSGMLAN_RX_MESSAGE_MASK_BYTE_5 {
*|      : is_calconst;
*|      : description = "DTC Trigger byte based on HS-GMLAN";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HSGMLAN_RX_MESSAGE_MASK_BYTE_5 = 0xFF;

/*
*| hmi_sys_caln.HSGMLAN_RX_MESSAGE_MASK_BYTE_6 {
*|      : is_calconst;
*|      : description = "DTC Trigger byte based on HS-GMLAN";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char HSGMLAN_RX_MESSAGE_MASK_BYTE_6 = 0xFF;

const char SYSTEM_DUMMY_11 = 0x00;

/*
*| hmi_sys_caln.LSGMLAN_RX_BYTE_DTC_TRIGGER {
*|      : is_calconst;
*|      : description = "DTC Trigger byte based on LS-GMLAN";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char LSGMLAN_RX_BYTE_DTC_TRIGGER = 0x01;

const char SYSTEM_DUMMY_30 = 0x00;

const unsigned short SYSTEM_DUMMY_12[40] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

const unsigned short SYSTEM_DUMMY_13[40] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

const char SYSTEM_DUMMY_31 = 0x00;

/*
*| hmi_sys_caln.INTERNET_FRAMEWORK_MONITORING_APPS {
*|      : is_calconst;
*|      : description = "The Framework shall support at most two Monitoring applications running at a time. This calibration shall set the number (2) in this sentence.";
*|      : units = "Apps";
*|      : type = fixed.UB0;
*| } 
*/ 
const char INTERNET_FRAMEWORK_MONITORING_APPS = 0x03;

const char SYSTEM_DUMMY_32 = 0x00;

/*
*| hmi_sys_caln.MAX_CACHE_SIZE {
*|      : is_calconst;
*|      : description = "The maximum number of MB to use as cache ";
*|      : units = "MB";
*|      : transform = fixed.MAX_CACHE_SIZE;
*| } 
*/ 
const char MAX_CACHE_SIZE = 0x05;

const char SYSTEM_DUMMY_33 = 0x00;

/*
*| hmi_sys_caln.INTERNET_SR_SESSION_TIMEOUT {
*|      : is_calconst;
*|      : description = "Defines the amount of time an app has between when it calls to start a speech rec session (and essentially is requesting the audio bus) and when it actually requests to utilize the microphone. This also applies between requests, i.e. if an app receives one \"result\" which could be a recording to send to the back end, for it to request to open the microphone to do another recording/SR request. ";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char INTERNET_SR_SESSION_TIMEOUT = 0x05;

const char SYSTEM_DUMMY_34 = 0x00;

const char SYSTEM_DUMMY_14 = 0x00;

/*
*| hmi_sys_caln.OSS_LINK {
*|      : is_calconst;
*|      : description = "The URL of the supplier stored Open Source License content located under the SETTINGS menu.";
*|      : units = "UTF-16";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short OSS_LINK[40] =
 {
  0x0068, 0x0074, 0x0074, 0x0070, 0x003a, 0x002f, 0x002f, 0x0078,
  0x002e, 0x0063, 0x006f, 0x002f, 0x0062, 0x006f, 0x0073, 0x0063,
  0x0068, 0x0063, 0x006d, 0x006f, 0x0073, 0x0073, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

/*
*| hmi_sys_caln.DISPLAY_VARIANT_ID {
*|      : is_calconst;
*|      : description = "The Display will provide its system variant number in this message.  This value will be used by the TLIN master to determine if the correct remote display is installed in the vehicle.  If the incorrect remote display is installed, the ILIN master shall set the appropriate error message / diagnostic messages.";
*|      : units = "ENUM";
*|      : type = fixed.DISPLAY_VARIANT_ID;
*| } 
*/ 
const char DISPLAY_VARIANT_ID = 0x00;

/*
*| hmi_sys_caln.SUBNET_CONFIG_LIST_TLIN_BYTE1 {
*|      : is_calconst;
*|      : description = "Used for Support of DID $BE ";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SUBNET_CONFIG_LIST_TLIN_BYTE1 = 0x81;

/*
*| hmi_sys_caln.SUBNET_CONFIG_LIST_TLIN_BYTE2 {
*|      : is_calconst;
*|      : description = "Used for Support of DID $BE ";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SUBNET_CONFIG_LIST_TLIN_BYTE2 = 0x80;

/*
*| hmi_sys_caln.HMI_MODULE_DISPLAY_CONFIG {
*|      : is_calconst;
*|      : description = "updated definition per Bosch's implementation to correctly document how the EOL value works.";
*|      : units = "ENM";
*|      : type = fixed.HMI_MODULE_DISPLAY_CONFIG;
*| } 
*/ 
const char HMI_MODULE_DISPLAY_CONFIG = 0x02;

/*
*| hmi_sys_caln.LVDS_LEGNTH_MODE {
*|      : is_calconst;
*|      : description = "updated definition per Bosch's implementation to correctly document how the EOL value works.\
\
 the LVDS_LENGTH_MODE as voltage levels for VODSEL (0 is an invalid voltage level so it will be interpreted as low (~0.5V), 1 is low (~0.5V), and 2 is high (~0.9V))\
\
The video source must support a cable length between the video source and the display from 0.2 meters to 10 meters with connectors.\
";
*|      : units = "LVDS mode";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LVDS_LEGNTH_MODE = 0x02;

/*
*| hmi_sys_caln.LVDS_DEEMPHASIS {
*|      : is_calconst;
*|      : description = "Per National Semiconductor datasheets and supplier feedback, the LVDS De-Emphasis must be set properly to match display receivers and cable lengths and the number of in-line connectors.\
\
The HMI Module is able to control the serializer chip in detail: the two voltage level (VODSEL), all eight De-Emphasis levels, and the four receiver configurations are supported.  \
\
Adjustments on both settings, the De-Emphasis and the VODSEL, need to be done.";
*|      : units = "ENM";
*|      : type = fixed.LVDS_DEEMPHASIS;
*| } 
*/ 
const char LVDS_DEEMPHASIS = 0x03;

/*
*| hmi_sys_caln.USB_1_HUB_ENABLE {
*|      : is_calconst;
*|      : description = "Use in DTC Failure Criteria";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char USB_1_HUB_ENABLE = 0x01;

/*
*| hmi_sys_caln.USB_2_HUB_ENABLE {
*|      : is_calconst;
*|      : description = "Use in DTC Failure Criteria";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char USB_2_HUB_ENABLE = 0x01;

/*
*| hmi_sys_caln.USB_1_HUB_MAX_PORTS {
*|      : is_calconst;
*|      : description = "Use in DTC Failure Criteria.";
*|      : units = "Ports";
*|      : type = fixed.UB0;
*| } 
*/ 
const char USB_1_HUB_MAX_PORTS = 0x07;

/*
*| hmi_sys_caln.USB_2_HUB_MAX_PORTS {
*|      : is_calconst;
*|      : description = "Use in DTC Failure Criteria.";
*|      : units = "Ports";
*|      : type = fixed.UB0;
*| } 
*/ 
const char USB_2_HUB_MAX_PORTS = 0x07;

/*
*| hmi_sys_caln.SYSTEM_OFFERING_BYTE1 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SYSTEM_OFFERING_BYTE1 = 0xF9;

/*
*| hmi_sys_caln.TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_MINIMUM_UPDATE_TIME {
*|      : is_calconst;
*|      : description = "The table below specifies the GMLAN Handler transmitted message calibrations to be provided.";
*|      : units = "ms";
*|      : transform = fixed.TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_MINIMUM_UPDATE_TIME;
*| } 
*/ 
const char TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_MINIMUM_UPDATE_TIME = 0x00;

/*
*| hmi_sys_caln.TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_PERIODIC_RATE {
*|      : is_calconst;
*|      : description = "The table below specifies the GMLAN Handler transmitted message calibrations to be provided.";
*|      : units = "ms";
*|      : transform = fixed.TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_PERIODIC_RATE;
*| } 
*/ 
const char TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_PERIODIC_RATE = 0x05;

/*
*| hmi_sys_caln.TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_DELAY_FOR_FIRST_PERIODIC_RATE {
*|      : is_calconst;
*|      : description = "The table below specifies the GMLAN Handler transmitted message calibrations to be provided.";
*|      : units = "ms";
*|      : transform = fixed.TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_DELAY_FOR_FIRST_PERIODIC_RATE;
*| } 
*/ 
const char TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_DELAY_FOR_FIRST_PERIODIC_RATE = 0x00;

const char SYSTEM_DUMMY_39 = 0x00;

/*
*| hmi_sys_caln.NO_SWITCH_PROG_MODE {
*|      : is_calconst;
*|      : description = "Please see latest GIS-358 for clarification.\
\"GIS-358-1221 No Client response received\"\
2. If programming master does not receive any request while it is NO_SWITCH_PROG_MODEwaiting for the client to advance the following protocol elements within WAITING-ON-CLIENT duration, it shall continue processing that manifest file.  10:01 AM \
4. If a client does not respond within NO-SWITCH-PROG-MODE time when a switch to programming mode is expected, after receiving SwitchProgMode.ResultAck(SUCCESS) and before requesting GetProgFile.StartResultAck() the master shall:\
a. Abort processing that manifest file\
b. and update  the manifest file name and status in non-volatile memory.  These stored records will be used  to display locally ( GIS-400, 9.17.1.9 or GIS-402, 9.10.1.3). \
c. For remote display the master shall send  ManifestFileProgrammingResult.Status(FAILURE).\
d. Retry is not required.\
 \
";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char NO_SWITCH_PROG_MODE = 0x01;

/*
*| hmi_sys_caln.CAL_HMI_SYSTEM_END {
*|      : is_calconst;
*|      : description = "END OF CAL BLOCK";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CAL_HMI_SYSTEM_END = 0x00;
