/**********************************************************FileHeaderBegin******
 *
 * FILE:        EOLLib.c
 *
 * CREATED:     2010-10-12
 *
 * AUTHOR:
 *
 * DESCRIPTION: -
 *
 * NOTES: -
 *
 * COPYRIGHT:  (c) 2005 Technik & Marketing Service GmbH
 *
 **********************************************************FileHeaderEnd*******/

/*****************************************************************************
 *
 * search for this KEYWORDS to jump over long histories:
 *
 * CONSTANTS - TYPES - MACROS - VARIABLES - FUNCTIONS - PUBLIC - PRIVATE
 *
 ******************************************************************************/

/******************************************************************************
 * LOG:
 *
 * $Log: $
 ******************************************************************************/


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/

#include "OsalConf.h"

#include "trace_interface.h"
/* OSAL Device header */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"

/* EOL table and offset definitions */
#include "EOLLib.h"

/* Needed for Trace */
#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#define ET_TRACE_INFO_ON
#include "etrace_if.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_DEV_DIAGEOL
#include "trcGenProj/Header/EOLLib.c.trc.h"
#endif

#if 0
enum tenTrcTraceClass {
   TR_CLASS_DEV_DIAGEOL = (0x00E7)  // trace class
};
#endif


typedef enum
{
   SYSTEM            =   2,
   DISPLAY_INTERFACE =   3,
   BLUETOOTH         =   4,
   NAVIGATION_SYSTEM =   5,
   NAVIGATION_ICON   =   6,
   BRAND             =   7,
   COUNTRY           =   8,
   SPEECH_RECOGNITION=   9,
   HAND_FREE_TUNING  =   10,
   REAR_VISION_CAMERA=   11,
} EOLLib_tenTable;


#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/


/*defines for shared memory*/
#define EOLLIB_C_SEM_NAME                       "EOLLIB_SHME_SEM"
#define EOLLIB_C_SHMEM_NAME                     "EOLLIB_SHME"

/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/

#define OSAL_C_S32_IOCTRL_DIAGEOL_RESET_TARGET_DEFAULTS     0x101
#define OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC                 0x102
#define OSAL_C_S32_IOCTRL_DIAGEOL_RESET_DEFAULTS            0x103


/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/

typedef struct sDiagEOLBlock
{
    tU8     u8TableId;
    tPCS8   ps8TableName;
    OSAL_tIODescriptor hFile;
    tenFDDDataSet enDataSet;
    int      hDev;
	tU8		Header[14]; 
    tU16    u16Length;
    tU8    pu8Data[U8_EOLLIB_MAX_BLOCK_LENGTH];
    EOLLib_ParameterRange arParameterRange[U8_EOLLIB_MAX_BLOCK_LENGTH];
} tsDiagEOLBlock;

typedef struct
{
  //tU8               u8MagicSharedMemoryLinux;
  //tU8               u8MagicSharedMemoryTengine;
  //tU32              u32KDSNumberOfEntries;
	tU8			u8ShMemInitiated;
  tsDiagEOLBlock    sDiagEOLBlocks[EOL_BLOCK_NBR];  
}tsEOLLIBSharedMemory;


/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/


/*****************************************************************
| static variable declaration
|----------------------------------------------------------------*/
#ifdef IOSC_ACTIV
#define IOSC_FOR_LINUX
#include "iosc_userspace_lib.h"
#include "iosc_config.h"
static tS32      			vsEOLLibHandleSemaphore = OSAL_C_INVALID_HANDLE;
#else
static OSAL_tSemHandle      vsEOLLibHandleSemaphore = OSAL_C_INVALID_HANDLE;
#endif
static OSAL_tShMemHandle    vsEOLLibHandleSharedMemory = OSAL_ERROR;

/*****************************************************************
| function prototypes (scope: local)
|----------------------------------------------------------------*/

static       tsEOLLIBSharedMemory* psEOLLIBLock(void);
static       void               vEOLLIBUnLock(tsEOLLIBSharedMemory* PtsShMemEOLLIB);
static tsDiagEOLBlock* psGetBlock(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8 u8TableId);


static tVoid DRV_DIAG_EOL_vTraceCommand(unsigned char* PubpData)
{
//    vTraceMsg(0x0B01, TR_LEVEL_USER_4);

/*#if 0
	switch (PubpData[1])
	{
		case 1: //DEV_DIAGEOL_RESET_TARGET_DEFAULTS
			{
//				vTraceMsg(0x0B03, TR_LEVEL_USER_4);

				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_TUNER), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_AUDIO), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_EQUALIZATION), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_SYSTEM), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_LIGHTING), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BUTTON), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_COUNTRY), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BRAND), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_RVC), 0);
			}
			break;

		case 2: //DEV_DIAGEOL_RESET_DEFAULTS %i(EOL_DEFAULTS)
			{
//				vTraceMsg(0x0B04, TR_LEVEL_USER_4);

				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_TUNER), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_AUDIO), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_EQUALIZATION), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_SYSTEM), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_LIGHTING), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BUTTON), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_COUNTRY), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BRAND), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_RVC), PubpData[2]);
			}
			break;
	}
#endif*/

//    vTraceMsg(0x0B02, TR_LEVEL_USER_4);
}


static tVoid DRV_DIAG_EOL_vTraceRegChannel(void)
{ 
	tS32 s32Trace;
	tS32 s32Size;
	LaunchChannel  oTraceChannel;

	oTraceChannel.enTraceChannel = TR_TTFIS_DEV_DIAGEOL;
	oTraceChannel.p_Callback = (OSAL_tpfCallback)DRV_DIAG_EOL_vTraceCommand;
	/*if ((s32Trace = tk_opn_dev((unsigned char*)"trcttfis", TD_UPDATE)) > E_OK)
	{
		if(tk_swri_dev(s32Trace,DN_TRCCALLBACKREG, &oTraceChannel, sizeof(LaunchChannel), &s32Size) < E_OK)
		{
//			vTraceMsg(0x0A04, TR_LEVEL_USER_4);
		}
	}   
	else
	{
//		vTraceMsg(0x0A03, TR_LEVEL_USER_4);
	}

	tk_cls_dev(s32Trace, 0);*/

//	rDevSpmTraceIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE, OSAL_EN_READWRITE);
}


static tsDiagEOLBlock* psGetBlock(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8 u8TableId)
{
   tsDiagEOLBlock* psBlock = NULL;

   ETG_TRACE_USR4_THR(("---> psGetBlock"));

	if (VsShMemEOLLIB!=NULL)
	{
		tU32 i;
		for (i=0;i<EOL_BLOCK_NBR;i++)
		{
			if ((VsShMemEOLLIB->sDiagEOLBlocks[i].u8TableId) == u8TableId)
			{
				psBlock = &(VsShMemEOLLIB->sDiagEOLBlocks[i]);
				break;
			}
		}
	}

   ETG_TRACE_USR4_THR(("<--- psGetBlock"));

   return psBlock;
}


/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

tS32 DRV_DIAG_EOL_s32IODeviceInit(tVoid)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IODeviceInit"));

    tS32 s32Result = OSAL_E_NOERROR;

	DRV_DIAG_EOL_vTraceRegChannel();

    /* create semaphore */
#ifndef IOSC_ACTIV

    if(OSAL_s32SemaphoreCreate(EOLLIB_C_SEM_NAME, &vsEOLLibHandleSemaphore, 1) != OSAL_OK)
    { // If create failed due to OSAL_E_ALREADYEXISTS, open the semaphore
        s32Result = (tS32)OSAL_u32ErrorCode();
        if(s32Result != (tS32)OSAL_E_ALREADYEXISTS)
        {
            ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IODeviceInit: s32Result=%d on OSAL_s32SemaphoreCreate", s32Result));
            NORMAL_M_ASSERT_ALWAYS();
        }
        else
        {
            if(OSAL_s32SemaphoreOpen(EOLLIB_C_SEM_NAME, &vsEOLLibHandleSemaphore) != OSAL_OK)
            {
                s32Result = (tS32)OSAL_u32ErrorCode();
                ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IODeviceInit: s32Result=%d on OSAL_s32SemaphoreOpen", s32Result));
            }
            else
            {
                s32Result = OSAL_E_NOERROR;
            }
        }
    }
#endif
    /* create shared memory */
    if(s32Result == OSAL_E_NOERROR)
    {
        vsEOLLibHandleSharedMemory = OSAL_SharedMemoryCreate(EOLLIB_C_SHMEM_NAME, OSAL_EN_READWRITE, sizeof(tsEOLLIBSharedMemory));

        if(vsEOLLibHandleSharedMemory == OSAL_ERROR)
        { /*check if error not error "memory already exist"*/
            s32Result = OSAL_u32ErrorCode();
            if(s32Result != OSAL_E_ALREADYEXISTS)
            {/* assert */
                ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IODeviceInit: s32Result=%d on OSAL_SharedMemoryCreate", s32Result));
                NORMAL_M_ASSERT_ALWAYS();
            }
            else
            {/* memory create ALREADYEXISTS; get now valid handle*/
                vsEOLLibHandleSharedMemory = OSAL_SharedMemoryOpen(EOLLIB_C_SHMEM_NAME, OSAL_EN_READWRITE);
                if(vsEOLLibHandleSharedMemory == OSAL_ERROR)
                {
                    s32Result = OSAL_u32ErrorCode();
                    ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IODeviceInit: s32Result=%d on OSAL_SharedMemoryOpen", s32Result));
                }
                else
                {
                    s32Result = OSAL_E_NOERROR;
                }
            }
        }
    }

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IODeviceInit"));

    return(s32Result);
}

tS32 DRV_DIAG_EOL_s32IODeviceRemove(tVoid)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IODeviceRemove"));

   tS32 s32Result = OSAL_E_NOERROR;

   /*close and delete shared memory*/
   OSAL_s32SharedMemoryClose(vsEOLLibHandleSharedMemory);
   OSAL_s32SharedMemoryDelete(EOLLIB_C_SHMEM_NAME);

#ifndef IOSC_ACTIV
   /*close and delete semaphore*/
   OSAL_s32SemaphoreClose(vsEOLLibHandleSemaphore);
   OSAL_s32SemaphoreDelete(EOLLIB_C_SEM_NAME);
#endif
   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IODeviceRemove"));

   return(s32Result);
}

/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_IOOpen
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_IOOpen(tVoid)
 * ARGUMENTS   : 
 *               tVoid 
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_IOOpen( tVoid )
{
   tS32 s32Result = OSAL_E_NOERROR;

   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_IOOpen"));
   
#ifdef IOSC_ACTIV

   
   if((vsEOLLibHandleSemaphore = iosc_create_semaphore(OSAL_SHM_EOL,1)) < 0){
	   s32Result = (tS32)u32MapErrorCodeIOSC(vsEOLLibHandleSemaphore);
   }

#else
   if(OSAL_s32SemaphoreOpen(EOLLIB_C_SEM_NAME, &vsEOLLibHandleSemaphore) != OSAL_OK)
   {
       s32Result = OSAL_u32ErrorCode();
   }
#endif
   else
   {
	   vsEOLLibHandleSharedMemory =OSAL_SharedMemoryOpen(EOLLIB_C_SHMEM_NAME,OSAL_EN_READWRITE);
	   if(vsEOLLibHandleSharedMemory == OSAL_ERROR)
	   {
		   s32Result = OSAL_u32ErrorCode();
		   ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_IOOpen: s32Result=%d on OSAL_SharedMemoryOpen", s32Result));
	   }
	   else
	   {
		   tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

		   if (VsShMemEOLLIB != NULL)
		   {
			   if (VsShMemEOLLIB->u8ShMemInitiated != EOLLIB_INIT_MAGIC)
			   {
				   ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_IOOpen: VsShMemEOLLIB->u8ShMemInitiated=0x%x", 
								  VsShMemEOLLIB->u8ShMemInitiated));

				   s32Result = OSAL_E_CANCELED;
			   }
 
			   vEOLLIBUnLock(VsShMemEOLLIB);
		   }
	   }
   }

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_IOOpen"));

   return(s32Result);
} /* DRV_DIAG_EOL_IOOpen */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IOClose
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IOClose(tVoid)
 * ARGUMENTS   : 
 *               tVoid 
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IOClose( tVoid )
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IOClose"));

   OSAL_s32SharedMemoryClose(vsEOLLibHandleSharedMemory);
   vsEOLLibHandleSharedMemory=OSAL_C_INVALID_HANDLE;
#ifdef IOSC_ACTIV
   (void)iosc_destroy_semaphore(vsEOLLibHandleSemaphore);
#else
   OSAL_s32SemaphoreClose(vsEOLLibHandleSemaphore);
#endif 
   vsEOLLibHandleSemaphore = OSAL_C_INVALID_HANDLE;

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IOClose"));

   return(OSAL_E_NOERROR);
} /* DRV_DIAG_EOL_s32IOClose */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IOControl
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IOControl(tS32 s32fun,tS32 s32arg)
 * ARGUMENTS   : 
 *               tS32 s32fun
 *               tS32 s32arg
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IOControl(tS32 s32fun, tS32 s32arg)
{
	   tS32 s32Result = OSAL_E_CANCELED;

    ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IOControl"));

     tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

   switch (s32fun)
   {
     case OSAL_C_S32_IOCTRL_DIAGEOL_GET_MODULE_IDENTIFIER:
     {
         OSAL_trDiagEOLModuleIdentifier* pArg = (OSAL_trDiagEOLModuleIdentifier*) s32arg;

         if (NULL == pArg)
         {
			 s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
			 tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pArg->u8Table);

             if (psBlock == NULL)
             {
                 // requested Table is not supported 
                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
                

				 pArg->u32PartNumber = 
					 (psBlock->Header[6] << 24) | 
					 (psBlock->Header[7] << 16) |
					 (psBlock->Header[8] << 8) |
					 psBlock->Header[9];

				 pArg->u8DesignLevelSuffix[0] = psBlock->Header[10];
				 pArg->u8DesignLevelSuffix[1] = psBlock->Header[11];

		         s32Result = OSAL_E_NOERROR;
			 }
         }         

		 break;
	 }

     case OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC:
     {
         tU32 i;
         tU32 u32Crc = 0;
         OSAL_trDiagEOLEntry* pArg = (OSAL_trDiagEOLEntry*)(void*) s32arg;

         if (pArg->pu8EntryData == NULL)
         {
             s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
             tU8* pu8Data;
             tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pArg->u8Table);

             if (psBlock == NULL)
             {
                 //requested Table is not supported 
                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
                

                 //Build the checksum over the header
                  i = sizeof(psBlock->Header)-2;
                  pu8Data = &psBlock->Header[2];
                  while(i > 1)
	               {
		               u32Crc += 0xFFFF & ((pu8Data[1]<<8) | pu8Data[0]);
		               pu8Data+=2;
		               i -= 2;
	               }

                  //Add trailing byte
	               if (i)
	               {
		               u32Crc += (0xFF & pu8Data[0]);
	               }

                  //Build the checksum over the data block
                  i = psBlock->u16Length;
                  pu8Data = &psBlock->pu8Data[0];
                  while(i > 1)
	               {
		               u32Crc += 0xFFFF & ((pu8Data[1]<<8) | pu8Data[0]);
		               pu8Data+=2;
		               i -= 2;
	               }

                  //Add trailing byte
	               if (i)
	               {
		               u32Crc += (0xFF & pu8Data[0]);
	               }

#if 0
                  //1's complement
                  //Add carry bits
	               while (u32Crc>>16)
	               {
		               u32Crc = (u32Crc & 0xFFFF)+(u32Crc >> 16);
	               }
                  u32Crc ^= 0xFFFF;
#else
                  //2's complement
                  u32Crc &= 0xFFFF;
                  u32Crc ^= 0xFFFF;
                  u32Crc++;
#endif

                 pArg->pu8EntryData[0] = psBlock->Header[0];
                 pArg->pu8EntryData[1] = psBlock->Header[1];
                 pArg->pu8EntryData[2] = u32Crc & 0xFF;
                 pArg->pu8EntryData[3] = (u32Crc >> 8) & 0xFF;

                 s32Result = OSAL_E_NOERROR;

                  {
                     tPU8 pu8TraceBuffer = OSAL_pvMemoryAllocate(2 + 1+ 2 + 4);
                     tPU8 pu8Trace = pu8TraceBuffer;

                     *pu8Trace++ = 0x03;
                     *pu8Trace++ = 0x03;
                     *pu8Trace++ = pArg->u8Table;
                     *pu8Trace++ = psBlock->Header[1];
                     *pu8Trace++ = psBlock->Header[0];
                     *pu8Trace++ = (u32Crc >> 24) & 0xFF;
                     *pu8Trace++ = (u32Crc >> 16) & 0xFF;
                     *pu8Trace++ = (u32Crc >> 8) & 0xFF;
                     *pu8Trace++ = (u32Crc >> 0) & 0xFF;

                     LLD_vTrace(TR_CLASS_DEV_DIAGEOL, TR_LEVEL_USER_4, pu8TraceBuffer, pu8Trace - pu8TraceBuffer);

                     OSAL_vMemoryFree(pu8TraceBuffer);
                  }
             }
         }

        break;
     }

     default:
       break;
   }

   vEOLLIBUnLock(VsShMemEOLLIB);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IOControl"));

   return(s32Result);
} /* DRV_DIAG_EOL_s32IOControl */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IOWrite
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IOWrite(tPCS8 ps8Buffer,tU32 u32nbytes)
 * ARGUMENTS   : 
 *               tPCS8 ps8Buffer
 *               tU32 u32nbytes
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IOWrite(tPCS8 ps8Buffer, tU32 u32nbytes)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IOWrite"));

   tS32 s32Result = OSAL_ERROR;

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IOWrite"));

   return (s32Result);
} /* DRV_DIAG_EOL_s32IOWrite */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IORead
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IORead(tPCS8 ps8Buffer,tU32 u32nbytes)
 * ARGUMENTS   : 
 *               tPCS8 ps8Buffer
 *               tU32 u32nbytes
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IORead(tPCS8 ps8Buffer, tU32 u32nbytes)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IORead"));

	tS32 s32Result = OSAL_E_NOERROR;

   tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

   if ((ps8Buffer == NULL) || (u32nbytes != sizeof(OSAL_trDiagEOLEntry)))
   {
       s32Result = OSAL_E_INVALIDVALUE;
   }
   else 
   {
     
         OSAL_trDiagEOLEntry* pEntry = (OSAL_trDiagEOLEntry*)(void*) ps8Buffer;

         ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IORead: Table:%u Offset:%u Len:%u",
            ETG_CENUM(EOLLib_tenTable, pEntry->u8Table), 
            pEntry->u16Offset, 
            pEntry->u16EntryLength));

         if (pEntry->pu8EntryData == NULL)
         {
             s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
             tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pEntry->u8Table);

             if (psBlock == NULL)
             {
                 // requested Table is not supported 
                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
				 if (pEntry->u16EntryLength == 0) 
				 {
                     u32nbytes = sizeof(psBlock->Header);

					 memcpy(pEntry->pu8EntryData,
                                       psBlock->Header, 
									   u32nbytes);
				 }
				 else
				 {
                     if (pEntry->u16Offset >= psBlock->u16Length)
                     {
                         // requested Offset is out-of-range 
                         s32Result = OSAL_E_INVALIDVALUE;
                     }
                     else
                     {
                         u32nbytes = pEntry->u16EntryLength;
                         if (u32nbytes > (psBlock->u16Length - pEntry->u16Offset))
                         {
                             u32nbytes = psBlock->u16Length - pEntry->u16Offset;
                         }

                         if (psBlock->pu8Data != NULL)
                         {
                             memcpy(pEntry->pu8EntryData, 
                                               psBlock->pu8Data + pEntry->u16Offset, 
                                               u32nbytes);
                         }
                         else
                         {
                             s32Result = OSAL_E_DOESNOTEXIST;
                         }
                     }
                 }
             }
         }

         if (s32Result == OSAL_E_NOERROR)
         {
            ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IORead: Size:%d Data:%*x",
               u32nbytes, 
               ETG_LIST_LEN((u32nbytes > 200)? 200: u32nbytes), 
               ETG_LIST_PTR_T8(pEntry->pu8EntryData) ));
         }
         else
         {
            ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IORead: s32Result=%d", 
               s32Result));
         }
   }

   vEOLLIBUnLock(VsShMemEOLLIB);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IORead"));

   return (s32Result == OSAL_E_NOERROR)? u32nbytes: s32Result;
} /* DRV_DIAG_EOL_s32IORead */

/***********************************************************************
 * static       tsEOLLIBSharedMemory*        psEOLLIBLock(void)
 *    	
 * This function gives a pointer to the start of the shared memory and 
 * locks the memory with the internal mutex. 
 *
 * @return  
 *    tsEOLLIBSharedMemory*  pointer of shared memory
 *
 * @date    15.11.2010
 *
 * @note
 *
 **********************************************************************/
static tsEOLLIBSharedMemory*  psEOLLIBLock(void)
{
   tsEOLLIBSharedMemory* VtsShMemEOLLIB=NULL;

   ETG_TRACE_USR4_THR(("---> psEOLLIBLock"));
#ifdef IOSC_ACTIV
  tS32 s32Ret;
  tU32 u32ErrorCode=OSAL_OK;

  s32Ret = iosc_obtain_semaphore(vsEOLLibHandleSemaphore, 1, IOSC_TIMEOUT_FOREVER);
  if(s32Ret!= IOSC_OK){
      u32ErrorCode = u32MapErrorCodeIOSC(s32Ret);
        
#else

   if(OSAL_s32SemaphoreWait(vsEOLLibHandleSemaphore, OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
   {
#endif
       ETG_TRACE_ERR_THR(("!!! psEOLLIBLock: OSAL_s32SemaphoreWait FAILED"));
       NORMAL_M_ASSERT_ALWAYS();
   }
   else
   {
       VtsShMemEOLLIB = (tsEOLLIBSharedMemory*)
          OSAL_pvSharedMemoryMap(vsEOLLibHandleSharedMemory,OSAL_EN_READONLY,sizeof(tsEOLLIBSharedMemory),0);
       /*check error*/
       if(VtsShMemEOLLIB==NULL)
       {
          ETG_TRACE_ERR_THR(("!!! psEOLLIBLock: OSAL_pvSharedMemoryMap FAILED"));
          NORMAL_M_ASSERT_ALWAYS();
       }

   ETG_TRACE_USR4_THR(("<--- psEOLLIBLock"));
   }

   return(VtsShMemEOLLIB);
}
/***********************************************************************
 * static void vEOLLIBUnLock(tsEOLLIBSharedMemory* PtsShMemEOLLIB)
 *    	
 * This function releases a shared memory pointer and unlocks the internal mutex.
 *
 * @parameter
 *     PtsShMemEOLLIB  Pointer to shared memory 
 *   
 * @date   15.11.2010
 *
 * @note
 *
 **********************************************************************/
static void vEOLLIBUnLock(tsEOLLIBSharedMemory* PtsShMemEOLLIB)
{
  ETG_TRACE_USR4_THR(("---> vEOLLIBUnLock"));
  
  if(OSAL_s32SharedMemoryUnmap(PtsShMemEOLLIB,sizeof(tsEOLLIBSharedMemory))==OSAL_ERROR)
  {
    ETG_TRACE_ERR_THR(("!!! vEOLLIBUnLock: OSAL_s32SharedMemoryUnmap FAILED"));
    NORMAL_M_ASSERT_ALWAYS();
  }

#ifdef IOSC_ACTIV
  iosc_release_semaphore(vsEOLLibHandleSemaphore, 1);
#else
  OSAL_s32SemaphorePost(vsEOLLibHandleSemaphore);
#endif

  ETG_TRACE_USR4_THR(("<--- vEOLLIBUnLock"));
}



#ifdef __cplusplus
}
#endif

/* End of File diag_eol.c                                                */

