
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#include <EOLLib.h>

/*
MOD ID 05 - HMI NAV SYS CALN
*/

/*
*| hmi_nav_sys_caln.HEADER_NAVSYSTEM {
*|      : is_calconst;
*|      : description = "CALDS Header";
*| } 
*/ 
const EOLLib_CalDsHeader HEADER_NAVSYSTEM = {0x0000, EOLLIB_TABLE_ID_NAVIGATION_SYSTEM << 8, 0x0000, 0x00000000, {0x41, 0x41}, 0x0401};

const char NAV_SYSTEM_DUMMY_15 = 0x00;

const unsigned short NAV_SYSTEM_DUMMY_1[3] =
 {
  0x0000, 0x0000, 0x0000
 }
;

/*
*| hmi_nav_sys_caln._3D_BLDG_ICON_DISPLAY_ZOOM_LEVEL {
*|      : is_calconst;
*|      : description = "Zoom level that 3D building icons are displayed ";
*|      : units = "Km";
*|      : transform = fixed._3D_BLDG_ICON_DISPLAY_ZOOM_LEVEL;
*| } 
*/ 
const unsigned long _3D_BLDG_ICON_DISPLAY_ZOOM_LEVEL = 0x00001F6F;

/*
*| hmi_nav_sys_caln.NORTH_UP_MODE_CHANGE_ZOOM_LEVEL {
*|      : is_calconst;
*|      : description = " Zoom level where the map changes to North Up from heading up automatically";
*|      : units = "Km";
*|      : transform = fixed.NORTH_UP_MODE_CHANGE_ZOOM_LEVEL;
*| } 
*/ 
const unsigned long NORTH_UP_MODE_CHANGE_ZOOM_LEVEL = 0x00001F6F;

const char NAV_SYSTEM_DUMMY_2 = 0x00;

/*
*| hmi_nav_sys_caln.DESTINATION_DETAIL_UNIQUE_LOCATION_ZOOM_LEVEL {
*|      : is_calconst;
*|      : description = "Unique Location zoom level for Destination Details screen";
*|      : units = "meters";
*|      : transform = fixed.DESTINATION_DETAIL_UNIQUE_LOCATION_ZOOM_LEVEL;
*| } 
*/ 
const unsigned long DESTINATION_DETAIL_UNIQUE_LOCATION_ZOOM_LEVEL = 0x0003B880;

/*
*| hmi_nav_sys_caln.DESTINATION_DETAIL_STREET_CITY_LOCATION_ZOOM_LEVEL {
*|      : is_calconst;
*|      : description = "Unique Location zoom level for Destination Details screen";
*|      : units = "Km";
*|      : transform = fixed.DESTINATION_DETAIL_STREET_CITY_LOCATION_ZOOM_LEVEL;
*| } 
*/ 
const unsigned long DESTINATION_DETAIL_STREET_CITY_LOCATION_ZOOM_LEVEL = 0x00001F6F;

/*
*| hmi_nav_sys_caln.DESTINATION_DETAIL_CITY_LOCATION_ZOOM_LEVEL {
*|      : is_calconst;
*|      : description = "Unique Location zoom level for Destination Details screen";
*|      : units = "Km";
*|      : transform = fixed.DESTINATION_DETAIL_CITY_LOCATION_ZOOM_LEVEL;
*| } 
*/ 
const unsigned long DESTINATION_DETAIL_CITY_LOCATION_ZOOM_LEVEL = 0x00001F6F;

const char NAV_SYSTEM_DUMMY_16 = 0x00;

/*
*| hmi_nav_sys_caln.POI_LIST_INSTANCE_MAX {
*|      : is_calconst;
*|      : description = "Cal to define the maximum POI search results list per instance";
*|      : units = "results";
*|      : type = fixed.UB0;
*| } 
*/ 
const char POI_LIST_INSTANCE_MAX = 0x14;

/*
*| hmi_nav_sys_caln.POI_EXPANDED_LIST_MAX {
*|      : is_calconst;
*|      : description = "Cal to define the expanded POI search number ";
*|      : units = "results";
*|      : type = fixed.UB0;
*| } 
*/ 
const char POI_EXPANDED_LIST_MAX = 0x0A;

/*
*| hmi_nav_sys_caln.AUTOZOOM_LEVEL {
*|      : is_calconst;
*|      : description = "Cal to detail turn maneuver zoom when viewing turn list (auto zoom level)";
*|      : units = "Km";
*|      : transform = fixed.AUTOZOOM_LEVEL;
*| } 
*/ 
const unsigned long AUTOZOOM_LEVEL = 0x00000325;

const char NAV_SYSTEM_DUMMY_17 = 0x00;

const char NAV_SYSTEM_DUMMY_18 = 0x00;

const unsigned long NAV_SYSTEM_DUMMY_11 = 0x00000000;

const unsigned long NAV_SYSTEM_DUMMY_12 = 0x00000000;

/*
*| hmi_nav_sys_caln.LANDMARK_GUIDANCE_POI_QUALIFIER {
*|      : is_calconst;
*|      : description = "Cal to define the Landmark guidance POI qualifier";
*|      : units = "meters";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LANDMARK_GUIDANCE_POI_QUALIFIER = 0x64;

/*
*| hmi_nav_sys_caln.ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_GAS_STATION {
*|      : is_calconst;
*|      : description = "Cal to define the Landmark guidance POI categories - Separate out the categories";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_GAS_STATION = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_FAST_FOOD {
*|      : is_calconst;
*|      : description = "Cal to define the Landmark guidance POI categories - Separate out the categories";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_FAST_FOOD = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_HOTELS {
*|      : is_calconst;
*|      : description = "Cal to define the Landmark guidance POI categories - Separate out the categories";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_HOTELS = 0x01;

const char NAV_SYSTEM_DUMMY_28 = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_2 {
*|      : is_calconst;
*|      : description = "Cal to define the Landmark guidance POI categories - Separate out the categories";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_2 = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_3 {
*|      : is_calconst;
*|      : description = "Cal to define the Landmark guidance POI categories - Separate out the categories";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_3 = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_4 {
*|      : is_calconst;
*|      : description = "Cal to define the Landmark guidance POI categories - Separate out the categories";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_4 = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_5 {
*|      : is_calconst;
*|      : description = "Cal to define the Landmark guidance POI categories - Separate out the categories";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_LANDMARK_GUIDANCE_POI_CATEGORIE_UNDEFINED_5 = 0x00;

/*
*| hmi_nav_sys_caln.ROUTE_OVERVIEW_TIME {
*|      : is_calconst;
*|      : description = "Cal to define the time the route review is displayed before disappearing. Typical calibtation values should be 3, 5, or 10 seconds";
*|      : units = "ms";
*|      : transform = fixed.ROUTE_OVERVIEW_TIME;
*| } 
*/ 
const char ROUTE_OVERVIEW_TIME = 0x64;

/*
*| hmi_nav_sys_caln.DESTINATION_DISPLAY {
*|      : is_calconst;
*|      : description = "Cal to define the time the destination address is displayed before disappearing. ";
*|      : units = "ms";
*|      : transform = fixed.DESTINATION_DISPLAY;
*| } 
*/ 
const char DESTINATION_DISPLAY = 0x64;

/*
*| hmi_nav_sys_caln.KEYBOARD_AUTOCOMPLETE_HISTORY {
*|      : is_calconst;
*|      : description = "Keyboard autocomplete history delete. ";
*|      : units = "Days";
*|      : type = fixed.UB0;
*| } 
*/ 
const char KEYBOARD_AUTOCOMPLETE_HISTORY = 0x3C;

/*
*| hmi_nav_sys_caln.BUTTON_PRESS_VOICE_VOLUME_DELTA {
*|      : is_calconst;
*|      : description = "Cal to define how many steps does a button press adjusts the voice volume (steps)";
*|      : units = "Steps";
*|      : type = fixed.UB0;
*| } 
*/ 
const char BUTTON_PRESS_VOICE_VOLUME_DELTA = 0x01;

const char NAV_SYSTEM_DUMMY_3[18] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_nav_sys_caln.RT_WEATHER_STORMTRACKER_ENABLE {
*|      : is_calconst;
*|      : description = "Cal to support the Storm Tracker feature";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char RT_WEATHER_STORMTRACKER_ENABLE = 0x01;

/*
*| hmi_nav_sys_caln.RT_HORIZON_DISPLAY {
*|      : is_calconst;
*|      : description = "Cal for the data used to render the Horizon on the navigation screen (Enum)";
*|      : units = "Enumeration";
*|      : type = fixed.RT_HORIZON_DISPLAY;
*| } 
*/ 
const char RT_HORIZON_DISPLAY = 0x02;

const char NAV_SYSTEM_DUMMY_4[2] =
 {
  0x00, 0x00
 }
;

/*
*| hmi_nav_sys_caln.ENABLE_VEHICLE_ADDRESS_CALLOUT {
*|      : is_calconst;
*|      : description = "If the user taps on the vehicle icon, the current address\
of the vehicle is overlaid on the map next to the vehicle\
icon in a callout. Another tap hides the information.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_VEHICLE_ADDRESS_CALLOUT = 0x01;

/*
*| hmi_nav_sys_caln.ZOOM_INDICATOR_TIMEOUT {
*|      : is_calconst;
*|      : description = "The zoom indicator is shown for 3000 ms (calibrateable) to indicate the current zoom level.";
*|      : units = "ms";
*|      : transform = fixed.ZOOM_INDICATOR_TIMEOUT;
*| } 
*/ 
const char ZOOM_INDICATOR_TIMEOUT = 0x1E;

/*
*| hmi_nav_sys_caln.ENABLE_GOOGLE_POI_SEARCH_RESULTS {
*|      : is_calconst;
*|      : description = "When an Internet connection is available, the keyboard\
search augments the internal database search with\
Google Maps results. These results are displayed on the\
map the same as other results, but are categorized in\
their own Google category.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_GOOGLE_POI_SEARCH_RESULTS = 0x01;

/*
*| hmi_nav_sys_caln.MAX_SINGLE_DESTINATION {
*|      : is_calconst;
*|      : description = "A single destination location is limited to being included\
in the list a maximum number of 10 times. When the\
11th instance of that destination would be entered, the\
oldest instance is deleted to maintain 10. This is used\
to help mitigate frequently navigated locations such as\
Home or Office from deleting less frequently visited addresses.";
*|      : units = "MAX Number of Single Destinations";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MAX_SINGLE_DESTINATION = 0x0A;

/*
*| hmi_nav_sys_caln.ENABLE_SCENIC_ROUTE {
*|      : is_calconst;
*|      : description = "If a scenic route is available and is close to the route that\
the user is traveling, a pop-up is shown to the user and\
allows them to choose whether or not they would like\
to follow the scenic route or keep the current route. If\
the user performs no action, the current route is kept\
and the popup is dismissed.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_SCENIC_ROUTE = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_VOICE_PREFERENCES_PROMPT_STYLE_LANDMARK_GUIDANCE {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_VOICE_PREFERENCES_PROMPT_STYLE_LANDMARK_GUIDANCE = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_FASTEST {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_FASTEST = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_SHORTEST {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_SHORTEST = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_GREENEST {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_GREENEST = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_FERRIES {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_FERRIES = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_TOLL_ROADS {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_TOLL_ROADS = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_HIGHWAY {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_HIGHWAY = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_CARPOOL_LANES {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_CARPOOL_LANES = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_AVOID_TRAFFIC {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_AVOID_TRAFFIC = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_AVOID_TUNNELS {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_AVOID_TUNNELS = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_SEASONAL {
*|      : is_calconst;
*|      : description = "If the user taps the More... button from the Map, they\
will be shown a list of additional navigation functions";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MAP_MORE_FEATURE_ROUTING_PREFERNCE_SEASONAL = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_RELATIVE_DIRECTION_TO_DESTINATION {
*|      : is_calconst;
*|      : description = "To assist the user with situational awareness and selfnavigation,\
the system indicates the direction to the destination\
with an arrow on the vehicle location icon.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RELATIVE_DIRECTION_TO_DESTINATION = 0x01;

const char NAV_SYSTEM_DUMMY_19 = 0x00;

const char NAV_SYSTEM_DUMMY_20 = 0x00;

/*
*| hmi_nav_sys_caln.TURN_LIST_DISTANCE_TO_MANEUVER_OPTION {
*|      : is_calconst;
*|      : description = "Each maneuver indicates the distance between it and\
the previous maneuver or the vehicle�s current location.\
The next maneuver at the top will count down until the\
maneuver is reached, and then the next maneuver will\
begin to count down.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char TURN_LIST_DISTANCE_TO_MANEUVER_OPTION = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_ETA_TO_MANEUVER {
*|      : is_calconst;
*|      : description = "Each maneuver has an estimated time of arrival based\
on the current driving conditions. This time is updated\
in real-time to reflect the current driving conditions.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_ETA_TO_MANEUVER = 0x01;

const char NAV_SYSTEM_DUMMY_5 = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_AGP_ARRIVING_AT_DESTINATION_SPECIALIZED_PROMPT {
*|      : is_calconst;
*|      : description = "The arriving at destination message repeats the house\
number and street name (and POI name if a POI) when\
arriving at the destination. If the location is a POI, a\
specialized message is added when the vehicle is parked\
and turned off. For example, �Enjoy your dinner� for a\
restaurant or �Have a good evening� for a hotel";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AGP_ARRIVING_AT_DESTINATION_SPECIALIZED_PROMPT = 0x01;

const char NAV_SYSTEM_DUMMY_21 = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_GUIDANCE_WHILE_ON_PHONE_CALL {
*|      : is_calconst;
*|      : description = "If the user is on a phone call while under route guidance,\
the auditory prompts that are played are shortened\
as much as possible so as to not interfere with\
the conversation that is going on. An example may be\
instead of saying �In 1/2 Mile, Turn Right Onto Jefferson\
St�, the system may say �Turn Right Ahead.�";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_GUIDANCE_WHILE_ON_PHONE_CALL = 0x01;

/*
*| hmi_nav_sys_caln.WARNING_SCREEN_FREQUENCY {
*|      : is_calconst;
*|      : description = "Nav Warning Screen \
How many ignition cycles before the display is shown again.  Differs by country.";
*|      : units = "Warrning Screen";
*|      : type = fixed.UB0;
*| } 
*/ 
const char WARNING_SCREEN_FREQUENCY = 0x32;

/*
*| hmi_nav_sys_caln.SWMI_CALIBRATION_DATA_FILE_HMI_NAV_SYSTEM_CAL {
*|      : is_calconst;
*|      : description = "The NoCalibration state is defined in order to make sure that Infotainment subsystem components have been updated with calibrations after a service event such as replacing one or more modules or upgrading software.  The mechanism for NoCalibration determination is the same as that for Theftlock and NoVIN determination.  That is, at initialization a module detects whether or not it has a valid calibration.  If it does not, it notifies the SystemState FBlock via the SystemState.SetNoCalibrationModuleState method.  Once a valid calibration is received, the module calls the SystemState.SetNoCalibrationModuleState method to clear the condition.\
The SystemState FBlock shall assume all module calibrations are valid upon each Sleep cycle.  Modules that already have a valid calibration or do not support NoCalibration detection (determined by each module CTS) do not need to report the NoCalibration clear condition at each initialization.\
Note: each of these Calibrations should be ...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SWMI_CALIBRATION_DATA_FILE_HMI_NAV_SYSTEM_CAL = 0x00;

const unsigned short NAV_SYSTEM_DUMMY_6 = 0x0000;

/*
*| hmi_nav_sys_caln.TRAFFIC_REROUTE_ALERT_DISTANCE_GUIDANCE {
*|      : is_calconst;
*|      : description = "The informatin alert will be shown when the user is not under route as well as if they are under route guidance but the system didn't find a route that would save them more than 10 minutes, and would be shown when the incident is 3 miles ahead of them on there route. POP UP";
*|      : units = "Km";
*|      : transform = fixed.TRAFFIC_REROUTE_ALERT_DISTANCE_GUIDANCE;
*| } 
*/ 
const unsigned long TRAFFIC_REROUTE_ALERT_DISTANCE_GUIDANCE = 0x00003EDE;

/*
*| hmi_nav_sys_caln.TRAFFIC_REROUTE_ALERT_TIME_GUIDANCE {
*|      : is_calconst;
*|      : description = "The informatin alert will be shown when the user is not under route as well as if they are under route guidance but the system didn't find a route that would save them more than 10 minutes, and would be shown when the incident is 3 miles ahead of them on there route. POP UP";
*|      : units = "Minutes";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TRAFFIC_REROUTE_ALERT_TIME_GUIDANCE = 0x05;

const unsigned short NAV_SYSTEM_DUMMY_22 = 0x0000;

const unsigned short NAV_SYSTEM_DUMMY_23 = 0x0000;

const unsigned long NAV_SYSTEM_DUMMY_13 = 0x00000000;

const unsigned long NAV_SYSTEM_DUMMY_14 = 0x00000000;

/*
*| hmi_nav_sys_caln.POI_HOURS_OF_OPERATION {
*|      : is_calconst;
*|      : description = "When a POI is selected and the Destination Details view is shown, the system performs a check to determine if the user may arrive at the location after it has closed.  This may be found out immediately if the current time is already past the closing time, or if the estimated arrival time is either 2 minutes (calibratable) before the closing time or after. ";
*|      : units = "Minutes";
*|      : type = fixed.UB0;
*| } 
*/ 
const char POI_HOURS_OF_OPERATION = 0x02;

/*
*| hmi_nav_sys_caln.NO_GPS_ICON_TIMER {
*|      : is_calconst;
*|      : description = "the system suppresses the No GPS icon for 10 seconds (calibrateable) to allow for short interruptions in signal before presenting it to the user";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char NO_GPS_ICON_TIMER = 0x0A;

const unsigned long NAV_SYSTEM_DUMMY_7 = 0x00000000;

/*
*| hmi_nav_sys_caln.INITIAL_HORIZON_TIME {
*|      : is_calconst;
*|      : description = "GIS-328-007 Appendix C � Calibrations&Configurations";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char INITIAL_HORIZON_TIME = 0x1E;

/*
*| hmi_nav_sys_caln.RETRANSMIT_HORIZON_TIME {
*|      : is_calconst;
*|      : description = "GIS-328-007 Appendix C � Calibrations&Configurations";
*|      : units = "ms";
*|      : transform = fixed.RETRANSMIT_HORIZON_TIME;
*| } 
*/ 
const char RETRANSMIT_HORIZON_TIME = 0x0A;

/*
*| hmi_nav_sys_caln.MINIMUM_AH_DRIVETIME {
*|      : is_calconst;
*|      : description = "GIS-328-007 Appendix C � Calibrations&Configurations";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MINIMUM_AH_DRIVETIME = 0x1E;

/*
*| hmi_nav_sys_caln.MINIMUM_MLP_BUILDUP_TIME {
*|      : is_calconst;
*|      : description = "GIS-328-007 Appendix C � Calibrations&Configurations";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MINIMUM_MLP_BUILDUP_TIME = 0x1E;

/*
*| hmi_nav_sys_caln.AH_ITERATIONS_STEPS {
*|      : is_calconst;
*|      : description = "GIS-328-007 Appendix C � Calibrations&Configurations";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char AH_ITERATIONS_STEPS = 0x1E;

/*
*| hmi_nav_sys_caln.AH_ITERATIONS_LENGHT {
*|      : is_calconst;
*|      : description = "GIS-328-007 Appendix C � Calibrations&Configurations";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char AH_ITERATIONS_LENGHT = 0x1E;

/*
*| hmi_nav_sys_caln.ADASIS_SUPPORT {
*|      : is_calconst;
*|      : description = "3.3 Possible Configurations of the ADASIS v2 Horizon Provider\
In general, ADASIS v2 Horizon Providers should initially consider and calculate all possible paths.\
However, the client ADAS application may need just a subset of those paths";
*|      : units = "Horizon Support";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ADASIS_SUPPORT = 0x00;

/*
*| hmi_nav_sys_caln.MAX_OFFSET_RANGE {
*|      : is_calconst;
*|      : description = "3.4.3 Path Length Limits\
The ADASIS concept uses different range values to define the length of the ADAS horizon. These are defined in this cha";
*|      : units = "Meters";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short MAX_OFFSET_RANGE = 0x0BB8;

/*
*| hmi_nav_sys_caln.MAX_LENGTH_OF_TRANSMITTED_PATH {
*|      : is_calconst;
*|      : description = "3.4.3 Path Length Limits\
The ADASIS concept uses different range values to define the length of the ADAS horizon. These are defined in this cha";
*|      : units = "Meters";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short MAX_LENGTH_OF_TRANSMITTED_PATH = 0x0096;

/*
*| hmi_nav_sys_caln.OFFSET_RANGE_LIMITATION_METHOD {
*|      : is_calconst;
*|      : description = "3.4.4 Unlimited Horizon Length\
There are two options how Av2HP can deal with offset range limitation.";
*|      : units = "boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char OFFSET_RANGE_LIMITATION_METHOD = 0x00;

/*
*| hmi_nav_sys_caln.PROFILE_INTERPOLATION_TYPE {
*|      : is_calconst;
*|      : description = "As there are a variety of ways to describe a profile as a function of offset along the path, the Profile Interpolation Type specifies how the profile\
value is to be calculated from the Profile Spots at intermediate locations using some interpolation scheme (e.g., discrete, step-profile, linear or\
higher order interpolation �).";
*|      : units = "Enum";
*|      : type = fixed.PROFILE_INTERPOLATION_TYPE;
*| } 
*/ 
const char PROFILE_INTERPOLATION_TYPE = 0x02;

/*
*| hmi_nav_sys_caln.DS_TRAILING_LENGTH {
*|      : is_calconst;
*|      : description = "5.4 Implicit entity removal on client\
This section describes how the AV2HR handles deletion of profiles and stubs behind the current vehicle position. The guidelines also show how\
the AV2HP must handle special use cases to be consistent with the reconstructor content to avoid \"memory leaks\".As the vehicle moves along the path, the AV2HP sends further profiles and one sub-path. The AV2HR should have a defined trailing length\
value (called, for instance, DS_TRAILING_LENGTH), which defines the distance back from the vehicle before entities can be deleted from the\
data store of AV2HR. This value must be consistent with the corresponding setting of the AV2HP.";
*|      : units = "Meters";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short DS_TRAILING_LENGTH = 0x0064;

/*
*| hmi_nav_sys_caln.DS_TRAILING_PERCENTAGE {
*|      : is_calconst;
*|      : description = "Within the Av2HP, more than one Map-Matched position might exist, especially in the case of a dense road network, where after passing an\
intersection it may not be clear which path the vehicle has taken. Especially if two alternatives have similar turn angles, such as at bifurcations,\
and run in parallel for a certain distance.\
Since no explicit deletion message is defined for segments and stubs, the Av2HR needs to decide when to delete segments. This could be\
done for all data that is more than, for instance, 100m behind the vehicle and for the alternative paths that have origins on these deleted\
segments and therefore cannot be reached anymore. Such an approach has the disadvantage that segments associated with probable\
alternative positions, e.g. at bifurcations, can be deleted in the Av2HR. If the Av2HP position jumps to one of these alternative paths, it does not\
know if this new path is still known by the Av2HR or has already been deleted. It therefore needs t...";
*|      : units = "Percent";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DS_TRAILING_PERCENTAGE = 0x63;

/*
*| hmi_nav_sys_caln.MIN_PATH_INDICES_REUSE_DISTANCE {
*|      : is_calconst;
*|      : description = "Av2HP Recommendation #3 The horizon provider should wait a certain distance until he re-uses path indices for new paths. If a path\
index e.g. 15 has already been used (not for the main path), the vehicle position should have passed the root of path 15 attached to the\
main path by at least the configured maximum back jump distance until this path index 15 can be re-used to attach a completely new\
path at the far end of the horizon. Also, the current vehicle position should be with a high probability not on a root with sub path index 15\
until path index 15 can be re-used. This increases the robustness and prevents possible index mix-ups in the case of position jumps.\
For back jumps see also section 5.5, for position jumps see chapter 6.4.";
*|      : units = "Meters";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short MIN_PATH_INDICES_REUSE_DISTANCE = 0x00C8;

const unsigned short NAV_SYSTEM_DUMMY_8[4] =
 {
  0x0000, 0x0000, 0x0000, 0x0000
 }
;

const char NAV_SYSTEM_DUMMY_9 = 0x00;

const char NAV_SYSTEM_DUMMY_24 = 0x00;

/*
*| hmi_nav_sys_caln.MANEUVER_CRITICALITY_DISTANCE_MEDIUM {
*|      : is_calconst;
*|      : description = "When the user has the navigation routing preference set\
for shortest distance, the criticality is then determined\
based on added distance to the original route rather\
than time. The criticalities are defined as follows:\
Normal: >5 miles (calibrateable)\
Medium: 5-10 miles (calibrateable)\
Critical: >10 miles (calibrateable)";
*|      : units = "Km";
*|      : transform = fixed.MANEUVER_CRITICALITY_DISTANCE_MEDIUM;
*| } 
*/ 
const unsigned long MANEUVER_CRITICALITY_DISTANCE_MEDIUM = 0x00000000;

const char NAV_SYSTEM_DUMMY_25[39] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_nav_sys_caln.ENABLE_HMI_FOR_NAV_WITHOUT_MAP_DB {
*|      : is_calconst;
*|      : description = "Boolean calibration to adjust for HMI differences needed to allows Navigation hardware/software to be shipped to a region that currently has no map database available but regioanal rollout is planned and upgrade is possible.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_HMI_FOR_NAV_WITHOUT_MAP_DB = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_APPLICATION_WEATHER {
*|      : is_calconst;
*|      : description = "HomeScreen Applications - XM Weather is a USA Navigation module only Feature and requires the Map database and service from XM is only offered in USA.\
Disable for all other regions/system.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_WEATHER = 0x00;

const char NAV_SYSTEM_DUMMY_26 = 0x00;

/*
*| hmi_nav_sys_caln.DEFAULT_WEATHER_MENUE_WEATHER_ALERT_SETTING {
*|      : is_calconst;
*|      : description = "The user can toggle Weather Alerts on or off by tapping this list item. The Defaut is ON\
- XM Weather is a USA Navigation module only Feature and requires the Map database and service from XM is only offered in USA.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DEFAULT_WEATHER_MENUE_WEATHER_ALERT_SETTING = 0x00;

/*
*| hmi_nav_sys_caln.INITIAL_DEFAULT_FORECAST_INFORMATION {
*|      : is_calconst;
*|      : description = "When the user enters the map application, the last view for the forecast will be shown, but the initial default is for hourly. If the user selects another type of forecast, that information is updated in the forecast region.\
- XM Weather is a USA Navigation module only Feature and requires the Map database and service from XM is only offered in USA.";
*|      : units = "Enumeration";
*|      : type = fixed.INITIAL_DEFAULT_FORECAST_INFORMATION;
*| } 
*/ 
const char INITIAL_DEFAULT_FORECAST_INFORMATION = 0x00;

const char NAV_SYSTEM_DUMMY_27 = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_SPEECHRECOGNITIONDOMAIN_XM_AUDIO {
*|      : is_calconst;
*|      : description = "When enabled, the XM audio grammar and talkbacks are available using speech recognition.\
Identify the bit to disable XM audio from the Speech grammar.  Originally was deleted per Bosch request, however, it was later determined as required and needs to be instated as the only method of determining when to load XM audio into the grammar context (for USA/CA) and when to remove XM audio from all Speech Recognition and talkbacks for non XM audio regions like Mexico. \
\
This is required to remove XM audio speech recognition items for the Mexican market when set to $0 Disable.\
NOTE:  Inclusion of XM Weather grammar uses an existing and different calibration, ENABLE_APPLICATION_WEATHER in the HMI Form and Behavior calibration per supplier direction.\
\
\
The user is able to command the system by using\
speech commands. The following domains are available\
for control using speech.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_SPEECHRECOGNITIONDOMAIN_XM_AUDIO = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_SPEECHRECOGNITIONDOMAIN_DAB {
*|      : is_calconst;
*|      : description = "The user is able to command the system by using speech commands. The following domains are available for control using speech.\
\
When enabled, the DAB  grammar and talkbacks are available using speech recognition.  Feature is European only and is tied to optional hardware content from the tuner module.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_SPEECHRECOGNITIONDOMAIN_DAB = 0x00;

/*
*| hmi_nav_sys_caln.RT_WEATHER_ENABLE {
*|      : is_calconst;
*|      : description = "Real Time Weather availability in North America bundle (US/PR/Canada/Mexico)\
UPDATE:  Calibration RT_WEATHER_ENABLE_NA also enables the speech recognition for the Weather domain.  As a secondary function, Weather SR is either added or removed from the grammar based on this calibration.  If disabled and user speaks weather SR commands, the system will say Pardon or Command not recognized.\
For MY2013, the calibration is only enabled for USA Navigation systems.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char RT_WEATHER_ENABLE = 0x01;

/*
*| hmi_nav_sys_caln.RT_TRAFFIC_ENABLE {
*|      : is_calconst;
*|      : description = "Real Time Traffic availability.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char RT_TRAFFIC_ENABLE = 0x01;

/*
*| hmi_nav_sys_caln.HIST_TRAFFIC_ENABLE {
*|      : is_calconst;
*|      : description = "Historic Traffic availability.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char HIST_TRAFFIC_ENABLE = 0x00;

/*
*| hmi_nav_sys_caln.RT_GAS_PRICES_ENABLE {
*|      : is_calconst;
*|      : description = "Real Time Gas Prices availability.\
Typically in North America bundle (US/PR/Canada/Mexico)";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char RT_GAS_PRICES_ENABLE = 0x01;

/*
*| hmi_nav_sys_caln.RT_MOVIE_THEATER_INFO_ENABLE {
*|      : is_calconst;
*|      : description = "Real Time Movie Theater information availability.\
Typically, in North America bundle (US/PR/Canada/Mexico)";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char RT_MOVIE_THEATER_INFO_ENABLE = 0x01;

/*
*| hmi_nav_sys_caln.ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_TRAFFIC_LIGHTS {
*|      : is_calconst;
*|      : description = "Cal to define the Landmark guidance POI categories - Separate out the categories";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_LANDMARK_GUIDANCE_POI_CATEGORY_TRAFFIC_LIGHTS = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_NAV_FEATURE_POI_ALERT {
*|      : is_calconst;
*|      : description = "Disables POI Alerts Feature and corresponding menu entries";
*|      : units = "enum";
*|      : type = fixed.ENABLE_NAV_FEATURE_POI_ALERT;
*| } 
*/ 
const char ENABLE_NAV_FEATURE_POI_ALERT = 0x00;

/*
*| hmi_nav_sys_caln.ENABLE_NAV_ME_OFFROAD_PACKAGE {
*|      : is_calconst;
*|      : description = "Enables Middle East Offroad Package";
*|      : units = "enum";
*|      : type = fixed.ENABLE_NAV_ME_OFFROAD_PACKAGE;
*| } 
*/ 
const char ENABLE_NAV_ME_OFFROAD_PACKAGE = 0x01;

/*
*| hmi_nav_sys_caln.CAL_HMI_NAV_SYSTEMS_END {
*|      : is_calconst;
*|      : description = "END OF CAL BLOCK";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CAL_HMI_NAV_SYSTEMS_END = 0x00;
