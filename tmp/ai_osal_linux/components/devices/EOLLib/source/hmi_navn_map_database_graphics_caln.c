
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#include <EOLLib.h>

/*
MOD ID 06 - HMI NAVN MAP DATABASE GRAPHICS CALN
*/

/*
*| hmi_navn_map_database_graphics_caln.HEADER_NAVICON {
*|      : is_calconst;
*|      : description = "CALDS Header";
*| } 
*/ 
const EOLLib_CalDsHeader HEADER_NAVICON = {0x0000, EOLLIB_TABLE_ID_NAVIGATION_ICON << 8, 0x0000, 0x00000000, {0x41, 0x41}, 0x0401};

/*
*| hmi_navn_map_database_graphics_caln.SWMI_CALIBRATION_DATA_FILE_HMI_NAV_ICON_CAL {
*|      : is_calconst;
*|      : description = "The NoCalibration state is defined in order to make sure that Infotainment subsystem components have been updated with calibrations after a service event such as replacing one or more modules or upgrading software.  The mechanism for NoCalibration determination is the same as that for Theftlock and NoVIN determination.  That is, at initialization a module detects whether or not it has a valid calibration.  If it does not, it notifies the SystemState FBlock via the SystemState.SetNoCalibrationModuleState method.  Once a valid calibration is received, the module calls the SystemState.SetNoCalibrationModuleState method to clear the condition.\
The SystemState FBlock shall assume all module calibrations are valid upon each Sleep cycle.  Modules that already have a valid calibration or do not support NoCalibration detection (determined by each module CTS) do not need to report the NoCalibration clear condition at each initialization.\
Note: each of these Calibrations should be ...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SWMI_CALIBRATION_DATA_FILE_HMI_NAV_ICON_CAL = 0x00;

const char NAVICON_DUMMY_1 = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.ECO_ROUTE_CURVE {
*|      : is_calconst;
*|      : description = "Cal to define the Engine for Eco routing\
\
Calibration represents the curves that define ECO routing performance and criteria.";
*|      : units = "Enum";
*|      : type = fixed.ECO_ROUTE_CURVE;
*| } 
*/ 
const char ECO_ROUTE_CURVE = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.ENABLE_APPLICATION_HYBRID {
*|      : is_calconst;
*|      : description = "HomeScreen Applications - This calibration turns the HYBRID icon on or off on the home screen";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_HYBRID = 0x01;

/*
*| hmi_navn_map_database_graphics_caln.ENABLE_APPLICATIONTRAY_HYBRID {
*|      : is_calconst;
*|      : description = "The four default applications in the tray are:\
Audio, Phone, Navigation, Climate Control\
If the navigation system is not available on the vehicle, the nav application is still shown but has no action when tapped. It provides direction information only.\
UPDATE:\
Item must be in all CALDS bundles and common to all HMI software sets.  \
*  However, the calibration only functions with 8\" systems and shall have the default described in this document.  \
*  The calibration will perform no action in the 4.2\" software sets and shall have default of 0 = DISABLE.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_HYBRID = 0x01;

/*
*| hmi_navn_map_database_graphics_caln.HYBRID_SYSTEM_HMI {
*|      : is_calconst;
*|      : description = "Calibration determined the type of HMI to suppoer for a specific Hybrid architacture/system.  The details of features and functions are documented in GIS-400 specifications and relate to the enumerations of this calibration.\
9.16.1.3~MY14 Unique Configurations\
Based on Hybrid / extended Range Vehicle\
\
";
*|      : units = "Enum";
*|      : type = fixed.HYBRID_SYSTEM_HMI;
*| } 
*/ 
const char HYBRID_SYSTEM_HMI = 0x01;

/*
*| hmi_navn_map_database_graphics_caln.DEFAULT_POI_FUEL_STATION {
*|      : is_calconst;
*|      : description = "Cal to define the default Fuel Station POI Types and for Eco Routing";
*|      : units = "Enum";
*|      : type = fixed.DEFAULT_POI_FUEL_STATION;
*| } 
*/ 
const char DEFAULT_POI_FUEL_STATION = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_WELCOME_ENABLE {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes\
\
All vehicles will have a Welcome Animation\
Only Hybrid vehicles would have Startup/Run and Shutdown/Goodbye AudioCues enabled.  ";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_WELCOME_ENABLE = 0x01;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_SHUTDOWN_ENABLE {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes\
\
All vehicles will have a Welcome Animation\
Only Hybrid vehicles would have Startup/Run and Shutdown/Goodbye AudioCues enabled.  ";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_SHUTDOWN_ENABLE = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_STARTUP_ENABLE {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes\
\
All vehicles will have a Welcome Animation\
Only Hybrid vehicles would have Startup/Run and Shutdown/Goodbye AudioCues enabled.  ";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_STARTUP_ENABLE = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_ACUE_RUN_CUE {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_ACUE_RUN_CUE = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_ACUE_STOP_CUE {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_ACUE_STOP_CUE = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_RESERVED_3 {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_RESERVED_3 = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_RESERVED_4 {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_RESERVED_4 = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_RESERVED_5 {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_RESERVED_5 = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_RESERVED_6 {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_RESERVED_6 = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_RESERVED_7 {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_RESERVED_7 = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.AUDIO_CUE_RESERVED_8 {
*|      : is_calconst;
*|      : description = "CueCalibrations [CueType] These are the boolean calibration flags for each of the cues defined within the AudioCues Fblock. There are currently 11 Cues defined. The CueType takes values from 0x01 - 0x0B.\
\
The diagram below defines the processing within the AudioCue FBlock for generating AudioCues relative to vehicle and system state changes";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char AUDIO_CUE_RESERVED_8 = 0x00;

/*
*| hmi_navn_map_database_graphics_caln.SCHEDULE_UPDATE_TIMEOUT_TIMER {
*|      : is_calconst;
*|      : description = "When the user changes the departure time (when in delayed based on departure time mode) or changes the summer or winter start dates, or rate schedules (when in delayed based on rate and departure time mode),  if the VICM does not respond within SCHEDULE_UPDATE_TIMEOUT_TIMER (3 seconds) to the data sent by the HMI Module,  the HMI Module shall display the Schedule Update Timeout Pop-Up (\"The system was unable to save your updates, please try again.\")";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SCHEDULE_UPDATE_TIMEOUT_TIMER = 0x06;

/*
*| hmi_navn_map_database_graphics_caln.INVALID_DATA_POPUP_TIMER {
*|      : is_calconst;
*|      : description = "4. The VICM is responsible for remembering the last charge mode over transitions to Off Asleep and should communicate this information to the silverbox at initialization. If a user selects the Charging tab or the silverbox powers up in charging mode and the charge mode is not received from the VICM, the silverbox shall display a pop-up with the text �Updating, please wait�� for a minmum of two seconds. If a valid charge mode is not received within 6s via the High Voltage Battery Time Based Charging Status message, the silverbox shall transition to a pop-up with the text �No data available from the high voltage charging system�.\
\
On release of the �+� or �-� button, if the VICM does not respond within 3s to the data sent by the silverbox, the silverbox shall display the error pop-up shown below until the calibratable timer Invalid_Data_PopUP_Timer expires and re-display the original time.";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char INVALID_DATA_POPUP_TIMER = 0x06;

/*
*| hmi_navn_map_database_graphics_caln.SETTINGS_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "Menu Configuration Settings";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SETTINGS_MASK_BYTE_1 = 0x02;

/*
*| hmi_navn_map_database_graphics_caln.ENERGYSUMMARY_POPUPREQUEST_WAITTIMER {
*|      : is_calconst;
*|      : description = "Defines how long the HMI Module shall wait for other Alert Activation from the alert manager on key-off before displaying the  Energy Summary Exit Pop-Up Alert.  Any device sending a request for a pop-up at key-off must send the request within 0.75 sec of key-off.  The HMI Module shall wait a calibratable amount of time for this request before displaying the Energy Screen pop-up in order to avoid display screen race conditions.\
";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ENERGYSUMMARY_POPUPREQUEST_WAITTIMER = 0x01;

/*
*| hmi_navn_map_database_graphics_caln.ENERGYSUMMARY_POPUP_DISPLAYTIMER {
*|      : is_calconst;
*|      : description = "Defines how long the HMI Module shall set its AutoDismiss timer to display the Energy Summary Exit Full screen Pop-Up Alert.";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ENERGYSUMMARY_POPUP_DISPLAYTIMER = 0x0A;

/*
*| hmi_navn_map_database_graphics_caln.ENERGYSUMMARY_MINIMUMPOPUP_DISPLAYTIMER {
*|      : is_calconst;
*|      : description = "1. In the event that another request for the display is received immediately after the HMI Module launches the Energy Summary Exit Pop-Up, The Energy Summary Exit Pop-Up shall be displayed for a minimum of 2s to avoid being interpreted as a screen flicker.  (i.e.  display the Energy Summary Exit Pop-Up for 2s min, (EnergySummary_MinimumPopUp_DisplayTimer) then transition to the requested pop-up.)";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ENERGYSUMMARY_MINIMUMPOPUP_DISPLAYTIMER = 0x02;

/*
*| hmi_navn_map_database_graphics_caln.MAXALLOWED_FUELECONOMY_DISPLAY {
*|      : is_calconst;
*|      : description = "Defines calibration used in the formula to determine if the calculated fuel economy or the max value defined in the calibration �MaxAllowed_FuelEconomy_Display� should be displayed in the HMI Modules Fuel Economy HMI.\
\
Maximum Fuel Economy Data Display\
The HMI Module shall limit the maximum Fuel Economy data value displayed as follows:\
1. If the calculated Fuel Economy data = calibration �MaxAllowed_FuelEconomy_Display�, display the calculated data value. (example; 145 mpg, 190 L/100km, etc�.if calibration �MaxAllowed_FuelEconomy_Display� is set to 250)\
2. If the calculated Fuel Economy data > calibration �MaxAllowed_FuelEconomy_Display�, display the calibration value followed by a plus sign.  (example; 250+ mpg, 250+ L/100km, etc� if calibration �MaxAllowed_FuelEconomy_Display� is set to 250.)\
\
";
*|      : units = "mpg";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short MAXALLOWED_FUELECONOMY_DISPLAY = 0x00FA;

/*
*| hmi_navn_map_database_graphics_caln.DEPARTURETIME_SLEWRATE {
*|      : is_calconst;
*|      : description = "The HMI Module shall use the calibration DepartureTime_SlewRate to control the update frequency of the hour and minute values when the �+� or �-� buttons are held.\
If the �+� hour button is held for more than 750ms, the silverbox shall increment the hour by one every 250ms.\
 If the �-� hour button is held for more than 750ms, the silverbox shall decrement the hour by one every 250ms.\
 If the �+� minute button is held for more than 750ms, the silverbox shall increment the minutes in 15 mins increments every 250ms.\
If the �-� minute button is held for more than 750ms, the silverbox shall decrement the minutes in 15 mins increments every 250ms.";
*|      : units = "ms";
*|      : transform = fixed.DEPARTURETIME_SLEWRATE;
*| } 
*/ 
const char DEPARTURETIME_SLEWRATE = 0x19;

/*
*| hmi_navn_map_database_graphics_caln.DEPARTURETIME_BUTTONHOLDTIME {
*|      : is_calconst;
*|      : description = "The HMI Module shall use the calibration DepartureTime_ButtonHoldTime to control the time a �+� or �-� button must be held to increment the time.\
 If the �+� hour button is held for more than 750ms, the silverbox shall increment the hour by one every 250ms.\
If the �-� hour button is held for more than 750ms, the silverbox shall decrement the hour by one every 250ms. \
If the �+� minute button is held for more than 750ms, the silverbox shall increment the minutes in 15 mins increments every 250ms.\
If the �-� minute button is held for more than 750ms, the silverbox shall decrement the minutes in 15 mins increments every 250ms.";
*|      : units = "ms";
*|      : transform = fixed.DEPARTURETIME_BUTTONHOLDTIME;
*| } 
*/ 
const char DEPARTURETIME_BUTTONHOLDTIME = 0x4B;

/*
*| hmi_navn_map_database_graphics_caln.DEPARTURETIME_HOURINCREMENTDECREMENTRATE {
*|      : is_calconst;
*|      : description = "The HMI Module shall use the calibration DepartureTime_HourIncrementDecrementRate to control the amount by which the hour value changes when button is held.\
 If the �+� hour button is held for more than 750ms, the silverbox shall increment the hour by one every 250ms.\
If the �-� hour button is held for more than 750ms, the silverbox shall decrement the hour by one every 250ms.\
If the �+� minute button is held for more than 750ms, the silverbox shall increment the minutes in 15 mins increments every 250ms.\
If the �-� minute button is held for more than 750ms, the silverbox shall decrement the minutes in 15 mins increments every 250ms.";
*|      : units = "hour";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DEPARTURETIME_HOURINCREMENTDECREMENTRATE = 0x01;

/*
*| hmi_navn_map_database_graphics_caln.DEPARTURETIME_MINUTEINCREMENTDECREMENTRATE {
*|      : is_calconst;
*|      : description = "The HMI Module shall use the calibration DepartureTime_MinuteIncrementDecrementRate to control the amount by which the minute value changes when button is held.\
 If the �+� hour button is held for more than 750ms, the silverbox shall increment the hour by one every 250ms.\
If the �-� hour button is held for more than 750ms, the silverbox shall decrement the hour by one every 250ms.\
If the �+� minute button is held for more than 750ms, the silverbox shall increment the minutes in 15 mins increments every 250ms.\
If the �-� minute button is held for more than 750ms, the silverbox shall decrement the minutes in 15 mins increments every 250ms.";
*|      : units = "min";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DEPARTURETIME_MINUTEINCREMENTDECREMENTRATE = 0x0F;

/*
*| hmi_navn_map_database_graphics_caln.MM_SCREEN_POPUP_TIMER {
*|      : is_calconst;
*|      : description = "Defines how long the Display shall display the Maintenance Mode alerts.  The Alert Manager shall utilize this value for �AutoTimeout� value for all Maintenance Mode Alerts.";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MM_SCREEN_POPUP_TIMER = 0x0F;

/*
*| hmi_navn_map_database_graphics_caln.CAL_HMI_NAV_ICON_END {
*|      : is_calconst;
*|      : description = "END OF CAL BLOCK";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CAL_HMI_NAV_ICON_END = 0x00;
