
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#include <EOLLib.h>

/*
MOD ID 07 - HMI BRAND CALN
*/

/*
*| hmi_brand_caln.HEADER_BRAND {
*|      : is_calconst;
*|      : description = "CALDS Header";
*| } 
*/ 
const EOLLib_CalDsHeader HEADER_BRAND = {0x0000, EOLLIB_TABLE_ID_BRAND << 8, 0x0000, 0x00000000, {0x41, 0x41}, 0x0401};

/*
*| hmi_brand_caln.GM_VEHICLE_BRAND {
*|      : is_calconst;
*|      : description = "The  GM_VEHICLE_BRAND sets the theme of the HMI.   For brands that support multiple themes, such as Chevrolet, GM_VEHICLE_BRAND is extended with DEFAULT_CHEVY_GRAPHICAL_THEME to select the factory setting menu default and CHEVY_THEMES_AVAILABLE_BYTE which determines the user selectable Chevy themes available all per GIS-40X Theme available section.\
\
NOTE:  Animations and AudioCues are not directly tied to GM_VEHICLE_BRAND starting in GIS-344 V2.2.  Instead, animations following the following six calibrations specuific to a given vehicle line:  \
VEHICLE_ANIMATION_WELCOME_TYPE\
VEHICLE_ANIMATION_STARTUP_TYPE\
VEHICLE_ANIMATION_SHUTDOWN_TYPE\
VEHICLE_AUDIOCUE_WELCOME_TYPE\
VEHICLE_AUDIOCUE_STARTUP_TYPE\
VEHICLE_AUDIOCUE_SHUTDOWN_TYPE\
\
In GIS-344 V2.5, original 72 animation / audioCue calibrations are reduced to 6.  This allows for different vehicle lines to have different animations/cues which is planned.  However, it accepts that animations/cues for a specific vehicle w...";
*|      : units = "Enumeration";
*|      : type = fixed.GM_VEHICLE_BRAND;
*| } 
*/ 
const char GM_VEHICLE_BRAND = 0x02;

const char BRAND_DUMMY_1[3] =
 {
  0x00, 0x00, 0x00
 }
;

const unsigned short BRAND_DUMMY_2[31] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

const unsigned short BRAND_DUMMY_3[44] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000
 }
;

const char BRAND_DUMMY_4 = 0x00;

const char BRAND_DUMMY_10 = 0x00;

/*
*| hmi_brand_caln.GYRO_OFFSET_X {
*|      : is_calconst;
*|      : description = "The navigation system shall support the calibration of the gyro offset by allowing an orientation of +/- 180 degrees from the X-axis and Y-axis";
*|      : units = "SNM";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short GYRO_OFFSET_X = 0x0000;

/*
*| hmi_brand_caln.GYRO_OFFSET_Y {
*|      : is_calconst;
*|      : description = "The navigation system shall support the calibration of the gyro offset by allowing an orientation of +/- 180 degrees from the X-axis and Y-axis";
*|      : units = "SNM";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short GYRO_OFFSET_Y = 0x0000;

/*
*| hmi_brand_caln.GYRO_OFFSET_Z {
*|      : is_calconst;
*|      : description = "The navigation system shall support the calibration of the gyro offset by allowing an orientation of +/- 180 degrees from the X-axis and Y-axis";
*|      : units = "SNM";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short GYRO_OFFSET_Z = 0x0000;

/*
*| hmi_brand_caln.SWMI_CALIBRATION_DATA_FILE_HMI_BRAND_CAL {
*|      : is_calconst;
*|      : description = "The NoCalibration state is defined in order to make sure that Infotainment subsystem components have been updated with calibrations after a service event such as replacing one or more modules or upgrading software.  The mechanism for NoCalibration determination is the same as that for Theftlock and NoVIN determination.  That is, at initialization a module detects whether or not it has a valid calibration.  If it does not, it notifies the SystemState FBlock via the SystemState.SetNoCalibrationModuleState method.  Once a valid calibration is received, the module calls the SystemState.SetNoCalibrationModuleState method to clear the condition.\
The SystemState FBlock shall assume all module calibrations are valid upon each Sleep cycle.  Modules that already have a valid calibration or do not support NoCalibration detection (determined by each module CTS) do not need to report the NoCalibration clear condition at each initialization.\
Note: each of these Calibrations should be ...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SWMI_CALIBRATION_DATA_FILE_HMI_BRAND_CAL = 0x00;

/*
*| hmi_brand_caln.CHEVY_APPS_TRAY_INACTIVITY_TIMER {
*|      : is_calconst;
*|      : description = "When the user has not interacted with the touchscreen\
display or faceplate controls for 30 seconds (calibratible)\
while viewing the full map view, the app tray and\
interaction selection fades into the background over a 3\
second period (calibratable - same as alert widget cal).";
*|      : units = "ms";
*|      : transform = fixed.CHEVY_APPS_TRAY_INACTIVITY_TIMER;
*| } 
*/ 
const char CHEVY_APPS_TRAY_INACTIVITY_TIMER = 0x96;

/*
*| hmi_brand_caln.DEFAULT_CHEVY_GRAPHICAL_THEME {
*|      : is_calconst;
*|      : description = "In discussion with Bosch there is a cal needed for \"graphical themes\".  For example, Chevrolet is a variant of the Cadillac HMI but there are themes (modern family, youth, truck,..) that are different \"skins\" - colors, background etc.  Each Chevrolet model may use a different theme.  So there is a Cadillac Variant with one theme, Chevrolet variant with 4 themes, GMC will be a theme under Chevrolet variant, and Buick / Opel is a variant of Chevrolet with one theme.";
*|      : units = "Enum";
*|      : type = fixed.DEFAULT_CHEVY_GRAPHICAL_THEME;
*| } 
*/ 
const char DEFAULT_CHEVY_GRAPHICAL_THEME = 0x01;

/*
*| hmi_brand_caln.FACEPLATE_FLING_MIN_SLIDER_POSITIONS {
*|      : is_calconst;
*|      : description = "This defines the minimum number of sensor positions that the HMI must detect before I qualifies the action as a swipe/fling of the faceplate volume.";
*|      : units = "Slider Positions";
*|      : type = fixed.UB0;
*| } 
*/ 
const char FACEPLATE_FLING_MIN_SLIDER_POSITIONS = 0x02;

/*
*| hmi_brand_caln.MAX_FACEPLATE_VOLUME_SLIDER_SENSORS {
*|      : is_calconst;
*|      : description = "This calibration defines the number of slider sensors the Faceplate has to generate the 0 - 100% information.";
*|      : units = "Faceplate Sensors";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MAX_FACEPLATE_VOLUME_SLIDER_SENSORS = 0x0C;

/*
*| hmi_brand_caln.FACEPLATE_VOLUME_STEP_PER_SLIDER_SENSOR {
*|      : is_calconst;
*|      : description = "This Calibrations allows for flexiblity as faceplates increas there number of sensors to still obtain the desired effect for the number of swipes needed to reach max volume. ";
*|      : units = "Steps";
*|      : transform = fixed.FACEPLATE_VOLUME_STEP_PER_SLIDER_SENSOR;
*| } 
*/ 
const unsigned short FACEPLATE_VOLUME_STEP_PER_SLIDER_SENSOR = 0x0014;

const unsigned short BRAND_DUMMY_5[5] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

/*
*| hmi_brand_caln.MAIN_DISP_FAILSOFT {
*|      : is_calconst;
*|      : description = "Value that is usee initialy before any information received from SBX";
*|      : units = "%Luminance";
*|      : transform = fixed.MAIN_DISP_FAILSOFT;
*| } 
*/ 
const unsigned short MAIN_DISP_FAILSOFT = 0x43FE;

const char BRAND_DUMMY_6 = 0x00;

/*
*| hmi_brand_caln.ENABLE_FACEPLATE_VOLUME_TAP {
*|      : is_calconst;
*|      : description = "Volume Tap";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_FACEPLATE_VOLUME_TAP = 0x01;

/*
*| hmi_brand_caln.ENABLE_FACEPLATE_VOLUME_PRESS_HOLD {
*|      : is_calconst;
*|      : description = "If the user presses and holds on a side, the volume\
ramps up or down depending on the side of the bar the\
hold on.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_FACEPLATE_VOLUME_PRESS_HOLD = 0x01;

/*
*| hmi_brand_caln.VOLUME_PRESS_HOLD_VOLUMESLEWRATE {
*|      : is_calconst;
*|      : description = "There should be some slew rate for the press and hold of volume adjustment on the Display.";
*|      : units = "ms";
*|      : transform = fixed.VOLUME_PRESS_HOLD_VOLUMESLEWRATE;
*| } 
*/ 
const char VOLUME_PRESS_HOLD_VOLUMESLEWRATE = 0x19;

const unsigned short BRAND_DUMMY_7[36] =
 {
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000
 }
;

const char BRAND_DUMMY_8 = 0x00;

/*
*| hmi_brand_caln.FACEPLATE_VOLUME_TAP_VOLUME_STEP_DELTA {
*|      : is_calconst;
*|      : description = "This defines the number of Volume Steps each tap on the Faceplate Volume bar either increments or decrements ";
*|      : units = "Step";
*|      : type = fixed.UB0;
*| } 
*/ 
const char FACEPLATE_VOLUME_TAP_VOLUME_STEP_DELTA = 0x01;

/*
*| hmi_brand_caln.HVAC_RADIO_DISPLAY_ENABLE {
*|      : is_calconst;
*|      : description = "CLIMATE ICON enable/disable.  Only Enable for Cadillac branded vehicles.\
Hvac_Radio_Display_Enable shall perform the following actions:\
o Hvac_Radio_Display_Enable = $00 Disable  -> Never display Climate Page HMI.  Remove from APP list.\
o Hvac_Radio_Display_Enable =$01 Enable  -> Climate Page HMI is available.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char HVAC_RADIO_DISPLAY_ENABLE = 0x01;

/*
*| hmi_brand_caln.REAR_HVAC_RADIO_DISPLAY_ENABLE {
*|      : is_calconst;
*|      : description = "CLIMATE ICON enable/disable.  Only Enable for Cadillac branded vehicles with Rear HVAC controls to be controlled via front system.\
REAR CLIMATE ICON enable / disable.\
Rear_Hvac_Radio_Display_Enable shall perform the following actions:\
o Rear_Hvac_Radio_Display_Enable = $00 Disable  -> Never display Rear Climate Page HMI.  Remove from APP list.\
o Rear_Hvac_Radio_Display_Enable =$01 Enable  -> Rear Climate Page HMI is available.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char REAR_HVAC_RADIO_DISPLAY_ENABLE = 0x01;

/*
*| hmi_brand_caln.MAN_MODE {
*|      : is_calconst;
*|      : description = "4 discrete mode = Cadillac / Chevy / GMC. \
3 mode combi for Opel / Buick / Vauxhaul / Holden\
\
Man_Mode shall perform the following actions:\
o ManMode  = $00 4-Button  -> display 4 button + Defrost Air Distribution Mode scheme on  Climate Page HMI and Pop-up HMI .\
\
o ManMode  = $01 3-Button  -> display �3 button combi + Defrost� Air Distribution Mode scheme on Climate Page HMI and Pop-Up HMI.\
";
*|      : units = "Enumeration";
*|      : type = fixed.MAN_MODE;
*| } 
*/ 
const char MAN_MODE = 0x00;

/*
*| hmi_brand_caln.ENABLE_APPLICATIONTRAY_CLIMATECONTROL {
*|      : is_calconst;
*|      : description = "The four default applications in the tray are:\
Audio, Phone, Navigation, Climate Control\
If the navigation system is not available on the vehicle, the nav application is still shown but has no action when tapped. It provides direction information only.\
UPDATE:\
Item must be in all CALDS bundles and common to all HMI software sets.  \
*  However, the calibration only functions with 8\" systems and shall have the default described in this document.  \
*  The calibration will perform no action in the 4.2\" software sets and shall have default of 0 = DISABLE.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_CLIMATECONTROL = 0x01;

/*
*| hmi_brand_caln.ENABLE_REVEALONAPPROACH {
*|      : is_calconst;
*|      : description = "9.13.4.4~MY13 Proximity Sensing\
If the system has Proximity Sensing, the Proximity Sensing setting menu should be dynamically added to the Display menu options.\
The options shown to the user are as follows:\
� Off\
� On\
� On - Map Only\
\
The Apps Tray is normally hidden from view. The Apps Tray is revealed on proximity on almost all views. In cases where the Apps Tray is NOT shown, it is documented in that section. Apps Tray will fade when reveal on approach time expires.";
*|      : units = "Enumeration";
*|      : type = fixed.ENABLE_REVEALONAPPROACH;
*| } 
*/ 
const char ENABLE_REVEALONAPPROACH = 0x01;

/*
*| hmi_brand_caln.ENABLE_REVEALONAPPROACHFADEOUT {
*|      : is_calconst;
*|      : description = "The Apps Tray is normally hidden from view. The Apps\
Tray is revealed on proximity on almost all views. In\
cases where the Apps Tray is NOT shown, it is documented\
in that section. Apps Tray will fade when reveal\
on approach time expires.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_REVEALONAPPROACHFADEOUT = 0x01;

/*
*| hmi_brand_caln.CHEVY_THEMES_AVAILABLE_BYTE {
*|      : is_calconst;
*|      : description = "The calibration defining which Chevy themes are available for customer selection is �Chevy_Themes_Available�.\
This calibration will indicate which of the MANY Chevy themes should be shown as available themes for the customer to choose from for a given vehicle.\
Specific logic must be applied to handle the user display of available themes as follows:\
If this calibration shows that only one theme is available\
(or none are available, in the case of non-Chevy brand vehicles), the �Theme� menu option should not be visible to the customer in the Display settings.\
If more than one theme is calibrated as being available in the system, the �Theme� menu option should be shown as visible. Within the Theme menu, only those\
themes that are calibrated as available would then be shown.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CHEVY_THEMES_AVAILABLE_BYTE = 0x48;

const char BRAND_DUMMY_9 = 0x00;

/*
*| hmi_brand_caln.FACEPLATE_TAP_MAX_TIME {
*|      : is_calconst;
*|      : description = "This defines the amount of time that still qualifies the action as a Tap and not a press and hold.";
*|      : units = "ms";
*|      : transform = fixed.FACEPLATE_TAP_MAX_TIME;
*| } 
*/ 
const char FACEPLATE_TAP_MAX_TIME = 0x96;

/*
*| hmi_brand_caln.VEHICLE_ANIMATION_WELCOME_TYPE {
*|      : is_calconst;
*|      : description = "After powerup, the silverbox shall display a Welcome Animation which is specific to the brand of the vehicle.\
The welcome animation shall be defined by a calibration setting as shown below\
";
*|      : units = "Enumeration";
*|      : type = fixed.VEHICLE_ANIMATION_WELCOME_TYPE;
*| } 
*/ 
const char VEHICLE_ANIMATION_WELCOME_TYPE = 0x00;

/*
*| hmi_brand_caln.VEHICLE_ANIMATION_STARTUP_TYPE {
*|      : is_calconst;
*|      : description = "Start-up Audio Cues type\
The Start-up Audio Cues shall be defined by a calibration setting as shown below\
";
*|      : units = "Enumeration";
*|      : type = fixed.VEHICLE_ANIMATION_STARTUP_TYPE;
*| } 
*/ 
const char VEHICLE_ANIMATION_STARTUP_TYPE = 0x00;

/*
*| hmi_brand_caln.VEHICLE_ANIMATION_SHUTDOWN_TYPE {
*|      : is_calconst;
*|      : description = "Shutdown Animation type\
The shutdown animation shall be defined by a calibration setting as shown below\
";
*|      : units = "Enumeration";
*|      : type = fixed.VEHICLE_ANIMATION_SHUTDOWN_TYPE;
*| } 
*/ 
const char VEHICLE_ANIMATION_SHUTDOWN_TYPE = 0x00;

/*
*| hmi_brand_caln.VEHICLE_AUDIOCUE_WELCOME_TYPE {
*|      : is_calconst;
*|      : description = "Welcome Audio Cues type\
The welcome Audio Cues shall be defined by a calibration setting as shown below\
";
*|      : units = "Enumeration";
*|      : type = fixed.VEHICLE_AUDIOCUE_WELCOME_TYPE;
*| } 
*/ 
const char VEHICLE_AUDIOCUE_WELCOME_TYPE = 0x00;

/*
*| hmi_brand_caln.VEHICLE_AUDIOCUE_STARTUP_TYPE {
*|      : is_calconst;
*|      : description = "Start-up Audio Cues type\
The Start-up Audio Cues shall be defined by a calibration setting as shown below\
";
*|      : units = "Enumeration";
*|      : type = fixed.VEHICLE_AUDIOCUE_STARTUP_TYPE;
*| } 
*/ 
const char VEHICLE_AUDIOCUE_STARTUP_TYPE = 0x00;

/*
*| hmi_brand_caln.VEHICLE_AUDIOCUE_SHUTDOWN_TYPE {
*|      : is_calconst;
*|      : description = "Shutdown Audio Cues type\
The Shutdown  Cues shall be defined by a calibration setting as shown below\
";
*|      : units = "Enumeration";
*|      : type = fixed.VEHICLE_AUDIOCUE_SHUTDOWN_TYPE;
*| } 
*/ 
const char VEHICLE_AUDIOCUE_SHUTDOWN_TYPE = 0x00;

/*
*| hmi_brand_caln.SETTINGS_MASK_BYTE_3 {
*|      : is_calconst;
*|      : description = "Menu Configuration Setting and function enable.\
Enable for Cadillac.  Disalble for all non-Cadillac.\
\
Proximity Sensing in the center stack enables specific\
interaction capabilities with the system. Proximity sensing\
in the center stack is used to provide feedback to\
the user and keep the screen clutter-free (i.e., elegant).\
When a user�s hand approaches the display, it comes\
alive and interactive elements are displayed that the\
user can touch. Users can interact with the interface in\
an exciting and delightful way when the system detects\
their hand approaching the screen.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SETTINGS_MASK_BYTE_3 = 0x80;

/*
*| hmi_brand_caln.BLUETOOTH_MODULE_DEVICE_NAME_STRING {
*|      : is_calconst;
*|      : description = "The Bluetooth Module device name (user-friendly name) shall be based on the vehicle brand and model. The vehicle brand and model shall be decoded from the VIN that is acquired from the Silverbox.\
The Bluetooth Module shall not allow changes of the device name.\
";
*|      : units = "Characters (UTF-8)";
*|      : type = fixed.UB0;
*| } 
*/ 
const char BLUETOOTH_MODULE_DEVICE_NAME_STRING[33] =
 {
  0x43, 0x41, 0x44, 0x49, 0x4C, 0x4C, 0x41, 0x43, 0x20, 0x58, 0x54, 0x53, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_brand_caln.VEHICLE_ICON_BODYSTYLE {
*|      : is_calconst;
*|      : description = "The current vehicle location is shown on the map as an\
icon. The icon looks like the vehicle and is shown in the\
color that it was purchased";
*|      : units = "ENM";
*|      : type = fixed.VEHICLE_ICON_BODYSTYLE;
*| } 
*/ 
const char VEHICLE_ICON_BODYSTYLE = 0x00;

/*
*| hmi_brand_caln.DTC_MASK_USB1 {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_USB1 = 0x00;

/*
*| hmi_brand_caln.DTC_MASK_USB2 {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_USB2 = 0x00;

/*
*| hmi_brand_caln.DTC_LVDS {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.  Reference DTC B127E_00";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DTC_LVDS = 0x00;

/*
*| hmi_brand_caln.CHARACTER_RECOGNITION_ON_FRONT_TOUCHSCREEN_DISPLAY_ENABLE {
*|      : is_calconst;
*|      : description = "Main calibration to enable/disable the Character_Recognition feature HMI on the front touchscreen display.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char CHARACTER_RECOGNITION_ON_FRONT_TOUCHSCREEN_DISPLAY_ENABLE = 0x01;

/*
*| hmi_brand_caln.TOUCHPAD_TIME_TO_RECOGNIZE_A_DOUBLE_TAP {
*|      : is_calconst;
*|      : description = "A Double Tap gesture consists of two Taps of the touch pad within a short duration of each other; for example, two taps in the approximate same location (that can be calibrated)";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_TIME_TO_RECOGNIZE_A_DOUBLE_TAP;
*| } 
*/ 
const char TOUCHPAD_TIME_TO_RECOGNIZE_A_DOUBLE_TAP = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GLIDE_LIST_ACCELERATION_COEFF {
*|      : is_calconst;
*|      : description = "List acceleration coefficient - for Glide function when using the touchpad gestures to move a list.  Applies only to vertical (up or down) list gestures.";
*|      : units = "uniits";
*|      : transform = fixed.TOUCHPAD_GLIDE_LIST_ACCELERATION_COEFF;
*| } 
*/ 
const unsigned long TOUCHPAD_GLIDE_LIST_ACCELERATION_COEFF = 0x00000000;

/*
*| hmi_brand_caln.TOUCHPAD_HIGHLIGHT_TIMEOUT {
*|      : is_calconst;
*|      : description = "GIS-407";
*|      : units = "Seconds";
*|      : transform = fixed.TOUCHPAD_HIGHLIGHT_TIMEOUT;
*| } 
*/ 
const char TOUCHPAD_HIGHLIGHT_TIMEOUT = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_NAV_MENU_DELAY {
*|      : is_calconst;
*|      : description = "GIS-407";
*|      : units = "Seconds";
*|      : transform = fixed.TOUCHPAD_NAV_MENU_DELAY;
*| } 
*/ 
const char TOUCHPAD_NAV_MENU_DELAY = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_DOUBLE_TAP_TIME {
*|      : is_calconst;
*|      : description = "A Double Tap gesture may be used to actuate a feature\
such as launch an application or peform a secondary function of a control. A Double Tap gesture consists of two Taps of the touch screen within a short duration of\
each other; for example, two taps in the approximate same location on the screen within 750 msecs of each other. Details of the action to be performed, including\
the max duration between taps, when a Double Tap is detected are described in the Application sections where used.";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_GESTURE_DOUBLE_TAP_TIME;
*| } 
*/ 
const char TOUCHPAD_GESTURE_DOUBLE_TAP_TIME = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_PRESS_HOLD_TIME_1 {
*|      : is_calconst;
*|      : description = "used for feaures like: ScrollBar_PageUpOrDown, StoringFavorites, EditFavorites, MoveFavorties, KeyboardDeleteButton,ContactKeypadEntryDeleteButton, RearrangingAppsICONs, AdjustingToneSettings, AdjustingToneSettings, StrongStationSearch, AllStationSearch, TimesShiftFastRate1, CDFastTrackSearch, MyMediaFastTrackSearch, Rotate_Camera_View, Front_Temperature_Adjustment, Rear_Temperature_Adjustment, Front_Fan_Speed_Adjustment, Rear_Fan_Speed_Adjustment, Phone_Keypad_+_*_Keys,  Phone_Keypad_Speed_Dial, New_Predefined_Msg_View_Delete_Word, Text_Signature_Setting_Delete_Word, Text_Signature_Setting_Delete_Entire_Field";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_GESTURE_PRESS_HOLD_TIME_1;
*| } 
*/ 
const char TOUCHPAD_GESTURE_PRESS_HOLD_TIME_1 = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_PRESS_HOLD_TIME_2 {
*|      : is_calconst;
*|      : description = "used for features like: XMFastChannelSearch, TimeShiftFastRate2, Phone_Delete_Entire_Field, New_Predefined_Msg_View_Delete_Entire_Field, Text_Signature_Setting_Delete_Entire_Field";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_GESTURE_PRESS_HOLD_TIME_2;
*| } 
*/ 
const char TOUCHPAD_GESTURE_PRESS_HOLD_TIME_2 = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_PRESS_HOLD_TIME_3 {
*|      : is_calconst;
*|      : description = "If after 5 seconds(calibrateable) the user cntinues the press and Hold Operation, the user continues the press and Hold Operation the system will advance/rewind at a rate of 30x the normal frame rate of video";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_GESTURE_PRESS_HOLD_TIME_3;
*| } 
*/ 
const char TOUCHPAD_GESTURE_PRESS_HOLD_TIME_3 = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_SPREAD_MIN_FACTOR {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Spread Factor";
*|      : transform = fixed.TOUCHPAD_GESTURE_SPREAD_MIN_FACTOR;
*| } 
*/ 
const char TOUCHPAD_GESTURE_SPREAD_MIN_FACTOR = 0x01;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_PINCH_MAX_FACTOR {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Pinch Factor";
*|      : transform = fixed.TOUCHPAD_GESTURE_PINCH_MAX_FACTOR;
*| } 
*/ 
const char TOUCHPAD_GESTURE_PINCH_MAX_FACTOR = 0x01;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_FLING_MIN_DISTANCE {
*|      : is_calconst;
*|      : description = "";
*|      : units = "mm";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_GESTURE_FLING_MIN_DISTANCE = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_FLING_MIN_SPEED {
*|      : is_calconst;
*|      : description = "";
*|      : units = "TBD";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_GESTURE_FLING_MIN_SPEED = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_SPREAD_GAIN {
*|      : is_calconst;
*|      : description = "4.1.1.13~MY14 Spread\
A Spread gesture may be used to zoom in on a portion of the map, image or web\
page, or perform an action specific to an application. To perform a Spread gesture, users place two fingers on the touch pad (e.g., thumb and index finger) and spread the fingers apart. The actual physics parameters for a Spread gesture such as  velocity of finger movement detected; or travel distance  of fingers spreading remain TBD and calibratable and/or will be defined as part of a pre-existing software widget for such an interaction gesture.";
*|      : units = "Spread Gain";
*|      : transform = fixed.TOUCHPAD_GESTURE_SPREAD_GAIN;
*| } 
*/ 
const char TOUCHPAD_GESTURE_SPREAD_GAIN = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_PINCH_GAIN {
*|      : is_calconst;
*|      : description = "4.1.1.14~MY14 Pinch\
A Pinch gesture may be used to zoom out on a map/web page, close a page/view or application, or perform an action specific to an application. To perform a Pinch gestures, users place two fingers on the touch pad (e.g., thumb and index finger) and bring the fingers together in a pinching motion. The actual physics parameters for a Pinch gesture such as velocity of finger movement detected or travel distance of fingers pinching remain TBD and calibratable and/or will be defined as part of a pre-existing software widget for such an interaction gesture.";
*|      : units = "Pinch Gain";
*|      : transform = fixed.TOUCHPAD_GESTURE_PINCH_GAIN;
*| } 
*/ 
const char TOUCHPAD_GESTURE_PINCH_GAIN = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_NUDGE_GAIN {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Nudge Gain";
*|      : transform = fixed.TOUCHPAD_GESTURE_NUDGE_GAIN;
*| } 
*/ 
const char TOUCHPAD_GESTURE_NUDGE_GAIN = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_FLING_GAIN {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Fling Gain";
*|      : transform = fixed.TOUCHPAD_GESTURE_FLING_GAIN;
*| } 
*/ 
const char TOUCHPAD_GESTURE_FLING_GAIN = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_FLING_FRICTION {
*|      : is_calconst;
*|      : description = "";
*|      : units = "TBD";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_GESTURE_FLING_FRICTION = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_TAP_MAX_DISTANCE {
*|      : is_calconst;
*|      : description = "GIS 407 4.1.1";
*|      : units = "Px";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_GESTURE_TAP_MAX_DISTANCE = 0x50;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_GLIDE_MIN_DISTANCE {
*|      : is_calconst;
*|      : description = "GIS 407 4.1.1";
*|      : units = "Px";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_GESTURE_GLIDE_MIN_DISTANCE = 0x3C;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_PINCHSPREAD_MAX_DISTANCE {
*|      : is_calconst;
*|      : description = "GIS 407 4.1.1";
*|      : units = "Px";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_GESTURE_PINCHSPREAD_MAX_DISTANCE = 0x64;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_DECISION_TIMER {
*|      : is_calconst;
*|      : description = "GIS 407 4.1.1";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_GESTURE_DECISION_TIMER;
*| } 
*/ 
const char TOUCHPAD_GESTURE_DECISION_TIMER = 0x19;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_FLING_MAX_SPEED {
*|      : is_calconst;
*|      : description = "GIS 407 4.1.1";
*|      : units = "m/s";
*|      : transform = fixed.TOUCHPAD_GESTURE_FLING_MAX_SPEED;
*| } 
*/ 
const char TOUCHPAD_GESTURE_FLING_MAX_SPEED = 0x50;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_GLIDE_GAIN {
*|      : is_calconst;
*|      : description = "GIS 407 4.1.1";
*|      : units = "1";
*|      : transform = fixed.TOUCHPAD_GESTURE_GLIDE_GAIN;
*| } 
*/ 
const char TOUCHPAD_GESTURE_GLIDE_GAIN = 0x50;

/*
*| hmi_brand_caln.TOUCHPAD_GESTURE_DOUBLE_TOUCH_TIME {
*|      : is_calconst;
*|      : description = "GIS 407 4.1.1";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_GESTURE_DOUBLE_TOUCH_TIME;
*| } 
*/ 
const char TOUCHPAD_GESTURE_DOUBLE_TOUCH_TIME = 0x4B;

/*
*| hmi_brand_caln.TOUCHPAD_CHARACTER_RECOGNITION_ON_TOUCHPAD_ENABLE {
*|      : is_calconst;
*|      : description = "Main calibration to enable/disable the TouchPad feature and HMI on the touchpad.";
*|      : units = "Enumeration";
*|      : type = fixed.TOUCHPAD_CHARACTER_RECOGNITION_ON_TOUCHPAD_ENABLE;
*| } 
*/ 
const char TOUCHPAD_CHARACTER_RECOGNITION_ON_TOUCHPAD_ENABLE = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_CHARACTER_RECOGNITION_PANE_TIMEOUT {
*|      : is_calconst;
*|      : description = "GIS-407";
*|      : units = "Seconds";
*|      : transform = fixed.TOUCHPAD_CHARACTER_RECOGNITION_PANE_TIMEOUT;
*| } 
*/ 
const char TOUCHPAD_CHARACTER_RECOGNITION_PANE_TIMEOUT = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_1 {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_1;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_1 = 0x21;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_1 {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_1;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_1 = 0x31;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_1 {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_1;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_1 = 0x41;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_2 {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_2;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_2 = 0x78;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_2 {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_2;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_2 = 0xB4;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_2 {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_2;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_2 = 0xF0;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_3 {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_3;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_FASTER_3 = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_3 {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_3;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_DEFAULT_3 = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_3 {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_3;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SLOWER_3 = 0x00;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SETTING_DEFAULT {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "enum";
*|      : type = fixed.TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SETTING_DEFAULT;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_DRAW_SPEED_SETTING_DEFAULT = 0x01;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_FASTER {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_FASTER;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_FASTER = 0x04;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_DEFAULT {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_DEFAULT;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_DEFAULT = 0x14;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_SLOWER {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "ms";
*|      : transform = fixed.TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_SLOWER;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_SLOWER = 0x1E;

/*
*| hmi_brand_caln.TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_SETTING_DEFAULT {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.1~MY14 Speed Settings";
*|      : units = "enum";
*|      : type = fixed.TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_SETTING_DEFAULT;
*| } 
*/ 
const char TOUCHPAD_CHAR_RECOG_CHAR_SELECT_SPEED_SETTING_DEFAULT = 0x01;

/*
*| hmi_brand_caln.TOUCHPAD_DISPLAY_SCALING_FACTOR_X {
*|      : is_calconst;
*|      : description = "GIS 407 Appendix";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_DISPLAY_SCALING_FACTOR_X = 0x3C;

/*
*| hmi_brand_caln.TOUCHPAD_DISPLAY_SCALING_FACTOR_Y {
*|      : is_calconst;
*|      : description = "GIS 407 Appendix";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_DISPLAY_SCALING_FACTOR_Y = 0x3C;

/*
*| hmi_brand_caln.TOUCHPAD_AUDIO_FEEDBACK_KEYBOARD {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.2";
*|      : units = "enum";
*|      : type = fixed.TOUCHPAD_AUDIO_FEEDBACK_KEYBOARD;
*| } 
*/ 
const char TOUCHPAD_AUDIO_FEEDBACK_KEYBOARD = 0x01;

/*
*| hmi_brand_caln.TOUCHPAD_AUDIO_FEEDBACK_LIST {
*|      : is_calconst;
*|      : description = "GIS 407 4.2.6.2";
*|      : units = "enum";
*|      : type = fixed.TOUCHPAD_AUDIO_FEEDBACK_LIST;
*| } 
*/ 
const char TOUCHPAD_AUDIO_FEEDBACK_LIST = 0x01;

/*
*| hmi_brand_caln.TOUCHPAD_FOCUS_CHANGE_HYSTERESIS {
*|      : is_calconst;
*|      : description = "GIS 407 Appendix";
*|      : units = "Pixel";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_FOCUS_CHANGE_HYSTERESIS = 0x32;

/*
*| hmi_brand_caln.L_REAR_CROSS_TRAFFIC_U_PIXEL {
*|      : is_calconst;
*|      : description = "Rear Cross Traffic ICON Location - Screen";
*|      : units = "pixel";
*|      : transform = fixed.L_REAR_CROSS_TRAFFIC_U_PIXEL;
*| } 
*/ 
const unsigned short L_REAR_CROSS_TRAFFIC_U_PIXEL = 0x0078;

/*
*| hmi_brand_caln.L_REAR_CROSS_TRAFFIC_V_PIEXEL {
*|      : is_calconst;
*|      : description = "Rear Cross Traffic ICON Location - Screen";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short L_REAR_CROSS_TRAFFIC_V_PIEXEL = 0x0078;

/*
*| hmi_brand_caln.R_REAR_CROSS_TRAFFIC_U_PIXEL {
*|      : is_calconst;
*|      : description = "Rear Cross Traffic ICON Location - Screen";
*|      : units = "pixel";
*|      : transform = fixed.R_REAR_CROSS_TRAFFIC_U_PIXEL;
*| } 
*/ 
const unsigned short R_REAR_CROSS_TRAFFIC_U_PIXEL = 0x0028;

/*
*| hmi_brand_caln.R_REAR_CROSS_TRAFFIC_V_PIXEL {
*|      : is_calconst;
*|      : description = "Rear Cross Traffic ICON Location - Screen";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short R_REAR_CROSS_TRAFFIC_V_PIXEL = 0x0078;

/*
*| hmi_brand_caln.REAR_CROSS_TRAFFIC_ALERT_FLASHING_HZ {
*|      : is_calconst;
*|      : description = "� If Rear Cross Traffic Alert Left Indication Control : Indication Request  = 2 (Flashing Indication), the visual display icons should be flashed at 2 Hz rate and 50% duty cycle.";
*|      : units = "Hz";
*|      : type = fixed.UB0;
*| } 
*/ 
const char REAR_CROSS_TRAFFIC_ALERT_FLASHING_HZ = 0x04;

/*
*| hmi_brand_caln.REAR_HVAC_RADIO_DISPLAY_TYPE {
*|      : is_calconst;
*|      : description = "\
REAR_HVAC_RADIO_DISPLAY_TYPE shall perform the following actions:\
o REAR_HVAC_RADIO_DISPLAY_TYPE = $00 Auto Single Zone\
\
o REAR_HVAC_RADIO_DISPLAY_TYPE  = $01 Auto Dual Zone\
\
o REAR_HVAC_RADIO_DISPLAY_TYPE  = $02 Manual Single Zone";
*|      : units = "Enumeration";
*|      : type = fixed.REAR_HVAC_RADIO_DISPLAY_TYPE;
*| } 
*/ 
const char REAR_HVAC_RADIO_DISPLAY_TYPE = 0x00;

/*
*| hmi_brand_caln.REAR_MAN_MODE {
*|      : is_calconst;
*|      : description = "REAR_MAN_MODE shall perform the following actions:\
o REAR_MAN_MODE  = $00 4-Button  \
\
o REAR_MAN_MODE  = $01 Up/Down Button";
*|      : units = "Enumeration";
*|      : type = fixed.REAR_MAN_MODE;
*| } 
*/ 
const char REAR_MAN_MODE = 0x00;

/*
*| hmi_brand_caln.K_UPA_TYPE {
*|      : is_calconst;
*|      : description = "Accomodates 8 zone UPA that is available with Global-A.";
*|      : units = "Enum";
*|      : type = fixed.K_UPA_TYPE;
*| } 
*/ 
const char K_UPA_TYPE = 0x00;

/*
*| hmi_brand_caln.UPA_LINE_WIDTH {
*|      : is_calconst;
*|      : description = "The system overlays caution symbols on top of the camera image indicating where objects are being detected by the rear park assist system. Alternatively, a 3-line weight box will outline the zone where an object ispixel detected. The characteristics/behavior of the outline will replicate that of the specified UPA symbols";
*|      : units = "Pixel";
*|      : type = fixed.UB0;
*| } 
*/ 
const char UPA_LINE_WIDTH = 0x03;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_1 {
*|      : is_calconst;
*|      : description = "Assumes 800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_1;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_1 = 0x006E;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_1 {
*|      : is_calconst;
*|      : description = "Assumes 800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_1 = 0x004B;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_2 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_2;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_2 = 0x0052;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_2 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_2 = 0x004B;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_3 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin.  ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_3;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_3 = 0x0031;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_3 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin.";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_3 = 0x004B;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_4 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin.  ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_4;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_4 = 0x0079;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_4 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_4 = 0x006A;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_5 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin.  ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_5;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_5 = 0x0052;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_5 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_5 = 0x006A;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_6 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin.  ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_6;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_6 = 0x0027;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_6 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_6 = 0x006A;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_7 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_7;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_7 = 0x0082;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_7 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_7 = 0x0088;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_8 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin.  ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_8;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_8 = 0x0052;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_8 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_8 = 0x008A;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_9 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin.  ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_9;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_9 = 0x001E;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_9 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_9 = 0x0088;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_10 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin.  ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_10;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_10 = 0x0088;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_10 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_10 = 0x00A3;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_11 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_11;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_11 = 0x0051;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_11 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_11 = 0x00AE;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_U_PIXEL_12 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin.  ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_UPA_CENTER_U_PIXEL_12;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_U_PIXEL_12 = 0x0018;

/*
*| hmi_brand_caln.RVS_UPA_CENTER_V_PIXEL_12 {
*|      : is_calconst;
*|      : description = "Assumes  800'480 image.  Top left corner is origin. ";
*|      : units = "pixel";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_UPA_CENTER_V_PIXEL_12 = 0x00A3;

/*
*| hmi_brand_caln.RVS_STEER_REAR_AXLE_OFFSET {
*|      : is_calconst;
*|      : description = "Distance from rear axle to back edge of rear bumper . ";
*|      : units = "mm";
*|      : transform = fixed.RVS_STEER_REAR_AXLE_OFFSET;
*| } 
*/ 
const char RVS_STEER_REAR_AXLE_OFFSET = 0x80;

/*
*| hmi_brand_caln.RVS_STEER_INVERSE_FRONT_RATIO {
*|      : is_calconst;
*|      : description = "Inverse of steering ratio - front wheels  .";
*|      : units = "";
*|      : transform = fixed.RVS_STEER_INVERSE_FRONT_RATIO;
*| } 
*/ 
const unsigned short RVS_STEER_INVERSE_FRONT_RATIO = 0x16BC;

/*
*| hmi_brand_caln.RVS_STEER_INVERSE_REAR_RATIO {
*|      : is_calconst;
*|      : description = "Inverse of steering ratio - rear wheels, low speed, (always zero unless quad-steering)  .";
*|      : units = "";
*|      : transform = fixed.RVS_STEER_INVERSE_REAR_RATIO;
*| } 
*/ 
const unsigned short RVS_STEER_INVERSE_REAR_RATIO = 0x0000;

/*
*| hmi_brand_caln.RVS_STEER_WHEELBASE {
*|      : is_calconst;
*|      : description = "Vehicle wheelbase ";
*|      : units = "mm";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_STEER_WHEELBASE = 0x0B82;

/*
*| hmi_brand_caln.RVS_STEER_SATURATION_ANGLE {
*|      : is_calconst;
*|      : description = "Steering wheel angle above which road wheel angle does not change  . ";
*|      : units = "deg";
*|      : transform = fixed.RVS_STEER_SATURATION_ANGLE;
*| } 
*/ 
const char RVS_STEER_SATURATION_ANGLE = 0x63;

/*
*| hmi_brand_caln.RVS_STEERING_WHEEL_DELTA {
*|      : is_calconst;
*|      : description = "RCS Steering Wheel Delta. ";
*|      : units = "deg";
*|      : type = fixed.UB0;
*| } 
*/ 
const char RVS_STEERING_WHEEL_DELTA = 0x05;

/*
*| hmi_brand_caln.RVS_OVLAY_TRACK_WIDTH {
*|      : is_calconst;
*|      : description = "Lateral distance between guidelines . ";
*|      : units = "mm";
*|      : transform = fixed.RVS_OVLAY_TRACK_WIDTH;
*| } 
*/ 
const char RVS_OVLAY_TRACK_WIDTH = 0x5F;

/*
*| hmi_brand_caln.RVS_OVLAY_DELTA_DIST_MARKS {
*|      : is_calconst;
*|      : description = "Spacing between longitudinal distance lines from first distance marker at 1m. . ";
*|      : units = "Enum";
*|      : type = fixed.RVS_OVLAY_DELTA_DIST_MARKS;
*| } 
*/ 
const char RVS_OVLAY_DELTA_DIST_MARKS = 0x01;

/*
*| hmi_brand_caln.RVS_OVLAY_NUM_DIST_MARKS {
*|      : is_calconst;
*|      : description = "Number of longitudinal distance lines including first distance marker at 1m. . ";
*|      : units = "Lines";
*|      : type = fixed.UB0;
*| } 
*/ 
const char RVS_OVLAY_NUM_DIST_MARKS = 0x05;

/*
*| hmi_brand_caln.RVS_CAMERA_ROTATION_X {
*|      : is_calconst;
*|      : description = "rotation vector (gets converted to rotation matrix) - depends on the mounting height/angle of the camera. ";
*|      : units = "deg (signed)";
*|      : transform = fixed.RVS_CAMERA_ROTATION_X;
*| } 
*/ 
const unsigned short RVS_CAMERA_ROTATION_X = 0x1450;

/*
*| hmi_brand_caln.RVS_CAMERA_ROTATION_Y {
*|      : is_calconst;
*|      : description = "same as omegaX .";
*|      : units = "deg (signed)";
*|      : transform = fixed.RVS_CAMERA_ROTATION_Y;
*| } 
*/ 
const unsigned short RVS_CAMERA_ROTATION_Y = 0x0000;

/*
*| hmi_brand_caln.RVS_CAMERA_ROTATION_Z {
*|      : is_calconst;
*|      : description = "same as omegaX . ";
*|      : units = "deg (signed)";
*|      : transform = fixed.RVS_CAMERA_ROTATION_Z;
*| } 
*/ 
const unsigned short RVS_CAMERA_ROTATION_Z = 0x0000;

/*
*| hmi_brand_caln.RVS_CAMERA_OFFSET_X {
*|      : is_calconst;
*|      : description = "loc of camera lens center to right of vehicle center (when facing rearward) . ";
*|      : units = "mm (signed)";
*|      : transform = fixed.RVS_CAMERA_OFFSET_X;
*| } 
*/ 
const char RVS_CAMERA_OFFSET_X = 0x00;

/*
*| hmi_brand_caln.RVS_CAMERA_OFFSET_Y {
*|      : is_calconst;
*|      : description = "loc of camera lens center rearward from back edge of rear bumper (normally this will be a negative number) . ";
*|      : units = "mm (signed)";
*|      : transform = fixed.RVS_CAMERA_OFFSET_Y;
*| } 
*/ 
const char RVS_CAMERA_OFFSET_Y = 0x00;

/*
*| hmi_brand_caln.RVS_CAMERA_OFFSET_Z {
*|      : is_calconst;
*|      : description = "loc of camera lens center up from ground plane .";
*|      : units = "mm (unsigned)";
*|      : transform = fixed.RVS_CAMERA_OFFSET_Z;
*| } 
*/ 
const char RVS_CAMERA_OFFSET_Z = 0x78;

/*
*| hmi_brand_caln.RVS_CAMERA_FOCAL_LENGTH_U {
*|      : is_calconst;
*|      : description = "horizontal focal length in pixels . ";
*|      : units = "%";
*|      : transform = fixed.RVS_CAMERA_FOCAL_LENGTH_U;
*| } 
*/ 
const unsigned short RVS_CAMERA_FOCAL_LENGTH_U = 0x3980;

/*
*| hmi_brand_caln.RVS_CAMERA_FOCAL_LENGTH_V {
*|      : is_calconst;
*|      : description = "vertical focal length in pixels .";
*|      : units = "%";
*|      : transform = fixed.RVS_CAMERA_FOCAL_LENGTH_V;
*| } 
*/ 
const unsigned short RVS_CAMERA_FOCAL_LENGTH_V = 0x1860;

/*
*| hmi_brand_caln.RVS_CAMERA_DISTORT_COEF_1 {
*|      : is_calconst;
*|      : description = "second order radial distortion coefficient . .";
*|      : units = "signed";
*|      : transform = fixed.RVS_CAMERA_DISTORT_COEF_1;
*| } 
*/ 
const unsigned short RVS_CAMERA_DISTORT_COEF_1 = 0x0000;

/*
*| hmi_brand_caln.RVS_CAMERA_DISTORT_COEF_2 {
*|      : is_calconst;
*|      : description = "fourth order radial distortion coefficient .";
*|      : units = "signed";
*|      : transform = fixed.RVS_CAMERA_DISTORT_COEF_2;
*| } 
*/ 
const unsigned short RVS_CAMERA_DISTORT_COEF_2 = 0x0000;

/*
*| hmi_brand_caln.RVS_CAMERA_OPTICAL_OFFSET_U {
*|      : is_calconst;
*|      : description = "Horizontal offset (in pixels) of optical center from center of image frame - positive right.";
*|      : units = "% (signed)";
*|      : transform = fixed.RVS_CAMERA_OPTICAL_OFFSET_U;
*| } 
*/ 
const char RVS_CAMERA_OPTICAL_OFFSET_U = 0x00;

/*
*| hmi_brand_caln.RVS_CAMERA_OPTICAL_OFFSET_V {
*|      : is_calconst;
*|      : description = "Vertical offset (in pixels) of optical center from center of image frame - positive down .";
*|      : units = "% (signed)";
*|      : transform = fixed.RVS_CAMERA_OPTICAL_OFFSET_V;
*| } 
*/ 
const char RVS_CAMERA_OPTICAL_OFFSET_V = 0x00;

/*
*| hmi_brand_caln.RVS_CAMERA_DELTA_PITCH {
*|      : is_calconst;
*|      : description = "Service mode camera orientation adjustment value. ";
*|      : units = "deg (signed)";
*|      : transform = fixed.RVS_CAMERA_DELTA_PITCH;
*| } 
*/ 
const char RVS_CAMERA_DELTA_PITCH = 0x00;

/*
*| hmi_brand_caln.RVS_CAMERA_DELTA_ROLL {
*|      : is_calconst;
*|      : description = "Service mode camera orientation adjustment value. ";
*|      : units = "deg (signed)";
*|      : transform = fixed.RVS_CAMERA_DELTA_ROLL;
*| } 
*/ 
const char RVS_CAMERA_DELTA_ROLL = 0x00;

/*
*| hmi_brand_caln.RVS_CAMERA_DELTA_YAW {
*|      : is_calconst;
*|      : description = "Service mode camera orientation adjustment value.";
*|      : units = "deg (signed)";
*|      : transform = fixed.RVS_CAMERA_DELTA_YAW;
*| } 
*/ 
const char RVS_CAMERA_DELTA_YAW = 0x00;

/*
*| hmi_brand_caln.RVS_CAMERA_DISP_BORDER_WIDTH {
*|      : is_calconst;
*|      : description = "Number of blank pixels in display to left of start of image. ";
*|      : units = "pixel";
*|      : transform = fixed.RVS_CAMERA_DISP_BORDER_WIDTH;
*| } 
*/ 
const char RVS_CAMERA_DISP_BORDER_WIDTH = 0x90;

/*
*| hmi_brand_caln.RVS_ON_HYSTERESIS {
*|      : is_calconst;
*|      : description = "RVS On Timer .";
*|      : units = "ms";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short RVS_ON_HYSTERESIS = 0x012C;

/*
*| hmi_brand_caln.RVS_OFF_HYSTERESIS {
*|      : is_calconst;
*|      : description = "RVS Off Timer.";
*|      : units = "sec";
*|      : type = fixed.UB0;
*| } 
*/ 
const char RVS_OFF_HYSTERESIS = 0x05;

/*
*| hmi_brand_caln.RVS_DISABLE_HYSTERESIS_SPEED {
*|      : is_calconst;
*|      : description = "RVS Off Speed.";
*|      : units = "kph";
*|      : type = fixed.UB0;
*| } 
*/ 
const char RVS_DISABLE_HYSTERESIS_SPEED = 0x08;

/*
*| hmi_brand_caln.ENABLE_APPLICATION_INTERIOR_LIGHTING {
*|      : is_calconst;
*|      : description = "Disables Interior Lighting Application on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_INTERIOR_LIGHTING = 0x00;

/*
*| hmi_brand_caln.ENABLE_APPLICATIONTRAY_INTERIOR_LIGHTING {
*|      : is_calconst;
*|      : description = "Shows interior Lighting Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_INTERIOR_LIGHTING = 0x00;

/*
*| hmi_brand_caln.ENABLE_APPLICATIONTRAY_REAR_CLIMATECONTROL {
*|      : is_calconst;
*|      : description = "Shows Rear Climate Control Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_REAR_CLIMATECONTROL = 0x00;

/*
*| hmi_brand_caln.ENABLE_APPLICATION_MIRRORLINK {
*|      : is_calconst;
*|      : description = "Disables MirrorLink Application on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_MIRRORLINK = 0x01;

/*
*| hmi_brand_caln.ENABLE_APPLICATIONTRAY_MIRRORLINK {
*|      : is_calconst;
*|      : description = "Shows MirrorLink Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_MIRRORLINK = 0x01;

/*
*| hmi_brand_caln.ENABLE_APPLICATION_DIGITAL_IPOD_OUT {
*|      : is_calconst;
*|      : description = "Disables Digital IPOD Out on Home Screen.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_DIGITAL_IPOD_OUT = 0x01;

/*
*| hmi_brand_caln.ENABLE_APPLICATION_NAVIGATION {
*|      : is_calconst;
*|      : description = "Disables Navigation Application on Home Screen.  The software will need to implement the following IF-THEN-ELSE statement:\
\
IF (SYSTEM_CONFIGURATION = 8\" or 10.2\" Navigation  OR ENABLE_APPLICATION_ONSTAR = Enabled) AND (ENABLE_APPLICATION_NAVIGATION = Enabled)\
\
THEN --->  DISPLAY NAVIGATION ICON\
\
ELSE ---->  DON'T DISPLAY NAVIGATION ICON\
\
\
\
\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_NAVIGATION = 0x00;

/*
*| hmi_brand_caln.ENABLE_APPLICATIONTRAY_NAVIGATION {
*|      : is_calconst;
*|      : description = "The four default applications in the tray are:\
Audio, Phone, Navigation, Climate Control\
If the navigation system is not available on the vehicle, the nav application is still shown but has no action when tapped. It provides direction information only.\
UPDATE:\
Item must be in all CALDS bundles and common to all HMI software sets.  \
*  However, the calibration only functions with 8\" systems and shall have the default described in this document.  \
*  The calibration will perform no action in the 4.2\" software sets and shall have default of 0 = DISABLE.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_NAVIGATION = 0x00;

/*
*| hmi_brand_caln.ENABLE_MIRRORLINK_NOTIFICATION {
*|      : is_calconst;
*|      : description = "Enables or disables the MirrorLink Notifications menu item";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MIRRORLINK_NOTIFICATION = 0x01;

/*
*| hmi_brand_caln.DEFAULT_AUDIO_TOUCH_FEEDBACK_SETTING {
*|      : is_calconst;
*|      : description = "The user can toggle Audio Touch Feedback on or off by tapping this list item. The Default is OFF\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DEFAULT_AUDIO_TOUCH_FEEDBACK_SETTING = 0x00;

/*
*| hmi_brand_caln.DEFAULT_MIRRORLINK_NOTIFICATION_SETTING {
*|      : is_calconst;
*|      : description = "The user can toggle MirrorLink Notifications on or off by tapping this list item. The Default is ON.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DEFAULT_MIRRORLINK_NOTIFICATION_SETTING = 0x01;

/*
*| hmi_brand_caln.CAL_HMI_BRAND_END {
*|      : is_calconst;
*|      : description = "END OF CAL BLOCK";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CAL_HMI_BRAND_END = 0x00;
