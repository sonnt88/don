/**********************************************************FileHeaderBegin******
 *
 * FILE:        eollib_dp.cpp
 *
 * CREATED:     2014-05-20
 *
 * AUTHOR:		Thomas Schmidt & Yuvaraj
 *
 * DESCRIPTION: -
 *
 * NOTES: -
 *
 * COPYRIGHT:  (c) 2013 Robert Bosch GmbH
 *
 **********************************************************FileHeaderEnd*******/

/*****************************************************************************
 *
 * search for this KEYWORDS to jump over long histories:
 *
 * CONSTANTS - TYPES - MACROS - VARIABLES - FUNCTIONS - PUBLIC - PRIVATE
 *
 ******************************************************************************/

/******************************************************************************
 * LOG:
 *
 * $Log: $
 ******************************************************************************/


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/

#include "OsalConf.h"

#include "trace_interface.h"

/* OSAL Device header */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"

/* for setting multi-user group for EOL Semaphore */ 
#include <dlfcn.h>
#include <grp.h>

/* Needed for Trace */
#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#define ET_TRACE_INFO_ON
#include "etrace_if.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_DEV_DIAGEOL
#include "trcGenProj/Header/eollib_dp.cpp.trc.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* EOL table and offset definitions */
#include "EOLLib.h"

#ifdef __cplusplus
}
#endif

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/

/* for persistent mount point checking */
#include <sys/stat.h>
#include <time.h>
#include "pdd.h"

#define FFD_EOL_FILE_FOLDER_PATH     PDD_FILE_DEFAULT_PATH_SECURE
#define FFD_MAX_PATH_NAME_SIZE 255
#define P3_FFD_MAX_MOUNT_TIME 5

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

/* Checking FFD Data Set compatibility */
#define EOLLIB_NEW_C3_CAL_FORM_ID 0x0411

/* Import Datapool Interfaces */
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_fc_diagnosis_if.h"

#endif

typedef struct sDiagEOLBlock
{
    tU8     u8TableId;
    tPCS8   ps8TableName;
    /*OSAL_tIODescriptor hFile;*/
    tenFDDDataSet enDataSet;
    int      hDev;
	tU8		Header[14]; 
    tU16    u16Length;
    tU8    pu8Data[U8_EOLLIB_MAX_BLOCK_LENGTH];
} tsDiagEOLBlock;

typedef struct
{
	tU8				u8ShMemInitiated;
	tsDiagEOLBlock	sDiagEOLBlocks[EOL_BLOCK_NBR];
}tsEOLLIBSharedMemory;

//ETG_TRACE_USR3_THR
//ETG_TRACE_COMP_THR
//ETG_TRACE_ERR_THR

#if 0
enum tenTrcTraceClass {
   TR_CLASS_DEV_DIAGEOL = (0x00E7)  // trace class
};
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/


/*defines for shared memory*/
#define EOLLIB_C_SHMEM_NAME                     "EOLLIB_SHME"

/*defines for semaphore - changed by yuv2cob for assigning group access rights to eco_osal*/
#define EOLLIB_ACCESS_RIGTHS_GROUP_NAME   "eco_osal"
#define EOLLIB_SEM_NAME                   "EOL_INIT"
#define EOLLIB_ACCESS_RIGTHS              (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP) /* 0660  defines access option for all Linux  objects via OSAL (MQ, Share Mem)  S_IWUSR | S_IRUSR | S_IRGRP| S_IWGRP | S_IROTH | S_IWOTH 0666 */

/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/

//#define OSAL_C_S32_IOCTRL_DIAGEOL_RESET_TARGET_DEFAULTS     0x101
//#define OSAL_C_S32_IOCTRL_DIAGEOL_RESET_DEFAULTS            0x103
#define OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC					  0x102

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/
#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS
typedef struct
{
	tU8		u8DPOffset;
	tU8		u8TableId;	
	tU16	u16EOLOffset;	
	tU8		u16EOLParamLength;
}tsDiag_EOL_SCC_Calib_Table;

//Contains SCC_EOL_MAX_PARAMETER_NBR entries
tsDiag_EOL_SCC_Calib_Table sDiagEOL_Table[] = 
{
	//Datapool Offset																		TableId						EOL Offset																EOL Parameter Length
	{CANEOL_DP_OFFSET_BUS_WAKEUP_DELAY_TIME,												EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_BUS_WAKEUP_DELAY_TIME,													1 },
	{CANEOL_DP_OFFSET_HS_BUS_OFF_DTC_FAILURE_CRITERIA_X,									EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HS_BUS_OFF_DTC_FAILURE_CRITERIA_X,										1 },
	{CANEOL_DP_OFFSET_HS_BUS_OFF_DTC_FAILURE_CRITERIA_Y,									EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HS_BUS_OFF_DTC_FAILURE_CRITERIA_Y,										1 },
	{CANEOL_DP_OFFSET_HSGMLAN_TX_MESSAGE_MASK_BYTE_1,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HSGMLAN_TX_MESSAGE_MASK_BYTE_1,											1 },
	{CANEOL_DP_OFFSET_HSGMLAN_TX_MESSAGE_MASK_BYTE_2,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HSGMLAN_TX_MESSAGE_MASK_BYTE_2,											1 },
	{CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_1,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_1,											1 },
	{CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_2,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_2,											1 },
	{CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_3,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_3,											1 },
	{CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_4,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_4,											1 },
	{CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_5,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_5,											1 },
	{CANEOL_DP_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_6,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_HSGMLAN_RX_MESSAGE_MASK_BYTE_6,											1 },
	{CANEOL_DP_OFFSET_LSGMLAN_RX_BYTE_DTC_TRIGGER,											EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_LSGMLAN_RX_BYTE_DTC_TRIGGER,												1 },
	{CANEOL_DP_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_MINIMUM_UPDATE_TIME,			EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_MINIMUM_UPDATE_TIME,				1 },
	{CANEOL_DP_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_PERIODIC_RATE,					EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_PERIODIC_RATE,						1 },
	{CANEOL_DP_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_DELAY_FOR_FIRST_PERIODIC_RATE,	EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_TX_ADASIS_MESSAGE_RAW_DATA_MULTIPLEXED_DELAY_FOR_FIRST_PERIODIC_RATE,		1 },
	{CANEOL_DP_OFFSET_TX_DTC_TRIGGERED_788_MINIMUM_UPDATE_TIME,								EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_TX_DTC_TRIGGERED_788_MINIMUM_UPDATE_TIME,									1 },
	{CANEOL_DP_OFFSET_TX_DTC_TRIGGERED_788_PERIODIC_RATE,									EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_TX_DTC_TRIGGERED_788_PERIODIC_RATE,										1 },
	{CANEOL_DP_OFFSET_TX_DTC_TRIGGERED_788_DELAY_FOR_FIRST_PERIODIC_RATE,					EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_TX_DTC_TRIGGERED_788_DELAY_FOR_FIRST_PERIODIC_RATE,						1 },
	{EOL_DP_OFFSET_SYSTEM_CONFIGURATION,													EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_SYSTEM_CONFIGURATION,														1 },
	{EOL_DP_OFFSET_MAIN_DISP_FAILSOFT,														EOLLIB_TABLE_ID_BRAND,		EOLLIB_OFFSET_MAIN_DISP_FAILSOFT,														2 },
	{CANEOL_DP_OFFSET_DTC_MASK_BYTE1,														EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_DTC_MASK_BYTE1,															1 },
	{CANEOL_DP_OFFSET_DTC_MASK_BYTE2,														EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_DTC_MASK_BYTE2,															1 },
	{CANEOL_DP_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE1,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE1,											1 },
	{CANEOL_DP_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE2,										EOLLIB_TABLE_ID_SYSTEM,		EOLLIB_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE2,											1 }
	//New DP Offset = Prev. Offset + Prev. param length
};

//Contains EOL_MAX_CAL_BLOCK_TRNSF_TO_SCC entries
tU8 szU8EOLBlock_Trnsf_to_SCC[] = 
{
	EOLLIB_TABLE_ID_SYSTEM,
	EOLLIB_TABLE_ID_BRAND
	//Add new EOL Block here if any calibration parameter from other block needs to be transfered.
};
#endif

/*****************************************************************
| static variable declaration
|----------------------------------------------------------------*/
static OSAL_tShMemHandle    vsEOLLibHandleSharedMemory = OSAL_ERROR;

/*****************************************************************
| function prototypes (scope: local)
|----------------------------------------------------------------*/

static tsEOLLIBSharedMemory* psEOLLIBLock(tVoid);
static tVoid vEOLLIBUnLock(tsEOLLIBSharedMemory* PtsShMemEOLLIB);
static tsDiagEOLBlock* psGetBlock(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8 u8TableId);

static tVoid vCreateDefaultHeader(tsDiagEOLBlock* psBlock, tU8 u8Defaults);
static tVoid vCreateDefaultBlock(tsDiagEOLBlock* psBlock, tU8 u8Defaults);
static tVoid vGetDefaultEOLBlockValues(tsDiagEOLBlock* psBlock);
static tVoid vInitEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB);
static tVoid vInitBlock(tsDiagEOLBlock* psEOLBlock,  tU8 u8TableId, tPCS8 ps8TableName, tenFDDDataSet enDataSet, tU16 u16Length);

static tS32 s32ReadBlockFromFFD(tsDiagEOLBlock* psBlock);
static tS32 s32WriteBlock(tsDiagEOLBlock* psBlock);

static tBool bCheckAndWaitForP3PartitionMounting(tVoid);
static tC8* pCheckIfPathIsMounted(tCString pCStrMountPath);

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

static tS32 s32PrepareAndSendDataToSCC(tsEOLLIBSharedMemory* VsShMemEOLLIB);

static tBool s32SyncDatapoolSCC(tsEOLLIBSharedMemory* VsShMemEOLLIB);
static tS32 s32PrepareSCCEOLData(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8* strEOLCalDataForSCC);
static tBool bCheckEOLBlockAndSendEOLDataToSCC(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8 u8TableId);

static tS32 s32ReadBlockFromDatapool(tsDiagEOLBlock* psBlock);
static tS32 s32WriteBlockToDatapool(tsDiagEOLBlock* psBlock);

static tVoid vReadEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB);
static tVoid vReadEOLBlocksDefaultValues(tsEOLLIBSharedMemory* VsShMemEOLLIB);
static tBool bIsFFDEOLBlockCompatibile(const tsDiagEOLBlock* psBlock);
static tS32 s32ReadEOLBlocksFromFFD(tsEOLLIBSharedMemory* VsShMemEOLLIB);

#else
static tVoid vSaveAllEOLBlocksInFFDFlash(tVoid);
static tS32 s32WriteBlockToFFD(tsDiagEOLBlock* psBlock);
static tVoid vRestoreEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB);
#endif

static tVoid DRV_DIAG_EOL_vTraceCommand(unsigned char* PubpData)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(PubpData);

//    vTraceMsg(0x0B01, TR_LEVEL_USER_4);

/*#if 0
	switch (PubpData[1])
	{
		case 1: //DEV_DIAGEOL_RESET_TARGET_DEFAULTS
			{
//				vTraceMsg(0x0B03, TR_LEVEL_USER_4);

				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_TUNER), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_AUDIO), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_EQUALIZATION), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_SYSTEM), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_LIGHTING), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BUTTON), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_COUNTRY), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BRAND), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_RVC), 0);
			}
			break;

		case 2: //DEV_DIAGEOL_RESET_DEFAULTS %i(EOL_DEFAULTS)
			{
//				vTraceMsg(0x0B04, TR_LEVEL_USER_4);

				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_TUNER), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_AUDIO), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_EQUALIZATION), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_SYSTEM), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_LIGHTING), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BUTTON), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_COUNTRY), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BRAND), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_RVC), PubpData[2]);
			}
			break;
	}
#endif*/

//    vTraceMsg(0x0B02, TR_LEVEL_USER_4);
}

static tVoid DRV_DIAG_EOL_vTraceRegChannel(tVoid)
{ 
	LaunchChannel  oTraceChannel;

	oTraceChannel.enTraceChannel = TR_TTFIS_DEV_DIAGEOL;
	oTraceChannel.p_Callback = (OSAL_tpfCallback)DRV_DIAG_EOL_vTraceCommand;
}

static tVoid vInitBlock(tsDiagEOLBlock* psEOLBlock,  tU8 u8TableId, tPCS8 ps8TableName, tenFDDDataSet enDataSet, tU16 u16Length)
{
    if (psEOLBlock != NULL)
    {
        psEOLBlock->u8TableId = u8TableId;
        psEOLBlock->ps8TableName = ps8TableName;
        psEOLBlock->u16Length = u16Length;
		psEOLBlock->hDev = OSAL_ERROR;
        //psEOLBlock->hFile = OSAL_ERROR;
        psEOLBlock->enDataSet = enDataSet;
    }
}

static tVoid vGetDefaultEOLBlockValues(tsDiagEOLBlock* psBlock)
{
   ETG_TRACE_USR3_THR(("---> vRestoreBlock"));

   /* Create a Default Block */
   vCreateDefaultHeader(psBlock, 0);
   vCreateDefaultBlock(psBlock, 0);

   ETG_TRACE_USR3_THR(("<--- vRestoreBlock"));
}

static tVoid vCreateDefaultHeader(tsDiagEOLBlock* psBlock, tU8 u8Defaults)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u8Defaults);

   //Checksum
   psBlock->Header[0] = 0;
   psBlock->Header[1] = 0;

   //ModuleID
   psBlock->Header[2] = 0;
   psBlock->Header[3] = psBlock->u8TableId;

   //HFI
   psBlock->Header[4] = 0x22;
   psBlock->Header[5] = 0;

   //PartNumber/AlphaCode
   psBlock->Header[6] = 0; //MSB
   psBlock->Header[7] = 0;
   psBlock->Header[8] = 0;
   psBlock->Header[9] = 0; //LSB
   psBlock->Header[10] = 'A';
   psBlock->Header[11] = 'A';

   //CalFormID
   psBlock->Header[12] = 0;
   psBlock->Header[13] = 0;   
}

static tVoid vCreateDefaultBlock(tsDiagEOLBlock* psBlock, tU8 u8Defaults)
{
	if(OSAL_NULL == psBlock)
	{
		ETG_TRACE_ERR_THR(("--- vCreateDefaultBlock: Null EOL Block pointer ARGUMENT Passed!!!"));
		return;
	}

	ETG_TRACE_USR3_THR(("---> vCreateDefaultBlock"));
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u8Defaults);

	tU32 i = 0;
	const EOLLib_Entry* pEntries = NULL;
	void* pModuleHandle = NULL;

	//OSAL_pvMemorySet(psBlock->pu8Data, 0, psBlock->u16Length);
	memset(psBlock->pu8Data,0x00,U8_EOLLIB_MAX_BLOCK_LENGTH);

	OSAL_tIODescriptor hReg = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL", OSAL_EN_READONLY);
	if(hReg != OSAL_ERROR)
	{
		OSAL_trIOCtrlRegistry   rReg;
		tU8                     szModuleName[256];

		rReg.pcos8Name = (tS8*) "SHARED_PATH";//lint !e1773: Attempt to cast away const (or volatile)
		rReg.u32Size   = sizeof(szModuleName);
		rReg.ps8Value  = (tPU8) szModuleName;
		rReg.s32Type   = OSAL_C_S32_VALUE_STRING;

		if(OSAL_s32IOControl(hReg, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&rReg) != OSAL_ERROR)
		{
			OSAL_szStringConcat(szModuleName, "libshared_eoldata_so.so");
			pModuleHandle = dlopen((const char *)szModuleName, RTLD_NOW);

			if (pModuleHandle == NULL)
			{
				ETG_TRACE_ERR_THR(("!!! vCreateDefaultBlock => ERROR: dlopen()='%s'",dlerror()));
			}
		}
		OSAL_s32IOClose(hReg);
	}

	if (pModuleHandle != NULL)
	{
		switch (psBlock->u8TableId)
		{
			case EOLLIB_TABLE_ID_SYSTEM:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arSystem"));
				break;
			}

			case EOLLIB_TABLE_ID_DISPLAY_INTERFACE:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arDisplay"));
				break;
			}

			case EOLLIB_TABLE_ID_BLUETOOTH:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arBluetooth"));
				break;
			}

			case EOLLIB_TABLE_ID_NAVIGATION_SYSTEM:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arNavSystem"));
				break;
			}

			case EOLLIB_TABLE_ID_NAVIGATION_ICON:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arNavIcon"));
				break;
			}

			case EOLLIB_TABLE_ID_BRAND:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arBrand"));
				break;
			}

			case EOLLIB_TABLE_ID_COUNTRY:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arCountry"));
				break;
			}

			case EOLLIB_TABLE_ID_SPEECH_RECOGNITION:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arSpeechRec"));
				break;
			}

			case EOLLIB_TABLE_ID_HAND_FREE_TUNING:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arHfTuning"));
				break;
			}

			case EOLLIB_TABLE_ID_REAR_VISION_CAMERA:
			{
				pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arRVC"));
				break;
			}

			default:
			{
				break;
			}
		} //switch (psBlock->u8TableId)

		if (pEntries == NULL)
		{
			ETG_TRACE_ERR_THR(("!!! vCreateDefaultBlock => ERROR: dlsym()='%s'",dlerror()));
		}

		//dlclose(pModuleHandle);
		//pModuleHandle = NULL;
	} //if (pModuleHandle != NULL)

	if (pEntries != NULL)
	{
		for (i = 0; pEntries[i].address != NULL; i++)
		{

			memcpy(psBlock->pu8Data + pEntries[i].offset, 
				pEntries[i].address, 
				pEntries[i].size);
		}
	}

	//tS32 s32ReturnValue = s32WriteBlock(psBlock);	
	//
	//if(OSAL_E_NOERROR != s32ReturnValue)
	//{
	//	ETG_TRACE_ERR_THR(("!!! vCreateDefaultBlock => ERROR: s32WriteBlock(%u)", ETG_ENUM(EOLLIB_TABLE_ID, psBlock->u8TableId)));
	//}

	ETG_TRACE_USR3_THR(("<--- vCreateDefaultBlock"));
}

static tVoid vInitEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
	if (VsShMemEOLLIB != NULL)
	{
		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[0]), EOLLIB_TABLE_ID_SYSTEM,          
			(tPCS8) EOLLIB_TABLE_NAME_SYSTEM, EN_FFD_DATA_SET_EOL_SYSTEM,
			EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[1]), EOLLIB_TABLE_ID_DISPLAY_INTERFACE,   
			(tPCS8) EOLLIB_TABLE_NAME_DISPLAY_INTERFACE, EN_FFD_DATA_SET_EOL_DISPLAY,
			EOLLIB_OFFSET_CAL_HMI_F_B_END);

		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[2]), EOLLIB_TABLE_ID_BLUETOOTH,         
			(tPCS8) EOLLIB_TABLE_NAME_BLUETOOTH, EN_FFD_DATA_SET_EOL_BLUETOOTH,
			EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END);

		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[3]), EOLLIB_TABLE_ID_NAVIGATION_SYSTEM,       
			(tPCS8) EOLLIB_TABLE_NAME_NAVIGATION_SYSTEM, EN_FFD_DATA_SET_EOL_NAV_SYSTEM,
			EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END);

		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[4]), EOLLIB_TABLE_ID_NAVIGATION_ICON,         
			(tPCS8) EOLLIB_TABLE_NAME_NAVIGATION_ICON, EN_FFD_DATA_SET_EOL_NAV_ICON,
			EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END);

		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[5]), EOLLIB_TABLE_ID_BRAND,        
			(tPCS8) EOLLIB_TABLE_NAME_BRAND, EN_FFD_DATA_SET_EOL_BRAND,
			EOLLIB_OFFSET_CAL_HMI_BRAND_END);

		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[6]), EOLLIB_TABLE_ID_COUNTRY,          
			(tPCS8) EOLLIB_TABLE_NAME_COUNTRY, EN_FFD_DATA_SET_EOL_COUNTRY,
			EOLLIB_OFFSET_CAL_HMI_COUNTRY_END);

		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[7]), EOLLIB_TABLE_ID_SPEECH_RECOGNITION,            
			(tPCS8) EOLLIB_TABLE_NAME_SPEECH_RECOGNITION, EN_FFD_DATA_SET_EOL_SPEECH_REC,
			EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END);

		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[8]), EOLLIB_TABLE_ID_HAND_FREE_TUNING,            
			(tPCS8) EOLLIB_TABLE_NAME_HAND_FREE_TUNING, EN_FFD_DATA_SET_EOL_HF_TUNING,
			EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END);

		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[9]), EOLLIB_TABLE_ID_REAR_VISION_CAMERA,            
			(tPCS8) EOLLIB_TABLE_NAME_REAR_VISION_CAMERA, EN_FFD_DATA_SET_EOL_RVC,
			EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END);		
	}
}


//Start: [yuv2cob - 25.07.2014] EOLLIB Datapool Migration
static tS32 s32WriteBlock(tsDiagEOLBlock* psBlock)
{
	tS32 s32ReturnValue = 0;

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS
	s32ReturnValue = s32WriteBlockToDatapool(psBlock);
#else
	s32ReturnValue= s32WriteBlockToFFD(psBlock);
	vSaveAllEOLBlocksInFFDFlash();
#endif

	return s32ReturnValue;
}

static tS32 s32ReadBlockFromFFD(tsDiagEOLBlock* psBlock)
{
	if(OSAL_NULL == psBlock)
	{
		ETG_TRACE_ERR_THR(("--- s32ReadBlockFromFFD: Null EOL Block pointer ARGUMENT Passed!!!"));
		return OSAL_E_INVALIDVALUE;
	}

	ETG_TRACE_USR3_THR(("---> s32ReadBlockFromFFD"));

	tS32 s32ReturnValue = OSAL_E_NOERROR;
	tS32 s32BytesRead = 0;
	psBlock->hDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD, OSAL_EN_READONLY);

	if (psBlock->hDev == OSAL_ERROR)
	{
		// Open FFD error 
		ETG_TRACE_ERR_THR(("--- s32ReadBlockFromFFD: OSAL_IOOpen on dev_ffd FAILED!!!"));
		s32ReturnValue = OSAL_E_IOERROR;
	}
	else
	{
		OSAL_trFFDDeviceInfo VtsFFDDevInfo;
		VtsFFDDevInfo.u8DataSet = psBlock->enDataSet;
		VtsFFDDevInfo.pvArg = psBlock->Header;
		
		if(OSAL_s32IORead(psBlock->hDev, (tPS8)&VtsFFDDevInfo, sizeof(psBlock->Header))!=sizeof(psBlock->Header))
		{
			// Header Read Error
			ETG_TRACE_ERR_THR(("--- s32ReadBlockFromFFD: OSAL_s32IORead on dev_ffd for Header Data FAILED!!!"));

			OSAL_s32IOClose(psBlock->hDev);
			s32ReturnValue = OSAL_E_IOERROR;
		}
		else
		{
			VtsFFDDevInfo.u8DataSet = psBlock->enDataSet|0x80;
			VtsFFDDevInfo.pvArg = (void*)psBlock->pu8Data;
			s32BytesRead=OSAL_s32IORead(psBlock->hDev,(tPS8)&VtsFFDDevInfo,psBlock->u16Length);
			if (s32BytesRead != psBlock->u16Length)
			{
				// Block Read Error 
				ETG_TRACE_ERR_THR(("--- s32ReadBlockFromFFD: OSAL_s32IORead on dev_ffd for Block Data FAILED!!!"));

				OSAL_s32IOClose(psBlock->hDev);
				s32ReturnValue = OSAL_E_IOERROR;
			}
			else
			{
				//Reading successful 
				OSAL_s32IOClose(psBlock->hDev);
			}          
		}
	}

	ETG_TRACE_COMP_THR(("--- s32ReadBlockFromFFD: Table:%u Len:%u Got:%u Data:%*x",
		ETG_ENUM(EOLLIB_TABLE_ID, psBlock->u8TableId), 
		psBlock->u16Length, 
		s32BytesRead,
		ETG_LIST_LEN((s32BytesRead > 200)? 200: s32BytesRead), 
		ETG_LIST_PTR_T8(psBlock->pu8Data) ));

	ETG_TRACE_USR3_THR(("<--- s32ReadBlockFromFFD"));
	return s32ReturnValue;
}

static tBool bCheckAndWaitForP3PartitionMounting(tVoid)
{
	struct timespec vTimer={0,0};
	tC8*  pC8Pos=NULL;
	tBool bRetVal = FALSE;

	do
	{
		pC8Pos=pCheckIfPathIsMounted(FFD_EOL_FILE_FOLDER_PATH);
		if(pC8Pos==NULL)
		{ 
			if(vTimer.tv_sec == 0)
			{
				vWritePrintfErrmem("EOLLib: Waiting for Persistent partition to mount");
			}
			/*not mounted get run time*/
			clock_gettime(CLOCK_MONOTONIC, &vTimer);
			
			/*trace out time*/ 
			//PDD_TRACE(PDD_FILE_TIME,&vTimer.tv_sec,sizeof(time_t));
			//PDD_TRACE(PDD_FILE_TIME,&P3_FFD_MAX_MOUNT_TIME,sizeof(time_t));

			/*check runtime*/
			if(vTimer.tv_sec < (time_t)P3_FFD_MAX_MOUNT_TIME)
			{ /*if not time reached sleep 200 ms*/
				usleep(200000);
			}
		}
	}while((pC8Pos==NULL)&&(vTimer.tv_sec < (time_t)P3_FFD_MAX_MOUNT_TIME));
	
	if(pC8Pos != NULL)
	{
		bRetVal = TRUE;
	}

	return bRetVal;
}

static tC8* pCheckIfPathIsMounted(tCString pCStrMountPath)
{
  if(pCStrMountPath == OSAL_NULL)
  {
	  return OSAL_NULL;
  }

  tC8* pC8PosMountPoint=OSAL_NULL;
  tC8* pC8Pos=OSAL_NULL;
  tC8  pStrPathName[FFD_MAX_PATH_NAME_SIZE];
  tS16 tS16Len=0;
  struct stat s1,s2;

  tS16Len=strlen(pCStrMountPath);
  /*if path to long or not a root path*/
  if((tS16Len+1>=(tS16)FFD_MAX_PATH_NAME_SIZE)||(tS16Len<1)||(pCStrMountPath[0]!='/'))
  {/*error path unvalid*/
    //tS32 s32Result=PDD_ERROR_FILE_PATH_INVALID; 	
	//PDD_TRACE(PDD_ERROR,&s32Result,sizeof(tS32));
  }
  else
  { /*copy path*/
    strcpy(pStrPathName, pCStrMountPath);
    // add trailing slash if not there.
    if(pStrPathName[tS16Len-1]!='/')
    {
	  pStrPathName[tS16Len++]='/';
	  pStrPathName[tS16Len]=0;
    }
	/*do until valid directory*/
	tBool bDirFound=FALSE;
	pC8Pos=strrchr(pStrPathName,'/');
	do
	{ /* get status of dir*/
	  //PDD_TRACE(PDD_FILE_TRACE_PATH,pStrPathName,strlen((tU8*)pStrPathName));

      if(stat(pStrPathName,&s1)>=0)
	  { /* check is this a directory*/
	    if(S_ISDIR(s1.st_mode))
	    { /* is a directory*/
		  bDirFound=TRUE;		
		  tS16Len=strlen(pStrPathName); /*save the last length*/
		}		
	  }
	  /* cut off '/'*/
	  if(pC8Pos!=NULL)
	    pC8Pos[0]=0;
	  /* get last '/'*/
	  pC8Pos=strrchr(pStrPathName,'/');
	  if(pC8Pos!=NULL)
	    pC8Pos[1]=0;		
	}while((bDirFound==FALSE)&&(pC8Pos!=NULL)&&(strlen(pStrPathName)>0));
	/*check if valid dir found*/
    if(bDirFound==FALSE)
	{
	  /*error path unvalid*/
	  //tS32 s32Result=PDD_ERROR_FILE_PATH_INVALID; 
	  //PDD_TRACE(PDD_ERROR,&s32Result,sizeof(tS32));
	}
	else
	{ /* do until different file system*/
      do
	  { /* trace out path*/
        //PDD_TRACE(PDD_FILE_TRACE_PATH,pStrPathName,strlen((tU8*)pStrPathName)); 

		if((stat(pStrPathName,&s2)>=0) && (pC8Pos != NULL))
	    {
	      if( s1.st_dev == s2.st_dev )
		  { /* is same filesystem , not the mountpoint*/
            /* save s1*/
            memmove(&s1.st_dev,&s2.st_dev,sizeof(s1.st_dev));
			tS16Len=strlen(pStrPathName); /*save the last position*/
            /* cut off '/'*/
	        pC8Pos[0]=0;
			/* get last '/'*/
	        pC8Pos=strrchr(pStrPathName,'/');
			if(pC8Pos!=NULL)
	          pC8Pos[1]=0;					 
		  }
		  else
		  {/*is a different filesystem. mountpoint found*/
			pC8PosMountPoint = const_cast<tC8*>(pCStrMountPath + tS16Len);
			//PDD_TRACE(PDD_FILE_FIND_MOUNT_POINT,pCStrMountPath,tS16Len); 
		  } 
		}
		else
		{/*error path unvalid*/
          pC8Pos=NULL;
		  //tS32 s32Result=PDD_ERROR_FILE_PATH_INVALID; 
	      //PDD_TRACE(PDD_ERROR,&s32Result,sizeof(tS32));
		}
	  }while((pC8PosMountPoint==NULL)&&(pC8Pos!=NULL)&&(strlen(pStrPathName)>0));
	}
  }
  return((char*)pC8PosMountPoint);
}

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

static tVoid vReadEOLBlocksDefaultValues(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
	if (VsShMemEOLLIB != NULL)
	{
		for (tU32 i=0; i<EOL_BLOCK_NBR; i++)
		{
			vGetDefaultEOLBlockValues(&(VsShMemEOLLIB->sDiagEOLBlocks[i]));
		}
	}
}

static tS32 s32ReadEOLBlocksFromFFD(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
	tS32 s32retVal = OSAL_E_INVALIDVALUE;
	
	if (VsShMemEOLLIB != NULL)
	{
		s32retVal = OSAL_E_NOERROR;

		for (tU32 i=0; i<EOL_BLOCK_NBR; i++)
		{
			if(OSAL_E_NOERROR != s32ReadBlockFromFFD(&(VsShMemEOLLIB->sDiagEOLBlocks[i])))
			{
				s32retVal = OSAL_E_IOERROR;
				vWritePrintfErrmem("EOLLib: EOL Block is not readable from FFD");
				break;
			}
			else //FFD Read success, check if the data set is compatible with current version
			{
				if(bIsFFDEOLBlockCompatibile(&(VsShMemEOLLIB->sDiagEOLBlocks[i])) == FALSE)
				{
					s32retVal = OSAL_E_INVALIDVALUE;
					break;
				}
			}
		}
	}

	return s32retVal;
}

static tS32 s32WriteBlockToDatapool(tsDiagEOLBlock* psBlock)
{
   ETG_TRACE_USR3_THR(("---> s32WriteBlockToDatapool"));

    tS32 s32ReturnValue = 0;
	tS32 s32HdrReturnValue = 0;
	tU8 u8TableId = 0;

	if(OSAL_NULL == psBlock)
	{
		ETG_TRACE_ERR_THR(("--- s32WriteBlockToDatapool: null pointer passed as parameter!!!"));
		s32ReturnValue = OSAL_E_INVALIDVALUE;
	}
	else
	{
		u8TableId = psBlock->u8TableId;

		tsDiagEOLBlockHeader tCalBlockHeader;
		
		tCalBlockHeader.u8TableId = u8TableId;
		tCalBlockHeader.u16Length = psBlock->u16Length;
		memcpy(tCalBlockHeader.Header, 
			psBlock->Header,
			sizeof(tCalBlockHeader.Header));

		switch (u8TableId)
		{
			case EOLLIB_TABLE_ID_SYSTEM:
			{
				dp_tclCalibModule02_DataSystemHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);

				dp_tclCalibModule02_DataSystem dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_DISPLAY_INTERFACE:
			{
				dp_tclCalibModule03_DataDisplayHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);

				dp_tclCalibModule03_DataDisplay dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_BLUETOOTH:
			{
				dp_tclCalibModule04_DataBluetoothHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);

				dp_tclCalibModule04_DataBluetooth dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_NAVIGATION_SYSTEM:
			{
				dp_tclCalibModule05_DataNavigationHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);

				dp_tclCalibModule05_DataNavigation dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_NAVIGATION_ICON:
			{
				dp_tclCalibModule06_DataNav_IconHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);

				dp_tclCalibModule06_DataNav_Icon dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_BRAND:
			{
				dp_tclCalibModule07_DataBrandHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);

				dp_tclCalibModule07_DataBrand dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_COUNTRY:
			{
				dp_tclCalibModule08_DataCountryHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);
	
				dp_tclCalibModule08_DataCountry dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_SPEECH_RECOGNITION:
			{
				dp_tclCalibModule09_DataSpeech_RecHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);

				dp_tclCalibModule09_DataSpeech_Rec dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_HAND_FREE_TUNING:
			{
				dp_tclCalibModule0a_DataHF_TuningHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);

				dp_tclCalibModule0a_DataHF_Tuning dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_REAR_VISION_CAMERA:
			{
				dp_tclCalibModule0b_DataRVCHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32SetData(tCalBlockHeader);

				dp_tclCalibModule0b_DataRVC dpCalModule;
				s32ReturnValue = dpCalModule.s32SetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			default:
			{
			   s32ReturnValue = OSAL_E_INVALIDVALUE;
			   break;
			}
		} //switch (psBlock->u8TableId)
	}	

	if( (0 > s32ReturnValue) || (0 > s32HdrReturnValue) )
	{
		ETG_TRACE_ERR_THR(("!!! s32WriteBlockToDatapool => ERROR: (%u), RetVal=%d, RetHdrVal=%d",
			ETG_ENUM(EOLLIB_TABLE_ID, u8TableId), s32ReturnValue, s32HdrReturnValue));
		s32ReturnValue = OSAL_E_IOERROR;
	}
	else if(OSAL_E_INVALIDVALUE == s32ReturnValue)
	{
		ETG_TRACE_ERR_THR(("--- s32WriteBlockToDatapool Invalid Block pointer or Table ID passed!!!"));
	}
	else
	{
		ETG_TRACE_USR2_THR(("!!! s32WriteBlockToDatapool status: (%u), RetVal=%d, RetHdrVal=%d",
			ETG_ENUM(EOLLIB_TABLE_ID, u8TableId), s32ReturnValue, s32HdrReturnValue));		
		s32ReturnValue = OSAL_E_NOERROR;
	}

	ETG_TRACE_USR3_THR(("<--- s32WriteBlockToDatapool"));
	return s32ReturnValue;
}

static tS32 s32ReadBlockFromDatapool(tsDiagEOLBlock* psBlock)
{
    ETG_TRACE_USR3_THR(("---> s32ReadBlockFromDatapool"));

    tS32 s32ReturnValue = OSAL_E_NOERROR;
	tS32 s32HdrReturnValue = OSAL_E_NOERROR;
	tU8 u8TableID = 0;

	if(OSAL_NULL == psBlock)
	{
		ETG_TRACE_ERR_THR(("--- s32ReadBlockFromDatapool: null pointer passed as parameter!!!"));
		s32ReturnValue = OSAL_E_INVALIDVALUE;
	}
	else
	{	
		u8TableID = psBlock->u8TableId;
		tsDiagEOLBlockHeader tCalBlockHeader = {0, {0}, 0};
		
		switch (u8TableID)
		{
			case EOLLIB_TABLE_ID_SYSTEM:
			{
				dp_tclCalibModule02_DataSystemHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);

				dp_tclCalibModule02_DataSystem dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_DISPLAY_INTERFACE:
			{
				dp_tclCalibModule03_DataDisplayHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);

				dp_tclCalibModule03_DataDisplay dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_BLUETOOTH:
			{
				dp_tclCalibModule04_DataBluetoothHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);

				dp_tclCalibModule04_DataBluetooth dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_NAVIGATION_SYSTEM:
			{
				dp_tclCalibModule05_DataNavigationHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);

				dp_tclCalibModule05_DataNavigation dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_NAVIGATION_ICON:
			{
				dp_tclCalibModule06_DataNav_IconHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);

				dp_tclCalibModule06_DataNav_Icon dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_BRAND:
			{
				dp_tclCalibModule07_DataBrandHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);

				dp_tclCalibModule07_DataBrand dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_COUNTRY:
			{
				dp_tclCalibModule08_DataCountryHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);
	
				dp_tclCalibModule08_DataCountry dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_SPEECH_RECOGNITION:
			{
				dp_tclCalibModule09_DataSpeech_RecHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);

				dp_tclCalibModule09_DataSpeech_Rec dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_HAND_FREE_TUNING:
			{
				dp_tclCalibModule0a_DataHF_TuningHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);

				dp_tclCalibModule0a_DataHF_Tuning dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			case EOLLIB_TABLE_ID_REAR_VISION_CAMERA:
			{
				dp_tclCalibModule0b_DataRVCHeader dpCalModuleHeader;
				s32HdrReturnValue = dpCalModuleHeader.s32GetData(tCalBlockHeader);

				dp_tclCalibModule0b_DataRVC dpCalModule;
				s32ReturnValue = dpCalModule.s32GetData(&psBlock->pu8Data[0], psBlock->u16Length);
				break;
			}

			default:
			{
			   s32ReturnValue = OSAL_E_INVALIDVALUE;
			   s32HdrReturnValue = OSAL_E_INVALIDVALUE;
			   break;
			}
		} //switch (u8TableID)

		memcpy(psBlock->Header, tCalBlockHeader.Header, 
			sizeof(psBlock->Header));

		
		//Testing
		/*tU8 trcMsg[70] = "echo EOLLib: read Data:-  ";		
		printTraceDMsg(trcMsg, tCalBlockHeader.u8TableId);
		printTraceDMsg(trcMsg, psBlock->pu8Data[0]);*/
		//Testing
		
		if(0x00 == tCalBlockHeader.u8TableId && psBlock->pu8Data[0] == 0x00) //This can only happen during software flash in Virgin/Brand New device
		{
			vWritePrintfErrmem("EOLLib: Datapool read from RAW_NOR returned Default Values");
			ETG_TRACE_ERR_THR(("--- s32ReadBlockFromDatapool: Virgin Flash! DP=%u not in RAW_NOR!", ETG_ENUM(EOLLIB_TABLE_ID, u8TableID)));			
			s32ReturnValue = OSAL_E_INVALIDVALUE;
		}
		else if( (0 > s32ReturnValue) || (0 > s32HdrReturnValue) )
		{
			vWritePrintfErrmem("EOLLib: Datapool read from RAW_NOR Failed");
			ETG_TRACE_ERR_THR(("--- s32ReadBlockFromDatapool: Datapool read of EOL Module FAILED!!! Retval = %d, RetHdrVal = %d", s32ReturnValue, s32HdrReturnValue));
			s32ReturnValue = OSAL_E_IOERROR;
		}
		else
		{
			s32ReturnValue = OSAL_E_NOERROR;
		}
	}

	ETG_TRACE_COMP_THR(("<--- s32ReadBlockFromDatapool(): Ret value = %d", s32ReturnValue));
    return s32ReturnValue;
}

static tS32 s32ReadEOLBlocksFromDatapool(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
	tS32 s32retVal = OSAL_E_INVALIDVALUE;

	if (VsShMemEOLLIB != NULL)
	{
		s32retVal = OSAL_E_NOERROR;

		for (tU32 i=0;i<EOL_BLOCK_NBR;i++)
		{
			if (OSAL_E_NOERROR != s32ReadBlockFromDatapool(&(VsShMemEOLLIB->sDiagEOLBlocks[i])))
			{
				s32retVal = OSAL_E_IOERROR;
				break;
			}
		}
	}	
	return s32retVal;
}

static tBool bIsFFDEOLBlockCompatibile(const tsDiagEOLBlock* psBlock)
{
	tBool bRetVal = FALSE;

	if(psBlock != NULL)
	{
		bRetVal = TRUE;
		EOLLib_CalDsHeader tEOLDataSetHeader;
		memcpy(&tEOLDataSetHeader, psBlock->Header, sizeof(psBlock->Header));

		if(	(tEOLDataSetHeader.Cal_Form_ID != (tU16)EOLLIB_NEW_C3_CAL_FORM_ID) ||
			(psBlock->pu8Data[0] == 0x00) //EOLLIB_OFFSET_SWMI_CALIBRATION_DATA_XXXX_CAL == 0x00?
			)
		{
			vWritePrintfErrmem("EOLLib: FFD EOL Block is not compatible with current version. CalFormId=0x%x, CAL_Offset=%d", tEOLDataSetHeader.Cal_Form_ID, psBlock->pu8Data[0]);
			bRetVal = FALSE;
		}
	}

	return bRetVal;
}

static tVoid vReadEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
	if (VsShMemEOLLIB != NULL)
	{
		if (OSAL_E_NOERROR != s32ReadEOLBlocksFromDatapool(VsShMemEOLLIB))
		{
			vWritePrintfErrmem("EOLLib: Reading EOL blocks from Datapool failed, try reading EOL blocks from FFD");

			if(bCheckAndWaitForP3PartitionMounting())
			{
				if(OSAL_E_NOERROR != s32ReadEOLBlocksFromFFD(VsShMemEOLLIB))
				{
					vWritePrintfErrmem("EOLLib: Reading EOL blocks from FFD failed, using default values for all EOL blocks");
					vReadEOLBlocksDefaultValues(VsShMemEOLLIB);
				}
				else
				{
					vWritePrintfErrmem("EOLLib: Reading EOL blocks from FFD is successful for all EOL blocks");
					//vInvalidateEOLBlocksInFFD(VsShMemEOLLIB);
				}

				//vSaveEOLValuesInDatapool(VsShMemEOLLIB); //During Early Startup, DP Write to RAW_NOR is not possible (Virgin Startup Scenario)
			}
			else
			{
				vWritePrintfErrmem("EOLLib: P3 partition mount failed, using default values for all EOL blocks");
				vReadEOLBlocksDefaultValues(VsShMemEOLLIB);
			}
		}
	}
}

//End: [yuv2cob - 25.07.2014] EOLLIB Datapool Migration

//Start: [yuv2cob - 26.08.2014] EOLLIB SCC implementation
static tS32 s32PrepareSCCEOLData(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8* strEOLCalDataForSCC )
{
	tS32 s32ReturnValue = OSAL_E_NOERROR;
	tU16 u16nbytes = 0, u16EOLOffset = 0;	
	tsDiagEOLBlock* psBlock = OSAL_NULL;

	if( (VsShMemEOLLIB == OSAL_NULL) || (strEOLCalDataForSCC == OSAL_NULL) )
	{
		ETG_TRACE_ERR_THR(("--- s32PrepareSCCEOLData: null pointer passed as parameter!!!"));
		s32ReturnValue = OSAL_E_INVALIDVALUE;
	}
	else
	{		
		psBlock = &(VsShMemEOLLIB->sDiagEOLBlocks[0]);

		for(tU16 i = 0; i < SCC_EOL_MAX_PARAMETER_NBR; i++)
		{

			if( psBlock->u8TableId != sDiagEOL_Table[i].u8TableId )
			{
				psBlock = psGetBlock(VsShMemEOLLIB, sDiagEOL_Table[i].u8TableId);
				
				if(psBlock == OSAL_NULL) //This should never happen
				{
					ETG_TRACE_ERR_THR(("--- s32PrepareSCCEOLData: ERRROR psGetBlock(%d) returned NULL, EOL data to SCC might be invalid", sDiagEOL_Table[i].u8TableId));
					vWritePrintfErrmem("EOLLib: Get EOL Block for table=%d returned NULL, EOL data to SCC might be invalid", sDiagEOL_Table[i].u8TableId);
					s32ReturnValue = OSAL_E_INVALIDVALUE;
					break;
				}
			}

			u16EOLOffset = sDiagEOL_Table[i].u16EOLOffset;
			u16nbytes = sDiagEOL_Table[i].u16EOLParamLength;

			if ((u16EOLOffset + u16nbytes) > psBlock->u16Length)
			{
				ETG_TRACE_ERR_THR(("--- s32PrepareSCCEOLData: INVALID Offset=%d or byte length=%d - read try from table(Id,Len)=(%d,%d)",
					u16EOLOffset, u16nbytes, psBlock->u8TableId, psBlock->u16Length));
				
				vWritePrintfErrmem("EOLLib: s32PrepareSCCEOLData - INVALID Offset=%d or byte length=%d - read try from table(Id,Len)=(%d,%d)",
					u16EOLOffset, u16nbytes, psBlock->u8TableId, psBlock->u16Length);
				s32ReturnValue = OSAL_E_INVALIDVALUE;
			}
			else
			{
				memcpy(strEOLCalDataForSCC + sDiagEOL_Table[i].u8DPOffset,
					psBlock->pu8Data + u16EOLOffset,
					u16nbytes);
			}
		}
	}
	return s32ReturnValue;
}

static tS32 s32PrepareAndSendDataToSCC(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
	ETG_TRACE_USR3_THR(("---> s32PrepareAndSendDataToSCC"));

	tS32 s32ReturnValue = OSAL_E_NOERROR;

	if(VsShMemEOLLIB == OSAL_NULL)
	{
		ETG_TRACE_ERR_THR(("--- s32PrepareAndSendDataToSCC: null pointer passed as parameter!!!"));
		s32ReturnValue = OSAL_E_INVALIDVALUE;
	}
	else
	{
		tU8 u8EOLCalDataForSCC[EOL_MAX_DATA_LENGTH + 1] = {0};
		s32ReturnValue = s32PrepareSCCEOLData(VsShMemEOLLIB, u8EOLCalDataForSCC);

		if(s32ReturnValue == OSAL_E_NOERROR)
		{
			ETG_TRACE_USR1_THR(("s32PrepareAndSendDataToSCC: DP Write! Len=%d, Data=%*x", EOL_MAX_DATA_LENGTH, ETG_LIST_LEN(EOL_MAX_DATA_LENGTH), 
																						   ETG_LIST_PTR_T8((tPU8)(&u8EOLCalDataForSCC[0])) ));
			dp_tclCanCalib_DataCANEOL dpCalDataForSCC;
			s32ReturnValue = dpCalDataForSCC.s32SetData(u8EOLCalDataForSCC, EOL_MAX_DATA_LENGTH);
		}
	}

	ETG_TRACE_USR3_THR(("<--- s32PrepareAndSendDataToSCC"));
	return s32ReturnValue;
}

//---------------------------------------------------------------------------------------------------------------------------------------
static tBool s32SyncDatapoolSCC(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
	tU8 u8InSync = TRUE;
	tS32 s32RetVal = OSAL_E_NOERROR;
	tU8 u8EOLCalSccImxCopy[EOL_MAX_DATA_LENGTH + 1] = {0};
	s32RetVal = s32PrepareSCCEOLData(VsShMemEOLLIB, u8EOLCalSccImxCopy);
	if(s32RetVal == OSAL_E_NOERROR)
	{
		tU8 u8EOLCalSccActual[EOL_MAX_DATA_LENGTH] = {0};
		dp_tclCanCalib_DataCANEOL dpSccCalDataImx;

		s32RetVal = dpSccCalDataImx.s32GetData(&u8EOLCalSccActual[0], EOL_MAX_DATA_LENGTH);
		if(0 <= s32RetVal)
		{
			tU8 i;
			for(i = 0; i < EOL_MAX_DATA_LENGTH; i++)
			{
				if(u8EOLCalSccImxCopy[i] != u8EOLCalSccActual[i])
				{
					u8InSync = FALSE;
					break;
				}
			}
		}
		else
		{
		    ETG_TRACE_FATAL_THR(("--- s32SyncDatapoolSCC: Error occured while reading SCC datapool, Error Value=%d", s32RetVal));
			vWritePrintfErrmem("EOLLib: Error occured while reading SCC datapool, Error Value=%d", s32RetVal);										
		}

		if(FALSE == u8InSync)
		{
			s32RetVal = dpSccCalDataImx.s32SetData(u8EOLCalSccImxCopy, EOL_MAX_DATA_LENGTH);

			if(0 != s32RetVal)
			{
				vWritePrintfErrmem("EOLLib: Error occured while synchronizing SCC datapool, Error Value=%d", s32RetVal);										
				ETG_TRACE_FATAL_THR(("--- s32SyncDatapoolSCC: Error occured while synchronizing SCC datapool, Error Value=%d", s32RetVal));
			}
			else
			{
				return TRUE;
			}
		}
	}
	else
	{
	    ETG_TRACE_FATAL_THR(("--- s32SyncDatapoolSCC: Error occured while fetching EOL values from EOLLib Shared Memory, Error Value=%d", s32RetVal));
	}
	return FALSE;
}

//---------------------------------------------------------------------------------------------------------------------------------------
static tBool bCheckEOLBlockAndSendEOLDataToSCC(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8 u8TableId)
{
	tBool bReturnValue = FALSE;
	if(VsShMemEOLLIB != NULL)
	{
		for(tU16 i = 0; i< EOL_MAX_CAL_BLOCK_TRNSF_TO_SCC; i++)
		{
			if((tU8)szU8EOLBlock_Trnsf_to_SCC[i] == u8TableId)
			{
				tS32 s32SCC_SendResult = s32PrepareAndSendDataToSCC(VsShMemEOLLIB);
				if(s32SCC_SendResult != 0)
				{
					vWritePrintfErrmem("EOLLib: Error occured while sending data to SCC, Error Value=%d", s32SCC_SendResult);										
					ETG_TRACE_ERR_THR(("--- bCheckEOLBlockAndSendEOLDataToSCC: Error occured while sending data to SCC, Error Value=%d", s32SCC_SendResult));
				}
				bReturnValue = TRUE;
				break;
			}
		}
	}
	return bReturnValue;
}

//End: [yuv2cob - 26.08.2014] EOLLIB SCC implementation

#else
static tS32 s32WriteBlockToFFD(tsDiagEOLBlock* psBlock)
{
	if(OSAL_NULL == psBlock)
	{
		ETG_TRACE_ERR_THR(("--- s32WriteBlockToFFD: Null EOL Block pointer ARGUMENT Passed!!!"));
		return OSAL_E_INVALIDVALUE;
	}

	ETG_TRACE_USR3_THR(("---> s32WriteBlockToFFD"));

	tS32 s32BytesWrite = 0;
	tS32 s32ReturnValue = OSAL_E_NOERROR;

	/*open the device*/
	psBlock->hDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD, OSAL_EN_READWRITE);
	if (psBlock->hDev == OSAL_ERROR)
	{
		/* Open FFD error */
		ETG_TRACE_ERR_THR(("--- s32WriteBlockToFFD: OSAL_IOOpen on dev_ffd FAILED!!!"));
		s32ReturnValue = OSAL_E_IOERROR;
	}
	else
	{
		OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
		VtsFFDDevInfo.u8DataSet = psBlock->enDataSet;
		VtsFFDDevInfo.pvArg = (void*)psBlock->Header;
		if (OSAL_s32IOWrite(psBlock->hDev,(tPCS8)&VtsFFDDevInfo,sizeof(psBlock->Header))!=sizeof(psBlock->Header))
		{
			/* Header Write error */
			ETG_TRACE_ERR_THR(("--- s32WriteBlockToFFD: OSAL_s32IOWrite on dev_ffd for Header FAILED!!!"));
			s32ReturnValue = OSAL_E_IOERROR;
		}
		else
		{
			VtsFFDDevInfo.u8DataSet = psBlock->enDataSet|0x80;
			VtsFFDDevInfo.pvArg = (void*)psBlock->pu8Data;
			s32BytesWrite = OSAL_s32IOWrite(psBlock->hDev,(tPCS8)&VtsFFDDevInfo,psBlock->u16Length);
			if (s32BytesWrite!=psBlock->u16Length)
			{
				/* Block Write error */
				ETG_TRACE_ERR_THR(("--- s32WriteBlockToFFD: OSAL_s32IOWrite on dev_ffd for Block FAILED (%d/%d)!!!",
					s32BytesWrite, psBlock->u16Length));
				s32ReturnValue = OSAL_E_IOERROR;
			}
		}

		ETG_TRACE_COMP_THR(("--- s32WriteBlockToFFD: Table:%u Len:%u Got:%u Data:%*x",
			ETG_ENUM(EOLLIB_TABLE_ID, psBlock->u8TableId), 
			psBlock->u16Length, 
			s32BytesWrite,
			ETG_LIST_LEN((s32BytesWrite > 200)? 200: s32BytesWrite), 
			ETG_LIST_PTR_T8(psBlock->pu8Data) ));

		OSAL_s32IOClose(psBlock->hDev);		
	}

	ETG_TRACE_USR3_THR(("<--- s32WriteBlockToFFD"));
	return s32ReturnValue;
}

static tVoid vSaveAllEOLBlocksInFFDFlash(tVoid)
{
	ETG_TRACE_USR4_THR(("---> vSaveAllEOLBlocksInFFDFlash"));

	OSAL_tIODescriptor hDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD, OSAL_EN_READWRITE);

	if (hDev == OSAL_ERROR)
	{
		/* Open FFD error */
		ETG_TRACE_ERR_THR(("--- vSaveAllEOLBlocksInFFDFlash: OSAL_IOOpen on dev_ffd FAILED!!!"));
		vWritePrintfErrmem("EOLLib: FFD failed to open for writing EOL blocks");
	}
	else
	{
		OSAL_trFFDDeviceInfo VtsFFDDevInfo;
		VtsFFDDevInfo.u8DataSet = EN_FFD_DATA_SET_EOL_SYSTEM; //savenow saves all datasets
		VtsFFDDevInfo.pvArg = 0;

		if (OSAL_s32IOControl(hDev, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW, (tS32)&VtsFFDDevInfo) == OSAL_ERROR)      
		{
			/* Save Now error */
			ETG_TRACE_ERR_THR(("--- vSaveAllEOLBlocksInFFDFlash: OSAL_s32IOControl(OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW) on dev_ffd FAILED!!!"));
			vWritePrintfErrmem("EOLLib: Writing EOL Blocks in FFD failed");
		}

		OSAL_s32IOClose(hDev);
	}

	ETG_TRACE_USR4_THR(("<--- vSaveAllEOLBlocksInFFDFlash"));
}

/***********************************************************************
 * static tVoid vRestoreEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB)
 *    	
 * This function is the old implementation of reading EOL Calibration
 * block values from FFD & loads into shared memory. If FFD read fails
 * it reads default EOL block values into shared memory.
 *
 * @return  
 *    None
 *
 * @date    15.11.2010
 *
 * @note
 *
 **********************************************************************/
static tVoid vRestoreEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
	tBool bResult = TRUE;

	if (VsShMemEOLLIB != NULL)
	{
		if(bCheckAndWaitForP3PartitionMounting())
		{
			for (tU32 i=0;i<EOL_BLOCK_NBR;i++)
			{
				if (OSAL_E_NOERROR != s32ReadBlockFromFFD(&(VsShMemEOLLIB->sDiagEOLBlocks[i])))
				{
					vGetDefaultEOLBlockValues(&(VsShMemEOLLIB->sDiagEOLBlocks[i]));
					bResult = FALSE;
				}
			}

			if (bResult == FALSE)
			{
				vSaveAllEOLBlocksInFFDFlash();
			}
		}
	}
}
#endif

static tsDiagEOLBlock* psGetBlock(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8 u8TableId)
{
   ETG_TRACE_USR4_THR(("---> psGetBlock"));

   tsDiagEOLBlock* psBlock = NULL;

	if (VsShMemEOLLIB != NULL)
	{
		tU32 i;
		for (i=0;i<EOL_BLOCK_NBR;i++)
		{
			if ((VsShMemEOLLIB->sDiagEOLBlocks[i].u8TableId) == u8TableId)
			{
				psBlock = &(VsShMemEOLLIB->sDiagEOLBlocks[i]);
				break;
			}
		}
	}

   ETG_TRACE_USR4_THR(("<--- psGetBlock"));
   return psBlock;
}

/***********************************************************************
 * static       tsEOLLIBSharedMemory*        psEOLLIBLock(tVoid)
 *    	
 * This function gives a pointer to the start of the shared memory and 
 * locks the memory with the internal mutex. 
 *
 * @return  
 *    tsEOLLIBSharedMemory*  pointer of shared memory
 *
 * @date    15.11.2010
 *
 * @note
 *
 **********************************************************************/
static tsEOLLIBSharedMemory*  psEOLLIBLock(tVoid)
{
   tsEOLLIBSharedMemory* VtsShMemEOLLIB=NULL;

   ETG_TRACE_USR4_THR(("---> psEOLLIBLock"));
   {
       VtsShMemEOLLIB = (tsEOLLIBSharedMemory*)
          OSAL_pvSharedMemoryMap(vsEOLLibHandleSharedMemory,OSAL_EN_READONLY,sizeof(tsEOLLIBSharedMemory),0);
       /*check error*/
       if(VtsShMemEOLLIB==NULL)
       {
          ETG_TRACE_ERR_THR(("!!! psEOLLIBLock: OSAL_pvSharedMemoryMap FAILED"));
          NORMAL_M_ASSERT_ALWAYS();
       }

   }
    ETG_TRACE_USR4_THR(("<--- psEOLLIBLock"));

   return(VtsShMemEOLLIB);
}

/***********************************************************************
 * static tVoid vEOLLIBUnLock(tsEOLLIBSharedMemory* PtsShMemEOLLIB)
 *    	
 * This function releases a shared memory pointer and unlocks the internal mutex.
 *
 * @parameter
 *     PtsShMemEOLLIB  Pointer to shared memory 
 *   
 * @date   15.11.2010
 *
 * @note
 *
 **********************************************************************/
static tVoid vEOLLIBUnLock(tsEOLLIBSharedMemory* PtsShMemEOLLIB)
{
	ETG_TRACE_USR4_THR(("---> vEOLLIBUnLock"));

	if(OSAL_s32SharedMemoryUnmap(PtsShMemEOLLIB,sizeof(tsEOLLIBSharedMemory))==OSAL_ERROR)
	{
		ETG_TRACE_ERR_THR(("!!! vEOLLIBUnLock: OSAL_s32SharedMemoryUnmap FAILED"));
		NORMAL_M_ASSERT_ALWAYS();
	}

	ETG_TRACE_USR4_THR(("<--- vEOLLIBUnLock"));
}

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

tS32 DRV_DIAG_EOL_s32IODeviceInit(tVoid)
{
	ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IODeviceInit"));

    tS32 s32Result = OSAL_E_NOERROR;

	DRV_DIAG_EOL_vTraceRegChannel();

    /* create shared memory */
    vsEOLLibHandleSharedMemory = OSAL_SharedMemoryCreate(EOLLIB_C_SHMEM_NAME, OSAL_EN_READWRITE, sizeof(tsEOLLIBSharedMemory));

    if(vsEOLLibHandleSharedMemory == OSAL_ERROR)
    { /*check if error not error "memory already exist"*/
        s32Result = OSAL_u32ErrorCode();
        if(s32Result != OSAL_E_ALREADYEXISTS)
        {/* assert */
            ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IODeviceInit: s32Result=%d on OSAL_SharedMemoryCreate", s32Result));
            NORMAL_M_ASSERT_ALWAYS();
        }
        else
        {/* memory create ALREADYEXISTS; get now valid handle*/
            vsEOLLibHandleSharedMemory = OSAL_SharedMemoryOpen(EOLLIB_C_SHMEM_NAME, OSAL_EN_READWRITE);
            if(vsEOLLibHandleSharedMemory == OSAL_ERROR)
            {
                s32Result = OSAL_u32ErrorCode();
                ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IODeviceInit: s32Result=%d on OSAL_SharedMemoryOpen", s32Result));
            }
            else
            {
                s32Result = OSAL_E_NOERROR;
            }
        }
    }

	if(s32Result == OSAL_E_NOERROR)
	{
		// lock and get memory
		tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

		//VsShMemEOLLIB->bShMemInitiated = 0;
		//TSH_TP(0x01);
		//clear data in shared memory
		if (VsShMemEOLLIB != NULL)	
		{
			//TSH_TP(0x02);
			memset(VsShMemEOLLIB->sDiagEOLBlocks,0x00,sizeof(VsShMemEOLLIB->sDiagEOLBlocks));
			
			//init EOL Blocks
			ETG_TRACE_USR3_THR(("DRV_DIAG_EOL_s32IODeviceInit - Call Init"));
			vInitEOLBlocks(VsShMemEOLLIB);

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

			//Create Datapool instance once before use!
			DP_vCreateDatapool();

			//Read data from Datapool (RAW_NOR) and save it into shared memory
			vReadEOLBlocks(VsShMemEOLLIB);
			//vCheckAllEOLBlocksAndSendEOLDataToSCC(VsShMemEOLLIB); //Not required to send explicitly @ startup. Data sync b/w iMx & SCC handled by PDD itself.

#else
			vRestoreEOLBlocks(VsShMemEOLLIB);
#endif
		}
		else
			s32Result = OSAL_ERROR;
		
		if (s32Result == OSAL_E_NOERROR)
		{
			//TSH_TP(0x03);
			VsShMemEOLLIB->u8ShMemInitiated = EOLLIB_INIT_MAGIC; //lint !e613: Possible use of null pointer 
		}
		vEOLLIBUnLock(VsShMemEOLLIB);
	}

	ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IODeviceInit"));

	return(s32Result);
}

/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_IOOpen
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_IOOpen(tVoid)
 * ARGUMENTS   : 
 *               tVoid 
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_IOOpen( tVoid )
{
   tS32 s32Result = OSAL_E_NOERROR;

   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_IOOpen"));

   {
	   vsEOLLibHandleSharedMemory =OSAL_SharedMemoryOpen(EOLLIB_C_SHMEM_NAME,OSAL_EN_READWRITE);
	   if(vsEOLLibHandleSharedMemory == OSAL_ERROR)
	   {
		   s32Result = OSAL_u32ErrorCode();
		   ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_IOOpen: s32Result=%d on OSAL_SharedMemoryOpen", s32Result));
	   }
	   else
	   {
		   tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

		   if (VsShMemEOLLIB != NULL)
		   {
               if (VsShMemEOLLIB->u8ShMemInitiated != EOLLIB_INIT_MAGIC)
			   {

					ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_IOOpen: VsShMemEOLLIB->u8ShMemInitiated=0x%x", 
						VsShMemEOLLIB->u8ShMemInitiated));
					 
					s32Result = OSAL_E_CANCELED;
			   }

			   vEOLLIBUnLock(VsShMemEOLLIB);
		   }
	   }
   }

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_IOOpen"));
   
   return(s32Result);
} /* DRV_DIAG_EOL_IOOpen */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IOControl
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IOControl(tS32 s32fun,tS32 s32arg)
 * ARGUMENTS   : 
 *               tS32 s32fun
 *               tS32 s32arg
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IOControl(tS32 s32fun, tS32 s32arg)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IOControl"));

   tS32 s32Result = OSAL_E_CANCELED;

     tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

   switch (s32fun)
   {
     case OSAL_C_S32_IOCTRL_DIAGEOL_GET_MODULE_IDENTIFIER:
     {
         OSAL_trDiagEOLModuleIdentifier* pArg = (OSAL_trDiagEOLModuleIdentifier*) s32arg;

         if (NULL == pArg)
         {
			 s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
			 tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pArg->u8Table);

             if (psBlock == NULL)
             {
                 // requested Table is not supported 
                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
                

				 pArg->u32PartNumber = 
					 (psBlock->Header[6] << 24) | 
					 (psBlock->Header[7] << 16) |
					 (psBlock->Header[8] << 8) |
					 psBlock->Header[9];

				 pArg->u8DesignLevelSuffix[0] = psBlock->Header[10];
				 pArg->u8DesignLevelSuffix[1] = psBlock->Header[11];

		         s32Result = OSAL_E_NOERROR;
			 }
         }         

		 break;
	 }

     case OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC:
     {
         tU32 i;
         tU32 u32Crc = 0;
         OSAL_trDiagEOLEntry* pArg = (OSAL_trDiagEOLEntry*)(void*) s32arg;

         if (pArg->pu8EntryData == NULL)
         {
             s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
             tU8* pu8Data;
             tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pArg->u8Table);

             if (psBlock == NULL)
             {
                 //requested Table is not supported 
                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
                

                 //Build the checksum over the header
                  i = sizeof(psBlock->Header)-2;
                  pu8Data = &psBlock->Header[2];
                  while(i > 1)
	               {
		               u32Crc += 0xFFFF & ((pu8Data[1]<<8) | pu8Data[0]);
		               pu8Data+=2;
		               i -= 2;
	               }

                  //Add trailing byte
	               if (i)
	               {
		               u32Crc += (0xFF & pu8Data[0]);
	               }

                  //Build the checksum over the data block
                  i = psBlock->u16Length;
                  pu8Data = &psBlock->pu8Data[0];
                  while(i > 1)
	               {
		               u32Crc += 0xFFFF & ((pu8Data[1]<<8) | pu8Data[0]);
		               pu8Data+=2;
		               i -= 2;
	               }

                  //Add trailing byte
	               if (i)
	               {
		               u32Crc += (0xFF & pu8Data[0]);
	               }

#if 0
                  //1's complement
                  //Add carry bits
	               while (u32Crc>>16)
	               {
		               u32Crc = (u32Crc & 0xFFFF)+(u32Crc >> 16);
	               }
                  u32Crc ^= 0xFFFF;
#else
                  //2's complement
                  u32Crc &= 0xFFFF;
                  u32Crc ^= 0xFFFF;
                  u32Crc++;
#endif

                 pArg->pu8EntryData[0] = psBlock->Header[0];
                 pArg->pu8EntryData[1] = psBlock->Header[1];
                 pArg->pu8EntryData[2] = u32Crc & 0xFF;
                 pArg->pu8EntryData[3] = (u32Crc >> 8) & 0xFF;

                 s32Result = OSAL_E_NOERROR;

                 ETG_TRACE_COMP_THR(("--- OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC: Table:%u Header CRC=0x%x Calc CRC=0x%x",
                    pArg->u8Table, 
                    (psBlock->Header[1] << 8) | psBlock->Header[0],
                    u32Crc));
             }
         }

        break;
     }

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS
	case OSAL_C_S32_IOCTRL_DIAGEOL_SYNC_SCC_DATAPOOL:
	{
		//Create Datapool instance once before use!
		DP_vCreateDatapool();

		tBool bRetVal = s32SyncDatapoolSCC(VsShMemEOLLIB);
		if(TRUE == bRetVal)
		{
			vWritePrintfErrmem("EOLLib: EOL calibration values are synchronized between SCC and iMx");										
			ETG_TRACE_FATAL_THR(("--- DRV_DIAG_EOL_s32IOControl: EOL calibration values are synchronized between SCC and iMx"));
			s32Result = OSAL_E_NOERROR;
		}
		break;
	}
#endif

     default:
       break;
   }

   vEOLLIBUnLock(VsShMemEOLLIB);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IOControl"));

   return(s32Result);
} /* DRV_DIAG_EOL_s32IOControl */


//Start: [yuv2cob - 25.07.2014] EOLLIB Datapool Migration

/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IOWrite
 * CREATED     : 2007-05-08
 * AUTHOR      : Thomas & Yuvaraj
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IOWrite(tPCS8 ps8Buffer,tU32 u32nbytes)
 * ARGUMENTS   : 
 *               tPCS8 ps8Buffer
 *               tU32 u32nbytes
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IOWrite(tPCS8 ps8Buffer, tU32 u32nbytes)
{
	ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IOWrite"));

	tS32 s32Result = OSAL_E_NOERROR;

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS
	//Create Datapool instance once before use!
	DP_vCreateDatapool();
#endif
	tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

	if ((ps8Buffer == NULL) || (u32nbytes != sizeof(OSAL_trDiagEOLEntry)))
	{
		ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IOWrite: INVALID Read Parameter!!!"));
		s32Result = OSAL_E_INVALIDVALUE;
	}
	else 
	{

		OSAL_trDiagEOLEntry* pEntry = (OSAL_trDiagEOLEntry*)(void*) ps8Buffer;//lint !e1773: Attempt to cast away const (or volatile)

		ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IOWrite: Table:%u Offset:%u Len:%u",
			ETG_ENUM(EOLLIB_TABLE_ID, pEntry->u8Table), 
			pEntry->u16Offset, pEntry->u16EntryLength));

         if (pEntry->pu8EntryData == NULL)
         {
             ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IOWrite: pu8EntryData is NULL POINTER!!!"));

             s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
             tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pEntry->u8Table);

             if (psBlock == NULL)
             {
				ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IOWrite: Table NOT SUPPORTED!!!"));
				s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {			 
				if (pEntry->u16EntryLength == 0) 
				{
					u32nbytes = sizeof(psBlock->Header);

					memcpy(psBlock->Header, 
								 pEntry->pu8EntryData, 
								 u32nbytes);

					s32Result = s32WriteBlock(psBlock);
				}
				else
				{
					if (pEntry->u16Offset >= psBlock->u16Length)
					{
						 ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IOWrite: INVALID Offset!!!"));
						 s32Result = OSAL_E_INVALIDVALUE;
					}
					else
					{
						 u32nbytes = pEntry->u16EntryLength;
						 if (u32nbytes > (tU32)(psBlock->u16Length - pEntry->u16Offset))
						 {
							 u32nbytes = psBlock->u16Length - pEntry->u16Offset;
						 }

						 memcpy(psBlock->pu8Data + pEntry->u16Offset, 
										 pEntry->pu8EntryData, 
										 u32nbytes);
						 s32Result = s32WriteBlock(psBlock);

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS					 	 
						 (tVoid)bCheckEOLBlockAndSendEOLDataToSCC(VsShMemEOLLIB, psBlock->u8TableId); //Changes for additional offset in BRAND cal block to be transferred to SCC
#endif
					}
				}
			}
        }

		if (s32Result == OSAL_E_NOERROR)
		{
			ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IOWrite: Size:%d Data:%*x",
			   u32nbytes, 
			   ETG_LIST_LEN((u32nbytes > 200)? 200: u32nbytes), 
			   ETG_LIST_PTR_T8(pEntry->pu8EntryData) ));
		}
   }

   vEOLLIBUnLock(VsShMemEOLLIB);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IOWrite"));

   return (s32Result == OSAL_E_NOERROR)? u32nbytes: s32Result;
} /* DRV_DIAG_EOL_s32IOWrite */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IORead
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IORead(tPCS8 ps8Buffer,tU32 u32nbytes)
 * ARGUMENTS   : 
 *               tPCS8 ps8Buffer
 *               tU32 u32nbytes
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IORead(tPCS8 ps8Buffer, tU32 u32nbytes)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IORead"));

   tS32 s32Result = OSAL_E_NOERROR;

   tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

   if ((ps8Buffer == NULL) || (u32nbytes != sizeof(OSAL_trDiagEOLEntry)))
   {
       ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IORead: INVALID Read Parameter!!!"));

       s32Result = OSAL_E_INVALIDVALUE;
   }
   else 
   {     
         OSAL_trDiagEOLEntry* pEntry = (OSAL_trDiagEOLEntry*)(void*) ps8Buffer;//lint !e1773: Attempt to cast away const (or volatile)

         ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IORead: Table:%u Offset:%u Len:%u",
            ETG_ENUM(EOLLIB_TABLE_ID, pEntry->u8Table), 
            pEntry->u16Offset, 
            pEntry->u16EntryLength));

         if (pEntry->pu8EntryData == NULL)
         {
             ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IORead: pu8EntryData is NULL POINTER!!!"));

             s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
             tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pEntry->u8Table);

             if (psBlock == NULL)
             {
                 ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IORead: Table NOT SUPPORTED!!!"));

                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
				 if (pEntry->u16EntryLength == 0) 
				 {
                     u32nbytes = sizeof(psBlock->Header);

					 memcpy(pEntry->pu8EntryData,
                                       psBlock->Header, 
									   u32nbytes);
				 }
				 else
				 {
                     if (pEntry->u16Offset >= psBlock->u16Length)
                     {
                         ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IORead: INVALID Offset (%d/%d) in table %d!!!",
                               pEntry->u16Offset, psBlock->u16Length, pEntry->u8Table));

                         s32Result = OSAL_E_INVALIDVALUE;
                     }
                     else
                     {
                         u32nbytes = pEntry->u16EntryLength;
                         if (u32nbytes > (tU32)(psBlock->u16Length - pEntry->u16Offset))
                         {
                             u32nbytes = psBlock->u16Length - pEntry->u16Offset;
                         }

                         memcpy(pEntry->pu8EntryData,
                               psBlock->pu8Data + pEntry->u16Offset, 
                               u32nbytes);
                     }
                 }
             }
         }

         if (s32Result == OSAL_E_NOERROR)
         {
            ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IORead: Size:%d Data:%*x",
               u32nbytes, 
               ETG_LIST_LEN((u32nbytes > 200)? 200: u32nbytes), 
               ETG_LIST_PTR_T8(pEntry->pu8EntryData) ));
         }
         else
         {
            ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IORead: s32Result=%d", 
               s32Result));
         }
   }

   vEOLLIBUnLock(VsShMemEOLLIB);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IORead"));

   return (s32Result == OSAL_E_NOERROR)? u32nbytes: s32Result;
} // DRV_DIAG_EOL_s32IORead 

/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IOClose
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IOClose(tVoid)
 * ARGUMENTS   : 
 *               tVoid 
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IOClose( tVoid )
{
   tS32 s32Result = OSAL_E_NOERROR;
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IOClose"));

   OSAL_s32SharedMemoryClose(vsEOLLibHandleSharedMemory);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IOClose"));

   return(s32Result);
} /* DRV_DIAG_EOL_s32IOClose */


tS32 DRV_DIAG_EOL_s32IODeviceRemove(tVoid)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IODeviceRemove"));

   tS32 s32Result = OSAL_E_NOERROR;

   /*close and delete shared memory*/
   OSAL_s32SharedMemoryClose(vsEOLLibHandleSharedMemory);
   OSAL_s32SharedMemoryDelete(EOLLIB_C_SHMEM_NAME);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IODeviceRemove"));

   return(s32Result);
}

//End: [yuv2cob - 25.07.2014] EOLLIB Datapool Migration

//tS32 EOLLib_Read(OSAL_trDiagEOLEntry* pEntry)
//{
//   static tsDiagEOLBlock sBlock;
//   tS32 s32Result = OSAL_E_NOERROR;
//   tS32 s32BytesRead = 0;
//   tU32 u32nbytes = 0;
//   
//   if ((pEntry == NULL) || (pEntry->pu8EntryData == NULL))
//   {
//      s32Result = OSAL_E_INVALIDVALUE;
//   }
//   else
//   {
//      sBlock.u8TableId = pEntry->u8Table;
//
//      switch (sBlock.u8TableId)
//      {
//      case EOLLIB_TABLE_ID_SYSTEM:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_SYSTEM;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END;
//         break;
//      case EOLLIB_TABLE_ID_DISPLAY_INTERFACE:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_DISPLAY;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_F_B_END;
//         break;
//      case EOLLIB_TABLE_ID_BLUETOOTH:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_BLUETOOTH;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END;
//         break;
//      case EOLLIB_TABLE_ID_NAVIGATION_SYSTEM:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_NAV_SYSTEM;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END;
//         break;
//      case EOLLIB_TABLE_ID_NAVIGATION_ICON:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_NAV_ICON;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END;
//         break;
//      case EOLLIB_TABLE_ID_BRAND:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_BRAND;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_BRAND_END;
//         break;
//      case EOLLIB_TABLE_ID_COUNTRY:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_COUNTRY;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END;
//         break;
//      case EOLLIB_TABLE_ID_SPEECH_RECOGNITION:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_SPEECH_REC;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END;
//         break;
//      case EOLLIB_TABLE_ID_HAND_FREE_TUNING:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_HF_TUNING;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END;
//         break;
//      case EOLLIB_TABLE_ID_REAR_VISION_CAMERA:
//         //sBlock.enDataSet = EN_FFD_DATA_SET_EOL_RVC;
//         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END;
//         break;
//
//      default:
//         s32Result = OSAL_E_INVALIDVALUE;
//         sBlock.u16Length = 0;
//         break;
//      }
//   }
//
//   if (pEntry->u16Offset >= sBlock.u16Length)
//   {
//      // requested Offset is out-of-range 
//      s32Result = OSAL_E_INVALIDVALUE;
//   }
//
//   if (s32Result == OSAL_E_NOERROR)
//   {
//      //sBlock.hDev = tk_opn_dev((UB*)"ffdlld",TD_UPDATE);
//      sBlock.hDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READONLY);
//      if (sBlock.hDev == OSAL_ERROR)
//      {
//         /* Open FFD error */
//         s32Result = OSAL_E_INVALIDVALUE;
//      }
//      else
//      {
//         OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
//         //VtsFFDDevInfo.u8DataSet=sBlock.enDataSet;
//         VtsFFDDevInfo.pvArg=sBlock.Header;
//         if(OSAL_s32IORead(sBlock.hDev,(tPS8)&VtsFFDDevInfo,sizeof(sBlock.Header))!=sizeof(sBlock.Header))
//         //if (E_OK != tk_srea_dev(sBlock.hDev, sBlock.enDataSet,
//	     //       (VP)sBlock.Header, sizeof(sBlock.Header), &s32BytesRead))
//         {
//            // Header Read Error 
//            s32Result = OSAL_E_INVALIDVALUE;
//         }
//         //else if (E_OK != tk_srea_dev(sBlock.hDev, sBlock.enDataSet | FFD_LLD_DN_FILE_POS,
//         //      (VP)sBlock.pu8Data, sBlock.u16Length, &s32BytesRead))
//         else
//         {
//            //VtsFFDDevInfo.u8DataSet=sBlock.enDataSet|0x80;
//            VtsFFDDevInfo.pvArg=(void*)sBlock.pu8Data;
//            s32BytesRead=OSAL_s32IORead(sBlock.hDev,(tPS8)&VtsFFDDevInfo,sBlock.u16Length);
//            if (s32BytesRead!=sBlock.u16Length)           
//            {
//              // Block Read Error 
//              s32Result = OSAL_E_INVALIDVALUE;
//            }
//         }
//         //tk_cls_dev(sBlock.hDev, 0);
//        OSAL_s32IOClose(sBlock.hDev);
//      }
//
//      if (s32Result == OSAL_E_NOERROR)
//      {
//         u32nbytes = pEntry->u16EntryLength;
//         if (u32nbytes > (sBlock.u16Length - pEntry->u16Offset))
//         {
//            u32nbytes = sBlock.u16Length - pEntry->u16Offset;
//         }
//
//         memcpy(pEntry->pu8EntryData, 
//                    sBlock.pu8Data + pEntry->u16Offset, 
//                    u32nbytes);
//      }
//   }
//
//   return (s32Result == OSAL_E_NOERROR)? u32nbytes: s32Result;
//}

#ifdef LOAD_EOL_SO
static sem_t       *initeol_lock;

extern tS32 DEV_FFD_s32IODeviceInit(tVoid);

//Start: [yuv2cob - 25.07.2014] LINT Warnings Fix
tS32 eol_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Id);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(szName);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enAccess);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32FD);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(app_id);

	return (tU32)(DRV_DIAG_EOL_IOOpen());
}

tS32 eol_drv_io_close(tS32 s32ID, tU32 u32FD)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
	return DRV_DIAG_EOL_s32IOClose();
}

tS32 eol_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
	return((tS32)DRV_DIAG_EOL_s32IOControl(s32fun, s32arg));
}

tS32 eol_drv_io_write(tS32 s32ID, tU32 u32FD, tPCS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ret_size);
	return((tS32)DRV_DIAG_EOL_s32IOWrite(pBuffer, u32Size));
}

tS32 eol_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
	OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ret_size);
	return((tS32)DRV_DIAG_EOL_s32IORead((tPS8)pBuffer, u32Size));
}

//End: [yuv2cob - 25.07.2014] LINT Warnings Fix

//Start: [yuv2cob - 30.08.2014] EOLLIB shared resource group set to eco_osal
void __attribute__ ((constructor)) eol_process_attach(void)
{
   tBool bFirstAttach = FALSE;
   initeol_lock = sem_open(EOLLIB_SEM_NAME, O_EXCL | O_CREAT, EOLLIB_ACCESS_RIGTHS, 0);

   if (initeol_lock != SEM_FAILED)
   {
      bFirstAttach = TRUE;
      struct group *pOsalGroup = getgrnam(EOLLIB_ACCESS_RIGTHS_GROUP_NAME);
      if(pOsalGroup==NULL)
      {
         // could be possible in non Multi User Systems, so do not Use Fatal or Error here
         ETG_TRACE_USR1_THR(("eol_process_attach getgrnam -> error %d", errno));
      }// if(pOsalGroup==NULL)
      else
      {
         tS32 s32OSALGroupID = pOsalGroup->gr_gid;
         if(s32OSALGroupID)
         {
           // umask (022) is overwriting the permissions on creation, so we have to set it again
           // change rigths for EOL_INT
           if(chmod("/dev/shm/sem."EOLLIB_SEM_NAME, EOLLIB_ACCESS_RIGTHS) == -1)
           {
               ETG_TRACE_ERR_THR(("eol_process_attach chmod -> error %d",errno));
           }// if(chmod(...

           if(chown("/dev/shm/sem."EOLLIB_SEM_NAME, (uid_t)-1, s32OSALGroupID) == -1)
           {
               ETG_TRACE_ERR_THR(("eol_process_attach chown -> error %d", errno));
           }// if(chown(...
         }// if(s32OSALGroupID)
      }// else // if(pOsalGroup==NULL)
   }
   else
   {
       initeol_lock = sem_open(EOLLIB_SEM_NAME, 0);
       bFirstAttach = FALSE;
       if(initeol_lock != SEM_FAILED)
       {
          sem_wait(initeol_lock);
       }
   }
   DEV_FFD_s32IODeviceInit();

   if(bFirstAttach)
   {
      DRV_DIAG_EOL_s32IODeviceInit();
   }

   if(initeol_lock != SEM_FAILED)
   {
      sem_post(initeol_lock);
   }
   else
   {
	   vWritePrintfErrmem("EOLLib: Semaphore open error: %d", errno);
   }
}
//End: [yuv2cob - 30.08.2014] EOLLIB shared resource group set to eco_osal

void vOnEolProcessDetach( void )
{
}
#endif


#ifdef __cplusplus
}
#endif

/* End of File eollib_dp.cpp*/

