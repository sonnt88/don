/**********************************************************FileHeaderBegin******
 *
 * FILE:        EOLLib.c
 *
 * CREATED:     2010-10-12
 *
 * AUTHOR:
 *
 * DESCRIPTION: -
 *
 * NOTES: -
 *
 * COPYRIGHT:  (c) 2005 Technik & Marketing Service GmbH
 *
 **********************************************************FileHeaderEnd*******/

/*****************************************************************************
 *
 * search for this KEYWORDS to jump over long histories:
 *
 * CONSTANTS - TYPES - MACROS - VARIABLES - FUNCTIONS - PUBLIC - PRIVATE
 *
 ******************************************************************************/

/******************************************************************************
 * LOG:
 *
 * $Log: $
 ******************************************************************************/


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/
#include "OsalConf.h"

#include "trace_interface.h"
/* OSAL Device header */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"

/* EOL table and offset definitions */
#include "EOLLib.h"

#include <dlfcn.h>

/* Needed for Trace */
#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#define ET_TRACE_INFO_ON
#include "etrace_if.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_DEV_DIAGEOL
#include "trcGenProj/Header/eollib_full.c.trc.h"
#endif



//ETG_TRACE_USR3_THR
//ETG_TRACE_COMP_THR
//ETG_TRACE_ERR_THR

#if 0
enum tenTrcTraceClass {
   TR_CLASS_DEV_DIAGEOL = (0x00E7)  // trace class
};
#endif

typedef enum
{
   SYSTEM            =   2,
   DISPLAY_INTERFACE =   3,
   BLUETOOTH         =   4,
   NAVIGATION_SYSTEM =   5,
   NAVIGATION_ICON   =   6,
   BRAND             =   7,
   COUNTRY           =   8,
   SPEECH_RECOGNITION=   9,
   HAND_FREE_TUNING  =   10,
   REAR_VISION_CAMERA=   11
} EOLLib_tenTable;


#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/


/*defines for shared memory*/
#define EOLLIB_C_SHMEM_NAME                     "EOLLIB_SHME"

/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/

#define OSAL_C_S32_IOCTRL_DIAGEOL_RESET_TARGET_DEFAULTS     0x101
#define OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC                 0x102
#define OSAL_C_S32_IOCTRL_DIAGEOL_RESET_DEFAULTS            0x103


/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/

typedef struct sDiagEOLBlock
{
    tU8     u8TableId;
    tPCS8   ps8TableName;
    OSAL_tIODescriptor hFile;
    tenFDDDataSet enDataSet;
    int      hDev;
	tU8		Header[14]; 
    tU16    u16Length;
    tU8    pu8Data[U8_EOLLIB_MAX_BLOCK_LENGTH];
} tsDiagEOLBlock;

typedef struct
{
  //tU8               u8MagicSharedMemoryLinux;
  //tU8               u8MagicSharedMemoryTengine;
  //tU32              u32KDSNumberOfEntries;
	tU8			u8ShMemInitiated;
  tsDiagEOLBlock    sDiagEOLBlocks[EOL_BLOCK_NBR];  
}tsEOLLIBSharedMemory;


/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/


/*****************************************************************
| static variable declaration
|----------------------------------------------------------------*/
static OSAL_tShMemHandle    vsEOLLibHandleSharedMemory = OSAL_ERROR;


/*****************************************************************
| function prototypes (scope: local)
|----------------------------------------------------------------*/
static       tsEOLLIBSharedMemory* psEOLLIBLock(void);
static       void               vEOLLIBUnLock(tsEOLLIBSharedMemory* PtsShMemEOLLIB);
static tsDiagEOLBlock* psGetBlock(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8 u8TableId);
static tVoid vCreateDefaultHeader(tsDiagEOLBlock* psBlock, tU8 u8Defaults);
static tVoid vCreateDefaultBlock(tsDiagEOLBlock* psBlock, tU8 u8Defaults);
static tVoid vRestoreBlock(tsDiagEOLBlock* psBlock);
static tVoid vInitEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB);
static tVoid vInitBlock(tsDiagEOLBlock* psEOLBlock,  tU8 u8TableId, tPCS8 ps8TableName, tenFDDDataSet enDataSet, tU16 u16Length);
static tVoid vRestoreEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB);


static tVoid vSaveNow()
{
    ETG_TRACE_USR3_THR(("---> vSaveNow"));

    OSAL_tIODescriptor hDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);

    if (hDev == OSAL_ERROR)
    {
		/* Open FFD error */
       ETG_TRACE_ERR_THR(("--- vSaveNow: OSAL_IOOpen on dev_ffd FAILED!!!"));
    }
    else
    {
       OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
       VtsFFDDevInfo.u8DataSet = EN_FFD_DATA_SET_EOL_SYSTEM; //savenow saves all datasets
       VtsFFDDevInfo.pvArg = 0;

       if (OSAL_s32IOControl(hDev, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW, (tS32)&VtsFFDDevInfo) == OSAL_ERROR)      
       {
			/* Save Now error */
         ETG_TRACE_ERR_THR(("--- vSaveNow: OSAL_s32IOControl(OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW) on dev_ffd FAILED!!!"));
		 }

       OSAL_s32IOClose(hDev);
    }

    ETG_TRACE_USR3_THR(("<--- vSaveNow"));
}

static tVoid DRV_DIAG_EOL_vTraceCommand(unsigned char* PubpData)
{
//    vTraceMsg(0x0B01, TR_LEVEL_USER_4);

/*#if 0
	switch (PubpData[1])
	{
		case 1: //DEV_DIAGEOL_RESET_TARGET_DEFAULTS
			{
//				vTraceMsg(0x0B03, TR_LEVEL_USER_4);

				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_TUNER), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_AUDIO), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_EQUALIZATION), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_SYSTEM), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_LIGHTING), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BUTTON), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_COUNTRY), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BRAND), 0);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_RVC), 0);
			}
			break;

		case 2: //DEV_DIAGEOL_RESET_DEFAULTS %i(EOL_DEFAULTS)
			{
//				vTraceMsg(0x0B04, TR_LEVEL_USER_4);

				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_TUNER), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_AUDIO), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_EQUALIZATION), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_SYSTEM), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_LIGHTING), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BUTTON), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_COUNTRY), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_BRAND), PubpData[2]);
				vResetBlock(psGetBlock(EOLCALIB_TABLE_ID_RVC), PubpData[2]);
			}
			break;
	}
#endif*/

//    vTraceMsg(0x0B02, TR_LEVEL_USER_4);
}

static tVoid DRV_DIAG_EOL_vTraceRegChannel(void)
{ 
	tS32 s32Trace;
	tS32 s32Size;
	LaunchChannel  oTraceChannel;

	oTraceChannel.enTraceChannel = TR_TTFIS_DEV_DIAGEOL;
	oTraceChannel.p_Callback = (OSAL_tpfCallback)DRV_DIAG_EOL_vTraceCommand;
	/*if ((s32Trace = tk_opn_dev((unsigned char*)"trcttfis", TD_UPDATE)) > E_OK)
	{
		if(tk_swri_dev(s32Trace,DN_TRCCALLBACKREG, &oTraceChannel, sizeof(LaunchChannel), &s32Size) < E_OK)
		{
//			vTraceMsg(0x0A04, TR_LEVEL_USER_4);
		}
	}   
	else
	{
//		vTraceMsg(0x0A03, TR_LEVEL_USER_4);
	}

	tk_cls_dev(s32Trace, 0);*/

//	rDevSpmTraceIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE, OSAL_EN_READWRITE);
}

/*static tVoid vAddBlock(tU8 u8TableId, tPCS8 ps8TableName, tU16 u16Length)
{
    ETG_TRACE_USR3_THR(("---> vAddBlock"));

    tsDiagEOLBlock* psNewBlock = NULL;

    psNewBlock = OSAL_pvMemoryAllocate(sizeof(tsDiagEOLBlock)); 
    if (psNewBlock != NULL)
    {
        psNewBlock->u8TableId = u8TableId;
        psNewBlock->ps8TableName = ps8TableName;
        psNewBlock->u16Length = u16Length;
        psNewBlock->hDev = E_SYS;
        psNewBlock->hFile = OSAL_ERROR;
        psNewBlock->pu8Data = NULL;
        psNewBlock->psNext = psDiagEOLBlocks;
        psDiagEOLBlocks = psNewBlock;
    }

    ETG_TRACE_USR3_THR(("<--- vAddBlock"));
}*/



static tVoid vInitBlock(tsDiagEOLBlock* psEOLBlock,  tU8 u8TableId, tPCS8 ps8TableName, tenFDDDataSet enDataSet, tU16 u16Length)
{
    if (psEOLBlock != NULL)
    {
        psEOLBlock->u8TableId = u8TableId;
        psEOLBlock->ps8TableName = ps8TableName;
        psEOLBlock->u16Length = u16Length;
        psEOLBlock->hDev = OSAL_ERROR;
        psEOLBlock->hFile = OSAL_ERROR;
        psEOLBlock->enDataSet = enDataSet;
    }
}


static tsDiagEOLBlock* psGetBlock(tsEOLLIBSharedMemory* VsShMemEOLLIB, tU8 u8TableId)
{
   ETG_TRACE_USR3_THR(("---> psGetBlock"));

   tsDiagEOLBlock* psBlock = NULL;

	if (VsShMemEOLLIB!=NULL)
	{
		tU32 i;
		for (i=0;i<EOL_BLOCK_NBR;i++)
		{
			if ((VsShMemEOLLIB->sDiagEOLBlocks[i].u8TableId) == u8TableId)
			{
				psBlock = &(VsShMemEOLLIB->sDiagEOLBlocks[i]);
				break;
			}
		}
	}

   ETG_TRACE_USR3_THR(("<--- psGetBlock"));

   return psBlock;
}


static tS32 s32WriteBlock(tsDiagEOLBlock* psBlock)
{
   ETG_TRACE_USR3_THR(("---> s32WriteBlock"));

//    char u8File[80];
    tS32 s32ReturnValue = 0;
	//int errornumber;
	tS32 s32BytesWrite = 0;
//	tS32 s32BytesSave = 0;

    /*open the device*/
    psBlock->hDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READWRITE);
    if (psBlock->hDev == OSAL_ERROR)
    {
		/* Open FFD error */
      ETG_TRACE_ERR_THR(("--- s32WriteBlock: OSAL_IOOpen on dev_ffd FAILED!!!"));
    }
    else
    {
        OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
        VtsFFDDevInfo.u8DataSet=psBlock->enDataSet;
        VtsFFDDevInfo.pvArg=(void*)psBlock->Header;
        if (OSAL_s32IOWrite(psBlock->hDev,(tPCS8)&VtsFFDDevInfo,sizeof(psBlock->Header))!=sizeof(psBlock->Header))
		{
			/* Header Write error */
         ETG_TRACE_ERR_THR(("--- s32WriteBlock: OSAL_s32IOWrite on dev_ffd for Header FAILED!!!"));
		}
        else
        {
          VtsFFDDevInfo.u8DataSet=psBlock->enDataSet|0x80;
          VtsFFDDevInfo.pvArg=(void*)psBlock->pu8Data;
          s32BytesWrite=OSAL_s32IOWrite(psBlock->hDev,(tPCS8)&VtsFFDDevInfo,psBlock->u16Length);
          if (s32BytesWrite!=psBlock->u16Length)
          {
          	/* Block Write error */
            ETG_TRACE_ERR_THR(("--- s32WriteBlock: OSAL_s32IOWrite on dev_ffd for Block FAILED (%d/%d)!!!",
                  s32BytesWrite, psBlock->u16Length));
          }
        }

       ETG_TRACE_COMP_THR(("--- s32WriteBlock: Table:%u Len:%u Got:%u Data:%*x",
         ETG_CENUM(EOLLib_tenTable, psBlock->u8TableId), 
         psBlock->u16Length, 
         s32BytesWrite,
         ETG_LIST_LEN((s32BytesWrite > 200)? 200: s32BytesWrite), 
         ETG_LIST_PTR_T8(psBlock->pu8Data) ));

       OSAL_s32IOClose(psBlock->hDev);
    }

   ETG_TRACE_USR3_THR(("<--- s32WriteBlock"));

   return s32ReturnValue;
}


static tVoid vRestoreBlock(tsDiagEOLBlock* psBlock)
{
   ETG_TRACE_USR3_THR(("---> vRestoreBlock"));

   /* Create a Default Block */
   vCreateDefaultHeader(psBlock, 0);
   vCreateDefaultBlock(psBlock, 0);

   ETG_TRACE_USR3_THR(("<--- vRestoreBlock"));
}


static tVoid vCreateDefaultHeader(tsDiagEOLBlock* psBlock, tU8 u8Defaults)
{
   //Checksum
   psBlock->Header[0] = 0;
   psBlock->Header[1] = 0;

   //ModuleID
   psBlock->Header[2] = 0;
   psBlock->Header[3] = psBlock->u8TableId;

   //HFI
   psBlock->Header[4] = 0x22;
   psBlock->Header[5] = 0;

   //PartNumber/AlphaCode
   psBlock->Header[6] = 0; //MSB
   psBlock->Header[7] = 0;
   psBlock->Header[8] = 0;
   psBlock->Header[9] = 0; //LSB
   psBlock->Header[10] = 'A';
   psBlock->Header[11] = 'A';

   //CalFormID
   psBlock->Header[12] = 0;
   psBlock->Header[13] = 0;
}

static tVoid vCreateDefaultBlock(tsDiagEOLBlock* psBlock, tU8 u8Defaults)
{
   ETG_TRACE_USR3_THR(("---> vCreateDefaultBlock"));

    tU32 i = 0;
    const EOLLib_Entry* pEntries = NULL;
    void* pModuleHandle = NULL;

    //OSAL_pvMemorySet(psBlock->pu8Data, 0, psBlock->u16Length);
	memset(psBlock->pu8Data,0x00,U8_EOLLIB_MAX_BLOCK_LENGTH);

	OSAL_tIODescriptor hReg = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL", OSAL_EN_READONLY);
	if(hReg != OSAL_ERROR)
	{
	   OSAL_trIOCtrlRegistry   rReg;
	   tU8                     szModuleName[256];

	    rReg.pcos8Name = (tS8*) "SHARED_PATH";
	    rReg.u32Size   = sizeof(szModuleName);
	    rReg.ps8Value  = (tPU8) szModuleName;
	    rReg.s32Type   = OSAL_C_S32_VALUE_STRING;

	    if(OSAL_s32IOControl(hReg, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&rReg) != OSAL_ERROR)
	    {
	       OSAL_szStringConcat(szModuleName, "libshared_eoldata_so.so");

	       pModuleHandle = dlopen(szModuleName, RTLD_NOW);

	       if (pModuleHandle == NULL)
	       {
             ETG_TRACE_ERR_THR(("!!! vCreateDefaultBlock => ERROR: dlopen()='%s'",dlerror()));
	       }
	    }
	    OSAL_s32IOClose(hReg);
	}

	if (pModuleHandle != NULL)
	{
    switch (psBlock->u8TableId)
    {
        case EOLLIB_TABLE_ID_SYSTEM:
        {
           pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arSystem"));
           break;
        }

        case EOLLIB_TABLE_ID_DISPLAY_INTERFACE:
        {
            pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arDisplay"));
            break;
        }

        case EOLLIB_TABLE_ID_BLUETOOTH:
        {
            pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arBluetooth"));
            break;
        }

        case EOLLIB_TABLE_ID_NAVIGATION_SYSTEM:
        {
            pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arNavSystem"));
            break;
        }

        case EOLLIB_TABLE_ID_NAVIGATION_ICON:
        {
            pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arNavIcon"));
            break;
        }

        case EOLLIB_TABLE_ID_BRAND:
        {
            pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arBrand"));
            break;
        }

        case EOLLIB_TABLE_ID_COUNTRY:
        {
            pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arCountry"));
            break;
        }

        case EOLLIB_TABLE_ID_SPEECH_RECOGNITION:
        {
            pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arSpeechRec"));
            break;
        }

        case EOLLIB_TABLE_ID_HAND_FREE_TUNING:
        {
            pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arHfTuning"));
            break;
        }

        case EOLLIB_TABLE_ID_REAR_VISION_CAMERA:
        {
            pEntries = (EOLLib_Entry*)(dlsym(pModuleHandle, (const char*)"EOLLib_arRVC"));
            break;
        }

        default:
        {
           break;
        }
    } //switch (psBlock->u8TableId)

       if (pEntries == NULL)
       {
          ETG_TRACE_ERR_THR(("!!! vCreateDefaultBlock => ERROR: dlsym()='%s'",dlerror()));
       }

//       dlclose(pModuleHandle);
//       pModuleHandle = NULL;
	} //if (pModuleHandle != NULL)

    if (pEntries != NULL)
    {
         for (i = 0; pEntries[i].address != NULL; i++)
         {

            memcpy(psBlock->pu8Data + pEntries[i].offset, 
               pEntries[i].address, 
               pEntries[i].size);
         }
    }

    s32WriteBlock(psBlock);

   ETG_TRACE_USR3_THR(("<--- vCreateDefaultBlock"));
}

static tS32 u32ReadBlock(tsDiagEOLBlock* psBlock)
{
    ETG_TRACE_USR3_THR(("---> u32ReadBlock"));

    tS32 s32ReturnValue = 0;
    tS32 s32BytesRead = 0;

    psBlock->hDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READONLY);

    if (psBlock->hDev == OSAL_ERROR)
    {
		// Open FFD error 
      ETG_TRACE_ERR_THR(("--- u32ReadBlock: OSAL_IOOpen on dev_ffd FAILED!!!"));

      //NORMAL_M_ASSERT(0);

      s32ReturnValue = -1;
    }
    else
    {
      OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
      VtsFFDDevInfo.u8DataSet=psBlock->enDataSet;
      VtsFFDDevInfo.pvArg=psBlock->Header;
      if(OSAL_s32IORead(psBlock->hDev,(tPS8)&VtsFFDDevInfo,sizeof(psBlock->Header))!=sizeof(psBlock->Header))
		{
			// Header Read Error 
         ETG_TRACE_ERR_THR(("--- u32ReadBlock: OSAL_s32IORead on dev_ffd for Header Data FAILED!!!"));

         //NORMAL_M_ASSERT(0);

         OSAL_s32IOClose(psBlock->hDev);

         s32ReturnValue = -1;
		}
        else
        {
           VtsFFDDevInfo.u8DataSet=psBlock->enDataSet|0x80;
           VtsFFDDevInfo.pvArg=(void*)psBlock->pu8Data;
           s32BytesRead=OSAL_s32IORead(psBlock->hDev,(tPS8)&VtsFFDDevInfo,psBlock->u16Length);
           if (s32BytesRead!=psBlock->u16Length)
           {
			     // Block Read Error 
              ETG_TRACE_ERR_THR(("--- u32ReadBlock: OSAL_s32IORead on dev_ffd for Block Data FAILED!!!"));

              //NORMAL_M_ASSERT(0);

              OSAL_s32IOClose(psBlock->hDev);

              s32ReturnValue = -1;
		     }
		     else
		     {
              //Reading successful 
              OSAL_s32IOClose(psBlock->hDev);
		     }          
        }
    }

    ETG_TRACE_COMP_THR(("--- u32ReadBlock: Table:%u Len:%u Got:%u Data:%*x",
      ETG_CENUM(EOLLib_tenTable, psBlock->u8TableId), 
      psBlock->u16Length, 
      s32BytesRead,
      ETG_LIST_LEN((s32BytesRead > 200)? 200: s32BytesRead), 
      ETG_LIST_PTR_T8(psBlock->pu8Data) ));

    ETG_TRACE_USR3_THR(("<--- u32ReadBlock"));
    return s32ReturnValue;
}

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

tS32 DRV_DIAG_EOL_s32IODeviceInit(tVoid)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IODeviceInit"));

    tS32 s32Result = OSAL_E_NOERROR;


	DRV_DIAG_EOL_vTraceRegChannel();

    /* create shared memory */
    if(s32Result == OSAL_E_NOERROR)
    {
        vsEOLLibHandleSharedMemory = OSAL_SharedMemoryCreate(EOLLIB_C_SHMEM_NAME, OSAL_EN_READWRITE, sizeof(tsEOLLIBSharedMemory));

        if(vsEOLLibHandleSharedMemory == OSAL_ERROR)
        { /*check if error not error "memory already exist"*/
            s32Result = OSAL_u32ErrorCode();
            if(s32Result != OSAL_E_ALREADYEXISTS)
            {/* assert */
                ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IODeviceInit: s32Result=%d on OSAL_SharedMemoryCreate", s32Result));
                NORMAL_M_ASSERT_ALWAYS();
            }
            else
            {/* memory create ALREADYEXISTS; get now valid handle*/
                vsEOLLibHandleSharedMemory = OSAL_SharedMemoryOpen(EOLLIB_C_SHMEM_NAME, OSAL_EN_READWRITE);
                if(vsEOLLibHandleSharedMemory == OSAL_ERROR)
                {
                    s32Result = OSAL_u32ErrorCode();
                    ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IODeviceInit: s32Result=%d on OSAL_SharedMemoryOpen", s32Result));

                }
                else
                {

                    s32Result = OSAL_E_NOERROR;

                }
            }
        }
    }
	if(s32Result==OSAL_E_NOERROR)
	{
		// lock and get memory
		tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

	//VsShMemEOLLIB->bShMemInitiated = 0;
		//TSH_TP(0x01);
		//clear data in shared memory
		if (VsShMemEOLLIB != NULL)	
		{
			//TSH_TP(0x02);
			memset(VsShMemEOLLIB->sDiagEOLBlocks,0x00,sizeof(VsShMemEOLLIB->sDiagEOLBlocks));
			//init EOL Blocks
			vInitEOLBlocks(VsShMemEOLLIB);
			//Read data from FFS and save it into shared memory
			vRestoreEOLBlocks(VsShMemEOLLIB);
		}
		else
			s32Result = OSAL_ERROR;
		

		if (s32Result == OSAL_E_NOERROR)
		{
			//TSH_TP(0x03);
			
			VsShMemEOLLIB->u8ShMemInitiated = EOLLIB_INIT_MAGIC;

		}
	

		  vEOLLIBUnLock(VsShMemEOLLIB);
	}

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IODeviceInit"));

   return(s32Result);
}





static tVoid vInitEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
	if (VsShMemEOLLIB != NULL)
	{
		vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[0]), EOLLIB_TABLE_ID_SYSTEM,          
                    (tPCS8) EOLLIB_TABLE_NAME_SYSTEM, EN_FFD_DATA_SET_EOL_SYSTEM,
                    EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);
          
          vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[1]), EOLLIB_TABLE_ID_DISPLAY_INTERFACE,   
                    (tPCS8) EOLLIB_TABLE_NAME_DISPLAY_INTERFACE, EN_FFD_DATA_SET_EOL_DISPLAY,
                    EOLLIB_OFFSET_CAL_HMI_F_B_END);
          
          vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[2]), EOLLIB_TABLE_ID_BLUETOOTH,         
                    (tPCS8) EOLLIB_TABLE_NAME_BLUETOOTH, EN_FFD_DATA_SET_EOL_BLUETOOTH,
                    EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END);
          
          vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[3]), EOLLIB_TABLE_ID_NAVIGATION_SYSTEM,       
                    (tPCS8) EOLLIB_TABLE_NAME_NAVIGATION_SYSTEM, EN_FFD_DATA_SET_EOL_NAV_SYSTEM,
                    EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END);

          vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[4]), EOLLIB_TABLE_ID_NAVIGATION_ICON,         
                    (tPCS8) EOLLIB_TABLE_NAME_NAVIGATION_ICON, EN_FFD_DATA_SET_EOL_NAV_ICON,
                    EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END);

          vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[5]), EOLLIB_TABLE_ID_BRAND,        
                    (tPCS8) EOLLIB_TABLE_NAME_BRAND, EN_FFD_DATA_SET_EOL_BRAND,
                    EOLLIB_OFFSET_CAL_HMI_BRAND_END);

          vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[6]), EOLLIB_TABLE_ID_COUNTRY,          
                    (tPCS8) EOLLIB_TABLE_NAME_COUNTRY, EN_FFD_DATA_SET_EOL_COUNTRY,
                    EOLLIB_OFFSET_CAL_HMI_COUNTRY_END);

          vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[7]), EOLLIB_TABLE_ID_SPEECH_RECOGNITION,            
                    (tPCS8) EOLLIB_TABLE_NAME_SPEECH_RECOGNITION, EN_FFD_DATA_SET_EOL_SPEECH_REC,
                    EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END);

          vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[8]), EOLLIB_TABLE_ID_HAND_FREE_TUNING,            
                    (tPCS8) EOLLIB_TABLE_NAME_HAND_FREE_TUNING, EN_FFD_DATA_SET_EOL_HF_TUNING,
                    EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END);

          vInitBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[9]), EOLLIB_TABLE_ID_REAR_VISION_CAMERA,            
                    (tPCS8) EOLLIB_TABLE_NAME_REAR_VISION_CAMERA, EN_FFD_DATA_SET_EOL_RVC,
                    EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END);
		
	}
}

static tVoid vRestoreEOLBlocks(tsEOLLIBSharedMemory* VsShMemEOLLIB)
{
   tBool bResult = TRUE;

	if (VsShMemEOLLIB!=NULL)
	{ 
		tU32 i;
		for (i=0;i<EOL_BLOCK_NBR;i++)
		{
			if (0 != u32ReadBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[i])))
         {
      		vRestoreBlock(&(VsShMemEOLLIB->sDiagEOLBlocks[i]));

            bResult = FALSE;
         }
		}

      if (bResult == FALSE)
      {
         vSaveNow();
      }
	}
}
tS32 DRV_DIAG_EOL_s32IODeviceRemove(tVoid)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IODeviceRemove"));

   tS32 s32Result = OSAL_E_NOERROR;

   /*close and delete shared memory*/
   OSAL_s32SharedMemoryClose(vsEOLLibHandleSharedMemory);
   OSAL_s32SharedMemoryDelete(EOLLIB_C_SHMEM_NAME);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IODeviceRemove"));

   return(s32Result);
}

/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_IOOpen
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_IOOpen(tVoid)
 * ARGUMENTS   : 
 *               tVoid 
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_IOOpen( tVoid )
{
   tS32 s32Result = OSAL_E_NOERROR;

   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_IOOpen"));


   {

	   vsEOLLibHandleSharedMemory =OSAL_SharedMemoryOpen(EOLLIB_C_SHMEM_NAME,OSAL_EN_READWRITE);
	   if(vsEOLLibHandleSharedMemory == OSAL_ERROR)
	   {
		   s32Result = OSAL_u32ErrorCode();
		   ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_IOOpen: s32Result=%d on OSAL_SharedMemoryOpen", s32Result));
	   }
	   else
	   {
		   tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

		   if (VsShMemEOLLIB != NULL)
		   {
               if (VsShMemEOLLIB->u8ShMemInitiated != EOLLIB_INIT_MAGIC)
			   {

				   ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_IOOpen: VsShMemEOLLIB->u8ShMemInitiated=0x%x", 
								  VsShMemEOLLIB->u8ShMemInitiated));
 
					s32Result = OSAL_E_CANCELED;



			   }
 
			   vEOLLIBUnLock(VsShMemEOLLIB);
		   }
	   }
   }

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_IOOpen"));

   return(s32Result);
} /* DRV_DIAG_EOL_IOOpen */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IOClose
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IOClose(tVoid)
 * ARGUMENTS   : 
 *               tVoid 
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IOClose( tVoid )
{
   tS32 s32Result = OSAL_E_NOERROR;
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IOClose"));

   OSAL_s32SharedMemoryClose(vsEOLLibHandleSharedMemory);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IOClose"));

   return(s32Result);
} /* DRV_DIAG_EOL_s32IOClose */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IOControl
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IOControl(tS32 s32fun,tS32 s32arg)
 * ARGUMENTS   : 
 *               tS32 s32fun
 *               tS32 s32arg
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IOControl(tS32 s32fun, tS32 s32arg)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IOControl"));

   tS32 s32Result = OSAL_E_CANCELED;

     tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

   switch (s32fun)
   {
     case OSAL_C_S32_IOCTRL_DIAGEOL_GET_MODULE_IDENTIFIER:
     {
         OSAL_trDiagEOLModuleIdentifier* pArg = (OSAL_trDiagEOLModuleIdentifier*) s32arg;

         if (NULL == pArg)
         {
			 s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
			 tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pArg->u8Table);

             if (psBlock == NULL)
             {
                 // requested Table is not supported 
                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
                

				 pArg->u32PartNumber = 
					 (psBlock->Header[6] << 24) | 
					 (psBlock->Header[7] << 16) |
					 (psBlock->Header[8] << 8) |
					 psBlock->Header[9];

				 pArg->u8DesignLevelSuffix[0] = psBlock->Header[10];
				 pArg->u8DesignLevelSuffix[1] = psBlock->Header[11];

		         s32Result = OSAL_E_NOERROR;
			 }
         }         

		 break;
	 }

     case OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC:
     {
         tU32 i;
         tU32 u32Crc = 0;
         OSAL_trDiagEOLEntry* pArg = (OSAL_trDiagEOLEntry*)(void*) s32arg;

         if (pArg->pu8EntryData == NULL)
         {
             s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
             tU8* pu8Data;
             tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pArg->u8Table);

             if (psBlock == NULL)
             {
                 //requested Table is not supported 
                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
                

                 //Build the checksum over the header
                  i = sizeof(psBlock->Header)-2;
                  pu8Data = &psBlock->Header[2];
                  while(i > 1)
	               {
		               u32Crc += 0xFFFF & ((pu8Data[1]<<8) | pu8Data[0]);
		               pu8Data+=2;
		               i -= 2;
	               }

                  //Add trailing byte
	               if (i)
	               {
		               u32Crc += (0xFF & pu8Data[0]);
	               }

                  //Build the checksum over the data block
                  i = psBlock->u16Length;
                  pu8Data = &psBlock->pu8Data[0];
                  while(i > 1)
	               {
		               u32Crc += 0xFFFF & ((pu8Data[1]<<8) | pu8Data[0]);
		               pu8Data+=2;
		               i -= 2;
	               }

                  //Add trailing byte
	               if (i)
	               {
		               u32Crc += (0xFF & pu8Data[0]);
	               }

#if 0
                  //1's complement
                  //Add carry bits
	               while (u32Crc>>16)
	               {
		               u32Crc = (u32Crc & 0xFFFF)+(u32Crc >> 16);
	               }
                  u32Crc ^= 0xFFFF;
#else
                  //2's complement
                  u32Crc &= 0xFFFF;
                  u32Crc ^= 0xFFFF;
                  u32Crc++;
#endif

                 pArg->pu8EntryData[0] = psBlock->Header[0];
                 pArg->pu8EntryData[1] = psBlock->Header[1];
                 pArg->pu8EntryData[2] = u32Crc & 0xFF;
                 pArg->pu8EntryData[3] = (u32Crc >> 8) & 0xFF;

                 s32Result = OSAL_E_NOERROR;

                 ETG_TRACE_COMP_THR(("--- OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC: Table:%u Header CRC=0x%x Calc CRC=0x%x",
                    pArg->u8Table, 
                    (psBlock->Header[1] << 8) | psBlock->Header[0],
                    u32Crc));
             }
         }

        break;
     }

     default:
       break;
   }

   vEOLLIBUnLock(VsShMemEOLLIB);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IOControl"));

   return(s32Result);
} /* DRV_DIAG_EOL_s32IOControl */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IOWrite
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IOWrite(tPCS8 ps8Buffer,tU32 u32nbytes)
 * ARGUMENTS   : 
 *               tPCS8 ps8Buffer
 *               tU32 u32nbytes
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IOWrite(tPCS8 ps8Buffer, tU32 u32nbytes)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IOWrite"));

   tS32 s32Result = OSAL_E_NOERROR;

  tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

   if ((ps8Buffer == NULL) || (u32nbytes != sizeof(OSAL_trDiagEOLEntry)))
   {
       ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IOWrite: INVALID Read Parameter!!!"));

       s32Result = OSAL_E_INVALIDVALUE;
   }
   else 
   {
     
         OSAL_trDiagEOLEntry* pEntry = (OSAL_trDiagEOLEntry*)(void*) ps8Buffer;

         ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IOWrite: Table:%u Offset:%u Len:%u",
            ETG_CENUM(EOLLib_tenTable, pEntry->u8Table), 
            pEntry->u16Offset, pEntry->u16EntryLength));

         if (pEntry->pu8EntryData == NULL)
         {
             ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IOWrite: pu8EntryData is NULL POINTER!!!"));

             s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
             tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pEntry->u8Table);

             if (psBlock == NULL)
             {
                 ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IOWrite: Table NOT SUPPORTED!!!"));

                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
                
				 if (pEntry->u16EntryLength == 0) 
				 {
					u32nbytes = sizeof(psBlock->Header);

					 memcpy(psBlock->Header, 
									   pEntry->pu8EntryData, 
									   u32nbytes);

					 s32WriteBlock(psBlock);

                vSaveNow();
				 }
				 else
				 {
                     if (pEntry->u16Offset >= psBlock->u16Length)
                     {
                         ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IOWrite: INVALID Offset!!!"));

                         s32Result = OSAL_E_INVALIDVALUE;
                     }
                     else
                     {
					     u32nbytes = pEntry->u16EntryLength;
					     if (u32nbytes > (psBlock->u16Length - pEntry->u16Offset))
					     {
						     u32nbytes = psBlock->u16Length - pEntry->u16Offset;
					     }

						     memcpy(psBlock->pu8Data + pEntry->u16Offset, 
										       pEntry->pu8EntryData, 
										       u32nbytes);

						     s32WriteBlock(psBlock);

                       vSaveNow();
                 }
				 }
             }

         }

         if (s32Result == OSAL_E_NOERROR)
         {
            ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IOWrite: Size:%d Data:%*x",
               u32nbytes, 
               ETG_LIST_LEN((u32nbytes > 200)? 200: u32nbytes), 
               ETG_LIST_PTR_T8(pEntry->pu8EntryData) ));
         }
   }

   vEOLLIBUnLock(VsShMemEOLLIB);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IOWrite"));

   return (s32Result == OSAL_E_NOERROR)? u32nbytes: s32Result;
} /* DRV_DIAG_EOL_s32IOWrite */


/******************************************************FunctionHeaderBegin******
 * FUNCTION    : DRV_DIAG_EOL_s32IORead
 * CREATED     : 2007-05-08
 * AUTHOR      : 
 * DESCRIPTION :   -
 * SYNTAX      : tS32 DRV_DIAG_EOL_s32IORead(tPCS8 ps8Buffer,tU32 u32nbytes)
 * ARGUMENTS   : 
 *               tPCS8 ps8Buffer
 *               tU32 u32nbytes
 * RETURN VALUE: 
 *               tS32 -
 * NOTES       :   -
 *******************************************************FunctionHeaderEnd******/

tS32 DRV_DIAG_EOL_s32IORead(tPCS8 ps8Buffer, tU32 u32nbytes)
{
   ETG_TRACE_USR3_THR(("---> DRV_DIAG_EOL_s32IORead"));

   tS32 s32Result = OSAL_E_NOERROR;

   tsEOLLIBSharedMemory* VsShMemEOLLIB=psEOLLIBLock();

   if ((ps8Buffer == NULL) || (u32nbytes != sizeof(OSAL_trDiagEOLEntry)))
   {
       ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IORead: INVALID Read Parameter!!!"));

       s32Result = OSAL_E_INVALIDVALUE;
   }
   else 
   {
     
         OSAL_trDiagEOLEntry* pEntry = (OSAL_trDiagEOLEntry*)(void*) ps8Buffer;

         ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IORead: Table:%u Offset:%u Len:%u",
            ETG_CENUM(EOLLib_tenTable, pEntry->u8Table), 
            pEntry->u16Offset, 
            pEntry->u16EntryLength));

         if (pEntry->pu8EntryData == NULL)
         {
             ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IORead: pu8EntryData is NULL POINTER!!!"));

             s32Result = OSAL_E_INVALIDVALUE;
         }
         else
         {
             tsDiagEOLBlock* psBlock = psGetBlock(VsShMemEOLLIB, pEntry->u8Table);

             if (psBlock == NULL)
             {
                 ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IORead: Table NOT SUPPORTED!!!"));

                 s32Result = OSAL_E_INVALIDVALUE;
             }
             else
             {
				 if (pEntry->u16EntryLength == 0) 
				 {
                     u32nbytes = sizeof(psBlock->Header);

					 memcpy(pEntry->pu8EntryData,
                                       psBlock->Header, 
									   u32nbytes);
				 }
				 else
				 {
                     if (pEntry->u16Offset >= psBlock->u16Length)
                     {
                         ETG_TRACE_ERR_THR(("--- DRV_DIAG_EOL_s32IORead: INVALID Offset (%d/%d) in table %d!!!",
                               pEntry->u16Offset, psBlock->u16Length, pEntry->u8Table));

                         s32Result = OSAL_E_INVALIDVALUE;
                     }
                     else
                     {
                         u32nbytes = pEntry->u16EntryLength;
                         if (u32nbytes > (psBlock->u16Length - pEntry->u16Offset))
                         {
                             u32nbytes = psBlock->u16Length - pEntry->u16Offset;
                         }

                         memcpy(pEntry->pu8EntryData,
                               psBlock->pu8Data + pEntry->u16Offset, 
                               u32nbytes);
                     }
                 }
             }
         }

         if (s32Result == OSAL_E_NOERROR)
         {
            ETG_TRACE_COMP_THR(("--- DRV_DIAG_EOL_s32IORead: Size:%d Data:%*x",
               u32nbytes, 
               ETG_LIST_LEN((u32nbytes > 200)? 200: u32nbytes), 
               ETG_LIST_PTR_T8(pEntry->pu8EntryData) ));
         }
         else
         {
            ETG_TRACE_ERR_THR(("!!! DRV_DIAG_EOL_s32IORead: s32Result=%d", 
               s32Result));
         }
   }

   vEOLLIBUnLock(VsShMemEOLLIB);

   ETG_TRACE_USR3_THR(("<--- DRV_DIAG_EOL_s32IORead"));

   return (s32Result == OSAL_E_NOERROR)? u32nbytes: s32Result;
} // DRV_DIAG_EOL_s32IORead 



/***********************************************************************
 * static       tsEOLLIBSharedMemory*        psEOLLIBLock(void)
 *    	
 * This function gives a pointer to the start of the shared memory and 
 * locks the memory with the internal mutex. 
 *
 * @return  
 *    tsEOLLIBSharedMemory*  pointer of shared memory
 *
 * @date    15.11.2010
 *
 * @note
 *
 **********************************************************************/
static tsEOLLIBSharedMemory*  psEOLLIBLock(void)
{
   tsEOLLIBSharedMemory* VtsShMemEOLLIB=NULL;

   ETG_TRACE_USR3_THR(("---> psEOLLIBLock"));
   {
       VtsShMemEOLLIB = (tsEOLLIBSharedMemory*)
          OSAL_pvSharedMemoryMap(vsEOLLibHandleSharedMemory,OSAL_EN_READONLY,sizeof(tsEOLLIBSharedMemory),0);
       /*check error*/
       if(VtsShMemEOLLIB==NULL)
       {
          ETG_TRACE_ERR_THR(("!!! psEOLLIBLock: OSAL_pvSharedMemoryMap FAILED"));
          NORMAL_M_ASSERT_ALWAYS();
       }

   }
    ETG_TRACE_USR3_THR(("<--- psEOLLIBLock"));

   return(VtsShMemEOLLIB);
}
/***********************************************************************
 * static void vEOLLIBUnLock(tsEOLLIBSharedMemory* PtsShMemEOLLIB)
 *    	
 * This function releases a shared memory pointer and unlocks the internal mutex.
 *
 * @parameter
 *     PtsShMemEOLLIB  Pointer to shared memory 
 *   
 * @date   15.11.2010
 *
 * @note
 *
 **********************************************************************/
static void vEOLLIBUnLock(tsEOLLIBSharedMemory* PtsShMemEOLLIB)
{
   ETG_TRACE_USR3_THR(("---> vEOLLIBUnLock"));

  if(OSAL_s32SharedMemoryUnmap(PtsShMemEOLLIB,sizeof(tsEOLLIBSharedMemory))==OSAL_ERROR)
   {
    ETG_TRACE_ERR_THR(("!!! vEOLLIBUnLock: OSAL_s32SharedMemoryUnmap FAILED"));
    NORMAL_M_ASSERT_ALWAYS();
  }

  ETG_TRACE_USR4_THR(("<--- vEOLLIBUnLock"));
}



tS32 EOLLib_Read(OSAL_trDiagEOLEntry* pEntry)
{
   static tsDiagEOLBlock sBlock;
   tS32 s32Result = OSAL_E_NOERROR;
   tS32 s32BytesRead = 0;
   tU32 u32nbytes = 0;
   
   if ((pEntry == NULL) || (pEntry->pu8EntryData == NULL))
   {
      s32Result = OSAL_E_INVALIDVALUE;
   }
   else
   {
      sBlock.u8TableId = pEntry->u8Table;

      switch (sBlock.u8TableId)
      {
      case EOLLIB_TABLE_ID_SYSTEM:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_SYSTEM;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_SYSTEM_END;
         break;
      case EOLLIB_TABLE_ID_DISPLAY_INTERFACE:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_DISPLAY;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_F_B_END;
         break;
      case EOLLIB_TABLE_ID_BLUETOOTH:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_BLUETOOTH;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END;
         break;
      case EOLLIB_TABLE_ID_NAVIGATION_SYSTEM:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_NAV_SYSTEM;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END;
         break;
      case EOLLIB_TABLE_ID_NAVIGATION_ICON:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_NAV_ICON;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END;
         break;
      case EOLLIB_TABLE_ID_BRAND:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_BRAND;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_BRAND_END;
         break;
      case EOLLIB_TABLE_ID_COUNTRY:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_COUNTRY;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_COUNTRY_END;
         break;
      case EOLLIB_TABLE_ID_SPEECH_RECOGNITION:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_SPEECH_REC;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END;
         break;
      case EOLLIB_TABLE_ID_HAND_FREE_TUNING:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_HF_TUNING;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END;
         break;
      case EOLLIB_TABLE_ID_REAR_VISION_CAMERA:
         sBlock.enDataSet = EN_FFD_DATA_SET_EOL_RVC;
         sBlock.u16Length = EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END;
         break;

      default:
         s32Result = OSAL_E_INVALIDVALUE;
         sBlock.u16Length = 0;
         break;
      }
   }

   if (pEntry->u16Offset >= sBlock.u16Length)
   {
      // requested Offset is out-of-range 
      s32Result = OSAL_E_INVALIDVALUE;
   }

   if (s32Result == OSAL_E_NOERROR)
   {
      //sBlock.hDev = tk_opn_dev((UB*)"ffdlld",TD_UPDATE);
      sBlock.hDev = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFD,OSAL_EN_READONLY);
      if (sBlock.hDev == OSAL_ERROR)
      {
         /* Open FFD error */
         s32Result = OSAL_E_INVALIDVALUE;
      }
      else
      {
         OSAL_trFFDDeviceInfo    VtsFFDDevInfo;
         VtsFFDDevInfo.u8DataSet=sBlock.enDataSet;
         VtsFFDDevInfo.pvArg=sBlock.Header;
         if(OSAL_s32IORead(sBlock.hDev,(tPS8)&VtsFFDDevInfo,sizeof(sBlock.Header))!=sizeof(sBlock.Header))
         //if (E_OK != tk_srea_dev(sBlock.hDev, sBlock.enDataSet,
	     //       (VP)sBlock.Header, sizeof(sBlock.Header), &s32BytesRead))
         {
            // Header Read Error 
            s32Result = OSAL_E_INVALIDVALUE;
         }
         //else if (E_OK != tk_srea_dev(sBlock.hDev, sBlock.enDataSet | FFD_LLD_DN_FILE_POS,
         //      (VP)sBlock.pu8Data, sBlock.u16Length, &s32BytesRead))
         else
         {
            VtsFFDDevInfo.u8DataSet=sBlock.enDataSet|0x80;
            VtsFFDDevInfo.pvArg=(void*)sBlock.pu8Data;
            s32BytesRead=OSAL_s32IORead(sBlock.hDev,(tPS8)&VtsFFDDevInfo,sBlock.u16Length);
            if (s32BytesRead!=sBlock.u16Length)           
            {
              // Block Read Error 
              s32Result = OSAL_E_INVALIDVALUE;
            }
         }
         //tk_cls_dev(sBlock.hDev, 0);
        OSAL_s32IOClose(sBlock.hDev);
      }

      if (s32Result == OSAL_E_NOERROR)
      {
         u32nbytes = pEntry->u16EntryLength;
         if (u32nbytes > (sBlock.u16Length - pEntry->u16Offset))
         {
            u32nbytes = sBlock.u16Length - pEntry->u16Offset;
         }

         memcpy(pEntry->pu8EntryData, 
                    sBlock.pu8Data + pEntry->u16Offset, 
                    u32nbytes);
      }
   }

   return (s32Result == OSAL_E_NOERROR)? u32nbytes: s32Result;
}


#ifdef LOAD_EOL_SO
static sem_t       *initeol_lock;

extern tS32 DEV_FFD_s32IODeviceInit(tVoid);

tS32 eol_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
  return (tU32)(DRV_DIAG_EOL_IOOpen());
}

tS32 eol_drv_io_close(tS32 s32ID, tU32 u32FD)
{
  return DRV_DIAG_EOL_s32IOClose();
}

tS32 eol_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{
  return((tS32)DRV_DIAG_EOL_s32IOControl(s32fun, s32arg));
}

tS32 eol_drv_io_write(tS32 s32ID, tU32 u32FD, tPCS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
  return((tS32)DRV_DIAG_EOL_s32IOWrite(pBuffer, u32Size));
}

tS32 eol_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
  return((tS32)DRV_DIAG_EOL_s32IORead((tPS8)pBuffer, u32Size));
}

void __attribute__ ((constructor)) eol_process_attach(void)
{
   tBool bFirstAttach = FALSE;
   char buffer[100];
  
   //TraceString("KDS attach start");
   initeol_lock = sem_open("EOL_INIT",O_EXCL | O_CREAT, 0660, 0);
   if (initeol_lock != SEM_FAILED)
   {
      bFirstAttach = TRUE;	 
   }
   else
   {
       initeol_lock = sem_open("EOL_INIT", 0);
       bFirstAttach = FALSE;
       if(initeol_lock != SEM_FAILED)
       {
          sem_wait(initeol_lock);
       }
   }
   DEV_FFD_s32IODeviceInit();
   if(bFirstAttach)
   {
      DRV_DIAG_EOL_s32IODeviceInit();
   }
   else
   {
   }
   if(initeol_lock != SEM_FAILED)
   {
      sem_post(initeol_lock);
   }
   else
   {
     vWritePrintfErrmem("EOL sem_open Error %d",errno);
   }
}


void vOnEolProcessDetach( void )
{
}
#endif






#ifdef __cplusplus
}
#endif

/* End of File diag_eol.c                                                */

