
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#include <EOLLib.h>

/*
MOD ID 04 - HMI WIRELESS COMN CALN
*/

/*
*| hmi_wireless_comn_caln.HEADER_BLUETOOTH {
*|      : is_calconst;
*|      : description = "CALDS Header";
*| } 
*/ 
const EOLLib_CalDsHeader HEADER_BLUETOOTH = {0x0000, EOLLIB_TABLE_ID_BLUETOOTH << 8, 0x0000, 0x00000000, {0x41, 0x41}, 0x0401};

/*
*| hmi_wireless_comn_caln.TELEPHONEFBLOCK_ALERT_TONE_DURATION {
*|      : is_calconst;
*|      : description = "After Receiving command to start source from AVManager, SourceActivity.StartResultAck() in response to request for LC_ALERT_TONE channel, \
The Telephone component shall play ALERT_TONE.wav file  for ALERT-TONE-DURATION duration in LC_ALERT_TONE channel (alert tone heard in the vehicle) periodically for ALERT-TONE-COUNT times until:\
a) Call is accepted: Follow GIS-366-2420 Incoming Call: AcceptCall\
b) Call is ignored: Follow GIS-366-2430 Incoming Call IgnoreCall\
c) Or Call rolls over to voicemail due to no action: Follow GIS-366-2430 Incoming Call :IgnoreCall\
";
*|      : units = "ms";
*|      : transform = fixed.TELEPHONEFBLOCK_ALERT_TONE_DURATION;
*| } 
*/ 
const char TELEPHONEFBLOCK_ALERT_TONE_DURATION = 0x7F;

/*
*| hmi_wireless_comn_caln.TELEPHONEFBLOCK_PERIODIC_WAIT_TIME {
*|      : is_calconst;
*|      : description = "After Receiving command to start source from AVManager, SourceActivity.StartResultAck() in response to request for LC_ALERT_TONE channel, \
The Telephone component shall play ALERT_TONE.wav file  for ALERT-TONE-DURATION duration in LC_ALERT_TONE channel (alert tone heard in the vehicle) periodically for ALERT-TONE-COUNT times until:\
a) Call is accepted: Follow GIS-366-2420 Incoming Call: AcceptCall\
b) Call is ignored: Follow GIS-366-2430 Incoming Call IgnoreCall\
c) Or Call rolls over to voicemail due to no action: Follow GIS-366-2430 Incoming Call :IgnoreCall\
";
*|      : units = "ms";
*|      : transform = fixed.TELEPHONEFBLOCK_PERIODIC_WAIT_TIME;
*| } 
*/ 
const char TELEPHONEFBLOCK_PERIODIC_WAIT_TIME = 0x0A;

/*
*| hmi_wireless_comn_caln.TELEPHONE_FBLOCK_ALERT_TONE_COUNT {
*|      : is_calconst;
*|      : description = "After Receiving command to start source from AVManager, SourceActivity.StartResultAck() in response to request for LC_ALERT_TONE channel, \
The Telephone component shall play ALERT_TONE.wav file  for ALERT-TONE-DURATION duration in LC_ALERT_TONE channel (alert tone heard in the vehicle) periodically for ALERT-TONE-COUNT times until:\
a) Call is accepted: Follow GIS-366-2420 Incoming Call: AcceptCall\
b) Call is ignored: Follow GIS-366-2430 Incoming Call IgnoreCall\
c) Or Call rolls over to voicemail due to no action: Follow GIS-366-2430 Incoming Call :IgnoreCall\
";
*|      : units = "UNM";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TELEPHONE_FBLOCK_ALERT_TONE_COUNT = 0x01;

/*
*| hmi_wireless_comn_caln.TELEPHONE_FBLOCK_RETRY_COUNT {
*|      : is_calconst;
*|      : description = "If the Telephone FBlock receives RequestAVActivation.ErrorAck(), in response to request for LC_ALERT_TONE channel, it shall:\
a) Retry for LC_ALERT_TONE channel for RETRY-COUNT count pausing PAUSE-DURATION duration after each try.\
b) If it is unable to acquire LC_ALERT_TONE channel it shall:\
a. Transfer call to voicemail.\
";
*|      : units = "UNM";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TELEPHONE_FBLOCK_RETRY_COUNT = 0x02;

/*
*| hmi_wireless_comn_caln.TELEPHONE_FBLOCK_PAUSE_DURATION {
*|      : is_calconst;
*|      : description = "If the Telephone FBlock receives RequestAVActivation.ErrorAck(), in response to request for LC_ALERT_TONE channel, it shall:\
a) Retry for LC_ALERT_TONE channel for RETRY-COUNT count pausing PAUSE-DURATION duration after each try.\
b) If it is unable to acquire LC_ALERT_TONE channel it shall:\
a. Transfer call to voicemail.\
";
*|      : units = "ms";
*|      : transform = fixed.TELEPHONE_FBLOCK_PAUSE_DURATION;
*| } 
*/ 
const char TELEPHONE_FBLOCK_PAUSE_DURATION = 0x02;

/*
*| hmi_wireless_comn_caln.BT_DISCOVERABLE_MODE_TIMER {
*|      : is_calconst;
*|      : description = "If the discoverable mode timer expires the Bluetooth Module shall notify the HMI and exit the Pairing Process.\
\
When a Bluetooth device is not detected within a certain\
amount of time, a popup is displayed. The user may\
tap retry and the system will go to discoverable mode\
again with the instructions for pairing displayed. The\
user may tap cancel, or the popup display will time out\
and return to the application view that Add New Device\
was initiated from.";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char BT_DISCOVERABLE_MODE_TIMER = 0xB4;

const unsigned short BLUETOOTH_DUMMY_1 = 0x0000;

/*
*| hmi_wireless_comn_caln.BT_CONNECTION_REQUEST_TIMER {
*|      : is_calconst;
*|      : description = "If a connection request is not received within 60sec, the Bluetooth Module shall send a notification to the HMI and exit the Pairing Process.";
*|      : units = "seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char BT_CONNECTION_REQUEST_TIMER = 0x3C;

/*
*| hmi_wireless_comn_caln.BT_IN_BAND_RINGING {
*|      : is_calconst;
*|      : description = "The Bluetooth Module shall be able to turn support for in-band ringing on or off from either the HMI or through a calibration. If the In-Band Ringing calibration parameter is not enabled the Bluetooth Module shall not support in-band ringing by muting the in-band ring tone from the AG (i.e. the Audio Connection) and use an internally generated ring tone in response to the RING alert sent by the AG.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char BT_IN_BAND_RINGING = 0x01;

/*
*| hmi_wireless_comn_caln.BT_AUDIO_VOICE_CONNECTION_TYPE {
*|      : is_calconst;
*|      : description = "The Bluetooth Module shall be able to support a calibration that utilizes eith eSCO or SCO for the Bluetooth Phone Audio Voice Connection Type.";
*|      : units = "Connection Type";
*|      : type = fixed.UB0;
*| } 
*/ 
const char BT_AUDIO_VOICE_CONNECTION_TYPE = 0x00;

/*
*| hmi_wireless_comn_caln.DEFAULT_BT_UPDATE_INBOX_POLLING_PERIOD {
*|      : is_calconst;
*|      : description = "The UpdateInbox function shall be initially sent to the MSE upon establishing a MAP session and on a periodic basis throughout the rest of the MAP session. The period shall be set by default to 5 minutes, but shall be adjustable based on user selection.";
*|      : units = "Seconds";
*|      : transform = fixed.DEFAULT_BT_UPDATE_INBOX_POLLING_PERIOD;
*| } 
*/ 
const char DEFAULT_BT_UPDATE_INBOX_POLLING_PERIOD = 0x1E;

/*
*| hmi_wireless_comn_caln.BT_RECORDING_TIME_LIMIT {
*|      : is_calconst;
*|      : description = "The Bluetooth Module recording time limit shall be set via a calibration. The default calibration value shall be 5 minutes.";
*|      : units = "Seconds";
*|      : transform = fixed.BT_RECORDING_TIME_LIMIT;
*| } 
*/ 
const char BT_RECORDING_TIME_LIMIT = 0x1E;

/*
*| hmi_wireless_comn_caln.ENABLE_BLUETOOTH {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_BLUETOOTH = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_HANDS_FREE_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_HANDS_FREE_PROFILE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_PHONE_BOOK_ACCESS_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_PHONE_BOOK_ACCESS_PROFILE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_MESSAGING_ACCESS_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MESSAGING_ACCESS_PROFILE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_GENERIC_AUDIO_VIDEO_DISTRIBUTION_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_GENERIC_AUDIO_VIDEO_DISTRIBUTION_PROFILE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_ADVANCED_AUDIO_DISTRIBUTION_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_ADVANCED_AUDIO_DISTRIBUTION_PROFILE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_AUDIO_VIDEO_REMOTE_CONTROL_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIO_VIDEO_REMOTE_CONTROL_PROFILE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_PORTABLE_NAVIGATION_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_PORTABLE_NAVIGATION_PROFILE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_DIAL_UP_NETWORKING_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DIAL_UP_NETWORKING_PROFILE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_PERSONAL_AREA_NETWORK_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_PERSONAL_AREA_NETWORK_PROFILE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_SERIAL_PORT_PROFILE {
*|      : is_calconst;
*|      : description = "These Calibrations are desired to be able to enable and disable a complete Bluetooth Profile and its related HMI.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_SERIAL_PORT_PROFILE = 0x01;

const char BLUETOOTH_DUMMY_2[33] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_wireless_comn_caln.LOCAL_PHONE_TIMEOUT {
*|      : is_calconst;
*|      : description = "Calibration to limit local Phone mode with a phone call when Igntion Off and ";
*|      : units = "Minutes";
*|      : type = fixed.UB0;
*| } 
*/ 
const char LOCAL_PHONE_TIMEOUT = 0x3C;

/*
*| hmi_wireless_comn_caln.BT_CHAUFFER_MODE_ENABLED {
*|      : is_calconst;
*|      : description = "Bluetooth may be disabled (calibration) for chauffeur\
mode in the front module and only enabled in the rear\
seat module. When Bluetooth is disabled, the Phone\
Application is not accessible. The icon will not be shown\
in the app tray or home page such that the phone app is\
not available.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char BT_CHAUFFER_MODE_ENABLED = 0x00;

/*
*| hmi_wireless_comn_caln.ENABLE_PHONE_AUTOCOMPLETE {
*|      : is_calconst;
*|      : description = "As the user is entering digits, the number is auto completed\
based upon matches to the recent call history. If\
there are multiple matches, the match shown is based\
upon the most recent date. The digits entered by the\
user will be shown visually different from the auto entry\
numbers. The user continues entering digits if the\
autocomplete phone number is not the desired phone\
number. The autocomplete is available when the user is\
not in a call or is in the add call mode.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_PHONE_AUTOCOMPLETE = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_PHONE_KEYPAD_AUTOCOMPLET_DROPDOWN {
*|      : is_calconst;
*|      : description = "If the autocomplete has multiple matches, a dropdown\
arrow is shown after the autocomplete text. This dropdown\
allows the user to see the entire list of matches\
and select the appropriate one without having to enter\
in more digits to obtain that match.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_PHONE_KEYPAD_AUTOCOMPLET_DROPDOWN = 0x01;

/*
*| hmi_wireless_comn_caln.PHONE_INTERACTION_SELECTOR_IN_CALL_VIEW_TIMEOUT {
*|      : is_calconst;
*|      : description = "If the user doesn't tat a button in the interaction selectior withing 5 seconds (Calibratable), the interaction is suppressed with the reveal interaction selector button shown.";
*|      : units = "ms";
*|      : transform = fixed.PHONE_INTERACTION_SELECTOR_IN_CALL_VIEW_TIMEOUT;
*| } 
*/ 
const char PHONE_INTERACTION_SELECTOR_IN_CALL_VIEW_TIMEOUT = 0x32;

/*
*| hmi_wireless_comn_caln.ENABLE_MTP_DEVICES {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_MTP_DEVICES = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_PHONE_STATUS_REGION_BATTERY_LEVEL {
*|      : is_calconst;
*|      : description = "An icon will be displayed to indicate the battery level.\
The icon will have six different states ranging from level\
0 for minimum battery charge to level 5 for maximum\
battery charge";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_PHONE_STATUS_REGION_BATTERY_LEVEL = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_PHONE_STATUS_REGION_ROAMING_INDICATOR {
*|      : is_calconst;
*|      : description = "An icon will be displayed when the Bluetooth device is\
roaming.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_PHONE_STATUS_REGION_ROAMING_INDICATOR = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_PHONE_STATUS_REGION_SIGNAL_STRENGTH {
*|      : is_calconst;
*|      : description = "An icon will be displayed to indicate the signal strength.\
The icon will have six different states ranging from level\
0 for no reception to level 5 for maximum reception.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_PHONE_STATUS_REGION_SIGNAL_STRENGTH = 0x01;

/*
*| hmi_wireless_comn_caln.PHONE_CALL_ENDED_INFORMATION_TIMEOUT {
*|      : is_calconst;
*|      : description = "When the user initiates ending the current call, the current\
caller hangs up, or service connection is lost, while\
in the phone view, the phone status will display call\
ended in addition to the call information for 3 seconds.\
The user may end the call through the center stack display,\
steering wheel controls, or the Bluetooth device.\
The call timer will stop and flash at a rate of TBD (calibratible).\
After the time out, the �call ended� indication\
will no longer be displayed and the caller info region will\
return to not in a call view. The call timer will no longer\
be displayed.";
*|      : units = "ms";
*|      : transform = fixed.PHONE_CALL_ENDED_INFORMATION_TIMEOUT;
*| } 
*/ 
const char PHONE_CALL_ENDED_INFORMATION_TIMEOUT = 0x1E;

const unsigned short BLUETOOTH_DUMMY_3 = 0x0000;

/*
*| hmi_wireless_comn_caln.ENABLE_CALL_TIMERS {
*|      : is_calconst;
*|      : description = "The call info region on the in call view is populated with\
the active call information.\
The Call Timer is shown in call information for all types\
of calls. The Call Timer is shown in HH:MM:SS format.\
The Call Timer value as provided by the Phone F-Block\
is displayed. When two calls are active, the Call Timer is\
shown individually for each call and the time update is\
synchronized.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_CALL_TIMERS = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_V_CARD_PICTURE_ICON {
*|      : is_calconst;
*|      : description = "The call info region, when in a call with the contact\
information known, displays v-card picture or generic\
icon, first and last name, phone type icon, and phone\
number. The font size and icon size is provided in the\
design style guide.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_V_CARD_PICTURE_ICON = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_GENERIC_PICTURE_ICON {
*|      : is_calconst;
*|      : description = "The call info region, when in a call with the contact\
information known, displays v-card picture or generic\
icon, first and last name, phone type icon, and phone\
number. The font size and icon size is provided in the\
design style guide.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_GENERIC_PICTURE_ICON = 0x01;

/*
*| hmi_wireless_comn_caln.ENABLE_THREEWAY_CALL_UNMERGE {
*|      : is_calconst;
*|      : description = "When in a three way call, the call info region displays\
the call information for both calls and indicates that\
both calls are active (hold option is not available). The\
user may tap the unmerge button to exit the three way\
calling session, in which the last call that was active remains\
active and the second call is put on hold.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_THREEWAY_CALL_UNMERGE = 0x01;

/*
*| hmi_wireless_comn_caln.BLUETOOTH_CALL_INITIATION_DELAY_TIME {
*|      : is_calconst;
*|      : description = "The Phone Interaction View with the Contact Details for\
that favorite shown with the calling status and the call\
information populated. The calling information will be\
the contact name / POI name and phone number. The\
initiation of the call to the current outgoing sourced\
Bluetooth phone will be delayed TBD (calibratible) time\
to give the user a chance to end the call in the case it\
was inadvertently initiated\
The initiation of the call the current outgoing sourced Bluetooth Phone will be delayed 3 seconds (calibratable) time to give the user a chance to end the call in the case it was inadvertently initiated.";
*|      : units = "ms";
*|      : transform = fixed.BLUETOOTH_CALL_INITIATION_DELAY_TIME;
*| } 
*/ 
const char BLUETOOTH_CALL_INITIATION_DELAY_TIME = 0x1E;

/*
*| hmi_wireless_comn_caln.FAVORITE_PHONE_CALL_ENDED_TIMEOUT {
*|      : is_calconst;
*|      : description = "When a call that was initiated through a favorite has\
ended and the phone view is still active, it is displayed\
for a calibratible length of time and then reverts to\
the display view that was active when the favorite was\
tapped.";
*|      : units = "ms";
*|      : transform = fixed.FAVORITE_PHONE_CALL_ENDED_TIMEOUT;
*| } 
*/ 
const char FAVORITE_PHONE_CALL_ENDED_TIMEOUT = 0x1E;

/*
*| hmi_wireless_comn_caln.SWMI_CALIBRATION_DATA_FILE_BLUETOOTH_CAL {
*|      : is_calconst;
*|      : description = "The NoCalibration state is defined in order to make sure that Infotainment subsystem components have been updated with calibrations after a service event such as replacing one or more modules or upgrading software.  The mechanism for NoCalibration determination is the same as that for Theftlock and NoVIN determination.  That is, at initialization a module detects whether or not it has a valid calibration.  If it does not, it notifies the SystemState FBlock via the SystemState.SetNoCalibrationModuleState method.  Once a valid calibration is received, the module calls the SystemState.SetNoCalibrationModuleState method to clear the condition.\
The SystemState FBlock shall assume all module calibrations are valid upon each Sleep cycle.  Modules that already have a valid calibration or do not support NoCalibration detection (determined by each module CTS) do not need to report the NoCalibration clear condition at each initialization.\
Note: each of these Calibrations should be ...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SWMI_CALIBRATION_DATA_FILE_BLUETOOTH_CAL = 0x00;

/*
*| hmi_wireless_comn_caln.IGNITION_OFF_IN_CALL_VIEW_DELAY {
*|      : is_calconst;
*|      : description = "When the user turns off ignition with an active bluetooth\
phone call, the display changes to the In Call view\
as long as there has been no inputs by the user on the\
current view for TBD time (calibratible). Otherwise,\
the In Call view is delayed until no activity for TBD time\
(calibratible).";
*|      : units = "ms";
*|      : transform = fixed.IGNITION_OFF_IN_CALL_VIEW_DELAY;
*| } 
*/ 
const char IGNITION_OFF_IN_CALL_VIEW_DELAY = 0x64;

const char BLUETOOTH_DUMMY_4 = 0x00;

const char BLUETOOTH_DUMMY_5 = 0x00;

const char BLUETOOTH_DUMMY_6 = 0x00;

/*
*| hmi_wireless_comn_caln.IN_PHONE_SPEECH_REC_ENABLE {
*|      : is_calconst;
*|      : description = "Add new calibration IN_PHONE_SPEECH_REC_ENABLE Boolean to allow the usage of the phone�s pass-thru internal speech recognition per supplier request.\
$00  = Disable phone�s SR for vehicle.  Phone SR stays on handset.  Application  and icon is removed from the Homescreen.\
$01 = Enable phone SR to use vehicle audio system (Speakers and microphone).  Application and icon is enabled and shown on the homepage.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char IN_PHONE_SPEECH_REC_ENABLE = 0x01;

/*
*| hmi_wireless_comn_caln.CAL_HMI_BLUETOOTH_END {
*|      : is_calconst;
*|      : description = "END OF CAL BLOCK";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CAL_HMI_BLUETOOTH_END = 0x00;
