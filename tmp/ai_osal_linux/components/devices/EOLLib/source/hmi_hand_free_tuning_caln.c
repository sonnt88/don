
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#include <EOLLib.h>

/*
MOD ID 0A - HMI HAND FREE TUNING CALN
*/

/*
*| hmi_hand_free_tuning_caln.HEADER_HFTUNING {
*|      : is_calconst;
*|      : description = "CALDS Header";
*| } 
*/ 
const EOLLib_CalDsHeader HEADER_HFTUNING = {0x0000, EOLLIB_TABLE_ID_HAND_FREE_TUNING << 8, 0x0000, 0x00000000, {0x41, 0x41}, 0x0401};

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF1 {
*|      : is_calconst;
*|      : description = "qwaAECSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF1 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF2 {
*|      : is_calconst;
*|      : description = "qwaAGCSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF2 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF3 {
*|      : is_calconst;
*|      : description = "qwaDiagSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF3 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF4 {
*|      : is_calconst;
*|      : description = "qwaHFESwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF4 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNRSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF5 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF6 {
*|      : is_calconst;
*|      : description = "qwaOpMode";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_CS_COEF6 = 0x00;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF7 {
*|      : is_calconst;
*|      : description = "qwaRECSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF7 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF8 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvBWESwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF9 = 0x00;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvDLCSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF10 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF11 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvEQSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF12 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF13 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF13 = 0x00;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF14 {
*|      : is_calconst;
*|      : description = "qwaSendEQSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF14 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF15 {
*|      : is_calconst;
*|      : description = "qwaWNDBMode";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_CS_COEF15 = 0x00;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF16 {
*|      : is_calconst;
*|      : description = "qwaWNDBSwitch";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char PHFTR_CS_COEF16 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF17 {
*|      : is_calconst;
*|      : description = "qwaAECFiltLen";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_CS_COEF17 = 0x28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF18 {
*|      : is_calconst;
*|      : description = "qwaFrameShiftIn";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF18 = 0x0100;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF19 {
*|      : is_calconst;
*|      : description = "qwaMaxFreqProc";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF19 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF20 {
*|      : is_calconst;
*|      : description = "qwaAnalogMicInCnt";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_CS_COEF20 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF21 {
*|      : is_calconst;
*|      : description = "qwaMOSTMicInCnt";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_CS_COEF21 = 0x00;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCAmpTgt1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF22 = 0xFA23;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCAmpTgt2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF23 = 0xFC7B;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCMaxRate1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF24 = 0x0534;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCMaxRate2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF25 = 0x061A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvInCnt";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_CS_COEF26 = 0x01;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvLimitThresh";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF27 = 0x7FFF;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvSampleRateIn";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF28 = 0x1F40;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRefMaxDelay";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF29 = 0x0064;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRefSampleRateIn";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF30 = 0x1F40;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF31 {
*|      : is_calconst;
*|      : description = "qwaSampleRateIn";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF31 = 0x3E80;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF32 {
*|      : is_calconst;
*|      : description = "qwaSendAGCAmpTgt1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF32 = 0xFA23;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCAmpTgt2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF33 = 0xFC7B;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCMaxRate1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF34 = 0x0534;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCMaxRate2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF35 = 0x061A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendLimitThresh";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF36 = 0x7FFF;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF37 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF37 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF38 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF38 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF39 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF40 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF40 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF41 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF41 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF42 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF42 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF43 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF43 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF44 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF44 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_CS_COEF45 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_CS_COEF45 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP1_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP1_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP1_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP1_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP1_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP1_COEF32 = 0x21;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP1_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP1_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP1_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP2_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP2_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP2_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP2_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP2_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP2_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP2_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP2_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP2_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP3_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP3_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP3_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP3_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP3_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP3_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP3_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP3_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP3_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP4_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP4_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP4_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP4_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP4_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP4_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP4_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP4_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP4_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP5_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP5_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP5_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP5_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP5_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP5_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP5_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP5_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP5_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP6_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP6_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP6_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP6_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP6_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP6_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP6_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP6_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP6_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP7_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP7_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP7_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP7_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP7_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP7_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP7_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP7_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP7_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP8_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP8_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP8_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP8_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP8_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP8_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP8_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP8_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP8_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP9_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP9_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP9_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP9_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP9_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP9_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP9_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP9_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP9_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP10_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP10_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP10_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP10_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP10_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP10_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_PP10_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_PP10_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_PP10_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_DEFAULT_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_DEFAULT_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_DEFAULT_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_DEFAULT_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_DEFAULT_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_DEFAULT_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_DEFAULT_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_DEFAULT_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_DEFAULT_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK1_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK1_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK1_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK1_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK1_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK1_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK1_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK1_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK1_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK2_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK2_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK2_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK2_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK2_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK2_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK2_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK2_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK2_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK3_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK3_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK3_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK3_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK3_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK3_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_NETWORK3_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_NETWORK3_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_NETWORK3_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER1_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER1_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER1_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER1_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER1_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER1_COEF32 = 0x21;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER1_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER1_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER1_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER2_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER2_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER2_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER2_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER2_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER2_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER2_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER2_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER2_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF1 {
*|      : is_calconst;
*|      : description = "qwaHFEExtent";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER3_COEF1 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF2 {
*|      : is_calconst;
*|      : description = "qwaNCParam1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF2 = 0xF1F0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF3 {
*|      : is_calconst;
*|      : description = "qwaNCParam2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF3 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF4 {
*|      : is_calconst;
*|      : description = "qwaNCParam3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF4 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF5 {
*|      : is_calconst;
*|      : description = "qwaNCParam4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF5 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF6 {
*|      : is_calconst;
*|      : description = "qwaNCParam5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF6 = 0x001A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF7 {
*|      : is_calconst;
*|      : description = "qwaNRAtten";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF7 = 0xFA8E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF8 {
*|      : is_calconst;
*|      : description = "qwaRECSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER3_COEF8 = 0x2D;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF9 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF9 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF10 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF10 = 0x02BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF11 {
*|      : is_calconst;
*|      : description = "qwaRecvAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF11 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF12 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEBrightness";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER3_COEF12 = 0x19;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF13 {
*|      : is_calconst;
*|      : description = "qwaRecvBWEWarmth";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER3_COEF13 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF14 {
*|      : is_calconst;
*|      : description = "qwaRecvENSActivityRatio";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF14 = 0x03BC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF15 {
*|      : is_calconst;
*|      : description = "qwaRecvENSSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER3_COEF15 = 0x14;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF16 {
*|      : is_calconst;
*|      : description = "qwaRecvGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF16 = 0x0006;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF17 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF17 = 0x00E0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF18 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF18 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF19 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF19 = 0xFEC5;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF20 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF20 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF21 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF21 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF22 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF22 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF23 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF23 = 0x0D62;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF24 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF24 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF25 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF25 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF26 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF26 = 0x11D8;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF27 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF27 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF28 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF28 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF29 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF29 = 0x164E;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF30 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF30 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF31 {
*|      : is_calconst;
*|      : description = "qwaRecvParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF31 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF32 {
*|      : is_calconst;
*|      : description = "qwaRefDelay";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER3_COEF32 = 0x2A;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF33 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF33 = 0x01F4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF34 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF34 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF35 {
*|      : is_calconst;
*|      : description = "qwaSendAGCGainVal3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF35 = 0x0A28;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF36 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF36 = 0x07D0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF37 {
*|      : is_calconst;
*|      : description = "qwaSendDynEQMinMaxSNR2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF37 = 0x0FA0;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF38 {
*|      : is_calconst;
*|      : description = "qwaSendGain";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF38 = 0xFED2;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF39 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF39 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF40 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF40 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF41 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF41 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF42 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF42 = 0x09C4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF43 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF43 = 0x05DC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF44 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF44 = 0x0258;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF45 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF45 = 0x06B1;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF46 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF46 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF47 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF47 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF48 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF48 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF49 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF49 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF50 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF50 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF51 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF51 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF52 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF52 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF53 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQ2Node15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF53 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF54 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode1";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF54 = 0x0640;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF55 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode2";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF55 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF56 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode3";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF56 = 0xFF38;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF57 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode4";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF57 = 0x0145;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF58 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode5";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF58 = 0x0190;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF59 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode6";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF59 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF60 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode7";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF60 = 0x0271;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF61 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode8";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF61 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF62 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode9";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF62 = 0xFED4;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF63 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode10";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF63 = 0x08EC;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF64 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode11";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF64 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF65 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode12";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF65 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF66 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode13";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF66 = 0x0B27;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF67 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode14";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF67 = 0x0096;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF68 {
*|      : is_calconst;
*|      : description = "qwaSendParamEQNode15";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF68 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF69 {
*|      : is_calconst;
*|      : description = "qwaWNDBSuppress";
*|      : units = "unsigned byte";
*|      : type = fixed.UB0;
*| } 
*/ 
const char PHFTR_VOICE_SERVER3_COEF69 = 0x32;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF70 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF70 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF71 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF71 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF72 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF72 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF73 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF73 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF74 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF74 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF75 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF75 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF76 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF76 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF77 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF77 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF78 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF78 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF79 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF79 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.PHFTR_VOICE_SERVER3_COEF80 {
*|      : is_calconst;
*|      : description = "reserved";
*|      : units = "signed short";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short PHFTR_VOICE_SERVER3_COEF80 = 0x0000;

/*
*| hmi_hand_free_tuning_caln.SWMI_CALIBRATION_DATA_FILE_HF_TUNING_CAL {
*|      : is_calconst;
*|      : description = "The NoCalibration state is defined in order to make sure that Infotainment subsystem components have been updated with calibrations after a service event such as replacing one or more modules or upgrading software.  The mechanism for NoCalibration determination is the same as that for Theftlock and NoVIN determination.  That is, at initialization a module detects whether or not it has a valid calibration.  If it does not, it notifies the SystemState FBlock via the SystemState.SetNoCalibrationModuleState method.  Once a valid calibration is received, the module calls the SystemState.SetNoCalibrationModuleState method to clear the condition.\
The SystemState FBlock shall assume all module calibrations are valid upon each Sleep cycle.  Modules that already have a valid calibration or do not support NoCalibration detection (determined by each module CTS) do not need to report the NoCalibration clear condition at each initialization.\
Note: each of these Calibrations should be ...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SWMI_CALIBRATION_DATA_FILE_HF_TUNING_CAL = 0x00;

/*
*| hmi_hand_free_tuning_caln.CAL_HMI_HF_TUNING_END {
*|      : is_calconst;
*|      : description = "END OF CAL BLOCK";
*|      : units = "";
*|      : type = fixed.UB0;
*| } 
*/ 
const char CAL_HMI_HF_TUNING_END = 0x00;
