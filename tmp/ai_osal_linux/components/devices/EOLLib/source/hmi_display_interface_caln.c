
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#include <EOLLib.h>

/*
MOD ID 03 - HMI DISPLAY INTERFACE CALN
*/

/*
*| hmi_display_interface_caln.HEADER_DISPLAY {
*|      : is_calconst;
*|      : description = "CALDS Header";
*| } 
*/ 
const EOLLib_CalDsHeader HEADER_DISPLAY = {0x0000, EOLLIB_TABLE_ID_DISPLAY_INTERFACE << 8, 0x0000, 0x00000000, {0x41, 0x41}, 0x0401};

/*
*| hmi_display_interface_caln.MAX_CHIME_VOL_STEPS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "steps";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MAX_CHIME_VOL_STEPS = 0x3F;

const char DISPLAY_INTERFACE_DUMMY_1 = 0x00;

/*
*| hmi_display_interface_caln.MAX_SURROUND_LEVEL {
*|      : is_calconst;
*|      : description = "1. The Audio Video Manager shall store a default calibratable value, as defined in GIS-344, for MAX_SURROUND_STEPS.";
*|      : units = "steps";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MAX_SURROUND_LEVEL = 0x0C;

/*
*| hmi_display_interface_caln.MAIN_GROUP_MAX_VOLUME {
*|      : is_calconst;
*|      : description = "MAIN_GROUP_MAX_VOLUME\
UPDATE:  change to 63 decimal which is the full volume range.  This avoids conflicts with tuner module startup volume HMI bar interactions.  Full bar will be shown.";
*|      : units = "steps";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MAIN_GROUP_MAX_VOLUME = 0x3F;

const char DISPLAY_INTERFACE_DUMMY_22 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_23 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_24 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_25 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_26 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_27 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_28 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_29 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_30 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_31 = 0x00;

/*
*| hmi_display_interface_caln.MID_EQ_ENABLE {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char MID_EQ_ENABLE = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_FADE {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean_";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_FADE = 0x01;

/*
*| hmi_display_interface_caln.TIME_SHIFT_ENABLE {
*|      : is_calconst;
*|      : description = "The availability of the timeshift feature shall be controlled by the calibration TIME_SHIFT_ENABLE defined in GIS-344.  When timeshift is disabled, all associated HMI elements shall be removed from the display and all timeshift functions shall be disabled.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char TIME_SHIFT_ENABLE = 0x01;

const char DISPLAY_INTERFACE_DUMMY_2 = 0x00;

/*
*| hmi_display_interface_caln.D_LONG_AUTO_ACK {
*|      : is_calconst;
*|      : description = "The alert will be considered acknowledged when displayed continuously for D_LONG_AUTO_ACK.\
";
*|      : units = "ms";
*|      : transform = fixed.D_LONG_AUTO_ACK;
*| } 
*/ 
const char D_LONG_AUTO_ACK = 0xFA;

/*
*| hmi_display_interface_caln.D_MEDIUM_AUTO_ACK {
*|      : is_calconst;
*|      : description = "The alert will be considered acknowledged when displayed continuously for D_MEDIUM_AUTO_ACK.";
*|      : units = "ms";
*|      : transform = fixed.D_MEDIUM_AUTO_ACK;
*| } 
*/ 
const char D_MEDIUM_AUTO_ACK = 0x64;

/*
*| hmi_display_interface_caln.D_SHORT_AUTO_ACK {
*|      : is_calconst;
*|      : description = "The alert will be considered acknowledged when displayed continuously for D_SHORT_AUTO_ACK.";
*|      : units = "ms";
*|      : transform = fixed.D_SHORT_AUTO_ACK;
*| } 
*/ 
const char D_SHORT_AUTO_ACK = 0x14;

/*
*| hmi_display_interface_caln.CANCLETIME {
*|      : is_calconst;
*|      : description = "Each alert shall have a �Cancel Time� attribute.  The Cancel Time attribute enables alerts that are time sensitive to be removed from the Current Alert Queue even if they have not been displayed. \
1. An alert with Cancel Time = Yes shall be treated as acknowledged if it has not been displayed within CANCELTIME seconds of activation.\
2. An alert with Cancel Time = No shall behave normally.\
";
*|      : units = "ms";
*|      : transform = fixed.CANCLETIME;
*| } 
*/ 
const char CANCLETIME = 0xFA;

const char DISPLAY_INTERFACE_DUMMY_3[3] =
 {
  0x00, 0x00, 0x00
 }
;

/*
*| hmi_display_interface_caln.OAT_DISPLAY_ENABLE {
*|      : is_calconst;
*|      : description = "The outside air temperature shall be displayed as shown in the following table";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char OAT_DISPLAY_ENABLE = 0x01;

/*
*| hmi_display_interface_caln.F_B_ENABLE_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "Proximity Sensing in the center stack enables specific\
interaction capabilities with the system. Proximity sensing\
in the center stack is used to provide feedback to\
the user and keep the screen clutter-free (i.e., elegant).\
When a user�s hand approaches the display, it comes\
alive and interactive elements are displayed that the\
user can touch. Users can interact with the interface in\
an exciting and delightful way when the system detects\
their hand approaching the screen.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char F_B_ENABLE_MASK_BYTE_1 = 0xFF;

/*
*| hmi_display_interface_caln.F_B_ENABLE_MASK_BYTE_2 {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char F_B_ENABLE_MASK_BYTE_2 = 0xF1;

/*
*| hmi_display_interface_caln.GESTURE_DOUBLE_TAP_TIME {
*|      : is_calconst;
*|      : description = "A Double Tap gesture may be used to actuate a feature such as launch an application or peform a secondary\
function of a control. A Double Tap gesture consists of two Taps of the touch screen within a short duration of\
each other; for example, two taps in the approximate same location on the screen within 750 msecs of each other. Details of the action to be performed, including the max duration between taps, when a Double Tap is detected are described in the Application sections where used.\
UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "ms";
*|      : transform = fixed.GESTURE_DOUBLE_TAP_TIME;
*| } 
*/ 
const char GESTURE_DOUBLE_TAP_TIME = 0x4B;

/*
*| hmi_display_interface_caln.SCROLLBAR_PAGEUPDOWN_SLEWRATE {
*|      : is_calconst;
*|      : description = "users can press and hold on the end-points of\
the Scroll Bar causing the list to page up/down at a rate\
of TBD Hz.";
*|      : units = "Hz";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SCROLLBAR_PAGEUPDOWN_SLEWRATE = 0x04;

/*
*| hmi_display_interface_caln.MINACTIVITYINDICATORENABLETIME {
*|      : is_calconst;
*|      : description = "The guideline for displaying an Activity Indicator is when\
the result of a button press or other system operation\
will take more than 1 second to complete (e.g., display\
something relevant to the task being performed), or\
when some system process is taking place in the background\
where users should be kept informed (see Applications\
for specific examples).";
*|      : units = "ms";
*|      : transform = fixed.MINACTIVITYINDICATORENABLETIME;
*| } 
*/ 
const char MINACTIVITYINDICATORENABLETIME = 0x64;

/*
*| hmi_display_interface_caln.MINACTIVITYINDICATORACTIVETIME {
*|      : is_calconst;
*|      : description = "to prevent blings of this indicator it should have a minimum active time.";
*|      : units = "ms";
*|      : transform = fixed.MINACTIVITYINDICATORACTIVETIME;
*| } 
*/ 
const char MINACTIVITYINDICATORACTIVETIME = 0xC8;

/*
*| hmi_display_interface_caln.ALERT_AUTODISMISSFADEOUTTIME {
*|      : is_calconst;
*|      : description = "To offer the user more time to make a final dismiss selection before the alert is available, the alert box will fade into the background over a 3 second period (calibratable).\
\
UPDATE:  \
ALERT_AUTODISMISSFADEOUTTIME must be in all CALDS bundles and common to all HMI software sets.  \
*  However, the calibration only functions with 8\" systems and shall have default of 3 seconds.\
*  The calibration will perform no action in the 4.2\" software sets and shall have default of 0 seconds.\
\
auto-dismiss fadeout is not described in GIS-402 and alerts will not FADE out.  No transitional animation for 4.2\" systems.";
*|      : units = "ms";
*|      : transform = fixed.ALERT_AUTODISMISSFADEOUTTIME;
*| } 
*/ 
const char ALERT_AUTODISMISSFADEOUTTIME = 0x1E;

/*
*| hmi_display_interface_caln.ALERTWINDOWELEMENT_DELAYEDACTIONPRESENTATIONTIME {
*|      : is_calconst;
*|      : description = "When a pop-up is activated for the display (regardless\
if proximity is detected or not), the Pop-up Title and\
Pop-up Detail will immediately be displayed with the\
Pop-up Audio Prompt. The display of the dismissal action\
buttons is suspended for 1500 msec (calibratable)\
to prevent the user from inadvertently dismissing the\
pop-up in an unintentional fashion";
*|      : units = "ms";
*|      : transform = fixed.ALERTWINDOWELEMENT_DELAYEDACTIONPRESENTATIONTIME;
*| } 
*/ 
const char ALERTWINDOWELEMENT_DELAYEDACTIONPRESENTATIONTIME = 0x0F;

const char DISPLAY_INTERFACE_DUMMY_5 = 0x00;

/*
*| hmi_display_interface_caln.POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_1 {
*|      : is_calconst;
*|      : description = "3. Pop-ups will auto-dismiss if not explicitly dismissed\
by the user \
Pop-ups are triggered to auto-dismiss after 20 seconds\
(calibratable).";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_1 = 0x14;

/*
*| hmi_display_interface_caln.POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_2 {
*|      : is_calconst;
*|      : description = "3. Pop-ups will auto-dismiss if not explicitly dismissed\
by the user \
Pop-ups are triggered to auto-dismiss after 20 seconds\
(calibratable).";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_2 = 0x0A;

/*
*| hmi_display_interface_caln.POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_3 {
*|      : is_calconst;
*|      : description = "3. Pop-ups will auto-dismiss if not explicitly dismissed\
by the user \
Pop-ups are triggered to auto-dismiss after 20 seconds\
(calibratable).";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_3 = 0x05;

/*
*| hmi_display_interface_caln.POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_4 {
*|      : is_calconst;
*|      : description = "3. Pop-ups will auto-dismiss if not explicitly dismissed\
by the user \
When users attempt to operate a locked-out control, the system displays a pop-up stating �This feature is unavailable while the vehicle is in motion.� The pop-up times out after 3 seconds with no other user interaction or users could press the BACK button on the faceplate to dismiss the time out";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char POP_UPS_WIDGET_AUTOMAITCDISMISAL_TIME_4 = 0x03;

/*
*| hmi_display_interface_caln.ENABLE_POP_UPS_WIDGET_AUTOMATICDISMISSAL {
*|      : is_calconst;
*|      : description = "Should be able to disable  the feature";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_POP_UPS_WIDGET_AUTOMATICDISMISSAL = 0x01;

/*
*| hmi_display_interface_caln.POPUPWIDGET_AUTODISMISSFADEOUTTIME {
*|      : is_calconst;
*|      : description = "To offer the user more time to make a\
final dismiss selection before the pop-up is unavailable,\
the pop-up will fade into the background over a 3 second\
period (calibratable).\
\
UPDATE:  \
ALERT_AUTODISMISSFADEOUTTIME must be in all CALDS bundles and common to all HMI software sets.  \
*  However, the calibration only functions with 8\" systems and shall have default of 3 seconds.\
*  The calibration will perform no action in the 4.2\" software sets and shall have default of 0 seconds.\
\
auto-dismiss fadeout is not described in GIS-402 and alerts will not FADE out.  No transitional animation for 4.2\" systems.";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char POPUPWIDGET_AUTODISMISSFADEOUTTIME = 0x03;

/*
*| hmi_display_interface_caln.QUICKSTATUSPANEWIDGET_AUTOMATICDISMISSALTIME {
*|      : is_calconst;
*|      : description = "Quick Status Panes auto-dismiss after 3 seconds (calibratable) of inactivity with the vehicle controls. The pane will immediately be removed from the display (no  gradual/slow fading).\
\
UPDATE:  \
QUICKSTATUSPANEWIDGET_AUTOMATICDISMISSALTIME must be in all CALDS bundles and common to all HMI software sets.  \
*  However, the calibration only functions with 8\" systems and shall have default of 3 seconds.\
*  The calibration will perform no action in the 4.2\" software sets and shall have default of 0 seconds.\
\
UPDATE:\
Omega Climate Controls Quick Status Pane GIS 400 V4.1 section 9.5.9.4 requirement added to original section 7.13.1.3 requirement for both climate controls and volume.";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char QUICKSTATUSPANEWIDGET_AUTOMATICDISMISSALTIME = 0x03;

const char DISPLAY_INTERFACE_DUMMY_6 = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_CLIMATECONTROL_QUICK_STATUS_PANE {
*|      : is_calconst;
*|      : description = "The Climate Control Quick Status Pane is displayed by tapping the Temperature in the Status widget area from any view.\
\
UPDATE:  \
QUICKSTATUSPANEWIDGET_AUTOMATICDISMISSALTIME must be in all CALDS bundles and common to all HMI software sets.  \
*  However, the calibration only functions with 8\" systems and shall have default of  1 = ENABLE\
*  The calibration will perform no action in the 4.2\" software sets and shall have default of 0 = DISABLE.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_CLIMATECONTROL_QUICK_STATUS_PANE = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_APPLICATIONTRAY_PHONE {
*|      : is_calconst;
*|      : description = "The four default applications in the tray are:\
Audio, Phone, Navigation, Climate Control\
If the navigation system is not available on the vehicle, the nav application is still shown but has no action when tapped. It provides direction information only.\
UPDATE:\
Item must be in all CALDS bundles and common to all HMI software sets.  \
*  However, the calibration only functions with 8\" systems and shall have the default described in this document.  \
*  The calibration will perform no action in the 4.2\" software sets and shall have default of 0 = DISABLE.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_PHONE = 0x01;

const char DISPLAY_INTERFACE_DUMMY_32 = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_APPLICATIONTRAY_AUDIO {
*|      : is_calconst;
*|      : description = "The four default applications in the tray are:\
Audio, Phone, Navigation, Climate Control\
If the navigation system is not available on the vehicle, the nav application is still shown but has no action when tapped. It provides direction information only.\
UPDATE:\
Item must be in all CALDS bundles and common to all HMI software sets.  \
*  However, the calibration only functions with 8\" systems and shall have the default described in this document.  \
*  The calibration will perform no action in the 4.2\" software sets and shall have default of 0 = DISABLE.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_AUDIO = 0x01;

const char DISPLAY_INTERFACE_DUMMY_7[5] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_display_interface_caln.REVEALONAPPROACHFADEOUTTIME {
*|      : is_calconst;
*|      : description = "The Apps Tray is normally hidden from view. The Apps\
Tray is revealed on proximity on almost all views. In\
cases where the Apps Tray is NOT shown, it is documented\
in that section. Apps Tray will fade when reveal\
on approach time expires.";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char REVEALONAPPROACHFADEOUTTIME = 0x03;

/*
*| hmi_display_interface_caln.APPLICATION_REVEALONAPPROACHTIME {
*|      : is_calconst;
*|      : description = "The page indicator is a reveal on approach element.\
\
APPLICATION_REVEALONAPPROACHTIME is the amount of time before the reveal starts to fade away.  ";
*|      : units = "Seconds";
*|      : type = fixed.UB0;
*| } 
*/ 
const char APPLICATION_REVEALONAPPROACHTIME = 0x1E;

/*
*| hmi_display_interface_caln.ENABLE_RENAMINGFAVORITES {
*|      : is_calconst;
*|      : description = "As the user types additional characters, causing\
the Favorite to exceed the Favorite cell border, the system\
will dynamically reduce the font size in the preview\
area, in single point increments down to a minimum\
TBD point/pixel size (calibratable), such that the largest\
font size is used while allowing the text to fit into the\
Favorite cell region. The text in the preview area (and\
ultimately the Favorite cell) will truncate if the user enters\
more characters than can fit with the minimum font\
size.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_RENAMINGFAVORITES = 0x01;

/*
*| hmi_display_interface_caln.MOVINGFAVORITE_ROWADVANCMENTSLEWRATE {
*|      : is_calconst;
*|      : description = "If there are more Favorites than the 15 that are able\
to be displayed on a single screen, the user is able to\
access those Favorite areas while moving a Favorite by\
dragging just beyond the first/last Favorite row displayed\
to move the Favorite into those regions. If the\
user is dragging a Favorite into those regions, a single\
row will be advanced every 500 ms (calibratable).";
*|      : units = "ms";
*|      : transform = fixed.MOVINGFAVORITE_ROWADVANCMENTSLEWRATE;
*| } 
*/ 
const char MOVINGFAVORITE_ROWADVANCMENTSLEWRATE = 0x7D;

const char DISPLAY_INTERFACE_DUMMY_8 = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_APPLICATION_AUDIO {
*|      : is_calconst;
*|      : description = "HomeScreen Applications";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_AUDIO = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_APPLICATION_PHONE {
*|      : is_calconst;
*|      : description = "HomeScreen Applications";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_PHONE = 0x01;

const char DISPLAY_INTERFACE_DUMMY_9[3] =
 {
  0x00, 0x00, 0x00
 }
;

const char DISPLAY_INTERFACE_DUMMY_33 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_34 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_10 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_45 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_11[2] =
 {
  0x00, 0x00
 }
;

/*
*| hmi_display_interface_caln.ENABLE_APPLICATION_RSE {
*|      : is_calconst;
*|      : description = "The RSE application is entered from the HOME screen by tapping on the RSE Application Icon.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_RSE = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_APPLICATION_SETTINGS {
*|      : is_calconst;
*|      : description = "HomeScreen Applications";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_SETTINGS = 0x01;

/*
*| hmi_display_interface_caln.REARRANGING_APP_ICON_TIMEOUT {
*|      : is_calconst;
*|      : description = "The rearrange mode esits and reverts back to normal mode after a timeout of inactivity of 30 seconds (calibratble) or if the user presses a certain hard key on the faceplate(home, power, or back)";
*|      : units = "Sec";
*|      : type = fixed.UB0;
*| } 
*/ 
const char REARRANGING_APP_ICON_TIMEOUT = 0x1E;

/*
*| hmi_display_interface_caln.SOURCECHANGETIMEOUT {
*|      : is_calconst;
*|      : description = "Metadata will not be shown in\
this case until after the source change timeout period of\
TBD seconds (calibrateable) or when broadcast data is\
available.";
*|      : units = "ms";
*|      : transform = fixed.SOURCECHANGETIMEOUT;
*| } 
*/ 
const char SOURCECHANGETIMEOUT = 0x0A;

/*
*| hmi_display_interface_caln.MEDIA_SOURCE_CHANGE_TO_NOW_PLAYING_TIMEOUT {
*|      : is_calconst;
*|      : description = "When the user selects a new media Source, the display will initially show the device name in the artist field except for the My Media Source. After a timeout of 3 seconds (calibratable) the display will be updated to the Now Playing View for that source and dsiplay the new metadata.\
The My Media Source mode is chosen by cycling through the MEDIA button on the audio interaction selector. The Main Media now playing view wit all screen elements will be shown on the display.  Initially the album art field will be populated with a generic graphic indicating the MY Media source Mdoe.  This should timeout to the album art after 3 seconds (calibratable). Device Names are not required for the My Media source.";
*|      : units = "ms";
*|      : transform = fixed.MEDIA_SOURCE_CHANGE_TO_NOW_PLAYING_TIMEOUT;
*| } 
*/ 
const char MEDIA_SOURCE_CHANGE_TO_NOW_PLAYING_TIMEOUT = 0x1E;

/*
*| hmi_display_interface_caln.ADJUSTINGTONESETTINGS_SLEW_RATE {
*|      : is_calconst;
*|      : description = "Users may also press and hold on either symbol to enable\
a faster slew rate of the tone setting at a rate of\
3 detents per second (calibrateable). The range of the\
tone settings are from 12 to +12 detent settings.";
*|      : units = "mS";
*|      : transform = fixed.ADJUSTINGTONESETTINGS_SLEW_RATE;
*| } 
*/ 
const char ADJUSTINGTONESETTINGS_SLEW_RATE = 0x0A;

/*
*| hmi_display_interface_caln.ADJUSTINGTONESETTINGS_RATEOFCHANGE {
*|      : is_calconst;
*|      : description = "Users may also press and hold on either symbol to enable\
a faster slew rate of the tone setting at a rate of\
3 detents per second (calibrateable). The range of the\
tone settings are from 12 to +12 detent settings.";
*|      : units = "steps";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ADJUSTINGTONESETTINGS_RATEOFCHANGE = 0x03;

/*
*| hmi_display_interface_caln.SWMI_CALIBRATION_DATA_FILE_HMI_F_B_CAL {
*|      : is_calconst;
*|      : description = "The NoCalibration state is defined in order to make sure that Infotainment subsystem components have been updated with calibrations after a service event such as replacing one or more modules or upgrading software.  The mechanism for NoCalibration determination is the same as that for Theftlock and NoVIN determination.  That is, at initialization a module detects whether or not it has a valid calibration.  If it does not, it notifies the SystemState FBlock via the SystemState.SetNoCalibrationModuleState method.  Once a valid calibration is received, the module calls the SystemState.SetNoCalibrationModuleState method to clear the condition.\
The SystemState FBlock shall assume all module calibrations are valid upon each Sleep cycle.  Modules that already have a valid calibration or do not support NoCalibration detection (determined by each module CTS) do not need to report the NoCalibration clear condition at each initialization.\
Note: each of these Calibrations should be ...";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SWMI_CALIBRATION_DATA_FILE_HMI_F_B_CAL = 0x00;

const char DISPLAY_INTERFACE_DUMMY_12 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_35 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_36 = 0x00;

/*
*| hmi_display_interface_caln.ATC_MENU_OPTION_ENABLE {
*|      : is_calconst;
*|      : description = "Used for NON DSP systems.  Disable for DSP systems.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ATC_MENU_OPTION_ENABLE = 0x01;

const char DISPLAY_INTERFACE_DUMMY_13[2] =
 {
  0x00, 0x00
 }
;

/*
*| hmi_display_interface_caln.GESTURE_TAP_MAX_DISTANCE {
*|      : is_calconst;
*|      : description = "software concept document\
UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "mm";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GESTURE_TAP_MAX_DISTANCE = 0x50;

/*
*| hmi_display_interface_caln.GESTURE_TAP_MAX_TIME {
*|      : is_calconst;
*|      : description = "software concept document\
UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "ms";
*|      : transform = fixed.GESTURE_TAP_MAX_TIME;
*| } 
*/ 
const char GESTURE_TAP_MAX_TIME = 0x4B;

/*
*| hmi_display_interface_caln.GESTURE_PRESS_HOLD_MAX_DISTANCE {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "mm";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GESTURE_PRESS_HOLD_MAX_DISTANCE = 0x28;

/*
*| hmi_display_interface_caln.GESTURE_PRESS_HOLD_TIME_1 {
*|      : is_calconst;
*|      : description = "used for feaures like: ScrollBar_PageUpOrDown, StoringFavorites, EditFavorites, MoveFavorties, KeyboardDeleteButton,ContactKeypadEntryDeleteButton, RearrangingAppsICONs, AdjustingToneSettings, AdjustingToneSettings, StrongStationSearch, AllStationSearch, TimesShiftFastRate1, CDFastTrackSearch, MyMediaFastTrackSearch, Rotate_Camera_View, Front_Temperature_Adjustment, Rear_Temperature_Adjustment, Front_Fan_Speed_Adjustment, Rear_Fan_Speed_Adjustment, Phone_Keypad_+_*_Keys,  Phone_Keypad_Speed_Dial, New_Predefined_Msg_View_Delete_Word, Text_Signature_Setting_Delete_Word, Text_Signature_Setting_Delete_Entire_Field\
\
UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "ms";
*|      : transform = fixed.GESTURE_PRESS_HOLD_TIME_1;
*| } 
*/ 
const char GESTURE_PRESS_HOLD_TIME_1 = 0x1E;

const char DISPLAY_INTERFACE_DUMMY_37 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_38 = 0x00;

/*
*| hmi_display_interface_caln.GESTURE_SPREAD_MIN_FACTOR {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "Spread Factor";
*|      : transform = fixed.GESTURE_SPREAD_MIN_FACTOR;
*| } 
*/ 
const char GESTURE_SPREAD_MIN_FACTOR = 0x40;

/*
*| hmi_display_interface_caln.GESTURE_PINCH_MAX_FACTOR {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "Pinch Factor";
*|      : transform = fixed.GESTURE_PINCH_MAX_FACTOR;
*| } 
*/ 
const char GESTURE_PINCH_MAX_FACTOR = 0x40;

/*
*| hmi_display_interface_caln.GESTURE_SPREAD_PINCH_DOUBLETOUCH_TIME {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "ms";
*|      : transform = fixed.GESTURE_SPREAD_PINCH_DOUBLETOUCH_TIME;
*| } 
*/ 
const char GESTURE_SPREAD_PINCH_DOUBLETOUCH_TIME = 0x64;

/*
*| hmi_display_interface_caln.GESTURE_NUDGE_MIN_DISTANCE {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "mm";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GESTURE_NUDGE_MIN_DISTANCE = 0x3C;

/*
*| hmi_display_interface_caln.GESTURE_FLING_MIN_DISTANCE {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "mm";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GESTURE_FLING_MIN_DISTANCE = 0x3C;

/*
*| hmi_display_interface_caln.GESTURE_FLING_MIN_SPEED {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "mm/s";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GESTURE_FLING_MIN_SPEED = 0x32;

/*
*| hmi_display_interface_caln.GESTURE_SPREAD_GAIN {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "Spread Gain";
*|      : transform = fixed.GESTURE_SPREAD_GAIN;
*| } 
*/ 
const char GESTURE_SPREAD_GAIN = 0x18;

/*
*| hmi_display_interface_caln.GESTURE_PINCH_GAIN {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "Pinch Gain";
*|      : transform = fixed.GESTURE_PINCH_GAIN;
*| } 
*/ 
const char GESTURE_PINCH_GAIN = 0x18;

/*
*| hmi_display_interface_caln.GESTURE_NUDGE_GAIN {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "Nudge Gain";
*|      : transform = fixed.GESTURE_NUDGE_GAIN;
*| } 
*/ 
const char GESTURE_NUDGE_GAIN = 0x20;

/*
*| hmi_display_interface_caln.GESTURE_FLING_GAIN {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "Fling Gain";
*|      : transform = fixed.GESTURE_FLING_GAIN;
*| } 
*/ 
const char GESTURE_FLING_GAIN = 0x40;

/*
*| hmi_display_interface_caln.GESTURE_FLING_FRICTION {
*|      : is_calconst;
*|      : description = "UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "m/m/s";
*|      : type = fixed.UB0;
*| } 
*/ 
const char GESTURE_FLING_FRICTION = 0x28;

const char DISPLAY_INTERFACE_DUMMY_14[77] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_BROADCASTAUDIO {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_BROADCASTAUDIO = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_GENRES {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_GENRES = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_ARTISTS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_ARTISTS = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_ALBUMS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_ALBUMS = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_SONGS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_SONGS = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_PLAYLISTS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_PLAYLISTS = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_PODCASTS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_PODCASTS = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_AUDIOBOOKS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_AUDIOBOOKS = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_VIDEOS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_VIDEOS = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_TONESETTINGS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_TONESETTINGS = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_INTERNETRADIOSTATIONS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_INTERNETRADIOSTATIONS = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_PANDORARADIOSTATIONS {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_PANDORARADIOSTATIONS = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_TUNESELECTALERTMENU {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_TUNESELECTALERTMENU = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_TAGSONGBUTTON {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_TAGSONGBUTTON = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORTIES_APPLICATION_SELECTION {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORTIES_APPLICATION_SELECTION = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_AUDIO_SOURCE_SELECTION {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_AUDIO_SOURCE_SELECTION = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_SHUFFLE_TOGGLE_ENABLE {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_SHUFFLE_TOGGLE_ENABLE = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_DIAL_PHONE_NUMBER_WITH_NO_CONTACT_INFO_SELECTION {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_DIAL_PHONE_NUMBER_WITH_NO_CONTACT_INFO_SELECTION = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_DIAL_VOICEMAIL_SELECTION_ENABLE {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_DIAL_VOICEMAIL_SELECTION_ENABLE = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_GENERATE_BLANK_EMAIL_TO_EMAIL_ADDRESS_SELECTION_ENABLE {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_GENERATE_BLANK_EMAIL_TO_EMAIL_ADDRESS_SELECTION_ENABLE = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_GENERATE_COMPOSED_EMAIL_TO_EMAIL_ADDRESS_SELECTION_ENABLE {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_GENERATE_COMPOSED_EMAIL_TO_EMAIL_ADDRESS_SELECTION_ENABLE = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_GENERATE_BLANK_SMS_TO_PHONE_NUMBER_CONTACT_SELECTION_ENABLE {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_GENERATE_BLANK_SMS_TO_PHONE_NUMBER_CONTACT_SELECTION_ENABLE = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_STORINGFAVORITES_GENERATE_COMPOSED_SMS_TO_PHONE_NUMBER_CONTACT_SELECTION_ENABLE {
*|      : is_calconst;
*|      : description = "The HMI should allow a Master Enable / Disable for each of the item that can be an iFavorite.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_STORINGFAVORITES_GENERATE_COMPOSED_SMS_TO_PHONE_NUMBER_CONTACT_SELECTION_ENABLE = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_BLUETOOTHSOURCEMODE {
*|      : is_calconst;
*|      : description = "The Bluetooth source mode is chosen by cycling through\
the �CD/AUX� button on the audio interaction selector.\
The main Bluetooth now playing view with all screen\
elements will be shown on the display.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_BLUETOOTHSOURCEMODE = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_AUDIONOWPLAYING_SENDTOCLUSTER {
*|      : is_calconst;
*|      : description = "The user is able to use a gesture to send the audio now playing information to the cluster\
\
UPDATE:  Not possible on a non-touch system such as the 4.2\".  Default value is $0 DISABLE for non-touch systems.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_AUDIONOWPLAYING_SENDTOCLUSTER = 0x01;

/*
*| hmi_display_interface_caln.FRONT_TEMPERATURE_ADJUSTMENT_SLEW_RATE {
*|      : is_calconst;
*|      : description = "Finally, users can increase/decrease the �set� temperature\
incrementally by tapping at the extremes of the\
displayed temperature range (e.g., as denoted by +/-\
symbols at the extremes). For each tap, the �set� temperature\
value is increased or decreased by one degree.\
Users can press-and-hold at the extremes to increase/\
decrease �set� temperature rapidly at the rate of 5 Hz\
(TBD, calibratable).";
*|      : units = "mS";
*|      : transform = fixed.FRONT_TEMPERATURE_ADJUSTMENT_SLEW_RATE;
*| } 
*/ 
const char FRONT_TEMPERATURE_ADJUSTMENT_SLEW_RATE = 0x14;

/*
*| hmi_display_interface_caln.FRONT_FAN_SPEED_ADJUSTMENT_SLEW_RATE {
*|      : is_calconst;
*|      : description = "Finally, users can also increase/decrease fan speed incrementally\
by tapping at the extremes of the displayed\
fan speed range (e.g., as denoted by +/- symbols or\
other icons at the extremes). For each tap, fan speed is\
increased or decreased by one setting value. Users can\
press-and-hold at the extremes to increase/decrease fan\
speed rapidly at the rate of 2 Hz (TBD, calibratable).";
*|      : units = "mS";
*|      : transform = fixed.FRONT_FAN_SPEED_ADJUSTMENT_SLEW_RATE;
*| } 
*/ 
const char FRONT_FAN_SPEED_ADJUSTMENT_SLEW_RATE = 0x14;

/*
*| hmi_display_interface_caln.REAR_TEMPERATURE_ADJUSTMENT_SLEW_RATE {
*|      : is_calconst;
*|      : description = "Finally, users can increase/decrease the �set� temperature\
incrementally by tapping at the extremes of the\
displayed temperature range (e.g., as denoted by +/-\
symbols at the extremes). For each tap, the �set� temperature\
value is increased or decreased by one degree.\
Users can press-and-hold at the extremes to increase/\
decrease �set� temperature rapidly at the rate of 5 Hz\
(TBD, calibratable).";
*|      : units = "mS";
*|      : transform = fixed.REAR_TEMPERATURE_ADJUSTMENT_SLEW_RATE;
*| } 
*/ 
const char REAR_TEMPERATURE_ADJUSTMENT_SLEW_RATE = 0x14;

/*
*| hmi_display_interface_caln.REAR_FAN_SPEED_ADJUSTMENT_SLEW_RATE {
*|      : is_calconst;
*|      : description = "Finally, users can also increase/decrease fan speed incrementally\
by tapping at the extremes of the displayed\
fan speed range (e.g., as denoted by +/- symbols or\
other icons at the extremes). For each tap, fan speed is\
increased or decreased by one setting value. Users can\
press-and-hold at the extremes to increase/decrease fan\
speed rapidly at the rate of 2 Hz (TBD, calibratable).";
*|      : units = "mS";
*|      : transform = fixed.REAR_FAN_SPEED_ADJUSTMENT_SLEW_RATE;
*| } 
*/ 
const char REAR_FAN_SPEED_ADJUSTMENT_SLEW_RATE = 0x14;

/*
*| hmi_display_interface_caln.MAX_AUDIO_TEXT_EMAIL_RECORD_TIME {
*|      : is_calconst;
*|      : description = "The user taps start to begin the voice recording. There\
is an audible beep and the display changes to show stop\
recording button to indicate to the user that recording\
has begun. When the user has completed the voice recorded\
message, the user will tap the Stop button. The\
maximum length of the recording is 5 minutes (calibratible).\
If the recording time reaches the limit prior to the\
user tapping Stop, there is an audible beep, recording\
stops, and the display changes to show the Edit / Send\
Audio message view. If the user taps Stop when recording\
is complete, the Edit / Send Audio Message view is\
be displayed.";
*|      : units = "Seconds";
*|      : transform = fixed.MAX_AUDIO_TEXT_EMAIL_RECORD_TIME;
*| } 
*/ 
const char MAX_AUDIO_TEXT_EMAIL_RECORD_TIME = 0x1E;

/*
*| hmi_display_interface_caln.MAX_NEW_PREDEFINED_TEXT_EMAIL_MSG_RESPONSES {
*|      : is_calconst;
*|      : description = "The user may tap on the add new predefined message\
to define their own message. The new predefined message\
view will be displayed\
The maximum number of predefined messages is 50.  If the user attempts to add a new predefined message whn there are already 50 messages, a popup is shown providing a tip to the user....";
*|      : units = "New_Text_Email_Message_response";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MAX_NEW_PREDEFINED_TEXT_EMAIL_MSG_RESPONSES = 0x0F;

/*
*| hmi_display_interface_caln.MAX_NEW_TEXT_EMAIL_MESSAGE_RESPONSE_CHARACTERS {
*|      : is_calconst;
*|      : description = "The message region will have a blank area for the typed\
message and be pre-populated with the signature. The\
signature will be deleted if the message is longer than\
the maximum length with the signature.\
The user may type in the new predefined message and\
tap save.\
The max lenght of a pre-defined message is 160 characters. if the user reaches 160 characteres, no more characters are allowed and a popup is displayed ot notify the user";
*|      : units = "characters";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MAX_NEW_TEXT_EMAIL_MESSAGE_RESPONSE_CHARACTERS = 0xA0;

/*
*| hmi_display_interface_caln.DEFAULT_SENT_TEXT_MESSAGE_SETTING {
*|      : is_calconst;
*|      : description = "The user may enable or disable the saving of a copy of\
a sent text message in the sent folder. The default is\
enabled. When this is enabled, a copy of the sent text\
message will be saved in the Sent Folder. When this is\
disabled, a copy of the sent text message will not be\
saved in the Sent Folder. This feature is desirable for the\
future if vehicle text messages are automatically generated";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DEFAULT_SENT_TEXT_MESSAGE_SETTING = 0x01;

/*
*| hmi_display_interface_caln.DEFAULT_EMAIL_SETTINGS_VIEW_SENT_EMAIL_SETTING {
*|      : is_calconst;
*|      : description = "The user may enable or disable the saving of a copy of\
a sent e-mail in the sent folder. The default is enabled.\
When this is enabled, a copy of the sent e-mail will be\
saved in the Sent Folder. When this is disabled, a copy\
of the sent e-mail will not be saved in the Sent Folder.\
This feature is desirable for the future if vehicle text\
messages are automatically generated.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char DEFAULT_EMAIL_SETTINGS_VIEW_SENT_EMAIL_SETTING = 0x00;

const unsigned short DISPLAY_INTERFACE_DUMMY_15 = 0x0000;

/*
*| hmi_display_interface_caln.VALET_MASK_MENU {
*|      : is_calconst;
*|      : description = "Vehicle - Specific Feature Support";
*|      : units = "Enumeration";
*|      : type = fixed.VALET_MASK_MENU;
*| } 
*/ 
const char VALET_MASK_MENU = 0x01;

/*
*| hmi_display_interface_caln.VALET_COMBO_FLASH_FREQUENCY {
*|      : is_calconst;
*|      : description = "Upon successfully entering a 4-digit code and tapping �LOCK�, the combination will flash the combination for 3 seconds at a 1 hertz rate.";
*|      : units = "Hz";
*|      : type = fixed.UB0;
*| } 
*/ 
const char VALET_COMBO_FLASH_FREQUENCY = 0x01;

/*
*| hmi_display_interface_caln.VALET_COMBO_FLASH_TIME {
*|      : is_calconst;
*|      : description = "Upon successfully entering a 4-digit code and tapping �LOCK�, the combination will flash the combination for 3 seconds at a 1 hertz rate.";
*|      : units = "ms";
*|      : transform = fixed.VALET_COMBO_FLASH_TIME;
*| } 
*/ 
const char VALET_COMBO_FLASH_TIME = 0x1E;

/*
*| hmi_display_interface_caln.VALET_COMBO_DIGITS_SUPPORTED {
*|      : is_calconst;
*|      : description = "Upon successfully entering a 4-digit code and tapping �LOCK�, the combination will flash the combination for 3 seconds at a 1 hertz rate.";
*|      : units = "Digits";
*|      : type = fixed.UB0;
*| } 
*/ 
const char VALET_COMBO_DIGITS_SUPPORTED = 0x04;

/*
*| hmi_display_interface_caln.SPEECH_SETTING_MENU_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "The Settings Application Speech view provides the fol�lowing settings: ";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char SPEECH_SETTING_MENU_MASK_BYTE_1 = 0xF8;

const char DISPLAY_INTERFACE_DUMMY_16 = 0x00;

/*
*| hmi_display_interface_caln.DEFAULT_MASK_DISPLAY_SETTINGS {
*|      : is_calconst;
*|      : description = "The Settings Application Display view provides the following settings";
*|      : units = "Enumeration";
*|      : type = fixed.DEFAULT_MASK_DISPLAY_SETTINGS;
*| } 
*/ 
const char DEFAULT_MASK_DISPLAY_SETTINGS = 0x01;

const char DISPLAY_INTERFACE_DUMMY_39 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_17[11] =
 {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
 }
;

/*
*| hmi_display_interface_caln.CLUSTER_SWIPE_APPLICATION_EVENT_MAX_NOTIFY_RATE {
*|      : is_calconst;
*|      : description = "If the SwipeApplication enumeration value has not changed, but the SwipePositionPercent is changing, the CenterStack HMI shall rate limit notifications of changes in the Property to the internal calibration value CLUSTER_SWIPE_APPLICATION_EVENT_MAX_NOTIFY_RATE. This is necessary to limit MOST control channel loading and will generally be in the tens of milliseconds. This calibration value is defined separately within the calibration specification of the module that drives the center stack HMI. For example, this is GIS-321 for an HMIModule equipped system. ";
*|      : units = "ms";
*|      : transform = fixed.CLUSTER_SWIPE_APPLICATION_EVENT_MAX_NOTIFY_RATE;
*| } 
*/ 
const char CLUSTER_SWIPE_APPLICATION_EVENT_MAX_NOTIFY_RATE = 0x0F;

const unsigned short DISPLAY_INTERFACE_DUMMY_18 = 0x0000;

/*
*| hmi_display_interface_caln.MAX_TAG_SONG_HANDLING {
*|      : is_calconst;
*|      : description = "A maximum of 50 song tags can be stored in the system.\
When a user�s ipod is not connected to the system and\
the song tag maximum has been reached, the system\
will display the pop-up message, �Tagged Songs Memory\
Full.�";
*|      : units = "Tagged Songs";
*|      : type = fixed.UB0;
*| } 
*/ 
const char MAX_TAG_SONG_HANDLING = 0x32;

/*
*| hmi_display_interface_caln.METADATA_FULLSCREEN_PAGETIME {
*|      : is_calconst;
*|      : description = "The HD Radio fields are shown in the following order\
and if there is more than 1 field per line, they page a\
single time at a rate of 5 seconds per field:\
Line 1: Frequency, HD logo, Call Letters\
Line 2: Artist Name\
Line 3: Song Title\
Metadata will be presented as shown on the screens to\
the left. Any information beyond the call letters in the\
station field should not be displayed\
\
The RDS radio fields are shown in the following order\
and if there is more content than will fit in the area,\
they page a single time at a rate of 5 seconds per field:\
Line 1: Frequency and/or RDS-PSN\
Line 2: RDS Text Line 1 as provided by broadcast\
Line 3: RDS Text Line 2 (if necessary - as provided)\
\
The DAB radio fields are shown in the following order\
and if there is more content than will fit in the area,\
they page a single time at a rate of 5 seconds per field:\
Line 1: DAB Station/Service Name\
Line 2: DAB Ensemble Name\
Line 3: DAB Text info (eg. - artist/song)";
*|      : units = "ms";
*|      : transform = fixed.METADATA_FULLSCREEN_PAGETIME;
*| } 
*/ 
const char METADATA_FULLSCREEN_PAGETIME = 0x64;

const unsigned short DISPLAY_INTERFACE_DUMMY_19[2] =
 {
  0x0000, 0x0000
 }
;

const char DISPLAY_INTERFACE_DUMMY_46 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_47 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_91 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_92 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_40 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_41 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_42 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_43 = 0x00;

/*
*| hmi_display_interface_caln.TOUCHPAD_FBS_ENABLE_MASK_BYTE_1 {
*|      : is_calconst;
*|      : description = "4.1.1.1~MY14 Standard Navigation / Cursor";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_FBS_ENABLE_MASK_BYTE_1 = 0x00;

/*
*| hmi_display_interface_caln.TOUCHPAD_FBS_ENABLE_MASK_BYTE_2 {
*|      : is_calconst;
*|      : description = "4.1.1.11~MY14 Two Finger Press / Click";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_FBS_ENABLE_MASK_BYTE_2 = 0x00;

/*
*| hmi_display_interface_caln.TOUCHPAD_FBS_ENABLE_MASK_BYTE_3 {
*|      : is_calconst;
*|      : description = "4.1.1.19~MY14 Three Finger Press / Click";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char TOUCHPAD_FBS_ENABLE_MASK_BYTE_3 = 0x00;

const unsigned long DISPLAY_INTERFACE_DUMMY_48 = 0x00000000;

const char DISPLAY_INTERFACE_DUMMY_49 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_50 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_51 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_52 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_53 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_54 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_55 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_56 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_57 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_58 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_59 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_60 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_61 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_44 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_62 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_63 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_64 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_20 = 0x00;

/*
*| hmi_display_interface_caln.MICROPHONE_2_CONFIG {
*|      : is_calconst;
*|      : description = "Secondary microphone input.   Use in DTC Failure Criteria and to control the mic configuration.  Typically proliferates based on vehicle amp/engine for ANC (Active Noise Cancellation)\
\
Examples:\
Microphone 2 = $00 No noise cancelling mic present.  \
Microphone 2 = $01 Vehicle has a second ANC microphone directly connected to the HMI module to assist speech.\
Microphone 2 = $02 Vehicle has an ANC MIC conected to the AMP and AMP provides MOST data.";
*|      : units = "ENUM";
*|      : type = fixed.MICROPHONE_2_CONFIG;
*| } 
*/ 
const char MICROPHONE_2_CONFIG = 0x00;

/*
*| hmi_display_interface_caln.DTC_MASK_BYTE_MIC2 {
*|      : is_calconst;
*|      : description = "Each DTC and FTB combination shall have the capability to be individually masked.  This shall be done via a calibration file.";
*|      : units = "Boolean";
*|      : type = fixed.UB0;
*| } 
*/ 
const char DTC_MASK_BYTE_MIC2 = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_NOISE_COMP_TEXT_STRING_OVERRIDE {
*|      : is_calconst;
*|      : description = "When NCV is active, the HMI should show \"Bose Audio Pilot\" instead of the list of High, Medium-High, Medium,  Medium-Low, Low, and Off.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_NOISE_COMP_TEXT_STRING_OVERRIDE = 0x00;

/*
*| hmi_display_interface_caln.NOISE_COMP_TEXT_STRING {
*|      : is_calconst;
*|      : description = "When NCV is active, the HMI should show \"Bose Audio Pilot\" instead of the list of High, Medium-High, Medium,  Medium-Low, Low, and Off.";
*|      : units = "UTF 16 Characters";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short NOISE_COMP_TEXT_STRING[31] =
 {
  0x0042, 0x004F, 0x0053, 0x0045, 0x0020, 0x0041, 0x0055, 0x0044,
  0x0049, 0x004F, 0x0020, 0x0050, 0x0049, 0x004C, 0x004F, 0x0054,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
 }
;

/*
*| hmi_display_interface_caln.SIMULATED_SURROUND_DISPLAY_TEXT_ENABLE {
*|      : is_calconst;
*|      : description = "";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char SIMULATED_SURROUND_DISPLAY_TEXT_ENABLE = 0x00;

/*
*| hmi_display_interface_caln.SIMULATED_SURROUND_DISPLAY_TEXT_STRING {
*|      : is_calconst;
*|      : description = "";
*|      : units = "UTF 16 Characters";
*|      : type = fixed.US0;
*| } 
*/ 
const unsigned short SIMULATED_SURROUND_DISPLAY_TEXT_STRING[11] =
 {
  0x0043, 0x0045, 0x004E, 0x0054, 0x0045, 0x0052, 0x0050, 0x004F,
  0x0049, 0x004E, 0x0054
 }
;

const char DISPLAY_INTERFACE_DUMMY_21 = 0x00;

/*
*| hmi_display_interface_caln.GESTURE_DECISION_TIMER {
*|      : is_calconst;
*|      : description = "This gesture timer determines how long to wait before deciding which gesture the user is performing.  formula to E=N*10 instead of E=N*50\
UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "ms";
*|      : transform = fixed.GESTURE_DECISION_TIMER;
*| } 
*/ 
const char GESTURE_DECISION_TIMER = 0x00;

/*
*| hmi_display_interface_caln.GESTURE_FLING_MAX_SPEED {
*|      : is_calconst;
*|      : description = "This calibration used as the maximum fling speed when flinging in list.\
\
UPDATE:  Does not apply to 4.2\" non-touch systems.";
*|      : units = "ms";
*|      : transform = fixed.GESTURE_FLING_MAX_SPEED;
*| } 
*/ 
const char GESTURE_FLING_MAX_SPEED = 0x50;

const char DISPLAY_INTERFACE_DUMMY_65 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_66 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_67 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_68 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_69 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_70 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_71 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_72 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_73 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_74 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_75 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_76 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_77 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_78 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_79 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_80 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_81 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_82 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_83 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_84 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_85 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_86 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_87 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_88 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_89 = 0x00;

const char DISPLAY_INTERFACE_DUMMY_90 = 0x00;

/*
*| hmi_display_interface_caln.ROTARY_ACCELERATION_LISTPART_THRESHOLD {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACCELERATION_LISTPART_THRESHOLD = 0x19;

/*
*| hmi_display_interface_caln.ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_1 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "Ticks";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_1 = 0x10;

/*
*| hmi_display_interface_caln.ROTARY_ACEELERATION_DELTACOUNT_SKIP_1 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "steps";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACEELERATION_DELTACOUNT_SKIP_1 = 0x05;

/*
*| hmi_display_interface_caln.ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_2 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "Ticks";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_2 = 0x64;

/*
*| hmi_display_interface_caln.ROTARY_ACEELERATION_DELTACOUNT_SKIP_2 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "%";
*|      : transform = fixed.ROTARY_ACEELERATION_DELTACOUNT_SKIP_2;
*| } 
*/ 
const char ROTARY_ACEELERATION_DELTACOUNT_SKIP_2 = 0x19;

/*
*| hmi_display_interface_caln.ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_3 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "Ticks";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_3 = 0x64;

/*
*| hmi_display_interface_caln.ROTARY_ACEELERATION_DELTACOUNT_SKIP_3 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACEELERATION_DELTACOUNT_SKIP_3 = 0x7d;

/*
*| hmi_display_interface_caln.ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_4 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "Ticks";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_4 = 0x0a;

/*
*| hmi_display_interface_caln.ROTARY_ACEELERATION_DELTACOUNT_SKIP_4 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACEELERATION_DELTACOUNT_SKIP_4 = 0x10;

/*
*| hmi_display_interface_caln.ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_5 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "Ticks";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACCELERATION_DELTACOUNT_THRESHOLD_5 = 0x0f;

/*
*| hmi_display_interface_caln.ROTARY_ACEELERATION_DELTACOUNT_SKIP_5 {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "%";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACEELERATION_DELTACOUNT_SKIP_5 = 0x20;

/*
*| hmi_display_interface_caln.ROTARY_ACCELERATION_START_DELAY {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "Ticks";
*|      : type = fixed.UB0;
*| } 
*/ 
const char ROTARY_ACCELERATION_START_DELAY = 0x14;

/*
*| hmi_display_interface_caln.ROTARY_ACCELERATION_CLEAR_TIMER {
*|      : is_calconst;
*|      : description = "GIS 402 v4.0 Cp. 7.3.2.1";
*|      : units = "ms";
*|      : transform = fixed.ROTARY_ACCELERATION_CLEAR_TIMER;
*| } 
*/ 
const char ROTARY_ACCELERATION_CLEAR_TIMER = 0x4b;

/*
*| hmi_display_interface_caln.RSE_FRONT_VIDEO_ENABLED {
*|      : is_calconst;
*|      : description = "Add calibration to HMIModule to hide WatchVideo button that allows user to get Front RSE video in order to disable Front RSE video for LVM-Nav w/ RSE combination.\
LVM-Nav is being offered with RSE, at least in GMT166 in Korea.  RSE front video is not supported with LVM-Nav because there are not enough LVDS inputs to route RSE video from front to LVM-Nav.  So customer can access RSE application in center stack HMI for the limited control via IRSEFrontControl but cannot watch the video.  We can name the cal RSE_FRONT_VIDEO_ENABLED = TRUE or FALSE with a default value of TRUE.In the HMIModule it hides the WatchVideo button on the front screen.\
RSE and LVM are mutually exclusive\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char RSE_FRONT_VIDEO_ENABLED = 0x01;

/*
*| hmi_display_interface_caln.ENABLE_APPLICATION_DMC3 {
*|      : is_calconst;
*|      : description = "Disables Appication DMC3 feature that is called DRIVE MODE on Home Screen.\
Elements inside Settings menu regarding DMCx are not affected.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATION_DMC3 = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_APPLICATIONTRAY_DMC3 {
*|      : is_calconst;
*|      : description = "Shows DMC3 Application that is called DRIVE MODE on the home screen on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_DMC3 = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_APPLICATIONTRAY_RSE {
*|      : is_calconst;
*|      : description = "Shows RSE Application on application tray on startup.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_APPLICATIONTRAY_RSE = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_SPORT {
*|      : is_calconst;
*|      : description = "Enables DMC3 Sport Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had Sport data, then we would not have legend item SPORT but we would color the SPORT components red per GIS-400.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_SPORT = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_NORMAL {
*|      : is_calconst;
*|      : description = "Enables DMC3 Normal Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_NORMAL = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_TOUR {
*|      : is_calconst;
*|      : description = "Enables DMC3 Tour Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_TOUR = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_SNOW_ICE {
*|      : is_calconst;
*|      : description = "Enables DMC3 Snow_Ice Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_SNOW_ICE = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_COMFORT {
*|      : is_calconst;
*|      : description = "Enables DMC3 Comfort Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_COMFORT = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_OFF_ROAD {
*|      : is_calconst;
*|      : description = "Enables DMC3 Off Road Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_OFF_ROAD = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_TRACK {
*|      : is_calconst;
*|      : description = "Enables DMC3 Track Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_TRACK = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_TOW_HAUL {
*|      : is_calconst;
*|      : description = "Enables DMC3 Tow_Haul Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_TOW_HAUL = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_ECO {
*|      : is_calconst;
*|      : description = "Enables DMC3 Eco Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_ECO = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_WEATHER {
*|      : is_calconst;
*|      : description = "Enables DMC3 Weather Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_WEATHER = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_SNOW {
*|      : is_calconst;
*|      : description = "Enables DMC3 Snow Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_SNOW = 0x00;

/*
*| hmi_display_interface_caln.ENABLE_DMC3_MODE_ALL_TERRAIN {
*|      : is_calconst;
*|      : description = "Enables DMC3 All Terrain Mode\
18 Sep 2013: Serial Data defines coloring of drive components. Calibration controls visibility of each legend item. These 2 are mutually exclusive. If cal is set to 0 and SBX had XXX data, then we would not have legend item XXX but we would color the XXX components per GIS-400.\
";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char ENABLE_DMC3_MODE_ALL_TERRAIN = 0x00;

/*
*| hmi_display_interface_caln.CAL_HMI_F_B_END {
*|      : is_calconst;
*|      : description = "END OF CAL BLOCK";
*|      : units = "Boolean";
*|      : type = fixed.Boolean;
*| } 
*/ 
const char CAL_HMI_F_B_END = 0x00;
