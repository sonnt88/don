
#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS 

//--- Start of s32PrepareSCCEOLData_Test --
void s32PrepareSCCEOLData_Test::SetUp()
{
	EOLLib_Test::SetUp();
	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]), EOLLIB_TABLE_ID_SYSTEM,          
		(tPCS8) EOLLIB_TABLE_NAME_SYSTEM, EN_FFD_DATA_SET_EOL_SYSTEM,
		EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]), EOLLIB_TABLE_ID_BRAND,        
		(tPCS8) EOLLIB_TABLE_NAME_BRAND, EN_FFD_DATA_SET_EOL_BRAND,
		EOLLIB_OFFSET_CAL_HMI_BRAND_END);

}

//Invalid arguments - do not send data to SCC
TEST_F(s32PrepareSCCEOLData_Test, onPassingNullSharedMemoryPtr_Return_InvalidValue)
{
	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32PrepareSCCEOLData(OSAL_NULL, OSAL_NULL));
}

TEST_F(s32PrepareSCCEOLData_Test, onPassingNullStringBufferPtr_Return_InvalidValue)
{
	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32PrepareSCCEOLData(m_ptsShMemEOLLIB, OSAL_NULL));
}

TEST_F(s32PrepareSCCEOLData_Test, onGettingInvalidEOLBlockPointer_Return_InvalidValue)
{
	tU8 u8EOLCalDataForSCC[EOL_MAX_DATA_LENGTH + 1] = {0};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]);
	psBlock->u8TableId = 0x00; //for checking negative test case in s32PrepareSCCEOLData() function [psGetBlock() returning OSAL_NULL]

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_) )
		.Times(1);
	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32PrepareSCCEOLData(m_ptsShMemEOLLIB, u8EOLCalDataForSCC));
	psBlock->u8TableId = BRAND; //Reset it back to valid Table Id
}

TEST_F(s32PrepareSCCEOLData_Test, onPassingValidSharedMemoryWithValidEOLBlocks_Return_NoError)
{
	tU8 u8EOLCalDataForSCC[EOL_MAX_DATA_LENGTH] = {0};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	tU8 u8ExpectedEOLCalDataForSCC[EOL_MAX_DATA_LENGTH] = {0};
	memset(u8ExpectedEOLCalDataForSCC, 0xCD, (EOL_MAX_DATA_LENGTH-EOL_TO_SCC_UNUSED)); //since only (EOL_MAX_DATA_LENGTH-EOL_TO_SCC_UNUSED) bytes are filled in by EOLLib. EOL_TO_SCC_UNUSED are reserved for future use!

	memset(psBlock->pu8Data, 0xCD, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]);
	memset(psBlock->pu8Data, 0xCD, EOLLIB_OFFSET_CAL_HMI_BRAND_END);

	EXPECT_EQ(OSAL_E_NOERROR, s32PrepareSCCEOLData(m_ptsShMemEOLLIB, u8EOLCalDataForSCC));
	EXPECT_FALSE(memcmp(u8ExpectedEOLCalDataForSCC, u8EOLCalDataForSCC, EOL_MAX_DATA_LENGTH));
}

TEST_F(s32PrepareSCCEOLData_Test, onPassingValidShMemAndEOLBlocksWithDiffEOLValues_Return_NoError) //Testing Boundry cases
{
	tU8 u8EOLCalDataForSCC[EOL_MAX_DATA_LENGTH] = {0};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	tU8 u8ExpectedEOLCalDataForSCC[EOL_MAX_DATA_LENGTH] = {0};
	memset(u8ExpectedEOLCalDataForSCC, 0xCD, (EOL_MAX_DATA_LENGTH - EOL_TO_SCC_UNUSED)); //since only (EOL_MAX_DATA_LENGTH - EOL_TO_SCC_UNUSED) bytes are filled in by EOLLib. EOL_TO_SCC_UNUSED are reserved for future use!

	memset(psBlock->pu8Data, 0xCD, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	//Set Specific values for new EOL values in SYSTEM Cal Block
	psBlock->pu8Data[EOLLIB_OFFSET_DTC_MASK_BYTE1] = 0xfd;
	psBlock->pu8Data[EOLLIB_OFFSET_DTC_MASK_BYTE2] = 0xf9;
	psBlock->pu8Data[EOLLIB_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE1] = 0x81;
	psBlock->pu8Data[EOLLIB_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE2] = 0x80;

	u8ExpectedEOLCalDataForSCC[CANEOL_DP_OFFSET_DTC_MASK_BYTE1] = 0xfd;
	u8ExpectedEOLCalDataForSCC[CANEOL_DP_OFFSET_DTC_MASK_BYTE2] = 0xf9;
	u8ExpectedEOLCalDataForSCC[CANEOL_DP_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE1] = 0x81;
	u8ExpectedEOLCalDataForSCC[CANEOL_DP_OFFSET_SUBNET_CONFIG_LIST_TLIN_BYTE2] = 0x80;
	//------ END ------

	psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]);
	memset(psBlock->pu8Data, 0xCD, EOLLIB_OFFSET_CAL_HMI_BRAND_END);

	//Set Specific values for new EOL values in BRAND Cal Block
	psBlock->pu8Data[EOLLIB_OFFSET_MAIN_DISP_FAILSOFT] = 0xfe;
	psBlock->pu8Data[EOLLIB_OFFSET_MAIN_DISP_FAILSOFT+1] = 0x43;
	u8ExpectedEOLCalDataForSCC[EOL_DP_OFFSET_MAIN_DISP_FAILSOFT] = 0xfe;
	u8ExpectedEOLCalDataForSCC[EOL_DP_OFFSET_MAIN_DISP_FAILSOFT+1] = 0x43;
	//------ END ------

	EXPECT_EQ(OSAL_E_NOERROR, s32PrepareSCCEOLData(m_ptsShMemEOLLIB, u8EOLCalDataForSCC));
	EXPECT_FALSE(memcmp(u8ExpectedEOLCalDataForSCC, u8EOLCalDataForSCC, EOL_MAX_DATA_LENGTH));
}

//--- End of s32PrepareSCCEOLData_Test --

//--- Start of s32PrepareAndSendDataToSCC_Test --
void s32PrepareAndSendDataToSCC_Test::SetUp()
{
	EOLLib_Test::SetUp();
	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]), EOLLIB_TABLE_ID_SYSTEM,          
		(tPCS8) EOLLIB_TABLE_NAME_SYSTEM, EN_FFD_DATA_SET_EOL_SYSTEM,
		EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]), EOLLIB_TABLE_ID_BRAND,        
		(tPCS8) EOLLIB_TABLE_NAME_BRAND, EN_FFD_DATA_SET_EOL_BRAND,
		EOLLIB_OFFSET_CAL_HMI_BRAND_END);

}

//Invalid arguments - do not send data to SCC
TEST_F(s32PrepareAndSendDataToSCC_Test, onPassingNullSharedMemoryPtr_Return_InvalidValue)
{
	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32PrepareAndSendDataToSCC(OSAL_NULL));
}

TEST_F(s32PrepareAndSendDataToSCC_Test, onPassingBRANDTableAndIfEOLDataConfigForSCCIsWrong_Return_InvalidValue)
{
	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]), EOLLIB_TABLE_ID_BRAND,        
		(tPCS8) EOLLIB_TABLE_NAME_BRAND, EN_FFD_DATA_SET_EOL_BRAND,
		10);//Length set to 10 instead EOLLIB_OFFSET_CAL_HMI_BRAND_END to check for negative test case in s32PrepareAndSendDataToSCC() function
	
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.Times(0);

	//Error in Configuration of sDiagEOL_Table to send data to SCC. 
	//Here BRAND Block is set to very less length to simulate/test error condtion.
	//In ideal case if developer configure sDiagEOL_Table wrongly, this error should get triggered!
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(HasSubstr("s32PrepareSCCEOLData - INVALID Offset")) )
		.Times(1);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32PrepareAndSendDataToSCC(m_ptsShMemEOLLIB));
}

TEST_F(s32PrepareAndSendDataToSCC_Test,  onPassingValidSharedMemoryWithVaidEOLBlocks_Return_DatapoolReturnValue)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(-1));
	
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(0);

	EXPECT_EQ(-1, s32PrepareAndSendDataToSCC(m_ptsShMemEOLLIB));
}

//--- End of s32PrepareAndSendDataToSCC_Test -

//--- Start of bCheckEOLBlockAndSendEOLDataToSCC_Test --
void bCheckEOLBlockAndSendEOLDataToSCC_Test::SetUp()
{
	EOLLib_Test::SetUp();
	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]), EOLLIB_TABLE_ID_SYSTEM,          
		(tPCS8) EOLLIB_TABLE_NAME_SYSTEM, EN_FFD_DATA_SET_EOL_SYSTEM,
		EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]), EOLLIB_TABLE_ID_BRAND,        
		(tPCS8) EOLLIB_TABLE_NAME_BRAND, EN_FFD_DATA_SET_EOL_BRAND,
		EOLLIB_OFFSET_CAL_HMI_BRAND_END);

}

//Invalid arguments - do not send data to SCC
TEST_F(bCheckEOLBlockAndSendEOLDataToSCC_Test, onPassingNullSharedMemoryPtr_Return_False)
{		
	EXPECT_EQ(FALSE, bCheckEOLBlockAndSendEOLDataToSCC(OSAL_NULL, 0));
}

TEST_F(bCheckEOLBlockAndSendEOLDataToSCC_Test, onPassingInvalidEOLBlockTableID_Return_False)
{		
	EXPECT_EQ(FALSE, bCheckEOLBlockAndSendEOLDataToSCC(m_ptsShMemEOLLIB, 0));
}

TEST_F(bCheckEOLBlockAndSendEOLDataToSCC_Test, onPassingBluetoothEOLBlockTableID_Return_False)
{
	EXPECT_EQ(FALSE, bCheckEOLBlockAndSendEOLDataToSCC(m_ptsShMemEOLLIB, BLUETOOTH));
}

//Sending data to SCC fails, write failure info in error memory
TEST_F(bCheckEOLBlockAndSendEOLDataToSCC_Test, onPassingValidEOLBlockTableIDAndIfSCCSendFailed_WriteErrMemAndReturn_True)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(-1));
	
	//If sending data to SCC fails, it is expected also to write failure info in error memory!
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(TRUE, bCheckEOLBlockAndSendEOLDataToSCC(m_ptsShMemEOLLIB, SYSTEM));
}

TEST_F(bCheckEOLBlockAndSendEOLDataToSCC_Test, onPassingBRANDTableAndIfEOLDataConfigForSCCIsWrong_WriteErrMemAndReturn_True)
{
	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]), EOLLIB_TABLE_ID_BRAND,        
		(tPCS8) EOLLIB_TABLE_NAME_BRAND, EN_FFD_DATA_SET_EOL_BRAND,
		10);//Length set to 10 instead of EOLLIB_OFFSET_CAL_HMI_BRAND_END to check for negative test case in s32PrepareAndSendDataToSCC() function
	
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.Times(0);

	//Error in Configuration of sDiagEOL_Table to send data to SCC. 
	//Here BRAND Block is set to very less length to simulate/test error condtion.
	//In ideal case if developer configure sDiagEOL_Table wrongly, this error should get triggered!
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(HasSubstr("s32PrepareSCCEOLData - INVALID Offset")) )
		.Times(1);
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(HasSubstr("Error occured while sending data to SCC")) )
		.Times(1);

	EXPECT_EQ(TRUE, bCheckEOLBlockAndSendEOLDataToSCC(m_ptsShMemEOLLIB, BRAND));
}

//Positive test cases - Sending data to SCC is successful
TEST_F(bCheckEOLBlockAndSendEOLDataToSCC_Test, onPassingSYSTEMEOLBlockTableID_Return_True)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(0));

	EXPECT_EQ(TRUE, bCheckEOLBlockAndSendEOLDataToSCC(m_ptsShMemEOLLIB, SYSTEM));
}

TEST_F(bCheckEOLBlockAndSendEOLDataToSCC_Test, onPassingBRANDEOLBlockTableID_Return_True)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(0));

	EXPECT_EQ(TRUE, bCheckEOLBlockAndSendEOLDataToSCC(m_ptsShMemEOLLIB, BRAND));
}

//--- End of bCheckEOLBlockAndSendEOLDataToSCC_Test ---

//--- Start of s32SyncDatapoolSCC_Test --
void s32SyncDatapoolSCC_Test::SetUp()
{
	EOLLib_Test::SetUp();
	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]), EOLLIB_TABLE_ID_SYSTEM,          
		(tPCS8) EOLLIB_TABLE_NAME_SYSTEM, EN_FFD_DATA_SET_EOL_SYSTEM,
		EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	vInitBlock(&(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]), EOLLIB_TABLE_ID_BRAND,        
		(tPCS8) EOLLIB_TABLE_NAME_BRAND, EN_FFD_DATA_SET_EOL_BRAND,
		EOLLIB_OFFSET_CAL_HMI_BRAND_END);

}

//Invalid arguments - do nothing
TEST_F(s32SyncDatapoolSCC_Test, onPassingNullSharedMemoryPtr_Return_FALSE)
{
	EXPECT_EQ(FALSE, s32SyncDatapoolSCC(OSAL_NULL));
}

TEST_F(s32SyncDatapoolSCC_Test, onPassingValidShMemButSCCReadFails_Return_FALSEAndMakeErrMemEntry)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.WillOnce(Return(-1));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(HasSubstr("Error occured while reading SCC datapool")) )
		.Times(1);

	EXPECT_EQ(FALSE, s32SyncDatapoolSCC(m_ptsShMemEOLLIB));
}

TEST_F(s32SyncDatapoolSCC_Test, onPassingValidShMemAndSCCDPareInSyncWithiMxDP_Return_FALSE)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.WillOnce(Return(EOL_MAX_DATA_LENGTH));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.Times(0);

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(0);

	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	memset(psBlock->pu8Data, 0x00, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	EXPECT_EQ(FALSE, s32SyncDatapoolSCC(m_ptsShMemEOLLIB));
}

TEST_F(s32SyncDatapoolSCC_Test, onPassingValidShMemAndDPsNotInSyncSCCDPwriteFails_Return_FALSEAndMakeErrMemEntry)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.WillOnce(Return(EOL_MAX_DATA_LENGTH));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(-1));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(HasSubstr("Error occured while synchronizing SCC datapool")))
		.Times(1);

	//Set SYSTEM Cal block with some value. SCC s32GetData() mock function will return zero. 
	//Hence both iMx and SCC will not be the same.
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	memset(psBlock->pu8Data, 0xCD, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	EXPECT_EQ(FALSE, s32SyncDatapoolSCC(m_ptsShMemEOLLIB));
}

TEST_F(s32SyncDatapoolSCC_Test, onPassingValidShMemAndSCCDPNotInSyncWithiMxDP_Return_TRUE)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.WillOnce(Return(EOL_MAX_DATA_LENGTH));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(0);

	//Set SYSTEM Cal block with some value. SCC s32GetData() mock function will return zero. 
	//Hence both iMx and SCC will not be the same.
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	memset(psBlock->pu8Data, 0xCD, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	EXPECT_EQ(TRUE, s32SyncDatapoolSCC(m_ptsShMemEOLLIB));
}

TEST_F(s32SyncDatapoolSCC_Test, onPassingValidShMemAndSCCDPNotInSyncWithiMxDPv2_Return_TRUE)
{
	//Set DP values for SCC side
	tU8 u8EOLCalSccData[EOL_MAX_DATA_LENGTH] = {0};
	memset(u8EOLCalSccData, 0xCD, EOL_MAX_DATA_LENGTH);
	u8EOLCalSccData[EOL_MAX_DATA_LENGTH-1] = 0x00; //Change one byte of data to make iMx & SCC DPs out of sync.

	//Set SYSTEM Cal block with some value for iMx side.
	//Hence both iMx and SCC will not be the same.
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	memset(psBlock->pu8Data, 0xCD, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOL_MAX_DATA_LENGTH))
		.WillOnce
		(DoAll
			( SetArrayArgument<0>( &u8EOLCalSccData[0], &u8EOLCalSccData[EOL_MAX_DATA_LENGTH] ), 
			  Return(EOL_MAX_DATA_LENGTH) 
			) 
		);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(0);

	EXPECT_EQ(TRUE, s32SyncDatapoolSCC(m_ptsShMemEOLLIB));
}

//--- End of s32SyncDatapoolSCC_Test --

//--- Start of s32ReadBlockFromDatapool_Test --

TEST_F(s32ReadBlockFromDatapool_Test, onPassingNullEOLBlockPtr_Return_InvalidValue)
{
	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32ReadBlockFromDatapool(OSAL_NULL));
}

TEST_F(s32ReadBlockFromDatapool_Test, onPassingInvalidBlock_Return_InvalidValue)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	psBlock->u8TableId = 0x00;
	psBlock->pu8Data[0] = 0x00;

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.Times(0);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.Times(0);

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(0x00, psBlock->u8TableId); //Invalid TableId passed
	psBlock->u8TableId = SYSTEM;
}

TEST_F(s32ReadBlockFromDatapool_Test, onPassingSYSTEMEOLBlockAndHeaderReadFail_Return_IOError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	psBlock->pu8Data[0] = 0x01;

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.WillOnce(Return(-1));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Datapool read from RAW_NOR Failed") ))
		.Times(1);

	EXPECT_EQ(OSAL_E_IOERROR, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(SYSTEM, psBlock->u8TableId);
}

TEST_F(s32ReadBlockFromDatapool_Test, onPassingBRANDEOLBlockAndBlockReadFail_Return_IOError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]);
	psBlock->pu8Data[0] = 0x01;

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_BRAND_END))
		.WillOnce(Return(-1));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Datapool read from RAW_NOR Failed") ))
		.Times(1);

	EXPECT_EQ(OSAL_E_IOERROR, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(BRAND, psBlock->u8TableId);
}

TEST_F(s32ReadBlockFromDatapool_Test, onPassingBLUETOOTHEOLBlockAndAllReadFail_Return_IOError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BLUETOOTH)]);
	psBlock->pu8Data[0] = 0x01;

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.WillOnce(Return(-1));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END))
		.WillOnce(Return(-1));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Datapool read from RAW_NOR Failed") ))
		.Times(1);

	EXPECT_EQ(OSAL_E_IOERROR, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(BLUETOOTH, psBlock->u8TableId);
}

//At Virgin Startup (brand new device), calibration block datas will not yet be available in RAW_NOR
TEST_F(s32ReadBlockFromDatapool_Test, onPassingCOUNTRYEOLBlockAndVirginStartUp_Return_InvalidValue)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(COUNTRY)]);
	psBlock->pu8Data[0] = 0x00;

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_COUNTRY_END))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Datapool read from RAW_NOR returned Default Values") ))
		.Times(1);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(COUNTRY, psBlock->u8TableId);
}

//Normal Case
TEST_F(s32ReadBlockFromDatapool_Test, onPassingSPEECHRECEOLBlockAndReadSuccess_Return_NoError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SPEECH_RECOGNITION)]);
	psBlock->pu8Data[0] = 0x01;

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END))
		.WillOnce(Return(0));

	EXPECT_EQ(OSAL_E_NOERROR, s32ReadBlockFromDatapool(psBlock) );
}

TEST_F(s32ReadBlockFromDatapool_Test, onPassingRVCEOLBlockAndReadSuccess_Return_NoError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(REAR_VISION_CAMERA)]);

	psBlock->pu8Data[0] = 0x01;
	tsDiagEOLBlockHeader tSystemHeaderParam = {REAR_VISION_CAMERA, {0}, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END};

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.WillOnce( DoAll( SetArgReferee<0>(tSystemHeaderParam), Return(0)) );

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END))
		.WillOnce(Return(0));

	EXPECT_EQ(OSAL_E_NOERROR, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(REAR_VISION_CAMERA, psBlock->u8TableId);
}

//Default EOL Calibration block data stored in RAW_NOR
TEST_F(s32ReadBlockFromDatapool_Test, onPassingNAVSYSTEMEOLBlockAndReadSuccess_Return_NoError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(NAVIGATION_SYSTEM)]);

	psBlock->pu8Data[0] = 0x00;
	tsDiagEOLBlockHeader tSystemHeaderParam = {NAVIGATION_SYSTEM, {0}, EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END};

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_) )
		.WillOnce( DoAll( SetArgReferee<0>(tSystemHeaderParam), Return(0)) );

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END))
		.WillOnce(Return(0));

	EXPECT_EQ(OSAL_E_NOERROR, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(NAVIGATION_SYSTEM, psBlock->u8TableId);
}

TEST_F(s32ReadBlockFromDatapool_Test, onPassingDISPLAYINTERFACEEOLBlockAndReadSuccess_Return_NoError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(DISPLAY_INTERFACE)]);

	psBlock->pu8Data[0] = 0x00;
	tsDiagEOLBlockHeader tSystemHeaderParam = {NAVIGATION_SYSTEM, {0xFF}, EOLLIB_OFFSET_CAL_HMI_F_B_END}; //Simulate as if we read some non-zero Header value from RAW_NOR


	ASSERT_EQ(0x00, psBlock->Header[0]);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_) )
		.WillOnce( DoAll( SetArgReferee<0>(tSystemHeaderParam), Return(0)) );

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_F_B_END))
		.WillOnce(Return(0));

	EXPECT_EQ(OSAL_E_NOERROR, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(DISPLAY_INTERFACE, psBlock->u8TableId);
	ASSERT_EQ(0xFF, psBlock->Header[0]);
}

TEST_F(s32ReadBlockFromDatapool_Test, onReadingHFTUNINGEOLBlockAndReadFullHeaderAndBlockDataSuccess_Return_NoError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(HAND_FREE_TUNING)]);
	tsDiagEOLBlockHeader tSystemHeaderParam = {NAVIGATION_SYSTEM, {0xFF}, EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END};
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END] = {0};

	psBlock->pu8Data[0] = 0x00;
	memset(u8Data, 0xFF, EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END);

	ASSERT_EQ(0x00, psBlock->Header[0]);

	//Simulate as if we read some non-zero Header & block datas from RAW_NOR
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_) )
		.WillOnce( DoAll( SetArgReferee<0>(tSystemHeaderParam), Return(0)) );

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END))
		.WillOnce
		(DoAll
			( SetArrayArgument<0>( &u8Data[0], &u8Data[EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END] ), 
			  Return(0) 
			) 
		);

	EXPECT_EQ(OSAL_E_NOERROR, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(HAND_FREE_TUNING, psBlock->u8TableId);

	ASSERT_EQ(0xFF, psBlock->Header[0]);
	ASSERT_FALSE( memcmp( u8Data, psBlock->pu8Data, EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END) );//memcmp() returns zero if both buffer contents are equal
}

TEST_F(s32ReadBlockFromDatapool_Test, onReadingNAV_ICONEOLBlockAndReadFullHeaderAndBlockDataSuccess_Return_NoError)
{
	tU8 u8HeaderData[EOLLIB_HEADER_SIZE] = {0};
	tU8 u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END] = {0};
	tsDiagEOLBlockHeader tSystemHeaderParam = {NAVIGATION_ICON, {0x00}, EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END};

	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(NAVIGATION_ICON)]);

	memset(u8HeaderData, 0xAA, EOLLIB_HEADER_SIZE);
	memcpy(tSystemHeaderParam.Header, 
			u8HeaderData,
			sizeof(tSystemHeaderParam.Header));
	memset(u8Data, 0xEE, EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END);
	psBlock->pu8Data[0] = 0x00;

	ASSERT_EQ(0x00, psBlock->Header[0]);

	//Simulate as if we read some non-zero Header & block datas from RAW_NOR
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_) )
		.WillOnce( DoAll( SetArgReferee<0>(tSystemHeaderParam), Return(0)) );

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END))
		.WillOnce
		(DoAll
			( SetArrayArgument<0>( &u8Data[0], &u8Data[EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END] ), 
			  Return(0) 
			) 
		);

	EXPECT_EQ(OSAL_E_NOERROR, s32ReadBlockFromDatapool(psBlock) );
	ASSERT_EQ(NAVIGATION_ICON, psBlock->u8TableId);

	ASSERT_FALSE( memcmp( u8HeaderData, &psBlock->Header[0], EOLLIB_HEADER_SIZE) );//memcmp() returns zero if both buffer contents are equal
	ASSERT_FALSE( memcmp( u8Data, psBlock->pu8Data, EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END) );
}

//--- End of s32ReadBlockFromDatapool_Test --

//--- Start of s32WriteBlockToDatapool_Test --

TEST_F(s32WriteBlockToDatapool_Test, onPassingNullEOLBlockPtr_Return_InvalidValue)
{
	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32WriteBlockToDatapool(OSAL_NULL));
}

TEST_F(s32WriteBlockToDatapool_Test, onPassingInvalidBlock_Return_InvalidValue)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	psBlock->u8TableId = 0x00;

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_))
		.Times(0);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.Times(0);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32WriteBlockToDatapool(psBlock) );
	ASSERT_EQ(0x00, psBlock->u8TableId); //Invalid TableId passed
	psBlock->u8TableId = SYSTEM;
}

//--- End of s32WriteBlockToDatapool_Test --

//--- Start of s32WriteBlockToDatapool_PTest --

tsDiagEOLBlockHeader tExpectedEOLHeader;

MATCHER(matchEOLHeader, "") 
{  
	bool bMatchResult = false;
	const tsDiagEOLBlockHeader &tActualEOLHeader = arg;
	if(	(tExpectedEOLHeader.u8TableId == tActualEOLHeader.u8TableId) &&
		(tExpectedEOLHeader.u16Length == tActualEOLHeader.u16Length) &&
		(memcmp(tExpectedEOLHeader.Header, tActualEOLHeader.Header, EOLLIB_HEADER_SIZE) == 0)
		)
	{
		bMatchResult = true;
	}
	return bMatchResult;	
}

INSTANTIATE_TEST_CASE_P(EOLLIB_WriteBlockToDatapool_Param_Test,
                        s32WriteBlockToDatapool_PTest,
						::testing::Values
						(
							//HeaderRetVal, BlockRetVal, Table Id,		DPRetVal,		EOL Data Block Length
							//Error Test Case
							make_tuple(-1,  0, SYSTEM, 				OSAL_E_IOERROR,		EOLLIB_OFFSET_CAL_HMI_SYSTEM_END),
							make_tuple( 0, -1, DISPLAY_INTERFACE,	OSAL_E_IOERROR,		EOLLIB_OFFSET_CAL_HMI_F_B_END),
							make_tuple(-1, -1, BLUETOOTH,			OSAL_E_IOERROR,		EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END),
							//Normal Test Case
							make_tuple( 0,	0, NAVIGATION_SYSTEM,	OSAL_E_NOERROR,		EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END),
							make_tuple( 0,	0, NAVIGATION_ICON,		OSAL_E_NOERROR,		EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END),
							make_tuple( 0,	0, BRAND,				OSAL_E_NOERROR,		EOLLIB_OFFSET_CAL_HMI_BRAND_END),
							make_tuple( 0,	0, COUNTRY,				OSAL_E_NOERROR,		EOLLIB_OFFSET_CAL_HMI_COUNTRY_END),
							make_tuple( 0,	0, SPEECH_RECOGNITION,	OSAL_E_NOERROR,		EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END),
							make_tuple( 0,	0, HAND_FREE_TUNING,	OSAL_E_NOERROR,		EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END),
							make_tuple( 0,	0, REAR_VISION_CAMERA,	OSAL_E_NOERROR,		EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END)
						)
					  );

void s32WriteBlockToDatapool_PTest::SetUpTestCase()
{
	EOLLib_BaseTest::vEOLLib_SetUpTestCase();
	vInitEOLBlocks(m_ptsShMemEOLLIB); //Init all EOL Cal Blocks
}

void s32WriteBlockToDatapool_PTest::TearDownTestCase()
{
	EOLLib_BaseTest::vEOLLib_TearDownTestCase();
}

void s32WriteBlockToDatapool_PTest::SetUp()
{
	vCreateMockObjects();
	
	s8HeaderReturnValue = get<0>(GetParam());
	s8DataBlockReturnValue = get<1>(GetParam());
	u8TableId = get<2>(GetParam());
	u32DatapoolReturnValue = get<3>(GetParam());
	u16EOLBlockLength = get<4>(GetParam());
}

void s32WriteBlockToDatapool_PTest::TearDown()
{
	vDeleteMockObjects();
}

TEST_P(s32WriteBlockToDatapool_PTest, testEOLBlocksDatapoolWriteFunction)
{	
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(u8TableId)]);
	
	psBlock->pu8Data[0] = u8TableId * u8TableId; //Write some value in last valid byte of this EOL block & verify it in EXPECT_CALL below!
	memset(psBlock->Header, u8TableId, sizeof(psBlock->Header)); //Write some value in header & verify it in EXPECT_CALL below!

	memset(tExpectedEOLHeader.Header, u8TableId, sizeof(tExpectedEOLHeader.Header));
	tExpectedEOLHeader.u16Length = u16EOLBlockLength;
	tExpectedEOLHeader.u8TableId = u8TableId;

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData( matchEOLHeader() ))
		.WillOnce(Return(s8HeaderReturnValue));

	EXPECT_CALL(*m_pMockEOLLibDPObj, 
		s32SetData( AllOf( 
							Eq(psBlock->pu8Data), Pointee( Eq(psBlock->pu8Data[0]) )
						 ), 
					u16EOLBlockLength)
				  )
				  .WillOnce(Return(s8DataBlockReturnValue));

	EXPECT_EQ(u32DatapoolReturnValue, s32WriteBlockToDatapool(psBlock) );
	ASSERT_EQ(u8TableId, psBlock->u8TableId); //Just to make sure in Mock call we don't accidently overwirte other datas of EOLBlock!
}

//--- End of s32WriteBlockToDatapool_PTest --
//--- Start of s32ReadEOLBlocksFromDatapool_Test --

void s32ReadEOLBlocksFromDatapool_Test::SetUp()
{
	EOLLib2_Test::SetUp();
	//Reset the EOL block datas
	tsDiagEOLBlock* psBlock = NULL;
	for (tU32 i=0;i<EOL_BLOCK_NBR;i++)
	{
		psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[i]);
		psBlock->pu8Data[0] = 0x00;
	}
}

TEST_F(s32ReadEOLBlocksFromDatapool_Test, onPassingNullSharedMemoryPtr_Return_InvalidValue)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.Times(0);
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.Times(0);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32ReadEOLBlocksFromDatapool(OSAL_NULL));
}

TEST_F(s32ReadEOLBlocksFromDatapool_Test, onPassingValidSharedMemoryPtrAndDatapoolReadVirginStartup_Return_IOError)
{
	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromDatapool_Test
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.Times(1)
		.WillRepeatedly(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.Times(1)
		.WillRepeatedly(Return(0));
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_)) //Since Virgin Startup!
		.Times(1);

	EXPECT_EQ(OSAL_E_IOERROR, s32ReadEOLBlocksFromDatapool(m_ptsShMemEOLLIB));
}

TEST_F(s32ReadEOLBlocksFromDatapool_Test, onPassingValidSharedMemoryPtrAndDatapoolReadFails_Return_IOError)
{
	//Init EOL block data with valid value (simulating as if EOL Block read is successfull
	tsDiagEOLBlock* psBlock = NULL;
	for (tU32 i=0;i<EOL_BLOCK_NBR;i++)
	{
		psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[i]);
		psBlock->pu8Data[0] = 0x01;
	}

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromDatapool_Test
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.Times(3)
		.WillOnce(Return(0))
		.WillOnce(Return(0))
		.WillRepeatedly(Return(-1));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.Times(3)
		.WillOnce(Return(0))
		.WillOnce(Return(0))
		.WillRepeatedly(Return(-1));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_)) //Since DP read fails!
		.Times(1);
	
	EXPECT_EQ(OSAL_E_IOERROR, s32ReadEOLBlocksFromDatapool(m_ptsShMemEOLLIB));
}

TEST_F(s32ReadEOLBlocksFromDatapool_Test, onPassingValidSharedMemoryPtrAndAllDatapoolReadSuccess_Return_NoError)
{
	//Init EOL block data with valid value in mock (simulating as if EOL Block read is successfull

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromDatapool_Test
	setExpectationDPReadOfAllEOLBlocksHeaderAndData_Success();

	EXPECT_EQ(OSAL_E_NOERROR, s32ReadEOLBlocksFromDatapool(m_ptsShMemEOLLIB));
}

//--- End of s32ReadEOLBlocksFromDatapool_Test --

//--- Start of vReadEOLBlocks_Test --

void vReadEOLBlocks_Test::SetUp()
{
	EOLLib2_Test::SetUp();
	
	//Reset the EOL block datas
	tsDiagEOLBlock* psBlock = NULL;
	for (tU32 i=0;i<EOL_BLOCK_NBR;i++)
	{
		psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[i]);
		psBlock->pu8Data[0] = 0x00;
		memset(psBlock->Header, 0x00, sizeof(psBlock->Header));
	}
}

TEST_F(vReadEOLBlocks_Test, onPassingNullSharedMemoryPtr_NoActionShouldHappen)
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.Times(0);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.Times(0);
	vReadEOLBlocks(OSAL_NULL);
}

TEST_F(vReadEOLBlocks_Test, onPassingValidSharedMemoryPtrAndDatapoolReadSuccess_ShouldExitFunction)
{
	//Init EOL block data with valid value in mock (simulating as if EOL Block read is successfull

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromDatapool_Test
	setExpectationDPReadOfAllEOLBlocksHeaderAndData_Success();

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(0);

	vReadEOLBlocks(m_ptsShMemEOLLIB);
}

TEST_F(vReadEOLBlocks_Test, onPassingValidSharedMemoryPtrAndDPReadVirginStartupAndP3PartitionNotMounted_ShouldReadFromDefault)
{
	struct timespec vTimer={P3_FFD_MAX_MOUNT_TIME, 0}; //System time elapsed more than minimum expected P3 mount time
	struct stat s1;
	memset(&s1, 0x00, sizeof(s1));

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromDatapool_Test
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.Times(1)
		.WillRepeatedly(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.Times(1)
		.WillRepeatedly(Return(0));

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(10)
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(3);
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("P3 partition mount failed") ))
		.Times(1);

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, stat(_, _))
		.Times(5)
		.WillRepeatedly(DoAll( SetArgPointee<1>(s1), Return(0) ) ); //We must update param-2, otherwise leaving the default/junk value of var-s1/s2 in pCheckIfPathIsMounted() might result in unexpected test result!

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, clock_gettime(_, _))
		.WillOnce(DoAll(SetArgPointee<1>(vTimer), Return(0)));

	vReadEOLBlocks(m_ptsShMemEOLLIB);
}

TEST_F(vReadEOLBlocks_Test, onPassingValidSharedMemoryPtrAndDatapoolAndFFDReadFails_ShouldReadFromDefault)
{
	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromDatapool_Test
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.Times(3)
		.WillOnce(Return(0))
		.WillOnce(Return(0))
		.WillRepeatedly(Return(-1));//Simulating that 1st and 2nd DP read is successful but 3rd DP Read fails! (Simulating different failure scenario)

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.Times(3)
		.WillOnce(DoAll(SetArgPointee<0>(0x01) ,Return(0)) )
		.WillOnce(DoAll(SetArgPointee<0>(0x01) ,Return(0)) )
		.WillRepeatedly(Return(-1));
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(11)
		.WillRepeatedly(Return(OSAL_ERROR));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_)) //Since DP Read Failed!
		.Times(3);
	
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Reading EOL blocks from FFD failed") ))
		.Times(1);

	setExpectationP3PartitionMount_Success();
	vReadEOLBlocks(m_ptsShMemEOLLIB);
}

TEST_F(vReadEOLBlocks_Test, onPassingValidSharedMemoryPtrAndDatapoolReadFailsFFDSuccess_ShouldReadFromFFD)
{
	initEOLBlocksWithValidHeaderAndSetBlockAsCalibrated();
	tsDiagEOLBlockHeader tSystemHeaderParam = {SYSTEM, {0}, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END};
	memcpy(tSystemHeaderParam.Header, 
			getEOLBlockHeaderInfo(SYSTEM),
			sizeof(tSystemHeaderParam.Header));

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromDatapool_Test
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_) )
		.WillOnce( DoAll( SetArgReferee<0>(tSystemHeaderParam), Return(0)) );

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.WillOnce(DoAll(SetArgPointee<0>(0x01) ,Return(-1)) );

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(2);

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Reading EOL blocks from FFD is successful") ))
		.Times(1);

	setExpectationP3PartitionMount_Success();
	setExpectationFFDReadOfAllEOLBlocks_Success();
	vReadEOLBlocks(m_ptsShMemEOLLIB);
}

//--- End of vReadEOLBlocks_Test --

#endif //VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

