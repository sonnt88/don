
#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS 

TEST(bIsFFDEOLBlockCompatibile, onPassingNullBlock_Return_False)
{
	EXPECT_EQ(FALSE, bIsFFDEOLBlockCompatibile(OSAL_NULL));
}

TEST(bIsFFDEOLBlockCompatibile, onPassingNonCalibratedEOLBlock_Return_False)
{
	tsDiagEOLBlock tBlock;
	tBlock.pu8Data[0] = 0x00;

	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.Cal_Form_ID = (tU16) EOLLIB_NEW_C3_CAL_FORM_ID;
	memcpy(tBlock.Header, &tEOLDataSetHeader, sizeof(tBlock.Header));

	MockEOLLibCFunctionDependency mEOLDepObj;
	EXPECT_CALL(mEOLDepObj, vWritePrintfErrmemMock(_))
		.Times(1);
	/*EXPECT_CALL(mEOLDepObj, vWritePrintfErrmemMock(Matcher<const char*>(StrNe(""))))
		.Times(1);*/
	
	EXPECT_EQ(FALSE, bIsFFDEOLBlockCompatibile(&tBlock));
}

TEST(bIsFFDEOLBlockCompatibile, onPassingBlockWithIncompatibleVersion_Return_False)
{
	tsDiagEOLBlock tBlock;
	tBlock.pu8Data[0] = 0x01;

	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.Cal_Form_ID = (tU16) 0x0401; //old EOL Header version (prior to C3 sample)
	memcpy(tBlock.Header, &tEOLDataSetHeader, sizeof(tBlock.Header));

	MockEOLLibCFunctionDependency mEOLDepObj;
	EXPECT_CALL(mEOLDepObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(FALSE, bIsFFDEOLBlockCompatibile(&tBlock));
}

TEST(bIsFFDEOLBlockCompatibile, onPassingCalibratedBlockWithCompatibleHeader_Return_True)
{
	tsDiagEOLBlock tBlock;
	tBlock.pu8Data[0] = 0x01; //Value is 0x01, if EOL calibration is done

	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.Cal_Form_ID = (tU16) EOLLIB_NEW_C3_CAL_FORM_ID;
	memcpy(tBlock.Header, &tEOLDataSetHeader, sizeof(tBlock.Header));

	EXPECT_EQ(TRUE, bIsFFDEOLBlockCompatibile(&tBlock));
}

TEST(dataConfigEOLTableTest, testNoOfCalBlocksReqTransferToSCC)
{
	tU8 u8SizeOfTable = (sizeof(szU8EOLBlock_Trnsf_to_SCC) / sizeof(tU8));
	EXPECT_EQ(EOL_MAX_CAL_BLOCK_TRNSF_TO_SCC, u8SizeOfTable);
}

TEST(dataConfigSCCDPOffsetTest, testSCCDPOffset)
{
	tU8 u8SizeOfTable = (sizeof(sDiagEOL_Table) / sizeof(tsDiag_EOL_SCC_Calib_Table));
	for(tU8 i = 0; i < u8SizeOfTable; i++)
	{
		EXPECT_LT(sDiagEOL_Table[i].u8DPOffset, EOL_MAX_DATA_LENGTH/*SCC_EOL_MAX_PARAMETER_NBR*/); //TODO: Something wrong. Check once here!
	}
}

TEST(dataConfigEOLOffsetTest, testEOLOffsetValidity)
{
	tU8 u8SizeOfTable = (sizeof(sDiagEOL_Table) / sizeof(tsDiag_EOL_SCC_Calib_Table));
	tU8 u8TableId = 0x00;
	tU16 u16EOLTableLength[EOL_BLOCK_NBR] = 
	{
		EOLLIB_OFFSET_CAL_HMI_SYSTEM_END,
		EOLLIB_OFFSET_CAL_HMI_F_B_END,
		EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END,
		EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END,
		EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END,
		EOLLIB_OFFSET_CAL_HMI_BRAND_END,
		EOLLIB_OFFSET_CAL_HMI_COUNTRY_END,
		EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END,
		EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END,
		EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END
	};

	for(tU8 i = 0; i < u8SizeOfTable; i++)
	{
		u8TableId = sDiagEOL_Table[i].u8TableId;
		ASSERT_TRUE(u8TableId);

		u8TableId = (u8TableId - 2);
		ASSERT_LT(u8TableId, EOL_BLOCK_NBR);

		EXPECT_LT(sDiagEOL_Table[i].u16EOLOffset, u16EOLTableLength[u8TableId]);
	}
}
#endif //VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

//--- Start of psGetBlock_Test --

TEST_F(psGetBlock_Test, onPassingNullSharedMemoryPtr_Return_Null)
{
	EXPECT_EQ(OSAL_NULL, psGetBlock(OSAL_NULL, 0));
}

TEST_F(psGetBlock_Test, onPassingInvalidEOLBlockTableID_Return_Null)
{
	EXPECT_EQ(OSAL_NULL, psGetBlock(m_ptsShMemEOLLIB, 0));
}

TEST_F(psGetBlock_Test, onPassingSYSTEMEOLBlockTableID_Return_SystemEOLBlock)
{
	tsDiagEOLBlock* psBlock = psGetBlock(m_ptsShMemEOLLIB, SYSTEM);
	ASSERT_TRUE(psBlock)
		<< "psGetBlock() returned " << ::testing::PrintToString(psBlock);

	EXPECT_EQ(SYSTEM, psBlock->u8TableId);
}

TEST_F(psGetBlock_Test, onPassingRVCEOLBlockTableID_Return_RVCEOLBlock)
{
	tsDiagEOLBlock* psBlock = psGetBlock(m_ptsShMemEOLLIB, REAR_VISION_CAMERA);
	ASSERT_TRUE(psBlock)
		<< "psGetBlock() returned " << ::testing::PrintToString(psBlock);

	EXPECT_EQ(REAR_VISION_CAMERA, psBlock->u8TableId);
}

TEST_F(psGetBlock_Test, onPassingSPEECHEOLBlockTableID_Return_SPEECHEOLBlock)
{
	tsDiagEOLBlock* psBlock = psGetBlock(m_ptsShMemEOLLIB, SPEECH_RECOGNITION);
	ASSERT_TRUE(psBlock)
		<< "psGetBlock() returned " << ::testing::PrintToString(psBlock);

	EXPECT_EQ(SPEECH_RECOGNITION, psBlock->u8TableId);
}

//--- End of psGetBlock_Test ---

//--- Start of vInitEOLBlocks_PTest --

INSTANTIATE_TEST_CASE_P(EOLLIB_InitBlocks_Param_Test,
                        vInitEOLBlocks_PTest,
						::testing::Values
						(
							//Normal Test Case
							make_tuple(0, SYSTEM, 				EN_FFD_DATA_SET_EOL_SYSTEM,		EOLLIB_OFFSET_CAL_HMI_SYSTEM_END),
							make_tuple(1, DISPLAY_INTERFACE,	EN_FFD_DATA_SET_EOL_DISPLAY,	EOLLIB_OFFSET_CAL_HMI_F_B_END),
							make_tuple(2, BLUETOOTH,			EN_FFD_DATA_SET_EOL_BLUETOOTH,	EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END),
							make_tuple(3, NAVIGATION_SYSTEM,	EN_FFD_DATA_SET_EOL_NAV_SYSTEM, EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END),
							make_tuple(4, NAVIGATION_ICON,		EN_FFD_DATA_SET_EOL_NAV_ICON,	EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END),
							make_tuple(5, BRAND,				EN_FFD_DATA_SET_EOL_BRAND,		EOLLIB_OFFSET_CAL_HMI_BRAND_END),
							make_tuple(6, COUNTRY,				EN_FFD_DATA_SET_EOL_COUNTRY,	EOLLIB_OFFSET_CAL_HMI_COUNTRY_END),
							make_tuple(7, SPEECH_RECOGNITION,	EN_FFD_DATA_SET_EOL_SPEECH_REC, EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END),
							make_tuple(8, HAND_FREE_TUNING,		EN_FFD_DATA_SET_EOL_HF_TUNING,	EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END),
							make_tuple(9, REAR_VISION_CAMERA,	EN_FFD_DATA_SET_EOL_RVC,		EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END)
						)
					  );

void vInitEOLBlocks_PTest::SetUpTestCase()
{
	EOLLib_BaseTest::vEOLLib_SetUpTestCase();
	vInitEOLBlocks(m_ptsShMemEOLLIB);
}

void vInitEOLBlocks_PTest::TearDownTestCase()
{
	EOLLib_BaseTest::vEOLLib_TearDownTestCase();
}

void vInitEOLBlocks_PTest::SetUp()
{
	vInit();
	calBlockArrayIndex = get<0>(GetParam());
	u8TableId = get<1>(GetParam());
	enFFDDataSet = get<2>(GetParam());
	u16EOLBlockLength = get<3>(GetParam());
}

TEST_P(vInitEOLBlocks_PTest, checkInitOf_EOLBlock_TableId_BlockLength)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[calBlockArrayIndex]);
	ASSERT_TRUE(psBlock)
		<< "EOL Block is invalid. Value of psBlock = " << ::testing::PrintToString(psBlock);

	EXPECT_EQ(u8TableId, psBlock->u8TableId);
	EXPECT_EQ(enFFDDataSet, psBlock->enDataSet);
	EXPECT_EQ(u16EOLBlockLength, psBlock->u16Length);
}

//--- End of vInitEOLBlocks_PTest ---

//--- Start of bCheckAndWaitForP3PartitionMounting_Test --

TEST_F(bCheckAndWaitForP3PartitionMounting_Test, onP3PartitionNotMountedAndMinimumMountTimeElapsed_Return_False)
{
	setExpectationP3PartitionMount_Fail();
	EXPECT_EQ(FALSE, bCheckAndWaitForP3PartitionMounting());
}

TEST_F(bCheckAndWaitForP3PartitionMounting_Test, onP3PartitionNotMountedUntilMinimumMountTimeElapse_Return_False)
{
	struct timespec vTimer1={3, 0};
	struct timespec vTimer2={4, 0};
	struct timespec vTimer3={P3_FFD_MAX_MOUNT_TIME, 0}; //System time elapsed more than minimum expected P3 mount time
	struct stat s1;
	memset(&s1, 0x00, sizeof(s1));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Waiting for Persistent partition to mount") ))
		.Times(1);

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, clock_gettime(_, _))
		.WillOnce(DoAll(SetArgPointee<1>(vTimer1), Return(0)) )
		.WillOnce(DoAll(SetArgPointee<1>(vTimer2), Return(0)) )
		.WillOnce(DoAll(SetArgPointee<1>(vTimer3), Return(0)) )
		.WillRepeatedly(DoAll(SetArgPointee<1>(vTimer3), Return(0)) );

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, usleep(_))
		.Times(2)
		.WillRepeatedly(Return(0));
	
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, stat(_, _))
		.Times(15)
		.WillRepeatedly(DoAll( SetArgPointee<1>(s1), Return(0) ) ); //We must update param-2, otherwise leaving the default/junk value of var-s1/s2 in pCheckIfPathIsMounted() might result in unexpected test result!

	EXPECT_EQ(FALSE, bCheckAndWaitForP3PartitionMounting());
}

TEST_F(bCheckAndWaitForP3PartitionMounting_Test, onP3PartitionMountSuccessfullyBeforeMinimumMountTimeElapse_Return_True)
{
	setExpectationWaitP3PartitionMount_Success();
	EXPECT_EQ(TRUE, bCheckAndWaitForP3PartitionMounting());
}

//--- End of bCheckAndWaitForP3PartitionMounting_Test --

//--- Start of pCheckIfPathIsMounted_Test --

TEST_F(pCheckIfPathIsMounted_Test, onPassingEmptyPathName_Return_Null)
{
	EXPECT_STREQ(OSAL_NULL, pCheckIfPathIsMounted(OSAL_NULL));
}

TEST_F(pCheckIfPathIsMounted_Test, onPassingPathNameSizeGreaterThanMaxSize_Return_Null)
{
	tU8 u8PathName[FFD_MAX_PATH_NAME_SIZE+2];
	u8PathName[FFD_MAX_PATH_NAME_SIZE+1] = 0x00;
	memset(u8PathName, 0xAA, FFD_MAX_PATH_NAME_SIZE+1);

	EXPECT_STREQ(NULL, pCheckIfPathIsMounted((tCString)&u8PathName[0]));
}

TEST_F(pCheckIfPathIsMounted_Test, onPassingInValidPathName1_Return_Null)
{
	tCString pStrPathName = "var/opt/bosch/persistent";
	EXPECT_STREQ(NULL, pCheckIfPathIsMounted(pStrPathName));
}

TEST_F(pCheckIfPathIsMounted_Test, onPassingInValidPathName2_Return_Null)
{
	tCString pStrPathName = "/var/opts/bosch/persistent";

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, stat(_, _))
		.Times(5)
		.WillRepeatedly(Return(-1));

	EXPECT_STREQ(NULL, pCheckIfPathIsMounted(pStrPathName));
}

TEST_F(pCheckIfPathIsMounted_Test, onPassingValidPathButPartitionNotMounted_Return_Null)
{
	tCString pStrPathName = "/var/opt/bosch/persistent";
	struct stat s1;
	memset(&s1, 0x00, sizeof(s1));	
	s1.st_mode = S_IFDIR;

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, stat(_, _))
		.Times(5)
		.WillRepeatedly(DoAll( SetArgPointee<1>(s1), Return(0) ) );

	EXPECT_STREQ(NULL, pCheckIfPathIsMounted(pStrPathName));
}

TEST_F(pCheckIfPathIsMounted_Test, onPassingInValidMountPath_Return_Null) //Just simulating Invalid mount path from Mocks!
{
	tCString pStrPathName = "/var/opt/bosch/persistent/";
	struct stat s1;
	memset(&s1, 0x00, sizeof(s1));	
	s1.st_mode = S_IFDIR;

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, stat(_, _))
		.Times(2)
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(-1) ) );

	EXPECT_STREQ(NULL, pCheckIfPathIsMounted(pStrPathName));
}

TEST_F(pCheckIfPathIsMounted_Test, onSuccessfulFindingValidMountPartition_Return_True)
{
	tCString pStrPathName = "/var/opt/bosch/persistent";
	tCString pStrExpectedFolderName = "persistent";
	
	struct stat s1, s2;
	s1.st_mode = S_IFDIR;
	s1.st_dev = 0x01;
	
	s2.st_mode = S_IFBLK;
	s2.st_dev = 0x02;
	
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, stat(_,_))
		.Times(3)
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )//Valid Folder Found - /var/opt/bosch/persistent/pdd/
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )//Valid Folder Found - /var/opt/bosch/persistent/ (but same filesystem)
		.WillOnce(DoAll( SetArgPointee<1>(s2), Return(0) ) );//Different Filesystems found - /var/opt/bosch/ & persistent partition mounted on it!

	EXPECT_STREQ(pStrExpectedFolderName, pCheckIfPathIsMounted(pStrPathName));
}

//--- End of pCheckIfPathIsMounted_Test --

//--- Start of vCreateDefaultBlock_Test --

TEST_F(vCreateDefaultBlock_Test, onPassingNullEOLBlock_NoActionShouldHappen)
{
	vCreateDefaultBlock(OSAL_NULL, 0);
}

TEST_F(vCreateDefaultBlock_Test, onIOOpenFail_NoActionShouldHappen)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, OSAL_EN_READONLY))
		.WillOnce(Return(OSAL_ERROR));
	vCreateDefaultBlock(psBlock, 0);
}

TEST_F(vCreateDefaultBlock_Test, onIOOpenSuccessAndRegistryOpenFail_NoActionShouldHappen)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, OSAL_EN_READONLY))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IOControl(_, _, _))
		.WillOnce(Return(OSAL_ERROR));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	vCreateDefaultBlock(psBlock, 0);
}

TEST_F(vCreateDefaultBlock_Test, onIOOpenSuccessAndRegistryOpenSucessdlOpenFail_NoActionShouldHappen)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	tVoid *retOfdlOpen = OSAL_NULL;

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, OSAL_EN_READONLY))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IOControl(_, _, _))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, dlopen(_, _))
		.WillOnce(Return((tVoid *)NULL));

	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	vCreateDefaultBlock(psBlock, 0);
}

TEST_F(vCreateDefaultBlock_Test, onPassingInvalidEOLBlockId_NoActionShouldHappen)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	psBlock->u8TableId = 0;
	tVoid *retOfdlOpen = (tVoid *)0xFFFFFFBB;
	
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, OSAL_EN_READONLY))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IOControl(_, _, _))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, dlopen(_, _))
		.WillOnce(Return(retOfdlOpen));

	vCreateDefaultBlock(psBlock, 0);
	
	psBlock->u8TableId = SYSTEM;//Reset it back!
}

TEST_F(vCreateDefaultBlock_Test, onPassingValidEOLBlockIdButSymbolReadFails_NoActionShouldHappen)
{
	tU8 u8EOLData[EOLLIB_OFFSET_CAL_HMI_SYSTEM_END];
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	tVoid *retOfdlOpen = (tVoid *)0xFFFFFFBB;
	EOLLib_Entry* ptEOLEntry = NULL;

	memset(u8EOLData, 0x00, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, OSAL_EN_READONLY))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IOControl(_, _, _))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, dlopen(_, _))
		.WillOnce(Return(retOfdlOpen));
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, dlsym(_, _))
		.WillOnce(Return(ptEOLEntry));

	vCreateDefaultBlock(psBlock, 0);
	EXPECT_FALSE(memcmp(psBlock->pu8Data, u8EOLData, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END) );
}

//--- End of vCreateDefaultBlock_Test --

//--- Start of vCreateDefaultBlock_PTest --

INSTANTIATE_TEST_CASE_P(EOLLIB_vCreateDefaultBlock_Param_Test,
                        vCreateDefaultBlock_PTest,
						::testing::Values
						(
							//Normal Test Case
							make_tuple(SYSTEM, 				EOLLIB_OFFSET_CAL_HMI_SYSTEM_END),
							make_tuple(DISPLAY_INTERFACE,	EOLLIB_OFFSET_CAL_HMI_F_B_END),
							make_tuple(BLUETOOTH,			EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END),
							make_tuple(NAVIGATION_SYSTEM,	EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END),
							make_tuple(NAVIGATION_ICON,		EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END),
							make_tuple(BRAND,				EOLLIB_OFFSET_CAL_HMI_BRAND_END),
							make_tuple(COUNTRY,				EOLLIB_OFFSET_CAL_HMI_COUNTRY_END),
							make_tuple(SPEECH_RECOGNITION,	EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END),
							make_tuple(HAND_FREE_TUNING,	EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END),
							make_tuple(REAR_VISION_CAMERA,	EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END)
						)
					  );

void vCreateDefaultBlock_PTest::SetUpTestCase()
{
	EOLLib_BaseTest::vEOLLib_SetUpTestCase();
	vInitEOLBlocks(m_ptsShMemEOLLIB);
}

void vCreateDefaultBlock_PTest::TearDownTestCase()
{
	EOLLib_BaseTest::vEOLLib_TearDownTestCase();
}

void vCreateDefaultBlock_PTest::SetUp()
{
	vCreateMockObjects();
	u8TableId = get<0>(GetParam());
	u16EOLBlockLength = get<1>(GetParam());
}

void vCreateDefaultBlock_PTest::TearDown()
{
	vDeleteMockObjects();
}

TEST_P(vCreateDefaultBlock_PTest, checkDataReadfromSharedLib_vCreateDefaultBlock)
{
	tU8 u8EOLData[u16EOLBlockLength];
	tU8 u8DataByte = (u8TableId * u8TableId);
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(u8TableId)]);
	ASSERT_TRUE(psBlock)
		<< "EOL Block is invalid. Value of psBlock = " << ::testing::PrintToString(psBlock);

	tVoid *retOfdlOpen = (tVoid *)0xFFFFFFBB;
	EOLLib_Entry ptEOLEntry[2];
	
	ptEOLEntry[0].offset = 0x00;
	ptEOLEntry[0].size = u16EOLBlockLength;
	ptEOLEntry[0].address = (const tVoid*)u8EOLData;

	ptEOLEntry[1].offset = 0x00;
	ptEOLEntry[1].size = 0x00;
	ptEOLEntry[1].address = NULL;


	memset(psBlock->pu8Data, 0x00, u16EOLBlockLength);
	memset(u8EOLData, u8DataByte, u16EOLBlockLength);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, OSAL_EN_READONLY))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IOControl(_, _, _))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, dlopen(_, _))
		.WillOnce(Return(retOfdlOpen));
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, dlsym(_, _))
		.WillOnce(Return(ptEOLEntry));

	vCreateDefaultBlock(psBlock, 0);
	EXPECT_FALSE(memcmp(psBlock->pu8Data, u8EOLData, u16EOLBlockLength) );
}

//--- End of vCreateDefaultBlock_PTest --

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS
//--- Start of vReadEOLBlocksDefaultValues_Test --

void vReadEOLBlocksDefaultValues_Test::SetUp()
{
	EOLLib2_Test::SetUp();
	tsDiagEOLBlock* psBlock = NULL;

	//Init Header and Block data so that it will not affect outcome of 
	//each s32ReadEOLBlocksFromFFD_Test, when test case execution is shuffled
	for (tU32 i=0;i<EOL_BLOCK_NBR;i++)
	{
		psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[i]);
		memset(psBlock->Header, 0x00, sizeof(psBlock->Header));
		psBlock->pu8Data[0] = 0x00;
	}
}

TEST_F(vReadEOLBlocksDefaultValues_Test, onPassingNullSharedMemoryPtr_NoActionShouldHappen)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(0);

	vReadEOLBlocksDefaultValues(OSAL_NULL);
}

TEST_F(vReadEOLBlocksDefaultValues_Test, onPassingNullSharedMemoryPtr_ShouldReadDefaultValue)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(10)
		.WillRepeatedly(Return(OSAL_ERROR));

	vReadEOLBlocksDefaultValues(m_ptsShMemEOLLIB);
}
//--- End of vReadEOLBlocksDefaultValues_Test --

#endif //VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS
