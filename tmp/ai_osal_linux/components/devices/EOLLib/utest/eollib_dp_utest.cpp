/****************************************************************************
* FILE                      [ $Workfile:   eollib_dp_utest.cpp  $ ]
* PROJECT (1st APPLICATION) [ GMG3 ]
* ---------------------------------------------------------------------------
* EXPLANATION
* Source file containing Unit testcases for EOLLib module using Google Unit 
* Testing Framework
* ---------------------------------------------------------------------------
* Last Review:
* ---------------------------------------------------------------------------
* COPYRIGHT (c) 2015 Robert Bosch GmbH
* AUTHOR    [ Yuvaraj Krishnaraj (yuv2cob) ]
* ---------------------------------------------------------------------------
* 
****************************************************************************/
#include "MockEOLLibDependency.h"

//Includes & defines to override actual definitions with our own definitions to facilitate unit testing!
#define NORMAL_M_ASSERT_ALWAYS NORMAL_M_ASSERT_ALWAYS_EOLLIB 
#define stat(a,b) EOLLib_stat(a,b)     //to mock stat() system function
#define dlopen(a,b) EOLLib_dlopen(a,b) //to mock dlopen() system function
#define dlsym(a,b) EOLLib_dlsym(a,b)   //to mock dlsym() system function
#define clock_gettime(a,b) EOLLib_clock_gettime(a,b) //to mock clock_gettime() system function
#define usleep(a) EOLLib_usleep(a) //to mock usleep() system function

#ifndef	_DLFCN_H
#define RTLD_NOW 1
#define	_DLFCN_H
#endif

//#include "eollib_dp.cpp"
#include "../source/eollib_dp.cpp"

#define NORMAL_M_ASSERT_ALWAYS_EOLLIB NORMAL_M_ASSERT_ALWAYS
#undef stat(a,b)
#undef dlopen(a,b)
#undef dlsym(a,b)
#undef clock_gettime(a,b)
#undef usleep(a)

#include "eollib_dp_utest.h"

/**
 * Really not sure but unfortunately, di_commonbase\components\etrace\et_Trace.h file strangely defined:
 * #define _ , 
 * This is unexpected and we need _ for setting expectations in Google Mock. So undef _ here!
 */
#undef _

using ::testing::_;
using ::testing::AtLeast;
using ::testing::Return;
using ::testing::Matcher;
using ::testing::StrNe;
using ::testing::Eq;
using ::testing::DoAll;
using ::testing::AllOf;
using ::testing::HasSubstr;
using ::testing::SetArgReferee;
using ::testing::SetArrayArgument;
using ::testing::Pointee;
using ::testing::Field;
using ::testing::InSequence;
using ::testing::SetArgPointee;

extern MockEOLLibDPDependency *pMockEOLLibDPDependency;
extern MockEOLLibCFunctionDependency *pMockEOLLibCFunctionDependency;

//Start of Test Fixture common member function definitions
tsEOLLIBSharedMemory* EOLLib_BaseTest::m_ptsShMemEOLLIB = OSAL_NULL;

void EOLLib_BaseTest::vEOLLib_SetUpTestCase()
{
	m_ptsShMemEOLLIB = OSAL_NEW tsEOLLIBSharedMemory();
	memset(m_ptsShMemEOLLIB->sDiagEOLBlocks,0x00,sizeof(m_ptsShMemEOLLIB->sDiagEOLBlocks));
}

void EOLLib_BaseTest::vEOLLib_TearDownTestCase()
{
	if(m_ptsShMemEOLLIB != OSAL_NULL)
	{
		OSAL_DELETE m_ptsShMemEOLLIB;
		m_ptsShMemEOLLIB = OSAL_NULL;
	}
}

void EOLLib_BaseTest::vInit()
{
	//It is expected to release the resource in respective test fixture TearDown() methods.
	m_pMockEOLLibDPObj = OSAL_NULL;
	m_pMockEOLLibCFunctionObj = OSAL_NULL;

	pMockEOLLibDPDependency = OSAL_NULL;
	pMockEOLLibCFunctionDependency = OSAL_NULL;
	m_pMockOsalObj = OSAL_NULL;
}

void EOLLib_BaseTest::vCreateMockObjects()
{
	vInit();
	m_pMockEOLLibDPObj = OSAL_NEW MockEOLLibDPDependency();
	m_pMockEOLLibCFunctionObj = OSAL_NEW MockEOLLibCFunctionDependency();
	m_pMockOsalObj = OSAL_NEW OsalMock();
}

void EOLLib_BaseTest::vDeleteMockObjects()
{
	pMockEOLLibDPDependency = OSAL_NULL;
	pMockEOLLibCFunctionDependency = OSAL_NULL;

	if(m_pMockEOLLibDPObj != OSAL_NULL)
	{
		OSAL_DELETE m_pMockEOLLibDPObj;
		m_pMockEOLLibDPObj = OSAL_NULL;
	}
	
	if(m_pMockEOLLibCFunctionObj != OSAL_NULL)
	{
		OSAL_DELETE m_pMockEOLLibCFunctionObj;
		m_pMockEOLLibCFunctionObj = OSAL_NULL;
	}

	if(m_pMockOsalObj != OSAL_NULL)
	{
		OSAL_DELETE m_pMockOsalObj;
		m_pMockOsalObj = OSAL_NULL;
	}
}

//Fill in header of EOL Blocks - Header with Valid Cal Form Id & Calibrated EOL block data
//(To simulate as if EOL Block read is successfull)
void EOLLib_BaseTest::initEOLBlocksWithValidHeaderAndSetBlockAsCalibrated()
{
	if(m_ptsShMemEOLLIB == OSAL_NULL)
	{
		FATAL_M_ASSERT_ALWAYS();
	}

	tsDiagEOLBlock* psBlock = NULL;
	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.Cal_Form_ID = (tU16)0x411;

	for (tU32 i=0;i<EOL_BLOCK_NBR;i++)
	{
		psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[i]);
		memcpy(psBlock->Header, &tEOLDataSetHeader, EOLLIB_HEADER_SIZE);
		psBlock->pu8Data[0] = 0x01;
	}
}

tU8* EOLLib_BaseTest::getEOLBlockHeaderInfo(tU8 u8TableId)
{
	if(m_ptsShMemEOLLIB == OSAL_NULL)
	{
		FATAL_M_ASSERT_ALWAYS();
	}

	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(u8TableId)]);
	return psBlock->Header;
}

void EOLLib_BaseTest::setExpectationP3PartitionMount_Fail()
{
	struct timespec vTimer={P3_FFD_MAX_MOUNT_TIME, 0}; //System time elapsed more than minimum expected P3 mount time
	struct stat s1;
	memset(&s1, 0x00, sizeof(s1));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Waiting for Persistent partition to mount") ))
		.Times(1);

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, clock_gettime(_, _))
		.WillOnce(DoAll(SetArgPointee<1>(vTimer), Return(0)));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, stat(_, _))
		.Times(5)
		.WillRepeatedly(DoAll( SetArgPointee<1>(s1), Return(0) ) ); //We must update param-2, otherwise leaving the default/junk value of var-s1/s2 in pCheckIfPathIsMounted() might result in unexpected test result!
}

void EOLLib_BaseTest::setExpectationP3PartitionMount_Success()
{
	struct stat s1, s2;
	s1.st_mode = S_IFDIR;
	s1.st_dev = 0x01;
	
	s2.st_mode = S_IFBLK;
	s2.st_dev = 0x02;

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, stat(_,_))
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )
		.WillOnce(DoAll( SetArgPointee<1>(s2), Return(0) ) );	
}

void EOLLib_BaseTest::setExpectationWaitP3PartitionMount_Success()
{
	struct timespec vTimer1={4, 0};
	struct timespec vTimer2={P3_FFD_MAX_MOUNT_TIME, 0}; //System time elapsed more than minimum expected P3 mount time

	struct stat s1, s2;
	s1.st_mode = S_IFDIR;
	s1.st_dev = 0x01;
	
	s2.st_mode = S_IFBLK;
	s2.st_dev = 0x02;

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, clock_gettime(_, _))
		.WillOnce(DoAll(SetArgPointee<1>(vTimer1), Return(0)) )
		.WillRepeatedly(DoAll(SetArgPointee<1>(vTimer2), Return(0)) );

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Waiting for Persistent partition to mount") ))
		.Times(1);

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, usleep(_))
		.Times(1)
		.WillOnce(Return(0));
		
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, stat(_,_))
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )//Try-1 Start - Folder Found
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )//Mount paths are same filesystem
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )//Try-1 End
		.WillOnce(DoAll( SetArgPointee<1>(s1), Return(0) ) )//Try-2 Start - Folder Found
		.WillOnce(DoAll( SetArgPointee<1>(s2), Return(0) ) );//To make successful P3 Partition mount! Filesystems different
}

void EOLLib_BaseTest::setExpectationFFDReadOfAllEOLBlocks_Success()
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(10)
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.Times(10)
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockOsalObj, s32IORead(_, _, _))
		.Times(20)
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_SYSTEM_END))
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_F_B_END))
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END))
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_NAV_SYSTEMS_END))
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_NAV_ICON_END))
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_BRAND_END))
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_COUNTRY_END))
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END))
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_HF_TUNING_END))
		.WillOnce(Return(EOLLIB_HEADER_SIZE) )
		.WillOnce(Return(EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END));
}

void EOLLib_BaseTest::setExpectationFFDOpenError_ReadDefaultIgnore_FFDSave(bool bFFDSaveSuccess)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, OSAL_EN_READONLY))
		.Times(20)
		.WillRepeatedly(Return(OSAL_ERROR));

	if(bFFDSaveSuccess) //set mock expecation such that FFD Save is successful
	{
		//For saving all blocks in FFD - from vSaveAllEOLBlocksInFFDFlash() function
		EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, OSAL_EN_READWRITE))
			.WillOnce(Return(OSAL_OK));
		EXPECT_CALL(*m_pMockOsalObj, s32IOControl(_, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW, _))
			.WillOnce(Return(OSAL_OK));
		EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
			.WillOnce(Return(OSAL_OK));
	}
	else //Set mock expecation to fail FFD Save by failing FFD Open
	{
		EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, OSAL_EN_READWRITE))
			.WillOnce(Return(OSAL_ERROR));

		EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("FFD failed to open for writing EOL blocks") ))
			.Times(1);		
	}
}

void EOLLib_BaseTest::setExpectationDPReadOfAllEOLBlocksHeaderAndData_Success()
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_))
		.Times(10)
		.WillRepeatedly(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.Times(10)
		.WillRepeatedly(
						DoAll(SetArgPointee<0>(0x01) ,Return(0) ) //To make sure DP read is not a virgin startup!
					   );
}

void EOLLib_BaseTest::setExpectationDPSaveEOLBlockHeaderAndData_Success()
{
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData( _ ))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(0));
}

void EOLLib_BaseTest::setExpectationSharedMemoryMapAndUnMap_Success()
{
	EXPECT_CALL(*m_pMockOsalObj, pvSharedMemoryMap(_, _, _, _))
		.WillOnce(Return(m_ptsShMemEOLLIB));
	EXPECT_CALL(*m_pMockOsalObj, s32SharedMemoryUnmap(_, _))
		.WillOnce(Return(OSAL_OK));
}

void EOLLib_Test::SetUpTestCase()
{
	EOLLib_BaseTest::vEOLLib_SetUpTestCase();
}

void EOLLib_Test::TearDownTestCase()
{
	EOLLib_BaseTest::vEOLLib_TearDownTestCase();
}

void EOLLib_Test::SetUp()
{
	vCreateMockObjects();
}

void EOLLib_Test::TearDown()
{
	vDeleteMockObjects();
}

void EOLLib2_Test::SetUpTestCase()
{
	EOLLib_BaseTest::vEOLLib_SetUpTestCase();
	vInitEOLBlocks(m_ptsShMemEOLLIB); //Init all EOL Cal Blocks
}

void EOLLib2_Test::TearDownTestCase()
{
	EOLLib_BaseTest::vEOLLib_TearDownTestCase();
}

void EOLLib2_Test::SetUp()
{
	vCreateMockObjects();
}

void EOLLib2_Test::TearDown()
{
	vDeleteMockObjects();
}

//End of Test Fixture common member function definitions

/**
 * Include all test cases here
 */

#include "eollib_dp_utest_Common_Functions.cpp"
#include "eollib_dp_utest_IO_Functions.cpp"
#include "eollib_dp_utest_FFD_Functions.cpp"
#include "eollib_dp_utest_DP_Functions.cpp"

//******* END ******
