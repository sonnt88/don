
//--- Start of s32ReadBlockFromFFD_Test --

TEST_F(s32ReadBlockFromFFD_Test, onPassingNullEOLBlockPtr_Return_InvalidValue)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(0);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32ReadBlockFromFFD(OSAL_NULL));
}

TEST_F(s32ReadBlockFromFFD_Test, onPassingValidBlockPtrAndFFDOpenFails_Return_IOError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_ERROR));

	EXPECT_EQ(OSAL_E_IOERROR, s32ReadBlockFromFFD(psBlock));
}

TEST_F(s32ReadBlockFromFFD_Test, onPassingValidBlockPtrAndFFDHeaderReadFails_Return_IOError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK));
	
	EXPECT_CALL(*m_pMockOsalObj, s32IORead(_, _, _))
		.WillOnce(Return(0));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_EQ(OSAL_E_IOERROR, s32ReadBlockFromFFD(psBlock));
}

TEST_F(s32ReadBlockFromFFD_Test, onPassingValidBlockPtrAndFFDDataReadFails_Return_IOError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK));
	
	EXPECT_CALL(*m_pMockOsalObj, s32IORead(_, _, _))
		.WillOnce(Return(sizeof(psBlock->Header)) )
		.WillOnce(Return(0));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_EQ(OSAL_E_IOERROR, s32ReadBlockFromFFD(psBlock));
}

TEST_F(s32ReadBlockFromFFD_Test, onPassingValidBlockPtrAndFFDAllReadSucess_Return_NoError)
{
	{
		InSequence se;
		tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

		//Mock should executed in the same sequence as below. Else test case will fail!
		EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
			.WillOnce(Return(OSAL_OK));

		EXPECT_CALL(*m_pMockOsalObj, s32IORead(OSAL_OK, _, sizeof(psBlock->Header) )) 
			.Times(1)
			.WillOnce(Return(sizeof(psBlock->Header)) );
		EXPECT_CALL(*m_pMockOsalObj, s32IORead(OSAL_OK, _, psBlock->u16Length))
			.Times(1)
			.WillOnce(Return(psBlock->u16Length));

		EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
			.WillOnce(Return(OSAL_OK));

		EXPECT_EQ(OSAL_E_NOERROR, s32ReadBlockFromFFD(psBlock));
	}
}

//--- End of s32ReadBlockFromFFD_Test --

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

//--- Start of s32ReadEOLBlocksFromFFD_Test --

void s32ReadEOLBlocksFromFFD_Test::SetUp()
{
	EOLLib2_Test::SetUp();
	tsDiagEOLBlock* psBlock = NULL;

	//Init Header and Block data so that it will not affect outcome of 
	//each s32ReadEOLBlocksFromFFD_Test, when test case execution is shuffled
	for (tU32 i=0;i<EOL_BLOCK_NBR;i++)
	{
		psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[i]);
		memset(psBlock->Header, 0x00, sizeof(psBlock->Header));
		psBlock->pu8Data[0] = 0x00;
	}
}

TEST_F(s32ReadEOLBlocksFromFFD_Test, onPassingNullSharedMemoryPtr_Return_InvalidValue)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(0);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32ReadEOLBlocksFromFFD(OSAL_NULL));
}

TEST_F(s32ReadEOLBlocksFromFFD_Test, onPassingValidSharedMemoryPtrAndFFDReadFails_Return_IOError)
{
	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromFFD_Test

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_ERROR));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(OSAL_E_IOERROR, s32ReadEOLBlocksFromFFD(m_ptsShMemEOLLIB));
}

TEST_F(s32ReadEOLBlocksFromFFD_Test, onPassingValidSharedMemoryPtrAndOneFFDBlockReadSuccessButNotCompatible_Return_InvalidValue)
{
	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromFFD_Test

	//EOL blocks (header & data) not init with valid values and hence it is already not compatible
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IORead(_, _, _))
		.WillOnce(Return(sizeof(psBlock->Header)) )
		.WillOnce(Return(psBlock->u16Length));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32ReadEOLBlocksFromFFD(m_ptsShMemEOLLIB));
}

TEST_F(s32ReadEOLBlocksFromFFD_Test, onPassingValidSharedMemoryPtrAndFFDReadOfDispInterfaceFail_Return_IOError)
{
	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromFFD_Test

	//Fill in header of SYSTEM EOL Block - Header with Valid Cal Form Id & Calibrated EOL block data
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.Cal_Form_ID = (tU16)EOLLIB_NEW_C3_CAL_FORM_ID;
	memcpy(psBlock->Header, &tEOLDataSetHeader, sizeof(psBlock->Header));
	psBlock->pu8Data[0] = 0x01;

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK))
		.WillOnce(Return(OSAL_ERROR));
	EXPECT_CALL(*m_pMockOsalObj, s32IORead(_, _, _))
		.WillOnce(Return(sizeof(psBlock->Header)) )
		.WillOnce(Return(psBlock->u16Length));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(OSAL_E_IOERROR, s32ReadEOLBlocksFromFFD(m_ptsShMemEOLLIB));
}

TEST_F(s32ReadEOLBlocksFromFFD_Test, onPassingValidSharedMemoryPtrAndFFDDataOfDispInterfaceNotCompatible_Return_InvalidValue)
{
	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromFFD_Test

	//Fill in header of SYSTEM EOL Block - Header with Valid Cal Form Id & Calibrated EOL block data
	tsDiagEOLBlock* psBlockSystem = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.Cal_Form_ID = (tU16)EOLLIB_NEW_C3_CAL_FORM_ID;
	memcpy(psBlockSystem->Header, &tEOLDataSetHeader, sizeof(psBlockSystem->Header));
	psBlockSystem->pu8Data[0] = 0x01;

	//EOL blocks (header & data) not init with valid values and hence it is already not compatible
	tsDiagEOLBlock* psBlockDispInterface = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(DISPLAY_INTERFACE)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32IORead(_, _, _))
		.WillOnce(Return(sizeof(psBlockSystem->Header)) )
		.WillOnce(Return(psBlockSystem->u16Length))
		.WillOnce(Return(sizeof(psBlockDispInterface->Header)) )
		.WillOnce(Return(psBlockDispInterface->u16Length));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32ReadEOLBlocksFromFFD(m_ptsShMemEOLLIB));
}

TEST_F(s32ReadEOLBlocksFromFFD_Test, onPassingValidSharedMemoryPtrAndAllFFDBlockReadSuccessAndCompatible_Return_NoError)
{
	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in s32ReadBlockFromFFD_Test

	initEOLBlocksWithValidHeaderAndSetBlockAsCalibrated();
	setExpectationFFDReadOfAllEOLBlocks_Success();

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(0);

	EXPECT_EQ(OSAL_E_NOERROR, s32ReadEOLBlocksFromFFD(m_ptsShMemEOLLIB));
}

//--- End of s32ReadEOLBlocksFromFFD_Test --

#else

//--- Start of vRestoreEOLBlocks_Test --

TEST_F(vRestoreEOLBlocks_Test, onPassingNullSharedMemoryPtr_NoActionRequired)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(0);

	vRestoreEOLBlocks(OSAL_NULL);
}

TEST_F(vRestoreEOLBlocks_Test, onP3PartitionMountFailure_NoActionRequired)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(0);

	setExpectationP3PartitionMount_Fail();

	vRestoreEOLBlocks(m_ptsShMemEOLLIB);
}

//From here, P3 Partition Mounted Successfully for all tests in this testcase
TEST_F(vRestoreEOLBlocks_Test, onFFDOpenError_ShouldReadFromDefault)
{
	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in vSaveAllEOLBlocksInFFDFlash_Test & vReadEOLBlocksDefaultValues_Test

	setExpectationP3PartitionMount_Success();
	setExpectationFFDOpenError_ReadDefaultIgnore_FFDSave(false); //set mock to fail opening of FFD for saving default values

	vRestoreEOLBlocks(m_ptsShMemEOLLIB);
}

TEST_F(vRestoreEOLBlocks_Test, onFFDOpenError_ShouldReadFromDefaultAndSaveAllBocksInFFD)
{
	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in vSaveAllEOLBlocksInFFDFlash_Test & vReadEOLBlocksDefaultValues_Test

	setExpectationP3PartitionMount_Success();
	setExpectationFFDOpenError_ReadDefaultIgnore_FFDSave(true); //set mock to pass FFD open to save default values

	vRestoreEOLBlocks(m_ptsShMemEOLLIB);
}

TEST_F(vRestoreEOLBlocks_Test, onFFDReadSuccess_ShouldNotReadFromDefault)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen( HasSubstr("/dev/registry/") , OSAL_EN_READONLY))
		.Times(0);

	setExpectationFFDReadOfAllEOLBlocks_Success();
	setExpectationP3PartitionMount_Success();

	vRestoreEOLBlocks(m_ptsShMemEOLLIB);
}

//--- End of vRestoreEOLBlocks_Test --

//--- Start of s32WriteBlockToFFD_Test --

TEST_F(s32WriteBlockToFFD_Test, onPassingNullEOLBlockPtr_Return_InvalidValue)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.Times(0);

	EXPECT_EQ(OSAL_E_INVALIDVALUE, s32WriteBlockToFFD(OSAL_NULL));
}

TEST_F(s32WriteBlockToFFD_Test, onPassingValidBlockPtrAndFFDOpenFails_Return_IOError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_ERROR));

	EXPECT_EQ(OSAL_E_IOERROR, s32WriteBlockToFFD(psBlock));
}

TEST_F(s32WriteBlockToFFD_Test, onPassingValidBlockPtrAndFFDHeaderWriteFails_Return_IOError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK));
	
	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(_, _, _))
		.WillOnce(Return(0));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_EQ(OSAL_E_IOERROR, s32WriteBlockToFFD(psBlock));
}

TEST_F(s32WriteBlockToFFD_Test, onPassingValidBlockPtrAndFFDDataWriteFails_Return_IOError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK));
	
	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(_, _, _))
		.WillOnce(Return(sizeof(psBlock->Header)) )
		.WillOnce(Return(0));
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_EQ(OSAL_E_IOERROR, s32WriteBlockToFFD(psBlock));
}


TEST_F(s32WriteBlockToFFD_Test, onPassingValidBlockPtrAndFFDAllWriteSucess_Return_NoError)
{
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	//Mock should executed in the same sequence as below. Else test case will fail!

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK));

	//EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(_, _, _)) 
	//	.WillOnce(Return(sizeof(psBlock->Header)) )
	//	.WillOnce(Return(psBlock->u16Length));

	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, sizeof(psBlock->Header) )) 
		.WillOnce(Return(sizeof(psBlock->Header)) );
	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, psBlock->u16Length))
		.WillOnce(Return(psBlock->u16Length));

	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_EQ(OSAL_E_NOERROR, s32WriteBlockToFFD(psBlock));
}

//--- End of s32WriteBlockToFFD_Test --

//--- Start of vSaveAllEOLBlocksInFFDFlash_Test --

TEST_F(vSaveAllEOLBlocksInFFDFlash_Test, onFFDOpenFail_WriteErrMemLog)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(OSAL_C_STRING_DEVICE_FFD, _))
		.WillOnce(Return(OSAL_ERROR));
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("FFD failed to open for writing EOL blocks") ))
		.Times(1);
	vSaveAllEOLBlocksInFFDFlash();
}

TEST_F(vSaveAllEOLBlocksInFFDFlash_Test, onFFDIOControlOperationFail_WriteErrMemLog)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(OSAL_C_STRING_DEVICE_FFD, _))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockOsalObj, s32IOControl(_, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW, _)) //IOControl Operation fails!
		.WillOnce(Return(OSAL_ERROR));

	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(OSAL_OK))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( HasSubstr("Writing EOL Blocks in FFD failed") ))
		.Times(1);
	vSaveAllEOLBlocksInFFDFlash();
}

TEST_F(vSaveAllEOLBlocksInFFDFlash_Test, onFFDIOControlOperationSuccess_NoErrMemLogWriteRequired)
{
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(OSAL_C_STRING_DEVICE_FFD, _))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockOsalObj, s32IOControl(_, OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW, _)) //IOControl Operation Success!
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(OSAL_OK))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock( _ ) )
		.Times(0);
	vSaveAllEOLBlocksInFFDFlash();
}

//--- End of vSaveAllEOLBlocksInFFDFlash_Test --

#endif //VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS
