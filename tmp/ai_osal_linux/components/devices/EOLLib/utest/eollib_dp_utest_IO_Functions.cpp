
//--- Start of DRV_DIAG_EOL_s32IODeviceInit_Test --

void DRV_DIAG_EOL_s32IODeviceInit_Test::SetUp()
{
	EOLLib_Test::SetUp();
	m_ptsShMemEOLLIB->u8ShMemInitiated = 0x00;
}

TEST_F(DRV_DIAG_EOL_s32IODeviceInit_Test, onSharedMemoryCreateFail_Return_IOError)
{
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryCreate(_, _, _))
		.WillOnce(Return(OSAL_ERROR));
	EXPECT_CALL(*m_pMockOsalObj, u32ErrorCode())
		.WillOnce(Return(OSAL_E_IOERROR));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vAssertFunction(_, _, _) ) //Usually in target system, the current process will restart right after this function is executed!
		.Times(1);

	EXPECT_EQ(OSAL_E_IOERROR, DRV_DIAG_EOL_s32IODeviceInit());
	EXPECT_EQ(0x00, m_ptsShMemEOLLIB->u8ShMemInitiated);
}

TEST_F(DRV_DIAG_EOL_s32IODeviceInit_Test, onSharedMemoryCreateAlreadyExistsAndOpenFail_Return_IOError)
{
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryCreate(_, _, _))
		.WillOnce(Return(OSAL_ERROR));
	EXPECT_CALL(*m_pMockOsalObj, u32ErrorCode())
		.WillOnce(Return(OSAL_E_ALREADYEXISTS))
		.WillOnce(Return(OSAL_E_IOERROR));

	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryOpen(_, _))
		.WillOnce(Return(OSAL_ERROR));

	EXPECT_EQ(OSAL_E_IOERROR, DRV_DIAG_EOL_s32IODeviceInit());
	EXPECT_EQ(0x00, m_ptsShMemEOLLIB->u8ShMemInitiated);
}

TEST_F(DRV_DIAG_EOL_s32IODeviceInit_Test, onSharedMemoryCreateAndOpenSuccessButMappingFails_Return_Error)
{
	tsEOLLIBSharedMemory* ptsShMemEOLLIB = OSAL_NULL; //To fail Shared Memory Mapping
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryCreate(_, _, _))
		.WillOnce(Return(OSAL_ERROR));
	EXPECT_CALL(*m_pMockOsalObj, u32ErrorCode())
		.WillOnce(Return(OSAL_E_ALREADYEXISTS));
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryOpen(_, _))
		.WillOnce(Return(OSAL_OK));


	EXPECT_CALL(*m_pMockOsalObj, pvSharedMemoryMap(_, _, _, _)) //Mapping fails!
		.WillOnce(Return(ptsShMemEOLLIB));
	EXPECT_CALL(*m_pMockOsalObj, s32SharedMemoryUnmap(_, _)) //UnMap fails here because mapping failed before!
		.WillOnce(Return(OSAL_ERROR));
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vAssertFunction(_, _, _) ) //Usually in target system, the current process will restart right after this function is executed!
		.Times(2);

	EXPECT_EQ(OSAL_ERROR, DRV_DIAG_EOL_s32IODeviceInit());
	EXPECT_EQ(0x00, m_ptsShMemEOLLIB->u8ShMemInitiated);
}

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS //since it has datapool expectation

TEST_F(DRV_DIAG_EOL_s32IODeviceInit_Test, onHavingValidSharedMemory_Return_NoError)
{
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryCreate(_, _, _))
		.WillOnce(Return(OSAL_OK));

	setExpectationSharedMemoryMapAndUnMap_Success();

	//Assuming Datapool read of all 10 EOL blocks are successfull
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, DP_vCreateDatapool() )
		.Times(1);

	setExpectationDPReadOfAllEOLBlocksHeaderAndData_Success();

	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_s32IODeviceInit());
	EXPECT_EQ(EOLLIB_INIT_MAGIC, m_ptsShMemEOLLIB->u8ShMemInitiated);
}

#else

TEST_F(DRV_DIAG_EOL_s32IODeviceInit_Test, onFFDOpenError_ShouldReadFromDefault)
{
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryCreate(_, _, _))
		.WillOnce(Return(OSAL_OK));

	setExpectationSharedMemoryMapAndUnMap_Success();

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in vSaveAllEOLBlocksInFFDFlash_Test & vReadEOLBlocksDefaultValues_Test

	setExpectationWaitP3PartitionMount_Success();
	setExpectationFFDOpenError_ReadDefaultIgnore_FFDSave(false); //set mock to fail opening of FFD for saving default value

	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_s32IODeviceInit());
	EXPECT_EQ(EOLLIB_INIT_MAGIC, m_ptsShMemEOLLIB->u8ShMemInitiated);
}

#endif //VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

//--- End of DRV_DIAG_EOL_s32IODeviceInit_Test --

//--- Start of DRV_DIAG_EOL_IOOpenClose_Test --

void DRV_DIAG_EOL_IOOpenClose_Test::SetUp()
{
	EOLLib2_Test::SetUp();
	m_ptsShMemEOLLIB->u8ShMemInitiated = 0x00;
}

TEST_F(DRV_DIAG_EOL_IOOpenClose_Test, onSharedMemoryOpenFail_Return_IOError)
{
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryOpen(_, _))
		.WillOnce(Return(OSAL_ERROR));
	EXPECT_CALL(*m_pMockOsalObj, u32ErrorCode())
		.WillOnce(Return(OSAL_E_IOERROR));

	EXPECT_EQ(OSAL_E_IOERROR, DRV_DIAG_EOL_IOOpen());
}

TEST_F(DRV_DIAG_EOL_IOOpenClose_Test, onSharedMemoryOpenSuccessAndNotInit_Return_ECanceled)
{
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryOpen(_, _))
		.WillOnce(Return(OSAL_OK));
	setExpectationSharedMemoryMapAndUnMap_Success();

	EXPECT_EQ(OSAL_E_CANCELED, DRV_DIAG_EOL_IOOpen());
}

TEST_F(DRV_DIAG_EOL_IOOpenClose_Test, onSharedMemoryMappingFailed_Should_AssertProcess)
{
	tsEOLLIBSharedMemory* ptsShMemEOLLIB = OSAL_NULL; //To fail memory mapping
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryOpen(_, _))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, pvSharedMemoryMap(_, _, _, _)) //Mapping fails
		.WillOnce(Return(ptsShMemEOLLIB));
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vAssertFunction(_, _, _) ) //Usually in target system, the current process will restart right after this function is executed!
		.Times(1);
	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_IOOpen());
}

TEST_F(DRV_DIAG_EOL_IOOpenClose_Test, onSharedMemoryOpenSuccessButNotInitAndUnMapFailed_Return_ECanceled)
{
	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryOpen(_, _))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, pvSharedMemoryMap(_, _, _, _))
		.WillOnce(Return(m_ptsShMemEOLLIB));
	EXPECT_CALL(*m_pMockOsalObj, s32SharedMemoryUnmap(_, _))
		.WillOnce(Return(OSAL_ERROR));
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vAssertFunction(_, _, _) ) //Usually in target system, the current process will restart right after this function is executed!
		.Times(1);

	EXPECT_EQ(OSAL_E_CANCELED, DRV_DIAG_EOL_IOOpen());
}

TEST_F(DRV_DIAG_EOL_IOOpenClose_Test, onSharedMemoryOpenSuccessAndInitialized_Return_NoError)
{
	m_ptsShMemEOLLIB->u8ShMemInitiated = EOLLIB_INIT_MAGIC;

	EXPECT_CALL(*m_pMockOsalObj, SharedMemoryOpen(_, _))
		.WillOnce(Return(OSAL_OK));
	setExpectationSharedMemoryMapAndUnMap_Success();

	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_IOOpen());
}

TEST_F(DRV_DIAG_EOL_IOOpenClose_Test, onSharedMemoryCloseCall_Return_NoError)
{
	EXPECT_CALL(*m_pMockOsalObj, s32SharedMemoryClose(vsEOLLibHandleSharedMemory))
		.WillOnce(Return(OSAL_OK));

	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_s32IOClose());
}

TEST_F(DRV_DIAG_EOL_IOOpenClose_Test, onSharedMemoryRemoveCall_Return_NoError)
{
	EXPECT_CALL(*m_pMockOsalObj, s32SharedMemoryClose(vsEOLLibHandleSharedMemory))
		.WillOnce(Return(OSAL_OK));
	EXPECT_CALL(*m_pMockOsalObj, s32SharedMemoryDelete(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_s32IODeviceRemove());
}

//--- End of DRV_DIAG_EOL_IOOpenClose_Test --


//--- Start of DRV_DIAG_EOL_s32IOControl_Test --

void DRV_DIAG_EOL_s32IOControl_Test::SetUp()
{
	EOLLib2_Test::SetUp();
	setExpectationSharedMemoryMapAndUnMap_Success();
}

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingInvalidFunctionId_Return_Canceled)
{
	EXPECT_EQ(OSAL_E_CANCELED, DRV_DIAG_EOL_s32IOControl(0, 0));
}

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingValidFunctionIdWithNullBlockInfo_Return_InvalidValue)
{
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IOControl(OSAL_C_S32_IOCTRL_DIAGEOL_GET_MODULE_IDENTIFIER, 0));
}

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingValidFunctionIdWithInvalidBlockInfo_Return_InvalidValue)
{
	OSAL_trDiagEOLModuleIdentifier argInput = {0, 0, {0}};
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IOControl(OSAL_C_S32_IOCTRL_DIAGEOL_GET_MODULE_IDENTIFIER, (tS32)(&argInput) ));
}

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingValidFunctionIdWithValidBlockInfo_Return_NoError)
{
	OSAL_trDiagEOLModuleIdentifier argInput = {SYSTEM, 0, {0}};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	tU32 expectedOutputPartNumber = 0xAABBCCDD;

	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.PART_NUMBER = 0xDDCCBBAA;
	tEOLDataSetHeader.DLS[0] = 0xEE;
	tEOLDataSetHeader.DLS[1] = 0xFF;

	ASSERT_TRUE(psBlock);
	memcpy(psBlock->Header, &tEOLDataSetHeader, sizeof(psBlock->Header)); //When tU32 is copied to char[4], the bytes order will reverse & hence the expectation bytes are reversed!

	//Return Value Expectation
	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_s32IOControl(OSAL_C_S32_IOCTRL_DIAGEOL_GET_MODULE_IDENTIFIER, (tS32)(&argInput) ));	

	//Parameter: out value expectation
	EXPECT_EQ(expectedOutputPartNumber, argInput.u32PartNumber);
	EXPECT_FALSE(memcmp(tEOLDataSetHeader.DLS, argInput.u8DesignLevelSuffix, sizeof(tEOLDataSetHeader.DLS)) );//memcmp() returns zero if both buffer contents are equal
}

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingValidFunctionIdWithNullEntryData_Return_InvalidValue)
{
	OSAL_trDiagEOLEntry tDiagEOLEntry = {0, //TableId
										 0, //Offset
										 0, //EntryLength
										 0	//EntryData (pointer to a buffer)
										};
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IOControl(OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC, (tS32)(&tDiagEOLEntry) ));
}

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingValidFunctionIdWithInvalidTableId_Return_InvalidValue)
{
	tU8 u8EntryData[2] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {0, 0, 0, u8EntryData};
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IOControl(OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC, (tS32)(&tDiagEOLEntry) ));
}

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingValidFunctionIdWithValidBlockEntryData1_Return_NoError)
{
	tU8 u8EntryData[5] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {SYSTEM, 0, 0, u8EntryData};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	//Prepare Header Data
	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.MODULE_ID = SYSTEM;
	tEOLDataSetHeader.Cal_Form_ID = 0x0411;
	tEOLDataSetHeader.PART_NUMBER = 0xDDCCBBAA;
	tEOLDataSetHeader.DLS[0] = 0xEE;
	tEOLDataSetHeader.DLS[1] = 0xFF;

	memcpy(psBlock->Header, &tEOLDataSetHeader, EOLLIB_HEADER_SIZE);
	memset(psBlock->pu8Data, 0xAB, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_s32IOControl(OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC, (tS32)(&tDiagEOLEntry) ));
}

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingValidFunctionIdWithValidBlockEntryData2_Return_NoError)
{
	tU8 u8EntryData[5] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {BLUETOOTH, 0, 0, u8EntryData};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BLUETOOTH)]);

	//Prepare Header Data
	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.MODULE_ID = BLUETOOTH;
	tEOLDataSetHeader.Cal_Form_ID = 0x0411;
	tEOLDataSetHeader.PART_NUMBER = 0xFFFFFFFF;
	tEOLDataSetHeader.DLS[0] = 0xFF;
	tEOLDataSetHeader.DLS[1] = 0xFF;

	memcpy(psBlock->Header, &tEOLDataSetHeader, EOLLIB_HEADER_SIZE);
	memset(psBlock->pu8Data, 0xAB, EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END);

	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_s32IOControl(OSAL_C_S32_IOCTRL_DIAGEOL_CHECK_CRC, (tS32)(&tDiagEOLEntry) ));
}

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingFunctionIdAsSyncSCCDPAndSyncFailOrNotRequired_Return_Canceled)
{
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, DP_vCreateDatapool() )
		.Times(1);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.WillOnce(Return(-1));
	
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(HasSubstr("Error occured while reading SCC datapool")) )
		.Times(1);

	EXPECT_EQ(OSAL_E_CANCELED, DRV_DIAG_EOL_s32IOControl(OSAL_C_S32_IOCTRL_DIAGEOL_SYNC_SCC_DATAPOOL, 0 ));
}

TEST_F(DRV_DIAG_EOL_s32IOControl_Test, onPassingFunctionIdAsSyncSCCDPAndSyncSuccess_Return_NoErrorAndMakeErrMemEntry)
{
	//Set SYSTEM Cal block with some value. SCC s32GetData() mock function will return zero. 
	//Hence both iMx and SCC will not be the same.
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);
	memset(psBlock->pu8Data, 0xCD, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, DP_vCreateDatapool() )
		.Times(1);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32GetData(_, _))
		.WillOnce(Return(EOL_MAX_DATA_LENGTH));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(HasSubstr("EOL calibration values are synchronized")) )
		.Times(1);

	EXPECT_EQ(OSAL_E_NOERROR, DRV_DIAG_EOL_s32IOControl(OSAL_C_S32_IOCTRL_DIAGEOL_SYNC_SCC_DATAPOOL, 0 ));
}

#endif

//--- End of DRV_DIAG_EOL_s32IOControl_Test --

//--- Start of DRV_DIAG_EOL_s32IORead_Test --

void DRV_DIAG_EOL_s32IORead_Test::SetUp()
{
	EOLLib2_Test::SetUp();
	setExpectationSharedMemoryMapAndUnMap_Success();
}

TEST_F(DRV_DIAG_EOL_s32IORead_Test, onPassingNullBuffer_Return_InvalidValue)
{
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IORead(NULL, sizeof(OSAL_trDiagEOLEntry)) );
}

TEST_F(DRV_DIAG_EOL_s32IORead_Test, onPassingEmptyBufferAndInvalidBufferSize_Return_InvalidValue)
{
	OSAL_trDiagEOLEntry tDiagEOLEntry = {0, //TableId
										 0, //Offset
										 0, //EntryLength
										 0	//EntryData (pointer to a buffer)
										};
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IORead((tPCS8)(&tDiagEOLEntry), 0) );
}

TEST_F(DRV_DIAG_EOL_s32IORead_Test, onPassingEmptyBufferAndValidBufferSize_Return_InvalidValue)
{
	OSAL_trDiagEOLEntry tDiagEOLEntry = {0, 0, 0, 0};
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IORead((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
}

TEST_F(DRV_DIAG_EOL_s32IORead_Test, onPassingValidDataBufferWithInValidEOLTableId_Return_InvalidValue)
{
	tU8 u8EntryData[5] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {0, 0, 5, u8EntryData};

	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IORead((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
}

TEST_F(DRV_DIAG_EOL_s32IORead_Test, onPassingValidEOLEntryBufferToReadHeader_Return_NoOfBytesReadEqualToSizeOfHeader)
{
	tU8 u8EntryData[EOLLIB_HEADER_SIZE] = {0};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BLUETOOTH)]);

	OSAL_trDiagEOLEntry tDiagEOLEntry = {BLUETOOTH, 0, 0, u8EntryData};//EntryLength = 0 means read Header

	//Prepare Header Data
	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.MODULE_ID = BLUETOOTH;
	tEOLDataSetHeader.Cal_Form_ID = 0x0411;
	tEOLDataSetHeader.PART_NUMBER = 0xDDCCBBAA;
	tEOLDataSetHeader.DLS[0] = 0xEE;
	tEOLDataSetHeader.DLS[1] = 0xFF;

	memcpy(psBlock->Header, &tEOLDataSetHeader, EOLLIB_HEADER_SIZE);

	EXPECT_EQ(EOLLIB_HEADER_SIZE, DRV_DIAG_EOL_s32IORead((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(psBlock->Header, u8EntryData, EOLLIB_HEADER_SIZE) );//memcmp() returns zero if both buffer contents are equal

	//Reset Header Info!
	memset(psBlock->Header, 0x00, EOLLIB_HEADER_SIZE);
}

TEST_F(DRV_DIAG_EOL_s32IORead_Test, onPassingEOLEntryBufferWithInvalidReadOffset_Return_InvalidValue)
{
	tU8 u8EntryData[EOLLIB_HEADER_SIZE] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {BLUETOOTH, EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END, 5, u8EntryData};

	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IORead((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
}

TEST_F(DRV_DIAG_EOL_s32IORead_Test, onPassingEOLEntryBufferWithValidData_Return_NoOfBytesRead)
{
	tU8 u8EntryData[5] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {BLUETOOTH, 0, 5, u8EntryData};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BLUETOOTH)]);

	memset(psBlock->pu8Data, 0xAA, 5);

	EXPECT_EQ(5, DRV_DIAG_EOL_s32IORead((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(psBlock->pu8Data, u8EntryData, 5) );//memcmp() returns zero if both buffer contents are equal
}

TEST_F(DRV_DIAG_EOL_s32IORead_Test, onPassingEOLEntryBufferWithEntryLengthGreaterThanEOLBlockSize_Return_NoOfBytesReadSameAsEOLBlockSize)
{
	tU8 u8EntryData[EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {BLUETOOTH, 0, (EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END + 1), u8EntryData};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BLUETOOTH)]);

	memset(psBlock->pu8Data, 0xBB, EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END);

	EXPECT_EQ(EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END, DRV_DIAG_EOL_s32IORead((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(psBlock->pu8Data, u8EntryData, EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END) );//memcmp() returns zero if both buffer contents are equal
	
	memset(psBlock->pu8Data, 0x00, EOLLIB_OFFSET_CAL_HMI_BLUETOOTH_END);
}
//--- End of DRV_DIAG_EOL_s32IORead_Test --

//--- Start of DRV_DIAG_EOL_s32IOWrite_Test --

void DRV_DIAG_EOL_s32IOWrite_Test::SetUp()
{
	EOLLib2_Test::SetUp();

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS
	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, DP_vCreateDatapool() )
		.Times(1);
#endif

	setExpectationSharedMemoryMapAndUnMap_Success();
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingNullBuffer_Return_InvalidValue)
{
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IOWrite(NULL, sizeof(OSAL_trDiagEOLEntry)) );
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingEmptyBufferAndInvalidBufferSize_Return_InvalidValue)
{
	OSAL_trDiagEOLEntry tDiagEOLEntry = {0, //TableId
										 0, //Offset
										 0, //EntryLength
										 0	//EntryData (pointer to a buffer)
										};
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), 0) );
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingEmptyBufferAndValidBufferSize_Return_InvalidValue)
{
	OSAL_trDiagEOLEntry tDiagEOLEntry = {0, 0, 0, 0};
	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingValidDataBufferWithInValidEOLTableId_Return_InvalidValue)
{
	tU8 u8EntryData[5] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {0, 0, 5, u8EntryData};

	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingEOLEntryBufferWithInvalidWriteOffset_Return_InvalidValue)
{
	tU8 u8EntryData[EOLLIB_HEADER_SIZE] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {BRAND, EOLLIB_OFFSET_CAL_HMI_BRAND_END, 5, u8EntryData};

	EXPECT_EQ(OSAL_E_INVALIDVALUE, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
}

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS //since it has datapool expectation

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingValidEOLEntryBufferToWriteHeaderButHeaderDPWriteFails_Return_IOError)
{
	tU8 u8EntryData[EOLLIB_HEADER_SIZE];
	memset(u8EntryData, 0x00, EOLLIB_HEADER_SIZE);
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]);

	OSAL_trDiagEOLEntry tDiagEOLEntry = {BRAND, 0, 0, u8EntryData};//EntryLength = 0 means write Header

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData( _ ))
		.WillOnce(Return(-1));
	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, _))
		.WillOnce(Return(0));

	EXPECT_EQ(OSAL_E_IOERROR, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingValidEOLEntryBufferToWriteHeader_Return_NoOfBytesReadEqualToSizeOfHeader)
{
	tU8 u8EntryData[EOLLIB_HEADER_SIZE] = {0};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]);

	OSAL_trDiagEOLEntry tDiagEOLEntry = {BRAND, 0, 0, u8EntryData};//EntryLength = 0 means write Header

	//Prepare Header Data
	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.MODULE_ID = BRAND;
	tEOLDataSetHeader.Cal_Form_ID = EOLLIB_NEW_C3_CAL_FORM_ID;
	tEOLDataSetHeader.PART_NUMBER = 0xDDCCBBAA;
	tEOLDataSetHeader.DLS[0] = 0xEE;
	tEOLDataSetHeader.DLS[1] = 0xFF;

	memcpy(u8EntryData, &tEOLDataSetHeader, EOLLIB_HEADER_SIZE);

	setExpectationDPSaveEOLBlockHeaderAndData_Success();

	EXPECT_EQ(EOLLIB_HEADER_SIZE, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(u8EntryData, psBlock->Header, EOLLIB_HEADER_SIZE) );//memcmp() returns zero if both buffer contents are equal

	//Reset Header Info!
	memset(psBlock->Header, 0x00, EOLLIB_HEADER_SIZE);
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingEOLEntryBufferWithValidData_Return_NoOfBytesWritten)
{
	tU8 u8EntryData[5] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {SPEECH_RECOGNITION, 0, 5, u8EntryData};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SPEECH_RECOGNITION)]);

	memset(psBlock->pu8Data, 0xCC, 5);

	setExpectationDPSaveEOLBlockHeaderAndData_Success();

	EXPECT_EQ(5, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(psBlock->pu8Data, u8EntryData, 5) );//memcmp() returns zero if both buffer contents are equal
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingEOLEntryBufferWithEntryLengthGreaterThanEOLBlockSize_Return_NoOfBytesWrittenSameAsEOLBlockSize)
{
	tU8 u8EntryData[EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {SPEECH_RECOGNITION, 0, (EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END + 1), u8EntryData};//Invalid EntryLength given but still should work!
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SPEECH_RECOGNITION)]);

	memset(u8EntryData, 0xDD, EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END);

	setExpectationDPSaveEOLBlockHeaderAndData_Success();

	EXPECT_EQ(EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(u8EntryData, psBlock->pu8Data, EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END) );//memcmp() returns zero if both buffer contents are equal
	
	//Reset Buffer data!
	memset(psBlock->pu8Data, 0x00, EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END);
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingEOLEntryBufferWithSYSTEMBlock_Return_NoOfBytesWrittenAndAlsoSendDataToSCC)
{
	tU8 u8EntryData[EOLLIB_OFFSET_CAL_HMI_SYSTEM_END] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {SYSTEM, 0, (EOLLIB_OFFSET_CAL_HMI_SYSTEM_END + 5), u8EntryData};//Invalid EntryLength given but still should work!
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SYSTEM)]);

	memset(u8EntryData, 0xDD, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData( _ ))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, EOL_MAX_DATA_LENGTH)) //Note: On write of the EOL Cal Block - SYSTEM, select EOL parameters from it should also be sent to SCC
		.WillOnce(Return(0));

	EXPECT_EQ(EOLLIB_OFFSET_CAL_HMI_SYSTEM_END, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(u8EntryData, psBlock->pu8Data, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END) );//memcmp() returns zero if both buffer contents are equal
	
	//Reset Buffer data!
	memset(psBlock->pu8Data, 0x00, EOLLIB_OFFSET_CAL_HMI_SYSTEM_END);
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingValidEOLEntryBufferButEOLDataWriteToDPFails_Return_IOError)
{
	tU8 u8EntryData[EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {REAR_VISION_CAMERA, 0, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END, u8EntryData};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(REAR_VISION_CAMERA)]);

	memset(u8EntryData, 0xDD, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END);

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData( _ ))
		.WillOnce(Return(0));

	EXPECT_CALL(*m_pMockEOLLibDPObj, s32SetData(_, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END)) //DP Write of RVC EOL CAL block data fails
		.WillOnce(Return(-1));

	EXPECT_EQ(OSAL_E_IOERROR, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(u8EntryData, psBlock->pu8Data, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END) );//memcmp() returns zero if both buffer contents are equal
	
	//Reset Buffer data!
	memset(psBlock->pu8Data, 0x00, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END);	
}

#else

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingValidEOLEntryBufferToWriteHeaderButHeaderFFDWriteFails_Return_IOError)
{
	tU8 u8EntryData[EOLLIB_HEADER_SIZE];
	memset(u8EntryData, 0x00, EOLLIB_HEADER_SIZE);
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]);

	OSAL_trDiagEOLEntry tDiagEOLEntry = {BRAND, 0, 0, u8EntryData};//EntryLength = 0 means write Header

	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK))
		.WillOnce(Return(OSAL_ERROR));//To fail IOOpen for saving blocks in FFD

	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(_, _, _))
		.WillOnce(Return(0)); //IOWrite fails as no. of bytes written is returned as zero!
	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(OSAL_E_IOERROR, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingValidEOLEntryBufferToWriteHeader_Return_NoOfBytesReadEqualToSizeOfHeader)
{
	tU8 u8EntryData[EOLLIB_HEADER_SIZE] = {0};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(BRAND)]);

	OSAL_trDiagEOLEntry tDiagEOLEntry = {BRAND, 0, 0, u8EntryData};//EntryLength = 0 means write Header

	//Prepare Header Data
	EOLLib_CalDsHeader tEOLDataSetHeader;
	tEOLDataSetHeader.MODULE_ID = BRAND;
	tEOLDataSetHeader.Cal_Form_ID = 0x0411;
	tEOLDataSetHeader.PART_NUMBER = 0xDDCCBBAA;
	tEOLDataSetHeader.DLS[0] = 0xEE;
	tEOLDataSetHeader.DLS[1] = 0xFF;

	memcpy(u8EntryData, &tEOLDataSetHeader, EOLLIB_HEADER_SIZE);

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in vSaveAllEOLBlocksInFFDFlash_Test
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK))
		.WillOnce(Return(OSAL_ERROR));//To fail IOOpen for saving blocks in FFD

	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, sizeof(psBlock->Header) )) 
		.WillOnce(Return(sizeof(psBlock->Header)) );
	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, psBlock->u16Length))
		.WillOnce(Return(psBlock->u16Length));

	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(EOLLIB_HEADER_SIZE, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(u8EntryData, psBlock->Header, EOLLIB_HEADER_SIZE) );//memcmp() returns zero if both buffer contents are equal

	//Reset Header Info!
	memset(psBlock->Header, 0x00, EOLLIB_HEADER_SIZE);
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingEOLEntryBufferWithValidData_Return_NoOfBytesWritten)
{
	tU8 u8EntryData[5] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {SPEECH_RECOGNITION, 0, 5, u8EntryData};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SPEECH_RECOGNITION)]);

	memset(psBlock->pu8Data, 0xCC, 5);

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in vSaveAllEOLBlocksInFFDFlash_Test
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK))
		.WillOnce(Return(OSAL_ERROR));//To fail IOOpen for saving blocks in FFD

	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, sizeof(psBlock->Header) )) 
		.WillOnce(Return(sizeof(psBlock->Header)) );
	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, psBlock->u16Length))
		.WillOnce(Return(psBlock->u16Length));

	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(5, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(psBlock->pu8Data, u8EntryData, 5) );//memcmp() returns zero if both buffer contents are equal
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingEOLEntryBufferWithEntryLengthGreaterThanEOLBlockSize_Return_NoOfBytesWrittenSameAsEOLBlockSize)
{
	tU8 u8EntryData[EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {SPEECH_RECOGNITION, 0, (EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END + 1), u8EntryData};//Invalid EntryLength given but still should work!
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(SPEECH_RECOGNITION)]);

	memset(u8EntryData, 0xDD, EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END);

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in vSaveAllEOLBlocksInFFDFlash_Test
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK))
		.WillOnce(Return(OSAL_ERROR));//To fail IOOpen for saving blocks in FFD

	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, sizeof(psBlock->Header) )) 
		.WillOnce(Return(sizeof(psBlock->Header)) );
	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, psBlock->u16Length))
		.WillOnce(Return(psBlock->u16Length));

	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(u8EntryData, psBlock->pu8Data, EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END) );//memcmp() returns zero if both buffer contents are equal
	
	//Reset Buffer data!
	memset(psBlock->pu8Data, 0x00, EOLLIB_OFFSET_CAL_HMI_SPEECH_REC_END);
}

TEST_F(DRV_DIAG_EOL_s32IOWrite_Test, onPassingValidEOLEntryBufferButEOLDataWriteToFFDFails_Return_IOError)
{
	tU8 u8EntryData[EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END] = {0};
	OSAL_trDiagEOLEntry tDiagEOLEntry = {REAR_VISION_CAMERA, 0, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END, u8EntryData};
	tsDiagEOLBlock* psBlock = &(m_ptsShMemEOLLIB->sDiagEOLBlocks[GET_EOL_BLOCK_INDEX(REAR_VISION_CAMERA)]);

	memset(u8EntryData, 0xDD, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END);

	//Here just verification of invocation of function is enough.
	//Further logics shall be tested in vSaveAllEOLBlocksInFFDFlash_Test
	EXPECT_CALL(*m_pMockOsalObj, IOOpen(_, _))
		.WillOnce(Return(OSAL_OK))
		.WillOnce(Return(OSAL_ERROR));//To fail IOOpen for saving blocks in FFD

	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, sizeof(psBlock->Header) )) 
		.WillOnce(Return(sizeof(psBlock->Header)) );
	EXPECT_CALL(*m_pMockOsalObj, s32IOWrite(OSAL_OK, _, psBlock->u16Length)) 
		.WillOnce(Return(0)); //FFD Write of RVC EOL CAL block data fails (No. of bytes written is zero)

	EXPECT_CALL(*m_pMockOsalObj, s32IOClose(_))
		.WillOnce(Return(OSAL_OK));

	EXPECT_CALL(*m_pMockEOLLibCFunctionObj, vWritePrintfErrmemMock(_))
		.Times(1);

	EXPECT_EQ(OSAL_E_IOERROR, DRV_DIAG_EOL_s32IOWrite((tPCS8)(&tDiagEOLEntry), sizeof(OSAL_trDiagEOLEntry)) );
	EXPECT_FALSE(memcmp(u8EntryData, psBlock->pu8Data, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END) );//memcmp() returns zero if both buffer contents are equal
	
	//Reset Buffer data!
	memset(psBlock->pu8Data, 0x00, EOLLIB_OFFSET_CAL_HMI_REAR_VIEW_CAMERA_END);	
}

#endif //VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

//--- End of DRV_DIAG_EOL_s32IOWrite_Test --
