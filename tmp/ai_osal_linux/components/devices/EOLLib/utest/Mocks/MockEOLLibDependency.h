#ifndef _MOCK_EOLLIB_DEPENDENCY_H_
#define _MOCK_EOLLIB_DEPENDENCY_H_

#include "gtest/gtest.h"
#include "gmock/gmock.h"

using namespace std;
using namespace tr1;

//#define SYSTEM_S_IMPORT_INTERFACE_COMPLETE
//#include "system_pif.h"
//#include "trace_interface.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define DIAG_EOL_LIB_S_IMPORT_INTERFACE_DP
#include "eollib_if.h"

//define for custom assert function defined in EOLLib
#define NORMAL_M_ASSERT_ALWAYS_EOLLIB() \
     (EOLLib_vAssertFunction("ALWAYS", __FILE__, (__LINE__)|0x80000000))

//C-functions prototype declaration
#ifdef __cplusplus
extern "C" {
#endif

tVoid vWritePrintfErrmem(tCString pCString, ...);
tVoid EOLLib_vAssertFunction(tCString exp, tCString file, tU32 line);
tS32 EOLLib_stat(tCString filePath, struct stat *st);
tVoid* EOLLib_dlopen(tCString fileName, tU16 flag);
tVoid* EOLLib_dlsym(tVoid * handle,  tCString symbol);
tS16 EOLLib_clock_gettime(clockid_t clk_id, struct timespec *tp);
tS16 EOLLib_usleep(useconds_t usec);

#ifdef __cplusplus
}  // extern "C"
#endif

tVoid DP_vCreateDatapool(tVoid);

//Mock class declarations to break EOLLib dependency on C Functions
class MockEOLLibCFunctionDependency
{
public:
	MockEOLLibCFunctionDependency();
	~MockEOLLibCFunctionDependency();

	MOCK_METHOD1(vWritePrintfErrmemMock, tVoid(tCString));
	MOCK_METHOD0(DP_vCreateDatapool, tVoid(tVoid));
	MOCK_METHOD2(stat, tS32(tCString, struct stat *));
	MOCK_METHOD3(vAssertFunction, tVoid(tCString ,tCString , tU32 ));
	MOCK_METHOD2(dlopen, tVoid*(tCString, tU16));
	MOCK_METHOD2(dlsym, tVoid*(tVoid *, tCString));
	MOCK_METHOD2(clock_gettime, tS16(clockid_t, struct timespec *));
	MOCK_METHOD1(usleep, tS16(useconds_t));
};

//Mock class declarations to break EOLLib dependency on Datapool Classes
class MockEOLLibDPDependency
{
public:
	MockEOLLibDPDependency();
	~MockEOLLibDPDependency();

	virtual MOCK_METHOD1(s32GetData, tS32(tsDiagEOLBlockHeader& tSystemHeaderParam));
	virtual MOCK_METHOD1(s32SetData, tS32(tsDiagEOLBlockHeader tSystemHeaderParam));	

	virtual MOCK_METHOD2(s32GetData, tS32(tU8* tSystemParam, tU32 u32ArraySize));
	virtual MOCK_METHOD2(s32SetData, tS32(tU8* tSystemParam, tU32 u32ArraySize));
};

tS32 invoke_s32GetData(tsDiagEOLBlockHeader& tSystemHeaderParam);
tS32 invoke_s32GetData(tU8* tSystemParam, tU32 u32ArraySize);
tS32 invoke_s32SetData(tsDiagEOLBlockHeader tSystemHeaderParam);
tS32 invoke_s32SetData(tU8* tSystemParam, tU32 u32ArraySize);

//Macros to create declaration of all EOLLib Datapool classes
#define DECLARE_DP_EOLBLOCK_HEADER_CLASS(dpClassName) \
	class dpClassName : public MockEOLLibDPDependency\
	{\
	public:\
		virtual tS32 s32GetData(tsDiagEOLBlockHeader& tSystemHeaderParam);\
		virtual tS32 s32SetData(tsDiagEOLBlockHeader tSystemHeaderParam);\
	};

#define DECLARE_DP_EOLBLOCK_DATA_CLASS(dpClassName) \
	class dpClassName : public MockEOLLibDPDependency\
	{\
	public:\
		virtual tS32 s32GetData(tU8* tSystemParam, tU32 u32ArraySize);\
		virtual tS32 s32SetData(tU8* tSystemParam, tU32 u32ArraySize);\
	};


DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule02_DataSystemHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule02_DataSystem)

DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule03_DataDisplayHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule03_DataDisplay)

DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule04_DataBluetoothHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule04_DataBluetooth)

DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule05_DataNavigationHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule05_DataNavigation)

DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule06_DataNav_IconHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule06_DataNav_Icon)

DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule07_DataBrandHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule07_DataBrand)

DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule08_DataCountryHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule08_DataCountry)

DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule09_DataSpeech_RecHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule09_DataSpeech_Rec)

DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule0a_DataHF_TuningHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule0a_DataHF_Tuning)

DECLARE_DP_EOLBLOCK_HEADER_CLASS(dp_tclCalibModule0b_DataRVCHeader)
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCalibModule0b_DataRVC)

//Mock class of CANEOL datapool to send few EOL Cal parameters to SCC
DECLARE_DP_EOLBLOCK_DATA_CLASS(dp_tclCanCalib_DataCANEOL)

#endif /* _MOCK_EOLLIB_DEPENDENCY_H_ */
