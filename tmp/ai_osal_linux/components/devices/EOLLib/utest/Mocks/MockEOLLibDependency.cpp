//gmock header already included by the header osal_if_mock.h
#include "MockEOLLibDependency.h"

MockEOLLibCFunctionDependency *pMockEOLLibCFunctionDependency = OSAL_NULL;
MockEOLLibDPDependency *pMockEOLLibDPDependency = OSAL_NULL;


#define CHECK_MOCK_EOLLIB_C_FUNC_DEPENDENCY_INIT() if(!pMockEOLLibCFunctionDependency) FATAL_M_ASSERT_ALWAYS() //ASSERT_FALSE(pMockEOLLibDependency) //Also can use:- NORMAL_M_ASSERT_ALWAYS()
#define CHECK_MOCK_EOLLIB_DP_DEPENDENCY_INIT() if(!pMockEOLLibDPDependency) FATAL_M_ASSERT_ALWAYS() //ASSERT_FALSE(pMockEOLLibDependency) //Also can use:- NORMAL_M_ASSERT_ALWAYS()

//Macros to create DP methods definitions for EOL Datapool Classes
#define CREATE_DP_GET_METHOD_ARG_ONE(dpClassName) \
	tS32 dpClassName::s32GetData(tsDiagEOLBlockHeader& tSystemHeaderParam)\
	{\
		return invoke_s32GetData(tSystemHeaderParam);\
	}

#define CREATE_DP_GET_METHOD_ARG_TWO(dpClassName) \
	tS32 dpClassName::s32GetData(tU8* tSystemParam, tU32 u32ArraySize)\
	{\
		return invoke_s32GetData(tSystemParam, u32ArraySize);\
	}

#define CREATE_DP_SET_METHOD_ARG_ONE(dpClassName) \
	tS32 dpClassName::s32SetData(tsDiagEOLBlockHeader tSystemHeaderParam)\
	{\
		return invoke_s32SetData(tSystemHeaderParam);\
	}

#define CREATE_DP_SET_METHOD_ARG_TWO(dpClassName) \
	tS32 dpClassName::s32SetData(tU8* tSystemParam, tU32 u32ArraySize)\
	{\
		return invoke_s32SetData(tSystemParam, u32ArraySize);\
	}



//Function calling mock functions to break EOLLib dependency on C Functions & also for setting expectation in Tests
void DP_vCreateDatapool(void)
{
	CHECK_MOCK_EOLLIB_C_FUNC_DEPENDENCY_INIT();
	pMockEOLLibCFunctionDependency->DP_vCreateDatapool();
}

void vWritePrintfErrmem(tCString pCString, ...)
{
	CHECK_MOCK_EOLLIB_C_FUNC_DEPENDENCY_INIT();
	pMockEOLLibCFunctionDependency->vWritePrintfErrmemMock(pCString);
}

tVoid EOLLib_vAssertFunction(tCString exp, tCString file, tU32 line)
{
	CHECK_MOCK_EOLLIB_C_FUNC_DEPENDENCY_INIT();
    pMockEOLLibCFunctionDependency->vAssertFunction( exp, file, line);
}

tS32 EOLLib_stat(tCString filePath, struct stat *st)
{
	CHECK_MOCK_EOLLIB_C_FUNC_DEPENDENCY_INIT();
    return pMockEOLLibCFunctionDependency->stat( filePath, st);
}

tVoid* EOLLib_dlopen(tCString fileName, tU16 flag)
{
	CHECK_MOCK_EOLLIB_C_FUNC_DEPENDENCY_INIT();
    return pMockEOLLibCFunctionDependency->dlopen( fileName, flag);
}

tVoid* EOLLib_dlsym(tVoid * handle,  tCString symbol)
{
	CHECK_MOCK_EOLLIB_C_FUNC_DEPENDENCY_INIT();
    return pMockEOLLibCFunctionDependency->dlsym( handle, symbol);
}

tS16 EOLLib_clock_gettime(clockid_t clk_id, struct timespec *tp)
{
	CHECK_MOCK_EOLLIB_C_FUNC_DEPENDENCY_INIT();
    return pMockEOLLibCFunctionDependency->clock_gettime( clk_id, tp);
}

tS16 EOLLib_usleep(useconds_t usec)
{
	CHECK_MOCK_EOLLIB_C_FUNC_DEPENDENCY_INIT();
    return pMockEOLLibCFunctionDependency->usleep( usec);
}

//Mock class definitions to break EOLLib dependency on C Functions
MockEOLLibCFunctionDependency::MockEOLLibCFunctionDependency() 
{
	pMockEOLLibCFunctionDependency = this;
}

MockEOLLibCFunctionDependency::~MockEOLLibCFunctionDependency() 
{
	pMockEOLLibCFunctionDependency = NULL;
}

//Mock class definitions to break EOLLib dependency on Datapool Classes
MockEOLLibDPDependency::MockEOLLibDPDependency() 
{
	if(pMockEOLLibDPDependency == OSAL_NULL)
	{
		pMockEOLLibDPDependency = this;
	}
}

MockEOLLibDPDependency::~MockEOLLibDPDependency()
{
	//pMockEOLLibDPDependency = NULL;
}

//Invoke respective DPMock function
tS32 invoke_s32GetData(tsDiagEOLBlockHeader& tSystemHeaderParam)
{
	CHECK_MOCK_EOLLIB_DP_DEPENDENCY_INIT();
	return pMockEOLLibDPDependency->MockEOLLibDPDependency::s32GetData(tSystemHeaderParam);
}

tS32 invoke_s32GetData(tU8* tSystemParam, tU32 u32ArraySize)
{
	CHECK_MOCK_EOLLIB_DP_DEPENDENCY_INIT();
	return pMockEOLLibDPDependency->MockEOLLibDPDependency::s32GetData(tSystemParam, u32ArraySize);
}

tS32 invoke_s32SetData(tsDiagEOLBlockHeader tSystemHeaderParam)
{
	CHECK_MOCK_EOLLIB_DP_DEPENDENCY_INIT();
	return pMockEOLLibDPDependency->MockEOLLibDPDependency::s32SetData(tSystemHeaderParam);
}

tS32 invoke_s32SetData(tU8* tSystemParam, tU32 u32ArraySize)
{
	CHECK_MOCK_EOLLIB_DP_DEPENDENCY_INIT();
	return pMockEOLLibDPDependency->MockEOLLibDPDependency::s32SetData(tSystemParam, u32ArraySize);
}


CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule02_DataSystemHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule02_DataSystem)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule02_DataSystemHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule02_DataSystem)

CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule03_DataDisplayHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule03_DataDisplay)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule03_DataDisplayHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule03_DataDisplay)

CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule04_DataBluetoothHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule04_DataBluetooth)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule04_DataBluetoothHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule04_DataBluetooth)

CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule05_DataNavigationHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule05_DataNavigation)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule05_DataNavigationHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule05_DataNavigation)

CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule06_DataNav_IconHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule06_DataNav_Icon)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule06_DataNav_IconHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule06_DataNav_Icon)

CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule07_DataBrandHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule07_DataBrand)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule07_DataBrandHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule07_DataBrand)

CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule08_DataCountryHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule08_DataCountry)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule08_DataCountryHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule08_DataCountry)

CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule09_DataSpeech_RecHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule09_DataSpeech_Rec)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule09_DataSpeech_RecHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule09_DataSpeech_Rec)

CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule0a_DataHF_TuningHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule0a_DataHF_Tuning)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule0a_DataHF_TuningHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule0a_DataHF_Tuning)

CREATE_DP_GET_METHOD_ARG_ONE(dp_tclCalibModule0b_DataRVCHeader)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCalibModule0b_DataRVC)
CREATE_DP_SET_METHOD_ARG_ONE(dp_tclCalibModule0b_DataRVCHeader)
CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCalibModule0b_DataRVC)

CREATE_DP_SET_METHOD_ARG_TWO(dp_tclCanCalib_DataCANEOL)
CREATE_DP_GET_METHOD_ARG_TWO(dp_tclCanCalib_DataCANEOL)

