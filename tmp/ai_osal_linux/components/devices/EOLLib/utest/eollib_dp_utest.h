/****************************************************************************
* FILE                      [ $Workfile:   eollib_dp_utest.h  $ ]
* PROJECT (1st APPLICATION) [ GMG3 ]
* ---------------------------------------------------------------------------
* EXPLANATION
* Header file containing class declarations to Unit Test EOLLib using
* Google Unit Testing Framework
* ---------------------------------------------------------------------------
* Last Review:
* ---------------------------------------------------------------------------
* COPYRIGHT (c) 2015 Robert Bosch GmbH
* AUTHOR    [ Yuvaraj Krishnaraj (yuv2cob) ]
* ---------------------------------------------------------------------------
* 
****************************************************************************/
#ifndef _EOLLIB_DP_UTEST_HEADER_
#define _EOLLIB_DP_UTEST_HEADER_

#include "MockEOLLibDependency.h"

//enum values of EOL Block Index starts from 2
#define GET_EOL_BLOCK_INDEX(x) (x-2)
#define EOLLIB_HEADER_SIZE sizeof(EOLLib_CalDsHeader)

typedef enum
{
   SYSTEM            =   2,	
   DISPLAY_INTERFACE =   3,	
   BLUETOOTH         =   4,	
   NAVIGATION_SYSTEM =   5,	
   NAVIGATION_ICON   =   6,	
   BRAND             =   7,	
   COUNTRY           =   8,	
   SPEECH_RECOGNITION=   9,	
   HAND_FREE_TUNING  =   10,
   REAR_VISION_CAMERA=   11 
} EOLLib_tenTable;			

class EOLLib_BaseTest
{
protected:
	static void vEOLLib_SetUpTestCase();
	static void vEOLLib_TearDownTestCase();

	void vInit();
	void vCreateMockObjects();
	void vDeleteMockObjects();

	static tsEOLLIBSharedMemory* m_ptsShMemEOLLIB;
	MockEOLLibDPDependency *m_pMockEOLLibDPObj;
	MockEOLLibCFunctionDependency *m_pMockEOLLibCFunctionObj;
	OsalMock *m_pMockOsalObj;

public:

	void initEOLBlocksWithValidHeaderAndSetBlockAsCalibrated();
	tU8* getEOLBlockHeaderInfo(tU8 u8TableId);

	//Common Mock expectation required for different tests are defined in this class
	void setExpectationP3PartitionMount_Fail();
	void setExpectationWaitP3PartitionMount_Success();
	void setExpectationP3PartitionMount_Success();

	void setExpectationFFDReadOfAllEOLBlocks_Success();
	void setExpectationFFDOpenError_ReadDefaultIgnore_FFDSave(bool bFFDSaveSuccess);
	void setExpectationDPReadOfAllEOLBlocksHeaderAndData_Success();
	void setExpectationDPSaveEOLBlockHeaderAndData_Success();
	void setExpectationSharedMemoryMapAndUnMap_Success();
};

class EOLLib_Test : public EOLLib_BaseTest, public ::testing::Test
{
public:
	static void SetUpTestCase();
	static void TearDownTestCase();

protected:
	virtual void SetUp();
	virtual void TearDown();
};

class EOLLib2_Test : public EOLLib_BaseTest, public ::testing::Test
{
public:
	static void SetUpTestCase(); //Also inits all EOL blocks
	static void TearDownTestCase();

protected:
	virtual void SetUp();
	virtual void TearDown();
};

//**********************-------------------***********************

#ifdef VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS
class bCheckEOLBlockAndSendEOLDataToSCC_Test : public EOLLib_Test
{
protected:
	virtual void SetUp();
};

class s32PrepareAndSendDataToSCC_Test : public EOLLib_Test
{
protected:
	virtual void SetUp();
};

class s32PrepareSCCEOLData_Test : public EOLLib_Test
{
protected:
	virtual void SetUp();
};

class s32SyncDatapoolSCC_Test : public EOLLib_Test
{
protected:
	virtual void SetUp();
};

class s32ReadBlockFromDatapool_Test : public EOLLib2_Test
{
};

class s32WriteBlockToDatapool_Test : public EOLLib2_Test
{
};

class s32WriteBlockToDatapool_PTest : public EOLLib_BaseTest, public ::testing::TestWithParam < tuple <tS8, tS8, tU8, tU32, tU16> >
{
public:
	static void SetUpTestCase();
	static void TearDownTestCase();

protected:
	virtual void SetUp();
	virtual void TearDown();

	tS8	 s8HeaderReturnValue;
	tS8	 s8DataBlockReturnValue;
	tU8  u8TableId;
	tU32 u32DatapoolReturnValue;
    tU16 u16EOLBlockLength;	
};

class s32ReadEOLBlocksFromDatapool_Test : public EOLLib2_Test
{
protected:
	virtual void SetUp();
};

class vReadEOLBlocks_Test : public EOLLib2_Test
{
protected:
	virtual void SetUp();
};

class s32ReadEOLBlocksFromFFD_Test : public EOLLib2_Test
{
protected:
	virtual void SetUp();
};

class vReadEOLBlocksDefaultValues_Test : public EOLLib2_Test
{
protected:
	virtual void SetUp();
};

#else 

class s32WriteBlockToFFD_Test : public EOLLib2_Test
{
};

class vSaveAllEOLBlocksInFFDFlash_Test : public EOLLib2_Test
{
};

class vRestoreEOLBlocks_Test : public EOLLib2_Test
{
};

#endif //VARIANT_S_FTR_ENABLE_GM_DIAGNOSIS

class s32ReadBlockFromFFD_Test : public EOLLib2_Test
{
};

class bCheckAndWaitForP3PartitionMounting_Test : public EOLLib_Test
{
};

class pCheckIfPathIsMounted_Test : public EOLLib_Test
{
};

class psGetBlock_Test : public EOLLib2_Test
{
};

class vInitEOLBlocks_PTest : public EOLLib_BaseTest, public ::testing::TestWithParam < tuple <tU8, tU8, tenFDDDataSet, tU16> >
{
public:
	static void SetUpTestCase();
	static void TearDownTestCase();

protected:
	virtual void SetUp();

	tU8	 calBlockArrayIndex;
	tU8  u8TableId;
    tU16 u16EOLBlockLength;
	tenFDDDataSet enFFDDataSet;
};

class vCreateDefaultBlock_Test : public EOLLib2_Test
{
};

class vCreateDefaultBlock_PTest : public EOLLib_BaseTest, public ::testing::TestWithParam < tuple <tU8, tU16> >
{
public:
	static void SetUpTestCase();
	static void TearDownTestCase();

protected:
	virtual void SetUp();
	virtual void TearDown();

	tU8  u8TableId;
    tU16 u16EOLBlockLength;
};

//EOLLib I/O interface functions test
class DRV_DIAG_EOL_s32IODeviceInit_Test : public EOLLib_Test //initBlocks() done as part of function itself!
{
protected:
	virtual void SetUp();
};

class DRV_DIAG_EOL_IOOpenClose_Test : public EOLLib2_Test
{
protected:
	virtual void SetUp();
};

class DRV_DIAG_EOL_s32IOControl_Test : public EOLLib2_Test
{
protected:
	virtual void SetUp();
};

class DRV_DIAG_EOL_s32IORead_Test : public EOLLib2_Test
{
protected:
	virtual void SetUp();
};

class DRV_DIAG_EOL_s32IOWrite_Test : public EOLLib2_Test
{
protected:
	virtual void SetUp();
};

#endif //_EOLLIB_DP_UTEST_HEADER_

