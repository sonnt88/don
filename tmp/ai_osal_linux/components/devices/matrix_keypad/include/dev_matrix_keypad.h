#ifndef DEV_MATRIX_KEYPAD_H
#define DEV_MATRIX_KEYPAD_H

/* ************************************************************************/
/*  defines (scope: global)                                               */
/* ************************************************************************/
#define DEV_MATRIX_KEYPAD_TCP_MSG_START     0xf00d
#define DEV_MATRIX_KEYPAD_TCP_CMD_PRESS     0x01
#define DEV_MATRIX_KEYPAD_TCP_CMD_RELEASE   0x02

/* *********************************************************************** */
/*  typedefs enum (scope: global)                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct (scope: global)                                        */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs function (scope: global)                                      */
/* *********************************************************************** */

/* *********************************************************************** */
/*  function prototypes (scope: global)                                    */
/* *********************************************************************** */
tS32    DEV_MATRIX_KEYPAD_s32IODeviceInit(void);
void    DEV_MATRIX_KEYPAD_s32IODeviceRemove(void);
tS32    DEV_MATRIX_KEYPAD_s32IOOpen(OSAL_tenAccess enAccess);
tS32    DEV_MATRIX_KEYPAD_s32IOClose(void);
tS32    DEV_MATRIX_KEYPAD_s32IOControl(tS32 s32Fun, tS32 s32Arg);

#else //DEV_MATRIX_KEYPAD_H
#error dev_matrix_keypad.h included several times
#endif //DEV_MATRIX_KEYPAD_H
