/* *********************************************************************** */
/*  includes                                                               */
/* *********************************************************************** */
#include "OsalConf.h"

#include <sys/socket.h>
#include <netinet/in.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "dev_matrix_keypad.h"

/* ************************************************************************/
/*  defines                                                               */
/* ************************************************************************/
//#define DEBUG_PRINT(...)    printf(__VA_ARGS__)
#define DEBUG_PRINT(...)

#define DEV_MATRIX_KEYPAD_RECV_THREAD_NAME  "MTX_KEY_IO_Thread"
#define DEV_MATRIX_KEYPAD_SEM_NAME          "MTX_KEY_SEM"

/* *********************************************************************** */
/*  typedefs enum                                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct                                                        */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static variables                                                       */
/* *********************************************************************** */  
static OSAL_tSemHandle          DevMatrixKeypadHandleSemaphore = OSAL_C_INVALID_HANDLE;
static int                      DevMatrixKeypadSocketFD = -1;
static int                      DevMatrixKeypadConnectionFD = -1;

/* *********************************************************************** */
/*  global variables                                                       */
/* *********************************************************************** */
extern trGlobalOsalData *pOsalData;

/* *********************************************************************** */
/*  function prototypes                                                    */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static functions                                                       */
/* *********************************************************************** */

/* *********************************************************************** */
/*  global functions                                                       */
/* *********************************************************************** */

/******************************************************************************
 *FUNCTION      :DEV_MATRIX_KEYPAD_ReceiveThread
 *
 *DESCRIPTION   :Thread to read from the socket
 *
 *PARAMETER     :Arg    unused
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
void DEV_MATRIX_KEYPAD_ReceiveThread(void *Arg)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;

    (void)Arg;

    DEBUG_PRINT("%s started\n", __func__);
    if(OSAL_s32SemaphoreOpen(DEV_MATRIX_KEYPAD_SEM_NAME, &semhandle) == OSAL_OK)
    {
        while(DevMatrixKeypadSocketFD != -1)
        {
            if(DevMatrixKeypadConnectionFD == -1)
            {
                DevMatrixKeypadConnectionFD = accept(DevMatrixKeypadSocketFD, NULL, NULL);
                if(DevMatrixKeypadConnectionFD == -1)
                {
                    OSAL_s32ThreadWait(10);
                }
                else
                {
                    DEBUG_PRINT("Connected!\n");
                }
            }
            else
            {
                static const int    buffsize = 64;
                unsigned char       buff[buffsize];
                tS32                buffbytes;
                tS32                inmsg = FALSE;

                // | 0+1    | 2   | 3     | 4+5+6+7 |
                // | 0xF00D | Len | State | Code    |

                buffbytes = 0;
                for(; ; )
                {
                    tS32    i, numbytes;
                    tS32    msglen;
                    tS32    checkmsg;

                    numbytes = recv(DevMatrixKeypadConnectionFD, buff + buffbytes, buffsize - buffbytes, 0);

                    if(numbytes <= 0)
                    {
                        // recv failed for some reason - probably the connection was closed
                        DEBUG_PRINT("%s Receive failed with %i\n", __func__, numbytes);
                        if(DevMatrixKeypadConnectionFD != -1)
                        {
                            close(DevMatrixKeypadConnectionFD);
                            DevMatrixKeypadConnectionFD = -1;
                        }
                        break;
                    }
                    DEBUG_PRINT("%s Received: %i Bytes\n", __func__, numbytes);

                    buffbytes += numbytes;
                    for(checkmsg = TRUE; checkmsg == TRUE; )
                    {
                        checkmsg = FALSE;

                        // If there was no message start tag, check if there is one now
                        for(i = 0; inmsg == FALSE && i < buffbytes - 1; i++)
                        {
                            if(*((tU16 *)(&buff[i])) == DEV_MATRIX_KEYPAD_TCP_MSG_START)
                            {
                                // Shift data in buffer - discard any garbage and drop message start tag
                                memmove(buff, buff + i + sizeof(tU16), buffsize - i - sizeof(tU16));
                                buffbytes -= i + sizeof(tU16);
                                inmsg = TRUE;
                                msglen = -1;
                                DEBUG_PRINT("%s Found tag\n", __func__);
                            }
                        }

                        if(inmsg == FALSE)
                        {
                            if(buffbytes > 1)
                            {
                                // There is some garbage in the buffer
                                // Discard garbage but make sure to keep the last byte as it could be the first byte of a message start tag
                                memmove(buff, buff + buffbytes - 1, buffbytes - 1);
                                DEBUG_PRINT("%s Discarded %i bytes of garbage\n", __func__, buffbytes);
                                buffbytes = 1;
                            }
                        }
                        else
                        {
                            // There was a message start tag
                            // Now check if there is enough data available to determine the message's length
                            if(msglen < 0 && buffbytes >= 1)
                            {
                                msglen = buff[0];
                                //if(msglen < 1 || msglen > buffsize)
                                if(msglen != 6)     // For now, there is exactly one kind of message...
                                {
                                    // The length of this message is not reasonable, discard it
                                    inmsg = FALSE;
                                    msglen = -1;
                                    DEBUG_PRINT("%s Found invalid size\n", __func__);
                                }
                            }

                            // Check if there is a complete message available
                            if(inmsg == TRUE && msglen > 0 && buffbytes >= msglen)
                            {
                                // Received at least one complete message
                                OSAL_trMatrixKeyCodeInfo    arg;
                                tS32                        argvalid = TRUE;

                                switch(buff[1])
                                {
                                case DEV_MATRIX_KEYPAD_TCP_CMD_PRESS:
                                    arg.u32KeyCode = *((tU32 *)&buff[2]);
                                    arg.enKeyState = OSAL_EN_MTX_KEY_PRESSED;
                                    pOsalData->DevMatrixKeypadKeyState |= arg.u32KeyCode;
                                    break;
                                case DEV_MATRIX_KEYPAD_TCP_CMD_RELEASE:
                                    arg.u32KeyCode = *((tU32 *)&buff[2]);
                                    arg.enKeyState = OSAL_EN_MTX_KEY_RELEASED;
                                    pOsalData->DevMatrixKeypadKeyState &= ~arg.u32KeyCode;
                                    break;
                                default:
                                    argvalid = FALSE;
                                    DEBUG_PRINT("%s Unknown command %02X\n", __func__, buff[1]);
                                    break;
                                }

                                // FIXME: Sanity check for values

                                if(argvalid == TRUE)
                                {
                                    // FIXME: Not safe! Might overwrite pOsalData->DevMatrixKeypadCBArgs with new data before callback is executed!
                                    if(OSAL_s32SemaphoreWait(semhandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                                    {
                                        if(pOsalData->DevMatrixKeypadCallback != NULL)
                                        {
                                            // Send it out, if someone is interested
                                            OSAL_trMatrixKeyCodeInfo    *osal_arg = &pOsalData->DevMatrixKeypadCBArgs[pOsalData->DevMatrixKeypadCBNextArg];

                                            memcpy(osal_arg, &arg, sizeof(arg));
                                            u32ExecuteCallback(OSAL_ProcessWhoAmI(), pOsalData->DevMatrixKeypadCallbackProcID, pOsalData->DevMatrixKeypadCallback, osal_arg, sizeof(arg));

                                            if(++pOsalData->DevMatrixKeypadCBNextArg >= OSAL_C_DEV_MATRIX_KEY_ARGS_MAX)
                                                pOsalData->DevMatrixKeypadCBNextArg = 0;
                                            DEBUG_PRINT("%s Message delivered to callback\n", __func__);
                                        }
                                        OSAL_s32SemaphorePost(semhandle);
                                    }
                                }

                                memmove(buff, buff + msglen, buffsize - msglen);
                                inmsg = FALSE;
                                buffbytes -= msglen;
                                msglen = -1;

                                checkmsg = TRUE;
                                DEBUG_PRINT("%s Message processed\n", __func__);
                            }
                        }
                    }
                }
            }
        }
        OSAL_s32SemaphoreClose(semhandle);
        OSAL_s32SemaphoreDelete(DEV_MATRIX_KEYPAD_SEM_NAME);
    }
    DEBUG_PRINT("%s ended\n", __func__);
}



/******************************************************************************
 *FUNCTION      :DEV_MATRIX_KEYPAD_s32IODeviceInit
 *
 *DESCRIPTION   :Initializes the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_MATRIX_KEYPAD_s32IODeviceInit(void)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;
    int             port = 3456;
    tS32            retval = OSAL_E_NOERROR;

    if(OSAL_s32SemaphoreCreate(DEV_MATRIX_KEYPAD_SEM_NAME, &semhandle, 1) != OSAL_OK)
    {
        retval = (tS32)OSAL_u32ErrorCode();
    }
    else
    {
        OSAL_s32SemaphoreClose(semhandle);
    }

    if(retval == OSAL_E_NOERROR)
    {
        OSAL_tIODescriptor  fd = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/LSIM/MATRIX_KEYPAD", OSAL_EN_READONLY);

        // It is not a problem and not an error if access to the registry fails. Defaults will be used.
        if(fd != OSAL_ERROR)
        {
            OSAL_trIOCtrlRegistry   entry;
            tS32                    val;

            entry.pcos8Name = "PORT";
            entry.u32Size   = sizeof(val);
            entry.s32Type   = OSAL_C_S32_VALUE_S32;
            entry.ps8Value  = (tU8 *)&val;
            if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
                port = (int)(*(tS32 *)entry.ps8Value);

            OSAL_s32IOClose (fd);
        }

        DevMatrixKeypadSocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
        if(DevMatrixKeypadSocketFD == -1)
        {
            DEBUG_PRINT("socket failed\n");
            retval = OSAL_ERROR;
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        int     flags = fcntl(DevMatrixKeypadSocketFD, F_GETFL, 0);
        fcntl(DevMatrixKeypadSocketFD, F_SETFL, flags | O_NONBLOCK);
    }

    if(retval == OSAL_E_NOERROR)
    {
        struct sockaddr_in      sockaddr;

        memset(&sockaddr, 0, sizeof(sockaddr));

        sockaddr.sin_family = AF_INET;
        sockaddr.sin_port = htons(port);
        sockaddr.sin_addr.s_addr = INADDR_ANY;

        if(bind(DevMatrixKeypadSocketFD, (struct sockaddr *)&sockaddr, sizeof(sockaddr)) == -1)
        {
            DEBUG_PRINT("bind failed\n");
            retval = OSAL_ERROR;
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        if(listen(DevMatrixKeypadSocketFD, 0) == -1)
        {
            DEBUG_PRINT("listen failed\n");
            retval = OSAL_ERROR;
        }
        else
        {
            DEBUG_PRINT("Listener opened for port %i\n", port);
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        OSAL_trThreadAttribute  thrdattr;

        thrdattr.szName         = DEV_MATRIX_KEYPAD_RECV_THREAD_NAME;
        thrdattr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        thrdattr.s32StackSize   = 4096;
        thrdattr.pfEntry        = DEV_MATRIX_KEYPAD_ReceiveThread;
        thrdattr.pvArg          = NULL;

        if(OSAL_ThreadSpawn((OSAL_trThreadAttribute* )&thrdattr) == OSAL_ERROR)
        {
            retval = OSAL_ERROR;
        }
    }

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_MATRIX_KEYPAD_s32IODeviceRemove
 *
 *DESCRIPTION   :Removes the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
void DEV_MATRIX_KEYPAD_s32IODeviceRemove(void)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;

    if(OSAL_s32SemaphoreOpen(DEV_MATRIX_KEYPAD_SEM_NAME, &semhandle) == OSAL_OK)
    {
        if(OSAL_s32SemaphoreWait(semhandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
        {
            pOsalData->DevMatrixKeypadCallbackProcID = 0;
            pOsalData->DevMatrixKeypadCallback = NULL;

            if(DevMatrixKeypadConnectionFD != -1)
            {
                shutdown(DevMatrixKeypadConnectionFD, SHUT_RDWR);
                DevMatrixKeypadConnectionFD = -1;
            }

            if(DevMatrixKeypadSocketFD != -1)
            {
                close(DevMatrixKeypadSocketFD);
                DevMatrixKeypadSocketFD = -1;
            }
            OSAL_s32SemaphorePost(semhandle);
            OSAL_s32SemaphoreClose(semhandle);
        }
    }
}



/******************************************************************************
 *FUNCTION      :DEV_MATRIX_KEYPAD_s32IOOpen
 *
 *DESCRIPTION   :Open the device
 *
 *PARAMETER     :enAccess   Desired access, one of
 *                              OSAL_EN_WRITEONLY
 *                              OSAL_EN_READWRITE
 *                              OSAL_EN_READONLY
 *
 *RETURNVALUE   :tS32   device handle on success or
 *                      OSAL_ERROR/OSAL_E_ALREADYOPENED in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_MATRIX_KEYPAD_s32IOOpen(OSAL_tenAccess enAccess)
{
    tS32    retval = OSAL_E_NOERROR;

    if((enAccess != OSAL_EN_READONLY))// && (enAccess != OSAL_EN_WRITEONLY) && (enAccess != OSAL_EN_READWRITE))
    {
        retval = OSAL_ERROR;
    }
    else if(pOsalData->DevMatrixKeypadOpenCount > 0)
    {
        retval = OSAL_E_ALREADYOPENED;
    }
    else
    {
        if(OSAL_s32SemaphoreOpen(DEV_MATRIX_KEYPAD_SEM_NAME, &DevMatrixKeypadHandleSemaphore) != OSAL_OK)
        {
            retval = OSAL_ERROR;
        }
        else
        {
            pOsalData->DevMatrixKeypadOpenCount++;
        }
    }

    // FIXME: Return a handle
    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_MATRIX_KEYPAD_s32IOClose
 *
 *DESCRIPTION   :Close the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_MATRIX_KEYPAD_s32IOClose(void)
{
    tS32    retval = OSAL_E_NOERROR;

    if(pOsalData->DevMatrixKeypadOpenCount <= 0)
    {
        retval = OSAL_ERROR;
    }
    else
    {
        if(DevMatrixKeypadHandleSemaphore != OSAL_C_INVALID_HANDLE)
        {
            pOsalData->DevMatrixKeypadOpenCount--;
            OSAL_s32SemaphoreClose(DevMatrixKeypadHandleSemaphore);
            DevMatrixKeypadHandleSemaphore = OSAL_C_INVALID_HANDLE;
        }
    }

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_MATRIX_KEYPAD_s32IOControl
 *
 *DESCRIPTION   :Control the device
 *
 *PARAMETER     :s32Fun     Function to execute
 *               s32Arg     Argument to function, meaning depends on s32Fun
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_MATRIX_KEYPAD_s32IOControl(tS32 s32Fun, tS32 s32Arg)
{
    tS32    retval = OSAL_E_NOERROR;

    if(DevMatrixKeypadHandleSemaphore == OSAL_C_INVALID_HANDLE)
    {
        retval = OSAL_E_UNKNOWN;
    }

    if(retval == OSAL_E_NOERROR)
    {
        switch(s32Fun)
        {
        case OSAL_C_S32_IOCTRL_DEV_MATRIX_KEY_REG_CALLBACK:
            if(OSAL_s32SemaphoreWait(DevMatrixKeypadHandleSemaphore, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
            {
                pOsalData->DevMatrixKeypadCallbackProcID = OSAL_ProcessWhoAmI();
                pOsalData->DevMatrixKeypadCallback = (OSAL_tpfMatrixKeypadCallbackFn)s32Arg;
                OSAL_s32SemaphorePost(DevMatrixKeypadHandleSemaphore);
            }
            break;
        case OSAL_C_S32_IOCTRL_DEV_MATRIX_KEY_GET_KEY_CODE:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                *((tU32 *)s32Arg) = pOsalData->DevMatrixKeypadKeyState;
            break;
        case OSAL_C_S32_IOCTRL_DEV_MATRIX_KEY_DEVICE_VERSION:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                *((tU32 *)s32Arg) = OSAL_MATRIX_KEYPAD_IO_CTRL_VERSION;
            break;
        default:
            retval = OSAL_E_NOTSUPPORTED;
            break;
        }
    }

    return retval;
}
