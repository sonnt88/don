/************************************************************************
| $Id: auxclock.c,v 1.0 2005 $
|************************************************************************
| FILE:         Auxclock.c
| PROJECT:      BMW L6
| SW-COMPONENT: OSAL IO device
|------------------------------------------------------------------------
| DESCRIPTION:  OSAL IO auxclock device implementation
|
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2002 Blaupunkt GmbH
| HISTORY:
| Date      | Modification               | Author
| --.--.--  | ----------------           | -------, -----
| 17.05.05  | Initial revision           | MRK2HI,----
|--.--.--   | ----------------           | -------, -----
| 28.12.12  | Modified Linux API to get  | rai5cob,----
|				| system up time				  | 
|************************************************************************
| This file is under RCS control (do not edit the following lines)
| $RCSfile: auxclock.c,v $
| $Revision:   1.0  $
| $Date:   05 Mai 2005   $
|************************************************************************/


/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include <time.h>

/* Basic OSAL includes */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define AUXCLOCK_MAIN
/* get /dev/auxclock OSALIO API interface function prototypes*/
#include "auxclock.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/**
 * @def AUXCLOCK_C_S32_IO_VERSION
 * return value for ioctrl call OSAL_C_S32_IOCTRL_VERSION
*/
#define AUXCLOCK_C_S32_IO_VERSION          ((tS32)0x00000101)   /* Read as v1.00 */

/**
 * @def AUX_SCAN_RATE_IN_NS
 * return value for ioctrl call OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETCYCLETIME
 * scan rate in ns
*/
#define  AUX_SCAN_RATE_IN_NS  (1000000)

/**
 * @def CONVERT_NS_2_HZ
 * macro for ioctrl call OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETRESOLUTION
 * to convert time in ns from @ref AUX_SCAN_RATE_IN_NS to Hz
*/
#define  CONVERT_NS_2_HZ(time_in_ns)                        \
                (tU16)((tU32)1000000000/((tU32)(time_in_ns)))

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/**
 * @var OSAL_trIOCtrlAuxClockTicks rIOAuxClockTicks
 * internal structure to hold timer values from @ref tk_get_otm()
*/



/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*!
 * \brief Convert /dev/auxclock ticks to POSIX time.
 *
 * \note Google will be happy to tell you why this function is named
 * as it is.
 */
 /* No component is using the iocontrol,this is removed*/
 #if 0
static
tS32 AUXCLOCK_s32Posixiate(
   OSAL_trIOCtrlAuxClockTicksToPosix *prConv)
{
   /* These two variables contain the synchronisation data */
   OSAL_trTimeDate rCurrentTime;
   OSAL_trIOCtrlAuxClockTicks rIOAuxClockTicksLocal;

   /* Current tick counter */
   tU64 u64CurrentTicks;
   /* Ticks to convert */
   tU64 u64Ticks;   
   /* Current POSIX time */
   tU32 u32CurrentPosix;
   /* Used for passing the OSAL system time to mktime */
   struct tm rTime;
   
   if( prConv == OSAL_NULL )
   {
      return OSAL_E_INVALIDVALUE;
   }

   if( OSAL_s32ClockGetTime( &rCurrentTime ) == OSAL_OK )
   {
      time_t t;
      
      rTime.tm_year  = (int) rCurrentTime.s32Year;
      rTime.tm_mon   = (int) rCurrentTime.s32Month-1;
      rTime.tm_mday  = (int) rCurrentTime.s32Day;
      rTime.tm_hour  = (int) rCurrentTime.s32Hour;
      rTime.tm_min   = (int) rCurrentTime.s32Minute;
      rTime.tm_sec   = (int) rCurrentTime.s32Second;
      rTime.tm_isdst = -1;      /* Information not available */

      t = mktime( &rTime );
      u32CurrentPosix = (tU32) t;
   }
   else
   {
      return OSAL_E_TEMP_NOT_AVAILABLE;
   }

   
   AUXCLOCK_s32IORead(&rIOAuxClockTicksLocal, (tU32)sizeof(OSAL_trIOCtrlAuxClockTicks));

   if( prConv->u32AuxClockTicksHigh != AUXCLOCK_C_U32_HEURISTIC_CONVERSION )
   {
      u64Ticks = (((tU64)prConv->u32AuxClockTicksHigh)<<32) |
         prConv->u32AuxClockTicksLow;
   }
   else
   {
      /* Use heuristic to determine high word of auxclock ticks */

      if( prConv->u32AuxClockTicksLow <= rIOAuxClockTicksLocal.u32Low )
      {
         u64Ticks = (((tU64)rIOAuxClockTicksLocal.u32High)<<32) |
            prConv->u32AuxClockTicksLow;
      }
      else
      {
         u64Ticks = (((tU64)rIOAuxClockTicksLocal.u32High - 1)<<32) |
            prConv->u32AuxClockTicksLow;
      }
   }

   u64CurrentTicks = (((tU64)rIOAuxClockTicksLocal.u32High)<<32) |
      rIOAuxClockTicksLocal.u32Low;

   if( u64CurrentTicks < u64Ticks )
   {
      /* In the future */

      /* Difference in seconds */
      tU64 u64Diff = (u64Ticks - u64CurrentTicks)/(CONVERT_NS_2_HZ(AUX_SCAN_RATE_IN_NS));

      if( u64Diff > OSAL_C_U32_MAX )
      {
         return OSAL_E_INVALIDVALUE;
      }
      else
      {
         prConv->u32PosixTime = u32CurrentPosix + (tU32)u64Diff;
         prConv->u16ExactTime = 0;
         return OSAL_E_NOERROR;
      }
   }
   else
   {
      /* In the past or present */

      /* Difference in seconds */
      tU64 u64Diff = (u64CurrentTicks - u64Ticks)/(CONVERT_NS_2_HZ(AUX_SCAN_RATE_IN_NS));

      if( u64Diff > OSAL_C_U32_MAX )
      {
         return OSAL_E_INVALIDVALUE;
      }
      else
      {
         prConv->u32PosixTime = u32CurrentPosix - (tU32)u64Diff;
         prConv->u16ExactTime = 0;
         return OSAL_E_NOERROR;
      }
   }
}
#endif
/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|AUXCLOCK OSAL Device Driver API
|-----------------------------------------------------------------------*/


/******************************************************************************
** FUNCTION:  AUXCLOCK_IOOpen
******************************************************************************/
/**
 *
 * @brief   Open auxclock device
 *
 *
 * @param :   none
 *
 * @retval tS32 : OSAL error code, currently always OSAL_E_NOERROR
 *
 *
*/
tS32 AUXCLOCK_IOOpen(tVoid)
{
   return(tS32)OSAL_E_NOERROR;
}


/******************************************************************************
** FUNCTION:  AUXCLOCK_s32IOClose
******************************************************************************/
/**
 *
 * @brief   Close auxclock device
 *
 *
 * @param :   none
 *
 * @retval tS32 : OSAL error code, currently always OSAL_E_NOERROR
 *
 *
*/
tS32 AUXCLOCK_s32IOClose(tVoid)
{
   return(tS32)OSAL_E_NOERROR;
}


/******************************************************************************
** FUNCTION:  AUXCLOCK_s32IOControl
******************************************************************************/
/**
 *
 * @brief   call an auxclock device driver control function
 *
 * Available options:
 *    - OSAL_C_S32_IOCTRL_VERSION
 *      (returns auxclock device driver version number)
 *    - OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETRESOLUTION
 *      (returns auxclock resolution in Hz, derived from @ref AUX_SCAN_RATE_IN_NS)
 *    - OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETCYCLETIME
 *      (returns auxclock cyle time in ns, defined by @ref AUX_SCAN_RATE_IN_NS)
 *
 *
 * @param [s32fun] :   Function identificator
 * @param [s32arg] :   Argument to be passed to function
 *
 * @retval tS32 : OSAL error code, @ref OSAL_E_NOERROR if ok
 *
 *
*/
tS32 AUXCLOCK_s32IOControl(tS32 s32fun, tS32 s32arg)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   /* s32arg is address of variable where to store requested data */
   tPS32 pArgument =  (tPS32) s32arg;

   switch(s32fun)
   {
      case OSAL_C_S32_IOCTRL_VERSION:
      {
         /* check input parameter */
         if( pArgument != OSAL_NULL )
         {
            *pArgument = AUXCLOCK_C_S32_IO_VERSION;
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETRESOLUTION:
      {
         /* check input parameter */
         if( pArgument != OSAL_NULL )
         {
            *pArgument = (tS32)(CONVERT_NS_2_HZ(AUX_SCAN_RATE_IN_NS));
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETCYCLETIME:
      {
         /* check input parameter */
         if( pArgument != OSAL_NULL )
         {
            *pArgument = (tS32)(AUX_SCAN_RATE_IN_NS);
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_CONVERTTICKSTOPOSIX:
      {
         /* No component is using the iocontrol,this is removed*/
         #if 0
         /* check input parameter */
         if( pArgument != OSAL_NULL )
         {
            /* the given argument is from type OSAL_trIOCtrlAuxClockTicksToPosix* */
            /*lint -e{826} */
            *pArgument = AUXCLOCK_s32Posixiate((OSAL_trIOCtrlAuxClockTicksToPosix*)pArgument);
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
         #endif
         
         s32RetVal = OSAL_E_NOTSUPPORTED;
      }

      default:
      {
         /* in case s32fun is neither of allowed above */
         s32RetVal = OSAL_E_UNKNOWN;
         break;
      }
   }

   return(s32RetVal);
}


/******************************************************************************
** FUNCTION:  AUXCLOCK_s32IORead
******************************************************************************/
/**
 *
 * @brief   Read auxclock time stamp, used just as wrapper for @ref clock_gettime() system call
 *             the internal struct of this driver is just updated on every read operation ( therefore
 *             no other interface makes sense )
 *
 *
 * @param [prIOAuxData] :   pointer to struct @ref OSAL_trIOCtrlAuxClockTicks
 *                                        to which the read data must be copied
 * @param [u32maxbytes] :  max number of bytes for read buffer
 *
 * @retval tS32 : number of bytes written to buffer in case of success
 *                       else OSAL error code
 *
 *
*/
tS32 AUXCLOCK_s32IORead(OSAL_trIOCtrlAuxClockTicks* prIOAuxData, tU32 u32maxbytes)
{
   /* time in Millisecond*/
   tU64 u64Time;
   struct timespec rtime;
   tS32 s32Retvalue =0;
   static OSAL_trIOCtrlAuxClockTicks rIOAuxClockTicks = {0,0} /*u32High,u32Low*/;
   
   /* check if read buffer is large enough */
   if (u32maxbytes < sizeof(OSAL_trIOCtrlAuxClockTicks))
   {
      /* if not return with Error */
      s32Retvalue= OSAL_E_INVALIDVALUE;
   }
   else if(prIOAuxData != OSAL_NULL)
   {
      /* copy data to driver internal structure */
      /* maybe needs critical section if tthis function is accessed */
      /* from various tasks and timer wraparound happens */
      if((clock_gettime(CLOCK_MONOTONIC ,&rtime ))== 0)
      {	/*Structure rtime holds value in sec and ns this should be converted to ms*/ 
         u64Time= (tU64)((((tS64)rtime.tv_sec)*(CONVERT_NS_2_HZ(AUX_SCAN_RATE_IN_NS))) + ((tS64)(rtime.tv_nsec) /AUX_SCAN_RATE_IN_NS));
         rIOAuxClockTicks.u32Low = (tU32) (u64Time);
         rIOAuxClockTicks.u32High = (tU32) (u64Time>>32);
         s32Retvalue=sizeof(OSAL_trIOCtrlAuxClockTicks);
      }

      /* copy rIOAuxClockTicks to returned read buffer */
      *prIOAuxData = rIOAuxClockTicks;
   }

#if 0
   printf("Auxclock read %ld , %ld !\n",rIOAuxClockTicks.u32High,rIOAuxClockTicks.u32Low);
   fflush(stdout);
#endif
   /* Return number of bytes copied */
   return(s32Retvalue);
}



#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file auxclock.c
|-----------------------------------------------------------------------*/
