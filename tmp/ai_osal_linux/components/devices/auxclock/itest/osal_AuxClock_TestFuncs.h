/******************************************************************************
 *FILE         : osal_Auxclock_TestFuncs.h
 *
 *SW-COMPONENT : Regression Framework based on gtest 
 *
 *DESCRIPTION  : This file has the function declarations for aux clock test cases 
 *               Ref:SW Test specification 0.1 draft
 *               
 *AUTHOR       : Rohit C R(ECG4) 
 *
 *COPYRIGHT    : 
 *
 *HISTORY      : 
 *
 *****************************************************************************/
#ifndef OEDT_AUXCLOCK_TESTFUNCS_HEADER
#define OEDT_AUXCLOCK_TESTFUNCS_HEADER

#include <persigtest.h>
#include <ResultOutput/traceprintf.h>
#include "osal_if.h"

#endif


