/*!
 * \file drv_gps.c
 *
 * Implementation of the OSAL device /dev/gps.  Part of the Paramount
 * platform software.
 *
 * Copyright (c) 2005 Blaupunkt GmbH.
 *   
 * \par History
 *
 * \date 2005-01-11 oej2hi  Created.
 * \date 2005-05-19 oej2hi  Implemented a blocking read.
 | Date      | Modification        |version    | Author
 |13.07.06   | Ported  for TSIM    | 1.1       |  Haribabu Sannapaneni (RBIN/ECM1)
 |09.05.12   | Ported for LSIM     | 1.2       |  Jaikumar Dhanapal (RBEI/ECF5)
 |************************************************************************
  
 * \par References
 *
 * SiRF Inc., OSAL GPS Device Definition, Revision 2.09
 *
 * This document describes the API of the OSAL device /dev/gps and was
 * written for the STA2052 platform.  The same API (or rather a subset
 * of it) should be implemented for the Paramount platform.  The goal
 * is that clients of the API should be completely source code
 * compatible between platforms.
 *
 * CM-DI/ESP1 Oester, SDD drv_gps and fd_gps, Version 1.0
 *
 * This is the design document for drv_gps and fd_gps.  It describes
 * the high level design and the interface between the modules.
 */

/************************************************************************
*-------------include----------------------------------------------------
*************************************************************************/
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"
#include "dev_gps.h"
#include <time.h>

/************************************************************************
*--------------------internal defines---------------------------------------
************************************************************************/

/*! \brief Used as a marker for invalid records. */
#define GPS_INVALID_RECORD_ID (0xffffffff)

/*! \brief This is the only supported combination of field
    specifiers. */
#define GPS_DEFAULT_FIELD_SPECS (OSAL_C_S32_GPS_FIELD_SPEC_TIME_UTC | \
                                 OSAL_C_S32_GPS_FIELD_SPEC_POS_LLA | \
                                 OSAL_C_S32_GPS_FIELD_SPEC_VEL_NED)
/* Define for the name and path of Sample data file*/
#define OSAL_C_STRING_LSIM_GPS_SAMPLE         "/var/opt/bosch/simdata/gps_sample.dat"

/* Read buffer size for GPS_s32GetData */
#define  GPS_READ_BUFF_MAXSIZE                 500
/* Defines for Major and Minor version Numbers */
#define  GPS_U16_MAJOR_VERSION                 12
#define  GPS_U16_MINOR_VERSION                 1
/*No.of fields in the GPS data sample file.*/
#define  GPS_FIELDS_GPS_DATA_SAMPLE_FILE       24
/* Maximum try to read the data in the GPS sample data file  */
#define  GPS_S32_MAX_TRY_SAMPLE_DATA           10
/* Define for default clock drift of the GPS device */
#define  GPS_S32_DEFAULT_CLOCK_DRIFT           0
/* Define for the size of the internal data of GPS device */
#define  GPS_S32_INTERNAL_DATA_SIZE            0

/* Defines for the default data to be used as sample data */
#define  GPS_EN_DATA_SOLUTION_TYPE               GPS_SOLTYPE_FOUR_OR_MORE_SATS
#define  GPS_BOOL_DATA_DOP_MASK_EXCEEDED         TRUE
#define  GPS_BOOL_DATA_DR_SENSOR_DATA            TRUE
#define  GPS_BOOL_DATA_FIX_IS_VALIDATED          TRUE
#define  GPS_BOOL_DATA_VELOCITY_IS_VALID         TRUE
#define  GPS_BOOL_DATA_DGPS_POSITION             TRUE
#define  GPS_EN_DATA_POSITION_FORMAT             GPS_POS_ECEF
#define  GPS_DOUBLE_DATA_POSITION_ECEF_DX        100.00
#define  GPS_DOUBLE_DATA_POSITION_ECEF_DY        200.01
#define  GPS_DOUBLE_DATA_POSITION_ECEF_DZ        300.02
#define  GPS_DOUBLE_DATA_POSITION_LLA_DALT       400.00
#define  GPS_DOUBLE_DATA_POSITION_LLA_DALT_MSL   500.01
#define  GPS_DOUBLE_DATA_POSITION_LLA_DLAT       600.02
#define  GPS_DOUBLE_DATA_POSITION_LLA_DLON       700.03
#define  GPS_EN_DATA_POSITION_LLA_ENDATUM        GPS_DATUM_WGS84
#define  GPS_EN_DATA_VELOCITY_FORMAT             GPS_VEL_NED
#define  GPS_DOUBLE_DATA_VELOCITY_ECEF_DVX       100.00
#define  GPS_DOUBLE_DATA_VELOCITY_ECEF_DVY       200.01
#define  GPS_DOUBLE_DATA_VELOCITY_ECEF_DVZ       300.02
#define  GPS_DOUBLE_DATA_VELOCITY_NED_DVD        101.00
#define  GPS_DOUBLE_DATA_VELOCITY_NED_DVE        102.01
#define  GPS_DOUBLE_DATA_VELOCITY_NED_DVN        103.02
#define  GPS_EN_DATA_TIME_FORMAT                 GPS_TIME_UTC
#define  GPS_EN_DATA_TIME_VALIDITY               GPS_TIME_VALID
/************************************************************************
*------------------------------typedef variables------------------------
*************************************************************************/

/************************************************************************
*-----------------------static variables & Functions---------------------
*************************************************************************/
/*!
 * \brief Bitmap representing which field types the client has
 * requested.
 *   
 * By default, all supported field types are sent to the client.
 */
static tU32 u32RequestedFieldTypes = 0xffffffff;

/*!
 * Bitmap representing which data representations the client
 * has requested.
 *
 * \bug Field specifiers are not really supported.  If the client
 * tries to set them, the new specifiers have to be equal to
 * GPS_DEFAULT_FIELD_SPECS.
 */
static tU32 u32RequestedFieldSpecs = GPS_DEFAULT_FIELD_SPECS;

/*! The chosen start mode.
    The default value is hot start.  If the GPS SW cannot do that, it
    should automatically chose an appropriate mode.  In other words,
    "hot start" really means "automatic start mode choice".
*/
static OSAL_tenGPSStartupMode enStartMode = GPS_STARTUP_HOT;

/*! Name of the critical section semaphore. */
static const tString coszGPSSemaphoreName = "DRVGPSSM";

/*! Semaphore, which is used to implement the critical
  section. */
static OSAL_tSemHandle hGPSSemaphore = OSAL_C_INVALID_HANDLE;

/*! Name of the event group which is used to signal new data to
  the client. */
static const tString coszGPSEventGroupName = "DRVGPSEV";

/*! Event field for data signaling. */
static OSAL_tEventHandle hGPSEvent = OSAL_C_INVALID_HANDLE;
/*! \brief Record ID.
    This is a simple running counter. */
static tU32 u32RecordId = GPS_INVALID_RECORD_ID;

/*!Validity flag for position.
   This flag is TRUE if the data is a) valid and b) has not already
   been sent to the client. */
static tBool bLatestPositionValid = FALSE;

/*! Latest position from the GPS SW. */
static OSAL_trGPSMeasuredPosition rLatestPosition;

/*! See bLatestPositionValid */
static tBool bLatestTrackDataValid = FALSE;

/*!Latest satellite track data. */
static OSAL_trGPSTrackData rLatestTrackData;

/*!See bLatestPositionValid */
static tBool bLatestVisibleListValid = FALSE;

/*! Latest visible list. */
static OSAL_trGPSVisibleList rLatestVisibleList;

/*!See bLatestPositionValid */
static tBool bLatestSatelliteHealthValid = FALSE;

/*! Latest satellite health data. */
static OSAL_trGPSSatelliteHealth rLatestSatelliteHealth;

/*! See bLatestPositionValid */
static tBool bLatestTestModeDataValid = FALSE;

/*!Latest test mode data. */
static OSAL_trGPSTestModeData rLatestTestModeData;

/*! See bLatestPositionValid */
static tBool bLatestExtDataValid = FALSE;

/*! Latest extended data. */
static OSAL_trGPSExtData rLatestExtData;

/*! See bLatestPositionValid */
static tBool bLatestNormalTestDataValid = FALSE;

/*! Latest extended data. */
static OSAL_trGPSNormalTestData rLatestNormalTestData;

/*! bLatestTightCouplingDataValid */
static tBool bLatestTightCouplingDataValid = FALSE;

/*! Latest extended data. */
static OSAL_trGPSTightCouplingData rLatestTightCouplingData;

/*!
 *  Buffer for a complete data record.
 *
 * This is necessary because the API is designed so that a record is
 * retrieved by the client in two steps.
 *
 * -# The client calls OSAL_s32IORead to retrieve the record header.
 *
 * -# The client calls OSAL_s32IOControl to retrieve the record data.
 *
 * This buffer is filled with data by Latest_s32IORead (step 1) and
 * these; data are copied to the clients buffer by GPS_s32IOControl
 * (step 2).  This method secures the device driver against
 * synchronization problems if a new data record should become
 * available from the GPS receiver between steps 1 and 2.
 */
static tU8 au8DataRecordBuffer[OSAL_C_S32_GPS_MAX_RECORD_DATA_SIZE];

/*!
 * Number of valid bytes in the data record in data_record_buffer.
 */
static tU32 u32DataRecordBufferSize = 0;

/*!
 *  Record ID of buffered data record.
 */
static tU32 u32DataRecordBufferId = GPS_INVALID_RECORD_ID;

/* Declaration of GPS full record structure */
static OSAL_trGPSFullRecord Bpgps_rGPSFullRecord;

/*! Antenna Status of the GPS device. */
static tBool bAntennaStatus = TRUE;

/* Declaration and initialization of the XOCOEFF structure */
static OSAL_trGPSXoCompensationData rGPSXoCoeff = {0,0,0,0,0,0,0};

/* Declaration and initialization for clock drift variable */
tS32    s32GPSClockDrift = GPS_S32_DEFAULT_CLOCK_DRIFT;

static tVoid devgps_vTraceOut(tU32 u32Level,const tChar *pcFormatString,... );
static tVoid GPS_vCopyData(tVoid);
static tS32 GPS_s32GetData(tVoid);/* Only for LSIM Porting */

/**********************************************************************
 *-----------------File Operation Related Declarations-----------------
 **********************************************************************/
/*Declaration for file handle*/
FILE *hGPSdata = NULL;
/*Buffer to store the data while reading from the sample file*/
tChar cGPSReadBuf[GPS_READ_BUFF_MAXSIZE];

/*********************************************************************
| function        :GPS_vEnterCriticalSection
|---------------------------------------------------------------------
| parameters      :tVoid
| return value    :None
|
|---------------------------------------------------------------------
| description     :Enter the critical section for data delivery.
|---------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
*********************************************************************/

static tVoid GPS_vEnterCriticalSection( tVoid )
{
   if( hGPSSemaphore != OSAL_C_INVALID_HANDLE )
   {
      OSAL_s32SemaphoreWait( hGPSSemaphore, OSAL_C_TIMEOUT_FOREVER );
   }
   else
   {
      /* This is an error.  Do something! */
   }
}

/*********************************************************************
| function        :GPS_vLeaveCriticalSection
|---------------------------------------------------------------------
| parameters      :tVoid
| return value    :None
|
|---------------------------------------------------------------------
| description     :Leave the critical section for data delivery.
|---------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
*********************************************************************/
static tVoid GPS_vLeaveCriticalSection( tVoid )
{
   if( hGPSSemaphore != OSAL_C_INVALID_HANDLE )
   {
      OSAL_s32SemaphorePost( hGPSSemaphore );
   }
   else
   {
      /* This is an error.  Do something! */
   }
}

/*********************************************************************************
| function        :GPS_s32IoctlSetRecordFields
|---------------------------------------------------------------------------------
| parameters      :OSAL_trGPSRecordFields
| return value    :OSAL_E_NOERROR - On success
|                  OSAL_E_INVALIDVALUE - On error
|---------------------------------------------------------------------------------
| description     :Implementation function for
|                  OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS.
|                  Allows the client to set which field types should be part of the
|                  data records and which data formats should be used for the data.
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
**********************************************************************************/

static tS32 GPS_s32IoctlSetRecordFields(OSAL_trGPSRecordFields *prSpec)
{
   tS32 s32ReturnValue = OSAL_E_NOERROR;
   /* Do some sanity checking first. */

   /* This is clearly an error. */
   if( prSpec == OSAL_NULL )
   {
      s32ReturnValue = OSAL_E_INVALIDVALUE;
   }

   /* We don't really support field specifiers. */
   if( prSpec->u32FieldSpecifiers != GPS_DEFAULT_FIELD_SPECS )
   {
      s32ReturnValue = OSAL_E_INVALIDVALUE;
   }
   /* The values are OK.  We will use them. */
   u32RequestedFieldTypes = prSpec->u32FieldTypes;
   u32RequestedFieldSpecs = prSpec->u32FieldSpecifiers;
   return s32ReturnValue;
}

/*********************************************************************************
| function        :GPS_s32IoctlGetRecordFields
|---------------------------------------------------------------------------------
| parameters      :OSAL_trGPSRecordFields
| return value    :OSAL_E_NOERROR
|
|---------------------------------------------------------------------------------
| description     :Implementation function for
|                  OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS.
|                  Lets the client retrieve the current field type and data format specifiers.
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
**********************************************************************************/

static tS32 GPS_s32IoctlGetRecordFields(OSAL_trGPSRecordFields *prSpec)
{
   prSpec->u32FieldTypes = u32RequestedFieldTypes;
   prSpec->u32FieldSpecifiers = u32RequestedFieldSpecs;
   return OSAL_E_NOERROR;
}

/*********************************************************************************
| function        :GPS_s32IoctlGetNoRecordsAvailable
|---------------------------------------------------------------------------------
| parameters      :tVoid
| return value    :0 - On Success
|                  1 - On Error
|---------------------------------------------------------------------------------
| description     :Implementation function for
|                  OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE.
|                  Returns the number of available data records.  In this
|                  implementation, the number of data records is at most one.
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN1 TSIM                 Jaikumar Dhanapal
**********************************************************************************/

static tS32 GPS_s32IoctlGetNoRecordsAvailable(tVoid)
{
   tS32 s32ReturnValue = 0;

   /* This is used to get the Dummy GPS data for LSIM porting */
   if (OSAL_E_NOERROR == GPS_s32GetData())
   {
      /* This function copies data to rLatestPosition from trMeasuredPosition */
      GPS_vCopyData();
      if( u32RecordId != GPS_INVALID_RECORD_ID )
      {
         s32ReturnValue = 1;
      }
   }
   return s32ReturnValue;
}

/*********************************************************************************
| function        :GPS_s32IoctlReadRecord
|---------------------------------------------------------------------------------
| parameters      :OSAL_trGPSReadRecordHeaderArgs
| return value    :OSAL_E_NOERROR - On Success
|                  OSAL_E_INVALIDVALUE - If the argument pointer is NULL
|                  OSAL_E_DOESNOTEXIST - If the record ID doesn't exist
|---------------------------------------------------------------------------------
| description     :Implementation function for
|                  OSAL_C_S32_IOCTL_GPS_READ_RECORD.
|                  Read a data record.  The pBuffer field points to a data buffer
|                  with enough room for the entire data record (as returned by a
|                  previous call to OSAL_s32IORead).  The u32RecordId field contains
|                  the record ID of the last data record (also taken from
|                  OSAL_s32IORead).
|                  OSAL_trGPSReadRecordHeaderArgs is pointer to a record header structure.
|                  This structure must come from a previous call to OSAL_s32IORead!
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
**********************************************************************************/

static tS32 GPS_s32IoctlReadRecord( OSAL_trGPSReadRecordHeaderArgs *ptrArgs )
{
   tS32 s32ReturnValue = OSAL_E_NOERROR;

   /* Sanity check of the arguments. */
   if( (ptrArgs == OSAL_NULL) || (ptrArgs->pBuffer == OSAL_NULL) )
   {
      s32ReturnValue = OSAL_E_INVALIDVALUE;
   }  
    
    /* Ideally, we would check that the buffer size is
       sufficient here, but the API does not include this
       information. */
    
    /* If the record requested is not the pending record, the
       request cannot be carried through. */
   else if( ptrArgs->u32RecordId != u32DataRecordBufferId)
   {
      s32ReturnValue = OSAL_E_DOESNOTEXIST;
   }  
   else
   {
      /* Copy the buffered data record to the client's buffer. */
      OSAL_pvMemoryCopy(
           ptrArgs->pBuffer,
           au8DataRecordBuffer,
           u32DataRecordBufferSize );

      /* Ensure that the record is only used once. */
      u32DataRecordBufferId = GPS_INVALID_RECORD_ID;
   }

   return s32ReturnValue;
}

/*********************************************************************************
| function        :GPS_vCopyData
|---------------------------------------------------------------------------------
| parameters      :tVoid
| return value    :tVoid
|
|---------------------------------------------------------------------------------
| description     :This function copies data to rLatestPosition from trMeasuredPosition.
|                  The data trMeasuredPosition is read from the dummy data.
|
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN1 TSIM                 Jaikumar Dhanapal
**********************************************************************************/

static tVoid GPS_vCopyData(tVoid)
{
   GPS_vEnterCriticalSection();

   bLatestPositionValid = TRUE;
   OSAL_pvMemoryCopy(
         &rLatestPosition,
         &Bpgps_rGPSFullRecord.trMeasuredPosition,
         sizeof( OSAL_trGPSMeasuredPosition ) );

   bLatestTrackDataValid = TRUE;
   OSAL_pvMemoryCopy(
         &rLatestTrackData,
         &Bpgps_rGPSFullRecord.trTrackData,
         sizeof( OSAL_trGPSTrackData ) );

   bLatestVisibleListValid = TRUE;
   OSAL_pvMemoryCopy(
         &rLatestVisibleList,
         &Bpgps_rGPSFullRecord.trVisibleList,
         sizeof( OSAL_trGPSVisibleList ) );

   bLatestSatelliteHealthValid = TRUE;
   OSAL_pvMemoryCopy(
         &rLatestSatelliteHealth,
         &Bpgps_rGPSFullRecord.trSatHealth,
         sizeof( OSAL_trGPSSatelliteHealth ) );

   bLatestExtDataValid = TRUE;
   OSAL_pvMemoryCopy(
         &rLatestExtData,
         &Bpgps_rGPSFullRecord.trExtData,
         sizeof( OSAL_trGPSExtData ) );

   bLatestNormalTestDataValid = TRUE;
   OSAL_pvMemoryCopy(
      &rLatestNormalTestData,
      &Bpgps_rGPSFullRecord.trNormalTestData,
      sizeof( OSAL_trGPSNormalTestData ) );

   bLatestTightCouplingDataValid = TRUE;
   OSAL_pvMemoryCopy(
      &rLatestTightCouplingData,
      &Bpgps_rGPSFullRecord.trTCData,
      sizeof( OSAL_trGPSTightCouplingData ) );

   ++u32RecordId;

   GPS_vLeaveCriticalSection( );
}

/*********************************************************************************
| function        :GPS_s32CreateGpsDevice
|---------------------------------------------------------------------------------
| parameters      :tVoid
| return value    :OSAL_E_NOSPACE - If there's no memory to create semaphore/event.
|                  OSAL_OK - If Creation of semaphore&event are success.
|---------------------------------------------------------------------------------
| description     :This function is exported to OSAL interface.
|                  This function creates event and semaphore for GPS device initialization.
|
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
**********************************************************************************/

tS32 GPS_s32CreateGpsDevice(tVoid)
{
   tS32 s32ReturnValue = OSAL_OK;

   /* Create the semaphore. */
   s32ReturnValue = OSAL_s32SemaphoreCreate(
                         coszGPSSemaphoreName,
                         &hGPSSemaphore,
                         1 );
            
   if( s32ReturnValue != OSAL_OK )
   {
      s32ReturnValue = OSAL_E_NOSPACE;
   }
   return s32ReturnValue;
}

/*********************************************************************************
| function        :GPS_s32DestroyGpsDevice
|---------------------------------------------------------------------------------
| parameters      :tVoid
| return value    :OSAL_OK - On success.
|---------------------------------------------------------------------------------
| description     :This function deletes event and semaphore which are created
|                  during GPS device destroy.
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
**********************************************************************************/

tS32 GPS_s32DestroyGpsDevice(tVoid)
{
      /* closing the semaphore and deleting it */
   if( hGPSSemaphore != OSAL_C_INVALID_HANDLE )
   {
      OSAL_s32SemaphoreClose( hGPSSemaphore );
      OSAL_s32SemaphoreDelete( coszGPSSemaphoreName );
      hGPSSemaphore = OSAL_C_INVALID_HANDLE;            
   }
   return OSAL_OK;
}

/*********************************************************************************
| function        :GPS_IOOpen
|---------------------------------------------------------------------------------
| parameters      :enAccess - Access mode of the device is obtained from device dispatcher
| return value    :OSAL_E_NOERROR - On Open Success
|---------------------------------------------------------------------------------
| description     :This function opens the device GPS.
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
**********************************************************************************/
tS32 GPS_IOOpen( OSAL_tenAccess enAccess )
{
   tS32 s32ReturnValue = OSAL_E_NOERROR;
   hGPSdata = fopen(OSAL_C_STRING_LSIM_GPS_SAMPLE,"r");
   devgps_vTraceOut( (tU32)TR_LEVEL_USER_3," Try to Open the data file"); /* Printing trace */
   if (hGPSdata == NULL)
   {
      devgps_vTraceOut( (tU32)TR_LEVEL_USER_3," File pointer is not valid. So, use the default data."); /* Printing trace */
   }
   else
   {
      devgps_vTraceOut( (tU32)TR_LEVEL_USER_3," File pointer is valid"); /* Printing trace */
   }
   return s32ReturnValue;
}

/*********************************************************************************
| function        :GPS_s32IOClose
|---------------------------------------------------------------------------------
| parameters      :tVoid
| return value    :OSAL_E_NOERROR - On close Success
|---------------------------------------------------------------------------------
| description     :This function closes the device GPS.
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
**********************************************************************************/

tS32 GPS_s32IOClose( tVoid )
{
   tS32 s32ReturnValue = OSAL_E_NOERROR;
   if (hGPSdata == NULL) /* if default data is used making the file pointer to NULL */
   {
      devgps_vTraceOut( (tU32)TR_LEVEL_USER_3," Sample file was not opened. So, Making the FILE pointer to NULL always."); /* Printing trace */
      hGPSdata = NULL; /* Making sure that the File pointer is always NULL when it was not opened */
   }
   else
   {
      fclose(hGPSdata);
   }
   devgps_vTraceOut( (tU32)TR_LEVEL_USER_3," Closing the Device is Success"); /* Printing trace */
   return s32ReturnValue;
}


/*********************************************************************************
| function        :GPS_s32IOControl
|---------------------------------------------------------------------------------
| parameters      :s32Fun - function ID ; s32Arg - Argument
| return value    :OSAL_E_NOTSUPPORTED - If the function ID passed is not supported.
|                  OSAL_E_INVALIDVALUE - If the argument pointer passed is NULL.
|                  OSAL_E_NOERROR - On IOControl Success.
|---------------------------------------------------------------------------------
| description     :Implements OSAL_s32IOControl for /dev/gps.
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
**********************************************************************************/

tS32 GPS_s32IOControl( tS32 fun, tS32 arg )
{
   tS32 s32ReturnValue = OSAL_E_NOERROR;
   switch( fun )
   {
      case OSAL_C_S32_IOCTRL_VERSION:
      {
         tU32 *pu32Version = (tU32*)arg;
         tU16 u16MajorVersion = (tU16)GPS_U16_MAJOR_VERSION;
         tU16 u16MinorVersion = (tU16)GPS_U16_MINOR_VERSION;
         *pu32Version = (u16MajorVersion << 16) | u16MinorVersion;
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_START_DEVICE:
      {
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_STOP_DEVICE:
      {
         enStartMode = GPS_STARTUP_HOT;
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE:
      {
         OSAL_tenGPSStartupMode enMode = (OSAL_tenGPSStartupMode)arg;
         /* Assign the corresponding start mode */
         switch (enMode)
         {
            case GPS_STARTUP_COLD:
            case GPS_STARTUP_WARM:
            case GPS_STARTUP_HOT:
            case GPS_STARTUP_FACTORY:
            {
               enStartMode = enMode;
               break;
            }
            case GPS_STARTUP_TEST:
            case GPS_STARTUP_CODE_GEN:
            default:
            {
               s32ReturnValue = OSAL_E_NOTSUPPORTED;
               break;
            }
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS:
        /* Allows the client to set which field types should be part
           of the data records and which data formats should be used
           for the data.  arg is interpreted as a pointer to an
           OSAL_trGPSRecordFields structure. */
      {
         OSAL_trGPSRecordFields *prSpec = (OSAL_trGPSRecordFields*)arg;
         if( prSpec == OSAL_NULL)
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         else
         {
            s32ReturnValue = GPS_s32IoctlSetRecordFields(prSpec);
         }
         break;
      }
          
      case OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS:

        /* Lets the client retrieve the current field type and data
           format specifiers.  arg is interpreted as a pointer to an
           OSAL_trGPSRecordFields structure. */
      {
         OSAL_trGPSRecordFields *prSpec = (OSAL_trGPSRecordFields*)arg;
         if( prSpec == OSAL_NULL)
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         else
         {
            s32ReturnValue = GPS_s32IoctlGetRecordFields(prSpec);
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE:

        /* Returns the number of available data records.  In this
           implementation, the number of data records is at most
           one. */
      {
         tU32 *pu32NoRecords = (tU32*)arg;
         if( pu32NoRecords == OSAL_NULL )
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         else
         {
            *pu32NoRecords = GPS_s32IoctlGetNoRecordsAvailable();
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_READ_RECORD:
        /* Read a data record.  arg is a pointer to an
           OSAL_trGPSReadRecordHeaderArgs structure.  The pBuffer
           field points to a data buffer with enough room for the
           entire data record (as returned by a previous call to
           OSAL_s32IORead).  The u32RecordId field contains the record
           ID of the last data record (also taken from
           OSAL_s32IORead). */
      {
         OSAL_trGPSReadRecordHeaderArgs  *ptrArgs =
               (OSAL_trGPSReadRecordHeaderArgs*) arg;
         if(ptrArgs == OSAL_NULL)
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         else
         {
            s32ReturnValue = GPS_s32IoctlReadRecord(ptrArgs);
         }
         break;
      }
        
      case OSAL_C_S32_IOCTL_GPS_SET_ANTENNA:
        /* Turn the GPS antenna current on or off.  arg is a boolean
           flag: true to turn the antenna current on. */
      {
         bAntennaStatus = (tBool) arg;
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_GET_ANTENNA:
      {
         tBool *pbAntennaArg = (tBool *) arg;
         if(pbAntennaArg == OSAL_NULL)
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         else
         {
            *pbAntennaArg = bAntennaStatus;
         }

         break;
      }

      case OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE:
      {
         tU32 *pu32InternalDataSize = (tU32*) arg;
         if( pu32InternalDataSize == OSAL_NULL)
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         else
         {
            *pu32InternalDataSize = GPS_S32_INTERNAL_DATA_SIZE;
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA:
      {
         OSAL_trGPSInternalData *prDescr = (OSAL_trGPSInternalData*) arg;
         if( prDescr != OSAL_NULL )
         {
            s32ReturnValue = OSAL_E_NOERROR;
         }
         else
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA:
      {
         OSAL_trGPSInternalData *prDescr = (OSAL_trGPSInternalData*) arg;
         if( prDescr != OSAL_NULL )
         {
            s32ReturnValue = OSAL_E_NOERROR;
         }
         else
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         break;
      } 

      case OSAL_C_S32_IOCTL_GPS_SET_TEMPERATURE:
      {
         tF32 *pf32Temp = (tF32*) arg;
         if( pf32Temp != OSAL_NULL )
         {
            if( *pf32Temp < 0.0 || *pf32Temp > 500.0 )
            {
               s32ReturnValue = OSAL_E_INVALIDVALUE;
            }
            else
            {
               s32ReturnValue = OSAL_E_NOERROR;
            }
         }
         else
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT:
      {
         tS32 *ps32ClockDrift = (tS32 *)arg;
         *ps32ClockDrift = s32GPSClockDrift;
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_DEFAULT_CLOCK_DRIFT:
      {
         s32GPSClockDrift = (tS32)arg;
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_KDS_XO_COEFF:
      {
         OSAL_trGPSXoCompensationData  *prSetXoCoeff = (OSAL_trGPSXoCompensationData *)arg;
         if (prSetXoCoeff != OSAL_NULL)
         {
            GPS_vEnterCriticalSection();
            rGPSXoCoeff = *prSetXoCoeff;
            GPS_vLeaveCriticalSection( );
         }
         else
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF:
      {
         OSAL_trGPSXoCompensationData  *prGetXoCoeff = (OSAL_trGPSXoCompensationData *)arg;
         if (prGetXoCoeff != OSAL_NULL)
         {
            GPS_vEnterCriticalSection();
            *prGetXoCoeff = rGPSXoCoeff;
            GPS_vLeaveCriticalSection( );
         }
         else
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF:
      {
         OSAL_trGPSXoCompensationData  *prSetXoCoeff = (OSAL_trGPSXoCompensationData *)arg;
         if (prSetXoCoeff != OSAL_NULL)
         {
            GPS_vEnterCriticalSection();
            rGPSXoCoeff = *prSetXoCoeff;
            GPS_vLeaveCriticalSection( );
         }
         else
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME:
      {
         OSAL_trGPSTimeUTC *prOsalDescr = (OSAL_trGPSTimeUTC*) arg;
         if( prOsalDescr != OSAL_NULL )
         {
            s32ReturnValue = OSAL_E_NOERROR;
         }
         else
         {
            s32ReturnValue = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_HINTS:
      {
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_FRONT_END_TEST:
      {
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_SETUP_EXTTESTMODE:
      {
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_GET_EXTTESTMODE_RESULTS:
      {
         break;
      }

      case OSAL_C_S32_IOCTL_GPS_SET_STARTUP_POSITION:
      {
         break;
      }

	  case OSAL_C_S32_IOCTL_GPS_GET_ANTENNA_STATUS:
      {
          if((tPS32)arg == OSAL_NULL)
          {
               s32ReturnValue = OSAL_E_INVALIDVALUE;
          }
          else
          {
            OSAL_trGPSAntennastatus *pAntennStatus = (OSAL_trGPSAntennastatus*)arg;
			/*if(s32CalcAntennaStatus(pAntennStatus)!=DEV_GPS_OK)
			{
				s32ReturnValue = OSAL_E_UNKNOWN;			
			}*/
			/* Not implemented, so tell that we don't know */
			pAntennStatus->enGPSAntennaStatus = enGpsAntennaUnknown;

          }
          break;
          
      }
      case OSAL_C_S32_IOCTL_GPS_GET_GPS_TEMPERATURE:
      {
          if((tPS32)arg == OSAL_NULL)
          {
               s32ReturnValue = OSAL_E_INVALIDVALUE;
          }
          else
          {
            tF32 *pfGPSTemperature = (tF32*) arg;;
			/*
            if(s32GetGPSTemperature(pfGPSTemperature) != DEV_GPS_OK)
			{
				s32ReturnValue = OSAL_E_UNKNOWN;
			}
			*/
			/*not implemented, so give a fake value */
			*pfGPSTemperature=298.15;
          }
          break;
      }       
      case OSAL_C_S32_IOCTL_GPS_SET_TRACE_COMMAND: /* This interface is not supported in LSIM */
      case OSAL_C_S32_IOCTL_GPS_REMOVE_RECORDS:
      case OSAL_C_S32_IOCTL_GPS_SET_DATA_SOURCE:
      case OSAL_C_S32_IOCTL_GPS_FORCE_DATA:
      case OSAL_C_S32_IOCTL_GPS_SET_CURRENT_CLOCK_DRIFT:
      case OSAL_C_S32_IOCTL_GPS_SET_CVM_STATE:
      default:
        /* All other commands are either not necessary or not
           implemented yet. */
      {
         s32ReturnValue = OSAL_E_NOTSUPPORTED;
         break;
      }
   }
   return s32ReturnValue;
}


/*********************************************************************************
| function        :GPS_s32GetData
|---------------------------------------------------------------------------------
| parameters      :tVoid
| return value    :OSAL_E_INVALIDVALUE - If the data in the file is invalid.
|                  OSAL_E_NOERROR - On read Success.
|---------------------------------------------------------------------------------
| description     :This function initializes and fills the data to the GPS Data structure
|                  Either from a file or default values.
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN1 TSIM                 Jaikumar Dhanapal
**********************************************************************************/

static tS32 GPS_s32GetData(tVoid)
{
   struct tm rSystemTime;
   struct timeval rTimeOfDay;
   OSAL_trGPSMeasuredPosition *prGPSPos;

   tS32 s32ReturnValue = OSAL_E_NOERROR;
   /*File operations implementation */
   tS32 s32NoFields                 =0;
   tS32 s32Try_Count                =0;
   tS32 s32Solution_Type            = 0;
   tS32 s32DOPMask_Exceeded         = 0;
   tS32 s32DR_Sensor_Data           = 0;
   tS32 s32Fix_Is_Validated         = 0;
   tS32 s32Velocity_Is_Valid        = 0;
   tS32 s32DGPS_Position            = 0;
   tS32 s32Position_Format          = 0;
   tS32 s32Position_LLA_enDATUM     = 0;
   tS32 s32Velocity_Format          = 0;
   tS32 s32Time_Format              = 0;
   tS32 s32Time_Validity            = 0;
   tF32 f32Position_ECEF_dX         = 0.000000;
   tF32 f32Position_ECEF_dY         = 0.000000;
   tF32 f32Position_ECEF_dZ         = 0.000000;
   tF32 f32Position_LLA_dAlt        = 0.000000;
   tF32 f32Position_LLA_dAlt_MSL    = 0.000000;
   tF32 f32Position_LLA_dLat        = 0.000000;
   tF32 f32Position_LLA_dLon        = 0.000000;
   tF32 f32Velocity_ECEF_dVx        = 0.000000;
   tF32 f32Velocity_ECEF_dVy        = 0.000000;
   tF32 f32Velocity_ECEF_dVz        = 0.000000;
   tF32 f32Velocity_NED_dVd         = 0.000000;
   tF32 f32Velocity_NED_dVe         = 0.000000;
   tF32 f32Velocity_NED_dVn         = 0.000000;

   /**********************************************
    * Linux API to get the system time
    **********************************************/
   gettimeofday(&rTimeOfDay ,NULL) ;
   localtime_r (&rTimeOfDay.tv_sec, &rSystemTime);

   devgps_vTraceOut( (tU32)TR_LEVEL_USER_4," Entered get data()");

   if (hGPSdata != NULL)
   {
      devgps_vTraceOut( (tU32)TR_LEVEL_USER_4," Read data from file ");
      while((s32NoFields != GPS_FIELDS_GPS_DATA_SAMPLE_FILE) && (s32Try_Count < GPS_S32_MAX_TRY_SAMPLE_DATA))
      {
         if (fgets(cGPSReadBuf, GPS_READ_BUFF_MAXSIZE, hGPSdata)!= NULL)
         {
            s32NoFields = OSAL_s32ScanFormat(cGPSReadBuf, "Sample=%d %d %d %d %d %d %d %f %f %f %f %f %f %f %d %d %f %f %f %f %f %f %d %d",
                                             &s32Solution_Type,
                                             &s32DOPMask_Exceeded,
                                             &s32DR_Sensor_Data,
                                             &s32Fix_Is_Validated,
                                             &s32Velocity_Is_Valid,
                                             &s32DGPS_Position,
                                             &s32Position_Format,
                                             &f32Position_ECEF_dX,
                                             &f32Position_ECEF_dY,
                                             &f32Position_ECEF_dZ,
                                             &f32Position_LLA_dAlt,
                                             &f32Position_LLA_dAlt_MSL,
                                             &f32Position_LLA_dLat,
                                             &f32Position_LLA_dLon,
                                             &s32Position_LLA_enDATUM,
                                             &s32Velocity_Format,
                                             &f32Velocity_ECEF_dVx,
                                             &f32Velocity_ECEF_dVy,
                                             &f32Velocity_ECEF_dVz,
                                             &f32Velocity_NED_dVd,
                                             &f32Velocity_NED_dVe,
                                             &f32Velocity_NED_dVn,
                                             &s32Time_Format,
                                             &s32Time_Validity);
           s32Try_Count++;
         }
         /* **************************************************************************
          * If no data are read from file and the return value of fgets() is NULL,
          * Then set the cursor in the file will be set to the starting position of
          * the data sample file
          * **************************************************************************/
         else
         {
            fseek(hGPSdata, 0, SEEK_SET);
         }
      }

      /* Printing the data which are read from the file. This is for debugging purpose.
       * CAUTION: When this trace level is enabled this will fill the TTFis Buffer ASAP.
       * So, the TTFis will keep on printing the Data for sometime */
      if (s32Try_Count < GPS_S32_MAX_TRY_SAMPLE_DATA)
      {
         devgps_vTraceOut( (tU32)TR_LEVEL_USER_4, " %d %d %d %d %d %d %d %f %f %f %f %f %f %f %d %d %f %f %f %f %f %f %d %d",
                                                   s32Solution_Type,
                                                   s32DOPMask_Exceeded,
                                                   s32DR_Sensor_Data,
                                                   s32Fix_Is_Validated,
                                                   s32Velocity_Is_Valid,
                                                   s32DGPS_Position,
                                                   s32Position_Format,
                                                   f32Position_ECEF_dX,
                                                   f32Position_ECEF_dY,
                                                   f32Position_ECEF_dZ,
                                                   f32Position_LLA_dAlt,
                                                   f32Position_LLA_dAlt_MSL,
                                                   f32Position_LLA_dLat,
                                                   f32Position_LLA_dLon,
                                                   s32Position_LLA_enDATUM,
                                                   s32Velocity_Format,
                                                   f32Velocity_ECEF_dVx,
                                                   f32Velocity_ECEF_dVy,
                                                   f32Velocity_ECEF_dVz,
                                                   f32Velocity_NED_dVd,
                                                   f32Velocity_NED_dVe,
                                                   f32Velocity_NED_dVn,
                                                   s32Time_Format,
                                                   s32Time_Validity);
      }
      else
      {
         devgps_vTraceOut( (tU32)TR_LEVEL_FATAL," No.of Fields read:%d, Try Count:%d. Please feed the Sample file with correct format",s32NoFields,s32Try_Count );
         s32ReturnValue = OSAL_E_INVALIDVALUE;
      }
   }
   else
   {
      devgps_vTraceOut( (tU32)TR_LEVEL_USER_4," Get default data");

      s32Solution_Type           = GPS_EN_DATA_SOLUTION_TYPE;
      s32DOPMask_Exceeded        = GPS_BOOL_DATA_DOP_MASK_EXCEEDED;
      s32DR_Sensor_Data          = GPS_BOOL_DATA_DR_SENSOR_DATA;
      s32Fix_Is_Validated        = GPS_BOOL_DATA_FIX_IS_VALIDATED;
      s32Velocity_Is_Valid       = GPS_BOOL_DATA_VELOCITY_IS_VALID;
      s32DGPS_Position           = GPS_BOOL_DATA_DGPS_POSITION;
      s32Position_Format         = GPS_EN_DATA_POSITION_FORMAT;
      f32Position_ECEF_dX        = GPS_DOUBLE_DATA_POSITION_ECEF_DX;
      f32Position_ECEF_dY        = GPS_DOUBLE_DATA_POSITION_ECEF_DY;
      f32Position_ECEF_dZ        = GPS_DOUBLE_DATA_POSITION_ECEF_DZ;
      f32Position_LLA_dAlt       = GPS_DOUBLE_DATA_POSITION_LLA_DALT;
      f32Position_LLA_dAlt_MSL   = GPS_DOUBLE_DATA_POSITION_LLA_DALT_MSL;
      f32Position_LLA_dLat       = GPS_DOUBLE_DATA_POSITION_LLA_DLAT;
      f32Position_LLA_dLon       = GPS_DOUBLE_DATA_POSITION_LLA_DLON;
      s32Position_LLA_enDATUM    = GPS_EN_DATA_POSITION_LLA_ENDATUM;
      s32Velocity_Format         = GPS_EN_DATA_VELOCITY_FORMAT;
      f32Velocity_ECEF_dVx       = GPS_DOUBLE_DATA_VELOCITY_ECEF_DVX;
      f32Velocity_ECEF_dVy       = GPS_DOUBLE_DATA_VELOCITY_ECEF_DVY;
      f32Velocity_ECEF_dVz       = GPS_DOUBLE_DATA_VELOCITY_ECEF_DVZ;
      f32Velocity_NED_dVd        = GPS_DOUBLE_DATA_VELOCITY_NED_DVD;
      f32Velocity_NED_dVe        = GPS_DOUBLE_DATA_VELOCITY_NED_DVE;
      f32Velocity_NED_dVn        = GPS_DOUBLE_DATA_VELOCITY_NED_DVN;
      s32Time_Format             = GPS_EN_DATA_TIME_FORMAT;
      s32Time_Validity           = GPS_EN_DATA_TIME_VALIDITY;
   }
   /*This is done to increase the readability of the code.*/
   prGPSPos = &(Bpgps_rGPSFullRecord.trMeasuredPosition);

   prGPSPos->trSolutionMode.enSolutionType = (OSAL_tenGPSNAVSolutionType)s32Solution_Type;
   prGPSPos->trSolutionMode.bDOPMaskExceeded = (tBool)s32DOPMask_Exceeded;
   prGPSPos->trSolutionMode.bDRSensorData = (tBool)s32DR_Sensor_Data;
   prGPSPos->trSolutionMode.bFixIsValidated = (tBool)s32Fix_Is_Validated;
   prGPSPos->trSolutionMode.bVelocityIsValid = (tBool)s32Velocity_Is_Valid;
   prGPSPos->trSolutionMode.bDGPSPosition = (tBool)s32DGPS_Position;

   prGPSPos->trPosition.enPositionFormat = (OSAL_tenGPSPositionFormat)s32Position_Format;
   /* Checking the position format and filling the values only appropriate for that */
   if(prGPSPos->trPosition.enPositionFormat == GPS_POS_ECEF)
   {
      prGPSPos->trPosition.uPosition.trPositionECEF.dX = (tDouble)f32Position_ECEF_dX;
      prGPSPos->trPosition.uPosition.trPositionECEF.dY = (tDouble)f32Position_ECEF_dY;
      prGPSPos->trPosition.uPosition.trPositionECEF.dZ = (tDouble)f32Position_ECEF_dZ;
   }
   else
   {
      prGPSPos->trPosition.uPosition.trPositionLLA.dAlt = (tDouble)f32Position_LLA_dAlt;
      prGPSPos->trPosition.uPosition.trPositionLLA.dAltMSL = (tDouble)f32Position_LLA_dAlt_MSL;
      prGPSPos->trPosition.uPosition.trPositionLLA.dLat = (tDouble)f32Position_LLA_dLat;
      prGPSPos->trPosition.uPosition.trPositionLLA.dLon = (tDouble)f32Position_LLA_dLon;
      prGPSPos->trPosition.uPosition.trPositionLLA.enDatum = (OSAL_tenGPSDatum)s32Position_LLA_enDATUM;
   }

   prGPSPos->trVelocity.enVelocityFormat = (OSAL_tenGPSVelocityFormat)s32Velocity_Format;
   /* Checking the velocity format and filling the values only appropriate for that */
   if (prGPSPos->trVelocity.enVelocityFormat == GPS_VEL_ECEF)
   {
      prGPSPos->trVelocity.uVelocity.trVelocityECEF.dVx = (tDouble)f32Velocity_ECEF_dVx;
      prGPSPos->trVelocity.uVelocity.trVelocityECEF.dVy = (tDouble)f32Velocity_ECEF_dVy;
      prGPSPos->trVelocity.uVelocity.trVelocityECEF.dVz = (tDouble)f32Velocity_ECEF_dVz;
   }
   else
   {
      prGPSPos->trVelocity.uVelocity.trVelocityNED.dVd  = (tDouble)f32Velocity_NED_dVd;
      prGPSPos->trVelocity.uVelocity.trVelocityNED.dVe  = (tDouble)f32Velocity_NED_dVe;
      prGPSPos->trVelocity.uVelocity.trVelocityNED.dVn  = (tDouble)f32Velocity_NED_dVn;
   }

   prGPSPos->trTime.enTimeFormat = (OSAL_tenGPSTimeFormat)s32Time_Format;
   prGPSPos->trTime.enTimeValidity = (OSAL_tenGPSTimeValidity)s32Time_Validity;

   prGPSPos->trTime.uTime.trUTCTime.u16Day = rSystemTime.tm_mday ;
   prGPSPos->trTime.uTime.trUTCTime.u16Hour = rSystemTime.tm_hour;
   prGPSPos->trTime.uTime.trUTCTime.u16Millisecond = rTimeOfDay.tv_usec / 1000;
   prGPSPos->trTime.uTime.trUTCTime.u16Minute = rSystemTime.tm_min;
   prGPSPos->trTime.uTime.trUTCTime.u16Month = rSystemTime.tm_mon + 1;
   prGPSPos->trTime.uTime.trUTCTime.u16Second = rSystemTime.tm_sec;
   prGPSPos->trTime.uTime.trUTCTime.u16Year = rSystemTime.tm_year + 1900 ; /* vd_sensor will subtract 1900 */
   return s32ReturnValue;
}

/*********************************************************************************
| function        :GPS_s32IORead
|---------------------------------------------------------------------------------
| parameters      :tPS8 pBuffer, tU32 maxbytes
| return value    :OSAL_E_NOSPACE - If the no memory is available.
|                  OSAL_E_TIMEOUT - If the record ID is invalid.
|                  size of the OSAL_trGPSRecordHeader - On read Success.
|---------------------------------------------------------------------------------
| description     :This function fills a user supplied buffer with information about
|                  the latest GPS data record.  The contents of the data record are
|                  retrieved through a call to OSAL_s32IOControl with the command
|                  being OSAL_C_S32_IOCTL_GPS_READ_RECORD and the argument being a
|                  pointer to a structure, which contains a pointer to a data buffer
|                  and a record id.  The record ID must correspond to the record ID
|                  that GPS_s32IORead wrote into pBuffer.
|                  The data record contents are written into the buffer
|                  au8DataRecordBuffer.  The function GPS_s32IOControl copies these
|                  buffered data to the client's buffer.
|---------------------------------------------------------------------------------
|  History:
|  Date       Modification                          Author
|  09.05.12   Ported from GEN2 T-Engine platform    Jaikumar Dhanapal
**********************************************************************************/

tS32 GPS_s32IORead( tPS8 pBuffer, tU32 maxbytes )
{
   tVoid *pvBuffer = (tVoid*)pBuffer;
   OSAL_trGPSRecordHeader *ptrHdr = (OSAL_trGPSRecordHeader*)pvBuffer;
   tS32 s32Size = 0;
   tS32 s32NoFields = 0;
   tS32 s32ReturnValue = OSAL_E_NOERROR;

   if( maxbytes < sizeof(OSAL_trGPSRecordHeader) )
   {
      s32ReturnValue = OSAL_E_NOSPACE;
   }

    /* Make sure a record is available. */

   else if( u32RecordId == GPS_INVALID_RECORD_ID )
   {
 		/* This should never happen. */
      s32ReturnValue = OSAL_E_TIMEOUT;
   }
   else
   {
      /* Ensure that new data are not delivered while we fill in the
            data structures. */
      GPS_vEnterCriticalSection();

      /* Set a default value.  If a measured position is available, it
            will be overwritten below. */
      ptrHdr->u32TimeStamp = 0;

      /* If a measured position field is available, is valid and is one
            of the field types the client has requested, it is added to the
            data record. */
      if( bLatestPositionValid &&
             (u32RequestedFieldTypes & OSAL_C_S32_GPS_FIELD_TYPE_MEASURED_POSITION) )
      {
         ptrHdr->trFieldInfo[s32NoFields].s32Type =
                 OSAL_C_S32_GPS_FIELD_TYPE_MEASURED_POSITION;
         ptrHdr->trFieldInfo[s32NoFields].s32Size =
                 (sizeof(OSAL_trGPSMeasuredPosition) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1))
                 &
                 ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

         ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

             /* Copy data to the data record buffer. */
         OSAL_pvMemoryCopy(
                 au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
                 &rLatestPosition,
                 ptrHdr->trFieldInfo[s32NoFields].s32Size );

         s32Size     += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
         s32NoFields += 1;

           /* Set the time stamp in the record header. */
         if( rLatestPosition.trTimeStamp.bTsValid )
         {
            ptrHdr->u32TimeStamp = rLatestPosition.trTimeStamp.u32Timestamp;
         }
      }

         /* Likewise for a tracking data field. */
      if( bLatestTrackDataValid &&
             (u32RequestedFieldTypes & OSAL_C_S32_GPS_FIELD_TYPE_TRACKING_DATA) )
      {
         ptrHdr->trFieldInfo[s32NoFields].s32Type =
                 OSAL_C_S32_GPS_FIELD_TYPE_TRACKING_DATA;

         ptrHdr->trFieldInfo[s32NoFields].s32Size =
                 (sizeof(OSAL_trGPSTrackData) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1))
                 &
                 ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

         ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

             /* Copy data to the data record buffer. */
         OSAL_pvMemoryCopy(
                 au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
                 &rLatestTrackData,
                 ptrHdr->trFieldInfo[s32NoFields].s32Size );

         s32Size     += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
         s32NoFields += 1;
      }

      /* Likewise for the visible satellites list. */
      if( bLatestVisibleListValid &&
             (u32RequestedFieldTypes & OSAL_C_S32_GPS_FIELD_TYPE_VISIBLE_LIST) )
      {

         ptrHdr->trFieldInfo[s32NoFields].s32Type =
                 OSAL_C_S32_GPS_FIELD_TYPE_VISIBLE_LIST;

         ptrHdr->trFieldInfo[s32NoFields].s32Size =
                 (sizeof(OSAL_trGPSVisibleList) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1))
                 &
                 ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

         ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

             /* Copy data to the data record buffer. */
         OSAL_pvMemoryCopy(
              au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
              &rLatestVisibleList,
              ptrHdr->trFieldInfo[s32NoFields].s32Size );

         s32Size     += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
         s32NoFields += 1;
      }

      /* Likewise for the satellite health data. */
      if( bLatestSatelliteHealthValid &&
             (u32RequestedFieldTypes & OSAL_C_S32_GPS_FIELD_TYPE_SATELLITE_HEALTH) )
      {
         ptrHdr->trFieldInfo[s32NoFields].s32Type =
                 OSAL_C_S32_GPS_FIELD_TYPE_SATELLITE_HEALTH;

         ptrHdr->trFieldInfo[s32NoFields].s32Size =
                 (sizeof(OSAL_trGPSSatelliteHealth) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1))
                 &
                 ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

         ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

             /* Copy data to the data record buffer. */
         OSAL_pvMemoryCopy(
                 au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
                 &rLatestSatelliteHealth,
                 ptrHdr->trFieldInfo[s32NoFields].s32Size );

         s32Size     += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
         s32NoFields += 1;
      }

         /* Likewise for the extended data. */
      if( bLatestExtDataValid &&
             (u32RequestedFieldTypes & OSAL_C_S32_GPS_FIELD_TYPE_EXT_DATA) )
      {
         ptrHdr->trFieldInfo[s32NoFields].s32Type =
                 OSAL_C_S32_GPS_FIELD_TYPE_EXT_DATA;

         ptrHdr->trFieldInfo[s32NoFields].s32Size =
                 (sizeof(OSAL_trGPSExtData) +(OSAL_C_U32_GPS_MAX_ELEMSIZE-1))
                 &
                 ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

         ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

             /* Copy data to the data record buffer. */
         OSAL_pvMemoryCopy(
                 au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
                 &rLatestExtData,
                 ptrHdr->trFieldInfo[s32NoFields].s32Size );

         s32Size     += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
         s32NoFields += 1;
      }

         /* Likewise for the test mode data. */
      if( bLatestTestModeDataValid &&
             (u32RequestedFieldTypes & OSAL_C_S32_GPS_FIELD_TYPE_TEST_MODE_DATA) )
      {
         ptrHdr->trFieldInfo[s32NoFields].s32Type =
                 OSAL_C_S32_GPS_FIELD_TYPE_TEST_MODE_DATA;

         ptrHdr->trFieldInfo[s32NoFields].s32Size =
                 (sizeof(OSAL_trGPSTestModeData) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1))
                 &
                 ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

         ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

             /* Copy data to the data record buffer. */
         OSAL_pvMemoryCopy(
                 au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
                 &rLatestTestModeData,
                 ptrHdr->trFieldInfo[s32NoFields].s32Size );

         s32Size     += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
         s32NoFields += 1;
      }
        /* Likewise for the normal mode test data. */
      if( bLatestNormalTestDataValid &&
            (u32RequestedFieldTypes & OSAL_C_S32_GPS_FIELD_TYPE_NORMAL_TEST_DATA) )
      {
         ptrHdr->trFieldInfo[s32NoFields].s32Type =
              OSAL_C_S32_GPS_FIELD_TYPE_NORMAL_TEST_DATA;

         ptrHdr->trFieldInfo[s32NoFields].s32Size =
              (sizeof(OSAL_trGPSNormalTestData) + 3) & ~3;

         ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

           /* Copy data to the data record buffer. */
         OSAL_pvMemoryCopy(
              au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
              &rLatestNormalTestData,
              (tU32) ptrHdr->trFieldInfo[s32NoFields].s32Size );

         s32Size += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
         s32NoFields += 1;
      }
      /* Likewise for the tigth coulping test data. */
      if( bLatestTightCouplingDataValid &&
            (u32RequestedFieldTypes & OSAL_C_S32_GPS_FIELD_TYPE_TC_DATA) )
      {
         ptrHdr->trFieldInfo[s32NoFields].s32Type =
              OSAL_C_S32_GPS_FIELD_TYPE_TC_DATA;

         ptrHdr->trFieldInfo[s32NoFields].s32Size =
              (sizeof(OSAL_trGPSTightCouplingData) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1))
              &
              ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

         ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

         /* Copy data to the data record buffer. */
         OSAL_pvMemoryCopy(
              au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
              &rLatestTightCouplingData,
              (tU32) ptrHdr->trFieldInfo[s32NoFields].s32Size );

         s32Size += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
         s32NoFields += 1;
      }

      /* Fill in the remaining header fields. */
      ptrHdr->u32RecordId = u32RecordId;
      ptrHdr->s32Size = s32Size;
      ptrHdr->s32NoFields = s32NoFields;

      /* Store information about the buffered data record. */
      u32DataRecordBufferId = u32RecordId;
      u32DataRecordBufferSize = s32Size;

      /* Set the current record ID to the invalid marker.  This ensures
         that the record will not be delivered to the client more than once. */
      u32RecordId = GPS_INVALID_RECORD_ID;

      /* Leave the critical section. */
      GPS_vLeaveCriticalSection();

      s32ReturnValue = sizeof(OSAL_trGPSRecordHeader);
   }
   return s32ReturnValue;
}

/*********************************************************************************
| function        :devgps_vTraceOut
|---------------------------------------------------------------------------------
| parameters      :u32Level -  The trace level for output
|                  const tChar *pcFormatString - Format String
| return value     :None
|---------------------------------------------------------------------------------
| description     :To print trace messages on console IO (TTFI). GPS device
|                  driver calls this function at necessary places.
|---------------------------------------------------------------------------------
|  History:
|  Date            Modification              Author
|  23.05.12        Initial Version           Jaikumar Dhanapal
**********************************************************************************/

static tVoid devgps_vTraceOut(  tU32 u32Level,const tChar *pcFormatString,... )
{
   if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_GPS, u32Level) != FALSE)
   {
      /*
   Parameter to hold the argument for a function, specified the format
   string in pcFormatString
   defined as:
   typedef char* va_list in stdarg.h
   */
      va_list argList = {0};
      /*
   vsnprintf Returns Number of bytes Written to buffer or a negative
   value in case of failure
   */
      tS32 s32Size ;
      /*
   Buffer to hold the string to trace out
   */
      tS8 u8Buffer[MAX_TRACE_SIZE];
      /*
   Position in buffer from where the format string is to be
   concatenated
   */
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /*
   Flush the String
   */
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /*
   Copy the String to indicate the trace is from the RTC device
   */

      /*
   Initialize the argList pointer to the beginning of the variable
   arguement list
   */
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
   Collect the format String's content into the remaining part of
   the Buffer
   */
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,
                  sizeof(u8Buffer),
                  pcFormatString,
                  argList ) ) )
      {
         return;
      }

      /*
   Trace out the Message to TTFis
   */
      LLD_vTrace( OSAL_C_TR_CLASS_DEV_GPS,
      u32Level,
      u8Buffer,
      (tU32)s32Size );   /* Send string to Trace*/
      /*
   Performs the appropiate actions to facilitate a normal return by a
   function that has used the va_list object
   */
      va_end(argList);
   }
}
