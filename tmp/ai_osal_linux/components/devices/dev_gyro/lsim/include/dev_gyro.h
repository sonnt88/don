/*------------------------------------------------------------------------------------------|
| FILE           : gyro_gyro.h                                                              |
|                                                                                           |
| DESCRIPTION    :This Is Header File Of dev_gyro.c                                         |
|-------------------------------------------------------------------------------------------|
| Date           |       Version        | Author & comments                                 |
|----------------|----------------------|---------------------------------------------------|
| 28.Dec.2012    |  Initial Version 1.0 | Sanjay G(RBEI/ECF5)                               |
-------------------------------------------------------------------------------------------*/


/********************************************************************************************
*                                   Macro Declarations                                      *
*********************************************************************************************/

/*LSIM Gyro Version*/               /* read v01.00 */
#define DEV_GYRO_DRIVER_VERSION         0x0100

/*Gyro Data Sampling Rate In Nano Seconds*/
#define  GYRO_DATA_SAMPLING_RATE_IN_NS  (50000000)

/*Maximum Time To Get Message From Message Queue*/
#define GYRO_DATA_TIMEOUT_VALUE  60 

/*Gyro Event Name To Start Reading Data From Message Queue */
#define GYRO_C_STRING_EVENT_NAME "GYRO_EV"

/*Flag To Be Set After Creation Of Gyro Driver */
#define DEV_GYRO_CREATE             0x01

/*Flag To Be Set After Destroying Gyro Driver */
#define DEV_GYRO_FLAG_CLEAR         0x00

/*!
* macro for ioctrl call @ref  OSAL_C_S32_IOCTRL_GYRO_GETRESOLUTION
* to convert time in ns from @ref GYRO_DATA_SAMPLING_RATE_IN_NS to Hz
*/
#define  GYRO_CONVERT_NS_2_HZ(time_in_ns)                        \
((tU16)((tU32)1000000000/((tU32)(time_in_ns))))

/*Total Number Of Gyro Entries In The Ring Buffer*/
#define NO_OF_GYRO_RINGBUFFER_ENTRIES    \
((tU16)(OSAL_C_S16_3D_GYRO_RINGBUFFERSIZE / sizeof(OSAL_trIOCtrl3dGyroData)))

/*Gyro Message Queue Name Where Trip File Data Is Updated By Data Dispatcher*/
#define  GYRO_C_STRING_MSGQ_NAME                "LsimGyroMessageQueue"
#define  GYRO_C_STRING_RING_BUF_SEMPHORE_NAME   "SEM_RING_BUFFER"
#define  GYRO_C_STRING_DIAGSEMPHORE_NAME        "SEM_DIAG_RING_BUFFER"
#define  GYRO_C_STRING_INT_BUF_SEMPHORE_NAME    "INTERMEDIATE_BUF_SEMAPHORE"


/*Gyro Message Queue Length*/
#define  GYRO_MESSAGE_QUEUE_LENGTH    (20)

/*Intermediate Buffer Length*/
#define  MAX_INT_BUF_SIZE  50

/*Maximum Number Elements In One Gyro Record*/
#define  GYRO_MAX_ELEMENTS     7

/*Index Of Elements In The Data Received(Array) From Message Queue*/
#define  TIME_STAMP    0
#define  R_VALUE       1
#define  S_VALUE       2
#define  T_VALUE       3


/*Gyro Mask Value To Get 16 Bit Value Of Gyro Yaw Output */
#define GYRO_RAW_VALUE_MASK         0x0000FFFF


/*Maximum Time To Wait In GYRO_s32IORead() and vUpdateGyroRingBuffer()
  For Getting Mutex Lock Of RingBuffer In MilliSeconds*/
#define GYRO_RINGBUFFER_MUTEX_LOCK_TIMEOUT_IN_MSEC OSAL_C_TIMEOUT_FOREVER


/*Macro To Get Cycle Time In MilliSeconds*/
#define GYRO_TIMER_IN_MS                      \
(OSAL_tMSecond)((tU32)(GYRO_DATA_SAMPLING_RATE_IN_NS)/((tU32)1000000))


/*Gyro Threads Attributes */
#define GYRO_MAIN_THREAD_PRIORITY        15
#define GYRO_MAIN_THREAD_STACK_SIZE      4096
#define GYRO_C_STRING_MAIN_THREAD_NAME   "GYRO_TASK"
#define GYRO_C_STRING_TIMER_THREAD_NAME  "GYRO_TIMER_TASK"
#define GYRO_C_STRING_MSGQ_THREAD_NAME   "GYRO_MSGQ_TASK"


/*This bit is set in hGyroEventDataReadTimeout if the gyro
* thread starts the next read*/
#define GYRO_C_EV_GET_DATA        0x01
#define GYRO_C_EV_GYRO_SHUTDOWN   0x02
#define GYRO_C_EV_MASK            0x7FF

/*Default Value Of Maximum Time To Wait In GYRO_s32IORead() For Ringbuffer Data In Seconds*/
#define GYRO_DEFAULT_IOREAD_TIMEOUTVAL_IN_SEC    1


/*Macros Used To Release Resources If Create Fails*/
#define MESSAGE_QUEUE          1  
#define TIMER_CREATE           2
#define RINGBUF_SEMAPHORE      3 
#define DIAGRAWBUF_SEMAPHORE   4
#define INT_MED_BUF_SEMAPHORE  5
#define DATA_COLLECTING_EVENT  6
#define MAIN_THREAD            7
#define TIMER_THREAD           8


/********************************************************************************************
*                           Variable Declaration (Scope: Global)                            *
*********************************************************************************************/


/*Data Structure For Gyro Ring Buffer*/
typedef struct
{
	/* Current Number Of Valid Data Entries In The Buffer */
	tU16  u16Valid_Entries;

	/* Index Of Next Data Record To Read */
	tU16  u16Read_ID;

	/*Gyro Data Records */
	OSAL_trIOCtrl3dGyroData Data[NO_OF_GYRO_RINGBUFFER_ENTRIES];
}trGyroBuf;

/*Gyro RingBuffer*/
static trGyroBuf rGyroBuf;

/*The production diagnosis reqired the raw value of gyro output*/
static OSAL_trIOCtrlDiag3dGyroRawData rDiagGyroRawData;

/*Maximum No.of Gyro Records In One Packet*/
#define  GYRO_MAX_RECORDS    10


/*Format In Which Gyro Data Is In Message Queue*/
typedef struct
{
  tU32 u32TotalNoOfRecords;  //No Of Records In The Packet
  tU32 u32TripGyro3dDataBundle[GYRO_MAX_RECORDS][GYRO_MAX_ELEMENTS];//Holds Bundle Of Records
}trGyroDataPacket;

/*Format In Which Gyro Data Is Posted In MsgQ*/
typedef struct
{
	tS32 s32ConFd;
	tU32 u32MaxData;
	trGyroDataPacket rGyroData;
}trGyroMsgQData;

typedef struct
{
	tU32 u32TimeStamp;
    tU32 u32RaxisVal;
	tU32 u32SaxisVal;
	tU32 u32TaxisVal;
	tU16 u16Rstatus;
	tU16 u16Sstatus;
	tU16 u16Tstatus;
}trGyroData;

typedef struct
{
  tU8 u8FifoIndex;
  trGyroData IntMedBuffer[50];
}trInterMediateBuffer;

trInterMediateBuffer  rIntBuf;

/*Message Queue Handle To Where Dispatcher Posts Data And From Where 
  Gyro Driver Collects Data*/
OSAL_tMQueueHandle pGyroMsgQueHandle=OSAL_NULL;

/*Gyro Open Flag*/
static tBool bGyroOpen= FALSE;

/*Gyro Data Will Be Read Only If This Flag Is TRUE.*/
static tBool bGyroEn = FALSE;

/*Flags To Maintain The Status Of Create And Open Of Device*/
static tU8 u8Gyro_Flags = 0;

/*Handle To OSAL timer, Trigger For Gyro Data Reading From MsgQ*/
static OSAL_tTimerHandle hTimerGyroSampleTrigger = 0;

/*Handle To OSAL Event To Signal Data Aquisition From MsgQ*/
static OSAL_tEventHandle hGyroEventDataReadTimeout = OSAL_C_INVALID_HANDLE;

/*OSAL RingBufferSemaphore Handle*/
static OSAL_tSemHandle hGyroRingBufSemHandle;

/*OSAL DiagRingBuffer Semaphore Handle*/
static OSAL_tSemHandle hGyroDiagRawBufSemHandle;

/*OSAL IntermediateBuffer Semaphore Handle*/
static OSAL_tSemHandle hGyroIntMedBufSemHandle;

/*OSAL Thread Running Flag, Set On Thread Creation*/
static tBool bGyroThreadRunning = FALSE;

/*OSAL Main Thread ID*/
static OSAL_tThreadID s32GyroMainThreadID = OSAL_ERROR;

/*OSAL Timer Thread ID*/
static OSAL_tThreadID s32GyroTimerThreadID = OSAL_ERROR;

/*Time out value for IORead in seconds*/
static tS32 s32ActIOReadTimeoutValueInSec = GYRO_DEFAULT_IOREAD_TIMEOUTVAL_IN_SEC;

/*Error Level*/
tU16 u16Errorlevel=0;

/********************************************************************************************
*                          Function Prototypes (Scope: Local)                               *
*********************************************************************************************/
static tVoid vGyroMainThread(tPVoid pvArg);
tS32 GYRO_s32CreateGyroDevice(tVoid);
tVoid vUpdateGyroRingBuffer(tU16 u16ErrorCount,tU32 u32TimeStamp,trGyroData s32Gyro3dData);
tVoid vLSIM_GYROTrace(  tU32 u32TraceLevel,const tChar *pcFormatString,... );
static tVoid vGyroTimerHdlr (tPVoid pUnused);
tS32 GYRO_s32DestroyGyroDevice(tVoid);
static tVoid vGyroReadTimeOut (tPVoid pUnused);
static tVoid vGyroTimerThread(tPVoid pvArg);
static  tVoid vGyroMsgQThread(tPVoid pUnused);
static tVoid tVReleaseSourcesFrom(tU8 u8ReleaseFrom);

tS32 GYRO_IOOpen(tVoid);
tS32 GYRO_s32IORead(tPS8 ps8Buffer, tU32 u32maxbytes);
tS32 GYRO_s32IOClose(tVoid);
tS32 GYRO_s32IOControl(tS32 s32fun, tS32 s32arg);
