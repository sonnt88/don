/*****************************************************************************************
* Copyright (C) RBEI, 2013
* This software is property of Robert Bosch.
* Unauthorized duplication and disclosure to third parties is prohibited.
******************************************************************************************/
/*!
*\file     dev_gyro.c

*\brief    1: This File Simulates The GYRO OSAL Driver For LSIM.
*          2: The Source Code Reads Data From Message Queue Which Is Filled By
*             Data Dispatcher.Dispatcher Gets Data From Gyro Trip File Parser.
*          3: This Code Collects Data From The Gyro Message Queue Parser And Fills
*             Gyro Ring Buffer.
*          4:This Code Also Provides OSAL Interfaces To Applications
* 
*\author:   Sanjay G - RBEI/ECF5
*
*\par Copyright:
*(c)2013 -  RBEI - Robert Bosch Engineering and Business Solutions Limited
*\par History:
* Created -  28.Dec.2012
*
* Date         | Modification               | Author
********************************************************************************************/



/********************************************************************************************
*                               Header File Declaration                                     *
*********************************************************************************************/

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"
#include "dev_gyro.h"


/**********************************************************************************************
*                                   Function Definitions                                      *
***********************************************************************************************/



/**********************************************************************************************/
/*!
*\fn   		tS32 GYRO_s32CreateGyroDevice(tVoid)
*
*\brief  	Allocates All Resources Which Required By Gyro Driver And Spawn Gyro Thread 
*
*\param 	None
*
*\return	Success:OSAL_OK   Fail   :OSAL_ERROR
*
************************************************************************************************/

tS32 GYRO_s32CreateGyroDevice(tVoid)
{
   tS32 s32RetVal = OSAL_OK;
   OSAL_trThreadAttribute  rMainThreadAttr,rTimerThreadAttr,rMsgQThreadAttr;

   /*Initialise Ring Buffer Fields To Zero*/
   rGyroBuf.u16Valid_Entries = (tU16)0;
   rGyroBuf.u16Read_ID       = (tU16)0;


   /*Create A Message Queue*/
   if(OSAL_OK != OSAL_s32MessageQueueCreate((tCString)GYRO_C_STRING_MSGQ_NAME,
                         GYRO_MESSAGE_QUEUE_LENGTH,sizeof(trGyroMsgQData),
                         (OSAL_tenAccess)OSAL_EN_READWRITE,&pGyroMsgQueHandle))
   {
      vLSIM_GYROTrace(  TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Message Queue Creation Failed");
      s32RetVal=OSAL_ERROR;
   }
   /*Create Osal Timer*/
   else if(OSAL_OK != OSAL_s32TimerCreate((OSAL_tpfCallback)vGyroReadTimeOut,OSAL_NULL, &hTimerGyroSampleTrigger))
   {
      tVReleaseSourcesFrom(MESSAGE_QUEUE);
      s32RetVal = OSAL_ERROR;
      vLSIM_GYROTrace(  TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Timer Creation Failed");
   }
   /*Set Osal Timer*/
   else if(OSAL_OK != OSAL_s32TimerSetTime(hTimerGyroSampleTrigger,GYRO_DATA_TIMEOUT_VALUE,GYRO_DATA_TIMEOUT_VALUE))
   {				                                                /*cycle time in ms*//* interval time in ms */			
      tVReleaseSourcesFrom(TIMER_CREATE);
      s32RetVal = OSAL_ERROR;
      vLSIM_GYROTrace(TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Setting Timer Failed");
   }
   /* Create Semaphore To Sync Gyro Buffer Read/Write Operations*/
   else if(OSAL_OK !=OSAL_s32SemaphoreCreate( GYRO_C_STRING_RING_BUF_SEMPHORE_NAME,&hGyroRingBufSemHandle,1 ))
   {
      tVReleaseSourcesFrom(TIMER_CREATE);
      s32RetVal = OSAL_ERROR;
      vLSIM_GYROTrace(  TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Mutex Creation Failed");
   }
   /* Create Semaphore To Sync Gyro Buffer Read/Write Operations*/
   else if(OSAL_OK !=OSAL_s32SemaphoreCreate( GYRO_C_STRING_DIAGSEMPHORE_NAME,&hGyroDiagRawBufSemHandle,1 ))
   {
      
	  tVReleaseSourcesFrom(RINGBUF_SEMAPHORE);  
      s32RetVal = OSAL_ERROR;
      vLSIM_GYROTrace(TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Diag Ring Buffer Mutex Creation Failed");
   }
   else if(OSAL_OK !=OSAL_s32SemaphoreCreate( GYRO_C_STRING_INT_BUF_SEMPHORE_NAME,&hGyroIntMedBufSemHandle,1 ))
   {
      tVReleaseSourcesFrom(DIAGRAWBUF_SEMAPHORE);  
      s32RetVal = OSAL_ERROR;
      vLSIM_GYROTrace(TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Diag Ring Buffer Mutex Creation Failed");
   }
   
   /* Create event for timer callback signal to gyro thread, to avoid
   long-lasting  sadc read operations directly in the callback routine */
   else if(OSAL_OK != OSAL_s32EventCreate((tString)GYRO_C_STRING_EVENT_NAME,&hGyroEventDataReadTimeout))
   {
      tVReleaseSourcesFrom(INT_MED_BUF_SEMAPHORE);  
      s32RetVal = OSAL_ERROR;
      vLSIM_GYROTrace(TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Event Creation Failed");
   }
   else
   {
      /*create osal thread for gyro data aquisition at sadc*/
      bGyroThreadRunning = TRUE;

      rMainThreadAttr.szName = (tString)GYRO_C_STRING_MAIN_THREAD_NAME;
      rMainThreadAttr.u32Priority = GYRO_MAIN_THREAD_PRIORITY;
      rMainThreadAttr.s32StackSize = GYRO_MAIN_THREAD_STACK_SIZE;
      rMainThreadAttr.pfEntry = (OSAL_tpfThreadEntry)vGyroMainThread;
      rMainThreadAttr.pvArg = OSAL_NULL;
      s32GyroMainThreadID = OSAL_ThreadSpawn(&rMainThreadAttr);
      if( s32GyroMainThreadID == OSAL_ERROR)
      {
	     tVReleaseSourcesFrom(DATA_COLLECTING_EVENT);  
         s32RetVal = OSAL_ERROR;
         vLSIM_GYROTrace(TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Thread Creation Failed");
      }
      else
      {
         rTimerThreadAttr.szName = (tString)GYRO_C_STRING_TIMER_THREAD_NAME;
         rTimerThreadAttr.u32Priority = GYRO_MAIN_THREAD_PRIORITY;
         rTimerThreadAttr.s32StackSize = GYRO_MAIN_THREAD_STACK_SIZE;
         rTimerThreadAttr.pfEntry = (OSAL_tpfThreadEntry)vGyroTimerThread;
         rTimerThreadAttr.pvArg = OSAL_NULL;
         s32GyroTimerThreadID = OSAL_ThreadSpawn(&rTimerThreadAttr);

         if( s32GyroTimerThreadID == OSAL_ERROR)
         {
		    tVReleaseSourcesFrom(MAIN_THREAD); 
            s32RetVal = OSAL_ERROR;
            vLSIM_GYROTrace(TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Thread Creation Failed");
         }
		 else
		 {
		      rMsgQThreadAttr.szName = (tString)GYRO_C_STRING_MSGQ_THREAD_NAME;
              rMsgQThreadAttr.u32Priority = GYRO_MAIN_THREAD_PRIORITY;
              rMsgQThreadAttr.s32StackSize = GYRO_MAIN_THREAD_STACK_SIZE;
              rMsgQThreadAttr.pfEntry = (OSAL_tpfThreadEntry)vGyroMsgQThread;
              rMsgQThreadAttr.pvArg = OSAL_NULL;
              s32GyroTimerThreadID = OSAL_ThreadSpawn(&rMsgQThreadAttr);

              if( s32GyroTimerThreadID == OSAL_ERROR)
              {
				  tVReleaseSourcesFrom(TIMER_THREAD); 
                   s32RetVal = OSAL_ERROR;
                   vLSIM_GYROTrace(TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Thread Creation Failed");
              }
		   }
       }
   }

   /* Enable Gyro data acquisition if adc channels, timer, mutex are ok */
   if(s32RetVal == OSAL_OK)
   {
      bGyroEn = TRUE;
      u8Gyro_Flags |= DEV_GYRO_CREATE;
   }
   else
   {
      printf("Oops!Creating dev_gyro Failed!!\n");
      vLSIM_GYROTrace(TR_LEVEL_FATAL,"GYRO_s32CreateGyroDevice:Gyro Driver Creation Failed");
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}


/************************************************************************/
/*!
*\fn   		tVoid tVReleaseSourcesFrom(tU8 u8ReleaseFrom) 
*
*\brief  	Release All Resources Which Are Allocated By Gyro Driver 
				  
*\param 	tU8 u8ReleaseFrom:Resources From Where To Release
*
*
*\return	Success:OSAL_OK 
*           Fail   :OSAL_ERROR 
**************************************************************************/
static tVoid tVReleaseSourcesFrom(tU8 u8ReleaseFrom)
{

   while(u8ReleaseFrom)
   {
      switch(u8ReleaseFrom)
      {
         case MESSAGE_QUEUE:
         {
            (tVoid)OSAL_s32MessageQueueClose(pGyroMsgQueHandle);
            (tVoid)OSAL_s32MessageQueueDelete(GYRO_C_STRING_MSGQ_NAME);
         }
         break;

         case TIMER_CREATE:
         {
            (tVoid)OSAL_s32TimerSetTime(hTimerGyroSampleTrigger,0,0);
            (tVoid)OSAL_s32TimerDelete (hTimerGyroSampleTrigger); 
         }
		 break;

         case RINGBUF_SEMAPHORE:
         {
            (tVoid)OSAL_s32SemaphoreWait( hGyroRingBufSemHandle,OSAL_C_TIMEOUT_FOREVER );
            (tVoid)OSAL_s32SemaphoreDelete( GYRO_C_STRING_RING_BUF_SEMPHORE_NAME );
         }
		 break;
	
         case DIAGRAWBUF_SEMAPHORE:
		 {
            (tVoid)OSAL_s32SemaphoreWait( hGyroDiagRawBufSemHandle,OSAL_C_TIMEOUT_FOREVER );
            (tVoid)OSAL_s32SemaphoreDelete( GYRO_C_STRING_DIAGSEMPHORE_NAME );
         }
         break;

         case INT_MED_BUF_SEMAPHORE:
         {
            (tVoid)OSAL_s32SemaphoreWait( hGyroDiagRawBufSemHandle,OSAL_C_TIMEOUT_FOREVER );
            (tVoid)OSAL_s32SemaphoreDelete( GYRO_C_STRING_INT_BUF_SEMPHORE_NAME );
         }
         break;

         case DATA_COLLECTING_EVENT:
         {
            (tVoid)OSAL_s32EventClose(hGyroEventDataReadTimeout);
            (tVoid)OSAL_s32EventDelete((tString)GYRO_C_STRING_EVENT_NAME);
         }
         break;

         case MAIN_THREAD:
		 {
            (tVoid)OSAL_s32ThreadDelete(s32GyroMainThreadID);
         }
         break;

         case TIMER_THREAD:
         {
            (tVoid)OSAL_s32ThreadDelete(s32GyroTimerThreadID);
         }
         break;
		
         default:
         break;
      }
      u8ReleaseFrom--;
   }
}


/************************************************************************/
/*!
*\fn   		static tVoid tVpush(trGyroData  trGyro3dData)
*
*\brief  	Push Data Into Intermediate Buffer 

*\param 	trGyroData  trGyro3dData:Gyro Data Structure
*
*\return	None
*
**************************************************************************/

static tVoid tVpush(trGyroData  trGyro3dData)
{
   /*Check Whether Buffer Has Any Space Or Not.If Not Wait*/
   while(rIntBuf.u8FifoIndex>MAX_INT_BUF_SIZE)
   {
      OSAL_s32ThreadWait(GYRO_TIMER_IN_MS);
   }

   if(OSAL_OK == OSAL_s32SemaphoreWait( hGyroIntMedBufSemHandle,OSAL_C_TIMEOUT_FOREVER ))
   { 
      rIntBuf.IntMedBuffer[rIntBuf.u8FifoIndex].u32TimeStamp=trGyro3dData.u32TimeStamp;
      rIntBuf.IntMedBuffer[rIntBuf.u8FifoIndex].u32RaxisVal=trGyro3dData.u32RaxisVal;
      rIntBuf.IntMedBuffer[rIntBuf.u8FifoIndex].u32SaxisVal=trGyro3dData.u32SaxisVal;
      rIntBuf.IntMedBuffer[rIntBuf.u8FifoIndex].u32TaxisVal=trGyro3dData.u32TaxisVal;
							 
      rIntBuf.u8FifoIndex++;
	 
      if(OSAL_OK != OSAL_s32SemaphorePost( hGyroIntMedBufSemHandle ))
      {
         vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMsgQThread:Oops!!Unlocking Semaphore Failed");
      }
   }
   else
   {
      vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMsgQThread:Oops!!Locking Semaphore Failed");
   }
}


/********************************************************************************************/
/*!
*\fn   		static tVoid tVpop(trGyroData  *trGyro3dData) 
*
*\brief  	Pops Data From Intermediate Ring Buffer
				  
*\param 	trGyroData  *trGyro3dData: Pointer To Data Where To Be Poped
*
*\return	None  
*
***********************************************************************************************/

static tVoid tVpop(trGyroData  *trGyro3dData)
{
     
   tU8 i=0;
	    
   while(rIntBuf.u8FifoIndex==0)/*No Data In The Buffer So Wait*/
   {
      OSAL_s32ThreadWait(GYRO_TIMER_IN_MS);
   }
    
	  
   if(OSAL_OK == OSAL_s32SemaphoreWait( hGyroIntMedBufSemHandle,OSAL_C_TIMEOUT_FOREVER ))
   {
      trGyro3dData->u32TimeStamp=rIntBuf.IntMedBuffer[0].u32TimeStamp;
      trGyro3dData->u32RaxisVal=rIntBuf.IntMedBuffer[0].u32RaxisVal;
      trGyro3dData->u32SaxisVal=rIntBuf.IntMedBuffer[0].u32SaxisVal;
      trGyro3dData->u32TaxisVal=rIntBuf.IntMedBuffer[0].u32TaxisVal;
		 
      for(i=0;i < rIntBuf.u8FifoIndex-1;i++)
      {
         rIntBuf.IntMedBuffer[i]=rIntBuf.IntMedBuffer[i+1];
      }
      rIntBuf.u8FifoIndex--;

      rIntBuf.IntMedBuffer[rIntBuf.u8FifoIndex].u32TimeStamp=0;
      rIntBuf.IntMedBuffer[rIntBuf.u8FifoIndex].u32RaxisVal=0;
      rIntBuf.IntMedBuffer[rIntBuf.u8FifoIndex].u32SaxisVal=0;
      rIntBuf.IntMedBuffer[rIntBuf.u8FifoIndex].u32TaxisVal=0;
		 
      if(OSAL_OK != OSAL_s32SemaphorePost( hGyroIntMedBufSemHandle ))
      {
         vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMsgQThread:Oops!!Unlocking Semaphore Failed");
      }
   }
}


/********************************************************************************************/
/*!
*\fn   		tVoid vGyroReadTimeOut (tPVoid pUnused) 
*
*\brief  	Osal Timer Call Back Function Which Trigger Event To Update Ring Buffer 
			With Error Counter Value 
				  
*\param 	pUnused :Unused Input Parameter 
*
*\return	None
*
***********************************************************************************************/

static tVoid vGyroReadTimeOut (tPVoid pUnused)
{
   tS32 s32RetVal = OSAL_ERROR;

   (tVoid)(pUnused);
   (tVoid)(s32RetVal);
   /* Post TimeOut Event To Update The Ring Buffer With Error Counter Value*/
   s32RetVal = OSAL_s32EventPost(hGyroEventDataReadTimeout,GYRO_C_EV_GET_DATA,OSAL_EN_EVENTMASK_OR);


   if( s32RetVal != OSAL_OK )
   {
      printf("vGyroReadTimeOut::Event Post Failed!!");
      vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroReadTimeOut:Event Post Failed");
   }
}

/*********************************************************************************************/
/*!
*\fn   		tVoid vGyroMsgQThread (tPVoid pUnused)   
*
*\brief  	Collects Data From Message Queue Updates In Intermediate Buffer   
				  
*\param 	pUnused :Unused Input Parameter
*
*\return	None
*
***********************************************************************************************/
static  tVoid vGyroMsgQThread(tPVoid pUnused)
{
   trGyroMsgQData rGyroMsgQMsg;
   trGyroData trGyro3dData;
   tU32 u32TimeStamp=0;
   tU32 pu32Message=0;
   tU32 u32Trav=0;
   rIntBuf.u8FifoIndex=0;
   
   while(bGyroThreadRunning == TRUE)
   {
      if( TRUE == bGyroEn )
      {
	        if(rIntBuf.u8FifoIndex<MAX_INT_BUF_SIZE)
	    	{
	      
		       if(sizeof(trGyroMsgQData)== OSAL_s32MessageQueueWait(pGyroMsgQueHandle,(tPU8)&rGyroMsgQMsg,
                                               sizeof(trGyroMsgQData),OSAL_NULL,(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER))
               {
				     
                  vLSIM_GYROTrace(TR_LEVEL_USER_3,"vGyroMsgQThread:Number Of Records=%d",rGyroMsgQMsg.rGyroData.u32TotalNoOfRecords);

                  for(u32Trav=0;u32Trav<rGyroMsgQMsg.rGyroData.u32TotalNoOfRecords;u32Trav++)
                  {
                     trGyro3dData.u32TimeStamp=rGyroMsgQMsg.rGyroData.u32TripGyro3dDataBundle[u32Trav][TIME_STAMP];
                     trGyro3dData.u32RaxisVal=rGyroMsgQMsg.rGyroData.u32TripGyro3dDataBundle[u32Trav][R_VALUE];
                     trGyro3dData.u32SaxisVal=rGyroMsgQMsg.rGyroData.u32TripGyro3dDataBundle[u32Trav][S_VALUE];
                     trGyro3dData.u32TaxisVal=rGyroMsgQMsg.rGyroData.u32TripGyro3dDataBundle[u32Trav][T_VALUE];
						   						   
                     vLSIM_GYROTrace(TR_LEVEL_USER_3,"vGyroMsgQThread:WaitTime=%d",trGyro3dData.u32TimeStamp);
                     vLSIM_GYROTrace(TR_LEVEL_USER_3,"vGyroMsgQThread:r-Value=%d",trGyro3dData.u32RaxisVal);
                     vLSIM_GYROTrace(TR_LEVEL_USER_3,"vGyroMsgQThread:s-Value=%d",trGyro3dData.u32SaxisVal);
                     vLSIM_GYROTrace(TR_LEVEL_USER_3,"vGyroMsgQThread:t-Value=%d",trGyro3dData.u32TaxisVal);
						 			 
                     tVpush(trGyro3dData);
                  }  
               }
               else
               {
			   
               }
            }
      }
   }
   OSAL_vThreadExit();
}


/*****************************************************************************************/
/*!
*\fn   		static tVoid vGyroTimerThread(tPVoid pvArg) 
*
*\brief  	Gyro Thread Which Waits For Event Triggered By Timer CallBack Starts 
			And Updates RingBuffer With Error Count
				  
*\param 	pvArg :Unused
*
*\return	None 
*
********************************************************************************************/


static tVoid vGyroTimerThread(tPVoid pvArg)
{

   tS32 s32EventWaitRetVal = OSAL_ERROR;
   trGyroData s32Gyro3dData;
   OSAL_tEventMask u32EventResultMask = 0;
   
   s32Gyro3dData.u32TimeStamp=0;
   s32Gyro3dData.u32RaxisVal=0;
   s32Gyro3dData.u32SaxisVal=0;
   s32Gyro3dData.u32TaxisVal=0;
   

   while(bGyroThreadRunning == TRUE)
   {
      
      if( TRUE == bGyroEn )
      {
         /* wait for an event */
         s32EventWaitRetVal = OSAL_s32EventWait(hGyroEventDataReadTimeout,GYRO_C_EV_MASK,
         OSAL_EN_EVENTMASK_OR,OSAL_C_TIMEOUT_FOREVER,&u32EventResultMask);

         if( s32EventWaitRetVal == OSAL_OK )
         {
            vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroTimerThread:Gyro TimeOut Error");
            /* correct event mask*/
            if(u32EventResultMask & GYRO_C_EV_GET_DATA)
            {
               /* clear event flag */
               (tVoid)OSAL_s32EventPost(hGyroEventDataReadTimeout,~GYRO_C_EV_GET_DATA,OSAL_EN_EVENTMASK_AND );
               u16Errorlevel++;
               vUpdateGyroRingBuffer(u16Errorlevel,0,s32Gyro3dData);
            }
            else if(u32EventResultMask & GYRO_C_EV_GYRO_SHUTDOWN)
            {
               /* clear event flag */
               (tVoid)OSAL_s32EventPost(hGyroEventDataReadTimeout,~GYRO_C_EV_GYRO_SHUTDOWN,OSAL_EN_EVENTMASK_AND );
            }
            else
            {
               vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMainThread:Clearing Event Failed");
            }
         }
         else
         {
            vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMainThread:Event Wait Failed");
            printf("vGyroMainThread:Event Wait Failed");
            break;
         }
      }
      else
	  {
         if(OSAL_OK != OSAL_s32TimerSetTime(hTimerGyroSampleTrigger,GYRO_DATA_TIMEOUT_VALUE,GYRO_DATA_TIMEOUT_VALUE))
         {
            vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMainThread:Timer Set Failed"); 
         }
		 /*Timer Expires For Every 60 Milli Seconds So Reset The Timer Before That*/
		 OSAL_s32ThreadWait(20);
      } 
   }
   OSAL_vThreadExit();
}

/***************************************************************************************/
/*!
*\fn   		static tVoid vGyroMainThread(tPVoid pvArg)    
*
*\brief  	Gyro Thread Which Waits For Event Triggered By Timer CallBack Starts Data
			Acquisition And Updates In Gyro Ring Buffer
				  
*\param 	pvArg :Unused
*
*
*\return	tVoid
*
******************************************************************************************/

static tVoid vGyroMainThread(tPVoid pvArg)
{

   OSAL_trIOCtrlAuxClockTicks rAuxClkTime = {0,0};
   trGyroData  trGyro3dData;
   tU32 u32TimeStamp=0;

   while(bGyroThreadRunning == TRUE)
   {

      if( TRUE == bGyroEn )
      {
         /*If Data Is Not There In Intermediate Buffer It Will Never Return
           And RingBuffer Will Be Updated By Error Counter*/
         tVpop(&trGyro3dData);
			   
         /* We received data. So reset the timer */
         if(OSAL_OK != OSAL_s32TimerSetTime(hTimerGyroSampleTrigger,GYRO_DATA_TIMEOUT_VALUE,GYRO_DATA_TIMEOUT_VALUE))
         {
            vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMainThread:Timer Set Failed"); 
         }

         vLSIM_GYROTrace(TR_LEVEL_USER_4,"vGyroMainThread:Time Satmp=%d",trGyro3dData.u32TimeStamp);
         vLSIM_GYROTrace(TR_LEVEL_USER_4,"vGyroMainThread:r-Value=%d",trGyro3dData.u32RaxisVal);
         vLSIM_GYROTrace(TR_LEVEL_USER_4,"vGyroMainThread:s-Value=%d",trGyro3dData.u32SaxisVal);
         vLSIM_GYROTrace(TR_LEVEL_USER_4,"vGyroMainThread:t-Value=%d",trGyro3dData.u32TaxisVal);	   
			   
         u16Errorlevel=0;

         if(sizeof(rAuxClkTime) == AUXCLOCK_s32IORead(&rAuxClkTime, sizeof(rAuxClkTime)))
         {
           if(trGyro3dData.u32TimeStamp > 0)
           {
		        /*trGyro3dData.u32TimeStamp::This Is The Wait Time i.e The Difference Between
                  Two Successive Records.This Difference Will Be Caliculated By Parser And Will Be 
				  Send As Time Stamp To The Server*/
                u32TimeStamp=(tU32)trGyro3dData.u32TimeStamp+rAuxClkTime.u32Low;
				vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMainThread:Wait Time=%d",trGyro3dData.u32TimeStamp);
                OSAL_s32ThreadWait((OSAL_tMSecond)trGyro3dData.u32TimeStamp);
                vUpdateGyroRingBuffer(u16Errorlevel,u32TimeStamp,trGyro3dData);										 
            }
            else
            {
               vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMainThread:TripFile TimeStamp Is Not Valid");
            }
        }
        else
        {
            vLSIM_GYROTrace(TR_LEVEL_ERROR,"vGyroMainThread:AUX Clock Read Failed");
        }
	 }    
   }
   OSAL_vThreadExit();
}

/*******************************************************************************************/
/*!
*\fn   		static tVoid vUpdateGyroRingBuffer (tU32 u32TimeStamp) 
*
*\brief  	Updates Gyro Ring Buffer By Reading Gyro Data From Gyro Message Queue    
				  
*\param 	 None
*
*\return	None
*
*********************************************************************************************/

tVoid vUpdateGyroRingBuffer(tU16 u16ErrorCount,tU32 u32TimeStamp,trGyroData s32Gyro3dData)
{
   tU16  u16Errorlevel = 0;
   tBool bGyroDataValid = TRUE;
   tS32  s32TempIndex = 0;
   tBool bError_Occurred = FALSE;
   tS32 tS32ReadBytes;

   if(OSAL_OK == OSAL_s32SemaphoreWait( hGyroRingBufSemHandle,OSAL_C_TIMEOUT_FOREVER ))
   {
      if( TRUE == bGyroEn )
      {
         if(rGyroBuf.u16Valid_Entries == NO_OF_GYRO_RINGBUFFER_ENTRIES)
         {//No Space In Ring Buffer
            s32TempIndex = (rGyroBuf.u16Read_ID - 1);
            if( s32TempIndex < 0 )
            {
               s32TempIndex = (NO_OF_GYRO_RINGBUFFER_ENTRIES - 1);
            }
            u16Errorlevel = (rGyroBuf.Data[s32TempIndex].u16ErrorCounter + 1);
            s32TempIndex = rGyroBuf.u16Read_ID;                               

            ++(rGyroBuf.u16Read_ID);
            if( rGyroBuf.u16Read_ID == NO_OF_GYRO_RINGBUFFER_ENTRIES )       
            {
               rGyroBuf.u16Read_ID = 0;                                           
            }
         }
         else                                                                  
         {
            s32TempIndex = rGyroBuf.u16Read_ID + rGyroBuf.u16Valid_Entries;  
            if( s32TempIndex >= NO_OF_GYRO_RINGBUFFER_ENTRIES )
            {
               s32TempIndex -= NO_OF_GYRO_RINGBUFFER_ENTRIES;                     
            }
            ++(rGyroBuf.u16Valid_Entries);                                   
         }

         rGyroBuf.Data[s32TempIndex].u16Gyro_r       = s32Gyro3dData.u32RaxisVal;
         rGyroBuf.Data[s32TempIndex].u16Gyro_s       = s32Gyro3dData.u32SaxisVal;
         rGyroBuf.Data[s32TempIndex].u16Gyro_t       = s32Gyro3dData.u32TaxisVal;
         rGyroBuf.Data[s32TempIndex].u16ErrorCounter = u16ErrorCount;
         rGyroBuf.Data[s32TempIndex].u32TimeStamp    = u32TimeStamp;

         vLSIM_GYROTrace(TR_LEVEL_USER_4,"vUpdateGyroRingBuffer:R-Axis=%d  S-Axis=%d  T-Axis=%d  TimeStamp=%d  ErrorCounter=%d",
                       rGyroBuf.Data[s32TempIndex].u16Gyro_r,rGyroBuf.Data[s32TempIndex].u16Gyro_s,rGyroBuf.Data[s32TempIndex].u16Gyro_t,
                       rGyroBuf.Data[s32TempIndex].u32TimeStamp,rGyroBuf.Data[s32TempIndex].u16ErrorCounter);

         if(OSAL_OK != OSAL_s32SemaphoreWait( hGyroDiagRawBufSemHandle,OSAL_C_TIMEOUT_FOREVER ))
         {
            vLSIM_GYROTrace(TR_LEVEL_ERROR,"vUpdateGyroRingBuffer:Oops!!Locking Mutex On Diag Buffer Failed");
         }
         else 
         {
            rDiagGyroRawData.u32Gyro_r =(tU32)(s32Gyro3dData.u32RaxisVal &GYRO_RAW_VALUE_MASK);
            rDiagGyroRawData.u32Gyro_s = (tU32)(s32Gyro3dData.u32SaxisVal & GYRO_RAW_VALUE_MASK);
            rDiagGyroRawData.u32Gyro_t = (tU32)(s32Gyro3dData.u32TaxisVal & GYRO_RAW_VALUE_MASK);
            if(OSAL_OK != OSAL_s32SemaphorePost( hGyroDiagRawBufSemHandle ))
            {
               vLSIM_GYROTrace(TR_LEVEL_ERROR,"vUpdateGyroRingBuffer:Oops!!Unlocking Mutex On Diag Buffer Failed");
            }
         }
      }

      if(OSAL_OK != OSAL_s32SemaphorePost( hGyroRingBufSemHandle ))
      {
         vLSIM_GYROTrace(TR_LEVEL_ERROR,"vUpdateGyroRingBuffer:Oops!!Unlocking Mutex Failed");
      }
   }
   else
   {
      vLSIM_GYROTrace(TR_LEVEL_ERROR,"vUpdateGyroRingBuffer:Oops!!Locking Semaphore Failed");
      //printf("vUpdateGyroRingBuffer:Oops!!Locking Semaphore Failed\n");
   }
}

/*******************************************************************************************/
/*!
*\fn   		tS32 GYRO_IOOpen(tVoid) 
*
*\brief  	Application Interface To Open Gyro Device
				  
*\param 	 None 
*
*
*\return	Success : OSAL_E_NOERROR    Fail    : OSAL_ERROR  
*
*********************************************************************************************/
tS32 GYRO_IOOpen(tVoid)
{
   tS32 s32Retval = OSAL_E_NOERROR;
    
   if(!(u8Gyro_Flags & DEV_GYRO_CREATE))
   {
      s32Retval = OSAL_ERROR;
   }
   
   if(bGyroOpen==FALSE)
   {
      s32Retval=OSAL_E_NOERROR;
	  bGyroOpen=TRUE;
   }
   else
   {
      s32Retval=OSAL_ERROR;
   }
   return s32Retval;
}

/*********************************************************************************************/
/*!
*\fn   		tS32 GYRO_s32IORead(tPS8 ps8Buffer, tU32 u32maxbytes)   
*
*\brief  	Application Interface To Read Gyro Data From Ring Buffer.This Function Reads
            Oldest Valid Entry In The Ring Buffer.
            If No Sample Is Detedted In The Ring Buffer Even After One Second Zero Byte	
            Read Will Be Returned.			
*\param 	ps8Buffer   :Pointer To Buffer Where Gyro Data To Be Written.
			u32maxbytes :Max Number Bytes Of Data TO Be Read. 
*
*\return	Success : No Of Bytes Read.
*           Fail    : OSAL_E_UNKNOWN  or OSAL_E_TIMEOUT  or OSAL_ERROR
************************************************************************************************/

tS32 GYRO_s32IORead(tPS8 ps8Buffer, tU32 u32maxbytes)
{
   tU32  u32NumberOfEntries = u32maxbytes/sizeof(OSAL_trIOCtrl3dGyroData);
   tU32  u32ParameterBufferIndex = 0;
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU16  u16RingBufferRead_ID = 0;
   tU32  u32ReadErrCount = 0;
   tU32  u32TimeoutValueInSeconds = 0;
   tBool bError_occurred = FALSE;
   OSAL_trIOCtrl3dGyroData *pGyroData = (OSAL_trIOCtrl3dGyroData *)((tU32)ps8Buffer);

   if(ps8Buffer == OSAL_NULL)
   {
      s32RetVal = OSAL_ERROR;
      vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32IORead:Read Failed Since Application Passing NULL Buffer");
   }
   else
   {
      /* check if RD-Buffer is large enough */
      if( u32maxbytes != u32NumberOfEntries * sizeof(OSAL_trIOCtrl3dGyroData))
      {
         s32RetVal = OSAL_E_UNKNOWN;
         vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32IORead:Read Failed Due To Not Enough In The Buffer");
      }
      else
      {
         /* 1.Set Timeout Value i.e How Much Time Need To Wait If There Is No Data In The Ring Buffer.
         2.Check Whether Ring Buffer Has Data Or Not.
         3.If Ring Buffer Has No Data Then Check It After Every Sample Clock Until Given TimeOut
           Value Is Elapsed.
         4.If There Is No Data In Ring Buffer Even After TimeOut Value,Just Return ERROR To Application.  
         */
         u32TimeoutValueInSeconds = (tU32) s32ActIOReadTimeoutValueInSec;

         while( u32NumberOfEntries > 0 )
         {
            u32ReadErrCount = 0;

            while( rGyroBuf.u16Valid_Entries < 1 )
            {
               (tVoid)OSAL_s32ThreadWait( GYRO_TIMER_IN_MS ); /* sleep one sample clock */
               u32ReadErrCount++;
               if(u32ReadErrCount > (GYRO_CONVERT_NS_2_HZ(GYRO_DATA_SAMPLING_RATE_IN_NS)*u32TimeoutValueInSeconds))
               {   /*Waited Till TimeOut.Now Exit And Report Error*/
                  bError_occurred = TRUE;
                  vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32IORead:Read Failed Due No Data In The Buffer");
                  s32RetVal = OSAL_E_TIMEOUT;
                  break;
               }
            }

            if(!bError_occurred)
            {
               u32NumberOfEntries--;

               /* Get Semaphore To Protect Gyro Ringbuffer From Syncronous Write/Read */
               if(OSAL_OK != OSAL_s32SemaphoreWait( hGyroRingBufSemHandle,OSAL_C_TIMEOUT_FOREVER ))
               {
                  bError_occurred = TRUE;
                  s32RetVal = OSAL_E_UNKNOWN;
                  vLSIM_GYROTrace(TR_LEVEL_FATAL,"GYRO_s32IORead:Semaphore Locking Failed");
               }
               if(bError_occurred == FALSE)
               {
                  /*Copy Data Into User Passed Buffer*/
                  u16RingBufferRead_ID = rGyroBuf.u16Read_ID;
                  pGyroData[u32ParameterBufferIndex].u16Gyro_r =rGyroBuf.Data[u16RingBufferRead_ID].u16Gyro_r;
                  pGyroData[u32ParameterBufferIndex].u16Gyro_s =rGyroBuf.Data[u16RingBufferRead_ID].u16Gyro_s;
                  pGyroData[u32ParameterBufferIndex].u16Gyro_t =rGyroBuf.Data[u16RingBufferRead_ID].u16Gyro_t;
                  pGyroData[u32ParameterBufferIndex].u16ErrorCounter =rGyroBuf.Data[u16RingBufferRead_ID].u16ErrorCounter;
                  pGyroData[u32ParameterBufferIndex].u32TimeStamp  = rGyroBuf.Data[u16RingBufferRead_ID].u32TimeStamp;

                  vLSIM_GYROTrace(TR_LEVEL_USER_4,"GYRO_s32IORead:R-Axis = %d  S-Axis =%d T-Axis = %d TimeStamp=%d ErrCount=%d",
                                     pGyroData[u32ParameterBufferIndex].u16Gyro_r,pGyroData[u32ParameterBufferIndex].u16Gyro_s,
                                     pGyroData[u32ParameterBufferIndex].u16Gyro_t,pGyroData[u32ParameterBufferIndex].u32TimeStamp,
                                     pGyroData[u32ParameterBufferIndex].u16ErrorCounter);

                  u32ParameterBufferIndex++;

                  rGyroBuf.u16Valid_Entries -= 1; /* decrement no. of valid entries*/
                  rGyroBuf.u16Read_ID += 1;       /* the new oldest ID is next one*/

                  if( rGyroBuf.u16Read_ID >= NO_OF_GYRO_RINGBUFFER_ENTRIES )
                  {
                     rGyroBuf.u16Read_ID = 0; /* adjust in case of wraparound */
                  }

                  /* release mutex to protect gyro ringbuffer from syncronous write/read */
                  if(OSAL_OK != OSAL_s32SemaphorePost( hGyroRingBufSemHandle ))
                  {
                     /*nothing meaningfull could be done in this case*/
                      vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32IORead:Thread Creation Failed");
                  }
               }
            }
            else
            {
               //printf("ABOUT TO BREAK\n");
               break;
            }
         }
      }
   }
   if(s32RetVal == (tS32)OSAL_E_NOERROR)
   {
      s32RetVal = (tS32)u32maxbytes -(tS32)(u32NumberOfEntries*sizeof(OSAL_trIOCtrl3dGyroData));
   }
   else
   {
      vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32IORead:Oops!Read Failed Due No Data In The Buffer");
   }

   return(s32RetVal);
}

/************************************************************************/
/*!
*\fn   		tS32 GYRO_s32IOClose(tVoid)
*
*\brief  	Application Interface To Close The Gyro Driver
 
*\param 	tVoid
*
*
*\return	OSAL_E_NOERROR:If Already not Closed
*           OSAL_ERROR:If Trying To Close Without Open
*
**************************************************************************/

tS32 GYRO_s32IOClose(tVoid)
{
   tS32 s32Retval = OSAL_E_NOERROR;
   if(bGyroOpen==TRUE)
   {
      s32Retval=OSAL_E_NOERROR;
      bGyroOpen=FALSE;
   }
   else
   {
      s32Retval=OSAL_ERROR;
   }
   return(s32Retval);
}

/*******************************************************************************************/
/*!
*\fn   		tS32 GYRO_s32IOControl(tS32 s32fun, tS32 s32arg)
*
*\brief  	Application Interface To Control Gyro Driver.
				  
*\param 	s32fun :Function identifier.
			s32arg :Argument to be passed to function. 
*
*
*\return	Success : OSAL_E_NOERROR   Fail    : OSAL_ERROR  
*
*********************************************************************************************/

tS32 GYRO_s32IOControl(tS32 s32fun, tS32 s32arg)
{
   tS32  s32RetVal = OSAL_E_NOERROR;
   tPS32 ps32Argument =  (tPS32) s32arg;


   switch( s32fun )
   {
      case OSAL_C_S32_IOCTRL_VERSION:
      {
         if( ps32Argument != OSAL_NULL )
         {
            *ps32Argument = (tS32)DEV_GYRO_DRIVER_VERSION;
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
        break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_FLUSH:
      {
         if(OSAL_OK == OSAL_s32SemaphoreWait( hGyroRingBufSemHandle,OSAL_C_TIMEOUT_FOREVER ))
         {
            /*Entries Are Not Deleted But Declared Invalid And Will Be Overridden */
            rGyroBuf.u16Valid_Entries = 0;
            if(OSAL_OK != OSAL_s32SemaphorePost( hGyroRingBufSemHandle ))
            {
               vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32IOControl:Oops!!Unlocking Semaphore Failed");
            }
         }
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_GETCNT:
      {
         /*Caution::Working On Global Data::Take Lock*/
         if(OSAL_OK == OSAL_s32SemaphoreWait( hGyroRingBufSemHandle,OSAL_C_TIMEOUT_FOREVER ))
         {
            if(ps32Argument != OSAL_NULL)
            {
               *ps32Argument = (tS32) rGyroBuf.u16Valid_Entries;
            }
            else
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
            if(OSAL_OK != OSAL_s32SemaphorePost( hGyroRingBufSemHandle ))
            {
               vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32IOControl:Oops!!Unlocking Semaphore On RingBuffer Failed");
            }
         }
         else
         {
            s32RetVal=OSAL_ERROR;
         }
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_SET_TIMEOUT_VALUE:
      {
         if((ps32Argument != OSAL_NULL)&&(*ps32Argument >= 0 )&&
                   /*see multiplication in GYRO_s32IORead*/
                   ((*ps32Argument) < ((LIMIT_C_MAX_S32 /
                   (GYRO_CONVERT_NS_2_HZ(GYRO_DATA_SAMPLING_RATE_IN_NS)))-1)))
         {
            s32ActIOReadTimeoutValueInSec = *ps32Argument;
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_GET_TIMEOUT_VALUE:
      {
         if( ps32Argument != OSAL_NULL )
         {
            *ps32Argument = s32ActIOReadTimeoutValueInSec;
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_GETDIAGMODE:
      {
         s32RetVal = OSAL_E_NOTSUPPORTED;
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_SETDIAGMODE:
      {
          s32RetVal = OSAL_E_NOTSUPPORTED;
          break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_RESET:
      {
         s32RetVal = OSAL_E_NOTSUPPORTED;
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_INIT:
      {
         s32RetVal = OSAL_E_NOTSUPPORTED;
         break;
      }
	  
      case OSAL_C_S32_IOCTRL_GYRO_START:
      {
         bGyroEn = TRUE;
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_STOP:
      {
         bGyroEn = FALSE;
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_GET_TYPE:
      {
         s32RetVal = OSAL_E_NOTSUPPORTED;
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_GETRESOLUTION:
      {
         if( ps32Argument != OSAL_NULL )
         {
            *ps32Argument=(tS32)(GYRO_CONVERT_NS_2_HZ(GYRO_DATA_SAMPLING_RATE_IN_NS));
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_GETCYCLETIME:
      {
         if( ps32Argument != OSAL_NULL )
         {
            *ps32Argument = (tS32)(GYRO_DATA_SAMPLING_RATE_IN_NS);
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_GET_3D_DIAG_RAW_DATA:
      {
         OSAL_trIOCtrlDiag3dGyroRawData* pDiagData =(OSAL_trIOCtrlDiag3dGyroRawData*)s32arg;

         if(ps32Argument != OSAL_NULL)
         {
            /*Caution::Accessing Global Data::Take Lock On DiagBuffer*/
            if(OSAL_OK != OSAL_s32SemaphoreWait( hGyroDiagRawBufSemHandle,OSAL_C_TIMEOUT_FOREVER ))
            {
               s32RetVal = OSAL_E_NOACCESS;
               break;
            }

            pDiagData->u32Gyro_r = rDiagGyroRawData.u32Gyro_r;
            pDiagData->u32Gyro_s = rDiagGyroRawData.u32Gyro_s;
            pDiagData->u32Gyro_t = rDiagGyroRawData.u32Gyro_t;
            /*Our Work Is Over Let Allow Others To Access Buffer By Unlocking Semaphore*/
            if(OSAL_OK != OSAL_s32SemaphorePost( hGyroDiagRawBufSemHandle ))
            {
               vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32IOControl:Oops!!Unlocking Semaphore On DiagBuffer Failed");
            }
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTRL_GYRO_GET_TEMP:
      {
         s32RetVal = OSAL_E_NOTSUPPORTED;
         break;
      }

      default:
      {
         vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32IOControl:Oops!!This ");
         s32RetVal = OSAL_E_WRONGFUNC;
         break;
      }
   }

   return(s32RetVal);
}


/*******************************************************************************************/
/*!
*\fn   		tS32 GYRO_s32DestroyGyroDevice(tVoid)
*
*\brief  	Release All The Resources Allocated To The Gyro Driver.   
				  
*\param 	 None
*
*\return	Success : OSAL_OK   Fail    : OSAL_ERROR
*
*********************************************************************************************/
tS32 GYRO_s32DestroyGyroDevice(tVoid)
{
   tS32 s32RetVal = OSAL_OK;

   /*Stop Data Acquisition*/
   bGyroEn = FALSE;

   /* attention on moving this code part, add code to correctly handle s32RetVal*/

   /*Stop Gyro Main Thread*/
   bGyroThreadRunning = FALSE;

   if(OSAL_NULL != pGyroMsgQueHandle)
   {
      (tVoid)OSAL_s32MessageQueueClose(pGyroMsgQueHandle);
      if(OSAL_OK != OSAL_s32MessageQueueDelete(GYRO_C_STRING_MSGQ_NAME))
      {
         vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32DestroyGyroDevice:MsgQ Delete Failed");
      }
   }

   if( OSAL_OK != OSAL_s32EventPost(hGyroEventDataReadTimeout,GYRO_C_EV_GYRO_SHUTDOWN,OSAL_EN_EVENTMASK_OR ))
   {
      s32RetVal = OSAL_ERROR;
   }

   (tVoid)OSAL_s32TimerSetTime(hTimerGyroSampleTrigger,0,0);
   if(hTimerGyroSampleTrigger != OSAL_NULL)
   {
      if ( OSAL_s32TimerDelete (hTimerGyroSampleTrigger) != OSAL_OK)
      {
         vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32DestroyGyroDevice:Oops!!Deleting Timer Failed");
         s32RetVal = OSAL_ERROR;
      }
   }

   (tVoid)OSAL_s32SemaphoreClose(hGyroRingBufSemHandle);
   if(OSAL_OK != OSAL_s32SemaphoreDelete(GYRO_C_STRING_RING_BUF_SEMPHORE_NAME))
   {
      vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32DestroyGyroDevice:Deleting RingBuf Semaphore Failed");
      s32RetVal = OSAL_ERROR;
   }

   (tVoid)OSAL_s32SemaphoreClose(hGyroDiagRawBufSemHandle);
   if(OSAL_OK != OSAL_s32SemaphoreDelete(GYRO_C_STRING_DIAGSEMPHORE_NAME))
   {
      vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32DestroyGyroDevice:Deleting DiagBuf Semaphore Failed");
      s32RetVal = OSAL_ERROR;
   }

   if(hGyroEventDataReadTimeout != OSAL_C_INVALID_HANDLE)
   {
      (tVoid)OSAL_s32EventClose(hGyroEventDataReadTimeout);
      if(OSAL_s32EventDelete((tString)GYRO_C_STRING_EVENT_NAME) != OSAL_OK)
      {
         vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32DestroyGyroDevice:Oops!!Deleting Event Failed");
         s32RetVal = OSAL_ERROR;
      }
   }

   if(s32GyroMainThreadID != OSAL_ERROR)
   {
      if ( OSAL_s32ThreadDelete(s32GyroMainThreadID) != OSAL_OK)
      {
         vLSIM_GYROTrace(TR_LEVEL_ERROR,"GYRO_s32DestroyGyroDevice:Oops!!Deleting Thread Failed");
         s32RetVal = OSAL_ERROR;
      }
   }

   u8Gyro_Flags &= DEV_GYRO_FLAG_CLEAR;
   return s32RetVal;
}


/********************************************************************************************/
/*!
*\fn   		tVoid vLSIM_GYROTrace(  tU32 u32TraceLevel,const tChar *pcFormatString,... )
*
*\brief  	Trace Out Function Of LSIM Gyro.
				  
*\param 	u32Level - Trace Level
			pcFormatString - Trace String
*
*\return	tU32 Error Codes
*
**********************************************************************************************/

tVoid vLSIM_GYROTrace(  tU32 u32TraceLevel,const tChar *pcFormatString,... )
{

   if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_GYRO, u32TraceLevel) != FALSE)
   {
      va_list ArgList = {0};
      tS32 s32Size =0;
      tChar cBuffer[MAX_TRACE_SIZE];

      (tVoid)OSAL_pvMemorySet( cBuffer,'\0',MAX_TRACE_SIZE );

      va_start( ArgList, pcFormatString );

      s32Size = vsnprintf( cBuffer,MAX_TRACE_SIZE-1,pcFormatString,ArgList );
      if(s32Size>0)
      {
         LLD_vTrace( OSAL_C_TR_CLASS_DEV_GYRO,u32TraceLevel,cBuffer,(tU32)s32Size);
      }
      va_end(ArgList);
   }
}
