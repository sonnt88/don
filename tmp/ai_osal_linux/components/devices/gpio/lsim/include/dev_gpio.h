/******************************************************************************
* FILE                         : dev_gpio.h
*
* SW-COMPONENT      : 
*
* DESCRIPTION          : Header file for LSIM gpio driver.
*                                      
* AUTHOR(s)              : Dharmender Suresh Chander (RBEI/ECF5)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date  17.jul.2014        |       Version        | Author & comments
*-------- --|---------------|-------------------------------------------------
*                  |  Initialversion 1.0        | Dharmender Suresh Chander (RBEI/ECF5)
* -----------------------------------------------------------------------------
*******************************************************************************/

#ifndef DEV_GPIO_H
#define DEV_GPIO_H




/************************************************************************
|Data type definitions  (scope: local)
|-----------------------------------------------------------------------*/






/************************************************************************
|function prototypes (scope: local)
|-----------------------------------------------------------------------*/
tS32 DEV_GPIO_s32IODeviceInit();
tS32 DEV_GPIO_s32IODeviceDeinit();

tS32 GPIO_IOOpen();
tS32 GPIO_s32IOClose();
tS32 GPIO_s32IOControl(tS32 s32Fun, tS32 s32Arg);
/*tVoid DRV_LSIM_GPIO_vSetGpio(tU32 u32Port, tU32 u32Pin, tU32 u32State);
tU32 DRV_LSIM_GPIO_u32GetGpioState(tU32 u32Port, tU32 u32Pin, tPBool pbGpioState);*/

#endif

