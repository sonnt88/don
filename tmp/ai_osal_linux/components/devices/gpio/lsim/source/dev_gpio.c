 /*****************************************************************************
 
 * FILE 	: dev_gpio.c
 
 * SW-COMPONENT  : Linux system components
 
 * DESCRIPTION	 : This implementation provides the Gpio driver support for LSIM.
 
 * AUTHOR(s)     : Dharmender Suresh Chander (RBEI/ECF5)
 
 * HISTORY		 :
 
 *-----------------------------------------------------------------------------l
 
 * Date 		  | 	  Version		 | Author & comments
 
 *-------- --|---------------|-------------------------------------------------
 
 * 17.jul.2015	  |  Initialversion 1.0 | Dharmender Suresh Chander (RBEI/ECF5)
 
 * 11.Sep 2015 | Version 1.1 | Rahana V S (RBEI/ECF5)
					Minimal implementation of GPIO driver in LSIM
					by hardcoding the initial values of GPIO to a text file.
 
 * ----------------------------------------------------------------------------
 
 *******************************************************************************/
 
#ifdef __cplusplus
extern "C" {
#endif
 
/************************************************************************
 | Header file declaration
|-----------------------------------------------------------------------*/
 
#define OSAL_S_IMPORT_INTERFACE_GENERIC
 
#include "OsalConf.h"
#include "osal_if.h"
#include "Linux_osal.h"
#include "dev_gpio.h"
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include "ostrace.h"
#include "osioctrl.h"
 
static OSAL_tSemHandle SemHnd = OSAL_C_INVALID_HANDLE;

typedef enum 
{
  GPIO_TRC_FN_INFO = 0,
  GPIO_TRC_FN_STRING
} enGPIOTraceMessages;

typedef enum
{
   GPIO_DIRECTION_IN = 0,
   GPIO_DIRECTION_OUT,
} enDevGpioDirection;

#define DEFAULT_DTS                               "/opt/bosch/base/lsim/dts/default_dts.txt"
#define DTS_TMP_FILE                              "/tmp/dts_tmp.txt"
#define LINE_SIZE                                 100
#define DTS_TMP_SIZE                              1000
 
#define LSIM_GPIO_SEMAPHORE_NAME                  "GPIO_LSIM_SEM"                        
#define LSIM_GPIO_SEMAPHORE_COUNT                  1                                      
 
#define DRV_LSIM_GPIO_VERSION                      ((tS32) 0x00000001)
#define STR_GPIO_DIRECTION_IN                      "GPIO_DIR= input"
#define STR_GPIO_DIRECTION_OUT                     "GPIO_DIR=output"

 typedef tU32 tDrvGpioDevID;
 tChar GPIO_LOGICAL_ID[LINE_SIZE] = "GPIO_LOGICAL_ID=";
 
/* IOControl functions */
static tU32 drv_LSIM_GPIO_vInitGpio();
static tS32 drv_LSIM_GPIO_GetVersion(tVoid);//To satisfy lint
static tU32 drv_LSIM_GPIO_SetInput(const OSAL_tGPIODevID *pID);
static tU32 drv_LSIM_GPIO_SetOutput(const OSAL_tGPIODevID *pID);
 
/* DRV GPIO internal functions */
/*static tVoid drv_GPIO_CheckLogicalIDs(tVoid);*/
static tU32 drv_LSIM_GPIO_SetDirection(const OSAL_tGPIODevID  *pID,
										   const enDevGpioDirection  direction);
static tU32 drv_LSIM_GPIO_GetDrvGpioDevID(const OSAL_tGPIODevID pID);
static tU32 drv_LSIM_GPIO_TextFileWriteStr(const OSAL_tGPIODevID  iID,
										 const tPChar pStr);
static tVoid drv_LSIM_GPIO_vTraceOut(tU32 u32Level,
										  const tChar *pcFormatString,...);
 
 
/*****************************************************************************
 
 * FUNCTION 	: DEV_GPIO_s32IODeviceInit
 
 * RETURNVALUE	: OSAL error code
 
 * DESCRIPTION	: This function creates Gpio device in LSIM .
 
 * HISTORY		: 11.Sep.2015| Initial Version	 | Rahana V S (RBEI/ECF5)
 
*****************************************************************************/
 tS32 DEV_GPIO_s32IODeviceInit()
 {
    /*drv_GPIO_CheckLogicalIDs();*/
	/*Initialize GPIO */
	 if (OSAL_E_NOERROR != drv_LSIM_GPIO_vInitGpio())
	{
       return OSAL_E_NOTINITIALIZED;
	}
	/*Create semaphore for GPIO*/
	if (OSAL_ERROR == OSAL_s32SemaphoreCreate(LSIM_GPIO_SEMAPHORE_NAME,
											 &SemHnd, 1))
	{
	   /*NORMAL_M_ASSERT_ALWAYS();*/
	   return (tS32)OSAL_u32ErrorCode();
	}
	return OSAL_E_NOERROR;
 }
 
 /*****************************************************************************
 
 * FUNCTION 	: DEV_GPIO_s32IODeviceDeinit
 
 * RETURNVALUE	: OSAL error code
 
 * DESCRIPTION	: This function creates Gpio device in LSIM .
 
 * HISTORY		: 11.Sep.2015| Initial Version  | Rahana V S (RBEI/ECF5)
 
 *****************************************************************************/
 tS32 DEV_GPIO_s32IODeviceDeinit()
 {
	/*Close the semaphore*/
	if (OSAL_ERROR == OSAL_s32SemaphoreClose(SemHnd))
	{
	   /*NORMAL_M_ASSERT_ALWAYS();*/
	   OSAL_s32SemaphoreDelete(LSIM_GPIO_SEMAPHORE_NAME);
	   return (tS32)OSAL_u32ErrorCode();
	}
	/*Delete semaphore*/
	if (OSAL_ERROR == OSAL_s32SemaphoreDelete(LSIM_GPIO_SEMAPHORE_NAME))
	{
	   /*NORMAL_M_ASSERT_ALWAYS();*/
	   return (tS32)OSAL_u32ErrorCode();
	}
 
	SemHnd = OSAL_C_INVALID_HANDLE;
	fcloseall();
	return OSAL_E_NOERROR;
 }
 
 /*****************************************************************************
 
 * FUNCTION 	:GPIO_IOOpen
 
 * RETURNVALUE	: OSAL error code
 
 * DESCRIPTION	: This function opens Gpio device in LSIM
 
 * HISTORY		: 11.Sep.2015| Initial Version  | Rahana V S (RBEI/ECF5)
 
 *****************************************************************************/
 tS32 GPIO_IOOpen()
 {
	tU32 retVal = OSAL_E_NOERROR;
	drv_LSIM_GPIO_vTraceOut(TR_LEVEL_USER_4,"Open GPIO");
	return retVal;
 }
 
 /*****************************************************************************
 
 * FUNCTION 	:GPIO_IOClose
 
 * RETURNVALUE	: OSAL error code
 
 * DESCRIPTION	: Close Gpio device in LSIM
 
 * HISTORY		: 11.Sep.2015| Initial Version	| Rahana V S (RBEI/ECF5)
 
 *****************************************************************************/
 tS32 GPIO_s32IOClose()
 {
	tU32 retVal = OSAL_E_NOERROR;
	drv_LSIM_GPIO_vTraceOut(TR_LEVEL_USER_4,"Close GPIO");
	return retVal;
 }
 
 /******************************************************************************
 
 * FUNCTION 	:GPIO_s32IOControl
 
 * PARAMETER	: s32Fun,s32Arg
 
 * RETURNVALUE	: OSAL error code
 
 * DESCRIPTION	: Provides the IO control function for Gpio driver in LSIM
 
 * HISTORY		: 11.Sep.2015| Initial Version  | Rahana V S (RBEI/ECF5)
 
 ******************************************************************************/
 tS32 GPIO_s32IOControl(tS32 s32Fun, tS32 s32Arg)
 {
	tU32 u32RetVal = OSAL_E_NOTSUPPORTED;
	drv_LSIM_GPIO_vTraceOut(TR_LEVEL_USER_4,"Enter");
 
	if(s32Arg)
	{
	   switch (s32Fun)
	   {
		  case OSAL_C_S32_IOCTRL_VERSION:
			 *((tS32*)s32Arg) = drv_LSIM_GPIO_GetVersion();
			 u32RetVal = OSAL_E_NOERROR;
			 break;
 
		  case OSAL_C_32_IOCTRL_GPIO_SET_INPUT:
			 u32RetVal = drv_LSIM_GPIO_SetInput((const OSAL_tGPIODevID *)s32Arg);
			 break;
 
		  case OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT:
			 u32RetVal = drv_LSIM_GPIO_SetOutput((const OSAL_tGPIODevID *)s32Arg);
			 break;
 
		  default:
			 drv_LSIM_GPIO_vTraceOut(TR_LEVEL_FATAL,"unknown IOCTRL");
			 break;
	   }
	}
	else
	{
	   drv_LSIM_GPIO_vTraceOut(TR_LEVEL_FATAL,"empty argument");
	   u32RetVal = OSAL_E_INVALIDVALUE;
	} 
	drv_LSIM_GPIO_vTraceOut(TR_LEVEL_USER_4,"Exit:");
	return (tS32)u32RetVal;
 }
 
 static tS32 drv_LSIM_GPIO_GetVersion(tVoid)//To satisfy lint
 {
	return DRV_LSIM_GPIO_VERSION;
 }
 
 /*****************************************************************************
 
 * FUNCTION 	:drv_LSIM_GPIO_vInitGpio
 
 * DESCRIPTION	: Initializes the pins for Gpio driver in LSIM
 
 * HISTORY		: 11.Sep.2015| Initial Version  | Rahana V S (RBEI/ECF5)
 
 *****************************************************************************/
 tU32 drv_LSIM_GPIO_vInitGpio()
 {  
	tChar command[LINE_SIZE] = {0};
	tChar file_string[LINE_SIZE] = {0};
 
	strcpy(file_string, DEFAULT_DTS);
	/* Copy the selected file to temporary file */
	sprintf(command,"cp %s %s",file_string,DTS_TMP_FILE);
	system(command);
	
	/* ensure that data are in sync */
	system("sync \n");
	OSAL_s32ThreadWait(2000);	
	return OSAL_E_NOERROR;
 }
 
 static tU32 drv_LSIM_GPIO_SetInput(const OSAL_tGPIODevID *pID)
 {
	return drv_LSIM_GPIO_SetDirection(pID, GPIO_DIRECTION_IN);
 }
 
 static tU32 drv_LSIM_GPIO_SetOutput(const OSAL_tGPIODevID *pID)
 {
	return drv_LSIM_GPIO_SetDirection(pID, GPIO_DIRECTION_OUT);
 }

 /*****************************************************************************
 
 * FUNCTION 	: drv_LSIM_GPIO_SetDirection
 
 * PARAMETER	: pID, direction
 
 * RETURNVALUE	: OSAL error code
 
 * DESCRIPTION	: Performs the IO control functionalities
 
 * HISTORY		: 11.Sep.2015| Initial Version  | Rahana V S (RBEI/ECF5)
 
 *****************************************************************************/
 static tU32 drv_LSIM_GPIO_SetDirection(const OSAL_tGPIODevID	 *pID,
								   const enDevGpioDirection  direction)
 {
	tU32 retVal = OSAL_E_NOERROR;
	tDrvGpioDevID iID = 0;
	
	/* Get the GPIO pin */
	iID = drv_LSIM_GPIO_GetDrvGpioDevID((OSAL_tGPIODevID)pID); 
	if (iID == 0)
	{
	   return OSAL_E_NOTSUPPORTED;
	}
 
	switch (direction)
	{
	/*Sets the corresponding pin to input mode*/
	case GPIO_DIRECTION_IN:
	   retVal = drv_LSIM_GPIO_TextFileWriteStr(iID,STR_GPIO_DIRECTION_IN);
	   drv_LSIM_GPIO_vTraceOut(TR_LEVEL_USER_4,"Set IN");
	   break;
 
	 /*Sets the corresponding pin to output mode*/
	case GPIO_DIRECTION_OUT:
	   retVal = drv_LSIM_GPIO_TextFileWriteStr(iID,
											STR_GPIO_DIRECTION_OUT);
	   drv_LSIM_GPIO_vTraceOut(TR_LEVEL_USER_4,"Set OUT");
		break;
 
	default:
	   drv_LSIM_GPIO_vTraceOut(TR_LEVEL_FATAL,"invalid direction");
	   /*NORMAL_M_ASSERT_ALWAYS();*/
	  retVal = OSAL_E_INVALIDVALUE;
	  break;
	}
	return retVal;
 }
 
 /*****************************************************************************
 
 * FUNCTION 	  : drv_LSIM_GPIO_GetDrvGpioDevID
 
 * PARAMETER	  : pID
 
 * RETURNVALUE	  : pin ID
 
 * DESCRIPTION	  : Obtain pin ID of GPIO
 
 * HISTORY		  : 11.Sep.2015| Initial Version  | Rahana V S (RBEI/ECF5)
 
 *****************************************************************************/
 static tU32 drv_LSIM_GPIO_GetDrvGpioDevID(const OSAL_tGPIODevID pID)
 {
	tDrvGpioDevID pDrvGpioID = 0; 

	/* map OSAL ID to DRV GPIO internal ID */
	if (pID < OSAL_GPIO_OFFSET)
	{
	   /* logical ID */
	   if (pID == 0 || pID >= OSAL_EN_GPIOPINS_LASTENTRY)
	   {
		  drv_LSIM_GPIO_vTraceOut(TR_LEVEL_FATAL,"invalid logic ID");
		  /*NORMAL_M_ASSERT_ALWAYS();*/
	   }
	   else
	   {
		  /* logical ID remains unchanged */
		  pDrvGpioID = pID;
	   }
	}
	return pDrvGpioID;
 }
 
 /*****************************************************************************
 
 * FUNCTION 	  : drv_LSIM_GPIO_TextFileWriteStr
 
 * PARAMETER	  : iID, pS, pStr
 
 * RETURNVALUE	  : OSAL error code
 
 * DESCRIPTION	  :Set the pin to input/output
 
 * HISTORY		  : 11.Sep.2015| Initial Version  | Rahana V S (RBEI/ECF5)
 
 *****************************************************************************/
 static tU32 drv_LSIM_GPIO_TextFileWriteStr(const OSAL_tGPIODevID  iID,
											   const tPChar pStr)
 {
    FILE *dts_tmp;
	tInt nFilepos , nCurrentline = 0 , match = 0;
	tChar gpio_logical_id[LINE_SIZE] = {0};
	tChar line[DTS_TMP_SIZE] = {0};

    dts_tmp = fopen(DTS_TMP_FILE,"r+");
 
	if(dts_tmp == NULL)
	{
	   drv_LSIM_GPIO_vTraceOut(TR_LEVEL_FATAL,"Error in opening the file");
	   return OSAL_E_UNKNOWN;
	}
	rewind(dts_tmp);
	sprintf(gpio_logical_id ,"%s%d" , GPIO_LOGICAL_ID,iID);
 
	/*Read the texfile line by line*/
	while(fgets(line , DTS_TMP_SIZE , dts_tmp) != NULL)
	{
	   nCurrentline++;
	   if((strstr(line ,gpio_logical_id)) != NULL)
	   {
		  match = 1;
		  drv_LSIM_GPIO_vTraceOut(TR_LEVEL_USER_4,"pID found");
		  break;
	   }
	}
 
	/*If logical ID found, set the pin to input/output mode */
	if(match == 1)
	{
	   nFilepos = ftell(dts_tmp);
       fseek(dts_tmp , nFilepos , SEEK_SET);
	   fputs(pStr , dts_tmp);
	}
	else
	{
	   drv_LSIM_GPIO_vTraceOut(TR_LEVEL_FATAL,"Logical ID not found");
	   /*NORMAL_M_ASSERT_ALWAYS();*/
	   return OSAL_E_UNKNOWN;
	} 
	fclose(dts_tmp);
	return OSAL_E_NOERROR;
 }

 /****************************************************************************
 /*!
  *\fn		 tVoid drv_GPIO_CheckLogicalIDs()
  *
  *\brief	 Make sure that the logical IDs defined in osioctrl.h
  * 		 match with the ones defined in this driver
  * 		 ! keep the LogicalGpioNames table in sync !
  *
  *\par History:
  * 09.10.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
  ***************************************************************************/
#if 0
static tVoid drv_GPIO_CheckLogicalIDs(tVoid)
{
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_SPI_ADR_SPI1_CS           == 1);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_ADR_REQ              == 2);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_ADR_RESET            == 3);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_HW_MUTE              == 4);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_MUTE_ENABLE      == 5);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_TUN_PWR_ANT1         == 6);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMPOP_ON             == 7);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_TUN_PWR_ANT1_PCB     == 8);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_SPI_HIT_SPI_CS            == 9);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_HIT_REQ              == 10);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_HIT_RESET            == 11);
   NORMAL_M_ASSERT(OSAL_EN_CDDRIVE_RESET_GPIO                      == 12);
   NORMAL_M_ASSERT(OSAL_EN_DISPLAY_SETTINGS                        == 13);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO                                == 14);
   NORMAL_M_ASSERT(OSAL_EN_BACKLIGHT_SETTINGS                      == 15);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_HIT_PWRSUPPLY        == 16);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AUX_IN_DIAG_ON_LEFT  == 17);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AUX_IN_DIAG_ON_RIGHT == 18);
   NORMAL_M_ASSERT(OSAL_EN_CAP_GPIO_REQ                            == 19);
   NORMAL_M_ASSERT(OSAL_EN_CAP_GPIO_RESET                          == 20);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_OFFSET_DETECT    == 21);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_FRONT_STANDBY    == 22);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_REAR_STANDBY     == 23);
   NORMAL_M_ASSERT(OSAL_EN_PHONE_MUTE_GPIO                         == 24);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AUD_AMP_MUTE         == 25);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AUD_AMP_ON           == 26);
   NORMAL_M_ASSERT(OSAL_EN_PWR_PHANTOM_MIC_ON                      == 27);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_TUN_PWR_ANT2         == 28);
   NORMAL_M_ASSERT(OSAL_EN_PWR_PHANTOM_XM_TUNER_ON                 == 29);
   NORMAL_M_ASSERT(OSAL_EN_SELECT_RC2                              == 30);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_FE_POWER_ON                        == 31);
   NORMAL_M_ASSERT(OSAL_EN_HC_GPIO_FAN_ON                          == 32);
   NORMAL_M_ASSERT(OSAL_EN_MIC_HW_POWER_CTRL                       == 33);
   NORMAL_M_ASSERT(OSAL_EN_MIC_SELECT_CTRL                         == 34);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_ANTENNA_ERROR_DETECT               == 35);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_ANTENNA_OPEN_DETECT                == 36);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_ANTENNA_SHORT_DETECT               == 37);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_ANTENNA_SHDN                       == 38);
   NORMAL_M_ASSERT(OSAL_EN_MIC_DIAG1_ENABLE                        == 39);
   NORMAL_M_ASSERT(OSAL_EN_MIC_DIAG2_ENABLE                        == 40);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_INPUT_LOCAL_1                 == 41);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_INPUT_LOCAL_2                 == 42);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_INPUT_REMOTE_1                == 43);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_INPUT_REMOTE_2                == 44);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1                == 45);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_2                == 46);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_3                == 47);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1               == 48);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_2               == 49);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_3               == 50);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO_WAKEUP_CAN                     == 51);
   NORMAL_M_ASSERT(OSAL_EN_CPU_RUN                                 == 52);
   NORMAL_M_ASSERT(OSAL_EN_PWR_UDROP_30                            == 53);
   NORMAL_M_ASSERT(OSAL_EN_SCC_RSTWARN_CPU                         == 54);
   NORMAL_M_ASSERT(OSAL_EN_CPU_PWR_OFF                             == 55);
   NORMAL_M_ASSERT(OSAL_EN_DAB_ANTENNA_ERROR_DETECT                == 56);
   NORMAL_M_ASSERT(OSAL_EN_FMAM_ANTENNA_ERROR_DETECT               == 57);
   NORMAL_M_ASSERT(OSAL_EN_MUTE_IN_ECALL                           == 58);
   NORMAL_M_ASSERT(OSAL_EN_MUTE_IN_VDA                             == 59);
   NORMAL_M_ASSERT(OSAL_EN_DAB_ANTENNA_SHDN                        == 60);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO_ON_TIPPER                      == 61);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO_WAKEUP_CD                      == 62);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO_CD_HW_EJECT                    == 63);
   NORMAL_M_ASSERT(OSAL_EN_PWR_UDROP_60                            == 64);
   NORMAL_M_ASSERT(OSAL_EN_PWR_UDROP_90                            == 65);
   NORMAL_M_ASSERT(OSAL_EN_MAX16946_SHDN                           == 66);
   NORMAL_M_ASSERT(OSAL_EN_MAX16946_SC                             == 67);
   NORMAL_M_ASSERT(OSAL_EN_MAX16946_OL                             == 68);
   NORMAL_M_ASSERT(OSAL_EN_TLF4277_EN                              == 69);
   NORMAL_M_ASSERT(OSAL_EN_TLF4277_ERROR                           == 70);
   NORMAL_M_ASSERT(OSAL_EN_ELMOS52240_EN_1                         == 71);
   NORMAL_M_ASSERT(OSAL_EN_ELMOS52240_NFLT_1                       == 72);
   NORMAL_M_ASSERT(OSAL_EN_ELMOS52240_EN_2                         == 73);
   NORMAL_M_ASSERT(OSAL_EN_ELMOS52240_NFLT_2                       == 74);
   NORMAL_M_ASSERT(OSAL_EN_REVERSE_DETECT                          == 75);
   NORMAL_M_ASSERT(OSAL_EN_JACK_DETECT                             == 76);
   NORMAL_M_ASSERT(OSAL_EN_PKB_DETECT                              == 77);
   NORMAL_M_ASSERT(OSAL_EN_MIC_DETECT                              == 78);
   NORMAL_M_ASSERT(OSAL_EN_CAMERA_DETECT                           == 79);
   NORMAL_M_ASSERT(OSAL_EN_PWR_RVC_SHDN                            == 80);
   NORMAL_M_ASSERT(OSAL_EN_CPU_RST_SCC                             == 81);
   NORMAL_M_ASSERT(OSAL_EN_CPU_RSTWARN_SCC                         == 82);
   NORMAL_M_ASSERT(OSAL_EN_TLF4277_EN_2                            == 83);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_TLF4277_ERROR_2                         == 84);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_DEBUG_WD_OFF                            == 85);
   NORMAL_M_ASSERT(OSAL_EN_MOST_DIAG_ECL_STATUS                    == 86);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_PWR_CDC                                 == 87);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_PWR_DISPLAY                             == 88);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_U140_SW_ENABLE	                      	 == 89);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_U140_SW_DIAG                            == 90);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_ACC_DETECT	                      	    == 91);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_HF_VR_MODE	                      	    == 92);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_MIC_CAM		                      	    == 93);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_TEL_MODE_AMP                            == 94);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_CPU_RST_BT                              == 95);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_CPU_RST_WL                              == 96);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_GPIOPINS_LASTENTRY                      == 97);	
}
#endif
 /******************************************************************************
 
 * FUNCTION   : Dev_GPIO_vTraceOut

* PARAMETER   : u32Level - trace level
																								
* RETURNVALUE : tU32 error codes

* DESCRIPTION : Trace out function for LSIM gpio.

* HISTORY     : 04.Jul.2012| Initial Version     |Sudharsanan Sivagnanam (RBEI/ECF5) 	
 ******************************************************************************/
 static tVoid drv_LSIM_GPIO_vTraceOut(	tU32 u32Level,
										  const tChar *pcFormatString,... )
 {
	if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_GPIO, u32Level) != FALSE)
	{
	   /* Parameter to hold the argument for a function,
				 specified the format string in pcFormatString
				 defined as char* va_list in stdarg.h */
	   va_list argList = {0};
 
	   /* vsnprintf Returns Number of bytes Written to
				 buffer or a negative value in case of failure */
	   tS32 s32Size ;
 
	   /* Buffer to hold the string to trace out */
	   tS8 u8Buffer[MAX_TRACE_SIZE];
 
	   /* position in buffer from where the format
				 string is to be concatenated */
	   tS8* ps8Buffer = (tS8*)&u8Buffer[0];
 
	   /* Flush the String */
	  (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint
 
	   /* Copy the String to indicate the trace is from the RTC device */
 
	   /* Initialize the argList pointer to the
				beginning of the variable arguement list */
	   va_start( argList, pcFormatString ); /*lint !e718 */
 
	   /* Collect the format String's content into the
				 remaining part of the Buffer */
	   if(0 >(s32Size = vsnprintf( (tString) ps8Buffer,sizeof(u8Buffer),
								  pcFormatString,argList)))
		{
		  return;
		}
 
	   /* Trace out the Message to TTFis */
	   LLD_vTrace( OSAL_C_TR_CLASS_DEV_GPIO,u32Level,u8Buffer,(tU32)s32Size );	 /* Send string to Trace*/
 
	   /*  Performs the appropiate actions to facilitate a normal
				  return by a function that has used the va_list object */
	   va_end(argList);
 
	}
 
 }
 
 
 
 
 
 
