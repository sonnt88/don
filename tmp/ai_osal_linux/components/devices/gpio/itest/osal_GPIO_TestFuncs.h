/******************************************************************************
 *FILE         : osal_GPIO_TestFuncs.h
 *
 *SW-COMPONENT : Regression Framework based on gtest
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file osal_GPIO_TestFuncs.c for the  
 *               GPIO device for the GM-GE hardware. This is a porting of the 
 *				 GPIO OEDT tests onto Google test
 *
 *AUTHOR       : Rohit C R(ECG4) 
 *
 *HISTORY      : 
*******************************************************************************/
#include <persigtest.h>
#include <ResultOutput/traceprintf.h>
#include "osal_if.h"

#include "osdevice.h"

/*
#include <stdio.h>
#include <stdlib.h>	
#include <sys/stat.h>
#include <fcntl.h>*/

extern "C" {
#ifndef TSIM_OSAL
#include <unistd.h>
#endif 
#include <string.h>
}

#ifndef OSAL_GPIO_TESTFUNCS_HEADER
#define OSAL_GPIO_TESTFUNCS_HEADER

/* Macro definition used in the code */
#define MQ_GPIO_MP_CONTROL_MAX_MSG     10
#define MQ_GPIO_MP_CONTROL_MAX_LEN     sizeof(tU32)
#define MQ_GPIO_MP_RESPONSE_NAME       "MQ_GPIO_RSPNS"

#endif
