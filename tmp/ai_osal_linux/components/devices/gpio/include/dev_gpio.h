/******************************************************************************
*****     (C) COPYRIGHT Robert Bosch GmbH CM-DI - All Rights Reserved     *****
******************************************************************************/
/*! 
***     @file      drv_gpio.h
***     @Authors
\n                  TMS-Dangers (mds)
\n                Fabian Zwissler, CM-DI/EAP (zw)
***
***     @brief       implementation of gpio in OSAL
***
***
***     @warning     
***
***     @todo        
***
*** --------------------------------------------------------------------------
\par   TECHNICAL INFORMATION:
***
\n     Hardware       : Dragon / ADIT-Platform; Tested with Dragon Working Sample 1.1
\n     Operatingsystem: T-Kernel
***
*** --------------------------------------------------------------------------
\par  VERSION HISTORY:
**  
*    $Log: 14.10.2005 - (mds) File created
*          08.03.2006 - (zw)  Adaptions to T-Kernel(Dragon Platform)
*          14.08.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
**
*\n*/
/*****************************************************************************/

#ifndef _DRV_GPIO_H
#define _DRV_GPIO_H

/*=============================================================================
=======                            INCLUDES                             =======
=============================================================================*/

/*=============================================================================
=======               DEFINES & MACROS FOR GENERAL PURPOSE              =======
=============================================================================*/

/* this switch shall be disabled by default */
//#define GPIO_VERBOSE_TRACE

/*
 * the total number of OSAL pin identifiers
 * consisting of logical IDs (enum OSAL_enGpioPins)
 * and hardware IDs (OSAL_GPIO_B<bank>_P<pin>)
 * see osioctrl.h
 */
#define GPIO_NOF_PINS_IDS    OSAL_EN_GPIOPINS_LASTENTRY \
                             + (OSAL_GPIO_LAST - OSAL_GPIO_OFFSET)

#define MAX_SYSFS_PATH_LEN   128

/*=============================================================================
=======                       CONSTANTS  &  TYPES                       =======
=============================================================================*/

/*
 * tDrvGpioDevID is the DRV GPIO internal device ID;
 * it is similar to the OSAL GPIO device ID (OSAL_tGPIODevID);
 * the logical IDs are in the same value range (1...0x0000FFFF)
 * but the hardware IDs (0x00010000...0xFFFFFFFF) are mapped
 * immediately after the last logical ID (OSAL_EN_GPIOPINS_LASTENTRY)
 */
typedef tU32 tDrvGpioDevID;

typedef enum 
{
  GPIO_TRC_FN_INFO=0,
  GPIO_TRC_FN_STRING
} enGPIOTraceMessages;

typedef enum 
{
  enDEV_GPIO_IOOpen=0,
  enDEV_GPIO_s32IOClose,
  enDEV_GPIO_s32IOControl,
  enDEV_GPIO_CallbackThread,
  enDEV_GPIO_EnableInt,
  enDEV_GPIO_GetDrvGpioDevID,
  enDEV_GPIO_IsActiveLow,
  enDEV_GPIO_IsStateActive,
  enDEV_GPIO_SetCallback,
  enDEV_GPIO_SetDirection,
  enDEV_GPIO_SetTrigger,
  enDEV_GPIO_SetLogicalState,
  enDEV_GPIO_SysfsExportPin,
  enDEV_GPIO_SysfsFileOpen,
  enDEV_GPIO_SysfsFileClose,
  enDEV_GPIO_SysfsFileReadInt,
  enDEV_GPIO_SysfsFileWriteStr,
  enDEV_GPIO_SysfsFileWriteU32,
  enDEV_GPIO_SysfsGetPath,
  enDEV_GPIO_SysfsSetEdge
} enDevGpioTraceFunction;

/*=============================================================================
=======                              EXPORTS                            =======
=============================================================================*/

/*=============================================================================
=======                    PROTOTYPES OF PUBLIC FUNCTIONS               =======
=============================================================================*/

tS32 DEV_GPIO_s32IODeviceInit(void);
tS32 DEV_GPIO_s32IODeviceDeinit(void);

tS32 GPIO_IOOpen(void);
tS32 GPIO_s32IOClose(void);
tS32 GPIO_s32IOControl(tS32 s32Fun, intptr_t s32Arg);

#endif
/* EOF */
