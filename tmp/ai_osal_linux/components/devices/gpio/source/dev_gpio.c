/***************************************************************************
*****     (C) COPYRIGHT Robert Bosch GmbH CM-DI - All Rights Reserved     **
******************************************************************************/
/*! 
***     @file        dev_gpio.c
***     @Authors
\n                  TMS-Dangers (mds)
\n                     Fabian Zwissler, CM-DI/EAP (zw)
***
***     @brief       Implementation of dev_gpio for OSAL
***
***
***     @warning     
***
***     @todo        
***
*** --------------------------------------------------------------------------
\par   TECHNICAL INFORMATION:
***
\n     Hardware       : Triton / ADIT-Platform; Tested with Navengine Working
*                       Sample 
\n    Operatingsystem: T-Kernel
***
*** --------------------------------------------------------------------------
\par  VERSION HISTORY:
**  $Log:
*  14.10.2005 - (mds) File created
*  08.03.2006 - (zw)  Adaptions to T-Kernel(Dragon Platform)
*  29.04.2008 - Implementation of SetCallback in thread 
*               context(Shilpa Bhat-RBEI/ECM1)
** 06.05.2008 - Changes in Resource allocation and deallocation for 
*               Setcallback Implementation of Callback Unregister 
*               feature (Shilpa Bhat-RBEI/ECM1)
*  15.05.2008 - Modified ISR's (Shilpa Bhat -RBEI/ECM1)
*  21.05.2008 - OSAL Enumeration for pins (Shilpa Bhat -RBEI/ECM1)
*  26.06.2008 - Updated osal_dragon_config_entry_t 
*               list (Shilpa Bhat -RBEI/ECM1)
*  02.07.2008 - Updated  osal_dragon_config_entry_t 
*               list (Shilpa Bhat -RBEI/ECM1)
*  19.08.2008 - Updated based on MMS ticket on bugfix for OSAL_s32EventWait
*               and changes based on OSAL_s32EventPost
*  14.11.2008 - Implementation of Active/Inactive
*               states (Shilpa Bhat -RBEI/ECM1) 
*  24.07.2009 - Ported for ADIT Gen2 (Hari Babu S- RBEI/ECF1)
*  24.03.2010 - Modified porting to support 128 GPIO pins(vij7kor, RBEI/ECF1)
*  23.09.2010 - New entries have been added in hashtable to related to Antenn Open,
*               Short and SDHN pins (Sainath Kalpuri - RBEI/ECF1)
*  22/06/2011 - Fix FOR LINTS- (Madhu Kiran Ramachandra (RBEI/ECF1C))
*  03/04/2012 - Added Single Callback Registration for positive edge trigger and
*               negative edge trigger- (Madhu Kiran Ramachandra (RBEI/ECF1C))
*  14.08.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
*  07.01.2013 - Sync test pins with osioctrl.h - Matthias Thomae (CM-AI/PJ-CF31)
*  07.05.2014 - Added GPIO PIN for GNSS ANT - Nikhil Ravindran (RBEI/ECF5)
*  14.07.2014 - Updated callback thread     - Nikhil Ravindran (RBEI/ECF5)
*  22.07.2014 - Added GPIO PIN for GMMY16 - Nikhil Ravindran (RBEI/ECF5)
*  12.02.2015 - Added GPIO PIN for PSA B2-B3 - Nikhil Ravindran (RBEI/ECF5)
*  18.11.2015 - Added GPIO PIN for A-IVI - Nikhil Ravindran (RBEI/ECF5)
*  17.12.2015 - Added IO Controls for LSIM - Sadhna Sharma (RBEI/ECF5)
*  18.11.2015 - Added GPIO PIN for CAF - Nikhil Ravindran (RBEI/ECF5)
*  15.06.2016 - Added GPIO PIN for A-IVI - Ajay Vishwanath Bande (RBEI/ECF5)
*  29.07.2016 - Added GPIO PIN for A-IVI - Nikhil Ravindran (RBEI/ECF5)
*  12.09.2016 - Added GPIO PIN for A-IVI - Nikhil Ravindran (RBEI/ECF5)
*\n*/

/***************************************************************************/

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <poll.h>

/* OSAL Device header */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
#include "dev_gpio.h"

/*****************************************************************
| defines and macros (scope: modul-local)
|----------------------------------------------------------------*/

#define DRV_GPIO_VERSION              ((tS32) 0x00000001)
#define DEV_GPIO_THREAD_STACKSIZE     (2048)
#define DEV_GPIO_SEM_NAME             "DEV_GPIO_SEM"

#define SYSFS_STR_GPIO_DIRECTION_IN   "in"
#define SYSFS_STR_GPIO_EDGE_NONE      "none"
#define SYSFS_STR_GPIO_EDGE_RISING    "rising"
#define SYSFS_STR_GPIO_EDGE_FALLING   "falling"
#define SYSFS_STR_GPIO_EDGE_BOTH      "both"
#define U32_MAX_DECIMALS              11 /* max nof decimals of tU32 + 1 */

#ifndef GEN3X86
#define SYSFS_STR_GPIO_DIRECTION      "direction"
#define SYSFS_STR_GPIO_VALUE          "value"
#define SYSFS_STR_GPIO_EDGE           "edge"
#define SYSFS_STR_GPIO_ACTIVE_LOW     "active_low"
#define SYSFS_STR_GPIO_DIRECTION_LOW  "low"
#define SYSFS_STR_GPIO_DIRECTION_HIGH "high"
#define INT_MAX_DECIMALS              10 /* max nof decimals of tInt + 1 */

#else
#define DEFAULT_DTS                   "/opt/bosch/base/lsim/dts/default_dts.txt"
#define DTS_TMP_FILE                  "/tmp/dts_tmp.txt"
#define DTS_LINE_SIZE                   100
#define STR_GPIO_DIRECTION            "GPIO_DIR="
#define STR_GPIO_ACTIVE_LOW           "GPIO_ACTIVE_LOW=0\n"
#define STR_GPIO_VAL                  "GPIO_VAL="
#define STR_GPIO_DIRECTION_OUT        "out"
#define STR_GPIO_EDGE                 "GPIO_EDGE="
#define FILE_OPEN_MODE                "r+"
static const tChar GPIO_LOGICAL_ID[DTS_LINE_SIZE] = "GPIO_LOGICAL_ID=";
#endif


typedef enum
{
   GPIO_DIRECTION_IN = 0,
   GPIO_DIRECTION_OUT,
   GPIO_DIRECTION_OUT_ACTIVE,
   GPIO_DIRECTION_OUT_INACTIVE
} enDevGpioDirection;

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

typedef struct
{
   OSAL_tpfGPIOCallback    callback;
   tVoid*                  callbackArg;
} GPIOCallbackData;

/*****************************************************************
| variable definition (scope: modul-local)
|----------------------------------------------------------------*/

/* the callback function pointers and arguments */
static GPIOCallbackData CallbackData[GPIO_NOF_PINS_IDS] = {0};
/* the ID of the callback thread */
static OSAL_tThreadID CallbkThrID = 0;
/*
 * the poll file descriptors for all GPIO pins
 * PollFdSet[0] contains the read end of the unblock pipe once initialized
 */
static struct pollfd PollFdSet[GPIO_NOF_PINS_IDS] = {0};
/* the count of currently open poll files */
static tU32 NofOpenPollFiles = 0;
/* the read/write ends of the pipe used to unblock the callback thread */
static tInt UnblkPipeFdSet[2] = {0};
/* the trigger edges for all GPIO pins */
static tU16 TriggerEdge[GPIO_NOF_PINS_IDS] = {OSAL_GPIO_EDGE_NONE};

/* the semaphore handle */
static OSAL_tSemHandle SemHndl = OSAL_C_INVALID_HANDLE;

/*
 * the reference count of users of this instance of DRV GPIO
 */
static tU32 RefCnt = 0;

/*
 * the process ID of this instance of DRV GPIO
 * also indicates the initialization state of the instance
 */
static tS32 s32Pid = 0;

/*****************************************************************
| function prototype (scope: modul-local)
|----------------------------------------------------------------*/

/* IOControl functions */
static inline tS32 drv_GPIO_GetVersion(tVoid);//To satisfy lint
static tU32 drv_GPIO_SetInput(const OSAL_tGPIODevID *prID);
static tU32 drv_GPIO_SetOutput(const OSAL_tGPIODevID *prID);
static tU32 drv_GPIO_SetOutputInactive(const OSAL_tGPIODevID *pcGpio);
static tU32 drv_GPIO_SetOutputActive(const OSAL_tGPIODevID *pcGpio);
static tU32 drv_GPIO_SetActiveState(const OSAL_trGPIOData *prData);
static tU32 drv_GPIO_SetInactiveState(const OSAL_trGPIOData *prData);
static tU32 drv_GPIO_SetCallback(const OSAL_trGPIOCallbackData *prData);
static tU32 drv_GPIO_SetTrigger(const OSAL_trGPIOData *prData);
static tU32 drv_GPIO_EnableInt(const OSAL_trGPIOData *prData);
/* GPIO internal functions */
static tVoid drv_GPIO_CallbackThread(tVoid);
static tU32 drv_GPIO_SysfsFileClose(const tInt fd);
static tDrvGpioDevID drv_GPIO_GetDrvGpioDevID(const OSAL_tGPIODevID pID);
tBool drv_GPIO_ExistsPath(const tPChar path);
static tVoid drv_GPIO_CheckLogicalIDs(tVoid);
tVoid DEV_GPIO_vTraceString(TR_tenTraceLevel enTraceLevel,
                            enDevGpioTraceFunction enFunction,
                            tPCChar copchDescription);
tVoid DEV_GPIO_vTraceInfo(TR_tenTraceLevel enTraceLevel,
                          enDevGpioTraceFunction enFunction,
                          tPCChar copchDescription,
                          tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
                          tU32 u32Par4);
#ifndef GEN3X86
/* IOControl functions */
static tU32 drv_GPIO_IsStateActive(OSAL_trGPIOData *prData);
/* DRV GPIO internal functions */
static tU32 drv_GPIO_SetDirection(const OSAL_tGPIODevID    *pID,
                                  const enDevGpioDirection  direction);
static tU32 drv_GPIO_SetLogicalState(const OSAL_trGPIOData *prData,
                                     const tBool            value);
static tU32 drv_GPIO_SysfsFileOpen(const tDrvGpioDevID iID,
                                   const tPChar        file,
                                   const tInt          flags,
                                   tPInt               pFd);
static tU32 drv_GPIO_SysfsFileReadInt(const tInt   fd,
                                      tPInt        pValue);
static tU32 drv_GPIO_SysfsFileWriteStr(const tInt   fd,
                                       const tPChar pStr);
static tU32 drv_GPIO_SysfsFileWriteU32(const tInt   fd,
                                const tU32 value);
static tU32 drv_GPIO_SysfsSetEdge(const tDrvGpioDevID iID,
                                  const tU16 osalEdge);
static tU32 drv_GPIO_SysfsIsActiveLow(const tDrvGpioDevID iID,
                                      tPBool              pActiveLow);
/* implemented in dev_gpio_boardcfg.c or dev_gpio_devicecfg.c */
tU32 drv_GPIO_SysfsGetPath(const tDrvGpioDevID iID,
                           tPChar              path);
#else
/*LSIM Internal functions*/
static tU32 drv_LSIM_GPIO_vInitGpio();
static tU32 drv_LSIM_GPIO_TextFileWriteStr(FILE *fp,const tPChar pStr);
static FILE* drv_LSIM_GPIO_TextFileOpen(const OSAL_tGPIODevID iID);
static tVoid drv_LSIM_GPIO_TextIsActiveLow(const tPChar gpio_value,
                                                 tPBool pActiveLow);
static tU32 drv_LSIM_GPIO_SetDirection(const OSAL_tGPIODevID *pID,
                                     const enDevGpioDirection direction);
static tU32 drv_LSIM_GPIO_SetLogicalState(const OSAL_trGPIOData *prData,
                                         const tBool            value);
static tU32 drv_LSIM_GPIO_SetEdge(const tDrvGpioDevID iID,
                                           const tU16 osalEdge);
static tU32 drv_LSIM_GPIO_TextFileReadValue(tPInt value,FILE * fp);
/* LSIM IOControl functions */
static tU32 drv_LSIM_GPIO_IsStateActive(OSAL_trGPIOData *prData);
#endif

/*****************************************************************
| function implementation (scope: modul-local)
|----------------------------------------------------------------*/

/************************************************************************/
/*! 
*\fn       tS32 DEV_GPIO_s32IODeviceInit()
*\brief    Cross-process/thread initialization of DRV GPIO
*
*\return   OSAL error code
*
*\par History:
* 09.08.2012 - Initial version for Linux - Matthias Thomae (CM-AI/PJ-CF31)
* 17.12.2015 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
tS32 DEV_GPIO_s32IODeviceInit()
{
   drv_GPIO_CheckLogicalIDs();
#ifdef  GEN3X86
   /*Initialize GPIO */
   if (OSAL_E_NOERROR != drv_LSIM_GPIO_vInitGpio())
   {
      return OSAL_E_NOTINITIALIZED;
   }
#endif
   /* create semaphore */
   if (OSAL_ERROR == OSAL_s32SemaphoreCreate(DEV_GPIO_SEM_NAME, &SemHndl, 1))
   {
      NORMAL_M_ASSERT_ALWAYS();
      return (tS32)OSAL_u32ErrorCode();
   }

   return OSAL_E_NOERROR;
}

/************************************************************************/
/*!
*\fn       tS32 DEV_GPIO_s32IODeviceDeinit()
*\brief    Cross-process/thread deinitialization of DRV GPIO
*
*\return   OSAL error code
*
*\par History:
* 09.08.2012 - Initial version for Linux - Matthias Thomae (CM-AI/PJ-CF31)
* 17.12.2015 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
tS32 DEV_GPIO_s32IODeviceDeinit()
{
   /* close semaphore */
   if (OSAL_ERROR == OSAL_s32SemaphoreClose(SemHndl))
   {
      NORMAL_M_ASSERT_ALWAYS();
      OSAL_s32SemaphoreDelete(DEV_GPIO_SEM_NAME);
      return (tS32)OSAL_u32ErrorCode();
   }
   /* delete semaphore */
   if (OSAL_ERROR == OSAL_s32SemaphoreDelete(DEV_GPIO_SEM_NAME))
   {
      NORMAL_M_ASSERT_ALWAYS();
      return (tS32)OSAL_u32ErrorCode();
   }

   SemHndl = OSAL_C_INVALID_HANDLE;
#ifdef  GEN3X86
   fcloseall();
#endif

   return OSAL_E_NOERROR;
}


/************************************************************************/
/*!
*\fn       tS32 GPIO_IOOpen()
*\brief    Opens and initializes the drv_gpio module for further usage
*          (process-specific)
*
*\return   OSAL error code
*
*\par History:  
* 14.10.2005 - (mds) Initial implementation
* 18.07.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
**********************************************************************/
tS32 GPIO_IOOpen()
{
#ifndef _LINUXX86_64_
   tU32 retVal = OSAL_E_NOERROR;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IOOpen,
                       "Enter:", s32Pid, RefCnt, 0, 0);
#endif

   if (s32Pid == 0)
   {
      /* initialize */
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IOOpen,
                       "initializing", s32Pid, 0, 0, 0);
#endif
      /* get process id */
      s32Pid = getpid();

      /* get semaphore */
      if(SemHndl == OSAL_C_INVALID_HANDLE)
      {
         if (OSAL_ERROR == OSAL_s32SemaphoreOpen(DEV_GPIO_SEM_NAME, &SemHndl))
         {
            DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_IOOpen,
                            "smphr open failed", s32Pid, RefCnt, 0, 0);
            NORMAL_M_ASSERT_ALWAYS();
            return (tS32)OSAL_u32ErrorCode();
         }
      }
   }

   /* enter critical section */
   if (OSAL_ERROR == OSAL_s32SemaphoreWait(SemHndl, OSAL_C_TIMEOUT_FOREVER))
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_IOOpen,
                         "smphr wait failed", s32Pid, RefCnt, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return (tS32)OSAL_u32ErrorCode();
   }

   /* increment reference count to keep track of 'users' of DRV GPIO */
   RefCnt++;

   /* exit critical section */
   if (OSAL_ERROR == OSAL_s32SemaphorePost(SemHndl))
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_IOOpen,
                         "smphr post failed", s32Pid, RefCnt, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return (tS32)OSAL_u32ErrorCode();
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IOOpen,
                       "Exit:", s32Pid, RefCnt, retVal, 0);
#endif

   return (tS32)retVal;
#else
    return OSAL_E_NOERROR;
#endif
}


/************************************************************************/
/*! 
*\fn       tS32 GPIO_s32IOClose()
*\brief    Reset drv_gpio to uninitialized state
*          (process-specific)
*
*\return   OSAL error code
*
*\par History:  
* 14.10.2005 - (mds) Initial implementation
* 18.07.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
**********************************************************************/

tS32 GPIO_s32IOClose()
{
#ifndef _LINUXX86_64_
   tU32 retVal = OSAL_E_NOERROR;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_s32IOClose,
                       "Enter:", s32Pid, RefCnt, 0, 0);
#endif

   /* decrement reference count to keep track of 'users' of DRV GPIO */
   if (RefCnt > 0)
   {
      /* enter critical section */
      if (OSAL_ERROR == OSAL_s32SemaphoreWait(SemHndl, OSAL_C_TIMEOUT_FOREVER))
      {
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_s32IOClose,
                            "smphr wait failed", s32Pid, RefCnt, 0, 0);
         NORMAL_M_ASSERT_ALWAYS();
         return (tS32)OSAL_u32ErrorCode();
      }

      RefCnt--;

      if (RefCnt == 0)
      {
         /* deinitialize */
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_s32IOClose,
                             "deinitializing", s32Pid, 0, 0, 0);
#endif
         /* reset process ID */
         s32Pid = 0;
      }

      /* exit critical section */
      if (OSAL_ERROR == OSAL_s32SemaphorePost(SemHndl))
      {
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_s32IOClose,
                            "smphr post failed", s32Pid, RefCnt, 0, 0);
         NORMAL_M_ASSERT_ALWAYS();
         return (tS32)OSAL_u32ErrorCode();
      }
   }
   else
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_s32IOClose,
                         "not open", 0, 0, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_DOESNOTEXIST;
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_s32IOClose,
                       "Exit:", s32Pid, RefCnt, retVal, 0);
#endif

   return (tS32)retVal;
#else
    return OSAL_E_NOERROR;
#endif
}


/************************************************************************/
/*! 
*\fn       tS32 GPIO_s32IOControl(tS32 s32Fun, tS32 s32Arg)
*\brief    IO control functions for drv_gpio
*
*\param    s32Fun - Function to execute
*\param    s32Arg - Function specific Parameter
*
* \return OSAL_E_NOERROR indicates success, check description for other 
*         possible values.
*
* \par History:  
* 14.10.2005 - (mds) Initial implementation
*  \n 08.03.2006 - (fz)  Adaptions for Dragon-Platform because no edge 
*                   trigger are available
* 14.08.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
* 17.12.2015 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
tS32 GPIO_s32IOControl(tS32 s32Fun, intptr_t s32Arg)
{
#ifndef _LINUXX86_64_
   tU32 u32RetVal = OSAL_E_NOTSUPPORTED;

   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_2, enDEV_GPIO_s32IOControl,
                      "Enter:", (tU32)s32Fun, (tU32)s32Arg, (tU32)s32Pid, 0);

   /* Check init state */
   if (s32Pid > 0)
   {
      /* Sanity check */
      if (s32Arg)
      {
         switch (s32Fun)
         {
         case OSAL_C_S32_IOCTRL_VERSION:
            *((tS32*)s32Arg) = drv_GPIO_GetVersion();
            u32RetVal = OSAL_E_NOERROR;
            break;
         case OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK:
            u32RetVal = drv_GPIO_SetCallback((
                  const OSAL_trGPIOCallbackData *)s32Arg);
            break;
         case OSAL_C_32_IOCTRL_GPIO_SET_INPUT:
            u32RetVal = drv_GPIO_SetInput((const OSAL_tGPIODevID *)s32Arg);
            break;
         case OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT:
            u32RetVal = drv_GPIO_SetOutput((const OSAL_tGPIODevID *)s32Arg);
            break;
         case OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER:
            u32RetVal = drv_GPIO_SetTrigger((const OSAL_trGPIOData *)s32Arg);
            break;
         case OSAL_C_32_IOCTRL_GPIO_ENABLE_INT:
            u32RetVal = drv_GPIO_EnableInt((const OSAL_trGPIOData *)s32Arg);
            break;
         case OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_ACTIVE:
            u32RetVal = drv_GPIO_SetOutputActive(
                  (const OSAL_tGPIODevID *)s32Arg);
            break;
         case OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_INACTIVE:
            u32RetVal = drv_GPIO_SetOutputInactive(
                  (const OSAL_tGPIODevID *)s32Arg);
            break;
         case OSAL_C_32_IOCTRL_GPIO_IS_STATE_ACTIVE:
#ifndef GEN3X86
            u32RetVal = drv_GPIO_IsStateActive((OSAL_trGPIOData *)s32Arg);
#else
            u32RetVal = drv_LSIM_GPIO_IsStateActive((OSAL_trGPIOData *)s32Arg);
#endif
            break;
         case OSAL_C_32_IOCTRL_GPIO_SET_ACTIVE_STATE:
            u32RetVal = drv_GPIO_SetActiveState(
                  (const OSAL_trGPIOData *)s32Arg);
            break;
         case OSAL_C_32_IOCTRL_GPIO_SET_INACTIVE_STATE:
            u32RetVal = drv_GPIO_SetInactiveState(
                  (const OSAL_trGPIOData *)s32Arg);
            break;
         /*
          * SET_STATE, GET_STATE, SET_OUTPUT_LOW and SET_OUTPUT_HIGH are not
          * supported on Linux. Applications shall not need to care if a signal
          * is active-low or active-high.
          * Besides, signal activity is handled by the kernel (see
          * /sys/class/gpio/gpioN/active_low). Hence, only SET_ACTIVE_STATE,
          * SET_INACTIVE_STATE and IS_STATE_ACTIVE are supported for changing
          * output pin states. Only SET_OUTPUT_ACTIVE and SET_OUTPUT_INACTIVE
          * are supported for setting output mode with initial value.
          */
         case OSAL_C_32_IOCTRL_GPIO_SET_STATE:
         case OSAL_C_32_IOCTRL_GPIO_GET_STATE:
         case OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_LOW:
         case OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT_HIGH:
         case OSAL_C_32_IOCTRL_GPIO_ENABLE_TESTMODE:
         case OSAL_C_32_IOCTRL_GPIO_GET_TESTMODE:
            DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_s32IOControl,
                               "unsupported IOCTRL",
                               (tU32)s32Fun, (tU32)s32Arg, (tU32)s32Pid, 0);
            NORMAL_M_ASSERT_ALWAYS();
            break;
         default:
            DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_s32IOControl,
                               "unknown IOCTRL",
                               (tU32)s32Fun, (tU32)s32Arg, (tU32)s32Pid, 0);
            NORMAL_M_ASSERT_ALWAYS();
            break;
         }
      }
      else
      {
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_s32IOControl,
                            "empty argument",
                            (tU32)s32Fun, (tU32)s32Arg, (tU32)s32Pid, 0);
         u32RetVal = OSAL_E_INVALIDVALUE;
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_s32IOControl,
                         "not initialized",
                         (tU32)s32Fun, (tU32)s32Arg, (tU32)s32Pid, 0);
      u32RetVal = OSAL_E_NOTINITIALIZED;
      NORMAL_M_ASSERT_ALWAYS();
   }

   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_2, enDEV_GPIO_s32IOControl,
                       "Exit:", (tU32)s32Fun, (tU32)s32Arg,
                       (tU32)s32Pid, u32RetVal);

   return (tS32)u32RetVal;
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Fun);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Arg);
    return OSAL_E_NOERROR;
#endif
}

/************************************************************************/
/*!
*\fn       tS32  drv_GPIO_GetVersion(tVoid)
*\brief    Returns the version of drv_gpio
*
*
*\return   The drv_gpio version.
*
*\par History:
* 3.11.2005 - (mds) Initial implementation
\n 08.03.2006 - (fz)  Changed Prototype because function is local
**********************************************************************/

static inline tS32 drv_GPIO_GetVersion(tVoid)//To satisfy lint
{
   return DRV_GPIO_VERSION;
}

/************************************************************************/
/*!
*\fn       tU32 drv_GPIO_SetInput(const OSAL_tGPIODevID *pID)
*\brief    Set the specified device to input mode
*
*            <B><I>Several GPIO-Pins are multiplexed with different functions.
*          Thus ensure that your PINMUX-Setting
*            is set up correctly for your appropriate hardware.</I></B>
*
*\param  *pID - Device ID
*
*\return OSAL_E_NOERROR indicates success, check description for other
*        possible values.
*
*\par History:
* 3.11.2005 - (mds) Initial implementation
\n 08.03.2006 - (fz)  Changed Prototype because function is local
   21.05.2008 - OSAL Enum implementation - Shilpa Bhat (RBEI/ECM1)
   19.07.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
   17.12.2015 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
static tU32 drv_GPIO_SetInput(const OSAL_tGPIODevID *pID)
{
#ifndef  GEN3X86
   return drv_GPIO_SetDirection(pID, GPIO_DIRECTION_IN);
#else
   return drv_LSIM_GPIO_SetDirection(pID, GPIO_DIRECTION_IN);
#endif
}


/************************************************************************/
/*!
*\fn       tU32 drv_GPIO_SetOutput(const OSAL_tGPIODevID *pID)
*\brief    Set the specified device to output mode. Default output is Low
*
*            <B><I>Several GPIO-Pins are multiplexed with different functions.
*          Thus ensure that your PINMUX-Setting
*            is set up correctly for your appropriate hardware.</I></B>
*
*\param    pID - Device ID
*
*\return OSAL_E_NOERROR indicates success, check description for other
*        possible values.
*
*\par History:
* 3.11.2005 - (mds) Initial implementation
\n 08.03.2006 - (fz)  Changed Prototype because function is local
   21.05.2008 - OSAL Enum implementation - Shilpa Bhat (RBEI/ECM1)
   19.07.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
   17.12.2015 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
static tU32 drv_GPIO_SetOutput(const OSAL_tGPIODevID *pID)
{
#ifndef  GEN3X86
   return drv_GPIO_SetDirection(pID, GPIO_DIRECTION_OUT);
#else
   return drv_LSIM_GPIO_SetDirection(pID, GPIO_DIRECTION_OUT);
#endif
}

static tU32 drv_GPIO_SetOutputInactive(const OSAL_tGPIODevID *pcGpio)
{
#ifndef  GEN3X86
   return drv_GPIO_SetDirection(pcGpio, GPIO_DIRECTION_OUT_INACTIVE);
#else
   return drv_LSIM_GPIO_SetDirection(pcGpio, GPIO_DIRECTION_OUT_INACTIVE);
#endif
}

static tU32 drv_GPIO_SetOutputActive(const OSAL_tGPIODevID *pcGpio)
{
#ifndef  GEN3X86
   return drv_GPIO_SetDirection(pcGpio, GPIO_DIRECTION_OUT_ACTIVE);
#else
   return drv_LSIM_GPIO_SetDirection(pcGpio, GPIO_DIRECTION_OUT_ACTIVE);
#endif
}

#ifndef GEN3X86
/************************************************************************/
/*!
*\fn       tU32 DRV_GPIO_IsStateActive(OSAL_trGPIOData *prData)

*\brief    Check the active state of the specified pin.
*
*\param    prData->tId           - (in)  pin ID
*          prData->unData.bState - (out) pin state
*
*\return   OSAL error code
*
*\par History:
* 26.08.2008 - Shilpa Bhat (RBEI/ECM1)
* 18.07.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
**********************************************************************/
static tU32 drv_GPIO_IsStateActive(OSAL_trGPIOData *prData)//To satisfy lint
{
   tU32 retVal = OSAL_E_NOERROR;
   tDrvGpioDevID iID = 0;
   tInt fd = 0, value = 0;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IsStateActive,
                       "Enter:", s32Pid, 0, 0, 0);
#endif

   if (prData == NULL)
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_IsStateActive,
                          "invalid prData", s32Pid, 0, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }

   iID = drv_GPIO_GetDrvGpioDevID(prData->tId);
   if (iID == 0)
   {
      return OSAL_E_NOTSUPPORTED;
   }

   retVal = drv_GPIO_SysfsFileOpen(iID, SYSFS_STR_GPIO_VALUE, O_RDONLY, &fd);
   if (retVal != OSAL_E_NOERROR)
   {
      return retVal;
   }

   retVal = drv_GPIO_SysfsFileReadInt(fd, &value);
   if (retVal != OSAL_E_NOERROR)
   {
      drv_GPIO_SysfsFileClose(fd);
      return retVal;
   }
   prData->unData.bState = (value!=0) ? TRUE : FALSE;

   retVal = drv_GPIO_SysfsFileClose(fd);

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IsStateActive,
                       "Exit:", s32Pid, 0, 0, 0);
#endif

   return retVal;
}

#endif
/************************************************************************/
/*!
*\fn       tU32 DRV_GPIO_SetActiveState(const OSAL_trGPIOData *prData)

*\brief    Set pin to active state
*
*\param    prData->tId - (in) pin ID
*
*\return   OSAL error code
*
*\par History:
* 26.08.2008 - Shilpa Bhat (RBEI/ECM1)
* 19.07.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
* 17.12.2015 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
static tU32 drv_GPIO_SetActiveState(const OSAL_trGPIOData *prData)//To satisfy lint
{
#ifndef  GEN3X86
   return drv_GPIO_SetLogicalState(prData, 1);
#else
   return drv_LSIM_GPIO_SetLogicalState(prData, 1);
#endif
}

/************************************************************************/
/*!
*\fn       tU32 DRV_GPIO_SetInactiveState(const OSAL_trGPIOData *prData)

*\brief    Set pin to inactive state
*
*\param    prData->tId - (in) pin ID
*
*\return   OSAL error code
*
*\par History:
* 26.08.2008 - Shilpa Bhat (RBEI/ECM1)
* 20.07.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
* 17.12.2015 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
static tU32 drv_GPIO_SetInactiveState(const OSAL_trGPIOData *prData)//To satisfy lint
{
#ifndef  GEN3X86
   return drv_GPIO_SetLogicalState(prData, 0);
#else
   return drv_LSIM_GPIO_SetLogicalState(prData, 0);
#endif
}

/************************************************************************/
/*!
*\fn       tU32 drv_GPIO_SetCallback(const OSAL_trGPIOCallbackData *prData)
*\brief    Install a callback for the specified device
*
*          Passing NULL as the callback pointer will uninstall previously
*          installed callbacks
*
*          It is assumed that this function won't be executed concurrently
*          for *identical* GPIO pins, hence mutex for CallbackData is
*          not required
*
*\param    prData - Callback configuration Data
*
*\return OSAL_E_NOERROR indicates success, check description for other
*        possible values.
*
*\par History:
* 3.11.2005 - (mds) Initial implementation
\n 08.03.2006 - (fz)  Changed Prototype because function is local
*             Modified by Shilpa Bhat (RBEI/ECM1) on 25 Apr, 2008
               Modified by Shilpa Bhat (RBEI/ECM1) on 07 May, 2008
               Modified by Shilpa Bhat (RBEI/ECM1) on 21 May, 2008
* 18.07.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
**********************************************************************/
static tU32 drv_GPIO_SetCallback(const OSAL_trGPIOCallbackData *prData)
{
   tDrvGpioDevID iID = 0;
   tU32 retVal = OSAL_E_NOERROR;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetCallback,
                       "Enter:", s32Pid, 0, 0, 0);
#endif

   if (prData == NULL)
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SetCallback,
                          "invalid prData", s32Pid, 0, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }

   iID = drv_GPIO_GetDrvGpioDevID(prData->rData.tId);
   if (iID == 0)
   {
      return OSAL_E_NOTSUPPORTED;
   }

   if (prData->rData.unData.pfCallback == 0)
   {
      /* unregistering callback */
      CallbackData[iID].callback = 0;
      CallbackData[iID].callbackArg = 0;
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetCallback,
                          "unreg callback", s32Pid, iID, 0, 0);
#endif
   }
   else
   {
      /* already registered? */
      if (CallbackData[iID].callback != 0)
      {
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SetCallback,
                              "already registered", s32Pid, iID, 0, 0);
         NORMAL_M_ASSERT_ALWAYS();
      }
      /* registering callback */
      CallbackData[iID].callback = prData->rData.unData.pfCallback;
      CallbackData[iID].callbackArg = prData->pvArg;
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetCallback,
                          "reg callback", s32Pid, iID,
                          (tU32) CallbackData[iID].callback,
                          (tU32) CallbackData[iID].callbackArg);
#endif
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetCallback,
                       "Exit:", s32Pid, 0, 0, 0);
#endif

   return retVal;
}

/************************************************************************/
/*!
*\fn       tU32 drv_GPIO_SetTrigger(const OSAL_trGPIOData *prData)

*\brief    Set the interrupt trigger level for the specified device
*          Before calling this function, the callback has to be installed.

*          It is assumed that this function won't be executed concurrently
*          for *identical* GPIO pins, hence mutex for PollFdSet is
*          not required
*
*\param    prData - contains the device ID and edge setting parameter
*
*\return OSAL_E_NOERROR indicates success, check description for other
*        possible values.
*
*\par History:
* 3.11.2005 - (mds) Initial implementation
\n 08.03.2006 - (fz)  Changed Prototype because function is local
   21.05.2008 - OSAL Enum implementation - Shilpa Bhat (RBEI/ECM1)
   21.05.2008 - Added feature for application to register single callback
               function for both edge triggers - (Madhu Kiran Ramachandra (RBEI/ECF1C))
* 18.07.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
* 29.12.2015 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
static tU32 drv_GPIO_SetTrigger(const OSAL_trGPIOData *prData)
{
   tU32 retVal = OSAL_E_NOERROR;
   tDrvGpioDevID iID = 0;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetTrigger,
                       "Enter:", s32Pid, iID, 0, 0);
#endif

   if (prData == NULL)
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SetTrigger,
                          "invalid prData", s32Pid, 0, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }

   iID = drv_GPIO_GetDrvGpioDevID(prData->tId);
   if (iID == 0)
   {
      return OSAL_E_NOTSUPPORTED;
   }

   /* save trigger edge in global variable */
   switch (prData->unData.u16Edge)
   {
   case OSAL_GPIO_EDGE_NONE:
   case OSAL_GPIO_EDGE_HIGH:
   case OSAL_GPIO_EDGE_LOW:
   case OSAL_GPIO_EDGE_BOTH:
      TriggerEdge[iID] = prData->unData.u16Edge;
      break;
   default:
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SetTrigger,
                          "invalid edge", prData->unData.u16Edge, 0, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }

   /* reconfigure trigger edge if interrupt enabled */
   if (PollFdSet[iID].fd != 0)
   {
#ifndef GEN3X86
      retVal = drv_GPIO_SysfsSetEdge(iID, TriggerEdge[iID]);
#else
      retVal = drv_LSIM_GPIO_SetEdge(iID, TriggerEdge[iID]);
#endif
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetTrigger,
                       "Exit:", s32Pid, iID, 0, 0);
#endif

   return retVal;
}

/************************************************************************/
/*!
*\fn       tU32 drv_GPIO_EnableInt(const OSAL_trGPIOData *prData)
*\brief    Enable/Disable Interrupt for the specified device
*
*          This function enables or disables the interrupt for the
*          device given in pData->tId. pData->unData.bState is
*          used to indicate the desired state (TRUE - enable/FALSE - disable)
*          If no Callback was installed before calling this function,
*          the result of this function will be an error.
*
*\param    prData - Indicates device and desired state for the Interrupt
*
*\return OSAL_E_NOERROR indicates success, check description for other
*        possible values.
*
*\par History:
* 03.11.2005 - (mds) Initial implementation
* 08.03.2006 - (fz)  Changed Prototype because function is local
* 23.05.2008 - Removed Null check - Shilpa Bhat (RBEI/ECM1)
* 08.10.2012 - Adapted for Linux - Matthias Thomae (CM-AI/PJ-CF31)
* 18.01.2016 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/

static tU32 drv_GPIO_EnableInt(const OSAL_trGPIOData *prData)
{
   tU32 retVal = OSAL_E_NOERROR;
   tDrvGpioDevID iID = 0;
   OSAL_trThreadAttribute CallbkThrAtr;
   tChar szThreadName[OSAL_C_U32_MAX_NAMELENGTH];
   tInt value = 0;
   tChar buf = 0;
#ifdef GEN3X86
   FILE *fp = NULL;
   tChar gpio_value[DTS_LINE_SIZE] = {0};
#endif

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_EnableInt,
                       "Enter:", s32Pid, iID, 0, 0);
#endif

   if (prData == NULL)
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                          "invalid prData", s32Pid, 0, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }

   iID = drv_GPIO_GetDrvGpioDevID(prData->tId);
   if (iID == 0)
   {
      return OSAL_E_NOTSUPPORTED;
   }

   /* open or close pollfd file for given pin ID */
   if (prData->unData.bState == TRUE)
   {
      /* enable interrupt -> create poll file descriptor if not already done */
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_EnableInt,
                          "enabling irq", s32Pid, iID, 0, 0);
#endif
      if (PollFdSet[iID].fd == 0)
      {
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_EnableInt,
                             "open poll file", s32Pid, iID, 0, 0);
#endif
#ifdef GEN3X86
         fp = drv_LSIM_GPIO_TextFileOpen(iID);
         if (fp == NULL)
         {
            tInt errsv = errno;
            NORMAL_M_ASSERT_ALWAYS();
            return OSAL_E_UNKNOWN;
         }
         PollFdSet[iID].fd = fileno(fp);
         /* read the value to prevent poll from returning immediately
          * with an event */
         retVal = drv_LSIM_GPIO_TextFileReadValue(&value,fp);
         if (retVal != OSAL_E_NOERROR)
         {
            return retVal;
         }
#else
         retVal = drv_GPIO_SysfsFileOpen(iID, SYSFS_STR_GPIO_VALUE,
                                         O_RDONLY, &(PollFdSet[iID].fd));
         if (retVal != OSAL_E_NOERROR)
         {
            return retVal;
         }

         /* read the value to prevent poll from returning immediately
          * with an event */
         retVal = drv_GPIO_SysfsFileReadInt(PollFdSet[iID].fd, &value);
         if (retVal != OSAL_E_NOERROR)
         {
            return retVal;
         }
#endif

         PollFdSet[iID].events = POLLPRI;

         /* enter critical section */
         if (OSAL_ERROR == OSAL_s32SemaphoreWait(SemHndl,
               OSAL_C_TIMEOUT_FOREVER))
         {
            DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                               "smphr wait failed", s32Pid, RefCnt, 0, 0);
            NORMAL_M_ASSERT_ALWAYS();
            return OSAL_u32ErrorCode();
         }

         /* increment number of poll files currently in use */
         NofOpenPollFiles++;

         /* exit critical section */
         if (OSAL_ERROR == OSAL_s32SemaphorePost(SemHndl))
         {
            DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                               "smphr post failed", s32Pid, RefCnt, 0, 0);
            NORMAL_M_ASSERT_ALWAYS();
            return OSAL_u32ErrorCode();
         }
      }
   }
   else if (prData->unData.bState == FALSE)
   {
      /* disable interrupt -> remove poll file descriptor */
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_EnableInt,
                          "disabling irq", s32Pid, iID, 0, 0);
#endif
      if (PollFdSet[iID].fd != 0)
      {
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_EnableInt,
                             "close poll file", s32Pid, iID, 0, 0);
#endif
         retVal = drv_GPIO_SysfsFileClose(PollFdSet[iID].fd);
         PollFdSet[iID].fd = 0;
         PollFdSet[iID].events = 0;
         if (retVal != OSAL_E_NOERROR)
         {
            return retVal;
         }

         /* enter critical section */
         if (OSAL_ERROR == OSAL_s32SemaphoreWait(SemHndl,
               OSAL_C_TIMEOUT_FOREVER))
         {
            DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                               "smphr wait failed", s32Pid, RefCnt, 0, 0);
            NORMAL_M_ASSERT_ALWAYS();
            return OSAL_u32ErrorCode();
         }

         /* decrement number of poll files currently in use */
         NofOpenPollFiles--;

         /* exit critical section */
         if (OSAL_ERROR == OSAL_s32SemaphorePost(SemHndl))
         {
            DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                               "smphr post failed", s32Pid, RefCnt, 0, 0);
            NORMAL_M_ASSERT_ALWAYS();
            return OSAL_u32ErrorCode();
         }
      }
   }
   else
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                         "invalid state", s32Pid, iID, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      retVal = OSAL_E_INVALIDVALUE;
   }
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_EnableInt,
                       "open poll files:", s32Pid, NofOpenPollFiles, 0, 0);
#endif

   /* enter critical section */
   if (OSAL_ERROR == OSAL_s32SemaphoreWait(SemHndl,
         OSAL_C_TIMEOUT_FOREVER))
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                         "smphr wait failed", s32Pid, RefCnt, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_u32ErrorCode();
   }

   /* initialize callback thread at first use */
   if (CallbkThrID == 0)
   {
      /* create anonymous pipe for unblocking poll call in callback thread */
      if (pipe(UnblkPipeFdSet) != 0)
      {
         tInt errsv = errno;
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                             "pipe create failed", s32Pid, iID, errsv, 0);
         NORMAL_M_ASSERT_ALWAYS();
         return OSAL_E_UNKNOWN;

      }
      PollFdSet[0].fd = UnblkPipeFdSet[0];
      PollFdSet[0].events = POLLIN;

      /* create callback thread */
      CallbkThrAtr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      CallbkThrAtr.s32StackSize = DEV_GPIO_THREAD_STACKSIZE;
      CallbkThrAtr.pfEntry      = (OSAL_tpfThreadEntry)drv_GPIO_CallbackThread;

      OSALUTIL_s32SaveNPrintFormat(szThreadName, OSAL_C_U32_MAX_NAMELENGTH,
            "DRV_GPIO-%u",s32Pid);
      CallbkThrAtr.szName = szThreadName;

      CallbkThrID = OSAL_ThreadSpawn(&CallbkThrAtr);
      if (OSAL_ERROR == CallbkThrID)
      {
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                             "spawn thread failed", s32Pid, iID,
                             OSAL_u32ErrorCode(), CallbkThrID);
         NORMAL_M_ASSERT_ALWAYS();
         return OSAL_E_THREAD_CREATE_FAILED;
      }
   }
   else
   {
      /* unblock poll call in callback thread to reconfigure PollFdSet */
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_EnableInt,
                          "unblock poll", s32Pid, iID, 0, 0);
#endif
      if (write(UnblkPipeFdSet[1], &buf, 1) != 1)
      {
         tInt errsv = errno;
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                             "pipe write failed", s32Pid, iID, errsv, 0);
         NORMAL_M_ASSERT_ALWAYS();
         return OSAL_E_UNKNOWN;
      }
   }

   if (NofOpenPollFiles == 0)
   {
      /* reset special file descriptor and delete anonymous pipe
       * this will cause the callback thread to exit */
      PollFdSet[0].fd = 0;
      retVal = drv_GPIO_SysfsFileClose(UnblkPipeFdSet[0]);
      if (retVal != OSAL_E_NOERROR)
      {
         return retVal;
      }
      retVal = drv_GPIO_SysfsFileClose(UnblkPipeFdSet[1]);
      if (retVal != OSAL_E_NOERROR)
      {
         return retVal;
      }

      /* reset callback thread ID */
      CallbkThrID = 0;
   }

   /* exit critical section */
   if (OSAL_ERROR == OSAL_s32SemaphorePost(SemHndl))
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_EnableInt,
                         "smphr post failed", s32Pid, RefCnt, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_u32ErrorCode();
   }


   /* set trigger */
   if (prData->unData.bState == TRUE)
   {
      /* enable interrupt */
#ifdef GEN3X86
      retVal = drv_LSIM_GPIO_SetEdge(iID, TriggerEdge[iID]);
#else
      retVal = drv_GPIO_SysfsSetEdge(iID, TriggerEdge[iID]);
#endif
   }
   else
   {
      /* disable interrupt */
#ifdef GEN3X86
      retVal = drv_LSIM_GPIO_SetEdge(iID, OSAL_GPIO_EDGE_NONE);
#else
      retVal = drv_GPIO_SysfsSetEdge(iID, OSAL_GPIO_EDGE_NONE);
#endif
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_EnableInt,
                       "Exit:", s32Pid, iID, 0, 0);
#endif

   return retVal;
}

/************************************************************************/
/*!
*\fn        tVoid drv_GPIO_CallbackThread(tVoid)
*
*\brief    Callback method to be called for interupts handling
*
*
*\return  No return value. Function type is void.
*
*\par History:
* 18.01.2016 - Adapted to Include LSIM code - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
static tVoid drv_GPIO_CallbackThread(tVoid)
{
   tU32 retVal = OSAL_E_NOERROR;
   tInt len = 0, value = 0;
   tDrvGpioDevID iID = 0;
   tChar buf;
#ifdef GEN3X86   
   FILE * fp = NULL;
   tChar gpio_value[DTS_LINE_SIZE] = {0};
#endif

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_CallbackThread,
                       "Enter:", s32Pid, 0, 0, 0);
#endif

   while (1)
   {
      len = poll(PollFdSet, GPIO_NOF_PINS_IDS, -1);
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_CallbackThread,
                          "poll(2) returned", s32Pid, 0, 0, 0);
#endif

      if (len < 0)
      {
         tInt errsv = errno;
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_CallbackThread,
                             "poll(2) failed", s32Pid, errsv, 0, 0);
         retVal = OSAL_E_UNKNOWN;
         if( errsv != EINTR )
         {
         NORMAL_M_ASSERT_ALWAYS();
         break;
         }      
      }

#ifdef GPIO_VERBOSE_TRACE
      if (len == 0)
      {
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_CallbackThread,
                             "poll(2) timeout", s32Pid, 0, 0, 0);
      }
#endif

      /* check if special file descriptor has been reset */
      if (PollFdSet[0].fd == 0)
      {
         /* normal exit from thread */
#ifdef GPIO_VERBOSE_TRACE
          DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_CallbackThread,
                             "callbk thr exit", s32Pid, 0, 0, 0);
#endif
          break;
      }

      /* check if the poll call was unblocked via the special pipe */
      if (PollFdSet[0].revents & POLLIN)
      {
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_CallbackThread,
                             "poll(2) unblk", s32Pid, 0, 0, 0);
#endif
         /* read the (dummy) data from the pipe */
         if (read(PollFdSet[0].fd, &buf, 1) != 1)
         {
            tInt errsv = errno;
            DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_CallbackThread,
                                "read pipe fail", s32Pid, errsv, 0, 0);
            retVal = OSAL_E_UNKNOWN;
            NORMAL_M_ASSERT_ALWAYS();
            break;
         }

         /* reset returned events */
         PollFdSet[0].revents = 0;
      }

      /* check which pins have input data to read */
      for (iID = 1; iID < GPIO_NOF_PINS_IDS; iID++)
      {
         if (PollFdSet[iID].revents & POLLPRI)
         {
            len = lseek(PollFdSet[iID].fd, 0, SEEK_SET);
            if (len < 0)
            {
               tInt errsv = errno;
               DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_CallbackThread,
                                   "seek error", s32Pid, iID, errsv, 0);
               retVal = OSAL_E_UNKNOWN;
               NORMAL_M_ASSERT_ALWAYS();
               break;
            }
#ifdef GEN3X86
            fp = fdopen(PollFdSet[iID].fd,FILE_OPEN_MODE);
            if (fp == NULL)
            {
               tInt errsv = errno;
               NORMAL_M_ASSERT_ALWAYS();
               break;
            }
            retVal = drv_LSIM_GPIO_TextFileReadValue(&value,fp);
            if (retVal != OSAL_E_NOERROR)
            {
               break;
            }
#else
            retVal = drv_GPIO_SysfsFileReadInt(PollFdSet[iID].fd, &value);
            if (retVal != OSAL_E_NOERROR)
            {
               break;
            }
#endif

#ifdef GPIO_VERBOSE_TRACE
            DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_CallbackThread,
                                "poll(2) event", s32Pid, iID, value, 0);
#endif

            /* execute callback function */
            if (CallbackData[iID].callback != 0)
            {
               CallbackData[iID].callback(CallbackData[iID].callbackArg);
            }
            else
            {
               DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_CallbackThread,
                                   "no callbk fun", s32Pid, iID, 0, 0);
               NORMAL_M_ASSERT_ALWAYS();
            }
         }

         /* reset returned events */
         PollFdSet[iID].revents = 0;
      }
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_CallbackThread,
                       "Exit:", s32Pid, retVal, 0, 0);
#endif

   return;
}

#ifndef GEN3X86
/************************************************************************/
/*! 
*\fn       tU32 drv_GPIO_SetDirection(const OSAL_tGPIODevID    *pID,
*                                     const enDevGpioDirection  direction)
*\brief    Set the direction of the specified pin.
*
*\param    pID       - (in) pin ID
*\param    direction - (in) pin direction (in or out with initial value)
*
*\return   OSAL error code
*
*\par History:  
* 19.07.2012 - Created for Linux - Matthias Thomae (CM-AI/PJ-CF31)
**********************************************************************/
static tU32 drv_GPIO_SetDirection(const OSAL_tGPIODevID    *pID,
                                  const enDevGpioDirection  direction)
{
   tDrvGpioDevID iID = 0;
   tU32 retVal = OSAL_E_NOERROR;
   tInt fd = 0;
   tBool isActiveLow;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                       "Enter:", s32Pid, (OSAL_tGPIODevID)pID,
                       (tU32)direction, 0);
#endif

   iID = drv_GPIO_GetDrvGpioDevID((OSAL_tGPIODevID)pID);
   if (iID == 0)
   {
      return OSAL_E_NOTSUPPORTED;
   }

   retVal = drv_GPIO_SysfsFileOpen(iID, SYSFS_STR_GPIO_DIRECTION,
                                   O_WRONLY, &fd);
   if (retVal != OSAL_E_NOERROR)
   {
      return retVal;
   }

   switch (direction)
   {
   case GPIO_DIRECTION_IN:
      retVal = drv_GPIO_SysfsFileWriteStr(fd, SYSFS_STR_GPIO_DIRECTION_IN);
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                             "set IN", iID, 0, 0, 0);
#endif
     break;
   case GPIO_DIRECTION_OUT_ACTIVE:
      retVal = drv_GPIO_SysfsIsActiveLow(iID, &isActiveLow);
      if (retVal != OSAL_E_NOERROR)
      {
         break;
      }
      if (isActiveLow)
      {
         retVal = drv_GPIO_SysfsFileWriteStr(fd, SYSFS_STR_GPIO_DIRECTION_LOW);
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                             "set OUT_LOW", iID, 0, 0, 0);
#endif
      }
      else
      {
         retVal = drv_GPIO_SysfsFileWriteStr(fd, SYSFS_STR_GPIO_DIRECTION_HIGH);
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                             "set OUT_HIGH", iID, 0, 0, 0);
#endif
      }
      break;
   case GPIO_DIRECTION_OUT:
   case GPIO_DIRECTION_OUT_INACTIVE:
      retVal = drv_GPIO_SysfsIsActiveLow(iID, &isActiveLow);
      if (retVal != OSAL_E_NOERROR)
      {
         break;
      }
      if (isActiveLow)
      {
         retVal = drv_GPIO_SysfsFileWriteStr(fd, SYSFS_STR_GPIO_DIRECTION_HIGH);
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                             "set OUT_HIGH", iID, 0, 0, 0);
#endif
      }
      else
      {
         retVal = drv_GPIO_SysfsFileWriteStr(fd, SYSFS_STR_GPIO_DIRECTION_LOW);
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                             "set OUT_LOW", iID, 0, 0, 0);
#endif
      }
      break;
   default:
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SetDirection,
                          "invalid direction", iID, (tU32)direction, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      retVal = OSAL_E_INVALIDVALUE;
      break;
   }

   if (retVal != OSAL_E_NOERROR)
   {
      drv_GPIO_SysfsFileClose(fd);
      return retVal;
   }

   retVal = drv_GPIO_SysfsFileClose(fd);

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                       "Exit:", s32Pid, (tDrvGpioDevID)iID,
                       (tU32)direction, retVal);
#endif
   return retVal;
}

/************************************************************************/
/*!
*\fn       tU32 drv_GPIO_SetLogicalState(const OSAL_trGPIOData *prData,
*                                        const tBool            value)
*
*\brief    Set logical pin state (active/inactive)
*
*\param    prData->tId - (in) pin ID
*          value       - (in) active (1) or inactive (0)
*
*\return   OSAL error code
*
*\par History:
* 19.07.2012 - Created for Linux - Matthias Thomae (CM-AI/PJ-CF31)
**********************************************************************/
static tU32 drv_GPIO_SetLogicalState(const OSAL_trGPIOData *prData,
                                     const tBool            value)
{
   tU32 retVal = OSAL_E_NOERROR;
   tDrvGpioDevID iID = 0;
   tInt fd = 0;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetLogicalState,
                       "Enter:", s32Pid, (tU32)value, 0, 0);
#endif

   if (prData == NULL)
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SetLogicalState,
                          "invalid prData", s32Pid, (tU32)value, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }
   iID = drv_GPIO_GetDrvGpioDevID(prData->tId);
   if (iID == 0)
   {
      return OSAL_E_NOTSUPPORTED;
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetLogicalState,
                       "pin ID", s32Pid, iID, (tU32)value, 0);
#endif

   retVal = drv_GPIO_SysfsFileOpen(iID, SYSFS_STR_GPIO_VALUE, O_WRONLY, &fd);
   if (retVal != OSAL_E_NOERROR)
   {
      return retVal;
   }

   retVal = drv_GPIO_SysfsFileWriteU32(fd, (tU32)value);
   if (retVal != OSAL_E_NOERROR)
   {
      drv_GPIO_SysfsFileClose(fd);
      return retVal;
   }

   retVal = drv_GPIO_SysfsFileClose(fd);

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetLogicalState,
                       "Exit:", s32Pid, (tU32)value, 0, 0);
#endif

   return retVal;
}
#endif

/************************************************************************/
/*!
 *\fn       tDrvGpioDevID drv_GPIO_GetDrvGpioDevID(const OSAL_tGPIODevID pID)

 *\brief    Map OSAL device ID to internal device ID
 *
 *\param    pID - (in) OSAL pin identifier
 *
 *\return   DRV GPIO pin identifier
 *
 *\par History:
 * 03.08.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
static tDrvGpioDevID drv_GPIO_GetDrvGpioDevID(const OSAL_tGPIODevID pID)
{
   tDrvGpioDevID pDrvGpioID = 0;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_GetDrvGpioDevID,
                       "Enter:", s32Pid, pID, 0, 0);
#endif

   /* map OSAL ID to DRV GPIO internal ID */
   if (pID < OSAL_GPIO_OFFSET)
   {
      /* logical ID */
      if (pID == 0 || pID >= OSAL_EN_GPIOPINS_LASTENTRY)
      {
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_GetDrvGpioDevID,
                             "invalid logic ID", s32Pid, pID, 0, 0);
         NORMAL_M_ASSERT_ALWAYS();
      }
      else
      {
         /* logical ID remains unchanged */
         pDrvGpioID = pID;
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_GetDrvGpioDevID,
                             "logic ID intern", s32Pid, pDrvGpioID, 0, 0);
#endif
      }
   }
   else
   {
      /* hardware ID */
      if (pID > OSAL_GPIO_LAST)
      {
         DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_GetDrvGpioDevID,
                             "invalid HW ID", s32Pid, pID, 0, 0);
         NORMAL_M_ASSERT_ALWAYS();
      }
      else
      {
         /* hardware ID is mapped after last logical ID */
         pDrvGpioID = pID - OSAL_GPIO_OFFSET + OSAL_EN_GPIOPINS_LASTENTRY;
#ifdef GPIO_VERBOSE_TRACE
         DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_GetDrvGpioDevID,
                             "HW ID intern", s32Pid, pDrvGpioID, 0, 0);
#endif
      }
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_GetDrvGpioDevID,
                       "Exit:", s32Pid, pID, 0, 0);
#endif

   return pDrvGpioID;
}

#ifndef GEN3X86
/************************************************************************/
/*!
 *\fn       tU32 drv_GPIO_SysfsFileOpen(const tDrvGpioDevID iID,
 *                                      const tPChar        file,
 *                                      const tInt          flags,
 *                                      tPInt               pFd)
 *\brief    Open Sysfs file
 *
 *\param    iID   - (in)  pin identifier
 *          file  - (in)  sysfs file (SYSFS_PATH_...)
 *          flags - (in)  flags for open(2)
 *          pFd   - (out) file descriptor
 *
 *
 *\return   OSAL error code
 *
 *\par History:
 * 18.07.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
static tU32 drv_GPIO_SysfsFileOpen(const tDrvGpioDevID iID,
                                   const tPChar        file,
                                   const tInt          flags,
                                   tPInt               pFd)
{
   tU32 retVal = OSAL_E_NOERROR;
   tInt len = 0;
   tChar cBasePath[MAX_SYSFS_PATH_LEN];
   tChar cFullPath[MAX_SYSFS_PATH_LEN];

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileOpen,
                       "Enter:", s32Pid, 0, 0, 0);
#endif

   retVal = drv_GPIO_SysfsGetPath(iID, cBasePath);
   if (retVal != OSAL_E_NOERROR)
   {
      return retVal;
   }

   len = snprintf(cFullPath, MAX_SYSFS_PATH_LEN, "%s/%s", cBasePath, file);
   if (len <= 0 || len >= MAX_SYSFS_PATH_LEN)
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileOpen,
                          "gen path failed", s32Pid, errsv, len, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   *pFd = open(cFullPath, flags);
   if (*pFd < 0)
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileOpen,
                          "open file failed", s32Pid, errsv, 0, 0);
      /* TODO: replace by macro not emitting full call stack
       * to limit log when V850 not working
       * */
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileOpen,
                       "Exit:", s32Pid, 0, 0, 0);
#endif

   return retVal;
}

#endif
/************************************************************************/
/*!
 *\fn       tU32 drv_GPIO_SysfsFileClose(const tInt fd)
 *\brief    Close Sysfs file
 *
 *\param    fd - (in)  file descriptor
 *
 *\return   OSAL error code
 *
 *\par History:
 * 19.07.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
static tU32 drv_GPIO_SysfsFileClose(const tInt fd)
{
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileClose,
                       "Enter:", s32Pid, 0, 0, 0);
#endif

   if (close(fd) < 0)
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileClose,
                          "close file failed", s32Pid, errsv, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileClose,
                       "Exit:", s32Pid, 0, 0, 0);
#endif

   return OSAL_E_NOERROR;
}

#ifndef GEN3X86
/************************************************************************/
/*!
 *\fn       tU32 drv_GPIO_SysfsFileReadInt(const tInt   fd,
 *                                         tPInt        pValue)
 *\brief    Read integer from Sysfs file
 *
 *\param    fd     - (in)  file descriptor
 *          pValue - (out) value read from Sysfs file
 *
 *
 *\return   OSAL error code
 *
 *\par History:
 * 19.07.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
static tU32 drv_GPIO_SysfsFileReadInt(const tInt   fd,
                                      tPInt        pValue)
{
   tInt len = 0;
   tChar cValue[INT_MAX_DECIMALS];

   len = read(fd, cValue, INT_MAX_DECIMALS);
   if (len < 0)
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileReadInt,
                          "read failed", s32Pid, errsv, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   *pValue = atoi(cValue);

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileReadInt,
                       "value:", s32Pid, *pValue, 0, 0);
#endif

   return OSAL_E_NOERROR;
}


/************************************************************************/
/*!
 *\fn       tU32 drv_GPIO_SysfsFileWriteStr(const tInt   fd,
 *                                          const tPChar pStr)
 *\brief    Write string to Sysfs file
 *
 *\param    fd   - (in) file descriptor
 *          pStr - (in) string to write to Sysfs file
 *
 *
 *\return   OSAL error code
 *
 *\par History:
 * 19.07.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
static tU32 drv_GPIO_SysfsFileWriteStr(const tInt   fd,
                                       const tPChar pStr)
{
   tInt len = 0;

   len = write(fd, pStr, strlen(pStr));
   if (len <= 0 )
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileWriteStr,
                          "write failed", s32Pid, errsv, len, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   return OSAL_E_NOERROR;
}


/************************************************************************/
/*!
 *\fn       tU32 drv_GPIO_SysfsFileWriteU32(const tInt fd,
 *                                          const tU32 value)
 *\brief    Write tU32 to Sysfs file
 *
 *\param    fd    - (in) file descriptor
 *          value - (in) tU32 value to write to Sysfs file
 *
 *
 *\return   OSAL error code
 *
 *\par History:
 * 19.07.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
static tU32 drv_GPIO_SysfsFileWriteU32(const tInt fd,
      const tU32 value)
{
   tInt len = 0;
   tChar cID[U32_MAX_DECIMALS];

   len = snprintf(cID, U32_MAX_DECIMALS, "%lu", value);
   if (len <= 0 || len >= U32_MAX_DECIMALS)
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileWriteU32,
                          "create str failed", s32Pid, value, errsv, len);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   len = write(fd, cID, strlen(cID));
   if (len <= 0 )
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileWriteU32,
                          "write failed", s32Pid, value, errsv, len);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   return OSAL_E_NOERROR;
}

/************************************************************************/
/*!
 *\fn       tU32 drv_GPIO_SysfsSetEdge(const tDrvGpioDevID iID,
 *                                     const tU16 osalEdge)
 *\brief    Set sysfs edge from OSAL edge
 *
 *\param    osalEdge - (in) OSAL edge identifier
 *
 *\return   OSAL error code
 *
 *\par History:
 * 08.10.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
static tU32 drv_GPIO_SysfsSetEdge(const tDrvGpioDevID iID,
                                  const tU16 osalEdge)
{
   tU32 retVal = OSAL_E_NOERROR;
   tInt fd = 0;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsSetEdge,
                       "Enter:", s32Pid, iID, (tU32)osalEdge, 0);
#endif

   retVal = drv_GPIO_SysfsFileOpen(iID, SYSFS_STR_GPIO_EDGE, O_WRONLY, &fd);
   if (retVal != OSAL_E_NOERROR)
   {
      return retVal;
   }

   switch (osalEdge)
   {
   case OSAL_GPIO_EDGE_NONE:
      retVal = drv_GPIO_SysfsFileWriteStr(fd, SYSFS_STR_GPIO_EDGE_NONE);
      break;
   case OSAL_GPIO_EDGE_HIGH:
      retVal = drv_GPIO_SysfsFileWriteStr(fd, SYSFS_STR_GPIO_EDGE_RISING);
      break;
   case OSAL_GPIO_EDGE_LOW:
      retVal = drv_GPIO_SysfsFileWriteStr(fd, SYSFS_STR_GPIO_EDGE_FALLING);
      break;
   case OSAL_GPIO_EDGE_BOTH:
      retVal = drv_GPIO_SysfsFileWriteStr(fd, SYSFS_STR_GPIO_EDGE_BOTH);
      break;
   default:
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsSetEdge,
                          "invalid edge", iID, (tU32)osalEdge, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      retVal = OSAL_E_INVALIDVALUE;
      break;
   }

   retVal = drv_GPIO_SysfsFileClose(fd);
   if (retVal != OSAL_E_NOERROR)
   {
      return retVal;
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsSetEdge,
                       "Exit:", s32Pid, iID, (tU32)osalEdge, 0);
#endif

   return retVal;
}

/************************************************************************/
/*!
*\fn       tU32 drv_GPIO_SysfsIsActiveLow(const tDrvGpioDevID iID,
                                          tPBool              pActiveLow)

*\brief    Check if specified pin is active_low by reading Sysfs value.
*
*\param    prData->tId           - (in)  pin ID
*          pActiveLow            - (out) is active low (true) or high (false)
*
*\return   OSAL error code
*
*\par History:
* 09.10.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
**********************************************************************/
static tU32 drv_GPIO_SysfsIsActiveLow(const tDrvGpioDevID iID,
                                      tPBool              pActiveLow)
{
   tU32 retVal = OSAL_E_NOERROR;
   tInt fd = 0, value = 0;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IsActiveLow,
                       "Enter:", s32Pid, 0, 0, 0);
#endif

   retVal = drv_GPIO_SysfsFileOpen(iID, SYSFS_STR_GPIO_ACTIVE_LOW,
         O_RDONLY, &fd);
   if (retVal != OSAL_E_NOERROR)
   {
      return retVal;
   }

   retVal = drv_GPIO_SysfsFileReadInt(fd, &value);
   if (retVal != OSAL_E_NOERROR)
   {
      drv_GPIO_SysfsFileClose(fd);
      return retVal;
   }
   *pActiveLow =  (value!=0) ? TRUE : FALSE;

   retVal = drv_GPIO_SysfsFileClose(fd);

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IsActiveLow,
                       "Exit:", s32Pid, 0, 0, 0);
#endif

   return retVal;
}
#endif

/************************************************************************/
/*!
 *\fn       tBool drv_GPIO_ExistsPath(const tPChar path)

 *\brief    Check if a path exists
 *
 *\param    path - (in) path
 *
 *\return   OSAL error code
 *
 *\par History:
 * 07.08.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
tBool drv_GPIO_ExistsPath(const tPChar path)
{
   tInt len = 0;
   struct stat st;

   /* do nothing if already exported */
   len = stat(path, &st);
   if (len == 0)
   {
      return TRUE;
   }

   return FALSE;
}

#ifdef GEN3X86
/************************************************************************/
/*! 
*\fn       tU32 drv_LSIM_GPIO_vInitGpio()
*
*\brief    Initializes the pins for Gpio driver in LSIM
*          Copies default_dts.txt to /tmp directory for Gpio operations.
*
*\return   OSAL error code
*
*\par History:  
* 11.09.2015 - Initial implementation - Rahana V S (RBEI/ECF5)
* 18.01.2016 - Modified for error checks - Sadhna Sharma (RBEI/ECF5)
**********************************************************************/
static tU32 drv_LSIM_GPIO_vInitGpio()
{
   tChar command[DTS_LINE_SIZE] = {0};
   tChar file_string[DTS_LINE_SIZE] = {0};

   strcpy(file_string, DEFAULT_DTS);
   
   if (!drv_GPIO_ExistsPath(file_string))
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsGetPath,
                          "path not exist", getpid(), (tU32)0, (tU32)0, (tU32)0);
      DEV_GPIO_vTraceString(TR_LEVEL_FATAL, enDEV_GPIO_SysfsGetPath, file_string);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_DOESNOTEXIST;
   }
   
   /* Copy the selected file to temporary file */
   sprintf(command, "cp %s %s", file_string, DTS_TMP_FILE);
   system(command);
   
   /* ensure that data are in sync */
   system("sync \n");
   OSAL_s32ThreadWait(2000);
   
   return OSAL_E_NOERROR;
}

/************************************************************************/
/*!
*\fn       tU32 drv_LSIM_GPIO_IsStateActive(OSAL_trGPIOData *prData)
*
*\brief    Check the active state of the specified pin.
*
*\param    prData->tId           - (in)  pin ID
*          prData->unData.bState - (out) pin state
*
*\return   OSAL error code
*
*\par History:
* 17.12.2015 -  Initial Version - Sadhna Sharma(RBEI/ECF5)
**********************************************************************/
static tU32 drv_LSIM_GPIO_IsStateActive(OSAL_trGPIOData *prData)
{
   tU32 retVal;
   tDrvGpioDevID iID;
   tInt value = 0;
   FILE *fp = NULL;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IsStateActive,
                       "Enter:", (tU32)s32Pid, (tU32)0, (tU32)0, (tU32)0);
#endif

   if (prData == NULL)
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_IsStateActive,
                          "invalid prData", (tU32)s32Pid, (tU32)0, (tU32)0, (tU32)0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }

   iID = drv_GPIO_GetDrvGpioDevID(prData->tId);
   if (iID == 0)
   {
      return OSAL_E_NOTSUPPORTED;
   }
   
   fp = drv_LSIM_GPIO_TextFileOpen(iID);   
   if (fp == NULL)
   {
      tInt errsv = errno;
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   /* Read STR_GPIO_VAL (0/1) from file */
   retVal = drv_LSIM_GPIO_TextFileReadValue(&value, fp);
   fclose(fp);
   fp = NULL;
   if (retVal != OSAL_E_NOERROR)
   {
      return retVal;
   }

   prData->unData.bState = (value != 0) ? TRUE : FALSE;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IsStateActive,
                       "Exit:", (tU32)s32Pid, (tU32)0, (tU32)0, (tU32)0);
#endif

   return retVal;
}

/****************************************************************************/
/*!
*\fn     tU32 drv_LSIM_GPIO_SetDirection(const OSAL_tGPIODevID    *pID,
*                                       const enDevGpioDirection  direction)
*
*\brief  Set the direction of the specified pin.
*
*\param  pID        - (in) pin ID
*        direction  - (in) pin direction (in or out with initial value)
*
*\return OSAL error code
*
*\par History:
* 11.09.2015 -  Initial Version - Rahana V S (RBEI/ECF5)
* 17.12.2015 -  Adapted for more IO controls - Sadhna Sharma(RBEI/ECF5)
****************************************************************************/
static tU32 drv_LSIM_GPIO_SetDirection(const OSAL_tGPIODevID    *pID,
                                      const enDevGpioDirection  direction)
{
   tU32 retVal = OSAL_E_NOERROR;
   tDrvGpioDevID iID;
   FILE *fp = NULL;
   tChar gpio_value[DTS_LINE_SIZE] = {0};
   tChar dest[DTS_LINE_SIZE]={0};

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                       "Enter:", (tU32)s32Pid, (OSAL_tGPIODevID)pID,
                       (tU32)direction, (tU32)0);
#endif

   /* Get the GPIO pin */
   iID = drv_GPIO_GetDrvGpioDevID((OSAL_tGPIODevID)pID); 
   if (iID == 0)
   {
      return OSAL_E_NOTSUPPORTED;
   }
  
   fp = drv_LSIM_GPIO_TextFileOpen(iID);
   if (fp == NULL)
   {
      tInt errsv = errno;
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   /*increment fp to point to line containing "GPIO_DIR" in file */
   if (fgets(gpio_value, DTS_LINE_SIZE, fp) == NULL)
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SetDirection,
                          "file read failed", (tU32)s32Pid, (tU32)0, 
                          (tU32)0, (tU32)0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   switch (direction)
   {
   /*Sets the corresponding pin to input mode*/
   case GPIO_DIRECTION_IN:
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                          "set IN", iID, (tU32)0, (tU32)0, (tU32)0);
#endif
      snprintf(dest, sizeof(dest), "%s%s%s", STR_GPIO_DIRECTION, " ",
               SYSFS_STR_GPIO_DIRECTION_IN);
      retVal = drv_LSIM_GPIO_TextFileWriteStr(fp, dest);	
      break;
   /* Sets the corresponding pin direction to out and value to 1 */
   case GPIO_DIRECTION_OUT_ACTIVE:
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                          "set OUT_LOW", iID, (tU32)0, (tU32)0, (tU32)0);
#endif
      snprintf(dest, sizeof(dest), "%s%s%s%s%s", STR_GPIO_DIRECTION,
               STR_GPIO_DIRECTION_OUT, "\n", STR_GPIO_VAL, "1\n");
      retVal = drv_LSIM_GPIO_TextFileWriteStr(fp, dest);      
      break;
   case GPIO_DIRECTION_OUT:
   /* Sets the corresponding pin direction to out and value to 0 */
   case GPIO_DIRECTION_OUT_INACTIVE:
#ifdef GPIO_VERBOSE_TRACE
      DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                          "set OUT_HIGH", iID, (tU32)0, (tU32)0, (tU32)0);
#endif
      snprintf(dest, sizeof(dest), "%s%s%s%s%s", STR_GPIO_DIRECTION,
               STR_GPIO_DIRECTION_OUT, "\n", STR_GPIO_VAL, "0\n");
      retVal = drv_LSIM_GPIO_TextFileWriteStr(fp, dest);
      break;
   default:
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SetDirection,
                          "invalid direction", iID, (tU32)direction, (tU32)0, (tU32)0);
      NORMAL_M_ASSERT_ALWAYS();
      retVal = OSAL_E_INVALIDVALUE;
      break;
   }

   fclose(fp);
   fp = NULL;
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetDirection,
                       "Exit:", (tU32)s32Pid, (tDrvGpioDevID)iID,
                       (tU32)direction, retVal);
#endif
   return retVal;
}

/************************************************************************/
/*!
*\fn      tU32 drv_LSIM_GPIO_SetLogicalState(const OSAL_trGPIOData *prData,
*                                            const tBool            value)
*
*\brief    Set logical pin state (active/inactive)
*
*\param    prData->tId - (in) pin ID
*          value       - (in) active (1) or inactive (0)
*
*\return   OSAL error code
*
*\par History:
* 17.12.2015 - Initial Version - Sadhna Sharma(RBEI/ECF5)
* 15.03.2016 - Modified to support change of state only on output pins 
             - Sadhna Sharma(RBEI/ECF5)
************************************************************************/
static tU32 drv_LSIM_GPIO_SetLogicalState(const OSAL_trGPIOData *prData,
                                          const tBool            value)
{
   tU32 retVal;
   tDrvGpioDevID iID;
   FILE *fp = NULL;
   tChar dest[DTS_LINE_SIZE] = {0};
   tChar cID[U32_MAX_DECIMALS] = {0};
   tChar gpio_value[DTS_LINE_SIZE] = {0};

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetLogicalState,
                       "Enter:", (tU32)s32Pid, (tU32)value, (tU32)0, (tU32)0);
#endif

   snprintf(cID,U32_MAX_DECIMALS,"%lu",value);

   if (prData == NULL)
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SetLogicalState,
                          "invalid prData", (tU32)s32Pid, (tU32)value, (tU32)0, (tU32)0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }
   
   iID = drv_GPIO_GetDrvGpioDevID(prData->tId);
   if (iID == 0)
   {
      return OSAL_E_NOTSUPPORTED;
   }

   fp = drv_LSIM_GPIO_TextFileOpen(iID);
   if (fp == NULL)
   {      
      tInt errsv = errno;
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   /*advances fp to point at  STR_GPIO_VAL line*/
   while(fgets(gpio_value, DTS_LINE_SIZE, fp) != NULL)
   {
      if (strstr(gpio_value, STR_GPIO_DIRECTION))
      {
         break;
      }
   }
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceString(TR_LEVEL_USER_4,enDEV_GPIO_SetLogicalState,gpio_value);
#endif
   /* allow state change only on output pin*/
   if (strstr(gpio_value, STR_GPIO_DIRECTION_OUT))
   {
      /* write value (0/1) to STR_GPIO_VAL line  */
      snprintf(dest, sizeof(dest), "%s%s%s", STR_GPIO_VAL, cID, "\n");
      retVal = drv_LSIM_GPIO_TextFileWriteStr(fp, dest);
   }
   else
   {
      retVal = OSAL_E_NOTSUPPORTED;
   }
   fclose(fp);
   fp = NULL;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SetLogicalState,
                       "Exit:", (tU32)s32Pid, (tU32)value, (tU32)0, (tU32)0);
#endif

   return retVal;
}

/************************************************************************/
/*!
*\fn        tU32 drv_LSIM_GPIO_TextFileReadValue(tPInt value,FILE * fp)
*
*\brief     Read value(0/1) of STR_GPIO_VAL from /tmp/dts_tmp.txt
*
*\param     fp          - (in) file pointer
*           value       - (out) active (1) or inactive (0)
*
*\return    OSAL error code
*
*\par History:
* 29.12.2015 - Initial Version - Sadhna Sharma(RBEI/ECF5)
*************************************************************************/
static tU32 drv_LSIM_GPIO_TextFileReadValue(tPInt value,FILE * fp)
{
   tChar gpio_value[DTS_LINE_SIZE] = {0};

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileReadInt,
                       "Exit:", (tU32)s32Pid, (tU32)value, (tU32)0, (tU32)0);
#endif

   if (fp == NULL)
   {
      tInt errsv = errno;
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   while(fgets(gpio_value,DTS_LINE_SIZE,fp) != NULL)
   {
     if (strstr(gpio_value,STR_GPIO_VAL))
     {
        /*set value to 1, if STR_GPIO_VAL=1 in file, for a pin*/
        if ((strstr(gpio_value,"=0") == NULL) && (value != NULL))
        {
           *value = 1;
        }
        break;
     }
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileReadInt,
                       "Exit:", (tU32)s32Pid, (tU32)*value, (tU32)0, (tU32)0);
#endif

   return OSAL_E_NOERROR;
}

/***********************************************************************/
/*!
*\fn        tU32 drv_LSIM_GPIO_SetEdge(const tDrvGpioDevID iID,
*                                        const tU16        osalEdge)
*
*\brief     Set edge of STR_GPIO_EDGE in /tmp/dts_tmp.txt 
*           from OSAL edge
*
*\param     osalEdge - (in) OSAL edge identifier
*
*\return    OSAL error code
*
*\par History:
* 29.12.2015 - Initial Version - Sadhna Sharma(RBEI/ECF5) 
***********************************************************************/
static tU32 drv_LSIM_GPIO_SetEdge(const tDrvGpioDevID iID,
                                    const tU16        osalEdge)
{
   tU32 retVal = OSAL_E_NOERROR;
   FILE * fp;
   tChar dest[DTS_LINE_SIZE] = {0};
   tChar gpio_value[DTS_LINE_SIZE] = {0};
   
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsSetEdge,
                       "Enter:", (tU32)s32Pid, iID, (tU32)osalEdge, (tU32)0);
#endif

   fp = drv_LSIM_GPIO_TextFileOpen(iID);
   if (fp == NULL)
   {
      tInt errsv = errno;
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   /*advances fp to point at  STR_GPIO_EDGE line*/
   while(fgets(gpio_value,DTS_LINE_SIZE , fp) != NULL)
   {
     if ((strstr(gpio_value,STR_GPIO_VAL)))
     {
        break;
     }
   }
   switch (osalEdge)
   {
   case OSAL_GPIO_EDGE_NONE:
      snprintf(dest, sizeof(dest), "%s%s%s%s", STR_GPIO_EDGE, "   ",
               SYSFS_STR_GPIO_EDGE_NONE, "\n");
      retVal = drv_LSIM_GPIO_TextFileWriteStr(fp, dest);
      break;
   case OSAL_GPIO_EDGE_HIGH:
      snprintf(dest, sizeof(dest), "%s%s%s%s", STR_GPIO_EDGE, " ",
               SYSFS_STR_GPIO_EDGE_RISING, "\n");
      retVal = drv_LSIM_GPIO_TextFileWriteStr(fp, dest); 
      break;
   case OSAL_GPIO_EDGE_LOW:
      snprintf(dest, sizeof(dest), "%s%s%s%s", STR_GPIO_EDGE, "",
               SYSFS_STR_GPIO_EDGE_FALLING, "\n");
      retVal = drv_LSIM_GPIO_TextFileWriteStr(fp, dest);
      break;
   case OSAL_GPIO_EDGE_BOTH:
      snprintf(dest, sizeof(dest), "%s%s%s%s", STR_GPIO_EDGE,"   ",
               SYSFS_STR_GPIO_EDGE_BOTH, "\n");
      retVal = drv_LSIM_GPIO_TextFileWriteStr(fp, dest);
      break;
   default:
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsSetEdge,
                          "invalid edge", iID, (tU32)osalEdge, (tU32)0, (tU32)0);
      NORMAL_M_ASSERT_ALWAYS();
      retVal = OSAL_E_INVALIDVALUE;
      break;
   }

   fclose(fp);

   fp = NULL;
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsSetEdge,
                       "Exit:", (tU32)s32Pid, iID, (tU32)osalEdge, (tU32)0);
#endif
   
   return retVal;
}

/************************************************************************/
/*!
*\fn      tVoid drv_LSIM_GPIO_TextIsActiveLow(tPChar gpio_value,
*                                             tPBool pActiveLow)

*\brief   Check if specified pin is active_low by reading value 
*         of GPIO_ACTIVE_LOW, from tmp/dts_tmp.txt
*
*\param   gpio_value    -(in)   GPIO_ACTIVE_LOW= (1/0)
*         pActiveLow    -(out)  is active low (true) or high (false)
*
*\return  No return value. Function type is void.
*
*\par History:
* 17.12.2015 - Initial Version - Sadhna Sharma (RBEI/ECF5)
**********************************************************************/
static tVoid drv_LSIM_GPIO_TextIsActiveLow(const tPChar gpio_value,
                                                 tPBool pActiveLow)
{
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IsActiveLow,
                       "Enter:", (tU32)s32Pid, (tU32)0, (tU32)0, (tU32)0);
#endif

   if (pActiveLow != NULL)
   {
      /* Check active_low(true/false) from file*/
      *pActiveLow = (strcmp(gpio_value,STR_GPIO_ACTIVE_LOW)!=0) ? TRUE:FALSE;
   }
   
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_IsActiveLow,
                       "Exit:", (tU32)s32Pid, (tU32)0, (tU32)0, (tU32)0);
#endif

}

/************************************************************************/
/*!
*\fn      FILE* drv_LSIM_GPIO_TextFileOpen(const OSAL_tGPIODevID  iID)
*
*\brief   Open dts_tmp file, find pin with iID, 
*         and set fp to the next line of GPIO_LOGICAL_ID
*
*\param   iID   - (in)pin ID.
*
*\return  File pointer
*
*\par History:
* 17.12.2015 - Initial Version - Sadhna Sharma (RBEI/ECF5)
**********************************************************************/
static FILE* drv_LSIM_GPIO_TextFileOpen(const OSAL_tGPIODevID  iID)
{
   tChar gpio_logical_id[DTS_LINE_SIZE] = {0};
   tChar line[DTS_LINE_SIZE] = {0};
   FILE *fp = NULL;
   
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileOpen,
                       "Enter:", (tU32)s32Pid, (tU32)0, (tU32)0, (tU32)0);
#endif
   
   fp = fopen(DTS_TMP_FILE,FILE_OPEN_MODE);
   if (fp == NULL)
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileOpen,
                          "open file failed", (tU32)s32Pid, (tU32)errsv, (tU32)0, (tU32)0);
      NORMAL_M_ASSERT_ALWAYS();
      return fp;
   }
   rewind(fp);
   /* find pin with its LOGICAL_ID*/
   sprintf(gpio_logical_id,"%s%d",GPIO_LOGICAL_ID,iID);
   while(fgets(line,DTS_LINE_SIZE,fp) != NULL)
   {
     if (strstr(line,gpio_logical_id))
     {
        break;
     }
   }
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileOpen,
                       "Exit:", (tU32)s32Pid, (tU32)0, (tU32)0, (tU32)0);
#endif
   return fp;
}

/**********************************************************************************/
/*!
*\fn      tU32 drv_LSIM_GPIO_TextFileWriteStr(FILE *fp,const tPChar pStr)

*\brief   Write String to /tmp/dts_tmp.txt
*
*\param   fp  - (in) file pointer.
*         pStr- (in) string to write
*
*\return  OSAL error code
*
*\par History:
* 11.09.2015 - Initial Version - Rahana V S (RBEI/ECF5)
* 17.12.2015 - Adapted to have write only functionality - Sadhna Sharma (RBEI/ECF5)
***********************************************************************************/
static tU32 drv_LSIM_GPIO_TextFileWriteStr(FILE *fp,const tPChar pStr)
{
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileWriteStr,
                       "Enter:", (tU32)s32Pid, (tU32)0, (tU32)0, (tU32)0);
#endif  

   if (fputs(pStr,fp) == EOF)
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileWriteStr,
                          "write failed", (tU32)s32Pid, (tU32)errsv, (tU32)0, (tU32)0);
      DEV_GPIO_vTraceString(TR_LEVEL_FATAL, enDEV_GPIO_SysfsFileWriteStr, pStr);
      fclose(fp);
      fp = NULL;
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsFileWriteStr,
                       "Exit:", (tU32)s32Pid, (tU32)0, (tU32)0, (tU32)0);
#endif  

   return OSAL_E_NOERROR;
} 

#endif

/************************************************************************/
/*!
 *\fn       tVoid drv_GPIO_CheckLogicalIDs()
 *
 *\brief    Make sure that the logical IDs defined in osioctrl.h
 *          match with the ones defined in this driver
 *          ! keep the LogicalGpioNames table in sync !
 *
 *\par History:
 * 09.10.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
static tVoid drv_GPIO_CheckLogicalIDs(tVoid)
{
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_SPI_ADR_SPI1_CS           == 1);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_ADR_REQ              == 2);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_ADR_RESET            == 3);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_HW_MUTE              == 4);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_MUTE_ENABLE      == 5);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_TUN_PWR_ANT1         == 6);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMPOP_ON             == 7);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_TUN_PWR_ANT1_PCB     == 8);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_SPI_HIT_SPI_CS            == 9);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_HIT_REQ              == 10);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_HIT_RESET            == 11);
   NORMAL_M_ASSERT(OSAL_EN_CDDRIVE_RESET_GPIO                      == 12);
   NORMAL_M_ASSERT(OSAL_EN_DISPLAY_SETTINGS                        == 13);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO                                == 14);
   NORMAL_M_ASSERT(OSAL_EN_BACKLIGHT_SETTINGS                      == 15);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_HIT_PWRSUPPLY        == 16);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AUX_IN_DIAG_ON_LEFT  == 17);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AUX_IN_DIAG_ON_RIGHT == 18);
   NORMAL_M_ASSERT(OSAL_EN_CAP_GPIO_REQ                            == 19);
   NORMAL_M_ASSERT(OSAL_EN_CAP_GPIO_RESET                          == 20);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_OFFSET_DETECT    == 21);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_FRONT_STANDBY    == 22);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_REAR_STANDBY     == 23);
   NORMAL_M_ASSERT(OSAL_EN_PHONE_MUTE_GPIO                         == 24);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AUD_AMP_MUTE         == 25);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_AUD_AMP_ON           == 26);
   NORMAL_M_ASSERT(OSAL_EN_PWR_PHANTOM_MIC_ON                      == 27);
   NORMAL_M_ASSERT(OSAL_EN_EMBEDDEDRADIO_GPIO_TUN_PWR_ANT2         == 28);
   NORMAL_M_ASSERT(OSAL_EN_PWR_PHANTOM_XM_TUNER_ON                 == 29);
   NORMAL_M_ASSERT(OSAL_EN_SELECT_RC2                              == 30);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_FE_POWER_ON                        == 31);
   NORMAL_M_ASSERT(OSAL_EN_HC_GPIO_FAN_ON                          == 32);
   NORMAL_M_ASSERT(OSAL_EN_MIC_HW_POWER_CTRL                       == 33);
   NORMAL_M_ASSERT(OSAL_EN_MIC_SELECT_CTRL                         == 34);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_ANTENNA_ERROR_DETECT               == 35);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_ANTENNA_OPEN_DETECT                == 36);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_ANTENNA_SHORT_DETECT               == 37);
   NORMAL_M_ASSERT(OSAL_EN_GNSS_ANTENNA_SHDN                       == 38);
   NORMAL_M_ASSERT(OSAL_EN_MIC_DIAG1_ENABLE                        == 39);
   NORMAL_M_ASSERT(OSAL_EN_MIC_DIAG2_ENABLE                        == 40);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_INPUT_LOCAL_1                 == 41);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_INPUT_LOCAL_2                 == 42);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_INPUT_REMOTE_1                == 43);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_INPUT_REMOTE_2                == 44);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1                == 45);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_2                == 46);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_3                == 47);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1               == 48);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_2               == 49);
   NORMAL_M_ASSERT(OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_3               == 50);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO_WAKEUP_CAN                     == 51);
   NORMAL_M_ASSERT(OSAL_EN_CPU_RUN                                 == 52);
   NORMAL_M_ASSERT(OSAL_EN_PWR_UDROP_30                            == 53);
   NORMAL_M_ASSERT(OSAL_EN_SCC_RSTWARN_CPU                         == 54);
   NORMAL_M_ASSERT(OSAL_EN_CPU_PWR_OFF                             == 55);
   NORMAL_M_ASSERT(OSAL_EN_DAB_ANTENNA_ERROR_DETECT                == 56);
   NORMAL_M_ASSERT(OSAL_EN_FMAM_ANTENNA_ERROR_DETECT               == 57);
   NORMAL_M_ASSERT(OSAL_EN_MUTE_IN_ECALL                           == 58);
   NORMAL_M_ASSERT(OSAL_EN_MUTE_IN_VDA                             == 59);
   NORMAL_M_ASSERT(OSAL_EN_DAB_ANTENNA_SHDN                        == 60);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO_ON_TIPPER                      == 61);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO_WAKEUP_CD                      == 62);
   NORMAL_M_ASSERT(OSAL_EN_SPM_GPIO_CD_HW_EJECT                    == 63);
   NORMAL_M_ASSERT(OSAL_EN_PWR_UDROP_60                            == 64);
   NORMAL_M_ASSERT(OSAL_EN_PWR_UDROP_90                            == 65);
   NORMAL_M_ASSERT(OSAL_EN_MAX16946_SHDN                           == 66);
   NORMAL_M_ASSERT(OSAL_EN_MAX16946_SC                             == 67);
   NORMAL_M_ASSERT(OSAL_EN_MAX16946_OL                             == 68);
   NORMAL_M_ASSERT(OSAL_EN_TLF4277_EN                              == 69);
   NORMAL_M_ASSERT(OSAL_EN_TLF4277_ERROR                           == 70);
   NORMAL_M_ASSERT(OSAL_EN_ELMOS52240_EN_1                         == 71);
   NORMAL_M_ASSERT(OSAL_EN_ELMOS52240_NFLT_1                       == 72);
   NORMAL_M_ASSERT(OSAL_EN_ELMOS52240_EN_2                         == 73);
   NORMAL_M_ASSERT(OSAL_EN_ELMOS52240_NFLT_2                       == 74);
   NORMAL_M_ASSERT(OSAL_EN_REVERSE_DETECT                          == 75);
   NORMAL_M_ASSERT(OSAL_EN_JACK_DETECT                             == 76);
   NORMAL_M_ASSERT(OSAL_EN_PKB_DETECT                              == 77);
   NORMAL_M_ASSERT(OSAL_EN_MIC_DETECT                              == 78);
   NORMAL_M_ASSERT(OSAL_EN_CAMERA_DETECT                           == 79);
   NORMAL_M_ASSERT(OSAL_EN_PWR_RVC_SHDN                            == 80);
   NORMAL_M_ASSERT(OSAL_EN_CPU_RST_SCC                             == 81);
   NORMAL_M_ASSERT(OSAL_EN_CPU_RSTWARN_SCC                         == 82);
   NORMAL_M_ASSERT(OSAL_EN_TLF4277_EN_2                            == 83);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_TLF4277_ERROR_2                         == 84);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_DEBUG_WD_OFF                            == 85);
   NORMAL_M_ASSERT(OSAL_EN_MOST_DIAG_ECL_STATUS                    == 86);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_PWR_CDC                                 == 87);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_PWR_DISPLAY                             == 88);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_U140_SW_ENABLE	                      	 == 89);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_U140_SW_DIAG                            == 90);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_ACC_DETECT	                      	    == 91);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_HF_VR_MODE	                      	    == 92);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_MIC_CAM		                      	    == 93);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_TEL_MODE_AMP		                      	== 94);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_CPU_RST_BT		                      	== 95);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_CPU_RST_WL		                      	== 96);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_ILLUMINATION_DETECT                     	== 97);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_IGNITION_DETECT	                      	== 98);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_FAREWELL_DETECT	                      	== 99);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_GALA_DETECT                      	    == 100);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_MR_OUT_DETECT	                      	== 101);/*lint !e641*//*lint !e506*/ 
   NORMAL_M_ASSERT(OSAL_EN_DETECT_PLUG_1                      	    == 102);/*lint !e641*//*lint !e506*/
   NORMAL_M_ASSERT(OSAL_EN_DETECT_PLUG_DIAG_1	                    == 103);/*lint !e641*//*lint !e506*/      
   NORMAL_M_ASSERT(OSAL_EN_OUTPUT_SPARE_3	                        == 104);/*lint !e641*//*lint !e506*/ 
   NORMAL_M_ASSERT(OSAL_EN_ELMOS52240_NFLT_3                        == 105);/*lint !e641*//*lint !e506*/ 
   NORMAL_M_ASSERT(OSAL_EN_GPIOPINS_LASTENTRY                       == 106);	
}

/************************************************************************/
/*!
*\fn       tVoid DEV_GPIO_vTraceString(TR_tenTraceLevel enTraceLevel,
                           enDevGpioTraceFunction enFunction,
                           tPCChar copchDescription)

*\brief    Function to print GPIO string trace messages on console IO
*
*          To print trace messages on console IO GPIO driver call this
*          function at nessessary places.
*
*\param    TR_tenTraceLevel [IN] - To opt trace level
*\param    enFunction [IN] - Current GPIO Funtion name
*\param    copchDescription [IN] - char string which prints on console IO
*
*\return  No return value. Function type is void.
*
*\par History:
*
**********************************************************************/
tVoid DEV_GPIO_vTraceString(TR_tenTraceLevel enTraceLevel,
                            enDevGpioTraceFunction enFunction,
                            tPCChar copchDescription)
{
  tU32 u32ThreadID;
  tU8 u8StringLength;

  tU8 au8TraceBuf[3 * sizeof(tU8) + 80 * sizeof(tChar) + 1 * sizeof(tU32)];

  /* Trace Level enabled ? */
  if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_GPIO, enTraceLevel))
  {
    /* calculate the given string length */
    u8StringLength = (tU8)OSAL_u32StringLength(copchDescription);

    /*string lentgh greater than reserved space ? */
    if (u8StringLength > 80)
    {
      /* set string length to maximum allowed value */
      u8StringLength = 80;
    }
    /* fill trace buffer with desired values */
    u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
    OSAL_M_INSERT_T8(&au8TraceBuf[0], (tU8) GPIO_TRC_FN_STRING);
    OSAL_M_INSERT_T32(&au8TraceBuf[1], u32ThreadID);
    OSAL_M_INSERT_T8(&au8TraceBuf[5], (tU8) enFunction);

    /* copy given string into the trace buffer */
    (tVoid)OSAL_szStringNCopy((tString) &au8TraceBuf[6], copchDescription,
          u8StringLength);
    /* set string end \0 */
    au8TraceBuf[u8StringLength+6] = 0;

    /* send gpio trace to IO-console */
    LLD_vTrace(OSAL_C_TR_CLASS_DEV_GPIO, enTraceLevel,
          au8TraceBuf, u8StringLength+7);
  }
}

/************************************************************************/
/*!
*\fn       tVoid DEV_GPIO_vTraceInfo(TR_tenTraceLevel enTraceLevel,
                           enDevGpioTraceFunction enFunction,
                           tPCChar copchDescription,
                           tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
                           tU32 u32Par4)

*\brief    Function to print a GPIO short string and 4 parameters as one
*          trace messges on console IO
*
*          To print trace messages on console IO GPIO driver call this
*          function at nessessary places.
*
*\param    TR_tenTraceLevel [IN] - To opt trace level
*\param    enFunction [IN] - Current GPIO Funtion name
*\param    copchDescription [IN] - char string which prints on console IO
*\param    u32Par1 [OUT] - Output value from the GPIO funciton
*\param    u32Par2 [OUT] - Output value from the GPIO funciton
*\param    u32Par3 [OUT] - Output value from the GPIO funciton
*\param    u32Par4 [OUT] - Output value from the GPIO funciton
*
*\return  No return value. Function type is void.
*
*\par History:
*
**********************************************************************/
tVoid DEV_GPIO_vTraceInfo(TR_tenTraceLevel enTraceLevel,
                         enDevGpioTraceFunction enFunction,
                         tPCChar copchDescription,
                         tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
  tChar ch;
  tInt i;

  tU32 u32ThreadID;
  tU8 u8StringLength;

  tU8 au8Buf[3 * sizeof(tU8) + 16 * sizeof(tChar) + 5 * sizeof(tU32)];

  /* Trace Level enabled ? */
  if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_GPIO,enTraceLevel))
  {
    /* calculate the given string length */
    u8StringLength = (tU8)OSAL_u32StringLength(copchDescription);

    /*string lentgh greater than reserved space ? */
    if (u8StringLength > 16)
    {
      /* set string length to maximum allowed value */
      u8StringLength = 16;
    }
    /* fill trace buffer with desired values */
    u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
    OSAL_M_INSERT_T8(&au8Buf[0], (tU8) GPIO_TRC_FN_INFO);
    OSAL_M_INSERT_T32(&au8Buf[1], u32ThreadID);
    OSAL_M_INSERT_T8(&au8Buf[5], (tU8) enFunction);

    /* copy given string into the trace buffer */
    for (i = 0; i < u8StringLength; ++i)
    {
       ch = copchDescription[i];
       OSAL_M_INSERT_T8(&au8Buf[6+i], (tU8) ch);
       if (ch == '\0') break;
    }
    /* set string end \0 */
    OSAL_M_INSERT_T8(&au8Buf[6+i], (tU8) '\0');
    /* copy par1 to par4 into the trace buffer */
    OSAL_M_INSERT_T32(&au8Buf[23], u32Par1);
    OSAL_M_INSERT_T32(&au8Buf[27], u32Par2);
    OSAL_M_INSERT_T32(&au8Buf[31], u32Par3);
    OSAL_M_INSERT_T32(&au8Buf[35], u32Par4);

    /* send gpio trace to IO-console */
    LLD_vTrace(OSAL_C_TR_CLASS_DEV_GPIO, enTraceLevel,
          au8Buf, sizeof(au8Buf));
  }
}

// The line below is needed at the end of the file for doxygen
/*! @}*/
