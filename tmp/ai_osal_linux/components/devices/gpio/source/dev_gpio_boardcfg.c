/***************************************************************************
*****     (C) COPYRIGHT Robert Bosch GmbH CM-DI - All Rights Reserved     **
******************************************************************************/
/*! 
***     @file        dev_gpio_boardcfg.c
***     @Authors
\n                   Matthias Thomae (CM-AI/PJ-CF31)
***
***     @brief       GPIO ID mapping using Board Configuration (Gen3 Linux)
***
***
***     @warning     
***
***     @todo        
***
\par  VERSION HISTORY:
**  $Log:
*  14.08.2012 - Initial version - Matthias Thomae (CM-AI/PJ-CF31)
*  07.01.2013 - Sync test pins with osioctrl.h - Matthias Thomae (CM-AI/PJ-CF31)
*  07.05.2014 - Added GPIO PIN for GNSS ANT - Nikhil Ravindran (RBEI/ECF5)
*  22.07.2014 - Added GPIO PIN for GMMY16 - Nikhil Ravindran (RBEI/ECF5)
*  12.02.2015 - Added GPIO PIN for PSA B2-B3 - Nikhil Ravindran (RBEI/ECF5)
*  18.11.2015 - Added GPIO PIN for A-IVI - Nikhil Ravindran (RBEI/ECF5)
*  29.02.2016 - Added GPIO PIN for CAF - Nikhil Ravindran (RBEI/ECF5)
*  15.06.2016 - Added GPIO PIN for A-IVI - Ajay Vishwanath Bande (RBEI/ECF5)
*  29.07.2016 - Added GPIO PIN for A-IVI - Nikhil Ravindran (RBEI/ECF5)
*  12.09.2016 - Added GPIO PIN for A-IVI - Nikhil Ravindran (RBEI/ECF5)
*/

/***************************************************************************/

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/

#include <errno.h>

/* OSAL Device header */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "dev_gpio.h"
#include <sys/types.h>
#include <unistd.h>

/*****************************************************************
| defines and macros (scope: modul-local)
|----------------------------------------------------------------*/

#define SYSFS_PATH_BOARDCFG     "/sys/devices/virtual/boardcfg/boardcfg"
#define GPIO_NOF_PINS_PER_BANK  32

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: modul-local)
|----------------------------------------------------------------*/

/*
 * keep the following table in sync with OSAL_enGpioPins in osioctrl.h
 * and with the drv_GPIO_CheckLogicalIDs() function in dev_gpio.c
 */
static const tChar* LogicalGpioNames[] =
      {
         /* No OSAL GPIO defined for index 0 */
         "",
         /* OSAL_EN_EMBEDDEDRADIO_SPI_ADR_SPI1_CS = 1 */
         "embeddedradio-spi-adr-spi1-cs",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_ADR_REQ = 2 */
         "embeddedradio-gpio-adr-req",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_ADR_RESET = 3 */
         "embeddedradio-gpio-adr-reset",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_HW_MUTE = 4 */
         "embeddedradio-gpio-hw-mute",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_MUTE_ENABLE = 5 */
         "embeddedradio-gpio-amp-mute-enable",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_TUN_PWR_ANT1 = 6 */
         "embeddedradio-gpio-tun-pwr-ant1",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_AMPOP_ON = 7 */
         "embeddedradio-gpio-ampop-on",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_TUN_PWR_ANT1_PCB = 8 */
         "embeddedradio-gpio-tun-pwr-ant1-pcb",
         /* OSAL_EN_EMBEDDEDRADIO_SPI_HIT_SPI_CS = 9 */
         "embeddedradio-spi-hit-spi-cs",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_HIT_REQ = 10 */
         "embeddedradio-gpio-hit-req",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_HIT_RESET = 11 */
         "embeddedradio-gpio-hit-reset",
         /* OSAL_EN_CDDRIVE_RESET_GPIO = 12 */
         "cddrive-reset-gpio",
         /* OSAL_EN_DISPLAY_SETTINGS = 13 */
         "display-settings",
         /* OSAL_EN_SPM_GPIO = 14 */
         "spm-gpio",
         /* OSAL_EN_BACKLIGHT_SETTINGS = 15 */
         "backlight-settings",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_HIT_PWRSUPPLY = 16 */
         "embeddedradio-gpio-hit-pwrsupply",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_AUX_IN_DIAG_ON_LEFT = 17 */
         "embeddedradio-gpio-aux-in-diag-on-left",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_AUX_IN_DIAG_ON_RIGHT = 18 */
         "embeddedradio-gpio-aux-in-diag-on-right",
         /* OSAL_EN_CAP_GPIO_REQ = 19 */
         "cap-gpio-req",
         /* OSAL_EN_CAP_GPIO_RESET = 20 */
         "cap-gpio-reset",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_OFFSET_DETECT = 21 */
         "embeddedradio-gpio-amp-offset-detect",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_FRONT_STANDBY = 22 */
         "embeddedradio-gpio-amp-front-standby",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_AMP_REAR_STANDBY = 23 */
         "embeddedradio-gpio-amp-rear-standby",
         /* OSAL_EN_PHONE_MUTE_GPIO = 24 */
         "phone-mute-gpio",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_AUD_AMP_MUTE = 25 */
         "embeddedradio-gpio-aud-amp-mute",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_AUD_AMP_ON = 26 */
         "embeddedradio-gpio-aud-amp-on",
         /* OSAL_EN_PWR_PHANTOM_MIC_ON = 27 */
         "pwr-phantom-mic-on",
         /* OSAL_EN_EMBEDDEDRADIO_GPIO_TUN_PWR_ANT2 = 28 */
         "embeddedradio-gpio-tun-pwr-ant2",
         /* OSAL_EN_PWR_PHANTOM_XM_TUNER_ON = 29 */
         "pwr-phantom-xm-tuner-on",
         /* OSAL_EN_SELECT_RC2 = 30 */
         "select-rc2",
         /* OSAL_EN_GNSS_FE_POWER_ON = 31 */
         "gnss-fe-power-on",
         /* OSAL_EN_HC_GPIO_FAN_ON = 32 */
         "hc-gpio-fan-on",
         /* OSAL_EN_MIC_HW_POWER_CTRL = 33 */
         "mic-hw-power-ctrl",
         /* OSAL_EN_MIC_SELECT_CTRL = 34 */
         "mic-select-ctrl",
         /* OSAL_EN_GNSS_ANTENNA_ERROR_DETECT = 35 */
         "gnss-antenna-error-detect",
         /* OSAL_EN_GNSS_ANTENNA_OPEN_DETECT = 36 */
         "gnss-antenna-open-detect",
         /* OSAL_EN_GNSS_ANTENNA_SHORT_DETECT = 37 */
         "gnss-antenna-short-detect",
         /* OSAL_EN_GNSS_ANTENNA_SHDN = 38 */
         "gnss-antenna-shdn",
         /* OSAL_EN_MIC_DIAG1_ENABLE = 39 */
         "mic-diag1-enable",
         /* OSAL_EN_MIC_DIAG2_ENABLE = 40 */
         "mic-diag2-enable",
         /* OSAL_EN_TEST_GPIO_INPUT_LOCAL_1 = 41 */
         "test-gpio-input-local-1",
         /* OSAL_EN_TEST_GPIO_INPUT_LOCAL_2 = 42 */
         "test-gpio-input-local-2",
         /* OSAL_EN_TEST_GPIO_INPUT_REMOTE_1 = 43 */
         "test-gpio-input-remote-1",
         /* OSAL_EN_TEST_GPIO_INPUT_REMOTE_2 = 44 */
         "test-gpio-input-remote-2",
         /* OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_1 = 45 */
         "test-gpio-output-local-1",
         /* OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_2 = 46 */
         "test-gpio-output-local-2",
         /* OSAL_EN_TEST_GPIO_OUTPUT_LOCAL_3 = 47 */
         "test-gpio-output-local-3",
         /* OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_1 = 48 */
         "test-gpio-output-remote-1",
         /* OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_2 = 49 */
         "test-gpio-output-remote-2",
         /* OSAL_EN_TEST_GPIO_OUTPUT_REMOTE_3 = 50 */
         "test-gpio-output-remote-3",
         /* OSAL_EN_SPM_GPIO_WAKEUP_CAN = 51 */
         "spm-gpio-wakeup-can",
         /* OSAL_EN_CPU_RUN = 52 */
         "cpu-run",
         /* OSAL_EN_PWR_UDROP_30 = 53 */
         "pwr-udrop-30",
         /* OSAL_EN_SCC_RSTWARN_CPU = 54 */
         "scc-rstwarn-cpu",
         /* OSAL_EN_CPU_PWR_OFF = 55 */
         "cpu-pwr-off",
         /* OSAL_EN_DAB_ANTENNA_ERROR_DETECT = 56 */
         "dab-antenna-error-detect",
         /* OSAL_EN_FMAM_ANTENNA_ERROR_DETECT = 57 */
         "fmam-antenna-error-detect",
         /* OSAL_EN_MUTE_IN_ECALL = 58 */
         "mute-in-ecall",
         /* OSAL_EN_MUTE_IN_VDA = 59 */
         "mute-in-vda",
         /* OSAL_EN_DAB_ANTENNA_SHDN = 60 */
         "dab-antenna-shdn",
         /* OSAL_EN_SPM_GPIO_ON_TIPPER = 61 */
         "spm-gpio-on-tipper",
         /* OSAL_EN_SPM_GPIO_WAKEUP_CD = 62 */
         "spm-gpio-wakeup-cd",
         /* OSAL_EN_SPM_GPIO_CD_HW_EJECT = 63 */
         "spm-gpio-cd-hw-eject",
         /* OSAL_EN_PWR_UDROP_60 = 64 */
         "pwr-udrop-60",
         /* OSAL_EN_PWR_UDROP_90 = 65 */
         "pwr-udrop-90",
         /* OSAL_EN_MAX16946_SHDN = 66 */
         "max16946-shdn",
         /* OSAL_EN_MAX16946_SC = 67 */
         "max16946-sc",
         /* OSAL_EN_MAX16946_OL = 68 */
         "max16946-ol",
         /* OSAL_EN_TLF4277_EN = 69 */
         "tlf4277-en",
         /* OSAL_EN_TLF4277_ERROR = 70 */
         "tlf4277-error",
         /* OSAL_EN_ELMOS52240_EN_1 = 71 */
         "elmos52240-en-1",
         /* OSAL_EN_ELMOS52240_NFLT_1 = 72 */
         "elmos52240-nflt-1",
         /* OSAL_EN_ELMOS52240_EN_2 = 73 */
         "elmos52240-en-2",
         /* OSAL_EN_ELMOS52240_NFLT_2 = 74 */
         "elmos52240-nflt-2",
         /* OSAL_EN_REVERSE_DETECT = 75 */
         "reverse-detect",
         /* OSAL_EN_JACK_DETECT = 76 */
         "jack-detect",
         /* OSAL_EN_PKB_DETECT = 77 */
         "pkb-detect",
         /* OSAL_EN_MIC_DETECT = 78 */
         "mic-detect",
         /* OSAL_EN_CAMERA_DETECT = 79 */
         "camera-detect",
         /* OSAL_EN_PWR_RVC_SHDN = 80 */
         "pwr-rvc-shdn",
         /* OSAL_EN_CPU_RST_SCC = 81 */
         "cpu-rst-scc",
         /* OSAL_EN_CPU_RSTWARN_SCC = 82 */
         "cpu-rstwarn-scc",
         /* OSAL_EN_TLF4277_EN_2 = 83 */
         "tlf4277-en-2",
         /* OSAL_EN_TLF4277_ERROR_2 = 84 */
         "tlf4277-error-2",
         /* OSAL_EN_DEBUG_WD_OFF = 85*/
         "debug-wd-off",
         /* OSAL_EN_MOST_DIAG_ECL_STATUS = 86*/
         "most-diag-ecl-status",
         /* OSAL_EN_PWR_CDC = 87*/
         "pwr-cdc-en",
         /* OSAL_EN_PWR_DISPLAY = 88*/
         "pwr-display-en",
         /* OSAL_EN_U140_SW_ENABLE = 89*/
         "u140-sw-enable",
         /* OSAL_EN_U140_SW_DIAG = 90*/
         "u140-sw-diag",
         /* OSAL_EN_ACC_DETECT = 91*/
         "acc-detect",
         /* OSAL_EN_HF_VR_MODE = 92*/
         "hf-vr-mode",
        /* OSAL_EN_MIC_CAM = 93*/
         "mic-cam-en",
        /* OSAL_EN_TEL_MODE_AMP = 94*/
         "tel-mode-amp", 
        /* OSAL_EN_CPU_RST_BT = 95*/
         "cpu-rst-bt", 
        /* OSAL_EN_CPU_RST_WL = 96*/
         "cpu-rst-wl",
        /* OSAL_EN_ILLUMINATION_DETECT = 97*/
         "illumination-detect", 
        /* OSAL_EN_IGNITION_DETECT = 98*/
         "ignition-detect",
        /* OSAL_EN_FAREWELL_DETECT = 99*/
         "farewell-detect",	 
        /* OSAL_EN_GALA_DETECT = 100*/
         "gala-detect",	 
        /* OSAL_EN_MR_OUT_DETECT = 101*/
         "mr-output-detect",	
        /* OSAL_EN_DETECT_PLUG_1 = 102*/
         "detect-plug-1",	 
        /* OSAL_EN_DETECT_PLUG_DIAG_1 = 103*/
         "detect-plug-1-diag",
        /* OSAL_EN_OUTPUT_SPARE_3 = 104*/
         "o-spare3",
         /* OSAL_EN_ELMOS52240_NFLT_3 = 105 */
         "elmos52240-nflt-3"		 
      };

/*****************************************************************
| function prototype (scope: modul-local)
|----------------------------------------------------------------*/

tBool drv_GPIO_ExistsPath(const tPChar path);

tVoid DEV_GPIO_vTraceString(TR_tenTraceLevel enTraceLevel,
                            enDevGpioTraceFunction enFunction,
                            tPCChar copchDescription);

tVoid DEV_GPIO_vTraceInfo(TR_tenTraceLevel enTraceLevel,
                          enDevGpioTraceFunction enFunction,
                          tPCChar copchDescription,
                          tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
                          tU32 u32Par4);

/*****************************************************************
| function implementation (scope: modul-local)
|----------------------------------------------------------------*/

/************************************************************************/
/*!
 *\fn       tU32 drv_GPIO_SysfsGetPath(tDrvGpioDevID iID,
 *                                     tPChar        path)
 *
 *\brief    Get Sysfs path of GPIO pin - using boardcfg
 *          and export pin to user space if needed
 *          the caller is responsible for allocating the path buffer
 *          of size MAX_SYSFS_PATH_LEN
 *
 *\param    iID  - (in)  pin identifier
 *          path - (out) path buffer
 *
 *\return   OSAL error code
 *
 *\par History:
 * 08.08.2012 - Matthias Thomae (CM-AI/PJ-CF31) initial version
 **********************************************************************/
tU32 drv_GPIO_SysfsGetPath(const tDrvGpioDevID iID,
                           tPChar              path)
{
   tInt len = 0;

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsGetPath,
                       "Enter:", getpid(), iID, 0, 0);
#endif

   if (iID == 0 || iID > GPIO_NOF_PINS_IDS)
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsGetPath,
                          "invalid ID", getpid(), iID, 0, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }

   if (iID < OSAL_EN_GPIOPINS_LASTENTRY)
   {
      /* logical ID */
      len = snprintf(path, MAX_SYSFS_PATH_LEN, "%s/%s",
            SYSFS_PATH_BOARDCFG, LogicalGpioNames[iID]);
   }
   else
   {
      /* hardware ID */
      len = snprintf(path, MAX_SYSFS_PATH_LEN, "%s/gpio-b%lu-p%lu",
            SYSFS_PATH_BOARDCFG,
            (iID - OSAL_EN_GPIOPINS_LASTENTRY) / GPIO_NOF_PINS_PER_BANK,
            (iID - OSAL_EN_GPIOPINS_LASTENTRY) % GPIO_NOF_PINS_PER_BANK);
   }

   if (len <= 0 || len >= MAX_SYSFS_PATH_LEN)
   {
      tInt errsv = errno;
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsGetPath,
                          "gen path failed", getpid(), errsv, len, 0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   /* check if path exists */
   if (!drv_GPIO_ExistsPath(path))
   {
      DEV_GPIO_vTraceInfo(TR_LEVEL_FATAL, enDEV_GPIO_SysfsGetPath,
                          "path not exist", getpid(), iID, 0, 0);
      DEV_GPIO_vTraceString(TR_LEVEL_FATAL, enDEV_GPIO_SysfsGetPath, path);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_DOESNOTEXIST;
   }

#ifdef GPIO_VERBOSE_TRACE
   DEV_GPIO_vTraceInfo(TR_LEVEL_USER_4, enDEV_GPIO_SysfsGetPath,
                       "Exit:", getpid(), iID, 0, 0);
#endif

   return OSAL_E_NOERROR;
}

// The line below is needed at the end of the file for doxygen
/*! @}*/

