/************************************************************************
 | FILE:         cd_cache.c
 | PROJECT:      GEN3
 | SW-COMPONENT: Cd driver
 |------------------------------------------------------------------------
 |************************************************************************/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cd_cache.c
 * @brief   information about inserted cd
 * @author  srt2hi
 * @date
 * @version
 * @note
 ******************************************************************************
 * @history  
 * 16 Mar,2016 | Bug Fix NCG3D-11114 : 
 *               Issue: CD Driver version changes for every CD operation which should be constant
 *               Reason: For HW and SW version valid bytes (33,34 and 35,36) should be considered. 
 *                       But here 57 and 58 bytes are considered which are not valid.
 *               Fix:    33,34 and 35,36 bytes are collected for SW and HW version instead of 57 and 58.
 *               Author: Kranthi Kiran (RBEI/ECF5)
 *//* ***************************************************FileHeaderEnd******* */

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <scsi/sg.h> /* take care: fetches glibc's /usr/include/scsi/sg.h */

/* Basic OSAL includes */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"
#include "cd_funcenum.h"
#include "cd_main.h"
#include "cd_cache.h"
#include "cd_trace.h"
#include "cd_scsi_if.h"

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: module-local)
 |-----------------------------------------------------------------------*/
//#define CD_MS2SECSONDS(_MIN_,_SEC_) ((((tInt)(_MIN_) * 60) + (tInt)(_SEC_)))
//sector number (ZLBA!) for reading FS-TYPE
#define CD_FS_SECTOR_LBA (16)

#define CD_C8_UPPERCASE(c)  ((c < 0x7B) ? ((c >= 0x61) ? (c - 0x20) : c) : c)

//ISO-volume descriptor types
#define CD_C_U8_ISO_STD_ID_LENGTH     5
#define CD_C_SZ_ISO_STD_IDENTIFIER    "CD001"
#define CD_C_U8_UDFS_STD_ID_LENGTH    5
#define CD_C_SZ_UDFS_STD_IDENTIFIER   "BEA01"
#define CD_C_C8_SEPARATOR2            0x3B      /*';'*/

#define CD_C_U8_LOADERINFO_UNKNOWN    0xFF

/************************************************************************
 |typedefs (scope: module-local)
 |----------------------------------------------------------------------*/

/************************************************************************
 | variable inclusion  (scope: global)
 |-----------------------------------------------------------------------*/
//extern tBool CD_bTraceOn;
/************************************************************************
 | variable definition (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: module-local)
 |-----------------------------------------------------------------------*/
//static CD_sCD_type garCD[CD_DRIVE_COUNT] = {0};
//used for reading FS-Type
static tU8 gau8Sector[CD_USER_BYTES_PER_SECTOR];

/************************************************************************
 |function prototype (scope: module-local)
 |-----------------------------------------------------------------------*/
static tU32 u32ZLBA2Sec(tU32 u32ZLBA);
static void vSec2MSF(tDevCDData *pShMem, tU32 u32Seconds, CD_sTMSF_type *pTMSF);
static void vZLBA2MSF(tDevCDData *pShMem, tU32 u32ZLBA, CD_sMSF_type *pMSF);
static tInt iPlaytimeInWhichTrack(tDevCDData *pShMem, tU32 u32AbsTimeSec,
                                  tInt iT0, tInt iT1);

/************************************************************************
 |function implementation (scope: module-local)
 |-----------------------------------------------------------------------*/

/*****************************************************************************
 * FUNCTION:     s16CompIdentifier
 * PARAMETER:    pu8IdentifierField (->I) = Volume Descriptor->standard_id_field
 *               pu8Identifier      (->I) = Identification string
 *               u8IdLength          ( I) = Identifier length
 * RETURNVALUE  : 0 if pu8IdentifierField == pu8Identifier
 *               differnce between first different char otherwise
 * DESCRIPTION:  Check if input string are equal
 ******************************************************************************/

static tS16 s16CompIdentifier(const tU8 *pu8IdentifierField,
                              const tU8 *pu8Identifier, tU8 u8IdLength)
{
  tS16 i;
  for(i = 0;
     (i < u8IdLength) && (CD_C8_UPPERCASE(pu8IdentifierField[i])
                          == CD_C8_UPPERCASE(pu8Identifier[i])); i++)
  {
    ;
  }

  /* lib functions need char, and not signed or unsigned char* */

  if(!(i && (i == (tS16)strlen((char *)pu8Identifier))
       && ((i == u8IdLength) || (pu8IdentifierField[i] == CD_C_C8_SEPARATOR2))))
  {
    if(i >= u8IdLength) /* identifier field not necessarily NULL terminated! */
    {
      /* pretend that identifier field was NULL terminated */
      return(0 - CD_C8_UPPERCASE(pu8Identifier[i]));
    }

    return( CD_C8_UPPERCASE(pu8IdentifierField[i])
            - CD_C8_UPPERCASE(pu8Identifier[i]));
  }
  return 0;
}

/*****************************************************************************
 * FUNCTION:     u32UpdateTOC
 * PARAMETER:    Drive Index
 * RETURNVALUE:  OSAL error code
 * DESCRIPTION:  rad toc from Device if necessary
 ******************************************************************************/
static tU32 u32UpdateTOC(tDevCDData *pShMem)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  tU8 u8Valid;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U4(CDID_u32UpdateTOC, 0, 0, 0, 0);

  u8Valid = pShMem->rCD.rTOC.u8Valid;

  if(CD_VALID != u8Valid)
  { // read toc from ATA
    u32Ret = CD_SCSI_IF_u32ReadTOC(pShMem, &(pShMem->rCD.rTOC));
  } //f(CD_VALID != u8Valid)
  CD_TRACE_LEAVE_U4(CDID_u32UpdateTOC, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     vZLBA2MSF
 * PARAMETER:    IN: ZLBA
 *               OUT: Pointer to struct MSF
 * RETURNVALUE:  void, MSF
 * DESCRIPTION:  calculates MSF from ZLBA
 ******************************************************************************/
static void vZLBA2MSF(tDevCDData *pShMem, tU32 u32ZLBA, CD_sMSF_type *pMSF)
{
  tU32 u32Seconds;

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  CD_TRACE_ENTER_U4(CDID_vZLBA2MSF, u32ZLBA, pMSF, 0, 0);

  u32Seconds = (u32ZLBA / CD_SECTORS_PER_SECOND);

  if(NULL != pMSF)
  {
    pMSF->u8Min = (tU8)(u32Seconds / 60);
    pMSF->u8Sec = (tU8)(u32Seconds % 60);
    pMSF->u8Frm = (tU8)(u32ZLBA % CD_SECTORS_PER_SECOND);

    CD_TRACE_LEAVE_U4(CDID_vZLBA2MSF, OSAL_E_NOERROR, u32ZLBA, pMSF->u8Sec,
                      pMSF->u8Frm, pMSF->u8Frm);
  }
  else  //if(NULL != pMSF)
  {
    CD_TRACE_LEAVE_U4(CDID_vZLBA2MSF, OSAL_E_INVALIDVALUE, u32ZLBA, 0, 0, 0);
  } //else  //if(NULL != pMSF)
}

/************************************************************************
 |function implementation (scope: global)
 |-----------------------------------------------------------------------*/
/*****************************************************************************
 * FUNCTION:     CD_vCacheClear
 * PARAMETER:    drive index
 *               bFullClear = TRUE: reset entire cache to startup values
 *               bFullClear = FALSE: reset only Media-Part (after CD-Eject)
 * RETURNVALUE:  void
 * DESCRIPTION:  deletes all cached information about a cd
 * HISTORY: 
 * 16 Mar,2016 - Initialzie au8HWVersion2 and au8SWVersion2 arrays to 0 | Kranthi Kiran(RBEI/ECF5)
 ************************************************************************************************/
tVoid CD_vCacheClear(tDevCDData *pShMem, OSAL_tSemHandle hSem, tBool bFullClear)
{
  tInt iT;

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  CD_TRACE_ENTER_U4(CDID_CD_vCacheClear, 0, bFullClear, hSem, 0);

  CD_SM_LOCK(hSem);

  if(bFullClear)
  { // clear device depended part
    pShMem->rCD.rInquiry.u8Valid = CD_INVALID;
    pShMem->rCD.rInquiry.au8HWVersion2[0] = 0;
    pShMem->rCD.rInquiry.au8SWVersion2[0] = 0;
    pShMem->rCD.rInquiry.au8Product16[0] = 0;
    pShMem->rCD.rInquiry.au8Revision4[0] = 0;
    pShMem->rCD.rInquiry.au8Vendor8[0] = 0;
    pShMem->rCD.rInquiry.au8VendorSpecific20[0] = 0;

    pShMem->rCD.u8LoaderInfo = CD_C_U8_LOADERINFO_UNKNOWN;
  } //if(bFullClear)

  //media depended part
  pShMem->rCD.u8InternalMediaType = CD_INTERNAL_MEDIA_TYPE_UNKNOWN_MEDIA;
  pShMem->rCD.u8InternalMediaTypeValid = CD_INVALID;
  pShMem->rCD.u8FSType = OSAL_C_U8_FS_TYPE_UNKNOWN;
  pShMem->rCD.u8FSTypeValid = CD_INVALID;

  pShMem->rCD.u8ForcedMixedModeMediaType = CD_INTERNAL_MEDIA_TYPE_AUDIO_MEDIA;

  memset(&(pShMem->rCD.rTOC), 0, sizeof(CD_sTOC_type));
    pShMem->rCD.rTOC.u8Valid = CD_INVALID;
  //pShMem->rCD.rTOC.u32LastLBA = 0;
  pShMem->rCD.rTOC.u8MinTrack = 0;
  pShMem->rCD.rTOC.u8MaxTrack = 0;
  pShMem->rCD.rTOC.u32LastZLBA = 0;
  pShMem->rCD.rTOC.u8Valid = CD_INVALID;

  for(iT = 0; iT < CD_TRACK_ARRAY_SIZE; iT++)
  {
    pShMem->rCD.rTOC.arTrack[iT].u32StartZLBA = 0;
    pShMem->rCD.rTOC.arTrack[iT].u8AdrCtrl = 0;
  } //for(iT = 0; iT < CD_TRACK_ARRAY_SIZE; iT++)

  CD_SM_UNLOCK(hSem);

  CD_TRACE_LEAVE_U4(CDID_CD_vCacheClear, 0, 0, 0, 0, 0);
}

/*****************************************************************************
 * FUNCTION:     CD_vCacheDebug
 * PARAMETER:    drive index
 * RETURNVALUE:  void
 * DESCRIPTION:  shows all cached information about a cd
 ******************************************************************************/
tVoid CD_vCacheDebug(tDevCDData *pShMem)
{
  tInt iT;
  tU32 u32StartZLBA; // absolute start of track
  tU32 u32EndZLBA;   // absolute end of track
  tU32 u32TLenLBA;   // play time of track in Sectors
  CD_sMSF_type rMSF;
  CD_sMSF_type rStartMSF;
  CD_sMSF_type rLenMSF;

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  CD_TRACE_ENTER_U4(CDID_CD_vCacheDebug, 0, 0, 0, 0);

  vZLBA2MSF(pShMem, pShMem->rCD.rTOC.u32LastZLBA, &rMSF);
  CD_PRINTF_FORCED("TOCValid:%u, "
                   "Last ZLBA %6u, "
                   "Last MSF %02u:%02u:%02u, "
                   "MinTrack %02u, "
                   "MaxTrack %02u",
                   (unsigned int)pShMem->rCD.rTOC.u8Valid,
                   (unsigned int)pShMem->rCD.rTOC.u32LastZLBA,
                   (unsigned int)rMSF.u8Min, (unsigned int)rMSF.u8Sec,
                   (unsigned int)rMSF.u8Frm,
                   (unsigned int)pShMem->rCD.rTOC.u8MinTrack,
                   (unsigned int)pShMem->rCD.rTOC.u8MaxTrack);

  for(iT = 0; iT < CD_TRACK_ARRAY_SIZE; iT++)
  {
    u32TLenLBA = 0;
    u32StartZLBA = 0;
    if((iT <= pShMem->rCD.rTOC.u8MaxTrack)
       && (iT >= pShMem->rCD.rTOC.u8MinTrack))
    {
      u32StartZLBA = pShMem->rCD.rTOC.arTrack[iT].u32StartZLBA;
      if(iT == pShMem->rCD.rTOC.u8MaxTrack)
      { // last track, use cd length as start of next track
        u32EndZLBA = pShMem->rCD.rTOC.u32LastZLBA;
      }
      else //if(iT == arCD[].u8LastTrack)
      {
        u32EndZLBA = pShMem->rCD.rTOC.arTrack[iT + 1].u32StartZLBA;
      } //else //if(iT == arCD[].u8LastTrack)
      u32TLenLBA = u32EndZLBA - u32StartZLBA;
    } //if iT >= && <=

    vZLBA2MSF(pShMem, u32TLenLBA, &rLenMSF);
    vZLBA2MSF(pShMem, u32StartZLBA, &rStartMSF);
    CD_PRINTF_FORCED(" Track [%02d]: "
                     "StartZLBA: %06u == "
                     "StartMSF: %02u:%02u:%02u, "
                     "length LBA %06u == "
                     "length MSF %02u:%02u:%02u "
                     "AdrCtrl 0x%02X",
                     (int)iT, (unsigned int)u32StartZLBA,
                     (unsigned int)rStartMSF.u8Min,
                     (unsigned int)rStartMSF.u8Sec,
                     (unsigned int)rStartMSF.u8Frm, (unsigned int)u32TLenLBA,
                     (unsigned int)rLenMSF.u8Min, (unsigned int)rLenMSF.u8Sec,
                     (unsigned int)rLenMSF.u8Frm,
                     (unsigned int)pShMem->rCD.rTOC.arTrack[iT].u8AdrCtrl);
  } //for(iT = 0; iT < CD_TRACK_ARRAY_SIZE; iT++)

  //Version info
  CD_PRINTF_FORCED("Product    [%s]",
                   (const char*) pShMem->rCD.rInquiry.au8Product16);
  CD_PRINTF_FORCED("Revision   [%s]",
                   (const char*) pShMem->rCD.rInquiry.au8Revision4);
  CD_PRINTF_FORCED("Vendor     [%s]",
                   (const char*) pShMem->rCD.rInquiry.au8Vendor8);
  CD_PRINTF_FORCED("VendorSpec [%s]",
                   (const char*) pShMem->rCD.rInquiry.au8VendorSpecific20);
  CD_PRINTF_FORCED("HW Version  [%s]",
                   (const char*) pShMem->rCD.rInquiry.au8HWVersion2);
  CD_PRINTF_FORCED("SW Version  [%s]",
                   (const char*) pShMem->rCD.rInquiry.au8SWVersion2);

  CD_TRACE_LEAVE_U4(CDID_CD_vCacheDebug, 0, 0, 0, 0, 0);
}

/*****************************************************************************
 * FUNCTION:     CD_u32CacheGetCDInfo
 * PARAMETER:    IN:drive index
 *               OUT: min track
 *               OUT: max track
 * RETURNVALUE:  OSAL Error
 * DESCRIPTION:  get track info from TOC
 ******************************************************************************/
tU32 CD_u32CacheGetCDInfo(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                          tU8 *pu8MinTrack, tU8 *pu8MaxTrack, tU32 *pu32LastLBA)
{
  tU32 u32Ret;
  tU8 u8MinTrack = 0;
  tU8 u8MaxTrack = 0;
  tU32 u32LastLBA = 0;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U4(CDID_CD_u32CacheGetCDInfo, 0, pu8MinTrack, pu8MaxTrack,
                    pu32LastLBA);

  u32Ret = u32UpdateTOC(pShMem);
  CD_SM_LOCK(hSem);
  if(CD_VALID == pShMem->rCD.rTOC.u8Valid)
  {
    u8MinTrack = pShMem->rCD.rTOC.u8MinTrack;
    (void)(NULL != pu8MinTrack ? *pu8MinTrack = u8MinTrack : 0);
    u8MaxTrack = pShMem->rCD.rTOC.u8MaxTrack;
    (void)(NULL != pu8MaxTrack ? *pu8MaxTrack = u8MaxTrack : 0);
    u32LastLBA = pShMem->rCD.rTOC.u32LastZLBA;
    (void)(NULL != pu32LastLBA ? *pu32LastLBA = u32LastLBA : 0);
  } //if(CD_VALID == pShMem->rCD.rTOC.u8Valid)
  CD_SM_UNLOCK(hSem);

  CD_TRACE_LEAVE_U4(CDID_CD_u32CacheGetCDInfo, u32Ret, 0, u8MinTrack,
                    u8MaxTrack, u32LastLBA);
  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CD_u32CacheGetVersion
 * PARAMETER:    IN:drive index
 *               OUT: Software version
 *               OUT: Hardware versaion
 *               OUT: vendor name (8 byte)
 * RETURNVALUE:  OSAL Error
 * DESCRIPTION:  get track info from TOC
 * HISTORY: 
 * 16 Mar,2016 - Get the two bytes each of SWVersion and HWVesion data from shared memory pShMem 
 *             - Kranthi Kiran(RBEI/ECF5)
 ************************************************************************************************/
tU32 CD_u32CacheGetVersion(tDevCDData *pShMem, tU8 *pu8SW, tU8 *pu8HW,
                           tU8 *pau8Vendor8)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  int i;
  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U4(CDID_CD_u32CacheGetVersion, 0, pu8SW, pu8HW, pau8Vendor8);

  if((NULL != pu8SW) && (NULL != pu8HW) && (NULL != pau8Vendor8))
  {
    if(CD_VALID != pShMem->rCD.rInquiry.u8Valid)
    { // read toc from ATA
      u32Ret = CD_SCSI_IF_u32Inquiry(pShMem, &pShMem->rCD.rInquiry);
    } //if(CD_VALID != pShMem->rCD.rTOC.u8Valid)

    if(CD_VALID == pShMem->rCD.rInquiry.u8Valid)
    {
      for(i = 0; i < 2; i ++)
      {
       pu8SW[i] = pShMem->rCD.rInquiry.au8SWVersion2[i];
       pu8HW[i] = pShMem->rCD.rInquiry.au8HWVersion2[i];
      }
      memcpy(pau8Vendor8, pShMem->rCD.rInquiry.au8Vendor8, 8);
    } //if(CD_VALID == pShMem->rCD.rInquiry.u8Valid)
  }
  else //if NULL !=
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if NULL !=

  CD_TRACE_LEAVE_U4(CDID_CD_u32CacheGetVersion, u32Ret, 0, 0, 0, 0);

  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CD_s32CacheGetTrackInfo
 * PARAMETER:    IN:drive index
 *               OUT: min track
 *               OUT: max track
 * RETURNVALUE:  OSAL Error
 * DESCRIPTION:  get track info from TOC
 ******************************************************************************/
tU32 CD_u32CacheGetTrackInfo(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                             tU8 u8Track, tU32 *pu32StartZLBA,
                             tU32 *pu32Control)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  tU32 u32SLBA = 0;
  tU8 u8Ctrl = 0;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U4(CDID_CD_u32CacheGetTrackInfo, 0, u8Track, pu32StartZLBA,
                    pu32Control);

  if((NULL != pu32StartZLBA) && (NULL != pu32Control))
  {
    u32Ret = u32UpdateTOC(pShMem);

    CD_SM_LOCK(hSem);
    if(CD_VALID == pShMem->rCD.rTOC.u8Valid)
    {
      if((u8Track <= pShMem->rCD.rTOC.u8MaxTrack)
         && (u8Track >= pShMem->rCD.rTOC.u8MinTrack))
      {
        u32SLBA = pShMem->rCD.rTOC.arTrack[u8Track].u32StartZLBA;
        *pu32StartZLBA = u32SLBA;
        u8Ctrl = pShMem->rCD.rTOC.arTrack[u8Track].u8AdrCtrl & 0x0F;
        *pu32Control = u8Ctrl;
      }
      else //if((u8Track <= rCD.rTOC.u8MaxTrack) ...
      {
        if(CD_TRACKINFO_MAX_LBA_TRACK == u8Track)
        { // return last LBA on CD
          u32SLBA = pShMem->rCD.rTOC.u32LastZLBA;
          *pu32StartZLBA = u32SLBA;
          u8Ctrl = 0x00;
          *pu32Control = u8Ctrl;
        }
        else //if(CD_TRACKINFO_MAX_LBA_TRACK == u8Track)
        {
          u32Ret = OSAL_E_INVALIDVALUE;
        } //else //if(CD_TRACKINFO_MAX_LBA_TRACK == u8Track)
      } //else //if((u8Track <= rCD.rTOC.u8MaxTrack) ...
    } //if(CD_VALID == pShMem->rCD.rTOC.u8Valid)
    CD_SM_UNLOCK(hSem);
  }
  else //if(NULL !=
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL !=

  CD_TRACE_LEAVE_U4(CDID_CD_u32CacheGetTrackInfo, u32Ret, 0, u8Track, u32SLBA,
                    u8Ctrl);

  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CD_u8GetLoaderInfo
 * PARAMETER:    drive index
 * RETURNVALUE:  loade rinfo
 * DESCRIPTION:  read loader info
 ******************************************************************************/
tU8 CD_u8GetLoaderInfo(tDevCDData *pShMem, tU8 *pu8InternalLoaderStatus)
{
  tU8 u8LI;
  tU8 u8InternalLoaderStatus;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_C_U8_NO_MEDIA);

  CD_TRACE_ENTER_U4(CDID_CD_u8GetLoaderInfo, 0, 0, 0, 0);

  //read media status from drive
  if(OSAL_E_NOERROR
     == CD_SCSI_IF_u32GetLoaderStatus(pShMem, &u8InternalLoaderStatus))
  {
    if(pu8InternalLoaderStatus)
      *pu8InternalLoaderStatus = u8InternalLoaderStatus;

    switch(u8InternalLoaderStatus)
    {
    case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
    case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
    case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
      u8LI = OSAL_C_U8_MEDIA_INSIDE;
      break;

    case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
      u8LI = OSAL_C_U8_EJECT_IN_PROGRESS;
      break;

    case CD_INTERNAL_LOADERSTATE_EJECT_ERROR:
    case CD_INTERNAL_LOADERSTATE_LOAD_ERROR:
    case CD_INTERNAL_LOADERSTATE_IN_SLOT:
      u8LI = OSAL_C_U8_MEDIA_IN_SLOT;
      break;

    case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
    case CD_INTERNAL_LOADERSTATE_UNKNOWN:
    case CD_INTERNAL_LOADERSTATE_EJECTED:
    case CD_INTERNAL_LOADERSTATE_NO_CD:
    default:
      u8LI = OSAL_C_U8_NO_MEDIA;
      break;
    } //switch(u8InternalLoaderStatus)
  }
  else //if(OSAL_E_NOERROR == ...
  {
    if(pu8InternalLoaderStatus)
      *pu8InternalLoaderStatus = CD_INTERNAL_LOADERSTATE_NO_CD;
    u8LI = OSAL_C_U8_NO_MEDIA;
  }

  CD_TRACE_LEAVE_U4(CDID_CD_u8GetLoaderInfo, OSAL_E_NOERROR, 0, u8LI,
                    u8InternalLoaderStatus, 0);
  return u8LI;
}

/*****************************************************************************
 * FUNCTION:     CD_u8GetInternalMediaType
 * PARAMETER:    Shared Mem Pointer
 * RETURNVALUE:  media type of recent inserted media
 * DESCRIPTION:  returns inernal media type (data, audio, mixed mode)
 *               example: CD_INTERNAL_MEDIA_TYPE_MIXED_MODE_MEDIA
 ******************************************************************************/
tU8 CD_u8GetInternalMediaType(tDevCDData *pShMem)
{
  tU8 u8IMT = CD_INTERNAL_MEDIA_TYPE_UNKNOWN_MEDIA;

  CD_CHECK_SHARED_MEM(pShMem, CD_INTERNAL_MEDIA_TYPE_UNKNOWN_MEDIA);

  CD_TRACE_ENTER_U4(CDID_CD_u8GetInternalMediaType, 0, 0, 0, 0);

  if(pShMem->rCD.u8InternalMediaTypeValid != CD_VALID)
  {
    if(OSAL_C_U8_MEDIA_INSIDE == CD_u8GetLoaderInfo(pShMem, NULL))
    {
      tU32 u32Ret;
      u32Ret = CD_SCSI_IF_u32ModeSense(pShMem, &u8IMT);

      pShMem->rCD.u8InternalMediaType = u8IMT;
      pShMem->rCD.u8InternalMediaTypeValid =
      u32Ret == OSAL_E_NOERROR ? CD_VALID : CD_INVALID;
    } //if(OSAL_C_U8_MEDIA_INSIDE == CD_u8GetLoaderInfo(pShMem))
  }
  else //if(pShMem->rCD.u8InternalMediaTypeValid != CD_VALID)
  {
    u8IMT = pShMem->rCD.u8InternalMediaType;
  } //else //if(pShMem->rCD.u8InternalMediaTypeValid != CD_VALID)

  CD_TRACE_LEAVE_U4(CDID_CD_u8GetInternalMediaType, OSAL_E_NOERROR, 0, u8IMT, 0,
                    0);
  return u8IMT;
}

/*****************************************************************************
 * FUNCTION:     CD_u8GetMediaType
 * PARAMETER:    drive index
 * RETURNVALUE:  media type of recebt inserted media
 * DESCRIPTION:  returns media type (data, audio)
 ******************************************************************************/
tU8 CD_u8GetMediaType(tDevCDData *pShMem)
{
  tU8 u8MT;
  tU8 u8IMT;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_C_U8_UNKNOWN_MEDIA);

  CD_TRACE_ENTER_U4(CDID_CD_u8GetMediaType, 0, 0, 0, 0);

  u8IMT = CD_u8GetInternalMediaType(pShMem);
  switch(u8IMT)
  {
  case CD_INTERNAL_MEDIA_TYPE_INCORRECT_MEDIA :
    u8MT = OSAL_C_U8_UNKNOWN_MEDIA;
    break;
  case CD_INTERNAL_MEDIA_TYPE_DATA_MEDIA :
    u8MT = OSAL_C_U8_DATA_MEDIA;
    break;
  case CD_INTERNAL_MEDIA_TYPE_AUDIO_MEDIA :
    u8MT = OSAL_C_U8_AUDIO_MEDIA;
    break;
  case CD_INTERNAL_MEDIA_TYPE_MIXED_MODE_MEDIA :
    switch(pShMem->rCD.u8ForcedMixedModeMediaType)
    {
    case CD_INTERNAL_MEDIA_TYPE_AUDIO_MEDIA :
      u8MT = OSAL_C_U8_AUDIO_MEDIA;
      break;
    case CD_INTERNAL_MEDIA_TYPE_DATA_MEDIA :
      u8MT = OSAL_C_U8_DATA_MEDIA;
      break;
    default:
      u8MT = OSAL_C_U8_AUDIO_MEDIA;
    } //switch(pShMem->rCD.u8ForcedMixedModeMediaType)
    break;
  case CD_INTERNAL_MEDIA_TYPE_UNKNOWN_MEDIA :
  default:
    u8MT = OSAL_C_U8_UNKNOWN_MEDIA;
  } //switch(u8IMT)

  CD_TRACE_LEAVE_U4(CDID_CD_u8GetMediaType, OSAL_E_NOERROR, 0, u8MT, u8IMT, 0);
  return u8MT;
}

/*****************************************************************************
 * FUNCTION:     CD_u8GetMediaType
 * PARAMETER:    drive index
 * RETURNVALUE:  media type of recebt inserted media
 * DESCRIPTION:  returns media type (data, audio)
 ******************************************************************************/
tU8 CD_u8GetFSType(tDevCDData *pShMem)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  tU8 u8FS;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_C_U8_FS_TYPE_UNKNOWN);

  CD_TRACE_ENTER_U4(CDID_CD_u8GetFSType, 0, 0, 0, 0);

  if(pShMem->rCD.u8FSTypeValid != CD_VALID)
  {
    if(OSAL_C_U8_MEDIA_INSIDE == CD_u8GetLoaderInfo(pShMem, NULL))
    { // read sector from ATA
      u32Ret = CD_SCSI_IF_u32Read(pShMem, CD_FS_SECTOR_LBA, 1, gau8Sector);
      if(OSAL_E_NOERROR == u32Ret)
      {
        if(s16CompIdentifier(&gau8Sector[1], /*identifier*/
                             (tU8 *)CD_C_SZ_ISO_STD_IDENTIFIER,
                             CD_C_U8_ISO_STD_ID_LENGTH)
           == 0)
        {
          pShMem->rCD.u8FSType = OSAL_C_U8_FS_TYPE_CDFS;
          pShMem->rCD.u8FSTypeValid = CD_VALID;
        }
        else
        {
          if(s16CompIdentifier(&gau8Sector[1], /*identifier*/
                               (tU8 *)CD_C_SZ_UDFS_STD_IDENTIFIER,
                               CD_C_U8_UDFS_STD_ID_LENGTH)
             == 0)
          {
            pShMem->rCD.u8FSType = OSAL_C_U8_FS_TYPE_UDFS;
            pShMem->rCD.u8FSTypeValid = CD_VALID;
          }
        }
      } //if(OSAL_E_NOERROR == u32Ret)
    } //if(OSAL_C_U8_MEDIA_INSIDE == CD_u8GetLoaderInfo(pShMem))
  } //if(pShMem->rCD.u8FSTypeValid != CD_VALID)

  u8FS = pShMem->rCD.u8FSType;

  CD_TRACE_LEAVE_U4(CDID_CD_u8GetFSType, u32Ret, 0, u8FS, 0, 0);
  return u8FS;
} //OSAL_C_U8_FS_TYPE_CDFS;

/*****************************************************************************
 * FUNCTION:     CD_u32GetStartOfTrackZLBA
 * PARAMETER:    IN:drive index
 *               IN: track
 * RETURNVALUE:  Start address of Track inZLBA
 * DESCRIPTION:  get track info from TOC
 ******************************************************************************/
tU32 CD_u32GetStartOfTrackZLBA(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                               tU8 u8Track)
{
  tU32 u32Ret;
  tU32 u32SZLBA = 0;

  CD_CHECK_SHARED_MEM(pShMem, 0);

  CD_TRACE_ENTER_U4(CDID_CD_u32GetStartOfTrackZLBA, 0, u8Track, 0, 0);

  u32Ret = u32UpdateTOC(pShMem);
  if(CD_VALID == pShMem->rCD.rTOC.u8Valid)
  {
    tU8 u8MinTrack = 0;
    tU8 u8MaxTrack = 0;
    u32Ret = CD_u32CacheGetCDInfo(pShMem, hSem, &u8MinTrack, &u8MaxTrack,
                                  NULL);
    if(OSAL_E_NOERROR == u32Ret)
    {
      if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
      {
        CD_SM_LOCK(hSem);
        u32SZLBA = pShMem->rCD.rTOC.arTrack[u8Track].u32StartZLBA;
        CD_SM_UNLOCK(hSem);
      } //if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
    } //if(OSAL_E_NOERROR == u32Ret)
  } //if(CD_VALID == pShMem->rCD.rTOC.u8Valid)

  CD_TRACE_LEAVE_U4(CDID_CD_u32GetStartOfTrackZLBA, u32Ret, 0, u8Track,
                    u32SZLBA, 0);

  return u32SZLBA;
}

/*****************************************************************************
 * FUNCTION:     CD_u32GetEndOfTrackZLBA
 * PARAMETER:    IN: Track
 * RETURNVALUE:  End of track in ZLBA
 * DESCRIPTION:  calculates last position of track
 ******************************************************************************/
tU32 CD_u32GetEndOfTrackZLBA(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                             tU8 u8Track)
{
  tU32 u32EndZLBA = (tU32)-1;

  CD_CHECK_SHARED_MEM(pShMem, (tU32)-1);

  CD_TRACE_ENTER_U4(CDID_CD_u32GetEndOfTrackZLBA, 0, u8Track, 0, 0);

  (void)u32UpdateTOC(pShMem);
  if(CD_VALID == pShMem->rCD.rTOC.u8Valid)
  {
    tU8 u8MinTrack = 0;
    tU8 u8MaxTrack = 0;
    tU32 u32Ret;
    u32Ret = CD_u32CacheGetCDInfo(pShMem, hSem, &u8MinTrack, &u8MaxTrack,
                                  NULL);
    if(OSAL_E_NOERROR == u32Ret)
    {
      if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
      {
        CD_SM_LOCK(hSem);
        if(u8Track == pShMem->rCD.rTOC.u8MaxTrack)
        { // last track, use cd length as start of next track
          u32EndZLBA = pShMem->rCD.rTOC.u32LastZLBA;
        }
        else //if(u8Track == pShMem->rCD.rTOC.u8MaxTrack)
        {
          u32EndZLBA = pShMem->rCD.rTOC.arTrack[u8Track + 1].u32StartZLBA - 1;
        } //else //if(u8Track == pShMem->rCD.rTOC.u8MaxTrack)
        CD_SM_UNLOCK(hSem);
      } //if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
    } //if(OSAL_E_NOERROR == u32Ret)
  }
  else //if(pShMem->rCD.rTOC.u8Valid == CD_VALID)
  {
    CD_PRINTF_ERRORS("TOC not valid");
  } //else //if(pShMem->rCD.rTOC.u8Valid == CD_VALID)

  CD_TRACE_LEAVE_U4(CDID_CD_u32GetEndOfTrackZLBA,
                    u32EndZLBA != (tU32)-1?OSAL_E_NOERROR:OSAL_E_UNKNOWN, 0,
                    u32EndZLBA, 0, 0);

  return u32EndZLBA;
}

/*****************************************************************************
 * FUNCTION:     CD_u32GetEndOfCDZLBA
 * PARAMETER:    IN:drive index
 * RETURNVALUE:  Last ZLBA of CD
 * DESCRIPTION:  get end of CD
 ******************************************************************************/
tU32 CD_u32GetEndOfCDZLBA(tDevCDData *pShMem)
{
  tU32 u32Ret;
  tU32 u32LastZLBA = 0;

  CD_CHECK_SHARED_MEM(pShMem, 0);

  CD_TRACE_ENTER_U4(CDID_CD_u32GetEndOfCDZLBA, 0, 0, 0, 0);

  u32Ret = u32UpdateTOC(pShMem);
  if(CD_VALID == pShMem->rCD.rTOC.u8Valid)
  {
    u32LastZLBA = pShMem->rCD.rTOC.u32LastZLBA;
  } //if(CD_VALID == pShMem->rCD.rTOC.u8Valid)

  CD_TRACE_LEAVE_U4(CDID_CD_u32GetEndOfCDZLBA, u32Ret, 0, u32LastZLBA, 0, 0);

  return u32LastZLBA;
}

/*****************************************************************************
 * FUNCTION:     CD_u32TS2ZLBA
 * PARAMETER:    IN: Track
 *               IN: Offset in track in secs
 * RETURNVALUE:  ZLBA for TS
 * DESCRIPTION:  calculates absolute playposition for
 *               given Track-Seconds
 ******************************************************************************/
tU32 CD_u32TS2ZLBA(tDevCDData *pShMem, OSAL_tSemHandle hSem, tU8 u8Track,
                   tU16 u16Seconds)
{
  tU32 u32ZLBA = (tU32)-1;
  tU32 u32StartZLBA;

  CD_CHECK_SHARED_MEM(pShMem, (tU32)-1);

  CD_TRACE_ENTER_U4(CDID_CD_u32TS2ZLBA, 0, u8Track, u16Seconds, 0);
  (void)u32UpdateTOC(pShMem);
  if(CD_VALID == pShMem->rCD.rTOC.u8Valid)
  {
    tU8 u8MinTrack = 0;
    tU8 u8MaxTrack = 0;
    tU32 u32Ret;
    u32Ret = CD_u32CacheGetCDInfo(pShMem, hSem, &u8MinTrack, &u8MaxTrack,
                                  NULL);
    if(OSAL_E_NOERROR == u32Ret)
    {
      tU32 u32OfsFrm = CD_SEC2ZLBA(u16Seconds);
      CD_SM_LOCK(hSem);
      u32StartZLBA = pShMem->rCD.rTOC.arTrack[u8Track].u32StartZLBA;
      CD_SM_UNLOCK(hSem);
      u32ZLBA = u32StartZLBA + u32OfsFrm;
    } //if(OSAL_E_NOERROR == u32Ret)
  }
  else //if(pShMem->rCD.rTOC.u8Valid == CD_VALID)
  {
    CD_PRINTF_ERRORS("TOC not valid");
  } //else //if(pShMem->rCD.rTOC.u8Valid == CD_VALID)

  CD_TRACE_LEAVE_U4(CDID_CD_u32TS2ZLBA,
                    u32ZLBA != (tU32)-1?OSAL_E_NOERROR:OSAL_E_UNKNOWN, 0,
                    u32ZLBA, 0, 0);

  return u32ZLBA;
}

/*****************************************************************************
 * FUNCTION:     CD_bGetTMSFromAbsZLBA
 * PARAMETER:    IN: DriveID, Absolute ZLBA
 *               OUT: Pointer to TMSF
 * RETURNVALUE:  TRUE: playtime converted, FALSE: not converted
 * DESCRIPTION:  calculates relative playtime, Binary search
 *               (recursive Divide Et Impera - version)
 ******************************************************************************/
tBool CD_bGetTMSFromAbsZLBA(tDevCDData *pShMem, tU32 u32ZLBA,
                            CD_sTMSF_type *prRelTMSF)
{
  tBool bOK = FALSE;
  tBool bTocValid;
  tU32 u32AbsSec = u32ZLBA2Sec(u32ZLBA);
  tInt iT = 0;
  CD_sTMSF_type rTMSF =
  { 0};

  CD_CHECK_SHARED_MEM(pShMem, FALSE);

  CD_TRACE_ENTER_U4(CDID_CD_bGetTMSFromAbsZLBA, 0, u32ZLBA, u32AbsSec, 0);

  if(NULL != prRelTMSF)
  {

    (void)u32UpdateTOC(pShMem);
    bTocValid = pShMem->rCD.rTOC.u8Valid;
    if(bTocValid)
    {
      tU32 u32LenOfCDSec = u32ZLBA2Sec(pShMem->rCD.rTOC.u32LastZLBA);
      tU8 u8FirstTrack = pShMem->rCD.rTOC.u8MinTrack;
      tU32 u32StartOfCDSec = u32ZLBA2Sec(
                           pShMem->rCD.rTOC.arTrack[u8FirstTrack].u32StartZLBA);

      if((u32AbsSec <= u32LenOfCDSec) && (u32AbsSec >= u32StartOfCDSec))
      {
        tU32 u32StartSec;
        tU32 u32RelSec;
        iT = iPlaytimeInWhichTrack(pShMem, u32AbsSec,
                                   pShMem->rCD.rTOC.u8MinTrack,
                                   pShMem->rCD.rTOC.u8MaxTrack + 1);
        if((iT >= 0) && (iT >= pShMem->rCD.rTOC.u8MinTrack)
           && (iT <= pShMem->rCD.rTOC.u8MaxTrack))
        {
          u32StartSec = u32ZLBA2Sec(pShMem->rCD.rTOC.arTrack[iT].u32StartZLBA);
        }
        else //if (iT ...
        {
          u32StartSec = 0;
        } //else //if (iT ...

        u32RelSec = u32AbsSec - u32StartSec;
        vSec2MSF(pShMem, u32RelSec, &rTMSF);
        rTMSF.u8Track = (tU8)iT;
        prRelTMSF->u8Track = rTMSF.u8Track;
        prRelTMSF->u8Min = rTMSF.u8Min;
        prRelTMSF->u8Sec = rTMSF.u8Sec;
        bOK = TRUE;
      }
      else //if((u32AbsSec <= u32LenOfCDSec) && (u32AbsSec >= u32StartOfCDSec))
      {
        //outside cd
        bOK = FALSE;
      } //else if((u32AbsSec<=u32LenOfCDSec) && (u32AbsSec>=u32StartOfCDSec))
    }
    else //if(bTocValid)
    {
      bOK = FALSE;
    } //else //if(bCDValid)

    CD_PRINTF_U2("CD_bGetTMSFromAbsZLBA "
                 "ZLBA [%06u] "
                 "TMS: [%02u-%02u:%02u] OK: %d",
                 (unsigned int)u32ZLBA, (unsigned int)rTMSF.u8Track,
                 (unsigned int)rTMSF.u8Min, (unsigned int)rTMSF.u8Sec,
                 (int)bOK);
  } //if(NULL != prRelTMSF)

  CD_TRACE_LEAVE_U4(CDID_CD_bGetTMSFromAbsZLBA,
                    bOK?OSAL_E_NOERROR:OSAL_E_UNKNOWN, rTMSF.u8Track,
                    rTMSF.u8Min, rTMSF.u8Sec, bOK);
  return bOK;
}

/*****************************************************************************
 * FUNCTION:     u32ZLBA2Sec
 * PARAMETER:    Zero based LBA
 * RETURNVALUE:  seconds
 * DESCRIPTION:  calculates Seconds from ZLBA without using Frames!
 ******************************************************************************/
static tU32 u32ZLBA2Sec(tU32 u32ZLBA)
{
  return u32ZLBA / CD_SECTORS_PER_SECOND;
}

/*****************************************************************************
 * FUNCTION:     iSec2MSF
 * PARAMETER:    Pointer to struct and values to set
 * RETURNVALUE:  void, MSF
 * DESCRIPTION:  calculates Seconds from MSF without using Frames!
 ******************************************************************************/
static void vSec2MSF(tDevCDData *pShMem, tU32 u32Seconds, CD_sTMSF_type *pTMSF)
{
  CD_TRACE_ENTER_U4(CDID_vSec2MSF, u32Seconds, pTMSF, 0, 0);

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  if(NULL != pTMSF)
  {
    //don't touch track entry ! pTMSF->u8Track = 0;
    pTMSF->u8Min = (tU8)(u32Seconds / 60);
    pTMSF->u8Sec = (tU8)(u32Seconds % 60);
    pTMSF->u8Frm = (tU8)0;
    CD_TRACE_LEAVE_U4(CDID_vSec2MSF, OSAL_E_NOERROR, pTMSF->u8Track,
                      pTMSF->u8Min, pTMSF->u8Sec, pTMSF->u8Frm);
  }
  else //if(NULL != pTMSF)
  {
    CD_TRACE_LEAVE_U4(CDID_vSec2MSF, OSAL_E_INVALIDVALUE, 0, 0, 0, 0);
  }
}

/*****************************************************************************
 * FUNCTION:     iPlaytimeInWhichTrack
 * PARAMETER:    Drive Index, absolute playtime, lower track, upper track
 * RETURNVALUE:  -1: playtime not in track; >=0 tracknumber
 * DESCRIPTION:  recursive function, looking for track for given playtime
 ******************************************************************************/
static tInt iPlaytimeInWhichTrack(tDevCDData *pShMem, tU32 u32AbsTimeSec,
                                  tInt iT0, tInt iT1)
{
  tInt iTrackFound = -1;
  tU32 u32SecMid;
  tInt iTMid;

  CD_CHECK_SHARED_MEM(pShMem, -1);

  CD_TRACE_ENTER_U4(CDID_iPlaytimeInWhichTrack, u32AbsTimeSec, iT0, iT1, 0);

  iTMid = iT0 + ((iT1 - iT0) / 2);
  CD_PRINTF_U4("ENTER iPlaytimeInWhichTrack AbsTime %5u secs,"
               " T0 %02d, T1 %02d, TMid %02d",
               u32AbsTimeSec, iT0, iT1, iTMid);

  if(iT0 == iTMid)
  {
    iTrackFound = iT0;
    CD_PRINTF_U4(" FOUND Track %02d", iTrackFound);
  }
  else //if(T0 == T1)
  {
    if((iTMid >= 0) && (iTMid < CD_TRACK_ARRAY_SIZE) &&
       CD_VALID == pShMem->rCD.rTOC.u8Valid)
    { //start time of track VALID
      u32SecMid = u32ZLBA2Sec(pShMem->rCD.rTOC.arTrack[iTMid].u32StartZLBA);
      if(u32AbsTimeSec < u32SecMid)
      {
        iTrackFound = iPlaytimeInWhichTrack(pShMem, u32AbsTimeSec, iT0, iTMid);
      }
      else
      {
        iTrackFound = iPlaytimeInWhichTrack(pShMem, u32AbsTimeSec, iTMid, iT1);
      }
    }
    else //if(CD_VALID ==
    {
      //start time of track NOT valid
      iTrackFound = -1;
    } //else //if(CD_VALID ==
  } //else //if(T0 == T1)

  CD_TRACE_LEAVE_U4(CDID_iPlaytimeInWhichTrack, OSAL_E_NOERROR, u32AbsTimeSec,
                    iT0, iT1, iTrackFound);

  return iTrackFound;
}

/*****************************************************************************
 * FUNCTION:     CD_u32CacheInit
 * PARAMETER:    void
 * RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  initialises cache
 ******************************************************************************/
tU32 CD_u32CacheInit(tDevCDData *pShMem)
{
  tU32 u32Result = OSAL_E_NOERROR;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U4(CDID_CD_u32CacheInit, 0, 0, 0, 0);
  //full reset
  CD_vCacheClear(pShMem, OSAL_C_INVALID_HANDLE, TRUE);
  //CD_u8SetMediaType(pShMem, OSAL_C_U8_AUDIO_MEDIA); //CD only supported
  CD_TRACE_LEAVE_U3(CDID_CD_u32CacheInit, u32Result, 0, 0, 0, 0);

  return u32Result;
}

/*****************************************************************************
 * FUNCTION:     CD_vCacheDestroy
 * PARAMETER:    void
 * RETURNVALUE:  void
 * DESCRIPTION:  initialises cache
 ******************************************************************************/
tVoid CD_vCacheDestroy(tDevCDData *pShMem)
{
  CD_CHECK_SHARED_MEM_VOID(pShMem);
}

#ifdef __cplusplus
}
#endif
/************************************************************************
 |end of file
 |-----------------------------------------------------------------------*/
