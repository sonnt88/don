/************************************************************************
| $Revision: 1.0 $
| $Date: 2013/01/01 $
|************************************************************************
| FILE:         cd_scsi_if.c
| PROJECT:      GEN3
| SW-COMPONENT: OSAL I/O Device
|------------------------------------------------------------------------
| DESCRIPTION:
|  This file contains the /dev/cdctrl device implementation
|------------------------------------------------------------------------
| Date      | Modification               | Author
| 2013      | GEN3 VW MIB                | srt2hi
|
|************************************************************************
| * HISTORY: 
| 16 Mar,2016 |  Bug Fix NCG3D-11114 : 
|                Issue: CD Driver version changes for every CD operation which should be constant
|                Reason: For HW and SW version valid bytes (33,34 and 35,36) should be considered. 
|                        But here 57 and 58 bytes are considered which are not valid.
|                Fix:    33,34 and 35,36 bytes are collected for SW and HW version instead of 57 and 58.
|                Author: Kranthi Kiran (RBEI/ECF5)
|********************************************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include <unistd.h>
#include <libudev.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <scsi/sg.h> /* take care: fetches glibc's /usr/include/scsi/sg.h */
#include <scsi/scsi.h>
#include <linux/cdrom.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"

#include "cd_funcenum.h"
#include "cd_main.h"
#include "cd_trace.h"
#include "cd_cache.h"
#include "cd_scsi_if.h"

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
//simulate MASCA behaviour even a CD drive is used which supports frames
#define CD_SCSI_SET_FRAMES_TO_ZERO

#define CD_SCSI_IF_TOC_ENTRY_LEN           8

//http://en.wikipedia.org/wiki/SCSI_command
#define CD_SCSI_IF_SG_CMD_00_1F_LEN        6
#define CD_SCSI_IF_SG_CMD_20_5F_LEN        10
#define CD_SCSI_IF_SG_CMD_A0_BF_LEN        12

#define CD_SCSI_IF_SG_RESPONSE_HEADER_LEN  4

#define CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN  96
#define CD_SCSI_IF_SG_TOC_BUFFER_LEN       1200
#define CD_SCSI_IF_SG_SENSE_BUFFER_LEN     18
/* The response buffer length should be 36. Because it receives 36 bytes of valid data from MASCA */
#define CD_SCSI_IF_SG_INQUIRY_BUFFER_LEN   36
#define CD_SCSI_IF_SG_CMD_TIMEOUT_MS       20000

#define CD_SCSI_IF_SG_CMD_STOP             GPCMD_STOP_PLAY_SCAN /*0x4e*/
#define CD_SCSI_IF_SG_CMD_PLAYAUDIO_TI     GPCMD_PLAY_AUDIO_TI  /*0x48*/
#define CD_SCSI_IF_SG_CMD_PLAYAUDIOMSF     GPCMD_PLAY_AUDIO_MSF /*0x47*/
#define CD_SCSI_IF_SG_CMD_GET_EVENT_STATUS_NOTIFICATION  0x4a
#define CD_SCSI_IF_SG_CMD_PAUSERESUME      GPCMD_PAUSE_RESUME   /*0x4B*/
#define CD_SCSI_IF_SG_CMD_READ_SUBCHANNEL  GPCMD_READ_SUBCHANNEL /*0x42*/
#define CD_SCSI_IF_SG_SUBCHANNEL_DATA_LEN  12
#define CD_SCSI_IF_SG_CMD_SCAN             GPCMD_SCAN            /*0xBA*/

//ASCSI ASC-ASCQ
#define CD_SCSI_ASC_ASCQ_0401 0x0401 // LOGICAL_UNIT_IS_IN_PROCESS_OF_
                                     // BECOMING_READY
#define CD_SCSI_ASC_ASCQ_0407 0x0407 // LOGICAL_UNIT_NOT_READY_OPERATION_
                                     // IN_PROGRESS
#define CD_SCSI_ASC_ASCQ_1000 0x1000 // ID_CRC_OR_ECC_ERROR
#define CD_SCSI_ASC_ASCQ_3001 0x3001 // CANNOT_READ_MEDIUM_UNKNOWN_FORMAT
#define CD_SCSI_ASC_ASCQ_3A00 0x3A00 // MEDIUM_NOT_PRESENT
#define CD_SCSI_ASC_ASCQ_5300 0x5300 // MEDIA_LOAD_OR_EJECT_FAILED
#define CD_SCSI_ASC_ASCQ_2400 0x2400 // INVALID_FIELD_IN_CDB
#define CD_SCSI_ASC_ASCQ_6500 0x6500 // VOLTAGE FAULT
  


/*Get Event / Status Notification - mmc3r10g.pdf: table 94*/
#define CD_SCSI_GETEVENT_MEDIA_EVENT_NOCHG         0x00
#define CD_SCSI_GETEVENT_MEDIA_EVENT_EJECT_REQ     0x01
#define CD_SCSI_GETEVENT_MEDIA_EVENT_NEW_MEDIA     0x02
#define CD_SCSI_GETEVENT_MEDIA_EVENT_MEDIA_REMOVAL 0x03
#define CD_SCSI_GETEVENT_MEDIA_EVENT_MEDIA_CHANGE  0x04
#define CD_SCSI_CONTROL_VENDOR_ID                  0x80


//SCSI DRIVER STATUS (as long as not defined in scsi.h)
#define CD_SCSI_DRIVER_OK           0x00


#define CD_SCSI_DRIVER_BUSY         0x01
#define CD_SCSI_DRIVER_SOFT         0x02
#define CD_SCSI_DRIVER_MEDIA        0x03
#define CD_SCSI_DRIVER_ERROR        0x04
#define CD_SCSI_DRIVER_INVALID      0x05
#define CD_SCSI_DRIVER_TIMEOUT      0x06
#define CD_SCSI_DRIVER_HARD         0x07
#define CD_SCSI_DRIVER_SENSE	      0x08

// Host byte codes, as long as not defined in scsi.h
#define CD_SCSI_DID_OK          0x00 /* NO error                              */
#define CD_SCSI_DID_NO_CONNECT  0x01 /* Couldn't connect before timeout period*/
#define CD_SCSI_DID_BUS_BUSY    0x02 /* BUS stayed busy through timeout period*/
#define CD_SCSI_DID_TIME_OUT    0x03 /* TIMED OUT for other reason            */
#define CD_SCSI_DID_BAD_TARGET  0x04 /* BAD target.                           */
#define CD_SCSI_DID_ABORT       0x05 /* Told to abort for some other reason   */
#define CD_SCSI_DID_PARITY      0x06 /* Parity error                          */
#define CD_SCSI_DID_ERROR       0x07 /* Internal error                        */
#define CD_SCSI_DID_RESET       0x08 /* Reset by somebody.                    */
#define CD_SCSI_DID_BAD_INTR    0x09 /* Got an interrupt we weren't expecting.*/
#define CD_SCSI_DID_PASSTHROUGH 0x0a /* Force command past mid-layer          */
#define CD_SCSI_DID_SOFT_ERROR  0x0b /* The low level driver just wish a retry*/
#define CD_SCSI_DID_IMM_RETRY   0x0c /* Retry without decrementing retry count*/
#define CD_SCSI_DID_REQUEUE	0x0d	/* Requeue command (no immediate retry) also
                            			   without decrementing the retry count	   */
#define CD_SCSI_DID_TRANSPORT_DISRUPTED 0x0e /* Transport error disrupted
                                        * execution
                                        * and the driver blocked the port to
                                        * recover the link. Transport class will
                                        * retry or fail IO */
#define CD_SCSI_DID_TRANSPORT_FAILFAST	0x0f /* Transport class fastfailed
                                               the io */
#define CD_SCSI_DID_TARGET_FAILURE 0x10 /* Permanent target failure,
                                          do not retry on
				                                  * other paths */
#define CD_SCSI_DID_NEXUS_FAILURE 0x11  /* Permanent nexus failure, retry
                              on other paths might yield different results */

//device node to use /dev/sgx or /dev/srx
#define CD_SCSI_USE_SG  1
#define CD_SCSI_USE_SR  2
/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable inclusion  (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static void vZLBA2MSF(tDevCDData *pShMem, tU32 u32ZLBA, CD_sMSF_type *pMSF);

/************************************************************************
|function prototype (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/******************************************************************************
 *FUNCTION      :bGetFirstCDSys
 *DESCRIPTION   :Searches for DevName of first CD drive
 *PARAMETER     :tDevCDData *pShMem: Pointer to shared memory
 *               CD_tsDevName * psDevOut: Pointer to structure to be initialized
 *RETURNVALUE   :TRUE: found
 *HISTORY:      :Created by srt2hi 2014
 *****************************************************************************/
static tBool bGetFirstCDSys(struct udev *udev, const char *pcszMascaPath,
                            const char* pcszSysName, char *pszDevNameOut)
{
  tBool bFound = FALSE;
  struct udev_enumerate *enumerate;
  struct udev_list_entry *listEntry;
  struct udev_device *dev;

  if(!pszDevNameOut || !pcszSysName || !pcszMascaPath || !udev)
    return FALSE;

  enumerate = udev_enumerate_new(udev);
  udev_enumerate_add_match_sysname(enumerate, pcszSysName);
  udev_enumerate_scan_devices(enumerate);
  listEntry = udev_enumerate_get_list_entry(enumerate);
  udev_list_entry_foreach(listEntry, listEntry)
  {
    const char *path;
    path = udev_list_entry_get_name(listEntry);
    if(strstr(path, pcszMascaPath))
    {
      bFound = TRUE;
      dev = udev_device_new_from_syspath(udev, path);
      strcpy(pszDevNameOut, udev_device_get_devnode(dev));
      udev_device_unref(dev);
    } //strstr
  }
  udev_enumerate_unref(enumerate);
  return bFound;
}

/******************************************************************************
 *FUNCTION      :u32GetFirstCD
 *DESCRIPTION   :Searches for DevName of first CD drive
 *PARAMETER     :tDevCDData *pShMem: Pointer to shared memory
 *              CD_tsDevName * psDevOut: Pointer to structure to be initialized
 *RETURNVALUE   :OSAL_E_NOERROR: cd drive found and could be opened
 *               OSAL_E_DOESNOTEXIST: no CDd drive found
 *HISTORY:      :Created by srt2hi 2013
 *****************************************************************************/
static tU32 u32GetFirstCD(tDevCDData *pShMem, CD_tsDevName * psDevOut)
{
  tU32 u32Result = OSAL_E_NOERROR;
  tBool bSRFound = FALSE;
  tBool bSGFound = FALSE;
  int fd;
  struct udev *udev;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_u32GetFirstCD, psDevOut, 0, 0, 0);

  if(!psDevOut)
    return OSAL_E_INVALIDVALUE;

  memset(psDevOut, 0, sizeof(CD_tsDevName));

  udev = udev_new();

  if(udev)
  {
    struct udev_enumerate *enumerate;
    struct udev_list_entry *listEntry;
    struct udev_device *dev;
    enumerate = udev_enumerate_new(udev);

    udev_enumerate_add_match_subsystem(enumerate, "masca");
    udev_enumerate_scan_devices(enumerate);
    listEntry = udev_enumerate_get_list_entry(enumerate);
    udev_list_entry_foreach(listEntry, listEntry)
    {
      const char *path;
      path = udev_list_entry_get_name(listEntry);
      dev = udev_device_new_from_syspath(udev, path);
      bSGFound = bGetFirstCDSys(udev, path, "sg*", psDevOut->szDevSGName);
      bSRFound = bGetFirstCDSys(udev, path, "sr*", psDevOut->szDevSRName);
      udev_device_unref(dev);
    }
    udev_enumerate_unref(enumerate);
  }
  else //if(udev)
  {
    //error get udev listEntry
    u32Result = OSAL_E_NOACCESS;
  } //else //if(udev)

  psDevOut->bSGFound = bSGFound;
  psDevOut->bSRFound = bSRFound;

  psDevOut->bDefaultSGNameUsed = !bSGFound;
  psDevOut->bDefaultSRNameUsed = !bSRFound;

  //no listEntry found, use default name e.g. "/dev/sr0"
  if(!bSGFound)
    strncpy(psDevOut->szDevSGName, "/dev/sr0",
            sizeof(psDevOut->szDevSGName) - 1);

  if(!bSRFound)
    strncpy(psDevOut->szDevSRName, "/dev/sr0",
            sizeof(psDevOut->szDevSRName) - 1);

  fd = open(psDevOut->szDevSRName, O_RDWR | O_NONBLOCK);
  if(-1 != fd)
  {
    psDevOut->bSRCouldBeOpened = TRUE;
    (void)close(fd);
  }
  else
  {
    psDevOut->bSRCouldBeOpened = FALSE;
    CD_PRINTF_ERRORS("Error open listEntry [%s]: [%s]\n", psDevOut->szDevSRName,
                     strerror(errno));
  }

  fd = open(psDevOut->szDevSGName, O_RDWR | O_NONBLOCK);
  if(-1 != fd)
  {
    psDevOut->bSGCouldBeOpened = TRUE;
    (void)close(fd);
  }
  else
  {
    psDevOut->bSGCouldBeOpened = FALSE;
    CD_PRINTF_ERRORS("Error open listEntry [%s]: [%s]\n", psDevOut->szDevSGName,
                     strerror(errno));
  }

  if(u32Result == OSAL_E_NOERROR)
    u32Result =
    psDevOut->bSRCouldBeOpened && psDevOut->bSGCouldBeOpened ?
              OSAL_E_NOERROR : OSAL_E_DOESNOTEXIST;

  CD_TRACE_LEAVE_U4(CDID_u32GetFirstCD, u32Result, psDevOut->bSRCouldBeOpened,
                    psDevOut->bSGCouldBeOpened, 0, 0);

  return u32Result;
}

/******************************************************************************
 *FUNCTION      :InitSGCmdPacket
 *DESCRIPTION   :Initializes a sg_io_hdr structure to be used with an
 *               SCSI command, sent via ioctl using SG_IO.
 *PARAMETER     :Packet             Pointer to structure to be initialized
 *RETURNVALUE   :none
 *HISTORY:      :Created by srt2hi
 *****************************************************************************/
static void InitSGCmdPacket(sg_io_hdr_t *prIO, unsigned char *pucCmd,
                            size_t cmdSize, unsigned char *pucResponse,
                            size_t responseSize, unsigned char *pucSense,
                            size_t senseSize, tS32 s32BufferSizeOffset)
{
  (void)s32BufferSizeOffset;
  if(NULL != prIO)
    memset(prIO, 0, sizeof(sg_io_hdr_t));
  if(NULL != pucCmd)
    memset(pucCmd, 0, cmdSize);
  if(NULL != pucResponse)
    memset(pucResponse, 0, responseSize);
  if(NULL != pucSense)
    memset(pucSense, 0, senseSize);

  if((NULL != prIO) && (NULL != pucCmd))
  {
    prIO->cmdp    = pucCmd;
    prIO->cmd_len = (unsigned char)cmdSize;

    prIO->dxferp      = pucResponse;
    prIO->dxfer_len   = responseSize; // 0 ? 

    prIO->sbp         = pucSense;
    prIO->mx_sb_len   = (unsigned char)senseSize;

    prIO->interface_id = 'S'; //INTERFACE_ID
    prIO->dxfer_direction = SG_DXFER_FROM_DEV;
    prIO->timeout         = CD_SCSI_IF_SG_CMD_TIMEOUT_MS;

    if((s32BufferSizeOffset >= 0)
       && ((size_t)(s32BufferSizeOffset + 1) < cmdSize))
    {
      pucCmd[s32BufferSizeOffset] = (unsigned char)((responseSize >> 8) & 0xFF);
      pucCmd[s32BufferSizeOffset + 1] = (unsigned char)(responseSize & 0xFF);
    } //if(s32BufferSizeOffset >= 0)
  } //if((NULL != prIO) && (NULL != pucCmd))

}

/*****************************************************************************
* FUNCTION:     vZLBA2MSF
* PARAMETER:    IN: ZLBA
*               OUT: Pointer to struct MSF
* RETURNVALUE:  void, MSF
* DESCRIPTION:  calculates MSF from ZLBA
******************************************************************************/
static void vZLBA2MSF(tDevCDData *pShMem, tU32 u32ZLBA, CD_sMSF_type *pMSF)
{
  tU32 u32Seconds;

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  CD_TRACE_ENTER_U4(CDID_vZLBA2MSF, u32ZLBA, pMSF, 0, 0);

  u32Seconds = (u32ZLBA / CD_SECTORS_PER_SECOND);

  if(NULL != pMSF)
  {
    pMSF->u8Min   = (tU8)(u32Seconds / 60);
    pMSF->u8Sec   = (tU8)(u32Seconds % 60);
    pMSF->u8Frm   = (tU8)(u32ZLBA % CD_SECTORS_PER_SECOND);

    CD_TRACE_LEAVE_U4(CDID_vZLBA2MSF, OSAL_E_NOERROR, u32ZLBA, pMSF->u8Sec,
                      pMSF->u8Frm, pMSF->u8Frm);
  }
  else  //if(NULL != pMSF)
  {
    CD_TRACE_LEAVE_U4(CDID_vZLBA2MSF, OSAL_E_INVALIDVALUE, u32ZLBA, 0, 0, 0);
  } //else  //if(NULL != pMSF)
}

/*****************************************************************************
* FUNCTION:     vTraceHex
* PARAMETER:    Buffer, Length of buffer, number of Bytes printed per Line
* RETURNVALUE:  result
* DESCRIPTION:  output hex dump to TTFIS
******************************************************************************/
static tVoid vTraceHex(tDevCDData *pShMem, const char *pcszHint,
                       const tU8 *pu8Buf, tInt iBufLen, tInt iBytesPerLine)
{
  tInt iBpl = iBytesPerLine;
  tInt iL, iC;
  unsigned char c;
  char szTmp[32];
  char szTxt[CD_COMMON_TEXT_BUFFER_LEN];
  tBool bTrace;

  if(NULL == pu8Buf)
  {
    return;
  } //if(NULL == pu8Buf)

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  bTrace = pShMem != NULL ? pShMem->CD_bTraceOn : TRUE;
  if(bTrace)
  {
    if(LLD_bIsTraceActive(CD_TRACE_CLASS, (tS32)TR_LEVEL_USER_3))
    {
      for(iL = 0; iL < iBufLen; iL += iBpl)
      {
        sprintf(szTxt, "%04X:", (unsigned int)(iL));
        for(iC = 0; iC < iBpl; iC++)
        {
          tInt iIndex = iL + iC;
          if((iIndex < iBufLen))
          { //satisfy L I N T:
            //  Warning 662: Possible creation of out-of-bounds pointer
            //  (15 beyond end of data) by operator '['
            sprintf(szTmp, " %02X", (unsigned int)pu8Buf[iIndex]);
            strcat(szTxt, szTmp);
          }
        } //for(iC = 0 ; iC < iBpl ; iC++)

        strcat(szTxt, " ");
        for(iC = 0; iC < iBpl; iC++)
        {
          tInt iIndex = iL + iC;
          if((iIndex < iBufLen))
          { //satisfy L I N T:
            //  Warning 662: Possible creation of out-of-bounds pointer
            //  (15 beyond end of data) by operator '['
            c = pu8Buf[iIndex];
            if((c < 0x20) || (c >= 0x80))
            {
              c = (unsigned char)'.';
            }
            sprintf(szTmp, "%c", c);
            strcat(szTxt, szTmp);
          }

        } //for(iC = 0 ; iC < iBpl ; iC++)
        CD_PRINTF_U4("%s:%s", pcszHint ? pcszHint : "", szTxt);
      } //for(i = 0 ; i < iBufLen ; i += iBpl)
    } //if(LLD_bIsTraceActive( ...
  } //if(CD_bTraceOn)
}

/*****************************************************************************
* FUNCTION:     u32MapLxErrorToOsalError
* PARAMETER:    int err, linux-Error
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  converts TK to OSAL error
******************************************************************************/
static tU32 u32MapLxErrorToOsalError(tDevCDData *pShMem, int err)
{
  tU32 u32Error = OSAL_E_NOERROR;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_u32MapLxErrorToOsalError, err, 0, 0, 0);

  // May use OSAL function u32Error = u32ConvertErrorCore(errno);

  /* l i n t -save -e845 */

  switch(err)
  {
  case EBADFD:  //l i n t !e845
  case EBADF:  //l i n t !e845
    u32Error = OSAL_E_BADFILEDESCRIPTOR;
    CD_PRINTF_ERRORS("BADF, EBADF: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;
  case EIO:
    u32Error = OSAL_E_IOERROR;
    CD_PRINTF_ERRORS("IO: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;
  case EACCES:
    u32Error = OSAL_E_NOACCESS;
    CD_PRINTF_ERRORS("ACCES: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;
  case EROFS:
    u32Error = OSAL_E_WRITE_PROTECTED;
    CD_PRINTF_ERRORS("ROFS: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;
  case ETIMEDOUT:
    u32Error = OSAL_E_TIMEOUT;
    CD_PRINTF_ERRORS("TIMEDOUT: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;
  case ECANCELED:
    u32Error = OSAL_E_CANCELED;
    CD_PRINTF_ERRORS("CANCELED: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;
  case EBUSY:
    u32Error = OSAL_E_BUSY;
    CD_PRINTF_ERRORS("BUSY: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;

  case ENOENT:
  case ENODEV:
  case ENXIO:
  case ECHILD:
  case ESRCH:
    u32Error = OSAL_E_DOESNOTEXIST;
    CD_PRINTF_ERRORS("NO...: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;
  case EINVAL:
    u32Error = OSAL_E_INVALIDVALUE;
    CD_PRINTF_ERRORS("INVAL: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;
  case ENOMEDIUM:
    u32Error = OSAL_E_MEDIA_NOT_AVAILABLE;
    CD_PRINTF_ERRORS("NOMEDIUM: [%s] = OSAL 0x%08X", strerror(errno),
                     (unsigned int)u32Error);
    break;

  default:
    u32Error = OSAL_E_UNKNOWN;
    CD_PRINTF_ERRORS("E_0x%08X == %d (Hi = %d, Lo = %d)", (unsigned int)err,
                     (int) err, (int) (tS16)((tU32)err >> 16),
                     (int) (tS16)((tU32)err & 0xFFFF));
    break;

  } //switch()
  /* l i n t -restore */

  CD_TRACE_LEAVE_U4(CDID_u32MapLxErrorToOsalError, u32Error, err, 0, 0, 0);
  return u32Error;
}

/*****************************************************************************
* FUNCTION:     bGetSenseKey
* PARAMETER:    SCSI Status byte and Sense-Buffer, pointer of returnvalues
* RETURNVALUE:  TRUE if Sense Key and ASC/ASCQ available
* DESCRIPTION:  retrieves SK and ASC/ASCQ
******************************************************************************/
static tBool bGetSenseKey(tU8 u8SCSIStatus, tU16 u16DRVStatus,
                          tU8 *pu8SenseBuffer, tU8 u8SenseBufferLen,
                          tU8 *pu8SenseKey, /*out*/
                          tU16 *pu16ASCASCQ /*out*/) 
{
  tBool bSKAvail = FALSE;
  u8SCSIStatus =  (u8SCSIStatus & STATUS_MASK) >> 1;

  if(CHECK_CONDITION == u8SCSIStatus ||
     CD_SCSI_DRIVER_SENSE == u16DRVStatus)
  {
    if(NULL != pu8SenseBuffer && u8SenseBufferLen >= 4)
    {
      tU8 u8SK   = 0xFF; //sense key
      tU8 u8ASC  = 0x00;
      tU8 u8ASCQ = 0x00;
      tU8 u8ResponseCode = pu8SenseBuffer[0] & 0x7F;
      //check response code
      switch(u8ResponseCode)
      {
      case 0x70:
      case 0x71:
        bSKAvail = TRUE;
        u8SK     = pu8SenseBuffer[2] & 0x0F;
        if(u8SenseBufferLen >= 14)
        {
          u8ASC    = pu8SenseBuffer[12];
          u8ASCQ   = pu8SenseBuffer[13];
        }
        else //if(u8SenseBufferLen >= 14)
        {
          CD_PRINTF_ERRORS("GetSenseKey: sense buffer too short for RC "
                           "[0x%02X], "
                           "Ptr [%p], Len %u",
                           (unsigned int)u8ResponseCode, pu8SenseBuffer,
                           (unsigned int)u8SenseBufferLen);
        } //else //if(u8SenseBufferLen >= 14)
        break;
      case 0x72:
      case 0x73:
        bSKAvail = TRUE;
        u8SK     = pu8SenseBuffer[1] & 0x0F;
        u8ASC    = pu8SenseBuffer[2];
        u8ASCQ   = pu8SenseBuffer[3];
        break;
      default:
        CD_PRINTF_ERRORS("Unknown Sense Code 0x%02X",
                         (unsigned int)u8ResponseCode);
        break;
      } //switch(u8ResponseCode)

      if(NULL != pu8SenseKey)
        *pu8SenseKey = u8SK;

      if(NULL != pu16ASCASCQ)
        *pu16ASCASCQ = (((tU16)u8ASC << 8) | (tU16)u8ASCQ);
    }
    else //if(NULL != pu8SenseBuffer && u8SenseBufferLen > 0)
    {
      CD_PRINTF_ERRORS("sense buffer too short, Ptr [%p], Len %u",
                       pu8SenseBuffer, (unsigned int)u8SenseBufferLen);
    } //else //if(NULL != pu8SenseBuffer && u8SenseBufferLen > 0)
  } //if(CHECK_CONDITION == u8SCSIStatus || ...)

  return bSKAvail;
}

/*****************************************************************************
* FUNCTION:     u32MapSCSI2OsalError
* PARAMETER:    SCSI IO structure
* RETURNVALUE:  OSAL error code
* DESCRIPTION:  converts TK to OSAL error
******************************************************************************/
static tU32 u32MapSCSI2OsalError(tDevCDData *pShMem, sg_io_hdr_t *prIO)
{
  tU32 u32Error = OSAL_E_NOERROR;
  tBool bMapSK = FALSE;
  tU8  u8SCSIStatus  = 0;
  tU16 u16HostStatus = 0;
  tU16 u16DRVStatus  = 0;
  tU8  u8SenseKey    = 0;
  tU16 u16ASCASCQ    = 0;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_u32MapSCSI2OsalError, prIO, 0, 0, 0);

  if(prIO)
  {
    tBool bSKAvail;
    u8SCSIStatus  = (prIO->status & STATUS_MASK) >> 1;
    u16HostStatus = prIO->host_status;
    u16DRVStatus  = prIO->driver_status;

    bSKAvail = bGetSenseKey(prIO->status, //not shifted!
                            u16DRVStatus,
                            prIO->sbp,
                            prIO->sb_len_wr,
                            &u8SenseKey,
                            &u16ASCASCQ);

    switch(u8SCSIStatus)
    {
    case GOOD:
      ; //u32Error = OSAL_E_NOERROR;
      break;

    case CHECK_CONDITION:
      u32Error = OSAL_E_UNKNOWN;
      bMapSK   = TRUE;
      break;

    case BUSY:
      u32Error = OSAL_E_BUSY;
      break;

    case QUEUE_FULL:
      u32Error = OSAL_E_QUEUEFULL;
      break;

    case CONDITION_GOOD:
    case INTERMEDIATE_GOOD:
    case INTERMEDIATE_C_GOOD:
    case RESERVATION_CONFLICT:
    case COMMAND_TERMINATED:
    default:
      u32Error = OSAL_E_UNKNOWN;
      CD_PRINTF_ERRORS("Unsupported SCSI Status 0x%02X",
                       (unsigned int)u8SCSIStatus);
      break;
    } //switch(u8SCSIStatus)

    switch(u16DRVStatus)
    {
    case CD_SCSI_DRIVER_OK: // 0x00
      break;

    case CD_SCSI_DRIVER_SENSE:  //0x08
      u32Error = OSAL_E_UNKNOWN;
      bMapSK   = TRUE;
      break;

    case CD_SCSI_DRIVER_BUSY: // 0x01
    case CD_SCSI_DRIVER_SOFT: //0x02
    case CD_SCSI_DRIVER_MEDIA:  //0x03
    case CD_SCSI_DRIVER_ERROR:  //0x04
    case CD_SCSI_DRIVER_INVALID:  //0x05
    case CD_SCSI_DRIVER_TIMEOUT:  //0x06
    case CD_SCSI_DRIVER_HARD: //0x07
    default:
      u32Error = OSAL_E_UNKNOWN;
      CD_PRINTF_ERRORS("Unsupported DRIVER Status 0x%02X",
                       (unsigned int)u16DRVStatus);
      break;
    } //switch(u16DRVStatus)

    if(bMapSK && bSKAvail)
    {
      tBool bASCASCQSupported = TRUE;

      switch(u8SenseKey)
      {
      case NOT_READY:
        switch(u16ASCASCQ)
        {
        case CD_SCSI_ASC_ASCQ_0401: //LOGICAL_UNIT_IS_IN_PROCESS_OF_
                                    //BECOMING_READY
        case CD_SCSI_ASC_ASCQ_0407: //LOGICAL_UNIT_NOT_READY_OPERATION_
                                    // IN_PROGRESS
          u32Error = OSAL_E_INPROGRESS;
          break;
        case CD_SCSI_ASC_ASCQ_3A00: // MEDIUM_NOT_PRESENT
          u32Error = OSAL_E_MEDIA_NOT_AVAILABLE;
          break;
        case CD_SCSI_ASC_ASCQ_3001: // CANNOT_READ_MEDIUM_UNKNOWN_FORMAT
          u32Error = OSAL_E_NOT_MOUNTED;
          break;
        default:
          u32Error = OSAL_E_DOESNOTEXIST;
          bASCASCQSupported = FALSE;
          break;
        } //switch(u16ASCASCQ)

        break;

      case MEDIUM_ERROR:
        switch(u16ASCASCQ)
        {
        case CD_SCSI_ASC_ASCQ_1000: //ID_CRC_OR_ECC_ERROR
          u32Error = OSAL_E_IOERROR;
          break;
        default:
          u32Error = OSAL_E_IOERROR;
          bASCASCQSupported = FALSE;
          break;
        } //switch(u16ASCASCQ)

        break;

        case ILLEGAL_REQUEST:
          switch(u16ASCASCQ)
          {
          case CD_SCSI_ASC_ASCQ_2400: //INVALID_FIELD_IN_CDB
            u32Error = OSAL_E_INVALIDVALUE;
            break;
          default:
            u32Error = OSAL_E_INVALIDVALUE;
            bASCASCQSupported = FALSE;
            break;
          } //switch(u16ASCASCQ)
          break;

      case NO_SENSE:
      case RECOVERED_ERROR:
      case HARDWARE_ERROR:
      case UNIT_ATTENTION:
      case DATA_PROTECT:
      case BLANK_CHECK:
      case COPY_ABORTED:
      case ABORTED_COMMAND:
      case VOLUME_OVERFLOW:
      case MISCOMPARE:
        u32Error = OSAL_E_NOTSUPPORTED;
        CD_PRINTF_U1("INFO: Unmapped Sense Key 0x%02X",
                         (unsigned int)u8SenseKey);
        break;
      default:
        u32Error = OSAL_E_UNKNOWN;
        CD_PRINTF_ERRORS("Unknown Sense Key 0x%02X",
                         (unsigned int)u8SenseKey);
        break;
      } //switch(u8SenseKey)
      
      if(!bASCASCQSupported)
        CD_PRINTF_U1("INFO: Unmapped ASC-ASCQ 0x%04X",
                         (unsigned int)u16ASCASCQ);
    } //if(bMapSK && bSKAvail)

    //check host status if no other error found before
    if(u32Error == OSAL_E_NOERROR)
      switch(u16HostStatus)
      {
      case CD_SCSI_DID_OK:
        ;
        break;
      case CD_SCSI_DID_BUS_BUSY:
        u32Error = OSAL_E_BUSY;
        break;
      case CD_SCSI_DID_TIME_OUT:
        u32Error = OSAL_E_TIMEOUT;
        break;
      case CD_SCSI_DID_ABORT:
        u32Error = OSAL_E_CANCELED;
        break;
      case CD_SCSI_DID_NO_CONNECT:
      case CD_SCSI_DID_BAD_TARGET:
      case CD_SCSI_DID_PARITY:
      case CD_SCSI_DID_ERROR:
      case CD_SCSI_DID_RESET:
      case CD_SCSI_DID_BAD_INTR:
      case CD_SCSI_DID_PASSTHROUGH:
      case CD_SCSI_DID_SOFT_ERROR:
      case CD_SCSI_DID_IMM_RETRY:
      case CD_SCSI_DID_REQUEUE:
      case CD_SCSI_DID_TRANSPORT_DISRUPTED:
      case CD_SCSI_DID_TRANSPORT_FAILFAST:
      case CD_SCSI_DID_TARGET_FAILURE:
      case CD_SCSI_DID_NEXUS_FAILURE:
        u32Error = OSAL_E_NOTSUPPORTED;
        CD_PRINTF_ERRORS("Unsupported Host Status 0x%04X",
                         (unsigned int)u16HostStatus);
        break;
      default:
        u32Error = OSAL_E_UNKNOWN;
        CD_PRINTF_ERRORS("Unknown Host Status 0x%04X",
                         (unsigned int)u16HostStatus);
      } //switch(u16HostStatus)

    if(u32Error != OSAL_E_NOERROR)
      CD_PRINTF_SCSI_ERRORS(prIO);
    else //if(u32Error != OSAL_E_NOERROR)
      CD_PRINTF_SCSI_U3(prIO);
  }
  else //if(prIO)
  {
    u32Error = OSAL_E_INVALIDVALUE;
  } //else //if(prIO)

  CD_TRACE_LEAVE_U4(CDID_u32MapSCSI2OsalError, u32Error, u8SCSIStatus,
                    u16HostStatus, u8SenseKey, u16ASCASCQ);
  return u32Error;
}

/*****************************************************************************
* FUNCTION:     u32SendIOCTL
* PARAMETER:    IN: *shared mem 
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  Sends an SCSI command to SCSI-CD
* HISTORY:
******************************************************************************/
static tU32 u32SendIOCTL(tDevCDData *pShMem,
                         int iReq,
                         void *pData,
                         int hOpenCD,
                         int iDevNode)
{
  int hCD = hOpenCD;
  int iRet;
  tU32 u32Ret = OSAL_E_NOERROR;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_u32SendIOCTL, pShMem, iReq, pData, hOpenCD);

  if(-1 == hCD)
  { //open own driver, if no handle is provided
    switch(iDevNode)
    {
    case CD_SCSI_USE_SG:
      hCD = open(pShMem->rCD.rCDDrive.szDevSGName, O_RDWR | O_NONBLOCK);
      break;
    default:
    case CD_SCSI_USE_SR:
      hCD = open(pShMem->rCD.rCDDrive.szDevSRName, O_RDWR | O_NONBLOCK);
      break;
    } //switch(iDevNode)
  } //if(-1 == hCD)

  if(-1 != hCD)
  {
    iRet = ioctl(hCD, (unsigned long)iReq, pData);
    if(-1 == iRet)
    {
      u32Ret  = u32MapLxErrorToOsalError(pShMem, errno);
      CD_PRINTF_ERRORS("IOCTL [%d] failed [%s] - OSAL 0x%08X", (int)iReq,
                       strerror(errno), (unsigned int)u32Ret);
    } //if(-1 == iRet)
  }
  else //if(-1 != hCD)
  {
    u32Ret  = u32MapLxErrorToOsalError(pShMem, errno);
    CD_PRINTF_ERRORS("open [%s] failed [%s] - OSAL 0x%08X",
                     (const char*)pShMem->rCD.rCDDrive.szDevName,
                     strerror(errno), (unsigned int)u32Ret);
  } //else //if(-1 != hCD)

  if((-1 == hOpenCD) && (-1 != hCD))
  { //close own handle, if no handle was provided
    (void)close(hCD);
  } //if((-1 == hOpenCD) && (-1 != hCD))

  CD_TRACE_LEAVE_U4(CDID_u32SendIOCTL, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     u32SendSGIO
* PARAMETER:    IN: *pointer to shared mem
*               IN/OUT: SCSI buffer
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  Sends a SG_IO command to SCSI-CD
* HISTORY:
******************************************************************************/
static tU32 u32SendSGIO(tDevCDData *pShMem, sg_io_hdr_t *prIO, int hOpenCD,
                        int iDevNode)
{
  tU32 u32Ret;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_u32SendSGIO, pShMem, prIO, hOpenCD, 0);

  CD_PRINTF_SCSI_U4(prIO);
  u32Ret = u32SendIOCTL(pShMem, SG_IO, prIO, hOpenCD, iDevNode);
  if(u32Ret == OSAL_E_NOERROR)
  {
    u32Ret =  u32MapSCSI2OsalError(pShMem, prIO);
  }
  else //if(u32Ret == OSAL_E_NOERROR)
  {
    CD_PRINTF_ERRORS("IOCTL SG_IO failed OSAL_E_[0x%08X]",
                     (unsigned int)u32Ret);
    CD_PRINTF_SCSI_ERRORS(prIO);
  } //else //if(u32Ret == OSAL_E_NOERROR)

  CD_TRACE_LEAVE_U4(CDID_u32SendSGIO, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32Eject
* PARAMETER:    void
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  send eject to SCSI
* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32Eject(tDevCDData *pShMem)
{
  tU32 u32Ret;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32Eject, 0, 0, 0, 0);

  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_00_1F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];
    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    rIO.cmdp[0] = START_STOP; /*0x1b*/
    rIO.cmdp[1] = 0x01; //0x01; //1=immediate, 0=not
    rIO.cmdp[4] = 0x02; //EJECT (LoEj=1 Start=0);
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SG);
  }

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32Eject, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32Load
 * PARAMETER:    Shared Memory Pointer
 * RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  load CD
* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32Load(tDevCDData *pShMem)
{
  tU32 u32Ret;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32Load, 0, 0, 0, 0);

  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_00_1F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];
    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    //http://en.wikipedia.org/wiki/SCSI_Start_Stop_Unit_Command
    rIO.cmdp[0] = START_STOP; /*0x1b*/
    rIO.cmdp[1] = 0x01; //0x01; //1=immediate, 0=not
    rIO.cmdp[4] = 0x03; //LOAD (LoEj=1 Start=1);
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SG);
  }

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32Load, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32SetSleepMode
 * PARAMETER:    Shared Memory Pointer, Sleep-Flag
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  set sleep mode or set active

* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32SetSleepMode(tDevCDData *pShMem, tBool bSleep)
{
  tU32 u32Ret;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32SetSleepMode, bSleep, 0, 0, 0);

  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_00_1F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];
    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    rIO.cmdp[0] = START_STOP; /*0x1b*/
    rIO.cmdp[1] = 0x01; //0x01; //1=immediate, 0=not
    rIO.cmdp[4] = bSleep ? 0x50 : 0x10; //SLEEP=0x50, ACTIVE =0x10
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SG);
  }

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32SetSleepMode, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32Read
 * PARAMETER:    Shared Memory Pointer
 *               Start sector (LBA)
*               number of sectors
*               buffer for sectors
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  read sectors from cd
******************************************************************************/
#define CD_SCSI_READ_SECTORS_MAX  10
#define CD_SCSI_USER_SECTOR_LEN_BYTES  2048
tU32 CD_SCSI_IF_u32Read(tDevCDData *pShMem, tU32 u32LBA, tU16 u16Sectors,
                        tU8 *pu8Buffer)
{
  tU32 u32Ret;
  tU8 *pu8B;
  tU16 u16SectorsToRead = 0;
  tU16 u16SectorsRead   = 0; /*entire number of sectors was read from device*/
  tU32 u32LBAToRead;
  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32Read, u32LBA, 0, u16Sectors, pu8Buffer);

  if(NULL != pu8Buffer)
  {
    do
    {
      sg_io_hdr_t rIO;
      unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
      unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];

      u16SectorsToRead =
      (u16Sectors - u16SectorsRead) > CD_SCSI_READ_SECTORS_MAX ?
      CD_SCSI_READ_SECTORS_MAX : (u16Sectors - u16SectorsRead);

      pu8B = &pu8Buffer[CD_SCSI_USER_SECTOR_LEN_BYTES * u16SectorsRead];
      u32LBAToRead = u32LBA + (tU32)u16SectorsRead;

      InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), pu8B,
                      u16SectorsToRead * CD_SCSI_USER_SECTOR_LEN_BYTES, ucSense,
                      sizeof(ucSense), -1);

      rIO.cmdp[0] = READ_10; //0x28
      rIO.cmdp[1] = 0x08; //DPO =0, FUA (force unit access) = 1; RELADR = 0
      rIO.cmdp[2] = CD_32BIT_B3(u32LBAToRead);
      rIO.cmdp[3] = CD_32BIT_B2(u32LBAToRead);
      rIO.cmdp[4] = CD_32BIT_B1(u32LBAToRead);
      rIO.cmdp[5] = CD_32BIT_B0(u32LBAToRead);
      rIO.cmdp[7] = CD_16BIT_MSB(u16SectorsToRead);
      rIO.cmdp[8] = CD_16BIT_LSB(u16SectorsToRead);

      u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SR);

      if(u32Ret == OSAL_E_NOERROR)
      {
        u16SectorsRead += u16SectorsToRead;

        CD_TR
        vTraceHex(pShMem, "READ_10_DAT", pu8B,
                        (tInt)u16SectorsToRead * CD_SCSI_USER_SECTOR_LEN_BYTES,
                        (tInt)32);
      }
      else //if(u32Ret == OSAL_E_NOERROR)
      {
        CD_PRINTF_ERRORS("Cannot read data from Device [%s], "
                         "OSALERR [%s]",
                         pShMem->rCD.rCDDrive.szDevName,
                         OSAL_coszErrorText(u32Ret));
      } //else //if(u32Ret == OSAL_E_NOERROR)
    }
    while((u16SectorsRead < u16Sectors) && (u32Ret == OSAL_E_NOERROR));
  }
  else //if(NULL != pu8B)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL != pu8MediaStatus)

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32Read, u32Ret, u16SectorsRead,
                    u16SectorsToRead, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32PlayRange
 * PARAMETER:    Shared Memory Pointer
 *               IN: start ZLBA
 *               IN: End ZLBA
 * RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  sends play
 * HISTORY:
 ******************************************************************************/
tU32 CD_SCSI_IF_u32PlayRange(tDevCDData *pShMem, tU32 u32StartZLBA,
                             tU32 u32EndZLBA)
{
  tU32 u32Ret;
  CD_sMSF_type rStartMSF =
  { 0};
  CD_sMSF_type rEndMSF =
  { 0};

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32PlayRange, u32StartZLBA, u32EndZLBA, 0,
                    0);

  if((tU32)-1 == u32StartZLBA)
  { // play from current position
    rStartMSF.u8Min = 0xFF;
    rStartMSF.u8Sec = 0xFF;
    rStartMSF.u8Frm = 0xFF;
  }
  else //if((tU32)-1 == u32StartZLBA)
  {
    vZLBA2MSF(pShMem, u32StartZLBA, &rStartMSF);
  } //else //if((tU32)-1 == u32StartZLBA)
  vZLBA2MSF(pShMem, u32EndZLBA, &rEndMSF);

  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];
    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    rIO.cmdp[0] = CD_SCSI_IF_SG_CMD_PLAYAUDIOMSF;
    rIO.cmdp[3] = rStartMSF.u8Min;
    rIO.cmdp[4] = rStartMSF.u8Sec;
    rIO.cmdp[5] = rStartMSF.u8Frm;
    rIO.cmdp[6] = rEndMSF.u8Min;
    rIO.cmdp[7] = rEndMSF.u8Sec;
    rIO.cmdp[8] = rEndMSF.u8Frm;
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SR);

  }

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32PlayRange, u32Ret, u32StartZLBA,
                    u32EndZLBA, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32PlayTNO
* PARAMETER:    IN: start track
*               IN: End track
* RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  send play_TI to SCSI
 * HISTORY:
 ******************************************************************************/
tU32 CD_SCSI_IF_u32PlayTNO(tDevCDData *pShMem, tU8 u8StartTrack, tU8 u8EndTrack)
{
  tU32 u32Ret;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32PlayTNO, u8StartTrack, u8EndTrack, 0, 0);

  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];
    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    rIO.cmdp[0] = CD_SCSI_IF_SG_CMD_PLAYAUDIO_TI; /*0x48*/

    rIO.cmdp[4] = u8StartTrack;
    rIO.cmdp[5] = 1; //start index

    rIO.cmdp[7] = u8EndTrack + 1;
    rIO.cmdp[8] = 0; //end index
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SR);
  }

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32PlayTNO, u32Ret, u8StartTrack,
                    u8EndTrack, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_u32Scan
* PARAMETER:    IN: start ZLBA
*               IN: TRUE: fast backward
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  send scan to SCSI
******************************************************************************/
tU32 CD_SCSI_IF_u32Scan(tDevCDData *pShMem, tU32 u32StartZLBA, tBool bBackward)
{
  tU32 u32Ret;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32Scan, u32StartZLBA, 0, bBackward, 0);

  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_A0_BF_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];
    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    rIO.cmdp[0] = CD_SCSI_IF_SG_CMD_SCAN;  //0xBA
    /*bit 4 == 0 fwd, 1 bwd, NOT SUPPORTED ? 10 ffwd, 11 fbwd*/
    rIO.cmdp[1] = bBackward ? 0x01 << 4 : 0x00; //FAST
    rIO.cmdp[2] = CD_32BIT_B3(u32StartZLBA);
    rIO.cmdp[3] = CD_32BIT_B2(u32StartZLBA);
    rIO.cmdp[4] = CD_32BIT_B1(u32StartZLBA);
    rIO.cmdp[5] = CD_32BIT_B0(u32StartZLBA);
    rIO.cmdp[9] = 0x00; //LBA, bit 6,7 = 00; MSF=01, TNO=10
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SR);
  }

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32Scan, u32Ret, u32StartZLBA, bBackward, 0,
                    0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32PauseResume
* PARAMETER:    bResume: TRUE==resume from last position
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  send pause or resume to  SCSI
******************************************************************************/
tU32 CD_SCSI_IF_u32PauseResume(tDevCDData *pShMem, tBool bResume)
{
  tU32 u32Ret;

  (void)bResume;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32PauseResume, 0, 0, 0, 0);

  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];
    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    rIO.cmdp[0] = CD_SCSI_IF_SG_CMD_PAUSERESUME; //0x4B
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SR);
  }

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32PauseResume, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32Stop
 * PARAMETER:    Shared Memory Pointer
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  send play to SCSI
* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32Stop(tDevCDData *pShMem)
{
  tU32 u32Ret;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32Stop, 0, 0, 0, 0);

  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];
    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    rIO.cmdp[0] = CD_SCSI_IF_SG_CMD_STOP;   //0x4e
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SR);
  }

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32Stop, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32ReadTOC
 * PARAMETER:    Shared Memory Pointer, pointer to Toc
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  send load to SCSI

* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32ReadTOC(tDevCDData *pShMem, CD_sTOC_type *prToc)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  int iSize = 0;
  tU8 u8TOCValid = CD_INVALID;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32ReadTOC, prToc, 0, 0, 0);

  if(NULL != prToc)
  {
    if(CD_INVALID == prToc->u8Valid)
    {
      sg_io_hdr_t rIO;
      unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
      unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
      unsigned char ucResponse[CD_SCSI_IF_SG_TOC_BUFFER_LEN];

      //command initalization
      InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse,
                      sizeof(ucResponse), ucSense, sizeof(ucSense), 7);

      rIO.cmdp[0] = READ_TOC; /*0x43*/
      rIO.cmdp[1] = 0x02; //MSF_MSF;
      rIO.cmdp[2] = 0x00; //FORMAT_TOC;
      u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SR);

      if(u32Ret == OSAL_E_NOERROR)
      {
        unsigned char *pucBuff = ucResponse;
        tU8 u8T;
        tU8 u8TrackCount;
        tU8 u8Track;
        tU32 u32TocLen;

        u32TocLen = (tU32)CD_8TO16(pucBuff[0], pucBuff[1]);
        if(u32TocLen >= CD_SCSI_IF_TOC_ENTRY_LEN)
        {
          CD_TR
          vTraceHex(pShMem, "TOCMSF", pucBuff, 4, 4);
          CD_TR
          vTraceHex(pShMem, "TOCMSF", &pucBuff[4], (tInt)u32TocLen - 4,
                          CD_SCSI_IF_TOC_ENTRY_LEN);

          prToc->u8MinTrack = pucBuff[2];
          prToc->u8MaxTrack = pucBuff[3];
          u8TrackCount = (prToc->u8MaxTrack - prToc->u8MinTrack) + 1;
          pucBuff = &pucBuff[4];
          for(u8T = 0; u8T < u8TrackCount; u8T++)
          {
            pucBuff  = &ucResponse[4 + ((tU32)u8T * CD_SCSI_IF_TOC_ENTRY_LEN)];
            u8Track = pucBuff[2];
            prToc->arTrack[u8Track].u8AdrCtrl    = pucBuff[1];
            prToc->arTrack[u8Track].u32StartZLBA = CD_MSF2ZLBA(pucBuff[5],
                                                               pucBuff[6],
                                                               pucBuff[7]);
          } //for(u8T = 0; u8T < u8TrackCount; u8T++)

          //read end of disk
          InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse,
                          sizeof(ucResponse), ucSense, sizeof(ucSense), 7);

          rIO.cmdp[0] = READ_CAPACITY; /*0x25*/
          u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SR);

          if((u32Ret == OSAL_E_NOERROR))
          {
            pucBuff = ucResponse;
            CD_TR
            vTraceHex(pShMem, "CAP", &pucBuff[0], 4,
                      CD_SCSI_IF_TOC_ENTRY_LEN);
            prToc->u32LastZLBA = CD_8TO32(pucBuff[0],
                                          pucBuff[1], pucBuff[2], pucBuff[3]);
            prToc->u8Valid = CD_VALID;
            u8TOCValid = prToc->u8Valid;
          }
          else //if((u32Ret == OSAL_E_NOERROR))
          {
            prToc->u8Valid = CD_INVALID;
            u8TOCValid = prToc->u8Valid;
            CD_PRINTF_ERRORS("Cannot read Capacity from Device [%s]",
                             pShMem->rCD.rCDDrive.szDevName);
            CD_PRINTF_ERRORS("TOC size [%d] Bytes, "
                             "OSALERR [%s]",
                             (int)iSize, OSAL_coszErrorText(u32Ret));
          } //else //if((u32Ret == OSAL_E_NOERROR))
        }
        else //if(u32TocLen >= CD_SCSI_IF_TOC_ENTRY_LEN)
        {
          prToc->u8Valid = CD_INVALID;
          u8TOCValid = prToc->u8Valid;
          CD_PRINTF_ERRORS("Length of TOC = 0x%04X from Device [%s]",
                           (unsigned int)u32TocLen,
                           pShMem->rCD.rCDDrive.szDevName);
        } //else //if(u32TocLen >= CD_SCSI_IF_TOC_ENTRY_LEN)
      }
      else //if((u32Ret == OSAL_E_NOERROR)
      {
        prToc->u8Valid = CD_INVALID;
        u8TOCValid = prToc->u8Valid;
        CD_PRINTF_ERRORS("Cannot read TOC from Device [%s], "
                         "OSALERR [%s]",
                         pShMem->rCD.rCDDrive.szDevName,
                         OSAL_coszErrorText(u32Ret));

        /*if(OSAL_E_INVALIDVALUE == u32Ret)
         { //no cd inserted, remap error
         u32Ret = OSAL_E_MEDIA_NOT_AVAILABLE;
         } //if(OSAL_E_INVALIDVALUE == u32Ret)
         */
      } //else //if((u32Ret == OSAL_E_NOERROR)
    } //if(CD_INVALID == prToc->u8Valid)
  }
  else //if(NULL != prToc)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL != prToc)

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32ReadTOC, u32Ret, iSize, u8TOCValid, 0,
                    0);
  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CD_SCSI_IF_u32ReadCDText
 * PARAMETER:    Shared Memory Pointer, Text-Buffer, Length of buffer
 *               OUT: read size of cd-text buffer
 * RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  send load to SCSI
 * HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32ReadCDText(tDevCDData *pShMem, tU8 *pu8Buffer,
                              tU32 u32BufferLen, tU32 *pu32CDTextSize)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  tU32 u32DataLen = 0;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32ReadCDText, pu8Buffer, u32BufferLen,
                    pu32CDTextSize, 0);

  if(NULL != pu8Buffer)
  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];

    //command initalization
    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), pu8Buffer, u32BufferLen,
                    ucSense, sizeof(ucSense), 7);

    rIO.cmdp[0] = READ_TOC; /*0x43*/
    rIO.cmdp[1] = 0x02; //MSF_MSF;
    rIO.cmdp[2] = 0x05; //FORMAT_CDTEXT;

    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SR);

    if(u32Ret == OSAL_E_NOERROR)
    {
      u32DataLen = CD_8TO16(pu8Buffer[0], pu8Buffer[1]);
      CD_TR
      vTraceHex(pShMem, "CDTXT", pu8Buffer, 4, 4);
      if(u32DataLen > 0)
      {
        CD_TR
        vTraceHex(pShMem, "CDTXT", &pu8Buffer[4], (tInt)(u32DataLen), 18);
      }

      if(NULL != pu32CDTextSize)
      {
        *pu32CDTextSize = u32DataLen;
      } //if(NULL != pu32CDTextSize)
    }
    else //if((u32Ret == OSAL_E_NOERROR) && (iSize > 4))
    {
      CD_PRINTF_ERRORS("Cannot read CD-Text from Device [%s]: OSALERR [%s]",
                       pShMem->rCD.rCDDrive.szDevName,
                       OSAL_coszErrorText(u32Ret));
    } //else //if((u32Ret == OSAL_E_NOERROR) && (iSize > 4))
  }
  else //if(NULL != prToc)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL != prToc)

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32ReadCDText, u32Ret, u32DataLen, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32Inquiry
* PARAMETER:    Shared Memory Pointer, OUT: Inquiry
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  read version infos
* HISTORY: 
* 16 Mar,2016 - SWVersion and HWVersion data collected from 33&34 bytes and 35&36 bytes of response buffer respectively.
*             - Kranthi Kiran(RBEI/ECF5)
************************************************************************************************************************/
tU32 CD_SCSI_IF_u32Inquiry(tDevCDData *pShMem, CD_sInquiry_type *prInquiry)
{
  tU32 u32Ret = OSAL_E_NOERROR;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32Inquiry, prInquiry, 0, 0, 0);

  if(NULL != prInquiry)
  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_00_1F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_INQUIRY_BUFFER_LEN];

    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    rIO.cmdp[0] = INQUIRY; /*0x12*/
    rIO.cmdp[4] = CD_SCSI_IF_SG_INQUIRY_BUFFER_LEN;
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SG);

    if(u32Ret == OSAL_E_NOERROR)
    {
      CD_TR
      vTraceHex(pShMem, "INQ", ucResponse,
                CD_SCSI_IF_SG_INQUIRY_BUFFER_LEN,
                16);

      memcpy(prInquiry->au8Vendor8, &ucResponse[8], 8);
      prInquiry->au8Vendor8[8] = 0;
      memcpy(prInquiry->au8Product16, &ucResponse[16], 16);
      prInquiry->au8Product16[16] = 0;
      memcpy(prInquiry->au8SWVersion2, &ucResponse[32], 2);
      memcpy(prInquiry->au8HWVersion2, &ucResponse[34], 2);
      prInquiry->u8Valid = CD_VALID;
    }
    else //if(u32Ret == OSAL_E_NOERROR)
    {
      CD_PRINTF_ERRORS("Cannot Inquiry Device [%s], "
                       "OSALERR [%s]",
                       pShMem->rCD.rCDDrive.szDevName,
                       OSAL_coszErrorText(u32Ret));
    } //else //if(u32Ret == OSAL_E_NOERROR)

  }
  else //if (NULL != prInquiry)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if (NULL != prInquiry)

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32Inquiry, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32GetLoaderStatus
 * PARAMETER:    Shared Memory Pointer, OUT: Loader status
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  read Status Event Notification for MEDIA_CLASS
* HISTORY:
*****************************************************************************/
tU32 CD_SCSI_IF_u32GetLoaderStatus(tDevCDData *pShMem,
                                   tU8 *pu8InternalLoaderState)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  static tU8 u8LastInternalLoaderState = CD_INTERNAL_LOADERSTATE_INITIALIZED;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32GetLoaderStatus, pu8InternalLoaderState,
                    0, 0, 0);

  if(NULL != pu8InternalLoaderState)
  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];
    tU8    u8SCSIStatus;
    tU16   u16DRVStatus;
    tU8    u8SCSISenseKey;
    tU16   u16SCSIASCASCQ;
    tBool  bCheckSK;
    tBool  bSKAvail;

    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), 7);

    rIO.cmdp[0] = CD_SCSI_IF_SG_CMD_GET_EVENT_STATUS_NOTIFICATION; //0x4A
    rIO.cmdp[1] = 0x01; //immediate
    rIO.cmdp[4] = 0x10; //notifaction class request bitmask: MEDIA_CLASS
    rIO.cmdp[9] = CD_SCSI_CONTROL_VENDOR_ID;

    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SG);

    u8SCSIStatus  = (rIO.status & STATUS_MASK) >> 1;
    u16DRVStatus  = rIO.driver_status;

    bSKAvail = bGetSenseKey(rIO.status, //not shifted!
                            u16DRVStatus, rIO.sbp, rIO.sb_len_wr,
                            &u8SCSISenseKey, &u16SCSIASCASCQ);

    *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_UNKNOWN;
    bCheckSK = u8SCSIStatus != GOOD || u16DRVStatus != CD_SCSI_DRIVER_OK;

    if(bCheckSK && bSKAvail)
    {
      switch(u8SCSISenseKey)
      {
      case HARDWARE_ERROR:
        switch(u16SCSIASCASCQ)
        {
        case CD_SCSI_ASC_ASCQ_5300: // MEDIA_LOAD_OR_EJECT_FAILED
          switch(u8LastInternalLoaderState)
          {
          case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
          case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
          case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
          case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
          case CD_INTERNAL_LOADERSTATE_EJECT_ERROR:
            u32Ret = OSAL_E_NOERROR;
            *pu8InternalLoaderState =
            (tU8)CD_INTERNAL_LOADERSTATE_EJECT_ERROR;
            CD_PRINTF_U2("EJECT_ERROR - "
                         "internal LoaderState 0x%02x",
                         (unsigned int)*pu8InternalLoaderState);
            break;
          case CD_INTERNAL_LOADERSTATE_NO_CD:
          case CD_INTERNAL_LOADERSTATE_IN_SLOT:
          case CD_INTERNAL_LOADERSTATE_EJECTED:
          case CD_INTERNAL_LOADERSTATE_INITIALIZED:
          case CD_INTERNAL_LOADERSTATE_UNKNOWN:
          case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
          case CD_INTERNAL_LOADERSTATE_LOAD_ERROR:
          default:
            u32Ret = OSAL_E_NOERROR;
            *pu8InternalLoaderState =
            (tU8)CD_INTERNAL_LOADERSTATE_LOAD_ERROR;
            CD_PRINTF_U2("LOAD_ERROR - "
                         "internal LoaderState 0x%02x",
                         (unsigned int)*pu8InternalLoaderState);
            break;
          } //switch(u8LastInternalLoaderState)
          break;
        
        case CD_SCSI_ASC_ASCQ_6500: // VOLTAGE FAULT
          switch(u8LastInternalLoaderState)
          {
          case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
          case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
            u32Ret = OSAL_E_NOERROR;
            *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_CD_INSIDE;
            CD_PRINTF_U2("VOLTAGE FAULT - "
                         "internal LoaderState 0x%02x",
                         (unsigned int)*pu8InternalLoaderState);
            break;
          case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
          case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
          case CD_INTERNAL_LOADERSTATE_EJECT_ERROR:
          case CD_INTERNAL_LOADERSTATE_NO_CD:
          case CD_INTERNAL_LOADERSTATE_IN_SLOT:
          case CD_INTERNAL_LOADERSTATE_EJECTED:
          case CD_INTERNAL_LOADERSTATE_INITIALIZED:
          case CD_INTERNAL_LOADERSTATE_UNKNOWN:
          case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
          case CD_INTERNAL_LOADERSTATE_LOAD_ERROR:
          default:
            u32Ret = OSAL_E_NOERROR;
            CD_PRINTF_U2("VOLTAGE FAULT - "
                         "internal LoaderState unchanged 0x%02x",
                         (unsigned int)*pu8InternalLoaderState);
            break;
          } //switch(u8LastInternalLoaderState)
          break; //case CD_SCSI_ASC_ASCQ_6500
          
        default:
          *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_UNKNOWN;
        } //switch(u16SCSIASCASCQ)
        break;

      case NOT_READY:
        switch(u16SCSIASCASCQ)
        {
          case CD_SCSI_ASC_ASCQ_0401:
            // LOGICAL_UNIT_IS_IN_PROCESS_OF_BECOMING_READY
            switch(u8LastInternalLoaderState)
            {
            case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
            case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
            case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
              u32Ret = OSAL_E_NOERROR;
              *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_CD_INSIDE;
              CD_PRINTF_U2("BECOMING READY WITH CD INSERTED - "
                           "internal LoaderState 0x%02x",
                           (unsigned int)*pu8InternalLoaderState);
              break;
            case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
            case CD_INTERNAL_LOADERSTATE_EJECT_ERROR:
            case CD_INTERNAL_LOADERSTATE_NO_CD:
            case CD_INTERNAL_LOADERSTATE_IN_SLOT:
            case CD_INTERNAL_LOADERSTATE_EJECTED:
            case CD_INTERNAL_LOADERSTATE_INITIALIZED:
            case CD_INTERNAL_LOADERSTATE_UNKNOWN:
            case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
            case CD_INTERNAL_LOADERSTATE_LOAD_ERROR:
            default:
              u32Ret = OSAL_E_NOERROR;
              CD_PRINTF_U2("BECOMING READY WITHOUT CD INSERTED - "
                           "internal LoaderState 0x%02x",
                           (unsigned int)*pu8InternalLoaderState);
              break;
            } //switch(u8LastInternalLoaderState)
          break; //case CD_SCSI_ASC_ASCQ_0401:

          case CD_SCSI_ASC_ASCQ_3001: // CANNOT_READ_MEDIUM_UNKNOWN_FORMAT
            u32Ret = OSAL_E_NOERROR;
            *pu8InternalLoaderState =
            (tU8)CD_INTERNAL_LOADERSTATE_CD_UNREADABLE;
            CD_PRINTF_U2("MEDIUM_UNKOWN_FORMAT - "
                         "internal LoaderState 0x%02x",
                         (unsigned int)*pu8InternalLoaderState);
          break; //case CD_SCSI_ASC_ASCQ_3001:
        default:
          ;
        } //switch(u16SCSIASCASCQ)
        break; //case NOT_READY:

      default:
        ;
      } //switch(u8SCSISenseKey)
    }
    else //if(!bCheckSK)
    {
      //no sense key to check
      tU32 u32Size = CD_8TO16(ucResponse[0], ucResponse[1]);
      tU8   u8MediaEvent  = ucResponse[4 + 0] & 0x0F; /*mmc3r10g.pdf: table 94*/
      tU8   u8MediaStatus = ucResponse[4 + 1];        /*mmc3r10g.pdf: table 95*/
      tBool  bMediaPresent = 0 != (u8MediaStatus & 0x02);  /* table 95*/
      tBool  bDoorOpen     = 0 != (u8MediaStatus & 0x01);  /* table 95*/

      CD_TR
      vTraceHex(pShMem, "STATUSEVNOT_HDR", ucResponse, 4, 4);
      CD_TR
      vTraceHex(pShMem, "STATUSEVNOT_DAT", &ucResponse[4], (tInt)u32Size,
                (tInt)u32Size);

      /*
        MASCA Status           Media Status of SCSI                  Comment
                              Media Event	    Media Present Door Open	
        No CD                 0 NoChg         0	            0	        No media at all
       Inserting in progress 4 MediaChanged	 0	            1	        User �feeds� the device with a media
        CD position           4 MediaChanged  1	            0	        TOC is now read and medium can be accessed
        CD in play position   2 NewMedia	    1	            0	        TOC is now read and medium can be accessed
        Ejecting in progress	1 EjectRequest	1	            1	        Ejecting request from application triggered
        In-Slot	              3 MediaRemoval	1	            1	        CD is still In-Slot and needs to be removed/reinserted by user or can be reinserted on application request.
        CD is ejected         3 MediaRemoval	0	            0	        Medium was removed from slot
      */

      switch(u8MediaEvent)
      {
      case CD_SCSI_GETEVENT_MEDIA_EVENT_NOCHG:
        if(!bMediaPresent && !bDoorOpen)
          *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_NO_CD;
        else if(bMediaPresent && !bDoorOpen)
          *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_CD_PLAYABLE;
        break;
      case CD_SCSI_GETEVENT_MEDIA_EVENT_MEDIA_CHANGE:
        if(!bMediaPresent && bDoorOpen)
          *pu8InternalLoaderState = 
          (tU8)CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS;
        else if(bMediaPresent && !bDoorOpen)
          *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_CD_INSIDE;
        break;
      case CD_SCSI_GETEVENT_MEDIA_EVENT_NEW_MEDIA:
        if(bMediaPresent && !bDoorOpen)
          *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_CD_PLAYABLE;
        break;
      case CD_SCSI_GETEVENT_MEDIA_EVENT_EJECT_REQ:
        if(bMediaPresent && bDoorOpen)
          *pu8InternalLoaderState = 
          (tU8)CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS;
        break;
      case CD_SCSI_GETEVENT_MEDIA_EVENT_MEDIA_REMOVAL:
        if(bMediaPresent && bDoorOpen)
          *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_IN_SLOT;
        else if(!bMediaPresent && !bDoorOpen)
          *pu8InternalLoaderState = (tU8)CD_INTERNAL_LOADERSTATE_EJECTED;
        break;
      default:
        ;
      } //switch(u8MediaEvent)

      CD_PRINTF_U2("u8MediaEvent 0x%02x, "
                   "MediaPresent %u, "
                   "DoorOpen %u, "
                   "internal LoaderState 0x%02x",
                   (unsigned int)u8MediaEvent, (unsigned int)bMediaPresent,
                   (unsigned int)bDoorOpen,
                   (unsigned int)*pu8InternalLoaderState);
    } //else//if(!bCheckSK)

    //save important loaderstate
    switch(*pu8InternalLoaderState)
    {
    case CD_INTERNAL_LOADERSTATE_NO_CD:
    case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
    case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
    case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
    case CD_INTERNAL_LOADERSTATE_IN_SLOT:
    case CD_INTERNAL_LOADERSTATE_EJECTED:
      u8LastInternalLoaderState = *pu8InternalLoaderState;
      break;
    case CD_INTERNAL_LOADERSTATE_UNKNOWN:
    case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
    case CD_INTERNAL_LOADERSTATE_LOAD_ERROR:
    case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
    case CD_INTERNAL_LOADERSTATE_EJECT_ERROR:
    case CD_INTERNAL_LOADERSTATE_INITIALIZED:
    default:
      break;
    } //switch(*pu8InternalLoaderState)
  }
  else //if(NULL != pu8InternalLoaderState)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL != pu8InternalLoaderState)

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32GetLoaderStatus, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32GetTemperature
 * PARAMETER:    Shared Memory Pointer, OUT: temperature in �C
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  read temperature from drive
* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32GetTemperature(tDevCDData *pShMem, tInt *ps32Temperature)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  tS32 s32Temperature = 0;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32GetTemperature, ps32Temperature, 0, 0,
                    0);

  if(NULL != ps32Temperature)
  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];

    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), 7);

    rIO.cmdp[0] = LOG_SENSE; //0x4D
    rIO.cmdp[2] = 0x0D;   //spc3r23.pdf, "7.2.13 Temperature log page"
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SG);

    if(u32Ret == OSAL_E_NOERROR)
    {
      s32Temperature = (tS32)ucResponse[9]; //! Degree Celsius + 100 !
      s32Temperature -= 100;  //temperature in degree celsius
      *ps32Temperature = s32Temperature;
    }
    else //if(u32Ret == OSAL_E_NOERROR)
    {
      CD_PRINTF_ERRORS("Cannot LOG SENSE Device [%s], "
                       "OSALERR [%s]",
                       pShMem->rCD.rCDDrive.szDevName,
                       OSAL_coszErrorText(u32Ret));
    } //else //if(u32Ret == OSAL_E_NOERROR)
  }
  else //if(NULL != ps32Temperature)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL != ps32Temperature)

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32GetTemperature, u32Ret, s32Temperature,
                    0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32TestUnitReady
 * PARAMETER:    Shared Memory Pointer OUT:
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  
* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32TestUnitReady(tDevCDData *pShMem, tS32 *ps32UserParam)
{
  tU32 u32Ret = OSAL_E_NOERROR;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32TestUnitReady, 0, 0, 0, 0);

  if(NULL != ps32UserParam)
  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_00_1F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];

    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), -1);

    rIO.cmdp[0] = TEST_UNIT_READY; /*0x00*/
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SG);

    if(u32Ret == OSAL_E_NOERROR)
    {
      *ps32UserParam = 1;
    }
    else //if(u32Ret == OSAL_E_NOERROR)
    {
      CD_PRINTF_ERRORS("Cannot TEST UNIT READY Device [%s], "
                       "OSALERR [%s]",
                       pShMem->rCD.rCDDrive.szDevName,
                       OSAL_coszErrorText(u32Ret));
    } //else //if(u32Ret == OSAL_E_NOERROR)

  }
  else //if(NULL != ps32UserParam)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL != ps32UserParam)

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32TestUnitReady, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32ReadSubChannel
* PARAMETER:    IN: shared mem pointer
*               OUT: PlayPos
*               INT:handle to open cd device
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  read play time

* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32ReadSubChannel(tDevCDData *pShMem, tCD_sSubchannel *prSub,
                                  int hOpenCD)
{
  tU32 u32Ret = OSAL_E_NOERROR;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32ReadSubChannel, prSub, hOpenCD, 0, 0);

  if(NULL != prSub)
  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_SUBCHANNEL_DATA_LEN
                             + CD_SCSI_IF_SG_RESPONSE_HEADER_LEN];

    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), 7);

    rIO.cmdp[0] = CD_SCSI_IF_SG_CMD_READ_SUBCHANNEL; //0x42
    rIO.cmdp[1] = 0x00; //LBA
    rIO.cmdp[2] = 0x40; //SubQ
    rIO.cmdp[3] = 0x01; //Subchannel Paramter List Code = CD current position
    u32Ret = u32SendSGIO(pShMem, &rIO, hOpenCD, CD_SCSI_USE_SR);
    //CD_TR vTraceHex(pShMem, "SENSEEJECT", ucSense, 16, 16);

    if(u32Ret == OSAL_E_NOERROR)
    {
      tU32 u32Size;

      u32Size = (tU32)CD_8TO16(ucResponse[2], ucResponse[3]);
      if(CD_SCSI_IF_SG_SUBCHANNEL_DATA_LEN == u32Size)
      {
        //check subchannel data format
        if(ucResponse[4] == 0x01)
        {
          prSub->u8AudioStatus = ucResponse[1];
          prSub->u8Index       = ucResponse[4 + 3];
          prSub->u8Track       = ucResponse[4 + 2];
          prSub->u8AdrCtrl     = ucResponse[4 + 1];
          prSub->u32AbsZLBA = CD_8TO32(ucResponse[4 + 4], ucResponse[4 + 5],
                                       ucResponse[4 + 6], ucResponse[4 + 7]);
          if(0x80000000 & prSub->u32AbsZLBA)
          { // avoid negative sectors
            prSub->u32AbsZLBA = 0;
          }
          #ifdef CD_SCSI_SET_FRAMES_TO_ZERO
          prSub->u32AbsZLBA -= prSub->u32AbsZLBA % 75;
          #endif //#ifdef CD_SCSI_SET_FRAMES_TO_ZERO



          prSub->u32RelZLBA = CD_8TO32(ucResponse[4 + 8], ucResponse[4 + 9],
                                       ucResponse[4 + 10], ucResponse[4 + 11]);
          if(0x80000000 & prSub->u32RelZLBA)
          { // avoid negative sectors
            prSub->u32RelZLBA = 0;
          }
          #ifdef CD_SCSI_SET_FRAMES_TO_ZERO



          prSub->u32RelZLBA -= prSub->u32RelZLBA % 75;
          #endif //#ifdef CD_SCSI_SET_FRAMES_TO_ZERO

          CD_PRINTF_U3("Subchannel data: AudioStatus 0x%02X, "
                       "Track %2u, "
                       "Index %2u, "
                       "AdrCtrl 0x%02X, "
                       "AbsZLBA 0x%08X == %06u, "
                       "RelZLBA 0x%08X == %06u, ",
                       (unsigned int)prSub->u8AudioStatus,
                       (unsigned int)prSub->u8Track,
                       (unsigned int)prSub->u8Index,
                       (unsigned int)prSub->u8AdrCtrl,
                       (unsigned int)prSub->u32AbsZLBA,
                       (unsigned int)prSub->u32AbsZLBA,
                       (unsigned int)prSub->u32RelZLBA,
                       (unsigned int)prSub->u32RelZLBA
                    );
        }
        else //if(ucResponse[4] == 0x01)
        {
          CD_PRINTF_ERRORS("Wrong Subchannel data format (%u), should be (1)",
                           (unsigned int)ucResponse[4]);
          u32Ret = OSAL_E_NOTSUPPORTED;
        } //else //if(ucResponse[4] == 0x01)
      }
      else //if(CD_SCSI_IF_SG_SUBCHANNEL_DATA_LEN == u32Size)
      {
        CD_PRINTF_ERRORS("Wrong Subchannel data len (%u), should be (%u)",
                         (unsigned int)u32Size,
                         (unsigned int)CD_SCSI_IF_SG_SUBCHANNEL_DATA_LEN);
        u32Ret = OSAL_E_NOTSUPPORTED;
      } //else //if(CD_SCSI_IF_SG_SUBCHANNEL_DATA_LEN == u32Size)
    }
    else //if(u32Ret == OSAL_E_NOERROR)
    {
      CD_PRINTF_ERRORS("Error: Read subchannel [%s], "
                       "OSALERR [%s]",
                       pShMem->rCD.rCDDrive.szDevSRName,
                       OSAL_coszErrorText(u32Ret));
    } //else //if(u32Ret == OSAL_E_NOERROR)

    prSub->u8Valid = OSAL_E_NOERROR == u32Ret ? CD_VALID : CD_INVALID;
  }
  else //if (NULL != prSub)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if (NULL != prSub)

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32ReadSubChannel, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32ModeSense
 * PARAMETER:    Shared Memory Pointer, OUT: Media type
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  mode sense

* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32ModeSense(tDevCDData *pShMem, tU8 *pu8InternalMediaType)
{
  tU32 u32Ret;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32ModeSense, pu8InternalMediaType,
                    0, 0, 0);

  if(NULL != pu8InternalMediaType)
  {
    sg_io_hdr_t rIO;
    unsigned char ucCmd[CD_SCSI_IF_SG_CMD_20_5F_LEN];
    // 6 byte command unsigned char ucCmd[CD_SCSI_IF_SG_CMD_00_1F_LEN];
    unsigned char ucSense[CD_SCSI_IF_SG_SENSE_BUFFER_LEN];
    unsigned char ucResponse[CD_SCSI_IF_SG_RESPONSE_BUFFER_LEN];

    InitSGCmdPacket(&rIO, ucCmd, sizeof(ucCmd), ucResponse, sizeof(ucResponse),
                    ucSense, sizeof(ucSense), 7);

    rIO.cmdp[0] = MODE_SENSE_10; //0x5A, SPC 6.10 + MMC 6.3.11
    rIO.cmdp[1] = 1 << 3; //DBD = 1, bit 3;
    rIO.cmdp[2] = 0x2A; //Page mechanical status.  MMC 6.3.11
    //rIO.cmdp[4] = 0x00; //allow all (bits 0,1)
    u32Ret = u32SendSGIO(pShMem, &rIO, -1, CD_SCSI_USE_SG);
    if(OSAL_E_NOERROR == u32Ret)
    {
      tU8 u8MT = ucResponse[2];
      switch(u8MT)
      {
      case 0x01:
        *pu8InternalMediaType = CD_INTERNAL_MEDIA_TYPE_DATA_MEDIA;
        break;
      case 0x02:
        *pu8InternalMediaType = CD_INTERNAL_MEDIA_TYPE_AUDIO_MEDIA;
        break;
      case 0x03:
        *pu8InternalMediaType = CD_INTERNAL_MEDIA_TYPE_MIXED_MODE_MEDIA;
        break;
      default:
        *pu8InternalMediaType = CD_INTERNAL_MEDIA_TYPE_UNKNOWN_MEDIA;
        break;
      } //switch(u8MT)
    }
    else //if(OSAL_E_NOERROR == u32Ret)
    {
      *pu8InternalMediaType = CD_INTERNAL_MEDIA_TYPE_UNKNOWN_MEDIA;
    } //else //if(OSAL_E_NOERROR == u32Ret)
  }
  else //if(NULL != pu8InternalMediaType) 
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL != pu8InternalMediaType) 

  CD_TRACE_LEAVE_U3(
                   CDID_CD_SCSI_IF_u32ModeSense, u32Ret,
                   NULL != pu8InternalMediaType ?
                   (tU32)*pu8InternalMediaType : (tU32)-1, 0, 0,
                   0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32IsCDAccessible
* PARAMETER:    Shared memory pointer
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  try to open cdrom device
* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32IsCDAccessible(tDevCDData *pShMem)
{
  tU32 u32Ret = OSAL_E_UNKNOWN;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U3(CDID_CD_SCSI_IF_u32IsCDAccessible, 0, 0, 0, 0);

  //try to open device /dev/sgx
  {
    int fd;
    fd = open(pShMem->rCD.rCDDrive.szDevSGName, O_RDWR | O_NONBLOCK);
    if(-1 != fd)
    {
      u32Ret = OSAL_E_NOERROR;
      (void)close(fd);
    }
    else
    {
      CD_PRINTF_ERRORS("Error open device [%s]: [%s]\n",
                       pShMem->rCD.rCDDrive.szDevSGName, strerror(errno));
      u32Ret = CD_SCSI_IF_u32FindCDDrive(pShMem);
    }
  }

  CD_TRACE_LEAVE_U3(CDID_CD_SCSI_IF_u32IsCDAccessible, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32Init
* PARAMETER:    pointer to shared mem
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  initializes data structire for first use
* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32FindCDDrive(tDevCDData *pShMem)
{
  tU32 u32Result;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U2(CDID_CD_SCSI_IF_u32FindCDDrive, 0, 0, 0, 0);

  { //get cd drive name
    CD_tsDevName *prCDDrive;
    prCDDrive = &pShMem->rCD.rCDDrive;
    u32Result = u32GetFirstCD(pShMem, prCDDrive);

    CD_PRINTF_U3("CD-Drive Result [0x%08X]"
                 " SRFound [%d]"
                 " SGFound [%d]"
                 " DefaultSRUsed [%d]"
                 " DefaultSGUsed [%d]"
                 " SRCouldBeOpened [%d]"
                 " SGCouldBeOpened [%d]"
                 " SRName [%s]"
                 " SGName [%s]\n",
                 (unsigned int)u32Result,
                 (int)prCDDrive->bSRFound,
                 (int)prCDDrive->bSGFound,
                 (int)prCDDrive->bDefaultSRNameUsed,
                 (int)prCDDrive->bDefaultSGNameUsed,
                 (int)prCDDrive->bSRCouldBeOpened,
                 (int)prCDDrive->bSGCouldBeOpened, prCDDrive->szDevSRName,
                 prCDDrive->szDevSGName);
    if(OSAL_E_NOERROR != u32Result)
    {
      CD_PRINTF_ERRORS("No usable CD-Drive found");
    } //if(OSAL_E_NOERROR != u32Result)

  }

  CD_TRACE_LEAVE_U2(CDID_CD_SCSI_IF_u32FindCDDrive, u32Result, 0, 0, 0, 0);
  return u32Result;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32Init
* PARAMETER:    pointer to shared mem
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  initializes data structire for first use
* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32Init(tDevCDData *pShMem)
{
  tU32 u32Result;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U2(CDID_CD_SCSI_IF_u32Init, 0, 0, 0, 0);

  u32Result = CD_u32CacheInit(pShMem);
  if(OSAL_E_NOERROR == u32Result)
  { //get cd drive name
    tU32 u32R;
    u32R = CD_SCSI_IF_u32FindCDDrive(pShMem);
    if(OSAL_E_NOERROR != u32R)
    { // do something usefull
      ;
    } //if(OSAL_E_NOERROR != u32R)
  }
  else //if(OSAL_E_NOERROR == u32Result)
  {
    CD_PRINTF_ERRORS("Cache could not be initalized");
  } //else //if(OSAL_E_NOERROR == u32Result)

  CD_TRACE_LEAVE_U2(CDID_CD_SCSI_IF_u32Init, u32Result, 0, 0, 0, 0);
  return u32Result;
}

/*****************************************************************************
* FUNCTION:     CD_SCSI_IF_u32Destroy
* PARAMETER:    pointer to shared mem
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  destroys data structures
* HISTORY:
******************************************************************************/
tU32 CD_SCSI_IF_u32Destroy(tDevCDData *pShMem)
{
  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_CD_SCSI_IF_u32Destroy, 0, 0, 0, 0);
  CD_vCacheDestroy(pShMem);
  CD_TRACE_LEAVE_U4(CDID_CD_SCSI_IF_u32Destroy, OSAL_E_NOERROR, 0, 0, 0, 0);
  return OSAL_E_NOERROR;
}

#ifdef __cplusplus
}
#endif
