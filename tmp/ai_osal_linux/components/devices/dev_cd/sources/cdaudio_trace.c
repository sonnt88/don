/************************************************************************
 | $Revision: 1.0 $
 | $Date: 2013/01/01 $
 |************************************************************************
 | FILE:         cdaudio_trace.c
 | PROJECT:      GEN3
 | SW-COMPONENT: OSAL I/O Device
 |------------------------------------------------------------------------
 | DESCRIPTION:
 |  This file contains the /dev/cdaudio device implementation
 |------------------------------------------------------------------------
 | Date      | Modification               | Author
 | 2013      | GEN3 VW MIB                | srt2hi
 |
 |************************************************************************
 |************************************************************************/

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

/* Interface for variable number of arguments */
/* Basic OSAL includes */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"
#include "cd_funcenum.h"
#include "cd_main.h"
#include "cdaudio_trace.h"
#include "cdaudio.h"
#include "cdaudio_cdtext.h"
#include "cd_cache.h"
#include "cd_scsi_if.h"

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: module-local)
 |-----------------------------------------------------------------------*/
#define CDAUDIO_PRINTF_BUFFER_SIZE 256
#define CDAUDIO_TRACE_BUFFER_SIZE  256
#define CDAUDIO_GET_U32_LE(PU8) (((tU32)(PU8)[0]) | (((tU32)(PU8)[1])<<8)\
                          | (((tU32)(PU8)[2])<<16) | (((tU32)(PU8)[3])<<24))

/************************************************************************
 |typedefs (scope: module-local)
 |----------------------------------------------------------------------*/

/************************************************************************
 | variable inclusion  (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: global)
 |-----------------------------------------------------------------------*/
/*trace speed up - if FALSE, calling of tracing functions are suppressed*/
/*is set automatically to TRUE, if trace level >= User1 is enabled at startup*/
//tBool pShMem->CDAUDIO_bTraceOn = TRUE; // srt2hi migrate

//extern BOOL UTIL_trace_isActive(TraceData* data);
/************************************************************************
 | variable definition (scope: module-local)
 |-----------------------------------------------------------------------*/
static volatile tBool gTraceCmdThreadIsRunning = FALSE;
static tU8 gu8TraceBuffer[CDAUDIO_TRACE_BUFFER_SIZE]; /*not sychronyzed!*/
static OSAL_tIODescriptor ghDevice = OSAL_ERROR;

/************************************************************************
 |function prototype (scope: module-local)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function implementation (scope: module-local)
 |-----------------------------------------------------------------------*/

/*****************************************************************************
 *
 * FUNCTION:    coszGetErrorText
 *
 * DESCRIPTION: get pointer to osal error text
 *
 *
 *****************************************************************************/
static const char* pGetErrTxt()
{
  return(const char*)OSAL_coszErrorText(OSAL_u32ErrorCode());
}

/*****************************************************************************
 *
 * FUNCTION:
 *     getArgList
 *
 * DESCRIPTION:
 *     This stupid function is used for satifying L I N T
 *
 *
 * PARAMETERS: *va_list pointer to a va_list
 *
 * RETURNVALUE:
 *     va_list
 *
 *
 *
 *****************************************************************************/
static va_list getArgList(va_list* a)
{
  (void)a;
  return *a;
}

/********************************************************************//**
 *  FUNCTION:      uiCGET
 *
 *  @brief         return time in ms
 *
 *  @return        time in ms
 *
 *  HISTORY:
 *
 ************************************************************************/
static unsigned int uiCGET(void)
{
  return(unsigned int)OSAL_ClockGetElapsedTime();
}
/********************************************************************//**
 *  FUNCTION:      CDAUDIO_vTraceEnter
 *
 *  @brief         enter function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/

tVoid CDAUDIO_vTraceEnter(const tDevCDData *pShMem, tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                          tenDevCDTrcFuncNames enFunction, tU32 u32Par1,
                          tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDAUDIO_bTraceOn : TRUE;
  if(bTrace)
  {
    if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      tUInt uiIndex;
      tU8 au8Buf[4 * sizeof(tU32) + 8 * sizeof(tU32)]; //stack?
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_ENTER);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], enFunction);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par1);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par2);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par3);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par4);
      uiIndex += sizeof(tU32);

      // trace the stuff
      // be aware uiIndex is not greater than aray size!
      // I can not test it at runtime,
      // because LINT throws a pointless warning.
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  }      //if(pShMem->CDAUDIO_bTraceOn)
}

/********************************************************************//**
 *  FUNCTION:      CDAUDIO_vTraceLeave
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDAUDIO_vTraceLeave(const tDevCDData *pShMem, tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                          tenDevCDTrcFuncNames enFunction, tU32 u32OSALResult,
                          tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
                          tU32 u32Par4)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDAUDIO_bTraceOn : TRUE;
  if(bTrace)
  {
    if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      tUInt uiIndex;
      tU8 au8Buf[4 * sizeof(tU8) + 9 * sizeof(tU32)]; //stack?
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_LEAVE);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)enFunction);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32OSALResult);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par1);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par2);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par3);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par4);
      uiIndex += sizeof(tU32);

      // trace the stuff
      // be aware uiIndex is not greater than aray size!
      // I can not test it at runtime,
      // because LINT throws a pointless warning.
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  }      //if(pShMem->CDAUDIO_bTraceOn)
}

/*****************************************************************************
 *
 * FUNCTION:
 *     CDAUDIO_vTracePrintf
 *
 * DESCRIPTION:
 *     This function creates the printf-style trace message
 *
 *
 * PARAMETERS:
 *
 * RETURNVALUE:
 *     None
 *
 *
 *
 *****************************************************************************/
tVoid CDAUDIO_vTracePrintf(const tDevCDData *pShMem, tBool bIsError,
                           tU32 u32Class, TR_tenTraceLevel enTraceLevel,
                           tBool bForced, tU32 u32Line, const char* coszFormat,
                           ...)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDAUDIO_bTraceOn : TRUE;
  if(bTrace || bIsError)
  {
    if(bForced || LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      static tU8 au8Buf[CDAUDIO_PRINTF_BUFFER_SIZE + 1]; /*TODO: ? size ?*/
      tUInt uiIndex;
      long lSize;
      size_t tMaxSize;
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();
      va_list argList;

      uiIndex = 0;
      OSAL_M_INSERT_T8(
                      &au8Buf[uiIndex],
                      (tU32)(enTraceLevel == TR_LEVEL_ERRORS 
                                             ? CDAUDIO_TRACE_PRINTF_ERROR :
                                             CDAUDIO_TRACE_PRINTF));
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);

      argList = getArgList(&argList);
      va_start(argList, coszFormat); /*lint !e718 */
      tMaxSize = CDAUDIO_PRINTF_BUFFER_SIZE - uiIndex;
      lSize = vsnprintf((char*)&au8Buf[uiIndex], tMaxSize, coszFormat, argList);
      uiIndex += (tUInt)lSize;
      va_end(argList);

      // trace the stuff
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  }      //if((pShMem->CDAUDIO_bTraceOn) || (bIsError))
}

/********************************************************************//**
 *  FUNCTION:      CDAUDIO_vTraceIoctrl
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDAUDIO_vTraceIoctrl(const tDevCDData *pShMem, tU32 u32Class,
                           TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                           CDAUDIO_enumIoctrlTraceID enIoctrlTraceID,
                           tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
                           tU32 u32Par4)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDAUDIO_bTraceOn : TRUE;
  if(bTrace)
  {
    if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      tUInt uiIndex;
      tU8 au8Buf[4 * sizeof(tU8) + 7 * sizeof(tU32)]; //stack?
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_IOCTRL);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enIoctrlTraceID);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par1);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par2);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par3);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par4);
      uiIndex += sizeof(tU32);

      // trace the stuff
      // be aware uiIndex is not greater than aray size!
      // I can not test it at runtime,
      // because L INT throws a pointless warning.
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  }      //if(pShMem->CDAUDIO_bTraceOn)
}

/********************************************************************//**
 *  FUNCTION:      CDAUDIO_vTraceIoctrlTxt
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDAUDIO_vTraceIoctrlTxt(const tDevCDData *pShMem, tU32 u32Class,
                              TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                              CDAUDIO_enumIoctrlTraceID enIoctrlTraceID,
                              tU32 u32Par1, tU32 u32Par2, const char *pcszTxt)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDAUDIO_bTraceOn : TRUE;
  if(bTrace)
  {
    if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      size_t iSize, iMaxSize;
      tUInt uiIndex;
      tU8 au8Buf[CDAUDIO_TRACE_BUFFER_SIZE]; //stack?
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_IOCTRL);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enIoctrlTraceID);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par1);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par2);
      uiIndex += sizeof(tU32);
      iSize = strlen(pcszTxt) + 1;
      iMaxSize = CDAUDIO_TRACE_BUFFER_SIZE - (uiIndex + 1);
      iSize = CD_MIN(iSize, iMaxSize);
      memcpy(&au8Buf[uiIndex], pcszTxt, iSize);
      uiIndex += iSize;

      // trace the stuff
      // be aware uiIndex is not greater than aray size!
      // I can not test it at runtime,
      // because L INT throws a pointless warning.
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  }      //if(pShMem->CDAUDIO_bTraceOn)
}

/********************************************************************//**
 *  FUNCTION:      CDAUDIO_vTraceIoctrlResult
 *
 *  @brief         error text with IOCtrl-name
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDAUDIO_vTraceIoctrlResult(const tDevCDData *pShMem, tU32 u32Class,
                                 TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                                 tS32 s32OsalIOCtrl, const char *pcszErrorTxt)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDAUDIO_bTraceOn : TRUE;
  if(bTrace)
  {
    if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      tUInt uiIndex;
      tU8 au8Buf[4 * sizeof(tU8) + 5 * sizeof(tU32) + 202]; //stack?
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_IOCTRL_RESULT);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 11);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Time);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)s32OsalIOCtrl);
      uiIndex += sizeof(tU32);
      strncpy((char*)&au8Buf[uiIndex], (const char*)pcszErrorTxt, 200);
      uiIndex += 200;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32)0);
      uiIndex += sizeof(tU8);

      // trace the stuff
      // be aware uiIndex is not greater than array size!
      // I can not test it at runtime,
      // because L INT throws a pointless warning.
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  }      //if(pShMem->CDAUDIO_bTraceOn)
}

/*****************************************************************************
 * FUNCTION:     bIsOpen
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  checks, if device is open

 * HISTORY:
 ******************************************************************************/
static tBool bIsOpen(tDevCDData *pShMem)
{
  tBool bOpen;

  CD_CHECK_SHARED_MEM(pShMem, (tBool)FALSE);

  if(OSAL_ERROR != ghDevice)
  {
    bOpen = TRUE;
  }
  else //if(OSAL_ERROR != ghDevice)
  {
    bOpen = FALSE;
    CDAUDIO_PRINTF_ERRORS("device not open");
  } //else //if(OSAL_ERROR != ghDevice)

  return bOpen;
}

/*****************************************************************************
 * FUNCTION:     bIOCtrlOK
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  checks, if ioctrl succeeded

 * HISTORY:
 ******************************************************************************/
static tBool bIOCtrlOK(tDevCDData *pShMem, tS32 s32Fun, tS32 s32Ret)
{
  tBool bOK = (s32Ret != OSAL_ERROR);
  CD_CHECK_SHARED_MEM(pShMem, FALSE);
  CDAUDIO_PRINTF_IOCTRL_RESULT(s32Fun, bOK ? "SUCCESS" : pGetErrTxt());
  return bOK;
}

/*****************************************************************************
 * FUNCTION:     vPlayInfoNotify
 * PARAMETER:    pvCookie
 * RETURNVALUE:  None
 * DESCRIPTION:  This is a PlayInfo-Callback

 * HISTORY:
 ******************************************************************************/
static tVoid vPlayInfoNotify(tPVoid pvCookie, const OSAL_trPlayInfo* prInfo)
{
  (void)pvCookie; // L I N T

  CDAUDIO_PRINTF_FORCED("vPlayInfoNotify:"
                        " AbsMSF [%02u:%02u:%02u],"
                        " RelTMSF [%02u-%02u:%02u:%02u],"
                        " PlayStatus [0x%08X]",
                        (unsigned int)prInfo->rAbsTrackAdr.u8MSFMinute,
                        (unsigned int)prInfo->rAbsTrackAdr.u8MSFSecond,
                        (unsigned int)prInfo->rAbsTrackAdr.u8MSFFrame,
                        (unsigned int)prInfo->u32TrackNumber,
                        (unsigned int)prInfo->rRelTrackAdr.u8MSFMinute,
                        (unsigned int)prInfo->rRelTrackAdr.u8MSFSecond,
                        (unsigned int)prInfo->rRelTrackAdr.u8MSFFrame,
                        (unsigned int)prInfo->u32StatusPlay);

  switch(prInfo->u32StatusPlay)
  {
  case OSAL_C_S32_PLAY_PAUSED:
    CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: PAUSE");
    break;
  case OSAL_C_S32_PLAY_COMPLETED:
    CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: COMPLETED");
    break;
  case OSAL_C_S32_PLAY_IN_PROGRESS:
    CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: PLAY_IN_PROGRESS");
    break;
  case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_ERROR:
    CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: STOPPED_DUE_TO_ERROR");
    break;
  case OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_UNDERVOLTAGE:
    CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: STOPPED_DUE_TO_UNDERVOLTAGE");
    break;
  case OSAL_C_S32_AUDIO_STATUS_NOT_VALID:
    CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: NOT VALID");
    break;
  case OSAL_C_S32_NO_CURRENT_AUDIO_STATUS:
    CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: NO CURRENT AUDIO STATUS");
    break;
  default:
    CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: - default -");
    break;
  } //switch(prInfo->u32StatusPlay)
}

/*****************************************************************************
 * FUNCTION:     vTraceCmdThread
 * PARAMETER:    pvData
 * RETURNVALUE:  None
 * DESCRIPTION:  This is a Trace Thread handler.

 * HISTORY:
 ******************************************************************************/
static tVoid vTraceCmdThread(tPVoid pvData)
{
  const tU8 *pu8Data = (const tU8*)pvData;
  const tU8 *pu8Par = &pu8Data[3];
  tU8 u8Len = pu8Data[1];
  tU8 u8Cmd = pu8Data[2];
  tU32 u32Ret;
  OSAL_tShMemHandle hShMem = OSAL_ERROR;
  tDevCDData *pShMem = NULL; /*OSAL shared memory*/
  OSAL_tSemHandle hSem = OSAL_C_INVALID_HANDLE;

  (void)pvData;

  u32Ret = CD_u32GetShMem(&pShMem, &hShMem, &hSem);
  if(OSAL_E_NOERROR == u32Ret)
  {
    CDAUDIO_PRINTF_U1("vTraceCmdThread 0x%08X,"
                      "TraceCmd[Len 0x%02X Cmd 0x%02X]",
                      (unsigned int)pu8Data, (unsigned int)u8Len,
                      (unsigned int)u8Cmd);

    switch(u8Cmd)
    {
    
    case CDAUDIO_TRACE_CMD_TRACE_ON:
      pShMem->CDAUDIO_bTraceOn = TRUE;
      break;

    case CDAUDIO_TRACE_CMD_TRACE_OFF:
      pShMem->CDAUDIO_bTraceOn = FALSE;
      break;

    case CDAUDIO_TRACE_CMD_OPEN:
      if(!bIsOpen(pShMem))
      {
        ghDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDAUDIO,
                                OSAL_EN_READWRITE);

        if(ghDevice == OSAL_ERROR)
        {
          CDAUDIO_PRINTF_ERRORS("OPEN: [%s]", pGetErrTxt());
        }
        else //if(ghDevice == OSAL_ERROR)
        {
          CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: OPEN: handle 0x%08X",
                                (unsigned int)ghDevice);
        } //else //if(ghDevice == OSAL_ERROR)
      }
      else //if(!bIsOpen(pShMem))
      {
        CDAUDIO_PRINTF_ERRORS("already open - handle 0x%08X",
                              (unsigned int)ghDevice);
      } //else //if(!bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_CLOSE:
      if(bIsOpen(pShMem))
      {
        tS32 s32Ret = OSAL_s32IOClose(ghDevice);
        if(s32Ret == OSAL_ERROR)
        {
          CDAUDIO_PRINTF_ERRORS("CLOSE: [%s]", pGetErrTxt());
        }
        else //if(ghDevice == OSAL_ERROR)
        {
          CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: CLOSE: Success");
          ghDevice = OSAL_ERROR;
        } //else //if(ghDevice == OSAL_ERROR)
      }
      else //if(bIsOpen(pShMem))
      {
        CDAUDIO_PRINTF_ERRORS("not opened");
      } //else //if(bIsOpen(pShMem))
      break;
    case CDAUDIO_TRACE_CMD_PLAY:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL PLAY [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_PLAY;
        s32Arg = 0;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
      } //if(bIsOpen(pShMem))
      break;
    case CDAUDIO_TRACE_CMD_STOP:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL STOP [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_STOP;
        s32Arg = 0;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
      } //if(bIsOpen(pShMem))
      break;
    case CDAUDIO_TRACE_CMD_PAUSE:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL PAUSE [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_PAUSE;
        s32Arg = 0;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_RESUME:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL RESUME [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_RESUME;
        s32Arg = 0;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_FASTFORWARD:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL FF [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTFORWARD;
        s32Arg = 0;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_FASTBACKWARD:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL FB [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTBACKWARD;
        s32Arg = 0;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
      } //if(bIsOpen(pShMem))
      break;
    case CDAUDIO_TRACE_CMD_SETPLAYRANGE:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL SETPLAYRANGE [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        OSAL_trPlayRange rPlayRange;
        tS32 s32Fun, s32Arg, s32Ret;
        tU8 u8StartTrack = (tU8)CDAUDIO_GET_U32_LE(&pu8Par[0]);
        tU16 u16StartOffset = (tU16)CDAUDIO_GET_U32_LE(&pu8Par[4]);
        tU8 u8EndTrack = (tU8)CDAUDIO_GET_U32_LE(&pu8Par[8]);
        tU16 u16EndOffset = (tU16)CDAUDIO_GET_U32_LE(&pu8Par[12]);

        rPlayRange.rStartAdr.u8Track = u8StartTrack;
        rPlayRange.rStartAdr.u16Offset = u16StartOffset;
        rPlayRange.rEndAdr.u8Track = u8EndTrack;
        rPlayRange.rEndAdr.u16Offset = u16EndOffset;

        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_SETPLAYRANGE;
        s32Arg = (tS32)&rPlayRange;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
        (void)rPlayRange; // satisfy L i n t
      } //if(bIsOpen(pShMem))
      break;
    case CDAUDIO_TRACE_CMD_SETMSF:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL SETMSF [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        OSAL_trAdrRange rPlayRange;
        tS32 s32Fun, s32Arg, s32Ret;
        tU8 u8SM = pu8Par[0];
        tU8 u8SS = pu8Par[1];
        tU8 u8SF = pu8Par[2];
        tU8 u8EM = pu8Par[3];
        tU8 u8ES = pu8Par[4];
        tU8 u8EF = pu8Par[5];

        rPlayRange.rStartAdr.u8MSFMinute = u8SM;
        rPlayRange.rStartAdr.u8MSFSecond = u8SS;
        rPlayRange.rStartAdr.u8MSFFrame = u8SF;
        rPlayRange.rEndAdr.u8MSFMinute = u8EM;
        rPlayRange.rEndAdr.u8MSFSecond = u8ES;
        rPlayRange.rEndAdr.u8MSFFrame = u8EF;

        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_SETMSF;
        s32Arg = (tS32)&rPlayRange;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
        (void)rPlayRange; // L i n T
      } //if(bIsOpen(pShMem))
      break;
    case CDAUDIO_TRACE_CMD_GETCDINFO:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETCDINFO [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        OSAL_trCDAudioInfo rInfo;

        rInfo.u32MinTrack = 0;
        rInfo.u32MaxTrack = 0;
        rInfo.u32TrMinutes = 0;
        rInfo.u32TrSeconds = 0;
        rInfo.u32CdText = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETCDINFO;
        s32Arg = (tS32)&rInfo;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
        {
          CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: CDInfo:"
                                " MinTrack [%02u],"
                                " MaxTrack [%02u],"
                                " Min:Sec  [%02u:%02u],"
                                " CD-Text  [0x%08X]",
                                (unsigned int)rInfo.u32MinTrack,
                                (unsigned int)rInfo.u32MaxTrack,
                                (unsigned int)rInfo.u32TrMinutes,
                                (unsigned int)rInfo.u32TrSeconds,
                                (unsigned int)rInfo.u32CdText);
        }
      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_GETPLAYINFO:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETPLAYINFO [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        OSAL_trPlayInfo rInfo;

        rInfo.u32TrackNumber = 0;
        rInfo.rAbsTrackAdr.u8MSFMinute = 0;
        rInfo.rAbsTrackAdr.u8MSFSecond = 0;
        rInfo.rAbsTrackAdr.u8MSFFrame = 0;
        rInfo.rRelTrackAdr.u8MSFMinute = 0;
        rInfo.rRelTrackAdr.u8MSFSecond = 0;
        rInfo.rRelTrackAdr.u8MSFFrame = 0;
        rInfo.u32StatusPlay = 0;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETPLAYINFO;
        s32Arg = (tS32)&rInfo;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
        {
          CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: PlayInfo:"
                                " Track [%02u],"
                                " AbsMSF [%02u:%02u:%02u],"
                                " RelMSF [%02u:%02u:%02u],"
                                " PlayStatus [0x%08X]",
                                (unsigned int)rInfo.u32TrackNumber,
                                (unsigned int)rInfo.rAbsTrackAdr.u8MSFMinute,
                                (unsigned int)rInfo.rAbsTrackAdr.u8MSFSecond,
                                (unsigned int)rInfo.rAbsTrackAdr.u8MSFFrame,
                                (unsigned int)rInfo.rRelTrackAdr.u8MSFMinute,
                                (unsigned int)rInfo.rRelTrackAdr.u8MSFSecond,
                                (unsigned int)rInfo.rRelTrackAdr.u8MSFFrame,
                                (unsigned int)rInfo.u32StatusPlay);
        }
      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_GETTRACKINFO:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETTRACKINFO [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tU8 u8Track = pu8Par[0];
        tS32 s32Fun, s32Arg, s32Ret;
        OSAL_trTrackInfo rInfo;

        rInfo.u32TrackNumber = u8Track;
        rInfo.rStartAdr.u8MSFMinute = 0;
        rInfo.rStartAdr.u8MSFSecond = 0;
        rInfo.rStartAdr.u8MSFFrame = 0;
        rInfo.rEndAdr.u8MSFMinute = 0;
        rInfo.rEndAdr.u8MSFSecond = 0;
        rInfo.rEndAdr.u8MSFFrame = 0;
        rInfo.u32TrackControl = 0;

        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO;
        s32Arg = (tS32)&rInfo;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
        {
          CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: TrackInfo:"
                                " Track [%02u],"
                                " StartMSF [%02u:%02u:%02u],"
                                " EndMSF [%02u:%02u:%02u],"
                                " Control [0x%08X]",
                                (unsigned int)rInfo.u32TrackNumber,
                                (unsigned int)rInfo.rStartAdr.u8MSFMinute,
                                (unsigned int)rInfo.rStartAdr.u8MSFSecond,
                                (unsigned int)rInfo.rStartAdr.u8MSFFrame,
                                (unsigned int)rInfo.rEndAdr.u8MSFMinute,
                                (unsigned int)rInfo.rEndAdr.u8MSFSecond,
                                (unsigned int)rInfo.rEndAdr.u8MSFFrame,
                                (unsigned int)rInfo.u32TrackControl);
        }
      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_GETALBUMNAME:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETALBUMNAME [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tU8 u8NameArray[OSAL_C_S32_CDAUDIO_MAXNAMESIZE + 1];
        tS32 s32Fun, s32Arg, s32Ret;

        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETALBUMNAME;
        s32Arg = (tS32)u8NameArray;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
        {
          CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: AlbumName: [%s]",
                                (const char *)u8NameArray);
        }

      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_GETTRACKCDINFO:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETTRACKCDINFO [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        OSAL_trTrackCdTextInfo rInfo;
        tS32 s32Fun, s32Arg, s32Ret;
        tU8 u8Track = pu8Par[0];

        rInfo.u32TrackNumber = (tU32)u8Track;
        /*OSAL_C_S32_CDAUDIO_MAXNAMESIZE*/
        rInfo.rPerformer[0] = '\0';
        rInfo.rTrackTitle[0] = '\0';

        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKCDINFO;
        s32Arg = (tS32)&rInfo;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
        {
          CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: TrackCDInfo:"
                                " Track  [%02u],"
                                " Artist  [%s],"
                                " Title   [%s]",
                                (unsigned int)rInfo.u32TrackNumber,
                                (const char*)rInfo.rPerformer,
                                (const char*)rInfo.rTrackTitle);
        }
      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_GETADDITIONALCDINFO:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETADDITIONALCDINFO [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        OSAL_trAdditionalCDInfo rInfo;
        tS32 s32Fun, s32Arg, s32Ret;

        rInfo.u32DataTracks[0] = 0;
        rInfo.u32DataTracks[1] = 0;
        rInfo.u32DataTracks[2] = 0;
        rInfo.u32DataTracks[3] = 0;
        rInfo.u32InfoBits = 0;

        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETADDITIONALCDINFO;
        s32Arg = (tS32)&rInfo;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
        {
          CDAUDIO_PRINTF_FORCED("vPlayInfoNotify: AdditionalCDInfo:"
                                " DataTrackBits"
                                " [0x%08X]"
                                " [0x%08X]"
                                " [0x%08X]"
                                " [0x%08X],"
                                " InfoBits [0x%08X],",
                                (unsigned int)rInfo.u32DataTracks[0],
                                (unsigned int)rInfo.u32DataTracks[1],
                                (unsigned int)rInfo.u32DataTracks[2],
                                (unsigned int)rInfo.u32DataTracks[3],
                                (unsigned int)rInfo.u32InfoBits);
        }
      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_REGPLAYNOTIFY:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL REGPLAYNOTIFY [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        OSAL_trPlayInfoReg rReg;

        rReg.pCallback = vPlayInfoNotify;
        rReg.pvCookie = NULL;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY;
        s32Arg = (tS32)&rReg;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
        (void)rReg;
      }
      break;

    case CDAUDIO_TRACE_CMD_UNREGPLAYNOTIFY:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL UNREGPLAYNOTIFY [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        tS32 s32Fun, s32Arg, s32Ret;
        OSAL_trPlayInfoReg rReg;

        rReg.pCallback = vPlayInfoNotify;
        rReg.pvCookie = NULL;
        s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_UNREGPLAYNOTIFY;
        s32Arg = (tS32)&rReg;
        if(bIsOpen(pShMem))
        {
          s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
        }
        else //if(bIsOpen(pShMem))
        {
          (void)CDAUDIO_u32IOOpen((tS32)0);
          (void)CDAUDIO_u32IOControl((tS32)0, s32Fun, s32Arg);
          (void)CDAUDIO_u32IOClose((tS32)0);
          s32Ret = OSAL_OK;
        }
        (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
        (void)rReg;
      } //if(bIsOpen(pShMem))
      break;

    case CDAUDIO_TRACE_CMD_CDTEXT_DEBUG:
      CDAUDIO_PRINTF_U1("TraceCmd CDTEXT DEBUG [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        CDAUDIO_vCDTEXTDebug(pShMem);
      }
      break;

    case CDAUDIO_TRACE_CMD_CDTEXT_CLEAR:
      CDAUDIO_PRINTF_U1("TraceCmd CDTEXT CLEAR [0x%02X]",
                        (unsigned int)pu8Par[0]);
      {
        CDAUDIO_vCDTEXTClear(pShMem);
      }
      break;

    default:
      CDAUDIO_PRINTF_U1("TraceCmd IOCTRL DEFAULT - "
                        "unknown command 0x%02X",
                        (unsigned int)u8Cmd);
      ;
    } //switch(u8Cmd)
  } //if(OSAL_E_NOERROR == u32Ret)

  CD_vReleaseShMem(pShMem, hShMem, hSem);
  gTraceCmdThreadIsRunning = FALSE; //simple sync
}

/*****************************************************************************
 * FUNCTION:     CDAUDIO_vTraceCmdCallback
 * PARAMETER:    buffer
 * RETURNVALUE:  None
 * DESCRIPTION:  This is a Trace callback handler.

 * HISTORY:
 ******************************************************************************/
tVoid CDAUDIO_vTraceCmdCallback(const tU8* pcu8Buffer)
{
  if(pcu8Buffer != NULL)
  {
    if(!gTraceCmdThreadIsRunning)
    {
      OSAL_trThreadAttribute traceCmdThreadAttribute;
      OSAL_tThreadID traceCmdThreadID; //l i n t  = OSAL_ERROR;
      tU8 u8Len = pcu8Buffer[0];
      gTraceCmdThreadIsRunning = TRUE;

      (void)OSAL_pvMemoryCopy(gu8TraceBuffer, pcu8Buffer, u8Len + 1);

      traceCmdThreadAttribute.szName = "CDAUDIOTRACE";
      traceCmdThreadAttribute.u32Priority = 0;
      traceCmdThreadAttribute.pfEntry = vTraceCmdThread;
      traceCmdThreadAttribute.pvArg = gu8TraceBuffer;
      traceCmdThreadAttribute.s32StackSize = 1024;

      traceCmdThreadID = OSAL_ThreadSpawn(&traceCmdThreadAttribute);
      if(OSAL_ERROR == traceCmdThreadID)
      {
        CDAUDIO_PRINTF_ERRORS("Create TraceThread");
      } //if(OSAL_ERROR == traceCmdThreadID)
    }
    else //if(!gTraceCmdThreadIsRunning)
    {
      CDAUDIO_PRINTF_ERRORS("last Trace thread is running yet");
    } //else //if(!gTraceCmdThreadIsRunning)
  } //if(pcu8Buffer != NULL)
}

#ifdef __cplusplus
}
#endif
/************************************************************************
 |end of file
 |-----------------------------------------------------------------------*/
