/************************************************************************
 | $Revision: 1.0 $
 | $Date: 2012/01/01 $
 |************************************************************************
 | FILE:         cdaudio_cdtext.c
 | PROJECT:      GEN3
 | SW-COMPONENT: OSAL I/O Device
 |------------------------------------------------------------------------
 | DESCRIPTION:
 |  This file contains the /dev/cdaudio device implementation
 |------------------------------------------------------------------------
 | Date      | Modification               | Author
 | 2013      | GEN3 VW MIB                | srt2hi
 |
 |************************************************************************
 |************************************************************************/

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/
/* Interface for variable number of arguments */
// srt2hi migrate #include <extension/dbgspt.h>
/* Basic OSAL includes */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"
#include "cd_main.h"
#include "cd_cache.h"
#include "cdaudio.h"
#include "cd_funcenum.h"
#include "cdaudio_cdtext.h"
#include "cdaudio_trace.h"
#include "cd_scsi_if.h"

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: module-local)
 |-----------------------------------------------------------------------*/
#define CDAUDIO_ATABYTE(pu8,row,col) (pu8\
                                     [(CDAUDIO_CDTEXT_ATA_BUFFER_BYTES_PER_LINE\
                                       * (tU32)(row)) + (tU32)(col)])

/************************************************************************
 |typedefs (scope: module-local)
 |----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable inclusion  (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: module-local)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function prototype (scope: module-local)
 |-----------------------------------------------------------------------*/

  /************************************************************************
 |function implementation (scope: module-local)
 |-----------------------------------------------------------------------*/

/*****************************************************************************
 * FUNCTION:     vTraceHex
 * PARAMETER:    some needed values
 * RETURNVALUE:  result
 * DESCRIPTION:  prints hex dump

 * HISTORY:
 ******************************************************************************/
static tVoid vTraceHex(tDevCDData *pShMem, const tU8 *pu8Buf, tInt iBufLen,
                       tInt iBytesPerLine)
{
  tInt iBpl = iBytesPerLine;
  tInt iL, iC;
  char c;
  char szTmp[32];
  char szTxt[CDAUDIO_CDTEXT_TEXT_BUFFER_LEN];
  if(NULL == pu8Buf)
  {
    return;
  } //if(NULL == pu8Buf)

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  for(iL = 0; iL < iBufLen; iL += iBpl)
  {
    sprintf(szTxt, "%04X:", (unsigned int)(iL));
    for(iC = 0; iC < iBpl; iC++)
    {
      sprintf(szTmp, " %02X", (unsigned int)pu8Buf[iL + iC]);
      strcat(szTxt, szTmp);
    } //for(iC = 0 ; iC < iBpl ; iC++)

    strcat(szTxt, " ");
    for(iC = 0; iC < iBpl; iC++)
    {
      c = (char)pu8Buf[iL + iC];
      if((c < 0x20) || (c >= 0x80))
      {
        c = '.';
      }
      sprintf(szTmp, "%c", (unsigned char)c);

      strcat(szTxt, szTmp);
    } //for(iC = 0 ; iC < iBpl ; iC++)
    CDAUDIO_PRINTF_U3("%s", szTxt);
  } //for(i = 0 ; i < iBufLen ; i += iBpl)
}

/*****************************************************************************
 * FUNCTION:     u32ConvertUglyATA2NiceCDText
 * PARAMETER:    some needed values
 * RETURNVALUE:  Osal error code
 * DESCRIPTION:  makes osal-style cd.text from ugly ATA format

 * HISTORY:
 ******************************************************************************/
static tU32 u32ConvertUglyATA2NiceCDText(tDevCDData *pShMem,
                                         CDAUDIO_sCDText_type *prCDTxt,
                                         tU32 u32ATACDTextLen)
{
  tU32 u32Ret = OSAL_E_NOERROR;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CDAUDIO_TRACE_ENTER_U4(CDID_u32ConvertUglyATA2NiceCDText, prCDTxt,
                         u32ATACDTextLen, 0, 0);

  if(NULL != prCDTxt)
  {
    tU32 u32ATARowCur;
    tU32 u32ATARowMax;
    tU8 *pu8RawATA;
    tU8 au8Txt[CDAUDIO_CDTEXT_TEXT_BUFFER_LEN] =
    { 0};
    tU8 u8TxtIndex = 0;
    tU8 u8C;
    tU8 u8PrevC = 0;

    pu8RawATA = &prCDTxt->au8ATARawCDTextBuffer[4];

    //clear old content
    memset(prCDTxt->saTrackText, 0, sizeof(CDAUDIO_sTrackCDText_type));

    u32ATARowMax = (u32ATACDTextLen / CDAUDIO_CDTEXT_ATA_BUFFER_BYTES_PER_LINE);

    u32ATARowCur = 0;
    while(u32ATARowCur < u32ATARowMax)
    {
      tInt iLineIndex;
      //work on current line
      for(iLineIndex = 0; iLineIndex < CDAUDIO_CDTEXT_ATA_BUFFER_TEXT_PER_LINE;
         ++iLineIndex)
      {
        u8C = CDAUDIO_ATABYTE(pu8RawATA, u32ATARowCur, 4 + iLineIndex);
        if(u8C == 0x00)
        { //end of text, next track started
          // eat contingous zeros
          if(u8PrevC != 0x00)
          { //copy text and increment track
            tU8 u8Track = CDAUDIO_ATABYTE(pu8RawATA, u32ATARowCur, 1);
            tU8 u8ATAId = CDAUDIO_ATABYTE(pu8RawATA, u32ATARowCur, 0);
            tU8 u8Len = CD_MIN(u8TxtIndex, (CDAUDIO_CDTEXT_TEXT_BUFFER_LEN-1));
            u8TxtIndex = 0;  //reset index of temporary text buffer
            switch(u8ATAId)
            {
            case enCDAUDIO_CDTEXT_TYPE_TITLE:
              memcpy(prCDTxt->saTrackText[u8Track].szTitle, au8Txt, u8Len);
              prCDTxt->saTrackText[u8Track].szTitle[u8Len] = 0x00;
              prCDTxt->saTrackText[u8Track].iTitleLen = u8Len;
              break;
            case enCDAUDIO_CDTEXT_TYPE_ARTIST:
              memcpy(prCDTxt->saTrackText[u8Track].szArtist, au8Txt, u8Len);
              prCDTxt->saTrackText[u8Track].szArtist[u8Len] = 0x00;
              prCDTxt->saTrackText[u8Track].iArtistLen = u8Len;
              break;
            default:
              ;
            } //switch(u8ATAId)
          } //if(u8PrevC != 0x00)
        }
        else  //if(u8C == 0x00)
        {
          //cumulate text in temporary buffer
          au8Txt[u8TxtIndex++] = u8C;
        } //else  //if(u8C == 0x00)
        u8PrevC = u8C;
      } //for(iLineIndex ...

      u32ATARowCur++;
    } //while(!bFound && (iATARow <= u32ATARowMax))
  }
  else //if(NULL != prCDTxt)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL != prCDTxt)

  CDAUDIO_TRACE_LEAVE_U4(CDID_u32ConvertUglyATA2NiceCDText, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CDAUDIO_vCDTEXTDebug
 * PARAMETER:    void
 * RETURNVALUE:  void
 * DESCRIPTION:  printf CD-Text
 ******************************************************************************/
tVoid CDAUDIO_vCDTEXTDebug(tDevCDData *pShMem)
{
  CD_CHECK_SHARED_MEM_VOID(pShMem);

  if(enCDAUDIO_CDTEXT_VALID == pShMem->rCDText.enValid)
  {
    tU8 u8T;
    tInt iALen, iTLen;
    tU32 u32DataLen;
    const char *pcszA, *pcszT;

    u32DataLen = CD_8TO16(pShMem->rCDText.au8ATARawCDTextBuffer[0],
                          pShMem->rCDText.au8ATARawCDTextBuffer[1]);
    vTraceHex(pShMem, pShMem->rCDText.au8ATARawCDTextBuffer, 4, 4);
    if(u32DataLen > 0)
    {
      vTraceHex(pShMem, &pShMem->rCDText.au8ATARawCDTextBuffer[4],
                (tInt)(u32DataLen - 4), 18);
    }

    for(u8T = 0; u8T < CD_TRACK_ARRAY_SIZE; u8T++)
    {
      iALen = pShMem->rCDText.saTrackText[u8T].iArtistLen;
      iTLen = pShMem->rCDText.saTrackText[u8T].iTitleLen;
      pcszA = pShMem->rCDText.saTrackText[u8T].szArtist;
      pcszT = pShMem->rCDText.saTrackText[u8T].szTitle;
      CDAUDIO_PRINTF_FORCED("Track%02u Title [%3d] [%s]", (unsigned int)u8T,
                            iTLen, pcszT);
      CDAUDIO_PRINTF_FORCED("        Artist [%3d] [%s]", iALen, pcszA);
    } //for(u8T = 0; u8T < CD_TRACK_ARRAY_SIZE; u8T++)
  }
  else //if(enCDAUDIO_CDTEXT_VALID == pShMem->rCDText.enValid)
  {
    CDAUDIO_PRINTF_FORCED("NO CD-TEXT available");
  } //else //if(enCDAUDIO_CDTEXT_VALID == pShMem->rCDText.enValid)
}

/*****************************************************************************
 * FUNCTION:     CDAUDIO_u32GetText
 * PARAMETER:    IN:drive index
 *               OUT: min track
 *               OUT: max track
 * RETURNVALUE:  OSAL Error
 * DESCRIPTION:  get track info from TOC
 ******************************************************************************/
tU32 CDAUDIO_u32GetText(tDevCDData *pShMem, OSAL_tSemHandle hSem, tU8 u8Track,
                        tU8 *pu8Title, tU8 *pu8Artist, tU32 u32TextBufferSize)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  tU32 u32AtaCDTextSize;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CDAUDIO_TRACE_ENTER_U4(CDID_CDAUDIO_u32GetText, 0, pu8Title, pu8Artist, 0);

  /*check if cd-text is valid*/
  if(enCDAUDIO_CDTEXT_INVALID == pShMem->rCDText.enValid)
  { //try to read cd-text from drive
    u32AtaCDTextSize = 0;
    u32Ret = CD_SCSI_IF_u32ReadCDText(pShMem,
                                      pShMem->rCDText.au8ATARawCDTextBuffer,
                                      CDAUDIO_ATA_CDTEXT_RAW_BUFFER_LEN,
                                      &u32AtaCDTextSize);

    if((u32AtaCDTextSize >= (2 + CDAUDIO_CDTEXT_ATA_BUFFER_BYTES_PER_LINE))
       && (u32Ret == OSAL_E_NOERROR))
    { //Get CD-text from uglyATA-buffer
      CDAUDIO_PRINTF_U3("Raw-CDText-Size %u bytes",
                        (unsigned int)u32AtaCDTextSize);

      u32Ret = u32ConvertUglyATA2NiceCDText(pShMem, &pShMem->rCDText,
                                            u32AtaCDTextSize);
      if(OSAL_E_NOERROR == u32Ret)
      {
        pShMem->rCDText.enValid = enCDAUDIO_CDTEXT_VALID;
      } //if(OSAL_E_NOERROR == u32Ret)
    } //if(u32Ret == OSAL_E_NOERROR)
  } //if(enCDAUDIO_CDTEXT_INVALID == pShMem->rCDText.enValid)

  if(enCDAUDIO_CDTEXT_VALID == pShMem->rCDText.enValid)
  {
    tU32 u32Len;
    tU8 u8MinTrack = 0;
    tU8 u8MaxTrack = 0;
    (void)CD_u32CacheGetCDInfo(pShMem, hSem, &u8MinTrack, &u8MaxTrack, NULL);
    if((u8Track == 0) || ((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack)))
    {
      if(NULL != pu8Title)
      {
        u32Len = CD_MIN(u32TextBufferSize,
                        (tU32)pShMem->rCDText.saTrackText[u8Track].iTitleLen);
        memset(pu8Title, 0, u32TextBufferSize);
        memcpy(pu8Title, pShMem->rCDText.saTrackText[u8Track].szTitle, u32Len);
      } //if(NULL != pu8Title)

      if(NULL != pu8Artist)
      {
        u32Len = CD_MIN(u32TextBufferSize,
                        (tU32)pShMem->rCDText.saTrackText[u8Track].iArtistLen);
        memset(pu8Artist, 0, u32TextBufferSize);
        memcpy(pu8Artist, pShMem->rCDText.saTrackText[u8Track].szArtist,
               u32Len);
      } //if(NULL != pu8Artist)
    }
    else //if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
    {
      u32Ret = OSAL_E_INVALIDVALUE;
    } //else //if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
  }
  else //if(enCDAUDIO_CDTEXT_VALID == pShMem->rCDText.enValid)
  {
    u32Ret = OSAL_E_DOESNOTEXIST;
  } //else //if(enCDAUDIO_CDTEXT_VALID == pShMem->rCDText.enValid)

  CDAUDIO_TRACE_LEAVE_U4(CDID_CDAUDIO_u32GetText, u32Ret, 0, 0, 0, 0);

  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CDAUDIO_vCDTEXTClear
 * PARAMETER:    drive index
 * RETURNVALUE:  void
 * DESCRIPTION:  deletes all cached information about a cd
 ******************************************************************************/
tVoid CDAUDIO_vCDTEXTClear(tDevCDData *pShMem)
{

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  CDAUDIO_TRACE_ENTER_U4(CDID_CDAUDIO_vCDTEXTClear, 0, 0, 0, 0);

  pShMem->rCDText.enValid = enCDAUDIO_CDTEXT_INVALID;

  //clear entire CD-Text (RAW+converted)
  memset(&pShMem->rCDText, 0, sizeof(CDAUDIO_sCDText_type));

  CDAUDIO_TRACE_LEAVE_U4(CDID_CDAUDIO_vCDTEXTClear, 0, 0, 0, 0, 0);
}

/*****************************************************************************
 * FUNCTION:     CDAUDIO_u32CDTextInit
 * PARAMETER:    void
 * RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  initialises CDTEXT
 ******************************************************************************/
tU32 CDAUDIO_u32CDTextInit(tDevCDData *pShMem)
{
  tU32 u32Result = OSAL_E_NOERROR;
  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CDAUDIO_TRACE_ENTER_U4(CDID_CDAUDIO_u32CDTextInit, 0, 0, 0, 0);
  // set startup values
  CDAUDIO_vCDTEXTClear(pShMem);
  CDAUDIO_TRACE_LEAVE_U4(CDID_CDAUDIO_u32CDTextInit, u32Result, 0, 0, 0, 0);

  return u32Result;
}

/*****************************************************************************
 * FUNCTION:     CDAUDIO_vCDTextDestroy
 * PARAMETER:    void
 * RETURNVALUE:  void
 * DESCRIPTION:  initialises CDTEXT
 ******************************************************************************/
tVoid CDAUDIO_vCDTextDestroy(tDevCDData *pShMem)
{
  CD_CHECK_SHARED_MEM_VOID(pShMem);
}

#ifdef __cplusplus
}
#endif
/************************************************************************
 |end of file
 |-----------------------------------------------------------------------*/
