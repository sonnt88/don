/************************************************************************
 | $Revision: 1.0 $
 | $Date: 2006/07/13 $
 |************************************************************************
 | FILE:         cdctrl.c
 | PROJECT:      Gen3
 | SW-COMPONENT: OSAL I/O Device
 |------------------------------------------------------------------------
 | DESCRIPTION:
 |  This file contains the /dev/cdctrl device implementation
 |------------------------------------------------------------------------
 | HISTORY:
 | 16 Mar,2016 | Bug Fix NCG3D-11114 : 
 |               Issue: CD Driver version changes for every CD operation which should be constant
 |               Reason: For HW and SW version valid bytes (33,34 and 35,36) should be considered. 
 |                       But here 57 and 58 bytes are considered which are not valid.
 |               Fix:    33,34 and 35,36 bytes are collected for SW and HW version instead of 57 and 58.
 |               Author: Kranthi Kiran (RBEI/ECF5)
 |**********************************************************************************************************/

/********************************************c****************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/
/* General headers */
#include <time.h>
#include <stdlib.h>

/* Basic OSAL includes */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"
#include "cd_funcenum.h"
#include "cd_main.h"
#include "cdctrl.h"
#include "cdctrl_trace.h"
#include "cd_cache.h"
#include "cd_scsi_if.h"
#include "cdaudio_cdtext.h"

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: module-local)
 |-----------------------------------------------------------------------*/
//#define CDCTRL_MAIN_SEMAPHORE_TIMEOUT_MS 15000

#define CDCTRL_FP_LEN 4  /* number of 32bit FingerPrints*/

/************************************************************************
 |typedefs (scope: module-local)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable inclusion  (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: module-local)
 |-----------------------------------------------------------------------*/
static OSAL_tSemHandle g_hSem = OSAL_C_INVALID_HANDLE;
static OSAL_tShMemHandle g_hShMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
static tDevCDData *g_pSharedMem = NULL; /*OSAL shared memory*/

/************************************************************************
 |function prototype (scope: module-local)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function prototype (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function implementation (scope: module-local)
 |-----------------------------------------------------------------------*/


/************************************************************************
 *
 * FUNCTIONS
 *      iGetFingerPrint
 *
 * DESCRIPTION
 *      Calculates a simple (xored) 32-characters-fingerprint of a given buffer
 *      If no source buffer is given, the fingerprint is a random number.
 *
 * Parameters
 *      IN  pShMem: Pointer to shared memory of dev_cdctrl
 *      OUT pcFingerPrint: output buffer for fingerprint
 *      IN  nFingerPrintSize: number of characters in *pcFingerPrint-buffer
 *      IN  pcucBuffer: buffer to be calculated; if NULL, a random fingerprint
 *                      is generated
 *      IN  nBufferLen: size of *pcucBuffer; if 0, a random fingerprint is
 *                      generated
 *
 * OUTPUTS
 *      pcFingerPrint: a fingerprint of max 32 characters, for example:
 *                     "5AD0DDBD94949EA56A15AF8CB5358FB5"
 *                     This string is NOT zero terminated!
 *
 * Return
 *      Number of characters written to pcFingerPrint 
 *
 *HISTORY: 25.05.2015, initial version taken from ADIT automounter, srt2hi
 *
 ************************************************************************/
static int iGetFingerPrint(const tDevCDData *pShMem, char *pcFingerPrint,
                           size_t nFingerPrintSize,
                           const unsigned char *pcucBuffer,
                           size_t nBufferLen)
{
  unsigned int uiFPBin[CDCTRL_FP_LEN];
  unsigned int *puiBuffer;
  size_t nuiBufferSize = nBufferLen / sizeof(unsigned int);
  unsigned int uiBuf, uiFP;
  size_t nTotalLen;

  CDCTRL_TRACE_ENTER_U2(CDID_CDCTRL_iGetFingerPrint,
                        pcFingerPrint, nFingerPrintSize,
                        pcucBuffer, nBufferLen);

  memset(uiFPBin, 0, sizeof(uiFPBin));
  puiBuffer = (unsigned int *)(void*)pcucBuffer;

  // xor buffer
  if(puiBuffer && (nBufferLen > 0))
  {
    for(uiBuf = 0 ; uiBuf < (nuiBufferSize - CDCTRL_FP_LEN) ; uiBuf++)
    {
      for (uiFP = 0; uiFP < CDCTRL_FP_LEN; uiFP++)
      {
        uiFPBin[uiFP] ^= puiBuffer[uiBuf + uiFP];
        //uiFPBin[uiFP] += puiBuffer[uiBuf + uiFP];
      }
    }
  }
  else //if(puiBuffer && (nBufferLen > 0))
  { // given buffer pointer is NULL or date size == 0,
    // simulate a changed fingerprint
    srand((unsigned int)time(NULL));
    for (uiFP = 0; uiFP < CDCTRL_FP_LEN; uiFP++)
    {
      uiFPBin[uiFP] = (unsigned int)rand();
    }
  } //else //if(puiBuffer && (nBufferLen > 0))

  // make text-hash
  nTotalLen=0;
  if(pcFingerPrint)
  {    
    for(uiFP = 0; uiFP < CDCTRL_FP_LEN; uiFP++)
    {
      char szTmp[32];
      size_t nLen;
      sprintf(szTmp, "%08X", uiFPBin[uiFP]);
      nLen = strlen(szTmp);
      if((nTotalLen+nLen) <= nFingerPrintSize)
      {
        memcpy(&pcFingerPrint[nTotalLen], szTmp, nLen);
        nTotalLen += nLen;
      }
    }
  } //if(pcFingerPrint)

  CDCTRL_TRACE_LEAVE_U2(CDID_CDCTRL_iGetFingerPrint,
                        OSAL_E_NOERROR, nTotalLen, 0, 0, 0);
  return (int)nTotalLen;
}


/************************************************************************
 |function implementation (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 * FUNCTIONS                                                             *
 *      CDCTRL_u32IOOpen                                                 *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Open cdctrl required resources                                   *
 *                                                                       *
 * CALLS                                                                 *
 *                                                                       *
 * INPUTS                                                                *
 *                                                                       *
 *      tU32 u32CD: Drive identificator                                  *
 *                                                                       *
 * OUTPUTS                                                               *
 *                                                                       *
 *      tS32: Error Code                                                 *
 *HISTORY      :                                                         *
 ************************************************************************/
tU32 CDCTRL_u32IOOpen(tS32 s32Dummy)
{
  tU32 u32Result;
  (void)s32Dummy;

  CDCTRL_TRACE_ENTER_U1_FORCED(CDID_CDCTRL_u32IOOpen, 0, 0, 0, 0);

  u32Result = CD_u32GetShMem(&g_pSharedMem, &g_hShMem, &g_hSem);

  //clean up if failed
  if(OSAL_E_NOERROR != u32Result)
  {
    (void)CDCTRL_u32IOClose(0);
  } //if(u32Result == OSAL_ERROR)

  CDCTRL_TRACE_LEAVE_U1_FORCED(CDID_CDCTRL_u32IOOpen, u32Result, 0, 0, 0, 0);
  return u32Result;
}

/************************************************************************
 *                                                                       *
 * FUNCTIONS                                                             *
 *                                                                       *
 *      CDCTRL_u32IOClose                                                *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Close cdctrl required device                                     *
 *                                                                       *
 *                                                                       *
 * INPUTS                                                                *
 *                                                                       *
 *      tU32 u32CD: Drive identificator                          *
 *                                                                       *
 * OUTPUTS                                                               *
 *                                                                       *
 *      tS32: Error code                                                 *
 *                                                                       *
 *HISTORY      :                                                         *
 *                                                                       *
 ************************************************************************/
tU32 CDCTRL_u32IOClose(tS32 s32Dummy)
{
  tU32 u32Result = OSAL_E_NOERROR;
  (void)s32Dummy;
  CDCTRL_TRACE_ENTER_U1_FORCED(CDID_CDCTRL_u32IOClose, 0, 0, 0, 0);

  CD_vReleaseShMem(g_pSharedMem, g_hShMem, g_hSem);

  g_pSharedMem = NULL;
  g_hShMem = OSAL_ERROR;
  g_hSem = OSAL_C_INVALID_HANDLE;

  CDCTRL_TRACE_LEAVE_U1_FORCED(CDID_CDCTRL_u32IOClose, u32Result, 0, 0, 0, 0);
  return u32Result;
}

/************************************************************************
 *                                                                       *
 * FUNCTIONS                                                             *
 *                                                                       *
 *      CDCTRL_u32IOControl                                              *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Call a cdctrl control function                                   *
 *                                                                       *
 *
 * INPUTS                                                                *
 *                                                                       *
 *       tU32 u32CD: Drive identificator                         *
 *       tS32 s32fun:               Function identificator               *
 *       tS32 s32Arg:               Argument to be passed to function    *
 *                                                                       *
 * OUTPUTS                                                               *
 *                                                                       *
 *      tS32: Error code                                                 *
 * HISTORY
 *
 * 16 Mar,2016 - Bug Fix NCG3D-11114
               - u8HW and u8SW are made to 3 byte arrays | Kranthi Kiran(RBEI/ECF5)
 **********************************************************************************/
tU32 CDCTRL_u32IOControl(tS32 s32Dummy, tS32 s32Fun, tS32 s32Arg)
{
  tU32 u32RetVal = OSAL_E_NOERROR;
  tDevCDData *pShMem = g_pSharedMem;
  OSAL_tSemHandle hSem = g_hSem;

  (void)s32Dummy;

  CDCTRL_TRACE_ENTER_U4(CDID_CDCTRL_u32IOControl, 0, s32Fun, s32Arg, 0);

  CDCTRL_TRACE_IOCTRL(CDCTRL_EN_IOCONTROL, 0, s32Fun, s32Arg, 0);

  switch(s32Fun)
  {
  case OSAL_C_S32_IOCTRL_VERSION:
    if((void*)s32Arg != NULL)
    {
      *(tPS32)s32Arg = CDCTRL_C_S32_IO_VERSION;
      CDCTRL_TRACE_IOCTRL(CDCTRL_EN_IOCTRL_VERSION, 0,
                          (tU32)(*(tPS32)s32Arg), 0, 0);
    }
    else
    {
      u32RetVal = OSAL_E_INVALIDVALUE;
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO:

    {
      OSAL_trLoaderInfo* prInfo = (OSAL_trLoaderInfo*)s32Arg;

      if(prInfo != NULL)
      {
        prInfo->u8LoaderInfoByte = CD_u8GetLoaderInfo(pShMem, NULL);
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_LOADER_INFO, 0,
                            (tU32)prInfo->u8LoaderInfoByte, 0, 0);

      }
      else
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_GET_DRIVE_VERSION:
    {
      OSAL_trCDDriveVersion* prVersion = (OSAL_trCDDriveVersion*)s32Arg;

      if(prVersion != NULL)
      {
        tU8 u8SW = 0x00;
        tU8 u8HW = 0x00;
        tU8 au8Vendor8[8 + 1] =
        { 0};

        u32RetVal = CD_u32CacheGetVersion(pShMem, &u8SW, &u8HW, au8Vendor8);

        prVersion->u32SWVersion = u8SW;
        prVersion->u32HWVersion = u8HW;
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_DRIVE_VERSION, 0, prVersion->u32SWVersion,
                            prVersion->u32HWVersion, 0);
      }
      else
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_GETDRIVEVERSION:
    {
      OSAL_trDriveVersion *prDriveVersion = (OSAL_trDriveVersion*)s32Arg;
      
      if(prDriveVersion != NULL)
      {
        tU8 u8SW[3] = "";
        tU8 u8HW[3] = "";
        tU8 au8Vendor8[8 + 1] =
        { 0};
        const char *pcszVendor = (char*)au8Vendor8;

        u32RetVal = CD_u32CacheGetVersion(pShMem, u8SW, u8HW, au8Vendor8);

        //fill whole array with space
        memset(prDriveVersion->au8FirmwareRevision, ' ',
               sizeof(prDriveVersion->au8FirmwareRevision));

        (void)snprintf((tChar *)prDriveVersion->au8FirmwareRevision,
                       sizeof(prDriveVersion->au8FirmwareRevision) - 1, "%s",u8SW);


        // prDriveVersion->au8ModelNumber  // 40
         memset(prDriveVersion->au8ModelNumber,
               ' ', //fill whole array with space
               sizeof(prDriveVersion->au8ModelNumber));

        (void)snprintf((tChar *)prDriveVersion->au8ModelNumber,
                         sizeof(prDriveVersion->au8ModelNumber) - 1, "%s HW%s",
                         pcszVendor, u8HW);

        // prDriveVersion->au8SerialNumber  // 20
        memset(prDriveVersion->au8SerialNumber, '0',
               sizeof(prDriveVersion->au8SerialNumber));

        prDriveVersion->u16MajorVersionNumber = 0xF000;
        prDriveVersion->u16MinorVersionNumber = 0x0001;

        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_DRIVE_VERSION_FULL, 0,
                            prDriveVersion->u16MajorVersionNumber,
                            prDriveVersion->u16MinorVersionNumber, 0);

      }
      else
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA:
    {
      tU8 u8InternalLI;
      (void)CD_u8GetLoaderInfo(pShMem, &u8InternalLI);
      u32RetVal = CD_SCSI_IF_u32Eject(pShMem);
      if(OSAL_E_NOERROR == u32RetVal)
      { //wait until  !EJECTINPROGRESS
        tBool bTimeOut;
        tBool bEndLoop;
        OSAL_tMSecond startMS;

        //if CD was in slot before, simply wait some time
        // until insert/eject loop ends (there is no status change during this)
        if(u8InternalLI == CD_INTERNAL_LOADERSTATE_IN_SLOT)
          (void)OSAL_s32ThreadWait(3500);

        startMS = OSAL_ClockGetElapsedTime();
        do
        {
          (void)OSAL_s32ThreadWait(500);
          bTimeOut = (OSAL_ClockGetElapsedTime() - startMS) > 20000;
          (void)CD_u8GetLoaderInfo(pShMem, &u8InternalLI);
          switch(u8InternalLI)
          {
          case CD_INTERNAL_LOADERSTATE_EJECTED:
          case CD_INTERNAL_LOADERSTATE_IN_SLOT:
          case CD_INTERNAL_LOADERSTATE_EJECT_ERROR:
          case CD_INTERNAL_LOADERSTATE_NO_CD:
            bEndLoop = TRUE;
            break;
          case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
          case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
          case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
          case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
          case CD_INTERNAL_LOADERSTATE_UNKNOWN:
          case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
          case CD_INTERNAL_LOADERSTATE_LOAD_ERROR:
          case CD_INTERNAL_LOADERSTATE_INITIALIZED:
          default:
            bEndLoop = FALSE;
          } //switch(u8InternalLI)
          bEndLoop = bEndLoop || bTimeOut;
        }
        while(!bEndLoop);

        if(bTimeOut)
        {
          u32RetVal = OSAL_E_TIMEOUT;
        }
      }
      else //if(OSAL_E_NOERROR == u32RetVal)
      {
        // in case of error, use media state as return value
        tU8 u8Loader = CD_u8GetLoaderInfo(pShMem, NULL);
        if((OSAL_C_U8_NO_MEDIA == u8Loader)
           || (OSAL_C_U8_MEDIA_IN_SLOT == u8Loader)
           || (OSAL_C_U8_EJECT_IN_PROGRESS == u8Loader))
        {
          u32RetVal = OSAL_E_NOERROR;
        }
      } //else //if(OSAL_E_NOERROR == u32RetVal)
      CD_vCacheClear(pShMem, hSem, TRUE);
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR:
    {
      tBool bTimeOut;
      tBool bEndLoop;
      OSAL_tMSecond startMS;

      CD_vCacheClear(pShMem, hSem, TRUE);
      startMS = OSAL_ClockGetElapsedTime();
      do
      {
        tU8 u8InternalLI;
        bTimeOut = (OSAL_ClockGetElapsedTime() - startMS) > 20000;
        (void)CD_u8GetLoaderInfo(pShMem, &u8InternalLI);
        switch(u8InternalLI)
        {
        case CD_INTERNAL_LOADERSTATE_LOAD_ERROR:
          (void)CD_SCSI_IF_u32Load(pShMem);
          bEndLoop = TRUE;
          break;
        case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
        case CD_INTERNAL_LOADERSTATE_NO_CD:
        case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
        case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
        case CD_INTERNAL_LOADERSTATE_EJECTED:
          bEndLoop = TRUE;
          break;
        case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
        case CD_INTERNAL_LOADERSTATE_EJECT_ERROR:
        case CD_INTERNAL_LOADERSTATE_IN_SLOT:
        case CD_INTERNAL_LOADERSTATE_UNKNOWN:
        case CD_INTERNAL_LOADERSTATE_INITIALIZED:
          (void)CD_SCSI_IF_u32Load(pShMem);
          (void)OSAL_s32ThreadWait(500);
          bEndLoop = FALSE;
          break;
        case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
        default:
          (void)OSAL_s32ThreadWait(500);
          bEndLoop = FALSE;
        } //switch(u8InternalLI)
        bEndLoop = bEndLoop || bTimeOut;
      }
      while(!bEndLoop);

      if(bTimeOut)
      {
        u32RetVal = OSAL_E_TIMEOUT;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP:
    {
      OSAL_trDriveTemperature* prTemp = (OSAL_trDriveTemperature*)s32Arg;

      if(prTemp != NULL)
      {
        tS32 s32Temperature;
        u32RetVal = CD_SCSI_IF_u32GetTemperature(pShMem, &s32Temperature);
        if(OSAL_E_NOERROR != u32RetVal)
        {
          s32Temperature = 155;
        } //if(OSAL_E_NOERROR == u32RetVal)

        prTemp->u8Temperature = (tU8)(s32Temperature + 100);

        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_TEMPERATURE, 0, (tU32)s32Temperature,
                            prTemp->u8Temperature, 0);
      }
      else
      { // prTemp is NULL!
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    } //case OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP:
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO:
    {
      OSAL_trCDROMTrackInfo* prInfo = (OSAL_trCDROMTrackInfo*)s32Arg;

      if(prInfo != 0)
      {
        u32RetVal = CD_u32CacheGetTrackInfo(pShMem, hSem,
                                            (tU8)prInfo->u32TrackNumber,
                                            &prInfo->u32LBAAddress,
                                            &prInfo->u32TrackControl);
        //mixed mode switch
        if((CD_TRACKINFO_MAX_LBA_TRACK != (tU8)prInfo->u32TrackNumber)
           && (CD_u8GetInternalMediaType(pShMem) ==
               CD_INTERNAL_MEDIA_TYPE_MIXED_MODE_MEDIA))
        {
          OSAL_tEventHandle hMainEvent = OSAL_C_INVALID_HANDLE;
          tU8 u8MediaType = CD_u8GetMediaType(pShMem);
          tBool bIsData;

          bIsData =
          (prInfo->u32TrackControl & CD_TYPE_MASK) == CD_TYPE_DATA ? TRUE :
          FALSE;

          if(OSAL_OK != OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent))
          {
            CDCTRL_PRINTF_ERRORS("GETTRACKINFO  ERROR [%s], "
                                 "OSAL_s32EventOpen(%s)",
                                 (const char*)
                                 OSAL_coszErrorText(OSAL_u32ErrorCode()),
                                 CD_MAIN_EVENT_NAME);
          } //if(OSAL_OK != OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent))

          if(bIsData)
          { // turn on DATA mode if recently in AUDIO mode
            if(u8MediaType == OSAL_C_U8_AUDIO_MEDIA)
            { // switch to DATA via reading a sector from CD

              (void)OSAL_s32EventPost(hMainEvent,
                                      CD_MAIN_EVENT_MASK_MIXED_NOT_READY,
                                      OSAL_EN_EVENTMASK_OR);

              pShMem->rCD.u8ForcedMixedModeMediaType =
              CD_INTERNAL_MEDIA_TYPE_DATA_MEDIA;
              CDCTRL_PRINTF_U1("Switch Mixed Mode to DATA");
            } //if(u8MediaType == OSAL_C_U8_AUDIO_MEDIA)
          }
          else //if(bIsData)
          {
            // // turn on AUDIO mode if recently in DATA mode
            if(u8MediaType == OSAL_C_U8_DATA_MEDIA)
            {
              (void)OSAL_s32EventPost(hMainEvent,
                                      CD_MAIN_EVENT_MASK_MIXED_NOT_READY,
                                      OSAL_EN_EVENTMASK_OR);
              pShMem->rCD.u8ForcedMixedModeMediaType =
              CD_INTERNAL_MEDIA_TYPE_AUDIO_MEDIA;
              (void)OSAL_s32EventPost(hMainEvent,
                                      CD_MAIN_EVENT_MASK_MIXED_READY_AUDIO,
                                      OSAL_EN_EVENTMASK_OR);
              CDCTRL_PRINTF_U1("Switch Mixed Mode to AUDIO");
            } //if(u8MediaType == OSAL_C_U8_DATA_MEDIA)
          } //else if(bIsData)

          //In Data-Mode: read a sector until succes or timeout
          u8MediaType = CD_u8GetMediaType(pShMem);
          if(u8MediaType == OSAL_C_U8_DATA_MEDIA)
          { // switch to DATA via reading a sector from CD
            tBool bTimeOut;
            tBool bEndLoop;
            OSAL_tMSecond startMS;
            startMS = OSAL_ClockGetElapsedTime();
            do
            {
              OSAL_tMSecond diffMS;
              static tU8 u8Buffer[CD_USER_BYTES_PER_SECTOR];

              pShMem->rCD.u8ForcedMixedModeMediaType =
              CD_INTERNAL_MEDIA_TYPE_DATA_MEDIA;
              u32RetVal = CD_SCSI_IF_u32Read(pShMem, prInfo->u32LBAAddress, 1,
                                             u8Buffer);

              diffMS = OSAL_ClockGetElapsedTime() - startMS;
              bTimeOut = diffMS > 40000;  //40 seconds
              CDCTRL_PRINTF_U1("Mixed Mode DATA - Read Sector 0x%08X, "
                               "TimeDiff %u ms, "
                               "TimeOut %d",
                               (unsigned int)u32RetVal, (unsigned int)diffMS,
                               (int)bTimeOut);

              bEndLoop = (u32RetVal == OSAL_E_NOERROR) || bTimeOut;
              if(!bEndLoop)
              {
                (void)OSAL_s32ThreadWait(500);
              } //if(!bEndLoop)
            }
            while(!bEndLoop);

            if(bTimeOut)
            {
              u32RetVal = OSAL_E_TIMEOUT;
            }
            else
            {
              (void)OSAL_s32EventPost(hMainEvent,
                                      CD_MAIN_EVENT_MASK_MIXED_READY_DATA,
                                      OSAL_EN_EVENTMASK_OR);
            }
          } //if(u8MediaType == OSAL_C_U8_AUDIO_MEDIA)

          if(OSAL_C_INVALID_HANDLE != hMainEvent)
          {
            (void)OSAL_s32EventClose(hMainEvent);
          } //if(OSAL_C_INVALID_HANDLE != hMainEvent)
        } //if MIXED MODE CD

        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_GETTRACKINFO, 0, prInfo->u32TrackNumber,
                            prInfo->u32LBAAddress, prInfo->u32TrackControl);
      }
      else
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATAUNCACHED:
    {
      OSAL_trReadRawInfo *prRRInfo = (OSAL_trReadRawInfo*)s32Arg;
      if(prRRInfo != NULL)
      {

        tU32 u32LBA = prRRInfo->u32LBAAddress;
        tU16 u16Blocks = (tU16)prRRInfo->u32NumBlocks;
        tU8 *pu8Buffer = (tU8*)prRRInfo->ps8Buffer;

        u32RetVal = CD_SCSI_IF_u32Read(pShMem, u32LBA, u16Blocks, pu8Buffer);
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_READ_RAW_LBA_UNCACHED, 0,
                            prRRInfo->u32NumBlocks, prRRInfo->u32LBAAddress,
                            (tU32)prRRInfo->ps8Buffer);

      }
      else
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA:
    {
      OSAL_trReadRawInfo *prRRInfo = (OSAL_trReadRawInfo*)s32Arg;
      if(prRRInfo != NULL)
      {

        tU32 u32LBA = prRRInfo->u32LBAAddress;
        tU16 u16Blocks = (tU16)prRRInfo->u32NumBlocks;
        tU8 *pu8Buffer = (tU8*)prRRInfo->ps8Buffer;

        u32RetVal = CD_SCSI_IF_u32Read(pShMem, u32LBA, u16Blocks, pu8Buffer);
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_READ_RAW_LBA, 0, prRRInfo->u32NumBlocks,
                            prRRInfo->u32LBAAddress,
                            (tU32)prRRInfo->ps8Buffer);

      }
      else
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF:
    {
      OSAL_trReadRawMSFInfo *pMSF = (OSAL_trReadRawMSFInfo *)s32Arg;
      if(pMSF != NULL)
      {
        tU32 u32LBA = CD_MSF2ZLBA(pMSF->u8Minute,
                                  pMSF->u8Second,
                                  pMSF->u8Frame)
                      - pShMem->rCD.rTOC.arTrack[1].u32StartZLBA;

        tU16 u16Blocks = (tU16)pMSF->u32NumBlocks;
        tU8 *pu8Buffer = (tU8*)pMSF->ps8Buffer;

        u32RetVal = CD_SCSI_IF_u32Read(pShMem, u32LBA, u16Blocks, pu8Buffer);
        CDCTRL_TRACE_IOCTRL(
                           CDCTRL_EN_READ_RAW_MSF,
                           0,
                           pMSF->u32NumBlocks,
                           ((tU32)pMSF->u8Minute) << 24
                           |
                           ((tU32)pMSF->u8Second) << 16
                           | ((tU32)pMSF->u8Frame),
                           (tU32)pu8Buffer);

      }
      else
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO:
    {
      OSAL_trCDInfo* prCDInfo = (OSAL_trCDInfo*)s32Arg;
      if(prCDInfo != NULL)
      {
        tU8 u8MinTrack = 0;
        tU8 u8MaxTrack = 0;

        u32RetVal = CD_u32CacheGetCDInfo(pShMem, hSem, &u8MinTrack, &u8MaxTrack,
                                         NULL);

        prCDInfo->u32MinTrack = (tU32)u8MinTrack;
        prCDInfo->u32MaxTrack = (tU32)u8MaxTrack;
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_CD_INFO, 0, prCDInfo->u32MinTrack,
                            prCDInfo->u32MaxTrack, 0);
      }
      else //if( prCDInfo != NULL)
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      } //else //if( prCDInfo != NULL)
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO:
    { // ATTENTION! 25.05.2015, srt2hi:
      //  The struct member prInfo->szcMediaID[32] contains 32 characters
      //  (the fingerprint of CD) without a trailing '\0')!
      OSAL_trMediaInfo* prInfo = (OSAL_trMediaInfo*)s32Arg;
      if(prInfo != NULL)
      {
        tU8 u8MediaType = CD_u8GetMediaType(pShMem);
        tU8 u8IMT = CD_u8GetInternalMediaType(pShMem);
        int iFingerPrintLen;

        prInfo->u8MediaType = u8MediaType;
        if(u8IMT == CD_INTERNAL_MEDIA_TYPE_MIXED_MODE_MEDIA)
          prInfo->u8FileSystemType = OSAL_C_U8_FS_TYPE_UNKNOWN;
        else
          prInfo->u8FileSystemType = CD_u8GetFSType(pShMem);

        prInfo->nTimeZone = 0; //s8GetTimeZone2App( u32CD);

        // start of: retrieve fingerprint
        // initialize with random
        iFingerPrintLen = iGetFingerPrint(pShMem,
                                           prInfo->szcMediaID,
                                           sizeof(prInfo->szcMediaID),
                                           (unsigned char *)NULL,
                                           0);

        if(u8MediaType == OSAL_C_U8_DATA_MEDIA)
        { //CD-ROM
          #define CDCTRL_HASH_SECTOR_COUNT 10 
          #define CDCTRL_HASH_SECTOR_START 16 
          tU8 *pu8FP; 
          tBool bUseBuffer = FALSE;
          size_t nBufferLen = CDCTRL_HASH_SECTOR_COUNT
                              *
                              CD_USER_BYTES_PER_SECTOR; // size of hash buffer

          pu8FP = (tU8*)malloc(nBufferLen);
          if(pu8FP)
          { // read sectors
            tU32 u32ReadRet;
            tU32 u32LBA = (tU32)CDCTRL_HASH_SECTOR_START;
            tU16 u16Blocks = (tU16)CDCTRL_HASH_SECTOR_COUNT;

            u32ReadRet = CD_SCSI_IF_u32Read(pShMem, u32LBA, u16Blocks, pu8FP);
            bUseBuffer = u32ReadRet == OSAL_E_NOERROR;
          } //if(pu8FP)

          if(bUseBuffer)
          { // calculatue hash over read sectors
            iFingerPrintLen = iGetFingerPrint(pShMem,
                                               prInfo->szcMediaID,
                                               sizeof(prInfo->szcMediaID),
                                               (unsigned char *)pu8FP,
                                               nBufferLen);
          } //if(bUseBuffer)

          if(pu8FP)
            free(pu8FP);
        } //if(u8MediaType == OSAL_C_U8_DATA_MEDIA)
        
        if(u8MediaType == OSAL_C_U8_AUDIO_MEDIA)
        { //CDDA
          tFingerPrint *pFP; 
          size_t nBufferLen = sizeof(tFingerPrint); // size of hash buffer

          pFP = (tFingerPrint*)malloc(nBufferLen);
          if(pFP)
          {
            // force reading of TOC
            (void)CD_u32GetStartOfTrackZLBA(pShMem, hSem, 1);
            // force reading of CD-Text
            (void)CDAUDIO_u32GetText(pShMem, hSem, 0, NULL, NULL,
                                     OSAL_C_S32_CDAUDIO_MAXNAMESIZE - 1);
            // make own copy of TOC and CD-Text
            CD_SM_LOCK(hSem);
            memset(pFP, 0, nBufferLen);
            memcpy(&(pFP->rTOC), &(pShMem->rCD.rTOC), sizeof(CD_sTOC_type)); 
            memcpy(&(pFP->rCDText), &(pShMem->rCDText),
                   sizeof(CDAUDIO_sCDText_type));
            CD_SM_UNLOCK(hSem);
            iFingerPrintLen = iGetFingerPrint(pShMem,
                                               prInfo->szcMediaID,
                                               sizeof(prInfo->szcMediaID),
                                               (unsigned char *)pFP,
                                               nBufferLen);
            free(pFP);
          } //if(pFP)
        } //if(u8MediaType == OSAL_C_U8_AUDIO_MEDIA)
        // end of: retrieve fingerprint of CDDA

        strncpy(prInfo->szcCreationDate, "00.00.0000",
                sizeof(prInfo->szcCreationDate) - 1);
        //strncpy(prInfo->szcMediaID, "UNKNOWN-VOL",
        //        sizeof(prInfo->szcMediaID) - 1);
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_MEDIA_INFO, 0, (tU32)prInfo->u8MediaType,
                            (tU32)prInfo->u8FileSystemType,
                            (tU32)prInfo->nTimeZone);
        char szTmp[sizeof(prInfo->szcMediaID) + 1];
        memset(szTmp, 0, sizeof(szTmp));
        memcpy(szTmp, prInfo->szcMediaID, sizeof(prInfo->szcMediaID));
        CDCTRL_PRINTF_U1("CreationDate [%s] -> MediaID = CD-HASH [%s], "
                         "FingerPrintLen [%d]",
                         (const char*)prInfo->szcCreationDate,
                         (const char*)szTmp,
                         iFingerPrintLen);
      }
      else
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO: // get media device type!
    if((void*)s32Arg != NULL)
    {
      *(tPS32)s32Arg = (tU8)OSAL_EN_MEDIADEV_CD;
      CDCTRL_TRACE_IOCTRL(CDCTRL_EN_GETDEVICEINFO, 0, (tU32)*(tPS32)s32Arg,
                          0, 0);
    }
    else
    {
      u32RetVal = OSAL_E_INVALIDVALUE;
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE:
    {
      OSAL_trDiskType* prDiskType = (OSAL_trDiskType*)s32Arg;
      if(prDiskType != NULL)
      {
        tU8 u8MediaType = CD_u8GetMediaType(pShMem);
        if((u8MediaType == OSAL_C_U8_DATA_MEDIA)
           || (u8MediaType == OSAL_C_U8_AUDIO_MEDIA))
        {
          prDiskType->u8DiskType = ATAPI_C_U8_DISK_TYPE_CD;
          prDiskType->u8DiskSubType = ATAPI_C_U8_DISK_SUB_TYPE_UNKNOWN;
        }
        else
        {
          prDiskType->u8DiskType = ATAPI_C_U8_DISK_TYPE_UNKNOWN;
          prDiskType->u8DiskSubType = ATAPI_C_U8_DISK_SUB_TYPE_UNKNOWN;
        }
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_GETDISKTYPE, 0,
                            (tU32)prDiskType->u8DiskType,
                            (tU32)prDiskType->u8DiskSubType, 0);
      }
      else
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_SETDRIVESPEED:
    if(s32Arg != OSAL_C_S32_IOCTRL_SETNORMALSPEED
       && s32Arg != OSAL_C_S32_IOCTRL_SETHIGHSPEED
       && s32Arg != OSAL_C_S32_IOCTRL_SETLOWNOISESPEED)
    {
      u32RetVal = OSAL_E_INVALIDVALUE;
    }
    else //if != && != && !=
    {
      CDCTRL_TRACE_IOCTRL(CDCTRL_EN_SETDRIVESPEED, 0, (tU32)s32Arg, 0, 0);

      u32RetVal = OSAL_E_NOTSUPPORTED;
    }
    break;

  case OSAL_C_S32_IOCTRL_REG_NOTIFICATION:
    {
      OSAL_trNotifyData *prReg = (OSAL_trNotifyData *)s32Arg;

      if((prReg == NULL) || (prReg->pCallback == NULL))
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
      else //if((prReg == NULL) || (prReg->pCallback == NULL))
      {
        u32RetVal = CD_u32RegisterMediaNotifier(pShMem, prReg);
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_REG_NOTIFICATION, 0, (tU32)s32Arg, 0, 0);
      } //else //if((prReg == NULL) || (prReg->pCallback == NULL))
    }
    break;

  case OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION:
    {
      OSAL_trRelNotifyData *prReg = (OSAL_trRelNotifyData *)s32Arg;
      if(prReg == NULL)
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
      else //if(prReg == NULL)
      {
        u32RetVal = CD_u32UnregisterMediaNotifier(pShMem, prReg);
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_UNREG_NOTIFICATION, 0, (tU32)s32Arg, 0,
                            0);
      } //else //if(prReg == NULL)
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_SETPOWEROFF:
    { // These IO Ctrl is used only by OEDT in order to simulate low-power
      OSAL_tEventHandle hMainEvent = OSAL_C_INVALID_HANDLE;
      u32RetVal = OSAL_E_NOTSUPPORTED;

      (void)OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent);
      if(s32Arg == 1)
      { // OEDT POWER OFF == SLEEP
        (void)OSAL_s32EventPost(hMainEvent,
                                CD_MAIN_EVENT_MASK_DRIVE_SLEEP,
                                OSAL_EN_EVENTMASK_OR);

      }
      else if(s32Arg == 2)
      {  // OEDT POWER ON == ACTIVE
        (void)OSAL_s32EventPost(hMainEvent,
                                CD_MAIN_EVENT_MASK_DRIVE_ACTIVE,
                                OSAL_EN_EVENTMASK_OR);
      }

      if(OSAL_C_INVALID_HANDLE != hMainEvent)
      {
        (void)OSAL_s32EventClose(hMainEvent);
      } //if(OSAL_C_INVALID_HANDLE != hMainEvent)
    }
    break;

  case OSAL_C_S32_IOCTRL_CDCTRL_SETMOTORON:
  case OSAL_C_S32_IOCTRL_CDCTRL_SETMOTOROFF:
  case OSAL_C_S32_IOCTRL_CDCTRL_READERRORBUFFER:
  case OSAL_C_S32_IOCTRL_CDCTRL_EJECTLOCK:
  case OSAL_C_S32_IOCTRL_CDCTRL_GETDVDINFO:
    CD_vCacheClear(pShMem, hSem, TRUE);
    u32RetVal = OSAL_E_NOTSUPPORTED;
    break;

  default:
    u32RetVal = OSAL_E_WRONGFUNC;
    break;
  } //switch( s32Fun)

  CDCTRL_TRACE_IOCTRL(CDCTRL_EN_IOCONTROL_RESULT, 0, (tU32)s32Fun, u32RetVal,
                      0);

  CDCTRL_TRACE_LEAVE_U4(CDID_CDCTRL_u32IOControl, u32RetVal, 0, s32Fun, s32Arg,
                        0);
  return u32RetVal;
}

/************************************************************************
 |function implementation (scope: global)
 |-----------------------------------------------------------------------*/
tU32 CDCTRL_u32Init(tDevCDData *pShMem)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  pShMem->CDCTRL_bTraceOn = LLD_bIsTraceActive(CDCTRL_TRACE_CLASS,
                                               (tS32)TR_LEVEL_USER_1);

  CDCTRL_TRACE_ENTER_U2(CDID_CDCTRL_u32Init, 0, 0, 0, 0);

#ifdef CDCTRL_DEBUG_OPEN_AT_STARTUP_TEST_ENABLED
  vCDCTRLtest();
#endif //#ifdef CDCTRL_DEBUG_OPEN_AT_STARTUP_TEST_ENABLED

  CDCTRL_TRACE_LEAVE_U2(CDID_CDCTRL_u32Init, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

tU32 CDCTRL_u32Destroy(const tDevCDData *pShMem)
{
  (void)pShMem;
  return OSAL_E_NOERROR;
}

#ifdef __cplusplus
}
#endif
