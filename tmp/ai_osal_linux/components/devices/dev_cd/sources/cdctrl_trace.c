/************************************************************************
 | FILE:         cdctrl_trace.c
 | PROJECT:      Gen3
 | SW-COMPONENT: Cdctrl driver
 |------------------------------------------------------------------------
 |************************************************************************/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cdctrl_trace.c
 *
 * @brief   This file includes trace stuff for the cdctrl device
 *
 * @author  srt2hi
 *
 * @date    today
 *
 * @version 1
 *
 * @note    1
 *
 *//* ***************************************************FileHeaderEnd******* */

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif

/* General headers */
/* Basic OSAL includes */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"
#include "cd_main.h"
#include "cd_funcenum.h"
#include "cdctrl.h"
#include "cdctrl_trace.h"
#include "cd_cache.h"

/************************************************************************
 |defines and macros (scope: module-local)
 |-----------------------------------------------------------------------*/
#define CDCTRL_PRINTF_BUFFER_SIZE 256
#define CDCTRL_TRACE_BUFFER_SIZE  256
#define CDCTRL_GET_U32_LE(PU8) (((tU32)(PU8)[0]) | (((tU32)(PU8)[1])<<8)\
                          | (((tU32)(PU8)[2])<<16) | (((tU32)(PU8)[3])<<24))

#define CDCTRL_LOWORD(U32) ((tU16)((U32)&(0xFFFFUL)))
#define CDCTRL_HIWORD(U32) ((tU16)((U32) >> 16))

/************************************************************************
 |typedefs (scope: module-local)
 |----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable inclusion  (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: module-local)
 |-----------------------------------------------------------------------*/
static volatile tBool gTraceCmdThreadIsRunning = FALSE;
static tU8 gu8TraceBuffer[CDCTRL_TRACE_BUFFER_SIZE]; /*not sychronyzed!*/
static OSAL_tIODescriptor ghDevice = OSAL_ERROR;

static OSAL_tThreadID gTraceCmdThreadID = OSAL_ERROR;
static OSAL_tEventHandle ghTraceOsalEvent = 0;
static tBool gbTraceThreadLoop = TRUE;

/************************************************************************
 |function prototype (scope: module-local)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function implementation (scope: module-local)
 |-----------------------------------------------------------------------*/

/*static OSAL_tMSecond tmsTimeDiff(OSAL_tMSecond timeMS)
 {
 OSAL_tMSecond diffMS;
 diffMS = OSAL_ClockGetElapsedTime() - timeMS;
 return diffMS;
 }*/

/*****************************************************************************
 * FUNCTION:     vTraceHex
 * PARAMETER:    Buffer, Length of buffer, number of Bytes printed per Line
 * RETURNVALUE:  result
 * DESCRIPTION:  output hex dump to TTFIS
 ******************************************************************************/
static tVoid vTraceHex(tDevCDData *pShMem, const tU8 *pu8Buf, tInt iBufLen,
                       tInt iBytesPerLine)
{
  tInt iBpl = iBytesPerLine;
  tInt iL, iC;
  char c;
  char szTmp[32];
  char szTxt[CD_COMMON_TEXT_BUFFER_LEN];
  if(NULL == pu8Buf)
  {
    return;
  } //if(NULL == pu8Buf)

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  for(iL = 0; iL < iBufLen; iL += iBpl)
  {
    sprintf(szTxt, "%04X:", (unsigned int)(iL));
    for(iC = 0; iC < iBpl; iC++)
    {
      sprintf(szTmp, " %02X", (unsigned int)pu8Buf[iL + iC]);
      strcat(szTxt, szTmp);
    } //for(iC = 0 ; iC < iBpl ; iC++)

    strcat(szTxt, " ");
    for(iC = 0; iC < iBpl; iC++)
    {
      c = (char)pu8Buf[iL + iC];
      if((c < 0x20) || (c >= 0x80))
      {
        c = '.';
      }
      sprintf(szTmp, "%c", (unsigned char)c);

      strcat(szTxt, szTmp);
    } //for(iC = 0 ; iC < iBpl ; iC++)
    CDCTRL_PRINTF_U1("%s", szTxt);
  } //for(i = 0 ; i < iBufLen ; i += iBpl)
}

/*****************************************************************************
 *
 * FUNCTION:    coszGetErrorText
 *
 * DESCRIPTION: get pointer to osal error text
 *
 *
 *****************************************************************************/
static const char* pGetErrTxt()
{
  return(const char*)OSAL_coszErrorText(OSAL_u32ErrorCode());
}
/*****************************************************************************
 *
 * FUNCTION:
 *     getArgList
 *
 * DESCRIPTION:
 *     This stupid function is used for satifying L I N T
 *
 *
 * PARAMETERS: *va_list pointer to a va_list
 *
 * RETURNVALUE:
 *     va_list
 *
 *
 *
 *****************************************************************************/
static va_list getArgList(va_list* a)
{
  (void)a;
  return *a;
}

/********************************************************************//**
 *  FUNCTION:      uiCGET
 *
 *  @brief         return time in ms
 *
 *  @return        time in ms
 *
 *  HISTORY:
 *
 ************************************************************************/
static unsigned int uiCGET(void)
{
  return(unsigned int)OSAL_ClockGetElapsedTime();
}

/********************************************************************//**
 *  FUNCTION:      CDCTRL_vTraceEnter
 *
 *  @brief         enter function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/

tVoid CDCTRL_vTraceEnter(const tDevCDData *pShMem, tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                         tenDevCDTrcFuncNames enFunction, tU32 u32Par1,
                         tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDCTRL_bTraceOn : TRUE;

  if(bTrace)
  {
    if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      tUInt uiIndex;
      tU8 au8Buf[4 * sizeof(tU32) + 8 * sizeof(tU32)]; //stack?
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) CDCTRL_TRACE_ENTER);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 11);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)enFunction);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par1);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par2);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par3);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par4);
      uiIndex += sizeof(tU32);

      // trace the stuff
      // be aware uiIndex is not greater than aray size!
      // I can not test it at runtime,
      // because L INT throws a pointless warning.
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  } //if(pShMem->CDCTRL_bTraceOn)
}

/********************************************************************//**
 *  FUNCTION:      CDCTRL_vTraceLeave
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDCTRL_vTraceLeave(const tDevCDData *pShMem, tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                         tenDevCDTrcFuncNames enFunction, tU32 u32OSALResult,
                         tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDCTRL_bTraceOn : TRUE;

  if(bTrace)
  {
    if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      tUInt uiIndex;
      tU8 au8Buf[4 * sizeof(tU8) + 9 * sizeof(tU32)]; //stack?
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDCTRL_TRACE_LEAVE);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 11);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)enFunction);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32OSALResult);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par1);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par2);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par3);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par4);
      uiIndex += sizeof(tU32);

      // trace the stuff
      // be aware uiIndex is not greater than aray size!
      // I can not test it at runtime,
      // because L INT throws a pointless warning.
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  } //if(pShMem->CDCTRL_bTraceOn)
}

/********************************************************************//**
 *  FUNCTION:      CDCTRL_vTraceIoctrl
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDCTRL_vTraceIoctrl(const tDevCDData *pShMem, tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                          CDCTRL_enumIoctrlTraceID enIoctrlTraceID,
                          tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
                          tU32 u32Par4)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDCTRL_bTraceOn : TRUE;
  if(bTrace)
  {
    if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      tUInt uiIndex;
      tU8 au8Buf[4 * sizeof(tU8) + 7 * sizeof(tU32)]; //stack?
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDCTRL_TRACE_IOCTRL);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enIoctrlTraceID);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par1);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par2);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par3);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Par4);
      uiIndex += sizeof(tU32);

      // trace the stuff
      // be aware uiIndex is not greater than aray size!
      // I can not test it at runtime,
      // because L INT throws a pointless warning.
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  } //if(pShMem->CDCTRL_bTraceOn)
}

/********************************************************************//**
 *  FUNCTION:      CDCTRL_vTraceIoctrlResult
 *
 *  @brief         error text with IOCtrl-name
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDCTRL_vTraceIoctrlResult(const tDevCDData *pShMem, tU32 u32Class,
                                TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                                tS32 s32OsalIOCtrl, const char *pcszErrorTxt)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDCTRL_bTraceOn : TRUE;
  if(bTrace)
  {
    if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      tUInt uiIndex;
      tU8 au8Buf[4 * sizeof(tU8) + 5 * sizeof(tU32) + 202]; //stack?
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDCTRL_TRACE_IOCTRL_RESULT);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 11);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Time);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)s32OsalIOCtrl);
      uiIndex += sizeof(tU32);
      strncpy((char*)&au8Buf[uiIndex], (const char*)pcszErrorTxt, 200);
      uiIndex += 200;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32)0);
      uiIndex += sizeof(tU8);

      // trace the stuff
      // be aware uiIndex is not greater than aray size!
      // I can not test it at runtime,
      // because L INT throws a pointless warning.
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  } //if(pShMem->CDCTRL_bTraceOn)
}

/*****************************************************************************
 *
 * FUNCTION:
 *     CDCTRL_vTracePrintf
 *
 * DESCRIPTION:
 *     This function creates the printf-style trace message
 * PARAMETERS:
 *
 * RETURNVALUE:
 *     None
 *****************************************************************************/
tVoid CDCTRL_vTracePrintf(const tDevCDData *pShMem, tBool bIsError,
                          tU32 u32Class, TR_tenTraceLevel enTraceLevel,
                          tBool bForced, tU32 u32Line, const char* coszFormat,
                          ...)
{
  tBool bTrace = pShMem != NULL ? pShMem->CDCTRL_bTraceOn : TRUE;
  if(bTrace || bIsError)
  {
    if((bForced) || LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
    {
      static tU8 au8Buf[CDCTRL_PRINTF_BUFFER_SIZE + 1]; /*TODO: ? size ?*/
      tUInt uiIndex;
      long lSize;
      size_t tMaxSize;
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();
      va_list argList;

      uiIndex = 0;
      OSAL_M_INSERT_T8(
                      &au8Buf[uiIndex],
                      (tU32)(enTraceLevel == TR_LEVEL_ERRORS 
                             ? CDCTRL_TRACE_PRINTF_ERROR :
                             CDCTRL_TRACE_PRINTF));
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);
      uiIndex += sizeof(tU8);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);

      argList = getArgList(&argList);
      va_start(argList, coszFormat); /*lint !e718 */
      tMaxSize = CDCTRL_PRINTF_BUFFER_SIZE - uiIndex;
      lSize = vsnprintf((char*)&au8Buf[uiIndex], tMaxSize, coszFormat, argList);
      uiIndex += (tUInt)lSize;
      va_end(argList);

      // trace the stuff
      LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
    }
  } //if(pShMem->CDCTRL_bTraceOn)

}

/*****************************************************************************
 * FUNCTION:     vMediaTypeNotify
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  PRM Callback

 * HISTORY:
 ******************************************************************************/
static tVoid vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
  tU16 u16NotiType = 0;
  tU16 u16State = 0;
  (void)pu32MediaChangeInfo; // L I N T

  if(pu32MediaChangeInfo != NULL)
  {
    u16NotiType = CDCTRL_HIWORD(*pu32MediaChangeInfo);
    u16State = CDCTRL_LOWORD(*pu32MediaChangeInfo);
    CDCTRL_PRINTF_FATAL("vMediaTypeNotify: 0x%08X",
                        (unsigned int)*pu32MediaChangeInfo);

    switch(u16NotiType)
    {
    case OSAL_C_U16_NOTI_MEDIA_CHANGE :
      {
        CDCTRL_PRINTF_FATAL(" NOTI_MEDIA_CHANGE ->");

        switch(u16State)
        {
        case OSAL_C_U16_MEDIA_EJECTED :
          CDCTRL_PRINTF_FATAL("  MEDIA_EJECTED");
          break;

        case OSAL_C_U16_INCORRECT_MEDIA :
          CDCTRL_PRINTF_FATAL("  INCORRECT_MEDIA");
          break;

        case OSAL_C_U16_DATA_MEDIA :
          CDCTRL_PRINTF_FATAL("  DATA_MEDIA");
          break;

        case OSAL_C_U16_AUDIO_MEDIA :
          CDCTRL_PRINTF_FATAL("  AUDIO_MEDIA");
          break;

        case OSAL_C_U16_UNKNOWN_MEDIA :
          CDCTRL_PRINTF_FATAL("  UNKNOWN_MEDIA");
          break;

        default:
          CDCTRL_PRINTF_FATAL("  MEDIA_TYPE_XXXXX 0x%04X",
                              (unsigned int)u16State);
          ;
        } //End of switch
      }
      break;

    case OSAL_C_U16_NOTI_TOTAL_FAILURE :
      {
        CDCTRL_PRINTF_FATAL(" NOTI_TOTAL_FAILURE 0x%04X",
                            (unsigned int)u16State);
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_OK :
          CDCTRL_PRINTF_FATAL("  OSAL_C_U16_DEVICE_OK");
          break;
        case OSAL_C_U16_DEVICE_FAIL :
          CDCTRL_PRINTF_FATAL("  OSAL_C_U16_DEVICE_FAIL");
          break;
        default:
          CDCTRL_PRINTF_FATAL("  OSAL_C_U16_DEVICE_XXX"
                              " UNKNOWN STATE 0x%04X",
                              (unsigned int)u16State);
          ;
        } //switch(u16State)
      }
      break;

    case OSAL_C_U16_NOTI_MODE_CHANGE :
      CDCTRL_PRINTF_FATAL(" NOTI_MODE_CHANGE");
      break;

    case OSAL_C_U16_NOTI_MEDIA_STATE :
      CDCTRL_PRINTF_FATAL(" NOTI_MEDIA_STATE 0x%04X -> ",
                          (unsigned int)u16State);

      switch(u16State)
      {
      case OSAL_C_U16_MEDIA_NOT_READY :
        CDCTRL_PRINTF_FATAL("  OSAL_C_U16_MEDIA_NOT_READY");
        break;
      case OSAL_C_U16_MEDIA_READY :
        CDCTRL_PRINTF_FATAL("  OSAL_C_U16_MEDIA_READY");
        break;
      default:
        CDCTRL_PRINTF_FATAL("  OSAL_C_U16_MEDIA_XXX 0x%04X",
                            (unsigned int)u16State);
        ;
      } //switch(u16MediaState)
      break;

    case OSAL_C_U16_NOTI_DVD_OVR_TEMP :
      CDCTRL_PRINTF_FATAL(" NOTI_DVD_OVR_TEMP");
      break;

    case OSAL_C_U16_NOTI_DEFECT :
      CDCTRL_PRINTF_FATAL(" NOTI_DEFECT");
      switch(u16State)
      {
      case OSAL_C_U16_DEFECT_LOAD_EJECT :
        CDCTRL_PRINTF_FATAL("  DEFECT_LOAD_EJECT");
        break;
      case OSAL_C_U16_DEFECT_LOAD_INSERT :
        CDCTRL_PRINTF_FATAL("  DEFECT_LOAD_INSERT");
        break;
      case OSAL_C_U16_DEFECT_DISCTOC :
        CDCTRL_PRINTF_FATAL("  DEFECT_DISCTOC");
        break;
      case OSAL_C_U16_DEFECT_DISC :
        CDCTRL_PRINTF_FATAL("  DEFECT_DISC");
        break;
      case OSAL_C_U16_DEFECT_READ_ERR :
        CDCTRL_PRINTF_FATAL("  DEFECT_READ_ERR");
        break;
      case OSAL_C_U16_DEFECT_READ_OK :
        CDCTRL_PRINTF_FATAL("  DEFECT_READ_OK");
        break;
      default:
        CDCTRL_PRINTF_FATAL("  OSAL_C_U16_DEFECT_XXX 0x%04X",
                            (unsigned int)u16State);
        break;
      } //switch(u16State)

      break;
    case OSAL_C_U16_NOTI_EJECTKEY :
      CDCTRL_PRINTF_FATAL(" NOTI_EJECTKEY");
      break;
    case OSAL_C_U16_NOTI_DEVICE_READY :
      {
        CDCTRL_PRINTF_FATAL(" NOTI_DEVICE_READY ->");
        switch(u16State)
        {
        case OSAL_C_U16_DEVICE_NOT_READY :
          CDCTRL_PRINTF_FATAL("  DEVICE_NOT_READY");
          break;
        case OSAL_C_U16_DEVICE_READY :
          CDCTRL_PRINTF_FATAL("  DEVICE_READY");
          break;
        case OSAL_C_U16_DEVICE_UV_NOT_READY :
          CDCTRL_PRINTF_FATAL("  DEVICE_UV_NOT_READY");
          break;
        case OSAL_C_U16_DEVICE_UV_READY :
          CDCTRL_PRINTF_FATAL("  DEVICE_UV_READY");
          break;
        default:
          CDCTRL_PRINTF_FATAL("  OSAL_C_U16_DEVICE_XXX 0x%04X",
                              (unsigned int)u16State);
          break;
        } //switch(u16State)
      }
      break;
    case OSAL_C_U16_NOTI_IDLE :
      CDCTRL_PRINTF_FATAL(" NOTI_IDLE");
      break;
    case OSAL_C_U16_NOTI_LOW_POW :
      CDCTRL_PRINTF_FATAL(" NOTI_LOW_POW");
      break;

    default:
      CDCTRL_PRINTF_FATAL(" NOTI_XXX: 0x%04X", (unsigned int)u16NotiType);
      ;
    } //End of switch
  } //if(pu32MediaChangeInfo != NULL)
}

/*****************************************************************************
 * FUNCTION:     bIsOpen
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  checks, if device is open

 * HISTORY:
 ******************************************************************************/
static tBool bIsOpen(tDevCDData *pShMem)
{
  tBool bOpen;

  CD_CHECK_SHARED_MEM(pShMem, FALSE);

  if(OSAL_ERROR != ghDevice)
  {
    bOpen = TRUE;
  }
  else //if(OSAL_ERROR != ghDevice)
  {
    bOpen = FALSE;
    CDCTRL_PRINTF_ERRORS("device not open");
  } //else //if(OSAL_ERROR != ghDevice)

  return bOpen;
}

/*****************************************************************************
 * FUNCTION:     bIOCtrlOK
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  checks, if ioctrl succeeded

 * HISTORY:
 ******************************************************************************/
static tBool bIOCtrlOK(tDevCDData *pShMem, tS32 s32Fun, tS32 s32Ret)
{
  tBool bOK = (s32Ret != OSAL_ERROR);

  CD_CHECK_SHARED_MEM(pShMem, FALSE);

  CDCTRL_PRINTF_IOCTRL_RESULT(s32Fun, bOK ? "SUCCESS" : pGetErrTxt());
  return bOK;
}

/*****************************************************************************
 * FUNCTION:     vTraceCmdThread
 * PARAMETER:    pvData
 * RETURNVALUE:  None
 * DESCRIPTION:  This is a Trace Thread handler.

 * HISTORY:
 ******************************************************************************/
#define CDCTRL_TRACE_EVENT_MASK_ANY  (~0)
#define CDCTRL_TRACE_EVENT_MASK_CMD  0x00000001
#define CDCTRL_TRACE_EVENT_MASK_END  0x00000002

static tVoid vTraceCmdThread(tPVoid pvData)
{
  const tU8 *pu8Data = (const tU8*)pvData;
  const tU8 *pu8Par = &pu8Data[3];
  OSAL_tEventMask tWaitEventMask = 0;
  tU8 u8Len;
  tU8 u8Cmd;
  tU32 u32Ret;
  OSAL_tShMemHandle hShMem = OSAL_ERROR;
  tDevCDData *pShMem = NULL; /*OSAL shared memory*/
  OSAL_tSemHandle hSem = OSAL_C_INVALID_HANDLE;

  (void)pvData;

  u32Ret = CD_u32GetShMem(&pShMem, &hShMem, &hSem);
  if(OSAL_E_NOERROR == u32Ret)
  {
    while(gbTraceThreadLoop)
    {
      (void)OSAL_s32EventWait(ghTraceOsalEvent, CDCTRL_TRACE_EVENT_MASK_ANY,
                              OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER,
                              &tWaitEventMask);
      gTraceCmdThreadIsRunning = TRUE;  //simple sync :-)

      (void)OSAL_s32EventPost(ghTraceOsalEvent, ~tWaitEventMask,
                              OSAL_EN_EVENTMASK_AND);

      u8Len = pu8Data[1];
      u8Cmd = pu8Data[2];
      if(0 != (tWaitEventMask & CDCTRL_TRACE_EVENT_MASK_END))
      {
        gbTraceThreadLoop = FALSE;
      }
      else //if(0 != (tWaitEventMask & CDCTRL_TRACE_EVENT_MASK_END))
      {
        CDCTRL_PRINTF_U4("vTraceCmdThread 0x%08X,"
                         " TraceCmd[Len 0x%02X Cmd 0x%02X]",
                         (unsigned int)pu8Data, (unsigned int)u8Len,
                         (unsigned int)u8Cmd);
        switch(u8Cmd)
        {
        case CDCTRL_TRACE_CMD_TRACE_ON:
          pShMem->CDCTRL_bTraceOn = TRUE;
          break;

        case CDCTRL_TRACE_CMD_TRACE_OFF:
          pShMem->CDCTRL_bTraceOn = FALSE;
          break;

        case CDCTRL_TRACE_CMD_OPEN:
          CDCTRL_PRINTF_U4("OPEN");
          if(!bIsOpen(pShMem))
          {
            ghDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_CDCTRL,
                                    OSAL_EN_READWRITE);

            if(ghDevice == OSAL_ERROR)
            {
              CDCTRL_PRINTF_ERRORS("OPEN: [%s]", pGetErrTxt());
            }
            else //if(ghDevice == OSAL_ERROR)
            {
              CDCTRL_PRINTF_FATAL("OPEN: handle 0x%08X",
                                  (unsigned int)ghDevice);
            } //else //if(ghDevice == OSAL_ERROR)
          }
          else //if(!bIsOpen(pShMem))
          {
            CDCTRL_PRINTF_ERRORS("already open - handle 0x%08X",
                                 (unsigned int)ghDevice);
          } //else //if(!bIsOpen(pShMem))
          break;

        case CDCTRL_TRACE_CMD_CLOSE:
          CDCTRL_PRINTF_U4("CLOSE");
          if(bIsOpen(pShMem))
          {
            tS32 s32Ret = OSAL_s32IOClose(ghDevice);
            if(s32Ret == OSAL_ERROR)
            {
              CDCTRL_PRINTF_ERRORS("CLOSE: [%s]", pGetErrTxt());
            }
            else //if(ghDevice == OSAL_ERROR)
            {
              CDCTRL_PRINTF_FATAL("CLOSE: Success");
              ghDevice = OSAL_ERROR;
            } //else //if(ghDevice == OSAL_ERROR)
          }
          else //if(bIsOpen(pShMem))
          {
            CDCTRL_PRINTF_ERRORS("not opened");
          } //else //if(bIsOpen(pShMem))

          break;

        case CDCTRL_TRACE_CMD_CLOSEDOOR:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL CLOSEDOOR [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
            s32Arg = 0;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_EJECTMEDIA:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL EJECTMEDIA [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
            s32Arg = 0;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GETTEMP:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETTEMP [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trDriveTemperature rTemp;

            rTemp.u8Temperature = 0;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP;
            s32Arg = (tS32)&rTemp;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              CDCTRL_PRINTF_FATAL("Temperature value: [%d]",
                                  (int)rTemp.u8Temperature);
            }
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GETLOADERINFO:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETLOADERINFO [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trLoaderInfo rInfo;

            rInfo.u8LoaderInfoByte = 0;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
            s32Arg = (tS32)&rInfo;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              CDCTRL_PRINTF_FATAL("Loader: [%d]",
                                  (int)rInfo.u8LoaderInfoByte);
            }
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GETTRACKINFO:
          {
            tU8 u8TrackNumber = pu8Par[0];
            CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETTRACKINFO Track [%02u]",
                             (unsigned int)u8TrackNumber);
            {
              tS32 s32Fun, s32Arg, s32Ret;
              OSAL_trCDROMTrackInfo rInfo;

              rInfo.u32TrackNumber = (tU32)u8TrackNumber;
              rInfo.u32TrackControl = 0;
              rInfo.u32LBAAddress = 0;
              s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
              s32Arg = (tS32)&rInfo;
              if(bIsOpen(pShMem))
              {
                s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
              }
              else //if(bIsOpen(pShMem))
              {
                (void)CDCTRL_u32IOOpen((tS32)0);
                (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
                (void)CDCTRL_u32IOClose((tS32)0);
                s32Ret = OSAL_OK;
              } //else //if(bIsOpen(pShMem))
              if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
              {
                CDCTRL_PRINTF_FATAL("GetTrackInfo:"
                                    " Track [%02d],"
                                    " StartLBA [%u],"
                                    " Control  [0x%08X]",
                                    (unsigned int)rInfo.u32TrackNumber,
                                    (unsigned int)rInfo.u32LBAAddress,
                                    (unsigned int)rInfo.u32TrackControl);
              }
            } //if(bIsOpen(pShMem))
          }
          break;
        case CDCTRL_TRACE_CMD_SETMOTORON:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL SETMOTORON [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETMOTORON;
            s32Arg = 0;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_SETMOTOROFF:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL SETMOTOROFF [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETMOTOROFF;
            s32Arg = 0;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_READRAWDATAUNCACHED:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL READRAWDATAUNCACHED -"
                           " MAPPED TO CACHED READ");
          /* FALLTHROUGH */
        case CDCTRL_TRACE_CMD_READRAWDATA:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL READRAWDATA [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS8 *ps8Buf;
            tU32 u32LBA = CDCTRL_GET_U32_LE(&pu8Par[0]);
            tU32 u32NumBlocks = CDCTRL_GET_U32_LE(&pu8Par[4]);
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trReadRawInfo rRRInfo;

            ps8Buf = (tS8*)malloc(u32NumBlocks * 2048);
            if(ps8Buf)
            {
              memset(ps8Buf, 0xFF, u32NumBlocks * 2048);
            }

            rRRInfo.u32NumBlocks = u32NumBlocks;
            rRRInfo.u32LBAAddress = u32LBA;
            rRRInfo.ps8Buffer = ps8Buf;

            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA;
            s32Arg = (tS32)&rRRInfo;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              //tU32 u32Block, u32BlockIndex;
              CDCTRL_PRINTF_FATAL("ReadRawDataLBA:"
                                  " LBA %u,"
                                  " Blocks %u,"
                                  " BufferAddress 0x%08X",
                                  (unsigned int)rRRInfo.u32LBAAddress,
                                  (unsigned int)rRRInfo.u32NumBlocks,
                                  (unsigned int)rRRInfo.ps8Buffer);
              //print blocks
              vTraceHex(pShMem, (tPU8)rRRInfo.ps8Buffer,
                        (tInt)u32NumBlocks * 2048, 16);
            } //if(bIOCtrlOK(pShMem, s32Fun, s32Ret))

            if(ps8Buf != NULL)
            {
              free(ps8Buf);
            } //if(ps8Buf != NULL)
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_READRAWDATA_MSF:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL READRAWDATA_MSF [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS8 *ps8Buf;
            tU8 u8Min = pu8Par[0];
            tU8 u8Sec = pu8Par[1];
            tU8 u8Frame = pu8Par[2];
            tU32 u32NumBlocks = CDCTRL_GET_U32_LE(&pu8Par[4]);
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trReadRawMSFInfo rRRInfo;

            ps8Buf = (tS8*)malloc(u32NumBlocks * 2048);
            if(ps8Buf)
            {
              memset(ps8Buf, 0xFF, u32NumBlocks * 2048);
            }

            rRRInfo.u32NumBlocks = u32NumBlocks;
            rRRInfo.u8Minute = u8Min;
            rRRInfo.u8Second = u8Sec;
            rRRInfo.u8Frame = u8Frame;
            rRRInfo.ps8Buffer = ps8Buf;

            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF;
            s32Arg = (tS32)&rRRInfo;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              CDCTRL_PRINTF_FATAL("ReadRawDataMSF:"
                                  " MSF %02u:%02u:%02u,"
                                  " Blocks %u,"
                                  " BufferAddress 0x%08X",
                                  (unsigned int)rRRInfo.u8Minute,
                                  (unsigned int)rRRInfo.u8Second,
                                  (unsigned int)rRRInfo.u8Frame,
                                  (unsigned int)rRRInfo.u32NumBlocks,
                                  (unsigned int)rRRInfo.ps8Buffer);
              //print blocks
              vTraceHex(pShMem, (tPU8)rRRInfo.ps8Buffer,
                        (tInt)u32NumBlocks * 2048, 16);
            } //if(bIOCtrlOK(pShMem, s32Fun, s32Ret))

            if(ps8Buf != NULL)
            {
              free(ps8Buf);
            } //if(ps8Buf != NULL)
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GETCDINFO:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETCDINFO [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trCDInfo rCDInfo;

            rCDInfo.u32MaxTrack = 0;
            rCDInfo.u32MinTrack = 0;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
            s32Arg = (tS32)&rCDInfo;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              CDCTRL_PRINTF_FATAL("CDInfo: MinTrack %02u"
                                  " MaxTrack %02u",
                                  (unsigned int)rCDInfo.u32MinTrack,
                                  (unsigned int)rCDInfo.u32MaxTrack);
            }
          }
          break;
        case CDCTRL_TRACE_CMD_GET_DRIVE_VERSION:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL"
                           " GET_DRIVE_VERSION [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trCDDriveVersion rVersion;

            rVersion.u32HWVersion = 0;
            rVersion.u32SWVersion = 0;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GET_DRIVE_VERSION;
            s32Arg = (tS32)&rVersion;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              CDCTRL_PRINTF_FATAL("Version: HW 0x%02X SW 0x%02X",
                                  (unsigned int)rVersion.u32HWVersion,
                                  (unsigned int)rVersion.u32SWVersion);
            }
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_SETPOWEROFF:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL SETPOWEROFF [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETPOWEROFF;
            s32Arg = (tS32)pu8Par[0];
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GETMEDIAINFO:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETMEDIAINFO [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trMediaInfo rInfo;

            rInfo.nTimeZone = 0;
            rInfo.szcCreationDate[0] = '\0';
            rInfo.szcMediaID[0] = '\0';
            rInfo.u8FileSystemType = 0;
            rInfo.u8MediaType = 0;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO;
            s32Arg = (tS32)&rInfo;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              char szTmp[33];
              memcpy(szTmp, rInfo.szcMediaID, 32);
              szTmp[32]='\0';
              CDCTRL_PRINTF_FATAL("MediaInfo: MediaType %u"
                                  " FS %u TimeZone %u"
                                  " Date [%s]"
                                  " ID [%s]",
                                  (unsigned int)rInfo.u8MediaType,
                                  (unsigned int)rInfo.u8FileSystemType,
                                  (unsigned int)rInfo.nTimeZone,
                                  (const char*)rInfo.szcCreationDate,
                                  (const char*)szTmp);
            }
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GETDEVICEINFO:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETDEVICEINFO [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            tU32 u32MediaDev = 0;

            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO;
            s32Arg = (tS32)&u32MediaDev;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              CDCTRL_PRINTF_FATAL("GetDeviceInfo: MediaDev 0x%04X",
                                  (unsigned int)u32MediaDev);
            }
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GETDISKTYPE:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETDISKTYPE [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trDiskType rDiskType;

            rDiskType.u8DiskSubType = 0;
            rDiskType.u8DiskType = 0;

            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE;
            s32Arg = (tS32)&rDiskType;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              CDCTRL_PRINTF_FATAL("GetDiskType: Type %u, SubType %u",
                                  (unsigned int)rDiskType.u8DiskType,
                                  (unsigned int)rDiskType.u8DiskSubType);
            }
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GETDRIVEVERSION:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETDRIVEVERSION [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trDriveVersion rVersion;

            rVersion.u16MajorVersionNumber = 0;
            rVersion.u16MinorVersionNumber = 0;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDRIVEVERSION;
            s32Arg = (tS32)&rVersion;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              /*! au8.. are not zero teminated!
               it is only working without crash
               as long Major/Minor contain at least one 0x00
               and are last members in structur*/
              CDCTRL_PRINTF_FATAL(
                                 "VersionFULL:"
                                 " MajorVersion 0x%04X"
                                 " MinorVersion 0x%04X",
                                 (unsigned int)rVersion.u16MajorVersionNumber,
                                 (unsigned int)rVersion.u16MinorVersionNumber);
              CDCTRL_PRINTF_FATAL(" FirmwarRevision [%s]",
                                  (const char*)rVersion.au8FirmwareRevision);
              CDCTRL_PRINTF_FATAL(" Model [%s],",
                                  (const char*)rVersion.au8ModelNumber);
              CDCTRL_PRINTF_FATAL(" Serial [%s]",
                                  (const char*)rVersion.au8SerialNumber);
            }
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GET_DEVICE_VERSION:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL"
                           " GET_DEVICE_VERSION [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            tU32 u32Version = 0;

            s32Fun = OSAL_C_S32_IOCTRL_VERSION;
            s32Arg = (tS32)&u32Version;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            if(bIOCtrlOK(pShMem, s32Fun, s32Ret))
            {
              CDCTRL_PRINTF_FATAL("IOCtrlVersion: 0x%04X",
                                  (unsigned int)u32Version);
            }
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_SETDRIVESPEED:
          {
            tU8 u8SpeedID = pu8Par[0];
            CDCTRL_PRINTF_U4("TraceCmd IOCTRL"
                             " SETDRIVESPEED [SpeedID %u]",
                             (unsigned int)u8SpeedID);
            {
              tS32 s32Fun, s32Arg, s32Ret;

              s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETDRIVESPEED;
              s32Arg = (tS32)u8SpeedID;
              if(bIsOpen(pShMem))
              {
                s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
              }
              else //if(bIsOpen(pShMem))
              {
                (void)CDCTRL_u32IOOpen((tS32)0);
                (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
                (void)CDCTRL_u32IOClose((tS32)0);
                s32Ret = OSAL_OK;
              } //else //if(bIsOpen(pShMem))
              (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
            } //if(bIsOpen(pShMem))

          }
          break;
        case CDCTRL_TRACE_CMD_READERRORBUFFER:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL READERRORBUFFER [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READERRORBUFFER;
            s32Arg = 0;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_GETDVDINFO:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETDVDINFO [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDVDINFO;
            s32Arg = 0;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
          } //if(bIsOpen(pShMem))
          break;
        case CDCTRL_TRACE_CMD_EJECTLOCK:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL EJECTLOCK [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTLOCK;
            s32Arg = 0;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
          } //if(bIsOpen(pShMem))
          break;

        case CDCTRL_TRACE_CMD_REG_NOTIFICATION:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL REG_NOTIFICATION [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trNotifyData rReg;

            rReg.pu32Data = 0;
            rReg.ResourceName = OSAL_C_STRING_RES_CDCTRL;
            rReg.u16AppID = OSAL_C_U16_AUDIOCD_APPID;
            rReg.u8Status = 0;
            rReg.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE
                                       | OSAL_C_U16_NOTI_MEDIA_STATE 
                                       | OSAL_C_U16_NOTI_DEVICE_READY
                                       | OSAL_C_U16_NOTI_TOTAL_FAILURE 
                                       | OSAL_C_U16_NOTI_DEFECT;

            rReg.pCallback = vMediaTypeNotify;

            s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
            s32Arg = (tS32)&rReg;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
            (void)rReg;
          } //if(bIsOpen(pShMem))
          break;

        case CDCTRL_TRACE_CMD_UNREG_NOTIFICATION:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL UNREG_NOTIFICATION [0x%02X]",
                           (unsigned int)pu8Par[0]);
          {
            tS32 s32Fun, s32Arg, s32Ret;
            OSAL_trRelNotifyData rReg;

            rReg.ResourceName = OSAL_C_STRING_RES_CDCTRL;
            rReg.u16AppID = OSAL_C_U16_AUDIOCD_APPID;
            rReg.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE
                                       | OSAL_C_U16_NOTI_MEDIA_STATE 
                                       | OSAL_C_U16_NOTI_DEVICE_READY
                                       | OSAL_C_U16_NOTI_TOTAL_FAILURE 
                                       | OSAL_C_U16_NOTI_DEFECT;
            s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
            s32Arg = (tS32)&rReg;
            if(bIsOpen(pShMem))
            {
              s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
            }
            else //if(bIsOpen(pShMem))
            {
              (void)CDCTRL_u32IOOpen((tS32)0);
              (void)CDCTRL_u32IOControl((tS32)0, s32Fun, s32Arg);
              (void)CDCTRL_u32IOClose((tS32)0);
              s32Ret = OSAL_OK;
            } //else //if(bIsOpen(pShMem))
            (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
            (void)rReg;
          } //if(bIsOpen(pShMem))
          break;

        case CDCTRL_TRACE_CMD_MASCA_CMD:
          {
            u8Len = pu8Par[0];
            const tU8 *pu8MascaCmd = &pu8Par[1];
            CDCTRL_PRINTF_U1("TraceCmd MASCA_CMD Len 0x%02X, CMD 0x%02X",
                             (unsigned int)u8Len,
                             (unsigned int)pu8MascaCmd[0]);
          }
          break;

        case CDCTRL_TRACE_CMD_MASCA_RESET:
          CDCTRL_PRINTF_U1("TraceCmd MASCA_RESET [0x%02X]",
                           (unsigned int)pu8Par[0]);
          break;

        default:
          CDCTRL_PRINTF_U4("TraceCmd IOCTRL DEFAULT  - NOT SUPPORTED");
        } //switch(u8Cmd)
      } //else //if(0 != (tWaitEventMask & CDCTRL_TRACE_EVENT_MASK_END))
      gTraceCmdThreadIsRunning = FALSE;
    } //while(gbTraceThreadLoop)
  } //if(OSAL_E_NOERROR == u32Ret)

  CD_vReleaseShMem(pShMem, hShMem, hSem);
}

/*****************************************************************************
 * FUNCTION:     CDCTRL_vTraceCmdCallback
 * PARAMETER:    buffer
 * RETURNVALUE:  None
 * DESCRIPTION:  This is a Trace callback handler.

 * HISTORY:
 ******************************************************************************/
tVoid CDCTRL_vTraceCmdCallback(const tU8* pcu8Buffer)
{
  if(pcu8Buffer != NULL)
  {
    // create trace event, if not exist */
    if(0 == ghTraceOsalEvent)
    {
      const char *pcszEventName = "CDCTRL_TRACE";
      if(OSAL_OK != OSAL_s32EventCreate(pcszEventName, &ghTraceOsalEvent))
      {
        tU32 u32Err = OSAL_u32ErrorCode();
        CDCTRL_PRINTF_ERRORS("TRACE  ERROR 0x%08X, "
                             "OSAL_s32EventCreate(%s)",
                             (unsigned int)u32Err, pcszEventName);
      }
    } //if(0 == ghTraceOsalEvent)

    //spawn thread, if not exist
    if(OSAL_ERROR == gTraceCmdThreadID)
    {
      OSAL_trThreadAttribute traceCmdThreadAttr;

      traceCmdThreadAttr.szName = "CDCTRLTRACE";
      traceCmdThreadAttr.u32Priority = 0;
      traceCmdThreadAttr.pfEntry = vTraceCmdThread;
      traceCmdThreadAttr.pvArg = gu8TraceBuffer;
      traceCmdThreadAttr.s32StackSize = 1024;

      gTraceCmdThreadID = OSAL_ThreadSpawn(&traceCmdThreadAttr);
      if(OSAL_ERROR == gTraceCmdThreadID)
      {
        CDCTRL_PRINTF_ERRORS("Create TraceThread");
      } //if(OSAL_ERROR == traceCmdThreadID)
    } //if(OSAL_ERROR == gTraceCmdThreadID)

    if(!gTraceCmdThreadIsRunning)
    {
      tU8 u8Len = pcu8Buffer[0];
      gTraceCmdThreadIsRunning = TRUE;

      (void)OSAL_pvMemoryCopy(gu8TraceBuffer, pcu8Buffer, u8Len + 1);
      (void)OSAL_s32EventPost(ghTraceOsalEvent,
                              CDCTRL_TRACE_EVENT_MASK_CMD,
                              OSAL_EN_EVENTMASK_OR);
    }
    else //if(!gTraceCmdThreadIsRunning)
    {
      CDCTRL_PRINTF_ERRORS("last Trace thread is running yet");
    } //else //if(!gTraceCmdThreadIsRunning)
  } //if(pcu8Buffer != NULL)
}

#ifdef CDCTRL_DEBUG_OPEN_AT_STARTUP_TEST_ENABLED
void vCDCTRLtest(tDevCDData *pShMem)
{
  int i;
  CD_CHECK_SHARED_MEM_VOID(pShMem);
  CDCTRL_PRINTF_U1("vCDCTRLtest");
  if(OSAL_ERROR == ghDevice)
  {
    ghDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL,
                             OSAL_EN_READWRITE);

    if(ghDevice == OSAL_ERROR)
    {
      CDCTRL_PRINTF_ERRORS("OPEN: [%s]", pGetErrTxt());
    }
    else //if(ghDevice == OSAL_ERROR)
    {
      CDCTRL_PRINTF_FATAL("OPEN: handle 0x%08X",
                          (unsigned int)ghDevice);
    } //else //if(ghDevice == OSAL_ERROR)
  }
  else //if(OSAL_ERROR == ghDevice)
  {
    CDCTRL_PRINTF_ERRORS("already open - handle 0x%08X",
                         (unsigned int)ghDevice);
  } //else //if(OSAL_ERROR == ghDevice)

  if(bIsOpen(pShMem))
  {
    tS32 s32Fun, s32Arg, s32Ret;
    OSAL_trNotifyData rReg;

    rReg.pu32Data = 0;
    rReg.ResourceName = OSAL_C_STRING_RES_CDCTRL;
    rReg.u16AppID = OSAL_C_U16_AUDIOCD_APPID;
    rReg.u8Status = 0;
    rReg.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE
                               | OSAL_C_U16_NOTI_MEDIA_STATE
                               | OSAL_C_U16_NOTI_DEVICE_READY
                               | OSAL_C_U16_NOTI_TOTAL_FAILURE
                               | OSAL_C_U16_NOTI_DEFECT;

    rReg.pCallback = vMediaTypeNotify;

    s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
    s32Arg = (tS32)&rReg;
    s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
    (void)bIOCtrlOK(pShMem, s32Fun, s32Ret);
    (void)rReg;
  } //if(bIsOpen(pShMem))

  for(i=0;i<10; i++)
  {
    tU32 u32Ret;
    tU8 u8MedStat = 0;

    {
      u32Ret = CDCTRL_u32ATAGetMediaStatus(&u8MedStat);
      if(OSAL_E_NOERROR == u32Ret)
      {
        CDCTRL_PRINTF_U1("TEST MEDIASTATE: 0x%02X",
                         (unsigned int)u8MedStat);
      } //if(OSAL_E_NOERROR == u32Ret)
      OSAL_s32ThreadWait(100);
    } //if(CDCTRL_C_U8_MEDIA_STATUS_UNKNOWN == u8LI)
  }

}
#endif //#ifdef CDCTRL_DEBUG_OPEN_AT_STARTUP_TEST_ENABLED
#ifdef __cplusplus
}
#endif
/************************************************************************
 |end of file
 |-----------------------------------------------------------------------*/
