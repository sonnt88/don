/************************************************************************
 | $Revision: 1.0 $
 | $Date: 2012/01/01 $
 |************************************************************************
 | FILE:         cd_main.c
 | PROJECT:      GEN3
 | SW-COMPONENT: OSAL I/O Device
 |------------------------------------------------------------------------
 | DESCRIPTION:
 |  This file contains global functionaliy
 |------------------------------------------------------------------------
 | Date      | Modification                | Author
 | 2013      | GEN3                        | srt2hi
 | 28/09/2016| Bugfix for NCG3D-23434      | boc7kor
 | 25/10/2016| Changes added to accomodate | boc7kor
 |           | three value for the registry|
 |           | entry "CD_LOW_VOLTAGE_      |
 |           | HANDLING_THRESHOLD"         |
 | 25/11/2016| Bugfix for NCG3D-29329      | boc7kor
 |************************************************************************
 |************************************************************************/

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/
#include <poll.h>
#include <unistd.h>
#include <libudev.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <scsi/sg.h> /* take care: fetches glibc's /usr/include/scsi/sg.h */

/* Basic OSAL includes */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"

#include "cd_funcenum.h"
#include "cd_main.h"
#include "cd_cache.h"
#include "cd_trace.h"
#include "cdaudio.h"
#include "cdaudio_trace.h"
#include "cdaudio_cdtext.h"
#include "cdctrl.h"
#include "cdctrl_trace.h"
#include "cd_scsi_if.h"

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: module-local)
 |-----------------------------------------------------------------------*/
//#define CD_STOP_SUBCHANNEL_TIMER_AUTOMATICALLY

#define DEV_CD_TRACE_COMMAND_CHANNEL_CDCTRL_ID  0x01
#define DEV_CD_TRACE_COMMAND_CHANNEL_CDAUDIO_ID 0x02
#define DEV_CD_TRACE_COMMAND_CHANNEL_CD_ID      0x03

//values of the registry key CD_LOW_VOLTAGE_HANDLING_THRESHOLD
#define CD_LOW_VOLTAGE_HANDLING_THRESHOLD_NOT_CONFIGURED    0x00000000
#define CD_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITICAL          0x00000002

//used by u16WhereIsPlayPosInPlayRange
#define CD_U16_PLAYRANGE_LBOUND       0x01 /*recent play time matched
                                                 lower bound of playrange*/
#define CD_U16_PLAYRANGE_UBOUND       0x02 /*recent play time matched
                                                  upper bound of playrange*/
#define CD_U16_PLAYRANGE_IN_RANGE     0x04 /*recent play time is in recent
                                                  playrange, not on
                                                  one of bounds*/
#define CD_U16_PLAYRANGE_OUT_OF_RANGE 0x08 /*recent play time is
                                                  out of recent playrange*/
#define CD_U16_PLAYRANGE_NEAR_LBOUND  0x0100 /*recent play time is in range
                                                    an d near lower bound
                                                    of playrange*/
#define CD_U16_PLAYRANGE_NEAR_UBOUND  0x0200 /*recent play time is in range
                                                    and near upper bound
                                                    of playrange*/
#define CD_U16_PLAYRANGE_BELOW_LBOUND 0x0400 /*recent play time is out of
                                                    range and less than
                                                    lower bound
                                                    of playrange*/
#define CD_U16_PLAYRANGE_ABOVE_UBOUND 0x0800 /*recent play time is out of
                                                    range and greater than
                                                    upper bound
                                                    of playrange*/

#define CD_U16_PLAYRANGE_ID_MASK                0x00FF
#define CD_U16_PLAYRANGE_ADDITIONALINFO_MASK    0xFF00

/*number of sectors when
 CD_U16_PLAYRANGE_NEAR_XBOUND is set before limits of playrange is reached*/
#define CD_U16_PLAYRANGE_NEAR_FRAMES ( 1 * CD_SECTORS_PER_SECOND)

//SCSI Audio Status (returned by READ_SUBCHANNEL)
#define CD_AUDIO_STATUS_NOT_VALID                      0x00
#define CD_AUDIO_STATUS_PLAY_IN_PROGRESS               0x11
#define CD_AUDIO_STATUS_PLAY_PAUSED                    0x12
#define CD_AUDIO_STATUS_PLAY_COMPLETED                 0x13
#define CD_AUDIO_STATUS_PLAY_STOPPED_DUE_TO_ERROR      0x14
#define CD_AUDIO_STATUS_NO_STATUS                      0x15
#define CD_AUDIO_STATUS_PLAY_STOPPED_DUE_TO_LOWVOLTAGE 0x94  /*simulated*/

//polling time of playtime
#define CD_SUBCHANNEL_TIMER_LOW_MS 225
#define CD_SUBCHANNEL_TIMER_HI_MS  66

//polling time of LoaderState
#define CD_LOADERSTATE_TIMER_LOW_MS 1000
#define CD_LOADERSTATE_TIMER_HI_MS  100

//Watchdog
#define CD_WATCHDOG_TIMER_MS        1000

//defines used only for shorten long identifier (DEV_VOLT)
#define CD_VOLT_I(X) DEV_VOLT_C_U32_BIT_MASK_INDICATE_ ## X
#define CD_VIO(X)    OSAL_C_S32_IOCTRL_VOLT_ ## X

/************************************************************************
 |typedefs (scope: module-local)
 |----------------------------------------------------------------------*/
//local data 
typedef struct CD_rMainData_tag
{
  OSAL_tEventHandle hMainEvent;
  OSAL_tTimerHandle hSubchannelTimer;
  OSAL_tTimerHandle hLoaderStateTimer;
  OSAL_tTimerHandle hWatchdogTimer;
  tBool bMainThreadIsRunning;
  tBool bVoltThreadIsRunning;
  tBool bUdevThreadIsRunning;
  tBool bUdevThreadDoLoop;
  OSAL_tIODescriptor rTraceIODescriptor;
  OSAL_tShMemHandle hShMem;
  tDevCDData *pSharedMem;
  DEV_VOLT_trClientRegistration sVolt;
  OSAL_tIODescriptor hVolt;
  /*stores the value of the registry key 
  "CD_LOW_VOLTAGE_HANDLING_THRESHOLD" in osal.reg*/
  tU32 u32LowVoltageHandlingThreshold;
} CD_trMainData;

/************************************************************************
 | variable inclusion  (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: module-local)
 |-----------------------------------------------------------------------*/

//module data
static CD_trMainData grMainData;

/************************************************************************
 |function prototype (scope: module-local)
 |-----------------------------------------------------------------------*/
static tU16 u16WhereIsPlayPosInPlayRange(tDevCDData *pShMem, tU32 u32ZLBA);

static tVoid vNotifyPlayInfoClients(tDevCDData *pShMem,
                                    const OSAL_trPlayInfo* prInfo,
                                    tBool bForced);
static tVoid vNotifyMediaClients(tDevCDData *pShMem, tU16 u16Notification,
                                 tU16 u16Status);

static tU32 u32SetDriveStatus(tDevCDData *pShMem, tU16 u16Type, tU32 u32Status);

tU32 u32ExecuteCallback(tS32 s32OwnPrcId, tS32 callbackpid,
                        OSAL_tpfCallback pcallback, tPVoid pcallbackarg,
                        tU32 callbackargsize);
						
static tVoid CD_vReadRegistryValues(tDevCDData *pShMem);
static tVoid CD_vSG_Check_Notify(tDevCDData *pShMem);
/************************************************************************
 |function implementation (scope: module-local)
 |-----------------------------------------------------------------------*/

/*****************************************************************************
 * FUNCTION:     vZLBA2MSF
 * PARAMETER:    IN: ZLBA
 *               OUT: Pointer to struct MSF
 * RETURNVALUE:  void, MSF
 * DESCRIPTION:  calculates MSF from ZLBA
 ******************************************************************************/
static void vZLBA2MSF(tU32 u32ZLBA, CD_sTMSF_type *pMSF)
{
  if(NULL != pMSF)
  {
    tU32 u32Second;
    u32Second = u32ZLBA / CD_SECTORS_PER_SECOND;
    pMSF->u8Min = (tU8)(u32Second / 60);
    pMSF->u8Sec = (tU8)(u32Second % 60);
    pMSF->u8Frm = (tU8)(u32ZLBA % CD_SECTORS_PER_SECOND);

  }
}

/*****************************************************************************
 * FUNCTION:     u16WhereIsPlayPosInPlayRange
 * PARAMETER:    DriveIndex
 *               IN: u32ZLBA
 * RETURNVALUE:  one of CD_U16_PLAYRANGE_XXX values
 * DESCRIPTION:  check if ZLBA lies within current playrange
 ******************************************************************************/
tU16 CD_u16WhereIsPlayPosInPlayRange(tDevCDData *pShMem, tU32 u32ZLBA)
{ //debug wrapper
  return u16WhereIsPlayPosInPlayRange(pShMem, u32ZLBA);
}

static tU16 u16WhereIsPlayPosInPlayRange(tDevCDData *pShMem, tU32 u32ZLBA)
{
  tU16 u16RangeID;
  tU32 u32SZLBA;
  tU32 u32EZLBA;

  CD_CHECK_SHARED_MEM(pShMem, (tU16)0);

  CD_TRACE_ENTER_U4(CDID_u16WhereIsPlayPosInPlayRange, 0, u32ZLBA, 0, 0);

  u32SZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA;
  u32EZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA;

  /* Check for u32StartZLBA == CD_U32NONE as this could be a special case 
   arising after setting plarange for REPEAT/RANDOM with KeyValue(/Magic) 0xFF
   as start track number (to continue from actual position)*/
  if((CDAUDIO_U32NONE == pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA)
     && pShMem->rCDAUDIO.bPlayInfoValid)
  {
    u32SZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32PrevZLBA;
  }

  if(u32ZLBA > u32SZLBA && u32ZLBA < u32EZLBA)
  { //in range
    u16RangeID = CD_U16_PLAYRANGE_IN_RANGE;

    if(u32ZLBA <= (u32SZLBA + CD_U16_PLAYRANGE_NEAR_FRAMES))
    { // near lower bound
      u16RangeID |= CD_U16_PLAYRANGE_NEAR_LBOUND;
    }

    if(u32ZLBA >= (u32EZLBA - CD_U16_PLAYRANGE_NEAR_FRAMES))
    { // near upper bound
      u16RangeID |= CD_U16_PLAYRANGE_NEAR_UBOUND;
    }

  }
  else if(u32ZLBA == u32SZLBA)
  { // lower bound
    u16RangeID = CD_U16_PLAYRANGE_LBOUND | CD_U16_PLAYRANGE_NEAR_LBOUND;
  }
  else if(u32ZLBA == u32EZLBA)
  { // upper bound
    u16RangeID = CD_U16_PLAYRANGE_UBOUND | CD_U16_PLAYRANGE_NEAR_UBOUND;
  }
  else
  { // out of range
    u16RangeID = CD_U16_PLAYRANGE_OUT_OF_RANGE;
    if(u32ZLBA < u32SZLBA)
    {
      u16RangeID |= CD_U16_PLAYRANGE_BELOW_LBOUND;
    } //if(u32ZLBA < u32SZLBA)
    if(u32ZLBA > u32EZLBA)
    {
      u16RangeID |= CD_U16_PLAYRANGE_ABOVE_UBOUND;
    } //if(u32ZLBA > u32EZLBA)
  }

  CD_PRINTF_U2("u16WhereIsPlayPosInPlayRange SZLBA %05u, "
               "EZLBA %05u, CurZLBA %05u, RangeID: 0x%04X",
               (unsigned int)u32SZLBA, (unsigned int)u32EZLBA,
               (unsigned int)u32ZLBA, (unsigned int)u16RangeID);

  CD_TRACE_LEAVE_U4(CDID_u16WhereIsPlayPosInPlayRange, OSAL_E_NOERROR, u32ZLBA,
                    u16RangeID, 0, 0);

  return u16RangeID;
}

/*****************************************************************************
 * FUNCTION:     vSubchannelTimerCallback
 * PARAMETER:
 * RETURNVALUE:
 * DESCRIPTION:  timer for reading subchannel data from CD
 * HISTORY:
 ******************************************************************************/
static tVoid vSubchannelTimerCallback(tPVoid pvData)
{
  CD_trMainData *pMTD = (CD_trMainData*)pvData;
  if((pMTD != NULL) && (pMTD->hMainEvent != 0))
  {
    (void)OSAL_s32EventPost(pMTD->hMainEvent,
                            CD_MAIN_EVENT_MASK_SUBCHANNEL_TIMER,
                            OSAL_EN_EVENTMASK_OR);
  }
}

/*****************************************************************************
 * FUNCTION:     vLoaderStateTimerCallback
 * PARAMETER:
 * RETURNVALUE:
 * DESCRIPTION:  timer for reading subchannel data from CD
 * HISTORY:
 ******************************************************************************/
static tVoid vLoaderStateTimerCallback(tPVoid pvData)
{
  CD_trMainData *pMTD = (CD_trMainData*)pvData;
  if((pMTD != NULL) && (pMTD->hMainEvent != 0))
  {
    (void)OSAL_s32EventPost(pMTD->hMainEvent,
                            CD_MAIN_EVENT_MASK_LOADERSTATE_TIMER,
                            OSAL_EN_EVENTMASK_OR);
  }
}

/*****************************************************************************
 * FUNCTION:     vWatchdogTimerCallback
 * PARAMETER:
 * RETURNVALUE:
 * DESCRIPTION:  timer for check drive
 * HISTORY:
 ******************************************************************************/
static tVoid vWatchdogTimerCallback(tPVoid pvData)
{
  CD_trMainData *pMTD = (CD_trMainData*)pvData;
  if((pMTD != NULL) && (pMTD->hMainEvent != 0))
  {
    (void)OSAL_s32EventPost(pMTD->hMainEvent,
                            CD_MAIN_EVENT_MASK_WATCHDOG_TIMER,
                            OSAL_EN_EVENTMASK_OR);
  }
}

/*****************************************************************************
 * FUNCTION:     u32InitializePlayInfo
 * PARAMETER:    DriveIndex
 *               OUT: play info
 * RETURNVALUE:  OSAL error code
 * DESCRIPTION:  set playinfo to defaults
 ******************************************************************************/
static tU32 u32InitializePlayInfo(tDevCDData *pShMem, OSAL_trPlayInfo* prInfo)
{
  tU32 u32Ret = OSAL_E_INVALIDVALUE;
  static const OSAL_trPlayInfo rDefaultInfo =
  {
    { 0x00, 0x00, 0x00, 0x00},
    { 0x00, 0x00, 0x00, 0x00}, 0,
    OSAL_C_S32_AUDIO_STATUS_NOT_VALID};

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_u32InitializePlayInfo, 0, prInfo, 0, 0);

  if(NULL != prInfo)
  {
    *prInfo = rDefaultInfo;

    if(pShMem->rCDAUDIO.bPlayInfoValid)
    {
      *prInfo = pShMem->rCDAUDIO.rPlayInfo;
      prInfo->u32StatusPlay = CD_u32GetPlayStatus(pShMem);
      u32Ret = OSAL_E_NOERROR;
    } //if(pShMem->rCDAUDIO.bPlayInfoValid)
  } //if(NULL != prInfo)

  CD_TRACE_LEAVE_U4(CDID_u32InitializePlayInfo, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     u32FakePlayPosTF
 * PARAMETER:    pointer to shared memory
 *               Handle of Semaphore
 * RETURNVALUE:  OSAL ERROR code
 * DESCRIPTION:  send start of playrange to registered clients
 *               Called only by MainThread!
 * HISTORY:
 ******************************************************************************/
static tU32 u32FakePlayPosTF(tDevCDData *pShMem, OSAL_tSemHandle hSem)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  OSAL_trPlayInfo rLocalInfo = { 0};

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_u32FakePlayPosTF, 0, 0, 0, 0);

  CD_SM_LOCK(hSem);
  {
    tU32 u32ZLBA = 0;
    CD_sTMSF_type rAbsTMSF = { 0};
    CD_sTMSF_type rRelTMSF = { 0};

    switch(pShMem->rCDAUDIO.u8Status)
    {
    default:
    case CDAUDIO_STATUS_PLAY:
    case CDAUDIO_STATUS_SCAN_FWD:
      u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA;
      break;
    case CDAUDIO_STATUS_SCAN_BWD:
      u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA;
      break;
    } //switch(pShMem->rCDAUDIO.u8Status)

    vZLBA2MSF(u32ZLBA, &rAbsTMSF);

    (void)u32InitializePlayInfo(pShMem, &rLocalInfo);
    if(CD_bGetTMSFromAbsZLBA(pShMem, u32ZLBA, &rRelTMSF))
    { // playtime inside a track
      rLocalInfo.rAbsTrackAdr.u8MSFMinute = rAbsTMSF.u8Min;
      rLocalInfo.rAbsTrackAdr.u8MSFSecond = rAbsTMSF.u8Sec;
      rLocalInfo.rAbsTrackAdr.u8MSFFrame = rAbsTMSF.u8Frm;
      rLocalInfo.u32TrackNumber = (tU32)rRelTMSF.u8Track;
      rLocalInfo.rRelTrackAdr.u8MSFMinute = rRelTMSF.u8Min;
      rLocalInfo.rRelTrackAdr.u8MSFSecond = rRelTMSF.u8Sec;
      rLocalInfo.rRelTrackAdr.u8MSFFrame = rRelTMSF.u8Frm;
    } //if (..)
    CD_PRINTF_U2("u32FakePlayPosTF ZLBA %6u, "
                 "AbsMSF %02u:%02u:%02u, "
                 "RelTMS %02u-%02u:%02u",
                 (unsigned int)u32ZLBA, (unsigned int)rAbsTMSF.u8Min,
                 (unsigned int)rAbsTMSF.u8Sec, (unsigned int)rAbsTMSF.u8Frm,
                 (unsigned int)rRelTMSF.u8Track, (unsigned int)rRelTMSF.u8Min,
                 (unsigned int)rRelTMSF.u8Sec);
  }
  CD_SM_UNLOCK(hSem);

  vNotifyPlayInfoClients(pShMem, &rLocalInfo, TRUE);

  CD_TRACE_LEAVE_U3(CDID_u32FakePlayPosTF, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     u32PlayPosTF
 * PARAMETER:    pointer to shared memory
 *               Handle of Semaphore
 *               Handle of open CD device
 * RETURNVALUE:  OSAL ERROR code
 * DESCRIPTION:  get playtime from drive,
 *               and notiies clients if changed. Called only by MainThread!
 * HISTORY:
 ******************************************************************************/
static tU32 u32PlayPosTF(tDevCDData *pShMem, OSAL_tSemHandle hSem, int hOpenCD,
                         tBool bFakeLowVoltage)
{
  static tCD_sSubchannel rSubPrevious = { 0, 0, 0, 0, 0, 0, CD_INVALID};
  static tU32 u32StatusPrevious = OSAL_C_S32_AUDIO_STATUS_NOT_VALID;
  tU32 u32Status;
  tU32 u32Ret = OSAL_E_NOERROR;
  tCD_sSubchannel rSub = { 0, 0, 0, 0, 0, 0, CD_INVALID};
  tU16 u16RangeID;
  tU32 u32ZLBA;
  OSAL_trPlayInfo rLocalInfo = { 0};
  tU8 u8PrevS = CDAUDIO_STATUS_ERROR;
  tBool bNotifyPlayInfoClients = FALSE;
  tBool bStop = FALSE;
  tBool bSendPlayTime = FALSE;
  tBool bNearLBound;
  tBool bNearUBound;
  tBool bBelowLBound;
  tBool bAboveUBound;
  tBool bSubChanged = FALSE;
  tBool bPlayPosChanged = FALSE;
  tBool bStatusChanged = FALSE;
  tBool bValidPlaytime = TRUE;
  tBool bSendEndOfPlayrangePlaytime = FALSE;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_u32PlayPosTF, hOpenCD, bFakeLowVoltage, 0, 0);

  if(bFakeLowVoltage)
  {
    tU8 u8S = CD_AUDIO_STATUS_PLAY_STOPPED_DUE_TO_LOWVOLTAGE;
    tU32 u32AbsZLBA;

    (void)CD_u32PostMainEvent(pShMem, CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER);
    u8PrevS = CD_u8SwitchState(pShMem, CDAUDIO_STATUS_UNDERVOLTAGE_ERROR);
    bNotifyPlayInfoClients = (u8PrevS != u8S);

    (void)u32InitializePlayInfo(pShMem, &rLocalInfo);
    
    bSendEndOfPlayrangePlaytime = TRUE;
    u32AbsZLBA = CD_MSF2ZLBA(rLocalInfo.rAbsTrackAdr.u8MSFMinute,
                             rLocalInfo.rAbsTrackAdr.u8MSFSecond,
                             rLocalInfo.rAbsTrackAdr.u8MSFFrame);
    bValidPlaytime = u32AbsZLBA != 0; 
    CD_PRINTF_U2("u32PlayPosTF SIMULATE LowPower,"
                 " bNotifyPlayInfoClients %d,"
                 " u32AbsZLBA %u,"
                 " bValidPlaytime %d",
                 (int)bNotifyPlayInfoClients,
                 (unsigned int)u32AbsZLBA,
                 (int)bValidPlaytime);
  }
  else //if(bFakeLowVoltage)
  {
    u32Ret = CD_SCSI_IF_u32ReadSubChannel(pShMem, &rSub, hOpenCD);
    if((OSAL_E_NOERROR == u32Ret) && (rSub.u8Valid == CD_VALID))
    {
      bValidPlaytime = rSub.u32AbsZLBA != 0; 
      CD_PRINTF_U1("%s: bValidPlaytime %u", __FUNCTION__,
                                          (unsigned int)bValidPlaytime);

      bSubChanged = memcmp(&rSubPrevious, &rSub, sizeof(tCD_sSubchannel)) == 0
                    ? FALSE : TRUE;
      bPlayPosChanged = rSubPrevious.u32AbsZLBA != rSub.u32AbsZLBA;
      rSubPrevious = rSub;
      CD_PRINTF_U2("u32PlayPosTF SubChannel: T %02u, I %u,"
                   " RZLBA %06u,"
                   " AZLBA %06u,"
                   " AdrCtrl 0x%02X,"
                   " AudStat 0x%02X,"
                   " u8Valid %u,"
                   " bSubChanged %d,"
                   " bPlayPosChg %d",
                   (unsigned int)rSub.u8Track, (unsigned int)rSub.u8Index,
                   (unsigned int)rSub.u32RelZLBA,
                   (unsigned int)rSub.u32AbsZLBA,
                   (unsigned int)rSub.u8AdrCtrl,
                   (unsigned int)rSub.u8AudioStatus,
                   (unsigned int)rSub.u8Valid, (int)bSubChanged,
                   (int)bPlayPosChanged);
      CD_SM_LOCK(hSem);

      //check audio status
      {
        tU8 u8S;
        switch(rSub.u8AudioStatus)
        {
        case CD_AUDIO_STATUS_PLAY_COMPLETED:
          u8S = CDAUDIO_STATUS_STOP;
          u8PrevS = CD_u8SwitchState(pShMem, u8S);
          //notify only if status changed (not stopped before)
          bNotifyPlayInfoClients = (u8PrevS != u8S);
          bSendPlayTime = TRUE;
          bSendEndOfPlayrangePlaytime = TRUE;
          (void)CD_u32PostMainEvent(pShMem,
                                    CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER);
          break;
        case CD_AUDIO_STATUS_PLAY_PAUSED:
          u8PrevS = pShMem->rCDAUDIO.u8Status;
          switch(u8PrevS)
          {
          case CDAUDIO_STATUS_STOP:
          case CDAUDIO_STATUS_PAUSED:
            bNotifyPlayInfoClients = FALSE;
            bSendPlayTime = FALSE;
            break;
          case CDAUDIO_STATUS_PLAY:
          case CDAUDIO_STATUS_SCAN_FWD:
          case CDAUDIO_STATUS_SCAN_BWD:
          case CDAUDIO_STATUS_PAUSED_BY_USER:
          case CDAUDIO_STATUS_ERROR:
          default:
            u8S = CDAUDIO_STATUS_PAUSED;
            u8PrevS = CD_u8SwitchState(pShMem, u8S);
            bNotifyPlayInfoClients = (u8PrevS != u8S);
            bSendPlayTime = TRUE;
          } //switch(pShMem->rCDAUDIO.u8Status)
          (void)CD_u32PostMainEvent(pShMem,
                                    CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER);
          break;
        case CD_AUDIO_STATUS_PLAY_STOPPED_DUE_TO_ERROR:
          u8S = CDAUDIO_STATUS_ERROR;
          u8PrevS = CD_u8SwitchState(pShMem, u8S);
          bNotifyPlayInfoClients = (u8PrevS != u8S);
          (void)CD_u32PostMainEvent(pShMem,
                                    CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER);
          break;
        case CD_AUDIO_STATUS_PLAY_IN_PROGRESS:
        case CD_AUDIO_STATUS_NOT_VALID:
        case CD_AUDIO_STATUS_NO_STATUS:
        default:
          ;
        } //switch(rSub.u8AudioStatus)
      }

      u32Status = CD_u32GetPlayStatus(pShMem);
      bStatusChanged = u32Status == u32StatusPrevious ? FALSE : TRUE;
      u32StatusPrevious = u32Status;

      if(bPlayPosChanged || bNotifyPlayInfoClients)
      {
        if(bSendEndOfPlayrangePlaytime)
        { // fake playtime to end of playrange
          switch(u8PrevS)
          {
          case CDAUDIO_STATUS_PLAY:
          case CDAUDIO_STATUS_SCAN_FWD:
            u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA;
            break;
          case CDAUDIO_STATUS_SCAN_BWD:
            u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA;
            break;
          default:
            u32ZLBA = rSub.u32AbsZLBA;
          } //switch(pShMem->rCDAUDIO.u8Status)
        }
        else //if(bSendEndOfPlayrangePlaytime)
        {
          u32ZLBA = rSub.u32AbsZLBA;
        } //else //if(bSendEndOfPlayrangePlaytime)

        u16RangeID   = u16WhereIsPlayPosInPlayRange(pShMem, u32ZLBA);
        bNearLBound  = CD_U16_PLAYRANGE_NEAR_LBOUND
                       == (u16RangeID & CD_U16_PLAYRANGE_ADDITIONALINFO_MASK);
        bNearUBound  = CD_U16_PLAYRANGE_NEAR_UBOUND
                       == (u16RangeID & CD_U16_PLAYRANGE_ADDITIONALINFO_MASK);
        bBelowLBound = CD_U16_PLAYRANGE_BELOW_LBOUND
                       == (u16RangeID & CD_U16_PLAYRANGE_ADDITIONALINFO_MASK);
        bAboveUBound = CD_U16_PLAYRANGE_ABOVE_UBOUND
                       == (u16RangeID & CD_U16_PLAYRANGE_ADDITIONALINFO_MASK);

        switch(u16RangeID & CD_U16_PLAYRANGE_ID_MASK)
        {
        case CD_U16_PLAYRANGE_LBOUND:
          bSendPlayTime = TRUE;
          switch(pShMem->rCDAUDIO.u8Status)
          {
          case CDAUDIO_STATUS_SCAN_BWD:
            bStop = TRUE;
            break;
          default:
            bStop = FALSE;
          } //switch(pShMem->rCDAUDIO.u8Status)
          break;
        case CD_U16_PLAYRANGE_UBOUND:
          bSendPlayTime = TRUE;
          switch(pShMem->rCDAUDIO.u8Status)
          {
          case CDAUDIO_STATUS_PLAY:
          case CDAUDIO_STATUS_SCAN_FWD:
            bStop = TRUE;
            break;
          default:
            bStop = FALSE;
          } //switch(pShMem->rCDAUDIO.u8Status)
          break;
        case CD_U16_PLAYRANGE_IN_RANGE:
          bSendPlayTime = TRUE;
          switch(pShMem->rCDAUDIO.u8Status)
          { //stop in fast modes before limits reached
          case CDAUDIO_STATUS_SCAN_FWD:
            if(bNearUBound)
            {
              bStop = TRUE;
              //in forward scan mode fake recent playpos to EndOfPlayrange
              u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA;
            } //if(bNearUBound)

            break;
          case CDAUDIO_STATUS_SCAN_BWD:
            if(bNearLBound)
            {
              bStop = TRUE;
              //in backward scan mode fake recent playpos to StartPlayrange
              u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA;
            } //if(bNearLBound)
            break;
          default:
            bStop = FALSE;
            ;
          } //switch(pShMem->rCDAUDIO.u8Status)
          break;
        case CD_U16_PLAYRANGE_OUT_OF_RANGE:
          //playpos out of current range
          CD_PRINTF_U2("u32PlayPosTF OUT OF RANGE, ZLBA %u, "
                       "bBelowLBound %u, "
                       "bAboveUBound %u",
                       (unsigned int)u32ZLBA, (unsigned int)bBelowLBound,
                       (unsigned int)bAboveUBound);
          //fake recent playpos to Start/EndOfPlayrange if out of range
          // avoiding notifying/displaying of playtimes outside playrange
          switch(pShMem->rCDAUDIO.u8Status)
          {
          case CDAUDIO_STATUS_PLAY:
          case CDAUDIO_STATUS_SCAN_FWD:
            if(bBelowLBound)
            {
              u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA;
              bSendPlayTime = TRUE;
              bStop = FALSE;
            }
            else //if(bBelowLBound)
            {
              u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA;
              bSendPlayTime = TRUE;
              bStop = TRUE;
            } //else //if(bBelowLBound)
            break;
          case CDAUDIO_STATUS_SCAN_BWD:
            if(bAboveUBound)
            {
              u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA;
              bSendPlayTime = TRUE;
              bStop = FALSE;
            }
            else //if(bAboveUBound)
            {
              u32ZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA;
              bSendPlayTime = TRUE;
              bStop = TRUE;
            } //else //if(bAboveUBound)
            break;
          default:
            ;
          } //switch(pShMem->rCDAUDIO.u8Status)
          break;
        default:
          ;
        } //switch(u16RangeID)

        if(bStop)
        {
          tU32 u32RetS;
          u32RetS = CD_SCSI_IF_u32PauseResume(pShMem, FALSE);
          if(u32RetS == OSAL_E_NOERROR)
          {
            tU8 u8S, u8PrvS;
            u8S = CDAUDIO_STATUS_STOP;
            u8PrvS = CD_u8SwitchState(pShMem, u8S);
            //notify only if status changed (not stopped before)
            bNotifyPlayInfoClients = (u8PrvS != u8S);
          } //if(u32RetS == OSAL_E_NOERROR)
        } //if(bStop)

        (void)u32InitializePlayInfo(pShMem, &rLocalInfo);

        if(bSendPlayTime && bValidPlaytime)
        {
          CD_sTMSF_type rAbsTMSF = {0};
          CD_sTMSF_type rRelTMSF = {0};
          vZLBA2MSF(u32ZLBA, &rAbsTMSF);

          if(CD_bGetTMSFromAbsZLBA(pShMem, u32ZLBA, &rRelTMSF))
          { // playtime inside a track
            rLocalInfo.rAbsTrackAdr.u8MSFMinute = rAbsTMSF.u8Min;
            rLocalInfo.rAbsTrackAdr.u8MSFSecond = rAbsTMSF.u8Sec;
            rLocalInfo.rAbsTrackAdr.u8MSFFrame  = rAbsTMSF.u8Frm;
            rLocalInfo.u32TrackNumber           = (tU32)rRelTMSF.u8Track;
            rLocalInfo.rRelTrackAdr.u8MSFMinute = rRelTMSF.u8Min;
            rLocalInfo.rRelTrackAdr.u8MSFSecond = rRelTMSF.u8Sec;
            rLocalInfo.rRelTrackAdr.u8MSFFrame  = rRelTMSF.u8Frm;

            pShMem->rCDAUDIO.u32ZLBA        = u32ZLBA;
            pShMem->rCDAUDIO.rPlayInfo      = rLocalInfo;
            pShMem->rCDAUDIO.bPlayInfoValid = TRUE;

            bNotifyPlayInfoClients = TRUE;
          } //if (..)
          CD_PRINTF_U2("u32PlayPosTF ZLBA %6u, "
                       "AbsMSF %02u:%02u:%02u, "
                       "RelTMS %02u-%02u:%02u",
                       (unsigned int)u32ZLBA, (unsigned int)rAbsTMSF.u8Min,
                       (unsigned int)rAbsTMSF.u8Sec,
                       (unsigned int)rAbsTMSF.u8Frm,
                       (unsigned int)rRelTMSF.u8Track,
                       (unsigned int)rRelTMSF.u8Min,
                       (unsigned int)rRelTMSF.u8Sec);
        }
        else //if(bSendPlayTime && bValidPlaytime)
        {
          CD_PRINTF_U1("%s Playtime not sent"
                       "ZLBA %6u, "
                       "bSendPlayTime %u, "
                       "bValidPlaytime %u",
                       __FUNCTION__,
                       (unsigned int)u32ZLBA,
                       (unsigned int)bSendPlayTime,
                       (unsigned int)bValidPlaytime);
        } //else //if(bSendPlayTime && bValidPlaytime)
      } //if(bSubChanged)
      CD_SM_UNLOCK(hSem);
    }
    else //if(OSAL_E_NOERROR == u32Ret)
    {
      CD_PRINTF_ERRORS("Error read subchannel [%s] Valid [%u]",
                       (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()),
                       (unsigned int)rSub.u8Valid);
    } //else //if(OSAL_E_NOERROR == u32Ret)
  } //else //if(bFakeLowVoltage)

  if(bNotifyPlayInfoClients && bValidPlaytime)
  {
    vNotifyPlayInfoClients(pShMem, &rLocalInfo, bSendEndOfPlayrangePlaytime);
  } //if(bNotifyPlayInfoClients && bValidPlaytime)

  CD_TRACE_LEAVE_U3(CDID_u32PlayPosTF, u32Ret, bSubChanged, bStatusChanged,
                    bNotifyPlayInfoClients, bSendPlayTime);
  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CD_vMainThread
 * PARAMETER:    pvData == CD_trMainData*
 * RETURNVALUE:  None
 * DESCRIPTION:  This is the Main Thread.
 * HISTORY:
 * Date       | Modification                | Author
 * 25/11/2016 | Added fix for NCG3D-29329   | boc7kor
 ******************************************************************************/
static tVoid CD_vMainThread(tPVoid pvData)
{
  CD_trMainData *pMTD = (CD_trMainData*)pvData;
  tBool bThreadLoop = TRUE;
  tU32 u32Ret;
  OSAL_tEventMask tWaitEventMask = 0;
  OSAL_tShMemHandle hShMem = OSAL_ERROR;
  tDevCDData *pShMem = NULL; /*OSAL shared memory*/
  OSAL_tEventHandle hMainEvent = OSAL_C_INVALID_HANDLE;
  OSAL_tSemHandle hSem = OSAL_C_INVALID_HANDLE;
  int hSR = -1; // handle to cd drive

  pMTD->bMainThreadIsRunning = TRUE;

  //Initialization
  u32Ret = CD_u32GetShMem(&pShMem, &hShMem, &hSem);
  if(u32Ret != OSAL_E_NOERROR)
  {
    bThreadLoop = FALSE;
    CD_PRINTF_ERRORS("CD_vMainThread ERROR: No Shared Memory or Semaphore");
  } //if(NULL == pShMem)

  if(OSAL_OK != OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent))
  {
    bThreadLoop = FALSE;
    CD_PRINTF_ERRORS("MAINThread  ERROR [%s], OSAL_s32EventOpen(%s)",
                     (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()),
                     CD_MAIN_EVENT_NAME);
  } //if(OSAL_OK != OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent))

  //WORKER Loop
  while(bThreadLoop && pShMem)
  {
    CD_PRINTF_U4("CD_vMainThread is waiting for event ...");

    tWaitEventMask = 0;
    (void)OSAL_s32EventWait(hMainEvent, CD_MAIN_EVENT_MASK_ANY,
                            OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER,
                            &tWaitEventMask);

    (void)OSAL_s32EventPost(hMainEvent, ~tWaitEventMask, OSAL_EN_EVENTMASK_AND);

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_END))
    { //END THREAD EVENT
      //bThreadLoop = FALSE;
      CD_PRINTF_U4("CD_vMainThread END EVENT");
      break;
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_END))

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_DRIVE_SLEEP))
    {
      CD_PRINTF_U2("CD_MAIN_EVENT_MASK_DRIVE_SLEEP");
      (void)CD_u32PostMainEvent(pShMem,
                           CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_HI
                           |
                           CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER);

      // hSR is -1, IOCtl will open /dev/srX by itself
      (void)u32PlayPosTF(pShMem, hSem, hSR, TRUE); /*fake low volt noti*/
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_MEDIA_STATE,
                        OSAL_C_U16_MEDIA_NOT_READY);
      
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_DEVICE_READY,
                        OSAL_C_U16_DEVICE_NOT_READY);

      (void)CD_SCSI_IF_u32SetSleepMode(pShMem, TRUE);
      continue; /*ignore further events*/
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_DRIVE_SLEEP))

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_DRIVE_ACTIVE))
    {
      CD_PRINTF_U2("CD_MAIN_EVENT_MASK_DRIVE_ACTIVE");
      (void)CD_SCSI_IF_u32SetSleepMode(pShMem, FALSE);
      (void)CD_u32PostMainEvent(pShMem,
                                CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW);
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_DEVICE_READY,
                        OSAL_C_U16_DEVICE_READY);
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_DRIVE_ACTIVE))

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_SUBCHANNEL_FAKE))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_SUBCHANNEL_FAKE");
      u32Ret = u32FakePlayPosTF(pShMem, hSem);
    } //CD_MAIN_EVENT_MASK_SUBCHANNEL_FAKE

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_SUBCHANNEL_TIMER))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_SUBCHANNEL_TIMER");
      if(hSR == -1)
      { // open /dev/srX, avoid cyclically open/close during poll of Subchannel
        hSR = open(pShMem->rCD.rCDDrive.szDevSRName, O_RDWR | O_NONBLOCK);
      } //if(hSR == -1)
      u32Ret = u32PlayPosTF(pShMem, hSem, hSR, FALSE);
    } //CD_MAIN_EVENT_MASK_SUBCHANNEL_TIMER

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_LOW))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_LOW");
      OSAL_s32TimerSetTime(grMainData.hSubchannelTimer, 200,
                           CD_SUBCHANNEL_TIMER_LOW_MS);
    } //CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_LOW

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_HI))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_HI");
      OSAL_s32TimerSetTime(grMainData.hSubchannelTimer, 100,
                           CD_SUBCHANNEL_TIMER_HI_MS);
    } //CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_HI

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER");
      OSAL_s32TimerSetTime(grMainData.hSubchannelTimer, 0, 0);
      if(-1 != hSR)
      { //close /dev/srX handle 
        (void)close(hSR);
        hSR = -1;
      } //if(-1 != hSR)
    } //CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_WATCHDOG_TIMER))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_WATCHDOG_TIMER");
      if(pShMem->rCD.rCDDrive.bSRCouldBeOpened
         && pShMem->rCD.rCDDrive.bSGCouldBeOpened)
      {
         (void)OSAL_s32TimerSetTime(grMainData.hWatchdogTimer, 0, 0);  
      }
      else
      {
        CD_vSG_Check_Notify(pShMem);
      }
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_WATCHDOG_TIMER))

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_LOADERSTATE_TIMER))
    {
      static tU8 u8InternalLoaderStatusBackup =
                                           CD_INTERNAL_LOADERSTATE_INITIALIZED;
      tU8 u8InternalLoaderStatus;
      tU32 u32Ret1;

      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_LOADERSTATE_TIMER");
      u32Ret1 = CD_SCSI_IF_u32GetLoaderStatus(pShMem, &u8InternalLoaderStatus);
      if(OSAL_E_NOERROR == u32Ret1)
      {
        if(u8InternalLoaderStatus != u8InternalLoaderStatusBackup)
        {
          u8InternalLoaderStatusBackup = u8InternalLoaderStatus;
          switch(u8InternalLoaderStatus)
          {
          case CD_INTERNAL_LOADERSTATE_IN_SLOT:
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_DEFECT,
                              CD_MAIN_U32_NOT_INITIALIZED);
            (void)CD_u32PostMainEvent(pShMem,
                                 CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_HI);
            break; //case CD_INTERNAL_LOADERSTATE_IN_SLOT:

          case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_DEFECT,
                              CD_MAIN_U32_NOT_INITIALIZED);
            (void)CD_u32PostMainEvent(pShMem,
                                     CD_MAIN_EVENT_MASK_STOP_LOADERSTATE_TIMER
                                     |
                                     CD_MAIN_EVENT_MASK_UDEV_MEDIA_CHANGE
                                     );
            break; //case CD_INTERNAL_LOADERSTATE_EJECTED:

          case CD_INTERNAL_LOADERSTATE_EJECTED:
            CD_vCacheClear(pShMem, hSem, TRUE);
            CDAUDIO_vCDTEXTClear(pShMem);
            /*u32SetDriveStatus(pShMem,
             OSAL_C_U16_NOTI_MEDIA_STATE,
             OSAL_C_U16_MEDIA_NOT_READY);*/
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_DEFECT,
                              CD_MAIN_U32_NOT_INITIALIZED);
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_MEDIA_CHANGE,
                              OSAL_C_U16_MEDIA_EJECTED);
            (void)CD_u32PostMainEvent(pShMem,
                                CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW);
            break; //case CD_INTERNAL_LOADERSTATE_EJECTED:

          case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
            CD_vCacheClear(pShMem, hSem, TRUE);
            CDAUDIO_vCDTEXTClear(pShMem);
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_DEFECT,
                              CD_MAIN_U32_NOT_INITIALIZED);
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_MEDIA_CHANGE,
                              OSAL_C_U16_INCORRECT_MEDIA);
            (void)CD_u32PostMainEvent(pShMem,
                                     CD_MAIN_EVENT_MASK_STOP_LOADERSTATE_TIMER);
            break; //case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:

          case CD_INTERNAL_LOADERSTATE_EJECT_ERROR:
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_DEFECT,
                              OSAL_C_U16_DEFECT_LOAD_EJECT);
            (void)CD_u32PostMainEvent(pShMem,
                                CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW);
            break;

          case CD_INTERNAL_LOADERSTATE_LOAD_ERROR:
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_DEFECT,
                              OSAL_C_U16_DEFECT_LOAD_INSERT);
            (void)CD_u32PostMainEvent(pShMem,
                                CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW);
            break;

          case CD_INTERNAL_LOADERSTATE_NO_CD:
          case CD_INTERNAL_LOADERSTATE_UNKNOWN:
          case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
          case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
          case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
          default:
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_DEFECT,
                              CD_MAIN_U32_NOT_INITIALIZED);
            (void)CD_u32PostMainEvent(pShMem,
                                CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW);
          } //switch(u8InternalLoaderStatus)
        } //if(u8InternalLoaderStatus != u8InternalLoaderStatusBackup)
      }
      else //if(OSAL_E_NOERROR == u32Ret1)
      {
        CD_vCacheClear(pShMem, hSem, TRUE);
        CDAUDIO_vCDTEXTClear(pShMem);
        (void)CD_u32PostMainEvent(pShMem,
                                CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW);
      } //else //if(OSAL_E_NOERROR == u32Ret1)
    } //CD_MAIN_EVENT_MASK_LOADERSTATE_TIMER

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_START_LOADERSTATE_LOW");
      OSAL_s32TimerSetTime(grMainData.hLoaderStateTimer,
                           CD_LOADERSTATE_TIMER_LOW_MS,
                           CD_LOADERSTATE_TIMER_LOW_MS);
    } //CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_HI))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_HI");
      OSAL_s32TimerSetTime(grMainData.hLoaderStateTimer,
                           CD_LOADERSTATE_TIMER_HI_MS,
                           CD_LOADERSTATE_TIMER_HI_MS);
    } //CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_HI

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_STOP_LOADERSTATE_TIMER))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_STOP_LOADERSTATE_TIMER");
      OSAL_s32TimerSetTime(grMainData.hLoaderStateTimer, 0, 0);
    } //CD_MAIN_EVENT_MASK_STOP_LOADERSTATE_TIMER

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_UDEV_EJECT))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_UDEV_EJECT");
      CD_vCacheClear(pShMem, hSem, TRUE);
      CDAUDIO_vCDTEXTClear(pShMem);
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_MEDIA_STATE,
                        OSAL_C_U16_MEDIA_NOT_READY);
      (void)CD_u32PostMainEvent(pShMem,
                                CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER |
                                CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW);
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_UDEV_EJECT))

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_UDEV_MEDIA_CHANGE))
    { //media change notificationm from udev
      tU8 u8InternalLoaderStatus;
      tU32 u32Ret1;
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_UDEV_MEDIA_CHANGE");

      u32Ret1 = CD_SCSI_IF_u32GetLoaderStatus(pShMem, &u8InternalLoaderStatus);
      if(OSAL_E_NOERROR == u32Ret1)
      {
        switch(u8InternalLoaderStatus)
        {
        case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
          {
            tU8 u8MediaType = CD_u8GetMediaType(pShMem);
            switch(u8MediaType)
            {
            case OSAL_C_U8_DATA_MEDIA :
              u32SetDriveStatus(pShMem,
                                OSAL_C_U16_NOTI_MEDIA_CHANGE,
                                OSAL_C_U16_DATA_MEDIA);
              u32SetDriveStatus(pShMem,
                                OSAL_C_U16_NOTI_MEDIA_STATE,
                                OSAL_C_U16_MEDIA_READY);
              break;
            case OSAL_C_U8_AUDIO_MEDIA :
              u32SetDriveStatus(pShMem,
                                OSAL_C_U16_NOTI_MEDIA_CHANGE,
                                OSAL_C_U16_AUDIO_MEDIA);
              u32SetDriveStatus(pShMem,
                                OSAL_C_U16_NOTI_MEDIA_STATE,
                                OSAL_C_U16_MEDIA_READY);
              break;
            case OSAL_C_U8_INCORRECT_MEDIA :
            case OSAL_C_U8_UNKNOWN_MEDIA :
            default:
              ;
            } //switch(u8MediaType)
          }
          break; //case CD_INTERNAL_LOADERSTATE_IN_PLAY_POSITION:

        case CD_INTERNAL_LOADERSTATE_NO_CD:
        case CD_INTERNAL_LOADERSTATE_EJECTED:
        case CD_INTERNAL_LOADERSTATE_IN_SLOT:
        case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
        case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
        case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
        case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
        case CD_INTERNAL_LOADERSTATE_UNKNOWN:
        default:
          ;
        } //switch(u8InternalLoaderStatus)
      } //if(OSAL_E_NOERROR == u32Ret1)
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_UDEV_MOUNT))

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_MIXED_NOT_READY))
    {
      CD_PRINTF_U2("CD_MAIN_EVENT_MASK_MIXED_NOT_READY");
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_MEDIA_STATE,
                        OSAL_C_U16_MEDIA_NOT_READY);
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_MIXED_NOT_READY))

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_MIXED_READY_DATA))
    {
      CD_PRINTF_U2("CD_MAIN_EVENT_MASK_MIXED_READY_DATA");
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_MEDIA_CHANGE,
                        OSAL_C_U16_DATA_MEDIA);
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_MEDIA_STATE,
                        OSAL_C_U16_MEDIA_READY);
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_MIXED_READY_DATA))

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_MIXED_READY_AUDIO))
    {
      CD_PRINTF_U2("CD_MAIN_EVENT_MASK_MIXED_READY_DATA");
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_MEDIA_CHANGE,
                        OSAL_C_U16_AUDIO_MEDIA);
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_MEDIA_STATE,
                        OSAL_C_U16_MEDIA_READY);
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_MIXED_READY_DATA))

    if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_WAKEUP_CHANGED))
    {
      CD_PRINTF_U3("CD_MAIN_EVENT_MASK_WAKEUP_CHANGED");
    } //if(0 != (tWaitEventMask & CD_MAIN_EVENT_MASK_WAKEUP_CHANGED))

    (void)u32Ret;
  } //while(bThreadLoop);

  //CLEAN UP
  if(-1 != hSR)
    (void)close(hSR);

  if(OSAL_C_INVALID_HANDLE != hMainEvent)
    (void)OSAL_s32EventClose(hMainEvent);

  CD_vReleaseShMem(pShMem, hShMem, hSem);

  CD_PRINTF_U4("CD_vMainThread EXIT");
  pMTD->bMainThreadIsRunning = FALSE;
}

/*****************************************************************************
 * FUNCTION:     CD_vUdevThread
 * PARAMETER:    pvData == CD_trMainData*
 * RETURNVALUE:  None
 * DESCRIPTION:  This is the Udev poll Thread.
 * HISTORY:
 ******************************************************************************/
static tVoid CD_vUdevThread(tPVoid pvData)
{
  CD_trMainData *pMTD = (CD_trMainData*)pvData;
  tBool bThreadLoop = TRUE;
  tU32 u32Ret;
  OSAL_tShMemHandle hShMem;
  tDevCDData *pShMem = NULL; /*OSAL shared memory*/
  OSAL_tEventHandle hMainEvent = OSAL_C_INVALID_HANDLE;
  OSAL_tSemHandle hSem = OSAL_C_INVALID_HANDLE;

  pMTD->bUdevThreadIsRunning = TRUE;

  //Initialization
  u32Ret = CD_u32GetShMem(&pShMem, &hShMem, &hSem);
  if(u32Ret != OSAL_E_NOERROR)
  {
    bThreadLoop = FALSE;
    CD_PRINTF_ERRORS("CD_vUdevThread ERROR: No Shared Memory");
  } //if(NULL == pShMem)

  if(OSAL_OK != OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent))
  {
    bThreadLoop = FALSE;
    CD_PRINTF_ERRORS("UdevThread  ERROR [%s], OSAL_s32EventOpen(%s)",
                     (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()),
                     CD_MAIN_EVENT_NAME);
  } //if(OSAL_OK != OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent))

  //UDEV mmonitor
  if(NULL != pShMem)
  {
    struct udev *udev;

    udev = udev_new();
    if(!udev)
    {
      CD_PRINTF_ERRORS("CD_vUdevThread no UDEV device");
    }
    else //if(!udev)
    {
      struct udev_monitor *monUdev;
      struct udev_device *devUdev;
      struct pollfd fdItems[2];

      //140723 monUdev = udev_monitor_new_from_netlink(udev, "kernel");
      monUdev = udev_monitor_new_from_netlink(udev, "udev");
      udev_monitor_filter_add_match_subsystem_devtype(monUdev, "block", "disk");
      udev_monitor_enable_receiving(monUdev);
      fdItems[0].events = POLLIN;
      fdItems[0].fd = udev_monitor_get_fd(monUdev);
      fdItems[1].events = POLLPRI;
      fdItems[1].fd = -1;

      //WORKER Loop
      while(bThreadLoop && grMainData.bUdevThreadDoLoop)
      {
        int iRet;
        CD_PRINTF_U4("CD_vUdevThread is waiting ...");

        fdItems[0].revents = 0;
        iRet = poll(fdItems, 2, 1201);
        CD_PRINTF_U4("CD_vUdevThread poll iRet %d, fd[0]=%d fd[1]=%d "
                     "revent[0]=0x%08X revent[1]=0x%08X",
                     iRet, fdItems[0].fd, fdItems[1].fd, fdItems[0].revents,
                     fdItems[1].revents);
        if(iRet > 0)
        {
          //work on UDEV
          if(fdItems[0].revents == POLLIN)
          {
            devUdev = udev_monitor_receive_device(monUdev);
            if(devUdev && pShMem)
            {
              const char * pcszDNN;
              pcszDNN = udev_device_get_devnode(devUdev);
              if((strcmp(pcszDNN, pShMem->rCD.rCDDrive.szDevSRName) == 0)
                 || (strcmp(pcszDNN, pShMem->rCD.rCDDrive.szDevSGName) == 0))
              {
                struct udev_list_entry * pudle;
                struct udev_list_entry * pudleName;
                pudle = udev_device_get_properties_list_entry(devUdev);
                if(pudle != NULL)
                {
                  pudleName = udev_list_entry_get_by_name(pudle,
                                                          "DISK_EJECT_REQUEST");
                  if(NULL != pudleName)
                    if(udev_list_entry_get_value(pudleName)[0] == '1')
                      (void)OSAL_s32EventPost(hMainEvent,
                                              CD_MAIN_EVENT_MASK_UDEV_EJECT,
                                              OSAL_EN_EVENTMASK_OR);

                  pudleName = udev_list_entry_get_by_name(pudle,
                                                          "DISK_MEDIA_CHANGE");
                  if(NULL != pudleName)
                    if(udev_list_entry_get_value(pudleName)[0] == '1')
                    {
                      pudleName = udev_list_entry_get_by_name(pudle,
                                                  "ID_CDROM_MEDIA_TRACK_COUNT");
                      if(NULL != pudleName)
                        (void)OSAL_s32EventPost(hMainEvent,
                                           CD_MAIN_EVENT_MASK_UDEV_MEDIA_CHANGE,
                                           OSAL_EN_EVENTMASK_OR);
                    } //if(udev_list_entry_get_value(pudleName)[0] == '1')
                }
                else //if(pudle != NULL)
                {
                  CD_PRINTF_U4("CD_vUdevThread No property list");
                } //else //if(pudle != NULL)
              } //if sr0
              udev_device_unref(devUdev);
            } //if(devUdev)
          }
          else //if(fdItems[0].revents == POLLIN)
          {
            CD_PRINTF_U4("CD_vUdevThread no POLLIN on UDEV event");
          } //else //if(fdItems[0].revents == POLLIN)

          //work on GPIO
          if(0 != (fdItems[1].revents & POLLPRI))
          {
            tU8 u8V;
            (void)lseek(fdItems[1].fd, SEEK_SET, 0);
            (void)read(fdItems[1].fd, &u8V, 1);
            //if(0 == (fdItems[1].revents & POLLERR))
            {
              (void)OSAL_s32EventPost(hMainEvent,
                                      CD_MAIN_EVENT_MASK_WAKEUP_CHANGED,
                                      OSAL_EN_EVENTMASK_OR);
            }
          } //if(fdItems[1].revents == POLLPRI)
        }
      } //while(bThreadLoop);

      //close FDs
      if(fdItems[0].fd != -1)
        (void)close(fdItems[0].fd);

      if(fdItems[1].fd != -1)
        (void)close(fdItems[1].fd);

    } //else //if(!udev)
  } //if(NULL != pShMem) UDEV monitor end

  //CLEAN UP
  if(OSAL_C_INVALID_HANDLE != hMainEvent)
  {
    (void)OSAL_s32EventClose(hMainEvent);
  } //if(OSAL_C_INVALID_HANDLE != hMainEvent)

  CD_vReleaseShMem(pShMem, hShMem, hSem);

  CD_PRINTF_U4("CD_vUdevThread EXIT");
  pMTD->bUdevThreadIsRunning = FALSE;
}

/*****************************************************************************
 * FUNCTION:     CD_vVoltThread
 * PARAMETER:    pvData == CD_trMainData*
 * RETURNVALUE:  None
 * DESCRIPTION:  This is the Voltage Thread.
 * HISTORY:
 *        | Date      | Modification               | Author
 *        | 28/09/2016| Change in handling for low | boc7kor
 *        |           | voltage condition as a fix | 
 *        |           | for NCG3D-23434            | 
 *        | 25/10/2016| Modified to use macro for  | boc7kor
 *        |           | low voltage handling       | 
 ******************************************************************************/
static tVoid CD_vVoltThread(tPVoid pvData)
{
  CD_trMainData *pMTD = (CD_trMainData*)pvData;
  tBool bThreadLoop = TRUE;
  tU32 u32Ret;
  tS32 s32Ret;
  OSAL_tEventMask tWaitEventMask = 0;
  OSAL_tShMemHandle hShMem = OSAL_ERROR;
  tDevCDData *pShMem = NULL; /*OSAL shared memory*/
  OSAL_tEventHandle hVoltEvent = OSAL_C_INVALID_HANDLE;
  OSAL_tEventHandle hMainEvent = OSAL_C_INVALID_HANDLE;
  OSAL_tSemHandle hSem = OSAL_C_INVALID_HANDLE;
  DEV_VOLT_trSystemVoltageRegistration rSVR;

  pMTD->bVoltThreadIsRunning = TRUE;

  //Initialization
  u32Ret = CD_u32GetShMem(&pShMem, &hShMem, &hSem);
  if(u32Ret != OSAL_E_NOERROR)
  {
    bThreadLoop = FALSE;
    CD_PRINTF_ERRORS("VoltThread ERROR: No Shared Memory or Semaphore");
  } //if(NULL == pShMem)

  s32Ret = OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent);
  if(OSAL_OK != s32Ret)
  {
    bThreadLoop = FALSE;
    CD_PRINTF_ERRORS("VoltThread  ERROR [%s], OSAL_s32EventOpen(%s)",
                     (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()),
                     CD_MAIN_EVENT_NAME);
  } //if(OSAL_OK != OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent))

  s32Ret = OSAL_s32EventOpen(pMTD->sVolt.szNotificationEventName, &hVoltEvent);
  if(OSAL_OK != s32Ret)
  {
    bThreadLoop = FALSE;
    CD_PRINTF_ERRORS("VoltThread  ERROR [%s], OSAL_s32EventOpen(%s)",
                     (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()),
                     pMTD->sVolt.szNotificationEventName);
  }
  else //if(OSAL_OK != s32Ret)
  {
    rSVR.u32ClientId = pMTD->sVolt.u32ClientId;
    //osioctrl.h: DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE
    rSVR.u32VoltageIndicationMask = CD_VOLT_I(CRITICAL_LOW_VOLTAGE) |
                                    CD_VOLT_I(HIGH_VOLTAGE) |
                                    CD_VOLT_I(CRITICAL_HIGH_VOLTAGE)|
                                    CD_VOLT_I(LOW_VOLTAGE);

    s32Ret = OSAL_s32IOControl(pMTD->hVolt,
                               CD_VIO(REGISTER_SYSTEM_VOLTAGE_NOTIFICATION),
                               (tS32)&rSVR);
    if(OSAL_OK != s32Ret)
    {
      bThreadLoop = FALSE;
      CD_PRINTF_ERRORS("VoltThread  ERROR [%s], OSAL_s32IoControl()",
                       (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
    } //if(OSAL_OK != s32Ret)

    //initial loop
    (void)OSAL_s32EventPost(hVoltEvent,
                        DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
                        OSAL_EN_EVENTMASK_OR);
  } //else //if(OSAL_OK != s32Ret)

  //WORKER Loop
  while(bThreadLoop)
  {
    CD_PRINTF_U2("VoltThread is waiting for event ...");
	
    tWaitEventMask = 0;
    (void)OSAL_s32EventWait(hVoltEvent, CD_VOLT_EVENT_MASK_ANY,
                            OSAL_EN_EVENTMASK_OR,
                            OSAL_C_TIMEOUT_FOREVER,
                            &tWaitEventMask);

    (void)OSAL_s32EventPost(hVoltEvent, ~tWaitEventMask, OSAL_EN_EVENTMASK_AND);

    if(0 != (tWaitEventMask & CD_VOLT_EVENT_MASK_END))
    { //END THREAD EVENT
      //bThreadLoop = FALSE;
      CD_PRINTF_U1("VoltThread END EVENT");
      break;
    } //if(0 != (tWaitEventMask & CD_VOLT_EVENT_MASK_END))

    if(0 != (tWaitEventMask
             &
             DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY))
    { // voltage has changed
      DEV_VOLT_trSystemVoltageHistory rSVH;

      CD_PRINTF_U2("SYSTEM_VOLTAGE_CHANGED_NOTIFY");

      rSVH.u32ClientId = pMTD->sVolt.u32ClientId;
      s32Ret = OSAL_s32IOControl(pMTD->hVolt,
                                 CD_VIO(GET_SYSTEM_VOLTAGE_HISTORY),
                                 (tS32)&rSVH);
      if(s32Ret == OSAL_OK)
      {
        static tU32 u32LastEvent = (tU32)CD_MAIN_EVENT_MASK_DRIVE_ACTIVE;
        tU32 u32Event;
        tU32 u32SVS  = rSVH.rSystemVoltage.u32CurrentSystemVoltageState;
        tU32 u32CLVC = rSVH.rSystemVoltage.u32CriticalLowVoltageCounter;
        tU32 u32LVC  = rSVH.rSystemVoltage.u32LowVoltageCounter;
        tU32 u32HVC  = rSVH.rSystemVoltage.u32HighVoltageCounter;
        tU32 u32CHVC = rSVH.rSystemVoltage.u32CriticalHighVoltageCounter;
        tBool bLVOccured; //LowVolt Occured since last event
        tBool bHVOccured; //HigVolt Occured since last event

        if(CD_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITICAL == 
             grMainData.u32LowVoltageHandlingThreshold)
        {
          //handle only critical voltage, ignore low voltage
          bLVOccured = u32CLVC > 0;
        }
        else
        {
          bLVOccured = (u32LVC > 0) || (u32CLVC > 0); 
        }
        
        bHVOccured = (u32HVC > 0) || (u32CHVC > 0);
        
        CD_PRINTF_U2("CritLowVoltCnt %u, LowVoltCnt %u, "
                     "CritHiVoltCnt %u, HiVoltCnt %u, "
                     "CurrState %u, "
                     "LowVolt occured %u, HighVolt occured %u",
                     (unsigned int)u32CLVC, (unsigned int)u32LVC,
                     (unsigned int)u32CHVC, (unsigned int)u32HVC,
                     (unsigned int)u32SVS,
                     (unsigned int)bLVOccured,(unsigned int)bHVOccured);

        // step 1 of 2: work on history since last event
        if(bLVOccured)
        { // if a low voltage condition occured since last event,
          // set drive in Sleep-Mode, if not done before
          u32Event = (tU32)CD_MAIN_EVENT_MASK_DRIVE_SLEEP;
          CD_PRINTF_U2("VoltThread: LOW occured since last event, "
                       "LastEvt %u, RecentEvt %u",
                       (unsigned int)u32LastEvent,
                       (unsigned int)u32Event);
          if(u32LastEvent != u32Event)
          {
            (void)OSAL_s32EventPost(hMainEvent, u32Event, OSAL_EN_EVENTMASK_OR);
            u32LastEvent = u32Event;
          } //if(u32LastEvent != u32Event)
        } //if(bLVOccured)

        // step 2 of 2: work on recent voltage state
        switch(u32SVS)
        {
        case DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW:
        case DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_LOW:
            if((CD_LOW_VOLTAGE_HANDLING_THRESHOLD_CRITICAL == 
                 grMainData.u32LowVoltageHandlingThreshold) && 
                  (DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW == u32SVS ))
            {
                u32Event = (tU32)CD_MAIN_EVENT_MASK_DRIVE_ACTIVE;
                if(u32LastEvent != u32Event)
                {
                    (void)OSAL_s32EventPost(hMainEvent, u32Event, OSAL_EN_EVENTMASK_OR);
                    u32LastEvent = u32Event;
                } 
                CD_PRINTF_U2("CD_LOW_VOLTAGE_HANDLING_THRESHOLD = %u :Low Voltage Handling not required",
                          grMainData.u32LowVoltageHandlingThreshold);
                break;                          
            }
            else
            {
              u32Event = (tU32)CD_MAIN_EVENT_MASK_DRIVE_SLEEP;
              CD_PRINTF_U2("VoltThread: LOW or Critical Low, "
                       "LastEvt %u, RecentEvt %u",
                       (unsigned int)u32LastEvent,
                       (unsigned int)u32Event);
              if(u32LastEvent != u32Event)
              {
                (void)OSAL_s32EventPost(hMainEvent, u32Event, OSAL_EN_EVENTMASK_OR);
                u32LastEvent = u32Event;
              } //if(u32LastEvent != u32Event)
              break;    
            }

        case DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_OPERATING:
          //DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_OPERATING
          u32Event = (tU32)CD_MAIN_EVENT_MASK_DRIVE_ACTIVE;
          CD_PRINTF_U2("VoltThread: Operating, "
                       "LastEvt %u, RecentEvt %u",
                       (unsigned int)u32LastEvent,
                       (unsigned int)u32Event);
          if(u32LastEvent != u32Event)
          {
            (void)OSAL_s32EventPost(hMainEvent, u32Event, OSAL_EN_EVENTMASK_OR);
            u32LastEvent = u32Event;
          } //if(u32LastEvent != u32Event)
          break;

        case DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_HIGH:
        case DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_HIGH:
          CD_PRINTF_U2("VoltThread: uninteresting VOLTAGE_STATE: %u",
                       (unsigned int)u32SVS);
          break;

        default:
          CD_PRINTF_ERRORS("VoltThread  ERROR unknown VOLTAGE_STATE [%u]",
                           (unsigned int)u32SVS);
        } //switch(u32SV)
      }
      else //if(s32Ret == OSAL_OK)
      {
        CD_PRINTF_ERRORS("VoltThread  ERROR [%s], GET_SYSTEM_VOLTAGE_HISTORY",
                         (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
      } //else //if(s32Ret == OSAL_OK)
    } //if DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY
  } //while(bThreadLoop);

  //CLEAN UP
  (void)OSAL_s32IOControl(pMTD->hVolt,
                          CD_VIO(UNREGISTER_SYSTEM_VOLTAGE_NOTIFICATION),
                          (tS32)pMTD->sVolt.u32ClientId);

  if(OSAL_C_INVALID_HANDLE != hVoltEvent)
  {
    (void)OSAL_s32EventClose(hVoltEvent);
  } //if(OSAL_C_INVALID_HANDLE != hVoltEvent)

  if(OSAL_C_INVALID_HANDLE != hMainEvent)
  {
    (void)OSAL_s32EventClose(hMainEvent);
  } //if(OSAL_C_INVALID_HANDLE != hMainEvent)

  CD_PRINTF_U1("VoltThread EXIT");

  CD_vReleaseShMem(pShMem, hShMem, hSem);
  pMTD->bVoltThreadIsRunning = FALSE;
}

/*****************************************************************************
 * FUNCTION:     vTraceCommandCallbackHandler
 * PARAMETER:    buffer
 * RETURNVALUE:  None
 * DESCRIPTION:  This is a Trace callback handler.

 * HISTORY:
 ******************************************************************************/
static tVoid vTraceCommandCallbackHandler(const tU8* pcu8Buffer)
{
  if(pcu8Buffer != NULL)
  {
    tU8 u8Device = pcu8Buffer[1];
    switch(u8Device)
    {
    case DEV_CD_TRACE_COMMAND_CHANNEL_CDCTRL_ID:
      CDCTRL_vTraceCmdCallback(pcu8Buffer);
      break;
    case DEV_CD_TRACE_COMMAND_CHANNEL_CDAUDIO_ID:
      CDAUDIO_vTraceCmdCallback(pcu8Buffer);
      break;
    case DEV_CD_TRACE_COMMAND_CHANNEL_CD_ID:
      CD_vTraceCmdCallback(pcu8Buffer);
      break;
    default:
      ;
    } //switch(u8Device)
  } //if(pcu8Buffer != NULL)
}

/*****************************************************************************
 * FUNCTION:     CD_vRegTrace
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  register trace command channel
 * HISTORY:
 ******************************************************************************/
static tVoid CD_vRegTrace(tVoid)
{
  grMainData.rTraceIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE,
                                              OSAL_EN_READWRITE);

  if(grMainData.rTraceIODescriptor != OSAL_ERROR)
  {
    OSAL_trIOCtrlLaunchChannel rIO;

    rIO.pCallback = (OSAL_tpfCallback)vTraceCommandCallbackHandler;
    rIO.enTraceChannel = (TR_tenTraceChan)TR_TTFIS_CDCTRL;

    if(OSAL_s32IOControl(grMainData.rTraceIODescriptor,
                         OSAL_C_S32_IOCTRL_CALLBACK_REG,
                         (tS32)&rIO)
       == OSAL_ERROR)
    {
      OSAL_s32IOClose(grMainData.rTraceIODescriptor);
      grMainData.rTraceIODescriptor = OSAL_ERROR;
    }
  }
}

/*****************************************************************************
 * FUNCTION:     CD_vUnregTrace
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  unregister trace command channel
 * HISTORY:
 ******************************************************************************/
static tVoid CD_vUnregTrace(tVoid)
{
  if(grMainData.rTraceIODescriptor != OSAL_ERROR)
  {
    OSAL_trIOCtrlLaunchChannel rIO;

    rIO.pCallback = (OSAL_tpfCallback)vTraceCommandCallbackHandler;
    rIO.enTraceChannel = (TR_tenTraceChan)TR_TTFIS_CDCTRL;

    if(OSAL_s32IOControl(grMainData.rTraceIODescriptor,
                         OSAL_C_S32_IOCTRL_CALLBACK_UNREG,
                         (tS32)&rIO)
       == OSAL_ERROR)
    {
      CD_PRINTF_ERRORS("OSAL_C_S32_IOCTRL_CALLBACK_UNREG failed [%s]",
                       OSAL_coszErrorText(OSAL_u32ErrorCode()));

    }
    OSAL_s32IOClose(grMainData.rTraceIODescriptor);
    grMainData.rTraceIODescriptor = OSAL_ERROR;
  }
}

/*****************************************************************************
 * FUNCTION:     u32InitMain
 * PARAMETER:    Handle to shared memory
 * RETURNVALUE:  OSAL Error Code
 * DESCRIPTION:  starts main thread, creates events and timer used by thread
 * HISTORY:
 ******************************************************************************/
static tU32 u32InitMain(tDevCDData *pShMem)
{
  tU32 u32Ret = OSAL_E_NOERROR;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U2(CDID_u32InitMain, 0, 0, 0, 0);

  pShMem->CD_bTraceOn = LLD_bIsTraceActive(CD_TRACE_CLASS,
                                           (tS32)TR_LEVEL_USER_1);

  //init shared mem data
  {
    int i;
    memset(pShMem->arPlayInfoReg, 0, sizeof(pShMem->arPlayInfoReg));
    memset(pShMem->arMediaReg, 0, sizeof(pShMem->arMediaReg));
    for(i = 0; i < CD_MAX_MEDIA_NOTIFIERS; i++)
    {
      pShMem->arMediaReg[i].u16AppID = OSAL_C_U16_INVALID_APPID;
    } //for(i = 0; i < CD_MAX_MEDIA_NOTIFIERS; i++)
  }

  //init drive status
  {
    tBool bCDOK = OSAL_E_NOERROR == CD_SCSI_IF_u32IsCDAccessible(pShMem);
    pShMem->rDriveStatus.u32Defect = CD_MAIN_U32_NOT_INITIALIZED;
    pShMem->rDriveStatus.u32DeviceReady =
    bCDOK ? OSAL_C_U16_DEVICE_READY : OSAL_C_U16_DEVICE_NOT_READY;
    pShMem->rDriveStatus.u32MediaChange = OSAL_C_U16_MEDIA_EJECTED;
    pShMem->rDriveStatus.u32MediaState = OSAL_C_U16_MEDIA_NOT_READY;
    pShMem->rDriveStatus.u32TotalFailure =
    bCDOK ? OSAL_C_U16_DEVICE_OK : OSAL_C_U16_DEVICE_FAIL;
  }

  //create event
  {
    grMainData.hMainEvent = OSAL_C_INVALID_HANDLE;
    if(OSAL_OK
       != OSAL_s32EventCreate(CD_MAIN_EVENT_NAME, &grMainData.hMainEvent))
    { //todo: try to open existing event if create fails
      u32Ret = OSAL_u32ErrorCode();
      CD_PRINTF_ERRORS("MAIN  ERROR 0x%08X, OSAL_s32EventCreate(%s)",
                       (unsigned int)u32Ret, CD_MAIN_EVENT_NAME);
    }
    else //if (OSAL_OK != OSAL_s32EventCreate(CD_MAIN_EVENT_NAME, &hEvent))
    {
      // event neede by timer (void)OSAL_s32EventClose(grMainData.hMainEvent);
    } //else //if (OSAL_OK != OSAL_s32EventCreate(CD_MAIN_EVENT_NAME, &hEvent))
  }

  //read media status from drive (must be done before udev thread is running)
  {
    tU8 u8InternalLoaderStatus;
    tU32 u32Ret1;
    u32Ret1 = CD_SCSI_IF_u32GetLoaderStatus(pShMem, &u8InternalLoaderStatus);

    if(OSAL_E_NOERROR != u32Ret1)
    {
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_MEDIA_STATE,
                        OSAL_C_U16_MEDIA_NOT_READY);
      u32SetDriveStatus(pShMem,
                        OSAL_C_U16_NOTI_MEDIA_CHANGE,
                        OSAL_C_U16_UNKNOWN_MEDIA);
    }
    else //if(OSAL_E_NOERROR != u32Ret1)
    {
      switch(u8InternalLoaderStatus)
      {
      case CD_INTERNAL_LOADERSTATE_CD_PLAYABLE:
        {
          tU8 u8MediaType = CD_u8GetMediaType(pShMem);
          switch(u8MediaType)
          {
          case OSAL_C_U8_DATA_MEDIA :
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_MEDIA_CHANGE,
                              OSAL_C_U16_DATA_MEDIA);
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_MEDIA_STATE,
                              OSAL_C_U16_MEDIA_READY);
            break;
          case OSAL_C_U8_AUDIO_MEDIA :
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_MEDIA_CHANGE,
                              OSAL_C_U16_AUDIO_MEDIA);
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_MEDIA_STATE,
                              OSAL_C_U16_MEDIA_READY);
            break;
          case OSAL_C_U8_INCORRECT_MEDIA :
          case OSAL_C_U8_UNKNOWN_MEDIA :
          default:
            u32SetDriveStatus(pShMem,
                              OSAL_C_U16_NOTI_MEDIA_CHANGE,
                              OSAL_C_U16_UNKNOWN_MEDIA);
            break;
          } //switch(u8MediaType)
        }
        break; //case CD_INTERNAL_LOADERSTATE_IN_PLAY_POSITION:

      case CD_INTERNAL_LOADERSTATE_NO_CD:
      case CD_INTERNAL_LOADERSTATE_EJECTED:
        u32SetDriveStatus(pShMem,
                          OSAL_C_U16_NOTI_MEDIA_STATE,
                          OSAL_C_U16_MEDIA_NOT_READY);
        u32SetDriveStatus(pShMem,
                          OSAL_C_U16_NOTI_MEDIA_CHANGE,
                          OSAL_C_U16_MEDIA_EJECTED);
        break; //case CD_INTERNAL_LOADERSTATE_EJECTED:

      case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:
        u32SetDriveStatus(pShMem,
                          OSAL_C_U16_NOTI_MEDIA_STATE,
                          OSAL_C_U16_MEDIA_NOT_READY);
        u32SetDriveStatus(pShMem,
                          OSAL_C_U16_NOTI_MEDIA_CHANGE,
                          OSAL_C_U16_INCORRECT_MEDIA);
        break;

      case CD_INTERNAL_LOADERSTATE_IN_SLOT:
      case CD_INTERNAL_LOADERSTATE_CD_INSIDE:
      case CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS:
      case CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS:
      case CD_INTERNAL_LOADERSTATE_UNKNOWN:
      default:
        u32SetDriveStatus(pShMem,
                          OSAL_C_U16_NOTI_MEDIA_STATE,
                          OSAL_C_U16_MEDIA_NOT_READY);
      } //switch(u8InternalLoaderStatus)
    } //else //if(OSAL_E_NOERROR != u32Ret1)
  }

  //open dev_volt
  grMainData.hVolt = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
  if(grMainData.hVolt == OSAL_ERROR)
  {
    u32Ret = OSAL_u32ErrorCode();
    CD_PRINTF_ERRORS("Error Open [%s] [%s]",
                     (const char*)OSAL_C_STRING_DEVICE_VOLT,
                     (const char*)OSAL_coszErrorText(u32Ret));
  }
  else //if (grMainData.hVolt == OSAL_ERROR)
  {
    // get event name and client ID
    if(OSAL_OK != OSAL_s32IOControl(grMainData.hVolt,
                                    OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
                                    (tS32)&grMainData.sVolt))
    {
      u32Ret = OSAL_u32ErrorCode();
      CD_PRINTF_ERRORS("Error Register Volt-Client [%s]",
                       (const char*)OSAL_coszErrorText(u32Ret));
    } //if(OSAL_OK !=  IO
  } //else //if (grMainData.hVolt == OSAL_ERROR)

  //create MAIN thread
  if(OSAL_E_NOERROR == u32Ret)
  {
    OSAL_trThreadAttribute rThreadAttr;
    OSAL_tThreadID threadID;

    rThreadAttr.szName = "CDMAIN";
    rThreadAttr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    rThreadAttr.pfEntry = CD_vMainThread;
    rThreadAttr.pvArg = &grMainData;
    rThreadAttr.s32StackSize = 4096;

    grMainData.bMainThreadIsRunning = FALSE;
    threadID = OSAL_ThreadSpawn(&rThreadAttr);
    if(OSAL_ERROR == threadID)
    {
      u32Ret = OSAL_u32ErrorCode();
      CD_PRINTF_ERRORS("Error Create CD_MainThread [%s]",
                       (const char*)OSAL_coszErrorText(u32Ret));
    } //if (OSAL_ERROR == threadID)
  } //if(OSAL_E_NOERROR == u32Ret)

  //create UDEV thread
  if(OSAL_E_NOERROR == u32Ret)
  {
    OSAL_trThreadAttribute rThreadAttr;
    OSAL_tThreadID threadID;

    rThreadAttr.szName = "CDUDEV";
    rThreadAttr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    rThreadAttr.pfEntry = CD_vUdevThread;
    rThreadAttr.pvArg = &grMainData;
    rThreadAttr.s32StackSize = 4096;

    grMainData.bUdevThreadIsRunning = FALSE;
    grMainData.bUdevThreadDoLoop = TRUE;
    threadID = OSAL_ThreadSpawn(&rThreadAttr);
    if(OSAL_ERROR == threadID)
    {
      u32Ret = OSAL_u32ErrorCode();
      CD_PRINTF_ERRORS("Error Create CD_UdevThread [%s]",
                       (const char*)OSAL_coszErrorText(u32Ret));
    } //if (OSAL_ERROR == threadID)
  } //if(OSAL_E_NOERROR == u32Ret)

  //create VOLT thread
  if(OSAL_E_NOERROR == u32Ret)
  {
    OSAL_trThreadAttribute rThreadAttr;
    OSAL_tThreadID threadID;

    rThreadAttr.szName = "CDVOLT";
    rThreadAttr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
    rThreadAttr.pfEntry = CD_vVoltThread;
    rThreadAttr.pvArg = &grMainData;
    rThreadAttr.s32StackSize = 4096;

    grMainData.bVoltThreadIsRunning = FALSE;
    threadID = OSAL_ThreadSpawn(&rThreadAttr);
    if(OSAL_ERROR == threadID)
    {
      u32Ret = OSAL_u32ErrorCode();
      CD_PRINTF_ERRORS("Error Create CD_VoltThread [%s]",
                       (const char*)OSAL_coszErrorText(u32Ret));
    } //if (OSAL_ERROR == threadID)
  } //if(OSAL_E_NOERROR == u32Ret)

  //create Playtime timer
  grMainData.hSubchannelTimer = OSAL_C_INVALID_HANDLE;
  if(OSAL_s32TimerCreate(vSubchannelTimerCallback, &grMainData,
                         &grMainData.hSubchannelTimer)
     != OSAL_OK)
  {
    CD_PRINTF_ERRORS("Error create Subchannel timer");
    u32Ret = OSAL_u32ErrorCode();
  }

  //create LoaderState timer
  grMainData.hLoaderStateTimer = OSAL_C_INVALID_HANDLE;
  if(OSAL_s32TimerCreate(vLoaderStateTimerCallback, &grMainData,
                         &grMainData.hLoaderStateTimer)
     != OSAL_OK)
  {
    CD_PRINTF_ERRORS("Error create LoaderState timer");
    u32Ret = OSAL_u32ErrorCode();
  }
  //start loader state timer LOW
  (void)CD_u32PostMainEvent(pShMem,
                            CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW);

  //create Watchdog timer
  grMainData.hWatchdogTimer = OSAL_C_INVALID_HANDLE;
  if(OSAL_s32TimerCreate(vWatchdogTimerCallback, &grMainData,
                         &grMainData.hWatchdogTimer)
     != OSAL_OK)
  {
    CD_PRINTF_ERRORS("Error create Watchdog timer");
    u32Ret = OSAL_u32ErrorCode();
  }
  else
  { // start watchdog
    if(OSAL_s32TimerSetTime(grMainData.hWatchdogTimer,
                            CD_WATCHDOG_TIMER_MS,
                            CD_WATCHDOG_TIMER_MS)
       != OSAL_OK)
    {
      CD_PRINTF_ERRORS("Error starting Watchdog timer");
      u32Ret = OSAL_u32ErrorCode();
    }
  }

  CD_TRACE_LEAVE_U2(CDID_u32InitMain, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     u32MainDestroy
 * PARAMETER:    Handle to shared memory
 * RETURNVALUE:  OSAL Error Code
 * DESCRIPTION:  stops  main thread, deletes events
 * HISTORY:
 ******************************************************************************/
static tU32 u32MainDestroy(tDevCDData *pShMem)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  int iCount;
  OSAL_tEventHandle hVoltEvent = OSAL_C_INVALID_HANDLE;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U3(CDID_u32MainDestroy, 0, 0, 0, 0);

  //End UDEV Task
  grMainData.bUdevThreadDoLoop = FALSE;

  //delete Watchdog timer
  if(grMainData.hWatchdogTimer != OSAL_C_INVALID_HANDLE)
  {
    (void)OSAL_s32TimerDelete(grMainData.hWatchdogTimer);
  }

  //delete Subchannel timer
  if(grMainData.hSubchannelTimer != OSAL_C_INVALID_HANDLE)
  {
    (void)OSAL_s32TimerDelete(grMainData.hSubchannelTimer);
  }

  //delete Loader timer
  if(grMainData.hLoaderStateTimer != OSAL_C_INVALID_HANDLE)
  {
    (void)OSAL_s32TimerDelete(grMainData.hLoaderStateTimer);
  }

  //send END message to MAIN thread
  if(OSAL_C_INVALID_HANDLE == grMainData.hMainEvent)
  {
    u32Ret = OSAL_u32ErrorCode();
    CD_PRINTF_ERRORS("Destroy  ERROR 0x%08X, No Main Event handle (%s)",
                     (unsigned int)u32Ret, CD_MAIN_EVENT_NAME);
  }
  else //if(OSAL_C_INVALID_HANDLE == grMainData.hMainEvent)
  {
    (void)OSAL_s32EventPost(grMainData.hMainEvent,
                            CD_MAIN_EVENT_MASK_END,
                            OSAL_EN_EVENTMASK_OR);
  } //else //if(OSAL_C_INVALID_HANDLE == grMainData.hMainEvent)

  //Open VOlt Event and send END message to VOLT thread
  if(OSAL_OK
     == OSAL_s32EventOpen(grMainData.sVolt.szNotificationEventName,
                          &hVoltEvent))
  {
    (void)OSAL_s32EventPost(hVoltEvent,
                            CD_VOLT_EVENT_MASK_END,
                            OSAL_EN_EVENTMASK_OR);
  } //if(OSAL_OK == OSAL_s32EventOpen

  // wait until MAIN thread has ended
  iCount = 20;
  while(grMainData.bMainThreadIsRunning && (iCount-- > 0))
  {
    OSAL_s32ThreadWait(100);
  } //while(grMainData.bMainThreadIsRunning && (iCount-- > 0))

  // wait until VOLT thread has ended
  iCount = 20;
  while(grMainData.bVoltThreadIsRunning && (iCount-- > 0))
  {
    OSAL_s32ThreadWait(100);
  } //while(grMainData.bVOltThreadIsRunning && (iCount-- > 0))

  // wait until UDEV thread has ended
  iCount = 20;
  while(grMainData.bUdevThreadIsRunning && (iCount-- > 0))
  {
    OSAL_s32ThreadWait(100);
  } //while(grMainData.bUdevThreadIsRunning && (iCount-- > 0))

  //close and delete main event
  if(grMainData.hMainEvent != OSAL_C_INVALID_HANDLE)
  {
    (void)OSAL_s32EventClose(grMainData.hMainEvent);
  }
  (void)OSAL_s32EventDelete(CD_MAIN_EVENT_NAME);

  //close VOLT event
  if(hVoltEvent != OSAL_C_INVALID_HANDLE)
  {
    (void)OSAL_s32EventClose(hVoltEvent);
  }

  //unregister and close dev_volt
  if(grMainData.hVolt != OSAL_ERROR)
  {
    //unregister volt vlient
    (void)OSAL_s32IOControl(grMainData.hVolt,
                            OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
                            (tS32)grMainData.sVolt.u32ClientId);
    (void)OSAL_s32IOClose(grMainData.hVolt);
  } //if(grMainData.hVolt != OSAL_ERROR)

  CD_TRACE_LEAVE_U3(CDID_u32MainDestroy, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/************************************************************************
 * FUNCTION                                                              *
 *      CDID_u32SetDriveStatus                                           *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Set drive status and notifies registered clients                 *
 *                                                                       *
 *                                                                       *
 ************************************************************************/
static tU32 u32SetDriveStatus(tDevCDData *pShMem, tU16 u16Type, tU32 u32Status)

{
  tBool bDoNotify;
  tU32 u32Ret = OSAL_E_NOERROR;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_u32SetDriveStatus, u16Type, u32Status, 0, 0);

  switch(u16Type)
  {
  case OSAL_C_U16_NOTI_MEDIA_CHANGE :
    /* u16Status:
     OSAL_C_U16_MEDIA_EJECTED
     OSAL_C_U16_INCORRECT_MEDIA
     OSAL_C_U16_DATA_MEDIA
     OSAL_C_U16_AUDIO_MEDIA
     OSAL_C_U16_UNKNOWN_MEDIA*/
    //notify client, if new status is different from last notified status
    bDoNotify = pShMem->rDriveStatus.u32MediaChange != u32Status;
    pShMem->rDriveStatus.u32MediaChange = u32Status;
    break;

  case OSAL_C_U16_NOTI_TOTAL_FAILURE :
    /*u16Status =
     OSAL_C_U16_DEVICE_OK
     OSAL_C_U16_DEVICE_FAIL*/
    //notify client, if new status is different from last notified status
    bDoNotify = pShMem->rDriveStatus.u32TotalFailure != u32Status;
    pShMem->rDriveStatus.u32TotalFailure = u32Status;
    break;

  case OSAL_C_U16_NOTI_MEDIA_STATE :
    /* u16Status =
     OSAL_C_U16_MEDIA_NOT_READY
     OSAL_C_U16_MEDIA_READY*/
    bDoNotify = pShMem->rDriveStatus.u32MediaState != u32Status;
    pShMem->rDriveStatus.u32MediaState = u32Status;
    if(u32Status == OSAL_C_U16_MEDIA_NOT_READY)
    { // if media state is NOT READY, media change must be resetted
      pShMem->rDriveStatus.u32MediaChange = (tU32)OSAL_C_U16_UNKNOWN_MEDIA;
    } //if(bDoNotify)
    break;

  case OSAL_C_U16_NOTI_DEFECT :
    /*u16Status =
     OSAL_C_U16_DEFECT_LOAD_EJECT
     OSAL_C_U16_DEFECT_LOAD_INSERT
     OSAL_C_U16_DEFECT_DISCTOC
     OSAL_C_U16_DEFECT_DISC
     OSAL_C_U16_DEFECT_READ_ERR
     OSAL_C_U16_DEFECT_READ_OK*/
    bDoNotify = pShMem->rDriveStatus.u32Defect != u32Status &&
                u32Status != CD_MAIN_U32_NOT_INITIALIZED;
    pShMem->rDriveStatus.u32Defect = u32Status;
    break;

  case OSAL_C_U16_NOTI_DEVICE_READY :
    /*u16Status =
     OSAL_C_U16_DEVICE_NOT_READY
     OSAL_C_U16_DEVICE_READY
     OSAL_C_U16_DEVICE_UV_NOT_READY
     OSAL_C_U16_DEVICE_UV_READY*/
    bDoNotify = pShMem->rDriveStatus.u32DeviceReady != u32Status;
    pShMem->rDriveStatus.u32DeviceReady = u32Status;
    break;
  default:
    bDoNotify = FALSE;
    u32Ret = OSAL_E_NOTSUPPORTED;
    CD_PRINTF_FATAL(" TYPE_XXX not supported: 0x%04X",
                    (unsigned int)u16Type);
  } //switch(u16Type)

  if(bDoNotify)
  {
    vNotifyMediaClients(pShMem, u16Type, (tU16)u32Status);
  } //if(bDoNotify)

  CD_TRACE_LEAVE_U4(CDID_u32SetDriveStatus, u32Ret, bDoNotify, u16Type,
                    u32Status, 0);

  return u32Ret;
}

/************************************************************************
 *                                                                       *
 * FUNCTION                                                              *
 *      iFindPlayInfoNotifier                                            *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Searches the PlayInfo notification table for an entry that       *
 *      matches a given specification (i.e. has the same cookie and      *
 *      callback function).                                              *
 *                                                                       *
 * NOTE                                                                  *
 *      The CDAUDIO_Lock should have been obtained before this function  *
 *      gets called.                                                     *
 *                                                                       *
 * CALLS                                                                 *
 *                                                                       *
 * INPUTS                                                                *
 *      OSAL_trPlayInfoReg* - a registration structure describing the    *
 *                            entry that is to be found.                 *
 *                                                                       *
 * OUTPUTS                                                               *
 *      tInt - the index of a matching entry in the notification table,  *
 *             or -1 if no matching entry has been found.                *
 *                                                                       *
 ************************************************************************/
static tInt iFindPlayInfoNotifier(tDevCDData *pShMem,
                                  const OSAL_trPlayInfoReg* prReg)
{
  tInt nPos = -1;
  tInt nIdx;

  CD_CHECK_SHARED_MEM(pShMem, -1);

  CD_TRACE_ENTER_U4(CDID_iFindPlayInfoNotifier, 0, prReg, 0, 0);

  for(nIdx = 0; (nIdx < CD_MAX_PLAYINFO_NOTIFIERS) && (nPos < 0); ++nIdx)
  {
    if((pShMem->arPlayInfoReg[nIdx].pvUserData == prReg->pvCookie)
       && (pShMem->arPlayInfoReg[nIdx].funcUserCallback == prReg->pCallback))
    {
      nPos = nIdx;
    }
  }
  CD_TRACE_LEAVE_U4(CDID_iFindPlayInfoNotifier, OSAL_E_NOERROR, nPos, nIdx,
                    CD_MAX_PLAYINFO_NOTIFIERS, 0);
  return nPos;
}

/************************************************************************
 *                                                                       *
 * FUNCTION                                                              *
 *      nFindMedia er                                                    *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Searches the PlayInfo notification table for an entry that       *
 *      matches a given specification (i.e. has the same cookie and      *
 *      callback function).                                              *
 *                                                                       *
 * NOTE                                                                  *
 *      The CDAUDIO_Lock should have been obtained before this function  *
 *      gets called.                                                     *
 *                                                                       *
 * CALLS                                                                 *
 *                                                                       *
 * INPUTS                                                                *
 *      OSAL_trPlayInfoReg* - a registration structure describing the    *
 *                            entry that is to be found.                 *
 *                                                                       *
 * OUTPUTS                                                               *
 *      tInt - the index of a matching entry in the notification table,  *
 *             or -1 if no matching entry has been found.                *
 *                                                                       *
 ************************************************************************/
static tInt iFindMediaNotifier(tDevCDData *pShMem,
                               const OSAL_trNotifyData* prReg)
{
  tInt nPos = -1;
  tInt nIdx;

  CD_CHECK_SHARED_MEM(pShMem, -1);

  CD_TRACE_ENTER_U4(CDID_iFindMediaNotifier, 0, prReg, 0, 0);

  for(nIdx = 0; (nIdx < CD_MAX_MEDIA_NOTIFIERS) && (nPos < 0); ++nIdx)
  {
    if(pShMem->arMediaReg[nIdx].u16AppID == prReg->u16AppID)
    {
      nPos = nIdx;
    }
  }
  CD_TRACE_LEAVE_U4(CDID_iFindMediaNotifier, OSAL_E_NOERROR, nPos, nIdx,
                    CD_MAX_MEDIA_NOTIFIERS, 0);
  return nPos;
}

/************************************************************************
 *                                                                       *
 * FUNCTION                                                              *
 *      vNotifyPlayInfoClients                                           *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Calls all registered callbacks.                                  *
 *                                                                       *
 * CALLS                                                                 *
 *      User supplied callback functions.                                *
 *                                                                       *
 * INPUTS                                                                *
 *      OSAL_trPlayInfo* - the current PlayInfo value.                   *
 *                                                                       *
 * OUTPUTS                                                               *
 *      None                                                             *
 *                                                                       *
 ************************************************************************/
static tVoid vNotifyPlayInfoClients(tDevCDData *pShMem,
                                    const OSAL_trPlayInfo* prInfo,
                                    tBool bForced)
{
  static OSAL_trPlayInfo rInfoPrev =
  { 0};
  OSAL_trPlayInfo rInfo;
  tInt i = 0;

  CD_CHECK_SHARED_MEM_VOID(pShMem);
  CD_TRACE_ENTER_U4(CDID_vNotifyPlayInfoClients, prInfo, 0, 0, 0);

  if(NULL != prInfo)
  {
    tCD_PLAYINFOCALLBACK_ARG arg;
    tBool bInfoHasChanged;

    rInfo = *prInfo;

    bInfoHasChanged = memcmp(&rInfo, &rInfoPrev, sizeof(OSAL_trPlayInfo)) != 0;

    if(bForced || bInfoHasChanged)
    {
      arg.rPlayinfo = rInfo;
      for(i = 0; i < CD_MAX_PLAYINFO_NOTIFIERS; i++)
      {
        if(pShMem->arPlayInfoReg[i].funcUserCallback)
        {
          arg.pvUserData = pShMem->arPlayInfoReg[i].pvUserData;
          arg.funcUserCallback = pShMem->arPlayInfoReg[i].funcUserCallback;
          u32ExecuteCallback(OSAL_ProcessWhoAmI(),
                             pShMem->arPlayInfoReg[i].ProcID,
                             pShMem->arPlayInfoReg[i].CD_Callback, &arg,
                             sizeof(arg));
        } //if(pShMem->arPlayInfoReg[i].funcUserCallback)
      } //for(i = 0; i < CD_MAX_PLAYINFO_NOTIFIERS; i++)

      rInfoPrev = rInfo;
    }
    else //if(bForced || bInfoHasChanged)
    {
      CD_PRINTF_U2("Playinfo has not changed");
    } //else //if(bForced || bInfoHasChanged)
  } //if(NULL != prInfo)

  CD_TRACE_LEAVE_U4(CDID_vNotifyPlayInfoClients, OSAL_E_NOERROR, i, 0,
                    CD_MAX_PLAYINFO_NOTIFIERS, 0);
}

/************************************************************************
 *                                                                       *
 * FUNCTION                                                              *
 *      vNotifyMediaClients                                              *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Calls all registered callbacks.                                  *
 *                                                                       *
 * CALLS                                                                 *
 *      User supplied callback functions.                                *
 *                                                                       *
 * INPUTS                                                                *
 *      OSAL_trPlayInfo* - the current PlayInfo value.                   *
 *                                                                       *
 * OUTPUTS                                                               *
 *      None                                                             *
 *                                                                       *
 ************************************************************************/
static tVoid vNotifyMediaClients(tDevCDData *pShMem, tU16 u16Notification,
                                 tU16 u16Status)
{
  tInt i;
  tCD_MEDIACALLBACK_ARG arg;

  CD_CHECK_SHARED_MEM_VOID(pShMem);
  CD_TRACE_ENTER_U4(CDID_vNotifyMediaClients, u16Notification, u16Status, 0, 0);

  for(i = 0; i < CD_MAX_MEDIA_NOTIFIERS; i++)
  {
    if(0 != (u16Notification & pShMem->arMediaReg[i].u16NotificationType))
    {
      if(pShMem->arMediaReg[i].funcUserCallback)
      {
        //example: OSAL_C_U16_NOTI_MEDIA_CHANGE <<16 | OSAL_C_U16_MEDIA_EJECTED
        arg.u32MediaType = ((tU32)u16Notification << 16) | u16Status;

        arg.funcUserCallback = pShMem->arMediaReg[i].funcUserCallback;
        CD_PRINTF_U1("[%s] Func [%p]"
                     " u16Notification [0x%04X] u16Status [0x%04X]",
                     __FUNCTION__,
                     arg.funcUserCallback,
                     (unsigned int)u16Notification,
                     (unsigned int)u16Status);
        
        //150114, srt2hi: in order to satisfy f*****g LÍNT,
        // I have to do this check a second time
        CD_CHECK_SHARED_MEM_VOID(pShMem);
        u32ExecuteCallback(OSAL_ProcessWhoAmI(), pShMem->arMediaReg[i].ProcID,
                           pShMem->arMediaReg[i].CD_Callback, &arg,
                           sizeof(arg));
      } //if(pShMem->arMediaReg[i].funcUserCallback)
    } //if(0 != (u16Notification & pShMem->arMediaReg[i].u16NotificationType))
  } //for(i = 0; i < CD_MAX_PLAYINFO_NOTIFIERS; i++)

  CD_TRACE_LEAVE_U4(CDID_vNotifyMediaClients, OSAL_E_NOERROR, i, 0,
                    CD_MAX_MEDIA_NOTIFIERS, 0);
}

/************************************************************************
 |function implementation (scope: global)
 |-----------------------------------------------------------------------*/
/*****************************************************************************
 * FUNCTION:     u32PostMainEvent
 * PARAMETER:    IN: event mask
 * RETURNVALUE:  OSAL ERROR code
 * DESCRIPTION:  open, post, close MAIN event
 ******************************************************************************/
tU32 CD_u32PostMainEvent(tDevCDData *pShMem, OSAL_tEventMask eventMask)
{
  tU32 u32Result = OSAL_E_NOERROR;
  tS32 s32Ret;
  OSAL_tEventHandle hMainEvent = OSAL_C_INVALID_HANDLE;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_CD_u32PostMainEvent, eventMask, 0, 0, 0);

  s32Ret = OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent);
  if(OSAL_OK == s32Ret)
  {
    s32Ret = OSAL_s32EventPost(hMainEvent, eventMask, OSAL_EN_EVENTMASK_OR);
    (void)OSAL_s32EventClose(hMainEvent);
  } //if(OSAL_OK == s32Ret)

  if(OSAL_OK != s32Ret)
  {
    u32Result = OSAL_u32ErrorCode();
  } //if(OSAL_OK != s32Ret)
  CD_TRACE_LEAVE_U4(CDID_CD_u32PostMainEvent, u32Result, eventMask, 0, 0, 0);
  return u32Result;
}

/************************************************************************
 * FUNCTION                                                              *
 *      CD_u16GetDriveStatus                                             *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Get drive status                                                 *
 *                                                                       *
 *                                                                       *
 ************************************************************************/
tU16 CD_u16GetDriveStatus(tDevCDData *pShMem, tU16 u16Type)

{
  tU16 u16Status;
  tU32 u32Status;

  CD_CHECK_SHARED_MEM(pShMem, CD_MAIN_U16_NOT_INITIALIZED);
  CD_TRACE_ENTER_U4(CDID_CD_u16GetDriveStatus, u16Type, 0, 0, 0);

  switch(u16Type)
  {
  case OSAL_C_U16_NOTI_MEDIA_CHANGE :
    /* u16Status:
     OSAL_C_U16_MEDIA_EJECTED
     OSAL_C_U16_INCORRECT_MEDIA
     OSAL_C_U16_DATA_MEDIA
     OSAL_C_U16_AUDIO_MEDIA
     OSAL_C_U16_UNKNOWN_MEDIA*/
    u32Status = pShMem->rDriveStatus.u32MediaChange;
    u16Status = (tU16)(
                      u32Status == CD_MAIN_U32_NOT_INITIALIZED
                                   ? OSAL_C_U16_MEDIA_EJECTED
                                   :
                                   u32Status);
    break;

  case OSAL_C_U16_NOTI_TOTAL_FAILURE :
    /*u16Status =
     OSAL_C_U16_DEVICE_OK
     OSAL_C_U16_DEVICE_FAIL*/
    //notify client, if new status is different from last notified status
    u32Status = pShMem->rDriveStatus.u32TotalFailure;
    u16Status = (tU16)(
                      u32Status == CD_MAIN_U32_NOT_INITIALIZED 
                                   ? OSAL_C_U16_DEVICE_OK :
                                   u32Status);
    break;

  case OSAL_C_U16_NOTI_MEDIA_STATE :
    /* u16Status =
     OSAL_C_U16_MEDIA_NOT_READY
     OSAL_C_U16_MEDIA_READY*/
    u32Status = pShMem->rDriveStatus.u32MediaState;
    u16Status = (tU16)(
                      u32Status == CD_MAIN_U32_NOT_INITIALIZED
                                   ? OSAL_C_U16_MEDIA_NOT_READY :
                                   u32Status);
    break;

  case OSAL_C_U16_NOTI_DEFECT :
    /*u16Status =
     OSAL_C_U16_DEFECT_LOAD_EJECT
     OSAL_C_U16_DEFECT_LOAD_INSERT
     OSAL_C_U16_DEFECT_DISCTOC
     OSAL_C_U16_DEFECT_DISC
     OSAL_C_U16_DEFECT_READ_ERR
     OSAL_C_U16_DEFECT_READ_OK*/
    u32Status = pShMem->rDriveStatus.u32Defect;
    u16Status = (tU16)(
                      u32Status == CD_MAIN_U32_NOT_INITIALIZED
                                   ? OSAL_C_U16_DEFECT_TEMP_READ :
                                   u32Status);
    break;

  case OSAL_C_U16_NOTI_DEVICE_READY :
    /*u16Status =
     OSAL_C_U16_DEVICE_NOT_READY
     OSAL_C_U16_DEVICE_READY
     OSAL_C_U16_DEVICE_UV_NOT_READY
     OSAL_C_U16_DEVICE_UV_READY*/
    u32Status = pShMem->rDriveStatus.u32DeviceReady;
    u16Status = (tU16)(
                      u32Status == CD_MAIN_U32_NOT_INITIALIZED 
                                   ? OSAL_C_U16_DEVICE_NOT_READY :
                                   u32Status);
    break;
  default:
    u16Status = CD_MAIN_U16_NOT_INITIALIZED;
    CD_PRINTF_FATAL(" TYPE_XXX not supported: 0x%04X",
                    (unsigned int)u16Type);
    ;
  } //switch(u16Type)

  CD_TRACE_LEAVE_U4(
                   CDID_CD_u16GetDriveStatus,
                   u16Status != CD_MAIN_U16_NOT_INITIALIZED 
                                ? OSAL_E_NOERROR : OSAL_E_INVALIDVALUE,
                                u16Type, u16Status, 0, 0);
  return u16Status;
}

/*****************************************************************************
 * FUNCTION:     CD_u8SwitchState
 * PARAMETER:    IN: pointer to shared memory
 *               IN: new state
 * RETURNVALUE:  previous status
 * DESCRIPTION:  sets new internal state and start or stop 
 *               polling of play time (read subchannels from SCSI device)
 ******************************************************************************/
tU8 CD_u8SwitchState(tDevCDData *pShMem, tU8 u8NewState)
{

  CD_CHECK_SHARED_MEM(pShMem, 0);

  CD_TRACE_ENTER_U4(CDID_CD_u8SwitchState, 0, pShMem->rCDAUDIO.u8PrevStatus,
                    pShMem->rCDAUDIO.u8Status, u8NewState);

  pShMem->rCDAUDIO.u8PrevStatus = pShMem->rCDAUDIO.u8Status;
  pShMem->rCDAUDIO.u8Status = u8NewState;

  //turn subchannel polling on or off
  switch(u8NewState)
  {
  case CDAUDIO_STATUS_PLAY:
    (void)CD_u32PostMainEvent(pShMem,
                              CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_LOW);
    break;

  case CDAUDIO_STATUS_SCAN_BWD:
  case CDAUDIO_STATUS_SCAN_FWD:
    (void)CD_u32PostMainEvent(pShMem,
                              CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_HI);
    break;

  case CDAUDIO_STATUS_PAUSED:
  case CDAUDIO_STATUS_PAUSED_BY_USER:
  case CDAUDIO_STATUS_STOP:
  case CDAUDIO_STATUS_ERROR:
  case CDAUDIO_STATUS_UNDERVOLTAGE_ERROR:
  default:
#   ifdef CD_STOP_SUBCHANNEL_TIMER_AUTOMATICALLY
    (void)CD_u32PostMainEvent(pShMem,
                              CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER);
#   endif //#ifdef CD_STOP_SUBCHANNEL_TIMER_AUTOMATICALLY
    break;
  } //switch (u8S)

  CD_TRACE_LEAVE_U4(CDID_CD_u8SwitchState, OSAL_E_NOERROR, 0,
                    pShMem->rCDAUDIO.u8PrevStatus, pShMem->rCDAUDIO.u8Status,
                    u8NewState);
  return pShMem->rCDAUDIO.u8PrevStatus;
}

/*****************************************************************************
 * FUNCTION:     CD_u32GetPlayStatus
 * PARAMETER:    DriveIndex
 * RETURNVALUE:  OSAL-Playstatus
 * DESCRIPTION:  convert internal playstatus to OSAL playstatus
 *
 ******************************************************************************/
tU32 CD_u32GetPlayStatus(tDevCDData *pShMem)
{
  tS32 s32Status = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
  tU8 u8S;
  tU8 u8PS;

  u8S = pShMem->rCDAUDIO.u8Status;
  u8PS = pShMem->rCDAUDIO.u8PrevStatus;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CD_TRACE_ENTER_U4(CDID_CD_u32GetPlayStatus, 0, u8S, u8PS, 0);

  switch(u8S)
  {
  case CDAUDIO_STATUS_PAUSED:
  case CDAUDIO_STATUS_PAUSED_BY_USER:
    s32Status = OSAL_C_S32_PLAY_PAUSED;
    break;

  case CDAUDIO_STATUS_STOP:
    s32Status = OSAL_C_S32_PLAY_COMPLETED;
    break;

  case CDAUDIO_STATUS_PLAY:
  case CDAUDIO_STATUS_SCAN_BWD:
  case CDAUDIO_STATUS_SCAN_FWD:
    s32Status = OSAL_C_S32_PLAY_IN_PROGRESS;
    break;

  case CDAUDIO_STATUS_ERROR:
    if((u8PS == CDAUDIO_STATUS_PLAY) || (u8PS == CDAUDIO_STATUS_SCAN_BWD)
       || (u8PS == CDAUDIO_STATUS_SCAN_FWD))
    {
      s32Status = OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_ERROR;
    }
    break;

  case CDAUDIO_STATUS_UNDERVOLTAGE_ERROR:
    if((u8PS == CDAUDIO_STATUS_PLAY) || (u8PS == CDAUDIO_STATUS_SCAN_BWD)
       || (u8PS == CDAUDIO_STATUS_SCAN_FWD))
    {
      s32Status = OSAL_C_S32_PLAY_OPERATION_STOPPED_DUE_TO_UNDERVOLTAGE;
    }
    break;

  default:
    break;
  } //switch (u8S)
  CD_TRACE_LEAVE_U4(CDID_CD_u32GetPlayStatus, OSAL_E_NOERROR, u8S, u8PS,
                    s32Status, 0);
  return(tU32)s32Status;
}

/******************************************************************************
 *FUNCTION      :CD_vPlayInfoCallback
 *
 *DESCRIPTION   :Function to be called by OSAL for callbacks.
 *               This in turn calls the client callback,
 *               registered via OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY.
 *               This additional indirection is necessary, because the 
 *               notification callback expects two parameters, 
 *               while OSAL provides only one
 *
 *PARAMETER     :Arg    Pointer to data for notification callback
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 05 05
 *****************************************************************************/
void CD_vPlayInfoCallback(void *pvArg)
{
  tCD_PLAYINFOCALLBACK_ARG *arg = (tCD_PLAYINFOCALLBACK_ARG *)pvArg;
  arg->funcUserCallback(arg->pvUserData, &arg->rPlayinfo);
}

/******************************************************************************
 *FUNCTION      :CD_vMediaCallback
 *
 *DESCRIPTION   :Function to be called by OSAL for callbacks.
 *               This in turn calls the client callback, registered 
 *               via OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY.
 *               This additional indirection is necessary, because the 
 *               notification callback expects two parameters, 
 *               while OSAL provides only one
 *
 *PARAMETER     :Arg    Pointer to data for notification callback
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 05 05
 *****************************************************************************/
void CD_vMediaCallback(void *pvArg)
{
  tCD_MEDIACALLBACK_ARG *arg = (tCD_MEDIACALLBACK_ARG *)pvArg;
  arg->funcUserCallback(&arg->u32MediaType);
}

/******************************************************************************
 *FUNCTION      :CD_RegisterNotifier
 *
 *DESCRIPTION   :Registers a callback for notification.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *
 *PARAMETER     :Info          Informations about current status of
 *                             CD_UpdateThread
 *               PlayInfoReg   Structure that describes the callback to register
 *
 *RETURNVALUE   :OSAL_E_ALREADYEXISTS   if PlayInfoReg is already registered,
 *               OSAL_E_NOSPACE         if Info->AudioCallbacks is full, or
 *               OSAL_E_NOERROR         if PlayInfoReg was registered
 *
 *HISTORY:      :Created by FAN4HI 2012 05 05
 *****************************************************************************/
tU32 CD_u32RegisterPlayInfoNotifier(tDevCDData *pShMem,
                                    const OSAL_trPlayInfoReg* rPlayInfoReg)
{
  tU32 u32Ret = OSAL_E_NOSPACE;
  tInt i = -1;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_CD_u32RegisterPlayInfoNotifier, 0, 0, 0, 0);

  if(iFindPlayInfoNotifier(pShMem, rPlayInfoReg) != -1)
  {
    u32Ret = OSAL_E_ALREADYEXISTS;
  }
  else
  {
    for(i = 0; i < CD_MAX_PLAYINFO_NOTIFIERS; i++)
    {
      if(pShMem->arPlayInfoReg[i].funcUserCallback == NULL)
      {
        pShMem->arPlayInfoReg[i].ProcID = OSAL_ProcessWhoAmI();
        pShMem->arPlayInfoReg[i].CD_Callback = CD_vPlayInfoCallback;
        pShMem->arPlayInfoReg[i].pvUserData = rPlayInfoReg->pvCookie;
        pShMem->arPlayInfoReg[i].funcUserCallback = rPlayInfoReg->pCallback;
        u32Ret = OSAL_E_NOERROR;
        break;
      }
    }
  }

  CD_TRACE_LEAVE_U4(CDID_CD_u32RegisterPlayInfoNotifier, u32Ret, i, 0, 0, 0);
  return u32Ret;
}

/******************************************************************************
 *FUNCTION      :CD_RegisterMediaNotifier
 *
 *DESCRIPTION   :Registers a callback for notification.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *
 *PARAMETER     :Info          Informations about current status of
 *                             CD_UpdateThread
 *               g   Structure that describes the callback to register
 *
 *RETURNVALUE   :OSAL_E_ALREADYEXISTS   if PlayInfoReg is already registered,
 *               OSAL_E_NOSPACE         if Info->AudioCallbacks is full, or
 *               OSAL_E_NOERROR         if PlayInfoReg was registered
 *
 *HISTORY:      :Created by FAN4HI 2012 05 05
 *****************************************************************************/
tU32 CD_u32RegisterMediaNotifier(tDevCDData *pShMem,
                                 const OSAL_trNotifyData* pReg)
{
  tU32 u32Ret = OSAL_E_NOSPACE;
  tInt i = -1;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_CD_u32RegisterMediaNotifier, pReg, 0, 0, 0);

  if(NULL != pReg)
  {
    i = iFindMediaNotifier(pShMem, pReg); // search for AppID only
    if(i != -1)
    { // notifier exist, update Type
      pShMem->arMediaReg[i].ProcID = OSAL_ProcessWhoAmI();
      pShMem->arMediaReg[i].CD_Callback = CD_vMediaCallback;
      pShMem->arMediaReg[i].funcUserCallback = pReg->pCallback;
      pShMem->arMediaReg[i].u16AppID = pReg->u16AppID;
      pShMem->arMediaReg[i].u16NotificationType |= pReg->u16NotificationType;
      u32Ret = OSAL_E_NOERROR;
    }
    else //if(i != -1)
    {
      // notifier not exist, search free entry
      for(i = 0; i < CD_MAX_MEDIA_NOTIFIERS; i++)
      {
        if(pShMem->arMediaReg[i].u16AppID == OSAL_C_U16_INVALID_APPID)
        {
          pShMem->arMediaReg[i].ProcID = OSAL_ProcessWhoAmI();
          pShMem->arMediaReg[i].CD_Callback = CD_vMediaCallback;
          pShMem->arMediaReg[i].funcUserCallback = pReg->pCallback;
          pShMem->arMediaReg[i].u16NotificationType = pReg->u16NotificationType;
          pShMem->arMediaReg[i].u16AppID = pReg->u16AppID;
          u32Ret = OSAL_E_NOERROR;
          break;
        } //if(pShMem->arMediaReg[i].u16AppID == OSAL_C_U16_INVALID_APPID)
      } //for(i = 0; i < CD_MAX_MEDIA_NOTIFIERS; i++)
    } //else //if(i != -1)

    //notify client first time with recent states
    // call callback directly, because we are in precess context of caller
    if(u32Ret == OSAL_E_NOERROR)
    {
      tU32 u32T = (tU32)pReg->u16NotificationType;
      tU32 u32Type;
      tU32 u32Status;
      tU32 u32Par;

      u32Type = (tU32)OSAL_C_U16_NOTI_DEVICE_READY;
      if(0 != (u32T & u32Type))
      {
        u32Status = pShMem->rDriveStatus.u32DeviceReady & 0xFFFF;
        u32Par = (u32Type << 16) | u32Status;
        pReg->pCallback(&u32Par);
      } //if(0 != (u16T  & ...))

      u32Type = (tU32)OSAL_C_U16_NOTI_MEDIA_CHANGE;
      if(0 != (u32T & u32Type))
      {
        u32Status = pShMem->rDriveStatus.u32MediaChange & 0xFFFF;
        u32Par = (u32Type << 16) | u32Status;
        pReg->pCallback(&u32Par);
      } //if(0 != (u16T  & ...))

      u32Type = (tU32)OSAL_C_U16_NOTI_MEDIA_STATE;
      if(0 != (u32T & u32Type))
      {
        u32Status = pShMem->rDriveStatus.u32MediaState & 0xFFFF;
        u32Par = (u32Type << 16) | u32Status;
        pReg->pCallback(&u32Par);
      } //if(0 != (u16T  & ...))

      u32Type = (tU32)OSAL_C_U16_NOTI_TOTAL_FAILURE;
      if(0 != (u32T & u32Type))
      {
        u32Status = pShMem->rDriveStatus.u32TotalFailure & 0xFFFF;
        u32Par = (u32Type << 16) | u32Status;
        pReg->pCallback(&u32Par);
      } //if(0 != (u16T  & ...))

      u32Type = (tU32)OSAL_C_U16_NOTI_DEFECT;
      if(0 != (u32T & u32Type))
      {
        if(pShMem->rDriveStatus.u32Defect != CD_MAIN_U32_NOT_INITIALIZED)
        {
          u32Status = pShMem->rDriveStatus.u32Defect & 0xFFFF;
          u32Par = (u32Type << 16) | u32Status;
          pReg->pCallback(&u32Par);
        } //if(u32Status != CD_MAIN_U32_NOT_INITIALIZED)
      } //if(0 != (u16T  & ...))
    } //if(u32Ret == OSAL_E_NOERROR)

  }
  else //if(NULL != pReg)
  {
    u32Ret = OSAL_E_INVALIDVALUE;
  } //else //if(NULL != pReg)

  CD_TRACE_LEAVE_U4(CDID_CD_u32RegisterMediaNotifier, u32Ret, i, 0, 0, 0);
  return u32Ret;
}

/******************************************************************************
 *FUNCTION      :CD_UnregisterNotifier
 *
 *DESCRIPTION   :Removes an entry from Info->AudioCallbacks.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *
 *PARAMETER     :Info           Informations about current status of
 *                              CD_UpdateThread
 *               PlayInfoReg    Structure that describes the callback to remove
 *
 *RETURNVALUE   :OSAL_E_DOESNOTEXIST    if no matching entry was found or,
 *               OSAL_E_NOERROR         if the entry was removed
 *
 *HISTORY:      :Created by FAN4HI 2012 05 05
 *****************************************************************************/
tU32 CD_u32UnregisterPlayInfoNotifier(tDevCDData *pShMem,
                                      const OSAL_trPlayInfoReg* PlayInfoReg)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  tInt idx;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_CD_u32UnregisterPlayInfoNotifier, PlayInfoReg, 0, 0,
                    0);

  idx = iFindPlayInfoNotifier(pShMem, PlayInfoReg);
  if(idx == -1)
  {
    u32Ret = OSAL_E_DOESNOTEXIST;
  }
  else
  {
    pShMem->arPlayInfoReg[idx].funcUserCallback = NULL;
  }

  CD_TRACE_LEAVE_U4(CDID_CD_u32UnregisterPlayInfoNotifier, u32Ret, idx, 0, 0,
                    0);
  return u32Ret;
}

/******************************************************************************
 *FUNCTION      :CD_UnregisterMediaNotifier
 *
 *DESCRIPTION   :Removes an entry from Info->AudioCallbacks.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *
 *PARAMETER     :Info           Informations about current status of
 *                              CD_UpdateThread
 *               PlayInfoReg    Structure that describes the callback to remove
 *
 *RETURNVALUE   :OSAL_E_DOESNOTEXIST    if no matching entry was found or,
 *               OSAL_E_NOERROR         if the entry was removed
 *
 *HISTORY:      :Created by FAN4HI 2012 05 05
 *****************************************************************************/
tU32 CD_u32UnregisterMediaNotifier(tDevCDData *pShMem,
                                   const OSAL_trRelNotifyData* pReg)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  tInt idx;
  OSAL_trNotifyData rReg;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CD_TRACE_ENTER_U4(CDID_CD_u32UnregisterMediaNotifier, pReg, 0, 0, 0);

  rReg.u16NotificationType = pReg->u16NotificationType;
  rReg.u16AppID = pReg->u16AppID;

  idx = iFindMediaNotifier(pShMem, &rReg); //search for AppID only!
  if(idx == -1)
  {
    u32Ret = OSAL_E_DOESNOTEXIST;
  }
  else
  {
    pShMem->arMediaReg[idx].u16NotificationType &= ~(pReg->u16NotificationType);
    if(0 == pShMem->arMediaReg[idx].u16NotificationType)
    { // remove notifier if empty
      pShMem->arMediaReg[idx].funcUserCallback = NULL;
      pShMem->arMediaReg[idx].u16AppID = OSAL_C_U16_INVALID_APPID;
      pShMem->arMediaReg[idx].ProcID = 0;
      pShMem->arMediaReg[idx].CD_Callback = NULL;
      pShMem->arMediaReg[idx].u16NotificationType = 0;
    } //if(0 == pShMem->arMediaReg[i].u16NotificationType)
  }

  CD_TRACE_LEAVE_U4(CDID_CD_u32UnregisterMediaNotifier, u32Ret, idx, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CD_u32GetShMem
 * PARAMETER:    OUT: Pointer to pointer (!) of shared mem
 *               OUT: Pointer to shared mem handle
 *               OUT: Pointer to handle of semaphore (protects shared mem)
 * RETURNVALUE:  OSAL_ERRORCODE
 * DESCRIPTION:  returns pointer to shared memory or NULL in case of error
 *               returns handle of shared mem or OSAL_ERROR
 *               returns handle of semaphore used to protect shared mem
 *                                             or OSAL_C_INVALID_HANDLE
 * HISTORY:
 ******************************************************************************/
tU32 CD_u32GetShMem(tDevCDData **ppShMem, OSAL_tShMemHandle *phShMem,
                    OSAL_tSemHandle *phSem)
{
  tU32 u32Result = OSAL_E_NOERROR;
  OSAL_tShMemHandle hShMem;
  tDevCDData *pShMem = NULL;
  OSAL_tSemHandle hSem = OSAL_C_INVALID_HANDLE;

  CD_TRACE_ENTER_U4F(CDID_CD_u32GetShMem, ppShMem, phShMem, phSem, 0);

  hShMem = OSAL_SharedMemoryOpen(CD_SHMEM_NAME, OSAL_EN_READWRITE);
  if(OSAL_ERROR == hShMem)
  {
    u32Result = OSAL_u32ErrorCode();
    CD_PRINTF_ERRORS("CD_bGetShMem ERROR: [%s] OSAL_SharedMemoryOpen(%s)",
                     (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()),
                     CD_SHMEM_NAME);
  }
  else //if(OSAL_ERROR == hShMem)
  {
    pShMem = (tDevCDData *)OSAL_pvSharedMemoryMap(hShMem, OSAL_EN_READWRITE,
                                                  sizeof(tDevCDData), 0);
    if(NULL == pShMem)
    {
      u32Result = OSAL_u32ErrorCode();
      CD_PRINTF_ERRORS("CD_bGetShMem cannot map SharedMemory [%s]",
                       (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
    } //if(NULL == pShMem)
  } //else //if(OSAL_ERROR == hShMem)

  if(OSAL_s32SemaphoreOpen(CD_SEM_NAME, &hSem) != OSAL_OK)
  {
    u32Result = OSAL_u32ErrorCode();
    CD_PRINTF_ERRORS("CD_bGetShMem cannot open Sempahore [%s]",
                     (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
  }

  if(ppShMem)
    *ppShMem = pShMem;
  if(phShMem)
    *phShMem = hShMem;
  if(phSem)
    *phSem = hSem;

  CD_TRACE_LEAVE_U4(CDID_CD_u32GetShMem, u32Result, pShMem, hShMem, hSem, 0);

  return u32Result;
}

/*****************************************************************************
 * FUNCTION:     CD_vReleaseShMem
 * PARAMETER:    pointer mapped memory, handle of shared mem, handle of
 *               sempahore
 * RETURNVALUE:  void
 * DESCRIPTION:  return pointer to ahredmemory or NULL in case of error
 * HISTORY:
 ******************************************************************************/
tVoid CD_vReleaseShMem(tDevCDData *pShMem, OSAL_tShMemHandle hShMem,
                       OSAL_tSemHandle hSem)
{
  if(NULL != pShMem)
  {
    (void)OSAL_s32SharedMemoryUnmap(pShMem, sizeof(tDevCDData));
  } //if(NULL != pShMem)

  if(OSAL_ERROR != hShMem)
  {
    (void)OSAL_s32SharedMemoryClose(hShMem);
  } //if(OSAL_ERROR != hShMem)

  if(OSAL_C_INVALID_HANDLE != hSem)
  {
    (void)OSAL_s32SemaphoreClose(hSem);
  } //if( = OSAL_C_INVALID_HANDLE != hSem)
}

/*****************************************************************************
 * FUNCTION:     CD_u32Init
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  startpoint of SCSI interface
 * HISTORY:
 *        | Date      | Modification               | Author
 *        | 28/09/2016| Addition of call to        | boc7kor
 *        |           | CD_vReadRegistryValues as a|
 *        |           | fix for NCG3D-23434        |
 ******************************************************************************/
tU32 CD_u32Init()
{
  tU32 u32Ret = OSAL_E_NOERROR;
  OSAL_tSemHandle hSem = OSAL_C_INVALID_HANDLE;
  tDevCDData *pShMem = NULL;

  //init module local data
  grMainData.bMainThreadIsRunning = FALSE;
  grMainData.bVoltThreadIsRunning = FALSE;
  grMainData.bUdevThreadDoLoop = FALSE;
  grMainData.bUdevThreadIsRunning = FALSE;
  grMainData.hMainEvent = OSAL_C_INVALID_HANDLE;
  //l i  n t grMainData.hShMem               = OSAL_ERROR;
  grMainData.hSubchannelTimer = OSAL_C_INVALID_HANDLE;
  grMainData.hLoaderStateTimer = OSAL_C_INVALID_HANDLE;
  grMainData.hWatchdogTimer = OSAL_C_INVALID_HANDLE;
  grMainData.pSharedMem = NULL;
  grMainData.rTraceIODescriptor = OSAL_ERROR;
  grMainData.hVolt = OSAL_ERROR;
  grMainData.sVolt.u32ClientId = (tU32)-1;
  grMainData.sVolt.szNotificationEventName[0] = '\0';
  grMainData.u32LowVoltageHandlingThreshold=0x00000000;

  grMainData.hShMem = OSAL_SharedMemoryCreate(CD_SHMEM_NAME, OSAL_EN_READWRITE,
                                              sizeof(tDevCDData));
  if(OSAL_ERROR == grMainData.hShMem)
  { //create fails, try to open existing mem
    grMainData.hShMem = OSAL_SharedMemoryOpen(CD_SHMEM_NAME, OSAL_EN_READWRITE);
    if(grMainData.hShMem == OSAL_ERROR)
    {
      u32Ret = OSAL_u32ErrorCode();
    }
  } //if(grMainData.hShMem == OSAL_ERROR)

  if(OSAL_E_NOERROR == u32Ret)
  {
    if(OSAL_s32SemaphoreCreate(CD_SEM_NAME, &hSem, 1) != OSAL_OK)
    {
      if(OSAL_s32SemaphoreOpen(CD_SEM_NAME, &hSem) != OSAL_OK)
      {
        u32Ret = OSAL_u32ErrorCode();
      }
    } //if(OSAL_s32SemaphoreCreate(semname, &semhandle, 1) != OSAL_OK)
  } //if(OSAL_E_NOERROR == u32Ret)

  if(OSAL_E_NOERROR == u32Ret)
  {
    (void)OSAL_s32SemaphoreClose(hSem);

    pShMem = (tDevCDData *)OSAL_pvSharedMemoryMap(grMainData.hShMem,
                                                  OSAL_EN_READWRITE,
                                                  sizeof(tDevCDData), 0);
    grMainData.pSharedMem = pShMem;
    if(pShMem != NULL)
    {
      CD_vRegTrace();

      u32Ret = CD_SCSI_IF_u32Init(pShMem);
      if(OSAL_E_NOERROR == u32Ret)
      {
        u32Ret = u32InitMain(pShMem);
      } //if(OSAL_E_NOERROR == u32Ret)
      if(OSAL_E_NOERROR == u32Ret)
      {
        u32Ret = CDAUDIO_u32Init(pShMem);
      } //if(OSAL_E_NOERROR == u32Ret)
      if(OSAL_E_NOERROR == u32Ret)
      {
        u32Ret = CDCTRL_u32Init(pShMem);
      } //if(OSAL_E_NOERROR == u32Ret)
    }
    else //if(pSharedMem != NULL)
    {
      u32Ret = OSAL_E_NOSPACE;
    } //else //if(pSharedMem != NULL)
  } //if(OSAL_E_NOERROR == u32Ret)
    
  //Check if Booster present according to entry of "osal.reg"
  CD_vReadRegistryValues(pShMem);
  CD_TRACE_LEAVE_U2(CDID_CD_u32Init, u32Ret, pShMem, hSem, 0, 0);

  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CD_u32Destroy
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  Endpoint of SCSI interface
 * HISTORY:
 ******************************************************************************/
tU32 CD_u32Destroy()
{
  tU32 u32Ret = OSAL_E_NOERROR;

  if(grMainData.pSharedMem != NULL)
  {
    (void)u32MainDestroy(grMainData.pSharedMem);
    (void)CDCTRL_u32Destroy(grMainData.pSharedMem);
    (void)CDAUDIO_u32Destroy(grMainData.pSharedMem);
    (void)CD_SCSI_IF_u32Destroy(grMainData.pSharedMem);

    CD_vReleaseShMem(grMainData.pSharedMem, grMainData.hShMem,
                     OSAL_C_INVALID_HANDLE);
  } //if(pSharedMem != NULL)

  (void)OSAL_s32SemaphoreDelete(CD_SEM_NAME);
  (void)OSAL_s32SharedMemoryDelete(CD_SHMEM_NAME);

  OSAL_s32ThreadWait(5000);
  CD_vUnregTrace();

  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CD_vReadRegistryValues
 * PARAMETER:
 * RETURNVALUE:  None
 * DESCRIPTION:  Read Registry Value for:
 *                        
 *                Registry Key Name             Value       Description
 *        | "CD_LOW_VOLTAGE_HANDLING_THRESHOLD"| 0    | Low Voltage Handling
 *        |                                    |      | Threshold LOW
 *        |                                    | 1    | Low Voltage Handling 
 *        |                                    |      | Threshold LOW
 *        |                                    | 2    | Low Voltage Handling
 *        |                                    |      | Threshold CRITICAL LOW
 *       
 *                       
 * HISTORY:
 *        | Date      | Modification               | Author
 *        | 28/09/2016| This function was added    | boc7kor
 *        |           | as a fix for NCG3D-23434   |
 *        | 25/10/2016| Modified to make an error  | boc7kor
 *        |           | memory entry if registry   |
          |           | key is not configured      |
 ******************************************************************************/
static tVoid CD_vReadRegistryValues(tDevCDData *pShMem)
{
    OSAL_tIODescriptor    rRegistryIODescriptor = OSAL_ERROR;
    OSAL_trIOCtrlRegistry rIOCtrlRegistry       = { 0 };
    tU32                  u32RegistryValue      = 0x00000000;

    rRegistryIODescriptor = OSAL_IOOpen(
                    OSAL_C_STRING_DEVICE_REGISTRY"/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/DEVICES/DEV_CD/",
                    OSAL_EN_READONLY);

    if (rRegistryIODescriptor == OSAL_ERROR) 
    {
        CD_PRINTF_U2("CD_vReadRegistryValues() => OSAL_IOOpen(DEVICE_REGISTRY) failed with error code = %s",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
  
    }
    else
    {
      rIOCtrlRegistry.pcos8Name = (tCS8*)"CD_LOW_VOLTAGE_HANDLING_THRESHOLD";
      rIOCtrlRegistry.ps8Value  = (tU8*)&u32RegistryValue;
      rIOCtrlRegistry.u32Size   = sizeof(tS32);

      if (OSAL_s32IOControl(
          rRegistryIODescriptor,
          OSAL_C_S32_IOCTRL_REGGETVALUE,
          (tS32)&rIOCtrlRegistry) == OSAL_OK)
      {
        grMainData.u32LowVoltageHandlingThreshold=u32RegistryValue;
        CD_PRINTF_U2("CD_LOW_VOLTAGE_HANDLING_THRESHOLD set to %u\n",grMainData.u32LowVoltageHandlingThreshold);
        
      }
      else
      {
        CD_PRINTF_U2(
         "CD_vReadRegistryValues() => OSAL_s32IOControl(REGGETVALUE) failed with error code = %s",
           OSAL_coszErrorText(OSAL_u32ErrorCode()));
      }
      
      if(CD_LOW_VOLTAGE_HANDLING_THRESHOLD_NOT_CONFIGURED == 
                    grMainData.u32LowVoltageHandlingThreshold )
      {
        vWritePrintfErrmem("CD: \"CD_LOW_VOLTAGE_HANDLING_THRESHOLD\" is NOT "
           "configured in /opt/bosch/base/registry/osal.reg,"
               "switching to default behavior");
      }
    
      if (OSAL_s32IOClose(rRegistryIODescriptor) == OSAL_ERROR)
      {
        CD_PRINTF_U2(
            "CD_vReadRegistryValues() => OSAL_IOClose(DEVICE_REGISTRY) failed with error code = %s",
            OSAL_coszErrorText(OSAL_u32ErrorCode()));
      }
    }
}
/*****************************************************************************
 * FUNCTION:     CD_vSG_Check_Notify
 * PARAMETER:    pointer to shared mem
 * RETURNVALUE:  void
 * DESCRIPTION:  -Checks if the device node /dev/sgX can be opened
 *               -notifies client through call back if open is successful
 *                and stops watchdog timer 
 * HISTORY:
 * Date      | Modification               | Author
 * 28/11/2016| Added                      | boc7kor
 ******************************************************************************/
 static tVoid CD_vSG_Check_Notify(tDevCDData *pShMem)
 {
    int hSG;
    hSG = open(pShMem->rCD.rCDDrive.szDevSGName, O_RDWR | O_NONBLOCK);
    if(-1 != hSG)
    {
       if(OSAL_E_NOERROR == CD_SCSI_IF_u32FindCDDrive(pShMem))
       {
          u32SetDriveStatus(pShMem,
                 OSAL_C_U16_NOTI_DEVICE_READY,
                      OSAL_C_U16_DEVICE_READY);
          (void)OSAL_s32TimerSetTime(grMainData.hWatchdogTimer, 0, 0);
       }
       (void)close(hSG);
    }
    else
    {
       CD_PRINTF_ERRORS("CD_vNotifyIfSGAccessible: Error open device [%s]: [%s]\n",
                       pShMem->rCD.rCDDrive.szDevSGName, strerror(errno));
    }
 } 


static sem_t *initcd_lock;

tS32 cdctrl_drv_io_open(tS32 s32Id,
                        tCString szName,
                        OSAL_tenAccess enAccess,
                        tU32 *pu32FD,
                        tU16 app_id)
{
  (void)szName;
  (void)enAccess;
  (void)pu32FD;
  (void)app_id;
  return(tS32)CDCTRL_u32IOOpen(s32Id);
}

tS32 cdctrl_drv_io_close(tS32 s32ID, tU32 u32FD)
{
  (void)u32FD;
  return(tS32)CDCTRL_u32IOClose(s32ID);
}

tS32 cdctrl_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{
  (void)u32FD;
  return(tS32)CDCTRL_u32IOControl(s32ID, s32fun, s32arg);
}

tS32 cdaudio_drv_io_open(tS32 s32ID,
                         tCString szName,
                         OSAL_tenAccess enAccess,
                         tU32 *pu32FD,
                         tU16 app_id)
{
  (void)szName;
  (void)enAccess;
  (void)pu32FD;
  (void)app_id;
  return(tS32)CDAUDIO_u32IOOpen(s32ID);
}

tS32 cdaudio_drv_io_close(tS32 s32ID, tU32 u32FD)
{
  (void)u32FD;
  return(tS32)CDAUDIO_u32IOClose(s32ID);
}

tS32 cdaudio_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{
  (void)u32FD;
  return(tS32)CDAUDIO_u32IOControl(s32ID, s32fun, s32arg);
}

void __attribute__ ((constructor)) od_process_attach(void)
{
  tBool bFirstAttach = FALSE;

  initcd_lock = sem_open("CD_INIT",O_EXCL | O_CREAT, 0660, 0);
  if(initcd_lock != SEM_FAILED)
  {
    bFirstAttach = TRUE;
    CD_u32Init();
  }
  else
  {
    initcd_lock = sem_open("CD_INIT", 0);
    bFirstAttach = FALSE;
    sem_wait(initcd_lock);
  }
  if(bFirstAttach)
  {
  }
  else
  {
  }
  sem_post(initcd_lock);
}

#ifdef __cplusplus

}
#endif
/************************************************************************
 |end of file
 |-----------------------------------------------------------------------*/
