/************************************************************************
 | FILE:         cdaudio.c
 | PROJECT:      Gen3
 | SW-COMPONENT: OSAL I/O Device
 |------------------------------------------------------------------------
 | DESCRIPTION:
 |  This file contains the /dev/cdaudio device implementation
 |------------------------------------------------------------------------
 | COPYRIGHT:    (c) Bosch
 | HISTORY:
 | Date      | Modification
 | 2013      | GEN3 VW MIB                | srt2hi
 |************************************************************************/

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/
/* Basic OSAL includes */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"
#include "cd_funcenum.h"
#include "cd_main.h"
#include "cdaudio.h"
#include "cdaudio_cdtext.h"
#include "cdaudio_trace.h"
#include "cd_cache.h"
#include "cd_scsi_if.h"

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: module-local)
 |-----------------------------------------------------------------------*/
#define CDAUDIO_C_S32_IO_VERSION    (tS32)0x00010000   /*Read as v1.0.0*/

//#define CDAUDIO_ADDINFO_ALLBITS_VALUE               0xFFFFFFFFUL
//#define CDAUDIO_ADDINFO_NULL_VALUE                  0x00UL
#define CDAUDIO_ADDINFO_SIMPLE_AUDIO_CD_VALUE         0x00UL
#define CDAUDIO_ADDINFO_FIRST_TRACK_DATA_BITMASK      0x00000001UL

/* first parameter: pointer to OSAL_trAdditionalCDInfo*/
/*#define CDAUDIO_ADDINFO_IS_DATA_TRACK(_pAddInfo_, _TRACKNUMBER_)  \
(0 != ((_pAddInfo_)->u32DataTracks[(_TRACKNUMBER_) / 32] & \
(1UL << ((_TRACKNUMBER_) & 0x1FUL))))
 */
/* first parameter: pointer to OSAL_trAdditionalCDInfo*/
#define CDAUDIO_ADDINFO_IS_DATA_TRACK_NUMBER_1(_pAddInfo_) \
  (0 != ((_pAddInfo_)->u32DataTracks[0] & 0x02UL))

/* first parameter: pointer to OSAL_trAdditionalCDInfo*/
#define CDAUDIO_ADDINFO_SET_DATA_TRACK(_pAddInfo_, _TRACKNUMBER_) \
  (((_pAddInfo_)->u32DataTracks[(_TRACKNUMBER_) / 32] |=  \
  (1UL << ((_TRACKNUMBER_) & 0x1FUL))))

/* first parameter: pointer to OSAL_trAdditionalCDInfo*/
#define CDAUDIO_ADDINFO_CLR_DATA_TRACK(_pAddInfo_, _TRACKNUMBER_) \
  (((_pAddInfo_)->u32DataTracks[(_TRACKNUMBER_) / 32] &= \
  ~(1UL << ((_TRACKNUMBER_) & 0x1FUL))))

/************************************************************************
 |typedefs (scope: module-local)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable inclusion  (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: global)
 |-----------------------------------------------------------------------*/
/************************************************************************
 | variable definition (scope: module-local)
 |-----------------------------------------------------------------------*/

/*process data*/
static OSAL_tSemHandle g_hSem = OSAL_C_INVALID_HANDLE;
static OSAL_tShMemHandle g_hShMem = (OSAL_tShMemHandle)OSAL_C_INVALID_HANDLE;
static tDevCDData *g_pSharedMem = NULL; /*OSAL shared memory*/

/************************************************************************
 |function prototype (scope: module-local)
 |-----------------------------------------------------------------------*/
static void vZLBA2MSF(tDevCDData *pShMem, tU32 u32ZLBA, CD_sTMSF_type *pMSF);
/************************************************************************
 |function prototype (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function implementation (scope: module-local)
 |-----------------------------------------------------------------------*/
/*****************************************************************************
 * FUNCTION:     vMainClear
 * PARAMETER:    DriveIndex
 * RETURNVALUE:  void
 * DESCRIPTION:  clear internal states (e.g. at startup and CD Eject)
 ******************************************************************************/
static void vMainClear(tDevCDData *pShMem)
{
  CD_CHECK_SHARED_MEM_VOID(pShMem);

  pShMem->rCDAUDIO.bPlayInfoValid = FALSE;
  /* Set the cd driver in INITIALIZED state */
  pShMem->rCDAUDIO.u8Status = CDAUDIO_STATUS_INITIALIZED;
  pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA = CDAUDIO_U32NONE;
  pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA = CDAUDIO_U32NONE;
  pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32PrevZLBA = CDAUDIO_U32NONE;
}

/*****************************************************************************
 * FUNCTION:     vCalculateNewPlayrange
 * PARAMETER:    IN: DriveIndex
 *               IN: bBackwards: TRUE = FastBackward-Mode
 * RETURNVALUE:  void
 * DESCRIPTION:  sets current playrange
 ******************************************************************************/
static tVoid vCalculateNewPlayrange(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                                    tBool bScanMode, tBool bBackWards)
{
  CD_CHECK_SHARED_MEM_VOID(pShMem);

  CDAUDIO_TRACE_ENTER_U4(CDID_vCalculateNewPlayrange, bScanMode, bBackWards, 0,
                         0);
  //calculate ZLBA range from T-Offset-Range
  {
    tU16 u16O;
    tU8 u8T;
    OSAL_trPlayRange *prNR; //new range
    CDAUDIO_rPlayrange_type rRZLBA; //range as ZLBA
    prNR = &pShMem->rCDAUDIO.rNewPlayRange;

    if(bBackWards)
    { //scan backward (backward is never normal play speed)
      u8T = prNR->rStartAdr.u8Track;
      if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
      { // scan BWD until current position - NOT SUPPORTED
        CDAUDIO_PRINTF_ERRORS("STARTTRACK==NONE - not supported");
        rRZLBA.u32StartZLBA = CD_u32GetStartOfTrackZLBA(pShMem, hSem, 1);
      }
      else //if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
      {
        //scan bwd untilgiven Track+Offset (mostly start of == Track + 0secs)
        rRZLBA.u32StartZLBA = CD_u32TS2ZLBA(pShMem, hSem,
                                            prNR->rStartAdr.u8Track,
                                            prNR->rStartAdr.u16Offset);
      } //else //if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)

      u8T = prNR->rEndAdr.u8Track;
      if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
      { // scan bwd from recent play position, ignore offset
        //SCSI SCAN don't support scan from current position,
        // therefore use last known playtime instead
        //get last play position if available
        if(pShMem->rCDAUDIO.bPlayInfoValid)
        { //valid position
          rRZLBA.u32EndZLBA = pShMem->rCDAUDIO.u32ZLBA;
        }
        else //if(pShMem->rCDAUDIO.bPlayInfoValid)
        {
          rRZLBA.u32EndZLBA = CDAUDIO_U32NONE; //no valid POSITION
        } //else //if(pShMem->rCDAUDIO.bPlayInfoValid)
      }
      else //if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
      {
        //start scan bwd at at given track
        u16O = prNR->rEndAdr.u16Offset;
        if(u16O == OSAL_C_U16_CDAUDIO_OFFSET_TRACK_END)
        { //scan bwd from End of Track
          rRZLBA.u32EndZLBA = CD_u32GetEndOfTrackZLBA(pShMem, hSem,
                                                      prNR->rEndAdr.u8Track);
        }
        else //if(u16O == OSAL_C_U16_CDAUDIO_OFFSET_TRACK_END)
        {
          //scan bwd starting given Track+offset
          rRZLBA.u32EndZLBA = CD_u32TS2ZLBA(pShMem, hSem,
                                            prNR->rEndAdr.u8Track,
                                            prNR->rEndAdr.u16Offset);
        } //else //if(u16O == OSAL_C_U16_CDAUDIO_OFFSET_TRACK_END)
      } //else //if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
    }
    else //if(bBackWards)
    {
      //play or scan  forward
      u8T = prNR->rStartAdr.u8Track;
      if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
      { // play fwd from current position, ignore offset
        //SCSI SCAN don't support scan from current position,
        // therefore use last known playtime instead
        if(bScanMode)
        { //scan
          rRZLBA.u32StartZLBA = CDAUDIO_U32NONE;
          if(pShMem->rCDAUDIO.bPlayInfoValid)
          {
            rRZLBA.u32StartZLBA = pShMem->rCDAUDIO.u32ZLBA;
            rRZLBA.u32PrevZLBA = pShMem->rCDAUDIO.u32ZLBA;
          }
        }
        else //if(bScanMode)
        {
          //normal play
          rRZLBA.u32StartZLBA = CDAUDIO_U32NONE;
          if(pShMem->rCDAUDIO.bPlayInfoValid)
          {
            rRZLBA.u32PrevZLBA = pShMem->rCDAUDIO.u32ZLBA;
          }
        } //else //if(bScanMode)
      }
      else //if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
      {
        //play fwd from given Track+Offset
        rRZLBA.u32StartZLBA = CD_u32TS2ZLBA(pShMem, hSem,
                                            prNR->rStartAdr.u8Track,
                                            prNR->rStartAdr.u16Offset);
      } //else //if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)

      u8T = prNR->rEndAdr.u8Track;
      if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
      { // play whole CD - NOT SUPPORTED
        CDAUDIO_PRINTF_ERRORS("ENDTRACK==NONE -"
                              " playing whole CD not supported");
        rRZLBA.u32EndZLBA = CD_u32GetEndOfCDZLBA(pShMem);
      }
      else //if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
      {
        //play until given Track
        u16O = prNR->rEndAdr.u16Offset;
        if(u16O == OSAL_C_U16_CDAUDIO_OFFSET_TRACK_END)
        { //play until End of Track
          rRZLBA.u32EndZLBA = CD_u32GetEndOfTrackZLBA(pShMem, hSem,
                                                      prNR->rEndAdr.u8Track);
        }
        else //if(u16O == OSAL_C_U16_CDAUDIO_OFFSET_TRACK_END)
        {
          //play until given seconds are reached
          rRZLBA.u32EndZLBA = CD_u32TS2ZLBA(pShMem, hSem,
                                            prNR->rEndAdr.u8Track,
                                            prNR->rEndAdr.u16Offset);
        } //else //if(u16O == OSAL_C_U16_CDAUDIO_OFFSET_TRACK_END)
      } //else //if(u8T == OSAL_C_U8_CDAUDIO_TRACK_NONE)
    } //else //if(bBackWards)
    pShMem->rCDAUDIO.rCurrentPlayRangeZLBA = rRZLBA;
  } //if(bNewR)

  CDAUDIO_TRACE_LEAVE_U4(CDID_vCalculateNewPlayrange, OSAL_E_NOERROR, 0,
                         pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA,
                         pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA,
                         bBackWards);
}

/*****************************************************************************
 * FUNCTION:     u32StartWithMode
 * PARAMETER:    IN: playmode
 * RETURNVALUE:  OSAL Erro code
 * DESCRIPTION:  start a new play mode
 ******************************************************************************/
static tU32 u32StartWithMode(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                             tU8 u8PlayMode)
{
  tU32 u32RetVal = OSAL_E_UNKNOWN;
  tU32 u32StartZLBA;
  tU32 u32EndZLBA;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CDAUDIO_TRACE_ENTER_U4(CDID_u32StartWithMode, 0, u8PlayMode, 0, 0);

  switch(u8PlayMode)
  {
  case CDAUDIO_STATUS_PLAY:
    {
      tU16 u16SO; // start offset in seconds
      tBool bPlayTNO = FALSE;
      tU8 u8ST;
      tU8 u8ET;

      vCalculateNewPlayrange(pShMem, hSem, FALSE, FALSE);
      u32StartZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA;
      u32EndZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA;
      u8ST = pShMem->rCDAUDIO.rNewPlayRange.rStartAdr.u8Track;
      u8ET = pShMem->rCDAUDIO.rNewPlayRange.rEndAdr.u8Track;

      u16SO = pShMem->rCDAUDIO.rNewPlayRange.rStartAdr.u16Offset;
      if(u8ST != OSAL_C_U8_CDAUDIO_TRACK_NONE && 0 == u16SO)
      { //case: start playing at start of track (offset == 0, index=1)
        u32RetVal = CD_SCSI_IF_u32PlayTNO(pShMem, u8ST, u8ET);
        bPlayTNO = OSAL_E_NOERROR == u32RetVal;
      } //if(u8ST != 0xFF && 0 == u16SO)

      if(!bPlayTNO)
      { //case: start playing in middle of track
        // (use case: resume from pause, turn on radio
        // or PlayTNO failed
        //case: continue playing until end of track
        // (use case: switch playmode [repeat, shuffle])
        //special case: start == End && LastTrack:
        // SCSI play will be successfully, but do not play and
        // will not notify "Completed"
        if(u32StartZLBA == u32EndZLBA)
        {
          u32RetVal = CD_SCSI_IF_u32PlayRange(pShMem, u32StartZLBA - 1,
                                              u32EndZLBA);
        }
        else //if(u32StartZLBA == u32EndZLBA)
        {
          u32RetVal = CD_SCSI_IF_u32PlayRange(pShMem, u32StartZLBA, u32EndZLBA);
        } //else //if(u32StartZLBA == u32EndZLBA)
      } //if(!bPlayTNO)
    }
    break;

  case CDAUDIO_STATUS_SCAN_FWD:
    vCalculateNewPlayrange(pShMem, hSem, TRUE, FALSE);
    u32StartZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA;
    u32RetVal = CD_SCSI_IF_u32Scan(pShMem, u32StartZLBA, FALSE);
    break;

  case CDAUDIO_STATUS_SCAN_BWD:
    vCalculateNewPlayrange(pShMem, hSem, TRUE, TRUE);
    u32EndZLBA = pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA;
    u32RetVal = CD_SCSI_IF_u32Scan(pShMem, u32EndZLBA, TRUE);
    break;

  default:
    CDAUDIO_PRINTF_ERRORS("UNKNOWN PLAYMODE %u", (unsigned int)u8PlayMode);
    u32RetVal = OSAL_E_INVALIDVALUE;
    break;
  } //switch (u8PlayState)

  if(u32RetVal == OSAL_E_NOERROR)
  {
    (void)CD_u8SwitchState(pShMem, u8PlayMode);
    (void)CD_u32PostMainEvent(pShMem, CD_MAIN_EVENT_MASK_SUBCHANNEL_FAKE);
  }

  CDAUDIO_TRACE_LEAVE_U4(CDID_u32StartWithMode, u32RetVal, 0, u8PlayMode, 0, 0);

  return u32RetVal;
}

/*****************************************************************************
 * FUNCTION:     bPlayRangeIsValid
 * PARAMETER:    IN: OSAL_trPlayRange
 * RETURNVALUE:  TRUE: valid, FALSE: invalid
 * DESCRIPTION:  checks playrange
 ******************************************************************************/
static tBool bPlayRangeIsValid(tDevCDData *pShMem,
                               const OSAL_trPlayRange* prRange)
{
  tBool bIsValid = TRUE;

  CD_CHECK_SHARED_MEM(pShMem, (tBool)FALSE);

  CDAUDIO_TRACE_ENTER_U4(CDID_bPlayRangeIsValid, prRange, 0, 0, 0);

  if(NULL != prRange)
  {
    if((prRange->rStartAdr.u8Track != OSAL_C_U8_CDAUDIO_TRACK_NONE)
       && (prRange->rEndAdr.u8Track != OSAL_C_U8_CDAUDIO_TRACK_NONE))
    {
      if((prRange->rStartAdr.u8Track > prRange->rEndAdr.u8Track)
         || ((prRange->rStartAdr.u8Track == prRange->rEndAdr.u8Track)
             && (prRange->rStartAdr.u16Offset >= prRange->rEndAdr.u16Offset)))
      {
        bIsValid = FALSE;
      } //if tracks ==, || offset != 
    } //if tracks !=
  }
  else //if(NULL != prRange)
  {
    bIsValid = FALSE;
  } //else //if(NULL != prRange)
  CDAUDIO_TRACE_LEAVE_U4(CDID_bPlayRangeIsValid,
                         bIsValid ? OSAL_E_NOERROR : OSAL_E_INVALIDVALUE,
                         bIsValid, 0, 0, 0);

  return bIsValid;
}

/*****************************************************************************
 * FUNCTION:     CDAUDIO_u32GetAdditionalCDInfo
 * PARAMETER:    DriveIndex
 *               OUT: filled struct
 * RETURNVALUE:  OSAl error code
 * DESCRIPTION:  retrieves cd infos
 ******************************************************************************/
tU32 CDAUDIO_u32GetAdditionalCDInfo(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                                    OSAL_trAdditionalCDInfo *prInfo)
{
  tU32 u32RetVal;
  tU8 u8Track;
  tU8 u8MinTrack = 0;
  tU8 u8MaxTrack = 0;
  tU32 u32LastZLBA = 0;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  CDAUDIO_TRACE_ENTER_U4(CDID_CDAUDIO_u32GetAdditionalCDInfo, 0, prInfo, 0, 0);

  (void)u32LastZLBA;

  u32RetVal = CD_u32CacheGetCDInfo(pShMem, hSem, &u8MinTrack, &u8MaxTrack,
                                   &u32LastZLBA);

  if(OSAL_E_NOERROR == u32RetVal)
  {
    memset(prInfo, 0, sizeof(OSAL_trAdditionalCDInfo));
    for(u8Track = u8MinTrack; u8Track <= u8MaxTrack; ++u8Track)
    {
      tU32 u32SZLBA = 0;
      tU32 u32Ctrl = 0;
      tBool bIsData;
      (void)CD_u32CacheGetTrackInfo(pShMem, hSem, u8Track, &u32SZLBA, &u32Ctrl);

      bIsData = (u32Ctrl & CD_TYPE_MASK) == CD_TYPE_DATA ? TRUE : FALSE;

      if(bIsData)
      {
        CDAUDIO_ADDINFO_SET_DATA_TRACK(prInfo, u8Track);
      }
      else //if(bIsData)
      {
        CDAUDIO_ADDINFO_CLR_DATA_TRACK(prInfo, u8Track);
      } //else if(bIsData)
    } //for (u8Track = u8MinTrack; u8Track <= u8MaxTrack; ++u8Track)

    prInfo->u32InfoBits = CDAUDIO_ADDINFO_SIMPLE_AUDIO_CD_VALUE;

    if(CDAUDIO_ADDINFO_IS_DATA_TRACK_NUMBER_1(prInfo))
    {
      prInfo->u32InfoBits |= CDAUDIO_ADDINFO_FIRST_TRACK_DATA_BITMASK;
    } //if(CDAUDIO_ADDINFO_IS_DATA_TRACK(prInfo, 1)
  } //if(OSAL_E_NOERROR == u32RetVal)

  CDAUDIO_TRACE_LEAVE_U4(CDID_CDAUDIO_u32GetAdditionalCDInfo, OSAL_E_NOERROR, 0,
                         0, 0, 0);

  return u32RetVal;
}

/*****************************************************************************
 * FUNCTION:     vZLBA2MSF
 * PARAMETER:    IN: ZLBA
 *               OUT: Pointer to struct MSF
 * RETURNVALUE:  void, MSF
 * DESCRIPTION:  calculates MSF from ZLBA
 ******************************************************************************/
static void vZLBA2MSF(tDevCDData *pShMem, tU32 u32ZLBA, CD_sTMSF_type *pMSF)
{
  tU32 u32Second;

  CD_CHECK_SHARED_MEM_VOID(pShMem);

  CDAUDIO_TRACE_ENTER_U4(CDID_vZLBA2MSF, u32ZLBA, pMSF, 0, 0);

  u32Second = u32ZLBA / CD_SECTORS_PER_SECOND;

  if(NULL != pMSF)
  {
    pMSF->u8Min = (tU8)(u32Second / 60);
    pMSF->u8Sec = (tU8)(u32Second % 60);
    pMSF->u8Frm = (tU8)(u32ZLBA % CD_SECTORS_PER_SECOND);

    CDAUDIO_TRACE_LEAVE_U4(CDID_vZLBA2MSF, OSAL_E_NOERROR, u32ZLBA, pMSF->u8Sec,
                           pMSF->u8Frm, pMSF->u8Frm);
  }
  else  //if(NULL != pMSF)
  {
    CDAUDIO_TRACE_LEAVE_U4(CDID_vZLBA2MSF, OSAL_E_INVALIDVALUE, u32ZLBA, 0, 0,
                           0);
  } //else  //if(NULL != pMSF)
}

/************************************************************************
 |function implementation (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 * FUNCTIONS                                                             *
 *      CDAUDIO_u32IOOpen                                                *
 *                                                                       *
 * DESCRIPTION                                                           *
 *      Open cdaudio required resources                                  *
 *                                                                       *
 * CALLS                                                                 *
 *      LLD Atapi API                                                    *
 *                                                                       *
 * INPUTS                                                                *
 *      tU32 u32DriveID: Drive identificator                             *
 *                                                                       *
 * OUTPUTS                                                               *
 *      tS32: Error Code                                                 *
 ************************************************************************/
tU32 CDAUDIO_u32IOOpen(tS32 s32Dummy)
{
  tU32 u32Result;

  CDAUDIO_TRACE_ENTER_U1_FORCED(CDID_CDAUDIO_u32IOOpen, s32Dummy, 0, 0, 0);

  u32Result = CD_u32GetShMem(&g_pSharedMem, &g_hShMem, &g_hSem);

  //clean up if failed
  if(OSAL_E_NOERROR != u32Result)
  {
    (void)CDAUDIO_u32IOClose(0);
  } //if(u32Result == OSAL_ERROR)

  CDAUDIO_TRACE_LEAVE_U1_FORCED(CDID_CDAUDIO_u32IOOpen, u32Result, 0, 0, 0, 0);
  return u32Result;
}

/************************************************************************
 *                                                                       *
 * FUNCTIONS                                                             *
 *                                                                       *
 *     CDAUDIO_u32IOClose                                                *
 *                                                                       *
 * DESCRIPTION                                                           *
 *     Close cdaudio required device                                     *
 *                                                                       *
 * CALLS                                                                 *
 *                                                                       *
 *      LLD Atapi API                                                    *
 *                                                                       *
 * INPUTS                                                                *
 *                                                                       *
 *      tU32 u32DriveID: Drive identificator                             *
 *                                                                       *
 * OUTPUTS                                                               *
 *                                                                       *
 *      tS32: Error code                                                 *
 *                                                                       *
 *HISTORY      :                                                         *
 *                                                                       *
 *               05.07.06  Venkateswara.N (RBIN/ECM1)                    *
 *               Added the code for TSIM                                 *
 ************************************************************************/
tU32 CDAUDIO_u32IOClose(tS32 s32Dummy)
{
  tU32 u32Result = OSAL_E_NOERROR;
  CDAUDIO_TRACE_ENTER_U1_FORCED(CDID_CDAUDIO_u32IOClose, s32Dummy, 2, 3, 4);

  CD_vReleaseShMem(g_pSharedMem, g_hShMem, g_hSem);

  g_pSharedMem = NULL;
  g_hShMem = OSAL_ERROR;
  g_hSem = OSAL_C_INVALID_HANDLE;

  CDAUDIO_TRACE_LEAVE_U1_FORCED(CDID_CDAUDIO_u32IOClose, u32Result, s32Dummy,
                                11, 12, 13);
  return u32Result;
}

/************************************************************************
 *                                                                       *
 * FUNCTIONS                                                             *
 *                                                                       *
 *     CDAUDIO_u32IOControl                                              *
 *                                                                       *
 * DESCRIPTION                                                           *
 *     Call a cdaudio control function                                   *
 *                                                                       *
 * CALLS                                                                 *
 *                                                                       *
 *      Is only a wrapper to s32[Atapi|Hdd|AtaNEC]IOControl              *
 *                                                                       *
 * INPUTS                                                                *
 *                                                                       *
 *       tU32 u32DriveID: Drive identificator                            *
 *       tS32 s32Fun:               Function identificator               *
 *       tS32 s32Arg:               Argument to be passed to function    *
 *                                                                       *
 * OUTPUTS                                                               *
 *                                                                       *
 *      tS32: Error code                                                 *
 *                                                                       *
 *HISTORY      :                                                         *
 *                                                                       *
 *               05.07.06  Venkateswara.N (RBIN/ECM1)                    *
 *               Added the code for TSIM                                 *
 ************************************************************************/
tU32 CDAUDIO_u32IOControl(tS32 s32Dummy, tS32 s32Fun, tS32 s32Arg)
{
  tU32 u32Result = OSAL_E_NOERROR;
  tDevCDData *pShMem = g_pSharedMem;
  OSAL_tSemHandle hSem = g_hSem;

  (void)s32Dummy;

  CDAUDIO_TRACE_ENTER_U4(CDID_CDAUDIO_u32IOControl, 0, s32Fun, s32Arg, 0);

  CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_IOCONTROL, 0, s32Fun, s32Arg, 0);

  switch(s32Fun)
  {
  case OSAL_C_S32_IOCTRL_VERSION:
    {
      tS32* ps32Version = (tS32*)s32Arg;

      if(ps32Version == NULL)
      {
        u32Result = OSAL_E_INVALIDVALUE;
      }
      else
      {
        *ps32Version = CDAUDIO_C_S32_IO_VERSION;
      } //if (ps32Version == NULL)
    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_PLAY:
    {
      u32Result = u32StartWithMode(pShMem, hSem, CDAUDIO_STATUS_PLAY);

      /*switch(pShMem->rCDAUDIO.u8Status)
      {
      case CDAUDIO_STATUS_INITIALIZED:
      case CDAUDIO_STATUS_PLAY:
      case CDAUDIO_STATUS_PAUSED:
      case CDAUDIO_STATUS_PAUSED_BY_USER:
      case CDAUDIO_STATUS_SCAN_FWD:
      case CDAUDIO_STATUS_SCAN_BWD:
      case CDAUDIO_STATUS_UNDERVOLTAGE_ERROR:
        u32Result = u32StartWithMode(pShMem, hSem, CDAUDIO_STATUS_PLAY);
        break;
      case CDAUDIO_STATUS_ERROR:
        u32Result = OSAL_E_NOTINITIALIZED;
        break;
      default:
        u32Result = OSAL_E_UNKNOWN;
      } //switch(pShMem->rCDAUDIO.u8Status)
      */
    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_STOP:
    switch(pShMem->rCDAUDIO.u8Status)
    {
    case CDAUDIO_STATUS_PLAY:
    case CDAUDIO_STATUS_SCAN_FWD:
    case CDAUDIO_STATUS_SCAN_BWD:
      u32Result = CD_SCSI_IF_u32Stop(pShMem);
      if(u32Result == OSAL_E_NOERROR)
      {
        (void)CD_u8SwitchState(pShMem, CDAUDIO_STATUS_STOP);
      }
      break;
    case CDAUDIO_STATUS_ERROR:
    case CDAUDIO_STATUS_PAUSED:
    case CDAUDIO_STATUS_PAUSED_BY_USER:
    case CDAUDIO_STATUS_INITIALIZED:
    case CDAUDIO_STATUS_UNDERVOLTAGE_ERROR:
    default:
      ;
    } //switch(pShMem->rCDAUDIO.u8Status)
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_PAUSE:
    switch(pShMem->rCDAUDIO.u8Status)
    {
    case CDAUDIO_STATUS_PLAY:
    case CDAUDIO_STATUS_SCAN_FWD:
    case CDAUDIO_STATUS_SCAN_BWD:
      u32Result = CD_SCSI_IF_u32PauseResume(pShMem, FALSE);
      if(u32Result == OSAL_E_NOERROR)
      {
        (void)CD_u8SwitchState(pShMem, CDAUDIO_STATUS_PAUSED_BY_USER);
      }
      break;
    case CDAUDIO_STATUS_ERROR:
    case CDAUDIO_STATUS_PAUSED:
    case CDAUDIO_STATUS_PAUSED_BY_USER:
    case CDAUDIO_STATUS_INITIALIZED:
    case CDAUDIO_STATUS_UNDERVOLTAGE_ERROR:
    default:
      u32Result = OSAL_E_UNKNOWN;
    } //switch(pShMem->rCDAUDIO.u8Status)
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_FASTFORWARD:
    switch(pShMem->rCDAUDIO.u8Status)
    {
    case CDAUDIO_STATUS_STOP:
    case CDAUDIO_STATUS_PAUSED:
    case CDAUDIO_STATUS_PAUSED_BY_USER:
    case CDAUDIO_STATUS_PLAY:
    case CDAUDIO_STATUS_SCAN_FWD:
    case CDAUDIO_STATUS_SCAN_BWD:
    case CDAUDIO_STATUS_UNDERVOLTAGE_ERROR:
      u32Result = u32StartWithMode(pShMem, hSem, CDAUDIO_STATUS_SCAN_FWD);
      break;
    case CDAUDIO_STATUS_ERROR:
    default:
      u32Result = OSAL_E_UNKNOWN;
    } //switch(pShMem->rCDAUDIO.u8Status)
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_FASTBACKWARD:
    switch(pShMem->rCDAUDIO.u8Status)
    {
    case CDAUDIO_STATUS_STOP:
    case CDAUDIO_STATUS_PAUSED:
    case CDAUDIO_STATUS_PAUSED_BY_USER:
    case CDAUDIO_STATUS_PLAY:
    case CDAUDIO_STATUS_SCAN_FWD:
    case CDAUDIO_STATUS_SCAN_BWD:
    case CDAUDIO_STATUS_UNDERVOLTAGE_ERROR:
      u32Result = u32StartWithMode(pShMem, hSem, CDAUDIO_STATUS_SCAN_BWD);
      break;
    case CDAUDIO_STATUS_ERROR:
    default:
      u32Result = OSAL_E_UNKNOWN;
    } //switch(pShMem->rCDAUDIO.u8Status)
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_RESUME:
    {
      u32Result = OSAL_E_NOTSUPPORTED;
    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_SETPLAYRANGE:
    {
      OSAL_trPlayRange* prRange = (OSAL_trPlayRange*)s32Arg;

      if(prRange == NULL)
      {
        u32Result = OSAL_E_INVALIDVALUE;
      }
      else
      {
        if(bPlayRangeIsValid(pShMem, prRange))
        {
          pShMem->rCDAUDIO.rNewPlayRange = *prRange;
          //pShMem->rCDAUDIO.bHaveNewPlayRange = TRUE;
          CDAUDIO_TRACE_IOCTRL(
                              CDAUDIO_EN_SETPLAYRANGE,
                              0,
                              (tU32)prRange->rStartAdr.u8Track << 16 
                              | (tU32)prRange->rStartAdr.u16Offset,
                              (tU32)prRange->rEndAdr.u8Track << 16 
                              | (tU32)prRange->rEndAdr.u16Offset,
                              0);
        }
        else
        {
          u32Result = OSAL_E_INVALIDVALUE;
        }
      }
    }
    break;
  case OSAL_C_S32_IOCTRL_CDAUDIO_SETMFS:
    {
      OSAL_trAdrRange* prRange = (OSAL_trAdrRange*)s32Arg;

      if(prRange == NULL)
      {
        u32Result = OSAL_E_INVALIDVALUE;
      }
      else
      {
        u32Result = OSAL_E_NOTSUPPORTED;

        CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_SETMSF,
                              0,
                              (tU32)prRange->rStartAdr.u8MSFMinute << 16 
                              | (tU32)prRange->rStartAdr.u8MSFSecond << 8
                              | (tU32)prRange->rStartAdr.u8MSFFrame,
                              (tU32)prRange->rEndAdr.u8MSFMinute << 16 
                              | (tU32)prRange->rEndAdr.u8MSFSecond << 8
                              | (tU32)prRange->rEndAdr.u8MSFFrame,
                              0);
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_GETCDINFO:
    {
      OSAL_trCDAudioInfo* prInfo = (OSAL_trCDAudioInfo*)s32Arg;

      if(prInfo == NULL)
      {
        u32Result = OSAL_E_INVALIDVALUE;
      }
      else //if(prInfo == NULL)
      {
          tU32 u32Ret;
          tU8 u8MinTrack = 0;
          tU8 u8MaxTrack = 0;
          tU32 u32LastZLBA = 0;
          tU8 u8CDTextAvailable;

          //check, if cd-text exist on CD
          u32Ret = CDAUDIO_u32GetText(pShMem, hSem, 0, NULL, NULL, 0);
          u8CDTextAvailable = OSAL_E_NOERROR == u32Ret ? 1 : 0;

          u32Result = CD_u32CacheGetCDInfo(pShMem, hSem, &u8MinTrack,
                                           &u8MaxTrack, &u32LastZLBA);

          if(OSAL_E_NOERROR == u32Result)
          {
            CD_sTMSF_type rTMSF =
            { 0};

            prInfo->u32CdText = (tU32)u8CDTextAvailable;
            prInfo->u32MinTrack = u8MinTrack;
            prInfo->u32MaxTrack = u8MaxTrack;
            vZLBA2MSF(pShMem, u32LastZLBA, &rTMSF);
            prInfo->u32TrMinutes = rTMSF.u8Min;
            prInfo->u32TrSeconds = rTMSF.u8Sec;
          }
          else //if(OSAL_E_NOERROR == u32Result)
          {
            u32Result = OSAL_E_UNKNOWN;
          } //else //if(OSAL_E_NOERROR == u32Result)

          CDAUDIO_TRACE_IOCTRL(
                              CDAUDIO_EN_GETCDINFO, 0,
                              (tU32)prInfo->u32MinTrack << 16 
                              | (tU32)prInfo->u32MaxTrack,
                              (tU32)prInfo->u32TrMinutes << 16 
                              | (tU32)prInfo->u32TrSeconds,
                              prInfo->u32CdText);
      } //else //if(prInfo == NULL)
    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_GETADDITIONALCDINFO:
    {
      if((void*)s32Arg == NULL)
      {
        u32Result = OSAL_E_INVALIDVALUE;
      }
      else //if(s32Arg == NULL)
      {
          OSAL_trAdditionalCDInfo* prAdditionalInfo =
          (OSAL_trAdditionalCDInfo*)s32Arg;

          u32Result = CDAUDIO_u32GetAdditionalCDInfo(pShMem, hSem,
                                                     prAdditionalInfo);

          CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_GETADDITIONALCDINFO_INFO, 0,
                               (tU32)prAdditionalInfo->u32InfoBits, 0, 0);
          CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_GETADDITIONALCDINFO_TRACKS,
                               (tU32)prAdditionalInfo->u32DataTracks[0],
                               (tU32)prAdditionalInfo->u32DataTracks[1],
                               (tU32)prAdditionalInfo->u32DataTracks[2],
                               (tU32)prAdditionalInfo->u32DataTracks[3]);
      } //else //if(s32Arg == NULL)
      break;
    }

  case OSAL_C_S32_IOCTRL_CDAUDIO_GETPLAYINFO:
    {
      OSAL_trPlayInfo* prPlayInfo = (OSAL_trPlayInfo*)s32Arg;
      tU16 u16Status;

      u16Status = CD_u16GetDriveStatus(pShMem, OSAL_C_U16_NOTI_MEDIA_STATE);
      if(u16Status == OSAL_C_U16_MEDIA_READY)
      {
        if(prPlayInfo == NULL)
        {
          u32Result = OSAL_E_INVALIDVALUE;
        }
        else //} //if (prPlayInfo == NULL)
        {
            if(pShMem->rCDAUDIO.bPlayInfoValid)
            {
              *prPlayInfo = pShMem->rCDAUDIO.rPlayInfo;
              prPlayInfo->u32StatusPlay = CD_u32GetPlayStatus(pShMem);
              u32Result = OSAL_E_NOERROR;
            }
            else //if (gabPlayInfoValid)
            {
              //u32Result = OSAL_E_TIMEOUT;
              u32Result = OSAL_E_MEDIA_NOT_AVAILABLE;
            } //else //if (gabPlayInfoValid)

            CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_GETPLAYINFO,
                              0,
                              (tU32)prPlayInfo->u32TrackNumber << 16 
                              | (tU32)prPlayInfo->u32StatusPlay,
                              (tU32)prPlayInfo->rAbsTrackAdr.u8MSFMinute << 16 
                              | (tU32)prPlayInfo->rAbsTrackAdr.u8MSFSecond << 8
                              | (tU32)prPlayInfo->rAbsTrackAdr.u8MSFFrame,
                              (tU32)prPlayInfo->rRelTrackAdr.u8MSFMinute << 16 
                              | (tU32)prPlayInfo->rRelTrackAdr.u8MSFSecond << 8
                              | (tU32)prPlayInfo->rRelTrackAdr.u8MSFFrame);
        } //if (prInfo == NULL)
      }
      else //if(u16Status == OSAL_C_U16_MEDIA_READY)
      {
        u32Result = OSAL_E_MEDIA_NOT_AVAILABLE;
      } //else //if(u16Status == OSAL_C_U16_MEDIA_READY)
    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO:
    {
      OSAL_trTrackInfo* prInfo = (OSAL_trTrackInfo*)s32Arg;
      tU16 u16Status;

      u16Status = CD_u16GetDriveStatus(pShMem, OSAL_C_U16_NOTI_MEDIA_STATE);
      if(u16Status == OSAL_C_U16_MEDIA_READY)
      {
        if(prInfo != 0)
        {
          tU8 u8MinTrack = 0;
          tU8 u8MaxTrack = 0;
          u32Result = CD_u32CacheGetCDInfo(pShMem, hSem, &u8MinTrack,
                                           &u8MaxTrack,
                                           NULL);
          if(OSAL_E_NOERROR == u32Result)
          {
            tU8 u8Track = (tU8)prInfo->u32TrackNumber;
            if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
            {
              tU32 u32SZLBA;
              tU32 u32EZLBA;
              tU32 u32Ctrl;
              CD_sTMSF_type rSTMSF =
              { 0};
              CD_sTMSF_type rETMSF =
              { 0};

              u32Result = CD_u32CacheGetTrackInfo(pShMem, hSem, u8Track,
                                                  &u32SZLBA, &u32Ctrl);
              u32EZLBA = CD_u32GetEndOfTrackZLBA(pShMem, hSem, u8Track);

              vZLBA2MSF(pShMem, u32SZLBA, &rSTMSF);
              vZLBA2MSF(pShMem, u32EZLBA, &rETMSF);

              prInfo->rStartAdr.u8MSFMinute = rSTMSF.u8Min;
              prInfo->rStartAdr.u8MSFSecond = rSTMSF.u8Sec;
              prInfo->rStartAdr.u8MSFFrame = rSTMSF.u8Frm;
              prInfo->rEndAdr.u8MSFMinute = rETMSF.u8Min;
              prInfo->rEndAdr.u8MSFSecond = rETMSF.u8Sec;
              prInfo->rEndAdr.u8MSFFrame = rETMSF.u8Frm;
              prInfo->u32TrackControl = u32Ctrl;

              CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_GETTRACKINFO,
                                  0,
                                  (tU32)u8Track << 16 | prInfo->u32TrackControl,
                                  (tU32)prInfo->rStartAdr.u8MSFMinute << 16 
                                  | (tU32)prInfo->rStartAdr.u8MSFSecond << 8
                                  | (tU32)prInfo->rStartAdr.u8MSFFrame,
                                  (tU32)prInfo->rEndAdr.u8MSFMinute << 16 
                                  | (tU32)prInfo->rEndAdr.u8MSFSecond << 8
                                  | (tU32)prInfo->rEndAdr.u8MSFFrame);
            }
            else //if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
            {
              u32Result = OSAL_E_INVALIDVALUE;
            } //else //if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
          } //if(OSAL_E_NOERROR == u32Result)
        }
        else
        {
          u32Result = OSAL_E_INVALIDVALUE;
        }
      }
      else //if(u16Status == OSAL_C_U16_MEDIA_READY)
      {
        u32Result = OSAL_E_MEDIA_NOT_AVAILABLE;
      } //else //if(u16Status == OSAL_C_U16_MEDIA_READY)
    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_GETALBUMNAME: /* CD-TEXT command */
    {
      tU8 *pu8Txt;
      tU32 u32Ret;
      pu8Txt = (tU8*)s32Arg;
      u32Ret = CDAUDIO_u32GetText(pShMem, hSem, 0 /*track 0 == album*/,
                                  pu8Txt,
                                  NULL,
                                  OSAL_C_S32_CDAUDIO_MAXNAMESIZE - 1);
      switch(u32Ret)
      {
      case OSAL_E_NOERROR:
        //u32Result = OSAL_E_NOERROR;
        break;
      case OSAL_E_MEDIA_NOT_AVAILABLE:
        u32Result = OSAL_E_TEMP_NOT_AVAILABLE;
        break;
      case OSAL_E_INVALIDVALUE:
        u32Result = OSAL_E_INVALIDVALUE;
        break;
      default:
        u32Result = OSAL_E_NOTSUPPORTED; // CD-TEXT not supported
      } //switch(u32Ret)
      CDAUDIO_TRACE_IOCTRL_TXT(CDAUDIO_EN_GETALBUMNAME, 0, (tU32)0,
                               (const char*)pu8Txt);
    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKCDINFO: /* CD-TEXT command */
    {
      OSAL_trTrackCdTextInfo *prTrkTextInfo;
      prTrkTextInfo = (OSAL_trTrackCdTextInfo*)s32Arg;
      if(NULL == prTrkTextInfo)
      {
        u32Result = OSAL_E_INVALIDVALUE;
      }
      else //if(NULL == prTrkTextInfo)
      {
          tU8 u8MinTrack = 0;
          tU8 u8MaxTrack = 0;
          u32Result = CD_u32CacheGetCDInfo(pShMem, hSem, &u8MinTrack,
                                           &u8MaxTrack,
                                           NULL);
          if(OSAL_E_NOERROR == u32Result)
          {
            tU8 u8Track = (tU8)prTrkTextInfo->u32TrackNumber;
            if((u8Track == 0)
               || ((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack)))
            {
              tU32 u32Ret;
              u32Ret = CDAUDIO_u32GetText(pShMem, hSem, u8Track,
                                          (tPU8)prTrkTextInfo->rTrackTitle,
                                          (tPU8)prTrkTextInfo->rPerformer,
                                          OSAL_C_S32_CDAUDIO_MAXNAMESIZE - 1);
              switch(u32Ret)
              {
              case OSAL_E_NOERROR:
                //u32Result = OSAL_E_NOERROR;
                break;
              case OSAL_E_MEDIA_NOT_AVAILABLE:
                u32Result = OSAL_E_TEMP_NOT_AVAILABLE;
                break;
              case OSAL_E_INVALIDVALUE:
                u32Result = OSAL_E_INVALIDVALUE;
                break;
              default:
                u32Result = OSAL_E_NOTSUPPORTED; // CD-TEXT not supported
              } //switch(u32Ret)
              CDAUDIO_TRACE_IOCTRL_TXT(CDAUDIO_EN_GETTRACKCDINFO_ARTIST, 0,
                                       (tU32)prTrkTextInfo->u32TrackNumber,
                                       (const char*)prTrkTextInfo->rPerformer);
              CDAUDIO_TRACE_IOCTRL_TXT(
                                      CDAUDIO_EN_GETTRACKCDINFO_TITLE, 0,
                                      (tU32)prTrkTextInfo->u32TrackNumber,
                                      (const char*)prTrkTextInfo->rTrackTitle);
            }
            else //if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
            {
              u32Result = OSAL_E_INVALIDVALUE;
            } //else //if((u8Track >= u8MinTrack) && (u8Track <= u8MaxTrack))
          } //if(OSAL_E_NOERROR == u32Result)
      } //else //if(NULL == prTrkTextInfo)

    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY:
    {
      OSAL_trPlayInfoReg* prReg = (OSAL_trPlayInfoReg*)s32Arg;

      if((prReg == NULL) || (prReg->pCallback == NULL))
      {
        u32Result = OSAL_E_INVALIDVALUE;
      }
      else //if((prReg == NULL) || (prReg->pCallback == NULL))
      {
        u32Result = CD_u32RegisterPlayInfoNotifier(pShMem, prReg);
        /*CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_REGPLAYNOTIFY,
         0,
         nPos-1,
         prReg->pCallback,
         bDone
        );*/
      } //else //if((prReg == NULL) || (prReg->pCallback == NULL))
    }
    break;

  case OSAL_C_S32_IOCTRL_CDAUDIO_UNREGPLAYNOTIFY:
    {
      OSAL_trPlayInfoReg* prReg = (OSAL_trPlayInfoReg*)s32Arg;

      if((prReg == NULL) || (prReg->pCallback == NULL))
      {
        u32Result = OSAL_E_INVALIDVALUE;
      }
      else //if((prReg == NULL) || (prReg->pCallback == NULL))
      {
        u32Result = CD_u32UnregisterPlayInfoNotifier(pShMem, prReg);
      } //else //if((prReg == NULL) || (prReg->pCallback == NULL))
    }
    break;

  default:
    u32Result = OSAL_E_WRONGFUNC;
    break;
  }

  CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_IOCONTROL_RESULT, 0, s32Fun, u32Result, 0);

  CDAUDIO_TRACE_LEAVE_U4(CDID_CDAUDIO_u32IOControl, u32Result, 0, s32Fun,
                         s32Arg, 0);

  return u32Result;
}

/*********************************************************************/
/*****************************************************************************
 * FUNCTION:     u32MainInit
 * PARAMETER:
 * RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  initializes resources of device
 ******************************************************************************/
static tU32 u32MainInit(tDevCDData *pShMem)
{
  tU32 u32Result; //OSAL_E_NOERROR
  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  // set startup values
  u32Result = CDAUDIO_u32CDTextInit(pShMem);
  vMainClear(pShMem);
  return u32Result;
}

/*****************************************************************************
 * FUNCTION:     u32MainDestroy
 * PARAMETER:
 * RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  destroy resources of device
 ******************************************************************************/
static tU32 u32MainDestroy(tDevCDData *pShMem)
{
  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  CDAUDIO_vCDTextDestroy(pShMem);
  return OSAL_E_NOERROR;
}

/*****************************************************************************
 * FUNCTION:     CDAUDIO_u32Init
 * PARAMETER:
 * RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  initializes resources of device
 ******************************************************************************/
tU32 CDAUDIO_u32Init(tDevCDData *pShMem)
{
  tU32 u32Ret;

  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);

  pShMem->CDAUDIO_bTraceOn = LLD_bIsTraceActive(CDAUDIO_TRACE_CLASS,
                                                (tS32)TR_LEVEL_USER_1);

  CDAUDIO_TRACE_ENTER_U2(CDID_CDAUDIO_u32Init, (tU32)0, (tU32)0, (tU32)0,
                         (tU32)0);

  u32Ret = u32MainInit(pShMem);

  CDAUDIO_TRACE_LEAVE_U2(CDID_CDAUDIO_u32Init, u32Ret, (tU32)0, (tU32)0,
                         (tU32)0, (tU32)0);

  return u32Ret;
}

/*****************************************************************************
 * FUNCTION:     CDAUDIO_u32Destroy
 * PARAMETER:
 * RETURNVALUE:  OSAL Error code
 * DESCRIPTION:  destroys resources of device
 ******************************************************************************/
tU32 CDAUDIO_u32Destroy(tDevCDData *pShMem)
{
  tU32 u32Ret;
  CD_CHECK_SHARED_MEM(pShMem, OSAL_E_INVALIDVALUE);
  u32Ret = u32MainDestroy(pShMem);
  return u32Ret;
}

#ifdef __cplusplus
}
#endif
