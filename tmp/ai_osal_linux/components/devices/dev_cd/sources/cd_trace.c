/************************************************************************
 | $Revision: 1.0 $
 | $Date: 2012/01/01 $
 |************************************************************************
 | FILE:         cd_trace.c
 | PROJECT:      GEN3
 | SW-COMPONENT: OSAL I/O Device
 |------------------------------------------------------------------------
 | DESCRIPTION:
 |  This file contains the trace command implementation
 |------------------------------------------------------------------------
 | Date      | Modification               | Author
 | 2013      | GEN3                       | srt2hi
 |
 |************************************************************************
 |************************************************************************/

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <scsi/sg.h> /* take care: fetches glibc's /usr/include/scsi/sg.h */

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
#include "cd_funcenum.h"
#include "cd_main.h"
#include "cd_cache.h"
#include "cd_trace.h"
#include "cd_scsi_if.h"

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: module-local)
 |-----------------------------------------------------------------------*/
#define CD_PRINTF_BUFFER_SIZE 256
#define CD_TRACE_BUFFER_SIZE  256

/************************************************************************
 |typedefs (scope: module-local)
 |----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable definition (scope: module-local)
 |-----------------------------------------------------------------------*/
//variables used only by first instance of osal!
static volatile tBool gTraceCmdThreadIsRunning = FALSE;
static tU8 gu8TraceBuffer[CD_TRACE_BUFFER_SIZE]; /*not sychronyzed!*/
static OSAL_tThreadID gTraceCmdThreadID = OSAL_ERROR;
static OSAL_tEventHandle ghTraceOsalEvent = OSAL_C_INVALID_HANDLE;
//END variables used only by first instance of osal! 

/************************************************************************
 |function prototype (scope: module-local)
 |-----------------------------------------------------------------------*/
static tVoid CD_vShMemDebug(tDevCDData *pShMem);


/************************************************************************
 |function implementation (scope: module-local)
 |-----------------------------------------------------------------------*/

/********************************************************************//**
 *  FUNCTION:      CD_vShMemDebug
 *
 *  @brief         printf debug information of Shared MEM
 *
 *  @return        void
 *
 *  HISTORY:
 *
 ************************************************************************/
static tVoid CD_vShMemDebug(tDevCDData *pShMem)
{
  if(NULL == pShMem)
  {
    CD_PRINTF_ERRORS("Shared MEM Pointer is nULL");
    return;
  } //if(NULL == pShMem)

  CD_PRINTF_FORCED("pShMem-> [%p]", pShMem);

  //rCD
  {
    CD_PRINTF_FORCED(" rCD.rCDDrive.");
    CD_PRINTF_FORCED("  szDevSRName           [%s]",
                     pShMem->rCD.rCDDrive.szDevSRName);
    CD_PRINTF_FORCED("  bDefaultSRNameUsed [%d]",
                     (int)pShMem->rCD.rCDDrive.bDefaultSRNameUsed);
    CD_PRINTF_FORCED("  bSRCouldBeOpened   [%d]",
                     (int)pShMem->rCD.rCDDrive.bSRCouldBeOpened);
    CD_PRINTF_FORCED("  bSRFound           [%d]",
                     (int)pShMem->rCD.rCDDrive.bSRFound);

    CD_PRINTF_FORCED("  szDevSGName           [%s]",
                     pShMem->rCD.rCDDrive.szDevSGName);
    CD_PRINTF_FORCED("  bDefaultSGNameUsed [%d]",
                     (int)pShMem->rCD.rCDDrive.bDefaultSGNameUsed);
    CD_PRINTF_FORCED("  bSGCouldBeOpened   [%d]",
                     (int)pShMem->rCD.rCDDrive.bSGCouldBeOpened);
    CD_PRINTF_FORCED("  bSGFound           [%d]",
                     (int)pShMem->rCD.rCDDrive.bSGFound);
  }
  //print registered play info notifiers
  {
    int i;
    for(i = 0; i < CD_MAX_PLAYINFO_NOTIFIERS; i++)
    {
      //if(pShMem->arPlayInfoReg[i].funcUserCallback)
      {
        CD_PRINTF_FORCED(" arPlayInfoReg[%d].", (int)i);
        CD_PRINTF_FORCED("  funcUserCallback [%p]",
                         pShMem->arPlayInfoReg[i].funcUserCallback);
        CD_PRINTF_FORCED("  CD_Callback      [%p]",
                         pShMem->arPlayInfoReg[i].CD_Callback);
        CD_PRINTF_FORCED("  pvUserData       [%p]",
                         pShMem->arPlayInfoReg[i].pvUserData);
        CD_PRINTF_FORCED("  ProcID           [%d]",
                         pShMem->arPlayInfoReg[i].ProcID);

      } //if(pShMem->arPlayInfoReg[i].funcUserCallback)
    } //for(i = 0; i < CD_MAX_NOTIFIERS; i++)
  }

  //print registered medianotifiers
  {
    int i;
    for(i = 0; i < CD_MAX_MEDIA_NOTIFIERS; i++)
    {
      //if(pShMem->arMediaReg[i].funcUserCallback)
      {
        CD_PRINTF_FORCED(" arMediaReg[%d].", (int)i);
        CD_PRINTF_FORCED("  funcUserCallback [%p]",
                         pShMem->arMediaReg[i].funcUserCallback);
        CD_PRINTF_FORCED("  CD_Callback      [%p]",
                         pShMem->arMediaReg[i].CD_Callback);
        CD_PRINTF_FORCED("  u16AppID         [%u]",
                        (unsigned int)pShMem->arMediaReg[i].u16AppID);
        CD_PRINTF_FORCED("  u16NotiType      [0x%04X]",
                       (unsigned int)pShMem->arMediaReg[i].u16NotificationType);
        CD_PRINTF_FORCED("  ProcID           [%d]",
                        pShMem->arMediaReg[i].ProcID);
      } //if(pShMem->arPlayInfoReg[i].funcUserCallback)
    } //for(i = 0; i < CD_MAX_NOTIFIERS; i++)
  }

  //print drive status
  {
    CD_PRINTF_FORCED(" rDriveStatus.");
    CD_PRINTF_FORCED("  u32MediaChange  [%u]",
                     (unsigned int)pShMem->rDriveStatus.u32MediaChange);
    CD_PRINTF_FORCED("  u32Defect       [%u]",
                     (unsigned int)pShMem->rDriveStatus.u32Defect);
    CD_PRINTF_FORCED("  u32DeviceReady  [%u]",
                     (unsigned int)pShMem->rDriveStatus.u32DeviceReady);
    CD_PRINTF_FORCED("  u32MediaState   [%u]",
                     (unsigned int)pShMem->rDriveStatus.u32MediaState);
    CD_PRINTF_FORCED("  u32TotalFailure [%u]",
                     (unsigned int)pShMem->rDriveStatus.u32TotalFailure);
  }

  //print rCDAUDIO
  {
    CD_PRINTF_FORCED(" rCDAUDIO.");
    CD_PRINTF_FORCED("  u8Status         [%u]",
                     (unsigned int)pShMem->rCDAUDIO.u8Status);
    CD_PRINTF_FORCED("  u8PrevStatus     [%u]",
                     (unsigned int)pShMem->rCDAUDIO.u8PrevStatus);
    CD_PRINTF_FORCED("  bPlayInfoValid   [%u]",
                     (unsigned int)pShMem->rCDAUDIO.bPlayInfoValid);
    CD_PRINTF_FORCED("  u32ZLBA          [%u]",
                     (unsigned int)pShMem->rCDAUDIO.u32ZLBA);
    CD_PRINTF_FORCED("  rPlayInfo.");
    CD_PRINTF_FORCED("   u32TrackNumber   [%u]",
                     (unsigned int)pShMem->rCDAUDIO.rPlayInfo.u32TrackNumber);
    CD_PRINTF_FORCED("   u32StatusPlay    [%u]",
                     (unsigned int)pShMem->rCDAUDIO.rPlayInfo.u32StatusPlay);
    CD_PRINTF_FORCED(
              "   rAbsTrackAdr.MSF [%02u]:[%02u]:[%02u]",
              (unsigned int)pShMem->rCDAUDIO.rPlayInfo.rAbsTrackAdr.u8MSFMinute,
              (unsigned int)pShMem->rCDAUDIO.rPlayInfo.rAbsTrackAdr.u8MSFSecond,
              (unsigned int)pShMem->rCDAUDIO.rPlayInfo.rAbsTrackAdr.u8MSFFrame);
    CD_PRINTF_FORCED(
              "   rRelTrackAdr.MSF [%02u]:[%02u]:[%02u]",
              (unsigned int)pShMem->rCDAUDIO.rPlayInfo.rRelTrackAdr.u8MSFMinute,
              (unsigned int)pShMem->rCDAUDIO.rPlayInfo.rRelTrackAdr.u8MSFSecond,
              (unsigned int)pShMem->rCDAUDIO.rPlayInfo.rRelTrackAdr.u8MSFFrame);
    CD_PRINTF_FORCED("  rCurrentPlayRangeZLBA.");
    CD_PRINTF_FORCED("   u32StartZLBA [%u]",
            (unsigned int)pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32StartZLBA);
    CD_PRINTF_FORCED("   u32EndZLBA   [%u]",
              (unsigned int)pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32EndZLBA);
    CD_PRINTF_FORCED("   u32PrevZLBA [%u]",
              (unsigned int)pShMem->rCDAUDIO.rCurrentPlayRangeZLBA.u32PrevZLBA);
    CD_PRINTF_FORCED("  rNewPlayRange.");
    CD_PRINTF_FORCED("   rStartAdr.u8Track:u16Offset [%02u]:[%05u]",
              (unsigned int)pShMem->rCDAUDIO.rNewPlayRange.rStartAdr.u8Track,
              (unsigned int)pShMem->rCDAUDIO.rNewPlayRange.rStartAdr.u16Offset);
    CD_PRINTF_FORCED("   rEndAdr.u8Track:u16Offset    [%02u]:[%05u]",
                (unsigned int)pShMem->rCDAUDIO.rNewPlayRange.rEndAdr.u8Track,
                (unsigned int)pShMem->rCDAUDIO.rNewPlayRange.rEndAdr.u16Offset);
  }
}

/*****************************************************************************
 *
 * FUNCTION:
 *     getArgList
 *
 * DESCRIPTION:
 *     This stupid function is used for satifying L I N T
 *
 *
 * PARAMETERS: *va_list pointer to a va_list
 *
 * RETURNVALUE:
 *     va_list
 *
 *
 *
 *****************************************************************************/
static va_list getArgList(va_list* a)
{
  (void)a;
  return *a;
}

/********************************************************************//**
 *  FUNCTION:      u32CGET
 *
 *  @brief         return time in ms
 *
 *  @return        time in ms
 *
 *  HISTORY:
 *
 ************************************************************************/
static tU32 u32CGET(void)
{
  return(tU32)OSAL_ClockGetElapsedTime();
}
/********************************************************************//**
 *  FUNCTION:      CD_vTraceEnter
 *
 *  @brief         enter function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/

tVoid CD_vTraceEnter(tU32 u32Class, TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                     tenDevCDTrcFuncNames enFunction, tU32 u32Par1,
                     tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
  if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
  {
    tUInt uiIndex;
    tU8 au8Buf[4 * sizeof(tU32) + 8 * sizeof(tU32)]; //stack?
    OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
    tU32 u32Time = u32CGET();

    uiIndex = 0;
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CD_TRACE_ENTER);
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);  // Fill Byte
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);  // Fill Byte
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);  // Fill Byte
    uiIndex += sizeof(tU8);
    CD_INSERT_T32(&au8Buf[uiIndex], enFunction);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Line);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], procID);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Par1);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Par2);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Par3);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Par4);
    uiIndex += sizeof(tU32);

    // trace the stuff
    LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex + 32);
  }
}

/********************************************************************//**
 *  FUNCTION:      CD_vTraceLeave
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CD_vTraceLeave(tU32 u32Class, TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                     tenDevCDTrcFuncNames enFunction, tU32 u32OSALResult,
                     tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
  if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
  {
    tUInt uiIndex;
    tU8 au8Buf[4 * sizeof(tU8) + 8 * sizeof(tU32) + 32]; //stack?
    OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
    tU32 u32Time = u32CGET();

    uiIndex = 0;
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CD_TRACE_LEAVE);
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);  // Fill Byte
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);  // Fill Byte
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);  // Fill Byte
    uiIndex += sizeof(tU8);
    CD_INSERT_T32(&au8Buf[uiIndex], (tU32)enFunction);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32OSALResult);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Par1);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Par2);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Par3);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Par4);
    uiIndex += sizeof(tU32);

    // trace the stuff
    // be aware index is not greater than aray size!
    // I could not test it while runtime,
    // because LINT throws a senseless warning.
    LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex + 32);
  }
}

/*****************************************************************************
 *
 * FUNCTION:
 *     CD_vTracePrintf
 *
 * DESCRIPTION:
 *     This function creates the printf-style trace message
 *
 *
 * PARAMETERS:
 *
 * RETURNVALUE:
 *     None
 *
 *
 *
 *****************************************************************************/
tVoid CD_vTracePrintf(tU32 u32Class, TR_tenTraceLevel enTraceLevel,
                      tBool bForced, tU32 u32Line, const char* coszFormat, ...)
{
  if(bForced || LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
  {
    static tU8 au8Buf[CD_PRINTF_BUFFER_SIZE + 1];  //TODO: ? size ?
    tUInt uiIndex;
    long lSize;
    size_t tMaxSize;
    OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
    tU32 u32Time = u32CGET();
    va_list argList;
    uiIndex = 0;
    OSAL_M_INSERT_T8(
                    &au8Buf[uiIndex],
                    (tU32)(enTraceLevel == TR_LEVEL_ERRORS 
                                           ? CD_TRACE_PRINTF_ERROR :
                                           CD_TRACE_PRINTF));
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);
    uiIndex += sizeof(tU8);
    CD_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
    uiIndex += sizeof(tU32);

    argList = getArgList(&argList);
    va_start(argList, coszFormat); /*lint !e718 */
    tMaxSize = CD_PRINTF_BUFFER_SIZE - uiIndex;
    lSize = vsnprintf((char*)&au8Buf[uiIndex], tMaxSize, coszFormat, argList);
    uiIndex += (tUInt)lSize;
    va_end(argList);

    // trace the stuff 
    LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
  }

}

/*****************************************************************************
 *
 * FUNCTION:
 *     CD_vTraceSCSICmd
 *
 * DESCRIPTION:
 *     This prints a SCSI SG_IO header
 *
 *
 * PARAMETERS:
 *
 * RETURNVALUE:
 *     None
 *
 *
 *
 *****************************************************************************/
tVoid CD_vTraceSCSICmd(tU32 u32Class, TR_tenTraceLevel enTraceLevel,
                       tU32 u32Line, const sg_io_hdr_t *cprIO)
{
  if(LLD_bIsTraceActive(u32Class, (tS32)enTraceLevel))
  {
    static tU8 au8Buf[CD_PRINTF_BUFFER_SIZE + 1];  //TODO: ? size ?
    tUInt uiIndex;
    OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
    tU32 u32Time = u32CGET();

    memset(au8Buf, 0, sizeof(au8Buf));

    uiIndex = 0;
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32)CD_TRACE_SCSI_SG_CMD);
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 21);
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 22);
    uiIndex += sizeof(tU8);
    OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 23);
    uiIndex += sizeof(tU8);
    CD_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], u32Time);
    uiIndex += sizeof(tU32);

    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->interface_id);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->dxfer_direction);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->cmd_len);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->mx_sb_len);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->iovec_count);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->dxfer_len);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->dxferp);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->cmdp);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->sbp);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->timeout);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->flags);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->pack_id);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->usr_ptr);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->status);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->masked_status);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->msg_status);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->sb_len_wr);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->host_status);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->driver_status);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->resid);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->duration);
    uiIndex += sizeof(tU32);
    CD_INSERT_T32(&au8Buf[uiIndex], cprIO->info);
    uiIndex += sizeof(tU32);

    //SCSI commando
    if(NULL != cprIO->cmdp)
    { //max 12 bytes of commando
      size_t nSize = CD_MIN(cprIO->cmd_len, 12);
      memcpy(&au8Buf[uiIndex], cprIO->cmdp, nSize);
    }
    uiIndex += 12; // if command is shortes than 12, skip 

    //FIRST 16 BYtes of RESPONSE BUFFER
    if(NULL != cprIO->dxferp)
    { //max 16 bytes of response
      size_t nSize = CD_MIN(cprIO->dxfer_len, 16);
      memcpy(&au8Buf[uiIndex], cprIO->dxferp, nSize);
    }
    uiIndex += 16;
    //FIRST 18 BYtes of SENSE BUFFER
    if(NULL != cprIO->sbp)
    { //max 18 bytes of sense buffer
      size_t nSize = CD_MIN(cprIO->mx_sb_len, 18);
      memcpy(&au8Buf[uiIndex], cprIO->sbp, nSize);
    }
    uiIndex += 18;

    // trace the stuff 
    LLD_vTrace(u32Class, (tS32)enTraceLevel, au8Buf, (tU32)uiIndex);
  }
}

/*****************************************************************************
 * FUNCTION:     vTraceCmdThread
 * PARAMETER:    pvData
 * RETURNVALUE:  None
 * DESCRIPTION:  This is a Trace Thread handler.

 * HISTORY:
 ******************************************************************************/
#define CD_TRACE_EVENT_MASK_ANY  (~0)
#define CD_TRACE_EVENT_MASK_CMD  0x00000001
#define CD_TRACE_EVENT_MASK_END  0x00000002

static tVoid vTraceCmdThread(tPVoid pvData)
{
  const tU8 *pu8Data = (const tU8*)pvData;
  const tU8 *pu8Par = &pu8Data[3];
  OSAL_tEventMask tWaitEventMask = 0;
  tU8 u8Len;
  tU8 u8Cmd;
  tBool bTraceThreadLoop = TRUE;
  tU32 u32Ret;
  OSAL_tShMemHandle hShMem = OSAL_ERROR;
  tDevCDData *pShMem = NULL; /*OSAL shared memory*/
  OSAL_tSemHandle hSem = OSAL_C_INVALID_HANDLE;

  (void)pvData;

  u32Ret = CD_u32GetShMem(&pShMem, &hShMem, &hSem);
  if(OSAL_E_NOERROR == u32Ret)
  {
    while(bTraceThreadLoop)
    {
      CD_PRINTF_FORCED("TraceCmdThread is waiting for command ...");

      (void)OSAL_s32EventWait(ghTraceOsalEvent, CD_TRACE_EVENT_MASK_ANY,
                              OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER,
                              &tWaitEventMask);
      gTraceCmdThreadIsRunning = TRUE;  //simple sync

      (void)OSAL_s32EventPost(ghTraceOsalEvent, ~tWaitEventMask,
                              OSAL_EN_EVENTMASK_AND);

      u8Len = pu8Data[1];
      u8Cmd = pu8Data[2];
      if(0 != (tWaitEventMask & CD_TRACE_EVENT_MASK_END))
      {
        bTraceThreadLoop = FALSE;
      }
      else //if(0 != (tWaitEventMask & CD_TRACE_EVENT_MASK_END))
      {
        CD_PRINTF_U4("vTraceCmdThread 0x%08X,"
                     " TraceCmd[Len 0x%02X Cmd 0x%02X]",
                     (unsigned int)pu8Data, (unsigned int)u8Len,
                     (unsigned int)u8Cmd);
        switch(u8Cmd)
        {
        case CD_TRACE_CMD_TRACE_ON:
          if(NULL != pShMem)
            pShMem->CD_bTraceOn = TRUE;
          break;

        case CD_TRACE_CMD_TRACE_OFF:
          if(NULL != pShMem)
            pShMem->CD_bTraceOn = FALSE;
          break;

        case CD_TRACE_CMD_CDCACHE_DEBUG:
          CD_PRINTF_FORCED("TraceCmd CDCACHE_DEBUG [0x%02X]",
                           (unsigned int)pu8Par[0]);
          CD_vCacheDebug(pShMem);
          CD_PRINTF_FORCED("TraceCmd CDCACHE_DEBUG - END");
          break;

        case CD_TRACE_CMD_CDCACHE_CLEAR:
          CD_PRINTF_FORCED("TraceCmd CDCACHE_CLEAR [0x%02X]",
                           (unsigned int)pu8Par[0]);
          CD_vCacheClear(pShMem, OSAL_C_INVALID_HANDLE, TRUE);
          break;

        case CD_TRACE_CMD_SHAREDMEM_DEBUG:
          CD_PRINTF_FORCED("TraceCmd SHAREDMEM_DEBUG [0x%02X]",
                           (unsigned int)pu8Par[0]);
          CD_vShMemDebug(pShMem);
          CD_PRINTF_FORCED("TraceCmd SHAREDMEM_DEBUG - END");
          break;

        case CD_TRACE_CMD_DESTROY:
          CD_PRINTF_FORCED("TraceCmd DESTROY");
          bTraceThreadLoop = FALSE;
          (void)CD_u32Destroy();
          break;

        case CD_TRACE_CMD_SUBCHANNEL_TIMER_START_LOW:
          CD_PRINTF_FORCED("TraceCmd TimerStartLow");
          {
            OSAL_tEventHandle hMainEvent = OSAL_C_INVALID_HANDLE;

            if(OSAL_OK != OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent))
            {
              CD_PRINTF_ERRORS("Trace ERROR [%s], OSAL_s32EventOpen(%s)",
                          (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()),
                          CD_MAIN_EVENT_NAME);
            }
            else //if(OSAL_OK != OSAL_s32EventOpen(...
            {
              (void)OSAL_s32EventPost(hMainEvent,
                                  CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_LOW,
                                  OSAL_EN_EVENTMASK_OR);
              (void)OSAL_s32EventClose(hMainEvent);
            }
          }
          break;
        case CD_TRACE_CMD_SUBCHANNEL_TIMER_START_HI:
          CD_PRINTF_FORCED("TraceCmd TimerStartHi");
          {
            OSAL_tEventHandle hMainEvent = OSAL_C_INVALID_HANDLE;
            (void)OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent);
            (void)OSAL_s32EventPost(hMainEvent,
                                  CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_HI,
                                  OSAL_EN_EVENTMASK_OR);
            (void)OSAL_s32EventClose(hMainEvent);
          }
          break;
        case CD_TRACE_CMD_SUBCHANNEL_TIMER_STOP:
          CD_PRINTF_FORCED("TraceCmd Stop");
          {
            OSAL_tEventHandle hMainEvent = OSAL_C_INVALID_HANDLE;

            (void)OSAL_s32EventOpen(CD_MAIN_EVENT_NAME, &hMainEvent);
            (void)OSAL_s32EventPost(hMainEvent,
                                    CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER,
                                    OSAL_EN_EVENTMASK_OR);
            (void)OSAL_s32EventClose(hMainEvent);
          }
          break;
        case CD_TRACE_CMD_EJECT:
          CD_PRINTF_FORCED("TraceCmd EJECT");
          (void)CD_SCSI_IF_u32Eject(pShMem);
          break;
        case CD_TRACE_CMD_TEST_SCSI:
          {
            //tS32 s32 = 0;
            //tU8 u8MediaType = 0;
            tU8 u8LoaderState = 0;
            CD_PRINTF_FORCED("TraceCmd TEST_SCSI");

            CD_SCSI_IF_u32GetLoaderStatus(pShMem, &u8LoaderState);
            CD_PRINTF_FORCED("LoaderInfo 0x%02X", (unsigned int)u8LoaderState);

            // CD_SCSI_IF_u32TestUnitReady(pShMem, &s32);

            // CD_SCSI_IF_u32ModeSense(pShMem, &u8MediaType);
            // CD_PRINTF_FORCED("MediaType 0x%02X", (unsigned int)u8MediaType);
          }
          break;

        default:
          {
            CD_PRINTF_U4("TraceCmd IOCTRL DEFAULT  - NOT SUPPORTED");
          }

        } //switch(u8Cmd)
      } //else //if(0 != (tWaitEventMask & CD_TRACE_EVENT_MASK_END))
      gTraceCmdThreadIsRunning = FALSE;
    } //while(bTraceThreadLoop)
  } //if(OSAL_E_NOERROR == u32Ret)

  CD_vReleaseShMem(pShMem, hShMem, hSem);
  CD_PRINTF_FORCED("TraceCmdThread EXIT");
}

/*****************************************************************************
 * FUNCTION:     CD_vTraceCmdCallback
 * PARAMETER:    buffer
 * RETURNVALUE:  None
 * DESCRIPTION:  This is a Trace callback handler.

 * HISTORY:
 ******************************************************************************/
tVoid CD_vTraceCmdCallback(const tU8* pcu8Buffer)
{
  if(pcu8Buffer != NULL)
  {
    // create trace event, if not exist */
    if(OSAL_C_INVALID_HANDLE == ghTraceOsalEvent)
    {
      const char *pcszEventName = "CD_TRACE";
      if(OSAL_OK != OSAL_s32EventCreate(pcszEventName, &ghTraceOsalEvent))
      {
        tU32 u32Err = OSAL_u32ErrorCode();
        CD_PRINTF_ERRORS("TRACE  ERROR 0x%08X, "
                         "OSAL_s32EventCreate(%s)",
                         (unsigned int)u32Err, pcszEventName);
      }
    } //if(OSAL_C_INVALID_HANDLE == ghTraceOsalEvent)

    //spawn thread, if not exist
    if(OSAL_ERROR == gTraceCmdThreadID)
    {
      OSAL_trThreadAttribute traceCmdThreadAttr;

      CD_PRINTF_FORCED("TraceThread will be created");

      traceCmdThreadAttr.szName = "CDTRACE";
      traceCmdThreadAttr.u32Priority = 0;
      traceCmdThreadAttr.pfEntry = vTraceCmdThread;
      traceCmdThreadAttr.pvArg = gu8TraceBuffer;
      traceCmdThreadAttr.s32StackSize = 1024;

      gTraceCmdThreadID = OSAL_ThreadSpawn(&traceCmdThreadAttr);
      if(OSAL_ERROR == gTraceCmdThreadID)
      {
        CD_PRINTF_ERRORS("Create TraceThread");
      } //if(OSAL_ERROR == traceCmdThreadID)
    }
    else //if(OSAL_ERROR == gTraceCmdThreadID)
    {
      CD_PRINTF_FORCED("TraceThread exists");
    }

    if(!gTraceCmdThreadIsRunning)
    {
      tU8 u8Len = pcu8Buffer[0];

      CD_PRINTF_FORCED("Trace command forward to TraceThread");

      gTraceCmdThreadIsRunning = TRUE;

      (void)OSAL_pvMemoryCopy(gu8TraceBuffer, pcu8Buffer, u8Len + 1);
      (void)OSAL_s32EventPost(ghTraceOsalEvent,
                              CD_TRACE_EVENT_MASK_CMD,
                              OSAL_EN_EVENTMASK_OR);
    }
    else //if(!gTraceCmdThreadIsRunning)
    {
      CD_PRINTF_ERRORS("last Trace thread is running yet");
    } //else //if(!gTraceCmdThreadIsRunning)
  } //if(pcu8Buffer != NULL)
}

#ifdef __cplusplus
}
#endif
/************************************************************************ 
 |end of file
 |-----------------------------------------------------------------------*/
