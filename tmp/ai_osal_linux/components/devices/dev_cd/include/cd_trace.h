/************************************************************************
 | FILE:         cd_trace.h
 | PROJECT:      Gen3
 | SW-COMPONENT: cd_
 |------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cd_trace.h
 *
 * @brief   This file includes trace stuff for the cd.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 *
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (CD_TRACE_H)
#define CD_TRACE_H

/************************************************************************ 
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************ 
 |defines and macros (scope: global)
 /-----------------------------------------------------------------------*/

#define CD_TR if(pShMem != NULL ? pShMem->CD_bTraceOn : TRUE)
//#define CD_TM ((unsigned int)OSAL_ClockGetElapsedTime())

#define CD_TRACE_ENABLED

/*trace classes used by cd_if, defined in osalcore/include/ostrace.h*/
#define CD_TRACE_CLASS    OSAL_C_TR_CLASS_LLD_ATAPICDVD

#ifdef CD_TRACE_ENABLED

//extern tBool    CD_bTraceOn;

#define CD_TRACE_ENTER_U2(FUNC,P1,P2,P3,P4) \
    if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTraceEnter(CD_TRACE_CLASS, TR_LEVEL_USER_2,\
                   __LINE__, FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define CD_TRACE_ENTER_U3(FUNC,P1,P2,P3,P4) \
    if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTraceEnter(CD_TRACE_CLASS, TR_LEVEL_USER_3,\
                   __LINE__, FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define CD_TRACE_ENTER_U4(FUNC,P1,P2,P3,P4) \
    if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTraceEnter(CD_TRACE_CLASS, TR_LEVEL_USER_4,\
                   __LINE__, FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define CD_TRACE_ENTER_U4F(FUNC,P1,P2,P3,P4) \
		CD_vTraceEnter(CD_TRACE_CLASS, TR_LEVEL_USER_4,\
                   __LINE__, FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define CD_TRACE_LEAVE_U2(FUNC,ERROR,P1,P2,P3,P4) \
    if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTraceLeave(CD_TRACE_CLASS, TR_LEVEL_USER_2,\
                   __LINE__, FUNC, (ERROR), (tU32)(P1),\
				   (tU32)(P2), (tU32)(P3), (tU32)(P4))

#define CD_TRACE_LEAVE_U3(FUNC,ERROR,P1,P2,P3,P4) \
    if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTraceLeave(CD_TRACE_CLASS, TR_LEVEL_USER_3,\
                   __LINE__, FUNC, (ERROR), (tU32)(P1),\
				   (tU32)(P2), (tU32)(P3), (tU32)(P4))

#define CD_TRACE_LEAVE_U4(FUNC,ERROR,P1,P2,P3,P4) \
    if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTraceLeave(CD_TRACE_CLASS, TR_LEVEL_USER_4,\
                   __LINE__, FUNC, (ERROR), (tU32)(P1),\
				   (tU32)(P2),  (tU32)(P3),  (tU32)(P4))

#define CD_PRINTF_FORCED(...) CD_vTracePrintf(CD_TRACE_CLASS,\
                              TR_LEVEL_FATAL, TRUE,\
                              __LINE__, __VA_ARGS__)

#define CD_PRINTF_FATAL(...) CD_vTracePrintf(CD_TRACE_CLASS,\
                             TR_LEVEL_FATAL, TRUE,\
                             __LINE__, __VA_ARGS__)

#define CD_PRINTF_ERRORS(...) CD_vTracePrintf(CD_TRACE_CLASS,\
                              TR_LEVEL_ERRORS, FALSE,\
                              __LINE__, __VA_ARGS__)

#define CD_PRINTF_U1(...) if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTracePrintf(CD_TRACE_CLASS, TR_LEVEL_USER_1, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CD_PRINTF_U2(...) if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTracePrintf(CD_TRACE_CLASS, TR_LEVEL_USER_2, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CD_PRINTF_U3(...) if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTracePrintf(CD_TRACE_CLASS, TR_LEVEL_USER_3, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CD_PRINTF_U4(...) if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTracePrintf(CD_TRACE_CLASS, TR_LEVEL_USER_4, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CD_PRINTF_SCSI_ERRORS(SG_IO) CD_vTraceSCSICmd(CD_TRACE_CLASS,\
                                         TR_LEVEL_ERRORS, __LINE__, (SG_IO))

  
#define CD_PRINTF_SCSI_U1(SG_IO) if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
    CD_vTraceSCSICmd(CD_TRACE_CLASS, TR_LEVEL_USER_1, __LINE__, (SG_IO))
  
#define CD_PRINTF_SCSI_U3(SG_IO) if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTraceSCSICmd(CD_TRACE_CLASS, TR_LEVEL_USER_3, __LINE__, (SG_IO))

#define CD_PRINTF_SCSI_U4(SG_IO) if(pShMem ? pShMem->CD_bTraceOn : TRUE) \
		CD_vTraceSCSICmd(CD_TRACE_CLASS, TR_LEVEL_USER_4, __LINE__, (SG_IO))

#else //#ifdef CD_TRACE_ENABLED

#define CD_TRACE_ENTER_U2(FUNC,P1,P2,P3,P4)
#define CD_TRACE_ENTER_U3(FUNC,P1,P2,P3,P4)
#define CD_TRACE_ENTER_U4(FUNC,P1,P2,P3,P4)
#define CD_TRACE_LEAVE_U2(FUNC,ERROR,P1,P2,P3,P4)
#define CD_TRACE_LEAVE_U3(FUNC,ERROR,P1,P2,P3,P4)
#define CD_TRACE_LEAVE_U4(FUNC,ERROR,P1,P2,P3,P4)
#define CD_PRINTF_FORCED(...)
#define CD_PRINTF_FATAL(...)
#define CD_PRINTF_ERRORS(...)
#define CD_PRINTF_U1(...)
#define CD_PRINTF_U2(...)
#define CD_PRINTF_U3(...)
#define CD_PRINTF_U4(...)
#endif //#ifdef CD_TRACE_ENABLED
/** trace messages */
/** trace message IDs */
enum CD_enTraceMessages
{
  CD_TRACE_ENTER = 1,
  CD_TRACE_LEAVE,
  CD_TRACE_PRINTF,
  CD_TRACE_PRINTF_ERROR,
  CD_TRACE_SCSI_SG_CMD
};

/** trace command messages */
enum CD_enTraceCommandMessages
{
  CD_TRACE_CMD_TRACE_ON = 0x01,
  CD_TRACE_CMD_TRACE_OFF = 0x02,
  CD_TRACE_CMD_CDCACHE_DEBUG = 0x03,
  CD_TRACE_CMD_CDCACHE_CLEAR = 0x04,
  CD_TRACE_CMD_SHAREDMEM_DEBUG = 0x05,
  CD_TRACE_CMD_DESTROY = 0x06,
  CD_TRACE_CMD_SUBCHANNEL_TIMER_START_LOW = 0x07,
  CD_TRACE_CMD_SUBCHANNEL_TIMER_START_HI = 0x08,
  CD_TRACE_CMD_SUBCHANNEL_TIMER_STOP = 0x09,
  CD_TRACE_CMD_EJECT = 0x0A,
  CD_TRACE_CMD_TEST_SCSI = 0x0B,
  CD_TRACE_END
};

/************************************************************************ 
 |typedefs and struct defs (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************ 
 | variable declaration (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************ 
 |function prototypes (scope: global)
 |-----------------------------------------------------------------------*/

tVoid CD_vTraceEnter(tU32 u32Class, TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                     tenDevCDTrcFuncNames enFunction, tU32 u32Par1,
                     tU32 u32Par2, tU32 u32Par3, tU32 u32Par4);

tVoid CD_vTraceLeave(tU32 u32Class, TR_tenTraceLevel enTraceLevel, tU32 u32Line,
                     tenDevCDTrcFuncNames enFunction, tU32 u32OSALError,
                     tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4);

tVoid CD_vTracePrintf(tU32 u32Class, TR_tenTraceLevel enTraceLevel,
                      tBool bForced, tU32 u32Line, const char* coszFormat, ...);

tVoid CD_vTraceCmdCallback(const tU8* pcu8Buffer);

tVoid CD_vTraceSCSICmd(tU32 u32Class, TR_tenTraceLevel enTraceLevel,
                       tU32 u32Line, const sg_io_hdr_t *cprIO);

#ifdef __cplusplus
}
#endif

#else // #if !defined (CD_TRACE_H)
#error cd_trace.h included several times
#endif // #else #if !defined (CD_TRACE_H)
