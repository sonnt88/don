/************************************************************************
 | FILE:         cdaudio.h
 | PROJECT:      Gen2
 | SW-COMPONENT: CDAUDIO driver
 |------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cdaudio.h
 *
 * @brief   This file includes public stuff for the cdaudio driver.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 *
 *//* ***************************************************FileHeaderEnd******* */

#ifndef CDAUDIO_H
#define CDAUDIO_H

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: global)
 |-----------------------------------------------------------------------*/
#define CDAUDIO_DRIVE_COUNT      1

//Playrange: NONE (ignore START-Position == use recent position)
#define CDAUDIO_U32NONE                               0xFFFFFFFF
//Playrange: Offset END-Of-Track
//#define CDAUDIO_U16ONONE                            0xFFFF

#define CDAUDIO_STATUS_INITIALIZED        0x01
#define CDAUDIO_STATUS_PLAY               0x02
#define CDAUDIO_STATUS_PAUSED             0x03
#define CDAUDIO_STATUS_SCAN_FWD           0x04
#define CDAUDIO_STATUS_SCAN_BWD           0x05
#define CDAUDIO_STATUS_STOP               (CDAUDIO_STATUS_INITIALIZED)
#define CDAUDIO_STATUS_ERROR              0x07
#define CDAUDIO_STATUS_UNDERVOLTAGE_ERROR 0x08
#define CDAUDIO_STATUS_PAUSED_BY_USER     0x09

/************************************************************************
 |typedefs and struct defs (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable declaration (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function prototypes (scope: global)
 |-----------------------------------------------------------------------*/
tU32 CDAUDIO_u32IOOpen(tS32 s32Dummy);
tU32 CDAUDIO_u32IOClose(tS32 s32Dummy);
tU32 CDAUDIO_u32IOControl(tS32 s32Dummy, tS32 s32Fun, tS32 s32Arg);
tU32 CDAUDIO_u32Destroy(tDevCDData *pShMem);
tU32 CDAUDIO_u32Init(tDevCDData *pShMem);

#ifdef __cplusplus
}
#endif

#else //CDAUDIO_H
#error cdaudio.h included several times
#endif //#else //CDAUDIO_H
