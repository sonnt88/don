/************************************************************************
 | FILE:         cd_cache.h
 | PROJECT:      Gen3
 | SW-COMPONENT: CD driver
 |------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cd_cache.h
 *
 * @brief   infos about inserted cd
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 *
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (CD_CACHE_H)
#define CD_CACHE_H

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: global)
 |-----------------------------------------------------------------------*/
// if Track 255 is given to OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO
// MAX LBA is returned
#define CD_TRACKINFO_MAX_LBA_TRACK            0xFF

/************************************************************************
 |typedefs and struct defs (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable declaration (scope: global)
 |-----------------------------------------------------------------------*/
tVoid CD_vCacheClear(tDevCDData *pShMem,
                     OSAL_tSemHandle hSem, tBool bFullClear);
tVoid CD_vCacheDebug(tDevCDData *pShMem);
tU32 CD_u32CacheGetCDInfo(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                          tU8 *pu8MinTrack, tU8 *pu8MaxTrack, tU32 *pLastLBA);
tU32 CD_u32CacheGetTrackInfo(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                             tU8 u8Track, tU32 *pu32StartLBA,
                             tU32 *pu32Control);
tU32 CD_u32CacheGetVersion(tDevCDData *pShMem, tU8 *pu8SW, tU8 *pu8HW,
                           tU8 *pau8Vendor8);

tU8 CD_u8GetLoaderInfo(tDevCDData *pShMem, tU8 *pu8InternalLoaderState);

tU8 CD_u8GetMediaType(tDevCDData *pShMem);
//tU8   CD_u8SetMediaType(tDevCDData *pShMem, tU8 u8MediaType);
tU8 CD_u8GetInternalMediaType(tDevCDData *pShMem);

tU8 CD_u8GetFSType(tDevCDData *pShMem);

tU32 CD_u32CacheInit(tDevCDData *pShMem);
tVoid CD_vCacheDestroy(tDevCDData *pShMem);

tU32 CD_u32GetStartOfTrackZLBA(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                               tU8 u8Track);
tU32 CD_u32TS2ZLBA(tDevCDData *pShMem, OSAL_tSemHandle hSem, tU8 u8Track,
                   tU16 u16Seconds);
tU32 CD_u32GetEndOfTrackZLBA(tDevCDData *pShMem, OSAL_tSemHandle hSem,
                             tU8 u8Track);
tU32 CD_u32GetEndOfCDZLBA(tDevCDData *pShMem);
tBool CD_bGetTMSFromAbsZLBA(tDevCDData *pShMem, tU32 u32ZLBA,
                            CD_sTMSF_type *prRelTMSF);

/************************************************************************
 |function prototypes (scope: global)
 |-----------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#else //CD_CACHE_H
#error cd_cache.h included several times
#endif //#else //CD_CACHE_H
