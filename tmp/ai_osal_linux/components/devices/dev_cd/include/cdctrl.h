/************************************************************************
 | FILE:         cdctrl.h
 | PROJECT:      Gen2
 | SW-COMPONENT: CDCTRL driver
 |------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cdctrl.h
 *
 * @brief   This file includes public stuff for the cdctrl driver.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 *
 *//* ***************************************************FileHeaderEnd******* */

#ifndef CDCTRL_H
#define CDCTRL_H

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: global)
 |-----------------------------------------------------------------------*/

#define CDCTRL_C_S32_IO_VERSION        (tS32)(0x00000210)
#define CDCTRL_ENABLE_DEBUG 1

#undef CDCTRL_DEBUG_OPEN_AT_STARTUP_TEST_ENABLED
//#ifdef CDCTRL_DEBUG_OPEN_AT_STARTUP_TEST_ENABLED

/*Vendor ID START*/
#define CDCTRL_MASCA_VENDOR_ID_NOT_AVAILABLE      0
#define CDCTRL_MASCA_VENDOR_ID_UNKNOWN            1
#define CDCTRL_MASCA_VENDOR_ID_TANASHIN           2
#define CDCTRL_MASCA_VENDOR_ID_KENWOOD            3
#define CDCTRL_MASCA_VENDOR_ID_PIONEER            4

/************************************************************************
 |typedefs and struct defs (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable declaration (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function prototypes (scope: global)
 |-----------------------------------------------------------------------*/
tU32 CDCTRL_u32Init(tDevCDData *pShMem);
tU32 CDCTRL_u32Destroy(const tDevCDData *pShMem);
tU32 CDCTRL_u32IOOpen(tS32 s32Dummy);
tU32 CDCTRL_u32IOClose(tS32 s32Dummy);
tU32 CDCTRL_u32IOControl(tS32 s32Dummy, tS32 s32Fun, tS32 s32Arg);

#ifdef __cplusplus
}
#endif

#else //CDCTRL_H
#error cdctrl.h included several times
#endif //#else //CDCTRL_H
