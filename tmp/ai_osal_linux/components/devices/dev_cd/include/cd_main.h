/************************************************************************
 | FILE:         cd_main.h
 | PROJECT:      GEN3
 | SW-COMPONENT: CD driver
 |------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cd_main.h
 *
 * @brief   Startup of dev_cd.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 * @History: 
 * 16 Mar,2016 | Bug Fix NCG3D-11114 : 
 *               au8HWVersion2 and au8SWVersion2 arrays are made to 3 bytes ( 2 bytes for data and 1 for '\0'
 *             | Kranthi Kiran (RBEI/ECF5)
 ****************************************************FileHeaderEnd******************************************/

#if !defined (CD_MAIN_H)
#define CD_MAIN_H

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif

/*l i n t -e522*/

/************************************************************************
 |defines and macros (scope: global)
 |-----------------------------------------------------------------------*/
#undef TRUE
#define TRUE (0==0)
#undef FALSE
#define FALSE (0!=0)

#define CD_SHMEM_NAME                 "CDShMem"
#define CD_SEM_NAME                   "CDSem"
#define CD_MAIN_EVENT_NAME            "CDMainEv"

#define CD_COMMON_TEXT_BUFFER_LEN  256
#define CD_DEV_NAME_LEN            32

#define CD_INVALID 0
#define CD_VALID   1

#define CD_DRIVE_COUNT               1
#define CD_MAX_TRACK_NUMBER         99
#define CD_SECONDS_PER_MINUTE       60
#define CD_SECTORS_PER_SECOND       75
#define CD_USER_BYTES_PER_SECTOR  2048
//#define CD_LBA_OFFSET              150
#define CD_TRACK_ARRAY_SIZE (CD_MAX_TRACK_NUMBER+1)
#define CD_MIN(X,Y) ((X) < (Y) ? (X) : (Y))

#define CD_MS2SECSONDS(_MIN_,_SEC_) ((((tInt)(_MIN_) * 60) + (tInt)(_SEC_)))
#define CD_SEC2ZLBA(_SEC_)          ((_SEC_)*CD_SECTORS_PER_SECOND)
//#define CD_ZLBA2LBA(_ZLBA_)         ((tS32)(_ZLBA_) - CD_LBA_OFFSET)

#define CD_16BIT_MSB(_X_) ((tU8)(((_X_) & 0xFF00) >> 8))
#define CD_16BIT_LSB(_X_) ((tU8)((_X_) & 0xFF))

#define CD_32BIT_B0(_X_) ((tU8)((_X_) & 0xFF))
#define CD_32BIT_B1(_X_) ((tU8)(((_X_) & 0x0000FF00UL) >> 8))
#define CD_32BIT_B2(_X_) ((tU8)(((_X_) & 0x00FF0000UL) >> 16))
#define CD_32BIT_B3(_X_) ((tU8)(((_X_) & 0xFF000000UL) >> 24))

#define CD_8TO32(_3_,_2_,_1_,_0_) (((tU32)(_3_) << 24) |\
                                  ((tU32)(_2_) << 16) |\
                                  ((tU32)(_1_) << 8) |\
                                  ((tU32)(_0_)))

#define CD_8TO16(_HI_,_LO_) ((((tU16)(_HI_)) << 8) | ((tU16)(_LO_)&0xFF))

//#define CD_ZLBA2LBA(_ZLBA_)   (((tS32)(_ZLBA_)) - CD_LBA_OFFSET)
//#define CD_LBA2ZLBA(_LBA_)    ((_LBA_)  + CD_LBA_OFFSET)

#define CD_MSF2ZLBA(M,S,F) (((tU32)(M) * CD_SECONDS_PER_MINUTE  \
                               + (tU32)(S)) * CD_SECTORS_PER_SECOND \
                               + (tU32)(F))

#define CD_CHECK_SHARED_MEM(SHARED_MEM_POINTER, RETURNVAL) \
                            if((SHARED_MEM_POINTER) == NULL) {\
                             return (RETURNVAL);} else \
                            {(void)(SHARED_MEM_POINTER);}

#define CD_CHECK_SHARED_MEM_VOID(SHARED_MEM_POINTER) \
                            if((SHARED_MEM_POINTER) == NULL){ return;}\
                            else {(void)(SHARED_MEM_POINTER);}

/*avoids l i n t "Warning 572: prio2:
 Excessive shift value (precision 7 shifted right by 24)*/
#define CD_INSERT_T32(buf, Long) {tU32 u32 = (tU32)(Long); \
                      (buf)[0] = (tU8) ((u32 >> 24) & 0xFF); \
                      (buf)[1] = (tU8) ((u32 >> 16) & 0xFF); \
                      (buf)[2] = (tU8) ((u32 >> 8 ) & 0xFF); \
                      (buf)[3] = (tU8) ((u32      ) & 0xFF);}

/*SharedMEm semaphore emergency exit in case of dead lock*/
#define CD_SM_SEMAPHORE_TIMEOUT_MS 10000
/*#define CD_SM_LOCK(_h_) if((_h_)!= OSAL_C_INVALID_HANDLE)\
                        { \
                          if(OSAL_OK != OSAL_s32SemaphoreWait((_h_),\
                          CD_SM_SEMAPHORE_TIMEOUT_MS))\
                          {\
  NORMAL_M_ASSERT(0);\
                          }\
                        }*/

#define CD_SM_LOCK(_h_) if((_h_)!= OSAL_C_INVALID_HANDLE)\
                          (void) OSAL_s32SemaphoreWait((_h_),\
                            CD_SM_SEMAPHORE_TIMEOUT_MS);

#define CD_SM_UNLOCK(_h_) if((_h_)!= OSAL_C_INVALID_HANDLE)\
                            (void)OSAL_s32SemaphorePost((_h_))

#define CD_MAX_PLAYINFO_NOTIFIERS                      3
#define CD_MAX_MEDIA_NOTIFIERS                         3
#define CDAUDIO_CDTEXT_TEXT_BUFFER_LEN      256 /*255 bytes text+trailing zero*/
#define CDAUDIO_ATA_CDTEXT_RAW_BUFFER_LEN  4000

//event used by CD_MAIN_EVENT_NAME
#define CD_MAIN_EVENT_MASK_ANY                               (~0)
#define CD_MAIN_EVENT_MASK_SUBCHANNEL_FAKE             0x00000001
#define CD_MAIN_EVENT_MASK_SUBCHANNEL_TIMER            0x00000002
#define CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_LOW  0x00000004
#define CD_MAIN_EVENT_MASK_START_SUBCHANNEL_TIMER_HI   0x00000008
#define CD_MAIN_EVENT_MASK_STOP_SUBCHANNEL_TIMER       0x00000010
#define CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_LOW 0x00000020
#define CD_MAIN_EVENT_MASK_START_LOADERSTATE_TIMER_HI  0x00000040
#define CD_MAIN_EVENT_MASK_STOP_LOADERSTATE_TIMER      0x00000080
#define CD_MAIN_EVENT_MASK_UDEV_EJECT                  0x00000100
#define CD_MAIN_EVENT_MASK_UDEV_MEDIA_CHANGE           0x00000200
#define CD_MAIN_EVENT_MASK_WAKEUP_CHANGED              0x00000400
#define CD_MAIN_EVENT_MASK_LOADERSTATE_TIMER           0x00000800
#define CD_MAIN_EVENT_MASK_DRIVE_SLEEP                 0x00001000
#define CD_MAIN_EVENT_MASK_DRIVE_ACTIVE                0x00002000
#define CD_MAIN_EVENT_MASK_MIXED_NOT_READY             0x00004000
#define CD_MAIN_EVENT_MASK_MIXED_READY_DATA            0x00008000
#define CD_MAIN_EVENT_MASK_MIXED_READY_AUDIO           0x00010000
#define CD_MAIN_EVENT_MASK_WATCHDOG_TIMER              0x00020000
#define CD_MAIN_EVENT_MASK_END                         0x80000000

//event used by VOLT EVENT
#define CD_VOLT_EVENT_MASK_ANY (~0)
#define CD_VOLT_EVENT_MASK_END DEV_VOLT_C_U32_EVENT_MASK_FREE_FOR_CLIENT_32

//used by tCD_sDriveStates
#define CD_MAIN_U32_NOT_INITIALIZED                    ((tU32)0xFFFFFFFF)
#define CD_MAIN_U16_NOT_INITIALIZED                    ((tU16)0xFFFF)

/*internal loaderstates used by get event / status notification*/
#define CD_INTERNAL_LOADERSTATE_UNKNOWN            0x00
#define CD_INTERNAL_LOADERSTATE_NO_CD              0x01  
#define CD_INTERNAL_LOADERSTATE_INSERT_IN_PROGRESS 0x02
#define CD_INTERNAL_LOADERSTATE_LOAD_ERROR         0x03
#define CD_INTERNAL_LOADERSTATE_CD_INSIDE          0x04
#define CD_INTERNAL_LOADERSTATE_CD_PLAYABLE        0x05
#define CD_INTERNAL_LOADERSTATE_CD_UNREADABLE      0x06
#define CD_INTERNAL_LOADERSTATE_EJECT_IN_PROGRESS  0x07
#define CD_INTERNAL_LOADERSTATE_EJECT_ERROR        0x08
#define CD_INTERNAL_LOADERSTATE_IN_SLOT            0x09
#define CD_INTERNAL_LOADERSTATE_EJECTED            0x0A
#define CD_INTERNAL_LOADERSTATE_INITIALIZED        0xFF

// For evaluate media type from mode sense scsi command
//used by CD_sCD_type.u8InternalMediaType
#define CD_INTERNAL_MEDIA_TYPE_INCORRECT_MEDIA            (tU8)0x10
#define CD_INTERNAL_MEDIA_TYPE_UNKNOWN_MEDIA              (tU8)0x11
#define CD_INTERNAL_MEDIA_TYPE_DATA_MEDIA                 (tU8)0x12
#define CD_INTERNAL_MEDIA_TYPE_AUDIO_MEDIA                (tU8)0x13
#define CD_INTERNAL_MEDIA_TYPE_MIXED_MODE_MEDIA           (tU8)0x14

// Control types of track
#define CD_TYPE_MASK              0x04
#define CD_TYPE_DATA              0x04

/************************************************************************
 |typedefs and struct defs (scope: global)
 |-----------------------------------------------------------------------*/
typedef enum
{
  enCDAUDIO_CDTEXT_INVALID,
  enCDAUDIO_CDTEXT_VALID
} enCDAUDIO_cdtext_valid_t;

enum
{
  enCDAUDIO_CDTEXT_TYPE_TITLE = 0x80,
  enCDAUDIO_CDTEXT_TYPE_ARTIST = 0x81
};

/* Status used for Media notifications (formerly handles by PRM)*/
typedef struct CD_sDriveStates_tag
{
  //OSAL_C_U16_NOTI_MEDIA_CHANGE 
  //- OSAL_C_U16_MEDIA_EJECTED
  //- OSAL_C_U16_INCORRECT_MEDIA
  //- OSAL_C_U16_DATA_MEDIA
  //- OSAL_C_U16_AUDIO_MEDIA
  //- OSAL_C_U16_UNKNOWN_MEDIA
  tU32 u32MediaChange;
  //OSAL_C_U16_NOTI_TOTAL_FAILURE
  //- OSAL_C_U16_DEVICE_OK
  //- OSAL_C_U16_DEVICE_FAIL
  tU32 u32TotalFailure;
  //OSAL_C_U16_NOTI_MEDIA_STATE
  //- OSAL_C_U16_MEDIA_NOT_READY
  //- OSAL_C_U16_MEDIA_READY
  tU32 u32MediaState;
  //OSAL_C_U16_NOTI_DEFECT
  //- OSAL_C_U16_DEFECT_LOAD_EJECT
  //- OSAL_C_U16_DEFECT_LOAD_INSERT
  //- OSAL_C_U16_DEFECT_DISCTOC
  //- OSAL_C_U16_DEFECT_DISC
  //- OSAL_C_U16_DEFECT_READ_ERR
  //- OSAL_C_U16_DEFECT_READ_OK
  tU32 u32Defect;
  //OSAL_C_U16_NOTI_DEVICE_READY
  //- OSAL_C_U16_DEVICE_NOT_READY
  //- OSAL_C_U16_DEVICE_READY
  //- OSAL_C_U16_DEVICE_UV_NOT_READY
  //- OSAL_C_U16_DEVICE_UV_READY
  tU32 u32DeviceReady;
} tCD_sDriveStates;

/* CD Subchannel data - SCSI READ_SUBCHANNEL, Playtime, Playinfo*/
typedef struct CD_sSubchannel_tag
{
  tU32 u32RelZLBA;
  tU32 u32AbsZLBA;
  tU8 u8Track;
  tU8 u8Index;
  tU8 u8AudioStatus;
  tU8 u8AdrCtrl;
  tU8 u8Valid;
} tCD_sSubchannel;

/*generic Track-Minute-Seconds-Frame type*/
typedef struct CDAUDIO_sTMSF_tag
{
  tU8 u8Track;
  tU8 u8Min;
  tU8 u8Sec;
  tU8 u8Frm;
} CD_sTMSF_type;

/* CD data used bei cd_cache*/
typedef struct CD_sTrack_tag
{
  tU32 u32StartZLBA;
  tU8 u8AdrCtrl;
} CD_sTrack_type;

typedef struct CD_sTOC_tag
{
  tU8 u8Valid;
  tU8 u8MinTrack;
  tU8 u8MaxTrack;
  tU32 u32LastZLBA;
  CD_sTrack_type arTrack[CD_TRACK_ARRAY_SIZE + 1]; /*+1: satisfy l i n t*/
} CD_sTOC_type;

typedef struct CD_sInquiry_tag
{
  tU8 u8Valid;
  tU8 au8Vendor8[8 + 1];
  tU8 au8Product16[16 + 1];
  tU8 au8Revision4[4 + 1];
  tU8 au8VendorSpecific20[20 + 1];
  tU8 au8SWVersion2[2+1];
  tU8 au8HWVersion2[2+1];
} CD_sInquiry_type;

/* holds information about first CD drive*/
typedef struct CD_sDevName_tag
{
  tBool bDevFound;
  tBool bDefaultDevNameUsed;
  tBool bDevCouldBeOpened;
  char szDevName[CD_DEV_NAME_LEN];

  tBool bSRFound;
  tBool bSGFound;
  tBool bDefaultSRNameUsed;
  tBool bDefaultSGNameUsed;
  tBool bSRCouldBeOpened;
  tBool bSGCouldBeOpened;
  char szDevSRName[CD_DEV_NAME_LEN];
  char szDevSGName[CD_DEV_NAME_LEN];

} CD_tsDevName;

/*contains information about CD-Drive and Media*/
typedef struct CD_sCD_tag
{
  CD_tsDevName rCDDrive; /* device name of drive*/
  CD_sInquiry_type rInquiry;
  CD_sTOC_type rTOC;
  tU8 u8LoaderInfo;
  tU8 u8InternalMediaType;  //CD_INTERNAL_MEDIA_TYPE_XXX
  tU8 u8InternalMediaTypeValid;
  tU8 u8FSType;
  tU8 u8FSTypeValid;
  tU8 u8ForcedMixedModeMediaType; /*media type forced by user command*/
} CD_sCD_type;

/*generic Minute-Seconds-Frame type*/
typedef struct CD_sMSF_tag
{
  tU8 u8Min;
  tU8 u8Sec;
  tU8 u8Frm;
} CD_sMSF_type;

typedef struct CDAUDIO_rPlayrange_tag
{
  tU32 u32StartZLBA;
  tU32 u32EndZLBA;
  // Backup of ZLBA - to be used as start ZLBA in range
  // checking when start track is OSAL_C_U8_CDAUDIO_TRACK_NONE  
  tU32 u32PrevZLBA;
} CDAUDIO_rPlayrange_type;

typedef struct CD_rPlayInfoReg_tag
{   // Structure to store informations about registered callbacks
  // ID of Process that registered for notification
  OSAL_tProcessID ProcID;
  // Address callback function in process' address space
  OSAL_tpfCallback CD_Callback;
  // Pointer to user data, the client
  //  passed to OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY
  void *pvUserData;
  // Pointer to callback function, the client
  //  passed to OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY
  OSAL_tPlayInfoCallback funcUserCallback;
} CD_rPlayInfoReg_type;

typedef struct CD_rMediaReg_tag
{   // Structure to store informations about registered callbacks
  // ID of Process that registered for notification
  OSAL_tProcessID ProcID;
  // Address callback function in process' address space
  OSAL_tpfCallback CD_Callback;
  tU16 u16NotificationType;
  tU16 u16AppID;
  // Pointer to callback function, the client
  OSALCALLBACKFUNC funcUserCallback;
} CD_rMediaReg_type;

typedef struct CD_PLAYINFOCALLBACK_ARG_tag
{ // Structure sent to ATAPI_IF_Callback to notify
  //registered clients about events
  OSAL_trPlayInfo rPlayinfo;
  void *pvUserData; // Pointer to user data,
                    //the client passed to
                    //OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY
  OSAL_tPlayInfoCallback funcUserCallback; // Pointer to callback function,
                                      //the client passed to
                                      //OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY
} tCD_PLAYINFOCALLBACK_ARG;

typedef struct CD_MEDIACALLBACK_ARG_tag
{
  tU32 u32MediaType;
  tU16 u16NotificationType;
  OSALCALLBACKFUNC funcUserCallback;
} tCD_MEDIACALLBACK_ARG;

/*holds all data*/
typedef struct CDAUDIO_rData_tag
{
  //OSAL_tSemHandle    hSemMain;
  tBool bPlayInfoValid;
  OSAL_trPlayInfo rPlayInfo;     //playtime in TMS
  tU32 u32ZLBA;      //playtime in ZLBA
  /*playrange*/
  CDAUDIO_rPlayrange_type rCurrentPlayRangeZLBA;
  OSAL_trPlayRange rNewPlayRange;

  tU8 u8Status;
  tU8 u8PrevStatus;

} CDAUDIO_rData_type;

typedef struct CDAUDIO_sTrackCDText_tag
{
  tInt iTitleLen;
  tInt iArtistLen;
  char szTitle[CDAUDIO_CDTEXT_TEXT_BUFFER_LEN];
  char szArtist[CDAUDIO_CDTEXT_TEXT_BUFFER_LEN];
} CDAUDIO_sTrackCDText_type;

typedef struct CDAUDIO_sCDText_tag
{
  enCDAUDIO_cdtext_valid_t enValid;
  tU8 au8ATARawCDTextBuffer[CDAUDIO_ATA_CDTEXT_RAW_BUFFER_LEN];
  CDAUDIO_sTrackCDText_type saTrackText[CD_TRACK_ARRAY_SIZE];
} CDAUDIO_sCDText_type;

/*THE GLOBAL SHARED MEMORY DATA*/
typedef struct
{
  tBool CDAUDIO_bTraceOn;
  tBool CDCTRL_bTraceOn;
  tBool CD_bTraceOn;
  CD_rPlayInfoReg_type arPlayInfoReg[CD_MAX_PLAYINFO_NOTIFIERS];
  CD_rMediaReg_type arMediaReg[CD_MAX_MEDIA_NOTIFIERS];
  CD_sCD_type rCD;
  CDAUDIO_rData_type rCDAUDIO;
  CDAUDIO_sCDText_type rCDText;
  tCD_sDriveStates rDriveStatus;  //recent states of drive
} tDevCDData;
/*THE GLOBAL SHARED MEMORY DATA*/

/* used only for calculation the fingerprint of CDDA */
typedef struct
{
  CD_sTOC_type rTOC;
  CDAUDIO_sCDText_type rCDText;
} tFingerPrint;

/************************************************************************
 | variable declaration (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function prototypes (scope: global)
 |-----------------------------------------------------------------------*/
tU32 CD_u32Init(void);
tU32 CD_u32Destroy(void);
tU32 CD_u32GetShMem(tDevCDData **ppShMem, OSAL_tShMemHandle *phShMem,
                    OSAL_tSemHandle *hSem);
tVoid CD_vReleaseShMem(tDevCDData *pShMem, OSAL_tShMemHandle hShMem,
                       OSAL_tSemHandle hSem);
tU32 CD_u32RegisterPlayInfoNotifier(tDevCDData *pShMem,
                                    const OSAL_trPlayInfoReg* rPlayInfoReg);
tU32 CD_u32UnregisterPlayInfoNotifier(tDevCDData *pShMem,
                                      const OSAL_trPlayInfoReg* PlayInfoReg);
tU32 CD_u32RegisterMediaNotifier(tDevCDData *pShMem,
                                 const OSAL_trNotifyData* pReg);
tU32 CD_u32UnregisterMediaNotifier(tDevCDData *pShMem,
                                   const OSAL_trRelNotifyData* pReg);
tU32 CD_u32GetPlayStatus(tDevCDData *pShMem);
tU8 CD_u8SwitchState(tDevCDData *pShMem, tU8 u8NewState);
tU16 CD_u16GetDriveStatus(tDevCDData *pShMem, tU16 u16Type);
tU32 CD_u32PostMainEvent(tDevCDData *pShMem, OSAL_tEventMask eventMask);

#ifdef __cplusplus
}
#endif

#else /* #if !defined (CD_MAIN_H)*/
#error cd_main.h included several times
#endif /* #else #if !defined (CD_MAIN_H)*/

