/************************************************************************
| FILE:         cd_trace_common.h
| PROJECT:      GEN3
| SW-COMPONENT: CD driver
|------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cd_trace_common.h
 *
 * @brief   This file includes common trace command stuff for the cd driver.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 *
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (CD_TRACE_COMMAND_H)
#define CD_TRACE_COMMAND_H

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/*l i n t -e522*/

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif



#else /* #if !defined (CD_TRACE_COMMAND_H)*/
#error cd_trace_command.h included several times
#endif /* #else #if !defined (CD_TRACE_COMMAND_H)*/

