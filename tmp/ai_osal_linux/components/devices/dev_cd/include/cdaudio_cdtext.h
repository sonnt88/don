/************************************************************************
 | FILE:         cdaudio_cdtext.h
 | PROJECT:      Gen2
 | SW-COMPONENT: CDAUDIO driver
 |------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cdaudio_cdtext.h
 *
 * @brief   This file includes cd-text stuff for the cdaudio driver.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 *
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (CDAUDIO_CDTEXT_H)
#define CDAUDIO_CDTEXT_H

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: global)
 |-----------------------------------------------------------------------*/
#define CDAUDIO_CDTEXT_MAX_TRACK_NUMBER 99  /*track 0 - 99*/

#define CDAUDIO_CDTEXT_ATA_BUFFER_BYTES_PER_LINE  18
#define CDAUDIO_CDTEXT_ATA_BUFFER_TEXT_PER_LINE   12

#define CDAUDIO_ATA_MAX_CD_TRACK_TITLE_SIZE  128
//#define CDAUDIO_ARRAY_SIZE(ar) (sizeof(ar) / sizeof(ar[0]))

/************************************************************************
 |typedefs and struct defs (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable declaration (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function prototypes (scope: global)
 |-----------------------------------------------------------------------*/

tU32 CDAUDIO_u32CDTextInit(tDevCDData *pShMem);
tVoid CDAUDIO_vCDTextDestroy(tDevCDData *pShMem);
tVoid CDAUDIO_vCDTEXTClear(tDevCDData *pShMem);
tVoid CDAUDIO_vCDTEXTDebug(tDevCDData *pShMem);

tU32 CDAUDIO_u32GetText(tDevCDData *pShMem, OSAL_tSemHandle hSem, tU8 u8Track,
                        tU8 *pu8Title, tU8 *pu8Artist, tU32 u32TextBufferSize);

#ifdef __cplusplus
}
#endif

#else //CDAUDIO_CDTEXT_H
#error cdaudio_cdtext.h included several times
#endif //#else //CDAUDIO_CDTEXT_H
