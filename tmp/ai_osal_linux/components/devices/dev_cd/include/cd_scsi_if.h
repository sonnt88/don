/************************************************************************
 | FILE:         cd_scsi_if.h
 | PROJECT:      Gen3
 | SW-COMPONENT: CD driver
 |------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cd_scsi_if.h
 *
 * @brief   This file includes ata stuff for the cd driver.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 *
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (CD_SCSI_IF_H)
#define CD_SCSI_IF_H

/************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
 |defines and macros (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |typedefs and struct defs (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable declaration (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function prototypes (scope: global)
 |-----------------------------------------------------------------------*/
tU32 CD_SCSI_IF_u32Eject(tDevCDData *pShMem);
tU32 CD_SCSI_IF_u32Load(tDevCDData *pShMem);
tU32 CD_SCSI_IF_u32SetSleepMode(tDevCDData *pShMem, tBool bSleep);
tU32 CD_SCSI_IF_u32ReadTOC(tDevCDData *pShMem, CD_sTOC_type *prToc);
tU32 CD_SCSI_IF_u32Read(tDevCDData *pShMem, tU32 u32LBA, tU16 u16Sectors,
                        tU8 *pu8Buffer);
tU32 CD_SCSI_IF_u32Stop(tDevCDData *pShMem);
tU32 CD_SCSI_IF_u32Inquiry(tDevCDData *pShMem, CD_sInquiry_type *prInquiry);
tU32 CD_SCSI_IF_u32GetLoaderStatus(tDevCDData *pShMem,
                                   tU8 *pu8InternalLoaderStatus);
tU32 CD_SCSI_IF_u32GetTemperature(tDevCDData *pShMem, tS32 *ps32Temperature);
tU32 CD_SCSI_IF_u32TestUnitReady(tDevCDData *pShMem, tS32 *ps32UserParam);
tU32 CD_SCSI_IF_u32PlayRange(tDevCDData *pShMem, tU32 u32StartZLBA,
                             tU32 u32EndZLBA);
tU32 CD_SCSI_IF_u32PlayTNO(tDevCDData *pShMem, tU8 u32StartTrack,
                           tU8 u8EndTrack);
tU32 CD_SCSI_IF_u32Scan(tDevCDData *pShMem, tU32 u32StartZLBA, tBool bBackward);
tU32 CD_SCSI_IF_u32PauseResume(tDevCDData *pShMem, tBool bResume);
tU32 CD_SCSI_IF_u32ReadCDText(tDevCDData *pShMem, tU8 *pu8Buffer,
                              tU32 u32BufferLen, tU32 *pu32CDTextSize);
tU32 CD_SCSI_IF_u32ReadSubChannel(tDevCDData *pShMem, tCD_sSubchannel *prSub,
                                  int hOpenCD);
//tU32 CD_SCSI_IF_u32AllowMediaRemoval(tDevCDData *pShMem);
tU32 CD_SCSI_IF_u32ModeSense(tDevCDData *pShMem, tU8 *pu8MediaType);
tU32 CD_SCSI_IF_u32IsCDAccessible(tDevCDData *pShMem);
tU32 CD_SCSI_IF_u32FindCDDrive(tDevCDData *pShMem);
tU32 CD_SCSI_IF_u32Init(tDevCDData *pShMem);
tU32 CD_SCSI_IF_u32Destroy(tDevCDData *pShMem);

#ifdef __cplusplus
}
#endif

#else
#error cd_scsi_if.h included several times
#endif
