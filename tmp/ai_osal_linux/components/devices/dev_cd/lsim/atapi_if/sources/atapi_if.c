/************************************************************************
 | $Revision: 1.0 $
 | $Date: 18/04/2014 $
 |************************************************************************
 | FILE:         atapi_if.c
 | PROJECT:      GEN3
 | SW-COMPONENT: Dependency layer for OSAL CDCTRL and CDAUDIO OSAL driver
 |------------------------------------------------------------------------
 | DESCRIPTION:
 |  ATAPI_IF which is used to interact with CD/DVD drive by sending generic
 |  command and reading response. Extract interest of data from response and
 | pass it above layer.
 |------------------------------------------------------------------------
 | Date      		| Modification                        | Author
 | 18/04/2014       | This file is ported from Gen2       | sgo1cob
 | 10/06/2016       | Modified for thread synchronisation | hsr9kor
 | 07/07/2016       | Function declaration for 		      | boc7kor
 |					| u32ExecuteCallback is added         |
 |************************************************************************
 |************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/cdrom.h>

#include <alsa/asoundlib.h>

/* Basic OSAL includes */
#include "ostrace.h"
#include "atapi_if.h"
#include "atapi_if_trace.h"

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
// Define this for debug output to console
#define ATAPI_IF_DEBUG_DUMP             
#define MEDIA_STATE_NOTIFY
#define READ_DATA_IN_MSF_FORMAT
#define ATAPI_IF_THRD_NAME              "ATAPI_IF_Thrdx"
#define ATAPI_IF_SCAN_SECTORS_TO_SKIP   35
#define ATAPI_IF_C_S32_IO_VERSION       (tS32)0x00010000   /*Read as v1.0.0*/

#define ATAPI_IF_SAMPLE_BUFF_CDFRAMES   5                                                           // Size of buffer in CD's frames (sectors)
#define ATAPI_IF_SAMPLE_BUFF_SIZE       (ATAPI_IF_SAMPLE_BUFF_CDFRAMES * ATAPI_IF_CDFRAME_BYTES)    // Size of Buffer in BYTES
#define AI_DEFAULT_PERIOD_SIZE 4096
#define ATAPI_IF_CROP_FRAMES_IN_CACHE   // If defined, frames are removed from track's starttimes when caching a CD, to be compatible with MASCA hardware


//sector number (ZLBA!) for reading FS-TYPE
#define ATAPI_FS_SECTOR_LBA (16)

#define ATAPI_C8_UPPERCASE(c)  ((c < 0x7B) ? ((c >= 0x61) ? (c - 0x20) : c ) : c)

//ISO-volume descriptor types
/* Refer 
http://www.o3one.org/hwdocs/cdrom_formats/ISO-IEC_96601999_Section_two.htm 
regarding ISO filesystem */
#define ATAPI_C_U8_ISO_STD_ID_LENGTH     5
#define ATAPI_C_SZ_ISO_STD_IDENTIFIER    "CD001"

/* http://www.ecma-international.org/publications/files/ECMA-TR/TR-071.pdf 
sec 2.4.1 */
#define ATAPI_C_U8_UDFS_STD_ID_LENGTH    5
#define ATAPI_C_SZ_UDFS_STD_IDENTIFIER   "BEA01"
#define ATAPI_C_C8_SEPARATOR2            0x3B      /*';'*/


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/
typedef struct ATAPI_IF_CMD_PACKET
{
    struct cdrom_generic_command    cmd;        // Buffer for ATAPI command, see ioctl/cdrom.txt
    struct request_sense            sense;      // Buffer for ATAPI sense, see ioctl/cdrom.txt
    unsigned char                   buff[4096]; // Buffer for ATAPI response, see ATAPI spec
} ATAPI_IF_CMD_PACKET;

typedef struct ATAPI_IF_CDTEXT_DESCRIPTOR
{
    unsigned char   PackType;       // Type of entry: Title/Performer/Composer/...
    unsigned char   TrackNumber;    // Track, this entry belongs to
    unsigned char   SequenceNumber; // Index of element in it's block
    unsigned char   BlockNumber;    // Index of Block (1 block per language) this element belongs to
    union
    { // CDText might contain characters in 8 or 16 bit format, OSAL has no mechanism to communicate that
        unsigned char   Text8[12];  // 8 bit characters
        unsigned short  Text16[6];  // 16 bit characters
    } TextData;
    unsigned char   CRC[2];         // CRC, might not be supported by drive, see ATAPI spec
} ATAPI_IF_CDTEXT_DESCRIPTOR;

/************************************************************************
| variable inclusion  (scope: global)
|-----------------------------------------------------------------------*/
extern tBool CDAUDIO_bTraceOn;

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static OSAL_tSemHandle      ATAPI_IF_SemHandle[ATAPI_IF_DRIVE_NUMBER] = { OSAL_ERROR };
static OSAL_tShMemHandle    ATAPI_IF_ShmHandle[ATAPI_IF_DRIVE_NUMBER] = { OSAL_ERROR };

tChar semname[] = ATAPI_IF_SEM_NAME;
OSAL_tSemHandle semhandle;
//static ATAPI_IF_PROC_INFO   *ATAPI_IF_ProcInfo[ATAPI_IF_DRIVE_NUMBER] = { NULL };

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
tU32 u32ExecuteCallback(tS32 s32OwnPrcId, tS32 callbackpid,
                        OSAL_tpfCallback pcallback, tPVoid pcallbackarg,
                        tU32 callbackargsize);//fix for CMG3GB-3460
						
/************************************************************************
|function prototype (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/


/******************************************************************************
 *FUNCTION      :GetStatusName
 *
 *DESCRIPTION   :Converts an Enum of type ATAPI_IF_STREAM_STATUS to a string in
 *               human readable form for debugging.
 *
 *PARAMETER     :Status Status
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static const char *GetStatusName(ATAPI_IF_STREAM_STATUS Status)
{
    switch(Status)
    {
    case ATAPI_IF_STRM_STATUS_NO_DISK:          return "NO_DISK";
    case ATAPI_IF_STRM_STATUS_STOP:             return "STOP";
    case ATAPI_IF_STRM_STATUS_PLAY:             return "PLAY";
    case ATAPI_IF_STRM_STATUS_PAUSE:            return "PAUSE";
    case ATAPI_IF_STRM_STATUS_FAST_FORWARD:     return "FAST_FWD";
    case ATAPI_IF_STRM_STATUS_FAST_BACKWARD:    return "FAST_BWD";
    case ATAPI_IF_STRM_STATUS_SHUTDOWN:         return "SHUTDOWN";
    case ATAPI_IF_STRM_REQUEST_PLAY_RANGE:      return "PLAY_RANGE";
    case ATAPI_IF_STRM_REQUEST_RESUME:          return "RESUME";
    }
    return "Unknown Status";
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_Callback
 *
 *DESCRIPTION   :Function to be called by OSAL for callbacks.
 *               This in turn calls the client callback, registered via OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY.
 *               This additional indirection is necessary, because the notification callback expects two parameters, while OSAL provides only one
 *
 *PARAMETER     :Arg    Pointer to data for notification callback
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
void ATAPI_IF_PlayCallback(void *Arg)
{
    ATAPI_IF_PLAY_CALLBACK_ARG   *arg = (ATAPI_IF_PLAY_CALLBACK_ARG *)Arg;
    OSAL_trPlayInfo         playinfo;

    ATAPI_IF_LBA2MSF_OSAL_Addr(&playinfo.rAbsTrackAdr, arg->AbsLBA);
    playinfo.u32TrackNumber = arg->Trck;
    ATAPI_IF_LBA2MSF_OSAL_Addr(&playinfo.rRelTrackAdr, arg->RelLBA);

    switch(arg->Status)
    {
    case ATAPI_IF_STRM_STATUS_STOP:
        playinfo.u32StatusPlay = OSAL_C_S32_PLAY_COMPLETED;
        break;
    case ATAPI_IF_STRM_STATUS_PAUSE:
        playinfo.u32StatusPlay = OSAL_C_S32_PLAY_PAUSED;
        break;
    case ATAPI_IF_STRM_STATUS_PLAY:
    case ATAPI_IF_STRM_STATUS_FAST_FORWARD:
    case ATAPI_IF_STRM_STATUS_FAST_BACKWARD:
        playinfo.u32StatusPlay = OSAL_C_S32_PLAY_IN_PROGRESS;
        break;
    default:
        playinfo.u32StatusPlay = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;
        break;
    }

    arg->UserCallback(arg->UserData, &playinfo);
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_PlayNotifyClients
 *
 *DESCRIPTION   :Called by ATAPI_IF_UpdateThread.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *               Calls all registered callbacks.
 *
 *PARAMETER     :DriveIdx   Index of drive
 *               Info       Informations about current status of
 *                          ATAPI_IF_UpdateThread
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static tVoid ATAPI_IF_PlayNotifyClients(tU32 DriveIdx, ATAPI_IF_THRD_INFO *Info)
{
    ATAPI_IF_PLAY_CALLBACK_ARG   arg;
    tU32                    i;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_PlayNotifyClients,
                            DriveIdx, Info->ThrdPlayStatus.Status, Info->ThrdPlayStatus.CurrentLBA, 0);

    arg.Status = Info->ThrdPlayStatus.Status;
    arg.AbsLBA = Info->ThrdPlayStatus.CurrentLBA;
    arg.Trck = ATAPI_IF_LBA2TRACK(&Info->ProcInfo->CDCache, Info->ThrdPlayStatus.CurrentLBA);
    if(arg.Trck > 0)
    {
        arg.RelLBA = Info->ThrdPlayStatus.CurrentLBA - Info->ProcInfo->CDCache.TrckStartLBA[arg.Trck - 1];
    }
    else
    {
        arg.RelLBA = 0;
    }
    for(i = 0; i < ATAPI_IF_MAX_NOTIFIERS; i++)
    {
        if(Info->ProcInfo->AudioPlayCallbacks[i].UserCallback)
        {
            arg.UserData = Info->ProcInfo->AudioPlayCallbacks[i].UserData;
            arg.UserCallback = Info->ProcInfo->AudioPlayCallbacks[i].UserCallback;
            u32ExecuteCallback(OSAL_ProcessWhoAmI(), 
                  Info->ProcInfo->AudioPlayCallbacks[i].ProcID, 
                  Info->ProcInfo->AudioPlayCallbacks[i].ATAPI_IF_Callback, 
                  &arg, sizeof(arg));
        }
    }

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_PlayNotifyClients,
                           0,
                           DriveIdx,
                           0,0,0);
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_FindPlayNotifier
 *
 *DESCRIPTION   :Searches Info for an entry that matches prReg.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *
 *PARAMETER     :Info       Informations about current status of
 *                          ATAPI_IF_UpdateThread
 *               prReg      Structure that describes the callback to find
 *
 *RETURNVALUE   :-1 if prReg is not found
 *               Index of matching entry in Info->AudioPlayCallbacks
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static tInt ATAPI_IF_FindPlayNotifier(ATAPI_IF_PROC_INFO *Info, 
                                       const OSAL_trPlayInfoReg* prReg)
{
   tInt nPos = -1;
   tInt nIdx;

   ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_FindPlayNotifier, prReg, 0, 0, 0);

   for(nIdx = 0; (nIdx < ATAPI_IF_MAX_NOTIFIERS) && (nPos < 0); ++nIdx)
   {
      if((Info->AudioPlayCallbacks[nIdx].UserData == prReg->pvCookie) &&
         (Info->AudioPlayCallbacks[nIdx].UserCallback == prReg->pCallback))
      {
         nPos = nIdx;
      }
   }

   ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_FindPlayNotifier, nPos, 0, 0, 0, 0);

   return nPos;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_RegisterNotifier
 *
 *DESCRIPTION   :Registers a callback for notification.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *
 *PARAMETER     :Info           Informations about current status of
 *                              ATAPI_IF_UpdateThread
 *               PlayInfoReg    Structure that describes the callback to register
 *
 *RETURNVALUE   :OSAL_E_ALREADYEXISTS   if PlayInfoReg is already registered,
 *               OSAL_E_NOSPACE         if Info->AudioPlayCallbacks is full, or
 *               OSAL_E_NOERROR         if PlayInfoReg was registered
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
tS32 ATAPI_IF_RegisterPlayNotifier(ATAPI_IF_PROC_INFO *Info, 
                                 const OSAL_trPlayInfoReg* PlayInfoReg)
{
    tS32    result = OSAL_E_NOSPACE;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_RegisterPlayNotifier, 
                                             PlayInfoReg, 0, 0, 0);

    if(ATAPI_IF_FindPlayNotifier(Info, PlayInfoReg) != -1)
    {
        result = OSAL_E_ALREADYEXISTS;
    }
    else
    {
        tU32    i;

        for(i = 0; i < ATAPI_IF_MAX_NOTIFIERS; i++)
        {
            if(Info->AudioPlayCallbacks[i].UserCallback == NULL)
            {
                Info->AudioPlayCallbacks[i].ProcID = OSAL_ProcessWhoAmI();
                Info->AudioPlayCallbacks[i].ATAPI_IF_Callback 
                                                = ATAPI_IF_PlayCallback;
                Info->AudioPlayCallbacks[i].UserData = PlayInfoReg->pvCookie;
                Info->AudioPlayCallbacks[i].UserCallback 
                                                = PlayInfoReg->pCallback;
                result = OSAL_E_NOERROR;
                break;
            }
        }
    }

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_RegisterPlayNotifier, result, 0, 0, 0, 0);

    return result;
}

/******************************************************************************
 *FUNCTION      :ATAPI_IF_UnregisterNotifier
 *
 *DESCRIPTION   :Removes an entry from Info->AudioPlayCallbacks.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *
 *PARAMETER     :Info           Informations about current status of
 *                              ATAPI_IF_UpdateThread
 *               PlayInfoReg    Structure that describes the callback to remove
 *
 *RETURNVALUE   :OSAL_E_DOESNOTEXIST    if no matching entry was found or,
 *               OSAL_E_NOERROR         if the entry was removed
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
tS32 ATAPI_IF_UnregisterPlayNotifier(ATAPI_IF_PROC_INFO *Info, 
                                       const OSAL_trPlayInfoReg* PlayInfoReg)
{
    tS32    result = OSAL_E_NOERROR;
    tS32    idx;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_UnregisterNotifier, PlayInfoReg, 0, 0, 0);

    idx = ATAPI_IF_FindPlayNotifier(Info, PlayInfoReg);
    if(idx == -1)
    {
        result = OSAL_E_DOESNOTEXIST;
    }
    else
    {
        Info->AudioPlayCallbacks[idx].UserCallback = NULL;
    }

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_UnregisterNotifier, result, 0, 0, 0, 0);

    return result;
}

#ifdef MEDIA_STATE_NOTIFY
/******************************************************************************
*FUNCTION      :ATAPI_IF_MediaCallback
*
*DESCRIPTION   :Function to be called by OSAL for callbacks.
*               This in turn calls the client callback, registered via 
*               OSAL_C_S32_IOCTRL_REG_NOTIFICATION.
*
*PARAMETER     :Arg    Pointer to data for notification callback
*
*RETURNVALUE   :none
*
*HISTORY:      :Created by sgo1cob
*****************************************************************************/
void ATAPI_IF_MediaCallback(void *Arg)
{
   ATAPI_IF_MEDIA_CALLBACK_ARG   *arg = (ATAPI_IF_MEDIA_CALLBACK_ARG *)Arg;
   arg->UserCallback(&arg->u32MediaType);
}

/******************************************************************************
*FUNCTION      :AIF_vMediaNotifyClients
*
*DESCRIPTION   :Called by ATAPI_IF_UpdateThread.
*               Accesses shared memory and must be protected by a semaphore
*               outside this function.
*               Calls all registered callbacks.
*
*PARAMETER     :DriveIdx   Index of drive
*               Info       Informations about current status of
*                          ATAPI_IF_UpdateThread
*
*RETURNVALUE   :none
*
*HISTORY:      :Ported from Gen2 by sgo1cob
*****************************************************************************/
static tVoid AIF_vMediaNotifyClients(ATAPI_IF_PROC_INFO *Info,
                              tU16 u16Notification,const tU32 u32MediaStatus)
{
   ATAPI_IF_MEDIA_CALLBACK_ARG   arg;
   tU32                    u32Counter;

   ATAPI_IF_TRACE_ENTER_U4(AIF_vMediaNotifyClients
   ,u16Notification, u32MediaStatus, 0,0);

   for(u32Counter = 0; u32Counter < ATAPI_IF_MAX_NOTIFIERS; u32Counter++)
   {
      ATAPI_IF_PRINTF_U4("search callback %x",u32Counter);
      if( (Info->arMediaStatusCallbacks[u32Counter].UserCallback) &&
            (0 != (u16Notification & 
            Info->arMediaStatusCallbacks[u32Counter].u16NotificationType)))
      {
         ATAPI_IF_PRINTF_U4("Found callback: execute %x",u32Counter);
         arg.u32MediaType = ((tU32)u16Notification<<16) | u32MediaStatus;
         arg.UserCallback 
               = Info->arMediaStatusCallbacks[u32Counter].UserCallback;
         u32ExecuteCallback(OSAL_ProcessWhoAmI(), 
               Info->arMediaStatusCallbacks[u32Counter].ProcID, 
               Info->arMediaStatusCallbacks[u32Counter].ATAPI_IF_Callback, 
               &arg, sizeof(arg));
      }
   }

   ATAPI_IF_TRACE_LEAVE_U4(AIF_vMediaNotifyClients,
   0,
   0,
   0,0,0);
}



/******************************************************************************
*FUNCTION      :AIF_s32FindMediaNotifier
*
*DESCRIPTION   :Searches Info for an entry that matches prReg.
*               Accesses shared memory and must be protected by a semaphore
*               outside this function.
*
*PARAMETER     :Info       Informations about current status of
*                          ATAPI_IF_UpdateThread
*               prFindRegCallback      Structure that describes the callback to find
*
*RETURNVALUE   :-1 if prFindRegCallback is not found
*               Index of matching entry in Info->arMediaStatusCallbacks
*
*HISTORY:      :Ported from Gen2 by sgo1cob
*****************************************************************************/
static tS32 AIF_s32FindMediaNotifier(ATAPI_IF_PROC_INFO *Info, 
                                          const OSAL_trRelNotifyData* prFindRegCallback)
{
   tS32 s32FoundPosition = -1;
   tS32 s32LocCounter;

   ATAPI_IF_TRACE_ENTER_U4(AIF_s32FindMediaNotifier, prFindRegCallback, 0, 0, 0);

   for(s32LocCounter = 0; (s32LocCounter < ATAPI_IF_MAX_NOTIFIERS) && 
                                     (s32FoundPosition < 0); ++s32LocCounter)
   {
      if(Info->arMediaStatusCallbacks[s32LocCounter].u16AppID 
                                             == prFindRegCallback->u16AppID)
      {
         s32FoundPosition = s32LocCounter;
      }
   }

   ATAPI_IF_TRACE_LEAVE_U4(AIF_s32FindMediaNotifier, 
                              s32FoundPosition, 0, 0, 0, 0);

   return s32FoundPosition;
}



/******************************************************************************
*FUNCTION      :AIF_u32RegisterMediaNotifier
*
*DESCRIPTION   :Registers a callback for notification.
*               Accesses shared memory and must be protected by a semaphore
*               outside this function.
*
*PARAMETER     :Info          Informations about current status of
*                             CD_UpdateThread
*               g   Structure that describes the callback to register
*
*RETURNVALUE   :OSAL_E_ALREADYEXISTS   if PlayInfoReg is already registered,
*               OSAL_E_NOSPACE  if Info->arMediaStatusCallbacks is full, or
*               OSAL_E_NOERROR         if PlayInfoReg was registered
*
*HISTORY:      :Ported from Gen2 by sgo1cob
*****************************************************************************/
tU32 AIF_u32RegisterMediaNotifier(ATAPI_IF_PROC_INFO *Info,
                                    const OSAL_trNotifyData* pReg)
{
   tU32 u32Ret = OSAL_E_NOSPACE;
   tS32 s32Counter=-1;
   OSAL_trRelNotifyData rNotifyData;
   ATAPI_IF_TRACE_ENTER_U4(AIF_u32RegisterMediaNotifier, pReg, 0, 0, 0);

   if(NULL != pReg)
   {
      rNotifyData.ResourceName=pReg->ResourceName;
      rNotifyData.u16AppID = pReg->u16AppID;
      rNotifyData.u16NotificationType = pReg->u16NotificationType;

      s32Counter = AIF_s32FindMediaNotifier(Info, &rNotifyData);
      if(s32Counter != -1)
      { // notifier exist, update Type
         Info->arMediaStatusCallbacks[s32Counter].ProcID = OSAL_ProcessWhoAmI();
         Info->arMediaStatusCallbacks[s32Counter].ATAPI_IF_Callback 
                                                      = ATAPI_IF_MediaCallback;
         Info->arMediaStatusCallbacks[s32Counter].UserCallback     
                                                              = pReg->pCallback;
         Info->arMediaStatusCallbacks[s32Counter].u16AppID            
                                                      = pReg->u16AppID;
         Info->arMediaStatusCallbacks[s32Counter].u16NotificationType  
                                                   |= pReg->u16NotificationType;
         u32Ret = OSAL_E_NOERROR;
      }
      else //if(s32Counter != -1)
      {
         // notifier not exist, search free entry
         s32Counter = 0;
         while( s32Counter < ATAPI_IF_MAX_NOTIFIERS )
         {
            if(Info->arMediaStatusCallbacks[s32Counter].u16AppID 
                                                == OSAL_C_U16_INVALID_APPID)
            {
               Info->arMediaStatusCallbacks[s32Counter].ProcID = 
                                                      OSAL_ProcessWhoAmI();
               Info->arMediaStatusCallbacks[s32Counter].ATAPI_IF_Callback         
                                                      = ATAPI_IF_MediaCallback;
               Info->arMediaStatusCallbacks[s32Counter].UserCallback    
                                                             = pReg->pCallback;
               Info->arMediaStatusCallbacks[s32Counter].u16NotificationType 
                                                   = pReg->u16NotificationType;
               Info->arMediaStatusCallbacks[s32Counter].u16AppID            
                                                              = pReg->u16AppID;
               u32Ret = OSAL_E_NOERROR;
               s32Counter = ATAPI_IF_MAX_NOTIFIERS; //break the loop
            } 
            s32Counter++;
         } 
      } //else //if(s32Counter != -1)

      //notify client first time with recent states
      // call callback directly, because we are in precess context of caller
      if(u32Ret == OSAL_E_NOERROR)
      {
         tU32 u32RequestType = (tU32)pReg->u16NotificationType;
         tU32 u32Type;
         tU32 u32Status;
         tU32 u32Par;

         u32Type = (tU32)OSAL_C_U16_NOTI_DEVICE_READY;
         if(0 != (u32RequestType  & u32Type))
         {
            u32Status = Info->DriveStateInfo.u32DeviceReady & 0xFFFF;
            u32Par = (u32Type << 16) | u32Status;
            pReg->pCallback(&u32Par);
         } //if(0 != (u16T  & ...))

         u32Type = (tU32)OSAL_C_U16_NOTI_MEDIA_CHANGE;
         if(0 != (u32RequestType  & u32Type))
         {
            u32Status = Info->DriveStateInfo.u32MediaChange & 0xFFFF;
            u32Par = (u32Type << 16) | u32Status;
            pReg->pCallback(&u32Par);
         } //if(0 != (u16T  & ...))

         u32Type = (tU32)OSAL_C_U16_NOTI_MEDIA_STATE;
         if(0 != (u32RequestType  & u32Type))
         {
            u32Status = Info->DriveStateInfo.u32MediaState & 0xFFFF;
            u32Par = (u32Type << 16) | u32Status;
            pReg->pCallback(&u32Par);
         } //if(0 != (u16T  & ...))


         u32Type = (tU32)OSAL_C_U16_NOTI_TOTAL_FAILURE;
         if(0 != (u32RequestType  & u32Type))
         {
            u32Status = Info->DriveStateInfo.u32TotalFailure & 0xFFFF;
            u32Par = (u32Type << 16) | u32Status;
            pReg->pCallback(&u32Par);
         } //if(0 != (u16T  & ...))

         u32Type = (tU32)OSAL_C_U16_NOTI_DEFECT;
         if(0 != (u32RequestType  & u32Type))
         {
            if(Info->DriveStateInfo.u32Defect != 
                              AIF_LDR_STATUS_NOT_INITIALIZED)
            {
               u32Status = Info->DriveStateInfo.u32Defect & 0xFFFF;
               u32Par = (u32Type << 16) | u32Status;
               pReg->pCallback(&u32Par);
            } //if(u32Status != AIF_LDR_STATUS_NOT_INITIALIZED)
         } //if(0 != (u16T  & ...))
      } //if(u32Ret == OSAL_E_NOERROR)

   }
   else //if(NULL != pReg)
   {
      u32Ret = OSAL_E_INVALIDVALUE;
   } //else //if(NULL != pReg)


   ATAPI_IF_TRACE_LEAVE_U4(AIF_u32RegisterMediaNotifier, u32Ret, 
                                                   s32Counter, 0, 0, 0);

   return u32Ret;
}


/******************************************************************************
*FUNCTION      :AIF_s32UnregisterMediaNotifier
*
*DESCRIPTION   :Removes an entry from Info->arMediaStatusCallbacks.
*               Accesses shared memory and must be protected by a semaphore
*               outside this function.
*
*PARAMETER     :Info           Informations about current status of
*                              ATAPI_IF_UpdateThread
*               PlayInfoReg    Structure that describes the callback to remove
*
*RETURNVALUE   :OSAL_E_DOESNOTEXIST    if no matching entry was found or,
*               OSAL_E_NOERROR         if the entry was removed
*
*HISTORY:      :Ported from Gen2 by sgo1cob
*****************************************************************************/
tS32 AIF_s32UnregisterMediaNotifier(ATAPI_IF_PROC_INFO *Info, 
                                    const OSAL_trRelNotifyData* rRelNotifyData)
{
   tS32    result = OSAL_E_NOERROR;
   tS32    idx;

   ATAPI_IF_TRACE_ENTER_U4(AIF_s32UnregisterMediaNotifier, 
                                                   rRelNotifyData, 0, 0, 0);

   idx = AIF_s32FindMediaNotifier(Info, rRelNotifyData);
   if(idx == -1)
   {
      result = OSAL_E_DOESNOTEXIST;
   }
   else
   {
      Info->arMediaStatusCallbacks[idx].u16NotificationType &=
                                    (~ (rRelNotifyData->u16NotificationType));
      if(Info->arMediaStatusCallbacks[idx].u16NotificationType == 0)
      {
         Info->arMediaStatusCallbacks[idx].UserCallback = NULL;
         Info->arMediaStatusCallbacks[idx].ATAPI_IF_Callback = NULL;
         Info->arMediaStatusCallbacks[idx].ProcID =0;
         Info->arMediaStatusCallbacks[idx].u16AppID=OSAL_C_U16_INVALID_APPID;

      }
   }

   ATAPI_IF_TRACE_LEAVE_U4(AIF_s32UnregisterMediaNotifier, result, 
                                                               0, 0, 0, 0);

   return result;
}

#endif
#ifndef ATAPI_IF_DEBUG_DUMP
#define ATAPI_IF_DEBUG_PRINTF(...)
#define ATAPI_IF_DEBUG_HEXDUMP(_TEXT, _BUFF, _LEN)
#define ATAPI_IF_DUMP_PLAYSTATUS(_TEXT, _STATUS)
#else //ATAPI_IF_DEBUG_DUMP
#define ATAPI_IF_DEBUG_PRINTF(...)                  printf(__VA_ARGS__)
#define ATAPI_IF_DEBUG_HEXDUMP(_TEXT, _BUFF, _LEN)  HexDump(_TEXT, _BUFF, _LEN)
#define ATAPI_IF_DUMP_PLAYSTATUS(_TEXT, _STATUS)             DumpPlayStatus(_TEXT, _STATUS)

/******************************************************************************
 *FUNCTION      :HexDump
 *
 *DESCRIPTION   :Prints a hexdump of the given memory for debugging.
 *
 *PARAMETER     :Buff   Pointer to memory to dump
 *               Len    Number of bytes to dump
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static void HexDump(const char *Text, const void *Buff, int Len)
{
    const unsigned char *buff = (const unsigned char *)Buff;
    int                 i;

    printf("%s\n", Text);
    for(i = 0; i < Len; i+=32)
    {
        int j;

        printf("%04x:  ", i);

        for(j = 0; j < 16 && i + j < Len; j++)
            printf("%02x ", (int)buff[i + j]);
        printf("- ");
        for(; j < 32 && i + j < Len; j++)
            printf("%02x ", (int)buff[i + j]);
        for(; j < 32; j++)
            printf("   ");

        printf(" :  ");

        for(j = 0; j < 16 && i + j < Len; j++)
            printf("%c", (buff[i + j] >= ' ')?buff[i + j]:'.');
        printf(" - ");
        for(; j < 32 && i + j < Len; j++)
            printf("%c", (buff[i + j] >= ' ')?buff[i + j]:'.');

        printf("\n");
    }
}



/******************************************************************************
 *FUNCTION      :DumpPlayStatus
 *
 *DESCRIPTION   :Prints parts of ATAPI_IF_PLAY_STATUS in a human readable form
 *               for debugging.
 *
 *PARAMETER     :Text       Some text to print
 *               PlayStatus Pointer to status
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static void DumpPlayStatus(const char *Text, ATAPI_IF_PLAY_STATUS *PlayStatus)
{
    // FIXME: Use GetStatusName(ATAPI_IF_STREAM_STATUS Status)
    printf("%s ", Text);
    switch(PlayStatus->Status)
    {
    case ATAPI_IF_STRM_STATUS_NO_DISK:          printf("NO_DISK");      break;
    case ATAPI_IF_STRM_STATUS_STOP:             printf("STOP");         break;
    case ATAPI_IF_STRM_STATUS_PLAY:             printf("PLAY");         break;
    case ATAPI_IF_STRM_STATUS_PAUSE:            printf("PAUSE");        break;
    case ATAPI_IF_STRM_STATUS_FAST_FORWARD:     printf("FAST_FWD");     break;
    case ATAPI_IF_STRM_STATUS_FAST_BACKWARD:    printf("FAST_BWD");     break;
    case ATAPI_IF_STRM_STATUS_SHUTDOWN:         printf("SHUTDOWN");     break;
    case ATAPI_IF_STRM_REQUEST_PLAY_RANGE:      printf("PLAY_RANGE");   break;
    case ATAPI_IF_STRM_REQUEST_RESUME:          printf("RESUME");       break;
    }
    if(PlayStatus->NewStartLBA == 0)
     printf(" without timechange\n");
    else
     printf(" NewStartLBA: %i, NewEndLBA: %i\n", PlayStatus->NewStartLBA, PlayStatus->NewEndLBA);
}
#endif //ATAPI_IF_DEBUG_DUMP



/******************************************************************************
 *FUNCTION      :ATAPI_IF_LBA2MSF
 *
 *DESCRIPTION   :Converts a Logical Block Address to Minute Second and Frame.
 *
 *PARAMETER     :Min    Pointer to tU32 to store Minute
 *               Sec    Pointer to tU32 to store Second
 *               Frm    Pointer to tU32 to store Frame
 *               LBA    Logical Block Address to convert
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
void ATAPI_IF_LBA2MSF(tU32 *Min, tU32 *Sec, tU32 *Frm, tU32 LBA)
{
    tU32    min, sec, frm;

    min = LBA / (60 * 75);
    LBA -= min * 60 * 75;
    sec = LBA / 75;
    LBA -= sec * 75;
    frm = LBA;

    if(Min)     *Min = min;
    if(Sec)     *Sec = sec;
    if(Frm)     *Frm = frm;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_LBA2MSF_OSAL_Addr
 *
 *DESCRIPTION   :Converts a Logical Block Address to Minute Second and Frame
 *               and stores the result in an OSAL_trMSFAddress.
 *
 *PARAMETER     :MSFAddr    Pointer to OSAL_trMSFAddress structure to store MFS
 *               LBA        Logical Block Address to convert
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
void ATAPI_IF_LBA2MSF_OSAL_Addr(OSAL_trMSFAddress *MSFAddr, tU32 LBA)
{
    tU32    min, sec, frm;

    min = LBA / (60 * 75);
    LBA -= min * 60 * 75;
    sec = LBA / 75;
    LBA -= sec * 75;
    frm = LBA;

    MSFAddr->u8MSFMinute = (tU8)min;
    MSFAddr->u8MSFSecond = (tU8)sec;
    MSFAddr->u8MSFFrame = (tU8)frm;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_MSF2LBA
 *
 *DESCRIPTION   :Converts Minute Second and Frame to a Logical Block Address.
 *
 *PARAMETER     :Min    Minute
 *               Sec    Second
 *               Frm    Frame
 *
 *RETURNVALUE   :Logical Block Address
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
tU32 ATAPI_IF_MSF2LBA(tU32 Min, tU32 Sec, tU32 Frm)
{
    return Min * 60 * 75 + Sec * 75 + Frm;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_OSAL_TrckPos2LBA
 *
 *DESCRIPTION   :Converts the content of an OSAL_trTrackPosition structure to a
 *               Logical Block Address.
 *               Probably accesses shared memory (Cache) and should be protected
 *               by a semaphore outside this function.
 *
 *PARAMETER     :Cache      Pointer to cached CD content
 *               TrackPos   Pointer to OSAL structure containing track index and
 *                          offset
 *
 *RETURNVALUE   :Logical Block Address or
 *               0 if track index is invalid
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
tU32 ATAPI_IF_OSAL_TrckPos2LBA(ATAPI_IF_CDCACHE *Cache, OSAL_trTrackPosition *TrackPos)
{
    if(TrackPos->u8Track == OSAL_C_U8_CDAUDIO_TRACK_NONE)
        return ATAPI_IF_LBA_USE_CURRENT;

    if(TrackPos->u8Track < Cache->MinTrck || TrackPos->u8Track > Cache->MaxTrck)
    {
        return 0;
    }

    if(TrackPos->u16Offset == OSAL_C_U16_CDAUDIO_OFFSET_TRACK_END)
        return ATAPI_IF_LBA_USE_END_OF_TRACK | (TrackPos->u8Track - 1);

    return Cache->TrckStartLBA[TrackPos->u8Track - 1] + TrackPos->u16Offset * 75;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_LBA2TRACK
 *
 *DESCRIPTION   :Determines the track to which a Logical Block Address belongs.
 *               Probably accesses shared memory (Cache) and should be protected
 *               by a semaphore outside this function.
 *
 *PARAMETER     :Cache  Pointer to cached CD content
 *               LBA    Logical Block Address
 *
 *RETURNVALUE   :Track index or 0 if LBA is invalid
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
tU32 ATAPI_IF_LBA2TRACK(ATAPI_IF_CDCACHE *Cache, tU32 LBA)
{
    int i;

    if(LBA < Cache->TrckStartLBA[Cache->MinTrck - 1] || LBA >= Cache->TrckStartLBA[ATAPI_IF_LEADOUT_INDEX])
    {
        return 0;
    }

    for(i = Cache->MinTrck; i <= Cache->MaxTrck; i++)
    {
        if(LBA < Cache->TrckStartLBA[i])
        {
            return i;
        }
    }

    return 0;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_TranslateSpecialLBA
 *
 *DESCRIPTION   :Translates a special LBA if it has the ATAPI_IF_LBA_USE_CURRENT
 *               or ATAPI_IF_LBA_USE_END_OF_TRACK flag set.
 *               Probably accesses shared memory (Cache) and should be protected
 *               by a semaphore outside this function.
 *
 *PARAMETER     :Cache  Pointer to cached CD content
 *               Status Current playback status
 *               LBA    Logical Block Address
 *
 *RETURNVALUE   :Modified LBA
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
tU32 ATAPI_IF_TranslateSpecialLBA(ATAPI_IF_CDCACHE *Cache, ATAPI_IF_PLAY_STATUS *Status, tU32 LBA)
{
    if(LBA == ATAPI_IF_LBA_USE_CURRENT)
    {
        return Status->CurrentLBA;
    }

    if(LBA & ATAPI_IF_LBA_USE_END_OF_TRACK)
    {
        int trck;

        if(!Cache->ValidFlag)
            return 0;

        trck = LBA & ATAPI_IF_LBA_USE_END_OF_TRACK_MASK;
        if(trck < Cache->MinTrck || trck >= Cache->MaxTrck)
            return 0;

        return Cache->TrckStartLBA[trck + 1] - 1;
    }

    return LBA;
}



/******************************************************************************
 *FUNCTION      :InitCmdPacket
 *
 *DESCRIPTION   :Initializes a ATAPI_IF_CMD_PACKET structure to be used with an
 *               ATAPI command, sent via ioctl using CDROM_SEND_PACKET.
 *
 *PARAMETER     :Packet             Pointer to structure to be initialized
 *               BufferSizeOffset   Position in command where the size of the
 *                                  buffer is stored - depends on ATAPI command.
 *                                  Set to -1 if the ATAPI command doesn't
 *                                  require it.
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static void InitCmdPacket(ATAPI_IF_CMD_PACKET *Packet, tS32 BufferSizeOffset)
{
    memset(&Packet->cmd, 0, sizeof(Packet->cmd));
    memset(&Packet->sense, 0, sizeof(Packet->sense));
    memset(&Packet->buff, 0, sizeof(Packet->buff));

    if(BufferSizeOffset >= 0)
    {
        Packet->cmd.cmd[BufferSizeOffset + 0] = sizeof(Packet->buff) >> 8;
        Packet->cmd.cmd[BufferSizeOffset + 1] = sizeof(Packet->buff) & 0xff;
    }

    Packet->cmd.buffer = Packet->buff;
    Packet->cmd.buflen = sizeof(Packet->buff);
//    Packet->cmd.stat = ???;   // CHECKME: Any usecases where this should be != 0?
    Packet->cmd.sense = &Packet->sense;
    Packet->cmd.data_direction = CGC_DATA_READ;
    Packet->cmd.quiet = 1;
    Packet->cmd.timeout = 100;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_GetCDText
 *
 *DESCRIPTION   :Decodes CD Text from a buffer returned by
 *               GPCMD_READ_TOC_PMA_ATIP
 *
 *PARAMETER     :OutText    Pointer to buffer to store text, contains "\000" if
 *                          no text of the requested type is available for the
 *                          track
 *               InBuff     Pointer to buffer provided by GPCMD_READ_TOC_PMA_ATIP
 *               DataLength Size of InBuff in bytes
 *               PackType   Type of packet to read, e.g. 0x80 for title,
 *                          0x81 for artist... see ATAPI spec
 *               Track      Index of track to retrieve text for
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static void ATAPI_IF_GetCDText(unsigned char *OutText, unsigned char *InBuff, tU32 DataLength, unsigned char PackType, unsigned char Track)
{
    tU32    off = 0;
    tU32    n;

    memset(OutText, 0, OSAL_C_S32_CDAUDIO_MAXNAMESIZE);
    for(n = 4; n + sizeof(ATAPI_IF_CDTEXT_DESCRIPTOR) < DataLength; n += sizeof(ATAPI_IF_CDTEXT_DESCRIPTOR))
    {
        ATAPI_IF_CDTEXT_DESCRIPTOR  *text_desc = (ATAPI_IF_CDTEXT_DESCRIPTOR *)(InBuff + n);
        if(text_desc->PackType == PackType)
        {
            tU32                dbcc = text_desc->BlockNumber & 0x80;
            tU32                trckidx = text_desc->TrackNumber & 0x7f;
            tU32                i;

            if(trckidx > Track)
                return;

            i = 0;
            while(trckidx < Track)
            {
                if(dbcc == 0)
                {
                    if(i >= 12)
                        break;

                    if(text_desc->TextData.Text8[i++] == 0)
                        trckidx++;
                }
                else
                {
                    if(i >= 6)
                        break;

                    if(text_desc->TextData.Text16[i++] == 0)
                        trckidx++;
                }
            }

            if(dbcc == 0 && i >= 12)
                continue;
            if(dbcc != 0 && i >= 6)
                continue;

            if(trckidx == Track)
            {
                if(dbcc == 0)
                {
                    while(i < 12)
                    {
                        if(text_desc->TextData.Text8[i] == 0)
                        {
                            trckidx++;
                            break;
                        }

                        OutText[off++] = text_desc->TextData.Text8[i++];
                    }
                }
                else
                {
                    while(i < 6)
                    {
                        if(text_desc->TextData.Text16[i] == 0)
                        {
                            trckidx++;
                            break;
                        }

                        OutText[off++] = text_desc->TextData.Text16[i++];
                    }
                }
            }
        }
    }

    OutText[OSAL_C_S32_CDAUDIO_MAXNAMESIZE-1] = 0;
}


/*****************************************************************************
* FUNCTION:     ATAPI_IF_u32Inquiry
* PARAMETER:    Cache: to store Drive info, CDHandle - access CD drive
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  read version info

* HISTORY:
******************************************************************************/
tU32 ATAPI_IF_u32Inquiry(ATAPI_IF_PROC_INFO *info, int CDHandle)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  ATAPI_IF_CMD_PACKET cmdpacket;
  ATAPI_IF_INQUIRY * prInquiry=&info->rCDDeviceInfo;
  ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_u32Inquiry,prInquiry,0,0,0);

  InitCmdPacket(&cmdpacket, 3);
  cmdpacket.cmd.cmd[0] = GPCMD_INQUIRY;
  //cmdpacket.cmd.cmd[4]=96;
  if(ioctl(CDHandle, CDROM_SEND_PACKET, &cmdpacket.cmd) < 0)
  {
      ATAPI_IF_PRINTF_U4("ATAPI_IF_u32Inquiry: GPCMD_INQUIRY failed ");
      u32Ret = OSAL_E_UNKNOWN;
  }
  else
  {
      ATAPI_IF_DEBUG_HEXDUMP("Content of GPCMD_INQUIRY:", cmdpacket.buff, 96);

      ATAPI_IF_PRINTF_U4("ATAPI_IF_u32Inquiry: GPCMD_INQUIRY succeeded");
      memcpy(prInquiry->au8Vendor8, &cmdpacket.buff[8], 8);
      prInquiry->au8Vendor8[8] = 0;
      memcpy(prInquiry->au8Product16, &cmdpacket.buff[16], 16);
      prInquiry->au8Product16[16] = 0;
      /*Fix it: byte 56 and 57 is reserved as per spec 
       *  INQUIRY command not available in mmc3r10g.pdf
       * but in mmc4r01d.pdf , it is mentioned that 56 and 57th bytes as reserved
       */
      prInquiry->u8SWVersion = cmdpacket.buff[56];
      prInquiry->u8HWVersion = cmdpacket.buff[57];
      prInquiry->u8Valid = TRUE;
      ATAPI_IF_PRINTF_U4("ATAPI_IF_u32Inquiry: vendor=%s,product=%s,SW=%x,HW=%x",
            prInquiry->au8Vendor8,prInquiry->au8Product16,
            prInquiry->u8SWVersion,prInquiry->u8HWVersion);
  }

  ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_u32Inquiry, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}

/*****************************************************************************
* FUNCTION:     ATAPI_IF_s16CompIdentifier
* PARAMETER:    pu8IdentifierField (->I) = Volume Descriptor->standard_id_field
*               pu8Identifier      (->I) = Identification string
*               u8IdLength          ( I ) = Identifier length
* RETURNVALUE  : 0 if pu8IdentifierField == pu8Identifier
*               differnce between first different char otherwise
* DESCRIPTION:  Check if input string are equal
******************************************************************************/

static tS16 ATAPI_IF_s16CompIdentifier(const tU8 *pu8IdentifierField,
                              const tU8 *pu8Identifier,
                              tU8 u8IdLength )
{
  tS16 i;
  for( i = 0;(i < u8IdLength)&&
     (ATAPI_C8_UPPERCASE(pu8IdentifierField[i])
      == ATAPI_C8_UPPERCASE(pu8Identifier[i]));
     i++)
  {
    ;
  }

  /* lib functions need char, and not signed or unsigned char* */

  if(!(i && (i == (tS16)strlen((char *)pu8Identifier)) &&
       ((i == u8IdLength) ||
        (pu8IdentifierField[i] == ATAPI_C_C8_SEPARATOR2))))
  {
    if( i >= u8IdLength )   /* identifier field not necessarily NULL terminated! */
    {
      /* pretend that identifier field was NULL terminated */
      return( 0 - ATAPI_C8_UPPERCASE(pu8Identifier[i]) );
    }

    return( ATAPI_C8_UPPERCASE(pu8IdentifierField[i]) -
            ATAPI_C8_UPPERCASE(pu8Identifier[i]) );
  }
  return 0;
}

/*****************************************************************************
* FUNCTION:     ATAPI_IF_u32ReadFileSystem
* PARAMETER:    Cache: to store filesystem info, CDHandle - access CD drive
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  read data and identify file system

* HISTORY:
******************************************************************************/
tU32 ATAPI_IF_u32ReadFileSystem(ATAPI_IF_CDCACHE *Cache, int CDHandle)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  ATAPI_IF_CMD_PACKET cmdpacket;
  ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_u32ReadFileSystem,0,0,0,0);

  InitCmdPacket(&cmdpacket, -1);
  cmdpacket.cmd.cmd[0] = GPCMD_READ_10;
  cmdpacket.cmd.cmd[1] = 0x08; //DPO =0, FUA (force unit access) = 1; RELADR = 0
  cmdpacket.cmd.cmd[2] = 0; //MSB of LBA
  cmdpacket.cmd.cmd[3] = 0;
  cmdpacket.cmd.cmd[4] = 0;
  cmdpacket.cmd.cmd[5] = (tU8)ATAPI_FS_SECTOR_LBA; //LSB of LBA
  cmdpacket.cmd.cmd[7] = 0; //MSB of Size
  cmdpacket.cmd.cmd[8] = 1; //LSB of Size
  if(ioctl(CDHandle, CDROM_SEND_PACKET, &cmdpacket.cmd) < 0)
  {
      ATAPI_IF_PRINTF_U4("ATAPI_IF_u32ReadFileSystem: GPCMD_READ_10 failed ");
      u32Ret = OSAL_E_UNKNOWN;
  }
  else
  {
	  //Fix it : last argument value
      ATAPI_IF_DEBUG_HEXDUMP("Content of GPCMD_READ_10:", cmdpacket.buff, 96);

      ATAPI_IF_PRINTF_U4("ATAPI_IF_u32ReadFileSystem: GPCMD_READ_10 succeeded");
      if( ATAPI_IF_s16CompIdentifier( &cmdpacket.buff[1],  /*identifier*/
                             (tU8 *)ATAPI_C_SZ_ISO_STD_IDENTIFIER,
                             ATAPI_C_U8_ISO_STD_ID_LENGTH) == 0 )
      {
      	Cache->FileSystemType = OSAL_C_U8_FS_TYPE_CDFS;
      }
      else
      {
        if( ATAPI_IF_s16CompIdentifier( &cmdpacket.buff[1],  /*identifier*/
                               (tU8 *)ATAPI_C_SZ_UDFS_STD_IDENTIFIER,
                               ATAPI_C_U8_UDFS_STD_ID_LENGTH) == 0 )
        {
      	  Cache->FileSystemType = OSAL_C_U8_FS_TYPE_UDFS;
        }
      }

      ATAPI_IF_PRINTF_U4("ATAPI_IF_u32ReadFileSystem: FileSystemType=%x",
      		Cache->FileSystemType);
  }

  ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_u32ReadFileSystem, u32Ret, 0, 0, 0, 0);
  return u32Ret;
}
/*****************************************************************************
* FUNCTION:     ATAPI_IF_u32CacheGetVersion
* PARAMETER:    Info - To access cache data,
* 					pu8SW - To update SW version
* 					pu8HW - To update HW version
* 					pau8Vendor8 - To update vendor info
* 					pau8Product16 - To update product info
* RETURNVALUE:  OSAL Error code
* DESCRIPTION:  Update argument with drive version

* HISTORY:
******************************************************************************/

tU32 ATAPI_IF_u32CacheGetVersion(ATAPI_IF_PROC_INFO * Info,tU8 *pu8SW, 
                  tU8 *pu8HW, tU8 *pau8Vendor8,tU8 *pau8Product16)
{
	  tU32 u32Ret = OSAL_E_NOERROR;


	  ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_u32CacheGetVersion, 0, 
                                       pu8SW, pu8HW, pau8Vendor8);

	  if((NULL != pu8SW) && (NULL != pu8HW) && 
            (NULL != pau8Vendor8) && (NULL != pau8Product16))
	  {

	    if(TRUE ==Info->rCDDeviceInfo.u8Valid)
	    {
	      *pu8SW = Info->rCDDeviceInfo.u8SWVersion;
	      *pu8HW = Info->rCDDeviceInfo.u8HWVersion;
	      memcpy(pau8Vendor8, Info->rCDDeviceInfo.au8Vendor8, 
                              sizeof(Info->rCDDeviceInfo.au8Vendor8));
	      memcpy(pau8Product16, Info->rCDDeviceInfo.au8Product16, 
                              sizeof(Info->rCDDeviceInfo.au8Product16));
	    } //if(CD_VALID == Info->rCD.rInquiry.u8Valid)
	    else
	    {
	    	u32Ret = OSAL_E_INVALIDVALUE;
	    }
	  }
	  else //if NULL !=
	  {
	    u32Ret = OSAL_E_INVALIDVALUE;
	  } //else //if NULL !=

	  ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_u32CacheGetVersion, u32Ret, 0, 0, 0, 0);

	  return u32Ret;

}


/******************************************************************************
 *FUNCTION      :ATAPI_IF_UpdateCDText
 *
 *DESCRIPTION   :Updates cached CDText.
 *               Probably accesses shared memory (Cache) and should be protected
 *               by a semaphore outside this function.
 *
 *PARAMETER     :Cache      Pointer to cached CD content
 *               CDHandle   Linux handle for CD device
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static void ATAPI_IF_UpdateCDText(ATAPI_IF_CDCACHE *Cache, int CDHandle)
{
    ATAPI_IF_CMD_PACKET cmdpacket;
    tU32                i;
    tU8                 buff[OSAL_C_S32_CDAUDIO_MAXNAMESIZE];

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_UpdateCDText, 0, 0, 0, 0);

    InitCmdPacket(&cmdpacket, 7);
    cmdpacket.cmd.cmd[0] = GPCMD_READ_TOC_PMA_ATIP;
    cmdpacket.cmd.cmd[2] = 5;  // CD-Text
    cmdpacket.cmd.cmd[6] = 0;  // Session # - to be compatible to MASCA driver, read session 0 only!
    if(ioctl(CDHandle, CDROM_SEND_PACKET, &cmdpacket.cmd) < 0)
    {
        ATAPI_IF_PRINTF_U4("ATAPI_IF_UpdateCDText: GPCMD_READ_TOC_PMA_ATIP for CD Text failed - possibly tried too soon");
    }
    else
    {
        tU32    datalen = ((tU32)cmdpacket.buff[0] << 8) | (tU32)cmdpacket.buff[1];

        ATAPI_IF_PRINTF_U4("ATAPI_IF_UpdateCDText: GPCMD_READ_TOC_PMA_ATIP for CD Text succeeded, datalen is %i", datalen);

        if(datalen > 0)
        { // Succeeded & length > 0 => CD-Text available
            Cache->CDTextFlag = TRUE;
            Cache->CDTextComplete = TRUE;

            ATAPI_IF_GetCDText(buff, cmdpacket.buff, datalen, 0x80, 0);
            if(strlen(Cache->CDTextAlbumName) < strlen(buff))
            { // Either read for the first time or it was incomplete last time
              // Anyways - replace old text and try again later because there might still be more to come
                memcpy(Cache->CDTextAlbumName, buff, OSAL_C_S32_CDAUDIO_MAXNAMESIZE);
                Cache->CDTextComplete = FALSE;
            }
            if(Cache->CDTextAlbumName[0] == 0)
            { // No Title => Either there is none or read failed => try again
                Cache->CDTextComplete = FALSE;
            }

            for(i = Cache->MinTrck; i <= Cache->MaxTrck; i++)
            {
                ATAPI_IF_GetCDText(buff, cmdpacket.buff, datalen, 0x80, i);
                if(strlen(Cache->CDTextTrackTitle[i - 1]) < strlen(buff))
                { // Either read for the first time or it was incomplete last time
                  // Anyways - replace old text and try again later because there might still be more to come
                    memcpy(Cache->CDTextTrackTitle[i - 1], buff, OSAL_C_S32_CDAUDIO_MAXNAMESIZE);
                    Cache->CDTextComplete = FALSE;
                }
                if(Cache->CDTextTrackTitle[i - 1][0] == 0)
                { // No Title => Either there is none or read failed => try again
                    Cache->CDTextComplete = FALSE;
                }

                ATAPI_IF_GetCDText(buff, cmdpacket.buff, datalen, 0x81, i);
                if(strlen(Cache->CDTextTrackPerformer[i - 1]) < strlen(buff))
                { // Either read for the first time or it was incomplete last time
                  // Anyways - replace old text and try again later because there might still be more to come
                    memcpy(Cache->CDTextTrackPerformer[i - 1], buff, OSAL_C_S32_CDAUDIO_MAXNAMESIZE);
                    Cache->CDTextComplete = FALSE;
                }
                if(Cache->CDTextTrackPerformer[i - 1][0] == 0)
                { // No Artist => Either there is none or read failed => try again
                    Cache->CDTextComplete = FALSE;
                }
            }
        }
    }

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_UpdateCDText, OSAL_E_NOERROR, 0, 0, 0, 0);
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_CacheMedia
 *
 *DESCRIPTION   :Caches TOC and CDText of a CD.
 *               Probably accesses shared memory (CDCache in Info) and should
 *               be protected by a semaphore outside this function.
 *
 *PARAMETER     :Info   Pointer to info structure used by ATAPI_IF_UpdateThread
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static void ATAPI_IF_CacheMedia(ATAPI_IF_THRD_INFO *Info)
{
    ATAPI_IF_CDCACHE    *cache;
    ATAPI_IF_CMD_PACKET cmdpacket;
    tS32                cdtype;
    tU32                i;
    struct cdrom_mcn    mcn;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_CacheMedia, 0, 0, 0, 0);

    cache = &(Info->ProcInfo->CDCache);
    memset(cache, 0, sizeof(ATAPI_IF_CDCACHE));

    InitCmdPacket(&cmdpacket, 7);
    cmdpacket.cmd.cmd[0] = GPCMD_READ_TOC_PMA_ATIP;
    cmdpacket.cmd.cmd[2] = 2;  // TOC
    cmdpacket.cmd.cmd[6] = 0;  // Session #
    if(ioctl(Info->CDHandle, CDROM_SEND_PACKET, &cmdpacket.cmd) < 0)
    {
        ATAPI_IF_PRINTF_U4("ATAPI_IF_CacheMedia: GPCMD_READ_TOC_PMA_ATIP failed for Full TOC");

        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_CacheMedia, -1, 0, 0, 0, 0);

        return;
    }
    else
    {
        tU32    datalen = ((tU32)cmdpacket.buff[0] << 8)|(tU32)cmdpacket.buff[1];
        tU32    off = 4;
        tU32    mintrck = 100, maxtrck = 0;
        tU32    audioflag = FALSE;
        tU32    dataflag = FALSE;

        ATAPI_IF_PRINTF_U4("ATAPI_IF_CacheMedia: GPCMD_READ_TOC_PMA_ATIP succeeded for Full TOC");

        ATAPI_IF_DEBUG_HEXDUMP("Content of GPCMD_READ_TOC_PMA_ATIP:", cmdpacket.buff, datalen);

        for(i = 4; i < datalen; i += 11)
        {
            tU32    trck = (tU32)cmdpacket.buff[i + 3];

            if(cmdpacket.buff[i] == 1)  // Read first session only. Remove this to scan all sessions.
            {
                if(trck == 0xa0 && cmdpacket.buff[i + 8] < mintrck)
                {
                    mintrck = (tU32)cmdpacket.buff[i + 8];   // Minimum Track # of this session, might be lesser then in previous sessions
                }
                else if(trck == 0xa1 && cmdpacket.buff[i + 8] > maxtrck)
                {
                    maxtrck = (tU32)cmdpacket.buff[i + 8];   // Maximum Track # of this session, might be bigger then in previous sessions
                }
            }
        }
        if(mintrck >= 100 ||  maxtrck <= 0)
        {
            ATAPI_IF_PRINTF_U4("ATAPI_IF_CacheMedia: Could not find first/last track on CD");

            ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_CacheMedia, -2, 0, 0, 0, 0);
            return;
        }

        cache->MinTrck  = mintrck;
        cache->MaxTrck  = maxtrck;

        for(i = 4; i < datalen; i += 11)
        {
            tU32    trck = (tU32)cmdpacket.buff[i + 3];

            ATAPI_IF_DEBUG_HEXDUMP("Packet in GPCMD_READ_TOC_PMA_ATIP:", cmdpacket.buff + i, 11);
            if(trck >= cache->MinTrck && trck <= cache->MaxTrck)
            {
                cache->TrckStartLBA[trck - 1] = ATAPI_IF_MSF2LBA((tU32)cmdpacket.buff[i + 8], (tU32)cmdpacket.buff[i + 9], (tU32)cmdpacket.buff[i + 10]);
#ifdef ATAPI_IF_CROP_FRAMES_IN_CACHE
                // The real MASCA drive can only read seconds, not frames. To be compatible with it, start times are rounded down to the next full second.
                cache->TrckStartLBA[trck - 1] = (cache->TrckStartLBA[trck - 1] / 75) * 75;
#endif //ATAPI_IF_CROP_FRAMES_IN_CACHE
                if(cmdpacket.buff[i + 1] & 0x04)
                {
                    ATAPI_IF_ADDITIONALCDINFO_SET_DATA_TRACK(cache->DataTrackInfo, trck);
                    dataflag = TRUE;
                }
                else
                {
                    audioflag = TRUE;
                }
            }
            else if(trck == 0xa2 || trck == cache->MaxTrck + 1)
            {
                tU32    lbanew = ATAPI_IF_MSF2LBA((tU32)cmdpacket.buff[i + 8], (tU32)cmdpacket.buff[i + 9], (tU32)cmdpacket.buff[i + 10]);
#ifdef ATAPI_IF_CROP_FRAMES_IN_CACHE
                // Add one, so the last second can be played completely
                lbanew = (lbanew / 75) * 75 + 1;
#endif //ATAPI_IF_CROP_FRAMES_IN_CACHE

                if(cache->TrckStartLBA[ATAPI_IF_LEADOUT_INDEX] == 0 || lbanew < cache->TrckStartLBA[ATAPI_IF_LEADOUT_INDEX])
                {
                    cache->TrckStartLBA[ATAPI_IF_LEADOUT_INDEX] = lbanew;
                    cache->TrckStartLBA[cache->MaxTrck] = lbanew;
                }
            }
        }

#ifdef ATAPI_IF_CROP_FRAMES_IN_CACHE
        // Subtract the one frame that was added above
        cache->PlayTime = cache->TrckStartLBA[ATAPI_IF_LEADOUT_INDEX] - cache->TrckStartLBA[cache->MinTrck - 1] - 1;
#else //ATAPI_IF_CROP_FRAMES_IN_CACHE
        cache->PlayTime = cache->TrckStartLBA[ATAPI_IF_LEADOUT_INDEX] - cache->TrckStartLBA[cache->MinTrck - 1];
#endif //ATAPI_IF_CROP_FRAMES_IN_CACHE

        if(audioflag)
        {
            Info->ProcInfo->CDCache.MediaType = OSAL_C_U8_AUDIO_MEDIA;
            Info->ProcInfo->CDCache.FileSystemType = OSAL_C_U8_FS_TYPE_UNKNOWN;
        }
        else if(dataflag)
        {
            Info->ProcInfo->CDCache.MediaType = OSAL_C_U8_DATA_MEDIA;
            Info->ProcInfo->CDCache.FileSystemType = OSAL_C_U8_FS_TYPE_CDFS;
        }
        else
        {
            Info->ProcInfo->CDCache.MediaType = OSAL_C_U8_UNKNOWN_MEDIA;
            Info->ProcInfo->CDCache.FileSystemType = OSAL_C_U8_FS_TYPE_UNKNOWN;
        }
    }

    if(ioctl(Info->CDHandle, CDROM_GET_MCN, &mcn) >= 0)
    {
        memcpy(Info->ProcInfo->CDCache.MediaCatNum, mcn.medium_catalog_number, sizeof(mcn.medium_catalog_number));
    }

    // CDText is a bit tricky, since PC CDROMs seem to be quite slow and unreliable, but his way seems to work well:
    // Read the CDText, since it seems unavailable or causes a change, it will be read at least one more time.
    // As long as the read data changes, the CDTest will be read again up to 4 times.
    // With the tested CDROMs this was able to read the correct CDText each time.
    // If there are CDROMs that still don't work, we can either add some sleep to slow down the process or add a few tries more.
    ATAPI_IF_UpdateCDText(cache, Info->CDHandle);
    for(i = 0; i < 4 && (!cache->CDTextFlag || !cache->CDTextComplete); i++)
        ATAPI_IF_UpdateCDText(cache, Info->CDHandle);
    ATAPI_IF_u32ReadFileSystem(cache, Info->CDHandle);


    cache->ValidFlag = TRUE;

    ATAPI_IF_PRINTF_U4("ATAPI_IF_CacheMedia: done");

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_CacheMedia, OSAL_E_NOERROR, 0, 0, 0, 0);
}


static snd_output_t *snd_output = NULL;
/******************************************************************************
 *FUNCTION      :ATAPI_IF_InitALSA
 *
 *DESCRIPTION   :Initializes ALSA to be used for audio playback because most
 *               CDROM drives in modern PCs aren't capable of native audio
 *               playback.
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :Handle for ALSA
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
#define ATAPI_IF_PRINTF_U4_ ATAPI_IF_PRINTF_U4
#define ATAPI_IF_PRINTF_U4__ ATAPI_IF_PRINTF_U4

    snd_pcm_hw_params_t *hw_params;
    snd_pcm_sw_params_t *sw_params;
    unsigned int        smplrate = 44100;
    snd_pcm_uframes_t   buffsize = 16384;
    snd_pcm_uframes_t   period_size = AI_DEFAULT_PERIOD_SIZE;
    snd_pcm_t           *alsahandle;
int dir;
//#if 0
static snd_pcm_t *ATAPI_IF_InitALSA(void)
{
	int                 retval;
    snd_output_stdio_attach(&snd_output, stdout, 0);
    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_InitALSA, 0, 0, 0, 0);

    ATAPI_IF_PRINTF_U4_("snd_pcm_open\n");
    retval = snd_pcm_open(&alsahandle, "CDDAOut", SND_PCM_STREAM_PLAYBACK, 0);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_open");
        ATAPI_IF_PRINTF_ERRORS("cannot open audio device %s (%s)", "CDDAOut", snd_strerror(retval));
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_malloc\n");
    retval = snd_pcm_hw_params_malloc(&hw_params);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_hw_params_malloc");
        ATAPI_IF_PRINTF_ERRORS("cannot allocate hardware parameter structure (%s)", snd_strerror(retval));
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_any\n");
    retval = snd_pcm_hw_params_any(alsahandle, hw_params);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_hw_params_any");
        ATAPI_IF_PRINTF_ERRORS("cannot initialize hardware parameter structure (%s)", snd_strerror(retval));
        snd_pcm_hw_params_free(hw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }

    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_access\n");
    retval = snd_pcm_hw_params_set_access(alsahandle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_hw_params_set_access");
        ATAPI_IF_PRINTF_ERRORS("cannot set access type (%s)", snd_strerror(retval));
        snd_pcm_hw_params_free(hw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }

    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_format\n");
    retval = snd_pcm_hw_params_set_format(alsahandle, hw_params, SND_PCM_FORMAT_S16_LE);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_hw_params_set_format");
        ATAPI_IF_PRINTF_ERRORS("cannot set sample format (%s)", snd_strerror(retval));
        snd_pcm_hw_params_free(hw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }

    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_rate_near request %d \n", smplrate);
    retval = snd_pcm_hw_params_set_rate_near(alsahandle, hw_params, &smplrate, 0);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_hw_params_set_rate_near");
        ATAPI_IF_PRINTF_ERRORS("cannot set sample rate (%s)", snd_strerror(retval));
        snd_pcm_hw_params_free(hw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_rate_near actual %d \n", smplrate);
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_channels\n");
    retval = snd_pcm_hw_params_set_channels(alsahandle, hw_params, 2);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_hw_params_set_channels");
        ATAPI_IF_PRINTF_ERRORS("cannot set channel count (%s)", snd_strerror(retval));
        snd_pcm_hw_params_free(hw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    snd_pcm_uframes_t buffer_size_min,buffer_size_max;
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_buffer_size_near default= %d \n", 
                                                (int)buffsize);
    retval = snd_pcm_hw_params_get_buffer_size_min(hw_params, &buffer_size_min);
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_buffer_size_near min= %d (errcode=%d)\n",
                                                (int)buffer_size_min,retval);
    retval = snd_pcm_hw_params_get_buffer_size_max(hw_params, &buffer_size_max);
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_buffer_size_near max= %d (errcode=%d)\n", 
                                                (int)buffer_size_max,retval);
    retval = snd_pcm_hw_params_set_buffer_size_near(alsahandle, hw_params, &buffsize);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_hw_params_set_buffer_size_near");
        ATAPI_IF_PRINTF_ERRORS("cannot set buffer size (%s)", snd_strerror(retval));
        snd_pcm_hw_params_free(hw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    snd_pcm_uframes_t period_size_min,period_size_max;
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_period_size_near default=%d \n", 
                                                (int)period_size);
    retval = snd_pcm_hw_params_get_period_size_min(hw_params, &period_size_min, NULL);
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_period_size_near min=%d(errcode=%d) \n", 
                                                (int)period_size_min,retval);
    retval = snd_pcm_hw_params_get_period_size_max(hw_params, &period_size_max, NULL);
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_set_period_size_near max=%d (errcode=%d) \n", 
                                                (int)period_size_max,retval);

    retval = snd_pcm_hw_params_set_period_size_near(alsahandle, 
                                                   hw_params, &period_size, 0);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_hw_params_set_period_size_near");
        ATAPI_IF_PRINTF_ERRORS("cannot set period size (%s)", snd_strerror(retval));
        snd_pcm_hw_params_free(hw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    else
    {
    	snd_pcm_hw_params_dump(hw_params, snd_output);
        ATAPI_IF_PRINTF_U4("buffer size is--- %i", (int)buffsize);
        ATAPI_IF_PRINTF_U4("period size is--- %i", (int)period_size);
    }
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params\n");
    retval = snd_pcm_hw_params(alsahandle, hw_params);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_hw_params-");
        ATAPI_IF_PRINTF_ERRORS("cannot set parameters (%s)", snd_strerror(retval));
        snd_pcm_hw_params_free(hw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    ATAPI_IF_PRINTF_U4_("snd_pcm_hw_params_free\n");
    snd_pcm_hw_params_free(hw_params);
    ATAPI_IF_PRINTF_U4_("snd_pcm_sw_params_malloc\n");
    retval = snd_pcm_sw_params_malloc(&sw_params);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_sw_params_malloc");
        ATAPI_IF_PRINTF_ERRORS("cannot allocate software parameters structure (%s)", snd_strerror(retval));
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    ATAPI_IF_PRINTF_U4_("snd_pcm_sw_params_current\n");
    retval = snd_pcm_sw_params_current(alsahandle, sw_params);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_sw_params_current");
        ATAPI_IF_PRINTF_ERRORS("cannot initialize software parameters structure (%s)", snd_strerror(retval));
        snd_pcm_sw_params_free(sw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }

    ATAPI_IF_PRINTF_U4_("snd_pcm_sw_params_set_avail_min\n");
    retval = snd_pcm_sw_params_set_avail_min(alsahandle, sw_params, buffsize / 2);    // Expects size in WORDS
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_sw_params_set_avail_min");
        ATAPI_IF_PRINTF_ERRORS("cannot set minimum available count (%s)", snd_strerror(retval));
        snd_pcm_sw_params_free(sw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    ATAPI_IF_PRINTF_U4_("snd_pcm_sw_params_set_start_threshold\n");
    retval = snd_pcm_sw_params_set_start_threshold(alsahandle, sw_params, ATAPI_IF_SAMPLE_BUFF_SIZE / 2 - 2);  // Expects size in WORDS
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_sw_params_set_start_threshold");
        ATAPI_IF_PRINTF_ERRORS("cannot set start mode (%s)", snd_strerror(retval));
        snd_pcm_sw_params_free(sw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    ATAPI_IF_PRINTF_U4_("snd_pcm_sw_params\n");
    retval = snd_pcm_sw_params(alsahandle, sw_params);
    if(retval < 0)
    {
    	ATAPI_IF_PRINTF_U4__("snd_pcm_sw_params");
        ATAPI_IF_PRINTF_ERRORS("cannot set software parameters (%s)", snd_strerror(retval));
        snd_pcm_sw_params_free(sw_params);
        snd_pcm_close(alsahandle);
        ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, -1, 0, 0, 0, 0);
        return NULL;
    }
    snd_pcm_sw_params_dump(sw_params, snd_output);
    ATAPI_IF_PRINTF_U4_("snd_pcm_sw_params_free\n");
    snd_pcm_sw_params_free(sw_params);

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_InitALSA, 0, 0, 0, 0, 0);
    return alsahandle;
}

//#endif
/******************************************************************************
 *FUNCTION      :ATAPI_IF_StreamData
 *
 *DESCRIPTION   :Reads audio data from CD and copies it into a buffer for
 *               playback through ALSA.
 *               Only called from ATAPI_IF_UpdateThread and does _not_
 *               access the shared memory portions of this structure, so there
 *               is no need to protect it with a semaphore.
 *
 *PARAMETER     :Info   Pointer to info structure used by ATAPI_IF_UpdateThread
 *
 *RETURNVALUE   :Number of frames processed or OSAL_ERROR in case of an error
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static tS32 ATAPI_IF_StreamData(ATAPI_IF_THRD_INFO *Info)
{
    int                 err;
    short               buff[ATAPI_IF_SAMPLE_BUFF_SIZE / 2];      // Size in WORDS is size in BYTES / 2
    tU32                readstartmin, readstartsec, readstartfrm;
    tU32                readendmin, readendsec, readendfrm;
    tU32                readcdfrmnum;
    ATAPI_IF_CMD_PACKET cmdpacket;
    ATAPI_IF_PRINTF_ERRORS("ATAPI_IF_StreamData start LBA=(%i) end LBA=(%i)", Info->ThrdPlayStatus.StartLBA,Info->ThrdPlayStatus.EndLBA);

    if(Info->ThrdPlayStatus.CurrentLBA >= Info->ThrdPlayStatus.EndLBA)
        return 0;

    if(Info->ThrdPlayStatus.CurrentLBA < Info->ThrdPlayStatus.StartLBA)
        return 0;

    readcdfrmnum = Info->ThrdPlayStatus.EndLBA - Info->ThrdPlayStatus.CurrentLBA;
    if(readcdfrmnum > ATAPI_IF_SAMPLE_BUFF_CDFRAMES)
    {
        readcdfrmnum = ATAPI_IF_SAMPLE_BUFF_CDFRAMES;
    }
    ATAPI_IF_PRINTF_ERRORS("ATAPI_IF_StreamData start LBA=(%i) end LBA=(%i),read frame=%i",
    		Info->ThrdPlayStatus.CurrentLBA,Info->ThrdPlayStatus.EndLBA,readcdfrmnum);

    ATAPI_IF_LBA2MSF(&readstartmin, &readstartsec, &readstartfrm, Info->ThrdPlayStatus.CurrentLBA);
    ATAPI_IF_LBA2MSF(&readendmin, &readendsec, &readendfrm, Info->ThrdPlayStatus.CurrentLBA + readcdfrmnum);

    InitCmdPacket(&cmdpacket, -1);
    cmdpacket.cmd.buffer = (unsigned char *)buff;
    cmdpacket.cmd.buflen = ATAPI_IF_SAMPLE_BUFF_SIZE;   // Expects size in BYTES

#ifdef READ_DATA_IN_MSF_FORMAT
    cmdpacket.cmd.cmd[0] = GPCMD_READ_CD_MSF;
    cmdpacket.cmd.cmd[1] = 2; // Any Sector Type, DAP
    cmdpacket.cmd.cmd[3] = readstartmin;
    cmdpacket.cmd.cmd[4] = readstartsec;
    cmdpacket.cmd.cmd[5] = readstartfrm;
    cmdpacket.cmd.cmd[6] = readendmin;
    cmdpacket.cmd.cmd[7] = readendsec;
    cmdpacket.cmd.cmd[8] = readendfrm;
    cmdpacket.cmd.cmd[9] = 0x10; // User Data only
    cmdpacket.cmd.cmd[10] = 0;
    cmdpacket.cmd.cmd[11] = 0;

#else

    cmdpacket.cmd.cmd[0] = GPCMD_READ_CD;
    cmdpacket.cmd.cmd[1] = 2; // Any Sector Type, DAP
    cmdpacket.cmd.cmd[2] = ((Info->ThrdPlayStatus.CurrentLBA)>>24) & 0xff;
    cmdpacket.cmd.cmd[3] = ((Info->ThrdPlayStatus.CurrentLBA)>>16) & 0xff;
    cmdpacket.cmd.cmd[4] = ((Info->ThrdPlayStatus.CurrentLBA)>>8) & 0xff;
    cmdpacket.cmd.cmd[5] = (Info->ThrdPlayStatus.CurrentLBA) & 0xff;
    cmdpacket.cmd.cmd[6] = ((readcdfrmnum)>>16) & 0xff;
    cmdpacket.cmd.cmd[7] = ((readcdfrmnum)>>8) & 0xff;
    cmdpacket.cmd.cmd[8] = ((readcdfrmnum)) & 0xff;
    cmdpacket.cmd.cmd[9] = 0x10; // User Data only
    cmdpacket.cmd.cmd[10] = 0;
    cmdpacket.cmd.cmd[11] = 0;

#endif
    if(ioctl(Info->CDHandle, CDROM_SEND_PACKET, &cmdpacket.cmd) < 0)
    {
        ATAPI_IF_PRINTF_ERRORS("GPCMD_READ_CD_MSF failed (%i)(%s)", errno,snd_strerror(err));
        return OSAL_ERROR;
    }
    if(readcdfrmnum < ATAPI_IF_SAMPLE_BUFF_CDFRAMES)
    {
    	ATAPI_IF_PRINTF_ERRORS("readcdfrmnum(=%i) < 5 ", readcdfrmnum);
        tU32    off = readcdfrmnum * ATAPI_IF_CDFRAME_BYTES;
        tU32    len = (ATAPI_IF_SAMPLE_BUFF_CDFRAMES - readcdfrmnum) * ATAPI_IF_CDFRAME_BYTES;
        memset(((tU8 *)buff) + off, 0, len);
    }
	
    if((err = snd_pcm_writei(Info->ALSAHandle, buff, ATAPI_IF_SAMPLE_BUFF_SIZE / 4)) < 0)   // Expects size in FRAMES, e.g. 2 WORDS (left/right) each
    { // This write fails quite often, especially when the system is busy - it is not much of a problem, so let's try again
        ATAPI_IF_DEBUG_PRINTF("write failed (%s)\n", snd_strerror(err));
        if((err = snd_pcm_prepare(Info->ALSAHandle)) < 0)
        {
            ATAPI_IF_PRINTF_ERRORS("cannot prepare audio interface for use (%s)", snd_strerror(err));
            return OSAL_ERROR;
        }
        if((err = snd_pcm_writei(Info->ALSAHandle, buff, ATAPI_IF_SAMPLE_BUFF_SIZE / 4)) < 0)
        {
            ATAPI_IF_PRINTF_ERRORS("write failed twice (%s)", snd_strerror(err));
            return OSAL_ERROR;
        }
        else
        {
            ATAPI_IF_PRINTF_ERRORS("\t2nd try to write succeeded for %i bytes", ATAPI_IF_SAMPLE_BUFF_SIZE);
        }
    } else {
    	ATAPI_IF_PRINTF_ERRORS("snd_pcm_writei - (%d)", err);
    }

    return readcdfrmnum;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_ChangeStatus
 *
 *DESCRIPTION   :Changes CurrentStatus to Request if possible.
 *               Probably accesses shared memory (Cache) and should be protected
 *               by a semaphore outside this function.
 *               Some requests cause a different status to be set, f.e.
 *               ATAPI_IF_STRM_REQUEST_RESUME might cause CurrentStatus to
 *               change to ATAPI_IF_STRM_STATUS_PLAY. This function is used to
 *               really change a status as well as to check if a change is
 *               possible, to verify requests. Therefore the flag
 *               KeepRequestStatus can be used to copy the original status from
 *               Request to CurrentStatus, e.g. f.e. ATAPI_IF_STRM_REQUEST_RESUME
 *               will not be changed to ATAPI_IF_STRM_STATUS_PLAY.
 *
 *PARAMETER     :Cache              Pointer to cached CD content
 *               CurrentStatus      Current status of playback, will be changed
 *                                  to new status, if the change is possible
 *               Request            Status to change to
 *               KeepRequestStatus  Flag, see description
 *
 *RETURNVALUE   :TRUE if request was possible,
 *               FALSE if request failed
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
static tU32 ATAPI_IF_ChangeStatus(ATAPI_IF_CDCACHE *Cache, ATAPI_IF_PLAY_STATUS *CurrentStatus, ATAPI_IF_PLAY_STATUS *Request, tU32 KeepRequestStatus)
{
    tU32    result = FALSE;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_ChangeStatus, 0, 0, 0, 0);
    ATAPI_IF_PRINTF_U4("Current Status is: %s", GetStatusName(CurrentStatus->Status));
    ATAPI_IF_PRINTF_U4("Requested Status is: %s", GetStatusName(Request->Status));

    if(Request->Status == ATAPI_IF_STRM_STATUS_SHUTDOWN)
    {
        // Current Status doesn't matter. Shutdown is always possible.
        memcpy(&CurrentStatus, &Request, sizeof(ATAPI_IF_PLAY_STATUS));
        result = TRUE;
    }
    else if(Request->Status == ATAPI_IF_STRM_STATUS_NO_DISK)
    {
        // Current Status doesn't matter. This is a just a fact, reported by the stream thread.
        // CHECKME: Clear Playrange when CD is removed?
        memcpy(&CurrentStatus, &Request, sizeof(ATAPI_IF_PLAY_STATUS));
        result = TRUE;
    }
    else if(CurrentStatus->Status == ATAPI_IF_STRM_STATUS_NO_DISK)
    {
        if(Request->Status == ATAPI_IF_STRM_STATUS_STOP)
        {
            // CHECKME: Does the MASCA driver clear the Playrange when a new CD is inserted?
            memcpy(&CurrentStatus, &Request, sizeof(ATAPI_IF_PLAY_STATUS));
            result = TRUE;
        }
    }
    else if(Request->Status == ATAPI_IF_STRM_REQUEST_PLAY_RANGE)
    {
        // As long as there is a CD, it is always possible to set a new range
        // CHECKME: Is this possible with the MASCA driver when no CD is present?
        if(Cache->ValidFlag)
        {
            tU32    startlba = ATAPI_IF_TranslateSpecialLBA(Cache, CurrentStatus, Request->NewStartLBA);
            tU32    endlba = ATAPI_IF_TranslateSpecialLBA(Cache, CurrentStatus, Request->NewEndLBA);

            if(startlba == 0 || startlba < Cache->TrckStartLBA[0])
            {
                result = FALSE;
            }
            else if(endlba == 0 || endlba >= Cache->TrckStartLBA[ATAPI_IF_LEADOUT_INDEX])
            {
                result = FALSE;
            }
            else if(endlba < startlba)
            {
                result = FALSE;
            }
            else
            {
                if(KeepRequestStatus)
                {
                    CurrentStatus->NewStartLBA = Request->NewStartLBA;
                    CurrentStatus->NewEndLBA = Request->NewEndLBA;
                    CurrentStatus->Status = Request->Status;
                }
                else
                {
                    CurrentStatus->NewStartLBA = startlba;
                    CurrentStatus->NewEndLBA = endlba;
                }
                result = TRUE;
            }
        }
    }
    else if(Request->Status == ATAPI_IF_STRM_STATUS_PLAY ||
            Request->Status == ATAPI_IF_STRM_STATUS_FAST_FORWARD)
    {
        CurrentStatus->StartLBA = CurrentStatus->NewStartLBA;
        CurrentStatus->EndLBA = CurrentStatus->NewEndLBA;
        CurrentStatus->CurrentLBA = CurrentStatus->StartLBA;
        CurrentStatus->Status = Request->Status;
        result = TRUE;
    }
    else if(Request->Status == ATAPI_IF_STRM_STATUS_FAST_BACKWARD)
    {
        CurrentStatus->StartLBA = CurrentStatus->NewStartLBA;
        CurrentStatus->EndLBA = CurrentStatus->NewEndLBA;
        CurrentStatus->CurrentLBA = CurrentStatus->EndLBA;
        CurrentStatus->Status = Request->Status;
        result = TRUE;
    }
    else if(Request->Status == ATAPI_IF_STRM_STATUS_STOP)
    {
        if(CurrentStatus->Status == ATAPI_IF_STRM_STATUS_PLAY ||
           CurrentStatus->Status == ATAPI_IF_STRM_STATUS_FAST_FORWARD ||
           CurrentStatus->Status == ATAPI_IF_STRM_STATUS_FAST_BACKWARD ||
           CurrentStatus->Status == ATAPI_IF_STRM_STATUS_PAUSE ||
           CurrentStatus->Status == ATAPI_IF_STRM_STATUS_STOP)
        {
            CurrentStatus->Status = Request->Status;
            result = TRUE;
        }
    }
    else if(Request->Status == ATAPI_IF_STRM_STATUS_PAUSE)
    {
        if(CurrentStatus->Status == ATAPI_IF_STRM_STATUS_PLAY ||
           CurrentStatus->Status == ATAPI_IF_STRM_STATUS_FAST_FORWARD ||
           CurrentStatus->Status == ATAPI_IF_STRM_STATUS_FAST_BACKWARD)
        {
            CurrentStatus->Status = Request->Status;
            result = TRUE;
        }
    }
    else if(Request->Status == ATAPI_IF_STRM_REQUEST_RESUME)
    {
        if(CurrentStatus->Status == ATAPI_IF_STRM_STATUS_PAUSE)
        {
            if(KeepRequestStatus)
                CurrentStatus->Status = Request->Status;
            else
                CurrentStatus->Status = ATAPI_IF_STRM_STATUS_PLAY;
            result = TRUE;
        }
    }

    ATAPI_IF_PRINTF_U4("New Status is: %s", GetStatusName(CurrentStatus->Status));
    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_ChangeStatus, result ? OSAL_E_NOERROR : OSAL_E_UNKNOWN, 0, 0, 0, 0);
    return result;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_UpdateProcStatus
 *
 *DESCRIPTION   :Copies ATAPI_IF_UpdateThread's real playback status to the
 *               structure seen by the other processes.
 *               ATAPI_IF_UpdateThread accesses the CDROM and the sound
 *               hardware which is very slow but sets the playback status. If
 *               it would use the same memory, other threads use when calling
 *               IOControl, those threads would stall for a long time. But
 *               since they can read from a copy, they can proceed quickly.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *
 *PARAMETER     :Info   Pointer to info structure used by ATAPI_IF_UpdateThread
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
void ATAPI_IF_UpdateProcStatus(ATAPI_IF_PROC_INFO *Info)
{
    if(Info->StatusChangePending != 0)
    {
        // This change syncs the virtual status as seen by processes to the real one in the streaming thread. Therefore there is no check.
        memcpy(&Info->PlayStatus, &Info->StatusChange, sizeof(ATAPI_IF_PLAY_STATUS));
        Info->StatusChangePending = 0;
    }
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_RequestStreamStatusChange
 *
 *DESCRIPTION   :Requests the ATAPI_IF_UpdateThread's playback status to change.
 *
 *PARAMETER     :Info   Pointer to info structure used by ATAPI_IF_UpdateThread
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
tS32 ATAPI_IF_RequestStreamStatusChange(ATAPI_IF_PROC_INFO *Info, OSAL_tShMemHandle SemHandle, ATAPI_IF_STREAM_STATUS Status, tU32 StartLBA, tU32 EndLBA)
{
    tS32                    result = OSAL_E_NOERROR;
    ATAPI_IF_PLAY_STATUS    newstatus;
    ATAPI_IF_PLAY_STATUS    request;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_RequestStreamStatusChange, SemHandle, &SemHandle, StartLBA, EndLBA);

    for(; ; )
    {
        // This access to shared memory is intentionally not protected by a semaphore.
        // It only reads from shared memory and it doesn't matter if the result is exact at this point.
        // It's only purpose is to minimize the number of unnecessary calls to OSAL_s32SemaphoreWait.
        if(Info->ChangeRequestPending == 0)
        {
            if(OSAL_s32SemaphoreWait(SemHandle, OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                result = OSAL_E_UNKNOWN;
                break;
            }

            if(Info->ChangeRequestPending == 0)
            {
                ATAPI_IF_UpdateProcStatus(Info);

                memcpy(&newstatus, &Info->PlayStatus, sizeof(ATAPI_IF_PLAY_STATUS));

                request.NewStartLBA = StartLBA;
                request.NewEndLBA = EndLBA;
                request.Status = Status;
                if(!Info->CDCache.ValidFlag && Info->MediaStatus.InternelStatus != AIF_LDR_STATUS_CD_PLAYABLE )
                {
                	switch(Info->MediaStatus.InternelStatus )
                	{
                	case AIF_LDR_STATUS_NO_DISK:
                	case AIF_LDR_STATUS_EJECTED:
                	case AIF_LDR_STATUS_IN_SLOT:
                		result= OSAL_E_MEDIA_NOT_AVAILABLE;
                		ATAPI_IF_PRINTF_U4("MEDIA NOT AVAILABLE");
                		break;
                	case AIF_LDR_STATUS_UNKNOWN:
                		result=OSAL_E_UNKNOWN;
                		ATAPI_IF_PRINTF_U4("MEDIA STATE Unknown");
                		break;
                	default:
                		result=Info->MediaStatus.InternelStatus;
                		ATAPI_IF_PRINTF_U4("MEDIA STATE code=%x",result);
                		break;
                	}


                }
                else if(!ATAPI_IF_ChangeStatus(&Info->CDCache, &newstatus, &request, TRUE))
                {

                    result = OSAL_E_UNKNOWN;
                }
                else
                {
                    memcpy(&Info->ChangeRequest, &newstatus, sizeof(ATAPI_IF_PLAY_STATUS));

                    Info->ChangeRequestPending = 1;

                    ATAPI_IF_DUMP_PLAYSTATUS("\n**Request: ", &Info->ChangeRequest);
                    ATAPI_IF_DUMP_PLAYSTATUS("**Local: ", &Info->PlayStatus);
                }

                OSAL_s32SemaphorePost(SemHandle);
                break;
            }

            OSAL_s32SemaphorePost(SemHandle);
        }
        OSAL_s32ThreadWait(10);
    }

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_RequestStreamStatusChange, result, 1, 2, 3, 4);
    return result;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_RequestData
 *
 *DESCRIPTION   :Requests the ATAPI_IF_UpdateThread's to read data.
 *               Used by IOControl for OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA and
 *               the like. During playback, ATAPI_IF_UpdateThread is constantly
 *               reading from the CDROM. Other threads must not interfere with
 *               that. Therefore they can only ask ATAPI_IF_UpdateThread to
 *               read the data, when it is not busy with audio playback.
 *
 *PARAMETER     :Buffer     Pointer to buffer to store the data
 *               Info       Pointer to Shared Memory object with process status
 *               SemHandle  Handle of semaphore to protect shared memory access
 *               StartLBA   Logical Block Address to start reading data
 *               NumFrames  Number of CD Frames to read
 *
 *RETURNVALUE   :OSAL_OK if the data was read
 *               OSAL_ERROR if read failed
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
tS32 ATAPI_IF_RequestData(tU8 *Buffer, ATAPI_IF_PROC_INFO *Info, OSAL_tShMemHandle SemHandle, tU32 StartLBA, tU32 NumFrames)
{
    tS32    result = OSAL_OK;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_RequestData, 0, 0, 0, 0);

    // Create a request for data for the streaming thread
    for(; ; )
    {
        // This access to shared memory is intentionally not protected by a semaphore.
        // It only reads from shared memory and it doesn't matter if the result is exact at this point.
        // It's only purpose is to minimize the number of unnecessary calls to OSAL_s32SemaphoreWait.
        if(Info->DataRequestPending == 0)
        {
            if(OSAL_s32SemaphoreWait(SemHandle, OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                result = OSAL_ERROR;
                break;
            }

            if(Info->DataRequestPending == 0)
            {
                Info->DataRequestLBA = StartLBA;
                Info->DataRequestNumFrames = NumFrames;
                Info->DataRequestPending = 1;
                Info->DataRequestComplete = 0;

                OSAL_s32SemaphorePost(SemHandle);
                break;
            }

            OSAL_s32SemaphorePost(SemHandle);
        }
        OSAL_s32ThreadWait(10);
    }

    // Wait for a response from the streaming thread
    for(; ; )
    {
        // This access to shared memory is intentionally not protected by a semaphore.
        // It only reads from shared memory and it doesn't matter if the result is exact at this point.
        // It's only purpose is to minimize the number of unnecessary calls to OSAL_s32SemaphoreWait.
        if(Info->DataRequestComplete != 0)
        {
            if(OSAL_s32SemaphoreWait(SemHandle, OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                result = OSAL_ERROR;
                break;
            }

            if(Info->DataRequestComplete != 0)
            {
                if(Info->DataRequestNumFrames <= 0)
                {
                    result = OSAL_ERROR;
                }
                else
                {
                    memcpy(Buffer, Info->DataRequestBuffer, Info->DataRequestNumFrames * ATAPI_IF_CDFRAME_BYTES);
                    Info->DataRequestPending = 0;
                    Info->DataRequestComplete = 0;
                }

                OSAL_s32SemaphorePost(SemHandle);
                break;
            }

            OSAL_s32SemaphorePost(SemHandle);
        }
        OSAL_s32ThreadWait(10);
    }

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_RequestData, result, 0, 0, 0, 0);
    return result;
}



/******************************************************************************
 *FUNCTION      :ATAPI_IF_ReadData
 *
 *DESCRIPTION   :Called by ATAPI_IF_UpdateThread to read data.
 *               Accesses shared memory and must be protected by a semaphore
 *               outside this function.
 *
 *PARAMETER     :Info   Pointer to info structure used by ATAPI_IF_UpdateThread
 *
 *RETURNVALUE   :Number of CD Frames read or
 *               OSAL_ERROR if read failed
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *****************************************************************************/
tS32 ATAPI_IF_ReadData(ATAPI_IF_THRD_INFO *Info)
{
    int                 err;
    tU32                readcdfrmnum;
    ATAPI_IF_CMD_PACKET cmdpacket;

    if(Info->ThrdPlayStatus.Status == ATAPI_IF_STRM_STATUS_NO_DISK)
        return OSAL_ERROR;

    readcdfrmnum = Info->ProcInfo->DataRequestNumFrames;
    if(readcdfrmnum <= 0)
        return 0;
    if(readcdfrmnum > ATAPI_IF_DATA_REQUEST_BUFF_CDFRAMES)
        readcdfrmnum = ATAPI_IF_DATA_REQUEST_BUFF_CDFRAMES;

    InitCmdPacket(&cmdpacket, -1);
    cmdpacket.cmd.buffer = (unsigned char *)Info->ProcInfo->DataRequestBuffer;
    cmdpacket.cmd.buflen = ATAPI_IF_DATA_REQUEST_BUFF_SIZE;

    cmdpacket.cmd.cmd[0] = GPCMD_READ_CD;
    cmdpacket.cmd.cmd[1] = 0; // Any Sector Type, no DAP
    cmdpacket.cmd.cmd[2] = (Info->ProcInfo->DataRequestLBA >> 24) & 0xff;
    cmdpacket.cmd.cmd[3] = (Info->ProcInfo->DataRequestLBA >> 16) & 0xff;
    cmdpacket.cmd.cmd[4] = (Info->ProcInfo->DataRequestLBA >> 8) & 0xff;
    cmdpacket.cmd.cmd[5] = Info->ProcInfo->DataRequestLBA & 0xff;
    cmdpacket.cmd.cmd[6] = (readcdfrmnum >> 16) & 0xff;
    cmdpacket.cmd.cmd[7] = (readcdfrmnum >> 8) & 0xff;
    cmdpacket.cmd.cmd[8] = readcdfrmnum & 0xff;
    cmdpacket.cmd.cmd[9] = 0x10; // User Data only
    if(ioctl(Info->CDHandle, CDROM_SEND_PACKET, &cmdpacket.cmd) < 0)
    {
        ATAPI_IF_DEBUG_PRINTF("GPCMD_READ_CD failed (%i)\n", errno);
        return OSAL_ERROR;
    }

    return readcdfrmnum;
}

#ifdef MEDIA_STATE_NOTIFY
tU16  AIF_u16GetDriveStatus(ATAPI_IF_PROC_INFO *Info, tU16 u16Type)

{
   tU16 u16Status;
   tU32 u32Status;

   ATAPI_IF_TRACE_ENTER_U4(CDID_CD_u16GetDriveStatus, u16Type, 0, 0, 0);

   switch(u16Type)
   {
   case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      /* u16Status:
   OSAL_C_U16_MEDIA_EJECTED
   OSAL_C_U16_INCORRECT_MEDIA
   OSAL_C_U16_DATA_MEDIA
   OSAL_C_U16_AUDIO_MEDIA
   OSAL_C_U16_UNKNOWN_MEDIA*/
      u32Status = Info->DriveStateInfo.u32MediaChange;
      u16Status = (tU16)(u32Status == AIF_LDR_STATUS_NOT_INITIALIZED
      ? OSAL_C_U16_MEDIA_EJECTED : u32Status);
      break;

   case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      /*u16Status =
   OSAL_C_U16_DEVICE_OK
   OSAL_C_U16_DEVICE_FAIL*/
      //notify client, if new status is different from last notified status
      u32Status = Info->DriveStateInfo.u32TotalFailure;
      u16Status =  (tU16)(u32Status == AIF_LDR_STATUS_NOT_INITIALIZED
      ? OSAL_C_U16_DEVICE_OK : u32Status);
      break;

   case OSAL_C_U16_NOTI_MEDIA_STATE:
      /* u16Status =
   OSAL_C_U16_MEDIA_NOT_READY
   OSAL_C_U16_MEDIA_READY*/
      u32Status = Info->DriveStateInfo.u32MediaState;
      u16Status =  (tU16)(u32Status == AIF_LDR_STATUS_NOT_INITIALIZED
      ? OSAL_C_U16_MEDIA_NOT_READY : u32Status);
      break;

   case OSAL_C_U16_NOTI_DEFECT:
      /*u16Status =
   OSAL_C_U16_DEFECT_LOAD_EJECT
   OSAL_C_U16_DEFECT_LOAD_INSERT
   OSAL_C_U16_DEFECT_DISCTOC
   OSAL_C_U16_DEFECT_DISC
   OSAL_C_U16_DEFECT_READ_ERR
   OSAL_C_U16_DEFECT_READ_OK*/
      u32Status = Info->DriveStateInfo.u32Defect;
      u16Status =  (tU16)(u32Status == AIF_LDR_STATUS_NOT_INITIALIZED
      ? OSAL_C_U16_DEFECT_TEMP_READ : u32Status);
      break;

   case OSAL_C_U16_NOTI_DEVICE_READY:
      /*u16Status =
   OSAL_C_U16_DEVICE_NOT_READY
   OSAL_C_U16_DEVICE_READY
   OSAL_C_U16_DEVICE_UV_NOT_READY
   OSAL_C_U16_DEVICE_UV_READY*/
      u32Status = Info->DriveStateInfo.u32DeviceReady;
      u16Status = (tU16)(u32Status == AIF_LDR_STATUS_NOT_INITIALIZED
      ? OSAL_C_U16_DEVICE_NOT_READY : u32Status);
      break;
   default:
      u16Status = AIF_LDR_STATUS_NOT_INITIALIZED;
      ATAPI_IF_PRINTF_FATAL(" TYPE_XXX not supported: 0x%04X",
      (unsigned int)u16Type);
      ;
   } //switch(u16Type)


   ATAPI_IF_TRACE_LEAVE_U4(CD_u16GetDriveStatus,
   u16Status != AIF_LDR_STATUS_NOT_INITIALIZED
   ? OSAL_E_NOERROR
   : OSAL_E_INVALIDVALUE, u16Type, u16Status, 0, 0);

   return u16Status;
}

/************************************************************************
* FUNCTION                                                              *
*      CDID_AIF_u32SetDriveStatus                                           *
*                                                                       *
* DESCRIPTION                                                           *
*      Set drive status and notifies registered clients                 *
*                                                                       *
*                                                                       *
************************************************************************/
static tU32 AIF_u32SetDriveStatus(ATAPI_IF_PROC_INFO *Info,
                                          tU16 u16Type, tU32 u32Status)

{
   tBool bDoNotify;
   tU32 u32Ret = OSAL_E_NOERROR;

   ATAPI_IF_TRACE_ENTER_U4(AIF_u32SetDriveStatus, u16Type, u32Status, 0, 0);

   switch(u16Type)
   {
   case OSAL_C_U16_NOTI_MEDIA_CHANGE:
      /* u16Status:
   OSAL_C_U16_MEDIA_EJECTED
   OSAL_C_U16_INCORRECT_MEDIA
   OSAL_C_U16_DATA_MEDIA
   OSAL_C_U16_AUDIO_MEDIA
   OSAL_C_U16_UNKNOWN_MEDIA*/
      //notify client, if new status is different from last notified status
      ATAPI_IF_PRINTF_U4("MEDIA_CHANGE:old=%x,new=%x\n",Info->DriveStateInfo.u32MediaChange,u32Status);
      bDoNotify = Info->DriveStateInfo.u32MediaChange != u32Status;
      Info->DriveStateInfo.u32MediaChange   = u32Status;

      break;

   case OSAL_C_U16_NOTI_TOTAL_FAILURE:
      /*u16Status =
   OSAL_C_U16_DEVICE_OK
   OSAL_C_U16_DEVICE_FAIL*/
      //notify client, if new status is different from last notified status
      ATAPI_IF_PRINTF_U4("TOTAL_FAILURE:old=%x\n",Info->DriveStateInfo.u32TotalFailure);
      bDoNotify = Info->DriveStateInfo.u32TotalFailure != u32Status;
      Info->DriveStateInfo.u32TotalFailure   = u32Status;
      break;

   case OSAL_C_U16_NOTI_MEDIA_STATE:
      /* u16Status =
   OSAL_C_U16_MEDIA_NOT_READY
   OSAL_C_U16_MEDIA_READY*/
      ATAPI_IF_PRINTF_U4("MEDIA_STATE:old=%x\n",Info->DriveStateInfo.u32MediaState);
      bDoNotify = Info->DriveStateInfo.u32MediaState
      != u32Status;
      Info->DriveStateInfo.u32MediaState   = u32Status;
      if(u32Status == OSAL_C_U16_MEDIA_NOT_READY)
      { // if media state is NOT READY, media change must be resetted
         Info->DriveStateInfo.u32MediaChange   = (tU32)OSAL_C_U16_UNKNOWN_MEDIA;
      } //if(bDoNotify)
      break;

   case OSAL_C_U16_NOTI_DEFECT:
      /*u16Status =
   OSAL_C_U16_DEFECT_LOAD_EJECT
   OSAL_C_U16_DEFECT_LOAD_INSERT
   OSAL_C_U16_DEFECT_DISCTOC
   OSAL_C_U16_DEFECT_DISC
   OSAL_C_U16_DEFECT_READ_ERR
   OSAL_C_U16_DEFECT_READ_OK*/
      ATAPI_IF_PRINTF_U4("MEDIA_DEFECT:old=%x\n",Info->DriveStateInfo.u32Defect);
      bDoNotify = Info->DriveStateInfo.u32Defect != u32Status
      &&
      u32Status != AIF_LDR_STATUS_NOT_INITIALIZED;
      Info->DriveStateInfo.u32Defect   = u32Status;
      break;

   case OSAL_C_U16_NOTI_DEVICE_READY:
      /*u16Status =
   OSAL_C_U16_DEVICE_NOT_READY
   OSAL_C_U16_DEVICE_READY
   OSAL_C_U16_DEVICE_UV_NOT_READY
   OSAL_C_U16_DEVICE_UV_READY*/
      ATAPI_IF_PRINTF_U4("DEVICE_READY:old=%x\n",Info->DriveStateInfo.u32DeviceReady);
      bDoNotify = Info->DriveStateInfo.u32DeviceReady
      != u32Status;
      Info->DriveStateInfo.u32DeviceReady   = u32Status;
      break;
   default:
      bDoNotify = FALSE;
      u32Ret    = OSAL_E_NOTSUPPORTED;
      ATAPI_IF_PRINTF_FATAL(" TYPE_XXX not supported: 0x%04X",
      (unsigned int)u16Type);
   } //switch(u16Type)


   if(bDoNotify)
   {
      AIF_vMediaNotifyClients(Info, u16Type, (tU16)u32Status);
   } //if(bDoNotify)

   ATAPI_IF_TRACE_LEAVE_U4(AIF_u32SetDriveStatus, u32Ret, bDoNotify,
   u16Type, u32Status, 0);

   return u32Ret;
}

void ATAPI_IF_u32Update_Loader_Status(tU8 u8MediaEvent,tU8 u8MediaStatus,ATAPI_IF_THRD_INFO *Info)
{
   ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_u32Update_Loader_Status,u8MediaEvent,u8MediaStatus,0,0);
   static tU8 u8InternalLoaderStatusBackup = AIF_LDR_STATUS_INITIALIZED;

   tU32 u32LoaderState=AIF_LDR_STATUS_INITIALIZED,
   u8LastInternalLoaderState=AIF_LDR_STATUS_INITIALIZED;

   tBool  bMediaPresent = 0 != (u8MediaStatus & 0x02);  /* table 95*/
   tBool  bDoorOpen     = 0 != (u8MediaStatus & 0x01);  /* table 95*/

   /*
      MASCA Status           Media Status of SCSI                  Comment
                           Media Event	  Media Present Door Open
      No CD                 NoChg         0	            0	        No media at all
      Inserting in progress	MediaChanged	0	            1	        User �feeds� the device with a media
      CD position           MediaChanged  1	            0	        TOC is now read and medium can be accessed
      CD in play position   NewMedia	    1	            0	        TOC is now read and medium can be accessed
      Ejecting in progress	EjectRequest	1	            1	        Ejecting request from application triggered
      In-Slot	              MediaRemoval	1	            1	        CD is still In-Slot and needs to be removed/reinserted by user or can be reinserted on application request.
      CD is ejected         MediaRemoval	0	            0	        Medium was removed from slot
   */

   if(AIF_MEDIA_EVENT_NOCHG == u8MediaEvent)
   {
      if(!bMediaPresent && !bDoorOpen)
      u32LoaderState = AIF_LDR_STATUS_NO_DISK;
      else if(bMediaPresent && !bDoorOpen)
      u32LoaderState = AIF_LDR_STATUS_CD_PLAYABLE;
      else if(!bMediaPresent && bDoorOpen)
      u32LoaderState = AIF_LDR_STATUS_EJECTED; //Fix me : AIF_LDR_STATUS_IN_SLOT also valid state

   } //if(AIF_MEDIA_EVENT_NOCHG == u8MediaEvent)
   else if(AIF_MEDIA_EVENT_MEDIA_CHANGE == u8MediaEvent)
   {
      if(!bMediaPresent && bDoorOpen)
      u32LoaderState = AIF_LDR_STATUS_INSERT_IN_PROG;
      else if(bMediaPresent && !bDoorOpen)
      u32LoaderState =
      AIF_LDR_STATUS_CD_INSIDE;
   } //if(AIF_MEDIA_EVENT_MEDIA_CHANGE == u8MediaEvent)
   else if(AIF_MEDIA_EVENT_NEW_MEDIA == u8MediaEvent)
   {
      if(bMediaPresent && !bDoorOpen)
      u32LoaderState = AIF_LDR_STATUS_CD_PLAYABLE;
   } //if(AIF_MEDIA_EVENT_NEW_MEDIA == u8MediaEvent)
   else if(AIF_MEDIA_EVENT_EJECT_REQ == u8MediaEvent)
   {
      if(bMediaPresent && bDoorOpen)
      u32LoaderState = AIF_LDR_STATUS_EJECT_IN_PROGRESS;
   } //if(AIF_MEDIA_EVENT_EJECT_REQ == u8MediaEvent)
   else if(AIF_MEDIA_EVENT_MEDIA_REMOVAL == u8MediaEvent)
   {
      //fixme::below condition should be (!bMediaPresent && bDoorOpen) Fixed
      if(!bMediaPresent && bDoorOpen)
      u32LoaderState = AIF_LDR_STATUS_IN_SLOT;
      else if(!bMediaPresent && !bDoorOpen)
      u32LoaderState = AIF_LDR_STATUS_EJECTED;
   } //if(AIF_MEDIA_EVENT_MEDIA_REMOVAL == u8MediaEvent)

   ATAPI_IF_PRINTF_U4("Loader detected state=%x,notifiable state=%x\n",u32LoaderState,u8LastInternalLoaderState);
   //save important loaderstate
   switch(u32LoaderState)
   {
   case AIF_LDR_STATUS_NO_DISK:
   case AIF_LDR_STATUS_CD_INSIDE:
   case AIF_LDR_STATUS_CD_PLAYABLE:
   case AIF_LDR_STATUS_CD_UNREADABLE:
   case AIF_LDR_STATUS_IN_SLOT:
   case AIF_LDR_STATUS_EJECTED:
      u8LastInternalLoaderState = u32LoaderState;
      Info->ProcInfo->MediaStatus.InternelStatus=(ATAPI_IF_LOADER_STATUS)u32LoaderState;
      break;
   case AIF_LDR_STATUS_UNKNOWN:
   case AIF_LDR_STATUS_INSERT_IN_PROG:
   case AIF_LDR_STATUS_LOAD_ERROR:
   case AIF_LDR_STATUS_EJECT_IN_PROGRESS:
   case AIF_LDR_STATUS_EJECT_ERROR:
   case AIF_LDR_STATUS_INITIALIZED:
   default:
      break;
   } //switch(u32LoaderState)
   ATAPI_IF_PRINTF_U4("Loader detected state=%x,notifiable state=%x\n",u32LoaderState,u8LastInternalLoaderState);
   if(u8LastInternalLoaderState != u8InternalLoaderStatusBackup)
   {
      u8InternalLoaderStatusBackup = u8LastInternalLoaderState;
      switch(u8LastInternalLoaderState)
      {
      case AIF_LDR_STATUS_IN_SLOT:
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_DEFECT,
         AIF_LDR_STATUS_NOT_INITIALIZED);
         break; //case AIF_LDR_STATUS_IN_SLOT:

      case AIF_LDR_STATUS_CD_PLAYABLE:
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_DEFECT,
         AIF_LDR_STATUS_NOT_INITIALIZED);
         switch(Info->ProcInfo->CDCache.MediaType)
         {
         case OSAL_C_U8_DATA_MEDIA:
            AIF_u32SetDriveStatus(Info->ProcInfo,
            OSAL_C_U16_NOTI_MEDIA_CHANGE,
            OSAL_C_U16_DATA_MEDIA);
            AIF_u32SetDriveStatus(Info->ProcInfo,
            OSAL_C_U16_NOTI_MEDIA_STATE,
            OSAL_C_U16_MEDIA_READY);
            break;
         case OSAL_C_U8_AUDIO_MEDIA:
            AIF_u32SetDriveStatus(Info->ProcInfo,
            OSAL_C_U16_NOTI_MEDIA_CHANGE,
            OSAL_C_U16_AUDIO_MEDIA);
            AIF_u32SetDriveStatus(Info->ProcInfo,
            OSAL_C_U16_NOTI_MEDIA_STATE,
            OSAL_C_U16_MEDIA_READY);
            break;
         case OSAL_C_U8_INCORRECT_MEDIA:
         case OSAL_C_U8_UNKNOWN_MEDIA:
         default:
            ;
         } //switch(u8MediaType)
         break; //case AIF_LDR_STATUS_CD_PLAYABLE:

      case AIF_LDR_STATUS_EJECTED:
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_MEDIA_STATE,
         OSAL_C_U16_MEDIA_NOT_READY);
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_DEFECT,
         AIF_LDR_STATUS_NOT_INITIALIZED);
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_MEDIA_CHANGE,
         OSAL_C_U16_MEDIA_EJECTED);
         break; //case AIF_LDR_STATUS_EJECTED:

      case AIF_LDR_STATUS_CD_UNREADABLE:
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_DEFECT,
         AIF_LDR_STATUS_NOT_INITIALIZED);
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_MEDIA_CHANGE,
         OSAL_C_U16_INCORRECT_MEDIA);
         break; //case CD_INTERNAL_LOADERSTATE_CD_UNREADABLE:

      case AIF_LDR_STATUS_EJECT_ERROR:
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_DEFECT,
         OSAL_C_U16_DEFECT_LOAD_EJECT);
         break;

      case AIF_LDR_STATUS_LOAD_ERROR:
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_DEFECT,
         OSAL_C_U16_DEFECT_LOAD_INSERT);
         break;

      case AIF_LDR_STATUS_NO_DISK:
      case AIF_LDR_STATUS_UNKNOWN:
      case AIF_LDR_STATUS_EJECT_IN_PROGRESS:
      case AIF_LDR_STATUS_INSERT_IN_PROG:
      case AIF_LDR_STATUS_CD_INSIDE:
      default:
         AIF_u32SetDriveStatus(Info->ProcInfo,
         OSAL_C_U16_NOTI_DEFECT,
         AIF_LDR_STATUS_NOT_INITIALIZED);
      } //switch(u8LastInternalLoaderState)
   } //if(u8LastInternalLoaderState != u8InternalLoaderStatusBackup)

   ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_u32Update_Loader_Status, u8MediaEvent,u8MediaStatus,
   u8LastInternalLoaderState, u8InternalLoaderStatusBackup, 0);

}
#endif
/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/******************************************************************************
 *FUNCTION      :ATAPI_IF_UpdateThread
 *
 *DESCRIPTION   :A thread, started by ATAPI_IF when the device is initialized.
 *               CDROM drives in modern PCs aren't capable of native audio
 *               playback. Therefore this thread reads data from the CD and
 *               usese ALSA to play it.
 *
 *PARAMETER     :Arg    Index of drive to handle
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob
 *              :Modified for thread synchronisation by hsr9kor
 *****************************************************************************/
void ATAPI_IF_UpdateThread(void *Arg)
{
    tU32                driveidx = (tU32)Arg;
//    tChar               semname[] = ATAPI_IF_SEM_NAME;
    tChar               shmname[] = ATAPI_IF_SHMEM_NAME;
    ATAPI_IF_THRD_INFO  info;
    ATAPI_IF_CMD_PACKET cmdpacket;
    tU32                lastnotifylba = 0xffffffff;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_UpdateThread, 0, 0, 0, 0);

    info.ThrdPlayStatus.Status = ATAPI_IF_STRM_STATUS_NO_DISK;
    snd_output_stdio_attach(&snd_output, stdout, 0);

    semname[sizeof(ATAPI_IF_SEM_NAME) - 2] = '0' + (tChar)driveidx;
    if(OSAL_s32SemaphoreOpen(semname, &info.SemHandle) != OSAL_OK)
    {
        ATAPI_IF_PRINTF_ERRORS("ATAPI_IF_UpdateThread failed to access Semaphore");
    }
    else
    {
        shmname[sizeof(ATAPI_IF_SHMEM_NAME) - 2] = '0' + (tChar)driveidx;
        info.ShmHandle = OSAL_SharedMemoryOpen(shmname, OSAL_EN_READWRITE);
        if(info.ShmHandle == OSAL_ERROR)
        {
            ATAPI_IF_PRINTF_ERRORS("ATAPI_IF_UpdateThread failed to access Shared Memory");
        }
        else
        {
            info.ProcInfo = (ATAPI_IF_PROC_INFO *)OSAL_pvSharedMemoryMap(info.ShmHandle, OSAL_EN_READWRITE, sizeof(ATAPI_IF_PROC_INFO), 0);

            if(info.ProcInfo == NULL)
            {
                ATAPI_IF_PRINTF_ERRORS("ATAPI_IF_UpdateThread failed to map Shared Memory");
            }
            else
            {
                static const int    sleepduration = 1000, timeout = 60000;
                int                 sleepcount = 0;

                memset(info.ProcInfo, 0, sizeof(ATAPI_IF_PROC_INFO));
#ifdef MEDIA_STATE_NOTIFY

    info.ProcInfo->MediaStatus.InternelStatus = AIF_LDR_STATUS_NOT_INITIALIZED;
#endif

                info.CDHandle = -1;
#ifdef MEDIA_STATE_NOTIFY

                int i;
                for(i = 0; i < ATAPI_IF_MAX_NOTIFIERS; i++)
                {
                    info.ProcInfo->arMediaStatusCallbacks[i].u16AppID = OSAL_C_U16_INVALID_APPID;
                } //for(i = 0; i < ATAPI_IF_MAX_NOTIFIERS; i++)
#endif
                if (OSAL_ERROR == OSAL_s32SemaphorePost(semhandle))
                {
                    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_s32Init,0, 0, 0, 0);
                    NORMAL_M_ASSERT_ALWAYS();
                }

                while(info.ThrdPlayStatus.Status != ATAPI_IF_STRM_STATUS_SHUTDOWN && info.CDHandle < 0)
                {
                    // Try /dev/cdrom first as it should point to the real device, no matter what it is
                    info.CDHandle = open(ATAPI_IF_LINUX_DEVICE_NAME_STRING, O_RDONLY | O_NONBLOCK);
                    if(info.CDHandle < 0)
                        // If that didn't work, try /dev/sr0 as it another symlink that should point to the real device
                        info.CDHandle = open(ATAPI_IF_LINUX_DEVICE_NAME_STRING2, O_RDONLY | O_NONBLOCK);
                    if(info.CDHandle < 0)
                        // If that still didn't work, try /dev/scd0 which is reserved for SCSI CDROMs but that is what VBox provides anyways
                        info.CDHandle = open(ATAPI_IF_LINUX_DEVICE_NAME_STRING3, O_RDONLY | O_NONBLOCK);

                    if(info.CDHandle < 0)
                    {
                        ATAPI_IF_PRINTF_ERRORS("ATAPI_IF_UpdateThread failed to open %s", ATAPI_IF_LINUX_DEVICE_NAME_STRING);
                    }
                    else
                        break;

                    if(info.ProcInfo->ChangeRequestPending)
                    {
                        if(OSAL_s32SemaphoreWait(info.SemHandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                        {
                            if(info.ProcInfo->ChangeRequest.Status == ATAPI_IF_STRM_STATUS_SHUTDOWN)
                                info.ThrdPlayStatus.Status = ATAPI_IF_STRM_STATUS_SHUTDOWN;

                            OSAL_s32SemaphorePost(info.SemHandle);
                        }
                    }

                    OSAL_s32ThreadWait(sleepduration);
                    sleepcount += sleepduration;
                    if(sleepcount > timeout)
                        break;
                }

                if(info.CDHandle < 0)
                {
                    ATAPI_IF_PRINTF_ERRORS("ATAPI_IF_UpdateThread tried to open %s for %i ms and gave up", ATAPI_IF_LINUX_DEVICE_NAME_STRING, sleepcount);
                }
                else
               {
                  ATAPI_IF_PRINTF_U4("UpdateThread is running...");
                  // Read SW and HW version info
                  ATAPI_IF_u32Inquiry(info.ProcInfo, info.CDHandle);

                  // Just started but there might be a CD already. Try to cache it.
                  if(OSAL_s32SemaphoreWait(info.SemHandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                  {
                     ATAPI_IF_PRINTF_U4("UpdateThread checks for CD on statup...");

#ifdef MEDIA_STATE_NOTIFY
                     //set initial state of MEDIA/DEVICE status
                     AIF_u32SetDriveStatus(info.ProcInfo,
                                          OSAL_C_U16_NOTI_MEDIA_STATE, 
                                          OSAL_C_U16_MEDIA_NOT_READY);
                     AIF_vMediaNotifyClients(info.ProcInfo,
                                       OSAL_C_U16_NOTI_MEDIA_STATE,
                                       OSAL_C_U16_MEDIA_NOT_READY);
                     AIF_u32SetDriveStatus(info.ProcInfo,
                                          OSAL_C_U16_NOTI_MEDIA_CHANGE,
                                          OSAL_C_U16_UNKNOWN_MEDIA);
                     AIF_vMediaNotifyClients(info.ProcInfo,
                                          OSAL_C_U16_NOTI_MEDIA_CHANGE,
                                          OSAL_C_U16_UNKNOWN_MEDIA);
                     AIF_u32SetDriveStatus(info.ProcInfo,
                                          OSAL_C_U16_NOTI_TOTAL_FAILURE,
                                          OSAL_C_U16_DEVICE_OK);
                     AIF_vMediaNotifyClients(info.ProcInfo,
                                          OSAL_C_U16_NOTI_TOTAL_FAILURE,
                                          OSAL_C_U16_DEVICE_OK);

                     AIF_u32SetDriveStatus(info.ProcInfo,
                                          OSAL_C_U16_NOTI_DEVICE_READY,
                                          OSAL_C_U16_DEVICE_READY);
                     AIF_vMediaNotifyClients(info.ProcInfo,
                                          OSAL_C_U16_NOTI_TOTAL_FAILURE,
                                          OSAL_C_U16_DEVICE_OK);
                     info.ThrdDriveLastResponse.u8MediaEventCode=0xFF;
                     info.ThrdDriveLastResponse.u8MediaEventStatusCode=0xFF;
#endif
                        ATAPI_IF_CacheMedia(&info);
                        if(info.ProcInfo->CDCache.ValidFlag)
                        {
                            info.ThrdPlayStatus.Status = ATAPI_IF_STRM_STATUS_STOP;
                            memcpy(&info.ProcInfo->StatusChange, &info.ThrdPlayStatus, sizeof(ATAPI_IF_PLAY_STATUS));
                            info.ProcInfo->StatusChangePending = 1;
                            ATAPI_IF_PlayNotifyClients(driveidx, &info);
                            lastnotifylba = 0xffffffff;
                        }
                        OSAL_s32SemaphorePost(info.SemHandle);
                    }

                    for(; info.ThrdPlayStatus.Status != ATAPI_IF_STRM_STATUS_SHUTDOWN; )
                    {
                        InitCmdPacket(&cmdpacket, 7);
                        cmdpacket.cmd.cmd[0] = GPCMD_GET_EVENT_STATUS_NOTIFICATION;
                        cmdpacket.cmd.cmd[1] = 0x01;  // Immed
                        cmdpacket.cmd.cmd[4] = 0x10;  // Enable only Media Class event //Refer mmc4r01d.pdf section 5.7 (Page no:155)
                        if(ioctl(info.CDHandle, CDROM_SEND_PACKET, &cmdpacket.cmd) < 0)
                        {
                            ATAPI_IF_PRINTF_ERRORS("GPCMD_GET_EVENT_STATUS_NOTIFICATION failed");
                        }
                        else
                        {
#ifdef MEDIA_STATE_NOTIFY
                           if ( (info.ThrdDriveLastResponse.u8MediaEventCode
                                                     !=cmdpacket.buff[4]) ||
                                 (info.ThrdDriveLastResponse.u8MediaEventStatusCode
                                                     !=cmdpacket.buff[5]))
                           {
                              if(OSAL_s32SemaphoreWait(info.SemHandle, 
                                                OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                              {
                                 ATAPI_IF_u32Update_Loader_Status(
                                                (cmdpacket.buff[4] & 0x0F),
                                                cmdpacket.buff[5],
                                                &info);
                                 OSAL_s32SemaphorePost(info.SemHandle);
                              }

                           }
#endif

                            if(info.ProcInfo->CDCache.ValidFlag)
                            {
                            	//Check event Header value and confirm is it a Media Event
                                if((cmdpacket.buff[2] & 0x07) == 4)     // If it is a Media Event,byte 2(3 to 0 bit) = 4  //refer mmc4r01d.pdf PN:157
                                {
                                	//Check Event Descriptor
                                    if((cmdpacket.buff[5] & 0x02) == 0) // No Media Present
                                    {
                                    	//ATAPI_IF_PRINTF_U4("cmdpacket.buff[4]=%x\n",cmdpacket.buff[4]);
                                        if(OSAL_s32SemaphoreWait(info.SemHandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                                        {
                                            ATAPI_IF_PRINTF_U4("CD removed - invalidating cache");
                                            info.ProcInfo->CDCache.ValidFlag = 0;
                                            info.ThrdPlayStatus.Status = ATAPI_IF_STRM_STATUS_NO_DISK;
                                            memcpy(&info.ProcInfo->StatusChange, &info.ThrdPlayStatus, sizeof(ATAPI_IF_PLAY_STATUS));
                                            info.ProcInfo->StatusChangePending = 1;
                                            ATAPI_IF_PlayNotifyClients(driveidx, &info);
                                            lastnotifylba = 0xffffffff;
                                            OSAL_s32SemaphorePost(info.SemHandle);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if((cmdpacket.buff[2] & 0x07) == 4)     // It is a Media Event
                                {
                                    if((cmdpacket.buff[4] & 0x0f) == 2) // NewMedia
                                    {
//                                        OSAL_s32ThreadWait(4000);   // Ok for my drive, too short for some others
                                        OSAL_s32ThreadWait(10000);  // Seems ok so far
                                        if(OSAL_s32SemaphoreWait(info.SemHandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                                        {
                                            ATAPI_IF_PRINTF_U4("UpdateThread checks for CD in response to GPCMD_GET_EVENT_STATUS_NOTIFICATION...");
                                            ATAPI_IF_CacheMedia(&info);
                                            if(info.ProcInfo->CDCache.ValidFlag == 1)
                                            {
                                                info.ThrdPlayStatus.Status = ATAPI_IF_STRM_STATUS_STOP;
                                                memcpy(&info.ProcInfo->StatusChange, &info.ThrdPlayStatus, sizeof(ATAPI_IF_PLAY_STATUS));
                                                info.ProcInfo->StatusChangePending = 1;
                                                ATAPI_IF_PlayNotifyClients(driveidx, &info);
                                                lastnotifylba = 0xffffffff;
                                            }
                                            OSAL_s32SemaphorePost(info.SemHandle);
                                        }
                                    }
                                }
                            }
                        	info.ThrdDriveLastResponse.u8MediaEventCode=cmdpacket.buff[4];
                        	info.ThrdDriveLastResponse.u8MediaEventStatusCode=cmdpacket.buff[5];

                        }

                        if(info.ProcInfo->DataRequestPending && !info.ProcInfo->DataRequestComplete)
                        {
                            if(OSAL_s32SemaphoreWait(info.SemHandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                            {
                                info.ProcInfo->DataRequestNumFrames = ATAPI_IF_ReadData(&info);

                                info.ProcInfo->DataRequestPending = 0;
                                info.ProcInfo->DataRequestComplete = 1;
                                OSAL_s32SemaphorePost(info.SemHandle);
                            }
                        }

                        if(info.ProcInfo->ChangeRequestPending)
                        {
                            if(OSAL_s32SemaphoreWait(info.SemHandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                            {
                                ATAPI_IF_STREAM_STATUS  oldstatus = info.ThrdPlayStatus.Status;
                                ATAPI_IF_DUMP_PLAYSTATUS("\n**Thread received request: ", &info.ProcInfo->ChangeRequest);
                                ATAPI_IF_ChangeStatus(&info.ProcInfo->CDCache, &info.ThrdPlayStatus, &info.ProcInfo->ChangeRequest, FALSE);
                                info.ProcInfo->ChangeRequestPending = 0;

                                memcpy(&info.ProcInfo->StatusChange, &info.ThrdPlayStatus, sizeof(ATAPI_IF_PLAY_STATUS));
                                info.ProcInfo->StatusChangePending = 1;
                                if(info.ThrdPlayStatus.Status != oldstatus)
                                {
                                    ATAPI_IF_PlayNotifyClients(driveidx, &info);
                                    lastnotifylba = 0xffffffff;
                                }
                                OSAL_s32SemaphorePost(info.SemHandle);

                                // CurrentLBA must be set to EndLBA for notify, but to stream data, it must be lowered by up to ATAPI_IF_SAMPLE_BUFF_CDFRAMES frames
                                if(info.ThrdPlayStatus.Status == ATAPI_IF_STRM_STATUS_FAST_BACKWARD)
                                {
                                    info.ThrdPlayStatus.CurrentLBA -= ATAPI_IF_SAMPLE_BUFF_CDFRAMES;
                                    if(info.ThrdPlayStatus.CurrentLBA < info.ThrdPlayStatus.StartLBA)
                                        info.ThrdPlayStatus.CurrentLBA = info.ThrdPlayStatus.StartLBA;
                                }
                            }
                        }

                        if(info.ThrdPlayStatus.Status == ATAPI_IF_STRM_STATUS_PLAY ||
                           info.ThrdPlayStatus.Status == ATAPI_IF_STRM_STATUS_FAST_FORWARD ||
                           info.ThrdPlayStatus.Status == ATAPI_IF_STRM_STATUS_FAST_BACKWARD)
                        {
                            if(info.ALSAHandle == NULL)
                                info.ALSAHandle = ATAPI_IF_InitALSA();

                            if(info.ALSAHandle != NULL)
                            {
                                tS32    result = ATAPI_IF_StreamData(&info);

                                if(result >= 0)
                                {
                                    if(info.ThrdPlayStatus.Status == ATAPI_IF_STRM_STATUS_FAST_FORWARD)
                                    {
                                        result += ATAPI_IF_SCAN_SECTORS_TO_SKIP;
                                    }
                                    else if(info.ThrdPlayStatus.Status == ATAPI_IF_STRM_STATUS_FAST_BACKWARD)
                                    {
                                        result -= ATAPI_IF_SCAN_SECTORS_TO_SKIP;
                                    }
                                    if(OSAL_s32SemaphoreWait(info.SemHandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                                    {
                                        info.ThrdPlayStatus.CurrentLBA += result;
                                        if(info.ThrdPlayStatus.CurrentLBA >= info.ThrdPlayStatus.EndLBA)
                                        {
                                            ATAPI_IF_DEBUG_PRINTF("End of play range reached - stopping\n");
                                            info.ThrdPlayStatus.CurrentLBA = info.ThrdPlayStatus.EndLBA;
                                            info.ThrdPlayStatus.Status = ATAPI_IF_STRM_STATUS_STOP;
                                            ATAPI_IF_PlayNotifyClients(driveidx, &info);
                                            lastnotifylba = info.ThrdPlayStatus.CurrentLBA; // Don't cause another callback for updated time
                                        }
                                        else if(info.ThrdPlayStatus.CurrentLBA < info.ThrdPlayStatus.StartLBA)
                                        {
                                            ATAPI_IF_DEBUG_PRINTF("Position is before start of play range - stopping\n");
                                            info.ThrdPlayStatus.CurrentLBA = info.ThrdPlayStatus.StartLBA;
                                            info.ThrdPlayStatus.Status = ATAPI_IF_STRM_STATUS_STOP;
                                            ATAPI_IF_PlayNotifyClients(driveidx, &info);
                                            lastnotifylba = info.ThrdPlayStatus.CurrentLBA; // Don't cause another callback for updated time
                                        }
                                        memcpy(&info.ProcInfo->StatusChange, &info.ThrdPlayStatus, sizeof(ATAPI_IF_PLAY_STATUS));
                                        info.ProcInfo->StatusChangePending = 1;
                                        if(lastnotifylba / 75 != info.ThrdPlayStatus.CurrentLBA / 75)
                                        {
                                            ATAPI_IF_PlayNotifyClients(driveidx, &info);
                                            lastnotifylba = info.ThrdPlayStatus.CurrentLBA;
                                        }
                                        OSAL_s32SemaphorePost(info.SemHandle);
                                    }
                                } else {
                                	OSAL_s32ThreadWait(1000);
                                }
                            }
                        }
                        else
                        {
                            if(info.ALSAHandle != NULL)
                            {
                                snd_pcm_close(info.ALSAHandle);
                                info.ALSAHandle = NULL;
                            }
                            OSAL_s32ThreadWait(100);
                        }
                    }

                    ATAPI_IF_PRINTF_U4("UpdateThread is shutting down...");

                    close(info.CDHandle);
                }
            }
            OSAL_s32SharedMemoryUnmap(info.ProcInfo, sizeof(ATAPI_IF_PROC_INFO));
        }
        OSAL_s32SharedMemoryClose(info.ShmHandle);
    }
    OSAL_s32SemaphoreClose(info.SemHandle);

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_UpdateThread, 0, 0, 0, 0, 0);
}

/******************************************************************************
 *FUNCTION      :ATAPI_IF_s32Init
 *
 *DESCRIPTION   :Initialize ATAPI_IF
 *
 *PARAMETER     :void
 *
 *RETURNVALUE   :s32Result: OSAL_E_NOERROR in case of success
 *						appropriate error-code in case of failure
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob 02/04/2014
                 Modified by hsr9kor on 10/06/2016 to have thread synchronisation
 *****************************************************************************/

tS32 ATAPI_IF_s32Init(void)
{
    tS32                s32Result = OSAL_E_NOERROR;
    tU32                driveidx;
    tChar               shmemname[] = ATAPI_IF_SHMEM_NAME;
    OSAL_tShMemHandle   shmemhandle;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_s32Init, 0, 0, 0, 0);

    /* create the semaphores */
    for(driveidx = 0; driveidx < ATAPI_IF_DRIVE_NUMBER; driveidx++)
    {
        semname[sizeof(ATAPI_IF_SEM_NAME) - 2] = '0' + (tChar)driveidx;
        shmemname[sizeof(ATAPI_IF_SHMEM_NAME) - 2] = '0' + (tChar)driveidx;

        shmemhandle = OSAL_SharedMemoryCreate(shmemname, OSAL_EN_READWRITE, sizeof(ATAPI_IF_PROC_INFO));
        if(shmemhandle == OSAL_ERROR)
        {
            shmemhandle = OSAL_SharedMemoryOpen(shmemname, OSAL_EN_READWRITE);
            if(shmemhandle == OSAL_ERROR)
            {
                s32Result = (tS32) OSAL_u32ErrorCode();
            }
        }

        if(s32Result == OSAL_E_NOERROR)
        {
            OSAL_s32SharedMemoryClose(shmemhandle);

            if(OSAL_s32SemaphoreCreate(semname, &semhandle, 1) != OSAL_OK)
            {
                if(OSAL_s32SemaphoreOpen(semname, &semhandle) != OSAL_OK)
                {
                    s32Result = (tS32) OSAL_u32ErrorCode();
                }
            }
        }
        if (OSAL_ERROR == OSAL_s32SemaphoreWait(semhandle,
                          OSAL_C_TIMEOUT_FOREVER))
        {
            NORMAL_M_ASSERT_ALWAYS();
          //  return OSAL_u32ErrorCode();
            s32Result = (tS32) OSAL_u32ErrorCode();
        }
        if(s32Result == OSAL_E_NOERROR)
        {
         //   OSAL_s32SemaphoreClose(semhandle);


            OSAL_trThreadAttribute    thrdattr;
            tChar                     thrdname[] = ATAPI_IF_THRD_NAME;
            thrdname[sizeof(ATAPI_IF_THRD_NAME) - 2] = '0' + (tChar)driveidx;

            thrdattr.szName         = thrdname;
            thrdattr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
            thrdattr.s32StackSize   = 4096;
            thrdattr.pfEntry        = ATAPI_IF_UpdateThread;
            thrdattr.pvArg          = (void *)driveidx;

            if(OSAL_ThreadSpawn((OSAL_trThreadAttribute *)&thrdattr) == OSAL_ERROR)
            {
                s32Result = (tS32) OSAL_u32ErrorCode();
            }
            if (OSAL_ERROR == OSAL_s32SemaphoreWait(semhandle,
                              OSAL_C_TIMEOUT_FOREVER))
            {
                NORMAL_M_ASSERT_ALWAYS();
                return OSAL_u32ErrorCode();
            }
            if (OSAL_ERROR == OSAL_s32SemaphorePost(semhandle))
            {
                NORMAL_M_ASSERT_ALWAYS();
                //return OSAL_u32ErrorCode();
                s32Result = (tS32) OSAL_u32ErrorCode();
            }
            OSAL_s32SemaphoreClose(semhandle);
        }
    }

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_s32Init, s32Result, 0, 0, 0, 0);

    return s32Result;
}

/******************************************************************************
 *FUNCTION      :ATAPI_IF_s32Destroy
 *
 *DESCRIPTION   :Deinitialize ATAPI_IF
 *
 *PARAMETER     :void
 *
 *RETURNVALUE   :s32Result: OSAL_E_NOERROR
 *
 *HISTORY:      :Ported from Gen2 by sgo1cob 02/04/2014
 *****************************************************************************/

tS32 ATAPI_IF_s32Destroy(void)
{
    tU32                iDriveIndex;
//    tChar               semname[] = ATAPI_IF_SEM_NAME;
    tChar               shmemname[] = ATAPI_IF_SHMEM_NAME;
  //  OSAL_tSemHandle     semhandle;
    OSAL_tShMemHandle   shmemhandle;
    ATAPI_IF_PROC_INFO  *procinfo;

    ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_s32Destroy, 0, 0, 0, 0);

    for( iDriveIndex = 0; iDriveIndex < ATAPI_IF_DRIVE_NUMBER; ++iDriveIndex )
    {
        semname[sizeof(ATAPI_IF_SEM_NAME) - 2] = '0' + (tChar)iDriveIndex;
        shmemname[sizeof(ATAPI_IF_SHMEM_NAME) - 2] = '0' + (tChar)iDriveIndex;

        shmemhandle = OSAL_SharedMemoryOpen(shmemname, OSAL_EN_READWRITE);
        if(shmemhandle != OSAL_ERROR)
        {
            if(OSAL_s32SemaphoreOpen(semname, &semhandle) != OSAL_OK)
            {
                procinfo = (ATAPI_IF_PROC_INFO *)OSAL_pvSharedMemoryMap(shmemhandle, OSAL_EN_READWRITE, sizeof(ATAPI_IF_PROC_INFO), 0);
                if(procinfo != NULL)
                {
//Geht nicht! Debuggen!
                    ATAPI_IF_RequestStreamStatusChange(procinfo, semhandle, ATAPI_IF_STRM_STATUS_SHUTDOWN, 0, 0);
                    OSAL_s32SharedMemoryUnmap(procinfo, sizeof(ATAPI_IF_PROC_INFO));
                }

                OSAL_s32SemaphoreClose(semhandle);
            }
            OSAL_s32SharedMemoryClose(shmemhandle);
        }
    }

    for( iDriveIndex = 0; iDriveIndex < ATAPI_IF_DRIVE_NUMBER; ++iDriveIndex )
    {
        semname[sizeof(ATAPI_IF_SEM_NAME) - 2] = '0' + (tChar)iDriveIndex;
        (void)OSAL_s32SemaphoreDelete(semname);
        shmemname[sizeof(ATAPI_IF_SHMEM_NAME) - 2] = '0' + (tChar)iDriveIndex;
        (void)OSAL_s32SharedMemoryDelete(shmemname);
    }

    ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_s32Destroy, OSAL_E_NOERROR, 0, 0, 0, 0);

    return OSAL_E_NOERROR;
}



extern sem_t *initcd_lock;
/* constructor used here to do initialization of driver in case if it is loaded
 * as shared object. constructor priority(101) has chosen to ensure ATAPI
 * interface ready to hadle request from CDCTRL and CDAUDIO request.
 * ATAPI Interface has constructor priority value of 101.Less value of
 * constructor priority will get called before when compared to higher
 * value of constructor priority in case if more than a constructor.
 */

void __attribute__ ((constructor(101))) od_process_atapi_if_attach(void)
{
   tBool bFirstAttach = FALSE;
   TraceIOString("od_process_atapi_if_attach starts\n");
   /* semaphore is used to avoid race condition in case of multiple process
    * trying to access CDCTRL or CDAUDIO at same time(only for first
    * accessing time )
    */
   initcd_lock = sem_open("CD_INIT",O_EXCL | O_CREAT, 0660, 0);
   if(initcd_lock != SEM_FAILED)
   {
      TraceIOString("sem_open success\n");

      bFirstAttach = TRUE;
      if(ATAPI_IF_s32Init() != (tS32)OSAL_E_NOERROR)
      {
         TraceIOString("OSAL IO Error: ATAPI Init failed\n");

      }
   }
   else
   {
      TraceIOString("sem_open failed,other process may be "
            "done CD init already or in-progress\n");

      initcd_lock = sem_open("CD_INIT", 0);
      bFirstAttach = FALSE;
      sem_wait(initcd_lock);
      sem_post(initcd_lock);

   }
   if(bFirstAttach)
   {
   }
   else
   {
   }

   TraceIOString("od_process_atapi_if_attach end\n");

}






#ifdef __cplusplus
}
#endif

