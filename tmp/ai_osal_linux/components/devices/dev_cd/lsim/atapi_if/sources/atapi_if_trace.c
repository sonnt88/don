 /************************************************************************
 | $Revision: 1.0 $
 | $Date: 18/04/2014 $
 |************************************************************************
 | FILE:         atapi_if_trace.c
 | PROJECT:      GEN3 - LSIM
 | SW-COMPONENT: CD CTRL and CD AUDIO
 |------------------------------------------------------------------------
 | DESCRIPTION:
 |  This file contains implementation of trace API used by ATAPI IF 
 |------------------------------------------------------------------------
 | Date           | Modification                      | Author
 | 18/04/2014     | This file is ported from Gen2     | sgo1cob
 |************************************************************************
 |************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/* Interface for variable number of arguments */
#include "Linux_osal.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
#ifdef ATAPI_IF_IS_USING_LOCAL_TRACE
#include "../../userland/platform/trace/trace/include/trace_interface.h"	// FIXME: Path!
#endif // ATAPI_IF_IS_USING_LOCAL_TRACE

#include "atapi_if_trace.h"
#include "atapi_if.h"
#include "trace_interface.h"
#ifdef __cplusplus
extern "C" {
#endif


/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define ATAPI_IF_IS_USING_LOCAL_TRACE
#define ATAPI_IF_PRINTF_BUFFER_SIZE 256
#define ATAPI_IF_TRACE_BUFFER_SIZE  256
#define ATAPI_IF_GET_U32_LE(PU8) (((tU32)(PU8)[0]) | (((tU32)(PU8)[1])<<8)\
                          | (((tU32)(PU8)[2])<<16) | (((tU32)(PU8)[3])<<24) )



/************************************************************************
|typedefs (scope: module-local)
|----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
/*trace speed up - if FALSE, calling of tracing functions are suppressed*/
/*is set automatically to TRUE, if trace level >= User1 is enabled at startup*/
tBool ATAPI_IF_bTraceOn = TRUE;//FALSE;


//extern BOOL UTIL_trace_isActive(TraceData* data);
/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static volatile tBool          gTraceCmdThreadIsRunning = FALSE;
static tU8 gu8TraceBuffer[ATAPI_IF_TRACE_BUFFER_SIZE]; /*not sychronyzed!*/
static OSAL_tIODescriptor ghDevice = OSAL_ERROR;                            


/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    coszGetErrorText
*
* DESCRIPTION: get pointer to osal error text
*
*
*****************************************************************************/
static const char* pGetErrTxt()
{
   return (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode());
}



/*****************************************************************************
*
* FUNCTION:
*     getArgList
*
* DESCRIPTION:
*     This stupid function is used for satifying L I N T
*     
*     
* PARAMETERS: *va_list pointer to a va_list
*
* RETURNVALUE:
*     va_list
*     
*
*
*****************************************************************************/
static va_list getArgList(va_list* a)
{
	(void)a;
	return *a;
}

/********************************************************************/ /**
*  FUNCTION:      uiCGET
*
*  @brief         return time in ms
*
*  @return        time in ms
*
*  HISTORY:
*
************************************************************************/
static unsigned int uiCGET(void)
{
    return (unsigned int)OSAL_ClockGetElapsedTime();
}
/********************************************************************/ /**
 *  FUNCTION:      ATAPI_IF_vTraceEnter
 *
 *  @brief         enter function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/

tVoid ATAPI_IF_vTraceEnter(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pFunction,
                         tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
        tUInt uiIndex;
        tU8  au8Buf[4 * sizeof(tU32) + 7 * sizeof(tU32) + 32]; //stack?
        OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
        tU32 u32Time = (tU32)uiCGET();

        uiIndex = 0;
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) ATAPI_IF_TRACE_ENTER);
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
        uiIndex += sizeof(tU32);
        strncpy(au8Buf + uiIndex, pFunction, sizeof(au8Buf) - uiIndex - 1);
        au8Buf[sizeof(au8Buf) - 1] = 0;

        // trace the stuff
        LLD_vTrace(u32Class,
                   (tS32)enTraceLevel,
                   au8Buf,
                   (tU32)uiIndex + 32);
    }
}




/********************************************************************/ /**
 *  FUNCTION:      ATAPI_IF_vTraceLeave
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid ATAPI_IF_vTraceLeave(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pFunction,
                         tU32 u32OSALResult,
                         tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
        tUInt uiIndex;
        tU8  au8Buf[4 * sizeof(tU8) + 8 * sizeof(tU32) + 32]; //stack?
        OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
        tU32 u32Time = (tU32)uiCGET();
        tU32 u32FunctionOffset;

        uiIndex = 0;
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) ATAPI_IF_TRACE_LEAVE);
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32OSALResult);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
        uiIndex += sizeof(tU32);
        strncpy(au8Buf + uiIndex, pFunction, sizeof(au8Buf) - uiIndex - 1);
        au8Buf[sizeof(au8Buf) - 1] = 0;

        // trace the stuff
        // be aware index is not greater than aray size!
        // I could not test it while runtime,
        // because LINT throws a senseless warning.
        LLD_vTrace(u32Class,
                   (tS32)enTraceLevel,
                   au8Buf,
                   (tU32)uiIndex + 32);
    }
}


/*****************************************************************************
*
* FUNCTION:
*     ATAPI_IF_vTracePrintf
*
* DESCRIPTION:
*     This function creates the printf-style trace message
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
*
*****************************************************************************/
tVoid ATAPI_IF_vTracePrintf(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
						  tBool bForced,
                          tU32 u32Line,
                          const char* coszFormat,...)
{
   if((TRUE == bForced)
	  ||
	  (LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE))
   {
      static tU8  au8Buf[ATAPI_IF_PRINTF_BUFFER_SIZE + 1];  //TODO: ? size ?
      tUInt uiIndex;
      long lSize;
      size_t tMaxSize;
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time     = (tU32)uiCGET();
	  va_list argList;


	  uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex],
				   (tU32) (enTraceLevel == TR_LEVEL_ERRORS
						    ?
						  ATAPI_IF_TRACE_PRINTF_ERROR : ATAPI_IF_TRACE_PRINTF));
      uiIndex += sizeof(tU8);
	  OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);
	  uiIndex += sizeof(tU8);
	  OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);
	  uiIndex += sizeof(tU8);
	  OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);
	  uiIndex += sizeof(tU8);
       //next pointer is fix set to 32 bit -be happy with 64 bit systems
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      
	  argList = getArgList(&argList);
	  va_start(argList, coszFormat); /*lint !e718 */
      tMaxSize = ATAPI_IF_PRINTF_BUFFER_SIZE - uiIndex;
      lSize = vsnprintf((char*)&au8Buf[uiIndex], tMaxSize, coszFormat, argList);
      uiIndex += (tUInt)lSize;
      va_end(argList);
      
      // trace the stuff 
      LLD_vTrace(u32Class,
                 (tS32)enTraceLevel,
                 au8Buf, 
                 (tU32)uiIndex);
   }
   
}


/********************************************************************/ /**
 *  FUNCTION:      ATAPI_IF_vTraceIoctrl
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid ATAPI_IF_vTraceIoctrl(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
                          tU32 u32Line,
                          ATAPI_IF_enumIoctrlTraceID enIoctrlTraceID,
                          tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
						  tU32 u32Par4)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
       tUInt uiIndex;
       tU8  au8Buf[4 * sizeof(tU8) + 7 * sizeof(tU32)]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();

       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) ATAPI_IF_TRACE_IOCTRL);
       uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enIoctrlTraceID);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
       uiIndex += sizeof(tU32);

       // trace the stuff 
       // be aware index is not greater than aray size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex);
    }
}

/********************************************************************/ /**
 *  FUNCTION:      ATAPI_IF_vTraceIoctrlTxt
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid ATAPI_IF_vTraceIoctrlTxt(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
                          tU32 u32Line,
                          ATAPI_IF_enumIoctrlTraceID enIoctrlTraceID,
                          tU32 u32Par1, tU32 u32Par2, const char *pcszTxt)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
	   size_t  iSize, iMaxSize;
       tUInt uiIndex;
       tU8  au8Buf[ATAPI_IF_TRACE_BUFFER_SIZE]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();

       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) ATAPI_IF_TRACE_IOCTRL);
       uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enIoctrlTraceID);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
       uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
	   uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
	   uiIndex += sizeof(tU32);
	   iSize = strlen(pcszTxt)+1;
	   iMaxSize = ATAPI_IF_TRACE_BUFFER_SIZE - (uiIndex + 1);
       iSize = (iSize < iMaxSize) ? iSize : iMaxSize;
       memcpy(&au8Buf[uiIndex], pcszTxt, iSize);
       uiIndex += iSize;

       // trace the stuff 
       // be aware index is not greater than aray size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex);
    }
}


/********************************************************************/ /**
 *  FUNCTION:      ATAPI_IF_vTraceIoctrlResult
 *
 *  @brief         error text with IOCtrl-name
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid ATAPI_IF_vTraceIoctrlResult(tU32 u32Class,
                               TR_tenTraceLevel enTraceLevel,
                               tU32 u32Line,
                               tS32 s32OsalIOCtrl,
                               const char *pcszErrorTxt)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
       tUInt uiIndex;
       tU8  au8Buf[4 * sizeof(tU8) + 5 * sizeof(tU32) + 202]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();

       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) ATAPI_IF_TRACE_IOCTRL_RESULT);
       uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 11);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
	   uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Time);
	   uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)s32OsalIOCtrl);
	   uiIndex += sizeof(tU32);
	   strncpy((char*)&au8Buf[uiIndex], (const char*)pcszErrorTxt, 200);
       uiIndex += 200;
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32)0);
	   uiIndex += sizeof(tU8);

       // trace the stuff 
       // be aware index is not greater than array size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex);
    }
}




/*****************************************************************************
* FUNCTION:     bIsOpen
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  checks, if device is open

* HISTORY:
******************************************************************************/
static tBool bIsOpen()
{
	tBool bOpen;
	if(OSAL_ERROR != ghDevice)
	{
		bOpen = TRUE;
	}
	else //if(OSAL_ERROR != ghDevice)
	{
		bOpen = FALSE;
		ATAPI_IF_PRINTF_ERRORS("device not open");
	} //else //if(OSAL_ERROR != ghDevice)

	return bOpen;
}

/*****************************************************************************
* FUNCTION:     bIOCtrlOK
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  checks, if ioctrl succeeded

* HISTORY:
******************************************************************************/
static tBool bIOCtrlOK(tS32 s32Fun, tS32 s32Ret)
{
	tBool bOK = (s32Ret != OSAL_ERROR);
	ATAPI_IF_PRINTF_IOCTRL_RESULT(s32Fun, bOK ? "SUCCESS" : pGetErrTxt());
	return bOK;
}


/*****************************************************************************
* FUNCTION:     vPlayInfoNotify
* PARAMETER:    pvCookie
* RETURNVALUE:  None
* DESCRIPTION:  This is a PlayInfo-Callback

* HISTORY:
******************************************************************************/
static tVoid vPlayInfoNotify(tPVoid pvCookie, const OSAL_trPlayInfo* prInfo)
{
 (void)pvCookie; // L I N T 

 ATAPI_IF_PRINTF_FORCED("vPlayInfoNotify:"
					   " Track [%02u],"
					   " AbsMSF [%02u:%02u:%02u],"
					   " RelMSF [%02u:%02u:%02u],"
					   " PlayStatus [0x%08X]",
					   (unsigned int)prInfo->u32TrackNumber,
					   (unsigned int)prInfo->rAbsTrackAdr.u8MSFMinute,
					   (unsigned int)prInfo->rAbsTrackAdr.u8MSFSecond,
					   (unsigned int)prInfo->rAbsTrackAdr.u8MSFFrame,
					   (unsigned int)prInfo->rRelTrackAdr.u8MSFMinute,
					   (unsigned int)prInfo->rRelTrackAdr.u8MSFSecond,
					   (unsigned int)prInfo->rRelTrackAdr.u8MSFFrame,
					   (unsigned int)prInfo->u32StatusPlay
					  );


 //BPCD_OEDT_T2_s32PlayTimeSeconds =
 // (tS32)(prInfo->rAbsTrackAdr.u8MSFMinute) * 60
 //  + (tS32)(prInfo->rAbsTrackAdr.u8MSFSecond);
 /*if(0 == BPCD_OEDT_T2_s32FirstPlayTimeSeconds)
 {
	 BPCD_OEDT_T2_s32FirstPlayTimeSeconds = BPCD_OEDT_T2_s32PlayTimeSeconds;
 }*/

 //BPCD_OEDT_TRACE(BPCD_OEDT_T2_s32PlayTimeSeconds);
}



/*****************************************************************************
* FUNCTION:     vTraceCmdThread
* PARAMETER:    pvData
* RETURNVALUE:  None
* DESCRIPTION:  This is a Trace Thread handler.

* HISTORY:
******************************************************************************/
static tVoid vTraceCmdThread(tPVoid pvData)
{
	(void)pvData;
}




/*****************************************************************************
* FUNCTION:     ATAPI_IF_vTraceCommandCallbackHandler
* PARAMETER:    buffer
* RETURNVALUE:  None
* DESCRIPTION:  This is a Trace callback handler.

* HISTORY:
******************************************************************************/
tVoid ATAPI_IF_vTraceCommandCallbackHandler(const tU8* pcu8Buffer)
{
   ATAPI_IF_TRACE_ENTER_U4(ATAPI_IF_vTraceCommandCallbackHandler,
   (tU32)(pcu8Buffer!=NULL?pcu8Buffer[0]:0xFF),
   (tU32)(pcu8Buffer!=NULL?pcu8Buffer[1]:0xFF),
   (tU32)(pcu8Buffer!=NULL?pcu8Buffer[2]:0xFF),
   (tU32)(pcu8Buffer!=NULL?pcu8Buffer[3]:0xFF)
   );
   if(pcu8Buffer != NULL)
   {
      if(!gTraceCmdThreadIsRunning)
      {
         OSAL_trThreadAttribute  traceCmdThreadAttribute;
         OSAL_tThreadID          traceCmdThreadID; //l i n t  = OSAL_ERROR;
         tU8 u8Len   = pcu8Buffer[0];
         gTraceCmdThreadIsRunning = TRUE;

         (void)OSAL_pvMemoryCopy(gu8TraceBuffer, pcu8Buffer, u8Len + 1);

         traceCmdThreadAttribute.szName       = "CDAUDIOTRACE";
         traceCmdThreadAttribute.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_LOWEST;
         traceCmdThreadAttribute.pfEntry      = vTraceCmdThread;
         traceCmdThreadAttribute.pvArg        = gu8TraceBuffer;
         traceCmdThreadAttribute.s32StackSize = 1024;

         traceCmdThreadID = OSAL_ThreadSpawn(&traceCmdThreadAttribute);
         if(OSAL_ERROR == traceCmdThreadID)
         {
            ATAPI_IF_PRINTF_ERRORS("Create TraceThread");
         } //if(OSAL_ERROR == traceCmdThreadID)
      }
      else //if(!gTraceCmdThreadIsRunning)
      {
         ATAPI_IF_PRINTF_ERRORS("last Trace thread is running yet");
      } //else //if(!gTraceCmdThreadIsRunning)
   } //if(pcu8Buffer != NULL)
   ATAPI_IF_TRACE_LEAVE_U4(ATAPI_IF_vTraceCommandCallbackHandler,OSAL_E_NOERROR,
   0,0,0,0);
}




/*****************************************************************************
* FUNCTION:     ATAPI_IF_vRegTrace
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  register trace command channel
* HISTORY:
******************************************************************************/
#ifdef ATAPI_IF_IS_USING_LOCAL_TRACE
tVoid ATAPI_IF_vRegTrace(tVoid)
{
   tBool bRet;
   bRet = TR_chan_acess_bRegChan(TR_TTFIS_CDCTRL,
                  (TRACE_CALLBACK)ATAPI_IF_vTraceCommandCallbackHandler);
   if(!bRet)
   {
      ATAPI_IF_PRINTF_ERRORS("ATAPI_IF_vRegTrace -"
      " Command Channel 0x%04X not registered",
      (unsigned int)TR_TTFIS_CDCTRL);
   } //if(!bRet)
}
#endif //#ifdef ATAPI_IF_IS_USING_LOCAL_TRACE

/*****************************************************************************
* FUNCTION:     ATAPI_IF_vUnregTrace
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  unregister trace command channel
* HISTORY:
******************************************************************************/
#ifdef ATAPI_IF_IS_USING_LOCAL_TRACE
tVoid ATAPI_IF_vUnregTrace(tVoid)
{
   
   TR_chan_acess_bUnRegChan(TR_TTFIS_CDCTRL,
   (TRACE_CALLBACK)ATAPI_IF_vTraceCommandCallbackHandler);

}
#endif //#ifdef ATAPI_IF_IS_USING_LOCAL_TRACE



#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file 
|-----------------------------------------------------------------------*/


