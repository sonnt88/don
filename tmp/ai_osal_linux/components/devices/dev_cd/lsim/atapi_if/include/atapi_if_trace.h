/************************************************************************
| FILE:         atapi_if_trace.h
| PROJECT:      Gen2
| SW-COMPONENT: atapi_if
|------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    atapi_if_trace.h
 *
 * @brief   This file includes trace stuff for the atapi_if.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 *
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (ATAPI_IF_TRACE_H)
   #define ATAPI_IF_TRACE_H
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/
// if defined, atapi_if registers its own trace command channel
//#define ATAPI_IF_IS_USING_LOCAL_TRACE
#define ATAPI_IF_TRACE_ENABLED


/*trace classes used by atapi_if, defined in osalcore/include/ostrace.h*/
#define ATAPI_IF_TRACE_CLASS    OSAL_C_TR_CLASS_LLD_ATAPICDVD

#ifdef ATAPI_IF_TRACE_ENABLED

extern tBool    ATAPI_IF_bTraceOn;

#define ATAPI_IF_TRACE_ENTER_U2(FUNC,P1,P2,P3,P4) if(ATAPI_IF_bTraceOn)\
		ATAPI_IF_vTraceEnter(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_2,\
                   __LINE__, #FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define ATAPI_IF_TRACE_ENTER_U3(FUNC,P1,P2,P3,P4) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTraceEnter(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_3,\
                   __LINE__, #FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define ATAPI_IF_TRACE_ENTER_U4(FUNC,P1,P2,P3,P4) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTraceEnter(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_4,\
                   __LINE__, #FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define ATAPI_IF_TRACE_LEAVE_U2(FUNC,ERROR,P1,P2,P3,P4) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTraceLeave(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_2,\
                   __LINE__, #FUNC, (ERROR), (tU32)(P1),\
				   (tU32)(P2), (tU32)(P3), (tU32)(P4))

#define ATAPI_IF_TRACE_LEAVE_U3(FUNC,ERROR,P1,P2,P3,P4) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTraceLeave(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_3,\
                   __LINE__, #FUNC, (ERROR), (tU32)(P1),\
				   (tU32)(P2), (tU32)(P3), (tU32)(P4))

#define ATAPI_IF_TRACE_LEAVE_U4(FUNC,ERROR,P1,P2,P3,P4) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTraceLeave(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_4,\
                   __LINE__, #FUNC, (ERROR), (tU32)(P1),\
				   (tU32)(P2),  (tU32)(P3),  (tU32)(P4))


#define ATAPI_IF_TRACE_IOCTRL(IOCTRL,P1,P2,P3,P4) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTraceIoctrl(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_1,\
                   __LINE__, (IOCTRL), (tU32)(P1), (tU32)(P2),\
				   (tU32)(P3), (tU32)(P4))

#define ATAPI_IF_TRACE_IOCTRL_TXT(IOCTRL,P1,P2,PTXT) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTraceIoctrlTxt(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_1,\
                   __LINE__, (IOCTRL), (tU32)(P1), (tU32)(P2), (PTXT))

#define ATAPI_IF_PRINTF_IOCTRL_RESULT(OSALIOCTRL,OSALERRORTXT) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTraceIoctrlResult(ATAPI_IF_TRACE_CLASS, TR_LEVEL_ERRORS,\
                     __LINE__, (OSALIOCTRL), (OSALERRORTXT))


#define ATAPI_IF_PRINTF_FORCED(...)\
		ATAPI_IF_vTracePrintf(ATAPI_IF_TRACE_CLASS, TR_LEVEL_FATAL, TRUE,\
                     __LINE__, __VA_ARGS__)

#define ATAPI_IF_PRINTF_FATAL(...)\
		ATAPI_IF_vTracePrintf(ATAPI_IF_TRACE_CLASS, TR_LEVEL_FATAL, FALSE,\
                      __LINE__, __VA_ARGS__)

#define ATAPI_IF_PRINTF_ERRORS(...)\
		ATAPI_IF_vTracePrintf(ATAPI_IF_TRACE_CLASS, TR_LEVEL_ERRORS, FALSE,\
                      __LINE__, __VA_ARGS__)

#define ATAPI_IF_PRINTF_U1(...) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTracePrintf(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_1, FALSE,\
                      __LINE__, __VA_ARGS__)

#define ATAPI_IF_PRINTF_U2(...) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTracePrintf(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_2, FALSE,\
                      __LINE__, __VA_ARGS__)

#define ATAPI_IF_PRINTF_U3(...) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTracePrintf(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_3, FALSE,\
                      __LINE__, __VA_ARGS__)

#define ATAPI_IF_PRINTF_U4(...) if(ATAPI_IF_bTraceOn) \
		ATAPI_IF_vTracePrintf(ATAPI_IF_TRACE_CLASS, TR_LEVEL_USER_4, FALSE,\
                      __LINE__, __VA_ARGS__)

#else //#ifdef ATAPI_IF_TRACE_ENABLED
	#define ATAPI_IF_TRACE_ENTER_U2(FUNC,P1,P2,P3,P4)
	#define ATAPI_IF_TRACE_ENTER_U3(FUNC,P1,P2,P3,P4)
	#define ATAPI_IF_TRACE_ENTER_U4(FUNC,P1,P2,P3,P4)
	#define ATAPI_IF_TRACE_LEAVE_U2(FUNC,ERROR,P1,P2,P3,P4)
	#define ATAPI_IF_TRACE_LEAVE_U3(FUNC,ERROR,P1,P2,P3,P4)
	#define ATAPI_IF_TRACE_LEAVE_U4(FUNC,ERROR,P1,P2,P3,P4)
	#define ATAPI_IF_TRACE_IOCTRL(IOCTRL,P1,P2,P3,P4)
	#define ATAPI_IF_TRACE_IOCTRL_TXT(IOCTRL,P1,P2,PTXT)
	#define ATAPI_IF_PRINTF_IOCTRL_RESULT(OSALIOCTRL,OSALERRORTXT)
	#define ATAPI_IF_PRINTF_FORCED(...)
	#define ATAPI_IF_PRINTF_FATAL(...)
	#define ATAPI_IF_PRINTF_ERRORS(...)
	#define ATAPI_IF_PRINTF_U1(...)
	#define ATAPI_IF_PRINTF_U2(...)
	#define ATAPI_IF_PRINTF_U3(...)
	#define ATAPI_IF_PRINTF_U4(...)
#endif //#ifdef ATAPI_IF_TRACE_ENABLED

/** trace messages */
/** trace message IDs */
enum ATAPI_IF_enTraceMessages
{
    ATAPI_IF_TRACE_ENTER = 1,
    ATAPI_IF_TRACE_LEAVE,
    ATAPI_IF_TRACE_PRINTF,
    ATAPI_IF_TRACE_PRINTF_ERROR,
    ATAPI_IF_TRACE_IOCTRL,
    ATAPI_IF_TRACE_IOCTRL_RESULT
};

/* Traces - these are not names of IOCTRLs!*/
typedef enum
{
    ATAPI_IF_EN_IOCONTROL,
    ATAPI_IF_EN_IOCONTROL_RESULT,
    ATAPI_IF_EN_SETPLAYRANGE,
    ATAPI_IF_EN_SETMSF,
    ATAPI_IF_EN_GETCDINFO,
    ATAPI_IF_EN_GETPLAYINFO, //5
    ATAPI_IF_EN_GETTRACKINFO,
    ATAPI_IF_EN_GETALBUMNAME,
    ATAPI_IF_EN_GETTRACKCDINFO_ARTIST,
    ATAPI_IF_EN_GETTRACKCDINFO_TITLE,
    ATAPI_IF_EN_GETADDITIONALCDINFO_INFO, //10
    ATAPI_IF_EN_GETADDITIONALCDINFO_TRACKS,
    ATAPI_IF_EN_REGPLAYNOTIFY,
    ATAPI_IF_EN_UNREGPLAYNOTIFY
}ATAPI_IF_enumIoctrlTraceID;

/** trace command messages */
enum ATAPI_IF_enTraceCommandMessages
{
    ATAPI_IF_TRACE_CMD_TRACE_ON           = 0x01,
    ATAPI_IF_TRACE_CMD_TRACE_OFF          = 0x02,
    ATAPI_IF_TRACE_CMD_OPEN               = 0x03,
    ATAPI_IF_TRACE_CMD_CLOSE              = 0x04,
	//ATAPI_IF_TRACE_CMD_IOCTRL = 0x03,
    ATAPI_IF_TRACE_CMD_PLAY               = 0x10,
    ATAPI_IF_TRACE_CMD_STOP               = 0x11,
    ATAPI_IF_TRACE_CMD_PAUSE              = 0x12,
    ATAPI_IF_TRACE_CMD_RESUME             = 0x13,
    ATAPI_IF_TRACE_CMD_FASTFORWARD        = 0x14,
    ATAPI_IF_TRACE_CMD_FASTBACKWARD       = 0x15,
    ATAPI_IF_TRACE_CMD_SETPLAYRANGE       = 0x16,
    ATAPI_IF_TRACE_CMD_SETMSF             = 0x17,
    ATAPI_IF_TRACE_CMD_GETCDINFO          = 0x18,
    ATAPI_IF_TRACE_CMD_GETPLAYINFO        = 0x19,
    ATAPI_IF_TRACE_CMD_GETTRACKINFO       = 0x1A,
    ATAPI_IF_TRACE_CMD_GETALBUMNAME       = 0x1B,
    ATAPI_IF_TRACE_CMD_GETTRACKCDINFO      = 0x1C,
    ATAPI_IF_TRACE_CMD_GETADDITIONALCDINFO = 0x1D,
    ATAPI_IF_TRACE_CMD_REGPLAYNOTIFY       = 0x1E,
	ATAPI_IF_TRACE_CMD_UNREGPLAYNOTIFY     = 0x1F,

	ATAPI_IF_TRACE_CMD_TEST_CDTEXT         = 0x30
};



/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/                     

#ifdef ATAPI_IF_IS_USING_LOCAL_TRACE
tVoid ATAPI_IF_vRegTrace(tVoid);
tVoid ATAPI_IF_vUnregTrace(tVoid);
#endif //#ifdef ATAPI_IF_IS_USING_LOCAL_TRACE


tVoid ATAPI_IF_vTraceEnter(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pFunction,
                         tU32 u32Par1,
                         tU32 u32Par2,
                         tU32 u32Par3,
                         tU32 u32Par4);

tVoid ATAPI_IF_vTraceLeave(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pFunction,
                         tU32 u32OSALError,
                         tU32 u32Par1,
                         tU32 u32Par2,
                         tU32 u32Par3,
                         tU32 u32Par4);


tVoid ATAPI_IF_vTraceIoctrl(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         ATAPI_IF_enumIoctrlTraceID enIoctrlTraceID,
                         tU32 u32Par1,
                         tU32 u32Par2,
                         tU32 u32Par3,
                         tU32 u32Par4);

tVoid ATAPI_IF_vTraceIoctrlTxt(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         ATAPI_IF_enumIoctrlTraceID enIoctrlTraceID,
						 tU32 u32Par1,
						 tU32 u32Par2,
                         const char * pcszTxt);

tVoid ATAPI_IF_vTraceIoctrlResult(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         tS32 s32IoCtrl,
                         const char* pcszErrorTxt);


tVoid ATAPI_IF_vTracePrintf(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
						  tBool bForced,
                          tU32 u32Line,
                          const char* coszFormat,...);



#ifdef __cplusplus
}
#endif
     
#else
#error atapi_if_trace.h included several times
#endif                

