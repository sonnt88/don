/************************************************************************
 | $Revision: 1.0 $
 | $Date: 02/04/2013 $
 |************************************************************************
 | FILE:         atapi_if.h
 | PROJECT:      GEN3
 | SW-COMPONENT:
 |------------------------------------------------------------------------
 | DESCRIPTION:
 |  This file contains various macro,structure and enum declaration used
 |  by ATAPI_IF and other dependent modules
 |------------------------------------------------------------------------
 | Date      		| Modification                        | Author
 | 02/04/2013       | This file is ported from Gen2       | sgo1cob
 | 07/07/2016       | Fix for CMG3GB-3460				  | boc7kor
 |                  | Function declarations for:          |
 |					|	ATAPI_IF_RequestData              |  
 |					|	ATAPI_IF_RequestStreamStatusChange| 
 |					|	ATAPI_IF_RegisterPlayNotifier     |
 |					|	ATAPI_IF_UnregisterPlayNotifier   |
 |					| are added                           |
 |************************************************************************
 |************************************************************************/
#ifndef ATAPI_IF_H
#define ATAPI_IF_H

#ifdef __cplusplus
extern "C" {
#endif

/* ************************************************************************/
/*  defines (scope: global)                                               */
/* ************************************************************************/
#define ATAPI_IF_LINUX_DEVICE_NAME_STRING   "/dev/cdrom"
#define ATAPI_IF_LINUX_DEVICE_NAME_STRING2  "/dev/sr0"
#define ATAPI_IF_LINUX_DEVICE_NAME_STRING3  "/dev/scd0"
#define ATAPI_IF_SEM_NAME                   "ATAPIIFSemx"
#define ATAPI_IF_SHMEM_NAME                 "ATAPIIFShMemx"
#define ATAPI_IF_DRIVE_NUMBER               1
#define ATAPI_IF_MAX_NOTIFIERS              5

#define ATAPI_IF_ADDITIONALCDINFO_IS_DATA_TRACK(_U32ARRAY_, _TRACKINDEX_)   (0 != ((_U32ARRAY_)[(_TRACKINDEX_) / 32] & (1UL << ((_TRACKINDEX_) & 0x1FUL))))
#define ATAPI_IF_ADDITIONALCDINFO_IS_DATA_TRACK_NUMBER_1(_U32ARRAY_)        (0 != ((_U32ARRAY_)[0] & 0x02UL))
#define ATAPI_IF_ADDITIONALCDINFO_SET_DATA_TRACK(_U32ARRAY_, _TRACKINDEX_)  ((_U32ARRAY_)[(_TRACKINDEX_) / 32] |=  (1UL << ((_TRACKINDEX_) & 0x1FUL)))
#define ATAPI_IF_ADDITIONALCDINFO_CLR_DATA_TRACK(_U32ARRAY_, _TRACKINDEX_)  ((_U32ARRAY_)[(_TRACKINDEX_) / 32] &= ~(1UL << ((_TRACKINDEX_) & 0x1FUL)))

#define ATAPI_IF_ADDITIONALCDINFO_NULL_VALUE                0x00UL
#define ATAPI_IF_ADDITIONALCDINFO_FIRST_TRACK_DATA_BITMASK  0x00000001UL

#define ATAPI_IF_MAXTRACKS      99
#define ATAPI_IF_LEADOUT_INDEX  ATAPI_IF_MAXTRACKS

#define ATAPI_IF_CDFRAME_BYTES              2352                        // Size of a CD's frame (sector) in bytes
#define ATAPI_IF_DATA_REQUEST_BUFF_CDFRAMES 5                           // Size of buffer in CD's frames (sectors)
#define ATAPI_IF_DATA_REQUEST_BUFF_SIZE     (ATAPI_IF_DATA_REQUEST_BUFF_CDFRAMES*ATAPI_IF_CDFRAME_BYTES)  // Size of buffer for read requests

#define ATAPI_IF_LBA_USE_CURRENT            0x80000000      // Bitflag, used in play range to set it to the current position
#define ATAPI_IF_LBA_USE_END_OF_TRACK       0x40000000      // Bitflag, used in play range to set it to the end of a track
#define ATAPI_IF_LBA_USE_END_OF_TRACK_MASK  0x3fffffff      // Bitmask, used to remove ATAPI_IF_LBA_USE_END_OF_TRACK flag

#define MEDIA_STATE_NOTIFY

/* *********************************************************************** */
/*  typedefs enum (scope: global)                                          */
/* *********************************************************************** */
typedef enum
{   // Stati of the streaming (e.g. audio playback) thread
    ATAPI_IF_STRM_STATUS_NO_DISK,       // No disk in drive
    ATAPI_IF_STRM_STATUS_STOP,          // Playback stopped
    ATAPI_IF_STRM_STATUS_PLAY,          // Playback in progress
    ATAPI_IF_STRM_STATUS_PAUSE,         // Playback paused (can be continued by requesting ATAPI_IF_STRM_REQUEST_RESUME)
    ATAPI_IF_STRM_STATUS_FAST_FORWARD,  // Play ATAPI_IF_SAMPLE_BUFF_CDFRAMES sectors, skip ATAPI_IF_SCAN_SECTORS_TO_SKIP sectors, and repeat
    ATAPI_IF_STRM_STATUS_FAST_BACKWARD, // Play ATAPI_IF_SAMPLE_BUFF_CDFRAMES sectors, rewind ATAPI_IF_SCAN_SECTORS_TO_SKIP sectors, and repeat
    ATAPI_IF_STRM_STATUS_SHUTDOWN,      // Set by ATAPI_IF_s32Destroy to notify ATAPI_IF_UpdateThread that is expected to end
    ATAPI_IF_STRM_REQUEST_PLAY_RANGE,   // Set range to play for play commands
    ATAPI_IF_STRM_REQUEST_RESUME,       // Resume playback, if the current status is ATAPI_IF_STRM_STATUS_PAUSE
} ATAPI_IF_STREAM_STATUS;

/* *********************************************************************** */
/*  typedefs enum (scope: global)                                          */
/* *********************************************************************** */
typedef enum
{   // Status of the loader
   AIF_LDR_STATUS_UNKNOWN=0,
   AIF_LDR_STATUS_NO_DISK, //1
   AIF_LDR_STATUS_INSERT_IN_PROG, //2
   AIF_LDR_STATUS_LOAD_ERROR, //3
   AIF_LDR_STATUS_CD_INSIDE, //4
   AIF_LDR_STATUS_CD_PLAYABLE, //5
   AIF_LDR_STATUS_CD_UNREADABLE, //6
   AIF_LDR_STATUS_EJECT_IN_PROGRESS, //7
   AIF_LDR_STATUS_EJECT_ERROR,//8
   AIF_LDR_STATUS_IN_SLOT,//9
   AIF_LDR_STATUS_EJECTED,//10
   AIF_LDR_STATUS_NOT_INITIALIZED=0xFE,
   AIF_LDR_STATUS_INITIALIZED=0xFF

} ATAPI_IF_LOADER_STATUS;

/*Get Event / Status Notification - mmc3r10g.pdf: table 94*/
#define AIF_MEDIA_EVENT_NOCHG         0x00
#define AIF_MEDIA_EVENT_EJECT_REQ     0x01
#define AIF_MEDIA_EVENT_NEW_MEDIA     0x02
#define AIF_MEDIA_EVENT_MEDIA_REMOVAL 0x03
#define AIF_MEDIA_EVENT_MEDIA_CHANGE  0x04

/* *********************************************************************** */
/*  typedefs struct (scope: global)                                        */
/* *********************************************************************** */
typedef struct ATAPI_IF_PLAY_CALLBACK_ARG
{   // Structure sent to ATAPI_IF_Callback to notify registered clients about events
    ATAPI_IF_STREAM_STATUS  Status;         // Playback status when the notification was triggered
    tU32                    Trck;           // Track that was playing when the notification was triggered
    tU32                    RelLBA;         // Offset from start of the track when the notification was triggered
    tU32                    AbsLBA;         // Absolute logical address when the notification was triggered
    void                    *UserData;      // Pointer to user data, the client passed to OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY
    OSAL_tPlayInfoCallback  UserCallback;   // Pointer to callback function, the client passed to OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY
} ATAPI_IF_PLAY_CALLBACK_ARG;
typedef struct
{
  tU32                    u32MediaType;
  tU16                    u16NotificationType;
  OSALCALLBACKFUNC        UserCallback;
} ATAPI_IF_MEDIA_CALLBACK_ARG;

typedef struct
{   // Structure to store informations about registered callbacks
	// ID of Process that registered for notification
    OSAL_tProcessID         ProcID;
    // Address callback function in process' address space
    OSAL_tpfCallback        ATAPI_IF_Callback;
    /* Pointer to user data, the client passed
     * to OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY
     */
    void                    *UserData;
    /* Pointer to callback function, the client
    passed to OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY */
    OSAL_tPlayInfoCallback  UserCallback;
} ATAPI_IF_Play_CALLBACK_REG_INFO;
typedef struct
{   // Structure to store informations about registered callbacks
	// ID of Process that registered for notification
    OSAL_tProcessID         ProcID;
    // Address callback function in process' address space
    OSAL_tpfCallback        ATAPI_IF_Callback;
    tU16                    u16NotificationType;
    tU16                    u16AppID;
    /* Pointer to callback function, the client
    passed to OSAL_C_S32_IOCTRL_REG_NOTIFICATION*/
    OSALCALLBACKFUNC  UserCallback;
} ATAPI_IF_Media_CALLBACK_REG_INFO;


typedef struct
{
  tU8 u8Valid;
  tU8 au8Vendor8[8 + 1];
  tU8 au8Product16[16 + 1];
  tU8 au8Revision4[4 + 1];
  tU8 au8VendorSpecific20[20 + 1];
  tU8 u8SWVersion;
  tU8 u8HWVersion;
} ATAPI_IF_INQUIRY;
typedef struct ATAPI_IF_CDCACHE
{
    tU32    ValidFlag;          // TRUE if data in cache is valid, FALSE if no CD present or unreadable
    tU32    MediaType;          // Type of CD, one of OSAL_C_U8_AUDIO_MEDIA, OSAL_C_U8_DATA_MEDIA, or OSAL_C_U8_UNKNOWN_MEDIA
    tU32    FileSystemType;     // OSAL_C_U8_FS_TYPE_CDFS for OSAL_C_U8_DATA_MEDIA, OSAL_C_U8_FS_TYPE_UNKNOWN for CDDAs
    tU8     MediaCatNum[16];    // Media Catalog Number if available, "\000" otherwise
    tU32    MinTrck, MaxTrck;   // Minimum and maximum number of track on CDDAs
    tU32    TrckStartLBA[ATAPI_IF_MAXTRACKS + 1];   // Logical Block Address for each track
                                                    // End of CDDA is stored as LBA(MaxTrack)+1 as additional track after the entry for MaxTrack and at ATAPI_IF_LEADOUT_INDEX for easier access
                                                    // Entries are based on 0 while tracks on a CD are based on 1, so the start of track N is found at TrckStartLBA[N-1]!
    tU32    PlayTime;           // Total play time of the CD
    tU32    DataTrackInfo[4];   // Bitfield for all tracks. 1 if it is a data track, 0 otherwise
    tU32    CDTextFlag;         // TRUE if CD Text is available, FALSE otherwise
    tU32    CDTextComplete;     // TRUE if CD Text seems to be completely read, FALSE otherwise. See notes in ATAPI_IF_CacheMedia
    tU8     CDTextAlbumName[OSAL_C_S32_CDAUDIO_MAXNAMESIZE];                            // Album Name if available, "\000" otherwise
    tU8     CDTextTrackTitle[ATAPI_IF_MAXTRACKS][OSAL_C_S32_CDAUDIO_MAXNAMESIZE];       // Title for each track if available, "\000" otherwise
    tU8     CDTextTrackPerformer[ATAPI_IF_MAXTRACKS][OSAL_C_S32_CDAUDIO_MAXNAMESIZE];   // Artist for each track if available, "\000" otherwise
} ATAPI_IF_CDCACHE;

typedef struct ATAPI_IF_PLAY_STATUS
{
    tU32                    NewStartLBA;    // Start of next play range as Logical Block Address
    tU32                    NewEndLBA;      // End of next play range as Logical Block Address
    tU32                    StartLBA;       // Start of current play range as Logical Block Address
    tU32                    EndLBA;         // End of current play range as Logical Block Address
    tU32                    CurrentLBA;     // Current position of playback as Logical Block Address
    ATAPI_IF_STREAM_STATUS  Status;         // Current status
} ATAPI_IF_PLAY_STATUS;
#ifdef MEDIA_STATE_NOTIFY
typedef struct
{
   ATAPI_IF_LOADER_STATUS  InternelStatus;         // Current status
}ATAPI_IF_LoaderStates;
typedef struct
{
  //OSAL_C_U16_NOTI_MEDIA_CHANGE
  //- OSAL_C_U16_MEDIA_EJECTED
  //- OSAL_C_U16_INCORRECT_MEDIA
  //- OSAL_C_U16_DATA_MEDIA
  //- OSAL_C_U16_AUDIO_MEDIA
  //- OSAL_C_U16_UNKNOWN_MEDIA
  tU32   u32MediaChange;
  //OSAL_C_U16_NOTI_TOTAL_FAILURE
  //- OSAL_C_U16_DEVICE_OK
  //- OSAL_C_U16_DEVICE_FAIL
  tU32   u32TotalFailure;
  //OSAL_C_U16_NOTI_MEDIA_STATE
  //- OSAL_C_U16_MEDIA_NOT_READY
  //- OSAL_C_U16_MEDIA_READY
  tU32   u32MediaState;
  //OSAL_C_U16_NOTI_DEFECT
  //- OSAL_C_U16_DEFECT_LOAD_EJECT
  //- OSAL_C_U16_DEFECT_LOAD_INSERT
  //- OSAL_C_U16_DEFECT_DISCTOC
  //- OSAL_C_U16_DEFECT_DISC
  //- OSAL_C_U16_DEFECT_READ_ERR
  //- OSAL_C_U16_DEFECT_READ_OK
  tU32   u32Defect;
  //OSAL_C_U16_NOTI_DEVICE_READY
  //- OSAL_C_U16_DEVICE_NOT_READY
  //- OSAL_C_U16_DEVICE_READY
  //- OSAL_C_U16_DEVICE_UV_NOT_READY
  //- OSAL_C_U16_DEVICE_UV_READY
  tU32   u32DeviceReady;
}ATAPI_IF_Drive_Info;

typedef struct
{
	tU8 u8MediaEventCode;
	tU8 u8MediaEventStatusCode;
}ATAPI_IF_Drive_Response;
#endif
typedef struct ATAPI_IF_PROC_INFO
{
    ATAPI_IF_CDCACHE        CDCache;                // Cached informations about current CD
    ATAPI_IF_PLAY_STATUS    PlayStatus;             // Current status of playback, as seen by clients
    ATAPI_IF_PLAY_STATUS    StatusChange;           // Next status to be set
    volatile tU32           StatusChangePending;    // != 0 if the current status has to be changed as defined by StatusChange, == 0 otherwise
    ATAPI_IF_PLAY_STATUS    ChangeRequest;          // Status requested by client
    volatile tU32           ChangeRequestPending;   // != 0 if the client requested the status to be changed, == 0 otherwise

    volatile tU32           DataRequestPending;     // != 0 if the client requests data to be read from CD, == 0 otherwise
    volatile tU32           DataRequestComplete;    // != 0 if requested data was read from CD, == 0 otherwise
    tU32                    DataRequestLBA;         // Logical Block Address to start reading data
    tS32                    DataRequestNumFrames;   // Number of frames to read
    tU8                     DataRequestBuffer[ATAPI_IF_DATA_REQUEST_BUFF_SIZE]; // Buffer to store data

    ATAPI_IF_Play_CALLBACK_REG_INFO  AudioPlayCallbacks[ATAPI_IF_MAX_NOTIFIERS]; // Informations about registered notification callbacks
#ifdef MEDIA_STATE_NOTIFY
    // Informations about registered notification callbacks
    ATAPI_IF_Media_CALLBACK_REG_INFO  arMediaStatusCallbacks[ATAPI_IF_MAX_NOTIFIERS];
    // Real media status
    ATAPI_IF_LoaderStates    MediaStatus;
    //recent states of drive
    ATAPI_IF_Drive_Info     DriveStateInfo;
#endif
    ATAPI_IF_INQUIRY		rCDDeviceInfo;
} ATAPI_IF_PROC_INFO;

typedef struct ATAPI_IF_THRD_INFO
{   // Structure to hold data for ATAPI_IF_UpdateThread
    OSAL_tSemHandle         SemHandle;      // Handle to semaphore to protect access to shared memory
    OSAL_tShMemHandle       ShmHandle;      // Shared memory to communicate with other processes
    tS32                    CDHandle;       // Linux handle for CD device
    void                    *ALSAHandle;    // Handle for ALSA sound library, used for playback
    ATAPI_IF_PROC_INFO      *ProcInfo;      // Structure to hold data, visible to threads outside ATAPI_IF_UpdateThread
    ATAPI_IF_PLAY_STATUS    ThrdPlayStatus; // Real playback status
    ATAPI_IF_Drive_Response     ThrdDriveLastResponse;  //recent states of drive

} ATAPI_IF_THRD_INFO;

/*this semaphore lock is used in constructor of libshared_cd.so initialization
* to avoid race condition in case of multiple process access at same time
*/
sem_t *initcd_lock;


/************************************************************************
 |function prototypes (scope: global)
 |-----------------------------------------------------------------------*/
tU32 ATAPI_IF_u32CacheGetVersion(ATAPI_IF_PROC_INFO * Info,tU8 *pu8SW,
                  tU8 *pu8HW, tU8 *pau8Vendor8,tU8 *pau8Product16);
void ATAPI_IF_UpdateProcStatus(ATAPI_IF_PROC_INFO *Info);
void ATAPI_IF_LBA2MSF(tU32 *Min, tU32 *Sec, tU32 *Frm, tU32 LBA);
void ATAPI_IF_LBA2MSF_OSAL_Addr(OSAL_trMSFAddress *MSFAddr, tU32 LBA);
tU32 ATAPI_IF_MSF2LBA(tU32 Min, tU32 Sec, tU32 Frm);
tU32 ATAPI_IF_OSAL_TrckPos2LBA(ATAPI_IF_CDCACHE *Cache,
                                    OSAL_trTrackPosition *TrackPos);
tU32 ATAPI_IF_LBA2TRACK(ATAPI_IF_CDCACHE *Cache, tU32 LBA);
tU32 AIF_u32RegisterMediaNotifier(ATAPI_IF_PROC_INFO *Info,
                                    const OSAL_trNotifyData* pReg);
tS32 AIF_s32UnregisterMediaNotifier(ATAPI_IF_PROC_INFO *Info,
                                    const OSAL_trRelNotifyData* rRelNotifyData);
/*Function declarartions added*/
tS32 ATAPI_IF_RequestData(tU8 *Buffer, ATAPI_IF_PROC_INFO *Info, 
									OSAL_tShMemHandle SemHandle, 
									tU32 StartLBA, tU32 NumFrames);//fix for CMG3GB-3460
tS32 ATAPI_IF_RequestStreamStatusChange(ATAPI_IF_PROC_INFO *Info, 
										OSAL_tShMemHandle SemHandle, 
										ATAPI_IF_STREAM_STATUS Status, 
										tU32 StartLBA, tU32 EndLBA);//fix for CMG3GB-3460
tS32 ATAPI_IF_RegisterPlayNotifier(ATAPI_IF_PROC_INFO *Info, 
                                 const OSAL_trPlayInfoReg* PlayInfoReg);//fix for CMG3GB-3460 check
tS32 ATAPI_IF_UnregisterPlayNotifier(ATAPI_IF_PROC_INFO *Info, 
                                       const OSAL_trPlayInfoReg* PlayInfoReg);//fix for CMG3GB-3460
									   
									   


#ifdef __cplusplus
}
#endif

#else //ATAPI_IF_H
# error atapi_if.h included several times
#endif //#else //ATAPI_IF_H
