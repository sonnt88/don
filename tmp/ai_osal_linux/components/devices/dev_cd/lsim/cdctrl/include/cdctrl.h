/*****************************************************************************
 * @file:    cdctrl.h
 *
 * @version  $Id$
 *
 * @component: CDCTRL
 * 
 * @description: Implements ATAPI device functionality like ejecting, reading
 *               track-info and so forth.
 *               In cdctrl.h, the respective ATAPI commands are defined
 *                
 * Featureswitches:
 *               VARIANT_FTR_MAP_CD2HDD         enables cd-to-hdd mapping
 *               VARIANT_FTR_ATANEC             for kenwood dvs3xxx drives
 *               VARIANT_FTR_OLD_ATAPI          for old atapi drive
 *               KENWOOD_DVS3XXX                to enable special Kenwood commands
 *
 *
 * @author: 3SOFT GmbH Erlangen, Stephan Sigwart
 *
 * @copyright:
 *
 * @history
 * 09.05.06 | initial revision      | 3SOFT, Stephan Sigwart
 *
 */
#ifndef CDCTRL_H
#define CDCTRL_H

#define CDCTRL_C_S32_IO_VERSION        (tS32)(0x00000210)
#define CDCTRL_ENABLE_DEBUG 1


/*Vendor ID START*/
#define CDCTRL_MASCA_VENDOR_ID_NOT_AVAILABLE      0
#define CDCTRL_MASCA_VENDOR_ID_UNKNOWN            1
#define CDCTRL_MASCA_VENDOR_ID_TANASHIN           2
#define CDCTRL_MASCA_VENDOR_ID_KENWOOD            3
#define CDCTRL_MASCA_VENDOR_ID_PIONEER            4

/************************************************************************
 |typedefs and struct defs (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 | variable declaration (scope: global)
 |-----------------------------------------------------------------------*/

/************************************************************************
 |function prototypes (scope: global)
 |-----------------------------------------------------------------------*/
tU32 CDCTRL_u32Init(void);
tU32 CDCTRL_u32Destroy(void);
tU32 CDCTRL_u32IOOpen(tS32 );
tU32 CDCTRL_u32IOClose(tS32);
tU32 CDCTRL_u32IOControl(tS32 , tS32 , tS32 );



// eof ifndef CDCTRL_H
#endif

