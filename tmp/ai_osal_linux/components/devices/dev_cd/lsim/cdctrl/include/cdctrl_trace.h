/************************************************************************
| FILE:         cdctrl_trace.h
| PROJECT:      Gen3 LSIM platform
| SW-COMPONENT: CDCTRL driver
|------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cdctrl_trace.h
 *
 * @brief   This file includes trace stuff for the cdctrl driver.
 * It is ported from Gen2
 *
 * @author  sgo1cob
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 * ------------------------------------------------------------------
 | Date      		| Modification                            | Author
 | 07/07/2016       | Fix for CMG3GB-3460                     | boc7kor
 |                  | CDAUDIO_IS_USING_LOCAL_TRACE is defined |
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (CDCTRL_TRACE_H)
   #define CDCTRL_TRACE_H
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/
// if defined, cdctrl registering its own trace command channel
#define CDCTRL_IS_USING_LOCAL_TRACE //fix for CMG3GB-3460





/*trace classes used by dev cdctrl, defined in osalcore/include/ostrace.h*/
#define CDCTRL_TRACE_CLASS OSAL_C_TR_CLASS_DEV_CDCTRL
    
#define CDCTRL_TRACE_ENTER_U2(FUNC,P1,P2,P3,P4) if(CDCTRL_bTraceOn)\
 CDCTRL_vTraceEnter(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_1,\
                   __LINE__, #FUNC, (P1), (P2), (P3), (P4))

#define CDCTRL_TRACE_ENTER_U3(FUNC,P1,P2,P3,P4) if(CDCTRL_bTraceOn)\
 CDCTRL_vTraceEnter(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_2,\
                   __LINE__, #FUNC, (P1), (P2), (P3), (P4))

#define CDCTRL_TRACE_ENTER_U4(FUNC,P1,P2,P3,P4) if(CDCTRL_bTraceOn)\
 CDCTRL_vTraceEnter(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_4,\
                   __LINE__, #FUNC, (P1), (P2), (P3), (P4))

#define CDCTRL_TRACE_LEAVE_U2(FUNC,ERROR,P1,P2,P3,P4) if(CDCTRL_bTraceOn)\
 CDCTRL_vTraceLeave(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_1,\
                   __LINE__, #FUNC, (ERROR), (P1), (P2), (P3), (P4))

#define CDCTRL_TRACE_LEAVE_U3(FUNC,ERROR,P1,P2,P3,P4) if(CDCTRL_bTraceOn)\
 CDCTRL_vTraceLeave(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_2,\
                   __LINE__, #FUNC, (ERROR), (P1), (P2), (P3), (P4))

#define CDCTRL_TRACE_LEAVE_U4(FUNC,ERROR,P1,P2,P3,P4) if(CDCTRL_bTraceOn)\
 CDCTRL_vTraceLeave(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_4,\
                   __LINE__, #FUNC, (ERROR), (P1), (P2), (P3), (P4))

#define CDCTRL_TRACE_IOCTRL(IOCTRL,P1,P2,P3,P4) if(CDCTRL_bTraceOn)\
 CDCTRL_vTraceIoctrl(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_1,\
                   __LINE__, (IOCTRL), (P1), (P2), (P3), (P4))

#define CDCTRL_PRINTF_IOCTRL_RESULT(OSALIOCTRL,OSALERRORTXT) if(CDCTRL_bTraceOn)\
 CDCTRL_vTraceIoctrlResult(CDCTRL_TRACE_CLASS, TR_LEVEL_ERRORS,\
                     __LINE__, (OSALIOCTRL), (OSALERRORTXT))


#define CDCTRL_PRINTF_FORCED(...)\
 CDCTRL_vTracePrintf(CDCTRL_TRACE_CLASS, TR_LEVEL_FATAL, TRUE,\
                     __LINE__, __VA_ARGS__)

#define CDCTRL_PRINTF_FATAL(...)\
 CDCTRL_vTracePrintf(CDCTRL_TRACE_CLASS, TR_LEVEL_FATAL, FALSE,\
                     __LINE__, __VA_ARGS__)


#define CDCTRL_PRINTF_ERRORS(...)\
 CDCTRL_vTracePrintf(CDCTRL_TRACE_CLASS, TR_LEVEL_ERRORS, FALSE,\
                     __LINE__, __VA_ARGS__)


#define CDCTRL_PRINTF_U1(...) if(CDCTRL_bTraceOn)\
 CDCTRL_vTracePrintf(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_1, FALSE,\
                     __LINE__, __VA_ARGS__)

#define CDCTRL_PRINTF_U2(...) if(CDCTRL_bTraceOn)\
 CDCTRL_vTracePrintf(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_2, FALSE,\
                     __LINE__, __VA_ARGS__)

#define CDCTRL_PRINTF_U3(...) if(CDCTRL_bTraceOn)\
 CDCTRL_vTracePrintf(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_3, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CDCTRL_PRINTF_U4(...) if(CDCTRL_bTraceOn)\
 CDCTRL_vTracePrintf(CDCTRL_TRACE_CLASS, TR_LEVEL_USER_4, FALSE,\
                      __LINE__, __VA_ARGS__)


/** trace message IDs */
enum CDCTRL_enTraceMessages
{
    CDCTRL_TRACE_ENTER = 1,
    CDCTRL_TRACE_LEAVE,
	CDCTRL_TRACE_PRINTF,
	CDCTRL_TRACE_PRINTF_ERROR,
	CDCTRL_TRACE_IOCTRL,
	CDCTRL_TRACE_IOCTRL_RESULT
};

/** trace command messages */
enum CDCTRL_enTraceCommandMessages
{
	CDCTRL_TRACE_CMD_TRACE_ON  = 0x01,
	CDCTRL_TRACE_CMD_TRACE_OFF = 0x02,
	CDCTRL_TRACE_CMD_OPEN      = 0x03,
	CDCTRL_TRACE_CMD_CLOSE     = 0x04,
	//CDCTRL_TRACE_CMD_IOCTRL = 0x03
	CDCTRL_TRACE_CMD_CLOSEDOOR = 0x10,
	CDCTRL_TRACE_CMD_EJECTMEDIA,

	CDCTRL_TRACE_CMD_GETTEMP   = 0x20,
	CDCTRL_TRACE_CMD_GETLOADERINFO,
	CDCTRL_TRACE_CMD_GETTRACKINFO,
	CDCTRL_TRACE_CMD_GETCDINFO,
	CDCTRL_TRACE_CMD_GET_DRIVE_VERSION,
	CDCTRL_TRACE_CMD_GETMEDIAINFO,
	CDCTRL_TRACE_CMD_GETDEVICEINFO,
	CDCTRL_TRACE_CMD_GETDISKTYPE,
	CDCTRL_TRACE_CMD_GETDRIVEVERSION,
	CDCTRL_TRACE_CMD_GET_DEVICE_VERSION,
	CDCTRL_TRACE_CMD_GETDVDINFO,

	CDCTRL_TRACE_CMD_SETMOTORON = 0x30,
	CDCTRL_TRACE_CMD_SETMOTOROFF,
	CDCTRL_TRACE_CMD_SETPOWEROFF,
	CDCTRL_TRACE_CMD_SETDRIVESPEED,

	CDCTRL_TRACE_CMD_READRAWDATA = 0x40,
	CDCTRL_TRACE_CMD_READRAWDATAUNCACHED,
	CDCTRL_TRACE_CMD_READRAWDATA_MSF,
	CDCTRL_TRACE_CMD_READERRORBUFFER,
	CDCTRL_TRACE_CMD_EJECTLOCK,
	CDCTRL_TRACE_CMD_REG_NOTIFICATION,
	CDCTRL_TRACE_CMD_UNREG_NOTIFICATION
};


/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/
/* Traces - these are not names of IOCTRLs!*/
typedef enum 
{
	CDCTRL_EN_IOCONTROL,
	CDCTRL_EN_IOCONTROL_RESULT,
	CDCTRL_EN_TEMPERATURE,
	CDCTRL_EN_LOADER_INFO,
	CDCTRL_EN_GETTRACKINFO,
	CDCTRL_EN_READ_RAW_LBA, //5
	CDCTRL_EN_READ_RAW_MSF,
	CDCTRL_EN_CD_INFO,
	CDCTRL_EN_MEDIA_INFO,
	CDCTRL_EN_DRIVE_VERSION,
	CDCTRL_EN_DRIVE_VERSION_FULL, //10
	CDCTRL_EN_IOCTRL_VERSION,
	CDCTRL_EN_GETDEVICEINFO,
	CDCTRL_EN_GETDISKTYPE,
	CDCTRL_EN_SETDRIVESPEED,
	CDCTRL_EN_REG_NOTIFICATION,  //15
	CDCTRL_EN_UNREG_NOTIFICATION  //16

}CDCTRL_enumIoctrlTraceID;


/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/                     
#ifdef CDCTRL_IS_USING_LOCAL_TRACE
tVoid CDCTRL_vRegTrace(tVoid);
tVoid CDCTRL_vUnregTrace(tVoid);
#endif //#ifdef CDCTRL_IS_USING_LOCAL_TRACE

tVoid CDCTRL_vTraceEnter(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pcvFunction,
                         tU32 u32Par1,
                         tU32 u32Par2,
                         tU32 u32Par3,
                         tU32 u32Par4);

tVoid CDCTRL_vTraceLeave(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pcvFunction,
                         tU32 u32OSALError,
                         tU32 u32Par1,
                         tU32 u32Par2,
                         tU32 u32Par3,
                         tU32 u32Par4);

tVoid CDCTRL_vTraceIoctrl(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         CDCTRL_enumIoctrlTraceID enIoctrlTraceID,
                         tU32 u32Par1,
                         tU32 u32Par2,
                         tU32 u32Par3,
                         tU32 u32Par4);

tVoid CDCTRL_vTraceIoctrlResult(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         tS32 s32IoCtrl,
                         const char* pcszErrorTxt);


tVoid CDCTRL_vTracePrintf(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
						  tBool bForced,
                          tU32 u32Line,
                          const char* coszFormat,...);




#ifdef __cplusplus
}
#endif
     
#else
#error cdctrl_trace.h included several times
#endif                

