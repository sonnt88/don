/************************************************************************
| FILE:         cdctrl_trace.c
| PROJECT:      Gen2
| SW-COMPONENT: Cdctrl driver
|------------------------------------------------------------------------
|************************************************************************/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cdctrl_trace.c
 *
 * @brief   This file includes trace stuff for the cdctrl device
 *
 * @author  srt2hi
 *
 * @date    today
 *
 * @version 1
 *
 * @note    1
 *
 *//* ***************************************************FileHeaderEnd******* */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/* General headers */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"


#include "Linux_osal.h"

//#define OSAL_S_IMPORT_INTERFACE_GENERIC
//#include "osal_if.h"
#include "trace_interface.h"
#include "ostrace.h"
#include "cdctrl_trace.h"




/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define CDCTRL_IS_USING_LOCAL_TRACE
#define CDCTRL_PRINTF_BUFFER_SIZE 256
#define CDCTRL_TRACE_BUFFER_SIZE  256
#define CDCTRL_GET_U32_LE(PU8) (((tU32)(PU8)[0]) | (((tU32)(PU8)[1])<<8)\
                          | (((tU32)(PU8)[2])<<16) | (((tU32)(PU8)[3])<<24) )



/************************************************************************
|typedefs (scope: module-local)
|----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
/*trace speed up - if FALSE, calling of tracing functions are suppressed*/
/*is set automatically to TRUE, if trace level >= User1 is enabled at startup*/
tBool CDCTRL_bTraceOn = TRUE;//FALSE;

//extern BOOL UTIL_trace_isActive(TraceData* data);
/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static volatile tBool          gTraceCmdThreadIsRunning = FALSE;
static tU8 gu8TraceBuffer[CDCTRL_TRACE_BUFFER_SIZE]; /*not sychronyzed!*/
static OSAL_tIODescriptor ghDevice = OSAL_ERROR;                            

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    coszGetErrorText
*
* DESCRIPTION: get pointer to osal error text
*
*
*****************************************************************************/
static const char* pGetErrTxt()
{
   return (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode());
}
/*****************************************************************************
*
* FUNCTION:
*     getArgList
*
* DESCRIPTION:
*     This stupid function is used for satifying L I N T
*     
*     
* PARAMETERS: *va_list pointer to a va_list
*
* RETURNVALUE:
*     va_list
*     
*
*
*****************************************************************************/
static va_list getArgList(va_list* a)
{
	(void)a;
	return *a;
}

/********************************************************************/ /**
*  FUNCTION:      uiCGET
*
*  @brief         return time in ms
*
*  @return        time in ms
*
*  HISTORY:
*
************************************************************************/
static unsigned int uiCGET(void)
{
    return (unsigned int)OSAL_ClockGetElapsedTime();
}



/********************************************************************/ /**
 *  FUNCTION:      CDCTRL_vTraceEnter
 *
 *  @brief         enter function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/

tVoid CDCTRL_vTraceEnter(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pFunction,
                         tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
       tUInt uiIndex;
       tU8  au8Buf[4 * sizeof(tU32) + 8 * sizeof(tU32) + 32]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();
	   tU32 u32FunctionOffset;

       uiIndex = 0;
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) CDCTRL_TRACE_ENTER);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 11);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
       uiIndex += sizeof(tU32);
       strncpy(au8Buf + uiIndex, pFunction, sizeof(au8Buf) - uiIndex - 1);
       au8Buf[sizeof(au8Buf) - 1] = 0;

       // trace the stuff 
       // be aware index is not greater than aray size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex + 32);
    }
}




/********************************************************************/ /**
 *  FUNCTION:      CDCTRL_vTraceLeave
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDCTRL_vTraceLeave(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pFunction,
                         tU32 u32OSALResult,
                         tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{

    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
       tUInt uiIndex;
       tU8  au8Buf[4 * sizeof(tU8) + 9 * sizeof(tU32) + 32]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();
	   tU32 u32FunctionOffset;

       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDCTRL_TRACE_LEAVE);
       uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 11);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32OSALResult);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
       uiIndex += sizeof(tU32);
       strncpy(au8Buf + uiIndex, pFunction, sizeof(au8Buf) - uiIndex - 1);
       au8Buf[sizeof(au8Buf) - 1] = 0;

       // trace the stuff 
       // be aware index is not greater than aray size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex + 32);
    }
}

/********************************************************************/ /**
 *  FUNCTION:      CDCTRL_vTraceIoctrl
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDCTRL_vTraceIoctrl(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
                          tU32 u32Line,
                          CDCTRL_enumIoctrlTraceID enIoctrlTraceID,
                          tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{

    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
       tUInt uiIndex;
       tU8  au8Buf[4 * sizeof(tU8) + 7 * sizeof(tU32)]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();

       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDCTRL_TRACE_IOCTRL);
       uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enIoctrlTraceID);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
       uiIndex += sizeof(tU32);

       // trace the stuff 
       // be aware index is not greater than aray size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex);
    }
}

/********************************************************************/ /**
 *  FUNCTION:      CDCTRL_vTraceIoctrlResult
 *
 *  @brief         error text with IOCtrl-name
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDCTRL_vTraceIoctrlResult(tU32 u32Class,
                               TR_tenTraceLevel enTraceLevel,
                               tU32 u32Line,
                               tS32 s32OsalIOCtrl,
                               const char *pcszErrorTxt)
{

    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
       tUInt uiIndex;
       tU8  au8Buf[4 * sizeof(tU8) + 5 * sizeof(tU32) + 202]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();

       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDCTRL_TRACE_IOCTRL_RESULT);
       uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 11);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
	   uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Time);
	   uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)s32OsalIOCtrl);
	   uiIndex += sizeof(tU32);
	   strncpy((char*)&au8Buf[uiIndex], (const char*)pcszErrorTxt, 200);
       uiIndex += 200;
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32)0);
	   uiIndex += sizeof(tU8);

       // trace the stuff 
       // be aware index is not greater than aray size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex);
    }
}


/*****************************************************************************
*
* FUNCTION:
*     CDCTRL_vTracePrintf
*
* DESCRIPTION:
*     This function creates the printf-style trace message
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
*
*****************************************************************************/
tVoid CDCTRL_vTracePrintf(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
						  tBool bForced,
                          tU32 u32Line,
                          const char* coszFormat,...)
{
   if((TRUE == bForced)
	  ||
	  (LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE))
   {
      static tU8  au8Buf[CDCTRL_PRINTF_BUFFER_SIZE + 1];  /*TODO: ? size ?*/
      tUInt uiIndex;
      long lSize;
      size_t tMaxSize;
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time     = (tU32)uiCGET();
	  va_list argList;


	  uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex],
				   (tU32) (enTraceLevel == TR_LEVEL_ERRORS
						    ?
						   CDCTRL_TRACE_PRINTF_ERROR : CDCTRL_TRACE_PRINTF));
      uiIndex += sizeof(tU8);
	  OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);
	  uiIndex += sizeof(tU8);
	  OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);
	  uiIndex += sizeof(tU8);
	  OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);
	  uiIndex += sizeof(tU8);
       /*next pointer is fix set to 32 bit -be happy with 64 bit systems*/
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      
	  argList = getArgList(&argList);
	  va_start(argList, coszFormat); /*lint !e718 */  
      tMaxSize = CDCTRL_PRINTF_BUFFER_SIZE - uiIndex;
      lSize = vsnprintf((char*)&au8Buf[uiIndex], tMaxSize, coszFormat, argList);
      uiIndex += (tUInt)lSize;
      va_end(argList);
      
      // trace the stuff 
      LLD_vTrace(u32Class,
                 (tS32)enTraceLevel,
                 au8Buf, 
                 (tU32)uiIndex);
   }
   
}


/*****************************************************************************
* FUNCTION:     vMediaTypeNotify 
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  PRM Callback 

* HISTORY:
******************************************************************************/
#define CDCTRL_LOWORD(U32) ((tU16)((U32)&(0xFFFFUL)))
#define CDCTRL_HIWORD(U32) ((tU16)((U32) >> 16))
static tVoid vMediaTypeNotify(tU32 *pu32MediaChangeInfo)
{
   tU16 u16NotiType   = 0;
   tU16 u16MediaState = 0;
   tU16 u16MediaType  = 0;
   (void)pu32MediaChangeInfo; // L I N T 


   if(pu32MediaChangeInfo != NULL)
   {
      u16NotiType = CDCTRL_HIWORD(*pu32MediaChangeInfo);
	  CDCTRL_PRINTF_FORCED("vMediaTypeNotify: 0x%08X",
							(unsigned int)*pu32MediaChangeInfo);

      //BPCD_OEDT_PRINTF(("BPCD_OEDT_T8_MediaTypeNotify u16NotiType 0x%08X, LoWord 0x%08X", (unsigned int)u16NotiType, (unsigned int)BPCD_OEDT_LOWORD(*pu32MediaChangeInfo)));

		switch(u16NotiType)
		{
		case OSAL_C_U16_NOTI_MEDIA_CHANGE:
			{
				u16MediaType = CDCTRL_LOWORD(*pu32MediaChangeInfo);
				switch(u16MediaType)
				{
				case OSAL_C_U16_MEDIA_EJECTED:
					break;

				case OSAL_C_U16_INCORRECT_MEDIA:
					break;

				case OSAL_C_U16_DATA_MEDIA:
					break;

				case OSAL_C_U16_AUDIO_MEDIA:
					break;

				case OSAL_C_U16_UNKNOWN_MEDIA:
					break;

				default:
					;
				} //End of switch
			}
			break;

		case OSAL_C_U16_NOTI_TOTAL_FAILURE:
			break;

		case OSAL_C_U16_NOTI_MODE_CHANGE:
			break;

		case OSAL_C_U16_NOTI_MEDIA_STATE:
			u16MediaState = CDCTRL_LOWORD(*pu32MediaChangeInfo);
			(void)u16MediaState;

			break;

		case OSAL_C_U16_NOTI_DVD_OVR_TEMP:
			break;

		default:
			;
		} //End of switch
	} //if(pu32MediaChangeInfo != NULL)
	else //Invalid pointer
	{
		//BPCD_OEDT_T8_u16MediaType = (tU16) 0xFFFF;
	}

	//BPCD_OEDT_TRACE(BPCD_OEDT_T8_u16MediaType);
}


/*****************************************************************************
* FUNCTION:     bIsOpen
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  checks, if device is open

* HISTORY:
******************************************************************************/
static tBool bIsOpen()
{
	tBool bOpen;
	if(OSAL_ERROR != ghDevice)
	{
		bOpen = TRUE;
	}
	else //if(OSAL_ERROR != ghDevice)
	{
		bOpen = FALSE;
		CDCTRL_PRINTF_ERRORS("device not open");
	} //else //if(OSAL_ERROR != ghDevice)

	return bOpen;
}

/*****************************************************************************
* FUNCTION:     bIOCtrlOK
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  checks, if ioctrl succeeded

* HISTORY:
******************************************************************************/
static tBool bIOCtrlOK(tS32 s32Fun, tS32 s32Ret)
{
	tBool bOK = (s32Ret != OSAL_ERROR);
	CDCTRL_PRINTF_IOCTRL_RESULT(s32Fun, bOK ? "SUCCESS" : pGetErrTxt());
	return bOK;
}


/*****************************************************************************
* FUNCTION:     vTraceCmdThread
* PARAMETER:    pvData
* RETURNVALUE:  None
* DESCRIPTION:  This is a Trace Thread handler.

* HISTORY:
******************************************************************************/
static tVoid vTraceCmdThread(tPVoid pvData)
{
  const tU8  *pu8Data = (const tU8*)pvData;
  const tU8  *pu8Par  = &pu8Data[3];
  tU8 u8Len    = pu8Data[1];
  tU8 u8Cmd    = pu8Data[2];

  (void)pvData;

  CDCTRL_PRINTF_U4("vTraceCmdThread %p, TraceCmd[Len 0x%02X Cmd 0x%02X]",
				   pu8Data,
				   (unsigned int)u8Len,
				   (unsigned int)u8Cmd);
	switch(u8Cmd)
	{
	case CDCTRL_TRACE_CMD_TRACE_ON:
		CDCTRL_bTraceOn = TRUE;
		break;

	case CDCTRL_TRACE_CMD_TRACE_OFF:
		CDCTRL_bTraceOn = FALSE;
		break;

	case CDCTRL_TRACE_CMD_OPEN:
		CDCTRL_PRINTF_U4("OPEN");
		if(OSAL_ERROR == ghDevice)
		{
			ghDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDCTRL,
									 OSAL_EN_READWRITE );

			if(ghDevice == OSAL_ERROR)
			{
				CDCTRL_PRINTF_ERRORS("OPEN: [%s]", pGetErrTxt());
			}
			else //if(ghDevice == OSAL_ERROR)
			{
				CDCTRL_PRINTF_FORCED("OPEN: handle 0x%08X",
									 (unsigned int)ghDevice);
			} //else //if(ghDevice == OSAL_ERROR)
		}
		else //if(OSAL_ERROR == ghDevice)
		{
			CDCTRL_PRINTF_ERRORS("already open - handle 0x%08X", 
								 (unsigned int)ghDevice);
		} //else //if(OSAL_ERROR == ghDevice)
		break;

	case CDCTRL_TRACE_CMD_CLOSE:
		CDCTRL_PRINTF_U4("CLOSE");
		if(bIsOpen())
		{
			tS32 s32Ret = OSAL_s32IOClose (ghDevice);
			if(s32Ret == OSAL_ERROR)
			{
				CDCTRL_PRINTF_ERRORS("CLOSE: [%s]", pGetErrTxt());
			}
			else //if(ghDevice == OSAL_ERROR)
			{
				CDCTRL_PRINTF_FORCED("CLOSE: Success");
				ghDevice = OSAL_ERROR;
			} //else //if(ghDevice == OSAL_ERROR)
		} //if(bIsOpen())
		break;

		/*case CDCTRL_TRACE_CMD_IOCTRL:
		{
		tU8 u8SubCmd = pu8Data[3];
		CDCTRL_PRINTF_U4("TraceCmd IOCTRL SubCmd[0x%02X]", (unsigned int)u8SubCmd);
		switch(u8SubCmd)
		{
		case 1:
		break;
		default:
		;
		} //switch(u8SubCmd)
		}
		break;*/
	case CDCTRL_TRACE_CMD_CLOSEDOOR:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL CLOSEDOOR [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_EJECTMEDIA:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL EJECTMEDIA [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_GETTEMP:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETTEMP [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trDriveTemperature rTemp;

				rTemp.u8Temperature = 0;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP;
				s32Arg = (tS32)&rTemp;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					CDCTRL_PRINTF_FORCED("Temperature value: [%d]",
										 (int)rTemp.u8Temperature);
				}
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_GETLOADERINFO:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETLOADERINFO [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trLoaderInfo rInfo;

				rInfo.u8LoaderInfoByte = 0;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO;
				s32Arg = (tS32)&rInfo;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					CDCTRL_PRINTF_FORCED("Loader: [%d]",
										 (int)rInfo.u8LoaderInfoByte);
				}
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_GETTRACKINFO:
		{
			tU8 u8TrackNumber = pu8Par[0];
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETTRACKINFO Track [%02u]",
							 (unsigned int)u8TrackNumber);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trCDROMTrackInfo rInfo;

				rInfo.u32TrackNumber  = (tU32)u8TrackNumber;
				rInfo.u32TrackControl = 0;
				rInfo.u32LBAAddress   = 0;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO;
				s32Arg = (tS32)&rInfo;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					CDCTRL_PRINTF_FORCED("GetTrackInfo:"
										 " Track [%02d],"
										 " StartLBA [%u],"
										 " Control  [0x%08X]",
										 (unsigned int)rInfo.u32TrackNumber,
										 (unsigned int)rInfo.u32LBAAddress,
										 (unsigned int)rInfo.u32TrackControl
										);
				}
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_SETMOTORON:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL SETMOTORON [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETMOTORON;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_SETMOTOROFF:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL SETMOTOROFF [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETMOTOROFF;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_READRAWDATAUNCACHED:  
		CDCTRL_PRINTF_U4("TraceCmd IOCTRL READRAWDATAUNCACHED -"
						 " MAPPED TO CACHED READ");
		/* FALLTHROUGH */
	case CDCTRL_TRACE_CMD_READRAWDATA:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL READRAWDATA [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS8 *ps8Buf;
				tU32 u32LBA       = CDCTRL_GET_U32_LE(&pu8Par[0]);
				tU32 u32NumBlocks = CDCTRL_GET_U32_LE(&pu8Par[4]);
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trReadRawInfo rRRInfo;

				ps8Buf = (tS8*)malloc(u32NumBlocks * 2048);
				if(ps8Buf)
				{
					memset(ps8Buf,0xFF,u32NumBlocks * 2048);
				}

				rRRInfo.u32NumBlocks  = u32NumBlocks;
				rRRInfo.u32LBAAddress = u32LBA;
				rRRInfo.ps8Buffer     = ps8Buf;

				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA;
				s32Arg = (tS32)&rRRInfo;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					tU32 u32Block, u32BlockIndex;
					CDCTRL_PRINTF_FORCED("ReadRawDataLBA:"
										 " LBA %u,"
										 " Blocks %u,"
										 " BufferAddress %p",
										 (unsigned int)rRRInfo.u32LBAAddress,
										 (unsigned int)rRRInfo.u32NumBlocks,
										 rRRInfo.ps8Buffer
										);
					//print blocks
					for(u32Block = 0; u32Block < rRRInfo.u32NumBlocks; ++u32Block)
					{
						for(u32BlockIndex=0; u32BlockIndex < 2048; u32BlockIndex+=16)
						{
							tU8 *pu8 = (tU8*)&rRRInfo.ps8Buffer[(u32Block * 2048) + u32BlockIndex];
							CDCTRL_PRINTF_FORCED("%04X:%04X "
												 "%02X %02X "
												 "%02X %02X "
												 "%02X %02X "
												 "%02X %02X - "
												 "%02X %02X "
												 "%02X %02X "
												 "%02X %02X "
												 "%02X %02X",
												 (unsigned int)u32Block,
												 (unsigned int)u32BlockIndex,
												 (unsigned int)pu8[0],
												 (unsigned int)pu8[1],
												 (unsigned int)pu8[2],
												 (unsigned int)pu8[3],
												 (unsigned int)pu8[4],
												 (unsigned int)pu8[5],
												 (unsigned int)pu8[6],
												 (unsigned int)pu8[7],
												 (unsigned int)pu8[8],
												 (unsigned int)pu8[9],
												 (unsigned int)pu8[10],
												 (unsigned int)pu8[11],
												 (unsigned int)pu8[12],
												 (unsigned int)pu8[13],
												 (unsigned int)pu8[14],
												 (unsigned int)pu8[15]
												);


						} //for(u32BlockIndex=0; u32BlockIndex < 2048, ++u32BlockIndex)
					} //for(u32Block = 0, u32Block < nBlocks)


				} //if(bIOCtrlOK(s32Fun, s32Ret))

				if(ps8Buf != NULL)
				{
					free(ps8Buf);
				} //if(ps8Buf != NULL)
			} //if(bIsOpen())

		}
		break;
	case CDCTRL_TRACE_CMD_READRAWDATA_MSF:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL READRAWDATA_MSF [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS8 *ps8Buf;
				tU8  u8Min   = pu8Par[0];
				tU8  u8Sec   = pu8Par[1];
				tU8  u8Frame = pu8Par[2];
				tU32 u32NumBlocks = CDCTRL_GET_U32_LE(&pu8Par[4]);
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trReadRawMSFInfo rRRInfo;

				ps8Buf = (tS8*)malloc(u32NumBlocks * 2048);
				if(ps8Buf)
				{
					memset(ps8Buf,0xFF,u32NumBlocks * 2048);
				}

				rRRInfo.u32NumBlocks  = u32NumBlocks;
				rRRInfo.u8Minute      = u8Min;
				rRRInfo.u8Second      = u8Sec;
				rRRInfo.u8Frame       = u8Frame;
				rRRInfo.ps8Buffer     = ps8Buf;

				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF;
				s32Arg = (tS32)&rRRInfo;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					tU32 u32Block, u32BlockIndex;
					CDCTRL_PRINTF_FORCED("ReadRawDataMSF:"
										 " MSF %02u:%02u:%02u,"
										 " Blocks %u,"
										 " BufferAddress %p",
										 (unsigned int)rRRInfo.u8Minute,
										 (unsigned int)rRRInfo.u8Second,
										 (unsigned int)rRRInfo.u8Frame,
										 (unsigned int)rRRInfo.u32NumBlocks,
										 rRRInfo.ps8Buffer
										);
					//print blocks
					for(u32Block = 0; u32Block < rRRInfo.u32NumBlocks; ++u32Block)
					{
						for(u32BlockIndex=0; u32BlockIndex < 2048; u32BlockIndex+=16)
						{
							tU8 *pu8 = (tU8*)&rRRInfo.ps8Buffer[(u32Block * 2048) + u32BlockIndex];
							CDCTRL_PRINTF_FORCED("%04X:%04X "
												 "%02X %02X "
												 "%02X %02X "
												 "%02X %02X "
												 "%02X %02X - "
												 "%02X %02X "
												 "%02X %02X "
												 "%02X %02X "
												 "%02X %02X",
												 (unsigned int)u32Block,
												 (unsigned int)u32BlockIndex,
												 (unsigned int)pu8[0],
												 (unsigned int)pu8[1],
												 (unsigned int)pu8[2],
												 (unsigned int)pu8[3],
												 (unsigned int)pu8[4],
												 (unsigned int)pu8[5],
												 (unsigned int)pu8[6],
												 (unsigned int)pu8[7],
												 (unsigned int)pu8[8],
												 (unsigned int)pu8[9],
												 (unsigned int)pu8[10],
												 (unsigned int)pu8[11],
												 (unsigned int)pu8[12],
												 (unsigned int)pu8[13],
												 (unsigned int)pu8[14],
												 (unsigned int)pu8[15]
												);


						} //for(u32BlockIndex=0; u32BlockIndex < 2048, ++u32BlockIndex)
					} //for(u32Block = 0, u32Block < nBlocks)


				} //if(bIOCtrlOK(s32Fun, s32Ret))

				if(ps8Buf != NULL)
				{
					free(ps8Buf);
				} //if(ps8Buf != NULL)
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_GETCDINFO:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETCDINFO [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trCDInfo  rCDInfo;

				rCDInfo.u32MaxTrack = 0;
				rCDInfo.u32MinTrack = 0;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO;
				s32Arg = (tS32)&rCDInfo;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					CDCTRL_PRINTF_FORCED("CDInfo: MinTrack %02u"
										 " MaxTrack %02u",
										 (unsigned int)rCDInfo.u32MinTrack,
										 (unsigned int)rCDInfo.u32MaxTrack
										);
				}
			}
		}
		break;
	case CDCTRL_TRACE_CMD_GET_DRIVE_VERSION:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GET_DRIVE_VERSION [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trCDDriveVersion rVersion;

				rVersion.u32HWVersion = 0;
				rVersion.u32SWVersion = 0;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GET_DRIVE_VERSION;
				s32Arg = (tS32)&rVersion;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					CDCTRL_PRINTF_FORCED("Version: HW 0x%02X SW 0x%02X",
										 (unsigned int)rVersion.u32HWVersion,
										 (unsigned int)rVersion.u32SWVersion
										);
				}
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_SETPOWEROFF:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL SETPOWEROFF [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETPOWEROFF;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_GETMEDIAINFO:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETMEDIAINFO [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trMediaInfo rInfo;

				rInfo.nTimeZone = 0;
				rInfo.szcCreationDate[0] ='\0';
				rInfo.szcMediaID[0] ='\0';
				rInfo.u8FileSystemType = 0;
				rInfo.u8MediaType = 0;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO;
				s32Arg = (tS32)&rInfo;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					CDCTRL_PRINTF_FORCED("MediaInfo: MediaType %u"
										 " FS %u TimeZone %u"
										 " Date [%s] ID [%s]",
										 (unsigned int)rInfo.u8MediaType,
										 (unsigned int)rInfo.u8FileSystemType,
										 (unsigned int)rInfo.nTimeZone,
										 (const char*)rInfo.szcCreationDate,
										 (const char*)rInfo.szcMediaID
										);
				}
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_GETDEVICEINFO:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETDEVICEINFO [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				tU32 u32MediaDev = 0;

				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO;
				s32Arg = (tS32)&u32MediaDev;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					CDCTRL_PRINTF_FORCED("GetDeviceInfo: MediaDev 0x%04X",
										 (unsigned int)u32MediaDev
										);
				}
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_GETDISKTYPE:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETDISKTYPE [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trDiskType rDiskType;

				rDiskType.u8DiskSubType = 0;
				rDiskType.u8DiskType    = 0;

				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE;
				s32Arg = (tS32)&rDiskType;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					CDCTRL_PRINTF_FORCED("GetDiskType: Type %u, SubType %u",
										 (unsigned int)rDiskType.u8DiskType,
										 (unsigned int)rDiskType.u8DiskSubType
										);
				}
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_GETDRIVEVERSION:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETDRIVEVERSION [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				OSAL_trDriveVersion rVersion;

				rVersion.u16MajorVersionNumber = 0;
				rVersion.u16MinorVersionNumber = 0;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDRIVEVERSION;
				s32Arg = (tS32)&rVersion;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					/*! au8.. are not zero teminated!
					  it is only working without crash 
					  as long Major/Minor contain at least one 0x00
					  and are last members in structur*/
					CDCTRL_PRINTF_FORCED("VersionFULL:"
										 " FirmwarRevision [%s],"
										 " Model [%s],"
										 " Serial [%s],"
										 " MajorVersion 0x%04X"
										 " MinorVersion 0x%04X",
										 (const char*)rVersion.au8FirmwareRevision,
										 (const char*)rVersion.au8ModelNumber,
										 (const char*)rVersion.au8SerialNumber,
										 (unsigned int)rVersion.u16MajorVersionNumber,
										 (unsigned int)rVersion.u16MinorVersionNumber
										);
				}
			} //if(bIsOpen())

		}
		break;
	case CDCTRL_TRACE_CMD_GET_DEVICE_VERSION:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GET_DEVICE_VERSION [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				tU32 u32Version = 0;

				s32Fun = OSAL_C_S32_IOCTRL_VERSION;
				s32Arg = (tS32)&u32Version;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				if(bIOCtrlOK(s32Fun, s32Ret))
				{
					CDCTRL_PRINTF_FORCED("IOCtrlVersion: 0x%04X",
										 (unsigned int)u32Version
										);
				}
			} //if(bIsOpen())
		}
		break;
		/*case CDCTRL_TRACE_CMD_SETSTREAMMODE:
			{
				tU8 u8StreamMode = pu8Par[0];
				CDCTRL_PRINTF_U4("TraceCmd IOCTRL SETSTREAMMODE [0x%02X]",
								 (unsigned int)pu8Par[0]);
				if(bIsOpen())
				{
					tS32 s32Fun, s32Arg, s32Ret;

					s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETDRIVESPEED;
					s32Arg = (tS32)u8SpeedID;
					s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
					(void)bIOCtrlOK(s32Fun, s32Ret);
				} //if(bIsOpen())
			}
			break;*/
	case CDCTRL_TRACE_CMD_SETDRIVESPEED:
		{
			tU8 u8SpeedID = pu8Par[0];
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL SETDRIVESPEED [SpeedID %u]",
							 (unsigned int)u8SpeedID);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;

				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_SETDRIVESPEED;
				s32Arg = (tS32)u8SpeedID;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())

		}
		break;
	case CDCTRL_TRACE_CMD_READERRORBUFFER:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL READERRORBUFFER [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_READERRORBUFFER;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_GETDVDINFO:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL GETDVDINFO [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_GETDVDINFO;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDCTRL_TRACE_CMD_EJECTLOCK:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL EJECTLOCK [0x%02X]",
							 (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDCTRL_EJECTLOCK;
					s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;

	case CDCTRL_TRACE_CMD_REG_NOTIFICATION:
		CDCTRL_PRINTF_U4("TraceCmd IOCTRL REG_NOTIFICATION [0x%02X]",
						 (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			tS32 s32Fun, s32Arg, s32Ret;
			OSAL_trNotifyData rReg;

			rReg.pu32Data            = 0;
			rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
			rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
			rReg.u8Status            = 0;
			rReg.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE | OSAL_C_U16_NOTI_MEDIA_STATE;
			rReg.pCallback           = vMediaTypeNotify;

			s32Fun = OSAL_C_S32_IOCTRL_REG_NOTIFICATION;
			s32Arg = (tS32)&rReg;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			(void)bIOCtrlOK(s32Fun, s32Ret);
			(void)rReg;
		} //if(bIsOpen())
		break;

	case CDCTRL_TRACE_CMD_UNREG_NOTIFICATION:
		CDCTRL_PRINTF_U4("TraceCmd IOCTRL UNREG_NOTIFICATION [0x%02X]",
						 (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			tS32 s32Fun, s32Arg, s32Ret;
			OSAL_trNotifyData rReg;

			rReg.pu32Data            = 0;
			rReg.ResourceName        = OSAL_C_STRING_RES_CDCTRL;
			rReg.u16AppID            = OSAL_C_U16_AUDIOCD_APPID;
			rReg.u8Status            = 0;
			rReg.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE | OSAL_C_U16_NOTI_MEDIA_STATE;
			rReg.pCallback           = vMediaTypeNotify;

			s32Fun = OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION;
			s32Arg = (tS32)&rReg;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			(void)bIOCtrlOK(s32Fun, s32Ret);
			(void)rReg;
		} //if(bIsOpen())
		break;



	default:
		{
			CDCTRL_PRINTF_U4("TraceCmd IOCTRL DEFAULT  - NOT SUPPORTED");
		}

	} //switch(u8Cmd)



  gTraceCmdThreadIsRunning = FALSE;
  OSAL_vThreadExit();
}




/*****************************************************************************
* FUNCTION:     CDCTRL_vTraceCommandCallbackHandler
* PARAMETER:    buffer
* RETURNVALUE:  None
* DESCRIPTION:  This is a Trace callback handler.

* HISTORY:
******************************************************************************/
tVoid CDCTRL_vTraceCommandCallbackHandler(const tU8* pcu8Buffer)
{
	 CDCTRL_TRACE_ENTER_U4(CDCTRL_vTraceCommandCallbackHandler,
						  (tU32)(pcu8Buffer!=NULL?pcu8Buffer[0]:0xFF),
						  (tU32)(pcu8Buffer!=NULL?pcu8Buffer[1]:0xFF),
						  (tU32)(pcu8Buffer!=NULL?pcu8Buffer[2]:0xFF),
						  (tU32)(pcu8Buffer!=NULL?pcu8Buffer[3]:0xFF)
						  );
    if(pcu8Buffer != NULL)
    {
		if(!gTraceCmdThreadIsRunning)
		{
			OSAL_trThreadAttribute  traceCmdThreadAttribute;
			OSAL_tThreadID          traceCmdThreadID;
			tU8 u8Len   = pcu8Buffer[0];
			gTraceCmdThreadIsRunning = TRUE;

			(void)OSAL_pvMemoryCopy(gu8TraceBuffer, pcu8Buffer, u8Len + 1);

			traceCmdThreadAttribute.szName       = "CDCTRLTRACE";
			traceCmdThreadAttribute.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_LOWEST;
			traceCmdThreadAttribute.pfEntry      = vTraceCmdThread;
			traceCmdThreadAttribute.pvArg        = gu8TraceBuffer;
			traceCmdThreadAttribute.s32StackSize = 1024;

			traceCmdThreadID = OSAL_ThreadSpawn(&traceCmdThreadAttribute);
			if(OSAL_ERROR == traceCmdThreadID)
			{
				CDCTRL_PRINTF_ERRORS("Create TraceThread");
			} //if(OSAL_ERROR == traceCmdThreadID)
		}
		else //if(!gTraceCmdThreadIsRunning)
		{
			CDCTRL_PRINTF_ERRORS("last Trace thread is running yet");
		} //else //if(!gTraceCmdThreadIsRunning)
    } //if(pcu8Buffer != NULL)
	CDCTRL_TRACE_LEAVE_U4(CDCTRL_vTraceCommandCallbackHandler,OSAL_E_NOERROR,
						  0,0,0,0);
}




/*****************************************************************************
* FUNCTION:     CDCTRL_vRegTrace
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  register trace command channel
* HISTORY:
******************************************************************************/
#ifdef CDCTRL_IS_USING_LOCAL_TRACE
tVoid CDCTRL_vRegTrace(tVoid)
{
	BOOL bRet;
	bRet = TR_chan_acess_bRegChan(TR_TTFIS_CDCTRL,
							   (TRACE_CALLBACK)CDCTRL_vTraceCommandCallbackHandler);
	if(!bRet)
	{
		CDCTRL_PRINTF_ERRORS("CDCTRL_vRegTrace -"
							 " Command Channel 0x%04X not registered",
							  (unsigned int)TR_TTFIS_CDCTRL);
	} //if(!bRet)
}
#endif //#ifdef CDCTRL_IS_USING_LOCAL_TRACE

/*****************************************************************************
* FUNCTION:     CDCTRL_vUnregTrace
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  unregister trace command channel
* HISTORY:
******************************************************************************/
#ifdef CDCTRL_IS_USING_LOCAL_TRACE
tVoid CDCTRL_vUnregTrace(tVoid)
{
	
	TR_chan_acess_bUnRegChan(TR_TTFIS_CDCTRL,
	                         (TRACE_CALLBACK) CDCTRL_vTraceCommandCallbackHandler);

}
#endif //#ifdef CDCTRL_IS_USING_LOCAL_TRACE


#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file 
|-----------------------------------------------------------------------*/

