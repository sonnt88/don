/************************************************************************
 | $Revision: 1.0 $
 | $Date: 02/04/2013 $
 |************************************************************************
 | FILE:         cdctrl.c
 | PROJECT:      GEN3
 | SW-COMPONENT: OSAL I/O Device
 |------------------------------------------------------------------------
 | DESCRIPTION:
 | This file contains implementation of OSAL CDCTRL driver.
 | Mostly this driver will interact with atapi_if layer to get information
 | as requested by user and pass it above layer.
 |------------------------------------------------------------------------
 | Date      	  | Modification                        | Author
 | 02/04/2013     | This file is ported from Gen2       | sgo1cob
 | 10/06/2016     | Modified  to calculate MediaID      | hsr9kor
 | 15/09/2016     | Typedef added for first argument of | boc7kor
 |                | ATAPI_IF_RequestData to avoid       |
 |                | lint warning                        |
 |************************************************************************
 |************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
/* General headers */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/cdrom.h>
#include <mntent.h>

/* Basic OSAL includes */
//#define OSAL_S_IMPORT_INTERFACE_GENERIC
//#include "osal_if.h"
#include "trace_interface.h"
#include "ostrace.h"
#include "../../atapi_if/include/atapi_if.h"
#include "cdctrl.h"
#include "cdctrl_trace.h"



/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define CDCTRL_DEBUG_DUMP           // Define this for debug output to console
#define CDCTRL_IS_USING_LOCAL_TRACE
#ifndef CDCTRL_DEBUG_DUMP
#define CDCTRL_DEBUG_PRINTF(...)
#else //CDCTRL_DEBUG_DUMP
#define CDCTRL_DEBUG_PRINTF(...)    printf(__VA_ARGS__)
#endif //CDCTRL_DEBUG_DUMP


#define CDCTRL_DRIVE_MAJOR_VER_NUMBER 0xF000
#define CDCTRL_DRIVE_MINOR_VER_NUMBER 0x0001

#define CDCTRL_LSIM_TEMP 170
#define CDCTRL_FP_LEN 4  /* number of 32bit FingerPrints*/
//#define CD_CTRL_READ_DRIVE_INFO_FROM_REG
/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable inclusion  (scope: global)
|-----------------------------------------------------------------------*/
extern tBool CDCTRL_bTraceOn;

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static OSAL_tSemHandle      CDCTRLSemHandle[ATAPI_IF_DRIVE_NUMBER] = { OSAL_ERROR };
static OSAL_tShMemHandle    CDCTRLShMemHandle[ATAPI_IF_DRIVE_NUMBER] = { OSAL_ERROR };

static ATAPI_IF_PROC_INFO   *CDCTRLProcInfo[ATAPI_IF_DRIVE_NUMBER] = { NULL };

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: global)
|-----------------------------------------------------------------------*/



/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
#ifdef CD_CTRL_READ_DRIVE_INFO_FROM_REG
// Now vendor-id, software as well as hardware version read by ATAPI_IF_u32Inquiry() API
// Project wants to read it from registry please reuse the below function
static tS32 GetDriveInfoFromReg(tU8 *VendorID, tU8 *HWVersion, tU8 *SWVersion)
{
    if(!VendorID || !HWVersion || !SWVersion)
        return OSAL_ERROR;

    *VendorID = CDCTRL_MASCA_VENDOR_ID_UNKNOWN;
    *HWVersion = 1;
    *SWVersion = 1;

    OSAL_tIODescriptor  fd = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/LSIM/DEV_CD", OSAL_EN_READONLY);

    // It is not a problem and not an error if access to the registry fails. Defaults will be used.
    if(fd != OSAL_ERROR)
    {
        OSAL_trIOCtrlRegistry   entry;
        tS32                    val;

        entry.pcos8Name = "VENDOR_ID";
        entry.u32Size   = sizeof(val);
        entry.s32Type   = OSAL_C_S32_VALUE_S32;
        entry.ps8Value  = (tU8 *)&val;
        if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
            *VendorID = (tU8)(*(tS32 *)entry.ps8Value);

        entry.pcos8Name = "HW_VERSION";
        entry.u32Size   = sizeof(val);
        entry.s32Type   = OSAL_C_S32_VALUE_S32;
        entry.ps8Value  = (tU8 *)&val;
        if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
            *HWVersion = (tU8)(*(tS32 *)entry.ps8Value);

        entry.pcos8Name = "SW_VERSION";
        entry.u32Size   = sizeof(val);
        entry.s32Type   = OSAL_C_S32_VALUE_S32;
        entry.ps8Value  = (tU8 *)&val;
        if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
            *SWVersion = (tU8)(*(tS32 *)entry.ps8Value);
        CDCTRL_PRINTF_U4("/dev/registry/LOCAL_MACHINE/LSIM/DEV_CD open success:vendor=%x,HW=%x,SW=%x\n",*VendorID,*HWVersion,*SWVersion);
        OSAL_s32IOClose (fd);
    }
    else
    {
   	 CDCTRL_PRINTF_U4("/dev/registry/LOCAL_MACHINE/LSIM/DEV_CD open error\n");
    }
    return OSAL_OK;
}
#endif
/*********************************************************************
| function:  	 CDCTRL_u32Init
|---------------------------------------------------------------------
| parameters: 	 none
| returnvalue:
|              	OSAL_E_NOERROR -In case of Success
|---------------------------------------------------------------------
| description:   Register cdctrl trace handler for debugging purpose
|				as a part of initialization
|---------------------------------------------------------------------
| history       Ported from Gen2 by sgo1cob
\*********************************************************************/
tU32 CDCTRL_u32Init(void)
{
#ifdef CDCTRL_IS_USING_LOCAL_TRACE
    CDCTRL_vRegTrace();
#endif
    return OSAL_E_NOERROR;
}

/*********************************************************************
| function:  	 CDCTRL_u32Destroy
|---------------------------------------------------------------------
| parameters: 	 none
| returnvalue:
|              	OSAL_E_NOERROR -In case of Success
|---------------------------------------------------------------------
| description:   unregister cdctrl trace handler
|				as a part of de-initialization
|---------------------------------------------------------------------
| history       Ported from Gen2 by sgo1cob
\*********************************************************************/

tU32 CDCTRL_u32Destroy(void)
{
#ifdef CDCTRL_IS_USING_LOCAL_TRACE
    CDCTRL_vUnregTrace();
#endif
    return OSAL_E_NOERROR;
}

/************************************************************************
 *
 * FUNCTIONS
 *      iGetFingerPrint
 *
 * DESCRIPTION
 *      Calculates a simple (xored) 32-characters-fingerprint of a given buffer
 *      If no source buffer is given, the fingerprint is a random number.
 *
 * Parameters
 *      OUT pcFingerPrint: output buffer for fingerprint
 *      IN  nFingerPrintSize: number of characters in *pcFingerPrint-buffer
 *      IN  pcucBuffer: buffer to be calculated; if NULL, a random fingerprint
 *                      is generated
 *      IN  nBufferLen: size of *pcucBuffer; if 0, a random fingerprint is
 *                      generated
 *
 * OUTPUTS
 *      pcFingerPrint: a fingerprint of max 32 characters, for example:
 *                     "5AD0DDBD94949EA56A15AF8CB5358FB5"
 *                     This string is NOT zero terminated!
 *
 * Return
 *      Number of characters written to pcFingerPrint 
 *
 *HISTORY: 10.06.2016, initial version taken from ARM code, hsr9kor
 *
 ************************************************************************/
static int iGetFingerPrint(char *pcFingerPrint,
                           size_t nFingerPrintSize,
                           const unsigned char *pcucBuffer,
                           size_t nBufferLen)
{
  unsigned int uiFPBin[CDCTRL_FP_LEN];
  unsigned int *puiBuffer;
  size_t nuiBufferSize = nBufferLen / sizeof(unsigned int);
  unsigned int uiBuf, uiFP;
  size_t nTotalLen;

  CDCTRL_TRACE_ENTER_U2(CDID_CDCTRL_iGetFingerPrint,
                        (tU32)pcFingerPrint, nFingerPrintSize,
                        (tU32)pcucBuffer, nBufferLen);

  memset(uiFPBin, 0, sizeof(uiFPBin));
  puiBuffer = (unsigned int *)(void*)pcucBuffer;

  // xor buffer
  if(puiBuffer && (nBufferLen > 0))
  {
    for(uiBuf = 0 ; uiBuf < (nuiBufferSize - CDCTRL_FP_LEN) ; uiBuf++)
    {
      for (uiFP = 0; uiFP < CDCTRL_FP_LEN; uiFP++)
      {
        uiFPBin[uiFP] ^= puiBuffer[uiBuf + uiFP];
        //uiFPBin[uiFP] += puiBuffer[uiBuf + uiFP];
      }
    }
  }
  else //if(puiBuffer && (nBufferLen > 0))
  { // given buffer pointer is NULL or date size == 0,
    // simulate a changed fingerprint
    srand((unsigned int)time(NULL));
    for (uiFP = 0; uiFP < CDCTRL_FP_LEN; uiFP++)
    {
      uiFPBin[uiFP] = (unsigned int)rand();
    }
  } //else //if(puiBuffer && (nBufferLen > 0))

  // make text-hash
  nTotalLen=0;
  if(pcFingerPrint)
  {    
    for(uiFP = 0; uiFP < CDCTRL_FP_LEN; uiFP++)
    {
      char szTmp[32];
      size_t nLen;
      sprintf(szTmp, "%08X", uiFPBin[uiFP]);
      nLen = strlen(szTmp);
      if((nTotalLen+nLen) <= nFingerPrintSize)
      {
        memcpy(&pcFingerPrint[nTotalLen], szTmp, nLen);
        nTotalLen += nLen;
      }
    }
  } //if(pcFingerPrint)

  CDCTRL_TRACE_LEAVE_U2(CDID_CDCTRL_iGetFingerPrint,
                        OSAL_E_NOERROR, nTotalLen, 0, 0, 0);
  return (int)nTotalLen;
}

/*********************************************************************
| function:  	 CDCTRL_u32IOOpen
|---------------------------------------------------------------------
| parameters: 	 tS32 s32DriveIndex
| returnvalue:
|				Error code 	 -  In case of failure
|              	OSAL_E_NOERROR -In case of Success
|---------------------------------------------------------------------
| description:   Open/create resources required by CDCTRL
|---------------------------------------------------------------------
| history       Ported from Gen2 by sgo1cob
\*********************************************************************/

tU32 CDCTRL_u32IOOpen(tS32 s32DriveIndex)
{
    tChar   semname[] = ATAPI_IF_SEM_NAME;
    tChar   shmemname[] = ATAPI_IF_SHMEM_NAME;
    tU32    u32Result = OSAL_E_NOERROR;

    CDCTRL_TRACE_ENTER_U3(CDCTRL_u32IOOpen,(tU32)s32DriveIndex,0,0,0);

    semname[sizeof(ATAPI_IF_SEM_NAME) - 2] = '0' + (tChar)s32DriveIndex;
    shmemname[sizeof(ATAPI_IF_SHMEM_NAME) - 2] = '0' + (tChar)s32DriveIndex;
    CDCTRLShMemHandle[s32DriveIndex] = OSAL_SharedMemoryOpen(shmemname, OSAL_EN_READWRITE);
    if(CDCTRLShMemHandle[s32DriveIndex] == OSAL_ERROR)
    {
        u32Result = OSAL_E_NOSPACE;
    }

    if(u32Result != OSAL_ERROR)
    {
        CDCTRLProcInfo[s32DriveIndex] = (ATAPI_IF_PROC_INFO *)OSAL_pvSharedMemoryMap(CDCTRLShMemHandle[s32DriveIndex], OSAL_EN_READWRITE, sizeof(ATAPI_IF_PROC_INFO), 0);
        if(CDCTRLProcInfo[s32DriveIndex] == NULL)
        {
            u32Result = OSAL_E_NOSPACE;
            //Clean up in case of failure
            OSAL_s32SharedMemoryClose(CDCTRLShMemHandle[s32DriveIndex]);
            CDCTRLShMemHandle[s32DriveIndex] = OSAL_ERROR;
        }
    }

    if(u32Result == OSAL_E_NOERROR && OSAL_s32SemaphoreOpen(semname, &CDCTRLSemHandle[s32DriveIndex]) != OSAL_OK)
    {
        u32Result = OSAL_E_NOSPACE;
        //Clean up in case of failure
        OSAL_s32SharedMemoryUnmap(CDCTRLProcInfo[s32DriveIndex], sizeof(ATAPI_IF_PROC_INFO));
        CDCTRLProcInfo[s32DriveIndex] = NULL;
        OSAL_s32SharedMemoryClose(CDCTRLShMemHandle[s32DriveIndex]);
        CDCTRLShMemHandle[s32DriveIndex] = OSAL_ERROR;
    }

    CDCTRL_TRACE_LEAVE_U3(CDCTRL_u32IOOpen,
                          u32Result,
                          (tU32)s32DriveIndex,
                          0,0,0);

    return u32Result;
}

/*********************************************************************
| function:  	 CDCTRL_u32IOClose
|---------------------------------------------------------------------
| parameters: 	 tS32 s32DriveIndex
| returnvalue:
|              	OSAL_E_NOERROR -In case of Success
|---------------------------------------------------------------------
| description:   Delete/close resources created by CDCTRL_u32IOOpen()
|---------------------------------------------------------------------
| history       Ported from Gen2 by sgo1cob
\*********************************************************************/

tU32 CDCTRL_u32IOClose(tS32 s32DriveIndex)
{
    tU32 u32Result = OSAL_E_NOERROR;
    CDCTRL_TRACE_ENTER_U3(CDCTRL_u32IOClose,(tU32)s32DriveIndex,0,0,0);

    if(CDCTRLProcInfo[s32DriveIndex] != NULL)
    {
        OSAL_s32SharedMemoryUnmap(CDCTRLProcInfo[s32DriveIndex], sizeof(ATAPI_IF_PROC_INFO));
        CDCTRLProcInfo[s32DriveIndex] = NULL;
    }
    if(CDCTRLShMemHandle[s32DriveIndex] != OSAL_ERROR)
    {
        OSAL_s32SharedMemoryClose(CDCTRLShMemHandle[s32DriveIndex]);
        CDCTRLShMemHandle[s32DriveIndex] = OSAL_ERROR;
    }
    if(CDCTRLSemHandle[s32DriveIndex] != OSAL_ERROR)
    {
        OSAL_s32SemaphoreClose(CDCTRLSemHandle[s32DriveIndex]);
        CDCTRLSemHandle[s32DriveIndex] = OSAL_ERROR;
    }

    CDCTRL_TRACE_LEAVE_U3(CDCTRL_u32IOClose,u32Result,(tU32)s32DriveIndex,0,0,0);
    return u32Result;
}


/*********************************************************************
| function:  	 CDCTRL_u32IOControl
|---------------------------------------------------------------------
| parameters:
|				tS32 s32DriveIndex - driver index
|				tS32 s32Fun - IOControl operation code
|				tS32 s32Arg - Argument according to IOControl operation code
| returnvalue:
|              	OSAL_E_NOERROR -In case of Success
|				Error code 	 -  In case of failure
|---------------------------------------------------------------------
| description:   Various operation performed by CDCTRL_u32IOControl()
|---------------------------------------------------------------------
| history       Ported from Gen2 by sgo1cob
|               Modified OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO to calculate szcMediaID, 
|               using iGetFingerPrint method, by hsr9kor
|				Typedef added for first argument of ATAPI_IF_RequestData 
|               to avoid lint warning by boc7kor   
|
\*********************************************************************/

tU32 CDCTRL_u32IOControl(tS32 s32DriveIndex, tS32 s32Fun, tS32 s32Arg)
{
    tU32 u32RetVal = OSAL_E_NOERROR;

    CDCTRL_TRACE_ENTER_U4(CDCTRL_u32IOControl,
                          (tU32)s32DriveIndex,
                          (tU32)s32Fun,
                          (tU32)s32Arg,
                          0);

    CDCTRL_TRACE_IOCTRL(CDCTRL_EN_IOCONTROL,
                        (tU32)s32DriveIndex,
                        (tU32)s32Fun,
                        (tU32)s32Arg,0);


    switch(s32Fun)
    {

    case OSAL_C_S32_IOCTRL_VERSION:
        if((void*)s32Arg == NULL)
        {
            u32RetVal = OSAL_E_INVALIDVALUE;
        }
        else
        {
            *(tPS32)s32Arg = CDCTRL_C_S32_IO_VERSION;
            CDCTRL_TRACE_IOCTRL(CDCTRL_EN_IOCTRL_VERSION,
                                (tU32)s32DriveIndex,
                                (tU32)(*(tPS32)s32Arg),
                                0,
                                0);
        }
        break;

    case OSAL_C_S32_IOCTRL_CDCTRL_GETLOADERINFO:
        {
            OSAL_trLoaderInfo* prInfo = (OSAL_trLoaderInfo*)s32Arg;

            if( prInfo == NULL )
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            /*
             * No need to use semaphore wait since it didnt use shared memory.
             */
            else
            {
#if 0
               /*Below funtionality will also work to get info about loader status,
               but NO_DISK status coming before actually validating the loader status
               */
               switch(CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus)
               {
               case AIF_LDR_STATUS_NO_DISK:
                  prInfo->u8LoaderInfoByte=OSAL_C_U8_NO_MEDIA;
                  break;
               case AIF_LDR_STATUS_CD_INSIDE:
               case AIF_LDR_STATUS_CD_PLAYABLE:
               case AIF_LDR_STATUS_CD_UNREADABLE:
                  prInfo->u8LoaderInfoByte=OSAL_C_U8_MEDIA_INSIDE;
                  break;
               case AIF_LDR_STATUS_IN_SLOT:
               case AIF_LDR_STATUS_EJECTED:
                  prInfo->u8LoaderInfoByte=OSAL_C_U8_MEDIA_IN_SLOT;
                  break;
               default:
                  prInfo->u8LoaderInfoByte=CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus;
                  break;
               }
#endif

               tS32 s32Result;
               int     cdhandle = open(ATAPI_IF_LINUX_DEVICE_NAME_STRING, O_RDONLY | O_NONBLOCK);
               if(cdhandle >= 0)
               {

                  s32Result = ioctl(cdhandle, CDROM_DRIVE_STATUS, 0);
                  switch(s32Result)
                  {
                  case CDS_DISC_OK:
                     prInfo->u8LoaderInfoByte = OSAL_C_U8_MEDIA_INSIDE;
                     CDCTRL_PRINTF_U4("CDS_DISC_OK\n");
                     break;
                  case CDS_NO_INFO:
                  case CDS_NO_DISC:
                     prInfo->u8LoaderInfoByte = OSAL_C_U8_NO_MEDIA;
                     CDCTRL_PRINTF_U4("CDS_NO_DISC\n");
                     break;
                  case CDS_TRAY_OPEN:
                     prInfo->u8LoaderInfoByte = OSAL_C_U8_MEDIA_IN_SLOT;
                     CDCTRL_PRINTF_U4("CDS_TRAY_OPEN\n");
                     break;
                  case CDS_DRIVE_NOT_READY:
                     prInfo->u8LoaderInfoByte = OSAL_C_U8_EJECT_IN_PROGRESS;
                     CDCTRL_PRINTF_U4("CDS_DRIVE_NOT_READY\n");
                     break;
                  default:
                     prInfo->u8LoaderInfoByte = (tU8)s32Result;
                     u32RetVal = OSAL_E_UNKNOWN;
                     CDCTRL_PRINTF_U4("unknown state errno=%x(%d)\n%s\n",errno,errno,strerror(errno));
                     break;
                  }
                  close(cdhandle);
               }
               else
               {
                  u32RetVal = OSAL_E_UNKNOWN;
               }


               CDCTRL_TRACE_IOCTRL(CDCTRL_EN_LOADER_INFO,
                                    (tU32)s32DriveIndex,
                                    (tU32)prInfo->u8LoaderInfoByte,
                                    0,
                                    0);

            }
        }
        break;
    case OSAL_C_S32_IOCTRL_CDCTRL_GET_DRIVE_VERSION:
        {
            OSAL_trCDDriveVersion* prVersion = (OSAL_trCDDriveVersion*)s32Arg;

            if(prVersion == NULL)
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else
            {
            	tU8 u8SW=0, u8HW=0, u8Vendor[9]={0},u8Product[17]={0};
            	if (OSAL_E_NOERROR==ATAPI_IF_u32CacheGetVersion(CDCTRLProcInfo[s32DriveIndex], &u8HW, &u8SW,u8Vendor,u8Product))
            	{

                    prVersion->u32SWVersion = u8SW;
                    prVersion->u32HWVersion = u8HW;

                    CDCTRL_TRACE_IOCTRL(CDCTRL_EN_DRIVE_VERSION,
                                        (tU32)s32DriveIndex,
                                        prVersion->u32SWVersion,
                                        prVersion->u32HWVersion,
                                        0);
                    CDCTRL_PRINTF_U4("product=%s,Vendor=%s\n",u8Product,u8Vendor);
            	}
            	else
            	{
            		CDCTRL_PRINTF_U4("OSAL_C_S32_IOCTRL_CDCTRL_GET_DRIVE_VERSION invalid\n");
            	}
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDCTRL_GETDRIVEVERSION:
        {
            OSAL_trDriveVersion *prDriveVersion = (OSAL_trDriveVersion*)s32Arg;
            static tU8 u8HWCount = 11;

            if( prDriveVersion == NULL )
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else
            {
                tU8 u8SW=0, u8HW=0;
#ifdef CD_CTRL_READ_DRIVE_INFO_FROM_REG
                tU8 u8Vendor = 0;
                if(GetDriveInfoFromReg(&u8Vendor, &u8HW, &u8SW) == OSAL_ERROR)
                    u32RetVal = OSAL_ERROR;
                else
#endif
                {
                    const char *pcszVendor = "-";
#ifdef CD_CTRL_READ_DRIVE_INFO_FROM_REG
                    switch(u8Vendor)
                    {
                    case CDCTRL_MASCA_VENDOR_ID_NOT_AVAILABLE:
                        u32RetVal = OSAL_E_TEMP_NOT_AVAILABLE;
                        pcszVendor = "n.a.";
                        break;
                    case CDCTRL_MASCA_VENDOR_ID_UNKNOWN:
                        pcszVendor = "unknown";
                        break;
                    case CDCTRL_MASCA_VENDOR_ID_TANASHIN:
                        pcszVendor = "Tanashin";
                        break;
                    case CDCTRL_MASCA_VENDOR_ID_KENWOOD:
                        pcszVendor = "Kenwood";
                        break;
                    case CDCTRL_MASCA_VENDOR_ID_PIONEER:
                        pcszVendor = "Pioneer";
                        break;
                    default:
                        pcszVendor = "undefined";
                        u32RetVal = OSAL_E_TEMP_NOT_AVAILABLE;
                        break;
                    } //switch(u8Vendor)
#else
                 	tU8 u8Vendor[9]={0},u8Product[17]={0};
                 	if (OSAL_E_NOERROR==ATAPI_IF_u32CacheGetVersion(CDCTRLProcInfo[s32DriveIndex], &u8HW, &u8SW,u8Vendor,u8Product))
                 	{

                         pcszVendor = (char *)u8Vendor;

                         CDCTRL_PRINTF_U4("product=%s,Vendor=%s\n",u8Product,u8Vendor);
                 	}

#endif
                    // prDriveVersion->au8FirmwareRevision  // 8
                    memset( prDriveVersion->au8FirmwareRevision,   // fill whole array with space
                            ' ',
                            sizeof(prDriveVersion->au8FirmwareRevision) );

                    (void)snprintf( (tChar *)prDriveVersion->au8FirmwareRevision,
                              sizeof(prDriveVersion->au8FirmwareRevision)-1, "%u",
                                     (unsigned int)u8SW);

                    // prDriveVersion->au8ModelNumber  // 40
                    memset( prDriveVersion->au8ModelNumber,   // fill whole array with space
                            ' ',
                            sizeof(prDriveVersion->au8ModelNumber) );

                    if(u8HWCount != 1)
                    {
                        (void)snprintf( (tChar *)prDriveVersion->au8ModelNumber,
                              sizeof(prDriveVersion->au8ModelNumber)-1, "%s HW%u",
                              pcszVendor, (unsigned int)u8HW);
                    }
                    else //if(u8HWCount > 0)
                    {
                        //debug only for NISSAN - on 10th call of VERSIOn from System-Menu, returns SW-Version instead of HW-Version
                        (void)snprintf( (tChar *)prDriveVersion->au8ModelNumber,
                                  sizeof(prDriveVersion->au8ModelNumber)-1,
                                  "%s SW%u", pcszVendor, (unsigned int)u8SW);
                    } //else //if(u8HWCount > 0)

                    if(u8HWCount > 0)
                    {
                        u8HWCount--;
                    } //if(u8HWCount > 0)


                    // prDriveVersion->au8SerialNumber  // 20
                    memset( prDriveVersion->au8SerialNumber,
                            '0',
                            sizeof(prDriveVersion->au8SerialNumber) );

                    prDriveVersion->u16MajorVersionNumber = CDCTRL_DRIVE_MAJOR_VER_NUMBER;
                    prDriveVersion->u16MinorVersionNumber = CDCTRL_DRIVE_MINOR_VER_NUMBER;

                    CDCTRL_TRACE_IOCTRL(CDCTRL_EN_DRIVE_VERSION_FULL,
                                        (tU32)s32DriveIndex,
                                        (tU32)prDriveVersion->u16MajorVersionNumber,
                                        prDriveVersion->u16MinorVersionNumber,
                                        (tU32)u8Vendor);
                    /*! au8.. are not zero teminated! - could be lead to a crash,
                    if minor/major dont contain 0x00
                    CDCTRL_PRINTF_U1("Firmware [%s], Serial [%s], Model [%s]",
                    (const char*)prDriveVersion->au8FirmwareRevision,
                    (const char*)prDriveVersion->au8SerialNumber,
                    (const char*)prDriveVersion->au8ModelNumber
                    );*/

                    /*
                    if( bTraceLevel2IsActive == TRUE )
                    {
                    au8Buf[0] = EN_DRIVE_VERSION;
                    au8Buf[1] = (tU8) s32DriveIndex;
                    au8Buf[2] = u8SW;
                    au8Buf[3] = u8HW;
                    LLD_vTrace_en( OSAL_C_TR_CLASS_DEV_CDCTRL, TR_LEVEL_USER_2, au8Buf, 4 );
                    }*/
                }

            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDCTRL_EJECTMEDIA:
    {
        // If the cdrom was mounted by Linux, it must be unmounted
        // Get the real name of /dev/cdrom
        int     cdhandle = open(ATAPI_IF_LINUX_DEVICE_NAME_STRING, O_RDONLY | O_NONBLOCK);
        if(cdhandle >= 0)
        {
            char    devname[PATH_MAX]={0};

            if(realpath(ATAPI_IF_LINUX_DEVICE_NAME_STRING, devname) != NULL)
            {
            	CDCTRL_PRINTF_U4("CDROM real path=%s\n",devname);

                FILE            *file;
                char            mountname[512] = { 0, };
                struct mntent   *entry;

                file = setmntent("/etc/mtab", "r");
                if(file != NULL)
                {
                    CDCTRL_PRINTF_U4("Resolved \'%s\' to \'%s\'\n", ATAPI_IF_LINUX_DEVICE_NAME_STRING, devname);
                    for(entry = getmntent(file); entry; entry = getmntent(file))
                    {
                    	CDCTRL_PRINTF_U4("mnt point=%s\n",entry->mnt_fsname);
                        if(!strcmp(entry->mnt_fsname, devname))
                        {
                            CDCTRL_PRINTF_U4("Found \'%s\' \'%s\'\n", entry->mnt_fsname, entry->mnt_dir);
                            strncpy(mountname, entry->mnt_dir, sizeof(mountname));
                            mountname[sizeof(mountname)-1]=0;

                        }
                    }
                    endmntent(file);
                }
                else
                    CDCTRL_PRINTF_U4("Failed to read /etc/mtab\n");

                if(mountname[0])
                {
                    tS32    status;

                    CDCTRL_PRINTF_U4("%s is mounted\n", devname);
                    // Spawn a process to unmount the device
                    CDCTRL_PRINTF_U4("Calling umount with \'%s\'\n", mountname);
                    if(fork() == 0)
                        // This is the child process
                        execlp("umount", "umount", mountname, NULL);
                    else
                        // This is the parent process
                        wait(&status);
                    CDCTRL_PRINTF_U4("Status of umount is %i\n", status);
                }
                else
                    CDCTRL_PRINTF_U4("%s is not mounted\n", devname);
            }
            else
                CDCTRL_PRINTF_U4("Could not resolve %s\n", ATAPI_IF_LINUX_DEVICE_NAME_STRING);

            CDCTRL_PRINTF_U4("Eject!\n");

            ioctl(cdhandle, CDROM_LOCKDOOR, 0);
            if(ioctl(cdhandle, CDROMEJECT, 0) < 0)
                u32RetVal = OSAL_ERROR;

            close(cdhandle);
        }
        else
        {
        	CDCTRL_PRINTF_U4("Eject!\n");
        }
        break;
    }

    case OSAL_C_S32_IOCTRL_CDCTRL_CLOSEDOOR:
        {
            int     cdhandle = open(ATAPI_IF_LINUX_DEVICE_NAME_STRING, O_RDONLY | O_NONBLOCK);
            if(cdhandle >= 0)
            {
                if(ioctl(cdhandle, CDROMCLOSETRAY, 0) < 0)
                {
                    u32RetVal = OSAL_ERROR;
                }
                close(cdhandle);
            }
            break;
        }

    case OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP:
        {
            OSAL_trDriveTemperature* prTemp = (OSAL_trDriveTemperature*)s32Arg;

            if( prTemp == NULL )
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else
            {
                tS32 s32Temperature = CDCTRL_LSIM_TEMP;
                prTemp->u8Temperature = (tU8)CDCTRL_LSIM_TEMP;

                CDCTRL_TRACE_IOCTRL(CDCTRL_EN_TEMPERATURE,
                                    (tU32)s32DriveIndex,
                                    (tU32)s32Temperature,
                                    prTemp->u8Temperature,
                                    0);

            }
        } //case OSAL_C_S32_IOCTRL_CDCTRL_GETTEMP:
        break;


    case OSAL_C_S32_IOCTRL_CDCTRL_GETTRACKINFO:
        {
            OSAL_trCDROMTrackInfo   *prInfo = (OSAL_trCDROMTrackInfo *)s32Arg;

            if(prInfo == NULL)
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDCTRLSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                u32RetVal = OSAL_ERROR;
            }
            else if((CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus != AIF_LDR_STATUS_CD_PLAYABLE)&
                  (CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus != AIF_LDR_STATUS_CD_INSIDE))
            {
               switch(CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus )
               {
               case AIF_LDR_STATUS_NO_DISK:
               case AIF_LDR_STATUS_EJECTED:
               case AIF_LDR_STATUS_IN_SLOT:
                  u32RetVal= OSAL_E_MEDIA_NOT_AVAILABLE;
                  CDCTRL_PRINTF_U4("MEDIA NOT AVAILABLE");
                  break;
               case AIF_LDR_STATUS_UNKNOWN:
                  u32RetVal=OSAL_E_UNKNOWN;
                  CDCTRL_PRINTF_U4("MEDIA STATE Unknown");
                  break;
               default:
                  u32RetVal=CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus;
                  CDCTRL_PRINTF_U4("MEDIA STATE code=%x",u32RetVal);
                  break;
               }

               OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
            }
            else
            {
                ATAPI_IF_UpdateProcStatus(CDCTRLProcInfo[s32DriveIndex]);

                if(!CDCTRLProcInfo[s32DriveIndex]->CDCache.ValidFlag)
                {
                    u32RetVal = OSAL_ERROR;
                }
                else if(prInfo->u32TrackNumber < CDCTRLProcInfo[s32DriveIndex]->CDCache.MinTrck)
                {
                    u32RetVal = OSAL_E_INVALIDVALUE;
                }
                else if(prInfo->u32TrackNumber > CDCTRLProcInfo[s32DriveIndex]->CDCache.MaxTrck)
                {
                    u32RetVal = OSAL_E_INVALIDVALUE;
                }
                else
                {
                    if(ATAPI_IF_ADDITIONALCDINFO_IS_DATA_TRACK(CDCTRLProcInfo[s32DriveIndex]->CDCache.DataTrackInfo, prInfo->u32TrackNumber))
                    {
                        prInfo->u32TrackControl = 0x40;
                    }
                    else
                    {
                        prInfo->u32TrackControl = 0x00;
                    }
                    prInfo->u32LBAAddress = CDCTRLProcInfo[s32DriveIndex]->CDCache.TrckStartLBA[prInfo->u32TrackNumber - 1];

                    CDCTRL_TRACE_IOCTRL(CDCTRL_EN_GETTRACKINFO,
                                        (tU32)s32DriveIndex,
                                        prInfo->u32TrackNumber,
                                        prInfo->u32LBAAddress,
                                        prInfo->u32TrackControl);
                }

                OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);

            }
        }
        break;


    case OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATAUNCACHED:
    case OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA:
        {
            OSAL_trReadRawInfo  *prRRInfo = (OSAL_trReadRawInfo *)s32Arg;

            if(prRRInfo == NULL)
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else if(prRRInfo->ps8Buffer == NULL)
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDCTRLSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                u32RetVal = OSAL_ERROR;
            }
            else if((CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus != AIF_LDR_STATUS_CD_PLAYABLE)&
                  (CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus != AIF_LDR_STATUS_CD_INSIDE))
            {
               switch(CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus )
               {
               case AIF_LDR_STATUS_NO_DISK:
               case AIF_LDR_STATUS_EJECTED:
               case AIF_LDR_STATUS_IN_SLOT:
                  u32RetVal= OSAL_E_MEDIA_NOT_AVAILABLE;
                  CDCTRL_PRINTF_U4("MEDIA NOT AVAILABLE");
                  break;
               case AIF_LDR_STATUS_UNKNOWN:
                  u32RetVal=OSAL_E_UNKNOWN;
                  CDCTRL_PRINTF_U4("MEDIA STATE Unknown");
                  break;
               default:
                  u32RetVal=CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus;
                  CDCTRL_PRINTF_U4("MEDIA STATE code=%x",u32RetVal);
                  break;
               }

               OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
            }

            else
            {
                tU32    startlba = prRRInfo->u32LBAAddress;
                int     len = prRRInfo->u32NumBlocks;

                while(len > 0)
                {
                    int numframes = len, result;
                    if(numframes > ATAPI_IF_DATA_REQUEST_BUFF_CDFRAMES)
                        numframes = ATAPI_IF_DATA_REQUEST_BUFF_CDFRAMES;
                    //typedef added to avoid lint warning
                    result = ATAPI_IF_RequestData((tU8 *)prRRInfo->ps8Buffer, CDCTRLProcInfo[s32DriveIndex], CDCTRLSemHandle[s32DriveIndex], startlba, numframes);
                    if(result <= 0)// || result != numframes)
                    {
                        u32RetVal = OSAL_ERROR;
                        break;
                    }

                    startlba += result;
                    len -= result;
                }

                CDCTRL_TRACE_IOCTRL(CDCTRL_EN_READ_RAW_LBA,
                                    (tU32)s32DriveIndex,
                                    prRRInfo->u32NumBlocks,
                                    prRRInfo->u32LBAAddress,
                                    (tU32)prRRInfo->ps8Buffer);
                OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDCTRL_READRAWDATA_MSF:
        {
            OSAL_trReadRawMSFInfo   *pMSFInfo = (OSAL_trReadRawMSFInfo *)s32Arg;

            if(pMSFInfo == NULL)
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else if(pMSFInfo->ps8Buffer == NULL)
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDCTRLSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                u32RetVal = OSAL_ERROR;
            }

            else if((CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus != AIF_LDR_STATUS_CD_PLAYABLE))
            {
            	switch(CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus )
            	{
            	case AIF_LDR_STATUS_NO_DISK:
            	case AIF_LDR_STATUS_EJECTED:
            	case AIF_LDR_STATUS_IN_SLOT:
            		u32RetVal= OSAL_E_MEDIA_NOT_AVAILABLE;
            		CDCTRL_PRINTF_U4("MEDIA NOT AVAILABLE");
            		break;
            	case AIF_LDR_STATUS_UNKNOWN:
            		u32RetVal=OSAL_E_UNKNOWN;
            		CDCTRL_PRINTF_U4("MEDIA STATE Unknown");
            		break;
            	default:
            		u32RetVal=CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus;
            		CDCTRL_PRINTF_U4("MEDIA STATE code=%x",u32RetVal);
            		break;
            	}

            	OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
            }

            else
            {
                tU32    startlba = ATAPI_IF_MSF2LBA(pMSFInfo->u8Minute, pMSFInfo->u8Second, pMSFInfo->u8Frame);
                int     len = pMSFInfo->u32NumBlocks;

                while(len > 0)
                {
                    int numframes = len, result;
                    if(numframes > ATAPI_IF_DATA_REQUEST_BUFF_CDFRAMES)
                        numframes = ATAPI_IF_DATA_REQUEST_BUFF_CDFRAMES;
                    //typedef added to avoid lint warning
                    result = ATAPI_IF_RequestData((tU8 *)pMSFInfo->ps8Buffer, CDCTRLProcInfo[s32DriveIndex], CDCTRLSemHandle[s32DriveIndex], startlba, numframes);
                    if(result <= 0)// || result != numframes)
                    {
                        u32RetVal = OSAL_ERROR;
                        break;
                    }

                    startlba += result;
                    len -= result;
                }

                CDCTRL_TRACE_IOCTRL(CDCTRL_EN_READ_RAW_MSF,
                                    (tU32)s32DriveIndex,
                                    pMSFInfo->u32NumBlocks,
                                      ((tU32)pMSFInfo->u8Minute) << 24
                                    | ((tU32)pMSFInfo->u8Second) << 16
                                    | ((tU32)pMSFInfo->u8Frame),
                                    (tU32)pMSFInfo->ps8Buffer
                                   );
                OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
            }
        }
        break;


    case OSAL_C_S32_IOCTRL_CDCTRL_GETCDINFO:
        {
            OSAL_trCDInfo   *prCDInfo = (OSAL_trCDInfo *)s32Arg;

            if(prCDInfo == NULL)
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDCTRLSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                u32RetVal = OSAL_ERROR;
            }
            else if((CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus != AIF_LDR_STATUS_CD_PLAYABLE)&
                  (CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus != AIF_LDR_STATUS_CD_INSIDE))
            {
            	switch(CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus )
            	{
            	case AIF_LDR_STATUS_NO_DISK:
            	case AIF_LDR_STATUS_EJECTED:
            	case AIF_LDR_STATUS_IN_SLOT:
            		u32RetVal= OSAL_E_MEDIA_NOT_AVAILABLE;
            		CDCTRL_PRINTF_U4("MEDIA NOT AVAILABLE");
            		break;
            	case AIF_LDR_STATUS_UNKNOWN:
            		u32RetVal=OSAL_E_UNKNOWN;
            		CDCTRL_PRINTF_U4("MEDIA STATE Unknown");
            		break;
            	default:
            		u32RetVal=(tU32)CDCTRLProcInfo[s32DriveIndex]->MediaStatus.InternelStatus;
            		CDCTRL_PRINTF_U4("MEDIA STATE code=%x",u32RetVal);
            		break;
            	}

            	OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
            }
            else
            {
                ATAPI_IF_UpdateProcStatus(CDCTRLProcInfo[s32DriveIndex]);

                if(!CDCTRLProcInfo[s32DriveIndex]->CDCache.ValidFlag)
                {
                    u32RetVal = OSAL_ERROR;
                }
                else
                {
                    prCDInfo->u32MinTrack = CDCTRLProcInfo[s32DriveIndex]->CDCache.MinTrck;
                    prCDInfo->u32MaxTrack = CDCTRLProcInfo[s32DriveIndex]->CDCache.MaxTrck;
                }

                CDCTRL_TRACE_IOCTRL(CDCTRL_EN_CD_INFO,
                                    (tU32)s32DriveIndex,
                                    prCDInfo->u32MinTrack,
                                    prCDInfo->u32MaxTrack,
                                    CDCTRLProcInfo[s32DriveIndex]->CDCache.ValidFlag);
                OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDCTRL_GETMEDIAINFO:
        {
            OSAL_trMediaInfo    *prInfo = (OSAL_trMediaInfo *)s32Arg;

            if(prInfo == NULL)
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDCTRLSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                u32RetVal = OSAL_ERROR;
            }
            else
            {
                if(!CDCTRLProcInfo[s32DriveIndex]->CDCache.ValidFlag)
                {
                   prInfo->u8MediaType = OSAL_C_U8_UNKNOWN_MEDIA;
                   prInfo->u8FileSystemType = OSAL_C_U8_FS_TYPE_UNKNOWN;
                }
                else
                {
                    prInfo->u8MediaType = CDCTRLProcInfo[s32DriveIndex]->CDCache.MediaType;
                    prInfo->u8FileSystemType = CDCTRLProcInfo[s32DriveIndex]->CDCache.FileSystemType;

                   // memset(prInfo->szcMediaID, 0, sizeof(prInfo->szcMediaID));
                   //strncpy(prInfo->szcMediaID, CDCTRLProcInfo[s32DriveIndex]->CDCache.MediaCatNum, sizeof(prInfo->szcMediaID) - 1);

                    prInfo->nTimeZone = 0;
                    strncpy(prInfo->szcCreationDate, "00.00.0000", sizeof(prInfo->szcCreationDate) - 1);
                    
                    if (prInfo->u8MediaType == OSAL_C_U8_AUDIO_MEDIA)
                    { 
                       ATAPI_IF_CDCACHE *pFP; 
                       size_t nBufferLen = sizeof(ATAPI_IF_CDCACHE); // size of hash buffer

                       pFP = (ATAPI_IF_CDCACHE*)malloc(nBufferLen);
                       if(pFP != NULL)
                       {
                          memset(pFP, 0, nBufferLen);
                          memcpy(pFP, &(CDCTRLProcInfo[s32DriveIndex]->CDCache), sizeof(ATAPI_IF_CDCACHE));
                       }  
                       int iFingerPrintLen = iGetFingerPrint(
                                                              prInfo->szcMediaID,
                                                              sizeof(prInfo->szcMediaID),
                                                              (unsigned char *)pFP,
                                                               nBufferLen);
                       CDCTRL_PRINTF_U1("FingerPrint [%d]",iFingerPrintLen);
                       free(pFP);

                       char szTmp[33];
                       memcpy(szTmp, prInfo->szcMediaID, 32);
                       szTmp[32]='\0';

                       memcpy(prInfo->szcMediaID, szTmp, 32);
                     //  prInfo->szcMediaID[32]='\0';
                    }
                }

                CDCTRL_TRACE_IOCTRL(CDCTRL_EN_MEDIA_INFO,
                                    (tU32)s32DriveIndex,
                                    (tU32)prInfo->u8MediaType,
                                    (tU32)prInfo->u8FileSystemType,
                                    (tU32)prInfo->nTimeZone);
                CDCTRL_PRINTF_U1("CreationDate [%s], MediaID [%s]",
                                 (const char*)prInfo->szcCreationDate,
                                 (const char*)prInfo->szcMediaID);
                OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
            }
        }
        break;


    case OSAL_C_S32_IOCTRL_CDCTRL_GETDEVICEINFO: // get media device type!
        if((void*)s32Arg != NULL)
        {
            *(tPS32)s32Arg = (tU8)OSAL_EN_MEDIADEV_CD;
            CDCTRL_TRACE_IOCTRL(CDCTRL_EN_GETDEVICEINFO,
                                (tU32)s32DriveIndex,
                                (tU32)*(tPS32)s32Arg,
                                0,
                                0);
        }
        else
        {
            u32RetVal = OSAL_E_INVALIDVALUE;
        }
        break;

    case OSAL_C_S32_IOCTRL_CDCTRL_GETDISKTYPE:
        {
            OSAL_trDiskType *prDiskType = (OSAL_trDiskType *)s32Arg;

            if(prDiskType == NULL )
            {
                u32RetVal = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDCTRLSemHandle[s32DriveIndex], 
                                          OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                u32RetVal = OSAL_ERROR;
            }

            else
            {
               if((CDCTRLProcInfo[s32DriveIndex]->CDCache.MediaType == 
                                                      OSAL_C_U8_AUDIO_MEDIA 
                   || CDCTRLProcInfo[s32DriveIndex]->CDCache.MediaType == 
                        OSAL_C_U8_DATA_MEDIA) && 
                     (CDCTRLProcInfo[s32DriveIndex]->CDCache.ValidFlag))
               {
                  prDiskType->u8DiskType = ATAPI_C_U8_DISK_TYPE_CD;
                  prDiskType->u8DiskSubType = ATAPI_C_U8_DISK_SUB_TYPE_UNKNOWN;
               }
               else
               {
                  prDiskType->u8DiskType    = ATAPI_C_U8_DISK_TYPE_UNKNOWN;
                  prDiskType->u8DiskSubType = ATAPI_C_U8_DISK_SUB_TYPE_UNKNOWN;
               }

               CDCTRL_TRACE_IOCTRL(CDCTRL_EN_GETDISKTYPE,
               (tU32)s32DriveIndex,
               (tU32)prDiskType->u8DiskType,
               (tU32)prDiskType->u8DiskSubType,
               0);
               OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
            }
        }
        break;
    case OSAL_C_S32_IOCTRL_CDCTRL_SETMOTORON:
    case OSAL_C_S32_IOCTRL_CDCTRL_SETMOTOROFF:
    case OSAL_C_S32_IOCTRL_CDCTRL_SETPOWEROFF:
        u32RetVal = OSAL_E_NOTSUPPORTED;
        break;

    case OSAL_C_S32_IOCTRL_CDCTRL_SETDRIVESPEED:
        if(s32Arg != OSAL_C_S32_IOCTRL_SETNORMALSPEED &&
           s32Arg != OSAL_C_S32_IOCTRL_SETHIGHSPEED &&
           s32Arg != OSAL_C_S32_IOCTRL_SETLOWNOISESPEED)
        {
            u32RetVal = OSAL_E_INVALIDVALUE;
        }
        else
        {
            u32RetVal = OSAL_E_NOTSUPPORTED;
        	// Not supported
            CDCTRL_TRACE_IOCTRL(CDCTRL_EN_SETDRIVESPEED,
                                (tU32)s32DriveIndex,
                                (tU32)s32Arg,
                                0,
                                0);

        }

        break;
#ifdef MEDIA_STATE_NOTIFY
    case OSAL_C_S32_IOCTRL_REG_NOTIFICATION:
    {
      OSAL_trNotifyData *prReg = (OSAL_trNotifyData *)s32Arg;

      if((prReg == NULL) || (prReg->pCallback == NULL))
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
      else if(OSAL_s32SemaphoreWait(CDCTRLSemHandle[s32DriveIndex], 
                                           OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
      {
          u32RetVal = OSAL_ERROR;
      }

      else
      {
        u32RetVal = AIF_u32RegisterMediaNotifier(CDCTRLProcInfo[s32DriveIndex],
                                                               prReg);

        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_REG_NOTIFICATION,
                            0,
                            (tU32)s32Arg,
                            0,
                            0);
        OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
      }
    }
    break;

  case OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION:
    {
      OSAL_trRelNotifyData *prReg = (OSAL_trRelNotifyData *)s32Arg;
      if(prReg == NULL)
      {
        u32RetVal = OSAL_E_INVALIDVALUE;
      }
      else if(OSAL_s32SemaphoreWait(CDCTRLSemHandle[s32DriveIndex], 
                                           OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
      {
          u32RetVal = OSAL_ERROR;
      }

      else
      {
        u32RetVal = AIF_s32UnregisterMediaNotifier(
                                       CDCTRLProcInfo[s32DriveIndex], prReg);
        CDCTRL_TRACE_IOCTRL(CDCTRL_EN_UNREG_NOTIFICATION,
                            0,
                            (tU32)s32Arg,
                            0,
                            0);
        OSAL_s32SemaphorePost(CDCTRLSemHandle[s32DriveIndex]);
      }
    }
    break;
#endif
    case OSAL_C_S32_IOCTRL_CDCTRL_READERRORBUFFER:
    case OSAL_C_S32_IOCTRL_CDCTRL_EJECTLOCK:
    case OSAL_C_S32_IOCTRL_CDCTRL_GETDVDINFO:
        u32RetVal = OSAL_E_NOTSUPPORTED;
        break;

    default:
        u32RetVal = OSAL_E_WRONGFUNC;
        break;
    }

    /*if( LLD_bIsTraceActive_en( OSAL_C_TR_CLASS_DEV_CDCTRL, TR_LEVEL_USER_1 ) == TRUE )
    {
    au8Buf[0] = EN_IOCONTROL_RESULT;
    au8Buf[1] = (tU8) s32DriveIndex;
    OSAL_M_INSERT_T32( &au8Buf[2], u32RetVal );
    LLD_vTrace_en( OSAL_C_TR_CLASS_DEV_CDCTRL, TR_LEVEL_USER_1, au8Buf, 6 );
    }*/

    CDCTRL_TRACE_IOCTRL(CDCTRL_EN_IOCONTROL_RESULT,
                        (tU32)s32DriveIndex,
                        (tU32)s32Fun,
                        u32RetVal,
                        0);

    CDCTRL_TRACE_LEAVE_U4(CDCTRL_u32IOControl,
                          u32RetVal,
                          (tU32)s32DriveIndex,
                          (tU32)s32Fun,
                          (tU32)s32Arg,0);

    return u32RetVal;
}



tS32 cdctrl_drv_io_open(tS32 s32Id,
                        tCString szName,
                        OSAL_tenAccess enAccess,
                        tU32 *pu32FD,
                        tU16 app_id)
{
  (void)szName;
  (void)enAccess;
  (void)pu32FD;
  (void)app_id;
  return(tS32)CDCTRL_u32IOOpen(0);
}

tS32 cdctrl_drv_io_close(tS32 s32ID, tU32 u32FD)
{
  (void)u32FD;
  return(tS32)CDCTRL_u32IOClose(0);
}

tS32 cdctrl_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{
  (void)u32FD;
  return(tS32)CDCTRL_u32IOControl(0, s32fun, s32arg);
}

/* constructor used here to do initialization of driver in case if it is loaded
 * as shared object. constructor priority(102) has chosen to ensure ATAPI
 * interface ready to handle CDCTRL request. ATAPI Interface has constructor
 * priority value of 101.Less value of constructor priority will get called
 * before when compared to higher constructor priority in case
 * if more than a constructor.
 */

void __attribute__ ((constructor(102))) od_process_cdctrl_attach(void)
{

   TraceIOString("od_process_cdctrl_attach starts\n");

     if(CDCTRL_u32Init() != OSAL_E_NOERROR)
     {
       TraceIOString("OSAL IO Error: CDCTRL Init failed\n");

     }
     TraceIOString("od_process_cdctrl_attach ends\n");

}




#ifdef __cplusplus
}
#endif
