/************************************************************************
| FILE:         cdaudio.c
| PROJECT:      Gen3
| SW-COMPONENT: OSAL I/O Device
|------------------------------------------------------------------------
| DESCRIPTION:
|  This file contains the /dev/cdaudio device implementation
|------------------------------------------------------------------------
| COPYRIGHT:    (c) Bosch
| HISTORY:
| Date           | Modification                      | Author
| 18/04/2014     | This file is ported from Gen2     | sgo1cob
|************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
/* General headers */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/cdrom.h>

#include <poll.h>
#include <alsa/asoundlib.h>

/* Basic OSAL includes */
//#define OSAL_S_IMPORT_INTERFACE_GENERIC
//#include "osal_if.h"
#include "ostrace.h"
#include "../../atapi_if/include/atapi_if.h"
#include "cdaudio.h"
#include "cdaudio_trace.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define CDAUDIO_DEBUG_DUMP              // Define this for debug output to console

#define CDAUDIO_IS_USING_LOCAL_TRACE
#define CDAUDIO_C_S32_IO_VERSION        (tS32)0x00010000   /*Read as v1.0.0*/



/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable inclusion  (scope: global)
|-----------------------------------------------------------------------*/
extern tBool CDAUDIO_bTraceOn;

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static OSAL_tSemHandle      CDAUDIOSemHandle[ATAPI_IF_DRIVE_NUMBER] = { OSAL_ERROR };
static OSAL_tShMemHandle    CDAUDIOShMemHandle[ATAPI_IF_DRIVE_NUMBER] = { OSAL_ERROR };

static ATAPI_IF_PROC_INFO   *CDAUDIOProcInfo[ATAPI_IF_DRIVE_NUMBER] = { NULL };

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: global)
|-----------------------------------------------------------------------*/
tS32 CDAUDIO_u32IOOpen(tU32 DriveIdx);
tS32 CDAUDIO_u32IOClose(tU32 DriveIdx);
tS32 CDAUDIO_u32IOControl(tU32 DriveIdx, tS32 s32Fun, tS32 s32Arg);



/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
/*****************************************************************************
*
* FUNCTION:    coszGetErrorText
*
* DESCRIPTION: get pointer to osal error text
*
*
*****************************************************************************/
static const char* pGetErrTxt()
{
   return (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode());
}



#ifndef CDAUDIO_DEBUG_DUMP
#define CDAUDIO_DEBUG_PRINTF(...)
#else //CDAUDIO_DEBUG_DUMP
#define CDAUDIO_DEBUG_PRINTF(...)    printf(__VA_ARGS__)

static void HexDump(const void *Buff, int Len)
{
    const unsigned char *buff = (const unsigned char *)Buff;
    int                 i;

    for(i = 0; i < Len; i+=32)
    {
        int j;

        printf("%04x:  ", i);

        for(j = 0; j < 16 && i + j < Len; j++)
            printf("%02x ", (int)buff[i + j]);
        printf("- ");
        for(; j < 32 && i + j < Len; j++)
            printf("%02x ", (int)buff[i + j]);
        for(; j < 32; j++)
            printf("   ");

        printf(" :  ");

        for(j = 0; j < 16 && i + j < Len; j++)
            printf("%c", (buff[i + j] >= ' ')?buff[i + j]:'.');
        printf(" - ");
        for(; j < 32 && i + j < Len; j++)
            printf("%c", (buff[i + j] >= ' ')?buff[i + j]:'.');

        printf("\n");
    }
}



static void DumpPlayStatus(const char *Text, ATAPI_IF_PLAY_STATUS *PlayStatus)
{
    printf("%s ", Text);
    switch(PlayStatus->Status)
    {
    case ATAPI_IF_STRM_STATUS_NO_DISK:          printf("NO_DISK");      break;
    case ATAPI_IF_STRM_STATUS_STOP:             printf("STOP");         break;
    case ATAPI_IF_STRM_STATUS_PLAY:             printf("PLAY");         break;
    case ATAPI_IF_STRM_STATUS_PAUSE:            printf("PAUSE");        break;
    case ATAPI_IF_STRM_STATUS_FAST_FORWARD:     printf("FAST_FWD");     break;
    case ATAPI_IF_STRM_STATUS_FAST_BACKWARD:    printf("FAST_BWD");     break;
    case ATAPI_IF_STRM_STATUS_SHUTDOWN:         printf("SHUTDOWN");     break;
    case ATAPI_IF_STRM_REQUEST_PLAY_RANGE:      printf("PLAY_RANGE");   break;
    case ATAPI_IF_STRM_REQUEST_RESUME:          printf("RESUME");       break;
    }
    if(PlayStatus->NewStartLBA == 0)
     printf(" without timechange\n");
    else
     printf(" NewStartLBA: %i, NewEndLBA: %i\n", PlayStatus->NewStartLBA, PlayStatus->NewEndLBA);
}
#endif //CDAUDIO_DEBUG_DUMP



tU32 CDAUDIO_u32Init(void)
{
#ifdef CDAUDIO_IS_USING_LOCAL_TRACE
    CDAUDIO_vRegTrace();
#endif //CDAUDIO_IS_USING_LOCAL_TRACE
    return OSAL_E_NOERROR;
}



tU32 CDAUDIO_u32Destroy(void)
{
#ifdef CDAUDIO_IS_USING_LOCAL_TRACE
    CDAUDIO_vUnregTrace();
#endif //CDAUDIO_IS_USING_LOCAL_TRACE
    return OSAL_E_NOERROR;
}



/************************************************************************
*                                                                       *
* FUNCTIONS                                                             *
*                                                                       *
*      CDAUDIO_u32IOOpen                                                *
*                                                                       *
* DESCRIPTION                                                           *
*      Open cdaudio required resources                                  *
*                                                                       *
* CALLS                                                                 *
*                                                                       *
*      LLD Atapi API                                                    *
*                                                                       *
* INPUTS                                                                *
*                                                                       *
*      OSAL_tenCdvd_DriveID enDriveID: Drive identificator              *
*                                                                       *
* OUTPUTS                                                               *
*                                                                       *
*      tS32: Error Code                                                 *
*HISTORY      :                                                         *
*                                                                       *     
*               05.07.06  Venkateswara.N (RBIN/ECM1)                    *
*               Added the code for TSIM                                 *
************************************************************************/
tS32 CDAUDIO_u32IOOpen(tU32 DriveIdx)
{
    tChar   semname[] = ATAPI_IF_SEM_NAME;
    tChar   shmemname[] = ATAPI_IF_SHMEM_NAME;
    tU32    u32Result = OSAL_E_NOERROR;

    CDAUDIO_TRACE_ENTER_U4(CDAUDIO_u32IOOpen,
                           DriveIdx,
                           0,0,0);

    semname[sizeof(ATAPI_IF_SEM_NAME) - 2] = '0' + (tChar)DriveIdx;
    shmemname[sizeof(ATAPI_IF_SHMEM_NAME) - 2] = '0' + (tChar)DriveIdx;

    CDAUDIOShMemHandle[DriveIdx] = OSAL_SharedMemoryOpen(shmemname, OSAL_EN_READWRITE);
    if(CDAUDIOShMemHandle[DriveIdx] == OSAL_ERROR)
    {
        u32Result = OSAL_ERROR;
    }

    if(u32Result != OSAL_ERROR)
    {
        CDAUDIOProcInfo[DriveIdx] = (ATAPI_IF_PROC_INFO *)OSAL_pvSharedMemoryMap(CDAUDIOShMemHandle[DriveIdx], OSAL_EN_READWRITE, sizeof(ATAPI_IF_PROC_INFO), 0);
        if(CDAUDIOProcInfo[DriveIdx] == NULL)
        {
            u32Result = OSAL_ERROR;
        }
    }

    if(u32Result != OSAL_ERROR && OSAL_s32SemaphoreOpen(semname, &CDAUDIOSemHandle[DriveIdx]) != OSAL_OK)
    {
        u32Result = OSAL_ERROR;
    }

    CDAUDIO_PRINTF_U1("Ein Audio Test 0x%08X, %p",
                      (unsigned int)CDAUDIO_u32IOOpen,CDAUDIO_u32IOOpen);

    CDAUDIO_TRACE_LEAVE_U4(CDAUDIO_u32IOOpen,
                           u32Result,
                           DriveIdx,
                           0,0,0);
    return u32Result;
}

/************************************************************************
*                                                                       *
* FUNCTIONS                                                             *
*                                                                       *
*      CDAUDIO_u32IOClose                                                *
*                                                                       *
* DESCRIPTION                                                           *
*      Close cdaudio required device                                     *
*                                                                       *
* CALLS                                                                 *
*                                                                       *
*      LLD Atapi API                                                    *
*                                                                       *
* INPUTS                                                                *
*                                                                       *
*      OSAL_tenCdvd_DriveID enDriveID: Drive identificator              *
*                                                                       *
* OUTPUTS                                                               *
*                                                                       *
*      tS32: Error code                                                 *
*                                                                       *
*HISTORY      :                                                         *
*                                                                       *     
*               05.07.06  Venkateswara.N (RBIN/ECM1)                    *
*               Added the code for TSIM                                 *
************************************************************************/


tS32 CDAUDIO_u32IOClose(tU32 DriveIdx)
{
    tU32 u32Result = OSAL_E_NOERROR;
    CDAUDIO_TRACE_ENTER_U4(CDAUDIO_u32IOClose, DriveIdx, 2, 3, 4);

    if(CDAUDIOProcInfo[DriveIdx] != NULL)
    {
        OSAL_s32SharedMemoryUnmap(CDAUDIOProcInfo[DriveIdx], sizeof(ATAPI_IF_PROC_INFO));
        CDAUDIOProcInfo[DriveIdx] = NULL;
    }
    if(CDAUDIOShMemHandle[DriveIdx] != OSAL_ERROR)
    {
        OSAL_s32SharedMemoryClose(CDAUDIOShMemHandle[DriveIdx]);
        CDAUDIOShMemHandle[DriveIdx] = OSAL_ERROR;
    }
    if(CDAUDIOSemHandle[DriveIdx] != OSAL_ERROR)
    {
        OSAL_s32SemaphoreClose(CDAUDIOSemHandle[DriveIdx]);
        CDAUDIOSemHandle[DriveIdx] = OSAL_ERROR;
    }

    CDAUDIO_TRACE_LEAVE_U4(CDAUDIO_u32IOClose, u32Result,
                           DriveIdx,
                           11,12,13);
    return u32Result;
}

/************************************************************************
*                                                                       *
* FUNCTIONS                                                             *
*                                                                       *
*      CDAUDIO_u32IOControl                                              *
*                                                                       *
* DESCRIPTION                                                           *
*      Call a cdaudio control function                                   *
*                                                                       *
* CALLS                                                                 *
*                                                                       *
*      Is only a wrapper to s32[Atapi|Hdd|AtaNEC]IOControl              *
*                                                                       *
* INPUTS                                                                *
*                                                                       *
*       OSAL_tenCdvd_DriveID enDriveID: Drive identificator             *
*       tS32 s32Fun:               Function identificator               *
*       tS32 s32Arg:               Argument to be passed to function    *
*                                                                       *
* OUTPUTS                                                               *
*                                                                       *
*      tS32: Error code                                                 *
*                                                                       *
*HISTORY      :                                                         *
*                                                                       *     
*               05.07.06  Venkateswara.N (RBIN/ECM1)                    *
*               Added the code for TSIM                                 *
************************************************************************/
tS32 CDAUDIO_u32IOControl(tU32 s32DriveIndex, tS32 s32Fun, tS32 s32Arg)
{
    tU32 u32Result = OSAL_E_NOERROR;
    //tU8 au8Buf[21];

    CDAUDIO_TRACE_ENTER_U4(CDAUDIO_u32IOControl,
                           s32DriveIndex,
                           s32Fun,
                           s32Arg,
                           0);

    CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_IOCONTROL,
                         s32DriveIndex,
                         s32Fun,
                         s32Arg,
                         0);


    switch(s32Fun)
    {
    case OSAL_C_S32_IOCTRL_VERSION:
        {
            tS32* ps32Version = (tS32*)s32Arg;

            if(ps32Version == NULL)
            {
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else
            {
                *ps32Version = CDAUDIO_C_S32_IO_VERSION;
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_PLAY:
        {
            u32Result = (tU32)ATAPI_IF_RequestStreamStatusChange(CDAUDIOProcInfo[s32DriveIndex], CDAUDIOSemHandle[s32DriveIndex], ATAPI_IF_STRM_STATUS_PLAY, 0, 0);
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_STOP:
        {
            u32Result = (tU32)ATAPI_IF_RequestStreamStatusChange(CDAUDIOProcInfo[s32DriveIndex], CDAUDIOSemHandle[s32DriveIndex], ATAPI_IF_STRM_STATUS_STOP, 0, 0);
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_PAUSE:
        {
            u32Result = (tU32)ATAPI_IF_RequestStreamStatusChange(CDAUDIOProcInfo[s32DriveIndex], CDAUDIOSemHandle[s32DriveIndex], ATAPI_IF_STRM_STATUS_PAUSE, 0, 0);
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_FASTFORWARD:
        {
           u32Result = (tU32)ATAPI_IF_RequestStreamStatusChange(CDAUDIOProcInfo[s32DriveIndex], CDAUDIOSemHandle[s32DriveIndex], ATAPI_IF_STRM_STATUS_FAST_FORWARD, 0, 0);
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_FASTBACKWARD:
        {
            u32Result = (tU32)ATAPI_IF_RequestStreamStatusChange(CDAUDIOProcInfo[s32DriveIndex], CDAUDIOSemHandle[s32DriveIndex], ATAPI_IF_STRM_STATUS_FAST_BACKWARD, 0, 0);
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_RESUME:
        {
            u32Result = (tU32)ATAPI_IF_RequestStreamStatusChange(CDAUDIOProcInfo[s32DriveIndex], CDAUDIOSemHandle[s32DriveIndex], ATAPI_IF_STRM_REQUEST_RESUME, 0, 0);
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_SETPLAYRANGE:
        {
            OSAL_trPlayRange    *prRange = (OSAL_trPlayRange *)s32Arg;

            if(prRange == NULL)
            {
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else
            {
                // FIXME: ATAPI_IF_OSAL_TrckPos2LBA must be protected by semaphore!
                tU32    startlba = ATAPI_IF_OSAL_TrckPos2LBA(&CDAUDIOProcInfo[s32DriveIndex]->CDCache, &prRange->rStartAdr);
                tU32    endlba = ATAPI_IF_OSAL_TrckPos2LBA(&CDAUDIOProcInfo[s32DriveIndex]->CDCache, &prRange->rEndAdr);

                u32Result = (tU32)ATAPI_IF_RequestStreamStatusChange(CDAUDIOProcInfo[s32DriveIndex], CDAUDIOSemHandle[s32DriveIndex], ATAPI_IF_STRM_REQUEST_PLAY_RANGE, startlba, endlba);
            }
        }
        break;
    case OSAL_C_S32_IOCTRL_CDAUDIO_SETMFS:
        {
            u32Result = OSAL_E_NOTSUPPORTED;
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_GETCDINFO:
        {
            OSAL_trCDAudioInfo* prInfo = (OSAL_trCDAudioInfo*)s32Arg;

            if(prInfo == NULL)
            {
                CDAUDIO_PRINTF_ERRORS("Pointer to OSAL_trCDAudioInfo is NULL");
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDAUDIOSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                CDAUDIO_PRINTF_ERRORS("Semaphore not available: Error Code: %s", (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
                u32Result = OSAL_E_UNKNOWN;
            }
            else
            {
                ATAPI_IF_UpdateProcStatus(CDAUDIOProcInfo[s32DriveIndex]);

                if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_NO_DISK)
                {
                    CDAUDIO_PRINTF_U4("No Disk");
                    u32Result = OSAL_E_MEDIA_NOT_AVAILABLE;
                }
                else
                {
                    prInfo->u32CdText    = CDAUDIOProcInfo[s32DriveIndex]->CDCache.CDTextFlag;
                    prInfo->u32MinTrack  = CDAUDIOProcInfo[s32DriveIndex]->CDCache.MinTrck;
                    prInfo->u32MaxTrack  = CDAUDIOProcInfo[s32DriveIndex]->CDCache.MaxTrck;
                    ATAPI_IF_LBA2MSF(&prInfo->u32TrMinutes, &prInfo->u32TrSeconds, NULL, CDAUDIOProcInfo[s32DriveIndex]->CDCache.PlayTime);

                    CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_GETCDINFO,
                                         (tU32)s32DriveIndex,
                                         (tU32)prInfo->u32MinTrack << 16
                                         | (tU32)prInfo->u32MaxTrack,
                                         (tU32)prInfo->u32TrMinutes << 16
                                         | (tU32)prInfo->u32TrSeconds,
                                         prInfo->u32CdText
                                        );
                }
                OSAL_s32SemaphorePost(CDAUDIOSemHandle[s32DriveIndex]);
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_GETADDITIONALCDINFO:
        {
            OSAL_trAdditionalCDInfo *prAdditionalInfo = (OSAL_trAdditionalCDInfo *)s32Arg;

            if(prAdditionalInfo == NULL)
            {
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDAUDIOSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                CDAUDIO_PRINTF_ERRORS("Semaphore not available: Error Code: %s", (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
                u32Result = OSAL_E_UNKNOWN;
            }
            else
            {
                ATAPI_IF_UpdateProcStatus(CDAUDIOProcInfo[s32DriveIndex]);

                if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_NO_DISK)
                {
                    CDAUDIO_PRINTF_U4("No Disk");
                    u32Result = OSAL_E_MEDIA_NOT_AVAILABLE;
                }
                else
                {
                    prAdditionalInfo->u32DataTracks[0] = CDAUDIOProcInfo[s32DriveIndex]->CDCache.DataTrackInfo[0];
                    prAdditionalInfo->u32DataTracks[1] = CDAUDIOProcInfo[s32DriveIndex]->CDCache.DataTrackInfo[1];
                    prAdditionalInfo->u32DataTracks[2] = CDAUDIOProcInfo[s32DriveIndex]->CDCache.DataTrackInfo[2];
                    prAdditionalInfo->u32DataTracks[3] = CDAUDIOProcInfo[s32DriveIndex]->CDCache.DataTrackInfo[3];

                    prAdditionalInfo->u32InfoBits = CDAUDIO_ADDITIONALCDINFO_NULL_VALUE;
                    if(CDAUDIO_ADDITIONALCDINFO_IS_DATA_TRACK_NUMBER_1(CDAUDIOProcInfo[s32DriveIndex]->CDCache.DataTrackInfo))
                    {
                        prAdditionalInfo->u32InfoBits |= CDAUDIO_ADDITIONALCDINFO_FIRST_TRACK_DATA_BITMASK;
                    }

                    CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_GETADDITIONALCDINFO_INFO,
                                         (tU32)s32DriveIndex,
                                         (tU32)prAdditionalInfo->u32InfoBits,
                                         0,0
                                        );
                    CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_GETADDITIONALCDINFO_TRACKS,
                                         (tU32)prAdditionalInfo->u32DataTracks[0],
                                         (tU32)prAdditionalInfo->u32DataTracks[1],
                                         (tU32)prAdditionalInfo->u32DataTracks[2],
                                         (tU32)prAdditionalInfo->u32DataTracks[3]
                                        );
                }
                OSAL_s32SemaphorePost(CDAUDIOSemHandle[s32DriveIndex]);
            }
            break;
        }

    case OSAL_C_S32_IOCTRL_CDAUDIO_GETPLAYINFO:
        {
            OSAL_trPlayInfo* prPlayInfo = (OSAL_trPlayInfo*)s32Arg;

            if(prPlayInfo == NULL)
            {
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDAUDIOSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                CDAUDIO_PRINTF_ERRORS("Semaphore not available: Error Code: %s", (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
                u32Result = OSAL_E_UNKNOWN;
            }
            else
            {
                ATAPI_IF_UpdateProcStatus(CDAUDIOProcInfo[s32DriveIndex]);

                if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_NO_DISK)
                {
                    CDAUDIO_PRINTF_U4("No Disk");
                    u32Result = OSAL_E_MEDIA_NOT_AVAILABLE;
                }
                else
                {
                    ATAPI_IF_LBA2MSF_OSAL_Addr(&prPlayInfo->rAbsTrackAdr, CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.CurrentLBA);
                    prPlayInfo->u32TrackNumber = ATAPI_IF_LBA2TRACK(&CDAUDIOProcInfo[s32DriveIndex]->CDCache, CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.CurrentLBA);
                    if(prPlayInfo->u32TrackNumber > 0)
                    {
                        ATAPI_IF_LBA2MSF_OSAL_Addr(&prPlayInfo->rRelTrackAdr, CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.CurrentLBA - CDAUDIOProcInfo[s32DriveIndex]->CDCache.TrckStartLBA[prPlayInfo->u32TrackNumber]);
                    }
                    else
                    {
                        ATAPI_IF_LBA2MSF_OSAL_Addr(&prPlayInfo->rRelTrackAdr, 0);
                    }
                    if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_STOP)
                        prPlayInfo->u32StatusPlay  = OSAL_C_S32_PLAY_COMPLETED;
                    else if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_PLAY)
                        prPlayInfo->u32StatusPlay  = OSAL_C_S32_PLAY_IN_PROGRESS;
                    else if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_PAUSE)
                        prPlayInfo->u32StatusPlay  = OSAL_C_S32_PLAY_PAUSED;
                    else if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_FAST_FORWARD)
                        prPlayInfo->u32StatusPlay  = OSAL_C_S32_PLAY_IN_PROGRESS;
                    else if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_FAST_BACKWARD)
                        prPlayInfo->u32StatusPlay  = OSAL_C_S32_PLAY_IN_PROGRESS;
                    else
                        prPlayInfo->u32StatusPlay  = OSAL_C_S32_NO_CURRENT_AUDIO_STATUS;

                    CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_GETPLAYINFO,
                                 (tU32)s32DriveIndex,
                                 (tU32)prPlayInfo->u32TrackNumber << 16
                                 |
                                 (tU32)prPlayInfo->u32StatusPlay,
                                 (tU32)prPlayInfo->rAbsTrackAdr.u8MSFMinute << 16
                                 | (tU32)prPlayInfo->rAbsTrackAdr.u8MSFSecond << 8
                                 | (tU32)prPlayInfo->rAbsTrackAdr.u8MSFFrame,
                                 (tU32)prPlayInfo->rRelTrackAdr.u8MSFMinute << 16
                                 | (tU32)prPlayInfo->rRelTrackAdr.u8MSFSecond << 8
                                 | (tU32)prPlayInfo->rRelTrackAdr.u8MSFFrame
                                );

                }
                OSAL_s32SemaphorePost(CDAUDIOSemHandle[s32DriveIndex]);
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO:
        {
            OSAL_trTrackInfo* prTrackInfo = (OSAL_trTrackInfo*)s32Arg;

            if(prTrackInfo == NULL)
            {
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDAUDIOSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                CDAUDIO_PRINTF_ERRORS("Semaphore not available: Error Code: %s", (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
                u32Result = OSAL_E_UNKNOWN;
            }
            else
            {
                ATAPI_IF_UpdateProcStatus(CDAUDIOProcInfo[s32DriveIndex]);

                if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_NO_DISK)
                {
                    CDAUDIO_PRINTF_U4("No Disk");
                    u32Result = OSAL_E_MEDIA_NOT_AVAILABLE;
                }
                else if(prTrackInfo->u32TrackNumber < CDAUDIOProcInfo[s32DriveIndex]->CDCache.MinTrck ||
                        prTrackInfo->u32TrackNumber > CDAUDIOProcInfo[s32DriveIndex]->CDCache.MaxTrck)
                {
                    CDAUDIO_PRINTF_U4("Tracknumber %i must be between MinTrack %i and MaxTrack %i", (int)prTrackInfo->u32TrackNumber, (int)CDAUDIOProcInfo[s32DriveIndex]->CDCache.MinTrck, (int)CDAUDIOProcInfo[s32DriveIndex]->CDCache.MaxTrck);
                    u32Result = OSAL_E_DOESNOTEXIST;
                }
                else
                {
                    ATAPI_IF_LBA2MSF_OSAL_Addr(&prTrackInfo->rStartAdr, CDAUDIOProcInfo[s32DriveIndex]->CDCache.TrckStartLBA[prTrackInfo->u32TrackNumber - 1]);
                    ATAPI_IF_LBA2MSF_OSAL_Addr(&prTrackInfo->rEndAdr, CDAUDIOProcInfo[s32DriveIndex]->CDCache.TrckStartLBA[prTrackInfo->u32TrackNumber] - 1);
                    prTrackInfo->u32TrackControl = 0;

                    CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_GETTRACKINFO,
                                     (tU32)s32DriveIndex,
                                     (tU32)prTrackInfo->u32TrackNumber << 16
                                     |
                                     (tU32)prTrackInfo->u32TrackControl,
                                     (tU32)prTrackInfo->rStartAdr.u8MSFMinute << 16
                                     | (tU32)prTrackInfo->rStartAdr.u8MSFSecond << 8
                                     | (tU32)prTrackInfo->rStartAdr.u8MSFFrame,
                                     (tU32)prTrackInfo->rEndAdr.u8MSFMinute << 16
                                     | (tU32)prTrackInfo->rEndAdr.u8MSFSecond << 8
                                     | (tU32)prTrackInfo->rEndAdr.u8MSFFrame
                                    );
                }
                OSAL_s32SemaphorePost(CDAUDIOSemHandle[s32DriveIndex]);
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_GETALBUMNAME: /* CD-TEXT command */
        {
            tU8  *pu8Txt = (tU8 *)s32Arg;

            if(pu8Txt == NULL)
            {
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDAUDIOSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                CDAUDIO_PRINTF_ERRORS("Semaphore not available: Error Code: %s", (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
                u32Result = OSAL_E_UNKNOWN;
            }
            else
            {
                ATAPI_IF_UpdateProcStatus(CDAUDIOProcInfo[s32DriveIndex]);

                if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_NO_DISK)
                {
                    CDAUDIO_PRINTF_U4("No Disk");
                    u32Result = OSAL_E_MEDIA_NOT_AVAILABLE;
                }
                else if(!CDAUDIOProcInfo[s32DriveIndex]->CDCache.CDTextFlag)
                {
                    u32Result = OSAL_E_NOTSUPPORTED;
                }
                else
                {
                    memset(pu8Txt, 0, OSAL_C_S32_CDAUDIO_MAXNAMESIZE);
                    strncpy(pu8Txt, CDAUDIOProcInfo[s32DriveIndex]->CDCache.CDTextAlbumName, OSAL_C_S32_CDAUDIO_MAXNAMESIZE);

                    CDAUDIO_TRACE_IOCTRL_TXT(CDAUDIO_EN_GETALBUMNAME,
                                             (tU32)s32DriveIndex,
                                             (tU32)0,
                                             pu8Txt
                                            );
                }
                OSAL_s32SemaphorePost(CDAUDIOSemHandle[s32DriveIndex]);
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKCDINFO: /* CD-TEXT command */
        {
            OSAL_trTrackCdTextInfo    *prTrkTextInfo = (OSAL_trTrackCdTextInfo *)s32Arg;
            struct cdrom_tochdr tochdr;

            if(prTrkTextInfo == NULL)
            {
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDAUDIOSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                CDAUDIO_PRINTF_ERRORS("Semaphore not available: Error Code: %s", (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
                u32Result = OSAL_E_UNKNOWN;
            }
            else
            {
                ATAPI_IF_UpdateProcStatus(CDAUDIOProcInfo[s32DriveIndex]);

                if(CDAUDIOProcInfo[s32DriveIndex]->PlayStatus.Status == ATAPI_IF_STRM_STATUS_NO_DISK)
                {
                    CDAUDIO_PRINTF_U4("No Disk");
                    u32Result = OSAL_E_MEDIA_NOT_AVAILABLE;
                }
                else if(!CDAUDIOProcInfo[s32DriveIndex]->CDCache.CDTextFlag)
                {
                    u32Result = OSAL_E_NOTSUPPORTED;
                }
                else if(prTrkTextInfo->u32TrackNumber < CDAUDIOProcInfo[s32DriveIndex]->CDCache.MinTrck ||
                        prTrkTextInfo->u32TrackNumber > CDAUDIOProcInfo[s32DriveIndex]->CDCache.MaxTrck)
                {
                    CDAUDIO_PRINTF_U4("Tracknumber %i must be between MinTrack %i and MaxTrack %i", (int)prTrkTextInfo->u32TrackNumber, (int)CDAUDIOProcInfo[s32DriveIndex]->CDCache.MinTrck, (int)CDAUDIOProcInfo[s32DriveIndex]->CDCache.MaxTrck);
                    u32Result = OSAL_E_DOESNOTEXIST;
                }
                else
                {
                    strncpy(prTrkTextInfo->rTrackTitle, CDAUDIOProcInfo[s32DriveIndex]->CDCache.CDTextTrackTitle[prTrkTextInfo->u32TrackNumber - 1], OSAL_C_S32_CDAUDIO_MAXNAMESIZE);
                    strncpy(prTrkTextInfo->rPerformer, CDAUDIOProcInfo[s32DriveIndex]->CDCache.CDTextTrackPerformer[prTrkTextInfo->u32TrackNumber - 1], OSAL_C_S32_CDAUDIO_MAXNAMESIZE);

                    CDAUDIO_TRACE_IOCTRL_TXT(CDAUDIO_EN_GETTRACKCDINFO_ARTIST,
                                         (tU32)s32DriveIndex,
                                         (tU32)prTrkTextInfo->u32TrackNumber,
                                         (const char*)prTrkTextInfo->rPerformer
                                        );
                    CDAUDIO_TRACE_IOCTRL_TXT(CDAUDIO_EN_GETTRACKCDINFO_TITLE,
                                         (tU32)s32DriveIndex,
                                         (tU32)prTrkTextInfo->u32TrackNumber,
                                         (const char*)prTrkTextInfo->rTrackTitle
                                        );
                }
                OSAL_s32SemaphorePost(CDAUDIOSemHandle[s32DriveIndex]);
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY:
        {
            OSAL_trPlayInfoReg  *prReg = (OSAL_trPlayInfoReg *)s32Arg;

            if((prReg == NULL) || (prReg->pCallback == NULL))
            {
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDAUDIOSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                CDAUDIO_PRINTF_ERRORS("Semaphore not available: Error Code: %s", (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
                u32Result = OSAL_E_UNKNOWN;
            }
            else
            {
                u32Result = ATAPI_IF_RegisterPlayNotifier(CDAUDIOProcInfo[s32DriveIndex], prReg);

/*                CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_REGPLAYNOTIFY,
                                     s32DriveIndex,
                                     i-1,
                                     prReg->pCallback,
                                     (tBool)(u32Result == OSAL_E_NOERROR)
                                    );*/
                OSAL_s32SemaphorePost(CDAUDIOSemHandle[s32DriveIndex]);
            }
        }
        break;

    case OSAL_C_S32_IOCTRL_CDAUDIO_UNREGPLAYNOTIFY:
        {
            OSAL_trPlayInfoReg  *prReg = (OSAL_trPlayInfoReg *)s32Arg;

            if((prReg == NULL) || (prReg->pCallback == NULL))
            {
                u32Result = OSAL_E_INVALIDVALUE;
            }
            else if(OSAL_s32SemaphoreWait(CDAUDIOSemHandle[s32DriveIndex], OSAL_C_TIMEOUT_FOREVER) != OSAL_OK)
            {
                CDAUDIO_PRINTF_ERRORS("Semaphore not available: Error Code: %s", (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode()));
                u32Result = OSAL_E_UNKNOWN;
            }
            else
            {
                u32Result = ATAPI_IF_UnregisterPlayNotifier(CDAUDIOProcInfo[s32DriveIndex], prReg);

/*                CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_UNREGPLAYNOTIFY,
                                         s32DriveIndex,
                                         nPos,
                                         prReg->pCallback,
                                         0
                                        );
                }*/
                OSAL_s32SemaphorePost(CDAUDIOSemHandle[s32DriveIndex]);
            }
        }
        break;

    default:
        u32Result = OSAL_E_WRONGFUNC;
        break;
    }


    CDAUDIO_TRACE_IOCTRL(CDAUDIO_EN_IOCONTROL_RESULT,
                         s32DriveIndex,
                         s32Fun,
                         u32Result,
                         0);

    CDAUDIO_TRACE_LEAVE_U4(CDAUDIO_u32IOControl,
                           u32Result,
                           s32DriveIndex,
                           s32Fun,
                           s32Arg,
                           0);

    return u32Result;
}

tS32 cdaudio_drv_io_open(tS32 s32ID,
                         tCString szName,
                         OSAL_tenAccess enAccess,
                         tU32 *pu32FD,
                         tU16 app_id)
{
  (void)szName;
  (void)enAccess;
  (void)pu32FD;
  (void)app_id;
  return(tS32)CDAUDIO_u32IOOpen(0);
}

tS32 cdaudio_drv_io_close(tS32 s32ID, tU32 u32FD)
{
  (void)u32FD;
  return(tS32)CDAUDIO_u32IOClose(0);
}

tS32 cdaudio_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{
  (void)u32FD;
  return(tS32)CDAUDIO_u32IOControl(0, s32fun, s32arg);
}

/* constructor used here to do initialization of driver in case if it is loaded
 * as shared object. constructor priority(103) has chosen to ensure ATAPI
 * interface ready to handle CDAUDIO request. ATAPI Interface has constructor
 * priority value of 101 and CDCTRL interface has constructor priority value of 
 * 102.Less value of constructor priority will get called
 * before when compared to higher 
 * constructor priority in case if more than a constructor.
 */
extern sem_t *initcd_lock;

void __attribute__ ((constructor(103))) od_process_cdaudio_attach(void)
{
   TraceIOString("od_process_cdaudio_attach start\n");
   if(CDAUDIO_u32Init() != OSAL_E_NOERROR)
   {
      TraceIOString("OSAL IO Error: CDAUDIO Init failed\n");

   }
   /* Initialization of ATAPI_IF,CDCTRL and CDAUDIO
    * completed, so indicate other process that init
    * done by releasing semaphore
    */
   sem_post(initcd_lock);
   
   TraceIOString("od_process_cdaudio_attach ends\n");

}



#ifdef __cplusplus
}
#endif
