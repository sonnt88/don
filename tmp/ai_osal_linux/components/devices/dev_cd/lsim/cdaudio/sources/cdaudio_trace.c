/************************************************************************
 | $Revision: 1.0 $
 | $Date: 18/04/2014  $
 |************************************************************************
 | FILE:         cdaudio_trace.c
 | PROJECT:      GEN3
 | SW-COMPONENT: OSAL I/O Device
 |------------------------------------------------------------------------
 | DESCRIPTION:
 | This file includes trace stuff for the cdaudio device
 |------------------------------------------------------------------------
 | Date      		| Modification                      | Author
 | 18/04/2014     | This file is ported from Gen2     | sgo1cob
 |************************************************************************
 |************************************************************************/
 /************************************************************************
 | includes of component-internal interfaces
 | (scope: component-local)
 |-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/* Interface for variable number of arguments */
#include "Linux_osal.h"


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
#ifdef CDAUDIO_IS_USING_LOCAL_TRACE
#include "../../userland/platform/trace/trace/include/trace_interface.h"	// FIXME: Path!
#endif // CDAUDIO_IS_USING_LOCAL_TRACE

#include "cdaudio_trace.h"
#include "cdaudio.h"
#include "trace_interface.h"
#ifdef __cplusplus
extern "C" {
#endif


/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define CDAUDIO_IS_USING_LOCAL_TRACE

#define CDAUDIO_PRINTF_BUFFER_SIZE 256
#define CDAUDIO_TRACE_BUFFER_SIZE  256
#define CDAUDIO_GET_U32_LE(PU8) (((tU32)(PU8)[0]) | (((tU32)(PU8)[1])<<8)\
                          | (((tU32)(PU8)[2])<<16) | (((tU32)(PU8)[3])<<24) )



/************************************************************************
|typedefs (scope: module-local)
|----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
/*trace speed up - if FALSE, calling of tracing functions are suppressed*/
/*is set automatically to TRUE, if trace level >= User1 is enabled at startup*/
tBool CDAUDIO_bTraceOn = TRUE;//FALSE;


//extern BOOL UTIL_trace_isActive(TraceData* data);
/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static volatile tBool          gTraceCmdThreadIsRunning = FALSE;
static tU8 gu8TraceBuffer[CDAUDIO_TRACE_BUFFER_SIZE]; /*not sychronyzed!*/
static OSAL_tIODescriptor ghDevice = OSAL_ERROR;                            


/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:    coszGetErrorText
*
* DESCRIPTION: get pointer to osal error text
*
*
*****************************************************************************/
static const char* pGetErrTxt()
{
   return (const char*)OSAL_coszErrorText(OSAL_u32ErrorCode());
}



/*****************************************************************************
*
* FUNCTION:
*     getArgList
*
* DESCRIPTION:
*     This stupid function is used for satifying L I N T
*     
*     
* PARAMETERS: *va_list pointer to a va_list
*
* RETURNVALUE:
*     va_list
*     
*
*
*****************************************************************************/
static va_list getArgList(va_list* a)
{
	(void)a;
	return *a;
}

/********************************************************************/ /**
*  FUNCTION:      uiCGET
*
*  @brief         return time in ms
*
*  @return        time in ms
*
*  HISTORY:
*
************************************************************************/
static unsigned int uiCGET(void)
{
    return (unsigned int)OSAL_ClockGetElapsedTime();
}
/********************************************************************/ /**
 *  FUNCTION:      CDAUDIO_vTraceEnter
 *
 *  @brief         enter function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/

tVoid CDAUDIO_vTraceEnter(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pFunction,
                         tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
        tUInt uiIndex;
        tU8  au8Buf[4 * sizeof(tU32) + 7 * sizeof(tU32) + 32]; //stack?
        OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
        tU32 u32Time = (tU32)uiCGET();

        uiIndex = 0;
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_ENTER);
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
        uiIndex += sizeof(tU32);
        strncpy(au8Buf + uiIndex, pFunction, sizeof(au8Buf) - uiIndex - 1);
        au8Buf[sizeof(au8Buf) - 1] = 0;

        // trace the stuff
        LLD_vTrace(u32Class,
                   (tS32)enTraceLevel,
                   au8Buf,
                   (tU32)uiIndex + 32);
    }
}




/********************************************************************/ /**
 *  FUNCTION:      CDAUDIO_vTraceLeave
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDAUDIO_vTraceLeave(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pFunction,
                         tU32 u32OSALResult,
                         tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
        tUInt uiIndex;
        tU8  au8Buf[4 * sizeof(tU8) + 8 * sizeof(tU32) + 32]; //stack?
        OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
        tU32 u32Time = (tU32)uiCGET();
        tU32 u32FunctionOffset;

        uiIndex = 0;
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_LEAVE);
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);  // Fill Byte
        uiIndex += sizeof(tU8);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32OSALResult);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
        uiIndex += sizeof(tU32);
        OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
        uiIndex += sizeof(tU32);
        strncpy(au8Buf + uiIndex, pFunction, sizeof(au8Buf) - uiIndex - 1);
        au8Buf[sizeof(au8Buf) - 1] = 0;

        // trace the stuff
        // be aware index is not greater than aray size!
        // I could not test it while runtime,
        // because LINT throws a senseless warning.
        LLD_vTrace(u32Class,
                   (tS32)enTraceLevel,
                   au8Buf,
                   (tU32)uiIndex + 32);
    }
}


/*****************************************************************************
*
* FUNCTION:
*     CDAUDIO_vTracePrintf
*
* DESCRIPTION:
*     This function creates the printf-style trace message
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
*
*****************************************************************************/
tVoid CDAUDIO_vTracePrintf(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
						  tBool bForced,
                          tU32 u32Line,
                          const char* coszFormat,...)
{
   if((TRUE == bForced)
	  ||
	  (LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE))
   {
      static tU8  au8Buf[CDAUDIO_PRINTF_BUFFER_SIZE + 1];  //TODO: ? size ?
      tUInt uiIndex;
      long lSize;
      size_t tMaxSize;
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time     = (tU32)uiCGET();
	  va_list argList;


	  uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex],
				   (tU32) (enTraceLevel == TR_LEVEL_ERRORS
						    ?
						   CDAUDIO_TRACE_PRINTF_ERROR : CDAUDIO_TRACE_PRINTF));
      uiIndex += sizeof(tU8);
	  OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 11);
	  uiIndex += sizeof(tU8);
	  OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 12);
	  uiIndex += sizeof(tU8);
	  OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) 13);
	  uiIndex += sizeof(tU8);
       //next pointer is fix set to 32 bit -be happy with 64 bit systems
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32) u32Line);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      
	  argList = getArgList(&argList);
	  va_start(argList, coszFormat); /*lint !e718 */
      tMaxSize = CDAUDIO_PRINTF_BUFFER_SIZE - uiIndex;
      lSize = vsnprintf((char*)&au8Buf[uiIndex], tMaxSize, coszFormat, argList);
      uiIndex += (tUInt)lSize;
      va_end(argList);
      
      // trace the stuff 
      LLD_vTrace(u32Class,
                 (tS32)enTraceLevel,
                 au8Buf, 
                 (tU32)uiIndex);
   }
   
}


/********************************************************************/ /**
 *  FUNCTION:      CDAUDIO_vTraceIoctrl
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDAUDIO_vTraceIoctrl(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
                          tU32 u32Line,
                          CDAUDIO_enumIoctrlTraceID enIoctrlTraceID,
                          tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
						  tU32 u32Par4)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
       tUInt uiIndex;
       tU8  au8Buf[4 * sizeof(tU8) + 7 * sizeof(tU32)]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();

       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_IOCTRL);
       uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enIoctrlTraceID);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
       uiIndex += sizeof(tU32);

       // trace the stuff 
       // be aware index is not greater than aray size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex);
    }
}

/********************************************************************/ /**
 *  FUNCTION:      CDAUDIO_vTraceIoctrlTxt
 *
 *  @brief         leave function message
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDAUDIO_vTraceIoctrlTxt(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
                          tU32 u32Line,
                          CDAUDIO_enumIoctrlTraceID enIoctrlTraceID,
                          tU32 u32Par1, tU32 u32Par2, const char *pcszTxt)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
	   size_t  iSize, iMaxSize;
       tUInt uiIndex;
       tU8  au8Buf[CDAUDIO_TRACE_BUFFER_SIZE]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();

       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_IOCTRL);
       uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enIoctrlTraceID);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
       uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
	   uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
	   uiIndex += sizeof(tU32);
	   iSize = strlen(pcszTxt)+1;
	   iMaxSize = CDAUDIO_TRACE_BUFFER_SIZE - (uiIndex + 1);
       iSize = (iSize < iMaxSize) ? iSize : iMaxSize;
       memcpy(&au8Buf[uiIndex], pcszTxt, iSize);
       uiIndex += iSize;

       // trace the stuff 
       // be aware index is not greater than aray size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex);
    }
}


/********************************************************************/ /**
 *  FUNCTION:      CDAUDIO_vTraceIoctrlResult
 *
 *  @brief         error text with IOCtrl-name
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         u32Line            Line number __LINE__
 *  @param         pFunction          Function pointer
 *  @param         u32OSALError       OSAL Result
 *
 *  @return        none
 *
 *  HISTORY:
 *
 ************************************************************************/
tVoid CDAUDIO_vTraceIoctrlResult(tU32 u32Class,
                               TR_tenTraceLevel enTraceLevel,
                               tU32 u32Line,
                               tS32 s32OsalIOCtrl,
                               const char *pcszErrorTxt)
{
    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
       tUInt uiIndex;
       tU8  au8Buf[4 * sizeof(tU8) + 5 * sizeof(tU32) + 202]; //stack?
       OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();

       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32) CDAUDIO_TRACE_IOCTRL_RESULT);
       uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 11);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 12);
	   uiIndex += sizeof(tU8);
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 13);
	   uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Line);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
	   uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)u32Time);
	   uiIndex += sizeof(tU32);
	   OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)s32OsalIOCtrl);
	   uiIndex += sizeof(tU32);
	   strncpy((char*)&au8Buf[uiIndex], (const char*)pcszErrorTxt, 200);
       uiIndex += 200;
	   OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU32)0);
	   uiIndex += sizeof(tU8);

       // trace the stuff 
       // be aware index is not greater than array size!
       // I could not test it while runtime,
       // because L INT throws a senseless warning.
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex);
    }
}




/*****************************************************************************
* FUNCTION:     bIsOpen
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  checks, if device is open

* HISTORY:
******************************************************************************/
static tBool bIsOpen()
{
	tBool bOpen;
	if(OSAL_ERROR != ghDevice)
	{
		bOpen = TRUE;
	}
	else //if(OSAL_ERROR != ghDevice)
	{
		bOpen = FALSE;
		CDAUDIO_PRINTF_ERRORS("device not open");
	} //else //if(OSAL_ERROR != ghDevice)

	return bOpen;
}

/*****************************************************************************
* FUNCTION:     bIOCtrlOK
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  checks, if ioctrl succeeded

* HISTORY:
******************************************************************************/
static tBool bIOCtrlOK(tS32 s32Fun, tS32 s32Ret)
{
	tBool bOK = (s32Ret != OSAL_ERROR);
	CDAUDIO_PRINTF_IOCTRL_RESULT(s32Fun, bOK ? "SUCCESS" : pGetErrTxt());
	return bOK;
}


/*****************************************************************************
* FUNCTION:     vPlayInfoNotify
* PARAMETER:    pvCookie
* RETURNVALUE:  None
* DESCRIPTION:  This is a PlayInfo-Callback

* HISTORY:
******************************************************************************/
static tVoid vPlayInfoNotify(tPVoid pvCookie, const OSAL_trPlayInfo* prInfo)
{
 (void)pvCookie; // L I N T 

 CDAUDIO_PRINTF_FORCED("vPlayInfoNotify:"
					   " Track [%02u],"
					   " AbsMSF [%02u:%02u:%02u],"
					   " RelMSF [%02u:%02u:%02u],"
					   " PlayStatus [0x%08X]",
					   (unsigned int)prInfo->u32TrackNumber,
					   (unsigned int)prInfo->rAbsTrackAdr.u8MSFMinute,
					   (unsigned int)prInfo->rAbsTrackAdr.u8MSFSecond,
					   (unsigned int)prInfo->rAbsTrackAdr.u8MSFFrame,
					   (unsigned int)prInfo->rRelTrackAdr.u8MSFMinute,
					   (unsigned int)prInfo->rRelTrackAdr.u8MSFSecond,
					   (unsigned int)prInfo->rRelTrackAdr.u8MSFFrame,
					   (unsigned int)prInfo->u32StatusPlay
					  );


 //BPCD_OEDT_T2_s32PlayTimeSeconds =
 // (tS32)(prInfo->rAbsTrackAdr.u8MSFMinute) * 60
 //  + (tS32)(prInfo->rAbsTrackAdr.u8MSFSecond);
 /*if(0 == BPCD_OEDT_T2_s32FirstPlayTimeSeconds)
 {
	 BPCD_OEDT_T2_s32FirstPlayTimeSeconds = BPCD_OEDT_T2_s32PlayTimeSeconds;
 }*/

 //BPCD_OEDT_TRACE(BPCD_OEDT_T2_s32PlayTimeSeconds);
}



/*****************************************************************************
* FUNCTION:     vTraceCmdThread
* PARAMETER:    pvData
* RETURNVALUE:  None
* DESCRIPTION:  This is a Trace Thread handler.

* HISTORY:
******************************************************************************/
static tVoid vTraceCmdThread(tPVoid pvData)
{
	const tU8  *pu8Data = (const tU8*)pvData;
	const tU8  *pu8Par  = &pu8Data[3];
	tU8 u8Len    = pu8Data[1];
	tU8 u8Cmd    = pu8Data[2];

	(void)pvData;

	CDAUDIO_PRINTF_U1("vTraceCmdThread %p, TraceCmd[Len 0x%02X Cmd 0x%02X]",
					  pu8Data,
					  (unsigned int)u8Len,
					  (unsigned int)u8Cmd);
	switch(u8Cmd)
	{

	case CDAUDIO_TRACE_CMD_TRACE_ON:
		CDAUDIO_bTraceOn = TRUE;
		break;

	case CDAUDIO_TRACE_CMD_TRACE_OFF:
		CDAUDIO_bTraceOn = FALSE;
		break;

	case CDAUDIO_TRACE_CMD_OPEN:
		if(OSAL_ERROR == ghDevice)
		{
			ghDevice = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_CDAUDIO "/0",
									 OSAL_EN_READWRITE );

			if(ghDevice == OSAL_ERROR)
			{
				CDAUDIO_PRINTF_ERRORS("OPEN: [%s]", pGetErrTxt());
			}
			else //if(ghDevice == OSAL_ERROR)
			{
				CDAUDIO_PRINTF_FORCED("OPEN: handle 0x%08X",
									  (unsigned int)ghDevice);
			} //else //if(ghDevice == OSAL_ERROR)
		}
		else //if(OSAL_ERROR == ghDevice)
		{
			CDAUDIO_PRINTF_ERRORS("already open - handle 0x%08X", 
								  (unsigned int)ghDevice);
		} //else //if(OSAL_ERROR == ghDevice)
		break;

	case CDAUDIO_TRACE_CMD_CLOSE:
		if(OSAL_ERROR != ghDevice)
		{
			tS32 s32Ret = OSAL_s32IOClose (ghDevice);
			if(s32Ret == OSAL_ERROR)
			{
				CDAUDIO_PRINTF_ERRORS("CLOSE: [%s]", pGetErrTxt());
			}
			else //if(ghDevice == OSAL_ERROR)
			{
				CDAUDIO_PRINTF_FORCED("CLOSE: Success");
				ghDevice = OSAL_ERROR;
			} //else //if(ghDevice == OSAL_ERROR)
		}
		else //if(OSAL_ERROR != ghDevice)
		{
			CDAUDIO_PRINTF_ERRORS("not opened");
		} //else //if(OSAL_ERROR != ghDevice)
		break;

		/*case CDAUDIO_TRACE_CMD_IOCTRL:
			{
				tU8 u8SubCmd = pu8Data[3];
				CDAUDIO_PRINTF_U1("TraceCmd IOCTRL SubCmd[0x%02X]",
				                  (unsigned int)u8SubCmd);
				switch(u8SubCmd)
				{
				case 1:
					break;
				default:
					;
				} //switch(u8SubCmd)
			}
			break;*/
	case CDAUDIO_TRACE_CMD_PLAY:
		{
			CDAUDIO_PRINTF_U1("TraceCmd IOCTRL PLAY [0x%02X]",
							  (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_PLAY;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDAUDIO_TRACE_CMD_STOP:
		{
			CDAUDIO_PRINTF_U1("TraceCmd IOCTRL STOP [0x%02X]",
							  (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_STOP;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDAUDIO_TRACE_CMD_PAUSE:
		{
			CDAUDIO_PRINTF_U1("TraceCmd IOCTRL PAUSE [0x%02X]",
							  (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_PAUSE;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;

	case CDAUDIO_TRACE_CMD_RESUME:
		{
			CDAUDIO_PRINTF_U1("TraceCmd IOCTRL RESUME [0x%02X]",
							  (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_RESUME;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;

	case CDAUDIO_TRACE_CMD_FASTFORWARD:
		{
			CDAUDIO_PRINTF_U1("TraceCmd IOCTRL FF [0x%02X]",
							  (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTFORWARD;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;

	case CDAUDIO_TRACE_CMD_FASTBACKWARD:
		{
			CDAUDIO_PRINTF_U1("TraceCmd IOCTRL FB [0x%02X]",
							  (unsigned int)pu8Par[0]);
			if(bIsOpen())
			{
				tS32 s32Fun, s32Arg, s32Ret;
				s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_FASTBACKWARD;
				s32Arg = 0;
				s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
				(void)bIOCtrlOK(s32Fun, s32Ret);
			} //if(bIsOpen())
		}
		break;
	case CDAUDIO_TRACE_CMD_SETPLAYRANGE:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL SETPLAYRANGE [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			OSAL_trPlayRange rPlayRange;
			tS32 s32Fun, s32Arg, s32Ret;
			tU8  u8StartTrack   = (tU8) CDAUDIO_GET_U32_LE(&pu8Par[0]);
			tU16 u16StartOffset = (tU16)CDAUDIO_GET_U32_LE(&pu8Par[4]);
			tU8  u8EndTrack     = (tU8) CDAUDIO_GET_U32_LE(&pu8Par[8]);
			tU16 u16EndOffset   = (tU16)CDAUDIO_GET_U32_LE(&pu8Par[12]);

			rPlayRange.rStartAdr.u8Track   = u8StartTrack;
			rPlayRange.rStartAdr.u16Offset = u16StartOffset;
			rPlayRange.rEndAdr.u8Track     = u8EndTrack;
			rPlayRange.rEndAdr.u16Offset   = u16EndOffset;

			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_SETPLAYRANGE;
			s32Arg = (tS32)&rPlayRange;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			(void)bIOCtrlOK(s32Fun, s32Ret);
			(void)rPlayRange; // satisfy L i n t
		} //if(bIsOpen())
		break;
	case CDAUDIO_TRACE_CMD_SETMSF:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL SETMSF [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			OSAL_trAdrRange rPlayRange;
			tS32 s32Fun, s32Arg, s32Ret;
			tU8  u8SM = pu8Par[0];
			tU8  u8SS = pu8Par[1];
			tU8  u8SF = pu8Par[2];
			tU8  u8EM = pu8Par[3];
			tU8  u8ES = pu8Par[4];
			tU8  u8EF = pu8Par[5];

			rPlayRange.rStartAdr.u8MSFMinute = u8SM;
			rPlayRange.rStartAdr.u8MSFSecond = u8SS;
			rPlayRange.rStartAdr.u8MSFFrame  = u8SF;
			rPlayRange.rEndAdr.u8MSFMinute = u8EM;
			rPlayRange.rEndAdr.u8MSFSecond = u8ES;
			rPlayRange.rEndAdr.u8MSFFrame  = u8EF;

			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_SETMSF;
			s32Arg = (tS32)&rPlayRange;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			(void)bIOCtrlOK(s32Fun, s32Ret);
			(void)rPlayRange; // L i n T
		} //if(bIsOpen())
		break;
	case CDAUDIO_TRACE_CMD_GETCDINFO:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETCDINFO [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			tS32 s32Fun, s32Arg, s32Ret;
			OSAL_trCDAudioInfo rInfo;

			rInfo.u32MinTrack = 0;
			rInfo.u32MaxTrack = 0;
			rInfo.u32TrMinutes = 0;
			rInfo.u32TrSeconds = 0;
			rInfo.u32CdText = 0;
			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETCDINFO;
			s32Arg = (tS32)&rInfo;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			if(bIOCtrlOK(s32Fun, s32Ret))
			{
				CDAUDIO_PRINTF_FORCED("CDInfo:"
									  " MinTrack [%02u],"
									  " MaxTrack [%02u],"
									  " Min:Sec  [%02u:%02u],"
									  " CD-Text  [0x%08X]",
									  (unsigned int)rInfo.u32MinTrack,
									  (unsigned int)rInfo.u32MaxTrack,
									  (unsigned int)rInfo.u32TrMinutes,
									  (unsigned int)rInfo.u32TrSeconds,
									  (unsigned int)rInfo.u32CdText
									 );
			}
		} //if(bIsOpen())
		break;

	case CDAUDIO_TRACE_CMD_GETPLAYINFO:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETPLAYINFO [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			tS32 s32Fun, s32Arg, s32Ret;
			OSAL_trPlayInfo rInfo;

			rInfo.u32TrackNumber           = 0;
			rInfo.rAbsTrackAdr.u8MSFMinute = 0;
			rInfo.rAbsTrackAdr.u8MSFSecond = 0;
			rInfo.rAbsTrackAdr.u8MSFFrame  = 0;
			rInfo.rRelTrackAdr.u8MSFMinute = 0;
			rInfo.rRelTrackAdr.u8MSFSecond = 0;
			rInfo.rRelTrackAdr.u8MSFFrame  = 0;
			rInfo.u32StatusPlay            = 0;
			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETPLAYINFO;
			s32Arg = (tS32)&rInfo;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			if(bIOCtrlOK(s32Fun, s32Ret))
			{
				CDAUDIO_PRINTF_FORCED("PlayInfo:"
									  " Track [%02u],"
									  " AbsMSF [%02u:%02u:%02u],"
									  " RelMSF [%02u:%02u:%02u],"
									  " PlayStatus [0x%08X]",
								  (unsigned int)rInfo.u32TrackNumber,
								  (unsigned int)rInfo.rAbsTrackAdr.u8MSFMinute,
								  (unsigned int)rInfo.rAbsTrackAdr.u8MSFSecond,
								  (unsigned int)rInfo.rAbsTrackAdr.u8MSFFrame,
								  (unsigned int)rInfo.rRelTrackAdr.u8MSFMinute,
								  (unsigned int)rInfo.rRelTrackAdr.u8MSFSecond,
								  (unsigned int)rInfo.rRelTrackAdr.u8MSFFrame,
								  (unsigned int)rInfo.u32StatusPlay
								 );
			}
		} //if(bIsOpen())
		break;

	case CDAUDIO_TRACE_CMD_GETTRACKINFO:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETTRACKINFO [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			tU8 u8Track = pu8Par[0];
			tS32 s32Fun, s32Arg, s32Ret;
			OSAL_trTrackInfo rInfo;

			rInfo.u32TrackNumber        = u8Track;
			rInfo.rStartAdr.u8MSFMinute = 0;
			rInfo.rStartAdr.u8MSFSecond = 0;
			rInfo.rStartAdr.u8MSFFrame  = 0;
			rInfo.rEndAdr.u8MSFMinute = 0;
			rInfo.rEndAdr.u8MSFSecond = 0;
			rInfo.rEndAdr.u8MSFFrame  = 0;
			rInfo.u32TrackControl     = 0;

			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKINFO;
			s32Arg = (tS32)&rInfo;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			if(bIOCtrlOK(s32Fun, s32Ret))
			{
				CDAUDIO_PRINTF_FORCED("TrackInfo:"
									  " Track [%02u],"
									  " StartMSF [%02u:%02u:%02u],"
									  " EndMSF [%02u:%02u:%02u],"
									  " Control [0x%08X]",
									  (unsigned int)rInfo.u32TrackNumber,
									  (unsigned int)rInfo.rStartAdr.u8MSFMinute,
									  (unsigned int)rInfo.rStartAdr.u8MSFSecond,
									  (unsigned int)rInfo.rStartAdr.u8MSFFrame,
									  (unsigned int)rInfo.rEndAdr.u8MSFMinute,
									  (unsigned int)rInfo.rEndAdr.u8MSFSecond,
									  (unsigned int)rInfo.rEndAdr.u8MSFFrame,
									  (unsigned int)rInfo.u32TrackControl
									 );
			}
		} //if(bIsOpen())
		break;

	case CDAUDIO_TRACE_CMD_GETALBUMNAME:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETALBUMNAME [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			tU8 u8NameArray[OSAL_C_S32_CDAUDIO_MAXNAMESIZE+1];
			tS32 s32Fun, s32Arg, s32Ret;

			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETALBUMNAME;
			s32Arg = (tS32)u8NameArray;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			if(bIOCtrlOK(s32Fun, s32Ret))
			{
				CDAUDIO_PRINTF_FORCED("AlbumName: [%s]",
									  (const char *)u8NameArray
									 );
			}

		} //if(bIsOpen())
		break;

	case CDAUDIO_TRACE_CMD_GETTRACKCDINFO:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETTRACKCDINFO [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			OSAL_trTrackCdTextInfo rInfo;
			tS32 s32Fun, s32Arg, s32Ret;
			tU8 u8Track = pu8Par[0];

			rInfo.u32TrackNumber = (tU32)u8Track;
			/*OSAL_C_S32_CDAUDIO_MAXNAMESIZE*/
			rInfo.rPerformer[0]  = '\0';
			rInfo.rTrackTitle[0] = '\0';

			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETTRACKCDINFO;
			s32Arg = (tS32)&rInfo;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			if(bIOCtrlOK(s32Fun, s32Ret))
			{
				CDAUDIO_PRINTF_FORCED("TrackCDInfo:"
									  " Track  [%02u],"
									  " Artist  [%s],"
									  " Title   [%s]",
									  (unsigned int)rInfo.u32TrackNumber,
									  (const char*)rInfo.rPerformer,
									  (const char*)rInfo.rTrackTitle
									 );
			}
		} //if(bIsOpen())
		break;

	case CDAUDIO_TRACE_CMD_GETADDITIONALCDINFO:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL GETADDITIONALCDINFO [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			OSAL_trAdditionalCDInfo rInfo;
			tS32 s32Fun, s32Arg, s32Ret;

			rInfo.u32DataTracks[0] = 0;
			rInfo.u32DataTracks[1] = 0;
			rInfo.u32DataTracks[2] = 0;
			rInfo.u32DataTracks[3] = 0;
			rInfo.u32InfoBits   = 0;

			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_GETADDITIONALCDINFO;
			s32Arg = (tS32)&rInfo;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			if(bIOCtrlOK(s32Fun, s32Ret))
			{
				CDAUDIO_PRINTF_FORCED("AdditionalCDInfo:"
									  " DataTrackBits"
									  " [0x%08X]"
									  " [0x%08X]"
									  " [0x%08X]"
									  " [0x%08X],"
									  " InfoBits [0x%08X],",
									  (unsigned int)rInfo.u32DataTracks[0],
									  (unsigned int)rInfo.u32DataTracks[1],
									  (unsigned int)rInfo.u32DataTracks[2],
									  (unsigned int)rInfo.u32DataTracks[3],
									  (unsigned int)rInfo.u32InfoBits
									 );
			}
		} //if(bIsOpen())
		break;

	case CDAUDIO_TRACE_CMD_REGPLAYNOTIFY:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL REGPLAYNOTIFY [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			tS32 s32Fun, s32Arg, s32Ret;
			OSAL_trPlayInfoReg rReg;

			rReg.pCallback = vPlayInfoNotify;
			rReg.pvCookie  = NULL;
			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_REGPLAYNOTIFY;
			s32Arg = (tS32)&rReg;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			(void)bIOCtrlOK(s32Fun, s32Ret);
			(void)rReg;
		} //if(bIsOpen())
		break;

	case CDAUDIO_TRACE_CMD_UNREGPLAYNOTIFY:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL UNREGPLAYNOTIFY [0x%02X]",
						  (unsigned int)pu8Par[0]);
		if(bIsOpen())
		{
			tS32 s32Fun, s32Arg, s32Ret;
			OSAL_trPlayInfoReg rReg;

			rReg.pCallback = vPlayInfoNotify;
			rReg.pvCookie  = NULL;
			s32Fun = OSAL_C_S32_IOCTRL_CDAUDIO_UNREGPLAYNOTIFY;
			s32Arg = (tS32)&rReg;
			s32Ret = OSAL_s32IOControl(ghDevice, s32Fun, s32Arg);
			(void)bIOCtrlOK(s32Fun, s32Ret);
			(void)rReg;
		} //if(bIsOpen())
		break;


	case CDAUDIO_TRACE_CMD_TEST_CDTEXT:
		CDAUDIO_PRINTF_U1("TraceCmd TEST CDTEXT [0x%02X] [0x%02X] [0x%02X]",
						  (unsigned int)pu8Par[0],
						  (unsigned int)pu8Par[1],
						  (unsigned int)pu8Par[2]);
		{
			tU8 u8MaxTrack = pu8Par[0];
			tU8 u8MinTextLen  = pu8Par[1];
			tU8 u8MaxTextLen  = pu8Par[2];
			tU8 u8L;

			for(u8L = u8MinTextLen ; u8L <= u8MaxTextLen ; u8L++)
			{
				//(tVoid)CDAUDIO_bDebugTestAtaCDText(u8MaxTrack, u8L);
			}
		} //if(bIsOpen())
		break;



	default:
		CDAUDIO_PRINTF_U1("TraceCmd IOCTRL DEFAULT - unknown command 0x%02X",
						  (unsigned int)u8Cmd);
		;
	} //switch(u8Cmd)



	gTraceCmdThreadIsRunning = FALSE; //simple sync
	OSAL_vThreadExit();
}




/*****************************************************************************
* FUNCTION:     CDAUDIO_vTraceCommandCallbackHandler
* PARAMETER:    buffer
* RETURNVALUE:  None
* DESCRIPTION:  This is a Trace callback handler.

* HISTORY:
******************************************************************************/
tVoid CDAUDIO_vTraceCommandCallbackHandler(const tU8* pcu8Buffer)
{
	CDAUDIO_TRACE_ENTER_U4(CDAUDIO_vTraceCommandCallbackHandler,
						  (tU32)(pcu8Buffer!=NULL?pcu8Buffer[0]:0xFF),
						  (tU32)(pcu8Buffer!=NULL?pcu8Buffer[1]:0xFF),
						  (tU32)(pcu8Buffer!=NULL?pcu8Buffer[2]:0xFF),
						  (tU32)(pcu8Buffer!=NULL?pcu8Buffer[3]:0xFF)
						  );
    if(pcu8Buffer != NULL)
    {
		if(!gTraceCmdThreadIsRunning)
		{
			OSAL_trThreadAttribute  traceCmdThreadAttribute;
			OSAL_tThreadID          traceCmdThreadID; //l i n t  = OSAL_ERROR;
			tU8 u8Len   = pcu8Buffer[0];
			gTraceCmdThreadIsRunning = TRUE;

			(void)OSAL_pvMemoryCopy(gu8TraceBuffer, pcu8Buffer, u8Len + 1);

			traceCmdThreadAttribute.szName       = "CDAUDIOTRACE";
			traceCmdThreadAttribute.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_LOWEST;
			traceCmdThreadAttribute.pfEntry      = vTraceCmdThread;
			traceCmdThreadAttribute.pvArg        = gu8TraceBuffer;
			traceCmdThreadAttribute.s32StackSize = 1024;

			traceCmdThreadID = OSAL_ThreadSpawn(&traceCmdThreadAttribute);
			if(OSAL_ERROR == traceCmdThreadID)
			{
				CDAUDIO_PRINTF_ERRORS("Create TraceThread");
			} //if(OSAL_ERROR == traceCmdThreadID)
		}
		else //if(!gTraceCmdThreadIsRunning)
		{
			CDAUDIO_PRINTF_ERRORS("last Trace thread is running yet");
		} //else //if(!gTraceCmdThreadIsRunning)
    } //if(pcu8Buffer != NULL)
	CDAUDIO_TRACE_LEAVE_U4(CDAUDIO_vTraceCommandCallbackHandler,OSAL_E_NOERROR,
						  0,0,0,0);
}




/*****************************************************************************
* FUNCTION:     CDAUDIO_vRegTrace
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  register trace command channel
* HISTORY:
******************************************************************************/
#ifdef CDAUDIO_IS_USING_LOCAL_TRACE
tVoid CDAUDIO_vRegTrace(tVoid)
{
	tBool bRet;
	bRet = TR_chan_acess_bRegChan(TR_TTFIS_CDAUDIO,
							   (TRACE_CALLBACK)
							              CDAUDIO_vTraceCommandCallbackHandler);
	if(!bRet)
	{
		CDAUDIO_PRINTF_ERRORS("CDAUDIO_vRegTrace -"
							 " Command Channel 0x%04X not registered",
							  (unsigned int)TR_TTFIS_CDCTRL);
	} //if(!bRet)
}
#endif //#ifdef CDAUDIO_IS_USING_LOCAL_TRACE

/*****************************************************************************
* FUNCTION:     CDAUDIO_vUnregTrace
* PARAMETER:    
* RETURNVALUE:  None
* DESCRIPTION:  unregister trace command channel
* HISTORY:
******************************************************************************/
#ifdef CDAUDIO_IS_USING_LOCAL_TRACE
tVoid CDAUDIO_vUnregTrace(tVoid)
{
	
	TR_chan_acess_bUnRegChan(TR_TTFIS_CDAUDIO,
	                        (TRACE_CALLBACK)
							              CDAUDIO_vTraceCommandCallbackHandler);

}
#endif //#ifdef CDAUDIO_IS_USING_LOCAL_TRACE



#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file 
|-----------------------------------------------------------------------*/


