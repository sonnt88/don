/************************************************************************
| FILE:         cdaudio.h
| PROJECT:      Gen2
| SW-COMPONENT: CDAUDIO driver
|------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cdaudio.h
 *
 * @brief   This file includes public stuff for the cdaudio driver.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 *
 *//* ***************************************************FileHeaderEnd******* */

#ifndef CDAUDIO_H
#define CDAUDIO_H

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
// NOTE: These macros require atapi_if.h to be included before this header
#define CDAUDIO_ADDITIONALCDINFO_IS_DATA_TRACK(_U32ARRAY_, _TRACKINDEX_)    ATAPI_IF_ADDITIONALCDINFO_IS_DATA_TRACK(_U32ARRAY_, _TRACKINDEX_)
#define CDAUDIO_ADDITIONALCDINFO_IS_DATA_TRACK_NUMBER_1(_U32ARRAY_)         ATAPI_IF_ADDITIONALCDINFO_IS_DATA_TRACK_NUMBER_1(_U32ARRAY_)
#define CDAUDIO_ADDITIONALCDINFO_SET_DATA_TRACK(_U32ARRAY_, _TRACKINDEX_)   ATAPI_IF_ADDITIONALCDINFO_SET_DATA_TRACK(_U32ARRAY_, _TRACKINDEX_)
#define CDAUDIO_ADDITIONALCDINFO_CLR_DATA_TRACK(_U32ARRAY_, _TRACKINDEX_)   ATAPI_IF_ADDITIONALCDINFO_CLR_DATA_TRACK(_U32ARRAY_, _TRACKINDEX_)

#define CDAUDIO_ADDITIONALCDINFO_NULL_VALUE                 ATAPI_IF_ADDITIONALCDINFO_NULL_VALUE
#define CDAUDIO_ADDITIONALCDINFO_FIRST_TRACK_DATA_BITMASK   ATAPI_IF_ADDITIONALCDINFO_FIRST_TRACK_DATA_BITMASK

#define CDAUDIO_MAXTRACKS       ATAPI_IF_MAXTRACKS
#define CDAUDIO_LEADOUT_INDEX   ATAPI_IF_LEADOUT_INDEX

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
//tS32 CDAUDIO_s32Init(void);

#ifdef __cplusplus
}
#endif

#else //CDAUDIO_H
 #error cdaudio.h included several times
#endif //#else //CDAUDIO_H
