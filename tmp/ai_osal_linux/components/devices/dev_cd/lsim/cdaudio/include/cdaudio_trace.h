/************************************************************************
| FILE:         cdaudio_trace.h
| PROJECT:      Gen2
| SW-COMPONENT: CDAUDIO driver
|------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
 * @file    cdaudio_trace.h
 *
 * @brief   This file includes trace stuff for the cdaudio driver.
 *
 * @author  srt2hi
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; Bosch
 * ------------------------------------------------------------------
 | Date      		| Modification                            | Author
 | 07/07/2016       | Fix for CMG3GB-3460                     | boc7kor
 |                  | CDAUDIO_IS_USING_LOCAL_TRACE is defined |
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (CDAUDIO_TRACE_H)
   #define CDAUDIO_TRACE_H
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/
// if defined, cdaudio registering its own trace command channel
#define CDAUDIO_IS_USING_LOCAL_TRACE//fix for CMG3GB-3460


#define CDAUDIO_TRACE_ENABLED


/*trace classes used by dev cdaudio, defined in osalcore/include/ostrace.h*/
#define CDAUDIO_TRACE_CLASS OSAL_C_TR_CLASS_DEV_CDAUDIO

#ifdef CDAUDIO_TRACE_ENABLED

#define CDAUDIO_TRACE_ENTER_U2(FUNC,P1,P2,P3,P4) if(CDAUDIO_bTraceOn)\
 CDAUDIO_vTraceEnter(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_2,\
                   __LINE__, #FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define CDAUDIO_TRACE_ENTER_U3(FUNC,P1,P2,P3,P4) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTraceEnter(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_3,\
                   __LINE__, #FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define CDAUDIO_TRACE_ENTER_U4(FUNC,P1,P2,P3,P4) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTraceEnter(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_4,\
                   __LINE__, #FUNC, (tU32)(P1), (tU32)(P2),\
				    (tU32)(P3), (tU32)(P4))

#define CDAUDIO_TRACE_LEAVE_U2(FUNC,ERROR,P1,P2,P3,P4) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTraceLeave(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_2,\
                   __LINE__, #FUNC, (ERROR), (tU32)(P1),\
				   (tU32)(P2), (tU32)(P3), (tU32)(P4))

#define CDAUDIO_TRACE_LEAVE_U3(FUNC,ERROR,P1,P2,P3,P4) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTraceLeave(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_3,\
                   __LINE__, #FUNC, (ERROR), (tU32)(P1),\
				   (tU32)(P2), (tU32)(P3), (tU32)(P4))

#define CDAUDIO_TRACE_LEAVE_U4(FUNC,ERROR,P1,P2,P3,P4) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTraceLeave(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_4,\
                   __LINE__, #FUNC, (ERROR), (tU32)(P1),\
				   (tU32)(P2),  (tU32)(P3),  (tU32)(P4))


#define CDAUDIO_TRACE_IOCTRL(IOCTRL,P1,P2,P3,P4) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTraceIoctrl(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_1,\
                   __LINE__, (IOCTRL), (tU32)(P1), (tU32)(P2),\
				   (tU32)(P3), (tU32)(P4))

#define CDAUDIO_TRACE_IOCTRL_TXT(IOCTRL,P1,P2,PTXT) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTraceIoctrlTxt(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_1,\
                   __LINE__, (IOCTRL), (tU32)(P1), (tU32)(P2), (PTXT))

#define CDAUDIO_PRINTF_IOCTRL_RESULT(OSALIOCTRL,OSALERRORTXT) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTraceIoctrlResult(CDAUDIO_TRACE_CLASS, TR_LEVEL_ERRORS,\
                     __LINE__, (OSALIOCTRL), (OSALERRORTXT))


#define CDAUDIO_PRINTF_FORCED(...)\
 CDAUDIO_vTracePrintf(CDAUDIO_TRACE_CLASS, TR_LEVEL_FATAL, TRUE,\
                     __LINE__, __VA_ARGS__)

#define CDAUDIO_PRINTF_FATAL(...)\
 CDAUDIO_vTracePrintf(CDAUDIO_TRACE_CLASS, TR_LEVEL_FATAL, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CDAUDIO_PRINTF_ERRORS(...)\
 CDAUDIO_vTracePrintf(CDAUDIO_TRACE_CLASS, TR_LEVEL_ERRORS, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CDAUDIO_PRINTF_U1(...) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTracePrintf(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_1, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CDAUDIO_PRINTF_U2(...) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTracePrintf(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_2, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CDAUDIO_PRINTF_U3(...) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTracePrintf(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_3, FALSE,\
                      __LINE__, __VA_ARGS__)

#define CDAUDIO_PRINTF_U4(...) if(CDAUDIO_bTraceOn) \
 CDAUDIO_vTracePrintf(CDAUDIO_TRACE_CLASS, TR_LEVEL_USER_4, FALSE,\
                      __LINE__, __VA_ARGS__)

#else //#ifdef CDAUDIO_TRACE_ENABLED
	#define CDAUDIO_TRACE_ENTER_U2(FUNC,P1,P2,P3,P4)
	#define CDAUDIO_TRACE_ENTER_U3(FUNC,P1,P2,P3,P4)
	#define CDAUDIO_TRACE_ENTER_U4(FUNC,P1,P2,P3,P4)
	#define CDAUDIO_TRACE_LEAVE_U2(FUNC,ERROR,P1,P2,P3,P4)
	#define CDAUDIO_TRACE_LEAVE_U3(FUNC,ERROR,P1,P2,P3,P4)
	#define CDAUDIO_TRACE_LEAVE_U4(FUNC,ERROR,P1,P2,P3,P4)
	#define CDAUDIO_TRACE_IOCTRL(IOCTRL,P1,P2,P3,P4)
	#define CDAUDIO_TRACE_IOCTRL_TXT(IOCTRL,P1,P2,PTXT)
	#define CDAUDIO_PRINTF_IOCTRL_RESULT(OSALIOCTRL,OSALERRORTXT)
	#define CDAUDIO_PRINTF_FORCED(...)
	#define CDAUDIO_PRINTF_FATAL(...)
	#define CDAUDIO_PRINTF_ERRORS(...)
	#define CDAUDIO_PRINTF_U1(...)
	#define CDAUDIO_PRINTF_U2(...)
	#define CDAUDIO_PRINTF_U3(...)
	#define CDAUDIO_PRINTF_U4(...)
#endif //#ifdef CDAUDIO_TRACE_ENABLED

/** trace messages */
/** trace message IDs */
enum CDAUDIO_enTraceMessages
{
    CDAUDIO_TRACE_ENTER = 1,
    CDAUDIO_TRACE_LEAVE,
	CDAUDIO_TRACE_PRINTF,
	CDAUDIO_TRACE_PRINTF_ERROR,
	CDAUDIO_TRACE_IOCTRL,
	CDAUDIO_TRACE_IOCTRL_RESULT
};

/* Traces - these are not names of IOCTRLs!*/
typedef enum
{
	CDAUDIO_EN_IOCONTROL,
	CDAUDIO_EN_IOCONTROL_RESULT,
	CDAUDIO_EN_SETPLAYRANGE,
	CDAUDIO_EN_SETMSF,
	CDAUDIO_EN_GETCDINFO,
	CDAUDIO_EN_GETPLAYINFO, //5
	CDAUDIO_EN_GETTRACKINFO,
	CDAUDIO_EN_GETALBUMNAME,
	CDAUDIO_EN_GETTRACKCDINFO_ARTIST,
	CDAUDIO_EN_GETTRACKCDINFO_TITLE,
	CDAUDIO_EN_GETADDITIONALCDINFO_INFO, //10
	CDAUDIO_EN_GETADDITIONALCDINFO_TRACKS,
	CDAUDIO_EN_REGPLAYNOTIFY,
	CDAUDIO_EN_UNREGPLAYNOTIFY
}CDAUDIO_enumIoctrlTraceID;

/** trace command messages */
enum CDAUDIO_enTraceCommandMessages
{
	CDAUDIO_TRACE_CMD_TRACE_ON           = 0x01,
	CDAUDIO_TRACE_CMD_TRACE_OFF          = 0x02,
	CDAUDIO_TRACE_CMD_OPEN               = 0x03,
	CDAUDIO_TRACE_CMD_CLOSE              = 0x04,
	//CDAUDIO_TRACE_CMD_IOCTRL = 0x03,
	CDAUDIO_TRACE_CMD_PLAY               = 0x10,
	CDAUDIO_TRACE_CMD_STOP               = 0x11,
	CDAUDIO_TRACE_CMD_PAUSE              = 0x12,
	CDAUDIO_TRACE_CMD_RESUME             = 0x13,
	CDAUDIO_TRACE_CMD_FASTFORWARD        = 0x14,
	CDAUDIO_TRACE_CMD_FASTBACKWARD       = 0x15,
	CDAUDIO_TRACE_CMD_SETPLAYRANGE       = 0x16,
	CDAUDIO_TRACE_CMD_SETMSF             = 0x17,
	CDAUDIO_TRACE_CMD_GETCDINFO          = 0x18,
	CDAUDIO_TRACE_CMD_GETPLAYINFO        = 0x19,
	CDAUDIO_TRACE_CMD_GETTRACKINFO       = 0x1A,
	CDAUDIO_TRACE_CMD_GETALBUMNAME       = 0x1B,
	CDAUDIO_TRACE_CMD_GETTRACKCDINFO      = 0x1C,
	CDAUDIO_TRACE_CMD_GETADDITIONALCDINFO = 0x1D,
	CDAUDIO_TRACE_CMD_REGPLAYNOTIFY       = 0x1E,
	CDAUDIO_TRACE_CMD_UNREGPLAYNOTIFY     = 0x1F,

	CDAUDIO_TRACE_CMD_TEST_CDTEXT         = 0x30
};



/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/                     

#ifdef CDAUDIO_IS_USING_LOCAL_TRACE
tVoid CDAUDIO_vRegTrace(tVoid);
tVoid CDAUDIO_vUnregTrace(tVoid);
#endif //#ifdef CDAUDIO_IS_USING_LOCAL_TRACE


tVoid CDAUDIO_vTraceEnter(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pcvFunction,
                         tU32 u32Par1,
                         tU32 u32Par2,
                         tU32 u32Par3,
                         tU32 u32Par4);

tVoid CDAUDIO_vTraceLeave(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         const char *pcvFunction,
                         tU32 u32OSALError,
                         tU32 u32Par1,
                         tU32 u32Par2,
                         tU32 u32Par3,
                         tU32 u32Par4);


tVoid CDAUDIO_vTraceIoctrl(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         CDAUDIO_enumIoctrlTraceID enIoctrlTraceID,
                         tU32 u32Par1,
                         tU32 u32Par2,
                         tU32 u32Par3,
                         tU32 u32Par4);

tVoid CDAUDIO_vTraceIoctrlTxt(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         CDAUDIO_enumIoctrlTraceID enIoctrlTraceID,
						 tU32 u32Par1,
						 tU32 u32Par2,
                         const char * pcszTxt);

tVoid CDAUDIO_vTraceIoctrlResult(tU32 u32Class,
                         TR_tenTraceLevel enTraceLevel,
                         tU32 u32Line,
                         tS32 s32IoCtrl,
                         const char* pcszErrorTxt);


tVoid CDAUDIO_vTracePrintf(tU32 u32Class,
                          TR_tenTraceLevel enTraceLevel,
						  tBool bForced,
                          tU32 u32Line,
                          const char* coszFormat,...);



#ifdef __cplusplus
}
#endif
     
#else
#error cdaudio_trace.h included several times
#endif                

