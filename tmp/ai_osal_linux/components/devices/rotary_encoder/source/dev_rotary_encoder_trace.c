/* *********************************************************************** */
/*  includes                                                               */
/* *********************************************************************** */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

//#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
//#include "system_pif.h"

#include "dev_rotary_encoder.h"
#include "dev_rotary_encoder_trace.h"

/* ************************************************************************/
/*  defines                                                               */
/* ************************************************************************/

/* *********************************************************************** */
/*  typedefs enum                                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct                                                        */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static variables                                                       */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static  functions                                                      */
/* *********************************************************************** */
