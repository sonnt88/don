/* *********************************************************************** */
/*  includes                                                               */
/* *********************************************************************** */
#include "OsalConf.h"

#include <sys/socket.h>
#include <netinet/in.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "dev_rotary_encoder.h"

/* ************************************************************************/
/*  defines                                                               */
/* ************************************************************************/
//#define DEBUG_PRINT(...)    printf(__VA_ARGS__)
#define DEBUG_PRINT(...)

#define DEV_ROTARY_ENCODER_RECV_THREAD_NAME "ROT_ENC_IO_Thread"
#define DEV_ROTARY_ENCODER_SEM_NAME         "ROT_ENC_SEM"

#define DEV_ROTARY_ENCODER_NUM_TYPES        ((tU32)ROTENC_EN_ROT_RIGHT + 1)

/* *********************************************************************** */
/*  typedefs enum                                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct                                                        */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static variables                                                       */
/* *********************************************************************** */  
static OSAL_tSemHandle          DevRotaryEncoderHandleSemaphore = OSAL_C_INVALID_HANDLE;
static int                      DevRotaryEncoderSocketFD = -1;
static int                      DevRotaryEncoderConnectionFD = -1;

/* *********************************************************************** */
/*  global variables                                                       */
/* *********************************************************************** */
extern trGlobalOsalData *pOsalData;

/* *********************************************************************** */
/*  function prototypes                                                    */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static functions                                                       */
/* *********************************************************************** */

/* *********************************************************************** */
/*  global functions                                                       */
/* *********************************************************************** */

/******************************************************************************
 *FUNCTION      :DEV_ROTARY_ENCODER_ReceiveThread
 *
 *DESCRIPTION   :Thread to read from the socket
 *
 *PARAMETER     :Arg    unused
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
void DEV_ROTARY_ENCODER_ReceiveThread(void *Arg)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;

    (void)Arg;

    DEBUG_PRINT("%s started\n", __func__);
    if(OSAL_s32SemaphoreOpen(DEV_ROTARY_ENCODER_SEM_NAME, &semhandle) == OSAL_OK)
    {
        while(DevRotaryEncoderSocketFD != -1)
        {
            if(DevRotaryEncoderConnectionFD == -1)
            {
                DevRotaryEncoderConnectionFD = accept(DevRotaryEncoderSocketFD, NULL, NULL);
                if(DevRotaryEncoderConnectionFD == -1)
                {
                    OSAL_s32ThreadWait(10);
                }
                else
                {
                    DEBUG_PRINT("Connected!\n");
                }
            }
            else
            {
                static const int    buffsize = 64;
                unsigned char       buff[buffsize];
                tS32                buffbytes;
                tS32                inmsg = FALSE;

                // | 0+1    | 2   | 3   | 4   | 5+6   | 7     |
                // | 0xF00D | Len | Pos | Dir | Count | State |

                buffbytes = 0;
                for(; ; )
                {
                    tS32    i, numbytes;
                    tS32    msglen;
                    tS32    checkmsg;

                    numbytes = recv(DevRotaryEncoderConnectionFD, buff + buffbytes, buffsize - buffbytes, 0);

                    if(numbytes <= 0)
                    {
                        // recv failed for some reason - probably the connection was closed
                        DEBUG_PRINT("%s Receive failed with %i\n", __func__, numbytes);
                        if(DevRotaryEncoderConnectionFD != -1)
                        {
                            close(DevRotaryEncoderConnectionFD);
                            DevRotaryEncoderConnectionFD = -1;
                        }
                        break;
                    }
                    DEBUG_PRINT("%s Received: %i Bytes\n", __func__, numbytes);

                    buffbytes += numbytes;
                    for(checkmsg = TRUE; checkmsg == TRUE; )
                    {
                        checkmsg = FALSE;

                        // If there was no message start tag, check if there is one now
                        for(i = 0; inmsg == FALSE && i < buffbytes - 1; i++)
                        {
                            if(*((tU16 *)(&buff[i])) == DEV_ROTARY_ENCODER_TCP_MSG_START)
                            {
                                // Shift data in buffer - discard any garbage and drop message start tag
                                memmove(buff, buff + i + sizeof(tU16), buffsize - i - sizeof(tU16));
                                buffbytes -= i + sizeof(tU16);
                                inmsg = TRUE;
                                msglen = -1;
                                DEBUG_PRINT("%s Found tag\n", __func__);
                            }
                        }

                        if(inmsg == FALSE)
                        {
                            if(buffbytes > 1)
                            {
                                // There is some garbage in the buffer
                                // Discard garbage but make sure to keep the last byte as it could be the first byte of a message start tag
                                memmove(buff, buff + buffbytes - 1, buffbytes - 1);
                                DEBUG_PRINT("%s Discarded %i bytes of garbage\n", __func__, buffbytes);
                                buffbytes = 1;
                            }
                        }
                        else
                        {
                            // There was a message start tag
                            // Now check if there is enough data available to determine the message's length
                            if(msglen < 0 && buffbytes >= 1)
                            {
                                msglen = buff[0];
                                //if(msglen < 1 || msglen > buffsize)
                                if(msglen != 6)     // For now, there is exactly one kind of message...
                                {
                                    // The length of this message is not reasonable, discard it
                                    inmsg = FALSE;
                                    msglen = -1;
                                    DEBUG_PRINT("%s Found invalid size\n", __func__);
                                }
                            }

                            // Check if there is a complete message available
                            if(inmsg == TRUE && msglen > 0 && buffbytes >= msglen)
                            {
                                // Received at least one complete message
                                OSAL_trRotEncoderData   arg;
                                tS32                    argvalid = TRUE;

                                arg.tenRot_Type = (OSAL_tenRotEncoderType)buff[1];
                                arg.tenRot_Dir  = (OSAL_tenRotEncoderDir)buff[2];
                                arg.u32Count    = (tU32)*((tU16 *)&buff[3]);

                                if((tS32)arg.tenRot_Type < 0 || (tS32)arg.tenRot_Type >= DEV_ROTARY_ENCODER_NUM_TYPES)
                                    argvalid = FALSE;

                                // FIXME: Sanity check for values

                                if(argvalid == TRUE)
                                {
                                    // FIXME: Not safe! Might overwrite pOsalData->DevRotaryEncoderCBArgs with new data before callback is executed!
                                    if(OSAL_s32SemaphoreWait(semhandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                                    {
                                        if(pOsalData->DevRotaryEncoderCallback != NULL)
                                        {
                                            // Send it out, if someone is interested
                                            OSAL_trRotEncoderData    *osal_arg = &pOsalData->DevRotaryEncoderCBArgs[pOsalData->DevRotaryEncoderCBNextArg];

                                            memcpy(osal_arg, &arg, sizeof(arg));
                                            u32ExecuteCallback(OSAL_ProcessWhoAmI(), pOsalData->DevRotaryEncoderCallbackProcID, pOsalData->DevRotaryEncoderCallback, osal_arg, sizeof(arg));

                                            if(++pOsalData->DevRotaryEncoderCBNextArg >= OSAL_C_DEV_ROT_ENCODER_ARGS_MAX)
                                                pOsalData->DevRotaryEncoderCBNextArg = 0;
                                            DEBUG_PRINT("%s Message delivered to callback\n", __func__);
                                        }
                                        OSAL_s32SemaphorePost(semhandle);
                                    }
                                }

                                memmove(buff, buff + msglen, buffsize - msglen);
                                inmsg = FALSE;
                                buffbytes -= msglen;
                                msglen = -1;

                                checkmsg = TRUE;
                                DEBUG_PRINT("%s Message processed\n", __func__);
                            }
                        }
                    }
                }
            }
        }
        OSAL_s32SemaphoreClose(semhandle);
        OSAL_s32SemaphoreDelete(DEV_ROTARY_ENCODER_SEM_NAME);
    }
    DEBUG_PRINT("%s ended\n", __func__);
}



/******************************************************************************
 *FUNCTION      :DEV_ROTARY_ENCODER_s32IODeviceInit
 *
 *DESCRIPTION   :Initializes the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_ROTARY_ENCODER_s32IODeviceInit(void)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;
    int             port = 3457;
    tS32            retval = OSAL_E_NOERROR;

    if(OSAL_s32SemaphoreCreate(DEV_ROTARY_ENCODER_SEM_NAME, &semhandle, 1) != OSAL_OK)
    {
        retval = (tS32)OSAL_u32ErrorCode();
    }
    else
    {
        OSAL_s32SemaphoreClose(semhandle);
    }

    if(retval == OSAL_E_NOERROR)
    {
        OSAL_tIODescriptor  fd = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/LSIM/ROTARY_ENCODER", OSAL_EN_READONLY);

        // It is not a problem and not an error if access to the registry fails. Defaults will be used.
        if(fd != OSAL_ERROR)
        {
            OSAL_trIOCtrlRegistry   entry;
            tS32                    val;

            entry.pcos8Name = "PORT";
            entry.u32Size   = sizeof(val);
            entry.s32Type   = OSAL_C_S32_VALUE_S32;
            entry.ps8Value  = (tU8 *)&val;
            if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
                port = (int)(*(tS32 *)entry.ps8Value);

            OSAL_s32IOClose (fd);
        }

        DevRotaryEncoderSocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
        if(DevRotaryEncoderSocketFD == -1)
        {
DEBUG_PRINT("socket failed\n");
            retval = OSAL_ERROR;
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        int     flags = fcntl(DevRotaryEncoderSocketFD, F_GETFL, 0);
        fcntl(DevRotaryEncoderSocketFD, F_SETFL, flags | O_NONBLOCK);
    }

    if(retval == OSAL_E_NOERROR)
    {
        struct sockaddr_in      sockaddr;

        memset(&sockaddr, 0, sizeof(sockaddr));

        sockaddr.sin_family = AF_INET;
        sockaddr.sin_port = htons(port);
        sockaddr.sin_addr.s_addr = INADDR_ANY;

        if(bind(DevRotaryEncoderSocketFD, (struct sockaddr *)&sockaddr, sizeof(sockaddr)) == -1)
        {
            DEBUG_PRINT("bind failed\n");
            retval = OSAL_ERROR;
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        if(listen(DevRotaryEncoderSocketFD, 0) == -1)
        {
            DEBUG_PRINT("listen failed\n");
            retval = OSAL_ERROR;
        }
        else
        {
            DEBUG_PRINT("Listener opened for port %i\n", port);
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        OSAL_trThreadAttribute  thrdattr;

        thrdattr.szName         = DEV_ROTARY_ENCODER_RECV_THREAD_NAME;
        thrdattr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        thrdattr.s32StackSize   = 4096;
        thrdattr.pfEntry        = DEV_ROTARY_ENCODER_ReceiveThread;
        thrdattr.pvArg          = NULL;

        if(OSAL_ThreadSpawn((OSAL_trThreadAttribute* )&thrdattr) == OSAL_ERROR)
        {
            retval = OSAL_ERROR;
        }
    }

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_ROTARY_ENCODER_s32IODeviceRemove
 *
 *DESCRIPTION   :Removes the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
void DEV_ROTARY_ENCODER_s32IODeviceRemove(void)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;

    if(OSAL_s32SemaphoreOpen(DEV_ROTARY_ENCODER_SEM_NAME, &semhandle) == OSAL_OK)
    {
        if(OSAL_s32SemaphoreWait(semhandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
        {
            pOsalData->DevRotaryEncoderCallbackProcID = 0;
            pOsalData->DevRotaryEncoderCallback = NULL;

            if(DevRotaryEncoderConnectionFD != -1)
            {
                shutdown(DevRotaryEncoderConnectionFD, SHUT_RDWR);
                DevRotaryEncoderConnectionFD = -1;
            }

            if(DevRotaryEncoderSocketFD != -1)
            {
                close(DevRotaryEncoderSocketFD);
                DevRotaryEncoderSocketFD = -1;
            }
            OSAL_s32SemaphorePost(semhandle);
            OSAL_s32SemaphoreClose(semhandle);
        }
    }
}



/******************************************************************************
 *FUNCTION      :DEV_ROTARY_ENCODER_s32IOOpen
 *
 *DESCRIPTION   :Open the device
 *
 *PARAMETER     :enAccess   Desired access, one of
 *                              OSAL_EN_WRITEONLY
 *                              OSAL_EN_READWRITE
 *                              OSAL_EN_READONLY
 *
 *RETURNVALUE   :tS32   device handle on success or
 *                      OSAL_ERROR/OSAL_E_ALREADYOPENED in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_ROTARY_ENCODER_s32IOOpen(OSAL_tenAccess enAccess)
{
    tS32    retval = OSAL_E_NOERROR;

    if((enAccess != OSAL_EN_READONLY) && (enAccess != OSAL_EN_WRITEONLY) && (enAccess != OSAL_EN_READWRITE))
    {
        retval = OSAL_ERROR;
    }
    else if(pOsalData->DevRotaryEncoderOpenCount > 0)
    {
        retval = OSAL_E_ALREADYOPENED;
    }
    else
    {
        if(OSAL_s32SemaphoreOpen(DEV_ROTARY_ENCODER_SEM_NAME, &DevRotaryEncoderHandleSemaphore) != OSAL_OK)
        {
            retval = OSAL_ERROR;
        }
        else
        {
            pOsalData->DevRotaryEncoderOpenCount++;
        }
    }

    // FIXME: Return a handle
    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_ROTARY_ENCODER_s32IOClose
 *
 *DESCRIPTION   :Close the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_ROTARY_ENCODER_s32IOClose(void)
{
    tS32    retval = OSAL_E_NOERROR;

    if(pOsalData->DevRotaryEncoderOpenCount <= 0)
    {
        retval = OSAL_ERROR;
    }
    else
    {
        if(DevRotaryEncoderHandleSemaphore != OSAL_C_INVALID_HANDLE)
        {
            pOsalData->DevRotaryEncoderOpenCount--;
            OSAL_s32SemaphoreClose(DevRotaryEncoderHandleSemaphore);
            DevRotaryEncoderHandleSemaphore = OSAL_C_INVALID_HANDLE;
        }
    }

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_ROTARY_ENCODER_s32IOControl
 *
 *DESCRIPTION   :Control the device
 *
 *PARAMETER     :s32Fun     Function to execute
 *               s32Arg     Argument to function, meaning depends on s32Fun
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_ROTARY_ENCODER_s32IOControl(tS32 s32Fun, tS32 s32Arg)
{
    tS32    retval = OSAL_E_NOERROR;

    if(DevRotaryEncoderHandleSemaphore == OSAL_C_INVALID_HANDLE)
    {
        retval = OSAL_E_UNKNOWN;
    }

    if(retval == OSAL_E_NOERROR)
    {
        switch(s32Fun)
        {
        case OSAL_C_S32_IOCTRL_ROT_ENCODER_REG_CALLBACK:
            if(OSAL_s32SemaphoreWait(DevRotaryEncoderHandleSemaphore, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
            {
                pOsalData->DevRotaryEncoderCallbackProcID = OSAL_ProcessWhoAmI();
                pOsalData->DevRotaryEncoderCallback = (OSAL_tpf_RotEncoder_Callback)s32Arg;
                OSAL_s32SemaphorePost(DevRotaryEncoderHandleSemaphore);
            }
            break;
        case OSAL_C_S32_IOCTRL_ROT_ENCODER_GET_VERSION:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                *((tU32 *)s32Arg) = 1;
            break;
        default:
            retval = OSAL_E_NOTSUPPORTED;
            break;
        }
    }

    return retval;
}
