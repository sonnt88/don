#ifndef DEV_ROTARY_ENCODER_H
#define DEV_ROTARY_ENCODER_H

/* ************************************************************************/
/*  defines (scope: global)                                               */
/* ************************************************************************/
#define DEV_ROTARY_ENCODER_TCP_MSG_START     0xf00d

/* *********************************************************************** */
/*  typedefs enum (scope: global)                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct (scope: global)                                        */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs function (scope: global)                                      */
/* *********************************************************************** */

/* *********************************************************************** */
/*  function prototypes (scope: global)                                    */
/* *********************************************************************** */
tS32    DEV_ROTARY_ENCODER_s32IODeviceInit(void);
void    DEV_ROTARY_ENCODER_s32IODeviceRemove(void);
tS32    DEV_ROTARY_ENCODER_s32IOOpen(OSAL_tenAccess enAccess);
tS32    DEV_ROTARY_ENCODER_s32IOClose(void);
tS32    DEV_ROTARY_ENCODER_s32IOControl(tS32 s32Fun, tS32 s32Arg);

#else //DEV_ROTARY_ENCODER_H
#error dev_rotary_encoder.h included several times
#endif //DEV_ROTARY_ENCODER_H
