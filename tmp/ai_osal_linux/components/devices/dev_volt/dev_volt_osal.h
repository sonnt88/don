/*******************************************************************************
*
* FILE:         dev_volt_osal.h
*
* SW-COMPONENT: Device Voltage
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Local header for OSAL functions.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_VOLT_OSAL_H_
#define _DEV_VOLT_OSAL_H_

/******************************************************************************/
/*                                                                            */
/* PUBLIC FUNCTION DECLARATIONS                                               */
/*                                                                            */
/******************************************************************************/

tU32 DEV_VOLT_OsalIO_u32Init(tVoid);
tU32 DEV_VOLT_OsalIO_u32Deinit(tVoid);
tU32 DEV_VOLT_OsalIO_u32Open(tVoid);
tU32 DEV_VOLT_OsalIO_u32Close(tVoid);
tU32 DEV_VOLT_OsalIO_u32Control(tS32 s32Fun, intptr_t pnArg);

#ifdef VARIANT_P_SHARED_VOLT
tS32 volt_drv_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t pnArg);
tS32 volt_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
tS32 volt_drv_io_close(tS32 s32ID, uintptr_t u32FD);
#endif

#endif //_DEV_VOLT_OSAL_H_
