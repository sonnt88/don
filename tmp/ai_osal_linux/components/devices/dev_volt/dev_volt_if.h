/*******************************************************************************
*
* FILE:         dev_volt_if.h
*
* SW-COMPONENT: Device Voltage
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Public interface header.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifdef DEV_VOLT_IMPORT_INTERFACE_GENERIC
  #ifndef DEV_VOLT_ALREADY_INCLUDE_GENERIC
    #define DEV_VOLT_ALREADY_INCLUDE_GENERIC
    #include "dev_volt_osal.h"
  #endif 
#endif 

#ifdef DEV_VOLT_IMPORT_INTERFACE_OSAL
  #ifndef DEV_VOLT_ALREADY_INCLUDE_OSAL
    #define DEV_VOLT_ALREADY_INCLUDE_OSAL
    #include "dev_volt_osal.h"
  #endif 
#endif 

#if !(defined DEV_VOLT_IMPORT_INTERFACE_GENERIC || \
      defined DEV_VOLT_IMPORT_INTERFACE_OSAL      )
 #error "Please define an interface when you include dev_volt_if.h"
#endif
