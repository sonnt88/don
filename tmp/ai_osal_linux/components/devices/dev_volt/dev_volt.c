/*******************************************************************************
*
* FILE:         dev_volt.c
*
* SW-COMPONENT: Device Voltage
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  The voltage device supervises the non capacitor buffered board
*               voltage via discrete interrupts and an ADC channel. Interested
*               clients can register at the voltage device to be notified
*               about level changes of the board voltage.
*
*               The core functionalities of the voltage device are :
*
*               - Supervise the board voltage and notify registered clients
*                 about a change between one of the following system voltage
*                 states (voltages in parenthesis are just examples).
*                 - CRITICAL HIGH VOLTAGE (greater than 18V)
*                 - HIGH VOLTAGE          (greater than 16V)
*                 - OPERATING VOLTAGE     (between 9V and 16V)
*                 - LOW VOLTAGE           (lower than 9V)
*                 - CRITICAL LOW VOLTAGE  (lower than 6V)
*               - Supervise the board voltage and notify registered clients
*                 about the crossing of a previously freely defined voltage
*                 level.
*               - Offer an interface to access actual state of the system
*                 voltage.
*               - Offer an interface to access the actual voltage of the
*                 boards power supply.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

#include <fcntl.h>
#include <unistd.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"

#define DEV_VOLT_IMPORT_INTERFACE_GENERIC
#include "dev_volt_if.h"

/******************************************************************************/
/*                                                                            */
/* DEFINES                                                                    */
/*                                                                            */
/******************************************************************************/

#define DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS                                ((tU8)6)

#define DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_USER_VOLTAGES                          ((tU8)4)

#define DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV               ((tU16)200)

#define DEV_VOLT_CONF_C_EN_GPIO_LOW_VOLTAGE                         OSAL_EN_PWR_UDROP_90
#define DEV_VOLT_CONF_C_EN_GPIO_CRITICAL_LOW_VOLTAGE                OSAL_EN_PWR_UDROP_60

#define DEV_VOLT_CONF_C_STRING_ADC_UBAT_SENSE        OSAL_C_STRING_DEVICE_ADC_UBAT_SENSE

#define DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_REFERENCE_MV                      ((tU16)3300)
#define DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_ADC_RESOLUTION                    ((tU16)1024) // 10 Bit = 1024 or 12 Bit = 4096

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U32_CREATOR_CONTEXT_MAGIC                          ((tU32)0xDEADBEEF)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_OFFSET_MV                   ((tU16)12)
#define DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_GRADIENT_MULT_BY_1000     ((tU16)6830)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_HIGH_MV                  ((tU16)16000)
#define DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_HIGH_HYS_MV                ((tU16)500)
#define DEV_VOLT_CONF_C_U32_DEFAULT_HIGH_VOLTAGE_TIMER_MS                   ((tU32)2000)
#define DEV_VOLT_CONF_C_U32_MINIMUM_HIGH_VOLTAGE_TIMER_MS                    ((tU32)500)

#define DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_CRIT_HIGH_MV             ((tU16)17200)
#define DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_CRIT_HIGH_HYS_MV           ((tU16)300)
#define DEV_VOLT_CONF_C_U32_DEFAULT_CRITICAL_HIGH_VOLTAGE_TIMER_MS          ((tU32)1000)
#define DEV_VOLT_CONF_C_U32_MINIMUM_CRITICAL_HIGH_VOLTAGE_TIMER_MS           ((tU32)500)

#define DEV_VOLT_CONF_C_U32_DEFAULT_DO_LOW_VOLTAGE_DETECTION                   ((tU32)1)
#define DEV_VOLT_CONF_C_U32_DEFAULT_LOW_VOLTAGE_OVER_DELAY_MS                ((tU32)200)

#define DEV_VOLT_CONF_C_U32_DEFAULT_DO_CRITICAL_LOW_VOLTAGE_DETECTION          ((tU32)1)
#define DEV_VOLT_CONF_C_U32_DEFAULT_CRITICAL_LOW_VOLTAGE_OVER_DELAY_MS       ((tU32)200)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U8_ERROR                                                     ((tU8)0)
#define DEV_VOLT_C_U8_OK                                                        ((tU8)1)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U8_TTFIS_COMMAND_UNKNOWN                                     ((tU8)0)
#define DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED                                      ((tU8)1)
#define DEV_VOLT_C_U8_TTFIS_COMMAND_EXECUTED                                    ((tU8)2)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U8_TRACE_TYPE_STRING                                      ((tU8)0x01)
#define DEV_VOLT_C_U8_TRACE_TYPE_INTERNAL_DATA                               ((tU8)0x02)
#define DEV_VOLT_C_U8_TRACE_TYPE_IO_CONTROL                                  ((tU8)0x03)
#define DEV_VOLT_C_U8_TRACE_TYPE_VOLTAGE_FLAGS                               ((tU8)0x04)
#define DEV_VOLT_C_U8_TRACE_TYPE_VOLTAGE_STATE                               ((tU8)0x05)
#define DEV_VOLT_C_U8_TRACE_TYPE_NOTIFICATION_EVENT                          ((tU8)0x06)
//#define DEV_VOLT_C_U8_TRACE_TYPE_INTERRUPT                                 ((tU8)0x07)
#define DEV_VOLT_C_U8_TRACE_TYPE_BOARD_VOLTAGE                               ((tU8)0x08)
#define DEV_VOLT_C_U8_TRACE_TYPE_REGISTERED_CLIENT_DATA                      ((tU8)0x09)
#define DEV_VOLT_C_U8_TRACE_TYPE_REGISTERED_CLIENT_DATA_USER_VOLTAGES        ((tU8)0x0A)
#define DEV_VOLT_C_U8_TRACE_TYPE_REGISTER_USER_VOLTAGE                       ((tU8)0x0B)

#define DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH                                ((tU8)254)
#define DEV_VOLT_C_U8_TRACE_RECEIVE_BUFFER_LENGTH                              ((tU8)32)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_STRING_SHARED_MEM_NAME                                  "DevVoltData"

#define DEV_VOLT_C_STRING_SEM_DATA_NAME                                    "DevVoltData"

#define DEV_VOLT_C_U32_SEM_DATA_TIMEOUT_MS                                  ((tU32)2000)

/* -------------------------------------------------------------------------- */

// Free client event bitmasks for /dev/volt starting at 0x00010000 (see osioctrl.h)

#define DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD                         ((tU32)0x00010000)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_STRING_SYSTEM_VOLT_THREAD_NAME                           "DevVoltSys"
#define DEV_VOLT_C_S32_SYSTEM_VOLT_THREAD_STK_SIZE                         ((tS32)10000)
#define DEV_VOLT_C_U32_SYSTEM_VOLT_THREAD_PRIO                        ((tU32)0x0000004A)

#define DEV_VOLT_C_STRING_SYSTEM_VOLT_THREAD_EVENT_NAME                     "DevVoltSys"

#define DEV_VOLT_C_U32_EVENT_MASK_TRACE_CALLBACK_COMMAND_RECEIVED     ((tU32)0x00000001)
#define DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED                 ((tU32)0x00000002)
#define DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED        ((tU32)0x00000004)
#define DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED                ((tU32)0x00000008)
#define DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED       ((tU32)0x00000010)
#define DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_TIMER_EXPIRED           ((tU32)0x00000020)
#define DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED  ((tU32)0x00000040)
#define DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED          ((tU32)0x00000080)
#define DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED ((tU32)0x00000100)
#define DEV_VOLT_C_U32_EVENT_MASK_INJECT_TTFIS_COMMAND                ((tU32)0x00000200)

#define DEV_VOLT_C_U32_SYSTEM_VOLT_THREAD_EVENT_MASK_ALL\
        (DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD |\
         DEV_VOLT_C_U32_EVENT_MASK_TRACE_CALLBACK_COMMAND_RECEIVED |\
         DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED |\
         DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED |\
         DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED |\
         DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED |\
         DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_TIMER_EXPIRED |\
         DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED |\
         DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED |\
         DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED |\
         DEV_VOLT_C_U32_EVENT_MASK_INJECT_TTFIS_COMMAND)

#define DEV_VOLT_C_STRING_SEM_SYSTEM_VOLT_THREAD_NAME                       "DevVoltSys"

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_STRING_HIGH_VOLT_THREAD_NAME                            "DevVoltHigh"
#define DEV_VOLT_C_S32_HIGH_VOLT_THREAD_STK_SIZE                           ((tS32)10000)
#define DEV_VOLT_C_U32_HIGH_VOLT_THREAD_PRIO                          ((tU32)0x0000004A)

#define DEV_VOLT_C_STRING_HIGH_VOLT_THREAD_EVENT_NAME                      "DevVoltHigh"

#define DEV_VOLT_C_U32_HIGH_VOLT_THREAD_EVENT_MASK_ALL \
        (DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD | \
         DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY)

#define DEV_VOLT_C_STRING_SEM_HIGH_VOLT_THREAD_NAME                        "DevVoltHigh"

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_STRING_USER_VOLT_THREAD_NAME                            "DevVoltUser"
#define DEV_VOLT_C_S32_USER_VOLT_THREAD_STK_SIZE                           ((tS32)10000)
#define DEV_VOLT_C_U32_USER_VOLT_THREAD_PRIO                          ((tU32)0x0000004A)

#define DEV_VOLT_C_STRING_USER_VOLT_THREAD_EVENT_NAME                      "DevVoltUser"

#define DEV_VOLT_C_U32_EVENT_MASK_GET_BOARD_VOLTAGE                   ((tU32)0x00000001)
#define DEV_VOLT_C_U32_EVENT_MASK_SET_USER_VOLTAGE_THRESHOLD          ((tU32)0x00000002)
#define DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_THRESHOLD_CHANGED      ((tU32)0x00000004)

#define DEV_VOLT_C_U32_USER_VOLT_THREAD_EVENT_MASK_ALL \
        (DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD | \
         DEV_VOLT_C_U32_EVENT_MASK_GET_BOARD_VOLTAGE | \
         DEV_VOLT_C_U32_EVENT_MASK_SET_USER_VOLTAGE_THRESHOLD |\
         DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_THRESHOLD_CHANGED)

#define DEV_VOLT_C_STRING_SEM_USER_VOLT_THREAD_NAME                       "DevVoltUser1"

#define DEV_VOLT_C_STRING_SEM_SET_USER_VOLTAGE_THRESHOLD_NAME             "DevVoltUser2"

#define DEV_VOLT_C_U32_SEM_SET_USER_VOLTAGE_THRESHOLD_TIMEOUT_MS             ((tU32)500)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_STRING_CLIENT_THREAD_NAME                             "DevVoltClient"
#define DEV_VOLT_C_S32_CLIENT_THREAD_STK_SIZE                              ((tS32)10000)
#define DEV_VOLT_C_U32_CLIENT_THREAD_PRIO                             ((tU32)0x0000004A)

#define DEV_VOLT_C_U32_EVENT_MASK_STOP_CLIENT_THREAD                  ((tU32)0x01000000)

#define DEV_VOLT_C_U32_CLIENT_THREAD_EVENT_MASK_ALL \
        (DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY | \
         DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY | \
         DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_HIGH_VOLTAGE | \
         DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_CRITICAL_HIGH_VOLTAGE | \
         DEV_VOLT_C_U32_EVENT_MASK_STOP_CLIENT_THREAD)

#define DEV_VOLT_C_STRING_SEM_CLIENT_THREAD_NAME                         "DevVoltClient"

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_STRING_SEM_GET_BOARD_VOLTAGE_NAME                      "DevVoltBoard"

#define DEV_VOLT_C_U32_SEM_BOARD_VOLTAGE_TIMEOUT_MS                         ((tU32)1000)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_STRING_SEM_INJECT_TTFIS_NAME                          "DevVoltInject"

#define DEV_VOLT_C_U32_SEM_INJECT_TTFIS_TIMEOUT_MS                          ((tU32)1000)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U8_THREAD_NOT_INSTALLED                                      ((tU8)0)
#define DEV_VOLT_C_U8_THREAD_RUNNING                                            ((tU8)1)
#define DEV_VOLT_C_U8_THREAD_SHUTTING_DOWN                                      ((tU8)2)
#define DEV_VOLT_C_U8_THREAD_OFF                                                ((tU8)3)

#define DEV_VOLT_C_U32_SEMAPHORE_THREAD_TIMEOUT_MS                          ((tU32)2000)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE                     ((tU32)0x00000001)
#define DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED                   ((tU32)0x00000002)
#define DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE            ((tU32)0x00000004)
#define DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED          ((tU32)0x00000008)

//#define DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATES                  ((tU32)0x00000005)
//#define DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_TRIGGER                 ((tU32)0x0000000A)
//#define DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_BITS                    ((tU32)0x0000000F)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE                    ((tU32)0x00000010)
#define DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED                  ((tU32)0x00000020)
#define DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE           ((tU32)0x00000040)
#define DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED         ((tU32)0x00000080)

//#define DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATES                 ((tU32)0x00000050)
//#define DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_TRIGGER                ((tU32)0x000000A0)
//#define DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_BITS                   ((tU32)0x000000F0)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U32_BIT_MASK_ALL_VOLTAGE_BITS                      ((tU32)0x000000FF)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_TIMER_EXPIRED             ((tU32)0x00000100)
#define DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED    ((tU32)0x00000400)
#define DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED            ((tU32)0x00000200)
#define DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED   ((tU32)0x00000800)

//#define DEV_VOLT_C_U32_BIT_MASK_TIMER_TRIGGER                       ((tU32)0x00000F00)
//#define DEV_VOLT_C_U32_BIT_MASK_TIMER_BITS                          ((tU32)0x00000F00)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U32_BIT_MASK_ALL_STATES                            ((tU32)0x00000055)
#define DEV_VOLT_C_U32_BIT_MASK_ALL_TRIGGER                           ((tU32)0x00000FAA)

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_C_U8_USER_VOLTAGE_NOT_CHANGED                                  ((tU8)0)
#define DEV_VOLT_C_U8_USER_VOLTAGE_HIGHER_LEVEL_EXCEEDED                        ((tU8)1)
#define DEV_VOLT_C_U8_USER_VOLTAGE_LOWER_LEVEL_DECEEDED                         ((tU8)2)

/******************************************************************************/
/*                                                                            */
/* TYPE DEFINITIONS                                                           */
/*                                                                            */
/******************************************************************************/

// Values and order of voltages is significant. 
// Only change them if you know what you do.
typedef enum
{
  DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW = 0,
  DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW_OVER_DELAY,
  DEV_VOLT_EN_VOLTAGE_STATE_LOW,
  DEV_VOLT_EN_VOLTAGE_STATE_LOW_OVER_DELAY,
  DEV_VOLT_EN_VOLTAGE_STATE_OPERATING,
  DEV_VOLT_EN_VOLTAGE_STATE_HIGH,
  DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH
} tenVoltageState;

typedef struct
{
  tU16 u16LevelMv;
  tU16 u16LevelAdc;
  tU16 u16HysteresisMv;
  tU16 u16HysteresisAdc;
  tU8  u8CrossDirection;
} trUserVoltage;

typedef struct
{
  tU32                         u32ClientId;
  tChar                        szNotificationEventName[DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH];
  tU32                         u32SystemVoltageIndicationMask;
  DEV_VOLT_trSystemVoltage     rSystemVoltage;
  trUserVoltage                arUserVoltage[DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_USER_VOLTAGES];
  tU8                          u8NumOfUserVoltageLevelsSet;
  tU16                         u16LatestCrossedUserVoltageLevelMv;
  tU8                          u8LatestUserVoltageState;
} trClientSpecificData;

typedef struct
{
  tU8                  u8NumberOfClients;
  tU32                 u32SystemVoltageState;
  tU32                 u32PreviousSystemVoltageState;
  tU8                  u8NumberOfRegisteredClients;
  tU32                 u32LatestAssignedClientId;
  tU16                 u16BoardVoltageAdc;
  tU16                 u16BoardVoltageMv;
  tU16                 u16NextHigherUserVoltageLevelAdc;
  tU16                 u16NextLowerUserVoltageLevelAdc;
  tU8                  u8PermHighVoltageCounter;
  tU8                  u8PermCriticalHighVoltageCounter;
  tU32                 u32SetUserVoltageThresholdsResult;
  tU32                 u32GetBoardVoltageResult;
  trClientSpecificData arClientSpecificData[DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS];
  tU16                 u16BoardVoltageOffsetMv;
  tU16                 u16BoardVoltageGradientMultipliedBy1000;
  tU8                  au8InjectTTFISBuffer[DEV_VOLT_C_U8_TRACE_RECEIVE_BUFFER_LENGTH];
} trGlobalData;

typedef struct
{
  tU32 u32LowVoltageFlags;
  tU32 u32HighVoltageFlags;
} trProtectedModuleData;

typedef tVoid (*tpfenStateMachineDoTransition)();

typedef struct
{
  tenVoltageState                             enCurrentVoltageState;
  tenVoltageState                             enRequestedVoltageState;
  tBool                                       bIsLowVoltageTimerExpired;
  tBool                                       bIsCriticalLowVoltageTimerExpired;
  tBool                                       bIsHighVoltageTimerExpired;
  tBool                                       bIsCriticalHighVoltageTimerExpired;
  const tpfenStateMachineDoTransition *       papfenStateMachineDoTransition;
} trStateMachineContext;

typedef struct
{
  tU32                                 u32CreatorContextMagic;
  OSAL_tEventHandle                    hEventSystemVoltageThread;
  OSAL_tEventHandle                    hEventHighVoltageThread;
  OSAL_tEventHandle                    hEventUserVoltageThread;
  OSAL_tEventHandle                    hEventClientThread;
  OSAL_tSemHandle                      hSemSystemVoltageThread;
  OSAL_tSemHandle                      hSemUserVoltageThread;
  OSAL_tSemHandle                      hSemHighVoltageThread;
  OSAL_tSemHandle                      hSemClientThread;
  OSAL_tSemHandle                      hSemDataAccess;
  OSAL_tSemHandle                      hSemGetBoardVoltage;
  OSAL_tSemHandle                      hSemSetUserVoltageThreshold;
  OSAL_tSemHandle                      hSemInjectTTFIS;
  OSAL_tTimerHandle                    hLowVoltageTimer;
  OSAL_tTimerHandle                    hCriticalLowVoltageTimer;
  OSAL_tTimerHandle                    hHighVoltageTimer;
  OSAL_tTimerHandle                    hCriticalHighVoltageTimer;
  OSAL_tShMemHandle                    hSharedMemory;
  OSAL_tIODescriptor                   rTraceIODescriptor;
  OSAL_tIODescriptor                   rGpioIODescriptor;
  OSAL_tIODescriptor                   rAdcUBatIODescriptor;
  OSAL_tIODescriptor                   rVoltIODescriptor;
  trGlobalData*                        prGlobalData;
  trProtectedModuleData                rProtectedModuleData;
  tU8                                  au8TraceCallbackBuffer[DEV_VOLT_C_U8_TRACE_RECEIVE_BUFFER_LENGTH];
  tBool                                bIsLowVoltageIrqRegistered;
  tBool                                bIsCriticalLowVoltageIrqRegistered;
  tBool                                bIsUserVoltageCallbackRegistered;
  tBool                                bBoardVoltageSimulationActive;
  tU8                                  u8SystemVoltageThreadState;
  tU8                                  u8HighVoltageThreadState;
  tU8                                  u8UserVoltageThreadState;
  tU8                                  u8ClientThreadState;
  trStateMachineContext                rStateMachineContext;
  OSAL_tEventHandle                    ahClientEventHandle[DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS];
  tU32                                 u32ClientId;
  tBool                                bReadBoardVoltageOffsetFromDeviceTree;
  tBool                                bReadBoardVoltageGradienFromDeviceTree;
  tU16                                 u16BoardVoltageHighMv;
  tU16                                 u16BoardVoltageHighHysMv;
  tU32                                 u32HighVoltageTimerMs;
  tU16                                 u16BoardVoltageCriticalHighMv;
  tU16                                 u16BoardVoltageCriticalHighHysMv;
  tU32                                 u32CriticalHighVoltageTimerMs;
  tU32                                 u32DoLowVoltageDetection;
  tU32                                 u32LowVoltageOverDelayMs;
  tU32                                 u32DoCriticalLowVoltageDetection;
  tU32                                 u32CriticalLowVoltageOverDelayMs;
  tU32                                 u32VoltageSimulationFlags;
} trModuleData;

typedef struct
{
  tString             szThreadName;
  tS32                s32ThreadStackSize;
  tU32                u32ThreadPriority;
  tString             szSemaphoreName;
  OSAL_tSemHandle*    phThreadSemaphore;
  tString             szEventName;
  OSAL_tEventHandle*  phThreadEvent;
  OSAL_tpfThreadEntry pfThreadFunction;
  tU8*                pu8ThreadState;
  tBool               bSelfCreatedEvent;
} trThreadSkeleton;

/******************************************************************************/
/*                                                                            */
/* LOCAL FUNCTION DECLARATIONS                                                */
/*                                                                            */
/******************************************************************************/

/* -------------------------------------------------------------------------- */
/* Calling context : Creator process                                          */
/* -------------------------------------------------------------------------- */

/* ----------------------------- Initialization ----------------------------- */

static tU32  DEV_VOLT_u32Init(tVoid);
static tU32  DEV_VOLT_u32Deinit(tVoid);

static tVoid DEV_VOLT_vInitModuleData(tVoid);
static tVoid DEV_VOLT_vInitGlobalData(tVoid);

static tVoid DEV_VOLT_vReadRegistryValues(tVoid);

static tVoid DEV_VOLT_vReadBoardVoltageOffsetFromDeviceTree(tVoid);
static tVoid DEV_VOLT_vReadBoardVoltageGradientFromDeviceTree(tVoid);

static tVoid DEV_VOLT_vRegisterTraceCallback(tVoid);
static tVoid DEV_VOLT_vUnregisterTraceCallback(tVoid);

static tU32  DEV_VOLT_u32RegisterLowVoltageCallback(tVoid);
static tU32  DEV_VOLT_u32UnregisterLowVoltageCallback(tVoid);

static tU32  DEV_VOLT_u32RegisterCriticalLowVoltageCallback(tVoid);
static tU32  DEV_VOLT_u32UnregisterCriticalLowVoltageCallback(tVoid);

static tU32  DEV_VOLT_u32CreateVoltageTimer(tVoid);
static tU32  DEV_VOLT_u32DeleteVoltageTimer(tVoid);

static tU32  DEV_VOLT_u32CreateClientNotificationEvents(tVoid);
static tVoid DEV_VOLT_vDeleteClientNotificationEvents(tVoid);

static tU32  DEV_VOLT_u32InstallThread(const trThreadSkeleton * const prThreadSkeleton);
static tU32  DEV_VOLT_u32UninstallThread(const trThreadSkeleton * const prThreadSkeleton);

static tU32  DEV_VOLT_u32InstallClientThread(tVoid);
static tU32  DEV_VOLT_u32UninstallClientThread(tVoid);

/* --------------------------------- Trace ---------------------------------- */

static tVoid DEV_VOLT_vTraceCallback(const tU8 * const pu8Data);
static tVoid DEV_VOLT_vEvaluateTraceCallbackCommand(tVoid);
static tU32  DEV_VOLT_u32ProcessInjectedTTFISCommand(tVoid);

static tVoid DEV_VOLT_vTraceInternalState(TR_tenTraceLevel enTraceLevel);
static tVoid DEV_VOLT_vTraceRegisteredClientData(TR_tenTraceLevel enTraceLevel);
static tVoid DEV_VOLT_vTraceVoltageFlags(tU32 u32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vTraceVoltageState(tenVoltageState enPreviousVoltageState);
static tVoid DEV_VOLT_vTraceBoardVoltage(tVoid);

/* ------------------------------ System Voltage ---------------------------- */

static tVoid DEV_VOLT_vSystemVoltageThread(tPVoid pvArg);

static tVoid DEV_VOLT_vOnSystemVoltageEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemSystemVoltageThread);
static tVoid DEV_VOLT_vHandleSystemVoltageEvents(OSAL_tEventMask* prEventMaskResult);
static tVoid DEV_VOLT_vOnLowVoltageChanged(tPU32 pu32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vOnCriticalLowVoltageChanged(tPU32 pu32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vOnHighVoltageChanged(tPU32 pu32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vOnCriticalHighVoltageChanged(tPU32 pu32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vOnLowVoltageTimerExpired(tPU32 pu32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vOnCriticalLowVoltageTimerExpired(tPU32 pu32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vOnHighVoltageTimerExpired(tPU32 pu32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vOnCriticalHighVoltageTimerExpired(tPU32 pu32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vEvaluateVoltageFlags(tU32 u32AccumulatedVoltageFlags);

static tVoid DEV_VOLT_vSetStateMachineAttributesByTriggerFlags(tU32 u32AccumulatedVoltageFlags);
static tVoid DEV_VOLT_vSetStateMachineAttributesByStateFlags(tU32 u32AccumulatedVoltageFlags);
static tBool DEV_VOLT_bCheckStayInCurrentVoltageStateByStateFlags(tU32 u32AccumulatedVoltageFlags);

static tVoid DEV_VOLT_vUpdateSystemVoltageHistoryAndNotify(tenVoltageState enPreviousVoltageState);

static tBool DEV_VOLT_bSimulateVoltageChange(tenVoltageState enSytemVoltage, tU8 u8VoltageState);
static tBool DEV_VOLT_bSimulateCombinedVoltageChanges(tU32 u32VoltageFlags);

static tVoid DEV_VOLT_vStateMachineDoTransition(tVoid);
static tVoid DEV_VOLT_vStateMachineDoTransitionFromCriticalLow(tVoid);
static tVoid DEV_VOLT_vStateMachineDoTransitionFromCriticalLowOverDelay(tVoid);
static tVoid DEV_VOLT_vStateMachineDoTransitionFromLow(tVoid);
static tVoid DEV_VOLT_vStateMachineDoTransitionFromLowOverDelay(tVoid);
static tVoid DEV_VOLT_vStateMachineDoTransitionFromOperating(tVoid);
static tVoid DEV_VOLT_vStateMachineDoTransitionFromHigh(tVoid);
static tVoid DEV_VOLT_vStateMachineDoTransitionFromCriticalHigh(tVoid);

static tVoid DEV_VOLT_vDoLowVoltageAction(tVoid);
static tVoid DEV_VOLT_vDoLowVoltageOverAction(tVoid);
static tVoid DEV_VOLT_vDoCriticalLowVoltageAction(tVoid);
static tVoid DEV_VOLT_vDoCriticalLowVoltageOverAction(tVoid);
static tVoid DEV_VOLT_vDoHighVoltageAction(tVoid);
static tVoid DEV_VOLT_vDoHighVoltageOverAction(tVoid);
static tVoid DEV_VOLT_vDoCriticalHighVoltageAction(tVoid);
static tVoid DEV_VOLT_vDoCriticalHighVoltageOverAction(tVoid);

/* --------------------------------- Low Voltage ---------------------------- */

static tVoid DEV_VOLT_vLowVoltageInterruptCallback(tPVoid pvArg);
static tVoid DEV_VOLT_vCriticalLowVoltageInterruptCallback(tPVoid pvArg);

static tVoid DEV_VOLT_vLowVoltageTimerCallback(tPVoid pvArg);
static tVoid DEV_VOLT_vCriticalLowVoltageTimerCallback(tPVoid pvArg);

/* --------------------------------- High Voltage --------------------------- */

static tVoid DEV_VOLT_vHighVoltageThread(tPVoid pvArg);

static tVoid DEV_VOLT_vOnHighVoltageEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemHighVoltageThread, tU32 u32ClientId, tenVoltageState* penLocalVoltageState);
static tVoid DEV_VOLT_vProcessHighVoltageStateChange(DEV_VOLT_trUserVoltage* prUserVoltage, tenVoltageState* penLocalVoltageState);

static tVoid DEV_VOLT_vHighVoltageTimerCallback(tPVoid pvArg);
static tVoid DEV_VOLT_vCriticalHighVoltageTimerCallback(tPVoid pvArg);

/* -------------------------------- User Voltage ---------------------------- */

static tVoid DEV_VOLT_vUserVoltageThread(tPVoid pvArg);

static tVoid DEV_VOLT_vOnUserVoltageEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemUserVoltageThread);

static tVoid DEV_VOLT_vUserVoltageChangedCallback(tVoid);

static tU8   DEV_VOLT_u8CheckUserVoltageChangedState(tVoid);
static tU32  DEV_VOLT_u32RecalcUserVoltageLimits(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tVoid DEV_VOLT_vSendUserVoltageChangedNotification(tU8 u8UserVoltageChangedState);

static tU16  DEV_VOLT_u16GetBoardVoltageMvPerDigit(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);

static tU32  DEV_VOLT_u32GetBoardVoltage(tVoid);
static tU32  DEV_VOLT_u32SetUserVoltageThresholds(tVoid);

static tBool DEV_VOLT_bSimulateBoardVoltage(tU16 u16BoardVoltageAdc, tU16 u16BoardVoltageMv);

/* --------------------------------- Test Client ---------------------------- */

static tVoid DEV_VOLT_vClientThread(tPVoid pvArg);

static tVoid DEV_VOLT_vOnClientEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemClientThread, tU32 u32ClientId);
static tBool DEV_VOLT_bAddPrivateUserVoltageNotification(tU16 u16UserVoltageLevelMv, tU16 u16HysteresisMv, tU8 u8LevelCrossDirection);
static tBool DEV_VOLT_bRemovePrivateUserVoltageNotification(tU16 u16UserVoltageLevelMv);

/* -------------------------------------------------------------------------- */
/* Calling context : Client processes                                         */
/* -------------------------------------------------------------------------- */

static tU32  DEV_VOLT_u32Open(tVoid);
static tU32  DEV_VOLT_u32Close(tVoid);
static tU32  DEV_VOLT_u32Control(tS32 s32Fun, intptr_t pnArg);
static tVoid DEV_VOLT_vTraceIOControl(tS32 s32Fun, intptr_t pnArg, tU32 u32OsalErrorCode);
static tVoid DEV_VOLT_vTraceNotificatonEvent(tU32 u32ClientId, tString szNotificationEventName, OSAL_tEventMask rEventMask);
static tU32  DEV_VOLT_u32AddClient(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32RemoveClient(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32RegisterClient(DEV_VOLT_trClientRegistration* prClientRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32UnregisterClient(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32RegisterSystemVoltage(DEV_VOLT_trSystemVoltageRegistration* prSystemVoltageRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32UnregisterSystemVoltage(tU32 u32Client, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32GetSystemVoltageHistory(DEV_VOLT_trSystemVoltageHistory* prSystemVoltageHistory, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32GetUserVoltageState(DEV_VOLT_trUserVoltage* prUserVoltage, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32InjectTTFISCommand(tU8* pu8TraceData, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);

/* -------------------------------------------------------------------------- */
/* Calling context : Creator process and/or Client processes                  */
/* -------------------------------------------------------------------------- */

static tU32  DEV_VOLT_u32MapGlobalData(OSAL_tShMemHandle* phDevVoltShMem, trGlobalData** pprGlobalData);
static tU32  DEV_VOLT_u32UnmapGlobalData(OSAL_tShMemHandle hDevVoltShMem, trGlobalData* prGlobalData);

static tU32  DEV_VOLT_u32DataLock(OSAL_tSemHandle hSemDataAccess);
static tVoid DEV_VOLT_vDataUnlock(OSAL_tSemHandle hSemDataAccess);

static tVoid DEV_VOLT_vTraceFormatted(TR_tenTraceLevel enTraceLevel, const tChar * const coszFormatString, ...);

static tU32  DEV_VOLT_u32RegisterUserVoltage(DEV_VOLT_trUserVoltageRegistration* prUserVoltageRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32UnregisterUserVoltage(DEV_VOLT_trUserVoltageDeregistration* prUserVoltageDeregistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32GetBoardVoltageFromOtherContext(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU32  DEV_VOLT_u32SetUserVoltageThresholdFromOtherContext(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);

static tVoid DEV_VOLT_vTraceRegisterUserVoltage(DEV_VOLT_trUserVoltageRegistration* prUserVoltageRegistration, tU16 u16UserVoltageAdc, tU16 u16HysteresisAdc, trGlobalData* prGlobalData);

static tU16  DEV_VOLT_u16ConvertBoardVoltageAdcToMv(tU16 u16BoardVoltageAdc, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);
static tU16  DEV_VOLT_u16ConvertBoardVoltageMvToAdc(tU16 u16BoardVoltageMv, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess);

/******************************************************************************/
/*                                                                            */
/* GLOBAL VARIABLES                                                           */
/*                                                                            */
/******************************************************************************/

static trModuleData g_rModuleData;

/******************************************************************************/
/*                                                                            */
/* GLOBAL CONSTANTS                                                           */
/*                                                                            */
/******************************************************************************/

static const trThreadSkeleton g_corSystemVoltageThreadSkeleton =
{
  (tString)DEV_VOLT_C_STRING_SYSTEM_VOLT_THREAD_NAME,
  DEV_VOLT_C_S32_SYSTEM_VOLT_THREAD_STK_SIZE,
  DEV_VOLT_C_U32_SYSTEM_VOLT_THREAD_PRIO,
  (tString)DEV_VOLT_C_STRING_SEM_SYSTEM_VOLT_THREAD_NAME,
  &g_rModuleData.hSemSystemVoltageThread,
  (tString)DEV_VOLT_C_STRING_SYSTEM_VOLT_THREAD_EVENT_NAME,
  &g_rModuleData.hEventSystemVoltageThread,
  DEV_VOLT_vSystemVoltageThread,
  &g_rModuleData.u8SystemVoltageThreadState,
  TRUE
};

static const trThreadSkeleton g_corUserVoltageThreadSkeleton =
{
  (tString)DEV_VOLT_C_STRING_USER_VOLT_THREAD_NAME,
  DEV_VOLT_C_S32_USER_VOLT_THREAD_STK_SIZE,
  DEV_VOLT_C_U32_USER_VOLT_THREAD_PRIO,
  (tString)DEV_VOLT_C_STRING_SEM_USER_VOLT_THREAD_NAME,
  &g_rModuleData.hSemUserVoltageThread,
  (tString)DEV_VOLT_C_STRING_USER_VOLT_THREAD_EVENT_NAME,
  &g_rModuleData.hEventUserVoltageThread,
  DEV_VOLT_vUserVoltageThread,
  &g_rModuleData.u8UserVoltageThreadState,
  TRUE
};

static const trThreadSkeleton g_corHighVoltageThreadSkeleton =
{
  (tString)DEV_VOLT_C_STRING_HIGH_VOLT_THREAD_NAME,
  DEV_VOLT_C_S32_HIGH_VOLT_THREAD_STK_SIZE,
  DEV_VOLT_C_U32_HIGH_VOLT_THREAD_PRIO,
  (tString)DEV_VOLT_C_STRING_SEM_HIGH_VOLT_THREAD_NAME,
  &g_rModuleData.hSemHighVoltageThread,
  (tString)DEV_VOLT_C_STRING_HIGH_VOLT_THREAD_EVENT_NAME,
  &g_rModuleData.hEventHighVoltageThread,
  DEV_VOLT_vHighVoltageThread,
  &g_rModuleData.u8HighVoltageThreadState,
  FALSE
};

static const tpfenStateMachineDoTransition g_capfenStateMachineDoTransition[] =
{
  DEV_VOLT_vStateMachineDoTransitionFromCriticalLow,
  DEV_VOLT_vStateMachineDoTransitionFromCriticalLowOverDelay,
  DEV_VOLT_vStateMachineDoTransitionFromLow,
  DEV_VOLT_vStateMachineDoTransitionFromLowOverDelay,
  DEV_VOLT_vStateMachineDoTransitionFromOperating,
  DEV_VOLT_vStateMachineDoTransitionFromHigh,
  DEV_VOLT_vStateMachineDoTransitionFromCriticalHigh
};

static const tChar g_acTextUnknown[8]  = "unknown";
static const tChar g_acTextExecuted[9] = "executed";
static const tChar g_acTextFailed[7]   = "failed";

/******************************************************************************/
/*                                                                            */
/* LOCAL FUNCTIONS                                                            */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Initialize the module local variables with default values.
*
*******************************************************************************/
static tVoid DEV_VOLT_vInitModuleData(tVoid)
{
	tU32 u32Index;

	g_rModuleData.u32CreatorContextMagic = DEV_VOLT_C_U32_CREATOR_CONTEXT_MAGIC;

	g_rModuleData.hEventSystemVoltageThread = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hEventHighVoltageThread   = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hEventUserVoltageThread   = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hEventClientThread        = OSAL_C_INVALID_HANDLE;

	g_rModuleData.hSemSystemVoltageThread     = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemHighVoltageThread       = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemUserVoltageThread       = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemClientThread            = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemDataAccess              = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemGetBoardVoltage         = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemSetUserVoltageThreshold = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hSemInjectTTFIS             = OSAL_C_INVALID_HANDLE;

	g_rModuleData.hLowVoltageTimer          = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hCriticalLowVoltageTimer  = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hHighVoltageTimer         = OSAL_C_INVALID_HANDLE;
	g_rModuleData.hCriticalHighVoltageTimer = OSAL_C_INVALID_HANDLE;

	g_rModuleData.hSharedMemory        = OSAL_ERROR;
	g_rModuleData.rTraceIODescriptor   = OSAL_ERROR;
	g_rModuleData.rGpioIODescriptor    = OSAL_ERROR;
	g_rModuleData.rAdcUBatIODescriptor = OSAL_ERROR;
	g_rModuleData.rVoltIODescriptor    = OSAL_ERROR;

	g_rModuleData.prGlobalData = NULL;

	g_rModuleData.rProtectedModuleData.u32LowVoltageFlags   = 0x00000000;
	g_rModuleData.rProtectedModuleData.u32HighVoltageFlags  = 0x00000000;

	OSAL_pvMemorySet(
		g_rModuleData.au8TraceCallbackBuffer,
		0,
		sizeof(g_rModuleData.au8TraceCallbackBuffer));

	g_rModuleData.bIsLowVoltageIrqRegistered         = FALSE;
	g_rModuleData.bIsCriticalLowVoltageIrqRegistered = FALSE;
	g_rModuleData.bIsUserVoltageCallbackRegistered   = FALSE;
	g_rModuleData.bBoardVoltageSimulationActive      = FALSE;

	g_rModuleData.u8SystemVoltageThreadState = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;
	g_rModuleData.u8HighVoltageThreadState   = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;
	g_rModuleData.u8UserVoltageThreadState   = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;
	g_rModuleData.u8ClientThreadState        = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;

	g_rModuleData.rStateMachineContext.enCurrentVoltageState              = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;
	g_rModuleData.rStateMachineContext.enRequestedVoltageState            = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;
	g_rModuleData.rStateMachineContext.bIsLowVoltageTimerExpired          = FALSE;
	g_rModuleData.rStateMachineContext.bIsCriticalLowVoltageTimerExpired  = FALSE;
	g_rModuleData.rStateMachineContext.bIsHighVoltageTimerExpired         = FALSE;
	g_rModuleData.rStateMachineContext.bIsCriticalHighVoltageTimerExpired = FALSE;
	g_rModuleData.rStateMachineContext.papfenStateMachineDoTransition     = g_capfenStateMachineDoTransition;

	for (u32Index = 0; 
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		g_rModuleData.ahClientEventHandle[u32Index] = OSAL_C_INVALID_HANDLE;
	}

	g_rModuleData.u32ClientId = DEV_VOLT_C_U32_CLIENT_ID_INVALID;

	g_rModuleData.bReadBoardVoltageOffsetFromDeviceTree  = FALSE;
	g_rModuleData.bReadBoardVoltageGradienFromDeviceTree = FALSE;

	g_rModuleData.u16BoardVoltageHighMv            = DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_HIGH_MV;
	g_rModuleData.u16BoardVoltageHighHysMv         = DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_HIGH_HYS_MV;
	g_rModuleData.u32HighVoltageTimerMs            = DEV_VOLT_CONF_C_U32_DEFAULT_HIGH_VOLTAGE_TIMER_MS;
	g_rModuleData.u16BoardVoltageCriticalHighMv    = DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_CRIT_HIGH_MV;
	g_rModuleData.u16BoardVoltageCriticalHighHysMv = DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_CRIT_HIGH_HYS_MV;
	g_rModuleData.u32CriticalHighVoltageTimerMs    = DEV_VOLT_CONF_C_U32_DEFAULT_CRITICAL_HIGH_VOLTAGE_TIMER_MS;
	g_rModuleData.u32DoLowVoltageDetection         = DEV_VOLT_CONF_C_U32_DEFAULT_DO_LOW_VOLTAGE_DETECTION;
	g_rModuleData.u32LowVoltageOverDelayMs         = DEV_VOLT_CONF_C_U32_DEFAULT_LOW_VOLTAGE_OVER_DELAY_MS;
	g_rModuleData.u32DoCriticalLowVoltageDetection = DEV_VOLT_CONF_C_U32_DEFAULT_DO_CRITICAL_LOW_VOLTAGE_DETECTION;
	g_rModuleData.u32CriticalLowVoltageOverDelayMs = DEV_VOLT_CONF_C_U32_DEFAULT_CRITICAL_LOW_VOLTAGE_OVER_DELAY_MS;

	g_rModuleData.u32VoltageSimulationFlags = 0x00000000;
}

/*******************************************************************************
*
* Initialize the global variables which are located in the shared-memory with 
* default values.
*
*******************************************************************************/
static tVoid DEV_VOLT_vInitGlobalData(tVoid)
{
	tU32 u32Index;
	tU32 u32Index2;

	tU16 u16HysteresisAdc = 0;

	g_rModuleData.prGlobalData->u8NumberOfClients           = 0;
	g_rModuleData.prGlobalData->u8NumberOfRegisteredClients = 0;
	g_rModuleData.prGlobalData->u32LatestAssignedClientId   = 0;

	g_rModuleData.prGlobalData->u32SystemVoltageState         = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;
	g_rModuleData.prGlobalData->u32PreviousSystemVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;

	g_rModuleData.prGlobalData->u16BoardVoltageAdc = 0;
	g_rModuleData.prGlobalData->u16BoardVoltageMv  = 0;

	g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc = 0xFFFF;
	g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc  = 0x0000;

	g_rModuleData.prGlobalData->u8PermHighVoltageCounter         = 0;
	g_rModuleData.prGlobalData->u8PermCriticalHighVoltageCounter = 0;

	g_rModuleData.prGlobalData->u32SetUserVoltageThresholdsResult = OSAL_E_NOERROR;
	g_rModuleData.prGlobalData->u32GetBoardVoltageResult          = OSAL_E_NOERROR;

	g_rModuleData.prGlobalData->u16BoardVoltageOffsetMv                 = DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_OFFSET_MV;
	g_rModuleData.prGlobalData->u16BoardVoltageGradientMultipliedBy1000 = DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_GRADIENT_MULT_BY_1000;

	u16HysteresisAdc = (tU16)(DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV / DEV_VOLT_u16GetBoardVoltageMvPerDigit(g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess));

	for (u32Index = 0; 
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS; 
	     u32Index++) {

		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId                = DEV_VOLT_C_U32_CLIENT_ID_INVALID;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName[0] = '\0';

		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32SystemVoltageIndicationMask = 0;

		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32CriticalLowVoltageCounter      = 0;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32LowVoltageCounter              = 0;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32HighVoltageCounter             = 0;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32CriticalHighVoltageCounter     = 0;

		for (u32Index2 = 0;
		     u32Index2 < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_USER_VOLTAGES;
		     u32Index2++) {

			g_rModuleData.prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelMv       = 0;
			g_rModuleData.prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelAdc      = 0;
			g_rModuleData.prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16HysteresisMv  = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV;
			g_rModuleData.prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16HysteresisAdc = u16HysteresisAdc;
			g_rModuleData.prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u8CrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_INVALID;
		}

		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u8NumOfUserVoltageLevelsSet        = 0;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u16LatestCrossedUserVoltageLevelMv = 0;
		g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u8LatestUserVoltageState           = DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_INVALID;
	}

	OSAL_pvMemorySet(
		g_rModuleData.prGlobalData->au8InjectTTFISBuffer,
		0,
		sizeof(g_rModuleData.prGlobalData->au8InjectTTFISBuffer));
}

/*******************************************************************************
*
* Read device related configuration values from the OSAL registry.
*
*******************************************************************************/
static tVoid DEV_VOLT_vReadRegistryValues(tVoid)
{
	OSAL_tIODescriptor    rRegistryIODescriptor = OSAL_ERROR;
	OSAL_trIOCtrlRegistry rIOCtrlRegistry       = { 0 };
	tU32                  u32RegistryValue      = 0x00000000;

	rRegistryIODescriptor = OSAL_IOOpen(
					OSAL_C_STRING_DEVICE_REGISTRY"/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/DEVICES/DEV_VOLT/",
					OSAL_EN_READWRITE);

	if (rRegistryIODescriptor == OSAL_ERROR) {
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => OSAL_IOOpen(DEVICE_REGISTRY) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		return;
	}

	rIOCtrlRegistry.pcos8Name = (tCS8*)"HIGH_VOLTAGE_LEVEL_MV";
	rIOCtrlRegistry.ps8Value  = (tU8*)&u32RegistryValue;
	rIOCtrlRegistry.u32Size   = sizeof(tS32);

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK) {
		g_rModuleData.u16BoardVoltageHighMv = (tU16)u32RegistryValue;

		if (DEV_VOLT_u16ConvertBoardVoltageMvToAdc(
			g_rModuleData.u16BoardVoltageHighMv,
			g_rModuleData.prGlobalData,
			g_rModuleData.hSemDataAccess) > DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_ADC_RESOLUTION) {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32ReadRegistryValues() => Value for HIGH_VOLTAGE_LEVEL_MV = %u mV from OSAL registry is too high (max = %u mV). Used default value = %u mV.",
				g_rModuleData.u16BoardVoltageHighMv,
				DEV_VOLT_u16ConvertBoardVoltageAdcToMv(
					DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_ADC_RESOLUTION,
					g_rModuleData.prGlobalData,
					g_rModuleData.hSemDataAccess),
				DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_HIGH_MV);

			g_rModuleData.u16BoardVoltageHighMv = DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_HIGH_MV;
		}
	} else {
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for HIGH_VOLTAGE_LEVEL_MV from OSAL registry. Used default value = %u mV.",
			g_rModuleData.u16BoardVoltageHighMv);
	}

	rIOCtrlRegistry.pcos8Name = (tCS8*)"HIGH_VOLTAGE_HYSTERESIS_MV";

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK)
		g_rModuleData.u16BoardVoltageHighHysMv = (tU16)u32RegistryValue;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for HIGH_VOLTAGE_HYSTERESIS_MV from OSAL registry. Used default value = %u ms.",
			g_rModuleData.u16BoardVoltageHighHysMv);

	rIOCtrlRegistry.pcos8Name = (tCS8*)"HIGH_VOLTAGE_TIMER_MS";

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK) {
		if ((u32RegistryValue == 0) || (u32RegistryValue >= DEV_VOLT_CONF_C_U32_MINIMUM_HIGH_VOLTAGE_TIMER_MS))
			g_rModuleData.u32HighVoltageTimerMs = (tU16)u32RegistryValue;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_ERRORS,
				"DEV_VOLT_u32ReadRegistryValues() => Configured value for HIGH_VOLTAGE_TIMER_MS = %u, but must be 0 or >= %u. Used default value of 0 ms.",
				u32RegistryValue,
				DEV_VOLT_CONF_C_U32_MINIMUM_HIGH_VOLTAGE_TIMER_MS);
	} else {
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for HIGH_VOLTAGE_TIMER_MS from OSAL registry. Used default value = %u ms.",
			g_rModuleData.u32HighVoltageTimerMs);
	}

	rIOCtrlRegistry.pcos8Name = (tCS8*)"CRITICAL_HIGH_VOLTAGE_LEVEL_MV";

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK) {
		g_rModuleData.u16BoardVoltageCriticalHighMv = (tU16)u32RegistryValue;

		if (DEV_VOLT_u16ConvertBoardVoltageMvToAdc(
			g_rModuleData.u16BoardVoltageCriticalHighMv,
			g_rModuleData.prGlobalData,
			g_rModuleData.hSemDataAccess) > DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_ADC_RESOLUTION) {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32ReadRegistryValues() => Value for CRITICAL_HIGH_VOLTAGE_LEVEL_MV = %u mV from OSAL registry is too high (max = %u mV). Used default value = %u mV.",
				g_rModuleData.u16BoardVoltageCriticalHighMv,
				DEV_VOLT_u16ConvertBoardVoltageAdcToMv(
					DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_ADC_RESOLUTION,
					g_rModuleData.prGlobalData,
					g_rModuleData.hSemDataAccess),
				DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_CRIT_HIGH_MV);

			g_rModuleData.u16BoardVoltageCriticalHighMv = DEV_VOLT_CONF_C_U16_DEFAULT_BOARD_VOLTAGE_CRIT_HIGH_MV;
		}
	} else {
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for CRITICAL_HIGH_VOLTAGE_LEVEL_MV from OSAL registry. Used default value = %u mV.",
			g_rModuleData.u16BoardVoltageCriticalHighMv);
	}

	rIOCtrlRegistry.pcos8Name = (tCS8*)"CRITICAL_HIGH_VOLTAGE_HYSTERESIS_MV";

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK)
		g_rModuleData.u16BoardVoltageCriticalHighHysMv = (tU16)u32RegistryValue;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for CRITICAL_HIGH_VOLTAGE_HYSTERESIS_MV from OSAL registry. Used default value = %u mV.",
			g_rModuleData.u16BoardVoltageCriticalHighHysMv);

	rIOCtrlRegistry.pcos8Name = (tCS8*)"CRITICAL_HIGH_VOLTAGE_TIMER_MS";

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK) {
		if ((u32RegistryValue == 0) || (u32RegistryValue >= DEV_VOLT_CONF_C_U32_MINIMUM_CRITICAL_HIGH_VOLTAGE_TIMER_MS))
			g_rModuleData.u32CriticalHighVoltageTimerMs = u32RegistryValue;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_ERRORS,
				"DEV_VOLT_u32ReadRegistryValues() => Configured value for CRITICAL_HIGH_VOLTAGE_TIMER_MS = %u, but must be 0 or >= %u. Used default value of 0 ms.",
				u32RegistryValue,
				DEV_VOLT_CONF_C_U32_MINIMUM_CRITICAL_HIGH_VOLTAGE_TIMER_MS);
	} else {
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for CRITICAL_HIGH_VOLTAGE_TIMER_MS from OSAL registry. Used default value = %u ms.",
			g_rModuleData.u32CriticalHighVoltageTimerMs);
	}

	rIOCtrlRegistry.pcos8Name = (tCS8*)"DO_LOW_VOLTAGE_DETECTION";

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK)
		g_rModuleData.u32DoLowVoltageDetection = u32RegistryValue;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for DO_LOW_VOLTAGE_DETECTION from OSAL registry. Used default value = %u.",
			g_rModuleData.u32DoLowVoltageDetection);

	rIOCtrlRegistry.pcos8Name = (tCS8*)"LOW_VOLTAGE_OVER_DELAY_MS";

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK)
		g_rModuleData.u32LowVoltageOverDelayMs = u32RegistryValue;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for LOW_VOLTAGE_OVER_DELAY_MS from OSAL registry. Used default value = %u ms.",
			g_rModuleData.u32LowVoltageOverDelayMs);

	rIOCtrlRegistry.pcos8Name = (tCS8*)"DO_CRITICAL_LOW_VOLTAGE_DETECTION";

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK)
		g_rModuleData.u32DoCriticalLowVoltageDetection = u32RegistryValue;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for DO_CRITICAL_LOW_VOLTAGE_DETECTION from OSAL registry. Used default value = %u.",
			g_rModuleData.u32DoCriticalLowVoltageDetection);

	rIOCtrlRegistry.pcos8Name = (tCS8*)"CRITICAL_LOW_VOLTAGE_OVER_DELAY_MS";

	if (OSAL_s32IOControl(
		rRegistryIODescriptor,
		OSAL_C_S32_IOCTRL_REGGETVALUE,
		(intptr_t)&rIOCtrlRegistry) == OSAL_OK)
		g_rModuleData.u32CriticalLowVoltageOverDelayMs = u32RegistryValue;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_u32ReadRegistryValues() => Failed to read value for CRITICAL_LOW_VOLTAGE_OVER_DELAY_MS from OSAL registry. Used default value = %u mV.",
			g_rModuleData.u32CriticalLowVoltageOverDelayMs);

	if (OSAL_s32IOClose(rRegistryIODescriptor) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32ReadRegistryValues() => OSAL_IOClose(DEVICE_REGISTRY) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
}

/*******************************************************************************
*
* Read ADC board-voltage-offset from device-tree.
*
*******************************************************************************/
static tVoid DEV_VOLT_vReadBoardVoltageOffsetFromDeviceTree(tVoid)
{
	tInt  nFd      = -1;
	tU32  u32Value = 0;
	tBool bResult = FALSE;

	nFd = open("/proc/device-tree/adc_calculation_parameter/ubat_sense/offset", O_RDONLY);

	if (nFd != -1) {
		if (read(nFd, &u32Value, sizeof(tU32)) != -1) {
			g_rModuleData.prGlobalData->u16BoardVoltageOffsetMv = (tU16)(be32toh(u32Value));
			bResult = TRUE;
		}
		close(nFd);
	}

	if (bResult == TRUE)
		g_rModuleData.bReadBoardVoltageOffsetFromDeviceTree = TRUE;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_vReadBoardVoltageOffsetFromDeviceTree() => Failed to read /proc/device-tree/adc_calculation_parameter/ubat_sense/offset");
}

/*******************************************************************************
*
* Read ADC board-voltage-gradient from device-tree.
*
*******************************************************************************/
static tVoid DEV_VOLT_vReadBoardVoltageGradientFromDeviceTree(tVoid)
{
	tInt  nFd      = -1;
	tU32  u32Value = 0;
	tBool bResult = FALSE;

	nFd = open("/proc/device-tree/adc_calculation_parameter/ubat_sense/gradient", O_RDONLY);

	if (nFd != -1) {
		if (read(nFd, &u32Value, sizeof(tU32)) != -1) {
			g_rModuleData.prGlobalData->u16BoardVoltageGradientMultipliedBy1000 = (tU16)(be32toh(u32Value));
			bResult = TRUE;
		}
		close(nFd);
	}

	if (bResult == TRUE)
		g_rModuleData.bReadBoardVoltageGradienFromDeviceTree = TRUE;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_vReadBoardVoltageGradientFromDeviceTree() => Failed to read /proc/device-tree/adc_calculation_parameter/ubat_sense/gradient");
}

/*******************************************************************************
*
* Initialization function of the OSAL device which creates the necessary OSAL
* resources like semaphores, events, message-queues and threads brings the
* device into an operational (initialized) state.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32Init(tVoid)
{
	tU32                  u32OsalErrorCode    = OSAL_E_NOERROR;
	tU32                  u32TmpOsalErrorCode = OSAL_E_NOERROR;
	OSAL_trAdcSetCallback rAdcSetCallback     = { enAdcCallbackThreshold, NULL };

	OSAL_trProcessControlBlock rProcessControlBlock;

	DEV_VOLT_vInitModuleData();

	if (OSAL_s32ProcessControlBlock(
		OSAL_ProcessWhoAmI(), 
		&rProcessControlBlock) == OSAL_ERROR)
		return OSAL_u32ErrorCode();

	DEV_VOLT_vTraceFormatted(TR_LEVEL_USER_1, 
		"DEV_VOLT_u32Init() => Called from process '%.15s' (%u)",
		rProcessControlBlock.szName,
		rProcessControlBlock.id);

	if (OSAL_s32SemaphoreCreate(
		DEV_VOLT_C_STRING_SEM_DATA_NAME,
		&g_rModuleData.hSemDataAccess,
		1U) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreCreate(
		DEV_VOLT_C_STRING_SEM_INJECT_TTFIS_NAME,
		&g_rModuleData.hSemInjectTTFIS,
		1U) == OSAL_ERROR)
		goto error_semaphore_create; /*lint !e801, authorized LINT-deactivation #<71> */

	g_rModuleData.hSharedMemory = OSAL_SharedMemoryCreate(
		DEV_VOLT_C_STRING_SHARED_MEM_NAME,
		OSAL_EN_READWRITE, 
		sizeof(trGlobalData));

	if (g_rModuleData.hSharedMemory == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_shared_memory_create; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.prGlobalData = (trGlobalData*) OSAL_pvSharedMemoryMap(
		g_rModuleData.hSharedMemory,
		OSAL_EN_READWRITE, 
		sizeof(trGlobalData), 
		0);

	if (g_rModuleData.prGlobalData == NULL) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_shared_memory_map; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_VOLT_vInitGlobalData();

	DEV_VOLT_vReadRegistryValues();
	DEV_VOLT_vReadBoardVoltageOffsetFromDeviceTree();
	DEV_VOLT_vReadBoardVoltageGradientFromDeviceTree();

	u32OsalErrorCode = DEV_VOLT_u32CreateClientNotificationEvents();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_create_client_notification_events; /*lint !e801, authorized LINT-deactivation #<71> */

	u32OsalErrorCode = DEV_VOLT_u32CreateVoltageTimer();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_create_voltage_timer; /*lint !e801, authorized LINT-deactivation #<71> */

	u32OsalErrorCode = DEV_VOLT_u32InstallThread(&g_corSystemVoltageThreadSkeleton);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_install_system_voltage_thread; /*lint !e801, authorized LINT-deactivation #<71> */

	if (OSAL_s32SemaphoreCreate(
		DEV_VOLT_C_STRING_SEM_GET_BOARD_VOLTAGE_NAME,
		&g_rModuleData.hSemGetBoardVoltage,
		0) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_sem_create_board_voltage; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreCreate(
		DEV_VOLT_C_STRING_SEM_SET_USER_VOLTAGE_THRESHOLD_NAME,
		&g_rModuleData.hSemSetUserVoltageThreshold,
		0) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_sem_create_user_voltage; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.rAdcUBatIODescriptor = OSAL_IOOpen(DEV_VOLT_CONF_C_STRING_ADC_UBAT_SENSE, OSAL_EN_READONLY);

	if (g_rModuleData.rAdcUBatIODescriptor != OSAL_ERROR) {
		rAdcSetCallback.enCallbackType      = enAdcCallbackThreshold;
		rAdcSetCallback.pfnCallbackFunction = DEV_VOLT_vUserVoltageChangedCallback;

		if (OSAL_s32IOControl(
			g_rModuleData.rAdcUBatIODescriptor,
			OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,
			(intptr_t)&rAdcSetCallback) == OSAL_OK) {

			g_rModuleData.bIsUserVoltageCallbackRegistered = TRUE;

			u32OsalErrorCode = DEV_VOLT_u32InstallThread(&g_corUserVoltageThreadSkeleton);

			if (u32OsalErrorCode != OSAL_E_NOERROR)
				goto error_install_user_voltage_thread; /*lint !e801, authorized LINT-deactivation #<71> */

			u32OsalErrorCode = DEV_VOLT_u32InstallThread(&g_corHighVoltageThreadSkeleton);

			if (u32OsalErrorCode != OSAL_E_NOERROR)
				goto error_install_high_voltage_thread; /*lint !e801, authorized LINT-deactivation #<71> */
		} else {
			g_rModuleData.prGlobalData->u32GetBoardVoltageResult = OSAL_E_ACCESS_FAILED;
			DEV_VOLT_vTraceFormatted(TR_LEVEL_ERRORS, "DEV_VOLT_u32Init() => Unable to register threshold callback for ADC channel = UBAT_SENSE. User voltage detection and board voltage access is NOT available");
		}
	} else {
		g_rModuleData.prGlobalData->u32GetBoardVoltageResult = OSAL_E_DOESNOTEXIST;
		DEV_VOLT_vTraceFormatted(TR_LEVEL_ERRORS, "DEV_VOLT_u32Init() => OSAL_IOOpen(DEVICE_ADC_UBAT_SENSE) failed with error code = %d = %s", OSAL_u32ErrorCode(), OSAL_coszErrorText(OSAL_u32ErrorCode()));
		DEV_VOLT_vTraceFormatted(TR_LEVEL_ERRORS, "DEV_VOLT_u32Init() => ... user and high voltage detection as well as board voltage access is NOT available");
	}

	if ((g_rModuleData.u32DoLowVoltageDetection         == 1) ||
	    (g_rModuleData.u32DoCriticalLowVoltageDetection == 1)   ) {

		g_rModuleData.rGpioIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, OSAL_EN_READWRITE);

		if (g_rModuleData.rGpioIODescriptor != OSAL_ERROR) {
	
			if (g_rModuleData.u32DoLowVoltageDetection == 1) {
				u32OsalErrorCode = DEV_VOLT_u32RegisterLowVoltageCallback();

				if (u32OsalErrorCode != OSAL_E_NOERROR)
					DEV_VOLT_vTraceFormatted(TR_LEVEL_ERRORS, "DEV_VOLT_u32Init() => GPIO register for UDROP_90 couldn't be configured. UDROP_90 detection is NOT available");
			}

			if (g_rModuleData.u32DoCriticalLowVoltageDetection == 1) {
				u32OsalErrorCode = DEV_VOLT_u32RegisterCriticalLowVoltageCallback();

				if (u32OsalErrorCode != OSAL_E_NOERROR)
					DEV_VOLT_vTraceFormatted(TR_LEVEL_ERRORS, "DEV_VOLT_u32Init() => GPIO register for UDROP_60 couldn't be configured. UDROP_60 detection is NOT available");
			}
		} else {
			DEV_VOLT_vTraceFormatted(TR_LEVEL_ERRORS, "DEV_VOLT_u32Init() => OSAL_IOOpen(DEVICE_GPIO) failed with error code = %s", OSAL_coszErrorText(OSAL_u32ErrorCode()));
			DEV_VOLT_vTraceFormatted(TR_LEVEL_ERRORS, "DEV_VOLT_u32Init() => ... UDROP_60 and UDROP_90 detection is NOT available");
		}
	}

	DEV_VOLT_vRegisterTraceCallback();

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_1,
		"DEV_VOLT_u32Init() left with success");

	return OSAL_E_NOERROR;

error_install_high_voltage_thread:

	if (g_rModuleData.bIsUserVoltageCallbackRegistered == TRUE) {
		u32TmpOsalErrorCode = DEV_VOLT_u32UninstallThread(&g_corUserVoltageThreadSkeleton);

		if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32Init() => DEV_VOLT_u32UninstallThread(UserVoltage) failed with error code = %s",
				OSAL_coszErrorText(u32TmpOsalErrorCode));
	}

error_install_user_voltage_thread:

	if (g_rModuleData.bIsUserVoltageCallbackRegistered == TRUE) {
		rAdcSetCallback.enCallbackType      = enAdcCallbackThreshold;
		rAdcSetCallback.pfnCallbackFunction = NULL;

		if (OSAL_s32IOControl(
			g_rModuleData.rAdcUBatIODescriptor,
			OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,
			(intptr_t)&rAdcSetCallback) == OSAL_OK)
			g_rModuleData.bIsUserVoltageCallbackRegistered = FALSE;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32Init() => OSAL_s32IOControl(ADC_CLR_CALLBACK) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	if (g_rModuleData.rAdcUBatIODescriptor != OSAL_ERROR) {
		if (OSAL_s32IOClose(g_rModuleData.rAdcUBatIODescriptor) == OSAL_OK)
			g_rModuleData.rAdcUBatIODescriptor = OSAL_ERROR;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32Init() => OSAL_s32IOClose(DEVICE_ADC) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemSetUserVoltageThreshold) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_SET_USER_VOLTAGE_THRESHOLD_NAME) == OSAL_OK)
			g_rModuleData.hSemSetUserVoltageThreshold = OSAL_C_INVALID_HANDLE;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32Init() => OSAL_s32SemaphoreDelete(SET_USER_VOLTAGE_THRESHOLD) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Init() => OSAL_s32SemaphoreClose(GetBoardVoltage) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_user_voltage:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemGetBoardVoltage) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_GET_BOARD_VOLTAGE_NAME) == OSAL_OK)
			g_rModuleData.hSemGetBoardVoltage = OSAL_C_INVALID_HANDLE;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32Init() => OSAL_s32SemaphoreDelete(GET_BOARD_VOLTAGE) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Init() => OSAL_s32SemaphoreClose(GetBoardVoltage) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_sem_create_board_voltage:

	u32TmpOsalErrorCode = DEV_VOLT_u32UninstallThread(&g_corSystemVoltageThreadSkeleton);

	if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Init() => DEV_VOLT_u32UninstallThread(SystemVoltage) failed with error code = %s",
			OSAL_coszErrorText(u32TmpOsalErrorCode));

error_install_system_voltage_thread:

	u32TmpOsalErrorCode = DEV_VOLT_u32DeleteVoltageTimer();
	
	if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Init() => DEV_VOLT_u32DeleteVoltageTimer() failed with error code = %s",
			OSAL_coszErrorText(u32TmpOsalErrorCode));
	
error_create_voltage_timer:

	DEV_VOLT_vDeleteClientNotificationEvents();

error_create_client_notification_events:

	if (OSAL_s32SharedMemoryUnmap(g_rModuleData.prGlobalData, sizeof(trGlobalData)) == OSAL_OK)
		g_rModuleData.prGlobalData = NULL;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Init() => OSAL_s32SharedMemoryUnmap() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_shared_memory_map:

	if (OSAL_s32SharedMemoryClose(g_rModuleData.hSharedMemory) == OSAL_OK)
		if (OSAL_s32SharedMemoryDelete(DEV_VOLT_C_STRING_SHARED_MEM_NAME) == OSAL_OK)
			g_rModuleData.hSharedMemory = OSAL_ERROR;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32Init() => OSAL_s32SharedMemoryDelete() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Init() => OSAL_s32SharedMemoryClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_shared_memory_create:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemInjectTTFIS) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_INJECT_TTFIS_NAME) == OSAL_OK)
			g_rModuleData.hSemInjectTTFIS = OSAL_C_INVALID_HANDLE;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32Init() => OSAL_s32SemaphoreDelete(INJECT_TTFIS) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Init() => OSAL_s32SemaphoreClose(InjectTTFIS) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_semaphore_create:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemDataAccess) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_DATA_NAME) == OSAL_OK)
			g_rModuleData.hSemDataAccess = OSAL_C_INVALID_HANDLE;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32Init() => OSAL_s32SemaphoreDelete(DATA) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Init() => OSAL_s32SemaphoreClose(DataAccess) failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
	return u32OsalErrorCode;
}

/*******************************************************************************
*
* De-Initialization function of the OSAL device which deletes all OSAL
* resources like semaphores, events, message-queues and threads and again brings
* the device into a non operational (de-initialzed) state.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32Deinit(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_FATAL,
		"DEV_VOLT_u32Deinit() entered. Creator context magic = 0x%08X",
		g_rModuleData.u32CreatorContextMagic);

	if (g_rModuleData.hSemDataAccess == OSAL_C_INVALID_HANDLE)
		return OSAL_E_NOTINITIALIZED;

	DEV_VOLT_vUnregisterTraceCallback();

	u32OsalErrorCode = DEV_VOLT_u32UnregisterLowVoltageCallback();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = DEV_VOLT_u32UnregisterCriticalLowVoltageCallback();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if (TRUE == g_rModuleData.bIsUserVoltageCallbackRegistered) {
		OSAL_trAdcSetCallback rAdcSetCallback;

		rAdcSetCallback.enCallbackType      = enAdcCallbackThreshold;
		rAdcSetCallback.pfnCallbackFunction = NULL;

		if (OSAL_s32IOControl(
			g_rModuleData.rAdcUBatIODescriptor,
			OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK,
			(intptr_t)&rAdcSetCallback) == OSAL_OK)
			g_rModuleData.bIsUserVoltageCallbackRegistered = FALSE;
		else
			return OSAL_u32ErrorCode();
	}

	u32OsalErrorCode = DEV_VOLT_u32UninstallClientThread();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = DEV_VOLT_u32UninstallThread(&g_corHighVoltageThreadSkeleton);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = DEV_VOLT_u32UninstallThread(&g_corUserVoltageThreadSkeleton);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = DEV_VOLT_u32UninstallThread(&g_corSystemVoltageThreadSkeleton);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if (g_rModuleData.rAdcUBatIODescriptor != OSAL_ERROR) {
		if (OSAL_s32IOClose(g_rModuleData.rAdcUBatIODescriptor) == OSAL_ERROR)
			return OSAL_u32ErrorCode();
		else
			g_rModuleData.rAdcUBatIODescriptor = OSAL_ERROR;
	}

	if (g_rModuleData.rGpioIODescriptor != OSAL_ERROR) {
		if (OSAL_s32IOClose(g_rModuleData.rGpioIODescriptor) == OSAL_ERROR)
			return OSAL_u32ErrorCode();
		else
			g_rModuleData.rGpioIODescriptor = OSAL_ERROR;
	}

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemSetUserVoltageThreshold) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_SET_USER_VOLTAGE_THRESHOLD_NAME) == OSAL_OK)
			g_rModuleData.hSemSetUserVoltageThreshold = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemGetBoardVoltage) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_GET_BOARD_VOLTAGE_NAME) == OSAL_OK)
			g_rModuleData.hSemGetBoardVoltage = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	u32OsalErrorCode = DEV_VOLT_u32DeleteVoltageTimer();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	DEV_VOLT_vDeleteClientNotificationEvents();

	if (OSAL_s32SharedMemoryUnmap(g_rModuleData.prGlobalData, sizeof(trGlobalData)) == OSAL_OK)
		g_rModuleData.prGlobalData = NULL;
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SharedMemoryClose(g_rModuleData.hSharedMemory) == OSAL_OK)
		if (OSAL_s32SharedMemoryDelete(DEV_VOLT_C_STRING_SHARED_MEM_NAME) == OSAL_OK)
			g_rModuleData.hSharedMemory = OSAL_ERROR;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemInjectTTFIS) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_INJECT_TTFIS_NAME) == OSAL_OK)
			g_rModuleData.hSemInjectTTFIS = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemDataAccess) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_DATA_NAME) == OSAL_OK)
			g_rModuleData.hSemDataAccess = OSAL_C_INVALID_HANDLE;
		else
			return OSAL_u32ErrorCode();
	else
		return OSAL_u32ErrorCode();

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_FATAL,
		"DEV_VOLT_u32Deinit() left with success");

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* Open function of the OSAL device.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32Open(tVoid)
{
	tU32                       u32OsalErrorCode     = OSAL_E_NOERROR;
	tU32                       u32TmpOsalErrorCode  = OSAL_E_NOERROR;
	trGlobalData*              prGlobalData         = NULL;
	OSAL_tShMemHandle          hDevVoltShMem        = OSAL_ERROR;
	OSAL_tSemHandle            hSemDataAccess       = OSAL_C_INVALID_HANDLE;
	OSAL_trThreadControlBlock  rThreadControlBlock;
	OSAL_trProcessControlBlock rProcessControlBlock;

	if (OSAL_s32ProcessControlBlock(
		OSAL_ProcessWhoAmI(),
		&rProcessControlBlock) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32ThreadControlBlock(
		OSAL_ThreadWhoAmI(),
		&rThreadControlBlock) == OSAL_ERROR) {
		// I can be a valid use case that a thread control block is not yet available if DEV_VOLT_u32Open() is called from a process main() function.
		// With the second call of the same function the pseudo thread control block should be created by OSAL. Try for a second time ...
		if (OSAL_s32ThreadControlBlock(
			OSAL_ThreadWhoAmI(),
			&rThreadControlBlock) == OSAL_ERROR)
				return OSAL_u32ErrorCode();
	}

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_1,
		"DEV_VOLT_u32Open() => Called from process '%.15s' (%u) and thread '%.15s' (%u)",
		rProcessControlBlock.szName,
		rProcessControlBlock.id,
		rThreadControlBlock.szName,
		rThreadControlBlock.id);

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_DATA_NAME,
		&hSemDataAccess) == OSAL_ERROR) {
		
		u32OsalErrorCode = OSAL_u32ErrorCode();
		
		// If this central semaphore doesn't exist, then the device wasn't initialized successfully.
		if (u32OsalErrorCode == OSAL_E_DOESNOTEXIST)
			u32OsalErrorCode = OSAL_E_NOTINITIALIZED;

		return u32OsalErrorCode;
	}

	u32OsalErrorCode = DEV_VOLT_u32MapGlobalData(
		&hDevVoltShMem, 
		&prGlobalData);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_map_global_data; /*lint !e801, authorized LINT-deactivation #<71> */

	u32OsalErrorCode = DEV_VOLT_u32AddClient(prGlobalData, hSemDataAccess);

	u32TmpOsalErrorCode = DEV_VOLT_u32UnmapGlobalData(hDevVoltShMem,prGlobalData);
	
	if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Open() => DEV_VOLT_u32UnmapGlobalData() failed with error code = %s",
			OSAL_coszErrorText(u32TmpOsalErrorCode));

error_map_global_data:

	if (OSAL_s32SemaphoreClose(hSemDataAccess) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Open() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return (u32OsalErrorCode);
}

/*******************************************************************************
*
* Close function of the OSAL device.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32Close(tVoid)
{
	tU32              u32OsalErrorCode    = OSAL_E_NOERROR;
	tU32              u32TmpOsalErrorCode = OSAL_E_NOERROR;
	trGlobalData*     prGlobalData        = NULL;
	OSAL_tShMemHandle hDevVoltShMem       = OSAL_ERROR;
	OSAL_tSemHandle   hSemDataAccess      = OSAL_C_INVALID_HANDLE;

	OSAL_trThreadControlBlock  rThreadControlBlock;
	OSAL_trProcessControlBlock rProcessControlBlock;

	if (OSAL_s32ProcessControlBlock(
		OSAL_ProcessWhoAmI(),
		&rProcessControlBlock) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32ThreadControlBlock(
		OSAL_ThreadWhoAmI(),
		&rThreadControlBlock) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_1,
		"DEV_VOLT_u32Close() => Called from process '%.15s' (%u) and thread '%.15s' (%u)",
		rProcessControlBlock.szName,
		rProcessControlBlock.id,
		rThreadControlBlock.szName,
		rThreadControlBlock.id);

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_DATA_NAME,
		&hSemDataAccess) == OSAL_ERROR) {
		
		u32OsalErrorCode = OSAL_u32ErrorCode();
		
		// If this central semaphore doesn't exist, then the device wasn't initialized successfully.
		if (u32OsalErrorCode == OSAL_E_DOESNOTEXIST)
			u32OsalErrorCode = OSAL_E_NOTINITIALIZED;

		return u32OsalErrorCode;
	}

	u32OsalErrorCode = DEV_VOLT_u32MapGlobalData(
		&hDevVoltShMem, 
		&prGlobalData);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_map_global_data; /*lint !e801, authorized LINT-deactivation #<71> */

	u32OsalErrorCode = DEV_VOLT_u32RemoveClient(prGlobalData, hSemDataAccess);

	u32TmpOsalErrorCode = DEV_VOLT_u32UnmapGlobalData(hDevVoltShMem,prGlobalData);

	if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Close() => DEV_VOLT_u32UnmapGlobalData() failed with error code = %s",
			OSAL_coszErrorText(u32TmpOsalErrorCode));

error_map_global_data:

	if (OSAL_s32SemaphoreClose(hSemDataAccess) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Close() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return (u32OsalErrorCode);
}

/*******************************************************************************
*
* IO-control function of the OSAL device.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32Control(tS32 s32Fun, intptr_t pnArg)
{
	tU32               u32OsalErrorCode                = OSAL_E_NOERROR;
	tU32               u32TmpOsalErrorCode             = OSAL_E_NOERROR;
	trGlobalData*      prGlobalData                    = NULL;
	OSAL_tShMemHandle  hDevVoltShMem                   = OSAL_ERROR;
	OSAL_tSemHandle    hSemDataAccess                  = OSAL_C_INVALID_HANDLE;

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_DATA_NAME, 
		&hSemDataAccess) == OSAL_ERROR) {
		
		u32OsalErrorCode = OSAL_u32ErrorCode();
		
		// If this central semaphore doesn't exist, then the device wasn't initialized successfully.
		if (u32OsalErrorCode == OSAL_E_DOESNOTEXIST)
			u32OsalErrorCode = OSAL_E_NOTINITIALIZED;

		return u32OsalErrorCode;
	}

	u32OsalErrorCode = DEV_VOLT_u32MapGlobalData(
		&hDevVoltShMem, 
		&prGlobalData);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_shared_memory_map; /*lint !e801, authorized LINT-deactivation #<71> */

	switch (s32Fun)	{
	case OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT: {
		DEV_VOLT_trClientRegistration* prClientRegistration = (DEV_VOLT_trClientRegistration*)pnArg;

		if (prClientRegistration != NULL)
			u32OsalErrorCode = DEV_VOLT_u32RegisterClient(
						prClientRegistration,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT: {
		tU32 u32ClientId = (tU32)pnArg;

		u32OsalErrorCode = DEV_VOLT_u32UnregisterClient(
					u32ClientId,
					prGlobalData,
					hSemDataAccess);
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_REGISTER_SYSTEM_VOLTAGE_NOTIFICATION: {
		DEV_VOLT_trSystemVoltageRegistration* prSystemVoltageRegistration = (DEV_VOLT_trSystemVoltageRegistration*)pnArg;

		if (prSystemVoltageRegistration != NULL)
			u32OsalErrorCode = DEV_VOLT_u32RegisterSystemVoltage(
						prSystemVoltageRegistration,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_SYSTEM_VOLTAGE_NOTIFICATION: {
		tU32 u32ClientId = (tU32)pnArg;

		u32OsalErrorCode = DEV_VOLT_u32UnregisterSystemVoltage(
					u32ClientId,
					prGlobalData,
					hSemDataAccess);
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_STATE: {
		tU32* pu32SystemVoltageState = (tU32*)pnArg;

		if (pu32SystemVoltageState != NULL) {
			u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);
			if (OSAL_E_NOERROR == u32OsalErrorCode) {
				*pu32SystemVoltageState = prGlobalData->u32SystemVoltageState;
				DEV_VOLT_vDataUnlock(hSemDataAccess);
			}
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY: {
		DEV_VOLT_trSystemVoltageHistory* prSystemVoltageHistory = (DEV_VOLT_trSystemVoltageHistory*)pnArg;

		if (prSystemVoltageHistory != NULL)
			u32OsalErrorCode = DEV_VOLT_u32GetSystemVoltageHistory(
						prSystemVoltageHistory,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_REGISTER_USER_VOLTAGE_NOTIFICATION: {
		DEV_VOLT_trUserVoltageRegistration* prUserVoltageRegistration = (DEV_VOLT_trUserVoltageRegistration*)pnArg;

		if (prUserVoltageRegistration != NULL)
			u32OsalErrorCode = DEV_VOLT_u32RegisterUserVoltage(
						prUserVoltageRegistration,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_USER_VOLTAGE_NOTIFICATION: {
		DEV_VOLT_trUserVoltageDeregistration* prUserVoltageDeregistration = (DEV_VOLT_trUserVoltageDeregistration*)pnArg;

		if (prUserVoltageDeregistration != NULL)
			u32OsalErrorCode = DEV_VOLT_u32UnregisterUserVoltage(
						prUserVoltageDeregistration,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_STATE: {
		DEV_VOLT_trUserVoltage* prUserVoltage = (DEV_VOLT_trUserVoltage*)pnArg;

		if (prUserVoltage != NULL)
			u32OsalErrorCode = DEV_VOLT_u32GetUserVoltageState(
						prUserVoltage,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE: {
		tU16* pu16BoardVoltageMv = (tU16*)pnArg;

		if (pu16BoardVoltageMv != NULL) {
			#if defined (GEN3X86) // GEN3X86 = LSim
			*pu16BoardVoltageMv = 14000;
			#else
			u32OsalErrorCode = DEV_VOLT_u32GetBoardVoltageFromOtherContext(
						prGlobalData,
						hSemDataAccess);

			if (OSAL_E_NOERROR == u32OsalErrorCode) {
				u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);
				if (OSAL_E_NOERROR == u32OsalErrorCode) {
					*pu16BoardVoltageMv = prGlobalData->u16BoardVoltageMv;
					DEV_VOLT_vDataUnlock(hSemDataAccess);
				}
			}
			#endif
		} else {
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		}
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND: {
		tU8* pu8TraceData = (tU8*)pnArg;

		if (pu8TraceData != NULL)
			u32OsalErrorCode = DEV_VOLT_u32InjectTTFISCommand(
						pu8TraceData,
						prGlobalData,
						hSemDataAccess);
		else
			u32OsalErrorCode = OSAL_E_INVALIDVALUE;
		break;
	}
	default:
		u32OsalErrorCode = OSAL_E_NOTSUPPORTED;
		break;
	}

	DEV_VOLT_vTraceIOControl(s32Fun, pnArg, u32OsalErrorCode);

	u32TmpOsalErrorCode = DEV_VOLT_u32UnmapGlobalData(hDevVoltShMem,prGlobalData);

	if (u32TmpOsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Control() => DEV_VOLT_u32UnmapGlobalData() failed with error code = %s",
			OSAL_coszErrorText(u32TmpOsalErrorCode));

error_shared_memory_map:

	if (OSAL_s32SemaphoreClose(hSemDataAccess) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32Control() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return (u32OsalErrorCode);
}

/*******************************************************************************
*
* This functions maps the global data of the device to the shared-memory.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32MapGlobalData(OSAL_tShMemHandle* phDevVoltShMem, trGlobalData** pprGlobalData)
{
	*phDevVoltShMem = OSAL_SharedMemoryOpen(DEV_VOLT_C_STRING_SHARED_MEM_NAME, OSAL_EN_READWRITE);

	if (*phDevVoltShMem == OSAL_ERROR)
		return OSAL_u32ErrorCode();

	*pprGlobalData = (trGlobalData*) OSAL_pvSharedMemoryMap(
		*phDevVoltShMem, 
		OSAL_EN_READWRITE, 
		sizeof(trGlobalData), 
		0);

	if (*pprGlobalData == NULL)
	{
		OSAL_s32SharedMemoryClose(*phDevVoltShMem);

		return OSAL_u32ErrorCode();
	}

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This functions un-maps the devices global data from the shared-memory.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32UnmapGlobalData(OSAL_tShMemHandle hDevVoltShMem, trGlobalData* prGlobalData)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (OSAL_s32SharedMemoryUnmap(prGlobalData, sizeof(trGlobalData)) == OSAL_ERROR)
		u32OsalErrorCode = OSAL_u32ErrorCode();

	if (OSAL_s32SharedMemoryClose(hDevVoltShMem) == OSAL_ERROR)
		u32OsalErrorCode = OSAL_u32ErrorCode();

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function locks the devices central data via a semaphore.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32DataLock(OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (OSAL_s32SemaphoreWait(
		hSemDataAccess,
		DEV_VOLT_C_U32_SEM_DATA_TIMEOUT_MS) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32DataLock() => OSAL_s32SemaphoreWait() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function unlocks the devices central data via a semaphore.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDataUnlock(OSAL_tSemHandle hSemDataAccess)
{
	if (OSAL_s32SemaphorePost(hSemDataAccess) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(TR_LEVEL_FATAL, "DEV_VOLT_vDataUnlock() => OSAL_s32SemaphorePost() failed with error code = %s", OSAL_coszErrorText(OSAL_u32ErrorCode()));
}

/*******************************************************************************
*
* This functions adds another anonymous client to the device.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32AddClient(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if (prGlobalData->u8NumberOfClients < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS)
		prGlobalData->u8NumberOfClients++;
	else
		u32OsalErrorCode = OSAL_E_BUSY;

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions removes an anonymous client from the device.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32RemoveClient(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if (prGlobalData->u8NumberOfClients > 0)
		prGlobalData->u8NumberOfClients--;
	else
		u32OsalErrorCode = OSAL_E_DOESNOTEXIST;

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function registers a so far anonymous client at the device.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32RegisterClient(DEV_VOLT_trClientRegistration* prClientRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;
	tU32  u32Index;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	if (prGlobalData->u8NumberOfRegisteredClients < prGlobalData->u8NumberOfClients) {

		u32OsalErrorCode = OSAL_E_UNKNOWN;

		for (u32Index = 0;
		     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
		     u32Index++) {

			if (DEV_VOLT_C_U32_CLIENT_ID_INVALID == prGlobalData->arClientSpecificData[u32Index].u32ClientId) {

				prGlobalData->u8NumberOfRegisteredClients++;
				prGlobalData->u32LatestAssignedClientId++;

				prGlobalData->arClientSpecificData[u32Index].u32ClientId = prGlobalData->u32LatestAssignedClientId;

				(tVoid) OSALUTIL_szSaveStringNCopy(
					prClientRegistration->szNotificationEventName,
					prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
					sizeof(prClientRegistration->szNotificationEventName));

				prClientRegistration->u32ClientId = prGlobalData->arClientSpecificData[u32Index].u32ClientId;

				u32OsalErrorCode = OSAL_E_NOERROR;
				break;
			}
		}
	} else {
		u32OsalErrorCode = OSAL_E_MAXFILES;
	}

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function un-registers an registered client from the device.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32UnregisterClient(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;
	tU32  u32Index;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = OSAL_E_DOESNOTEXIST;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == u32ClientId) {

			prGlobalData->u8NumberOfRegisteredClients--;

			prGlobalData->arClientSpecificData[u32Index].u32ClientId = DEV_VOLT_C_U32_CLIENT_ID_INVALID;

			u32OsalErrorCode =  OSAL_E_NOERROR;
			break;
		}
	}

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions registers a callback function at the TTFIS trace device.
*
*******************************************************************************/
static tVoid DEV_VOLT_vRegisterTraceCallback(tVoid)
{
	g_rModuleData.rTraceIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE, OSAL_EN_READWRITE);

	if (g_rModuleData.rTraceIODescriptor != OSAL_ERROR) {
		OSAL_trIOCtrlLaunchChannel rIOCtrlLaunchChannel;

		rIOCtrlLaunchChannel.pCallback      = (OSAL_tpfCallback)DEV_VOLT_vTraceCallback;
		rIOCtrlLaunchChannel.enTraceChannel = (TR_tenTraceChan) TR_TTFIS_DEV_VOLT;

		if (OSAL_s32IOControl(
			g_rModuleData.rTraceIODescriptor,
			OSAL_C_S32_IOCTRL_CALLBACK_REG, 
			(intptr_t) &rIOCtrlLaunchChannel) == OSAL_ERROR) {

				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_vRegisterTraceCallback() => OSAL_s32IOControl(OSAL_C_S32_IOCTRL_CALLBACK_REG) failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

				if (OSAL_s32IOClose(g_rModuleData.rTraceIODescriptor) == OSAL_OK)
					g_rModuleData.rTraceIODescriptor = OSAL_ERROR;
				else
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_vRegisterTraceCallback() => OSAL_s32IOClose failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	} else {
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vRegisterTraceCallback() => OSAL_IOOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}
}

/*******************************************************************************
*
* This functions un-registers the callback function from the TTFIS trace device.
*
*******************************************************************************/
static tVoid DEV_VOLT_vUnregisterTraceCallback(tVoid)
{
	if (g_rModuleData.rTraceIODescriptor != OSAL_ERROR) {
		OSAL_trIOCtrlLaunchChannel rIOCtrlLaunchChannel;

		rIOCtrlLaunchChannel.pCallback      = (OSAL_tpfCallback)DEV_VOLT_vTraceCallback;
		rIOCtrlLaunchChannel.enTraceChannel = (TR_tenTraceChan) TR_TTFIS_DEV_VOLT;

		if (OSAL_s32IOControl(
			g_rModuleData.rTraceIODescriptor,
			OSAL_C_S32_IOCTRL_CALLBACK_UNREG,
			(intptr_t) &rIOCtrlLaunchChannel) == OSAL_OK) {
				if (OSAL_s32IOClose(g_rModuleData.rTraceIODescriptor) == OSAL_OK)
					g_rModuleData.rTraceIODescriptor = OSAL_ERROR;
				else
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_vUnregisterTraceCallback() => OSAL_s32IOClose failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
		} else {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vUnregisterTraceCallback() => OSAL_s32IOControl(OSAL_C_S32_IOCTRL_CALLBACK_UNREG) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}
}

/*******************************************************************************
*
* This function represents the callback function for the TTFIS trace device.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceCallback(const tU8 * const pu8Data)
{
	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_FATAL, 
		"DEV_VOLT_vTraceCallback() => Received command 0x%02X with size %u", 
		pu8Data[1], 
		pu8Data[0]);

	if (pu8Data[0] <= sizeof(g_rModuleData.au8TraceCallbackBuffer))
	{
		OSAL_pvMemoryCopy(
			g_rModuleData.au8TraceCallbackBuffer,
			&pu8Data[1],
			pu8Data[0]);

		if (OSAL_s32EventPost(
			g_rModuleData.hEventSystemVoltageThread,
			DEV_VOLT_C_U32_EVENT_MASK_TRACE_CALLBACK_COMMAND_RECEIVED,
			OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
		{
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vTraceCallback() => OSAL_s32EventPost() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}
	else
	{
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL, 
			"DEV_VOLT_vTraceCallback() => Size %u of command 0x%02X too long (max = %u). Command not executed", 
			pu8Data[0], 
			pu8Data[1],
			sizeof(g_rModuleData.au8TraceCallbackBuffer));
	}
}

/*******************************************************************************
*
* This function evaluates the received commands which are received via the
* TTFIS trac callback and performs the requested actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vEvaluateTraceCallbackCommand(tVoid)
{
	tU8           u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_EXECUTED;
	tChar const * cszCommandText      = g_acTextExecuted;

	switch (g_rModuleData.au8TraceCallbackBuffer[0]) {
	case 0x01: { // Command = DEV_VOLT_GET_INTERNAL_STATE
		DEV_VOLT_vTraceInternalState(TR_LEVEL_FATAL);
		break;
	}
	case 0x02: { // Command = DEV_VOLT_GET_REGISTERED_CLIENT_DATA
		DEV_VOLT_vTraceRegisteredClientData(TR_LEVEL_FATAL);
		break;
	}
	case 0x03: { // Command = DEV_VOLT_GET_BOARD_VOLTAGE
		if (DEV_VOLT_u32GetBoardVoltage() == OSAL_E_NOERROR) {
			DEV_VOLT_vTraceBoardVoltage();
		} else {
			u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;
		}
		break;
	}
	case 0x80: { // Command = DEV_VOLT_SIMULATE_BOARD_VOLTAGE_ADC
		if (g_rModuleData.hEventUserVoltageThread != OSAL_C_INVALID_HANDLE) {

			tU16 u16BoardVoltageAdc =
				(tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[2]) << 8) |
				        ((tU16)g_rModuleData.au8TraceCallbackBuffer[1])        );

			if (DEV_VOLT_bSimulateBoardVoltage(u16BoardVoltageAdc, 0) == FALSE)
				u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;

		} else {
			u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;
		}
		break;
	}
	case 0x81: { // Command = DEV_VOLT_SIMULATE_BOARD_VOLTAGE_MV
		if (g_rModuleData.hEventUserVoltageThread != OSAL_C_INVALID_HANDLE) {

			tU16 u16BoardVoltageMv =
				(tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[2]) << 8) |
				        ((tU16)g_rModuleData.au8TraceCallbackBuffer[1])        );

			if (DEV_VOLT_bSimulateBoardVoltage(0, u16BoardVoltageMv) == FALSE)
				u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;

		} else {
			u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;
		}
		break;
	}
	case 0x82: { // Command = DEV_VOLT_SIMULATE_VOLTAGE_CHANGE
		if (DEV_VOLT_bSimulateVoltageChange(
			(tenVoltageState)g_rModuleData.au8TraceCallbackBuffer[1],
			g_rModuleData.au8TraceCallbackBuffer[2]) == FALSE)
				u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;
		break;
	}
	case 0x83: { // Command = DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
		tU32 u32VoltageFlags =
			((((tU32)g_rModuleData.au8TraceCallbackBuffer[1]) <<  0) |
			 (((tU32)g_rModuleData.au8TraceCallbackBuffer[2]) <<  8) |
			 (((tU32)g_rModuleData.au8TraceCallbackBuffer[3]) << 16) |
			 (((tU32)g_rModuleData.au8TraceCallbackBuffer[4]) << 24)  );
	
		if (DEV_VOLT_bSimulateCombinedVoltageChanges(u32VoltageFlags) == FALSE)
				u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;
		break;
	}
	case 0xC0: { // Command = DEV_VOLT_INVOKE_CLIENT_THREAD
		if (g_rModuleData.u8ClientThreadState != DEV_VOLT_C_U8_THREAD_RUNNING) {
			if (DEV_VOLT_u32InstallClientThread() != OSAL_E_NOERROR)
				u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;
		} else {
			u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;
		}
		break;
	}
	case 0xC1: { // Command = DEV_VOLT_ADD_USER_VOLTAGE_LEVEL
		tU16 u16UserVoltageLevelMv =
			(tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[2]) << 8) |
			        ((tU16)g_rModuleData.au8TraceCallbackBuffer[1])        );

		tU16 u16HysteresisMv =
			(tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[4]) << 8) |
			        ((tU16)g_rModuleData.au8TraceCallbackBuffer[3])        );

		tU8  u8LevelCrossDirection = g_rModuleData.au8TraceCallbackBuffer[5];

		if (DEV_VOLT_bAddPrivateUserVoltageNotification(
			u16UserVoltageLevelMv,
			u16HysteresisMv,
			u8LevelCrossDirection) == FALSE)
				u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;

		break;
	}
	case 0xC2: { // Command = DEV_VOLT_REMOVE_USER_VOLTAGE_LEVEL
		tU16 u16UserVoltageLevelMv =
			(tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[2]) << 8) |
			        ((tU16)g_rModuleData.au8TraceCallbackBuffer[1])        );

		if (DEV_VOLT_bRemovePrivateUserVoltageNotification(u16UserVoltageLevelMv) == FALSE)
			u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;
		break;
	}

	case 0xC3: { // Command = DEV_VOLT_SET_ADC_THRESHOLD_HARD
		g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc =
			(tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[2]) << 8) |
			        ((tU16)g_rModuleData.au8TraceCallbackBuffer[1])        );

		g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc =
			(tU16)((((tU16)g_rModuleData.au8TraceCallbackBuffer[4]) << 8) |
			        ((tU16)g_rModuleData.au8TraceCallbackBuffer[3])        );

		if (DEV_VOLT_u32SetUserVoltageThresholds() != OSAL_E_NOERROR)
			u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED;
		break;
	}

	default: u8TTFISCommandState = DEV_VOLT_C_U8_TTFIS_COMMAND_UNKNOWN;
		break;
	}

	if (DEV_VOLT_C_U8_TTFIS_COMMAND_UNKNOWN == u8TTFISCommandState) {
		cszCommandText = g_acTextUnknown;
	} else if (DEV_VOLT_C_U8_TTFIS_COMMAND_FAILED == u8TTFISCommandState) {
		cszCommandText = g_acTextFailed;
	}

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_FATAL,
		"DEV_VOLT_vEvaluateTraceCallbackCommand() => Command 0x%02X %s", 
		g_rModuleData.au8TraceCallbackBuffer[0], 
		cszCommandText);
}

/*******************************************************************************
*
* Inject a TTFIS command data stream into the global TTFIS callback
* buffer and post an event to the system voltage thread to evaluate this
* TTFIS command as if it would have been passed by a TTFIS client. This
* handling is protected by a locking semaphore to avoid to overwrite the
* injected TTFIS command before it has been processed and executed.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32InjectTTFISCommand(tU8* pu8TraceData, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32              u32OsalErrorCode          = OSAL_E_NOERROR;
	OSAL_tEventHandle hEventSystemVoltageThread = OSAL_C_INVALID_HANDLE;;
	OSAL_tSemHandle   hSemInjectTTFIS           = OSAL_C_INVALID_HANDLE;

	if ((pu8TraceData[0]+1) > sizeof(prGlobalData->au8InjectTTFISBuffer))
		return OSAL_E_NOSPACE;

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_INJECT_TTFIS_NAME,
		&hSemInjectTTFIS) == OSAL_ERROR)
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreWait(
		hSemInjectTTFIS,
		DEV_VOLT_C_U32_SEM_INJECT_TTFIS_TIMEOUT_MS) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */

	OSAL_pvMemoryCopy(
		prGlobalData->au8InjectTTFISBuffer,
		pu8TraceData,
		pu8TraceData[0]+1U);

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	if (OSAL_s32EventOpen(
		DEV_VOLT_C_STRING_SYSTEM_VOLT_THREAD_EVENT_NAME,
		&hEventSystemVoltageThread) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventPost(
		hEventSystemVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_INJECT_TTFIS_COMMAND,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
		u32OsalErrorCode = OSAL_u32ErrorCode();

	if (OSAL_s32EventClose(hEventSystemVoltageThread) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32InjectTTFISCommand() => OSAL_s32EventClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_out:

	if (OSAL_s32SemaphoreClose(hSemInjectTTFIS) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32InjectTTFISCommand() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}





/*******************************************************************************
*
* 
*
*******************************************************************************/
static tU32 DEV_VOLT_u32ProcessInjectedTTFISCommand(tVoid)
{
	tU32            u32OsalErrorCode = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemInjectTTFIS  = OSAL_C_INVALID_HANDLE;

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_INJECT_TTFIS_NAME,
		&hSemInjectTTFIS) == OSAL_ERROR)
		return OSAL_u32ErrorCode();

	u32OsalErrorCode = DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */

	OSAL_pvMemoryCopy(
		g_rModuleData.au8TraceCallbackBuffer,
		&g_rModuleData.prGlobalData->au8InjectTTFISBuffer[1],
		g_rModuleData.prGlobalData->au8InjectTTFISBuffer[0]);

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);

	DEV_VOLT_vEvaluateTraceCallbackCommand();

	if (OSAL_s32SemaphorePost(hSemInjectTTFIS) == OSAL_ERROR)
		u32OsalErrorCode = OSAL_u32ErrorCode();

error_out:

	if (OSAL_s32SemaphoreClose(hSemInjectTTFIS) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32ProcessInjectedTTFISCommand() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function prints a formatted string on TTFIS.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceFormatted(TR_tenTraceLevel enTraceLevel, const tChar * const coszFormatString, ...)
{
	tU8 au8TraceSendBuffer[DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (LLD_bIsTraceActive(TR_CLASS_DEV_VOLT, enTraceLevel) != FALSE) {
		au8TraceSendBuffer[0] = DEV_VOLT_C_U8_TRACE_TYPE_STRING;

		OSAL_tVarArgList ArgList; 
		OSAL_VarArgStart(ArgList, coszFormatString); //lint !e530
		OSAL_s32VarNPrintFormat(
			(char*)(&(au8TraceSendBuffer[1])), 
			sizeof(au8TraceSendBuffer) - 1, 
			coszFormatString, 
			ArgList); //lint !e530
			
		OSAL_VarArgEnd(ArgList);

		LLD_vTrace(
			TR_CLASS_DEV_VOLT,
			enTraceLevel,
			au8TraceSendBuffer,
			(tU32)(OSAL_u32StringLength(au8TraceSendBuffer) + 1U));
	}
}

/*******************************************************************************
*
* This function prints the internal state of the device on TTFIS.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceInternalState(TR_tenTraceLevel enTraceLevel)
{
	tU8 au8TraceSendBuffer[DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH];

	tU16 u16UpperUserVoltageThreshold = DEV_VOLT_u16ConvertBoardVoltageAdcToMv(g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc, g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess);
	tU16 u16LowerUserVoltageThreshold = DEV_VOLT_u16ConvertBoardVoltageAdcToMv(g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc, g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess);

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	if (LLD_bIsTraceActive(TR_CLASS_DEV_VOLT, enTraceLevel) != FALSE) {
		au8TraceSendBuffer[0]  = DEV_VOLT_C_U8_TRACE_TYPE_INTERNAL_DATA;

		au8TraceSendBuffer[1]  = (tU8)((((tU32)g_rModuleData.u32DoLowVoltageDetection) & 0x000000FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[2]  = (tU8)((((tU32)g_rModuleData.u32DoLowVoltageDetection) & 0x0000FF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[3]  = (tU8)((((tU32)g_rModuleData.u32DoLowVoltageDetection) & 0x00FF0000) >> 16); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[4]  = (tU8)((((tU32)g_rModuleData.u32DoLowVoltageDetection) & 0xFF000000) >> 24); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[5]  = (tU8)((((tU32)g_rModuleData.u32LowVoltageOverDelayMs) & 0x000000FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[6]  = (tU8)((((tU32)g_rModuleData.u32LowVoltageOverDelayMs) & 0x0000FF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[7]  = (tU8)((((tU32)g_rModuleData.u32LowVoltageOverDelayMs) & 0x00FF0000) >> 16); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[8]  = (tU8)((((tU32)g_rModuleData.u32LowVoltageOverDelayMs) & 0xFF000000) >> 24); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[9]  = (tU8)((((tU32)g_rModuleData.u32DoCriticalLowVoltageDetection) & 0x000000FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[10] = (tU8)((((tU32)g_rModuleData.u32DoCriticalLowVoltageDetection) & 0x0000FF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[11] = (tU8)((((tU32)g_rModuleData.u32DoCriticalLowVoltageDetection) & 0x00FF0000) >> 16); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[12] = (tU8)((((tU32)g_rModuleData.u32DoCriticalLowVoltageDetection) & 0xFF000000) >> 24); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[13]  = (tU8)((((tU32)g_rModuleData.u32CriticalLowVoltageOverDelayMs) & 0x000000FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[14]  = (tU8)((((tU32)g_rModuleData.u32CriticalLowVoltageOverDelayMs) & 0x0000FF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[15]  = (tU8)((((tU32)g_rModuleData.u32CriticalLowVoltageOverDelayMs) & 0x00FF0000) >> 16); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[16]  = (tU8)((((tU32)g_rModuleData.u32CriticalLowVoltageOverDelayMs) & 0xFF000000) >> 24); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[17]  = (tU8)(((tU16)(g_rModuleData.u16BoardVoltageHighMv) & 0x00FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[18] = (tU8)(((tU16)(g_rModuleData.u16BoardVoltageHighMv) & 0xFF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[19] = (tU8)(((tU16)(g_rModuleData.u16BoardVoltageHighHysMv) & 0x00FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[20] = (tU8)(((tU16)(g_rModuleData.u16BoardVoltageHighHysMv) & 0xFF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[21] = (tU8)(((tU32)(g_rModuleData.u32HighVoltageTimerMs) & 0x000000FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[22] = (tU8)(((tU32)(g_rModuleData.u32HighVoltageTimerMs) & 0x0000FF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[23] = (tU8)(((tU32)(g_rModuleData.u32HighVoltageTimerMs) & 0x00FF0000) >> 16); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[24] = (tU8)(((tU32)(g_rModuleData.u32HighVoltageTimerMs) & 0xFF000000) >> 24); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[25] = (tU8)(((tU16)(g_rModuleData.u16BoardVoltageCriticalHighMv) & 0x00FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[26] = (tU8)(((tU16)(g_rModuleData.u16BoardVoltageCriticalHighMv) & 0xFF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[27] = (tU8)(((tU16)(g_rModuleData.u16BoardVoltageCriticalHighHysMv) & 0x00FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[28] = (tU8)(((tU16)(g_rModuleData.u16BoardVoltageCriticalHighHysMv) & 0xFF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[29] = (tU8)(((tU32)(g_rModuleData.u32CriticalHighVoltageTimerMs) & 0x000000FF) >>  0); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[30] = (tU8)(((tU32)(g_rModuleData.u32CriticalHighVoltageTimerMs) & 0x0000FF00) >>  8); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[31] = (tU8)(((tU32)(g_rModuleData.u32CriticalHighVoltageTimerMs) & 0x00FF0000) >> 16); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */
		au8TraceSendBuffer[32] = (tU8)(((tU32)(g_rModuleData.u32CriticalHighVoltageTimerMs) & 0xFF000000) >> 24); /*lint !e778 !e572, authorized LINT-deactivation #<72> #<73> */

		au8TraceSendBuffer[33] = (tU8)(((tU16)(g_rModuleData.prGlobalData->u16BoardVoltageOffsetMv) & 0x00FF) >>  0);
		au8TraceSendBuffer[34] = (tU8)(((tU16)(g_rModuleData.prGlobalData->u16BoardVoltageOffsetMv) & 0xFF00) >>  8);
		au8TraceSendBuffer[35] = g_rModuleData.bReadBoardVoltageOffsetFromDeviceTree;

		au8TraceSendBuffer[36] = (tU8)(((tU16)(g_rModuleData.prGlobalData->u16BoardVoltageGradientMultipliedBy1000) & 0x00FF) >>  0);
		au8TraceSendBuffer[37] = (tU8)(((tU16)(g_rModuleData.prGlobalData->u16BoardVoltageGradientMultipliedBy1000) & 0xFF00) >>  8);
		au8TraceSendBuffer[38] = g_rModuleData.bReadBoardVoltageGradienFromDeviceTree;

		au8TraceSendBuffer[39] = (g_rModuleData.prGlobalData->u32GetBoardVoltageResult == OSAL_E_NOERROR) ? 1 : 0;
		au8TraceSendBuffer[40] = g_rModuleData.bIsLowVoltageIrqRegistered;
		au8TraceSendBuffer[41] = g_rModuleData.bIsCriticalLowVoltageIrqRegistered;

		au8TraceSendBuffer[42] = g_rModuleData.bBoardVoltageSimulationActive;

		au8TraceSendBuffer[43] = g_rModuleData.u8SystemVoltageThreadState;
		au8TraceSendBuffer[44] = g_rModuleData.u8UserVoltageThreadState;
		au8TraceSendBuffer[45] = g_rModuleData.u8HighVoltageThreadState;
		au8TraceSendBuffer[46] = g_rModuleData.u8ClientThreadState;

		au8TraceSendBuffer[47] = (tU8)(((g_rModuleData.rProtectedModuleData.u32LowVoltageFlags) & 0x000000FF) >>  0);
		au8TraceSendBuffer[48] = (tU8)(((g_rModuleData.rProtectedModuleData.u32LowVoltageFlags) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[49] = (tU8)(((g_rModuleData.rProtectedModuleData.u32LowVoltageFlags) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[50] = (tU8)(((g_rModuleData.rProtectedModuleData.u32LowVoltageFlags) & 0xFF000000) >> 24);
		au8TraceSendBuffer[51] = (tU8)(((g_rModuleData.rProtectedModuleData.u32HighVoltageFlags) & 0x000000FF) >>  0);
		au8TraceSendBuffer[52] = (tU8)(((g_rModuleData.rProtectedModuleData.u32HighVoltageFlags) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[53] = (tU8)(((g_rModuleData.rProtectedModuleData.u32HighVoltageFlags) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[54] = (tU8)(((g_rModuleData.rProtectedModuleData.u32HighVoltageFlags) & 0xFF000000) >> 24);
		au8TraceSendBuffer[55] = g_rModuleData.prGlobalData->u8PermHighVoltageCounter;
		au8TraceSendBuffer[56] = g_rModuleData.prGlobalData->u8PermCriticalHighVoltageCounter;
		au8TraceSendBuffer[57] = (tU8)(((u16UpperUserVoltageThreshold) & 0x00FF) >>  0);
		au8TraceSendBuffer[58] = (tU8)(((u16UpperUserVoltageThreshold) & 0xFF00) >>  8);
		au8TraceSendBuffer[59] = (tU8)(((g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc) & 0x00FF) >>  0);
		au8TraceSendBuffer[60] = (tU8)(((g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc) & 0xFF00) >>  8);
		au8TraceSendBuffer[61] = (tU8)(((u16LowerUserVoltageThreshold) & 0x00FF) >>  0);
		au8TraceSendBuffer[62] = (tU8)(((u16LowerUserVoltageThreshold) & 0xFF00) >>  8);
		au8TraceSendBuffer[63] = (tU8)(((g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc) & 0x00FF) >>  0);
		au8TraceSendBuffer[64] = (tU8)(((g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc) & 0xFF00) >>  8);

		au8TraceSendBuffer[65] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[66] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[67] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[68] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0xFF000000) >> 24);

		au8TraceSendBuffer[69] = (tU8)(((g_rModuleData.prGlobalData->u32PreviousSystemVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[70] = (tU8)(((g_rModuleData.prGlobalData->u32PreviousSystemVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[71] = (tU8)(((g_rModuleData.prGlobalData->u32PreviousSystemVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[72] = (tU8)(((g_rModuleData.prGlobalData->u32PreviousSystemVoltageState) & 0xFF000000) >> 24);
		au8TraceSendBuffer[73] = (tU8)(((g_rModuleData.prGlobalData->u32SystemVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[74] = (tU8)(((g_rModuleData.prGlobalData->u32SystemVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[75] = (tU8)(((g_rModuleData.prGlobalData->u32SystemVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[76] = (tU8)(((g_rModuleData.prGlobalData->u32SystemVoltageState) & 0xFF000000) >> 24);

		au8TraceSendBuffer[77] = g_rModuleData.prGlobalData->u8NumberOfClients;
		au8TraceSendBuffer[78] = g_rModuleData.prGlobalData->u8NumberOfRegisteredClients;
		au8TraceSendBuffer[79] = (tU8)(((g_rModuleData.prGlobalData->u32LatestAssignedClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[80] = (tU8)(((g_rModuleData.prGlobalData->u32LatestAssignedClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[81] = (tU8)(((g_rModuleData.prGlobalData->u32LatestAssignedClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[82] = (tU8)(((g_rModuleData.prGlobalData->u32LatestAssignedClientId) & 0xFF000000) >> 24);

		LLD_vTrace(
			TR_CLASS_DEV_VOLT,
			enTraceLevel,
			au8TraceSendBuffer,
			83);
	}

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This functions prints information of each registered client on TTFIS.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceRegisteredClientData(TR_tenTraceLevel enTraceLevel)
{
	tU8  au8TraceSendBuffer[DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH];
	tU8 u8Index1;
	tU8 u8Index2;

	if (LLD_bIsTraceActive(TR_CLASS_DEV_VOLT, enTraceLevel) == FALSE)
		return;

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;
	
	for (u8Index1 = 0;
	    u8Index1 < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS; 
	    u8Index1++) {

		if (g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u32ClientId != DEV_VOLT_C_U32_CLIENT_ID_INVALID) {
			au8TraceSendBuffer[0] = DEV_VOLT_C_U8_TRACE_TYPE_REGISTERED_CLIENT_DATA;

			au8TraceSendBuffer[1] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u32ClientId) & 0x000000FF) >>  0);
			au8TraceSendBuffer[2] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u32ClientId) & 0x0000FF00) >>  8);
			au8TraceSendBuffer[3] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u32ClientId) & 0x00FF0000) >> 16);
			au8TraceSendBuffer[4] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u32ClientId) & 0xFF000000) >> 24);

			for (u8Index2 = 0; 
			     u8Index2 < DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH; 
			     u8Index2++)
				au8TraceSendBuffer[5+u8Index2] = g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].szNotificationEventName[u8Index2];

			au8TraceSendBuffer[5+DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u32SystemVoltageIndicationMask) & 0x000000FF) >>  0);
			au8TraceSendBuffer[6+DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u32SystemVoltageIndicationMask) & 0x0000FF00) >>  8);
			au8TraceSendBuffer[7+DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u32SystemVoltageIndicationMask) & 0x00FF0000) >> 16);
			au8TraceSendBuffer[8+DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH] = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u32SystemVoltageIndicationMask) & 0xFF000000) >> 24);

			au8TraceSendBuffer[9+DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH] = g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u8NumOfUserVoltageLevelsSet;

			LLD_vTrace(
				TR_CLASS_DEV_VOLT,
				enTraceLevel,
				au8TraceSendBuffer,
				11 + DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH);

			if (g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].u8NumOfUserVoltageLevelsSet != 0) {
				for (u8Index2 = 0; 
				     u8Index2 < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_USER_VOLTAGES; 
				     u8Index2++) {

					if (g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u16LevelAdc != 0) {
						au8TraceSendBuffer[0]  = DEV_VOLT_C_U8_TRACE_TYPE_REGISTERED_CLIENT_DATA_USER_VOLTAGES;
						au8TraceSendBuffer[1]  = u8Index2;
						au8TraceSendBuffer[2]  = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u16LevelMv) & 0x00FF) >>  0);
						au8TraceSendBuffer[3]  = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u16LevelMv) & 0xFF00) >>  8);
						au8TraceSendBuffer[4]  = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u16LevelAdc) & 0x00FF) >>  0);
						au8TraceSendBuffer[5]  = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u16LevelAdc) & 0xFF00) >>  8);
						au8TraceSendBuffer[6]  = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u16HysteresisMv) & 0x00FF) >>  0);
						au8TraceSendBuffer[7]  = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u16HysteresisMv) & 0xFF00) >>  8);
						au8TraceSendBuffer[8]  = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u16HysteresisAdc) & 0x00FF) >>  0);
						au8TraceSendBuffer[9]  = (tU8)(((g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u16HysteresisAdc) & 0xFF00) >>  8);
						au8TraceSendBuffer[10] = g_rModuleData.prGlobalData->arClientSpecificData[u8Index1].arUserVoltage[u8Index2].u8CrossDirection;

						LLD_vTrace(
							TR_CLASS_DEV_VOLT,
							enTraceLevel,
							au8TraceSendBuffer,
							11);
					}
				}
			}
		}
	}

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function prints the IO-control accesses to the device on TTFIS.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceIOControl(tS32 s32Fun, intptr_t pnArg, tU32 u32OsalErrorCode)
{
	tU8 au8TraceSendBuffer[DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH];

	tU8              u8BufferLength = 0;
	TR_tenTraceLevel enTraceLevel   = TR_LEVEL_USER_1;

	tU32 u32Index;

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		enTraceLevel = TR_LEVEL_ERRORS;

	if (LLD_bIsTraceActive(TR_CLASS_DEV_VOLT, enTraceLevel) == FALSE)
		return;
	
	au8TraceSendBuffer[0] = DEV_VOLT_C_U8_TRACE_TYPE_IO_CONTROL;
	au8TraceSendBuffer[1] = (tU8)(((tU32)s32Fun & 0x000000FF) >>  0);
	au8TraceSendBuffer[2] = (tU8)(((tU32)s32Fun & 0x0000FF00) >>  8);
	au8TraceSendBuffer[3] = (tU8)(((tU32)s32Fun & 0x00FF0000) >> 16);
	au8TraceSendBuffer[4] = (tU8)(((tU32)s32Fun & 0xFF000000) >> 24);

	if (u32OsalErrorCode == OSAL_E_NOERROR)
		au8TraceSendBuffer[5] = DEV_VOLT_C_U8_OK;
	else
		au8TraceSendBuffer[5] = DEV_VOLT_C_U8_ERROR;

	au8TraceSendBuffer[6] = (tU8)(((u32OsalErrorCode) & 0x000000FF) >>  0);
	au8TraceSendBuffer[7] = (tU8)(((u32OsalErrorCode) & 0x0000FF00) >>  8);
	au8TraceSendBuffer[8] = (tU8)(((u32OsalErrorCode) & 0x00FF0000) >> 16);
	au8TraceSendBuffer[9] = (tU8)(((u32OsalErrorCode) & 0xFF000000) >> 24);

	switch(s32Fun) {
	case OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_STATE : {
		tU32* pu32SystemVoltageState = (tU32*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((*pu32SystemVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((*pu32SystemVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((*pu32SystemVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((*pu32SystemVoltageState) & 0xFF000000) >> 24);

		u8BufferLength = 14;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT : {
		DEV_VOLT_trClientRegistration* prClientRegistration = (DEV_VOLT_trClientRegistration*)pnArg;

		au8TraceSendBuffer[10]   = (tU8)(((prClientRegistration->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11]   = (tU8)(((prClientRegistration->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12]   = (tU8)(((prClientRegistration->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13]   = (tU8)(((prClientRegistration->u32ClientId) & 0xFF000000) >> 24);

		for (u32Index = 0; 
		     u32Index < DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH; 
		     u32Index++)
			au8TraceSendBuffer[14 + u32Index] = prClientRegistration->szNotificationEventName[u32Index];

		u8BufferLength = 14 + DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT : {
		tU32 u32ClientId = (tU32)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((u32ClientId) & 0xFF000000) >> 24);

		u8BufferLength = 14;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_REGISTER_SYSTEM_VOLTAGE_NOTIFICATION : {
		DEV_VOLT_trSystemVoltageRegistration* prSystemVoltageRegistration = (DEV_VOLT_trSystemVoltageRegistration*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prSystemVoltageRegistration->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prSystemVoltageRegistration->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((prSystemVoltageRegistration->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((prSystemVoltageRegistration->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[14] = (tU8)(((prSystemVoltageRegistration->u32VoltageIndicationMask) & 0x000000FF) >>  0);
		au8TraceSendBuffer[15] = (tU8)(((prSystemVoltageRegistration->u32VoltageIndicationMask) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[16] = (tU8)(((prSystemVoltageRegistration->u32VoltageIndicationMask) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[17] = (tU8)(((prSystemVoltageRegistration->u32VoltageIndicationMask) & 0xFF000000) >> 24);
		au8TraceSendBuffer[18] = prSystemVoltageRegistration->u8PermHighVoltageCounter;
		au8TraceSendBuffer[19] = prSystemVoltageRegistration->u8PermCriticalHighVoltageCounter;
		au8TraceSendBuffer[20] = (tU8)(((prSystemVoltageRegistration->u32CurrentSystemVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[21] = (tU8)(((prSystemVoltageRegistration->u32CurrentSystemVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[22] = (tU8)(((prSystemVoltageRegistration->u32CurrentSystemVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[23] = (tU8)(((prSystemVoltageRegistration->u32CurrentSystemVoltageState) & 0xFF000000) >> 24);

		u8BufferLength = 24;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_SYSTEM_VOLTAGE_NOTIFICATION : {
		tU32 u32ClientId = (tU32)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((u32ClientId) & 0xFF000000) >> 24);

		u8BufferLength = 14;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY : {
		DEV_VOLT_trSystemVoltageHistory* prSystemVoltageHistory = (DEV_VOLT_trSystemVoltageHistory*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prSystemVoltageHistory->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prSystemVoltageHistory->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((prSystemVoltageHistory->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((prSystemVoltageHistory->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[14] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CurrentSystemVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[15] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CurrentSystemVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[16] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CurrentSystemVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[17] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CurrentSystemVoltageState) & 0xFF000000) >> 24);
		au8TraceSendBuffer[18] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CriticalLowVoltageCounter) & 0x000000FF) >>  0);
		au8TraceSendBuffer[19] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CriticalLowVoltageCounter) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[20] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CriticalLowVoltageCounter) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[21] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CriticalLowVoltageCounter) & 0xFF000000) >> 24);
		au8TraceSendBuffer[22] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32LowVoltageCounter) & 0x000000FF) >>  0);
		au8TraceSendBuffer[23] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32LowVoltageCounter) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[24] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32LowVoltageCounter) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[25] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32LowVoltageCounter) & 0xFF000000) >> 24);
		au8TraceSendBuffer[26] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32HighVoltageCounter) & 0x000000FF) >>  0);
		au8TraceSendBuffer[27] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32HighVoltageCounter) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[28] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32HighVoltageCounter) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[29] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32HighVoltageCounter) & 0xFF000000) >> 24);
		au8TraceSendBuffer[30] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CriticalHighVoltageCounter) & 0x000000FF) >>  0);
		au8TraceSendBuffer[31] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CriticalHighVoltageCounter) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[32] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CriticalHighVoltageCounter) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[33] = (tU8)(((prSystemVoltageHistory->rSystemVoltage.u32CriticalHighVoltageCounter) & 0xFF000000) >> 24);

		u8BufferLength = 34;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE : {
		tU16* pu16BoardVoltageMv = (tU16*)pnArg;

		au8TraceSendBuffer[10]  = (tU8)(((*pu16BoardVoltageMv) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11]  = (tU8)(((*pu16BoardVoltageMv) & 0x0000FF00) >>  8);

		u8BufferLength = 12;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_REGISTER_USER_VOLTAGE_NOTIFICATION : {
		DEV_VOLT_trUserVoltageRegistration* prUserVoltageRegistration = (DEV_VOLT_trUserVoltageRegistration*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prUserVoltageRegistration->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prUserVoltageRegistration->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((prUserVoltageRegistration->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((prUserVoltageRegistration->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[14] = (tU8)(((prUserVoltageRegistration->u16UserVoltageLevelMv) & 0x00FF) >>  0);
		au8TraceSendBuffer[15] = (tU8)(((prUserVoltageRegistration->u16UserVoltageLevelMv) & 0xFF00) >>  8);
		au8TraceSendBuffer[16] = (tU8)(((prUserVoltageRegistration->u16HysteresisMv) & 0x00FF) >>  0);
		au8TraceSendBuffer[17] = (tU8)(((prUserVoltageRegistration->u16HysteresisMv) & 0xFF00) >>  8);
		au8TraceSendBuffer[18] = prUserVoltageRegistration->u8LevelCrossDirection;
		au8TraceSendBuffer[19] = prUserVoltageRegistration->u8UserVoltageState;

		u8BufferLength = 20;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_USER_VOLTAGE_NOTIFICATION : {
		DEV_VOLT_trUserVoltageDeregistration* prUserVoltageDeregistration = (DEV_VOLT_trUserVoltageDeregistration*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prUserVoltageDeregistration->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prUserVoltageDeregistration->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((prUserVoltageDeregistration->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((prUserVoltageDeregistration->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[14] = (tU8)(((prUserVoltageDeregistration->u16UserVoltageLevelMv) & 0x00FF) >>  0);
		au8TraceSendBuffer[15] = (tU8)(((prUserVoltageDeregistration->u16UserVoltageLevelMv) & 0xFF00) >>  8);

		u8BufferLength = 16;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_STATE : {
		DEV_VOLT_trUserVoltage* prUserVoltage = (DEV_VOLT_trUserVoltage*)pnArg;

		au8TraceSendBuffer[10] = (tU8)(((prUserVoltage->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[11] = (tU8)(((prUserVoltage->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[12] = (tU8)(((prUserVoltage->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[13] = (tU8)(((prUserVoltage->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[14] = (tU8)(((prUserVoltage->u16LatestCrossedUserVoltageLevelMv) & 0x00FF) >>  0);
		au8TraceSendBuffer[15] = (tU8)(((prUserVoltage->u16LatestCrossedUserVoltageLevelMv) & 0xFF00) >>  8);
		au8TraceSendBuffer[16] = prUserVoltage->u8LatestUserVoltageState;

		u8BufferLength = 17;
		break;
	}
	case OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND: {
		tU8* pu8TraceCallbackData = (tU8*)pnArg;

		OSAL_pvMemoryCopy(
			&au8TraceSendBuffer[10],
			pu8TraceCallbackData,
			DEV_VOLT_C_U8_TRACE_RECEIVE_BUFFER_LENGTH);

		u8BufferLength = 10 + DEV_VOLT_C_U8_TRACE_RECEIVE_BUFFER_LENGTH;
		break;
	}
	default:
		// Intentionally do nothing
		break;
	}

	if (u8BufferLength != 0)
		LLD_vTrace(
			TR_CLASS_DEV_VOLT,
			enTraceLevel,
			au8TraceSendBuffer,
			u8BufferLength);
}

/*******************************************************************************
*
* This function prints the state of the internal voltage flags on TTFIS.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceVoltageFlags(tU32 u32AccumulatedVoltageFlags)
{
	tU8 au8TraceSendBuffer[DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (LLD_bIsTraceActive(TR_CLASS_DEV_VOLT, TR_LEVEL_USER_3) != FALSE) {
		au8TraceSendBuffer[0]  = DEV_VOLT_C_U8_TRACE_TYPE_VOLTAGE_FLAGS;
		au8TraceSendBuffer[1]  = ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE)         ? 0x03 : 0x01);
		if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED)
			au8TraceSendBuffer[1]++;
		au8TraceSendBuffer[2]  = ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE)                  ? 0x03 : 0x01);
		if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED)
			au8TraceSendBuffer[2]++;
		au8TraceSendBuffer[3]  = ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE)                   ? 0x03 : 0x01);
		if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED)
			au8TraceSendBuffer[3]++;
		au8TraceSendBuffer[4]  = ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE)          ? 0x03 : 0x01);
		if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED)
			au8TraceSendBuffer[4]++;
		au8TraceSendBuffer[5]  = ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED) ? 0x01 : 0x00);
		au8TraceSendBuffer[6]  = ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED)          ? 0x01 : 0x00);
		au8TraceSendBuffer[7]  = ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_TIMER_EXPIRED)           ? 0x01 : 0x00);
		au8TraceSendBuffer[8]  = ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED)  ? 0x01 : 0x00);
		au8TraceSendBuffer[9]  = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[10] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[11] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[12] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0xFF000000) >> 24);

		LLD_vTrace(
			TR_CLASS_DEV_VOLT,
			TR_LEVEL_USER_3,
			au8TraceSendBuffer,
			13);
	}
}

/*******************************************************************************
*
* This function prints the requested, previous and current voltage state on
* TTFIS.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceVoltageState(tenVoltageState enPreviousVoltageState)
{
	tU8 au8TraceSendBuffer[DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (LLD_bIsTraceActive(TR_CLASS_DEV_VOLT, TR_LEVEL_USER_3) != FALSE) {
		au8TraceSendBuffer[0]  = DEV_VOLT_C_U8_TRACE_TYPE_VOLTAGE_STATE;
		au8TraceSendBuffer[1]  = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enRequestedVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[2]  = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enRequestedVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[3]  = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enRequestedVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[4]  = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enRequestedVoltageState) & 0xFF000000) >> 24);
		au8TraceSendBuffer[5]  = (tU8)((((tU32)enPreviousVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[6]  = (tU8)((((tU32)enPreviousVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[7]  = (tU8)((((tU32)enPreviousVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[8]  = (tU8)((((tU32)enPreviousVoltageState) & 0xFF000000) >> 24);
		au8TraceSendBuffer[9]  = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0x000000FF) >>  0);
		au8TraceSendBuffer[10] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[11] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[12] = (tU8)((((tU32)g_rModuleData.rStateMachineContext.enCurrentVoltageState) & 0xFF000000) >> 24);

		LLD_vTrace(
			TR_CLASS_DEV_VOLT,
			TR_LEVEL_USER_3,
			au8TraceSendBuffer,
			13);
	}
}

/*******************************************************************************
*
* This function prints the event-mask, ID and event-name of registered clients
* which just got notified about changed properties via an OSAL event on TTFIS.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceNotificatonEvent(tU32 u32ClientId, tString szNotificationEventName, OSAL_tEventMask rEventMask)
{
	tU32 u32Index;
	tU8  au8TraceSendBuffer[DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (LLD_bIsTraceActive(TR_CLASS_DEV_VOLT, TR_LEVEL_USER_3) != FALSE) {
		au8TraceSendBuffer[0]  = DEV_VOLT_C_U8_TRACE_TYPE_NOTIFICATION_EVENT;
		au8TraceSendBuffer[1] = (tU8)(((rEventMask) & 0x000000FF) >>  0);
		au8TraceSendBuffer[2] = (tU8)(((rEventMask) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[3] = (tU8)(((rEventMask) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[4] = (tU8)(((rEventMask) & 0xFF000000) >> 24);
		au8TraceSendBuffer[5] = (tU8)(((u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[6] = (tU8)(((u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[7] = (tU8)(((u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[8] = (tU8)(((u32ClientId) & 0xFF000000) >> 24);

		for (u32Index = 0;
		     u32Index < DEV_VOLT_C_U8_NOTIFICATION_EVENT_NAME_LENGTH;
		     u32Index++)
			au8TraceSendBuffer[9+u32Index] = szNotificationEventName[u32Index];

		LLD_vTrace(
			TR_CLASS_DEV_VOLT,
			TR_LEVEL_USER_3,
			au8TraceSendBuffer,
			9 + u32Index - 1);
	}
}

/*******************************************************************************
*
* This function prints the current value of the board voltage on TTFIS.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceBoardVoltage(tVoid)
{
	tU8 au8TraceSendBuffer[DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	au8TraceSendBuffer[0]  = DEV_VOLT_C_U8_TRACE_TYPE_BOARD_VOLTAGE;
	au8TraceSendBuffer[1]  = (tU8)(((g_rModuleData.prGlobalData->u16BoardVoltageMv) & 0x00FF) >>  0);
	au8TraceSendBuffer[2]  = (tU8)(((g_rModuleData.prGlobalData->u16BoardVoltageMv) & 0xFF00) >>  8);
	au8TraceSendBuffer[3]  = (tU8)(((g_rModuleData.prGlobalData->u16BoardVoltageAdc) & 0x00FF) >>  0);
	au8TraceSendBuffer[4]  = (tU8)(((g_rModuleData.prGlobalData->u16BoardVoltageAdc) & 0xFF00) >>  8);

	LLD_vTrace(
		TR_CLASS_DEV_VOLT,
		TR_LEVEL_FATAL,
		au8TraceSendBuffer,
		5);

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function prints the parameters for a registration of user voltage
* notification on TTFIS.
*
*******************************************************************************/
static tVoid DEV_VOLT_vTraceRegisterUserVoltage(DEV_VOLT_trUserVoltageRegistration* prUserVoltageRegistration, tU16 u16UserVoltageAdc, tU16 u16HysteresisAdc, trGlobalData* prGlobalData)
{
	tU8 au8TraceSendBuffer[DEV_VOLT_C_U8_TRACE_SEND_BUFFER_LENGTH];

	if (LLD_bIsTraceActive(TR_CLASS_DEV_VOLT, TR_LEVEL_USER_3) != FALSE) {
		au8TraceSendBuffer[0]  = DEV_VOLT_C_U8_TRACE_TYPE_REGISTER_USER_VOLTAGE;
		au8TraceSendBuffer[1]  = (tU8)(((prUserVoltageRegistration->u32ClientId) & 0x000000FF) >>  0);
		au8TraceSendBuffer[2]  = (tU8)(((prUserVoltageRegistration->u32ClientId) & 0x0000FF00) >>  8);
		au8TraceSendBuffer[3]  = (tU8)(((prUserVoltageRegistration->u32ClientId) & 0x00FF0000) >> 16);
		au8TraceSendBuffer[4]  = (tU8)(((prUserVoltageRegistration->u32ClientId) & 0xFF000000) >> 24);
		au8TraceSendBuffer[5]  = (tU8)(((prUserVoltageRegistration->u16UserVoltageLevelMv) & 0x00FF) >>  0);
		au8TraceSendBuffer[6]  = (tU8)(((prUserVoltageRegistration->u16UserVoltageLevelMv) & 0xFF00) >>  8);
		au8TraceSendBuffer[7]  = (tU8)(((u16UserVoltageAdc) & 0x00FF) >>  0);
		au8TraceSendBuffer[8]  = (tU8)(((u16UserVoltageAdc) & 0xFF00) >>  8);
		au8TraceSendBuffer[9]  = (tU8)(((prUserVoltageRegistration->u16HysteresisMv) & 0x00FF) >>  0);
		au8TraceSendBuffer[10] = (tU8)(((prUserVoltageRegistration->u16HysteresisMv) & 0xFF00) >>  8);
		au8TraceSendBuffer[11] = (tU8)(((u16HysteresisAdc) & 0x00FF) >>  0);
		au8TraceSendBuffer[12] = (tU8)(((u16HysteresisAdc) & 0xFF00) >>  8);
		au8TraceSendBuffer[13] = prUserVoltageRegistration->u8LevelCrossDirection;
		au8TraceSendBuffer[14] = (tU8)(((prGlobalData->u16BoardVoltageMv) & 0x00FF) >>  0);
		au8TraceSendBuffer[15] = (tU8)(((prGlobalData->u16BoardVoltageMv) & 0xFF00) >>  8);
		au8TraceSendBuffer[16] = (tU8)(((prGlobalData->u16BoardVoltageAdc) & 0x00FF) >>  0);
		au8TraceSendBuffer[17] = (tU8)(((prGlobalData->u16BoardVoltageAdc) & 0xFF00) >>  8);
		au8TraceSendBuffer[18] = prUserVoltageRegistration->u8UserVoltageState;

		LLD_vTrace(
			TR_CLASS_DEV_VOLT,
			TR_LEVEL_USER_3,
			au8TraceSendBuffer,
			19);
	}
}

/*******************************************************************************
*
* This function creates client individual notification events which are used to 
* notify registered clients about property changes.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32CreateClientNotificationEvents(tVoid)
{
	tU32 u32Index;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (OSALUTIL_s32SaveNPrintFormat(
			g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
			sizeof(g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName),
			DEV_VOLT_C_STRING_NOTIFICATION_EVENT_POSTFIX_FORMAT,
			DEV_VOLT_C_STRING_NOTIFICATION_EVENT_PREFIX,
			u32Index+1) < 0)
				return OSAL_E_UNKNOWN;

		if (OSAL_s32EventCreate(
			g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
			&g_rModuleData.ahClientEventHandle[u32Index]) == OSAL_ERROR)
				return OSAL_u32ErrorCode();
	}

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This function deletes the client individual notification events.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDeleteClientNotificationEvents(tVoid)
{
	tU32 u32Index;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (g_rModuleData.ahClientEventHandle[u32Index] != OSAL_C_INVALID_HANDLE) {
			if (OSAL_s32EventClose(g_rModuleData.ahClientEventHandle[u32Index]) == OSAL_OK) {
				if (OSAL_s32EventDelete(g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName) == OSAL_OK) {
					g_rModuleData.ahClientEventHandle[u32Index] = OSAL_C_INVALID_HANDLE;
					g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName[0] = '\0';
				} else {
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_u32DeleteClientNotificationEvents() => OSAL_s32EventDelete() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
				}
			} else {
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_u32DeleteClientNotificationEvents() => OSAL_s32EventClose() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
			}
		}
	}
}

/*******************************************************************************
*
* This function registers the callback function for a low voltage interrupt
* and enables it. It also checks the initial state of the GPIO level and
* sends a DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED event in case the GPIO
* level indicates an already pending low voltage.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32RegisterLowVoltageCallback(tVoid)
{
	OSAL_trGPIOCallbackData rGPIOCallbackData;
	OSAL_trGPIOData         rGPIOData;

	/* ------------------------------------------------------------------------- */
	/* Configure interrupt edge for GPIO                                         */
	/* ------------------------------------------------------------------------- */

	rGPIOData.tId            = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_LOW_VOLTAGE;
	rGPIOData.unData.u16Edge = OSAL_GPIO_EDGE_BOTH;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER, 
		(intptr_t) &rGPIOData) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	/* ------------------------------------------------------------------------- */
	/* Assign callback function to GPIO interrupt                                */
	/* ------------------------------------------------------------------------- */

	rGPIOCallbackData.rData.tId               = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_LOW_VOLTAGE;
	rGPIOCallbackData.rData.unData.pfCallback = DEV_VOLT_vLowVoltageInterruptCallback;
	rGPIOCallbackData.pvArg                   = OSAL_NULL;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor, 
		OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,
		(intptr_t) &rGPIOCallbackData) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	/* ------------------------------------------------------------------------- */
	/* Enable interrupt                                                          */
	/* ------------------------------------------------------------------------- */

	rGPIOData.unData.bState = TRUE;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, 
		(intptr_t) &rGPIOData) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	/* ------------------------------------------------------------------------- */
	/* Read state of GPIO                                                        */
	/* ------------------------------------------------------------------------- */

	rGPIOData.tId = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_LOW_VOLTAGE;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor, 
		OSAL_C_32_IOCTRL_GPIO_IS_STATE_ACTIVE,
		(intptr_t) &rGPIOData) == OSAL_OK) {
			g_rModuleData.bIsLowVoltageIrqRegistered = TRUE;

			if (TRUE == rGPIOData.unData.bState)
				if (OSAL_s32EventPost(
					g_rModuleData.hEventSystemVoltageThread,
					DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED,
					OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
						DEV_VOLT_vTraceFormatted(
							TR_LEVEL_FATAL,
							"DEV_VOLT_u32RegisterLowVoltageCallback() => OSAL_s32EventPost(LOW_VOLTAGE_CHANGED) failed with error code = %s",
							OSAL_coszErrorText(OSAL_u32ErrorCode()));
	} else {
		rGPIOCallbackData.rData.unData.pfCallback = OSAL_NULL;
		rGPIOCallbackData.pvArg                   = OSAL_NULL;

		if (OSAL_s32IOControl(
			g_rModuleData.rGpioIODescriptor,
			OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,
			(intptr_t) &rGPIOCallbackData) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_u32RegisterLowVoltageCallback() => OSAL_s32IOControl(OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK) failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
	
		return OSAL_u32ErrorCode();
	}

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This function registers the callback function for a critical low voltage
* interrupt and enables it. It also checks the initial state of the GPIO level
* and sends a DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED event in
* case the GPIO level indicates an already pending critical low voltage.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32RegisterCriticalLowVoltageCallback(tVoid)
{
	OSAL_trGPIOCallbackData rGPIOCallbackData;
	OSAL_trGPIOData         rGPIOData;

	/* ------------------------------------------------------------------------- */
	/* Configure interrupt edge for GPIO                                         */
	/* ------------------------------------------------------------------------- */

	rGPIOData.tId            = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_CRITICAL_LOW_VOLTAGE;
	rGPIOData.unData.u16Edge = OSAL_GPIO_EDGE_BOTH;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_SET_TRIGGER, 
		(intptr_t) &rGPIOData) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	/* ------------------------------------------------------------------------- */
	/* Assign callback function to GPIO interrupt                                */
	/* ------------------------------------------------------------------------- */

	rGPIOCallbackData.rData.tId               = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_CRITICAL_LOW_VOLTAGE;
	rGPIOCallbackData.rData.unData.pfCallback = DEV_VOLT_vCriticalLowVoltageInterruptCallback;
	rGPIOCallbackData.pvArg                   = OSAL_NULL;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,
		(intptr_t) &rGPIOCallbackData) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	/* ------------------------------------------------------------------------- */
	/* Enable interrupt                                                          */
	/* ------------------------------------------------------------------------- */

	rGPIOData.unData.bState = TRUE;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, 
		(intptr_t) &rGPIOData) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	/* ------------------------------------------------------------------------- */
	/* Read state of GPIO                                                        */
	/* ------------------------------------------------------------------------- */

	rGPIOData.tId = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_CRITICAL_LOW_VOLTAGE;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_IS_STATE_ACTIVE,
		(intptr_t) &rGPIOData) == OSAL_OK) {
			g_rModuleData.bIsCriticalLowVoltageIrqRegistered = TRUE;

			if (TRUE == rGPIOData.unData.bState)
				if (OSAL_s32EventPost(
					g_rModuleData.hEventSystemVoltageThread,
					DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED,
					OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
						DEV_VOLT_vTraceFormatted(
							TR_LEVEL_FATAL,
							"DEV_VOLT_u32RegisterCriticalLowVoltageCallback() => OSAL_s32EventPost(CRITICAL_LOW_VOLTAGE_CHANGED) failed with error code = %s",
							OSAL_coszErrorText(OSAL_u32ErrorCode()));
	} else {
		rGPIOCallbackData.rData.unData.pfCallback = OSAL_NULL;
		rGPIOCallbackData.pvArg                   = OSAL_NULL;

		if (OSAL_s32IOControl(
			g_rModuleData.rGpioIODescriptor,
			OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,
			(intptr_t) &rGPIOCallbackData) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_u32RegisterCriticalLowVoltageCallback() => OSAL_s32IOControl(OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK) failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
	
		return OSAL_u32ErrorCode();
	}

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This function disables and un-registers the callback of a low voltage
* interrupt.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32UnregisterLowVoltageCallback(tVoid)
{
	OSAL_trGPIOCallbackData rGPIOCallbackData;
	OSAL_trGPIOData         rGPIOData;

	if (g_rModuleData.u32DoLowVoltageDetection == 0)
		return OSAL_E_NOERROR;

	rGPIOData.tId           = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_LOW_VOLTAGE;
	rGPIOData.unData.bState = FALSE;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, 
		(intptr_t) &rGPIOData) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	rGPIOCallbackData.rData.tId               = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_LOW_VOLTAGE;
	rGPIOCallbackData.rData.unData.pfCallback = OSAL_NULL;
	rGPIOCallbackData.pvArg                   = OSAL_NULL;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,
		(intptr_t) &rGPIOCallbackData) == OSAL_OK)
			g_rModuleData.bIsLowVoltageIrqRegistered = FALSE;
	else
			return OSAL_u32ErrorCode();

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This function disables and un-registers the callback of a critical low
* voltage interrupt.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32UnregisterCriticalLowVoltageCallback(tVoid)
{
	OSAL_trGPIOCallbackData rGPIOCallbackData;
	OSAL_trGPIOData         rGPIOData;

	if (g_rModuleData.u32DoCriticalLowVoltageDetection == 0)
		return OSAL_E_NOERROR;

	rGPIOData.tId           = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_CRITICAL_LOW_VOLTAGE;
	rGPIOData.unData.bState = FALSE;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_ENABLE_INT, 
		(intptr_t) &rGPIOData) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	rGPIOCallbackData.rData.tId               = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_CRITICAL_LOW_VOLTAGE;
	rGPIOCallbackData.rData.unData.pfCallback = OSAL_NULL;
	rGPIOCallbackData.pvArg                   = OSAL_NULL;

	if (OSAL_s32IOControl(
		g_rModuleData.rGpioIODescriptor,
		OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,
		(intptr_t) &rGPIOCallbackData) == OSAL_OK)
			g_rModuleData.bIsCriticalLowVoltageIrqRegistered = FALSE;
	else
			return OSAL_u32ErrorCode();

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This function represents the low voltage interrupt callback which informs the
* system voltage thread about each low voltage state change via an OSAL event.
*
*******************************************************************************/
static tVoid DEV_VOLT_vLowVoltageInterruptCallback(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	if (g_rModuleData.hEventSystemVoltageThread == OSAL_C_INVALID_HANDLE) {
		NORMAL_M_ASSERT_ALWAYS();
		return;
	}

	if (OSAL_s32EventPost(
		g_rModuleData.hEventSystemVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
			NORMAL_M_ASSERT_ALWAYS();
}

/*******************************************************************************
*
* This function represents the critical low voltage interrupt callback which
* sets the current state of the critical low voltage accordingly and informs
* the system voltage thread aboout the state change via an OSAL event.
*
*******************************************************************************/
static tVoid DEV_VOLT_vCriticalLowVoltageInterruptCallback(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	if (g_rModuleData.hEventSystemVoltageThread == OSAL_C_INVALID_HANDLE) {
		NORMAL_M_ASSERT_ALWAYS();
		return;
	}

	if (OSAL_s32EventPost(
		g_rModuleData.hEventSystemVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
			NORMAL_M_ASSERT_ALWAYS();
}

/*******************************************************************************
*
* This function creates some low and high voltage related timers.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32CreateVoltageTimer(tVoid)
{
	if (OSAL_s32TimerCreate(
		(OSAL_tpfCallback)DEV_VOLT_vLowVoltageTimerCallback,
		NULL,
		&g_rModuleData.hLowVoltageTimer) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32TimerCreate(
		(OSAL_tpfCallback)DEV_VOLT_vCriticalLowVoltageTimerCallback,
		NULL,
		&g_rModuleData.hCriticalLowVoltageTimer) == OSAL_ERROR)
			return OSAL_u32ErrorCode();
		
	if (OSAL_s32TimerCreate(
		(OSAL_tpfCallback)DEV_VOLT_vHighVoltageTimerCallback,
		NULL,
		&g_rModuleData.hHighVoltageTimer) == OSAL_ERROR)
			return OSAL_u32ErrorCode();
		
	if (OSAL_s32TimerCreate(
		(OSAL_tpfCallback)DEV_VOLT_vCriticalHighVoltageTimerCallback,
		NULL,
		&g_rModuleData.hCriticalHighVoltageTimer) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This function deletes the low and high voltage related timers.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32DeleteVoltageTimer(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (g_rModuleData.hLowVoltageTimer != OSAL_C_INVALID_HANDLE)
		if (OSAL_s32TimerDelete(g_rModuleData.hLowVoltageTimer) == OSAL_ERROR)
			u32OsalErrorCode = OSAL_u32ErrorCode();

	if (g_rModuleData.hCriticalLowVoltageTimer != OSAL_C_INVALID_HANDLE)
		if (OSAL_s32TimerDelete(g_rModuleData.hCriticalLowVoltageTimer) == OSAL_ERROR)
			u32OsalErrorCode = OSAL_u32ErrorCode();

	if (g_rModuleData.hHighVoltageTimer != OSAL_C_INVALID_HANDLE)
		if (OSAL_s32TimerDelete(g_rModuleData.hHighVoltageTimer) == OSAL_ERROR)
			u32OsalErrorCode = OSAL_u32ErrorCode();

	if (g_rModuleData.hCriticalHighVoltageTimer != OSAL_C_INVALID_HANDLE)
		if (OSAL_s32TimerDelete(g_rModuleData.hCriticalHighVoltageTimer) == OSAL_ERROR)
			u32OsalErrorCode = OSAL_u32ErrorCode();

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function represents the low voltage timer callback. In case of a expired
* low voltage timer it sets the related flag and sends a timer expired OSAL
* event to the system voltgae thread.
*
*******************************************************************************/
static tVoid DEV_VOLT_vLowVoltageTimerCallback(tVoid* pvArg)
{
	(tVoid) pvArg; // Unused parameter

	if (g_rModuleData.hEventSystemVoltageThread == OSAL_C_INVALID_HANDLE) {
		NORMAL_M_ASSERT_ALWAYS();
		return;
	}

	if (OSAL_s32EventPost(
		g_rModuleData.hEventSystemVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_TIMER_EXPIRED,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
			NORMAL_M_ASSERT_ALWAYS();
}

/*******************************************************************************
*
* This function represents the low critical voltage timer callback. In case of
* a expired critical low voltage timer it sets the related flag and sends a
* timer expired OSAL event to the system voltgae thread.
*
*******************************************************************************/
static tVoid DEV_VOLT_vCriticalLowVoltageTimerCallback(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	if (g_rModuleData.hEventSystemVoltageThread == OSAL_C_INVALID_HANDLE) {
		NORMAL_M_ASSERT_ALWAYS();
		return;
	}

	if (OSAL_s32EventPost(
		g_rModuleData.hEventSystemVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
			NORMAL_M_ASSERT_ALWAYS();
}

/*******************************************************************************
*
* This function represents the high voltage timer callback. In case of
* a expired high voltage timer it sets the related flag and sends a
* timer expired OSAL event to the system voltgae thread.
*
*******************************************************************************/
static tVoid DEV_VOLT_vHighVoltageTimerCallback(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	if (g_rModuleData.hEventSystemVoltageThread == OSAL_C_INVALID_HANDLE) {
		NORMAL_M_ASSERT_ALWAYS();
		return;
	}

	if (OSAL_s32EventPost(
		g_rModuleData.hEventSystemVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
			NORMAL_M_ASSERT_ALWAYS();
}

/*******************************************************************************
*
* This function represents the critical high voltage timer callback. In case of
* a expired critical high voltage timer it sets the related flag and sends a
* timer expired OSAL event to the system voltgae thread.
*
*******************************************************************************/
static tVoid DEV_VOLT_vCriticalHighVoltageTimerCallback(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	if (g_rModuleData.hEventSystemVoltageThread == OSAL_C_INVALID_HANDLE) {
		NORMAL_M_ASSERT_ALWAYS();
		return;
	}

	if (OSAL_s32EventPost(
		g_rModuleData.hEventSystemVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
			NORMAL_M_ASSERT_ALWAYS();
}

/*******************************************************************************
*
* This function represents the user voltage changed callback, which is called in
* case the registered ADC thresholds are crossed. If this function is called a
* user voltage changed OSAL event is send to the user voltage thread.
*
*******************************************************************************/
static tVoid DEV_VOLT_vUserVoltageChangedCallback(tVoid)
{
	DEV_VOLT_vTraceFormatted(TR_LEVEL_USER_3, "DEV_VOLT_vUserVoltageChangedCallback() called");

	g_rModuleData.bBoardVoltageSimulationActive = FALSE; // Disable eventually running board voltage simulation.

	if (OSAL_s32EventPost(
		g_rModuleData.hEventUserVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_THRESHOLD_CHANGED, 
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vUserVoltageChangedCallback() => OSAL_s32EventPost() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
}

/*******************************************************************************
*
* This functions sets the upper and lower limit for the ADC thresholds of the
* user voltage callback as per the voltage level settings of registered user
* voltage clients and in respect to the actual pending board voltage.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32SetUserVoltageThresholds(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	OSAL_trAdcSetThreshold rAdcSetThreshold;

	tU16 u16UpperLimitMv = 0;
	tU16 u16LowerLimitMv = 0;

	rAdcSetThreshold.bEnable = TRUE;

	u16UpperLimitMv = DEV_VOLT_u16ConvertBoardVoltageAdcToMv(g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc, g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess);
	u16LowerLimitMv = DEV_VOLT_u16ConvertBoardVoltageAdcToMv(g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc, g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess);

	u32OsalErrorCode = DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_3,
		"DEV_VOLT_u32SetUserVoltageThresholds() => NewUpperLimitMv = %u, NewUpperLimitAdc = %u, bBoardVoltageSimulationActive = %u",
		u16UpperLimitMv,
		g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc,
		g_rModuleData.bBoardVoltageSimulationActive);
		
	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_3,
		"DEV_VOLT_u32SetUserVoltageThresholds() => NewLowerLimitMv = %u, NewLowerLimitAdc = %u, bBoardVoltageSimulationActive = %u",
		u16LowerLimitMv,
		g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc,
		g_rModuleData.bBoardVoltageSimulationActive);

	if (FALSE == g_rModuleData.bBoardVoltageSimulationActive) {
		if (g_rModuleData.rAdcUBatIODescriptor != OSAL_ERROR) {

			rAdcSetThreshold.u16Threshold = g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc;
			rAdcSetThreshold.enComparison = enAdcThresholdGt;

			if (OSAL_s32IOControl(
				g_rModuleData.rAdcUBatIODescriptor,
				OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,
				(intptr_t)&rAdcSetThreshold) == OSAL_OK) {

				rAdcSetThreshold.u16Threshold = g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc;
				rAdcSetThreshold.enComparison = enAdcThresholdLt;

				if (OSAL_s32IOControl(
					g_rModuleData.rAdcUBatIODescriptor,
					OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD,
					(intptr_t)&rAdcSetThreshold) == OSAL_ERROR)
						u32OsalErrorCode = OSAL_u32ErrorCode();
			} else {
				u32OsalErrorCode = OSAL_u32ErrorCode();
			}
		} else {
			u32OsalErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
		}
	}

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function installs the thread which is passed via the thread skeleton
* parameter. This is either the system-voltage-thread, the user-voltage-thread
* or the high-voltage-thread.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32InstallThread(const trThreadSkeleton * const prThreadSkeleton)
{
	tU32                   u32OsalErrorCode  = OSAL_E_NOERROR;
	OSAL_trThreadAttribute rThreadAttribute;

	if (OSAL_s32SemaphoreCreate(
		prThreadSkeleton->szSemaphoreName,
		prThreadSkeleton->phThreadSemaphore,
		0) == OSAL_ERROR)
			return OSAL_u32ErrorCode();
		
	if (TRUE == prThreadSkeleton->bSelfCreatedEvent)
		if (OSAL_s32EventCreate(
			prThreadSkeleton->szEventName,
			prThreadSkeleton->phThreadEvent) == OSAL_ERROR) {
				u32OsalErrorCode = OSAL_u32ErrorCode();
				goto error_event_create; /*lint !e801, authorized LINT-deactivation #<71> */
		}

	rThreadAttribute.szName       = (tString)prThreadSkeleton->szThreadName;
	rThreadAttribute.s32StackSize = prThreadSkeleton->s32ThreadStackSize;
	rThreadAttribute.u32Priority  = prThreadSkeleton->u32ThreadPriority;
	rThreadAttribute.pfEntry      = (OSAL_tpfThreadEntry)prThreadSkeleton->pfThreadFunction;
	rThreadAttribute.pvArg        = NULL;

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32InstallThread() => Try to spawn thread '%s' ...",
		prThreadSkeleton->szThreadName);

	if (OSAL_ThreadSpawn(&rThreadAttribute) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_thread_spawn; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		*prThreadSkeleton->phThreadSemaphore, 
		DEV_VOLT_C_U32_SEMAPHORE_THREAD_TIMEOUT_MS) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_semaphore_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (*(prThreadSkeleton->pu8ThreadState) != DEV_VOLT_C_U8_THREAD_RUNNING) {

		u32OsalErrorCode = OSAL_E_UNKNOWN;

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32InstallThread() => ... failed to install thread '%s'",
			prThreadSkeleton->szThreadName);

		goto error_thread_state; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32InstallThread() => ... properly installed thread '%s'",
		prThreadSkeleton->szThreadName);

	return OSAL_E_NOERROR;

error_thread_state:
error_semaphore_wait:
error_thread_spawn:

	if (TRUE == prThreadSkeleton->bSelfCreatedEvent) {
		if (OSAL_s32EventClose(*(prThreadSkeleton->phThreadEvent)) == OSAL_OK)
			if (OSAL_s32EventDelete(prThreadSkeleton->szEventName) == OSAL_OK)
				*(prThreadSkeleton->phThreadEvent) = OSAL_C_INVALID_HANDLE;
			else
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_u32InstallThread() => OSAL_s32EventDelete() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32InstallThread() => OSAL_s32EventClose() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

error_event_create:

	if (OSAL_s32SemaphoreClose(*(prThreadSkeleton->phThreadSemaphore)) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(prThreadSkeleton->szSemaphoreName) == OSAL_OK)
			*(prThreadSkeleton->phThreadSemaphore) = OSAL_C_INVALID_HANDLE;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32InstallThread() => OSAL_s32SemaphoreDelete() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32InstallThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function un-installs the thread which is passed via the thread skeleton
* parameter. This is either the system-voltage-thread, the user-voltage-thread
* or the high-voltage-thread.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32UninstallThread(const trThreadSkeleton * const prThreadSkeleton)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (*(prThreadSkeleton->pu8ThreadState) != DEV_VOLT_C_U8_THREAD_RUNNING)
		return OSAL_E_NOERROR;

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32UninstallThread() => Send event STOP_THREAD to leave thread '%s'",
		prThreadSkeleton->szThreadName);

	if (OSAL_s32EventPost(
		*(prThreadSkeleton->phThreadEvent),
		DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD, 
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		*(prThreadSkeleton->phThreadSemaphore),
		DEV_VOLT_C_U32_SEMAPHORE_THREAD_TIMEOUT_MS) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (*(prThreadSkeleton->pu8ThreadState) != DEV_VOLT_C_U8_THREAD_SHUTTING_DOWN) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}
	
	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32UninstallThread() => Thread '%s' acknowledged shutdown request ... wait 1 second ...",
		prThreadSkeleton->szThreadName);

	OSAL_s32ThreadWait(1000); // Give thread 1 second to leave its own thread context.

	*(prThreadSkeleton->pu8ThreadState) = DEV_VOLT_C_U8_THREAD_OFF;

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32UninstallThread() => ... 1 second expired and thread '%s' should be gone",
		prThreadSkeleton->szThreadName);

error_out:

	if (TRUE == prThreadSkeleton->bSelfCreatedEvent) {
		if (OSAL_s32EventClose(*(prThreadSkeleton->phThreadEvent)) == OSAL_OK)
			if (OSAL_s32EventDelete(prThreadSkeleton->szEventName) == OSAL_OK)
				*(prThreadSkeleton->phThreadEvent) = OSAL_C_INVALID_HANDLE;
			else
				u32OsalErrorCode = OSAL_u32ErrorCode();
		else
			u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	if (OSAL_s32SemaphoreClose(*(prThreadSkeleton->phThreadSemaphore)) == OSAL_OK)
		if (OSAL_s32SemaphoreDelete(prThreadSkeleton->szSemaphoreName) == OSAL_OK)
			*(prThreadSkeleton->phThreadSemaphore) = OSAL_C_INVALID_HANDLE;
		else
			u32OsalErrorCode = OSAL_u32ErrorCode();
	else
		u32OsalErrorCode = OSAL_u32ErrorCode();

	return u32OsalErrorCode;
}
/*******************************************************************************
*
* This function represents the thread which handles the detection and
* notification of system voltage state changes.
*
*******************************************************************************/
static tVoid DEV_VOLT_vSystemVoltageThread(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	OSAL_tSemHandle hSemSystemVoltageThread = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask rEventMaskResult        = 0;

	g_rModuleData.u8SystemVoltageThreadState = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_SYSTEM_VOLT_THREAD_NAME,
		&hSemSystemVoltageThread) == OSAL_ERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vSystemVoltageThread() => OSAL_s32SemaphoreOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		
		goto error_semaphore_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.u8SystemVoltageThreadState = DEV_VOLT_C_U8_THREAD_RUNNING;

	if (OSAL_s32SemaphorePost(hSemSystemVoltageThread) == OSAL_ERROR) {
		g_rModuleData.u8SystemVoltageThreadState = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vSystemVoltageThread() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	while (DEV_VOLT_C_U8_THREAD_RUNNING == g_rModuleData.u8SystemVoltageThreadState) {
		if (OSAL_s32EventWait(
			g_rModuleData.hEventSystemVoltageThread,
			DEV_VOLT_C_U32_SYSTEM_VOLT_THREAD_EVENT_MASK_ALL, 
			OSAL_EN_EVENTMASK_OR, 
			OSAL_C_TIMEOUT_FOREVER,
			&rEventMaskResult) == OSAL_OK) {

			if (OSAL_s32EventPost(
				g_rModuleData.hEventSystemVoltageThread, 
				~rEventMaskResult, 
				OSAL_EN_EVENTMASK_AND) == OSAL_ERROR)
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_vSystemVoltageThread() => OSAL_s32EventPost() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));

			DEV_VOLT_vOnSystemVoltageEventReceived(rEventMaskResult, hSemSystemVoltageThread);
		} else {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vSystemVoltageThread() => OSAL_s32EventWait() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}

	if (OSAL_s32SemaphoreClose(hSemSystemVoltageThread) != OSAL_OK)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vSystemVoltageThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_semaphore_open:

	if (DEV_VOLT_C_U8_THREAD_NOT_INSTALLED == g_rModuleData.u8SystemVoltageThreadState)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vSystemVoltageThread() => Immediately left due to setup failure");
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_VOLT_vSystemVoltageThread() => Left after controlled shutdown");
}

/*******************************************************************************
*
* This function evaluates the OSAL events which are received within the context
* of the thread function DEV_VOLT_vSystemVoltageThread().
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnSystemVoltageEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemSystemVoltageThread)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD) {
		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD);

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_VOLT_vOnSystemVoltageEventReceived() => Event STOP_THREAD received, thread is shutting down ...");

		g_rModuleData.u8SystemVoltageThreadState = DEV_VOLT_C_U8_THREAD_SHUTTING_DOWN;

		if (OSAL_s32SemaphorePost(hSemSystemVoltageThread) == OSAL_ERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vOnSystemVoltageEventReceived() => OSAL_s32SemaphorePost() for thread shutdown failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_TRACE_CALLBACK_COMMAND_RECEIVED) {
		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_TRACE_CALLBACK_COMMAND_RECEIVED);
		DEV_VOLT_vEvaluateTraceCallbackCommand();
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_INJECT_TTFIS_COMMAND) {
		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_INJECT_TTFIS_COMMAND);

		u32OsalErrorCode = DEV_VOLT_u32ProcessInjectedTTFISCommand();
		
		if (u32OsalErrorCode != OSAL_E_NOERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32ProcessInjectedTTFISCommand() => failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));
	}

	if (rEventMaskResult & (DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED                |
				DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED       |
				DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED               |
				DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED      |
				DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_TIMER_EXPIRED          |
				DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED |
				DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED         |
				DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED ))
		DEV_VOLT_vHandleSystemVoltageEvents(&rEventMaskResult);

	if (rEventMaskResult)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vOnSystemVoltageEventReceived() => OSAL_s32EventWait() received unknown event = 0x%08X",
			rEventMaskResult);
}

/*******************************************************************************
*
* This function evaluates the system voltage related OSAL events which indicate
* voltage state changes and voltage timer expirations.
*
*******************************************************************************/
static tVoid DEV_VOLT_vHandleSystemVoltageEvents(OSAL_tEventMask* prEventMaskResult)
{
	tU32 u32AccumulatedVoltageFlags = 0x00000000;

	// Copy currently pending high and low voltage states from
	// global bit-mask vectors to local bit-mask vector.
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {
		u32AccumulatedVoltageFlags |= (g_rModuleData.rProtectedModuleData.u32LowVoltageFlags  |
					       g_rModuleData.rProtectedModuleData.u32HighVoltageFlags  );

		DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
	}

	// Check and process voltage changed and timer expired
	// events and store the results to local bit-mask vector.
	if (*prEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED) {
		*prEventMaskResult &= ~DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED;
		DEV_VOLT_vOnLowVoltageChanged(&u32AccumulatedVoltageFlags);
	}

	if (*prEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED) {
		*prEventMaskResult &= ~DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED;
		DEV_VOLT_vOnCriticalLowVoltageChanged(&u32AccumulatedVoltageFlags);
	}

	if (*prEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED) {
		*prEventMaskResult &= ~DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED;
		DEV_VOLT_vOnHighVoltageChanged(&u32AccumulatedVoltageFlags);
	}

	if (*prEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED) {
		*prEventMaskResult &= ~DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED;
		DEV_VOLT_vOnCriticalHighVoltageChanged(&u32AccumulatedVoltageFlags);
	}

	if (*prEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_TIMER_EXPIRED) {
		*prEventMaskResult &= ~DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_TIMER_EXPIRED;
		DEV_VOLT_vOnLowVoltageTimerExpired(&u32AccumulatedVoltageFlags);
	}

	if (*prEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED) {
		*prEventMaskResult &= ~DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED;
		DEV_VOLT_vOnCriticalLowVoltageTimerExpired(&u32AccumulatedVoltageFlags);
	}

	if (*prEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED) {
		*prEventMaskResult &= ~DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED;
		DEV_VOLT_vOnHighVoltageTimerExpired(&u32AccumulatedVoltageFlags);
	}

	if (*prEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED) {
		*prEventMaskResult &= ~DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED;
		DEV_VOLT_vOnCriticalHighVoltageTimerExpired(&u32AccumulatedVoltageFlags);
	}

	// Finally check all trigger flags (state-changed or timer-expired) for being set
	// and evaluate the local bit-mask vector which contains all accumulated flags of
	// 'state-changed', 'timer-expired' and 'current-state'.
	if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_ALL_TRIGGER)
		DEV_VOLT_vEvaluateVoltageFlags(u32AccumulatedVoltageFlags);
}

/*******************************************************************************
*
* This function is called in case of a low voltage state change. It checks the
* currently pending state of the low voltage and sets the respective changed
* and state bits in the passed bit-mask vector.
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnLowVoltageChanged(tPU32 pu32AccumulatedVoltageFlags)
{
	OSAL_trGPIOData rGPIOData;

	rGPIOData.tId = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_LOW_VOLTAGE;

	if (g_rModuleData.u32VoltageSimulationFlags & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED) {
		rGPIOData.unData.bState = FALSE;
		if (g_rModuleData.u32VoltageSimulationFlags & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE)
			rGPIOData.unData.bState = TRUE;
		g_rModuleData.u32VoltageSimulationFlags &= ~DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED;
	} else {
		if (OSAL_s32IOControl(
			g_rModuleData.rGpioIODescriptor,
			OSAL_C_32_IOCTRL_GPIO_IS_STATE_ACTIVE,
			(intptr_t) &rGPIOData) == OSAL_ERROR) {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vOnLowVoltageChanged() => OSAL_s32IOControl(GPIO_IS_STATE_ACTIVE) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
			return;
		}
	}

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {
		// Set changed bit in passed bit-mask vector.
		*pu32AccumulatedVoltageFlags |=  DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED;

		// Set currently pending low voltage state in passed as well as global bit-mask vector.
		if (TRUE == rGPIOData.unData.bState) {
			g_rModuleData.rProtectedModuleData.u32LowVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE;
			*pu32AccumulatedVoltageFlags                          |= DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE;
		} else {
			g_rModuleData.rProtectedModuleData.u32LowVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE;
			*pu32AccumulatedVoltageFlags                          &= ~DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE;
		}

		DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
	}
}

/*******************************************************************************
*
* This function is called in case of a critical low voltage state change. It
* checks the currently pending state of the critical low voltage and sets the
* respective changed and state bits in the passed bit-mask vector.
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnCriticalLowVoltageChanged(tPU32 pu32AccumulatedVoltageFlags)
{
	OSAL_trGPIOData rGPIOData;

	rGPIOData.tId = (OSAL_tGPIODevID) DEV_VOLT_CONF_C_EN_GPIO_CRITICAL_LOW_VOLTAGE;

	if (g_rModuleData.u32VoltageSimulationFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED) {
		rGPIOData.unData.bState = FALSE;
		if (g_rModuleData.u32VoltageSimulationFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE)
			rGPIOData.unData.bState = TRUE;
		g_rModuleData.u32VoltageSimulationFlags &= ~DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED;
	} else {
		if (OSAL_s32IOControl(
			g_rModuleData.rGpioIODescriptor,
			OSAL_C_32_IOCTRL_GPIO_IS_STATE_ACTIVE,
			(intptr_t) &rGPIOData) == OSAL_ERROR) {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vOnCriticalLowVoltageChanged() => OSAL_s32IOControl(GPIO_IS_STATE_ACTIVE) failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
			return;
		}
	}

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {
		// Set changed bit in passed bit-mask vector.
		*pu32AccumulatedVoltageFlags |=  DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED;

		// Set currently pending critical low voltage state in passed as well as global bit-mask vector.
		if (TRUE == rGPIOData.unData.bState) {
			g_rModuleData.rProtectedModuleData.u32LowVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE;
			*pu32AccumulatedVoltageFlags                          |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE;
		} else {
			g_rModuleData.rProtectedModuleData.u32LowVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE;
			*pu32AccumulatedVoltageFlags                          &= ~DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE;
		}

		DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
	}
}

/*******************************************************************************
*
* This function is called in case of a high voltage state change. It checks the
* currently pending state of the high voltage and sets the respective changed
* and state bits in the passed bit-mask vector.
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnHighVoltageChanged(tPU32 pu32AccumulatedVoltageFlags)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {

		if (g_rModuleData.u32VoltageSimulationFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED) {

			if (g_rModuleData.u32VoltageSimulationFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE)
				g_rModuleData.rProtectedModuleData.u32HighVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE;
			else
				g_rModuleData.rProtectedModuleData.u32HighVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE;

			g_rModuleData.u32VoltageSimulationFlags &= ~DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED;
		}

		// Set changed bit in passed bit-mask vector.
		*pu32AccumulatedVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED;

		// Set currently pending high voltage state in passed bit-mask vector.
		if (g_rModuleData.rProtectedModuleData.u32HighVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE)
			*pu32AccumulatedVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE;
		else
			*pu32AccumulatedVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE;

		// Clear high voltage changed bit from the global bit-mask vector.
		g_rModuleData.rProtectedModuleData.u32HighVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED;

		DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
	}
}

/*******************************************************************************
*
* This function is called in case of a critical high voltage state change. It 
* checks the currently pending state of the high voltage and sets the respective
* changed and state bits in the passed bit-mask vector.
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnCriticalHighVoltageChanged(tPU32 pu32AccumulatedVoltageFlags)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {

		if (g_rModuleData.u32VoltageSimulationFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED) {

			if (g_rModuleData.u32VoltageSimulationFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE)
				g_rModuleData.rProtectedModuleData.u32HighVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE;
			else
				g_rModuleData.rProtectedModuleData.u32HighVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE;

			g_rModuleData.u32VoltageSimulationFlags &= ~DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED;
		}

		// Set changed bit in passed bit-mask vector.
		*pu32AccumulatedVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED;

		// Set currently pending critical high voltage state in passed bit-mask vector.
		if (g_rModuleData.rProtectedModuleData.u32HighVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE)
			*pu32AccumulatedVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE;
		else
			*pu32AccumulatedVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE;

		// Clear critical high voltage changed bit from the global bit-mask vector.
		g_rModuleData.rProtectedModuleData.u32HighVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED;

		DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
	}
}

/*******************************************************************************
*
* This function is called in case of an expired low voltage timer and sets the
* respective expired bit in the passed bit-mask vector.
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnLowVoltageTimerExpired(tPU32 pu32AccumulatedVoltageFlags)
{
	// Set low voltage timer expired flag in local bit-mask vector.
	*pu32AccumulatedVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_TIMER_EXPIRED;
}

/*******************************************************************************
*
* This function is called in case of an expired critical low voltage timer and
* sets the respective expired bit in the passed bit-mask vector.
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnCriticalLowVoltageTimerExpired(tPU32 pu32AccumulatedVoltageFlags)
{
	// Set critical low voltage timer expired flag in local bit-mask vector.
	*pu32AccumulatedVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED;
}

/*******************************************************************************
*
* This function is called in case of an expired high voltage timer and sets the
* respective expired bit in the passed bit-mask vector.
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnHighVoltageTimerExpired(tPU32 pu32AccumulatedVoltageFlags)
{
	// Set high voltage timer expired flag in local bit-mask vector.
	*pu32AccumulatedVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED;
}

/*******************************************************************************
*
* This function is called in case of an expired critical high voltage timer and
* sets the respective expired bit in the passed bit-mask vector.
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnCriticalHighVoltageTimerExpired(tPU32 pu32AccumulatedVoltageFlags)
{
	// Set critical high voltage timer expired flag in local bit-mask vector.
	*pu32AccumulatedVoltageFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED;
}

/*******************************************************************************
*
* This function reacts on changes of the passed voltage flags and steps
* through the states of the state-machine according to the to be entered
* new voltage state and the possible state transitions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vEvaluateVoltageFlags(tU32 u32AccumulatedVoltageFlags)
{
	tBool bStayInCurrentVoltageState = TRUE;

	DEV_VOLT_vTraceVoltageFlags(u32AccumulatedVoltageFlags);

	DEV_VOLT_vSetStateMachineAttributesByTriggerFlags(u32AccumulatedVoltageFlags);

	do {
		DEV_VOLT_vStateMachineDoTransition();

		if (FALSE == (bStayInCurrentVoltageState = DEV_VOLT_bCheckStayInCurrentVoltageStateByStateFlags(u32AccumulatedVoltageFlags)))
			DEV_VOLT_vSetStateMachineAttributesByStateFlags(u32AccumulatedVoltageFlags);
	
	} while (FALSE == bStayInCurrentVoltageState);
}

/*******************************************************************************
*
* This function performs the state-transistions of the state-machine until the
* current voltage state matches the requested voltage state and notifies each
* registered client about each state change.
*
*******************************************************************************/
static tVoid DEV_VOLT_vStateMachineDoTransition(tVoid)
{
	tenVoltageState enPreviousVoltageState;

	do {
		enPreviousVoltageState = g_rModuleData.rStateMachineContext.enCurrentVoltageState;

		g_rModuleData.rStateMachineContext.papfenStateMachineDoTransition[g_rModuleData.rStateMachineContext.enCurrentVoltageState](); /*lint !e746, authorized LINT-deactivation #<74> */

		if (enPreviousVoltageState != g_rModuleData.rStateMachineContext.enCurrentVoltageState)
			DEV_VOLT_vTraceVoltageState(enPreviousVoltageState);

		DEV_VOLT_vUpdateSystemVoltageHistoryAndNotify(enPreviousVoltageState);

	} while (enPreviousVoltageState != g_rModuleData.rStateMachineContext.enCurrentVoltageState);
}

/*******************************************************************************
*
* This function sets the new requested voltage state according to the passed
* voltage TRIGGER flags. The trigger flags are of type 'state changed' or
* 'timer expired'.
*
*******************************************************************************/
static tVoid DEV_VOLT_vSetStateMachineAttributesByTriggerFlags(tU32 u32AccumulatedVoltageFlags)
{
	// The order of voltage masks check is significant. Don't change it without knowning what you do.

	if ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED)                &&
	    (g_rModuleData.rStateMachineContext.enCurrentVoltageState > DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW)  ) {

		g_rModuleData.rStateMachineContext.enRequestedVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW;

	} else if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_TIMER_EXPIRED) {

		g_rModuleData.rStateMachineContext.bIsCriticalLowVoltageTimerExpired = TRUE;

	} else if ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED)                &&
		  (g_rModuleData.rStateMachineContext.enCurrentVoltageState > DEV_VOLT_EN_VOLTAGE_STATE_LOW)   ) {

		g_rModuleData.rStateMachineContext.enRequestedVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_LOW;

	} else if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_TIMER_EXPIRED) {

		g_rModuleData.rStateMachineContext.bIsLowVoltageTimerExpired = TRUE;

	} else if ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED)                 &&
		   (g_rModuleData.rStateMachineContext.enCurrentVoltageState >= DEV_VOLT_EN_VOLTAGE_STATE_OPERATING)    &&
		   (g_rModuleData.rStateMachineContext.enCurrentVoltageState <  DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH)  ) {

		g_rModuleData.rStateMachineContext.enRequestedVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH;

	} else if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_TIMER_EXPIRED) {

		g_rModuleData.rStateMachineContext.bIsCriticalHighVoltageTimerExpired = TRUE;

	} else if ((u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED)                      &&
		   (g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_OPERATING)  ) {

		g_rModuleData.rStateMachineContext.enRequestedVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_HIGH;

	} else if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_TIMER_EXPIRED) {

		g_rModuleData.rStateMachineContext.bIsHighVoltageTimerExpired = TRUE;
	}
}

/*******************************************************************************
*
* This function sets the new requested voltage state according to the passed
* voltage STATE flags.
*
*******************************************************************************/
static tVoid DEV_VOLT_vSetStateMachineAttributesByStateFlags(tU32 u32AccumulatedVoltageFlags)
{
	// The order of voltage masks check is significant. Don't change it without knowning what you do.

	if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE) {

		g_rModuleData.rStateMachineContext.enRequestedVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW;

	} else if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE) {

		g_rModuleData.rStateMachineContext.enRequestedVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_LOW;

	} else if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE) {

		g_rModuleData.rStateMachineContext.enRequestedVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH;

	} else if (u32AccumulatedVoltageFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE) {

		g_rModuleData.rStateMachineContext.enRequestedVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_HIGH;

	} else {

		g_rModuleData.rStateMachineContext.enRequestedVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;
	}
}

/*******************************************************************************
*
* This function determines if the state-machine should keep staying in its
* current voltage state, depending on state of the passed voltage flags.
*
*******************************************************************************/
static tBool DEV_VOLT_bCheckStayInCurrentVoltageStateByStateFlags(tU32 u32AccumulatedVoltageFlags)
{
	// Stay in VOLTAGE_STATE_CRITICAL_LOW if critical low voltage state is still set.
	if ((g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW)            &&
	    (u32AccumulatedVoltageFlags                                & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE)  )
		return TRUE;

	// Stay in VOLTAGE_STATE_LOW if low voltage state is still set.
	if ((g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_LOW)            &&
	    (u32AccumulatedVoltageFlags                                & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE)  )
		return TRUE;

	// Stay in VOLTAGE_STATE_CRITICAL_HIGH if critical high voltage state is still set.
	if ((g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH)            &&
	    (u32AccumulatedVoltageFlags                                & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE)  )
		return TRUE;

	// Stay in VOLTAGE_STATE_HIGH if high voltage state is still set.
	if ((g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_HIGH)            &&
	    (u32AccumulatedVoltageFlags                                & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE)  )
		return TRUE;

	// Stay in VOLTAGE_STATE_OPERATING if none the above voltage states is NOT set anymore.
	if ((g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_OPERATING) &&
	    !(u32AccumulatedVoltageFlags                               & DEV_VOLT_C_U32_BIT_MASK_ALL_STATES)    )
		return TRUE;

	// We stay in VOLTAGE_STATE_LOW_OVER_DELAY or VOLTAGE_STATE_CRITICAL_LOW_OVER_DELAY without checking any
	// of the voltage state flags because these states we only leave due to a timer expired trigger flag.
	if ((g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_LOW_OVER_DELAY)          ||
	    (g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW_OVER_DELAY)   )
		return TRUE;

	return FALSE;
}

/*******************************************************************************
*
* This function updates the client individual occurance counters for low-voltage,
* critical-low-voltage, high-voltage and critical-high voltage of each registered
* client as per the passed state-change and informs the client via an OSAL event
* about the changes.
*
*******************************************************************************/
static tVoid DEV_VOLT_vUpdateSystemVoltageHistoryAndNotify(tenVoltageState enPreviousVoltageState)
{
	tU32 u32Index;

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if ((g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId != DEV_VOLT_C_U32_CLIENT_ID_INVALID) &&
		    (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32SystemVoltageIndicationMask != 0)               ) {

			OSAL_tEventMask rEventMask = 0;

			if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32SystemVoltageIndicationMask & DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE) {
				if ((enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_OPERATING) &&
				    (g_rModuleData.rStateMachineContext.enCurrentVoltageState  == DEV_VOLT_EN_VOLTAGE_STATE_LOW)         ) {
					g_rModuleData.prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32LowVoltageCounter++;
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY; 
				}

				if (((enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_LOW)           || 
				    (enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_LOW_OVER_DELAY)  ) &&
				    (g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_OPERATING)             )
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY; 
			}

			if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32SystemVoltageIndicationMask & DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE) {
				if ((enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_LOW)         &&
				    (g_rModuleData.rStateMachineContext.enCurrentVoltageState  == DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW)  ) {
					g_rModuleData.prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32CriticalLowVoltageCounter++;
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY; 
				}

				if (((enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW)           || 
				     (enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW_OVER_DELAY)  ) &&
				     (g_rModuleData.rStateMachineContext.enCurrentVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_LOW)                            )
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY; 
			}

			if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32SystemVoltageIndicationMask & DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE) {
				if ((enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_OPERATING) &&
				    (g_rModuleData.rStateMachineContext.enCurrentVoltageState  == DEV_VOLT_EN_VOLTAGE_STATE_HIGH)        ) {
					g_rModuleData.prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32HighVoltageCounter++;
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY; 
				}

				if ((enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_HIGH)     &&
				    (g_rModuleData.rStateMachineContext.enCurrentVoltageState  == DEV_VOLT_EN_VOLTAGE_STATE_OPERATING)  )
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY; 

				if (TRUE == g_rModuleData.rStateMachineContext.bIsHighVoltageTimerExpired)
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_HIGH_VOLTAGE; 
			}

			if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32SystemVoltageIndicationMask & DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE) {
				if ((enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_HIGH)         &&
				    (g_rModuleData.rStateMachineContext.enCurrentVoltageState  == DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH)  ) {
					g_rModuleData.prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32CriticalHighVoltageCounter++;
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY; 
				}

				if ((enPreviousVoltageState == DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH) &&
				    (g_rModuleData.rStateMachineContext.enCurrentVoltageState  == DEV_VOLT_EN_VOLTAGE_STATE_HIGH)            )
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY; 

				if (TRUE == g_rModuleData.rStateMachineContext.bIsCriticalHighVoltageTimerExpired)
					rEventMask = DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_CRITICAL_HIGH_VOLTAGE; 
			}

			if (rEventMask != 0) {
				if (OSAL_s32EventPost(
					g_rModuleData.ahClientEventHandle[u32Index],
					rEventMask,
					OSAL_EN_EVENTMASK_OR) == OSAL_OK)
						DEV_VOLT_vTraceNotificatonEvent(
							g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId,
							g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
							rEventMask);
				else
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_vUpdateSystemVoltageHistoryAndNotify() => OSAL_s32EventPost() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
			}
		}
	}

	// After all clients have been notified, the high voltage and critical high voltage expired flags must be reset to FALSE.
	g_rModuleData.rStateMachineContext.bIsHighVoltageTimerExpired         = FALSE;
	g_rModuleData.rStateMachineContext.bIsCriticalHighVoltageTimerExpired = FALSE;

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function checks if the conditions are fulfilled to perform a transition
* from the critical-low-voltage state to another voltage state and triggers
* related state change actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vStateMachineDoTransitionFromCriticalLow(tVoid)
{
	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState > DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW) {
		if (g_rModuleData.u32CriticalLowVoltageOverDelayMs > 0) {
			g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW_OVER_DELAY;

			if (OSAL_s32TimerSetTime(
				g_rModuleData.hCriticalLowVoltageTimer,
				g_rModuleData.u32CriticalLowVoltageOverDelayMs,
				0) == OSAL_ERROR)
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_bStateMachineCheckTransitionFromCriticalLow() => OSAL_s32TimerSetTime() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
		} else {
			g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_LOW;

			DEV_VOLT_vDoCriticalLowVoltageOverAction();
		}
	}
}

/*******************************************************************************
*
* This function checks if the conditions are fulfilled to perform a transition
* from the critical-low-voltage-over-delay state to another voltage state and
* triggers related state change actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vStateMachineDoTransitionFromCriticalLowOverDelay(tVoid)
{
	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState < DEV_VOLT_EN_VOLTAGE_STATE_LOW) {
		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW;

		if (OSAL_s32TimerSetTime(
			g_rModuleData.hCriticalLowVoltageTimer,
			0,
			0) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_bStateMachineCheckTransitionFromCriticalLowOverDelay() => OSAL_s32TimerSetTime() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

		g_rModuleData.rStateMachineContext.bIsCriticalLowVoltageTimerExpired = FALSE;
	}

	if (TRUE == g_rModuleData.rStateMachineContext.bIsCriticalLowVoltageTimerExpired) {
		g_rModuleData.rStateMachineContext.bIsCriticalLowVoltageTimerExpired = FALSE;

		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_LOW;

		DEV_VOLT_vDoCriticalLowVoltageOverAction();
	}
}

/*******************************************************************************
*
* This function checks if the conditions are fulfilled to perform a transition
* from the low-voltage state to another voltage state and triggers related state
* change actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vStateMachineDoTransitionFromLow(tVoid)
{
	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState < DEV_VOLT_EN_VOLTAGE_STATE_LOW) {

		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW;

		DEV_VOLT_vDoCriticalLowVoltageAction();
	}

	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState > DEV_VOLT_EN_VOLTAGE_STATE_LOW) {
		if (g_rModuleData.u32LowVoltageOverDelayMs > 0) {
			g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_LOW_OVER_DELAY;

			if (OSAL_s32TimerSetTime(
				g_rModuleData.hLowVoltageTimer,
				g_rModuleData.u32LowVoltageOverDelayMs,
				0) == OSAL_ERROR)
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_bStateMachineCheckTransitionFromLow() => OSAL_s32TimerSetTime() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
		} else {
			g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;

			DEV_VOLT_vDoLowVoltageOverAction();
		}
	}
}

/*******************************************************************************
*
* This function checks if the conditions are fulfilled to perform a transition
* from the low-voltage-over-delay state to another voltage state and triggers
* related state change actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vStateMachineDoTransitionFromLowOverDelay(tVoid)
{
	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState < DEV_VOLT_EN_VOLTAGE_STATE_OPERATING) {
		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_LOW;

		if (OSAL_s32TimerSetTime(
			g_rModuleData.hLowVoltageTimer,
			0,
			0) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_bStateMachineCheckTransitionFromLowOverDelay() => OSAL_s32TimerSetTime() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

		g_rModuleData.rStateMachineContext.bIsLowVoltageTimerExpired = FALSE;
	}

	if (TRUE == g_rModuleData.rStateMachineContext.bIsLowVoltageTimerExpired) {
		g_rModuleData.rStateMachineContext.bIsLowVoltageTimerExpired = FALSE;

		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;

		DEV_VOLT_vDoLowVoltageOverAction();
	}
}

/*******************************************************************************
*
* This function checks if the conditions are fulfilled to perform a transition
* from the operating-voltage state to another voltage state and triggers
* related state change actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vStateMachineDoTransitionFromOperating(tVoid)
{
	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState < DEV_VOLT_EN_VOLTAGE_STATE_OPERATING) {

		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_LOW;

		DEV_VOLT_vDoLowVoltageAction();
	}

	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState > DEV_VOLT_EN_VOLTAGE_STATE_OPERATING) {

		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_HIGH;

		if (g_rModuleData.u32HighVoltageTimerMs > 0)
			if (OSAL_s32TimerSetTime(
				g_rModuleData.hHighVoltageTimer,
				g_rModuleData.u32HighVoltageTimerMs,
				0) == OSAL_ERROR)
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_bStateMachineCheckTransitionFromOperating() => OSAL_s32TimerSetTime() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));

		DEV_VOLT_vDoHighVoltageAction();
	}
}

/*******************************************************************************
*
* This function checks if the conditions are fulfilled to perform a transition
* from the high-voltage state to another voltage state and triggers related
* state change actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vStateMachineDoTransitionFromHigh(tVoid)
{
	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState < DEV_VOLT_EN_VOLTAGE_STATE_HIGH) {

		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;

		if (OSAL_s32TimerSetTime(
			g_rModuleData.hHighVoltageTimer,
			0,
			0) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_bStateMachineCheckTransitionFromHigh() => OSAL_s32TimerSetTime() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

		DEV_VOLT_vDoHighVoltageOverAction();
	}

	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState > DEV_VOLT_EN_VOLTAGE_STATE_HIGH) {
		
		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH;

		if (g_rModuleData.u32CriticalHighVoltageTimerMs > 0)
			if (OSAL_s32TimerSetTime(
				g_rModuleData.hCriticalHighVoltageTimer,
				g_rModuleData.u32CriticalHighVoltageTimerMs,
				0) == OSAL_ERROR)
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_bStateMachineCheckTransitionFromHigh() => OSAL_s32TimerSetTime() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));

		DEV_VOLT_vDoCriticalHighVoltageAction();
	}
}

/*******************************************************************************
*
* This function checks if the conditions are fulfilled to perform a transition
* from the critical-high-voltage state to another voltage state and triggers
* related state change actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vStateMachineDoTransitionFromCriticalHigh(tVoid)
{
	if (g_rModuleData.rStateMachineContext.enRequestedVoltageState < DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH) {

		g_rModuleData.rStateMachineContext.enCurrentVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_HIGH;

		if (OSAL_s32TimerSetTime(
			g_rModuleData.hCriticalHighVoltageTimer,
			0,
			0) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_bStateMachineCheckTransitionFromCriticalHigh() => OSAL_s32TimerSetTime() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

		DEV_VOLT_vDoCriticalHighVoltageOverAction();
	}
}

/*******************************************************************************
*
* This function sets the extern visible system volage state to
* DEV_VOLT_EN_VOLTAGE_STATE_LOW and performs state related on-enter actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDoLowVoltageAction(tVoid)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	g_rModuleData.prGlobalData->u32PreviousSystemVoltageState = g_rModuleData.prGlobalData->u32SystemVoltageState;
	g_rModuleData.prGlobalData->u32SystemVoltageState         = DEV_VOLT_EN_VOLTAGE_STATE_LOW;

	// No on-enter actions for state low-voltage.

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function sets the extern visible system volage state to
* DEV_VOLT_EN_VOLTAGE_STATE_OPERATING and performs state related on-exit actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDoLowVoltageOverAction(tVoid)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	g_rModuleData.prGlobalData->u32PreviousSystemVoltageState = g_rModuleData.prGlobalData->u32SystemVoltageState;
	g_rModuleData.prGlobalData->u32SystemVoltageState         = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;

	// No on-exit actions for state low-voltage.

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function sets the extern visible system volage state to
* DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW and performs state related on-enter
* actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDoCriticalLowVoltageAction(tVoid)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	g_rModuleData.prGlobalData->u32PreviousSystemVoltageState = g_rModuleData.prGlobalData->u32SystemVoltageState;
	g_rModuleData.prGlobalData->u32SystemVoltageState         = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW;

	// No on-enter actions for state critical-low-voltage.

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function sets the extern visible system volage state to
* DEV_VOLT_EN_VOLTAGE_STATE_LOW and performs state related on-exit actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDoCriticalLowVoltageOverAction(tVoid)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	g_rModuleData.prGlobalData->u32PreviousSystemVoltageState = g_rModuleData.prGlobalData->u32SystemVoltageState;
	g_rModuleData.prGlobalData->u32SystemVoltageState         = DEV_VOLT_EN_VOLTAGE_STATE_LOW;

	// No on-exit actions for state critical-low-voltage.

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function sets the extern visible system volage state to
* DEV_VOLT_EN_VOLTAGE_STATE_HIGH and performs state related on-enter
* actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDoHighVoltageAction(tVoid)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	g_rModuleData.prGlobalData->u32PreviousSystemVoltageState = g_rModuleData.prGlobalData->u32SystemVoltageState;
	g_rModuleData.prGlobalData->u32SystemVoltageState         = DEV_VOLT_EN_VOLTAGE_STATE_HIGH;

	// No on-enter actions for state high-voltage.

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function sets the extern visible system volage state to
* DEV_VOLT_EN_VOLTAGE_STATE_OPERATING and performs state related on-exit actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDoHighVoltageOverAction(tVoid)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	g_rModuleData.prGlobalData->u32PreviousSystemVoltageState = g_rModuleData.prGlobalData->u32SystemVoltageState;
	g_rModuleData.prGlobalData->u32SystemVoltageState         = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;

	// No on-exit actions for state high-voltage.

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function sets the extern visible system volage state to
* DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH and performs state related on-exit
* actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDoCriticalHighVoltageAction(tVoid)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	g_rModuleData.prGlobalData->u32PreviousSystemVoltageState = g_rModuleData.prGlobalData->u32SystemVoltageState;
	g_rModuleData.prGlobalData->u32SystemVoltageState         = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH;

	// No on-enter actions for state critical-high-voltage.

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function sets the extern visible system volage state to
* DEV_VOLT_EN_VOLTAGE_STATE_HIGH and performs state related on-exit actions.
*
*******************************************************************************/
static tVoid DEV_VOLT_vDoCriticalHighVoltageOverAction(tVoid)
{
	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	g_rModuleData.prGlobalData->u32PreviousSystemVoltageState = g_rModuleData.prGlobalData->u32SystemVoltageState;
	g_rModuleData.prGlobalData->u32SystemVoltageState         = DEV_VOLT_EN_VOLTAGE_STATE_HIGH;

	// No on-exit actions for state critical-high-voltage.

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
}

/*******************************************************************************
*
* This function simulates exact one voltage state change by setting the respective
* voltage flags and sending a voltage changed OSAL event to the system voltage
* thread.
*
*******************************************************************************/
static tBool DEV_VOLT_bSimulateVoltageChange(tenVoltageState enSytemVoltage, tU8 u8VoltageState)
{
	OSAL_tEventMask u32EventMask = 0x00000000;

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return FALSE;

	g_rModuleData.u32VoltageSimulationFlags = 0;

	switch (enSytemVoltage) {
	case DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_LOW:
		u32EventMask = DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED;

		g_rModuleData.u32VoltageSimulationFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED;

		if (0x01 == u8VoltageState)
			g_rModuleData.u32VoltageSimulationFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE;
		break;
	case DEV_VOLT_EN_VOLTAGE_STATE_LOW:
		u32EventMask = DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED;

		g_rModuleData.u32VoltageSimulationFlags |= DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED;

		if (0x01 == u8VoltageState)
			g_rModuleData.u32VoltageSimulationFlags |=  DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE;
		break;
	case DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH:
		u32EventMask = DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED;

		g_rModuleData.u32VoltageSimulationFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED;

		if (0x01 == u8VoltageState)
			g_rModuleData.u32VoltageSimulationFlags |= DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE;
		break;
	case DEV_VOLT_EN_VOLTAGE_STATE_HIGH:
		u32EventMask = DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED;

		g_rModuleData.u32VoltageSimulationFlags |= DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED;

		if (0x01 == u8VoltageState)
			g_rModuleData.u32VoltageSimulationFlags |= DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE;
		break;
	default: // Intentionally do nothing.
		break;
	}

	if (OSAL_s32EventPost(
		g_rModuleData.hEventSystemVoltageThread,
		u32EventMask,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_bSimulateVoltageChange() => OSAL_s32EventPost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		return FALSE;
	}

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);

	return TRUE;
}

/*******************************************************************************
*
* This function simulates one or multiple voltage state changes at the same time
* by setting the respective voltage flags and sending a voltage changed OSAL
* event to the system voltage thread.
*
*******************************************************************************/
static tBool DEV_VOLT_bSimulateCombinedVoltageChanges(tU32 u32VoltageFlags)
{
	OSAL_tEventMask u32EventMask = 0x00000000;

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return FALSE;

	g_rModuleData.u32VoltageSimulationFlags = (u32VoltageFlags & DEV_VOLT_C_U32_BIT_MASK_ALL_VOLTAGE_BITS);

	if (u32VoltageFlags & DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED)
		u32EventMask |= DEV_VOLT_C_U32_EVENT_MASK_LOW_VOLTAGE_CHANGED;

	if (u32VoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED)
		u32EventMask |= DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED;

	if (u32VoltageFlags & DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED)
		u32EventMask |= DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED;

	if (u32VoltageFlags & DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED)
		u32EventMask |= DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED;

	if (OSAL_s32EventPost(
		g_rModuleData.hEventSystemVoltageThread,
		u32EventMask,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_bSimulateCombindedVoltageChanges() => OSAL_s32EventPost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		return FALSE;
	}

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);

	return TRUE;
}

/*******************************************************************************
*
* This function simulates a change of the board voltage by setting it to a
* discrete value and sending a voltage changed OSAL event to the user voltage
* thread.
*
*******************************************************************************/
static tBool DEV_VOLT_bSimulateBoardVoltage(tU16 u16BoardVoltageAdc, tU16 u16BoardVoltageMv)
{
	if ((u16BoardVoltageAdc == 0) && (u16BoardVoltageMv == 0)) {
		g_rModuleData.bBoardVoltageSimulationActive = FALSE;
	} else if ((u16BoardVoltageAdc != 0) && (u16BoardVoltageMv != 0)) {
		return FALSE; // Only one value is allowed to be > 0 at the same time.
	} else {
		if (u16BoardVoltageMv == 0)
			u16BoardVoltageMv = DEV_VOLT_u16ConvertBoardVoltageAdcToMv(u16BoardVoltageAdc, g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess);

		if (u16BoardVoltageAdc == 0)
			u16BoardVoltageAdc = DEV_VOLT_u16ConvertBoardVoltageMvToAdc(u16BoardVoltageMv, g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess);

		if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {

			g_rModuleData.bBoardVoltageSimulationActive = TRUE;

			g_rModuleData.prGlobalData->u16BoardVoltageAdc = u16BoardVoltageAdc;
			g_rModuleData.prGlobalData->u16BoardVoltageMv  = u16BoardVoltageMv;

			DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);

			DEV_VOLT_vTraceBoardVoltage();

			if (OSAL_s32EventPost(
				g_rModuleData.hEventUserVoltageThread,
				DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_THRESHOLD_CHANGED, 
				OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
					return FALSE;
		} else {
			return FALSE;
		}
	}

	return TRUE;
}

/*******************************************************************************
*
* This functions registers a client to be notified about changes of the
* system voltage.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32RegisterSystemVoltage(DEV_VOLT_trSystemVoltageRegistration* prSystemVoltageRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;
	tU32  u32Index;

	if (prSystemVoltageRegistration->u32ClientId == DEV_VOLT_C_U32_CLIENT_ID_INVALID)
		return OSAL_E_INVALIDVALUE;

	if (prSystemVoltageRegistration->u32VoltageIndicationMask > (DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
								     DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
								     DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
								     DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  ))
		return OSAL_E_INVALIDVALUE;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = OSAL_E_INVALIDVALUE;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == prSystemVoltageRegistration->u32ClientId) {

			prGlobalData->arClientSpecificData[u32Index].u32SystemVoltageIndicationMask = prSystemVoltageRegistration->u32VoltageIndicationMask;

			prSystemVoltageRegistration->u32CurrentSystemVoltageState = prGlobalData->u32SystemVoltageState;

			prSystemVoltageRegistration->u8PermHighVoltageCounter = prGlobalData->u8PermHighVoltageCounter;

			prSystemVoltageRegistration->u8PermCriticalHighVoltageCounter = prGlobalData->u8PermCriticalHighVoltageCounter;

			u32OsalErrorCode = OSAL_E_NOERROR;
			break;
		}
	}

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions un-registers a client to be notified about changes of the
* system voltage.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32UnregisterSystemVoltage(tU32 u32ClientId, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;
	tU32  u32Index;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = OSAL_E_INVALIDVALUE;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == u32ClientId) {

			prGlobalData->arClientSpecificData[u32Index].u32SystemVoltageIndicationMask = 0;

			u32OsalErrorCode = OSAL_E_NOERROR;
			break;
		}
	}

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions registers a client to be notified about changes of an
* individual user voltage.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32RegisterUserVoltage(DEV_VOLT_trUserVoltageRegistration* prUserVoltageRegistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32              u32OsalErrorCode          = OSAL_E_NOERROR;
	tBool             bClientFound              = FALSE;
	tBool             bNewUserVoltageEntryAdded = FALSE;
	tBool             bUserVoltageAlreadyExists = FALSE;
	tBool             bThresholdLimitsChanged   = FALSE;
	tU16              u16UserVoltageLevelAdc    = 0;
	tU16              u16HysteresisAdc          = 0;

	tU32  u32Index;
	tU32  u32Index2;

	if (prUserVoltageRegistration->u32ClientId == DEV_VOLT_C_U32_CLIENT_ID_INVALID)
		return OSAL_E_INVALIDVALUE;
	
	if (prUserVoltageRegistration->u16UserVoltageLevelMv == 0)
		return OSAL_E_INVALIDVALUE;

	if ((prUserVoltageRegistration->u8LevelCrossDirection != DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD)  &&
	    (prUserVoltageRegistration->u8LevelCrossDirection != DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD)  )
		return OSAL_E_INVALIDVALUE;

	if ((prUserVoltageRegistration->u8LevelCrossDirection == DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD)  &&
	    (prUserVoltageRegistration->u16HysteresisMv >= prUserVoltageRegistration->u16UserVoltageLevelMv)           )
		return OSAL_E_INVALIDVALUE;

	u32OsalErrorCode = DEV_VOLT_u32GetBoardVoltageFromOtherContext(prGlobalData, hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u16UserVoltageLevelAdc = DEV_VOLT_u16ConvertBoardVoltageMvToAdc(prUserVoltageRegistration->u16UserVoltageLevelMv, prGlobalData, hSemDataAccess);
	u16HysteresisAdc       = (tU16)(prUserVoltageRegistration->u16HysteresisMv / DEV_VOLT_u16GetBoardVoltageMvPerDigit(prGlobalData, hSemDataAccess));

	if (u16UserVoltageLevelAdc > DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_ADC_RESOLUTION)
		return OSAL_E_INVALIDVALUE;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	// Lookup client
	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == prUserVoltageRegistration->u32ClientId) {

			bClientFound = TRUE;

			// Lookup free entry for new user voltage
			for (u32Index2 = 0;
			     u32Index2 < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_USER_VOLTAGES;
			     u32Index2++) {

				if (prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelMv == prUserVoltageRegistration->u16UserVoltageLevelMv) {
					bUserVoltageAlreadyExists = TRUE;
					break;
				}

				if (prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelMv == 0) {

					bNewUserVoltageEntryAdded = TRUE;

					prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelMv       = prUserVoltageRegistration->u16UserVoltageLevelMv;
					prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelAdc      = u16UserVoltageLevelAdc;
					prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u8CrossDirection = prUserVoltageRegistration->u8LevelCrossDirection;

					// Only override hysteresis if value is higher than the minimum of DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV
					if (prUserVoltageRegistration->u16HysteresisMv > DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV) {
						prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16HysteresisMv  = prUserVoltageRegistration->u16HysteresisMv;
						prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16HysteresisAdc = u16HysteresisAdc;
					}
					else
					{
						u16HysteresisAdc = prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16HysteresisAdc;
					}

					prGlobalData->arClientSpecificData[u32Index].u8NumOfUserVoltageLevelsSet++;

					// Verify new user-level and eventually adapt next-higher and/or next-lower levels.
					if (prUserVoltageRegistration->u8LevelCrossDirection == DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD) {
						if (u16UserVoltageLevelAdc > prGlobalData->u16BoardVoltageAdc) {
							if (u16UserVoltageLevelAdc < prGlobalData->u16NextHigherUserVoltageLevelAdc) {
								prGlobalData->u16NextHigherUserVoltageLevelAdc = u16UserVoltageLevelAdc;
								bThresholdLimitsChanged = TRUE;
							}
						} else {
							if (u16UserVoltageLevelAdc > prGlobalData->u16NextLowerUserVoltageLevelAdc) {
								prGlobalData->u16NextLowerUserVoltageLevelAdc = (tU16)(u16UserVoltageLevelAdc - u16HysteresisAdc);
								bThresholdLimitsChanged = TRUE;
							}
						}

						// Check if board voltage hit into hysteresis range of this user voltage.
						if ((u16UserVoltageLevelAdc - u16HysteresisAdc) < prGlobalData->u16BoardVoltageAdc) {
							if ((u16UserVoltageLevelAdc - u16HysteresisAdc) > prGlobalData->u16NextLowerUserVoltageLevelAdc)
								prGlobalData->u16NextLowerUserVoltageLevelAdc = (tU16)(u16UserVoltageLevelAdc - u16HysteresisAdc);
						}
					} else {
						if (u16UserVoltageLevelAdc < prGlobalData->u16BoardVoltageAdc) {
							if (u16UserVoltageLevelAdc > prGlobalData->u16NextLowerUserVoltageLevelAdc) {
								prGlobalData->u16NextLowerUserVoltageLevelAdc = u16UserVoltageLevelAdc;
								bThresholdLimitsChanged = TRUE;
							}
						} else {
							if (u16UserVoltageLevelAdc < prGlobalData->u16NextHigherUserVoltageLevelAdc) {
								prGlobalData->u16NextHigherUserVoltageLevelAdc = (tU16)(u16UserVoltageLevelAdc + u16HysteresisAdc);
								bThresholdLimitsChanged = TRUE;
							}
						}

						// Check if board voltage hit into hysteresis range of this user voltage.
						if ((u16UserVoltageLevelAdc + u16HysteresisAdc) > prGlobalData->u16BoardVoltageAdc) {
							if ((u16UserVoltageLevelAdc + u16HysteresisAdc) < prGlobalData->u16NextHigherUserVoltageLevelAdc)
								prGlobalData->u16NextHigherUserVoltageLevelAdc = (tU16)(u16UserVoltageLevelAdc + u16HysteresisAdc);
						}
					}
					break;
				}
			}
			break;
		}
	}

	if (FALSE == bClientFound)
		u32OsalErrorCode = OSAL_E_INVALIDVALUE;
	else if (TRUE == bUserVoltageAlreadyExists)
		u32OsalErrorCode = OSAL_E_ALREADYEXISTS;
	else if (FALSE == bNewUserVoltageEntryAdded)
		u32OsalErrorCode = OSAL_E_MAXFILES;

	if (u32OsalErrorCode != OSAL_E_NOERROR) {
		DEV_VOLT_vDataUnlock(hSemDataAccess);
		return u32OsalErrorCode;
	}
	
	// Set level state for just registered user voltage level in contrast to the actually pending board voltage.
	if (prUserVoltageRegistration->u8LevelCrossDirection == DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD) {
		if (prGlobalData->u16BoardVoltageAdc >= u16UserVoltageLevelAdc)
			prUserVoltageRegistration->u8UserVoltageState = DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED;
		else // don't consider hysteresis for immediate update
			prUserVoltageRegistration->u8UserVoltageState = DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_DECEEDED;
	} else {
		if (prGlobalData->u16BoardVoltageAdc <= u16UserVoltageLevelAdc)
			prUserVoltageRegistration->u8UserVoltageState = DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_DECEEDED;
		else // don't consider hysteresis for immediate update
			prUserVoltageRegistration->u8UserVoltageState = DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED;
	}

	DEV_VOLT_vTraceRegisterUserVoltage(prUserVoltageRegistration, u16UserVoltageLevelAdc, u16HysteresisAdc, prGlobalData);

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	// After setting of new voltage limits, request the new tresholds to be set at the ADC.
	if (TRUE == bThresholdLimitsChanged)
		u32OsalErrorCode = DEV_VOLT_u32SetUserVoltageThresholdFromOtherContext(prGlobalData, hSemDataAccess);
		
	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This functions un-registers a client to be notified about changes of an
* individual user voltage.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32UnregisterUserVoltage(DEV_VOLT_trUserVoltageDeregistration* prUserVoltageDeregistration, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;
	tU32  u32Index;
	tU32  u32Index2;

	tU16 u16HysteresisAdc = (tU16)(DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV / DEV_VOLT_u16GetBoardVoltageMvPerDigit(prGlobalData, hSemDataAccess));

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = OSAL_E_INVALIDVALUE;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == prUserVoltageDeregistration->u32ClientId) {

			for (u32Index2 = 0;
			     u32Index2 < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_USER_VOLTAGES;
			     u32Index2++) {

				if (prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelMv == prUserVoltageDeregistration->u16UserVoltageLevelMv) {
					prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelMv       = 0;
					prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelAdc      = 0;
					prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16HysteresisMv  = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV;
					prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16HysteresisAdc = u16HysteresisAdc;
					prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u8CrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_INVALID;

					prGlobalData->arClientSpecificData[u32Index].u8NumOfUserVoltageLevelsSet--;

					u32OsalErrorCode = OSAL_E_NOERROR;
					break;
				}
			}
			break;
		}
	}

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = DEV_VOLT_u32RecalcUserVoltageLimits(prGlobalData, hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	// After recalculation of the voltage limits, request the new tresholds to be set at the ADC.
	u32OsalErrorCode = DEV_VOLT_u32SetUserVoltageThresholdFromOtherContext(prGlobalData, hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function returns the client individual history of system voltage changes
* as well as the current system voltage state.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32GetSystemVoltageHistory(DEV_VOLT_trSystemVoltageHistory* prSystemVoltageHistory, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;
	tU32 u32Index;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = OSAL_E_INVALIDVALUE;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == prSystemVoltageHistory->u32ClientId) {

			prSystemVoltageHistory->rSystemVoltage.u32CurrentSystemVoltageState  = prGlobalData->u32SystemVoltageState;
			prSystemVoltageHistory->rSystemVoltage.u32CriticalHighVoltageCounter = prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32CriticalHighVoltageCounter;
			prSystemVoltageHistory->rSystemVoltage.u32CriticalLowVoltageCounter  = prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32CriticalLowVoltageCounter;
			prSystemVoltageHistory->rSystemVoltage.u32HighVoltageCounter         = prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32HighVoltageCounter;
			prSystemVoltageHistory->rSystemVoltage.u32LowVoltageCounter          = prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32LowVoltageCounter;

			// Clear voltage counters after having the client informed about it.
			prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32CriticalHighVoltageCounter = 0;
			prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32CriticalLowVoltageCounter  = 0;
			prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32HighVoltageCounter         = 0;
			prGlobalData->arClientSpecificData[u32Index].rSystemVoltage.u32LowVoltageCounter          = 0;

			u32OsalErrorCode = OSAL_E_NOERROR;
			break;
		}
	}

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function represents the thread which handles the detection and
* notification of user voltage state changes.
*
*******************************************************************************/
static tVoid DEV_VOLT_vUserVoltageThread(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	OSAL_tSemHandle  hSemUserVoltageThread = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask  rEventMaskResult                   = 0;

	g_rModuleData.u8UserVoltageThreadState = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_USER_VOLT_THREAD_NAME,
		&hSemUserVoltageThread) == OSAL_ERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => OSAL_s32SemaphoreOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		
		goto error_semaphore_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.u8UserVoltageThreadState = DEV_VOLT_C_U8_THREAD_RUNNING;

	if (OSAL_s32SemaphorePost(hSemUserVoltageThread) == OSAL_ERROR) {
		g_rModuleData.u8UserVoltageThreadState = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vUserVoltageThread() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

	while (DEV_VOLT_C_U8_THREAD_RUNNING == g_rModuleData.u8UserVoltageThreadState) {
		
		if (OSAL_s32EventWait(
			g_rModuleData.hEventUserVoltageThread,
			DEV_VOLT_C_U32_USER_VOLT_THREAD_EVENT_MASK_ALL,
			OSAL_EN_EVENTMASK_OR,
			OSAL_C_TIMEOUT_FOREVER,
			&rEventMaskResult) == OSAL_OK) {
			
			if (OSAL_s32EventPost(
				g_rModuleData.hEventUserVoltageThread,
				~rEventMaskResult, 
				OSAL_EN_EVENTMASK_AND) == OSAL_ERROR)
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_vUserVoltageThread() => OSAL_s32EventPost() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));

			DEV_VOLT_vOnUserVoltageEventReceived(rEventMaskResult, hSemUserVoltageThread);
		} else {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vUserVoltageThread() => OSAL_s32EventWait() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}

	if (OSAL_s32SemaphoreClose(hSemUserVoltageThread) != OSAL_OK)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vUserVoltageThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_semaphore_open:

	if (DEV_VOLT_C_U8_THREAD_NOT_INSTALLED == g_rModuleData.u8UserVoltageThreadState)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vUserVoltageThread() => Immediately left due to setup failure");
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_VOLT_vUserVoltageThread() => Left after controlled shutdown");
}

/*******************************************************************************
*
* This function evaluates the OSAL events which are received within the context
* of the thread function DEV_VOLT_vUserVoltageThread().
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnUserVoltageEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemUserVoltageThread)
{
	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD) {

		DEV_VOLT_vTraceFormatted(TR_LEVEL_USER_4, "DEV_VOLT_vUserVoltageThread() => Event STOP_THREAD received, thread is shutting down ...");

		g_rModuleData.u8UserVoltageThreadState = DEV_VOLT_C_U8_THREAD_SHUTTING_DOWN;

		if (OSAL_s32SemaphorePost(hSemUserVoltageThread) == OSAL_ERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vUserVoltageThread() => OSAL_s32SemaphorePost() for thread shutdown failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD);
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_GET_BOARD_VOLTAGE) {

		tU32 u32GetBoardVoltageResult = OSAL_E_NOERROR;

		u32GetBoardVoltageResult = DEV_VOLT_u32GetBoardVoltage();

		if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {
			g_rModuleData.prGlobalData->u32GetBoardVoltageResult = u32GetBoardVoltageResult;
			DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);

			if (OSAL_s32SemaphorePost(g_rModuleData.hSemGetBoardVoltage) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_vOnUserVoltageEventReceived(GET_BOARD_VOLTAGE) => OSAL_s32SemaphorePost() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_GET_BOARD_VOLTAGE);
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SET_USER_VOLTAGE_THRESHOLD) {
	
		tU32 u32SetUserVoltageThresholdsResult = OSAL_E_NOERROR;

		u32SetUserVoltageThresholdsResult = DEV_VOLT_u32SetUserVoltageThresholds();

		if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {
			g_rModuleData.prGlobalData->u32SetUserVoltageThresholdsResult = u32SetUserVoltageThresholdsResult;
			DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);

			if (OSAL_s32SemaphorePost(g_rModuleData.hSemSetUserVoltageThreshold) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_vOnUserVoltageEventReceived(SET_USER_VOLTAGE_THRESHOLD) => OSAL_s32SemaphorePost() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_SET_USER_VOLTAGE_THRESHOLD);
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_THRESHOLD_CHANGED) {
		if (DEV_VOLT_u32GetBoardVoltage() == OSAL_E_NOERROR) {

			tU8 u8UserVoltageChangedState = DEV_VOLT_C_U8_USER_VOLTAGE_NOT_CHANGED;

			if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_USER_3,
					"DEV_VOLT_vOnUserVoltageEventReceived(USER_VOLTAGE_THRESHOLD_CHANGED) => CurrentBoardVoltageAdc = %u , CurrentUpperLimitAdc = %u , CurrentLowerLimitAdc = %u",
					g_rModuleData.prGlobalData->u16BoardVoltageAdc,
					g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc,
					g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc);

				DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
			}

			u8UserVoltageChangedState = DEV_VOLT_u8CheckUserVoltageChangedState();

			if (u8UserVoltageChangedState != DEV_VOLT_C_U8_USER_VOLTAGE_NOT_CHANGED) {

				DEV_VOLT_vSendUserVoltageChangedNotification(u8UserVoltageChangedState);

				if (DEV_VOLT_u32RecalcUserVoltageLimits(
					g_rModuleData.prGlobalData,
					g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR) {
						DEV_VOLT_vTraceFormatted(
							TR_LEVEL_FATAL,
							"DEV_VOLT_vOnUserVoltageEventReceived() => DEV_VOLT_u32RecalcUserVoltageLimits() failed with error code = %s",
							OSAL_coszErrorText(OSAL_u32ErrorCode()));
				}

				if (DEV_VOLT_u32SetUserVoltageThresholds() != OSAL_E_NOERROR)
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_vOnUserVoltageEventReceived(USER_VOLTAGE_THRESHOLD_CHANGED) => DEV_VOLT_u32SetUserVoltageThresholds() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
			}
		} else {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vOnUserVoltageEventReceived() => DEV_VOLT_u32GetBoardVoltage() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_THRESHOLD_CHANGED);
	}

	if (rEventMaskResult)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vUserVoltageThread() => OSAL_s32EventWait() received unknown event = 0x%08X",
			rEventMaskResult);
}

/*******************************************************************************
*
* This function checks if the just reported crossing of a configured ADC
* threshold really matches in respect to the currently pending board voltage.
*
*******************************************************************************/
static tU8 DEV_VOLT_u8CheckUserVoltageChangedState(tVoid)
{
	tU8 u8UserVoltageChangedState = DEV_VOLT_C_U8_USER_VOLTAGE_NOT_CHANGED;

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) == OSAL_E_NOERROR) {

		if (g_rModuleData.prGlobalData->u16BoardVoltageAdc >= g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc)
			u8UserVoltageChangedState = DEV_VOLT_C_U8_USER_VOLTAGE_HIGHER_LEVEL_EXCEEDED;

		if (g_rModuleData.prGlobalData->u16BoardVoltageAdc <= g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc)
			u8UserVoltageChangedState = DEV_VOLT_C_U8_USER_VOLTAGE_LOWER_LEVEL_DECEEDED;

		DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
	}

	return u8UserVoltageChangedState;
}

/*******************************************************************************
*
* This function notifies each registerd client about the crossing of a user
* voltage level.
*
*******************************************************************************/
static tVoid DEV_VOLT_vSendUserVoltageChangedNotification(tU8 u8UserVoltageChangedState)
{
	tBool bNotificationClientFound          = FALSE;
	tBool bUserVoltageLevelCrossed          = FALSE;
	tU16  u16LowestDeceededVoltageLevelAdc  = 0xFFFF;
	tU16  u16HighestExceededVoltageLevelAdc = 0x0000;
	tU16  u16LowestDeceededVoltageLevelMv   = 0xFFFF;
	tU16  u16HighestExceededVoltageLevelMv  = 0x0000;
	tU8   u8UserVoltageState                = DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_INVALID;

	tU32 u32Index;
	tU32 u32Index2;

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId != 0) {
			if (g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u8NumOfUserVoltageLevelsSet > 0) {

				bUserVoltageLevelCrossed = FALSE;

				u16LowestDeceededVoltageLevelAdc  = 0xFFFF;
				u16HighestExceededVoltageLevelAdc = 0x0000;

				u16LowestDeceededVoltageLevelMv   = 0xFFFF;
				u16HighestExceededVoltageLevelMv  = 0x0000;

				for (u32Index2 = 0;
				     u32Index2 < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_USER_VOLTAGES;
				     u32Index2++) {

					tU16 u16LevelMv       = g_rModuleData.prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelMv;
					tU16 u16LevelAdc      = g_rModuleData.prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelAdc;
					tU16 u16HysteresisAdc = g_rModuleData.prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16HysteresisAdc;
					tU8  u8CrossDirection = g_rModuleData.prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u8CrossDirection;

					if (u16LevelMv != 0) {

						g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u16LatestCrossedUserVoltageLevelMv = 0;

						if (DEV_VOLT_C_U8_USER_VOLTAGE_LOWER_LEVEL_DECEEDED == u8UserVoltageChangedState) {

							u8UserVoltageState = DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_DECEEDED;

							if (u8CrossDirection == DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD) {
								if (((u16LevelAdc - u16HysteresisAdc) <= g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc) &&
								    ((u16LevelAdc - u16HysteresisAdc) >= g_rModuleData.prGlobalData->u16BoardVoltageAdc)                ) {
									bNotificationClientFound = TRUE;
									bUserVoltageLevelCrossed = TRUE;

									if (u16LevelAdc < u16LowestDeceededVoltageLevelAdc) {
										u16LowestDeceededVoltageLevelAdc = u16LevelAdc;
										u16LowestDeceededVoltageLevelMv  = u16LevelMv;
									}
								}
							} else {
								if ((u16LevelAdc <= g_rModuleData.prGlobalData->u16NextLowerUserVoltageLevelAdc) &&
								    (u16LevelAdc >= g_rModuleData.prGlobalData->u16BoardVoltageAdc)                ) {
									bNotificationClientFound = TRUE;
									bUserVoltageLevelCrossed = TRUE;

									if (u16LevelAdc < u16LowestDeceededVoltageLevelAdc) {
										u16LowestDeceededVoltageLevelAdc = u16LevelAdc;
										u16LowestDeceededVoltageLevelMv  = u16LevelMv;
									}
								}
							}
						}

						if (DEV_VOLT_C_U8_USER_VOLTAGE_HIGHER_LEVEL_EXCEEDED == u8UserVoltageChangedState) {

							u8UserVoltageState = DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED;

							if (u8CrossDirection == DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD) {
								if ((u16LevelAdc >= g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc) &&
								    (u16LevelAdc <= g_rModuleData.prGlobalData->u16BoardVoltageAdc)                 ) {
									bNotificationClientFound = TRUE;
									bUserVoltageLevelCrossed = TRUE;

									if (u16LevelAdc > u16HighestExceededVoltageLevelAdc) {
										u16HighestExceededVoltageLevelAdc = u16LevelAdc;
										u16HighestExceededVoltageLevelMv  = u16LevelMv;
									}
								}
							} else {
								if (((u16LevelAdc + u16HysteresisAdc) >= g_rModuleData.prGlobalData->u16NextHigherUserVoltageLevelAdc) &&
								    ((u16LevelAdc + u16HysteresisAdc) <= g_rModuleData.prGlobalData->u16BoardVoltageAdc)                 ) {
									bNotificationClientFound = TRUE;
									bUserVoltageLevelCrossed = TRUE;

									if (u16LevelAdc > u16HighestExceededVoltageLevelAdc) {
										u16HighestExceededVoltageLevelAdc = u16LevelAdc;
										u16HighestExceededVoltageLevelMv  = u16LevelMv;
									}
								}
							}
						}
					}
				}
				
				if (TRUE == bUserVoltageLevelCrossed) {
					if (u16LowestDeceededVoltageLevelMv != 0xFFFF) {
						g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u16LatestCrossedUserVoltageLevelMv = u16LowestDeceededVoltageLevelMv;
						g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u8LatestUserVoltageState           = u8UserVoltageState;
					}

					if (u16HighestExceededVoltageLevelMv != 0x0000) {
						g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u16LatestCrossedUserVoltageLevelMv = u16HighestExceededVoltageLevelMv;
						g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u8LatestUserVoltageState           = u8UserVoltageState;
					}

					if (OSAL_s32EventPost(
						g_rModuleData.ahClientEventHandle[u32Index],
						DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY,
						OSAL_EN_EVENTMASK_OR) == OSAL_OK)
							DEV_VOLT_vTraceNotificatonEvent(
								g_rModuleData.prGlobalData->arClientSpecificData[u32Index].u32ClientId,
								g_rModuleData.prGlobalData->arClientSpecificData[u32Index].szNotificationEventName,
								DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY);
					else
						DEV_VOLT_vTraceFormatted(
							TR_LEVEL_FATAL,
							"DEV_VOLT_vSendUserVoltageChangedNotification() => OSAL_s32EventPost() failed with error code = %s",
							OSAL_coszErrorText(OSAL_u32ErrorCode()));
				}
			}
		}
	}

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (FALSE == bNotificationClientFound)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vSendUserVoltageChangedNotification() => No client found for voltage changed notification although it should");
}

/*******************************************************************************
*
* This function re-calculates the new upper and lower limit for the new to be
* configured ADC threshold in respect to the current board voltage and
* observed user voltage levels of all registerd clients.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32RecalcUserVoltageLimits(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	tU32 u32Index;
	tU32 u32Index2;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	prGlobalData->u16NextHigherUserVoltageLevelAdc = 0xFFFF;
	prGlobalData->u16NextLowerUserVoltageLevelAdc  = 0x0000; 

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId != 0) {
			if (prGlobalData->arClientSpecificData[u32Index].u8NumOfUserVoltageLevelsSet > 0) {
				for (u32Index2 = 0;
				     u32Index2 < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_USER_VOLTAGES;
				     u32Index2++) {

					if (prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelAdc != 0) {
					
						tU16 u16LevelAdc      = prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16LevelAdc;
						tU16 u16HysteresisAdc = prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u16HysteresisAdc;
						tU8  u8CrossDirection = prGlobalData->arClientSpecificData[u32Index].arUserVoltage[u32Index2].u8CrossDirection;
					
						if (u8CrossDirection == DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD) {
							if (u16LevelAdc > prGlobalData->u16BoardVoltageAdc) {
								if (u16LevelAdc < prGlobalData->u16NextHigherUserVoltageLevelAdc)
									prGlobalData->u16NextHigherUserVoltageLevelAdc = u16LevelAdc;
							} else {
								if ((u16LevelAdc - u16HysteresisAdc) > prGlobalData->u16NextLowerUserVoltageLevelAdc)
									prGlobalData->u16NextLowerUserVoltageLevelAdc = (tU16)(u16LevelAdc - u16HysteresisAdc);
							}

							// Check if board voltage hit into hysteresis range of this user voltage.
							if ((u16LevelAdc - u16HysteresisAdc) < prGlobalData->u16BoardVoltageAdc) {
								if ((u16LevelAdc - u16HysteresisAdc) > prGlobalData->u16NextLowerUserVoltageLevelAdc)
									prGlobalData->u16NextLowerUserVoltageLevelAdc = (tU16)(u16LevelAdc - u16HysteresisAdc);
							}
						} else {
							if (u16LevelAdc < prGlobalData->u16BoardVoltageAdc) {
								if (u16LevelAdc > prGlobalData->u16NextLowerUserVoltageLevelAdc)
									prGlobalData->u16NextLowerUserVoltageLevelAdc = u16LevelAdc;
							} else {
								if ((u16LevelAdc + u16HysteresisAdc) < prGlobalData->u16NextHigherUserVoltageLevelAdc)
									prGlobalData->u16NextHigherUserVoltageLevelAdc = (tU16)(u16LevelAdc + u16HysteresisAdc);
							}

							// Check if board voltage hit into hysteresis range of this user voltage.
							if ((u16LevelAdc + u16HysteresisAdc) > prGlobalData->u16BoardVoltageAdc) {
								if ((u16LevelAdc + u16HysteresisAdc) < prGlobalData->u16NextHigherUserVoltageLevelAdc)
									prGlobalData->u16NextHigherUserVoltageLevelAdc = (tU16)(u16LevelAdc + u16HysteresisAdc);
							}
						}
					}
				}
			}
		}
	}
	
	DEV_VOLT_vDataUnlock(hSemDataAccess);

	return OSAL_E_NOERROR;
}

/*******************************************************************************
*
* This function converts the board voltage from ADC digits to millivolts.
*
*******************************************************************************/
static tU16 DEV_VOLT_u16ConvertBoardVoltageAdcToMv(tU16 u16BoardVoltageAdc, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU16 u16BoardVoltageMv = 0;

	if (DEV_VOLT_u32DataLock(hSemDataAccess) == OSAL_E_NOERROR) {

		u16BoardVoltageMv =
		(
		  (tU16)(
			  (
			    (
			      (
			       (tF32)(u16BoardVoltageAdc) / DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_ADC_RESOLUTION
			      )
			      * DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_REFERENCE_MV
			    )
			    * ((tF32)(prGlobalData->u16BoardVoltageGradientMultipliedBy1000) / 1000)
			  )
			  + prGlobalData->u16BoardVoltageOffsetMv
			)
		);

		DEV_VOLT_vDataUnlock(hSemDataAccess);
	}

	return u16BoardVoltageMv;
}

/*******************************************************************************
*
* This function converts the board voltage from millivolts to ADC digits.
*
*******************************************************************************/
static tU16 DEV_VOLT_u16ConvertBoardVoltageMvToAdc(tU16 u16BoardVoltageMv, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU16 u16BoardVoltageAdc = 0;

	if (DEV_VOLT_u32DataLock(hSemDataAccess) == OSAL_E_NOERROR) {

		u16BoardVoltageAdc =
		(
		  (tU16)(
			  (
			    (
			      (
			        (tF32)(u16BoardVoltageMv) - prGlobalData->u16BoardVoltageOffsetMv
			      )
			      / ((tF32)(prGlobalData->u16BoardVoltageGradientMultipliedBy1000) / 1000)
			    )
			    / DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_REFERENCE_MV
			  )
			  * DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_ADC_RESOLUTION
			)
		);

		DEV_VOLT_vDataUnlock(hSemDataAccess);
	}

	return u16BoardVoltageAdc;
}

/*******************************************************************************
*
* This function returns the number of millivolts per ADC digit.
*
*******************************************************************************/
static tU16 DEV_VOLT_u16GetBoardVoltageMvPerDigit(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU16 u16BoardVoltageMvPerDigit = 0;

	if (DEV_VOLT_u32DataLock(hSemDataAccess) == OSAL_E_NOERROR) {

		u16BoardVoltageMvPerDigit =
		(
		  (tU16)(
			  (tF32)(DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_REFERENCE_MV)
			  * ((tF32)(prGlobalData->u16BoardVoltageGradientMultipliedBy1000) / 1000)
			  / DEV_VOLT_CONF_C_U16_BOARD_VOLTAGE_ADC_RESOLUTION
			)
		);

		DEV_VOLT_vDataUnlock(hSemDataAccess);
	}

	return u16BoardVoltageMvPerDigit;
}

/*******************************************************************************
*
* This function reads to currently pending board voltage from the ADC.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32GetBoardVoltage(tVoid)
{
	tU32 u32OsalErrorCode   = OSAL_E_NOERROR;
	tU16 u16BoardVoltageAdc = 0;
	tU16 u16BoardVoltageMv  = 0;

	// Do nothing if board voltage simulation is active. The value is then only 
	// changed by the TTFIS command DEV_VOLT_SIMULATE_BOARD_VOLTAGE
	if (TRUE == g_rModuleData.bBoardVoltageSimulationActive)
		return OSAL_E_NOERROR;

	if (g_rModuleData.rAdcUBatIODescriptor == OSAL_ERROR)
		return OSAL_E_TEMP_NOT_AVAILABLE;

	if (OSAL_s32IORead(
		g_rModuleData.rAdcUBatIODescriptor,
		(tPS8)&u16BoardVoltageAdc,
		sizeof(u16BoardVoltageAdc)) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	u16BoardVoltageMv = DEV_VOLT_u16ConvertBoardVoltageAdcToMv(u16BoardVoltageAdc, g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess);

	u32OsalErrorCode = DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess);

	if (OSAL_E_NOERROR == u32OsalErrorCode) {

		g_rModuleData.prGlobalData->u16BoardVoltageAdc = u16BoardVoltageAdc;
		g_rModuleData.prGlobalData->u16BoardVoltageMv  = u16BoardVoltageMv;

		DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);
	}

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function can be called from another context than the devices creator
* context and reads to currently pending board voltage from the ADC.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32GetBoardVoltageFromOtherContext(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32              u32OsalErrorCode        = OSAL_E_NOERROR;
	OSAL_tEventHandle hEventUserVoltageThread = OSAL_C_INVALID_HANDLE;;
	OSAL_tSemHandle   hSemGetBoardVoltage     = OSAL_C_INVALID_HANDLE;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);
	
	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;
	
	// The value of prGlobalData->u32GetBoardVoltageResult is set to OSAL_E_DOESNOTEXIST
	// during the device initialization if the board voltage ADC channel is not available.
	u32OsalErrorCode = prGlobalData->u32GetBoardVoltageResult;

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;
	
	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_GET_BOARD_VOLTAGE_NAME, 
		&hSemGetBoardVoltage) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32EventOpen(
		DEV_VOLT_C_STRING_USER_VOLT_THREAD_EVENT_NAME,
		&hEventUserVoltageThread) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventPost(
		hEventUserVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_GET_BOARD_VOLTAGE, 
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_event_post; /*lint !e801, authorized LINT-deactivation #<71> */
	}
		
	if (OSAL_s32SemaphoreWait(
		hSemGetBoardVoltage, 
		DEV_VOLT_C_U32_SEM_BOARD_VOLTAGE_TIMEOUT_MS) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_semaphore_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (OSAL_E_NOERROR == u32OsalErrorCode) {
		u32OsalErrorCode = prGlobalData->u32GetBoardVoltageResult;
		prGlobalData->u32GetBoardVoltageResult = OSAL_E_NOERROR;
		DEV_VOLT_vDataUnlock(hSemDataAccess);
	}

error_semaphore_wait:
error_event_post:

	if (OSAL_s32EventClose(hEventUserVoltageThread) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32GetBoardVoltageFromOtherContext() => OSAL_s32EventClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_event_open:

	if (OSAL_s32SemaphoreClose(hSemGetBoardVoltage) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32GetBoardVoltageFromOtherContext() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function can be called from another context than the devices creator
* context and sets the upper and lower limit for the ADC thresholds of the
* user voltage callback as per the voltage level settings of registered user
* voltage clients and in respect to the actual pending board voltage.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32SetUserVoltageThresholdFromOtherContext(trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32              u32OsalErrorCode            = OSAL_E_NOERROR;
	OSAL_tEventHandle hEventUserVoltageThread     = OSAL_C_INVALID_HANDLE;;
	OSAL_tSemHandle   hSemSetUserVoltageThreshold = OSAL_C_INVALID_HANDLE;

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_SET_USER_VOLTAGE_THRESHOLD_NAME, 
		&hSemSetUserVoltageThreshold) == OSAL_ERROR)
			return OSAL_u32ErrorCode();

	if (OSAL_s32EventOpen(
		DEV_VOLT_C_STRING_USER_VOLT_THREAD_EVENT_NAME,
		&hEventUserVoltageThread) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventPost(
		hEventUserVoltageThread,
		DEV_VOLT_C_U32_EVENT_MASK_SET_USER_VOLTAGE_THRESHOLD,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_event_post; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		hSemSetUserVoltageThreshold, 
		DEV_VOLT_C_U32_SEM_SET_USER_VOLTAGE_THRESHOLD_TIMEOUT_MS) == OSAL_ERROR) {
			u32OsalErrorCode = OSAL_u32ErrorCode();
			goto error_semaphore_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (OSAL_E_NOERROR == u32OsalErrorCode) {
		u32OsalErrorCode = prGlobalData->u32SetUserVoltageThresholdsResult;
		DEV_VOLT_vDataUnlock(hSemDataAccess);
	}

error_semaphore_wait:
error_event_post:

	if (OSAL_s32EventClose(hEventUserVoltageThread) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32SetUserVoltageThresholdFromOtherContext() => OSAL_s32EventClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_event_open:

	if (OSAL_s32SemaphoreClose(hSemSetUserVoltageThreshold) == OSAL_ERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32SetUserVoltageThresholdFromOtherContext() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function gets the client individual state of the latest crossed user
* voltage level.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32GetUserVoltageState(DEV_VOLT_trUserVoltage* prUserVoltage, trGlobalData* prGlobalData, OSAL_tSemHandle hSemDataAccess)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	tU32 u32Index;

	u32OsalErrorCode = DEV_VOLT_u32DataLock(hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		return u32OsalErrorCode;

	u32OsalErrorCode = OSAL_E_INVALIDVALUE;

	for (u32Index = 0;
	     u32Index < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_CLIENTS;
	     u32Index++) {

		if (prGlobalData->arClientSpecificData[u32Index].u32ClientId == prUserVoltage->u32ClientId) {
			if (prGlobalData->arClientSpecificData[u32Index].u8NumOfUserVoltageLevelsSet > 0) {
				prUserVoltage->u16LatestCrossedUserVoltageLevelMv = prGlobalData->arClientSpecificData[u32Index].u16LatestCrossedUserVoltageLevelMv;
				prUserVoltage->u8LatestUserVoltageState           = prGlobalData->arClientSpecificData[u32Index].u8LatestUserVoltageState;
				
				u32OsalErrorCode = OSAL_E_NOERROR;
				break;
			} else {
				u32OsalErrorCode = OSAL_E_DOESNOTEXIST;
				break;
			}
		}
	}

	DEV_VOLT_vDataUnlock(hSemDataAccess);

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function represents the thread which handles the high voltage and 
* critical high voltage detection and notifies the system voltage thread via a
* high voltage changed OSAL event in case of changes between operating, high
* and critical high voltage.
*
*******************************************************************************/
static tVoid DEV_VOLT_vHighVoltageThread(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	tU32             u32OsalErrorCode                 = OSAL_E_NOERROR;
	OSAL_tSemHandle  hSemHighVoltageThread            = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask  rEventMaskResult                 = 0;
	tenVoltageState  enLocalVoltageState              = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;

	DEV_VOLT_trClientRegistration        rClientRegistration = { 0 };
	DEV_VOLT_trUserVoltageRegistration   rUserVoltageRegistration = { 0 };
	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration = { 0 };
	DEV_VOLT_trUserVoltage               rUserVoltage = { 0 };

	rUserVoltage.u8LatestUserVoltageState           = DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_INVALID;

	g_rModuleData.u8HighVoltageThreadState = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_HIGH_VOLT_THREAD_NAME,
		&hSemHighVoltageThread) == OSAL_ERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => OSAL_s32SemaphoreOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		
		goto error_semaphore_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	u32OsalErrorCode = DEV_VOLT_u32AddClient(
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => DEV_VOLT_u32AddClient() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));

		goto error_add_client; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rClientRegistration.u32ClientId = DEV_VOLT_C_U32_CLIENT_ID_INVALID;

	u32OsalErrorCode = DEV_VOLT_u32RegisterClient(
				&rClientRegistration,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => DEV_VOLT_u32RegisterClient() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));

		goto error_register_client; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventOpen(
		rClientRegistration.szNotificationEventName,
		&g_rModuleData.hEventHighVoltageThread) == OSAL_ERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => OSAL_s32EventOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD;

	if (g_rModuleData.u16BoardVoltageHighMv > 0) {
		rUserVoltageRegistration.u16UserVoltageLevelMv = g_rModuleData.u16BoardVoltageHighMv;
		rUserVoltageRegistration.u16HysteresisMv       = g_rModuleData.u16BoardVoltageHighHysMv;

		u32OsalErrorCode = DEV_VOLT_u32RegisterUserVoltage(
					&rUserVoltageRegistration,
					g_rModuleData.prGlobalData,
					g_rModuleData.hSemDataAccess);

		if (u32OsalErrorCode != OSAL_E_NOERROR) {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vHighVoltageThread() => DEV_VOLT_u32RegisterUserVoltage(HIGH) failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));
		
			goto error_register_user_voltage_high; /*lint !e801, authorized LINT-deactivation #<71> */
		}

		rUserVoltage.u16LatestCrossedUserVoltageLevelMv = g_rModuleData.u16BoardVoltageHighMv;
		rUserVoltage.u8LatestUserVoltageState           = rUserVoltageRegistration.u8UserVoltageState;
	}

	if (g_rModuleData.u16BoardVoltageCriticalHighMv > 0) {
		if (g_rModuleData.u16BoardVoltageHighMv > 0) {
			rUserVoltageRegistration.u16UserVoltageLevelMv = g_rModuleData.u16BoardVoltageCriticalHighMv;
			rUserVoltageRegistration.u16HysteresisMv       = g_rModuleData.u16BoardVoltageCriticalHighHysMv;

			u32OsalErrorCode = DEV_VOLT_u32RegisterUserVoltage(
						&rUserVoltageRegistration,
						g_rModuleData.prGlobalData,
						g_rModuleData.hSemDataAccess);

			if (u32OsalErrorCode != OSAL_E_NOERROR) {
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_vHighVoltageThread() => DEV_VOLT_u32RegisterUserVoltage(CRITICAL-HIGH) failed with error code = %s",
					OSAL_coszErrorText(u32OsalErrorCode));
	
				goto error_register_user_voltage_critical_high; /*lint !e801, authorized LINT-deactivation #<71> */
			}

			if (DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED == rUserVoltageRegistration.u8UserVoltageState) {
				rUserVoltage.u16LatestCrossedUserVoltageLevelMv = g_rModuleData.u16BoardVoltageCriticalHighMv;
				rUserVoltage.u8LatestUserVoltageState           = rUserVoltageRegistration.u8UserVoltageState;
			}
		} else {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vHighVoltageThread() => Critical high voltage detection is only possible if high voltage detection is also configured. Please check settings in osal.reg");
		}
	}

	g_rModuleData.u8HighVoltageThreadState = DEV_VOLT_C_U8_THREAD_RUNNING;

	if (OSAL_s32SemaphorePost(hSemHighVoltageThread) == OSAL_OK) {
		if (g_rModuleData.u16BoardVoltageHighMv > 0)
			DEV_VOLT_vProcessHighVoltageStateChange(&rUserVoltage, &enLocalVoltageState);
	} else {
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		g_rModuleData.u8HighVoltageThreadState = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;
	}

	while (DEV_VOLT_C_U8_THREAD_RUNNING == g_rModuleData.u8HighVoltageThreadState) {
		if (OSAL_s32EventWait(
			g_rModuleData.hEventHighVoltageThread,
			DEV_VOLT_C_U32_HIGH_VOLT_THREAD_EVENT_MASK_ALL,
			OSAL_EN_EVENTMASK_OR,
			OSAL_C_TIMEOUT_FOREVER,
			&rEventMaskResult) == OSAL_OK) {
			
			if (OSAL_s32EventPost(
				g_rModuleData.hEventHighVoltageThread,
				~rEventMaskResult, 
				OSAL_EN_EVENTMASK_AND) == OSAL_ERROR)
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_vHighVoltageThread() => OSAL_s32EventPost() to clear event mask failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));

			DEV_VOLT_vOnHighVoltageEventReceived(
				rEventMaskResult,
				hSemHighVoltageThread,
				rClientRegistration.u32ClientId,
				&enLocalVoltageState);
		} else {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vHighVoltageThread() => OSAL_s32EventWait() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}

	if ((g_rModuleData.u16BoardVoltageCriticalHighMv > 0) &&
	    (g_rModuleData.u16BoardVoltageHighMv         > 0)   ) {
		rUserVoltageDeregistration.u32ClientId           = rUserVoltageRegistration.u32ClientId;
		rUserVoltageDeregistration.u16UserVoltageLevelMv = g_rModuleData.u16BoardVoltageCriticalHighMv;

		u32OsalErrorCode = DEV_VOLT_u32UnregisterUserVoltage(
					&rUserVoltageDeregistration,
					g_rModuleData.prGlobalData,
					g_rModuleData.hSemDataAccess);

		if (u32OsalErrorCode != OSAL_E_NOERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vHighVoltageThread() => DEV_VOLT_u32UnregisterUserVoltage(CRITICAL-HIGH) failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));
	}

error_register_user_voltage_critical_high:

	if (g_rModuleData.u16BoardVoltageHighMv > 0) {
		rUserVoltageDeregistration.u32ClientId           = rUserVoltageRegistration.u32ClientId;
		rUserVoltageDeregistration.u16UserVoltageLevelMv = g_rModuleData.u16BoardVoltageHighMv;

		u32OsalErrorCode = DEV_VOLT_u32UnregisterUserVoltage(
					&rUserVoltageDeregistration,
					g_rModuleData.prGlobalData,
					g_rModuleData.hSemDataAccess);

		if (u32OsalErrorCode != OSAL_E_NOERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vHighVoltageThread() => DEV_VOLT_u32UnregisterUserVoltage(HIGH) failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));
	}

error_register_user_voltage_high:

	if (OSAL_s32EventClose(g_rModuleData.hEventHighVoltageThread) == OSAL_OK)
		g_rModuleData.hEventHighVoltageThread = OSAL_C_INVALID_HANDLE;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => OSAL_s32EventClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_event_open:

	u32OsalErrorCode = DEV_VOLT_u32UnregisterClient(
				rClientRegistration.u32ClientId,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => DEV_VOLT_u32UnregisterClient() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));

error_register_client:

	u32OsalErrorCode = DEV_VOLT_u32RemoveClient(g_rModuleData.prGlobalData, g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => DEV_VOLT_u32RemoveClient() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));

error_add_client:

	if (OSAL_s32SemaphoreClose(hSemHighVoltageThread) != OSAL_OK)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_semaphore_open:

	if (DEV_VOLT_C_U8_THREAD_NOT_INSTALLED == g_rModuleData.u8HighVoltageThreadState)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vHighVoltageThread() => Immediately left due to setup failure");
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_VOLT_vHighVoltageThread() => Left after controlled shutdown");
}

/*******************************************************************************
*
* This function evaluates the OSAL events which are received within the context
* of the thread function DEV_VOLT_vHighVoltageThread().
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnHighVoltageEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemHighVoltageThread, tU32 u32ClientId, tenVoltageState* penLocalVoltageState)
{
	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_VOLT_vOnHighVoltageEventReceived() => Event STOP_THREAD received, thread is shutting down ...");
    
		g_rModuleData.u8HighVoltageThreadState = DEV_VOLT_C_U8_THREAD_SHUTTING_DOWN;

		if (OSAL_s32SemaphorePost(hSemHighVoltageThread) == OSAL_ERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vOnHighVoltageEventReceived() => OSAL_s32SemaphorePost() for thread shutdown failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD);
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY) {

		tU32 u32OsalErrorCode = OSAL_E_NOERROR;

		DEV_VOLT_trUserVoltage rUserVoltage;

		rUserVoltage.u32ClientId = u32ClientId;

		u32OsalErrorCode = DEV_VOLT_u32GetUserVoltageState(
					&rUserVoltage,
					g_rModuleData.prGlobalData,
					g_rModuleData.hSemDataAccess);

		if (OSAL_E_NOERROR == u32OsalErrorCode)
			DEV_VOLT_vProcessHighVoltageStateChange(&rUserVoltage, penLocalVoltageState);
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vOnHighVoltageEventReceived() => DEV_VOLT_u32GetUserVoltageState() failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY);
	}

	if (rEventMaskResult)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vOnHighVoltageEventReceived() => OSAL_s32EventWait() received unknown event = 0x%08X",
			rEventMaskResult);
}

/*******************************************************************************
*
* This function processes the high voltage state changes by determining the
* next high voltage state on the basis of the latest crossed board voltage
* levels of high-voltage or critical-high-voltage. When the local high
* voltage state-machine changed its state then the respective high voltage
* flags are set and the thread DEV_VOLT_vSystemVoltageThread() is informed via
* a high voltage changed OSAL event.
*
*******************************************************************************/
static tVoid DEV_VOLT_vProcessHighVoltageStateChange(DEV_VOLT_trUserVoltage* prUserVoltage, tenVoltageState* penLocalVoltageState)
{
	tenVoltageState enPreviousLocalVoltageState = *penLocalVoltageState;
	OSAL_tEventMask u32EventMask                = 0x00000000;

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_3,
		"DEV_VOLT_vProcessHighVoltageStateChange() => enLocalVoltageState = %u, u16LatestCrossedUserVoltageLevelMv = %u, u8LatestUserVoltageState = %u",
		*penLocalVoltageState,
		prUserVoltage->u16LatestCrossedUserVoltageLevelMv,
		prUserVoltage->u8LatestUserVoltageState);

	if (DEV_VOLT_u32DataLock(g_rModuleData.hSemDataAccess) != OSAL_E_NOERROR)
		return;

	if (DEV_VOLT_EN_VOLTAGE_STATE_OPERATING == *penLocalVoltageState) {
		if ((prUserVoltage->u16LatestCrossedUserVoltageLevelMv == g_rModuleData.u16BoardVoltageCriticalHighMv) &&
		    (prUserVoltage->u8LatestUserVoltageState           == DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED)            ) {
			*penLocalVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH;
			u32EventMask = (DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED | DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED);

			g_rModuleData.rProtectedModuleData.u32HighVoltageFlags |= (DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED          |
										   DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED |
										   DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE            |
										   DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE    );
		} else if ((prUserVoltage->u16LatestCrossedUserVoltageLevelMv == g_rModuleData.u16BoardVoltageHighMv) &&
			   (prUserVoltage->u8LatestUserVoltageState           == DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED)   ) {
			*penLocalVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_HIGH;
			u32EventMask = DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED;

			g_rModuleData.rProtectedModuleData.u32HighVoltageFlags |= (DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED | 
										   DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE    );
		}
	} else if (DEV_VOLT_EN_VOLTAGE_STATE_HIGH == *penLocalVoltageState) {
		if ((prUserVoltage->u16LatestCrossedUserVoltageLevelMv == g_rModuleData.u16BoardVoltageHighMv) &&
		    (prUserVoltage->u8LatestUserVoltageState           == DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_DECEEDED)   ) {
			*penLocalVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;
			u32EventMask = DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED;

			g_rModuleData.rProtectedModuleData.u32HighVoltageFlags |=  DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED;

			g_rModuleData.rProtectedModuleData.u32HighVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE;
		} else if ((prUserVoltage->u16LatestCrossedUserVoltageLevelMv == g_rModuleData.u16BoardVoltageCriticalHighMv) &&
			   (prUserVoltage->u8LatestUserVoltageState           == DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED)            ) {
			*penLocalVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH;
			u32EventMask = DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED;

			g_rModuleData.rProtectedModuleData.u32HighVoltageFlags |= (DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED |
										   DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE    );
		}
	} else if (DEV_VOLT_EN_VOLTAGE_STATE_CRITICAL_HIGH == *penLocalVoltageState) {
		if ((prUserVoltage->u16LatestCrossedUserVoltageLevelMv == g_rModuleData.u16BoardVoltageHighMv) &&
		    (prUserVoltage->u8LatestUserVoltageState           == DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_DECEEDED)   ) {
			*penLocalVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_OPERATING;
			u32EventMask = (DEV_VOLT_C_U32_EVENT_MASK_HIGH_VOLTAGE_CHANGED | DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED);

			g_rModuleData.rProtectedModuleData.u32HighVoltageFlags |= (DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED         |  
										   DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED );

			g_rModuleData.rProtectedModuleData.u32HighVoltageFlags &= ~(DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE         |
										          DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE );
		} else if ((prUserVoltage->u16LatestCrossedUserVoltageLevelMv == g_rModuleData.u16BoardVoltageCriticalHighMv) &&
			   (prUserVoltage->u8LatestUserVoltageState           == DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_DECEEDED)            ) {
			*penLocalVoltageState = DEV_VOLT_EN_VOLTAGE_STATE_HIGH;
			u32EventMask = DEV_VOLT_C_U32_EVENT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED;

			g_rModuleData.rProtectedModuleData.u32HighVoltageFlags |=  (DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED |
										    DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE             );

			g_rModuleData.rProtectedModuleData.u32HighVoltageFlags &= ~DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE;
		}
	}

	DEV_VOLT_vDataUnlock(g_rModuleData.hSemDataAccess);

	if (*penLocalVoltageState != enPreviousLocalVoltageState)
		if (OSAL_s32EventPost(
			g_rModuleData.hEventSystemVoltageThread,
			u32EventMask,
			OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_vProcessHighVoltageStateChange() => OSAL_s32EventPost() failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));
}

/*******************************************************************************
*
* This function installs the thread which operates as a test client for this
* device.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32InstallClientThread(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	OSAL_trThreadAttribute rThreadAttribute;
	OSAL_tIODescriptor     rVoltIODescriptor;

	if (g_rModuleData.rVoltIODescriptor != OSAL_ERROR)
		return OSAL_E_ALREADYEXISTS;

	rVoltIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

	g_rModuleData.rVoltIODescriptor = rVoltIODescriptor;

	if (g_rModuleData.rVoltIODescriptor == OSAL_ERROR)
		return OSAL_u32ErrorCode();

	if (OSAL_s32SemaphoreCreate(
		DEV_VOLT_C_STRING_SEM_CLIENT_THREAD_NAME,
		&g_rModuleData.hSemClientThread,
		0) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_semaphore_create; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rThreadAttribute.szName       = (tString)DEV_VOLT_C_STRING_CLIENT_THREAD_NAME;
	rThreadAttribute.s32StackSize = DEV_VOLT_C_S32_CLIENT_THREAD_STK_SIZE;
	rThreadAttribute.u32Priority  = DEV_VOLT_C_U32_CLIENT_THREAD_PRIO;
	rThreadAttribute.pfEntry      = (OSAL_tpfThreadEntry) DEV_VOLT_vClientThread;
	rThreadAttribute.pvArg        = NULL;

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32InstallClientThread() => Try to spawn thread ...");

	if (OSAL_ThreadSpawn(&rThreadAttribute) == OSAL_ERROR) {
		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_thread_spawn; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32SemaphoreWait(
		g_rModuleData.hSemClientThread, 
		DEV_VOLT_C_U32_SEMAPHORE_THREAD_TIMEOUT_MS) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_semaphore_wait; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (g_rModuleData.u8ClientThreadState != DEV_VOLT_C_U8_THREAD_RUNNING) {

		u32OsalErrorCode = OSAL_E_UNKNOWN;

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_u32InstallClientThread() => ... failed to install thread");

		goto error_thread_state; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32InstallClientThread() => ... properly installed thread");

	return OSAL_E_NOERROR;

error_thread_state:
error_semaphore_wait:
error_thread_spawn:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemClientThread) == OSAL_OK) {
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_CLIENT_THREAD_NAME) == OSAL_OK) {
			g_rModuleData.hSemClientThread = OSAL_C_INVALID_HANDLE;
		} else {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32InstallClientThread() => OSAL_s32SemaphoreDelete() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	} else {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32InstallClientThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
	}

error_semaphore_create:

	if (OSAL_s32IOClose(g_rModuleData.rVoltIODescriptor) == OSAL_ERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_u32InstallClientThread() => OSAL_s32IOClose() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function un-installs the thread which operates as a test client for this
* device.
*
*******************************************************************************/
static tU32 DEV_VOLT_u32UninstallClientThread(tVoid)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	if (g_rModuleData.u8ClientThreadState != DEV_VOLT_C_U8_THREAD_RUNNING)
		return OSAL_E_NOERROR;

	if (OSAL_s32EventPost(
		g_rModuleData.hEventClientThread,
		DEV_VOLT_C_U32_EVENT_MASK_STOP_CLIENT_THREAD,
		OSAL_EN_EVENTMASK_OR) == OSAL_ERROR)
			return  OSAL_u32ErrorCode();

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32UninstallClientThread() => Sent event STOP_THREAD to leave thread ...");

	if (OSAL_s32SemaphoreWait(
		g_rModuleData.hSemClientThread,
		DEV_VOLT_C_U32_SEMAPHORE_THREAD_TIMEOUT_MS) == OSAL_ERROR) {

		u32OsalErrorCode = OSAL_u32ErrorCode();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (g_rModuleData.u8ClientThreadState != DEV_VOLT_C_U8_THREAD_SHUTTING_DOWN) {

		u32OsalErrorCode = OSAL_E_UNKNOWN;
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32UninstallClientThread() => Thread acknowledged shutdown request ... wait 1 second ...");

	OSAL_s32ThreadWait(1000); // Give thread 1 second to leave its own thread context.

	g_rModuleData.u8ClientThreadState = DEV_VOLT_C_U8_THREAD_OFF;

	DEV_VOLT_vTraceFormatted(
		TR_LEVEL_USER_4,
		"DEV_VOLT_u32UninstallClientThread() => ... 1 second expired and thread should be gone");

error_out:

	if (OSAL_s32SemaphoreClose(g_rModuleData.hSemClientThread) == OSAL_OK) {
		if (OSAL_s32SemaphoreDelete(DEV_VOLT_C_STRING_SEM_CLIENT_THREAD_NAME) == OSAL_OK) {
			g_rModuleData.hSemClientThread = OSAL_C_INVALID_HANDLE;
		} else {
			u32OsalErrorCode = OSAL_u32ErrorCode();
		}
	} else {
		u32OsalErrorCode = OSAL_u32ErrorCode();
	}

	if (OSAL_s32IOClose(g_rModuleData.rVoltIODescriptor) == OSAL_ERROR)
		u32OsalErrorCode = OSAL_u32ErrorCode();

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function represents the thread which handles the OSAL events which are
* used to perform actions of the test client.
*
*******************************************************************************/
static tVoid DEV_VOLT_vClientThread(tPVoid pvArg)
{
	(tVoid) pvArg; // Unused parameter

	tU32            u32OsalErrorCode = OSAL_E_NOERROR;
	OSAL_tSemHandle hSemClientThread = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask rEventMaskResult = 0;

	DEV_VOLT_trClientRegistration        rClientRegistration = { 0 };
	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration = { 0 };
	DEV_VOLT_trUserVoltageRegistration   rUserVoltageRegistration = { 0 };
	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration = { 0 };

	g_rModuleData.u8ClientThreadState = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_C_STRING_SEM_CLIENT_THREAD_NAME,
		&hSemClientThread) == OSAL_ERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => OSAL_s32SemaphoreOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));
		
		goto error_semaphore_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rClientRegistration.u32ClientId = DEV_VOLT_C_U32_CLIENT_ID_INVALID;

	u32OsalErrorCode = DEV_VOLT_u32RegisterClient(
				&rClientRegistration,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => DEV_VOLT_u32RegisterClient() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));

		goto error_register_client; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.u32ClientId = rClientRegistration.u32ClientId;

	if (OSAL_s32EventOpen(
		rClientRegistration.szNotificationEventName,
		&g_rModuleData.hEventClientThread) == OSAL_ERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => OSAL_s32EventOpen() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rSystemVoltageRegistration.u32ClientId              = rClientRegistration.u32ClientId;
	rSystemVoltageRegistration.u32VoltageIndicationMask = (DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
							       DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
							       DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
							       DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  );

	u32OsalErrorCode = DEV_VOLT_u32RegisterSystemVoltage(
				&rSystemVoltageRegistration,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => DEV_VOLT_u32RegisterSystemVoltage() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));

		goto error_register_system_voltage_notification; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
	rUserVoltageRegistration.u16UserVoltageLevelMv = 10000;
	rUserVoltageRegistration.u16HysteresisMv       = 500;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD;

	u32OsalErrorCode = DEV_VOLT_u32RegisterUserVoltage(
				&rUserVoltageRegistration,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR) {
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => DEV_VOLT_u32RegisterUserVoltage() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));

		goto error_register_user_voltage_notification; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	g_rModuleData.u8ClientThreadState = DEV_VOLT_C_U8_THREAD_RUNNING;

	if (OSAL_s32SemaphorePost(hSemClientThread) == OSAL_ERROR) {
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => OSAL_s32SemaphorePost() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

		g_rModuleData.u8ClientThreadState = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;
	}

	while (DEV_VOLT_C_U8_THREAD_RUNNING == g_rModuleData.u8ClientThreadState) {
		if (OSAL_s32EventWait(
			g_rModuleData.hEventClientThread, 
			DEV_VOLT_C_U32_CLIENT_THREAD_EVENT_MASK_ALL, 
			OSAL_EN_EVENTMASK_OR, 
			OSAL_C_TIMEOUT_FOREVER,
			&rEventMaskResult) == OSAL_OK) {
			
			if (OSAL_s32EventPost(
				g_rModuleData.hEventClientThread,
				~rEventMaskResult,
				OSAL_EN_EVENTMASK_AND) == OSAL_ERROR)
					DEV_VOLT_vTraceFormatted(
						TR_LEVEL_FATAL,
						"DEV_VOLT_vClientThread() => OSAL_s32EventPost() failed with error code = %s",
						OSAL_coszErrorText(OSAL_u32ErrorCode()));
			
			DEV_VOLT_vOnClientEventReceived(
				rEventMaskResult,
				hSemClientThread,
				rClientRegistration.u32ClientId);
		} else {
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vClientThread() => OSAL_s32EventWait() failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));
		}
	}

	rUserVoltageDeregistration.u32ClientId           = rUserVoltageRegistration.u32ClientId;
	rUserVoltageDeregistration.u16UserVoltageLevelMv = rUserVoltageRegistration.u16UserVoltageLevelMv;

	u32OsalErrorCode = DEV_VOLT_u32UnregisterUserVoltage(
				&rUserVoltageDeregistration,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => DEV_VOLT_u32UnregisterUserVoltage() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));

error_register_user_voltage_notification:

	u32OsalErrorCode = DEV_VOLT_u32UnregisterSystemVoltage(
				rSystemVoltageRegistration.u32ClientId,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vClientThread() => DEV_VOLT_u32UnregisterSystemVoltage failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));

error_register_system_voltage_notification:

	if (OSAL_s32EventClose(g_rModuleData.hEventClientThread) == OSAL_OK)
		g_rModuleData.hEventClientThread = OSAL_C_INVALID_HANDLE;
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => OSAL_s32EventClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_event_open:

	u32OsalErrorCode = DEV_VOLT_u32UnregisterClient(
				rClientRegistration.u32ClientId,
				g_rModuleData.prGlobalData,
				g_rModuleData.hSemDataAccess);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => u32UnregisterClient() failed with error code = %s",
			OSAL_coszErrorText(u32OsalErrorCode));

	g_rModuleData.u32ClientId = DEV_VOLT_C_U32_CLIENT_ID_INVALID;

error_register_client:

	if (OSAL_s32SemaphoreClose(hSemClientThread) != OSAL_OK)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => OSAL_s32SemaphoreClose() failed with error code = %s",
			OSAL_coszErrorText(OSAL_u32ErrorCode()));

error_semaphore_open:

	if (DEV_VOLT_C_U8_THREAD_NOT_INSTALLED == g_rModuleData.u8ClientThreadState)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vClientThread() => Immediately left due to setup failure");
	else
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_VOLT_vClientThread() => Left after controlled shutdown");
}

/*******************************************************************************
*
* This function evaluates the OSAL events which are received within the context
* of the thread function DEV_VOLT_vClientThread().
*
*******************************************************************************/
static tVoid DEV_VOLT_vOnClientEventReceived(OSAL_tEventMask rEventMaskResult, OSAL_tSemHandle hSemClientThread, tU32 u32ClientId)
{
	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_STOP_CLIENT_THREAD) {

		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_USER_4,
			"DEV_VOLT_vOnClientEventReceived() => Event STOP_THREAD received, thread is shutting down ...");

		g_rModuleData.u8ClientThreadState = DEV_VOLT_C_U8_THREAD_SHUTTING_DOWN;

		if (OSAL_s32SemaphorePost(hSemClientThread) == OSAL_ERROR)
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vOnClientEventReceived() => OSAL_s32SemaphorePost() for thread shutdown failed with error code = %s",
				OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_STOP_CLIENT_THREAD);
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
		
		DEV_VOLT_trSystemVoltageHistory rSystemVoltageHistory;

		rSystemVoltageHistory.u32ClientId = u32ClientId;

		if (OSAL_s32IOControl(
			g_rModuleData.rVoltIODescriptor,
			OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
			(intptr_t)&rSystemVoltageHistory) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_vOnClientEventReceived() => OSAL_s32IOControl(GET_SYSTEM_VOLTAGE_CHANGED_HISTORY) failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY);
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY) {

		DEV_VOLT_trUserVoltage rUserVoltage;

		rUserVoltage.u32ClientId = u32ClientId;

		if (OSAL_s32IOControl(
			g_rModuleData.rVoltIODescriptor,
			OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_STATE, 
			(intptr_t)&rUserVoltage) == OSAL_ERROR)
				DEV_VOLT_vTraceFormatted(
					TR_LEVEL_FATAL,
					"DEV_VOLT_vOnClientEventReceived() => OSAL_s32IOControl(OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_STATE) failed with error code = %s",
					OSAL_coszErrorText(OSAL_u32ErrorCode()));

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY);
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_HIGH_VOLTAGE) {

		// REACT TO PERMANENT HIGH VOLTAGE

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_HIGH_VOLTAGE);
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_CRITICAL_HIGH_VOLTAGE) {

		// REACT TO PERMANENR CRITICAL HIGH VOLTAGE

		rEventMaskResult = rEventMaskResult & (~DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_CRITICAL_HIGH_VOLTAGE);
	}

	if (rEventMaskResult)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_vOnClientEventReceived() => OSAL_s32EventWait() received unknown event = 0x%08X",
			rEventMaskResult);
}

/*******************************************************************************
*
* This function is used within the context of TTFIS callback and allows to add
* free definable user voltage levels the test client should be notified about.
*
*******************************************************************************/
static tBool DEV_VOLT_bAddPrivateUserVoltageNotification(tU16 u16UserVoltageLevelMv, tU16 u16HysteresisMv, tU8 u8LevelCrossDirection)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;
	tBool bResult          = FALSE;

	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	if (g_rModuleData.u32ClientId != DEV_VOLT_C_U32_CLIENT_ID_INVALID) {

		rUserVoltageRegistration.u32ClientId           = g_rModuleData.u32ClientId;
		rUserVoltageRegistration.u8LevelCrossDirection = u8LevelCrossDirection;
		rUserVoltageRegistration.u16UserVoltageLevelMv = u16UserVoltageLevelMv;
		rUserVoltageRegistration.u16HysteresisMv       = u16HysteresisMv;

		u32OsalErrorCode = DEV_VOLT_u32RegisterUserVoltage(
					&rUserVoltageRegistration,
					g_rModuleData.prGlobalData,
					g_rModuleData.hSemDataAccess);

		if (OSAL_E_NOERROR == u32OsalErrorCode)
			bResult = TRUE;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vAddPrivateUserVoltageNotification() => DEV_VOLT_u32RegisterUserVoltage() failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));
	}

	return bResult;
}

/*******************************************************************************
*
* This function is used within the context of TTFIS callback and allows to
* remove free definable user voltage levels the test client should be notified
* about.
*
*******************************************************************************/
static tBool DEV_VOLT_bRemovePrivateUserVoltageNotification(tU16 u16UserVoltageLevelMv)
{
	tU32  u32OsalErrorCode = OSAL_E_NOERROR;
	tBool bResult          = FALSE;

	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration;

	if (g_rModuleData.u32ClientId != DEV_VOLT_C_U32_CLIENT_ID_INVALID) {

		rUserVoltageDeregistration.u32ClientId           = g_rModuleData.u32ClientId;
		rUserVoltageDeregistration.u16UserVoltageLevelMv = u16UserVoltageLevelMv;

		u32OsalErrorCode = DEV_VOLT_u32UnregisterUserVoltage(
					&rUserVoltageDeregistration,
					g_rModuleData.prGlobalData,
					g_rModuleData.hSemDataAccess);

		if (OSAL_E_NOERROR == u32OsalErrorCode)
			bResult = TRUE;
		else
			DEV_VOLT_vTraceFormatted(
				TR_LEVEL_FATAL,
				"DEV_VOLT_vRemovePrivateUserVoltageNotification() => DEV_VOLT_u32UnregisterUserVoltage() failed with error code = %s",
				OSAL_coszErrorText(u32OsalErrorCode));
		}

	return bResult;
}

/******************************************************************************/
/*                                                                            */
/* PUBLIC FUNCTIONS                                                           */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* This function initializes the /dev/volt device.
*
*******************************************************************************/
tU32 DEV_VOLT_OsalIO_u32Init()
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_VOLT_u32Init();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_OsalIO_u32Init() left with OSAL error = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function de-initializes the /dev/volt device.
*
*******************************************************************************/
tU32 DEV_VOLT_OsalIO_u32Deinit()
{
	tU32 u32OsalErrorCode  = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_VOLT_u32Deinit();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_OsalIO_u32Deinit() left with OSAL error = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function opens the /dev/volt device.
*
*******************************************************************************/
tU32 DEV_VOLT_OsalIO_u32Open()
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_VOLT_u32Open();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_OsalIO_u32Open() left with OSAL error = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function closes the /dev/volt device.
*
*******************************************************************************/
tU32 DEV_VOLT_OsalIO_u32Close()
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_VOLT_u32Close();

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_FATAL,
			"DEV_VOLT_OsalIO_u32Close() left with OSAL error = '%s'",
			OSAL_coszErrorText(u32OsalErrorCode));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function is the IO-control interface of the /dev/volt device.
*
*******************************************************************************/
tU32 DEV_VOLT_OsalIO_u32Control(tS32 s32Fun, intptr_t pnArg)
{
	tU32 u32OsalErrorCode = OSAL_E_NOERROR;

	u32OsalErrorCode = DEV_VOLT_u32Control(s32Fun, pnArg);

	if (u32OsalErrorCode != OSAL_E_NOERROR)
		DEV_VOLT_vTraceFormatted(
			TR_LEVEL_ERRORS,
			"DEV_VOLT_OsalIO_u32Control() => Function %d left with OSAL error = '%s'",
			s32Fun,
			OSAL_coszErrorText(u32OsalErrorCode));

	return u32OsalErrorCode;
}

/*******************************************************************************
*
* This function is a wrapper to open the /dev/volt device.
*
*******************************************************************************/
tS32 volt_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16 app_id)
{
	(tVoid) s32Id;    // Unused parameter
	(tVoid) szName;   // Unused parameter
	(tVoid) enAccess; // Unused parameter
	(tVoid) pu32FD;   // Unused parameter
	(tVoid) app_id;   // Unused parameter

	return ((tS32)DEV_VOLT_OsalIO_u32Open());
}

/*******************************************************************************
*
* This function is a wrapper to close the /dev/volt device.
*
*******************************************************************************/
tS32 volt_drv_io_close(tS32 s32ID, uintptr_t u32FD)
{
	(tVoid) s32ID; // Unused parameter
	(tVoid) u32FD; // Unused parameter

	return ((tS32)DEV_VOLT_OsalIO_u32Close());
}

/*******************************************************************************
*
* This function is a wrapper for the IO-control interface of the /dev/volt
* device.
*
*******************************************************************************/
tS32 volt_drv_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t pnArg)
{
	(tVoid) s32ID; // Unused parameter
	(tVoid) u32FD; // Unused parameter

	return ((tS32)DEV_VOLT_OsalIO_u32Control(s32fun, pnArg));
}

/******************************************************************************/
