/*******************************************************************************
*
* FILE:         dev_volt_integration_test.h
*
* SW-COMPONENT: Device Voltage
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Integration tests.
*
* AUTHOR:       CM-AI/PJ-CF33-Kalms
*
* COPYRIGHT:    (c) 2013 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_VOLT_INTEGRATION_TEST_H_
#define _DEV_VOLT_INTEGRATION_TEST_H_

/******************************************************************************/

#define DEV_VOLT_TEST_C_STRING_SEM_CLIENT_PROCESS_START_NAME      "DevVoltStart"

#define DEV_VOLT_TEST_C_STRING_SEM_CLIENT_PROCESS_STOP_NAME        "DevVoltStop"

#define DEV_VOLT_TEST_C_U8_TRACE_BUFFER_LENGTH                                32

/******************************************************************************/
/*                                                                            */
/* CLASS DECLARATION                                                          */
/*                                                                            */
/******************************************************************************/

#ifdef __cplusplus
class DevVoltEnvironment : public testing::Environment
{
  private:
    OSAL_tSemHandle m_hSemClientProcessStart;
    OSAL_tSemHandle m_hSemClientProcessStop;

  public:

    DevVoltEnvironment();

    ~DevVoltEnvironment();

    virtual tVoid SetUp();
    virtual tVoid TearDown();
};

/* -------------------------------------------------------------------------- */

class DevVoltFixture : public testing::Test
{
	protected:

		OSAL_tIODescriptor m_hIODescVoltDriver;
		tU8                m_au8TraceBuffer[DEV_VOLT_TEST_C_U8_TRACE_BUFFER_LENGTH];

  public:
  
    DevVoltFixture();
};

/* -------------------------------------------------------------------------- */

class DevVoltFixtureOpenedDevice : public DevVoltFixture
{
  protected:

    virtual void SetUp()
    {
      m_hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
    } 
};

/* -------------------------------------------------------------------------- */

class DevVoltFixtureCloseDevice : public DevVoltFixture
{

  protected:

    virtual void TearDown()
    {
      OSAL_s32IOClose(m_hIODescVoltDriver);  
    } 
};

/* -------------------------------------------------------------------------- */

class DevVoltFixtureOsal : public DevVoltFixture
{
  protected:

    virtual void SetUp()
    {
      m_hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);
    } 

    virtual void TearDown()
    {
      OSAL_s32IOClose(m_hIODescVoltDriver);  
    } 
};

/* -------------------------------------------------------------------------- */

class DevVoltFixtureSystemVoltage : public DevVoltFixture
{
	protected:
		OSAL_tEventHandle                    m_hEventHandle;
		DEV_VOLT_trClientRegistration        m_rClientRegistration;
		DEV_VOLT_trSystemVoltageRegistration m_rSystemVoltageRegistration;

		virtual void SetUp();
		virtual void TearDown();

	public:
		DevVoltFixtureSystemVoltage();
};

/* -------------------------------------------------------------------------- */

class DevVoltFixtureUserVoltage : public DevVoltFixture
{
	protected:
		OSAL_tEventHandle                    m_hEventHandle;
		DEV_VOLT_trClientRegistration        m_rClientRegistration;

		virtual void SetUp();
		virtual void TearDown();

	public:
		DevVoltFixtureUserVoltage();
};

/* -------------------------------------------------------------------------- */
#endif /* __cplusplus */

#endif //_DEV_VOLT_INTEGRATION_TEST_H_
