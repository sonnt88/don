/*******************************************************************************
*
* FILE:         dev_volt_integration_test.cpp
*
* SW-COMPONENT: Device Voltage
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Integration tests.
*
* AUTHOR:       CM-AI/PJ-CF33-Kalms
*
* COPYRIGHT:    (c) 2013 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

/* ---------------------------- Linux standard ------------------------------ */
#include <errno.h>
#include <stdio.h>
/* ------------------------------ Google-Test ------------------------------- */
#include "gmock/gmock.h"
#include "gtest/gtest.h"
/* ---------------------------------- OSAL ---------------------------------- */
extern "C"
{
  #undef __cplusplus 
    #define OSAL_S_IMPORT_INTERFACE_GENERIC
    #include "osal_if.h"
  #define __cplusplus
}
/* ----------------------------- Voltage Device ----------------------------- */
extern "C"
{
  #undef __cplusplus 
    #define DEV_VOLT_IMPORT_INTERFACE_GENERIC
    #include "../dev_volt_if.h"
  #define __cplusplus
}
/* ---------------------------------- Self ---------------------------------- */
#include "dev_volt_integration_test.h"

/******************************************************************************/
/*                                                                            */
/* DEFINES                                                                    */
/*                                                                            */
/******************************************************************************/

// These Default settings ...
#define DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS                         600
#define DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS                    600
#define DEV_VOLT_CONF_C_U32_PERMANENT_HIGH_VOLTAGE_DURATION_MS              3000
#define DEV_VOLT_CONF_C_U32_PERMANENT_CRITICAL_HIGH_VOLTAGE_DURATION_MS     2000
// ... must match to the below default settings of the voltage device :
//
//    - DEV_VOLT_CONF_C_U32_DEFAULT_LOW_VOLTAGE_OVER_DELAY_MS                200
//    - DEV_VOLT_CONF_C_U32_DEFAULT_CRITICAL_LOW_VOLTAGE_OVER_DELAY_MS       200
//    - DEV_VOLT_CONF_C_U32_DEFAULT_HIGH_VOLTAGE_TIMER_MS                   2000
//    - DEV_VOLT_CONF_C_U32_DEFAULT_CRITICAL_HIGH_VOLTAGE_TIMER_MS          1000

#define DEV_VOLT_CONF_C_U32_PROCESS_BOARD_VOLTAGE_CHANGED_MS                 100

/* -------------------------------------------------------------------------- */

#define DEV_VOLT_TEST_C_STRING_CLIENT_PROCESS_PATH\
		"./dev_volt_client_process_out.out"
#define DEV_VOLT_TEST_C_STRING_CLIENT_PROCESS_NAME\
		"dev_volt_client_process_out.out"

#define DEV_VOLT_C_U32_SEM_CLIENT_PROCESS_TIMEOUT_MS                        1000

/******************************************************************************/
/*                                                                            */
/* GLOBAL VARIABLES                                                           */
/*                                                                            */
/******************************************************************************/

static DevVoltEnvironment* g_poDevVoltEnvironment;

/******************************************************************************/
/*                                                                            */
/* PUBLIC FUNCTIONS                                                           */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Main function which attaches a global test environment to be able to execute
* SetUp() and TearDown() methods at the beginning and at the end of all tests
* and which starts the execution of the tests via the RUN_ALL_TESTS() macro.
*
*******************************************************************************/
extern "C"
{
	void vStartApp(int argc, char **argv)
	{
		g_poDevVoltEnvironment = new DevVoltEnvironment;

		::testing::AddGlobalTestEnvironment(g_poDevVoltEnvironment);

		::testing::InitGoogleMock(&argc, argv);
		
		RUN_ALL_TESTS();
    }
} // extern "C"

/******************************************************************************/
/*                                                                            */
/* METHODS                                                                    */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Constructor for class DevVoltFixture.
*
*******************************************************************************/
DevVoltFixture::DevVoltFixture():
	m_hIODescVoltDriver(OSAL_ERROR)
{
	OSAL_pvMemorySet(
		m_au8TraceBuffer,
		0,
		sizeof(m_au8TraceBuffer));
}

/*******************************************************************************
*
* Constructor for class DevVoltFixtureSystemVoltage.
*
*******************************************************************************/
DevVoltFixtureSystemVoltage::DevVoltFixtureSystemVoltage():
	m_hEventHandle(OSAL_C_INVALID_HANDLE)
{
	m_rClientRegistration.u32ClientId                     = DEV_VOLT_C_U32_CLIENT_ID_INVALID;
	m_rSystemVoltageRegistration.u32VoltageIndicationMask = 0;
}

/*******************************************************************************
*
* Constructor for class DevVoltFixtureUserVoltage.
*
*******************************************************************************/
DevVoltFixtureUserVoltage::DevVoltFixtureUserVoltage():
	m_hEventHandle(OSAL_C_INVALID_HANDLE)
{
	m_rClientRegistration.u32ClientId                     = DEV_VOLT_C_U32_CLIENT_ID_INVALID;
}

/*******************************************************************************
*
* SetUp method for 
*
*******************************************************************************/
tVoid DevVoltFixtureSystemVoltage::SetUp()
{
	m_hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

	if (m_hIODescVoltDriver == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&m_rClientRegistration) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_register_client; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventOpen(
		m_rClientRegistration.szNotificationEventName,
		&m_hEventHandle) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	m_rSystemVoltageRegistration.u32ClientId              = m_rClientRegistration.u32ClientId;
	m_rSystemVoltageRegistration.u32VoltageIndicationMask =
		(DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
		 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
		 DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
		 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  );

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
		(intptr_t)&m_rSystemVoltageRegistration) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_register_system_voltage; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	return;

error_register_system_voltage:

	OSAL_s32EventClose(m_hEventHandle);

error_event_open:

	OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
		(intptr_t)m_rClientRegistration.u32ClientId);

error_register_client:

	OSAL_s32IOClose(m_hIODescVoltDriver);  
}

/*******************************************************************************
*
* TearDown method for 
*
*******************************************************************************/
tVoid DevVoltFixtureSystemVoltage::TearDown()
{
	if ((m_hEventHandle != OSAL_C_INVALID_HANDLE)         &&
	    (OSAL_s32EventClose(m_hEventHandle) == OSAL_ERROR)  )
		ADD_FAILURE();

	if (m_rClientRegistration.u32ClientId != DEV_VOLT_C_U32_CLIENT_ID_INVALID) {
		if (OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
			(intptr_t)m_rClientRegistration.u32ClientId) == OSAL_ERROR)
				ADD_FAILURE();
		if (OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)m_rClientRegistration.u32ClientId) == OSAL_ERROR)
				ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0xAA; // 0x000000AA = Critical low voltage +
	m_au8TraceBuffer[3] = 0x00; //              Low voltage +
	m_au8TraceBuffer[4] = 0x00; //              Critical high voltage +
	m_au8TraceBuffer[5] = 0x00; //              High voltage => changed to inactive

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR)
		ADD_FAILURE();

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);
	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);

	m_au8TraceBuffer[0] = 0x03; // Command length
	m_au8TraceBuffer[1] = 0x81; // DEV_VOLT_SIMULATE_BOARD_VOLTAGE_MV
	m_au8TraceBuffer[2] = (tU8)((14000 & 0x00FF) >> 0);
	m_au8TraceBuffer[3] = (tU8)((14000 & 0xFF00) >> 8);

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR)
		ADD_FAILURE();

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if ((m_hIODescVoltDriver != OSAL_ERROR)                 &&
	    (OSAL_s32IOClose(m_hIODescVoltDriver) == OSAL_ERROR)  )
		ADD_FAILURE();
}

/*******************************************************************************
*
* SetUp method for 
*
*******************************************************************************/
tVoid DevVoltFixtureUserVoltage::SetUp()
{
	m_hIODescVoltDriver = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

	if (m_hIODescVoltDriver == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&m_rClientRegistration) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_register_client; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventOpen(
		m_rClientRegistration.szNotificationEventName,
		&m_hEventHandle) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	return;

error_event_open:

	OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
		(intptr_t)m_rClientRegistration.u32ClientId);

error_register_client:

	OSAL_s32IOClose(m_hIODescVoltDriver);  
}

/*******************************************************************************
*
* TearDown method for 
*
*******************************************************************************/
tVoid DevVoltFixtureUserVoltage::TearDown()
{
	if ((m_hEventHandle != OSAL_C_INVALID_HANDLE)         &&
	    (OSAL_s32EventClose(m_hEventHandle) == OSAL_ERROR)  )
		ADD_FAILURE();

	if ((m_rClientRegistration.u32ClientId != DEV_VOLT_C_U32_CLIENT_ID_INVALID) &&
	    (OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)m_rClientRegistration.u32ClientId) == OSAL_ERROR)       )
				ADD_FAILURE();

	m_au8TraceBuffer[0] = 0x03; // Command length
	m_au8TraceBuffer[1] = 0x81; // DEV_VOLT_SIMULATE_BOARD_VOLTAGE_MV
	m_au8TraceBuffer[2] = (tU8)((14000 & 0x00FF) >> 0);
	m_au8TraceBuffer[3] = (tU8)((14000 & 0xFF00) >> 8);

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR)
		ADD_FAILURE();

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_PROCESS_BOARD_VOLTAGE_CHANGED_MS);

	if ((m_hIODescVoltDriver != OSAL_ERROR)                 &&
	    (OSAL_s32IOClose(m_hIODescVoltDriver) == OSAL_ERROR)  )
		ADD_FAILURE();
}

/*******************************************************************************
*
* Constructor for class DevVoltEnvironment.
*
*******************************************************************************/
DevVoltEnvironment::DevVoltEnvironment() :
  m_hSemClientProcessStart(OSAL_C_INVALID_HANDLE),
  m_hSemClientProcessStop(OSAL_C_INVALID_HANDLE)
{
}

/*******************************************************************************
*
* Destructor for class DevVoltEnvironment.
*
*******************************************************************************/
DevVoltEnvironment::~DevVoltEnvironment()
{
}

/*******************************************************************************
*
* SetUp method for the entire test environment, which is executed once before
* all tests.
*
*******************************************************************************/
tVoid DevVoltEnvironment::SetUp()
{
	OSAL_trProcessAttribute rProcessAttribute = { 0 };

	rProcessAttribute.szAppName   = (tString)DEV_VOLT_TEST_C_STRING_CLIENT_PROCESS_PATH;
	rProcessAttribute.szName      = (tString)DEV_VOLT_TEST_C_STRING_CLIENT_PROCESS_NAME;
	rProcessAttribute.u32Priority = 100;

	if (OSAL_s32SemaphoreCreate(
		DEV_VOLT_TEST_C_STRING_SEM_CLIENT_PROCESS_START_NAME,
		&m_hSemClientProcessStart,
		(tU32) 0) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	if (OSAL_ProcessSpawn(&rProcessAttribute) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	if (OSAL_s32SemaphoreWait(
		m_hSemClientProcessStart,
		DEV_VOLT_C_U32_SEM_CLIENT_PROCESS_TIMEOUT_MS) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_TEST_C_STRING_SEM_CLIENT_PROCESS_STOP_NAME,
		&m_hSemClientProcessStop) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	return;
}

/*******************************************************************************
*
* TearDown method for the entire test environment, which is executed once after
* all tests.
*
*******************************************************************************/
tVoid DevVoltEnvironment::TearDown()
{
	if (m_hSemClientProcessStart != OSAL_C_INVALID_HANDLE) {
		if (OSAL_s32SemaphoreClose(m_hSemClientProcessStart) == OSAL_OK) {
			if (OSAL_s32SemaphoreDelete(DEV_VOLT_TEST_C_STRING_SEM_CLIENT_PROCESS_START_NAME) == OSAL_ERROR)
				ADD_FAILURE();
		} else {
			ADD_FAILURE();
		}
	}

	if (m_hSemClientProcessStop != OSAL_C_INVALID_HANDLE) {
		if (OSAL_s32SemaphorePost(m_hSemClientProcessStop) == OSAL_ERROR)
			ADD_FAILURE();

		if (OSAL_s32SemaphoreClose(m_hSemClientProcessStop) == OSAL_ERROR)
			ADD_FAILURE();
	}

	// Wait until the 'client' process is terminated BEFORE this 'integration_test'
	// process terminates because the OSAL will drop its core resources when the
	// process which initially loaded the OSAL library is terminated. The termination
	// of the 'client' process will lead to a de-initialization and termination of
	// the /dev/wup.

	// The duration for the delay must be longer than 2000 ms because the /dev/wup
	// has an active 1000 ms delay inside the shutdown sequence of the three central
	// threads 'SystemVoltageThread', 'UserVoltageThread' and 'HighVoltageThread'. 
	OSAL_s32ThreadWait(4000);
}

/******************************************************************************/
/*                                                                            */
/* TESTS                                                                      */
/*                                                                            */
/******************************************************************************/

TEST_F(DevVoltFixtureCloseDevice, OpenDevice)
{
	m_hIODescVoltDriver = OSAL_IOOpen(
				OSAL_C_STRING_DEVICE_VOLT,
				OSAL_EN_READWRITE);

	EXPECT_NE (OSAL_ERROR, m_hIODescVoltDriver);
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOpenedDevice, CloseDevice)
{
	EXPECT_EQ (OSAL_OK, OSAL_s32IOClose(m_hIODescVoltDriver)); 
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_GET_BOARD_VOLTAGE)
{
	tU16 u16BoardVoltageMv;

	EXPECT_EQ(
		OSAL_OK,
		OSAL_s32IOControl(m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE,
			(intptr_t)&u16BoardVoltageMv));
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_GET_SYSTEM_VOLTAGE_STATE)
{
	tU32 u32SystemVoltageState;

	EXPECT_EQ(
		OSAL_OK,
		OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_STATE, 
			(intptr_t)&u32SystemVoltageState));
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_REGISTER_CLIENT)
{
	OSAL_tEventHandle hEventHandle = OSAL_C_INVALID_HANDLE;

	DEV_VOLT_trClientRegistration rClientRegistration;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_OK) {

		if (OSAL_s32EventOpen(
			rClientRegistration.szNotificationEventName,
			&hEventHandle) == OSAL_ERROR)
			ADD_FAILURE();

		if (OSAL_s32EventClose(hEventHandle) == OSAL_ERROR)
			ADD_FAILURE();

		OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)rClientRegistration.u32ClientId);
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_UNREGISTER_CLIENT)
{
	DEV_VOLT_trClientRegistration rClientRegistration;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_OK) {
		if (OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)rClientRegistration.u32ClientId) == OSAL_ERROR)
				ADD_FAILURE();
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_REGISTER_SYSTEM_VOLTAGE_NOTIFICATION)
{
	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_OK) {

		rSystemVoltageRegistration.u32ClientId              = rClientRegistration.u32ClientId;
		rSystemVoltageRegistration.u32VoltageIndicationMask =
			(DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
			 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
			 DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
			 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  );

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_REGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
			(intptr_t)&rSystemVoltageRegistration) == OSAL_OK) {
			OSAL_s32IOControl(
				m_hIODescVoltDriver, 
				OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
				(intptr_t)rSystemVoltageRegistration.u32ClientId);
		} else {
			ADD_FAILURE();
		}

		OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)rClientRegistration.u32ClientId);
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_UNREGISTER_SYSTEM_VOLTAGE_NOTIFICATION)
{
	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_OK) {

		rSystemVoltageRegistration.u32ClientId              = rClientRegistration.u32ClientId;
		rSystemVoltageRegistration.u32VoltageIndicationMask =
			(DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
			 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
			 DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
			 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  );

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_REGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
			(intptr_t)&rSystemVoltageRegistration) == OSAL_OK) {
			if (OSAL_s32IOControl(
				m_hIODescVoltDriver, 
				OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
				(intptr_t)rSystemVoltageRegistration.u32ClientId) == OSAL_ERROR)
				ADD_FAILURE();
		} else {
			ADD_FAILURE();
		}

		OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)rClientRegistration.u32ClientId);
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_REGISTER_USER_VOLTAGE_NOTIFICATION)
{
	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration   rUserVoltageRegistration;
	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_OK) {

		rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
		rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD;
		rUserVoltageRegistration.u16UserVoltageLevelMv = 18000;
		rUserVoltageRegistration.u16HysteresisMv       = 300;

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_REGISTER_USER_VOLTAGE_NOTIFICATION,
			(intptr_t)&rUserVoltageRegistration) == OSAL_OK) {
				
			rUserVoltageDeregistration.u32ClientId           = rUserVoltageRegistration.u32ClientId;
			rUserVoltageDeregistration.u16UserVoltageLevelMv = rUserVoltageRegistration.u16UserVoltageLevelMv;

			OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_USER_VOLTAGE_NOTIFICATION,
				(intptr_t)&rUserVoltageDeregistration);
		} else {
			ADD_FAILURE();
		}

		OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)rClientRegistration.u32ClientId);
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_UNREGISTER_USER_VOLTAGE_NOTIFICATION)
{
	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration   rUserVoltageRegistration;
	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_OK) {

		rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
		rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD;
		rUserVoltageRegistration.u16UserVoltageLevelMv = 18000;
		rUserVoltageRegistration.u16HysteresisMv       = 300;

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_REGISTER_USER_VOLTAGE_NOTIFICATION,
			(intptr_t)&rUserVoltageRegistration) == OSAL_OK) {
				
			rUserVoltageDeregistration.u32ClientId           = rUserVoltageRegistration.u32ClientId;
			rUserVoltageDeregistration.u16UserVoltageLevelMv = rUserVoltageRegistration.u16UserVoltageLevelMv;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_USER_VOLTAGE_NOTIFICATION,
				(intptr_t)&rUserVoltageDeregistration) == OSAL_ERROR)
				ADD_FAILURE();
		} else {
			ADD_FAILURE();
		}

		OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)rClientRegistration.u32ClientId);
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_GET_SYSTEM_VOLTAGE_HISTORY)
{
	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;
	DEV_VOLT_trSystemVoltageHistory      rSystemVoltageHistory;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_OK) {

		rSystemVoltageRegistration.u32ClientId              = rClientRegistration.u32ClientId;
		rSystemVoltageRegistration.u32VoltageIndicationMask =
			(DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
			 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
			 DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
			 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  );

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_REGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
			(intptr_t)&rSystemVoltageRegistration) == OSAL_OK) {

			rSystemVoltageHistory.u32ClientId = rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_ERROR)
				ADD_FAILURE();

			OSAL_s32IOControl(
				m_hIODescVoltDriver, 
				OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
				(intptr_t)rSystemVoltageRegistration.u32ClientId);
		} else {
			ADD_FAILURE();
		}

		OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)rClientRegistration.u32ClientId);
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureOsal, IOControl_GET_USER_VOLTAGE_STATE)
{
	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration   rUserVoltageRegistration;
	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration;
	DEV_VOLT_trUserVoltage               rUserVoltage;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration) == OSAL_OK) {

		rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
		rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD;
		rUserVoltageRegistration.u16UserVoltageLevelMv = 18000;
		rUserVoltageRegistration.u16HysteresisMv       = 300;

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_REGISTER_USER_VOLTAGE_NOTIFICATION,
			(intptr_t)&rUserVoltageRegistration) == OSAL_OK) {

			rUserVoltage.u32ClientId = rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_STATE,
				(intptr_t)&rUserVoltage) == OSAL_ERROR)
				ADD_FAILURE();

			rUserVoltageDeregistration.u32ClientId           = rUserVoltageRegistration.u32ClientId;
			rUserVoltageDeregistration.u16UserVoltageLevelMv = rUserVoltageRegistration.u16UserVoltageLevelMv;

			OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_USER_VOLTAGE_NOTIFICATION,
				(intptr_t)&rUserVoltageDeregistration);
		} else {
			ADD_FAILURE();
		}

		OSAL_s32IOControl(
			m_hIODescVoltDriver, 
			OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
			(intptr_t)rClientRegistration.u32ClientId);
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, LowVoltageActiveWhileOperatingVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x03; // 0x00000003 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE   = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalLowVoltageActiveWhileOperatingVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x0C; // 0x0000000C => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE   = 0x00000004
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED = 0x00000008
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 1)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, HighVoltageActiveWhileOperatingVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x30; // 0x00000030 => DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE   = 0x00000010
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED = 0x00000020
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 1)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_HIGH)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalHighVoltageActiveWhileOperatingVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0xC0; // 0x000000C0 => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE   = 0x00000040
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED = 0x00000080
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_HIGH)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalLowVoltageActiveAndLowVoltageActiveWhileOperatingVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x0F; // 0x0000000F => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE            = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED          = 0x00000002
	m_au8TraceBuffer[4] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE   = 0x00000004
	m_au8TraceBuffer[5] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED = 0x00000008

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 1)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalLowVoltageSpikeAndLowVoltageActiveWhileOperatingVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x0B; // 0x0000000B => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE            = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED          = 0x00000002
	m_au8TraceBuffer[4] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED = 0x00000008
	m_au8TraceBuffer[5] = 0x00; //

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 1)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, LowVoltageSpikeWhileOperatingVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x02; // 0x00000002 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[3] = 0x00; // 
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_OPERATING)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalLowVoltageSpikeWhileOperatingVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x08; // 0x00000008 => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED = 0x00000008
	m_au8TraceBuffer[3] = 0x00; // 
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);
	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 1)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_OPERATING)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, Set16000MvWhileOperatingVoltageToEnterHighVoltage)
{
	OSAL_tEventMask                   rEventMaskResult        = 0;
	DEV_VOLT_trSystemVoltageHistory   rSystemVoltageHistory   = { 0 };

	m_au8TraceBuffer[0] = 0x03; // Command length
	m_au8TraceBuffer[1] = 0x81; // DEV_VOLT_SIMULATE_BOARD_VOLTAGE_MV
	m_au8TraceBuffer[2] = (tU8)((16000 & 0x00FF) >> 0);
	m_au8TraceBuffer[3] = (tU8)((16000 & 0xFF00) >> 8);

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 1)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_HIGH)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, Set17200MvWhileOperatingVoltageToEnterCriticalHighVoltage)
{
	OSAL_tEventMask                   rEventMaskResult        = 0;
	DEV_VOLT_trSystemVoltageHistory   rSystemVoltageHistory   = { 0 };

	m_au8TraceBuffer[0] = 0x03; // Command length
	m_au8TraceBuffer[1] = 0x81; // DEV_VOLT_SIMULATE_BOARD_VOLTAGE_MV
	m_au8TraceBuffer[2] = (tU8)((17200 & 0x00FF) >> 0);
	m_au8TraceBuffer[3] = (tU8)((17200 & 0xFF00) >> 8);

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_HIGH)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalLowVoltageSpikeWhileCriticalHigh)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0xC0; // 0x000000C0 => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE   = 0x00000040
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED = 0x00000080
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_HIGH)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x08; // 0x00000008 => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED = 0x00000008
	m_au8TraceBuffer[3] = 0x00; // 
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);
	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_HIGH)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, MultipleLowVoltagesActiveWhileOperatingVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x03; // 0x00000003 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE   = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x02; // 0x00000002 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[3] = 0x00; //
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x03; // 0x00000003 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE   = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 2)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, LowVoltageActiveWhileLowVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x03; // 0x00000003 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE   = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x03; // 0x00000003 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE   = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
		ADD_FAILURE();
	} else {
		if (OSAL_u32ErrorCode() == OSAL_E_TIMEOUT) {
			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalLowVoltageActiveWhileLowVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x03; // 0x00000003 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE   = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x0C; // 0x0000000C => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE   = 0x00000004
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED = 0x00000008
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 1)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, HighVoltageActiveWhileLowVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x03; // 0x00000003 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE   = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x31; // 0x00000031 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE    = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE   = 0x00000010
	m_au8TraceBuffer[4] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED = 0x00000020
	m_au8TraceBuffer[5] = 0x00; //

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
		ADD_FAILURE();
	} else {
		if (OSAL_u32ErrorCode() == OSAL_E_TIMEOUT) {
			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalHighVoltageActiveWhileLowVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x03; // 0x00000003 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE   = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0xC1; // 0x000000C1 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE             = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE   = 0x00000040
	m_au8TraceBuffer[4] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED = 0x00000080
	m_au8TraceBuffer[5] = 0x00; //

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
		ADD_FAILURE();
	} else {
		if (OSAL_u32ErrorCode() == OSAL_E_TIMEOUT) {
			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, LowVoltageInactiveWhileLowVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x03; // 0x00000003 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_STATE   = 0x00000001
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                      ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x02; // 0x00000002 => DEV_VOLT_C_U32_BIT_MASK_LOW_VOLTAGE_CHANGED = 0x00000002
	m_au8TraceBuffer[3] = 0x00; //
	m_au8TraceBuffer[4] = 0x00; //
	m_au8TraceBuffer[5] = 0x00; //

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_OPERATING)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalLowVoltageInactiveWhileCritricalLowVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x0C; // 0x0000000C => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_STATE   = 0x00000004
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED = 0x00000008
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 1)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 1)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                               ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_LOW)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x08; // 0x00000008 => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_LOW_VOLTAGE_CHANGED = 0x00000008
	m_au8TraceBuffer[3] = 0x00; //
	m_au8TraceBuffer[4] = 0x00; //
	m_au8TraceBuffer[5] = 0x00; //

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);
	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_OPERATING)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, HighVoltageInactiveWhileHighVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x30; // 0x00000030 => DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE   = 0x00000010
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED = 0x00000020
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 1)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                       ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_HIGH)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x20; // 0x00000020 => DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED = 0x00000020
	m_au8TraceBuffer[3] = 0x00; //
	m_au8TraceBuffer[4] = 0x00; //
	m_au8TraceBuffer[5] = 0x00; //

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_OPERATING)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, CriticalHighVoltageInactiveWhileCriticalHighVoltage)
{
	OSAL_tEventMask                  rEventMaskResult      = 0;
	DEV_VOLT_trSystemVoltageHistory  rSystemVoltageHistory = { 0 };

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0xC0; // 0x000000C0 => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_STATE   = 0x00000040
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED = 0x00000080
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_STATE_CHANGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 1)                                                ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_CRITICAL_HIGH)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x80; // 0x00000080 => DEV_VOLT_C_U32_BIT_MASK_CRITICAL_HIGH_VOLTAGE_CHANGED = 0x00000080
	m_au8TraceBuffer[3] = 0x00; //
	m_au8TraceBuffer[4] = 0x00; //
	m_au8TraceBuffer[5] = 0x00; //

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_OVER_DELAY_PASSED_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_OK) {
			
		if (OSAL_s32EventPost(
			m_hEventHandle,
			~rEventMaskResult,
			OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
		}

		if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_SYSTEM_VOLTAGE_CHANGED_NOTIFY) {
			rSystemVoltageHistory.u32ClientId = m_rClientRegistration.u32ClientId;

			if (OSAL_s32IOControl(
				m_hIODescVoltDriver,
				OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_HISTORY,
				(intptr_t)&rSystemVoltageHistory) == OSAL_OK) {
				
				if ((rSystemVoltageHistory.rSystemVoltage.u32CriticalLowVoltageCounter  != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32LowVoltageCounter          != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32HighVoltageCounter         != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CriticalHighVoltageCounter != 0)                                            ||
				    (rSystemVoltageHistory.rSystemVoltage.u32CurrentSystemVoltageState  != DEV_VOLT_C_U32_SYSTEM_VOLTAGE_STATE_OPERATING)  )
					ADD_FAILURE();
			} else {
				ADD_FAILURE();
			}
		} else {
			ADD_FAILURE();
		}
	} else {
		ADD_FAILURE();
	}
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureUserVoltage, MultipleClientsWithIdenticalLevelButDifferentAndIncreasingHysteresis)
{
	OSAL_tIODescriptor                 hIODescVoltDriver2           = OSAL_ERROR;
	OSAL_tEventHandle                  hEventHandle2                = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask                    rEventMaskResult             = 0;

	DEV_VOLT_trClientRegistration      rClientRegistration2         = { 0 };
	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration1    = { 0 };
	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration2    = { 0 };

	DEV_VOLT_trUserVoltage               rUserVoltage               = { 0 };
	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration = { 0 };

	rUserVoltageRegistration1.u32ClientId           = m_rClientRegistration.u32ClientId;
	rUserVoltageRegistration1.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD;
	rUserVoltageRegistration1.u16UserVoltageLevelMv = 16000;
	rUserVoltageRegistration1.u16HysteresisMv       = 200;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_USER_VOLTAGE_NOTIFICATION,
		(intptr_t)&rUserVoltageRegistration1) == OSAL_ERROR) {
			ADD_FAILURE();
			return;
	}

	hIODescVoltDriver2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

	if (hIODescVoltDriver2 == OSAL_ERROR) {
		goto error_open_device; /*lint !e801, authorized LINT-deactivation #<71> */
		return;
	}

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration2) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_register_client_2; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventOpen(
		rClientRegistration2.szNotificationEventName,
		&hEventHandle2) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rUserVoltageRegistration2.u32ClientId           = rClientRegistration2.u32ClientId;
	rUserVoltageRegistration2.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD;
	rUserVoltageRegistration2.u16UserVoltageLevelMv = 16000;
	rUserVoltageRegistration2.u16HysteresisMv       = 250;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_USER_VOLTAGE_NOTIFICATION,
		(intptr_t)&rUserVoltageRegistration2) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_register_user_voltage_2; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	m_au8TraceBuffer[0] = 0x03; // Command length
	m_au8TraceBuffer[1] = 0x81; // DEV_VOLT_SIMULATE_BOARD_VOLTAGE_MV
	m_au8TraceBuffer[2] = (tU8)((16000 & 0x00FF) >> 0);
	m_au8TraceBuffer[3] = (tU8)((16000 & 0xFF00) >> 8);

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_PROCESS_BOARD_VOLTAGE_CHANGED_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}		

	if (OSAL_s32EventPost(
		m_hEventHandle,
		~rEventMaskResult,
		OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY) {
		rUserVoltage.u32ClientId = m_rClientRegistration.u32ClientId;

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_STATE,
			(intptr_t)&rUserVoltage) == OSAL_OK) {
				if ((rUserVoltage.u16LatestCrossedUserVoltageLevelMv != 16000)                                    ||
				    (rUserVoltage.u8LatestUserVoltageState           != DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED)  ) {
					ADD_FAILURE();
					goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
				}
		} else {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}
	} else {
		ADD_FAILURE();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventWait(
		hEventHandle2,
		DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}		

	if (OSAL_s32EventPost(
		hEventHandle2,
		~rEventMaskResult,
		OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY) {
		rUserVoltage.u32ClientId = rClientRegistration2.u32ClientId;

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_STATE,
			(intptr_t)&rUserVoltage) == OSAL_OK) {
				if ((rUserVoltage.u16LatestCrossedUserVoltageLevelMv != 16000)                                    ||
				    (rUserVoltage.u8LatestUserVoltageState           != DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED)  ) {
					ADD_FAILURE();
					goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
				}
		} else {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}
	} else {
		ADD_FAILURE();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	m_au8TraceBuffer[0] = 0x03; // Command length
	m_au8TraceBuffer[1] = 0x81; // DEV_VOLT_SIMULATE_BOARD_VOLTAGE_MV
	m_au8TraceBuffer[2] = (tU8)((12000 & 0x00FF) >> 0);
	m_au8TraceBuffer[3] = (tU8)((12000 & 0xFF00) >> 8);

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_PROCESS_BOARD_VOLTAGE_CHANGED_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}		

	if (OSAL_s32EventPost(
		m_hEventHandle,
		~rEventMaskResult,
		OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY) {
		rUserVoltage.u32ClientId = m_rClientRegistration.u32ClientId;

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_STATE,
			(intptr_t)&rUserVoltage) == OSAL_OK) {
				if ((rUserVoltage.u16LatestCrossedUserVoltageLevelMv != 16000)                                    ||
				    (rUserVoltage.u8LatestUserVoltageState           != DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_DECEEDED)  ) {
					ADD_FAILURE();
					goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
				}
		} else {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}
	} else {
		ADD_FAILURE();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventWait(
		hEventHandle2,
		DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}		

	if (OSAL_s32EventPost(
		hEventHandle2,
		~rEventMaskResult,
		OSAL_EN_EVENTMASK_AND) == OSAL_ERROR) {
		ADD_FAILURE();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (rEventMaskResult & DEV_VOLT_C_U32_EVENT_MASK_USER_VOLTAGE_CHANGED_NOTIFY) {
		rUserVoltage.u32ClientId = rClientRegistration2.u32ClientId;

		if (OSAL_s32IOControl(
			m_hIODescVoltDriver,
			OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_STATE,
			(intptr_t)&rUserVoltage) == OSAL_OK) {
				if ((rUserVoltage.u16LatestCrossedUserVoltageLevelMv != 16000)                                    ||
				    (rUserVoltage.u8LatestUserVoltageState           != DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_DECEEDED)  ) {
					ADD_FAILURE();
					goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
				}
		} else {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
		}
	} else {
		ADD_FAILURE();
		goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

error_out:

	rUserVoltageDeregistration.u32ClientId           = rClientRegistration2.u32ClientId;
	rUserVoltageDeregistration.u16UserVoltageLevelMv = rUserVoltageRegistration2.u16UserVoltageLevelMv;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_USER_VOLTAGE_NOTIFICATION,
		(intptr_t)&rUserVoltageDeregistration) == OSAL_ERROR)
			ADD_FAILURE();

error_register_user_voltage_2:

	if (OSAL_s32EventClose(hEventHandle2) == OSAL_ERROR)
		ADD_FAILURE();

error_event_open:

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
		(intptr_t)rClientRegistration2.u32ClientId) == OSAL_ERROR)
			ADD_FAILURE();

error_register_client_2:

	if (OSAL_s32IOClose(hIODescVoltDriver2) == OSAL_ERROR)
		ADD_FAILURE();


error_open_device:

	rUserVoltageDeregistration.u32ClientId           = m_rClientRegistration.u32ClientId;
	rUserVoltageDeregistration.u16UserVoltageLevelMv = rUserVoltageRegistration1.u16UserVoltageLevelMv;

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver,
		OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_USER_VOLTAGE_NOTIFICATION,
		(intptr_t)&rUserVoltageDeregistration) == OSAL_ERROR)
			ADD_FAILURE();
}

/* -------------------------------------------------------------------------- */

TEST_F(DevVoltFixtureSystemVoltage, PermanentHighVoltageNotificationForMultipleClients)
{
	OSAL_tIODescriptor                   hIODescVoltDriver_2          = OSAL_ERROR;
	OSAL_tEventHandle                    hEventHandle_2               = OSAL_C_INVALID_HANDLE;
	OSAL_tEventMask                      rEventMaskResult             = 0;
	DEV_VOLT_trClientRegistration        rClientRegistration_2        = { 0 };
	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration_2 = { 0 };

	hIODescVoltDriver_2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

	if (hIODescVoltDriver_2 == OSAL_ERROR) {
		ADD_FAILURE();
		return;
	}

	if (OSAL_s32IOControl(
		hIODescVoltDriver_2,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_CLIENT,
		(intptr_t)&rClientRegistration_2) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_register_client; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventOpen(
		rClientRegistration_2.szNotificationEventName,
		&hEventHandle_2) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_event_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	rSystemVoltageRegistration_2.u32ClientId              = rClientRegistration_2.u32ClientId;
	rSystemVoltageRegistration_2.u32VoltageIndicationMask =
		(DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
		 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
		 DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
		 DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  );

	if (OSAL_s32IOControl(
		hIODescVoltDriver_2,
		OSAL_C_S32_IOCTRL_VOLT_REGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
		(intptr_t)&rSystemVoltageRegistration_2) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_register_system_voltage; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	m_au8TraceBuffer[0] = 0x05; // Command length
	m_au8TraceBuffer[1] = 0x83; // DEV_VOLT_SIMULATE_COMBINED_VOLTAGE_CHANGES
	m_au8TraceBuffer[2] = 0x30; // 0x00000030 => DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_STATE   = 0x00000010
	m_au8TraceBuffer[3] = 0x00; //               DEV_VOLT_C_U32_BIT_MASK_HIGH_VOLTAGE_CHANGED = 0x00000020
	m_au8TraceBuffer[4] = 0x00; // 
	m_au8TraceBuffer[5] = 0x00; // 

	if (OSAL_s32IOControl(
		m_hIODescVoltDriver, 
		OSAL_C_S32_IOCTRL_VOLT_INJECT_TTFIS_COMMAND,
		(intptr_t)m_au8TraceBuffer) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	OSAL_s32ThreadWait(DEV_VOLT_CONF_C_U32_PERMANENT_HIGH_VOLTAGE_DURATION_MS);

	if (OSAL_s32EventWait(
		m_hEventHandle,
		DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_HIGH_VOLTAGE,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	if (OSAL_s32EventWait(
		hEventHandle_2,
		DEV_VOLT_C_U32_EVENT_MASK_PERMANENT_HIGH_VOLTAGE,
		OSAL_EN_EVENTMASK_OR, 
		OSAL_C_TIMEOUT_NOBLOCKING,
		&rEventMaskResult) == OSAL_ERROR) {
			ADD_FAILURE();
			goto error_out; /*lint !e801, authorized LINT-deactivation #<71> */
	}

error_out:

	if (OSAL_s32IOControl(
		hIODescVoltDriver_2,
		OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_SYSTEM_VOLTAGE_NOTIFICATION,
		(intptr_t)rSystemVoltageRegistration_2.u32ClientId) == OSAL_ERROR)
			ADD_FAILURE();

error_register_system_voltage:

	OSAL_s32EventClose(hEventHandle_2);

error_event_open:

	OSAL_s32IOControl(
		hIODescVoltDriver_2, 
		OSAL_C_S32_IOCTRL_VOLT_UNREGISTER_CLIENT,
		(intptr_t)rClientRegistration_2.u32ClientId);

error_register_client:

	OSAL_s32IOClose(hIODescVoltDriver_2);
}

/******************************************************************************/

