/*******************************************************************************
*
* FILE:         dev_volt_client_process.c
*
* SW-COMPONENT: Device Voltage
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Integration tests.
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim                          
*
*******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "../dev_volt_integration_test.h"

/******************************************************************************/
/*                                                                            */
/* PUBLIC FUNCTIONS                                                           */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Main function of the client-process which has the job to be the instance
* which opens the /dev/volt for the first time and thereby will become the
* creator process context of the /dev/volt.
*
* As this is the creator context of the /dev/volt it will finally unload
* the /dev/volt as a shared library.
*
*******************************************************************************/
void vStartApp(int argc, char **argv)
{
	(tVoid) argc;
	(tPVoid) argv;

	OSAL_tSemHandle    hSemClientProcessStart = OSAL_C_INVALID_HANDLE;
	OSAL_tSemHandle    hSemClientProcessStop  = OSAL_C_INVALID_HANDLE;
	OSAL_tIODescriptor rVoltIODescriptor      = OSAL_ERROR;

	if (OSAL_s32SemaphoreCreate(
		DEV_VOLT_TEST_C_STRING_SEM_CLIENT_PROCESS_STOP_NAME,
		&hSemClientProcessStop,
		(tU32) 0) == OSAL_ERROR)
		goto error_semaphore_create; /*lint !e801, authorized LINT-deactivation #<71> */

	if (OSAL_s32SemaphoreOpen(
		DEV_VOLT_TEST_C_STRING_SEM_CLIENT_PROCESS_START_NAME,
		&hSemClientProcessStart) == OSAL_ERROR)
		goto error_semaphore_open; /*lint !e801, authorized LINT-deactivation #<71> */

	rVoltIODescriptor = OSAL_IOOpen(OSAL_C_STRING_DEVICE_VOLT, OSAL_EN_READWRITE);

	if (rVoltIODescriptor == OSAL_ERROR) {
		(tVoid)OSAL_s32SemaphoreClose(hSemClientProcessStart);
		goto error_io_open; /*lint !e801, authorized LINT-deactivation #<71> */
	}

	(tVoid)OSAL_s32IOClose(rVoltIODescriptor);

	(tVoid)OSAL_s32SemaphorePost(hSemClientProcessStart);
	(tVoid)OSAL_s32SemaphoreClose(hSemClientProcessStart);

	(tVoid)OSAL_s32SemaphoreWait(
		hSemClientProcessStop,
		OSAL_C_TIMEOUT_FOREVER);

error_io_open:
error_semaphore_open:
	if (OSAL_s32SemaphoreClose(hSemClientProcessStop) == OSAL_OK)
		(tVoid)OSAL_s32SemaphoreDelete(DEV_VOLT_TEST_C_STRING_SEM_CLIENT_PROCESS_STOP_NAME);

error_semaphore_create:

	OSAL_vProcessExit();
}

/******************************************************************************/
