/*******************************************************************************
*
* FILE:         dev_volt_mock.h
* 
* SW-COMPONENT: Voltage Wakeup
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Mocks for unit testing
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_VOLT_MOCK_H_
#define _DEV_VOLT_MOCK_H_

#include "gmock/gmock.h"

#include "MockDelegatee.h"

#define DEV_VOLT_C_S32_OSAL_PROCESS_ID                                 0x00000100

class DevVoltMock : public MockDelegatee<DevVoltMock>
{
  public:
	static inline const char *GetMockName() { return "DevVoltMock"; }

	static DevVoltMock &GetDelegatee()
	{
		return MockDelegatee<DevVoltMock>::GetDelegatee();
	};

	/* GLIBC */
	MOCK_METHOD2(open_mock,             int (const char*, int));
	MOCK_METHOD1(close_mock,            int (int));

	/* LLD */
	MOCK_METHOD2(LLD_bIsTraceActive,    tBool(tU32, tU32));
	MOCK_METHOD4(LLD_vTrace,            tVoid(tU32, tU32, const void*, tU32));
};

#endif // _DEV_VOLT_MOCK_H_

/******************************************************************************/
