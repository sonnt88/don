/*******************************************************************************
*
* FILE:         dev_volt_mock.cpp
*
* SW-COMPONENT: Voltage Device
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Mocks for unit testing
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#include "system_types.h"
#include "system_definition.h"

#include "dev_volt_mock.h"

extern "C"
{
  /******************************************************************************/
  /*                                   GLIBC                                    */
  /******************************************************************************/

  int open_mock(const char *pathname, int flags)
  {
	return DevVoltMock::GetDelegatee().open_mock(pathname, flags);
  }

  int close_mock(int fd)
  {
	return DevVoltMock::GetDelegatee().close_mock(fd);
  }

  /******************************************************************************/
  /*                                    LLD                                     */
  /******************************************************************************/

  tBool LLD_bIsTraceActive(tU32 u32Class, tU32 u32Level)
  {
	return TRUE;
//  	return DevVoltMock::GetDelegatee().LLD_bIsTraceActive(u32Class, u32Level);
  } 

  tVoid LLD_vTrace(tU32 u32Class, tU32 u32Level, const void* pvData, tU32 u32Length)
  {
#if 0
	char* chData = (char*) pvData;

	if (chData[0] == 0x01) // 0x01 = DEV_VOLT_C_U8_TRACE_TYPE_STRING
		printf("LLD_vTrace : %s\n", &chData[1]);
#endif
//  	return DevVoltMock::GetDelegatee().LLD_vTrace(u32Class, u32Level, pvData, u32Length);
  } 

} // extern "C"
