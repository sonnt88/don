/*******************************************************************************
*
* FILE:         dev_volt_utest.h
* 
* SW-COMPONENT: Voltage Wakeup
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Unit tests
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_VOLT_UNIT_TEST_H_
#define _DEV_VOLT_UNIT_TEST_H_

class DevVoltTest : public ::testing::Test 
{
	public:
		DevVoltTest() {}
		virtual ~DevVoltTest() {}

	protected:
		virtual void SetUp();
		virtual void TearDown();

		OSAL_trProcessControlBlock m_rPCB;
		OSAL_trThreadControlBlock  m_rTCB;

		trGlobalData m_rGlobalData;

		OsalMock m_oOsalMock;
		DevVoltMock m_oDevVoltMock;
};

class DevVoltTestInternal : public DevVoltTest
{
	public:
		DevVoltTestInternal() {}
		virtual ~DevVoltTestInternal() {}

	protected:
		virtual void SetUp();
		virtual void TearDown();
};

#endif //_DEV_VOLT_UNIT_TEST_H

/******************************************************************************/
