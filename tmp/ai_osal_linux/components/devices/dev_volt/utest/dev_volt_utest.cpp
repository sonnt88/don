/*******************************************************************************
*
* FILE:         dev_volt_utest.cpp
*
* SW-COMPONENT: Voltage Device
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Unit tests
*
* AUTHOR:       CM-AI/ECO3-Kalms
*
* COPYRIGHT:    (c) 2014 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#include "gmock/gmock.h"
#include "gtest/gtest.h"

extern "C"
{
  #include "../dev_volt.c"
}

#include "dev_volt_mock.h"

#include "osal_if_mock.h"

#include "dev_volt_utest.h"

/******************************************************************************/

using ::testing::Return;
using ::testing::ReturnNull;
using ::testing::ReturnArg;
using ::testing::ReturnPointee;
using ::testing::SetArgPointee;
using ::testing::DoAll;
using ::testing::_;
using ::testing::Invoke;
using ::testing::WithArg;
using ::testing::WithArgs;
using ::testing::Pointee;

/******************************************************************************/

#define DEV_VOLT_C_S32_OSAL_PROCESS_ID                                0x00000100
#define DEV_VOLT_C_S32_OSAL_THREAD_ID                                 0x00000200

/******************************************************************************/

static tU32 g_u32OsalErrorCode = OSAL_E_NOERROR;

/******************************************************************************/

int main(int argc, char** argv)
{
	::testing::InitGoogleMock(&argc, argv);
	return RUN_ALL_TESTS();
}

/******************************************************************************/

OSAL_tThreadID My_OSAL_ThreadSpawn(const OSAL_trThreadAttribute* pcorAttr)
{
	g_rModuleData.u8SystemVoltageThreadState = DEV_VOLT_C_U8_THREAD_RUNNING;
	g_rModuleData.u8HighVoltageThreadState   = DEV_VOLT_C_U8_THREAD_RUNNING;
	g_rModuleData.u8UserVoltageThreadState   = DEV_VOLT_C_U8_THREAD_RUNNING;
	g_rModuleData.u8ClientThreadState        = DEV_VOLT_C_U8_THREAD_NOT_INSTALLED;

	return 0x00000001;
}

/* -------------------------------------------------------------------------- */

tVoid My_OSAL_vSetErrorCode(tU32 u32ErrorCode)
{
	g_u32OsalErrorCode = u32ErrorCode;
}

/* -------------------------------------------------------------------------- */

tU32 My_OSAL_u32ErrorCode(tVoid)
{
	return g_u32OsalErrorCode;
}

/******************************************************************************/

ACTION_P(My_OSAL_vSetErrorCode_Action, u32ErrorCode)
{
	My_OSAL_vSetErrorCode(u32ErrorCode);
}

/* -------------------------------------------------------------------------- */

ACTION_P(DevVoltTest_vSetThreadStates, u8ThreadState)
{
	g_rModuleData.u8SystemVoltageThreadState = u8ThreadState;
	g_rModuleData.u8HighVoltageThreadState   = u8ThreadState;
	g_rModuleData.u8UserVoltageThreadState   = u8ThreadState;
	g_rModuleData.u8ClientThreadState        = u8ThreadState;
}

/******************************************************************************/

void DevVoltTest::SetUp()
{
	m_rPCB.id          = DEV_VOLT_C_S32_OSAL_PROCESS_ID;
	m_rPCB.szName      = (tString)"dev_volt_utest_process";
	m_rPCB.startTime   = 0;
	m_rPCB.runningTime = 0;

	m_rTCB.id          = DEV_VOLT_C_S32_OSAL_THREAD_ID;
	m_rTCB.szName      = (tString)"dev_volt_utest_thread";

	memset(
		&m_rGlobalData,
		0,
		sizeof(m_rGlobalData));

	g_rModuleData.prGlobalData = &m_rGlobalData;

	// Expect calls for m_oOsalMock

	EXPECT_CALL(m_oOsalMock, ClockGetElapsedTime())
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oOsalMock, ProcessWhoAmI())
		.WillRepeatedly(Return(DEV_VOLT_C_S32_OSAL_PROCESS_ID));

	EXPECT_CALL(m_oOsalMock, s32ProcessControlBlock(_, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(m_rPCB), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreCreate(_, _, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SharedMemoryClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SharedMemoryDelete(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreOpen(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreDelete(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SemaphorePost(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, SharedMemoryCreate(_, _, _))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, pvSharedMemoryMap(_, _, _, _))
		.WillRepeatedly(Return((tVoid*)&m_rGlobalData));

	EXPECT_CALL(m_oOsalMock, s32SharedMemoryUnmap(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, SharedMemoryOpen(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, IOOpen(_, _))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, s32IOClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32IORead(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32IOWrite(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32SaveNPrintFormat(_, _, _))
		.WillRepeatedly(Return(1));

	EXPECT_CALL(m_oOsalMock, szSaveStringNCopy(_, _, _))
		.WillRepeatedly(WithArgs<0,1,2>(Invoke(strncpy)));

	EXPECT_CALL(m_oOsalMock, s32EventCreate(_, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(1), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32EventOpen(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32EventClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32EventDelete(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32EventPost(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, coszErrorText(_))
		.WillRepeatedly(Return("DUMMY ERROR STRING"));

	EXPECT_CALL(m_oOsalMock, s32MessageQueueCreate(_, _, _, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32MessageQueueClose(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32MessageQueueDelete(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32MessageQueueOpen(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32MessageQueuePost(_, _, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, ThreadSpawn(_))
		.WillRepeatedly(Invoke(My_OSAL_ThreadSpawn));

	EXPECT_CALL(m_oOsalMock, ThreadWhoAmI())
		.WillRepeatedly(Return(DEV_VOLT_C_S32_OSAL_THREAD_ID));

	EXPECT_CALL(m_oOsalMock, s32ThreadControlBlock(_, _))
		.WillRepeatedly(DoAll(SetArgPointee<1>(m_rTCB), Return(OSAL_OK)));

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32IOControl(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, vSetErrorCode(_))
		.WillRepeatedly(WithArg<0>(Invoke(My_OSAL_vSetErrorCode)));

	EXPECT_CALL(m_oOsalMock, u32ErrorCode())
		.WillRepeatedly(Invoke(My_OSAL_u32ErrorCode));

	EXPECT_CALL(m_oOsalMock, s32ThreadWait(_))
		.WillRepeatedly(Return(OSAL_OK));

	EXPECT_CALL(m_oOsalMock, s32TimerCreate(_, _, _))
		.WillRepeatedly(Return(OSAL_OK));

	// Expect calls for m_oDevVoltMock

	EXPECT_CALL(m_oDevVoltMock, open_mock(_, _))
		.WillRepeatedly(Return(0));

	EXPECT_CALL(m_oDevVoltMock, close_mock(_))
		.WillRepeatedly(Return(0));
}

/* -------------------------------------------------------------------------- */

void DevVoltTest::TearDown()
{


}

/* -------------------------------------------------------------------------- */

void DevVoltTestInternal::SetUp()
{
	DevVoltTest::SetUp();

	DEV_VOLT_OsalIO_u32Init();
}

/* -------------------------------------------------------------------------- */

void DevVoltTestInternal::TearDown()
{
	EXPECT_CALL(m_oOsalMock, s32EventPost(_, DEV_VOLT_C_U32_EVENT_MASK_STOP_THREAD, _))
		.WillRepeatedly(DoAll(DevVoltTest_vSetThreadStates(DEV_VOLT_C_U8_THREAD_SHUTTING_DOWN), Return(OSAL_OK)));

	DEV_VOLT_OsalIO_u32Deinit();

	DevVoltTest::TearDown();
}

/******************************************************************************/
/******************************************************************************/
/*                                DevVoltTest                                 */
/******************************************************************************/
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/* DEV_VOLT_OsalIO_u32Init()                                                  */
/*----------------------------------------------------------------------------*/

TEST_F(DevVoltTest, DEV_VOLT_OsalIO_u32Init__OSAL_E_NOERROR)
{
	EXPECT_EQ(OSAL_E_NOERROR, DEV_VOLT_OsalIO_u32Init());
}

/*----------------------------------------------------------------------------*/
/* DEV_VOLT_OsalIO_u32Open()                                                  */
/*----------------------------------------------------------------------------*/

TEST_F(DevVoltTest, DEV_VOLT_OsalIO_u32Open__OSAL_E_NOERROR)
{
	DEV_VOLT_OsalIO_u32Init();

	EXPECT_EQ(OSAL_E_NOERROR, DEV_VOLT_OsalIO_u32Open());
}

TEST_F(DevVoltTest, DEV_VOLT_u32Open__OSAL_E_NOTINITIALIZED)
{
	EXPECT_CALL(m_oOsalMock, s32SemaphoreOpen(DEV_VOLT_C_STRING_SEM_DATA_NAME, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_DOESNOTEXIST), Return(OSAL_ERROR)));

	EXPECT_EQ(OSAL_E_NOTINITIALIZED, DEV_VOLT_OsalIO_u32Open());
}

/******************************************************************************/
/******************************************************************************/
/*                             DevVoltTestInternal                            */
/******************************************************************************/
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/* DEV_VOLT_u32RegisterClient()                                               */
/*----------------------------------------------------------------------------*/

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterClient__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration rClientRegistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_VOLT_u32RegisterClient(
			&rClientRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_VOLT_u32RegisterSystemVoltage()                                        */
/*----------------------------------------------------------------------------*/

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterSystemVoltage__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rSystemVoltageRegistration.u32ClientId              = rClientRegistration.u32ClientId;
	rSystemVoltageRegistration.u32VoltageIndicationMask = DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  ;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_VOLT_u32RegisterSystemVoltage(
			&rSystemVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		rSystemVoltageRegistration.u32VoltageIndicationMask,
		g_rModuleData.prGlobalData->arClientSpecificData[0].u32SystemVoltageIndicationMask);

	EXPECT_EQ(
		DEV_VOLT_EN_VOLTAGE_STATE_OPERATING,
		rSystemVoltageRegistration.u32CurrentSystemVoltageState);

	EXPECT_EQ(
		0,
		rSystemVoltageRegistration.u8PermHighVoltageCounter);

	EXPECT_EQ(
		0,
		rSystemVoltageRegistration.u8PermCriticalHighVoltageCounter);
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterSystemVoltage__InvalidClientId__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;

	rSystemVoltageRegistration.u32ClientId = DEV_VOLT_C_U32_CLIENT_ID_INVALID;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32RegisterSystemVoltage(
			&rSystemVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterSystemVoltage__InvalidVoltageIndicationMask__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;

	rSystemVoltageRegistration.u32ClientId              = 1;
	rSystemVoltageRegistration.u32VoltageIndicationMask = 0x000000FF;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32RegisterSystemVoltage(
			&rSystemVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterSystemVoltage__PermanentLockFailure__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;

	rSystemVoltageRegistration.u32ClientId              = 1;
	rSystemVoltageRegistration.u32VoltageIndicationMask = DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  ;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_VOLT_u32RegisterSystemVoltage(
			&rSystemVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterSystemVoltage__UnknownClientId__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rSystemVoltageRegistration.u32ClientId              = 2;
	rSystemVoltageRegistration.u32VoltageIndicationMask = DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  ;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32RegisterSystemVoltage(
			&rSystemVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}


/*----------------------------------------------------------------------------*/
/* DEV_VOLT_u32UnregisterSystemVoltage()                                      */
/*----------------------------------------------------------------------------*/

TEST_F(DevVoltTestInternal, DEV_VOLT_u32UnregisterSystemVoltage__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rSystemVoltageRegistration.u32ClientId              = rClientRegistration.u32ClientId;
	rSystemVoltageRegistration.u32VoltageIndicationMask = DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  ;

	if (DEV_VOLT_u32RegisterSystemVoltage(
		&rSystemVoltageRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_VOLT_u32UnregisterSystemVoltage(
			rSystemVoltageRegistration.u32ClientId,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		0,
		g_rModuleData.prGlobalData->arClientSpecificData[0].u32SystemVoltageIndicationMask);
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32UnregisterSystemVoltage__PermanentLockFailure__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_VOLT_u32UnregisterSystemVoltage(
			1,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32UnregisterSystemVoltage__UnknownClientId__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trSystemVoltageRegistration rSystemVoltageRegistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rSystemVoltageRegistration.u32ClientId              = rClientRegistration.u32ClientId;
	rSystemVoltageRegistration.u32VoltageIndicationMask = DEV_VOLT_C_U32_BIT_MASK_INDICATE_LOW_VOLTAGE           |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_LOW_VOLTAGE  |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_HIGH_VOLTAGE          |
							      DEV_VOLT_C_U32_BIT_MASK_INDICATE_CRITICAL_HIGH_VOLTAGE  ;

	if (DEV_VOLT_u32RegisterSystemVoltage(
		&rSystemVoltageRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32UnregisterSystemVoltage(
			2,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_VOLT_u32RegisterUserVoltage()                                          */
/*----------------------------------------------------------------------------*/

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__WithHysteresisGreaterThanMinimum__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration      rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	g_rModuleData.prGlobalData->u16BoardVoltageAdc = DEV_VOLT_u16ConvertBoardVoltageMvToAdc(
		14000,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		rUserVoltageRegistration.u16HysteresisMv,
		g_rModuleData.prGlobalData->arClientSpecificData[0].arUserVoltage[0].u16HysteresisMv);

	EXPECT_EQ(
		DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED,
		rUserVoltageRegistration.u8UserVoltageState);
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__WithHysteresisLesserThanMinimum__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration      rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	g_rModuleData.prGlobalData->u16BoardVoltageAdc = DEV_VOLT_u16ConvertBoardVoltageMvToAdc(
		14000,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV - 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV,
		g_rModuleData.prGlobalData->arClientSpecificData[0].arUserVoltage[0].u16HysteresisMv);

	EXPECT_EQ(
		DEV_VOLT_C_U8_USER_VOLTAGE_LEVEL_EXCEEDED,
		rUserVoltageRegistration.u8UserVoltageState);
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__InvalidClientId__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	rUserVoltageRegistration.u32ClientId           = DEV_VOLT_C_U32_CLIENT_ID_INVALID;
	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__InvalidUserVoltageLevelMin__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	rUserVoltageRegistration.u16UserVoltageLevelMv = 0;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__InvalidLevelCrossDirection__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	rUserVoltageRegistration.u8LevelCrossDirection = 0;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__InvalidHysteresisValue__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = 9000;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__GetBoardVoltageFailed__OSAL_E_DOESNOTEXIST)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	g_rModuleData.prGlobalData->u32GetBoardVoltageResult = OSAL_E_DOESNOTEXIST;

	EXPECT_EQ(
		OSAL_E_DOESNOTEXIST,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__InvalidUserVoltageLevelMax__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	rUserVoltageRegistration.u16UserVoltageLevelMv = 30000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_UPWARD;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__PermanentLockFailure__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__UnknownClientId__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	rUserVoltageRegistration.u32ClientId           = 1;
	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__RegisterForSameVoltageLevelTwice__OSAL_E_ALREADYEXISTS)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration      rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	g_rModuleData.prGlobalData->u16BoardVoltageAdc = DEV_VOLT_u16ConvertBoardVoltageMvToAdc(
		14000,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	if (DEV_VOLT_u32RegisterUserVoltage(
		&rUserVoltageRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	EXPECT_EQ(
		OSAL_E_ALREADYEXISTS,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32RegisterUserVoltage__RegisterTooManyUserVoltages__OSAL_E_MAXFILES)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration      rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration rUserVoltageRegistration;
	tU8                                u8UserVoltageRegistrationCount;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	g_rModuleData.prGlobalData->u16BoardVoltageAdc = DEV_VOLT_u16ConvertBoardVoltageMvToAdc(
		14000,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	for (u8UserVoltageRegistrationCount = 0; 
	     u8UserVoltageRegistrationCount < DEV_VOLT_CONF_C_U8_MAX_NUMBER_OF_USER_VOLTAGES;
	     u8UserVoltageRegistrationCount++) {
		rUserVoltageRegistration.u16UserVoltageLevelMv = 9000 + u8UserVoltageRegistrationCount;

		if (DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess) != OSAL_E_NOERROR) {
				ADD_FAILURE();
				return;
		}
	}

	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000 + u8UserVoltageRegistrationCount;

	EXPECT_EQ(
		OSAL_E_MAXFILES,
		DEV_VOLT_u32RegisterUserVoltage(
			&rUserVoltageRegistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/*----------------------------------------------------------------------------*/
/* DEV_VOLT_u32UnregisterUserVoltage()                                        */
/*----------------------------------------------------------------------------*/

TEST_F(DevVoltTestInternal, DEV_VOLT_u32UnregisterUserVoltage__OSAL_E_NOERROR)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration   rUserVoltageRegistration;
	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	g_rModuleData.prGlobalData->u16BoardVoltageAdc = DEV_VOLT_u16ConvertBoardVoltageMvToAdc(
		14000,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	if (DEV_VOLT_u32RegisterUserVoltage(
		&rUserVoltageRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rUserVoltageDeregistration.u32ClientId           = rUserVoltageRegistration.u32ClientId;
	rUserVoltageDeregistration.u16UserVoltageLevelMv = rUserVoltageRegistration.u16UserVoltageLevelMv;

	EXPECT_EQ(
		OSAL_E_NOERROR,
		DEV_VOLT_u32UnregisterUserVoltage(
			&rUserVoltageDeregistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));

	EXPECT_EQ(
		0,
		g_rModuleData.prGlobalData->arClientSpecificData[0].arUserVoltage[0].u16LevelMv);
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32UnregisterUserVoltage__PermanentLockFailure__OSAL_E_TIMEOUT)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration;

	EXPECT_CALL(m_oOsalMock, s32SemaphoreWait(_, _))
		.WillRepeatedly(DoAll(My_OSAL_vSetErrorCode_Action(OSAL_E_TIMEOUT), Return(OSAL_ERROR)));

	EXPECT_EQ(
		OSAL_E_TIMEOUT,
		DEV_VOLT_u32UnregisterUserVoltage(
			&rUserVoltageDeregistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32UnregisterUserVoltage__UnknownClientId__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration   rUserVoltageRegistration;
	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	g_rModuleData.prGlobalData->u16BoardVoltageAdc = DEV_VOLT_u16ConvertBoardVoltageMvToAdc(
		14000,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	if (DEV_VOLT_u32RegisterUserVoltage(
		&rUserVoltageRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rUserVoltageDeregistration.u32ClientId = 2;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32UnregisterUserVoltage(
			&rUserVoltageDeregistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

TEST_F(DevVoltTestInternal, DEV_VOLT_u32UnregisterUserVoltage__InvalidVoltageLevel__OSAL_E_INVALIDVALUE)
{
	OSAL_tSemHandle hSemDataAccess = 1;

	DEV_VOLT_trClientRegistration        rClientRegistration;
	DEV_VOLT_trUserVoltageRegistration   rUserVoltageRegistration;
	DEV_VOLT_trUserVoltageDeregistration rUserVoltageDeregistration;

	if (DEV_VOLT_OsalIO_u32Open() != OSAL_E_NOERROR) {
		ADD_FAILURE();
		return;
	}

	if (DEV_VOLT_u32RegisterClient(
		&rClientRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	g_rModuleData.prGlobalData->u16BoardVoltageAdc = DEV_VOLT_u16ConvertBoardVoltageMvToAdc(
		14000,
		g_rModuleData.prGlobalData,
		hSemDataAccess);

	rUserVoltageRegistration.u32ClientId           = rClientRegistration.u32ClientId;
	rUserVoltageRegistration.u16UserVoltageLevelMv = 9000;
	rUserVoltageRegistration.u16HysteresisMv       = DEV_VOLT_CONF_C_U16_DEFAULT_USER_VOLTAGE_HYSTERISIS_MV + 1;
	rUserVoltageRegistration.u8LevelCrossDirection = DEV_VOLT_C_U8_USER_VOLTAGE_CROSS_DIRECTION_DOWNWARD;

	if (DEV_VOLT_u32RegisterUserVoltage(
		&rUserVoltageRegistration,
		g_rModuleData.prGlobalData,
		hSemDataAccess) != OSAL_E_NOERROR) {
			ADD_FAILURE();
			return;
	}

	rUserVoltageDeregistration.u32ClientId           = rUserVoltageRegistration.u32ClientId;
	rUserVoltageDeregistration.u16UserVoltageLevelMv = 10000;

	EXPECT_EQ(
		OSAL_E_INVALIDVALUE,
		DEV_VOLT_u32UnregisterUserVoltage(
			&rUserVoltageDeregistration,
			g_rModuleData.prGlobalData,
			hSemDataAccess));
}

/******************************************************************************/
