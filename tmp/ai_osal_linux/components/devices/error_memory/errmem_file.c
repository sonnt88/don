#include "OsalConf.h"
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"


#include "errmem.h"

const char  default_devname[]="/tmp/OSAL/errmem"; //OSAL_C_STRING_ERRMEM_AS_FILE;
const char  default_backup_devname[]="/tmp/OSAL/errmem_backup"; //OSAL_C_STRING_ERRMEM_AS_FILE;
char        devname[64];

#define ERRMEM_MAX_FILE_SIZE        (20 * 1024 * 1024) /* 10 KB Max Errmem File */

/*************************************/
/* TODO: Only applies to 'default_devname' */
/* Overwriting using the registry is not supported */
tVoid HandleMaxErrMemFilesize(tS32 u32Size){

  struct stat trFileStatInfo;
  int nError = stat(default_devname, &trFileStatInfo);
  if(nError >= 0){
    if(u32Size == 0)u32Size = ERRMEM_MAX_FILE_SIZE;
    if(u32Size < trFileStatInfo.st_size){
      TraceString("ERRMEM: Current Errmem Filesize = %d KB --> rename !", trFileStatInfo.st_size >> 10);

      nError = rename(default_devname, default_backup_devname);
      if(nError < 0){
      TraceString("ERRMEM: rename error:  (errno:%d)", errno);
      }

    }    
  }else{
      TraceString("ERRMEM: stat error:  (errno:%d)", errno);
  }
  return;
}


tS32 ERRMEM_S32IOOpen_impl(tS32 s32Id, tCString szName,
                      OSAL_tenAccess enAccess, tU32 *pu32FD,
                      tU16 app_id)
{
    int                 fd;
    (void)app_id;
    (void)s32Id;
    (void)szName;

    *pu32FD = 0;
    if((enAccess == 0)||(enAccess > OSAL_EN_READWRITE))
    {
       return(OSAL_E_INVALIDVALUE); 
    }
    fd = open(default_devname, O_CREAT | O_RDWR | O_APPEND, S_IRUSR|S_IWUSR);
    if (fd > 0)
    {
		*pu32FD = (tU32)fd;
		return OSAL_E_NOERROR;
    }
    else
    {
        return OSAL_E_DOESNOTEXIST;
    }
}

tS32 ERRMEM_s32IOWrite_impl(tS32 s32ID, tU32 u32FD, tPCS8 buffer, tU32 size)
{
    int ret;
    (void) s32ID;
    ret = write((int)u32FD, buffer, size);
	if (ret > 0)
	{
		fsync((int)u32FD);
	}
	return ret;
}

tS32 ERRMEM_s32IOControl_impl(tS32 s32ID, tU32 u32fd, tS32 io_func, tS32 param)
{
	(void)s32ID;
	(void)u32fd;
	(void)param;

	tS32 rc = OSAL_E_DOESNOTEXIST;
	(void) s32ID;
	char errstr[64];
    switch (io_func)
    {
        case OSAL_C_S32_IOCTRL_ERRMEM_CLEAR:
        	rc = ftruncate((int)u32fd, 0);
        	if( rc == -1)
        	{
        	    snprintf(errstr, sizeof(errstr),"OSAL_EM_ERASE error! (errno:%d)", errno);
        	    TraceString(errstr);
        	    rc = OSAL_E_IOERROR;
        	} else {
        		rc = OSAL_E_NOERROR;
        	}
        	break;
        default:
        	break;
    }
	return rc;
}
#else
void HandleMaxErrMemFilesize(int u32Size)
{
  (void)u32Size;
  return;
}

#endif // #ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE

