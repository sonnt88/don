#include "OsalConf.h"
#ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "ostrace.h"

#include "errmem.h"
#include "adit-components/errmem_lib.h"




extern int  errmem_fd;
static tS32 ReadHandle = -1;

tS32 ERRMEM_S32IOOpen(tS32 s32Id, tCString szName,OSAL_tenAccess enAccess, uintptr_t *pu32FD,tU16 app_id)
{
    (void)app_id;
    (void)s32Id;
    (void)szName;
    (void)pu32FD;
    if((enAccess == 0)||(enAccess > OSAL_EN_READWRITE))
    {
       return(OSAL_E_INVALIDVALUE); 
    }
    if(errmem_fd == -1)
    {
      errmem_fd = open("/dev/errmem", O_WRONLY,OSAL_ACCESS_RIGTHS);
    }
    if(errmem_fd < 0)
    {
       return u32ConvertErrorCore(errno);
    }
    else
    {
      return OSAL_E_NOERROR;
    }
}

tS32 ERRMEM_S32IOClose(tS32 s32ID, uintptr_t u32FD)
{
   ((void)s32ID);
   ((void)u32FD);
   return OSAL_E_NOERROR;
}

tS32 ERRMEM_s32IOWrite(tS32 s32ID, uintptr_t u32FD, tPCS8 buffer,tU32 size, uintptr_t *ret_size)
{
   (void)s32ID;
   (void)u32FD;
#ifdef OSAL_ERRMEM_TIMESTAMP
   vWriteUtcTime(errmem_fd);
#endif
   struct errmem_message  rErrMem;

   if(size < sizeof(trErrmemEntry))
   {
      TraceString("size < sizeof(trErrmemEntry) ");
      return OSAL_E_INVALIDVALUE;
   }
   memset(&rErrMem,0,sizeof(rErrMem));
   trErrmemEntry* pOsalEntry = (trErrmemEntry*)buffer;/*lint !e826 */ /* used buffer is always size of trErrmemEntry*/

   rErrMem.type = ERRMEM_TYPE_TRACE;
   rErrMem.message[0] = 0;

 /*  if(pOsalEntry->u16Entry)
   {
        rErrMem.type = ERRMEM_TYPE_TRACE;
        rErrMem.message[0] = 0;
   }
   else
   {
        rErrMem.type = ERRMEM_TYPE_ASCII;
        rErrMem.message[0] = 1;
   }*/
   rErrMem.length = pOsalEntry->u16EntryLength;
   if(pOsalEntry->u16EntryLength == ERRMEM_MAX_ENTRY_LENGTH)
   {
       pOsalEntry->u16EntryLength = pOsalEntry->u16EntryLength-1;
   }
   memcpy(&rErrMem.message[1],pOsalEntry->au8EntryData,pOsalEntry->u16EntryLength);

   
   /*
     Changed the len from 'sizeof(rErrMem.message)' to 'pOsalEntry->u16EntryLength+1'
      to transmitt the real required size.
      Trace TTFIS TRC rules gets confused if teh message contains tooo much trailing '00's
   */
   *ret_size = write(errmem_fd,(char*)&rErrMem.message,pOsalEntry->u16EntryLength+1);
   if((int)*ret_size == -1)
   {
      *ret_size = 0;
      return u32ConvertErrorCore(errno);
   }
   return (tS32)size;
}


tS32 ERRMEM_s32IORead(tS32 s32ID, uintptr_t u32FD, tPS8 buffer, tU32 size, uintptr_t *ret_size)
{
   (void)s32ID;
   (void)u32FD;
   struct errmem_message  rErrMem;
   tS32 s32Ret;
   trErrmemEntry* pEntry;
//   tBool bIgnore = FALSE;

//   TraceString("ERRMEM_s32IORead start");
   if((buffer == NULL)||(size < sizeof(trErrmemEntry)))
   {
       TraceString("ReadBackend parameter error  buffer:0x%x , size:%d", buffer,size);
       return (tS32)OSAL_E_INVALIDVALUE;
   }
   if(ReadHandle == -1)
   {
      TraceString("errmem_backend_open_session start");
      ReadHandle = errmem_backend_open_session();
      if(ReadHandle < 0)
      {
         TraceString("errmem_backend_open_session failed %d Check the Daemon", ReadHandle);
         ReadHandle = -1;
         return (tS32)u32ConvertErrorCore(errno);
      }
      if(pOsalData->u32ErrmemBE == 0)pOsalData->u32ErrmemBE = 1;
      if((s32Ret = errmem_backend_set_storage(ReadHandle,pOsalData->u32ErrmemBE)) < 0)
      {
         close(ReadHandle);
         ReadHandle = -1;
         TraceString("errmem_backend_set_storage %d failed error:%d",s32Ret);
         return (tS32)OSAL_E_UNKNOWN;
      }
   }

   while(1)
   {
      s32Ret = errmem_backend_read(ReadHandle,&rErrMem);
      if(s32Ret < 0)
      {
         TraceString("ReadBackend failed %d", s32Ret);
         close(ReadHandle);
         ReadHandle = -1;
         return (tS32)u32ConvertErrorCore(s32Ret);
      }
      else
      {
         pEntry = (trErrmemEntry*)buffer; /*lint !e826 */ /* used buffer is always size of trErrmemEntry*/
   //   TraceString("ReadBackend Ret:%d Type:%d  Size:%d %s",s32Ret,rErrMem.type,rErrMem.length,rErrMem.message);

         /* finish session when last entry received */
         if(s32Ret > 0)
         {
            if(s32Ret == 1)
            {
                TraceString("errmem_backend_read last entry reached ");
            }
            if(s32Ret == 2)
            {
               TraceString("errmem_backend_read errmem content invalidated");
            }
            if((s32Ret = errmem_backend_close_session(ReadHandle)) == -1)
            {
               return (tS32)u32ConvertErrorCore(s32Ret);
            }
            TraceString("errmem_backend_close_session ");
            ReadHandle = -1;
            return (tS32)OSAL_E_IOERROR;
         }

   /*      if(rErrMem.internal.seqnum == 0)
         {
            if(strstr(rErrMem.message,"MESSAGE CRC ERROR ====="))
            {
                bIgnore = TRUE;
            }
         }

         if(bIgnore == FALSE)
         {*/
            tU64 time_local;
            time_t current_time;
            struct tm result;

            time_local = ((tU64)(rErrMem.internal.local_clock[1])) << 32
                        | (tU64)(rErrMem.internal.local_clock[0]);
            time_local /= 1000000000UL;
            current_time = (time_t)(time_local);
            localtime_r(&current_time, &result);
            pEntry->rEntryTime.s32Second         = result.tm_sec;
            pEntry->rEntryTime.s32Minute         = result.tm_min;
            pEntry->rEntryTime.s32Hour           = result.tm_hour;
            pEntry->rEntryTime.s32Day            = result.tm_mday;
            pEntry->rEntryTime.s32Month          = result.tm_mon+1;
            pEntry->rEntryTime.s32Year           = result.tm_year;
            pEntry->rEntryTime.s32Weekday        = result.tm_wday;
            pEntry->rEntryTime.s32Yearday        = result.tm_yday;
            pEntry->rEntryTime.s32Daylightsaving = result.tm_isdst; /* do we have locale info? */

            if(rErrMem.type == ERRMEM_TYPE_TRACE) 
            {
                  pEntry->u16Entry       = 0x4c5a;
                  pEntry->u16EntryLength = (unsigned short)(rErrMem.length-1);
                  pEntry->eEntryType     = eErrmemEntryNormal;
                  memcpy(&pEntry->au8EntryData[0],&rErrMem.message[1],rErrMem.length-1);
                  break;
            }
            else
            {
               int len;
               if(rErrMem.length >= ERRMEM_MAX_ENTRY_LENGTH-4)
               {
                  TraceString("Entry is truncated from %d to %d Bytes",rErrMem.length,ERRMEM_MAX_ENTRY_LENGTH-3);
                  rErrMem.length=ERRMEM_MAX_ENTRY_LENGTH - 4;
               }
               len = rErrMem.length;
               if (rErrMem.message[len-1] == '\n') 
               {
                  --len;
               }
               rErrMem.message[len] = 0;

               pEntry->u16Entry = 0x4c5a;
               pEntry->u16EntryLength  = (unsigned short)(len+3);
               pEntry->eEntryType      = eErrmemEntryNormal;
               pEntry->au8EntryData[0] =  ((int)TR_CLASS_EXCEPTION) & 0x000000FF;
               pEntry->au8EntryData[1] =  0; /*(((int)TR_CLASS_EXCEPTION) >> 8) & 0x000000FF; */
               pEntry->au8EntryData[2] =  255;
               memcpy(&pEntry->au8EntryData[3],&rErrMem.message[0],(size_t)len);
               break;
            }
     /*    }//if(bIgnore == FALSE)
         else
         {
            if(rErrMem.internal.seqnum != 0)
            {
               bIgnore = FALSE;
            }
         }//if(bIgnore == FALSE)*/
      }//if(s32Ret < 0)
   }//end while 
   pEntry->u16EntryCount   = 0;
   *ret_size = sizeof(trErrmemEntry);
   return (tS32)size;
}

tS32 ERRMEM_s32IOControl(tS32 s32ID, uintptr_t u32fd, tS32 io_func, intptr_t param)
{
   (void)s32ID;
   (void)u32fd;

   tU32 u32Ret = OSAL_E_NOERROR;
   uintptr_t s32Val = *((uintptr_t*)param);

   int Handle = errmem_backend_open_session();
   if(io_func == OSAL_C_S32_IOCTRL_ERRMEM_CLEAR)
   {
      if(errmem_backend_erase(Handle,pOsalData->u32ErrmemBE) != 0)
      {
         TraceString("errmem_backend_erase failed error:%d",errno);
         u32Ret = u32ConvertErrorCore(errno);
      }
   }
   else if(io_func == OSAL_C_S32_IOCTRL_ERRMEM_SET_BE)
   {
          pOsalData->u32ErrmemBE = s32Val;
   }
   else if(io_func == OSAL_C_S32_IOCTRL_ERRMEM_STOP_BE)
   {
          errmem_backend_stop(Handle);
   }
   else if(io_func == OSAL_C_S32_IOCTRL_ERRMEM_SET_LOGLEVEL)
   {
      if(errmem_fd != -1)
      {
         if(ioctl(errmem_fd, IOCTL_ERRMEM_SET_KERNEL_MSG_LEVEL, &s32Val) ==  -1)
         {
            TraceString("OSAL_C_S32_IOCTRL_ERRMEM_SET_LOGLEVEL Errmem ioctl failed errno %d",errno);
         }
      }
   }
   else if(io_func == OSAL_C_S32_IOCTRL_ERRMEM_FLUSH)
   {
      if(errmem_fd != -1)
      {
        if(ioctl(errmem_fd, IOCTL_ERRMEM_FLUSH, &s32Val) ==  -1)
        {
            TraceString("IOCTL_ERRMEM_FLUSH Errmem ioctl failed errno %d",errno);
        }
      }
   }     

   if(errmem_backend_close_session(Handle) == -1)
   {
      u32Ret = u32ConvertErrorCore(errno);
   }
   return (tS32)u32Ret;
}

#endif // #ifndef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE


