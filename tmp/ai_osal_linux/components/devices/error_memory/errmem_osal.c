#include "OsalConf.h"

#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "errmem.h"

extern tS32 ERRMEM_S32IOOpen_impl(tS32 s32Id, tCString szName,
                      OSAL_tenAccess enAccess, uintptr_t *pu32FD,
                      tU16 app_id);

tS32 ERRMEM_S32IOOpen(tS32 s32Id, tCString szName, 
                      OSAL_tenAccess enAccess, uintptr_t *pu32FD,
                      tU16 app_id)
{
	return ERRMEM_S32IOOpen_impl(s32Id, szName, enAccess, pu32FD, app_id);
}

tS32 ERRMEM_S32IOClose(tS32 s32ID, uintptr_t u32FD)
{
   (void) s32ID;
   tS32 n = close((int)u32FD);

   if(n == 0)
      return OSAL_E_NOERROR;
   
   return OSAL_E_UNKNOWN;
}

tS32 ERRMEM_s32IORead(tS32 s32ID, uintptr_t u32FD, tPS8 buffer,
                      tU32 size, uintptr_t *ret_size)
{
    (void) s32ID;
    tS32 n = read((int)u32FD, buffer, size);
      if(n == 0) 		
      {
                return OSAL_E_IOERROR;
      }
      *ret_size = (uintptr_t)n;
     return n;
}


extern tS32 ERRMEM_s32IOWrite_impl(tS32 s32ID, tU32 u32FD, tPCS8 buffer, tU32 size);

tS32 ERRMEM_s32IOWrite(tS32 s32ID, uintptr_t u32FD, tPCS8 buffer,
		tU32 size, uintptr_t *ret_size)
{
    (void) s32ID;
	tS32 n = ERRMEM_s32IOWrite_impl(s32ID, u32FD, buffer, size);
	*ret_size = (tU32)n;
	return n;
}

tS32 ERRMEM_s32IOControl_impl(tS32 s32ID, uintptr_t u32fd, tS32 io_func, intptr_t param);

tS32 ERRMEM_s32IOControl(tS32 s32ID, uintptr_t u32fd, tS32 io_func, intptr_t param)
{
    intptr_t *version;
    tS32 rc = OSAL_E_DOESNOTEXIST;

    switch (io_func)
    {
        case OSAL_C_S32_IOCTRL_VERSION:
            version = (intptr_t *)param;
            *version = 1;
            rc = OSAL_E_NOERROR;
            break;
        default:
        	rc = ERRMEM_s32IOControl_impl(s32ID, u32fd, io_func, param);
        	break;
    }
    return rc;
}
#endif