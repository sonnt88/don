/**********************************************************FileHeaderBegin******
*
* FILE:        dev_volt.c
*
* CREATED:     
*
* AUTHOR:      Suresh Dhandapani(RBEI/ECF5)
*
* DESCRIPTION: -This file implements the SPM volt driver for LSIM
*
* NOTES: -
*
* COPYRIGHT:  (c) 2012 Robert Bosch Engineering and Business solutions Limited
*
**********************************************************FileHeaderEnd*******/


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#include "osal_if.h"
#include "ostrace.h"
#include "Linux_osal.h"
#include "dev_volt.h"

/***************************MACRO definitions******************************************/


/*CVM state levels*/
#define DEV_VOLT_CVM_CRIT_LOW_VOLTAGE               0
#define DEV_VOLT_CVM_LOW_VOLTAGE                    1
#define DEV_VOLT_CVM_LOW_VOLTAGE_DELAY              2
#define DEV_VOLT_CVM_OPERATING_VOLTAGE              3
#define DEV_VOLT_CVM_HIGH_VOLTAGE                   4
#define DEV_VOLT_CVM_CRIT_HIGH_VOLTAGE              5

#define E_VOLT_SYSTEM_THRESHOLDS                    0 /* used to register with system   */
//#define E_VOLT_SYSTEM_CVM_THRESHOLDS                5 /* or cvm system levels           */
#define E_VOLT_FIRST_USER_THRESHOLD                 10 /* first user threshold id       */

#define EN_VOLT_EVVOLT_INVALID                      0 /* Invalid state used during registration */
#define EN_VOLT_EVVOLT_OVERRUN                      1 /* under-run of defined user range */
#define EN_VOLT_EVVOLT_UNDERRUN                     2 /* over-run of defined user range  */
#define EN_VOLT_EVVOLT_INRANGE                      3 /* inside the defined user range  */

#define DEV_VOLT_MSG_SIZE                           50
#define DEV_VOLT_INVALID_ID                         -1
#define DEV_VOLT_INITIALIZE                         0
#define DEV_VOLT_SEM_NAME                           "VOLT_SEM"
#define MSG_QUEUE_NAME                              "VOLTMSGQUE"
#define DEV_VOLT_THREAD_PRIO                        55

#define DEV_VOLT_CVM_LOW_VOLTAGE_DELAY_MS           2000 /* 2s delay after low voltage */

#define DEV_VOLT_VERYLOW                            3000
#define DEV_VOLT_CRITICALLOW_VALUE                  6000
#define DEV_VOLT_LOW_VALUE                          9000
#define DEV_VOLT_OPERATING_VALUE                    12000
#define DEV_VOLT_HIGH_VALUE                         13000
#define DEV_VOLT_CRITICALHIGH_VALUE                 15000

#define DEV_VOLT_LOWESTVOLT                         5500
#define DEV_VOLT_HIGHESTVOLT                        20000
#define DEV_VOLT_ADCREFCORRECTION                   3200

#define DEV_VOLT_CURRENT                            5
#define DEV_VOLT_DEACTIVATE_TASK_THREAD             0
#define DEV_VOLT_DONT_EXECUTE_TIMER                 0
#define DEV_VOLT_EXECUTE_TIMER                      1
#define DEV_VOLT_MASK_NOT_SET                       0
#define DEV_VOLT_TIMER_INTERVAL                     0
#define DEV_VOLT_TIMER_MSEC                         0
#define DEV_VOLT_SEM_INIT                           1
#define DEV_VOLT_ACTIVATE_TASK_THREAD               1
#define DEV_VOLT_FINAL_CLOSE                        1
#define DEV_VOLT_ERROR                              -1
#define TR_CLASS_DEV_VOLT                           TR_CLASS_DRV_SPM_CVM
#define DEV_VOLT_MQ_NORMAL_PRIORITY                 5
#define DEV_VOLT_MQ_HIGH_PRIORITY                   0
#define DEV_VOLT_MQ_TIMER_PRIORITY                  1

enum
{
   DEV_VOLT_CMD_CURRENT,
   DEV_VOLT_CMD_VOLTAGE,
   DEV_VOLT_CMD_SHUTDOWN,
   DEV_VOLT_CMD_DUMMY_MSG
};

/*Messagequeue data structure*/
typedef struct
{
   tS32  s32Command;
   tU32  u32Value;
}trVoltMsg;

typedef struct
{
   tS32                s32taskAct;
   OSAL_tTimerHandle   hTimer;
   tS32                s32ExecTimer;
   tU32                u32PreVoltage;
   OSAL_tSemHandle     sem;
   OSAL_tMQueueHandle  hSPMvoltMq;
   OSAL_tThreadID      ThreadID;
   tS32                s32OpenFlag;
}trVoltPrv;
trVoltPrv        rVoltPrv;


trVoltDrvInf *prVoltDrvInf;

/************************************************************************
| variable definition (scope: module-local) 
|-----------------------------------------------------------------------*/

static tS32 dev_volt_s32createClient(tVoid);
static tS32 dev_volt_s32removeClient(tS32 cl);
static tS32 dev_volt_s32addUserThreshold(const OSAL_tVoltUserThresholdNotification* prOsalUserThres);
static tS32 dev_volt_s32removeUserThreshold(const OSAL_tVoltRemoveThresholdNotification* prOsalUserThres);
static tS32 dev_volt_s32addSystemThreshold(const OSAL_tVoltSystemThresholdNotification* prOsalSystemThres);
static tS32 dev_volt_s32removeSystemThreshold(tS32 s32RemoveSystemThres);
static tS32 dev_volt_s32getSystemThreshold(OSAL_tVoltSystemThresholdHistory* prOsalSysHistory);
static tS32 dev_volt_s32getUserThreshold(OSAL_tVoltUserThresholdHistory* prOsalUserHistory);
static tVoid dev_volt_vtaskthread(tVoid);
static tVoid dev_volt_vTraceOut(  tU32 u32Level,const tChar *pcFormatString,... );
static tBool dev_volt_lock(tVoid);
static tVoid dev_volt_unlock(tVoid);
static tS32 dev_volt_s32findUserThreshold(const trOsalIOInternUserThreshold arUserThreshold[], tU32 u32threshold);
static tS32 dev_volt_s32findUserThresholdGroup(const trOsalIOInternUserThreshold arUserThreshold[], tU32 u32group);
static tS32 dev_volt_s32findEmptyUserThreshold(const trOsalIOInternUserThreshold arUserThreshold[]);
static tVoid dev_volt_userCallback(trVoltDrvInf* prVoltDrvInf, trOsalIOClient arClients[], tU32 u32voltBits);
static tVoid dev_volt_cb_user(tU32 u32reason, tU32 u32idThreshold,tU32 u32userdefined);
static tS32 dev_volt_cvm_eStartTimer(tU32 u32TimeValue);
static tS32 dev_volt_cvm_eStopTimer(tVoid);
static tVoid vSendSystemNotify(tS32 s32Client,tU32 u32PrevSystemLevel);
static tU32 u32calculateSystemLevel(tVoid);
static tVoid vNotifySystem(tU32 u32PrevSystemLevel);
static tVoid vUserNotify(tU32 u32Client, tU32 u32user, trOsalIOClient arClients[], tU32 u32voltBits);
static tVoid vSendCVMSystemNotify(tU32 u32PrevSystemLevel);
static tVoid dev_volt_cvm_TimerCallback(tVoid);
static tVoid vSendCVMNotify(tU32 u32cvmEvent);
static tVoid dev_volt_TimerExpiryOperation(tVoid);
static tVoid vSPMCallbackHandler(void* pvBuffer );
static tS32 dev_volt_s32findValidSystemThreshold(const trOsalIOInternSystemThreshold rOsalSysThresh[]);
static tS32 dev_volt_s32findEmptySystemThreshold(const trOsalIOInternSystemThreshold arOsalSysThresh[]);
static tVoid vRegisterSPM_Callback(void);
static tVoid vUnregisterSPM_Callback(void);
extern void LLD_vRegTraceCallback(TR_tenTraceChan enTraceChannel, OSAL_tpfCallback pfCallback);
extern void LLD_vUnRegTraceCallback(TR_tenTraceChan enTraceChannel);

/****************************************************************************
* FUNCTION    : dev_volt_OsalIO_s32Init
* CREATED     : 
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION :  This function initializes this driver
* SYNTAX      : tS32 dev_volt_OsalIO_s32Init(tVoid)
* ARGUMENTS   : 
*               tVoid  
* RETURN VALUE: 
*               tS32 - Success or Failure
* NOTES       :   -
****************************************************************************/

tS32 dev_volt_OsalIO_s32Init(tVoid)
{
   tS32 s32retValue = OSAL_OK;
   trOsalIOInternSystemThreshold *prSystemThreshold;
   trOsalIOInternUserThreshold *prUserThreshold;
   tS32 s32Client,s32UserThresholdEntry,s32SystemThresholdEntry;
   OSAL_trThreadAttribute  threadAttr;

   dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Inside INIT");
   prVoltDrvInf = &pOsalData->rVoltDrvInf;
	
   OSAL_pvMemorySet((tPVoid)&rVoltPrv, DEV_VOLT_INITIALIZE, sizeof(trVoltPrv));
   OSAL_pvMemorySet((tPVoid)prVoltDrvInf, DEV_VOLT_INITIALIZE, sizeof(trVoltDrvInf));
   dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Memory set success");

   /*create semaphore*/
   if (OSAL_OK != OSAL_s32SemaphoreCreate(DEV_VOLT_SEM_NAME, &rVoltPrv.sem, DEV_VOLT_SEM_INIT))
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"semaphore creation failed in dev_volt_OsalIO_s32Init");
      s32retValue = OSAL_ERROR;
   }

   /*create Timer*/
   else if(OSAL_ERROR == OSAL_s32TimerCreate((OSAL_tpfCallback)dev_volt_cvm_TimerCallback,NULL,
                                              &rVoltPrv.hTimer))
   {
      s32retValue = OSAL_ERROR;
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Timer creation failed in dev_volt_OsalIO_s32Init");
   }

   else if(OSAL_s32MessageQueueCreate( MSG_QUEUE_NAME, DEV_VOLT_MSG_SIZE, sizeof(trVoltMsg),
          OSAL_EN_READWRITE, &rVoltPrv.hSPMvoltMq) == OSAL_ERROR )
   {
      s32retValue = OSAL_ERROR;
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Messagequeue creation failed in dev_volt_OsalIO_s32Init");
   }
   else
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Initializing variables");
      /*Initialize variables*/
      rVoltPrv.s32taskAct = DEV_VOLT_ACTIVATE_TASK_THREAD;
      prVoltDrvInf->u32AdcRefCorrection = DEV_VOLT_ADCREFCORRECTION;//DEV_VOLT_ADC_SCALE_BASE;
      rVoltPrv.u32PreVoltage = DEV_VOLT_OPERATING_VALUE;
      prVoltDrvInf->u32voltage = DEV_VOLT_OPERATING_VALUE;
      prVoltDrvInf->u32current = DEV_VOLT_CURRENT;
      prVoltDrvInf->u32systemLevel = OSAL_VOLT_OPERATING_VOLTAGE;
      prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_OPERATING_VOLTAGE;
      prVoltDrvInf->u32OsalCvmState = OSALCVM_LOW_VOLTAGE_END;

      /*Initialize the client structure*/
      for(s32Client = 0; s32Client < DEV_VOLT_NR_OF_CLIENTS; ++s32Client)
      {
         prVoltDrvInf->arOsalIOClients[s32Client].s32idClient= DEV_VOLT_INVALID_ID; /* empty client */
         for(s32SystemThresholdEntry=0;s32SystemThresholdEntry<DEV_VOLT_NR_OF_SYSTEM_THRESHOLDS;s32SystemThresholdEntry++)
         {
            prSystemThreshold = &prVoltDrvInf->arOsalIOClients[s32Client].arSystemThresholds[s32SystemThresholdEntry];
            prSystemThreshold->s32idThreshold = DEV_VOLT_INVALID_ID;
            prSystemThreshold->hdl = (OSAL_tEventHandle)OSAL_C_INVALID_HANDLE; // set handle to illegal value
            prSystemThreshold->s32kind = OSAL_ERROR;
         }
         for(s32UserThresholdEntry = 0; s32UserThresholdEntry < DEV_VOLT_NR_OF_USER_THRESHOLDS; ++s32UserThresholdEntry)
         {
            prUserThreshold = &prVoltDrvInf->arOsalIOClients[s32Client].arUserThresholds[s32UserThresholdEntry];
            prUserThreshold->s32idThreshold = DEV_VOLT_INVALID_ID;
            prUserThreshold->hdl = (OSAL_tEventHandle)OSAL_C_INVALID_HANDLE; // set handle to illegal value
         }
      }
      threadAttr.szName = "VOLT_NOTIFICATIONS";
      threadAttr.u32Priority  = DEV_VOLT_THREAD_PRIO;
      threadAttr.s32StackSize = OSALIO_TASK_STACK_SIZE_PRM_RECO_TA;
      threadAttr.pfEntry = (OSAL_tpfThreadEntry)dev_volt_vtaskthread;
      threadAttr.pvArg   = NULL;
      rVoltPrv.ThreadID = OSAL_ThreadSpawn( &threadAttr );
      if( rVoltPrv.ThreadID == OSAL_ERROR )
      {
         s32retValue = OSAL_ERROR;
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Thread not created in dev_volt_OsalIO_s32Init");
      }
      else
      {
         /*Register the SPM trace callback*/
         vRegisterSPM_Callback();
         /*Set flag not to open again*/
         rVoltPrv.s32OpenFlag++;
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Init success");
      }
   }

   /*check for error*/
   if(s32retValue == OSAL_ERROR)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Inside INIT:Error occured");
      /*check for valid handle*/
      if(OSAL_C_INVALID_HANDLE != rVoltPrv.sem)
      {
         /*close the semaphore*/
         if (OSAL_s32SemaphoreClose(rVoltPrv.sem) == OSAL_OK)
         {
            dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"semaphore close failed in dev_volt_OsalIO_s32Init");
            if (OSAL_s32SemaphoreDelete(DEV_VOLT_SEM_NAME) != OSAL_OK)
            {
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"semaphore delete failed in dev_volt_OsalIO_s32Init");
               s32retValue = OSAL_ERROR;
            }
         }
      }
      if(OSAL_C_INVALID_HANDLE != rVoltPrv.hTimer)
      {
         if(OSAL_ERROR == OSAL_s32TimerDelete(rVoltPrv.hTimer))
         {
            dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Timer deletion failed in dev_volt_OsalIO_s32Init");
            s32retValue = OSAL_ERROR;
         }
      }
      if(OSAL_C_INVALID_HANDLE != rVoltPrv.hSPMvoltMq)
      {
         /*close the message queue*/
         if(OSAL_OK != OSAL_s32MessageQueueClose( rVoltPrv.hSPMvoltMq ))
         {
            dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Message close failed in dev_volt_OsalIO_s32Init");
         }
         /*Delete the message queue*/
         if(OSAL_OK != OSAL_s32MessageQueueDelete( MSG_QUEUE_NAME ))
         {
            dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Message delete failed in dev_volt_OsalIO_s32Init");
         }
      }
   }
   return(s32retValue);
}/*end function*/

/****************************************************************************
* FUNCTION    : dev_volt_OsalIO_s32DeInit
* CREATED     : 
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION :   This function is to deinitializes the driver
* SYNTAX      : tS32 dev_volt_OsalIO_s32DeInit(tVoid)
* ARGUMENTS   : 
*               tVoid  
* RETURN VALUE: 
*               tS32 - Success or Failure
* NOTES       :   -
****************************************************************************/

tS32 dev_volt_OsalIO_s32DeInit(tVoid)
{
   trVoltMsg rMsg;
   tS32 s32retValue = OSAL_OK;

   rVoltPrv.s32taskAct = DEV_VOLT_DEACTIVATE_TASK_THREAD;
   rMsg.s32Command = DEV_VOLT_CMD_SHUTDOWN;

   dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Inside Deinit");

   /*Post msg queue to exit the thread*/
   if(OSAL_s32MessageQueuePost(rVoltPrv.hSPMvoltMq,(tPCU8)&rMsg,sizeof(trVoltMsg),DEV_VOLT_MQ_HIGH_PRIORITY)!=OSAL_OK)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Message queue post failed in dev_volt_OsalIO_s32DeInit");
      s32retValue = OSAL_ERROR;
   }
   else
   {
      /*Unregister the SPM trace callback*/
      vUnregisterSPM_Callback();
      rVoltPrv.s32OpenFlag = 0;
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"DeInit success");
   }

   return s32retValue;
}




/****************************************************************************
* FUNCTION    : dev_volt_s32IOOpen
* CREATED     : 
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION :   -
* SYNTAX      : tS32 dev_volt_s32IOOpen(tVoid)
* ARGUMENTS   : 
*               tVoid -
* RETURN VALUE: 
*               tS32 errorcode - Success or Failure
* NOTES       :   -
****************************************************************************/

tS32 dev_volt_s32IOOpen(tVoid)
{
   tS32 s32retValue = OSAL_E_NOERROR;

   if(!rVoltPrv.s32OpenFlag)
   {
      prVoltDrvInf = &pOsalData->rVoltDrvInf;
      /*Open semaphore*/
      if(OSAL_OK != OSAL_s32SemaphoreOpen(DEV_VOLT_SEM_NAME, &rVoltPrv.sem))
      {
         s32retValue = OSAL_ERROR;
      }
      else
      {
         rVoltPrv.s32OpenFlag++;
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Driver Open Successful");
      }
   }
   else
   {
      rVoltPrv.s32OpenFlag++;
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Driver Open flag incremented");
   }
   return(s32retValue);
}

/****************************************************************************
* FUNCTION    : dev_volt_s32IOClose
* CREATED     : 
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION :   -
* SYNTAX      : tS32 dev_volt_s32IOClose(tVoid)
* ARGUMENTS   : 
*               tVoid -
* RETURN VALUE: 
*               tS32 errorcode - Success or Failure
* NOTES       :   -
****************************************************************************/

tS32 dev_volt_s32IOClose(tVoid)
{
   tS32 s32retValue = OSAL_E_NOERROR;
   
   if(rVoltPrv.s32OpenFlag == DEV_VOLT_FINAL_CLOSE)
   {
      /*close semaphore*/
      if (OSAL_C_INVALID_HANDLE != rVoltPrv.sem)
      {
         if(OSAL_OK !=OSAL_s32SemaphoreClose(rVoltPrv.sem))
         {
            s32retValue = OSAL_ERROR;
         }
         else
         {
            rVoltPrv.s32OpenFlag--;
            dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Driver Close Successful");
         }
      }

   }
   else
   {
      rVoltPrv.s32OpenFlag--;
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Driver close flag decremented");
      //do nothing
   }

   return(s32retValue);
}

/****************************************************************************
* FUNCTION    : dev_volt_s32IOControl
* CREATED     : 
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION :   -
* SYNTAX      : tS32 dev_volt_s32IOControl(tS32 fun, tS32 arg)
* ARGUMENTS   : 
*               tS32 fun - function number
*               tS32 arg - args passed from user
* RETURN VALUE: 
*               tS32 errorcode - Success or Failure
* NOTES       :   -
****************************************************************************/

tS32 dev_volt_s32IOControl(tS32 fun, tS32 arg)
{
   tS32 s32retVal= OSAL_E_NOERROR;
   tPS32  ps32Argument =  (tPS32) arg;

   if(arg == OSAL_NULL)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32IOControl:arg is NULL");
   }

   switch (fun)
   {
      case OSAL_C_S32_IOCTRL_VOLT_CREATE_NOTIFICATION_CLIENT:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            *ps32Argument = dev_volt_s32createClient();
            if (*ps32Argument == DEV_VOLT_INVALID_ID)
            {
               s32retVal = OSAL_E_NOSPACE;
            }
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_REMOVE_NOTIFICATION_CLIENT:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            s32retVal = dev_volt_s32removeClient(*ps32Argument);
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_SET_USER_THRESHOLD_NOTIFICATION:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            OSAL_tVoltUserThresholdNotification* prUserThresholdNotification =(OSAL_tVoltUserThresholdNotification*)arg;

            /* correct the user client id */
            prUserThresholdNotification->idThreshold += E_VOLT_FIRST_USER_THRESHOLD;

            s32retVal = dev_volt_s32addUserThreshold(prUserThresholdNotification);
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_DEREG_USER_THRESHOLD_NOTIFICATION:
      {
         if(arg == OSAL_NULL)
         {
           s32retVal = OSAL_ERROR;
         }
         else
         {
            OSAL_tVoltRemoveThresholdNotification* rRemoveThreshold = (OSAL_tVoltRemoveThresholdNotification*)arg;

            /* correct the user client id */
            rRemoveThreshold->idThreshold += E_VOLT_FIRST_USER_THRESHOLD;

            s32retVal = dev_volt_s32removeUserThreshold(rRemoveThreshold);
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_SET_SYSTEM_THRESHOLD_NOTIFICATION:
      {
         if(arg == OSAL_NULL)
         {
           s32retVal = OSAL_ERROR;
         }
         else
         {
            OSAL_tVoltSystemThresholdNotification* prSystemThreshold = (OSAL_tVoltSystemThresholdNotification*)arg;

            s32retVal = dev_volt_s32addSystemThreshold(prSystemThreshold);

         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_DEREG_SYSTEM_THRESHOLD_NOTIFICATION:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            s32retVal = dev_volt_s32removeSystemThreshold((tS32)ps32Argument);
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_LEVEL_HISTORY:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            s32retVal = dev_volt_s32getSystemThreshold((OSAL_tVoltSystemThresholdHistory*)arg);
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_LEVEL_HISTORY:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            OSAL_tVoltUserThresholdHistory* prUserHistory = (OSAL_tVoltUserThresholdHistory*)arg;

            /* correct the user client id */
            prUserHistory->idThreshold += E_VOLT_FIRST_USER_THRESHOLD;

            if (dev_volt_s32getUserThreshold(prUserHistory) == DEV_VOLT_ERROR)
            {
               s32retVal = OSAL_E_INVALIDVALUE;
            }
            else
            {
               tU32 u32Count;

               // correct the user client id back again
               for(u32Count = 0; u32Count < prUserHistory->idx; ++u32Count)
               {
                  prUserHistory->arClients[u32Count] -= E_VOLT_FIRST_USER_THRESHOLD;
               }
               prUserHistory->idThreshold -= E_VOLT_FIRST_USER_THRESHOLD;

               s32retVal = OSAL_E_NOERROR;
            }
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_SYSTEM_VOLTAGE_LEVEL:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            if(dev_volt_lock())
            {
               *((tU32*)arg) = prVoltDrvInf->u32systemLevel;
               dev_volt_unlock();
            }
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_CVM_VOLTAGE_LEVEL:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            if(dev_volt_lock())
            {
               *((tU32*)arg) = prVoltDrvInf->u32DrvCvmState;
               dev_volt_unlock();
            }
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_USER_VOLTAGE_RANGE:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            ((OSAL_tVoltUserThresholdRange*)(arg))->highestVoltage = DEV_VOLT_HIGHESTVOLT;
            ((OSAL_tVoltUserThresholdRange*)(arg))->lowestVoltage = DEV_VOLT_LOWESTVOLT;
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            if(dev_volt_lock())
            {
               /* History: Convert millivolts to centivolts: */
               *((tU32*)arg) = (prVoltDrvInf->u32voltage/10);
               dev_volt_unlock();
            }
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_MV:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            if(dev_volt_lock())
            {
               *((tU32*)arg) = (prVoltDrvInf->u32voltage);
               dev_volt_unlock();
            }
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_RAW:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            if(dev_volt_lock())
            {
               *((tU32*)arg) = (prVoltDrvInf->u32voltage);
               dev_volt_unlock();
            }
         }
         
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_SCALE:
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_SCALE:Driver Successful");
         // Not needed anymore: Dev_volt_osalio now reads self from KDS to get adc ref value and sets GDI_VOLT_DRV_SET_BOARD_VOLTAGE_SCALE.}
         // IOCTRL is left for compatibility reason. Now OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_UREF is to be used, if spm
         // wants to change uref voltage factor
         //
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_VOLTAGE_UREF:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            prVoltDrvInf->u32AdcRefCorrection = (*((tU32*)(arg)));
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_VOLTAGE_SCALE:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            *((tU32*)arg) = prVoltDrvInf->u32AdcRefCorrection;
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_GET_BOARD_CURRENT:
      {
         if(arg == OSAL_NULL)
         {
            s32retVal = OSAL_ERROR;
         }
         else
         {
            if(dev_volt_lock())
            {
               *((tU32*)arg)= prVoltDrvInf->u32current;
               dev_volt_unlock();
            }
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_CURRENT_SCALE:
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"OSAL_C_S32_IOCTRL_VOLT_SET_BOARD_CURRENT_SCALE:Driver Successful");
         break;
      }
      default:
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32IOControl:Invalid command:%d",fun);
         s32retVal = OSAL_E_INVALIDVALUE;
         break;
      }
   }
   return s32retVal;
}

/****************************************************************************
* FUNCTION    : vRegisterSPM_Callback
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : Registers the vSPMCallbackHandler
* SYNTAX      : tVoid vRegisterSPM_Callback(void)
* ARGUMENTS   : NONE
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
static tVoid vRegisterSPM_Callback(void)
{
   LLD_vRegTraceCallback((TR_tenTraceChan)TR_TTFIS_DEV_SPM,(OSAL_tpfCallback)vSPMCallbackHandler);
}

/****************************************************************************
* FUNCTION    : vUnregisterSPM_Callback
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : Unregisters the vUnregisterSPM_Callback
* SYNTAX      : tVoid vUnregisterSPM_Callback(void)
* ARGUMENTS   : NONE
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
static tVoid vUnregisterSPM_Callback(void)
{
   LLD_vUnRegTraceCallback((TR_tenTraceChan)TR_TTFIS_DEV_SPM);
}

/****************************************************************************
* FUNCTION    : vSPMCallbackHandler
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is handling spm callback
* SYNTAX      : tVoid vSPMCallbackHandler(void* pvBuffer )
* ARGUMENTS   : pvBuffer   command via trace
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
static tVoid vSPMCallbackHandler(void* pvBuffer )
{
   tPU8 pu8Buffer;

   pu8Buffer = (tPU8)pvBuffer;

   switch ((tS32)pu8Buffer[2])
   {
      case OSAL_SPM_VOLTAGE:
         dev_volt_set_voltage((tPVoid)&pu8Buffer[3]);
      break;
      case OSAL_SPM_CURRENT:
         dev_volt_set_current((tPVoid)&pu8Buffer[3]);
      break;
      default: // do nothing
         TraceIOString("Unknown Command for LINUX Dev Volt Callback handler");
      break;
   }
}

/****************************************************************************
* FUNCTION    : dev_volt_set_current
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is called when user gives input from TTFis
* SYNTAX      : tVoid dev_volt_set_current(tPCVoid pvTraceData)
* ARGUMENTS   : pvTraceData - 16bit data from trace
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
tVoid dev_volt_set_current(tPCVoid pvTraceData)
{
   trVoltMsg rMsg;

   rMsg.s32Command = DEV_VOLT_CMD_CURRENT;
   /*Update the current value to the message*/
   rMsg.u32Value = (tU32)(*(tU16 *)pvTraceData);
   /*Post the message with the current value*/
   if(OSAL_s32MessageQueuePost(rVoltPrv.hSPMvoltMq,(tPCU8)&rMsg,sizeof(trVoltMsg),DEV_VOLT_MQ_NORMAL_PRIORITY) == OSAL_ERROR)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"MessageQueue post failed in VOLT_trace_callback");
   }
}

/****************************************************************************
* FUNCTION    : dev_volt_set_voltage
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is called when user gives input from TTFis
* SYNTAX      : tVoid dev_volt_set_voltage(tPCVoid pvTraceData)
* ARGUMENTS   : pvTraceData - 16 bit data from trace
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
tVoid dev_volt_set_voltage(tPCVoid pvTraceData)
{
   trVoltMsg rMsg;

   rMsg.s32Command = DEV_VOLT_CMD_VOLTAGE;
   /*Update the voltage value to the message*/
   rMsg.u32Value = (tU32)(*(tU16 *)pvTraceData);
   /*Post the message with the current value*/
   if(OSAL_s32MessageQueuePost(rVoltPrv.hSPMvoltMq,(tPCU8)&rMsg,sizeof(trVoltMsg),DEV_VOLT_MQ_NORMAL_PRIORITY) == OSAL_ERROR)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"MessageQueue post failed in VOLT_trace_callback");
   }
}

/****************************************************************************
* FUNCTION    : dev_volt_TimerExpiryOperation
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is called when a timer flag is set from timer callback
*               This function gives notification to the registered CVM clients by checking
*               present system level
* SYNTAX      : tVoid dev_volt_TimerExpiryOperation(tVoid)
* ARGUMENTS   : tVoid
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
static tVoid dev_volt_TimerExpiryOperation(tVoid)
{
   /*Update the CVM state*/
   prVoltDrvInf->u32OsalCvmState = OSALCVM_LOW_VOLTAGE_END;
   prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_OPERATING_VOLTAGE;
   /*Do notification*/
   vSendCVMNotify(prVoltDrvInf->u32OsalCvmState);
   /*Check for present voltage level*/
   if((prVoltDrvInf->u32systemLevel == OSAL_VOLT_OVER_VOLTAGE) ||
         (prVoltDrvInf->u32systemLevel == OSAL_VOLT_CRITICAL_OVER_VOLTAGE))
   {
      /*Update the CVM state*/
      prVoltDrvInf->u32OsalCvmState = OSALCVM_HIGH_VOLTAGE_START;
      prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_HIGH_VOLTAGE;
      /*Do notification*/
      vSendCVMNotify(prVoltDrvInf->u32OsalCvmState);
   }
   /*Check for critical high voltage*/
   if(prVoltDrvInf->u32systemLevel == OSAL_VOLT_CRITICAL_OVER_VOLTAGE)
   {
      /*Update the CVM state*/
      prVoltDrvInf->u32OsalCvmState = OSALCVM_CRITICAL_HIGH_VOLTAGE_START;
      prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_CRIT_HIGH_VOLTAGE;
      /*Do notification*/
      vSendCVMNotify(prVoltDrvInf->u32OsalCvmState);
   }
   /*Update this flag,not to execute next time until by timer*/
   rVoltPrv.s32ExecTimer = DEV_VOLT_DONT_EXECUTE_TIMER;
}


/****************************************************************************
* FUNCTION    : dev_volt_vtaskthread
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is executed as thread which performs voltage and
*               current updation,does callback notification event.
* SYNTAX      : tVoid dev_volt_vtaskthread(tVoid)
* ARGUMENTS   : tVoid
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/

static tVoid dev_volt_vtaskthread(tVoid)
{
   tS32 s32Return;
   trVoltMsg rMsg;
   tU32 u32systemLevel;
   tU32 u32PrevSystemLevel;

   while(rVoltPrv.s32taskAct)
   {
      /*Wait for the message */
      s32Return = OSAL_s32MessageQueueWait( rVoltPrv.hSPMvoltMq, (tPU8)&rMsg, sizeof(trVoltMsg),
                                          OSAL_NULL, OSAL_C_TIMEOUT_FOREVER );
      if( s32Return != OSAL_ERROR )
      {
         /* Every time this timer is done to ensure that timer notification is done as earliest */
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Received Message");
         if(rVoltPrv.s32ExecTimer)
         {
            dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_vtaskthread:Executing timer operation");
            dev_volt_TimerExpiryOperation();
         }

         /*check for current/voltage data updation*/
         switch(rMsg.s32Command)
         {
            case DEV_VOLT_CMD_DUMMY_MSG:
            {
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_vtaskthread:Dummy message command");
               break;
            }
            case DEV_VOLT_CMD_CURRENT://current
            {

               /*Update in driver structure*/
               prVoltDrvInf->u32current = rMsg.u32Value;
               break;
            }

            case DEV_VOLT_CMD_VOLTAGE://voltage
            {
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Voltage received");

               /*Update in driver structure*/
               prVoltDrvInf->u32voltage = rMsg.u32Value;

               /*check for very high voltage*/
               if(prVoltDrvInf->u32voltage > DEV_VOLT_HIGHESTVOLT)
               {
                  dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Very high voltage");
                  /*platform_shutdown is not yet implemented*/
               }
               /*check for very low voltage*/
               else if(prVoltDrvInf->u32voltage <= DEV_VOLT_VERYLOW)
               {
                  dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Very low voltage");
                  /*platform_shutdown is not yet implemented*/
               }

               /* check for system level change */
               u32systemLevel = u32calculateSystemLevel();
               if(u32systemLevel != prVoltDrvInf->u32systemLevel)
               {
                  dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"System level changed to:%d",u32systemLevel);
                  /*store the previous system level*/
                  u32PrevSystemLevel = prVoltDrvInf->u32systemLevel;
                  prVoltDrvInf->u32systemLevel = u32systemLevel;

                  /*perform the system level notification*/
                  vNotifySystem(u32PrevSystemLevel);

                  /*Perform CVM system notification*/
                  vSendCVMSystemNotify(u32PrevSystemLevel);
               }

               /*check for voltage level change*/
               if(rVoltPrv.u32PreVoltage != prVoltDrvInf->u32voltage)
               {
                  /*perform user level notification*/
                  dev_volt_userCallback(prVoltDrvInf, prVoltDrvInf->arOsalIOClients,
                                       prVoltDrvInf->u32voltage);
               }

               /*Update the previous voltage value*/
               rVoltPrv.u32PreVoltage = prVoltDrvInf->u32voltage;
               break;
            }
            case DEV_VOLT_CMD_SHUTDOWN:
            {
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_vtaskthread:shutdown command");
               break;
            }
            default:
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_vtaskthread:Invalid command");
            break;
         }
      }
      else
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_vtaskthread:Message Queue Wait Failed");
      }
   }
   /*Delete Timer*/
   if(OSAL_ERROR == OSAL_s32TimerDelete(rVoltPrv.hTimer))
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Timer deletion failed in dev_volt_vtaskthread");
   }
   if (OSAL_C_INVALID_HANDLE != rVoltPrv.sem)
   {
      if (OSAL_s32SemaphoreClose(rVoltPrv.sem) == OSAL_OK)
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"semaphore close success in dev_volt_vtaskthread");
         if (OSAL_s32SemaphoreDelete(DEV_VOLT_SEM_NAME) != OSAL_OK)
         {
            dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"semaphore delete failed in dev_volt_vtaskthread");
         }
      }
      else
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"semaphore close failed in dev_volt_vtaskthread");
      }
   }
   /*close the message queue*/
   if(OSAL_C_INVALID_HANDLE != rVoltPrv.hSPMvoltMq)
   {
      if(OSAL_OK != OSAL_s32MessageQueueClose( rVoltPrv.hSPMvoltMq ))
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Message close failed in dev_volt_vtaskthread");
      }
      /*Delete the message queue*/
      if(OSAL_OK != OSAL_s32MessageQueueDelete( MSG_QUEUE_NAME ))
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Message delete failed in dev_volt_vtaskthread");
      }
   }
}

/****************************************************************************
* FUNCTION    : u32calculateSystemLevel
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to calculate system level
* SYNTAX      : tU32 u32calculateSystemLevel()
* ARGUMENTS   : tVoid
*
* RETURN VALUE: tU32 - system level
* NOTES       :   -
****************************************************************************/
static tU32 u32calculateSystemLevel()
{
   tU32 u32SystemLevel;

   /*check for greater than critical high voltage*/
   if(prVoltDrvInf->u32voltage >= DEV_VOLT_CRITICALHIGH_VALUE)
   {
      u32SystemLevel = OSAL_VOLT_CRITICAL_OVER_VOLTAGE;
   }
   /*check for greater than high voltage*/
   else if(prVoltDrvInf->u32voltage >= DEV_VOLT_HIGH_VALUE)
   {
      u32SystemLevel = OSAL_VOLT_OVER_VOLTAGE;
   }
   /*check for less than very low voltage*/
   else if(prVoltDrvInf->u32voltage <= DEV_VOLT_CRITICALLOW_VALUE)
   {
      u32SystemLevel = OSAL_VOLT_CRITICAL_LOW_VOLTAGE;
   }
   /*check for less than low voltage*/
   else if(prVoltDrvInf->u32voltage <= DEV_VOLT_LOW_VALUE)
   {
      u32SystemLevel = OSAL_VOLT_LOW_VOLTAGE;
   }
   else
   {
      u32SystemLevel = OSAL_VOLT_OPERATING_VOLTAGE;
   }

   return u32SystemLevel;
}

/****************************************************************************
* FUNCTION    : vNotifySystem
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to notify system level
* SYNTAX      : tVoid vNotifySystem(tU32 u32PrevSystemLevel)
* ARGUMENTS   : u32PrevSystemLevel - Previous system level
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/

static tVoid vNotifySystem(tU32 u32PrevSystemLevel)
{
   tS32 s32Client;
   tS32 s32SystemThresholdEntry;
   trOsalIOInternSystemThreshold *prSystemThreshold;

   /*iterate for all clients*/
   for(s32Client = 0; s32Client < DEV_VOLT_NR_OF_CLIENTS; ++s32Client)
   {
      /*check for valid client ID*/
      if (prVoltDrvInf->arOsalIOClients[s32Client].s32idClient != DEV_VOLT_INVALID_ID)
      {
         for(s32SystemThresholdEntry = 0; s32SystemThresholdEntry < DEV_VOLT_NR_OF_SYSTEM_THRESHOLDS; s32SystemThresholdEntry++)
         {
            prSystemThreshold = &prVoltDrvInf->arOsalIOClients[s32Client].arSystemThresholds[s32SystemThresholdEntry];
            /*check for kind is system*/
            if(prSystemThreshold->s32kind == OSAL_VOLT_KIND_SYSTEM_THRESHOLDS)
            {
               /*Perform system notification*/
               vSendSystemNotify(s32Client,u32PrevSystemLevel);
            }
         }
      }
   }
}

/****************************************************************************
* FUNCTION    : vSendCVMSystemNotify
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to perform system level notification
*               Checks for each system level change happened and perform history update and give
*               notification to the user.
* SYNTAX      : tVoid vSendCVMSystemNotify(tS32 s32Client,tU32 u32PrevSystemLevel)
* ARGUMENTS   : u32PrevSystemLevel - Previous system level
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
static tVoid vSendCVMSystemNotify(tU32 u32PrevSystemLevel)
{
   tU32 u32level;
   tU32 u32TempSystemLevel;
   tU32 u32LevelChange = DEV_VOLT_INITIALIZE;
   tBool bEventFlag = TRUE;

   dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"vSendCVMSystemNotify:u32PrevSystemLevel:%d",u32PrevSystemLevel);
   /*identify the system level shift*/
   if(u32PrevSystemLevel > prVoltDrvInf->u32systemLevel)
   {
      u32LevelChange = u32PrevSystemLevel - prVoltDrvInf->u32systemLevel;
   }
   else
   {
      u32LevelChange = prVoltDrvInf->u32systemLevel - u32PrevSystemLevel;
   }

   /*Update the previous system level in temporary variable*/
   u32TempSystemLevel = u32PrevSystemLevel;

   /*perform loop for level change*/
   for(u32level = 0; u32level < u32LevelChange; u32level++)
   {
      bEventFlag = TRUE;
      /*check for system level decreases*/
      if(u32PrevSystemLevel > prVoltDrvInf->u32systemLevel)
      {
         /*Check for previous system level*/
         switch(u32TempSystemLevel)
         {
            /*check whether system level decreases from low voltage*/
            case OSAL_VOLT_LOW_VOLTAGE:
            {
               prVoltDrvInf->u32OsalCvmState = OSALCVM_CRITICAL_LOW_VOLTAGE_START;
               prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_CRIT_LOW_VOLTAGE;
               break;
            }
            /*check whether system level decreases from operating voltage*/
            case OSAL_VOLT_OPERATING_VOLTAGE:
            {
               /*If timer is already started,then stop it and no need to update the states because we already in low voltage*/
               if(prVoltDrvInf->u32DrvCvmState == DEV_VOLT_CVM_LOW_VOLTAGE_DELAY)
               {
                  dev_volt_cvm_eStopTimer();
                  bEventFlag = FALSE;
               }
               else
               {
                  prVoltDrvInf->u32OsalCvmState = OSALCVM_LOW_VOLTAGE_START;
               }
               prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_LOW_VOLTAGE;
               break;
            }
            /*check whether system level decreases from over voltage*/
            case OSAL_VOLT_OVER_VOLTAGE:
            {
               /*If timer is already started,then no need to do any operation in this level until timer expires,
               because timer callback will takescare*/
               if(prVoltDrvInf->u32DrvCvmState == DEV_VOLT_CVM_LOW_VOLTAGE_DELAY)
               {
                  bEventFlag = FALSE;
               }
               else
               {
                  prVoltDrvInf->u32OsalCvmState = OSALCVM_HIGH_VOLTAGE_END;
                  prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_OPERATING_VOLTAGE;
               }
               break;
            }
            /*check whether system level decreases from critical over voltage*/
            case OSAL_VOLT_CRITICAL_OVER_VOLTAGE:
            {
               /*If timer is already started,then no need to do any operation in this level until timer expires,
               because timer callback will takescare*/
               if(prVoltDrvInf->u32DrvCvmState == DEV_VOLT_CVM_LOW_VOLTAGE_DELAY)
               {
                  bEventFlag = FALSE;
               }
               else
               {
                  prVoltDrvInf->u32OsalCvmState = OSALCVM_CRITICAL_HIGH_VOLTAGE_END;
                  prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_HIGH_VOLTAGE;
               }
               break;
            }
            default:
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"vSendCVMSystemNotify:Invalid system level on decrease");
            break;
         }
         u32TempSystemLevel--;
      }

      /*check for system level increases*/
      else
      {
         switch(u32TempSystemLevel)
         {
            /*check whether system level increases from critical low voltage*/
            case OSAL_VOLT_CRITICAL_LOW_VOLTAGE:
            {
               prVoltDrvInf->u32OsalCvmState = OSALCVM_CRITICAL_LOW_VOLTAGE_END;
               prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_LOW_VOLTAGE;
               break;
            }
            /*check whether system level increases from low voltage*/
            case OSAL_VOLT_LOW_VOLTAGE:
            {
               prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_LOW_VOLTAGE_DELAY;
               dev_volt_cvm_eStartTimer(DEV_VOLT_CVM_LOW_VOLTAGE_DELAY_MS);
               bEventFlag = FALSE;
               break;
            }
            /*check whether system level increases from operating voltage*/
            case OSAL_VOLT_OPERATING_VOLTAGE:
            {
               /*If timer is already started,then no need to do any operation in this level until timer expires,
               because timer callback will takescare*/
               if(prVoltDrvInf->u32DrvCvmState == DEV_VOLT_CVM_LOW_VOLTAGE_DELAY)
               {
                  bEventFlag = FALSE;
               }
               else
               {
                  prVoltDrvInf->u32OsalCvmState = OSALCVM_HIGH_VOLTAGE_START;
                  prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_HIGH_VOLTAGE;
               }
               break;
            }
            /*check whether system level increases from high voltage*/
            case OSAL_VOLT_OVER_VOLTAGE:
            {
               /*If timer is already started,then no need to do any operation in this level until timer expires,
               because timer callback will takescare*/
               if(prVoltDrvInf->u32DrvCvmState == DEV_VOLT_CVM_LOW_VOLTAGE_DELAY)
               {
                  bEventFlag = FALSE;
               }
               else
               {
                  prVoltDrvInf->u32OsalCvmState = OSALCVM_CRITICAL_HIGH_VOLTAGE_START;
                  prVoltDrvInf->u32DrvCvmState = DEV_VOLT_CVM_CRIT_HIGH_VOLTAGE;
               }
               break;
            }
            default:
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"vSendCVMSystemNotify:Invalid system level on increase");
            break;
         }
         u32TempSystemLevel++;
      }

      if(bEventFlag)
      {
         vSendCVMNotify(prVoltDrvInf->u32OsalCvmState);
      }
   }

}

/****************************************************************************
* FUNCTION    : vSendCVMNotify
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to perform CVM level notification
* SYNTAX      : tVoid vSendCVMNotify(tU32 u32cvmEvent)
* ARGUMENTS   : u32cvmEvent - cvm state
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/

static tVoid vSendCVMNotify(tU32 u32cvmEvent)
{
   tS32 s32Client;
   tS32 s32SystemThresholdEntry;
   trOsalIOInternSystemThreshold *prSystemThreshold;

   for(s32Client = 0; s32Client < DEV_VOLT_NR_OF_CLIENTS; s32Client++)
   {
      if (prVoltDrvInf->arOsalIOClients[s32Client].s32idClient != DEV_VOLT_INVALID_ID)
      {
         for(s32SystemThresholdEntry = 0; s32SystemThresholdEntry < DEV_VOLT_NR_OF_SYSTEM_THRESHOLDS; s32SystemThresholdEntry++)
         {
            prSystemThreshold = &prVoltDrvInf->arOsalIOClients[s32Client].arSystemThresholds[s32SystemThresholdEntry];
            /*check for KIND is CVM*/
            if(prSystemThreshold->s32kind == OSAL_VOLT_KIND_SYSTEM_CVM_THRESHOLDS)
            {
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"vSendCVMNotify,CVM matches with Client:%d",s32Client);
               if(dev_volt_lock())
               {
                  /*check whether the history level reaches to a max. allowed*/
                  if(prSystemThreshold->u32idx == OSAL_VOLT_MAX_SYSTEM_HISTORY)
                  {
                     prSystemThreshold->u32idx = 0;
                     prSystemThreshold->bBuffRefill = TRUE;
                     dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"vSendCVMNotify:Buffer refill starts");
                  }
                  /*Update the history*/
                  prSystemThreshold->au8Events[prSystemThreshold->u32idx] = (tU8)u32cvmEvent;
                  prSystemThreshold->u32idx++;
                  dev_volt_unlock();
               }
               /*Check for valid mask*/
               if ((prSystemThreshold->u32mask != DEV_VOLT_MASK_NOT_SET)
                        && (prSystemThreshold->hdl != OSAL_C_INVALID_HANDLE)) /* mask is set so trigger an event */
               {
                  dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Mask is set and posting event");
                  /*post the event to the registered system threshold*/
                  OSAL_s32EventPost(prSystemThreshold->hdl,
                                    prSystemThreshold->u32mask,
                                    OSAL_EN_EVENTMASK_OR);
               }
            }
         }
      }
   }
}

/****************************************************************************
* FUNCTION    : vSendSystemNotify
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to perform system level notification
*               Checks for present system level and performs history update and
*               notification without checking any level change
* SYNTAX      : tVoid vSendSystemNotify(tS32 s32Client,tU32 u32PrevSystemLevel)
* ARGUMENTS   : s32Client - client ID
*               u32PrevSystemLevel - Previous system level
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
static tVoid vSendSystemNotify(tS32 s32Client,tU32 u32PrevSystemLevel)
{
   tS32 s32SystemThresholdEntry;
   tU32 u32SysEvent = DEV_VOLT_INITIALIZE;
   trOsalIOInternSystemThreshold *prSystemThreshold;

   for(s32SystemThresholdEntry = 0; s32SystemThresholdEntry < DEV_VOLT_NR_OF_SYSTEM_THRESHOLDS; s32SystemThresholdEntry++)
   {
      prSystemThreshold = &prVoltDrvInf->arOsalIOClients[s32Client].arSystemThresholds[s32SystemThresholdEntry];

      /*check for system level decreases*/
      if(u32PrevSystemLevel > prVoltDrvInf->u32systemLevel)
      {
         /*Check for present system level*/
         switch(prVoltDrvInf->u32systemLevel)
         {
            /*check whether the system level decreased to very low voltage*/
            case OSAL_VOLT_CRITICAL_LOW_VOLTAGE:
            {
               u32SysEvent = OSALCVM_CRITICAL_LOW_VOLTAGE_START;
               break;
            }
            /*check whether the system level decreased to low voltage*/
            case OSAL_VOLT_LOW_VOLTAGE:
            {
               u32SysEvent = OSALCVM_LOW_VOLTAGE_START;
               break;
            }
            /*check whether the system level decreased to operating voltage*/
            case OSAL_VOLT_OPERATING_VOLTAGE:
            {
               u32SysEvent = OSALCVM_HIGH_VOLTAGE_END;
               break;
            }
            /*check whether the system level decreased to high voltage*/
            case OSAL_VOLT_OVER_VOLTAGE:
            {
               u32SysEvent = OSALCVM_CRITICAL_HIGH_VOLTAGE_END;
               break;
            }
            default:
            {
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"vSendSystemNotify:Invalid system level");
               break;
            }
         }
      }
      /*check for system level increases*/
      else
      {
         switch(prVoltDrvInf->u32systemLevel)
         {
            /*check whether the system level increased to low voltage*/
            case OSAL_VOLT_LOW_VOLTAGE:
            {
               u32SysEvent = OSALCVM_CRITICAL_LOW_VOLTAGE_END;
               break;
            }
            /*check whether the system level increased to operating voltage*/
            case OSAL_VOLT_OPERATING_VOLTAGE:
            {
               u32SysEvent = OSALCVM_LOW_VOLTAGE_END;
               break;
            }
            /*check whether the system level increased to high voltage*/
            case OSAL_VOLT_OVER_VOLTAGE:
            {
               u32SysEvent = OSALCVM_HIGH_VOLTAGE_START;
               break;
            }
            /*check whether the system level increased to very high voltage*/
            case OSAL_VOLT_CRITICAL_OVER_VOLTAGE:
            {
               u32SysEvent = OSALCVM_CRITICAL_HIGH_VOLTAGE_START;
               break;
            }
            default:
            {
               dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"vSendSystemNotify:Invalid system level");
               break;
            }
         }
      }
     /*check whether the history level reaches to a max. allowed*/
      if(prSystemThreshold->u32idx == OSAL_VOLT_MAX_SYSTEM_HISTORY)
      {
         prSystemThreshold->u32idx = 0;
         prSystemThreshold->bBuffRefill = TRUE;
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"vSendSystemNotify:Buffer refill starts");
      }
      /*Update the history*/
      prSystemThreshold->au8Events[prSystemThreshold->u32idx] = (tU8)u32SysEvent;
      prSystemThreshold->u32idx++;
   }


   if((prSystemThreshold->u32mask != DEV_VOLT_MASK_NOT_SET)
            && (prSystemThreshold->hdl != OSAL_C_INVALID_HANDLE))/* mask is set so trigger an event */
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Mask is set and Posting event");
      /*post the event to the registered system threshold*/
      OSAL_s32EventPost(prSystemThreshold->hdl,
                        prSystemThreshold->u32mask,
                        OSAL_EN_EVENTMASK_OR);
   }
}


/****************************************************************************
* FUNCTION    : dev_volt_vTraceOut
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : To print trace messages on console IO (TTFI). VOLT device
*                  driver calls this function at necessary places.
* SYNTAX      : tVoid dev_volt_vTraceOut(tU32 u32Level,const tChar *pcFormatString,...)
* ARGUMENTS   : u32Level -  The trace level for output
*                  const tChar *pcFormatString - Format String
*
* RETURN VALUE: None
* NOTES       :   -
****************************************************************************/

static tVoid dev_volt_vTraceOut(  tU32 u32Level,const tChar *pcFormatString,... )
{
   if(LLD_bIsTraceActive((tU32)TR_CLASS_DEV_VOLT, u32Level) != FALSE)
   {
      /*
Parameter to hold the argument for a function, specified the format
string in pcFormatString
defined as:
typedef char* va_list in stdarg.h
*/
      va_list argList = {0};
      /*
vsnprintf Returns Number of bytes Written to buffer or a negative
value in case of failure
*/
      tS32 s32Size ;
      /*
Buffer to hold the string to trace out
*/
      tS8 u8Buffer[MAX_TRACE_SIZE];
      /*
Position in buffer from where the format string is to be
concatenated
*/
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /*
Flush the String
*/
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /*
Copy the String to indicate the trace is from the RTC device
*/

      /*
Initialize the argList pointer to the beginning of the variable
arguement list
*/
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
Collect the format String's content into the remaining part of
the Buffer
*/
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,
                  sizeof(u8Buffer),
                  pcFormatString,
                  argList ) ) )
      {
         return;
      }

      /*
Trace out the Message to TTFis
*/
      LLD_vTrace( (tU32)TR_CLASS_DEV_VOLT,
      u32Level,
      u8Buffer,
      (tU32)s32Size );   /* Send string to Trace*/
      /*
Performs the appropiate actions to facilitate a normal return by a
function that has used the va_list object
*/
      va_end(argList);
   }
}

/****************************************************************************
* FUNCTION    : dev_volt_lock
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function gets the semaphore
* SYNTAX      : tBool dev_volt_lock()
* ARGUMENTS   : None
*
* RETURN VALUE: tBool - success or failure
* NOTES       :   -
****************************************************************************/

static tBool dev_volt_lock(tVoid)
{
   tBool bretvalue = FALSE;

   /*wait for semaphore*/
   if ((rVoltPrv.sem != OSAL_C_INVALID_HANDLE) &&
         (OSAL_s32SemaphoreWait(rVoltPrv.sem, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK))
   {
      bretvalue = TRUE;
   }
   return bretvalue;
}

/****************************************************************************
* FUNCTION    : dev_volt_unlock
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function releases the semaphore
* SYNTAX      : tBool dev_volt_unlock()
* ARGUMENTS   : None
*
* RETURN VALUE: None
* NOTES       :   -
****************************************************************************/

static tVoid dev_volt_unlock(tVoid)
{
   /*post the semaphore*/
   (tVoid)OSAL_s32SemaphorePost(rVoltPrv.sem);

}


/****************************************************************************
* FUNCTION    : dev_volt_s32createClient
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to create a client ID.
* SYNTAX      : tS32 dev_volt_s32createClient(tVoid)
* ARGUMENTS   : tVoid
*
* RETURN VALUE: index or error(DEV_VOLT_INVALID_ID)
* NOTES       :   -
****************************************************************************/


static tS32 dev_volt_s32createClient(tVoid)
{
   tS32 s32Clientid;
   tS32 s32SystemThresholdEntry;
   tS32 s32retvalue;
   trOsalIOClient *prClient;
   tS32 s32UserThresholdEntry;

   /*perform loop for all clients*/
   for(s32Clientid = 0; s32Clientid < DEV_VOLT_NR_OF_CLIENTS; ++s32Clientid)
   {
      prClient = &(prVoltDrvInf->arOsalIOClients[s32Clientid]);

      /* check for client which is free */
      if (prClient->s32idClient == DEV_VOLT_INVALID_ID)
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32createClient:Client ID:%d",s32Clientid);

         /*assign this clientID to the structure*/
         prClient->s32idClient = s32Clientid;

         /*perform handle initialization to system threshold structure*/
         for(s32SystemThresholdEntry = 0; s32SystemThresholdEntry < DEV_VOLT_NR_OF_SYSTEM_THRESHOLDS; s32SystemThresholdEntry++)
         {
            /*  set handle to illegal value */
            prClient->arSystemThresholds[s32SystemThresholdEntry].hdl = OSAL_C_INVALID_HANDLE;
         }

         /* perform handle initialization to user threshold structure */
         for(s32UserThresholdEntry = 0; s32UserThresholdEntry < DEV_VOLT_NR_OF_USER_THRESHOLDS; ++s32UserThresholdEntry)
         {
            prClient->arUserThresholds[s32UserThresholdEntry].hdl = OSAL_C_INVALID_HANDLE;
         }
         break;
      }
   }

   if(s32Clientid < DEV_VOLT_NR_OF_CLIENTS)
   {
      s32retvalue = s32Clientid;
   }
   else
   {
      s32retvalue = DEV_VOLT_INVALID_ID;
   }

   return s32retvalue;
}

/****************************************************************************
* FUNCTION    : dev_volt_s32removeClient
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to remove a client ID..
* SYNTAX      : tS32 dev_volt_s32removeClient(tS32 s32Client)
* ARGUMENTS   : s32Client - client ID
*
* RETURN VALUE: OSAL_E_NOERROR or OSAL_E_DOESNOTEXIST
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32removeClient(tS32 s32Client)
{
   tS32 s32retvalue ;

   if (prVoltDrvInf->arOsalIOClients[s32Client].s32idClient == s32Client)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32removeClient:Matching Client ID:%d",s32Client);

      /* Free this client entry */
      prVoltDrvInf->arOsalIOClients[s32Client].s32idClient = DEV_VOLT_INVALID_ID;
      s32retvalue = OSAL_E_NOERROR;
   }
   else
   {
      /* don't find an entry */
      s32retvalue = OSAL_E_DOESNOTEXIST;
   }

   return s32retvalue;
}

/****************************************************************************
* FUNCTION    : dev_volt_s32findUserThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to find user threshold among all user threshold.
* SYNTAX      : tS32 dev_volt_s32findUserThreshold(const tsOsalIOInternUserThreshold arUserThreshold[], tU32 u32threshold)
* ARGUMENTS   : arUserThreshold - array of user threshold info
*               u32threshold - threshold value
*
* RETURN VALUE: index or error(DEV_VOLT_INVALID_ID)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32findUserThreshold(const trOsalIOInternUserThreshold arUserThreshold[], tU32 u32threshold)
{
   tS32 s32UserThresholdEntry;
   tS32 s32retvalue;

   /*perform loop for all user threshold*/
   for(s32UserThresholdEntry = 0; s32UserThresholdEntry < DEV_VOLT_NR_OF_USER_THRESHOLDS; ++s32UserThresholdEntry)
   {
      /*check for valid handle and id of the threshold matches*/
      if (arUserThreshold[s32UserThresholdEntry].hdl != (OSAL_tEventHandle)OSAL_C_INVALID_HANDLE
                  && arUserThreshold[s32UserThresholdEntry].s32idThreshold == (tS32)u32threshold)
      {
         break; /* return index of entry*/
      }
   }
   if(s32UserThresholdEntry < DEV_VOLT_NR_OF_USER_THRESHOLDS)
   {
      s32retvalue = s32UserThresholdEntry;
   }
   else
   {
      s32retvalue = DEV_VOLT_INVALID_ID;/* not found */
   }

   return s32retvalue;
}

/****************************************************************************
* FUNCTION    : dev_volt_s32findUserThresholdGroup
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to find user threshold group among all user threshold.
* SYNTAX      : tS32 dev_volt_s32findUserThresholdGroup(const tsOsalIOInternUserThreshold arUserThreshold[], tU32 u32group)
* ARGUMENTS   : arUserThreshold - array of user threshold info
*               u32group - group value
*
* RETURN VALUE: index or error(DEV_VOLT_ERROR)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32findUserThresholdGroup(const trOsalIOInternUserThreshold arUserThreshold[], tU32 u32group)
{
   tS32 s32UserThresholdEntry;
   tS32 s32retvalue;

   /*perform loop for all user threshold*/
   for(s32UserThresholdEntry = 0; s32UserThresholdEntry < DEV_VOLT_NR_OF_USER_THRESHOLDS; ++s32UserThresholdEntry)
   {
      /*check for valid handle and id of the group matches*/
      if (arUserThreshold[s32UserThresholdEntry].hdl != (OSAL_tEventHandle)OSAL_C_INVALID_HANDLE
                  && arUserThreshold[s32UserThresholdEntry].s32idGroup == (tS32)u32group)
      {
         break; /* return index of entry*/
      }
   }
   if(s32UserThresholdEntry < DEV_VOLT_NR_OF_USER_THRESHOLDS)
   {
      s32retvalue = s32UserThresholdEntry;
   }
   else
   {
      s32retvalue = DEV_VOLT_ERROR;/* not found */
   }

   return s32retvalue;
}

/****************************************************************************
* FUNCTION    : dev_volt_s32findEmptyUserThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to find empty user threshold among all user threshold.
* SYNTAX      : tS32 dev_volt_s32findEmptyUserThreshold(const tsOsalIOInternUserThreshold arUserThreshold[])
* ARGUMENTS   : arUserThreshold - array of user threshold info
*
* RETURN VALUE: index or error(DEV_VOLT_INVALID_ID)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32findEmptyUserThreshold(const trOsalIOInternUserThreshold arUserThreshold[])
{
   tS32 s32UserThresholdEntry;
   tS32 s32retvalue;

   /*perform loop for all user threshold*/
   for(s32UserThresholdEntry = 0; s32UserThresholdEntry < DEV_VOLT_NR_OF_USER_THRESHOLDS; ++s32UserThresholdEntry)
   {
      /*check for invalid handle*/
      if (arUserThreshold[s32UserThresholdEntry].hdl == (OSAL_tEventHandle)OSAL_C_INVALID_HANDLE)
      {
         break;
      }
   }
   if(s32UserThresholdEntry < DEV_VOLT_NR_OF_USER_THRESHOLDS)
   {
      s32retvalue = s32UserThresholdEntry;
   }
   else
   {
      s32retvalue = DEV_VOLT_INVALID_ID;
   }

   return s32retvalue;
}

/****************************************************************************
* FUNCTION    : dev_volt_s32findEmptySystemThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to find empty system threshold.
* SYNTAX      : tS32 dev_volt_s32findEmptySystemThreshold(const trOsalIOInternSystemThreshold arOsalSysThresh[])
* ARGUMENTS   : arOsalSysThresh - array of system threshold info
*
* RETURN VALUE: index or error(DEV_VOLT_INVALID_ID)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32findEmptySystemThreshold(const trOsalIOInternSystemThreshold arOsalSysThresh[])
{
   tS32 s32SystemThresholdEntry;
   tS32 s32retvalue;

   /*perform loop for system threshold*/
   for(s32SystemThresholdEntry = 0; s32SystemThresholdEntry < DEV_VOLT_NR_OF_SYSTEM_THRESHOLDS; ++s32SystemThresholdEntry)
   {
      /*check for invalid handle*/
      if ((arOsalSysThresh[s32SystemThresholdEntry].hdl == (OSAL_tEventHandle)OSAL_C_INVALID_HANDLE) &&
            (arOsalSysThresh[s32SystemThresholdEntry].s32idThreshold == DEV_VOLT_INVALID_ID))
      {
         break;
      }
   }
   if(s32SystemThresholdEntry < DEV_VOLT_NR_OF_SYSTEM_THRESHOLDS)
   {
      s32retvalue = s32SystemThresholdEntry;
   }
   else
   {
      s32retvalue = DEV_VOLT_INVALID_ID;
   }

   return s32retvalue;
}

/****************************************************************************
* FUNCTION    : dev_volt_s32findValidSystemThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to find valid system threshold.
* SYNTAX      : tS32 dev_volt_s32findValidSystemThreshold(const trOsalIOInternSystemThreshold arOsalSysThresh[])
* ARGUMENTS   : arOsalSysThresh - array of system threshold info
*
* RETURN VALUE: index or error(DEV_VOLT_INVALID_ID)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32findValidSystemThreshold(const trOsalIOInternSystemThreshold arOsalSysThresh[])
{
   tS32 s32SystemThresholdEntry;
   tS32 s32retvalue;

   /*perform loop for system threshold*/
   for(s32SystemThresholdEntry = 0; s32SystemThresholdEntry < DEV_VOLT_NR_OF_SYSTEM_THRESHOLDS; ++s32SystemThresholdEntry)
   {
      /*check for invalid idthreshold*/
      if (arOsalSysThresh[s32SystemThresholdEntry].s32idThreshold != DEV_VOLT_INVALID_ID)
      {
         break;
      }
   }
   if(s32SystemThresholdEntry < DEV_VOLT_NR_OF_SYSTEM_THRESHOLDS)
   {
      s32retvalue = s32SystemThresholdEntry;
   }
   else
   {
      s32retvalue = DEV_VOLT_INVALID_ID;
   }

   return s32retvalue;
}

/****************************************************************************
* FUNCTION    : dev_volt_s32addUserThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to add user threshold.
* SYNTAX      : tS32 dev_volt_s32addUserThreshold(const OSAL_tVoltUserThresholdNotification* prOsalUserThres)
* ARGUMENTS   : prOsalUserThres - User threshold notification info
*
* RETURN VALUE: success or error(DEV_VOLT_ERROR)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32addUserThreshold(const OSAL_tVoltUserThresholdNotification* prOsalUserThres)
{
   tS32 s32retvalue = OSAL_E_NOERROR;
   tS32 s32UserThresholdEntry;
   trOsalIOInternUserThreshold *prUserThreshold;
   tS32 s32idClient = prOsalUserThres->idClient;

   /*check for valid clientID*/
   if (s32idClient < 0 || s32idClient >= DEV_VOLT_NR_OF_CLIENTS ||
         prVoltDrvInf->arOsalIOClients[s32idClient].s32idClient != s32idClient)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Client not created:FAILED");
      s32retvalue = OSAL_E_INVALIDVALUE;/* index is not valid */
   }

   /*find user threshold for the given idthreshold*/
   else if (dev_volt_s32findUserThreshold(prVoltDrvInf->arOsalIOClients[s32idClient].arUserThresholds,
               (tU32)prOsalUserThres->idThreshold) != DEV_VOLT_INVALID_ID)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32findUserThreshold:FAILED");
      s32retvalue = OSAL_E_INVALIDVALUE; /* threshold already defined*/
   }

   /*find an empty user threshold*/
   else if((s32UserThresholdEntry = dev_volt_s32findEmptyUserThreshold(prVoltDrvInf->arOsalIOClients[s32idClient].arUserThresholds)) == DEV_VOLT_INVALID_ID)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32findEmptyUserThreshold:FAILED");
      s32retvalue = OSAL_E_NOSPACE; /* array is full */
   }
   else
   {
      prUserThreshold = &prVoltDrvInf->arOsalIOClients[s32idClient].arUserThresholds[s32UserThresholdEntry];
      /*Assign the details provided by user to the driver data structure*/
      prUserThreshold->hdl = prOsalUserThres->hdl;
      prUserThreshold->s32idThreshold = prOsalUserThres->idThreshold;
      prUserThreshold->u32mask = prOsalUserThres->mask;
      prUserThreshold->s32idGroup = prOsalUserThres->idGroup;
      prUserThreshold->u32idx = 0; // history is empty

      /*calculate threshold lower limit and update it in driver data structure*/
      prUserThreshold->u32thresholdLowerLimit = prOsalUserThres->thresholdVoltage - prOsalUserThres->thresholdHysteresis;

      /* check for the calculated lower limit with the lower limit supported by this hardware */
      if (prUserThreshold->u32thresholdLowerLimit < DEV_VOLT_LOWESTVOLT)
      {
         prUserThreshold->u32thresholdLowerLimit = DEV_VOLT_LOWESTVOLT;
      }

      /*calculate threshold upper limit and update it in driver data structure*/
      prUserThreshold->u32thresholdUpperLimit = prOsalUserThres->thresholdVoltage + prOsalUserThres->thresholdHysteresis;

      /*check for the calculated upper limit with the upper limit supported by this hardware*/
      if (prUserThreshold->u32thresholdUpperLimit > DEV_VOLT_HIGHESTVOLT)
      {
         prUserThreshold->u32thresholdUpperLimit = DEV_VOLT_HIGHESTVOLT;
      }

      prUserThreshold->u32active = EN_VOLT_EVVOLT_INVALID;

      /* Perform user level notification */
      vUserNotify((tU32)s32idClient, (tU32)s32UserThresholdEntry, prVoltDrvInf->arOsalIOClients,prVoltDrvInf->u32voltage);
   }

   return s32retvalue;
}

/****************************************************************************
* FUNCTION    : dev_volt_s32removeUserThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to remove user threshold.
* SYNTAX      : tS32 dev_volt_s32removeUserThreshold(const OSAL_tVoltRemoveThresholdNotification* prRemoveThresh)
* ARGUMENTS   : prRemoveThresh - User threshold notification info
*
* RETURN VALUE: success or error(DEV_VOLT_ERROR)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32removeUserThreshold(const OSAL_tVoltRemoveThresholdNotification* prRemoveThresh)
{
   tS32 s32UserThresholdEntry;
   tS32 s32retvalue = OSAL_E_NOERROR;
   tU32 u32UserCount;
   trOsalIOInternUserThreshold *prUserThreshold;
   tS32 s32idClient = prRemoveThresh->idClient;

   /*check for valid clientID*/
   if (s32idClient < 0 || s32idClient >= DEV_VOLT_NR_OF_CLIENTS ||
         prVoltDrvInf->arOsalIOClients[s32idClient].s32idClient != s32idClient)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Client not created:FAILED");
      s32retvalue = OSAL_E_INVALIDVALUE;
   }

   /*find user threshold for the given id of the threshold*/
   else if((s32UserThresholdEntry = dev_volt_s32findUserThreshold(prVoltDrvInf->arOsalIOClients[s32idClient].arUserThresholds, (tU32)prRemoveThresh->idThreshold)) == DEV_VOLT_INVALID_ID)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"finduserthreshold:FAILED");
      s32retvalue = OSAL_E_INVALIDVALUE; /* not found */
   }
   else
   {
      prUserThreshold = &prVoltDrvInf->arOsalIOClients[s32idClient].arUserThresholds[s32UserThresholdEntry];
      /*check for valid id of the threshold */
      if ((prUserThreshold->s32idThreshold < E_VOLT_FIRST_USER_THRESHOLD ))
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"User threshold value is invalid:FAILED");
         s32retvalue = OSAL_E_INVALIDVALUE; /*lint !e648*/
      }
      else
      {
         /*perform loop until all user threshold*/
         for (u32UserCount = 0; u32UserCount < DEV_VOLT_NR_OF_USER_THRESHOLDS; ++u32UserCount)
         {
            prUserThreshold = &prVoltDrvInf->arOsalIOClients[s32idClient].arUserThresholds[u32UserCount];
            if(prRemoveThresh->idThreshold == prUserThreshold->s32idThreshold)
            {
               /*Assign invalid value to the user threshold structure*/
               prUserThreshold->hdl = (OSAL_tEventHandle)OSAL_C_INVALID_HANDLE;
               prUserThreshold->s32idThreshold = DEV_VOLT_INVALID_ID; // invalidate entry of array
            }
         }
      }
   }

   return s32retvalue; /* successfully removed the user threshold */
}

/****************************************************************************
* FUNCTION    : dev_volt_s32addSystemThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to add system threshold.
* SYNTAX      : tS32 dev_volt_s32addSystemThreshold(const OSAL_tVoltSystemThresholdNotification* prOsalSystemThreshold)
* ARGUMENTS   : prOsalSystemThreshold - system threshold notification info
*
* RETURN VALUE: index or error(DEV_VOLT_ERROR)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32addSystemThreshold(const OSAL_tVoltSystemThresholdNotification* prOsalSystemThreshold)
{
   tS32 s32retvalue = OSAL_E_NOERROR;
   trOsalIOInternSystemThreshold *prSystemThreshold;
   tS32 s32idClient = prOsalSystemThreshold->idClient;
   tS32 s32SystemThresholdEntry;

   /* check for valid clientID */
   if (s32idClient < 0 || s32idClient >= DEV_VOLT_NR_OF_CLIENTS ||
         prVoltDrvInf->arOsalIOClients[s32idClient].s32idClient != s32idClient)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Client not created:FAILED");
      s32retvalue = OSAL_E_INVALIDVALUE; /* index is not valid */
   }
   else
   {
      /* find an empty system threshold */
      if ((s32SystemThresholdEntry = dev_volt_s32findEmptySystemThreshold(prVoltDrvInf->arOsalIOClients[s32idClient].arSystemThresholds)) == DEV_VOLT_INVALID_ID)
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32findEmptySystemThreshold:FAILED");
         s32retvalue = OSAL_E_NOSPACE; /* array is full */
      }
      else
      {
         prSystemThreshold = &(prVoltDrvInf->arOsalIOClients[s32idClient].arSystemThresholds[s32SystemThresholdEntry]);

         /*check whether its already registered or not*/
         if (prSystemThreshold->s32idThreshold == DEV_VOLT_INVALID_ID)
         {
            prSystemThreshold->hdl = prOsalSystemThreshold->hdl;
            prSystemThreshold->s32kind = prOsalSystemThreshold->kind;
            prSystemThreshold->u32mask = prOsalSystemThreshold->mask;
            prSystemThreshold->u32idx = 0; /* empty history */
            /* Finally mark this entry as valid */
            prSystemThreshold->s32idThreshold = E_VOLT_SYSTEM_THRESHOLDS;
         }
         else
         {
            s32retvalue = OSAL_ERROR;
         }
      }
   }

   return s32retvalue; /* callbacks were successfully registered to volt driver */
}

/****************************************************************************
* FUNCTION    : dev_volt_s32removeSystemThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to remove system threshold.
* SYNTAX      : tS32 dev_volt_s32removeSystemThreshold(tS32 s32RemoveThresh)
* ARGUMENTS   : s32RemoveThresh - system threshold notification info
*
* RETURN VALUE: index or error(DEV_VOLT_ERROR)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32removeSystemThreshold(tS32 s32RemoveThresh)
{
   tS32 s32retvalue = OSAL_E_NOERROR;
   tS32 s32SystemThresholdEntry;
   OSAL_tVoltRemoveThresholdNotification *prThresRemvNotify;
   trOsalIOInternSystemThreshold *prSystemThreshold;
   tS32 s32idClient;

   prThresRemvNotify = (OSAL_tVoltRemoveThresholdNotification *)s32RemoveThresh;
   s32idClient = prThresRemvNotify->idClient;

   /*check for valid clientID*/
   if (s32idClient < 0 || s32idClient >= DEV_VOLT_NR_OF_CLIENTS ||
         prVoltDrvInf->arOsalIOClients[s32idClient].s32idClient != s32idClient)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Client not created:FAILED");
      s32retvalue = OSAL_E_INVALIDVALUE; /* index is not valid */
   }
   else
   {
      /*check whether its registered*/
      if ((s32SystemThresholdEntry = dev_volt_s32findValidSystemThreshold(prVoltDrvInf->arOsalIOClients[s32idClient].arSystemThresholds)) == DEV_VOLT_INVALID_ID)
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32findValidSystemThreshold:FAILED");
         s32retvalue = DEV_VOLT_ERROR;
      }
      else
      {
         prSystemThreshold = &prVoltDrvInf->arOsalIOClients[s32idClient].arSystemThresholds[s32SystemThresholdEntry];
         if (prSystemThreshold->s32idThreshold == E_VOLT_SYSTEM_THRESHOLDS)
         {
            prSystemThreshold->hdl = OSAL_C_INVALID_HANDLE;
            prSystemThreshold->s32kind = OSAL_ERROR;
            prSystemThreshold->s32idThreshold = DEV_VOLT_INVALID_ID;
         }
         else
         {
            s32retvalue = OSAL_ERROR;
         }
      }
   }
   return s32retvalue; /* sucessfully released system threshold */
}

/****************************************************************************
* FUNCTION    : dev_volt_s32getSystemThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to retrieve the system threshold level history.
* SYNTAX      : tS32 dev_volt_s32getSystemThreshold(OSAL_tVoltSystemThresholdHistory* prOsalSysHistory)
* ARGUMENTS   : prOsalSysHistory - system threshold history info
*
* RETURN VALUE: success or error(DEV_VOLT_ERROR)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32getSystemThreshold(OSAL_tVoltSystemThresholdHistory* prOsalSysHistory)
{
   tS32 s32retvalue = OSAL_E_NOERROR;
   tS32 s32buffref;
   tS32 s32SystemThresholdEntry;
   tU8 au8Events[OSAL_VOLT_MAX_SYSTEM_HISTORY];
   trOsalIOInternSystemThreshold *prSystemThreshold;
   tS32 s32idClient = prOsalSysHistory->idClient;

   /*check for valid clientID*/
   if (s32idClient < 0 || s32idClient >= DEV_VOLT_NR_OF_CLIENTS ||
         prVoltDrvInf->arOsalIOClients[s32idClient].s32idClient != s32idClient)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32getSystemThreshold:Client not created:FAILED");
      s32retvalue = OSAL_E_INVALIDVALUE; /* index is not valid */
   }
   else
   {
      if ((s32SystemThresholdEntry = dev_volt_s32findValidSystemThreshold(prVoltDrvInf->arOsalIOClients[s32idClient].arSystemThresholds)) == DEV_VOLT_INVALID_ID)
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32findValidSystemThreshold:FAILED");
         s32retvalue = DEV_VOLT_ERROR;
      }
      else
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32findValidSystemThreshold:s32SystemThresholdEntry:%d",s32SystemThresholdEntry);
         prSystemThreshold = &(prVoltDrvInf->arOsalIOClients[s32idClient].arSystemThresholds[s32SystemThresholdEntry]);
         /*check for the kind*/
         if (prSystemThreshold->s32kind == prOsalSysHistory->kind)
         {
            if (dev_volt_lock())
            {
               /*check for buffer overflow*/
               if(prSystemThreshold->bBuffRefill)
               {
                  dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32getSystemThreshold:Buffer refill is set");
                  for(s32buffref = 0; s32buffref < OSAL_VOLT_MAX_SYSTEM_HISTORY; s32buffref++)
                  {
                     /*Store the events according to the sequence occured*/
                     au8Events[s32buffref] = prSystemThreshold->au8Events[prSystemThreshold->u32idx - 1];
                     prSystemThreshold->u32idx++;
                     if(prSystemThreshold->u32idx > OSAL_VOLT_MAX_SYSTEM_HISTORY)
                     {
                        prSystemThreshold->u32idx = 1;
                     }
                  }
                  /*Update the user buffer*/
                  OSAL_pvMemoryCopy(prOsalSysHistory->arEvents, au8Events, sizeof(prOsalSysHistory->arEvents));
                  prOsalSysHistory->idx = OSAL_VOLT_MAX_SYSTEM_HISTORY;
                  prSystemThreshold->bBuffRefill = FALSE;
               }
               else
               {
                  /*update to the user provided buffer*/
                  OSAL_pvMemoryCopy(prOsalSysHistory->arEvents, prSystemThreshold->au8Events,
                                    sizeof(prOsalSysHistory->arEvents));
                  prOsalSysHistory->idx = prSystemThreshold->u32idx;
               }
               prSystemThreshold->u32idx = 0; // empty history
               dev_volt_unlock();
            }
         }
      }
   }

   return s32retvalue; // the kind of events is not available
}

/****************************************************************************
* FUNCTION    : dev_volt_s32getUserThreshold
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is to retrieve the user threshold level history.
* SYNTAX      : tS32 dev_volt_s32getUserThreshold(OSAL_tVoltUserThresholdHistory* prOsalUserHistory)
* ARGUMENTS   : prOsalUserHistory - user threshold history info
*
* RETURN VALUE: success or error(DEV_VOLT_ERROR)
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_s32getUserThreshold(OSAL_tVoltUserThresholdHistory* prOsalUserHistory)
{
   tS32 s32UserThresholdEntry;
   tS32 s32Usergrp;
   tS32 s32retvalue = OSAL_OK;
   tS32 s32buffref;
   tU8 au8Events[OSAL_VOLT_MAX_USER_HISTORY];
   tU8 au8Clients[OSAL_VOLT_MAX_USER_HISTORY];
   trOsalIOInternUserThreshold *prUserThreshold;
   tS32 s32idClient = prOsalUserHistory->idClient;

   /*check for valid clientID*/
   if (s32idClient < 0 || s32idClient >= DEV_VOLT_NR_OF_CLIENTS ||
         prVoltDrvInf->arOsalIOClients[s32idClient].s32idClient != s32idClient)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"Client not created:FAILED");
      s32retvalue = DEV_VOLT_ERROR;
   }
   /*find the user threshold for the given id of the threshold*/
   else if ((s32UserThresholdEntry = dev_volt_s32findUserThreshold(prVoltDrvInf->arOsalIOClients[s32idClient].arUserThresholds, (tU32)prOsalUserHistory->idThreshold)) == DEV_VOLT_INVALID_ID)
   {
      dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"dev_volt_s32findUserThreshold:FAILED");
      s32retvalue = DEV_VOLT_ERROR; /* not found */
   }
   else
   {
      /*find the user group for the given id of the group*/
      s32Usergrp = dev_volt_s32findUserThresholdGroup(prVoltDrvInf->arOsalIOClients[s32idClient].arUserThresholds, (tU32)prOsalUserHistory->idGroup);
      if (dev_volt_lock())
      {
         if (s32Usergrp > 0) s32UserThresholdEntry = s32Usergrp;
         /*update to the user provided buffer*/
         prUserThreshold = &prVoltDrvInf->arOsalIOClients[s32idClient].arUserThresholds[s32UserThresholdEntry];
         /*check for buffer overflow*/
         if(prUserThreshold->bBuffRefill)
         {
            for(s32buffref = 0; s32buffref < OSAL_VOLT_MAX_USER_HISTORY; s32buffref++)
            {
               /*Store the events according to the sequence occured*/
               au8Events[s32buffref] = prUserThreshold->au8Events[prUserThreshold->u32idx - 1];
               au8Clients[s32buffref] = prUserThreshold->au8Clients[prUserThreshold->u32idx - 1];
               prUserThreshold->u32idx++;
               if(prUserThreshold->u32idx > OSAL_VOLT_MAX_USER_HISTORY)
               {
                  prUserThreshold->u32idx = 1;
               }
            }
            /*Update the user buffer*/
            OSAL_pvMemoryCopy(prOsalUserHistory->arEvents, au8Events, sizeof(prOsalUserHistory->arEvents));
            OSAL_pvMemoryCopy(prOsalUserHistory->arClients, au8Clients, sizeof(prOsalUserHistory->arClients));
            prOsalUserHistory->idx = OSAL_VOLT_MAX_USER_HISTORY;
            prUserThreshold->bBuffRefill = FALSE;
         }
         else
         {
            /*Update the user buffer*/
            OSAL_pvMemoryCopy(prOsalUserHistory->arEvents, prUserThreshold->au8Events,
                              sizeof(prOsalUserHistory->arEvents));
            OSAL_pvMemoryCopy(prOsalUserHistory->arClients, prUserThreshold->au8Clients,
                              sizeof(prOsalUserHistory->arClients));
            prOsalUserHistory->idx = prUserThreshold->u32idx;
         }
         prUserThreshold->u32idx = 0; // empty history
         dev_volt_unlock();
      }
   }

   return s32retvalue;
}




/****************************************************************************
* FUNCTION    : dev_volt_userCallback
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function gives the notification to the registered user for specified user threshold.
* SYNTAX      : tVoid dev_volt_userCallback(trVoltDrvInf* VtspDrvInf, tsOsalIOClients arClients, tU32 u32voltBits)
* ARGUMENTS   : VtspDrvInf - Driver data structure
*               u32voltBits - voltage value
*               arClients - Client data structure.
*
* RETURN VALUE: NONE
* NOTES       :   -
****************************************************************************/

static tVoid dev_volt_userCallback(trVoltDrvInf* VtspDrvInf, trOsalIOClient arClients[], tU32 u32voltBits)
{
   tU32 u32Client;
   tU32 u32user;

   (tVoid)VtspDrvInf;
   /*check for user callback is active*/

      /*perform loop until all clients*/
      for (u32Client = 0; u32Client < DEV_VOLT_NR_OF_CLIENTS; ++u32Client)
      {
         if(arClients[u32Client].s32idClient != DEV_VOLT_INVALID_ID)
         {
            /*perform loop until all user threshold*/
            for (u32user = 0; u32user < DEV_VOLT_NR_OF_USER_THRESHOLDS; ++u32user)
            {
               vUserNotify(u32Client, u32user, arClients, u32voltBits);
            }
         }
      }

}


/****************************************************************************
* FUNCTION    : vUserNotify
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function gives the notification to the registered user for specified user threshold.
* SYNTAX      : tVoid vUserNotify(tU32 u32Client, tU32 u32user, trOsalIOClient arClients, tU32 u32voltBits)
* ARGUMENTS   : u32Client - Client ID
*               u32user - User threshold ID
*               u32voltBits - voltage value
*               arClients - Client data structure.
*
* RETURN VALUE: NONE
* NOTES       :   -
****************************************************************************/

static tVoid vUserNotify(tU32 u32Client, tU32 u32user, trOsalIOClient arClients[], tU32 u32voltBits)
{
   /*check for valid id of the threshold*/
   if (arClients[u32Client].arUserThresholds[u32user].s32idThreshold >= E_VOLT_FIRST_USER_THRESHOLD)
   {
      /*check for underrun*/
      if (u32voltBits <= arClients[u32Client].arUserThresholds[u32user].u32thresholdLowerLimit)
      {
         // underrun found
         if (arClients[u32Client].arUserThresholds[u32user].u32active != EN_VOLT_EVVOLT_UNDERRUN)
         {

            dev_volt_vTraceOut( (tU32)TR_LEVEL_USER_3," vUserNotify called underrun:Client:%d,Volt:%d",u32Client,u32voltBits); /* Printing trace */

            /*check for valid handle*/
            if (arClients[u32Client].arUserThresholds[u32user].hdl != OSAL_C_INVALID_HANDLE)
            {
               /*perform user callback*/
               dev_volt_cb_user(EN_VOLT_EVVOLT_UNDERRUN,
               (tU32)arClients[u32Client].arUserThresholds[u32user].s32idThreshold,
               u32Client);
            }

         }
         arClients[u32Client].arUserThresholds[u32user].u32active = EN_VOLT_EVVOLT_UNDERRUN;
      }
      else if (u32voltBits >= arClients[u32Client].arUserThresholds[u32user].u32thresholdUpperLimit)/*check for overrrun*/
      {
         // overrun found
         if (arClients[u32Client].arUserThresholds[u32user].u32active != EN_VOLT_EVVOLT_OVERRUN)
         {

            dev_volt_vTraceOut( (tU32)TR_LEVEL_USER_3,"vUserNotify called overrun:Client:%d,Volt:%d",u32Client,u32voltBits); /* Printing trace */


            /*check for valid handle*/
            if (arClients[u32Client].arUserThresholds[u32user].hdl != OSAL_C_INVALID_HANDLE)
            {
               /*perform user callback*/
               dev_volt_cb_user(EN_VOLT_EVVOLT_OVERRUN,
               (tU32)arClients[u32Client].arUserThresholds[u32user].s32idThreshold,
               u32Client);
            }

         }
         arClients[u32Client].arUserThresholds[u32user].u32active = EN_VOLT_EVVOLT_OVERRUN;
      }
      else
      {
         // in range of the defined user threshold
         if (arClients[u32Client].arUserThresholds[u32user].u32active != EN_VOLT_EVVOLT_INRANGE)
         {

            dev_volt_vTraceOut( (tU32)TR_LEVEL_USER_3,"vUserNotify called inrange:Client:%d,Volt:%d",u32Client,u32voltBits); /* Printing trace */

            /*check for valid handle*/
            if (arClients[u32Client].arUserThresholds[u32user].hdl != OSAL_C_INVALID_HANDLE)
            {
               /*perform user callback*/
               dev_volt_cb_user(EN_VOLT_EVVOLT_INRANGE,
               (tU32)arClients[u32Client].arUserThresholds[u32user].s32idThreshold,
               u32Client);
            }


         }
         arClients[u32Client].arUserThresholds[u32user].u32active = EN_VOLT_EVVOLT_INRANGE;
      }
   }
}

/****************************************************************************
* FUNCTION    : dev_volt_cb_user
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function gives the notification to the registered user for specified user threshold.
* SYNTAX      : tVoid dev_volt_cb_user(tU32 u32reason, tU32 u32idThreshold,tU32 u32userdefined)
* ARGUMENTS   : u32reason - reason of the event occured
*               u32idThreshold - id of the threshold
*               u32userdefined - id of the client.
*
* RETURN VALUE: NONE
* NOTES       :   -
****************************************************************************/

static tVoid dev_volt_cb_user(tU32 u32reason, tU32 u32idThreshold,tU32 u32userdefined)
{
   tS32 s32idx2 = DEV_VOLT_ERROR;
   tS32 s32index;
   tS32 s32grp;
   tU8  u8origclient;
   tU32 u32Mask;
   trOsalIOInternUserThreshold *prUserThreshold;

   /*check for valid client ID*/
   if (u32userdefined < DEV_VOLT_NR_OF_CLIENTS)
   {
      /*find user threshold for the given id of the threshold*/
      s32index = dev_volt_s32findUserThreshold(prVoltDrvInf->arOsalIOClients[u32userdefined].arUserThresholds, u32idThreshold);
      if(s32index != DEV_VOLT_INVALID_ID)
      {
         prUserThreshold = &prVoltDrvInf->arOsalIOClients[u32userdefined].arUserThresholds[s32index];
         /*obtain the group ID*/
         s32grp = prUserThreshold->s32idGroup;
         /*obtain the id of the threshold*/
         u8origclient = (tU8)prUserThreshold->s32idThreshold;

         // is a group associated to this userthreshold?
         if (s32grp != 0)
         {
            s32idx2 = dev_volt_s32findUserThresholdGroup(prVoltDrvInf->arOsalIOClients[u32userdefined].arUserThresholds, (tU32)s32grp);
         }
         /*obtain the mask*/
         u32Mask = prUserThreshold->u32mask;
         /*check for the valid handle*/
         if (u32Mask != DEV_VOLT_MASK_NOT_SET && prUserThreshold->hdl != (OSAL_tEventHandle)OSAL_C_INVALID_HANDLE)
         {
            if (dev_volt_lock())
            {
               if (s32idx2 >= 0)
               {
                  s32index = s32idx2; // only log the events in the group user threshold
                  prUserThreshold = &prVoltDrvInf->arOsalIOClients[u32userdefined].arUserThresholds[s32index];
               }

               if(prUserThreshold->u32idx == OSAL_VOLT_MAX_USER_HISTORY)
               {
                  prUserThreshold->bBuffRefill = TRUE;
                  prUserThreshold->u32idx = 0;
               }
               prUserThreshold->au8Events[prUserThreshold->u32idx] = (tU8)u32reason;
               prUserThreshold->au8Clients[prUserThreshold->u32idx] = u8origclient;
               prUserThreshold->u32idx++;
               dev_volt_unlock();

               /*post the event to the registered user threshold*/
               OSAL_s32EventPost(prUserThreshold->hdl,
                                 u32Mask,
                                 OSAL_EN_EVENTMASK_OR);
            }
         }
      }
   }
}

/****************************************************************************
* FUNCTION    : dev_volt_cvm_TimerCallback
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function triggers the event.
* SYNTAX      : tVoid dev_volt_cvm_TimerCallback(tVoid)
* ARGUMENTS   : NONE
*
* RETURN VALUE: NONE
* NOTES       :   -
****************************************************************************/

static tVoid dev_volt_cvm_TimerCallback(tVoid)
{
   trVoltMsg rMsg;

   rMsg.s32Command = DEV_VOLT_CMD_DUMMY_MSG;/*Invalid value inorder to ensure that message queue is processing*/
   /*check for LOW VOLTAGE DELAY STATE*/
   if(prVoltDrvInf->u32DrvCvmState == DEV_VOLT_CVM_LOW_VOLTAGE_DELAY)
   {
      rVoltPrv.s32ExecTimer = DEV_VOLT_EXECUTE_TIMER;
      /*Post the message with the current value*/
      if(OSAL_s32MessageQueuePost(rVoltPrv.hSPMvoltMq,(tPCU8)&rMsg,sizeof(trVoltMsg),DEV_VOLT_MQ_TIMER_PRIORITY) == OSAL_ERROR)
      {
         dev_volt_vTraceOut((tU32)TR_LEVEL_USER_3,"MessageQueue post failed in TIMER_trace_callback");
      }
   }
}

/****************************************************************************
* FUNCTION    : dev_volt_cvm_eStartTimer
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function starts the timer.
* SYNTAX      : tS32 dev_volt_cvm_eStartTimer(tU32 u32PtimTimeValue)
* ARGUMENTS   : u32PtimTimeValue - time value
*
* RETURN VALUE: success or error
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_cvm_eStartTimer(tU32 u32PtimTimeValue)
{
   return(OSAL_s32TimerSetTime(rVoltPrv.hTimer,u32PtimTimeValue,DEV_VOLT_TIMER_INTERVAL));
}

/****************************************************************************
* FUNCTION    : dev_volt_cvm_eStopTimer
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function stops the timer.
* SYNTAX      : tS32 dev_volt_cvm_eStopTimer(tVoid)
* ARGUMENTS   : NONE
*
* RETURN VALUE: success or error
* NOTES       :   -
****************************************************************************/

static tS32 dev_volt_cvm_eStopTimer(tVoid)
{
   return(OSAL_s32TimerSetTime(rVoltPrv.hTimer,DEV_VOLT_TIMER_MSEC,DEV_VOLT_TIMER_INTERVAL));
}


#ifdef __cplusplus
}
#endif
