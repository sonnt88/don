/**********************************************************FileHeaderBegin******
*
* FILE:        dev_volt.h
*
* CREATED:     
*
* AUTHOR:		Suresh Dhandapani(RBEI/ECF5)
*
* DESCRIPTION: - This implementation provides the voltage support for LSIM
*
* NOTES: -
*
* COPYRIGHT:  (c) 2012 Robert Bosch Engineering and Business solutions Limited
*
**********************************************************FileHeaderEnd*******/

#ifndef _DEV_VOLT_H
#define _DEV_VOLT_H

/************************************************************************ 
|typedefs (scope: module-local) 
|-----------------------------------------------------------------------*/

/**********Function declarations**************************************************************/
tS32 dev_volt_OsalIO_s32Init(tVoid);
tS32 dev_volt_OsalIO_s32DeInit(tVoid);
tS32 dev_volt_s32IOOpen(tVoid);
tS32 dev_volt_s32IOClose(tVoid);
tS32 dev_volt_s32IOControl(tS32 fun, tS32 arg);
tVoid dev_volt_set_current(tPCVoid pvTraceData);
tVoid dev_volt_set_voltage(tPCVoid pvTraceData);
tU32 u32Test_SPMVOLT( void );
/******************************************end****************************************************************/

#endif
