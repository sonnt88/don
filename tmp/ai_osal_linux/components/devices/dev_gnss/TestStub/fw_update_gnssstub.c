/******************************************************************************
* FILE         : GnssSimApp.c
*             
* DESCRIPTION  : This file implements a test stub that simulates v850 for GNSS proxy.
*                In GEN3 GNSS hardware is present on V850. GNSS data will be sent from V850 to imx via INC.
*                It is the job of GNSS proxy module to collect this data, and deliver this to VD-Sensor.
*                As we dont have v850 up presently, functionality of GNSS proxy is tested 
*                using this test stub. It communicates with sensor proxy on imx via socket using TCP/IP.
*                IP Address of imx: 172.17.0.1 PORT on IMX: 0x08
*                IP addredd of ubuntu in which test stub is executed shall be : 172.17.0.6  PORT on UBUNTU: 0x08
* 
* AUTHOR(s)    : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 17.APR.2013|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<limits.h>
#include<errno.h>
#include<sys/types.h>

/* inet_pton() */
#include <arpa/inet.h>
/* htons() */
#include <netinet/in.h>
#include<sys/socket.h>

#include <time.h>

/* To Comply with bosch standards */

typedef unsigned char tU8;
typedef signed char tS8;
typedef unsigned int tU32;
typedef const char * tCString;
typedef unsigned char tBool;
typedef unsigned short tU16;
typedef int tS32;
typedef void * tPVoid;
typedef void tVoid;

/* OUR error codes. */
#define OSAL_OK 0
#define OSAL_ERROR -1

/*  Data sizes af different message fields  */
#define STATUS_MSG_SIZE (3)
#define CONFIG_MSG_SIZE (5)
#define REJECT_MSG_SIZE (3)
#define FLASH_BEGIN_SIZE (3)
#define FLASH_BUF_SIZE (3)
#define FLASH_END_SIZE (3)
#define FLASH_DATA_SIZE (3)
#define ACK_MSG_SIZE (3)
#define HOST_RDY_SIZE (4)


/* Buffer to send data */
#define TRANS_BUFFER_SIZE (2048)
static tU8 u8Buff[TRANS_BUFFER_SIZE];

/* Handle to socket */
tS32 s32ConnFD;

/* For delay */
struct timespec req;

/* For incrementing sensor dummy data */
static int cnt = 0;
tU32 u32NmeaPosEnd = 0;



tVoid vCommunicateToAPP();
tVoid vStartFwUpdate();
tVoid vPrintData(tS32 s32Bytes);
tVoid vSendDataResponse();
tVoid vSendACK();
tVoid vPrintText(tS32 s32Bytes);

/* Start of program execution  */

tS32 main(tVoid)
{
   struct sockaddr_in rSocAttr;
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32OptState = 1;
   tS32 s32SocketFD= 0;

   memset(&rSocAttr,0, sizeof(rSocAttr));
   rSocAttr.sin_family      = AF_INET;
   rSocAttr.sin_port        = htons(0x06); // Todo: this has to be changed
   rSocAttr.sin_addr.s_addr = htonl(INADDR_ANY);

   /* create a socket
      SOCK_STREAM: sequenced, reliable, two-way, connection-based
      byte streams with an OOB data transmission mechanism.*/
   s32SocketFD = socket(AF_INET, SOCK_STREAM, 0);
   if(s32SocketFD == -1)
   {
      perror("socket() failed");
   }
   else
   {
      /* This option informs os to reuse the socket address even if it is busy*/
      if(setsockopt(s32SocketFD, SOL_SOCKET, SO_REUSEADDR,
                    &s32OptState, sizeof(s32OptState)) == -1)
      {
         perror( "!!!setsockopt() Failed");
      }
      /* Bind the socket created to a address */
      s32RetVal = bind(s32SocketFD, (struct sockaddr *)&rSocAttr, sizeof(rSocAttr));
      if(s32RetVal == -1)
      {
         perror("Bind Failed");
      }
      else
      {
         /*Listen system call makes the socket as a passive socket. 
                  i.e socket will be used to accept incoming connections*/
         s32RetVal = listen(s32SocketFD, 10);
         if(s32RetVal == -1)
         {
            perror("Listen Failed");
         }
         else
         {
            s32RetVal = OSAL_OK;
      s32ConnFD = accept(s32SocketFD, NULL, NULL);
           if(s32ConnFD == -1)
           {
              perror( "accept Failed !!!");
              /* This should never happen. Only reason for this is something went
                 wrong while configuring the socket*/
           }
      else
      {
               printf( "Socket creation and Setup completed successfully\n");
      vCommunicateToAPP(s32ConnFD);
      }
         }
      }
   }
   return s32RetVal;
}

/* All communications to APP (IMX) is done from here */
tVoid vCommunicateToAPP()
{

   tS32 s32ErrChk = OSAL_ERROR;
   tU32 u32ConfigSent = 0;
   tU32 u32Offset = 0;
   int i;

   /* First IMX is expected to send status. So call recv from socket */
   s32ErrChk = recv(s32ConnFD, &u8Buff, STATUS_MSG_SIZE,0);
   if(s32ErrChk == STATUS_MSG_SIZE)
   {
      /* Just tracce it out */
      printf("Msg ID %d  APP STATUS %d IMX firmware version %d\n",u8Buff[0], u8Buff[1], u8Buff[2]);
      /* Next its v850 trun to send status */
      u8Buff[0] = STATUS_MSG_SIZE; /* In every message sent to IMX, 
                                          (First_byte) = (Total bytes transmitted -1)*/
                                    
      u8Buff[1] = 0x21;/*  Message ID */
      u8Buff[2] = 0x01;/*  Status as active */
      u8Buff[3] = 0x01;/*  V850 Firmware version */
      s32ErrChk = write( s32ConnFD, &u8Buff, (STATUS_MSG_SIZE+1));
      if(s32ErrChk == (STATUS_MSG_SIZE+1))
      {
         printf("status sent sucessfully \n");
			vStartFwUpdate();
      }
      else
      {
         perror(" status write to socket failed  \n");
      }
   }
   else
   {
      perror("");
      printf("Un expexted status message size from IMX %d\n",s32ErrChk);
   }

while(1);
   
}

tVoid vStartFwUpdate()
{
	tS32 s32ErrChk = 0;
	int i =0;
	int datacnt = 0;
/* Wait for FLASH_BEGIN */

	s32ErrChk = recv(s32ConnFD, &u8Buff, STATUS_MSG_SIZE,0);
	if(s32ErrChk == FLASH_BEGIN_SIZE)
	{
		printf(" FLASH_BEGIN ret %d", s32ErrChk);
		vPrintData(s32ErrChk);
	}
	else
	{
		printf(" error wait FLASH_BEGIN ret %d", s32ErrChk);
		perror ("\n");
		exit(-1);
	}

	/* Send flash begin success */
   u8Buff[0] = FLASH_BEGIN_SIZE; /* In every message sent to IMX, 
                                       (First_byte) = (Total bytes transmitted -1)*/
                                 
   u8Buff[1] = 0x31;/*  Message ID */
   u8Buff[2] = 0x10;/*  FLASH_BEGIN ID */
   u8Buff[3] = 0x01;/*  OK */
	s32ErrChk = write( s32ConnFD, &u8Buff, (u8Buff[0] + 1));
	if(s32ErrChk == (u8Buff[0] + 1))
	{
		printf("FLASH_BEGIN sent sucessfully \n");
	}
	else
	{
		perror(" FLASH_BEGIN write to socket failed  \n");
		exit (-1);
	}

	/* Wait for flash buffer */

	s32ErrChk = recv(s32ConnFD, &u8Buff, STATUS_MSG_SIZE,0);
	if(s32ErrChk == FLASH_BUF_SIZE)
	{
		printf(" FLASH_BUF ret %d\n", s32ErrChk);
		vPrintData(s32ErrChk);
	}
	else
	{
		printf(" error wait FLASH_BUF ret %d\n", s32ErrChk);
		perror ("\n");
		exit(-1);
	}
	


	/* Send flash BUf response */
	u8Buff[0] = FLASH_BUF_SIZE + 2; /* In every message sent to IMX, 
													(First_byte) = (Total bytes transmitted -1)*/

	u8Buff[1] = 0x31;/*	Message ID */
	u8Buff[2] = 0x13;/*	FLASH_BUF ID */
	u8Buff[3] = 0x01;/*	FLASH_BUF state ok */
	u8Buff[4] = 0x20;/*	Size of buffer for flshing */
	u8Buff[5] = 0x00;/*	Size of buffer for flshing */
	s32ErrChk = write( s32ConnFD, &u8Buff, (u8Buff[0] + 1));
	if(s32ErrChk == (u8Buff[0] + 1))
	{
		printf("FLASH_BUF Res sent sucessfully\n");
	}
	else
	{
		perror("FLASH_BUF write to socket failed\n");
		exit (-1);
	}

	/* Receive XLoader frame: This is a fixed size frame of 3Buff sizes for testing */
	i =0;
	while (i < 6)
	{
		i++;
		s32ErrChk = recv(s32ConnFD, &u8Buff, TRANS_BUFFER_SIZE,0);
		if(s32ErrChk > 0)
		{
			//printf(" FLASH_BUF ret %d\n", s32ErrChk);
			vPrintText(s32ErrChk);
			vSendDataResponse();
		}
		else
		{
			printf(" error wait FLASH_BUF ret %d\n", s32ErrChk);
			perror ("\n");
			exit(-1);
		}
		sleep(1);
	}

	/* Send ACK */

		/* Send flash begin success */
   u8Buff[0] = ACK_MSG_SIZE; /* In every message sent to IMX, 
                                       (First_byte) = (Total bytes transmitted -1)*/
                                 
   u8Buff[1] = 0x31;/*  Message ID */
   u8Buff[2] = 0x12;/*  CHIP_RES ID */
   u8Buff[3] = 0xCC;/* ACKecho  */
	s32ErrChk = write( s32ConnFD, &u8Buff, (u8Buff[0] + 1));
	if(s32ErrChk == (u8Buff[0] + 1))
	{
		printf("\nX-LOader ACK sent sucessfully \n");
	}
	else
	{
		perror(" \n X-LOader ACK write to socket failed  \n");
		exit (-1);
	}
/* Wait for Host Ready */
		s32ErrChk = recv(s32ConnFD, &u8Buff, TRANS_BUFFER_SIZE,0);
	if(s32ErrChk == HOST_RDY_SIZE)
	{
		printf(" HOST_RDY ret %d\n", s32ErrChk);
		vPrintData(s32ErrChk);
	}
	else
	{
		printf(" error wait HOST_RDY ret %d\n", s32ErrChk);
		perror ("\n");
		exit(-1);
	}

/* Send ACK for HOST_RDY */
	/* Send ACK */

   u8Buff[0] = ACK_MSG_SIZE; /* In every message sent to IMX, 
                                       (First_byte) = (Total bytes transmitted -1)*/
   u8Buff[1] = 0x31;/*  Message ID */
   u8Buff[2] = 0x12;/*  CHIP_RES ID */
   u8Buff[3] = 0xCC;/*  ACK */
	s32ErrChk = write( s32ConnFD, &u8Buff, (u8Buff[0] + 1));
	if(s32ErrChk == (u8Buff[0] + 1))
	{
		printf("HOST_RDY ACK sent sucessfully \n");
	}
	else
	{
		perror(" HOST_RDY ACK write to socket failed  \n");
		exit (-1);
	}

/*  Wait for binary options */
	s32ErrChk = recv(s32ConnFD, &u8Buff, TRANS_BUFFER_SIZE,0);
	if(s32ErrChk > 0)
	{
		printf(" Bin OPts ret %d\n", s32ErrChk);
		vPrintData(s32ErrChk);
	}
	else
	{
		printf(" error wait Bin OPts ret %d\n", s32ErrChk);
		perror ("\n");
		exit(-1);
	}

/* Send ACK for Binary Opts */
	/* Send ACK */
	u8Buff[0] = ACK_MSG_SIZE; /* In every message sent to IMX, 
     (First_byte) = (Total bytes transmitted -1)*/

	u8Buff[1] = 0x31;/* Message ID */
	u8Buff[2] = 0x12;/* CHIP_RES ID */
	u8Buff[3] = 0xCC;/* ACK */
	s32ErrChk = write( s32ConnFD, &u8Buff, (u8Buff[0] + 1));
	if(s32ErrChk == (u8Buff[0] + 1))
	{
		printf("HOST_RDY ACK sent sucessfully \n");
	}
	else
	{
		perror("HOST_RDY ACK write to socket failed \n");
		exit (-1);
	}

	sleep(1);

		/* Receive XLoader frame: This is a fixed size frame of 3Buff sizes for testing */
	i =0;
	while (1)
	{
		i++;
//		printf("Calling wait on Teseo FW image");
		s32ErrChk = recv(s32ConnFD, &u8Buff, TRANS_BUFFER_SIZE,0);
		if(s32ErrChk > 0)
		{
			//printf(" Tes FW ret %d\n", s32ErrChk);
			vPrintText(s32ErrChk);
			vSendDataResponse();
			datacnt = (datacnt + s32ErrChk) - 3;
			/* 30 = 16KB */
			if (datacnt == 30)
			{
				//printf( " datacnt == 30 itr %d ",i );
				//printf( "\nSending ACK" );
				vSendACK();
				datacnt =0;
			}
			/* Data transfer complete */
			if (i == 15)
			{
				
				printf( "\n\nSending Final ACK Bye \n" );
				vSendACK();
				sleep(3);
				exit(0);
				datacnt =0;
			}
			
		}
		else
		{
			printf(" error wait Tes FW ret %d\n", s32ErrChk);
			perror ("\n");
			exit(-1);
		}
		sleep(1);
	}

	while(1);

}


tVoid vSendACK()
{
int s32ErrChk =0;
	/* Send ACK for Binary Opts */
		/* Send ACK */
		u8Buff[0] = ACK_MSG_SIZE; /* In every message sent to IMX, 
		  (First_byte) = (Total bytes transmitted -1)*/
	
		u8Buff[1] = 0x31;/* Message ID */
		u8Buff[2] = 0x12;/* CHIP_RES ID */
		u8Buff[3] = 0xCC;/* ACK */
		s32ErrChk = write( s32ConnFD, &u8Buff, (u8Buff[0] + 1));
		if(s32ErrChk == (u8Buff[0] + 1))
		{
			//printf("ACK sent sucessfully \n");
		}
		else
		{
			perror(" ACK write to socket failed \n");
			exit (-1);
		}


}
tVoid vPrintData(tS32 s32Bytes)
{
	int i =0;
	for (i=0; i< s32Bytes; i++)
		printf("[%d]:%x : %c\n", i, u8Buff[i], u8Buff[i]);

}


tVoid vPrintText(tS32 s32Bytes)
{
	int i =0;
	for (i=3; i< s32Bytes; i++)
		printf("%c",u8Buff[i]);

}


tVoid vSendDataResponse()
{
	tS32 s32ErrChk = 0;
		/* Send flash BUf response */
	u8Buff[0] = FLASH_DATA_SIZE ; /* In every message sent to IMX, 
													(First_byte) = (Total bytes transmitted -1)*/

	u8Buff[1] = 0x31;/*	Message ID */
	u8Buff[2] = 0x11;/*	FLASH_BUF ID */
	u8Buff[3] = 0x01;/*	FLASH_BUF state ok */
	s32ErrChk = write( s32ConnFD, &u8Buff, (u8Buff[0] + 1));
	if(s32ErrChk == (u8Buff[0] + 1))
	{
		//printf("FLASH_DATA Res sent sucessfully \n");
	}
	else
	{
		perror(" FLASH_DATA write to socket failed  \n");
		exit (-1);
	}
}

