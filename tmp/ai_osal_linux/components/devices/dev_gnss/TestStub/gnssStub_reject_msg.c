/******************************************************************************
* FILE         : gnssStub_reject_.c
*             
* DESCRIPTION  : This file implements a test stub that simulates v850 for GNSS proxy.
*                In GEN3 GNSS hardware is present on V850. GNSS data will be sent from V850 to imx via INC.
*                It is the job of GNSS proxy module to collect this data, and deliver this to VD-Sensor.
*                As we dont have v850 up presently, functionality of GNSS proxy is tested 
*                using this test stub. It communicates with sensor proxy on imx via socket using TCP/IP.
*                IP Address of imx: 172.17.0.1 PORT on IMX: 0x08
*                IP addredd of ubuntu in which test stub is executed shall be : 172.17.0.6  PORT on UBUNTU: 0x08
* 
* AUTHOR(s)    : Srinivas Prakash Anvekar (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 01.JUL.2015|  Initialversion 1.0  | Srinivas Prakash Anvekar (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<limits.h>
#include<errno.h>
#include<sys/types.h>

/* inet_pton() */
#include <arpa/inet.h>
/* htons() */
#include <netinet/in.h>
#include<sys/socket.h>

#include <time.h>

/* To Comply with bosch standards */

typedef unsigned char tU8;
typedef signed char tS8;
typedef unsigned int tU32;
typedef const char * tCString;
typedef unsigned char tBool;
typedef unsigned short tU16;
typedef int tS32;
typedef void * tPVoid;
typedef void tVoid;

/* OUR error codes. */
#define OSAL_OK 0
#define OSAL_ERROR -1

/*  Data sizes af different message fields  */
#define STATUS_MSG_SIZE (3)
#define CONFIG_MSG_SIZE (5)
#define REJECT_MSG_SIZE (3)

#define MSG_SIZE_SCC_SENSOR_R_ODO_DATA                (7)
#define MSG_SIZE_SCC_SENSOR_R_GYRO_DATA               (10)
#define MSG_SIZE_SCC_SENSOR_R_ACC_DATA               (10)
#define MSG_SIZE_SCC_SENSOR_R_GYRO_TEMP               (3)
#define MSG_SIZE_SCC_SENSOR_R_ACC_TEMP                (3)

/* Buffer to send data */
#define TRANS_BUFFER_SIZE (2048)
static tU8 u8Buff[TRANS_BUFFER_SIZE];
char* sGPGGA = "$GPGGA,124521.000,5205.49183,N,00954.92152,E,2,08,1.2,0120.8,M,47.2,M,,*6F";
char* sGPRMC = "$GPRMC,124521.000,A,5205.49183,N,00954.92152,E,24.7,344.7,050913,,,D*51";
char* sGPVTG = "$GPVTG,344.7,T,,M,24.7,N,45.8,K,D*04";
char* sGPGSA = "$GPGSA,A,3,05,21,07,24,30,16,12,,,,,,2.4,1.9,1.5*38";
char* sGPGSV1 = "$GPGSV,3,1,12,02,04,037,,05,27,125,44,06,78,051,23,07,83,021,30*7C";
char* sGPGSV2 = "$GPGSV,3,2,12,10,16,067,30,12,11,119,36,16,24,301,41,21,44,175,50*73";
char* sGPGSV3 = "$GPGSV,3,3,12,23,06,326,28,24,61,118,40,30,45,122,43,31,52,253,37*7C";
char* sPSTMKFCOV = "$PSTMKFCOV,9.6,30.2,62.9,24.9,1.2,0.8,0.6,1.0*7d";


/* Handle to socket */
tS32 s32ConnFD;

/* Function descriptor for socket */
tS32 s32SocketFD = 0;

/* For delay */
struct timespec req;

/* For incrementing sensor dummy data */
static int cnt = 0;
tU32 u32NmeaPosEnd = 0;



tVoid vCommunicateToAPP();
tS32 SocSetup();
tVoid vFillGpgga(tU32 u32offset);
tVoid vFillGprmc(tU32 u32offset);
tVoid vFillGpvtg(tU32 u32offset);
tVoid vFillGpgsa(tU32 u32offset);
tVoid vFillGpgsv1(tU32 u32offset);
tVoid vFillGpgsv2(tU32 u32offset);
tVoid vFillGpgsv3(tU32 u32offset);
tVoid vFillPstmkfcov(tU32 u32offset );


tS32 SocSetup()
{
   struct sockaddr_in rSocAttr;
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32OptState = 1;

   memset(&rSocAttr,0, sizeof(rSocAttr));
   rSocAttr.sin_family      = AF_INET;
   rSocAttr.sin_port        = htons(0x06); // Todo: this has to be changed
   rSocAttr.sin_addr.s_addr = htonl(INADDR_ANY);

   /* create a socket
      SOCK_STREAM: sequenced, reliable, two-way, connection-based
      byte streams with an OOB data transmission mechanism.*/
   s32SocketFD = socket(AF_INET, SOCK_STREAM, 0);
   if(s32SocketFD == -1)
   {
      perror("socket() failed");
   }
   else
   {
      /* This option informs os to reuse the socket address even if it is busy*/
      if(setsockopt(s32SocketFD, SOL_SOCKET, SO_REUSEADDR,
                    &s32OptState, sizeof(s32OptState)) == -1)
      {
         perror( "!!!setsockopt() Failed");
      }
      /* Bind the socket created to a address */
      s32RetVal = bind(s32SocketFD, (struct sockaddr *)&rSocAttr, sizeof(rSocAttr));
      if(s32RetVal == -1)
      {
         perror("Bind Failed");
      }
      else
      {
         /*Listen system call makes the socket as a passive socket. 
                  i.e socket will be used to accept incoming connections*/
         s32RetVal = listen(s32SocketFD, 10);
         if(s32RetVal == -1)
         {
            perror("Listen Failed");
         }
         else
         {
            s32RetVal = OSAL_OK;
            s32ConnFD = accept(s32SocketFD, NULL, NULL);
            if(s32ConnFD == -1)
            {
              perror( "accept Failed !!!");
              /* This should never happen. Only reason for this is something went
                 wrong while configuring the socket*/
            }
	   else
	   {
            	printf( "Socket creation and Setup completed successfully\n");
		vCommunicateToAPP(s32ConnFD);
	   }
         }
      }
   }
   return s32RetVal;
}

/* Start of program execution  */

tVoid main(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;
   s32RetVal = SocSetup();
   if( s32RetVal != OSAL_OK )
   {
      perror(" error in socket setup \n ");
   }
   vCommunicateToAPP(s32ConnFD);
   while(1)
      sleep(5);
}

/* All communications to APP (IMX) is done from here */

tVoid vCommunicateToAPP()
{
   tS32 s32ErrChk = OSAL_ERROR;
	tU32 u32ConfigSent = 0;
	tU32 u32Offset = 0;

	/* First IMX is expected to send status. So call recv from socket */
	s32ErrChk = recv(s32ConnFD, &u8Buff, STATUS_MSG_SIZE, 0);
   
	if( s32ErrChk == STATUS_MSG_SIZE )
	{
   
		printf("Msg ID : %d  APP STATUS : %d IMX firmware version : %d\n",u8Buff[0], u8Buff[1], u8Buff[2]);
      
		/* reject message is sent from V850 to imx */
		u8Buff[0] = REJECT_MSG_SIZE; /* In every message sent to IMX, 
		                                    (First_byte) = (Total bytes transmitted -1)*/
												
		u8Buff[1] = 0x0B;/*  Message ID */
		u8Buff[2] = 0x03;/*  Reject Reason = REJ_TMP_UNAVAIL */
		u8Buff[3] = 0x20;/*  reject component status */
		s32ErrChk = write( s32ConnFD, &u8Buff, (REJECT_MSG_SIZE+1));
      
		if(s32ErrChk == (REJECT_MSG_SIZE+1))
		{
         /* to accept further data from imx a second accpet is needed */   
         s32ConnFD = accept(s32SocketFD, NULL, NULL);
         if(s32ConnFD == -1)
         {
            perror( "accept Failed !!!\n");
         }
         
			printf("reject msg sent sucessfully \n");
         
         /* receive status from imx */
         s32ErrChk = recv(s32ConnFD, &u8Buff, STATUS_MSG_SIZE, 0);
         
         if( s32ErrChk == STATUS_MSG_SIZE )
         {
            printf("Msg ID : %d  APP STATUS : %d IMX firmware version : %d\n",u8Buff[0], u8Buff[1], u8Buff[2]);
            
            /* Next its v850 turn to send inactive status to imx */
            u8Buff[0] = 3; /* In every message sent to IMX, 
                                                (First_byte) = (Total bytes transmitted -1)*/
                                          
            u8Buff[1] = 0x21;/* Message ID */
            u8Buff[2] = 0x02;/* Status as inactive */
            u8Buff[3] = 0x01;/* version info */
            s32ErrChk = write( s32ConnFD, &u8Buff, 4);
            
            
            
            if( s32ErrChk == 4 )
            {
               /* to accept further data from imx a third accept is needed */   
               s32ConnFD = accept(s32SocketFD, NULL, NULL);
               if(s32ConnFD == -1)
               {
                  perror( "accept Failed !!!");
               }
            
               printf("InActive Status msg sent successfully\n");
               
               /* receive status from imx */
               s32ErrChk = recv(s32ConnFD, &u8Buff, STATUS_MSG_SIZE, 0);
         
               if( s32ErrChk == STATUS_MSG_SIZE )
               {
                  printf("Msg ID : %d  APP STATUS : %d IMX firmware version : %d\n",u8Buff[0], u8Buff[1], u8Buff[2]);
                  
                  /* Next its v850 trun to send active status */
                  u8Buff[0] = 3; /* In every message sent to IMX, 
                                                      (First_byte) = (Total bytes transmitted -1)*/
                                                
                  u8Buff[1] = 0x21;/* Message ID */
                  u8Buff[2] = 0x01;/* Status as active */
                  u8Buff[3] = 0x01;/* version info */
                  s32ErrChk = write( s32ConnFD, &u8Buff, 4);
                  
                  if( s32ErrChk == 4 )
                  {
                     printf("Active Status msg sent successfully \n");
                     
                     
                     u8Buff[0]=CONFIG_MSG_SIZE;// (bytes_transmitted -1)
                     u8Buff[1]=0x31;// Msg ID
                     u8Buff[2]=0x00;// Service type as Config RQ
                     u8Buff[3]=0xE8;// Data intervall : 0x3E8 = 1000 in decimal
                     u8Buff[4]=0x03; 
                     u8Buff[5]=0x01; // GNSS chip type used
                     
                     /* Send config message. */
                     s32ErrChk = write(s32ConnFD, &u8Buff,(CONFIG_MSG_SIZE+1));
                     
                     if(s32ErrChk == (CONFIG_MSG_SIZE+1))
                     {
                        printf("Config sent \n");
                        u32ConfigSent = 1;
                        sleep(0);
                     }
                     else
                     {
                        perror("!!! config Failed \n");
                     }
                  }
                  else
                  {
                     perror("!!! active message send failed \n");
                  }
                     
               }
               else
               {
                  perror("!!!  recv after inactive msg send failed \n");
               }
            }
            else
            {
               perror(" inactive status msg sending failed  \n ");
            }
         }
         else
         {
            perror(" !!! receive after reject failed  \n");
         }
      }
      else
      {
         perror("!!! reject message send failed \n");
      }
	}
	else
	{
		perror("");
		printf("Un expexted status message size from IMX %d\n",s32ErrChk);
	}
   
   if(u32ConfigSent == 1)
	{
      printf(" Config Msg send was successful  \n");

      u8Buff[1] = 0x41; /* GNSS data Msg ID */
      u8Buff[2] = 11;
      u8Buff[3] = 0;
      u8Buff[4] = 0;
      u8Buff[5] = 0; // 4 bytes of time stamp;
      
      vFillGpgga(6);
      vFillGprmc(u8Buff[0]);
      vFillGpvtg(u8Buff[0]);

      u8Buff[u8Buff[0]]= 0;
      u8Buff[0]++;
       
      s32ErrChk = write(s32ConnFD, u8Buff,u8Buff[0]+1 );
      if(s32ErrChk == u8Buff[0]+1 )
      {
         printf("GPGGA5, GPRMC, GPVTG sent u8Buff[2] %d :-) \n",u8Buff[2]);
      }
      else
      {
         perror("GPGGA5, GPRMC, GPVTG sending failed");
      }

/*-------------------------------------------------------------------------------------------------------------------*/
      u8Buff[1] = 0x41; /* GNSS data Msg ID */
      u8Buff[2] =12;
      u8Buff[3] =0;
      u8Buff[4] =0;
      u8Buff[5] = 0; // 4 bytes of time stamp;
      
      vFillGpgsa(6);
      vFillGpgsv1(u8Buff[0]);

      u8Buff[u8Buff[0]]= 0;
      u8Buff[0]++;
       
      s32ErrChk = write(s32ConnFD, u8Buff,u8Buff[0]+1 );
      if(s32ErrChk == u8Buff[0]+1 )
      {
         printf("GPGSA, GPGSV1 sent u8Buff[2] %d :-) \n",u8Buff[2]);
      }
      else
      {
         perror("GPGSA, GPGSV1 sending failed");
      }

/*-------------------------------------------------------------------------------------------------------------------*/			
      u8Buff[1] = 0x41; /* GNSS data Msg ID */
      u8Buff[2] = (strlen(sGPGSV2)+2)+(strlen(sGPGSV3)+2)+1; /* Number of Bytes */
      u8Buff[3] = 0;
      u8Buff[4] =13;
      u8Buff[5] =0;
      u8Buff[6] =0;
      u8Buff[7] = 0; // 4 bytes of time stamp;
      
      vFillGpgsv2(6);
      vFillGpgsv3(u8Buff[0]);
      vFillPstmkfcov(u8Buff[0]);

      u8Buff[u8Buff[0]]= 0;
      u8Buff[0]++;
      u8Buff[u8Buff[0]]= 0x0C;
      u8Buff[0]++;
       
      s32ErrChk = write(s32ConnFD, u8Buff,u8Buff[0]+1 );
      if(s32ErrChk == u8Buff[0]+1 )
      {
         printf("GPGSV2, GPGSV3 sent u8Buff[2] %d :-) \n",u8Buff[2]);
      }
      else
      {
         perror("GPGSV2, GPGSV3 sending failed");
      }

/*-------------------------------------------------------------------------------------------------------------------*/ 	

	}
   
   while(1);
}

tVoid vFillGpgga(tU32 u32offset)
{

	tS32 s32ErrChk = OSAL_ERROR;
	memcpy(&u8Buff[u32offset], sGPGGA, strlen(sGPGGA));
	u32NmeaPosEnd = u32offset+strlen(sGPGGA);
	u8Buff[u32NmeaPosEnd]= 0x0D; // Carriage return ascii value
	u8Buff[u32NmeaPosEnd+1]= 0x0A; // Line feed ascii value
//	u8Buff[u32NmeaPosEnd+2]= 0x00; // Null terminator ascii value
	u8Buff[0]=u32NmeaPosEnd +2; // +3 is for CR, LF, and null tereminator

}

tVoid vFillGprmc(tU32 u32offset)
{

	tS32 s32ErrChk = OSAL_ERROR;

	memcpy(&u8Buff[u32offset], sGPRMC, strlen(sGPRMC));
	u32NmeaPosEnd = u32offset+strlen(sGPRMC);
	u8Buff[u32NmeaPosEnd]= 0x0D; // Carriage return ascii value
	u8Buff[u32NmeaPosEnd+1]= 0x0A; // Line feed ascii value
//	u8Buff[u32NmeaPosEnd+2]= 0x00; // Null terminator ascii value
	u8Buff[0]=u32NmeaPosEnd +2; // +3 is for CR, LF, and null tereminator

//	printf("u8Buff[0] %d", u8Buff[0]);

}

tVoid vFillGpvtg(tU32 u32offset)
{
	tS32 s32ErrChk = OSAL_ERROR;

	memcpy(&u8Buff[u32offset], sGPVTG, strlen(sGPVTG));
	u32NmeaPosEnd = u32offset+strlen(sGPVTG);
	u8Buff[u32NmeaPosEnd]= 0x0D; // Carriage return ascii value
	u8Buff[u32NmeaPosEnd+1]= 0x0A; // Line feed ascii value
	u8Buff[0]=u32NmeaPosEnd +2; // +3 is for CR, LF, and null tereminator

}

tVoid vFillGpgsa(tU32 u32offset)
{
	tS32 s32ErrChk = OSAL_ERROR;

	memcpy(&u8Buff[u32offset], sGPGSA, strlen(sGPGSA));
	u32NmeaPosEnd = u32offset+strlen(sGPGSA);
	u8Buff[u32NmeaPosEnd]= 0x0D; // Carriage return ascii value
	u8Buff[u32NmeaPosEnd+1]= 0x0A; // Line feed ascii value
	u8Buff[0]=u32NmeaPosEnd +2; // +3 is for CR, LF, and null tereminator

}

tVoid vFillGpgsv1(tU32 u32offset)
{
	tS32 s32ErrChk = OSAL_ERROR;

	memcpy(&u8Buff[u32offset], sGPGSV1, strlen(sGPGSV1));
	u32NmeaPosEnd = u32offset+strlen(sGPGSV1);
	u8Buff[u32NmeaPosEnd]= 0x0D; // Carriage return ascii value
	u8Buff[u32NmeaPosEnd+1]= 0x0A; // Line feed ascii value
	u8Buff[0]=u32NmeaPosEnd +2; // +3 is for CR, LF, and null tereminator

}

tVoid vFillGpgsv2(tU32 u32offset)
{
	memcpy(&u8Buff[u32offset], sGPGSV2, strlen(sGPGSV2));
	u32NmeaPosEnd = u32offset+strlen(sGPGSV2);
	u8Buff[u32NmeaPosEnd]= 0x0D; // Carriage return ascii value
	u8Buff[u32NmeaPosEnd+1]= 0x0A; // Line feed ascii value
	u8Buff[0]=u32NmeaPosEnd +2; // +3 is for CR, LF, and null tereminator

}

tVoid vFillGpgsv3(tU32 u32offset)
{
	memcpy(&u8Buff[u32offset], sGPGSV3, strlen(sGPGSV3));
	u32NmeaPosEnd = u32offset+strlen(sGPGSV3);
	u8Buff[u32NmeaPosEnd]= 0x0D; // Carriage return ascii value
	u8Buff[u32NmeaPosEnd+1]= 0x0A; // Line feed ascii value
	u8Buff[0]=u32NmeaPosEnd +2; // +3 is for CR, LF, and null tereminator

}

tVoid vFillPstmkfcov(tU32 u32offset)
{
        memcpy(&u8Buff[u32offset], sPSTMKFCOV, strlen(sPSTMKFCOV));
        u32NmeaPosEnd = u32offset+strlen(sPSTMKFCOV);
        u8Buff[u32NmeaPosEnd]= 0x0D; // Carriage return ascii value
        u8Buff[u32NmeaPosEnd+1]= 0x0A; // Line feed ascii value
        u8Buff[0]=u32NmeaPosEnd +2; // +3 is for CR, LF, and null tereminator

}