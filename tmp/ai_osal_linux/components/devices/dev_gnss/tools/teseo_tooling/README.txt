How to use the tool ?

1. If the binary - teseo_tool_out.out and flasher - flasher.bin are not present in the target at - /opt/bosch/base/bin/, install the ipk - gnss_teseo_tool_verXX.ipk
2. Navigate to the location of binary - cd /opt/bosch/base/bin/
3. Execute the binary with corresponding options -

	a. Flash dump -  (*)
	  ./teseo_tool_out --dump flash  --flasher flasher.bin  --version TeseoVersion  [--out OutputFilename]  [--size SizeInWords]

	b. Status Register dump - 
      ./teseo_tool_out --dump register  --flasher flasher.bin --version TeseoVersion

	c. Mode change - 
	  ./teseo_tool_out --mode <nmea/xloader> --version TeseoVersion

	d. Release -
	  ./teseo_tool_out --release --version TeseoVersion
   
   e. Register write - 
      ./teseo_tool_out --write register NumOfRegs R1 R2 R3  --flasher flasher.bin  --version TeseoVersion

	  

Note: 

TeseoVersion - teseo2 - in case of Teseo2 GNSS chip
           and teseo3 - in case of Teseo3 GNSS chip 

flasher.bin - flasher_teseo2.bin - in case of Teseo2 GNSS chip
          and flasher_teseo3.bin - in case of Teseo3 GNSS chip 
	  
(*) If OutputFilename is not specified then, default output file - /var/tmp/TeseoDumpOutput
	If SizeInWords is not specified then, default output file size - 2MB
   
   
   
Don'ts

1. Do not touch the display or press any button on target while taking flash dump as it may cause the flashdump operation to fail.
