/*************************************************************************
 * FILE        : teseo_tool_types.h
 *
 * DESCRIPTION : header file for teseo tooling application
 *
 *---------------------------------------------------------------------------
 * AUTHOR(s)   : Kulkarni Ramchandra (CM-AI/PJ-CF35)
 *
 * HISTORY     :
 *---------------------------------------------------------------------------
 * Date        |       Version          | Author & comments
 *-------------|------------------------|------------------------------------
 * 19.July.2016 | Initial version: 0.1   | Kulkarni Ramchandra (ECF12)
 * --------------------------------------------------------------------------

 **************************************************************************/


#ifndef TESEO_TOOL_HEADER
#define TESEO_TOOL_HEADER

//all errors should be non zero

#define  E_FILEOPEN            2
#define  E_CREATESOCKET        4
#define  E_GETHOSTBYNAME       5
#define  E_CONNECT             6
#define  E_BIND                7
#define  E_DGRAMINIT          10
#define  E_MISSINGARGS        19
#define  E_STATUSEXCHANGE     22
#define  E_INITCOMMSCC        23
#define  E_FILEWRITE          24
#define  E_TESEOACKSEND       25
#define  E_FLASHER             26
#define  E_HOSTREADY           27
#define  E_BINARYOPTIONS       28
#define  E_FLASHERREADY        29
#define  E_TESEODATA           30
#define  E_SENDMSGTOSCC        32
#define  E_WAITFORSCCRESPONSE  33
#define  E_WRONGRESPONSE       34
#define  E_UNEXPECTEDDATARECV  35
#define  E_INVALIDARGS         36
#define  E_NORESPONSEFROMSCC   37
#define  E_FILEREAD            38
#define  E_FILESEEK            39
#define  E_UARTSYNC            40
#define  E_FLASHERCRCERROR     41
#define  E_TESEOACKRECV        42

#define  E_UNKNOWN            0xff


//Macros
#define  FILLER_INIT                                    0
#define  TESEO2_CHUNK_16KB                             (16*1024)
#define  TESEO3_CHUNK_5KB                              (5*1024)
#define  FLASH_DUMP_WORD_2MB                            524288


#define  GNSS_MAX_HANDLED_PACKET_SIZE                   (1024)  //4KB    --one page
#define  GNSS_APP_PROXY_DRIVER_VERSION                  (0x02) 

#define  GNSS_FW_UPDATE_COMPONENT_VERSION_ONE           (0x01)
#define  GNSS_FW_UPDATE_COMPONENT_VERSION_TWO           (0x02)
#define  GNSS_FW_UPDATE_COMPONENT_VERSION_THREE         (0x03)


//Macros for forming messages

#define  GNSS_TESEO_HOST_READY                           0x4A
#define  GNSS_TESEO_ACK                                  0xCC
#define  GNSS_TESEO_NACK                                 0xE6


#define  GNSS_MSG_CMD_CONTROL_FLASH_BEGIN                0x20
#define  GNSS_MSG_CMD_CONTROL_FLASH_DATA                 0x21
#define  GNSS_MSG_CMD_CONTROL_FLASH_END                  0x2F


#define  GNSS_INC_TP_MARKER_HEADER                       0x00
#define  GNSS_MSG_ID_CMD_CONTROL                         0x30
#define  GNSS_MSG_ID_RES_CONTROL                         0x31
#define  GNSS_MSG_CONTROL_FLASH_BUF_SIZE                 0x23
#define  GNSS_MSG_CONTROL_COMP_STATUS_MSGID              0x20
#define  GNSS_MSG_RESPONSE_COMP_STATUS_MSGID             0x21
#define  GNSS_APP_COMPONENT_STATUS_ACTIVE                0x01
#define  GNSS_SCC_COMPONENT_STATUS_ACTIVE                0x01

#define  GNSS_MSG_RESPONSE_SCC_OK                        0x00
#define  GNSS_MSG_RESPONSE_FLASH_DATA                    0x22
#define  GNSS_TOOL_MIN_ARGS                              2
#define  GNSS_TOOL_MAX_ARGS                              12

#define  CHIP_UNKNOWN                                    0x00
#define  CHIP_TESEO2                                     0x02
#define  CHIP_TESEO3                                     0x03

//Register write
#define  MAX_NUM_OF_STATUS_REGISTERS                     3
#define  FIRMWARE_UPDATE_BUFF_SIZE                       (16*1024)

#define  TESEO3_SYNC_RETRIES                             1000

#define  GNSS_TESEO2_HOST_READY                          0x4A
#define  GNSS_TESEO2_FLASHER_READY                       0x4A
#define  GNSS_TESEO3_HOST_READY                          0x5A
#define  GNSS_TESEO3_FLASHER_READY                       0x4A


//different delays
#define  WAIT_20SEC       20000
#define  WAIT_5SEC        5000
#define  WAIT_10SEC       10000


typedef struct
{
   signed    int    code_size;
   unsigned  int    target_device;
   unsigned  int    crc32;
   signed    int    dest_addr;
   signed    int    entry_offset;
   signed    int    baud_rate;
   unsigned  char   nvm_erase;
   unsigned  char   erase_only;
   unsigned  char   program_only;
   unsigned  char   debug_enable;
   signed    int    nvm_offset;
   signed    int    nvm_erase_size;
   unsigned  int    debug_mode;
   unsigned  int    debug_addr;
   signed    int    debug_size;
   unsigned  int    debug_data;
}Teseo2_BinaryImageOptions;


typedef enum 
{
   FLASHDUMP = 0,
   REGREAD = 1,
   REGWRITE = 2
}BinOpts_Mode;


typedef enum
{
   MODE_NMEA,
   MODE_XLOADER
}Teseo_Mode;


typedef struct 
{
   char dump;
   char dump_flash;
   char dump_register;

   char write;
   char write_register;
   char reg_values[3];
   int num_of_regs;

   char name_flasher[50];
   char name_output[30];

   char change_mode;
   Teseo_Mode set_mode;
   char release;

   char send_cmd;
   int timeout_cmd;
   char nmea_cmd[80];


   int fd_flasher;
   int fd_output;

   int chip_teseo;

}cmd_line_options;


typedef struct
{
   unsigned int XloaderIdentifierMsp;
   unsigned int XloaderIdentifierLsp;
   unsigned int XloaderOptions;
   unsigned int XloaderDefDestAddr;
   unsigned int XloaderCrc32;
   unsigned int XloaderSize;
   unsigned int XloaderDefEntryPoint;
}Teseo3XloaderPreamble;


typedef struct
{
  int code_size;
  unsigned int target_device;
  unsigned int crc32;
  unsigned int dest_addr;
  unsigned int entry_offset;
  unsigned char nvm_erase;
  unsigned char erase_only;
  unsigned char program_only;
  unsigned char sub_sector;
  unsigned char sta8090f;
  unsigned char res1;
  unsigned char res2;
  unsigned char res3;
  int nvm_offset;
  int nvm_erase_size;
  unsigned int debug_enable;
  unsigned int debug_mode;
  unsigned int debug_address;
  int debug_size;
  unsigned int debug_data;
}Teseo3_BinaryImageOptions;


#endif

/*End of file*/