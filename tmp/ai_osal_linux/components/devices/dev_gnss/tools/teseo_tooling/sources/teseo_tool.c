/*************************************************************************
 * FILE        : teseo_tool.c
 *
 * DESCRIPTION : Application for teseo tooling
 *
 *---------------------------------------------------------------------------
 * AUTHOR(s)   : Kulkarni Ramchandra (CM-AI/PJ-CF35)
 *
 * HISTORY     :
 *---------------------------------------------------------------------------
 * Date        |       Version          | Author & comments
 *-------------|------------------------|------------------------------------
 * 19.July.2016 | Initial version: 0.1   | Kulkarni Ramchandra (ECF12)
 * --------------------------------------------------------------------------

 **************************************************************************/


// ---------------- Libraries ------------------------------
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <poll.h>
#include <unistd.h>
#include <time.h>
#include <getopt.h>

#include "inc.h"
#include "inc_ports.h"
#include "dgram_service.c"
#include "teseo_tool_types.h"


//For Debugging
int debug_print_enable = 0;


//---------------- End of Libraries--------------------------------

// Global variables and initialization

//Command line options
cmd_line_options CmdLineOptions = 
{
   .dump = 0,
   .dump_flash = 0,
   .dump_register = 0,

   .set_mode = MODE_NMEA,
   .change_mode = 0,
   .release = 0,

   .name_flasher[0] = '\0',
   .name_output[0] = '\0',

   .send_cmd = 0,
   .nmea_cmd[0] = '\0',
   .timeout_cmd = 5,

   .fd_flasher = 0,
   .fd_output = 0,

   .chip_teseo = CHIP_UNKNOWN,

   .write = 0,
   .write_register = 0,
   .num_of_regs = 0,

   .reg_values[0] = 0x00, 0x00, 0x00
};


//Binary options for Teseo2
Teseo2_BinaryImageOptions image_options_teseo2[] = 
{
//bin options for flashdump
   {
      .code_size        = 0,
      .target_device    = 1,
      .crc32            = 0,
      .dest_addr        = 0x30000000,
      .entry_offset     = 0x00000000,
      .baud_rate        = 0,
      .nvm_erase        = 0,
      .erase_only       = 0,
      .program_only     = 0,
      .debug_enable     = 1,
      .nvm_offset       = 0x00100000,
      .debug_mode       = 0,
      .nvm_erase_size   = 0,
      .debug_addr       = 0x30000000,
      .debug_size       = 524288,
      .debug_data       = 0
   },
//bin options for regread
   {
      .code_size        = 0,
      .target_device    = 1,
      .crc32            = 0,
      .dest_addr        = 0x30000000,
      .entry_offset     = 0x00000000,
      .baud_rate        = 0,
      .nvm_erase        = 0,
      .erase_only       = 0,
      .program_only     = 0,
      .debug_enable     = 1,
      .nvm_offset       = 0x00100000,
      .nvm_erase_size   = 0,
      .debug_mode       = 2,
      .debug_addr       = 0,
      .debug_size       = 0,
      .debug_data       = 0
   },
      //bin options for write_register
   {
      .code_size        = 0,
      .target_device    = 1,
      .crc32            = 0,
      .dest_addr        = 0x30000000,
      .entry_offset     = 0x00000000,
      .baud_rate        = 0,
      .nvm_erase        = 0,
      .erase_only       = 0,
      .program_only     = 0,
      .debug_enable     = 1,
      .nvm_offset       = 0x00100000,
      .nvm_erase_size   = 0,//0x00100000,
      .debug_mode       = 3,
      .debug_addr       = 0,
      .debug_size       = 0,
      .debug_data       = 0
   }
};


// Binary options for Teseo3
Teseo3_BinaryImageOptions image_options_teseo3[] = 
{
   //bin options for flashdump
   {
      .code_size      = 0,
      .target_device  = 1,
      .crc32          = 0,
      .dest_addr      = 0x10000000, // SQI flash memory base address
      .entry_offset   = 0x00000000,
      .nvm_erase      = 0,
      .erase_only     = 0,
      .program_only   = 0,
      .sub_sector     = 0,
      .sta8090f       = 0,
      .res1           = 0,
      .res2           = 0,
      .res3           = 0,
      .nvm_offset     = 0x00100000,
      .nvm_erase_size = 0,
      .debug_enable   = 1,
      .debug_mode     = 0,
      .debug_address  = 0x10000000,
      .debug_size     = 524288,
      .debug_data     = 0
   },
   //bin options for regread
   {
      .code_size      = 0,
      .target_device  = 1,
      .crc32          = 0,
      .dest_addr      = 0x10000000, // SQI flash memory base address
      .entry_offset   = 0x00000000,
      .nvm_erase      = 0,
      .erase_only     = 0,
      .program_only   = 0,
      .sub_sector     = 0,
      .sta8090f       = 0,
      .res1           = 0,
      .res2           = 0,
      .res3           = 0,
      .nvm_offset     = 0x00100000,
      .nvm_erase_size = 0,
      .debug_enable   = 1,
      .debug_mode     = 2,
      .debug_address  = 0,
      .debug_size     = 0,
      .debug_data     = 0
   },
   //bin options for write_register
   {
      .code_size      = 0,
      .target_device  = 1,
      .crc32          = 0,
      .dest_addr      = 0x10000000, // SQI flash memory base address
      .entry_offset   = 0x00000000,
      .nvm_erase      = 0,
      .erase_only     = 0,
      .program_only   = 0,
      .sub_sector     = 0,
      .sta8090f       = 0,
      .res1           = 0,
      .res2           = 0,
      .res3           = 0,
      .nvm_offset     = 0x00100000,
      .nvm_erase_size = 0,//0x00100000,
      .debug_enable   = 1,
      .debug_mode     = 3,
      .debug_address  = 0,
      .debug_size     = 0,
      .debug_data     = 0
   }
};

//Flasher Sync for Teseo3
Teseo3XloaderPreamble preamble =
{
   .XloaderIdentifierMsp = 0xBCD501F4,
   .XloaderIdentifierLsp = 0x83984073,
   .XloaderOptions =       0x00FF0104,
   .XloaderDefDestAddr =   0x00000000,
   .XloaderCrc32 =         0,
   .XloaderSize =          0,
   .XloaderDefEntryPoint = 0x00000000
};

//CRC table for Teseo3
static unsigned int crc32_tab[] =
{
   0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
   0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
   0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
   0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
   0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
   0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
   0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c,
   0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
   0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
   0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
   0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190, 0x01db7106,
   0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
   0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
   0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
   0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
   0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
   0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
   0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
   0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
   0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
   0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
   0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
   0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
   0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
   0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
   0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
   0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
   0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
   0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
   0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
   0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
   0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
   0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
   0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
   0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
   0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
   0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
   0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
   0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
   0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
   0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
   0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
   0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};




static int fd_sock;
static sk_dgram* sock_dgram_handle = 0;

static char msg_seq_counter = 0;
static char data_recv_buf[1024];
static char data_send_buf[1024];
static int gnss_buf_size_on_scc;
static int gnss_version_on_scc;
static char teseo_data_holdbuffer[TESEO2_CHUNK_16KB] = {0};

static char *name_defaultOutput = "/var/tmp/TeseoDumpOutput";
int size_flashdump = FLASH_DUMP_WORD_2MB;


// End of Global variables and initialization



// Local function declarations
static void gnss_tool_deinit_resources( void );
static int gnss_tool_setup_comm_with_scc( void );
static int gnss_tool_store_buffer_size_on_scc( void );
static int gnss_tool_send_flasher_to_teseo( void );
static int gnss_tool_exch_status_with_scc( void );
static int gnss_tool_recv_fdump_data_from_teseo ( void );
static int gnss_tool_send_msg_to_scc( char*, int      );
static int gnss_tool_wait_for_msg_from_scc( char*, int, int );
static int gnss_tool_parse_cmdline_args( int  , char * const *  );
static int gnss_tool_recv_regs_from_teseo( void );
static int gnss_tool_send_ack_to_teseo( void );
//static void  gnss_tool_function_name_print( const char * name);
static int gnss_tool_get_status_regs( void );
static int gnss_tool_change_teseo_mode( Teseo_Mode mode);
static void gnss_tool_print_debug_options( const char *name_binary);
static int gnss_tool_init_scc_comm( void );
static int gnss_tool_init_files( void );
static int gnss_tool_validate_msg( const char *ExpectedResponse, const char *rxbuf, int size);
static int gnss_tool_recv_msg( char *data_buf, char *wait_msg, int msg_size, int timeout);
static int gnss_tool_get_flash_dump( void );
static int gnss_tool_send_cmd_to_teseo( void );
static int gnss_tool_scan_set_cmdline_options( void );
static int gnss_tool_reinit_resources( void );
static int gnss_tool_get_reg_write_subopts(int argc, char * const *argv);
static int gnss_tool_teseo_specific_options(void);
static int gnss_tool_write_teseo_status_regs(void);
static int gnss_tool_send_reg_vals_to_teseo(void);
static int gnss_tool_send_byte_to_teseo_without_ACK(char data);
static int gnss_tool_wait_for_teseo_ACK(void);
static int gnss_tool_wait_for_SCC_OK(void);


//Teseo2 specific functions
static int gnss_tool_teseo2_send_host_ready(void);
static int gnss_tool_teseo2_send_flasher_ready(void);
static int gnss_tool_teseo2_get_status_regs(void);
static int gnss_tool_teseo2_write_teseo_regs(void);
static int gnss_tool_teseo2_get_flash_dump(void);
static int gnss_tool_teseo2_send_binary_image_options( BinOpts_Mode );


//Teseo3 specific functions
static int gnss_tool_teseo3_send_uart_sync(void);
static int gnss_tool_teseo3_send_host_ready(void);
static int gnss_tool_teseo3_send_flasher_ready_without_ACK(void);
static int gnss_tool_teseo3_send_flasher_ready(void);
static int gnss_tool_teseo3_send_flasher_sync(void);
static int gnss_tool_teseo3_send_binary_image_options(BinOpts_Mode mode);
static int gnss_tool_teseo3_get_flash_dump(void);
static int gnss_tool_teseo3_get_status_regs(void);
static int gnss_tool_teseo3_get_flasher_CRC32( int fp, unsigned int *pu32FinalCrc, unsigned int u32ImageLength );
static unsigned int  gnss_tool_teseo3_u32_crc32(unsigned int crc32val, const char *buf, int len);
static int gnss_tool_teseo3_write_teseo_regs(void);


// End of Local function declarations


/*********************************************************************************************************************
* FUNCTION     : gnss_tool_deinit_resources(void)
* PARAMETER    : None
* RETURNVALUE  :  None
* DESCRIPTION  : De initializes allocated resources
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static void gnss_tool_deinit_resources( void )
{
   //close open file descriptors
   if( CmdLineOptions.fd_flasher > 0 )
   {
      close(CmdLineOptions.fd_flasher);
   }
   if( CmdLineOptions.fd_output > 0 )
   {
     close(CmdLineOptions.fd_output);
   }
   //close socket
   if( fd_sock > 0 )
   {
      close(fd_sock);
   }
}

// function calls tracking  ---- for debugging

/*static void gnss_tool_function_name_print( const char *name )
{
   printf("\n\n..........Function - %s..........\n\n", name);
}*/

/*********************************************************************************************************************
* FUNCTION     : gnss_tool_teseo2_get_flash_dump(void)
* PARAMETER    : None
* RETURNVALUE  :  0 on success. Non-zero error value on failure
* DESCRIPTION  : receives data from teseo2 and writes it into a file
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo2_get_flash_dump( void )
{
   int recv_flash_size = 0, total_recv_size = 0;
   int total_debug_size = image_options_teseo2[FLASHDUMP].debug_size * 4;
   char *pholdcurr = teseo_data_holdbuffer;

   //transfer flasher.bin
   if( 0 !=  gnss_tool_send_flasher_to_teseo() )
   {
      printf("\nFailed to transfer flasher.bin to SCC");
      return -E_FLASHER;
   }
   //send host_ready to scc
   if( 0 != gnss_tool_teseo2_send_host_ready() )
   {
      printf("\nFailed to send HOST_READY");
      return -E_HOSTREADY;
   }
   //send binary image options
   if( 0 != gnss_tool_teseo2_send_binary_image_options(FLASHDUMP) )
   {
      printf("\nFailed to send Binary Image Options");
      return -E_BINARYOPTIONS;
   }
   if( 0 != gnss_tool_teseo2_send_flasher_ready() )
   {
      printf("\nFailed to send FLASHER_READY");
      return -E_FLASHERREADY;
   }

   while( total_recv_size < total_debug_size )
   {
      //wait for dump data.
      if ( 0 >= (recv_flash_size = gnss_tool_recv_fdump_data_from_teseo()) )
      {
         printf("\nError receiving dump data from Teseo");
         return -E_TESEODATA;
      }
      total_recv_size += recv_flash_size;
      if ( (unsigned int)(pholdcurr - teseo_data_holdbuffer) < TESEO2_CHUNK_16KB )
      {
         memcpy(pholdcurr, &data_recv_buf[2], (unsigned int)recv_flash_size);
         pholdcurr += recv_flash_size;
      }

      //For every 16KB of data received, write the data to a file and send ACK to Teseo
      if (0 == (total_recv_size % TESEO2_CHUNK_16KB))
      {
         fflush(stdout);
         printf("\rReceived Flashdump size:   %d / %d...................%d%%", total_recv_size, total_debug_size,
                                                                            (total_recv_size*100/total_debug_size));
         fflush(stdout);
         //Write to file only when 16KB chunk of data is received from Teseo
         if( TESEO2_CHUNK_16KB != write(CmdLineOptions.fd_output, teseo_data_holdbuffer, TESEO2_CHUNK_16KB) )
         {
            printf("\nFile Write Error. Errno: %d", errno);
            return -E_FILEWRITE;
         }
         else
         {
            pholdcurr = teseo_data_holdbuffer;
            fsync(CmdLineOptions.fd_output);
         }
         if( 0 != gnss_tool_send_ack_to_teseo() )
         {
            printf("\nACK not sent to Teseo");
            return -E_TESEOACKSEND;
         }
      }
   }
   // if size of flashdump requested is not a multiple of Teseo2 chunk size
   if (0 != (total_recv_size % TESEO2_CHUNK_16KB))
   {
      printf("\nReceived Flashdump size:   %d / %d...................%d%%", total_recv_size, total_debug_size,
                                                      (total_recv_size*100/total_debug_size));
      if( (pholdcurr - teseo_data_holdbuffer) != write( CmdLineOptions.fd_output, teseo_data_holdbuffer, (unsigned int)(pholdcurr - teseo_data_holdbuffer)) )
      {
         printf("\nFile Write Error. Errno: %d", errno);
         return -E_FILEWRITE;
      }
      else
      {
         fsync(CmdLineOptions.fd_output);
      }
   }
   return 0;
}

/*********************************************************************************************************************
* FUNCTION     : static int put_teseo_in_boot_mode(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : Puts teseo in boot mode
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int put_teseo_in_boot_mode(void)
{
   return (gnss_tool_change_teseo_mode(MODE_XLOADER));
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_get_flash_dump( void )
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : Interface to get flashdump from teseo2
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_get_flash_dump( void )
{
   int retval;
   //set teseo in boot mode
   if( 0 != (retval = put_teseo_in_boot_mode()) )
   {
      printf("\nError setting Teseo in Boot mode");
   }
   //get gnss data buffer size on scc.
   else if( 0 != (retval = gnss_tool_store_buffer_size_on_scc()) )
   {
      printf("\nError getting Buffer size from SCC");
   }
   else
   {
      if(CHIP_TESEO2 == CmdLineOptions.chip_teseo)
      {
         if( 0 != (retval = gnss_tool_teseo2_get_flash_dump()) )
         {
            printf("\nUnable to get Teseo2 Flash dump");
         }
      } 
      else if(CHIP_TESEO3 == CmdLineOptions.chip_teseo)
      {
         if( 0 != (retval = gnss_tool_teseo3_get_flash_dump()) )
         {
            printf("\nUnable to get Teseo3 Flash dump");
         }
      }  
      else
      {
         printf("\nInvalid Chip type");
         retval = -E_INVALIDARGS;
      }
   }
   // Return teseo to normal operation - pos mode
   if( 0 != gnss_tool_change_teseo_mode(MODE_NMEA))
   {
      printf("\nUnable to change Teseo mode");
   }
   return retval;
}

/*********************************************************************************************************************
* FUNCTION     : static  int   gnss_tool_recv_regs_from_teseo ( void )
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : Reads the status register values and displays them on the terminal
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static  int   gnss_tool_recv_regs_from_teseo ( void )
{
   int ret_val;
   char ExpectedResponse[2] = { GNSS_MSG_ID_RES_CONTROL, 
                      GNSS_MSG_RESPONSE_FLASH_DATA };

   if( 0 > ( ret_val = gnss_tool_recv_msg( data_recv_buf, "Register read", sizeof(data_recv_buf), WAIT_20SEC) ))
   {
       printf("\nFailed to get Registry read OK response from SCC");
       return -E_WAITFORSCCRESPONSE;
   }
   //check received data size.
   if( 5 != ret_val )
   {
      printf("\nMsg size received is incorrect");
      return -E_WRONGRESPONSE;
   }
   if( 0 != gnss_tool_validate_msg(ExpectedResponse, data_recv_buf, sizeof(ExpectedResponse)))
   {
      printf("\nWrong Response received");
      return -E_WRONGRESPONSE;
   }
   // Print out all reg values
   printf ("\n--------------------------------------------------------------------");
   printf("\n\nTeseo Status Registers: \nR1:0x%x \nR2:0x%x \nR3:0x%x",(unsigned int)data_recv_buf[2],
            (unsigned int)data_recv_buf[3], (unsigned int)data_recv_buf[4] );
   printf ("\n\n--------------------------------------------------------------------");

   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_validate_msg(char *ExpectedResponse, char *rxbuf, int size)
* PARAMETER    : char *, char *, int 
* RETURNVALUE  : 0  on successful validation
*                         Non-zero Error value on failure
* DESCRIPTION  : Validates the expected message against received message.
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_validate_msg(const char *ExpectedResponse, const char *rxbuf, int size)
{
   int i;
   for(i=0; i<size; i++)
   {
      if( ExpectedResponse[i] != rxbuf[i])
      {
         printf("\nExpected[%d] = 0x%x. Received[%d] = 0x%x", i, ExpectedResponse[i], i, rxbuf[i]);
         return -E_UNEXPECTEDDATARECV;
      }
   
}
   return 0;

}

/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_change_teseo_mode(Teseo_Mode mode)
* PARAMETER    : Teseo_Mode
* RETURNVALUE  : 0  on success
*                         Non-zero Error value on failure
* DESCRIPTION  : Changes teseo2 operating mode based on the parameter sent to it
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_change_teseo_mode(Teseo_Mode mode)
{
   int retval = 0;
   char buf[4] = { GNSS_INC_TP_MARKER_HEADER, 
                   FILLER_INIT, 
                   GNSS_MSG_ID_CMD_CONTROL, 
                   FILLER_INIT };

   char ExpectedResponse[3] = { GNSS_MSG_ID_RES_CONTROL, 
                      FILLER_INIT, 
                      GNSS_MSG_RESPONSE_SCC_OK };

   buf[1] = msg_seq_counter++;
   buf[3] = (char)((mode == MODE_NMEA) ? GNSS_MSG_CMD_CONTROL_FLASH_END: GNSS_MSG_CMD_CONTROL_FLASH_BEGIN);
   ExpectedResponse[1] = buf[3];

   if( 4 != gnss_tool_send_msg_to_scc(buf, 4) )
   {
      printf("\nFailed to communicate with SCC");
      retval = -E_SENDMSGTOSCC;
   }
   //wait for response.
   else if( 3 != gnss_tool_recv_msg(data_recv_buf, "Teseo mode change OK", sizeof(data_recv_buf), WAIT_20SEC) )
   {
      printf("\nFailed to recv Mode change OK");
      retval = -E_WAITFORSCCRESPONSE;
   }
   //validate the received response
   else if( 0 != gnss_tool_validate_msg(ExpectedResponse, data_recv_buf, sizeof(ExpectedResponse)) )
   {
      printf("\nWrong response received");
      retval = -E_WRONGRESPONSE;
   }
   else
   {
      printf("\nTeseo mode changed successfully");
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : gnss_tool_get_status_regs(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Non-zero Error value on failure
* DESCRIPTION  : Provides the interface to read status register values
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_get_status_regs(void)
{
     int retval;
   //set teseo in boot mode
   if( 0 != (retval = put_teseo_in_boot_mode()) )
   {
      printf("\nError setting Teseo in Boot mode");
   }
   //get gnss data buffer size on scc.
   else if( 0 != (retval = gnss_tool_store_buffer_size_on_scc()) )
   {
      printf("\nError getting Buffer size from SCC");
   }
   else
   {
      if(CHIP_TESEO2 == CmdLineOptions.chip_teseo)
      {
         if( 0 != (retval = gnss_tool_teseo2_get_status_regs()) )
         {
            printf("\nUnable to get Teseo2 status registers");
         }
      } 
      else if(CHIP_TESEO3 == CmdLineOptions.chip_teseo)
      {
         if( 0 != (retval = gnss_tool_teseo3_get_status_regs()) )
         {
            printf("\nUnable to get Teseo3 status registers");
         }
      }  
      else
      {
         printf("\nInvalid Chip type");
         retval = -E_INVALIDARGS;
      }
   }
   // Return teseo to normal operation - pos mode
   if( 0 != gnss_tool_change_teseo_mode(MODE_NMEA))
   {
      printf("\nUnable to change Teseo mode");
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_init_scc_comm(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Non-zero Error value on failure
* DESCRIPTION  : Init function. Sets up communication with SCC and exchanges Status with it.
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_init_scc_comm(void)
{
   //socket setup with scc
   int retval;
   if( 0 != (retval = gnss_tool_setup_comm_with_scc()) )
   {
      printf("\nError setting up Comm with SCC");
   }
   //exchange status with SCC
   else if(0 != (retval = gnss_tool_exch_status_with_scc()) )
   {
      printf("\nError exchanging Status with SCC");
   }
   else
   {
      // init successful
   }
   return retval;
}

/*********************************************************************************************************************
* FUNCTION     : gnss_tool_init_scc_comm(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Non-zero Error value on failure
* DESCRIPTION  : File Init function.Opens all the files required for operation.
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_init_files(void)
{
   if((1 == CmdLineOptions.dump) || (1 == CmdLineOptions.write))
   {
      //open bootloader file.
      if( 0 > (CmdLineOptions.fd_flasher = open( CmdLineOptions.name_flasher, O_RDONLY)))
      {
         printf("\nUnable to open flasher.bin. Error: %s", strerror(errno));
         return -E_FILEOPEN;
      }
      // if the option selected is flashdump, store flashdump into an output file
      if( 1 == CmdLineOptions.dump_flash )
      {
         //Use default filename when not specified
         if( '\0' == CmdLineOptions.name_output[0] )
         {
            strncpy(CmdLineOptions.name_output, name_defaultOutput, strlen(name_defaultOutput)+1);
         }
         if( 0 > (CmdLineOptions.fd_output = open(CmdLineOptions.name_output, O_WRONLY|O_TRUNC|O_CREAT)) )
         {
            printf("\nUnable to open output file: %s. Error: %s", CmdLineOptions.name_output, strerror(errno));
            return -E_FILEOPEN;
         }
      }
   }
   else
   {
      CmdLineOptions.dump_flash = 0;
      CmdLineOptions.dump_register = 0;
      CmdLineOptions.write_register = 0;
   }
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : main()
* PARAMETER    : Command line parameters can be sent based on different options
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : Main function
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
int main(int argc, char * const argv[] )
{
   int retval;
   time_t time_init, time_final;
   unsigned int min, sec;

   printf("\n");
   time_init = time(NULL);
   //verify args
   if( 0 != (retval = gnss_tool_parse_cmdline_args( argc, (char * const*)argv )) )
   {
      printf("\nInvalid arguments");
      gnss_tool_print_debug_options(argv[0]);
   }
   else if ( 0 != (retval = gnss_tool_init_scc_comm()) )
   {
      printf("\nInit comm with SCC failed");
   }
   else if ( 0 != (retval = gnss_tool_init_files()) )
   {
      printf("\nFile init failed");
   }
   else if( 0 != (retval = gnss_tool_teseo_specific_options()) )
   {
      printf("\nSetting Teseo options failed");
   }
   else if( 0 != (retval = gnss_tool_scan_set_cmdline_options()) )
   {
      printf("\nError during operation");
   }
   else
   {
      //operations completed successfully
   }
   time_final = time(NULL);
   min = (unsigned int)(time_final - time_init)/60;
   sec = (unsigned int)(time_final - time_init)%60;
   printf("\nThe operation completed in %umin %usec", min, sec);
   gnss_tool_deinit_resources();
   printf("\n\n");
   return retval;
}

/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo_specific_options(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Sets the parameters specific for GNSS chip
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 29.SEPT.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo_specific_options(void)
{
   unsigned int crcval = 0;
   struct stat st;
   unsigned int fsize;

   switch(CmdLineOptions.chip_teseo)
   {
      case CHIP_TESEO2:
      {
         if(1 == CmdLineOptions.dump)
         {
            image_options_teseo2[FLASHDUMP].debug_size = size_flashdump;
         }
      }
      break;

      case CHIP_TESEO3:
      {
         if(1 == CmdLineOptions.dump)
         {
            image_options_teseo3[FLASHDUMP].debug_size = size_flashdump;
            if( 0 != stat(CmdLineOptions.name_flasher, &st))
            {
               printf("\nFlasher file stat could not be computed");
               return -E_FLASHER;
            }
            fsize = (unsigned int)st.st_size;
            if( 0 != gnss_tool_teseo3_get_flasher_CRC32(CmdLineOptions.fd_flasher, &crcval, fsize))
            {
               return -E_FLASHERCRCERROR;
            }
            preamble.XloaderSize = fsize;
            preamble.XloaderCrc32 = crcval;
         }
         else if(1 == CmdLineOptions.write)
         {
            if( 0 != stat(CmdLineOptions.name_flasher, &st))
            {
               printf("\nFlasher file stat could not be computed");
               return -E_FLASHER;
            }
            fsize = (unsigned int)st.st_size;
            if( 0 != gnss_tool_teseo3_get_flasher_CRC32(CmdLineOptions.fd_flasher, &crcval, fsize))
            {
               return -E_FLASHERCRCERROR;
            }
            preamble.XloaderSize = fsize;
            preamble.XloaderCrc32 = crcval;
         }
         else
         {
            //Do nothing for other options
         }
      }
      break;

      default:
      {
         //Do nothing. Chip type may not be specified for options such as --cmd
      }
      break;
   }
   return 0;
}

/*********************************************************************************************************************
* FUNCTION     : gnss_tool_scan_set_cmdline_options(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : Scans CmdLineOptions and performs tasks such as flashdump, registry read accordingly
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_scan_set_cmdline_options(void)
{
   int retval = 0;

   if( 1 == CmdLineOptions.dump )
   {
      if( 1 == CmdLineOptions.dump_flash )
      {
        printf("\n----------Getting flash dump----------\n");
        if(0 != (retval = gnss_tool_get_flash_dump()))
        {
           printf("\nError during Flashdump\n");
        }
        else
        {
           printf("\nTeseo flash dump successful");
        }

      }
      else if( 1 == CmdLineOptions.dump_register )
      {
        printf("\n----------Reading Status Registers----------\n");
        if(0 != (retval = gnss_tool_get_status_regs()))
        {
           printf("\nError during Register read\n");
        }
        else
        {
           printf("\nRegister read successful");
        }

      }
      else
      {
         printf("\nInvalid Dump options");
         return -E_INVALIDARGS;
      }
   }
   else if( 1 == CmdLineOptions.write )
   {
      if( 1 == CmdLineOptions.write_register )
      {
         printf("\n----------Writing Status Registers----------\n");
         if(0 != (retval = gnss_tool_write_teseo_status_regs()))
         {
            printf("\nError during Teseo write registers\n");
         }
         else
         {
            printf("\nRegister write successful");
         }
      }
      else
      {
         printf("\nInvalid Write options");
         return -E_INVALIDARGS;
      }
   }
   else if ( 1 == CmdLineOptions.change_mode )
   {
      printf("\n----------Changing Mode----------\n");
      if( CmdLineOptions.set_mode == MODE_XLOADER )
      {
         if(0 != (retval = gnss_tool_change_teseo_mode(MODE_XLOADER)))
         {
            printf("\nError during Teseo Mode change\n");
         }
         else
         {
            printf("\nMode set: xloader\n");
         }
      }
      else
      {
         // do nothing if nmea mode needs to be set
         printf("\nMode set: nmea\n");
      }
   }
   else if( 1 == CmdLineOptions.send_cmd )
   {
      printf("\n----------Sending command to Teseo----------\n");
      if( 0 != (retval = gnss_tool_send_cmd_to_teseo()) )
      {
         printf("\nError sending command to Teseo\n");
      }
      else
      {
         printf("\nSending command to Teseo successful");
      }

   }
   else if( 1 == CmdLineOptions.release)
   {
       printf("\n----------Releasing Teseo----------\n");
      if(0 != (retval = gnss_tool_change_teseo_mode(MODE_NMEA)))
      {
         printf("\nError during Teseo release\n");
      }
      else
      {
         printf("\nTeseo Release successful");
      }
   }
   else
   {
      //nothing to do
      retval = 0;
   }
   return retval;
}

/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_write_teseo_status_regs(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Writes Status registers into Teseo
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 29.SEPT.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_write_teseo_status_regs(void)
{
   int retval;
   //set teseo in boot mode
   if( 0 != (retval = put_teseo_in_boot_mode()) )
   {
      printf("\nSetting Teseo in boot mode failed");
   }
   //get gnss data buffer size on scc.
   else if( 0 != (retval = gnss_tool_store_buffer_size_on_scc()) )
   {
      printf("\nUnable to get Gnss Buffer size on SCC");
   }
   else
   {
      if(CHIP_TESEO2 == CmdLineOptions.chip_teseo)
      {
         if( 0 != (retval = gnss_tool_teseo2_write_teseo_regs()) )
         {
            printf("\nUnable to write Teseo2 Status Registers");
         }
      } 
      else if(CHIP_TESEO3 == CmdLineOptions.chip_teseo)
      {
         if( 0 != (retval = gnss_tool_teseo3_write_teseo_regs()) )
         {
            printf("\nUnable to write Teseo3 Status Registers");
         }
      }  
      else
      {
         printf("\nInvalid Chip type");
         retval = -E_INVALIDARGS;
      }
   }
    //Return teseo to normal operation - pos mode
   if(0 != gnss_tool_change_teseo_mode(MODE_NMEA))
   {
      printf("\nUnable to change Teseo mode to Normal");
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_send_reg_vals_to_teseo(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Sends Status registers to Teseo individually
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 29.SEPT.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_send_reg_vals_to_teseo(void)
{
   char regval;
   int regIndex = 0;
   //Send register values to Teseo individually
   while(regIndex < CmdLineOptions.num_of_regs)
   {
      regval = CmdLineOptions.reg_values[regIndex++];
      printf("\nWriting value - 0x%x into R%d", regval, regIndex);
      
      if( 0 != gnss_tool_send_byte_to_teseo_without_ACK(regval) )
      {
         printf("\nCould not send register to Teseo");
         return -E_SENDMSGTOSCC;
      }
   }
   if( 0 != gnss_tool_wait_for_teseo_ACK() )
   {
      printf("\nTeseo ACK not received for Register write");
      return -E_TESEOACKRECV;
   }
   //printf("\nRegisters written successfully");
    return 0;
}

/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_wait_for_teseo_ACK(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Waits for Teseo ACK on sending Flash Data
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 29.SEPT.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_wait_for_teseo_ACK(void)
{
   char ExpectedResponse[3] = { GNSS_MSG_ID_RES_CONTROL, 
                      GNSS_MSG_RESPONSE_FLASH_DATA, 
                      GNSS_TESEO_ACK };

   if( 0 > gnss_tool_recv_msg(data_recv_buf, "Teseo ACK", sizeof(data_recv_buf), WAIT_5SEC) )//wait_for_msg( data_recv_buf, "ASDF",sizeof(data_recv_buf),5000) ))
   {
       printf("\nFailed to receive Teseo ACK");
       return -E_WAITFORSCCRESPONSE;
   }
   if( 0 != gnss_tool_validate_msg(ExpectedResponse, data_recv_buf, sizeof(ExpectedResponse)) )
   {
      printf("\nFailed to get Teseo ACK");
      return -E_WRONGRESPONSE;
   }
   return 0;
}

/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_wait_for_SCC_OK(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Waits for OK to be received from SCC for Flash Data
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 29.SEPT.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_wait_for_SCC_OK(void)
{
   char ExpectedResponse[3] = { GNSS_MSG_ID_RES_CONTROL, 
                      GNSS_MSG_CMD_CONTROL_FLASH_DATA, 
                      GNSS_MSG_RESPONSE_SCC_OK };
   if( 0 > gnss_tool_recv_msg(data_recv_buf, "SCC OK", sizeof(data_recv_buf), WAIT_5SEC))
   {
       printf("\nFailed to get data from SCC");
       return -E_WAITFORSCCRESPONSE;
   }
   if( 0 != gnss_tool_validate_msg(ExpectedResponse, data_recv_buf, sizeof(ExpectedResponse)))
   {
      printf("\nSCC OK not received");
      return -E_WRONGRESPONSE;
   }
   return 0;
}

/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_send_byte_to_teseo_without_ACK(char data)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Sends a byte to Teseo (FLASH_DATA type) and does not wait for an ACK
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 29.SEPT.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_send_byte_to_teseo_without_ACK(char data)
{
   char msg_buf[5] = { GNSS_INC_TP_MARKER_HEADER, 
                       FILLER_INIT, 
                       GNSS_MSG_ID_CMD_CONTROL, 
                       GNSS_MSG_CMD_CONTROL_FLASH_DATA, 
                       data };
   msg_buf[1] = msg_seq_counter++;
                      
   if( 5 != gnss_tool_send_msg_to_scc(msg_buf, 5) )
   {
      printf("\nFailed to send data to SCC");
      return -E_SENDMSGTOSCC;
   }
   if( 0!= gnss_tool_wait_for_SCC_OK() )
   {
      printf("\nFailed to receive SCC OK");
      return -E_NORESPONSEFROMSCC;
   }
   //printf("\nData sent successfully");
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_send_byte_to_teseo_wait_for_ACK(char data)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Sends a byte to Teseo (FLASH_DATA type) and waits for ACK
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 29.SEPT.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_send_byte_to_teseo_wait_for_ACK(char data)
{
   char msg_buf[5] = { GNSS_INC_TP_MARKER_HEADER, 
                       FILLER_INIT, 
                       GNSS_MSG_ID_CMD_CONTROL, 
                       GNSS_MSG_CMD_CONTROL_FLASH_DATA, 
                       data };
   msg_buf[1]  = msg_seq_counter++;

   if( 5 != gnss_tool_send_msg_to_scc( msg_buf, sizeof(msg_buf) ) )
   {
      printf("\nFailed to data to SCC");
      return -E_SENDMSGTOSCC;
   }
   if( 0 != gnss_tool_wait_for_SCC_OK() )
   {
      printf("\nFailed to receive SCC OK");
      return -E_NORESPONSEFROMSCC;
   }
   if( 0 != gnss_tool_wait_for_teseo_ACK() )
   {
      printf("\nFailed to receive ACK from Teseo");
      return -E_TESEOACKRECV;
   }
   //printf("\nData sent successfully");
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_send_cmd_to_teseo(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : Interface to send commands to Teseo directly
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_send_cmd_to_teseo(void)
{
   int retval;
   int len;
   time_t time_init, time_final;
   char cmdToTeseo[80] = {'\0'};
   unsigned char response_received = 0;

   data_send_buf[0] = GNSS_INC_TP_MARKER_HEADER;
   data_send_buf[1] = msg_seq_counter++;
   data_send_buf[2] = GNSS_MSG_ID_CMD_CONTROL;
   data_send_buf[3] = GNSS_MSG_CMD_CONTROL_FLASH_DATA;
   len = (int)strlen(CmdLineOptions.nmea_cmd);

   strncpy( cmdToTeseo, CmdLineOptions.nmea_cmd, (unsigned int)len);

   cmdToTeseo[len] = '\r';
   cmdToTeseo[len + 1] = '\n'; 

   //printf("\nCommand: %s", cmdToTeseo);
   len += 2;
   memcpy(&data_send_buf[4], cmdToTeseo, (unsigned int)len);

   len +=4;

   if( len != gnss_tool_send_msg_to_scc(data_send_buf, len) )
   {
      printf("\nCould not send msg to SCC");
      return -E_SENDMSGTOSCC;
   }
   if( 0 != gnss_tool_wait_for_SCC_OK() )
   {
      printf("\nError receiving Msg OK from SCC");
      return -E_WAITFORSCCRESPONSE;
   }
   time_init = time(NULL);
   time_final = time(NULL);
   while((int)(time_final - time_init) < CmdLineOptions.timeout_cmd)
   {
       if( 0 > (retval = gnss_tool_recv_msg( data_recv_buf, "NMEA msg", sizeof(data_recv_buf), WAIT_5SEC )) )
       {
         //printf("\nError recving data");
       }
       else
       {
          response_received = 1;
          data_recv_buf[retval] = '\0';
          printf("\n%s", data_recv_buf);
       }
       time_final = time(NULL);
       //printf("\nTimestamp: %d sec", (finalTime - initTime));
   }
   //if no response is received from Teseo for the command sent, within timeout.
   if( 0 == response_received )
   {
      printf("\nNo response from Teseo");
      return -E_TESEODATA;
   }
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_store_buffer_size_on_scc(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : Scans DebugOptions and performs tasks such as flashdump, registry read accordingly
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_store_buffer_size_on_scc( void )
{
   int retval = 0;
   char msg_buf[4] = { GNSS_INC_TP_MARKER_HEADER, 
                       FILLER_INIT, 
                       GNSS_MSG_ID_CMD_CONTROL, 
                       GNSS_MSG_CONTROL_FLASH_BUF_SIZE };

   char ExpectedResponse[3] = { GNSS_MSG_ID_RES_CONTROL, 
                      GNSS_MSG_CONTROL_FLASH_BUF_SIZE, 
                      GNSS_MSG_RESPONSE_SCC_OK };

   msg_buf[1] = msg_seq_counter++;

   //send buffer request command to scc.
   if( 4 != gnss_tool_send_msg_to_scc(msg_buf, sizeof(msg_buf) ))
   {
      printf("\nFailed to send Buffer size request command");
      retval = -E_SENDMSGTOSCC;
   }
   //wait for response.
   else if( 5 != gnss_tool_recv_msg(data_recv_buf, "Buffer size", sizeof(data_recv_buf), WAIT_5SEC))
   {
      printf("\nFailed to recv Buffer size response");
      retval = -E_WAITFORSCCRESPONSE;
   }
   //validate the response received from SCC
   else if( 0 != gnss_tool_validate_msg(ExpectedResponse, data_recv_buf, sizeof(ExpectedResponse)))
   {
      printf("\nWrong response received");
      retval = -E_WRONGRESPONSE;
   }
   //copy buffer size
   else
   {
      gnss_buf_size_on_scc = data_recv_buf[4] << 8 | data_recv_buf[3];
      printf("\nBuffer size read successfully. Size = %d", gnss_buf_size_on_scc);
      // check to ensure that send buffer is always less than GNSS_FW_CFG_DEVICE_COMM_NMEA_BUFFER_SIZE on SCC
      if( (int)sizeof(data_send_buf) > gnss_buf_size_on_scc)
      {
         printf("\nBuffer size cannot be accomodated on SCC");
         return -E_INITCOMMSCC;
      }
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_send_flasher_to_teseo(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : Transfers flasher.bin to Teseo
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_send_flasher_to_teseo( void )
{
   int no_of_bytes_read;
   unsigned int fsize;
   unsigned int txsize = 0, prevtxsize = 0;
   struct stat st;
   
   if( 0 != stat(CmdLineOptions.name_flasher, &st))
   {
      printf("\nFlasher file stat could not be computed");
      return -E_FLASHER;
   }
   fsize = (unsigned int)st.st_size;

   data_send_buf[0] = GNSS_INC_TP_MARKER_HEADER;
   data_send_buf[2] = GNSS_MSG_ID_CMD_CONTROL;
   data_send_buf[3] = GNSS_MSG_CMD_CONTROL_FLASH_DATA;

   while ( 0 < ( no_of_bytes_read = read(CmdLineOptions.fd_flasher, &data_send_buf[4], sizeof(data_send_buf) - 4 )) )
   {
      data_send_buf[1] = msg_seq_counter++;
      if( (4 + no_of_bytes_read) != gnss_tool_send_msg_to_scc( data_send_buf, 4 + no_of_bytes_read ) )
      {
         printf("\nFailed to send flasher data to SCC");
         return -E_SENDMSGTOSCC;
      }
      if( 0 != gnss_tool_wait_for_SCC_OK() )
      {
          printf("\nFailed to get SCC OK for flasher");
          return -E_WAITFORSCCRESPONSE;
      }
      txsize += (unsigned int)no_of_bytes_read;

      //to print the status of transfer for every 5% transfer
      if( (((txsize*100)/fsize - (prevtxsize*100)/fsize) > 5) || (100 == (txsize*100)/fsize) )
      {
         prevtxsize = txsize;
         fflush(stdout);
         printf("\rFlasher transfer in progress:   %u / %u...................%u%%", txsize, fsize, (txsize*100)/fsize);
         fflush(stdout);
      }
   }

   if( no_of_bytes_read == 0 )
   {
      if( 0 > lseek(CmdLineOptions.fd_flasher, 0, SEEK_SET))
      {
         printf("\nlseek failed");
         return -E_FILESEEK;
      }
      if( 0 != gnss_tool_wait_for_teseo_ACK() )
      {
         printf("\nFailed to get Teseo ACK for Flasher");
         return -E_TESEOACKRECV;
      }

      printf("\nFlasher transferred successfully\n");
      return 0;
   }
   printf("\nFlasher read failed");
   return -E_FILEREAD;
}


/*********************************************************************************************************************
* FUNCTION     : int gnss_tool_recv_msg( char *data_buf, char *wait_msg, int msg_size,  int timeout) 
* PARAMETER    : char *, char *, int , unsigned int 
* RETURNVALUE  : size of received message -  on success
*                         Non-zero error value on Failure
* DESCRIPTION  : Waits for message from SCC
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_recv_msg( char *data_buf, char *wait_msg, int msg_size, int timeout) 
{
   int retval;

   (void)wait_msg;
   //printf("\nWaiting on message: %s\n", wait_msg);
   if( 0 >= (retval = gnss_tool_wait_for_msg_from_scc( data_buf, msg_size, timeout )) )
   {
      retval = -E_NORESPONSEFROMSCC;
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_recv_fdump_data_from_teseo( void )
* PARAMETER    : None
* RETURNVALUE  : size of message received - on success
*                         Non-zero error value on Failure
* DESCRIPTION  : Receives message from SCC
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_recv_fdump_data_from_teseo( void )
{
   int retval;

   if( 0 >= ( retval = gnss_tool_wait_for_msg_from_scc( data_recv_buf, sizeof(data_recv_buf), WAIT_20SEC) ))
   {
       printf("\nTimeout during recv from SCC" );
       return -E_WAITFORSCCRESPONSE;
   }
   return (retval - 2);
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_send_msg_to_scc( char *msg, int size )
* PARAMETER    : char *, int  
* RETURNVALUE  : size of message sent - on success
*                          error value on Failure
* DESCRIPTION  : Sends message to SCC
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_send_msg_to_scc( char *msg, int size )
{
   if(debug_print_enable)
   {
      int indx;
      printf("\nSent data size: %d\n", size);
      for( indx = 0; indx < size; indx++ )
      {
         printf(" %x ",msg[indx]);
      }
      printf("\n");
   }
   return dgram_send(sock_dgram_handle, (void*)msg, (unsigned int)size);
}


/*********************************************************************************************************************
* FUNCTION     : static int  gnss_tool_send_ack_to_teseo( void )
* PARAMETER    : None
* RETURNVALUE  : size of message sent - on success
*                          error value on Failure
* DESCRIPTION  : Sends message to SCC
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int  gnss_tool_send_ack_to_teseo( void )
{
   char msg[5] = { GNSS_INC_TP_MARKER_HEADER, 
                   FILLER_INIT, 
                   GNSS_MSG_ID_CMD_CONTROL, 
                   GNSS_MSG_CMD_CONTROL_FLASH_DATA, 
                   GNSS_TESEO_ACK };

   msg[1] = msg_seq_counter++;

   if( 5 != gnss_tool_send_msg_to_scc( msg, 5) )
   {
      printf("\nFailed to send ACK msg to SCC");
      return -E_SENDMSGTOSCC;
   }
   if( 0 != gnss_tool_wait_for_SCC_OK() )
   {
      printf("\nSCC OK not received for Teseo ACK");
      return -E_WAITFORSCCRESPONSE;
   }
   //printf("\nACK sent to Teseo successfully\n");
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static void gnss_tool_print_debug_options(const char *)
* PARAMETER    : None
* RETURNVALUE  : None
*                         
* DESCRIPTION  : Prints the options provided by the tool
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static void gnss_tool_print_debug_options( const char *name_binary)
{
   printf("\n--------------------------------------------------------------------------------\n");
   printf("\nAvailable options :");
   printf("\n1. %s  [--dump flash]     [--flasher flasher.bin]  [-o OutputFilename]  [--size SizeInWords]     [-v TeseoVersion]", name_binary);
   printf("\n2. %s  [--dump register]  [--flasher flasher.bin]  [-v TeseoVersion]", name_binary);
   printf("\n3. %s  [--write register NumOfRegs R1(in hex) R2(in hex) R3(in hex)]    [--flasher flasher.bin]  [-v TeseoVersion]", name_binary);
   printf("\n4. %s  [--mode ModeSelect]", name_binary);
   printf("\n5. %s  [--cmd CommandToTeseo]", name_binary);
   printf("\n6. %s  [--release]", name_binary);
   printf("\n\n--------------------------------------------------------------------------------\n");
   printf("\nNote::\n1.flasher.bin = flasher_teseo2.bin/flasher_teseo3.bin for Teseo2/Teseo3 respectively \n2.TeseoVersion = teseo2/teseo3");   
   printf("\n\n--------------------------------------------------------------------------------\n");
}

/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_parse_cmdline_args( int argc, char * const * argv )
* PARAMETER    : int argc, const char ** argv
* RETURNVALUE  : int
*                         
* DESCRIPTION  : Parses command line options sent to the binary
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* 29.SEPT.2016  | Using getopt.h library for parsing options
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_parse_cmdline_args( int argc, char * const * argv )
{
   int retval = 0;
   int index; 
   int option_index = 0;
   int valTimeout;   
   static struct option availcmdOptions[] =
   {
      /* These options don�t set a flag.
      We distinguish them by their indices. */
      {"dump",     required_argument, 0, 'd'},
      {"flasher",  required_argument, 0, 'f'},
      {"mode",     required_argument, 0, 'm'},
      {"cmd",      required_argument, 0, 'c'},
      {"size",     required_argument, 0, 's'},
      {"out",      required_argument, 0, 'o'},
      {"timeout",  required_argument, 0, 't'},
      {"release",  no_argument,       0, 'r'},
      {"version",  required_argument, 0, 'v'},
      {"write",    required_argument, 0, 'w'},
      {"debug",    no_argument,       0, 'x'},
      {0, 0, 0, 0}
   };
   const char *shortoptions = "d:f:m:c:s:o:t:v:w:x";
      
   //check the passed arguments.
   if(( argc < GNSS_TOOL_MIN_ARGS) || (argc > GNSS_TOOL_MAX_ARGS ))
   {
      return -E_MISSINGARGS;
   }

   while(-1 != (index = getopt_long (argc, argv, shortoptions, availcmdOptions, &option_index)))
   {
      switch (index)
      {
         case 'd':
         {
             CmdLineOptions.dump = 1;
             if(!strncmp(optarg, "flash", sizeof("flash")))
             {
                CmdLineOptions.dump_flash = 1;
             }
             else if(!strncmp(optarg, "register", sizeof("register")))
             {
                CmdLineOptions.dump_register = 1;
             }
             else
             {
                printf("\nInvalid option selected");
                return -E_INVALIDARGS;
             }
             break;
         }
         case 'f':
         {
             if((strlen(optarg)+1) > sizeof(CmdLineOptions.name_flasher))
             {
                printf("\nFlasher name too long");
                return -E_INVALIDARGS;
             }
             strncpy(CmdLineOptions.name_flasher, optarg, strlen(optarg)+1);
             break;
         } 
         case 'o':
         {
            if((strlen(optarg)+1) > sizeof(CmdLineOptions.name_output))
            {
               printf("\nOutput file name too long");
               return -E_INVALIDARGS;
            }
            strncpy(CmdLineOptions.name_output, optarg, strlen(optarg)+1);
            break;
         }
         case 's':
         {            
            if(0 >= (size_flashdump = (int)strtol(optarg, NULL, 10)))
            {
               size_flashdump = FLASH_DUMP_WORD_2MB;
               printf("\nInvalid size. Resetting to default size: %d words", FLASH_DUMP_WORD_2MB);
            }
            break;
         }
         case 'm':
         {
             if(!strncmp(optarg, "nmea", sizeof("nmea")))
             {
                CmdLineOptions.set_mode = MODE_NMEA;
             }
             else if(!strncmp(optarg, "xloader", sizeof("xloader")))
             {
                CmdLineOptions.set_mode = MODE_XLOADER;
             }
             else
             {
                printf("\nInvalid mode. Select nmea/xloader");
                return -E_INVALIDARGS;
             }
             CmdLineOptions.change_mode = 1;
             break;
         } 
         case 'r':
         {
             CmdLineOptions.release = 1;
             break;
         }   
         case 'c':
         {
             if((strlen(optarg)+1) > sizeof(CmdLineOptions.nmea_cmd))
             {
                printf("\nNMEA command too long");
                return -E_INVALIDARGS;
             }
             CmdLineOptions.send_cmd = 1;
             strncpy(CmdLineOptions.nmea_cmd, optarg, strlen(optarg)+1);
             break;
         } 
         case 't':
         {     
            if(0 >= (valTimeout = (int)strtol(optarg, NULL, 10)))
            {
                printf("\nInvalid timeout. Resetting to default timeout: %d sec", CmdLineOptions.timeout_cmd);
            }
            else
            {
               CmdLineOptions.timeout_cmd = valTimeout;
            }
            break;
         }
         case 'v':
         {
             if(!strncmp(optarg, "teseo2", sizeof("teseo2")))
             {
                CmdLineOptions.chip_teseo = CHIP_TESEO2;
             }
             else if(!strncmp(optarg, "teseo3", sizeof("teseo3")))
             {
                CmdLineOptions.chip_teseo = CHIP_TESEO3;
             }
             else
             {
                printf("\nInvalid Chip type. Select teseo2 / teseo3");
                return -E_INVALIDARGS;
             }
             break;
         }
         case 'w':
         {
            CmdLineOptions.write = 1;
            if(!strncmp(optarg, "register", sizeof("flash")))
            {
               CmdLineOptions.write_register = 1;
               if(0 != gnss_tool_get_reg_write_subopts(argc, argv))
               {
                  return -E_INVALIDARGS;
               }
            }
            else
            {
               printf("\nInvalid option selected");
               return -E_INVALIDARGS;
            }
            break;
         }
         case 'x':
         {
            printf("\nDebug mode enabled");
            debug_print_enable = 1;   
            break;
         }
         case '?':
         {
             retval = -E_INVALIDARGS;
             break;
         }             
         default:
         {
            printf("\nWrong parameter");
            break;
         }
      }
   }

   if (optind < argc)
   {
     printf ("\nnon-option ARGV-elements:");
     while (optind < argc)
        printf ("\n%s", argv[optind++]);
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_get_reg_write_subopts(int argc, char * const *argv)
* PARAMETER    : int , const char ** 
* RETURNVALUE  : 0  on success
*                Error value on Failure
* DESCRIPTION  : Scans and stores the register values sent in the command line arguments
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_get_reg_write_subopts(int argc, char * const * argv)
{
   int index = optind; // to set the index to Register count value
   int regindex = 0;
   
   CmdLineOptions.num_of_regs = (int)strtol(argv[index++], NULL, 10);
   
   if( (CmdLineOptions.num_of_regs <= 0) || (CmdLineOptions.num_of_regs > MAX_NUM_OF_STATUS_REGISTERS) )
   {
      printf("\nInvalid register count. Valid values : 1-%d", MAX_NUM_OF_STATUS_REGISTERS);
      return -E_INVALIDARGS;
   }
   
   while((index < argc) && (regindex < CmdLineOptions.num_of_regs))
   {
      if( EINVAL == (CmdLineOptions.reg_values[regindex++] = (char)strtol(argv[index++], NULL, 16)))
      {
         printf("\nInvalid register value");
         return -E_INVALIDARGS;
      }
   }
   if(regindex != CmdLineOptions.num_of_regs)
   {
      return -E_INVALIDARGS;
   }
   optind = index; // Register values have already been scanned. Setting the opt index to next available option.
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static int  gnss_tool_wait_for_msg_from_scc( char *msg,int size,int wait_time_in_msec )
* PARAMETER    : char *,int ,int 
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : polls for message from SCC
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int  gnss_tool_wait_for_msg_from_scc( char *msg,int size,int wait_time_in_msec )
{

   struct pollfd fd_to_poll;
   int no_of_poll_iter = wait_time_in_msec/100;
   int ret_val;
   int indx;

   fd_to_poll.fd = fd_sock;
   fd_to_poll.revents = 0;
   fd_to_poll.events = POLLIN;
   nfds_t no_of_fds = 1;

   while( no_of_poll_iter-- )
   {
      ret_val = poll( &fd_to_poll, no_of_fds, 100 );
      if(ret_val < 0)
      {
         printf("\npoll failed");
         return 0;
      }

      if(ret_val == 0)
      {
         //printf("\npoll time out. %dms left before return", no_of_poll_iter*100 );
         continue;
      }

      if(fd_to_poll.revents & POLLIN )
      {
        if( 0 > ( ret_val = dgram_recv(sock_dgram_handle, (void*)msg, (unsigned int)size)))
        {
           printf("\ndgram_recv failed");
           return 0;
        }
        else
        {
           if(debug_print_enable)
           {
              printf("\nReceived data size: %d\n", ret_val);
              for(indx = 0; indx < ret_val; indx++)
              {
                 printf(" %x ", msg[indx]);
              }
              printf("\n");
           }
           return ret_val;
        }
      }
   }
   printf("\nUnable to get data from SCC in %dms", wait_time_in_msec);
   return 0;
}

/*********************************************************************************************************************
* FUNCTION     : gnss_tool_exch_status_with_scc(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Exchanges Status with SCC.
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/

static int gnss_tool_exch_status_with_scc( void )
{
   char buf[3] = { GNSS_MSG_CONTROL_COMP_STATUS_MSGID, 
                   GNSS_APP_COMPONENT_STATUS_ACTIVE, 
                   GNSS_FW_UPDATE_COMPONENT_VERSION_THREE };

   char ExpectedResponse[2] = { GNSS_MSG_RESPONSE_COMP_STATUS_MSGID, 
                      GNSS_SCC_COMPONENT_STATUS_ACTIVE };// 0x01 - STATUS_ACTIVE

   int retry = 5;
   int retval;

   while(retry-- )
   {
      //send app status to gnss component on scc.
      if( 3 != gnss_tool_send_msg_to_scc(buf, 3) )
      {
          printf("\nFailed to send Status msg to SCC");
          retval = -E_SENDMSGTOSCC;
      }
      //wait for response from gnss component  on scc
      else if( 0 > (retval = gnss_tool_recv_msg(data_recv_buf, "Status Exch", sizeof(data_recv_buf), WAIT_5SEC)) )
      {
         printf("\nFailed to receive Status OK from SCC");
      }
      else if( 3 != retval )
      {
         printf("\nIncorrect msg size for status exchange");
         retval = -E_WRONGRESPONSE;
      }
      //validate status message.
      else if( 0 != (retval = gnss_tool_validate_msg(ExpectedResponse, data_recv_buf, sizeof(ExpectedResponse))) )
      {
         printf("\nWrong Response received");
      }
      //Successfully read GNSS version
      else
      {
         gnss_version_on_scc = data_recv_buf[2];
         printf("\nGnss version on SCC is 0x%x", gnss_version_on_scc);
         return 0;
      }

      printf("\nRetrying to exchange status with SCC");
      if( 0 != gnss_tool_reinit_resources() )
      {
         printf("\nRe-init failed");
         //end retries
         retry = 0;
      }
   }
   return  -E_STATUSEXCHANGE;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_reinit_resources(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Tries to reinit comm with SCC
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_reinit_resources( void )
{
   gnss_tool_deinit_resources();
   if( 0 != gnss_tool_setup_comm_with_scc() )
   {
      printf("\nError setting up Comm with SCC");
      return -E_INITCOMMSCC;
   }
   return 0;
}

/*********************************************************************************************************************
* FUNCTION     : gnss_tool_setup_comm_with_scc(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Sets up socket communication with SCC
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_setup_comm_with_scc( void )
{
   struct hostent *host_name;
   struct sockaddr_in sockaddr_app,sockaddr_scc;

   //create socket.
   fd_sock = socket( AF_BOSCH_INC_AUTOSAR, (int)SOCK_STREAM, 0 );
   if(fd_sock < 0)
   {
      printf("\nSocket creation failed with error: %s",strerror(errno));
      return -E_CREATESOCKET;
   }
   host_name = gethostbyname("scc-local");
   if(!host_name)
   {
      printf("\nGet host address(scc-local) by name failed error: %d\n",h_errno);
      return -E_GETHOSTBYNAME; 
   }

   sockaddr_app.sin_family = AF_BOSCH_INC_AUTOSAR;
   sockaddr_app.sin_port   = htons( GNSS_FW_UPDATE_PORT | PORT_OFFSET );
   memcpy( &sockaddr_app.sin_addr.s_addr,
           host_name->h_addr,
           (unsigned int)(host_name->h_length) );

   //bind the socket to the port.
   if( bind( fd_sock,(const struct sockaddr *)(void*)&sockaddr_app, sizeof(sockaddr_app)) )
   {
      printf("\nBinding the socket failed with %d. error: %s\n", errno, strerror(errno));
      return -E_BIND;
   }

   //get the scc ip details.
   host_name = gethostbyname("scc");
   if(!host_name)
   {
      printf("\nGetting host address(scc) by name failed with error: %d\n",h_errno);
      return -E_GETHOSTBYNAME;
   }

   sockaddr_scc.sin_family = AF_BOSCH_INC_AUTOSAR;
   sockaddr_scc.sin_port   = htons( GNSS_FW_UPDATE_PORT|PORT_OFFSET );
   memcpy( &sockaddr_scc.sin_addr.s_addr, host_name->h_addr, (unsigned int )(host_name->h_length) );

   //connect to the scc.   
   if( connect( fd_sock,(const struct sockaddr *)(void*)&sockaddr_scc, sizeof(sockaddr_scc) ))
   {
      printf("\nConnecting to remote socket failed with %d. error %s\n",errno,strerror(errno));
      return -E_CONNECT;
   }

   //initialise dgram services.
   sock_dgram_handle = dgram_init(fd_sock, GNSS_MAX_HANDLED_PACKET_SIZE, NULL);
   if( !sock_dgram_handle )
   {
      printf( "\nUnable to initialize dgram_services\n");
      return -E_DGRAMINIT;
   }

   printf("\nEstablished communication with SCC successfully");
   return 0;
}


/*----------------------------- Teseo2 specific functions -----------------------------------------*/


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo2_send_binary_image_options( BinOpts_Mode mode )
* PARAMETER    : BinOpts_Mode
* RETURNVALUE  : 0  on success
*                         Error value which caused the failure, on Failure
* DESCRIPTION  : Sends binary options to Teseo depending on the parameter - mode
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo2_send_binary_image_options( BinOpts_Mode mode )
{
   data_send_buf[0] = GNSS_INC_TP_MARKER_HEADER;
   data_send_buf[1] = msg_seq_counter++;
   data_send_buf[2] = GNSS_MSG_ID_CMD_CONTROL;
   data_send_buf[3] = GNSS_MSG_CMD_CONTROL_FLASH_DATA;

   memcpy(&data_send_buf[4], &image_options_teseo2[mode], (unsigned int)sizeof(Teseo2_BinaryImageOptions));

   if( (int)(4+ sizeof(Teseo2_BinaryImageOptions)) != gnss_tool_send_msg_to_scc( data_send_buf, (int)(4+sizeof(Teseo2_BinaryImageOptions)) ) )
   {
      printf("\nFailed to send image_options to SCC");
      return -E_SENDMSGTOSCC;
   }
   if( 0 != gnss_tool_wait_for_SCC_OK() )
   {
      printf("\nFailed to get SCC OK for Binary options");
      return -E_WAITFORSCCRESPONSE;
   }
   if( 0 != gnss_tool_wait_for_teseo_ACK() )
   {
      printf("\nFailed to get Teseo ACK for Binary options");
      return -E_TESEOACKRECV;
   }
   //printf("\nBinary Options sent successfully");
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo2_write_teseo_regs(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Writes Status registers of Teseo2
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 29.SEPT.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo2_write_teseo_regs(void)
{
   int retval;
   //transfer flasher.bin to scc.
   if( 0 != (retval = gnss_tool_send_flasher_to_teseo()) )
   {
      printf("\nFailed to transfer flasher.bin to SCC");
   }
   //send host_ready to scc
   else if( 0 != (retval = gnss_tool_teseo2_send_host_ready()) )
   {
      printf("\nFailed to send HOST_READY during regread");
   }
   //send binary image options
   else if( 0 != (retval = gnss_tool_teseo2_send_binary_image_options(REGWRITE)) )
   {
      printf("\nFailed to send binary image options");
   }
   //send flasher ready
   else if( 0 != (retval = gnss_tool_teseo2_send_flasher_ready()) )
   {
      printf("\nFailed to flasher ready to SCC");
   }
   //send register values
   else if( 0 != (retval = gnss_tool_send_reg_vals_to_teseo()) )
   {
      printf("\nFailed to Write Teseo Status registers");
   }
   else
   {
      printf("\nRegister write successful");
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo2_get_status_regs(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Non-zero Error value on failure
* DESCRIPTION  : Gets the status register values from Teseo2
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo2_get_status_regs(void)
{
   int retval;
   //transfer flasher.bin to scc.
   if( 0 != (retval = gnss_tool_send_flasher_to_teseo()) )
   {
      printf("\nFailed to transfer flasher.bin to SCC");
   }
   //send host_ready to scc
   else if( 0 != (retval = gnss_tool_teseo2_send_host_ready()) )
   {
      printf("\nFailed to send HOST_READY during regread");
   }
   //send binary image options
   else if( 0 != (retval = gnss_tool_teseo2_send_binary_image_options(REGREAD)) )
   {
      printf("\nFailed to send binary image options");
   }
   else if( 0 != (retval = gnss_tool_teseo2_send_flasher_ready( )) )
   {
      printf("\nFailed to flasher ready to SCC");
   }
   //wait for dump data.
   else if( 0 != (retval = gnss_tool_recv_regs_from_teseo()) )
   {
      printf("\nFailed to read Teseo status registers");
   }
   else
   {
      //Regread successful
      //printf("\nStatus registers read successfully");
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo2_send_host_ready(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Sends Host Ready to Teseo2
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo2_send_host_ready(void)
{
   return gnss_tool_send_byte_to_teseo_wait_for_ACK(GNSS_TESEO2_HOST_READY);
}

/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo2_send_flasher_ready(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Sends Flasher Ready to Teseo2
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo2_send_flasher_ready(void)
{
   return gnss_tool_send_byte_to_teseo_without_ACK(GNSS_TESEO2_FLASHER_READY);
}


/*----------------------------- Teseo3 specific functions -----------------------------------------*/


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_write_teseo_regs(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Writes Status registers of Teseo3
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 29.SEPT.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_write_teseo_regs(void)
{
   int retval;
   //transfer flasher.bin to scc.
   if( 0 != (retval = gnss_tool_teseo3_send_uart_sync()) )
   {
      printf("\nUnable to send UART sync");
   }
   //send host ready
   else if( 0 != (retval = gnss_tool_teseo3_send_host_ready()) )
   {
      printf("\nFailed to transfer flasher.bin to SCC");
   }
   //send flasher sync to teseo
   else if( 0 != (retval = gnss_tool_teseo3_send_flasher_sync()) )
   {
      printf("\nUnable to send Flasher sync");
   }
   //send flasher.bin to teseo
   else if( 0 != (retval = gnss_tool_send_flasher_to_teseo()) )
   {
      printf("\nFailed to transfer flasher.bin to SCC");
   }
   //send flasher ready to scc
   else if( 0 != (retval = gnss_tool_teseo3_send_flasher_ready()) )
   {
      printf("\nFailed to send HOST_READY during regread");
   }
   //send binary image options
   else if( 0 != (retval = gnss_tool_teseo3_send_binary_image_options(REGWRITE)) )
   {
      printf("\nFailed to send binary image options");
   }
   else if( 0 != (retval = gnss_tool_teseo3_send_flasher_ready_without_ACK()) )
   {
      printf("\nFailed to flasher ready to SCC");
   }
   else if( 0 != (retval = gnss_tool_send_reg_vals_to_teseo()) )
   {
      printf("\nFailed to Write Teseo Status registers");
   }
   else
   {
      //printf("\nRegister write successful");
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_get_status_regs(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                         Non-zero Error value on failure
* DESCRIPTION  : Gets the status register values from Teseo3
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_get_status_regs(void)
{
   int retval;
   if( 0 != (retval = gnss_tool_teseo3_send_uart_sync()) )
   {
      printf("\nUART sync failed");
   }
   //Send host ready to Teseo 0x5a
   else if( 0 != (retval = gnss_tool_teseo3_send_host_ready()) )
   {
      printf("\nSending host ready failed");
   }
   else if( 0 != (retval = gnss_tool_teseo3_send_flasher_sync()) )
   {
      printf("\nSending flasher sync options failed");
   }
   //transfer flasher.bin to scc.
   else if( 0 != (retval = gnss_tool_send_flasher_to_teseo()) )
   {
      printf("\nFailed to transfer flasher.bin to SCC");
   }
   else if( 0 != (retval = gnss_tool_teseo3_send_flasher_ready()) )
   {
      printf("\nFailed to send Flasher Ready to Teseo");
   }
   //send binary image options
   else if( 0 != (retval = gnss_tool_teseo3_send_binary_image_options(REGREAD)) )
   {
      printf("\nFailed to send binary image options");
   }
   else if( 0 != (retval = gnss_tool_teseo3_send_flasher_ready_without_ACK()) )
   {
      printf("\nFailed to send Flasher Ready");
   }
   //wait for dump data.
   else if( 0 != (retval = gnss_tool_recv_regs_from_teseo( )) )
   {
      printf("\nFailed to read Teseo status registers");
   }
   else
   {
      //Regread successful
      //printf("\nStatus registers read successfully");
   }
   return retval;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_get_flash_dump(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on success
*                Error value which caused the failure, on Failure
* DESCRIPTION  : Gets Teseo3 flash dump
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_get_flash_dump(void)
{
   int recv_flash_size = 0, total_recv_size = 0;
   int total_debug_size = image_options_teseo3[FLASHDUMP].debug_size * 4;
   char *pholdcurr = teseo_data_holdbuffer;

   if( 0 != gnss_tool_teseo3_send_uart_sync() )
   {
      printf("\nFailed to send UART sync");
      return -E_UARTSYNC;
   }
   if( 0 != gnss_tool_teseo3_send_host_ready() )
   {
      printf("\nFailed to send Host ready");
      return -E_HOSTREADY;
   }
   if( 0 != gnss_tool_teseo3_send_flasher_sync() )
   {
      printf("\nFailed to send Flasher sync");
   }
   //transfer flasher.bin
   if( 0 !=  gnss_tool_send_flasher_to_teseo())
   {
      printf("\nFailed to transfer flasher.bin to SCC");
      return -E_FLASHER;
   }
   //send flasher ready to scc
   if( 0 != gnss_tool_teseo3_send_flasher_ready())
   {
      printf("\nFailed to send Flasher ready");
      return -E_HOSTREADY;
   }
   //send binary image options
   if( 0 != gnss_tool_teseo3_send_binary_image_options(FLASHDUMP))
   {
      printf("\nFailed to send Binary Image Options");
      return -E_BINARYOPTIONS;
   }
   if( 0 != gnss_tool_teseo3_send_flasher_ready_without_ACK())
   {
      printf("\nFailed to send FLASHER_READY");
      return -E_FLASHERREADY;
   }

   while( total_recv_size < total_debug_size )
   {
      //wait for dump data.
      if ( 0 >= (recv_flash_size = gnss_tool_recv_fdump_data_from_teseo()) )
      {
         printf("\nError receiving dump data from Teseo");
         return -E_TESEODATA;
      }
      total_recv_size += recv_flash_size;
      if ( (unsigned int)(pholdcurr - teseo_data_holdbuffer) < TESEO3_CHUNK_5KB )
      {
         memcpy(pholdcurr, &data_recv_buf[2], (unsigned int)recv_flash_size);
         pholdcurr += recv_flash_size;
      }

      //For every 5KB of data received, write the data to a file and send ACK to Teseo
      if (0 == (total_recv_size % TESEO3_CHUNK_5KB))
      {
         fflush(stdout);
         printf("\rReceived Flashdump size:   %d / %d...................%d%%", total_recv_size, total_debug_size,
                                                                            (total_recv_size*100/total_debug_size));
         fflush(stdout);
         //Write to file only when 5KB chunk of data is received from Teseo
         if( TESEO3_CHUNK_5KB != write(CmdLineOptions.fd_output, teseo_data_holdbuffer, TESEO3_CHUNK_5KB) )
         {
            printf("\nFile Write Error. Errno: %d", errno);
            return -E_FILEWRITE;
         }
         else
         {
            pholdcurr = teseo_data_holdbuffer;
            fsync(CmdLineOptions.fd_output);
         }
         if( 0 != gnss_tool_send_ack_to_teseo() )
         {
            printf("\nACK not sent to Teseo");
            return -E_TESEOACKSEND;
         }
      }
   }
   // if size of flashdump requested is not a multiple of Teseo2 chunk size
   if (0 != (total_recv_size % TESEO3_CHUNK_5KB))
   {
      printf("\nReceived Flashdump size:   %d / %d...................%d%%", total_recv_size, total_debug_size,
                                                      (total_recv_size*100/total_debug_size));
      if( (pholdcurr - teseo_data_holdbuffer) != write( CmdLineOptions.fd_output, teseo_data_holdbuffer, (unsigned int)(pholdcurr - teseo_data_holdbuffer)) )
      {
         printf("\nFile Write Error. Errno: %d", errno);
         return -E_FILEWRITE;
      }
      else
      {
         fsync(CmdLineOptions.fd_output);
      }
   }
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static unsigned int  gnss_tool_teseo3_u32_crc32(unsigned int crc32val, const char *buf, int len)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Calculates CRC for the given block
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static unsigned int  gnss_tool_teseo3_u32_crc32(unsigned int crc32val, const char *buf, int len)
{
   int i;
   //printf("CRC32 line %d crc %u\n", __LINE__, crc32val );
   if (buf == 0) return 0L;
   
   crc32val = crc32val ^ 0xffffffff;
   for (i = 0;  i < len;  i++) {
       crc32val = crc32_tab[(crc32val ^ buf[i]) & 0xff] ^ (crc32val >> 8);
   }
   //printf("CRC32 line %d crc ^ ~0U %x\n", __LINE__, (crc32val ^ 0xffffffff) );
   return crc32val ^ 0xffffffff;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_get_flasher_CRC32( int fp, unsigned int *pu32FinalCrc, unsigned int u32ImageLength )
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Calculates CRC for the entire flasher file
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_get_flasher_CRC32( int fp, unsigned int *pu32FinalCrc, unsigned int u32ImageLength )
{
   int retval =  0;
   unsigned int u32Crc32 = 0;
   unsigned int u32EntryAdr = 0;

   if ((NULL == pu32FinalCrc) || (0 == fp) )
   {
      return -E_INVALIDARGS;
   }
   else
   {
      /* calculate CRC on Image size field first*/
      u32Crc32 = gnss_tool_teseo3_u32_crc32( u32Crc32, (const char *)&u32ImageLength, (int)sizeof(u32ImageLength) );
      /* calculate CRC on Flasher entry address field */
      u32Crc32 = gnss_tool_teseo3_u32_crc32( u32Crc32, (const char *)&u32EntryAdr, (int)sizeof(u32EntryAdr) );
      //rewind (fp);
      if( 0 > lseek(fp, 0, SEEK_SET))
      {
         printf("\nlseek failed");
         return -E_FILESEEK;
      }
      /* calculate crc for full binary */
      while(0 < (retval = (int)read(fp, teseo_data_holdbuffer, FIRMWARE_UPDATE_BUFF_SIZE)))
      {
         u32Crc32 = gnss_tool_teseo3_u32_crc32( u32Crc32, (const char *)teseo_data_holdbuffer, (int)retval );
      }
      if (0 == retval)
      {
         //printf("FLASHER CRC calc complete crc32- 0x%x", u32Crc32 );
         *pu32FinalCrc = u32Crc32;
      
         //rewind (fp);
         if( 0 > lseek(fp, 0, SEEK_SET))
         {
            printf("\nlseek failed");
            return -E_FILESEEK;
         }
         return retval;
      } 
      printf("Flasher read Failed");
      //if (0 != feof(fp))
      return -E_FILEREAD;
   }
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_send_uart_sync(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Sends UART sync message continuously(for 1000 retries) until the required response is received from Teseo. 
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_send_uart_sync(void)
{
   int retry;
   char ExpectedResponse[6] = 
   {
       GNSS_MSG_ID_RES_CONTROL, GNSS_MSG_RESPONSE_FLASH_DATA, 0x73, 0x40, 0x98, 0x83
   };
   char flasher_word[4] = 
   {
      0x40, 0x1A, 0x5D, 0x21
   };
   
   data_send_buf[0] = GNSS_INC_TP_MARKER_HEADER;
   data_send_buf[1] = msg_seq_counter++;
   data_send_buf[2] = GNSS_MSG_ID_CMD_CONTROL;
   data_send_buf[3] = GNSS_MSG_CMD_CONTROL_FLASH_DATA;
   
   memcpy(&data_send_buf[4], flasher_word, sizeof(flasher_word));
   
   for( retry = 0; retry < (int)TESEO3_SYNC_RETRIES; retry++)
   {
      if( (int)(4+sizeof(flasher_word)) != gnss_tool_send_msg_to_scc(data_send_buf, (4+sizeof(flasher_word))) )
      {
         printf("\nUART sync send error");
         return -E_SENDMSGTOSCC;
      }
      else if( 0 != gnss_tool_wait_for_SCC_OK() )
      {
         printf("\nUART Tx OK not received");
         return -E_WRONGRESPONSE;
      }
      else
      {
         if( 0 >= gnss_tool_wait_for_msg_from_scc( data_recv_buf, sizeof(data_recv_buf), 100) )
         {
             printf("\nWaiting for UART sync from Teseo..");
             //return -E_WAITFORSCCRESPONSE;
         }
         else if( 0 != gnss_tool_validate_msg(ExpectedResponse, data_recv_buf, sizeof(ExpectedResponse)) )
         {
            printf("\nUART sync not received");
            //return -E_WRONGRESPONSE;
         }
         else 
         {
            printf("\nUART sync successful");
            break;
         }
      }
      data_send_buf[1] = msg_seq_counter++;
   }
   if(retry == TESEO3_SYNC_RETRIES)
   {
      printf("\nUnable to send UART sync to Teseo");
      return -E_UARTSYNC;
   }
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_send_host_ready(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Sends Host ready to Teseo3
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_send_host_ready(void)
{
   return gnss_tool_send_byte_to_teseo_wait_for_ACK(GNSS_TESEO3_HOST_READY);
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_send_flasher_ready(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Sends Flasher ready to Teseo3
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_send_flasher_ready(void)
{
   return gnss_tool_send_byte_to_teseo_wait_for_ACK(GNSS_TESEO3_FLASHER_READY);
}

/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_send_flasher_ready_without_ACK(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Sends Flasher ready to Teseo3 but does not wait for Teseo ACK
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_send_flasher_ready_without_ACK(void)
{
   return gnss_tool_send_byte_to_teseo_without_ACK(GNSS_TESEO3_FLASHER_READY);
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_send_flasher_sync(void)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Sends Flasher Sync word to Teseo3
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_send_flasher_sync(void)
{
   data_send_buf[0] = GNSS_INC_TP_MARKER_HEADER;
   data_send_buf[1] = msg_seq_counter++;
   data_send_buf[2] = GNSS_MSG_ID_CMD_CONTROL;
   data_send_buf[3] = GNSS_MSG_CMD_CONTROL_FLASH_DATA;

   memcpy(&data_send_buf[4], &preamble, ((unsigned int)sizeof(Teseo3XloaderPreamble)));

   if( (int)(4+ sizeof(Teseo3XloaderPreamble)) != gnss_tool_send_msg_to_scc( data_send_buf, (int)(4+sizeof(Teseo3XloaderPreamble)) ) )
   {
      printf("\nFailed to send flasher sync options to SCC");
      return -E_SENDMSGTOSCC;
   }
   if( 0 != gnss_tool_wait_for_SCC_OK() )
   {
       printf("\nFailed to get FLASH_DATA OK from SCC for flasher sync");
       return -E_WAITFORSCCRESPONSE;
   }
   return 0;
}


/*********************************************************************************************************************
* FUNCTION     : static int gnss_tool_teseo3_send_binary_image_options(BinOpts_Mode mode)
* PARAMETER    : NONE
* RETURNVALUE  : 0  on successful initialization
*                         Non-zero Error value on failure
* DESCRIPTION  : Sends Binary image options to Teseo3
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 13.JULY.2016  | Initial version: 1.0  | 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static int gnss_tool_teseo3_send_binary_image_options(BinOpts_Mode mode)
{
   data_send_buf[0] = GNSS_INC_TP_MARKER_HEADER;
   data_send_buf[1] = msg_seq_counter++;
   data_send_buf[2] = GNSS_MSG_ID_CMD_CONTROL;
   data_send_buf[3] = GNSS_MSG_CMD_CONTROL_FLASH_DATA;

   memcpy(&data_send_buf[4], &image_options_teseo3[mode], (unsigned int)sizeof(Teseo3_BinaryImageOptions));

   if( (int)(4+ sizeof(Teseo3_BinaryImageOptions)) != gnss_tool_send_msg_to_scc( data_send_buf, (int)(4+sizeof(Teseo3_BinaryImageOptions)) ) )
   {
      printf("\nFailed to send image_options to SCC");
      return -E_SENDMSGTOSCC;
   }
   if( 0 != gnss_tool_wait_for_SCC_OK() )
   {
      printf("\nFailed to get FLASH_DATA OK from SCC for image_options");
      return -E_WAITFORSCCRESPONSE;
   }
   if( 0 != gnss_tool_wait_for_teseo_ACK() )
   {
      printf("\nFailed to get image_options ACK msg from SCC");
      return -E_TESEOACKRECV;
   }
   printf("\nBinary Options sent successfully");
   return 0;
}

/*End of file*/