#include "gmock/gmock.h"
#include "gtest/gtest.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if_mock.h"
#define GNSS_MOCKSTOBEREMOVED
#include "GnssMock.h"
#include "GnssMock.cpp"

#undef GnssProxy_s32GetEpochTime
#undef GnssProxy_s32SetEpoch

#include "../sources/dev_gnss_epoch.c"

using namespace testing;
using namespace std;

int main(int argc, char** argv)
{
   ::testing::InitGoogleMock(&argc, argv);
   return RUN_ALL_TESTS();
}

class GnssEpoch_InitGlobals : public ::testing::Test
{
public:
   GnssMock Gnss;
   NiceMock<OsalMock> Osal_mock;
   virtual void SetUp()
   {}
};

/*********************GnssProxy_s32GetEpoch*********************/

TEST_F(GnssEpoch_InitGlobals, GnssProxyGetEpoch)
{
   tU16 u16DummyMinWeek;
   tU16 u16DummyMaxWeek;
   tS32 s32RetVal;
   u32GnssEpochMinMaxWeek = 0x04150014;  // this value is dummy value where upper 2 bytes are for max week and lower 2 bytes are for minweek
                                          // maxweek > minweek+1024
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(21));// Length of = 
                                                                           //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                           //strlen(GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK)
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_COM_GET_EPOCH_RESPONSE_MASK)
                                                                     ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32GetEpoch(&u16DummyMinWeek, &u16DummyMaxWeek);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxyGetEpoch_SendDatatoSCCErr)
{
   tU16 u16DummyMinWeek;
   tU16 u16DummyMaxWeek;
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(99));
   s32RetVal = GnssProxy_s32GetEpoch(&u16DummyMinWeek, &u16DummyMaxWeek);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxyGetEpoch_EveWaitErr)
{
   tU16 u16DummyMinWeek;
   tU16 u16DummyMaxWeek;
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(21));// Length of = 
                                                                           //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                           //strlen(GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK)
   s32RetVal = GnssProxy_s32GetEpoch(&u16DummyMinWeek, &u16DummyMaxWeek);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxyGetEpoch_EvePostErr)
{
   tU16 u16DummyMinWeek;
   tU16 u16DummyMaxWeek;
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(21));// Length of = 
                                                                           //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                           //strlen(GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK)
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_COM_GET_EPOCH_RESPONSE_MASK)
                                                                     ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GnssProxy_s32GetEpoch(&u16DummyMinWeek, &u16DummyMaxWeek);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxyGetEpoch_ShutdownEvent)
{
   tU16 u16DummyMinWeek;
   tU16 u16DummyMaxWeek;
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvReleaseResource(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(21));// Length of = 
                                                                           //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                           //strlen(GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK)
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT)
                                                                     ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32GetEpoch(&u16DummyMinWeek, &u16DummyMaxWeek);
   EXPECT_EQ(OSAL_E_CANCELED, s32RetVal);
}

/*********************GnssProxy_s32CheckUpdatedEpoch*********************/

TEST_F(GnssEpoch_InitGlobals, GnssProxy_CheckUpdatedEpoch)
{
   tS32 s32RetVal;
   u32GnssEpochMinMaxWeek = 0x04150014;  // this value is dummy value where upper 2 bytes are for max week and lower 2 bytes are for minweek
                                          // maxweek > minweek+1024EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(21));// Length of = 
                                                                           //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                           //strlen(GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK)
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_COM_GET_EPOCH_RESPONSE_MASK)
                                                                     ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32CheckUpdatedEpoch(20, 1045);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_DifferentSetGetEpoch)
{
   tS32 s32RetVal;
   u32GnssEpochMinMaxWeek = 0x04150014;  // this value is dummy value where upper 2 bytes are for max week and lower 2 bytes are for minweek
                                          // maxweek > minweek+1024EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(21));// Length of = 
                                                                           //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                           //strlen(GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK)
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_COM_GET_EPOCH_RESPONSE_MASK)
                                                                     ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32CheckUpdatedEpoch(10, 1045);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/*********************GnssProxy_bCheckDataValidity*********************/

TEST_F(GnssEpoch_InitGlobals, GnssProxy_CheckDataValidity)
{
   tBool bDummy;
   OSAL_trGnssTimeUTC  rGPSTimeUTC = {1999,8,21,0,0,0,0};
   bDummy = GnssProxy_bCheckDataValidity( &rGPSTimeUTC );
   EXPECT_EQ(TRUE, bDummy);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_CheckDataValidity_YearErr)
{
   tBool bDummy;
   OSAL_trGnssTimeUTC  rGPSTimeUTC = {1979,8,21,0,0,0,0};
   bDummy = GnssProxy_bCheckDataValidity( &rGPSTimeUTC );
   EXPECT_EQ(FALSE, bDummy);
}

/*********************GnssProxy_bCheckLeapYear*********************/


TEST_F(GnssEpoch_InitGlobals, GnssProxy_CheckLeapYear_PosiCase)
{
   tBool bDummy;
   bDummy = GnssProxy_bCheckLeapYear(2016);
   EXPECT_EQ(TRUE, bDummy);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_CheckLeapYear_NegCase)
{
   tBool bDummy;
   bDummy = GnssProxy_bCheckLeapYear(2015);
   EXPECT_EQ(FALSE, bDummy);
}

/*********************GnssProxy_s32GetEpochTime*********************/

TEST_F(GnssEpoch_InitGlobals, GnssProxy_GetEpochTime)
{
   tS32 s32RetVal;
   OSAL_trGnssTimeUTC* rEpochSetTime;
   rEpochSetTime = (OSAL_trGnssTimeUTC*)malloc(sizeof(OSAL_trGnssTimeUTC));
   u32GnssEpochMinMaxWeek = 0x04150014;  // this value is dummy value where upper 2 bytes are for max week and lower 2 bytes are for minweek
                                          // maxweek > minweek+1024
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(21));// Length of = 
                                                                           //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                           //strlen(GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK)
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_COM_GET_EPOCH_RESPONSE_MASK)
                                                                     ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32GetEpochTime(rEpochSetTime);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(25, rEpochSetTime->u8Day);         //based on the value returned from utest
   EXPECT_EQ(5, rEpochSetTime->u8Month);        //based on the value returned from utest
   EXPECT_EQ(GNSS_PROXY_EPOCH_REFERENCE_YEAR, rEpochSetTime->u16Year);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_GetEpochTime_NullData)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   s32RetVal = GnssProxy_s32GetEpochTime(OSAL_NULL);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_GetEpochTime_GetEpochError)
{
   tS32 s32RetVal;
   OSAL_trGnssTimeUTC* rEpochSetTime;
   rEpochSetTime = (OSAL_trGnssTimeUTC*)malloc(sizeof(OSAL_trGnssTimeUTC));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GnssProxy_s32GetEpochTime(rEpochSetTime);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/*********************GnssProxy_s32CalculateNewUTCDate*********************/


TEST_F(GnssEpoch_InitGlobals, GnssProxy_CalculateNewUTCDate)
{
   tS32 s32RetVal;
   tS32 s32AddDays;
   OSAL_trGnssTimeUTC* rUTCDate;
   rUTCDate = (OSAL_trGnssTimeUTC*)malloc(sizeof(OSAL_trGnssTimeUTC));
   s32RetVal = GnssProxy_s32CalculateNewUTCDate(rUTCDate, s32AddDays);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_CalculateNewUTCDate_NullArg)
{
   tS32 s32RetVal;
   tS32 s32AddDays;
   OSAL_trGnssTimeUTC* rUTCDate = OSAL_NULL;
   s32RetVal = GnssProxy_s32CalculateNewUTCDate(rUTCDate, s32AddDays);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

/*********************GnssProxy_s32SetEpoch*********************/

TEST_F(GnssEpoch_InitGlobals, GnssProxy_SetEpoch)
{
   tS32 s32RetVal;
   OSAL_trGnssTimeUTC  rGPSTimeUTC = {2016,10,25,0,0,0,0};
   u32GnssEpochMinMaxWeek = 0x0B800780;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).WillOnce(Return(35)).WillOnce(Return(21));
   EXPECT_CALL(Gnss, GnssProxys32SaveConfigDataBlock()).Times(1).WillOnce(Return(OSAL_E_NOERROR));
   EXPECT_CALL(Gnss, GnssProxys32RebootTeseo()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_COM_SET_EPOCH_RESPONSE_MASK)
                                                                     ,Return(OSAL_OK)))
                                                  .WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_COM_GET_EPOCH_RESPONSE_MASK)
                                                                     ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   s32RetVal = GnssProxy_s32SetEpoch( &rGPSTimeUTC );
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_SetEpoch_InvalidDate)
{
   tS32 s32RetVal;
   OSAL_trGnssTimeUTC  rGPSTimeUTC = {1975,10,25,0,0,0,0};
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   s32RetVal = GnssProxy_s32SetEpoch( &rGPSTimeUTC );
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_SetEpoch_SendDataSccErr)
{
   tS32 s32RetVal;
   OSAL_trGnssTimeUTC  rGPSTimeUTC = {2016,10,25,0,0,0,0};
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   s32RetVal = GnssProxy_s32SetEpoch( &rGPSTimeUTC );
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_SetEpoch_SaveCfgBlkErr)
{
   tS32 s32RetVal;
   OSAL_trGnssTimeUTC  rGPSTimeUTC = {2016,10,25,0,0,0,0};
   EXPECT_CALL(Gnss, GnssProxys32SaveConfigDataBlock()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_COM_SET_EPOCH_RESPONSE_MASK)
                                                                     ,Return(OSAL_OK)));
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).WillOnce(Return(35));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   s32RetVal = GnssProxy_s32SetEpoch( &rGPSTimeUTC );
   EXPECT_EQ(35, s32RetVal); //value returned from GnssProxy_s32SendDataToScc
}

TEST_F(GnssEpoch_InitGlobals, GnssProxy_SetEpoch_ShutDownEve)
{
   tS32 s32RetVal;
   OSAL_trGnssTimeUTC  rGPSTimeUTC = {2016,10,25,0,0,0,0};
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT)
                                                                     ,Return(OSAL_OK)));
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).WillOnce(Return(35));
   EXPECT_CALL(Gnss, GnssProxyvReleaseResource(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   s32RetVal = GnssProxy_s32SetEpoch( &rGPSTimeUTC );
   EXPECT_EQ(OSAL_E_CANCELED, s32RetVal); //value returned from GnssProxy_s32SendDataToScc
}


/*********************GnssProxy_u16GetGpsWeekFromDate*********************/
TEST_F(GnssEpoch_InitGlobals, GnssProxy_GetGpsWeekFromDate)
{
   tS32 s32RetVal;
   OSAL_trGnssTimeUTC  rGPSTimeUTC = {2016,10,25,0,0,0,0};
   s32RetVal = GnssProxy_u16GetGpsWeekFromDate( &rGPSTimeUTC );
   EXPECT_EQ(1920, s32RetVal); //value returned from GnssProxy_s32SendDataToScc
}


