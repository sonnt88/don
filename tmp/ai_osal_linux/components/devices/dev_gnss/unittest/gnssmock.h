#ifndef _GNSSMOCK_H_
#define _GNSSMOCK_H_

#include "gmock/gmock.h"
#include "MockDelegatee.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if_mock.h"
#include "osioctrl_gnss.h"
#include "dev_gnss_types.h"
#include "dev_gnss_trace.h"
#include "inc2soc.h"

using namespace testing;

class GnssMock : public MockDelegatee<GnssMock>
{
   public:
      static inline const char *GetMockName() { return "GnssMock"; }
      static GnssMock &GetDelegatee()
      {
         return MockDelegatee<GnssMock>::GetDelegatee();
      }

      MOCK_METHOD0(GnssProxyFws32Init, tS32(tVoid));
      MOCK_METHOD0(GnssProxyvReadRegistryValues, tVoid(tVoid));
      MOCK_METHOD0(GnssProxys32GetTeseoFwVer,tS32(tVoid));
      MOCK_METHOD0(GnssProxys32GetTeseoCRC, tS32(tVoid));
      MOCK_METHOD0(GnssProxys32SetSatConfigReq, tS32(tVoid));
      MOCK_METHOD0(GnssProxys32GetCfgBlk200_227, tS32(tVoid));
      MOCK_METHOD0(GnssProxyFws32GetTeseoCRC, tS32(tVoid));
      MOCK_METHOD0(GnssProxys32InitCommunToSCC, tS32(tVoid));
      MOCK_METHOD0(GnssProxys32HandleStatusMsg, tS32(tVoid));
      MOCK_METHOD0(GnssProxys32HandleConfigMsg, tS32(tVoid));
      MOCK_METHOD0(GnssProxyvHandleCommandReject, tVoid(tVoid));
      MOCK_METHOD0(GnssProxyvHandleGnssData, tVoid(tVoid));
      MOCK_METHOD0(GnssProxys32FlushSccBuff, tS32(tVoid));
      MOCK_METHOD0(GnssProxyFws32DeInit, tS32(tVoid));
      MOCK_METHOD1(GnssProxyFws32SetChipType, tS32(tEnGnssChipType));
      MOCK_METHOD1(GnssProxyvWriteLastUsedSatSys, tVoid(tU32));
      MOCK_METHOD1(GnssProxys32SetEpoch, tS32(const OSAL_trGnssTimeUTC*));
      MOCK_METHOD1(GnssProxys32SetSatSys, tS32(tPU32));
      MOCK_METHOD1(GnssProxys32GetDataFromScc, tS32(tPU32));
      MOCK_METHOD1(GnssProxys32GetNmeaRecvdList, tS32(tPU32));
      MOCK_METHOD1(GnssProxyFws32ConfigBinOpts, tS32(const trImageOptions*));
      MOCK_METHOD1(GnssProxys32GetDataFromScc, tS32(tU32));
      MOCK_METHOD1(GnssProxys32SocketSetup, tS32(tU32));
      MOCK_METHOD1(GnssProxys32GetEpochTime, tS32(OSAL_trGnssTimeUTC*));
      MOCK_METHOD1(GnssProxyvReleaseResource, tVoid(tU32 u32Resource));
      MOCK_METHOD1(s32Inc2SocCreateResources, tS32( tEnInc2SocDev enDeviceType ));
      MOCK_METHOD1(vInc2SocPostDrivShutdown, tVoid(tEnInc2SocDev enDeviceType));
      MOCK_METHOD1(vInc2SocWriteDataToBuffer, tVoid(tEnInc2SocDev enDeviceType));
      MOCK_METHOD2(GnssProxyFws32SendBinToTeseo, tS32(const char*, tU32));
      MOCK_METHOD2(GnssProxys32SendStatusCmd, tS32(tU8, tU8));
      MOCK_METHOD1(GnssProxys32SendDataToScc, tS32(tU32));
      MOCK_METHOD0(GnssProxys32SaveConfigDataBlock, tS32(tVoid));
      MOCK_METHOD0(GnssProxys32RebootTeseo, tS32(tVoid));
      MOCK_METHOD1(GnssProxyvEnterCriticalSection, tVoid(tU32));
      MOCK_METHOD1(GnssProxyvLeaveCriticalSection, tVoid(tU32));
      MOCK_METHOD1(GnssProxyvSyncAuxClock, tVoid(tU32));
      MOCK_METHOD2(GnssProxys32WaitForMsg, tS32(tU32,tU32));
      MOCK_METHOD3(GnssProxyvTraceOut, tVoid(TR_tenTraceLevel,enGNSSPXYTRC,const tChar*));
      #ifdef GNSS_MOCKSTOBEREMOVED
      MOCK_METHOD2(LLD_bIsTraceActive, tBool(tU32, tU32));
      MOCK_METHOD2(OSAL_s32ThreadJoin, tS32(OSAL_tThreadID, OSAL_tMSecond));
      MOCK_METHOD3(dgram_init, sk_dgram* (int, int, void*));
      MOCK_METHOD3(dgram_send, int (sk_dgram*, void*, size_t));
      MOCK_METHOD3(dgram_recv, int (sk_dgram*, void*, size_t));
      MOCK_METHOD4(LLD_vTrace, tVoid(tU32, tU32, const void*, tU32));
      MOCK_METHOD3(poll, int(struct pollfd *fds, nfds_t nfds, int timeout));
      MOCK_METHOD1(close_mock, int(int));
      MOCK_METHOD2(open_mock, int(const char*, int));
      MOCK_METHOD3(write_mock, ssize_t(int, const void*, size_t));
      MOCK_METHOD3(socket_mock, int(int , int , int));
      MOCK_METHOD3(bind_mock, int(int , const struct sockaddr*, socklen_t));
      MOCK_METHOD3(connect_mock, int(int , const struct sockaddr*, socklen_t));
      #endif
};


#define  GnssProxyFw_s32Init                    mocked_GnssProxyFw_s32Init
#define  GnssProxy_vReadRegistryValues          mocked_GnssProxy_vReadRegistryValues
#define  GnssProxy_s32GetTeseoFwVer             mocked_GnssProxy_s32GetTeseoFwVer
#define  GnssProxy_s32GetTeseoCRC               mocked_GnssProxy_s32GetTeseoCRC
#define  GnssProxy_s32SetSatConfigReq           mocked_GnssProxy_s32SetSatConfigReq
#define  GnssProxy_s32GetCfgBlk200_227          mocked_GnssProxy_s32GetCfgBlk200_227
#define  GnssProxy_s32SocketSetup               mocked_GnssProxy_s32SocketSetup
#define  GnssProxy_s32InitCommunToSCC           mocked_GnssProxy_s32InitCommunToSCC
#define  GnssProxy_s32GetDataFromScc            mocked_GnssProxy_s32GetDataFromScc
#define  GnssProxy_s32HandleStatusMsg           mocked_GnssProxy_s32HandleStatusMsg
#define  GnssProxy_s32HandleConfigMsg           mocked_GnssProxy_s32HandleConfigMsg
#define  GnssProxy_vHandleCommandReject         mocked_GnssProxy_vHandleCommandReject
#define  GnssProxy_vHandleGnssData              mocked_GnssProxy_vHandleGnssData
#define  GnssProxy_s32SetSatSys                 mocked_GnssProxy_s32SetSatSys
#define  GnssProxy_s32GetNmeaRecvdList          mocked_GnssProxy_s32GetNmeaRecvdList
#define  GnssProxyFw_s32ConfigBinOpts           mocked_GnssProxyFw_s32ConfigBinOpts
#define  GnssProxy_s32FlushSccBuff              mocked_GnssProxy_s32FlushSccBuff
#define  GnssProxyFw_s32SetChipType             mocked_GnssProxyFw_s32SetChipType
#define  GnssProxyFw_s32SendBinToTeseo          mocked_GnssProxyFw_s32SendBinToTeseo
#define  GnssProxyFw_s32DeInit                  mocked_GnssProxyFw_s32DeInit
#define  GnssProxy_s32SendStatusCmd             mocked_GnssProxy_s32SendStatusCmd
#define  GnssProxy_s32SetEpoch                  mocked_GnssProxy_s32SetEpoch
#define  GnssProxy_vWriteLastUsedSatSys         mocked_GnssProxy_vWriteLastUsedSatSys
#define  GnssProxy_vTraceOut                    mocked_GnssProxy_vTraceOut
#define  GnssProxyFw_s32GetTeseoCRC             mocked_GnssProxyFw_s32GetTeseoCRC
#define  GnssProxy_s32GetEpochTime              mocked_GnssProxy_s32GetEpochTime
#define  GnssProxy_vReleaseResource             mocked_GnssProxy_vReleaseResource
#define  Inc2Soc_s32CreateResources             mocked_Inc2Soc_s32CreateResources
#define  Inc2Soc_vWriteDataToBuffer             mocked_Inc2Soc_vWriteDataToBuffer
#define  Inc2Soc_vPostDrivShutdown              mocked_Inc2Soc_vPostDrivShutdown
#define  GnssProxy_s32SendDataToScc             mocked_GnssProxy_s32SendDataToScc
#define  GnssProxy_s32SaveConfigDataBlock       mocked_GnssProxy_s32SaveConfigDataBlock
#define  GnssProxy_s32RebootTeseo               mocked_GnssProxy_s32RebootTeseo
#define  GnssProxy_vEnterCriticalSection        mocked_GnssProxy_vEnterCriticalSection
#define  GnssProxy_vLeaveCriticalSection        mocked_GnssProxy_vLeaveCriticalSection
#define  GnssProxy_vSyncAuxClock                mocked_GnssProxy_vSyncAuxClock
#define  GnssProxy_s32WaitForMsg                mocked_GnssProxy_s32WaitForMsg
#ifdef GNSS_MOCKSTOBEREMOVED
#define  close                                  mocked_close
#define  open                                   mocked_open
#define  write                                  mocked_write
#define  socket                                 mocked_socket
#define  bind                                   mocked_bind
#define  connect                                mocked_connect
#endif


tS32     mocked_GnssProxyFw_s32Init(tVoid);
tVoid    mocked_GnssProxy_vReadRegistryValues(tVoid);
tS32     mocked_GnssProxy_s32GetTeseoFwVer(tVoid);
tS32     mocked_GnssProxy_s32GetTeseoCRC(tVoid);
tS32     mocked_GnssProxy_s32SetSatConfigReq(tVoid);
tS32     mocked_GnssProxy_s32GetCfgBlk200_227(tVoid);
tS32     mocked_GnssProxy_s32SocketSetup(tU32 u32IncPort);
tS32     mocked_GnssProxy_s32InitCommunToSCC(tVoid);
tS32     mocked_GnssProxy_s32GetDataFromScc(tU32 u32Bytes);
tS32     mocked_GnssProxy_s32HandleStatusMsg(tVoid);
tS32     mocked_GnssProxy_s32HandleConfigMsg(tVoid);
tVoid    mocked_GnssProxy_vHandleCommandReject(tVoid);
tVoid    mocked_GnssProxy_vHandleGnssData(tVoid);
tS32     mocked_GnssProxy_s32SetSatSys(tPU32 pu32SatSys);
tS32     mocked_GnssProxy_s32GetNmeaRecvdList(tPU32 pu32NmeaList);
tS32     mocked_GnssProxyFw_s32ConfigBinOpts(const trImageOptions *prImageOptions);
tS32     mocked_GnssProxy_s32FlushSccBuff(tVoid);
tVoid    mocked_GnssProxy_vWriteLastUsedSatSys( tU32 u32actGnssSatSys );
tS32     mocked_GnssProxyFw_s32SetChipType(tEnGnssChipType enGnssChipType);
tS32     mocked_GnssProxyFw_s32SendBinToTeseo(const char* pBuffer, tU32 u32maxbytes);
tS32     mocked_GnssProxyFw_s32DeInit(tVoid);
tS32     mocked_GnssProxy_s32SendStatusCmd(tU8 u8Status, tU8 u8CompVer);
tS32     mocked_GnssProxy_s32SetEpoch( const OSAL_trGnssTimeUTC *rEpochToSet );
tVoid    mocked_GnssProxy_vTraceOut(TR_tenTraceLevel enLevel,enGNSSPXYTRC enGnssPxyTrcVal,const tChar *pcFormatString,...);
tS32     mocked_GnssProxyFw_s32GetTeseoCRC();
tS32     mocked_GnssProxy_s32GetEpochTime( OSAL_trGnssTimeUTC* rEpochSetTime );
tVoid    mocked_GnssProxy_vReleaseResource(tU32 u32Resource );
tS32     mocked_Inc2Soc_s32CreateResources( tEnInc2SocDev enDeviceType );
tVoid    mocked_Inc2Soc_vWriteDataToBuffer( tEnInc2SocDev enDeviceType );
tVoid    mocked_Inc2Soc_vPostDrivShutdown( tEnInc2SocDev enDeviceType );
tS32     mocked_GnssProxy_s32SendDataToScc(tU32 u32Bytes);
tS32     mocked_GnssProxy_s32SaveConfigDataBlock(tVoid);
tS32     mocked_GnssProxy_s32RebootTeseo(tVoid);
tVoid    mocked_GnssProxy_vEnterCriticalSection( tU32 u32LineNum );
tVoid    mocked_GnssProxy_vLeaveCriticalSection( tU32 u32LineNum );
tVoid    mocked_GnssProxy_vSyncAuxClock( tU32 u32V850TimStmp );
tS32     mocked_GnssProxy_s32WaitForMsg( tU32 u32EventMask, tU32 u32WaitTime );
#ifdef GNSS_MOCKSTOBEREMOVED
int mocked_close(int fd);
int mocked_open(const char *pathname, int flags);
ssize_t mocked_write(int, const void*, size_t);
int mocked_socket(int domain, int type, int protocol);
int mocked_bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int mocked_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
#endif

#endif
