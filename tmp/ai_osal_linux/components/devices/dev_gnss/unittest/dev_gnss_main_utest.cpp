#include "gmock/gmock.h"
#include "gtest/gtest.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if_mock.h"
#define GNSS_MOCKSTOBEREMOVED
#include "GnssMock.h"
#include "GnssMock.cpp"

#undef GnssProxy_vLeaveCriticalSection
#undef GnssProxy_vEnterCriticalSection
#undef GnssProxy_vReleaseResource

#include "../sources/dev_gnss_main.c"

using namespace testing;
using namespace std;

int main(int argc, char** argv)
{
   ::testing::InitGoogleMock(&argc, argv);
   return RUN_ALL_TESTS();
}

class Gnss_InitGlobals : public ::testing::Test
{
public:
   GnssMock Gnss;
   NiceMock<OsalMock> Osal_mock;
   virtual void SetUp()
   {
      OSAL_tSemHandle hGnssDummySem;
      OSAL_tEventHandle hDummyTeseoHandle;
      OSAL_tEventHandle hDummyReadHandle;
      OSAL_tEventHandle hDummyInitHandle;
      rGnssProxyInfo.hGnssProxyTeseoComEvent = hDummyTeseoHandle;
      rGnssProxyInfo.hGnssProxyReadEvent = hDummyReadHandle;
      rGnssProxyInfo.hGnssProxySemaphore = hGnssDummySem;
      rGnssProxyInfo.hGnssProxyInitEvent = hDummyInitHandle;
      rGnssProxyInfo.enAccessMode = OSAL_EN_READONLY;
      rGnssProxyInfo.s32SocketFD = 100;
      rGnssProxyInfo.enGnssProxyState = DEVICE_NOT_INITIALIZED;
      rGnssProxyInfo.bShutdownFlag = FALSE;
   }
};

class Gnss_Opened : public Gnss_InitGlobals
{
public:
   virtual void SetUp()
   {
      EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
      EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll((SetArgPointee<4>(GNSS_PROXY_EVENT_INIT_SUCCESS)),Return(OSAL_OK)));
      EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
      EXPECT_CALL(Gnss, GnssProxyvReadRegistryValues()).Times(1);
      EXPECT_CALL(Gnss, GnssProxys32GetTeseoFwVer()).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(Gnss, GnssProxys32GetTeseoCRC()).Times(1);
      EXPECT_CALL(Gnss, GnssProxys32SetSatConfigReq()).Times(1).WillOnce(Return(OSAL_ERROR));
      EXPECT_CALL(Gnss, GnssProxys32GetCfgBlk200_227()).Times(1).WillOnce(Return(OSAL_E_NOERROR));
      EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(7);
      GnssProxy_s32Init();
      Gnss_InitGlobals::SetUp();
   }
};


/**********************GNSS Init**********************/

TEST_F(Gnss_InitGlobals, GnssProxy_Init)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll((SetArgPointee<4>(GNSS_PROXY_EVENT_INIT_SUCCESS)),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvReadRegistryValues()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoFwVer()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoCRC()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SetSatConfigReq()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32GetCfgBlk200_227()).Times(1).WillOnce(Return(OSAL_E_NOERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(7);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_OK, retval);
   EXPECT_EQ(100, tid_sockRd);
   EXPECT_EQ(DEVICE_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}

TEST_F(Gnss_InitGlobals, GnssProxy_InitTesFwVerErr)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll((SetArgPointee<4>(GNSS_PROXY_EVENT_INIT_SUCCESS)),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvReadRegistryValues()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoFwVer()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoCRC()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SetSatConfigReq()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32GetCfgBlk200_227()).Times(1).WillOnce(Return(OSAL_E_NOERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(8);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_OK, retval);
   EXPECT_EQ(100, tid_sockRd);
   EXPECT_EQ(DEVICE_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}

TEST_F(Gnss_InitGlobals, GnssProxy_InitGetCfgBlkErr)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll((SetArgPointee<4>(GNSS_PROXY_EVENT_INIT_SUCCESS)),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvReadRegistryValues()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoFwVer()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoCRC()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SetSatConfigReq()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32GetCfgBlk200_227()).WillOnce(Return(OSAL_ERROR)).WillOnce(Return(OSAL_E_NOERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(8);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_OK, retval);
   EXPECT_EQ(100, tid_sockRd);
   EXPECT_EQ(DEVICE_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}

TEST_F(Gnss_InitGlobals, GnssProxy_Init_GetCfgBlkErr)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll((SetArgPointee<4>(GNSS_PROXY_EVENT_INIT_SUCCESS)),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvReadRegistryValues()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoFwVer()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoCRC()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SetSatConfigReq()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32GetCfgBlk200_227()).WillOnce(Return(OSAL_ERROR)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(9);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_OK, retval);
   EXPECT_EQ(100, tid_sockRd);
   EXPECT_EQ(DEVICE_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}

TEST_F(Gnss_InitGlobals, GnssProxy_Init_ThdSpwnErr)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_ERROR, retval);
   EXPECT_EQ(OSAL_ERROR, tid_sockRd);
   EXPECT_EQ(DEVICE_NOT_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}

TEST_F(Gnss_InitGlobals, GnssProxy_Init_ThdEveWaitErr)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).WillOnce(Return(OSAL_ERROR)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_ERROR, retval);
   EXPECT_EQ(100, tid_sockRd);
   EXPECT_EQ(DEVICE_NOT_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}


TEST_F(Gnss_InitGlobals, GnssProxy_Init_ThdEveWaitErr_Timeout)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).Times(1).WillOnce(Return(OSAL_E_TIMEOUT));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_ERROR, retval);
   EXPECT_EQ(100, tid_sockRd);
   EXPECT_EQ(DEVICE_NOT_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}

TEST_F(Gnss_InitGlobals, GnssProxy_Init_Failed)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll((SetArgPointee<4>(GNSS_PROXY_EVENT_INIT_FAILED)),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_ERROR, retval);
   EXPECT_EQ(100, tid_sockRd);
   EXPECT_EQ(DEVICE_NOT_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}

TEST_F(Gnss_InitGlobals, GnssProxy_Init_FailedEvePostFail)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll((SetArgPointee<4>(GNSS_PROXY_EVENT_INIT_FAILED)),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).Times(1).WillOnce(Return(OSAL_E_TIMEOUT));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_ERROR, retval);
   EXPECT_EQ(100, tid_sockRd);
   EXPECT_EQ(DEVICE_NOT_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}

TEST_F(Gnss_InitGlobals, GnssProxy_Init_EvePostFail)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll((SetArgPointee<4>(GNSS_PROXY_EVENT_INIT_SUCCESS)),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).Times(1).WillOnce(Return(OSAL_E_TIMEOUT));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32Init();
   EXPECT_EQ(OSAL_ERROR, retval);
   EXPECT_EQ(100, tid_sockRd);
   EXPECT_EQ(DEVICE_NOT_INITIALIZED, rGnssProxyInfo.enGnssProxyState);
}



/**********************GNSS IOOpen**********************/

TEST_F(Gnss_InitGlobals, GnssProxy_OpenReadOnly)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll((SetArgPointee<4>(GNSS_PROXY_EVENT_INIT_SUCCESS)),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvReadRegistryValues()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoFwVer()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoCRC()).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SetSatConfigReq()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32GetCfgBlk200_227()).Times(1).WillOnce(Return(OSAL_E_NOERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(9);
   retval = GnssProxy_s32IOOpen(OSAL_EN_READONLY);
   EXPECT_EQ(OSAL_E_NOERROR, retval);
}

TEST_F(Gnss_InitGlobals, GnssProxy_OpenReadOnly_Opened)
{
   tS32 retval;
   rGnssProxyInfo.enGnssProxyState = DEVICE_INITIALIZED;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOOpen(OSAL_EN_READONLY);
   EXPECT_EQ(OSAL_E_ALREADYOPENED, retval);
}


TEST_F(Gnss_InitGlobals, GnssProxy_OpenReadOnly_Err)
{
   tS32 retval;
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendStatusCmd(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, vInc2SocPostDrivShutdown(_)).Times(1);
   EXPECT_CALL(Gnss, OSAL_s32ThreadJoin(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(10);
   retval = GnssProxy_s32IOOpen(OSAL_EN_READONLY);
   EXPECT_EQ(OSAL_ERROR, retval);
   EXPECT_EQ(DEVICE_DEINITIALIZED,rGnssProxyInfo.enGnssProxyState);
   EXPECT_EQ(TRUE, rGnssProxyInfo.bShutdownFlag);
   EXPECT_EQ(OSAL_NULL, rGnssProxyInfo.s32SocketFD);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxySemaphore);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyReadEvent);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyTeseoComEvent);
}


TEST_F(Gnss_InitGlobals, GnssProxy_OpenWriteOnly)
{
   tS32 retval;
   EXPECT_CALL(Gnss, GnssProxyFws32Init()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   retval = GnssProxy_s32IOOpen(OSAL_EN_WRITEONLY);
   EXPECT_EQ(OSAL_E_NOERROR, retval);
}

TEST_F(Gnss_InitGlobals, GnssProxy_OpenWriteOnly_Err)
{
   tS32 retval;
   EXPECT_CALL(Gnss, GnssProxyFws32Init()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOOpen(OSAL_EN_WRITEONLY);
   EXPECT_EQ(OSAL_E_UNKNOWN, retval);
}

TEST_F(Gnss_InitGlobals, GnssProxy_OpenUnknownAccess)
{
   tS32 retval;
   retval = GnssProxy_s32IOOpen(OSAL_EN_READWRITE);
   EXPECT_EQ(OSAL_E_NOACCESS, retval);
}


/**********************GNSS CreateResources**********************/

TEST_F(Gnss_InitGlobals, GnssProxy_CreateRes)
{
   tS32 retval;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(2).WillRepeatedly(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreCreate(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32InitCommunToSCC()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32SocketSetup(_)).Times(1).WillOnce(Return(OSAL_OK));
   retval = GnssProxy_s32CreateResources();
   EXPECT_EQ(OSAL_OK, retval);
}

TEST_F(Gnss_InitGlobals, GnssProxy_CreateRes_SemaphoreError)
{
   tS32 retval;
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32SemaphoreCreate(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32InitCommunToSCC()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32SocketSetup(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   retval = GnssProxy_s32CreateResources();
   EXPECT_EQ(OSAL_ERROR, retval);
}

TEST_F(Gnss_InitGlobals, GnssProxy_CreateRes_SocError)
{
   tS32 retval;
   EXPECT_CALL(Gnss, GnssProxys32SocketSetup(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   retval = GnssProxy_s32CreateResources();
   EXPECT_EQ(OSAL_ERROR, retval);
}

TEST_F(Gnss_InitGlobals, GnssProxy_CreateRes_InitCommError)
{
   tS32 retval;
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32InitCommunToSCC()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32SocketSetup(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   retval = GnssProxy_s32CreateResources();
   EXPECT_EQ(OSAL_ERROR, retval);
}

TEST_F(Gnss_InitGlobals, GnssProxy_CreateRes_ReadEventErr)
{
   tS32 retval;
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32InitCommunToSCC()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32SocketSetup(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   retval = GnssProxy_s32CreateResources();
   EXPECT_EQ(OSAL_ERROR, retval);
}

TEST_F(Gnss_InitGlobals, GnssProxy_CreateRes_TeseoEventErr)
{
   tS32 retval;
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32InitCommunToSCC()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32SocketSetup(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(5);
   retval = GnssProxy_s32CreateResources();
   EXPECT_EQ(OSAL_ERROR, retval);
}


/**********************GnssProxy_vEnterCriticalSection**********************/

TEST_F(Gnss_InitGlobals, GnssProxy_EnterCricSec)
{
   tU32 u32DummyLineNum = 1;
   EXPECT_CALL(Osal_mock, s32SemaphoreWait(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   GnssProxy_vEnterCriticalSection(u32DummyLineNum);
}

TEST_F(Gnss_InitGlobals, GnssProxy_EnterCricSec_SemWaitErr)
{
   tU32 u32DummyLineNum = 1;
   EXPECT_CALL(Osal_mock, s32SemaphoreWait(_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vEnterCriticalSection(u32DummyLineNum);
}


TEST_F(Gnss_InitGlobals, GnssProxy_EnterCricSec_Err)
{
   tU32 u32DummyLineNum = 1;
   rGnssProxyInfo.hGnssProxySemaphore = OSAL_C_INVALID_HANDLE;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vEnterCriticalSection(u32DummyLineNum);
}


/**********************GnssProxy_vLeaveCriticalSection**********************/

TEST_F(Gnss_InitGlobals, GnssProxy_LeaveCricSec)
{
   tU32 u32DummyLineNum = 1;
   EXPECT_CALL(Osal_mock, s32SemaphorePost(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   GnssProxy_vLeaveCriticalSection(u32DummyLineNum);
}


TEST_F(Gnss_InitGlobals, GnssProxy_LeaveCricSec_SemPostErr)
{
   tU32 u32DummyLineNum = 1;
   EXPECT_CALL(Osal_mock, s32SemaphorePost(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vLeaveCriticalSection(u32DummyLineNum);
}


TEST_F(Gnss_InitGlobals, GnssProxy_LeaveCricSec_Err)
{
   tU32 u32DummyLineNum = 1;
   rGnssProxyInfo.hGnssProxySemaphore = OSAL_C_INVALID_HANDLE;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vLeaveCriticalSection(u32DummyLineNum);
}


/**********************Gnss IOREAD**********************/

TEST_F(Gnss_InitGlobals, GnssProxy_Read_DeviceDeInitialized)
{
   tS32 retval;
   OSAL_trGnssFullRecord *pDummyBuffer={0};
   tU32 u32maxbytes = 100;
   rGnssProxyInfo.enGnssProxyState = DEVICE_DEINITIALIZED;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   retval = GnssProxy_s32IORead((tPS8)pDummyBuffer,u32maxbytes);
   EXPECT_EQ(OSAL_E_TEMP_NOT_AVAILABLE,retval);
}

TEST_F(Gnss_Opened, GnssProxy_Read_NullData)
{
   tS32 retval;
   OSAL_trGnssFullRecord *pDummyBuffer={0};
   tU32 u32maxbytes = 100;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   retval = GnssProxy_s32IORead((tPS8)pDummyBuffer,u32maxbytes);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,retval);
}

TEST_F(Gnss_Opened, GnssProxy_Read_Notenoughspace)
{
   tS32 retval;
   tU8 i;
   OSAL_trGnssFullRecord *pDummyBuffer;
   OSAL_trGnssPVTData rDummyPVTData={0};
   tU32 u32maxbytes = 100;
   pDummyBuffer = (OSAL_trGnssFullRecord *)malloc(sizeof(OSAL_trGnssFullRecord));
   pDummyBuffer->u32TimeStamp = 115200;
   pDummyBuffer->rPVTData = rDummyPVTData;
   for (int i =0; i<OSAL_C_U8_GNSS_NO_CHANNELS; i++)
   {
      pDummyBuffer->rChannelStatus[i] = {0};
   }
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   retval = GnssProxy_s32IORead((tPS8)pDummyBuffer,u32maxbytes);
   EXPECT_EQ(OSAL_E_NOSPACE,retval);
}

TEST_F(Gnss_Opened, GnssProxy_ReadData_ValidRecord)
{
   tS32 retval;
   tU8 i;
   OSAL_trGnssFullRecord *pDummyBuffer;
   OSAL_trGnssPVTData rDummyPVTData={0};
   pDummyBuffer = (OSAL_trGnssFullRecord *)malloc(sizeof(OSAL_trGnssFullRecord));
   pDummyBuffer->u32TimeStamp = 115200;
   pDummyBuffer->rPVTData = rDummyPVTData;
   for(i=0; i < OSAL_C_U8_GNSS_NO_CHANNELS; i++)
   {
      pDummyBuffer->rChannelStatus[i]={0};
   }
   tU32 u32maxbytes = 1000;
   rGnssProxyData.bIsRecordValid = TRUE;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   retval = GnssProxy_s32IORead((tPS8)pDummyBuffer,u32maxbytes);
   EXPECT_EQ(FALSE, rGnssProxyData.bIsRecordValid);
}

TEST_F(Gnss_Opened, GnssProxy_ReadData_WaitForRec_TimeOut)
{
   tS32 retval;
   tU8 i;
   OSAL_trGnssFullRecord *pDummyBuffer;
   OSAL_trGnssPVTData rDummyPVTData={0};
   pDummyBuffer = (OSAL_trGnssFullRecord *)malloc(sizeof(OSAL_trGnssFullRecord));
   pDummyBuffer->u32TimeStamp = 115200;
   pDummyBuffer->rPVTData = rDummyPVTData;
   for(i=0; i < OSAL_C_U8_GNSS_NO_CHANNELS; i++)
   {
      pDummyBuffer->rChannelStatus[i]={0};
   }
   tU32 u32maxbytes = 1000;
   rGnssProxyData.bIsRecordValid = FALSE;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(5);
   EXPECT_CALL(Osal_mock, s32SemaphorePost(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreWait(_,_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_EVENT_DATA_READY),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).WillOnce(Return(OSAL_OK));
   retval = GnssProxy_s32IORead((tPS8)pDummyBuffer,u32maxbytes);
   EXPECT_EQ(OSAL_E_TIMEOUT,retval);
   EXPECT_EQ(FALSE, rGnssProxyData.bIsRecordValid);
}

/**********************Gnss IOWrite**********************/


TEST_F(Gnss_Opened, Gnss_Write_NullData)
{
   char *pBuffer;
   tU32 u32maxbytes;
   tS32 s32RetVal;
   pBuffer = {0};
   u32maxbytes = 100;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal = GnssProxy_s32IOWrite( pBuffer, u32maxbytes);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
   EXPECT_EQ(GNSS_FW_UPDATE_FAILED, enGnssFwUpdateStatus);
}

TEST_F(Gnss_Opened, Gnss_Write_InvalidSize)
{
   char *pBuffer;
   tU32 u32maxbytes;
   tS32 s32RetVal;
   char dummydata = '\0';
   pBuffer = &dummydata;
   u32maxbytes = 0;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal = GnssProxy_s32IOWrite( pBuffer, u32maxbytes);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
   EXPECT_EQ(GNSS_FW_UPDATE_FAILED, enGnssFwUpdateStatus);
}

TEST_F(Gnss_Opened, Gnss_Write_SendBintoTesFail)
{
   char *pBuffer;
   tU32 u32maxbytes;
   tS32 s32RetVal;
   char dummydata = '\0';
   pBuffer = &dummydata;
   u32maxbytes = 100;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, GnssProxyFws32SendBinToTeseo(_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GnssProxy_s32IOWrite( pBuffer, u32maxbytes);
   EXPECT_EQ(OSAL_E_UNKNOWN, s32RetVal);
   EXPECT_EQ(GNSS_FW_UPDATE_FAILED, enGnssFwUpdateStatus);
}

TEST_F(Gnss_Opened, Gnss_Write)
{
   char *pBuffer;
   tU32 u32maxbytes;
   tS32 s32RetVal;
   char dummydata = '\0';
   pBuffer = &dummydata;
   u32maxbytes = 100;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyFws32SendBinToTeseo(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32IOWrite( pBuffer, u32maxbytes);
   EXPECT_EQ(100, s32RetVal);
}


/**********************Gnss IOClose**********************/


TEST_F(Gnss_Opened, Gnss_Close_DefCase)
{
   tS32 s32RetVal;
   rGnssProxyInfo.enAccessMode = (OSAL_tenAccess)999;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   s32RetVal = GnssProxy_s32IOClose();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(Gnss_Opened, Gnss_Close_ReadOnlyCase)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(8);
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendStatusCmd(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, vInc2SocPostDrivShutdown(_)).Times(1);
   EXPECT_CALL(Gnss, OSAL_s32ThreadJoin(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32IOClose();
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(DEVICE_CLOSED_GNSS,rGnssProxyInfo.enGnssProxyState);
   EXPECT_EQ(TRUE, rGnssProxyInfo.bShutdownFlag);
   EXPECT_EQ(OSAL_NULL, rGnssProxyInfo.s32SocketFD);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxySemaphore);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyReadEvent);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyTeseoComEvent);
}

TEST_F(Gnss_Opened, Gnss_Close_WriteOnlyCase)
{
   tS32 s32RetVal;
   enGnssFwUpdateStatus = GNSS_FW_UPDATE_SUCCESSFULL;
   rGnssProxyInfo.enAccessMode = OSAL_EN_WRITEONLY;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyFws32DeInit()).Times(1);
   s32RetVal = GnssProxy_s32IOClose();
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(GNSS_FW_UPDATE_NOT_ACTIVE,enGnssFwUpdateStatus);
}

TEST_F(Gnss_Opened, Gnss_Close_WriteOnlyCase_UpdateStartOrFail)
{
   tS32 s32RetVal;
   enGnssFwUpdateStatus = GNSS_FW_UPDATE_STARTED;
   rGnssProxyInfo.enAccessMode = OSAL_EN_WRITEONLY;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, GnssProxyFws32DeInit()).Times(1);
   s32RetVal = GnssProxy_s32IOClose();
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(GNSS_FW_UPDATE_NOT_ACTIVE,enGnssFwUpdateStatus);
}



/**********************GNSS DeInit**********************/

TEST_F(Gnss_Opened, GnssProxy_DeInit)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(7);
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendStatusCmd(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, vInc2SocPostDrivShutdown(_)).Times(1);
   EXPECT_CALL(Gnss, OSAL_s32ThreadJoin(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32DeInit();
   EXPECT_EQ(DEVICE_DEINITIALIZED,rGnssProxyInfo.enGnssProxyState);
   EXPECT_EQ(TRUE, rGnssProxyInfo.bShutdownFlag);
   EXPECT_EQ(OSAL_NULL, rGnssProxyInfo.s32SocketFD);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxySemaphore);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyReadEvent);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyTeseoComEvent);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Gnss_Opened, GnssProxy_DeInit_SendStatusErr)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(8);
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendStatusCmd(_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, vInc2SocPostDrivShutdown(_)).Times(1);
   EXPECT_CALL(Gnss, OSAL_s32ThreadJoin(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32DeInit();
   EXPECT_EQ(DEVICE_DEINITIALIZED,rGnssProxyInfo.enGnssProxyState);
   EXPECT_EQ(TRUE, rGnssProxyInfo.bShutdownFlag);
   EXPECT_EQ(OSAL_NULL, rGnssProxyInfo.s32SocketFD);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxySemaphore);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyReadEvent);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyTeseoComEvent);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Gnss_Opened, GnssProxy_DeInit_ThdJoinErr)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(8);
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendStatusCmd(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, vInc2SocPostDrivShutdown(_)).Times(1);
   EXPECT_CALL(Gnss, OSAL_s32ThreadJoin(_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, s32SemaphoreClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32DeInit();
   EXPECT_EQ(DEVICE_DEINITIALIZED,rGnssProxyInfo.enGnssProxyState);
   EXPECT_EQ(TRUE, rGnssProxyInfo.bShutdownFlag);
   EXPECT_EQ(OSAL_NULL, rGnssProxyInfo.s32SocketFD);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxySemaphore);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyReadEvent);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE, rGnssProxyInfo.hGnssProxyTeseoComEvent);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}


/**********************GNSS IOCTRL**********************/

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_SetSatSys)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS;
   tS32 s32Arg = OSAL_C_U8_GNSS_SATSYS_ALL;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SetSatSys(_)).Times(1).WillOnce(Return(OSAL_E_NOERROR));
   EXPECT_CALL(Gnss, GnssProxyvWriteLastUsedSatSys(_)).Times(1);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_SetSatSys_NullErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,OSAL_NULL);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,retval);
}


TEST_F(Gnss_Opened, GnssProxy_IOCtrl_GetSatSys)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS;
   tS32 s32Arg;
   EXPECT_CALL(Osal_mock, s32SemaphoreWait(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphorePost(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_GetSatSys_NullErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,OSAL_NULL);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_DiagSetSatSys)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTRL_GNSS_DIAG_SET_SAT_SYS;
   tS32 s32Arg = OSAL_C_U8_GNSS_SATSYS_ALL;
   EXPECT_CALL(Gnss, GnssProxys32SetSatSys(_)).Times(1).WillOnce(Return(OSAL_E_NOERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_DiagSetSatSys_NullErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTRL_GNSS_DIAG_SET_SAT_SYS;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,OSAL_NULL);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_NmeaRcvdList)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_GET_NMEA_RECVD_LIST;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxys32GetNmeaRecvdList(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_OK,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_NmeaRcvdList_NullErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_GET_NMEA_RECVD_LIST;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,OSAL_NULL);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_FlashImg)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE;
   tS32 s32Arg;
   rGnssProxyInfo.enAccessMode = OSAL_EN_WRITEONLY;
   EXPECT_CALL(Gnss, GnssProxyFws32ConfigBinOpts(_)).Times(1).WillOnce(Return(OSAL_E_NOERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_FlashImg_NOWriteAccess)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE;
   tS32 s32Arg;
   rGnssProxyInfo.enAccessMode = OSAL_EN_READONLY;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOACCESS,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_FlashImg_NullErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE;
   rGnssProxyInfo.enAccessMode = OSAL_EN_WRITEONLY;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,OSAL_NULL);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_SetEpoch)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_SET_EPOCH;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SetEpoch(_)).Times(1);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_OK,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_SetEpoch_NullErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_SET_EPOCH;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,OSAL_NULL);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_FlushData)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxys32FlushSccBuff()).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_FlushData_RetERR)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxys32FlushSccBuff()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_ERROR,retval);
}


TEST_F(Gnss_Opened, GnssProxy_IOCtrl_CRC_NOAccess)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_GET_GNSS_CHIP_CRC;
   tS32 s32Arg;
   rGnssProxyInfo.enAccessMode = OSAL_EN_READONLY;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOACCESS,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_CRC_NullErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_GET_GNSS_CHIP_CRC;
   rGnssProxyInfo.enAccessMode = OSAL_EN_WRITEONLY;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,OSAL_NULL);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_CRC_ReturnErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_GET_GNSS_CHIP_CRC;
   tS32 s32Arg;
   rGnssProxyInfo.enAccessMode = OSAL_EN_WRITEONLY;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, GnssProxyFws32GetTeseoCRC()).Times(1).WillOnce(Return(OSAL_ERROR));
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_TEMP_NOT_AVAILABLE,retval);
   EXPECT_EQ(0, rGnssProxyInfo.rGnssConfigData.u32GnssRecvFwCrc);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_CRC)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_GET_GNSS_CHIP_CRC;
   tS32 s32Arg;
   rGnssProxyInfo.enAccessMode = OSAL_EN_WRITEONLY;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyFws32GetTeseoCRC()).Times(1).WillOnce(Return(OSAL_OK));
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_GetEpoch)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_GET_EPOCH;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, GnssProxys32GetEpochTime(_)).Times(1).WillOnce(Return(OSAL_OK));
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_GetEpoch_ReturnErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_GET_EPOCH;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   EXPECT_CALL(Gnss, GnssProxys32GetEpochTime(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_SetChipType)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_SET_CHIP_TYPE;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyFws32SetChipType(_)).Times(1).WillOnce(Return(OSAL_OK));
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_SetChipType_ReturnErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTL_GNSS_SET_CHIP_TYPE;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyFws32SetChipType(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_ERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_GetCfgData)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTRL_GNSS_GET_CONFIG_DATA;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoCRC()).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoFwVer()).Times(1).WillOnce(Return(OSAL_OK));
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}

TEST_F(Gnss_Opened, GnssProxy_IOCtrl_GetCfgData_NullData)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTRL_GNSS_GET_CONFIG_DATA;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,OSAL_NULL);
   EXPECT_EQ(OSAL_E_INVALIDVALUE,retval);
}


TEST_F(Gnss_Opened, GnssProxy_IOCtrl_GetCfgData_TesCrcTesFwVerRetErr)
{
   tS32 retval;
   tS32 s32FunId = OSAL_C_S32_IOCTRL_GNSS_GET_CONFIG_DATA;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(5);
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoCRC()).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32GetTeseoFwVer()).Times(1).WillOnce(Return(OSAL_ERROR));
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(0,rGnssProxyInfo.rGnssConfigData.u32GnssRecvFwCrc);
   EXPECT_EQ(OSAL_E_NOERROR,retval);
}


TEST_F(Gnss_Opened, GnssProxy_IOCtrl_DefCase)
{
   tS32 retval;
   tS32 s32FunId = 999;
   tS32 s32Arg;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   retval = GnssProxy_s32IOControl(s32FunId,(tLong)&s32Arg);
   EXPECT_EQ(OSAL_E_NOTSUPPORTED,retval);
}

/**********************GNSS ReleaseResource**********************/

TEST_F(Gnss_Opened, GnssProxy_RelRes_Socket)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELSEASE_SOCKET;
   EXPECT_CALL(Gnss, close_mock(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_NULL,rGnssProxyInfo.s32SocketFD);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_SocketClsErr)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELSEASE_SOCKET;
   EXPECT_CALL(Gnss, close_mock(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_NULL,rGnssProxyInfo.s32SocketFD);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_SemaphoreClsErr)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_SEMAPHORE;
   EXPECT_CALL(Osal_mock, s32SemaphoreClose(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_SemaphoreDelErr)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_SEMAPHORE;
   EXPECT_CALL(Osal_mock, s32SemaphoreClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreDelete(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE,rGnssProxyInfo.hGnssProxySemaphore);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_Semaphore)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_SEMAPHORE;
   EXPECT_CALL(Osal_mock, s32SemaphoreClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE,rGnssProxyInfo.hGnssProxySemaphore);
}



TEST_F(Gnss_Opened, GnssProxy_RelRes_ReadEve)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_READ_EVENT;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE,rGnssProxyInfo.hGnssProxyReadEvent);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_ReadEveDelErr)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_READ_EVENT;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE,rGnssProxyInfo.hGnssProxyReadEvent);
}


TEST_F(Gnss_Opened, GnssProxy_RelRes_ReadEveClsErr)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_READ_EVENT;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
}


TEST_F(Gnss_Opened, GnssProxy_RelRes_TeseoEve)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE,rGnssProxyInfo.hGnssProxyTeseoComEvent);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_TesEveDelErr)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE,rGnssProxyInfo.hGnssProxyTeseoComEvent);
}


TEST_F(Gnss_Opened, GnssProxy_RelRes_TesEveClsErr)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_InitEveDelErr)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_INIT_EVENT;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE,rGnssProxyInfo.hGnssProxyInitEvent);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_InitEveClsErr)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_INIT_EVENT;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_InitEve)
{
   tU32 u32Resource = GNSS_PROXY_RESOURCE_RELEASE_INIT_EVENT;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   GnssProxy_vReleaseResource(u32Resource);
   EXPECT_EQ(OSAL_C_INVALID_HANDLE,rGnssProxyInfo.hGnssProxyInitEvent);
}

TEST_F(Gnss_Opened, GnssProxy_RelRes_DefCase)
{
   tU32 u32Resource = 999;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vReleaseResource(u32Resource);
}


