HOW TO:

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


Run Utest Files:

1) Build the unittest product with the following command : 

   build.pl --release [--info] --x86 <product name> [i.e unittest_gyro] --> generates debug binaries
   
   build.pl --release [--info] --x86 <product name> [i.e unittest_gyro] --> generates release binaries

2) To run each unittest file. 

   a) go to the directory where the file is generated
   b) run the unittest with command ./<generated .out file> [i.e ./unittest_gyro_out.out]

                                 OR
   To run all the tests at once ( runs only debug binaries )
   
   build.pl --utrun

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

   
Below Steps are needed to generate the Code Coverage

1) In order to generate the code coverage for the file, use the following command :
   
   build.pl --release [--info] --x86 --coverage <product name i.e unittest_gyro> 
   
2) Run the unittest file.

3) Open a new terminal if needed. go to the directory where the coverage and other object files are generated.
   
   example path for dev_gyro :
   
   PATH - cd /home/<NT-ID>/bosch/<NT-ID>_AI_PRJ_ESO_BASE_SW_16.0F36.vws_GEN/ai_projects/generated/build/linuxx86make/osal_linux/unittest_gyro_out_r/objects/dev_gyro/unittest

4) Run the lcov command to generate info file containing coverage data: 

   lcov --directory < *.info generated folder > --capture --output-file <your output file>
   
   directory example - (pwd - i.e <----  /home/<NT-ID>/bosch/<NT-ID>_AI_PRJ_ESO_BASE_SW_16.0F36.vws_GEN/ai_projects/generated/build/linuxx86make/osal_linux/unittest_gyro_out_r/objects/dev_gyro/unittest ---->)

5) Once done a *.info file will be generated. using this file generate the coverage in HTML format using the below command.

   Command : genhtml <your_generated.info> --output-directory <output folder>


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------