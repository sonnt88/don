#include "GnssMock.h"

tS32 mocked_GnssProxyFw_s32Init(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxyFws32Init();
}

tVoid mocked_GnssProxy_vReadRegistryValues(tVoid)
{
   GnssMock::GetDelegatee().GnssProxyvReadRegistryValues();
}

tS32 mocked_GnssProxy_s32GetTeseoFwVer(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32GetTeseoFwVer();
}

tS32 mocked_GnssProxy_s32GetTeseoCRC(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32GetTeseoCRC();
}

tS32 mocked_GnssProxy_s32SetSatConfigReq(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32SetSatConfigReq();
}

tS32 mocked_GnssProxy_s32GetCfgBlk200_227(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32GetCfgBlk200_227();
}

tS32  mocked_GnssProxy_s32SocketSetup(tU32 u32IncPort)
{
   return GnssMock::GetDelegatee().GnssProxys32SocketSetup(u32IncPort);
}

tS32  mocked_GnssProxy_s32InitCommunToSCC(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32InitCommunToSCC();
}

tS32  mocked_GnssProxy_s32GetDataFromScc(tU32 u32Bytes)
{
   return GnssMock::GetDelegatee().GnssProxys32GetDataFromScc(u32Bytes);
}

tS32  mocked_GnssProxy_s32HandleStatusMsg(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32HandleStatusMsg();
}

tS32  mocked_GnssProxy_s32HandleConfigMsg(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32HandleConfigMsg();
}

tVoid mocked_GnssProxy_vHandleCommandReject(tVoid)
{
   GnssMock::GetDelegatee().GnssProxyvHandleCommandReject();
}

tVoid mocked_GnssProxy_vHandleGnssData(tVoid)
{
   GnssMock::GetDelegatee().GnssProxyvHandleGnssData();
}

tS32  mocked_GnssProxy_s32SetSatSys(tPU32 pu32SatSys)
{
   return GnssMock::GetDelegatee().GnssProxys32SetSatSys(pu32SatSys);
}

tS32  mocked_GnssProxy_s32GetNmeaRecvdList(tPU32 pu32NmeaList)
{
   return GnssMock::GetDelegatee().GnssProxys32GetNmeaRecvdList(pu32NmeaList);
}

tS32  mocked_GnssProxyFw_s32ConfigBinOpts(const trImageOptions *prImageOptions)
{
   return GnssMock::GetDelegatee().GnssProxyFws32ConfigBinOpts(prImageOptions);
}

tS32  mocked_GnssProxy_s32FlushSccBuff(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32FlushSccBuff();
}

tVoid mocked_GnssProxy_vWriteLastUsedSatSys( tU32 u32actGnssSatSys )
{
   GnssMock::GetDelegatee().GnssProxyvWriteLastUsedSatSys(u32actGnssSatSys);
}

tS32  mocked_GnssProxyFw_s32SetChipType(tEnGnssChipType enGnssChipType)
{
   return GnssMock::GetDelegatee().GnssProxyFws32SetChipType(enGnssChipType);
}

tS32  mocked_GnssProxyFw_s32SendBinToTeseo(const char* pBuffer, tU32 u32maxbytes)
{
   return GnssMock::GetDelegatee().GnssProxyFws32SendBinToTeseo(pBuffer,u32maxbytes);
}

tS32  mocked_GnssProxyFw_s32DeInit(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxyFws32DeInit();
}

tS32  mocked_GnssProxy_s32SendStatusCmd(tU8 u8Status, tU8 u8CompVer)
{
   return GnssMock::GetDelegatee().GnssProxys32SendStatusCmd(u8Status,u8CompVer);
}

tS32  mocked_GnssProxy_s32SetEpoch( const OSAL_trGnssTimeUTC *rEpochToSet )
{
   return GnssMock::GetDelegatee().GnssProxys32SetEpoch(rEpochToSet);
}

tVoid mocked_GnssProxy_vTraceOut(TR_tenTraceLevel enLevel,enGNSSPXYTRC enGnssPxyTrcVal,const tChar *pcFormatString,...)
{
   GnssMock::GetDelegatee().GnssProxyvTraceOut(enLevel,enGnssPxyTrcVal,pcFormatString);
}

tS32  mocked_GnssProxyFw_s32GetTeseoCRC(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxyFws32GetTeseoCRC();
}

tS32 mocked_Inc2Soc_s32CreateResources( tEnInc2SocDev enDeviceType )
{
   return GnssMock::GetDelegatee().s32Inc2SocCreateResources(enDeviceType);
}

tVoid mocked_GnssProxy_vReleaseResource(tU32 u32Resource )
{
   GnssMock::GetDelegatee().GnssProxyvReleaseResource(u32Resource);
}

tS32  mocked_GnssProxy_s32SaveConfigDataBlock(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32SaveConfigDataBlock();
}

tS32  mocked_GnssProxy_s32SendDataToScc(tU32 u32Bytes)
{
   return GnssMock::GetDelegatee().GnssProxys32SendDataToScc(u32Bytes);
}


tS32  mocked_GnssProxy_s32RebootTeseo(tVoid)
{
   return GnssMock::GetDelegatee().GnssProxys32RebootTeseo();
}

tVoid mocked_GnssProxy_vEnterCriticalSection( tU32 u32LineNum )
{
   GnssMock::GetDelegatee().GnssProxyvEnterCriticalSection(u32LineNum);
}

tVoid mocked_GnssProxy_vLeaveCriticalSection( tU32 u32LineNum )
{
   GnssMock::GetDelegatee().GnssProxyvLeaveCriticalSection(u32LineNum);
}

tVoid mocked_GnssProxy_vSyncAuxClock( tU32 u32V850TimStmp )
{
   GnssMock::GetDelegatee().GnssProxyvSyncAuxClock(u32V850TimStmp);
}

tVoid mocked_Inc2Soc_vPostDrivShutdown( tEnInc2SocDev enDeviceType )
{
   GnssMock::GetDelegatee().vInc2SocPostDrivShutdown(enDeviceType);
}

tVoid mocked_Inc2Soc_vWriteDataToBuffer( tEnInc2SocDev enDeviceType )
{
   GnssMock::GetDelegatee().vInc2SocWriteDataToBuffer(enDeviceType);
}

tS32  mocked_GnssProxy_s32GetEpochTime( OSAL_trGnssTimeUTC *rEpochSetTime )
{
   return GnssMock::GetDelegatee().GnssProxys32GetEpochTime(rEpochSetTime);
}

tS32 mocked_GnssProxy_s32WaitForMsg( tU32 u32EventMask, tU32 u32WaitTime )
{
   return GnssMock::GetDelegatee().GnssProxys32WaitForMsg(u32EventMask, u32WaitTime);
}

#ifdef GNSS_MOCKSTOBEREMOVED
// Will be removed once OSAL team creates the following mocks.
tS32 OSAL_s32ThreadJoin(OSAL_tThreadID tid, OSAL_tMSecond msec)
{
   return GnssMock::GetDelegatee().OSAL_s32ThreadJoin(tid, msec);
}

tBool LLD_bIsTraceActive(tU32 u32Class, tU32 u32Level)
{
   return TRUE;
}

tVoid LLD_vTrace(tU32 u32Class, tU32 u32Level, const void* pvData, tU32 u32Length)
{
   //do nothing
}

// The Following mocks have to be remove once the mocks are 
// in place from INC/Communication Team
int mocked_close(int fd)
{
   return GnssMock::GetDelegatee().close_mock(fd);
}

int mocked_open(const char *pathname, int flags)
{
return GnssMock::GetDelegatee().open_mock(pathname, flags);
}

ssize_t mocked_write(int fd, const void *buf, size_t count)
{
return GnssMock::GetDelegatee().write_mock(fd, buf, count);
}

int mocked_socket(int domain, int type, int protocol)
{
return GnssMock::GetDelegatee().socket_mock(domain, type, protocol);
}

int mocked_bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
return GnssMock::GetDelegatee().bind_mock(sockfd, addr, addrlen);
}

int mocked_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
return GnssMock::GetDelegatee().connect_mock(sockfd, addr, addrlen);
}

int  poll(struct pollfd *fds, nfds_t nfds, int timeout)
{
  timeout=1;
  fds[0].revents = POLLIN;
  return GnssMock::GetDelegatee().poll(fds,nfds,timeout);
}

int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen)
{
   return GnssMock::GetDelegatee().dgram_send(skd, ubuf, ulen);
}

int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen)
{
   return GnssMock::GetDelegatee().dgram_recv(skd, ubuf, ulen);
}
  
sk_dgram* dgram_init(int sk, int dgram_max, void *options)
{
   return GnssMock::GetDelegatee().dgram_init(sk, dgram_max, options);
}




#endif //GNSS_MOCKSTOBEREMOVED