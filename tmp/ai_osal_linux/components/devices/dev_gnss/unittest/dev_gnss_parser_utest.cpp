#include "gmock/gmock.h"
#include "gtest/gtest.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if_mock.h"
#define GNSS_MOCKSTOBEREMOVED
#include "GnssMock.h"
#include "GnssMock.cpp"
#include <float.h>

#undef GnssProxy_s32HandleStatusMsg
#undef GnssProxy_vHandleCommandReject
#undef GnssProxy_vHandleGnssData
#undef GnssProxy_s32HandleConfigMsg
#undef GnssProxy_s32GetCfgBlk200_227
#undef GnssProxy_s32SetSatSys
#undef GnssProxy_vReadRegistryValues
#undef GnssProxy_vWriteLastUsedSatSys
#undef GnssProxy_s32SetSatConfigReq
#undef GnssProxy_s32SaveConfigDataBlock
#undef GnssProxy_s32RebootTeseo
#undef GnssProxy_s32GetTeseoFwVer
#undef GnssProxy_s32GetTeseoCRC
#undef GnssProxy_s32FlushSccBuff
#undef GnssProxy_s32GetNmeaRecvdList

#include "../sources/dev_gnss_parser.c"

using namespace testing;
using namespace std;

int main(int argc, char** argv)
{
   ::testing::InitGoogleMock(&argc, argv);
   return RUN_ALL_TESTS();
}

class GnssParser_InitGlobals : public ::testing::Test
{
public:
   GnssMock Gnss;
   NiceMock<OsalMock> Osal_mock;
   virtual void SetUp()
   {
      rGnssProxyInfo = {0};
   }
};

/************************* GnssProxy_HandleStatusMsg *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleStatusMsg_StatusActive)
{
   tS32 s32RetVal;
   rGnssProxyInfo.u32RecvdMsgSize = MSG_SIZE_SCC_GNSS_R_COMPONENT_STATUS;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_STATUS] = GNSS_PROXY_COMPONENT_STATUS_ACTIVE;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION] = GNSS_PROXY_COMPONENT_VERSION;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   s32RetVal = GnssProxy_s32HandleStatusMsg();
   EXPECT_EQ(OSAL_OK, s32RetVal);
   EXPECT_EQ(GNSS_PROXY_COMPONENT_STATUS_ACTIVE, rGnssProxyInfo.u8SccStatus);
   EXPECT_EQ(GNSS_PROXY_COMPONENT_VERSION, rGnssProxyInfo.u8VerInfo);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleStatusMsg_UnknownMsgRcvd)
{
   tS32 s32RetVal;
   rGnssProxyInfo.u32RecvdMsgSize = 99;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal = GnssProxy_s32HandleStatusMsg();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleStatusMsg_StatusUnknown)
{
   tS32 s32RetVal;
   rGnssProxyInfo.u32RecvdMsgSize = MSG_SIZE_SCC_GNSS_R_COMPONENT_STATUS;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_STATUS] = 99;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal = GnssProxy_s32HandleStatusMsg();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(GNSS_PROXY_COMPONENT_STATUS_UNKNOWN, rGnssProxyInfo.u8SccStatus);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleStatusMsg_StatusInacitve)
{
   tS32 s32RetVal;
   rGnssProxyInfo.u32RecvdMsgSize = MSG_SIZE_SCC_GNSS_R_COMPONENT_STATUS;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_STATUS] = GNSS_PROXY_COMPONENT_STATUS_NOT_ACTIVE;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal = GnssProxy_s32HandleStatusMsg();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(GNSS_PROXY_COMPONENT_STATUS_NOT_ACTIVE, rGnssProxyInfo.u8SccStatus);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleStatusMsg_VersionUnknown)
{
   tS32 s32RetVal;
   rGnssProxyInfo.u32RecvdMsgSize = MSG_SIZE_SCC_GNSS_R_COMPONENT_STATUS;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_STATUS] = GNSS_PROXY_COMPONENT_STATUS_ACTIVE;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION] = 99;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   s32RetVal = GnssProxy_s32HandleStatusMsg();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(GNSS_PROXY_COMPONENT_STATUS_ACTIVE, rGnssProxyInfo.u8SccStatus);
   EXPECT_EQ(99, rGnssProxyInfo.u8VerInfo);
}

/************************* GnssProxy_HandleCommandReject *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleCommandReject)
{
   rGnssProxyInfo.u32RecvdMsgSize = MSG_SIZE_SCC_GNSS_R_REJECT;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   GnssProxy_vHandleCommandReject();
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleCommandReject_ErrCond)
{
   rGnssProxyInfo.u32RecvdMsgSize = 4;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   GnssProxy_vHandleCommandReject();
}

/************************* GnssProxy_HandleConfigMsg *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleConfigMsg_CfgReq)
{
   tS32 s32RetVal;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_SERVICE_TYPE] = GNSS_PROXY_SERVICE_TYPE_CONFIG_RQ;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_HW_TYPE] = GNSS_HW_STA8088;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_DATA_INTERVAL] = 100;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal =GnssProxy_s32HandleConfigMsg();
   EXPECT_EQ(200, rGnssProxyInfo.u16ReadWaitTimeMs);
   EXPECT_EQ(10, rGnssProxyInfo.rGnssConfigData.u8UpdateFrequency);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleConfigMsg_FlushSensDataOK)
{
   tS32 s32RetVal;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_SERVICE_TYPE] = GNSS_FLUSH_SENSOR_DATA_REQ;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CFG_FLUSH_BUFF_RES] = GNSS_PROXY_FLUSH_BUFF_RES_OK;
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1);
   s32RetVal =GnssProxy_s32HandleConfigMsg();
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleConfigMsg_FlushSensDataErr)
{
   tS32 s32RetVal;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_SERVICE_TYPE] = GNSS_FLUSH_SENSOR_DATA_REQ;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CFG_FLUSH_BUFF_RES] = 99;
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1);
   s32RetVal =GnssProxy_s32HandleConfigMsg();
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandleConfigMsg_Unsupp_DefCases)
{
   tS32 s32RetVal;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_SERVICE_TYPE] = GNSS_PROXY_SERVICE_TYPE_HW_RESET;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal =GnssProxy_s32HandleConfigMsg();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/************************* GnssProxy_ParseUtcTimeDate *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_ParseUtcTimeDate)
{
   OSAL_trGnssTimeUTC  rGPSTimeUTC = {2016,10,26,0,0,0,0};
   tChar cTmpDatTim[GNSS_PROXY_ARRAY_SIZE] = {'2','0','1','6'};
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vParseUtcTimeDate((char*)&rGPSTimeUTC,cTmpDatTim);
}

/************************* GnssProxy_ValidateSatSys *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_ValidateSatSys)
{
   tU32 u32SatSys = OSAL_C_U8_GNSS_SATSYS_GPS;
   tS32 s32RetVal;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   s32RetVal = GnssProxy_s32ValidateSatSys(u32SatSys);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_ValidateSatSys_CompassUnsupp)
{
   tU32 u32SatSys = OSAL_C_U8_GNSS_SATSYS_COMPASS;
   tS32 s32RetVal;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal = GnssProxy_s32ValidateSatSys(u32SatSys);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_ValidateSatSys_InvalidSatSys)
{
   tU32 u32SatSys = 99;
   tS32 s32RetVal;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal = GnssProxy_s32ValidateSatSys(u32SatSys);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_ValidateSatSys_NullSatSys)
{
   tU32 u32SatSys = 0;
   tS32 s32RetVal;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   s32RetVal = GnssProxy_s32ValidateSatSys(u32SatSys);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

/************************* GnssProxy_FrameCfgBlk200_227 *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameCfgBlk200_227)
{
   tU32 u32SatSys = OSAL_C_U8_GNSS_SATSYS_GPS|OSAL_C_U8_GNSS_SATSYS_GALILEO;
   tU32 u32TeseoBitMaskBlk200 = 0;
   tU32 u32TeseoBitMaskBlk227 = 0;
   tS32 s32RetVal;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   s32RetVal = GnssProxy_s32FrameCfgBlk200_227(u32SatSys, &u32TeseoBitMaskBlk200, &u32TeseoBitMaskBlk227);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(0x410000, u32TeseoBitMaskBlk200);
   EXPECT_EQ(0xC0, u32TeseoBitMaskBlk227);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameCfgBlk200_227_NullCfg)
{
   tU32 u32SatSys = 0;
   tU32 u32TeseoBitMaskBlk200 = 0;
   tU32 u32TeseoBitMaskBlk227 = 0;
   tS32 s32RetVal;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   s32RetVal = GnssProxy_s32FrameCfgBlk200_227(u32SatSys, &u32TeseoBitMaskBlk200, &u32TeseoBitMaskBlk227);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
   EXPECT_EQ(0, u32TeseoBitMaskBlk200);
   EXPECT_EQ(0, u32TeseoBitMaskBlk227);
}

/************************* GnssProxy_GetCfgBlk200_227 *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_GetCfgBlk200_227)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).WillOnce(Return(21))
                                                  .WillOnce(Return(21)); //21 =  length of 
                                                                         //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                         //OSAL_u32StringLength(GNSS_PROXY_TESEO_GET_APP_CFG_BLK_227_CMD)
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).WillOnce(Return(OSAL_OK)).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32GetCfgBlk200_227();
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_GetCfgBlk200_227_CfgReqErr)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).WillOnce(Return(0));
   s32RetVal = GnssProxy_s32GetCfgBlk200_227();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/************************* GnssProxy_SetSatSysConfBlk200 *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_SetSatSysConfBlk200)
{
   tS32 s32RetVal;
   tU32 u32TeseoCfg = GNSS_PROXY_TESEO_BIT_MASK_GPS_ENABLE;
   rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk200 = GNSS_PROXY_CDB_200_SAT_CONSTE_MASK;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   EXPECT_CALL(Gnss, GnssProxyvEnterCriticalSection(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvLeaveCriticalSection(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(28));//28 =  length of 
                                                                                  //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                                  //OSAL_u32StringLength(&rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG]);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32SetSatSysConfBlk200(u32TeseoCfg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_SetSatSysConfBlk200_NullOldCfg)
{
   tS32 s32RetVal;
   tU32 u32TeseoCfg = GNSS_PROXY_TESEO_BIT_MASK_GPS_ENABLE;
   rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk200 = 0;
   EXPECT_CALL(Gnss, GnssProxyvEnterCriticalSection(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvLeaveCriticalSection(_)).Times(1);
   s32RetVal = GnssProxy_s32SetSatSysConfBlk200(u32TeseoCfg);
   EXPECT_EQ(OSAL_E_UNKNOWN, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_SetSatSysConfBlk200_AppendErr)
{
   tS32 s32RetVal;
   tU32 u32TeseoCfg = GNSS_PROXY_TESEO_BIT_MASK_GPS_ENABLE;
   rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk200 = 0x410000;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   EXPECT_CALL(Gnss, GnssProxyvEnterCriticalSection(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvLeaveCriticalSection(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(-1));
   s32RetVal = GnssProxy_s32SetSatSysConfBlk200(u32TeseoCfg);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/************************* GnssProxy_Set227CfgMask *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_Set227CfgMask)
{
   tS32 s32RetVal;
   tU32 u32OldTeseoCfgBlk227;
   tU32 u32TeseoBitMask = GNSS_PROXY_TESEO_BIT_MASK_GALILEO_ENABLE;
   u32OldTeseoCfgBlk227 = 0xC0;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(24));//24 =  length of 
                                                                         //GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                         //OSAL_u32StringLength(&rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG]);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32Set227CfgMask(u32TeseoBitMask, u32OldTeseoCfgBlk227);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_Set227CfgMask_AppendErr)
{
   tS32 s32RetVal;
   tU32 u32OldTeseoCfgBlk227;
   tU32 u32TeseoBitMask = GNSS_PROXY_TESEO_BIT_MASK_GALILEO_ENABLE;
   u32OldTeseoCfgBlk227 = 0xC0;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(-1));
   s32RetVal = GnssProxy_s32Set227CfgMask(u32TeseoBitMask, u32OldTeseoCfgBlk227);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/************************* GnssProxy_SetSatSysConfBlk227 *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_SetSatSysConfBlk227_NullOldCfgBlk)
{
   tS32 s32RetVal;
   tU32 u32OldTeseoCfgBlk227;
   tU32 u32TeseoBitMask = GNSS_PROXY_TESEO_BIT_MASK_GALILEO_ENABLE;
   rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk227 = 0;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   EXPECT_CALL(Gnss, GnssProxyvEnterCriticalSection(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvLeaveCriticalSection(_)).Times(1);
   s32RetVal = GnssProxy_s32SetSatSysConfBlk227(u32TeseoBitMask);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_SetSatSysConfBlk227_CompassCfgReqErr)
{
   tS32 s32RetVal;
   tU32 u32OldTeseoCfgBlk227;
   tU32 u32TeseoBitMask = GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE;
   rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk227 = 0xC0;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvEnterCriticalSection(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvLeaveCriticalSection(_)).Times(1);
   s32RetVal = GnssProxy_s32SetSatSysConfBlk227(u32TeseoBitMask);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_SetSatSysConfBlk227_STA8089)
{
   tS32 s32RetVal;
   tU32 u32OldTeseoCfgBlk227;
   tU32 u32TeseoBitMask = GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE;
   rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk227 = 0xC0;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8089;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(25));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvEnterCriticalSection(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvLeaveCriticalSection(_)).Times(1);
   s32RetVal = GnssProxy_s32SetSatSysConfBlk227(u32TeseoBitMask);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_SetSatSysConfBlk227_TesUnKnown)
{
   tS32 s32RetVal;
   tU32 u32OldTeseoCfgBlk227;
   tU32 u32TeseoBitMask = GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE;
   rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk227 = 0xC0;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_UNKNOWN;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvEnterCriticalSection(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxyvLeaveCriticalSection(_)).Times(1);
   s32RetVal = GnssProxy_s32SetSatSysConfBlk227(u32TeseoBitMask);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

/************************* GnssProxy_SaveConfigDataBlock *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_SaveConfigDataBlock)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_COM_SAVE_CDB_RESPONSE_MASK)
                                                            ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(16));
   s32RetVal = GnssProxy_s32SaveConfigDataBlock();
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_SaveConfigDataBlock_ShutdownEve)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT)
                                                            ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Gnss, GnssProxyvReleaseResource(_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(16));
   s32RetVal = GnssProxy_s32SaveConfigDataBlock();
   EXPECT_EQ(OSAL_E_CANCELED, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_SaveConfigDataBlock_EveWaitErr)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(16));
   s32RetVal = GnssProxy_s32SaveConfigDataBlock();
   EXPECT_EQ(16, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_SaveConfigDataBlock_EvePostErr)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT)
                                                            ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(16));
   s32RetVal = GnssProxy_s32SaveConfigDataBlock();
   EXPECT_EQ(16, s32RetVal);
}

/************************* GnssProxy_RebootTeseo *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_RebootTeseo)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(12));//u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                                //OSAL_u32StringLength( GNSS_PROXY_TESEO_REBOOT );
   s32RetVal = GnssProxy_s32RebootTeseo();
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_RebootTeseo_SendDatatoSccErr)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(-1));
   s32RetVal = GnssProxy_s32RebootTeseo();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/************************* GnssProxy_StoreSatSysUsed *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_StoreSatSysUsed_EnableAllSatsCfg200)
{
   tU32 u32SatSysConf = GNSS_PROXY_TESEO_BIT_MASK_GPS_ENABLE|
                        GNSS_PROXY_TESEO_BIT_MASK_GLONASS_ENABLE|
                        GNSS_PROXY_TESEO_BIT_MASK_QZSS_ENABLE|
                        GNSS_PROXY_TESEO_BIT_MASK_SBAS_ENABLE;
   tU32 u32BlkID = GNSS_PROXY_APP_LIST_BLOCK_ID_200;
   rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed = 0;
   GnssProxy_vStoreSatSysUsed(u32SatSysConf, u32BlkID);
   EXPECT_EQ(0x33, rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_StoreSatSysUsed_EnableAllSatsCfg227)
{
   tU32 u32SatSysConf = GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE|
                        GNSS_PROXY_TESEO_BIT_MASK_SBAS_ENABLE;
   tU32 u32BlkID = GNSS_PROXY_APP_LIST_BLOCK_ID_227;
   rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed = 0;
   GnssProxy_vStoreSatSysUsed(u32SatSysConf, u32BlkID);
   EXPECT_EQ(0x18, rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed);
}


TEST_F(GnssParser_InitGlobals, GnssProxy_StoreSatSysUsed_NullCfg)
{
   tU32 u32SatSysConf = 0;
   tU32 u32BlkID = GNSS_PROXY_APP_LIST_BLOCK_ID_200;
   rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed = 0;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   GnssProxy_vStoreSatSysUsed(u32SatSysConf, u32BlkID);
   EXPECT_EQ(0x00, rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed);
}

/************************* GnssProxy_FrameSatSysBlk227 *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameSatSysBlk227)
{
   tU32 u32SatSysCfgBlk = 0;
   tU32 u32SatSysConfig = OSAL_C_U8_GNSS_SATSYS_GALILEO | OSAL_C_U8_GNSS_SATSYS_COMPASS;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8089;
   u32SatSysCfgBlk= GnssProxy_u32FrameSatSysBlk227(u32SatSysConfig);
   EXPECT_EQ(0x3C0, u32SatSysCfgBlk); //All Sats Set
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameSatSysBlk227_SetGALILEO)
{
   tU32 u32SatSysCfgBlk = 0;
   tU32 u32SatSysConfig = OSAL_C_U8_GNSS_SATSYS_GALILEO;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   u32SatSysCfgBlk= GnssProxy_u32FrameSatSysBlk227(u32SatSysConfig);
   EXPECT_EQ(0x0C0, u32SatSysCfgBlk); //only Galileo Set
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameSatSysBlk227_SetCOMPASS)
{
   tU32 u32SatSysCfgBlk = 0;
   tU32 u32SatSysConfig = OSAL_C_U8_GNSS_SATSYS_COMPASS;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8089;
   u32SatSysCfgBlk= GnssProxy_u32FrameSatSysBlk227(u32SatSysConfig);
   EXPECT_EQ(0x300, u32SatSysCfgBlk); //only COMPASS Set
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameSatSysBlk227_CompassSetError)
{
   tU32 u32SatSysCfgBlk = 0;
   tU32 u32SatSysConfig = OSAL_C_U8_GNSS_SATSYS_GALILEO | OSAL_C_U8_GNSS_SATSYS_COMPASS;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   u32SatSysCfgBlk= GnssProxy_u32FrameSatSysBlk227(u32SatSysConfig);
   EXPECT_EQ(0xC0, u32SatSysCfgBlk); //only Galileo Set
}

/************************* GnssProxy_FrameSatSysBlk200 *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameSatSysBlk200_SetAllSats)
{
   tU32 u32SatSysCfgBlk = 0;
   tU32 u32SatSysConfig = OSAL_C_U8_GNSS_SATSYS_GPS |
                          OSAL_C_U8_GNSS_SATSYS_SBAS |
                          OSAL_C_U8_GNSS_SATSYS_GLONASS |
                          OSAL_C_U8_GNSS_SATSYS_QZSS;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   u32SatSysCfgBlk= GnssProxy_u32FrameSatSysBlk200(u32SatSysConfig);
   EXPECT_EQ(0xE70004, u32SatSysCfgBlk); //AllSats Set
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameSatSysBlk200_SBAS)
{
   tU32 u32SatSysCfgBlk = 0;
   tU32 u32SatSysConfig = OSAL_C_U8_GNSS_SATSYS_SBAS;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   u32SatSysCfgBlk= GnssProxy_u32FrameSatSysBlk200(u32SatSysConfig);
   EXPECT_EQ(0x04, u32SatSysCfgBlk); //only SBAS Set
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameSatSysBlk200_SetGPS)
{
   tU32 u32SatSysCfgBlk = 0;
   tU32 u32SatSysConfig = OSAL_C_U8_GNSS_SATSYS_GPS;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   u32SatSysCfgBlk= GnssProxy_u32FrameSatSysBlk200(u32SatSysConfig);
   EXPECT_EQ(0x410000, u32SatSysCfgBlk); //only GPS Set
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameSatSysBlk200_SetGLONASS)
{
   tU32 u32SatSysCfgBlk = 0;
   tU32 u32SatSysConfig = OSAL_C_U8_GNSS_SATSYS_GLONASS;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   u32SatSysCfgBlk= GnssProxy_u32FrameSatSysBlk200(u32SatSysConfig);
   EXPECT_EQ(0x220000, u32SatSysCfgBlk); //only GLONASS Set
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FrameSatSysBlk200_SetQZSS)
{
   tU32 u32SatSysCfgBlk = 0;
   tU32 u32SatSysConfig = OSAL_C_U8_GNSS_SATSYS_QZSS;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   u32SatSysCfgBlk= GnssProxy_u32FrameSatSysBlk200(u32SatSysConfig);
   EXPECT_EQ(0x840000, u32SatSysCfgBlk); //only QZSS Set
}

/************************* GnssProxy_GetTeseoFwVer *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_GetTeseoFwVer)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(19));//u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                                  //OSAL_u32StringLength(GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_FIRMWARE_VERSION_WAIT_EVENT)
                                                            ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32GetTeseoFwVer();
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_GetTeseoFwVer_WriteFail)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(-1));
   s32RetVal = GnssProxy_s32GetTeseoFwVer();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_GetTeseoFwVer_WaitFail)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(19));//u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                                  //OSAL_u32StringLength(GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GnssProxy_s32GetTeseoFwVer();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_GetTeseoFwVer_PostFail)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(19));//u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                                  //OSAL_u32StringLength(GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_FIRMWARE_VERSION_WAIT_EVENT)
                                                            ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GnssProxy_s32GetTeseoFwVer();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_GetTeseoFwVer_UnexpectedEve)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(19));//u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                                                                                  //OSAL_u32StringLength(GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(-1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GnssProxy_s32GetTeseoFwVer();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/************************* GnssProxy_GetTeseoCRC *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_GetTeseoCRC)
{
   tS32 s32RetVal;
   rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
   rGnssProxyInfo.enAccessMode = OSAL_EN_READONLY;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(42));//u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER 
                                                                                 //+ OSAL_u32StringLength(pcCrcCmd); pcCrcCmd is either 8088/8089
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_TESEO_CRC_CHECK_WAIT_EVENT)
                                                            ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32GetTeseoCRC();
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

/************************* GnssProxy_FlushSccBuff *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_FlushSccBuff)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(GNSS_PROXY_SIZE_OF_C_FLUSH_DATA_MSG_BUFF));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_FLUSH_SENSOR_BUFF_SUCCESS_EVENT)
                                                            ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32FlushSccBuff();
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FlushSccBuff_WriteFail)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(-1));
   s32RetVal = GnssProxy_s32FlushSccBuff();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FlushSccBuff_WaitFail)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(GNSS_PROXY_SIZE_OF_C_FLUSH_DATA_MSG_BUFF));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GnssProxy_s32FlushSccBuff();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FlushSccBuff_PostFail)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(GNSS_PROXY_SIZE_OF_C_FLUSH_DATA_MSG_BUFF));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_FLUSH_SENSOR_BUFF_FAILURE_EVENT)
                                                            ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GnssProxy_s32FlushSccBuff();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_FlushSccBuff_FlushFail)
{
   tS32 s32RetVal;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, GnssProxys32SendDataToScc(_)).Times(1).WillOnce(Return(GNSS_PROXY_SIZE_OF_C_FLUSH_DATA_MSG_BUFF));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GNSS_PROXY_FLUSH_SENSOR_BUFF_FAILURE_EVENT)
                                                            ,Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32FlushSccBuff();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/************************* GnssProxy_AddSatSysOffset *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_AddSatSysOffset_GPS)
{
   tU32 u32RetVal;
   tU32 u32RawSatId = 10;
   u32RetVal = GnssProxy_u32AddSatSysOffset(u32RawSatId);
   EXPECT_EQ(10, u32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_AddSatSysOffset_SBAS)
{
   tU32 u32RetVal;
   tU32 u32RawSatId = 40;
   u32RetVal = GnssProxy_u32AddSatSysOffset(u32RawSatId);
   EXPECT_EQ(407, u32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_AddSatSysOffset_GLONASS)
{
   tU32 u32RetVal;
   tU32 u32RawSatId = 70;
   u32RetVal = GnssProxy_u32AddSatSysOffset(u32RawSatId);
   EXPECT_EQ(105, u32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_AddSatSysOffset_COMPASS)
{
   tU32 u32RetVal;
   tU32 u32RawSatId = 145;
   u32RetVal = GnssProxy_u32AddSatSysOffset(u32RawSatId);
   EXPECT_EQ(304, u32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_AddSatSysOffset_QZSSSet1)
{
   tU32 u32RetVal;
   tU32 u32RawSatId = 185;
   u32RetVal = GnssProxy_u32AddSatSysOffset(u32RawSatId);
   EXPECT_EQ(502, u32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_AddSatSysOffset_QZSSSet2)
{
   tU32 u32RetVal;
   tU32 u32RawSatId = 295;
   u32RetVal = GnssProxy_u32AddSatSysOffset(u32RawSatId);
   EXPECT_EQ(502, u32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_AddSatSysOffset_GALILEO)
{
   tU32 u32RetVal;
   tU32 u32RawSatId = 320;
   u32RetVal = GnssProxy_u32AddSatSysOffset(u32RawSatId);
   EXPECT_EQ(219, u32RetVal);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_AddSatSysOffset_UnknownSatID)
{
   tU32 u32RetVal;
   tU32 u32RawSatId = 9999;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   u32RetVal = GnssProxy_u32AddSatSysOffset(u32RawSatId);
   EXPECT_EQ(9999, u32RetVal);
}

/************************* GnssProxy_HandlePSTMVER *************************/

TEST_F(GnssParser_InitGlobals, GnssProxy_HandlePSTMVER_NullData)
{
   tPChar pcFieldIndex = OSAL_NULL;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   GnssProxy_vHandlePSTMVER( (const tChar * const*) pcFieldIndex);
   EXPECT_EQ(GNSS_HW_UNKNOWN, rGnssProxyInfo.rGnssConfigData.enGnssHwType);
   EXPECT_EQ(0, rGnssProxyInfo.rGnssConfigData.u16NumOfChannels);
   EXPECT_EQ(0, rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer);
   EXPECT_EQ(0, rGnssProxyInfo.rGnssConfigData.u16GnssSatStatusSupported);
}


TEST_F(GnssParser_InitGlobals, GnssProxy_HandlePSTMVER_Teseo2Board)
{
   tPChar pcFieldIndex[GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE];
   memset( pcFieldIndex, 0, (sizeof (tPChar) * GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE) );
   pcFieldIndex[GNSS_PROXY_PSTMVER_OFFSET_BINIMG_VER] = "BINIMG_3.1.18.2_ARM";
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   GnssProxy_vHandlePSTMVER( (const tChar * const*) pcFieldIndex);
   EXPECT_EQ(GNSS_HW_STA8088, rGnssProxyInfo.rGnssConfigData.enGnssHwType);
   EXPECT_EQ(GNSS_PROXY_STA8088_SUPPORTED_CHANNELS, rGnssProxyInfo.rGnssConfigData.u16NumOfChannels);
   EXPECT_EQ(0x3011202, rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer);
   EXPECT_EQ(OSAL_C_U16_GNSS_SAT_USED_FOR_POSCALC, rGnssProxyInfo.rGnssConfigData.u16GnssSatStatusSupported);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandlePSTMVER_Teseo3Board)
{
   tPChar pcFieldIndex[GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE];
   memset( pcFieldIndex, 0, (sizeof (tPChar) * GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE) );
   pcFieldIndex[GNSS_PROXY_PSTMVER_OFFSET_BINIMG_VER] = "BINIMG_4.5.2.1_ARM";
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   GnssProxy_vHandlePSTMVER( (const tChar * const*) pcFieldIndex);
   EXPECT_EQ(GNSS_HW_STA8089, rGnssProxyInfo.rGnssConfigData.enGnssHwType);
   EXPECT_EQ(GNSS_PROXY_STA8089_SUPPORTED_CHANNELS, rGnssProxyInfo.rGnssConfigData.u16NumOfChannels);
   EXPECT_EQ(0x4050201, rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer);
   EXPECT_EQ(OSAL_C_U16_GNSS_SAT_USED_FOR_POSCALC, rGnssProxyInfo.rGnssConfigData.u16GnssSatStatusSupported);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandlePSTMVER_UnknownBoard)
{
   tPChar pcFieldIndex[GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE];
   memset( pcFieldIndex, 0, (sizeof (tPChar) * GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE) );
   pcFieldIndex[GNSS_PROXY_PSTMVER_OFFSET_BINIMG_VER] = "BINIMG_0.0.0.0_ARM";
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(4);
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   GnssProxy_vHandlePSTMVER( (const tChar * const*) pcFieldIndex);
   EXPECT_EQ(GNSS_HW_UNKNOWN, rGnssProxyInfo.rGnssConfigData.enGnssHwType);
   EXPECT_EQ(OSAL_C_U8_GNSS_NO_CHANNELS, rGnssProxyInfo.rGnssConfigData.u16NumOfChannels);
   EXPECT_EQ(0, rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer);
   EXPECT_EQ(0, rGnssProxyInfo.rGnssConfigData.u16GnssSatStatusSupported);
}

TEST_F(GnssParser_InitGlobals, GnssProxy_HandlePSTMVER_UnknownBoard_PostErr)
{
   tPChar pcFieldIndex[GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE];
   memset( pcFieldIndex, 0, (sizeof (tPChar) * GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE) );
   pcFieldIndex[GNSS_PROXY_PSTMVER_OFFSET_BINIMG_VER] = "BINIMG_0.0.0.0_ARM";
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(5);
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   GnssProxy_vHandlePSTMVER( (const tChar * const*) pcFieldIndex);
   EXPECT_EQ(GNSS_HW_UNKNOWN, rGnssProxyInfo.rGnssConfigData.enGnssHwType);
   EXPECT_EQ(OSAL_C_U8_GNSS_NO_CHANNELS, rGnssProxyInfo.rGnssConfigData.u16NumOfChannels);
   EXPECT_EQ(0, rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer);
   EXPECT_EQ(0, rGnssProxyInfo.rGnssConfigData.u16GnssSatStatusSupported);
}



