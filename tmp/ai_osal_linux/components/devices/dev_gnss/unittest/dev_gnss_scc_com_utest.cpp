#include "gmock/gmock.h"
#include "gtest/gtest.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if_mock.h"
#define GNSS_MOCKSTOBEREMOVED
#include "GnssMock.h"
#include "GnssMock.cpp"

#undef GnssProxy_s32GetDataFromScc
#undef GnssProxy_s32InitCommunToSCC
#undef GnssProxy_s32SendStatusCmd
#undef GnssProxy_s32SocketSetup
#undef GnssProxy_s32SendDataToScc

#include "../sources/dev_gnss_scc_com.c"

using namespace testing;
using namespace std;

int main(int argc, char** argv)
{
   ::testing::InitGoogleMock(&argc, argv);
   return RUN_ALL_TESTS();
}

class GnssScc_InitGlobals : public ::testing::Test
{
public:
   GnssMock Gnss;
   NiceMock<OsalMock> Osal_mock;
   virtual void SetUp()
   {}
};

TEST_F(GnssScc_InitGlobals,GnssProxy_GetDataFromScc_GnssShutdown)
{
   tU32 u32Bytes;
   tS32 s32RetVal;
   u32Bytes =100;
   rGnssProxyInfo.bShutdownFlag = TRUE;
   EXPECT_CALL(Gnss, poll(_,_,_)).Times(1).WillOnce(Return(1));
   s32RetVal = GnssProxy_s32GetDataFromScc(u32Bytes);
   EXPECT_EQ(-1, s32RetVal);
}

TEST_F(GnssScc_InitGlobals,GnssProxy_GetDataFromScc_PollTimeout)
{
   tU32 u32Bytes;
   tS32 s32RetVal;
   u32Bytes =100;
   rGnssProxyInfo.bShutdownFlag = FALSE;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, poll(_,_,_)).Times(10).WillRepeatedly(Return(0));
   s32RetVal = GnssProxy_s32GetDataFromScc(u32Bytes);
   EXPECT_EQ(0, s32RetVal);
}

TEST_F(GnssScc_InitGlobals,GnssProxy_GetDataFromScc_PollErr)
{
   tU32 u32Bytes;
   tS32 s32RetVal;
   u32Bytes =100;
   rGnssProxyInfo.bShutdownFlag = FALSE;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, poll(_,_,_)).Times(1).WillOnce(Return(-1));
   s32RetVal = GnssProxy_s32GetDataFromScc(u32Bytes);
   EXPECT_EQ(-1, s32RetVal);
}

TEST_F(GnssScc_InitGlobals,GnssProxy_GetDataFromScc)
{
   tU32 u32Bytes;
   tS32 s32RetVal;
   u32Bytes =GNSS_PROXY_MAX_PACKET_SIZE;
   rGnssProxyInfo.bShutdownFlag = FALSE;
   EXPECT_CALL(Gnss, poll(_,_,_)).Times(1).WillOnce(Return(1));
   EXPECT_CALL(Gnss, dgram_recv(_,_,_)).Times(1).WillOnce(Return(10));
   s32RetVal = GnssProxy_s32GetDataFromScc(u32Bytes);
   EXPECT_EQ(10, s32RetVal);
}

TEST_F(GnssScc_InitGlobals,GnssProxy_SendDataFromScc)
{
   tU32 u32Bytes;
   tS32 s32RetVal;
   u32Bytes =MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS;
   EXPECT_CALL(Gnss, dgram_send(_,_,_)).Times(1).WillOnce(Return(MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS));
   s32RetVal = GnssProxy_s32SendDataToScc(u32Bytes);
   EXPECT_EQ(MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS, s32RetVal);
}

TEST_F(GnssScc_InitGlobals,GnssProxy_SendDataFromScc_IncorrectBytesErr)
{
   tU32 u32Bytes;
   tS32 s32RetVal;
   u32Bytes =100;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, dgram_send(_,_,_)).Times(1).WillOnce(Return(-1));
   s32RetVal = GnssProxy_s32SendDataToScc(u32Bytes);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssScc_InitGlobals, GnssProxy_SendStatusCmd)
{
   tS32 s32RetVal;
   tU8 u8Status = MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS;
   tU8 u8CompVer = GNSS_PROXY_COMPONENT_VERSION;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, dgram_send(_,_,_)).Times(1).WillOnce(Return(MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS));
   s32RetVal = GnssProxy_s32SendStatusCmd(u8Status, u8CompVer);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(GnssScc_InitGlobals, GnssProxy_SendStatusCmd_Err)
{
   tS32 s32RetVal;
   tU8 u8Status = MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS;
   tU8 u8CompVer = GNSS_PROXY_COMPONENT_VERSION;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, dgram_send(_,_,_)).Times(1).WillOnce(Return(-1));
   s32RetVal = GnssProxy_s32SendStatusCmd(u8Status, u8CompVer);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssScc_InitGlobals, GnssProxy_ExchStatus)
{
   tS32 s32RetVal;
   tU8 u8Status = MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS;
   tU8 u8CompVer = GNSS_PROXY_COMPONENT_VERSION;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] = GNSS_PROXY_SCC_R_COMPONENT_STATUS_MSGID;
   rGnssProxyInfo.bShutdownFlag = FALSE;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, poll(_,_,_)).Times(1).WillOnce(Return(1));
   EXPECT_CALL(Gnss, dgram_recv(_,_,_)).Times(1).WillOnce(Return(10));
   EXPECT_CALL(Gnss, dgram_send(_,_,_)).Times(1).WillOnce(Return(MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS));
   s32RetVal = GnssProxy_s32ExchStatus(u8Status, u8CompVer);
}

TEST_F(GnssScc_InitGlobals, GnssProxy_InitCommunToSCC_DgramInitErr)
{
   tS32 s32RetVal;
   sk_dgram* dDummy = OSAL_NULL;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(1);
   EXPECT_CALL(Gnss, dgram_init(_,_,_)).Times(1).WillOnce(Return(dDummy));
   s32RetVal = GnssProxy_s32InitCommunToSCC();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssScc_InitGlobals, GnssProxy_InitCommunToSCC_ExchStatusErr)
{
   tS32 s32RetVal;
   sk_dgram* dDummy;
   dDummy = (sk_dgram*)malloc(sizeof(sk_dgram));
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   EXPECT_CALL(Gnss, dgram_send(_,_,_)).Times(1).WillOnce(Return(100));
   EXPECT_CALL(Gnss, dgram_init(_,_,_)).Times(1).WillOnce(Return(dDummy));
   s32RetVal = GnssProxy_s32InitCommunToSCC();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(GnssScc_InitGlobals, GnssProxy_InitCommunToSCC_HandleStatusErr)
{
   tS32 s32RetVal;
   sk_dgram* dDummy;
   dDummy = (sk_dgram*)malloc(sizeof(sk_dgram));
   rGnssProxyInfo.bShutdownFlag = FALSE;
   rGnssProxyInfo.bConfigrecvd = TRUE;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] = GNSS_PROXY_SCC_R_COMPONENT_STATUS_MSGID;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(2);
   EXPECT_CALL(Gnss, poll(_,_,_)).Times(1).WillOnce(Return(1));
   EXPECT_CALL(Gnss, dgram_recv(_,_,_)).Times(1).WillOnce(Return(10));
   EXPECT_CALL(Gnss, dgram_init(_,_,_)).Times(1).WillOnce(Return(dDummy));
   EXPECT_CALL(Gnss, dgram_send(_,_,_)).Times(1).WillOnce(Return(MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS));
   EXPECT_CALL(Gnss, GnssProxys32HandleStatusMsg()).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GnssProxy_s32InitCommunToSCC();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}


TEST_F(GnssScc_InitGlobals, GnssProxy_InitCommunToSCC)
{
   tS32 s32RetVal;
   sk_dgram* dDummy;
   dDummy = (sk_dgram*)malloc(sizeof(sk_dgram));
   rGnssProxyInfo.bShutdownFlag = FALSE;
   rGnssProxyInfo.bConfigrecvd = TRUE;
   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] = GNSS_PROXY_SCC_R_COMPONENT_STATUS_MSGID;
   EXPECT_CALL(Gnss, GnssProxyvTraceOut(_,_,_)).Times(3);
   EXPECT_CALL(Gnss, poll(_,_,_)).Times(1).WillOnce(Return(1));
   EXPECT_CALL(Gnss, dgram_recv(_,_,_)).Times(1).WillOnce(Return(10));
   EXPECT_CALL(Gnss, dgram_init(_,_,_)).Times(1).WillOnce(Return(dDummy));
   EXPECT_CALL(Gnss, dgram_send(_,_,_)).Times(1).WillOnce(Return(MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS));
   EXPECT_CALL(Gnss, GnssProxys32HandleStatusMsg()).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GnssProxy_s32InitCommunToSCC();
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

