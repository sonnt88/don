/*********************************************************************************************************************
* FILE         : dev_gnss.h
*
* DESCRIPTION  : This is the header file corresponding to dev_gnss.c
*
* AUTHOR(s)    : Niyatha S Rao (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |       Version        | Author & comments
*--------------|----------------------|---------------------------
* 22.APR.2013  | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
*--------------|----------------------|---------------------------
* 29.AUG.2013  | Version: 2.0         | Madhu Kiran Ramachandra (RBEI/ECF5)
*              |                      | Extended from dev_gps to dev_gnss. Made code more modular.
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef DEV_GNSS_HEADER
#define DEV_GNSS_HEADER

tS32 GpsProxy_s32IOControl(tS32 s32fun, tLong sArg);
tS32 GpsProxy_s32IORead(tPS8 pBuffer, tU32 u32maxbytes);

#endif
/*End of file*/

