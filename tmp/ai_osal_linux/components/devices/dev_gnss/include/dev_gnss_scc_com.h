/*********************************************************************************************************************
* FILE        : dev_gnss_scc_com.h
*
* DESCRIPTION : Header for dev_gnss_scc_com.c
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 28.AUG.2013 | Initial version: 0.1   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -----------------------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef GNSS_SCC_COM_HEADER
#define GNSS_SCC_COM_HEADER

tS32 GnssProxy_s32InitCommunToSCC(tVoid);
tS32 GnssProxy_s32SocketSetup( tU32 u32IncPort );
tS32 GnssProxy_s32SendDataToScc(tU32 u32Bytes);
tS32 GnssProxy_s32GetDataFromScc(tU32 u32Bytes);
tS32 GnssProxy_s32ExchStatus( tU8 u8Status, tU8 u8CompVer );
tS32 GnssProxy_s32SendStatusCmd( tU8 u8Status, tU8 u8CompVer );
#endif
