/*********************************************************************************************************************
* FILE        : dev_gnss_trace.h
*
* DESCRIPTION : Header for dev_gnss_trace.c
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 28.AUG.2013 | Initial version: 0.1   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -----------------------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef GNSS_TRACE_HEADER
#define GNSS_TRACE_HEADER


typedef enum
{
   GNSSPXY_DEF_TRC_RULE = 0,


   /********************dev_gnss_epoch.c********************/
   
   
   GNSSPXY_EPOCH_GT_MINMAX_WK = 0x0021,      //s32GetEpoch:get min max week 
   GNSSPXY_EPOCH_ST_Y_M_D = 0x0022,          //s32SetEpoch:u16Year u8Month u8Day
   GNSSPXY_EPOCH_ST_MIXMAX_WK = 0x0023,      //s32SetEpoch:set min max week
   GNSSPXY_EPOCH_ST_SND_CMD = 0x0024,        //s32SetEpoch:send command
   

   /********************dev_gnss_main.c********************/
   

   GNSSPXY_RDOPEN = 0x0040,                   //OPEN called with READ ONLY
   GNSSPXY_WROPEN = 0x0041,                   //OPEN called with WRITE ONLY
   GNSSPXY_WT_TESEO_VER = 0x0042,             //Wait for teseo version
   GNSSPXY_WT_TESEO_FWCRC = 0x0043,           //wait for teseo fw crc
   GNSSPXY_SETTING_DEF_SAT_SYS = 0x0044,
   GNSSPXY_GET_SAT_SYS = 0x0045,
   GNSSPXY_INIT_SUCC = 0x0046,                //GnssProxy s32Init SUCCESS
   GNSSPXY_IOCTRL_ENTER = 0x0047,
   GNSSPXY_WAIT_TESEO_VER = 0x0048,
   GNSSPXY_WAIT_TESEO_FWCRC = 0x0049,
   GNSSPXY_CVM_STATE = 0x004A,
   GNSSPXY_SEM_WAIT_CAL = 0x004B,
   GNSSPXY_SEM_POST_CAL = 0x004C,
   GNSSPXY_WR_SZ = 0x004D,
   GNSSPXY_DEV_CLS = 0x004E,                 //Device close ENTER
   GNSSPXY_SHTDWN_SET = 0x004F,              //Shutdown flag is set
   GNSSPXY_SCKT_THRD = 0x0050,               //In Gnss Socket Thread
   GNSSPXY_INC_MSG_RCVD = 0x0051,
   GNSSPXY_DEV_SHTDWN = 0x0052,
   GNSSPXY_SHTDWN_COMP = 0x0053,
   GNSSPXY_SHTDWN_SCK_SUC = 0x0054,          //Socket Shutdown Success
   GNSSPXY_CLS_SCK_SUC = 0x0055,             //Socket Close Success
   

   /********************dev_gnss_parser.c********************/
   

   GNSSPXY_GNSS_STATUS_SCC = 0x0070,
   GNSSPXY_GNSS_PXYVER_V850VER = 0x0071,     //proxy version 
   GNSSPXY_INC_DATA_LEN = 0x0072,
   GNSSPXY_NMEA_DATA_TS = 0x0073,
   GNSSPXY_NMEA_STRT = 0x0074,
   GNSSPXY_CHKSUM_VAL = 0x0075,
   GNSSPXY_TOKENS = 0x0076,
   GNSSPXY_FORM_FEED_IGNRD = 0x0077,
   GNSSPXY_GNSS_CFG = 0x0078,
   GNSSPXY_GNSS_UTC_DATE = 0x0079,
   GNSSPXY_PSTMCPU_FIX = 0x007A,              //pstmcpu end of fix
   GNSSPXY_RECORDID = 0x007B,
   GNSSPXY_RCVD_GPGGA = 0x007C,
   GNSSPXY_GPGGADATA_1 = 0x007D,                //GPGGA Data : Lat-Long-AltWGS84-GeoSep-DGps
   GNSSPXY_RCVD_GSA = 0x007E,
   GNSSPXY_GSA_FIX_STATUS = 0x007F,
   GNSSPXY_GSA_TOT_SAT_USED = 0x0081,         //GSA : Total Satellites used
   GNSSPXY_GSA_DIL_PREC_VAL = 0x0082,         //GSA : Dilution of Precision Values
   GNSSPXY_RCVD_GSV = 0x0083,
   GNSSPXY_GSV_MSG_DATA = 0x0084,             //GSV : NoGPGSVMsgRvcd CurrMsgNum SatellitsInView
   GNSSPXY_GSV_SAT_INFO_1 = 0x0085,             //GSV : SAT NUM-PRN-ELEV-Azim-C/No
   GNSSPXY_RCVD_GPRMC = 0x0086,
   GNSSPXY_RCVD_GPVTG = 0x0087,
   GNSSPXY_GPVTG_MSG_DATA = 0x0088,           //GPVTG : Received GPVTG MSG DATA T-H: H-S: V-N: V-E: Values
   GNSSPXY_RCVD_PSTMKFCOV = 0x0089,
   GNSSPXY_PSTMKFCOV_MSG_DATA_POS = 0x008A,       //PSTMFCOV :  Message Data COV N:E:V and POS Values and VEL Values
   GNSSPXY_RCVD_PSTMSETPAR = 0x008B,
   GNSSPXY_PSTMSETPAR_BLKID_CFG = 0x008C,     //Block Id and config
   GNSSPXY_PSTMSETPAR_EPOCH_DATA = 0x008D,    //GnssEpochMinMaxWeek
   GNSSPXY_RCVD_PSTMSETPAROK = 0x008E,
   GNSSPXY_PSTMSETPAROK_CFG_RCVD = 0x008F,
   GNSSPXY_SWITCH_DEF = 0x0090,
   GNSSPXY_RCVD_PSTMVER = 0x0091,
   GNSSPXY_PSTMVER_BIN_VER = 0x0092,          //sBinImgVer
   GNSSPXY_PSTMVER_BIN_VER2 = 0x0093,         //Bin_Image_Version2
   GNSSPXY_RCVD_PSTMCRCCHECK = 0x0094,
   GNSSPXY_PSTMCRCCHECK_CRC = 0x0095,         //CRC Value Recvd
   GNSSPXY_SATSYSEOL_EOL_VAL = 0x0096,        //eol value
   GNSSPXY_GETSATCFG_EVE_CLR = 0x0097,
   GNSSPXY_OLDNEW_TES_CFG = 0x0098,           //old and new teseo cfg
   GNSSPXY_FINAL_MSG_SZ = 0x0099,             //final message size
   GNSSPXY_SETCFG_FAIL = 0x009A,              //cfg set fail
   GNSSPXY_SETCFG_RESP_OK = 0x009B,           //cfg response ok
   GNSSPXY_REST_TES = 0x009C,
   GNSSPXY_SVCDBLK_SV_TES_CFG = 0x009D,
   GNSSPXY_RCVD_PSTMSAVEPAROK = 0x009E,
   GNSSPXY_REBGPSENG_REB_TES = 0x009F,        //reboot teseo
   GNSSPXY_SAT_SYS_STORED = 0x0101,
   GNSSPXY_BIN_VER_WT_SUC = 0x0102,
   GNSSPXY_BIN_VER_WT_FAIL = 0x0103,        // Not used
   GNSSPXY_CRCCHK_WT_SUC = 0x0104,
   GNSSPXY_RCVD_PSTMPV = 0x0105,
   GNSSPXY_PSTMPV_RCVR_VEL = 0x0106,          //Elements of Receiver Velocity
   GNSSPXY_PSTMPV_POS_COV_MAT_1 = 0x0107,       //Elements of Position covariance matrix
   GNSSPXY_PSTMPV_VEL_COV_MAT_1 = 0x0108,       //Elements of Velocity covariance matrix
   GNSSPXY_RCVD_PSTMPVQ = 0x0109,
   GNSSPXY_PSTMPVQ_POS_NS_MATRIX = 0x010A,        //Position and Velocity Noise Matrix values
   GNSSPXY_FLSHBUF_WT_SUC = 0x010B,
   GNSSPXY_PSTMPV_POS_COV_MAT_2 = 0x010C,
   GNSSPXY_PSTMPV_VEL_COV_MAT_2 = 0x010D,
   GNSSPXY_PSTMPVQ_VEL_NS_MATRIX = 0x010E,
   GNSSPXY_PSTMKFCOV_MSG_DATA_VEL = 0x010F,
   GNSSPXY_SAT_NUM_PRN = 0x0110,
   GNSSPXY_GPGGADATA_2 = 0x0111,
   GNSSPXY_GNSS_UTC_TIME = 0x0112,
   

   /********************dev_gnss_scc_com.c********************/		   

   GNSSPXY_STUB_RCV_PASS = 0x0120,           //stub recv called
   GNSSPXY_PERS_CFG = 0x0121,                //Using persistent configuration
   GNSSPXY_INIT_COM_SUC_SCC = 0x0122,        //Initial Communication Successful with SCC
   GNSSPXY_WR_PASS = 0x0123,                 //write passed for Component status
   GNSSPXY_CON_PASS = 0x0124,                //Connect passed
   GNSSPXY_SIM_CON_PASS = 0x0125,            //Connect to SCC simulation STUB passed
   GNSSPXY_TES_TD_CON_ACP = 0x0126,          //teseo thread connect on accept
   GNSSPXY_TES_TD_CMD_RCV = 0x0127,          //teseo thread command receive         
   GNSSPXY_TES_TD_WR_RTVAL = 0x0128,         //teseo thread write return value
   GNSSPXY_TES_TD_BLK = 0x0129,              //teseo thread blocking on accept
   GNSSPXY_STUB_RCVD_PASS = 0x012A,           //stub recv called
   

   /********************dev_gps_main.c********************/
   
   
   GNSSPXY_GPS_IOCTRL = 0x0140,             //IO_Ctrl Enter s32FunId:
   GNSSPXY_INV_SZ = 0x0141,                 //"Invalid Size"
   GNSSPXY_MSRMT_CPD = 0x0142,              //"Measured position copied to Data block"
   GNSSPXY_TRC_DATA_CPD = 0x0143,           //"Track data copied to Data block"
   GNSSPXY_VIS_LST_CPD = 0x0144,            //"Visibility list copied to Data block"
   GNSSPXY_EXT_DATA_CPD = 0x0145,           //"Ext Data copied to Data block"
   GNSSPXY_GNSS_REC_POPU = 0x0146,          //"Ext Data copied to Data block" 631 to be changed
   

   /********************gnss_auxclock.c********************/
   

   GNSSPXY_AUX_INIT = 0x0160,                //Aux clock INIT 
   GNSSPXY_AUX_OPEN = 0x0161,                //Aux clock Open 
   GNSSPXY_AUX_CLS = 0x0162,                 //Aux clock Close 
   GNSSPXY_AUX_READ = 0x0163,
   GNSSPXY_CORTED_AUX = 0x0164,              //corrected aux clock
   GNSSPXY_AUX_PL_READ = 0x0165,             //Aux Clock Plain Read
   GNSSPXY_AUX_TS_DIFF = 0x0166,             //Aux Clock Timestamp Difference
   GNSSPXY_AUX_CLK_INFO = 0x0167,

}enGNSSPXYTRC;

tVoid GnssProxy_vErrmemLog( const char* pu8Buffer, tS32 s32Size );

tVoid GnssProxy_vTraceOut(TR_tenTraceLevel enLevel,
      enGNSSPXYTRC enGnssPxyTrcVal,const tChar *pcFormatString,...);


#endif
