/*********************************************************************************************************************
* FILE         : dev_gnss_main.h
*
* DESCRIPTION  : This is the header file corresponding to dev_gnss_main.c
*
* AUTHOR(s)    : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |       Version        | Author & comments
*--------------|----------------------|----------------------------------------
* 30.AUG.2013  | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef DEV_GNSS_HEADER
#define DEV_GNSS_HEADER

tS32 GnssProxy_s32IOOpen( OSAL_tenAccess enAccess );
tS32 GnssProxy_s32IOClose(tVoid);
tS32 GnssProxy_s32IOControl(tS32 s32fun, tLong sArg);
tS32 GnssProxy_s32IORead(tPS8 pBuffer, tU32 u32maxbytes);
tS32 GnssProxy_s32IOWrite( (const char*) pBuffer, tU32 u32maxbytes);
#endif
/*End of file*/
