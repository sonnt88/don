/*********************************************************************************************************************
* FILE        : dev_gnss_types.h
*
* DESCRIPTION : Contains commonly accessed elements across GNSS proxy module.
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 28.AUG.2013 | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -----------------------------------------------------------------------------------------------
* 09.FEB.2015 |         version: 1.1   | Madhu Kiran Ramachandra (RBEI/ECF5)
*                                      | Modified CRC related Macros
* -----------------------------------------------------------------------------------------------
*                                      |
*********************************************************************************************************************/

/*************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#ifndef GNSS_COMMON_TYPES
#define GNSS_COMMON_TYPES

/* Adaptations needed for the driver to work with CBC in JLR-NGA are incorporated
   "KENDRICK_PEAK" macro is defined only in JLR-NGA project.
   In order to connect the driver to stub: enable KENDRICK_PEAK and
   change the SERVER_IP_ADDRESS  to "172.17.0.6". */

#if defined (GEN3X86)
#define KENDRICK_PEAK
#endif


#ifdef KENDRICK_PEAK
#define GNSS_PROXY_SERVER_IP_ADDRESS "127.0.0.1"
  /* Has to be enabled to connect to test stub */
  #define GNSS_PROXY_TEST_STUB_ACTIVE
#endif

/* Enable this if you want to send commands to Teseo from external application */
//#define GNSS_PROXY_REMOTE_TESEO_CONROL_FEATURE

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"
#include "osal_public.h"


#ifndef GNSS_PROXY_TEST_STUB_ACTIVE
#include "dgram_service.h"
#endif


/*************************************************************************
* Macro declaration (scope: Global)
*-----------------------------------------------------------------------*/
/* Retry count for Init of GNSS */
#define GNSS_PROXY_INIT_RETRY_CNT (3)
#define GNSS_PROXY_MAX_INCORRECT_STS_RES (3)

#define GNSS_PROXY_RETRY_INTERVALL_MS (100)

/* This thread reads from socket, segregate data
*  and buffer the data till VD-Sensor reads it.
*  Pretty high traffic is expected through socket.
*  So performance may improve to if this thread is of highest priority */

#define GNSS_PROXY_SOCKET_READ_THREAD_NAME       ("GnssPrxyReadThread")
#define GNSS_PROXY_SOCKET_READ_THREAD_PRIORITY   (OSAL_C_U32_THREAD_PRIORITY_HIGHEST)
#define GNSS_PROXY_SOCKET_READ_THREAD_STACKSIZE  (2048)

/* This thread listens to INC GNSS FW update port */
#define GNSS_PROXY_FW_UPDATE_THREAD_NAME       ("GnssPrxyFwUpThrd")
#define GNSS_PROXY_FW_UPDATE_THREAD_PRIORITY   (OSAL_C_U32_THREAD_PRIORITY_NORMAL)
#define GNSS_PROXY_FW_UPDATE_THREAD_STACKSIZE  (2048)

/* Macros related to status messages */
#define GNSS_PROXY_COMPONENT_STATUS_UNKNOWN      (0x00)
#define GNSS_PROXY_COMPONENT_STATUS_ACTIVE       (0x01)
#define GNSS_PROXY_COMPONENT_STATUS_NOT_ACTIVE   (0x02)

/* First version referred to as 1
 * Version update: GNSS support 2 */
#define GNSS_PROXY_COMPONENT_VERSION       (0x01)
/* GNSS proxy Firmware update component version */
#define GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_ONE       (0x01)
#define GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_TWO       (0x02)
#define GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_THREE     (0x03)

/* size of message ID field in all messages */
#define GNSS_PROXY_MSGID_SIZE    (1)
/*Sizes of the messages and their individual fields
  from SCC GNSS component.*/

/* Status message size */
#define MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS    ( GNSS_PROXY_MSGID_SIZE + 2)
#define MSG_SIZE_SCC_GNSS_R_COMPONENT_STATUS    ( GNSS_PROXY_MSGID_SIZE + 2)
/* Reject Message size */
#define MSG_SIZE_SCC_GNSS_R_REJECT              ( GNSS_PROXY_MSGID_SIZE + 2)

/* GNSS configuration service type */
#define GNSS_PROXY_SERVICE_TYPE_CONFIG_RQ            (0x00)
#define GNSS_PROXY_SERVICE_TYPE_HW_RESET             (0x10)
#define GNSS_FLUSH_SENSOR_DATA_REQ                   (0x11)
#define GNSS_PROXY_SERVICE_TYPE_FLASH_BEGIN          (0x20)
#define GNSS_PROXY_SERVICE_TYPE_FLASH_DATA           (0x21)
#define GNSS_PROXY_SERVICE_TYPE_FLASH_END            (0x2F)
#define GNSS_PROXY_SERVICE_TYPE_DIAG_INFO_RQ         (0x80)
#define GNSS_PROXY_SERVICE_TYPE_DIAG_RUN_SELF_TEST   (0x90)
#define GNSS_PROXY_SERVICE_TYPE_UNKNOWN_ERROR        (0xF0)
#define GNSS_PROXY_SERVICE_TYPE_BUFFER_OVERFLOW_ERR  (0xF1)

/*Offset of fields in message from SCC*/
#define GNSS_PROXY_OFFSET_MSG_ID                (0)
/* Status message offsets */
#define GNSS_PROXY_OFFSET_COMPONENT_STATUS      (1)
#define GNSS_PROXY_OFFSET_COMPONENT_VERSION     (2)
/* Control message offsets */
#define GNSS_PROXY_OFFSET_CONFIG_SERVICE_TYPE   (1)
#define GNSS_PROXY_OFFSET_CONFIG_DATA_INTERVAL  (2)
#define GNSS_PROXY_OFFSET_CONFIG_HW_TYPE        (4)
#define GNSS_PROXY_OFFSET_CFG_FLUSH_BUFF_RES    (2)

/* Reject message offsets */
#define GNSS_PROXY_OFFSET_REJECT_REASON         (1)
#define GNSS_PROXY_OFFSET_REJECTED_MSG_ID       (2)
/* GNSS data message offsets */
#define GNSS_PROXY_OFFSET_TIMESTAMP_DATA_MSG    (1)
#define GNSS_PROXY_OFFSET_START_DATA            (5)

/* Size of each field type  */
/* In control message */
#define GNSS_PROXY_FIELD_SIZE_CONFIG_DATA_INTERVAL  (2)
#define GNSS_PROXY_FIELD_SIZE_CONFIG_HW_TYPE        (1)
/* In data message */
#define GNSS_PROXY_FIELD_SIZE_DATA_TIME_STAMP  (4)

/* This is the maximum size of the message expected from SCC.
If SCC tries to send a message bigger that this, GNSS proxy driver will crash*/
#define GNSS_PROXY_MAX_PACKET_SIZE                               (8192)
#define GNSS_PROXY_TRANSMIT_BUFF_SIZE                            (1024)
/* Retry count in case of read failure. If read on socket
   fails for some reason, we will retry for these many times*/
#define GNSS_PROXY_SOCKET_READ_FAILURE_COUNT                   (3)

/*Message ID's derived from INC PDU table */
#define GNSS_PROXY_SCC_C_COMPONENT_STATUS_MSGID   (tU8)(0x20)
#define GNSS_PROXY_SCC_R_COMPONENT_STATUS_MSGID   (tU8)(0x21)
#define GNSS_PROXY_SCC_R_REJECT_MSGID             (tU8)(0x0B)
#define GNSS_PROXY_SCC_C_CONFIG_MSGID             (tU8)(0x30)
#define GNSS_PROXY_SCC_R_CONFIG_START_MSGID       (tU8)(0x31)
#define GNSS_PROXY_SCC_C_DATA_MSGID               (tU8)(0x40)
#define GNSS_PROXY_SCC_R_DATA_START_MSGID         (tU8)(0x41)

#define GNSS_PROXY_FW_SCC_C_STATUS_MSGID          (tU8)(0x20)
#define GNSS_PROXY_FW_SCC_R_STATUS_MSGID          (tU8)(0x21)
#define GNSS_PROXY_FW_SCC_R_REJECT_MSGID          (tU8)(0x0B)
#define GNSS_PROXY_FW_SCC_C_CONTROL_MSGID         (tU8)(0x30)
#define GNSS_PROXY_FW_SCC_R_CONTROL_MSGID         (tU8)(0x31)

#define GNSS_PROXY_FW_OFFSET_C_CONTROL_SEQ_CNTR    (tU8)(1)
#define GNSS_PROXY_FW_OFFSET_C_CONTROL_MSG_ID      (tU8)(1)
#define GNSS_PROXY_FW_OFFSET_C_CONTROL_SRV_TYPE    (tU8)(2)
#define GNSS_PROXY_FW_OFFSET_C_CONTROL_DATA        (tU8)(3)

#define GNSS_PROXY_FW_OFFSET_R_CONFIG_SERVICE_TYPE      (1)
#define GNSS_PROXY_FW_OFFSET_R_CONFIG_SERVICE_DATA      (2)
#define GNSS_PROXY_FW_OFFSET_R_CONFIG_FLASH_BUFF_SIZE   (3)

#define GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES          (2)

#define GNSS_PROXY_FLUSH_BUFF_RES_OK                    (0x00)


#define GNSS_PROXY_FW_RES_SERVICE_DATA_OK    (0)
#define GNSS_PROXY_FW_RES_SERVICE_UNKNOWN_ERROR (0x01)
#define GNSS_PROXY_FW_RES_SERVICE_BUFFER_OVERFLOW  (0x02)

#define GNSS_PROXY_FW_CONTROL_MSG_HDR_SIZE     (3)
#define GNSS_PROXY_FW_SIZE_OF_HOST_READY_FIELD     (1)
#define GNSS_PROXY_FW_CONTROL_FLASH_END_SIZE   (GNSS_PROXY_FW_CONTROL_MSG_HDR_SIZE)
#define GNSS_PROXY_FW_SIZE_OF_FLASHER_READY_FIELD     (1)

#define GNSS_PROXY_FW_FIELD_SIZE_FLASH_BUFF  (2)

#define GNSS_PROXY_FW_ACK_WAIT_CHUNK_SIZE_TESEO_2 ((1024)*(16) ) /* 16 KB of data */
#define GNSS_PROXY_FW_ACK_WAIT_CHUNK_SIZE_TESEO_3 ((1024)*(5) )   /* 05 KB of data */

//#define GNSS_PROXY_FW_TESEO_ACK_WAIT_INTERVALL (30 ) /* Only for testing */

#define GNSS_PROXY_FW_CMD_HOST_READY_TESEO_2       (0x4A)
#define GNSS_PROXY_FW_CMD_HOST_READY_TESEO_3       (0x5A)
#define GNSS_PROXY_FW_CMD_FLASHER_READY_TESEO_3    (0x4A)

#define GNSS_PROXY_FW_CMD_ACK_TESEO             (0xCC)
#define GNSS_PROXY_FW_CMD_NACK_TESEO            (0xE6)

#define GNSS_PROXY_INC_TP_MARKER_HEADER            (tU8)(0x00)

#define GNSS_PROXY_INC_CNTL_MSG_SRV_FLASH_BEGIN    (tU8)(0x20)
#define GNSS_PROXY_INC_CNTL_MSG_SRV_FLASH_DATA     (tU8)(0x21)
#define GNSS_PROXY_INC_CNTL_MSG_SRV_GNSS_CHIP_RES  (tU8)(0x22)
#define GNSS_PROXY_INC_CNTL_MSG_SRV_FLASH_BUF_SIZE (tU8)(0x23)
#define GNSS_PROXY_INC_CNTL_MSG_SRV_FLASH_END      (tU8)(0x2F)
#define GNSS_PROXY_INC_CNTL_MSG_SRV_UNKNOWN_ERROR  (tU8)(0xF0)

/* This is the only supported combination of field specifiers. */
#define GNSS_PROXY_DEFAULT_FIELD_SPECS  (OSAL_C_S32_GPS_FIELD_SPEC_TIME_UTC |   \
                                         OSAL_C_S32_GPS_FIELD_SPEC_POS_LLA |    \
                                         OSAL_C_S32_GPS_FIELD_SPEC_VEL_NED)

/* By default, all supported field types are sent to the client. */
#define GNSS_PROXY_DEFAULT_FIELD_TYPES (0xffffffff)

#define GNSS_PROXY_MSG_TYPE_GPRMC             "$GPRMC"
#define GNSS_PROXY_MSG_TYPE_GPGGA             "$GPGGA"
#define GNSS_PROXY_MSG_TYPE_GSA               "GSA"
#define GNSS_PROXY_MSG_TYPE_GPGSA             "$GPGSA"
#define GNSS_PROXY_MSG_TYPE_GLGSA             "$GLGSA"
#define GNSS_PROXY_MSG_TYPE_QZGSA             "$QZGSA"
#define GNSS_PROXY_MSG_TYPE_GNGSA             "$GNGSA"
#define GNSS_PROXY_MSG_TYPE_GSV               "GSV"
#define GNSS_PROXY_MSG_TYPE_GPGSV             "$GPGSV"
#define GNSS_PROXY_MSG_TYPE_GLGSV             "$GLGSV"
#define GNSS_PROXY_MSG_TYPE_QZGSV             "$QZGSV"
#define GNSS_PROXY_MSG_TYPE_GNGSV             "$GNGSV"
#define GNSS_PROXY_MSG_TYPE_GPVTG             "$GPVTG"
#define GNSS_PROXY_MSG_TYPE_PSTMKFCOV         "$PSTMKFCOV"
#define GNSS_PROXY_MSG_TYPE_PSTMSETPAR        "$PSTMSETPAR"
#define GNSS_PROXY_MSG_TYPE_PSTMSETPAR_OK     "$PSTMSETPAROK"
#define GNSS_PROXY_MSG_TYPE_PSTMSETPAR_ERROR  "$PSTMSETPARERROR"
#define GNSS_PROXY_MSG_TYPE_PSTMVER           "$PSTMVER"
#define GNSS_PROXY_MSG_TYPE_PSTMCPU           "$PSTMCPU"
#define GNSS_PROXY_MSG_TYPE_PSTMPV            "$PSTMPV"
#define GNSS_PROXY_MSG_TYPE_PSTMPVQ           "$PSTMPVQ"
#define GNSS_PROXY_MSG_TYPE_PSTMCRCCHECK      "$PSTMCRCCHECK"
#define GNSS_PROXY_MSG_TYPE_PSTMSAVEPAROK     "$PSTMSAVEPAROK"
#define GNSS_PROXY_MSG_TYPE_GPTXT             "$GPTXT"

#define GNSS_PROXY_TESEO_BIN_IMAGE_LIB        "BINIMG"
/* Used as a marker for invalid records. */
#define GNSS_PROXY_INVALID_RECORD_ID  (0xffffffff)
/* This is used to index NMEA message. */
#define GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE  (80)
/* This is used to extract time/date from NMEA message */
#define GNSS_PROXY_INDEX_UNIT_PLACE        (0)
#define GNSS_PROXY_INDEX_TENTH_PLACE       (1)
#define GNSS_PROXY_INDEX_HUNDREDTH_PLACE   (2)
#define GNSS_PROXY_INDEX_THOUSANDTH_PLACE  (3)
#define GNSS_PROXY_ARRAY_SIZE              (4)
/* Offsets to date received in NMEA message:
    (DD)(MM)(YY)
    (01)(23)(45) */
#define GNSS_PROXY_OFFSET_DATE_UNITS_PLACE   (0)
#define GNSS_PROXY_OFFSET_DATE_TENTH_PLACE   (1)
#define GNSS_PROXY_OFFSET_MONTH_UNITS_PLACE  (2)
#define GNSS_PROXY_OFFSET_MONTH_TENTH_PLACE  (3)
#define GNSS_PROXY_OFFSET_YEAR_UNITS_PLACE   (4)
#define GNSS_PROXY_OFFSET_YEAR_TENTH_PLACE   (5)

/* Offsets to time received in NMEA message:
    (HH)(MM)(XX)(.)(mmm)
    (01)(23)(45)(6)(789) */
#define GNSS_PROXY_OFFSET_HOUR_UNITS_PLACE               (0)
#define GNSS_PROXY_OFFSET_HOUR_TENTH_PLACE               (1)
#define GNSS_PROXY_OFFSET_MINUTE_UNITS_PLACE             (2)
#define GNSS_PROXY_OFFSET_MINUTE_TENTH_PLACE             (3)
#define GNSS_PROXY_OFFSET_SECONDS_UNITS_PLACE            (4)
#define GNSS_PROXY_OFFSET_SECONDS_TENTH_PLACE            (5)
#define GNSS_PROXY_OFFSET_MILLISECONDS_UNITS_PLACE       (7)
#define GNSS_PROXY_OFFSET_MILLISECONDS_TENTH_PLACE       (8)
#define GNSS_PROXY_OFFSET_MILLISECONDS_HUNDRENDTH_PLACE  (9)
/* These macros are used to release OS resources */
#define GNSS_PROXY_RESOURCE_RELSEASE_SOCKET       (1)
#define GNSS_PROXY_RESOURCE_RELEASE_SEMAPHORE     (2)
#define GNSS_PROXY_RESOURCE_RELEASE_READ_EVENT    (3)
#define GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT (4)
#define GNSS_PROXY_RESOURCE_RELEASE_INIT_EVENT    (5)

/*  Name of the critical section semaphore. */
#define GNSS_PROXY_SEMAPHORE_NAME         ((tCString)"GNSSPRXYSM")
/* Read Wait Event */
#define GNSS_PROXY_READ_WAIT_EVENT_NAME       ((tCString)"GnssPxyRdEv")
#define GNSS_PROXY_EVENT_DATA_READY           ((OSAL_tEventMask)0x01)
#define GNSS_PROXY_EVENT_SHUTDOWN_READ_EVENT  ((OSAL_tEventMask)0x02)

/* Read Wait Event */
#define GNSS_PROXY_INIT_EVENT_NAME       ((tCString)"GnssPxyInEv")
#define GNSS_PROXY_EVENT_INIT_SUCCESS    ((OSAL_tEventMask)0x01)
#define GNSS_PROXY_EVENT_INIT_FAILED     ((OSAL_tEventMask)0x02)
#define GNSS_PROXY_EVENT_INIT_THRD_EXT   ((OSAL_tEventMask)0x04) 
#define GNSS_PROXY_EVENT_INIT_WAIT_MASK  (GNSS_PROXY_EVENT_INIT_THRD_EXT | \
                                          GNSS_PROXY_EVENT_INIT_FAILED  | \
                                          GNSS_PROXY_EVENT_INIT_SUCCESS    )

#define GNSS_PROXY_INIT_EVENT_WAIT_TIME  ((OSAL_tMSecond)10000) /*  10 seconds */
#define GNSS_PROXY_INIT_THRD_EXT_EVENT_WAIT_TIME ((OSAL_tMSecond)500)

#define GNSS_PROXY_TESEO_COM_WAIT_EVENT_NAME     ((tCString)"GnssPxyTeseoComEv")

#define GNSS_PROXY_SAT_SYS_CONF_BLK200_REQ_WAIT_EVENT     ((OSAL_tEventMask)0x0001)
#define GNSS_PROXY_SAT_SYS_APP_BLK_200_SET_SUCCESS_EVENT  ((OSAL_tEventMask)0x0002)
#define GNSS_PROXY_SAT_SYS_CONF_SET_RES_FAILURE_EVENT     ((OSAL_tEventMask)0x0004)
#define GNSS_PROXY_TESEO_FIRMWARE_VERSION_WAIT_EVENT      ((OSAL_tEventMask)0x0008)
#define GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT         ((OSAL_tEventMask)0x0010)
#define GNSS_PROXY_TESEO_CRC_CHECK_WAIT_EVENT             ((OSAL_tEventMask)0x0020)
#define GNSS_PROXY_TESEO_COM_GET_EPOCH_RESPONSE_MASK      ((OSAL_tEventMask)0x0040)
#define GNSS_PROXY_TESEO_COM_SET_EPOCH_RESPONSE_MASK      ((OSAL_tEventMask)0x0080)
#define GNSS_PROXY_TESEO_COM_SAVE_CDB_RESPONSE_MASK       ((OSAL_tEventMask)0x0100)
#define GNSS_PROXY_FLUSH_SENSOR_BUFF_SUCCESS_EVENT        ((OSAL_tEventMask)0x0200)
#define GNSS_PROXY_FLUSH_SENSOR_BUFF_FAILURE_EVENT        ((OSAL_tEventMask)0x0400)
#define GNSS_PROXY_SAT_SYS_CONF_BLK227_REQ_WAIT_EVENT     ((OSAL_tEventMask)0x0800)
#define GNSS_PROXY_SAT_SYS_APP_BLK_227_SET_SUCCESS_EVENT  ((OSAL_tEventMask)0x1000)

// Event Wait times
#define GNSS_PROXY_GNSS_EPOCH_RESPONSE_RECEIVE_WAIT_TIME       ((OSAL_tMSecond)5000)
#define GNSS_PROXY_TESEO_RESPONSE_WAIT_TIME ((OSAL_tMSecond)3000)
#define GNSS_PROXY_FLUSH_SENSOR_BUFFER_WAIT_TIME ((OSAL_tMSecond)5000)


#define GNSS_PROXY_FW_FLASH_BEGIN_WAIT_TIME   ((OSAL_tMSecond)10000)
#define GNSS_PROXY_FW_FLASH_BUF_WAIT_TIME     ((OSAL_tMSecond)1500)
#define GNSS_PROXY_FW_EVENT_CONSUME_WAIT_TIME ((OSAL_tMSecond)2000)
#define GNSS_PROXY_FW_FLASH_END_WAIT_TIME     ((OSAL_tMSecond)10000)

/* This is the maximum time for which Teseo Waits for Teseo responses */
#define GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME         (30000)
#define GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME  (10000)
#define GNSS_PROXY_FW_TESEO_3_UART_SYNC_WAIT_TIME  (100)
#define GNSS_PROXY_FW_TESEO_NMEA_WAIT_TIME        (10000)

#define GNSS_PROXY_FW_UPDATE_EVENT_NAME         ((tCString)"GNSSPRXYFWUPEV")
#define GNSS_PROXY_FW_UPDATE_INT_EVENT_NAME     ((tCString)"GNSPXFWIN")

#define GNSS_PROXY_FW_GNSS_CHIP_RESET_OK                    ((OSAL_tEventMask)0x00000001)
#define GNSS_PROXY_FW_GNSS_CHIP_RESET_ERROR                 ((OSAL_tEventMask)0x00000002)
#define GNSS_PROXY_FW_FLASH_BEGIN_OK                        ((OSAL_tEventMask)0x00000004)
#define GNSS_PROXY_FW_FLASH_BEGIN_ERROR                     ((OSAL_tEventMask)0x00000008)
#define GNSS_PROXY_FW_FLASH_DATA_OK                         ((OSAL_tEventMask)0x00000010)
#define GNSS_PROXY_FW_FLASH_DATA_ERROR                      ((OSAL_tEventMask)0x00000020)
#define GNSS_PROXY_FW_FLASH_BUF_SIZE_OK                     ((OSAL_tEventMask)0x00000040)
#define GNSS_PROXY_FW_FLASH_BUF_SIZE_ERROR                  ((OSAL_tEventMask)0x00000080)
#define GNSS_PROXY_FW_UNKNOWN_ERROR                         ((OSAL_tEventMask)0x00000100)
#define GNSS_PROXY_FW_TESEO_ACK                             ((OSAL_tEventMask)0x00000200)
#define GNSS_PROXY_FW_TESEO_NACK                            ((OSAL_tEventMask)0x00000400)
#define GNSS_PROXY_FW_INIT_SUCCESS                          ((OSAL_tEventMask)0x00000800)
#define GNSS_PROXY_FW_INIT_FAILED                           ((OSAL_tEventMask)0x00001000)
#define GNSS_PROXY_FW_FLASH_END_OK                          ((OSAL_tEventMask)0x00002000)
#define GNSS_PROXY_FW_FLASH_END_ERROR                       ((OSAL_tEventMask)0x00004000)
#define GNSS_PROXY_FW_T3_FLASHER_SYNC_WORD                  ((OSAL_tEventMask)0x00008000)
#define GNSS_PROXY_FW_TESEO_CRC_CHECK_WAIT_EVENT            ((OSAL_tEventMask)0x00010000)
#define GNSS_PROXY_FW_TESEO_NMEA_LIST_1_CFG_BLK_WAIT_EVENT  ((OSAL_tEventMask)0x00020000)
#define GNSS_PROXY_FW_TESEO_NMEA_LIST_2_CFG_BLK_WAIT_EVENT  ((OSAL_tEventMask)0x00040000)
#define GNSS_PROXY_FW_TESEO_FLASH_PRO_CFG_BLK_WAIT_EVENT    ((OSAL_tEventMask)0x00080000)
#define GNSS_PROXY_FW_TESEO_COM_SAVE_CDB_RESPONSE_EVENT     ((OSAL_tEventMask)0x00100000)
#define GNSS_PROXY_FW_TESEO_BOOT_MSG_GPTXT_WAIT_EVENT       ((OSAL_tEventMask)0x00200000)
#define GNSS_PROXY_FW_TESEO_FIRMWARE_VERSION_WAIT_EVENT     ((OSAL_tEventMask)0x00400000)


#define GNSS_PROXY_MSG_SIZE_FLASH_BEGIN_RES (3)
#define GNSS_PROXY_MSG_SIZE_FLASH_BUFF_RES  (5)
#define GNSS_PROXY_MSG_SIZE_FLASH_DATA_RES  (3)
#define GNSS_PROXY_MSG_SIZE_FLASH_END_RES (3)

#define GNSS_PROXY_FW_EVENT_MASK_ALL  (GNSS_PROXY_FW_GNSS_CHIP_RESET_OK                      | \
                                       GNSS_PROXY_FW_GNSS_CHIP_RESET_ERROR                   | \
                                       GNSS_PROXY_FW_FLASH_BEGIN_OK                          | \
                                       GNSS_PROXY_FW_FLASH_BEGIN_ERROR                       | \
                                       GNSS_PROXY_FW_FLASH_DATA_OK                           | \
                                       GNSS_PROXY_FW_FLASH_DATA_ERROR                        | \
                                       GNSS_PROXY_FW_FLASH_BUF_SIZE_OK                       | \
                                       GNSS_PROXY_FW_FLASH_BUF_SIZE_ERROR                    | \
                                       GNSS_PROXY_FW_UNKNOWN_ERROR                           | \
                                       GNSS_PROXY_FW_TESEO_ACK                               | \
                                       GNSS_PROXY_FW_TESEO_NACK                              | \
                                       GNSS_PROXY_FW_FLASH_END_OK                            | \
                                       GNSS_PROXY_FW_FLASH_END_ERROR                         | \
                                       GNSS_PROXY_FW_T3_FLASHER_SYNC_WORD                    | \
                                       GNSS_PROXY_FW_TESEO_CRC_CHECK_WAIT_EVENT              | \
                                       GNSS_PROXY_FW_TESEO_NMEA_LIST_1_CFG_BLK_WAIT_EVENT    | \
                                       GNSS_PROXY_FW_TESEO_NMEA_LIST_2_CFG_BLK_WAIT_EVENT    | \
                                       GNSS_PROXY_FW_TESEO_FLASH_PRO_CFG_BLK_WAIT_EVENT      | \
                                       GNSS_PROXY_FW_TESEO_COM_SAVE_CDB_RESPONSE_EVENT       | \
                                       GNSS_PROXY_FW_TESEO_BOOT_MSG_GPTXT_WAIT_EVENT         | \
                                       GNSS_PROXY_FW_TESEO_FIRMWARE_VERSION_WAIT_EVENT         \
                                       ) 

/* These are default options for ST configuration on GEN3 */
#define GNSS_PROXY_FW_TARGET_FLASH_DEVICE_SQI              (1)
#define GNSS_PROXY_FW_TESEO_NVM_FLASH_OFFSET               (0x00100000)
#define GNSS_PROXY_FW_TESEO_NVM_FLASH_ERASE_SIZE           (0x00100000)
#define GNSS_PROXY_FW_TESEO_FLASH_DST_ADDRESS_TESEO_2      (0x30000000)
#define GNSS_PROXY_FW_TESEO_FLASH_DST_ADDRESS_TESEO_3      (0x10000000)

#define GNSS_PROXY_FW_TESEO_3_FLASHER_IDENTIFIER_WORD ((tU32) 0x215D1A40)
#define GNSS_PROXY_FW_TESEO_3_FLASHER_SYNC_WORD       ((tU32) 0x83984073)
#define GNSS_PROXY_FW_TESEO_3_XLOADER_IDENTIFIER_MSP  ((tU32) 0xBCD501F4)
#define GNSS_PROXY_FW_TESEO_3_XLOADER_IDENTIFIER_LSP  ((tU32) 0x83984073)
#define GNSS_PROXY_FW_TESEO_3_XLOADER_OPTIONS                 ((tU32) 0x00FF0104)
#define GNSS_PROXY_FW_TESEO_3_XLOADER_DEFAULT_DESTINATION_ADDRESS  ((tU32) 0x00000000)
#define GNSS_PROXY_FW_TESEO_3_XLOADER_DEFAULT_ENTRY_POINT          ((tU32) 0x00000000)


#define GNSS_PROXY_FW_FLASHER_SYNC_RETRY_COUNT (1000)

// TODO: check if these events are used ?
//#define GNSS_PROXY_SAT_SYS_CONF_SET_RES_FAILURE_EVENT     ((OSAL_tEventMask)0x04)
//#define GNSS_PROXY_TESEO_FIRMWARE_VERSION_WAIT_EVENT      ((OSAL_tEventMask)0x08)
//#define GNSS_PROXY_EVENT_SHUTDOWN_SAT_SYS_EVENT           ((OSAL_tEventMask)0x10)

/* ASCII values. */
#define GNSS_PROXY_ASCII_VALUE_ZERO       ('0')
#define GNSS_PROXY_ASCII_VALUE_ONE        ('1')
#define GNSS_PROXY_ASCII_VALUE_TWO        ('2')
#define GNSS_PROXY_ASCII_VALUE_THREE      ('3')
#define GNSS_PROXY_ASCII_VALUE_FORM_FEED  (0x0C)
#define GNSS_PROXY_ASCII_VALUE_DOLLAR     ('$')
#define GNSS_PROXY_ASCII_VALUE_CARRIAGE_RETURN  (0x0D)
#define GNSS_PROXY_ASCII_VALUE_LINE_FEED  (0x0A)
#define GNSS_PROXY_ASCII_VALUE_S          ('S')
#define GNSS_PROXY_ASCII_VALUE_W          ('W')
#define GNSS_PROXY_ASCII_VALUE_A          ('A')
#define GNSS_PROXY_NMEA_CHECKSUM_DELIMITER_ASTERISK  ('*')
#define GNSS_PROXY_NMEA_FIELD_DELIMITER_COMMA        (',')

/* Macros for some single character field type in NMEA messages */
#define GNSS_PROXY_INDEX_TO_MSG_TYPE  (0)

/* Differential GPS solution type */
#define GNSS_PROXY_GPGGA_SOL_TYPE_DGPS    GNSS_PROXY_ASCII_VALUE_TWO
/* Fix type 2D/3D/No fix */
#define GNSS_PROXY_GPGSA_FIX_TYPE_NO_FIX   GNSS_PROXY_ASCII_VALUE_ONE
#define GNSS_PROXY_GPGSA_FIX_TYPE_2D       GNSS_PROXY_ASCII_VALUE_TWO
#define GNSS_PROXY_GPGSA_FIX_TYPE_3D       GNSS_PROXY_ASCII_VALUE_THREE
/* Directions for latitude and longitude*/
#define GNSS_PROXY_GPGGA_LAT_DIR_SOUTH    GNSS_PROXY_ASCII_VALUE_S
#define GNSS_PROXY_GPGGA_LON_DIR_WEST     GNSS_PROXY_ASCII_VALUE_W

/* CRC Evaluation Result */
#define GNSS_PROXY_PSTMCRCCHECK_EVAL_RESULT_PASS     GNSS_PROXY_ASCII_VALUE_ONE
#define GNSS_PROXY_PSTMCRCCHECK_EVAL_RESULT_FAIL     GNSS_PROXY_ASCII_VALUE_ZERO

/* GPRMC Time Validity Message */
#define GNSS_PROXY_GPRMC_TIME_VALID     GNSS_PROXY_ASCII_VALUE_A

/* Offsets to all fields in GPGGA message */
#define GNSS_PROXY_GPGGA_OFFSET_LATITUDE            (2)
#define GNSS_PROXY_GPGGA_OFFSET_LATITUDE_DIRECTION  (3)
#define GNSS_PROXY_GPGGA_OFFSET_LONGITUDE           (4)
#define GNSS_PROXY_GPGGA_OFFSET_LONGITUDE_DIRECTION (5)
#define GNSS_PROXY_GPGGA_OFFSET_MSL_ALTUTUDE            (9)
#define GNSS_PROXY_GPGGA_OFFSET_GEOSEP              (11)
#define GNSS_PROXY_GPGGA_OFFSET_DGPS_SOL            (6)
/* Offsets to all fields in GPGSA message */
#define GNSS_PROXY_GPGSA_OFFSET_FIX_TYPE       (2)
#define GNSS_PROXY_GPGSA_OFFSET_FIRST_SAT_ID   (3)
#define GNSS_PROXY_GPGSA_OFFSET_PDOP           (15)
#define GNSS_PROXY_GPGSA_OFFSET_HDOP           (16)
#define GNSS_PROXY_GPGSA_OFFSET_VDOP           (17)
#define GNSS_PROXY_GPGSA_SATS_PER_MSG          (12)
/* Offsets to all fields in GPRMC message */
#define GNSS_PROXY_GPRMC_OFFSET_UTC_TIME (1)
#define GNSS_PROXY_GPRMC_OFFSET_UTC_TIME_VALIDIY (2)
#define GNSS_PROXY_GPRMC_OFFSET_UTC_DATE (9)
/* Offsets to all fields in GPGSV message */
#define GNSS_PROXY_GPGSV_OFFSET_EXPECTED_MSGS (1)
#define GNSS_PROXY_GPGSV_OFFSET_NUM_GPS_SAT (3)
#define GNSS_PROXY_GPGSV_OFFSET_CURR_MSG_NUM (2)
#define GNSS_PROXY_GPGSV_OFFSET_BASE_SAT_PRN_NUM (4)
#define GNSS_PROXY_GPGSV_OFFSET_BASE_SAT_ELEVATION (5)
#define GNSS_PROXY_GPGSV_OFFSET_BASE_SAT_AZIMUTHAL (6)
#define GNSS_PROXY_GPGSV_OFFSET_BASE_SAT_CARR_NOISE_RATIO (7)
#define GNSS_PROXY_NUM_SATS_IN_ONE_GPGSV (4)

/* Offsets to all fields in GPVTG message */
#define GNSS_PROXY_GPVTG_OFFSET_TRUE_HEADING_ANGLE (1)
#define GNSS_PROXY_GPVTG_OFFSET_HORIZONTAL_SPEED (7)
/* Offsets to all fields in PSTMKFCOV message */
#define GNSS_PROXY_PSTMKFCOV_OFFSET_POS_COV_NORTH    (2)
#define GNSS_PROXY_PSTMKFCOV_OFFSET_POS_COV_EAST     (3)
#define GNSS_PROXY_PSTMKFCOV_OFFSET_POS_COV_VERTICAL (4)
#define GNSS_PROXY_PSTMKFCOV_OFFSET_VEL_COV_NORTH    (6)
#define GNSS_PROXY_PSTMKFCOV_OFFSET_VEL_COV_EAST     (7)
#define GNSS_PROXY_PSTMKFCOV_OFFSET_VEL_COV_VERTICAL (8)
/* offsets for $PSTMSETPAR */
#define GNSS_PROXY_PSTMSETPAR_OFFSET_CONF_BLK_ID (1)
#define GNSS_PROXY_PSTMSETPAR_OFFSET_CONF_DATA   (2)
/* offsets for $PSTMSETPAROK */
#define GNSS_PROXY_PSTMSETPAROK_OFFSET_CONF_BLK_ID (1)
/* offsets for $PSTMVER */
#define GNSS_PROXY_PSTMVER_OFFSET_BINIMG_VER (1)

/* Offsets to all fields in PSTMPV message */
#define GNSS_PROXY_PSTMPV_OFFSET_VEL_NORTH     (8)
#define GNSS_PROXY_PSTMPV_OFFSET_VEL_EAST      (9)
#define GNSS_PROXY_PSTMPV_OFFSET_VEL_VERTICAL  (10)

#define GNSS_PROXY_PSTMPV_OFFSET_POS_COV_NORTH          (11)
#define GNSS_PROXY_PSTMPV_OFFSET_POS_COV_NORTH_EAST     (12)
#define GNSS_PROXY_PSTMPV_OFFSET_POS_COV_NORTH_VERTICAL (13)
#define GNSS_PROXY_PSTMPV_OFFSET_POS_COV_EAST           (14)
#define GNSS_PROXY_PSTMPV_OFFSET_POS_COV_EAST_VERTICAL  (15)
#define GNSS_PROXY_PSTMPV_OFFSET_POS_COV_VERTICAL       (16)

#define GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_NORTH          (17)
#define GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_NORTH_EAST     (18)
#define GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_NORTH_VERTICAL (19)
#define GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_EAST           (20)
#define GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_EAST_VERTICAL  (21)
#define GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_VERTICAL       (22)

/* Offsets to all fields in PSTMPVQ message */
#define GNSS_PROXY_PSTMPVQ_OFFSET_P_Q_N         (1)
#define GNSS_PROXY_PSTMPVQ_OFFSET_P_Q_E         (2)
#define GNSS_PROXY_PSTMPVQ_OFFSET_P_Q_V         (3)
#define GNSS_PROXY_PSTMPVQ_OFFSET_V_Q_N         (6)
#define GNSS_PROXY_PSTMPVQ_OFFSET_V_Q_E         (7)
#define GNSS_PROXY_PSTMPVQ_OFFSET_V_Q_V         (8)

/* offsets for $PSTMCRCCHECK */
#define GNSS_PROXY_PSTMCRCCHECK_OFFSET_RECVD_CRC (4)

#define GNSS_PROXY_OFFSET_C_TP_MARKER        (0)
#define GNSS_PROXY_OFFSET_C_DATA_MSG_ID      (1)
#define GNSS_PROXY_OFFSET_C_CONTROL_MSG_ID   (1)

#define GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG    (2)
#define GNSS_PROXY_OFFSET_C_CTRL_MSG_SRV_TYPE    (2)
#define GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER (2)

#define GNSS_PROXY_SRV_TYPE_FLUSH_SENSOR_DATA (0x011)
#define GNSS_PROXY_SIZE_OF_C_FLUSH_DATA_MSG_BUFF (3)

/* Validity check UTC date and time */
#define GNSS_PROXY_MIN_UTC_YEAR   (2013)
#define GNSS_PROXY_MIN_UTC_MONTH  (1)
#define GNSS_PROXY_MAX_UTC_MONTH  (12)
#define GNSS_PROXY_MIN_UTC_DAY    (1)
#define GNSS_PROXY_MAX_UTC_DAY    (31)
#define GNSS_PROXY_MAX_UTC_HOUR    (23)
#define GNSS_PROXY_MAX_UTC_MINUTE  (59)
#define GNSS_PROXY_MAX_UTC_SECONDS (59)
#define GNSS_PROXY_MAX_UTC_MILLI_SECONDS (999)

#define GNSS_PROXY_YEAR_BASE_VALUE (2000)
/* Dop limits. Lower the dop value, better is the accuracy of fix */
#define GNSS_PROXY_PDOP_MAX_LIMIT (30)
#define GNSS_PROXY_HDOP_MAX_LIMIT (25)
/* These bit fields are used keep track of received NMEA messages */
#define GNSS_PROXY_GPGGA_ID         (0x00000001)
#define GNSS_PROXY_GPGSA_ID         (0x00000002)
#define GNSS_PROXY_GPGSV_ID         (0x00000004)
#define GNSS_PROXY_GPRMC_ID         (0x00000008)
#define GNSS_PROXY_GPVTG_ID         (0x00000010)
#define GNSS_PROXY_PSTMKFCOV_ID     (0x00000020)
#define GNSS_PROXY_PSTMPV_ID        (0x00000040)
#define GNSS_PROXY_PSTMPVQ_ID       (0x00000080)
#define GNSS_PROXY_CHECKSUM_ERROR   (0x80000000)

#define GNSS_MIN_EXPECTED_NMEA_MESSAGES_FOR_3D_FIX (  GNSS_PROXY_GPGGA_ID  |\
                                                      GNSS_PROXY_GPGSA_ID  |\
                                                      GNSS_PROXY_GPGSV_ID  |\
                                                      GNSS_PROXY_GPRMC_ID  |\
                                                      GNSS_PROXY_PSTMPV_ID  |\
                                                      GNSS_PROXY_PSTMPVQ_ID  )


/* This is number of fields on satellite info in GPGSV message */
#define GNSS_PROXY_GPGSV_MUL_FACTOR_TO_NEXT_ENTRY (4)

#define GNSS_PROXY_DEG_TO_RAD(angle_in_degree) \
   ((tDouble)((tDouble)(angle_in_degree) * (tDouble)(M_PI/180.0)))

#define GNSS_PROXY_CONVERT_MS_TO_SEC(TimeInMs) \
   ( (tF32) ((tF32)(TimeInMs) / (tF32)1000.0 ))

#define GNSS_PROXY_KM_PER_HOUR_TO_METER_PER_SEC (tF32)(1/3.6)

#define GNSS_PROXY_DATA_FORM_BASE_TEN (10)
#define GNSS_PROXY_DATA_FORM_BASE_SIXTEEN (16)
/* Has to be changed for GLONASS support */
#define GNSS_PROXY_SATSYS_GPS     (0x1)

#define GNSS_PROXY_MIN_CN0    (14) //minimum CN0 in dB for signal tracking

/* Total number of fields expected in Each NMEA message. */
#define GNSS_PROXY_FIELDS_COUNT_GPGGA                 (16)
#define GNSS_PROXY_FIELDS_COUNT_GPGSA                 (19)
#define GNSS_PROXY_FIELDS_COUNT_GPGSV                 (21)
#define GNSS_PROXY_FIELDS_COUNT_GPRMC                 (14)
#define GNSS_PROXY_FIELDS_COUNT_GPVTG                 (11)
#define GNSS_PROXY_FIELDS_COUNT_PSTMTFCOV             (10)
#define GNSS_PROXY_FIELDS_COUNT_PSTMSETPAR            (4)
#define GNSS_PROXY_FIELDS_COUNT_PSTMSETPAR_OK         (3)
#define GNSS_PROXY_FIELDS_COUNT_PSTMSETPAR_ERROR      (2)
#define GNSS_PROXY_FIELDS_COUNT_PSTMVER               (3)
#define GNSS_PROXY_FIELDS_COUNT_PSTMPV                (24)
#define GNSS_PROXY_FIELDS_COUNT_PSTMPVQ               (12)
#define GNSS_PROXY_FIELDS_COUNT_PSTMCRCCHECK          (7)
#define GNSS_PROXY_FIELDS_COUNT_PSTMSAVEPAROK         (2)

#define GNSS_PROXY_STA8088_SUPPORTED_CHANNELS         (32)
#define GNSS_PROXY_STA8089_SUPPORTED_CHANNELS         (48)

/* Config Block 1200 bit masks to configure specific
 * satellite system constellation for tracking*/
#define GNSS_PROXY_TESEO_SBAS_AUGMENTATION_SYSTEM     (0x00004)
#define GNSS_PROXY_TESEO_GALILEO_CONSTELLATION_ENABLE (0x00040)
#define GNSS_PROXY_TESEO_COMPASS_CONSTELLATION_ENABLE (0x00100)
#define GNSS_PROXY_TESEO_GPS_CONTSELLATION_ENABLE     (0x10000)
#define GNSS_PROXY_TESEO_GLONASS_CONTSELLATION_ENABLE (0x20000)
#define GNSS_PROXY_TESEO_QZSS_CONTSELLATION_ENABLE    (0x40000)

/* Config Block 1200 bit masks to configure specific
 * satellite system for positioning*/
#define GNSS_PROXY_TESEO_GALILEO_USE_FOR_POSITIONING  (0x000080)
#define GNSS_PROXY_TESEO_COMPASS_USE_FOR_POSITIONING  (0x000200)
#define GNSS_PROXY_TESEO_GPS_USE_FOR_POSITIONING      (0x400000)
#define GNSS_PROXY_TESEO_GLONAS_USE_FOR_POSITIONING   (0x200000)
#define GNSS_PROXY_TESEO_QZSS_USE_FOR_POSITIONING     (0x800000)

/* Bit masks to enable satellite systems for TESEO */
#define GNSS_PROXY_TESEO_BIT_MASK_GPS_ENABLE          (  (GNSS_PROXY_TESEO_GPS_CONTSELLATION_ENABLE)| \
                                                         (GNSS_PROXY_TESEO_GPS_USE_FOR_POSITIONING) )

#define GNSS_PROXY_TESEO_BIT_MASK_SBAS_ENABLE         (GNSS_PROXY_TESEO_SBAS_AUGMENTATION_SYSTEM)

#define GNSS_PROXY_TESEO_BIT_MASK_GLONASS_ENABLE      (  (GNSS_PROXY_TESEO_GLONASS_CONTSELLATION_ENABLE)| \
                                                         (GNSS_PROXY_TESEO_GLONAS_USE_FOR_POSITIONING) )

#define GNSS_PROXY_TESEO_BIT_MASK_QZSS_ENABLE         (  (GNSS_PROXY_TESEO_QZSS_CONTSELLATION_ENABLE)| \
                                                         (GNSS_PROXY_TESEO_QZSS_USE_FOR_POSITIONING) )

#define GNSS_PROXY_TESEO_BIT_MASK_GALILEO_ENABLE      (  (GNSS_PROXY_TESEO_GALILEO_CONSTELLATION_ENABLE)|\
                                                         (GNSS_PROXY_TESEO_GALILEO_USE_FOR_POSITIONING) )

#define GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE      (  (GNSS_PROXY_TESEO_COMPASS_CONSTELLATION_ENABLE)|\
                                                         (GNSS_PROXY_TESEO_COMPASS_USE_FOR_POSITIONING) )

#define GNSS_PROXY_CDB_200_SAT_CONSTE_MASK            (  (GNSS_PROXY_TESEO_BIT_MASK_GPS_ENABLE)| \
                                                         (GNSS_PROXY_TESEO_BIT_MASK_GLONASS_ENABLE)| \
                                                         (GNSS_PROXY_TESEO_BIT_MASK_QZSS_ENABLE) | \
                                                         (GNSS_PROXY_TESEO_BIT_MASK_SBAS_ENABLE))
                                     
#define GNSS_PROXY_CDB_227_SAT_CONSTE_MASK            (  (GNSS_PROXY_TESEO_BIT_MASK_GALILEO_ENABLE) | \
                                                         (GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE) )

#define GNSS_PXY_BLK_200_CLR_SAT_CONST_FIELD          ( (tU32) (~(GNSS_PROXY_CDB_200_SAT_CONSTE_MASK)))
#define GNSS_PXY_BLK_227_CLR_SAT_CONST_FIELD          ( (tU32) (~(GNSS_PROXY_CDB_227_SAT_CONSTE_MASK)))

#define GNSS_PROXY_TESEO_GET_APP_CFG_BLK_200_CMD      ("$PSTMGETPAR,1200*\r\n")
#define GNSS_PROXY_TESEO_GET_APP_CFG_BLK_227_CMD      ("$PSTMGETPAR,1227*\r\n")
#define GNSS_PROXY_TESEO_SET_APP_CBD_200_HEADER       ("$PSTMSETPAR,1200,")   //Set this block to enable Gps,Glo,Qzss and Sbas satellite system
#define GNSS_PROXY_TESEO_SET_APP_CBD_227_HEADER       ("$PSTMSETPAR,1227,")   //Set this block to enable galileo and compass satellite system
#define GNSS_PROXY_TESEO_RESET_GPS_ENGINE             ("$PSTMGPSRESET\r\n")
#define GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY          ("$PSTMGETSWVER,6\r\n")
#define GNSS_PROXY_TESEO_REBOOT                       ("$PSTMSRR\r\n")
#define GNSS_PROXY_TESEO_SAVE_CONFIG_DATA_BLOCK       ("$PSTMSAVEPAR\r\n")
/*PSTMGETPAR is to read any parameter with configuration block number followed
  by ID in above case 1237 where 1 is configuration block and 237 is ID.*/
#define GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK   ("$PSTMGETPAR,1237*\r\n")

/* Teseo_2 CRC from the ADDRESS  0x30000000 till the end of firmware area (1MB - 4 Bytes)*/
#define GNSS_PROXY_TESEO_CRC_CHECK_CMD_TESEO_2   ("$PSTMCRCCHECK,7,0x30000000,0xffffC,0x0\r\n")

/* Teseo-3 CRC from the ADDRESS  0x10000000 till the end of firmware area (1MB - 4 Bytes)*/
#define GNSS_PROXY_TESEO_CRC_CHECK_CMD_TESEO_3   ("$PSTMCRCCHECK,7,0x10000000,0xffffC,0x0\r\n")

#define GNSS_PROXY_TESEO_FLASH_UNLOCK_CMD             ("$PSTMSETPAR,1249,0\r\n")
#define GNSS_PROXY_TESEO_DISABLE_NMEA_BLK_201         ("$PSTMSETPAR,1201,0\r\n")
#define GNSS_PROXY_TESEO_DISABLE_NMEA_BLK_228         ("$PSTMSETPAR,1228,0\r\n")

#define GNSS_PROXY_APP_LIST_BLOCK_ID_200              (0x1200)
#define GNSS_PROXY_APP_LIST_BLOCK_ID_227              (0x1227)
#define GNSS_PROXY_CLR_BLK_200_SAT_SYS                (~(0x0033))
#define GNSS_PROXY_CLR_BLK_227_SAT_SYS                (~(0x000C))


#define GNSS_PROXY_GNSS_MIN_MAX_WEEK_BLOCK_ID         (0x1237)
#define GNSS_PROXY_NMEA_LIST_1_BLOCK_ID               (0x1201)
#define GNSS_PROXY_NMEA_LIST_2_BLOCK_ID               (0x1228)
#define GNSS_PROXY_FLASH_PROTECTION_BLOCK_ID          (0x1249)


#define GNSS_PROXY_NUM_FIELDS_TESEO_FIRMWARE_MSG (4)

#ifdef GNSS_PROXY_REMOTE_TESEO_CONROL_FEATURE

#define GNSS_PROXY_TESEO_CTRL_THREAD_NAME             ("GnssPrxyTesCrtlTh")
#define GNSS_PROXY_TESEO_CTRL_THREAD_PROIORITY        (OSAL_C_U32_THREAD_PRIORITY_LOWEST)
#define GNSS_PROXY_TESEO_CTRL_THREAD_STACKSIZE        (2048)

#endif

/* Satellite ID offsets for different satellite systems */

#define GNSS_PROXY_GPS_SAT_ID_OFFSET                  (tS32)(0)
#define GNSS_PROXY_SBAS_SAT_ID_OFFSET                 (tS32)(367)
#define GNSS_PROXY_GLONASS_SAT_ID_OFFSET              (tS32)(35)
#define GNSS_PROXY_BAIDEU_SAT_ID_OFFSET               (tS32)(159)
#define GNSS_PROXY_QZSS_SAT_ID_OFFSET_SET1            (tS32)(317)
#define GNSS_PROXY_QZSS_SAT_ID_OFFSET_SET2            (tS32)(207)
#define GNSS_PROXY_GALILEO_SAT_ID_OFFSET              (tS32)(-101)

/* Teseo Satellite ID ranges for satellite systems */
#define GNSS_PROXY_TESEO_GPS_MIN_SAT_ID               (tU32)(1)
#define GNSS_PROXY_TESEO_GPS_MAX_SAT_ID               (tU32)(32)
#define GNSS_PROXY_TESEO_SBAS_MIN_SAT_ID              (tU32)(33)
#define GNSS_PROXY_TESEO_SBAS_MAX_SAT_ID              (tU32)(51)
#define GNSS_PROXY_TESEO_GLONASS_MIN_SAT_ID           (tU32)(65)
#define GNSS_PROXY_TESEO_GLONASS_MAX_SAT_ID           (tU32)(92)
#define GNSS_PROXY_TESEO_BAIDEU_MIN_SAT_ID            (tU32)(141)
#define GNSS_PROXY_TESEO_BAIDEU_MAX_SAT_ID            (tU32)(172)
#define GNSS_PROXY_TESEO_QZSS_MIN_SAT_ID_SET1         (tU32)(183)
#define GNSS_PROXY_TESEO_QZSS_MAX_SAT_ID_SET1         (tU32)(197)
#define GNSS_PROXY_TESEO_QZSS_MIN_SAT_ID_SET2         (tU32)(293)
#define GNSS_PROXY_TESEO_QZSS_MAX_SAT_ID_SET2         (tU32)(297)
#define GNSS_PROXY_TESEO_GALILEO_MIN_SAT_ID           (tU32)(301)
#define GNSS_PROXY_TESEO_GALILEO_MAX_SAT_ID           (tU32)(330)

#define GNSS_PXY_VALID_SATSYS_MASK                    (0x0000003F)
#define GNSS_PXY_CFGBLK200_SAT_SYS_MASK               (0xFFCC)
#define GNSS_PXY_CFGBLK227_SAT_SYS_MASK               (0xFFF3)

#define GNSS_PROXY_POLL_NUM_OF_FDS                    (1)
#define GNSS_PROXY_POLL_TIMEOUT_MS                    (200)
#define GNSS_PROXY_READ_TIME_OUT_SCALE_FACTOR         (2)
#define GNSS_PROXY_DATA_INTERVAL_MS                   (1000)
#define GNSS_PROXY_POLL_COUNTER_THRESHOLD             ( (GNSS_PROXY_DATA_INTERVAL_MS * \
                                                         GNSS_PROXY_READ_TIME_OUT_SCALE_FACTOR) \
                                                         /GNSS_PROXY_POLL_TIMEOUT_MS )

#define GNSS_PROXY_SOCK_READ_EXIT_TIMEOUT             (OSAL_tMSecond)(2 * GNSS_PROXY_POLL_TIMEOUT_MS)


//Number of days in each month.
#define   NO_OF_DAYS_JAN    31
#define   NO_OF_DAYS_FEB    28
#define   NO_OF_DAYS_MAR    31
#define   NO_OF_DAYS_APR    30
#define   NO_OF_DAYS_MAY    31
#define   NO_OF_DAYS_JUN    30
#define   NO_OF_DAYS_JUL    31
#define   NO_OF_DAYS_AUG    31
#define   NO_OF_DAYS_SEP    30
#define   NO_OF_DAYS_OCT    31
#define   NO_OF_DAYS_NOV    30
#define   NO_OF_DAYS_DEC    31
#define   NO_OF_DAYS_LEAP_FEB  29

//Satellite system configuration location
#define GNSS_PROXY_SAT_SYS_LOC_EOL	(0)
#define GNSS_PROXY_SAT_SYS_LOC_KDS	(1)
#define GNSS_PROXY_SAT_SYS_LOC_STATIC	(2)

/* Epoch Default Configuration */
#define GNSS_PROXY_EPOCH_REFERENCE_DAY                (6)
#define GNSS_PROXY_EPOCH_REFERENCE_MONTH              (1)
#define GNSS_PROXY_EPOCH_REFERENCE_YEAR               (tU16)(1980)

/* Constants for months and number of days in non-leap and leap years */
#define NO_OF_MONTHS_IN_A_YEAR                        (12)
#define NO_OF_DAYS_NON_LEAP_YEAR                      (tS32)(365)
#define NO_OF_DAYS_LEAP_YEAR                          (tS32)(366)

#define MONTH_JAN                (1)
#define MONTH_FEB                (2)
#define MONTH_MAR                (3)
#define MONTH_APR                (4)
#define MONTH_MAY                (5)
#define MONTH_JUN                (6)
#define MONTH_JUL                (7)
#define MONTH_AUG                (8)
#define MONTH_SEP                (9)
#define MONTH_OCT                (10)
#define MONTH_NOV                (11)
#define MONTH_DEC                (12)

#define GNSS_PROXY_ARGUMENT_INTENTIONALLY_UNUSED(X)  (tVoid)(X)

#define GNSS_PROXY_TESEO_2_FW_VER_MAJOR_NUMBER    (0x03000000)
#define GNSS_PROXY_TESEO_3_FW_VER_MAJOR_NUMBER    (0x04000000)
#define GNSS_PROXY_TESEO_FW_VER_MAJOR_NUMBER_MASK (0xFF000000)

/*************************************************************************
* Driver specific structure definition
*-----------------------------------------------------------------------*/
/*------------------------------------------- dev_gnss specific ------------------------------------------- */
typedef enum
{
   DEVICE_NOT_INITIALIZED = 0,
   DEVICE_INITIALIZED = 1,
   DEVICE_DEINITIALIZED = 2,
   DEVICE_CLOSED_GNSS = 3
}tEnGnssProxyStatus;
/* To Send GNSS proxy state on IMX status to SCC  */
typedef struct
{
   tU8 u8MsgID;
   tU8 u8HostStatus;
   tU8 u8HostAppVer;
}trGnssProxyCmdHostStatus;

typedef struct
{
   tU32 u32SatSysUsed;
   tU32 u32SatConfDataBlk200;
   tU32 u32SatConfDataBlk227;
}trGnssSatSysData;

typedef struct
{

   tU32 u32RecvdGnssMsgs; /* Used to keep trace of received NMEA messages */
   tU32 u32GsvChanCnt;    /* Number of channels info received in current fix */
   tU32 u32ExpecGSVMsgs;  /* Number of GPGSV messages received in current fix */
    /* We store the satellite ID's used for Fix here temporarily.
       Later This will be used to update the channel status. */
   tU16 u16SatUsedFix[OSAL_C_U8_GNSS_NO_CHANNELS];
   tBool bDGpsUsed;
   tBool bChecksumError;

}trGnssTmpFixInfo;

/* Gnss driver running configuration  */
typedef struct
{

   tS32 s32SocketFD;  /* Handle to socket  */
#ifndef GNSS_PROXY_TEST_STUB_ACTIVE
   sk_dgram* hldSocDgram; /*datagram handle*/
#endif
   OSAL_trGnssConfigData rGnssConfigData;
   tBool bConfigrecvd;
   tU16 u16ReadWaitTimeMs;
   tU32 u32RecvdMsgSize;  /* Size of latest received message */
   tU32 u32GnssSatSysLoc;
   tU8 u8RecvBuffer[GNSS_PROXY_MAX_PACKET_SIZE]; /* Buffer to receive data from socket */
   tU8 u8TransBuffer[GNSS_PROXY_TRANSMIT_BUFF_SIZE]; /* Buffer to receive data from socket */
   tU8 u8SccStatus;  /* SCC status */
   tEnGnssProxyStatus enGnssProxyState; /* holds present state of GNSS proxy */
   tBool bShutdownFlag; /* Shutdown event status */
   tBool bThreadWaitInRead; /* To know if a thread is waiting at the time of shutdown */
   tBool bThreadWaitInSatSys; /* To know if a thread is waiting at the time of shutdown */
   OSAL_tSemHandle hGnssProxySemaphore; /* For read synchronization */
   OSAL_tEventHandle hGnssProxyReadEvent; /* For read synchronization */
   OSAL_tEventHandle hGnssProxyInitEvent; /* For INIT */
   OSAL_tEventHandle hGnssProxyTeseoComEvent; /* satellite system notification event */
   trGnssSatSysData rGnssSatSysData; /* Holds the satellite system configuration data */
   OSAL_tenAccess enAccessMode;
   tU8 u8VerInfo; /* Holds the version infomation */

}trGnssProxyInfo;

typedef struct
{
   /* To store the GNSS data at the time of parsing.
      Once the parsing of current fix info is complete, contents of this record will be copied to rGnssTmpRecord */
   OSAL_trGnssFullRecord rGnssTmpRecord;
   /* To store the parsed data. VD-Sensor will read data from this record */
   OSAL_trGnssFullRecord rGnssDataRecord;
   /* This is set to TRUE once complete fix is received from V850*/
   tBool bIsRecordValid;
   tU32 u32NmeaMsgsRecvd;
   /* This is used only for internal diagnostics.
      This is to keep a count of number of records parsed and number of records read by VD-Sensor. */
   tU32 u32NumRecordsParsed;
   tU32 u32NumRecordsRead;
   /* This is used to store data particular to a fix information */
   trGnssTmpFixInfo rGnssTmpFixInfo;

}trGnssProxyData;

/*--------------------------------- Start:Firmware update defines ---------------------------*/
typedef enum
{
   GNSS_FW_UPDATE_NOT_STARTED                   = 0,
   GNSS_FW_UPDATE_DEVICE_OPENED                 = 1,
   GNSS_FW_UPDATE_BOOTLOADER_CONFIG_RECVD       = 2,
   GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_STARTED   = 3,
   GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_COMPLETE  = 4,
   GNSS_FW_UPDATE_FIRMWARE_CONFIG_RECVD         = 5,
   GNSS_FW_UPDATE_FIRMWARE_TRANSFER_STARTED     = 6,
   GNSS_FW_UPDATE_FIRMWARE_TRANSFER_COMPLETE    = 7,
   GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_FAILED    = 8,
   GNSS_FW_UPDATE_FIRMWARE_TRANSFER_FAILED      = 9,
}tEnGnssFwUpdateState;

/*----------------------------------TRACES:FW Upadte status Defines---------------------------------------------------*/

/*This  is added to  put traces in  Error Memory  in case of
 Firware update failure as a fix for GMMY17-3213 Ticket */

typedef enum
{
   GNSS_FW_UPDATE_NOT_ACTIVE     = 0,
   GNSS_FW_UPDATE_STARTED        = 1,
   GNSS_FW_UPDATE_SUCCESSFULL    = 2,
   GNSS_FW_UPDATE_FAILED         = 3
   
}tEnGnssFwUpdateStatus;


typedef enum
{
   SRV_TYPE_GNSS_CHIP_RESET          = 0x10,
   SRV_TYPE_FLASH_BEGIN              = 0x20,
   SRV_TYPE_FLASH_DATA               = 0x21,
   SRV_TYPE_GNSS_CHIP_FLASH_RESPONSE = 0x22,
   SRV_TYPE_FLASH_BUFFER_SIZE        = 0x23,
   SRV_TYPE_FLASH_END                = 0x2F,
   SRV_TYPE_UNKNOWN_ERROR            = 0xF0 

}tEnIncCtrlMsgSrvType;

typedef struct
{
      tEnGnssFwUpdateState enGnssFWUpdateState;
      
      tU32 u32BootLdrSize;
      tU32 u32BootLdrChksum;
      tU32 u32FwImageSize;
      tU32 u32FwImgChksum;
      tU32 u32SentImageSize;
      tU32 u32IncBlockSize;
      tU32 u32TeseoAckChunkSize;
      /* Holds the received software version on V850.
              Based on version, behavior of Linux driver has to change*/
      tU8 u8SccSwVer;
      /* Holds the offset for INC message fields in case the sequence
              counter is used. If not used, this should be 0*/
      tU8 u8SeqCntrOffset;
      /* 8 bit sequence counter, wrap around at 255.
              Reset everytime on status exchange*/
      tU8 u8MsgSeqCntr;
      tU16 u16FilledTxMsgSize;
      OSAL_tEventHandle hGnssFwInEve;
      tEnGnssChipType enGnssChipType;


}trGnssProxyFwUpdateInfo;

// Teseo2 binary image options
typedef struct
{
   signed int code_size;
   unsigned int target_device;
   unsigned int crc32;
   signed int dest_addr;
   signed int entry_offset;
   signed int baud_rate;
   unsigned char nvm_erase;
   unsigned char erase_only;
   unsigned char program_only;
   unsigned char debug_enable;
   signed int nvm_offset;
   signed int nvm_erase_size;
   unsigned int debug_mode;
   unsigned int debug_addr;
   signed int debug_size;
   unsigned int debug_data;
}trTeseo_2_BinaryImageOptions;

typedef struct
{
  tS32 code_size;
  tU32 target_device;
  tU32 crc32;
  tU32 dest_addr;
  tU32 entry_offset;
  tU8 nvm_erase;
  tU8 erase_only;
  tU8 program_only;
  tU8 sub_sector;
  tU8 sta8090f;
  tU8 res1;
  tU8 res2;
  tU8 res3;
  tS32 nvm_offset;
  tS32 nvm_erase_size;
  tU32 debug_enable;
  tU32 debug_mode;
  tU32 debug_address;
  tS32 debug_size;
  tU32 debug_data;
} trTeseo_3_BinaryImageOptions;



typedef struct
{

   tU32 u32XloaderIdentifierMsp;
   tU32 u32XloaderIdentifierLsp;
   tU32 u32XloaderOptions;
   tU32 u32XloaderDefDestAddr;
   tU32 u32XloaderCrc32;
   tU32 u32XloaderSize;
   tU32 u32XloaderDefEntryPoint;

}trTeseo3XloaderPreamble;




/*--------------------------------- END:Firmware update defines ---------------------------*/
/*************************************************************************
* Function prototype declaration
*-----------------------------------------------------------------------*/
tVoid GnssProxy_vLeaveCriticalSection(  tU32 u32LineNum  );
tVoid GnssProxy_vEnterCriticalSection(  tU32 u32LineNum  );
tVoid GnssProxy_vReleaseResource( tU32 u32Resource );

#endif

