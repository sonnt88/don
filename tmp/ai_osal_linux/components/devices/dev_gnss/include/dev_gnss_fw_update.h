/*********************************************************************************************************************
* FILE        : dev_gnss_fw_update.h
*
* DESCRIPTION : dev_gnss_fw_update.c
*               This exports all the interfaces for firmware update.
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 07.DEC.2013 | Initial version: 0.1   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -----------------------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef GNSS_FW_UPDATE_HEADER
#define GNSS_FW_UPDATE_HEADER

tS32 GnssProxyFw_s32Init(tVoid);
tS32 GnssProxyFw_s32DeInit(tVoid);
tS32 GnssProxyFw_s32ConfigBinOpts( const trImageOptions *prImageOptions );
tS32 GnssProxyFw_s32SendBinToTeseo(const char* pBuffer, tU32 u32maxbytes);
tS32 GnssProxy_s32WaitForMsg( tU32 u32EventMask, tU32 u32WaitTime );
tS32 GnssProxyFw_s32SetChipType ( tEnGnssChipType enGnssChipType );
tVoid GnssProxyFw_vCreateIncCtrlMsg ( tEnIncCtrlMsgSrvType enIncSrvType );
tS32 GnssProxyFw_s32GetTeseoCRC(tVoid);

#endif
