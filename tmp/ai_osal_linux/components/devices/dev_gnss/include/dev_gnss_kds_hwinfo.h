/*********************************************************************************************************************
* FILE         : dev_gnss_kds_hwinfo.h
*
* DESCRIPTION  : This header contains macros for GNSS data stored in KDS
*
* AUTHOR(s)    : Ramchandra  (ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |       Version        | Author & comments
*--------------|----------------------|----------------------------------------
* 25.11.2015  | Initial version: 1.0 | Ramchandra (ECF5)
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef GNSS_KDS_HEADER
#define GNSS_KDS_HEADER

/* KDS DID(KDS key) Entry and Length for GNSS */
#define KDS_GNSS_DID_ENTRY_LEN              (0x0C)
#define KDS_GNSS_DID_ENTRY                  (0x0530)

/* KDS entry for GNSS hardware information */
#define KDS_GNSS_SAT_SYS_OFFSET             (11)
#define KDS_GNSS_SAY_SYS_MASK               (0x3F)

#endif
/* End of file */
