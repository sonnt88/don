/*********************************************************************************************************************
* FILE         : gnss_auxclock.h
*
* DESCRIPTION  : This is the header file corresponding to dev_gnss.c
*
* AUTHOR(s)    : Ramachandra Madhu Kiran (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |       Version        | Author & comments
*--------------|----------------------|---------------------------
* 07.MAR.2014  | Initial version: 1.0 | Ramachandra Madhu Kiran (RBEI/ECF5)
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef GNSS_AUX_CLOCK_HEADER
#define GNSS_AUX_CLOCK_HEADER

tS32 AUXCLOCK_s32IOOpen(tVoid);
tS32 AUXCLOCK_s32IOClose(tVoid);
tS32 AUXCLOCK_s32IOControl(tS32 s32fun, tLong sArg);
tVoid GnssProxy_vSyncAuxClock( tU32 u32TimeStamp );
tVoid AUXCLOCK_vInit(tVoid);
tVoid AUXCLOCK_vDeinit(tVoid);
tS32 AUXCLOCK_s32IORead(OSAL_trIOCtrlAuxClockTicks* prIOAuxData, tU32 u32maxbytes);

/* Global Macros */
#define AUX_CLOCK_INIT_SEM (tCString)("AUX_CLOCK_INIT_SEM")


#endif
/*End of file*/
