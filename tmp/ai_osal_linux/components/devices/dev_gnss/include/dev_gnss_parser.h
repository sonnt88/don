/*********************************************************************************************************************
* FILE        : dev_gnss_parser.h
*
* DESCRIPTION : Header for dev_gnss_parser.c
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 28.AUG.2013 | Initial version: 0.1   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -----------------------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef GNSS_GPS_HEADER
#define GNSS_GPS_HEADER

tS32 GnssProxy_s32HandleStatusMsg(tVoid);
tVoid GnssProxy_vHandleCommandReject(tVoid);
tVoid GnssProxy_vHandleGnssData(tVoid);
tS32 GnssProxy_s32HandleConfigMsg(tVoid);
tS32 GnssProxy_s32SetSatSys(tPU32 pu32SatSys );
tS32 GnssProxy_s32GetTeseoFwVer(tVoid);
tS32 GnssProxy_s32GetTeseoCRC(tVoid);
tS32 GnssProxy_s32GetSatConfigReq(tU32 u32CfgBlkID);
tS32 GnssProxy_s32SetSatConfigReq(tVoid);
tS32 GnssProxy_s32GetCfgBlk200_227(tVoid);
tS32 GnssProxy_s32GetNmeaRecvdList( tPU32 pu32NmeaList );
tVoid GnssProxy_vDispatchMsg ( tChar const * const pcFieldIndex[], tU32 u32Tokens );
tS32 GnssProxy_s32FlushSccBuff( tVoid );
tVoid GnssProxy_vReadRegistryValues(tVoid);
#endif
