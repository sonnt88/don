/************************************************************************************
* FILE        : dev_gnss_epoch.c
*
* DESCRIPTION : This file contains necessary implementation of GNSS epoch.
*
*------------------------------------------------------------------------------------
* AUTHOR(s)   : Sanjay G ( RBEI/ECF5 )
*
* HISTORY     :
*------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------
* 29.OCT.2014 | Initial version: 1.0   | Sanjay G(RBEI/ECF5)
* -----------------------------------------------------------------------------------
************************************************************************************/

/***********************************************************************************
* Header file declaration
*----------------------------------------------------------------------------------*/
#include "dev_gnss_types.h"
#include "dev_gnss_trace.h"

/*************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/
tU32 u32GnssEpochMinMaxWeek;
extern trGnssProxyInfo rGnssProxyInfo;

/*************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
static tBool GnssProxy_bCheckDataValidity( const OSAL_trGnssTimeUTC* rUTC_Date );
static tBool GnssProxy_bCheckLeapYear( tU16 u16Year );
static tU16  GnssProxy_u16GetGpsWeekFromDate( const OSAL_trGnssTimeUTC *rReqDate );
static tS32  GnssProxy_s32GetEpoch( tU16 *u16MinWeek,tU16 *u16MaxWeek );
static tS32  GnssProxy_s32CheckUpdatedEpoch( tU16 u16MinWeek ,tU16 u16MaxWeek );
static tS32 GnssProxy_s32CalculateNewUTCDate( OSAL_trGnssTimeUTC * rUTCDate, tS32 s32AddDays );


/*************************************************************************
* Function declaration (scope: Global)
*-----------------------------------------------------------------------*/
tS32 GnssProxy_s32SetEpoch( const OSAL_trGnssTimeUTC *rEpochToSet );
extern tS32 GnssProxy_s32RebootTeseo( tVoid );
extern tS32 GnssProxy_s32SendDataToScc(tU32 u32Bytes);
extern tS32 GnssProxy_s32SaveConfigDataBlock(tVoid);
tS32 GnssProxy_s32GetEpochTime( OSAL_trGnssTimeUTC * rEpochSetTime );


/************************************************************************************
* FUNCTION     : GnssProxy_s32GetEpoch
*
* PARAMETER    : tU16 *u16MinWeek - To copy min week number
*                tU16 *u16MaxWeek - To copy max week number
*
* RETURNVALUE  : OSAL_E_NOERROR  on Success
*                Respective OSAL errors on Failure
*
* DESCRIPTION  : Control functions corresponding to Proxy GNSS device
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|----------------------------------------------
* 24.OCT.2014  | Initial version: 1.0| Sanjay G(RBEI/ECF5)
* ----------------------------------------------------------------------------------
**************************************************************************************/
static tS32 GnssProxy_s32GetEpoch( tU16 *u16MinWeek,tU16 *u16MaxWeek )
{
   tU32 u32MsgLength ;
   tU32 u32ResultMask;
   tS32 s32RetVal = OSAL_ERROR;

   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;

   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                      GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK,
                      OSAL_u32StringLength(GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK) );

   u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                  OSAL_u32StringLength(GNSS_PROXY_TESEO_GET_CDB_GNSS_MIN_MAX_WEEK);

   if ( (tS32)u32MsgLength != GnssProxy_s32SendDataToScc(u32MsgLength) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32GetEpoch:Sending request to SCC failed" );
   }
   //Wait for response.
   else if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                              ( GNSS_PROXY_TESEO_COM_GET_EPOCH_RESPONSE_MASK | 
                                                GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT ),
                                              OSAL_EN_EVENTMASK_OR,
                                              GNSS_PROXY_GNSS_EPOCH_RESPONSE_RECEIVE_WAIT_TIME,
                                              &u32ResultMask ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32GetEpoch:Event wait failed with error %d",
                                             OSAL_u32ErrorCode() );
   }
   //Clear the current occurred event.
   else if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                             ~u32ResultMask,
                                             OSAL_EN_EVENTMASK_AND) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32GetEpoch:Clearing event failed err %lu line %lu",
                                              OSAL_u32ErrorCode(), __LINE__ );
   }
   /* Cancel operation in case of shutdown event */
   else  if ( GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT == u32ResultMask )
   {
      s32RetVal = OSAL_E_CANCELED;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Getting min max week canceled: Shutdown signaled ");
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT );
   }
   else
   {
      *u16MaxWeek = (tU16)( ( u32GnssEpochMinMaxWeek & 0xFFFF0000 ) >> 16 );
      *u16MinWeek = (tU16)( u32GnssEpochMinMaxWeek & 0xFFFF );
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_EPOCH_GT_MINMAX_WK,
                           " %d , %d",
                           *u16MaxWeek,*u16MinWeek );
      //Print a trace if MaxWeek < MinWeek + 1024.
      if( *u16MaxWeek < *u16MinWeek + 1024 )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                     "s32GetEpoch:ERROR:MaxWeek < MinWeek + 1024" );
      }
      else
      {
         s32RetVal = OSAL_E_NOERROR;
      }
   }

   return s32RetVal;
}

/*****************************************************************************************
* FUNCTION     : GnssProxy_s32SetEpoch
*
* PARAMETER    : OSAL_trGnssTimeUTC *rEpochToSet - Date to set epoch
*
* RETURNVALUE  : OSAL_E_NOERROR  on Success
*                Respective OSAL errors on Failure
*
* DESCRIPTION  : Sets GNSS Epoch
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|----------------------------------------------
* 24.OCT.2014  | Initial version: 1.0| Sanjay G(RBEI/ECF5)
* ----------------------------------------------------------------------------------
******************************************************************************************/
tS32 GnssProxy_s32SetEpoch( const OSAL_trGnssTimeUTC *rEpochToSet )
{
   tU32 u32MsgLength;
   tU32 u32ResultMask;
   tS32 s32RetVal = OSAL_ERROR;
   tU16 u16SetGnssWeekMin,u16SetGnssWeekMax;
   tU32 u32GnssMaxMinWeek;
   tU8  u8CommandBuf[40] = "$PSTMSETPAR,1237,0x";
   tU8  u8MaxMinWeek[]= "WEEKHERE";
   tU8  u8ComBuf[] = "[,0]*\r\n";

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_EPOCH_ST_Y_M_D,
                        " %d , %d , %d",
                        rEpochToSet->u16Year,rEpochToSet->u8Month,rEpochToSet->u8Day );
   
   if( GnssProxy_bCheckDataValidity(rEpochToSet) )
   {
      u16SetGnssWeekMin = GnssProxy_u16GetGpsWeekFromDate( rEpochToSet );
      u16SetGnssWeekMax = u16SetGnssWeekMin + 1024;

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_EPOCH_ST_MIXMAX_WK,
                           " %d , %d ",
                           u16SetGnssWeekMin,u16SetGnssWeekMax);
      u32GnssMaxMinWeek = ( ( u16SetGnssWeekMax << 16 ) | u16SetGnssWeekMin );
      OSAL_s32PrintFormat( (char*)u8MaxMinWeek,"%x", u32GnssMaxMinWeek );
      OSAL_szStringNConcat( u8CommandBuf,u8MaxMinWeek,OSAL_u32StringLength(u8MaxMinWeek) );
      OSAL_szStringNConcat( u8CommandBuf,u8ComBuf,sizeof(u8ComBuf)-1 );

      rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
      rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
      OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                         u8CommandBuf,
                         OSAL_u32StringLength(u8CommandBuf) );

      u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER + OSAL_u32StringLength( u8CommandBuf );

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_EPOCH_ST_SND_CMD, " %s",u8CommandBuf );

      s32RetVal = GnssProxy_s32SendDataToScc(u32MsgLength);
      if ((tS32)u32MsgLength != s32RetVal)
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32SetEpoch:Write fail for setting epoch cmd" );
      }
      // Wait for response from ST.
      else if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                 ( GNSS_PROXY_TESEO_COM_SET_EPOCH_RESPONSE_MASK |
                                                   GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT ),
                                                 OSAL_EN_EVENTMASK_OR,
                                                 GNSS_PROXY_TESEO_RESPONSE_WAIT_TIME,
                                                 &u32ResultMask ) )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32SetEpoch:Event wait failed err %d",OSAL_u32ErrorCode() );
      }
      //Clear event.
      else if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                ~u32ResultMask,
                                                OSAL_EN_EVENTMASK_AND) )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32SetEpoch:Event Post failed err %lu line %lu",
                                              OSAL_u32ErrorCode(), __LINE__ );
      }
      /* Cancel operation in case of shutdown event */
      else if ( GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT == u32ResultMask )
      {
         s32RetVal = OSAL_E_CANCELED;
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32SetEpoch:Setting epoch canceled: Shutdown signaled ");
         GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT );
      }
      else if( (tS32)OSAL_E_NOERROR != GnssProxy_s32SaveConfigDataBlock() )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32SetEpoch:Saving config block failed line %lu", __LINE__ );
      }
      else if( OSAL_ERROR == GnssProxy_s32RebootTeseo() )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32SetEpoch:Rebooting teseo failed line %lu", __LINE__ );
      }
      else
      {
         //Wait for some time after reboot before requesting teseo for info.
         OSAL_s32ThreadWait(500);
         s32RetVal = GnssProxy_s32CheckUpdatedEpoch(u16SetGnssWeekMin,u16SetGnssWeekMax);
      }
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "s32SetEpoch:Requested date is invalid" );
   }

   return s32RetVal;

}

/*****************************************************************************************
* FUNCTION     : GnssProxy_s32CheckUpdatedEpoch
*
* PARAMETER    : tU16 u16MinWeek - Min week will be copied here.
*                tU16 u16MaxWeek - Max week will be copied here.
*
* RETURNVALUE  : OSAL_E_NOERROR  on Success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : Compares the updated Week number with requested week number.
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|----------------------------------------------
* 24.OCT.2014  | Initial version: 1.0| Sanjay G(RBEI/ECF5)
* ----------------------------------------------------------------------------------
******************************************************************************************/

static tS32 GnssProxy_s32CheckUpdatedEpoch( tU16 u16MinWeek ,tU16 u16MaxWeek )
{

   tU16 u16LatestMaxWeek = 0;
   tU16 u16LatestMinWeek = 0;
   tS32 s32RetVal = OSAL_ERROR;
   tU8 u8Exit = 0;

   do
   {
      if( (tS32)OSAL_E_NOERROR == GnssProxy_s32GetEpoch( &u16LatestMinWeek,&u16LatestMaxWeek ) )
      {
         if( ( u16LatestMinWeek == u16MinWeek )&&
             ( u16LatestMaxWeek == u16MaxWeek ) )
         {
            s32RetVal = OSAL_E_NOERROR;
         }
         else
         {
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                                 "s32CheckUpdatedEpoch:Set and Get epochs are different.", __LINE__ );
         }

         u8Exit = 5;
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                              "s32CheckUpdatedEpoch:Get epoch failed %lu", __LINE__ );
         u8Exit++;
      }
   }while( u8Exit < 5 );

   return s32RetVal;
}

/****************************************************************************************
* FUNCTION     : GnssProxy_u16GetGpsWeekFromDate
*
* PARAMETER    : const OSAL_trGnssTimeUTC *rReqDate  - Date to find corresponding week
*
* RETURNVALUE  : GPS week number 
*
* DESCRIPTION  : Gets GPS Week number of the inputted date.
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|----------------------------------------------
* 24.OCT.2014  | Initial version: 1.0| Sanjay G(RBEI/ECF5)
* ----------------------------------------------------------------------------------
*******************************************************************************************/
static tU16 GnssProxy_u16GetGpsWeekFromDate( const OSAL_trGnssTimeUTC *rReqDate )
{
   tU16 u16GpsWeekNumber;
   tU16 u16NoOfYears;
   tU16 u16LeapDays;
   tU32 u32DaysGPS;
   tU16 u16MonthTable[13] =
                  {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};

   u16NoOfYears = rReqDate->u16Year - 1980;
   u16LeapDays = (u16NoOfYears / 4) + 1;

   if (((u16NoOfYears % 4) == 0) && (rReqDate->u8Month <= (tU16)2))
   {
      u16LeapDays--;
   }

   u32DaysGPS = ( u16NoOfYears * 365 )
         + u16MonthTable[ rReqDate->u8Month - 1]
         + (tS32)rReqDate->u8Day
         + (u16LeapDays - 6);

   u16GpsWeekNumber = (tU16)( u32DaysGPS / 7 );

   return u16GpsWeekNumber;
}

/*****************************************************************************************
* FUNCTION     : GnssProxy_bCheckDataValidity
*
* PARAMETER    : const OSAL_trGnssTimeUTC* rUTC_Date
*                    :Pointer to the structure to the date which has to be checked.
*
* RETURNVALUE  : FALSE - If date is invalid
*                TRUE - If date is valid
*
* DESCRIPTION  : Checks the validity of the date.
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|----------------------------------------------
* 24.OCT.2014  | Initial version: 1.0| Sanjay G(RBEI/ECF5)
* ----------------------------------------------------------------------------------
******************************************************************************************/
static tBool GnssProxy_bCheckDataValidity( const OSAL_trGnssTimeUTC* rUTC_Date )
{
   tBool bRetVal = TRUE;
   tU8 u8NoOfDaysInTheMonth[] = { NO_OF_DAYS_JAN,
                                  NO_OF_DAYS_FEB,
                                  NO_OF_DAYS_MAR,
                                  NO_OF_DAYS_APR,
                                  NO_OF_DAYS_MAY,
                                  NO_OF_DAYS_JUN,
                                  NO_OF_DAYS_JUL,
                                  NO_OF_DAYS_AUG,
                                  NO_OF_DAYS_SEP,
                                  NO_OF_DAYS_OCT,
                                  NO_OF_DAYS_NOV,
                                  NO_OF_DAYS_DEC };

   if( TRUE == GnssProxy_bCheckLeapYear( rUTC_Date->u16Year ) )
   {
      u8NoOfDaysInTheMonth[1] = NO_OF_DAYS_LEAP_FEB;
   }

   if( ( rUTC_Date->u16Year < 1980 )||
       ( rUTC_Date->u16Year > 9999 )||
       ( rUTC_Date->u8Month == 0 )||
       ( rUTC_Date->u8Month > 12 )||
       ( rUTC_Date->u8Day == 0   )||
       ( rUTC_Date->u8Day > u8NoOfDaysInTheMonth[ rUTC_Date->u8Month - 1 ] )
     )
   {
      bRetVal = FALSE;
   }
   else if( ( rUTC_Date->u16Year == 1980 )&&
            ( rUTC_Date->u8Month == 1 )&&
            ( rUTC_Date->u8Day < 6 ) )
   {
      bRetVal = FALSE;
   }
   else
   {
      //Nothing
   }

   return bRetVal;
}

/*************************************************************************************
* FUNCTION     : GnssProxy_bCheckLeapYear
*
* PARAMETER    : tU16 u16Year - Year to check
*
* RETURNVALUE  : FALSE - If the year is common year
*                TRUE - If the year is leap year
*
* DESCRIPTION  : Checks whether inputted year is common or leap.
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|----------------------------------------------
* 24.OCT.2014  | Initial version: 1.0| Sanjay G(RBEI/ECF5)
* ----------------------------------------------------------------------------------
***************************************************************************************/
static tBool GnssProxy_bCheckLeapYear( tU16 u16Year )
{

   tBool bRetVal = 0;

   if( u16Year % 4 != 0 )
   {
      bRetVal = FALSE;   //Common year
   }
   else if ( u16Year % 100 != 0 )
   {
      bRetVal = TRUE;   //Leap year
   }
   else if ( u16Year % 400 != 0 )
   {
      bRetVal = FALSE;   //Common year
   }
   else
   {
      bRetVal = TRUE;   //Leap year
   }

   return bRetVal;
}

/*************************************************************************************
* FUNCTION     : GnssProxy_s32GetEpochTime
*
* PARAMETER    : OSAL_trGnssTimeUTC * rEpochSetTime - Pointer to return the epoch set time.
*
* RETURNVALUE  : OSAL_E_NO_ERROR - on getting the epoch set week
*                         OSAL_ERROR - if GetEpoch function is not successful
*                         OSAL_E_INVALIDVALUE - if the date received in the parameter is a NULL pointer
*
* DESCRIPTION  : Gets the first day of the week when epoch was last set
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date            |       Version        |  Author & comments
*-----------------|--------------------- |----------------------------------------------
* 12.NOV.2015     | Initial version: 1.0 | Ramchandra Kulkarni (RBEI/ECF5)
* ----------------------------------------------------------------------------------
***************************************************************************************/
tS32 GnssProxy_s32GetEpochTime( OSAL_trGnssTimeUTC * rEpochSetTime )
{
   tU16 u16EpochMaxWeek     = 0;
   tU16 u16EpochMinWeek     = 0;
   tS32 s32EpochMinDays     = 0;
   tS32 s32ReturnValue      = OSAL_E_NOERROR;

   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "GnssProxy_s32GetEpochTime function is running");

   if( rEpochSetTime == OSAL_NULL )
   {
      s32ReturnValue = OSAL_E_INVALIDVALUE;
      GnssProxy_vTraceOut( TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE,  "Null pointer parameter -> OSAL_trGnssTimeUTC" );

   }
   else
   {
      if((tS32)OSAL_E_NOERROR == GnssProxy_s32GetEpoch( &u16EpochMinWeek, &u16EpochMaxWeek ))
      {
          rEpochSetTime->u8Day   = GNSS_PROXY_EPOCH_REFERENCE_DAY;
          rEpochSetTime->u8Month = GNSS_PROXY_EPOCH_REFERENCE_MONTH;
          rEpochSetTime->u16Year = GNSS_PROXY_EPOCH_REFERENCE_YEAR;

          GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "Epoch date reference - "
                                                                       "%2d/%2d/%4d", rEpochSetTime->u8Day, rEpochSetTime->u8Month, rEpochSetTime->u16Year);

          s32EpochMinDays = (tS32)(u16EpochMinWeek * 7);

          GnssProxy_s32CalculateNewUTCDate( rEpochSetTime, s32EpochMinDays );

          GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "Epoch was last set on - "
                                                                       "%2d/%2d/%4d", rEpochSetTime->u8Day, rEpochSetTime->u8Month, rEpochSetTime->u16Year);
      }
      else
      {
          GnssProxy_vTraceOut( TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE,  "GnssProxy_s32GetEpoch returned ERROR" );
          s32ReturnValue = OSAL_ERROR;
      }
   }
   return s32ReturnValue;
}


/****************************************************************************************
* FUNCTION     : GnssProxy_s32CalculateNewUTCDate
*
* PARAMETER    : OSAL_trGnssTimeUTC * rUTCDate  - Initial date
*                        tS32 s32AddDays - Number of days to be added to the given date
*
* RETURNVALUE  : OSAL_E_INVALIDVALUE - if Initial date is NULL
*                          OSAL_E_NOERROR - if the function executes properly
*
* DESCRIPTION  : Adds specified number of days to a given date
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date                |       Version                  | Author & comments
*--------------|---------------------|----------------------------------------------
* 12.NOV.2015    | Initial version: 1.0         | Ramchandra Kulkarni(RBEI/ECF5)
* ----------------------------------------------------------------------------------
*******************************************************************************************/

static tS32 GnssProxy_s32CalculateNewUTCDate( OSAL_trGnssTimeUTC * rUTCDate, tS32 s32AddDays )
{
   tS32 s32ReturnValue      = (tS32)OSAL_E_NOERROR;
   tS32 s32Days             = 0;
   tU8  u8Month             = 0;
   tU16 u16Year             = 0;
   tU8  u8MonthTable[12]    = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
   tU8  u8MonthIndex        = 0;
   tS32 s32DaysTemp         = 0;

   if( rUTCDate == OSAL_NULL )
   {
       s32ReturnValue = (tS32)OSAL_E_INVALIDVALUE;
   }
   else
   {
       s32Days = (tS32)(rUTCDate->u8Day);
       u8Month = rUTCDate->u8Month;
       u16Year = rUTCDate->u16Year;

       s32DaysTemp = s32Days;

       for(u8MonthIndex = MONTH_JAN; u8MonthIndex < u8Month; u8MonthIndex++)
       {
           s32DaysTemp += u8MonthTable[ u8MonthIndex - 1 ];
           if( (u8Month == MONTH_FEB) && ( TRUE == GnssProxy_bCheckLeapYear(u16Year)) )
           {
               s32DaysTemp += 1;
           }
       }

       s32DaysTemp = s32DaysTemp + s32AddDays;


       while(s32DaysTemp > NO_OF_DAYS_NON_LEAP_YEAR)
       {
           if( TRUE == GnssProxy_bCheckLeapYear(u16Year))
           {
               s32DaysTemp = s32DaysTemp - NO_OF_DAYS_LEAP_YEAR;
           }
           else
           {
               s32DaysTemp = s32DaysTemp - NO_OF_DAYS_NON_LEAP_YEAR;
           }
           u16Year = u16Year + 1;
       }

       if(s32DaysTemp == 0)
       {
          s32Days = 1;
          u8Month = MONTH_JAN;
       }
       else
       {
          for( u8MonthIndex = MONTH_JAN; ((s32DaysTemp > 0) && (u8MonthIndex <= NO_OF_MONTHS_IN_A_YEAR)); u8MonthIndex++ )
          {
              s32Days = s32DaysTemp;
              s32DaysTemp = s32DaysTemp - u8MonthTable[ u8MonthIndex - 1 ];

              if( (u8MonthIndex == MONTH_FEB) && (TRUE == GnssProxy_bCheckLeapYear(u16Year)) && (s32DaysTemp > 0) )
              {
                  s32DaysTemp = s32DaysTemp - 1;
              }
          }
          u8Month = u8MonthIndex - 1; 
       }

       rUTCDate->u16Year = u16Year;
       rUTCDate->u8Month = u8Month;
       rUTCDate->u8Day   = (tU8)s32Days;
   }

   return s32ReturnValue;
}


//End of file

