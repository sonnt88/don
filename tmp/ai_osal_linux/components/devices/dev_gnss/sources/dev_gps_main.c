/*********************************************************************************************************************
* FILE        : dev_gps_main.c
*
* DESCRIPTION : This file implements GNSS Proxy driver module.
*               In GEN3 the GNSS solution consists of ST chip which is connected to UART of SCC .
*               It provides the GNSS information as standard NMEA messages and also special ST messages.
*               A software component running on the SCC collects the GNSS information from the GNSS chip and
*               sends the information to IMX via INC. It is the function of the GNSS Proxy driver on IMX to 
*               collect the incoming GNSS data from V850, parse it and populate the required OSAL parameters 
*               in its local buffers and provide it to VD-SENSOR application as and when it requests.
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Niyatha S Rao (RBEI/ECF5)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 22.APR.2013 | Initial version: 0.1   | Niyatha S Rao (RBEI/ECF5)
* -----------------------------------------------------------------------------------------------
* 8.May.2013  | Version: 1.0           | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                        | Completed implementation of GNSS proxy.
* -----------------------------------------------------------------------------------------------
* 1.JULY.2013 | Version: 1.1           | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                        | Modified Function GnssProxy_vHandleGPRMC and
*             |                        | GnssProxy_vHandleGPVTG
* -----------------------------------------------------------------------------------------------
* 17.AUG.2013 | Version: 1.2           | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                        | Added additional check along with checksum before parsing a message.
*             |                        | This is needed because of weak checksum of NMEA message.
* -----------------------------------------------------------------------------------------------
* 29.AUG.2013 | Version: 2.0           | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                        | Extended from dev_gps to dev_gnss. Made code more modular.
* -----------------------------------------------------------------------------------------------
*********************************************************************************************************************/

/*************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/

#include "dev_gnss_types.h"
#include "dev_gps_main.h"
#include "dev_gnss_parser.h"
#include "dev_gnss_scc_com.h"
#include "dev_gnss_trace.h"

typedef struct
{
      tU32 u32RequestedFieldTypes;
      tU32 u32RequestedFieldSpecs;
      tU32 u32DataRecordBufferId;
      OSAL_trGPSFullRecord rGpsTmpRecord;
      tU32 u32DataRecordBufferSize;
      /* This is used to store fix information in the format needed by VD-Sensor */
      tU8 au8DataRecordBuffer[OSAL_C_S32_GPS_MAX_RECORD_DATA_SIZE];
} trGpsSpecificData;

/*************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/
extern trGnssProxyInfo rGnssProxyInfo;
extern trGnssProxyData rGnssProxyData;
trGpsSpecificData rGpsSpecificData;

/*************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/

static tS32 GnssProxy_s32IoctlReadRecord( const OSAL_trGPSReadRecordHeaderArgs * );
static tS32 GnssProxy_s32GetAvailRecords( tPUInt puAvailrecords );
static tVoid GpsProxy_vSetDataReady(tVoid);

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_s32IOControl
*
* PARAMETER    : s32FunId,  Function identifier
*                s32Arg , Argument to be passed to function
*
* RETURNVALUE  : OSAL_E_NOERROR  on Success
*                Respective OSAL errors on Failure
*
* DESCRIPTION  : Control functions corresponding to Proxy GNSS device
*
* HISTORY      :
*--------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|-------------------------------------
* 26.APR.2013  | Initial version: 1.0| Niyatha S Rao (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GpsProxy_s32IOControl( tS32 s32FunId, tLong sArg )
{
   tS32  s32RetVal = OSAL_E_NOERROR;
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_GPS_IOCTRL, " %d", s32FunId);

   if( rGnssProxyInfo.enGnssProxyState == DEVICE_DEINITIALIZED )
   {
      s32RetVal = OSAL_E_TEMP_NOT_AVAILABLE;
   }
   else
   {
      switch( s32FunId )
      {
         case OSAL_C_S32_IOCTRL_VERSION:
         {
            if( OSAL_NULL != sArg )
            {
                tULong *puVersion = (tU64*)sArg;
                /* This lint suppression is intentional. */
                //lint -e971.
                *puVersion = (GNSS_PROXY_COMPONENT_VERSION << 16);
            }
            else
            {
                GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Invalid Param to OSALC_S32_IOCTRL_VERSION" );
                s32RetVal =  OSAL_E_INVALIDVALUE;
            }
         }
         break;

         case OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS:
         /* Allows the client to set which field types should be part of the data
         * records and which data formats should be used for the data. s32Arg is
         * interpreted as a pointer to an OSAL_trGPSRecordFields structure.
         */
         {   
            OSAL_trGPSRecordFields *prSpec = (OSAL_trGPSRecordFields*)sArg;
            /* This is clearly an error. */
            if( prSpec == OSAL_NULL )
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
              GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                              "Invalid Param to IOCTL_GPS_SET_RECORD_FIELDS" );

            }
            /* We don't really support field specifiers. */
            else if( prSpec->u32FieldSpecifiers != GNSS_PROXY_DEFAULT_FIELD_SPECS )
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                              "Invalid Field Specifiers to IOCTL_GPS_SET_RECORD_FIELDS" );

            }
            else
            {
               /* The values are OK.  We will use them. */
               rGpsSpecificData.u32RequestedFieldTypes = prSpec->u32FieldTypes;
               rGpsSpecificData.u32RequestedFieldSpecs = prSpec->u32FieldSpecifiers;
            }
         }
         break;

         case OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS:
         /* Lets the client retrieve the current field type and data format
         * specifiers. s32Arg is interpreted as a pointer to an OSAL_trGPSRecordFields structure.
         */
         {
         
              OSAL_trGPSRecordFields *prSpec = (OSAL_trGPSRecordFields*)sArg;
               /* This is clearly an error. */
             if ( OSAL_NULL != prSpec )
             {
                  if ( (rGpsSpecificData.u32RequestedFieldSpecs != GNSS_PROXY_DEFAULT_FIELD_SPECS) ||
                       (rGpsSpecificData.u32RequestedFieldTypes != GNSS_PROXY_DEFAULT_FIELD_TYPES) )
                  {
                     rGpsSpecificData.u32RequestedFieldSpecs = GNSS_PROXY_DEFAULT_FIELD_SPECS;
                     rGpsSpecificData.u32RequestedFieldTypes = GNSS_PROXY_DEFAULT_FIELD_TYPES;
                  }
                  prSpec->u32FieldTypes = rGpsSpecificData.u32RequestedFieldTypes;
                  prSpec->u32FieldSpecifiers = rGpsSpecificData.u32RequestedFieldSpecs;
             }
             else
             {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                                   "Invalid Param to OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS" );
               s32RetVal = OSAL_E_INVALIDVALUE;
             }
         }
         break;

         case OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE:
         /* Returns the number of available data records.
         * In this implementation, the number of data records is at most one.
         */
         {
            if ( OSAL_NULL != sArg )
            {
              
               /* rGnssProxyData.bIsRecordValid == TRUE,then we have a valid record in 
                  rGnssProxyData.rGnssTmpRecord. */
               s32RetVal = GnssProxy_s32GetAvailRecords( (tPUInt) sArg );
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                                    "Invalid Param to OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE" );
               s32RetVal =  OSAL_E_INVALIDVALUE;
            }
         }
         break;

         case OSAL_C_S32_IOCTL_GPS_READ_RECORD:
         /* Read a data record. s32Arg is a pointer to an
         * OSAL_trGPSReadRecordHeaderArgs structure.  The pBuffer field points to
         * a data buffer with enough room for the entire data record (as returned
         * by a previous call to OSAL_s32IORead).  The u32RecordId field contains
         * the record ID of the last data record (also taken from OSAL_s32IORead).
         */
         {
            if( OSAL_NULL != sArg )
            {
               s32RetVal = GnssProxy_s32IoctlReadRecord( (OSAL_trGPSReadRecordHeaderArgs*)sArg );
            }
            else
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
         }
         break;
         case OSAL_C_S32_IOCTL_GET_SAT_SYS:
         {
            tULong *puArg = (tULong *) sArg;
            if (NULL != puArg )
            {
               *puArg = GNSS_PROXY_SATSYS_GPS;
            }
            else
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
         }
         break;

         case OSAL_C_S32_IOCTL_GPS_START_DEVICE:
         case OSAL_C_S32_IOCTL_GPS_STOP_DEVICE:
         {
            // Just return success. This is explicit requirement by VD_SENSOR.
            break;
         }
         /* OSAL_C_S32_IOCTL_GPS_START_DEVICE and OSAL_C_S32_IOCTL_GPS_STOP_DEVICE
         * are not supported as in other platforms these IOCTRLs were used to start
         * and stop calculations in the GPS low level drivers which is not valid for
         * GEN3 platform
         */

         case OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE:
         /*OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE is currently not supported on IMX as this support is currently
         * handled by the SCC processor.
         * This support may be needed in future on IMX side and this can be provided by using the
         * ST commands listed below:
         * GPS_STARTUP_COLD -> $PSTMCOLD
         * GPS_STARTUP_WARM -> $PSTMWARM
         * GPS_STARTUP_HOT -> $PSTMHOT
         * GPS_STARTUP_FACTORY -> $PSTMRESTOREPAR
         */

         case OSAL_C_S32_IOCTL_GPS_SET_ANTENNA:
         /* OSAL_C_S32_IOCTL_GPS_SET_ANTENNA is not supported as the ANTENNA powering ON/OFF is
         * handled by SCC on GEN3
         */

         case OSAL_C_S32_IOCTL_GPS_GET_ANTENNA:
         /*
         * OSAL_C_S32_IOCTL_GPS_GET_ANTENNA is currently not supported as the ANTENNA powering ON?OFF
         * is handled by SCC on GEN3
         */

         case OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE:
         case OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA:
         case OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA:
         /*
         * OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE, OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA
         *  and OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA are not supported as VDSensor will not be
         * storing the Ephemeris and Almanac data in the persistent memory in GEN3.
         * The ST chip takes care of having the back up of Ephemeris and Almanac data internally
         */
         case OSAL_C_S32_IOCTL_GPS_SET_TEMPERATURE:
         case OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT:
         case OSAL_C_S32_IOCTL_GPS_SET_DEFAULT_CLOCK_DRIFT:
         case OSAL_C_S32_IOCTL_GPS_SET_KDS_XO_COEFF:
         case OSAL_C_S32_IOCTL_GPS_GET_XO_COEFF:
         case OSAL_C_S32_IOCTL_GPS_SET_XO_COEFF:
         case OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME:
         case OSAL_C_S32_IOCTL_GPS_SET_HINTS:
         case OSAL_C_S32_IOCTL_GPS_FRONT_END_TEST:
         case OSAL_C_S32_IOCTL_GPS_SET_SETUP_EXTTESTMODE:
         case OSAL_C_S32_IOCTL_GPS_GET_EXTTESTMODE_RESULTS:
         case OSAL_C_S32_IOCTL_GPS_SET_STARTUP_POSITION:
         case OSAL_C_S32_IOCTL_GPS_SET_TRACE_COMMAND:
         case OSAL_C_S32_IOCTL_SET_SAT_SYS:         
         case OSAL_C_S32_IOCTL_GPS_GET_ANTENNA_STATUS:
         case OSAL_C_S32_IOCTL_GPS_GET_GPS_TEMPERATURE:
         case OSAL_C_S32_IOCTL_GPS_REMOVE_RECORDS:
         case OSAL_C_S32_IOCTL_GPS_SET_DATA_SOURCE:
         case OSAL_C_S32_IOCTL_GPS_FORCE_DATA:
         case OSAL_C_S32_IOCTL_GPS_SET_CURRENT_CLOCK_DRIFT:
         case OSAL_C_S32_IOCTL_GPS_SET_CVM_STATE:
         {
            s32RetVal = OSAL_E_NOTSUPPORTED;
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  " FUN ID %d Not Supported here", s32FunId );
            break;
         }
         default:
         {
            /* All other commands are either not necessary or not implemented yet. */
            s32RetVal = OSAL_E_NOTSUPPORTED;
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Unknown IO_CONTROL" );
         }
      }
   }
   return(s32RetVal);
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32GetAvailRecords
*
* PARAMETER   : pu32Availrecords, Pointer to a u32 type.
*
* RETURNVALUE : OSAL_E_NOERROR  on Success
*               OSAL_ERROR  on error
*               OSAL_E_INVALID_VALUE for wrong parameter
*
* DESCRIPTION : Implementation function for OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE
*               If we have a valid record, Return *pu32Availrecords =1; else
*               Wait till timeout on a data ready event. If we don't get the event, Return
*               *pu32Availrecords = 0;
*
* HISTORY     :
*---------------------------------------------------------------------
* Date        |  Version     | Author & comments
*-------------|--------------|----------------------------------------
* 31.MAR.2013 | Version 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |              | Initial Version
* --------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxy_s32GetAvailRecords( tPUInt puAvailrecords )
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   OSAL_tEventMask u32ResultMask = 0;

   /* Sanity check */
   if( OSAL_NULL == puAvailrecords )
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   /* Do we have a record Ready ? */
   else if( rGnssProxyData.bIsRecordValid == TRUE )
   {
      *puAvailrecords = 1;
   }
   /* We don't have a record, So wait for timeout period */
   else
   {
      if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyReadEvent,
                                                 (GNSS_PROXY_EVENT_DATA_READY | GNSS_PROXY_EVENT_SHUTDOWN_READ_EVENT),
                                                 OSAL_EN_EVENTMASK_OR,
                                                 rGnssProxyInfo.u16ReadWaitTimeMs,
                                                 &u32ResultMask ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event wait failed err %d line %d",
                                               OSAL_u32ErrorCode(),__LINE__ );

      }
      /* Event wait returned with a success, Clear the event */
      else if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyReadEvent,
                                               (OSAL_tEventMask) ~u32ResultMask,
                                               OSAL_EN_EVENTMASK_AND))
      {
           GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event Post failed err %lu line %d",
                                               OSAL_u32ErrorCode(), __LINE__ );
      }
      /* If shutdown event is received or if shutdown flag is set, return 
           *pu32Availrecords = 0 */
      if (( u32ResultMask == GNSS_PROXY_EVENT_SHUTDOWN_READ_EVENT ) ||
          ( rGnssProxyInfo.bShutdownFlag == TRUE ))
      {
         *puAvailrecords = 0;
         GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_READ_EVENT );
         s32RetVal = OSAL_E_CANCELED;
      }
      /* Event wait is done. Now check if we have a valid record */
      else if ( rGnssProxyData.bIsRecordValid == FALSE )
      {
         *puAvailrecords = 0;
      }
      else
      {
         *puAvailrecords = 1;
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32IoctlReadRecord
*
* PARAMETER   : ptrArgs, Pointer to a record header structure.
*               This structure must come from a previous call to OSAL_s32IORead.
*
* RETURNVALUE : OSAL_E_NOERROR  on Success
*               Respective OSAL errors on Failure
*
* DESCRIPTION : Implementation function for OSAL_C_S32_IOCTL_GPS_READ_RECORD
*               Read a data record. The pBuffer field of the structure points to a data buffer
*               with enough room for the entire data record(as returned by a previous call to
*               OSAL_s32IORead).The u32RecordId field contains the record ID of the last
*               data record (also taken from OSAL_s32IORead).
*
* HISTORY     :
*-----------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|----------------------------------------
* 26.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* ----------------------------------------------------------------------------
* 8.MAR.2013  | Version 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | Removed multiple returns
* -----------------------------------------------------------------------------
*********************************************************************************************************************/

static tS32 GnssProxy_s32IoctlReadRecord(const OSAL_trGPSReadRecordHeaderArgs *ptrArgs)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   /* Sanity check of the arguments. */
   if( (ptrArgs == OSAL_NULL) || (ptrArgs->pBuffer == OSAL_NULL) )
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
   }

   /* Ideally, we would check that the buffer size is
      sufficient here, but the API does not include this
      information. */

   /* If the record requested is not the pending record, the
      request cannot be carried through. */
   else if( ptrArgs->u32RecordId != rGpsSpecificData.u32DataRecordBufferId )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Invalid record ID: Expected %lu Got %lu",
                                             rGpsSpecificData.u32DataRecordBufferId,
                                             ptrArgs->u32RecordId );
      s32RetVal = OSAL_E_DOESNOTEXIST;
   }
   else
   {
      /* Copy the buffered data record to the client's buffer. */
      (tVoid)OSAL_pvMemoryCopy( ptrArgs->pBuffer,
                                rGpsSpecificData.au8DataRecordBuffer,
                                rGpsSpecificData.u32DataRecordBufferSize );
      /* Ensure that the record is only used once. */
      rGpsSpecificData.u32DataRecordBufferId = GNSS_PROXY_INVALID_RECORD_ID;
      rGpsSpecificData.u32DataRecordBufferSize = 0;
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32IORead
*
* PARAMETER   : pBuffer, pointer to buffer(must be of type OSAL_trGPSRecordHeader)
*               u32maxbytes, size of pBuffer
*
* RETURNVALUE : Number of bytes to read on success
*               Respective OSAL errors on Failure
*               Possible error codes:
*               OSAL_E_NOSPACE, OSAL_E_TIMEOUT
*
* DESCRIPTION : This function fills a user supplied buffer with information about the latest
*               GNSS data record.  The contents of the data record are retrieved through a call
*               to OSAL_s32IOControl with the command being OSAL_C_S32_IOCTL_GPS_READ_RECORD
*               and the argument being a pointer to a structure, which contains a pointer to a
*               data buffer and a record id.  The record ID must correspond to the record ID
*               that GnssProxy_s32IORead wrote into pBuffer.
*               The data record contents are written into the buffer au8DataRecordBuffer.  The
*               function GnssProxy_s32IOControl copies these buffered data to the client's(VDSensor's) buffer.
*
* HISTORY     :
*----------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|---------------------------------------------
* 26.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* ---------------------------------------------------------------------------------
* 8.MAR.2013  | Version 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | customized read for Gen3. Also removed 
*             |                      | multiple returns from the function.
* ---------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GpsProxy_s32IORead(tPS8 pBuffer, tU32 u32maxbytes)
{
   OSAL_trGPSRecordHeader *ptrHdr = (OSAL_trGPSRecordHeader*)(tPVoid)pBuffer;
   tS32 s32Size = 0;
   tS32 s32NoFields = 0;
   tS32 s32RetVal = OSAL_ERROR;

   /* rGnssProxyData.rGnssTmpRecord and bIsRecordValid are accessed in two
      thread contexts. In application thread context as well as in
      GnssProxy_vSocketReadThread. Thus this guy needs protection */
   GnssProxy_vEnterCriticalSection(__LINE__);

   /* If socket communication fails in middle, device will be de-initialized but not closed */
   if( rGnssProxyInfo.enGnssProxyState == DEVICE_DEINITIALIZED )
   {
      s32RetVal = OSAL_E_TEMP_NOT_AVAILABLE;
   }
   /* Check the available space in the client's buffer. */
   else if( u32maxbytes < sizeof(OSAL_trGPSRecordHeader) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_INV_SZ, NULL );
      s32RetVal = OSAL_E_NOSPACE;
   }
   /* Make sure a record is available. */
   else if( rGnssProxyData.bIsRecordValid != TRUE )
   {
      /* This should never happen. */
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "TIMEOUT" );
      s32RetVal = OSAL_E_TIMEOUT;
   }
   else
   {
      /* Set a default value.  If a measured position is available, it will be
         overwritten below. */
      ptrHdr->u32TimeStamp = 0;

      GpsProxy_vSetDataReady();

      /* Update field type */
      ptrHdr->trFieldInfo[s32NoFields].s32Type =
                        OSAL_C_S32_GPS_FIELD_TYPE_MEASURED_POSITION;
      
      /* Update field size */
      ptrHdr->trFieldInfo[s32NoFields].s32Size =
              ( sizeof(OSAL_trGPSMeasuredPosition) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1) ) &
              ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

      /* Update field offset */
      ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

      /* Copy data to the data record buffer. */
      (tVoid)OSAL_pvMemoryCopy(
                   rGpsSpecificData.au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
                   &rGpsSpecificData.rGpsTmpRecord.trMeasuredPosition,
                   sizeof(OSAL_trGPSMeasuredPosition) );

      s32Size += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
      s32NoFields += 1;

      /* Set the time stamp in the record header. */
      if( rGpsSpecificData.rGpsTmpRecord.trMeasuredPosition.trTimeStamp.bTsValid )
      {
         ptrHdr->u32TimeStamp =
               rGpsSpecificData.rGpsTmpRecord.trMeasuredPosition.trTimeStamp.u32Timestamp;
      }
      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_MSRMT_CPD, NULL );


   /* Likewise for a tracking data field. */
      /* Update field type */
      ptrHdr->trFieldInfo[s32NoFields].s32Type =
              OSAL_C_S32_GPS_FIELD_TYPE_TRACKING_DATA;

      /* Update field size */
      ptrHdr->trFieldInfo[s32NoFields].s32Size =
              ( sizeof(OSAL_trGPSTrackData) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1) ) &
              ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

      /* Update field offset */
      ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

      /* Copy data to the data record buffer. */
      (tVoid)OSAL_pvMemoryCopy(
               rGpsSpecificData.au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset,
               &rGpsSpecificData.rGpsTmpRecord.trTrackData,
               sizeof(OSAL_trGPSTrackData));

      s32Size     += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
      s32NoFields += 1;
      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_TRC_DATA_CPD, NULL );

      /* Update field type */
      ptrHdr->trFieldInfo[s32NoFields].s32Type =
              OSAL_C_S32_GPS_FIELD_TYPE_VISIBLE_LIST;

      /* Update field size */
      ptrHdr->trFieldInfo[s32NoFields].s32Size =
              ( sizeof(OSAL_trGPSVisibleList) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1) ) &
              ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

      /* Update field offset */
      ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

      /* Copy data to the data record buffer. */
      (tVoid)OSAL_pvMemoryCopy(
             ( rGpsSpecificData.au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset ),
             &rGpsSpecificData.rGpsTmpRecord.trVisibleList,
             sizeof(OSAL_trGPSVisibleList) );
    GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_VIS_LST_CPD, NULL );

      s32Size += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
      s32NoFields += 1;

      /* Likewise for the covariance data. */

      /* Update field type */
      ptrHdr->trFieldInfo[s32NoFields].s32Type =
              OSAL_C_S32_GPS_FIELD_TYPE_EXT_DATA;

      /* Update field size */
      ptrHdr->trFieldInfo[s32NoFields].s32Size =
              ( sizeof(OSAL_trGPSExtData) + (OSAL_C_U32_GPS_MAX_ELEMSIZE-1) ) &
              ~(OSAL_C_U32_GPS_MAX_ELEMSIZE-1);

      /* Update field offset */
      ptrHdr->trFieldInfo[s32NoFields].s32Offset = s32Size;

      /* Copy data to the data record buffer. */
      (tVoid)OSAL_pvMemoryCopy(
             ( rGpsSpecificData.au8DataRecordBuffer + ptrHdr->trFieldInfo[s32NoFields].s32Offset ),
             &rGpsSpecificData.rGpsTmpRecord.trExtData,
             sizeof(OSAL_trGPSExtData) );

      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_EXT_DATA_CPD, NULL );

      s32Size += ptrHdr->trFieldInfo[s32NoFields].s32Size ;
      s32NoFields += 1;

      /* Fill in the remaining header fields. */
      ptrHdr->u32RecordId = rGnssProxyData.u32NumRecordsParsed;
      ptrHdr->s32Size = s32Size;
      ptrHdr->s32NoFields = s32NoFields;

      /* rGnssProxyData.rGnssTmpRecord is read. Hence it is no more valid */
      rGnssProxyData.bIsRecordValid = FALSE;
      OSAL_pvMemorySet(&rGpsSpecificData.rGpsTmpRecord, 0, sizeof(OSAL_trGPSFullRecord) );

      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_GNSS_REC_POPU, "%lu, %lu, %lu",
      ptrHdr->u32RecordId,ptrHdr->s32Size, ptrHdr->s32NoFields);

      /* Store information about the buffered data record. */
      rGpsSpecificData.u32DataRecordBufferId = rGnssProxyData.u32NumRecordsParsed;
      rGpsSpecificData.u32DataRecordBufferSize = (tU32)s32Size;

      s32RetVal = sizeof(OSAL_trGPSRecordHeader);
   }
   GnssProxy_vLeaveCriticalSection(__LINE__);
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : GpsProxy_vSetDataReady
*
* PARAMETER   : None
*
* RETURNVALUE : None
*
* DESCRIPTION : Copies data from GNSS format to GPS structure.
*
* HISTORY     :
*-----------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|----------------------------------------
* 2.Oct.2013  | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ----------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GpsProxy_vSetDataReady ()
{
   tU32 u32LoopCnt;
   tU32 u32SatIndex = 0;
   OSAL_trGPSMeasuredPosition *prGPSMeasuredPosition =
                               &rGpsSpecificData.rGpsTmpRecord.trMeasuredPosition;
   OSAL_trGPSVisibleList *prGPSVisibleList =
                               &rGpsSpecificData.rGpsTmpRecord.trVisibleList;
   OSAL_trGPSTrackData *prGPSTrackData =
                               &rGpsSpecificData.rGpsTmpRecord.trTrackData;
   OSAL_trGPSExtData *prGPSExtData =
                               &rGpsSpecificData.rGpsTmpRecord.trExtData;

   OSAL_trGnssFullRecord *prGnssFullRecord = &(rGnssProxyData.rGnssDataRecord);
   /*------------------------------Measured position-------------------------------------*/

   /*------ Lat-Lon-Alt information ------- */
   prGPSMeasuredPosition->trPosition.enPositionFormat = GPS_POS_LLA;
   prGPSMeasuredPosition->trPosition.uPosition.trPositionLLA.dLat = prGnssFullRecord->rPVTData.f64Latitude;
   prGPSMeasuredPosition->trPosition.uPosition.trPositionLLA.dLon = prGnssFullRecord->rPVTData.f64Longitude;
   prGPSMeasuredPosition->trPosition.uPosition.trPositionLLA.dAlt = prGnssFullRecord->rPVTData.f32AltitudeWGS84;
   prGPSMeasuredPosition->trPosition.uPosition.trPositionLLA.dAltMSL =
                  prGnssFullRecord->rPVTData.f32AltitudeWGS84 - prGnssFullRecord->rPVTData.f32GeoidalSeparation;
   /* Fix Status */
   if ( GNSS_FIX_TYPE_3D == prGnssFullRecord->rPVTData.rFixStatus.enMode)
   {
      prGPSMeasuredPosition->trSolutionMode.enSolutionType = GPS_SOLTYPE_LSQ_3D;
   }
   else if (GNSS_FIX_TYPE_2D == prGnssFullRecord->rPVTData.rFixStatus.enMode)
   {
      prGPSMeasuredPosition->trSolutionMode.enSolutionType = GPS_SOLTYPE_LSQ_2D;
   }
   else
   {
      prGPSMeasuredPosition->trSolutionMode.enSolutionType = GPS_SOLTYPE_NO_SOLUTION;
   }

   /* ------ Dops ------ */
   prGPSMeasuredPosition->trDOPS.dGDOP = prGnssFullRecord->rPVTData.f32GDOP;
   prGPSMeasuredPosition->trDOPS.dHDOP = prGnssFullRecord->rPVTData.f32HDOP;
   prGPSMeasuredPosition->trDOPS.dTDOP = prGnssFullRecord->rPVTData.f32TDOP;
   prGPSMeasuredPosition->trDOPS.dPDOP = prGnssFullRecord->rPVTData.f32PDOP;
   prGPSMeasuredPosition->trDOPS.dVDOP = prGnssFullRecord->rPVTData.f32VDOP;
   /*-------Time-------*/
   /*u16Year can never be zero after time fix. So this is used to check if we have time fix or not.*/
   if (0 != prGnssFullRecord->rPVTData.rTimeUTC.u16Year)
   {
      prGPSMeasuredPosition->trTime.enTimeFormat = GPS_TIME_UTC;
      prGPSMeasuredPosition->trTime.enTimeValidity = GPS_TIME_VALID;
      prGPSMeasuredPosition->trTime.uTime.trUTCTime.u16Year =
                              prGnssFullRecord->rPVTData.rTimeUTC.u16Year;
      prGPSMeasuredPosition->trTime.uTime.trUTCTime.u16Month =
                              prGnssFullRecord->rPVTData.rTimeUTC.u8Month;
      prGPSMeasuredPosition->trTime.uTime.trUTCTime.u16Day =
                              prGnssFullRecord->rPVTData.rTimeUTC.u8Day;
      prGPSMeasuredPosition->trTime.uTime.trUTCTime.u16Hour =
                              prGnssFullRecord->rPVTData.rTimeUTC.u8Hour;
      prGPSMeasuredPosition->trTime.uTime.trUTCTime.u16Minute =
                              prGnssFullRecord->rPVTData.rTimeUTC.u8Minute;
      prGPSMeasuredPosition->trTime.uTime.trUTCTime.u16Second =
                              prGnssFullRecord->rPVTData.rTimeUTC.u8Second;
      prGPSMeasuredPosition->trTime.uTime.trUTCTime.u16Millisecond =
                              prGnssFullRecord->rPVTData.rTimeUTC.u16Millisecond;
   }
   /*----------- DGPS-------------*/
   if (prGnssFullRecord->rPVTData.rFixStatus.enQuality == GNSSQUALITY_DIFFERENTIAL )
   {
      prGPSMeasuredPosition->trSolutionMode.bDGPSPosition = TRUE;
   }
   /*-----------Velocity-------------*/
   prGPSMeasuredPosition->trVelocity.enVelocityFormat = GPS_VEL_NED;
   prGPSMeasuredPosition->trVelocity.uVelocity.trVelocityNED.dVn =
                                       prGnssFullRecord->rPVTData.f32VelocityNorth;
   prGPSMeasuredPosition->trVelocity.uVelocity.trVelocityNED.dVe =
                                       prGnssFullRecord->rPVTData.f32VelocityEast;
   /* Direction of velocity vectors down and up are opposite */
   prGPSMeasuredPosition->trVelocity.uVelocity.trVelocityNED.dVd =
                                       - (prGnssFullRecord->rPVTData.f32VelocityUp);
   /*-----------Time stamp-------------*/
   prGPSMeasuredPosition->trTimeStamp.bTsValid = TRUE;
   prGPSMeasuredPosition->trTimeStamp.u32Timestamp = prGnssFullRecord->u32TimeStamp;
   /*-----------Satellites used for fix-------------*/
   prGPSMeasuredPosition->u8NoSatellitesInFix = (tU8)prGnssFullRecord->rPVTData.u16SatsUsed;
   for( u32LoopCnt =0;
        ((u32LoopCnt < OSAL_C_S32_GPS_NO_CHANNELS) && (u32LoopCnt < prGnssFullRecord->rPVTData.u16SatsVisible));
        u32LoopCnt++ )
   {
      if ( OSAL_C_U16_GNSS_SAT_USED_FOR_POSCALC ==
            prGnssFullRecord->rChannelStatus[u32LoopCnt].u16SatStatus )
      {
         prGPSMeasuredPosition->u8SvIds[u32SatIndex] =
               (tU8)prGnssFullRecord->rChannelStatus[u32LoopCnt].u16SvID;
         u32SatIndex++;
      }
   }
   if ( u32SatIndex != prGPSMeasuredPosition->u8NoSatellitesInFix )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
      "!!error u8NoSatellitesInFix u32SatIndex:%d prGPSMeasuredPosition->u8NoSatellitesInFix:%d",
                                      u32SatIndex, prGPSMeasuredPosition->u8NoSatellitesInFix  );
   }

   /*------------------------------Visibility list-------------------------------------*/

   prGPSVisibleList->u8NoVisibleSvs = (tU8)prGnssFullRecord->rPVTData.u16SatsVisible;

   for ( u32LoopCnt = 0;
         ((u32LoopCnt < OSAL_C_S32_GPS_NO_CHANNELS) && (u32LoopCnt < prGnssFullRecord->rPVTData.u16SatsVisible));
         u32LoopCnt++ )
   {
      prGPSVisibleList->trSvDir[u32LoopCnt].u8SvId =
            (tU8)prGnssFullRecord->rChannelStatus[u32LoopCnt].u16SvID;
      prGPSVisibleList->trSvDir[u32LoopCnt].dAzimuth =
            prGnssFullRecord->rChannelStatus[u32LoopCnt].f32Azimuthal;
      prGPSVisibleList->trSvDir[u32LoopCnt].dElevation =
            prGnssFullRecord->rChannelStatus[u32LoopCnt].f32Elevation;
   }

   /*------------------------------Track Data-------------------------------------*/

   prGPSTrackData->u8NoChans = (tU8)prGnssFullRecord->rPVTData.u16SatsVisible;
   OSAL_pvMemoryCopy( &(prGPSTrackData->trTime),
                      &(prGPSMeasuredPosition->trTime),
                      sizeof(OSAL_trGPSTime) );
   for ( u32LoopCnt = 0;
         ((u32LoopCnt < OSAL_C_S32_GPS_NO_CHANNELS) && (u32LoopCnt < prGnssFullRecord->rPVTData.u16SatsVisible));
         u32LoopCnt++ )
   {
      OSAL_pvMemoryCopy( &(prGPSTrackData->trChanTrackData[u32LoopCnt].trSvDir),
                         &(prGPSVisibleList->trSvDir[u32LoopCnt]),
                         sizeof(OSAL_trGPSSvDir) );

      prGPSTrackData->trChanTrackData[u32LoopCnt].u8AverageCNo =
            prGnssFullRecord->rChannelStatus[u32LoopCnt].u8CarrierToNoiseRatio;

      if ( prGPSTrackData->trChanTrackData[u32LoopCnt].u8AverageCNo
                                                               >= GNSS_PROXY_MIN_CN0)
      {
         prGPSTrackData->trChanTrackData[u32LoopCnt].enChanState =
                                                           GPS_CHANSTATE_ACQ_SUCCESS;
      }
   }

   /*------------------------------GPS Ext Data-------------------------------------*/

   OSAL_pvMemoryCopy( &(prGPSExtData->rPositionCovarianceMatrix),
                      &(prGnssFullRecord->rPVTData.rPositionCovarianceMatrix),
                      sizeof(OSAL_trCovarianceMatrix) );

   OSAL_pvMemoryCopy( &(prGPSExtData->rVelocityCovarianceMatrix),
                      &(prGnssFullRecord->rPVTData.rVelocityCovarianceMatrix),
                      sizeof(OSAL_trCovarianceMatrix) );

   /*Position processing noise values*/
   prGPSExtData->rFilteredPosition.dLat = prGnssFullRecord->rPVTData.rPositionNoiseMatrix.f32Elem0;
   prGPSExtData->rFilteredPosition.dLon = prGnssFullRecord->rPVTData.rPositionNoiseMatrix.f32Elem5;
   prGPSExtData->rFilteredPosition.dAlt = prGnssFullRecord->rPVTData.rPositionNoiseMatrix.f32Elem10;

   /*Velocity processing noise values*/
   prGPSExtData->rFilteredVelocity.dVn = prGnssFullRecord->rPVTData.rVelocityNoiseMatrix.f32Elem0;
   prGPSExtData->rFilteredVelocity.dVe = prGnssFullRecord->rPVTData.rVelocityNoiseMatrix.f32Elem5;
   prGPSExtData->rFilteredVelocity.dVd = prGnssFullRecord->rPVTData.rVelocityNoiseMatrix.f32Elem10;

}

#ifdef LOAD_SENSOR_SO

tS32 gps_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tLong sArg)
{
   (tVoid)u32FD;
   (tVoid)s32ID;
   return GpsProxy_s32IOControl(s32fun, sArg);
}

tS32 gps_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   (tVoid)ret_size;
   (tVoid)u32FD;
   (tVoid)s32ID;
   return GpsProxy_s32IORead(pBuffer, u32Size);
}

#endif

/***********************************************End of file**********************************************************/

