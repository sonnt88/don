/*********************************************************************************************************************
* FILE        : dev_gnss_main.c
*
* DESCRIPTION : This file implements GNSS Proxy driver module.
*               In GEN3 the GNSS solution consists of ST chip which is connected to UART of SCC .
*               It provides the GNSS information as standard NMEA messages and also special ST messages.
*               A software component running on the SCC collects the GNSS information from the GNSS chip and
*               sends the information to IMX via INC. It is the function of the GNSS Proxy driver on IMX to 
*               collect the incoming GNSS data from V850, parse it and populate the required OSAL parameters 
*               in its local buffers and provide it to VD-SENSOR application as and when it requests.
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 22.Aug.2013 | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -----------------------------------------------------------------------------------------------
* 03.APR.2014 | Initial version: 1.1   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                        | Made Initialization as a part of GNSS thread.
* -----------------------------------------------------------------------------------------------
* 09.FEB.2015 |         version: 1.2   | Madhu Kiran Ramachandra (RBEI/ECF5)
*                                      | Modified CRC message parsing
* -----------------------------------------------------------------------------------------------
*                                      |
*********************************************************************************************************************/

/*************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/

#include "dev_gnss_types.h"
#include "dev_gps_main.h"
#include "dev_gnss_parser.h"
#include "dev_gnss_scc_com.h"
#include "dev_gnss_trace.h"
#include "dev_gnss_fw_update.h"
#include <sys/socket.h>
#include "inc_ports.h"

//Header file for third party INC forwarder
#include "inc2soc.h"



/*************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/
trGnssProxyInfo rGnssProxyInfo;
trGnssProxyData rGnssProxyData;
trGnssProxyFwUpdateInfo rGnssProxyFwUpdateInfo;
tEnGnssFwUpdateStatus  enGnssFwUpdateStatus;
OSAL_tThreadID tid_sockRd = OSAL_ERROR;



/*************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/

static tS32 GnssProxy_s32Init(tVoid);
static tS32 GnssProxy_s32DeInit(tVoid );
static tVoid GnssProxy_vSocketReadThread(tPVoid pvArg);
static tS32 GnssProxy_s32CreateResources(tVoid);
extern tS32 GnssProxy_s32SetEpoch( const OSAL_trGnssTimeUTC *rEpochToSet );
extern tVoid GnssProxy_vWriteLastUsedSatSys( tU32 u32actGnssSatSys );
extern tS32 GnssProxy_s32GetEpochTime( OSAL_trGnssTimeUTC *rEpochSetTime );



/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32IOOpen
*
* PARAMETER   : OSAL_tenAccess enAccess: Mode in which device is opened
*               VD-Sensor always in Read-only mode
*               Software download component always opens in Write-only mode
*               This is compulsory. Behavior of dev_gnss will be different in different modes.
* RETURNVALUE : OSAL_E_NOERROR  on success
*               Valid Osal Error codes on Failure
*
* DESCRIPTION : Initializes the Gnss proxy driver and sets open flag on success.
*
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
*-------------|-----------------------|-------------------------------------
* 22.APR.2013 | Initial version: 1.0  | Niyatha S Rao (RBEI/ECF5)
* --------------------------------------------------------------------------
* 08.MAY.2013 | version: 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                       | Updated comments and traces
* --------------------------------------------------------------------------
**********************************************************************************************************************/
tS32 GnssProxy_s32IOOpen( OSAL_tenAccess enAccess )
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   rGnssProxyInfo.enAccessMode = enAccess;

   /* Device will be opened in read-only mode by VD-Sensor.
    * This will connect to GNSS Data channel on INC */
   if ( OSAL_EN_READONLY == enAccess )
   {
      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_RDOPEN, NULL );
       /* GNSS open will initialize GNSS device. So open should not be done twice */
      if( rGnssProxyInfo.enGnssProxyState != DEVICE_INITIALIZED )
      {
         //GNSS initialization
         if(OSAL_OK != GnssProxy_s32Init())
         {
            s32RetVal = OSAL_ERROR;
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:GnssProxy_s32Init FAILED. "
                                                                        "Calling De-init" );
            (tVoid)GnssProxy_s32DeInit();
         }
         else
         {

            GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "GnssProxy_s32Init SUCCESS" );
         }
      }
      /* Device is already opened. So return failure */
      else
      {
         s32RetVal = OSAL_E_ALREADYOPENED;
         GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Gnss proxy driver already opened" );
      }
   }
   /* Device will be opened in Write-Only mode by Software Download Component.
    * This will connect to GNSS firmware update channel */
   else if ( OSAL_EN_WRITEONLY == enAccess )
   {
      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_WROPEN, NULL );
      if ( OSAL_OK != GnssProxyFw_s32Init() )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Init Failed for GNSS Firmware Update" );
         s32RetVal = OSAL_E_UNKNOWN;
      }
   }
   else
   {
      s32RetVal = OSAL_E_NOACCESS;
   }

   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_s32Init
*
* PARAMETER    : NONE
*
* RETURNVALUE  : OSAL_OK  on success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : This is initialization function for GNSS.
*                1: Spawn GNSS thread and wait for the initialization to complete.
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 22.APR.2013  | Initial version: 1.0  | Niyatha S Rao (RBEI/ECF5)
* ------------------------------------------------------------------------------
* 08.MAY.2013  | version: 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*              |                       | Updated comments and traces
* ------------------------------------------------------------------------------
* 08.Sep.2013  | version: 2.0          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*              |                       | Updated the functionality for GNSS support
* ------------------------------------------------------------------------------
* 02.Apr.2014  | version: 2.1          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*              |                       | Moved Init to Gnss Thread
* ------------------------------------------------------------------------------
* 14.Nov.2014  | version: 2.2          | Madhu Kiran Ramachandra (RBEI/ECF5)
*              |                       | Retry added for Satellite system Info
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxy_s32Init(tVoid)
{

   OSAL_trThreadAttribute rThreadAttr;
   tU32 u32ResultMask = 0;
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32ErrVal = OSAL_E_NOERROR;

   //To satisfy lint
   (tVoid)s32ErrVal;
   (tVoid)u32ResultMask;
   (tVoid)s32RetVal;

   /*Update thread attributes*/
   rThreadAttr.szName       = GNSS_PROXY_SOCKET_READ_THREAD_NAME;
   rThreadAttr.u32Priority  = GNSS_PROXY_SOCKET_READ_THREAD_PRIORITY;
   rThreadAttr.s32StackSize = GNSS_PROXY_SOCKET_READ_THREAD_STACKSIZE;
   rThreadAttr.pfEntry      = GnssProxy_vSocketReadThread;
   rThreadAttr.pvArg        = OSAL_NULL;

   /* This event is used to wait till the initialization is complete */
   if( OSAL_ERROR == OSAL_s32EventCreate( GNSS_PROXY_INIT_EVENT_NAME,
                                            &rGnssProxyInfo.hGnssProxyInitEvent ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Event create failed err %lu line %lu",
                                            OSAL_u32ErrorCode(), __LINE__ );
   }
   else
   {
      /* Creating all resources and INC stuff */
      if(OSAL_ERROR == (tid_sockRd = OSAL_ThreadSpawn(&rThreadAttr)) )
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Thread spawn fail Err: %lu, line %lu",
                                               OSAL_u32ErrorCode(), __LINE__ );
      }
      /* Wait for Init to complete */
      else if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyInitEvent,
                                                 (GNSS_PROXY_EVENT_INIT_FAILED| GNSS_PROXY_EVENT_INIT_SUCCESS),
                                                 OSAL_EN_EVENTMASK_OR,
                                                 GNSS_PROXY_INIT_EVENT_WAIT_TIME,
                                                 &u32ResultMask ))
      {
         /* If there is no response from Gnss thread, only reason could be V850 is not responding. */
         if ( OSAL_E_TIMEOUT == OSAL_u32ErrorCode() )
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:NO response from V850" );
         }
         else
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Event wait fail Err: %lu, line %lu",
                                                  OSAL_u32ErrorCode(), __LINE__ );
         }
      }
      /* Init failed for some reason */
      else if ( GNSS_PROXY_EVENT_INIT_FAILED == u32ResultMask )
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Init Fail" );
         // In case of failure, clear the event and return OSAL_ERROR to the calling function.
         if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyInitEvent,
                                               (OSAL_tEventMask) ~u32ResultMask,
                                               OSAL_EN_EVENTMASK_AND))
         {
             GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event INIT_FAIL Post failed err %lu line %d",
                                                    OSAL_u32ErrorCode(), __LINE__ );
         }
      }
       // Clear the Event in case of Init Success
      else if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyInitEvent,
                                               (OSAL_tEventMask) ~u32ResultMask,
                                               OSAL_EN_EVENTMASK_AND))
      {
          GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event INIT_SUCCESS Post failed err %lu line %d",
                                                 OSAL_u32ErrorCode(), __LINE__ );
      }
      else
      {
//Communication with stub is one way i.e, Stub ==> iMX. Therefore, requests from iMX will not be read by the stub.
#ifndef GEN3X86
         // wait till initial improper INC messages are discarded
         OSAL_s32ThreadWait(1000);
         //Read device related configuration values from the OSAL registry
         GnssProxy_vReadRegistryValues();
         //! get gnss firmware version.
         //! this is needed to identify the gnss receiver chip mounted on the 
         //! board. teseo2 binary image vesion starts with 3 i.e. something like
         //! 3.x.x whereas teseo3's starts with 4 something like 4.x.x.
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_WT_TESEO_VER, NULL );
         if ( OSAL_OK != GnssProxy_s32GetTeseoFwVer())
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                                 "GNSS Init:unable to get teseo firmware version");
         }
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_WT_TESEO_FWCRC, NULL );
         //! get crc checksum of the binary image present on the gnss device.
         //! just for sanity check.
         (tVoid)GnssProxy_s32GetTeseoCRC();
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_SETTING_DEF_SAT_SYS, NULL );
         if( (tS32)OSAL_E_NOERROR != GnssProxy_s32SetSatConfigReq() )
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS Init: Unable to set DEF sat-sys" );

            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_GET_SAT_SYS, NULL );

            //get initial cfg for block-200 and 227
            s32ErrVal = GnssProxy_s32GetCfgBlk200_227();
            
            if( ((tS32)OSAL_E_NOERROR != s32ErrVal) 
                && ((tS32)OSAL_E_CANCELED != s32ErrVal) )
            {
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, 
                                    "GNSS: attempt1 failed - GnssProxy_s32GetSatConfigReq" );
               // TRIAL2
               if( (tS32)OSAL_E_NOERROR != 
                   GnssProxy_s32GetCfgBlk200_227() )
               {
                  GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  
                                       "GNSS:Init-Unable to get the sat-sys used" );
               }
            }
            
         }
#endif
         s32RetVal= OSAL_OK;
         rGnssProxyInfo.enGnssProxyState = DEVICE_INITIALIZED ;
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_INIT_SUCC, NULL );
      }
      //Releasing Init event 
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_INIT_EVENT );
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_s32CreateResources
*
* PARAMETER    : NONE
*
* RETURNVALUE  : OSAL_OK  on success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : This is a part of initialization for GNSS.
*                1: Configures socket and connects to SCC
*                2. Handles communication with SCC and exchange status.
*                3: Handles the initial configuration message received from SCC
*                4: Creates all the necessary OS resources.
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 22.APR.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxy_s32CreateResources(tVoid)
{

   /* Initialization of few parameters */
   rGnssProxyData.bIsRecordValid = FALSE;
   rGnssProxyInfo.bShutdownFlag = FALSE;
   rGnssProxyData.u32NumRecordsParsed = GNSS_PROXY_INVALID_RECORD_ID;
   rGnssProxyData.u32NumRecordsRead = GNSS_PROXY_INVALID_RECORD_ID;
   rGnssProxyData.rGnssTmpFixInfo.bChecksumError = FALSE;
   rGnssProxyInfo.hGnssProxySemaphore = OSAL_C_INVALID_HANDLE;
   rGnssProxyInfo.hGnssProxyReadEvent = OSAL_C_INVALID_HANDLE;
   rGnssProxyInfo.hGnssProxyTeseoComEvent = OSAL_C_INVALID_HANDLE;
   rGnssProxyInfo.u8SccStatus = GNSS_PROXY_COMPONENT_STATUS_UNKNOWN;
   tS32 s32RetVal = OSAL_ERROR;

   /* Do socket Config. Just connect, no communication*/
   if (OSAL_ERROR ==  GnssProxy_s32SocketSetup(GNSS_PORT))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Soc Setup Failed port %x", GNSS_PORT );
   }
   /* Exchange status and Get GNSS Configuration Info */
   else if( OSAL_ERROR == GnssProxy_s32InitCommunToSCC() )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:GnssProxy_s32InitCommunToSCC Failed" );
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );
   }
   else if( OSAL_OK != OSAL_s32SemaphoreCreate( (tCString)GNSS_PROXY_SEMAPHORE_NAME,
                                                &rGnssProxyInfo.hGnssProxySemaphore,
                                                1) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Sem create failed err %lu", OSAL_u32ErrorCode() );
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );
   }
   else if( OSAL_OK != OSAL_s32EventCreate( GNSS_PROXY_READ_WAIT_EVENT_NAME,
                                            &rGnssProxyInfo.hGnssProxyReadEvent ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Event create failed err %lu", OSAL_u32ErrorCode() );
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_SEMAPHORE );
   }
   else if( OSAL_OK != OSAL_s32EventCreate( GNSS_PROXY_TESEO_COM_WAIT_EVENT_NAME,
                                            &rGnssProxyInfo.hGnssProxyTeseoComEvent ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Event create failed err %lu", OSAL_u32ErrorCode() );
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_SEMAPHORE );
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_READ_EVENT );
   }
   else
   {
      s32RetVal = OSAL_OK;
   }

   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_s32IOControl
*
* PARAMETER    : s32FunId,  Function identifier
*                sArg , Argument to be passed to function
*
* RETURNVALUE  : OSAL_E_NOERROR  on Success
*                Respective OSAL errors on Failure
*
* DESCRIPTION  : Control functions corresponding to Proxy GNSS device
*
* HISTORY      :
*-----------------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|----------------------------------------------
* 26.APR.2013  | Initial version: 1.0| Niyatha S Rao (RBEI/ECF5)
* ----------------------------------------------------------------------------------
* 08.SEP.2013  | version: 2.0        | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*              |                     | Updated the functionality for GNSS support
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32IOControl( tS32 s32FunId, tLong sArg)
{
   tS32  s32RetVal = OSAL_E_NOERROR;
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_IOCTRL_ENTER, " %d", s32FunId);

   if( rGnssProxyInfo.enGnssProxyState == DEVICE_DEINITIALIZED )
   {
      s32RetVal = OSAL_E_TEMP_NOT_AVAILABLE;
   }
   else
   {
      switch( s32FunId )
      {

       /* Get the currently used satellite system for position calculation */
         case OSAL_C_S32_IOCTRL_GNSS_GET_SAT_SYS:
         {
            if ( OSAL_NULL == sArg )
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Invalid param to Get Sat Sys" );
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
            else
            {
               tPU32 pu32Arg = (tPU32) sArg;
               GnssProxy_vEnterCriticalSection(__LINE__);
               *pu32Arg = rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed;
               GnssProxy_vLeaveCriticalSection(__LINE__);
            }
            break;
         }
         /* Set the satellite system for position calculation */
         case OSAL_C_S32_IOCTRL_GNSS_SET_SAT_SYS:
         {
            if(OSAL_NULL != sArg)
            {
               s32RetVal = GnssProxy_s32SetSatSys( (tPU32)sArg );
               if(OSAL_E_NOERROR == (tU32)s32RetVal)
               {
                  /* Write the current used satellite system into a file, used to decide the sat sys in next boot*/
                  GnssProxy_vWriteLastUsedSatSys( *((tPU32)sArg) );
               }
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Set Sat-System: NULL param" );
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
            break;
         }
         /* Set the satellite system for position calculation using Diag Ctrl*/
         case OSAL_C_S32_IOCTRL_GNSS_DIAG_SET_SAT_SYS:
         {
            if(OSAL_NULL != sArg)
            {
               s32RetVal = GnssProxy_s32SetSatSys( (tPU32)sArg );
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Set Sat-System using Diag: NULL param" );
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
            break;
         }
         /* VD-Sensor Gets complete configuration data about GNSS with this ioctl */
         case OSAL_C_S32_IOCTRL_GNSS_GET_CONFIG_DATA:
         {
            if ( OSAL_NULL == sArg )
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Invalid param to Get config data" );
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
            else
            {
               // FW version query.
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_WAIT_TESEO_VER, NULL );
               if ( OSAL_ERROR == GnssProxy_s32GetTeseoFwVer())
               {
                  GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS: GnssProxy_s32GetTeseoFwVer Failed" );
                  // Value should be zero if Get FW version from Teseo Failed
                  rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer = 0;
               }
               // Teseo FW CRC query.
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_WAIT_TESEO_FWCRC, NULL );
               if( OSAL_ERROR == GnssProxy_s32GetTeseoCRC() )
               {
                  // Value should be zero if Get CRC from Teseo Failed
                  rGnssProxyInfo.rGnssConfigData.u32GnssRecvFwCrc = 0;
                  GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS: GnssProxy_s32GetTeseoCRC Failed" );
               }
               OSAL_trGnssConfigData *prGnssConfigData = (OSAL_trGnssConfigData *)sArg;
               OSAL_pvMemoryCopy( prGnssConfigData,
                                  &rGnssProxyInfo.rGnssConfigData,
                                  sizeof(OSAL_trGnssConfigData) );
            }
            break;
         }
         case OSAL_C_S32_IOCTL_GNSS_GET_NMEA_RECVD_LIST:
         {
            if ( OSAL_NULL == sArg )
            {
                 GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Get NMEA LIST IOCTL: NULL param" );
                 s32RetVal = OSAL_E_INVALIDVALUE;
            }
            else
            {
               s32RetVal = GnssProxy_s32GetNmeaRecvdList( (tPU32) sArg );
            }
            break;
         }
         /* This is a trigger to flash binary image to GNSS chip */
         case OSAL_C_S32_IOCTL_GNSS_FLASH_IMAGE:
         {
            enGnssFwUpdateStatus=GNSS_FW_UPDATE_FAILED;       
            if ( OSAL_EN_WRITEONLY != rGnssProxyInfo.enAccessMode )
            {
               s32RetVal = OSAL_E_NOACCESS;
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                      "Teseo Firmware update functionality available only in WRITE ONLY mode" );
               
            }
            else if ( OSAL_NULL != sArg )
            {
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "\n TESEO FIRMWARE update started \n" );
               s32RetVal = GnssProxyFw_s32ConfigBinOpts( (trImageOptions *) sArg );
               if(s32RetVal == (tS32)OSAL_E_NOERROR)//to avoid lint 
               {          
                  enGnssFwUpdateStatus=GNSS_FW_UPDATE_STARTED;  
               }  
               
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "NULL param IOCTL_GNSS_FLASH_IMAGE" );
               s32RetVal = OSAL_E_INVALIDVALUE;
               
            }
            break;
         }

         //Set epoch.
         //fix this once VDS is adopted.
         case OSAL_C_S32_IOCTL_GNSS_SET_EPOCH:
         {
            if ( OSAL_NULL != sArg )
            {
               s32RetVal = GnssProxy_s32SetEpoch( (OSAL_trGnssTimeUTC*)sArg );
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "NULL param IOCTL_GNSS_SET_EPOCH" );
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
            break;
         }
         // Flush sensor buffer command to SCC
         case OSAL_C_S32_IOCTL_GNSS_FLUSH_SENSOR_DATA:
         {
            #ifndef GEN3X86
               if ( OSAL_ERROR == GnssProxy_s32FlushSccBuff() )
               {
                  s32RetVal = OSAL_ERROR;
               }
            #endif
            break;
         }
         case OSAL_C_S32_IOCTL_GNSS_GET_GNSS_CHIP_CRC:
         {
            if ( OSAL_EN_WRITEONLY != rGnssProxyInfo.enAccessMode )
            {
               s32RetVal = OSAL_E_NOACCESS;
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                      "FW:Teseo Firmware update functionality available only in WRITE ONLY mode" );
            }
            else if ( OSAL_NULL == sArg )
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "FW:NULL Pntr line %d", __LINE__ );
            }
            else if( OSAL_ERROR == GnssProxyFw_s32GetTeseoCRC() )
            {
               // Value should be zero if Get CRC from Teseo Failed
               rGnssProxyInfo.rGnssConfigData.u32GnssRecvFwCrc = 0;
               s32RetVal = OSAL_E_TEMP_NOT_AVAILABLE;
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:FW:GnssProxy_s32GetTeseoCRC Failed" );
            }
            else
            {
               tPU32 pu32Arg = (tPU32) sArg;
               *pu32Arg = rGnssProxyInfo.rGnssConfigData.u32GnssRecvFwCrc;
            }

            break;
         }
         case OSAL_C_S32_IOCTL_GNSS_GET_EPOCH:
         {
             GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "GET Epoch function running"
                                                                          " with ID : %d", s32FunId );
             if(OSAL_ERROR == GnssProxy_s32GetEpochTime((OSAL_trGnssTimeUTC *)sArg))
             {
                 GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GetEpochSetTime function returned an error" );
            }

            break;
         }
         case OSAL_C_S32_IOCTL_GNSS_SET_CHIP_TYPE:
         {
            s32RetVal = GnssProxyFw_s32SetChipType( (tEnGnssChipType) sArg );
            if ( OSAL_OK == s32RetVal)
            {
               s32RetVal = OSAL_E_NOERROR;
            }
            break;
         }
         default:
         {
            /* All other commands are either not necessary or not implemented yet. */
            s32RetVal = OSAL_E_NOTSUPPORTED;
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "FUN ID %d Not Supported", s32FunId );
            break;
         }
      }
   }
   return(s32RetVal);
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vEnterCriticalSection
*
* PARAMETER   : u32LineNum: Line number from where it is called.
*
* RETURNVALUE : NONE
*
* DESCRIPTION : Enter the critical section.
*
* HISTORY     :
*-------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|------------------------------------------
* 26.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* ------------------------------------------------------------------------------
* 8.MAR.2013  | Version 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | Added Param u32LineNum. This aids debugging
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vEnterCriticalSection( tU32 u32LineNum )
{
   GnssProxy_vTraceOut( TR_LEVEL_USER_4 , GNSSPXY_SEM_WAIT_CAL, " %lu", u32LineNum );
   if( rGnssProxyInfo.hGnssProxySemaphore != OSAL_C_INVALID_HANDLE )
   {
      if ( OSAL_OK != OSAL_s32SemaphoreWait( rGnssProxyInfo.hGnssProxySemaphore,
                                             OSAL_C_TIMEOUT_FOREVER ) )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS , GNSSPXY_DEF_TRC_RULE, "Sem wait Failed line %lu", u32LineNum );
      }
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS , GNSSPXY_DEF_TRC_RULE,  "Sem wait:Handle Invalid line %lu", u32LineNum );
   }
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vLeaveCriticalSection
*
* PARAMETER   : u32LineNum: Line number from where it is called.
*
* RETURNVALUE : NONE
*             
* DESCRIPTION : Leave the critical section.
*
* HISTORY     :
*-----------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 26.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* -----------------------------------------------------------------------------
* 8.MAR.2013  | Version 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | Added Param u32LineNum. This aids debugging
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vLeaveCriticalSection( tU32 u32LineNum )
{

   GnssProxy_vTraceOut( TR_LEVEL_USER_4 , GNSSPXY_SEM_POST_CAL, " %lu", u32LineNum );
  
   if( rGnssProxyInfo.hGnssProxySemaphore != OSAL_C_INVALID_HANDLE )
   {
      if ( OSAL_OK != OSAL_s32SemaphorePost(rGnssProxyInfo.hGnssProxySemaphore) )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS , GNSSPXY_DEF_TRC_RULE, "Sem post Failed line %lu", u32LineNum );
      }
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS , GNSSPXY_DEF_TRC_RULE,  "Sem post:Handle Invalid line %lu", u32LineNum );
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32IORead
*
* PARAMETER   : pBuffer, pointer to buffer(must be of type OSAL_trGPSRecordHeader)
*               u32maxbytes, size of pBuffer
*
* RETURNVALUE : Number of bytes to read on success
*               Respective OSAL errors on Failure
*               Possible error codes:
*               OSAL_E_NOSPACE, OSAL_E_TIMEOUT
*
* DESCRIPTION : This function fills a user supplied buffer with information about the latest
*               GNSS data record.  The contents of the data record are retrieved through a call
*               to OSAL_s32IOControl with the command being OSAL_C_S32_IOCTL_GPS_READ_RECORD
*               and the argument being a pointer to a structure, which contains a pointer to a
*               data buffer and a record id.  The record ID must correspond to the record ID
*               that GnssProxy_s32IORead wrote into pBuffer.
*               The data record contents are written into the buffer au8DataRecordBuffer.  The
*               function GnssProxy_s32IOControl copies these buffered data to the client's(VDSensor's) buffer.
*
* HISTORY     :
*----------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|---------------------------------------------
* 26.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* ---------------------------------------------------------------------------------
* 8.MAR.2013  | Version 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | customized read for Gen3. Also removed 
*             |                      | multiple returns from the function.
* ---------------------------------------------------------------------------------
* 08.MAY.2013 | version: 2.0         | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                      | Updated the functionality for GNSS support
* ---------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32IORead(tPS8 pBuffer, tU32 u32maxbytes)
{

   OSAL_trGnssFullRecord *ptrGnssFullRecord = (OSAL_trGnssFullRecord*)(tPVoid)pBuffer;
   tS32 s32RetVal = OSAL_ERROR;
   tU32 u32ResultMask = 0;

   /* rGnssProxyData.rGnssTmpRecord and bIsRecordValid are accessed in two
      thread contexts. In application (VD-Sensor) thread as well as in
      GnssProxy_vSocketReadThread. So we need synchronization mechanism here */
   GnssProxy_vEnterCriticalSection(__LINE__);

   /* If socket communication fails, device will be de-initialized but not closed */
   if( rGnssProxyInfo.enGnssProxyState == DEVICE_DEINITIALIZED )
   {
      s32RetVal = OSAL_E_TEMP_NOT_AVAILABLE;
   }
   else if (OSAL_NULL == ptrGnssFullRecord )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "NULL pointer to read function" );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   /* Check the available space in the client's buffer. */
   else if( u32maxbytes < sizeof(OSAL_trGnssFullRecord) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Invalid Size" );
      s32RetVal = OSAL_E_NOSPACE;
   }
   /* Make sure a record is available. */
   else if( rGnssProxyData.bIsRecordValid == TRUE )
   {
      OSAL_pvMemoryCopy( ptrGnssFullRecord,
                         &rGnssProxyData.rGnssDataRecord,
                         sizeof (OSAL_trGnssFullRecord) );
      rGnssProxyData.bIsRecordValid = FALSE;
      s32RetVal = sizeof (OSAL_trGnssFullRecord);
   }
   else
   {
      /* wait on event with 1000ms time out for data from SCC */
      rGnssProxyInfo.bThreadWaitInRead = TRUE;
      /* We are about to block ourself, So leave critical section */
      GnssProxy_vLeaveCriticalSection(__LINE__);

      if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyReadEvent,
                                            (GNSS_PROXY_EVENT_DATA_READY | GNSS_PROXY_EVENT_SHUTDOWN_READ_EVENT),
                                            OSAL_EN_EVENTMASK_OR,
                                            rGnssProxyInfo.u16ReadWaitTimeMs,
                                            &u32ResultMask ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event wait failed err %d line %d",
                                                OSAL_u32ErrorCode(),__LINE__ );
   
      }
      /* Event wait returned with a success, Clear the event */
      else if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyReadEvent,
                                               (OSAL_tEventMask) ~u32ResultMask,
                                               OSAL_EN_EVENTMASK_AND))
      {
          GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event Post failed err %lu line %d",
                                                 OSAL_u32ErrorCode(), __LINE__ );
      }
      GnssProxy_vEnterCriticalSection(__LINE__);
      rGnssProxyInfo.bThreadWaitInRead = FALSE;
      /* If shutdown event is received or if shutdown flag is set, return 
         s32RetVal = 0 and release the event */
      if (( u32ResultMask == GNSS_PROXY_EVENT_SHUTDOWN_READ_EVENT ) ||
         ( rGnssProxyInfo.bShutdownFlag == TRUE ))
      {
         s32RetVal = 0;
         GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_READ_EVENT );
      }
      /* Event wait is done. Now check if we have a valid record */
      else if ( rGnssProxyData.bIsRecordValid == TRUE )
      {
         OSAL_pvMemoryCopy( ptrGnssFullRecord,
                            &rGnssProxyData.rGnssDataRecord,
                            sizeof (OSAL_trGnssFullRecord) );
         rGnssProxyData.bIsRecordValid = FALSE;
         s32RetVal = sizeof (OSAL_trGnssFullRecord);
      }
      else
      {
         s32RetVal = OSAL_E_TIMEOUT;
      }
      /* This is to keep a count of number of records parsed and number of records read by VD-Sensor. */
      if (sizeof (OSAL_trGnssFullRecord) == (size_t)s32RetVal )
      {
         rGnssProxyData.u32NumRecordsRead++;
      }
   }
   GnssProxy_vLeaveCriticalSection(__LINE__);
   GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Read end : %d", s32RetVal);

   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32IOWrite
*
* PARAMETER   : pBuffer: pointer buffer holding the data to be written
*               u32maxbytes: Number of bytes
*
* RETURNVALUE : Number of bytes of data written Or
*               Valid Osal Error codes on Failure
*
* DESCRIPTION : This is used to transfer firmware image or boot loader to Teseo.
*
* HISTORY     :
*----------------------------------------------------------------------------------
* Date        |       Version         | Author & comments
*-------------|-----------------------|--------------------------------------------
* 09.DEC.2013 | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ---------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32IOWrite( const char* pBuffer, tU32 u32maxbytes)
{
   tS32 s32RetVal = OSAL_ERROR;

   if ( OSAL_NULL == pBuffer )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:NULL write pointer");
      s32RetVal = OSAL_E_INVALIDVALUE;
      enGnssFwUpdateStatus=GNSS_FW_UPDATE_FAILED;
   }
   else if ( 0 == u32maxbytes )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Invalid data size: 0");
      s32RetVal = OSAL_E_INVALIDVALUE;
      enGnssFwUpdateStatus=GNSS_FW_UPDATE_FAILED;
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_WR_SZ, " %d", u32maxbytes );
      /* This will check the current state of the download in state diagram
        and routes the call accordingly to flash firmware or boot loader */
      if (OSAL_ERROR == GnssProxyFw_s32SendBinToTeseo((const char*)pBuffer, u32maxbytes ))
      {
         s32RetVal = OSAL_E_UNKNOWN;
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Write Failed" );
         enGnssFwUpdateStatus=GNSS_FW_UPDATE_FAILED;
      }
      else
      {
         s32RetVal = (tS32)u32maxbytes;
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32IOClose
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_E_NOERROR  on success
*                 OSAL_ERROR on Failure
*
* DESCRIPTION : Close the GNSS device
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 22.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* --------------------------------------------------------------------------
* 8.MAR.2013  | Version 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | Moved Semaphore release to Socket read thread
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32IOClose(tVoid)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   
   GnssProxy_vTraceOut(TR_LEVEL_USER_1, GNSSPXY_DEV_CLS, NULL );

   switch ( rGnssProxyInfo.enAccessMode )
   {
      case OSAL_EN_READONLY:
      {
         if(OSAL_OK != GnssProxy_s32DeInit())
         {
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GnssProxy_s32DeInit failed" );
            s32RetVal = OSAL_ERROR;
         }
         else
         {
            rGnssProxyInfo.enGnssProxyState = DEVICE_CLOSED_GNSS;
         }
         break;
      }
      case OSAL_EN_WRITEONLY:
      {  
         /*Checking Teseo Firmware update status  put traces in Error Memory in case of
         update failure as a fix for GMMY17-3213 Ticket */
         if( (enGnssFwUpdateStatus == GNSS_FW_UPDATE_STARTED)||(enGnssFwUpdateStatus==GNSS_FW_UPDATE_FAILED) )
         { 
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Teseo Firmware update  failed" );            
         }
         enGnssFwUpdateStatus=GNSS_FW_UPDATE_NOT_ACTIVE;
         GnssProxyFw_s32DeInit();
         break;
      }
      default:
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:GnssProxy_s32DeInit Access mode : %d",
                                              rGnssProxyInfo.enAccessMode );
         s32RetVal = OSAL_ERROR;
         break;
      }
   }
   return(s32RetVal);
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32DeInit
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_OK  on success
*               OSAL_ERROR on Failure
*
* DESCRIPTION : This is the De-initialization function for Gnss Dispatcher.
*               1. Update the driver status as closed
*               2. Trigger component shutdown by releasing the socket.
*               3. All other resources will be released by socket read thread.
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 22.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* -------------------------------------------------------------------------
* 8.MAR.2013  | Version 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | Updated function header
* -------------------------------------------------------------------------
* 10.FEB.2016|  version 2.6         | Srinivas Prakash Anvekar (RBEI/ECF5)
*            |                      | CFG3-1729 : Send inactive status to 
*            |                      | to SCC during Shutdown.
*********************************************************************************************************************/
static tS32 GnssProxy_s32DeInit(tVoid )
{
   tS32 s32ErrorVal = 0;

   //To satisfy lint
   (tVoid)s32ErrorVal;
   
   /* Send Inactive status to SCC. This would switch to buffering mode only if PoS is also
      in inactive state */
   if ( rGnssProxyInfo.s32SocketFD != OSAL_NULL )
   {
      if( OSAL_OK != GnssProxy_s32SendStatusCmd( GNSS_PROXY_COMPONENT_STATUS_NOT_ACTIVE, 
          GNSS_PROXY_COMPONENT_VERSION ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
            " GNSS : Status sending failed during shutdown ");
      }
   }

   /* Set shutdown flag to indicate close event to the GNSS socket read thread*/
   rGnssProxyInfo.bShutdownFlag = TRUE;

   //Posting event to shutdown third party INC forwarder
   Inc2Soc_vPostDrivShutdown( INC2SOC_DEV_TYPE_GNSS );

   rGnssProxyInfo.enGnssProxyState = DEVICE_DEINITIALIZED;
   GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );

   //Wait for the GnssProxy_vSocketReadThread thread to exit
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "GNSS: Waiting for "
                        "GnssProxy_vSocketReadThread to exit" );
   s32ErrorVal = OSAL_s32ThreadJoin( tid_sockRd, GNSS_PROXY_SOCK_READ_EXIT_TIMEOUT );
   if( s32ErrorVal != 0)
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GNSS: pthread_join on"
                        " GnssProxy_vSocketReadThread returned error: %d", s32ErrorVal);
   }

   // Release all resources
   GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_SEMAPHORE );
   GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_READ_EVENT );
   GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT );

   GnssProxy_vTraceOut( TR_LEVEL_SYSTEM, GNSSPXY_SHTDWN_SET, NULL );
   return OSAL_OK;
}

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_PROXY_SocketReadThread
*
* PARAMETER    : NONE
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : This thread will be waiting for data from SCC. Based on the message ID
*                of the received data, call respective function to parse the data further.
* HISTORY      :
*--------------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|-----------------------------------------------
* 22.APR.2013  | Initial version: 1.0  | Niyatha S Rao (RBEI/ECF5)
* -------------------------------------------------------------------------------------
* 08.MAY.2013  | version: 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*              |                       | Updated comments and added support to parse
*              |                       | run time config message
* -------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vSocketReadThread( tPVoid pvArg )
{
   tS32 s32ErrChk = OSAL_ERROR;
   tU32 u32RetryCnt;

   /* Dummy Argument */
   (tVoid)pvArg;
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_SCKT_THRD, NULL );

   /* Sucessive Initialization attempts in case od failure */
   for ( u32RetryCnt = 0; 
         ((u32RetryCnt < GNSS_PROXY_INIT_RETRY_CNT) && (OSAL_ERROR == s32ErrChk));
         u32RetryCnt++ )
   {
      /* Create all resources needed and also connect to SCC */
      if( OSAL_ERROR == (s32ErrChk = GnssProxy_s32CreateResources()) )
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Fail GnssProxy_s32CreateResources" );
         OSAL_s32ThreadWait(GNSS_PROXY_RETRY_INTERVALL_MS);
      }
   }

   /* POST Init success event */
   if ( OSAL_OK == s32ErrChk )
   {
      if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyInitEvent,
                                           GNSS_PROXY_EVENT_INIT_SUCCESS,
                                           OSAL_EN_EVENTMASK_OR ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Event Post failed err %lu line %u",
                                              OSAL_u32ErrorCode(), __LINE__ );
         s32ErrChk = OSAL_ERROR;
      }
   }
   /* POST Init Fail event */
   else if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyInitEvent,
                                             GNSS_PROXY_EVENT_INIT_FAILED,
                                             OSAL_EN_EVENTMASK_OR ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:Event Post failed err %lu line %u",
                                            OSAL_u32ErrorCode(), __LINE__ );
   }      

   if (OSAL_OK == s32ErrChk)
   {
      if(OSAL_ERROR == Inc2Soc_s32CreateResources( INC2SOC_DEV_TYPE_GNSS ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GNSS: INC fwdr resources for GNSS could not be created");
      }

      while( TRUE != rGnssProxyInfo.bShutdownFlag )
      {
         /* Wait for data from SCC */
         s32ErrChk = GnssProxy_s32GetDataFromScc(GNSS_PROXY_MAX_PACKET_SIZE);
         
         /* CHeck if read on socket passed */
         if( s32ErrChk > 0 )
         {
            rGnssProxyInfo.u32RecvdMsgSize = (tU32)s32ErrChk;
            GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_INC_MSG_RCVD, " %x , %d",
                                 (tS32)rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID], s32ErrChk );

            /* first byte says the type of message */
            switch (rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID])
            {
               /* Status update message from SCC */
               case GNSS_PROXY_SCC_R_COMPONENT_STATUS_MSGID:
               {
                  (tVoid)GnssProxy_s32HandleStatusMsg();
                  break;
               }
               /* Control message */
               case GNSS_PROXY_SCC_R_CONFIG_START_MSGID:
               {
                  (tVoid)GnssProxy_s32HandleConfigMsg();
                  break;
               }
               /* Command reject message from SCC */
               case GNSS_PROXY_SCC_R_REJECT_MSGID:
               {
                  GnssProxy_vHandleCommandReject();
                  break;
               }
               /* GNSS data message from SCC */
               case GNSS_PROXY_SCC_R_DATA_START_MSGID:
               {
                  Inc2Soc_vWriteDataToBuffer(INC2SOC_DEV_TYPE_GNSS);
                  GnssProxy_vHandleGnssData();
                  break;
               }
               default:
               {
                  GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "GnssProxy_vSocketReadThread: Unknown Msg ID %x",
                                       (tS32)rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] );
                  break;
               }
            }
         }
         else if ( TRUE != rGnssProxyInfo.bShutdownFlag )
         {
            (tVoid)OSAL_s32ThreadWait(GNSS_PROXY_RETRY_INTERVALL_MS);
         }
         else
         {
            GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEV_SHTDWN, NULL );
         }

         OSAL_pvMemorySet( (void*)rGnssProxyInfo.u8RecvBuffer,
                           0,
                           rGnssProxyInfo.u32RecvdMsgSize );
      }
   
      /* Application may be waiting for data. Release it by posting shutdown event */
      if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyReadEvent,
                                          (OSAL_tEventMask) GNSS_PROXY_EVENT_SHUTDOWN_READ_EVENT,
                                          OSAL_EN_EVENTMASK_OR ))
      {
          GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event Post failed err %lu line %d",
                                                OSAL_u32ErrorCode(), __LINE__ );
      }
      /* Application may be waiting for Sat-Sys-Events. Release it by posting shutdown event */
      if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                          (OSAL_tEventMask) GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT,
                                          OSAL_EN_EVENTMASK_OR ))
      {
          GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event Post failed err %lu line %d",
                                                OSAL_u32ErrorCode(), __LINE__ );
      }
   }

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_SHTDWN_COMP, NULL );
   OSAL_vThreadExit();
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vReleaseResource
*
* PARAMETER   : u32Resource: Resource to be released
*
* RETURNVALUE : None
*
* DESCRIPTION  : This function is used to release the acquired resources.
*
* HISTORY      :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 22.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* --------------------------------------------------------------------------------
* 8.MAR.2013  | Version 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | Added u32Resource parameter and introduced
*             |                      | semaphore release functionality
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vReleaseResource(tU32 u32Resource )
{

   /* Successive ReleaseResource calls on the same resource should not try to clear the resource again. 
           Therefore, assign Invalid OR Null (whichever is applicable) to the resource in the first success case.*/
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, 
                        "GNSS: Releasing Resource: %lu", u32Resource );

   switch (u32Resource)
   {
      /* Release socket  */
   case GNSS_PROXY_RESOURCE_RELSEASE_SOCKET :
      {
         if( OSAL_NULL != rGnssProxyInfo.s32SocketFD )
         {

            if ( 0 != close ( rGnssProxyInfo.s32SocketFD ) )
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                                    "socket close Failed" );
            }
            else
            {
                GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_CLS_SCK_SUC, NULL );
            }
            rGnssProxyInfo.s32SocketFD = OSAL_NULL;
         }
         break;
      }
      /* Release semaphore */
   case GNSS_PROXY_RESOURCE_RELEASE_SEMAPHORE:
      {
         if( rGnssProxyInfo.hGnssProxySemaphore != OSAL_C_INVALID_HANDLE )
         {
            if( OSAL_OK != OSAL_s32SemaphoreClose( rGnssProxyInfo.hGnssProxySemaphore ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "SEM close failed err %lu",
                                                      OSAL_u32ErrorCode() );
            }
            else if( OSAL_OK != OSAL_s32SemaphoreDelete( GNSS_PROXY_SEMAPHORE_NAME ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "SEM  Delete failed err %lu",
                                                      OSAL_u32ErrorCode() );
               rGnssProxyInfo.hGnssProxySemaphore = OSAL_C_INVALID_HANDLE;
            }
            else
            {
               rGnssProxyInfo.hGnssProxySemaphore = OSAL_C_INVALID_HANDLE;
            }
         }
         break;
      }
     case GNSS_PROXY_RESOURCE_RELEASE_READ_EVENT:
     {
         if( rGnssProxyInfo.hGnssProxyReadEvent != OSAL_C_INVALID_HANDLE )
         {
            if( OSAL_OK != OSAL_s32EventClose( rGnssProxyInfo.hGnssProxyReadEvent ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Read Event close failed err %lu",
                                                      OSAL_u32ErrorCode() );
            }
            else if( OSAL_OK != OSAL_s32EventDelete( GNSS_PROXY_READ_WAIT_EVENT_NAME ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Read Event Delete failed err %lu",
                                                      OSAL_u32ErrorCode() );
               rGnssProxyInfo.hGnssProxyReadEvent = OSAL_C_INVALID_HANDLE;
            }
            else
            {
               rGnssProxyInfo.hGnssProxyReadEvent = OSAL_C_INVALID_HANDLE;;
            }
         }
         break;
     }
     case GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT:
     {
         if( rGnssProxyInfo.hGnssProxyTeseoComEvent!= OSAL_C_INVALID_HANDLE )
         {
            if( OSAL_OK != OSAL_s32EventClose( rGnssProxyInfo.hGnssProxyTeseoComEvent ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Teseo com event close failed err %lu",
                                                      OSAL_u32ErrorCode() );
            }
            else if( OSAL_OK != OSAL_s32EventDelete( GNSS_PROXY_TESEO_COM_WAIT_EVENT_NAME ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Teseo com event delete failed err %lu",
                                                      OSAL_u32ErrorCode() );
               rGnssProxyInfo.hGnssProxyTeseoComEvent = OSAL_C_INVALID_HANDLE;
            }
            else
            {
               rGnssProxyInfo.hGnssProxyTeseoComEvent = OSAL_C_INVALID_HANDLE;;
            }
         }
        break;
     }
     case GNSS_PROXY_RESOURCE_RELEASE_INIT_EVENT:
     {
         if( rGnssProxyInfo.hGnssProxyInitEvent!= OSAL_C_INVALID_HANDLE )
         {
            if( OSAL_OK != OSAL_s32EventClose( rGnssProxyInfo.hGnssProxyInitEvent ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Init Event close failed err %lu",
                                                      OSAL_u32ErrorCode() );
            }
            else if( OSAL_OK != OSAL_s32EventDelete( GNSS_PROXY_INIT_EVENT_NAME ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Init Event Delete failed err %lu",
                                                      OSAL_u32ErrorCode() );
               rGnssProxyInfo.hGnssProxyInitEvent = OSAL_C_INVALID_HANDLE;
            }
            else
            {
               rGnssProxyInfo.hGnssProxyInitEvent = OSAL_C_INVALID_HANDLE;;
            }
         }
        break;
     }
     default :
     {
        GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "default @ line %d", __LINE__ );
        break;
     }
   }
}

#ifdef LOAD_SENSOR_SO
tS32 gnss_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
   (tVoid)app_id;
   (tVoid)pu32FD;
   (tVoid)enAccess;
   (tVoid)szName;
   (tVoid)s32Id;
   return GnssProxy_s32IOOpen(enAccess);
}

tS32 gnss_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   return GnssProxy_s32IOClose();
}

tS32 gnss_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tLong sArg)
{
   (tVoid)u32FD;
   (tVoid)s32ID;
   return GnssProxy_s32IOControl(s32fun, sArg);
}

tS32 gnss_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   (tVoid)ret_size;
   (tVoid)u32FD;
   (tVoid)s32ID;
   return GnssProxy_s32IORead(pBuffer, u32Size);
}

tS32 gnss_drv_io_write(tS32 s32ID, tU32 u32FD, const char* pBuffer, tU32 u32Size, tU32 *ret_size)
{
   (tVoid)ret_size;
   (tVoid)u32FD;
   (tVoid)s32ID;
   return GnssProxy_s32IOWrite( (const char*) pBuffer, u32Size);
}

#endif

/***********************************************End of file**********************************************************/
