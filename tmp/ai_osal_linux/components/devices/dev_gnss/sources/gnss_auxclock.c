/*********************************************************************************************************************
* FILE        : gnss_auxclock.c
*
* DESCRIPTION : This implements aux clock feature for VD-Sensor. dev_gnss adds sync feature to synchronize with the
*               system time of V850.
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 22.APR.2013 | Initial version: 1.0   | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------------------------------
*********************************************************************************************************************/


/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include <time.h>

/* Basic OSAL includes */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "dev_gnss_types.h"
#include "dev_gnss_trace.h"
/* get /dev/auxclock OSALIO API interface function prototypes*/
//#include "auxclock.h"
#include <fcntl.h>
#include "gnss_auxclock.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/**
 * @def AUXCLOCK_C_S32_IO_VERSION
 * return value for ioctrl call OSAL_C_S32_IOCTRL_VERSION
*/
#define AUXCLOCK_C_S32_IO_VERSION          ((tS32)0x00000101)   /* Read as v1.00 */

/**
 * @def AUX_SCAN_RATE_IN_NS
 * return value for ioctrl call OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETCYCLETIME
 * scan rate in ns
*/
#define  AUX_SCAN_RATE_IN_NS  (1000000)

/**
 * @def CONVERT_NS_2_HZ
 * macro for ioctrl call OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETRESOLUTION
 * to convert time in ns from @ref AUX_SCAN_RATE_IN_NS to Hz
*/
#define  CONVERT_NS_2_HZ(time_in_ns)                        \
                (tU16)((tU32)1000000000/((tU32)(time_in_ns)))

/* As division is involved, this average calculation may take significant CPU time.
   It is sufficient to calculate average for first 1000 samples and keep the offset constant there after. */
#define AUX_CLOCK_MAX_SYNC_COUNT (1000)

#define AUX_CLOCK_SHM_NAME (const char *)("AUX_CLK_SHM_NAME")

#define AUX_CLOCK_INIT_TIMEOUT (OSAL_tMSecond)(500)

#define AUX_CLOCK_INIT_SEM_VALUE (tU32)(0)
/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

typedef struct
{
      tS32 s32V850TsDiff;
      tU32 u32NumProcAttached;
} trGnssSharedData;


typedef struct 
{
   tU32 u32AvgCntr;
   OSAL_tSemHandle h_AuxClkInitSem;
   tS32 h_AuxClkShm;
   trGnssSharedData *prGnssSharedData;
   tBool bGnssAuxClkInitSuccess;
}trAuxClkInfo;

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

static trAuxClkInfo rAuxClkInfo;

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static tS32 AuxClk_s32GetPlainAuxClkVal( OSAL_trIOCtrlAuxClockTicks* prIOAuxData, tU32 u32maxbytes );
static tS32 AuxClk_s32InitShm(tVoid);
static tVoid AuxClock_vEnterCriticalSection( tU32 u32LineNo );
static tVoid AuxClock_vLeaveCriticalSection( tU32 u32LineNo );

/************************************************************************
|function prototype (scope: module-global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|AUXCLOCK OSAL Device Driver API
|-----------------------------------------------------------------------*/
   
/*********************************************************************************************************************
* FUNCTION     : AUXCLOCK_vInit
*
* PARAMETER   : NONE
*
* RETURNVALUE : None
*
* DESCRIPTION : Enter into critical section.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |      Version       | Author & comments
* -------------------------------------------------------------------------
* 24.JUN.2014 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | sensor_proxy\sharedlib\sharedlibentry.c from 
              |                      | constructor sensor_process_attach.
* -------------------------------------------------------------------------
*********************************************************************************************************************/

tVoid AUXCLOCK_vInit()
{
   tBool bErrOccoured = FALSE;
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_AUX_INIT, " %d", getpid() );

   /* Semaphore is used for sync b/w precesses */
   if ( OSAL_OK !=  OSAL_s32SemaphoreCreate( AUX_CLOCK_INIT_SEM,
                                  &rAuxClkInfo.h_AuxClkInitSem,
                                  AUX_CLOCK_INIT_SEM_VALUE ))
   {
     if ( OSAL_E_ALREADYEXISTS != OSAL_u32ErrorCode() )
     {
       bErrOccoured = TRUE;
       GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
              "AUXCLOCK_vInit():line %lu err %lu",__LINE__ ,OSAL_u32ErrorCode());
     }
     /* If sem already exists just open and use it */
     else if( OSAL_OK != OSAL_s32SemaphoreOpen( AUX_CLOCK_INIT_SEM,
                                     &rAuxClkInfo.h_AuxClkInitSem ))
     {
       bErrOccoured = TRUE;
       GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
          "AUXCLOCK_vInit():line %lu err %lu",__LINE__ , OSAL_u32ErrorCode() );
     }
     /* Wait until First process does initialization */
     else if ( OSAL_OK != OSAL_s32SemaphoreWait( rAuxClkInfo.h_AuxClkInitSem,
                                                 AUX_CLOCK_INIT_TIMEOUT ))
     {
        bErrOccoured = TRUE;
        GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
         "AUXCLOCK_vInit():line %lu err %lu",__LINE__ , OSAL_u32ErrorCode() );
     }
     else if( 0 > ( rAuxClkInfo.h_AuxClkShm = shm_open( AUX_CLOCK_SHM_NAME,
                                           (O_RDWR),
                                           0)))
     {
        //open shared memory failed
        bErrOccoured = TRUE;
        GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
             "AUXCLOCK_vInit():line %lu err %d", __LINE__ , errno );
     }
   }
   /* Initialization from first process */
   else
   {
     /* Initialize shared memory */
      if ( OSAL_OK != AuxClk_s32InitShm())
      {
         bErrOccoured = TRUE;
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                         "AuxClk_s32InitShm():line %lu ",__LINE__  );
      }
   }

   /* Map shared memory into process virtual address space */
   if( MAP_FAILED == ( rAuxClkInfo.prGnssSharedData =
                                    (trGnssSharedData *)mmap( NULL,
                                    sizeof(trGnssSharedData),
                                    (PROT_WRITE|PROT_READ),
                                    MAP_SHARED,
                                    rAuxClkInfo.h_AuxClkShm,
                                    0 ) ))
   {
      // print error for failed shm creation
      bErrOccoured = TRUE;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
            "AUXCLOCK_vInit():line %lu err %d", __LINE__, errno );
      close(rAuxClkInfo.h_AuxClkShm);
   }

   /* If Initialization is successful use Gnss_Aux-clock else use plain Aux-Clock */
   if ( TRUE == bErrOccoured )
   {
      rAuxClkInfo.bGnssAuxClkInitSuccess = FALSE;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "AUX CLK INIT FAILED" );
   }
   else if ( OSAL_NULL != rAuxClkInfo.prGnssSharedData )
   {
      rAuxClkInfo.bGnssAuxClkInitSuccess = TRUE;
      rAuxClkInfo.prGnssSharedData->u32NumProcAttached++;
      /* Post semaphore to allow others who open AUX-Clock*/
      if ( OSAL_OK != OSAL_s32SemaphorePost( rAuxClkInfo.h_AuxClkInitSem ) )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
          "OSAL_s32SemaphorePost():line %lu err %lu",
                                               __LINE__, OSAL_u32ErrorCode() );
      // sem post failed. print osal err code
      }
   }
   else
   {
       GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                             "AUX init fail line %lu", __LINE__ );
   }
}

/******************************************************************************
** FUNCTION:  AUXCLOCK_s32IOOpen
 *
 * @brief   Open auxclock device
 *
 *
 * @param :   none
 *
 * @retval tS32 : OSAL error code, currently always OSAL_E_NOERROR
 *
 *
******************************************************************************/
tS32 AUXCLOCK_s32IOOpen(tVoid)
{
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_AUX_OPEN, " %d", getpid() );

   /* Always return success */
   return(tS32)OSAL_E_NOERROR;
}


/******************************************************************************
** FUNCTION:  AUXCLOCK_s32IOClose

 *
 * @brief   Close auxclock device
 *
 *
 * @param :   none
 *
 * @retval tS32 : OSAL error code, currently always OSAL_E_NOERROR
 *
 *
******************************************************************************/
tS32 AUXCLOCK_s32IOClose(tVoid)
{
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_AUX_CLS, " %d", getpid() );
   return(tS32)OSAL_E_NOERROR;

}
/******************************************************************************
** FUNCTION:  AUXCLOCK_vDeinit

 *
 * @brief   Close auxclock device
 *
 *
 * @param :   none
 *
 * @retval tS32 : OSAL error code, currently always OSAL_E_NOERROR
 *
 *
******************************************************************************/
tVoid AUXCLOCK_vDeinit(tVoid)
{
   if ( TRUE == rAuxClkInfo.bGnssAuxClkInitSuccess )
   {
      rAuxClkInfo.prGnssSharedData->u32NumProcAttached--;
      if ( 0 == rAuxClkInfo.prGnssSharedData->u32NumProcAttached )
      {
         /* Release shared memory */
         if (shm_unlink(AUX_CLOCK_SHM_NAME))
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
           "AUXCLOCK_vDeinit():line %lu err %d", __LINE__, errno );
         }
         /* release semaphore */
         if ( OSAL_OK != OSAL_s32SemaphoreClose( rAuxClkInfo.h_AuxClkInitSem ))
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                             "AUXCLOCK_vDeinit():line %lu err %d",
                                       __LINE__, OSAL_u32ErrorCode() );
         }
         if ( OSAL_OK != OSAL_s32SemaphoreDelete( AUX_CLOCK_INIT_SEM ))
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                                 "AUXCLOCK_vDeinit(): %lu err %d",
                                       __LINE__, OSAL_u32ErrorCode() );
         }
      }
   }
}
/******************************************************************************
** FUNCTION:  AUXCLOCK_s32IOControl
 *
 * @brief   call an auxclock device driver control function
 *
 * Available options:
 *    - OSAL_C_S32_IOCTRL_VERSION
 *      (returns auxclock device driver version number)
 *    - OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETRESOLUTION
 *      (returns auxclock resolution in Hz, derived from @ref AUX_SCAN_RATE_IN_NS)
 *    - OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETCYCLETIME
 *      (returns auxclock cyle time in ns, defined by @ref AUX_SCAN_RATE_IN_NS)
 *
 *
 * @param [s32fun] :   Function identificator
 * @param [s32arg] :   Argument to be passed to function
 *
 * @retval tS32 : OSAL error code, @ref OSAL_E_NOERROR if ok
 *
 *
******************************************************************************/
tS32 AUXCLOCK_s32IOControl(tS32 s32fun, tLong sArg)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   /* s32arg is address of variable where to store requested data */
   tPS32 pArgument =  (tPS32) sArg;

   switch(s32fun)
   {
      case OSAL_C_S32_IOCTRL_VERSION:
      {
         /* check input parameter */
         if( pArgument != OSAL_NULL )
         {
            *pArgument = AUXCLOCK_C_S32_IO_VERSION;
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETRESOLUTION:
      {
         /* check input parameter */
         if( pArgument != OSAL_NULL )
         {
            *pArgument = (tS32)(CONVERT_NS_2_HZ(AUX_SCAN_RATE_IN_NS));
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_GETCYCLETIME:
      {
         /* check input parameter */
         if( pArgument != OSAL_NULL )
         {
            *pArgument = (tS32)(AUX_SCAN_RATE_IN_NS);
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }
      case OSAL_C_S32_IOCTRL_AUXILIARY_CLOCK_CONVERTTICKSTOPOSIX:
      {
         /* No component is using the iocontrol,this is removed*/       
         s32RetVal = OSAL_E_NOTSUPPORTED;
         break;
      }

      default:
      {
         /* in case s32fun is neither of allowed above */
         s32RetVal = OSAL_E_WRONGFUNC;
         break;
      }
   }

   return(s32RetVal);
}


/******************************************************************************
** FUNCTION:  AUXCLOCK_s32IORead
 *
 * @brief   Read auxclock time stamp, used just as wrapper for @ref clock_gettime() system call
 *             the internal struct of this driver is just updated on every read operation ( therefore
 *             no other interface makes sense ). It also adds the offset needed to sync the IMX 
 *             system time to V850 system time.
 *
 * @param [prIOAuxData] :   pointer to struct @ref OSAL_trIOCtrlAuxClockTicks
 *                                        to which the read data must be copied
 * @param [u32maxbytes] :  max number of bytes for read buffer
 *
 * @retval tS32 : number of bytes written to buffer in case of success
 *                       else OSAL error code
 *
 *
******************************************************************************/
tS32 AUXCLOCK_s32IORead(OSAL_trIOCtrlAuxClockTicks* prIOAuxData, tU32 u32maxbytes)
{
   tS32 s32Retvalue =0;
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_AUX_READ, " %d", getpid() );

   /* check if read buffer is large enough */
   if (u32maxbytes < sizeof(OSAL_trIOCtrlAuxClockTicks))
   {
      /* if not return with Error */
      s32Retvalue= OSAL_E_INVALIDVALUE;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
          "AUXCLOCK_s32IORead():Invalid param line %u", __LINE__ );
   }
   else if(prIOAuxData == OSAL_NULL)
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
          "AUXCLOCK_s32IORead():Invalid param line %u", __LINE__ );
   }
   else
   {
      s32Retvalue = AuxClk_s32GetPlainAuxClkVal( prIOAuxData, u32maxbytes );
      if ( (tS32)sizeof(OSAL_trIOCtrlAuxClockTicks) != s32Retvalue )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
                     "Plain Auxclock read Failed line %u", __LINE__ );
      }
      else if ( (TRUE == rAuxClkInfo.bGnssAuxClkInitSuccess) &&
                (rAuxClkInfo.prGnssSharedData != OSAL_NULL) )
      {
         /* Use GNSS AUX clock */
         AuxClock_vEnterCriticalSection( __LINE__ );
         prIOAuxData->u32Low += (tU32)rAuxClkInfo.prGnssSharedData->s32V850TsDiff;
         AuxClock_vLeaveCriticalSection( __LINE__ );

         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_CORTED_AUX, " %lu", prIOAuxData->u32Low );
      }
   }
   /* Return number of bytes copied */
   return(s32Retvalue);
}

/*********************************************************************************************************************
* FUNCTION      : AuxClk_s32GetPlainAuxClkVal
*
* PARAMETER    : None
*
* RETURNVALUE : None
*
* DESCRIPTION : Read auxclock time stamp, used just as wrapper for @ref clock_gettime() system call
*                         the internal struct of this driver is just updated on every read operation ( therefore
*                         no other interface makes sense ). 
*
* HISTORY        :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 07.Feb.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 AuxClk_s32GetPlainAuxClkVal( OSAL_trIOCtrlAuxClockTicks* prIOAuxData, tU32 u32maxbytes )
{
   /* time in Millisecond*/
   tU64 u64Time;
   struct timespec rtime;
   tS32 s32Retvalue =OSAL_ERROR;
   static OSAL_trIOCtrlAuxClockTicks rIOAuxClockTicks = {0,0} /*u32High,u32Low*/;

   /* check if read buffer is large enough */
   if (u32maxbytes < sizeof(OSAL_trIOCtrlAuxClockTicks))
   {
      /* if not return with Error */
      s32Retvalue= OSAL_E_INVALIDVALUE;
   }
   else if(prIOAuxData != OSAL_NULL)
   {
      /* copy data to driver internal structure */
      /* maybe needs critical section if this function is accessed */
      /* from various tasks and timer wrap around happens */
      if((clock_gettime(CLOCK_MONOTONIC ,&rtime ))== 0)
      {  /*Structure rtime holds value in sec and ns this should be converted to ms*/
         u64Time = (tU64)((((tS64)rtime.tv_sec)*(CONVERT_NS_2_HZ(AUX_SCAN_RATE_IN_NS))) +
                         ((tS64)(rtime.tv_nsec) /AUX_SCAN_RATE_IN_NS));
         rIOAuxClockTicks.u32Low = (tU32) (u64Time);
         rIOAuxClockTicks.u32High = (tU32) (u64Time>>32);
         s32Retvalue=sizeof(OSAL_trIOCtrlAuxClockTicks);
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  
              "!! Fail clock_gettime line-%ld ,errno-%ld", __LINE__, errno );
         s32Retvalue = OSAL_E_UNKNOWN;
      }
      /* copy rIOAuxClockTicks to returned read buffer */
      *prIOAuxData = rIOAuxClockTicks;
   }
   //GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_AUX_PL_READ, " %ld , %ld",
   //                    rIOAuxClockTicks.u32High,rIOAuxClockTicks.u32Low );
   return s32Retvalue;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vSyncAuxClock
*
* PARAMETER   : None
*
* RETURNVALUE : None
*
* DESCRIPTION : Calculate sync offset.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 07.Feb.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vSyncAuxClock( tU32 u32V850TimStmp )
{
   tF32 f32TimStmpDiff;
   tF32 f32AvgTsDiff;
   OSAL_trIOCtrlAuxClockTicks rlAuxClockTicks = {0};

   if ( (tS32)sizeof(rlAuxClockTicks) == AuxClk_s32GetPlainAuxClkVal( &rlAuxClockTicks, sizeof(rlAuxClockTicks) ) )
   {
      f32TimStmpDiff = (tF32)((tS32)u32V850TimStmp - (tS32)rlAuxClockTicks.u32Low);
      if ( f32TimStmpDiff < 0 )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,   "Aux:Error s32TimStmpDiff = %f", f32TimStmpDiff );
      }
      else if ( (TRUE == rAuxClkInfo.bGnssAuxClkInitSuccess) &&
                (rAuxClkInfo.prGnssSharedData != OSAL_NULL) )
      {
         rAuxClkInfo.u32AvgCntr++;
         /* FirstValue. No need for AVERAGE calculation */
         if ( 0 == (rAuxClkInfo.u32AvgCntr -1) )
         {
            AuxClock_vEnterCriticalSection( __LINE__ );
            rAuxClkInfo.prGnssSharedData->s32V850TsDiff = (tS32)f32TimStmpDiff;
            AuxClock_vLeaveCriticalSection( __LINE__ );
          
            //GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_AUX_TS_DIFF, "%d %f", __LINE__, f32TimStmpDiff );
         }
         /* As division is involved, this average calculation may take significant CPU time.
            It is sufficient to calculate average for first 1000 samples and keep the offset constant there after. */
         else if ( AUX_CLOCK_MAX_SYNC_COUNT >= rAuxClkInfo.u32AvgCntr )
         {
            /* Always perform division first to avoid Over flow */
            f32AvgTsDiff =(tF32)rAuxClkInfo.prGnssSharedData->s32V850TsDiff *
                              ((tF32)(rAuxClkInfo.u32AvgCntr -1)/(tF32)(rAuxClkInfo.u32AvgCntr));
            f32AvgTsDiff += (tF32)((f32TimStmpDiff)/(tF32)(rAuxClkInfo.u32AvgCntr));
            AuxClock_vEnterCriticalSection( __LINE__ );
            rAuxClkInfo.prGnssSharedData->s32V850TsDiff = (tS32)f32AvgTsDiff;
            AuxClock_vLeaveCriticalSection( __LINE__ );
         }

         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_AUX_CLK_INFO, " %lu , %d , %lu", 
                                               rAuxClkInfo.u32AvgCntr,
                                               rAuxClkInfo.prGnssSharedData->s32V850TsDiff,
                                               u32V850TimStmp );
      }

   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "Aux clock read failed line %u", __LINE__ );
   }
}
/*********************************************************************************************************************
* FUNCTION    : AuxClk_s32InitShm
*
* PARAMETER   : None
*
* RETURNVALUE : None
*
* DESCRIPTION : Initialize shared memory.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 12.JUN.2013 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 AuxClk_s32InitShm(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;

   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  
                            "AuxClk_s32InitShm()%d", __LINE__ );

   rAuxClkInfo.h_AuxClkShm = shm_open( AUX_CLOCK_SHM_NAME,
                                       (O_EXCL|O_RDWR|O_CREAT|O_TRUNC),
                                       S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP );
   if( 0 > rAuxClkInfo.h_AuxClkShm )
   {
       GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
         "AuxClk_s32InitShm():line %d err: %d", __LINE__, errno );
   }
   else if( 0 > ftruncate( rAuxClkInfo.h_AuxClkShm,
                         sizeof(trGnssSharedData) ) )
   {
       GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "%d", __LINE__ );
      // print errno
      close(rAuxClkInfo.h_AuxClkShm);
   }
   else
   {
      s32RetVal = OSAL_OK;
   }

   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : AuxClock_vEnterCriticalSection
*
* PARAMETER   : Line Number
*
* RETURNVALUE : None
*
* DESCRIPTION : Enter into critical section.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 24.JUN.2014 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid AuxClock_vEnterCriticalSection( tU32 u32LineNo )
{
   if ( OSAL_OK != OSAL_s32SemaphoreWait( rAuxClkInfo.h_AuxClkInitSem,
                                      AUX_CLOCK_INIT_TIMEOUT ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
               "Sem Wait Fail %lu Err %lu", u32LineNo, OSAL_u32ErrorCode() );
   }
}
/*********************************************************************************************************************
* FUNCTION    : AuxClock_vLeaveCriticalSection
*
* PARAMETER   : Line Number
*
* RETURNVALUE : None
*
* DESCRIPTION : Leave critical section.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 24.JUN.2014 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/

static tVoid AuxClock_vLeaveCriticalSection( tU32 u32LineNo )
{
   if ( OSAL_OK != OSAL_s32SemaphorePost( rAuxClkInfo.h_AuxClkInitSem ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                "Sem Post Fail %lu Err %lu", u32LineNo, OSAL_u32ErrorCode() );
   }

}

#ifdef LOAD_SENSOR_SO

tS32 aux_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
   (tVoid)s32Id;
   (tVoid)szName;
   (tVoid)enAccess;
   (tVoid)pu32FD;
   (tVoid)app_id;
   return AUXCLOCK_s32IOOpen();
}

tS32 aux_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   return AUXCLOCK_s32IOClose(); 
}

tS32 aux_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tLong sArg)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   return AUXCLOCK_s32IOControl(s32fun, sArg); 
}

tS32 aux_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   (tVoid)ret_size;
   (tVoid)u32FD;
   (tVoid)s32ID;
   return AUXCLOCK_s32IORead( (OSAL_trIOCtrlAuxClockTicks*)(tPVoid)pBuffer, u32Size);
}
#endif


#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file gnss_auxclock.c
|-----------------------------------------------------------------------*/

