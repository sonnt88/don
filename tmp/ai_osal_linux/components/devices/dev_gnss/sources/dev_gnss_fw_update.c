/************************************************************************************************
 * FILE        : dev_gnss_fw_update.c
 *
 * DESCRIPTION : This file adds support for firmware update of the GNSS chip.
 *               Currently Implementation is available for Teseo STA8088XX.
 *------------------------------------------------------------------------------------------------
 * AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 *
 * HISTORY     :
 *------------------------------------------------------------------------------------------------
 * Date        |       Version          | Author & comments
 *-------------|------------------------|---------------------------------------------------------
 * 07.DEC.2013 | Initial version: 0.1   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -----------------------------------------------------------------------------------------------
 * 26.MAY.2015 | Initial version: 3.0   | Madhu Kiran Ramachandra (RBEI/ECF5)
 *             |                        | Support added for autosar ver 3 software of 
 *             |                        | Teseo Firmware update. This has sequence counter 
 *             |                        | for messages exchanged from IMX to V850
 * -----------------------------------------------------------------------------------------------
 * 04.FEB.2016 |      version: 3.1      | Madhu Kiran Ramachandra (RBEI/ECF5)
 *             |                        | Earlier The ACK received from Teseo for last chunk (remaining bytes) was
 *             |                        | wrongly considered as CRC verification ACK. Now an extra ACK wait added after 
 *             |                        | complete firmware transfer: GMMY17-12739
  * -----------------------------------------------------------------------------------------------
 * 18.MAR.2016 |      version: 3.2      | Madhu Kiran Ramachandra (RBEI/ECF5)
 *             |                        | Added support for unlocking the teseo flash. This is needed for teseo downgrade 
 *             |                        | as older flasher.bin in the downgrade stick cannot un-protect the flash.
 * -----------------------------------------------------------------------------------------------*/
 
 
 
 /*************************************************************************
 * Header file declaration
 *-----------------------------------------------------------------------*/

#include "dev_gnss_types.h"
#include "dev_gnss_scc_com.h"
#include "dev_gnss_trace.h"
#include "dev_gnss_fw_update.h"
#include "dev_gnss_trace.h"
#include "dev_gnss_parser.h"
#include <sys/socket.h>

#include "inc_ports.h"

static tS32 GnssProxyFw_s32EvalStsMsg(tVoid);
static tVoid GnssProxyFw_vSocketReadThread(tPVoid pvArg);
static tS32 GnssProxyFw_s32CfgTeseoBootLdr(const trImageOptions *prImageOptions);
static tS32 GnssProxyFw_s32CfgTeseoFwOpts(const trImageOptions *prImageOptions);
static tS32 GnssProxyFw_s32TrigFwUpdate(tVoid);
static tS32 GnssProxyFw_s32ExchFlashBegin(tVoid);
static tS32 GnssProxyFw_s32GetFlashBufSize(tVoid);
static tS32 GnssProxyFw_s32SendHostRdy(tVoid);
static tS32 GnssProxyFw_s32FillBinOpts(tVoid);
static tS32 GnssProxyFw_s32SendBtLdr(const char* pBuffer, tU32 u32maxbytes);
static tS32 GnssProxyFw_SendBinCnk( tU32 u32BinBytesToSend );
static tS32 GnssProxyFw_s32SendFirmware(const char* pBuffer, tU32 u32maxbytes);
static tVoid GnssProxyFw_vHandleConfigMsg( tVoid );
static tVoid GnssProxyFw_vFlashBufRes( tVoid );
static tVoid GnssProxyFw_vChipRes( tVoid );
static tVoid GnssProxyFw_vFlashDataRes( tVoid );
static tVoid GnssProxyFw_vFlashBegRes( tVoid );
static tS32 GnssProxyFw_s32CfgBinOpts(tVoid);
static tS32 GnssProxyFw_s32SendBinOpts(tVoid);
static tVoid GnssProxyFw_vReleaseResource(tU32 u32Resource );
static tS32 GnssProxyFw_s32SendFlashEndCmd( tVoid );
static tS32 GnssProxyFw_s32CreateResources(tVoid);
static tVoid GnssProxyFw_vHandleNmeaMsg(tVoid);
static tVoid GnssProxyFw_vFlashEndRes(tVoid);
static tS32 GnssProxyFw_s32ExchUartSync(tVoid);
static tBool GnssProxyFw_bValidateTesFlasherSync(tU32 u32SizeOfChipRes);
static tS32 GnssProxyFw_s32SendFlasherSync(tVoid);
static tS32 GnssProxyFw_s32SendFlasherReady( tVoid );
static tVoid GnssProxyFw_vHandlePSTMCRCCHECK ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxyFw_vHandlePSTMSETPAROK ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxyFw_vHandlePSTMSAVEPAROK ( tChar const * const pcFieldIndex[] );
static tS32 GnssProxyFw_s32DisableNmeaMsgs( tVoid );
static tS32 GnssProxyFw_s32SaveTesCfgPersist( tVoid );
static tS32 GnssProxyFw_s32SendMsg( tU32 u32EventToSend );
static tS32 GnssProxyFw_s32FlashUnlock( tVoid );
static tS32 GnssProxyFw_s32ReBootTesNormalMode( tVoid );
static tVoid  GnssProxyFw_vHandleGPTXT( const tChar * const pcFieldIndex[] );
static tS32 GnssProxyFw_s32GetTeseoFwVer( void );
static tVoid GnssProxyFw_vHandlePSTMVER( tChar const * const pcFieldIndex[] );

/*************************************************************************
* MACROS declaration
*-----------------------------------------------------------------------*/

#define GNSS_PROXY_FW_RESOURCE_RELEASE_SOCKET             (0x01)
#define GNSS_PROXY_FW_RESOURCE_RELEASE_SCC_RESPONSE_EVENT (0x02)
#define GNSS_PROXY_FW_RESOURCE_RELEASE_INTER_COMM_EVENT   (0x03)

/*************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/
extern trGnssProxyInfo rGnssProxyInfo;
extern trGnssProxyFwUpdateInfo rGnssProxyFwUpdateInfo;
extern trGnssProxyData rGnssProxyData;
extern tEnGnssFwUpdateStatus enGnssFwUpdateStatus;

/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32Init
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK  on success
 *                OSAL_ERROR on Failure
 *
 * DESCRIPTION  : This is initialization function for GNSS FW update.
 *                1: Configures socket and connects to SCC on GNSS_FW_UPDATE Port
 *                2. Handles communication with SCC and exchange status.
 *                3: Spawns a thread which handles further communications with SCC on GNSS_FW_UPDATE Port
 * HISTORY      :
 *-------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|----------------------------------------
 * 07.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * ------------------------------------------------------------------------------
 *********************************************************************************************************************/
tS32 GnssProxyFw_s32Init(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;
   OSAL_trThreadAttribute rThreadAttr;
   tU32 u32ResultMask = 0;
   
   enGnssFwUpdateStatus=GNSS_FW_UPDATE_NOT_ACTIVE;
   rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_DEVICE_OPENED;
   rGnssProxyInfo.hGnssProxyTeseoComEvent = OSAL_C_INVALID_HANDLE;
   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize = 0;
   rGnssProxyFwUpdateInfo.enGnssChipType = GNSS_CHIP_TYPE_TESEO_2;
   rGnssProxyFwUpdateInfo.u32TeseoAckChunkSize = GNSS_PROXY_FW_ACK_WAIT_CHUNK_SIZE_TESEO_2;

   rGnssProxyInfo.bShutdownFlag = FALSE;
   /*Update thread attributes*/
   rThreadAttr.szName = GNSS_PROXY_FW_UPDATE_THREAD_NAME;
   rThreadAttr.u32Priority = GNSS_PROXY_FW_UPDATE_THREAD_PRIORITY;
   rThreadAttr.s32StackSize = GNSS_PROXY_FW_UPDATE_THREAD_STACKSIZE;
   rThreadAttr.pfEntry = GnssProxyFw_vSocketReadThread;
   rThreadAttr.pvArg = OSAL_NULL;

   GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS Firmware Update Started.");
   GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
         "Please Don't restart the device or remove the download media till update is complete" );

   if (OSAL_OK != OSAL_s32EventCreate( GNSS_PROXY_FW_UPDATE_EVENT_NAME,
                                            &rGnssProxyInfo.hGnssProxyTeseoComEvent))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW INIT Event create failed err %lu",
                                            OSAL_u32ErrorCode());
   }
   else if (OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW Update Thread spawn failed err %lu",
                                            OSAL_u32ErrorCode());
      GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SCC_RESPONSE_EVENT);
   }
   /* Wait for Init to complete */
   else if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                              (GNSS_PROXY_FW_INIT_SUCCESS| GNSS_PROXY_FW_INIT_FAILED),
                                              OSAL_EN_EVENTMASK_OR,
                                              GNSS_PROXY_INIT_EVENT_WAIT_TIME,
                                              &u32ResultMask ))
   {
      /* If there is no response from Gnss thread, only reason could be V850 is not responding. */
      if ( OSAL_E_TIMEOUT == OSAL_u32ErrorCode() )
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW NO response from V850" );
         s32RetVal = OSAL_E_TIMEOUT;
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW Event wait fail Err: %lu, line %lu",
                                               OSAL_u32ErrorCode(), __LINE__ );
         s32RetVal = OSAL_E_UNKNOWN;
      }
      GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SCC_RESPONSE_EVENT);
      rGnssProxyInfo.bShutdownFlag =  TRUE;
   }
   else if ( GNSS_PROXY_FW_INIT_FAILED == u32ResultMask )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW Init Fail" );
      rGnssProxyInfo.bShutdownFlag =  TRUE;
      GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SCC_RESPONSE_EVENT);
      s32RetVal = OSAL_E_UNKNOWN;
   }
   else 
   {
      if ( OSAL_OK != GnssProxyFw_s32GetTeseoFwVer())
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                              "GnssFw: unable to get teseo firmware version");
      }

      GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Gnss FW update Init Success");
      s32RetVal = OSAL_OK;
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_s32CreateResources
*
* PARAMETER    : NONE
*
* RETURNVALUE  : OSAL_OK  on success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : This is a part of initialization for GNSS FW update.
*                1: Configures socket and connects to SCC
*                2. Handles communication with SCC and exchange status.
*                3: Handles the initial configuration message received from SCC
*                4: Creates all the necessary OS resources.
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------------
* 22.APR.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxyFw_s32CreateResources(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;

   /* Event for internal two communication between APP thread and Socket read thread*/
   if (OSAL_OK != OSAL_s32EventCreate( GNSS_PROXY_FW_UPDATE_INT_EVENT_NAME,
                                            &rGnssProxyFwUpdateInfo.hGnssFwInEve ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW INTER Event create failed err %lu",
                                            OSAL_u32ErrorCode());
   }
   /* Socket setup. Just connect */
   else if (OSAL_ERROR == GnssProxy_s32SocketSetup(GNSS_FW_UPDATE_PORT))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW Sock Setup Failed" );
      GnssProxyFw_vReleaseResource( GNSS_PROXY_FW_RESOURCE_RELEASE_INTER_COMM_EVENT );
   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Socket Setup Successful");

#ifndef GNSS_PROXY_TEST_STUB_ACTIVE
      rGnssProxyInfo.hldSocDgram = dgram_init( rGnssProxyInfo.s32SocketFD,
                                               GNSS_PROXY_MAX_PACKET_SIZE,
                                               OSAL_NULL);
      if (rGnssProxyInfo.hldSocDgram == OSAL_NULL)
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW dgram_init Failed ");
         GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SOCKET);
         GnssProxyFw_vReleaseResource( GNSS_PROXY_FW_RESOURCE_RELEASE_INTER_COMM_EVENT );
      }
      /* Send status message to V850 */
      else
#endif

      if (OSAL_OK != GnssProxy_s32ExchStatus( GNSS_PROXY_COMPONENT_STATUS_ACTIVE,
                                                   GNSS_PROXY_COMPONENT_VERSION))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW GnssProxy_s32ExchStatus failed err %lu",
                                                                   OSAL_u32ErrorCode());
         GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SOCKET);
         GnssProxyFw_vReleaseResource( GNSS_PROXY_FW_RESOURCE_RELEASE_INTER_COMM_EVENT );
      }
      /* Check if V850 is active and component version's match ? */
      else if (OSAL_OK != GnssProxyFw_s32EvalStsMsg())
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW GnssProxyFw_s32EvalStsMsg failed err %lu",
                                                                   OSAL_u32ErrorCode());
         GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SOCKET);
         GnssProxyFw_vReleaseResource( GNSS_PROXY_FW_RESOURCE_RELEASE_INTER_COMM_EVENT );
      }
      else
      {
         GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "GNSS:FW GnssProxyFW_s32CreateResources success" );
         s32RetVal =  OSAL_OK;
      }
   }

   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32DeInit
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK  on success
 *                OSAL_ERROR on Failure
 *
 * DESCRIPTION  : This is De-initialization function for GNSS FW update.
 *                1: Closes and releases the socket connection to SCC on GNSS_FW_UPDATE Port
 * HISTORY      :
 *-------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|----------------------------------------
 * 09.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * ------------------------------------------------------------------------------
 *********************************************************************************************************************/
tS32 GnssProxyFw_s32DeInit(tVoid)
{
   tS32 s32RetVal = OSAL_E_NOERROR;

   GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Closing FW device" );
   if ( OSAL_OK !=  GnssProxyFw_s32SendFlashEndCmd())
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!ERROR Unable to send FLASH_END to Scc. Restart the device" );
      s32RetVal = OSAL_ERROR;
   }
   rGnssProxyInfo.bShutdownFlag = TRUE;
   GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SOCKET);
   /*cmg3gb-1728: Wait till socket read thread exits */ 
   OSAL_s32ThreadWait( GNSS_PROXY_POLL_TIMEOUT_MS * 2);
   
   return s32RetVal;
}

/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32ConfigBinOpts
 *
 * PARAMETER    : trImageOptions *: Pointer to the structure with binary image
 *                details to be flashed
 *
 * RETURNVALUE  : OSAL_OK  on success
 *                OSAL_ERROR on Failure
 *
 * DESCRIPTION  : In Gnss chip firmware update process, this ioctl always precedes
 *                the Write call. Write call is used to transfer the binary and this
 *                ioctl prepares the GNSS chip for flashing the image in successive
 *                write calls.
 * HISTORY      :
 *-------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|----------------------------------------
 * 09.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * ------------------------------------------------------------------------------
 *********************************************************************************************************************/
tS32 GnssProxyFw_s32ConfigBinOpts(const trImageOptions *prImageOptions)
{
   tS32 s32RetVal = OSAL_E_UNKNOWN;

   if (OSAL_NULL == prImageOptions)
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "NULL pointer @ %d", __LINE__ );
   }
   else
   {
      /* Action to be taken depends on the type of image download
       * application is about to flash */
      switch (prImageOptions->enType)
      {
         /* This configures the device to flash the Teseo Boot loader. */
         case IMAGE_TESEO_FLASHER_BIN:
         {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Ioctl called IMAGE_TESEO_FLASHER_BIN" );
            s32RetVal = GnssProxyFw_s32CfgTeseoBootLdr(prImageOptions);
            break;
         }
         /* This configures the device to flash the actual teseo firmware.
          * Teseo firmware can be flashed only after flashing Teseo Boot loader */
         case IMAGE_TESEO_FIRMWARE:
         {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Ioctl called IMAGE_TESEO_FIRMWARE" );
            /* This sends HOST_READY command to Teseo and then sends Binary Image options */
            s32RetVal = GnssProxyFw_s32CfgTeseoFwOpts(prImageOptions);
            break;
         }
         default:
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "unknown Ioctl line %lu", __LINE__ );
            break;
         }
      }
   }
   return s32RetVal;
}

/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32SendBinToTeseo
 *
 * PARAMETER    : pBuffer: Pointer to data to be sent
 *                u32maxbytes: Size of the data to be sent
 *
 * RETURNVALUE  : OSAL_OK  on success
 *                OSAL_ERROR on Failure
 *
 * DESCRIPTION  : This function checks the state of the download and transfers the
 *                Binary to Teseo.
 * HISTORY      :
 *-------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|----------------------------------------
 * 09.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * ------------------------------------------------------------------------------
 *********************************************************************************************************************/
tS32 GnssProxyFw_s32SendBinToTeseo(const char* pBuffer, tU32 u32maxbytes)
{
   tS32 s32RetVal = OSAL_ERROR;

   if (OSAL_NULL == pBuffer)
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "NULL pointer line %lu", __LINE__ );
   }
   else if (0 == u32maxbytes)
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Invalid BT-LDR size: 0" );
   }
   else
   {
      switch ( rGnssProxyFwUpdateInfo.enGnssFWUpdateState )
      {
         /* Boot loader configuration was received successfully and this is the
          * start of the transfer of boot loader image to GNSS chip */
         case GNSS_FW_UPDATE_BOOTLOADER_CONFIG_RECVD:
         {
            rGnssProxyFwUpdateInfo.u32SentImageSize = 0;
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "GNSSFW:Boot Loader Flashing started" );
            rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_STARTED;
         }
         /* There will be successive write calls to transfer the boot loader image.
          * All the calls to transfer the boot loader will be routed to here */
         //–fallthrough
         //lint -e825
         case GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_STARTED:
         {
            if ( 0 != rGnssProxyFwUpdateInfo.u32BootLdrSize )
            {
               GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Flashing BOOT Loader %lu %% complete",
                        ((rGnssProxyFwUpdateInfo.u32SentImageSize * 100) / rGnssProxyFwUpdateInfo.u32BootLdrSize) );
               /* This will send the Boot Loader software to Teseo */
               s32RetVal = GnssProxyFw_s32SendBtLdr( pBuffer, u32maxbytes );
               if ( OSAL_ERROR == s32RetVal)
               {
                  rGnssProxyFwUpdateInfo.enGnssFWUpdateState =
                                         GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_FAILED;
                  GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!! ERROR Boot Loader Flashing Failed" );
               }
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                      "Internal error rGnssProxyFwUpdateInfo.u32BootLdrSize is 0" );
            }
            break;
         }
         /* Boot loader transfer was successful and binary options were also sent.
          * So the write call is to flash the teseo firmware */
         case GNSS_FW_UPDATE_FIRMWARE_CONFIG_RECVD:
         {
            rGnssProxyFwUpdateInfo.u32SentImageSize = 0;
            GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "GNSSFW:Firmware Flashing started");
            rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_FIRMWARE_TRANSFER_STARTED;
         }
         //lint –fallthrough
         /* There will be successive write calls to transfer the firmware image.
          * All the calls to transfer the firmware will be routed to here */
         //lint -e825
         case GNSS_FW_UPDATE_FIRMWARE_TRANSFER_STARTED:
         {
            if ( 0 != rGnssProxyFwUpdateInfo.u32FwImageSize )
            {
               s32RetVal = GnssProxyFw_s32SendFirmware(pBuffer, u32maxbytes);
               if ( OSAL_ERROR == s32RetVal)
               {
                  rGnssProxyFwUpdateInfo.enGnssFWUpdateState =
                                         GNSS_FW_UPDATE_FIRMWARE_TRANSFER_FAILED;
               }
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Internal error u32FwImageSize is 0" );
            }
            break;
         }
         default:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Write not allowed in state %d",
                                                 rGnssProxyFwUpdateInfo.enGnssFWUpdateState );
         }
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32EvalStsMsg
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK  on success
 *                OSAL_ERROR on Failure
 *
 * DESCRIPTION  : Handle status Message response
 * HISTORY      :
 *-------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|----------------------------------------
 * 09.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * ------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32EvalStsMsg(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;

   /* Is expected message size and received message size same ? */
   if (MSG_SIZE_SCC_GNSS_R_COMPONENT_STATUS == rGnssProxyInfo.u32RecvdMsgSize)
   {
      rGnssProxyInfo.u8SccStatus = rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_STATUS];
      /* We should get only ACTIVE or INACTIVE */
      if (((tU8) GNSS_PROXY_COMPONENT_STATUS_ACTIVE != rGnssProxyInfo.u8SccStatus)
            && ((tU8) GNSS_PROXY_COMPONENT_STATUS_NOT_ACTIVE != rGnssProxyInfo.u8SccStatus))
      {
         GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "!!!! Unknown status: %u of SCC ",
               (tU32) rGnssProxyInfo.u8SccStatus);
         rGnssProxyInfo.u8SccStatus = GNSS_PROXY_COMPONENT_STATUS_UNKNOWN;
      }
      else if ((tU8) GNSS_PROXY_COMPONENT_STATUS_ACTIVE == rGnssProxyInfo.u8SccStatus)
      {
         /* Don't know what to do IF SCC status is INACTIVE */
         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "STATUS OF GNSS on SCC is %u",
               (tU32) rGnssProxyInfo.u8SccStatus );

         /* Check if the software version matches ? */
         if ( (GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_ONE 
                  != rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION]) &&
               (GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_TWO 
                  != rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION]) &&
               (GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_THREE
                  != rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION]))
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                  "!!FW update VER MISMATCH, expec %d / %d / %d, but recvd %d from SCC",
                  (tS32) GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_ONE,
                  (tS32) GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_TWO,
                  (tS32) GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_THREE,
                  rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION]);
         }
         else
         {
            GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "GNSS proxy version %d  GNSS version on V850 : %d",
                  GNSS_PROXY_COMPONENT_VERSION,
                  rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION]);
            rGnssProxyFwUpdateInfo.u8SccSwVer =
                                   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION];

            
            if ( GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_THREE <= rGnssProxyFwUpdateInfo.u8SccSwVer )
            {
               // Enable Seq counter
               rGnssProxyFwUpdateInfo.u8SeqCntrOffset = 1;
               //Reset it on status exchange
               rGnssProxyFwUpdateInfo.u8MsgSeqCntr = 0;
            }
            else
            {
               // No Sequence counter
               rGnssProxyFwUpdateInfo.u8SeqCntrOffset = 0;
            }

            s32RetVal = OSAL_OK;
         }
      }
      else
      {
         // TODO: How to handle Inactive SCC status ?
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!V850 not active");
      }
   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Size of status message should be %d but got %d",
            MSG_SIZE_SCC_GNSS_R_COMPONENT_STATUS, rGnssProxyInfo.u32RecvdMsgSize);
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_PROXY_SocketReadThread
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : NONE
 *
 * DESCRIPTION  : This thread will be waiting for data from SCC. Based on the message ID
 *                of the received data, call respective function to parse the data further.
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 09.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tVoid GnssProxyFw_vSocketReadThread(tPVoid pvArg)
{
   tS32 s32ErrChk = OSAL_ERROR;

   /* Dummy Argument */
   (tVoid) pvArg;
   GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, " In gnss FW Update thread ");

   // Initialize resources
   if ( OSAL_ERROR != GnssProxyFw_s32CreateResources() )
   {
      // Post success event and continue with the firmware update
      if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                           GNSS_PROXY_FW_INIT_SUCCESS,
                                           OSAL_EN_EVENTMASK_OR ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS_FW:Event Post failed err %lu line %u",
                                              OSAL_u32ErrorCode(), __LINE__ );

         GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SOCKET);         
      }
      else
      {
         s32ErrChk = OSAL_OK;      
      }
   }
   // Post success event and continue with the firmware update
   else
   {
      if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                      GNSS_PROXY_FW_INIT_FAILED,
                                                      OSAL_EN_EVENTMASK_OR ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS_FW:Event Post failed err %lu line %u",
                                              OSAL_u32ErrorCode(), __LINE__ );
         GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SOCKET);   
      }
   }
   
   if ( OSAL_OK == s32ErrChk )
   {
      while ( TRUE != rGnssProxyInfo.bShutdownFlag )
      {
         GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Waiting on Data from SCC");
         /* Wait for data from SCC */
         s32ErrChk = GnssProxy_s32GetDataFromScc(GNSS_PROXY_MAX_PACKET_SIZE);
         /* CHeck if read on socket passed */
         if (s32ErrChk > 0)
         {
            rGnssProxyInfo.u32RecvdMsgSize = (tU32) s32ErrChk;
            GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "INC Msg recvd on FW Update port-Msg ID %x , Size %d",
                                                  (tS32) rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID],
                                                  rGnssProxyInfo.u32RecvdMsgSize);

            /* first byte says the type of message */
            switch ( rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] )
            {
               /* Status update message from SCC */
               case GNSS_PROXY_FW_SCC_R_STATUS_MSGID:
               {
                  GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Status Msg Not Expected in Middle of update" );
                  break;
               }
               /* Control message */
               case GNSS_PROXY_FW_SCC_R_CONTROL_MSGID:
               {
                  GnssProxyFw_vHandleConfigMsg();
                  break;
               }
               /* Command reject message from SCC */
               case GNSS_PROXY_FW_SCC_R_REJECT_MSGID:
               {
                  GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Command reject sent from V850 Msg: %d reason %d",
                                                      rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_REJECTED_MSG_ID],
                                                      rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_REJECT_REASON]);
                  /* If any message is rejected by V850, Download cannot continue */
                  rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_FIRMWARE_TRANSFER_FAILED;
                  break;
               }
               default:
               {
                  GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GnssProxyFW_vSocketReadThread: Unknown Msg ID %x",
                                                      (tS32)rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID]);
                  break;
               }
            }
         }
         /* Reason for dgram_recv to fail is not shut down */
         else if (TRUE != rGnssProxyInfo.bShutdownFlag)
         {
            (tVoid) OSAL_s32ThreadWait(GNSS_PROXY_RETRY_INTERVALL_MS);
         }
         else
         {
            GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Device Shutdown signaled");
         }
         OSAL_pvMemorySet((void*) rGnssProxyInfo.u8RecvBuffer, 0, GNSS_PROXY_MAX_PACKET_SIZE);
      }
      GnssProxyFw_vReleaseResource(GNSS_PROXY_FW_RESOURCE_RELEASE_SCC_RESPONSE_EVENT);
      GnssProxyFw_vReleaseResource( GNSS_PROXY_FW_RESOURCE_RELEASE_INTER_COMM_EVENT );
   }
   GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "GNSS FW Update Thread Closed");
   OSAL_vThreadExit();
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32CfgTeseoBootLdr
 *
 * PARAMETER    : trImageOptions *prImageOptions
 *
 * RETURNVALUE  : OSAL_E_NOERROR on success
 *                Appropriate OSAL error codes on failure.
 *
 * DESCRIPTION  : This configures GNSS chip to flash Boot loader
 *                This implements a state machine. Entry step of this state machine
 *                is flashing the Boot Loader. If firmware update process fails,
 *                again it has to stared from flashing Boot-Loader.
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 09.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32CfgTeseoBootLdr( const trImageOptions *prImageOptions )
{
   tS32 s32RetVal = OSAL_E_NOERROR;

   if ( OSAL_NULL == prImageOptions )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "!!!prImageOptions = NULL to GnssProxyFw_s32CfgTeseoBootLdr" );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else if ( 0 == prImageOptions->u32Size )
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Error:Invalid Boot loader size 0");
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else
   {
      /* All the cases below result in same state of the download sequences.
       * But cases are handled just for tracing proper error codes and make debugging easier. */
      switch (rGnssProxyFwUpdateInfo.enGnssFWUpdateState)
      {
         /* Device is just opened. No update is started. */
         case GNSS_FW_UPDATE_DEVICE_OPENED:
         {
            GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Fresh Download started");
            break;
         }
         /* Boot loader configuration is received */
         case GNSS_FW_UPDATE_BOOTLOADER_CONFIG_RECVD:
         {
            GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Overwriting Previous Boot loader Config");
            break;
         }
         /* Update process is started again */
         case GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_FAILED:
         //–fallthrough
         //lint -e825
         case GNSS_FW_UPDATE_FIRMWARE_TRANSFER_FAILED:
         //–fallthrough
         //lint -e825
         case GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_STARTED:
         //–fallthrough
         //lint -e825
         case GNSS_FW_UPDATE_FIRMWARE_CONFIG_RECVD:
         //–fallthrough
         //lint -e825
         case GNSS_FW_UPDATE_FIRMWARE_TRANSFER_STARTED:
         {
            GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Restarting Update from %d",
                  rGnssProxyFwUpdateInfo.enGnssFWUpdateState);
            break;
         }
         /* Update was successful */
         case GNSS_FW_UPDATE_FIRMWARE_TRANSFER_COMPLETE:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                  "!!!Warning: Previous update was successful. This is another update in a row");
            break;
         }
         default:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Warning:Unknown state of FW update software %d",
                                                 rGnssProxyFwUpdateInfo.enGnssFWUpdateState);
         }
      }
      /* Store the size and checksum of Boot loader. This shall be used for flashing the boot loader */
      rGnssProxyFwUpdateInfo.u32BootLdrSize = prImageOptions->u32Size;
      rGnssProxyFwUpdateInfo.u32BootLdrChksum = prImageOptions->u32ChkSum;
      GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "BOOT-Loader Size %lu CRC %x",
                                                 rGnssProxyFwUpdateInfo.u32BootLdrSize,
                                                 rGnssProxyFwUpdateInfo.u32BootLdrChksum );
      /* This will send FLASH_BEGIN command to V850.
       * V850 will then prepare Teseo for firmware update */
      if (OSAL_OK == GnssProxyFw_s32TrigFwUpdate())
      {
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Ready to transfer Boot strap code" );
         rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_BOOTLOADER_CONFIG_RECVD;
      }
      else
      {
         rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_FAILED;
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!! FAILED: GnssProxyFw_s32TrigFwUpdate");
         s32RetVal = OSAL_E_UNKNOWN;
      }
   }
   return s32RetVal;
}

/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32CfgTeseoFwOpts
 *
 * PARAMETER    : trImageOptions *prImageOptions
 *
 * RETURNVALUE  : OSAL_E_NOERROR on success
 *                Appropriate OSAL error codes on failure.
 *
 * DESCRIPTION  : Checks if download sequence is in proper state to process the call
 *                Then prepares Teseo for receiving firmware.
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 09.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32CfgTeseoFwOpts( const trImageOptions *prImageOptions)
{
   tS32 s32RetVal = OSAL_E_UNKNOWN;

   if (OSAL_NULL == prImageOptions)
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!NULL ptr line %lu", __LINE__ );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else if (0 == prImageOptions->u32Size)
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Invalid Firmware Image size 0" );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else
   {
      /* We have only one success case below. All cases are handled separately just to make
       * debugging easier */
      switch (rGnssProxyFwUpdateInfo.enGnssFWUpdateState)
      {
         /* Only after successfully flashing the boot loader,
          * It is possible to configure Teseo for flashing the firmware */
         /*Only success case*/
         case GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_COMPLETE:
         {
            /* Store the firmware options */
            rGnssProxyFwUpdateInfo.u32FwImageSize = prImageOptions->u32Size;
            rGnssProxyFwUpdateInfo.u32FwImgChksum = prImageOptions->u32ChkSum;
            GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, 
                                       "Teseo Binary  Options recvd: Image size %lu Checksum %lu",
                                       rGnssProxyFwUpdateInfo.u32FwImageSize,
                                       rGnssProxyFwUpdateInfo.u32FwImgChksum );
            /*Send HOST_READY and binary image options to Teseo and evaluate the response*/
            if (OSAL_OK == GnssProxyFw_s32CfgBinOpts())
            {
               rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_FIRMWARE_CONFIG_RECVD;
               s32RetVal = OSAL_E_NOERROR;
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Failed to configure Binary Options" );
               rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_FIRMWARE_TRANSFER_FAILED;
            }
            break;
         }
            /* All cases below would result in failure of current update.
             * These are Invalid states for configuring Teseo Firmware options */
         case GNSS_FW_UPDATE_DEVICE_OPENED:
         //–fallthrough
         //lint -e825
         case GNSS_FW_UPDATE_BOOTLOADER_CONFIG_RECVD:
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "ERROR !!! Aborting Download:Invalid Update sequence %d",
                                                 rGnssProxyFwUpdateInfo.enGnssFWUpdateState);
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Teseo Boot loader has to be flashed before firmware" );

            rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_FAILED;
            break;
         }
         case GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_FAILED:
         //–fallthrough
         //lint -e825
         case GNSS_FW_UPDATE_FIRMWARE_TRANSFER_FAILED:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "ERROR !!! Aborting Download:Previous Update failed %d",
                  rGnssProxyFwUpdateInfo.enGnssFWUpdateState);
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Please restart from beginning");
            rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_FIRMWARE_TRANSFER_FAILED;
            break;
         }
         case GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_STARTED:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                  "ERROR !!! Aborting Download:Boot loader transfer is not complete %d",
                  rGnssProxyFwUpdateInfo.enGnssFWUpdateState);
            rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_FAILED;
            break;
         }
         case GNSS_FW_UPDATE_FIRMWARE_CONFIG_RECVD:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                  "ERROR !!! Aborting Download:Over writing previous FW config %d",
                  rGnssProxyFwUpdateInfo.enGnssFWUpdateState);
            rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_FIRMWARE_TRANSFER_FAILED;
            break;
         }
         case GNSS_FW_UPDATE_FIRMWARE_TRANSFER_STARTED:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "ERROR !!! Aborting Download:Previous update aborted %d",
                  rGnssProxyFwUpdateInfo.enGnssFWUpdateState);
            rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_FIRMWARE_TRANSFER_FAILED;
            break;
         }
         case GNSS_FW_UPDATE_FIRMWARE_TRANSFER_COMPLETE:
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                  "ERROR !! prev update was successful. If needed please start from beginning" );
            break;
         }
         default:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                  "!!ERROR:Unknown state of FW update software %d. Aborting update",
                  rGnssProxyFwUpdateInfo.enGnssFWUpdateState);
            break;
         }
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32TrigFwUpdate
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : 1: Send Flash Begin command to V850
 *                2: Get Buffer size to be used for exchange.
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 09.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32TrigFwUpdate( tVoid )
{
   tS32 s32RetVal = OSAL_ERROR;

   /* This will get the Maximum INC chunk size to be used for communication
    * between IMX and V850 over INC. */
   if (OSAL_OK != GnssProxyFw_s32GetFlashBufSize())
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!ERROR Failed to Get Flash Buff Size");
   }
   else
   {
   
#ifndef VARIANT_S_FTR_ENABLE_SUZUKI
      /* Un-lock the flash of Teseo. Even if this fails, we proceed and try the update. */
      if ( OSAL_OK != GnssProxyFw_s32FlashUnlock() )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GnssFw: Flash Unlock Failed. Lets try if we can still do the update");
      }
#endif
      /* This Will send FLASH_BEGIN command to V850 and evaluates the response */
      if (OSAL_OK != GnssProxyFw_s32ExchFlashBegin())
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!ERROR Failed to start Flashing");
      }
      else
      {
         /* Teseo Firmware update is triggered */
         GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "FLASH_BEGIN and BUFF_SIZE %lu success",
                                                      rGnssProxyFwUpdateInfo.u32IncBlockSize );
         s32RetVal = OSAL_OK;
      }

      /* UART Sync phase specific to Teseo-3 */
      if ( GNSS_CHIP_TYPE_TESEO_3 == rGnssProxyFwUpdateInfo.enGnssChipType )
      {
         /* Default value of return variable of this function is reset to OSAL_ERROR
            for Teseo-3 as we still have some work to do. */
         s32RetVal = OSAL_ERROR;
         /* STAGE-1: Uart Sync */
         if ( OSAL_OK != GnssProxyFw_s32ExchUartSync() )
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW: STAGE-1: Uart Sync Failed");
         }
         /* Stage-2: BaudRate Sync (Only Host_ready Exchange) */
         else if ( OSAL_OK != GnssProxyFw_s32SendHostRdy() )
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW: Stage-2: BaudRate Sync Failed");
         }
         /* Stage-3: Flasher Sync */
         else if ( OSAL_OK != GnssProxyFw_s32SendFlasherSync() )
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW: Stage-3: Flasher Sync Failed");
         }
         else
         {
            GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Teseo-3 is now Ready to receive Flasher.bin");
            s32RetVal = OSAL_OK;
         }
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32CfgBinOpts
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : 1: Send HOST_READY command to V850 till ACK is received
 *                2: Then Send the binary options and evaluate them
 *                3: Waits for all the acknowledgments which confirm device is ready for flashing.
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32CfgBinOpts(tVoid)
{
   tS32 s32RetVal = OSAL_OK;

   /* Send Flasher ready to Teseo-3 and evaluates the response */
   if ( GNSS_CHIP_TYPE_TESEO_3 == rGnssProxyFwUpdateInfo.enGnssChipType )
   {
      if (OSAL_ERROR == GnssProxyFw_s32SendFlasherReady())
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR Sending FLASHER_READY Cmd");
         s32RetVal = OSAL_ERROR;
      }
   }
   /* Send Host ready to Teseo-2 and evaluates the response */
   else if (OSAL_ERROR == GnssProxyFw_s32SendHostRdy())
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR Sending T2-HST_RDY Cmd");      
      s32RetVal = OSAL_ERROR;
   }

   /* If sending HOST_READY(Teseo-2)/FLASHER_READY(Teseo-3) is successfull */
   if ( OSAL_OK == s32RetVal )
   {
      /* Reset the default return value of the function to OSAL_ERROR */
      s32RetVal = OSAL_ERROR;

      /* Configures the binary options, sends it to teseo and evaluate the response */
      if ( OSAL_ERROR == GnssProxyFw_s32SendBinOpts() )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!ERROR Fw_s32SendBinOpts");
      }
      /* Wait for Flash Memory Check ACK */
      else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_ACK,
                                                      GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Flash Memory Check ACK Failed");
      }
      /* Wait for Flash erase ACK */
      else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_ACK,
                                                      GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Flash erase ACK Failed");
      }
      /* Wait for NVM erase ACK */
      else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_ACK,
                                                      GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!NVM erase ACK Failed");
      }
      else
      {
         s32RetVal = OSAL_OK;
      }
   }
   return s32RetVal;
}

/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32ExchFlashBegin
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : 1: Send FLASH_BEGIN command to V850 and evaluate the response
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32ExchFlashBegin(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_BEGIN);

   /* Clear all pending events */
   if (OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
         ~(GNSS_PROXY_FW_EVENT_MASK_ALL), OSAL_EN_EVENTMASK_AND))
   {
      GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "cleanup:Event Post failed err %lu line %lu", OSAL_u32ErrorCode(),
            __LINE__);
   }
   else
   {
      if ( GnssProxy_s32SendDataToScc( rGnssProxyFwUpdateInfo.u16FilledTxMsgSize) != 
                                       (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "sending FLASH_BEGIN Failed" );
      }
      else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_BEGIN_OK,
                                                      GNSS_PROXY_FW_FLASH_BEGIN_WAIT_TIME ) )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!ERROR FLASH_BEGIN Failed");         
      }
      else
      {
         GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "FLASH_BEGIN Success: Teseo put in Boot Serial mode");
         s32RetVal = OSAL_OK;
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32GetFlashBufSize
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : 1: Gets Buffer size to be used for data exchange for flashing
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32GetFlashBufSize(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_BUFFER_SIZE);

   /* Send FLASH_BUFF size command */
   if (GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize) != 
                                    (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "sending FLASH_BUFF Failed" );
   }
   /* Wait for Response */
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_BUF_SIZE_OK,
                                                   GNSS_PROXY_FW_FLASH_BEGIN_WAIT_TIME ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!! ERROR-FLASH_BUFF_SIZE Failed" );
   }
   else
   {
      /* Buffer size to be used for flashing is received and stored */
      GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "FLASH_BUFF_SIZE Success");
      s32RetVal = OSAL_OK;
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32SendHostRdy
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : 1: Sends HOST_READY and wait for ACK
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32SendHostRdy(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);
   
   /* Value and meaning of HOST_READY is different for Teseo-2 and Teseo-3 */
   if ( GNSS_CHIP_TYPE_TESEO_3 == rGnssProxyFwUpdateInfo.enGnssChipType )
   {
      rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize] = GNSS_PROXY_FW_CMD_HOST_READY_TESEO_3;
   }
   else
   {
      rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize] = GNSS_PROXY_FW_CMD_HOST_READY_TESEO_2;
   }

   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += GNSS_PROXY_FW_SIZE_OF_HOST_READY_FIELD;

   /* Send HOST_READY Message */
   if ((tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize != 
                                    GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "HOST_READY cmd to SCC fail");
   }
   else if ( GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_TWO <= rGnssProxyFwUpdateInfo.u8SccSwVer )
   {
      /* Wait for FLASH_DATA OK response from V850*/
      if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                 GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Res:ERROR-HOST_READY FLASH_DATA_OK Failed" );
      }
      /* Wait for HOST_READY Response which is ACK from Teseo */
      else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_ACK,
                                                      GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Res:ERROR-HOST_READY Failed" );
      }
      else
      {
         GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "HOST_READY Success");
         s32RetVal = OSAL_OK;
      }
   }
   /* Wait for HOST_READY Response which is ACK from Teseo */
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_ACK,
                                                   GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Res:ERROR-HOST_READY Failed" );
   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "HOST_READY Success");
      s32RetVal = OSAL_OK;
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32SendBinOpts
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : Send Binary options and wait for ACK
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32SendBinOpts(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;

   /* Fill in the binary image options */
   if (OSAL_ERROR == GnssProxyFw_s32FillBinOpts( ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "ERROR !!!Unable to set Binary options");
   }
   else
   {
      /* Send binary options to Teseo */
      GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Sending Binary image options and waiting for ACK");

      if ( GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize) != 
                                                (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                                "!!!ERROR unable to send bin Image Opts to SCC" );
      }
      if ( GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_ONE < rGnssProxyFwUpdateInfo.u8SccSwVer )
      {
         /* Wait for FLASH_DATA OK response from V850*/
         if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                    GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Res:ERROR-BIN_OPTS FLASH_DATA_OK Failed" );
         }
         else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_ACK,
                                                        GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
        {
           GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!ERROR-BIN OPTS ACK Failed");
   
        }
        else
        {
           GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Bin Image Opts Success");
           s32RetVal = OSAL_OK;
        }
      }
      /* Wait for ACK for Binary Image Opts */
      else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_ACK,
                                                      GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!ERROR-BIN OPTS ACK Failed");

      }
      else
      {
         GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Bin Image Opts Success");
         s32RetVal = OSAL_OK;
      }
   }
   return s32RetVal;
}

/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32FillBinOpts
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : Fill the binary options to be sent to Teseo in the TX buffer
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32FillBinOpts()
{
   tS32 s32RetVal = OSAL_ERROR;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);

   if ( GNSS_CHIP_TYPE_TESEO_3 == rGnssProxyFwUpdateInfo.enGnssChipType )
   {
      trTeseo_3_BinaryImageOptions rTeseo_3_BinaryImageOptions;
      rTeseo_3_BinaryImageOptions.code_size      = (tS32)rGnssProxyFwUpdateInfo.u32FwImageSize;
      rTeseo_3_BinaryImageOptions.target_device  = GNSS_PROXY_FW_TARGET_FLASH_DEVICE_SQI;
      rTeseo_3_BinaryImageOptions.crc32          = rGnssProxyFwUpdateInfo.u32FwImgChksum;
      rTeseo_3_BinaryImageOptions.dest_addr      = GNSS_PROXY_FW_TESEO_FLASH_DST_ADDRESS_TESEO_3; // SQI flash memory base address
      rTeseo_3_BinaryImageOptions.entry_offset   = 0x00000000;
      rTeseo_3_BinaryImageOptions.nvm_erase      = 0x1;
      rTeseo_3_BinaryImageOptions.erase_only     = 0x0;
      rTeseo_3_BinaryImageOptions.program_only   = 0x0;
      rTeseo_3_BinaryImageOptions.sub_sector     = 0x0;
      rTeseo_3_BinaryImageOptions.sta8090f       = 0x0;
      rTeseo_3_BinaryImageOptions.res1           = 0x0;
      rTeseo_3_BinaryImageOptions.res2           = 0x0;
      rTeseo_3_BinaryImageOptions.res3           = 0x0;
      rTeseo_3_BinaryImageOptions.nvm_offset     = GNSS_PROXY_FW_TESEO_NVM_FLASH_OFFSET;
      rTeseo_3_BinaryImageOptions.nvm_erase_size = GNSS_PROXY_FW_TESEO_NVM_FLASH_ERASE_SIZE;
      rTeseo_3_BinaryImageOptions.debug_enable   = 0x0;
      rTeseo_3_BinaryImageOptions.debug_mode     = 0x0;
      rTeseo_3_BinaryImageOptions.debug_address  = 0x0;
      rTeseo_3_BinaryImageOptions.debug_size     = 0x0;
      rTeseo_3_BinaryImageOptions.debug_data     = 0x0;

      OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                         &rTeseo_3_BinaryImageOptions,
                         sizeof(rTeseo_3_BinaryImageOptions) );
      rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += sizeof(rTeseo_3_BinaryImageOptions);
      s32RetVal = OSAL_OK;
   }
   else if ( GNSS_CHIP_TYPE_TESEO_2 == rGnssProxyFwUpdateInfo.enGnssChipType )
   {
      trTeseo_2_BinaryImageOptions rTeseo_2_BinaryImageOptions;
      /* This is the information received from Download component */
      rTeseo_2_BinaryImageOptions.code_size = (tS32)rGnssProxyFwUpdateInfo.u32FwImageSize;
      rTeseo_2_BinaryImageOptions.crc32 = rGnssProxyFwUpdateInfo.u32FwImgChksum;
      /*Default Options*/
      rTeseo_2_BinaryImageOptions.target_device = GNSS_PROXY_FW_TARGET_FLASH_DEVICE_SQI;
      rTeseo_2_BinaryImageOptions.dest_addr = GNSS_PROXY_FW_TESEO_FLASH_DST_ADDRESS_TESEO_2;
      rTeseo_2_BinaryImageOptions.entry_offset = 0;
      rTeseo_2_BinaryImageOptions.baud_rate = 0;
      /* Better to clear old NVM config before flashing */
      rTeseo_2_BinaryImageOptions.nvm_erase = 1;
      rTeseo_2_BinaryImageOptions.erase_only = 0;
      rTeseo_2_BinaryImageOptions.program_only = 0;
      rTeseo_2_BinaryImageOptions.debug_enable = 0;
      rTeseo_2_BinaryImageOptions.nvm_offset = GNSS_PROXY_FW_TESEO_NVM_FLASH_OFFSET;
      rTeseo_2_BinaryImageOptions.nvm_erase_size = GNSS_PROXY_FW_TESEO_NVM_FLASH_ERASE_SIZE;
      rTeseo_2_BinaryImageOptions.debug_mode = 0;
      rTeseo_2_BinaryImageOptions.debug_addr = 0;
      rTeseo_2_BinaryImageOptions.debug_size = 0;
      rTeseo_2_BinaryImageOptions.debug_data = 0;

      OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                         &rTeseo_2_BinaryImageOptions,
                         sizeof(rTeseo_2_BinaryImageOptions) );
      rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += sizeof(rTeseo_2_BinaryImageOptions);
      s32RetVal = OSAL_OK;
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "UNknown GNSS Chip Type line %lu", __LINE__ );
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32SendBtLdr
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : This will transfer the boot loader software to teseo in sizes
 *                suitable on INC. Also Implements Flow control between Gnss
 *                download service and dev_gnss for data exchange.
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32SendBtLdr(const char* pBuffer, tU32 u32maxbytes)
{
   tS32 s32RetVal = OSAL_ERROR;
   /* Bytes to be sent for this write call */
   tU32 u32RemainBytes = u32maxbytes;
   tBool bWriteFailed = FALSE;
   /* This holds the number of bytes of Binary to be sent in current INC message */
   tU32 u32BinBytesToSend = 0;
   tU32 u32Offset =0;
   /* This holds the maximum bytes of binary image that can be
    * sent over INC in one message */
   tU32 u32MaxBytesPerMsg = rGnssProxyFwUpdateInfo.u32IncBlockSize -
                               (GNSS_PROXY_FW_CONTROL_MSG_HDR_SIZE + rGnssProxyFwUpdateInfo.u8SeqCntrOffset);

   /* Create the header of the control message. Sequence counter will be updated while sending data below. */
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_MSG_ID  + rGnssProxyFwUpdateInfo.u8SeqCntrOffset]
                                                                              = GNSS_PROXY_FW_SCC_C_CONTROL_MSGID;

   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_SRV_TYPE + rGnssProxyFwUpdateInfo.u8SeqCntrOffset] =
                                                                                 (tU8) SRV_TYPE_FLASH_DATA;
   /*Update the control Messasge size */
   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize = 
                         GNSS_PROXY_FW_CONTROL_MSG_HDR_SIZE  + rGnssProxyFwUpdateInfo.u8SeqCntrOffset;   

   if ( OSAL_NULL ==  pBuffer )
   {
      /* Null Pointer */
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "NULL pointer line %lu", __LINE__ );
   }
   else if ( 0 == u32maxbytes )
   {
      /* In correct binary size */
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "invalid param @ line %lu", __LINE__ );
   }
   else if ( 0 == rGnssProxyFwUpdateInfo.u32IncBlockSize )
   {
      /* No Flash buff size received */
        GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Internal error %s %lu", __FILE__,__LINE__ );
   }
   else
   {
      /*------------------Send data in buffer in Small chunks----------------------------*/
      while ( ( u32RemainBytes > 0 ) && ( TRUE != bWriteFailed ) )
      {
         /* Do we have large data than INC exchange chunk size? */
         if ( u32MaxBytesPerMsg <=  u32RemainBytes )
         {
            /* Send u32MaxBytesPerMsg */
            u32BinBytesToSend = u32MaxBytesPerMsg;
         }
         else
         {
            /* Send u32RemainBytes */
            u32BinBytesToSend = u32RemainBytes;
         }
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Boot Loader: Bytes to send %lu", u32BinBytesToSend );

         /* Do this only if autosar version supports Sequence counter */
         if ( 0 != rGnssProxyFwUpdateInfo.u8SeqCntrOffset)
         {
            /* Sequence counter */
            rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_SEQ_CNTR] = 
                                                        rGnssProxyFwUpdateInfo.u8MsgSeqCntr++;
         }

         OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                            (pBuffer + u32Offset),
                            u32BinBytesToSend );


         /*rGnssProxyFwUpdateInfo.u16FilledTxMsgSize holds the size of control message header 
                              + 
                     u32BinBytesToSend   holds size of FLASH_DATA in TX Buffer*/
         if ( OSAL_ERROR ==  GnssProxyFw_SendBinCnk(
                             ( u32BinBytesToSend + rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )))
         {
            /* Write Failed */
            bWriteFailed = TRUE;
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!GnssFw:ERROR  write fail line %lu", __LINE__ );
         }
         /* All the data for this write call is sent */
         else
         {
            u32RemainBytes -= u32BinBytesToSend;
            u32Offset += u32BinBytesToSend;
         }

      }
      /*--------------------- Update what amount of Boot loader is sent--------------------------------*/
      /* If complete data in this write call is sent successfully */
      if( TRUE != bWriteFailed )
      {
         rGnssProxyFwUpdateInfo.u32SentImageSize += u32Offset;
         s32RetVal = OSAL_OK;
         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Boot loader %lu Bytes complete",
                                                      rGnssProxyFwUpdateInfo.u32SentImageSize );
      }
      /*---------------------Check if Boot loader transfer is complete--------------------------------*/
      /* Is Boot-Loader transfer complete ? */
      if( rGnssProxyFwUpdateInfo.u32SentImageSize == rGnssProxyFwUpdateInfo.u32BootLdrSize )
      {
         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Boot-Loader transfer complete. Waiting for ACk" );

         /* Boot Loader transfer is complete. Wait for ACK before returning Success */
         if ( OSAL_ERROR == GnssProxy_s32WaitForMsg ( GNSS_PROXY_FW_TESEO_ACK,
                                                      GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME))
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!GNSS FW: BOOT-LOADER Flashing Failed" );
            s32RetVal = OSAL_ERROR;
         }
         else
         {
            /* ACK received for Boot-Loader */
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "BOOT-LOADER Flashing COMPLETE" );
            rGnssProxyFwUpdateInfo.enGnssFWUpdateState = GNSS_FW_UPDATE_BOOTLOADER_TRANSFER_COMPLETE;
            s32RetVal = OSAL_OK;
         }
      }
      /* This should never occur */
      else if (rGnssProxyFwUpdateInfo.u32SentImageSize > rGnssProxyFwUpdateInfo.u32BootLdrSize)
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Internal ERROR Sending BOOT-LOADER line %lu", __LINE__ );
         s32RetVal = OSAL_ERROR;
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxy_s32WaitForMsg
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : This is wait for the requested message from V850.
 *                Actual message will be received by GnssProxyFw_vSocketReadThread
 *                and an this function will be intimated
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 *--------------------------------------------------------------------------------------
 *********************************************************************************************************************/
tS32 GnssProxy_s32WaitForMsg( tU32 u32EventMask, tU32 u32WaitTime )
{
   tS32 s32RetVal = OSAL_ERROR;
   tU32 u32ResultMask = 0;

   if (OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                        GNSS_PROXY_FW_EVENT_MASK_ALL,
                                        OSAL_EN_EVENTMASK_OR,
                                        u32WaitTime,
                                        &u32ResultMask))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "EventWait Failed for Event %x err %lu",
                                           u32EventMask, OSAL_u32ErrorCode());

   }
   /* Clear received event */
   else if (OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                             ~(u32ResultMask),
                                             OSAL_EN_EVENTMASK_AND) )
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
              "GnssProxy_s32WaitForMsg():Event Post failed err %lu Event %lu at Line %lu",
                                            OSAL_u32ErrorCode(), u32ResultMask, __LINE__ );
   }
   /* Post the event consumed signal */
   else if (OSAL_ERROR == OSAL_s32EventPost( rGnssProxyFwUpdateInfo.hGnssFwInEve,
                                             u32ResultMask,
                                             OSAL_EN_EVENTMASK_OR) )
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
          "GnssProxy_s32WaitForMsg():Event Post failed err %lu Event %lu at Line %lu",
                                                 OSAL_u32ErrorCode(), u32ResultMask );
   }
   else if (!(u32ResultMask & u32EventMask))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!Wrong Event recvd %x line %d",
                                                      u32ResultMask, __LINE__);
   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Event %x Success", u32ResultMask );
      s32RetVal = OSAL_OK;
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_SendBinCnk
 *
 * PARAMETER    : u32BytesToSend: Amount of data to be sent to Teseo
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : 
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_SendBinCnk( tU32 u32BytesToSend )
{

   tS32 s32RetVal = OSAL_ERROR;

   if ( 0 == u32BytesToSend )
   {
      /* Invalid Parameters */
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Invalid Param %s", __FUNCTION__ );
   }
   else if ( (tS32)u32BytesToSend != GnssProxy_s32SendDataToScc( u32BytesToSend ) )
   {
      /* Sending data Failed */
       GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "%s returned Error %lu", __FUNCTION__ ,u32BytesToSend );
   }
   else if ( OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                    GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME) )
   {
      /* No Response from V850 for Flash data sent */
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!No Response from V850 for Flash data sent");
   }
   else
   {
      s32RetVal = OSAL_OK;
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_s32SendFirmware
 *
 * PARAMETER    : pBuffer: Pointer to the firmware
 *                u32maxbytes: Amount of data to be sent.
 *
 * RETURNVALUE  : OSAL_OK on success
 *                OSAL_ERROR on failure
 *
 * DESCRIPTION  : Send firmware to Teseo and handle its responses
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tS32 GnssProxyFw_s32SendFirmware(const char* pBuffer, tU32 u32maxbytes)
{
   tS32 s32RetVal = OSAL_ERROR;
   tU32 u32RemainBytes = u32maxbytes;
   tBool bWriteFailed = FALSE;
   static tU32 u32AckIntCnt = 0;
   tU32 u32BinBytesToSend =0;
   tU32 u32Offset =0;

   /* This is the maximum amount of binary image that
    * can be transmitted per INC message */
   tU32 u32MaxBinPerMsg = rGnssProxyFwUpdateInfo.u32IncBlockSize -
                             (GNSS_PROXY_FW_CONTROL_MSG_HDR_SIZE + rGnssProxyFwUpdateInfo.u8SeqCntrOffset);

   /* Create the header of the control message. Sequence counter will be updated while sending data below. */
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_MSG_ID  + rGnssProxyFwUpdateInfo.u8SeqCntrOffset]
                                                                              = GNSS_PROXY_FW_SCC_C_CONTROL_MSGID;

   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_SRV_TYPE + rGnssProxyFwUpdateInfo.u8SeqCntrOffset] =
                                                                                 (tU8) SRV_TYPE_FLASH_DATA;
   /*Update the control Messasge size */
   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize = 
                         GNSS_PROXY_FW_CONTROL_MSG_HDR_SIZE  + rGnssProxyFwUpdateInfo.u8SeqCntrOffset;

   if ( OSAL_NULL == pBuffer)
   {
      /* NULL pointer */
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "NULL pointer line %lu", __LINE__ );
   }
   else if ( 0 == u32maxbytes )
   {
      /* Invalid Parameter */
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Invalid firmware image size 0" );
   }
   else
   {
      /* Transfer the complete data from the write call */
      while ( (u32RemainBytes > 0) && (FALSE == bWriteFailed) )
      {
         GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "u32RemainBytes %lu  u32FwImageSize %lu",
                                               u32RemainBytes, rGnssProxyFwUpdateInfo.u32FwImageSize );

         if ( u32MaxBinPerMsg < u32RemainBytes )
         {
            /* Send u32MaxBinPerMsg number of bytes */
            u32BinBytesToSend = u32MaxBinPerMsg;
         }
         else
         {
            /* Send u32RemainBytes */
            u32BinBytesToSend = u32RemainBytes;
         }
         /* If we reach ONE CHUNK(5-Kb for Teseo3/ 16-Kb for Teseo-2) of data, Make sure that exactly ONE CHUNK(5-Kb for Teseo3/ 16-Kb for Teseo-2) is sent and ACK is
          * received before we proceed further */
         if ( (u32AckIntCnt + u32BinBytesToSend) > rGnssProxyFwUpdateInfo.u32TeseoAckChunkSize )
         {
            u32BinBytesToSend = rGnssProxyFwUpdateInfo.u32TeseoAckChunkSize - u32AckIntCnt;
         }
         GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "u32BinBytesToSend %lu", u32BinBytesToSend );
         /* Copy the data to be sent into Transfer buffer */
         (tVoid)OSAL_pvMemoryCopy( 
            &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_DATA + rGnssProxyFwUpdateInfo.u8SeqCntrOffset],
            (tPCVoid)(pBuffer + u32Offset),
            u32BinBytesToSend );

         /* Do this only if autosar version supports Sequence counter */
         if ( 0 != rGnssProxyFwUpdateInfo.u8SeqCntrOffset)
         {
            /* Sequence counter */
            rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_SEQ_CNTR]
                                                      =  rGnssProxyFwUpdateInfo.u8MsgSeqCntr++;
         }

         if ( OSAL_ERROR ==  GnssProxyFw_SendBinCnk
                 ( u32BinBytesToSend + GNSS_PROXY_FW_CONTROL_MSG_HDR_SIZE + rGnssProxyFwUpdateInfo.u8SeqCntrOffset) )
         {
            /* Write Failed */
            bWriteFailed = TRUE;
            s32RetVal = OSAL_ERROR;
            GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "!!!FW write() to Teseo Failed" );
         }
         /* All the data for this write call is sent */
         else
         {
            u32RemainBytes -= u32BinBytesToSend;
            u32Offset += u32BinBytesToSend;
            u32AckIntCnt += u32BinBytesToSend;
            rGnssProxyFwUpdateInfo.u32SentImageSize += u32BinBytesToSend;
         }
         /* Check IF One Chunk of data is sent and wait for ACK */
         if ( u32AckIntCnt >= rGnssProxyFwUpdateInfo.u32TeseoAckChunkSize )
         {
            GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "One Chunk FW sent, waiting for ACK" );
            if ( OSAL_ERROR == GnssProxy_s32WaitForMsg ( GNSS_PROXY_FW_TESEO_ACK,
                                                         GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
            {
               s32RetVal = OSAL_ERROR;
               bWriteFailed = TRUE;
            }
            else
            {
               /* Total rGnssProxyFwUpdateInfo.u32SentImageSize bytes of data flashed successfully */
               if ( 0!= rGnssProxyFwUpdateInfo.u32FwImageSize )
               {
                  GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Flashing GNSS Firmware %d %% complete",
                                    ((rGnssProxyFwUpdateInfo.u32SentImageSize * 100)/ rGnssProxyFwUpdateInfo.u32FwImageSize) );
                  s32RetVal = OSAL_OK;
               }
               else
               {
                  GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Invalid FW IMG Size");
               }
            }
            /* Clear the variable for next count */
            u32AckIntCnt = 0;
         }
      }
      /* If complete data in this write call is sent successfully */
      if( TRUE != bWriteFailed )
      {
         s32RetVal = OSAL_OK;
      }

       /* check  if complete Image is transferred */
      if ( rGnssProxyFwUpdateInfo.u32SentImageSize ==
                                  rGnssProxyFwUpdateInfo.u32FwImageSize )
      {
         /* If firmware is exactly divisible by 1 Chunk, Final Chunk ACK would have been received and processed above. 
            So we should not wait here again */
         if ( 0 != u32AckIntCnt)
         {
            /* Wait for the last firmware image chunk ACK (u32AckIntCnt < One_Chunk && u32AckIntCnt != 0)*/
            GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Complete Firmware image sent, waiting for ACK" );
            if ( OSAL_ERROR == GnssProxy_s32WaitForMsg ( GNSS_PROXY_FW_TESEO_ACK,
                                                         GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
            {
               s32RetVal = OSAL_ERROR;
               GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!FW Image last chunk, wait for ACK Failed" );
            }
            u32AckIntCnt = 0;
         }
         if ( OSAL_ERROR != s32RetVal )
         {
            /* Wait for CRC Verification ACK */
            GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Waiting for CRC Verification ACK" );
            if ( OSAL_ERROR == GnssProxy_s32WaitForMsg ( GNSS_PROXY_FW_TESEO_ACK,
                                                         GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
            {
               s32RetVal = OSAL_ERROR;
               GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS FW: wait for CRC-ACK Failed" );
            }
            else
            {
               /* Flashing completed successfully */
               enGnssFwUpdateStatus=GNSS_FW_UPDATE_SUCCESSFULL;
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Teseo Firmware update successful" );
               s32RetVal = OSAL_OK;
            }
         }
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_vHandleConfigMsg
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : None
 *
 * DESCRIPTION  : Checks the config message type received from v850 and
 *                sends appropriate event.
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tVoid GnssProxyFw_vHandleConfigMsg( tVoid )
{

   /* Which is the service type of the message received? */
   switch ( rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_SERVICE_TYPE] )
   {
      /* response for FLASH_BEGIN */
      case GNSS_PROXY_INC_CNTL_MSG_SRV_FLASH_BEGIN:
      {
         GnssProxyFw_vFlashBegRes();
         break;
      }
      /* response for FLASH_DATA */
      case GNSS_PROXY_INC_CNTL_MSG_SRV_FLASH_DATA:
      {
         GnssProxyFw_vFlashDataRes();
         break;
      }
      /* response from GNSS_CHIP_RESPONSE */
      case GNSS_PROXY_INC_CNTL_MSG_SRV_GNSS_CHIP_RES:
      {
         GnssProxyFw_vChipRes();
         break;
      }
      /* response for FLASH_BUF_SIZE */
      case GNSS_PROXY_INC_CNTL_MSG_SRV_FLASH_BUF_SIZE:
      {
         GnssProxyFw_vFlashBufRes();
         break;
      }
      case GNSS_PROXY_INC_CNTL_MSG_SRV_FLASH_END:
      {
         GnssProxyFw_vFlashEndRes();
         break;
      }
      /* response for UNKNOWN ERROR on V850 */
      case GNSS_PROXY_INC_CNTL_MSG_SRV_UNKNOWN_ERROR:
      {

         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "V850 Res UNKNOWN_ERROR" );
         break;
      }
      default:
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!UNKNOWN service type %x",
                  (tS32)rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_SERVICE_TYPE]);

         (tVoid)GnssProxyFw_s32SendMsg(GNSS_PROXY_FW_UNKNOWN_ERROR);
         break;
      }
   }
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_u32FlashBegRes
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : None
 *
 * DESCRIPTION  : Posts success/Fail event based of response
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tVoid GnssProxyFw_vFlashBegRes( tVoid )
{
   tU32 u32EventToSend = GNSS_PROXY_FW_UNKNOWN_ERROR;
   if ( GNSS_PROXY_MSG_SIZE_FLASH_BEGIN_RES == rGnssProxyInfo.u32RecvdMsgSize )
   {

      switch ( (tU8)rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_SERVICE_DATA] )
      {
         case GNSS_PROXY_FW_RES_SERVICE_DATA_OK:
         {
            u32EventToSend = GNSS_PROXY_FW_FLASH_BEGIN_OK;
            GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Flash_BEGIN response OK" );
            break;
         }
         case GNSS_PROXY_FW_RES_SERVICE_UNKNOWN_ERROR:
         {
            u32EventToSend = GNSS_PROXY_FW_FLASH_BEGIN_ERROR;
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Flash_BEGIN response Error" );
            break;
         }
         default:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!FLASH_BEGIN res Unknown data recvd" );
            break;
         }
      }
      /* Post the event received */
      (tVoid)GnssProxyFw_s32SendMsg(u32EventToSend);

   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "FLASH_BEGIN INV Size" );
   }
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_u32FlashDataRes
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : None
 *
 * DESCRIPTION  : Posts success/Fail event based of response
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tVoid GnssProxyFw_vFlashDataRes( tVoid )
{
   tU32 u32EventToSend = GNSS_PROXY_FW_UNKNOWN_ERROR;

   if ( GNSS_PROXY_MSG_SIZE_FLASH_DATA_RES == rGnssProxyInfo.u32RecvdMsgSize )
   {
      switch ( (tU8)rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_SERVICE_DATA] )
      {
         case GNSS_PROXY_FW_RES_SERVICE_DATA_OK:
         {
            u32EventToSend = GNSS_PROXY_FW_FLASH_DATA_OK;
            GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Flash_DATA response OK" );
            break;
         }
         case GNSS_PROXY_FW_RES_SERVICE_UNKNOWN_ERROR:
         {
            u32EventToSend = GNSS_PROXY_FW_FLASH_DATA_ERROR;
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Flash_DATA response Error" );
            break;
         }
         default:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!FLASH_DATA res Unknown data recvd" );
            break;
         }
      }
      /* Post the event received */
      (tVoid)GnssProxyFw_s32SendMsg(u32EventToSend);
   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "FLASH_DATA_RES INV Size" );
   }
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_vChipRes
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : None
 *
 * DESCRIPTION  : IF ACK or NAK is received, Posts success/Fail event based on whether ACK or NAK is received
 *                          IF NEA mesage is received call NMEA parsing function to extract and dispatch CRC
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tVoid GnssProxyFw_vChipRes( tVoid )
{
   tU32 u32EventToSend = GNSS_PROXY_FW_UNKNOWN_ERROR;
   tS32 s32NumOfTeseoResponces = (tS32)(rGnssProxyInfo.u32RecvdMsgSize - GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES);

   if ( s32NumOfTeseoResponces <= 0 )
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNNS:FW!!Invalid FLASH_CHIP_RESPONSE" );
   }   
   // NMEA response from Teseo
   else if ( GNSS_PROXY_ASCII_VALUE_DOLLAR == 
             rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES] )
   {
      GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "GNNS:FW NMEA Res Recvd" );
      GnssProxyFw_vHandleNmeaMsg();
   }
   // ACK or NAC received from Teseo
   else if( (GNSS_PROXY_FW_CMD_ACK_TESEO == rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES]) ||
            (GNSS_PROXY_FW_CMD_NACK_TESEO == rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES]) )
   {
      GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "GNSS:FW NUM of CHIP_RES %d", s32NumOfTeseoResponces);

      tS32 s32Cnt;
      for( s32Cnt = 0; s32Cnt < s32NumOfTeseoResponces; s32Cnt++ )
      {
         switch ( (tU8)rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES + s32Cnt] )
         {

            case GNSS_PROXY_FW_CMD_ACK_TESEO:
            {
               u32EventToSend = GNSS_PROXY_FW_TESEO_ACK;
               GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "TESEO: ACK recvd" );
               break;
            }
            case GNSS_PROXY_FW_CMD_NACK_TESEO:
            {
               u32EventToSend = GNSS_PROXY_FW_TESEO_NACK;
               GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!TESEO: NACK recvd" );
               break;
            }
            default:
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Switch Default file %s line %d",
                                                                         __FILE__, __LINE__  );
               break;
            }
         }
         // Post success or failure event
         if ( GNSS_PROXY_FW_UNKNOWN_ERROR != u32EventToSend )
         {
            (tVoid)GnssProxyFw_s32SendMsg(u32EventToSend);
        }
      }
   }
   // currently last option is FLASHER SYNC response
   else if ( TRUE == GnssProxyFw_bValidateTesFlasherSync((tU32)s32NumOfTeseoResponces) )
   {
      GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "FlasherSync received");
   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:unknown FLASH_CHIP_RESPONSE size %d", s32NumOfTeseoResponces);
      /* TODO: traceout complete block of memory at one shot */
      tS32 s32Cnt;
      for (s32Cnt =0; s32Cnt < s32NumOfTeseoResponces; s32Cnt++)
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW: %x",
                              rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES + s32Cnt]);
      }
   }
}
/*********************************************************************************************************************
 * FUNCTION     : GnssProxyFw_vFlashBufRes
 *
 * PARAMETER    : NONE
 *
 * RETURNVALUE  : None
 *
 * DESCRIPTION  : Posts success/Fail event based of response
 *
 * HISTORY      :
 *--------------------------------------------------------------------------------------
 * Date         |       Version         | Author & comments
 *--------------|-----------------------|-----------------------------------------------
 * 10.DEC.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
 * -------------------------------------------------------------------------------------
 *********************************************************************************************************************/
static tVoid GnssProxyFw_vFlashBufRes( tVoid )
{
   tU32 u32EventToSend = GNSS_PROXY_FW_UNKNOWN_ERROR;

   if ( GNSS_PROXY_MSG_SIZE_FLASH_BUFF_RES == rGnssProxyInfo.u32RecvdMsgSize )
   {

      switch ( rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_SERVICE_DATA] )
      {
         case GNSS_PROXY_FW_RES_SERVICE_DATA_OK:
         {
            u32EventToSend = GNSS_PROXY_FW_FLASH_BUF_SIZE_OK;
            /* Clear the variable as memcpy is done only for 2 bytes */
            rGnssProxyFwUpdateInfo.u32IncBlockSize = 0;

            OSAL_pvMemoryCopy( &rGnssProxyFwUpdateInfo.u32IncBlockSize,
                               &rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_FLASH_BUFF_SIZE],
                               GNSS_PROXY_FW_FIELD_SIZE_FLASH_BUFF );

            GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "FLASH_BUFF size  %lu", rGnssProxyFwUpdateInfo.u32IncBlockSize );
            break;
         }
         case GNSS_PROXY_FW_RES_SERVICE_UNKNOWN_ERROR:
         {
            u32EventToSend = GNSS_PROXY_FW_FLASH_BUF_SIZE_ERROR;
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!FLASH_DATA ERROR res recvd" );
            break;
         }
         default:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!FLASH_DATA res Unknown data recvd" );
            break;
         }
      }
      /* Post the event received */
      (tVoid)GnssProxyFw_s32SendMsg(u32EventToSend);

   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "FLASH_BUFF_RES INV Size" );
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_s32SendFlashEndCmd
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_ERROR/ OSAL_OK
*
* DESCRIPTION  : This Sends FLASH_END command to SCC. NO Further communication
*                possible with SCC GNSS download service after sending FLASH_END
*                to SCC.
*
* HISTORY      :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.DEC.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxyFw_s32SendFlashEndCmd( tVoid )
{
   tS32 s32RetVal = OSAL_ERROR;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_END);

   if ((tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize != 
                                    GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "FLASH_END cmd to SCC fail");
   }
   /* Wait for Response */
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_END_OK,
                                                   GNSS_PROXY_FW_FLASH_END_WAIT_TIME ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!! ERROR-FLASH_END Failed" );
   }
   else
   {
      /* Buffer size to be used for flashing is received and stored */
      GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "FLASH_END Success");
      s32RetVal = OSAL_OK;
   }

   return s32RetVal;

}

/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_vFlashEndRes
*
* PARAMETER   : None
*
* RETURNVALUE : None
*
* DESCRIPTION  : Receive Flash end and signal Socket thread
*
* HISTORY      :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.DEC.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxyFw_vFlashEndRes(tVoid)
{
   tU32 u32EventToSend = GNSS_PROXY_FW_UNKNOWN_ERROR;

   if ( GNSS_PROXY_MSG_SIZE_FLASH_END_RES == rGnssProxyInfo.u32RecvdMsgSize )
   {

      switch ( (tU8)rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_SERVICE_DATA] )
      {
         case GNSS_PROXY_FW_RES_SERVICE_DATA_OK:
         {
            u32EventToSend = GNSS_PROXY_FW_FLASH_END_OK;
            GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Flash_END response OK" );
            break;
         }
         case GNSS_PROXY_FW_RES_SERVICE_UNKNOWN_ERROR:
         {
            u32EventToSend = GNSS_PROXY_FW_FLASH_END_ERROR;
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Flash_END response Error" );
            break;
         }
         default:
         {
            GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!FLASH_END res Unknown data recvd" );
            break;
         }
      }
      /* Post the event received */
      (tVoid)GnssProxyFw_s32SendMsg(u32EventToSend);
   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "FLASH_END INV Size" );
   }
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vReleaseResource
*
* PARAMETER   : u32Resource: Resource to be released
*
* RETURNVALUE : None
*
* DESCRIPTION  : This function is used to release the acquired resources.
*
* HISTORY      :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.DEC.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxyFw_vReleaseResource(tU32 u32Resource )
{
   switch ( u32Resource )
   {
      case GNSS_PROXY_FW_RESOURCE_RELEASE_SOCKET:
      {
         if( OSAL_NULL != rGnssProxyInfo.s32SocketFD )
         {

   #ifdef GNSS_PROXY_TEST_STUB_ACTIVE
            if ( 0 != shutdown( rGnssProxyInfo.s32SocketFD,0 ) )
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "shutdown socket Failed" );
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "shutdown of socket is successful" );
            }
   #endif
            if ( 0 != close( rGnssProxyInfo.s32SocketFD ) )
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW socket close Failed" );
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "SOCKET closed successfully" );
            }
         }
         break;
      }
      case GNSS_PROXY_FW_RESOURCE_RELEASE_SCC_RESPONSE_EVENT:
      {
          if( rGnssProxyInfo.hGnssProxyTeseoComEvent != OSAL_C_INVALID_HANDLE )
          {
             if( OSAL_OK != OSAL_s32EventClose( rGnssProxyInfo.hGnssProxyTeseoComEvent ))
             {
                GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW SCC Event close failed err %lu",
                                                       OSAL_u32ErrorCode() );
             }
             else if( OSAL_OK != OSAL_s32EventDelete( GNSS_PROXY_FW_UPDATE_EVENT_NAME ))
             {
                GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW SCC Event Delete failed err %lu",
                                                       OSAL_u32ErrorCode() );
                rGnssProxyInfo.hGnssProxyTeseoComEvent = OSAL_C_INVALID_HANDLE;
             }
             else
             {
                rGnssProxyInfo.hGnssProxyTeseoComEvent = OSAL_C_INVALID_HANDLE;;
             }
          }
          break;
      }
      case GNSS_PROXY_FW_RESOURCE_RELEASE_INTER_COMM_EVENT:
      {
         if( rGnssProxyFwUpdateInfo.hGnssFwInEve != OSAL_C_INVALID_HANDLE )
         {
            if( OSAL_OK != OSAL_s32EventClose( rGnssProxyFwUpdateInfo.hGnssFwInEve ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW INT Event close failed err %lu",
                                                      OSAL_u32ErrorCode() );
            }
            else if( OSAL_OK != OSAL_s32EventDelete( GNSS_PROXY_FW_UPDATE_INT_EVENT_NAME ))
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW INT Event Delete failed err %lu",
                                                      OSAL_u32ErrorCode() );
               rGnssProxyFwUpdateInfo.hGnssFwInEve = OSAL_C_INVALID_HANDLE;
            }
            else
            {
               rGnssProxyFwUpdateInfo.hGnssFwInEve = OSAL_C_INVALID_HANDLE;;
            }
         }
         break;
      }
      default:
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW Default line %lu arg %x",
                                                             __LINE__, u32Resource );
         break;
      }
   }

}
/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_vHandleNmeaMsg
*
* PARAMETER   : None
*
* RETURNVALUE : None
*
* DESCRIPTION : Index NMEA message, check for checksum errors 
*               and dispatch it for extracting data.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.DEC.2013 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxyFw_vHandleNmeaMsg()
{
   tULong uPduEndAdd;
   tPChar pcCharToParse;
   tPChar pcFieldIndex[GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE];
   tU32 u32Tokens = 0;
   tU8 u8Chksumcalc =0;
   tU8 u8ChksumFrmMsg = 0;
   tBool bChksumCalcDone = FALSE;
   tBool bIndexLimitReached = FALSE;

   pcCharToParse = (tPChar)&( rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES] );
   uPduEndAdd =  (tULong)(pcCharToParse + 
                           rGnssProxyInfo.u32RecvdMsgSize - GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES);

   pcFieldIndex[u32Tokens] = pcCharToParse;
   u32Tokens++;
   
   /* Index current record. i.e. Store pointers to each field in NMEA message in pcFieldIndex */
   /* Stop indexing when we reach end of NMEA message i.e line feed */
   while( ((tULong)pcCharToParse < uPduEndAdd) &&
          ( GNSS_PROXY_ASCII_VALUE_CARRIAGE_RETURN != *pcCharToParse )&&
          ( TRUE != bChksumCalcDone) )
   {
      /* Checksum calculation from message */
      /* Here we extract original checksum from message */
      if( *pcCharToParse == GNSS_PROXY_NMEA_CHECKSUM_DELIMITER_ASTERISK )
      {
         u8ChksumFrmMsg = (tU8)OSAL_u32StringToU32( (pcCharToParse+1),
                                                         OSAL_NULL,
                                                         16 );
         bChksumCalcDone = TRUE;
      }
      /* Here we calculate checksum */
      else if ( (*pcCharToParse !=  GNSS_PROXY_ASCII_VALUE_DOLLAR) &&
                (bChksumCalcDone == FALSE) )
      {
         u8Chksumcalc ^= (tU8)*pcCharToParse;
      }

      /* "," is the delimiter in NMEA messages. so search for the "," and store the address 
         of next field in pcFieldIndex. This is indexing*/
      if((( *pcCharToParse == GNSS_PROXY_NMEA_FIELD_DELIMITER_COMMA ) ||
            ( *pcCharToParse == GNSS_PROXY_NMEA_CHECKSUM_DELIMITER_ASTERISK ))&&
            ( u32Tokens < GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE) )
      {
         pcFieldIndex[u32Tokens] = (pcCharToParse+1);
         u32Tokens++;
         *pcCharToParse = '\0';
      }
      /* This should never happen. All the NMEA and ST messages are of size less than
         GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE. If a new NMEA or ST special message is introduced,
         this has to be checked */
      else if( u32Tokens >= GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS , GNSSPXY_DEF_TRC_RULE, "FW:Increase FIELDS_IN_NMEA_MESSAGE" );
         u32Tokens=0;
         bIndexLimitReached = TRUE;
      }
      /* parse next character */
      pcCharToParse++;
   }

   GnssProxy_vTraceOut( TR_LEVEL_USER_4 , GNSSPXY_DEF_TRC_RULE, "FW:u8Chksumcalc=0x %x u8ChksumFrmMsg=0x %x",
                                        u8Chksumcalc, u8ChksumFrmMsg );
   /* Check if the calculated checksum and checksum from the message is same*/
   if ( bChksumCalcDone != TRUE )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "FW:Checksum Identifier Missing");
      rGnssProxyData.rGnssTmpFixInfo.bChecksumError = TRUE;
   }
   else if( u8Chksumcalc != u8ChksumFrmMsg )
   {
       GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "FW:!!!Checksum error");
       rGnssProxyData.rGnssTmpFixInfo.bChecksumError = TRUE;
   }
   /* In this case, we have a corrupt record */
   else if ( bIndexLimitReached == TRUE )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "FW:Index crossed beyond received data" );
   }
   else if ( OSAL_NULL == pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE] )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "FW:Message index field is null" );
   }
   else if (OSAL_NULL == pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE] )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "GnssFw:Nmea Msg index is null");
   }
   /*Teseo Bin_Image Version*/
   /*PSTMVER*/
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMVER ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMVER == u32Tokens )
      {
         GnssProxyFw_vHandlePSTMVER( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                               "GnssFw:corrupted PSTMVER message");
      }
   }
   /* $PSTMCRCCHECK */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                            GNSS_PROXY_MSG_TYPE_PSTMCRCCHECK ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMCRCCHECK == u32Tokens )
      {
         GnssProxyFw_vHandlePSTMCRCCHECK( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GnssFw:corrupted PSTMCRCCHECK message");
      }
   }
   /* $PSTMSAVEPAROK */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                            GNSS_PROXY_MSG_TYPE_PSTMSAVEPAROK ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMSAVEPAROK == u32Tokens )
      {
         GnssProxyFw_vHandlePSTMSAVEPAROK( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GnssFw:corrupted PSTMSAVEPAROK message");
      }
   }
   /* $PSTMSETPAROK */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                            GNSS_PROXY_MSG_TYPE_PSTMSETPAR_OK ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMSETPAR_OK == u32Tokens )
      {
         GnssProxyFw_vHandlePSTMSETPAROK( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GnssFw:corrupted PSTMSETPAROK message");
      }
   }
   /* $GPTXT */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                            GNSS_PROXY_MSG_TYPE_GPTXT ) == 0 )
   {
         GnssProxyFw_vHandleGPTXT( (const tChar * const*) pcFieldIndex );
   }

   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GnssFw:Unknown NMEA %s",
                                                             pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE] );
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_s32SetChipType
*
* PARAMETER   : tEnGnssChipType enGnssChipType : GNSS chip type mounted on board
*
* RETURNVALUE : OSAL_OK on success ELSE OSAL error codes
*
* DESCRIPTION : To store the GNSS chip type mounted on the board.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 18.DEC.2015 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxyFw_s32SetChipType ( tEnGnssChipType enGnssChipType )
{
   tS32 s32RetVal = OSAL_E_INVALIDVALUE;
   
   if ( (GNSS_CHIP_TYPE_TESEO_2 == enGnssChipType) || 
        (GNSS_CHIP_TYPE_TESEO_3 == enGnssChipType) )
   {
      rGnssProxyFwUpdateInfo.enGnssChipType = enGnssChipType;
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                           "GNSSFW:CHIP type %d", (tS32)enGnssChipType);

      if (GNSS_CHIP_TYPE_TESEO_3 == enGnssChipType)
      {
         rGnssProxyFwUpdateInfo.u32TeseoAckChunkSize = GNSS_PROXY_FW_ACK_WAIT_CHUNK_SIZE_TESEO_3;
      }
      else
      {
         rGnssProxyFwUpdateInfo.u32TeseoAckChunkSize = GNSS_PROXY_FW_ACK_WAIT_CHUNK_SIZE_TESEO_2;
      }

      s32RetVal = OSAL_OK;
   }
   else
   {
       GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,"GNSSFW:Unknown GNSS CHIP type");
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_vCreateIncCtrlMsg
*
* PARAMETER   : tEnIncCtrlMsgSrvType enIncSrvType: Service type of the control message to be prepared
*
* RETURNVALUE : NONE
*
* DESCRIPTION : To prepare complete control message of requested type to be sent to SCC.
*               This also adds an updated sequence counter.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 18.DEC.2015 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxyFw_vCreateIncCtrlMsg ( tEnIncCtrlMsgSrvType enIncSrvType )
{

   /* CONTROL_MESSAGE is a TP message. So first byte is always TP_MARKER */
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;

   /* Do this only if autosar version supports Sequence counter */
   if ( 0 != rGnssProxyFwUpdateInfo.u8SeqCntrOffset)
   {
      /* Sequence counter */
      rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_SEQ_CNTR] =  rGnssProxyFwUpdateInfo.u8MsgSeqCntr++;
   }
   /* Fill in the control message ID */
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_MSG_ID  + rGnssProxyFwUpdateInfo.u8SeqCntrOffset]
                                                                               = GNSS_PROXY_FW_SCC_C_CONTROL_MSGID;

   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_SRV_TYPE + rGnssProxyFwUpdateInfo.u8SeqCntrOffset] =
                                                                                  (tU8) enIncSrvType;
   /*Update the control Messasge size */
   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize = 
                          GNSS_PROXY_FW_CONTROL_MSG_HDR_SIZE  + rGnssProxyFwUpdateInfo.u8SeqCntrOffset;   
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_s32ExchUartSync
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK for Success OSAL_ERROR for Failure
*
* DESCRIPTION : Send GNSS_PROXY_FW_TESEO_3_FLASHER_IDENTIFIER_WORD to Teseo
*               Wait for Response TESEO2_FLASHER_SYNC from Teseo
*               Retry in case of Failures
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 18.DEC.2015 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxyFw_s32ExchUartSync()
{
   tS32 s32RetVal = OSAL_ERROR;
   tBool bSyncSuccess = FALSE;
   tU32 u32FlasherIdenWord = GNSS_PROXY_FW_TESEO_3_FLASHER_IDENTIFIER_WORD;
   tU32 u32count;
   tU32 u32ResultMask = 0;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);

   // THIS IS NEEDED BECAUSE, THE SEQUENCE WIL BE UPDATED IN THE LOOP AGAIN
   rGnssProxyFwUpdateInfo.u8MsgSeqCntr--;

   OSAL_pvMemoryCopy(
      &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
      &u32FlasherIdenWord,
      sizeof(u32FlasherIdenWord) );

   /* Size of the INC message with the binary options */
   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += sizeof(u32FlasherIdenWord);

   for ( u32count =0; 
         (u32count < GNSS_PROXY_FW_FLASHER_SYNC_RETRY_COUNT) && (FALSE == bSyncSuccess) ;
         u32count++ )
   {

      /* Do this only if autosar version supports Sequence counter */
      if ( 0 != rGnssProxyFwUpdateInfo.u8SeqCntrOffset)
      {
         /* Sequence counter */
         rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_FW_OFFSET_C_CONTROL_SEQ_CNTR] = rGnssProxyFwUpdateInfo.u8MsgSeqCntr++;
      }

      if ( GnssProxy_s32SendDataToScc( rGnssProxyFwUpdateInfo.u16FilledTxMsgSize) != 
                                       (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "sending FLASHER_IDENTIFIER Failed" );
         // Wait for some time before doing a retry.
         OSAL_s32ThreadWait(1000);
      }
      /* Expected events are FLASH_DATA_OK or FLASH_CHIP_RESPONSE(FLASHER_SYNC_WORD) */
      else if (OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                 GNSS_PROXY_FW_EVENT_MASK_ALL,
                                                 OSAL_EN_EVENTMASK_OR,
                                                 GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME,
                                                 &u32ResultMask))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "EventWait Failed for Event %x err %lu",
                                             (GNSS_PROXY_FW_T3_FLASHER_SYNC_WORD|GNSS_PROXY_FW_FLASH_DATA_OK), OSAL_u32ErrorCode());
      }
      /* Clear received event */
      else if (OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                ~(u32ResultMask),
                                                OSAL_EN_EVENTMASK_AND) )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                 "GnssProxy_s32WaitForMsg():Event Post failed err %lu Event %lu at Line %lu",
                                               OSAL_u32ErrorCode(), u32ResultMask, __LINE__ );
      }
      /* Post the event consumed signal */
      else if (OSAL_ERROR == OSAL_s32EventPost( rGnssProxyFwUpdateInfo.hGnssFwInEve,
                                                u32ResultMask,
                                                OSAL_EN_EVENTMASK_OR) )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
             "GnssProxy_s32WaitForMsg():Event Post failed err %lu Event %lu at Line %lu",
                                                    OSAL_u32ErrorCode(), u32ResultMask );
      }
      /* If FLASHER_SYNC_WORD is received, go to next stage  */
      else if( u32ResultMask & GNSS_PROXY_FW_T3_FLASHER_SYNC_WORD )
      {
         s32RetVal = OSAL_OK;
         bSyncSuccess = TRUE;
         /* There is always a possibility that we have sent an extra FLASHER_IDENTIFIER message. Wait till you get the response for this from V850.
            This extra FLASHER_IDENTIFIER will not cause any problem from Teseo. But in our design, we need to make sure that all the requests are
            attended and cleared before we go to next stage*/
         OSAL_s32ThreadWait(500);
         if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                    GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
         {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                                 "!!!GNSS_PROXY_FW_T3_FLASHER_SYNC_WORD clearing FLASH_DATA_OK Failed" );
         }
      }
      /* If FLASH_DATA_OK is received, Send FLASHER_IDENTIFIER (Sync Word) to Teseo Again  */
      else if ( u32ResultMask & GNSS_PROXY_FW_FLASH_DATA_OK )
      {
         // Send Flasher identifier again
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                              "UART_SYNC FLASHER_IDENTIFIER Trial %lu", u32count);
      }
      else
      {
         // Send Flasher identifier again
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                              "UART_SYNC: Unknoen event received %x FLASHER_IDENTIFIER Trial %lu",
                              u32ResultMask, u32count);
      }
      u32ResultMask = 0;
   }/* End of For */

   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_bValidateTesFlasherSync
*
* PARAMETER   : u32SizeOfChipRes: Number of responses in units of bytes
*
* RETURNVALUE : TRUE for Success FALSE for Failure
*
* DESCRIPTION : Check if the response from Teseo is TESEO2_FLASHER_SYNC
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 18.DEC.2015 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool GnssProxyFw_bValidateTesFlasherSync( tU32 u32SizeOfChipRes )
{
   tBool bRetVal = FALSE;
   tU32 u32FlasherSyncWord = 0;

   /* Check for size of received response */
   if ( sizeof(u32FlasherSyncWord) != u32SizeOfChipRes )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                                   "Invalid size for Flasher Sync: Recvd size:", u32SizeOfChipRes );
   }
   else
   {
      OSAL_pvMemoryCopy( &u32FlasherSyncWord,
                         &rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_FW_OFFSET_R_CONFIG_CHIP_RES],
                         sizeof(u32FlasherSyncWord) );
      /* Check for sequence of received word */      
      if ( GNSS_PROXY_FW_TESEO_3_FLASHER_SYNC_WORD != u32FlasherSyncWord )
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                                      "Invalid Seq Flasher Sync Recvd: 0x %x", u32FlasherSyncWord );
      }
      
      /* Post the event for flasher sync */
      else if(OSAL_ERROR == GnssProxyFw_s32SendMsg(GNSS_PROXY_FW_T3_FLASHER_SYNC_WORD))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW: Msg send Failed :0x %x",
                                                                    GNSS_PROXY_FW_T3_FLASHER_SYNC_WORD );
      }
      else
      {
         /* All is well. */
         bRetVal = TRUE;
         GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "FLASHER_SYNC Success" );
      }
   }
   return bRetVal;
}


/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_s32SendFlasherSync
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK for Success OSAL_ERROR for Failure
*
* DESCRIPTION : Send Flasher Sync block to Teseo
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 18.DEC.2015 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxyFw_s32SendFlasherSync(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;
   trTeseo3XloaderPreamble rTeseo3XloaderPreamble;

   /* Create FLASH_DATA Message header */
   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);

   /* Fill in flasher sync data: Each field in 4 Bytes*/

   /*Order: 1: XLOADER_IDENTIFIER_MSP 
            2: XLOADER_IDENTIFIER_LSP 
            3: XLOADER_OPTIONS
            4: XLOADER_DEFAULT_DESTINATION_ADDRESS
            5: XLOADER (Flasher.bin) CRC
            6: XLOADER (Flasher.bin) Syze
            7: XLOADER_DEFAULT_ENTRY_POINT
            */

   rTeseo3XloaderPreamble.u32XloaderIdentifierMsp = GNSS_PROXY_FW_TESEO_3_XLOADER_IDENTIFIER_MSP;
   rTeseo3XloaderPreamble.u32XloaderIdentifierLsp = GNSS_PROXY_FW_TESEO_3_XLOADER_IDENTIFIER_LSP;
   rTeseo3XloaderPreamble.u32XloaderOptions =       GNSS_PROXY_FW_TESEO_3_XLOADER_OPTIONS;
   rTeseo3XloaderPreamble.u32XloaderDefDestAddr =   GNSS_PROXY_FW_TESEO_3_XLOADER_DEFAULT_DESTINATION_ADDRESS;
   rTeseo3XloaderPreamble.u32XloaderCrc32 =         rGnssProxyFwUpdateInfo.u32BootLdrChksum;
   rTeseo3XloaderPreamble.u32XloaderSize =          rGnssProxyFwUpdateInfo.u32BootLdrSize;
   rTeseo3XloaderPreamble.u32XloaderDefEntryPoint = GNSS_PROXY_FW_TESEO_3_XLOADER_DEFAULT_ENTRY_POINT;

   /* Copy the preable into TX buffer */
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                      &rTeseo3XloaderPreamble,
                      sizeof(trTeseo3XloaderPreamble) );

   /* Update TX buffer send size */
   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += sizeof(trTeseo3XloaderPreamble);

   /* Send it to Teseo */
   if ( GnssProxy_s32SendDataToScc( rGnssProxyFwUpdateInfo.u16FilledTxMsgSize) != 
                                    (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "sending Xloader Preamble Failed" );
   }
   /* Wait for FLASH_DATA OK response from V850*/
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                   GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Res:Xloader Preamble FLASH_DATA_OK Failed" );
   }
   else
   {
      /* No resposnse will be received from Teseo For this block */
      s32RetVal = OSAL_OK;
      GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Xloader Preamble sent" );
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_s32SendFlasherReady
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK for Success OSAL_ERROR for Failure
*
* DESCRIPTION : Send FLASHER_READY byte to Teseo and wait for ACK from Teseo
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 18.DEC.2015 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxyFw_s32SendFlasherReady( tVoid )
{
   tS32 s32RetVal = OSAL_ERROR;

   /* Create FLASH_DATA Message header */
   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);

   rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize] =
                                                       GNSS_PROXY_FW_CMD_FLASHER_READY_TESEO_3;

   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += GNSS_PROXY_FW_SIZE_OF_FLASHER_READY_FIELD;

   /* Send FLASHER_READY Message */
   if ((tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize != 
                                    GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW: FLASHER_READY cmd to SCC fail");
   }
   else if ( GNSS_PROXY_FW_UPDATE_COMPONENT_VERSION_TWO <= rGnssProxyFwUpdateInfo.u8SccSwVer )
   {
      /* Wait for FLASH_DATA OK response from V850*/
      if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                 GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:Res:ERROR-FLASHER_READY FLASH_DATA_OK Failed" );
      }
      /* Wait for HOST_READY Response which is ACK from Teseo */
      else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_ACK,
                                                      GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:Res:ERROR-FLASHER_READY ACK wait Failed" );
      }
      else
      {
         GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "FLASHER_READY Success");
         s32RetVal = OSAL_OK;
      }
   }
   /* Wait for FLASHER_READY Response which is ACK from Teseo */
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_ACK,
                                                   GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW: Res:ERROR-FLASHER_READY Failed" );
   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "FLASHER_READY Success");
      s32RetVal = OSAL_OK;
   }

   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_s32FlashUnlock
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK for Success OSAL_ERROR for Failure
*
* DESCRIPTION : Unlock Teseo Flash:
*                 1: Unlock FLash
*                 2: Disable All positioning NMEA commands Except GPTXT
*                 3: Save the configuration persistently
*                 4: Reboot Teseo with $PSTMRR.
*                    Teseo will do the flash unlock only in the next boot time after receiving flash unlock command.
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxyFw_s32FlashUnlock()
{
   tS32 s32RetVal =  OSAL_ERROR;
   
   /*----------------Send Flash unlock command----------------------*/
   
   /* Create FLASH_DATA Message header */
   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);

   /* Fill in flash unlock command */
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                      GNSS_PROXY_TESEO_FLASH_UNLOCK_CMD,
                      OSAL_u32StringLength(GNSS_PROXY_TESEO_FLASH_UNLOCK_CMD) );

   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += (tU16)
                  OSAL_u32StringLength(GNSS_PROXY_TESEO_FLASH_UNLOCK_CMD);
   
   /* Send the INC message to V850 */
   if ( GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize ) != 
                                                                     (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Gnssfw:write fail for Flash-unlock cmd" );
   }
   /* Wait for FLASH_DATA OK response from V850*/
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                              GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:Res:ERROR-Flash-unlock FLASH_DATA_OK Failed" );
   }
   /* Wait for PSTMSETPAR Response from Teseo */
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_FLASH_PRO_CFG_BLK_WAIT_EVENT,
                                                   GNSS_PROXY_FW_TESEO_NMEA_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:Res:ERROR-Flash-unlock PSTMSETPAROK wait Failed" );
   }
   else
   {
      if ( OSAL_OK != GnssProxyFw_s32DisableNmeaMsgs() )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR flash-unlock Disable NMEA failed Failed" );
      }
      /* If disabling NMEA messages fail, still we can try to unlock the flash */
      
      /* Save configuration persistently */
      if ( OSAL_OK != GnssProxyFw_s32SaveTesCfgPersist() )
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR flash-unlock Disable NMEA failed Failed" );
      }
      else if ( OSAL_OK != GnssProxyFw_s32ReBootTesNormalMode() )
      {
        GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR flash-unlock Teseo Reboot Failed" );
      }
      else
      {
         s32RetVal = OSAL_OK;
         GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "GNSSFW: Flash-unlock Complete" );
      }
      
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_vHandlePSTMVER()
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer

*
* RETURNVALUE : OSAL_OK for Success OSAL_ERROR for Failure
*
* DESCRIPTION : Parses $PSTMVER to Evaluate Teseo binary image firmware version.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 21.Jun.2016 | Initial version: 1.0 | Sanjay G ( RBEI/ECF5 )
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxyFw_vHandlePSTMVER( tChar const * const pcFieldIndex[] )
{
   tString sBinImgVer,sNxtInvChar;
   tU32 u32LoopCnt;

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_RCVD_PSTMVER, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                           "GnssFw:vHandlePSTMVER: NULL pointer argument" );
   }
   else
   {
      sBinImgVer = (tString)pcFieldIndex[GNSS_PROXY_PSTMVER_OFFSET_BINIMG_VER];
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT,
                                GNSSPXY_PSTMVER_BIN_VER, " %s",sBinImgVer);

      if( OSAL_s32StringNCompare( sBinImgVer,
                                  GNSS_PROXY_TESEO_BIN_IMAGE_LIB,
                                  sizeof(GNSS_PROXY_TESEO_BIN_IMAGE_LIB) - 1 ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                              "GnssFw:vHandlePSTMVER:Unknown type received" );
      }
      else
      {
         sBinImgVer += sizeof( GNSS_PROXY_TESEO_BIN_IMAGE_LIB );

         for (u32LoopCnt =0;
              u32LoopCnt < GNSS_PROXY_NUM_FIELDS_TESEO_FIRMWARE_MSG;
              u32LoopCnt++ )
         {
            rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer <<= 8;
            rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer +=
                  OSAL_u32StringToU32( sBinImgVer,
                                       &sNxtInvChar,
                                       GNSS_PROXY_DATA_FORM_BASE_TEN );

            sBinImgVer = sNxtInvChar + 1;
         }

         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMVER_BIN_VER2,
                           " 0x0%x",
                           rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer );

         //! teseo2 binary image vesion starts with 3 i.e. something like
         //! 3.x.x.x whereas teseo3's starts with 4 something like 4.x.x.x
         if ( GNSS_PROXY_TESEO_3_FW_VER_MAJOR_NUMBER ==
                  (rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer & 
                            GNSS_PROXY_TESEO_FW_VER_MAJOR_NUMBER_MASK ) )
         {
            //! gnss receiver on board is teseo3.
            rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8089;
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                                 "GnssFw:vHandlePSTMVER:teseo3 is on board" );
         }
         else if  ( GNSS_PROXY_TESEO_2_FW_VER_MAJOR_NUMBER == 
                         ( rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer &
                                GNSS_PROXY_TESEO_FW_VER_MAJOR_NUMBER_MASK ) )
         {
            //! gnss receiver on board is teseo2.
            rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                                 "GnssFw:vHandlePSTMVER:teseo2 is on board" );
         }
         else
         {
            //! don't know the gnss receiver on board. use default teseo2.
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                  "GnssFw: unknown teseo Type, using default config teseo2" );
            rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
         }

         (tVoid)GnssProxyFw_s32SendMsg( GNSS_PROXY_FW_TESEO_FIRMWARE_VERSION_WAIT_EVENT );

      }
   }
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_s32GetTeseoFwVer
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_OK on success
*               OSAL_ERROR on Failure
*
* DESCRIPTION : 1: Send command to get teseo binary image version.
*               2: Wait for response with timeout
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 21.Jun.2016 | Initial version: 1.0 | Sanjay G ( RBEI/ECF5 )
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxyFw_s32GetTeseoFwVer( void )
{

   tS32 s32RetVal = OSAL_ERROR;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);

   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[ rGnssProxyFwUpdateInfo.u16FilledTxMsgSize ],
                      GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY,
                      OSAL_u32StringLength(GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY) );

   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize  += 
            (tU16)OSAL_u32StringLength( GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY );

   if ( (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize  != 
         GnssProxy_s32SendDataToScc( rGnssProxyFwUpdateInfo.u16FilledTxMsgSize ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                               "GnssFw:send failed to get firmware version" );
   }
   /* Wait for response from ST */
   else
   {
      /* Wait for FLASH_DATA OK response from V850*/
      if (OSAL_ERROR == 
             GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                      GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                           "GnssFw:firmware version FLASH_DATA_OK Failed" );
      }

      if ( OSAL_ERROR == 
            GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_FIRMWARE_VERSION_WAIT_EVENT,
                                     GNSS_PROXY_FW_TESEO_NMEA_WAIT_TIME  ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                              "GnssFw:get teseo fw version event wait failed err %d",
                              OSAL_u32ErrorCode() );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT,GNSSPXY_DEF_TRC_RULE,
                              "GnssFw: got teseo firmware version" );
         s32RetVal = OSAL_OK;
      }
   }

   return s32RetVal;

}
/*********************************************************************************************************************
* FUNCTION    : GnssProxyFw_s32GetTeseoCRC
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_OK on success
*               OSAL_ERROR on Failure
*
* DESCRIPTION : 1: Send command to get teseo CRC.
*               2: Wait for response with timeout
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxyFw_s32GetTeseoCRC()
{
   tS32 s32RetVal = OSAL_ERROR;
   tPChar pcCrcCmd = OSAL_NULL;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);

   // Choose CRC comand based on Teseo variant type.
   if ( GNSS_HW_STA8088 == rGnssProxyInfo.rGnssConfigData.enGnssHwType )
   {
      pcCrcCmd = GNSS_PROXY_TESEO_CRC_CHECK_CMD_TESEO_2;
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT,
                           GNSSPXY_DEF_TRC_RULE, "GnssFw: getting crc for teseo2" );
   }
   else if ( GNSS_HW_STA8089 == rGnssProxyInfo.rGnssConfigData.enGnssHwType )
   {
      pcCrcCmd = GNSS_PROXY_TESEO_CRC_CHECK_CMD_TESEO_3;
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT,
                           GNSSPXY_DEF_TRC_RULE, "GnssFw: getting crc for teseo3" );
   }
   else
   {
      pcCrcCmd = GNSS_PROXY_TESEO_CRC_CHECK_CMD_TESEO_2;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                           "GnssFw: unknown teseo variant, using teseo2 crc get command" );
   }

   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                      pcCrcCmd,
                      OSAL_u32StringLength(pcCrcCmd) );

   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize  += (tU16)OSAL_u32StringLength(pcCrcCmd);

   if ( GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize) != 
                                                   (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GnssFW:write fail for crc check cmd" );
   }
   /* Wait for response from ST */
   else
   {
      /* Wait for FLASH_DATA OK response from V850*/
      if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                 GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:CRC FLASH_DATA_OK Failed" );
      }
   
      if ( OSAL_ERROR == GnssProxy_s32WaitForMsg(GNSS_PROXY_FW_TESEO_CRC_CHECK_WAIT_EVENT,
                                                 GNSS_PROXY_FW_TESEO_NMEA_WAIT_TIME))
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSSFW:Crc check:Event wait failed err %d",
                                                OSAL_u32ErrorCode() );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_CRCCHK_WT_SUC, NULL );
         s32RetVal = OSAL_OK;
      }
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMCRCCHECK
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : Parses $PSTMCRCCHECK to Evaluate CRC of Teseo
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxyFw_vHandlePSTMCRCCHECK( tChar const * const pcFieldIndex[] )
{
   tString sExtractedCrc;

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_RCVD_PSTMCRCCHECK, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "GnssFw:!!! pointers to GnssProxy_vHandlePSTMCRCCHECK are null" );
   }
   else
   {
      //Extract GNSS FW CRC from the response
      sExtractedCrc = (tString)pcFieldIndex[GNSS_PROXY_PSTMCRCCHECK_OFFSET_RECVD_CRC];   
      rGnssProxyInfo.rGnssConfigData.u32GnssRecvFwCrc =
            OSAL_u32StringToU32( sExtractedCrc,
                                 OSAL_NULL,
                                 GNSS_PROXY_DATA_FORM_BASE_SIXTEEN );

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMCRCCHECK_CRC, "GnssFw: 0x%x",
                           rGnssProxyInfo.rGnssConfigData.u32GnssRecvFwCrc );
     
      (tVoid)GnssProxyFw_s32SendMsg(GNSS_PROXY_FW_TESEO_CRC_CHECK_WAIT_EVENT);
   }

}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMSETPAROK
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : This is positive response for $PSTMSETPAR.
*               A event will be posted for the application thread waiting
*               for this response.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 22.FEB.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxyFw_vHandlePSTMSETPAROK ( tChar const * const pcFieldIndex[] )
{
   tU32 u32CfgBlk = 0;

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_RCVD_PSTMSETPAROK, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "GnssFw:!!! NULL at GnssProxy_vHandlePSTMSETPAROK" );
   }
   else
   {
      /* Get the config block for which the response is received */
      u32CfgBlk = OSAL_u32StringToU32( pcFieldIndex[ GNSS_PROXY_PSTMSETPAROK_OFFSET_CONF_BLK_ID ],
                                                     OSAL_NULL,
                                                     GNSS_PROXY_DATA_FORM_BASE_SIXTEEN );

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                           "GnssFw:CFG Block_OK %x", u32CfgBlk );

      switch (u32CfgBlk)
      {
         case GNSS_PROXY_NMEA_LIST_1_BLOCK_ID:
         {

            /* Application will be blocked waiting for the config response event. */
            (tVoid)GnssProxyFw_s32SendMsg(GNSS_PROXY_FW_TESEO_NMEA_LIST_1_CFG_BLK_WAIT_EVENT);

            break;
         }

         case GNSS_PROXY_NMEA_LIST_2_BLOCK_ID:
         {
            /* Application might be blocked waiting for config. */
            (tVoid)GnssProxyFw_s32SendMsg(GNSS_PROXY_FW_TESEO_NMEA_LIST_2_CFG_BLK_WAIT_EVENT);
         }
         break;

         case GNSS_PROXY_FLASH_PROTECTION_BLOCK_ID:
         {
            /* Application might be blocked waiting for config. */
            (tVoid)GnssProxyFw_s32SendMsg(GNSS_PROXY_FW_TESEO_FLASH_PRO_CFG_BLK_WAIT_EVENT);
         }
         break;

         default:
         {
            GnssProxy_vTraceOut( TR_LEVEL_ERROR, GNSSPXY_SWITCH_DEF, " %d , %s",
                                                      __LINE__, __FILE__ );
            break;
         }
      }
   }
}

/*****************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMSAVEPAROK
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : This is positive response for $PSTMSAVEPAR.
*               A event will be posted for the application thread waiting
*               for this response.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 22.FEB.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
****************************************************************************/
static tVoid GnssProxyFw_vHandlePSTMSAVEPAROK ( tChar const * const pcFieldIndex[] )
{

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_RCVD_PSTMSAVEPAROK, NULL );
   

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "GnssFw:!!! pointers to GnssProxy_vHandlePSTMSAVEPAROK are null" );
   }
   //Someone is waiting for this event
   else
   {
      (tVoid)GnssProxyFw_s32SendMsg(GNSS_PROXY_FW_TESEO_COM_SAVE_CDB_RESPONSE_EVENT);
   }
}

/*****************************************************************************
* FUNCTION    : GnssProxyFw_s32DisableNmeaMsgs
*
* PARAMETER   : None
*
* RETURNVALUE  : OSAL_OK on success
*                OSAL_ERROR on failure
*
* DESCRIPTION : Used to Turn-off all NMEA messages.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 22.FEB.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
****************************************************************************/
static tS32 GnssProxyFw_s32DisableNmeaMsgs( tVoid )
{
   tS32 s32RetVal = OSAL_ERROR;

   /* Create FLASH_DATA Message header */
   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);

   /* Fill in NMEA BLOCK 1 Disable command */
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                      GNSS_PROXY_TESEO_DISABLE_NMEA_BLK_201,
                      OSAL_u32StringLength(GNSS_PROXY_TESEO_DISABLE_NMEA_BLK_201) );

   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += (tU16)
                  OSAL_u32StringLength(GNSS_PROXY_TESEO_DISABLE_NMEA_BLK_201);
   
   /* Send the INC message to V850 */
   if ( GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize ) != 
                                                                     (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Gnssfw:write fail for NMEA Blk 1 disable cmd" );
   }
   /* Wait for FLASH_DATA OK response from V850*/
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                              GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:Res:ERROR for NMEA Blk 1 disable FLASH_DATA_OK Failed" );
   }
   /* Wait for FLASH_DATA OK response from V850*/
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_NMEA_LIST_1_CFG_BLK_WAIT_EVENT,
                                                   GNSS_PROXY_FW_TESEO_NMEA_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR NMEA Blk 1 disable res from Teseo" );
   }
   /* Disable Second Block */
   else
   {
      /* Create FLASH_DATA Message header */
      GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);
      
      /* Fill in NMEA BLOCK 2 Disable command */
      OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                         GNSS_PROXY_TESEO_DISABLE_NMEA_BLK_228,
                         OSAL_u32StringLength(GNSS_PROXY_TESEO_DISABLE_NMEA_BLK_228) );
      
      rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += (tU16)
                     OSAL_u32StringLength(GNSS_PROXY_TESEO_DISABLE_NMEA_BLK_228);
      
      /* Send the INC message to V850 */
      if ( GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize ) != 
                                                                        (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Gnssfw:write fail for NMEA Blk 2 disable cmd" );
      }
      /* Wait for FLASH_DATA OK response from V850*/
      else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                 GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:Res:ERROR for NMEA Blk 2 disable FLASH_DATA_OK Failed" );
      }
      /* Wait for FLASH_DATA OK response from V850*/
      else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_NMEA_LIST_2_CFG_BLK_WAIT_EVENT,
                                                      GNSS_PROXY_FW_TESEO_NMEA_WAIT_TIME ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR NMEA Blk 2 disable from Teseo" );
      }
      else
      {
         s32RetVal = OSAL_OK;
         GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "GNSSFW:NMEA Disable Success" );
      }
   }

   return s32RetVal;
}

/*****************************************************************************
* FUNCTION    : GnssProxyFw_s32SaveTesCfgPersist
*
* PARAMETER   : None
*
* RETURNVALUE  : OSAL_OK on success
*                OSAL_ERROR on failure
*
* DESCRIPTION : Send PSTMSAVEPAR and evaluate response to save
*               teseo configuration persistently
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 22.FEB.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
****************************************************************************/
static tS32 GnssProxyFw_s32SaveTesCfgPersist(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);
   /* Fill in PSTMSAVEPAR command */
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                      GNSS_PROXY_TESEO_SAVE_CONFIG_DATA_BLOCK,
                      OSAL_u32StringLength(GNSS_PROXY_TESEO_SAVE_CONFIG_DATA_BLOCK) );

   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += (tU16)
                  OSAL_u32StringLength(GNSS_PROXY_TESEO_SAVE_CONFIG_DATA_BLOCK);

   /* Send the INC message to V850 */
   if ( GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize ) != 
                                                                     (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GnssFW: write fail $PSTMSAVEPAR cmd" );
   }
   /* Wait for FLASH_DATA OK response from V850*/
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                   GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR PSTMSAVEPAR FLASH_DATA_OK Failed" );
   }
   /* Wait for PSTMSAVEPAR Response from Teseo */
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_COM_SAVE_CDB_RESPONSE_EVENT,
                                                   GNSS_PROXY_FW_TESEO_NMEA_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR PSTMSAVEPAROK wait Failed" );
   }
   else
   {
      s32RetVal = OSAL_OK;
   }

   return s32RetVal;
}


/*****************************************************************************
* FUNCTION    : GnssProxyFw_s32ReBootTesNormalMode
*
* PARAMETER   : None
*
* RETURNVALUE  : OSAL_OK on success
*                OSAL_ERROR on failure
*
* DESCRIPTION : Send $PSTMSAVEPAR and wait till teseo boots
*               i.e till teseo sends GPTXT Message
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 22.FEB.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
****************************************************************************/
static tS32 GnssProxyFw_s32ReBootTesNormalMode(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;

   GnssProxyFw_vCreateIncCtrlMsg(SRV_TYPE_FLASH_DATA);
   /* Fill in PSTMSAVEPAR command */
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[rGnssProxyFwUpdateInfo.u16FilledTxMsgSize],
                      GNSS_PROXY_TESEO_REBOOT,
                      OSAL_u32StringLength(GNSS_PROXY_TESEO_REBOOT) );

   rGnssProxyFwUpdateInfo.u16FilledTxMsgSize += (tU16)
                  OSAL_u32StringLength(GNSS_PROXY_TESEO_REBOOT);

   /* Send the INC message to V850 */
   if ( GnssProxy_s32SendDataToScc(rGnssProxyFwUpdateInfo.u16FilledTxMsgSize ) != 
                                                                     (tS32)rGnssProxyFwUpdateInfo.u16FilledTxMsgSize )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GnssFW: write fail $PSTMSAVEPAR cmd" );
   }
   /* Wait for FLASH_DATA OK response from V850*/
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                   GNSS_PROXY_FW_TESEO_FLASH_DATA_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR $PSTMSAVEPAR FLASH_DATA_OK Failed" );
   }
   /* Wait for GPTXT MSG from Teseo */
   else if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_TESEO_BOOT_MSG_GPTXT_WAIT_EVENT,
                                                   GNSS_PROXY_FW_TESEO_NMEA_WAIT_TIME ))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSSFW:ERROR $GPTXT wait Failed" );
   }
   else
   {
      s32RetVal = OSAL_OK;
   }

   return s32RetVal;

}


/*****************************************************************************
* FUNCTION    : GnssProxyFw_s32SendMsg
*
* PARAMETER   : tU32 u32EventToSend 
*
* RETURNVALUE : OSAL_OK on success
*               OSAL_ERROR on failure
*
* DESCRIPTION : Send the event u32EventToSend and wait for the response that
*               says event is consumed.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 29.FEB.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
****************************************************************************/
static tS32 GnssProxyFw_s32SendMsg( tU32 u32EventToSend )
{

   tS32 s32RetVal = OSAL_ERROR;
   tU32 u32ResultMask = 0;

   /* Post the event received */
   if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                       u32EventToSend,
                                       OSAL_EN_EVENTMASK_OR ))
   {
       GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Event post 0x %x failed err %lu",
                                              u32EventToSend,
                                              OSAL_u32ErrorCode() );
   }
   /* Wait till previously posted event to be consumed */
   else if (OSAL_ERROR == OSAL_s32EventWait( rGnssProxyFwUpdateInfo.hGnssFwInEve,
                                        GNSS_PROXY_FW_EVENT_MASK_ALL,
                                        OSAL_EN_EVENTMASK_OR,
                                        GNSS_PROXY_FW_EVENT_CONSUME_WAIT_TIME,
                                        &u32ResultMask))
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW INT Eve Wait Fail Eve %x err %lu",
                                           u32EventToSend, OSAL_u32ErrorCode());
   }
   /* Clear received event */
   else if (OSAL_ERROR == OSAL_s32EventPost( rGnssProxyFwUpdateInfo.hGnssFwInEve,
                                             ~(u32ResultMask),
                                             OSAL_EN_EVENTMASK_AND) )
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW cleanup:INT Event Post failed err %lu Event 0x %x",
                                            OSAL_u32ErrorCode(), u32ResultMask );
   }

   if ( u32EventToSend != (u32ResultMask & u32EventToSend) )
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "GNSS:FW Event Consumption problem: Exp 0x %x recvd 0x %x",
                                                                 u32EventToSend, u32ResultMask );
   }
   else
   {
      s32RetVal = OSAL_OK;
   }

   return s32RetVal;
}

/*****************************************************************************
* FUNCTION    : GnssProxyFw_vHandleGPTXT
*
* PARAMETER   : const tChar * const pcFieldIndex[]
*
* RETURNVALUE : None
*
* DESCRIPTION : Parse the GPTXT message.
*               To know if Teseo is Up and running.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 01.Mar.2016 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
****************************************************************************/
static tVoid  GnssProxyFw_vHandleGPTXT( const tChar * const pcFieldIndex[] )
{
   GNSS_PROXY_ARGUMENT_INTENTIONALLY_UNUSED(pcFieldIndex);
   (tVoid)GnssProxyFw_s32SendMsg(GNSS_PROXY_FW_TESEO_BOOT_MSG_GPTXT_WAIT_EVENT);
}
/* ---------------------END OF FILE------------------- */

