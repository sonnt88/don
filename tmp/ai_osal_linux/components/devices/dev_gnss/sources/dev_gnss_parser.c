/*********************************************************************************************************************
* FILE        : dev_gnss_parser.c
*
* DESCRIPTION : This file is a part of GNSS Proxy driver module.
*               This contain all NMEA message parsing and populating the OSAL_trGnssFullRecord stuff.
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 28.AUG.2013 | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -----------------------------------------------------------------------------------------------
* 12.JUN.2014 | Initial version: 1.1   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                        | Ticket: GMMY16-12922
*             |                        | Made changes for PSTMKFCOV message parsing.
* -----------------------------------------------------------------------------------------------
* 06.AUG.2014 |         version: 1.2   | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                        | Added GNSS receiver CRC check functionality
* -----------------------------------------------------------------------------------------------
* 09.FEB.2015 |         version: 1.3   | Madhu Kiran Ramachandra (RBEI/ECF5)
*                                      | Modified GNSS receiver CRC check functionality for single CRC
* -----------------------------------------------------------------------------------------------
*                                      |
*********************************************************************************************************************/

/*************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#include "dev_gnss_types.h"
#include "dev_gnss_scc_com.h"
#include "dev_gnss_trace.h"
#include "dev_gnss_parser.h"
#include "gnss_auxclock.h"
#include "EOLLib.h"
#include "dev_gnss_fw_update.h"
#include "dev_gnss_kds_hwinfo.h"

/*************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/
extern trGnssProxyInfo rGnssProxyInfo;
extern trGnssProxyData rGnssProxyData;
extern tU32 u32GnssEpochMinMaxWeek;
/* Contains the old and new calibration values of default satellite system.*/
static tU32 u32OldSatSysCalib = OSAL_C_U8_GNSS_SATSYS_UNKNOWN;
static tU32 u32NewSatSysCalib = OSAL_C_U8_GNSS_SATSYS_GPS;
/* File to place the last used sat sys. The file is read while
*   the satsys is configured and written each time the
*   sat sys is set*/
#define GNSS_LAST_SATSYS_USED_FILENAME OSAL_C_STRING_DEVICE_FFS3"/lastSatSysUsed.dat"
#define GNSS_DEVICE_REGISTRY OSAL_C_STRING_DEVICE_REGISTRY"/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/DEVICES/SENSOR/"
#define GNSS_SATSYS_DEFAULT_GPS 0

/*************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
static tVoid GnssProxy_vHandleGPGGA ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandleGSA ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandleGSV (  tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandleGPRMC ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandleGPVTG ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandlePSTMKFCOV ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandlePSTMSETPAR ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandlePSTMSETPAROK ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandlePSTMSETPARERROR ( tVoid );
static tVoid GnssProxy_vParseUtcTimeDate ( const tChar* psUtcTime, const tChar* psUtcDate );
static tVoid GnssProxy_vSetDataReady ( tVoid );
static tDouble GnssProxy_dConvToDeg( tDouble dRawdata );
static tVoid vPreProcessRecord (tVoid);
static tS32 GnssProxy_s32SetSatSysConfBlk200 (tU32 u32TeseoBitMask);
static tS32 GnssProxy_s32SetSatSysConfBlk227 (tU32 u32TeseoBitMask);
#ifdef GNSS_PROXY_ENABLE_RESET_GPS_ENGINE
static tS32 GnssProxy_s32ResetGpsEngine (tVoid);
#endif
static tVoid GnssProxy_vStoreSatSysUsed (tU32 u32SatSysConfig, tU32 u32BlkID );
static tU32 GnssProxy_u32FrameSatSysBlk200 (tU32 u32SatSysConfig);
static tU32 GnssProxy_u32FrameSatSysBlk227 (tU32 u32SatSysConfig);
static tVoid GnssProxy_vHandlePSTMVER ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandlePSTMPV ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandlePSTMPVQ ( tChar const * const pcFieldIndex[] );
static tVoid GnssProxy_vHandlePSTMCRCCHECK( tChar const * const pcFieldIndex[] );
static tU32 GnssProxy_u32AddSatSysOffset( tU32 u32RawSatId );
static tVoid GnssProxy_vHandlePSTMSAVEPAROK ( tChar const * const pcFieldIndex[] );
tVoid GnssProxy_vWriteLastUsedSatSys( tU32 u32actGnssSatSys );
static tU32 GnssProxy_u32ReadLastUsedSatSys( tVoid );
static tU32 GnssProxy_u32GetGnssDefaultSatSysEOL ( void );
static tU32 GnssProxy_u32GetGnssDefaultSatSysKDS ( void );
static tU32 GnssProxy_u32GetGnssDefaultSatSysDefine ( void );
tS32 GnssProxy_s32ApndSndCfgBlk(tU32 u32MsgLength);
tS32 GnssProxy_s32ValidateSatSys(tU32 u32SatSys);
tS32 GnssProxy_s32FrameCfgBlk200_227(tU32 u32SatSys, tPU32 pu32TeseoBitMaskBlk200, tPU32 pu32TeseoBitMaskBlk227);
tS32 GnssProxy_s32SetCfgBlk200_227(tU32 u32TeseoBitMaskBlk200,tU32 u32TeseoBitMaskBlk227);
tS32 GnssProxy_s32Set227CfgMask(tU32 u32TeseoBitMask,tU32 u32OldTeseoCfgBlk227 );

/*************************************************************************
* Function declaration (scope: Global)
*-----------------------------------------------------------------------*/
tS32  GnssProxy_s32SaveConfigDataBlock(tVoid);
tS32  GnssProxy_s32RebootTeseo(tVoid);

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_s32HandleStatusMsg
*
* PARAMETER    : NONE
*
* RETURNVALUE  : OSAL_OK  on Success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : Just updates the status and version of SCC in GNSS config structure
*
* HISTORY      :
*----------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|-------------------------------------
* 22.APR.2013  | Initial version: 1.0  | Niyatha S Rao (RBEI/ECF5)
* ---------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32HandleStatusMsg()
{
   tS32 s32RetVal = OSAL_ERROR;

   /* Is expected message size and received message size same ? */
   if ( MSG_SIZE_SCC_GNSS_R_COMPONENT_STATUS == rGnssProxyInfo.u32RecvdMsgSize )
   {
      rGnssProxyInfo.u8SccStatus = rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_STATUS];
      /* We should get only ACTIVE or INACTIVE */
      if( ((tU8)GNSS_PROXY_COMPONENT_STATUS_ACTIVE != rGnssProxyInfo.u8SccStatus ) &&
          ((tU8)GNSS_PROXY_COMPONENT_STATUS_NOT_ACTIVE != rGnssProxyInfo.u8SccStatus) )
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "!!!! Unknown status: %u of SCC ",
                                               (tU32) rGnssProxyInfo.u8SccStatus );
         rGnssProxyInfo.u8SccStatus = GNSS_PROXY_COMPONENT_STATUS_UNKNOWN;
      }
      else if ((tU8)GNSS_PROXY_COMPONENT_STATUS_ACTIVE == rGnssProxyInfo.u8SccStatus )
      {
         /* Don't know what to do IF SCC status is INACTIVE */
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_GNSS_STATUS_SCC, " %u", (tU32)rGnssProxyInfo.u8SccStatus );

         // TODO: How to handle Inactive SCC status ?

         /* Check if the software version matches ? */
         rGnssProxyInfo.u8VerInfo = (tU8)rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION];
         if ( GNSS_PROXY_COMPONENT_VERSION != rGnssProxyInfo.u8VerInfo )
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL , GNSSPXY_DEF_TRC_RULE, "!!!! VERSION MISMATCH, expected %d, but received %d from SCC ",
                                                  (tS32)GNSS_PROXY_COMPONENT_VERSION,
                                                  rGnssProxyInfo.u8VerInfo );
         }
         else
         {
            GnssProxy_vTraceOut( TR_LEVEL_USER_1 , GNSSPXY_GNSS_PXYVER_V850VER, " %d , %d",
                                                  GNSS_PROXY_COMPONENT_VERSION,
                                                  rGnssProxyInfo.u8VerInfo );
            s32RetVal = OSAL_OK;
         }
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "!!!V850 not active" );
      }
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "Size of status message should be %d but got %d",
                                            MSG_SIZE_SCC_GNSS_R_COMPONENT_STATUS,
                                            rGnssProxyInfo.u32RecvdMsgSize );
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_vHandleCommandReject
*
* PARAMETER    : NONE
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : Handling of the reject message from SCC is yet to be implemented here.
*                Presently we just trace it out.
*
* HISTORY      :
*----------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|-------------------------------------
* 22.APR.2013  | Initial version: 1.0  | Niyatha S Rao (RBEI/ECF5)
* ---------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vHandleCommandReject(tVoid)
{
   if( MSG_SIZE_SCC_GNSS_R_REJECT == rGnssProxyInfo.u32RecvdMsgSize )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Command 0x0%x Rejected by SCC. Reason : %x",
                           rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_REJECTED_MSG_ID],
                           rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_REJECT_REASON] );
      // TODO: Don't know how to handle a reject from SCC
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Improper size: %lu of the Reject Message from SCC",
                                             rGnssProxyInfo.u32RecvdMsgSize );
   }
}

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_vHandleGnssData
*
* PARAMETER    : NONE
*
* RETURNVALUE  : None
*
* DESCRIPTION  : Parses complete NMEA record received from SCC
*                1: Extracts Length and Time stamp from message
*                2: Parse the INC message for the NMEA message Header like GPPGGA, GPGSV etc
*                3: Index the NMEA record.
*                4: Then call the respective function to parse that NMEA record
* HISTORY      :
*----------------------------------------------------------------------------------
* Date         |       Version        | Author & comments
*--------------|----------------------|--------------------------------------------
* 22.APR.2013  | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* ---------------------------------------------------------------------------------
* 08.MAY.2013  | version: 1.1         | Madhu Kiran Ramachandra (RBEI/ECF5)
*              |                      | Updated comments and added support to parse
*              |                      | All needed NMEA messages. Also detection of
*              |                      | End of fix info is added.
* ---------------------------------------------------------------------------------
* 08.MAY.2013  | version: 2.0         | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*              |                      | Added support for dev_gnss. Moved the part to
*              |                      | detect the NMEA message type to another function.
* ---------------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vHandleGnssData(tVoid)
{
   tU32 u32LenInBytes;
   tULong uPduEndAdd;
   tPChar pcCharToParse;
   tPChar pcFieldIndex[GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE];
   tU32 u32Tokens = 0;
   tU8 u8Chksumcalc =0;
   tU8 u8ChksumFrmMsg = 0;
   tBool bChksumCalcDone = FALSE;
   tBool bIndexLimitReached = FALSE;

#ifdef  GEN3X86
   /* For LSIM use LSIM aux clock as reference */
   OSAL_trIOCtrlAuxClockTicks rIOCtrlAuxClockTicks;
   if ( sizeof(rIOCtrlAuxClockTicks) != 
        AUXCLOCK_s32IORead(&rIOCtrlAuxClockTicks, sizeof(rIOCtrlAuxClockTicks)))
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "LSIM:AUX read failed");
   }
   else
   {
      rGnssProxyData.rGnssTmpRecord.u32TimeStamp = rIOCtrlAuxClockTicks.u32Low;
   }
#endif

   OSAL_pvMemorySet(pcFieldIndex, 0, (sizeof(tPChar) * GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE) );

   /*Get the length of incoming NMEA data + time stamp in bytes.*/
   u32LenInBytes = rGnssProxyInfo.u32RecvdMsgSize - ( GNSS_PROXY_MSGID_SIZE + 
                                                      GNSS_PROXY_FIELD_SIZE_DATA_TIME_STAMP );

   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_INC_DATA_LEN, " %u", u32LenInBytes );
#ifndef  GEN3X86
   /*Get the time stamp from data record.*/
   OSAL_pvMemoryCopy( &rGnssProxyData.rGnssTmpRecord.u32TimeStamp,
                      &rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_TIMESTAMP_DATA_MSG],
                      GNSS_PROXY_FIELD_SIZE_DATA_TIME_STAMP );
#endif
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_NMEA_DATA_TS, " %lu",
                                         rGnssProxyData.rGnssTmpRecord.u32TimeStamp );

   pcCharToParse = (tPChar)&( rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_START_DATA] );

   if ( (*pcCharToParse != GNSS_PROXY_ASCII_VALUE_DOLLAR) || (u32LenInBytes == 0) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "Improper INC Data record ");
   }
   else
   {
      /* Find the End of NMEA data. pcCharToParse: holds start of NMEA data
         u32LenInBytes: length of data */
      uPduEndAdd = (tULong)pcCharToParse + u32LenInBytes - 1;
      /* Parse the complete record until we reach end address */
      while( (tULong)pcCharToParse < uPduEndAdd )
      {        
         switch ( (tU32)*pcCharToParse )
         {
            /* All NMEA and ST messages start with dollar*/
            case GNSS_PROXY_ASCII_VALUE_DOLLAR :
            {
               //GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_NMEA_STRT, NULL );
               pcFieldIndex[u32Tokens] = pcCharToParse;
               u32Tokens++;
               u8ChksumFrmMsg = 0;
               u8Chksumcalc = 0;
               /* Index current record. i.e. Store pointers to each field in NMEA message in pcFieldIndex */
               /* Stop indexing when we reach end of NMEA message i.e line feed */
               while( (*pcCharToParse != GNSS_PROXY_ASCII_VALUE_LINE_FEED) &&
                     ((tULong)pcCharToParse < uPduEndAdd) )
               {
                  /* Checksum calculation from message */
                  /* Here we extract original checksum from message */
                  if( *pcCharToParse == GNSS_PROXY_NMEA_CHECKSUM_DELIMITER_ASTERISK )
                  {
                     u8ChksumFrmMsg = (tU8)OSAL_u32StringToU32( (pcCharToParse+1),
                                                                     OSAL_NULL,
                                                                     16 );
                     bChksumCalcDone = TRUE;
                  }
                  /* Here we calculate checksum */
                  else if ( (*pcCharToParse !=  GNSS_PROXY_ASCII_VALUE_DOLLAR) &&
                            (bChksumCalcDone == FALSE) )
                  {
                     u8Chksumcalc ^= (tU8)*pcCharToParse;
                  }

                  /* "," is the delimiter in NMEA messages. so search for the "," and store the address 
                     of next field in pcFieldIndex. This is indexing*/
                  if((( *pcCharToParse == GNSS_PROXY_NMEA_FIELD_DELIMITER_COMMA ) ||
                        ( *pcCharToParse == GNSS_PROXY_NMEA_CHECKSUM_DELIMITER_ASTERISK ))&&
                        ( u32Tokens < GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE) )
                  {
                     pcFieldIndex[u32Tokens] = (pcCharToParse+1);
                     u32Tokens++;
                     *pcCharToParse = '\0';
                  }
                  /* This should never happen. All the NMEA and ST messages are of size less than
                     GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE. If a new NMEA or ST special message is introduced,
                     this has to be checked */
                  else if( u32Tokens >= GNSS_PROXY_MAX_FIELDS_IN_NMEA_MESSAGE )
                  {
                     GnssProxy_vTraceOut( TR_LEVEL_ERRORS , GNSSPXY_DEF_TRC_RULE,  "Increase FIELDS_IN_NMEA_MESSAGE");
                     u32Tokens=0;
                     bIndexLimitReached = TRUE;
                  }
                  /* parse next character */
                  pcCharToParse++;
               }

               //GnssProxy_vTraceOut( TR_LEVEL_USER_4 , GNSSPXY_CHKSUM_VAL, " 0x%x 0x%x",
               //                                     u8Chksumcalc, u8ChksumFrmMsg );
               /* Check if the calculated checksum and checksum from the message is same*/
               if ( bChksumCalcDone != TRUE )
               {
                  GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Checksum Identifier Missing");
                  rGnssProxyData.rGnssTmpFixInfo.bChecksumError = TRUE;
               }
               else if( u8Chksumcalc != u8ChksumFrmMsg )
               {
                   GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!!Checksum error");
                   rGnssProxyData.rGnssTmpFixInfo.bChecksumError = TRUE;
               }
               /* In this case, we have a corrupt record */
               else if ( bIndexLimitReached == TRUE )
               {
                  GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Index crossed beyond received data" );
               }
               else if ( OSAL_NULL == pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE] )
               {
                  GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Message index field is null" );
               }
               else
               {
                  //GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_TOKENS, " %lu", u32Tokens );
                  GnssProxy_vDispatchMsg ( (const tChar * const*)pcFieldIndex, u32Tokens );

               }
               /* Indexed message is parsed. Make u32Tokens=0 to index next message */
               u32Tokens = 0;
               bChksumCalcDone = FALSE;
               bIndexLimitReached = FALSE;
               break;
            }
            /* $PSTMCPU messages is used to identify the end of fix. Form feed is no longer used.
               This shall be removed after some time.*/
            case GNSS_PROXY_ASCII_VALUE_FORM_FEED :
            {
               /* Record complete */
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_FORM_FEED_IGNRD, NULL );
               break;
            }
            case GNSS_PROXY_ASCII_VALUE_CARRIAGE_RETURN :
            case GNSS_PROXY_ASCII_VALUE_LINE_FEED:
            {
               /*Ignore.*/
               break;
            }
            default:
            {
               /*Do nothing just trace out the character ASCII value*/
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Unknown Char: %lu", (tU32)*pcCharToParse );
               break;
            }
         }
         /* Parse Next character */
         pcCharToParse++;
      }
   }
}

/*********************************************************************************************************************
* FUNCTION     : GnssProxy_s32HandleConfigMsg
*
* PARAMETER    : NONE
*
* RETURNVALUE  : OSAL_OK  on success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : Configuration data will be received only once per connect.
*                This data is used to know the interval between GNSS data receptions and
*                the hardware chip used for providing GNSS solution
*                There are some more configuration messages that could be received from SCC but
*                currently they are not handled, but just traced out.
*
* HISTORY      :
*----------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|-------------------------------------
* 22.APR.2013  | Initial version: 1.0  | Niyatha S Rao (RBEI/ECF5)
* ---------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32HandleConfigMsg(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;
   tU8 u8GnssHwType;
   /* Switch on the service type received */
   switch ( rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_SERVICE_TYPE] )
   {
      /* Service type is Config request */
      case GNSS_PROXY_SERVICE_TYPE_CONFIG_RQ:
      {
         /* Extract GNSS data interval */
         OSAL_pvMemoryCopy( &rGnssProxyInfo.u16ReadWaitTimeMs,
                            &rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_DATA_INTERVAL],
                            GNSS_PROXY_FIELD_SIZE_CONFIG_DATA_INTERVAL );

         /* Extract GNSS HW type info */
         OSAL_pvMemoryCopy( &u8GnssHwType,
                            &rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_HW_TYPE],
                            GNSS_PROXY_FIELD_SIZE_CONFIG_HW_TYPE );
      
         if ( 0 != rGnssProxyInfo.u16ReadWaitTimeMs )
         {
            rGnssProxyInfo.rGnssConfigData.u8UpdateFrequency = (tU8) ((1.0/(tF32)rGnssProxyInfo.u16ReadWaitTimeMs)*1000);
         }
   
         GnssProxy_vTraceOut(TR_LEVEL_COMPONENT, GNSSPXY_GNSS_CFG, " %u , %u",
                                             u8GnssHwType,
                                             rGnssProxyInfo.u16ReadWaitTimeMs );
         /* Keep some buffer for the read timeout */
         rGnssProxyInfo.u16ReadWaitTimeMs *= GNSS_PROXY_READ_TIME_OUT_SCALE_FACTOR;

         s32RetVal = OSAL_OK;
         break;
      }
      case GNSS_FLUSH_SENSOR_DATA_REQ:
      {
         if ( GNSS_PROXY_FLUSH_BUFF_RES_OK == 
                         rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CFG_FLUSH_BUFF_RES] )
         {
            // post flush buff success event
            if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                GNSS_PROXY_FLUSH_SENSOR_BUFF_SUCCESS_EVENT,
                                                OSAL_EN_EVENTMASK_OR ))
            {
                GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event Post failed err %lu line %u",
                                                       OSAL_u32ErrorCode(), __LINE__ );
            }
         }
         // post flush buff failure event
         else if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                GNSS_PROXY_FLUSH_SENSOR_BUFF_FAILURE_EVENT,
                                                OSAL_EN_EVENTMASK_OR ))
         {
             GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event Post failed err %lu line %u",
                                                    OSAL_u32ErrorCode(), __LINE__ );
         }
         s32RetVal = OSAL_OK;
         break;
      }
      /* Known service types but not supported */
      case GNSS_PROXY_SERVICE_TYPE_HW_RESET:
      case GNSS_PROXY_SERVICE_TYPE_FLASH_BEGIN:
      case GNSS_PROXY_SERVICE_TYPE_FLASH_DATA:
      case GNSS_PROXY_SERVICE_TYPE_FLASH_END:
      case GNSS_PROXY_SERVICE_TYPE_DIAG_INFO_RQ:
      case GNSS_PROXY_SERVICE_TYPE_DIAG_RUN_SELF_TEST:
      case GNSS_PROXY_SERVICE_TYPE_UNKNOWN_ERROR:
      case GNSS_PROXY_SERVICE_TYPE_BUFFER_OVERFLOW_ERR:
      {
         GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!!Service type 0x %x not supported",
                             rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_SERVICE_TYPE] );
         break;
      }
      /* Unknown Service types */
      default:
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!!Service type %d Unknown",
                              rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_CONFIG_SERVICE_TYPE] );
         break;
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vDispatchMsg
*
* PARAMETER   :  *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*                 tU32 u32Tokens -> number of tokens in the current message
*
* RETURNVALUE : NONE
*
* DESCRIPTION : Dispatches the message based to respective function for parsing.
*
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 27.SEP.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vDispatchMsg ( tChar const * const pcFieldIndex[], tU32 u32Tokens )
{
   /* GPRMC: Time and Date information message */
   //lint -e530
   //lint -e506
   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                            "pcFieldIndex:GnssProxy_vDispatchMsg is null");
   }
   else if (OSAL_NULL == pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE] )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                            "Msg index: GnssProxy_vDispatchMsg is null");
   }
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                             GNSS_PROXY_MSG_TYPE_GPRMC ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_GPRMC == u32Tokens )
      {
         GnssProxy_vHandleGPRMC( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted GPRMC message");
      }
   }
   /* GPGGA: Fix information message */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_GPGGA ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_GPGGA == u32Tokens )
      {
         GnssProxy_vHandleGPGGA( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted GPGGA message");
      }
   }
   /* --GSA: DOPS and fix info */
   else if( (OSAL_ps8StringSubString( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                      GNSS_PROXY_MSG_TYPE_GSA ) != OSAL_NULL) )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_GPGSA == u32Tokens )
      {
         GnssProxy_vHandleGSA( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted --GSA message" );
      }
   }
   /* --GSV: Satellite tracking information */
   else if( (OSAL_ps8StringSubString( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                      GNSS_PROXY_MSG_TYPE_GSV )) != OSAL_NULL )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_GPGSV == u32Tokens )
      {
         GnssProxy_vHandleGSV( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted --GSV message" );
      }
   }
   /*receiver velocity and Position and velocity Covariance: information */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMPV ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMPV == u32Tokens )
      {
         GnssProxy_vHandlePSTMPV( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted PSTMPV message");
      }
   }
   /* Position and velocity processing noise matrix values */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMPVQ ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMPVQ == u32Tokens )
      {
         GnssProxy_vHandlePSTMPVQ( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted PSTMPVQ message");
      }
   }
   /* GPVTG: Velocity information */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_GPVTG ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_GPVTG == u32Tokens )
      {
         GnssProxy_vHandleGPVTG( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted GPGVTG");
      }
   }
   /*Position and velocity Covariance: information */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMKFCOV ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMTFCOV == u32Tokens )
      {
         GnssProxy_vHandlePSTMKFCOV( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted PSTMKFCOV message");
      }
   }
   /*config request response */
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMSETPAR ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMSETPAR == u32Tokens )
      {
         GnssProxy_vHandlePSTMSETPAR( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted PSTMSETPAR message");
      }
   }
   /*Config set response success*/
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMSETPAR_OK ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMSETPAR_OK == u32Tokens )
      {
         GnssProxy_vHandlePSTMSETPAROK( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted PSTMSETPAR_OK message");
      }
   }
   /*Config set response error*/
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMSETPAR_ERROR ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMSETPAR_ERROR == u32Tokens )
      {
         GnssProxy_vHandlePSTMSETPARERROR( );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted PSTMSETPAR_ERROR message");
      }
   }
   /*Teseo Bin_Image Version*/
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMVER ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMVER == u32Tokens )
      {
         GnssProxy_vHandlePSTMVER( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted PSTMVER message");
      }
   }
   /*Teseo CRC Evaluation*/
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMCRCCHECK ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMCRCCHECK == u32Tokens )
      {
         GnssProxy_vHandlePSTMCRCCHECK( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted PSTMCRCCHECK message");
      }
   }
   /*Confirmation of saving configuration data block*/
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMSAVEPAROK ) == 0 )
   {
      if ( GNSS_PROXY_FIELDS_COUNT_PSTMSAVEPAROK == u32Tokens )
      {
         GnssProxy_vHandlePSTMSAVEPAROK( (const tChar * const*) pcFieldIndex );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "corrupted PSTMSAVEPAROK message");
      }
   }
   /*End of fix info*/
   else if( OSAL_s32StringCompare( pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE],
                                   GNSS_PROXY_MSG_TYPE_PSTMCPU ) == 0 )
   {
      /* This message is only to know that we have reached the end of fix.
       * This will not be parsed */
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMCPU_FIX, NULL );
      GnssProxy_vSetDataReady();
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Message %s Not supported",
                                             pcFieldIndex[GNSS_PROXY_INDEX_TO_MSG_TYPE] );
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vParseUtcTimeDate
*
* PARAMETER   : psUtcTime : pointer to time in HHMMSS.mmm format
*               psUtcDate : pointer to date in DD_MM_YY format
*
* RETURNVALUE : NONE
*
* DESCRIPTION : Populates the UTC time and date from the NMEA message which is of the format HHMMSS.mmm
*               HH refers to hour, MM refers to minutes, SS refers to seconds and mmm refers to milliseconds,
*               Ex: 183417.366:  similarly DDMMYY.
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 22.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* -------------------------------------------------------------------------
* 8.MAR.2013  | Version 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | Added Date parsing Functionality
* --------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vParseUtcTimeDate (  tChar const * const psUtcTime,  tChar const * const psUtcDate )
{

   tChar cTmpDatTim[GNSS_PROXY_ARRAY_SIZE];
   OSAL_trGnssTimeUTC *prGnssTimeUTC = &(rGnssProxyData.rGnssTmpRecord.rPVTData.rTimeUTC);

   if ( (OSAL_NULL == psUtcTime) || ( psUtcDate == OSAL_NULL) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!! pointers to GnssProxy_vParseUtcTimeDate are null" );
   }
   else
   {
      /* Populate the Year information */
      cTmpDatTim[GNSS_PROXY_INDEX_UNIT_PLACE] = *( psUtcDate + GNSS_PROXY_OFFSET_YEAR_UNITS_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_TENTH_PLACE] = *( psUtcDate + GNSS_PROXY_OFFSET_YEAR_TENTH_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_HUNDREDTH_PLACE] = '\0';
      prGnssTimeUTC->u16Year = (tU16)OSAL_u32StringToU32( cTmpDatTim,
                                                          OSAL_NULL,
                                                          GNSS_PROXY_DATA_FORM_BASE_TEN );
      /* Year information in NMEA would give only last two digits. Add 2000 to this value to get complete year info */
      prGnssTimeUTC->u16Year += GNSS_PROXY_YEAR_BASE_VALUE;

      /* Populate the Year information */
      cTmpDatTim[GNSS_PROXY_INDEX_UNIT_PLACE] =  *( psUtcDate + GNSS_PROXY_OFFSET_MONTH_UNITS_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_TENTH_PLACE] = *( psUtcDate + GNSS_PROXY_OFFSET_MONTH_TENTH_PLACE );
      prGnssTimeUTC->u8Month = (tU8)(tU16)OSAL_u32StringToU32( cTmpDatTim,
                                                               OSAL_NULL,
                                                               GNSS_PROXY_DATA_FORM_BASE_TEN);
      /* Populate the Year information */
      cTmpDatTim[GNSS_PROXY_INDEX_UNIT_PLACE] =  *( psUtcDate + GNSS_PROXY_OFFSET_DATE_UNITS_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_TENTH_PLACE] = *( psUtcDate + GNSS_PROXY_OFFSET_DATE_TENTH_PLACE );
      prGnssTimeUTC->u8Day = (tU8)OSAL_u32StringToU32( cTmpDatTim,
                                                        OSAL_NULL,
                                                        GNSS_PROXY_DATA_FORM_BASE_TEN );
      /* Populate the Hour information */
      cTmpDatTim[GNSS_PROXY_INDEX_UNIT_PLACE] =  *( psUtcTime + GNSS_PROXY_OFFSET_HOUR_UNITS_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_TENTH_PLACE] = *( psUtcTime + GNSS_PROXY_OFFSET_HOUR_TENTH_PLACE );
      prGnssTimeUTC->u8Hour = (tU8)OSAL_u32StringToU32( cTmpDatTim,
                                                         OSAL_NULL,
                                                         GNSS_PROXY_DATA_FORM_BASE_TEN );
      /* Populate the Minute information */
      cTmpDatTim[GNSS_PROXY_INDEX_UNIT_PLACE] = *( psUtcTime + GNSS_PROXY_OFFSET_MINUTE_UNITS_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_TENTH_PLACE] = *( psUtcTime + GNSS_PROXY_OFFSET_MINUTE_TENTH_PLACE );
      prGnssTimeUTC->u8Minute = (tU8)OSAL_u32StringToU32( cTmpDatTim,
                                                           OSAL_NULL,
                                                           GNSS_PROXY_DATA_FORM_BASE_TEN );
      /* Populate the Second information */
      cTmpDatTim[GNSS_PROXY_INDEX_UNIT_PLACE] = *( psUtcTime + GNSS_PROXY_OFFSET_SECONDS_UNITS_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_TENTH_PLACE] = *( psUtcTime + GNSS_PROXY_OFFSET_SECONDS_TENTH_PLACE );
      prGnssTimeUTC->u8Second = (tU8)OSAL_u32StringToU32( cTmpDatTim,
                                                           OSAL_NULL,
                                                           GNSS_PROXY_DATA_FORM_BASE_TEN );
      /* Populate the Millisecond information */
      cTmpDatTim[GNSS_PROXY_INDEX_UNIT_PLACE] = *( psUtcTime + GNSS_PROXY_OFFSET_MILLISECONDS_UNITS_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_TENTH_PLACE] = *( psUtcTime + GNSS_PROXY_OFFSET_MILLISECONDS_TENTH_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_HUNDREDTH_PLACE] = *( psUtcTime + GNSS_PROXY_OFFSET_MILLISECONDS_HUNDRENDTH_PLACE );
      cTmpDatTim[GNSS_PROXY_INDEX_THOUSANDTH_PLACE] = '\0';
      prGnssTimeUTC->u16Millisecond = (tU16)OSAL_u32StringToU32( cTmpDatTim,
                                                                 OSAL_NULL,
                                                                 GNSS_PROXY_DATA_FORM_BASE_TEN );

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_GNSS_UTC_DATE, " %d , %d , %d",
                                            prGnssTimeUTC->u16Year,
                                            prGnssTimeUTC->u8Month,
                                            prGnssTimeUTC->u8Day );

     GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_GNSS_UTC_TIME, " %d , %d , %d , %d",
                                            prGnssTimeUTC->u8Hour,
                                            prGnssTimeUTC->u8Minute,
                                            prGnssTimeUTC->u8Second,
                                            prGnssTimeUTC->u16Millisecond );
   }
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vSetDataReady
*
* PARAMETER   : None
*
* RETURNVALUE : None
*
* DESCRIPTION : Once end of fix information is identified, it identifies all
*               received and valid fields.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 8.MAR.2013  | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vSetDataReady( tVoid )
{

   /* Data from different NMEA messages has to be put in proper locations. */
   vPreProcessRecord();

   /* Semaphore is needed to protect  rGnssProxyData.u32RecordId.
      As this is accessed from two thread contexts */
   GnssProxy_vEnterCriticalSection(__LINE__);

   /* This will be checked in read function */
   rGnssProxyData.bIsRecordValid = TRUE;
   /* This is used Internal Diagnostics */
   rGnssProxyData.u32NumRecordsParsed++;
   if( rGnssProxyData.u32NumRecordsParsed == GNSS_PROXY_INVALID_RECORD_ID )
   {
      rGnssProxyData.u32NumRecordsParsed++;
   }
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_RECORDID, " %lu",
                                         rGnssProxyData.u32NumRecordsParsed);

   OSAL_pvMemoryCopy( &rGnssProxyData.rGnssDataRecord,
                      &rGnssProxyData.rGnssTmpRecord,
                      sizeof(OSAL_trGnssFullRecord) );
   /* Clear the tmp record and make it ready for storing next fix information */
   OSAL_pvMemorySet( &rGnssProxyData.rGnssTmpRecord, 0, sizeof(OSAL_trGnssFullRecord) );
   /* This is used to test if the Teseo configuration */
   rGnssProxyData.u32NmeaMsgsRecvd = rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs;
   /* If checksum error has to be informed before validating the test data */
   if ( TRUE == rGnssProxyData.rGnssTmpFixInfo.bChecksumError )
   {
      rGnssProxyData.u32NmeaMsgsRecvd |= GNSS_PROXY_CHECKSUM_ERROR;
   }
#ifndef  GEN3X86
   /* Sync V850 and Linux System times.
      This has to be call from critical section */
   GnssProxy_vSyncAuxClock( rGnssProxyData.rGnssDataRecord.u32TimeStamp );
#endif
   GnssProxy_vLeaveCriticalSection(__LINE__);
   /* Application might be blocked waiting for data. post data ready event */
   if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyReadEvent,
                                       GNSS_PROXY_EVENT_DATA_READY,
                                       OSAL_EN_EVENTMASK_OR ))
   {
       GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event Post failed err %lu",
                                              OSAL_u32ErrorCode() );
   }
   /* Reset these fields */
   /* This contains all the info specific for a particular fix */
   OSAL_pvMemorySet(&(rGnssProxyData.rGnssTmpFixInfo),0,sizeof(trGnssTmpFixInfo));
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_HandleGPGGA
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : Parses GPGGA Message and populates OSAL_trGPSMeasuredPosition.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 22.APR.2013 | Initial version: 1.0 | Niyatha S Rao (RBEI/ECF5)
* -------------------------------------------------------------------------
* 8.MAR.2013  | Initial version: 1.1 | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | Made parsing of GPGGA Complete.
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandleGPGGA ( tChar const * const pcFieldIndex[] )
{
   tF32 f32AltMSL;
   OSAL_trGnssPVTData *prPVTData = &(rGnssProxyData.rGnssTmpRecord.rPVTData);
   trGnssTmpFixInfo *prGnssTmpFixInfo = &(rGnssProxyData.rGnssTmpFixInfo); 

   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_RCVD_GPGGA, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!! pointers to GnssProxy_vHandleGPGGA are null" );
   }
   else
   {
      /* Get Latitude */
      prPVTData->f64Latitude =
                OSAL_dStringToDouble( pcFieldIndex[GNSS_PROXY_GPGGA_OFFSET_LATITUDE], OSAL_NULL );

      /* Convert Deg-Min-Sec to Decimal Degree*/
      prPVTData->f64Latitude = GnssProxy_dConvToDeg( prPVTData->f64Latitude );

      /* With Equator as reference, Latitude towards North is positive and south is negative */                       
      if( *pcFieldIndex[GNSS_PROXY_GPGGA_OFFSET_LATITUDE_DIRECTION] == GNSS_PROXY_GPGGA_LAT_DIR_SOUTH )
      {
        prPVTData->f64Latitude *= -1;
      }

      /* Get Longitude */
      prPVTData->f64Longitude =
                 OSAL_dStringToDouble( pcFieldIndex[GNSS_PROXY_GPGGA_OFFSET_LONGITUDE], OSAL_NULL );

      /* Convert Deg-Min-Sec to Decimal Degree*/
      prPVTData->f64Longitude = GnssProxy_dConvToDeg( prPVTData->f64Longitude );

      /* With Equator as reference, Longitude towards East is positive and West is negative */
      if( *pcFieldIndex[GNSS_PROXY_GPGGA_OFFSET_LONGITUDE_DIRECTION] == GNSS_PROXY_GPGGA_LON_DIR_WEST )
      {
         prPVTData->f64Longitude *= -1;
      }

      /* Get MSL Altitude */
      f32AltMSL = (tF32)OSAL_dStringToDouble( pcFieldIndex[GNSS_PROXY_GPGGA_OFFSET_MSL_ALTUTUDE], OSAL_NULL );

      /* Get Geoidal separation. This is needed to calculate dAltMSL : Mean sea level altitude */
      prPVTData->f32GeoidalSeparation =
                 (tF32)OSAL_dStringToDouble( pcFieldIndex[GNSS_PROXY_GPGGA_OFFSET_GEOSEP], OSAL_NULL );

      /* WGS84Attitude = MSL Altitude + Geoidal separation */
      prPVTData->f32AltitudeWGS84 = f32AltMSL +  prPVTData->f32GeoidalSeparation;


      /* Is Differential GPS used in fix ? */
      if( *pcFieldIndex[GNSS_PROXY_GPGGA_OFFSET_DGPS_SOL] == GNSS_PROXY_GPGGA_SOL_TYPE_DGPS )
      {
         prGnssTmpFixInfo->bDGpsUsed = TRUE;
      }
      else
      {
          prGnssTmpFixInfo->bDGpsUsed = FALSE;
      }

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_GPGGADATA_1, " %011.6f , %011.6f , %011.3f , %012.6f , %d", 
                                            prPVTData->f64Latitude,
                                            prPVTData->f64Longitude,
                                            prPVTData->f32AltitudeWGS84,
                                            prPVTData->f32GeoidalSeparation,
                                            prGnssTmpFixInfo->bDGpsUsed );

      /* Make a note that GPGGA is received for current fix */
      rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs |= GNSS_PROXY_GPGGA_ID;
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandleGSA
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE : None
*
* DESCRIPTION : Parses [(GP)|(GN)|(GL)]GSA Message and populates OSAL_trGPSMeasuredPosition.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 8.MAR.2013  | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandleGSA (  tChar const * const pcFieldIndex[] )
{
   tU16 u16ChannelCnt = 0;
   tBool bEndofList = FALSE;
   OSAL_trGnssPVTData *prPVTData = &(rGnssProxyData.rGnssTmpRecord.rPVTData);
   trGnssTmpFixInfo *prGnssTmpFixInfo = &(rGnssProxyData.rGnssTmpFixInfo);

   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_RCVD_GSA, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!! pointers to GnssProxy_vHandleGPGSA are null" );
   }
   else
   {

      /* Type of fix  - No fix */
      if ( *pcFieldIndex[GNSS_PROXY_GPGSA_OFFSET_FIX_TYPE] == GNSS_PROXY_GPGSA_FIX_TYPE_NO_FIX )
      {
        prPVTData->rFixStatus.enMode = GNSS_FIX_TYPE_NOFIX;
      }
      /* Type of fix  - 2D fix */
      else if( *pcFieldIndex[GNSS_PROXY_GPGSA_OFFSET_FIX_TYPE] == GNSS_PROXY_GPGSA_FIX_TYPE_2D )
      {
         prPVTData->rFixStatus.enMode = GNSS_FIX_TYPE_2D;
      }
      /* Type of fix  - 3D fix */
      else if( *pcFieldIndex[GNSS_PROXY_GPGSA_OFFSET_FIX_TYPE] == GNSS_PROXY_GPGSA_FIX_TYPE_3D )
      {
         prPVTData->rFixStatus.enMode = GNSS_FIX_TYPE_3D;
      }
      /* Fix type UnExpected value */
      else
      {
        prPVTData->rFixStatus.enMode = GNSS_FIX_TYPE_UNKNOWN;
      }
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_GSA_FIX_STATUS, " %d",
                                             prPVTData->rFixStatus.enMode );

      /* Get the list of satellites used in FIX */
      for( u16ChannelCnt =0;
           (u16ChannelCnt < GNSS_PROXY_GPGSA_SATS_PER_MSG) && ( TRUE != bEndofList ); 
           u16ChannelCnt++ )
      {
        if( prPVTData->u16SatsUsed < OSAL_C_U8_GNSS_NO_CHANNELS )
        {
            /* Get Satellite ID or SatPRN . This will mapped to final record when complete fix info is received. */
            prGnssTmpFixInfo->u16SatUsedFix[prPVTData->u16SatsUsed] = (tU16)OSAL_u32StringToU32(
                              pcFieldIndex[u16ChannelCnt + GNSS_PROXY_GPGSA_OFFSET_FIRST_SAT_ID],
                              OSAL_NULL,
                              GNSS_PROXY_DATA_FORM_BASE_TEN );
        }
        else
        {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "Channel Limit crossed %d",
                                                  prPVTData->u16SatsUsed );
        }
      
         /* If Sat ID or SatPRN is not zero then satellite is used in fix */
         if( 0 != prGnssTmpFixInfo->u16SatUsedFix[prPVTData->u16SatsUsed] )
         {
            prGnssTmpFixInfo->u16SatUsedFix[prPVTData->u16SatsUsed] = 
                (tU16)GnssProxy_u32AddSatSysOffset(prGnssTmpFixInfo->u16SatUsedFix[prPVTData->u16SatsUsed]);

            GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_SAT_NUM_PRN, " %02d , %02d",
                                 ( prPVTData->u16SatsUsed+1 ),
                                 prGnssTmpFixInfo->u16SatUsedFix[prPVTData->u16SatsUsed] );
            prPVTData->u16SatsUsed++;
         }
         else
         {
            bEndofList = TRUE;
         }
      }

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_GSA_TOT_SAT_USED," %lu",  prPVTData->u16SatsUsed );

      /* Extract all DOP values */
      prPVTData->f32PDOP = (tF32)OSAL_dStringToDouble( pcFieldIndex[GNSS_PROXY_GPGSA_OFFSET_PDOP], OSAL_NULL );
      prPVTData->f32HDOP = (tF32)OSAL_dStringToDouble( pcFieldIndex[GNSS_PROXY_GPGSA_OFFSET_HDOP], OSAL_NULL );
      prPVTData->f32VDOP = (tF32)OSAL_dStringToDouble( pcFieldIndex[GNSS_PROXY_GPGSA_OFFSET_VDOP], OSAL_NULL );


      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_GSA_DIL_PREC_VAL, " %05.2f , %05.2f , %05.2f",
                                             prPVTData->f32PDOP,
                                             prPVTData->f32HDOP,
                                             prPVTData->f32VDOP );

      /* Make a note that GPGSA is received for current fix */
      rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs |= GNSS_PROXY_GPGSA_ID;
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandleGSV
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE : None
*
* DESCRIPTION : Parses [(GP)|(GN)|(GL)]GSV Message and populates OSAL_trGPSTrackData and OSAL_trGPSVisibleList.
*
* HISTORY     :
*----------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|---------------------------------------------
* 8.MAR.2013  | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* ---------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandleGSV (  tChar const * const pcFieldIndex[] )
{

   tU32 u32CurrGSVMsgNum = 0,u32GsvSatCnt = 0;
   tBool bEndofList = FALSE;
   OSAL_trGnssChannelStatus *prGnssChanSts = (rGnssProxyData.rGnssTmpRecord.rChannelStatus);
   OSAL_trGnssPVTData *prPVTData = &(rGnssProxyData.rGnssTmpRecord.rPVTData);
   trGnssTmpFixInfo *prGnssTmpFixInfo = &(rGnssProxyData.rGnssTmpFixInfo); 

   GnssProxy_vTraceOut( TR_LEVEL_USER_4 , GNSSPXY_RCVD_GSV, NULL);

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!! pointers to GnssProxy_vHandleGPGSV are null" );
   }
   else
   {

      /* Get Current GPGSV message number */
      u32CurrGSVMsgNum = OSAL_u32StringToU32( pcFieldIndex[GNSS_PROXY_GPGSV_OFFSET_CURR_MSG_NUM],
                                              OSAL_NULL,
                                              GNSS_PROXY_DATA_FORM_BASE_TEN );

      /* Get Number of GSV messages for the current fix and number of satellites in view
         It is sufficient if this is done for first message.*/
      if( 1 == u32CurrGSVMsgNum )
      {
         /* We are in this if block because this is the first GPGSV message for the current fix */
         prGnssTmpFixInfo->u32ExpecGSVMsgs = OSAL_u32StringToU32( pcFieldIndex[GNSS_PROXY_GPGSV_OFFSET_EXPECTED_MSGS],
                                                               OSAL_NULL,
                                                               GNSS_PROXY_DATA_FORM_BASE_TEN );
         prPVTData->u16SatsVisible += (tU16)OSAL_u32StringToU32( pcFieldIndex[GNSS_PROXY_GPGSV_OFFSET_NUM_GPS_SAT],
                                                               OSAL_NULL,
                                                               GNSS_PROXY_DATA_FORM_BASE_TEN);
      }

      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_GSV_MSG_DATA, " %d , %d , %02d",
                                            prGnssTmpFixInfo->u32ExpecGSVMsgs,
                                            u32CurrGSVMsgNum,
                                            prPVTData->u16SatsVisible );
      
      /* Get Elevation, Azimuthal and Carrier to noise ration for satellites in View */
      while( (u32GsvSatCnt < GNSS_PROXY_NUM_SATS_IN_ONE_GPGSV) &&
             (prGnssTmpFixInfo->u32GsvChanCnt < OSAL_C_U8_GNSS_NO_CHANNELS) &&
             (FALSE == bEndofList))
      {
         /* rGnssProxyInfo.u32GsvChanCnt is made global because, this value has to be retained
            for parsing all GPGSV message in a fix. */
          prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].u16SvID = 
                        (tU16)OSAL_u32StringToU32( pcFieldIndex[ GNSS_PROXY_GPGSV_OFFSET_BASE_SAT_PRN_NUM +
                                                  (u32GsvSatCnt)* GNSS_PROXY_GPGSV_MUL_FACTOR_TO_NEXT_ENTRY ],
                                                  OSAL_NULL,
                                                  GNSS_PROXY_DATA_FORM_BASE_TEN );

         prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].f32Elevation =
                        (tF32)OSAL_u32StringToU32( pcFieldIndex[ GNSS_PROXY_GPGSV_OFFSET_BASE_SAT_ELEVATION +
                                                   (u32GsvSatCnt)* GNSS_PROXY_GPGSV_MUL_FACTOR_TO_NEXT_ENTRY ],
                                                   OSAL_NULL,
                                                   GNSS_PROXY_DATA_FORM_BASE_TEN );

         prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].f32Azimuthal =
                        (tF32)OSAL_u32StringToU32( pcFieldIndex[ GNSS_PROXY_GPGSV_OFFSET_BASE_SAT_AZIMUTHAL +
                                                   (u32GsvSatCnt)* GNSS_PROXY_GPGSV_MUL_FACTOR_TO_NEXT_ENTRY ],
                                                   OSAL_NULL,
                                                   GNSS_PROXY_DATA_FORM_BASE_TEN );

         prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].u8CarrierToNoiseRatio =
                         (tU8)OSAL_u32StringToU32( pcFieldIndex[ GNSS_PROXY_GPGSV_OFFSET_BASE_SAT_CARR_NOISE_RATIO +
                                                   (u32GsvSatCnt) * GNSS_PROXY_GPGSV_MUL_FACTOR_TO_NEXT_ENTRY ],
                                                   OSAL_NULL,
                                                   GNSS_PROXY_DATA_FORM_BASE_TEN );

         if ( 0 != prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].u16SvID )
         {
            prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].u16SvID =
               (tU16)GnssProxy_u32AddSatSysOffset(prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].u16SvID);

                     /* Update satellites used information */
            if ( GNSS_PROXY_MIN_CN0 <= prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].u8CarrierToNoiseRatio )
            {
               prPVTData->u16Received++;
            }

            GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_GSV_SAT_INFO_1, " %02lu , %02d , %08.3f , %08.3f , %02d",
                                                 (prGnssTmpFixInfo->u32GsvChanCnt+1),
                                                 prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].u16SvID,
                                                 prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].f32Elevation,
                                                 prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].f32Azimuthal,
                                                 prGnssChanSts[prGnssTmpFixInfo->u32GsvChanCnt].u8CarrierToNoiseRatio );
            prGnssTmpFixInfo->u32GsvChanCnt++;
            u32GsvSatCnt++;
         }
         else
         {
            bEndofList = TRUE;
         }

      }

      /* This has to be done For the last GSV message in every fix */
      if( prGnssTmpFixInfo->u32ExpecGSVMsgs == u32CurrGSVMsgNum )
      {
          /* Make a note that all GPGSV messages are received for current fix */
         rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs |= GNSS_PROXY_GPGSV_ID;
         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Sats received: %u", prPVTData->u16Received);
      }
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandleGPRMC
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*               u32TimeStamp -> Time stamp of message
*
* RETURNVALUE : None
*
* DESCRIPTION : Parses GPRMC Message and populates OSAL_trGPSTime in OSAL_trGPSMeasuredPosition.
*
* HISTORY     :
*---------------------------------------------------------------------------------------------------------
* Date        |  Version             | Author & comments
*-------------|----------------------|--------------------------------------------------------------------
* 8.MAR.2013  | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
*-------------|----------------------|--------------------------------------------------------------------
* 1.July.2013 |  version: 1.1        | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | Added Check for Time validity field in GPRMC Message
* --------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandleGPRMC(  tChar const * const pcFieldIndex[] )
{

   GnssProxy_vTraceOut( TR_LEVEL_USER_4 , GNSSPXY_RCVD_GPRMC, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!! pointers to GnssProxy_vHandleGPRMC are null" );
   }
   else
   {
      /* Populate Time stamp received from SCC into GNSS record */
      /* Check if time from $GPRMC is valid */
      if ( GNSS_PROXY_GPRMC_TIME_VALID ==
          *pcFieldIndex[GNSS_PROXY_GPRMC_OFFSET_UTC_TIME_VALIDIY] )
      {
            /* This function will parse GPRMC for date and time related information */
            GnssProxy_vParseUtcTimeDate( pcFieldIndex[GNSS_PROXY_GPRMC_OFFSET_UTC_TIME],
                                         pcFieldIndex[GNSS_PROXY_GPRMC_OFFSET_UTC_DATE] );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "NO time FIX !!!" );
      }
      /* Make a note that GPRMC is received for current fix */
      rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs |= GNSS_PROXY_GPRMC_ID;
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandleGPVTG
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE : None
*
* DESCRIPTION : Parses GPRMC Message and populates OSAL_trGPSVelocity in OSAL_trGPSMeasuredPosition.
*
* HISTORY     :
*------------------------------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-----------------------------------------------------------------
* 8.MAR.2013  | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
*------------------------------------------------------------------------------------------------------
* 1.July.2013 | Version 1.1          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                      | Added velocity units conversion from Km/h to m/sec
*------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandleGPVTG(  tChar const * const pcFieldIndex[] )
{

   tDouble dTrueHeading = 0;
   tDouble dHorizonSpeed = 0;
   OSAL_trGnssPVTData *prPVTData = &(rGnssProxyData.rGnssTmpRecord.rPVTData);
   
   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_RCVD_GPVTG, NULL);

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!! pointers to GnssProxy_vHandleGPVTG are null" );
   }
   else
   {

      /* Get heading angle towards true north of the earth */
      dTrueHeading = OSAL_dStringToDouble( pcFieldIndex[GNSS_PROXY_GPVTG_OFFSET_TRUE_HEADING_ANGLE],
                                           OSAL_NULL );
      /* Get the horizontal speed */
      dHorizonSpeed = OSAL_dStringToDouble( pcFieldIndex[GNSS_PROXY_GPVTG_OFFSET_HORIZONTAL_SPEED],
                                            OSAL_NULL );

      /* Computation of velocity

                                          True North
                                            |           *
                                        dvn ^         * (Direction of vehicle motion)
                                            | /_H   *
                                            |.... *
                                            |   *
                                            | *            dve
     west ----------------------------------*-------------->------------------ East
                                            |
                                            |
                                            |
                                            |
                                            |
                                            |
                                            |
                                          South

      /_H -> True Heading Angle
      Velocity along north: dvn = Horizontal speed * cosine (/_H)
      Velocity along east : dvn = Horizontal speed * sine   (/_H)

      Velocity is a vector. It has both direction and magnitude. In Geometric terms
      dvn is positive if vehicle is moving along north else negative.
      dve is positive if vehicle is moving along east  else negative.

      */ 
      prPVTData->f32VelocityNorth =
                    (tF32)(OSAL_dCos(GNSS_PROXY_DEG_TO_RAD(dTrueHeading)) * dHorizonSpeed);

      prPVTData->f32VelocityEast =
                     (tF32)(OSAL_dSin(GNSS_PROXY_DEG_TO_RAD(dTrueHeading)) * dHorizonSpeed);

      /* Convert velocity from km/h to m/s */
      prPVTData->f32VelocityNorth *= GNSS_PROXY_KM_PER_HOUR_TO_METER_PER_SEC;
      prPVTData->f32VelocityEast  *= GNSS_PROXY_KM_PER_HOUR_TO_METER_PER_SEC;

      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_GPVTG_MSG_DATA, " %f , %f , %f , %f",
                                             dTrueHeading,
                                             dHorizonSpeed,
                                             prPVTData->f32VelocityNorth,
                                             prPVTData->f32VelocityEast );

      /* Make a note that GPVTG is received for current fix */
      rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs |= GNSS_PROXY_GPVTG_ID;
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMKFCOV
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : Parses PSTMKFCOV Message and populates OSAL_trGPSMeasuredPosition.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 18.Jun.2013 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
* 12.JUN.2014 | Initial version: 1.1 | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                      | No Need to square co-variance from
*             |                      | this message.
* ------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandlePSTMKFCOV ( tChar const * const pcFieldIndex[] )
{
   OSAL_trGnssPVTData *prPVTData = &(rGnssProxyData.rGnssTmpRecord.rPVTData);

   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_RCVD_PSTMKFCOV, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!! pointers to GnssProxy_vHandlePSTMKFCOV are null" );
   }
   else
   {
      /* Update the elements of position covariance matrix */
      /* Variance of position along north direction */
      prPVTData->rPositionCovarianceMatrix.f32Elem0 = (tF32)OSAL_dStringToDouble(
                                pcFieldIndex[GNSS_PROXY_PSTMKFCOV_OFFSET_POS_COV_NORTH],
                                OSAL_NULL );

      /* Variance of position along east direction */
      prPVTData->rPositionCovarianceMatrix.f32Elem5 = (tF32)OSAL_dStringToDouble(
                                pcFieldIndex[GNSS_PROXY_PSTMKFCOV_OFFSET_POS_COV_EAST],
                                OSAL_NULL );

      /* Variance of position along vertical direction */
      prPVTData->rPositionCovarianceMatrix.f32Elem10 = (tF32)OSAL_dStringToDouble(
                                pcFieldIndex[GNSS_PROXY_PSTMKFCOV_OFFSET_POS_COV_VERTICAL],
                                OSAL_NULL );

      /* Variance of velocity along north direction */
      prPVTData->rVelocityCovarianceMatrix.f32Elem0 = (tF32)OSAL_dStringToDouble(
                                pcFieldIndex[GNSS_PROXY_PSTMKFCOV_OFFSET_VEL_COV_NORTH],
                                OSAL_NULL );

      /* Variance of velocity along east direction */
      prPVTData->rVelocityCovarianceMatrix.f32Elem5 = (tF32)OSAL_dStringToDouble(
                                pcFieldIndex[GNSS_PROXY_PSTMKFCOV_OFFSET_VEL_COV_EAST],
                                OSAL_NULL );

      /* Variance of velocity along vertical direction */
      prPVTData->rVelocityCovarianceMatrix.f32Elem10 = (tF32)OSAL_dStringToDouble(
                                pcFieldIndex[GNSS_PROXY_PSTMKFCOV_OFFSET_VEL_COV_VERTICAL],
                                OSAL_NULL );

      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_PSTMKFCOV_MSG_DATA_POS, " %f , %f , %f",
                                             prPVTData->rPositionCovarianceMatrix.f32Elem0,
                                             prPVTData->rPositionCovarianceMatrix.f32Elem5,
                                             prPVTData->rPositionCovarianceMatrix.f32Elem10 );
	  
	  GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_PSTMKFCOV_MSG_DATA_VEL, " %f , %f , %f",
                                             prPVTData->rVelocityCovarianceMatrix.f32Elem0,
                                             prPVTData->rVelocityCovarianceMatrix.f32Elem5,
                                             prPVTData->rVelocityCovarianceMatrix.f32Elem10 );

      /* Make a note that PSTMKFCOV is received for current fix */
      rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs |= GNSS_PROXY_PSTMKFCOV_ID;
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMSETPAR
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : Parses $PSTMSETPAR to extract configuration requested
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 18.Aug.2013 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandlePSTMSETPAR ( tChar const * const pcFieldIndex[] )
{
   tU32 u32BlkID =0, u32CfgBlk =0;
   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_RCVD_PSTMSETPAR, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "%s:!!! pointers to GnssProxy_vHandlePSTMSETPAR are null", __FUNCTION__ );
   }
   else
   {
      /* Extract configuration block ID */
      u32BlkID = OSAL_u32StringToU32( pcFieldIndex[ GNSS_PROXY_PSTMSETPAR_OFFSET_CONF_BLK_ID ],
                                                       OSAL_NULL,
                                                       GNSS_PROXY_DATA_FORM_BASE_SIXTEEN );

      /* Extract configuration block */
      u32CfgBlk = OSAL_u32StringToU32( pcFieldIndex[ GNSS_PROXY_PSTMSETPAR_OFFSET_CONF_DATA ],
                                                       OSAL_NULL,
                                                       GNSS_PROXY_DATA_FORM_BASE_SIXTEEN );

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMSETPAR_BLKID_CFG,
                           " %x , %x", u32BlkID, u32CfgBlk);

      switch ( u32BlkID )
      {
         case GNSS_PROXY_APP_LIST_BLOCK_ID_200:
         {
            rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk200 = u32CfgBlk;
            GnssProxy_vStoreSatSysUsed(u32CfgBlk, u32BlkID);

            /* Application might be blocked waiting for config. */
            if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                GNSS_PROXY_SAT_SYS_CONF_BLK200_REQ_WAIT_EVENT,
                                                OSAL_EN_EVENTMASK_OR ))
            {
                GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                                     "Event Post failed err %lu line %d",
                                     OSAL_u32ErrorCode(), __LINE__ );
            }
            break;
         }

         case GNSS_PROXY_APP_LIST_BLOCK_ID_227:
         {
            rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk227 = u32CfgBlk;
            GnssProxy_vStoreSatSysUsed(u32CfgBlk, u32BlkID);

            /* Application might be blocked waiting for config. */
            if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                GNSS_PROXY_SAT_SYS_CONF_BLK227_REQ_WAIT_EVENT,
                                                OSAL_EN_EVENTMASK_OR ))
            {
                GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                                     "Event Post failed err %lu line %d",
                                     OSAL_u32ErrorCode(), __LINE__ );
            }
            break;
         }

         case GNSS_PROXY_GNSS_MIN_MAX_WEEK_BLOCK_ID:
         {
            u32GnssEpochMinMaxWeek = u32CfgBlk;
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMSETPAR_EPOCH_DATA,
                                 " 0x%x",
                                 u32GnssEpochMinMaxWeek );
            /* Application might be blocked waiting for config. */
            if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                 GNSS_PROXY_TESEO_COM_GET_EPOCH_RESPONSE_MASK,
                                                 OSAL_EN_EVENTMASK_OR ) )
            {
                GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                                     "vHandlePSTMSETPAR:Event Post failed err %lu",
                                     OSAL_u32ErrorCode() );
            }
            break;
         }

         default:
         {
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                                 "Switch Default:line %d file %s",
                                 __LINE__, __FILE__ );
            break;
         }
      }
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMSETPAROK
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : This is positive response for $PSTMSETPAR.
*               A event will be posted for the application thread waiting
*               for this response.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 18.Aug.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandlePSTMSETPAROK ( tChar const * const pcFieldIndex[] )
{
   tU32 u32CfgBlk = 0;

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_RCVD_PSTMSETPAROK, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "!!! pointers to GnssProxy_vHandlePSTMSETPAROK are null" );
   }
   else
   {
      // Get the config block for which the response is received 
      u32CfgBlk = OSAL_u32StringToU32( pcFieldIndex[ GNSS_PROXY_PSTMSETPAROK_OFFSET_CONF_BLK_ID ],
                                                     OSAL_NULL,
                                                     GNSS_PROXY_DATA_FORM_BASE_SIXTEEN );
      switch (u32CfgBlk)
      {
         case GNSS_PROXY_APP_LIST_BLOCK_ID_200:
         {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMSETPAROK_CFG_RCVD, NULL );
            // Application will be blocked waiting for the config response event. 
            if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                GNSS_PROXY_SAT_SYS_APP_BLK_200_SET_SUCCESS_EVENT,
                                                OSAL_EN_EVENTMASK_OR ))
            {
                GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                                     "Event Post failed err %lu line %d",
                                                       OSAL_u32ErrorCode(),__LINE__ );
            }
            break;
         }

         case GNSS_PROXY_APP_LIST_BLOCK_ID_227:
         {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMSETPAROK_CFG_RCVD, NULL );
            // Application will be blocked waiting for the config response event. 
            if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                GNSS_PROXY_SAT_SYS_APP_BLK_227_SET_SUCCESS_EVENT,
                                                OSAL_EN_EVENTMASK_OR ))
            {
                GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                                     "Event Post failed err %lu line %d",
                                     OSAL_u32ErrorCode(),__LINE__ );
            }
            break;
         }

         case GNSS_PROXY_GNSS_MIN_MAX_WEEK_BLOCK_ID:
         {
            // Application might be blocked waiting for config.
            if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                GNSS_PROXY_TESEO_COM_SET_EPOCH_RESPONSE_MASK,
                                                OSAL_EN_EVENTMASK_OR ))
            {
                GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "vHandlePSTMSETPAR:Event Post failed err %lu",
                                                       OSAL_u32ErrorCode() );
            }
         }
         break;

         default:
         {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_SWITCH_DEF, " %d , %s",
                                                      __LINE__, __FILE__ );
            break;
         }
      }
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMSETPARERROR
*
* PARAMETER   : None
*
* RETURNVALUE :
*
* DESCRIPTION : This is error response for $PSTMSETPAR.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 18.Aug.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandlePSTMSETPARERROR ( )
{
   GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "PSTMSETPARERROR recvd" );
   /* Application will be blocked waiting for the config response event. */
   if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                       GNSS_PROXY_SAT_SYS_CONF_SET_RES_FAILURE_EVENT,
                                       OSAL_EN_EVENTMASK_OR ))
   {
       GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                            "Event Post failed err %lu line %d",
                            OSAL_u32ErrorCode(),__LINE__ );
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMVER
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : Parses $PSTMVER to extract Teseo Firmware version
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 18.Sep.2013 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandlePSTMVER( tChar const * const pcFieldIndex[] )
{
   tString sBinImgVer,sNxtInvChar;
   tU32 u32LoopCnt,u32BibImgLen;

   //! just print a trace saying entered the function.
   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_RCVD_PSTMVER, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      //! its null. cannot proceed forward.
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                           "vHandlePSTMVER: NULL pointer argument" );
   }
   else
   {
      sBinImgVer = (tString)pcFieldIndex[GNSS_PROXY_PSTMVER_OFFSET_BINIMG_VER];
      u32BibImgLen = OSAL_u32StringLength(GNSS_PROXY_TESEO_BIN_IMAGE_LIB);
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT,GNSSPXY_PSTMVER_BIN_VER,
                            " %s",sBinImgVer);

      //! check whether received string is as excpectd.
      if( OSAL_s32StringNCompare(sBinImgVer,
                                       GNSS_PROXY_TESEO_BIN_IMAGE_LIB,
                                 u32BibImgLen) )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                           "vHandlePSTMVER: received unknown bin image type" );
      }
      else
      {
         //! move the pointer to point the begining of the binary image version number.
         sBinImgVer += (OSAL_u32StringLength(GNSS_PROXY_TESEO_BIN_IMAGE_LIB) + 1);

         for ( u32LoopCnt = 0;
              u32LoopCnt < GNSS_PROXY_NUM_FIELDS_TESEO_FIRMWARE_MSG;
              u32LoopCnt++ )
         {
            rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer <<= 8;
            rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer +=
                  OSAL_u32StringToU32( sBinImgVer,
                                       &sNxtInvChar,
                                       GNSS_PROXY_DATA_FORM_BASE_TEN );
            sBinImgVer = sNxtInvChar + 1;
         }

         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMVER_BIN_VER2,
                              " 0x%0x",
                              rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer );

         //! update the gnss device id based on the received gnss firmware version.
         if ( GNSS_PROXY_TESEO_3_FW_VER_MAJOR_NUMBER ==
                  (rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer &
                          GNSS_PROXY_TESEO_FW_VER_MAJOR_NUMBER_MASK ) )
         {
            //! gnss device mounted on the board is teseo3.
            rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8089;
            rGnssProxyInfo.rGnssConfigData.u16NumOfChannels = GNSS_PROXY_STA8089_SUPPORTED_CHANNELS;
            rGnssProxyInfo.rGnssConfigData.u16GnssSatStatusSupported = OSAL_C_U16_GNSS_SAT_USED_FOR_POSCALC;
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT,
                                 GNSSPXY_DEF_TRC_RULE,
                                 "vHandlePSTMVER: teseo3 is on board" );
         }
         else if ( GNSS_PROXY_TESEO_2_FW_VER_MAJOR_NUMBER == 
                       ( rGnssProxyInfo.rGnssConfigData.u32GnssRecvBinVer &
                                GNSS_PROXY_TESEO_FW_VER_MAJOR_NUMBER_MASK ) )
         {
            //! gnss device mounted on the board is teseo2.
            rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_STA8088;
            rGnssProxyInfo.rGnssConfigData.u16NumOfChannels = GNSS_PROXY_STA8088_SUPPORTED_CHANNELS;
            rGnssProxyInfo.rGnssConfigData.u16GnssSatStatusSupported = OSAL_C_U16_GNSS_SAT_USED_FOR_POSCALC;
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT,
                                 GNSSPXY_DEF_TRC_RULE,
                                 "vHandlePSTMVER: teseo2 is on board" );
         }
         else
         {
            //! don't know what is mounted.
            rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_UNKNOWN;
            rGnssProxyInfo.rGnssConfigData.u16NumOfChannels = OSAL_C_U8_GNSS_NO_CHANNELS;
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
             "vHandlePSTMVER:Gnss device unknown" );
         }

         //! post the received event.
         if( OSAL_ERROR ==
              OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                             GNSS_PROXY_TESEO_FIRMWARE_VERSION_WAIT_EVENT,
                                 OSAL_EN_EVENTMASK_OR ) )
         {
             GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                          "vHandlePSTMVER:Event Post failed err %lu line %d",
                                  OSAL_u32ErrorCode(),__LINE__ );
         }
      }
   }
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMCRCCHECK
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : Parses $PSTMCRCCHECK to Evaluate CRC of Teseo
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 06.Aug.2014 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandlePSTMCRCCHECK( tChar const * const pcFieldIndex[] )
{
   tString sExtractedCrc;

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_RCVD_PSTMCRCCHECK, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "!!! pointers to GnssProxy_vHandlePSTMCRCCHECK are null" );
   }
   else
   {
      //Extract GNSS FW CRC from the response
      sExtractedCrc = (tString)pcFieldIndex[GNSS_PROXY_PSTMCRCCHECK_OFFSET_RECVD_CRC];   
      rGnssProxyInfo.rGnssConfigData.u32GnssRecvFwCrc =
            OSAL_u32StringToU32( sExtractedCrc,
                                 OSAL_NULL,
                                 GNSS_PROXY_DATA_FORM_BASE_SIXTEEN );

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMCRCCHECK_CRC, "Gnss: 0x%x",
                           rGnssProxyInfo.rGnssConfigData.u32GnssRecvFwCrc );

      if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                          GNSS_PROXY_TESEO_CRC_CHECK_WAIT_EVENT,
                                          OSAL_EN_EVENTMASK_OR ))
      {
          GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "Gnss:Event Post failed err %lu line %d",
                                                 OSAL_u32ErrorCode(),__LINE__ );
      }
   }

}


/*********************************************************************************************************************
* FUNCTION     : GnssProxy_dConvToDeg
*
* PARAMETER    : dRawdata,  Data to be converted
*
* RETURNVALUE  : Converted result
*
* DESCRIPTION  : Converts position fix information from NMEA format to
*                degrees.
*                dRawdata format: DDMM.MMMMM
*                Decimal part of Degrees : (int)(DDMM.MMMMM)/100 = DD;
*                MINUTES = DDMM.MMMMM - DD*100;
*                Fractional part of degrees = MINUTES/60; //1 Deg = 60 Min 
*                final result = Decimal part of Degrees + Fractional part of degree.
* HISTORY      :
*---------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|-------------------------------------
* 18.JUN.2013  | Initial version: 1.0| MADHU KIRAN (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tDouble GnssProxy_dConvToDeg( tDouble dRawdata )
{
   tS32 s32Tmp =  (tS32)dRawdata;
   tDouble dResult, dDegrees, dMinutes;

   dDegrees = (tU32)(s32Tmp /100);
   dMinutes = dRawdata - (dDegrees * 100) ;
   dResult = dDegrees + (dMinutes / 60.0);

   return dResult;
}

/*********************************************************************************************************************
* FUNCTION     : vPreProcessRecord
*
* PARAMETER    : None
*
* RETURNVALUE  : None
*
* DESCRIPTION  : 1: Updates the channel status using the info of --GSA message
*                2: Updates GNSS Quality
*                3: Update Satellite system used
* HISTORY      :
*---------------------------------------------------------------------------
* Date         |       Version       | Author & comments
*--------------|---------------------|-------------------------------------
* 10.SEP.2013  | Initial version: 1.0| MADHU KIRAN (RBEI/ECF5)
* -------------------------------------------------------------------------
* 14.Nov.2014  | version: 2.2        | Madhu Kiran Ramachandra (RBEI/ECF5)
*              |                     | Update Sat-System info for GNSS Data
* ------------------------------------------------------------------------------
*********************************************************************************************************************/

static tVoid vPreProcessRecord()
{
   tU32 u32UsedSatIndx,u32VisibleSatIndx;
   tBool bSatFound;
   OSAL_tenGnssMode enGnssFixMode;
   OSAL_tenGnssQuality *penGnssQuality = &(rGnssProxyData.rGnssTmpRecord.rPVTData.rFixStatus.enQuality);

   /* Search in the list of visible satellites for the satellites used for fix.
      When found, update the channel status of the found satellite to
      OSAL_C_U16_GNSS_SAT_USED_FOR_POSCALC */

   for( u32UsedSatIndx = 0; 
        u32UsedSatIndx < rGnssProxyData.rGnssTmpRecord.rPVTData.u16SatsUsed;
        u32UsedSatIndx++ )
   {
       bSatFound = FALSE;
       for( u32VisibleSatIndx = 0; 
           (u32VisibleSatIndx < rGnssProxyData.rGnssTmpRecord.rPVTData.u16SatsVisible) && (TRUE != bSatFound);
            u32VisibleSatIndx++ )

      {
         if( rGnssProxyData.rGnssTmpRecord.rChannelStatus[u32VisibleSatIndx].u16SvID ==
            rGnssProxyData.rGnssTmpFixInfo.u16SatUsedFix[u32UsedSatIndx] )
         {
            rGnssProxyData.rGnssTmpRecord.rChannelStatus[u32VisibleSatIndx].u16SatStatus = 
                                                OSAL_C_U16_GNSS_SAT_USED_FOR_POSCALC;
            bSatFound = TRUE;
         }
      }

      if(FALSE == bSatFound)
      {
          GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Mismatch in sat Id");
      }

   }
   /* In case of Checksum error, it is requirement from NAVI that a NO-Fix status
    * is needed. This will avoid the usage of data which is not updated because of
    *  checksum errors. */
   if ( TRUE == rGnssProxyData.rGnssTmpFixInfo.bChecksumError )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "GNSS: Checksum error for NMEA" );

      //! if there is a checksum error in the recevied nmea message and the fix type
      //! in the corrupted message is 2D or 3D then make the fix type to NO FIX.
      if( ( ( GNSS_FIX_TYPE_2D == rGnssProxyData.rGnssTmpRecord.rPVTData.rFixStatus.enMode ) || 
            ( GNSS_FIX_TYPE_3D == rGnssProxyData.rGnssTmpRecord.rPVTData.rFixStatus.enMode )) &&
            ( GNSS_MIN_EXPECTED_NMEA_MESSAGES_FOR_3D_FIX != 
            ( rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs & GNSS_MIN_EXPECTED_NMEA_MESSAGES_FOR_3D_FIX )))
      {
         rGnssProxyData.rGnssTmpRecord.rPVTData.rFixStatus.enMode = GNSS_FIX_TYPE_NOFIX;
         *penGnssQuality = GNSSQUALITY_NOFIX;
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "Checksumerror_fix_type_changed_to_NOFIX Exp %x rec %x",
                        GNSS_MIN_EXPECTED_NMEA_MESSAGES_FOR_3D_FIX, rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs);
      }
   }
   else
   {
      /* Update GNSS Quality */
      enGnssFixMode = rGnssProxyData.rGnssTmpRecord.rPVTData.rFixStatus.enMode;
      switch (enGnssFixMode)
      {
         case GNSS_FIX_TYPE_NOFIX:
         {
            *penGnssQuality = GNSSQUALITY_NOFIX;
            break;
         }
         case GNSS_FIX_TYPE_3D:
         case GNSS_FIX_TYPE_2D:
         {
            if( TRUE == rGnssProxyData.rGnssTmpFixInfo.bDGpsUsed )
            {
               *penGnssQuality = GNSSQUALITY_DIFFERENTIAL;
            }
            else
            {
               *penGnssQuality = GNSSQUALITY_AUTONOMOUS;
            }
            break;
         }
         default:
         {
            *penGnssQuality = GNSSQUALITY_UNKNOWN;
            break;
         }
      }
   }
   // Update the satellite system used. This is read during initialization
   // Later it can be updated by Ioctl.
   rGnssProxyData.rGnssTmpRecord.rPVTData.u8SatSysUsed =
                  (tU8)rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32ValidateSatSys
*
* PARAMETER   : u32SatSys - sat sys to be set
*
* RETURNVALUE : s32RetVal
*
* DESCRIPTION : 1: Check if invalid bit 6 and bit 7 is set
*                       2: Send error on failure.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 27.JUN.2016 | Initial version: 1.0 | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/

tS32 GnssProxy_s32ValidateSatSys(tU32 u32SatSys)
{
   tS32 s32RetVal = OSAL_E_NOERROR;

   if( 0 == u32SatSys )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "SatSys Value cannot be zero");
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else if( ( OSAL_C_U8_GNSS_SATSYS_COMPASS & u32SatSys ) && 
        ( rGnssProxyInfo.rGnssConfigData.enGnssHwType == GNSS_HW_STA8088 ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "Compass is not supported in Teseo-2 Chip");
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else if(u32SatSys & ~(GNSS_PXY_VALID_SATSYS_MASK) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "GnssProxy_s32SetSatSys: Invalid "
                           "Sat System Value" );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }

   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32SetFrameBlk200_227
*
* PARAMETER   : u32SatSys - sat sys to be set
*                      pu32TeseoBitMaskBlk200 - pointer to bit mask for cfg block 200
*                      pu32TeseoBitMaskBlk227 - pointer to bit mask for cfg block 227
*
* RETURNVALUE : s32RetVal
*
* DESCRIPTION : 1: Set the cfg for block 200 and 227
*                       2: The cfg block 200 and 227 is either set to the value based on 
*                           the sat system or initial configuration is cleared
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 27.JUN.2016 | Initial version: 1.0 | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32FrameCfgBlk200_227(tU32 u32SatSys, tPU32 pu32TeseoBitMaskBlk200, tPU32 pu32TeseoBitMaskBlk227)
{
   tS32 s32RetVal = OSAL_E_NOERROR;

   *pu32TeseoBitMaskBlk200 = GnssProxy_u32FrameSatSysBlk200( u32SatSys );
   *pu32TeseoBitMaskBlk227 = GnssProxy_u32FrameSatSysBlk227( u32SatSys );

   if ( ( 0 == *pu32TeseoBitMaskBlk200 ) && ( 0 == *pu32TeseoBitMaskBlk227 ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "%s Both the config blocks cannot be set to 0 ",__FUNCTION__ );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                           "%s New Cfg for Block ID 200 "
                           " 0x%x",__FUNCTION__, *pu32TeseoBitMaskBlk200 );
   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                           "%s New Cfg for Block ID 227 "
                           " 0x%x",__FUNCTION__, *pu32TeseoBitMaskBlk227 );

   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32GetCfgBlk200_227
*
* PARAMETER   : None
*
* RETURNVALUE : None
*
* DESCRIPTION : Get Configuration of block 200 and 227 
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 13-JUL-2017 | Initial version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32GetCfgBlk200_227()
{
   tS32 s32ErrVal = OSAL_E_NOERROR;
   
   (tVoid)s32ErrVal;
   
   if((tS32)OSAL_E_NOERROR != ( s32ErrVal = GnssProxy_s32GetSatConfigReq(GNSS_PROXY_APP_LIST_BLOCK_ID_200)) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, 
                           "%s GnssProxy_s32GetSatConfigReq for block 200 failed",
                           __FUNCTION__);
      s32ErrVal = OSAL_ERROR;
   }
   else if( (tS32)OSAL_E_NOERROR != ( s32ErrVal = GnssProxy_s32GetSatConfigReq(GNSS_PROXY_APP_LIST_BLOCK_ID_227)) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, 
                           "%s GnssProxy_s32GetSatConfigReq for block 227 failed",
                           __FUNCTION__);
      s32ErrVal = OSAL_ERROR;
   }

   return s32ErrVal;
}



/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32SetCfgBlk200_227
*
* PARAMETER   : u32TeseoBitMaskBlk200 - bit mask to be set for cfg blk 200
*                      u32TeseoBitMaskBlk227 - bit mask to be set for cfg blk 227
*
* RETURNVALUE : None
*
* DESCRIPTION : 1: Set the bit mask for config block for 200 and 227
*                       2: Also clear the initial cfg blk bit mask
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 27.JUN.2016 | Initial version: 1.0 | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/

tS32 GnssProxy_s32SetCfgBlk200_227(tU32 u32TeseoBitMaskBlk200, tU32 u32TeseoBitMaskBlk227)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   (tVoid)s32RetVal;
   if ( (tS32)OSAL_E_NOERROR != ( s32RetVal = 
         GnssProxy_s32SetSatSysConfBlk200(u32TeseoBitMaskBlk200) ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                          "%s GnssProxy_s32SetSatSys failed for Cfg 200"
                          "Bit Mask %d",__FUNCTION__, u32TeseoBitMaskBlk200 );
      s32RetVal = OSAL_ERROR;
   }
   // Send command to set configuration block 227 
   else if ( (tS32)OSAL_E_NOERROR != ( s32RetVal = 
              GnssProxy_s32SetSatSysConfBlk227(u32TeseoBitMaskBlk227) ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                          "%s GnssProxy_s32SetSatSys failed for Cfg 227"
                          "Bit Mask %d",__FUNCTION__, u32TeseoBitMaskBlk227 );
      s32RetVal = OSAL_ERROR;
   }
   else
   {
      
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                           "Cfg for Block ID 200 "
                           " 0x%x", u32TeseoBitMaskBlk200 );
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                           "Cfg for Block ID 227 "
                           " 0x%x", u32TeseoBitMaskBlk227 );
   }

   // clear the value 
   GnssProxy_vEnterCriticalSection(__LINE__);
   rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk200 =0;
   rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk227 =0;
   GnssProxy_vLeaveCriticalSection(__LINE__);
   
   return s32RetVal;
}
   



/*********************************************************************************************************************
* FUNCTION    : GnssProxy_32SetSatSys
*
* PARAMETER   : *pU32Arg: Pointer to the satellite system to be set.
*               Satellite being configured is stored in the same parameter.
*
* RETURNVALUE : None
*
* DESCRIPTION : 1: Send command to set the requested satellite system
*               2: If response from Teseo is positive, reset GPS engine
*               3: Get the configuration block and check the satellite system used
*               4: Respond to the application with the configured satellite system.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.SEP.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32SetSatSys( tPU32 pu32SatSys )
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32TeseoBitMaskBlk200 = 0;
   tU32 u32TeseoBitMaskBlk227 = 0;

   (tVoid)s32RetVal;
   // Validate the Satellite system mask
   if ( (tS32)OSAL_E_NOERROR != ( s32RetVal = GnssProxy_s32ValidateSatSys(*pu32SatSys)))
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "%s: Validation of SatSys failed\n", __FUNCTION__);
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   //Clear existing teseo Com events
   else if ( (tS32)OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                    0,
                                                    OSAL_EN_EVENTMASK_AND) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
                           "%s: Cleanup Event Post failed err %lu line %lu\n",
                           __FUNCTION__, OSAL_u32ErrorCode(), __LINE__ );
       s32RetVal = OSAL_ERROR;
   }
   //Get the existing configuration for blocks 200 and 227
   else if( ( (tS32)OSAL_E_NOERROR ) != ( s32RetVal = GnssProxy_s32GetCfgBlk200_227() ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
                            "%s:Get existing frame block cfg failed\n",
                            __FUNCTION__, OSAL_u32ErrorCode(), __LINE__ );
      s32RetVal = OSAL_ERROR;
   }
   //frame the configuration blocks 200 and 227 according to requested sat sys configuration
   else if ( (tS32)OSAL_E_NOERROR != ( s32RetVal = 
             GnssProxy_s32FrameCfgBlk200_227(*pu32SatSys, &u32TeseoBitMaskBlk200, &u32TeseoBitMaskBlk227) ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                                  "%s:Setting of frameblock failed\n",__FUNCTION__);
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   // Send command to set configuration  block 200 and 227
   else if ( (tS32)OSAL_E_NOERROR != ( s32RetVal = 
              GnssProxy_s32SetCfgBlk200_227(u32TeseoBitMaskBlk200, u32TeseoBitMaskBlk227) ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "%s GnssProxy_s32SetSatSys failed for "
                           "Blks 200 and 227",__FUNCTION__ );
      s32RetVal = OSAL_ERROR;
   }
   //Resetting GPS Engine will cause side effects if PPS is enabled.Also in new firmware versions 
   //GPS engine reset is not needed to set satellite configuration. So this is disabled now. 
#ifdef GNSS_PROXY_ENABLE_RESET_GPS_ENGINE
      else if( (tS32)OSAL_E_NOERROR !=( s32RetVal = GnssProxy_s32ResetGpsEngine() ))
      {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
                           "%s GnssProxy_s32ResetGpsEngine failed line %lu",
                           __FUNCTION__, __LINE__ );
      s32RetVal = OSAL_ERROR;
   }
#endif
   // Send command to get configuration block 200 and 227,  this is needed to validate the set configuration
   else if( (tS32)OSAL_E_NOERROR != ( s32RetVal = GnssProxy_s32GetCfgBlk200_227() ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
                           "%s GnssProxy_s32GetSatConfigReq failed", __FUNCTION__ );
      s32RetVal = OSAL_ERROR;
   }
   else
   {
      // set sat sys value that has been set
      GnssProxy_vEnterCriticalSection(__LINE__);
      *pu32SatSys = rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed;
      GnssProxy_vLeaveCriticalSection(__LINE__);
   }

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, 
                        GNSSPXY_DEF_TRC_RULE, "Sat System Stored : %x",
                        rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed);
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vReadRegistryValues
*
* PARAMETER   : None
*
* RETURNVALUE : None
*
* DESCRIPTION : Read device related configuration values from the OSAL registry.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 28.NOV.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vReadRegistryValues(tVoid)
{
   OSAL_tIODescriptor    rRegistryIODescriptor ;
   OSAL_trIOCtrlRegistry rIOCtrlRegistry       = { 0 };
   tU32                  u32RegistryValue      = 0x00000000;

   rRegistryIODescriptor = OSAL_IOOpen(
                           GNSS_DEVICE_REGISTRY,
                           OSAL_EN_READONLY);

   if (rRegistryIODescriptor == OSAL_ERROR)
   {
      rGnssProxyInfo.u32GnssSatSysLoc = GNSS_PROXY_SAT_SYS_LOC_STATIC;
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                          "GNSS: GnssProxy_u32ReadRegistryValues() => OSAL_IOOpen(GNSS_DEVICE_REGISTRY) failed with error code = %s",
                          OSAL_coszErrorText(OSAL_u32ErrorCode()));
   }
   else
   {
      (tVoid)rIOCtrlRegistry;
      
      rIOCtrlRegistry.pcos8Name = (tCS8*)"GNSS_SAT_SYS_CFG";
      rIOCtrlRegistry.ps8Value  = (tU8*)&u32RegistryValue;
      rIOCtrlRegistry.u32Size   = sizeof(tS32);

      if (OSAL_s32IOControl( rRegistryIODescriptor, OSAL_C_S32_IOCTRL_REGGETVALUE,
                             (tLong)&rIOCtrlRegistry) == OSAL_OK)
      {
         rGnssProxyInfo.u32GnssSatSysLoc = u32RegistryValue;
      }
      else
      {
         rGnssProxyInfo.u32GnssSatSysLoc = GNSS_PROXY_SAT_SYS_LOC_STATIC;
         GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                            "GNSS: GnssProxy_u32ReadRegistryValues() => Failed to read value for GNSS_SAT_SYS_CFG from OSAL registry. Used default value = %u mV.",
                            rGnssProxyInfo.u32GnssSatSysLoc);
      }

      if (OSAL_s32IOClose(rRegistryIODescriptor) == OSAL_ERROR)
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                              "GNSS: GnssProxy_u32ReadRegistryValues() => OSAL_IOClose(DEVICE_REGISTRY) failed with error code = %s",
                              OSAL_coszErrorText(OSAL_u32ErrorCode()));
      }
   }
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vWriteLastUsedSatSys
*
* PARAMETER   : u32actGnssSatSys - Current used satellite systems
*
* RETURNVALUE : None
*
* DESCRIPTION : Write the current used GNSS sat system and default sat sys from persistent storage to a file
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 28.NOV.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vWriteLastUsedSatSys( tU32 u32actGnssSatSys )
{
   OSAL_tIODescriptor hFile;
   tS32 s32Length;
   
   hFile = OSAL_IOOpen( GNSS_LAST_SATSYS_USED_FILENAME, OSAL_EN_WRITEONLY );

   if ( OSAL_ERROR == hFile )
   {
      if ( OSAL_E_DOESNOTEXIST == OSAL_u32ErrorCode () )
      {
         hFile = OSAL_IOCreate( GNSS_LAST_SATSYS_USED_FILENAME, 
                                OSAL_EN_READWRITE );

         if ( OSAL_ERROR == hFile )
         {
            /* file could not be created do nothing */
            GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "OSAL_s32IOCreate : Fail line %d", __LINE__);
         }
      }
   }

   if (hFile != OSAL_ERROR)
   {
      /* handle is valid */
      s32Length = OSAL_s32IOWrite ( hFile, 
                        (tPCS8) &u32actGnssSatSys, 
                        sizeof ( u32actGnssSatSys ) );
      if(s32Length == OSAL_ERROR)
      {
         GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "OSAL_s32IOWrite : Fail line %d", __LINE__);
      }
      /* Storing the New Satellite system calibration value,
      which is read during the open*/
      s32Length = OSAL_s32IOWrite ( hFile, 
                 (tPCS8) &u32NewSatSysCalib , 
                 sizeof ( u32NewSatSysCalib  ) );
      if(s32Length == OSAL_ERROR)
      {
         GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "OSAL_s32IOWrite : Fail line %d", __LINE__);
      }

      if (OSAL_s32IOClose(hFile) == OSAL_ERROR)
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                              "GNSS: GnssProxy_vWriteLastUsedSatSys() => OSAL_IOClose() failed with error code = %s",
                              OSAL_coszErrorText(OSAL_u32ErrorCode()));
      }
   }
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_u32ReadLastUsedSatSys
*
* PARAMETER   : None
*
* RETURNVALUE : bitfield of satellite systems according to OSAL_C_U8_GNSS_SATSYS_XXXX
*
* DESCRIPTION : Read from file the last used GNSS sat systems
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 28.NOV.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 GnssProxy_u32ReadLastUsedSatSys( tVoid )
{
   OSAL_tIODescriptor hFile;
   tU32 u32lastUsedGnssSatSys = OSAL_C_U8_GNSS_SATSYS_UNKNOWN;
   tS32 s32Length = OSAL_ERROR;
   
   hFile = OSAL_IOOpen( GNSS_LAST_SATSYS_USED_FILENAME, OSAL_EN_READONLY );
   
   if ( OSAL_ERROR == hFile )
   {
      GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "OSAL_s32IOOpen : Fail line %d", __LINE__);
   }
   else
   {
      s32Length = OSAL_s32IORead ( hFile,
                       (tPS8 ) &u32lastUsedGnssSatSys,
                       sizeof ( u32lastUsedGnssSatSys ) );
      if(s32Length != (tS32)sizeof ( u32lastUsedGnssSatSys ))
      {
         GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "OSAL_s32IORead : Fail line %d", __LINE__);
      }
      else
      {
         s32Length = OSAL_s32IORead ( hFile,
                           (tPS8 ) &u32OldSatSysCalib ,
                            sizeof ( u32OldSatSysCalib  ) );
         if(s32Length != (tS32)sizeof ( u32OldSatSysCalib))
         {
            GnssProxy_vTraceOut(TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, "OSAL_s32IORead : Fail line %d", __LINE__);
         }
      }

      if (OSAL_s32IOClose(hFile) == OSAL_ERROR)
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                              "GNSS: GnssProxy_u32ReadLastUsedSatSys() => OSAL_IOClose() failed with error code = %s",
                              OSAL_coszErrorText(OSAL_u32ErrorCode()));
      }
   }
   return ( u32lastUsedGnssSatSys );
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_u32GetGnssDefaultSatSysEOL
*
* PARAMETER   : None
*
* RETURNVALUE : Default satellite system according to OSAL_C_U8_GNSS_SATSYS_XXXX
*
* DESCRIPTION : Get the default satellite system from EOL
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 28.NOV.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 GnssProxy_u32GetGnssDefaultSatSysEOL(void)
{
   OSAL_tIODescriptor EOLFd = {0};
   OSAL_trDiagEOLEntry rEOLData = {0};
   tU8 u8EolGlonassDefault = GNSS_SATSYS_DEFAULT_GPS; //Default value is 0, this is to 
                                                      //set GPS as default satellite system
   tS32 s32Length = OSAL_ERROR;
   tU32 u32Return = OSAL_C_U8_GNSS_SATSYS_GPS;
   tU32 u32lastSatSysUsed;

   rEOLData.u8Table        = EOLLIB_TABLE_ID_COUNTRY;
   rEOLData.u16Offset      = EOLLIB_OFFSET_ENABLE_GLONAS_AS_GPS_DEFAULT;
   rEOLData.u16EntryLength = sizeof(tU8);
   rEOLData.pu8EntryData   = (tU8*) &u8EolGlonassDefault;

   /* Read default from EOL */
   EOLFd = OSAL_IOOpen ( OSAL_C_STRING_DEVICE_DIAG_EOL, (OSAL_tenAccess)0 );

   if ( EOLFd != OSAL_ERROR )
   {
      /* u8EolGlonassDefault value is read, this value is set to 1 if GLONASS is 
         used as default and 0 if GPS is used as default */
      s32Length = OSAL_s32IORead( EOLFd,
                                  (tPS8)(& rEOLData),
                                  (tU32)(sizeof(rEOLData))); 
      if( s32Length != 1 )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE,  "GNSS: OSAL_s32IORead for EOLFd Failed with error" 
                                           "code = %s", OSAL_coszErrorText(OSAL_u32ErrorCode()));
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_SATSYSEOL_EOL_VAL, " %d", 
                                     *(rEOLData.pu8EntryData));
      }
      if (OSAL_s32IOClose(EOLFd) == OSAL_ERROR)
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "GNSS: GnssProxy_u32GetGnssDefaultSatSysEOL() => OSAL_IOClose() failed with error code = %s",
                           OSAL_coszErrorText(OSAL_u32ErrorCode()));
      }
   }
   
   /* Expected value of u8EolGlonassDefault is either 0 or 1, any other value GLONAS
         is set as default satellite system */
   if (u8EolGlonassDefault)
   {
      u32NewSatSysCalib = OSAL_C_U8_GNSS_SATSYS_GLONASS;
   }
   else
   {
      u32NewSatSysCalib = OSAL_C_U8_GNSS_SATSYS_GPS;
   }
   /* Driver will compare  the newly read calibration value and previously stored 
     calibration value, if ther are differ it uses the satellite system selected
     mentioned in calibgration parameter or else use the last used satellite system*/
   u32lastSatSysUsed = GnssProxy_u32ReadLastUsedSatSys();

   if((u32OldSatSysCalib  != u32NewSatSysCalib) && (1 == s32Length))
   {
      u32Return = u32NewSatSysCalib;
   }
   else
   {
      u32Return = u32lastSatSysUsed;
   }
   return ( u32Return );
}


/*********************************************************************************************************************
* FUNCTION    : GnssProxy_u32GetGnssDefaultSatSysKDS
*
* PARAMETER   : None
*
* RETURNVALUE : Default satellite system according to OSAL_C_U8_GNSS_SATSYS_XXXX
*
* DESCRIPTION : Get the default satellite system from KDS
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 28.NOV.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 GnssProxy_u32GetGnssDefaultSatSysKDS ( void )
{
   OSAL_tIODescriptor KDSFd;
   tsKDSEntry rKDSData;

   tS32 s32Length = OSAL_ERROR;
   tU32 u32Return = OSAL_C_U8_GNSS_SATSYS_GPS;
   tU32 u32lastSatSysUsed;

   OSAL_pvMemorySet( &rKDSData, 0, sizeof( rKDSData) );

   rKDSData.u16Entry        =  KDS_GNSS_DID_ENTRY;
   rKDSData.u16EntryLength  =  KDS_GNSS_DID_ENTRY_LEN;
   rKDSData.u16EntryFlags   =  M_KDS_ENTRY_FLAG_NONE;

   KDSFd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY);

   if( KDSFd != OSAL_ERROR )
   {  
      s32Length = OSAL_s32IORead(KDSFd,
                                (tS8 *)(&rKDSData),
                                (tS32)sizeof(rKDSData));

      if(s32Length == OSAL_ERROR)
      {
        GnssProxy_vTraceOut( TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, 
                             "GNSS: OSAL_s32IORead for KDSFd Failed with error"
                             " code = %d", OSAL_u32ErrorCode());
      }
      else
      {
         u32NewSatSysCalib = (rKDSData.au8EntryData[KDS_GNSS_SAT_SYS_OFFSET] & KDS_GNSS_SAY_SYS_MASK);
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                              "GNSS Sat sys value stored in KDS  : 0x%x ", u32NewSatSysCalib);
      }
      if (OSAL_s32IOClose(KDSFd) == OSAL_ERROR)
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                             "GNSS: GnssProxy_u32GetGnssDefaultSatSysKDS() =>"
                             " OSAL_IOClose() failed with error code = %d",
                             OSAL_u32ErrorCode());
      }
   }

   //If bit 6 or bit 7 is set then invalid sat sys value read from KDS
   if( (tS32)OSAL_E_NOERROR != GnssProxy_s32ValidateSatSys(u32NewSatSysCalib) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE, 
                           "GnssProxy_u32GetGnssDefaultSatSysKDS: "
                           "Invalid Sat System Set, Setting to default" );
      u32Return = GNSS_SATSYS_DEFAULT_GPS;
   }
   else
   {
      /* Driver will compare  the newly read calibration value and previously stored 
         calibration value, if ther are differ it uses the satellite system selected
         mentioned in calibration parameter or else use the last used satellite system */
      u32lastSatSysUsed = GnssProxy_u32ReadLastUsedSatSys();
      if((u32OldSatSysCalib  != u32NewSatSysCalib) && (s32Length != OSAL_ERROR))
      {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, 
                                 "Stored value is different than value read from KDS");
         u32Return = u32NewSatSysCalib;
      }
      else
      {
         u32Return = u32lastSatSysUsed;
      }
   }
   return ( u32Return );
}


/*********************************************************************************************************************
* FUNCTION    : GnssProxy_u32GetGnssDefaultSatSysDefine
*
* PARAMETER   : None
*
* RETURNVALUE : Default satellite system according to OSAL_C_U8_GNSS_SATSYS_XXXX
*
* DESCRIPTION : Return statically defined satellite system
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 28.NOV.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 GnssProxy_u32GetGnssDefaultSatSysDefine ( void )
{
   return (OSAL_C_U8_GNSS_SATSYS_GPS);
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_u32GetGnssDefaultSatSys
*
* PARAMETER   : None
*
* RETURNVALUE : Default satellite system according to OSAL_C_U8_GNSS_SATSYS_XXXX
*
* DESCRIPTION : Get the default satellite system from EOL/KDS or static data based on registry value
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 28.NOV.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 GnssProxy_u32GetGnssDefaultSatSys(void)
{
   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "GetGnssDefaultSatSys function called");

   tU32 u32SatSys = OSAL_C_U8_GNSS_SATSYS_GPS;
   
   switch(rGnssProxyInfo.u32GnssSatSysLoc)
   {
      case GNSS_PROXY_SAT_SYS_LOC_EOL :
         
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Case entered : EOL");
         u32SatSys = GnssProxy_u32GetGnssDefaultSatSysEOL();
         break;
      case GNSS_PROXY_SAT_SYS_LOC_KDS :
         
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Case entered : KDS");
         u32SatSys = GnssProxy_u32GetGnssDefaultSatSysKDS();
         break;
      case GNSS_PROXY_SAT_SYS_LOC_STATIC :
         
         GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE, "Case entered : Static");
         u32SatSys = GnssProxy_u32GetGnssDefaultSatSysDefine();
         break;
      default :
        GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:GnssProxy_u32GetGnssDefaultSatSys : Undefined satellite system location " );
         break;
   }
   return u32SatSys;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32SetSatConfigReq
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK on success
*               OSA_ERROR in case of failure
*
* DESCRIPTION : Set Configuration of teseo application list.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 18.NOV.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32SetSatConfigReq()
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32SatSysToUse;

   u32SatSysToUse = GnssProxy_u32GetGnssDefaultSatSys();
   if (u32SatSysToUse != 0)
   {
      s32RetVal = GnssProxy_s32SetSatSys( (tPU32)&u32SatSysToUse );
      if (OSAL_E_NOERROR != (tU32)s32RetVal)
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GnssProxy_s32SetSatConfigReq FAIL line %d, RetVal %d",
                                             __LINE__, s32RetVal );
      }
      else
      {
         GnssProxy_vWriteLastUsedSatSys( u32SatSysToUse );
      }
   }
   else
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "GnssProxy_s32SetSatConfigReq FAIL line %d, RetVal %d",
                                             __LINE__, s32RetVal );
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32GetSatConfigReq
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK on success
*               OSA_ERROR in case of failure
*
* DESCRIPTION : Get Configuration of teseo application list.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.SEP.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32GetSatConfigReq(tU32 u32CfgBlkID)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32ResultMask =0;
   tU32 u32MsgLength = 0;

   (tVoid)s32RetVal;
      
   switch (u32CfgBlkID)
   {
      case GNSS_PROXY_APP_LIST_BLOCK_ID_200:
         
         /* INC message to get the current config */
         rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
         rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
         OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                            GNSS_PROXY_TESEO_GET_APP_CFG_BLK_200_CMD,
                            OSAL_u32StringLength(GNSS_PROXY_TESEO_GET_APP_CFG_BLK_200_CMD) );

         u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                  OSAL_u32StringLength(GNSS_PROXY_TESEO_GET_APP_CFG_BLK_200_CMD);
         break;
         
      case GNSS_PROXY_APP_LIST_BLOCK_ID_227:
         
         /* INC message to get the current config */
         rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
         rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
         OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                            GNSS_PROXY_TESEO_GET_APP_CFG_BLK_227_CMD,
                            OSAL_u32StringLength(GNSS_PROXY_TESEO_GET_APP_CFG_BLK_227_CMD) );

         u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                  OSAL_u32StringLength(GNSS_PROXY_TESEO_GET_APP_CFG_BLK_227_CMD);
         break;
      default:
         break;
   }

   s32RetVal = GnssProxy_s32SendDataToScc(u32MsgLength);

   if ( (tS32)u32MsgLength != s32RetVal)
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  
                           "GnssProxy_s32SendDataToScc FAIL line %d, RetVal %d",
                                             __LINE__, s32RetVal );
      s32RetVal = OSAL_E_UNKNOWN;
   }
   /* Wait for configuration block from ST */
   else if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                              (GNSS_PROXY_SAT_SYS_CONF_BLK200_REQ_WAIT_EVENT | 
                                              GNSS_PROXY_SAT_SYS_CONF_BLK227_REQ_WAIT_EVENT | 
                                              GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT),
                                              OSAL_EN_EVENTMASK_OR,
                                              GNSS_PROXY_TESEO_RESPONSE_WAIT_TIME,
                                              &u32ResultMask ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Config req:Event wait failed err %d line %lu",
                                             OSAL_u32ErrorCode(), __LINE__ );
      if( OSAL_E_TIMEOUT == OSAL_u32ErrorCode() )
      {
         s32RetVal = OSAL_E_TIMEOUT;
      }
      else
      {
         s32RetVal = OSAL_E_UNKNOWN;
      }
   }
   /* Clear event. */
   else
   {
      if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                ~u32ResultMask,
                                                OSAL_EN_EVENTMASK_AND) )
      {
          GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "cleanup:Event Post failed err %lu line %lu",
                                                 OSAL_u32ErrorCode(), __LINE__ );
          s32RetVal = OSAL_E_UNKNOWN;
      }
      /* Cancel operation in case of shutdown event */
      if ( GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT == u32ResultMask )
      {
         s32RetVal = OSAL_E_CANCELED;
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Set Sat system canceled: Shutdown signaled ");
         GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT );
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_USER_1, GNSSPXY_GETSATCFG_EVE_CLR, NULL );
         s32RetVal = OSAL_E_NOERROR;
      }
   }

   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32ApndSndCfgBlk
*
* PARAMETER   : u32MsgLength-Message Length
*
* RETURNVALUE : OSAL_E_NOERROR  on success
*               Appropriate osal error codes in case of failure
*
* DESCRIPTION : 1: Append the end of teseo message and send to SCC 
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 27.JUN.2016 | Initial Version: 1.0 | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32ApndSndCfgBlk(tU32 u32MsgLength)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32ResultMask =0;

   (tVoid)s32RetVal;
      
      /* Add command tail and complete the message */
      rGnssProxyInfo.u8TransBuffer[u32MsgLength] = GNSS_PROXY_NMEA_CHECKSUM_DELIMITER_ASTERISK;
      u32MsgLength++;
      rGnssProxyInfo.u8TransBuffer[u32MsgLength] = GNSS_PROXY_ASCII_VALUE_CARRIAGE_RETURN;
      u32MsgLength++;
      rGnssProxyInfo.u8TransBuffer[u32MsgLength] = GNSS_PROXY_ASCII_VALUE_LINE_FEED;
      u32MsgLength++;

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_FINAL_MSG_SZ, " %s size: %d",
                           (tString)&rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                           u32MsgLength);
      /* Send the message to SCC */
      s32RetVal = GnssProxy_s32SendDataToScc(u32MsgLength);

      if ((tS32)u32MsgLength != s32RetVal)
      {
         s32RetVal = OSAL_E_UNKNOWN;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "%s write failed for Set CFG", __FUNCTION__);
      }
      /* Wait for response from TESEO */
      else if ( OSAL_ERROR == OSAL_s32EventWait(
                                     rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                  (GNSS_PROXY_SAT_SYS_APP_BLK_200_SET_SUCCESS_EVENT |
                                   GNSS_PROXY_SAT_SYS_APP_BLK_227_SET_SUCCESS_EVENT |
                                     GNSS_PROXY_SAT_SYS_CONF_SET_RES_FAILURE_EVENT |
                                     GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT ),
                                     OSAL_EN_EVENTMASK_OR,
                                     GNSS_PROXY_TESEO_RESPONSE_WAIT_TIME,
                                     &u32ResultMask ))
      {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "Config set res:Event wait failed err %d line %lu",
                           OSAL_u32ErrorCode(), __LINE__ );
      s32RetVal = OSAL_ERROR;
   }
   else
   {
      // Clear event. 
      if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                          ~u32ResultMask,
                                          OSAL_EN_EVENTMASK_AND))
      {
          GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                               "cleanup:Event Post failed err %lu line %lu",
                               OSAL_u32ErrorCode(), __LINE__ );
          s32RetVal = OSAL_E_UNKNOWN;
      }
      // Cancel operation in case of shutdown event 
      if ( GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT == u32ResultMask )
      {
         s32RetVal = OSAL_E_CANCELED;
         GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT );
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,
                              "%s Set Sat system canceled: Shutdown signaled ",
                              __FUNCTION__);
      }
      // Got a error response for set config 
         else if ( GNSS_PROXY_SAT_SYS_CONF_SET_RES_FAILURE_EVENT == u32ResultMask )
         {
         GnssProxy_vTraceOut( TR_LEVEL_ERROR, GNSSPXY_DEF_TRC_RULE,
                              "%s Error response rcvd for "
                              "$PSTMSETPAR,1200, or $PSTMSETPAR,1227,",__FUNCTION__ );
         s32RetVal = OSAL_E_UNKNOWN;
      }
      // Got the success response from TESEO 
         else
         {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_SETCFG_RESP_OK, NULL );
            s32RetVal = OSAL_E_NOERROR;
         }
      }

      return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32SetSatSysConfBlk200
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK on success
*               Appropriate osal error codes in case of failure
*
* DESCRIPTION : 1: Configure teseo application list config block 200
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.SEP.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxy_s32SetSatSysConfBlk200(tU32 u32TeseoBitMask)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32OldTeseoCfgBlk200;
   tU32 u32NewTeseoCfg;
   tU32 u32MsgLength;

   GnssProxy_vEnterCriticalSection(__LINE__);
   u32OldTeseoCfgBlk200 = rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk200;
   GnssProxy_vLeaveCriticalSection(__LINE__);

   if(0 == u32OldTeseoCfgBlk200)
   {
      s32RetVal = OSAL_E_UNKNOWN;
   }
   else
   {
      u32NewTeseoCfg = (u32OldTeseoCfgBlk200 & GNSS_PXY_BLK_200_CLR_SAT_CONST_FIELD);
      u32NewTeseoCfg |= u32TeseoBitMask;

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_OLDNEW_TES_CFG,
                           " %x , %x", u32OldTeseoCfgBlk200, u32NewTeseoCfg );

      //Clear transmit buffer 
      OSAL_pvMemorySet( rGnssProxyInfo.u8TransBuffer,0,GNSS_PROXY_TRANSMIT_BUFF_SIZE);

      //Add message Header 
      rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
      rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
      
      (tVoid)OSAL_szStringNCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                                 (tString)GNSS_PROXY_TESEO_SET_APP_CBD_200_HEADER,
                                 OSAL_u32StringLength(GNSS_PROXY_TESEO_SET_APP_CBD_200_HEADER) );
      
      //Get to the end of the message and append configuration block to message 
      u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER 
                     + OSAL_u32StringLength(GNSS_PROXY_TESEO_SET_APP_CBD_200_HEADER);
      OSAL_s32PrintFormat( (char*)&rGnssProxyInfo.u8TransBuffer[u32MsgLength], "%x", (tS32)u32NewTeseoCfg );

      u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                     OSAL_u32StringLength(&rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG]);

      s32RetVal = GnssProxy_s32ApndSndCfgBlk(u32MsgLength);

      if( (tS32)OSAL_E_NOERROR != s32RetVal )
      {
         s32RetVal = OSAL_ERROR;
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                              "%s Append and Sending Teseo config Error", __FUNCTION__ );
      }
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32Set227CfgMask
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK on success
*               Appropriate osal error codes in case of failure
*
* DESCRIPTION : 1: Configure teseo application list config block 227
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32Set227CfgMask(tU32 u32TeseoBitMask,tU32 u32OldTeseoCfgBlk227 )
{
   
   tU32 u32NewTeseoCfg;
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32MsgLength;

   (tVoid)s32RetVal;
   
   u32NewTeseoCfg = (u32OldTeseoCfgBlk227) & (GNSS_PXY_BLK_227_CLR_SAT_CONST_FIELD);
   u32NewTeseoCfg |= u32TeseoBitMask;

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_OLDNEW_TES_CFG, 
                        " %x , %x", u32OldTeseoCfgBlk227, u32NewTeseoCfg );
   /* Clear transmit buffer */
   OSAL_pvMemorySet( rGnssProxyInfo.u8TransBuffer,0,GNSS_PROXY_TRANSMIT_BUFF_SIZE);

   /* Add message Header */
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
   
   //configuration block 227
   (tVoid)OSAL_szStringNCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                              (tString)GNSS_PROXY_TESEO_SET_APP_CBD_227_HEADER,
                              OSAL_u32StringLength(GNSS_PROXY_TESEO_SET_APP_CBD_227_HEADER) );
   /* Get to the end of the message and append configuration block to message */
   u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER \
                  + OSAL_u32StringLength(GNSS_PROXY_TESEO_SET_APP_CBD_227_HEADER);
   OSAL_s32PrintFormat( (char*)&rGnssProxyInfo.u8TransBuffer[u32MsgLength], "%x", (tS32)u32NewTeseoCfg );

   u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                  OSAL_u32StringLength(&rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG]);
   s32RetVal = (tS32)GnssProxy_s32ApndSndCfgBlk(u32MsgLength);
   
   if( s32RetVal != (tS32)OSAL_E_NOERROR )
   {
      s32RetVal = OSAL_ERROR;
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "%s Append and Sending Teseo config Error", __FUNCTION__ );
   }
   else
   {
      s32RetVal = OSAL_E_NOERROR;
   }
   
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32SetSatSysConfBlk227
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK on success
*               Appropriate osal error codes in case of failure
*
* DESCRIPTION : 1: Configure teseo application list config block 227
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.SEP.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxy_s32SetSatSysConfBlk227(tU32 u32TeseoBitMask)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32OldTeseoCfgBlk227;
   
   GnssProxy_vEnterCriticalSection(__LINE__);
   u32OldTeseoCfgBlk227 = rGnssProxyInfo.rGnssSatSysData.u32SatConfDataBlk227;
   GnssProxy_vLeaveCriticalSection(__LINE__);

   if(0 == u32OldTeseoCfgBlk227)
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else
   {
      // Copy TESEO command header to set config 
      switch(rGnssProxyInfo.rGnssConfigData.enGnssHwType)
      {
         case GNSS_HW_STA8088 :
            if(!( u32TeseoBitMask & GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE ))
            {
               if( (tS32)OSAL_E_NOERROR != ( s32RetVal = 
                    GnssProxy_s32Set227CfgMask( u32TeseoBitMask, 
                                                u32OldTeseoCfgBlk227 ) ) )
               {
                  GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                                       "Setting Cfg Blk 227 for Teseo-2 Failed" );
                  s32RetVal = OSAL_ERROR;
               }
            }
            //this case should never enter, because check is  done earlier
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                                    "Compass Sat Sys cannot be set in "
                                    "Teseo-2, Invalid Value cfg setting failed" );
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
         break;

         case GNSS_HW_STA8089 :
            if( (tS32)OSAL_E_NOERROR != ( s32RetVal = 
                 GnssProxy_s32Set227CfgMask( u32TeseoBitMask, 
                                             u32OldTeseoCfgBlk227 ) ) )
            {
               GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                                    "Setting Cfg Blk 227 for Teseo-3 Failed" );
               s32RetVal = OSAL_ERROR;
            }
         break;

         case GNSS_HW_UNKNOWN:
         default:
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                              "Invalid Teseo Fw Version, cfg setting failed" );
         break;
      }
      
   }
   return s32RetVal;
}
#ifdef GNSS_PROXY_ENABLE_RESET_GPS_ENGINE
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32ResetGpsEngine
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK on success
*               OSA_ERROR in case of failure
*
* DESCRIPTION : Resets the Teseo GPS engine. This is needed to be sure that
*               new satellite configuration is used by teseo.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.SEP.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 GnssProxy_s32ResetGpsEngine(tVoid)
{
   tS32 s32RetVal;
   tU32 u32MsgLength;

   /* Frame the INC message to reset the GPS engine */
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                      GNSS_PROXY_TESEO_RESET_GPS_ENGINE,
                      OSAL_u32StringLength(GNSS_PROXY_TESEO_RESET_GPS_ENGINE) );

   u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                  OSAL_u32StringLength(GNSS_PROXY_TESEO_RESET_GPS_ENGINE);

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_REST_TES, NULL );

   s32RetVal = GnssProxy_s32SendDataToScc(u32MsgLength);
   if ((tS32)u32MsgLength != s32RetVal)
   {
      s32RetVal = OSAL_E_UNKNOWN;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "write fail for reset cmd" );
   }
   else
   {
      s32RetVal = OSAL_E_NOERROR;
   }

   return s32RetVal;
}
#endif
/************************************************************************************
* FUNCTION    : GnssProxy_s32SaveConfigDataBlock
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_E_NOERROR on success
*               OSAL_ERROR in case of failure
*
* DESCRIPTION : The current configuration data block,
*               including changed parameters, will be stored into
*               the backup memory (NVM).
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 29.OCT.2014 | Initial version: 1.0 | Sanjay G(RBEI/ECF5)
* --------------------------------------------------------------------------------
*************************************************************************************/
tS32 GnssProxy_s32SaveConfigDataBlock(tVoid)
{
   tS32 s32RetVal;
   tU32 u32MsgLength;
   tU32 u32ResultMask;

   /* Frame the INC message to reset the GPS engine */
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                      GNSS_PROXY_TESEO_SAVE_CONFIG_DATA_BLOCK,
                      OSAL_u32StringLength( GNSS_PROXY_TESEO_SAVE_CONFIG_DATA_BLOCK ) );

   u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                  OSAL_u32StringLength( GNSS_PROXY_TESEO_SAVE_CONFIG_DATA_BLOCK );

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_SVCDBLK_SV_TES_CFG, NULL );

   s32RetVal = GnssProxy_s32SendDataToScc( u32MsgLength );
   if ( (tS32)u32MsgLength != s32RetVal )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "s32SaveCDBlock:Write failed for saving teseo config data block" );
   }
   // Wait for response from teseo.
   else if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                              ( GNSS_PROXY_TESEO_COM_SAVE_CDB_RESPONSE_MASK |
                                                GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT ),
                                              OSAL_EN_EVENTMASK_OR,
                                              GNSS_PROXY_TESEO_RESPONSE_WAIT_TIME,
                                              &u32ResultMask ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "s32SaveCDBlock:Event wait failed err %d",
                           OSAL_u32ErrorCode() );
   }
   //Clear event.
   else if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                             ~u32ResultMask,
                                             OSAL_EN_EVENTMASK_AND) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "s32SaveCDBlock:Event Post failed err %lu line %lu",
                           OSAL_u32ErrorCode(), __LINE__ );
   }
   /* Cancel operation in case of shutdown event */
   else  if ( GNSS_PROXY_EVENT_SHUTDOWN_TESEO_COM_EVENT == u32ResultMask )
   {
      s32RetVal = OSAL_E_CANCELED;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Saving config data canceled: Shutdown signaled ");
      GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELEASE_TESEO_COM_EVENT );
   }
   else
   {
      s32RetVal = OSAL_E_NOERROR;
   }

   return s32RetVal;
}

/*****************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMSAVEPAROK
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : This is positive response for $PSTMSAVEPAR.
*               A event will be posted for the application thread waiting
*               for this response.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 28.OCT.2014 | Initial version: 1.0 | Sanjay G( RBEI/ECF5 )
* -------------------------------------------------------------------------
****************************************************************************/
static tVoid GnssProxy_vHandlePSTMSAVEPAROK ( tChar const * const pcFieldIndex[] )
{

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_RCVD_PSTMSAVEPAROK, NULL );
   

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "!!! pointers to GnssProxy_vHandlePSTMSAVEPAROK are null" );
   }
   //Someone is waiting for this event
   else if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                             GNSS_PROXY_TESEO_COM_SAVE_CDB_RESPONSE_MASK,
                                             OSAL_EN_EVENTMASK_OR ) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "vHandlePSTMSAVEPAROK:Event Post failed err %lu line %lu",
                           OSAL_u32ErrorCode(), __LINE__ );
   }
   else
   {
      //Nothing
   }

}

/************************************************************************************
* FUNCTION    : GnssProxy_s32RebootTeseo
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_E_NOERROR on success
*               OSA_ERROR in case of failure
*
* DESCRIPTION : Reboots the Teseo.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 29.OCT.2014 | Initial version: 1.0 | Sanjay G(RBEI/ECF5)
* --------------------------------------------------------------------------------
*************************************************************************************/
tS32 GnssProxy_s32RebootTeseo(tVoid)
{
   tS32 s32RetVal;
   tU32 u32MsgLength;

   /* Frame the INC message to reset the GPS engine */
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                      GNSS_PROXY_TESEO_REBOOT,
                      OSAL_u32StringLength( GNSS_PROXY_TESEO_REBOOT ) );

   u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                  OSAL_u32StringLength( GNSS_PROXY_TESEO_REBOOT );

   GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_REBGPSENG_REB_TES, NULL );

   s32RetVal = GnssProxy_s32SendDataToScc( u32MsgLength );
   if ( (tS32)u32MsgLength != s32RetVal )
   {
      s32RetVal = OSAL_ERROR;
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "s32RebootGpsEngine:write failed for teseo reboot cmd" );
   }
   else
   {
      s32RetVal = OSAL_E_NOERROR;
   }

   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vStoreSatSysUsed
*
* PARAMETER   : tU32 u32SatSysConf: satellite config sent by Teseo
*
* RETURNVALUE : OSAL_OK on success
*               OSA_ERROR in case of failure
*
* DESCRIPTION : 1: This parses the config block sent by Teseo and
*                  stores it in the format understandable by VD-sensor
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.SEP.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vStoreSatSysUsed(tU32 u32SatSysConf, tU32 u32BlkID)
{
   if( u32BlkID == GNSS_PROXY_APP_LIST_BLOCK_ID_200 )
   {
      rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed &= GNSS_PROXY_CLR_BLK_200_SAT_SYS;
   }
   else if ( u32BlkID == GNSS_PROXY_APP_LIST_BLOCK_ID_227 )
   {
      rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed &= GNSS_PROXY_CLR_BLK_227_SAT_SYS;
   }

   if(0 != u32SatSysConf)
   {
      if( (GNSS_PROXY_TESEO_BIT_MASK_GPS_ENABLE & u32SatSysConf) ==
           GNSS_PROXY_TESEO_BIT_MASK_GPS_ENABLE )
      {
         rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed |= OSAL_C_U8_GNSS_SATSYS_GPS;
      }
      if( (GNSS_PROXY_TESEO_BIT_MASK_GLONASS_ENABLE & u32SatSysConf) ==
           GNSS_PROXY_TESEO_BIT_MASK_GLONASS_ENABLE )
      {
         rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed |= OSAL_C_U8_GNSS_SATSYS_GLONASS;
      }
      if( (GNSS_PROXY_TESEO_BIT_MASK_QZSS_ENABLE & u32SatSysConf) ==
           GNSS_PROXY_TESEO_BIT_MASK_QZSS_ENABLE )
      {
         rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed |= OSAL_C_U8_GNSS_SATSYS_QZSS;
      }
      if( (GNSS_PROXY_TESEO_BIT_MASK_GALILEO_ENABLE & u32SatSysConf) ==
           GNSS_PROXY_TESEO_BIT_MASK_GALILEO_ENABLE )
      {
         rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed |= OSAL_C_U8_GNSS_SATSYS_GALILEO;
      }
      if( (GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE & u32SatSysConf) ==
           GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE )
      {
         rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed |= OSAL_C_U8_GNSS_SATSYS_COMPASS;
      }
      if( (GNSS_PROXY_TESEO_BIT_MASK_SBAS_ENABLE & u32SatSysConf) ==
           GNSS_PROXY_TESEO_BIT_MASK_SBAS_ENABLE )
      {
         rGnssProxyInfo.rGnssSatSysData.u32SatSysUsed |= OSAL_C_U8_GNSS_SATSYS_SBAS;
      }
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                           "%s:!!!!Zero received for sat config", __FUNCTION__ );
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_u32FrameSatSysBlk227
*
* PARAMETER   : u32SatSysConfig: Sat System Cfg received from TESEO
*
* RETURNVALUE : tU32-> Satellite system to be set in format needed by Teseo.for config block 227
*
* DESCRIPTION :
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 27.JUN.2016 | Initial version: 1.0 | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 GnssProxy_u32FrameSatSysBlk227(tU32 u32SatSysConfig)
{
   tU32 u32SatSysCfgBlk = 0;
   
   if( OSAL_C_U8_GNSS_SATSYS_GALILEO & u32SatSysConfig )
   {
      u32SatSysCfgBlk |= GNSS_PROXY_TESEO_BIT_MASK_GALILEO_ENABLE;
   }
   if( OSAL_C_U8_GNSS_SATSYS_COMPASS & u32SatSysConfig  )
   {
      if( rGnssProxyInfo.rGnssConfigData.enGnssHwType == GNSS_HW_STA8089 )
      {
         u32SatSysCfgBlk |= GNSS_PROXY_TESEO_BIT_MASK_COMPASS_ENABLE;
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                              "Teseo-2 Chip, Compass is not supported");
      }
   }
   
   return u32SatSysCfgBlk;
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_u32FrameSatSysBlk200
*
* PARAMETER   : u32SatSysConfig: Sat System Cfg received from TESEO
*
* RETURNVALUE : tU32-> Satellite system to be set in format needed by Teseo.for config block 200
*
* DESCRIPTION :
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 27.JUN.2016 | Initial version: 1.0 | Srinivas Anvekar (RBEI/ECF1)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/

static tU32 GnssProxy_u32FrameSatSysBlk200(tU32 u32SatSysConfig)
{
   tU32 u32SatSysCfgBlk = 0;

   /* Get the satellite system to be enabled */
   if( OSAL_C_U8_GNSS_SATSYS_GPS & u32SatSysConfig )
   {
      u32SatSysCfgBlk |= GNSS_PROXY_TESEO_BIT_MASK_GPS_ENABLE;
   }
   if( OSAL_C_U8_GNSS_SATSYS_SBAS & u32SatSysConfig )
   {
      u32SatSysCfgBlk |= GNSS_PROXY_TESEO_BIT_MASK_SBAS_ENABLE;
   }
   if( OSAL_C_U8_GNSS_SATSYS_GLONASS & u32SatSysConfig )
   {
      u32SatSysCfgBlk |= GNSS_PROXY_TESEO_BIT_MASK_GLONASS_ENABLE;
   }
   if ( OSAL_C_U8_GNSS_SATSYS_QZSS & u32SatSysConfig )
   {
      u32SatSysCfgBlk |= GNSS_PROXY_TESEO_BIT_MASK_QZSS_ENABLE;
   }

   return u32SatSysCfgBlk;
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32GetTeseoFwVer
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_OK on success
*                         OSAL_ERROR on Failure
*
* DESCRIPTION : 1: Send command to get teseo firmware version.
*               2: Wait for response with timeout
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 16.SEP.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
   tS32 GnssProxy_s32GetTeseoFwVer()
   {
      tU32 u32MsgLength;
      tU32 u32ResultMask;
   
      tS32 s32RetVal = OSAL_ERROR;
   
      rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
      rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
      OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                         GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY,
                         OSAL_u32StringLength(GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY) );
   
      u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER +
                     OSAL_u32StringLength(GNSS_PROXY_TESEO_BIN_IMAGE_VER_QUERY);
   
      if ((tS32)u32MsgLength != GnssProxy_s32SendDataToScc(u32MsgLength))
   
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "write fail for binary version Query cmd" );
      }
      /* Wait for response from ST */
      else if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                 (GNSS_PROXY_TESEO_FIRMWARE_VERSION_WAIT_EVENT ),
                                                 OSAL_EN_EVENTMASK_OR,
                                                 GNSS_PROXY_TESEO_RESPONSE_WAIT_TIME,
                                                 &u32ResultMask ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Firmware version req:Event wait failed err %d",
                                                OSAL_u32ErrorCode() );
      }
      /* Clear event. */
      else
      {
         if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                   ~u32ResultMask,
                                                   OSAL_EN_EVENTMASK_AND) )
         {
             GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "cleanup:Event Post failed err %lu line %lu",
                                                    OSAL_u32ErrorCode(), __LINE__ );
         }
         else if( GNSS_PROXY_TESEO_FIRMWARE_VERSION_WAIT_EVENT != u32ResultMask )
         {
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "GNSS:FW EVENT Wait: Un expected Event %x",
                                                                        u32ResultMask );
         }
         else
         {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_BIN_VER_WT_SUC, NULL );
            s32RetVal = OSAL_OK;
   
         }
      }
      return s32RetVal;
   }


/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32GetTeseoCRC
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_OK on success
*                         OSAL_ERROR on Failure
*
* DESCRIPTION : 1: Send command to get teseo CRC.
*               2: Wait for response with timeout
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 06.AUG.2013 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32GetTeseoCRC()
{
   tU32 u32MsgLength;
   tU32 u32ResultMask;
   tS32 s32RetVal;
   tPChar pcCrcCmd = OSAL_NULL;

   //! choose NMEA comand for CRC based on teseo variant type.
   if ( GNSS_HW_STA8089 == rGnssProxyInfo.rGnssConfigData.enGnssHwType )
   {
      pcCrcCmd = GNSS_PROXY_TESEO_CRC_CHECK_CMD_TESEO_3;
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE,
                                     "GnssProxy_s32GetTeseoCRC:getting crc for teseo3" );
   }
   else if ( GNSS_HW_STA8088 == rGnssProxyInfo.rGnssConfigData.enGnssHwType )
   {
      pcCrcCmd = GNSS_PROXY_TESEO_CRC_CHECK_CMD_TESEO_2;
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_DEF_TRC_RULE,
                                     "GnssProxy_s32GetTeseoCRC:getting crc for teseo2" );
   }
   else
   {
      rGnssProxyInfo.rGnssConfigData.enGnssHwType = GNSS_HW_UNKNOWN;
      pcCrcCmd = "\r\n";   //dummy carriage return and new line to remove lint warning
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                           "GnssProxy_s32GetTeseoCRC: unknown teseo variant" );
   }

   if( (GNSS_HW_STA8088 == rGnssProxyInfo.rGnssConfigData.enGnssHwType) || 
       (GNSS_HW_STA8089 == rGnssProxyInfo.rGnssConfigData.enGnssHwType) )
   {
      rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
      rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_DATA_MSGID;
      OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                         pcCrcCmd,
                         OSAL_u32StringLength(pcCrcCmd) );

      u32MsgLength = GNSS_PROXY_SIZE_OF_C_DATA_MSG_HEADER + OSAL_u32StringLength(pcCrcCmd);

      //! send the composed command to scc.
      s32RetVal = GnssProxy_s32SendDataToScc(u32MsgLength);
      if ((tS32)u32MsgLength != s32RetVal)
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "write fail for crc check cmd" );
         s32RetVal = OSAL_ERROR;
      }
      /* Wait for response from ST */
      else
      {
         /* FLASH DATA OK message will be received only in firmware update mode */
         if( OSAL_EN_WRITEONLY == rGnssProxyInfo.enAccessMode )
         {
            /* Wait for FLASH_DATA OK response from V850*/
            if (OSAL_ERROR == GnssProxy_s32WaitForMsg( GNSS_PROXY_FW_FLASH_DATA_OK,
                                                            GNSS_PROXY_FW_TESEO_ACK_WAIT_TIME ))
            {
               GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "!!!Res:CRC FLASH_DATA_OK Failed" );
            }
         }
   
         if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                    (GNSS_PROXY_TESEO_CRC_CHECK_WAIT_EVENT ),
                                                    OSAL_EN_EVENTMASK_OR,
                                                    GNSS_PROXY_TESEO_RESPONSE_WAIT_TIME,
                                                    &u32ResultMask ))
         {
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Crc check:Event wait failed err %d",
                                                   OSAL_u32ErrorCode() );
            s32RetVal = OSAL_ERROR;
         }
         /* Clear event. */
         else if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                                   ~u32ResultMask,
                                                   OSAL_EN_EVENTMASK_AND) )
         {
             GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "cleanup:Event Post failed err %lu line %lu",
                                                    OSAL_u32ErrorCode(), __LINE__ );
             s32RetVal = OSAL_ERROR;
         }
         else if ( GNSS_PROXY_TESEO_CRC_CHECK_WAIT_EVENT != u32ResultMask )
         {
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "CRC response Wait Failed" );
            s32RetVal = OSAL_ERROR;
         }
         else
         {
            GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_CRCCHK_WT_SUC, NULL );
            s32RetVal = OSAL_OK;
         }
      }
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  
                           "GnssProxy_s32GetTeseoCRC: Unknown Teseo Variant, GetCRC Failed");
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMPV
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : Parses PSTMPV Message and populates Velocity and covariance information.
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 23.Aug.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandlePSTMPV ( tChar const * const pcFieldIndex[] )
{
   OSAL_trGnssPVTData *prPVTData = &(rGnssProxyData.rGnssTmpRecord.rPVTData);

   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_RCVD_PSTMPV, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!! pointers to GnssProxy_vHandlePSTMPV are null" );
   }
   else
   {

      /*---------------------Update the elements of receiver velocity-----------------------------------------------------*/
      /* Velocity along north */
      prPVTData->f32VelocityNorth = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_VEL_NORTH],
                         OSAL_NULL );
      /* Velocity along east */
      prPVTData->f32VelocityEast = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_VEL_EAST],
                         OSAL_NULL );
      /* Velocity along vertical */
      prPVTData->f32VelocityUp = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_VEL_VERTICAL],
                         OSAL_NULL );

      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_PSTMPV_RCVR_VEL, "\t %08.3f , %08.3f , %08.3f",
                                             prPVTData->f32VelocityNorth,
                                             prPVTData->f32VelocityEast,
                                             prPVTData->f32VelocityUp );

      /*---------------------Update the elements of Position covariance matrix-------------------------------------------*/

      /* Standard Deviation of position along north direction */
      prPVTData->rPositionCovarianceMatrix.f32Elem0 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_POS_COV_NORTH],
                         OSAL_NULL );
      /* Variance = (standard deviation * standard deviation) */
      prPVTData->rPositionCovarianceMatrix.f32Elem0 *= prPVTData->rPositionCovarianceMatrix.f32Elem0;

      /* Standard Deviation of position along north-east direction */
      prPVTData->rPositionCovarianceMatrix.f32Elem4 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_POS_COV_NORTH_EAST],
                         OSAL_NULL );
      prPVTData->rPositionCovarianceMatrix.f32Elem4 *= prPVTData->rPositionCovarianceMatrix.f32Elem4;

      /* Standard Deviation of position along north-Vertical direction */
      prPVTData->rPositionCovarianceMatrix.f32Elem8 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_POS_COV_NORTH_VERTICAL],
                         OSAL_NULL );
      prPVTData->rPositionCovarianceMatrix.f32Elem8 *= prPVTData->rPositionCovarianceMatrix.f32Elem8;

      /* Standard Deviation of position along east direction */
      prPVTData->rPositionCovarianceMatrix.f32Elem5 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_POS_COV_EAST],
                         OSAL_NULL );
      prPVTData->rPositionCovarianceMatrix.f32Elem5 *= prPVTData->rPositionCovarianceMatrix.f32Elem5;

      /* Standard Deviation of position along east-Vertical direction */
      prPVTData->rPositionCovarianceMatrix.f32Elem9 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_POS_COV_EAST_VERTICAL],
                         OSAL_NULL );
      prPVTData->rPositionCovarianceMatrix.f32Elem9 *= prPVTData->rPositionCovarianceMatrix.f32Elem9;

      /* Standard Deviation of position along vertical direction */
      prPVTData->rPositionCovarianceMatrix.f32Elem10 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_POS_COV_VERTICAL],
                         OSAL_NULL );
      prPVTData->rPositionCovarianceMatrix.f32Elem10 *= prPVTData->rPositionCovarianceMatrix.f32Elem10;

      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_PSTMPV_POS_COV_MAT_1, "\t %08.3f , %08.3f , %08.3f",
                                             prPVTData->rPositionCovarianceMatrix.f32Elem0,
                                             prPVTData->rPositionCovarianceMatrix.f32Elem4,
                                             prPVTData->rPositionCovarianceMatrix.f32Elem8 );
	  
     GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_PSTMPV_POS_COV_MAT_2, "\t %08.3f , %08.3f , %08.3f",
                                             prPVTData->rPositionCovarianceMatrix.f32Elem5,
                                             prPVTData->rPositionCovarianceMatrix.f32Elem9,
                                             prPVTData->rPositionCovarianceMatrix.f32Elem10 );

      /*---------------------Update the elements of velocity covariance matrix-------------------------------------------*/

      /* Standard Deviation of velocity along north direction */
      prPVTData->rVelocityCovarianceMatrix.f32Elem0 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_NORTH],
                         OSAL_NULL );
      prPVTData->rVelocityCovarianceMatrix.f32Elem0 *= prPVTData->rVelocityCovarianceMatrix.f32Elem0;

      /* Standard Deviation of velocity along north-east direction */
      prPVTData->rVelocityCovarianceMatrix.f32Elem4 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_NORTH_EAST],
                         OSAL_NULL );
      prPVTData->rVelocityCovarianceMatrix.f32Elem4 *= prPVTData->rVelocityCovarianceMatrix.f32Elem4;

      /* Standard Deviation of velocity along north-Vertical direction */
      prPVTData->rVelocityCovarianceMatrix.f32Elem8 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_NORTH_VERTICAL],
                         OSAL_NULL );
      prPVTData->rVelocityCovarianceMatrix.f32Elem8 *= prPVTData->rVelocityCovarianceMatrix.f32Elem8;

      /* Standard Deviation of velocity along east direction */
      prPVTData->rVelocityCovarianceMatrix.f32Elem5 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_EAST],
                         OSAL_NULL );
      prPVTData->rVelocityCovarianceMatrix.f32Elem5 *= prPVTData->rVelocityCovarianceMatrix.f32Elem5;

      /* Standard Deviation of velocity along east-Vertical direction */
      prPVTData->rVelocityCovarianceMatrix.f32Elem9 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_EAST_VERTICAL],
                         OSAL_NULL );
      prPVTData->rVelocityCovarianceMatrix.f32Elem9 *= prPVTData->rVelocityCovarianceMatrix.f32Elem9;

      /* Standard Deviation of velocity along vertical direction */
      prPVTData->rVelocityCovarianceMatrix.f32Elem10 = (tF32)OSAL_dStringToDouble(
                         pcFieldIndex[GNSS_PROXY_PSTMPV_OFFSET_VEL_COV_VERTICAL],
                         OSAL_NULL );
      prPVTData->rVelocityCovarianceMatrix.f32Elem10 *= prPVTData->rVelocityCovarianceMatrix.f32Elem10;

      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_PSTMPV_VEL_COV_MAT_1, "\t %08.3f , %08.3f , %08.3f",
                                             prPVTData->rVelocityCovarianceMatrix.f32Elem0,
                                             prPVTData->rVelocityCovarianceMatrix.f32Elem4,
                                             prPVTData->rVelocityCovarianceMatrix.f32Elem8 );
	  
     GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_PSTMPV_VEL_COV_MAT_2, "\t %08.3f , %08.3f , %08.3f",
                                             prPVTData->rVelocityCovarianceMatrix.f32Elem5,
                                             prPVTData->rVelocityCovarianceMatrix.f32Elem9,
                                             prPVTData->rVelocityCovarianceMatrix.f32Elem10 );

      /* Make a note that PSTMPV is received for current fix */
      rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs |= GNSS_PROXY_PSTMPV_ID;
   }
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vHandlePSTMPVQ
*
* PARAMETER   : *pcFieldIndex[] ->  Array of pointers to indexed data buffer
*
* RETURNVALUE :
*
* DESCRIPTION : parses the PSTMPVQ message for position and noise matrix values
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 23.Aug.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vHandlePSTMPVQ ( tChar const * const pcFieldIndex[] )
{
   OSAL_trGnssPVTData *prPVTData = &(rGnssProxyData.rGnssTmpRecord.rPVTData);

   GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_RCVD_PSTMPVQ, NULL );

   if ( OSAL_NULL == pcFieldIndex )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "!!! pointers to GnssProxy_vHandlePSTMPVQ are null" );
   }
   else
   {

      /*---------------------Update the elements of Position north processing noise [m]------------------------*/
      /* Noise along north */
      prPVTData->rPositionNoiseMatrix.f32Elem0 = (tF32)OSAL_dStringToDouble(
                                                         pcFieldIndex[GNSS_PROXY_PSTMPVQ_OFFSET_P_Q_N],
                                                         OSAL_NULL );
      /* Noise along East */
      prPVTData->rPositionNoiseMatrix.f32Elem5 = (tF32)OSAL_dStringToDouble(
                                                         pcFieldIndex[GNSS_PROXY_PSTMPVQ_OFFSET_P_Q_E],
                                                         OSAL_NULL );
      /* Noise along Vertical */
      prPVTData->rPositionNoiseMatrix.f32Elem10 = (tF32)OSAL_dStringToDouble(
                                                         pcFieldIndex[GNSS_PROXY_PSTMPVQ_OFFSET_P_Q_V],
                                                         OSAL_NULL );

      /*---------------------Update the elements of Velocity north processing noise [m/s]------------------------*/
      /* Noise along north */
      prPVTData->rVelocityNoiseMatrix.f32Elem0 = (tF32)OSAL_dStringToDouble(
                                                        pcFieldIndex[GNSS_PROXY_PSTMPVQ_OFFSET_V_Q_N],
                                                        OSAL_NULL );
      /* Noise along East */
      prPVTData->rVelocityNoiseMatrix.f32Elem5 = (tF32)OSAL_dStringToDouble(
                                                        pcFieldIndex[GNSS_PROXY_PSTMPVQ_OFFSET_V_Q_E],
                                                        OSAL_NULL );
      /* Noise along Vertically - up */
      prPVTData->rVelocityNoiseMatrix.f32Elem10 = (tF32)OSAL_dStringToDouble(
                                                        pcFieldIndex[GNSS_PROXY_PSTMPVQ_OFFSET_V_Q_V],
                                                        OSAL_NULL );

      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_PSTMPVQ_POS_NS_MATRIX,"\t %08.3f , %08.3f , %08.3f",
                                            prPVTData->rPositionNoiseMatrix.f32Elem0,
                                            prPVTData->rPositionNoiseMatrix.f32Elem5,
                                            prPVTData->rPositionNoiseMatrix.f32Elem10 );
	  
     GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_PSTMPVQ_VEL_NS_MATRIX,"\t %08.3f , %08.3f , %08.3f",
                                            prPVTData->rVelocityNoiseMatrix.f32Elem0,
                                            prPVTData->rVelocityNoiseMatrix.f32Elem5,
                                            prPVTData->rVelocityNoiseMatrix.f32Elem10 );

      /* Make a note that PSTMPVQ is received for current fix */
      rGnssProxyData.rGnssTmpFixInfo.u32RecvdGnssMsgs |= GNSS_PROXY_PSTMPVQ_ID;
   }
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32GetNmeaRecvdList
*
* PARAMETER   : tPU32 pu32NmeaList: pointer to U32 type to store received NMEA List
*
* RETURNVALUE : OSAL_E_NOERROR on Success or
*               Appropriate OSAL_ERROR codes
*
* DESCRIPTION : Waits for one complete Fix info and fills application buffer
*               with the list of NMEA message received.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 27.Dec.2013 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32GetNmeaRecvdList( tPU32 pu32NmeaList )
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32ResultMask = 0;

   if ( OSAL_NULL == pu32NmeaList )
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else
   {
      /* Wait for one complete Fix info */
      if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyReadEvent,
                                            (GNSS_PROXY_EVENT_DATA_READY | GNSS_PROXY_EVENT_SHUTDOWN_READ_EVENT),
                                            OSAL_EN_EVENTMASK_OR,
                                            rGnssProxyInfo.u16ReadWaitTimeMs,
                                            &u32ResultMask ))
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event wait failed err %d line %d",
                                                OSAL_u32ErrorCode(),__LINE__ );
         s32RetVal = (tS32)OSAL_u32ErrorCode();
      }
      else
      {
         /* Event wait returned with a success, Clear the event */
         if(OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyReadEvent,
                                                  (OSAL_tEventMask) ~u32ResultMask,
                                                  OSAL_EN_EVENTMASK_AND))
         {
             GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Event Post failed err %lu line %d",
                                                    OSAL_u32ErrorCode(), __LINE__ );
         }
         /*  */
         if ( GNSS_PROXY_EVENT_DATA_READY != u32ResultMask )
         {
            s32RetVal = OSAL_E_UNKNOWN;
         }
         else
         {
            GnssProxy_vEnterCriticalSection(__LINE__);
            *pu32NmeaList = rGnssProxyData.u32NmeaMsgsRecvd;
            GnssProxy_vLeaveCriticalSection(__LINE__);
         }
      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_u32AddSatSysOffset
*
* PARAMETER   : tU32 u32RawSatId: SatID to be converted to Sensor FI requirements
*
* RETURNVALUE : Converted SatID
*
* DESCRIPTION :Converts Teseo satellite ID ranges to Sensor FI ranges.
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 09.APR.2014 | Initial version: 1.0 | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* 27.JUN.2016 | Version: 1.0          | Srinivas Anvekar (RBEI/ECF1)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 GnssProxy_u32AddSatSysOffset( tU32 u32RawSatId )
{
   tU32 u32ProcessedSatID = 0;
   
   if( GNSS_PROXY_TESEO_GPS_MIN_SAT_ID <= u32RawSatId && GNSS_PROXY_TESEO_GPS_MAX_SAT_ID >= u32RawSatId )
   {
      u32ProcessedSatID = u32RawSatId + GNSS_PROXY_GPS_SAT_ID_OFFSET;
   }
   else if ( GNSS_PROXY_TESEO_SBAS_MIN_SAT_ID <= u32RawSatId && GNSS_PROXY_TESEO_SBAS_MAX_SAT_ID >= u32RawSatId )
   {
      u32ProcessedSatID = u32RawSatId + GNSS_PROXY_SBAS_SAT_ID_OFFSET;
   }
   else if ( GNSS_PROXY_TESEO_GLONASS_MIN_SAT_ID <= u32RawSatId && GNSS_PROXY_TESEO_GLONASS_MAX_SAT_ID >= u32RawSatId )
   {
      u32ProcessedSatID = u32RawSatId + GNSS_PROXY_GLONASS_SAT_ID_OFFSET;
   }
   else if ( GNSS_PROXY_TESEO_BAIDEU_MIN_SAT_ID <= u32RawSatId && GNSS_PROXY_TESEO_BAIDEU_MAX_SAT_ID >= u32RawSatId )
   {
      u32ProcessedSatID = u32RawSatId + GNSS_PROXY_BAIDEU_SAT_ID_OFFSET;
   }
   else if ( GNSS_PROXY_TESEO_QZSS_MIN_SAT_ID_SET1 <= u32RawSatId && GNSS_PROXY_TESEO_QZSS_MAX_SAT_ID_SET1 >= u32RawSatId )
   {
      u32ProcessedSatID = u32RawSatId + GNSS_PROXY_QZSS_SAT_ID_OFFSET_SET1;
   }
   else if ( GNSS_PROXY_TESEO_QZSS_MIN_SAT_ID_SET2 <= u32RawSatId && GNSS_PROXY_TESEO_QZSS_MAX_SAT_ID_SET2 >= u32RawSatId )
   {
      u32ProcessedSatID = u32RawSatId + GNSS_PROXY_QZSS_SAT_ID_OFFSET_SET2;
   }
   else if ( GNSS_PROXY_TESEO_GALILEO_MIN_SAT_ID <= u32RawSatId && GNSS_PROXY_TESEO_GALILEO_MAX_SAT_ID >= u32RawSatId )
   {
      u32ProcessedSatID = (tU32)((tS32)u32RawSatId + GNSS_PROXY_GALILEO_SAT_ID_OFFSET);
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,
                              "Unknown sat system satid:%d", u32RawSatId );
      u32ProcessedSatID = u32RawSatId;
   }

   return u32ProcessedSatID;
}


/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32FlushSccBuff
*
* PARAMETER   : None
*
* RETURNVALUE : OSAL_OK on success. OSAL_ERROR on failure
*
* DESCRIPTION : Sends GNSS_FLUSH_SENSOR_DATA_REQ Msg to SCC
*
* HISTORY     :
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
* -------------------------------------------------------------------------
* 21.NOV.2014 | Initial version: 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32FlushSccBuff( tVoid )
{
   tS32 s32RetVal;
   tU32 u32ResultMask = 0;
   

   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_TP_MARKER] = GNSS_PROXY_INC_TP_MARKER_HEADER;
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = GNSS_PROXY_SCC_C_CONFIG_MSGID;
   rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_C_CTRL_MSG_SRV_TYPE] = 
                                                            GNSS_PROXY_SRV_TYPE_FLUSH_SENSOR_DATA;

   s32RetVal = GnssProxy_s32SendDataToScc(GNSS_PROXY_SIZE_OF_C_FLUSH_DATA_MSG_BUFF);
   if ((tS32)GNSS_PROXY_SIZE_OF_C_FLUSH_DATA_MSG_BUFF != s32RetVal)
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:write fail for Flush Buff cmd" );
      s32RetVal = OSAL_ERROR;
   }
   /* Wait for response from SCC */
   else if ( OSAL_ERROR == OSAL_s32EventWait( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                              (GNSS_PROXY_FLUSH_SENSOR_BUFF_SUCCESS_EVENT|GNSS_PROXY_FLUSH_SENSOR_BUFF_FAILURE_EVENT ),
                                              OSAL_EN_EVENTMASK_OR,
                                              GNSS_PROXY_FLUSH_SENSOR_BUFFER_WAIT_TIME,
                                              &u32ResultMask ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Gnss: FlushBuff Event wait failed err %d",
                                             OSAL_u32ErrorCode() );
      s32RetVal = OSAL_ERROR;
   }
   /* Clear event. */
   else if( OSAL_ERROR == OSAL_s32EventPost( rGnssProxyInfo.hGnssProxyTeseoComEvent,
                                             ~u32ResultMask,
                                             OSAL_EN_EVENTMASK_AND) )
   {
       GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Gnss: cleanup FlushBuff Event Post failed err %lu line %lu",
                                              OSAL_u32ErrorCode(), __LINE__ );
       s32RetVal = OSAL_ERROR;
   }
   else if( GNSS_PROXY_FLUSH_SENSOR_BUFF_SUCCESS_EVENT == u32ResultMask )
   {
      GnssProxy_vTraceOut( TR_LEVEL_COMPONENT, GNSSPXY_FLSHBUF_WT_SUC, NULL );
      s32RetVal = OSAL_OK;
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "FlushBuff Event fail %x", u32ResultMask );
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;

}

/***********************************************End of file**********************************************************/

