/*********************************************************************************************************************
* FILE        : dev_gnss_trace.c
*
* DESCRIPTION : This file is a part of GNSS Proxy driver module.
*               This contain tracing functionality for GNSS Proxy module.
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 28.AUG.2013 | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -----------------------------------------------------------------------------------------------
* 03.APR.2014 | Initial version: 1.1   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                        | Added automatic errmem logging for FATAL traces
* -----------------------------------------------------------------------------------------------
*********************************************************************************************************************/


/*************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#include "dev_gnss_types.h"
#include "dev_gnss_trace.h"

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vTraceOut
*
* PARAMETER   : u32Level -> Trace level
*               enGnssPxyTrcVal -> enum value for trace rule
*               pcFormatString -> Trace string
* 
* RETURNVALUE : NONE
*
* DESCRIPTION : Trace function for GNSS Proxy driver
*
* HISTORY     :
*------------------------------------------------------------------------------
* Date        |       Version       | Author & comments
*-------------|---------------------|-------------------------------------
* 22.APR.2013 | Initial version: 1.0| Niyatha S Rao (RBEI/ECF5)
* ---------------------------------------------------------------------------------
* 04.APR.2015 | Version: 1.1| Srinivas P Anvekar (RBEI/ECF5)
* ---------------------------------------------------------------------------------
*********************************************************************************************************************/


tVoid GnssProxy_vTraceOut(TR_tenTraceLevel enLevel,
      enGNSSPXYTRC enGnssPxyTrcVal,const tChar *pcFormatString,...)
{
   
   /* Buffer to hold the string to trace out */
   
   tU8 u8Buffer[MAX_TRACE_SIZE] = { 0 };
   
   if( LLD_bIsTraceActive((tU32)OSAL_C_TR_CLASS_DEV_GNSS, (tU32)enLevel) != FALSE )
   {

      
      tS32 s32Size = 2;           //initialized to 2 because enum value is 
                                  //of two bytes which is stored in the buffer
      
      /*
      Parameter to hold the argument for a function, 
      specified the format string in pcFormatString
      defined as: typedef char* va_list in stdarg.h
      */
   
      /*
      vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure
      */ 

      va_list argList = {0};

      /*
      Position in buffer from where the format string is to be
      concatenated
      */

      tS8* ps8Buffer = (tS8*)&u8Buffer[2];

      /* Flush the String */

      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );// To satisfy lint

      /* Copy the String to indicate the trace is from the RTC device */

      /*
      Initialize the argList pointer to the beginning of the variable
      argument list
      */

      (tVoid)OSAL_pvMemoryCopy( u8Buffer, (tU8*)&enGnssPxyTrcVal, 2) ;

      if( pcFormatString != OSAL_NULL )
      {

         va_start( argList, pcFormatString ); /*lint !e718 */

         /*
         Collect the format String's content into the remaining part of
         the Buffer
         */

         if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,(sizeof(u8Buffer))-2,
                                                 pcFormatString, argList ) ) )
         {
            return;
         }

         va_end(argList);
         s32Size += 2;

      }

      /* Trace out the Message to TTFis */
      LLD_vTrace( (tU32)OSAL_C_TR_CLASS_DEV_GNSS, (tU32)enLevel,    /* Send string to Trace*/
                                      u8Buffer, ((tU32)s32Size) );

      /*
      Performs the appropriate actions to facilitate a normal return by a
      function that has used the va_list object
      */

      if ( TR_LEVEL_FATAL == enLevel )
      {
         GnssProxy_vErrmemLog( (const char*)u8Buffer, s32Size );
      }

   }
}

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_vErrmemLog
*
* PARAMETER   : tCString csErrmemLog: String to be logged
*
* RETURNVALUE : None
*
* DESCRIPTION : Log Into error memory
*
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
*-------------|-----------------------|-------------------------------------
* 01.APR.2014 | Initial version: 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid GnssProxy_vErrmemLog( const char* pu8Buffer, tS32 s32Size )
{

/*

         NO "TR_LEVEL_FATAL" TRACING ALLOWED FROM THIS FUNCTION. 
         
         ***************TR_LEVEL_FATAL****NOT***ALLOWED***************
         
         Or else this will result in unconditional recursion and stack overflow.


*/

//#define ENABLE_TRACING

   trErrmemEntry rErrmemEntry;
   rErrmemEntry.eEntryType = eErrmemEntryFatal;
   rErrmemEntry.u16Entry = OSAL_C_TR_CLASS_DEV_GNSS;
   OSAL_tIODescriptor hErrmemDev;
   
   if ( OSAL_NULL == pu8Buffer )
   {
#ifdef ENABLE_TRACING
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "!!!NULL POINTER at line %lu", __LINE__ );
#endif
   }
   else
   {

      rErrmemEntry.u16EntryLength = (tU16)s32Size;
      if( (ERRMEM_MAX_ENTRY_LENGTH) < rErrmemEntry.u16EntryLength )
      {
         rErrmemEntry.u16EntryLength = ERRMEM_MAX_ENTRY_LENGTH;
      }

      OSAL_pvMemoryCopy( (tPVoid)rErrmemEntry.au8EntryData,
                          (tPVoid)pu8Buffer,
                          rErrmemEntry.u16EntryLength );

      hErrmemDev = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
      if ( OSAL_ERROR == hErrmemDev )
      {
#ifdef ENABLE_TRACING
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "Errmem open fail %lu", OSAL_u32ErrorCode() );
#endif
      }
      else
      {
         if ( (tS32)sizeof(trErrmemEntry) != 
               OSAL_s32IOWrite( hErrmemDev, (tPVoid)&rErrmemEntry , sizeof(trErrmemEntry) ))
         {
#ifdef ENABLE_TRACING
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "Errmem write fail %lu", OSAL_u32ErrorCode() );
#endif
         }

         if ( OSAL_ERROR == OSAL_s32IOClose(hErrmemDev) )
         {
#ifdef ENABLE_TRACING
            GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "Errmem close fail %lu", OSAL_u32ErrorCode() );
#endif
         }
      }
   }

}


/***********************************************End of file**********************************************************/
