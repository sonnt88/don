/*********************************************************************************************************************
* FILE        : dev_gnss_scc_com.c
*
* DESCRIPTION : This file is a part of GNSS Proxy driver module.
*               This contain all SCC-IMX communication implementation.
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 28.AUG.2013 | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -----------------------------------------------------------------------------------------------
*********************************************************************************************************************/

/*************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#include "dev_gnss_types.h"
#include "dev_gnss_scc_com.h"
#include "dev_gnss_trace.h"
#include "dev_gnss_parser.h"

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
//#include <poll.h>
//#include<errno.h>

/* inet_pton() */
#include <arpa/inet.h>

#include "inc.h"
#include "dev_gps_main.h"
#include "inc_ports.h"

/*************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/
extern trGnssProxyInfo rGnssProxyInfo;

/*************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
#ifdef GNSS_PROXY_REMOTE_TESEO_CONROL_FEATURE
static tVoid GnssProxy_vTesCtrThread(tPVoid pvArg);
static tVoid GnssProxy_vInitTesCntFea (tVoid);
#endif

#ifdef GNSS_PROXY_TEST_STUB_ACTIVE
static int dgram_recv(int s32ConFD, void *pvDataBuff, tU32 u32Length);
tS32 GnssProxy_s32GetDataFromStub( tU32 u32Bytes );

#endif


/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32GetDataFromStub
*
* PARAMETER   : NONE
*
* RETURNVALUE : Number of bytes received on success
*                        -1 on Receive Failure
*                          0 on Timeout
*
* DESCRIPTION : This is a wrapper over the function used to Get data from SCC stub
*                         poll() is used to avoid permanent blocking of the caller
*
* HISTORY     :
*-----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 13.APR.2016 | Initial version: 1.0   | Kulkarni Ramchandra (RBEI/ECF5)
* ---------------------------------------------------------------------------
*********************************************************************************************************************/

#ifdef GNSS_PROXY_TEST_STUB_ACTIVE
tS32 GnssProxy_s32GetDataFromStub( tU32 u32Bytes )
{
   tS32 s32RetVal = -1;
   struct pollfd fds;

   //To satisfy lint
   (tVoid)s32RetVal;

   fds.fd = rGnssProxyInfo.s32SocketFD;
   fds.events = POLLIN;

   //clearing the flag
   fds.revents = 0;
   nfds_t nfds = GNSS_PROXY_POLL_NUM_OF_FDS;

   s32RetVal = poll( &fds, nfds, GNSS_PROXY_POLL_TIMEOUT_MS );
   // If shutdown flag is set, return
   if ( TRUE ==  rGnssProxyInfo.bShutdownFlag )
   {
      s32RetVal =  -1;
   }
   // Timeout occoured
   else if ( 0 == s32RetVal )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                           "GNSS: No data received from stub in %lu millisec", 
                           GNSS_PROXY_POLL_TIMEOUT_MS);
   }
   else if( 0 > s32RetVal )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS: stub poll() failed. errno: %d", errno );
   }
   // check if event received is for data
   else if ( fds.revents & POLLIN )
   {
      s32RetVal =  dgram_recv( rGnssProxyInfo.s32SocketFD ,
                         (void *)rGnssProxyInfo.u8RecvBuffer,
                         (size_t)u32Bytes );
      if ( 0 >= s32RetVal )
      {
         GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
                              "GNSS: stub dgram_recv failed. Line: %d, RetVal: %d, errno : %d", 
                              __LINE__, s32RetVal, errno );
      }
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
                           "GNSS: stub poll() returned invalid event: %x", 
                           fds.revents );
      s32RetVal = -1;
   }
   return s32RetVal;
}

#endif


/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32GetDataFromScc
*
* PARAMETER   : NONE
*
* RETURNVALUE : Number of bytes Sent on success
*               OSAL_ERROR on Failure
*
* DESCRIPTION : This is a wrapper over the function used to Get data from SCC
*                         poll() is used to avoid permanent blocking of the caller
*
* HISTORY     :
*-----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 22.APR.2013 | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ---------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32GetDataFromScc(tU32 u32Bytes)
{

#ifdef GNSS_PROXY_TEST_STUB_ACTIVE
   return (GnssProxy_s32GetDataFromStub(u32Bytes));
#else

   tS32 s32RetVal = -1;
   struct pollfd fds[1];
   tU32 u32PollCnrt = 0;
   static tU32 u32NoDataInt = 0;
   tBool bret;

   fds[0].fd = rGnssProxyInfo.s32SocketFD;
   fds[0].events = POLLIN;
   nfds_t nfds = GNSS_PROXY_POLL_NUM_OF_FDS;

   do
   {
      bret = TRUE;
      s32RetVal = poll( fds, nfds, GNSS_PROXY_POLL_TIMEOUT_MS );
      // If shutdown flag is set, return
      if ( TRUE ==  rGnssProxyInfo.bShutdownFlag )
      {
         s32RetVal =  -1;
      }
      // Timeout occoured
      else if ( 0 == s32RetVal )
      {
         if ( GNSS_PROXY_POLL_COUNTER_THRESHOLD == ++u32PollCnrt )
         {
            u32NoDataInt += (tU32)GNSS_PROXY_CONVERT_MS_TO_SEC ( GNSS_PROXY_DATA_INTERVAL_MS * GNSS_PROXY_READ_TIME_OUT_SCALE_FACTOR );
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "NO GNSS data for %lu sec", u32NoDataInt );
         }
         else
         {
            bret = FALSE;
         }
      }
      else if( 0 > s32RetVal )
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "poll() fail errno:%d", errno );
      }
      // check if event received is for data
      else if ( fds[0].revents & POLLIN )
      {
         s32RetVal =  dgram_recv( rGnssProxyInfo.hldSocDgram ,
                            (void *)rGnssProxyInfo.u8RecvBuffer,
                            (size_t)u32Bytes );
         if ( 0 >= s32RetVal )
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "dgram_recv Failed Line %d RetVal: %d errno : %d",
                                                 __LINE__, s32RetVal, errno );
         }
         // clear the data timeout counter
         u32NoDataInt = 0;
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "GNSS:poll() ret %d ev %x", s32RetVal, fds[0].revents );
         s32RetVal = -1;
      }
   }while ( FALSE == bret );
   return s32RetVal;
#endif

}
/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32SendDataToScc
*
* PARAMETER   : tU32 u32Bytes: Number of bytes of data to be sent.
*
* RETURNVALUE : Number of bytes Sent on success
*               OSAL_ERROR on Failure
*
* DESCRIPTION : This is a wrapper over the function used to send data to SCC
*
* HISTORY     :
*-----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 22.APR.2013 | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ---------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32SendDataToScc(tU32 u32Bytes)
{

#ifdef GNSS_PROXY_TEST_STUB_ACTIVE
   return write( rGnssProxyInfo.s32SocketFD, rGnssProxyInfo.u8TransBuffer, u32Bytes );
#else
   tS32 s32RetVal;
   s32RetVal =  dgram_send( rGnssProxyInfo.hldSocDgram ,
                      (void *)rGnssProxyInfo.u8TransBuffer,
                      (size_t)u32Bytes );
   if ( s32RetVal != (tS32) u32Bytes)
   {
      GnssProxy_vTraceOut(TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, 
      "Gnss:dgram_send failed ret: %d Expected: %lu", s32RetVal, u32Bytes );
   }
   return s32RetVal;
#endif

}
#ifdef GNSS_PROXY_TEST_STUB_ACTIVE

/********************************************************************************
* FUNCTION       : dgram_recv 
*
* PARAMETER      : s32ConFD  : Socket FD
*                  pvDataBuff: pointer to memory for data to be stored
*                  u32Length : Maximum size of data that memory can hold
*                                                      
* RETURNVALUE    : (Number of bytes of data received - header_size)  on success
*                  OSAL_ERROR on Failure
*
* DESCRIPTION    : This is used to receive data from SCC.
*                  With the help of a header, it identifies message boundaries
*                  and delivers messages one by one as sent from SCC
*
* HISTORY        : 31.MAY.2013| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static int dgram_recv(int s32ConFD, void *pvDataBuff, tU32 u32Length)
{

   tS32 s32RetVal=OSAL_ERROR;
   tU16 u16Size = 0;
   /* Routine parameter check */
   if((s32ConFD == 0) || (!u32Length) || (!pvDataBuff))
   {
      GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, 
      "!!!some parameters passed to dgram_recv are Invalid");
      s32RetVal = OSAL_ERROR;
   }
   else
   {
      /* receive message header from network stack.
         Data in header says how many bytes is the message from SCC */
      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "stub recv called");
      s32RetVal = recv(s32ConFD, &u16Size, sizeof(u16Size), 0);
      if( s32RetVal != (tS32)sizeof(u16Size) )
      {
         GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "!!! recv FAIL for Size RetVal %d", s32RetVal);
         s32RetVal=OSAL_ERROR;
      }
      else
      {
         GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, " stub recv passed for Header. line: %lu, payload size: %d", __LINE__, u16Size);
         /* If recv buffer is large enough to hold complete message */
         if(u32Length > u16Size)
         {
            /* receive complete message from network stack*/
            s32RetVal = recv(s32ConFD, pvDataBuff, u16Size, 0);
            if(s32RetVal == u16Size)
            {
               GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, " stub recv passed for Data. line: %lu, size: %d", __LINE__, u16Size);
            }
            else
            {
               GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "!!! recv FAIL for data RetVal %d", s32RetVal);
               s32RetVal=OSAL_ERROR;
            }
         }
         else
         {
            GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "Buffer not Big enough needed %d given %d", u16Size, u32Length);
            s32RetVal = OSAL_ERROR;
         }
      }
   }
   return s32RetVal;
}
#endif

/*********************************************************************************************************************
* FUNCTION    : GnssProxy_s32InitCommunToSCC
*
* PARAMETER   : NONE
*             
* RETURNVALUE : OSAL_OK  on success
*               OSAL_ERROR on Failure
*
* DESCRIPTION : This does all the initial communication with SCC
*               1. Status exchange
*               2. Receive and process configuration message.
* HISTORY     :
*-----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 22.APR.2013 | Initial version: 1.0   | Niyatha S Rao (RBEI/ECF5)
* ---------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32InitCommunToSCC()
{
   tS32 s32RetVal = OSAL_ERROR;

#ifndef GNSS_PROXY_TEST_STUB_ACTIVE
   rGnssProxyInfo.hldSocDgram = dgram_init(rGnssProxyInfo.s32SocketFD,GNSS_PROXY_MAX_PACKET_SIZE, OSAL_NULL);
   
   if( rGnssProxyInfo.hldSocDgram == OSAL_NULL )
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL ,  GNSSPXY_DEF_TRC_RULE, " dgram_init Failed ");
   }
   /* Send status message to V850 */
   else
#endif
   if (OSAL_OK != GnssProxy_s32ExchStatus( GNSS_PROXY_COMPONENT_STATUS_ACTIVE,
                                           GNSS_PROXY_COMPONENT_VERSION) )
   {
      s32RetVal = OSAL_ERROR;
   }
   else if ( OSAL_OK != GnssProxy_s32HandleStatusMsg() )
   {
      s32RetVal = OSAL_ERROR;
   }
   else if ( TRUE == rGnssProxyInfo.bConfigrecvd )
   {
      GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_PERS_CFG, NULL );
      s32RetVal = OSAL_OK;
   }
   else
   {
      /* Wait for configuration message from V850 */
      s32RetVal = GnssProxy_s32GetDataFromScc(GNSS_PROXY_MAX_PACKET_SIZE);
      if( s32RetVal > 0 )
      {
         rGnssProxyInfo.u32RecvdMsgSize = (tU32)s32RetVal;
         /* Check if we got Config message as expected */
         if ( GNSS_PROXY_SCC_R_CONFIG_START_MSGID != rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] )
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL,  GNSSPXY_DEF_TRC_RULE, " Expected Config message but received msg ID %x",
                                                 rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] );
            s32RetVal = OSAL_ERROR;
         }
         else
         {
            /* Process the config message */
            s32RetVal = GnssProxy_s32HandleConfigMsg();
            if ( OSAL_OK == s32RetVal )
            {
               rGnssProxyInfo.bConfigrecvd = TRUE;
               GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_INIT_COM_SUC_SCC, NULL );
            }
         }
      }
      else
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "dgram_recv Failed Line %d RetVal: %d errno : %d",
                                                __LINE__, s32RetVal, errno );
         s32RetVal = OSAL_ERROR;
      }
   }



#ifdef GNSS_PROXY_REMOTE_TESEO_CONROL_FEATURE
      GnssProxy_vInitTesCntFea();
#endif

   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION      : GnssProxy_s32SendStatusCmd
*
* PARAMETER     :  tU8 u8Status: Status tobe sent
*                           tU8 u8CompVer : Component version
*
* RETURNVALUE   : OSAL_OK  on success
*                 OSAL_ERROR on Failure
*
* DESCRIPTION   : Frames the status command message as ACTIVE and transmits it to SCC.
*
* HISTORY       :
*------------------------------------------------------------------------------
* Date          |       Version          | Author & comments
*---------------|------------------------|-------------------------------------
* 22.APR.2013   | Initial version: 1.0   | Niyatha S Rao (RBEI/ECF5)
* -----------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32SendStatusCmd( tU8 u8Status, tU8 u8CompVer )
{
   tS32 s32RetVal;
   trGnssProxyCmdHostStatus rMsgCmdEnqStatus;

   rMsgCmdEnqStatus.u8MsgID = GNSS_PROXY_SCC_C_COMPONENT_STATUS_MSGID;
   rMsgCmdEnqStatus.u8HostStatus = u8Status;
   rMsgCmdEnqStatus.u8HostAppVer = u8CompVer;

   /* Fill in message ID */
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_MSG_ID],
                      &rMsgCmdEnqStatus.u8MsgID,
                      sizeof(rMsgCmdEnqStatus.u8MsgID) );

   /* Fill in ACTIVE status */
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_COMPONENT_STATUS],
                      &rMsgCmdEnqStatus.u8HostStatus,
                      sizeof(rMsgCmdEnqStatus.u8HostStatus) );

   /* Fill in Host application status */
   OSAL_pvMemoryCopy( &rGnssProxyInfo.u8TransBuffer[GNSS_PROXY_OFFSET_COMPONENT_VERSION],
                      &rMsgCmdEnqStatus.u8HostAppVer,
                      sizeof(rMsgCmdEnqStatus.u8HostAppVer) );

   s32RetVal = GnssProxy_s32SendDataToScc(MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS);

   if( MSG_SIZE_SCC_GNSS_C_COMPONENT_STATUS == s32RetVal )
   {
      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_WR_PASS, NULL );
      s32RetVal = OSAL_OK;
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE, "unable to send component status err: %d", errno );
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION      : GnssProxy_s32SocketSetup
*
* PARAMETER     : tU32 u32IncPort : INC port ID
*
* RETURNVALUE   : OSAL_OK  on success
*                 OSAL_ERROR on Failure
*
* DESCRIPTION   : It creates socket and tries to connect to SCC.
*                 Connect will succeed if HOST (SCC) is waiting on accept call.
*
* HISTORY       :
*----------------------------------------------------------------------------------
* Date          |       Version         | Author & comments
*---------------|-----------------------|------------------------------------------
* 22.APR.2013   | Initial version: 1.0  | Niyatha S Rao (RBEI/ECF5)
* ---------------------------------------------------------------------------------
* 08.MAY.2013   | version: 1.1          | Madhu Kiran Ramachandra (RBEI/ECF5)
*               |                       | Updated comments and traces
* ---------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef GNSS_PROXY_TEST_STUB_ACTIVE
/* TO connect to v850.  */
tS32 GnssProxy_s32SocketSetup(tU32 u32IncPort)
{
   tS32 s32RetVal = OSAL_ERROR;
   struct hostent *SccIP,*ImxIP;
   struct sockaddr_in SocAddr_local,SocAddr_remote;

    /* Socket Type */
   SocAddr_local.sin_family = AF_BOSCH_INC_AUTOSAR;
   SocAddr_remote.sin_family = AF_BOSCH_INC_AUTOSAR;

   /* Port */
   //lint -e160
   SocAddr_local.sin_port = (tU16)htons((tU16)(u32IncPort | PORT_OFFSET ));
   SocAddr_remote.sin_port = (tU16)htons((tU16)(u32IncPort | PORT_OFFSET ));

   rGnssProxyInfo.s32SocketFD = socket( AF_BOSCH_INC_AUTOSAR,(tS32)SOCK_STREAM, 0 );
   if( rGnssProxyInfo.s32SocketFD > 0 )
   {
      if( (SccIP = gethostbyname("scc")) == OSAL_NULL )
      {
         GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "scc gethostbyname returned null errno %d", errno );
         GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );
      }
      else 
      {
        OSAL_pvMemoryCopy( (char *) &SocAddr_remote.sin_addr.s_addr,
                           (char *)(SccIP->h_addr),
                           (tU32)SccIP->h_length);
        
        if( (ImxIP = gethostbyname("scc-local") ) == OSAL_NULL )
         {
            GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "scc-local gethostbyname returned null errno %d", errno );
            GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );
         }
        else
        {
            OSAL_pvMemoryCopy( (char *) &SocAddr_local.sin_addr.s_addr,
                               (char *)(ImxIP->h_addr),
                               (tU32)ImxIP->h_length );

          
            /* Bind to local address */
            //lint -e64
            s32RetVal = bind( rGnssProxyInfo.s32SocketFD,
                              /* Double type cast to satisfy lint */
                              (const struct sockaddr *)(tPVoid)&SocAddr_local,
                              // -epn, -eps, -epu, -ep
                               sizeof(SocAddr_local));
           
            if( s32RetVal == 0 )
            {
            /* Connect to remote scc address */
               s32RetVal = connect( rGnssProxyInfo.s32SocketFD,
                                    /* Double type cast to satisfy lint */
                                    (const struct sockaddr *)(tPVoid)&SocAddr_remote,
                                     sizeof(SocAddr_remote) );
               if( s32RetVal == 0 )
               {
                  GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_CON_PASS, " %x", u32IncPort );
                  s32RetVal = OSAL_OK;
               }
               else
               {
                  GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "Connect Failed errno=%d port %x", errno, u32IncPort );
                  GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );
               }
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "Bind failed errno=%d port %x", errno, u32IncPort );
               GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );
            }
         }
      }
   }
   else
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "Socket system call failed errno=%d port %x", errno, u32IncPort );
   }
   return s32RetVal;
}

#else
/* to connect to GNSS test stub */
/* tU32 u32IncPort is a dummy argument to this function*/
tS32 GnssProxy_s32SocketSetup(tU32 u32IncPort)
{
   tS32 s32RetVal = OSAL_ERROR;
   struct sockaddr_in SocAddr;

   /* create a socket */
   rGnssProxyInfo.s32SocketFD = socket( AF_INET, (tS32)SOCK_STREAM, 0 );
   if( rGnssProxyInfo.s32SocketFD > 0 )
   {
      /* Fill in the socket config structure */
      SocAddr.sin_family = AF_INET; /* Family */
      s32RetVal = inet_pton(AF_INET, GNSS_PROXY_SERVER_IP_ADDRESS, &SocAddr.sin_addr); /* IP of HOST */
      if(s32RetVal < 1)
      {
         perror("inet_pton:");
         GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "inet_pton Failed");
      }
      /*Port for GNSS is 0x08. But 0x08 is busy. So Some other port is used*/
      SocAddr.sin_port = htons(0x08); /* Port in HOST */
      /* Try to connect to host */
      s32RetVal = connect(rGnssProxyInfo.s32SocketFD, (struct sockaddr *)&SocAddr, sizeof(SocAddr));
      if( s32RetVal == 0 )
      {
         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_SIM_CON_PASS, NULL );
         s32RetVal = OSAL_OK;
      }
      else
      {
         GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "Connect failed. Make sure that GNSS test stub is on");
         GnssProxy_vReleaseResource( GNSS_PROXY_RESOURCE_RELSEASE_SOCKET );
         s32RetVal = OSAL_ERROR;
      }
   }
   else
   {
      GnssProxy_vTraceOut(TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE, "socket create failed");
   }
   return s32RetVal;

}
#endif

#ifdef GNSS_PROXY_REMOTE_TESEO_CONROL_FEATURE
/*********************************************************************************************************************
* FUNCTION     : GnssProxy_vInitTesCntFea
*
* PARAMETER    : NONE
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : This function will spawn a thread waiting for that accepts requests
*                and sends commands to teseo.
* HISTORY      :
*--------------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|-----------------------------------------------
* 22.APR.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vInitTesCntFea (tVoid)
{

   OSAL_trThreadAttribute rThreadAttr;
   /*Update thread attributes*/
   rThreadAttr.szName       = GNSS_PROXY_TESEO_CTRL_THREAD_NAME;
   rThreadAttr.u32Priority  = GNSS_PROXY_TESEO_CTRL_THREAD_PROIORITY;
   rThreadAttr.s32StackSize = GNSS_PROXY_TESEO_CTRL_THREAD_STACKSIZE;
   rThreadAttr.pfEntry      = GnssProxy_vTesCtrThread;
   rThreadAttr.pvArg        = OSAL_NULL;

   if(OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr) )
   {
      GnssProxy_vTraceOut( TR_LEVEL_ERRORS, GNSSPXY_DEF_TRC_RULE,  "Teseo control: Thread spawn failed" );
   }
}
/*********************************************************************************************************************
* FUNCTION     : GnssProxy_vTesCtrThread
*
* PARAMETER    : NONE
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : This thread will be waiting for requests 172.17.0.6.
*                These requests will be forwarded to TESEO on INC
* HISTORY      :
*--------------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|-----------------------------------------------
* 22.APR.2013  | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* -------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid GnssProxy_vTesCtrThread( tPVoid pvArg )
{
   tS32 s32ErrChk = 0;
   tS32 s32ConnFD = 0;
   tS32 s32OptState = 1;
   tU8 u8ExchBuff[GNSS_PROXY_TRANSMIT_BUFF_SIZE];
   struct sockaddr_in rSocAttr;
   tS32 s32SocketFD = 0;

   OSAL_pvMemorySet(&rSocAttr,0, sizeof(rSocAttr));
   rSocAttr.sin_family      = AF_INET;
   rSocAttr.sin_port        = htons(0x06);
   rSocAttr.sin_addr.s_addr = htonl("172.17.0.1");

   /* create a socket
      SOCK_STREAM: sequenced, reliable, two-way, connection-based
      byte streams with an OOB data transmission mechanism.*/
   s32SocketFD = socket(AF_INET, SOCK_STREAM, 0);
   if(s32SocketFD == -1)
   {
      GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "socket() failed");
   }
   else
   {
      /* This option informs OS to reuse the socket address even if it is busy*/
      if(setsockopt(s32SocketFD, SOL_SOCKET, SO_REUSEADDR,
                    &s32OptState, sizeof(s32OptState)) == -1)
      {
         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "!!!setsockopt() Failed");
      }
      /* Bind the socket created to a address */
      s32ErrChk = bind(s32SocketFD, (struct sockaddr *)&rSocAttr, sizeof(rSocAttr));
      if(s32ErrChk == -1)
      {
         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Bind Failed");
      }
      else
      {
         /* Listen system call makes the socket as a passive socket.
            i.e socket will be used to accept incoming connections */
         s32ErrChk = listen(s32SocketFD, 10);
         if(s32ErrChk == -1)
         {
            GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, "Listen Failed");
         }
         else
         {
            s32ErrChk = OSAL_E_NOERROR;
         }
      }
   }
   /* proceed only if socket config was successful */
   if ( OSAL_E_NOERROR == s32ErrChk )
   {
      while ( TRUE != rGnssProxyInfo.bShutdownFlag )
      {
         OSAL_pvMemorySet( u8ExchBuff, 0, GNSS_PROXY_TRANSMIT_BUFF_SIZE );
         u8ExchBuff[GNSS_PROXY_OFFSET_C_TP_MARKER] = 1;
         u8ExchBuff[GNSS_PROXY_OFFSET_C_DATA_MSG_ID] = 0x40;
         GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_TES_TD_BLK, NULL );
         s32ConnFD = accept(s32SocketFD, NULL, NULL);
         if(s32ConnFD == -1)
         {
            GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "Teseo-cfg Thread :accept Failed !!!");
            /* This should never happen. Only reason for this is something went
              wrong while configuring the socket*/
         }
         else
         {
            GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_TES_TD_CON_ACP, NULL );
            s32ErrChk = recv( s32ConnFD,
                              &u8ExchBuff[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG],
                              GNSS_PROXY_TRANSMIT_BUFF_SIZE,
                              0);
            if (s32ErrChk <= 0 )
            {
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE,  "Teseo control Thread: recv failed" );
            }
            else
            {
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_TES_TD_CMD_RCV, " %s",
                                                     &u8ExchBuff[GNSS_PROXY_OFFSET_C_DATA_NMEA_MSG]);

#ifdef GNSS_PROXY_TEST_STUB_ACTIVE
               s32ErrChk =  write( rGnssProxyInfo.s32SocketFD,
                                   u8ExchBuff,
                                   s32ErrChk + GNSS_TROXY_SIZE_OF_C_DATA_MSG_HEADER );
#else
               s32ErrChk = dgram_send( rGnssProxyInfo.hldSocDgram ,
                                       (void *)u8ExchBuff,
                                       (size_t)s32ErrChk + GNSS_TROXY_SIZE_OF_C_DATA_MSG_HEADER );
#endif
               GnssProxy_vTraceOut( TR_LEVEL_USER_4, GNSSPXY_TES_TD_WR_RTVAL, " %d", s32ErrChk);
            }
            /* we get data from client. close the connection */
            close(s32ConnFD);
         }
      }
   }
   OSAL_vThreadExit();
}
#endif

/*********************************************************************************************************************
* FUNCTION      : GnssProxy_s32ExchStatus
*
* PARAMETER     : None
*
* RETURNVALUE   : OSAL_OK if V850 Service is active.
*                 OSAL_ERROR if not active or Failure
*
* DESCRIPTION   : 1: Send Status active command to SCC
*                 2: Wait for response from SCC
*                 3: If response is success and active return success else Error
*
* HISTORY       :
*----------------------------------------------------------------------------------
* Date          |       Version         | Author & comments
*---------------|-----------------------|------------------------------------------
* 09.DEC.2013   | Initial version: 1.0  | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ---------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 GnssProxy_s32ExchStatus( tU8 u8Status, tU8 u8CompVer )
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32cntr;
   tBool bRetrySts = TRUE;

   if( OSAL_ERROR == GnssProxy_s32SendStatusCmd( u8Status, u8CompVer ))
   {
      GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  " Unable to send IMX status to SCC");
   }
   else
   {
       GnssProxy_vTraceOut(TR_LEVEL_USER_4, GNSSPXY_DEF_TRC_RULE, " Waiting for status from SCC");
       /* Wait for status message of V850 */
       for ( s32cntr=0;
             ( ( GNSS_PROXY_MAX_INCORRECT_STS_RES > s32cntr ) && ( TRUE == bRetrySts ) );
             s32cntr++ )
      {
         bRetrySts = FALSE;
         s32RetVal = GnssProxy_s32GetDataFromScc(GNSS_PROXY_MAX_PACKET_SIZE);
         if( s32RetVal > 0 )
         {
            rGnssProxyInfo.u32RecvdMsgSize = (tU32)s32RetVal;
             
            /* Check if we got status message */
            if ( ( GNSS_PROXY_SCC_R_COMPONENT_STATUS_MSGID != rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] ) && 
            ( GNSS_PROXY_SCC_R_REJECT_MSGID != rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] ) )
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL , GNSSPXY_DEF_TRC_RULE, "Expected status message but Got msg ID 0x%x",
                                                     rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] );
               s32RetVal = OSAL_ERROR;
               bRetrySts = TRUE;
            }
             
            /* Check if we got reject message */
             
            else if( GNSS_PROXY_SCC_R_REJECT_MSGID == rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_MSG_ID] )
            {
               GnssProxy_vTraceOut( TR_LEVEL_FATAL, GNSSPXY_DEF_TRC_RULE,  "Command 0x%x Rejected by SCC. Reason : 0x%x",
                                   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_REJECTED_MSG_ID],
                                   rGnssProxyInfo.u8RecvBuffer[GNSS_PROXY_OFFSET_REJECT_REASON] );
                           
               s32RetVal = OSAL_ERROR;
            }
            else
            {
               s32RetVal = OSAL_OK;
            }
         }
         else
         {
            // Do nothing.
         }
      }
   }
   return s32RetVal;
}
