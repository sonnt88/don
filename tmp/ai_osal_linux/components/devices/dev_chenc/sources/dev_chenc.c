/*********************************************************************************************************************
* FILE        : dev_chenc.c
*
* DESCRIPTION : This file implements China encoder osal driver for GEN3.
*               In GEN3 encoding of the positioning information needed for accessing the china map is done by
*               a library provided by chinese government. dev_chenc is just a wrapper over the library. Also dev_chenc
*               loads the library dynamically whenever needed and links the symbols needed to encode the positioning
*               information.
*---------------------------------------------------------------------------------------------------------------------
* AUTHOR(s)   : Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*
* HISTORY     :
*------------------------------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|---------------------------------------------------------
* 12.JUL.2013 | Initial version: 0.1   |Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*-------------|------------------------|---------------------------------------------------------
*
*********************************************************************************************************************/

/*************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"

/* Dynamic loading functionality */
#include<dlfcn.h>

#include "dev_chenc.h"

/*************************************************************************
* Macro declaration (scope: Global)
*-----------------------------------------------------------------------*/
/* This is used only for testing and shall be disabled later.
    It is not allowed to load the encoded or raw positioning information from GPS.
    Hence only for testing this macro has to be enabled. As enabling this will
    enable the traces that of positioning information. */
//#define CHINA_ENC_TEST_MODE

/* If you don set the LD_LIBRARY_PATH environment variable you can use an absolute path. Or
    library should be present in /lib/ or /usr/lib/ */
/* #define CHINAENCODER_LIB "/opt/bosch/base/bin/libchinaencoder.so" */

/* GMMY16 the chinalib is placed in /opt/bosch/chenc/lib/ */
/* #define CHINA_ENC_LIB_NAME "libchinaencoder.so" */
/* #define CHINA_ENC_LIB_NAME "chinaencoder_1_0_0" */
#define CHINA_ENC_LIB_NAME "/opt/bosch/chenc/lib/libchinaencoder.so"

/* Conversion factor from radians to the angular unit (1/1024 '') used
   by the Chinese encoding function. */
#define CHINA_ENC_RAD2CHINA ((180.0/3.141592653589)*60*60*1024)

/* Conversion factor from Chinese angular units (see RAD2CHINA) to radians. */
#define CHINA_ENC_CHINA2RAD (1.0/CHINA_ENC_RAD2CHINA)

#ifdef CHINA_ENC_TEST_MODE
#define CHINA_ENC_CHINA2GRAD(x) ((tDouble) x/(60*60*1024))
#endif

/* The Function name in the library. This will be linked dynamically for
   encoding */
#define CHINA_ENC_ENCODING_FUNCTION_NAME "wgtochina_lb"

/* Boundry checks for positioning information */
#define CHINE_ENC_LATITUDE_UPPER_LIMIT (3.2)
#define CHINE_ENC_LATITUDE_LOWER_LIMIT (-3.2)

#define CHINE_ENC_LONGITUDE_UPPER_LIMIT (6.3)
#define CHINE_ENC_LONGITUDE_LOWER_LIMIT (-6.3)

#define CHINE_ENC_ALTITUDE_UPPER_LIMIT (50000)
#define CHINE_ENC_ALTITUDE_LOWER_LIMIT (-10000)

/*************************************************************************
* Driver specific structure definition
*-----------------------------------------------------------------------*/

/* function pointer to the china encoder function */
typedef  tU32 (*t_fp_wgtochina_lb)( tS32,
                                    tU32,
                                    tU32,
                                    tS32,
                                    tS32,
                                    tU32,
                                    tPU32,
                                    tPU32 );
/* Encoder device running information */
typedef struct
{
   tBool bDevOpenFlag;
   tVoid *h_lib;
   t_fp_wgtochina_lb fp_wgtochina_lb;
   tBool bDisChEnc;

}trChEncInfo;
/*************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/

trChEncInfo rChEncInfo;

/*************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
tS32 ChEnc_s32Init ( tVoid );
tS32 ChEnc_s32DeInit ( tVoid );
static tVoid ChEnc_vTraceOut(  TR_tenTraceLevel enLevel,const tChar *pcFormatString,... );
tS32 ChEnc_s32EncodePos ( OSAL_trChencConversionData *prChencConversionData );
/*********************************************************************************************************************
* FUNCTION    : ChEnc_s32IOOpen
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_E_NOERROR  on success
*               Relevant OSAL error codes on Failure
*
* DESCRIPTION : Initializes the China encoder and sets open flag on sucess.
*
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
* --------------------------------------------------------------------------
* 15.JUL.2013 | version: 1.0          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                       | Initial Version
* --------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 ChEnc_s32IOOpen( tVoid )
{
   tS32 s32RetVal;

   if( TRUE == rChEncInfo.bDevOpenFlag )
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "!!!Device already open" );
      s32RetVal =  OSAL_E_ALREADYOPENED;
   }
   else if ( OSAL_E_NOERROR != (tU32)ChEnc_s32Init() )
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "ChEnc Initialization Failed" );
      s32RetVal = OSAL_E_NOTINITIALIZED;
   }
   else
   {
      rChEncInfo.bDevOpenFlag = TRUE;
      s32RetVal = OSAL_E_NOERROR;
   }
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : ChEnc_s32Init
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_E_NOERROR  on success
*               OSAL_E_UNKNOWN or relevant OSAL error codes on Failure
*
* DESCRIPTION : Initializes the Chenc proxy driver.
*                         1: Try to load the china encoder library
*                         2: Try to link the symbols to encoding function in shared library
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
* --------------------------------------------------------------------------
* 15.JUL.2013 | version: 1.0          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                       | Initial Version
* --------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 ChEnc_s32Init ( tVoid )
{

   tS32 s32RetVal = OSAL_E_NOERROR;
   tPChar pcErrorStr;
   rChEncInfo.bDisChEnc = FALSE;

   /* Load the china encoder library */
   ChEnc_vTraceOut( TR_LEVEL_USER_4,"opening:%s",CHINA_ENC_LIB_NAME );
   rChEncInfo.h_lib = dlopen ( CHINA_ENC_LIB_NAME, RTLD_LAZY);
   if ( OSAL_NULL == rChEncInfo.h_lib )
   {
      pcErrorStr = dlerror();
      if ( OSAL_NULL != pcErrorStr )
      {
         ChEnc_vTraceOut( TR_LEVEL_ERRORS, "Error while opening chinaencoder lib: %s", pcErrorStr );
      }
      else
      {
         ChEnc_vTraceOut( TR_LEVEL_ERRORS, "Error while opening chinaencoder lib & dlerror returned NULL" );
      }
      s32RetVal = OSAL_E_IOERROR;
   }
   else
   {
      /* Get handle to encoding function in shared library */
      rChEncInfo.fp_wgtochina_lb = dlsym( rChEncInfo.h_lib, CHINA_ENC_ENCODING_FUNCTION_NAME );
      if ( OSAL_NULL == rChEncInfo.fp_wgtochina_lb )
      {
         pcErrorStr = dlerror();
         if ( OSAL_NULL != pcErrorStr )
         {
            ChEnc_vTraceOut( TR_LEVEL_ERRORS,
                               "could not get pointer to function wgtochina_lb: %s", pcErrorStr );
         }
         else
         {
            ChEnc_vTraceOut( TR_LEVEL_ERRORS,
                               "could not get pointer to function wgtochina_lb & dlerror returned NULL" );
         }
         s32RetVal = OSAL_E_UNKNOWN;
      }
      else
      {
         ChEnc_vTraceOut( TR_LEVEL_USER_4, "ChEnc_s32Init:sucess" );
      }
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : ChEnc_s32EncodePos
*
* PARAMETER   : OSAL_trChencConversionData * : pointer to position info and to store result.
*
* RETURNVALUE : OSAL_E_NOERROR  on success
*               OSAL_E_UNKNOWN or relevant OSAL error codes on Failure
*
* DESCRIPTION : Encode the position info with encoding library.
*                    1: Does some sanitary check
*                    2: Call the encoding function with proper parameters
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
* --------------------------------------------------------------------------
* 15.JUL.2013 | version: 1.0          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                       | Initial Version
* --------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 ChEnc_s32EncodePos ( OSAL_trChencConversionData *prChencConversionData )
{

   tS32 s32Initflag=0;
   tU32 u32Rawlatitude=0;
   tU32 u32Rawlongitude=0;
   tS32 s32Altitude=0;
   tS32 s32GPSWeekNum=0;
   tU32 u32Time=0;
   tU32 u32ChinaLongitude=0;
   tU32 u32ChinaLatitude=0;
   tS32 s32RetVal = OSAL_E_NOERROR;

   /* Parameter check */
   if ( OSAL_NULL == prChencConversionData )
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "Invalid Param to ChEnc_s32EncodePos" );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else if( OSAL_NULL == rChEncInfo.fp_wgtochina_lb )
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "!!!rChEncInfo.fp_wgtochina_lb is null" );
      s32RetVal = OSAL_E_UNKNOWN;
   }
   /* Check if positioning information is proper */
   else if ( (prChencConversionData->dLongitude < CHINE_ENC_LONGITUDE_LOWER_LIMIT) ||
           (prChencConversionData->dLongitude > CHINE_ENC_LONGITUDE_UPPER_LIMIT) )
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "Longitude out of range" );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else if ( (prChencConversionData->dLatitude < CHINE_ENC_LATITUDE_LOWER_LIMIT) ||
           (prChencConversionData->dLatitude > CHINE_ENC_LATITUDE_UPPER_LIMIT) )
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "Latitude out of range" );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else if(   (prChencConversionData->dAltitude < CHINE_ENC_ALTITUDE_LOWER_LIMIT)
         || (prChencConversionData->dAltitude > CHINE_ENC_ALTITUDE_UPPER_LIMIT))
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "ALTITUDE out of range" );
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   /* Encoding functionality can be disabled temporarily by a IO_Control */
   else if ( TRUE == rChEncInfo.bDisChEnc )
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "ChEnc Temp Disabled");
      s32RetVal = OSAL_E_TEMP_NOT_AVAILABLE;
   }
   else
   {
      s32Initflag     = prChencConversionData->s32InitializationFlag;
      u32Rawlongitude = (tU32) (prChencConversionData->dLongitude * CHINA_ENC_RAD2CHINA);
      u32Rawlatitude  = (tU32) (prChencConversionData->dLatitude * CHINA_ENC_RAD2CHINA);
      s32Altitude     = (tS32) prChencConversionData->dAltitude;
      s32GPSWeekNum   = prChencConversionData->s32GpsWeek;
      u32Time         = prChencConversionData->u32GpsSecond;

#ifdef CHINA_ENC_TEST_MODE
      ChEnc_vTraceOut( TR_LEVEL_USER_4, "Iflg: %d,RLon:%lu,RLat:%lu,Alt:%d,GPSWeek:%d,Time:%lu",
                                        s32Initflag,
                                        u32Rawlongitude,
                                        u32Rawlatitude,
                                        s32Altitude,
                                        s32GPSWeekNum,
                                        u32Time );
#endif

      prChencConversionData->u32ReturnCode = (*rChEncInfo.fp_wgtochina_lb)
                                                         (
                                                          s32Initflag,
                                                          u32Rawlongitude,
                                                          u32Rawlatitude,
                                                          s32Altitude,
                                                          s32GPSWeekNum,
                                                          u32Time,
                                                          &u32ChinaLongitude,
                                                          &u32ChinaLatitude );
      if ( prChencConversionData->u32ReturnCode )
      {
         ChEnc_vTraceOut( TR_LEVEL_ERRORS, "fp_wgtochina_lb returned error, %lu=0x%lx",
                          prChencConversionData->u32ReturnCode,
                          prChencConversionData->u32ReturnCode );
         s32RetVal = OSAL_E_UNKNOWN;
      }
      else
      {
         prChencConversionData->dChinaLongitude = (tDouble)u32ChinaLongitude * CHINA_ENC_CHINA2RAD;
         prChencConversionData->dChinaLatitude = (tDouble)u32ChinaLatitude * CHINA_ENC_CHINA2RAD;

#ifdef CHINA_ENC_TEST_MODE
         ChEnc_vTraceOut( TR_LEVEL_USER_4, "ENCODED: LON: %lu LAT: %lu => Lat:%lf Long:%lf" ,
                          u32ChinaLongitude,
                          u32ChinaLatitude,
                          CHINA_ENC_CHINA2GRAD(u32ChinaLatitude),
                          CHINA_ENC_CHINA2GRAD(u32ChinaLongitude) );
#endif


      }
   }
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : ChEnc_s32IOClose
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_E_NOERROR  on success
*               OSAL_E_UNKNOWN or relevant OSAL error codes on Failure
*
* DESCRIPTION : De-Initialize the device and reset the open flag
*
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
* --------------------------------------------------------------------------
* 15.JUL.2013 | version: 1.0          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                       | Initial Version
* --------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 ChEnc_s32IOClose(tVoid)
{
   tS32 s32RetVal;

   if ( OSAL_E_NOERROR != (tU32) ChEnc_s32DeInit() )
   {
      s32RetVal = OSAL_E_UNKNOWN;
   }
   else
   {
      s32RetVal = OSAL_E_NOERROR;
      rChEncInfo.bDevOpenFlag =  FALSE;
   }
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION    : ChEnc_s32DeInit
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_E_NOERROR  on success
*               OSAL_E_UNKNOWN or relevant OSAL error codes on Failure
*
* DESCRIPTION : Unload the shared library and reset the handles/Variables
*
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
* --------------------------------------------------------------------------
* 15.JUL.2013 | version: 1.0          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                       | Initial Version
* --------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 ChEnc_s32DeInit ( tVoid )
{
   tS32 s32RetVal =  OSAL_E_UNKNOWN;

   if ( OSAL_NULL == rChEncInfo.h_lib )
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "rChEncInfo.h_lib is null in close" );
   }
   else if ( dlclose( rChEncInfo.h_lib ) )
   {
      ChEnc_vTraceOut( TR_LEVEL_ERRORS, "dlclose Failed errno: %d",errno);
   }
   else
   {
      ChEnc_vTraceOut( TR_LEVEL_USER_4, "ChEnc_s32DeInit sucess" );
      rChEncInfo.h_lib = OSAL_NULL;
      rChEncInfo.fp_wgtochina_lb = OSAL_NULL;
      s32RetVal = OSAL_E_NOERROR;
   }
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : s32ChEnc_TmpDisable
*
* PARAMETER    : bDisableFlag: TRUE: China encoder should be disabled
*                              FALSE: China Encoder should not be disabled
*
* RETURNVALUE : OSAL_E_NOERROR  on success
*               OSAL_E_UNKNOWN or relevant OSAL error code on Failure
*
* DESCRIPTION : To Enable or disable China Encoder.
*
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
* --------------------------------------------------------------------------
* 15.JUL.2013 | version: 1.0          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                       | Initial Version
* --------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 s32ChEnc_TmpDisable(tBool bDisableFlag )
{
   ChEnc_vTraceOut( TR_LEVEL_USER_4, "s32ChEnc_TmpDisable called flag: %d", bDisableFlag );
   rChEncInfo.bDisChEnc = bDisableFlag;
   return OSAL_E_NOERROR;
}
/*********************************************************************************************************************
* FUNCTION    : ChEnc_s32IOControl
*
* PARAMETER    : s32FunId,  Function identifier
*                s32Arg , Argument to be passed to function
*
* RETURNVALUE : OSAL_E_NOERROR  on success
*               OSAL_E_UNKNOWN or relevant OSAL error code on Failure
*
* DESCRIPTION : Control functions corresponding to China Encoder
*
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
* --------------------------------------------------------------------------
* 15.JUL.2013 | version: 1.0          | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*             |                       | Initial Version
* --------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 ChEnc_s32IOControl(tS32 s32fun, tLong sArg)
{
   tS32 s32RetVal = OSAL_E_NOERROR;

   switch ( s32fun )
   {
      /* To call encoding library */
      case OSAL_C_S32_IOCTRL_CHENC_CONVERT:
      {
         if( OSAL_NULL != sArg )
         {
            s32RetVal = ChEnc_s32EncodePos( (OSAL_trChencConversionData *) sArg );
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }
      /* To Enable or disable china encoder */
      case OSAL_C_S32_IOCTRL_CHENC_TEMP_DISABLE:
      {
         s32RetVal = s32ChEnc_TmpDisable( (tBool)sArg );
         break;
      }
      case OSAL_C_S32_IOCTRL_CHENC_CONVERT_RAW:
      case OSAL_C_S32_IOCTRL_CHENC_SET_CB:
      {
         ChEnc_vTraceOut( TR_LEVEL_ERRORS, "s32fun %d Not Supported", s32fun );
         s32RetVal = OSAL_E_NOTSUPPORTED;
         break;
      }
      default:
      {
         ChEnc_vTraceOut( TR_LEVEL_ERRORS, "Unknown s32fun %d", s32fun );
         s32RetVal = OSAL_E_INVALIDVALUE;
         break;
      }

   }

   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION    : ChEnc_vTraceOut
*
* PARAMETER   : u32Level -> Trace level
*               pcFormatString -> Trace string
*
* RETURNVALUE : NONE
*
* DESCRIPTION : Trace function for China Encoder
*
* HISTORY     :
*------------------------------------------------------------------------------
* Date        |       Version       | Author & comments
*-------------|---------------------|-------------------------------------
* 11.JUL.2013 | Initial version: 1.0| Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ---------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid ChEnc_vTraceOut(  TR_tenTraceLevel enLevel,const tChar *pcFormatString,... )
{
   if(LLD_bIsTraceActive((tU32)OSAL_C_TR_CLASS_DRV_CHENC, (tU32)enLevel) != FALSE)
   {
      /*
      Parameter to hold the argument for a function,
      specified the format string in pcFormatString
      defined as: typedef char* va_list in stdarg.h
      */

      /*
      vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure
      */
      va_list argList = {0};
      tS32 s32Size;

      /*
      Buffer to hold the string to trace out
      */
      tS8 u8Buffer[MAX_TRACE_SIZE] = { 0 };

      /*
      Position in buffer from where the format string is to be
      concatenated
      */
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /* Flush the String */
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /* Copy the String to indicate the trace is from the RTC device */

      /*
      Initialize the argList pointer to the beginning of the variable
      arguement list
      */
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
      Collect the format String's content into the remaining part of
      the Buffer
      */
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,sizeof(u8Buffer),
                  pcFormatString, argList ) ) )
      {
         return;
      }

      /* Trace out the Message to TTFis */
      LLD_vTrace( (tU32)OSAL_C_TR_CLASS_DRV_CHENC, (tU32)enLevel, u8Buffer, (tU32)s32Size );   /* Send string to Trace*/

      /*
      Performs the appropiate actions to facilitate a normal return by a
      function that has used the va_list object
      */
      va_end(argList);
   }
}
#ifdef LOAD_SENSOR_SO

tS32 chenc_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
   (tVoid)s32Id;
   (tVoid)szName;
   (tVoid)enAccess;
   (tVoid)pu32FD;
   (tVoid)app_id;
   return ChEnc_s32IOOpen();
}

tS32 chenc_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   return ChEnc_s32IOClose();
}

tS32 chenc_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tLong sArg)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   return ChEnc_s32IOControl(s32fun, sArg);
}

#endif

/***********************************************End of file**********************************************************/
