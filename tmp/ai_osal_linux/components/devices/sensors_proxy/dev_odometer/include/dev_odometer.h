/******************************************************************************
* FILE				: Dev_odometer.h
*				  
* DESCRIPTION	: This is the header file for Dev_odometer.c
*				  
* AUTHOR(s) 		: Niyatha S Rao (RBEI/ECF5)
*
* HISTORY           :
*------------------------------------------------------------------------
* Date      		|       Version     		  	| Author & comments
*--------------|--------------------|-------------------------------------
* 14.MAR.2013	| Initial version: 1.0		| Niyatha S Rao (RBEI/ECF5)
* -----------------------------------------------------------------------
********************************************************************************/

#ifndef DEV_ODOMETER_HEADER
#define DEV_ODOMETER_HEADER

tS32 ODOMETER_s32IOOpen(tVoid);
tS32 ODOMETER_s32IOClose(tVoid);
tS32 ODOMETER_s32IOControl(tS32 s32fun, tLong sArg);
tS32 ODOMETER_s32IORead( tPS8 pBuffer, tU32 u32maxbytes);

#endif

/*End of file*/




