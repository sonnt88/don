/******************************************************************************
* FILE         : Dev_odometer.c
*             
* DESCRIPTION  : This file implements proxy Odometer driver module.
*                In GEN3 gyro, odometer and accelerometer sensor hardwares are present on V850.
*                Sensor data will be sent from V850 to IMX via INC. 
*                It is the job of each of the proxy sensor drivers(gyro, odo & acc) present on IMX to collect the
*                data  populated by the dispatcher module, in the each of the corresponding ring buffers 
*                and provide it to VDS_SENSOR application as and when it requests.
*
* AUTHOR(s)    : Niyatha S Rao (RBEI/ECF5)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date       |       Version      | Author & comments
*------------|--------------------|-------------------------------------------
* 14.MAR.2013| Initial version:1.0| Niyatha S Rao (RBEI/ECF5)
* -----------------------------------------------------------------------------
* 06/26/2014 | Version: 1.1       | Madhu Kiran Ramachandra (RBEI/ECF5)
*            |                    | Moved Config message receive section to open 
* -------------------------------------------------------------------------
* 07/23/2014 | Version: 1.2       | Madhu Kiran Ramachandra (RBEI/ECF5)
*            |                    | LINT Fix
* -------------------------------------------------------------------------
* 08/08/2014 | Version: 1.3       | Madhu Kiran Ramachandra (RBEI/ECF5)
*            |                    | Fix for GMMY16-15647. bGyroThreadRunning updated
*            |                    | correctly.
* -----------------------------------------------------------------------------
* 11/12/2015 | Version: 1.4       | Shivasharnappa Mothpalli (RBEI/ECF5)
*            |                    | Removed Unused IOCTRLS (Fix for  CFG3-1622 ):
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_FLUSH,
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_GET_TIMEOUT_VALUE,
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_GETRESOLUTION,
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_SET_TIMEOUT_VALUE,
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_RESET ):
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_INIT 
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_GETDIRECTION 
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_SETDIRECTION 
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_GET_WHEELCOUNTER 
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_INIT_OVC 
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_INACTIVATE_OIC
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_ACTIVATE_OIC 
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_SET_K_VALUE 
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_PULSES_PER_REVOLUTION
             |                    | OSAL_C_S32_IOCTRL_ODOMETER_WHEEL_CIRCUMFERENCE 
*********************************************************************************/

/************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"

#include "dev_odometer.h"
#include "sensor_dispatcher.h"
#include "sensor_ring_buffer.h"

/************************************************************************
* Macro declaration (scope: Global)
*-----------------------------------------------------------------------*/
#ifdef ODO_THREAD_ACTIVE

#define ODOMETER_C_STRING_MAIN_THREAD_NAME ("ODOMETER_THREAD_MAIN")
#define OSALCORE_C_U32_STACK_SIZE_ODOMETER          (2048)
#define OSALCORE_C_U32_PRIORITY_ODOMETER            (15)

#endif

#define ODOMETER_COUNTER_RANGE                      (0x03ff)
/* Timeout for configuration */
#define ODOMETER_INIT_CONFIG_MSG_WAIT_TIME_MS ((OSAL_tMSecond)500)


#define DEV_ODO_DRIVER_VERSION         (0x0100)  /* read v01.00 *///check the version

/************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/

static OSAL_tMQueueHandle hldOdoMsgQueue = OSAL_NULL;
#ifdef ODO_THREAD_ACTIVE
/*!
* osal thread running flag, set on thread creation
*/
static tBool bOdoThreadRunning = FALSE;

/*!
* osal thread ID, returned on thread creation
*/
static OSAL_tThreadID s32OdoThreadID = OSAL_ERROR;
#endif

/*!
* Gyro config Data, set on receiving configuration data 
*/
static trOdoConfigData *prOdoConfigData = OSAL_NULL;

/* the flag is set when config data is obtained */
static tBool bOdoConfigEn = FALSE;

/*Odometer Open Flag*/ 
static tBool bisOdoOpen= FALSE; //TO be used in future

/*Configuration message buffer */
tU8 pu8OdoConfigBuffer[ODO_MSG_QUE_LENGTH]={ 0 }; 

/* Odomenter Counter */
/* This holds the previous odometer counter value received from V850 */
static tU32 u32OdoPrevCntr;
/* This holds the last updated odometer value to VD-Sensor */
static tU32 u32OdoCntr;
/* First odometer value always needs to be Zero. 
   This may not be the case in some projects. Hence we usually ignore first value
   and compute diffence from second value onwards. */
static tBool bFirstValue;

/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
#ifdef ODO_THREAD_ACTIVE
static tVoid vOdoMainThread(tVoid);
#endif

static tVoid DEV_ODO_vTraceOut(TR_tenTraceLevel u32Level,const tChar *pcFormatString,... );

static tVoid Odo_vUpdateCounter( OSAL_trIOCtrlOdometerData* prIOOdoData );

/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
/********************************************************************************
* FUNCTION         : ODOMETER_S32IOOpen 

* PARAMETER       : NONE
*                                                      
* RETURNVALUE   : OSAL_E_NOERROR  on sucess
*                         OSAL_ERROR on Failure
*
* DESCRIPTION    : Opens the Odometer device
*
* HISTORY           :
*-----------------------------------------------------------------------------
* Date       |       Version      | Author & comments
*------------|--------------------|-------------------------------------------
* 14.MAR.2013| Initial version:1.0| Niyatha S Rao (RBEI/ECF5)
* -----------------------------------------------------------------------------
* 06/26/2014 | Version 1.1        | Madhu Kiran Ramachandra (RBEI/ECF5)
*            |                    | Moved Config message receive section to open 
* -------------------------------------------------------------------------
**********************************************************************************/
tS32 ODOMETER_s32IOOpen(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;
   DEV_ODO_vTraceOut( TR_LEVEL_USER_1, "Entered ODOMETER_S32IOOpen");
   bFirstValue = TRUE;
   u32OdoPrevCntr = 0;
   u32OdoCntr  = 0;
   
   if (bisOdoOpen == FALSE)
   {
#ifdef ODO_THREAD_ACTIVE
      /*Create and Spawn thread to  wait on message on message queue*/        
      OSAL_trThreadAttribute rThreadAttr;
      rThreadAttr.szName       = (tString)ODOMETER_C_STRING_MAIN_THREAD_NAME;
      rThreadAttr.s32StackSize = OSALCORE_C_U32_STACK_SIZE_ODOMETER;
      rThreadAttr.u32Priority  = OSALCORE_C_U32_PRIORITY_ODOMETER;
      rThreadAttr.pfEntry      = ( OSAL_tpfThreadEntry )vOdoMainThread;
      rThreadAttr.pvArg        = (tPVoid)OSAL_NULL;
#endif
      //sensor Dispatcher initialization 
      if(OSAL_OK != s32SensorDiapatcherInit(SEN_DISP_ODO_ID)) /* init dispatcher */
      {
         DEV_ODO_vTraceOut( TR_LEVEL_ERRORS, "s32SensorDiapatcherInit Failed" );
      }
      //sensor_ring_buffer open
      else if(OSAL_OK != SenRingBuff_s32Open(SEN_DISP_ODO_ID))   /* open ring buffer for handle */
      {
         DEV_ODO_vTraceOut( TR_LEVEL_ERRORS, "SenRingBuff_s32Open Failed" );
         (tVoid)s32SensorDiapatcherDeInit( SEN_DISP_ODO_ID );
      }
      /*open the message queue */
      else if(OSAL_OK != (OSAL_s32MessageQueueOpen((tCString)SEN_DISP_TO_ODO_MSGQUE_NAME,
         (OSAL_tenAccess)OSAL_EN_READWRITE, &hldOdoMsgQueue)))
      {
         DEV_ODO_vTraceOut(TR_LEVEL_ERRORS,"OSAL_s32MessageQueueOpen FAILED, Err = %u",
                                            OSAL_u32ErrorCode());
         (tVoid)s32SensorDiapatcherDeInit( SEN_DISP_ODO_ID );               
         (tVoid)SenRingBuff_s32Close(SEN_DISP_ODO_ID);
      }
      else 
      {
         if ( FALSE == bOdoConfigEn )
         {
            if ( (tS32)sizeof(trOdoConfigData) != OSAL_s32MessageQueueWait(
                                                   hldOdoMsgQueue,
                                                   pu8OdoConfigBuffer,
                                                   ODO_MSG_QUE_LENGTH,
                                                   OSAL_NULL,
                                                   ODOMETER_INIT_CONFIG_MSG_WAIT_TIME_MS) )
            {
               DEV_ODO_vTraceOut( TR_LEVEL_FATAL, "Init:MQ wait fail err %lu", OSAL_u32ErrorCode());
               (tVoid)OSAL_s32MessageQueueClose( hldOdoMsgQueue);
               (tVoid)SenRingBuff_s32Close(SEN_DISP_ODO_ID );
               (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_ODO_ID);
            }
            else if ( SEN_DISP_MSG_TYPE_CONFIG != pu8OdoConfigBuffer[0] )
            {
               DEV_ODO_vTraceOut( TR_LEVEL_FATAL, "In-correct config Msg. ID %d", (tS32)pu8OdoConfigBuffer[0] );
               (tVoid)OSAL_s32MessageQueueClose( hldOdoMsgQueue);
               (tVoid)SenRingBuff_s32Close(SEN_DISP_ODO_ID );
               (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_ODO_ID);
            }
            else
            {
               /* Store configuration */
               bOdoConfigEn = TRUE;
               prOdoConfigData = (trOdoConfigData*)(pu8OdoConfigBuffer);
               DEV_ODO_vTraceOut(TR_LEVEL_COMPONENT, "INIT: Odo Int:%d", prOdoConfigData->u16OdoDataInterval );
            }
         }
         if ( TRUE == bOdoConfigEn )
         {
            /* Start Odo Thread. 
               Currently we don't need the thread here. This shall be enabled again if need arises */
#ifdef ODO_THREAD_ACTIVE
            bOdoThreadRunning = TRUE;
            s32OdoThreadID = OSAL_ThreadSpawn(&rThreadAttr);
            if( s32OdoThreadID == OSAL_ERROR)
            {
               DEV_ODO_vTraceOut(TR_LEVEL_ERRORS,"ODO ThreadSpawn FAILED, Err = %u", OSAL_u32ErrorCode());
               (tVoid)s32SensorDiapatcherDeInit( SEN_DISP_ODO_ID );
               (tVoid)SenRingBuff_s32Close(SEN_DISP_ODO_ID);   
               (tVoid)OSAL_s32MessageQueueClose( hldOdoMsgQueue);
               bOdoThreadRunning = FALSE;
            }
            else
            {
#else
            /* If we dont have thread, close Message Queue. This will be of no use any more */
            if( OSAL_OK != ( OSAL_s32MessageQueueClose( hldOdoMsgQueue)))
            {
                /* error, Message Queue close failed */
                DEV_ODO_vTraceOut( TR_LEVEL_FATAL,"vOdoMainThread: Close MessageQueue FAILED, Err = %u", OSAL_u32ErrorCode());
             }   

#endif
               DEV_ODO_vTraceOut(TR_LEVEL_USER_4, "Odo Init SUCCESS" );
               s32RetVal=OSAL_E_NOERROR;
               bisOdoOpen=TRUE;
#ifdef ODO_THREAD_ACTIVE
            }
#endif
         }
      }
   }
   else
   {
      s32RetVal=OSAL_E_ALREADYOPENED;
      DEV_ODO_vTraceOut(TR_LEVEL_ERRORS,"ODOMETER_S32IOOpen:ODO driver already opened");
   }
   return s32RetVal;
}
#ifdef ODO_THREAD_ACTIVE
/********************************************************************************
* FUNCTION   : vOdoMainThread 
*
* PARAMETER  : NONE
*                                                      
* RETURNVALUE: OSAL_E_NOERROR  on sucess
*              OSAL_ERROR on Failure
*
* DESCRIPTION:  Osal thread function spawned in @ref ODOMETER_S32IOOpen
*               Is used to wait for Configuration, Temperature and Error messages 
*               Sent by the dispatcher via message queue
* HISTORY    :
*------------|--------------------|-------------------------------------------
* 14.MAR.2013| Initial version:1.0| Niyatha S Rao (RBEI/ECF5)
* -----------------------------------------------------------------------------
* 06/26/2014 | Version 1.1        | Madhu Kiran Ramachandra (RBEI/ECF5)
*            |                    | Moved Config message receive section to open 
* -------------------------------------------------------------------------
**********************************************************************************/
static tVoid vOdoMainThread(tVoid)
{
   trOdoConfigData *pConfigData = (trOdoConfigData*)(pu8OdoConfigBuffer);  

   DEV_ODO_vTraceOut(TR_LEVEL_USER_1, "vOdoMainThread: ENTER");

   while( bOdoThreadRunning == TRUE ) //to continuously wait for the message on the message queue
   {
      DEV_ODO_vTraceOut(TR_LEVEL_USER_4, "vOdoMainThread: Waiting on Msg Queue");
      if(OSAL_s32MessageQueueWait( hldOdoMsgQueue,(tPU8)pu8OdoConfigBuffer,sizeof(pu8OdoConfigBuffer),
                             OSAL_NULL, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) == 0 )                             
      {
         DEV_ODO_vTraceOut(TR_LEVEL_ERRORS, "vOdoMainThread: Message Queue Wait failed with error code =%u", OSAL_u32ErrorCode());
      }
      else if( bOdoThreadRunning == FALSE )
      { 
         DEV_ODO_vTraceOut(TR_LEVEL_USER_4, "vOdoMainThread: Closing the Message Queue");
         /* close Message Queue */
         if( OSAL_OK != ( OSAL_s32MessageQueueClose( hldOdoMsgQueue)))
         {
            /* error, Message Queue close failed */
            DEV_ODO_vTraceOut( TR_LEVEL_FATAL,"vOdoMainThread: Close MessageQueue FAILED, Err = %u", OSAL_u32ErrorCode());
         }   
         else
         {
            hldOdoMsgQueue = OSAL_NULL;
            DEV_ODO_vTraceOut(TR_LEVEL_USER_1, "vOdoMainThread: Message Queue closed SUCCESSFULLY");
         }
      }
      else if (pConfigData->u8MsgType != SEN_DISP_MSG_TYPE_CONFIG)
      {
         DEV_ODO_vTraceOut( TR_LEVEL_FATAL,"vOdoMainThread: Unknown Msg Type");
      }
      else
      {
         //NIYATHA: there is no other configuration message possible for ODO */
         DEV_ODO_vTraceOut( TR_LEVEL_FATAL,"ODO Config Message not Expected in run time");
/*          Moved Config message receive section to open call
         bOdoConfigEn = TRUE;
         prOdoConfigData = (trOdoConfigData*)(pu8OdoConfigBuffer);
         DEV_ODO_vTraceOut( TR_LEVEL_USER_4, "OdoDataInterval %d", prOdoConfigData->u16OdoDataInterval);
         */
      }
   }
   DEV_ODO_vTraceOut( TR_LEVEL_USER_1, "vOdoMainThread: EXIT");
   OSAL_vThreadExit();
}
#endif

/********************************************************************************
* FUNCTION         : ODOMETER_s32IOClose 

* PARAMETER       : NONE
*                                                      
* RETURNVALUE   : OSAL_E_NOERROR  on sucess
*                         OSAL_ERROR on Failure
*
* DESCRIPTION    :  Close the Odometer device
*
* HISTORY           :
*------------------------------------------------------------------------
* Date            |       Version                | Author & comments
*--------------|--------------------|-------------------------------------
* 14.MAR.2013   | Initial version: 1.0      | Niyatha S Rao (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
tS32 ODOMETER_s32IOClose(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;
#ifdef ODO_THREAD_ACTIVE
   /*used to stop thread in conjunction with the event post below*/
   tU8 myTerminateMsg = 0;
#endif
   
   DEV_ODO_vTraceOut(TR_LEVEL_USER_1,"ODOMETER_s32IOClose ENTER");

   if(bisOdoOpen==TRUE)
   {
#ifdef ODO_THREAD_ACTIVE
      bOdoThreadRunning = FALSE;

      if (OSAL_OK != OSAL_s32MessageQueuePost(hldOdoMsgQueue,
                                         (tCU8*)&myTerminateMsg,
                                          sizeof(tU8), 
                                          OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST)) 
      {
         DEV_ODO_vTraceOut(TR_LEVEL_FATAL, "ODOMETER_s32IOClose: MessageQueue Post FAILED, Err = %u", OSAL_u32ErrorCode());
         s32RetVal = OSAL_ERROR;
      }
#endif
      /* close Ring Buffer */
      if( OSAL_OK != SenRingBuff_s32Close(SEN_DISP_ODO_ID))
      {
         /* error, Message Queue close failed */
         s32RetVal = OSAL_ERROR;
         DEV_ODO_vTraceOut(TR_LEVEL_FATAL,"ODOMETER_s32IOClose :senRingBuff close FAILED, Err = %u", OSAL_u32ErrorCode());
      }   
   
      if(OSAL_OK != s32SensorDiapatcherDeInit( SEN_DISP_ODO_ID ))
      {
         s32RetVal = OSAL_ERROR;
         DEV_ODO_vTraceOut( TR_LEVEL_FATAL,"ODOMETER_s32IOClose :sensor dispatcher deInit FAILED, Err = %u", OSAL_u32ErrorCode());
      }   

      else
      {
         s32RetVal = OSAL_E_NOERROR;
         bisOdoOpen= FALSE;
         DEV_ODO_vTraceOut(TR_LEVEL_USER_1,"ODOMETER_s32IOClose: Device closed successfully");   

      }
   }
   
   else
   {
      s32RetVal=OSAL_ERROR;
      DEV_ODO_vTraceOut(TR_LEVEL_FATAL, "ODOMETER_s32IOClose: Device is NOT OPEN");      
   }

   DEV_ODO_vTraceOut(TR_LEVEL_USER_1,"ODOMETER_s32IOClose: EXIT");
   return(s32RetVal);
}


/********************************************************************************
* FUNCTION         : ODOMETER_s32IORead 

* PARAMETER       : pBuffer, pointer to struct @ref OSAL_trIOCtrlOdometerData
*                  where the read data must be copied
*                                                      
* RETURNVALUE   :  Number of bytes read on sucess
*                         OSAL_ERROR on Failure
*
* DESCRIPTION    :  Reads the Odometer data
*
* HISTORY           :
*------------------------------------------------------------------------
* Date            |       Version                | Author & comments
*--------------|--------------------|-------------------------------------
* 14.MAR.2013   | Initial version: 1.0      | Niyatha S Rao (RBEI/ECF5)
* ------------------------------------------------------------------------
**********************************************************************************/
tS32 ODOMETER_s32IORead( tPS8 pBuffer, tU32 u32maxbytes)
{
   tU32  u32NumberOfEntries = u32maxbytes / sizeof( OSAL_trIOCtrlOdometerData );
   tS32  s32RetVal = OSAL_E_NOERROR;
   OSAL_trIOCtrlOdometerData* prIOOdoData   = OSAL_NULL;
   tS32 s32Cnt;

   DEV_ODO_vTraceOut( TR_LEVEL_USER_4, "ODOMETER_s32IORead : requested records =  %d", u32NumberOfEntries);
   
   if(pBuffer == OSAL_NULL)
   {
       DEV_ODO_vTraceOut(TR_LEVEL_ERRORS,"ODOMETER_s32IORead :Input Buffer is a NULL pointer");
       s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else
   {
      prIOOdoData = (OSAL_trIOCtrlOdometerData*)((tPVoid)pBuffer);   

      s32RetVal = SenRingBuff_s32Read(SEN_DISP_ODO_ID, (tPVoid) prIOOdoData, u32NumberOfEntries );
      
      if((tS32)OSAL_ERROR == s32RetVal)
      {
         DEV_ODO_vTraceOut( TR_LEVEL_ERROR, "ODOMETER_s32IORead :Read from ring buffer failed" ); 

      }
      else if((tS32)OSAL_E_TIMEOUT == s32RetVal)
      {
         DEV_ODO_vTraceOut( TR_LEVEL_ERROR, "ODOMETER_s32IORead :Read from ring buffer failed due to timeout" ); 
      }   
      else
      {
         DEV_ODO_vTraceOut(TR_LEVEL_USER_4 ,"Num of reaords read from RB %d",s32RetVal);
         for (s32Cnt =0; s32Cnt < s32RetVal ; s32Cnt++)
         {
            Odo_vUpdateCounter( (prIOOdoData + s32Cnt) );
         }
         s32RetVal = (s32RetVal * (tS32)sizeof(OSAL_trIOCtrlOdometerData));
      }
   }
   return(s32RetVal);    /*return no. of bytes written into Readbuffer*/
}
/****************************************************************************************
* FUNCTION     : Odo_vUpdateCounter 

* PARAMETER    : prIOOdoData: pointer to odmeter data for which counter has to be updated
*                                                      
* RETURNVALUE  : None
*
* DESCRIPTION  :  Reads the Odometer data
*
* HISTORY      :
*---------------------------------------------------------------------------------------
* Date         |       Version          | Author & comments
*--------------|------------------------|-----------------------------------------------
* 21.OCT.2013  | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* --------------------------------------------------------------------------------------
**********************************************************************************/

static tVoid Odo_vUpdateCounter( OSAL_trIOCtrlOdometerData* prIOOdoData )
{
   tS32 s32TicksDiff = 0;
   tU32 u32CurrentCntr = 0;
   /* Error Counter to keep track of how many unknown and invalid records 
       are received before a valid record is encountered. */
   static tU16 u16ErrorCount = 0;

   if(OSAL_NULL == prIOOdoData)
   {
      DEV_ODO_vTraceOut(TR_LEVEL_FATAL, "pointers to Odo_vUpdateCounter are NULL");
   }
   /* Check if this is a timeout record. */
   else if(0 == prIOOdoData->u16ErrorCounter)
   {
      if(prIOOdoData->enOdometerStatus != ODOMSTATE_CONNECTED_DATA_INVALID)
      {
         u32CurrentCntr = prIOOdoData->u32WheelCounter;
         DEV_ODO_vTraceOut( TR_LEVEL_USER_4, "before WheelCounter %lu, dir %d",
                            prIOOdoData->u32WheelCounter, prIOOdoData->enDirection  );
         /* Dont pass the first value to application but store it for further calculations*/
         if( FALSE == bFirstValue )
         {
            /* Check the ticks counted from previous signal */
            s32TicksDiff = (tS32)u32CurrentCntr - (tS32)u32OdoPrevCntr;
            /* Corrections for overflows */
            if (s32TicksDiff < 0)
            {
               s32TicksDiff += ODOMETER_COUNTER_RANGE + 1;
            }
            
            switch( prIOOdoData->enDirection ) 
            {
               case OSAL_EN_RFS_FORWARD :
                  u32OdoCntr += (tU32)s32TicksDiff;
                  u16ErrorCount = 0; // counter value is reset to 0 on receiving a  valid record
                  DEV_ODO_vTraceOut( TR_LEVEL_USER_4, "ODO direction received : FORWARD");
                  break;
                  
               case OSAL_EN_RFS_REVERSE :
                  u32OdoCntr -= (tU32)s32TicksDiff;
                  u16ErrorCount = 0; // counter value is reset to 0 on receiving a  valid record
                  DEV_ODO_vTraceOut( TR_LEVEL_USER_4, "ODO direction received : REVERSE");
                  break;

               case OSAL_EN_RFS_UNKNOWN :
                  bFirstValue = TRUE;
                  ++u16ErrorCount;   // increment the counter value on receiving Unknown value
                  --prIOOdoData->u16OdoMsgCounter;
                  prIOOdoData->enOdometerStatus = ODOMSTATE_CONNECTED_NOINFO;
                  DEV_ODO_vTraceOut( TR_LEVEL_USER_4, "ODO direction received : UNKNOWN");
                  break;

               default :
                  bFirstValue = TRUE;
                  ++u16ErrorCount;   // This case should never occur. Handling done here is redundant
                  --prIOOdoData->u16OdoMsgCounter;
                  prIOOdoData->enOdometerStatus = ODOMSTATE_CONNECTED_DATA_INVALID;
                  DEV_ODO_vTraceOut( TR_LEVEL_ERROR, "ODO direction received case : DEFAULT -> This case should never occur");
                  break;
            }
         }
         else
         {
            /* when UNKNOWN values are received consecutively, ErrorCount and MsgCounter values are updated accordingly */
            /* when a valid direction (FORWARD or REVERSE) is received, bFirstValue is set to FALSE and normal operation is resumed from the next cycle */
            if( prIOOdoData->enDirection != OSAL_EN_RFS_UNKNOWN )
            {
               bFirstValue = FALSE;
            }
            else
            {
               ++u16ErrorCount;
               --prIOOdoData->u16OdoMsgCounter;
               prIOOdoData->enOdometerStatus = ODOMSTATE_CONNECTED_NOINFO;
               DEV_ODO_vTraceOut( TR_LEVEL_USER_4, "ODO direction received : UNKNOWN");
            }
         }
         u32OdoPrevCntr = u32CurrentCntr;
      }
      else
      {
         bFirstValue = TRUE;
         ++u16ErrorCount;   // increment the counter value on receiving an invalid record
         --prIOOdoData->u16OdoMsgCounter;
         DEV_ODO_vTraceOut(TR_LEVEL_ERROR, "ODO Data received is INVALID");
      }

      prIOOdoData->u32WheelCounter = u32OdoCntr;
      prIOOdoData->u16ErrorCounter = u16ErrorCount;
      DEV_ODO_vTraceOut(TR_LEVEL_USER_1, "Direction: %d",prIOOdoData->enDirection);
      DEV_ODO_vTraceOut(TR_LEVEL_USER_1, "Error Counter: %d",prIOOdoData->u16ErrorCounter);
      DEV_ODO_vTraceOut(TR_LEVEL_USER_1, "Odometer Status %lu", prIOOdoData->enOdometerStatus);
      DEV_ODO_vTraceOut(TR_LEVEL_USER_1, "WheelCounter %lu", prIOOdoData->u32WheelCounter);
      DEV_ODO_vTraceOut(TR_LEVEL_USER_1, "TimeStamp %lu", prIOOdoData->u32TimeStamp);
   }
   else
   {
      /* Just Ignore the timeout records */
      prIOOdoData->u32WheelCounter = u32OdoCntr;
      ++u16ErrorCount;
      prIOOdoData->u16ErrorCounter = u16ErrorCount;
      DEV_ODO_vTraceOut(TR_LEVEL_USER_4, "Odo Timeout Recv; PrevValidWhlCnt:%lu "
                                         "Stat:%lu Dir:%d ErrCnt:%d TS:%lu",
                        prIOOdoData->u32WheelCounter,
                        prIOOdoData->enOdometerStatus,
                        prIOOdoData->enDirection,
                        prIOOdoData->u16ErrorCounter,
                        prIOOdoData->u32TimeStamp);
   }

}

/********************************************************************************
* FUNCTION        : ODOMETER_s32IOControl 

* PARAMETER       : s32fun :  Function identificator
*                   s32arg : Argument to be passed to function

* RETURNVALUE     : OSAL_E_NOERROR on sucess
*                   OSAL_ERROR on Failure

* DESCRIPTION     : Odometer driver control functions

* HISTORY         :
*------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------
* 14.MAR.2013  | Initial version: 1.0  | Niyatha S Rao (RBEI/ECF5)
*--------------|-----------------------|-------------------------------------
* 11.Dec.2015  | Version: 1.1          | Shivasharnappa Mothpalli (RBEI/ECF5)
*              |                       | Removed Unused IOCTRLS (Fix for  CFG3-1622) :
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_FLUSH,
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_GET_TIMEOUT_VALUE,
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_GETRESOLUTION,
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_SET_TIMEOUT_VALUE,
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_RESET ):
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_INIT 
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_GETDIRECTION 
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_SETDIRECTION 
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_GET_WHEELCOUNTER 
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_INIT_OVC 
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_INACTIVATE_OIC
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_ACTIVATE_OIC 
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_SET_K_VALUE 
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_PULSES_PER_REVOLUTION
               |                       | OSAL_C_S32_IOCTRL_ODOMETER_WHEEL_CIRCUMFERENCE 
*********************************************************************************/

tS32 ODOMETER_s32IOControl(tS32 s32fun, tLong sArg)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tPS32 ps32Argument = (tPS32) sArg;
   tS32 s32ErrChk = OSAL_ERROR;

   DEV_ODO_vTraceOut(TR_LEVEL_USER_1,"ODOMETER_s32IOControl: IOControl ENTERED");

   switch( s32fun)
   {
      case OSAL_C_S32_IOCTRL_VERSION:
      {
         /* check input parameter */
         if( ps32Argument != OSAL_NULL )
         {
            /*return initial version number*/
            *ps32Argument = (tS32)DEV_ODO_DRIVER_VERSION;
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTRL_ODOMETER_GETCNT:
      {
         /* check input parameter */
         if(ps32Argument != OSAL_NULL)
         {
            /*return number of valid ringbuffer entries*/
            s32ErrChk = SenRingBuff_s32GetAvailRecords(SEN_DISP_ODO_ID);
            if (OSAL_ERROR != s32ErrChk)
            {
            *ps32Argument =s32ErrChk;
            }
            else
            {
            s32RetVal =  (tS32)OSAL_u32ErrorCode();
            }
            DEV_ODO_vTraceOut(TR_LEVEL_USER_4,"SenRingBuff_s32GetAvailRecords ret : %d", s32ErrChk );
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

      case OSAL_C_S32_IOCTRL_ODOMETER_GETCYCLETIME:
      {
         if(bOdoConfigEn)
         {
            /* check input parameter */
            if( ps32Argument != OSAL_NULL )
            {
               *ps32Argument = (tS32)((prOdoConfigData->u16OdoDataInterval)*1000000);
            }
            else
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
         }
         else
         {
            s32RetVal =OSAL_E_INPROGRESS;
         }
         break;
      }

      default:
      {
         /* in case s32fun is neither of allowed above */
         s32RetVal = OSAL_E_NOTSUPPORTED;
         DEV_ODO_vTraceOut( TR_LEVEL_ERRORS, "ODOMETER_s32IOControl: IOControl Not supported");
         break;
      }
   }

   DEV_ODO_vTraceOut( TR_LEVEL_USER_1, "ODOMETER_s32IOControl: IOControl EXIT");
   return(s32RetVal);
}

/********************************************************************************
* FUNCTION         : DEV_ODO_vTraceOut 

* PARAMETER       : u32Level, Trace level
*                  pcFormatString, Trace string
*                                                      
* RETURNVALUE   : None
*
* DESCRIPTION    :  Trace out function for Odometer.
*
* HISTORY           :
*------------------------------------------------------------------------
* Date            |       Version                | Author & comments
*--------------|--------------------|-------------------------------------
* 14.MAR.2013   | Initial version: 1.0      | Niyatha S Rao (RBEI/ECF5)
* ------------------------------------------------------------------------
**********************************************************************************/
static tVoid DEV_ODO_vTraceOut(  TR_tenTraceLevel u32Level,const tChar *pcFormatString,... )
{
   if(LLD_bIsTraceActive((tU32)OSAL_C_TR_CLASS_DEV_ODO, (tU32)u32Level) != FALSE)
   {
      /*
      Parameter to hold the argument for a function, 
      specified the format string in pcFormatString
      defined as: typedef char* va_list in stdarg.h
      */

      /*
      vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure
      */      
      va_list argList;
      tS32 s32Size;
      
      /*
      Buffer to hold the string to trace out
      */
      tS8 u8Buffer[MAX_TRACE_SIZE] = { 0 };
      
      /*
      Position in buffer from where the format string is to be
      concatenated
      */
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /* Flush the String */
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /* Copy the String to indicate the trace is from the RTC device */

      /*
      Initialize the argList pointer to the beginning of the variable
      arguement list
      */
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
      Collect the format String's content into the remaining part of
      the Buffer
      */
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,sizeof(u8Buffer),
      pcFormatString, argList ) ) )
      {
         return;
      }

      /* Trace out the Message to TTFis */
      LLD_vTrace( (tU32)OSAL_C_TR_CLASS_DEV_ODO, (tU32)u32Level, u8Buffer, (tU32)s32Size );   /* Send string to Trace*/

      /*
      Performs the appropiate actions to facilitate a normal return by a
      function that has used the va_list object
      */
      va_end(argList);
      
   }
}

#ifdef LOAD_SENSOR_SO
tS32 odo_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
   (tVoid)s32Id;
   (tVoid)szName;
   (tVoid)enAccess;
   (tVoid)pu32FD;
   (tVoid)app_id;

   return ODOMETER_s32IOOpen();

}

tS32 odo_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   (tVoid)s32ID;
   (tVoid)u32FD;

   return ODOMETER_s32IOClose(); 

}

tS32 odo_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tLong sArg)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   return ODOMETER_s32IOControl(s32fun, sArg); 
}

tS32 odo_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   (tVoid)ret_size;
   return ODOMETER_s32IORead(pBuffer, u32Size);
}
#endif

/*End of file*/

