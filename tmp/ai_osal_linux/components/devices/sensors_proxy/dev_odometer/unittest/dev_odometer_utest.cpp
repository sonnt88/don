#define SENSPXYMOCKSTOBEREMOVED
#define ODO_THREAD_ACTIVE
#include "Sensor_InternalMock.h"
#include "Sensor_InternalMock.cpp"

#ifdef __cplusplus
extern "C"
{
#include "../source/dev_odometer.c"
}
#endif

using namespace testing;
using namespace std;

int main(int argc, char** argv)
{
   ::testing::InitGoogleMock(&argc, argv);
   return RUN_ALL_TESTS();
}

void Odometer_ResetGlobals()
{
   bisOdoOpen = FALSE;
   bOdoConfigEn = FALSE;
   bOdoThreadRunning =FALSE;
   bFirstValue = TRUE;
   u32OdoPrevCntr = 0;
   u32OdoCntr  = 0;
}

class Odometer_InitGlobals : public ::testing::Test
{
public:
   SensorMock PoS;
   NiceMock<OsalMock> Osal_mock;
   virtual void SetUp()
   {
      Odometer_ResetGlobals();
   }
};

class Odometer_Open : public Odometer_InitGlobals
{
public:
   virtual void SetUp()
   {
      tU8 pu8Buff[ODO_MSG_QUE_LENGTH];
      pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
      pu8Buff[2] = 50;
      pu8Buff[3] = 0;
      Odometer_InitGlobals::SetUp();
      EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trOdoConfigData))));
      ODOMETER_s32IOOpen();
   }
};

class Odometer_IgnoreFirstRecord : public Odometer_Open
{
public:
   virtual void SetUp()
   {
      //Create a dummy record
      OSAL_trIOCtrlOdometerData pDummyFirstRecord;
      pDummyFirstRecord.u16ErrorCounter= 0;
      pDummyFirstRecord.enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
      pDummyFirstRecord.enDirection = OSAL_EN_RFS_FORWARD;
      pDummyFirstRecord.u32WheelCounter = 100;
      pDummyFirstRecord.u32TimeStamp = 115200;
      pDummyFirstRecord.u16OdoMsgCounter = 1;
      pDummyFirstRecord.u8FillByte1 = 0;
      pDummyFirstRecord.u8FillByte2 = 0;
      pDummyFirstRecord.u8FillByte3 = 0;
      pDummyFirstRecord.u8FillByte4 = 0;
      pDummyFirstRecord.u8FillByte5 = 0;
      Odometer_Open::SetUp();
      Odo_vUpdateCounter(&pDummyFirstRecord);
   }
};


/********************Odometer Open********************/

TEST_F(Odometer_InitGlobals, Odo_Open_Success)
{
   tS32 s32RetVal = OSAL_ERROR;
   tU8 pu8Buff[ODO_MSG_QUE_LENGTH];
   //Create a dummy record
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trOdoConfigData))));
   s32RetVal=ODOMETER_s32IOOpen();
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(TRUE, bOdoConfigEn);
   EXPECT_EQ(TRUE, bisOdoOpen);
   EXPECT_EQ(TRUE, bOdoThreadRunning);
}

TEST_F(Odometer_InitGlobals, Odo_Open_DispError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[ODO_MSG_QUE_LENGTH];
   //Create a dummy record
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal=ODOMETER_s32IOOpen();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bOdoConfigEn);
   EXPECT_EQ(FALSE, bisOdoOpen);
   EXPECT_EQ(FALSE, bOdoThreadRunning);
}

TEST_F(Odometer_InitGlobals, Odo_Open_RngBuffError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[ODO_MSG_QUE_LENGTH];
   //Create a dummy record
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal=ODOMETER_s32IOOpen();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bOdoConfigEn);
   EXPECT_EQ(FALSE, bisOdoOpen);
   EXPECT_EQ(FALSE, bOdoThreadRunning);
}

TEST_F(Odometer_InitGlobals, Odo_Open_MsgQueOpenError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[ODO_MSG_QUE_LENGTH];
   //Create a dummy record
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal=ODOMETER_s32IOOpen();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bOdoConfigEn);
   EXPECT_EQ(FALSE, bisOdoOpen);
   EXPECT_EQ(FALSE, bOdoThreadRunning);
}

TEST_F(Odometer_InitGlobals, Odo_Open_MsgQueWaitError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[ODO_MSG_QUE_LENGTH];
   //Create a dummy record
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(Return(0));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal=ODOMETER_s32IOOpen();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bOdoConfigEn);
   EXPECT_EQ(FALSE, bisOdoOpen);
   EXPECT_EQ(FALSE, bOdoThreadRunning);
}

TEST_F(Odometer_InitGlobals, Odo_Open_ThdSpwnError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[ODO_MSG_QUE_LENGTH];
   //Create a dummy record
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trOdoConfigData))));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(OSAL_ERROR));   
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal=ODOMETER_s32IOOpen();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(TRUE, bOdoConfigEn);
   EXPECT_EQ(FALSE, bisOdoOpen);
   EXPECT_EQ(FALSE, bOdoThreadRunning);
}

TEST_F(Odometer_InitGlobals, Odo_Open_AlreadyOpen)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   bisOdoOpen = TRUE;
   s32RetVal=ODOMETER_s32IOOpen();
   EXPECT_EQ(OSAL_E_ALREADYOPENED, s32RetVal);
}

/********************Odometer Close********************/
TEST_F(Odometer_Open, Odo_Close_success)
{
   tS32 s32RetVal = OSAL_ERROR;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal=ODOMETER_s32IOClose();
   EXPECT_EQ(FALSE, bisOdoOpen);
   EXPECT_EQ(FALSE, bOdoThreadRunning);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(Odometer_Open, Odo_CloseRngBuff_DispError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, u32ErrorCode()).Times(2).WillRepeatedly(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal=ODOMETER_s32IOClose();
   EXPECT_EQ(TRUE, bisOdoOpen);
   EXPECT_EQ(FALSE, bOdoThreadRunning);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(Odometer_Open, Odo_Close_DispError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ODO_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal=ODOMETER_s32IOClose();
   EXPECT_EQ(TRUE, bisOdoOpen);
   EXPECT_EQ(FALSE, bOdoThreadRunning);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(Odometer_Open, Odo_Close_OdoNotOpen)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   bisOdoOpen = FALSE;
   s32RetVal=ODOMETER_s32IOClose();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/********************Odometer Read********************/
TEST_F(Odometer_Open, Odo_Read_success)
{
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 140;
   OSAL_trIOCtrlOdometerData *pOdoData;
   //Create a dummy record
   pOdoData = (OSAL_trIOCtrlOdometerData *)malloc(sizeof(OSAL_trIOCtrlOdometerData)*5);

   for (int i =0; i<5; i++)
   {
      pOdoData[i].u16ErrorCounter= 0;
      pOdoData[i].enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
      pOdoData[i].enDirection = OSAL_EN_RFS_FORWARD;
      pOdoData[i].u32WheelCounter = 100;
      pOdoData[i].u32TimeStamp = 115200;
      pOdoData[i].u16OdoMsgCounter = 1;
      pOdoData[i].u8FillByte1 = 0;
      pOdoData[i].u8FillByte2 = 0;
      pOdoData[i].u8FillByte3 = 0;
      pOdoData[i].u8FillByte4 = 0;
      pOdoData[i].u8FillByte5 = 0;
   }
   EXPECT_CALL(PoS, s32SensRingBuffs32Read(_,_,_)).Times(1).WillOnce(Return(5));
   s32RetVal = ODOMETER_s32IORead((tPS8)pOdoData, u32maxbytes);
   EXPECT_EQ(140, s32RetVal);
}

TEST_F(Odometer_Open, Odo_Read_NoData)
{
   OSAL_trIOCtrlOdometerData *pOdoData = {0};
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 18;
   s32RetVal = ODOMETER_s32IORead((tPS8)pOdoData, u32maxbytes);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(Odometer_Open, Odo_Read_RngBuffReadFail)
{
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 140;
   OSAL_trIOCtrlOdometerData *pOdoData[5];
   //Create a dummy record
   for (int i =0; i<5; i++)
   {
      pOdoData[i] = (OSAL_trIOCtrlOdometerData *)malloc(sizeof(OSAL_trIOCtrlOdometerData));
      pOdoData[i]->u16ErrorCounter= 0;
      pOdoData[i]->enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
      pOdoData[i]->enDirection = OSAL_EN_RFS_FORWARD;
      pOdoData[i]->u32WheelCounter = 100;
      pOdoData[i]->u32TimeStamp = 115200;
      pOdoData[i]->u16OdoMsgCounter = 5;
      pOdoData[i]->u8FillByte1 = 0;
      pOdoData[i]->u8FillByte2 = 0;
      pOdoData[i]->u8FillByte3 = 0;
      pOdoData[i]->u8FillByte4 = 0;
      pOdoData[i]->u8FillByte5 = 0;
   }
   EXPECT_CALL(PoS, s32SensRingBuffs32Read(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = ODOMETER_s32IORead((tPS8)pOdoData, u32maxbytes);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(Odometer_Open, Odo_Read_RngBuffReadTimeout)
{
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 140;
   OSAL_trIOCtrlOdometerData *pOdoData[5];
   //Create a dummy record
   for (int i =0; i<5; i++)
   {
      pOdoData[i] = (OSAL_trIOCtrlOdometerData *)malloc(sizeof(OSAL_trIOCtrlOdometerData));
      pOdoData[i]->u16ErrorCounter= 0;
      pOdoData[i]->enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
      pOdoData[i]->enDirection = OSAL_EN_RFS_FORWARD;
      pOdoData[i]->u32WheelCounter = 100;
      pOdoData[i]->u32TimeStamp = 115200;
      pOdoData[i]->u16OdoMsgCounter = 5;
      pOdoData[i]->u8FillByte1 = 0;
      pOdoData[i]->u8FillByte2 = 0;
      pOdoData[i]->u8FillByte3 = 0;
      pOdoData[i]->u8FillByte4 = 0;
      pOdoData[i]->u8FillByte5 = 0;
   }
   EXPECT_CALL(PoS, s32SensRingBuffs32Read(_,_,_)).Times(1).WillOnce(Return(OSAL_E_TIMEOUT));
   s32RetVal = ODOMETER_s32IORead((tPS8)pOdoData, u32maxbytes);
   EXPECT_EQ(OSAL_E_TIMEOUT, s32RetVal);
}

/********************Odometer IOControl********************/
TEST_F(Odometer_Open, Odo_IOCtrl_ErrorCase)
{
   //similar to other IOControls, raken IOCTRL_VERSION as example
   tS32 s32RetVal = OSAL_E_NOERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_VERSION;
   tLong sArg = 0;
   s32RetVal = ODOMETER_s32IOControl(s32fun, sArg);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(Odometer_Open, Odo_IOCtrl_VerSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_VERSION;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   s32RetVal = ODOMETER_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(DEV_ODO_DRIVER_VERSION, s32dummyarg);
}

TEST_F(Odometer_Open, Odo_IOCtrl_GetCntSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_ODOMETER_GETCNT;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   EXPECT_CALL(PoS, s32SensRngBuffGetAvailRecs(_)).Times(1).WillOnce(Return(100));
   s32RetVal = ODOMETER_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(100, s32dummyarg);
}

TEST_F(Odometer_Open, Odo_IOCtrl_GetCycleTimeSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_ODOMETER_GETCYCLETIME;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   s32RetVal = ODOMETER_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(50000000, s32dummyarg);
}

TEST_F(Odometer_Open, Odo_IOCtrl_GetCycleTimeFailureCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_ODOMETER_GETCYCLETIME;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   bOdoConfigEn = FALSE;
   s32RetVal = ODOMETER_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_INPROGRESS, s32RetVal);
}

TEST_F(Odometer_Open, Odo_IOCtrl_DefaultCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32func = 100;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   s32RetVal = ODOMETER_s32IOControl(s32func, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOTSUPPORTED, s32RetVal);
}

/********************Odometer UpdateCounter********************/

TEST_F(Odometer_Open, Odo_UpdateCounter_InvalidRecord)
{
   OSAL_trIOCtrlOdometerData pOdoData;
   pOdoData.u16ErrorCounter= 0;
   pOdoData.enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
   pOdoData.enDirection = OSAL_EN_RFS_UNKNOWN;
   pOdoData.u32WheelCounter = 0;
   pOdoData.u32TimeStamp = 115200;
   pOdoData.u16OdoMsgCounter = 1;
   pOdoData.u8FillByte1 = 0;
   pOdoData.u8FillByte2 = 0;
   pOdoData.u8FillByte3 = 0;
   pOdoData.u8FillByte4 = 0;
   pOdoData.u8FillByte5 = 0;
   u32OdoCntr =  1000; //dummy previous counter value
   Odo_vUpdateCounter(&pOdoData);
   EXPECT_EQ(TRUE, bFirstValue);
   EXPECT_EQ(1000, pOdoData.u32WheelCounter);
   EXPECT_EQ(1, pOdoData.u16ErrorCounter);
   EXPECT_EQ(0, pOdoData.u16OdoMsgCounter);
}

TEST_F(Odometer_Open, Odo_UpdateCounter_IgnoreFirstRecord)
{
   OSAL_trIOCtrlOdometerData pOdoData;;
   pOdoData.u16ErrorCounter= 0;
   pOdoData.enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
   pOdoData.enDirection = OSAL_EN_RFS_FORWARD;
   pOdoData.u32WheelCounter = 100;
   pOdoData.u32TimeStamp = 115200;
   pOdoData.u16OdoMsgCounter = 1;
   pOdoData.u8FillByte1 = 0;
   pOdoData.u8FillByte2 = 0;
   pOdoData.u8FillByte3 = 0;
   pOdoData.u8FillByte4 = 0;
   pOdoData.u8FillByte5 = 0;
   Odo_vUpdateCounter(&pOdoData);
   EXPECT_EQ(FALSE, bFirstValue);
   EXPECT_EQ(1, pOdoData.u16OdoMsgCounter);
   EXPECT_EQ(1, pOdoData.u16ErrorCounter);
}

TEST_F(Odometer_Open, Odo_UpdateCounter_TimeoutRecord)
{
   OSAL_trIOCtrlOdometerData pOdoData ;
   pOdoData.u16ErrorCounter= 0;
   pOdoData.enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
   pOdoData.enDirection = OSAL_EN_RFS_UNKNOWN;
   pOdoData.u32WheelCounter = 0;
   pOdoData.u32TimeStamp = 115200;
   pOdoData.u16OdoMsgCounter = 0;
   pOdoData.u8FillByte1 = 0;
   pOdoData.u8FillByte2 = 0;
   pOdoData.u8FillByte3 = 0;
   pOdoData.u8FillByte4 = 0;
   pOdoData.u8FillByte5 = 0;
   u32OdoCntr =  1000; //dummy previous counter value
   Odo_vUpdateCounter(&pOdoData);
   EXPECT_EQ(1000, pOdoData.u32WheelCounter);
   EXPECT_EQ(2, pOdoData.u16ErrorCounter);
}

TEST_F(Odometer_IgnoreFirstRecord, Odo_UpdateCounter_DirectionFwd)
{
   OSAL_trIOCtrlOdometerData pOdoData;
   pOdoData.u16ErrorCounter= 0;
   pOdoData.enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
   pOdoData.enDirection = OSAL_EN_RFS_FORWARD;
   pOdoData.u32WheelCounter = 150;
   pOdoData.u32TimeStamp = 115200;
   pOdoData.u16OdoMsgCounter = 0;
   pOdoData.u8FillByte1 = 0;
   pOdoData.u8FillByte2 = 0;
   pOdoData.u8FillByte3 = 0;
   pOdoData.u8FillByte4 = 0;
   pOdoData.u8FillByte5 = 0;
   Odo_vUpdateCounter(&pOdoData);
   EXPECT_EQ(50, pOdoData.u32WheelCounter);
   EXPECT_EQ(0, pOdoData.u16ErrorCounter);
   EXPECT_EQ(FALSE, bFirstValue);
}

TEST_F(Odometer_IgnoreFirstRecord, Odo_UpdateCounter_DirectionUnknown)
{
     OSAL_trIOCtrlOdometerData pOdoData;
   pOdoData.u16ErrorCounter= 0;
   pOdoData.enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
   pOdoData.enDirection = OSAL_EN_RFS_UNKNOWN;
   pOdoData.u32WheelCounter = 150;
   pOdoData.u32TimeStamp = 115200;
   pOdoData.u16OdoMsgCounter = 1;
   pOdoData.u8FillByte1 = 0;
   pOdoData.u8FillByte2 = 0;
   pOdoData.u8FillByte3 = 0;
   pOdoData.u8FillByte4 = 0;
   pOdoData.u8FillByte5 = 0;
   Odo_vUpdateCounter(&pOdoData);
   EXPECT_EQ(0, pOdoData.u32WheelCounter);
   EXPECT_EQ(1, pOdoData.u16ErrorCounter);
   EXPECT_EQ(0, pOdoData.u16OdoMsgCounter);
   EXPECT_EQ(ODOMSTATE_CONNECTED_NOINFO, pOdoData.enOdometerStatus);
   EXPECT_EQ(TRUE, bFirstValue);
}

TEST_F(Odometer_IgnoreFirstRecord, Odo_UpdateCounter_DirectionRev)
{
   OSAL_trIOCtrlOdometerData pOdoData;
   pOdoData.u16ErrorCounter= 0;
   pOdoData.enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
   pOdoData.enDirection = OSAL_EN_RFS_REVERSE;
   pOdoData.u32WheelCounter = 150;
   pOdoData.u32TimeStamp = 115200;
   pOdoData.u16OdoMsgCounter = 0;
   pOdoData.u8FillByte1 = 0;
   pOdoData.u8FillByte2 = 0;
   pOdoData.u8FillByte3 = 0;
   pOdoData.u8FillByte4 = 0;
   pOdoData.u8FillByte5 = 0;
   u32OdoCntr = 50; // set to fifty so that the wheelcounter does not become negative
   Odo_vUpdateCounter(&pOdoData);
   EXPECT_EQ(0, pOdoData.u32WheelCounter);
   EXPECT_EQ(0, pOdoData.u16ErrorCounter);
   EXPECT_EQ(FALSE, bFirstValue);
}

TEST_F(Odometer_IgnoreFirstRecord, Odo_UpdateCounter_DirectionInvalid)
{
   OSAL_trIOCtrlOdometerData pOdoData;
   pOdoData.u16ErrorCounter= 0;
   pOdoData.enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
   pOdoData.enDirection = (OSAL_tenIOCtrlOdometerRFS)999;
   pOdoData.u32WheelCounter = 150;
   pOdoData.u32TimeStamp = 115200;
   pOdoData.u16OdoMsgCounter = 1;
   pOdoData.u8FillByte1 = 0;
   pOdoData.u8FillByte2 = 0;
   pOdoData.u8FillByte3 = 0;
   pOdoData.u8FillByte4 = 0;
   pOdoData.u8FillByte5 = 0;
   Odo_vUpdateCounter(&pOdoData);
   EXPECT_EQ(0, pOdoData.u32WheelCounter);
   EXPECT_EQ(1, pOdoData.u16ErrorCounter);
   EXPECT_EQ(0, pOdoData.u16OdoMsgCounter);
   EXPECT_EQ(ODOMSTATE_CONNECTED_DATA_INVALID, pOdoData.enOdometerStatus);
   EXPECT_EQ(TRUE, bFirstValue);
}


//EOF
