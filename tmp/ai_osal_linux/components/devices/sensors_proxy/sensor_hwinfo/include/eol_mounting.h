/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    [short description]
 *
 * [long description]
 * 
 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History: 
 *
 ******************************************************************************/
#ifndef EOL_MOUNTING_H
#define EOL_MOUNTING_H
typedef struct 
{
   tU16 u16Alpha;   
   tU16 u16Beta;
   tU16 u16Gamma;
}tEulerAngles;

tBool bGetMountingfromEOL( OSAL_tr3dMountAngles *pr3dMountAngles );
#else
#warning eol_mounting.h included multiple times
#endif
