/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    [short description]
 *
 * [long description]
 * 
 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History: 
 *
 ******************************************************************************/
#ifndef GM_EULER_ROTATION_H
#define GM_EULER_ROTATION_H

typedef tF32 taf32Mat3d[9];

void vCalcRSTXYZangles( tU16 u16Alpha, tU16 u16Beta, tU16 u16Gamma, taf32Mat3d af32A );

#else
#warning gm_euler_rotation.h included multiple times
#endif
