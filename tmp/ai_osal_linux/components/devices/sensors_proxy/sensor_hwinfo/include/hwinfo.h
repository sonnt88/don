/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    [short description]
 *
 * [long description]
 * 
 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History: 
 *
 ******************************************************************************/
#ifndef HWINFO_H
#define HWINFO_H
/** enum to distinguish acc and gyro information */
typedef enum
{
   GYRO_ID = 1, /**< for gyro */
   ACC_ID  = 2, /**< for acc  */
}tEnSensor;
tBool SenHwInfo_bGetHwinfo ( tEnSensor enType , OSAL_trIOCtrlHwInfo *prIoCtrlHwInfo);
#else
#warning hwinfo.h included multiple times
#endif	/* HWINFO_H */
