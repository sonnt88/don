/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    conversion routines for the kdsfloats
 *
 * [long description]
 * 
 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History: 
 *
 ******************************************************************************/
#ifndef KDSFLOAT_H
#define KDSFLOAT_H
typedef struct
{
   tS16 s16Mant;
   tS8 s8Exp;
} trKdsFloat;

/* tF32 */
tF32 f32KdsFloatToF32( trKdsFloat rKdsFloatIn);
trKdsFloat rF32ToKdsFloat( tF32 f32In );
/* double */
double dKdsFloatToDouble(trKdsFloat rKdsFloatIn);
trKdsFloat rDoubleToKdsFloat (double dIn);
#else
#warning "kdsfloat.h included multiple times"
#endif //KDSFLOAT_H
