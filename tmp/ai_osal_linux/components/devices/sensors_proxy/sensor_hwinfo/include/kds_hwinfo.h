/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    [short description]
 *
 * [long description]
 * 
 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History:
 *
 ******************************************************************************/
#ifndef KDS_HWINFO_H
#define KDS_HWINFO_H
tBool SenHwInfo_bGetKdsHwinfo ( tEnSensor enType , OSAL_trIOCtrlHwInfo *prIoCtrlHwInfo);

/* All the following macros which provides the offset/entry in the KDS are derived from the diagnostics specification document
* in the section SensorConfigurationGyro, SensorConfigurationACC  for Gyro and ACC respectively*/

/* KDS DID(KDS key) Entry and Lenght for Gyro */
#define KDS_GYRO_DID_ENTRY_LEN 		     		(203)
#define KDS_GYRO_DID_ENTRY	             		(0x0700)

/* KDS DID(KDS key) Entry and Lenght for Acc */
#define KDS_ACC_DID_ENTRY_LEN 		     		(203)
#define KDS_ACC_DID_ENTRY	             		(0x0780)

/* KDS entry for GYRO hardware information */
#define KDS_GYRO_MAJ_VER_OFFSET					(8)
#define KDS_GYRO_MIN_VER_OFFSET					(9)
#define KDS_GYRO_ANG_RX_OFFSET					(11)
#define KDS_GYRO_ANG_RY_OFFSET					(12)
#define KDS_GYRO_ANG_RZ_OFFSET					(13)
#define KDS_GYRO_ANG_SX_OFFSET					(14)
#define KDS_GYRO_ANG_SY_OFFSET					(15)
#define KDS_GYRO_ANG_SZ_OFFSET					(16)
#define KDS_GYRO_ANG_TX_OFFSET					(17)
#define KDS_GYRO_ANG_TY_OFFSET					(18)
#define KDS_GYRO_ANG_TZ_OFFSET					(19)
#define KDS_GYRO_RAXES_BASE_OFFSET				(20)
#define KDS_GYRO_SAXES_BASE_OFFSET				(81)
#define KDS_GYRO_TAXES_BASE_OFFSET				(142)


/* KDS entry for ACC hardware information */
#define KDS_ACC_MAJ_VER_OFFSET					(8)
#define KDS_ACC_MIN_VER_OFFSET					(9)
#define KDS_ACC_ANG_RX_OFFSET					(11)
#define KDS_ACC_ANG_RY_OFFSET					(12)
#define KDS_ACC_ANG_RZ_OFFSET					(13)
#define KDS_ACC_ANG_SX_OFFSET					(14)
#define KDS_ACC_ANG_SY_OFFSET					(15)
#define KDS_ACC_ANG_SZ_OFFSET					(16)
#define KDS_ACC_ANG_TX_OFFSET					(17)
#define KDS_ACC_ANG_TY_OFFSET					(18)
#define KDS_ACC_ANG_TZ_OFFSET					(19)
#define KDS_ACC_RAXES_BASE_OFFSET				(20)
#define KDS_ACC_SAXES_BASE_OFFSET				(81)
#define KDS_ACC_TAXES_BASE_OFFSET				(142)

/*KDS relative entry for sensor hardware information */
#define KDS_SENSOR_ADC_RANGE_MIN_OFFSET					(0)
#define KDS_SENSOR_ADC_RANGE_MAX_OFFSET					(4)
#define KDS_SENSOR_SAMPLE_MIN_OFFSET					(8)
#define KDS_SENSOR_SAMPLE_MAX_OFFSET					(12)
#define KDS_SENSOR_MIN_NOISE_VAL_M_OFFSET				(16)
#define KDS_SENSOR_MIN_NOISE_VAL_E_OFFSET				(18)
#define KDS_SENSOR_ESTIM_OFFSET_M_OFFSET				(19)
#define KDS_SENSOR_ESTIM_OFFSET_E_OFFSET				(21)
#define KDS_SENSOR_MIN_OFFSET_M_OFFSET					(22)
#define KDS_SENSOR_MIN_OFFSET_E_OFFSET					(24)
#define KDS_SENSOR_MAX_OFFSET_M_OFFSET					(25)
#define KDS_SENSOR_MAX_OFFSET_E_OFFSET					(27)
#define KDS_SENSOR_DRIFT_OFFSET_M_OFFSET				(28)
#define KDS_SENSOR_DRIFT_OFFSET_E_OFFSET				(30)
#define KDS_SENSOR_MAX_UNSTEAD_OFFSET_M_OFFSET 			(31)
#define KDS_SENSOR_MAX_UNSTEAD_OFFSET_E_OFFSET			(33)
#define KDS_SENSOR_BEST_CALIB_OFFSET_M_OFFSET			(34)
#define KDS_SENSOR_BEST_CALIB_OFFSET_E_OFFSET			(36)
#define KDS_SENSOR_ESTIM_SCALE_FACTOR_M_OFFSET			(37)
#define KDS_SENSOR_ESTIM_SCALE_FACTOR_E_OFFSET			(39)
#define KDS_SENSOR_MIN_SCALE_FACTOR_M_OFFSET			(40)
#define KDS_SENSOR_MIN_SCALE_FACTOR_E_OFFSET			(42)
#define KDS_SENSOR_MAX_SCALE_FACTOR_M_OFFSET			(43)
#define KDS_SENSOR_MAX_SCALE_FACTOR_E_OFFSET			(45)
#define KDS_SENSOR_DRIFT_SCALE_FACTOR_M_OFFSET			(46)
#define KDS_SENSOR_DRIFT_SCALE_FACTOR_E_OFFSET			(48)
#define KDS_SENSOR_MAX_UNSTEAD_SCALE_FACTOR_M_OFFSET	(49)
#define KDS_SENSOR_MAX_UNSTEAD_SCALE_FACTOR_E_OFFSET	(51)
#define KDS_SENSOR_BEST_CALIB_SCALE_FACTOR_M_OFFSET		(52)
#define KDS_SENSOR_BEST_CALIB_SCALE_FACTOR_E_OFFSET		(54)
#define KDS_SENSOR_DRIFT_OFFSET_TIME_M_OFFSET			(55)
#define KDS_SENSOR_DRIFT_OFFSET_TIME_E_OFFSET			(57)
#define KDS_SENSOR_DRIFT_SCALE_FACTOR_TIME_M_OFFSET		(58)
#define KDS_SENSOR_DRIFT_SCALE_FACTOR_TIMR_E_OFFSET		(60)

#else
#warning kds_hwinfo.h included multiple times
#endif
