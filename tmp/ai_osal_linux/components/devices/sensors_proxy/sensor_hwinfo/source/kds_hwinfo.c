/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    [short description]
 *
 * [long description]
 * 
 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History: 
 *
 ******************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "hwinfo.h"
#include "kds_hwinfo.h"
#include "kdsfloat.h"
#include "sensor_dispatcher.h"

/******************************************************************************
*
* Static functions
******************************************************************************/
static tBool SenHwInfo_bReadDIDfromKDS( tU16 u16DIDEntry, tU16 u16DIDLenght, tPU8 pu8DID );
static tBool SenHwInfo_bParseHwinfoFromKDSData( tEnSensor enType, tPU8 pu8KDSData,  OSAL_trIOCtrlHwInfo *prIoCtrlHwInfo);
static tBool SenHwInfo_bParseAxesInfoFromKDSData( tU8 u8BaseAdr, tPU8 pu8KDSData, OSAL_trSensorHwInfo *prAxesHwInfo);



/**********************************************************************************
* FUNCTION    : SenHwInfo_bGetKdsHwinfo
*
* PARAMETER   : enType - Type of Sensor
*               prIoCtrlHwInfo - Pointer where to put hwinfo
*
* RETURNVALUE : True on success
*               False in case of failure
*
* DESCRIPTION : Read hardware information from KDS and update the input structure
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 10.Dec.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
***********************************************************************************/
tBool SenHwInfo_bGetKdsHwinfo(tEnSensor enType, OSAL_trIOCtrlHwInfo *prIoCtrlHwInfo)
{
   tBool bRet = TRUE;
   tU16 u16KDSEntry = 0, u16KDSLength = 0;

   /* Get the KDS entry and lenght based on sensor type */
   if (enType == GYRO_ID)
   {
      u16KDSEntry = KDS_GYRO_DID_ENTRY;
      u16KDSLength = KDS_GYRO_DID_ENTRY_LEN;
   }
   else if (enType == ACC_ID)
   {
      u16KDSEntry = KDS_ACC_DID_ENTRY;
      u16KDSLength = KDS_ACC_DID_ENTRY_LEN;
   }
   else
   {
      SenDisp_vTraceOut(TR_LEVEL_FATAL,"SEN_DISP : SenHwInfo_bGetKdsHwinfo : Invalid sensor type");
      bRet = FALSE;
   }
   
   if (bRet != FALSE)
   {
      tU8 au8KDSData[u16KDSLength];
   
      /* Read data from KDS */
      if ( SenHwInfo_bReadDIDfromKDS(u16KDSEntry, u16KDSLength, au8KDSData) == TRUE )
      {
         /* Parse the data read from KDS and fill the input hwinfo structure */
         if (SenHwInfo_bParseHwinfoFromKDSData(enType, au8KDSData, prIoCtrlHwInfo) == FALSE)
         {
            SenDisp_vTraceOut(TR_LEVEL_FATAL,"SEN_DISP : SenHwInfo_bGetKdsHwinfo : Parse kds data failed");
            bRet = FALSE;
         }
      }
      else
      {
         SenDisp_vTraceOut(TR_LEVEL_FATAL,"SEN_DISP : SenHwInfo_bGetKdsHwinfo : Read data from KDS failed");
         bRet = FALSE;
      }
   }
   
   return (bRet);
}


/************************************************************************************
* FUNCTION    : SenHwInfo_bReadDIDfromKDS
*
* PARAMETER   : u16DIDEntry - DID Entry
*               u16DIDLenght - DID Length
*               pu8DID - Pointer to data
*
* RETURNVALUE : True on success
*               False in case of failure
*
* DESCRIPTION : Read data from KDS at specified  KDS Entry and length
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 10.Dec.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
*************************************************************************************/

static tBool SenHwInfo_bReadDIDfromKDS( tU16 u16DIDEntry, tU16 u16DIDLenght, tPU8 pu8DID )
{
   tBool bRet = TRUE;
   OSAL_tIODescriptor KdsFd;
   tsKDSEntry sKDSEntry;
   tS32 s32RetVal = 0;

   /* Open KDS device */
   KdsFd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY );

   if( KdsFd != OSAL_ERROR )
   {
      OSAL_pvMemorySet( &sKDSEntry, 0, sizeof( sKDSEntry ) );

      /* Read from KDS at Entry location */
      sKDSEntry.u16Entry       =  u16DIDEntry;
      sKDSEntry.u16EntryLength =  u16DIDLenght;
      s32RetVal = OSAL_s32IORead( KdsFd,
                                 ( tPS8 )&sKDSEntry,
                                 ( tU32 )sizeof( sKDSEntry ) );

      if ( s32RetVal == (tS32)sizeof(sKDSEntry) )
      {
         /* Read successful, Copy to input buffer*/
         OSAL_pvMemoryCopy( pu8DID, sKDSEntry.au8EntryData, sKDSEntry.u16EntryLength );
      }
      else
      {
         /* Read failed */
         SenDisp_vTraceOut(TR_LEVEL_FATAL,"SEN_DISP : SenHwInfo_bReadDIDfromKDS : KDS Read failed, Fail line - %d", __LINE__);
         bRet = FALSE;
      }

      s32RetVal = OSAL_s32IOClose( KdsFd );
      if( s32RetVal != OSAL_OK )
      {
         SenDisp_vTraceOut(TR_LEVEL_FATAL,"SEN_DISP : SenHwInfo_bReadDIDfromKDS : KDS Close failed, Fail line - %d", __LINE__);
         bRet = FALSE;
      }
   }
   else
   {
      /* Open KDS Failed*/
      SenDisp_vTraceOut(TR_LEVEL_FATAL,"SEN_DISP : SenHwInfo_bReadDIDfromKDS : KDS Open failed, Fail line %d", __LINE__);
      bRet = FALSE;
   }

   return (bRet);
}


/************************************************************************************
* FUNCTION    : SenHwInfo_bParseHwinfoFromKDSData
*
* PARAMETER   : enType - Sensor type
*               pu8DID - Pointer to KDS data
*               prIoCtrlHwInfo - Pointer to structure of type OSAL_trIOCtrlHwInfo
*
* RETURNVALUE : True on success
*               False in case of failure
*
* DESCRIPTION : Parse Hwinfo from KDS data and fill hardware information structure(OSAL_trIOCtrlHwInfo)
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 10.Dec.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
*************************************************************************************/
static tBool SenHwInfo_bParseHwinfoFromKDSData( tEnSensor enType, tPU8 pu8KDSData,  OSAL_trIOCtrlHwInfo *prIoCtrlHwInfo)
{
   tBool bRet = TRUE;
   tU8 u8RAxesBase = 0, u8SAxesBase = 0, u8TAxesbase = 0;
   tU8 u8MajVer = 0;
   tU16 u16MinVer = 0;
   tChar acMajVer[2] = {' ','\0'}  ;
   tChar acMinVer[3] = {' ',' ', '\0'};
   tChar acTag[9] = {' ',' ',' ',' ',' ',' ',' ',' ','\0'};
   
   /* Input validation */
   if (pu8KDSData == NULL)
   {
      SenDisp_vTraceOut(TR_LEVEL_FATAL, "SenHwInfo_bParseHwinfoFromKDSData : Invalid input, Parse data is NULL");
      bRet = FALSE;
   }
   else
   {
      OSAL_pvMemoryCopy( acTag, pu8KDSData, 8 );
      /* Get Major and Minor number */
      if (enType == GYRO_ID)
      {
         u8MajVer = pu8KDSData[KDS_GYRO_MAJ_VER_OFFSET];
         OSAL_pvMemoryCopy( &u16MinVer, pu8KDSData+KDS_GYRO_MIN_VER_OFFSET, sizeof(tU16) );

	 acMajVer[0] = (tChar) pu8KDSData[KDS_GYRO_MAJ_VER_OFFSET];
	 acMinVer[0] = (tChar) pu8KDSData[KDS_GYRO_MIN_VER_OFFSET];
	 acMinVer[1] = (tChar) pu8KDSData[KDS_GYRO_MIN_VER_OFFSET+1];
      }
      else if (enType == ACC_ID)
      {
         u8MajVer = pu8KDSData[KDS_ACC_MAJ_VER_OFFSET];
         OSAL_pvMemoryCopy( &u16MinVer, pu8KDSData+KDS_ACC_MIN_VER_OFFSET, sizeof(tU16) );	

	 acMajVer[0] = (tChar) pu8KDSData[KDS_ACC_MAJ_VER_OFFSET];
	 acMinVer[0] = (tChar)pu8KDSData[KDS_ACC_MIN_VER_OFFSET];
	 acMinVer[1] = (tChar)pu8KDSData[KDS_ACC_MIN_VER_OFFSET+1];

      }
      SenDisp_vTraceOut( TR_LEVEL_USER_1,
			 "KDS HwInfo for sensor:%d tag:%s major:%s minor:%s",
			 enType,
			 acTag,
			 acMajVer,
			 acMinVer);
      
      /* Check if the Hwinfo in the KDS is valid, if major number is 0 or  0x20 and minor number is 0 or 0x2020 then it is invalid data in KDS */
      if ((u8MajVer != 0 && u8MajVer != 0x20) && (u16MinVer != 0 && u16MinVer != 0x2020))
      {
         /* Mounting angle */
         if (enType == GYRO_ID)
         {
            prIoCtrlHwInfo->rMountAngles.u8AngRX = pu8KDSData[KDS_GYRO_ANG_RX_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngRY = pu8KDSData[KDS_GYRO_ANG_RY_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngRZ = pu8KDSData[KDS_GYRO_ANG_RZ_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngSX = pu8KDSData[KDS_GYRO_ANG_SX_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngSY = pu8KDSData[KDS_GYRO_ANG_SY_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngSZ = pu8KDSData[KDS_GYRO_ANG_SZ_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngTX = pu8KDSData[KDS_GYRO_ANG_TX_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngTY = pu8KDSData[KDS_GYRO_ANG_TY_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngTZ = pu8KDSData[KDS_GYRO_ANG_TZ_OFFSET];
            u8RAxesBase = KDS_GYRO_RAXES_BASE_OFFSET;
            u8SAxesBase = KDS_GYRO_SAXES_BASE_OFFSET;
            u8TAxesbase = KDS_GYRO_TAXES_BASE_OFFSET;
         }
         else if (enType == ACC_ID)
         {
            prIoCtrlHwInfo->rMountAngles.u8AngRX = pu8KDSData[KDS_ACC_ANG_RX_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngRY = pu8KDSData[KDS_ACC_ANG_RY_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngRZ = pu8KDSData[KDS_ACC_ANG_RZ_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngSX = pu8KDSData[KDS_ACC_ANG_SX_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngSY = pu8KDSData[KDS_ACC_ANG_SY_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngSZ = pu8KDSData[KDS_ACC_ANG_SZ_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngTX = pu8KDSData[KDS_ACC_ANG_TX_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngTY = pu8KDSData[KDS_ACC_ANG_TY_OFFSET];
            prIoCtrlHwInfo->rMountAngles.u8AngTZ = pu8KDSData[KDS_ACC_ANG_TZ_OFFSET];
            u8RAxesBase = KDS_ACC_RAXES_BASE_OFFSET;
            u8SAxesBase = KDS_ACC_SAXES_BASE_OFFSET;
            u8TAxesbase = KDS_ACC_TAXES_BASE_OFFSET;
         }
         else
         {
            SenDisp_vTraceOut(TR_LEVEL_FATAL, "SenHwInfo_bParseHwinfoFromKDSData : Invalid sensor type");
            bRet = FALSE;
         }
   	  
      
         /* RAxes */
         if (bRet != FALSE)
         {
            if (SenHwInfo_bParseAxesInfoFromKDSData(u8RAxesBase, pu8KDSData, &(prIoCtrlHwInfo->rRAxes)) == FALSE)
            {
               SenDisp_vTraceOut(TR_LEVEL_FATAL, "SenHwInfo_bParseHwinfoFromKDSData : Unabel to update RAxes Hwinfo from KDS data");
               bRet = FALSE;
            }
         }
   
         /* SAxes */
         if (bRet != FALSE)
         {
            if (SenHwInfo_bParseAxesInfoFromKDSData(u8SAxesBase, pu8KDSData, &(prIoCtrlHwInfo->rSAxes)) == FALSE)
            {
               SenDisp_vTraceOut(TR_LEVEL_FATAL, "SenHwInfo_bParseHwinfoFromKDSData : Unabel to update SAxes Hwinfo from KDS data");
               bRet = FALSE;
            }
         }
             
         /* TAxes */
         if (bRet != FALSE)
         {
            if (SenHwInfo_bParseAxesInfoFromKDSData(u8TAxesbase, pu8KDSData, &(prIoCtrlHwInfo->rTAxes)) == FALSE)
            {
               SenDisp_vTraceOut(TR_LEVEL_FATAL, "SenHwInfo_bParseHwinfoFromKDSData : Unabel to update TAxes Hwinfo from KDS data");
               bRet = FALSE;
            }
         }
      }
      else
      {
         SenDisp_vTraceOut(TR_LEVEL_FATAL, "SenHwInfo_bParseHwinfoFromKDSData : Invalid Major and Minor number, Invalid KDS data");
         bRet = FALSE;
      }
   }

   return (bRet);
}


/**************************************************************************//**
 * @brief get a 16bit value from a KDS buffer
 ******************************************************************************/
tU16 u16getFromKds(tPU8 pu8src)
/**************************************************************************//**
 * get the endianess right for a 16bit value
 ******************************************************************************/
{
   tU16 u16Ret = 0;

   if (NULL != pu8src)
   {
      u16Ret =
	 ((tU16) pu8src[0] << 8)
	 +
	 ((tU16) pu8src[1]);
   }
   return (u16Ret);
}

/**************************************************************************//**
 * @brief get a tU32 value from a KDS buffer
 ******************************************************************************/
tU32 u32getFromKds(tPU8 pu8src)
/**************************************************************************//**
 * get the endianess right for a 32bit value
 ******************************************************************************/
{
   tU32 u32Ret = 0;

   if (NULL != pu8src)
   {
      u32Ret =
	 ((tU32) pu8src[0] << 24)
	 +
	 ((tU32) pu8src[1] << 16)
	 +
	 ((tU32) pu8src[2] << 8)
	 +
	 (tU32) pu8src[3];
   }

   return (u32Ret);
}

/**************************************************************************//**
 * @brief get a tF32 value which is codes as a KdsFloat from KDS buffer
 ******************************************************************************/
tF32 f32getFromKds(tPU8 pu8src)
/**************************************************************************//**
 * alignement of the kdsfloat value in the KDS buffer is
 *   +------------+-----------+---------+    
 *   | mantiassa1 | mantissa2 | exponent|
 *   +------------+-----------+---------+
 * where the mantissa is a 16bit value
 ******************************************************************************/
{
   tF32 f32out=0.0f;
   trKdsFloat rKdsFloat = {0,0};

   if (NULL != pu8src )
   {      
      rKdsFloat.s16Mant = u16getFromKds(pu8src);
      rKdsFloat.s8Exp = (tS8) pu8src[2];
      f32out = f32KdsFloatToF32(rKdsFloat);
   }
   return (f32out);
}

/************************************************************************************
* FUNCTION    : SenHwInfo_bParseAxesInfoFromKDSData
*
* PARAMETER   : u8BaseAdr - Base index for Axes information
*               pu8KDSData - Starting address to KDS data
*               prAxesHwInfo - Pointer to Axes Hwinfo structure
*
* RETURNVALUE : True on success
*               False in case of failure
*
* DESCRIPTION : Read Axes information from KDS data based on base index and update input Axes Hwinfo
*                         It is not possible to keep the floating point data in KDS, 
*                         hence the floating point data can be converted in to exponent and mantissa,
*                         where as these two will be stored as 2 integers.
*                         In this function after reading these two integers data, it has to convert back to floating point data
*                         and then update the respective structure field with this data.
*
* HISTORY     :
*---------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|--------------------------------------------
* 10.Dec.2014 | Initial version: 1.0 | Arun Magi(RBEI/ECF5)
* --------------------------------------------------------------------------------
***************************************************************************************/
static tBool SenHwInfo_bParseAxesInfoFromKDSData(tU8 u8BaseAdr, tPU8 pu8KDSData, OSAL_trSensorHwInfo *prAxesHwInfo)
{
   tBool bRet = TRUE;

   if (pu8KDSData == NULL)
   {
      SenDisp_vTraceOut(TR_LEVEL_FATAL, "SenHwInfo_bParseAxesInfoFromKDSData : Invalid input, parse data is NULL");
      bRet = FALSE;
   }
   else
   {
      tPU8 pu8AxesBaseAdr = pu8KDSData+u8BaseAdr;

      prAxesHwInfo->u32AdcRangeMin	     = u32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_ADC_RANGE_MIN_OFFSET);
      prAxesHwInfo->u32AdcRangeMax	     = u32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_ADC_RANGE_MAX_OFFSET);
      prAxesHwInfo->u32SampleMin	     = u32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_SAMPLE_MIN_OFFSET);
      prAxesHwInfo->u32SampleMax	     = u32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_SAMPLE_MAX_OFFSET); 
      prAxesHwInfo->f32MinNoiseValue         = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_MIN_NOISE_VAL_M_OFFSET);
      prAxesHwInfo->f32EstimOffset           = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_ESTIM_OFFSET_M_OFFSET);
      prAxesHwInfo->f32MinOffset             = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_MIN_OFFSET_M_OFFSET);
      prAxesHwInfo->f32MaxOffset             = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_MAX_OFFSET_M_OFFSET);
      prAxesHwInfo->f32DriftOffset           = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_DRIFT_OFFSET_M_OFFSET);
      prAxesHwInfo->f32MaxUnsteadOffset      = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_MAX_UNSTEAD_OFFSET_M_OFFSET);
      prAxesHwInfo->f32BestCalibOffset       = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_BEST_CALIB_OFFSET_M_OFFSET);
      prAxesHwInfo->f32EstimScaleFactor      = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_ESTIM_SCALE_FACTOR_M_OFFSET);
      prAxesHwInfo->f32MinScaleFactor        = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_MIN_SCALE_FACTOR_M_OFFSET);
      prAxesHwInfo->f32MaxScaleFactor        = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_MAX_SCALE_FACTOR_M_OFFSET);
      prAxesHwInfo->f32DriftScaleFactor      = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_DRIFT_SCALE_FACTOR_M_OFFSET);
      prAxesHwInfo->f32MaxUnsteadScaleFactor = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_MAX_UNSTEAD_SCALE_FACTOR_M_OFFSET);
      prAxesHwInfo->f32BestCalibScaleFactor  = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_BEST_CALIB_SCALE_FACTOR_M_OFFSET);
      prAxesHwInfo->f32DriftOffsetTime       = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_DRIFT_OFFSET_TIME_M_OFFSET);
      prAxesHwInfo->f32DriftScaleFactorTime  = f32getFromKds(pu8AxesBaseAdr+KDS_SENSOR_DRIFT_SCALE_FACTOR_TIME_M_OFFSET);
      
   }
   
   return (bRet);

}


