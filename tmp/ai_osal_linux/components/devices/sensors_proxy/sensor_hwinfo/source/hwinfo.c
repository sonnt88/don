/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    get and provide the HW information from gyro and acc
 *
 * depending on the customer project the HW information and mounting position
 * can be stored in different locations. GM uses for the mounting position the
 * EOL. Suzuki, JAC and all other projects currently use a static configuration
 * but they are prepared to use the KDS.

 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History: 
 *
 ******************************************************************************/
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"

#include "hwinfo.h"
#include "kds_hwinfo.h"
#include "eol_mounting.h"
#include "sensor_dispatcher.h"


static OSAL_trSensorHwInfo rSmi130DefaultGyroHwInfo =
{
/* (tU32)20,           SampleRate is not part of the OSAL structure */
   (tU32)0,        /*  AdcRangeMin          */
   (tU32)65535,    /*  AdcRangeMax          */
   (tU32)0,        /*  SampleMin            */
   (tU32)65535,    /*  SampleMax            */
   (tF32)0.0,      /*  MinNoiseValue        */
   (tF32)32768.0,  /*  EstimOffset          */
   (tF32)30144.0,  /*  MinOffset            */
   (tF32)35392.00, /*  MaxOffset            */
   (tF32)3.94,     /*  DriftOffset          */
   (tF32)0.0,      /*  MaxUnsteadOffset     */
   (tF32)0.0,      /*  BestCalibOffset      */
   (tF32)262.4,    /*  EstimScaleFactor     */
   (tF32)236.16,   /*  MinScaleFactor       */
   (tF32)288.64,   /*  MaxScaleFactor       */
   (tF32)0.08,     /*  DriftScaleFactor     */
   (tF32)0.0,      /*  MaxUnsteadScaleFacto */
   (tF32)0.0,      /*  BestCalibScaleFactor */
   (tF32)0.0,      /*  DriftOffsetTime      */
   (tF32)0.0       /*  DriftScaleFactorTime */
};

static OSAL_trSensorHwInfo rSmi130DefaultAccHwInfo=
//acc
{
/* (tU32)20,         SampleRate is not part of the OSAL structure */
   (tU32) 0,      /* AdcRangeMin           */
   (tU32)4095,    /* AdcRangeMax           */
   (tU32)0,       /* SampleMin             */
   (tU32)4095,    /* SampleMax             */
   (tF32)0.0,     /* MinNoiseValue         */
   (tF32)2048.0,  /* EstimOffset           */
   (tF32)1832.96, /* MinOffset             */
   (tF32)2263.04, /* MaxOffset             */
   (tF32)1.02,    /* DriftOffset           */
   (tF32)0.0,     /* MaxUnsteadOffset      */
   (tF32)0.0,     /* BestCalibOffset       */
   (tF32)104.42,  /* EstimScaleFactor      */
   (tF32)101.29,  /* MinScaleFactor        */
   (tF32)107.55,  /* MaxScaleFactor        */
   (tF32)0.021,   /* DriftScaleFactor      */
   (tF32)0.0,     /* MaxUnsteadScaleFactor */
   (tF32)0.0,     /* BestCalibScaleFactor  */
   (tF32)0.0,     /* DriftOffsetTime       */
   (tF32)0.0      /* DriftScaleFactorTime  */
};

/* static mounting */
/** @todo switches should be removed when KDS is in place. Then it
 * should be the only source for mounting (except GM) */
#if defined VARIANT_S_FTR_SUZUKI_GYRO_MOUNTING
OSAL_tr3dMountAngles rDefaultMountAngles =
{
   (tU8) 90, (tU8)  0, (tU8)  90,
   (tU8) 25, (tU8) 90, (tU8) 115,
   (tU8)115, (tU8) 90, (tU8) 155
};
#elif defined VARIANT_S_FTR_JAC_GYRO_MOUNTING
OSAL_tr3dMountAngles rDefaultMountAngles =
{
   (tU8)172, (tU8) 90, (tU8)82,
   (tU8) 90, (tU8)180, (tU8)90,
   (tU8) 82, (tU8) 90, (tU8) 8
};
#else
OSAL_tr3dMountAngles rDefaultMountAngles =
{
  (tU8) 0, (tU8)90, (tU8)90,
  (tU8)90, (tU8) 0, (tU8) 90,
  (tU8)90, (tU8)90, (tU8)  0
};
#endif

tBool SenHwInfo_bGetHwinfo ( tEnSensor enType , OSAL_trIOCtrlHwInfo *prIoCtrlHwInfo)
{
   tBool bReturn = TRUE;

   /* try reading out the KDS for HW info, if failed then use static value*/
   if ( FALSE == SenHwInfo_bGetKdsHwinfo ( enType, prIoCtrlHwInfo) )
   {
      SenDisp_vTraceOut(TR_LEVEL_FATAL,"SEN_DISP : SenHwInfo_bGetHwinfo : Get hardware information from kds failed, Use static value");
      // KDS reading not successfull -> use static configuration
      prIoCtrlHwInfo->rMountAngles = rDefaultMountAngles;

      switch (enType)
      {
      case GYRO_ID:	   
	 prIoCtrlHwInfo->rRAxes = rSmi130DefaultGyroHwInfo;
	 prIoCtrlHwInfo->rSAxes = rSmi130DefaultGyroHwInfo;
	 prIoCtrlHwInfo->rTAxes = rSmi130DefaultGyroHwInfo;
	 break;
      case ACC_ID:
	 prIoCtrlHwInfo->rRAxes = rSmi130DefaultAccHwInfo;
	 prIoCtrlHwInfo->rSAxes = rSmi130DefaultAccHwInfo;
	 prIoCtrlHwInfo->rTAxes = rSmi130DefaultAccHwInfo;
	 break;
      default:	 
	 /* unknown sensor. Do not initialize */
	 bReturn = FALSE;
      }
   };
   /* only for GM use the mounting angles provided by the EOL */
#ifdef  VARIANT_S_FTR_EOL_GYRO_MOUNTING
   bGetMountingfromEOL( &(prIoCtrlHwInfo->rMountAngles) );
#endif

   SenDisp_vTraceOut(
      TR_LEVEL_USER_1,
      "RX:%2u RY:%2u RZ:%2u SX:%2u SY:%2u SZ:%2u TX:%2u TY:%2u TZ:%2u\n",
      prIoCtrlHwInfo->rMountAngles.u8AngRX,
      prIoCtrlHwInfo->rMountAngles.u8AngRY,
      prIoCtrlHwInfo->rMountAngles.u8AngRZ,
      prIoCtrlHwInfo->rMountAngles.u8AngSX,
      prIoCtrlHwInfo->rMountAngles.u8AngSY,
      prIoCtrlHwInfo->rMountAngles.u8AngSZ,
      prIoCtrlHwInfo->rMountAngles.u8AngTX,
      prIoCtrlHwInfo->rMountAngles.u8AngTY,
      prIoCtrlHwInfo->rMountAngles.u8AngTZ );
   
   return (bReturn);
}
