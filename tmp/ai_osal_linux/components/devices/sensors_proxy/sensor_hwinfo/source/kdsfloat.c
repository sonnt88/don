/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    represent float or double values with two signed integer values
 *
 * provides functions to convert from float/double to the integer
 * representation and vice versa. The integer representation uses a
 * tS16 value for the mantissa and a tS8 for the exponent. This
 * representation is called kdsfloat since it is used for storing
 * floar values in the KDS. This is used for the HWInfo of gyro and acc.
 * 
 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History: 
 *
 ******************************************************************************/
#include <math.h>
#include <stdlib.h>

#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "kdsfloat.h"

#define KDS_MANT_BITS 15

/**************************************************************************//**
 * @brief convert kdsfloat value to a tF32 value
 ******************************************************************************/
tF32 
f32KdsFloatToF32(
   trKdsFloat rKdsFloatIn
)
/**************************************************************************//**
 * [long description] 
 ******************************************************************************/
{
   tS32 iExp = 0;
   tS32 iMant = 0;
   double dValue = 0.0;   
   double dSign = 1.0;
   tF32 f32Out = 0.0;

   if ( rKdsFloatIn.s16Mant != (tS16) abs (rKdsFloatIn.s16Mant) )
   {
      dSign = -1;
      iMant = abs (rKdsFloatIn.s16Mant);
   }
   else
   {
      iMant = rKdsFloatIn.s16Mant;
   }

   dValue = (float) iMant;
   iExp = (tS32) rKdsFloatIn.s8Exp - (tS32) KDS_MANT_BITS;
   dValue = ldexp (dValue, iExp);
   dValue = copysign (dValue, dSign);
   f32Out = (tF32) dValue;
   
   return (f32Out);
}

/**************************************************************************//**
 * @brief converts a f32 to a kdsfloat
 ******************************************************************************/
trKdsFloat 
rF32ToKdsFloat (
   tF32 f32In /**< [in] float */
)
/**************************************************************************//**
 * [long description]
 ******************************************************************************/
{
   tS32 iExp = 0;
   double fMant = 0.0;
   tS32 iMant = 0;
   trKdsFloat rKdsFloatOut = {0,(tS8) 0};

   /* check for NANs etc. */
   /* int iSignMant; */
   /* int iSignExp ; */

   fMant = frexp(fabs((double) f32In), &iExp);
   rKdsFloatOut.s8Exp = (tS8) iExp;
   fMant = ldexp(fMant, KDS_MANT_BITS);
   iMant = (tS32) trunc(fMant);

   rKdsFloatOut.s16Mant=(tS16) iMant;
   if ( 0 != (int) signbit(f32In) ) //lint !e747 PQM_authorized_multi_481
   {
      rKdsFloatOut.s16Mant *= -1 ;
   }

   return (rKdsFloatOut);
}

/**************************************************************************//**
 * @brief convert a kdsfloat value to double
 ******************************************************************************/
double 
dKdsFloatToDouble(
   trKdsFloat rKdsFloatIn /**< [in]  */
)
/**************************************************************************//**
 * [long description]
 ******************************************************************************/

{
   tS32 iExp = 0;
   tS32 iMant = 0;
   double dValue = 0.0;   
   double dSign = 1.0;
   double dOut = 0.0;

   if ( rKdsFloatIn.s16Mant != (tS16) abs (rKdsFloatIn.s16Mant) )
   {
      dSign = -1;
      iMant = abs (rKdsFloatIn.s16Mant);
   }
   else
   {
      iMant = rKdsFloatIn.s16Mant;
   }

   dValue = (float) iMant;
   iExp = (tS32) rKdsFloatIn.s8Exp - (tS32) KDS_MANT_BITS;
   dValue = ldexp (dValue, iExp);
   dValue = copysign (dValue, dSign);
   dOut = dValue;
   
   return (dOut);
}

/**************************************************************************//**
 * @brief converts a double value to a kdsfloat value
 ******************************************************************************/
trKdsFloat 
rDoubleToKdsFloat(
   double dIn  /**< [in] double value */
)
/**************************************************************************//**
 * [long description]
 ******************************************************************************/

{
   tS32 iExp = 0;
   double fMant = 0.0;
   tS32 iMant = 0;
   trKdsFloat rKdsFloatOut = {0,(tS8) 0};

   /* check for NANs etc. */
   /* int iSignMant; */
   /* int iSignExp ; */

   fMant = frexp( fabs(dIn), &iExp );
   rKdsFloatOut.s8Exp = (tS8) iExp;
   fMant = ldexp(fMant, KDS_MANT_BITS);
   iMant = (tS32) trunc(fMant);
   
   rKdsFloatOut.s16Mant=(tS16) iMant;
   if ( 0 != (int) (signbit( dIn)) ) //lint !e747 PQM_authorized_multi_481
   {
      rKdsFloatOut.s16Mant *= -1 ;
   }

   return (rKdsFloatOut);
}

