/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    routines for converting GM Euler angles to CM mounting position.
 *
 * GM is describing the mounting position in terms of euler angles. CM is using
 * the angles between the gyro/acc RST coordinate system and the car XYZ
 * coordinate system. The function bCalcRSTXYZangles() will calculate from the
 * euler angles the CM angles.
 * 
 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History: 
 *
 ******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "gm_euler_rotation.h"

typedef tF32 taf32Vec3d[3];

/**************************************************************************//**
 * @brief 3x3 matrix multiplication
 ******************************************************************************/
void 
vMatMultMat3d (
   const taf32Mat3d af32Ma,  /**< [in] matrix a  */
   const taf32Mat3d af32Mb,  /**< [in] matrix b  */
   taf32Mat3d af32Mc         /**< [out] matrix c */   
)
/**************************************************************************//**
 * Function to multiply matrix a with matrix b and place the result in matrix
 * c. All matices are of type taf32Mat3d and have a size of 3x3.
 * | a[0] a[1] a[2] |   | b[0] b[1] b[2] |   | c[0] c[1] c[2] |
 * | a[3] a[4] a[5] | * | b[3] b[4] b[5] | = | c[3] c[4] c[5] |
 * | a[6] a[7] a[8] |   | b[6] b[7] b[8] |   | c[6] c[7] c[8] |
 ******************************************************************************/
{
   if (( NULL != af32Ma ) && ( NULL != af32Mb ) && ( NULL != af32Mc ) )
   {   
      af32Mc[0] =
	 af32Ma[0] * af32Mb[0] + af32Ma[1] * af32Mb[3] + af32Ma[2] * af32Mb[6];

      af32Mc[1] =
	 af32Ma[0] * af32Mb[1] + af32Ma[1] * af32Mb[4] + af32Ma[2] * af32Mb[7];

      af32Mc[2] =
	 af32Ma[0] * af32Mb[2] + af32Ma[1] * af32Mb[5] + af32Ma[2] * af32Mb[8];

      af32Mc[3] =
	 af32Ma[3] * af32Mb[0] + af32Ma[4] * af32Mb[3] + af32Ma[5] * af32Mb[6];

      af32Mc[4] =
	 af32Ma[3] * af32Mb[1] + af32Ma[4] * af32Mb[4] + af32Ma[5] * af32Mb[7];

      af32Mc[5] =
	 af32Ma[3] * af32Mb[2] + af32Ma[4] * af32Mb[5] + af32Ma[5] * af32Mb[8];

      af32Mc[6] =
	 af32Ma[6] * af32Mb[0] + af32Ma[7] * af32Mb[3] + af32Ma[8] * af32Mb[6];

      af32Mc[7] =
	 af32Ma[6] * af32Mb[1] + af32Ma[7] * af32Mb[4] + af32Ma[8] * af32Mb[7];

      af32Mc[8] =
	 af32Ma[6] * af32Mb[2] + af32Ma[7] * af32Mb[5] + af32Ma[8] * af32Mb[8];
   }
   return;
}

/**************************************************************************//**
 * @brief multiply a 3x3 matrix with a vector of dimension 3
 ******************************************************************************/
void 
vMatMultVec3d (
   const taf32Mat3d af32Ma,   /**< [in]  matrix a  */
   const taf32Vec3d af32Vb,   /**< [in]  vector b  */
   taf32Vec3d af32Vc          /**< [out] vector c */ 
)
/**************************************************************************//**
 * multiply matrix a with vector b and place the resulting vector in c
 * | a[0] a[1] a[2] |   | b[0] |   | c[0] |
 * | a[3] a[4] a[5] | * | b[1] | = | c[1] |
 * | a[6] a[7] a[8] |   | b[2] |   | c[2] |
 ******************************************************************************/
{
   af32Vc[0] =
      af32Ma[0] * af32Vb[0] + af32Ma[1] * af32Vb[1] + af32Ma[2] * af32Vb[2];

   af32Vc[1] =
      af32Ma[3] * af32Vb[0] + af32Ma[4] * af32Vb[1] + af32Ma[5] * af32Vb[2];

   af32Vc[2] =
      af32Ma[6] * af32Vb[0] + af32Ma[7] * af32Vb[1] + af32Ma[8] * af32Vb[2];

   return;
}


/**************************************************************************//**
 * @brief calculate the innerproduct of the vectors
 ******************************************************************************/
tF32                        
f32InnerProd3d (
   const taf32Vec3d af32Va, /**< [in] vector a*/
   const taf32Vec3d af32Vb  /**< [in] vector b*/
)
/**************************************************************************//**
 * inner product (projection) of vector a and vector b. Both vectors are of
 * dimension 3.
 * | a[0] |   | b[0] |
 * | a[1] | * | b[1] | = c
 * | a[2] |   | b[2] | 
 ******************************************************************************/
{
   return (af32Va[0] * af32Vb[0] + af32Va[1] * af32Vb[1] + af32Va[2] * af32Vb[2] );
}


/**************************************************************************//**
 * @brief convert angles in grad to radians
 ******************************************************************************/
tF32 
f32angGrad2Rad(
   tU16 u16Grad /**< input angle in grad */
)
/**************************************************************************//**
 * 
 ******************************************************************************/
{
   return ((tF32) (OSAL_C_DOUBLE_PI * (double) u16Grad / 180.0));
}


/**************************************************************************//**
 * @brief convert angle in radian to grad
 ******************************************************************************/
tF32 
f32angRad2Grad(
   tF32 tF32Rad /**< [in] angle in radians */
)
/**************************************************************************//**
 * 
 ******************************************************************************/
{
   return ((tF32)( 180.0 * tF32Rad / OSAL_C_DOUBLE_PI ));
}


/**************************************************************************//**
 * @brief set the rotation matrix according to the euler angles
 ******************************************************************************/
void 
vSetRotMatrix( 
   tU16 u16Alpha,   /**< [in] euler angle alpha */
   tU16 u16Beta,    /**< [in] euler angle beta */
   tU16 u16Gamma,   /**< [in] euler angle gamme */
   taf32Mat3d af32A /**< [out] rotation matrix described by the euler angles */
)
/**************************************************************************//**
 * In the EOL alpha is called OFFSET_X, beta OFFSET_Y and gamme OFFSET_Z
 * For GM (left handed coordinate system) 
 * If you have installed emacs you can use the calc-mode to verify the results
 * create calc variables a,b c which hold the euler angles
 * ( a=alpha rotates around Y , b=beta rotates around X', c=gamma rotates around Z")
 * and a variable of name nYXZ which holds the rotation matrix
 * [ [ cos(a) cos(c) - sin(a) sin(b) sin(c), cos(a) sin(c) + sin(a) sin(b) cos(c), -(sin(a) cos(b)) ]
 * [           -(cos(b) sin(c)),                      cos(b) cos(c),                  sin(b)      ]
 * [ sin(a) cos(c) + cos(a) sin(b) sin(c), sin(a) sin(c) - cos(a) sin(b) cos(c),  cos(a) cos(b)   ] ]
 ******************************************************************************/
{
   const tF32 f32C1 = (tF32) OSAL_dCos( (double) f32angGrad2Rad(u16Alpha) );
   const tF32 f32S1 = (tF32) OSAL_dSin( (double) f32angGrad2Rad(u16Alpha) );
   const tF32 f32C2 = (tF32) OSAL_dCos( (double) f32angGrad2Rad(u16Beta)  );
   const tF32 f32S2 = (tF32) OSAL_dSin( (double) f32angGrad2Rad(u16Beta)  );
   const tF32 f32C3 = (tF32) OSAL_dCos( (double) f32angGrad2Rad(u16Gamma) );
   const tF32 f32S3 = (tF32) OSAL_dSin( (double) f32angGrad2Rad(u16Gamma) );

   af32A[0] = f32C1 * f32C3 - f32S1 * f32S2 * f32S3;
   af32A[1] = f32C1 * f32S3 + f32S1 * f32S2 * f32C3;
   af32A[2] = -(f32S1 * f32C2);
   af32A[3] = -(f32C2 * f32S3);
   af32A[4] = f32C2 * f32C3;
   af32A[5] = f32S2;
   af32A[6] = f32S1 * f32C3 + f32C1 * f32S2 * f32S3;
   af32A[7] = f32S1 * f32S3 - f32C1 * f32S2 * f32C3;
   af32A[8] = f32C1 * f32C2;

   return;
}


/**************************************************************************//**
 * @brief calculate the CM mounting angles 
 ******************************************************************************/
void vCalcRSTXYZangles(
   tU16 u16Alpha,    /**< [in] euler angle alpha   */
   tU16 u16Beta,     /**< [in] euler angle beta    */
   tU16 u16Gamma,    /**< [in] euler angle gamm    */
   taf32Mat3d af32A  /**< [out] all nine CM angles */
)
/**************************************************************************//**
 * take the GM euler angles and calculate the resulting CM mounting angles which
 * describe the angles between the axes RX, RY, RZ, SX, SY, SZ, TX, TY and TZ
 ******************************************************************************/
{
   // this is for BOSCH BMI055
   // turns RST to GM coordinates
   const taf32Mat3d af32MM={
       0.0f, -1.0f,  0.0f,
       0.0f,  0.0f, -1.0f,
      -1.0f,  0.0f,  0.0f
   };

   // rotation of the GM-System according to euler angles. Will be filled after
   // reading out the EOL parameters
   taf32Mat3d af32MR={
      0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f
   };

   // turns the GM System to the BOSCH System
   const taf32Mat3d af32MN={
      1.0f,  0.0f, 0.0f,
      0.0f, -1.0f, 0.0f,
      0.0f,  0.0f, 1.0f
   };

   // Z=N*R*M complete rotation inluding transformation from RST to GM
   // coordinats (matrix M), euler rotation (matrix R) and transformation from
   // GM to XYZ-Bosch coordinates (matrix N)
   taf32Mat3d af32MZ = {
      0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f
   };

   taf32Mat3d af32Mtemp = {
      0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f
   };

   // unit vectors E of the RST system
   taf32Vec3d af32VecEr = {1.0f, 0.0f, 0.0f};
   taf32Vec3d af32VecEs = {0.0f, 1.0f, 0.0f};
   taf32Vec3d af32VecEt = {0.0f, 0.0f, 1.0f};

   // unit vectors E of the XYZ-Bosch system (redundant but makes clear what is
   // going on with the inner product)
   taf32Vec3d af32VecEx = {1.0f, 0.0f, 0.0f};
   taf32Vec3d af32VecEy = {0.0f, 1.0f, 0.0f};
   taf32Vec3d af32VecEz = {0.0f, 0.0f, 1.0f};

   taf32Vec3d af32VecOut = {0.0f, 0.0f, 0.0f};

   // Z=N*R*M complete rotation inluding transformation from RST to GM
   vSetRotMatrix( u16Alpha, u16Beta, u16Gamma, af32MR );
   vMatMultMat3d( af32MR, af32MM, af32Mtemp);  
   vMatMultMat3d( af32MN, af32Mtemp, af32MZ);

   // rotate unit vector Er
   vMatMultVec3d(af32MZ, af32VecEr ,af32VecOut);
   
   af32A[0]= f32angRad2Grad( (tF32)OSAL_dArcCos( (tDouble)f32InnerProd3d(af32VecEx, af32VecOut) ));
   af32A[1]= f32angRad2Grad( (tF32)OSAL_dArcCos( (tDouble)f32InnerProd3d(af32VecEy, af32VecOut) ));
   af32A[2]= f32angRad2Grad( (tF32)OSAL_dArcCos( (tDouble)f32InnerProd3d(af32VecEz, af32VecOut) ));

   // rotate unit vector Es
   vMatMultVec3d(af32MZ, af32VecEs ,af32VecOut);

   af32A[3]= f32angRad2Grad( (tF32)OSAL_dArcCos( (tDouble)f32InnerProd3d(af32VecEx, af32VecOut) ));
   af32A[4]= f32angRad2Grad( (tF32)OSAL_dArcCos( (tDouble)f32InnerProd3d(af32VecEy, af32VecOut) ));
   af32A[5]= f32angRad2Grad( (tF32)OSAL_dArcCos( (tDouble)f32InnerProd3d(af32VecEz, af32VecOut) ));

   // rotate unit vector Et
   vMatMultVec3d(af32MZ, af32VecEt ,af32VecOut);

   af32A[6]= f32angRad2Grad( (tF32)OSAL_dArcCos( (tDouble)f32InnerProd3d(af32VecEx, af32VecOut) ));
   af32A[7]= f32angRad2Grad( (tF32)OSAL_dArcCos( (tDouble)f32InnerProd3d(af32VecEy, af32VecOut) ));
   af32A[8]= f32angRad2Grad( (tF32)OSAL_dArcCos( (tDouble)f32InnerProd3d(af32VecEz, af32VecOut) ));

   return;
}
