/*******************************************************************************
 * COPYRIGHT RESERVED, 2014 Robert Bosch Car Multimedia GmbH.
 * All rights reserved. The reproduction, distribution and utilization of this
 * document as well as the communication of its contents to others without 
 * explicit authorization is prohibited. Offenders will be held liable for the 
 * payment of damage. All rights reserved in the event of the grant of a patent,
 * utility model or design.
 *******************************************************************************/
/**
 * @file
 *
 * @brief    functions to get the mounting position from the EOL
 * 
 * @author   CM-DI/ECO1 Joachim Friess
 *
 * @par History: 
 * Joachim Friess 
 ******************************************************************************/
#include <math.h>
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "eol_mounting.h"
#include "gm_euler_rotation.h"
#include "EOLLib.h"
#include "sensor_dispatcher.h"

/**************************************************************************//**
 * @brief convert tF32 angle to tU8 angle
 ******************************************************************************/
tU8
tU8ConvF32Angle(
   tF32 f32Angle /**< input angle to convert */
)
/**************************************************************************//**
 * conversion of a floating point mounting angle to an tU8 mounting
 * angle. Handle negative values and angles > 180°.
 ******************************************************************************/
{
   tU8 u8Angle = 0;
   
   f32Angle = fabsf ((float) f32Angle);

   if ( isgreater( f32Angle, (tF32) 360.0 ))
   {
      f32Angle = (tF32) fmodf ((float) f32Angle, (float) 360.0);
   }      

   if ( isgreater(f32Angle,(tF32) 180.0) )
   {
      f32Angle = (tF32) 360.0 - f32Angle;
   }

   u8Angle = (tU8) rintf((float) f32Angle);
   return (u8Angle);
}

/**************************************************************************//**
 * @brief copy the mounting angles stored in a matrix to the OSAL structure
 ******************************************************************************/
tBool 
bCopyMountAngles( 
   taf32Mat3d af32A,                      /**< [in] mounting angles  */
   OSAL_tr3dMountAngles  *pr3dMountAngles /**< [out] mounting angles */
)
/**************************************************************************//**
 * the values from the 3x3 float matrix are converted and copied to
 * the OSAL struct which has a tU8 entry for each angle
 ******************************************************************************/
{
   tBool bReturn = TRUE;

   if (NULL != pr3dMountAngles)
   {
      pr3dMountAngles->u8AngRX = tU8ConvF32Angle(af32A[0]);
      pr3dMountAngles->u8AngRY = tU8ConvF32Angle(af32A[1]);
      pr3dMountAngles->u8AngRZ = tU8ConvF32Angle(af32A[2]);
      pr3dMountAngles->u8AngSX = tU8ConvF32Angle(af32A[3]);
      pr3dMountAngles->u8AngSY = tU8ConvF32Angle(af32A[4]);
      pr3dMountAngles->u8AngSZ = tU8ConvF32Angle(af32A[5]);
      pr3dMountAngles->u8AngTX = tU8ConvF32Angle(af32A[6]);
      pr3dMountAngles->u8AngTY = tU8ConvF32Angle(af32A[7]);
      pr3dMountAngles->u8AngTZ = tU8ConvF32Angle(af32A[8]);
   }
   else
   {
      bReturn = FALSE;
   }
   return (bReturn);
}


/**************************************************************************//**
 * @brief get the mounting angles from the EOL (GM only)
 ******************************************************************************/
tBool 
bGetMountingfromEOL( 
   OSAL_tr3dMountAngles *pr3dMountAngles //!< [out] mounting angles from EOL
)
/**************************************************************************//**
 * It fetches the EOL mounting angles and converts these euler angles to the
 * nine angles that are used by Bosch to describe the mounting position of the
 * gyro in the car. EOL is used only in the GM projects
 ******************************************************************************/
{
   OSAL_tIODescriptor EOLFd;
   OSAL_trDiagEOLEntry rEOLData;
   tU16 au16Angles[3]={0};
   tU32 u32Cnt=0;
   tS32 s32Length;
   tBool bSucess = TRUE;
   taf32Mat3d af32A;

   EOLFd = OSAL_IOOpen (OSAL_C_STRING_DEVICE_DIAG_EOL, (OSAL_tenAccess)0 );

   if( (EOLFd != OSAL_ERROR) && (NULL != pr3dMountAngles))
   {
      tU16 au16Offsets[3]={ EOLLIB_OFFSET_GYRO_OFFSET_X,\
			    EOLLIB_OFFSET_GYRO_OFFSET_Y,\
			    EOLLIB_OFFSET_GYRO_OFFSET_Z};


      rEOLData.u8Table        = EOLLIB_TABLE_ID_BRAND;
      rEOLData.u16EntryLength = sizeof(tU16);

      for (u32Cnt= 0; u32Cnt < 3; u32Cnt++)
      {
	 rEOLData.u16Offset    = au16Offsets[u32Cnt];
	 rEOLData.pu8EntryData = (tU8*) &au16Angles[u32Cnt];

	 s32Length = OSAL_s32IORead( EOLFd, 
				     (tPS8)(& rEOLData), 
				     (tU32)(sizeof(rEOLData)));

	 if (s32Length  >  0)
	 {
	    /* trace */
	 }
	 else
	 {
	    au16Angles[u32Cnt] = 0;
	    bSucess = FALSE;
	 }
      }
      OSAL_s32IOClose(EOLFd);
     
      if ( bSucess )
      {

         SenDisp_vTraceOut( TR_LEVEL_USER_1,
                            "EOL GYRO_OFFSET X:%u Y:%u Z:%u",
                            au16Angles[0],
                            au16Angles[1],
                            au16Angles[2] );
         
	 vCalcRSTXYZangles(au16Angles[0], au16Angles[1], au16Angles[2], af32A);
	 bSucess = bCopyMountAngles(af32A, pr3dMountAngles);
      }
   }
   else
   {
      /* could not open EOL device or NULL pointer as parameter */
      bSucess = FALSE;
   }
   return bSucess;
}
