/******************************************************************************
* FILE         : sensor_proxt_test_stub_reject_msg.c
*             
* DESCRIPTION  : This file implements a test stub that simulates v850 for sensor proxy.
*                In GEN3 gyro, odometer and accelerometer sensor hardwares are present on V850.
*                Sensor data will be sent from V850 to imx via INC. It is the job of sensor
*                data proxy module to collect this data, and deliver this to VD-Sensor.
*                As we dont have v850 up presently, functionality of sensor proxy is tested 
*                using this test stub. It communicates with sensor proxy on imx via socket using TCP/IP.
*                IP Address of imx:  172.17.0.1
*                IP addredd of ubuntu in which test stub is executed shall be : 172.17.0.6
* 
* AUTHOR(s)    : Srinivas Prakash Anvekar (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 01.JUL.2015|  Initialversion 1.0  | Srinivas Prakash Anvekar (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<limits.h>
#include<errno.h>
#include<sys/types.h>

/* inet_pton() */
#include <arpa/inet.h>
/* htons() */
#include <netinet/in.h>
#include<sys/socket.h>

#include <time.h>

/* To Comply with bosch standards */

typedef unsigned char tU8;
typedef signed char tS8;
typedef unsigned long tU32;
typedef const char * tCString;
typedef unsigned char tBool;
typedef unsigned short tU16;
typedef int tS32;
typedef void * tPVoid;
typedef void tVoid;

/* OUR error codes. */
#define OSAL_OK 0
#define OSAL_ERROR -1

/*  Data sizes af different message fields  */
#define STATUS_MSG_SIZE (3)
#define CONFIG_MSG_SIZE (5)
#define REJECT_MSG_SIZE (3)

/*  Data sizes af different message fields  */
#define MSG_SIZE_SCC_SENSOR_R_ODO_DATA                (7)
#define MSG_SIZE_SCC_SENSOR_R_GYRO_DATA               (10)
#define MSG_SIZE_SCC_SENSOR_R_ACC_DATA               (10)
#define MSG_SIZE_SCC_SENSOR_R_GYRO_TEMP               (3)
#define MSG_SIZE_SCC_SENSOR_R_ACC_TEMP                (3)

/* Buffer to send data */
static tU8 u8Buff[1024];

/* Handle to socket */
tS32 s32ConnFD;

/* For delay */
struct timespec req;

/* For incrementing sensor dummy data */
static int cnt = 0;

/* socket function descriptor */
tS32 s32SocketFD= 0;


tVoid vCommunicateToAPP();
tVoid vFillOdoData(tU32 u32Offset);
tVoid vFillGyroData(tU32 u32Offset);
tVoid vFillAccData(tU32 u32Offset);
tVoid vFillGyroTemp(tU32 u32Offset);
tVoid vFillAccTemp(tU32 u32Offset);
tVoid SendData(tU32 u32Bytes);
tVoid SocSetup();

tVoid SocSetup()
{

   struct sockaddr_in rSocAttr;
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32OptState = 1;

   memset(&rSocAttr,0, sizeof(rSocAttr));
   rSocAttr.sin_family      = AF_INET;
   rSocAttr.sin_port        = htons(6); // Todo: this has to be changed
   rSocAttr.sin_addr.s_addr = htonl(INADDR_ANY);

   /* create a socket
   SOCK_STREAM: sequenced, reliable, two-way, connection-based
   byte streams with an OOB data transmission mechanism.*/
   s32SocketFD = socket(AF_INET, SOCK_STREAM, 0);
   if(s32SocketFD == -1)
   {
      perror("socket() failed");
   }
   else
   {
      /* This option informs os to reuse the socket address even if it is busy*/
      if(setsockopt(s32SocketFD, SOL_SOCKET, SO_REUSEADDR,
                    &s32OptState, sizeof(s32OptState)) == -1)
      {
         perror( "!!!setsockopt() Failed");
      }
      /* Bind the socket created to a address */
      s32RetVal = bind(s32SocketFD, (struct sockaddr *)&rSocAttr, sizeof(rSocAttr));
      if(s32RetVal == -1)
      {
         perror("Bind Failed");
      }
      else
      {
         /* Listen system call makes the socket as a passive socket. 
            i.e socket will be used to accept incoming connections */
         s32RetVal = listen(s32SocketFD, 10);
         if(s32RetVal == -1)
         {
            perror("Listen Failed");
         }
         else
         {
            s32RetVal = OSAL_OK;
            s32ConnFD = accept(s32SocketFD, NULL, NULL);
            if(s32ConnFD == -1)
            {
               perror( "accept Failed !!!");
               /* This should never happen. Only reason for this is something went
                  wrong while configuring the socket */
            }
            else
            {
               perror( "Socket creation and Setup completed successfully");
            }
         }
      }
   }
}


/* Start of program execution  */

tS32 main(tVoid)
{
   SocSetup();
   vCommunicateToAPP(s32ConnFD);
   while(1)
      sleep(5);
}

/* All communications to APP (IMX) is done from here */
tVoid vCommunicateToAPP()
{
   tS32 s32ErrChk = OSAL_ERROR;
   tU32 u32ConfigSent = 0;
   tU32 u32Offset = 0;
   
   /* First IMX is expected to send status. So call recv from socket */
   s32ErrChk = recv(s32ConnFD, &u8Buff, STATUS_MSG_SIZE,0);
   if(s32ErrChk == STATUS_MSG_SIZE)
   {
   
      printf("Msg ID %d  APP STATUS %d Version Info %d\n", u8Buff[0], u8Buff[1], u8Buff[2]);
      
      /*Send Reject message to IMX*/
      u8Buff[0] = REJECT_MSG_SIZE; /* In every message sent to IMX, 
                                          (First_byte) = (Total bytes transmitted -1)*/
                                    
      u8Buff[1] = 0x0B;/* Message ID */
      u8Buff[2] = 0x03;/* Reject Reason = REJ_TMP_UNAVAIL */
      u8Buff[3] = 0x20;/* reject component status*/
      s32ErrChk = write( s32ConnFD, &u8Buff, 4);

      
       
      if(s32ErrChk == 4)
      {
         /* to accept further data from imx a second accept is needed */   
         s32ConnFD = accept(s32SocketFD, NULL, NULL);
         if(s32ConnFD == -1)
         {
            perror( "accept Failed !!!");
         }
      
         /* Receive status msg from imx */
         s32ErrChk = recv(s32ConnFD, &u8Buff, STATUS_MSG_SIZE,0);
         if(s32ErrChk == STATUS_MSG_SIZE)
         {
            
            printf("Msg ID %d  APP STATUS %d Version Info %d\n", u8Buff[0], u8Buff[1], u8Buff[2]);
            
            /* Send inactive status to IMX */
            u8Buff[0] = 3; /* In every message sent to IMX, 
                                                (First_byte) = (Total bytes transmitted -1)*/
                                          
            u8Buff[1] = 0x21;/* Message ID */
            u8Buff[2] = 0x02;/* Status as inactive */
            u8Buff[3] = 0x01;/* version info */
            s32ErrChk = write( s32ConnFD, &u8Buff, 4);
            
            
            
            if(s32ErrChk == 4)
            {
               /* to accept further data from imx a another accept is needed */
               s32ConnFD = accept(s32SocketFD, NULL, NULL);
               if(s32ConnFD == -1)
               {
                  perror( "accept Failed !!!");
               }
            
               /* Receive status msg from imx */
               s32ErrChk = recv(s32ConnFD, &u8Buff, STATUS_MSG_SIZE, 0);
               if(s32ErrChk == STATUS_MSG_SIZE)
               {
                  printf("Msg ID %d  APP STATUS %d Version Info %d\n", u8Buff[0], u8Buff[1], u8Buff[2]);
                  
                  /* Send active status Msg to Imx */
                  u8Buff[0] = 3; /* In every message sent to IMX, 
                                                      (First_byte) = (Total bytes transmitted -1)*/
                                                
                  u8Buff[1] = 0x21;/* Message ID */
                  u8Buff[2] = 0x01;/* Status as active */
                  u8Buff[3] = 0x01;/* version info */
                  
                  s32ErrChk = write( s32ConnFD, &u8Buff, 4);
                  if( s32ErrChk == 4 )
                  {
                     printf("status message sent sucessfully \n");
                              
                     u8Buff[0]=11;// (bytes_transmitted -1)
                     u8Buff[1]=0x31;// Msg ID
                     u8Buff[2]=100;// Odo Config Interval
                     u8Buff[3]=0;// ----||----
                     u8Buff[4]=50;// gyro Config intervall
                     u8Buff[5]=0;
                     u8Buff[6]=1; // gyro type
                     u8Buff[7]=0xf4;// ACC interval 500 0x1f4
                     u8Buff[8]=1;
                     u8Buff[9]=2;//ACC type
                     u8Buff[10]=0;// ABS Config Interval
                     u8Buff[11]=0;// ----||----

                     /* Send config message. */
                     s32ErrChk = write(s32ConnFD, &u8Buff,12);
                     if(s32ErrChk == 10)
                     {
                        printf("Config msg sent \n");
                        u32ConfigSent = 1;
                        //sleep(5);
                     }
                     else
                     {
                        perror("!!! config msg send Failed \n");
                     }
                  }
                  else
                  {
                     perror("!!!active status msg send failed \n");
                  }
               }
               else
               {
                  perror(" recv from imx failed \n ");
               }
            }
            else
            {
               perror(" inactive status msg send failed  \n");
            }
         }
         else
         {
            perror("!!! receive after reject failed \n");
         }
      }
      else
      {
         perror(" reject message write to socket failed  \n");
      }
   }
   else
   {
      perror("");
      printf("recv ??? Boss something is wrong Err %d\n",s32ErrChk);
   }

   if(u32ConfigSent == 1)
   {
      printf("Congfig msg send was successful \n");

      u8Buff[1] = 0x41; /* Message ID */
      u8Buff[2] = 5;  /* number of entries */
      u8Buff[3] = 0;

      u32Offset+=4;

      /* Add Odo data */
      vFillOdoData(u32Offset);
      u32Offset+=MSG_SIZE_SCC_SENSOR_R_ODO_DATA;
#if 1
      /* Add Gyro data */
      vFillGyroData(u32Offset);
      u32Offset+=MSG_SIZE_SCC_SENSOR_R_GYRO_DATA;
      /* Add ACC data */
      vFillAccData(u32Offset);
      u32Offset+=MSG_SIZE_SCC_SENSOR_R_ACC_DATA;
      /* Add ACC TEMP */
      vFillAccTemp(u32Offset);
      u32Offset+=MSG_SIZE_SCC_SENSOR_R_ACC_TEMP;
      /* Add gyro Temp */
      vFillGyroTemp(u32Offset);
      u32Offset+=MSG_SIZE_SCC_SENSOR_R_GYRO_TEMP;
#endif

      printf("\n total bytes u32Offset: %lu  \n",u32Offset);

      /* number of bytes transmitted */
      u8Buff[0] = u32Offset - 1;

       SendData(u32Offset);

      /* Send a diagnosis message */
       u8Buff[0] = 9; /* (bytes transmitted -1) */
       u8Buff[1] = 0x41;// (Msg ID)
       u8Buff[2] = 1; // (Number of entries)
          u8Buff[3] = 0;
       u8Buff[4] = 0; // Entry type: diag
       u8Buff[5] = 5; //( time stamp )
       u8Buff[6] = 0;
       u8Buff[7] = 0;
       u8Buff[8] = 212; // ( Diag message just some number to see same is received )
       u8Buff[9] = 0;

      SendData(10);

      /* Send data in loop */
      
      u8Buff[1] = 0x41; /* Msg ID */
      u8Buff[2] = 3; /* Number of entries */
      u8Buff[3] = 0; 

      /* Delay between 2 samples in namo seconds */
      req.tv_sec = 0;
      req.tv_nsec = 100000000; //50 milli seconds
               
      while(cnt < 500)
      {

         nanosleep(&req, NULL);

         u32Offset=4;
         cnt++; // Increment data before sending
         
         vFillOdoData(u32Offset);
         u32Offset+=MSG_SIZE_SCC_SENSOR_R_ODO_DATA;

         vFillGyroData(u32Offset);
         u32Offset+=MSG_SIZE_SCC_SENSOR_R_GYRO_DATA;
         
         vFillAccData(u32Offset);
         u32Offset+=MSG_SIZE_SCC_SENSOR_R_ACC_DATA;
#if 0
         vFillAccTemp(u32Offset);
         u32Offset+=MSG_SIZE_SCC_SENSOR_R_ACC_TEMP;
         
         vFillGyroTemp(u32Offset);
         u32Offset+=MSG_SIZE_SCC_SENSOR_R_GYRO_TEMP;
#endif
         printf("\n total bytes u32Offset: %lu  \n",u32Offset);
         
         u8Buff[0] = u32Offset - 1;
         /* Number of bytes sent */
         SendData(u32Offset);
      
      }
   }

   
}

tVoid vFillOdoData(tU32 u32Offset)
{

   tS32 s32ErrChk = OSAL_ERROR;
   static tU16 u16Wheelcnt = 200;
   static int i =0;

   u16Wheelcnt += 300;
   i++;

   if(u16Wheelcnt > 0x03ff)
      u16Wheelcnt -= 0x03ff;
   
   memcpy(&u8Buff[u32Offset + 4], &u16Wheelcnt, 2);

   u8Buff[u32Offset + 0] = 1;//type
   u8Buff[u32Offset + 1] = 1+cnt;//ts
   u8Buff[u32Offset + 2] = 0;
   u8Buff[u32Offset + 3] = 0;
   if (i > 5)
      u8Buff[u32Offset + 6] = 2;
   else
      u8Buff[u32Offset + 6] = 1;

   if ((i % 5) == 0)
   u8Buff[u32Offset + 3] = 100;


}
tVoid vFillAccData(tU32 u32Offset)
{
   static int i =0;
   i++;
   tS32 s32ErrChk = OSAL_ERROR;

   u8Buff[u32Offset + 0] = 4;//type
   u8Buff[u32Offset + 1] = 2+cnt;//ts
   u8Buff[u32Offset + 2] = 0;
   u8Buff[u32Offset + 3] = 0;
   u8Buff[u32Offset + 4] = 0x0f4+cnt; // R data
   u8Buff[u32Offset + 5] = 1;
   u8Buff[u32Offset + 6] = 0x058+cnt; // S data
   u8Buff[u32Offset + 7] = 2;
   u8Buff[u32Offset + 8] = 255+cnt; // T data
   u8Buff[u32Offset + 9] = 255;
   if ((i % 5) == 0)
   u8Buff[u32Offset + 3] = 100;

}

tVoid vFillGyroData(tU32 u32Offset)
{
   static int i =0;
   i++;
   tS32 s32ErrChk = OSAL_ERROR;

   u8Buff[u32Offset + 0] = 2;//type
   u8Buff[u32Offset + 1] = 3+cnt;//ts
   u8Buff[u32Offset + 2] = 0;
   u8Buff[u32Offset + 3] = 0;
   u8Buff[u32Offset + 4] = 76+cnt; // R data
   u8Buff[u32Offset + 5] = 0;
   u8Buff[u32Offset + 6] = 54+cnt; // S data
   u8Buff[u32Offset + 7] = 0;
   u8Buff[u32Offset + 8] = 32+cnt; // T data
   u8Buff[u32Offset + 9] = 0;
   if ((i % 5) == 0)
   u8Buff[u32Offset + 3] = 100;


}

tVoid vFillGyroTemp(tU32 u32Offset)
{

   tS32 s32ErrChk = OSAL_ERROR;

   u8Buff[u32Offset + 0] = 3;//type
   u8Buff[u32Offset + 1] = 100;//temp
   u8Buff[u32Offset + 2] = 1;

}

tVoid vFillAccTemp(tU32 u32Offset)
{

   tS32 s32ErrChk = OSAL_ERROR;


   u8Buff[u32Offset + 0] = 5;//type
   u8Buff[u32Offset + 1] = 100;//temp
   u8Buff[u32Offset + 2] = 2;


}


tVoid SendData(tU32 u32Bytes)
{

   tS32 s32ErrChk;
   /* Send data */
   s32ErrChk = write(s32ConnFD, &u8Buff, u32Bytes);
   if(s32ErrChk == u32Bytes)
   {
      printf("u32Bytes %lu Sent \n",u32Bytes);
   }
   else
   {
      perror("Send failed Failed");
   }


}