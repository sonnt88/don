sensor_proxy_test_stub_rej_msg.c
The following use cases are handled in this test Stub
1)Reject message sent from stub( v850 ) to imx
2)Inactive status message sent from stub( v850 ) to imx
3)Active status and Config message sent from stub( v850 ) to imx
Procedure to execute:
Step 1: A macro is defined for test stub ( SENSOR_PROXY_TEST_STUB_ACTIVE). Enable the sensor_dispatcher macro for the test 
        stub.

Step 2: build the project for sensordrv component after enabling the stub.

Step 3: Open another terimal and cd to the below path,
	Path : \<mounted folder>\ccstg\<view>\ai_osal_linux\components\devices\sensors_proxy\test_stub 

Step 4: build the test stub using the command,
        Cmd: cc sensor_proxy_test_stub_rej_msg.c

Step 5: Run the stub, 
	Cmd : sudo ./a.out

Step 6: Flash the sensordrv binary to the target.

Step 7: Open TTFis and load the trc and run one of the PoS OEDTs.

Step 8: The test case should pass with expected results.
