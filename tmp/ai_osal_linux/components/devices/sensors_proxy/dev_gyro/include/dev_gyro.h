/******************************************************************************
* FILE            : dev_gyro.h
*
*
* DESCRIPTION     : This is the header file for dev_gyro.c
*
* AUTHOR(s)       :  (RBEI/ECF5)
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*15.MAR.2013| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/
#ifndef DEV_GYRO_HEADER

#define DEV_GYRO_HEADER

/********************************************************************************************
*                                   Macro Declarations                                      *
*********************************************************************************************/

/********************************************************************************************
*                           Variable Declaration (Scope: Global)                            *
*********************************************************************************************/


typedef struct
{
    tU32 u32AdcRangeMin;
    tU32 u32AdcRangeMax;
    tU32 u32SampleMin;
    tU32 u32SampleMax;
    tF32 f32MinNoiseValue;
    tF32 f32EstimOffset;
    tF32 f32MinOffset;
    tF32 f32MaxOffset;
    tF32 f32DriftOffset;
    tF32 f32MaxUnsteadOffset;
    tF32 f32BestCalibOffset;
    tF32 f32EstimscaleFactor;
    tF32 f32MinScaleFactor;
    tF32 f32MaxScaleFactor;
    tF32 f32DriftScaleFactor;
    tF32 f32MaxUnsteadScaleFactor;
    tF32 f32BestCalibScaleFactor;
}trGyroSensorHwInfo;

typedef enum 
{    
GYRO_BST_BMI055,    //3D Gyro
GYRO_INVALID
}tenGyroModel;



/********************************************************************************************
*                          Function Prototypes (Scope: Local)                               *
*********************************************************************************************/
tS32 GYRO_s32IOOpen(tVoid);
tS32 GYRO_s32IOClose(tVoid);
tS32 GYRO_s32IOControl(tS32 s32fun, tLong sArg);
tS32 GYRO_s32IORead(tPS8 ps8Buffer, tU32 u32maxbytes);


#endif

/*End of file*/
