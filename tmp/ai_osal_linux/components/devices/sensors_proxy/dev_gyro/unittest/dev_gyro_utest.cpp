#define SENSPXYMOCKSTOBEREMOVED
#include "Sensor_InternalMock.h"
#include "Sensor_InternalMock.cpp"

#ifdef __cplusplus
extern "C"
{
#include "../source/dev_gyro.c"
}
#endif

using namespace testing;
using namespace std;

int main(int argc, char** argv)
{
   ::testing::InitGoogleMock(&argc, argv);
   return RUN_ALL_TESTS();
}

tVoid Gyro_setThreadRunning()
{
   bGyroThreadRunning = FALSE;
}

tVoid Gyro_ResetGlobals()
{
   bisGyoropen = FALSE;
   bGyroConfigEn = FALSE;
   nGyroModel = GYRO_INVALID;
   bGyroThreadRunning = FALSE;
   rGyroConfigData = {0};
}

class Gyro_InitGlobals : public ::testing::Test
{
public:
   SensorMock PoS;
   NiceMock<OsalMock> Osal_mock;
   virtual void SetUp()
   {
      Gyro_ResetGlobals();
   }
};

class Gyro_Open : public Gyro_InitGlobals
{
public:
   virtual void SetUp()
   {
      tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
      pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
      pu8Buff[1] = GYRO_INVALID;
      pu8Buff[2] = 50;
      pu8Buff[3] = 0;
      Gyro_InitGlobals::SetUp();
      EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trGyroConfigData))));
      EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
      GYRO_s32IOOpen();
   }
};

/********************Gyro Open********************/

TEST_F(Gyro_InitGlobals, Gyro_IOOpen_Success)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trGyroConfigData))));
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_s32IOOpen();
   EXPECT_EQ(GYRO_BST_BMI055, nGyroModel);
   EXPECT_EQ(50, rGyroConfigData.u16GyroDataInterval);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(TRUE, bGyroConfigEn);
   EXPECT_EQ(TRUE, bisGyoropen);
   EXPECT_EQ(TRUE, bGyroThreadRunning);
}

TEST_F(Gyro_InitGlobals, Gyro_IOOpen_DispError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_s32IOOpen();
   EXPECT_EQ(GYRO_INVALID, nGyroModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bGyroConfigEn);
   EXPECT_EQ(FALSE, bisGyoropen);
   EXPECT_EQ(FALSE, bGyroThreadRunning);
}

TEST_F(Gyro_InitGlobals, Gyro_IOOpen_RngBuffError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GYRO_s32IOOpen();
   EXPECT_EQ(GYRO_INVALID, nGyroModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bGyroConfigEn);
   EXPECT_EQ(FALSE, bisGyoropen);
   EXPECT_EQ(FALSE, bGyroThreadRunning);
}


TEST_F(Gyro_InitGlobals, Gyro_IOOpen_MsgQueOpenError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_s32IOOpen();
   EXPECT_EQ(GYRO_INVALID, nGyroModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bGyroConfigEn);
   EXPECT_EQ(FALSE, bisGyoropen);
   EXPECT_EQ(FALSE, bGyroThreadRunning);
}

TEST_F(Gyro_InitGlobals, Gyro_IOOpen_MsgQueWaitError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(Return(0));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_s32IOOpen();
   EXPECT_EQ(GYRO_INVALID, nGyroModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bGyroConfigEn);
   EXPECT_EQ(FALSE, bisGyoropen);
   EXPECT_EQ(FALSE, bGyroThreadRunning);
}

TEST_F(Gyro_InitGlobals, Gyro_IOOpen_ThdSpwnError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trGyroConfigData))));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_s32IOOpen();
   EXPECT_EQ(GYRO_BST_BMI055, nGyroModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(TRUE, bGyroConfigEn);
   EXPECT_EQ(FALSE, bisGyoropen);
}

TEST_F(Gyro_InitGlobals, Gyro_IOOpen_AlreadyOpen)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   bisGyoropen = TRUE;
   s32RetVal = GYRO_s32IOOpen();
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/********************Gyro Close********************/

TEST_F(Gyro_Open, Gyro_close_success)
{
   tS32 s32RetVal = OSAL_ERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GYRO_C_EV_THD_EXT),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32MessageQueueStatus(_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<3>(1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trGyroConfigData))));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal=GYRO_s32IOClose();
   EXPECT_EQ(FALSE,bisGyoropen);
   EXPECT_EQ(FALSE,bGyroThreadRunning);
   EXPECT_EQ(OSAL_E_NOERROR,s32RetVal);
}

TEST_F(Gyro_Open, Gyro_close_RngBufError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GYRO_C_EV_THD_EXT),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32MessageQueueStatus(_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<3>(1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trGyroConfigData))));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_s32IOClose();
   EXPECT_EQ(FALSE,bisGyoropen);
   EXPECT_EQ(FALSE,bGyroThreadRunning);
   EXPECT_EQ(OSAL_ERROR,s32RetVal);
}

TEST_F(Gyro_Open, Gyro_close_DeinitError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_GYRO_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GYRO_C_EV_THD_EXT),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32MessageQueueStatus(_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<3>(1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trGyroConfigData))));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_s32IOClose();
   EXPECT_EQ(FALSE,bisGyoropen);
   EXPECT_EQ(FALSE,bGyroThreadRunning);
   EXPECT_EQ(OSAL_ERROR,s32RetVal);
}

/********************Gyro TgrTdExtAndClrMsgQ ********************/

TEST_F(Gyro_Open, Gyro_TgrTdExtAndClrMsgQ_Success)
{
   tS32 s32RetVal = OSAL_ERROR;
   tU8 pu8Buff[GYRO_MSG_QUE_LENGTH];
   OSAL_tEventMask u32EveThdExtRes = GYRO_C_EV_THD_EXT;
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = GYRO_INVALID;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GYRO_C_EV_THD_EXT),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32MessageQueueStatus(_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<3>(1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(trGyroConfigData))));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_s32TgrTdExtAndClrMsgQ();
   EXPECT_EQ(OSAL_OK,s32RetVal);
}


/********************Gyro Handle Event********************/
TEST_F(Gyro_Open, Gyro_HandleEventCreate_ReturnOK)
{
   tU8 u8Task = EVENT_CREATE;
   tS32 s32RetVal= OSAL_ERROR;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_S32HandleEvent(u8Task);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Gyro_Open, Gyro_HandleEventCreate_RetError)
{
   tU8 u8Task = EVENT_CREATE;
   tS32 s32RetVal= OSAL_ERROR;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GYRO_S32HandleEvent(u8Task);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}


TEST_F(Gyro_Open, Gyro_HandleEventDelete_ReturnOK)
{
   tU8 u8Task = EVENT_DELETE;
   tS32 s32RetVal= OSAL_ERROR;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_S32HandleEvent(u8Task);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Gyro_Open, Gyro_HandleEventDelete_RetError)
{
   tU8 u8Task = EVENT_DELETE;
   tS32 s32RetVal= OSAL_ERROR;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GYRO_S32HandleEvent(u8Task);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}


/********************Gyro Selftest********************/
TEST_F(Gyro_Open, Gyro_ExecuteSelfTest)
{
   tS32 s32Dummyargument = OSAL_NULL;
   tS32 s32RetVal = OSAL_ERROR;
   EXPECT_CALL(PoS, s32SensDispTrigGyroSelfTest()).Times(1);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(GYRO_C_EV_SELFTEST_FINISH),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = GYRO_s32ExecuteSelfTest(&s32Dummyargument);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(Gyro_Open, Gyro_ExecuteSelfTest_RetError)
{
   tS32 s32Dummyargument;
   tS32 s32RetVal = OSAL_E_NOERROR;
   EXPECT_CALL(PoS, s32SensDispTrigGyroSelfTest()).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GYRO_s32ExecuteSelfTest(&s32Dummyargument);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(Gyro_Open, Gyro_ExecuteSelfTest_EventWaitFail)
{
   tS32 s32Dummyargument;
   tS32 s32RetVal = OSAL_E_NOERROR;
   EXPECT_CALL(PoS, s32SensDispTrigGyroSelfTest()).Times(1);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GYRO_s32ExecuteSelfTest(&s32Dummyargument);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/********************Gyro Read********************/
TEST_F(Gyro_Open, Gyro_Read)
{
   OSAL_trIOCtrl3dGyroData *pGyroData[5];
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 60;
   for (int i =0; i<5; i++)
   {
      pGyroData[i] = (OSAL_trIOCtrl3dGyroData *)malloc(sizeof(OSAL_trIOCtrl3dGyroData));
      pGyroData[i]->u16ErrorCounter= 0;
      pGyroData[i]->u16Gyro_r = 1000;
      pGyroData[i]->u16Gyro_s = 1000;
      pGyroData[i]->u16Gyro_t = 1000;
      pGyroData[i]->u32TimeStamp = 115200;
   }
   EXPECT_CALL(PoS, s32SensRingBuffs32Read(_,_,_)).Times(1).WillOnce(Return(5));
   s32RetVal = GYRO_s32IORead((tPS8)pGyroData, u32maxbytes);
   EXPECT_EQ(60, s32RetVal);
}

TEST_F(Gyro_Open, Gyro_Read_NoData)
{
   OSAL_trIOCtrl3dGyroData *pGyroData = {0};
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 12;
   s32RetVal = GYRO_s32IORead((tPS8)pGyroData, u32maxbytes);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(Gyro_Open, Gyro_Read_RngBufReadFail)
{
   OSAL_trIOCtrl3dGyroData *pGyroData[5];
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 60;
   for (int i =0; i<5; i++)
   {
      pGyroData[i] = (OSAL_trIOCtrl3dGyroData *)malloc(sizeof(OSAL_trIOCtrl3dGyroData));
      pGyroData[i]->u16ErrorCounter= 0;
      pGyroData[i]->u16Gyro_r = 1000;
      pGyroData[i]->u16Gyro_s = 1000;
      pGyroData[i]->u16Gyro_t = 1000;
      pGyroData[i]->u32TimeStamp = 115200;
   }
   EXPECT_CALL(PoS, s32SensRingBuffs32Read(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = GYRO_s32IORead((tPS8)pGyroData, u32maxbytes);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(Gyro_Open, Gyro_Read_RngBufRetTimeout)
{
   OSAL_trIOCtrl3dGyroData *pGyroData[5];
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 60;
   for (int i =0; i<5; i++)
   {
      pGyroData[i] = (OSAL_trIOCtrl3dGyroData *)malloc(sizeof(OSAL_trIOCtrl3dGyroData));
      pGyroData[i]->u16ErrorCounter= 0;
      pGyroData[i]->u16Gyro_r = 1000;
      pGyroData[i]->u16Gyro_s = 1000;
      pGyroData[i]->u16Gyro_t = 1000;
      pGyroData[i]->u32TimeStamp = 115200;
   }
   EXPECT_CALL(PoS, s32SensRingBuffs32Read(_,_,_)).Times(1).WillOnce(Return(OSAL_E_TIMEOUT));
   s32RetVal = GYRO_s32IORead((tPS8)pGyroData, u32maxbytes);
   EXPECT_EQ(OSAL_E_TIMEOUT, s32RetVal);
}

/********************Gyro IOControl********************/
TEST_F(Gyro_Open, Gyro_IOCtrl_ErrorCase)
{
   //similar to other IOControls, raken IOCTRL_VERSION as example
   tS32 s32RetVal = OSAL_E_NOERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_VERSION;
   tLong sArg = 0;
   s32RetVal = GYRO_s32IOControl(s32fun, sArg);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(Gyro_Open, Gyro_IOCtrl_VerSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_VERSION;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   s32RetVal = GYRO_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(DEV_GYRO_DRIVER_VERSION, s32dummyarg);
}

TEST_F(Gyro_Open, Gyro_IOCtrl_GetCntSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_GYRO_GETCNT;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   EXPECT_CALL(PoS, s32SensRngBuffGetAvailRecs(_)).Times(1).WillOnce(Return(100));
   s32RetVal = GYRO_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(100, s32dummyarg);
}

TEST_F(Gyro_Open, Gyro_IOCtrl_GetTypeSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_GYRO_GET_TYPE;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   s32RetVal = GYRO_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(GYRO_INVALID, s32dummyarg);
}

TEST_F(Gyro_Open, Gyro_IOCtrl_GetCycleTimeSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_GYRO_GETCYCLETIME;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   s32RetVal = GYRO_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(50000000, s32dummyarg);
}

TEST_F(Gyro_Open, Gyro_IOCtrl_GetTempSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_GYRO_GET_TEMP;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   bGyroTempEn = TRUE;
   u16GyroTemperature = 35;
   s32RetVal = GYRO_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(35, s32dummyarg);
}

TEST_F(Gyro_Open, Gyro_IOCtrl_GetHwinfoSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_GYRO_GET_HW_INFO;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   EXPECT_CALL(PoS, s32SensHwInfobGetHwinfo(_,_)).Times(1).WillOnce(Return(TRUE));
   s32RetVal = GYRO_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(Gyro_Open, Gyro_IOCtrl_DefaultCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32func = 100;
   tS32 s32dummyarg;
   tPS32 sP32dummyarg = &s32dummyarg; // dummy value
   s32RetVal = GYRO_s32IOControl(s32func, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOTSUPPORTED, s32RetVal);
}

//EOF
