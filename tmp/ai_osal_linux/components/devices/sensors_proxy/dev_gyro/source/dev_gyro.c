/******************************************************************************
* FILE         : dev_gyro.c
*
* DESCRIPTION  : This file contains dev_gyro device driver                                 
*
** AUTHOR(s)   : (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*15.MAR.2013| Initial version 1.0 | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
* 06/26/2014| Version: 1.1        | Madhu Kiran Ramachandra (RBEI/ECF5)
*           |                     | Moved Config message receive section to open 
* -----------------------------------------------------------------------------
* 08/08/2014| Version: 1.2        | Madhu Kiran Ramachandra (RBEI/ECF5)
*           |                     | Fix for GMMY16-15647. bGyroThreadRunning updated
*           |                     | correctly.
* -----------------------------------------------------------------------------
* 11/12/2015| Version: 1.3        | Shivasharnappa Mothpalli (RBEI/ECF5)
*           |                     | Removed Unused IOCTRLS(Fix for  CFG3-1622):
            |                     | OSAL_C_S32_IOCTRL_GYRO_FLUSH,
            |                     | OSAL_C_S32_IOCTRL_GYRO_SET_TIMEOUT_VALUE,
            |                     | OSAL_C_S32_IOCTRL_GYRO_GET_TIMEOUT_VALUE,
            |                     | OSAL_C_S32_IOCTRL_GYRO_GETDIAGMODE,
            |                     | OSAL_C_S32_IOCTRL_GYRO_SETDIAGMODE,
            |                     | OSAL_C_S32_IOCTRL_GYRO_RESET,
            |                     | OSAL_C_S32_IOCTRL_GYRO_INIT,
            |                     | OSAL_C_S32_IOCTRL_GYRO_START,
            |                     | OSAL_C_S32_IOCTRL_GYRO_STOP.
            |                     | OSAL_C_S32_IOCTRL_GYRO_GETRESOLUTION,
            |                     | OSAL_C_S32_IOCTRL_GYRO_GET_3D_DIAG_RAW_DATA.
**********************************************************************************/

/*************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"

#include "dev_gyro.h"
#include "sensor_dispatcher.h"
#include "sensor_ring_buffer.h"
#include "hwinfo.h"

/************************************************************************
* Macro declaration (scope: Global)
*-----------------------------------------------------------------------*/
/*!
* return value for ioctrl call OSAL_C_S32_IOCTRL_VERSION
*/
#define DEV_GYRO_DRIVER_VERSION         0x0100  /* read v01.00 */


/*!
* Thread attribute related information
*/
#define GYRO_MAIN_THREAD_PRIORITY 49
#define GYRO_MAIN_THREAD_STACK_SIZE 4096
#define GYRO_C_STRING_MAIN_THREAD_NAME "GYRO_TASK"



/* Timeout for configuration */
#define GYRO_INIT_CONFIG_MSG_WAIT_TIME_MS ((OSAL_tMSecond)500)

/*!
* Gyro Selftest event name.
*/
#define GYRO_C_STRING_EVENT_SELFTEST "GYRO_SELFTEST"

/*!
* Gyro Thread Exit Event Name.
*/

#define GYRO_C_STRING_EVENT_THDEXT "GYRO_THREADEXITEVENT"

/*!
* macros for Event creation and deletion in self test
*/
#define EVENT_CREATE ((tU8)(0))
#define EVENT_DELETE ((tU8)(1))

/*!
* This bit is set in hGyroEventSelfTest if the gyro
* self test finish event occurs
*/
#define GYRO_C_EV_SELFTEST_FINISH      ((tU32)(0x01))
#define GYRO_C_EV_PATTERN              ((tU32)(0x01))

/*!
* Time out for Gyro self test 
*/
#define GYRO_SELFTEST_TIMEOUT   ((tU32)(2000))

/*!
* Gyro Self Test Status
*/
#define GYRO_SELF_TEST_PASSED       ((tU8)(0x01))
#define GYRO_SELF_TEST_FAILED       ((tU8)(0x00))

/*!
* Gyro Self Test result
*/
#define  GYRO_SELFTEST_RESULT_SUCCESS  ((tU8)(0xFF))

/*!
* Gyro Event Pattern for Thread Exit
*/
#define GYRO_C_EV_THD_EXT  ((tU32)(0x01))

/*!
* Time out for Gyro Thd Ext 
*/
#define GYRO_THDEXT_TIMEOUT   ((tU32)(2000))
/************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/

/*!
*the flag is set when config data is obtained
*/
static tBool bGyroConfigEn = FALSE;

/*!
*the flag is set when gyro temperature is obtained
*/
static tBool bGyroTempEn = FALSE;


/*!
* osal thread running flag, set on thread creation
*/
static tBool bGyroThreadRunning = FALSE;

/*!
* osal thread ID, returned on thread creation
*/
static OSAL_tThreadID s32GyroThreadID = OSAL_ERROR;

/*!
* Gyro config Data, set on receiving configuration data 
*/
trGyroConfigData rGyroConfigData= {0};

/*!
* Gyro config Data, set on receiving configuration data 
*/
tU16 u16GyroTemperature = 0;

/*!
* handle for Message Queue 
*/
static OSAL_tMQueueHandle hldGyroMsgQueue = 0;

/*Gyro Open Flag*/
static tBool bisGyoropen= FALSE;

/*!
* self test result
*/
static tU8 u8GyroSelfTestResult = GYRO_SELF_TEST_FAILED;

/*!
* Self test OSAL event handle
*/
static OSAL_tEventHandle hGyroEventSelfTest = OSAL_C_INVALID_HANDLE;

/*!
* Gyro Thread Exit Event Handle.
*/
static OSAL_tEventHandle hGyroEventThdExt = OSAL_C_INVALID_HANDLE;

tenGyroModel nGyroModel = GYRO_INVALID;


/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
static tVoid vGyroMainThread(tVoid);
static tVoid DEV_GYRO_vTraceOut(  TR_tenTraceLevel u32Level,const tChar *pcFormatString,... );
static tS32 GYRO_S32HandleEvent(tU8 u8Task);
static tS32 GYRO_s32ExecuteSelfTest(tS32 *ps32Argument);
static tS32 GYRO_s32TgrTdExtAndClrMsgQ(tVoid);

/********************************************************************************
* FUNCTION        : GYRO_s32IOOpen 

* PARAMETER       : NONE
*                                                      
* RETURNVALUE     : OSAL_E_NOERROR  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : Opens the gyro device                
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*15.MAR.2013| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
* 06/26/2014| Version 1.1         | Madhu Kiran Ramachandra (RBEI/ECF5)
*           |                     | Moved Config message receive section to open 
* -----------------------------------------------------------------------------
**********************************************************************************/
tS32 GYRO_s32IOOpen(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;
   tU8 pu8Buffer[GYRO_MSG_QUE_LENGTH]={ 0 };

   if(bisGyoropen==FALSE)
   {
      DEV_GYRO_vTraceOut(TR_LEVEL_USER_4, "GYRO_s32IOOpen true" );
      /*Create and Spawn thread to  wait on message on message queue*/
      OSAL_trThreadAttribute rAttr;
      rAttr.szName = ( tString )GYRO_C_STRING_MAIN_THREAD_NAME;
      rAttr.u32Priority = GYRO_MAIN_THREAD_PRIORITY;
      rAttr.s32StackSize = GYRO_MAIN_THREAD_STACK_SIZE;
      rAttr.pfEntry = ( OSAL_tpfThreadEntry )vGyroMainThread;
      rAttr.pvArg = OSAL_NULL;

      //sensor Dispatcher initialization 
      if(OSAL_OK != s32SensorDiapatcherInit(SEN_DISP_GYRO_ID)) /* init dispatcher */
      {
         DEV_GYRO_vTraceOut( TR_LEVEL_FATAL , "GYRO_s32IOOpen: s32SensorDiapatcherInit Failed");
      }
      //sensor_ring_buffer open
      else if(OSAL_OK != SenRingBuff_s32Open(SEN_DISP_GYRO_ID ))  /* open ring buffer for handle */
      {
         DEV_GYRO_vTraceOut( TR_LEVEL_FATAL , "SenRingBuff_s32Open Failed");
         (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_GYRO_ID);
      }
      /*open the message queue */
      else if(OSAL_OK != (OSAL_s32MessageQueueOpen((tCString)SEN_DISP_TO_GYRO_MSGQUE_NAME,
                  (OSAL_tenAccess)OSAL_EN_READWRITE,
                  &hldGyroMsgQueue)))
      {
         DEV_GYRO_vTraceOut(TR_LEVEL_FATAL ,"OSAL_s32MessageQueueOpen failed" );
         (tVoid)SenRingBuff_s32Close(SEN_DISP_GYRO_ID );
         (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_GYRO_ID);
      }
      else
      {
         // Wait for config message only in first open
         if ( FALSE == bGyroConfigEn )
         {
            if ( (tS32)sizeof(trGyroConfigData) != OSAL_s32MessageQueueWait(
                     hldGyroMsgQueue,
                     pu8Buffer,
                     GYRO_MSG_QUE_LENGTH,
                     OSAL_NULL,
                     GYRO_INIT_CONFIG_MSG_WAIT_TIME_MS) )
            {
               DEV_GYRO_vTraceOut( TR_LEVEL_FATAL, "Init:MQ wait fail err %lu", OSAL_u32ErrorCode());
               (tVoid)OSAL_s32MessageQueueClose( hldGyroMsgQueue);
               (tVoid)SenRingBuff_s32Close(SEN_DISP_GYRO_ID );
               (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_GYRO_ID);
            }
            else if ( SEN_DISP_MSG_TYPE_CONFIG != pu8Buffer[0] )
            {
               DEV_GYRO_vTraceOut( TR_LEVEL_FATAL, "In-correct config Msg. ID %d", (tS32)pu8Buffer[0] );
               (tVoid)OSAL_s32MessageQueueClose( hldGyroMsgQueue);
               (tVoid)SenRingBuff_s32Close(SEN_DISP_GYRO_ID );
               (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_GYRO_ID);
            }
            else
            {
               rGyroConfigData.u8GyroType = ((trGyroConfigData *)pu8Buffer)->u8GyroType;
               if(rGyroConfigData.u8GyroType == ( tU8 )1 )
               {
                  nGyroModel = GYRO_BST_BMI055;
               }
               else
               {
                  nGyroModel = GYRO_INVALID;
               }
               bGyroConfigEn = TRUE;
               rGyroConfigData.u16GyroDataInterval=((trGyroConfigData *)pu8Buffer)->u16GyroDataInterval;
               DEV_GYRO_vTraceOut( TR_LEVEL_COMPONENT, "Init:GyroType %d Gyro Int %d", 
               rGyroConfigData.u8GyroType,
               rGyroConfigData.u16GyroDataInterval );
            }
         }
         if( TRUE == bGyroConfigEn )
         {
            if( OSAL_s32EventCreate ( (tString) GYRO_C_STRING_EVENT_THDEXT, &hGyroEventThdExt ) != OSAL_OK )
            {
               DEV_GYRO_vTraceOut( TR_LEVEL_FATAL,
               "GYRO_s32IOOpen: Event Create failed with error 0x%x",
               OSAL_u32ErrorCode());
               (tVoid)OSAL_s32MessageQueueClose( hldGyroMsgQueue);
               (tVoid)SenRingBuff_s32Close(SEN_DISP_GYRO_ID );
               (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_GYRO_ID);
            }
            else
            {
               bGyroThreadRunning = TRUE;
               s32GyroThreadID = OSAL_ThreadSpawn(&rAttr);
               if( s32GyroThreadID == OSAL_ERROR)
               {
                  DEV_GYRO_vTraceOut( TR_LEVEL_FATAL ,"GYRO_s32IOOpen OSAL_ThreadSpawn Fail error : 0x%x", OSAL_u32ErrorCode());
                  (tVoid)OSAL_s32MessageQueueClose( hldGyroMsgQueue );
                  (tVoid)OSAL_s32EventClose( hGyroEventThdExt );
                  (tVoid)OSAL_s32EventDelete( (tString)GYRO_C_STRING_EVENT_THDEXT );
                  (tVoid)SenRingBuff_s32Close( SEN_DISP_GYRO_ID );
                  (tVoid)s32SensorDiapatcherDeInit( SEN_DISP_GYRO_ID );
                  bGyroThreadRunning = FALSE;
               }
               else
               {
                  DEV_GYRO_vTraceOut(TR_LEVEL_COMPONENT, "GYRO init success" );
                  bisGyoropen = TRUE;
                  s32RetVal = OSAL_E_NOERROR;
               }
            } 
         }
      }
   }
   else
   {
      DEV_GYRO_vTraceOut(TR_LEVEL_FATAL, "GYRO_s32IOOpen bisGyoropen false" );
   }        

   DEV_GYRO_vTraceOut(TR_LEVEL_USER_4,"GYRO_s32IOOpen Exit" );
   return s32RetVal;
}

/********************************************************************************
* FUNCTION        : GYRO_s32IOClose 

* PARAMETER       : NONE
*                                                      
* RETURNVALUE     : OSAL_E_NOERROR  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : Close the gyro device           
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*15.MAR.2013| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
tS32 GYRO_s32IOClose(tVoid)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   
   /*used to stop thread in conjunction with the event post below*/
   if(bisGyoropen==TRUE)
   {
      /* close Ring Buffer */
      if( OSAL_OK != SenRingBuff_s32Close(SEN_DISP_GYRO_ID  ))
      {
         /* error, Message Queue close failed */
         s32RetVal = OSAL_ERROR;
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR ,"GYRO_s32IOClose :senRingBuff close failed" );
      }
      /* dispatcher deinitailization */
      if(OSAL_OK != s32SensorDiapatcherDeInit( SEN_DISP_GYRO_ID ))
      {
         s32RetVal = OSAL_ERROR;
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,"GYRO_s32IOClose :sensor dispatcher deInit failed" );
      }
      
      bGyroThreadRunning = FALSE;
      
      if( OSAL_OK != GYRO_s32TgrTdExtAndClrMsgQ() )
      {
         s32RetVal = OSAL_ERROR;
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR, "GYRO_s32IOClose:Thread Exit Failed");
      }

      bisGyoropen=FALSE;    
      
   }
   else
   {
      DEV_GYRO_vTraceOut( TR_LEVEL_ERROR, "GYRO_s32IOClose:Driver already closed or not opened");
      s32RetVal=OSAL_ERROR;
   }
   
   DEV_GYRO_vTraceOut(TR_LEVEL_USER_4,"GYRO_s32IOClose Exit");
   return(s32RetVal);
}

/********************************************************************************
* FUNCTION        : GYRO_s32TgrTdExtAndClrMsgQ 

* PARAMETER       : tVoid
*                                                      
* RETURNVALUE     : OSAL_OK  on success
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : call to handle thread exit event and msg queue clear               
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19.AUG.2015| Initialversion 1.0  | Srinivas Prakash Anvekar (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static tS32 GYRO_s32TgrTdExtAndClrMsgQ(tVoid)
{
   tS32 s32RetVal = OSAL_OK;
   tU32 u32Messages;
   OSAL_tEventMask u32EveThdExtRes = 0;
   tU8 pu8Buffer[GYRO_MSG_QUE_LENGTH]={ 0 };
   tU8 myTerminateMsg = 0;
   
   if ( OSAL_ERROR == OSAL_s32MessageQueuePost( hldGyroMsgQueue,
                                                (tCU8*)&myTerminateMsg,
                                                sizeof(tU8), 
                                                OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST) ) 
   {
      s32RetVal = OSAL_ERROR;
      DEV_GYRO_vTraceOut(  TR_LEVEL_ERROR , 
                           "GYRO_s32TgrTdExtAndClrMsgQ :MessageQueue Post Failed %lu",
                           OSAL_u32ErrorCode() );
   }
   
   if( OSAL_s32EventWait(  hGyroEventThdExt,
                           GYRO_C_EV_THD_EXT,
                           OSAL_EN_EVENTMASK_OR,
                           GYRO_THDEXT_TIMEOUT,
                           &u32EveThdExtRes ) != OSAL_OK ) 
   {
      DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,
                         "GYRO_s32TgrTdExtAndClrMsgQ :Event Wait Failed with error 0x%x ", OSAL_u32ErrorCode() );
      s32RetVal = OSAL_ERROR;
   }
   /*clear the thread exit event*/
   else if(u32EveThdExtRes & GYRO_C_EV_THD_EXT)
   {
      if( OSAL_OK != (  OSAL_s32EventPost( hGyroEventThdExt,
                        ~GYRO_C_EV_THD_EXT,
                        OSAL_EN_EVENTMASK_AND ) ) )
      {
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,
         "GYRO_s32TgrTdExtAndClrMsgQ: Event post failed with error 0x%x",
         OSAL_u32ErrorCode());
         s32RetVal = OSAL_ERROR;
      }
      /*
       Main thread doesn't exit immediately once it post the event.
       Just give some time for the thread to exit.
      */
      OSAL_s32ThreadWait(100);
   }
   
   /*
     Check number of messages left in the Queue and if messages exist then
     clear all the messages by reading. Since if any one else opens 
     dev_gyro again.These invalid messages will be read out.
   */
   if( OSAL_ERROR == OSAL_s32MessageQueueStatus
                     ( hldGyroMsgQueue, OSAL_NULL, OSAL_NULL, &u32Messages ) )
   {
      s32RetVal = OSAL_ERROR;
      DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,"GYRO_s32TgrTdExtAndClrMsgQ :MsgQue Status returned failed" );
   }
   else
   {
      while( 0 != u32Messages )
      {
         if( OSAL_ERROR == OSAL_s32MessageQueueWait( hldGyroMsgQueue, (tPU8)pu8Buffer, sizeof(pu8Buffer),
                                                   OSAL_NULL, (OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING ) )
         {
            DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,"GYRO_s32TgrTdExtAndClrMsgQ :MsgQue Wait Failed" );
            s32RetVal = OSAL_ERROR;
         }
         u32Messages--;
      }
   }
   
   if( OSAL_OK != ( OSAL_s32MessageQueueClose( hldGyroMsgQueue )))
   {
      /* error, Message Queue close failed */
      s32RetVal = OSAL_ERROR;
      DEV_GYRO_vTraceOut( TR_LEVEL_ERROR ,"GYRO_s32TgrTdExtAndClrMsgQ :close message Queue failed" );
   }
   
   
   /*close the event*/
   if( OSAL_s32EventClose( hGyroEventThdExt ) != OSAL_OK )
   {
   
      s32RetVal = OSAL_ERROR;
      DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,
      "GYRO_s32TgrTdExtAndClrMsgQ: Event close failed with error 0x%x",
      OSAL_u32ErrorCode()); 
      
   }
   
   /*delete the event*/
   if( OSAL_s32EventDelete((tString)GYRO_C_STRING_EVENT_THDEXT) != OSAL_OK)
   {
   
      s32RetVal = OSAL_ERROR;
      DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,
      "GYRO_s32TgrTdExtAndClrMsgQ: Event Delete failed with error 0x%x",
      OSAL_u32ErrorCode());
      
   }
   
   return s32RetVal;
}
/********************************************************************************
* FUNCTION        : GYRO_S32HandleEvent 

* PARAMETER       : tU8
*                                                      
* RETURNVALUE     : OSAL_OK  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : call to handle gyro self test event               
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19.MAY.2014| Initialversion 1.0  | Sai Chowdary Samineni (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static tS32 GYRO_S32HandleEvent(tU8 u8Task)
{
   tS32 s32RetVal  = OSAL_OK;

   if(u8Task == EVENT_CREATE)
   {
      if( OSAL_s32EventCreate((tString)GYRO_C_STRING_EVENT_SELFTEST,&hGyroEventSelfTest) != OSAL_OK)
      {
         s32RetVal = OSAL_ERROR;
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,
         "GYRO_S32HandleEvent: Event Create failed with error 0x%x",
         OSAL_u32ErrorCode());
      }
   }
   else if(u8Task == EVENT_DELETE)
   {
      if( OSAL_s32EventClose(hGyroEventSelfTest) != OSAL_OK )
      {
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,
         "GYRO_S32HandleEvent: Event close failed with error 0x%x",
         OSAL_u32ErrorCode()); 
      }
      if( OSAL_s32EventDelete((tString)GYRO_C_STRING_EVENT_SELFTEST) != OSAL_OK)
      {
         s32RetVal = OSAL_ERROR;
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,
         "GYRO_S32HandleEvent: Event Delete failed with error 0x%x",
         OSAL_u32ErrorCode());
      }
   }
   else
   {
      //Nothing
   }
   
   return s32RetVal;
}

/********************************************************************************
* FUNCTION        : GYRO_s32ExecuteSelfTest 

* PARAMETER       : Void
*                                                      
* RETURNVALUE     : OSAL_OK  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : call to execute gyro self test                
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19.MAY.2014| Initialversion 1.0  | Sai Chowdary Samineni (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static tS32 GYRO_s32ExecuteSelfTest(tS32 *ps32Argument)
{
   OSAL_tEventMask u32EventResult = 0;

   //Start gyro self test
   tS32 s32RetVal = SenDisp_s32TriggerGyroSelfTest();
   
   if(s32RetVal != OSAL_ERROR)
   {
      if( OSAL_s32EventWait(  hGyroEventSelfTest,
               GYRO_C_EV_PATTERN,
               OSAL_EN_EVENTMASK_OR,
               GYRO_SELFTEST_TIMEOUT,
               &u32EventResult) == OSAL_OK ) 
      {
         if(u32EventResult & GYRO_C_EV_SELFTEST_FINISH)
         {
            OSAL_s32EventPost( hGyroEventSelfTest,
            ~GYRO_C_EV_SELFTEST_FINISH,
            OSAL_EN_EVENTMASK_AND );
            *ps32Argument = (tS32)u8GyroSelfTestResult;
            s32RetVal = OSAL_E_NOERROR; 
         }
      }
      else
      {
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR,
         "GYRO_s32ExecuteSelfTest: Event Wait Failed with error %d",
         OSAL_u32ErrorCode());
         s32RetVal = OSAL_ERROR;
      }
   }

   return s32RetVal;
}
/********************************************************************************
* FUNCTION        : GYRO_s32IOControl 

* PARAMETER       : s32fun :   Function identificator
*                   s32arg :   Argument to be passed to function

* RETURNVALUE     : OSAL_E_NOERROR  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : call a gyro device driver control function
*
* HISTORY         :
*------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|--------------------------------
* 15.MAR.2013  |  Initial version:1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
* 12.MAY.2014  |  version:1.1          | Sai Chowdary Samineni (RBEI/ECF5)
*                                        Added Self test IO Control
*--------------|-----------------------|-------------------------------------
* 11.Dec.2015  |  version:1.2          | Shivasharnappa Mothpalli (RBEI/ECF5)
*              |                       | Removed Unused IOCTRLS (Fix for  CFG3-1622) :
               |                       | OSAL_C_S32_IOCTRL_GYRO_FLUSH,
               |                       | OSAL_C_S32_IOCTRL_GYRO_SET_TIMEOUT_VALUE,
               |                       | OSAL_C_S32_IOCTRL_GYRO_GET_TIMEOUT_VALUE,
               |                       | OSAL_C_S32_IOCTRL_GYRO_GETDIAGMODE,
               |                       | OSAL_C_S32_IOCTRL_GYRO_SETDIAGMODE,
               |                       | OSAL_C_S32_IOCTRL_GYRO_RESET,
               |                       | OSAL_C_S32_IOCTRL_GYRO_INIT,
               |                       | OSAL_C_S32_IOCTRL_GYRO_START,
               |                       | OSAL_C_S32_IOCTRL_GYRO_STOP.
               |                       | OSAL_C_S32_IOCTRL_GYRO_GETRESOLUTION,
               |                       | OSAL_C_S32_IOCTRL_GYRO_GET_3D_DIAG_RAW_DATA.
**********************************************************************************/

tS32 GYRO_s32IOControl(tS32 s32fun, tLong sArg)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tPS32 ps32Argument =  (tPS32) sArg;
   tS32 s32ErrChk = OSAL_ERROR;

   DEV_GYRO_vTraceOut(TR_LEVEL_USER_4,"GYRO_s32IOControl: IOControl ENTERED");

   switch( s32fun)
   {
   case OSAL_C_S32_IOCTRL_VERSION:
      {
         /* check input parameter */
         if( ps32Argument != OSAL_NULL )
         {
            /*return internal version number*/
            *ps32Argument = (tS32)DEV_GYRO_DRIVER_VERSION;
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

   case OSAL_C_S32_IOCTRL_GYRO_GETCNT:
      {
         /* check input parameter */
         if(ps32Argument != OSAL_NULL)
         {
            s32ErrChk = SenRingBuff_s32GetAvailRecords( SEN_DISP_GYRO_ID);
            if(s32ErrChk != OSAL_ERROR)
            {
               *ps32Argument =s32ErrChk;
            }
            else
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
            
            DEV_GYRO_vTraceOut(TR_LEVEL_USER_4,"SenRingBuff_s32GetAvailRecords ret : %d", s32ErrChk );
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

   case OSAL_C_S32_IOCTRL_GYRO_GET_TYPE://model-sensor type
      {
         if(bGyroConfigEn)
         {
            /* check input parameter */
            if( ps32Argument != OSAL_NULL )
            {
               *ps32Argument =(tS32)rGyroConfigData.u8GyroType; 
            }
            else
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
         }
         else
         {
            s32RetVal =OSAL_E_INPROGRESS;
         }
         break;
      }

   case OSAL_C_S32_IOCTRL_GYRO_GETCYCLETIME:
      {
         if(bGyroConfigEn)
         {
            /* check input parameter */
            if( ps32Argument != OSAL_NULL )
            {
               *ps32Argument = (tS32)((rGyroConfigData.u16GyroDataInterval )*1000000); 
            }
            else
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
         }
         else
         {
            s32RetVal =OSAL_E_INPROGRESS;
         }
         //s32RetVal = OSAL_E_NOTSUPPORTED;
         break;
      }

   case OSAL_C_S32_IOCTRL_GYRO_GET_TEMP:
      {
         if(bGyroTempEn)
         {
            *ps32Argument = (tS32) u16GyroTemperature; //populate u32GyroTemperature from dispatcher message queue
            DEV_GYRO_vTraceOut( TR_LEVEL_USER_4,"u16GyroTemperature = %d ",u16GyroTemperature);
         }
         else
         {
            s32RetVal =OSAL_E_INPROGRESS;
         }
         break;
      }
      
   case OSAL_C_S32_IOCTRL_GYRO_GET_HW_INFO  :
      {
         if (OSAL_NULL != ps32Argument )
         {
            if ( FALSE == SenHwInfo_bGetHwinfo( GYRO_ID ,
                     (OSAL_trIOCtrlHwInfo *)(tPVoid) ps32Argument))
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
         }
         break;
      }
   case OSAL_C_S32_IOCTRL_GYRO_SELF_TEST:  
      {
         if( ps32Argument != OSAL_NULL )
         {
            if( GYRO_S32HandleEvent(EVENT_CREATE) == OSAL_OK)
            {
               s32RetVal = GYRO_s32ExecuteSelfTest(ps32Argument);
               GYRO_S32HandleEvent(EVENT_DELETE);
            }
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }
   default:
      {
         /* in case s32fun is neither of allowed above */
         s32RetVal = OSAL_E_NOTSUPPORTED;
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR, "GYRO_s32IOControl: IOControl Not supported");
         break;
      }
   }

   DEV_GYRO_vTraceOut( TR_LEVEL_USER_4, " GYRO_s32IOControl: IOControl EXIT");
   return(s32RetVal);
}

/********************************************************************************
* FUNCTION        : GYRO_s32IORead 

* PARAMETER       : ps8Buffer : pointer to struct @ref OSAL_trIOCtrl3dGyroData
*                               where the read data must be copied
*                   u32maxbytes : max number of bytes for read buffer
*                                                      
* RETURNVALUE     : Number of bytes read on sucess
*                               OSAL_ERROR on Failure
*
* DESCRIPTION     : Reads gyro data           
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*15.MAR.2013| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
tS32 GYRO_s32IORead(tPS8 ps8Buffer, tU32 u32maxbytes)
{
   tU32  u32NumberOfEntries = u32maxbytes / sizeof( OSAL_trIOCtrl3dGyroData );
   tS32  s32RetVal = OSAL_E_NOERROR;
   tS32 s32ErrChk = 0;
   OSAL_trIOCtrl3dGyroData *prIOGyroData =  (OSAL_trIOCtrl3dGyroData *)(ps8Buffer);
   
   
   DEV_GYRO_vTraceOut( TR_LEVEL_USER_4, "GYRO_s32IORead Entry : u32NumberOfEntries %d", u32NumberOfEntries);
   
   if(prIOGyroData == OSAL_NULL)
   {
      DEV_GYRO_vTraceOut(TR_LEVEL_FATAL,"GYRO_s32IORead :prIOGyroData == NULL");
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   else
   {
      s32ErrChk = SenRingBuff_s32Read(SEN_DISP_GYRO_ID , ( tPVoid) prIOGyroData, u32NumberOfEntries );
      if(OSAL_ERROR == s32ErrChk)
      {
         s32RetVal = OSAL_ERROR;
         DEV_GYRO_vTraceOut(TR_LEVEL_ERROR ,"GYRO_s32IORead :Read from ring buffer failed with error code =%d", OSAL_u32ErrorCode()); 
#ifdef OSAL_GEN3_SIM
         OSAL_s32ThreadWait(1000);
#endif
      }
      else if((tS32)OSAL_E_TIMEOUT == s32ErrChk)
      {
         s32RetVal = OSAL_E_TIMEOUT;
         DEV_GYRO_vTraceOut(TR_LEVEL_ERROR ,"GYRO_s32IORead :Read from ring buffer failed due to timeout with error code =%d", OSAL_u32ErrorCode()); 
      }
      else
      {
         DEV_GYRO_vTraceOut(TR_LEVEL_USER_4,"GYRO_s32IORead: Block-1 u32TimeStamp=%d   u16ErrorCounter:%d   u16Gyro_r=%d   u16Gyro_s=%d   u16Gyro_t=%d",
                            prIOGyroData->u32TimeStamp, prIOGyroData->u16ErrorCounter,
                            prIOGyroData->u16Gyro_r, prIOGyroData->u16Gyro_s, 
                            prIOGyroData->u16Gyro_t);
      }
      
   }
   if(s32RetVal == (tS32)OSAL_E_NOERROR)
   {
      s32RetVal = (s32ErrChk * (tS32)sizeof(OSAL_trIOCtrl3dGyroData));
   }
   return(s32RetVal);    /*return no. of bytes written into Readbuffer*/

} /*NEW END*/


/********************************************************************************
* FUNCTION        : vGyroMainThread 

* PARAMETER       : NONE
*                                                      
* RETURNVALUE     : void 
*
* DESCRIPTION     :  Osal thread function started in @ref GYRO_s32CreateGyroDevice
*                    used to wait for configuration, Temperature and error messages 
*                    sent by the dispatcher via message queue
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*15.MAR.2013| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.MAY.2014|                     | Sai Chowdary Samineni (RBEI/ECF5)
*           |                     | Modified to include Self test Result
* -----------------------------------------------------------------------
* 06/26/2014| Version 1.1         | Madhu Kiran Ramachandra (RBEI/ECF5)
*           |                     | Moved Config message receive section to open 
* -----------------------------------------------------------------------------
**********************************************************************************/

static tVoid vGyroMainThread( tVoid)
{
   tU8 pu8Buffer[GYRO_MSG_QUE_LENGTH]={ 0 }; 

   while( bGyroThreadRunning == TRUE ) //to continuously wait for the message on the message queue
   {
      if( 0 == (OSAL_s32MessageQueueWait( hldGyroMsgQueue, (tPU8)pu8Buffer, sizeof(pu8Buffer),
                  OSAL_NULL, (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER)))
      {
         DEV_GYRO_vTraceOut( TR_LEVEL_ERROR , "Open message Queue wait failed with error code =%d", OSAL_u32ErrorCode());
      }
      else
      {
         if( bGyroThreadRunning == FALSE )
         { 
            continue;
         }
         else
         {
            switch( pu8Buffer[0] )
            {
            case SEN_DISP_MSG_TYPE_CONFIG: //When message in the message queue is of type Configuration Message
               {
                  DEV_GYRO_vTraceOut( TR_LEVEL_FATAL, "Run time config update not expected" );

                  /*          Moved Config message receive section to open call
                        rGyroConfigData.u8GyroType = ((trGyroConfigData *)pu8Buffer)->u8GyroType;
                        if(rGyroConfigData.u8GyroType == ( tU8 )1 )
                        {
                           nGyroModel = GYRO_BST_BMI055;
                        }
                        else
                        {
                           nGyroModel = GYRO_INVALID;
                        }
                        bGyroConfigEn = TRUE;
                        rGyroConfigData.u16GyroDataInterval=((trGyroConfigData *)pu8Buffer)->u16GyroDataInterval;
                        DEV_GYRO_vTraceOut( TR_LEVEL_USER_4, "GyroType =%d Gyro Int %d", 
                                                            rGyroConfigData.u8GyroType,
                                                            rGyroConfigData.u16GyroDataInterval );
*/
                  break;
               }
            case SEN_DISP_MSG_TYPE_TEMP_UPDATE: //When message in the message queue is of type Temperature Message
               {
                  u16GyroTemperature =((trGyroTemp *)pu8Buffer)->u16GyroTemp;
                  bGyroTempEn=TRUE;
                  DEV_GYRO_vTraceOut( TR_LEVEL_USER_4, " Gyro Temp update = %d", u16GyroTemperature );
                  break;
               }
            case SEN_DISP_MSG_TYPE_SELF_TEST_RESULT:   
               {
                  if(((SensorSelfTestResult *)pu8Buffer)->u8SelfTestResult == GYRO_SELFTEST_RESULT_SUCCESS )
                  {
                     u8GyroSelfTestResult = GYRO_SELF_TEST_PASSED;
                  }
                  else
                  {
                     u8GyroSelfTestResult = GYRO_SELF_TEST_FAILED;
                  }
                  DEV_GYRO_vTraceOut( TR_LEVEL_USER_4,
                  "vGyroMainThread: Gyro self test result is %d",
                  ((SensorSelfTestResult *)pu8Buffer)->u8SelfTestResult);
                  OSAL_s32EventPost( hGyroEventSelfTest,
                  GYRO_C_EV_SELFTEST_FINISH,
                  OSAL_EN_EVENTMASK_OR );
                  break;
               }
            default:
               {
                  DEV_GYRO_vTraceOut( TR_LEVEL_FATAL,"vGyroMainThread: Unknown Msg Type");
                  break;
               }
            }
         }
      }
      
   }
   
   if( OSAL_OK != ( OSAL_s32EventPost( hGyroEventThdExt, 
               GYRO_C_EV_THD_EXT,
               OSAL_EN_EVENTMASK_OR ) ) )
   {
      DEV_GYRO_vTraceOut( TR_LEVEL_ERROR ,"GYRO_s32IOClose :Event Post Failed with error"
      "0x%x ", OSAL_u32ErrorCode() );
   }
   
   
   OSAL_vThreadExit();
}
/********************************************************************************
* FUNCTION          : DEV_GYRO_vTraceOut

* PARAMETER         : u32Level - trace level
                     pcFormatString - Trace string     

* RETURNVALUE       : tU32 error codes

* DESCRIPTION       : Trace out function for GYRO.

* HISTORY           :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*15.MAR.2013| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static tVoid DEV_GYRO_vTraceOut(  TR_tenTraceLevel u32Level,const tChar *pcFormatString,... )
{
   if(LLD_bIsTraceActive((tU32)OSAL_C_TR_CLASS_DEV_GYRO, (tU32)u32Level) != FALSE)
   {
      /*
      Parameter to hold the argument for a function, specified the format
      string in pcFormatString
      defined as:
      typedef char* va_list in stdarg.h
      */
      va_list argList;
      /*
      vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure
      */
      tS32 s32Size;
      /*
      Buffer to hold the string to trace out
      */
      tS8 u8Buffer[MAX_TRACE_SIZE];
      /*
      Position in buffer from where the format string is to be
      concatenated
      */
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /*
      Flush the String
      */
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /*
      Copy the String to indicate the trace is from the RTC device
      */

      /*
      Initialize the argList pointer to the beginning of the variable
      arguement list
      */
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
      Collect the format String's content into the remaining part of
      the Buffer
      */
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,
                  sizeof(u8Buffer),
                  pcFormatString,
                  argList ) ) )
      {
         return;
      }

      /*
      Trace out the Message to TTFis
      */
      LLD_vTrace((tU32)OSAL_C_TR_CLASS_DEV_GYRO,
      (tU32)u32Level,
      u8Buffer,
      (tU32)s32Size );   /* Send string to Trace*/
      /*
      Performs the appropiate actions to facilitate a normal return by a
      function that has used the va_list object
      */
      va_end(argList);
   }
}

#ifdef LOAD_SENSOR_SO
tS32 gyro_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
   (tVoid)s32Id;
   (tVoid)szName;
   (tVoid)enAccess;
   (tVoid)pu32FD;
   (tVoid)app_id;

   return GYRO_s32IOOpen();

}

tS32 gyro_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   (tVoid)s32ID;
   (tVoid)u32FD;

   return GYRO_s32IOClose(); 

}

tS32 gyro_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tLong sArg)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   return GYRO_s32IOControl(s32fun, sArg); 
}

tS32 gyro_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   (tVoid)ret_size;
   return GYRO_s32IORead(pBuffer, u32Size);
}
#endif

