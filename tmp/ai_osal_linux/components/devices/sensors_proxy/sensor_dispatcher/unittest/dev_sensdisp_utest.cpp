#define SENSPXYMOCKSTOBEREMOVED
#include "Sensor_InternalMock.h"
#include "Sensor_InternalMock.cpp"
//#include "dev_gyro.h"

#undef s32SensorDiapatcherInit
#undef s32SensorDiapatcherDeInit
#undef SenDisp_s32TriggerGyroSelfTest
#undef SenDisp_s32TriggerAccSelfTest

#include "../source/sensor_dispatcher.c"

using namespace testing;
using namespace std;

int main(int argc, char** argv)
{
   ::testing::InitGoogleMock(&argc, argv);
   return RUN_ALL_TESTS();
}

class Dispatcher_InitGlobals : public ::testing::Test
{
public:
   SensorMock PoS;
   NiceMock<OsalMock> Osal_mock;
   virtual void SetUp()
   {
      rSensorDispInfo.bShutdownFlag = TRUE;
      rSensorDispInfo.bCreateFlag = FALSE;
   }
};

class Dispatcher_Initialized : public Dispatcher_InitGlobals
{
public:
   virtual void SetUp()
   {
      rSensorDispInfo.rOdoInfo.bIsFirstRecord = TRUE;
      rSensorDispInfo.rGyroInfo.bIsFirstRecord = TRUE;
      rSensorDispInfo.rAccInfo.bIsFirstRecord = TRUE;
      rSensorDispInfo.rAbsInfo.bIsFirstRecord = TRUE;
      rSensorDispInfo.bShutdownFlag = FALSE;
      rSensorDispInfo.bCreateFlag = TRUE;
      rSensorDispInfo.u8SccStatus = (tU8)SCC_SENSOR_COMPONENT_STATUS_ACTIVE;
      rSensorDispInfo.u8SccVerInfo = SEN_DISP_VERSION_INFO;
      rSensorDispInfo.rOdoInfo.enOdoSts = DEVICE_CLOSED;
      rSensorDispInfo.rGyroInfo.enGyroSts = DEVICE_CLOSED;
      rSensorDispInfo.rAccInfo.enAccSts = DEVICE_CLOSED;
      rSensorDispInfo.rAbsInfo.enAbsSts = DEVICE_CLOSED;
   }
};

class Dispatcher_Opened : public Dispatcher_Initialized
{
public:
   virtual void SetUp()
   {
      rSensorDispInfo.rOdoInfo.bIsFirstRecord = FALSE;
      rSensorDispInfo.rGyroInfo.bIsFirstRecord = FALSE;
      rSensorDispInfo.rAccInfo.bIsFirstRecord = FALSE;
      rSensorDispInfo.rAbsInfo.bIsFirstRecord = FALSE;
      rSensorDispInfo.rOdoInfo.enOdoSts = DEVICE_OPENED;
      rSensorDispInfo.rGyroInfo.enGyroSts = DEVICE_OPENED;
      rSensorDispInfo.rAccInfo.enAccSts = DEVICE_OPENED;
      rSensorDispInfo.rAbsInfo.enAbsSts = DEVICE_OPENED;
   }
};


TEST_F(Dispatcher_InitGlobals, Disp_StartThread)
{
   tS32 s32RetVal;
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(SEN_DISP_EVENT_INIT_SUCCESS),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(100));
   s32RetVal = SenDisp_s32StartThread();
   EXPECT_EQ(100,tid_SenDispSockRd);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Dispatcher_InitGlobals, Disp_SendStatusCmd_Complete)
{
   tS32 s32RetVal;
   tPVoid pvArg;
   tU8 u8Status = SCC_SENSOR_COMPONENT_STATUS_ACTIVE;
   EXPECT_CALL(PoS, dgram_send(_,_,_)).Times(1).WillOnce(Return(MSG_SIZE_SCC_SENSORS_C_COMPONENT_STATUS));
   s32RetVal = SenDisp_s32SendStatusCmd(u8Status);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Dispatcher_InitGlobals, Disp_SendStatusCmd_InComplete)
{
   tS32 s32RetVal;
   tPVoid pvArg;
   tU8 u8Status = SCC_SENSOR_COMPONENT_STATUS_NOT_ACTIVE;
   EXPECT_CALL(PoS, dgram_send(_,_,_)).Times(1).WillOnce(Return(MSG_SIZE_SCC_SENSORS_R_CONFIG));
   s32RetVal = SenDisp_s32SendStatusCmd(u8Status);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(Dispatcher_InitGlobals, Disp_HandleStatusMsg_Active)
{
   tS32 retval;
   rSensorDispInfo.u32RecvdMsgSize = MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_COMPONENT_STATUS] = SCC_SENSOR_COMPONENT_STATUS_ACTIVE;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_VERSION_STATUS] = SEN_DISP_VERSION_INFO;
   retval= s32HandleStatusMsg();
   EXPECT_EQ(SCC_SENSOR_COMPONENT_STATUS_ACTIVE, rSensorDispInfo.u8SccStatus);
   EXPECT_EQ(SEN_DISP_VERSION_INFO, rSensorDispInfo.u8SccVerInfo);
   EXPECT_EQ(OSAL_OK, retval);
}

TEST_F(Dispatcher_InitGlobals, Disp_HandleStatusMsg_NtAcive)
{
   tS32 retval;
   rSensorDispInfo.u32RecvdMsgSize = MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_COMPONENT_STATUS] = SCC_SENSOR_COMPONENT_STATUS_NOT_ACTIVE;
   retval =s32HandleStatusMsg();
   EXPECT_EQ(SCC_SENSOR_COMPONENT_STATUS_NOT_ACTIVE, rSensorDispInfo.u8SccStatus);
   EXPECT_EQ(OSAL_ERROR, retval);
}

TEST_F(Dispatcher_InitGlobals, Disp_HandleStatusMsg_InvalidSz)
{
   tS32 retval;
   rSensorDispInfo.u32RecvdMsgSize = 0;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_COMPONENT_STATUS] = 0;
   retval =s32HandleStatusMsg();
   EXPECT_EQ(SCC_SENSOR_COMPONENT_STATUS_NOT_ACTIVE, rSensorDispInfo.u8SccStatus);
   EXPECT_EQ(OSAL_ERROR, retval);
}

TEST_F(Dispatcher_InitGlobals, Disp_HandleStatusMsg_InvalidStatus)
{
   tS32 retval;
   rSensorDispInfo.u32RecvdMsgSize = MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_COMPONENT_STATUS] = 0;
   retval =s32HandleStatusMsg();
   EXPECT_EQ(SCC_SENSOR_COMPONENT_STATUS_NOT_ACTIVE, rSensorDispInfo.u8SccStatus);
   EXPECT_EQ(OSAL_ERROR, retval);
}

TEST_F(Dispatcher_InitGlobals, SenDisp_HandleSensorConfigMsg)
{
   tS32 s32RetVal;
   rSensorDispInfo.u32RecvdMsgSize = MSG_SIZE_SCC_SENSORS_R_CONFIG;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ODO_CONFIG_DATA_INTERVAL] = 100;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ABS_CONFIG_DATA_INTERVAL] = 100;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_GYRO_CONFIG_DATA_INTERVAL] = 50;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ACC_CONFIG_DATA_INTERVAL] = 50;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ACC_CONFIG_TYPE] = 0;
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_GYRO_CONFIG_TYPE] = 0;
   EXPECT_CALL(PoS, s32SenRingBuffInit(_,_)).WillOnce(Return(OSAL_OK))
                                            .WillOnce(Return(OSAL_OK))
                                            .WillOnce(Return(OSAL_OK))
                                            .WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).WillOnce(Return(OSAL_OK))
                                                       .WillOnce(Return(OSAL_OK))
                                                       .WillOnce(Return(OSAL_OK))
                                                       .WillOnce(Return(OSAL_OK));
   s32RetVal = s32HandleSensorConfigMsg();
   EXPECT_EQ(OSAL_OK, s32RetVal);
   EXPECT_EQ(DEVICE_EXISTS, rSensorDispInfo.rOdoInfo.enOdoSts);
   EXPECT_EQ(DEVICE_EXISTS, rSensorDispInfo.rGyroInfo.enGyroSts);
   EXPECT_EQ(DEVICE_EXISTS, rSensorDispInfo.rAccInfo.enAccSts);
   EXPECT_EQ(DEVICE_EXISTS, rSensorDispInfo.rAbsInfo.enAbsSts);
}

TEST_F(Dispatcher_InitGlobals, SensDisp_GetDataFromScc_Shutdown)
{
   tU32 u32Bytes = SEN_DISP_MAX_PACKET_SIZE;
   tS32 s32RetVal;
   EXPECT_CALL(PoS, poll(_,_,_)).Times(1).WillOnce(Return(1));
   s32RetVal = SensorProxy_s32GetDataFromScc(u32Bytes);
   EXPECT_EQ(-1, s32RetVal);
}

TEST_F(Dispatcher_Initialized, Disp_HandleGyroSelfTestResult)
{
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_SELF_TEST_INDEX] = SEN_DISP_SELF_TEST_RESULT_GYROMETER;
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   SenDisp_vHandleSensorSelfTestResult();
}

TEST_F(Dispatcher_Initialized, Disp_HandleAccSelfTestResult)
{
   rSensorDispInfo.u8RecvBuffer[SEN_DISP_SELF_TEST_INDEX] = SEN_DISP_SELF_TEST_RESULT_ACCELEROMETER;
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   SenDisp_vHandleSensorSelfTestResult();
}

TEST_F(Dispatcher_Initialized, Disp_GyroSelfTest)
{
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   SenDisp_vPostAccSelfTestResult();
}

TEST_F(Dispatcher_Initialized, Disp_AccSelfTest)
{
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   SenDisp_vPostGyroSelfTestResult();
}

TEST_F(Dispatcher_Initialized, SensDisp_GetDataFromScc_PollTimeout)
{
   tU32 u32Bytes = SEN_DISP_MAX_PACKET_SIZE;
   tS32 s32RetVal;
   rSensorDispInfo.bShutdownFlag = FALSE;
   EXPECT_CALL(PoS, poll(_,_,_)).Times(10).WillRepeatedly(Return(0));
   s32RetVal = SensorProxy_s32GetDataFromScc(u32Bytes);
   EXPECT_EQ(0, s32RetVal);
}

TEST_F(Dispatcher_Initialized, SensDisp_GetDataFromScc_DgramRecvErr)
{
   tU32 u32Bytes = SEN_DISP_MAX_PACKET_SIZE;
   tS32 s32RetVal;
   EXPECT_CALL(PoS, poll(_,_,_)).Times(1).WillOnce(Return(1));
   EXPECT_CALL(PoS, dgram_recv(_,_,_)).Times(1).WillOnce(Return(-1));
   s32RetVal = SensorProxy_s32GetDataFromScc(u32Bytes);
   EXPECT_EQ(-1, s32RetVal);
}


TEST_F(Dispatcher_Initialized, SensDisp_GetDataFromScc)
{
   tU32 u32Bytes = SEN_DISP_MAX_PACKET_SIZE;
   tS32 s32RetVal;
   EXPECT_CALL(PoS, poll(_,_,_)).Times(1).WillOnce(Return(1));
   EXPECT_CALL(PoS, dgram_recv(_,_,_)).Times(1).WillOnce(Return(SEN_DISP_MAX_PACKET_SIZE));
   s32RetVal = SensorProxy_s32GetDataFromScc(u32Bytes);
   EXPECT_EQ(SEN_DISP_MAX_PACKET_SIZE, s32RetVal);
}

TEST_F(Dispatcher_Initialized, SensDisp_DriverSuppOdo)
{
   tEnSenDrvID enSenID = SEN_DISP_ODO_ID;
   tS32 s32RetVal;
   EXPECT_CALL(Osal_mock, s32SemaphoreCreate(_,_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreWait(_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphorePost(_)).WillOnce(Return(OSAL_OK));
   s32RetVal = s32SensorDiapatcherInit(enSenID);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Dispatcher_Initialized, SensDisp_DriverSuppGyro)
{
   tEnSenDrvID enSenID = SEN_DISP_GYRO_ID;
   tS32 s32RetVal;
   EXPECT_CALL(Osal_mock, s32SemaphoreCreate(_,_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreWait(_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphorePost(_)).WillOnce(Return(OSAL_OK));
   s32RetVal = s32SensorDiapatcherInit(enSenID);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Dispatcher_Initialized, SensDisp_DriverSuppACC)
{
   tEnSenDrvID enSenID = SEN_DISP_ACC_ID;
   tS32 s32RetVal;
   EXPECT_CALL(Osal_mock, s32SemaphoreCreate(_,_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreWait(_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphorePost(_)).WillOnce(Return(OSAL_OK));
   s32RetVal = s32SensorDiapatcherInit(enSenID);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Dispatcher_Initialized, SensDisp_DriverSuppABS)
{
   tEnSenDrvID enSenID = SEN_DISP_ABS_ID;
   tS32 s32RetVal;
   EXPECT_CALL(Osal_mock, s32SemaphoreCreate(_,_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphoreWait(_,_)).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphorePost(_)).WillOnce(Return(OSAL_OK));
   s32RetVal = s32SensorDiapatcherInit(enSenID);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Dispatcher_Opened, Disp_ValidateAccRecord_InvalidCounter)
{
   OSAL_trIOCtrlAccData rAccCurData =  {1000,1024,10005,
                                             1024, 0};
   SenDisp_vValidateAccRecord(&rAccCurData);
   EXPECT_EQ(FALSE, rSensorDispInfo.rAccInfo.bIsFirstRecord);
   EXPECT_EQ(1000, rSensorDispInfo.rAccInfo.rAccPrevData.u32TimeStamp);
   EXPECT_EQ(1024, rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_x);
   EXPECT_EQ(10005, rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_y);
   EXPECT_EQ(1024, rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_z);
   EXPECT_EQ(1, rAccCurData.u16ErrorCounter);
}

TEST_F(Dispatcher_Opened, Disp_ValidateAccRecord)
{
   OSAL_trIOCtrlAccData rAccCurData =  {1000,1024,1024,
                                             1024, 0};
   SenDisp_vValidateAccRecord(&rAccCurData);
   EXPECT_EQ(FALSE, rSensorDispInfo.rAccInfo.bIsFirstRecord);
   EXPECT_EQ(1000, rSensorDispInfo.rAccInfo.rAccPrevData.u32TimeStamp);
   EXPECT_EQ(1024, rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_x);
   EXPECT_EQ(1024, rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_y);
   EXPECT_EQ(1024, rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_z);
   EXPECT_EQ(0, rAccCurData.u16ErrorCounter);
}


TEST_F(Dispatcher_Opened, Disp_ValidateGyroRecord_InvalidCounter)
{
    OSAL_trIOCtrl3dGyroData rGyroCurData =  {1000,1024,10005,
                                             1024, 0};
   SenDisp_vValidateGyroRecord(&rGyroCurData);
   EXPECT_EQ(FALSE, rSensorDispInfo.rGyroInfo.bIsFirstRecord);
   EXPECT_EQ(1000, rSensorDispInfo.rGyroInfo.rGyroPrevData.u32TimeStamp);
   EXPECT_EQ(1024, rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_r);
   EXPECT_EQ(10005, rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_s);
   EXPECT_EQ(1024, rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_t);
   EXPECT_EQ(1, rGyroCurData.u16ErrorCounter);
}

TEST_F(Dispatcher_Opened, Disp_ValidateGyroRecord)
{
    OSAL_trIOCtrl3dGyroData rGyroCurData =  {1000,1024,1024,
                                             1024, 0};
   SenDisp_vValidateGyroRecord(&rGyroCurData);
   EXPECT_EQ(FALSE, rSensorDispInfo.rGyroInfo.bIsFirstRecord);
   EXPECT_EQ(1000, rSensorDispInfo.rGyroInfo.rGyroPrevData.u32TimeStamp);
   EXPECT_EQ(1024, rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_r);
   EXPECT_EQ(1024, rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_s);
   EXPECT_EQ(1024, rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_t);
   EXPECT_EQ(0, rGyroCurData.u16ErrorCounter);
}


TEST_F(Dispatcher_Opened, Disp_ValidateOdoRecord)
{
   OSAL_trIOCtrlOdometerData rOdoCurData =  {1000,1024,
                                             OSAL_EN_RFS_FORWARD, 0,
                                             ODOMSTATE_CONNECTED_NORMAL,
                                             0, 0, 0, 0, 0, 0};
   SenDisp_vValidateOdoRecord(&rOdoCurData);
   EXPECT_EQ(1000,rSensorDispInfo.rOdoInfo.rOdoPrevData.u32TimeStamp);
   EXPECT_EQ(FALSE, rSensorDispInfo.rOdoInfo.bIsFirstRecord);
   EXPECT_EQ(1, rOdoCurData.u16OdoMsgCounter);
   EXPECT_EQ(0, rOdoCurData.u16ErrorCounter);
}

TEST_F(Dispatcher_Opened, Disp_ValidateOdoRecord_InvalidCounter)
{
   OSAL_trIOCtrlOdometerData rOdoCurData =  {5000,1024,
                                             OSAL_EN_RFS_FORWARD, 0,
                                             ODOMSTATE_CONNECTED_NORMAL,
                                             0, 0, 0, 0, 0, 0};
   SenDisp_vValidateOdoRecord(&rOdoCurData);
   EXPECT_EQ(5000,rSensorDispInfo.rOdoInfo.rOdoPrevData.u32TimeStamp);
   EXPECT_EQ(FALSE, rSensorDispInfo.rOdoInfo.bIsFirstRecord);
   EXPECT_EQ(0, rOdoCurData.u16OdoMsgCounter);
   EXPECT_EQ(1, rOdoCurData.u16ErrorCounter);
}

TEST_F(Dispatcher_Opened, Disp_ValidateAbsRecord)
{
   OSAL_trAbsData rAbsCurData = {1000,1024,1024,1024,1024,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 0};
   SenDisp_vValidateAbsRecord(&rAbsCurData);
   EXPECT_EQ(1000,rSensorDispInfo.rAbsInfo.rAbsPrevData.u32TimeStamp);
   EXPECT_EQ(FALSE, rSensorDispInfo.rAbsInfo.bIsFirstRecord);
   EXPECT_EQ(0, rAbsCurData.u16ErrorCounter);
}

TEST_F(Dispatcher_Opened, Disp_ValidateAbsRecord_InvalidTimeStamp)
{
   OSAL_trAbsData rAbsCurData = {3500,1024,1024,1024,1024,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 0};
   SenDisp_vValidateAbsRecord(&rAbsCurData);
   EXPECT_EQ(3500,rSensorDispInfo.rAbsInfo.rAbsPrevData.u32TimeStamp);
   EXPECT_EQ(FALSE, rSensorDispInfo.rAbsInfo.bIsFirstRecord);
   EXPECT_EQ(1,rAbsCurData.u16ErrorCounter);
}

TEST_F(Dispatcher_Opened, Disp_ValidateAbsRecord_InvalidCounterValue)
{
   OSAL_trAbsData rAbsCurData = {1500,1024,1024,1024,32888,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_STATUS_NORMAL,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 OSAL_C_U8_ABS_DIR_FORWARD,
                                 0};
   SenDisp_vValidateAbsRecord(&rAbsCurData);
   EXPECT_EQ(1500,rSensorDispInfo.rAbsInfo.rAbsPrevData.u32TimeStamp);
   EXPECT_EQ(FALSE, rSensorDispInfo.rAbsInfo.bIsFirstRecord);
   EXPECT_EQ(1,rAbsCurData.u16ErrorCounter);
}

TEST_F(Dispatcher_Opened, Disp_DeInit)
{
   tS32 s32RetVal = OSAL_ERROR;
   EXPECT_CALL(PoS, dgram_send(_,_,_)).Times(1).WillOnce(Return(MSG_SIZE_SCC_SENSORS_C_COMPONENT_STATUS));
   EXPECT_CALL(Osal_mock, s32SemaphoreWait(_,_)).WillOnce(Return(OSAL_OK))
                                                .WillOnce(Return(OSAL_OK))
                                                .WillOnce(Return(OSAL_OK))
                                                .WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32SemaphorePost(_)).WillOnce(Return(OSAL_OK))
                                              .WillOnce(Return(OSAL_OK))
                                              .WillOnce(Return(OSAL_OK))
                                              .WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, vInc2SocPostDrivShutdown(_)).Times(1);
   EXPECT_CALL(PoS, OSAL_s32ThreadJoin(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32SensorDiapatcherDeInit(SEN_DISP_ODO_ID);
   s32SensorDiapatcherDeInit(SEN_DISP_GYRO_ID);
   s32SensorDiapatcherDeInit(SEN_DISP_ACC_ID);
   s32RetVal = s32SensorDiapatcherDeInit(SEN_DISP_ABS_ID);
   EXPECT_EQ(DEVICE_CLOSED,rSensorDispInfo.rOdoInfo.enOdoSts);
   EXPECT_EQ(OSAL_OK,s32RetVal);
}

