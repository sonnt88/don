/******************************************************************************
* FILE         : sensor_dispatcher.c
*
* DESCRIPTION  : This file implements sensor data dispatcher module.
*                In GEN3 Gyro, odometer and accelerometer sensors are connected to V850.
*                Sensor data will be sent from V850 to IMX via INC. It is the job of sensor
*                data dispatcher module to collect this data, and deliver this to respective
*                sensors modules . It adds sensor data to ring buffer. Driver may later read this
*                data from ring buffer. Sensor configuration will be  delivered to driver via a
*                message queue.
*
* AUTHOR(s)    : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY      :
*---------------------------------------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|---------------------------------------------------------------------
* 15.MAR.2013|  Initial version 1.0 | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------------------------------------
* 22.AUG.2013|  version 2.0         | Madhu Kiran Ramachandra (RBEI/ECF5)
*            |                      | Made necessary changes needed for the device to connect to v850.
* --------------------------------------------------------------------------------------------------------
* 19.NOV.2013|  version 2.1         | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*            |                      | Added Filter to detect erroneous POS records.
* --------------------------------------------------------------------------------------------------------
* 14.JAN.2014|  version 2.2         | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*            |                      | Added boundary check for sensor data received from scc
* --------------------------------------------------------------------------------------------------------
* 03.APR.2014|  version 2.3         | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
*            |                      | Added Errmem tracing and change in Initialization process.
* --------------------------------------------------------------------------------------------------------
* 09.JUL.2014|  version 2.4         | Madhu Kiran Ramachandra (RBEI/ECF5)
*            |                      | Added ability to discard incorrect responses from V850.
* --------------------------------------------------------------------------------------------------------
* 04.AUG.2014|  version 2.5         | Madhu Kiran Ramachandra (RBEI/ECF5)
*            |                      | Fix for SUZUKI-14230
* --------------------------------------------------------------------------------------------------------
* 10.FEB.2016|  version 2.6         | Srinivas Prakash Anvekar (RBEI/ECF5)
*            |                      | Merged changes from Fix for GMMY17-7125: Fix to buffer POS data when
*            |                      | sensor dispatcher is not active.
* --------------------------------------------------------------------------------------------------------
**********************************************************************************************************/
/*-----------------------------------------------------------------------
* Header file declaration
*-----------------------------------------------------------------------*/
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"
#include "osal_public.h"


#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>  /* inet_pton() */

#include "sensor_dispatcher_types.h"
#include "sensor_dispatcher.h"
#include "sensor_ring_buffer.h"
#include "inc2soc.h"


/*-----------------------------------------------------------------------
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/

trSensorDispInfo rSensorDispInfo;
OSAL_tThreadID tid_SenDispSockRd;

/*-----------------------------------------------------------------------
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
static tS32 s32ConfigureSocket(tVoid);
static tVoid SenDisp_pvSocketReadThread(tPVoid pvArg);
static tS32 SenDisp_s32SendStatusCmd(tU8 u8Status);
static tS32 s32HandleStatusMsg(tVoid);
static tS32 s32HandleSensorConfigMsg(tVoid);
static tVoid vHandleCommandReject(tVoid);
static tVoid vHandleSensorData(tVoid);
static tVoid SenDisp_vReleaseResourse(tU32 u32Resource);
static tS32 s32InitCommunToSCC(tVoid);
static tVoid SenDisp_vDispatchOdoData( tU32 u32Offset );
static tVoid SenDisp_vDispatchGyroData( tU32 u32Offset );
static tVoid SenDisp_vDispatchAccData( tU32 u32Offset );
static tVoid SenDisp_vDispatchGyroTemp( tU32 u32Offset );
static tVoid SenDisp_vDispatchAccTemp( tU32 u32Offset );
static tVoid SenDisp_vValidateOdoRecord(OSAL_trIOCtrlOdometerData *rOdoCurData);
static tVoid SenDisp_vValidateGyroRecord(OSAL_trIOCtrl3dGyroData *rGyroCurData);
static tVoid SenDisp_vValidateAccRecord(OSAL_trIOCtrlAccData *rAccCurData);
static tS32 SenDisp_s32StartThread ( tVoid );
static tS32 SenDisp_s32CreateResources ( tVoid );
static tVoid SenDisp_vErrmemLog( const unsigned char* pu8Buffer, tS32 s32Size );
static tVoid SenDisp_vHandleSensorSelfTestResult(tVoid);
static tVoid SenDisp_vPostAccSelfTestResult(tVoid);
static tVoid SenDisp_vPostGyroSelfTestResult(tVoid);
static tS32 SensorProxy_s32GetDataFromScc(tU32 u32Bytes);
#if defined (GEN3X86)
static tS32 SenDisp_pvSendStaticData (tVoid);
static tS32 SensorProxy_s32GetDataFromStub(tU32 u32Bytes);
#endif
static tVoid SenDisp_vExtractOdoStatus( OSAL_trIOCtrlOdometerData *rOdometerData );




#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE

/********************************************************************************
* FUNCTION       : dgram_recv 
*
* PARAMETER      : s32ConFD  : Socket FD
*                  pvDataBuff: pointer to memory for data to be stored
*                  u32Length : Maximum size of data that memory can hold
*
* RETURNVALUE    : (Number of bytes of data received - header_size)  on success
*                  OSAL_ERROR on Failure
*
* DESCRIPTION    : This is used to receive data from SCC.
*                  With the help of a header, it identifies message boundaries
*                  and delivers messages one by one as sent from SCC
*
* HISTORY        : 19.MAR.2013| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

static int dgram_recv(int s32ConFD, void *pvDataBuff, tU32 u32Length)
{

   tS32 s32RetVal=OSAL_ERROR;
   tU8 u8Size = 0;
   /* Routine parameter check */
   if((s32ConFD == 0) || (!u32Length) || (!pvDataBuff))
   {
      SenDisp_vTraceOut(TR_LEVEL_ERRORS,
      "!!!some parameters passed to dgram_recv are Invalid");
      s32RetVal = OSAL_ERROR;
   }
   else
   {
      /* receive message header from network stack. Data in header says how many bytes is the message from SCC */

      s32RetVal = recv(s32ConFD, &u8Size, sizeof(u8Size), 0);
      if( s32RetVal != (tS32)sizeof(u8Size) )
      {
         SenDisp_vTraceOut(TR_LEVEL_ERRORS,"!!! recv FAIL for Size RetVal %d", s32RetVal);
         s32RetVal=OSAL_ERROR;
      }
      else
      {
         SenDisp_vTraceOut(TR_LEVEL_USER_4,"!!! recv PASSED FOR SIZE. Data Size %d", u8Size);
         /* If recv buffer is large enough to hold complete message */
         if(u32Length > u8Size)
         {
            /* receive complete message from network stack*/
            s32RetVal = recv(s32ConFD, pvDataBuff, u8Size, 0);
            if(s32RetVal == u8Size)
            {
               SenDisp_vTraceOut(TR_LEVEL_USER_4,"!!! recv PASSED FOR DATA ");
            }
            else
            {
               SenDisp_vTraceOut(TR_LEVEL_ERRORS,"!!! recv FAIL for data RetVal %d", s32RetVal);
               s32RetVal=OSAL_ERROR;
            }
         }
         else
         {
            SenDisp_vTraceOut(TR_LEVEL_ERRORS,"Buffer not Big enough needed %d given %d", u8Size, u32Length);
            s32RetVal = OSAL_ERROR;
         }
      }
   }
   return s32RetVal;
}
#endif  // SENSOR_PROXY_TEST_STUB_ACTIVE


/********************************************************************************
* FUNCTION        : SensorProxy_s32GetDataFromStub
*
* PARAMETER       : Packet size
*
* RETURNVALUE     : Number of bytes received on success
*                            -1 on Failure
*                             0 on Timeout
*
* DESCRIPTION     : This function polls for the specified time 
*                            until data is available on the socket from V850 stub. 
*
* HISTORY         : 14-Apr-2016 - Initial Version - Kulkarni Ramchandra(RBEI/ECF5)
*------------------------------------------------------------------------------*/
#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE
static tS32 SensorProxy_s32GetDataFromStub(tU32 u32Bytes)
{
   tS32 s32RetVal = -1;
   struct pollfd fds;

   //To satisfy lint
   (tVoid)s32RetVal;

   fds.fd = rSensorDispInfo.s32SocketFD;
   fds.events = POLLIN;

   //clearing the event
   fds.revents = 0;
   nfds_t nfds = SEN_DISP_POLL_NUM_OF_FDS;

   s32RetVal = poll( &fds, nfds, SEN_DISP_POLL_TIMEOUT_MS );
   // If shutdown flag is set, return
   if ( TRUE ==  rSensorDispInfo.bShutdownFlag )
   {
      s32RetVal = -1;
   }
   // Timeout occoured
   else if ( 0 == s32RetVal )
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                        "Sen_disp: No Pos data received from stub in %lu millisec", 
                        SEN_DISP_POLL_TIMEOUT_MS);
   }
   else if( 0 > s32RetVal )
   {
      SenDisp_vTraceOut(TR_LEVEL_FATAL, 
                        "Sen_disp: stub poll() failed. errno: %d", 
                        errno );
   }
   // check if event received is for data
   else if ( fds.revents & POLLIN )
   {
      s32RetVal =  dgram_recv( rSensorDispInfo.s32SocketFD ,
                         (void *)rSensorDispInfo.u8RecvBuffer,
                         (size_t)u32Bytes );
      if ( 0 >= s32RetVal )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                           "Sen_disp: stub dgram_recv failed. Line: %d, RetVal: %d, errno: %d",
                           __LINE__, s32RetVal, errno );
      }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_FATAL, 
                        "Sen_disp: stub poll() returned invalid event: %x",
                        fds.revents );
      s32RetVal = -1;
   }
   return s32RetVal;
}
#endif


/********************************************************************************
* FUNCTION        : SensorProxy_s32GetDataFromScc 
*
* PARAMETER       : Packet size
*
* RETURNVALUE     : OSAL_OK  on success
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : This function makes the sensor dispatcher to wait for a specific time ,
                    Until it receives data from V850 . if data not found after the specific time ,
                    Gives the time out error and gives  multiple  try to get data.
*
* HISTORY         : 07.OCT.2015 - Initial Version - Shivasharnappa Mothpalli(RBEI/ECF5)
*------------------------------------------------------------------------------*/

static tS32 SensorProxy_s32GetDataFromScc(tU32 u32Bytes)
{

#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE
   return SensorProxy_s32GetDataFromStub( u32Bytes );
#else

   tS32 s32RetVal = -1;
   struct pollfd fds;
   tU32 u32PollCnrt = 0;
   static tU32 u32NoDataInt = 0;
   tBool bret;

   fds.fd = rSensorDispInfo.s32SocketFD;
   fds.events = POLLIN;
   nfds_t nfds = SEN_DISP_POLL_NUM_OF_FDS;

   do
   {
      bret = TRUE;
      s32RetVal = poll( &fds, nfds, SEN_DISP_POLL_TIMEOUT_MS );
      // If shutdown flag is set, return
      if ( TRUE ==  rSensorDispInfo.bShutdownFlag )
      {
         s32RetVal = -1;
      }
      // Timeout occoured
      else if ( 0 == s32RetVal )
      {
         u32PollCnrt++;
         if ( u32PollCnrt == SEN_DISP_POLL_COUNTER_THRESHOLD )
         {
            u32NoDataInt += (tU32)SEN_DISP_CONVERT_MS_TO_SEC ( SEN_DISP_DATA_INTERVAL_MS * SEN_DISP_READ_TIME_OUT_SCALE_FACTOR );
            SenDisp_vTraceOut( TR_LEVEL_FATAL,  "Sen_disp:NO Pos data for %lu sec", u32NoDataInt );
         }
         else
         {
            bret = FALSE;
         }
      }
      else if( 0 > s32RetVal )
      {
         SenDisp_vTraceOut(TR_LEVEL_FATAL,"Sen_disp:poll() fail errno:%d", s32RetVal );
      }
      // check if event received is for data
      else if ( fds.revents & POLLIN )
      {
         s32RetVal =  dgram_recv( rSensorDispInfo.hldSocDgram ,
                            (void *)rSensorDispInfo.u8RecvBuffer,
                            (size_t)u32Bytes );
         if ( 0 >= s32RetVal )
         {
            SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:dgram_recv Failed Line %d RetVal: %d errno : %d",
                                                 __LINE__, s32RetVal, errno );
         }
         // clear the data timeout counter
         u32NoDataInt = 0;
      }
      else
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL,"Sen_disp:poll() ret %d ev %x", s32RetVal, fds.revents );
         s32RetVal = -1;
      }
   }while ( FALSE == bret );

   return s32RetVal;

#endif
}

/********************************************************************************
* FUNCTION        : s32SensorDiapatcherInit 
*
* PARAMETER       : tEnSenDrvID : Unique sensor ID defined in sensor_dispatcher.h
*
* RETURNVALUE     : OSAL_OK  on success
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : This is initialization function for Sensor Dispatcher. 
*                   This shall be called from sensor drivers.
*                   1: Creates message queues to communicate to driver
*                   2: Establishes communication with SCC, exchange status.
*                   3: Based on configuration message received from SCC initializes ring buffer.
*                   4: Informs sensor config to respective drivers.
*                   5: Updates driver status in its configuration structure.
* HISTORY
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
*-------------|-----------------------|-------------------------------------
* 19.MAR.2013 | Initial version: 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------
* 03.Apr.2014 |         version: 1.1  | Moved Initialization to Sen Disp thread
* --------------------------------------------------------------------------*/
tS32 s32SensorDiapatcherInit(tEnSenDrvID enSenID)
{

   tS32 s32RetVal;

   SenDisp_vTraceOut(TR_LEVEL_USER_4," Disp init called by %d",(tS32) enSenID);

   /* This can be called by multiple sensor drivers. But init should happen once.
   So we need a semaphore to do this. But this is init, where to create semaphore ?
   So try to create semaphore. If it say already created, some other sensor has 
   created it. So just go ahead and use it.*/
   s32RetVal = OSAL_s32SemaphoreCreate( SEN_DISP_INIT_DEINT_SEM_NAME,
   &(rSensorDispInfo.HldInitSem),
   1  );
   if( (OSAL_ERROR == s32RetVal) && ( OSAL_u32ErrorCode() != OSAL_E_ALREADYEXISTS ) )
   {
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:%s Sem creation failed Err: %lu", 
      SEN_DISP_INIT_DEINT_SEM_NAME,
      OSAL_u32ErrorCode() );
      s32RetVal = OSAL_ERROR;
   }
   /* Take Semaphore */
   else if ( OSAL_OK == OSAL_s32SemaphoreWait( rSensorDispInfo.HldInitSem ,
            (OSAL_tMSecond) SEN_DISP_INIT_SEM_WAIT_TIME ))
   {
      /* If dispatcher is not initialized, Go ahead and initialize it. Else skip it */
      if( FALSE == rSensorDispInfo.bCreateFlag )
      {
         rSensorDispInfo.rOdoInfo.enOdoSts    = NOT_INITIALIZED;
         rSensorDispInfo.rGyroInfo.enGyroSts  = NOT_INITIALIZED;
         rSensorDispInfo.rAccInfo.enAccSts    = NOT_INITIALIZED;
         rSensorDispInfo.rAbsInfo.enAbsSts    = NOT_INITIALIZED;

         rSensorDispInfo.rOdoInfo.bIsFirstRecord = TRUE;
         rSensorDispInfo.rGyroInfo.bIsFirstRecord = TRUE;
         rSensorDispInfo.rAccInfo.bIsFirstRecord = TRUE;
         rSensorDispInfo.rAbsInfo.bIsFirstRecord = TRUE;

         rSensorDispInfo.bShutdownFlag = FALSE;

         if ( OSAL_OK == SenDisp_s32StartThread())
         {
            rSensorDispInfo.bCreateFlag = TRUE;
         }
         
      }

      /* IF DISPATCHER IS INITIALIZED, CHECK IF THE DRIVER IS SUPPORTED FROM V850.
         This is updated when config message is received */
      if( TRUE == rSensorDispInfo.bCreateFlag )
      {
         switch (enSenID)
         {
         case SEN_DISP_ODO_ID :
            {
               if( ( DEVICE_EXISTS == rSensorDispInfo.rOdoInfo.enOdoSts ) ||
                     ( DEVICE_CLOSED == rSensorDispInfo.rOdoInfo.enOdoSts ))
               {
                  s32RetVal = OSAL_OK;
                  rSensorDispInfo.rOdoInfo.enOdoSts = DEVICE_OPENED ;
                  SenDisp_vTraceOut(TR_LEVEL_USER_4,"Disp opened by Odo");
               }
               else
               {
                  s32RetVal = OSAL_E_NOTSUPPORTED;
                  SenDisp_vTraceOut(TR_LEVEL_FATAL,"Sen_disp:Odo not supported");
               }
               break;
            }
         case SEN_DISP_GYRO_ID :
            {
               if(( DEVICE_EXISTS == rSensorDispInfo.rGyroInfo.enGyroSts ) ||
                     ( DEVICE_CLOSED == rSensorDispInfo.rGyroInfo.enGyroSts ))
               {
                  s32RetVal = OSAL_OK;
                  rSensorDispInfo.rGyroInfo.enGyroSts = DEVICE_OPENED ;
                  SenDisp_vTraceOut(TR_LEVEL_USER_4,"Disp opened by Gyro");
               }
               else
               {
                  s32RetVal = OSAL_E_NOTSUPPORTED;
                  SenDisp_vTraceOut(TR_LEVEL_FATAL,"Sen_disp:Gyro not supported");
               }
               break;
            }
         case SEN_DISP_ACC_ID :
            {
               if(( DEVICE_EXISTS == rSensorDispInfo.rAccInfo.enAccSts ) ||
                     ( DEVICE_CLOSED == rSensorDispInfo.rAccInfo.enAccSts ))
               {
                  s32RetVal = OSAL_OK;
                  rSensorDispInfo.rAccInfo.enAccSts = DEVICE_OPENED ;
                  SenDisp_vTraceOut(TR_LEVEL_USER_4,"Disp opened by ACC");
               }
               else
               {
                  s32RetVal = OSAL_E_NOTSUPPORTED;
                  SenDisp_vTraceOut(TR_LEVEL_FATAL,"Sen_disp:ACC not supported");
               }   
               break;
            }
         case SEN_DISP_ABS_ID :
            {
               if(( DEVICE_EXISTS == rSensorDispInfo.rAbsInfo.enAbsSts ) ||
                     ( DEVICE_CLOSED == rSensorDispInfo.rAbsInfo.enAbsSts ))
               {
                  s32RetVal = OSAL_OK;
                  rSensorDispInfo.rAbsInfo.enAbsSts = DEVICE_OPENED ;
                  SenDisp_vTraceOut(TR_LEVEL_USER_4,"Disp opened by ABS");
               }
               else
               {
                  s32RetVal = OSAL_E_NOTSUPPORTED;
                  SenDisp_vTraceOut(TR_LEVEL_FATAL,"Sen_disp:ABS not supported");
               }   
               break;
            }
         default:
            {
               SenDisp_vTraceOut(TR_LEVEL_FATAL,"Default in Switch line %d", __LINE__);
               break;
            }
         }
      }
      /* Dispatcher is not initialized. Return error */
      else
      {
         s32RetVal = OSAL_ERROR;
      }
      /* Post Semaphore */
      if (OSAL_OK != OSAL_s32SemaphorePost(rSensorDispInfo.HldInitSem ))
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Sem post Failed line %d Err %u",
         __LINE__, OSAL_u32ErrorCode() );
         s32RetVal = OSAL_ERROR;
      }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Sem Wait Failed line %d Err %u",
      __LINE__, OSAL_u32ErrorCode() );
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}

#if defined (GEN3X86)
static tS32 SenDisp_pvSendStaticData ( tVoid )
{
    
    OSAL_trIOCtrlOdometerData rOdometerData = {SEN_DISP_STATIC_ODO_TIMESTAMP,SEN_DISP_STATIC_ODO_WHEELCOUNT,
                                                SEN_DISP_STATIC_ODO_DIRECTION,SEN_DISP_STATIC_ODO_ERRORCOUNT,SEN_DISP_STATIC_ODO_STATUS};
    OSAL_trIOCtrl3dGyroData r3dGyroData = {SEN_DISP_STATIC_GYRO_TIMESTAMP,SEN_DISP_STATIC_GYRO_R_VAL,SEN_DISP_STATIC_GYRO_S_VAL,
                                          SEN_DISP_STATIC_GYRO_T_VAL,SEN_DISP_STATIC_GYRO_ERRORCOUNT};
    OSAL_trIOCtrlAccData rAccData = {SEN_DISP_STATIC_ACC_TIMESTAMP,SEN_DISP_STATIC_ACC_X_VAL,SEN_DISP_STATIC_ACC_Y_VAL,
                                     SEN_DISP_STATIC_ACC_Z_VAL,SEN_DISP_STATIC_ACC_ERRORCOUNT};

    memset(rSensorDispInfo.u8RecvBuffer,0,sizeof(rSensorDispInfo.u8RecvBuffer));

    rSensorDispInfo.u32RecvdMsgSize = SEN_DISP_STATIC_RCVD_MSG_SIZE;
    rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ODO_CONFIG_DATA_INTERVAL] = SEN_DISP_STATIC_ODO_CONFIG_INTERVAL;
    rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_GYRO_CONFIG_DATA_INTERVAL] = SEN_DISP_STATIC_GYRO_CONFIG_INTERVAL;
    rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_GYRO_CONFIG_TYPE] = SEN_DISP_STATIC_GYRO_CONFIG_TYPE;
    rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ACC_CONFIG_DATA_INTERVAL] = SEN_DISP_STATIC_ACC_CONFIG_INTERVAL;
    rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ACC_CONFIG_TYPE] = SEN_DISP_STATIC_ACC_CONFIG_TYPE;

    if ( OSAL_OK == s32HandleSensorConfigMsg() )
    {
        SenDisp_vTraceOut(TR_LEVEL_FATAL,"Config message handled succssfully");
    }
    else
    {
        SenDisp_vTraceOut(TR_LEVEL_FATAL,"Config message handling failed");
    }
    if(OSAL_ERROR == OSAL_s32EventPost( rSensorDispInfo.hSenDispInitEvent,
                        SEN_DISP_EVENT_INIT_SUCCESS,OSAL_EN_EVENTMASK_OR ))
    {
        SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Event Post failed err %lu line %u",
                            OSAL_u32ErrorCode(), __LINE__ );
    }
    else
    {
        SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Event Post succesful");
    }
    while(1)
    {
        if( 1 != SenRingBuff_s32Write( SEN_DISP_ODO_ID,(tPVoid)&rOdometerData,1 ) )
        {
            SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write FAIL for ODO data");
        }
        else
        {
            SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write for 6 PASSED for ODO data");
        }
        OSAL_s32ThreadWait(SEN_DISP_STATIC_ODO_WAIT_TIME);
        if( 1 != SenRingBuff_s32Write( SEN_DISP_GYRO_ID,(tPVoid)&r3dGyroData,1 ) )
        {
            SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write FAIL for GYRO data");
        }
        else
        {
            SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write PASSED for GYRO data");
        }
        if( 1 != SenRingBuff_s32Write( SEN_DISP_ACC_ID,(tPVoid)&rAccData,1 ) )
        {
            SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write FAIL for ACC data");
        }
        else
        {
            SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write PASSED for ACC data");
        }
        OSAL_s32ThreadWait(SEN_DISP_STATIC_GYRO_ACC_WAIT_TIME);
    }
}
#endif
/********************************************************************************
* FUNCTION        : SenDisp_s32CreateResources 
*
* PARAMETER       : None
*
* RETURNVALUE     : OSAL_OK  on success
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : This creates all the OS resources needed
*                   1: Creates message queues to communicate to driver
*                   2: Establishes communication with SCC, exchange status.
*----------------------------------------------------------------------------
* HISTORY     :
*----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 03.APR.2014 | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ---------------------------------------------------------------------------
***********************************************************************************/
static tS32 SenDisp_s32CreateResources ( tVoid )
{
   tS32 s32RetVal;
#if defined (GEN3X86)
    tBool s32sockfail;
#endif
   
   /* Do socket setup. Just connect, no transactions */
  s32RetVal = s32ConfigureSocket();
  if( s32RetVal == OSAL_ERROR)
#if !defined(GEN3X86)
    {
        SenDisp_vTraceOut( TR_LEVEL_FATAL,"Sen_disp:s32ConfigureSocket Failed");
    }
    else 
#else
    {
        SenDisp_vTraceOut( TR_LEVEL_ERROR,"Sen_disp:connect Failed for LSIM sending static Data \n");
        s32sockfail = TRUE;
    }
#endif
   /* Create Odo message queue */
   if(OSAL_OK != (OSAL_s32MessageQueueCreate((tCString)SEN_DISP_TO_ODO_MSGQUE_NAME,
               ODO_MAX_MESSAGES_IN_MSGQUE,
               ODO_MSG_QUE_LENGTH,
               (OSAL_tenAccess)OSAL_EN_READWRITE,
               &rSensorDispInfo.rOdoInfo.hldOdoMsgQue)))
   {
      rSensorDispInfo.rOdoInfo.hldOdoMsgQue = OSAL_NULL;
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:MsgQ create Failed For ODO Err: %d",
      OSAL_u32ErrorCode());
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_SOCKET);
   }
   /* Create Gyro message queue */
   else if(OSAL_OK != (OSAL_s32MessageQueueCreate((tCString)SEN_DISP_TO_GYRO_MSGQUE_NAME,
               GYRO_MAX_MESSAGES_IN_MSGQUE,
               GYRO_MSG_QUE_LENGTH,
               (OSAL_tenAccess)OSAL_EN_READWRITE,
               &rSensorDispInfo.rGyroInfo.hldGyroMsgQue)))
   {
      rSensorDispInfo.rGyroInfo.hldGyroMsgQue = OSAL_NULL;
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:MsgQ create Failed For GYRO Err: %d",
      OSAL_u32ErrorCode());
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_SOCKET);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ODO_MSGQUE);
   }
   /* Create ACC message queue */
   else if(OSAL_OK != (OSAL_s32MessageQueueCreate( (tCString)SEN_DISP_TO_ACC_MSGQUE_NAME,
               ACC_MAX_MESSAGES_IN_MSGQUE,
               ACC_MSG_QUE_LENGTH,
               (OSAL_tenAccess)OSAL_EN_READWRITE,
               &rSensorDispInfo.rAccInfo.hldAccMsgQue)) )
   {
      rSensorDispInfo.rAccInfo.hldAccMsgQue = OSAL_NULL;
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:MsgQ create Failed For ACC Err: %d",
      OSAL_u32ErrorCode());
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_SOCKET);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ODO_MSGQUE);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_GYRO_MSGQUE);
   }
   /* Create ABS message queue */
   else if(OSAL_OK != (OSAL_s32MessageQueueCreate( (tCString)SEN_DISP_TO_ABS_MSGQUE_NAME,
               ABS_MAX_MESSAGES_IN_MSGQUE,
               ABS_MSG_QUE_LENGTH,
               (OSAL_tenAccess)OSAL_EN_READWRITE,
               &rSensorDispInfo.rAbsInfo.hldAbsMsgQue)) )
    {
      rSensorDispInfo.rAbsInfo.hldAbsMsgQue = OSAL_NULL;
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:MsgQ create Failed For ABS Err: %d",
      OSAL_u32ErrorCode());
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_SOCKET);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ODO_MSGQUE);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_GYRO_MSGQUE);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ACC_MSGQUE);
    }
    else
    {
#if defined(GEN3X86)
        if( s32sockfail == TRUE )
        {
            OSAL_tThreadID tid;
            OSAL_trThreadAttribute SenDispLSIM_SendStaticData = {OSAL_NULL};
            SenDispLSIM_SendStaticData.szName = "LSIM_DataSender";
            SenDispLSIM_SendStaticData.pfEntry = SenDisp_pvSendStaticData;
            SenDispLSIM_SendStaticData.s32StackSize = SEN_DISP_SOCKET_READ_THREAD_STACKSIZE; 
            SenDispLSIM_SendStaticData.u32Priority = SEN_DISP_STAIC_DATA_THREAD_PRIORITY;
            SenDispLSIM_SendStaticData.pvArg = OSAL_NULL;
            if(OSAL_ERROR != (tid = OSAL_ThreadSpawn(&SenDispLSIM_SendStaticData)))
            {
                SenDisp_vTraceOut( TR_LEVEL_USER_4, "OSAL_ThreadSpawn for LSIM successful.. \n");
                OSAL_vThreadExit();
            }
            else
            {
                SenDisp_vTraceOut( TR_LEVEL_FATAL, "OSAL_ThreadSpawn for LSIM failed \n");
            }
        }
#endif 
        /* Resource creation is done. Now communicate to SCC  */
        if( OSAL_OK != s32InitCommunToSCC() )
        {
            SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:InitCommunToSCC Failed Err: %d",
                                         OSAL_u32ErrorCode());
            SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ODO_MSGQUE);
            SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_GYRO_MSGQUE);
            SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ACC_MSGQUE);
            SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ABS_MSGQUE);
            SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_SOCKET);
        }
        else
        {
            s32RetVal = OSAL_OK;
        }
    }
    return s32RetVal;
}
/********************************************************************************
* FUNCTION        : SenDisp_s32StartThread 
*
* PARAMETER       : None
*
* RETURNVALUE     : OSAL_OK  on success
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : Start GNSS thread and wait for the thread to initialize
*                   all resources needed
*----------------------------------------------------------------------------
* HISTORY     :
*----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 03.APR.2014 | Initial version: 1.0   | Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
* ---------------------------------------------------------------------------
***********************************************************************************/
static tS32 SenDisp_s32StartThread ( tVoid )
{
   OSAL_trThreadAttribute rThreadAttr;
   tU32 u32ResultMask = 0;
   tS32 s32RetVal = OSAL_ERROR;

   /* This event is used to wait till the initialization is complete */
   if( OSAL_ERROR == OSAL_s32EventCreate( SEN_DISP_INIT_EVENT_NAME,
            &rSensorDispInfo.hSenDispInitEvent ))
   {
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Event create failed err %lu line %lu",
      OSAL_u32ErrorCode(), __LINE__ );
   }
   else
   {

      /*Update thread attributes*/
      rThreadAttr.szName         = SEN_DISP_SOCKET_READ_THREAD_NAME;
      rThreadAttr.u32Priority    = SEN_DISP_SOCKET_READ_THREAD_PRIORITY;      
      rThreadAttr.s32StackSize   = SEN_DISP_SOCKET_READ_THREAD_STACKSIZE;   
      rThreadAttr.pfEntry        = SenDisp_pvSocketReadThread;
      rThreadAttr.pvArg          = OSAL_NULL;
      /* This thread will create all OS resources and wait for data from SCC */
      if(OSAL_ERROR == (tid_SenDispSockRd = OSAL_ThreadSpawn(&rThreadAttr)) )
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp: Thread spawn failed" );
         s32RetVal = OSAL_ERROR;
      }
      /* Wait for Init to complete */
      else if ( OSAL_ERROR == OSAL_s32EventWait( rSensorDispInfo.hSenDispInitEvent,
               (SEN_DISP_EVENT_WAIT_MASK),
               OSAL_EN_EVENTMASK_OR,
               SEN_DISP_INIT_EVENT_WAIT_TIME,
               &u32ResultMask ))
      {
         /* If there is no response from Sen Pxy thread, only reason could be V850 is not responding. */
         if ( OSAL_E_TIMEOUT == OSAL_u32ErrorCode() )
         {
            SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp: NO response from V850" );
         }
         else
         {
            SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Event wait fail Err: %lu, line %lu",
            OSAL_u32ErrorCode(), __LINE__ );
         }
      }
      
      else if ( OSAL_ERROR == OSAL_s32EventPost( rSensorDispInfo.hSenDispInitEvent,
               ~u32ResultMask,
               OSAL_EN_EVENTMASK_AND ) )
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Event clear fail Err: %lu, line %lu",
            OSAL_u32ErrorCode(), __LINE__ );
      }
      
      if( SEN_DISP_EVENT_INIT_SUCCESS == u32ResultMask)
      {
         SenDisp_vTraceOut( TR_LEVEL_USER_4,"Dispatcher initialization success." );
         s32RetVal = OSAL_OK;
      }
      /* Init failed for some reason */
      else if( ( SEN_DISP_EVENT_INIT_FAILED == u32ResultMask ) ||
               ( SEN_DISP_EVENT_DEINIT_COMPLETE == u32ResultMask ) )
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "SenDisp Init Fail waited on unknown event %lu", u32ResultMask );
      }
      SenDisp_vReleaseResourse( SEN_DISP_RESOURCE_RELEASE_INIT_EVENT );
   }
   return s32RetVal;
}

/********************************************************************************
* FUNCTION       : SenDisp_pvSocketReadThread 
*
* PARAMETER      : pvArg : Dummy argument
*
* RETURNVALUE    : None
*
* DESCRIPTION    : This thread will be waiting for data from SCC. It parses the data and 
*                  delivers it to respective driver via ring buffer or message queue.
*
*----------------------------------------------------------------------------
* HISTORY     :
*----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 19.MAR.2013 | Initial version: 1.0   | Madhu Kiran Ramachandra (RBEI/ECF5)
* ---------------------------------------------------------------------------
* 03.Apr.2014 |         version: 1.1   | Madhu Kiran Ramachandra (RBEI/ECF5)
*             |                        | Move resources creation and INC Init
*             |                        | as a part of thread
* ---------------------------------------------------------------------------
* 12.MAY.2014 |         version: 1.2   | Sai Chowdary Samineni (RBEI/ECF5)
*                                          Modified to include self test
**********************************************************************************/
static tVoid SenDisp_pvSocketReadThread(tPVoid pvArg)
{
   tS32 s32ErrChk = OSAL_ERROR;
   tU32 u32InitRetryCnt;
   (tVoid)pvArg; //To satisfy lint

   /* Try Init in a loop */
   for ( u32InitRetryCnt = 0;
   ((u32InitRetryCnt < SEN_DISP_SOCKET_INIT_RETRY_COUNT)&&(OSAL_ERROR == s32ErrChk));
   u32InitRetryCnt++ )
   {
      if ( OSAL_ERROR == (s32ErrChk = SenDisp_s32CreateResources()) )
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:SenDisp_s32CreateResources Fail" );
         OSAL_s32ThreadWait( SEN_DISP_INIT_RETRY_WAIT_TIME_MS );
      }
   }

   /* POST Init success event */
   if ( OSAL_OK == s32ErrChk )
   {
      if( OSAL_ERROR == OSAL_s32EventPost( rSensorDispInfo.hSenDispInitEvent,
               SEN_DISP_EVENT_INIT_SUCCESS,
               OSAL_EN_EVENTMASK_OR ))
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Event Post failed err %lu line %u",
         OSAL_u32ErrorCode(), __LINE__ );
         s32ErrChk = OSAL_ERROR;
      }
   }
   /* POST Init Fail event */
   else if( OSAL_ERROR == OSAL_s32EventPost( rSensorDispInfo.hSenDispInitEvent,
            SEN_DISP_EVENT_INIT_FAILED,
            OSAL_EN_EVENTMASK_OR ))
   {
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Event Post failed err %lu line %u",
      OSAL_u32ErrorCode(), __LINE__ );
   }   


   if ( OSAL_OK == s32ErrChk )
   {
      if(OSAL_ERROR == Inc2Soc_s32CreateResources( INC2SOC_DEV_TYPE_POS ))
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Sen_disp: Resource creation failed for third party PoS");
      }
      while( TRUE != rSensorDispInfo.bShutdownFlag )
      {

         s32ErrChk = SensorProxy_s32GetDataFromScc(SEN_DISP_MAX_PACKET_SIZE);

         if( s32ErrChk > 0 )
         {
            rSensorDispInfo.u32RecvdMsgSize = (tU32)s32ErrChk;
            SenDisp_vTraceOut( TR_LEVEL_USER_4, "Msg received with ID %x , Size %d",
            (tS32)rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID], s32ErrChk);
            /* first byte says the type of message */
            switch (rSensorDispInfo.u8RecvBuffer[0])
            {
            case MSG_ID_SCC_SENSORS_R_COMPONENT_STATUS:
               {
                  (tVoid)s32HandleStatusMsg();
                  break;
               }
            case MSG_ID_SCC_SENSORS_R_CONFIG:
               {

                  /* Configuration should not change in run time */
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Runtime Config change is not Expected" );
                  break;
               }
            case MSG_ID_SCC_SENSORS_R_REJECT:
               {
                  vHandleCommandReject();
                  break;
               }
            case MSG_ID_SCC_SENSOR_R_DATA:
               {
                  Inc2Soc_vWriteDataToBuffer(INC2SOC_DEV_TYPE_POS);
                  vHandleSensorData();
                  break;
               }
            case MSG_ID_SCC_SENSORS_R_SELFTEST:  
               {
                  SenDisp_vHandleSensorSelfTestResult();
                  break;
               }
            default:
               {
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS," Unknown Msg ID %x",
                  (tS32)rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID] );
                  break;
               }
            }
         }
         else
         {
            SenDisp_vTraceOut (TR_LEVEL_ERRORS, "dgram_recv Failed Line %d RetVal: %d errno : %d",
            __LINE__, s32ErrChk, errno);
            (tVoid)OSAL_s32ThreadWait(SEN_DISP_RETRY_INTERVALL_MS);
         }
      }

      SenDisp_vTraceOut( TR_LEVEL_USER_4," Shutdown signaled");
      
      SenDisp_vReleaseResourse( SEN_DISP_RESOURCE_RELSEASE_SOCKET );
      
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ODO_MSGQUE);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_GYRO_MSGQUE);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ACC_MSGQUE);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ABS_MSGQUE);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_RINGBUFF);
      SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_INIT_SEM);
      rSensorDispInfo.bCreateFlag = FALSE;
      rSensorDispInfo.u8SccStatus =  (tU8)SCC_SENSOR_COMPONENT_STATUS_NOT_ACTIVE;
   }
   SenDisp_vTraceOut(TR_LEVEL_USER_4,"Shutdown Complete");
   OSAL_vThreadExit();
}


/********************************************************************************
* FUNCTION       : SenDisp_vHandleSensorSelfTestResult  
*
* PARAMETER      :  NONE
*
* RETURNVALUE    : void
*
* DESCRIPTION    : Handles received selftest messages.
*
*----------------------------------------------------------------------------
* HISTORY     :
*----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 12.MAY.2014 | Initial version: 1.0   | Sai Chowdary Samineni (RBEI/ECF5)
* ---------------------------------------------------------------------------
**********************************************************************************/

static tVoid SenDisp_vHandleSensorSelfTestResult(tVoid)
{

   switch(rSensorDispInfo.u8RecvBuffer[SEN_DISP_SELF_TEST_INDEX])
   {

   case SEN_DISP_SELF_TEST_RESULT_DIAGNOSIS:
      //Nothing for now
      break;
   case SEN_DISP_SELF_TEST_RESULT_ODOMETER:
      //Nothing for now
      break;
   case SEN_DISP_SELF_TEST_RESULT_GYROMETER:
      {
         SenDisp_vPostGyroSelfTestResult();
      }
      break;
   case SEN_DISP_SELF_TEST_RESULT_GYRO_TEMPERATURE:
      //Nothing for now
      break;
   case SEN_DISP_SELF_TEST_RESULT_ACCELEROMETER:
      {
         SenDisp_vPostAccSelfTestResult();
      }
      break;
   case SEN_DISP_SELF_TEST_RESULT_ACC_TEMPERATURE:
      //Nothing for now
      break;
   default:
      //Nothing for now
      break;

   }

}

/********************************************************************************
* FUNCTION       : SenDisp_vPostAccSelfTestResult 
*
* PARAMETER      :  void
*
* RETURNVALUE    : void
*
* DESCRIPTION    : Posts the Acc Self Test Result in the message queue. 
*
*----------------------------------------------------------------------------
* HISTORY     :
*----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 12.MAY.2014 | Initial version: 1.0   | Sai Chowdary Samineni (RBEI/ECF5)
* ---------------------------------------------------------------------------
**********************************************************************************/


static tVoid SenDisp_vPostAccSelfTestResult(tVoid)
{
   SensorSelfTestResult rAccSelfTestResult ;
   
   /*Self test result */
   rAccSelfTestResult.u8MsgType = SEN_DISP_MSG_TYPE_SELF_TEST_RESULT;
   rAccSelfTestResult.u8SelfTestResult = rSensorDispInfo.u8RecvBuffer[2];
   
   SenDisp_vTraceOut( TR_LEVEL_USER_4,"Acc selftest result %x",
   rAccSelfTestResult.u8SelfTestResult);
   
   /* Post data to gyro message queue */
   if( OSAL_OK != OSAL_s32MessageQueuePost( rSensorDispInfo.rAccInfo.hldAccMsgQue,
            (tPCU8) &rAccSelfTestResult,
            sizeof(SensorSelfTestResult),
            SEN_DISP_MSGQUE_PRIO) )
   {
      SenDisp_vTraceOut(  TR_LEVEL_ERROR, 
      "Posting Acc selftest result to gyro MsgQ failed %lu",
      OSAL_u32ErrorCode());
   }

}


/********************************************************************************
* FUNCTION       : SenDisp_vPostGyroSelfTestResult 
*
* PARAMETER      :  void
*
* RETURNVALUE    : void
*
* DESCRIPTION    : Posts the Gyro Self Test Result in the message queue. 
*
*----------------------------------------------------------------------------
* HISTORY     :
*----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 12.MAY.2014 | Initial version: 1.0   | Sai Chowdary Samineni (RBEI/ECF5)
* ---------------------------------------------------------------------------
**********************************************************************************/

static tVoid SenDisp_vPostGyroSelfTestResult(tVoid)   
{
   SensorSelfTestResult rGyroSelfTestResult ;
   
   /*Self test result */
   rGyroSelfTestResult.u8MsgType = SEN_DISP_MSG_TYPE_SELF_TEST_RESULT;
   rGyroSelfTestResult.u8SelfTestResult = rSensorDispInfo.u8RecvBuffer[2];
   
   SenDisp_vTraceOut( TR_LEVEL_USER_4,"Gyro selftest result %x",
   rGyroSelfTestResult.u8SelfTestResult);
   
   /* Post data to gyro message queue */
   if( OSAL_OK != OSAL_s32MessageQueuePost( rSensorDispInfo.rGyroInfo.hldGyroMsgQue,
            (tPCU8) &rGyroSelfTestResult,
            sizeof(SensorSelfTestResult),
            SEN_DISP_MSGQUE_PRIO) )
   {
      SenDisp_vTraceOut( TR_LEVEL_ERROR,
      "Posting Gyro selftest result to gyro MsgQ failed %lu",
      OSAL_u32ErrorCode());
   }

}

/********************************************************************************
* FUNCTION       : SenDisp_s32TriggerGyroSelfTest 
*
* PARAMETER      :  void
*
* RETURNVALUE    : OSAL_E_NOERROR  on success
*                           OSAL_ERROR on Failure
*
* DESCRIPTION    :  Composes and sends INC message to V850 to trigger Gyro Selftest messgae.
*
*----------------------------------------------------------------------------
* HISTORY     :
*----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 12.MAY.2014 | Initial version: 1.0   | Sai Chowdary Samineni (RBEI/ECF5)
* ---------------------------------------------------------------------------
**********************************************************************************/

tS32 SenDisp_s32TriggerGyroSelfTest(tVoid)
{

   tS32 s32RetVal;
   trMsgSensorSelfTest rMsgSensorSelfTest;
   tU8 u8Buf[2] = {0};

   rMsgSensorSelfTest.u8MsgID = MSG_ID_SCC_SENSORS_C_SELFTEST;
   rMsgSensorSelfTest.u8SelfTest = SEN_DISP_SELF_TEST_DEVICE_GYRO;

   /* Fill in message ID */
   OSAL_pvMemoryCopy( &u8Buf[SEN_DISP_OFFSET_MSG_ID],
   &rMsgSensorSelfTest.u8MsgID,
   sizeof(rMsgSensorSelfTest.u8MsgID));
   /* Fill in Device */
   OSAL_pvMemoryCopy( &u8Buf[SEN_DISP_OFFSET_SELFTEST_DEVICE_ID],
   &rMsgSensorSelfTest.u8SelfTest,
   sizeof(rMsgSensorSelfTest.u8SelfTest) );

#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE

   /* Write data to socket */
   s32RetVal = write( rSensorDispInfo.s32SocketFD,
   (void *) u8Buf,
   (size_t) sizeof(u8Buf) );

#else
   /* Send data via datagram layer */
   s32RetVal = dgram_send( rSensorDispInfo.hldSocDgram, 
   (void *)u8Buf,
   (size_t) sizeof(u8Buf) );
#endif

   if( (tS32)sizeof(trMsgSensorSelfTest) == s32RetVal )
   {
      SenDisp_vTraceOut( TR_LEVEL_USER_4,"Sent Gyro self test trigger message successfully" );
      s32RetVal = OSAL_E_NOERROR;
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_ERROR,"Sending Gyro self test trigger message failed" );
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;

}

/********************************************************************************
* FUNCTION       : SenDisp_s32TriggerAccSelfTest 
*
* PARAMETER      :  void
*
* RETURNVALUE    : OSAL_E_NOERROR  on success
*                           OSAL_ERROR on Failure
*
* DESCRIPTION    :  Composes and sends INC message to V850 to trigger Acc Selftest messgae.
*
*----------------------------------------------------------------------------
* HISTORY     :
*----------------------------------------------------------------------------
* Date        |       Version          | Author & comments
*-------------|------------------------|-------------------------------------
* 12.MAY.2014 | Initial version: 1.0   | Sai Chowdary Samineni (RBEI/ECF5)
* ---------------------------------------------------------------------------
**********************************************************************************/

tS32 SenDisp_s32TriggerAccSelfTest(tVoid)
{

   tS32 s32RetVal;
   trMsgSensorSelfTest rMsgSensorSelfTest;
   tU8 u8Buf[2] = {0};

   rMsgSensorSelfTest.u8MsgID = MSG_ID_SCC_SENSORS_C_SELFTEST;
   rMsgSensorSelfTest.u8SelfTest = SEN_DISP_SELF_TEST_DEVICE_ACC;

   /* Fill in message ID */
   OSAL_pvMemoryCopy( &u8Buf[SEN_DISP_OFFSET_MSG_ID],
   &rMsgSensorSelfTest.u8MsgID,
   sizeof(rMsgSensorSelfTest.u8MsgID));
   /* Fill in Device */
   OSAL_pvMemoryCopy( &u8Buf[SEN_DISP_OFFSET_SELFTEST_DEVICE_ID],
   &rMsgSensorSelfTest.u8SelfTest,
   sizeof(rMsgSensorSelfTest.u8SelfTest) );
   
#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE

   /* Write data to socket */
   s32RetVal = write( rSensorDispInfo.s32SocketFD,
   (void *) u8Buf,
   (size_t) sizeof(u8Buf) );

#else
   /* Send data via datagram layer */
   s32RetVal = dgram_send( rSensorDispInfo.hldSocDgram, 
   (void *)u8Buf,
   (size_t) sizeof(u8Buf) );
#endif

   if( (tS32)sizeof(trMsgSensorSelfTest) == s32RetVal )
   {
      SenDisp_vTraceOut( TR_LEVEL_USER_4,"Sent Acc self test trigger message successfully" );
      s32RetVal = OSAL_E_NOERROR;
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_ERROR,"Sending Acc self test trigger message failed" );
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;

}


/********************************************************************************
* FUNCTION        : SenDisp_s32SendStatusCmd 
*
* PARAMETER       : NONE
*
* RETURNVALUE     : OSAL_OK  on success
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : Frames the status command message as ACTIVE and transmits it to SCC.
*
* HISTORY         : 19.MAR.2013| Initial Version   |Madhu Kiran Ramachandra (RBEI/ECF5)
*                   10.FEB.2016| Version 1.1       |Srinivas Prakash Anvekar ()
*                                                   added a variable to get the status 
*                                                   which has to be sent to v850
**********************************************************************************/
static tS32 SenDisp_s32SendStatusCmd(tU8 u8Status)
{

   tS32 s32RetVal;
   trMsgCmdHostStatus rMsgCmdEnqStatus;

   rMsgCmdEnqStatus.u8MsgID = MSG_ID_SCC_SENSORS_C_COMPONENT_STATUS; 
   rMsgCmdEnqStatus.u8HostStatus = u8Status;
   rMsgCmdEnqStatus.u8VersionInfo = SEN_DISP_VERSION_INFO;

   /* Fill in message ID */
   OSAL_pvMemoryCopy( &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID],
   &rMsgCmdEnqStatus.u8MsgID,
   sizeof(rMsgCmdEnqStatus.u8MsgID));
   /* Fill in ACTIVE status */
   OSAL_pvMemoryCopy( &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_COMPONENT_STATUS],
   &rMsgCmdEnqStatus.u8HostStatus,
   sizeof(rMsgCmdEnqStatus.u8HostStatus) );
   
   /* Fill in Version Info */
   OSAL_pvMemoryCopy( &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_VERSION_STATUS],
   &rMsgCmdEnqStatus.u8VersionInfo,
   sizeof(rMsgCmdEnqStatus.u8VersionInfo) );
   
#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE

   /* Write data to socket */
   s32RetVal = write( rSensorDispInfo.s32SocketFD,
   (void *) rSensorDispInfo.u8RecvBuffer,
   (size_t) MSG_SIZE_SCC_SENSORS_C_COMPONENT_STATUS );

#else
   /* Send data via datagram layer */
   s32RetVal = dgram_send( rSensorDispInfo.hldSocDgram, 
   (void *) rSensorDispInfo.u8RecvBuffer,
   (size_t) MSG_SIZE_SCC_SENSORS_C_COMPONENT_STATUS );
#endif

   /* Did we send complete message ? */
   if( MSG_SIZE_SCC_SENSORS_C_COMPONENT_STATUS == s32RetVal )
   {
      SenDisp_vTraceOut( TR_LEVEL_USER_4,"write passed for Component status" );
      s32RetVal = OSAL_OK;
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_FATAL,"Sen_Disp:write failed for Component status" );
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}

/********************************************************************************
* FUNCTION      : s32ConfigureSocket 
*
* PARAMETER     : NONE
*
* RETURNVALUE   : OSAL_OK  on success
*                 OSAL_ERROR on Failure
*
* DESCRIPTION   : It creates socket and tries to connect to SCC. 
*                 Connect will succeed if HOST (SCC) has called accept.
* 
* HISTORY       : 29.JAN.2013| Initial Version   |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tS32 s32ConfigureSocket()
{
#ifndef SENSOR_PROXY_TEST_STUB_ACTIVE

   tS32 s32RetVal = OSAL_ERROR;
   struct hostent *SccIP,*ImxIP;
   struct sockaddr_in SocAddr_local,SocAddr_remote;

   /* Socket Type */
   SocAddr_local.sin_family = AF_BOSCH_INC_AUTOSAR;
   SocAddr_remote.sin_family = AF_BOSCH_INC_AUTOSAR;

   /* Port */
   //lint -e160
   SocAddr_local.sin_port = (tU16)htons(SENSORS_PORT);
   SocAddr_remote.sin_port = (tU16)htons(SENSORS_PORT);

   rSensorDispInfo.s32SocketFD = socket( AF_BOSCH_INC_AUTOSAR,(tS32)SOCK_STREAM, 0 );
   if( rSensorDispInfo.s32SocketFD > 0 )
   {
      if( (SccIP = gethostbyname("scc")) == OSAL_NULL )
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:scc gethostbyname returned null errno %d", errno );
         SenDisp_vReleaseResourse( SEN_DISP_RESOURCE_RELSEASE_SOCKET );
      }
      else 
      {
         OSAL_pvMemoryCopy( (char *) &SocAddr_remote.sin_addr.s_addr,
         (char *)(SccIP->h_addr),
         (tU32)SccIP->h_length);
         
         if( (ImxIP = gethostbyname("scc-local") ) == OSAL_NULL )
         {
            SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:scc-local gethostbyname returned null errno %d",
            errno );
            SenDisp_vReleaseResourse( SEN_DISP_RESOURCE_RELSEASE_SOCKET );
         }
         else
         {
            OSAL_pvMemoryCopy( (char *) &SocAddr_local.sin_addr.s_addr,
            (char *)(ImxIP->h_addr),
            (tU32)ImxIP->h_length );

            
            /* Bind to local address */
            //lint -e64
            s32RetVal = bind( rSensorDispInfo.s32SocketFD,
            /* Double type cast to satisfy lint */
            (struct sockaddr *)(tPVoid)&SocAddr_local,
            sizeof(SocAddr_local));
            
            if( s32RetVal == 0 )
            {
               /* Connect to remote scc address */
               s32RetVal = connect( rSensorDispInfo.s32SocketFD,
               /* Double type cast to satisfy lint */
               (struct sockaddr *)(tPVoid)&SocAddr_remote,
               sizeof(SocAddr_remote) );
               if( s32RetVal == 0 )
               {
                  SenDisp_vTraceOut( TR_LEVEL_USER_4,"Connect passed :-)" );
                  s32RetVal = OSAL_OK;
               }
               else
               {
                  SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Connect Failed errno=%d", errno );
                  SenDisp_vReleaseResourse( SEN_DISP_RESOURCE_RELSEASE_SOCKET );
                  OSAL_s32ThreadWait( 500 );
               }
            }
            else
            {
               SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Bind to socket failed errno=%d", errno );
               SenDisp_vReleaseResourse( SEN_DISP_RESOURCE_RELSEASE_SOCKET );
            }
         }
      }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:Socket system call failed errno=%d", errno );
   }

#else
   /* to connect to sensor test stub */
   tS32 s32RetVal = OSAL_ERROR; 
   struct sockaddr_in SocAddr;

   /* create a socket */
   rSensorDispInfo.s32SocketFD = socket( AF_INET, (tS32)SOCK_STREAM, 0 );
   if( rSensorDispInfo.s32SocketFD > 0 )
   {
      /* Fill in the socket config structure */
      SocAddr.sin_family = AF_INET; /* Family */
      s32RetVal = inet_pton(AF_INET, SOCKET_IP_ADDR, &SocAddr.sin_addr); /* IP of HOST */
      if(s32RetVal < 1)
      {
         SenDisp_vTraceOut(TR_LEVEL_ERRORS,"inet_pton Failed");
      }

      SocAddr.sin_port = htons(INC_PORT_SENSOR_COMPONENT); /* Port in HOST */
      /* Try to connect to host */
      s32RetVal = connect(rSensorDispInfo.s32SocketFD, (struct sockaddr *)&SocAddr, sizeof(SocAddr));
      if( s32RetVal == 0 )
      {
         SenDisp_vTraceOut( TR_LEVEL_USER_4, "Connect to SCC simulation STUB passed :-)" );
         s32RetVal = OSAL_OK;
      }
      else
      {
         SenDisp_vTraceOut(TR_LEVEL_ERRORS,"Connect failed. Make sure that sensor stub is on");
         SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_SOCKET);
         s32RetVal = OSAL_ERROR;
      }
   }
   else
   {
      SenDisp_vTraceOut(TR_LEVEL_ERRORS,"socket create failed");
   }

#endif
   return s32RetVal;

}

/********************************************************************************
* FUNCTION    : s32HandleStatusMsg 
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_OK  on success
*               OSAL_ERROR on Failure
*
* DESCRIPTION : Just updates the status of SCC in dispatcher config structure
*
* HISTORY     : 19.MAR.2013| Initial Version    |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tS32 s32HandleStatusMsg()
{
   tS32 s32RetVal = OSAL_ERROR;

   if ( ( MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS == rSensorDispInfo.u32RecvdMsgSize ) || ( MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS_OLD == rSensorDispInfo.u32RecvdMsgSize ) )
   {
      if( (tU8)SCC_SENSOR_COMPONENT_STATUS_ACTIVE == rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_COMPONENT_STATUS] )
      {
         SenDisp_vTraceOut( TR_LEVEL_USER_4,"Sen_disp: Sensor @ SCC status ACTIVE" );
         rSensorDispInfo.u8SccStatus = (tU8)SCC_SENSOR_COMPONENT_STATUS_ACTIVE;
         
         if( MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS == rSensorDispInfo.u32RecvdMsgSize )
         {
            rSensorDispInfo.u8SccVerInfo = (tU8)rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_VERSION_STATUS];
            
            if( SEN_DISP_VERSION_INFO != rSensorDispInfo.u8SccVerInfo )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Sen_disp: ERROR: expected %lu recvd %lu", 
               SEN_DISP_VERSION_INFO, rSensorDispInfo.u8SccVerInfo );
            }
            else
            {
               SenDisp_vTraceOut( TR_LEVEL_USER_4,"Sen_disp: Version Check passed, expected %lu recvd %lu", 
               SEN_DISP_VERSION_INFO, rSensorDispInfo.u8SccVerInfo );
            }
         }
         else
         {
            SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Sen_disp: The Status message size received is %u,"
            "No version info received from SCC", 
            MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS_OLD );
         }
         
         s32RetVal = OSAL_OK;
      }
      else if( SCC_SENSOR_COMPONENT_STATUS_NOT_ACTIVE == rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_COMPONENT_STATUS] )
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:!!!! Sensor @ SCC is not ACTIVE. " );
         rSensorDispInfo.u8SccStatus = (tU8)SCC_SENSOR_COMPONENT_STATUS_NOT_ACTIVE;
      }
      else
      {
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp:!!!! Unknown status: %u of SCC ",
         (tS32) rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_COMPONENT_STATUS] );
      }

   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_disp: Size of status message should be either %d or %d but got %d bytes",
      MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS_OLD,
      MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS, rSensorDispInfo.u32RecvdMsgSize );
   }
   return s32RetVal;
}


/********************************************************************************
* FUNCTION     : SenDisp_vValidateAbsRecord
*
* PARAMETER    : OSAL_trAbsData * : POinter to Abs Data
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This is a filter to detect corrupt Abs data
* HISTORY      : 9.JULY.2014| Initial Version    |Sai Chowdary Samineni (RBEI/ECF5)
**********************************************************************************/
static tVoid SenDisp_vValidateAbsRecord(OSAL_trAbsData *rAbsCurData)
{

   tS32 s32DataDiff;

   if( OSAL_NULL == rAbsCurData )
   {
      SenDisp_vTraceOut( TR_LEVEL_FATAL, "NULL pointer line: %lu", __LINE__ );
   }
   else
   {
      //Set Default values
      rAbsCurData->u16ErrorCounter = 0;

      s32DataDiff = (tS32)(rAbsCurData->u32TimeStamp - rSensorDispInfo.rAbsInfo.rAbsPrevData.u32TimeStamp);
      s32DataDiff = OSAL_s32Absolute( s32DataDiff );
      //Store data for next comparison and validation
      rSensorDispInfo.rAbsInfo.rAbsPrevData.u32TimeStamp = rAbsCurData->u32TimeStamp;

      if ( TRUE == rSensorDispInfo.rAbsInfo.bIsFirstRecord )
      {
         rSensorDispInfo.rAbsInfo.bIsFirstRecord = FALSE;
      }
      //Validate time stamp
      else if( s32DataDiff > SEN_DISP_MAX_TIME_STAMP_DIFFERENCE )
      {
         rAbsCurData->u16ErrorCounter = 1;
         SenDisp_vTraceOut( TR_LEVEL_ERRORS,
         "Invalid ABS time stamp diff: %d",
         s32DataDiff );
      }
      else if( (rAbsCurData->u32CounterRearRight > SEN_DISP_MAX_ABS_COUNTER_VALUE )||
            (rAbsCurData->u32CounterRearLeft > SEN_DISP_MAX_ABS_COUNTER_VALUE ) )
      {
         rAbsCurData->u16ErrorCounter = 1;
         SenDisp_vTraceOut( TR_LEVEL_ERRORS,
         "Counter value exceeded max range: %u  %u",
         rAbsCurData->u32CounterRearRight,rAbsCurData->u32CounterRearLeft );
      }

      //We don't have a plan to validate ABS data as of now.
      //TODO:If validation is required.
   }
}

/********************************************************************************
* FUNCTION     : SenDisp_vDispatchAbsData
*
* PARAMETER    : tU32 u32Offset: Offset to the record to be extracted
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This will extract one Abs sample, validate it and add it to ring buffer
* HISTORY      : 9.JULY.2014| Initial Version    | Sai Chowdary Samineni (RBEI/ECF5)
**********************************************************************************/
static tVoid SenDisp_vDispatchAbsData( tU32 u32Offset )
{

   OSAL_trAbsData rAbsData;
   tU8 u8Status = 0;

   //This is needed because source and destination locations are not of same size
   OSAL_pvMemorySet(&rAbsData, 0, sizeof(rAbsData));

   //Extract Time stamp
   OSAL_pvMemoryCopy( &rAbsData.u32TimeStamp,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_TIME_STAMP],
   SEN_DISP_FIELD_SIZE_TIME_STAMP );

   //Extract Rear Right wheel counter
   OSAL_pvMemoryCopy( &rAbsData.u32CounterRearRight,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_ABS_RR_WHEEL_CNTR],
   SEN_DISP_FIELD_SIZE_ABS_RR_WHEEL_CNTR );

   //Extract Rear Left wheel counter
   OSAL_pvMemoryCopy( &rAbsData.u32CounterRearLeft,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_ABS_RL_WHEEL_CNTR],
   SEN_DISP_FIELD_SIZE_ABS_RL_WHEEL_CNTR );

   //Extract status flag
   OSAL_pvMemoryCopy( &u8Status,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_ABS_STATUS],
   SEN_DISP_FIELD_SIZE_ABS_STATUS_FLAGS );

   SenDisp_vTraceOut(TR_LEVEL_USER_4,"Received ABS status = %d",u8Status);
   //Check rear right wheel counter data validity
   if( u8Status & SEN_DISP_ABS_RR_STATUS_BIT_FIELD )
   {
      rAbsData.u8StatusRearRight = OSAL_C_U8_ABS_STATUS_DATA_INVALID;
   }
   else
   {
      rAbsData.u8StatusRearRight = OSAL_C_U8_ABS_STATUS_NORMAL;
   }
   //Check rear left wheel counter data validity
   if( u8Status & SEN_DISP_ABS_RL_STATUS_BIT_FIELD )
   {
      rAbsData.u8StatusRearLeft = OSAL_C_U8_ABS_STATUS_DATA_INVALID;
   }
   else
   {
      rAbsData.u8StatusRearLeft = OSAL_C_U8_ABS_STATUS_NORMAL;
   }
/*
  Updating the direction received from SCC to that required by the IMX

  SCC sends the following direction values to IMX
  
  Direction from SCC
  00  -- DATA INVALID
  01  -- FORWARD direction
  10  -- REVERSE direction
  11  -- UNKNOWN direction

  Handling of direction is done in the following way in IMX

  Direction data in IMX
  00  -- UNKNOWN direction
  01  -- FORWARD direction
  10  -- REVERSE direction
  11  -- DATA INVALID

  It is seen that direction data for the cases 00 and 11 are not compatible in IMX and SCC
  Therefore, direction received from SCC is converted to the values as required by the IMX
  */

   if( ( u8Status & SEN_DISP_ABS_BIT_FIELD_MASK_DRIV_DIRE_REV ) == 
         SEN_DISP_ABS_BIT_FIELD_DRIV_DIRE_FWD )
   {
      rAbsData.u8DirectionRearLeft  = OSAL_C_U8_ABS_DIR_FORWARD;
      rAbsData.u8DirectionRearRight = OSAL_C_U8_ABS_DIR_FORWARD;
   }
   else if( ( u8Status & SEN_DISP_ABS_BIT_FIELD_MASK_DRIV_DIRE_REV ) == 
         SEN_DISP_ABS_BIT_FIELD_DRIV_DIRE_REV )
   {
      rAbsData.u8DirectionRearLeft  = OSAL_C_U8_ABS_DIR_REVERSE;
      rAbsData.u8DirectionRearRight = OSAL_C_U8_ABS_DIR_REVERSE;
   }
   else if( (u8Status & SEN_DISP_ABS_BIT_FIELD_MASK_DRIV_DIRE_REV) == 
            SEN_DISP_ABS_BIT_FIELD_DRIV_DIRE_INVALID )
   {
      rAbsData.u8StatusRearLeft     = OSAL_C_U8_ABS_STATUS_DATA_INVALID;
      rAbsData.u8StatusRearRight    = OSAL_C_U8_ABS_STATUS_DATA_INVALID;
   }
   else
   {
      rAbsData.u8DirectionRearLeft  = OSAL_C_U8_ABS_DIR_UNKNOWN;
      rAbsData.u8DirectionRearRight = OSAL_C_U8_ABS_DIR_UNKNOWN;
   }

   //The below info is not available so update appropriate values indicating unavailability.
   rAbsData.u32CounterFrontLeft  = 0;
   rAbsData.u32CounterFrontRight = 0;
   rAbsData.u8DirectionFrontLeft  = OSAL_C_U8_ABS_DIR_UNKNOWN;
   rAbsData.u8DirectionFrontRight = OSAL_C_U8_ABS_DIR_UNKNOWN;
   rAbsData.u8StatusFrontLeft     = OSAL_C_U8_ABS_STATUS_UNKNOWN;
   rAbsData.u8StatusFrontRight    = OSAL_C_U8_ABS_STATUS_UNKNOWN; 
   //rAbsData.u16ErrorCounter will be updated after validating record.

   SenDisp_vTraceOut( TR_LEVEL_USER_4,
   "ABS -- Ts %u,CRR %u,SRR %u,DRR %u,CRL %u,SRL %u,DRL %u",
   rAbsData.u32TimeStamp,
   rAbsData.u32CounterRearRight,rAbsData.u8StatusRearRight,rAbsData.u8DirectionRearRight,
   rAbsData.u32CounterRearLeft,rAbsData.u8StatusRearLeft,rAbsData.u8DirectionRearLeft);

   SenDisp_vValidateAbsRecord(&rAbsData);
   if ( 0 == rAbsData.u16ErrorCounter )
   {
      /* Add to Ring buffer */
      if( 1 != SenRingBuff_s32Write( SEN_DISP_ABS_ID,(tPVoid)&rAbsData,1 ) )
      {
         SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write FAIL for ABS data");
      }
   }
}

/********************************************************************************
* FUNCTION     : vHandleSensorData 
*
* PARAMETER    : NONE
*
* RETURNVALUE  : OSAL_OK  on success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : Parses complete data record entry by entry.
*                Actual sensor data will be added to respective ring buffer.
*                Sensor temperature will be sent to respective sensor driver
*                via proper message queue.
*
* HISTORY      : 19.MAR.2013| Initial Version      |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tVoid vHandleSensorData(tVoid)
{

   tS32 s32ErrChk = OSAL_ERROR;
   tU16 u16NumSenRecords;
   tU32 u32OffsetNextRecord = SEN_DISP_OFFSET_FIRST_RECORD_IN_DATA_MSG;
   tU32 u32NumRecordsParsed;
   tU16 u16DiagMessage;
   tBool bErrOccurred = FALSE;

   if( rSensorDispInfo.u32RecvdMsgSize >= ( SEN_DISP_OFFSET_NUM_OF_ENTRIES_IN_DATA_MSG +
            SEN_DISP_FIELD_SIZE_NUM_ENTRTIES ))
   {
      /*Get the number of entries in data record.*/
      OSAL_pvMemoryCopy( &u16NumSenRecords, 
      &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_NUM_OF_ENTRIES_IN_DATA_MSG],
      SEN_DISP_FIELD_SIZE_NUM_ENTRTIES );

      SenDisp_vTraceOut( TR_LEVEL_USER_4,"Number of Entries in Data message %u", u16NumSenRecords );

      /* Parse the record entry by entry */
      for( u32NumRecordsParsed =0;
      ((u32NumRecordsParsed < u16NumSenRecords) && (FALSE == bErrOccurred));
      u32NumRecordsParsed++ )
      {
         switch( rSensorDispInfo.u8RecvBuffer[u32OffsetNextRecord + SEN_DISP_OFFSET_ENTRY_TYPE] )
         {
            /* We don't know what to do with diagnosis info. Just trace it out */
         case SEN_DISP_ENTRY_TYPE_DIAGNOSIS :
            {
               if ( rSensorDispInfo.u32RecvdMsgSize >= (u32OffsetNextRecord + MSG_SIZE_SCC_SENSOR_R_DIAG_MESSAGE -1) )
               {
                  /* Extract diagnosis message */
                  OSAL_pvMemoryCopy( &u16DiagMessage,
                  &rSensorDispInfo.u8RecvBuffer[u32OffsetNextRecord + SEN_DISP_OFFSET_DIAGNOSIS_MESSAGE],
                  SEN_DISP_FIELD_SIZE_DIAG_MESSAGE );
                  SenDisp_vTraceOut( TR_LEVEL_COMPONENT,"Diagnosis message: %x", u16DiagMessage );
                  /* Update from where next entry starts */
                  u32OffsetNextRecord += MSG_SIZE_SCC_SENSOR_R_DIAG_MESSAGE;
               }
               else
               {
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Improper Data msg:line %u", __LINE__ );
                  bErrOccurred = TRUE;
               }
               break;
            }
            /* We got Odo data. Extract and add it to ring buffer */
         case SEN_DISP_ENTRY_TYPE_ODO_DATA :
            {
               if ( rSensorDispInfo.u32RecvdMsgSize >= (u32OffsetNextRecord + MSG_SIZE_SCC_SENSOR_R_ODO_DATA -1) )
               {
                  /* Extract, validate and add data to ring buffer */
                  SenDisp_vDispatchOdoData(u32OffsetNextRecord);
                  /* Update from where next entry starts */
                  u32OffsetNextRecord += MSG_SIZE_SCC_SENSOR_R_ODO_DATA;
               }
               else
               {
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Improper Data msg:line %u", __LINE__ );
                  bErrOccurred = TRUE;
               }
               
               break;
            }
            /* We got Gyro data. Extract and add it to ring buffer */
         case SEN_DISP_ENTRY_TYPE_GYRO_DATA :
            {

               if ( rSensorDispInfo.u32RecvdMsgSize >= (u32OffsetNextRecord + MSG_SIZE_SCC_SENSOR_R_GYRO_DATA -1) )
               {
                  /* Extract, validate and add data to ring buffer */
                  SenDisp_vDispatchGyroData(u32OffsetNextRecord);
                  /* Update from where next entry starts */
                  u32OffsetNextRecord += MSG_SIZE_SCC_SENSOR_R_GYRO_DATA;
               }
               else
               {
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Improper Data msg:line %u", __LINE__ );
                  bErrOccurred = TRUE;
               }
               break;
            }

            /* We got GYRO temperature update. Send it to driver via message queue */
         case SEN_DISP_ENTRY_TYPE_GYRO_TEMP :
            {
               if ( rSensorDispInfo.u32RecvdMsgSize >= (u32OffsetNextRecord + MSG_SIZE_SCC_SENSOR_R_GYRO_TEMP -1) )
               {
                  /* Extract and dispatch Gyro temp to Gyro driver */
                  SenDisp_vDispatchGyroTemp(u32OffsetNextRecord);
                  /* Update from where next entry starts */
                  u32OffsetNextRecord += MSG_SIZE_SCC_SENSOR_R_GYRO_TEMP;
               }
               else
               {
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Improper Data msg:line %u", __LINE__ );
                  bErrOccurred = TRUE;
               }
               break;
            }
            /* We got ACC data. Extract and add it to ring buffer */
         case SEN_DISP_ENTRY_TYPE_ACC_DATA :
            {

               if ( rSensorDispInfo.u32RecvdMsgSize >= (u32OffsetNextRecord + MSG_SIZE_SCC_SENSOR_R_ACC_DATA -1) )
               {
                  /* Extract, validate and add data to ring buffer */
                  SenDisp_vDispatchAccData(u32OffsetNextRecord);
                  /* Update from where next entry starts */
                  u32OffsetNextRecord += MSG_SIZE_SCC_SENSOR_R_ACC_DATA;
               }
               else
               {
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Improper Data msg:line %u", __LINE__ );
                  bErrOccurred = TRUE;
               }
               break;
            }
            /* We got ACC temperature update. Send it to driver via message queue */
         case SEN_DISP_ENTRY_TYPE_ACC_TEMP :
            {

               if ( rSensorDispInfo.u32RecvdMsgSize >= (u32OffsetNextRecord + MSG_SIZE_SCC_SENSOR_R_ACC_TEMP -1) )
               {
                  /* Extract and dispatch Gyro temp to Gyro driver */
                  SenDisp_vDispatchAccTemp(u32OffsetNextRecord);
                  /* Update from where next entry starts */
                  u32OffsetNextRecord += MSG_SIZE_SCC_SENSOR_R_ACC_TEMP;
               }
               else
               {
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Improper Data msg:line %u", __LINE__ );
                  bErrOccurred = TRUE;
               }
               break;
            }
            /* We got ABS data. Extract and add it to ring buffer */
         case SEN_DISP_ENTRY_TYPE_ABS_DATA :
            {
               if( rSensorDispInfo.u32RecvdMsgSize >= (u32OffsetNextRecord + MSG_SIZE_SCC_SENSOR_R_ABS_DATA -1) )
               {
                  /* Extract, validate and add data to ring buffer */
                  SenDisp_vDispatchAbsData(u32OffsetNextRecord);
                  /* Update from where next entry starts */
                  u32OffsetNextRecord += MSG_SIZE_SCC_SENSOR_R_ABS_DATA;
               }
               else
               {
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Improper Data msg:line %u", __LINE__ );
                  bErrOccurred = TRUE;
               }
               break;
            }

         default:
            {
               /* If we miss even a single entry, we can't proceed further. So we have to drop the whole record. */
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, "!!! Encountered unknown Entry type" );
               bErrOccurred = TRUE;
            }
         }
      }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Improper Data msg: size %d line %u", s32ErrChk, __LINE__ );
   }
}

/********************************************************************************
* FUNCTION      : vHandleCommandReject 
*
* PARAMETER     : NONE
*
* RETURNVALUE   : NONE
*
* DESCRIPTION   : We don't know what to do for a reject. Presently we just trace it out.
*
* HISTORY       : 19.MAR.2013| Initial Version   |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tVoid vHandleCommandReject(tVoid)
{

   if( MSG_SIZE_SCC_SENSORS_R_REJECT == rSensorDispInfo.u32RecvdMsgSize )
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Command 0x%x Rejected by SCC. Reason : 0x%x ",
      rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_REJECTED_MSG_ID],
      rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_REJECT_REASON]);
      // TODO: Don't know how to handle a reject from SCC
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS," IMproper size of Reject Message %lu ", rSensorDispInfo.u32RecvdMsgSize);
   }

}

/********************************************************************************
* FUNCTION     : s32HandleSensorConfigMsg 
*
* PARAMETER    : NONE
*
* RETURNVALUE  : OSAL_OK  on success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : Config data will be received only once per connect.
*                This data is used to know which devices are active in the project.
*                Further this information has to be passed to drivers if devices are active.
*
* HISTORY     :19.MAR.2013| Initial Version    |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tS32 s32HandleSensorConfigMsg(tVoid)
{

   trOdoConfigData  rOdoConfigData;
   trGyroConfigData rGyroConfigData;
   trAccConfigData  rAccConfigData;
   trAbsConfigData  rAbsConfigData = {0};
   tS32 s32RetVal = OSAL_OK;
   trSenRingBuffConfig rSenRingBuffConfig;


   if ( MSG_SIZE_SCC_SENSORS_R_CONFIG == rSensorDispInfo.u32RecvdMsgSize )
   {

      /* Handle Odo CONFIG */
      rOdoConfigData.u8MsgType = SEN_DISP_MSG_TYPE_CONFIG;
      /* Extract Odo data interval */
      OSAL_pvMemoryCopy( &rOdoConfigData.u16OdoDataInterval,
      &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ODO_CONFIG_DATA_INTERVAL],
      SEN_DISP_FIELD_SIZE_CONFIG_SENSOR_DATA_INTERVALL );
      
      
      SenDisp_vTraceOut(TR_LEVEL_USER_4,"Odo config  interval : %u ",
      rOdoConfigData.u16OdoDataInterval);

      /* If data interval is 0, then device is not used. Don't create ring buff for this device */
      if( SEN_DISP_DEVICE_NOT_USED != rOdoConfigData.u16OdoDataInterval )
      {
         /* Fill in the data needed to create ring buffer */
         rSenRingBuffConfig.u32DataTimeout = SEN_DISP_ODO_TIMEOUT;
         rSenRingBuffConfig.u32MaxRecords = SEN_DISP_MAX_RINGBUFF_ODO_RECORDS;
         rSenRingBuffConfig.u32SizeOfEachRecord = sizeof( OSAL_trIOCtrlOdometerData );
         /* Initialize ring buffer */
         if( OSAL_OK != SenRingBuff_s32Init( SEN_DISP_ODO_ID,
                  &rSenRingBuffConfig ))
         {      
            SenDisp_vTraceOut(TR_LEVEL_FATAL,"sen_Disp:SenRingBuff_s32Init Failed for Odo" );
            s32RetVal = OSAL_ERROR;
         }
         /* Send configuration information to driver  */
         else if( OSAL_ERROR == OSAL_s32MessageQueuePost( rSensorDispInfo.rOdoInfo.hldOdoMsgQue,
                  (tPCU8)&rOdoConfigData,
                  sizeof(rOdoConfigData),
                  SEN_DISP_MSGQUE_PRIO) )
         {
            SenDisp_vTraceOut(TR_LEVEL_FATAL,"sen_Disp:ODO MsgQ Post Failed OSAL_ERR %u ", OSAL_u32ErrorCode());
            s32RetVal = OSAL_ERROR;
         }
         else
         {
            /* Device should be moved from not initialized to exists state only if everything goes well */
            rSensorDispInfo.rOdoInfo.enOdoSts = DEVICE_EXISTS;
         }

      }
      else
      {
         /* We don't use this device in current project. So delete message queue*/
         rSensorDispInfo.rOdoInfo.enOdoSts = DEVICE_NOT_USED;
         SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ODO_MSGQUE);
      }

      if( OSAL_OK == s32RetVal )
      {
         /* Handle Gyro CONFIG */

         rGyroConfigData.u8MsgType = SEN_DISP_MSG_TYPE_CONFIG;
         
         /* Extract GYRO data interval */
         OSAL_pvMemoryCopy( &rGyroConfigData.u16GyroDataInterval,
         &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_GYRO_CONFIG_DATA_INTERVAL],
         SEN_DISP_FIELD_SIZE_CONFIG_SENSOR_DATA_INTERVALL );
         
         /* Extract GYRO type info */
         OSAL_pvMemoryCopy( &rGyroConfigData.u8GyroType,
         &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_GYRO_CONFIG_TYPE],
         SEN_DISP_FIELD_SIZE_CONFIG_SENSOR_TYPE );

         SenDisp_vTraceOut(TR_LEVEL_USER_4,"config GYRO type: %u  interval : %u ",
         rGyroConfigData.u8GyroType, rGyroConfigData.u16GyroDataInterval);

         /* If data interval is 0, then device is not used. Don't create ring buff for this device */
         if( SEN_DISP_DEVICE_NOT_USED != rGyroConfigData.u16GyroDataInterval )
         {
            /* Fill in the data needed to create ring buffer */
            rSenRingBuffConfig.u32DataTimeout = rGyroConfigData.u16GyroDataInterval * SEN_DISP_RINGBUFF_TIMEOUT_MUL_FACTOR ;
            rSenRingBuffConfig.u32MaxRecords = SEN_DISP_MAX_RINGBUFF_GYRO_RECORDS;
            rSenRingBuffConfig.u32SizeOfEachRecord = sizeof( OSAL_trIOCtrl3dGyroData );
            /* Initialize ring buffer */
            if( OSAL_OK != SenRingBuff_s32Init( SEN_DISP_GYRO_ID,
                     &rSenRingBuffConfig ))
            {      
               SenDisp_vTraceOut(TR_LEVEL_FATAL,"sen_Disp:SenRingBuff_s32Init Failed for Gyro" );
               s32RetVal = OSAL_ERROR;
            }   
            /* Send configuration information to driver  */
            else if( OSAL_ERROR == OSAL_s32MessageQueuePost( rSensorDispInfo.rGyroInfo.hldGyroMsgQue,
                     (tPCU8)&rGyroConfigData,
                     sizeof(rGyroConfigData),
                     SEN_DISP_MSGQUE_PRIO)  )
            {
               SenDisp_vTraceOut(TR_LEVEL_FATAL,"sen_Disp:Gyro MsgQ Post Failed OSAL_ERR %u ", OSAL_u32ErrorCode());
               s32RetVal = OSAL_ERROR;
            }
            else
            {
               /* Device should be moved from not initialized to exists state only if everything goes well */
               rSensorDispInfo.rGyroInfo.enGyroSts = DEVICE_EXISTS;
            }
            
         }
         else
         {
            /* We don't use this device in current project. So delete message queue*/
            rSensorDispInfo.rGyroInfo.enGyroSts = DEVICE_NOT_USED;
            SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_GYRO_MSGQUE);
         }

      }

      if( OSAL_ERROR != s32RetVal )
      {
         /* Handle ACC CONFIG */
         
         rAccConfigData.u8MsgType = SEN_DISP_MSG_TYPE_CONFIG;
         
         /* Extract ACC data interval */
         OSAL_pvMemoryCopy( &rAccConfigData.u16AccDataInterval,
         &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ACC_CONFIG_DATA_INTERVAL],
         SEN_DISP_FIELD_SIZE_CONFIG_SENSOR_DATA_INTERVALL );
         
         /* Extract ACC type info */
         OSAL_pvMemoryCopy( &rAccConfigData.u8AccType,
         &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ACC_CONFIG_TYPE],
         SEN_DISP_FIELD_SIZE_CONFIG_SENSOR_TYPE );

         SenDisp_vTraceOut(TR_LEVEL_USER_4,"config ACC type: %u  interval : %u ",
         rAccConfigData.u8AccType, rAccConfigData.u16AccDataInterval);

         /* If data interval is 0, then device is not used. Don't create ring buff for this device */
         if( SEN_DISP_DEVICE_NOT_USED != rAccConfigData.u16AccDataInterval )
         {
            /* Fill in the data needed to create ring buffer */
            rSenRingBuffConfig.u32DataTimeout = rAccConfigData.u16AccDataInterval * SEN_DISP_RINGBUFF_TIMEOUT_MUL_FACTOR ;
            rSenRingBuffConfig.u32MaxRecords = SEN_DISP_MAX_RINGBUFF_ACC_RECORDS;
            rSenRingBuffConfig.u32SizeOfEachRecord = sizeof( OSAL_trIOCtrlAccData );
            
            if( OSAL_OK != SenRingBuff_s32Init( SEN_DISP_ACC_ID,
                     &rSenRingBuffConfig ))
            {      
               SenDisp_vTraceOut(TR_LEVEL_FATAL,"sen_Disp:SenRingBuff_s32Init Failed for ACC" );
               s32RetVal = OSAL_ERROR;
            }
            else if( OSAL_ERROR == OSAL_s32MessageQueuePost( rSensorDispInfo.rAccInfo.hldAccMsgQue,
                     (tPCU8)&rAccConfigData,
                     sizeof(rAccConfigData),
                     SEN_DISP_MSGQUE_PRIO) )
            {
               SenDisp_vTraceOut( TR_LEVEL_FATAL,"sen_Disp:ACC MsgQ Post Failed OSAL_ERR %u ", OSAL_u32ErrorCode() );
               s32RetVal = OSAL_ERROR;
            }
            else
            {
               /* Device should be moved from not initialized to exists state only if everything goes well */
               rSensorDispInfo.rAccInfo.enAccSts = DEVICE_EXISTS;
            }
         }
         else
         {
            /* We don't use this device in current project. So delete message queue*/
            rSensorDispInfo.rAccInfo.enAccSts = DEVICE_NOT_USED;
            SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ACC_MSGQUE);
         }
      }

      if( OSAL_ERROR != s32RetVal )
      {
         /* Handle ABS CONFIG */
         rAbsConfigData.u8MsgType = SEN_DISP_MSG_TYPE_CONFIG;

         /* Extract ABS data interval */
         OSAL_pvMemoryCopy( &rAbsConfigData.u16AbsDataInterval,
         &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_ABS_CONFIG_DATA_INTERVAL],
         SEN_DISP_FIELD_SIZE_CONFIG_SENSOR_DATA_INTERVALL );

         SenDisp_vTraceOut(TR_LEVEL_USER_4,"config ABS interval : %u ",
         rAbsConfigData.u16AbsDataInterval);

         /* If data interval is 0, then device is not used. Don't create ring buff for this device */
         if( SEN_DISP_DEVICE_NOT_USED != rAbsConfigData.u16AbsDataInterval )
         {
            /* Fill in the data needed to create ring buffer */
            rSenRingBuffConfig.u32DataTimeout = rAbsConfigData.u16AbsDataInterval * SEN_DISP_RINGBUFF_TIMEOUT_MUL_FACTOR;
            rSenRingBuffConfig.u32MaxRecords = SEN_DISP_MAX_RINGBUFF_ABS_RECORDS;
            rSenRingBuffConfig.u32SizeOfEachRecord = sizeof( OSAL_trAbsData );

            if( OSAL_OK != SenRingBuff_s32Init( SEN_DISP_ABS_ID,&rSenRingBuffConfig ) )
            {
               SenDisp_vTraceOut(TR_LEVEL_FATAL,"sen_Disp:sen_Disp:SenRingBuff_s32Init Failed for ABS" );
               s32RetVal = OSAL_ERROR;
            }
            else if( OSAL_ERROR == OSAL_s32MessageQueuePost( rSensorDispInfo.rAbsInfo.hldAbsMsgQue,
                     (tPCU8)&rAbsConfigData,
                     sizeof(rAbsConfigData),
                     SEN_DISP_MSGQUE_PRIO) )
            {
               SenDisp_vTraceOut( TR_LEVEL_FATAL,"sen_Disp:ABS MsgQ Post Failed OSAL_ERR %u ", OSAL_u32ErrorCode() );
               s32RetVal = OSAL_ERROR;
            }
            else
            {
               /* Device should be moved from not initialized to exists state only if everything goes well */
               rSensorDispInfo.rAbsInfo.enAbsSts = DEVICE_EXISTS;
            }
         }
         else
         {
            /* We don't use this device in current project. So delete message queue*/
            rSensorDispInfo.rAbsInfo.enAbsSts = DEVICE_NOT_USED;
            SenDisp_vReleaseResourse(SEN_DISP_RESOURCE_RELSEASE_ABS_MSGQUE);
         }
      }
   }
   else
   {
      SenDisp_vTraceOut(TR_LEVEL_FATAL,"sen_Disp:MSG_SIZE_SCC_SENSORS_R_CONFIG Invalid Size : %u",
      rSensorDispInfo.u32RecvdMsgSize);
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}

/********************************************************************************
* FUNCTION    : s32SensorDiapatcherDeInit 
*
* PARAMETER   : NONE
*
* RETURNVALUE : OSAL_OK  on success
*               OSAL_ERROR on Failure
*
* DESCRIPTION : This is De-initialization function for Sensor Dispatcher. 
*               This will be called from different sensor drivers. 
*               1: Update the driver status as closed
*               2: If nobody is using the device, trigger dispatcher component shutdown.
*
* HISTORY    : 19.MAR.2013| Initial Version    | Madhu Kiran Ramachandra (RBEI/ECF5)
*              10.FEB.2016| Version: 1.1       | Srinivas Prakash Anvekar (RBEI/ECF5)
*                         |                    | CFG3-1729 : Send inactive status to 
*                         |                    | to SCC during Shutdown and also switch 
*                         |                    | to buffering mode.
**********************************************************************************/

tS32 s32SensorDiapatcherDeInit( tEnSenDrvID enSenID )
{
   tS32 s32RetVal = OSAL_OK;
   tS32 s32ErrorVal=0;

   //To satisfy lint
   (tVoid)s32RetVal;
   (tVoid)s32ErrorVal;
   SenDisp_vTraceOut( TR_LEVEL_USER_4,"s32SensorDiapatcherDeInit called from %d", enSenID );

   /* It is possible that you get open from a driver and close concurrently.
      So better take semaphore even though it is not a good idea to take semaphore in de-init*/
   if ( OSAL_OK == OSAL_s32SemaphoreWait( rSensorDispInfo.HldInitSem ,
            (OSAL_tMSecond) SEN_DISP_DEINIT_SEM_WAIT_TIME ))
   {
      /* Update who called close */
      switch ( enSenID )
      {
      case SEN_DISP_ODO_ID :
         {
            if( DEVICE_OPENED == rSensorDispInfo.rOdoInfo.enOdoSts )
            {
               rSensorDispInfo.rOdoInfo.enOdoSts = DEVICE_CLOSED;
            }
            else
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Current Odo status %d Close is not expected",
               rSensorDispInfo.rOdoInfo.enOdoSts );
               s32RetVal = OSAL_ERROR;
            }
            break;
         }
      case SEN_DISP_GYRO_ID :
         {
            if( DEVICE_OPENED == rSensorDispInfo.rGyroInfo.enGyroSts)
            {
               rSensorDispInfo.rGyroInfo.enGyroSts = DEVICE_CLOSED;
            }
            else
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Current Gyro status %d Close is not expected",
               rSensorDispInfo.rGyroInfo.enGyroSts );
               s32RetVal = OSAL_ERROR;
            }
            break;
         }
      case SEN_DISP_ACC_ID :
         {
            if( DEVICE_OPENED == rSensorDispInfo.rAccInfo.enAccSts )
            {
               rSensorDispInfo.rAccInfo.enAccSts = DEVICE_CLOSED;
               s32RetVal = OSAL_OK;
            }
            else
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Current ACC status %d Close is not expected",
               rSensorDispInfo.rAccInfo.enAccSts );
               s32RetVal = OSAL_ERROR;
            }
            break;
         }
      case SEN_DISP_ABS_ID :
         {
            if( DEVICE_OPENED == rSensorDispInfo.rAbsInfo.enAbsSts )
            {
               rSensorDispInfo.rAbsInfo.enAbsSts = DEVICE_CLOSED;
               s32RetVal = OSAL_OK;
            }
            else
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Current ABS status %d Close is not expected",
               rSensorDispInfo.rAbsInfo.enAbsSts );
               s32RetVal = OSAL_ERROR;
            }
            break;
         }
      default:
         {
            SenDisp_vTraceOut( TR_LEVEL_ERRORS,"default line %d unknown Device ID %d", __LINE__, enSenID);
            s32RetVal = OSAL_ERROR;
            break;
         }
      }
      

      if( (( DEVICE_NOT_USED == rSensorDispInfo.rOdoInfo.enOdoSts )  || ( DEVICE_CLOSED == rSensorDispInfo.rOdoInfo.enOdoSts ))   &&
            (( DEVICE_NOT_USED == rSensorDispInfo.rGyroInfo.enGyroSts) || ( DEVICE_CLOSED == rSensorDispInfo.rGyroInfo.enGyroSts )) &&
            (( DEVICE_NOT_USED == rSensorDispInfo.rAccInfo.enAccSts)   || ( DEVICE_CLOSED == rSensorDispInfo.rAccInfo.enAccSts ))   &&
            (( DEVICE_NOT_USED == rSensorDispInfo.rAbsInfo.enAbsSts)   || ( DEVICE_CLOSED == rSensorDispInfo.rAbsInfo.enAbsSts )) )
      {
         /* Send Inactive Status to SCC. This should switch SCC to data buffering mode */
         if( OSAL_OK != SenDisp_s32SendStatusCmd( HOST_SENSOR_COMPONENT_STATUS_NOT_ACTIVE ) )
         {
            SenDisp_vTraceOut( TR_LEVEL_FATAL, "Sen_Disp: Status sending failed during shutdown");
         }
         SenDisp_vTraceOut( TR_LEVEL_SYSTEM, " All sensors are closed. Shutdown Flag is set ");
         rSensorDispInfo.bShutdownFlag = TRUE;
      }

      /* Release Semaphore */
      if (OSAL_OK != OSAL_s32SemaphorePost( rSensorDispInfo.HldInitSem ))
      {
         s32RetVal = OSAL_ERROR;
         SenDisp_vTraceOut(TR_LEVEL_ERRORS, "Sem post Failed line %d Err %u", __LINE__, OSAL_u32ErrorCode() );
      }

      if ( TRUE == rSensorDispInfo.bShutdownFlag )
      {
         //Posting shutdown event to third party INC forwarder
         Inc2Soc_vPostDrivShutdown( INC2SOC_DEV_TYPE_POS );

         /* We have only one thread presently in dispatcher. This thread will be accessing all the resources of dispatcher.
         So inform him that shutdown is about to occur. This can be done by setting shutdown flag and closing the socket.
         closing the socket will unblock the thread. i.e read will return failure. */

         //Wait for the SenDisp_pvSocketReadThread thread to exit
         SenDisp_vTraceOut( TR_LEVEL_USER_4, "Sen_disp: Waiting for "
                            "SenDisp_pvSocketReadThread to exit" );
         s32ErrorVal = OSAL_s32ThreadJoin( tid_SenDispSockRd, SEN_DISP_INIT_EVENT_WAIT_TIME );
         if( s32ErrorVal != 0)
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Sen_disp: pthread_join on"
                               " SenDisp_pvSocketReadThread returned error: %d", 
                              s32ErrorVal);
      }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_USER_4,"Sem wait failed line %d Err %lu",__LINE__, OSAL_u32ErrorCode() );
      s32RetVal = OSAL_ERROR;
   }

   return s32RetVal;
}

/********************************************************************************
* FUNCTION     : SenDisp_vReleaseResourse 
*
* PARAMETER    : tU32 u32Resource: which resource to be released.
*
* RETURNVALUE  : OSAL_OK  on success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : This function is used to make the process or releasing a resource easier.
*
* HISTORY     : 19.MAR.2013| Initial Version    |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

static tVoid SenDisp_vReleaseResourse(tU32 u32Resource)
{
   SenDisp_vTraceOut( TR_LEVEL_USER_4, "Sen_Disp: Release Resource: %lu", u32Resource );

   switch (u32Resource)
   {
      /* Odometer message queue  */
   case SEN_DISP_RESOURCE_RELSEASE_ODO_MSGQUE :
      {
         if( OSAL_NULL != rSensorDispInfo.rOdoInfo.hldOdoMsgQue )
         {
            if(OSAL_OK != OSAL_s32MessageQueueClose( rSensorDispInfo.rOdoInfo.hldOdoMsgQue ))
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS," ODO MsgQ close Failed" );
            }
            else if ( OSAL_OK != OSAL_s32MessageQueueDelete( SEN_DISP_TO_ODO_MSGQUE_NAME) )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS," ODO OSAL_s32MessageQueueDelete Failed" );
            }
            rSensorDispInfo.rOdoInfo.hldOdoMsgQue = OSAL_NULL;
         }
         break;
      }
      /* Gyro message queue  */
   case SEN_DISP_RESOURCE_RELSEASE_GYRO_MSGQUE :
      {
         if( OSAL_NULL != rSensorDispInfo.rGyroInfo.hldGyroMsgQue )
         {
            if(OSAL_OK != OSAL_s32MessageQueueClose( rSensorDispInfo.rGyroInfo.hldGyroMsgQue ))
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS," GYRO msgQ close Failed" );
            }
            else if ( OSAL_OK != OSAL_s32MessageQueueDelete( SEN_DISP_TO_GYRO_MSGQUE_NAME) )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS," GYRO OSAL_s32MessageQueueDelete Failed" );
            }
            rSensorDispInfo.rGyroInfo.hldGyroMsgQue = OSAL_NULL;
         }
         break;
      }
      /* ACC message queue  */
   case SEN_DISP_RESOURCE_RELSEASE_ACC_MSGQUE :
      {
         if( OSAL_NULL != rSensorDispInfo.rAccInfo.hldAccMsgQue )
         {
            if(OSAL_OK != OSAL_s32MessageQueueClose( rSensorDispInfo.rAccInfo.hldAccMsgQue ))
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS," ACC MsgQ close Failed" );
            }
            else if ( OSAL_OK != OSAL_s32MessageQueueDelete( SEN_DISP_TO_ACC_MSGQUE_NAME) )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS," ACC OSAL_s32MessageQueueDelete Failed" );
            }
            rSensorDispInfo.rAccInfo.hldAccMsgQue = OSAL_NULL;
         }
         break;
      }
      /* ABS message queue  */
   case SEN_DISP_RESOURCE_RELSEASE_ABS_MSGQUE :
      {
         if( OSAL_NULL != rSensorDispInfo.rAbsInfo.hldAbsMsgQue )
         {
            if(OSAL_OK != OSAL_s32MessageQueueClose( rSensorDispInfo.rAbsInfo.hldAbsMsgQue ))
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS," ABS MsgQ close Failed" );
            }
            else if ( OSAL_OK != OSAL_s32MessageQueueDelete( SEN_DISP_TO_ABS_MSGQUE_NAME) )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS,"ABS OSAL_s32MessageQueueDelete Failed" );
            }
            rSensorDispInfo.rAbsInfo.hldAbsMsgQue = OSAL_NULL;
         }
         break;
      }

      /* Release socket  */
   case SEN_DISP_RESOURCE_RELSEASE_SOCKET :
      {
         if( OSAL_NULL != rSensorDispInfo.s32SocketFD )
         {
            if ( 0 != close( rSensorDispInfo.s32SocketFD ) )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS,"socket close Failed" );
            }
            else
            {
               SenDisp_vTraceOut( TR_LEVEL_USER_4,"SOCKET closed successfully" );
            }
            rSensorDispInfo.s32SocketFD = OSAL_NULL;
         }
         break;
      }
      /* De-initialize ring buffer if used */
   case SEN_DISP_RESOURCE_RELSEASE_RINGBUFF:
      {
         if( DEVICE_NOT_USED != rSensorDispInfo.rOdoInfo.enOdoSts ) 
         
         {
            if ( OSAL_OK != SenRingBuff_s32DeInit(SEN_DISP_ODO_ID) )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Ring buff De-init failed for ODO" );
            }
         }
         if( DEVICE_NOT_USED != rSensorDispInfo.rGyroInfo.enGyroSts ) 
         
         {
            if ( OSAL_OK != SenRingBuff_s32DeInit(SEN_DISP_GYRO_ID) )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Ring buff De-init failed for GYRO" );
            }
         }
         if( DEVICE_NOT_USED != rSensorDispInfo.rAccInfo.enAccSts ) 
         
         {
            if ( OSAL_OK != SenRingBuff_s32DeInit(SEN_DISP_ACC_ID) )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Ring buff De-init failed for ACC" );
            }
         }

         if( DEVICE_NOT_USED != rSensorDispInfo.rAbsInfo.enAbsSts )
         {
            if ( OSAL_OK != SenRingBuff_s32DeInit(SEN_DISP_ABS_ID) )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS,"Ring buff De-init failed for ABS" );
            }
         }
         break;
      }
      /* Release initialize/De-initialize semaphore */
   case SEN_DISP_RESOURCE_RELSEASE_INIT_SEM:
      {
         if(OSAL_NULL != rSensorDispInfo.HldInitSem )
         {
            if( OSAL_OK != OSAL_s32SemaphoreClose( rSensorDispInfo.HldInitSem ))
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, "DISP INIT SEM close failed err %lu", OSAL_u32ErrorCode() );
            }
            else if( OSAL_OK != OSAL_s32SemaphoreDelete( SEN_DISP_INIT_DEINT_SEM_NAME ))
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, "DISP INIT SEM Delete failed err %lu", OSAL_u32ErrorCode() );
            }
            rSensorDispInfo.HldInitSem = OSAL_NULL;
         }
         break;
      }
   case SEN_DISP_RESOURCE_RELEASE_INIT_EVENT:
      {
         if( rSensorDispInfo.hSenDispInitEvent != OSAL_C_INVALID_HANDLE )
         {
            if( OSAL_OK != OSAL_s32EventClose( rSensorDispInfo.hSenDispInitEvent ))
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Init Event close failed err %lu",
               OSAL_u32ErrorCode() );
            }
            else if( OSAL_OK != OSAL_s32EventDelete( SEN_DISP_INIT_EVENT_NAME ))
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Init Event Delete failed err %lu",
               OSAL_u32ErrorCode() );
               rSensorDispInfo.hSenDispInitEvent = OSAL_C_INVALID_HANDLE;
            }
            else
            {
               rSensorDispInfo.hSenDispInitEvent = OSAL_C_INVALID_HANDLE;;
            }
         }
         break;
      }
      default :
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS,"default @ line %d", __LINE__ );
         break;
      }
   }
}

/********************************************************************************
* FUNCTION     : s32InitCommunToSCC 
*
* PARAMETER    : NONE
*
* RETURNVALUE  : OSAL_OK  on success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : This does all the initial communication with SCC
*                1: Status exchange
*                2: Receive configuration message.
*                3: Process configuration message
* HISTORY      :
* -------------------------------------------------------------------------------------------
* 19.MAR.2013| Initial Version      |Madhu Kiran Ramachandra (RBEI/ECF5)
* -------------------------------------------------------------------------------------------
* 09.JUL.2014|  version 1.1         | Madhu Kiran Ramachandra (RBEI/ECF5)
*            |                      | Added ability to discard incorrect responses from V850.
* --------------------------------------------------------------------------------------------
**********************************************************************************/
static tS32 s32InitCommunToSCC()
{
   tS32 s32RetVal = OSAL_ERROR;
   tU32 u32RetryCnt = 0;
   tBool bRetrySts = FALSE;

#ifndef SENSOR_PROXY_TEST_STUB_ACTIVE

   rSensorDispInfo.hldSocDgram = dgram_init( rSensorDispInfo.s32SocketFD,
   SEN_DISP_MAX_PACKET_SIZE,
   OSAL_NULL);
   
   if( rSensorDispInfo.hldSocDgram == OSAL_NULL )
   {
      SenDisp_vTraceOut(TR_LEVEL_FATAL, "Sen_disp:dgram_init Failed ");
   }
   /* Send status message to V850 */
   else
#endif
   if( OSAL_ERROR == SenDisp_s32SendStatusCmd(HOST_SENSOR_COMPONENT_STATUS_ACTIVE) )
   {
      SenDisp_vTraceOut(TR_LEVEL_FATAL,"Sen_disp:Unable to send IMX status to SCC");
   }
   else
   {
      do
      {
         /* Wait for status message of V850 */
#ifndef SENSOR_PROXY_TEST_STUB_ACTIVE
         s32RetVal = SensorProxy_s32GetDataFromScc(SEN_DISP_MAX_PACKET_SIZE);
#else
         s32RetVal = dgram_recv( rSensorDispInfo.s32SocketFD,
         rSensorDispInfo.u8RecvBuffer,
         SEN_DISP_MAX_PACKET_SIZE);
#endif
         if( s32RetVal > 0 )
         {
            bRetrySts = FALSE;
            rSensorDispInfo.u32RecvdMsgSize = (tU32)s32RetVal;
            
            /* Check if we got status message */
            if ( ( MSG_ID_SCC_SENSORS_R_COMPONENT_STATUS != rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID] ) && 
                  ( MSG_ID_SCC_SENSORS_R_REJECT != rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID] ) )
            {
               SenDisp_vTraceOut(TR_LEVEL_FATAL, "Sen_disp:Expected Msg ID : 0x%x, but received 0x%x", 
               MSG_ID_SCC_SENSORS_R_COMPONENT_STATUS, rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID] );
               
               s32RetVal = OSAL_ERROR;
               bRetrySts = TRUE;
            } 
            
            /* check if we got reject message*/
            else if( MSG_ID_SCC_SENSORS_R_REJECT == rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID] )
            {
               SenDisp_vTraceOut( TR_LEVEL_FATAL,"Sen_disp: Command 0x%x Rejected by SCC. Reason : 0x%x ",
               rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_REJECTED_MSG_ID],
               rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_REJECT_REASON]);
               
               s32RetVal = OSAL_ERROR;
            }
            
            else if ( OSAL_OK != s32HandleStatusMsg() )
            {
               s32RetVal = OSAL_ERROR;
            }
            
            else
            {
               /* Wait for configuration message from V850 */
#ifndef SENSOR_PROXY_TEST_STUB_ACTIVE
               s32RetVal = dgram_recv( rSensorDispInfo.hldSocDgram,
               (void*)rSensorDispInfo.u8RecvBuffer,
               (size_t)SEN_DISP_MAX_PACKET_SIZE);
#else
               s32RetVal = dgram_recv( rSensorDispInfo.s32SocketFD,
               rSensorDispInfo.u8RecvBuffer,
               SEN_DISP_MAX_PACKET_SIZE);
#endif
               //  TODO: Implement retry for config message
               if( s32RetVal > 0 )
               {
                  rSensorDispInfo.u32RecvdMsgSize = (tU32)s32RetVal;
                  /* Check if we got Config message as expected */
                  if ( MSG_ID_SCC_SENSORS_R_CONFIG != rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID] )
                  {
                     SenDisp_vTraceOut(TR_LEVEL_FATAL, "Sen_disp:Expected Config message but Got msg ID 0x%x",
                     rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID] );
                     s32RetVal = OSAL_ERROR;
                  }
                  else
                  {
                     /* Process the config message */
                     s32RetVal = s32HandleSensorConfigMsg();
                     if ( OSAL_OK == s32RetVal )
                     {
                        SenDisp_vTraceOut(TR_LEVEL_USER_4,"Initial Com Successful with SCC");
                     }
                  }
               }
               else
               {
                  SenDisp_vTraceOut(TR_LEVEL_USER_4," dgram_recv Failed Line %d", __LINE__);
                  s32RetVal = OSAL_ERROR;
               }
            }  
         }
         else
         {
            SenDisp_vTraceOut(TR_LEVEL_USER_4," dgram_recv Failed Line %d", __LINE__);
            s32RetVal = OSAL_ERROR;
         }
      }while ( ( u32RetryCnt++ < SEN_DISP_MAX_INCORRECT_RES_FRM_V850 ) && 
      ( TRUE == bRetrySts ) );
   }
   return s32RetVal;
}
/********************************************************************************
* FUNCTION     : SenDisp_vValidateOdoRecord
*
* PARAMETER    : OSAL_trIOCtrlOdometerData * : POinter to Odo Data
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This is a filter to detect corrupt Odo data
* HISTORY      : 18.NOV.2013| Initial Version    |Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
**********************************************************************************/
static tVoid SenDisp_vValidateOdoRecord(OSAL_trIOCtrlOdometerData *rOdoCurData)
{

   tS32 s32DataDiff;

   if( OSAL_NULL == rOdoCurData )
   {
      SenDisp_vTraceOut ( TR_LEVEL_FATAL, "NULL pointer line: %lu", __LINE__ );
   }
   else
   {
      /* Set Default values */
      rOdoCurData->u16ErrorCounter = 0;

      s32DataDiff = (tS32)( rOdoCurData->u32TimeStamp - rSensorDispInfo.rOdoInfo.rOdoPrevData.u32TimeStamp);

      /* Store data for next comparison and validation */
      rSensorDispInfo.rOdoInfo.rOdoPrevData.u32TimeStamp = rOdoCurData->u32TimeStamp;

      if ( TRUE == rSensorDispInfo.rOdoInfo.bIsFirstRecord )
      {
         rSensorDispInfo.rOdoInfo.bIsFirstRecord = FALSE;
      }
      /* Validate time stamp */
      else if( OSAL_s32Absolute(s32DataDiff) > SEN_DISP_MAX_TIME_STAMP_DIFFERENCE )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Invalid Odo time stamp diff: %d",
         OSAL_s32Absolute(s32DataDiff) );
         rOdoCurData->u16ErrorCounter = 1;
      }
      /* We don't have a plan to validate odometer data as of now. By can be configured if needed in future. */
#if 0
      s32DataDiff = (tS32) (rOdoCurData->u32WheelCounter - rSensorDispInfo.rOdoInfo.rOdoPrevData.u32WheelCounter);
      if( s32DataDiff > SEN_DISP_MAX_ODO_DATA_DIFFERENCE )
      {
         rOdoCurData->u16ErrorCounter = 1;
         SenDisp_vTraceOut( TR_LEVEL_FATAL, "Invalid Odo cntr diff: %d",
         s32DataDiff );
      }
      else
      {
         rSensorDispInfo.rOdoInfo.rOdoPrevData.u32WheelCounter = rOdoCurData->u32WheelCounter;
      }
#endif

      if(0 == rOdoCurData->u16ErrorCounter )
      {
         rOdoCurData->u16OdoMsgCounter++;
      }
   }
}
/********************************************************************************
* FUNCTION     : SenDisp_vValidateGyroRecord
*
* PARAMETER    : OSAL_trIOCtrl3dGyroData * : POinter to Gyro Data
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This is a filter to detect corrupt Gyro data
* HISTORY      : 18.NOV.2013| Initial Version    |Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
**********************************************************************************/
static tVoid SenDisp_vValidateGyroRecord(OSAL_trIOCtrl3dGyroData *rGyroCurData)
{
   tS32 s32DataDiff;

   if( OSAL_NULL == rGyroCurData )
   {
      SenDisp_vTraceOut ( TR_LEVEL_FATAL, "NULL pointer line: %lu", __LINE__ );
   }
   else
   {
      /* Set Default values */
      rGyroCurData->u16ErrorCounter = 0;

      s32DataDiff = (tS32)( rGyroCurData->u32TimeStamp - rSensorDispInfo.rGyroInfo.rGyroPrevData.u32TimeStamp);

      if ( TRUE == rSensorDispInfo.rGyroInfo.bIsFirstRecord )
      {
         rSensorDispInfo.rGyroInfo.bIsFirstRecord = FALSE;
      }
      else
      {
         /* Validate time stamp */
         if( OSAL_s32Absolute(s32DataDiff) > SEN_DISP_MAX_TIME_STAMP_DIFFERENCE )
         {
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Invalid Gyro time stamp diff: %d",
            OSAL_s32Absolute(s32DataDiff) );
            rGyroCurData->u16ErrorCounter = 1;
         }

         s32DataDiff = (tS32) ( rGyroCurData->u16Gyro_r -
         rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_r );

         if( OSAL_s32Absolute(s32DataDiff) >= SEN_DISP_MAX_GYRO_DATA_DIFFERENCE )
         {
            rGyroCurData->u16ErrorCounter = 1;
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Invalid Gyro-R data diff: %d",
            OSAL_s32Absolute(s32DataDiff) );
         }

         s32DataDiff = (tS32) ( rGyroCurData->u16Gyro_s -
         rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_s );

         if( OSAL_s32Absolute(s32DataDiff) > SEN_DISP_MAX_GYRO_DATA_DIFFERENCE )
         {
            rGyroCurData->u16ErrorCounter = 1;
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Invalid Gyro-s data diff: %d",
            OSAL_s32Absolute(s32DataDiff) );
         }

         s32DataDiff = (tS32) ( rGyroCurData->u16Gyro_t -
         rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_t );

         if( OSAL_s32Absolute(s32DataDiff) > SEN_DISP_MAX_GYRO_DATA_DIFFERENCE )
         {
            rGyroCurData->u16ErrorCounter = 1;
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Invalid Gyro-t data diff: %d",
            OSAL_s32Absolute(s32DataDiff) );
         }
      }

      /* Store data for next comparison and validation */
      rSensorDispInfo.rGyroInfo.rGyroPrevData.u32TimeStamp = rGyroCurData->u32TimeStamp;
      rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_r = rGyroCurData->u16Gyro_r;
      rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_s = rGyroCurData->u16Gyro_s;
      rSensorDispInfo.rGyroInfo.rGyroPrevData.u16Gyro_t = rGyroCurData->u16Gyro_t;
   }
}
/********************************************************************************
* FUNCTION     : SenDisp_vValidateAccRecord
*
* PARAMETER    : OSAL_trIOCtrlAccData * : POinter to Acc Data
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This is a filter to detect corrupt Acc data
* HISTORY      : 18.NOV.2013| Initial Version    |Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
**********************************************************************************/
static tVoid SenDisp_vValidateAccRecord(OSAL_trIOCtrlAccData *rAccCurData)
{
   tS32 s32DataDiff;

   if( OSAL_NULL == rAccCurData )
   {
      SenDisp_vTraceOut ( TR_LEVEL_FATAL, "NULL pointer line: %lu", __LINE__ );
   }
   else
   {
      /* Set Default values */
      rAccCurData->u16ErrorCounter = 0;

      s32DataDiff = (tS32)( rAccCurData->u32TimeStamp - rSensorDispInfo.rAccInfo.rAccPrevData.u32TimeStamp);

      if ( TRUE == rSensorDispInfo.rAccInfo.bIsFirstRecord )
      {
         rSensorDispInfo.rAccInfo.bIsFirstRecord = FALSE;
      }
      else
      {
         /* Validate time stamp */
         if( OSAL_s32Absolute(s32DataDiff) > SEN_DISP_MAX_TIME_STAMP_DIFFERENCE )
         {
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Invalid Acc time stamp diff: %d",
            OSAL_s32Absolute(s32DataDiff) );
            rAccCurData->u16ErrorCounter = 1;
         }

         s32DataDiff = (tS32) ( rAccCurData->u16Acc_x -
         rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_x );
         if( OSAL_s32Absolute(s32DataDiff) > SEN_DISP_MAX_ACC_DATA_DIFFERENCE )
         {
            rAccCurData->u16ErrorCounter = 1;
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Invalid Acc-X data diff: %d",
            OSAL_s32Absolute(s32DataDiff) );
         }

         s32DataDiff = (tS32) ( rAccCurData->u16Acc_y -
         rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_y );

         if( OSAL_s32Absolute(s32DataDiff) > SEN_DISP_MAX_ACC_DATA_DIFFERENCE )
         {
            rAccCurData->u16ErrorCounter = 1;
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Invalid Acc-Y data diff: %d",
            OSAL_s32Absolute(s32DataDiff) );
         }


         s32DataDiff = (tS32) ( rAccCurData->u16Acc_z -
         rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_z );

         if( OSAL_s32Absolute(s32DataDiff) > SEN_DISP_MAX_ACC_DATA_DIFFERENCE )
         {
            rAccCurData->u16ErrorCounter = 1;
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Invalid Acc-Z data diff: %d",
            OSAL_s32Absolute(s32DataDiff) );
         }
      }

      /* Store data for next comparison and validation */
      rSensorDispInfo.rAccInfo.rAccPrevData.u32TimeStamp = rAccCurData->u32TimeStamp;
      rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_x = rAccCurData->u16Acc_x;
      rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_y = rAccCurData->u16Acc_y;
      rSensorDispInfo.rAccInfo.rAccPrevData.u16Acc_z = rAccCurData->u16Acc_z;
   }
}

/********************************************************************************
* FUNCTION     : SenDisp_vExtractOdoStatus
*
* PARAMETER    : OSAL_trIOCtrlOdometerData* : pointer to Odo Data
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This function will update the odometer status and the direction.
*
* HISTORY      : 24.NOV.2015| Initial Version    |Srinivas Prakash Anvekar (RBEI/ECF5)
**********************************************************************************/
static tVoid SenDisp_vExtractOdoStatus( OSAL_trIOCtrlOdometerData *rOdometerData )
{
   //Update the Odometer Status by reading calibration bit
   if( (tU32)rOdometerData->enDirection >> SEN_DISP_ODO_CALIB_BIT_POS )
   {
      rOdometerData->enOdometerStatus = ODOMSTATE_CONNECTED_NO_CALIBRATION;
      SenDisp_vTraceOut( TR_LEVEL_WARNING, "Odometer data not to be used for calibration" );
   }
   else
   {
      rOdometerData->enOdometerStatus = ODOMSTATE_CONNECTED_NORMAL;
   }

   //Update the Odometer Direction
   rOdometerData->enDirection = (OSAL_tenIOCtrlOdometerRFS)( (tU32)rOdometerData->enDirection & SEN_DISP_ODO_DIR_MASK );

/*
  Updating the direction received from SCC to that required by the IMX

  SCC sends the following direction values to IMX
  
  Direction from SCC
  00  -- DATA INVALID
  01  -- FORWARD direction
  10  -- REVERSE direction
  11  -- UNKNOWN direction

  Handling of direction is done in the following way in IMX

  Direction data in IMX
  00  -- UNKNOWN direction
  01  -- FORWARD direction
  10  -- REVERSE direction
  11  -- DATA INVALID

  It is seen that direction data for the cases 00 and 11 are not compatible in IMX and SCC
  Therefore, direction received from SCC is converted to the values as required by the IMX
  */

   /* Check for data validity and update status to invalid depending on the 0th and 1st bits */
   if( SEN_DISP_ODO_BIT_FIELD_DRIV_DIRE_INVALID == (tU16)rOdometerData->enDirection )
   {
      rOdometerData->enOdometerStatus = ODOMSTATE_CONNECTED_DATA_INVALID;
   }
   /* Adjust the value of direction received for UNKNOWN condition */
   else if( SEN_DISP_ODO_BIT_FIELD_DRIV_DIRE_UNKNOWN == (tU16)rOdometerData->enDirection )
   {
      rOdometerData->enDirection = OSAL_EN_RFS_UNKNOWN;
   }
}

/********************************************************************************
* FUNCTION     : SenDisp_vDispatchOdoData
*
* PARAMETER    : tU32 u32Offset: Offset to the record to be extracted
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This will extract one odo sample, validate it and add it to ring buffer
* HISTORY      : 13.JAN.2014| Initial Version    |Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
**********************************************************************************/
static tVoid SenDisp_vDispatchOdoData( tU32 u32Offset )
{
   
   OSAL_trIOCtrlOdometerData rOdometerData;

   /* This is needed because source and destination locations are not of same size */
   OSAL_pvMemorySet(&rOdometerData, 0, sizeof(rOdometerData));

   /* Extract Time stamp */
   OSAL_pvMemoryCopy( &rOdometerData.u32TimeStamp,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_TIME_STAMP],
   SEN_DISP_FIELD_SIZE_TIME_STAMP );

   /* Extract wheel counter */
   OSAL_pvMemoryCopy( &rOdometerData.u32WheelCounter,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_ODO_WHEEL_CNTR],
   SEN_DISP_FIELD_SIZE_ODO_WHEEL_CNTR );

   /* Extract direction flag */
   OSAL_pvMemoryCopy( &rOdometerData.enDirection,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_ODO_DIRECTION_FLAG],
   SEN_DISP_FIELD_SIZE_ODO_DIRETION_FLAG );

   //update the odometer status and direction
   SenDisp_vExtractOdoStatus(&rOdometerData);
      
   SenDisp_vTraceOut( TR_LEVEL_USER_4,"Odo Data Recvd: whl cntr %u -- TS: %u -- Dir %d -- errcnt: %u -- msg cnt: %u -- sts : %u",
   rOdometerData.u32WheelCounter, rOdometerData.u32TimeStamp, 
   rOdometerData.enDirection, rOdometerData.u16ErrorCounter,
   rOdometerData.u16OdoMsgCounter, rOdometerData.enOdometerStatus);

   SenDisp_vValidateOdoRecord(&rOdometerData);
   if ( 0 == rOdometerData.u16ErrorCounter )
   {
      /* Add to Ring buffer */
      if( 1 != SenRingBuff_s32Write( SEN_DISP_ODO_ID,
               (tPVoid)&rOdometerData,
               1 ) )
      {
         SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write FAIL for ODO data");
      }
   }
}
/********************************************************************************
* FUNCTION     : SenDisp_vDispatchGyroData
*
* PARAMETER    : tU32 u32Offset: Offset to the record to be extracted
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This will extract one Gyro sample, validate it and add it to ring buffer
* HISTORY      :  13.JAN.2014| Initial Version    |Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
**********************************************************************************/
static tVoid SenDisp_vDispatchGyroData( tU32 u32Offset )
{
   OSAL_trIOCtrl3dGyroData r3dGyroData;
   
   /* This is needed because source and destination locations are not of same size */
   OSAL_pvMemorySet(&r3dGyroData, 0, sizeof(r3dGyroData));
   
   /* Extract Time stamp */
   OSAL_pvMemoryCopy( &r3dGyroData.u32TimeStamp,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_TIME_STAMP],
   SEN_DISP_FIELD_SIZE_TIME_STAMP );
   
   /* Extract gyro R value */
   OSAL_pvMemoryCopy( &r3dGyroData.u16Gyro_r,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_GYRO_R_DATA],
   SEN_DISP_FIELD_SIZE_EACH_GYRO_DATA );
   
   /* Extract gyro S value */
   OSAL_pvMemoryCopy( &r3dGyroData.u16Gyro_s,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_GYRO_S_DATA],
   SEN_DISP_FIELD_SIZE_EACH_GYRO_DATA );
   
   /* Extract gyro T value */
   OSAL_pvMemoryCopy( &r3dGyroData.u16Gyro_t,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_GYRO_T_DATA],
   SEN_DISP_FIELD_SIZE_EACH_GYRO_DATA );
   
   SenDisp_vTraceOut( TR_LEVEL_USER_4,"GYRO data errcnt: % u -- R: %u -- S: %u -- T: %u -- TS: %u",
   r3dGyroData.u16ErrorCounter, r3dGyroData.u16Gyro_r, 
   r3dGyroData.u16Gyro_s,r3dGyroData.u16Gyro_t, r3dGyroData.u32TimeStamp );
   
   SenDisp_vValidateGyroRecord(&r3dGyroData);
   if ( 0 == r3dGyroData.u16ErrorCounter )
   {
      /* Add to Ring buffer */
      if( 1 != SenRingBuff_s32Write( SEN_DISP_GYRO_ID, (tPVoid)&r3dGyroData, 1 ) )
      {
         SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write Gyro data failed" );
      }
   }

}

/********************************************************************************
* FUNCTION     : SenDisp_vDispatchGyroData
*
* PARAMETER    : tU32 u32Offset: Offset to the record to be extracted
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This will extract one ACC sample, validate it and add it to ring buffer
* HISTORY      :  13.JAN.2014| Initial Version    |Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
**********************************************************************************/
static tVoid SenDisp_vDispatchAccData( tU32 u32Offset )
{

   OSAL_trIOCtrlAccData rAccData;

   /* This is needed because source and destination locations are not of same size */
   OSAL_pvMemorySet(&rAccData, 0, sizeof(rAccData));

   /* Extract Time stamp */
   OSAL_pvMemoryCopy( &rAccData.u32TimeStamp,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_TIME_STAMP],
   SEN_DISP_FIELD_SIZE_TIME_STAMP);

   /* Extract ACC R value */
   OSAL_pvMemoryCopy( &rAccData.u16Acc_x,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_ACC_R_DATA],
   SEN_DISP_FIELD_SIZE_EACH_ACC_DATA);

   /* Extract ACC S value */
   OSAL_pvMemoryCopy( &rAccData.u16Acc_y,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_ACC_S_DATA],
   SEN_DISP_FIELD_SIZE_EACH_ACC_DATA);

   /* Extract ACC T value */
   OSAL_pvMemoryCopy( &rAccData.u16Acc_z,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_ACC_T_DATA],
   SEN_DISP_FIELD_SIZE_EACH_ACC_DATA);
   
   SenDisp_vTraceOut( TR_LEVEL_USER_4, "ACC data: errcnt: % u -- X: %u -- Y: %u -- Z: %u -- TS: %u",
   rAccData.u16ErrorCounter , rAccData.u16Acc_x, 
   rAccData.u16Acc_y,rAccData.u16Acc_z, rAccData.u32TimeStamp);

   SenDisp_vValidateAccRecord(&rAccData);
   if ( 0 == rAccData.u16ErrorCounter )
   {
      /* Add to Ring buffer */
      if( 1 != SenRingBuff_s32Write( SEN_DISP_ACC_ID, (tPVoid)&rAccData, 1 ) )
      {
         SenDisp_vTraceOut(TR_LEVEL_ERRORS,"SenRingBuff_s32Write ACC data failed ");
      }
   }
}
/********************************************************************************
* FUNCTION     : SenDisp_vDispatchGyroTemp
*
* PARAMETER    : tU32 u32Offset: Offset to the record to be extracted
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This will extract one Gyro Temp and sends it to driver.
* HISTORY      :  13.JAN.2014| Initial Version    |Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
**********************************************************************************/
static tVoid SenDisp_vDispatchGyroTemp( tU32 u32Offset )
{
   trGyroTemp rGyroTemp;

   /* Temperature of sensor has changed on SCC */
   rGyroTemp.u8MsgType = SEN_DISP_MSG_TYPE_TEMP_UPDATE;

   /* Extract Temperature */
   OSAL_pvMemoryCopy( &rGyroTemp.u16GyroTemp,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_GYRO_TEMP],
   SEN_DISP_FIELD_SIZE_TEMP );

   SenDisp_vTraceOut( TR_LEVEL_USER_4,"Gyro tmp %u", rGyroTemp.u16GyroTemp);
   if ( DEVICE_OPENED == rSensorDispInfo.rGyroInfo.enGyroSts )
   {
      /* Post data to gyro message queue */
      if( OSAL_OK != OSAL_s32MessageQueuePost( rSensorDispInfo.rGyroInfo.hldGyroMsgQue,
               (tPCU8) &rGyroTemp,
               sizeof(rGyroTemp),
               SEN_DISP_MSGQUE_PRIO) )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, "MsgQ post Failed for GYRO TEMP Err %lu",
         OSAL_u32ErrorCode());
      }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_USER_4,"Gyro tmp post skipped");
   }
}
/********************************************************************************
* FUNCTION     : SenDisp_vDispatchAccTemp
*
* PARAMETER    : tU32 u32Offset: Offset to the record to be extracted
*
* RETURNVALUE  : None
*
* DESCRIPTION  : This will extract one Acc Temp and sends it to driver.
* HISTORY      :  13.JAN.2014| Initial Version    |Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
**********************************************************************************/
static tVoid SenDisp_vDispatchAccTemp( tU32 u32Offset )
{
   trAccTemp rAccTemp;
   /* Temperature of sensor has changed on SCC */
   rAccTemp.u8MsgType = SEN_DISP_MSG_TYPE_TEMP_UPDATE;

   /* Extract Temperature */
   OSAL_pvMemoryCopy( &rAccTemp.u16AccTemp,
   &rSensorDispInfo.u8RecvBuffer[u32Offset + SEN_DISP_OFFSET_ACC_TEMP],
   SEN_DISP_FIELD_SIZE_TEMP);

   SenDisp_vTraceOut( TR_LEVEL_USER_4, "ACC tmp %u", rAccTemp.u16AccTemp );

   if ( DEVICE_OPENED == rSensorDispInfo.rAccInfo.enAccSts )
   {
      /* Post data to gyro message queue */
      if( OSAL_OK != OSAL_s32MessageQueuePost( rSensorDispInfo.rAccInfo.hldAccMsgQue,
               (tPCU8) &rAccTemp,
               sizeof(rAccTemp),
               SEN_DISP_MSGQUE_PRIO) )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, "MsgQ post Failed for ACC TEMP Err %lu",
         OSAL_u32ErrorCode());
      }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_USER_4,"ACC tmp post skipped");
   }
}
/********************************************************************************
* FUNCTION   : SenDisp_vTraceOut
*
* PARAMETER  : u32Level - trace level
*              pcFormatString - Trace string
*
* RETURNVALUE: tU32 error codes
*
* DESCRIPTION:Trace out function for sensor dispatcher.
*
* HISTORY    : 29.JAN.2013| Initial Version  |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tVoid SenDisp_vTraceOut(  TR_tenTraceLevel enTraceLevel, const tChar *pcFormatString,... )
{
   tU32 u32Level = (tU32) enTraceLevel;
   tS32 s32Size;
   /* Buffer to hold the string to trace out */
   tS8 u8Buffer[MAX_TRACE_SIZE];

   if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_SENSOR_DISPATCHER, u32Level) != FALSE)
   {
      /*
      Parameter to hold the argument for a function, specified the format
      string in pcFormatString
      defined as:
      typedef char* va_list in stdarg.h
      */
      /*
      vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure
      */
      va_list argList;
      
      /*
      Position in buffer from where the format string is to be
      concatenated
      */
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /*
      Flush the String
      */
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /*
      Copy the String to indicate the trace is from the RTC device
      */

      /*
      Initialize the argList pointer to the beginning of the variable
      argument list
      */
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
      Collect the format String's content into the remaining part of
      the Buffer
      */
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,
                  sizeof(u8Buffer),
                  pcFormatString,
                  argList ) ) )
      {
         return;
      }

      /*
      Trace out the Message to TTFis
      */
      LLD_vTrace( OSAL_C_TR_CLASS_SENSOR_DISPATCHER,
      u32Level,
      u8Buffer,
      (tU32)s32Size );   /* Send string to Trace*/
      /*
      Performs the appropriate actions to facilitate a normal return by a
      function that has used the va_list object
      */
      va_end(argList);
      
      if ( TR_LEVEL_FATAL == enTraceLevel )
      {
         SenDisp_vErrmemLog( (tPU8)u8Buffer, s32Size );
      }

   }

}

/*********************************************************************************************************************
* FUNCTION    : SenDisp_vErrmemLog
*
* PARAMETER   : tCString csErrmemLog: String to be logged
*
* RETURNVALUE : None
*
* DESCRIPTION : Log Into error memory
*
* HISTORY     :
*---------------------------------------------------------------------------
* Date        |       Version         | Author & comments
*-------------|-----------------------|-------------------------------------
* 03.APR.2014 | Initial version: 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* --------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid SenDisp_vErrmemLog( const unsigned char *pu8Buffer, tS32 s32Size )
{

   /*

         NO "TR_LEVEL_FATAL" TRACING ALLOWED FROM THIS FUNCTION. 
         
         ***************TR_LEVEL_FATAL****NOT***ALLOWED***************
         
         Or else this will result in unconditional recursion and stack overflow.


*/
   //#define ENABLE_TRACING

   trErrmemEntry rErrmemEntry;
   rErrmemEntry.eEntryType = eErrmemEntryFatal;
   rErrmemEntry.u16Entry = OSAL_C_TR_CLASS_SENSOR_DISPATCHER;
   OSAL_tIODescriptor hErrmemDev;
   
   if ( OSAL_NULL == pu8Buffer )
   {
#ifdef ENABLE_TRACING
      SenDisp_vTraceOut( TR_LEVEL_ERRORS, "!!!NULL POINTER at line %lu", __LINE__ );
#endif
   }
   else
   {

      rErrmemEntry.u16EntryLength = (tU16)s32Size;
      if( (ERRMEM_MAX_ENTRY_LENGTH) < rErrmemEntry.u16EntryLength )
      {
         rErrmemEntry.u16EntryLength = ERRMEM_MAX_ENTRY_LENGTH;
      }

      OSAL_pvMemoryCopy( (tPVoid)rErrmemEntry.au8EntryData,
      (tPVoid)pu8Buffer,
      rErrmemEntry.u16EntryLength );

      hErrmemDev = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_READWRITE );
      if ( OSAL_ERROR == hErrmemDev )
      {
#ifdef ENABLE_TRACING
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Errmem open fail %lu", OSAL_u32ErrorCode() );
#endif
      }
      else
      {
         if ( (tS32)sizeof(trErrmemEntry) != 
               OSAL_s32IOWrite( hErrmemDev, (tPCS8)&rErrmemEntry , sizeof(trErrmemEntry) ))
         {
#ifdef ENABLE_TRACING
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Errmem write fail %lu", OSAL_u32ErrorCode() );
#endif
         }
      }

      if ( OSAL_ERROR == OSAL_s32IOClose(hErrmemDev) )
      {
#ifdef ENABLE_TRACING
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, "Errmem close fail %lu", OSAL_u32ErrorCode() );
#endif
      }
   }
}









/* End Of File */

