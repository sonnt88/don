/******************************************************************************
* FILE            : sensor_dispatcher_types.h
*
* DESCRIPTION     : This file contains datatypes and macros common for 
*                             sensor_dispatcher.c and sensor_dispatcher_tdpy.c
*
* AUTHOR(s)       : Kulkarni Ramchandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 19.FEB.2016|  Initial version 1.0  | Kulkarni Ramchandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#ifndef SENSOR_DISPATCHER_TYPES
#define SENSOR_DISPATCHER_TYPES


/*************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#if defined (GEN3X86)
   #define SENSOR_PROXY_TEST_STUB_ACTIVE
   #define SOCKET_IP_ADDR "127.0.0.1"
//  #define SOCKET_IP_ADDR "172.17.0.6" //define this address if we are running on stub and 
                                    //commect the macros defined for LSIM 
#endif

#ifndef SENSOR_PROXY_TEST_STUB_ACTIVE
/* INC functionality*/
#include "inc.h"
#include "inc_ports.h"
#include "dgram_service.h"
#endif


/*-----------------------------------------------------------------------
* Macro declaration (scope: Global)
*-----------------------------------------------------------------------*/

#define SEN_DISP_INIT_EVENT_NAME       ((tCString)"SenDisIniEv")
#define SEN_DISP_EVENT_INIT_SUCCESS    ((OSAL_tEventMask)0x01)
#define SEN_DISP_EVENT_INIT_FAILED     ((OSAL_tEventMask)0x02)
#define SEN_DISP_EVENT_DEINIT_COMPLETE ((OSAL_tEventMask)0x04)
#define SEN_DISP_EVENT_WAIT_MASK       ((SEN_DISP_EVENT_INIT_FAILED)|(SEN_DISP_EVENT_INIT_SUCCESS)|(SEN_DISP_EVENT_DEINIT_COMPLETE))
#define SEN_DISP_INIT_EVENT_WAIT_TIME  ((OSAL_tMSecond)5000) /*  5 seconds */
#define SEN_DISP_RETRY_INTERVALL_MS    ((OSAL_tMSecond)100)

/*Message ID of all possible types of messages used to communicate with SCC Sensor Component*/
#define MSG_ID_SCC_SENSORS_C_COMPONENT_STATUS (0x20)
#define MSG_ID_SCC_SENSORS_R_COMPONENT_STATUS (0x21)
#define MSG_ID_SCC_SENSORS_R_REJECT           (0x0B)
//#define MSG_ID_SCC_SENSORS_C_CONFIG           (0x30)
#define MSG_ID_SCC_SENSORS_R_CONFIG           (0x31)
//#define MSG_ID_SCC_SENSOR_C_DATA              (0x40)
#define MSG_ID_SCC_SENSOR_R_DATA              (0x41)
#define MSG_ID_SCC_SENSORS_C_SELFTEST         (0x50)
#define MSG_ID_SCC_SENSORS_R_SELFTEST         (0x51)

/* size of message ID field in all messages */
#define MSGID_SIZE (1)

/*Size of the each of the response messages and fields from SSC sensor component.*/
#define MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS_OLD      ( MSGID_SIZE + 1)
#define MSG_SIZE_SCC_SENSORS_C_COMPONENT_STATUS       ( MSGID_SIZE + 2)
#define MSG_SIZE_SCC_SENSORS_R_COMPONENT_STATUS       ( MSGID_SIZE + 2)
#define MSG_SIZE_SCC_SENSORS_R_REJECT                 ( MSGID_SIZE + 2)
#define MSG_SIZE_SCC_SENSORS_R_CONFIG                 ( MSGID_SIZE + 10)

/* Size of each entry type in sensor data message */
#define MSG_SIZE_SCC_SENSOR_R_DIAG_MESSAGE            (6)
#define MSG_SIZE_SCC_SENSOR_R_ODO_DATA                (7)
#define MSG_SIZE_SCC_SENSOR_R_GYRO_DATA               (10)
#define MSG_SIZE_SCC_SENSOR_R_ACC_DATA                (10)
#define MSG_SIZE_SCC_SENSOR_R_GYRO_TEMP               (6)
#define MSG_SIZE_SCC_SENSOR_R_ACC_TEMP                (6)
#define MSG_SIZE_SCC_SENSOR_R_ABS_DATA                (9)

/* Size of each field type  */

/* In config message */
#define SEN_DISP_FIELD_SIZE_CONFIG_SENSOR_DATA_INTERVALL  (2)
#define SEN_DISP_FIELD_SIZE_CONFIG_SENSOR_TYPE (1)
/* In data message */
#define SEN_DISP_FIELD_SIZE_NUM_ENTRTIES (2)
#define SEN_DISP_FIELD_SIZE_TIME_STAMP (3)
/* Diag data */
#define SEN_DISP_FIELD_SIZE_DIAG_MESSAGE (2)
/* Odo Data */
#define SEN_DISP_FIELD_SIZE_ODO_WHEEL_CNTR (2)
#define SEN_DISP_FIELD_SIZE_ODO_DIRETION_FLAG (1)
/* Gyro data */
#define SEN_DISP_FIELD_SIZE_EACH_GYRO_DATA (2)
/* ACC data */
#define SEN_DISP_FIELD_SIZE_EACH_ACC_DATA (2)
/* Temp Update */
#define SEN_DISP_FIELD_SIZE_TEMP (2)
/* ABS Data */
#define SEN_DISP_FIELD_SIZE_ABS_RR_WHEEL_CNTR (2)
#define SEN_DISP_FIELD_SIZE_ABS_RL_WHEEL_CNTR (2)
#define SEN_DISP_FIELD_SIZE_ABS_STATUS_FLAGS  (1)
/* Bit fields indicating ABS data status */
#define SEN_DISP_ABS_RR_STATUS_BIT_FIELD          (0x01)
#define SEN_DISP_ABS_RL_STATUS_BIT_FIELD          (0x02)
#define SEN_DISP_ABS_BIT_FIELD_DRIV_DIRE_FWD      (0x01 << 2)
#define SEN_DISP_ABS_BIT_FIELD_DRIV_DIRE_REV      (0x02 << 2)
#define SEN_DISP_ABS_BIT_FIELD_MASK_DRIV_DIRE_REV (0x0C)
#define SEN_DISP_ABS_BIT_FIELD_DRIV_DIRE_INVALID  (0x00)


/* Validity of ABS cues counter data */
//#define SEN_DISP_ABS_COUNTER_DATA_VALID    (1)
//#define SEN_DISP_ABS_COUNTER_DATA_INVALID  (0)

/* Bit fields for ODO data */
#define SEN_DISP_ODO_CALIB_BIT_POS         (tU32)(0x07)
#define SEN_DISP_ODO_DIR_MASK              (tU32)(0x03)

#define SEN_DISP_ODO_BIT_FIELD_DRIV_DIRE_UNKNOWN          (0x03)
#define SEN_DISP_ODO_BIT_FIELD_DRIV_DIRE_INVALID          (0x00)



/*Offset of fields in message from SCC*/
#define SEN_DISP_OFFSET_MSG_ID (0)
#define SEN_DISP_OFFSET_COMPONENT_STATUS (1)
#define SEN_DISP_OFFSET_VERSION_STATUS (SEN_DISP_OFFSET_COMPONENT_STATUS + 1)
#define SEN_DISP_OFFSET_ODO_CONFIG_DATA_INTERVAL (1)
#define SEN_DISP_OFFSET_GYRO_CONFIG_DATA_INTERVAL (3)
#define SEN_DISP_OFFSET_GYRO_CONFIG_TYPE (5)
#define SEN_DISP_OFFSET_ACC_CONFIG_DATA_INTERVAL (6)
#define SEN_DISP_OFFSET_ACC_CONFIG_TYPE (8)
#define SEN_DISP_OFFSET_ABS_CONFIG_DATA_INTERVAL (9)
#define SEN_DISP_OFFSET_REJECT_REASON   (1)
#define SEN_DISP_OFFSET_REJECTED_MSG_ID (2)
#define SEN_DISP_OFFSET_NUM_OF_ENTRIES_IN_DATA_MSG (1)
#define SEN_DISP_OFFSET_FIRST_RECORD_IN_DATA_MSG (3)
#define SEN_DISP_OFFSET_ENTRY_TYPE (0)
#define SEN_DISP_OFFSET_TIME_STAMP (1)
#define SEN_DISP_OFFSET_DIAGNOSIS_MESSAGE (4)
#define SEN_DISP_OFFSET_ODO_WHEEL_CNTR (4)
#define SEN_DISP_OFFSET_ODO_DIRECTION_FLAG (6)
#define SEN_DISP_OFFSET_GYRO_R_DATA (4)
#define SEN_DISP_OFFSET_GYRO_S_DATA (6)
#define SEN_DISP_OFFSET_GYRO_T_DATA (8)
#define SEN_DISP_OFFSET_GYRO_TEMP (4)
#define SEN_DISP_OFFSET_ACC_R_DATA (4)
#define SEN_DISP_OFFSET_ACC_S_DATA (6)
#define SEN_DISP_OFFSET_ACC_T_DATA (8)
#define SEN_DISP_OFFSET_ACC_TEMP (4)
#define SEN_DISP_OFFSET_SELFTEST_DEVICE_ID (1)
#define SEN_DISP_OFFSET_ABS_RR_WHEEL_CNTR (4)
#define SEN_DISP_OFFSET_ABS_RL_WHEEL_CNTR (6)
#define SEN_DISP_OFFSET_ABS_STATUS        (8)
#define SEN_DISP_VERSION_INFO  (0x01)  //macro for version status check


/*Entry types in data record from SCC */
#define SEN_DISP_ENTRY_TYPE_DIAGNOSIS  (0)
#define SEN_DISP_ENTRY_TYPE_ODO_DATA   (1)
#define SEN_DISP_ENTRY_TYPE_GYRO_DATA  (2)
#define SEN_DISP_ENTRY_TYPE_GYRO_TEMP  (3)
#define SEN_DISP_ENTRY_TYPE_ACC_DATA   (4)
#define SEN_DISP_ENTRY_TYPE_ACC_TEMP   (5)
#define SEN_DISP_ENTRY_TYPE_ABS_DATA   (6)


/*Command reject reason from SCC*/
/* Presently not used
#define REJ_NO_REASON       (0x00)
#define REJ_UNKNOWN_MESSAGE (0x01)
#define REJ_INVALID_PARA    (0x02)
#define REJ_TMP_UNAVAIL     (0x03) */

/* HOST Status  */
#define HOST_SENSOR_COMPONENT_STATUS_ACTIVE           (0x01)
#define HOST_SENSOR_COMPONENT_STATUS_NOT_ACTIVE       (0x02)

/*SSC status responses*/
#define SCC_SENSOR_COMPONENT_STATUS_ACTIVE     (0x01)
#define SCC_SENSOR_COMPONENT_STATUS_NOT_ACTIVE (0x02)

/* Sensor device state. If data update interval in config message is
0  then that sensor is not used in the project*/
#define SEN_DISP_DEVICE_NOT_USED (0)

/* This thread reads from socket, segregate data and delivers it. Pretty high traffic is expected via traffic.
So performance may improve to keep the thread priority highest*/
#define SEN_DISP_SOCKET_READ_THREAD_NAME "DisReThr"
#define SEN_DISP_SOCKET_READ_THREAD_PRIORITY (OSAL_C_U32_THREAD_PRIORITY_HIGHEST)
#define SEN_DISP_SOCKET_READ_THREAD_STACKSIZE (2048)

/* This semaphore is used to initialize and de-initialize dispatcher. This is needed because
init and de-init calls are expected from multiple threads*/
#define SEN_DISP_INIT_DEINT_SEM_NAME "DisInSem"
/* During init wait timeout is more because a thread that has semaphore will be trying
to connect to socket, initialize dispatcher and ring buffer. This may take time */
#define SEN_DISP_INIT_SEM_WAIT_TIME (1000)
/* It is not advisable to take semaphore during de-init/shutdown. But we badly need
one here. Hence timeout is kept very less */
#define SEN_DISP_DEINIT_SEM_WAIT_TIME (25)

/* Very few messages are sent to drivers. So a small message queue should do the job */
#define ODO_MAX_MESSAGES_IN_MSGQUE  (5)
#define GYRO_MAX_MESSAGES_IN_MSGQUE (5)
#define ACC_MAX_MESSAGES_IN_MSGQUE  (5)
#define ABS_MAX_MESSAGES_IN_MSGQUE  (5)

/* Well messages are to be posted with some fixed priority */
#define SEN_DISP_MSGQUE_PRIO (5)

/* This is the maximum size of the message expected from SCC.
If SCC tries to send a message bigger that this, driver will crash*/
#define SEN_DISP_MAX_PACKET_SIZE (4096)

#ifdef SENSOR_PROXY_TEST_STUB_ACTIVE
/* OUR port/LUN number */
#define INC_PORT_SENSOR_COMPONENT 0x06
#endif

/* These are used for release OS resources */
#define SEN_DISP_RESOURCE_RELSEASE_ODO_MSGQUE  (1)
#define SEN_DISP_RESOURCE_RELSEASE_GYRO_MSGQUE (2)
#define SEN_DISP_RESOURCE_RELSEASE_ACC_MSGQUE  (3)
#define SEN_DISP_RESOURCE_RELSEASE_SOCKET      (4)
#define SEN_DISP_RESOURCE_RELSEASE_RINGBUFF    (5)
#define SEN_DISP_RESOURCE_RELSEASE_INIT_SEM    (6)
#define SEN_DISP_RESOURCE_RELEASE_INIT_EVENT   (7)
#define SEN_DISP_RESOURCE_RELSEASE_ABS_MSGQUE  (8)


/* Timeout multiplication factor. This will be passed to Ring buffer. Ring buffer will
add a error entry if dispatcher doesn't add data within
(sensor config interval * below value)  */
#define SEN_DISP_RINGBUFF_TIMEOUT_MUL_FACTOR (10)

/* Timeout for odometer */
#define SEN_DISP_ODO_TIMEOUT (300)

/* Number of records to be stored in Ring Buffer. Currently we don't know how long
VD sensor takes to start reading records from ring buff. Till then we shall try to
buffer it*/
#define SEN_DISP_MAX_RINGBUFF_ODO_RECORDS  (512)
#define SEN_DISP_MAX_RINGBUFF_GYRO_RECORDS (512)
#define SEN_DISP_MAX_RINGBUFF_ACC_RECORDS  (512)
#define SEN_DISP_MAX_RINGBUFF_ABS_RECORDS  (512)

/* Retry count in case of read failure. If read on socket fails for some reason,
we do a retry for these many times*/
#define SEN_DISP_SOCKET_INIT_RETRY_COUNT   (3)
#define SEN_DISP_INIT_RETRY_WAIT_TIME_MS   (100)

/* Data validation thresholds*/
#define SEN_DISP_MAX_TIME_STAMP_DIFFERENCE (tS32)(2000)
#define SEN_DISP_MAX_GYRO_DATA_DIFFERENCE  (tS32)(10000)
#define SEN_DISP_MAX_ACC_DATA_DIFFERENCE   (tS32)(10000)

#ifdef ODO_DATA_FILTER_ENABLE
#define SEN_DISP_MAX_ODO_DATA_DIFFERENCE   (tS32)(500)
#endif
#ifdef ABS_DATA_FILTER_ENABLE
#define SEN_DISP_MAX_ABS_DATA_DIFFERENCE   (tS32)(500)
#endif
/* ABS counter is 15 bit so max value is 0x7fff*/
#define SEN_DISP_MAX_ABS_COUNTER_VALUE     (tU32)(0x7fff)

/*Macros to represent Self test result*/
#define SEN_DISP_SELF_TEST_RESULT_DIAGNOSIS         (0x00)  
#define SEN_DISP_SELF_TEST_RESULT_ODOMETER          (0x01)
#define SEN_DISP_SELF_TEST_RESULT_GYROMETER         (0x02)
#define SEN_DISP_SELF_TEST_RESULT_GYRO_TEMPERATURE  (0x03)
#define SEN_DISP_SELF_TEST_RESULT_ACCELEROMETER     (0x04)
#define SEN_DISP_SELF_TEST_RESULT_ACC_TEMPERATURE   (0x05)

/*Macros to represent self test devices*/
#define SEN_DISP_SELF_TEST_DEVICE_GYRO (0X02)
#define SEN_DISP_SELF_TEST_DEVICE_ACC  (0X04)

/* Index of self test */
#define SEN_DISP_SELF_TEST_INDEX (1)

#define SEN_DISP_MAX_INCORRECT_RES_FRM_V850 (5)

/* Macros related to sensor dispatcher wait for data
 from V850 in SensorProxy_s32GetDataFromScc function  */
#define SEN_DISP_POLL_NUM_OF_FDS 1
#define SEN_DISP_POLL_TIMEOUT_MS 200
#define SEN_DISP_DATA_INTERVAL_MS  1000
#define SEN_DISP_READ_TIME_OUT_SCALE_FACTOR 2
#define SEN_DISP_POLL_COUNTER_THRESHOLD    ( ( SEN_DISP_DATA_INTERVAL_MS * SEN_DISP_READ_TIME_OUT_SCALE_FACTOR ) \
                                                 /SEN_DISP_POLL_TIMEOUT_MS )

#define SEN_DISP_CONVERT_MS_TO_SEC(TimeInMs) \
   ( (tF32) ((tF32)(TimeInMs) / (tF32)1000.0 ))


#if defined (GEN3X86)

#define SEN_DISP_STATIC_ODO_TIMESTAMP 737942
#define SEN_DISP_STATIC_ODO_WHEELCOUNT 0
#define SEN_DISP_STATIC_ODO_DIRECTION 1
#define SEN_DISP_STATIC_ODO_ERRORCOUNT 0
#define SEN_DISP_STATIC_ODO_STATUS 0
#define SEN_DISP_STATIC_ODO_CONFIG_INTERVAL 70
#define SEN_DISP_STATIC_ODO_WAIT_TIME 30


#define SEN_DISP_STATIC_GYRO_TIMESTAMP 737897
#define SEN_DISP_STATIC_GYRO_R_VAL 32858
#define SEN_DISP_STATIC_GYRO_S_VAL 32679
#define SEN_DISP_STATIC_GYRO_T_VAL 32711
#define SEN_DISP_STATIC_GYRO_ERRORCOUNT 0
#define SEN_DISP_STATIC_GYRO_CONFIG_INTERVAL 50
#define SEN_DISP_STATIC_GYRO_CONFIG_TYPE 1
#define SEN_DISP_STATIC_GYRO_ACC_WAIT_TIME 20


#define SEN_DISP_STATIC_ACC_TIMESTAMP 737752
#define SEN_DISP_STATIC_ACC_X_VAL 2020
#define SEN_DISP_STATIC_ACC_Y_VAL 1513
#define SEN_DISP_STATIC_ACC_Z_VAL 1275
#define SEN_DISP_STATIC_ACC_ERRORCOUNT 0
#define SEN_DISP_STATIC_ACC_CONFIG_INTERVAL 50
#define SEN_DISP_STATIC_ACC_CONFIG_TYPE 1

#define SEN_DISP_STATIC_RCVD_MSG_SIZE 11
#define SEN_DISP_STAIC_DATA_THREAD_PRIORITY 50

#endif

/*-----------------------------------------------------------------------
* Driver specific structure definition
*-----------------------------------------------------------------------*/

/* To Send IMX status to SCC  */
typedef struct
{
   tU8 u8MsgID;
   tU8 u8HostStatus;
   tU8 u8VersionInfo;
}trMsgCmdHostStatus;

/*To send selftest trigger message to SCC*/
typedef struct
{
   tU8 u8MsgID;
   tU8 u8SelfTest;
}trMsgSensorSelfTest;



/* To know if a particular sensor driver has opened the device or not */
typedef enum
{
   NOT_INITIALIZED = 0,
   DEVICE_EXISTS = 1,
   DEVICE_OPENED = 2,
   DEVICE_CLOSED = 3,
   DEVICE_NOT_USED = 4
}tEnSensorStatus;

/* Driver specific information  */
typedef struct
{
   OSAL_tMQueueHandle hldOdoMsgQue; /* Handle to Odometer driver message queue */
   tEnSensorStatus enOdoSts;       /* Odometer driver status  */
   tBool bIsFirstRecord;
   OSAL_trIOCtrlOdometerData rOdoPrevData;

}trOdoInfo;

typedef struct
{
   OSAL_tMQueueHandle hldGyroMsgQue; /* Handle to Gyro driver message queue  */
   tEnSensorStatus enGyroSts;        /* Gyro driver status  */
   tBool bIsFirstRecord;
   OSAL_trIOCtrl3dGyroData rGyroPrevData;
}trGyroInfo;

typedef struct
{
   OSAL_tMQueueHandle hldAccMsgQue;  /* Handle to ACC driver message queue   */
   tEnSensorStatus enAccSts;         /* ACC driver status  */
   tBool bIsFirstRecord;
   OSAL_trIOCtrlAccData rAccPrevData;
}trAccInfo;

typedef struct
{
   OSAL_tMQueueHandle hldAbsMsgQue;  /* Handle to ACC driver message queue   */
   tEnSensorStatus enAbsSts;/* ACC driver status*/
   tBool bIsFirstRecord;
   OSAL_trAbsData rAbsPrevData;
}trAbsInfo;




/* Dispatcher running configuration  */
typedef struct
{
   tS32 s32SocketFD;  /* Handle to socket  */
#ifndef SENSOR_PROXY_TEST_STUB_ACTIVE
   sk_dgram* hldSocDgram; /*datagram handle*/
#endif
   OSAL_tSemHandle HldInitSem; /* Handle to init and de-init semaphore  */
   OSAL_tEventHandle hSenDispInitEvent; /* For INIT */
   tU8 u8RecvBuffer[SEN_DISP_MAX_PACKET_SIZE]; /* Buffer to receive data from socket */
   tU32 u32RecvdMsgSize;  /* Size of latest received message */
   trOdoInfo rOdoInfo;  /* Odo driver Details */
   trGyroInfo rGyroInfo; /* Gyro Driver Details */
   trAccInfo rAccInfo;  /* ACC driver details */
   trAbsInfo rAbsInfo; /* ABS driver details*/
   tU8 u8SccStatus;  /* SCC status */
   tBool bCreateFlag; /* Dispatcher status */
   tBool bShutdownFlag; /* Shutdown event status */
   tU8 u8SccVerInfo;  /* Version Info */

}trSensorDispInfo;

#endif

/* End of file */
