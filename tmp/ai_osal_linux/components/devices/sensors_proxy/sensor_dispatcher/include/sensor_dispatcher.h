/******************************************************************************
* FILE            : sensor_dispatcher.h
*
* DESCRIPTION     : This is the header file for sensor_dispatcher.c
*
* AUTHOR(s)       : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 19.MAR.2012|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#ifndef SENSOR_DISPATCHER_HEADER
#define SENSOR_DISPATCHER_HEADER


/* If a new driver is added, this is the place where you get the ID for your driver
    Any mistake done here may cause all sensor drivers to crash. Please Dont insert new 
    driver in the middle. Append it at the last*/
typedef enum
{

   SEN_DISP_ODO_ID = 0,
   SEN_DISP_GYRO_ID = 1,
   SEN_DISP_ACC_ID =2,
   SEN_DISP_ABS_ID =3,
   SEN_DISP_MAX_ID

}tEnSenDrvID;

/* Messages transmitted to sensor drivers via message queue */
/* Odo config message */
typedef struct
{
   tU8  u8MsgType;
   tU16 u16OdoDataInterval;
   
}trOdoConfigData;

/* Abs config message */
typedef struct
{
   tU8  u8MsgType;
   tU16 u16AbsDataInterval;
}trAbsConfigData;

/* Gyro config message */
typedef struct
{
   tU8  u8MsgType;
   tU8  u8GyroType;
   tU16 u16GyroDataInterval;
   
}trGyroConfigData;

/* Gyro temp update */
typedef struct
{
   tU8  u8MsgType;
   tU16 u16GyroTemp;
   
}trGyroTemp;

/* ACC config message */
typedef struct
{
   tU8  u8MsgType;
   tU8  u8AccType;
   tU16 u16AccDataInterval;
   
}trAccConfigData;

/* ACC temp update */
typedef struct
{
   tU8  u8MsgType;
   tU16 u16AccTemp;
   
}trAccTemp;

/*Self test result*/  
typedef struct
{
   tU8  u8MsgType;
   tU8  u8SelfTestResult;
}SensorSelfTestResult;

/* Message ID's */
#define SEN_DISP_MSG_TYPE_CONFIG      (1)
#define SEN_DISP_MSG_TYPE_TEMP_UPDATE (2)
#define SEN_DISP_MSG_TYPE_SELF_TEST_RESULT (3)

/* Message queue names for drivers to open */
#define SEN_DISP_TO_ODO_MSGQUE_NAME "DisToOdo"
#define SEN_DISP_TO_GYRO_MSGQUE_NAME "DisToGyr"
#define SEN_DISP_TO_ACC_MSGQUE_NAME "DisToAcc"
#define SEN_DISP_TO_ABS_MSGQUE_NAME "DisToAbs"

/* message queue lenghts */
#define ODO_MSG_QUE_LENGTH  (sizeof(trOdoConfigData))
#define GYRO_MSG_QUE_LENGTH (sizeof(trGyroConfigData))
#define ACC_MSG_QUE_LENGTH  (sizeof(trAccConfigData))
#define ABS_MSG_QUE_LENGTH  (sizeof(trAbsConfigData))


tS32 s32SensorDiapatcherInit(tEnSenDrvID enSenID);
tS32 s32SensorDiapatcherDeInit(tEnSenDrvID enSenID);
tS32 SenDisp_s32TriggerGyroSelfTest(tVoid);
tS32 SenDisp_s32TriggerAccSelfTest(tVoid);
tVoid SenDisp_vTraceOut(  TR_tenTraceLevel enTraceLevel, const tChar *pcFormatString,... );

#endif

/*End of file*/
