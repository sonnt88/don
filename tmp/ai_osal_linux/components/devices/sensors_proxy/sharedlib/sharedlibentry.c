
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"

extern tS32 GnssProxy_s32IOOpen( OSAL_tenAccess enAccess );
extern tS32 ChEnc_s32IOOpen( tVoid );
extern tS32 ACC_s32IOOpen(tVoid);
extern tS32 GYRO_s32IOOpen(tVoid);
extern tS32 ODOMETER_s32IOOpen(tVoid);
extern tS32 GpsProxy_s32IOControl( tS32 s32FunId, tLong sArg );
extern tS32 ABS_s32IOOpen(tVoid);
extern tS32 AUXCLOCK_s32IOOpen(tVoid);
extern tVoid AUXCLOCK_vInit(tVoid);
extern tVoid AUXCLOCK_vDeinit(tVoid);

static sem_t *initSen_lock;
static int *tmp = (int*)0xffffffff;
 
void __attribute__ ((constructor)) sensor_process_attach(void)
{
   tBool bFirstAttach = FALSE;
   char buffer[100];
   AUXCLOCK_vInit();
  
   initSen_lock = sem_open("SEN_INIT",O_EXCL | O_CREAT, 0660, 0);
   if (initSen_lock != SEM_FAILED)
   {
      if(s32OsalGroupId)
      {
         if(chown("/dev/shm/sem.SEN_INIT",(uid_t)-1,s32OsalGroupId) == -1)
         {
             vWritePrintfErrmem("sensor_process_attach  -> chown error %d",errno);
         }
         if(chmod("/dev/shm/sem.SEN_INIT", OSAL_ACCESS_RIGTHS) == -1)
         {
             vWritePrintfErrmem("sensor_process_attach chmod -> fchmod error %d",errno);
         }
      }
      bFirstAttach = TRUE;
   }
   else
   {
       initSen_lock = sem_open("SEN_INIT", 0);
       bFirstAttach = FALSE;
       sem_wait(initSen_lock);
   }
   if(bFirstAttach)
   {
         if( (int*)initSen_lock == tmp)
         {
            buffer[0] = 100;
            GnssProxy_s32IOOpen(OSAL_EN_READONLY);
            ChEnc_s32IOOpen();
            ACC_s32IOOpen();
            GYRO_s32IOOpen();
            ODOMETER_s32IOOpen();
            GpsProxy_s32IOControl( buffer[0], 0 );
            AUXCLOCK_s32IOOpen();
            ABS_s32IOOpen();
         }
   }
   else
   {
   }
   sem_post(initSen_lock);
}


void vOnProcessDetach( void )
{
   AUXCLOCK_vDeinit();
}
