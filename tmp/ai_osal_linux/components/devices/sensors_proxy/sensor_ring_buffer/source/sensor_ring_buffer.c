/******************************************************************************
* FILE         : sensor_ring_buffer.c
*             
* DESCRIPTION  : This file implements a ring buffer with special features needed for sensors.
*                Apart from storing data, updation of error record in ringbuffer when data 
*                is not filled at regular intervals is also embedded in the ring buffer.
*             
* AUTHOR(s)    : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 19.MAR.2013|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "Linux_osal.h"
#include "ostrace.h"

#include "sensor_dispatcher.h"
#include "sensor_ring_buffer.h"



/************************************************************************
* Driver specific structure definition
*-----------------------------------------------------------------------*/
/* These are the possible states the ring buffer can get into. */
typedef enum
{
   RING_BUFF_NOT_INITIALIZED = 0,
   RING_BUFF_INITIALIZED = 1,
   RING_BUFF_OPENED = 2,
   RING_BUFF_CLOSED = 3
}tEnRingBuffStatus;

/*  Every driver opening the ring buffer will have a handle pointing to
   this structure*/
typedef struct
{
   OSAL_tSemHandle hRngBuffUpdateSem;
   OSAL_tTimerHandle hDataTimeoutTimer;
   OSAL_tEventHandle hDataReadWaitEvent;
   trSenRingBuffConfig rRingBuffConfig;
   tPChar pcSenData;
   tU32 u32WriteIndex;
   tU32 u32ReadIndex;
   tU32 u32AvaiRecords;

}trSenRingBuffInfo;

/* This holds information about all possible sensors that can use ring buffer */
typedef struct
{

   tEnSenDrvID enSenDrvID;   //lint -e754
   tCString csDevRngBuffSemName;
   tCString csReadEventName;
   tEnRingBuffStatus enRingBuffStatus;
   tU16 u16ErrorCounter;
   trSenRingBuffInfo* pHldRngBuf;

}trSensorDetails;


/* Some common part the ringbuffer shares among all drivers */

typedef struct
{
   OSAL_tEventHandle h_SenRngBuffEvent;
   tBool bShutdownFlag;
   tBool bRingBuffInitFlag;
   
}trSenRngBufComInfo;




/************************************************************************
* Macro declaration (scope: Global)
*-----------------------------------------------------------------------*/
/* Event used to indicate timeouts  */
#define SEN_RNGBUFF_TIMEOUT_EVENT_NAME "SrTimEnt"
/* Thread that waits for timeout events*/
#define SEN_RNGBUFF_DATA_MONITOR_THREAD_NAME "SrMonThr"
#define SEN_RNGBUFF_C_U32_THREAD_STACK_SIZE (2048)
#define SEN_RNGBUFF_C_U32_THREAD_PRIORITY OSAL_C_U32_THREAD_PRIORITY_NORMAL
/* Timeout event masks */
#define SEN_RNGBUFF_ODO_DATA_TIMEOUT_EVENT  (0x001)
#define SEN_RNGBUFF_GYRO_DATA_TIMEOUT_EVENT (0x002)
#define SEN_RNGBUFF_ACC_DATA_TIMEOUT_EVENT  (0x004)
#define SEN_RNGBUFF_SHUTDOWN_EVENT          (0x008)
#define SEN_RNGBUFF_ABS_DATA_TIMEOUT_EVENT  (0x010)

#define SEN_RNGBUFF_READ_READY_EVENT (0x001)

/* This is the semaphore wait timeout for writing into ring buffer. 
   Dispatcher will be trying to write to ring buffer. Blocking dispatcher
   for more than 10 milliseconds may fill up the socket buffer*/
#define SEN_RNGBUFF_WRITE_EVENTWAIT_TIME    (100)
#define SEN_RNGBUFF_READ_EVENTWAIT_TIME     (500)
#define SEN_RNGBUFF_SHUTDOWN_EVENTWAIT_TIME (100)


/************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/

/* This holds all sensor details. When a new sensor is added, this has to be updated */
trSensorDetails rSensorDetails[] =
{
   /*  Driver ID          Semaphore name to sync read/write    Event to keep read thread waiting     Ring buffer state         Err count  Handle to ring buff details */
   {   SEN_DISP_ODO_ID,     (tCString)"OdoRnSem",              (tCString)"OdoRdEve",                RING_BUFF_NOT_INITIALIZED,   0,       OSAL_NULL   },
   {   SEN_DISP_GYRO_ID,    (tCString)"GyrRnSem",              (tCString)"GyrRdEve",                RING_BUFF_NOT_INITIALIZED,   0,       OSAL_NULL   },
   {   SEN_DISP_ACC_ID,     (tCString)"AccRnSem",              (tCString)"AccRdEve",                RING_BUFF_NOT_INITIALIZED,   0,       OSAL_NULL   },
   {   SEN_DISP_ABS_ID,     (tCString)"AbsRnSem",              (tCString)"AbsRdEve",                RING_BUFF_NOT_INITIALIZED,   0,       OSAL_NULL   },
   
   {   SEN_DISP_MAX_ID,     (tCString)OSAL_NULL,               (tCString)OSAL_NULL,                 RING_BUFF_NOT_INITIALIZED,   0,       OSAL_NULL   },

};

trSenRngBufComInfo rSenRngBufComInfo;
OSAL_tIODescriptor AuxClockFd;
OSAL_trIOCtrlAuxClockTicks rAuxClockTicks;
tS32 s32ErrChk = OSAL_ERROR;

/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
static tVoid SenRngBuff_vTraceOut(  TR_tenTraceLevel enTraceLevel, const tChar *pcFormatString,... );
static void SenRngBuff_vTimerCallBack (tPVoid pvArg);
static tVoid SenRngBuff_vMonitorThread (tPVoid pvDummyArg);
static tVoid SenRingBuff_vAddTimeoutRec( OSAL_tEventMask );
static tVoid SenRingBuff_vAddOdoTimeoutRec(  );
static tVoid SenRingBuff_vAddGyroTimeoutRec(  );
static tVoid SenRingBuff_vAddAccTimeoutRec(  );
static tVoid SenRingBuff_vAddAbsTimeoutRec(  );

/********************************************************************************
* FUNCTION        : SenRingBuff_s32Init 

* PARAMETER       :  tEnSenDrvID enDeviceID : For which sensor ring buff has to be created.
*                    trSenRingBuffConfig *prSenRingBuffConfig : pointer to ring buff config
*
* RETURNVALUE     : OSAL_OK  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : This is used to initialize ring buffer for a particular driver.
*                   This will be done by dispatcher.
*                   1: This creates a thread to monitor if ring buffer is filled at regular
*                      intervals. Else it will add error records.
*                   2: A event to notify thread about data timeout
*                   3: A timer for every driver with a specific timeout value.
*
* HISTORY         : 19.MAR.2013| Initial Version    |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 SenRingBuff_s32Init( tEnSenDrvID enDeviceID, const trSenRingBuffConfig *prSenRingBuffConfig )
{

   tS32 s32RetVal = OSAL_ERROR;
   trSenRingBuffInfo *prSenRingBuffInfo;
   OSAL_trThreadAttribute rThreadAttr;

   /* Paramerer check*/
   if( (enDeviceID >= SEN_DISP_MAX_ID) || (prSenRingBuffConfig == OSAL_NULL) )
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
   }  
   /* This has to be done only once. bRingBuffInitFlag is used to ensure this.  */         
   else if( FALSE == rSenRngBufComInfo.bRingBuffInitFlag )
   {
      /* This Events is used to inform timeout events */
      if(OSAL_OK != OSAL_s32EventCreate( (tCString) SEN_RNGBUFF_TIMEOUT_EVENT_NAME,
                                         &rSenRngBufComInfo.h_SenRngBuffEvent) )
      {
         SenRngBuff_vTraceOut( TR_LEVEL_FATAL, "SEN_RNGBUFF_TIMEOUT_EVENT_NAME create Failed" );
      }
      else
      {
         AuxClockFd = OSAL_IOOpen( OSAL_C_STRING_DEVICE_AUXILIARY_CLOCK, OSAL_EN_READONLY );

         if( OSAL_ERROR  ==  AuxClockFd )
         {
            s32RetVal = OSAL_ERROR;
            SenRngBuff_vTraceOut( TR_LEVEL_ERRORS,
                       "SenRngBuff:Opening DevAuxclock failed with error = 0x%x",
                       OSAL_u32ErrorCode());
         }
         else
         {
            SenRngBuff_vTraceOut( TR_LEVEL_USER_4,
                       "SenRngBuff:DevAuxclock opened successfully.");
         }
         
         rThreadAttr.szName      = (tString)SEN_RNGBUFF_DATA_MONITOR_THREAD_NAME;
         rThreadAttr.s32StackSize = SEN_RNGBUFF_C_U32_THREAD_STACK_SIZE;
         rThreadAttr.u32Priority  = SEN_RNGBUFF_C_U32_THREAD_PRIORITY;
         rThreadAttr.pfEntry      = SenRngBuff_vMonitorThread;
         rThreadAttr.pvArg       = (tPVoid)OSAL_NULL;
         
         /* This thread handles   timeout events. In case of timeouts it will add error records to ring buffers.*/
         if( OSAL_ERROR == (OSAL_ThreadSpawn(&rThreadAttr)) )
         {
            SenRngBuff_vTraceOut( TR_LEVEL_FATAL,"SenRngBuff_vMonitorThread creation failed.Error code %lu",
                                                 OSAL_u32ErrorCode() );
            if(OSAL_OK != OSAL_s32EventDelete((tCString) SEN_RNGBUFF_TIMEOUT_EVENT_NAME ))
            {
               SenRngBuff_vTraceOut(TR_LEVEL_FATAL,"OSAL_s32EventDelete Failed for SEN_RNGBUFF_TIMEOUT_EVENT_NAME" );
            }
         }
         else
         {
            /* Resources needed for internal function of Ring buffer is done. */
            rSenRngBufComInfo.bRingBuffInitFlag = TRUE;
            rSenRngBufComInfo.bShutdownFlag = FALSE;
            s32RetVal = OSAL_OK;
         }
      }
   }
   else
   {
      /* Common ring buffer resources are created. You proceed with creation of
         driver specific resources*/
      s32RetVal = OSAL_OK;
   }

   /* This has to be done for every Sensor driver */
   if( ( OSAL_OK == s32RetVal ) && ( prSenRingBuffConfig != OSAL_NULL ) )
   {

      /*Allocate memory to store configuration of the sensor driver*/
      prSenRingBuffInfo = (trSenRingBuffInfo *)OSAL_pvMemoryAllocate( sizeof(trSenRingBuffInfo) );
      if( OSAL_NULL != prSenRingBuffInfo )
      {

         OSAL_pvMemorySet(prSenRingBuffInfo, 0, sizeof(trSenRingBuffInfo));

         /* Extract sensor info from parameter to config structure*/
         OSAL_pvMemoryCopy ( &prSenRingBuffInfo->rRingBuffConfig,
                             prSenRingBuffConfig,
                             sizeof(trSenRingBuffConfig) );
         /*Allocate memory for requesed number of bytes.
           Number of records * numer of bytes per record */
         prSenRingBuffInfo->pcSenData = (tPChar) OSAL_pvMemoryAllocate(
                                        (prSenRingBuffInfo->rRingBuffConfig.u32MaxRecords) * 
                                        (prSenRingBuffInfo->rRingBuffConfig.u32SizeOfEachRecord) );

         /* Has malloc returned a valid pointer ?  */
         if( OSAL_NULL == prSenRingBuffInfo->pcSenData)
         {
            SenRngBuff_vTraceOut(TR_LEVEL_FATAL,"Malloc failed line %d file %s", __LINE__, __FILE__);
            s32RetVal = OSAL_ERROR;
            free(prSenRingBuffInfo);
         }
         /* This is used to sync read and write events on memory */
         else if(OSAL_OK != (OSAL_s32SemaphoreCreate( rSensorDetails[enDeviceID].csDevRngBuffSemName,
                                                      &(prSenRingBuffInfo->hRngBuffUpdateSem),
                                                      1  )))
         {
            SenRngBuff_vTraceOut(TR_LEVEL_FATAL, "%s Sem creation failed Err: %lu", 
                                                  rSensorDetails[enDeviceID].csDevRngBuffSemName,
                                                  OSAL_u32ErrorCode() );
            s32RetVal = OSAL_ERROR;
            free(prSenRingBuffInfo->pcSenData);      
            free(prSenRingBuffInfo);
         }
         /* This timer expaires if data is not filled into ring buffer within timeout specified  */
         else if( OSAL_OK != (OSAL_s32TimerCreate( (OSAL_tpfCallback) SenRngBuff_vTimerCallBack,
                                                   (tPVoid) enDeviceID,
                                                   &(prSenRingBuffInfo->hDataTimeoutTimer ))) )
         {
            SenRngBuff_vTraceOut(TR_LEVEL_FATAL, "Timer creation failed for driver ID : %d Errror : %lu", 
                                                  enDeviceID,
                                                  OSAL_u32ErrorCode() );
            (tVoid)OSAL_s32SemaphoreClose( (prSenRingBuffInfo->hRngBuffUpdateSem) );
            OSAL_s32SemaphoreDelete( rSensorDetails[enDeviceID].csDevRngBuffSemName );
            free(prSenRingBuffInfo->pcSenData);      
            free(prSenRingBuffInfo);
            s32RetVal = OSAL_ERROR;
         }
         /* start timer */
         else if( OSAL_OK != OSAL_s32TimerSetTime( prSenRingBuffInfo->hDataTimeoutTimer,
                                                   prSenRingBuffInfo->rRingBuffConfig.u32DataTimeout,
                                                   prSenRingBuffInfo->rRingBuffConfig.u32DataTimeout ))
         {
            SenRngBuff_vTraceOut(TR_LEVEL_FATAL, "Set Time failed for driver ID : %d Errror : %lu", 
                                                  enDeviceID,
                                                  OSAL_u32ErrorCode() );
            (tVoid)OSAL_s32SemaphoreClose( (prSenRingBuffInfo->hRngBuffUpdateSem) );
            OSAL_s32SemaphoreDelete(rSensorDetails[ enDeviceID ].csDevRngBuffSemName);
            OSAL_s32TimerDelete(prSenRingBuffInfo->hDataTimeoutTimer);
            free(prSenRingBuffInfo->pcSenData);
            free(prSenRingBuffInfo);
            s32RetVal = OSAL_ERROR;
         }
         /* This event is needed to keep application therad waiting till atlease one record is available */
         else if(OSAL_OK != OSAL_s32EventCreate( rSensorDetails[ enDeviceID ].csReadEventName,
                                                 &prSenRingBuffInfo->hDataReadWaitEvent ))
         {
            SenRngBuff_vTraceOut(TR_LEVEL_FATAL,"SEN_RNGBUFF_READ_EVENT create Failed ID %d Err %lu",
                                                 enDeviceID,
                                                 OSAL_u32ErrorCode() );
            (tVoid)OSAL_s32SemaphoreClose( (prSenRingBuffInfo->hRngBuffUpdateSem) );
            OSAL_s32SemaphoreDelete( rSensorDetails[ enDeviceID ].csDevRngBuffSemName );
            OSAL_s32TimerDelete(prSenRingBuffInfo->hDataTimeoutTimer);
            free(prSenRingBuffInfo->pcSenData);
            free(prSenRingBuffInfo);
            s32RetVal = OSAL_ERROR;
         }
         else
         {
            /* This handle has to be returned when sensor driver calls open on ring buffer */
            rSensorDetails[ enDeviceID ].pHldRngBuf = prSenRingBuffInfo;
            rSensorDetails[ enDeviceID ].enRingBuffStatus = RING_BUFF_INITIALIZED;
         }
      }
      else
      {
         SenRngBuff_vTraceOut(TR_LEVEL_FATAL, "OSAL_pvMemoryAllocate Failed in Line %lu , file %s", __LINE__, __FILE__ );
         s32RetVal = OSAL_ERROR;
      }
   }

   return s32RetVal;
}


/********************************************************************************
* FUNCTION     : SenRingBuff_vAddOdoTimeoutRec
*
* PARAMETER    : tVoid
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : Add Odo Timeout Record to the ring buffer.
*
* HISTORY      : 21.JUL.2015| Initial Version   |Srinivas Prakash Anvekar (RBEI/ECF5)
**********************************************************************************/

static tVoid SenRingBuff_vAddOdoTimeoutRec( )
{
   static OSAL_trIOCtrlOdometerData rOdoTimeoutData;
      
   
   /* set values of timeout message*/
   rOdoTimeoutData.u32TimeStamp = rAuxClockTicks.u32Low;
   rOdoTimeoutData.enDirection = OSAL_EN_RFS_UNKNOWN;
   rOdoTimeoutData.enOdometerStatus = ODOMSTATE_DISCONNECTED;
   rSensorDetails[SEN_DISP_ODO_ID].u16ErrorCounter++;
   rOdoTimeoutData.u16ErrorCounter = rSensorDetails[SEN_DISP_ODO_ID].u16ErrorCounter;

   SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "ODO_DATA_TIMEOUT Occured; TimeStamp"
                       " %lu, WhlCnt %lu, Direction %d, Status %d, ErrCnt %lu", 
                       rOdoTimeoutData.u32TimeStamp, rOdoTimeoutData.u32WheelCounter,
                       rOdoTimeoutData.enDirection, rOdoTimeoutData.enOdometerStatus,
                       rOdoTimeoutData.u16ErrorCounter );

   /* Add timeout record to ring buffer */
   s32ErrChk = SenRingBuff_s32Write( SEN_DISP_ODO_ID,
                                     (tPVoid)&rOdoTimeoutData,
                                     1 );
   if( s32ErrChk !=  1)
   {
      SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "SenRingBuff_s32Write failed for Odo timeout record");
   }
      
  
}


/********************************************************************************
* FUNCTION     : SenRingBuff_vAddGyroTimeoutRec
*
* PARAMETER    : tVoid
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : Add Gyro Timeout Record to the ring buffer
*
* HISTORY      : 21.JUL.2015| Initial Version   |Srinivas Prakash Anvekar (RBEI/ECF5)
**********************************************************************************/


static tVoid SenRingBuff_vAddGyroTimeoutRec( )
{
   static OSAL_trIOCtrl3dGyroData rGyroTimeoutData;
      
   rGyroTimeoutData.u32TimeStamp = rAuxClockTicks.u32Low;
      
   /* Increment error counter*/
   rSensorDetails[SEN_DISP_GYRO_ID].u16ErrorCounter++;
   rGyroTimeoutData.u16ErrorCounter = rSensorDetails[SEN_DISP_GYRO_ID].u16ErrorCounter;

   SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "GYRO_DATA_TIMEOUT Timestamp : %lu, Occoured cnt %lu ",
                                          rGyroTimeoutData.u32TimeStamp, 
                                          rSensorDetails[SEN_DISP_GYRO_ID].u16ErrorCounter );

   /* Add timeout record to ring buffer */
   s32ErrChk = SenRingBuff_s32Write( SEN_DISP_GYRO_ID,
                                     (tPVoid)&rGyroTimeoutData,
                                     1 );
   if( s32ErrChk !=  1)
   {
      SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "SenRingBuff_s32Write failed for Gyro timeout record");
   }

}


/********************************************************************************
* FUNCTION     : SenRingBuff_vAddAccTimeoutRec
*
* PARAMETER    : tVoid
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : Add ACC timeout record to the ring buffer
*
* HISTORY      : 21.JUL.2015| Initial Version   |Srinivas Prakash Anvekar (RBEI/ECF5)
**********************************************************************************/


static tVoid SenRingBuff_vAddAccTimeoutRec( )
{
   static OSAL_trIOCtrlAccData rAccTimeoutData;
      
   rAccTimeoutData.u32TimeStamp =  rAuxClockTicks.u32Low;
      
   /* Increment error counter*/
   rSensorDetails[SEN_DISP_ACC_ID].u16ErrorCounter++;
   rAccTimeoutData.u16ErrorCounter = rSensorDetails[SEN_DISP_ACC_ID].u16ErrorCounter;

   SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "ACC_DATA_TIMEOUT Timestamp : %lu, Occoured cnt %lu",
                                          rAccTimeoutData.u32TimeStamp, 
                                          rSensorDetails[SEN_DISP_ACC_ID].u16ErrorCounter);

   /* Add timeout record to ring buffer */
   s32ErrChk = SenRingBuff_s32Write( SEN_DISP_ACC_ID,
                                     (tPVoid)&rAccTimeoutData,
                                     1 );
   if( s32ErrChk !=  1)
   {
      SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "SenRingBuff_s32Write failed for Acc timeout record");
   }

}



/********************************************************************************
* FUNCTION     : SenRingBuff_vAddAbsTimeoutRec
*
* PARAMETER    : tVoid
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : Add ABS timeout record to the ring buffer.
*
* HISTORY      : 21.JUL.2015| Initial Version   |Srinivas Prakash Anvekar (RBEI/ECF5)
**********************************************************************************/

static tVoid SenRingBuff_vAddAbsTimeoutRec( )
{
   static OSAL_trAbsData rAbsTimeoutData;
   
   /* Increment error counter*/
   rSensorDetails[SEN_DISP_ABS_ID].u16ErrorCounter++;
   
   rAbsTimeoutData.u32TimeStamp =  rAuxClockTicks.u32Low;
   rAbsTimeoutData.u16ErrorCounter = rSensorDetails[SEN_DISP_ABS_ID].u16ErrorCounter;
   rAbsTimeoutData.u32CounterFrontLeft  = 0;
   rAbsTimeoutData.u32CounterFrontRight = 0;
   rAbsTimeoutData.u32CounterRearRight  = 0;
   rAbsTimeoutData.u32CounterRearLeft   = 0;
   rAbsTimeoutData.u8DirectionFrontLeft  = OSAL_C_U8_ABS_DIR_UNKNOWN;
   rAbsTimeoutData.u8DirectionFrontRight = OSAL_C_U8_ABS_DIR_UNKNOWN;
   rAbsTimeoutData.u8DirectionRearLeft   = OSAL_C_U8_ABS_DIR_UNKNOWN;
   rAbsTimeoutData.u8DirectionRearRight  = OSAL_C_U8_ABS_DIR_UNKNOWN;
   rAbsTimeoutData.u8StatusFrontLeft     = OSAL_C_U8_ABS_STATUS_DISCONNECTED;
   rAbsTimeoutData.u8StatusFrontRight    = OSAL_C_U8_ABS_STATUS_DISCONNECTED; 
   rAbsTimeoutData.u8StatusRearLeft      = OSAL_C_U8_ABS_STATUS_DISCONNECTED;
   rAbsTimeoutData.u8StatusRearRight     = OSAL_C_U8_ABS_STATUS_DISCONNECTED;
   SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "ABS_DATA_TIMEOUT Timestamp : %lu, Occurred cnt %lu",
                                          rAbsTimeoutData.u32TimeStamp, 
                                          rAbsTimeoutData.u16ErrorCounter );

   /* Add time out record to ring buffer */
   s32ErrChk = SenRingBuff_s32Write( SEN_DISP_ABS_ID,
                                     (tPVoid)&rAbsTimeoutData,
                                     1 );
   if( s32ErrChk !=  1)
   {
      SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "SenRingBuff_s32Write failed for Abs time out record");
   }
}


/********************************************************************************
* FUNCTION     : SenRingBuff_vAddTimeoutRec
*
* PARAMETER    : u32EventResultMask - TimeOut Event Mask
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : Call respective Driver timeout record function based on eventmask.
*
* HISTORY      : 21.JUL.2015| Initial Version   |Srinivas Prakash Anvekar (RBEI/ECF5)
**********************************************************************************/

static tVoid SenRingBuff_vAddTimeoutRec( OSAL_tEventMask u32EventResultMask )
{

   if( u32EventResultMask & SEN_RNGBUFF_ODO_DATA_TIMEOUT_EVENT )
   {
      SenRingBuff_vAddOdoTimeoutRec( );
   }
   
   if( u32EventResultMask & SEN_RNGBUFF_GYRO_DATA_TIMEOUT_EVENT )
   {
      SenRingBuff_vAddGyroTimeoutRec( );
   }
   
   if( u32EventResultMask & SEN_RNGBUFF_ACC_DATA_TIMEOUT_EVENT )
   {
      SenRingBuff_vAddAccTimeoutRec( );
   }
   
   if( u32EventResultMask & SEN_RNGBUFF_ABS_DATA_TIMEOUT_EVENT )
   {
      SenRingBuff_vAddAbsTimeoutRec( );
   }
   
   
}

/********************************************************************************
* FUNCTION     : SenRngBuff_vMonitorThread
*
* PARAMETER    : pvDummyArg - Dummy argument
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  : Monitors ringbuffer for timeout events. In case of timeout events, a error 
*                will be added to ringbuffer.
*
* HISTORY      : 19.MAR.2013| Initial Version   |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
   static tVoid SenRngBuff_vMonitorThread (tPVoid pvDummyArg)
   {
      OSAL_tEventMask u32EventResultMask = 0; 
      
      (tVoid)pvDummyArg;
   
      /* monitor untill shutdown event */
      while( SEN_RNGBUFF_SHUTDOWN_EVENT != u32EventResultMask )
      {
         /* Wait for event */
         s32ErrChk = OSAL_s32EventWait( rSenRngBufComInfo.h_SenRngBuffEvent,
                                        ( (OSAL_tEventMask)SEN_RNGBUFF_GYRO_DATA_TIMEOUT_EVENT|SEN_RNGBUFF_ODO_DATA_TIMEOUT_EVENT|
                                        SEN_RNGBUFF_ACC_DATA_TIMEOUT_EVENT|SEN_RNGBUFF_SHUTDOWN_EVENT|SEN_RNGBUFF_ABS_DATA_TIMEOUT_EVENT ),
                                        OSAL_EN_EVENTMASK_OR,
                                        (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER,
                                        &u32EventResultMask );
   
   
         if(s32ErrChk == OSAL_OK)
         {
            s32ErrChk = OSAL_s32IORead( AuxClockFd,
                             (tPS8)&rAuxClockTicks,
                             sizeof(OSAL_trIOCtrlAuxClockTicks));
            if( s32ErrChk == OSAL_ERROR)
            {
               SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "ODO_DATA_TIMEOUT :AuxClock Read Failed" );
            }
            else
            {
               /*Clear the event */
               if(OSAL_OK != OSAL_s32EventPost( rSenRngBufComInfo.h_SenRngBuffEvent,~(u32EventResultMask),OSAL_EN_EVENTMASK_AND ))
               {
                  SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "OSAL_s32EventPost failed %u", __LINE__);
               }
      
               if( u32EventResultMask & SEN_RNGBUFF_SHUTDOWN_EVENT )
               {
                  SenRngBuff_vTraceOut(TR_LEVEL_SYSTEM,"Shutdown signalled in monitor thread");
               }
               else
               {
                  SenRingBuff_vAddTimeoutRec( u32EventResultMask );
               }
            }
         }
         else
         {
            SenRngBuff_vTraceOut( TR_LEVEL_ERRORS,"Event wait failed in SenRngBuff_vMonitorThread Err :%lu",
                                                   OSAL_u32ErrorCode() );
         }
      } 
   
      /* Close the AuxClock */
      if( OSAL_OK != OSAL_s32IOClose(AuxClockFd) )
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "OSAL_s32IOClose for Auxclock failed line %d ID %d ",
                                                 __LINE__,
                                                 AuxClockFd );
      }
      /* Release the event */
      else if( OSAL_OK != OSAL_s32EventClose( rSenRngBufComInfo.h_SenRngBuffEvent ))
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "OSAL_s32EventClose failed line %d Err %lu ",
                                                 __LINE__,
                                                 OSAL_u32ErrorCode() );
      }
      else if( OSAL_OK != OSAL_s32EventDelete( SEN_RNGBUFF_TIMEOUT_EVENT_NAME ))
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "OSAL_s32EventDelete failed line %d Err %lu ",
                                                 __LINE__,
                                                 OSAL_u32ErrorCode() );
      }
      else
      {
         rSenRngBufComInfo.h_SenRngBuffEvent = OSAL_NULL;
         /* This is needed because we may have to initialize ringbuffer */
         rSenRngBufComInfo.bShutdownFlag = FALSE;
      }
   
      SenRngBuff_vTraceOut(TR_LEVEL_USER_4,"Monitor thread exiting ...");
      OSAL_vThreadExit();
   }

/********************************************************************************
* FUNCTION    : SenRngBuff_vTimerCallBack
*
* PARAMETER   : tPVoid pvArg : Sensor Device ID- which timeout occoured
*
* RETURNVALUE : NONE
* DESCRIPTION : Informs ringbuffer monitor thread about timeout events.
*               This is a single callback function for timers of all drivers.
*               Based on the parameter it is identified as for which driver
*               timeout occoured.
*
* HISTORY     : 19.MAR.2013| Initial Version   |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

static void SenRngBuff_vTimerCallBack(tPVoid pvArg)
{

   tULong uSensorType = (tULong) pvArg;

   switch ( uSensorType )
   {
      /* Odo Timeout occoured */
      case SEN_DISP_ODO_ID:
      {
         if(OSAL_OK != OSAL_s32EventPost( rSenRngBufComInfo.h_SenRngBuffEvent,
                                          SEN_RNGBUFF_ODO_DATA_TIMEOUT_EVENT,
                                          OSAL_EN_EVENTMASK_OR ))
         {
            SenRngBuff_vTraceOut( TR_LEVEL_FATAL, "OSAL_s32EventPost failed %u", __LINE__ );
         }
         break;
      }
      /* GYRO Timeout occoured */
      case SEN_DISP_GYRO_ID:
      {

         if(OSAL_OK != OSAL_s32EventPost( rSenRngBufComInfo.h_SenRngBuffEvent,
                                          SEN_RNGBUFF_GYRO_DATA_TIMEOUT_EVENT,
                                          OSAL_EN_EVENTMASK_OR ))
         {
            SenRngBuff_vTraceOut( TR_LEVEL_FATAL, "OSAL_s32EventPost failed %u", __LINE__ );
         }

         break;
      }
      /* ACC Timeout occoured */
      case SEN_DISP_ACC_ID:
      {

         if(OSAL_OK != OSAL_s32EventPost( rSenRngBufComInfo.h_SenRngBuffEvent,
                                          SEN_RNGBUFF_ACC_DATA_TIMEOUT_EVENT,
                                          OSAL_EN_EVENTMASK_OR ))
         {
            SenRngBuff_vTraceOut( TR_LEVEL_FATAL, "OSAL_s32EventPost failed %u", __LINE__ );
         }

         break;
      }
      /* ABS Time out occurred */
      case SEN_DISP_ABS_ID:
      {
         if(OSAL_OK != OSAL_s32EventPost( rSenRngBufComInfo.h_SenRngBuffEvent,
                                          SEN_RNGBUFF_ABS_DATA_TIMEOUT_EVENT,
                                          OSAL_EN_EVENTMASK_OR ))
         {
            SenRngBuff_vTraceOut( TR_LEVEL_FATAL, "OSAL_s32EventPost failed %u", __LINE__ );
         }
         break;
      }
      default:
      {
         SenRngBuff_vTraceOut( TR_LEVEL_FATAL,"Default in switch line %u %s entry %d",
                                              __LINE__, __FUNCTION__, uSensorType );
         break;
      }
   }
   
}

/********************************************************************************
* FUNCTION   : SenRingBuff_s32Write
*
*PARAMETER   : tEnSenDrvID enDeviceID : For which sensor ring buffer data has to be added.
*              tPVoid pvData : Pointer to data to be written
*              tU32 u32NumOfEntries : Number of entries. (Not bytes)
*
*RETURNVALUE : Number of entries written on sucess
*              OSAL_ERROR in case of error
*
*DESCRIPTION : Trace out function for LSIM gpio.
*
* HISTORY    : 19.MAR.2013| Initial Version   |Madhu Kiran Ramachandra (RBEI/ECF5)
*---------------------------------------------------------------------------------------------------
* 15.OCT.2014 | Madhu Kiran Ramachandra (RBEI/ECF5)    | Post data ready even before semaphore 
*             |                                        | release so that read thread consumes the 
*             |                                        | posted event when data is read.
*             |                                        | Ticket: MY16-17478
*---------------------------------------------------------------------------------------------------
**********************************************************************************/

tS32 SenRingBuff_s32Write( tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32NumOfEntries )
{
   tS32 s32RetVal = OSAL_ERROR;
   trSenRingBuffInfo* prSenRngBuffInfo = rSensorDetails[enDeviceID].pHldRngBuf;
   tU32 u32WriteOffset = 0;
   tU32 u32ReadOffset = 0;
   tU32 u32LoopCount = 0;
   tPChar pCSenSource = (tPChar) pvData;

   /* Routine Parameter Check */
   if( (enDeviceID >= SEN_DISP_MAX_ID) || (OSAL_NULL == pvData) || (0 == u32NumOfEntries) )
   {
      SenRngBuff_vTraceOut( TR_LEVEL_ERRORS,"Invalid param to SenRingBuff_s32Write" );
      s32RetVal = OSAL_ERROR;
   }
   /* Write is allowed only after initialization */
   else if( rSensorDetails[enDeviceID].enRingBuffStatus == RING_BUFF_NOT_INITIALIZED )
   {
      SenRngBuff_vTraceOut(TR_LEVEL_ERRORS,"Ring buff not in open state. Present state :%d",
      rSensorDetails[enDeviceID].enRingBuffStatus );
      s32RetVal = OSAL_ERROR;   
   }
   /* Enter Protected Region */
   else if( OSAL_OK == OSAL_s32SemaphoreWait( prSenRngBuffInfo->hRngBuffUpdateSem ,
                                              (OSAL_tMSecond) SEN_RNGBUFF_WRITE_EVENTWAIT_TIME ))
   {
      /* If Read and Wirte indexes are same, shift read index and discard u32NumOfEntries to make space
               for new records*/
      if( prSenRngBuffInfo->u32AvaiRecords == prSenRngBuffInfo->rRingBuffConfig.u32MaxRecords)
      {
         /* Shift read index by number of entries to be written*/
         prSenRngBuffInfo->u32ReadIndex += u32NumOfEntries;
         /* Ring buffer read index should be less than max records */
         prSenRngBuffInfo->u32ReadIndex %= prSenRngBuffInfo->rRingBuffConfig.u32MaxRecords;
         /* Reduce the available records as some records are discarded */
         prSenRngBuffInfo->u32AvaiRecords -= u32NumOfEntries;
      }

      /* Copy record by record. */
      for( ; u32LoopCount < u32NumOfEntries; u32LoopCount++ )
      {
         /* Calculate Read and write offsets */
         u32WriteOffset = prSenRngBuffInfo->u32WriteIndex * prSenRngBuffInfo->rRingBuffConfig.u32SizeOfEachRecord;
         u32ReadOffset = u32LoopCount * prSenRngBuffInfo->rRingBuffConfig.u32SizeOfEachRecord;

         OSAL_pvMemoryCopy( (prSenRngBuffInfo->pcSenData +  u32WriteOffset),
                            (pCSenSource + u32ReadOffset),
                            prSenRngBuffInfo->rRingBuffConfig.u32SizeOfEachRecord );

         /* one record is copied. Update write index */
         
         prSenRngBuffInfo->u32WriteIndex = (prSenRngBuffInfo->u32WriteIndex + 1) %
                                            prSenRngBuffInfo->rRingBuffConfig.u32MaxRecords;
      }

      /* Copy is done. update number of available entries. */
      prSenRngBuffInfo->u32AvaiRecords += u32NumOfEntries;

      /* Available can never be greater that max records */
      if( prSenRngBuffInfo->u32AvaiRecords > prSenRngBuffInfo->rRingBuffConfig.u32MaxRecords )
      {
         prSenRngBuffInfo->u32AvaiRecords = prSenRngBuffInfo->rRingBuffConfig.u32MaxRecords;
      }
      
      /* A new entry is added. Sensor thread may be waiting for the data. So inform him that data is ready  */
      if(OSAL_OK != OSAL_s32EventPost( prSenRngBuffInfo->hDataReadWaitEvent,
                                       SEN_RNGBUFF_READ_READY_EVENT,
                                       OSAL_EN_EVENTMASK_OR ))
      {
         SenRngBuff_vTraceOut( TR_LEVEL_FATAL, "OSAL_s32EventPost failed %u", __LINE__ );
         s32RetVal = OSAL_ERROR;
      }
      
      /* Come out of protected region */
      if( OSAL_OK != OSAL_s32SemaphorePost(prSenRngBuffInfo->hRngBuffUpdateSem) )
      {
         SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "Sem Post Failed line %d Err: %lu",
                                                __LINE__, OSAL_u32ErrorCode());
      }
      else
      {
         /* Every thing Went as expected. */
         s32RetVal = OSAL_OK;
         SenRngBuff_vTraceOut(TR_LEVEL_USER_4, "%lu records written to RB from ID %d", 
                                                u32NumOfEntries, enDeviceID );
      }

      /* Reset timer that monitors whether data is added at regular intervals or not ? */
      if( OSAL_OK != OSAL_s32TimerSetTime( prSenRngBuffInfo->hDataTimeoutTimer,
                                           prSenRngBuffInfo->rRingBuffConfig.u32DataTimeout,
                                           prSenRngBuffInfo->rRingBuffConfig.u32DataTimeout ))
      {
         SenRngBuff_vTraceOut(TR_LEVEL_FATAL, "Set Time failed for driver ID : %d Errror : %lu", 
                                              enDeviceID,
                                              OSAL_u32ErrorCode() ); 
         s32RetVal = OSAL_ERROR;
      }

      if( OSAL_OK == s32RetVal )
      {
         /* Return the number of entries copied to ring buffer */
         s32RetVal = (tS32) u32NumOfEntries;
      }

   }
   else
   {
      SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "Sem wait Failed line %d Err: %lu",
                                            __LINE__, OSAL_u32ErrorCode());
   }

   return s32RetVal;
}


/********************************************************************************
* FUNCTION   : SenRingBuff_s32GetAvailRecords
*
* PARAMETER  : tEnSenDrvID enDeviceID: for which ring buffer, you are refering to.
*
*RETURNVALUE : Number of available entries in ring buffer - on Sucess
*              OSAL_ERROR on failure
*
*DESCRIPTION : This takes semaphore, gets number of available records from ringbuffer,
*              and releases semaphore. Very simple.
*
* HISTORY    : 19.MAR.2013| Initial Version   |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 SenRingBuff_s32GetAvailRecords( tEnSenDrvID enDeviceID )
{
   tS32 s32RetVal = OSAL_ERROR;
   trSenRingBuffInfo* prSenRngBuffInfo = rSensorDetails[enDeviceID].pHldRngBuf;

   if( enDeviceID < SEN_DISP_MAX_ID )
   {
      if( RING_BUFF_OPENED == rSensorDetails[enDeviceID].enRingBuffStatus )
      {
         /* Enter protected Region */
         if( OSAL_OK == OSAL_s32SemaphoreWait( prSenRngBuffInfo->hRngBuffUpdateSem ,
                                               (OSAL_tMSecond) SEN_RNGBUFF_READ_EVENTWAIT_TIME ))
         {
            /* Get number of entries available in ring buffer */
            s32RetVal = (tS32) prSenRngBuffInfo->u32AvaiRecords;

            /* Leave protected region  */
            if( OSAL_OK != OSAL_s32SemaphorePost( prSenRngBuffInfo->hRngBuffUpdateSem ) )
            {
               SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Sem Post Failed line %d Err: %lu",
                                                      __LINE__, OSAL_u32ErrorCode() );
               s32RetVal = OSAL_ERROR;
            }
         }
         else
         {
            SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Sem Wait Failed line %d Err: %lu",
                                                   __LINE__, OSAL_u32ErrorCode() );
         }
      }
      else
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Ring buffnot in open state. present state %d",
                                                 rSensorDetails[enDeviceID].enRingBuffStatus );
      }

   }
   else
   {
      SenRngBuff_vTraceOut( TR_LEVEL_ERRORS,"INvalid Sensor ID to SenRingBuff_s32GetAvailRecords" );
   }

   return s32RetVal;

}

/********************************************************************************
* FUNCTION   : SenRingBuff_s32Read
*
* PARAMETER  : tEnSenDrvID enDeviceID : From which ring buffer you want to read.
*              tPVoid pvData : Pointer to destination memory
*              tU32 u32NumOfEntries : Number of entries requested. (Not bytes)    
*
*RETURNVALUE : Number of entries read from ringbuffer.
*
*DESCRIPTION : 1 If requested entries are available, immediately they are copied to
*                buffer.
*              2 Else available number of entries are copied.
*              3 Else if not even a single entry is available, application thread is blocked
*                on a event with a timeout. If we get atleast one entry within timeout,
*                it is copied to application buffer. If we dont get even a single entry,
*                OSAL_E_TIMEOUT is returned. This is explisit requirement from VD-Sensor
*                as VD-Sensor does continous read without any delay.
*
* HISTORY    : 29.JAN.2013| Initial Version  |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 SenRingBuff_s32Read( tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32EntRequested )
{
   tS32 s32RetVal = OSAL_OK;
   trSenRingBuffInfo* prSenRngBuffInfo = rSensorDetails[enDeviceID].pHldRngBuf;
   tU32 u32WriteOffset = 0;
   tU32 u32ReadOffset = 0;
   tU32 u32LoopCount = 0;
   tPChar pCSenDst = (tPChar) pvData;
   tU32 u32EventResultMask = 0;
   tU32 u32EntToRead = 0;

   /* Routine Parameter Check */
   if( (enDeviceID >= SEN_DISP_MAX_ID) || (OSAL_NULL == pvData) || (0 == u32EntRequested) )
   {
      SenRngBuff_vTraceOut(TR_LEVEL_ERRORS,"Invalid param to SenRingBuff_s32Read" );
      s32RetVal = OSAL_ERROR;
   }
   /* Read is allowed only after opening the ring buffer */
   else  if( RING_BUFF_OPENED != rSensorDetails[enDeviceID].enRingBuffStatus )
   {
      SenRngBuff_vTraceOut( TR_LEVEL_ERRORS,"Device is not in opened state. Present state %d",
                                             rSensorDetails[enDeviceID].enRingBuffStatus );
      s32RetVal = OSAL_ERROR;
   }
   /* Enter Protected Region */
   else if( OSAL_OK == OSAL_s32SemaphoreWait( prSenRngBuffInfo->hRngBuffUpdateSem ,
                                              (OSAL_tMSecond) SEN_RNGBUFF_READ_EVENTWAIT_TIME ))
   {
      /* If we dont Have at least one record wait for timeout period */
      if( 0 == prSenRngBuffInfo->u32AvaiRecords )
      {

         /* Leave protected region  */
         if( OSAL_OK != OSAL_s32SemaphorePost( prSenRngBuffInfo->hRngBuffUpdateSem ))
         {
            SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Sem Post Failed line %d Err: %lu",
                                                    __LINE__, OSAL_u32ErrorCode() );
         }

         /* Wait for at least one record */
         else if( OSAL_OK != OSAL_s32EventWait( prSenRngBuffInfo->hDataReadWaitEvent,
                                                (OSAL_tEventMask)SEN_RNGBUFF_READ_READY_EVENT,
                                                OSAL_EN_EVENTMASK_OR,
                                                (OSAL_tMSecond)prSenRngBuffInfo->rRingBuffConfig.u32DataTimeout,
                                                &u32EventResultMask) )
         {
            SenRngBuff_vTraceOut( TR_LEVEL_ERRORS,"Event Data wait timed out line %d Err: %lu for ID : %d",
                                                   __LINE__,
                                                   OSAL_u32ErrorCode(),
                                                   enDeviceID );
            if( OSAL_E_TIMEOUT == OSAL_u32ErrorCode() )
            {
               s32RetVal = OSAL_E_TIMEOUT;
            }
            else
            {
               s32RetVal = OSAL_ERROR;
            }
         }
         /* Enter Protected Region */
         else if( OSAL_OK != OSAL_s32SemaphoreWait( prSenRngBuffInfo->hRngBuffUpdateSem ,
                                                   (OSAL_tMSecond) SEN_RNGBUFF_READ_EVENTWAIT_TIME ))
         {
            SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "sem wait Failed line %d Err: %lu for ID : %d",
                                                    __LINE__,
                                                    OSAL_u32ErrorCode(),
                                                    enDeviceID );
            s32RetVal = OSAL_ERROR;
         }
      }

      if( (OSAL_OK == s32RetVal) )
      {
         /* Atleast now do we have minimum of one record.*/
         /* Also if closed, dont read. Just return error  */
         if( ( prSenRngBuffInfo->u32AvaiRecords > 0 ) &&
               ( RING_BUFF_OPENED == rSensorDetails[enDeviceID].enRingBuffStatus ) )
         {
            /* Now do we have requested number of entries */
            if( u32EntRequested < prSenRngBuffInfo->u32AvaiRecords )
            {
               u32EntToRead = u32EntRequested;
            }
            /* We shall read at-least available entries  */
            else
            {
               u32EntToRead = prSenRngBuffInfo->u32AvaiRecords;
            }

            /* Copy record by record. Copy at once is not possible in ring buffer*/
            for( ; u32LoopCount < u32EntToRead; u32LoopCount++ )
            {
               /* Calculate Read and write offsets */
               u32ReadOffset = prSenRngBuffInfo->u32ReadIndex * prSenRngBuffInfo->rRingBuffConfig.u32SizeOfEachRecord;
               u32WriteOffset = u32LoopCount * prSenRngBuffInfo->rRingBuffConfig.u32SizeOfEachRecord;

               OSAL_pvMemoryCopy( (pCSenDst + u32WriteOffset),
                                  (prSenRngBuffInfo->pcSenData + u32ReadOffset),
                                  prSenRngBuffInfo->rRingBuffConfig.u32SizeOfEachRecord );

               /* one record is copied. Update Read index */
               prSenRngBuffInfo->u32ReadIndex = (prSenRngBuffInfo->u32ReadIndex + 1) %
               prSenRngBuffInfo->rRingBuffConfig.u32MaxRecords;
            }

            /* Copy is done. update number of available entries. */
            prSenRngBuffInfo->u32AvaiRecords -= u32EntToRead;
            /*Clear the event after reading from RB*/
            if(OSAL_OK != OSAL_s32EventPost( prSenRngBuffInfo->hDataReadWaitEvent,
                                             ~(SEN_RNGBUFF_READ_READY_EVENT),OSAL_EN_EVENTMASK_AND) )
            {
               SenRngBuff_vTraceOut( TR_LEVEL_FATAL, "OSAL_s32EventPost failed %u", __LINE__ );   
            }

            SenRngBuff_vTraceOut( TR_LEVEL_USER_4," %lu records read from RB from ID %d",
                                                    u32EntToRead, enDeviceID );
            /* Always return number of entries read not requested */
            s32RetVal = (tS32) u32EntToRead;
         }
         else
         {
            /* Even after waiting we dont have atleast one record. */
            s32RetVal = OSAL_ERROR;
            SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Err line %lu", __LINE__ );
         }
         /* Leave protected region  */
         if( OSAL_OK != OSAL_s32SemaphorePost( prSenRngBuffInfo->hRngBuffUpdateSem ) )
         {
            SenRngBuff_vTraceOut(TR_LEVEL_ERRORS, "Sem Post Failed line %d Err: %lu",
                                                   __LINE__, OSAL_u32ErrorCode());
            s32RetVal = OSAL_ERROR;
         }
      }
   }
   else
   {
      SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Sem wait Failed line %d Err: %lu",
                                              __LINE__, OSAL_u32ErrorCode() );
      s32RetVal = OSAL_ERROR;
   }

   return s32RetVal;
}

/********************************************************************************
*FUNCTION    : SenRingBuff_s32Open
*
*PARAMETER   : tEnSenDrvID enDeviceID : For which sensor driver device is opened.
*
*RETURNVALUE : OSAL_OK on sucess
*              OSAL_ERROR on error
*
*DESCRIPTION : An interface to ring buffer to know that someone is about to read/write data
*              from ring buffer
*
* HISTORY    : 19.MAR.2013| Initial Version  |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 SenRingBuff_s32Open( tEnSenDrvID enDeviceID )
{
   tS32 s32RetVal = OSAL_ERROR;

   /* Parameter check */
   if( SEN_DISP_MAX_ID > enDeviceID )
   {
      /* Only after initialization, it is allowed to open device  */
      if(( RING_BUFF_INITIALIZED == rSensorDetails[enDeviceID].enRingBuffStatus ) ||
         ( RING_BUFF_CLOSED == rSensorDetails[enDeviceID].enRingBuffStatus      ))
      {
         s32RetVal = OSAL_OK;
         /* Change the state of device to opened. This is used to handle read/close/deinit */
         rSensorDetails[enDeviceID].enRingBuffStatus = RING_BUFF_OPENED;
         SenRngBuff_vTraceOut( TR_LEVEL_USER_4, "SenRingBuff_s32Open called for ID %d", enDeviceID );
      }
      else
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Ring buff is not initialized for %d", enDeviceID );
      }
   }
   else
   {
      SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "SenRingBuff_s32Open Invalid Param" );
   }

   return s32RetVal;
}
/********************************************************************************
*FUNCTION    : SenRingBuff_s32Close
*
*PARAMETER   : tEnSenDrvID enDeviceID : For which sensor driver, close is called.                                                      
*
*RETURNVALUE : OSAL_OK on sucess
*              OSAL_ERROR on error
*
*DESCRIPTION : An interface to ring buffer to know that the buffer will not be used by
*              a specific driver. Also this is used by ring buffer to free application
*              thread waiting for data in read function.
*
*HISTORY     : 19.MAR.2013| Initial Version   |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 SenRingBuff_s32Close( tEnSenDrvID enDeviceID )
{
   tS32 s32RetVal = OSAL_ERROR;
   trSenRingBuffInfo *prSenRngBuffInfo = rSensorDetails[enDeviceID].pHldRngBuf;

   /* Parameter check */
   if( enDeviceID >= SEN_DISP_MAX_ID )
   {
      SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Invalid device ID to SenRingBuff_s32Close" );
   }
   /* Close is allowed only if device is in open state */
   else if( RING_BUFF_OPENED == rSensorDetails[enDeviceID].enRingBuffStatus ) 
   {
      SenRngBuff_vTraceOut( TR_LEVEL_USER_4, "SenRingBuff_s32Close called for ID %d",
      enDeviceID);
      /* This semaphore is needed to avoid access of ring buffer memory after de-allocating it.
         A thread may use prSenRngBuffInfo pointer in read calleven after de-init of ring buffer.
         Taking this semaphore will ensure that above isue wont happen. Ring buffer state is updated
         to closed here to ensure this.*/
      if( OSAL_OK != OSAL_s32SemaphoreWait( prSenRngBuffInfo->hRngBuffUpdateSem, SEN_RNGBUFF_SHUTDOWN_EVENTWAIT_TIME ))
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Sem wait failed in close ID %d Err %lu ",
                                                 enDeviceID, OSAL_u32ErrorCode() );
      }
      else
      {
         rSensorDetails[enDeviceID].enRingBuffStatus = RING_BUFF_CLOSED;

         if( OSAL_OK != OSAL_s32SemaphorePost( prSenRngBuffInfo->hRngBuffUpdateSem) )
         {
            SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Sem post failed in close ID %d Err %lu ",
                                                    enDeviceID, OSAL_u32ErrorCode() );
         }
      }
      
      /* A thread may be waiting in ring buffer read for read ready event. Release it  */
      if(OSAL_OK != OSAL_s32EventPost( prSenRngBuffInfo->hDataReadWaitEvent,
                                       SEN_RNGBUFF_READ_READY_EVENT,
                                       OSAL_EN_EVENTMASK_OR) )
      {
         SenRngBuff_vTraceOut( TR_LEVEL_FATAL, "OSAL_s32EventPost failed %u", __LINE__ );
         s32RetVal = OSAL_ERROR;
      }
      else
      {
         /* Close is sucessfull now */
         s32RetVal = OSAL_OK;
      }
   }
   else
   {
      SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "ring buff close fail. Not in open state. present state %d ",
                                              rSensorDetails[enDeviceID].enRingBuffStatus );
   }

   return s32RetVal;
}

/********************************************************************************
*FUNCTION    : SenRingBuff_s32DeInit
*
*PARAMETER   : tEnSenDrvID enDeviceID: for which driver resources are to be released.     
*
*RETURNVALUE : OSAL_OK on sucess
*              OSAL_ERROR on failure
*DESCRIPTION : Trace out function for LSIM gpio.
*
*HISTORY     : 29.JAN.2013| Initial Version   |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 SenRingBuff_s32DeInit( tEnSenDrvID enDeviceID )
{
   tS32 s32RetVal = OSAL_OK;
   trSenRingBuffInfo *prSenRingBuffInfo = rSensorDetails[enDeviceID].pHldRngBuf;

   rSenRngBufComInfo.bShutdownFlag = TRUE;

   if( SEN_DISP_MAX_ID > enDeviceID )
   {
      /* Release all driver specific resources */
      /* Delete Timer */
      if( OSAL_OK != OSAL_s32TimerDelete( prSenRingBuffInfo->hDataTimeoutTimer ))
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "OSAL_s32TimerDelete failed line %d ID %d ",
                                                 __LINE__,
                                                 enDeviceID );
         s32RetVal = OSAL_ERROR;
      }
      /* Release read wait event */
      if( OSAL_OK != OSAL_s32EventClose( prSenRingBuffInfo->hDataReadWaitEvent ))
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "OSAL_s32EventClose failed line %d ID %d ",
                                                 __LINE__,
                                                 enDeviceID );
         s32RetVal = OSAL_ERROR;
      }

      else if( OSAL_OK != OSAL_s32EventDelete( rSensorDetails[enDeviceID].csReadEventName ))
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "OSAL_s32EventDelete failed line %d ID %d ",
                                                 __LINE__,
                                                 enDeviceID );
         s32RetVal = OSAL_ERROR;
      }
      /* Release read/write sync semaphore */
      if( OSAL_OK != OSAL_s32SemaphoreClose( prSenRingBuffInfo->hRngBuffUpdateSem ))
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "OSAL_s32SemaphoreClose failed line %d ID %d ",
                                                 __LINE__,
                                                 enDeviceID );
         s32RetVal = OSAL_ERROR;
      }
      else if( OSAL_OK != OSAL_s32SemaphoreDelete( rSensorDetails[enDeviceID].csDevRngBuffSemName ))
      {
         SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "OSAL_s32SemaphoreDelete failed line %d ID %d ",
                                                 __LINE__,
                                                 enDeviceID );
         s32RetVal = OSAL_ERROR;
      }
      /* Make data timeout error counter zero */
      rSensorDetails[enDeviceID].u16ErrorCounter = 0;
      /* Ring buffer is no more useable untill initialized again */
      rSensorDetails[enDeviceID].enRingBuffStatus = RING_BUFF_NOT_INITIALIZED;
      /* Free ring buffer data storage memory */
      free( prSenRingBuffInfo->pcSenData );
      /* Free ring buffer config info structure */
      free( prSenRingBuffInfo);

      /*Now if none of the ring buffers are in opened and initialized (May be opened) state,
         clean up common ring buffer resources*/
      if( (rSensorDetails[SEN_DISP_ODO_ID].enRingBuffStatus  != RING_BUFF_INITIALIZED) &&
          (rSensorDetails[SEN_DISP_ODO_ID].enRingBuffStatus  != RING_BUFF_OPENED)      &&
          (rSensorDetails[SEN_DISP_GYRO_ID].enRingBuffStatus != RING_BUFF_INITIALIZED) &&
          (rSensorDetails[SEN_DISP_GYRO_ID].enRingBuffStatus != RING_BUFF_OPENED)      &&
          (rSensorDetails[SEN_DISP_ACC_ID].enRingBuffStatus  != RING_BUFF_INITIALIZED) &&
          (rSensorDetails[SEN_DISP_ACC_ID].enRingBuffStatus  != RING_BUFF_OPENED)      &&
          (rSensorDetails[SEN_DISP_ABS_ID].enRingBuffStatus  != RING_BUFF_INITIALIZED) &&
          (rSensorDetails[SEN_DISP_ABS_ID].enRingBuffStatus  != RING_BUFF_OPENED)      &&
          (TRUE == rSenRngBufComInfo.bRingBuffInitFlag) )
      {
         rSenRngBufComInfo.bRingBuffInitFlag = FALSE;
         /* Signal shutdown event to monitor thread  */
         if(OSAL_OK != OSAL_s32EventPost( rSenRngBufComInfo.h_SenRngBuffEvent,
                                          SEN_RNGBUFF_SHUTDOWN_EVENT,
                                          OSAL_EN_EVENTMASK_OR) )
         {
            SenRngBuff_vTraceOut( TR_LEVEL_FATAL, "OSAL_s32EventPost failed %u", __LINE__ );
         }
      }
      
   }
   else
   {
      SenRngBuff_vTraceOut( TR_LEVEL_ERRORS, "Invalid param to SenRingBuff_s32DeInit" );
      s32RetVal = OSAL_ERROR;
   }

   return s32RetVal;

}

/********************************************************************************
*FUNCTION    : SenRngBuff_vTraceOut
*
*PARAMETER   : u32Level - trace level
*              pcFormatString - Trace string     
*
*RETURNVALUE : tU32 error codes
*
*DESCRIPTION : Trace out function for Ring buffer.
*
*HISTORY     : 19.MAR.2013| Initial Version |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tVoid SenRngBuff_vTraceOut(  TR_tenTraceLevel enTraceLevel, const tChar *pcFormatString,... )
{

   tU32 u32Level = (tU32)enTraceLevel;
      
   if(LLD_bIsTraceActive( OSAL_C_TR_CLASS_SENSOR_RINGBUFFER, u32Level ) != FALSE)
   {
      /*
      Parameter to hold the argument for a function, specified the format
      string in pcFormatString
      defined as:
      typedef char* va_list in stdarg.h
      */
      va_list argList;
      /*
      vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure
      */
      tS32 s32Size ;
      /*
      Buffer to hold the string to trace out
      */
      tS8 u8Buffer[MAX_TRACE_SIZE];
      /*
      Position in buffer from where the format string is to be
      concatenated
      */
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /*
      Flush the String
      */
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /*
      Copy the String to indicate the trace is from the RTC device
      */

      /*
      Initialize the argList pointer to the beginning of the variable
      arguement list
      */
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
      Collect the format String's content into the remaining part of
      the Buffer
      */
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,
                  sizeof(u8Buffer),
                  pcFormatString,
                  argList ) ) )
      {
         return;
      }

      /*
      Trace out the Message to TTFis
      */
      LLD_vTrace( OSAL_C_TR_CLASS_SENSOR_RINGBUFFER,
      u32Level,
      u8Buffer,
      (tU32)s32Size );   /* Send string to Trace*/
      /*
      Performs the appropiate actions to facilitate a normal return by a
      function that has used the va_list object
      */
      va_end(argList);
   }
}

/* End of file */
