/******************************************************************************
* FILE         : sensor_ring_buffer.h
*
*
* DESCRIPTION  : This is the header file for sensor_ring_buffer.c
*
* AUTHOR(s)    : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 19.MAR.2012|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#ifndef SENSOR_RING_BUFFER_HEADER

#define SENSOR_RING_BUFFER_HEADER

typedef struct
{
   tU32 u32DataTimeout;
   tU32 u32MaxRecords;
   tU32 u32SizeOfEachRecord;
}trSenRingBuffConfig;

typedef tU32 Sen_tRngBuffHandle;

tS32 SenRingBuff_s32Init( tEnSenDrvID enDeviceID, const trSenRingBuffConfig *prSenRingBuffConfig );
tS32 SenRingBuff_s32Write( tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32NumOfEntries );
tS32 SenRingBuff_s32Read( tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32EntRequested );
tS32 SenRingBuff_s32GetAvailRecords( tEnSenDrvID enDeviceID ); 
tS32 SenRingBuff_s32Open( tEnSenDrvID enDeviceID );
tS32 SenRingBuff_s32Close( tEnSenDrvID enDeviceID ); 
tS32 SenRingBuff_s32DeInit( tEnSenDrvID enDeviceID );

#endif

