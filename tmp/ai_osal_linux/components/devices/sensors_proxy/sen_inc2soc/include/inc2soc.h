/******************************************************************************
* FILE            : inc2soc.h
*
* DESCRIPTION     : This is the header file for inc2soc.c
*
* AUTHOR(s)       : Kulkarni Ramchandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 19.FEB.2016|  Initial version 1.0  | Kulkarni Ramchandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#ifndef INC2SOC_HEADER
#define INC2SOC_HEADER

/*************************************************************************
* Header files
*-----------------------------------------------------------------------*/

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>


/*************************************************************************
* PoS specific Macro declarations
*-----------------------------------------------------------------------*/

#define SEN_DISP_TDPY_SEM_WAIT_DRIV_DLY                      (OSAL_tMSecond)(10)


#define SEN_EVENT_DRIV_WRITE_COMPLETE                        ((OSAL_tEventMask)0x01)
#define SEN_EVENT_DRIV_SHUTDOWN                              ((OSAL_tEventMask)0x02)


/*************************************************************************
* GNSS specific Macro declarations
*-----------------------------------------------------------------------*/

#define GNSS_TDPY_SEM_WAIT_DRIV_DLY                  (OSAL_tMSecond)(10)


#define GNSS_EVENT_DRIV_WRITE_COMPLETE               ((OSAL_tEventMask)0x01)
#define GNSS_EVENT_DRIV_SHUTDOWN                     ((OSAL_tEventMask)0x02)


/*************************************************************************
* Datatype declaration
*-----------------------------------------------------------------------*/
typedef enum
{
   INC2SOC_DEV_TYPE_GNSS = 0,
   INC2SOC_DEV_TYPE_POS  = 1,

   INC2SOC_NUM_OF_DEVICES
}tEnInc2SocDev;


// Holds the thread status
typedef enum
{
   INC2SOC_THREAD_SHUTDOWN = 0,
   INC2SOC_CLIENT_DISCONNECTED = 1,
   INC2SOC_CLIENT_CONNECTED = 2
}tEnInc2SocThrStatus;


/*************************************************************************
* Function declarations (Scope : Global)
*-----------------------------------------------------------------------*/

// These interfaces are used by both proxy driver and INC forwarder
tS32 Inc2Soc_s32CreateResources( tEnInc2SocDev enDeviceType );
tVoid Inc2Soc_vWriteDataToBuffer( tEnInc2SocDev enDeviceType );

tVoid Inc2Soc_vPostDrivShutdown( tEnInc2SocDev enDeviceType );

#endif

/* End of file */
