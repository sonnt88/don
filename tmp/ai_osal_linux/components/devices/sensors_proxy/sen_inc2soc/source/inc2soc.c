/*************************************************************************
* Header files
*-----------------------------------------------------------------------*/
#include <poll.h>

#include "dev_gnss_types.h"
#include "sensor_dispatcher_types.h"
#include "sensor_dispatcher.h"
#include "inc2soc.h"

/*************************************************************************
* PoS specific Macro declarations
*-----------------------------------------------------------------------*/

#define SEN_DISP_TDPY_SOCKET_WRITE_THREAD_NAME               "TPSenThr"
#define SEN_DISP_TDPY_SOCKET_WRITE_THREAD_PRIORITY           (OSAL_C_U32_THREAD_PRIORITY_HIGHEST)
#define SEN_DISP_TDPY_SOCKET_WRITE_THREAD_STACKSIZE          (2048)


#define SEN_DISP_TDPY_SOCKET_PATH                            (tCString)"/tmp/sock_sensors"
#define MAX_SENSOR_DATA_SIZE_TDPY                            (10240)
#define SEN_DISP_TDPY_MSG_HEADER_SIZE                        (MSGID_SIZE + SEN_DISP_FIELD_SIZE_NUM_ENTRTIES)

#define SEN_TDPY_SEMAPHORE_NAME                              (tCString)"TPSenSem"
#define SEN_TDPY_EVENT_NAME                                  (tCString)"TPSenEve"


/*************************************************************************
* GNSS specific Macro declarations
*-----------------------------------------------------------------------*/
#define GNSS_TDPY_SOCKET_WRITE_THREAD_NAME           "TPGnssThr"
#define GNSS_TDPY_SOCKET_WRITE_THREAD_PRIORITY       (OSAL_C_U32_THREAD_PRIORITY_HIGHEST)
#define GNSS_TDPY_SOCKET_WRITE_THREAD_STACKSIZE      (2048)

#define GNSS_TDPY_SOCKET_PATH                        (tCString)"/tmp/sock_gnss"
#define MAX_GNSS_DATA_SIZE_TDPY                      (4096)

#define GNSS_TDPY_SEMAPHORE_NAME                     (tCString)"TPGnssSem"
#define GNSS_TDPY_EVENT_NAME                         (tCString)"TPGnssEve"


/*************************************************************************
* Common Macro declarations
*-----------------------------------------------------------------------*/
#define INC2SOC_PF_SOCK_TYPE                           (tS32)AF_UNIX
#define INC2SOC_MAX_NUM_OF_CLIENTS                     (1)
#define INC2SOC_NUM_OF_POLL_FDS                        (1)
#define INC2SOC_ACCEPT_POLL_TIMEOUT                    (OSAL_tMSecond)(100)


/*************************************************************************
* Datatype declaration
*-----------------------------------------------------------------------*/

// Holds the information on socket used to send data via unix socket
typedef struct
{
   tS32 s32ServerFD;                      /* Handle for server socket  */
   tS32 s32ClientFD;                      /* Handle for client Navi socket */
   struct sockaddr_un rSerSocAdd;         /* Server socket */
   struct sockaddr_un rCliSocAdd;         /* Client socket */
   socklen_t u32SocAddrLength;                 /* Holds the sizeof struct socket_un */
}trUnixSockInfo;


// Holds the type of resource. Consecutive numbers should be assigned to resources
typedef enum
{
   INC2SOC_RES_SEM = 0,
   INC2SOC_RES_EVENT = 1,
   INC2SOC_RES_SOCK = 2,
   INC2SOC_RES_MEM = 4,

   INC2SOC_RES_ALL
}tEnInc2SocResType;

// Holds auxillary data required for each device
typedef struct 
{
   tCString csSemName;
   tCString csEventName;
   OSAL_trThreadAttribute rIncFwThreadAttr;
}trInc2SocAuxInfo;

// Device handle
typedef struct
{
   // Critical section. Synchronized using event and semaphore
   tPU8 pu8SendBuffer;                                    /* pointer to buffer */
   tU32 u32WriteIndex;                                    /* Holds location where next message will be written */

   //Event and Semaphore
   OSAL_tSemHandle hSemaphore;                            /* For read-write synchronization */
   OSAL_tEventHandle hEvent;                              /* For read-write synchronization */


   //Auxillary info 
   tEnInc2SocDev enDevice;
   trInc2SocAuxInfo rAuxInfo;
   trUnixSockInfo rSockInfo;
   tEnInc2SocThrStatus enThrStatus;                      /* Holds the thread status for each device */

}trInc2SocHandle;



/*************************************************************************
* Function declarations (scope: Local to file)
*-----------------------------------------------------------------------*/
static tS32 Inc2Soc_s32SemInit( tEnInc2SocDev enDeviceType );
static tVoid Inc2Soc_vSemDeInit( tEnInc2SocDev enDeviceType );
static tS32 Inc2Soc_s32EventInit( tEnInc2SocDev enDeviceType );
static tVoid Inc2Soc_vEventDeInit( tEnInc2SocDev enDeviceType );
static tS32 Inc2Soc_s32SockInit( tEnInc2SocDev enDeviceType );
static tVoid Inc2Soc_vSockDeInit( tEnInc2SocDev enDeviceType );


static tS32 Inc2Soc_s32InitDevHandle ( tEnInc2SocDev enDeviceType );
static tVoid Inc2Soc_vReleaseResources( tEnInc2SocDev enDeviceType, tEnInc2SocResType enResType );

static tVoid Inc2Soc_vEnterCriticalSection( tEnInc2SocDev enDeviceType, tU32 u32LineNum, tU32 u32Timeout );
static tVoid Inc2Soc_vLeaveCriticalSection( tEnInc2SocDev enDeviceType, tU32 u32LineNum );
static tU32 Inc2Soc_vWaitOnEvent( tEnInc2SocDev enDeviceType, tU32 u32Mask, tU32 u32Timeout, tU32 u32LineNum );
static tVoid Inc2Soc_vPostEvent( tEnInc2SocDev enDeviceType, tU32 u32Mask, OSAL_tenEventMaskFlag enMaskFlag, tU32 u32LineNum );


static tVoid Inc2Soc_pvGnssThread( tPVoid pvArg);
static tVoid Inc2Soc_pvPoSThread( tPVoid pvArg );
static tVoid Inc2Soc_vWriteGnssDataToBfr( tVoid );
static tVoid Inc2Soc_vWritePoSDataToBfr( tVoid );


/*************************************************************************
* Variable declarations (scope: Global)
*-----------------------------------------------------------------------*/
extern trGnssProxyInfo rGnssProxyInfo;
extern trSensorDispInfo rSensorDispInfo;

trInc2SocHandle rInc2SocDevHandle[ INC2SOC_NUM_OF_DEVICES ];



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_s32InitDevHandle
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*
* RETURNVALUE : OSAL_OK  on success
*                       OSAL_ERROR on Failure
*
* DESCRIPTION : This is the initialization function for INC forwarder.
*               1. Allocates memory required for PoS and GNSS buffers when called using respective handles.
*               2. Initializes semaphore and event required for synchronization of critical section.
*               3. Initializes socket attributes.
*               4. Initializes thread attributes.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 Inc2Soc_s32InitDevHandle ( tEnInc2SocDev enDeviceType )
{
   tS32 s32RetVal = OSAL_OK;

   switch ( enDeviceType )
   {
      case INC2SOC_DEV_TYPE_GNSS : 
         {
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].enDevice = INC2SOC_DEV_TYPE_GNSS;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].pu8SendBuffer = (tPU8)OSAL_pvMemoryAllocate(MAX_GNSS_DATA_SIZE_TDPY*sizeof(tU8));
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].rAuxInfo.csSemName = GNSS_TDPY_SEMAPHORE_NAME;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].rAuxInfo.csEventName = GNSS_TDPY_EVENT_NAME;

            // socket related initializations
            (tVoid)OSAL_szStringNCopy( rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].rSockInfo.rSerSocAdd.sun_path, 
                   GNSS_TDPY_SOCKET_PATH, 
                  (sizeof(rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].rSockInfo.rSerSocAdd.sun_path) - 1));

            // thread related initializations
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].rAuxInfo.rIncFwThreadAttr.szName         = GNSS_TDPY_SOCKET_WRITE_THREAD_NAME;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].rAuxInfo.rIncFwThreadAttr.u32Priority    = GNSS_TDPY_SOCKET_WRITE_THREAD_PRIORITY;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].rAuxInfo.rIncFwThreadAttr.s32StackSize   = GNSS_TDPY_SOCKET_WRITE_THREAD_STACKSIZE;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].rAuxInfo.rIncFwThreadAttr.pfEntry        = Inc2Soc_pvGnssThread;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].rAuxInfo.rIncFwThreadAttr.pvArg          = OSAL_NULL;
        }
         break;

      case INC2SOC_DEV_TYPE_POS : 
         {
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].enDevice = INC2SOC_DEV_TYPE_POS;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].pu8SendBuffer = (tPU8)OSAL_pvMemoryAllocate(MAX_SENSOR_DATA_SIZE_TDPY*sizeof(tU8));
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].rAuxInfo.csSemName = SEN_TDPY_SEMAPHORE_NAME;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].rAuxInfo.csEventName = SEN_TDPY_EVENT_NAME;

            // socket related initializations
            (tVoid)OSAL_szStringNCopy( rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].rSockInfo.rSerSocAdd.sun_path, 
                   SEN_DISP_TDPY_SOCKET_PATH, 
                  (sizeof(rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].rSockInfo.rSerSocAdd.sun_path) - 1));

            // thread related initializations
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].rAuxInfo.rIncFwThreadAttr.szName         = SEN_DISP_TDPY_SOCKET_WRITE_THREAD_NAME;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].rAuxInfo.rIncFwThreadAttr.u32Priority    = SEN_DISP_TDPY_SOCKET_WRITE_THREAD_PRIORITY;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].rAuxInfo.rIncFwThreadAttr.s32StackSize   = SEN_DISP_TDPY_SOCKET_WRITE_THREAD_STACKSIZE;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].rAuxInfo.rIncFwThreadAttr.pfEntry        = Inc2Soc_pvPoSThread;
            rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].rAuxInfo.rIncFwThreadAttr.pvArg          = OSAL_NULL;
         }
         break;

      default : 
         {
            s32RetVal = OSAL_ERROR;
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                              "INC2SOC: Invalid Device type" );
         }
         break;
   }

   return s32RetVal;
}


/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_s32CreateResources
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*
* RETURNVALUE : OSAL_OK  on success
*                       OSAL_ERROR on Failure
*
* DESCRIPTION : .Creates resources required for INC forwarder of device enDeviceType.
*               1. Calls Init function to initialize device handle.
*               2. Initializes WriteIndex, ThreadStatus before they are accessed as critical section.
*               3. Creates semaphore, event, socket and spawns the INC forwarder thread.
*               In case of an error, it releases all resources occupied and returns OSAL_ERROR.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 Inc2Soc_s32CreateResources( tEnInc2SocDev enDeviceType )
{
   tS32 s32RetVal = OSAL_ERROR;

   if( OSAL_ERROR != Inc2Soc_s32InitDevHandle( enDeviceType ))
   {
      /* Critical section is being accessed without using semaphore which is OKAY
         *because the third party thread is not active yet.*/

      // Initializing write pointers of buffer 
      rInc2SocDevHandle[ enDeviceType ].u32WriteIndex = 0;

      rInc2SocDevHandle[ enDeviceType ].hSemaphore = OSAL_C_INVALID_HANDLE;
      rInc2SocDevHandle[ enDeviceType ].hEvent = OSAL_C_INVALID_HANDLE;

      /* This is the only part where the status of INC forwarder thread is being 
               *updated by the SocketRead thread.*/
      rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_CLIENT_DISCONNECTED;

      /* Creating a semaphore and an event to handle buffer sharing between 
            *SocketRead thread and SocketWrite thread */
      if( OSAL_ERROR == Inc2Soc_s32SemInit( enDeviceType ) )
      {
         rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_THREAD_SHUTDOWN;
         SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                           "INC2SOC: Semaphore creation failed for device: %d."
                           " Not spawning the thread", enDeviceType);
      }
      else if( OSAL_ERROR == Inc2Soc_s32EventInit( enDeviceType ) )
      {
         rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_THREAD_SHUTDOWN;
         SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                          "INC2SOC: Event creation failed for the device: %d."
                          " Not spawning the thread.", enDeviceType);
         Inc2Soc_vReleaseResources( enDeviceType, INC2SOC_RES_SEM );
      }
      else if ( OSAL_ERROR == Inc2Soc_s32SockInit( enDeviceType ) )
      {
         rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_THREAD_SHUTDOWN;
         SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                          "INC2SOC: Socket creation failed for the device: %d."
                          " Not spawning the thread.", enDeviceType);
         Inc2Soc_vReleaseResources( enDeviceType, INC2SOC_RES_SEM );
         Inc2Soc_vReleaseResources( enDeviceType, INC2SOC_RES_EVENT );

      }
      // This thread will create all OS resources and wait for INC data 
      else if( OSAL_ERROR == OSAL_ThreadSpawn(&(rInc2SocDevHandle[ enDeviceType ].rAuxInfo.rIncFwThreadAttr)) )
      {
         rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_THREAD_SHUTDOWN;
         SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                            "INC2SOC: Thread spawn failed for device: %d", 
                            enDeviceType);
         Inc2Soc_vReleaseResources( enDeviceType, INC2SOC_RES_ALL );
      }
      else
      {
         SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                           "INC2SOC: Init success for device: %d", 
                           enDeviceType );
         s32RetVal = OSAL_OK;
      }
   }
   else
   {
      rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_THREAD_SHUTDOWN;
      SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                         "INC2SOC: Init failed for device: %d.", 
                         enDeviceType);
   }
   return s32RetVal;
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vReleaseResources
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*                       tEnInc2SocResType enResType - Type of resource
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function is used to de-initialize resources used by INC forwarder.
*                       It frees enResType for the device enDeviceType.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_vReleaseResources( tEnInc2SocDev enDeviceType, tEnInc2SocResType enResType )
{
   switch( enResType )
   {
      case INC2SOC_RES_SEM:
            {
               SenDisp_vTraceOut( TR_LEVEL_USER_4, "INC2SOC: De initializing semaphore for device: %d", enDeviceType );
               Inc2Soc_vSemDeInit( enDeviceType );
            }
            break;

      case INC2SOC_RES_EVENT:
            {
               SenDisp_vTraceOut( TR_LEVEL_USER_4, "INC2SOC: De initializing socket for device: %d", enDeviceType );
               Inc2Soc_vEventDeInit( enDeviceType );
            }
            break;

      case INC2SOC_RES_SOCK:
            {
               SenDisp_vTraceOut( TR_LEVEL_USER_4, "INC2SOC: De initializing socket for device: %d", enDeviceType );
               Inc2Soc_vSockDeInit( enDeviceType );
            }
            break;

      case INC2SOC_RES_MEM:
            {
               SenDisp_vTraceOut( TR_LEVEL_USER_4, "INC2SOC: Freeing memory used by device: %d", enDeviceType );
               OSAL_vMemoryFree( rInc2SocDevHandle[ enDeviceType ].pu8SendBuffer );
            }
            break;

      case INC2SOC_RES_ALL:
            {
               SenDisp_vTraceOut( TR_LEVEL_USER_4, "INC2SOC: Freeing all resources used by device: %d", enDeviceType );
               Inc2Soc_vSemDeInit( enDeviceType );
               Inc2Soc_vEventDeInit( enDeviceType );
               Inc2Soc_vSockDeInit( enDeviceType );
               OSAL_vMemoryFree( rInc2SocDevHandle[ enDeviceType ].pu8SendBuffer );
            }
            break;

      default:
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                                 "INC2SOC: Invalid Resource type release called for device: %d", enDeviceType );
            }
            break;
   }
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_s32SemInit
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*
* RETURNVALUE : OSAL_OK - Success
*                        OSAL_ERROR - Failure
*
* DESCRIPTION : This function is used to create semaphore for the device enDeviceType. 
*                       In case of an error, it prints the OSAL errorcode which caused the failure.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 Inc2Soc_s32SemInit( tEnInc2SocDev enDeviceType )
{
   tS32 s32RetVal = OSAL_OK;
   if( OSAL_OK != OSAL_s32SemaphoreCreate( rInc2SocDevHandle[ enDeviceType ].rAuxInfo.csSemName,
                                          &rInc2SocDevHandle[ enDeviceType ].hSemaphore,
                                          1) )
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                        "INC2SOC: Sem create failed for device: %d, with error: %lu", 
                        enDeviceType, OSAL_u32ErrorCode() );
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}


/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vSemDeInit
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function is used to de-initialize semaphore for the device enDeviceType.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_vSemDeInit( tEnInc2SocDev enDeviceType )
{
   if( rInc2SocDevHandle[ enDeviceType ].hSemaphore != OSAL_C_INVALID_HANDLE )
   {
      if( OSAL_OK != OSAL_s32SemaphoreClose( rInc2SocDevHandle[ enDeviceType ].hSemaphore ) )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                           "INC2SOC: SEM Close failed for device:%d, err %lu",
                            enDeviceType, OSAL_u32ErrorCode() );
      }
      if ( OSAL_OK != OSAL_s32SemaphoreDelete( rInc2SocDevHandle[ enDeviceType ].rAuxInfo.csSemName ) )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                            "INC2SOC: SEM Delete failed for device:%d, err %lu",
                            enDeviceType, OSAL_u32ErrorCode() );
      }
      rInc2SocDevHandle[ enDeviceType ].hSemaphore = OSAL_C_INVALID_HANDLE;
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                         "INC2SOC: Invalid SEM handle for device: %d", enDeviceType );
   }
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_s32EventInit
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*
* RETURNVALUE : OSAL_OK - Success
*                        OSAL_ERROR - Failure
*
* DESCRIPTION : This function is used to create the INC forwarder event for the device enDeviceType. 
*                       In case of an error, it prints the OSAL errorcode which caused the failure.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 Inc2Soc_s32EventInit( tEnInc2SocDev enDeviceType )
{
   tS32  s32RetVal = OSAL_OK;
   if( OSAL_ERROR == OSAL_s32EventCreate( rInc2SocDevHandle[ enDeviceType ].rAuxInfo.csEventName,
                                          &rInc2SocDevHandle[ enDeviceType ].hEvent))
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                        "INC2SOC: Event Create failed for device: %d, with error: %lu", 
                        enDeviceType, OSAL_u32ErrorCode() );
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vEventDeInit
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function is used to de-initialize the INC forwarder event for the device enDeviceType. 
*                       In case of an error, it prints the OSAL errorcode which caused the failure.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_vEventDeInit( tEnInc2SocDev enDeviceType )
{
   if( rInc2SocDevHandle[ enDeviceType ].hEvent != OSAL_C_INVALID_HANDLE )
   {
      if( OSAL_OK != OSAL_s32EventClose( rInc2SocDevHandle[ enDeviceType ].hEvent ) )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                           "INC2SOC: Event Close failed for device: %d, with error: %lu", 
                           enDeviceType, OSAL_u32ErrorCode() );

      }
      if( OSAL_OK != OSAL_s32EventDelete( rInc2SocDevHandle[ enDeviceType ].rAuxInfo.csEventName ))
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                           "INC2SOC: Event Delete failed for device: %d, with error: %lu", 
                           enDeviceType, OSAL_u32ErrorCode() );
      }
      rInc2SocDevHandle[ enDeviceType ].hEvent = OSAL_C_INVALID_HANDLE;;
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                        "INC2SOC: Invalid Event handle for device: %d", enDeviceType );
   }
}


/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_s32SockInit
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*
* RETURNVALUE : OSAL_OK - Success
*                       OSAL_ERROR - Failure
*
* DESCRIPTION : This function initializes socket used in INC forwarder for the device enDeviceType.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 Inc2Soc_s32SockInit( tEnInc2SocDev enDeviceType )
{
   tS32 s32RetVal = OSAL_OK;
   tS32 s32ErrChk;

   rInc2SocDevHandle[ enDeviceType ].rSockInfo.u32SocAddrLength = (socklen_t)sizeof( struct sockaddr_un );

   // Socket Type
   rInc2SocDevHandle[ enDeviceType ].rSockInfo.rSerSocAdd.sun_family = INC2SOC_PF_SOCK_TYPE;

   rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD = socket( INC2SOC_PF_SOCK_TYPE, (tS32)SOCK_SEQPACKET, 0 );

   //Socket call returns FD on success and -1 on failure
   if( rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD != -1 )
   {
      // Unlink any existing connections to the socket. Otherwise, socket bind will fail 
      unlink( rInc2SocDevHandle[ enDeviceType ].rSockInfo.rSerSocAdd.sun_path );

      //lint -e64, PQM_authorized_multi_579
      s32ErrChk = bind( rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD, 
                       (const struct sockaddr *)(tPVoid)&(rInc2SocDevHandle[ enDeviceType ].rSockInfo.rSerSocAdd), 
                       rInc2SocDevHandle[ enDeviceType ].rSockInfo.u32SocAddrLength );
      //Bind returns 0 on success and -1 on failure
      if( s32ErrChk == 0 )
      {
         // Listen for MAX_GNSS_TDPY_CLIENTS number of connections on the socket 
         s32ErrChk = listen( rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD, INC2SOC_MAX_NUM_OF_CLIENTS );

         // Listen returns 0 on success and -1 on failure
         if( s32ErrChk == -1 )
         {
            SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                               "INC2SOC: Sock Listen for device:%d FAILED ", enDeviceType );
            s32RetVal = OSAL_ERROR;
         }
       }
       else
       {
          SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                             "INC2SOC: Sock Bind for device:%d FAILED ", enDeviceType );
          s32RetVal = OSAL_ERROR;
       }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                         "INC2SOC: Invalid Sock handle for device:%d ", enDeviceType );
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vSockDeInit
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function is used to de-initialize socket used in INC forwarder for the device enDeviceType.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_vSockDeInit( tEnInc2SocDev enDeviceType )
{
   if( OSAL_NULL != rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD )
   {
      if ( 0 != close( rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD ) )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                            "INC2SOC: Server Close failed for device: %d", enDeviceType );
      }
   }
}


/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vPostDrivShutdown
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle 
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function is used to post Driver_Shutdown event to respective device handle.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid Inc2Soc_vPostDrivShutdown( tEnInc2SocDev enDeviceType )
{
   switch ( enDeviceType )
   {
      case INC2SOC_DEV_TYPE_GNSS :
      {
         //Post shutdown event if INC forwarder is active
         if( INC2SOC_THREAD_SHUTDOWN != rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].enThrStatus )
         {
            Inc2Soc_vPostEvent(INC2SOC_DEV_TYPE_GNSS, GNSS_EVENT_DRIV_SHUTDOWN, OSAL_EN_EVENTMASK_OR, __LINE__);
         }
      }
      break;

      case INC2SOC_DEV_TYPE_POS :
      {
         //Post shutdown event if INC forwarder is active
         if( INC2SOC_THREAD_SHUTDOWN != rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].enThrStatus )
         {
            Inc2Soc_vPostEvent(INC2SOC_DEV_TYPE_POS, SEN_EVENT_DRIV_SHUTDOWN, OSAL_EN_EVENTMASK_OR, __LINE__);
         }
      }
      break;

      default :
      {
            SenDisp_vTraceOut( TR_LEVEL_USER_4, "Invalid Device type called Inc2Soc_vPostDrivShutdown");
      }
      break;
   }
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vEnterCriticalSection
*
* PARAMETER   :  tEnInc2SocDev enDeviceType - Device handle
*                       tU32 u32Timeout - Worst case timeout after which the call is unblocked
*                       tU32 u32LineNum - Line number where the call was made
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function waits for the Semaphore to be released for u32Timeout milliseconds. 
*                       Worst case wait on the semaphore is received as a parameter. In case of timeout,
*                       the call is unblocked.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_vEnterCriticalSection( tEnInc2SocDev enDeviceType, tU32 u32Timeout, tU32 u32LineNum )
{
   if( rInc2SocDevHandle[ enDeviceType ].hSemaphore != OSAL_C_INVALID_HANDLE )
   {
      if ( OSAL_OK != OSAL_s32SemaphoreWait( rInc2SocDevHandle[ enDeviceType ].hSemaphore, 
                                             (OSAL_tMSecond)u32Timeout ) )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                           "INC2SOC: SEM wait FAILED for device: %d, line: %lu, error: %d",
                           enDeviceType, u32LineNum, OSAL_u32ErrorCode());
      }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                        "INC2SOC: Invalid SEM handle for device: %d",
                        enDeviceType );
   }
}


/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vLeaveCriticalSection
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle
*                       tU32 u32LineNum - Line number where the call was made
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function releases the semaphore for the device enDeviceType.
*                       In case of failure, it prints the OSAL errorcode.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_vLeaveCriticalSection( tEnInc2SocDev enDeviceType, tU32 u32LineNum )
{
   if( rInc2SocDevHandle[ enDeviceType ].hSemaphore != OSAL_C_INVALID_HANDLE )
   {
      if ( OSAL_OK != OSAL_s32SemaphorePost(rInc2SocDevHandle[ enDeviceType ].hSemaphore ) )
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                           "INC2SOC: SEM post FAILED for device: %d, line: %lu, error: %d",
                           enDeviceType, u32LineNum, OSAL_u32ErrorCode());
      }
   }
   else
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                        "INC2SOC: Invalid SEM handle for device: %d",
                        enDeviceType );
   }
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vWaitOnEvent
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle
*                       tU32 u32Mask - Events to wait on
*                       tU32 u32Timeout - Worst case timeout after which the call is unblocked
*                       tU32 u32LineNum - Line number where the call was made
*
* RETURNVALUE : u32ResultMask - Event occured.
*
* DESCRIPTION : This function waits for the events mentioned in u32Mask and returns corresponding result masks.
*                       Worst case wait on the event is received as a parameter.In case of timeout,
*                       the call is unblocked. In case of an error or timeout, 0 is returned as u32ResultMask.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 Inc2Soc_vWaitOnEvent( tEnInc2SocDev enDeviceType, tU32 u32Mask, tU32 u32Timeout, tU32 u32LineNum )
{
   tU32 u32ResultMask = 0;
   if( OSAL_ERROR == OSAL_s32EventWait( rInc2SocDevHandle[ enDeviceType ].hEvent,
                                       (OSAL_tEventMask)u32Mask,
                                        OSAL_EN_EVENTMASK_OR,
                                       (OSAL_tMSecond)u32Timeout,
                                        &u32ResultMask ) )
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                        "INC2SOC: Event wait FAILED for device: %d, line: %lu, error: %lu", 
                        enDeviceType, u32LineNum, OSAL_u32ErrorCode()  );
   }
   return u32ResultMask;
}


/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vPostEvent
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle
*                       tU32 u32Timeout - Worst case timeout after which the call is unblocked
*                       tU32 enMaskFlag - Events to be posted
*                       tU32 u32LineNum - Line number where the call was made
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function posts the events sent in the enMaskFlag. It prints 
*                       OSAL errorcode on failure.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_vPostEvent( tEnInc2SocDev enDeviceType, tU32 u32Mask, OSAL_tenEventMaskFlag enMaskFlag, tU32 u32LineNum )
{
   if(OSAL_ERROR == OSAL_s32EventPost( rInc2SocDevHandle[ enDeviceType ].hEvent,
                                      (OSAL_tEventMask)u32Mask,
                                       enMaskFlag ))
    {
        SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                          "INC2SOC: Event Post failed for device: %d, error: %lu, line: %u",
                          enDeviceType, OSAL_u32ErrorCode(), u32LineNum );
    }
}


/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_pvGnssThread
*
* PARAMETER   : tPVoid pvArg
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This thread waits for the Gnss SocketRead thread to write data into the common buffer.
*                       Then it forwards the buffer to third party navigation application via socket
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_pvGnssThread( tPVoid pvArg)
{
   tS32 s32BytesWritten = 0;
   struct pollfd fds_sock;
   nfds_t nfds_sock = INC2SOC_NUM_OF_POLL_FDS;
   tU32 u32ResultMask;
   tEnInc2SocDev enDeviceType = INC2SOC_DEV_TYPE_GNSS;
   tS32 s32ErrChk;

   (tVoid)pvArg; //To satisfy lint
   fds_sock.fd = rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD;
   fds_sock.events = POLLIN;
   fds_sock.revents = 0;


      while( (TRUE != rGnssProxyInfo.bShutdownFlag) && (INC2SOC_THREAD_SHUTDOWN != rInc2SocDevHandle[ enDeviceType ].enThrStatus) )
      {
         SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                           "INC2SOC: Waiting for GNSS Client to connect.." );
         // Gnss driver may shutdown anytime. The while loop keeps track of the shutdown flag before client is connected
         while((TRUE != rGnssProxyInfo.bShutdownFlag)&&(!(fds_sock.revents & POLLIN)))
         {
            s32ErrChk = poll(&fds_sock, nfds_sock, INC2SOC_ACCEPT_POLL_TIMEOUT);

            if( s32ErrChk == -1 )
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                                 "INC2SOC: poll() error on GNSS socket, errno: %d."
                                 " Retrying..", errno);
            }
         }
         if( fds_sock.revents & POLLIN )
         {
            //Clear the event
            fds_sock.revents &= ~POLLIN;
            SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                              "INC2SOC: accept() GNSS client connection" );

            //lint -e64, PQM_authorized_multi_579
            rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD = accept( rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD, 
                                             (struct sockaddr *)(tPVoid)&(rInc2SocDevHandle[ enDeviceType ].rSockInfo.rCliSocAdd), 
                                              &(rInc2SocDevHandle[ enDeviceType ].rSockInfo.u32SocAddrLength) );

            // Accept returns the client socket FD on success and -1 on failure
            if( rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD != -1 )
            {
               rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_CLIENT_CONNECTED;
               SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                                 "INC2SOC: New GNSS client connected: %d", 
                                 rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD );
               do
               {
                  u32ResultMask = Inc2Soc_vWaitOnEvent(enDeviceType, 
                                                      (GNSS_EVENT_DRIV_WRITE_COMPLETE|GNSS_EVENT_DRIV_SHUTDOWN), 
                                                       OSAL_C_TIMEOUT_FOREVER,
                                                       __LINE__);

                  // This event is triggered when Gnss driver shuts down, 
                  //but SocketWrite thread is waiting on GNSS_EVENT_DISP_WRITE_COMPLETE event
                  // rGnssProxyInfo.bShutdownFlag will be set to TRUE
                  if( u32ResultMask & GNSS_EVENT_DRIV_SHUTDOWN )
                  {
                     SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                                        "INC2SOC: Waiting for data but GNSS driver Shutdown occured" );
                     rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_THREAD_SHUTDOWN;
                     Inc2Soc_vPostEvent(enDeviceType, ~u32ResultMask, OSAL_EN_EVENTMASK_AND, __LINE__);
                  }
                  else if ( u32ResultMask & GNSS_EVENT_DRIV_WRITE_COMPLETE )
                  {
                     Inc2Soc_vEnterCriticalSection(enDeviceType, OSAL_C_TIMEOUT_FOREVER, __LINE__);
                     Inc2Soc_vPostEvent(enDeviceType, ~u32ResultMask, OSAL_EN_EVENTMASK_AND, __LINE__);
                     if(rInc2SocDevHandle[ enDeviceType ].u32WriteIndex > 0)
                     {
                        s32BytesWritten = send(rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD, 
                                               rInc2SocDevHandle[ enDeviceType ].pu8SendBuffer, 
                                               rInc2SocDevHandle[ enDeviceType ].u32WriteIndex, 
                                               0);
                        SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                                          "INC2SOC: Total size of data written to GNSS socket: %d", 
                                          s32BytesWritten );
                        rInc2SocDevHandle[ enDeviceType ].u32WriteIndex = 0;
                     }
                     Inc2Soc_vLeaveCriticalSection(enDeviceType, __LINE__);
                  }
               }while( (s32BytesWritten != -1) && (TRUE != rGnssProxyInfo.bShutdownFlag) );

               if( s32BytesWritten == -1 )
               {
                  rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_CLIENT_DISCONNECTED;
                  SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                                     "INC2SOC: Connection lost with GNSS client, errno: %d", errno );
               }
               close(rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD);
            }
            else
            {
               SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                                  "INC2SOC: Could not accept GNSS client. Retrying.." );
            }
         }
      }

      if(TRUE == rGnssProxyInfo.bShutdownFlag)
      {
         SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                           "INC2SOC: GNSS driver shutdown initiated." 
                           " Killing the GNSS SocketWrite thread" );
      }

   rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_THREAD_SHUTDOWN;

   //Release all resources
   Inc2Soc_vReleaseResources( enDeviceType, INC2SOC_RES_ALL );
   OSAL_vThreadExit();
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_pvPoSThread
*
* PARAMETER   : tPVoid pvArg
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This thread waits for the PoS SocketRead thread to write data into the common buffer.
*                       Then it forwards the buffer to third party navigation application via socket
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_pvPoSThread( tPVoid pvArg)
{
   tS32 s32BytesWritten = 0;
   struct pollfd fds_sock;
   nfds_t nfds_sock = INC2SOC_NUM_OF_POLL_FDS;
   tU32 u32ResultMask;
   tEnInc2SocDev enDeviceType = INC2SOC_DEV_TYPE_POS;
   tS32 s32ErrChk;

  (tVoid)pvArg; //To satisfy lint
  fds_sock.fd = rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD;
  fds_sock.events = POLLIN;
  fds_sock.revents = 0;

   while( (TRUE != rSensorDispInfo.bShutdownFlag) && (INC2SOC_THREAD_SHUTDOWN != rInc2SocDevHandle[ enDeviceType ].enThrStatus) )
   {
      SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                        "INC2SOC: Waiting for PoS Client to connect..");
      // SensorDispatcher may shutdown anytime. The while loop keeps track of the shutdown flag before client is connected
      while((TRUE != rSensorDispInfo.bShutdownFlag)&&(!(fds_sock.revents & POLLIN)))
      {
         s32ErrChk = poll(&fds_sock, nfds_sock, INC2SOC_ACCEPT_POLL_TIMEOUT);

         if( s32ErrChk == -1 )
         {
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                              "INC2SOC: poll() error on PoS socket, errno: %d."
                              " Retrying..", errno);
         }
      }
      if( fds_sock.revents & POLLIN )
      {
         fds_sock.revents &= ~POLLIN;
         SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                           "INC2SOC: accept() PoS client connection" );

         //lint -e64, PQM_authorized_multi_579
         rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD = accept( rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ServerFD, 
                                          (struct sockaddr *)(tPVoid)&(rInc2SocDevHandle[ enDeviceType ].rSockInfo.rCliSocAdd), 
                                           &(rInc2SocDevHandle[ enDeviceType ].rSockInfo.u32SocAddrLength) );

         // Accept returns the client socket FD on success and -1 on failure
         if( rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD != -1 )
         {
            rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_CLIENT_CONNECTED;
            SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                              "INC2SOC: New PoS client connected: %d", 
                              rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD );
            do
            {
               u32ResultMask = Inc2Soc_vWaitOnEvent(enDeviceType, 
                                                   (SEN_EVENT_DRIV_WRITE_COMPLETE|SEN_EVENT_DRIV_SHUTDOWN), 
                                                    OSAL_C_TIMEOUT_FOREVER, 
                                                    __LINE__);

               // This event is triggered when SensorDispatcher shuts down, 
               //but SocketWrite thread is waiting on SEN_EVENT_DRIV_WRITE_COMPLETE event
               // rSensorDispInfo.bShutdown flag will be set to TRUE
               if( u32ResultMask & SEN_EVENT_DRIV_SHUTDOWN )
               {
                  SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                                     "INC2SOC: Waiting for data but SenDisp Shutdown occured" );
                  rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_THREAD_SHUTDOWN;
                  Inc2Soc_vPostEvent(enDeviceType, ~u32ResultMask, OSAL_EN_EVENTMASK_AND, __LINE__);
               }
               else if ( u32ResultMask & SEN_EVENT_DRIV_WRITE_COMPLETE )
               {
                  Inc2Soc_vEnterCriticalSection(enDeviceType, OSAL_C_TIMEOUT_FOREVER, __LINE__);
                  Inc2Soc_vPostEvent(enDeviceType, ~u32ResultMask,  OSAL_EN_EVENTMASK_AND, __LINE__);
                  if(rInc2SocDevHandle[ enDeviceType ].u32WriteIndex > 0)
                  {
                     s32BytesWritten = send(rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD, 
                                            rInc2SocDevHandle[ enDeviceType ].pu8SendBuffer, 
                                            rInc2SocDevHandle[ enDeviceType ].u32WriteIndex, 
                                            0);
                     SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                                       "INC2SOC: Total size of data written to PoS socket: %d", 
                                       s32BytesWritten );
                     rInc2SocDevHandle[ enDeviceType ].u32WriteIndex = 0;
                  }
                  Inc2Soc_vLeaveCriticalSection(enDeviceType, __LINE__);
               }
            }while( (s32BytesWritten != -1) && (TRUE != rSensorDispInfo.bShutdownFlag) );

            if( s32BytesWritten == -1 )
            {
               rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_CLIENT_DISCONNECTED;
               SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                                  "INC2SOC: Connection lost with PoS client, errno: %d", errno );
            }
            close(rInc2SocDevHandle[ enDeviceType ].rSockInfo.s32ClientFD);

         }
         else
         {
            SenDisp_vTraceOut( TR_LEVEL_ERRORS, 
                              "INC2SOC: Accept call failed for PoS client. Retrying.." );
         }
      }
   }

   if(TRUE == rSensorDispInfo.bShutdownFlag)
   {
      SenDisp_vTraceOut( TR_LEVEL_ERRORS , 
                        "INC2SOC: SenDisp shutdown initiated. Killing PoS SocketWrite thread" );
   }

   rInc2SocDevHandle[ enDeviceType ].enThrStatus = INC2SOC_THREAD_SHUTDOWN;

   //Release all resources
   Inc2Soc_vReleaseResources( enDeviceType, INC2SOC_RES_ALL );
   OSAL_vThreadExit();
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vWriteDataToBuffer
*
* PARAMETER   : tEnInc2SocDev enDeviceType - Device handle
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function takes semaphore for device - enDeviceType,
*                       calls respective BufferWrite function and posts DrivWriteComplete event for it.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
tVoid Inc2Soc_vWriteDataToBuffer( tEnInc2SocDev enDeviceType )
{
   switch ( enDeviceType )
   {
      case INC2SOC_DEV_TYPE_GNSS:
      {
         // GNSS data is written to the buffer only when third party client is connected
         if( INC2SOC_CLIENT_CONNECTED == rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].enThrStatus )
         {
            Inc2Soc_vEnterCriticalSection(INC2SOC_DEV_TYPE_GNSS, GNSS_TDPY_SEM_WAIT_DRIV_DLY, __LINE__);
            Inc2Soc_vWriteGnssDataToBfr();
            Inc2Soc_vLeaveCriticalSection(INC2SOC_DEV_TYPE_GNSS, __LINE__);
            Inc2Soc_vPostEvent(INC2SOC_DEV_TYPE_GNSS, GNSS_EVENT_DRIV_WRITE_COMPLETE, OSAL_EN_EVENTMASK_OR, __LINE__);
         }
      }
      break;

      case INC2SOC_DEV_TYPE_POS:
      {
         // PoS data is buffered when the third party client is not connected
         if( INC2SOC_THREAD_SHUTDOWN != rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].enThrStatus )
         {
            Inc2Soc_vEnterCriticalSection(INC2SOC_DEV_TYPE_POS, SEN_DISP_TDPY_SEM_WAIT_DRIV_DLY, __LINE__);
            Inc2Soc_vWritePoSDataToBfr();
            Inc2Soc_vLeaveCriticalSection(INC2SOC_DEV_TYPE_POS, __LINE__);
            Inc2Soc_vPostEvent(INC2SOC_DEV_TYPE_POS, SEN_EVENT_DRIV_WRITE_COMPLETE, OSAL_EN_EVENTMASK_OR, __LINE__);
         }
      }
      break;

      default:
      {
         SenDisp_vTraceOut(TR_LEVEL_ERRORS,
                           "Invalid device type sent to WriteDataToBfr func" );
      }
      break;
   }
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vWriteGnssDataToBfr
*
* PARAMETER   : NONE
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function writes Gnss data from INC into the shared buffer. 
*                       Access to buffer should be synchronized using semaphore.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_vWriteGnssDataToBfr( tVoid )
{
      OSAL_pvMemoryCopy( rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].pu8SendBuffer,
                         &rGnssProxyInfo.u8RecvBuffer[ 0 ], 
                         rGnssProxyInfo.u32RecvdMsgSize );
      rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].u32WriteIndex = rGnssProxyInfo.u32RecvdMsgSize;
      SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                        "INC2SOC: Total size of GNSS buffer: %lu",
                        rInc2SocDevHandle[ INC2SOC_DEV_TYPE_GNSS ].u32WriteIndex, INC2SOC_DEV_TYPE_GNSS );
}



/*********************************************************************************************************************
* FUNCTION    : Inc2Soc_vWritePoSDataToBfr
*
* PARAMETER   : NONE
*
* RETURNVALUE : NONE
*
* DESCRIPTION : This function writes PoS data from INC into the shared buffer. 
*                       Access to buffer should be synchronized using semaphore.
*                       The MSG_ID field of the buffer is copied once during the first message.
*                       For further messages, the number of messages field is updated and
*                       the remaining part of the message is appended to the buffer.
*
* HISTORY     :
*
*--------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------------|----------------------|-------------------------------------
* 19.FEB.2016 | Initial version: 1.0 | Kulkarni Ramchandra (RBEI/ECF5)
* -------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid Inc2Soc_vWritePoSDataToBfr( tVoid )
{
   tU32 u32PrevWriteIndex;
   tU32 u32BytesToWrite;
   tU16  u16TotalNumEnt = 0;

   /* Num of entries in the INC message and Third party buffer are accessed using pointers. The pointers are initialized 
          here and accessed during the updation (in the else section) */
   tPU16 const pu16NumEntSD     = (tPU16)(tPVoid)&(rSensorDispInfo.u8RecvBuffer[ MSGID_SIZE ]);
   tPU16 const pu16NumEntTdPySD = (tPU16)(tPVoid)&(rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].pu8SendBuffer[ MSGID_SIZE ]);

   /* 
         if the buffer is being written for the first time, the header and data should be copied from the INC message. 
         For further messages, the data is appended to the buffer and header data is changed according to the incoming message .
     */
   if( rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].u32WriteIndex == 0 )
   {
      OSAL_pvMemoryCopy( &rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].pu8SendBuffer[SEN_DISP_OFFSET_MSG_ID], 
                         &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_MSG_ID], 
                         MSGID_SIZE );
      OSAL_pvMemoryCopy( &rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].pu8SendBuffer[SEN_DISP_OFFSET_NUM_OF_ENTRIES_IN_DATA_MSG], 
                         &rSensorDispInfo.u8RecvBuffer[SEN_DISP_OFFSET_NUM_OF_ENTRIES_IN_DATA_MSG], 
                         SEN_DISP_FIELD_SIZE_NUM_ENTRTIES );

      // the remaining part of the message is copied outside the if-else check 
      rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].u32WriteIndex = rSensorDispInfo.u32RecvdMsgSize;
      /* u32PrevWriteIndex will hold the value of index where write begins for the current copy operation*/
      u32PrevWriteIndex = SEN_DISP_TDPY_MSG_HEADER_SIZE;
   }
   else
   {
      u32PrevWriteIndex = rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].u32WriteIndex;
      // if the incoming messages do not fit on the buffer, discard messages until the current data is written to socket 
      if( (u32PrevWriteIndex + rSensorDispInfo.u32RecvdMsgSize - SEN_DISP_TDPY_MSG_HEADER_SIZE) >= MAX_SENSOR_DATA_SIZE_TDPY )
      {
         SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                           "INC2SOC: Third party PoS buffer overflow. Discarding the message");
      }
      else
      {
         // Updating the number of entries by adding the existing count on buffer to the new messages received
         u16TotalNumEnt = (*pu16NumEntSD) + (*pu16NumEntTdPySD);

         SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                            "INC2SOC: Msgs in Sen_disp: %d, previous TP_buffer: %d,"
                            " updated TP_buffer: %d", 
                            *pu16NumEntSD, 
                            *pu16NumEntTdPySD, 
                            u16TotalNumEnt);

         OSAL_pvMemoryCopy( &rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].pu8SendBuffer[ MSGID_SIZE ], 
                            &u16TotalNumEnt,
                            SEN_DISP_FIELD_SIZE_NUM_ENTRTIES );
         rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].u32WriteIndex += rSensorDispInfo.u32RecvdMsgSize - SEN_DISP_TDPY_MSG_HEADER_SIZE;
      }
   }

   u32BytesToWrite = rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].u32WriteIndex - u32PrevWriteIndex;
   // In case of buffer overflow, u32BytesToWrite will be 0
   if( u32BytesToWrite > 0)
   {
      OSAL_pvMemoryCopy( &rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].pu8SendBuffer[ u32PrevWriteIndex ], 
                         &rSensorDispInfo.u8RecvBuffer[ SEN_DISP_TDPY_MSG_HEADER_SIZE ],
                         u32BytesToWrite );
      SenDisp_vTraceOut( TR_LEVEL_USER_4, 
                         "INC2SOC: Bytes written to PoS third party buffer: %d."
                         " Current size of buffer: %lu", 
                         u32BytesToWrite, rInc2SocDevHandle[ INC2SOC_DEV_TYPE_POS ].u32WriteIndex );
   }
}


/* End of file */
