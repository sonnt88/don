#include "Sensor_InternalMock.h"

tS32 mocked_s32SensorDiapatcherInit(tEnSenDrvID enSenID)
{
   return SensorMock::GetDelegatee().s32SenDispInit(enSenID);
}

tS32 mocked_s32SensorDiapatcherDeInit(tEnSenDrvID enSenID)
{
   return SensorMock::GetDelegatee().s32SenDispDeInit(enSenID);
}

tS32 mocked_SenDisp_s32TriggerGyroSelfTest(tVoid)
{
   return SensorMock::GetDelegatee().s32SensDispTrigGyroSelfTest();
}

tS32 mocked_SenDisp_s32TriggerAccSelfTest(tVoid)
{
   return SensorMock::GetDelegatee().s32SensDispTrigAccSelfTest();
}

tS32 mocked_SenRingBuff_s32Open(tEnSenDrvID enDeviceID)
{
   return SensorMock::GetDelegatee().s32SensRngBuffOpen(enDeviceID);
}

tS32 mocked_SenRingBuff_s32Close(tEnSenDrvID enDeviceID)
{
   return SensorMock::GetDelegatee().s32SensRngBuffCls(enDeviceID);
}

tS32 mocked_SenRingBuff_s32GetAvailRecords(tEnSenDrvID enDeviceID)
{
   return SensorMock::GetDelegatee().s32SensRngBuffGetAvailRecs(enDeviceID);
}

tBool mocked_SenHwInfo_bGetHwinfo ( tEnSensor enType , OSAL_trIOCtrlHwInfo *prIoCtrlHwInfo)
{
   return SensorMock::GetDelegatee().s32SensHwInfobGetHwinfo(enType, prIoCtrlHwInfo);
}

tS32 mocked_SenRingBuff_s32Read( tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32EntRequested )
{
   return SensorMock::GetDelegatee().s32SensRingBuffs32Read(enDeviceID, pvData, u32EntRequested);
}

tS32 mocked_SenRingBuff_s32Write( tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32EntRequested )

{
   return SensorMock::GetDelegatee().s32SenRingBuffWrite(enDeviceID, pvData, u32EntRequested);
}

tS32 mocked_SenRingBuff_s32Init( tEnSenDrvID enDeviceID, const trSenRingBuffConfig *prSenRingBuffConfig )
{
   return SensorMock::GetDelegatee().s32SenRingBuffInit( enDeviceID, prSenRingBuffConfig );
}

tS32 mocked_SenRingBuff_s32DeInit( tEnSenDrvID enDeviceID )
{
   return SensorMock::GetDelegatee().s32SensRingBuffDeinit(enDeviceID);
}

tS32 mocked_Inc2Soc_s32CreateResources( tEnInc2SocDev enDeviceType )
{
   return SensorMock::GetDelegatee().s32Inc2SocCreateResources(enDeviceType);
}

tVoid mocked_Inc2Soc_vPostDrivShutdown( tEnInc2SocDev enDeviceType )
{
   SensorMock::GetDelegatee().vInc2SocPostDrivShutdown(enDeviceType);
}

tVoid mocked_Inc2Soc_vWriteDataToBuffer( tEnInc2SocDev enDeviceType )
{
   SensorMock::GetDelegatee().vInc2SocWriteDataToBuffer(enDeviceType);
}


#ifdef SENSPXYMOCKSTOBEREMOVED
// The Following mocks have to be remove once the mocks are 
// in place from INC/Communication Team 
int mocked_close(int fd)
{
   return SensorMock::GetDelegatee().internalmock_close(fd);
}

int mocked_open(const char *pathname, int flags)
{
   return SensorMock::GetDelegatee().internalmock_open(pathname, flags);
}

ssize_t mocked_write(int fd, const void *buf, size_t count)
{
   return SensorMock::GetDelegatee().internalmock_write(fd, buf, count);
}

int mocked_socket(int domain, int type, int protocol)
{
   return SensorMock::GetDelegatee().internalmock_socket(domain, type, protocol);
}

int mocked_bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
   return SensorMock::GetDelegatee().internalmock_bind(sockfd, addr, addrlen);
}

int mocked_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
   return SensorMock::GetDelegatee().internalmock_connect(sockfd, addr, addrlen);
}

int  poll(struct pollfd *fds, nfds_t nfds, int timeout)
{
  timeout=1;
  fds[0].revents = POLLIN;
  return SensorMock::GetDelegatee().poll(fds,nfds,timeout);
}

#ifdef __cplusplus
extern "C"
{

// Will be removed once OSAL team creates the following mocks.
tS32 OSAL_s32ThreadJoin(OSAL_tThreadID tid, OSAL_tMSecond msec)
{
   return SensorMock::GetDelegatee().OSAL_s32ThreadJoin(tid, msec);
}

tBool LLD_bIsTraceActive(tU32 u32Class, tU32 u32Level)
{
   return TRUE;
}

tVoid LLD_vTrace(tU32 u32Class, tU32 u32Level, const void* pvData, tU32 u32Length)
{
   //do nothing
}

// The Following mocks have to be remove once the mocks are 
// in place from INC/Communication Team 

int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen)
{
   return SensorMock::GetDelegatee().dgram_send(skd, ubuf, ulen);
}

int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen)
{
   return SensorMock::GetDelegatee().dgram_recv(skd, ubuf, ulen);
}
  
sk_dgram* dgram_init(int sk, int dgram_max, void *options)
{
   return SensorMock::GetDelegatee().dgram_init(sk, dgram_max, options);
}

}

#endif //SENSPXYMOCKSTOBEREMOVED

#endif
