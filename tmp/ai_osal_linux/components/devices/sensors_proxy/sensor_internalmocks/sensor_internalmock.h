#ifndef _POSMOCK_H_
#define _POSMOCK_H_

#include "gmock/gmock.h"
#include "MockDelegatee.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if_mock.h"
#include "inc.h"
#include "System_types.h"
#include "Ostypes.h"
#include "sensor_dispatcher.h"
#include "hwinfo.h"
#include "sensor_ring_buffer.h"
#include "inc2soc.h"
#include "Tri_types.h"
#include "poll.h"
#include "Dgram_service.h"

using namespace testing;

class SensorMock : public MockDelegatee<SensorMock>
{
   public:
      static inline const char *GetMockName() { return "SensorMock"; }
      static SensorMock &GetDelegatee()
      {
         return MockDelegatee<SensorMock>::GetDelegatee();
      }

      MOCK_METHOD1(s32SenDispInit, tS32(tEnSenDrvID enSenID));
      MOCK_METHOD1(s32SenDispDeInit, tS32(tEnSenDrvID enSenID));
      MOCK_METHOD0(s32SensDispTrigGyroSelfTest, tS32(tVoid));
      MOCK_METHOD0(s32SensDispTrigAccSelfTest, tS32(tVoid));
      MOCK_METHOD1(s32SensRngBuffOpen, tS32(tEnSenDrvID enDeviceID));
      MOCK_METHOD1(s32SensRngBuffCls, tS32(tEnSenDrvID enDeviceID));
      MOCK_METHOD1(s32SensRngBuffGetAvailRecs, tS32(tEnSenDrvID enDeviceID));
      MOCK_METHOD2(s32SensHwInfobGetHwinfo, tS32(tEnSensor enType, OSAL_trIOCtrlHwInfo *prIoCtrlHwInfo));
      MOCK_METHOD3(s32SensRingBuffs32Read, tS32(tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32EntRequested));
      MOCK_METHOD3(s32SenRingBuffWrite, tS32( tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32NumOfEntries ));
      MOCK_METHOD2(s32SenRingBuffInit, tS32( tEnSenDrvID enDeviceID, const trSenRingBuffConfig *prSenRingBuffConfig ));
      MOCK_METHOD1(s32SensRingBuffDeinit, tS32( tEnSenDrvID enDeviceID ));
      MOCK_METHOD1(s32Inc2SocCreateResources, tS32( tEnInc2SocDev enDeviceType ));
      MOCK_METHOD1(vInc2SocPostDrivShutdown, tVoid(tEnInc2SocDev enDeviceType));
      MOCK_METHOD1(vInc2SocWriteDataToBuffer, tVoid(tEnInc2SocDev enDeviceType));
      #ifdef SENSPXYMOCKSTOBEREMOVED
      // Will be removed once OSAL team creates the following mocks.
      MOCK_METHOD2(LLD_bIsTraceActive, tBool(tU32, tU32));
      MOCK_METHOD4(LLD_vTrace, tVoid(tU32, tU32, const void*, tU32));
      MOCK_METHOD2(OSAL_s32ThreadJoin, tS32(OSAL_tThreadID, OSAL_tMSecond));
      // The Following mocks have to be remove once the mocks are 
      // in place from INC/Communication Team
      MOCK_METHOD3(dgram_init, sk_dgram* (int, int, void*));
      MOCK_METHOD3(dgram_send, int (sk_dgram*, void*, size_t));
      MOCK_METHOD3(dgram_recv, int (sk_dgram*, void*, size_t));
      MOCK_METHOD1(internalmock_close, int(int));
      MOCK_METHOD2(internalmock_open, int(const char*, int));
      MOCK_METHOD3(internalmock_write, ssize_t(int, const void*, size_t));
      MOCK_METHOD3(internalmock_socket, int(int , int , int));
      MOCK_METHOD3(internalmock_bind, int(int , const struct sockaddr*, socklen_t));
      MOCK_METHOD3(internalmock_connect, int(int , const struct sockaddr*, socklen_t));
      MOCK_METHOD3(poll, int(struct pollfd *fds, nfds_t nfds, int timeout));
      #endif
};

#define  s32SensorDiapatcherInit                mocked_s32SensorDiapatcherInit
#define  s32SensorDiapatcherDeInit              mocked_s32SensorDiapatcherDeInit
#define  SenDisp_s32TriggerGyroSelfTest         mocked_SenDisp_s32TriggerGyroSelfTest
#define  SenDisp_s32TriggerAccSelfTest          mocked_SenDisp_s32TriggerAccSelfTest
#define  SenRingBuff_s32Close                   mocked_SenRingBuff_s32Close
#define  SenRingBuff_s32Open                    mocked_SenRingBuff_s32Open
#define  SenRingBuff_s32GetAvailRecords         mocked_SenRingBuff_s32GetAvailRecords
#define  SenRingBuff_s32Read                    mocked_SenRingBuff_s32Read
#define  SenRingBuff_s32Write                   mocked_SenRingBuff_s32Write
#define  SenRingBuff_s32Init                    mocked_SenRingBuff_s32Init
#define  SenRingBuff_s32DeInit                  mocked_SenRingBuff_s32DeInit
#define  SenHwInfo_bGetHwinfo                   mocked_SenHwInfo_bGetHwinfo
#define  Inc2Soc_s32CreateResources             mocked_Inc2Soc_s32CreateResources
#define  Inc2Soc_vWriteDataToBuffer             mocked_Inc2Soc_vWriteDataToBuffer
#define  Inc2Soc_vPostDrivShutdown              mocked_Inc2Soc_vPostDrivShutdown
#ifdef SENSPXYMOCKSTOBEREMOVED
// The Following mocks have to be remove once the mocks are 
// in place from INC/Communication Team
#define  close                                  mocked_close
#define  open                                   mocked_open
#define  write                                  mocked_write
#define  socket                                 mocked_socket
#define  bind                                   mocked_bind
#define  connect                                mocked_connect
#endif

tS32  mocked_s32SensorDiapatcherInit(tEnSenDrvID enSenID);
tS32  mocked_s32SensorDiapatcherDeInit(tEnSenDrvID enSenID);
tS32  mocked_SenDisp_s32TriggerGyroSelfTest(tVoid);
tS32  mocked_SenDisp_s32TriggerAccSelfTest(tVoid);
tS32  mocked_SenRingBuff_s32Open( tEnSenDrvID enDeviceID );
tS32  mocked_SenRingBuff_s32Close( tEnSenDrvID enDeviceID );
tS32  mocked_SenRingBuff_s32GetAvailRecords( tEnSenDrvID enDeviceID );
tS32  mocked_SenRingBuff_s32Read( tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32EntRequested );
tS32  mocked_SenRingBuff_s32Write( tEnSenDrvID enDeviceID, tPVoid pvData, tU32 u32NumOfEntries );
tS32  mocked_SenRingBuff_s32Init( tEnSenDrvID enDeviceID, const trSenRingBuffConfig *prSenRingBuffConfig );
tS32  mocked_SenRingBuff_s32DeInit( tEnSenDrvID enDeviceID );
tBool mocked_SenHwInfo_bGetHwinfo ( tEnSensor enType , OSAL_trIOCtrlHwInfo *prIoCtrlHwInfo);
tS32  mocked_Inc2Soc_s32CreateResources( tEnInc2SocDev enDeviceType );
tVoid mocked_Inc2Soc_vWriteDataToBuffer( tEnInc2SocDev enDeviceType );
tVoid mocked_Inc2Soc_vPostDrivShutdown( tEnInc2SocDev enDeviceType );
#ifdef SENSPXYMOCKSTOBEREMOVED
// The Following mocks have to be remove once the mocks are 
// in place from INC/Communication Team 
int mocked_close(int fd);
int mocked_open(const char *pathname, int flags);
ssize_t mocked_write(int, const void*, size_t);
int mocked_socket(int domain, int type, int protocol);
int mocked_bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int mocked_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
#endif

#endif

