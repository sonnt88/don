#define SENSPXYMOCKSTOBEREMOVED
#include "Sensor_InternalMock.h"
#include "Sensor_InternalMock.cpp"

#ifdef __cplusplus
extern "C"
{
#include "../source/dev_acc.c"
}
#endif

using namespace testing;
using namespace std;

int main(int argc, char** argv)
{
   ::testing::InitGoogleMock(&argc, argv);
   return RUN_ALL_TESTS();
}

tVoid Acc_setThreadRunning()
{
   bAccThreadRunning = FALSE;
}

tVoid Acc_ResetGlobals()
{
   u8Acc_Flags =0;
   bAccConfigEn = FALSE;
   bAccThreadRunning = FALSE;
   enAccModel = ACC_TYPE_INVALID;
   rAccConfigData = {0};
}

class Acc_InitGlobals : public ::testing::Test
{
public:
   SensorMock PoS;
   NiceMock<OsalMock> Osal_mock;
   virtual void SetUp()
   {
      Acc_ResetGlobals();
   }
};

class Acc_Open : public Acc_InitGlobals
{
public:
   virtual void SetUp()
   {
      tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
      pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
      pu8Buff[1] = ACC_TYPE_BST_BMI055;
      pu8Buff[2] = 50;
      pu8Buff[3] = 0;
      Acc_InitGlobals::SetUp();
      EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
      EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(rAccConfigData))));
      EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
      ACC_s32IOOpen();
   }
};

/********************ACC Open********************/

TEST_F(Acc_InitGlobals, Acc_IOOpen_Success)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(rAccConfigData))));
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_s32IOOpen();
   EXPECT_EQ(ACC_TYPE_BST_BMI055, enAccModel);
   EXPECT_EQ(50, rAccConfigData.u16AccDataInterval);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(TRUE, bAccConfigEn);
   EXPECT_EQ(DEV_ACC_OPEN, u8Acc_Flags);
   EXPECT_EQ(TRUE, bAccThreadRunning);
}

TEST_F(Acc_InitGlobals, Acc_IOOpen_DispError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_s32IOOpen();
   EXPECT_EQ(ACC_TYPE_INVALID, enAccModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bAccConfigEn);
   EXPECT_EQ(DEV_ACC_FLAG_CLEAR, u8Acc_Flags);
   EXPECT_EQ(FALSE, bAccThreadRunning);
   EXPECT_EQ(0, rAccConfigData.u16AccDataInterval);
}

TEST_F(Acc_InitGlobals, Acc_IOOpen_RngBuffError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = ACC_s32IOOpen();
   EXPECT_EQ(ACC_TYPE_INVALID, enAccModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bAccConfigEn);
   EXPECT_EQ(DEV_ACC_FLAG_CLEAR, u8Acc_Flags);
   EXPECT_EQ(FALSE, bAccThreadRunning);
}


TEST_F(Acc_InitGlobals, Acc_IOOpen_MsgQueOpenError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_s32IOOpen();
   EXPECT_EQ(ACC_TYPE_INVALID, enAccModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bAccConfigEn);
   EXPECT_EQ(DEV_ACC_FLAG_CLEAR, u8Acc_Flags);
   EXPECT_EQ(FALSE, bAccThreadRunning);
}

TEST_F(Acc_InitGlobals, Acc_IOOpen_MsgQueWaitError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(Return(0));
   s32RetVal = ACC_s32IOOpen();
   EXPECT_EQ(ACC_TYPE_INVALID, enAccModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(FALSE, bAccConfigEn);
   EXPECT_EQ(DEV_ACC_FLAG_CLEAR, u8Acc_Flags);
   EXPECT_EQ(FALSE, bAccThreadRunning);
}

TEST_F(Acc_InitGlobals, Acc_IOOpen_ThdSpwnError)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SenDispInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffOpen(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueOpen(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(rAccConfigData))));
   EXPECT_CALL(Osal_mock, ThreadSpawn(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_s32IOOpen();
   EXPECT_EQ(ACC_TYPE_BST_BMI055, enAccModel);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
   EXPECT_EQ(TRUE, bAccConfigEn);
   EXPECT_EQ(DEV_ACC_FLAG_CLEAR, u8Acc_Flags);
}

TEST_F(Acc_InitGlobals, Acc_IOOpen_AlreadyOpen)
{
   tS32 s32RetVal= OSAL_E_NOERROR;
   u8Acc_Flags = DEV_ACC_OPEN;
   s32RetVal = ACC_s32IOOpen();
   EXPECT_EQ(OSAL_E_ALREADYOPENED, s32RetVal);
}

/********************Acc Close********************/

TEST_F(Acc_Open, Acc_close_success)
{
   tS32 s32RetVal = OSAL_ERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(ACC_C_EV_THD_EXT),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32MessageQueueStatus(_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<3>(1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(rAccConfigData))));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal=ACC_s32IOClose();
   EXPECT_EQ( FALSE, bAccThreadRunning );
   EXPECT_EQ( 0, u8Acc_Flags );
   EXPECT_EQ( OSAL_E_NOERROR, s32RetVal );
}

TEST_F(Acc_Open, Acc_close_RngBufError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(ACC_C_EV_THD_EXT),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32MessageQueueStatus(_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<3>(1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(rAccConfigData))));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_s32IOClose();
   EXPECT_EQ( FALSE, bAccThreadRunning );
   EXPECT_EQ( 0, u8Acc_Flags );
   EXPECT_EQ( OSAL_ERROR, s32RetVal );
}

TEST_F(Acc_Open, Acc_close_DeinitError)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(ACC_C_EV_THD_EXT),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32MessageQueueStatus(_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<3>(1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(rAccConfigData))));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_s32IOClose();
   EXPECT_EQ( FALSE, bAccThreadRunning );
   EXPECT_EQ( 0, u8Acc_Flags );
   EXPECT_EQ( OSAL_ERROR, s32RetVal );
}

TEST_F(Acc_Open, Acc_close_NotOpen)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(PoS, s32SensRngBuffCls(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(PoS, s32SenDispDeInit(SEN_DISP_ACC_ID)).Times(1).WillOnce(Return(OSAL_ERROR));
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(ACC_C_EV_THD_EXT),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32MessageQueueStatus(_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<3>(1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(rAccConfigData))));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_s32IOClose();
   EXPECT_EQ( 0, u8Acc_Flags );
   EXPECT_EQ( OSAL_ERROR, s32RetVal );
}



/********************Acc TgrTdExtAndClrMsgQ ********************/

TEST_F(Acc_Open, Acc_TgrTdExtAndClrMsgQ_Success)
{
   tS32 s32RetVal = OSAL_ERROR;
   tU8 pu8Buff[ACC_MSG_QUE_LENGTH];
   pu8Buff[0] = SEN_DISP_MSG_TYPE_CONFIG;
   pu8Buff[1] = ACC_TYPE_BST_BMI055;
   pu8Buff[2] = 50;
   pu8Buff[3] = 0;
   EXPECT_CALL(Osal_mock, s32MessageQueuePost(_,_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(ACC_C_EV_THD_EXT),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32ThreadWait(_)).Times(1);
   EXPECT_CALL(Osal_mock, s32MessageQueueStatus(_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<3>(1),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32MessageQueueWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArrayArgument<1>(pu8Buff, pu8Buff + sizeof(pu8Buff)), Return((tS32)sizeof(rAccConfigData))));
   EXPECT_CALL(Osal_mock, s32MessageQueueClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_s32TgrTdExtAndClrMsgQ();
   EXPECT_EQ(OSAL_OK,s32RetVal);
}



/********************Acc Handle Event********************/
TEST_F(Acc_Open, Acc_HandleEventCreate_ReturnOK)
{
   tU8 u8Task = EVENT_CREATE;
   tS32 s32RetVal= OSAL_ERROR;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_S32HandleEvent(u8Task);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Acc_Open, Acc_HandleEventCreate_RetError)
{
   tU8 u8Task = EVENT_CREATE;
   tS32 s32RetVal= OSAL_ERROR;
   EXPECT_CALL(Osal_mock, s32EventCreate(_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = ACC_S32HandleEvent(u8Task);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}


TEST_F(Acc_Open, Acc_HandleEventDelete_ReturnOK)
{
   tU8 u8Task = EVENT_DELETE;
   tS32 s32RetVal= OSAL_ERROR;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_S32HandleEvent(u8Task);
   EXPECT_EQ(OSAL_OK, s32RetVal);
}

TEST_F(Acc_Open, Acc_HandleEventDelete_RetError)
{
   tU8 u8Task = EVENT_DELETE;
   tS32 s32RetVal= OSAL_ERROR;
   EXPECT_CALL(Osal_mock, s32EventClose(_)).Times(1).WillOnce(Return(OSAL_OK));
   EXPECT_CALL(Osal_mock, s32EventDelete(_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = ACC_S32HandleEvent(u8Task);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

/********************Acc Selftest********************/
TEST_F(Acc_Open, Acc_ExecuteSelfTest)
{
   tS32 s32Dummyargument;
   tS32 s32RetVal = OSAL_ERROR;
   EXPECT_CALL(PoS, s32SensDispTrigAccSelfTest()).Times(1);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(DoAll(SetArgPointee<4>(ACC_C_EV_SELFTEST_FINISH),Return(OSAL_OK)));
   EXPECT_CALL(Osal_mock, s32EventPost(_,_,_)).Times(1).WillOnce(Return(OSAL_OK));
   s32RetVal = ACC_s32ExecuteSelfTest(&s32Dummyargument);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(Acc_Open, Acc_ExecuteSelfTest_RetError)
{
   tS32 s32Dummyargument;
   tS32 s32RetVal = OSAL_E_NOERROR;
   EXPECT_CALL(PoS, s32SensDispTrigAccSelfTest()).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = ACC_s32ExecuteSelfTest(&s32Dummyargument);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(Acc_Open, Acc_ExecuteSelfTest_EventWaitFail)
{
   tS32 s32Dummyargument;
   tS32 s32RetVal = OSAL_E_NOERROR;
   EXPECT_CALL(PoS, s32SensDispTrigAccSelfTest()).Times(1);
   EXPECT_CALL(Osal_mock, s32EventWait(_,_,_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = ACC_s32ExecuteSelfTest(&s32Dummyargument);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}




/********************Acc Read********************/
TEST_F(Acc_Open, Acc_Read)
{
   OSAL_trIOCtrlAccData *pAccData[5];
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 60;
   for (int i =0; i<5; i++)
   {
      pAccData[i] = (OSAL_trIOCtrlAccData *)malloc(sizeof(OSAL_trIOCtrlAccData));
      pAccData[i]->u16ErrorCounter= 0;
      pAccData[i]->u16Acc_x= 1000;
      pAccData[i]->u16Acc_y = 1000;
      pAccData[i]->u16Acc_z = 1000;
      pAccData[i]->u32TimeStamp = 115200;
   }
   EXPECT_CALL(PoS, s32SensRingBuffs32Read(_,_,_)).Times(1).WillOnce(Return(5));
   s32RetVal = ACC_s32IORead((tPS8)pAccData, u32maxbytes);
   EXPECT_EQ(60, s32RetVal);
}



TEST_F(Acc_Open, Acc_Read_NoData)
{
   OSAL_trIOCtrlAccData *pAccData = {0};
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 12;
   s32RetVal = ACC_s32IORead((tPS8)pAccData, u32maxbytes);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(Acc_Open, Acc_Read_RngBufReadFail)
{
   OSAL_trIOCtrlAccData *pAccData[5];
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 60;
   for (int i =0; i<5; i++)
   {
      pAccData[i] = (OSAL_trIOCtrlAccData *)malloc(sizeof(OSAL_trIOCtrlAccData));
      pAccData[i]->u16ErrorCounter= 0;
      pAccData[i]->u16Acc_x= 1000;
      pAccData[i]->u16Acc_y = 1000;
      pAccData[i]->u16Acc_z = 1000;
      pAccData[i]->u32TimeStamp = 115200;
   }
   EXPECT_CALL(PoS, s32SensRingBuffs32Read(_,_,_)).Times(1).WillOnce(Return(OSAL_ERROR));
   s32RetVal = ACC_s32IORead((tPS8)pAccData, u32maxbytes);
   EXPECT_EQ(OSAL_ERROR, s32RetVal);
}

TEST_F(Acc_Open, Acc_Read_RngBufRetTimeout)
{
   OSAL_trIOCtrlAccData *pAccData[5];
   tS32  s32RetVal = OSAL_E_NOERROR;
   tU32 u32maxbytes = 60;
   for (int i =0; i<5; i++)
   {
      pAccData[i] = (OSAL_trIOCtrlAccData *)malloc(sizeof(OSAL_trIOCtrlAccData));
      pAccData[i]->u16ErrorCounter= 0;
      pAccData[i]->u16Acc_x = 1000;
      pAccData[i]->u16Acc_y = 1000;
      pAccData[i]->u16Acc_z = 1000;
      pAccData[i]->u32TimeStamp = 115200;
   }
   EXPECT_CALL(PoS, s32SensRingBuffs32Read(_,_,_)).Times(1).WillOnce(Return(OSAL_E_TIMEOUT));
   s32RetVal = ACC_s32IORead((tPS8)pAccData, u32maxbytes);
   EXPECT_EQ(OSAL_E_TIMEOUT, s32RetVal);
}

/********************ACC IOControl********************/
TEST_F(Acc_Open, Acc_IOCtrl_ErrorCase)
{
   //similar to other IOControls, raken IOCTRL_VERSION as example
   tS32 s32RetVal = OSAL_E_NOERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_VERSION;
   tLong sArg = 0;
   s32RetVal = ACC_s32IOControl(s32fun, sArg);
   EXPECT_EQ(OSAL_E_INVALIDVALUE, s32RetVal);
}

TEST_F(Acc_Open, Acc_IOCtrl_VerSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_VERSION;
   tLong sDummyarg;
   tPLong sP32dummyarg = &sDummyarg; // dummy value
   s32RetVal = ACC_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(DEV_ACC_DRIVER_VERSION, sDummyarg);
}

TEST_F(Acc_Open, Acc_IOCtrl_GetCntSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_ACC_GETCNT;
   tLong sDummyarg;
   tPLong sP32dummyarg = &sDummyarg; // dummy value
   EXPECT_CALL(PoS, s32SensRngBuffGetAvailRecs(_)).Times(1).WillOnce(Return(100));
   s32RetVal = ACC_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(100, sDummyarg);
}

TEST_F(Acc_Open, Acc_IOCtrl_GetCycleTimeSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_ACC_GETCYCLETIME;
   tLong sDummyarg;
   tPLong sP32dummyarg = &sDummyarg; // dummy value
   s32RetVal = ACC_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(50000000, sDummyarg);
}

TEST_F(Acc_Open, Acc_IOCtrl_GetTempSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_ACC_GET_TEMP;
   tLong sDummyarg;
   tPLong sP32dummyarg = &sDummyarg; // dummy value
   bAccTempEn = TRUE;
   rAccTemp.u16AccTemp = 35;
   s32RetVal = ACC_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
   EXPECT_EQ(35, sDummyarg);
}

TEST_F(Acc_Open, Acc_IOCtrl_GetHwinfoSuccessCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32fun = OSAL_C_S32_IOCTRL_ACC_GET_HW_INFO;
   tLong sDummyarg;
   tPLong sP32dummyarg = &sDummyarg; // dummy value
   EXPECT_CALL(PoS, s32SensHwInfobGetHwinfo(_,_)).Times(1).WillOnce(Return(TRUE));
   s32RetVal = ACC_s32IOControl(s32fun, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOERROR, s32RetVal);
}

TEST_F(Acc_Open, Acc_IOCtrl_DefaultCase)
{
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32func = 100;
   tLong sDummyarg;
   tPLong sP32dummyarg = &sDummyarg; // dummy value
   s32RetVal = ACC_s32IOControl(s32func, (tLong)sP32dummyarg);
   EXPECT_EQ(OSAL_E_NOTSUPPORTED, s32RetVal);
}
//EOF
