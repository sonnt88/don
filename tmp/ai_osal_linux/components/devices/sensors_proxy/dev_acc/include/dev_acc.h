/************************************************************************
* FILE         : dev_acc.h
*             
* DESCRIPTION  : dev_acc header file.                               
*
* HISTORY      :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
* 3/14/2013 | Initialversion 1.0  | G Sanjay(RBEI/ECF5)
* -----------------------------------------------------------------------
*************************************************************************/


/*acc sensor hardware information*/
typedef struct
{
    tU32 u32AdcRangeMin;
    tU32 u32AdcRangeMax;
    tU32 u32SampleMin;
    tU32 u32SampleMax;
    tF32 f32MinNoiseValue;
    tF32 f32EstimOffset;
    tF32 f32MinOffset;
    tF32 f32MaxOffset;
    tF32 f32DriftOffset;
    tF32 f32MaxUnsteadOffset;
    tF32 f32BestCalibOffset;
    tF32 f32EstimscaleFactor;
    tF32 f32MinScaleFactor;
    tF32 f32MaxScaleFactor;
    tF32 f32DriftScaleFactor;
    tF32 f32MaxUnsteadScaleFactor;
    tF32 f32BestCalibScaleFactor;
}trAccSensorHwInfo;


/*Accelerometer type which is using in the project*/

typedef enum 
{
   ACC_TYPE_INVALID=0x00,
   ACC_TYPE_BST_BMI055=0x01
}tenAccModel;

//EOF
