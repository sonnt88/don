/******************************************************************************
* FILE         : dev_acc.c
*             
* DESCRIPTION  : This file contains source code of dev_acc Gen3 device driver                               
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------------
* 3/14/2013 | Initial version 1.0 | G Sanjay(RBEI/ECF5)
* -----------------------------------------------------------------------------
* 06/26/2014| Version: 1.1        | Madhu Kiran Ramachandra (RBEI/ECF5)
*           |                     | Moved Config message receive section to open 
* -----------------------------------------------------------------------------
* 08/08/2014| Version: 1.2        | Madhu Kiran Ramachandra (RBEI/ECF5)
*           |                     | Fix for GMMY16-15647. bAccThreadRunning updated correctly.
* -----------------------------------------------------------------------------
* 11/12/2015| Version: 1.3        | Shivasharnappa Mothpalli (RBEI/ECF5)
*           |                     | Removed Unused IOCTRLS (Fix for  CFG3-1622):
            |                     | OSAL_C_S32_IOCTRL_ACC_FLUSH,
            |                     | OSAL_C_S32_IOCTRL_ACC_SET_TIMEOUT_VALUE,
            |                     | OSAL_C_S32_IOCTRL_ACC_GET_TIMEOUT_VALUE,
            |                     | OSAL_C_S32_IOCTRL_ACC_START,
            |                     | OSAL_C_S32_IOCTRL_ACC_STOP,
            |                     | OSAL_C_S32_IOCTRL_ACC_GET_DIAG_RAW_DATA.                                           
* ---------------------------------------------------------------------------
*******************************************************************************/

/*-----------------------------------------------------------------------------
* Header file declaration
*-----------------------------------------------------------------------------*/

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"

#include "sensor_dispatcher.h"
#include "sensor_ring_buffer.h"
#include "hwinfo.h"
#include "dev_acc.h"

/*************************************************************************
* Macro declaration (scope: Global)
*------------------------------------------------------------------------*/

/*return value for ioctrl call OSAL_C_S32_IOCTRL_VERSION */
#define DEV_ACC_DRIVER_VERSION         0x0100  /* read v01.00 */


/*Acc device flags for create and open functinality*/
#define DEV_ACC_OPEN               0x01
#define DEV_ACC_FLAG_CLEAR         0x00

/* Flags to maintain the status of create and open of a device */
static tU8 u8Acc_Flags = 0;


/*Thread attribute related information*/
#define ACC_MAIN_THREAD_PRIORITY 49
#define ACC_MAIN_THREAD_STACK_SIZE 4096
#define ACC_C_STRING_MAIN_THREAD_NAME "ACC_TASK"


/*Max size of acc message*/
#define ACC_MESSAGE_LENGTH (sizeof(trAccConfigData))

/*Data Received From Message Queue Order With Respect To index*/
#define MESSAGE_TYPE_INDEX  0

/*!
* Acc Selftest event name.
*/
#define ACC_C_STRING_EVENT_SELFTEST "ACC_SELFTEST"


/*!
* Acc Thread Exit Event Name.
*/

#define ACC_C_STRING_EVENT_THDEXT "ACC_THREADEXITEVENT"


/*!
* macros for Event creation and deletion in self test
*/
#define EVENT_CREATE ((tU8)(0))
#define EVENT_DELETE ((tU8)(1))

/*!
* This bit is set in hAccEventSelfTest if the acc
* self test finish event occurs
*/
#define ACC_C_EV_SELFTEST_FINISH      ((tU32)(0x01))
#define ACC_C_EV_PATTERN              ((tU32)(0x01))

#define ACC_INIT_CONFIG_MSG_WAIT_TIME_MS ((OSAL_tMSecond)500)

/*!
* Time out for Acc self test 
*/
#define ACC_SELFTEST_TIMEOUT   ((tU32)(3000))

/*!
* Acc Self Test Status
*/
#define ACC_SELF_TEST_PASSED       ((tU8)(0x01))
#define ACC_SELF_TEST_FAILED       ((tU8)(0x00))

/*!
* Acc Self Test result
*/
#define  ACC_SELFTEST_RESULT_SUCCESS  ((tU8)(0xFF))

/*!
* Acc Event Pattern for Thread Exit
*/
#define ACC_C_EV_THD_EXT  ((tU32)(0x01))

/*!
* Time out for Acc Thd Ext 
*/
#define ACC_THDEXT_TIMEOUT   ((tU32)(2000))
/************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/

/*the flag is set when config data is available*/
static tBool bAccConfigEn = FALSE;

/*the flag is set when Temp data is available*/
static tBool bAccTempEn = FALSE;

/*osal thread running flag, set on thread creation*/
static tBool bAccThreadRunning = FALSE;

/*osal thread ID, returned on thread creation*/
static OSAL_tThreadID s32AccThreadID = OSAL_ERROR;

/*Acc Message Queue Handle*/
OSAL_tMQueueHandle hldAccMsgQue = 0;

/*Varible to hold Acc Configuration data*/
static trAccConfigData rAccConfigData;

/*Variable to hold Accelerometer temperature data*/
static trAccTemp  rAccTemp;

/*enum to maintain acc models*/
tenAccModel enAccModel=ACC_TYPE_INVALID;

/* Self test OSAL event handle*/
static OSAL_tEventHandle hAccEventSelfTest = OSAL_C_INVALID_HANDLE;

/*Acc Self Test Result*/
static tU8 u8AccSelfTestResult = ACC_SELF_TEST_FAILED;

/*!
* Acc Thread Exit Event Handle.
*/
static OSAL_tEventHandle hAccEventThdExt = OSAL_C_INVALID_HANDLE;

/********************************************************************************
* Function declaration (scope: Local to file)
*-------------------------------------------------------------------------------*/

static tVoid vAccMainThread(tVoid);

tS32 ACC_s32IOOpen(tVoid);
tS32 ACC_s32IOClose(tVoid);
tS32 ACC_s32IOControl(tS32 s32fun, tLong sArg);
tS32 ACC_s32IORead(tPS8 ps8Buffer, tU32 u32maxbytes);
tVoid DEV_ACC_vTraceOut(  TR_tenTraceLevel u32Level,const tChar *pcFormatString,... );
static tS32 ACC_S32HandleEvent(tU8 u8Task);
static tS32 ACC_s32ExecuteSelfTest(tS32 *ps32Argument);
static tS32 ACC_s32TgrTdExtAndClrMsgQ(tVoid);

/*-----------------------------------------------------------------------------
* Function Definitions
*-----------------------------------------------------------------------------*/


/*******************************************************************************
* FUNCTION     : ACC_s32IOOpen 
*
* PARAMETER    : NONE
*                                                      
* RETURNVALUE  : OSAL_E_NOERROR  on sucess
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : Opens Acc proxy driver                
*
* HISTORY      :
*--------------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|----------------------------------------------
* 06/26/2014| Version 1.1         | Madhu Kiran Ramachandra (RBEI/ECF5)
*           |                     | Moved Config message receive section to open 
* -----------------------------------------------------------------------------
*********************************************************************************/
tS32 ACC_s32IOOpen(tVoid)
{

   tS32 s32RetVal = OSAL_ERROR;
   tU8 pu8Buffer[ACC_MSG_QUE_LENGTH]={ 0 };

   /*Acc driver main thread attributes*/
   OSAL_trThreadAttribute rAttr;
   rAttr.szName = (tString)ACC_C_STRING_MAIN_THREAD_NAME;
   rAttr.u32Priority = ACC_MAIN_THREAD_PRIORITY;
   rAttr.s32StackSize = ACC_MAIN_THREAD_STACK_SIZE;
   rAttr.pfEntry = (OSAL_tpfThreadEntry)vAccMainThread;
   rAttr.pvArg = OSAL_NULL;
   
   DEV_ACC_vTraceOut( TR_LEVEL_USER_4, "Open enter" );
   
   /*check whether driver is opened already or not*/
   if(!(u8Acc_Flags & DEV_ACC_OPEN))
   {

      if(OSAL_OK != s32SensorDiapatcherInit(SEN_DISP_ACC_ID))
      {
         DEV_ACC_vTraceOut(TR_LEVEL_ERROR,"ACC_s32IOOpen:Dispatcher Init Failed");
      }
      /*Open Acc ring buffer and get handle*/
      else if(OSAL_OK != SenRingBuff_s32Open(SEN_DISP_ACC_ID))
      {
         (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_ACC_ID);
         DEV_ACC_vTraceOut(TR_LEVEL_ERROR,"ACC_s32IOOpen:SenRingBuff_s32Open Failed");
      }
      /*open the message queue */
      else if(OSAL_OK != OSAL_s32MessageQueueOpen((tCString)SEN_DISP_TO_ACC_MSGQUE_NAME,
               (OSAL_tenAccess)OSAL_EN_READWRITE,&hldAccMsgQue))
      {
         (tVoid)SenRingBuff_s32Close(SEN_DISP_ACC_ID);
         (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_ACC_ID);
         DEV_ACC_vTraceOut(TR_LEVEL_ERROR,"ACC_s32IOOpen:Openning message Queue failed");
      }
      else 
      {
         if ( FALSE == bAccConfigEn )
         {
            if ( (tS32)sizeof(trAccConfigData) != OSAL_s32MessageQueueWait(
                     hldAccMsgQue,
                     pu8Buffer,
                     ACC_MSG_QUE_LENGTH,
                     OSAL_NULL,
                     ACC_INIT_CONFIG_MSG_WAIT_TIME_MS) )
            {
               DEV_ACC_vTraceOut( TR_LEVEL_FATAL, "INIT:MQ wait fail err %lu", OSAL_u32ErrorCode());
            }
            else if ( SEN_DISP_MSG_TYPE_CONFIG != pu8Buffer[0] )
            {
               DEV_ACC_vTraceOut( TR_LEVEL_FATAL, "In-correct config Msg. ID %d", (tS32)pu8Buffer[0] );
               (tVoid)OSAL_s32MessageQueueClose( hldAccMsgQue);
               (tVoid)SenRingBuff_s32Close (SEN_DISP_ACC_ID );
               (tVoid)s32SensorDiapatcherDeInit (SEN_DISP_ACC_ID);
            }
            else
            {
               rAccConfigData.u8AccType = ((trAccConfigData *)pu8Buffer)->u8AccType;
               rAccConfigData.u16AccDataInterval=((trAccConfigData *)pu8Buffer)->u16AccDataInterval;
               
               if(rAccConfigData.u8AccType==(tU8)ACC_TYPE_BST_BMI055)
               {
                  enAccModel=ACC_TYPE_BST_BMI055;
               }
               else
               {
                  enAccModel=ACC_TYPE_INVALID;
               }
               bAccConfigEn = TRUE;
               DEV_ACC_vTraceOut( TR_LEVEL_USER_4,"Init:AccType %d  Acc Int %d",
               rAccConfigData.u8AccType,rAccConfigData.u16AccDataInterval);
            }
         }
         if ( TRUE == bAccConfigEn )
         {
            if( OSAL_s32EventCreate ( (tString) ACC_C_STRING_EVENT_THDEXT, &hAccEventThdExt ) != OSAL_OK )
            {
               s32RetVal = OSAL_ERROR;
               DEV_ACC_vTraceOut( TR_LEVEL_FATAL,
               "ACC_s32IOOpen: Event Create failed with error 0x%x",
               OSAL_u32ErrorCode());
               (tVoid)OSAL_s32MessageQueueClose( hldAccMsgQue );
               (tVoid)SenRingBuff_s32Close(SEN_DISP_ACC_ID );
               (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_ACC_ID);
            }
            else
            {
            
               bAccThreadRunning = TRUE;
               s32AccThreadID = OSAL_ThreadSpawn(&rAttr);
               if( s32AccThreadID == OSAL_ERROR)
               {
                  bAccThreadRunning = FALSE;
                  (tVoid)SenRingBuff_s32Close(SEN_DISP_ACC_ID);
                  (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_ACC_ID);
                  (tVoid)OSAL_s32MessageQueueClose(hldAccMsgQue);
                  (tVoid)OSAL_s32EventClose( hAccEventThdExt );
                  (tVoid)OSAL_s32EventDelete( (tString)ACC_C_STRING_EVENT_THDEXT );
                  DEV_ACC_vTraceOut(TR_LEVEL_FATAL,"ACC_s32IOOpen:Acc Main Thread Creation Failed");
               }
               else
               {
                  u8Acc_Flags |= DEV_ACC_OPEN;
                  s32RetVal = OSAL_E_NOERROR;
                  DEV_ACC_vTraceOut(TR_LEVEL_ERROR,"ACC Init success");
               }
            }
         }
      }
   }
   else
   {
      s32RetVal=OSAL_E_ALREADYOPENED;
      DEV_ACC_vTraceOut(TR_LEVEL_ERROR,"ACC_s32IOOpen:Acc driver already opened");
   }

   return s32RetVal;
}


/********************************************************************************
* FUNCTION     : ACC_s32IOClose 
*
* PARAMETER    : NONE
*                                                      
* RETURNVALUE  : OSAL_E_NOERROR  on sucess
*                OSAL_ERROR on Failure
*
* DESCRIPTION  : Closes Acc device           
*
* HISTORY      :
*--------------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|----------------------------------------------
*********************************************************************************/

tS32 ACC_s32IOClose(tVoid)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   
   /*Check whether driver is actually opened or not*/
   if(u8Acc_Flags & DEV_ACC_OPEN)
   {
      /*Change Acc driver status flags appropriately*/
      bAccThreadRunning = FALSE;
      u8Acc_Flags &= DEV_ACC_FLAG_CLEAR;

      
      /*Close ring buffer*/
      if(OSAL_OK != SenRingBuff_s32Close(SEN_DISP_ACC_ID))
      {
         DEV_ACC_vTraceOut( TR_LEVEL_ERROR, "ACC_s32IOClose:Ring buffer Closing Failed ");
         s32RetVal = OSAL_ERROR;
      }
      
      /*Deinitialise dispatcher*/
      if(OSAL_ERROR == s32SensorDiapatcherDeInit(SEN_DISP_ACC_ID))
      {
         DEV_ACC_vTraceOut( TR_LEVEL_ERROR, "ACC_s32IOClose:DiapatcherDeInit Failed ");
         s32RetVal = OSAL_ERROR;
      }
      
      if( OSAL_OK != ACC_s32TgrTdExtAndClrMsgQ() )
      {
         s32RetVal = OSAL_ERROR;
         DEV_ACC_vTraceOut( TR_LEVEL_ERROR, "ACC_s32IOClose:Thread Exit Failed");
      }
   }
   else
   {
      DEV_ACC_vTraceOut( TR_LEVEL_ERROR, "ACC_s32IOClose:Driver already closed or not opened");
      s32RetVal = OSAL_ERROR;
   }
   
   return s32RetVal;
}

/********************************************************************************
* FUNCTION        : ACC_s32TgrTdExtAndClrMsgQ 

* PARAMETER       : tVoid
*                                                      
* RETURNVALUE     : OSAL_OK  on success
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : call to handle thread exit event and msg queue clear               
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19.AUG.2015| Initialversion 1.0  | Srinivas Prakash Anvekar (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static tS32 ACC_s32TgrTdExtAndClrMsgQ(tVoid)
{
   tS32 s32RetVal = OSAL_OK;
   tU8 pu8Buffer[ACC_MSG_QUE_LENGTH]={ 0 };
   tU32 u32Messages;
   OSAL_tEventMask u32AccEveThdExtRes = 0;
   tU8 myTerminateMsg = 0;

   if (OSAL_ERROR == OSAL_s32MessageQueuePost(  hldAccMsgQue,
                                                (tCU8*)&myTerminateMsg,
                                                sizeof(tU8),
                                                OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST))
   {
      s32RetVal = OSAL_ERROR;
      DEV_ACC_vTraceOut( TR_LEVEL_ERROR,
                         "ACC_s32TgrTdExtAndClrMsgQ:MsgQ Post Failed with error %d",
                         OSAL_u32ErrorCode());
   }

   if( OSAL_s32EventWait( hAccEventThdExt,
               ACC_C_EV_THD_EXT,
               OSAL_EN_EVENTMASK_OR,
               ACC_THDEXT_TIMEOUT,
               &u32AccEveThdExtRes ) != OSAL_OK ) 
   {
      s32RetVal = OSAL_ERROR;
      DEV_ACC_vTraceOut( TR_LEVEL_ERROR,
                         "ACC_s32TgrTdExtAndClrMsgQ :Event Wait Failed with error 0x%x",
                         OSAL_u32ErrorCode() );
   }
   else if( u32AccEveThdExtRes & ACC_C_EV_THD_EXT )
   {
      if( OSAL_OK != ( OSAL_s32EventPost( hAccEventThdExt,
                                          ~ACC_C_EV_THD_EXT,
                                          OSAL_EN_EVENTMASK_AND ) ) )
      {
         DEV_ACC_vTraceOut( TR_LEVEL_ERROR,
         "ACC_s32TgrTdExtAndClrMsgQ: Event post failed with error 0x%x",
         OSAL_u32ErrorCode());
         s32RetVal = OSAL_ERROR;
      }
      /*
       Main thread doesn't exit immediately once it post the event.
       Just give some time for the thread to exit.
      */
      OSAL_s32ThreadWait(100);
   }

   /*
   Check number of messages left in the Queue and if messages exist then
   clear all the messages by reading. Since if any one else opens 
   dev_acc again.These invalid messages will be read out.
   */
   if( OSAL_ERROR == OSAL_s32MessageQueueStatus
                     ( hldAccMsgQue, OSAL_NULL, OSAL_NULL, &u32Messages ) )
   {
      DEV_ACC_vTraceOut( TR_LEVEL_ERROR,"ACC_s32TgrTdExtAndClrMsgQ :MsgQue Status returned failed" );
      s32RetVal = OSAL_ERROR;
   }
   else
   {
      while( 0 != u32Messages )
      {
         if( OSAL_ERROR == OSAL_s32MessageQueueWait(  hldAccMsgQue,
                                                      (tPU8)pu8Buffer,
                                                      sizeof(pu8Buffer),
                                                      OSAL_NULL,
                                                      (OSAL_tMSecond)OSAL_C_TIMEOUT_NOBLOCKING ) )
         {
            DEV_ACC_vTraceOut( TR_LEVEL_ERROR,"ACC_s32TgrTdExtAndClrMsgQ :MsgQue Wait Failed" );
            s32RetVal = OSAL_ERROR;
         }
         u32Messages--;
      }
   }

   if( OSAL_OK !=  OSAL_s32MessageQueueClose( hldAccMsgQue ) )
   {
      /* error, Message Queue close failed */
      s32RetVal = OSAL_ERROR;
      DEV_ACC_vTraceOut( TR_LEVEL_ERROR ,"ACC_s32TgrTdExtAndClrMsgQ :close message Queue failed" );
   }

   /*close the event*/
   if( OSAL_s32EventClose( hAccEventThdExt ) != OSAL_OK )
   {
      s32RetVal = OSAL_ERROR;
      DEV_ACC_vTraceOut( TR_LEVEL_ERROR,
      "ACC_s32TgrTdExtAndClrMsgQ: Event close failed with error 0x%x",
      OSAL_u32ErrorCode()); 
   }
   
   /*delete the event*/
   if( OSAL_s32EventDelete((tString)ACC_C_STRING_EVENT_THDEXT) != OSAL_OK)
   {
      s32RetVal = OSAL_ERROR;
      DEV_ACC_vTraceOut( TR_LEVEL_ERROR,
      "ACC_s32TgrTdExtAndClrMsgQ: Event Delete failed with error 0x%x",
      OSAL_u32ErrorCode());
   }

   return s32RetVal;
}

/********************************************************************************
* FUNCTION        : ACC_S32HandleEvent 

* PARAMETER       : tU8
*                                                      
* RETURNVALUE     : OSAL_OK  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : call to handle acc self test event               
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19.MAY.2014| Initialversion 1.0  | Sai Chowdary Samineni (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static tS32 ACC_S32HandleEvent(tU8 u8Task)
{
   tS32 s32RetVal  = OSAL_OK;

   if(u8Task == EVENT_CREATE)
   {
      if( OSAL_s32EventCreate((tString)ACC_C_STRING_EVENT_SELFTEST,&hAccEventSelfTest) != OSAL_OK)
      {
         s32RetVal = OSAL_ERROR;
         DEV_ACC_vTraceOut( TR_LEVEL_ERROR,
         "ACC_S32HandleEvent: Event Create failed with error 0x%x",
         OSAL_u32ErrorCode());
      }
   }
   else if(u8Task == EVENT_DELETE)
   {
      if( OSAL_s32EventClose(hAccEventSelfTest) != OSAL_OK )
      {
         DEV_ACC_vTraceOut( TR_LEVEL_ERROR,
         "ACC_S32HandleEvent: Event close failed with error 0x%x",
         OSAL_u32ErrorCode()); 
      }
      if( OSAL_s32EventDelete((tString)ACC_C_STRING_EVENT_SELFTEST) != OSAL_OK)
      {
         s32RetVal = OSAL_ERROR;
         DEV_ACC_vTraceOut( TR_LEVEL_ERROR,
         "ACC_S32HandleEvent: Event Delete failed with error 0x%x",
         OSAL_u32ErrorCode());
      }
   }
   else
   {
      //Nothing
   }
   
   return s32RetVal;
}

/********************************************************************************
* FUNCTION        : ACC_s32ExecuteSelfTest 

* PARAMETER       : Void
*                                                      
* RETURNVALUE     : OSAL_OK  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : call to execute ACC self test                
*
* HISTORY         :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19.MAY.2014| Initialversion 1.0  | Sai Chowdary Samineni (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static tS32 ACC_s32ExecuteSelfTest(tS32 *ps32Argument)
{
   OSAL_tEventMask u32EventResult = 0;

   //Start acc self test
   tS32 s32RetVal = SenDisp_s32TriggerAccSelfTest();
   
   if(s32RetVal != OSAL_ERROR)
   {
      if( OSAL_s32EventWait(  hAccEventSelfTest,
               ACC_C_EV_PATTERN,
               OSAL_EN_EVENTMASK_OR,
               ACC_SELFTEST_TIMEOUT,
               &u32EventResult) == OSAL_OK ) 
      {
         if(u32EventResult & ACC_C_EV_SELFTEST_FINISH)
         {
            OSAL_s32EventPost( hAccEventSelfTest,
            ~ACC_C_EV_SELFTEST_FINISH,
            OSAL_EN_EVENTMASK_AND );
            *ps32Argument = (tS32)u8AccSelfTestResult;
            s32RetVal = OSAL_E_NOERROR; 
         }
      }
      else
      {
         DEV_ACC_vTraceOut( TR_LEVEL_FATAL,
         "ACC_s32ExecuteSelfTest: Event Wait Failed with error %d",
         OSAL_u32ErrorCode());
         s32RetVal = OSAL_ERROR;
      }
   }

   return s32RetVal;
}

/************************************************************************************
* FUNCTION    : ACC_s32IOControl()

* PARAMETER   : s32fun : Function identificator
                s32arg : Argument to be passed to function

* RETURNVALUE : tS32
*               OSAL_E_NOERROR : On Success
*               OSAL_ERROR: On Error

* DESCRIPTION : IO Control function for dev_acc
*
* HISTORY     :
*--------------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|------------------------------------------
* 12.MAY.2014  | Initial version :1.0  | Sai Chowdary Samineni (RBEI/ECF5)
*                                        Added Self test IO Control
*--------------|-----------------------|-------------------------------------
* 11.Dec.2015  | Version: 1.1          | Shivasharnappa Mothpalli (RBEI/ECF5)
*              |                       | Removed Unused IOCTRLS (Fix for  CFG3-1622):
               |                       | OSAL_C_S32_IOCTRL_ACC_FLUSH,
               |                       | OSAL_C_S32_IOCTRL_ACC_SET_TIMEOUT_VALUE,
               |                       | OSAL_C_S32_IOCTRL_ACC_GET_TIMEOUT_VALUE,
               |                       | OSAL_C_S32_IOCTRL_ACC_START,
               |                       | OSAL_C_S32_IOCTRL_ACC_STOP,
               |                       | OSAL_C_S32_IOCTRL_ACC_GET_DIAG_RAW_DATA.
* ---------------------------------------------------------------------------
*********************************************************************************/

tS32 ACC_s32IOControl(tS32 s32fun, tLong sArg)
{
   tS32  s32RetVal = OSAL_E_NOERROR;
   /* s32arg is address of variable where to store requested data */
   tPS32 ps32Argument =  (tPS32) sArg;

   DEV_ACC_vTraceOut(TR_LEVEL_USER_4,"ACC_s32IOControl: IOControl ENTERED");

   switch( s32fun )
   {
   case OSAL_C_S32_IOCTRL_VERSION:
      {
         /* check input parameter */
         if( ps32Argument != OSAL_NULL )
         {
            /*return internal version number*/
            *ps32Argument = (tS32)DEV_ACC_DRIVER_VERSION;
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

   case OSAL_C_S32_IOCTRL_ACC_GETCNT:
      {
         /* check input parameter */
         if( ps32Argument != OSAL_NULL )
         {
            tS32 s32NoOfRecords=0;
            (tVoid)s32NoOfRecords;   //Satisfy Lint!
            /*return number of valid ringbuffer entries*/
            s32NoOfRecords = SenRingBuff_s32GetAvailRecords(SEN_DISP_ACC_ID);
            if(s32NoOfRecords!=OSAL_ERROR)
            {
               *ps32Argument=s32NoOfRecords;
            }
            else
            {
               s32RetVal=OSAL_ERROR;
            }

         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }

   case OSAL_C_S32_IOCTRL_ACC_GET_TEMP:
      {
         if((bAccTempEn==TRUE)&&(ps32Argument != OSAL_NULL))
         {
            *ps32Argument=(tS32)rAccTemp.u16AccTemp;
         }
         else
         {
            s32RetVal=OSAL_ERROR;
         }
         break;
      }

   case OSAL_C_S32_IOCTRL_ACC_GET_HW_INFO:
      {
         if (OSAL_NULL != ps32Argument )
         {
            if ( FALSE == SenHwInfo_bGetHwinfo( ACC_ID ,
                     (OSAL_trIOCtrlHwInfo *) (tPVoid) ps32Argument))
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
         }
         break;
      }
   case OSAL_C_S32_IOCTRL_ACC_SELF_TEST:   
      {
         if( ps32Argument != OSAL_NULL )
         {
            if( ACC_S32HandleEvent(EVENT_CREATE) == OSAL_OK)
            {
               s32RetVal = ACC_s32ExecuteSelfTest(ps32Argument);
               ACC_S32HandleEvent(EVENT_DELETE);
            }
         }
         else
         {
            s32RetVal = OSAL_E_INVALIDVALUE;
         }
         break;
      }
   case OSAL_C_S32_IOCTRL_ACC_GETCYCLETIME:
      {
         if(bAccConfigEn)
         {
            /* check input parameter */
            if( ps32Argument != OSAL_NULL )
            {
               *ps32Argument = (tS32)((rAccConfigData.u16AccDataInterval )*1000000); 
            }
            else
            {
               s32RetVal = OSAL_E_INVALIDVALUE;
            }
         }
         else
         {
            s32RetVal =OSAL_E_INPROGRESS;
         }
         break;
      }

      default:
      {
         /* in case s32fun is neither of allowed above */
         s32RetVal =OSAL_E_NOTSUPPORTED;
         DEV_ACC_vTraceOut(TR_LEVEL_ERROR,"ACC_s32IOControl: IOControl Not supported");
         break;
      }
   }

   DEV_ACC_vTraceOut(TR_LEVEL_USER_4,"ACC_s32IOControl: IOControl EXIT");
   return(s32RetVal);
}

/********************************************************************************
* FUNCTION        : ACC_s32IORead 
*
* PARAMETER       : rIOCtrlAccData : pointer to struct
*                               where the read data must be copied
*                   u32maxbytes : max number of bytes for read buffer
*                                                      
* RETURNVALUE     : Number of bytes read on success
*                               OSAL_ERROR on Failure
*
* DESCRIPTION     : Reads ACC data
*
* HISTORY      :
*--------------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|----------------------------------------------
*********************************************************************************/

tS32 ACC_s32IORead(tPS8 ps8Buffer, tU32 u32maxbytes)
{
   tU32  u32NumberOfEntries = u32maxbytes/sizeof(OSAL_trIOCtrlAccData);
   tS32  s32RetVal = OSAL_E_NOERROR;
   tS32  s32ReadBytes = 0;	
   OSAL_trIOCtrlAccData *rIOCtrlAccData = (OSAL_trIOCtrlAccData *)(ps8Buffer);

   if(rIOCtrlAccData == OSAL_NULL)
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
      DEV_ACC_vTraceOut(TR_LEVEL_ERROR,"ACC_s32IORead:Application passing null buffer");
   }
   else
   {

      s32ReadBytes = SenRingBuff_s32Read( SEN_DISP_ACC_ID, rIOCtrlAccData, u32NumberOfEntries);
      if(s32ReadBytes == OSAL_ERROR)
      {
         DEV_ACC_vTraceOut(TR_LEVEL_ERROR,"ACC_s32IORead:Read from ring buffer failed with error %d",
         OSAL_u32ErrorCode()); 
         s32RetVal = OSAL_ERROR;

      }
      else if((tS32)OSAL_E_TIMEOUT == s32ReadBytes)
      {
         DEV_ACC_vTraceOut(TR_LEVEL_ERROR ,"ACC_s32IORead :Read from ring buffer failed due to timeout with error code =%d",
         OSAL_u32ErrorCode()); 
         s32RetVal = (tS32)OSAL_E_TIMEOUT;
      }   
      else
      {
         DEV_ACC_vTraceOut(TR_LEVEL_USER_4,"ACC_s32IORead: u32TimeStamp=%d   u16ErrorCounter:%d",
         rIOCtrlAccData->u32TimeStamp,rIOCtrlAccData->u16ErrorCounter);
         
         DEV_ACC_vTraceOut(TR_LEVEL_USER_4,"ACC_s32IORead:u16Acc_r=%d   u16Acc_s=%d  u16Acc_t=%d",
         rIOCtrlAccData->u16Acc_x,rIOCtrlAccData->u16Acc_y,rIOCtrlAccData->u16Acc_z);
         //return number of bytes read
         s32RetVal = (s32ReadBytes * (tS32)sizeof(OSAL_trIOCtrlAccData));

      }

   }
   
   return s32RetVal;    /*return no.of bytes read from Ringdbuffer*/
}

/************************************************************************************
* FUNCTION        : vAccMainThread 

* PARAMETER       : NONE
*                                                      
* RETURNVALUE     : OSAL_E_NOERROR  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     :  Osal thread function used to wait for configuration and error messages 
*                    sent by the dispatcher via message queue
* HISTORY         :
*--------------------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|----------------------------------------------------
*2.MAY.2014 |                     | Sai Chowdary Samineni (RBEI/ECF5)
*           |                     | Modified to include Self test Result
*-----------|---------------------|----------------------------------------------------
* 06/26/2014| Version 1.1         | Madhu Kiran Ramachandra (RBEI/ECF5)
*           |                     | Moved Config message receive section to open 
* -----------------------------------------------------------------------------
***************************************************************************************/
static tVoid vAccMainThread(tVoid)
{

   tU8 u8Buffer[ACC_MESSAGE_LENGTH]={ 0 };
   
   while( bAccThreadRunning == TRUE )
   {
      if( 0 == (OSAL_s32MessageQueueWait( hldAccMsgQue,(tPU8)u8Buffer,sizeof(u8Buffer),
                  (tPU32)OSAL_NULL,(OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER)))
      {
         DEV_ACC_vTraceOut( TR_LEVEL_ERROR, "vAccMainThread:message Queue Wait failed");
      }
      else if(bAccThreadRunning == TRUE)
      {
         switch( u8Buffer[MESSAGE_TYPE_INDEX])
         {
         case SEN_DISP_MSG_TYPE_CONFIG: //Configuration Message
            {
               DEV_ACC_vTraceOut(TR_LEVEL_FATAL,"Run time config update not expected");

               /*          Moved Config message receive section to open call

               rAccConfigData.u8AccType = ((trAccConfigData *)u8Buffer)->u8AccType;
               rAccConfigData.u16AccDataInterval=((trAccConfigData *)u8Buffer)->u16AccDataInterval;

               if(rAccConfigData.u8AccType==(tU8)ACC_TYPE_BST_BMI055)
               {
                  enAccModel=ACC_TYPE_BST_BMI055;
               }
               else
               {
                  enAccModel=ACC_TYPE_INVALID;
               }

               bAccConfigEn = TRUE;
               DEV_ACC_vTraceOut( TR_LEVEL_USER_4,"AccType=%d  Acc Int %d",
                                    rAccConfigData.u8AccType,rAccConfigData.u16AccDataInterval);
*/
               break;
            }

         case SEN_DISP_MSG_TYPE_TEMP_UPDATE://Temperature Message
            {
               rAccTemp.u16AccTemp=((trAccTemp *)u8Buffer)->u16AccTemp;

               bAccTempEn=TRUE;
               DEV_ACC_vTraceOut( TR_LEVEL_USER_4,"TEMP_UPDATE %d", rAccTemp.u16AccTemp);
               break;
            }
         case SEN_DISP_MSG_TYPE_SELF_TEST_RESULT:
            {
               if(((SensorSelfTestResult *)u8Buffer)->u8SelfTestResult == ACC_SELFTEST_RESULT_SUCCESS )
               {
                  u8AccSelfTestResult = ACC_SELF_TEST_PASSED;
               }
               else
               {
                  u8AccSelfTestResult = ACC_SELF_TEST_FAILED;
               }
               DEV_ACC_vTraceOut( TR_LEVEL_USER_4,"Acc Self Test result %d",((SensorSelfTestResult *)u8Buffer)->u8SelfTestResult);
               OSAL_s32EventPost( hAccEventSelfTest,
               ACC_C_EV_SELFTEST_FINISH,
               OSAL_EN_EVENTMASK_OR );
               break;
            }

         default:
            {
               DEV_ACC_vTraceOut( TR_LEVEL_ERROR,"vAccMainThread: Unknown Msg Type");
               break;
            }
         }
      }
   }

   if( OSAL_OK != ( OSAL_s32EventPost( hAccEventThdExt, 
                                       ACC_C_EV_THD_EXT,
                                       OSAL_EN_EVENTMASK_OR ) ) )
   {
      DEV_ACC_vTraceOut( TR_LEVEL_ERROR ,
                        "ACC_s32IOClose :Event Post Failed with error 0x%x ",
                        OSAL_u32ErrorCode() );
   }

   OSAL_vThreadExit();
}

/********************************************************************************
* FUNCTION     : DEV_ACC_vTraceOut

* PARAMETER    : u32Level - trace level
               pcFormatString - Trace string     
* RETURNVALUE  : tU32 error codes

* DESCRIPTION  : Trace out function for Acc.

* HISTORY      :
*--------------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|----------------------------------------------
*********************************************************************************/
tVoid DEV_ACC_vTraceOut(  TR_tenTraceLevel u32Level,const tChar *pcFormatString,... )
{
   if(LLD_bIsTraceActive((tU32)OSAL_C_TR_CLASS_DEV_ACC, (tU32)u32Level) != FALSE)
   {
      /*
      Parameter to hold the argument for a function, specified the format
      string in pcFormatString
      defined as:
      typedef char* va_list in stdarg.h
      */
      va_list argList;
      
      /*vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure*/
      tS32 s32Size ;
      
      /*Buffer to hold the string to trace out*/
      tS8 u8Buffer[MAX_TRACE_SIZE];
      
      /*Position in buffer from where the format string is to be concatenated*/
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /*Flush the String*/
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /*Initialize the argList pointer to the beginning of the variable arguement list*/
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*Collect the format String's content into the remaining part of the Buffer*/
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,sizeof(u8Buffer),pcFormatString,argList )))
      {
         return;
      }

      /*Trace out the Message to TTFis*/
      LLD_vTrace( (tU32)OSAL_C_TR_CLASS_DEV_ACC,(tU32)u32Level,u8Buffer,(tU32)s32Size );
      
      /*Performs the appropiate actions to facilitate a normal return by a
      function that has used the va_list object*/
      va_end(argList);
   }
}

#ifdef LOAD_SENSOR_SO
tS32 acc_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
   (tVoid)s32Id;
   (tVoid)szName;
   (tVoid)enAccess;
   (tVoid)pu32FD;
   (tVoid)app_id;

   return ACC_s32IOOpen();

}

tS32 acc_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   (tVoid)s32ID;
   (tVoid)u32FD;

   return ACC_s32IOClose(); 

}

tS32 acc_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tLong sArg)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   return ACC_s32IOControl(s32fun, sArg); 
}

tS32 acc_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   (tVoid)ret_size;
   return ACC_s32IORead(pBuffer, u32Size);
}
#endif

//EOF
