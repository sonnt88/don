/***************************************************************************
* FILE         : dev_abs.c
*
* DESCRIPTION  : This file implements proxy Abs driver module.
*                In GEN3 gyro, odometer, accelerometer and abs sensor
*                hardware is present on V850.Sensor data will be sent
*                from V850 to IMX via INC.It is the job of each 
*                proxy sensor drivers(gyro, abs & acc & abs) present on
*                IMX to collect the data populated by the dispatcher module,
*                in each of the corresponding ring buffers and provide
*                it to VDS_SENSOR application as and when it requests.
*
* AUTHOR(s)    : Sai Chowdary Samineni (RBEI/ECF5)
*
* HISTORY      :
*--------------------------------------------------------------------------
* Date         |       Version        | Author & comments
*--------------|----------------------|------------------------------------
* 7.July.2014  | Initial version: 1.0 | Sai Chowdary Samineni(RBEI/ECF5)
* -------------|----------------------|-------------------------------------
* 11.Dec.2015  | Version: 1.1         | Shivasharnappa Mothpalli (RBEI/ECF5)
               |                      | Removed Unused IOCTRLS (Fix for  CFG3-1622):
               |                      | OSAL_C_S32_IOCTRL_ABS_FLUSH,
               |                      | OSAL_C_S32_IOCTRL_ABS_GETCNT.
* ---------------------------------------------------------------------------
***************************************************************************/

/*************************************************************************
* Header file declaration
*------------------------------------------------------------------------*/

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"

#include "dev_abs.h"
#include "sensor_dispatcher.h"
#include "sensor_ring_buffer.h"

/************************************************************************
* Macro declaration (scope: Global)
*-----------------------------------------------------------------------*/

//Main thread attributes.
#ifdef ABS_THREAD_ACTIVE

#define ABS_C_STRING_MAIN_THREAD_NAME      ("ABS_THREAD_MAIN")
#define OSALCORE_C_U32_STACK_SIZE_ABS      (4096)
#define OSALCORE_C_U32_PRIORITY_ABS        (15)

#endif

#define ABS_COUNTER_RANGE                  (0x7FFF)

/* Timeout for configuration */
#define ABS_INIT_CONFIG_MSG_WAIT_TIME_MS ((OSAL_tMSecond)500)

/************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/

static OSAL_tMQueueHandle hldAbsMsgQueue = OSAL_NULL;
#ifdef ABS_THREAD_ACTIVE
/*!
* osal thread running flag, set on thread creation
*/
static tBool bAbsThreadRunning = FALSE;

/*!
* osal thread ID, returned on thread creation
*/
static OSAL_tThreadID s32AbsThreadID = OSAL_ERROR;
#endif
/*!
* Abs config Data, set on receiving configuration data 
*/
static trAbsConfigData *prAbsConfigData = OSAL_NULL;

/* the flag is set when config data is obtained */
static tBool bAbsConfigEn = FALSE;

/*Abs Open Flag*/ 
static tBool bisAbsOpen = FALSE; //TO be used in future

/*Configuration message buffer */
tU8 pu8AbsConfigBuffer[ABS_MSG_QUE_LENGTH]={ 0 }; 

/* Abs Counter */
/* This holds the previous Abs counter values received from V850 */
static tS32 s32AbsPrevRRCntr = 0;
static tS32 s32AbsPrevRLCntr = 0;
/* This holds the last updated Abs values to VD-Sensor */
static tU32 u32AbsRRCntr = 0;
static tU32 u32AbsRLCntr = 0;

/* First Abs value always needs to be Zero.
   This may not be the case in some projects. Hence we usually ignore first value
   and compute difference from second value onwards. */ 
static tBool bFirstVal = TRUE;

/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
#ifdef ABS_THREAD_ACTIVE
static tVoid vAbsMainThread(tVoid);
#endif
static tVoid DEV_ABS_vTraceOut(TR_tenTraceLevel u32Level,const tChar *pcFormatString,... );
static tVoid Abs_vUpdateCounter( OSAL_trAbsData* prIOAbsData );

/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/

/*************************************************************************
* FUNCTION     : ABS_s32IOOpen
*
* PARAMETER    : NONE
*
* RETURNVALUE  : SUCCESS - OSAL_E_NOERROR
*                FAILURE - OSAL_ERROR
*
* DESCRIPTION  : Opens the ABS device
*
* HISTORY      :
*-------------------------------------------------------------------------
* Date         |       Version        |       Author & comments
*--------------|----------------------|-----------------------------------
* 11.July.2014 | Initial version: 1.0 | Sai Chowdary Samineni (RBEI/ECF5)
* ------------------------------------------------------------------------
**************************************************************************/

tS32 ABS_s32IOOpen(tVoid)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   //Check whether device is already opened or not.
   if( bisAbsOpen == TRUE )
   {
      s32RetVal = OSAL_E_ALREADYOPENED;
      DEV_ABS_vTraceOut(TR_LEVEL_ERRORS,"IOOpen:ABS driver already opened");
   }
   //Initialize sensor dispatcher
   else if(OSAL_OK != s32SensorDiapatcherInit(SEN_DISP_ABS_ID))
   {
      s32RetVal = OSAL_ERROR;
      DEV_ABS_vTraceOut( TR_LEVEL_FATAL,"IOOpen:Sensor Dispatcher Init failed");
   }
   //Open sensor_ring_buffer
   else if(OSAL_OK != SenRingBuff_s32Open(SEN_DISP_ABS_ID))
   {
      s32RetVal = OSAL_ERROR;
      (tVoid)s32SensorDiapatcherDeInit( SEN_DISP_ABS_ID );
      DEV_ABS_vTraceOut( TR_LEVEL_FATAL,"IOOpen: Ring Buffer open failed" );
   }
   //Open the message queue
   else if( OSAL_OK != ( OSAL_s32MessageQueueOpen( (tCString)SEN_DISP_TO_ABS_MSGQUE_NAME,
                                                   (OSAL_tenAccess)OSAL_EN_READWRITE,
                                                   &hldAbsMsgQueue)) )
   {
      s32RetVal = OSAL_ERROR;
      (tVoid)s32SensorDiapatcherDeInit( SEN_DISP_ABS_ID );
      (tVoid)SenRingBuff_s32Close(SEN_DISP_ABS_ID);
      DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,
                         "IOOpen:MsgQ open failed with error = %u",
                         OSAL_u32ErrorCode());
   }
   else
   {
      if ( FALSE == bAbsConfigEn )
         {
            if ( (tS32)sizeof(trAbsConfigData) != OSAL_s32MessageQueueWait(
                                                   hldAbsMsgQueue,
                                                   pu8AbsConfigBuffer,
                                                   ABS_MSG_QUE_LENGTH,
                                                   OSAL_NULL,
                                                   ABS_INIT_CONFIG_MSG_WAIT_TIME_MS) )
            {
               DEV_ABS_vTraceOut( TR_LEVEL_FATAL, "Init:MQ wait fail err %lu", OSAL_u32ErrorCode());
               (tVoid)OSAL_s32MessageQueueClose( hldAbsMsgQueue);
               (tVoid)SenRingBuff_s32Close(SEN_DISP_ABS_ID );
               (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_ABS_ID);
            }
            else if ( SEN_DISP_MSG_TYPE_CONFIG != pu8AbsConfigBuffer[0] )
            {
               DEV_ABS_vTraceOut( TR_LEVEL_FATAL, "In-correct config Msg. ID %d", (tS32)pu8AbsConfigBuffer[0] );
               (tVoid)OSAL_s32MessageQueueClose( hldAbsMsgQueue);
               (tVoid)SenRingBuff_s32Close(SEN_DISP_ABS_ID );
               (tVoid)s32SensorDiapatcherDeInit(SEN_DISP_ABS_ID);
            }
            else
            {
               /* Store configuration */
               bAbsConfigEn = TRUE;
               prAbsConfigData = (trAbsConfigData*)(pu8AbsConfigBuffer);
               DEV_ABS_vTraceOut(TR_LEVEL_COMPONENT, "INIT: Abs Int:%d", prAbsConfigData->u16AbsDataInterval );
            }
         }
      
      if ( TRUE == bAbsConfigEn )
         {
            /* Start Abs Thread. 
               Currently we don't need the thread here. This shall be enabled again if need arises */
#ifdef ABS_THREAD_ACTIVE
            //Create and spawn thread to wait on message queue.
            OSAL_trThreadAttribute rThdAttr;
            rThdAttr.szName       = (tString)ABS_C_STRING_MAIN_THREAD_NAME;
            rThdAttr.s32StackSize = OSALCORE_C_U32_STACK_SIZE_ABS;
            rThdAttr.u32Priority  = OSALCORE_C_U32_PRIORITY_ABS;
            rThdAttr.pfEntry      = (OSAL_tpfThreadEntry)vAbsMainThread;
            rThdAttr.pvArg        = (tPVoid)OSAL_NULL;
            //Set the flag indicating the running status of the thread.
            bAbsThreadRunning = TRUE;

            s32AbsThreadID = OSAL_ThreadSpawn(&rThdAttr);
            if( s32AbsThreadID == OSAL_ERROR)
            {
               DEV_ABS_vTraceOut(TR_LEVEL_ERRORS,"ABS ThreadSpawn FAILED, Err = %u", OSAL_u32ErrorCode());
               bAbsThreadRunning = FALSE;
               (tVoid)s32SensorDiapatcherDeInit( SEN_DISP_ABS_ID );
               (tVoid)SenRingBuff_s32Close(SEN_DISP_ABS_ID);   
               (tVoid)OSAL_s32MessageQueueClose( hldAbsMsgQueue);
            }
            else
            {
#else
            /* If we dont have thread, close Message Queue. This will be of no use any more */
            if( OSAL_OK != ( OSAL_s32MessageQueueClose( hldAbsMsgQueue)))
            {
                /* error, Message Queue close failed */
                DEV_ABS_vTraceOut( TR_LEVEL_FATAL,"IOOpen: Close MessageQueue FAILED, Err = %u", OSAL_u32ErrorCode());
             }   

#endif
               DEV_ABS_vTraceOut(TR_LEVEL_USER_4, "Abs Init SUCCESS" );
               s32RetVal=OSAL_E_NOERROR;
               bisAbsOpen=TRUE;
#ifdef ABS_THREAD_ACTIVE
            }
#endif
         }
   }

   return s32RetVal;
}

#ifdef ABS_THREAD_ACTIVE
/*************************************************************************
* FUNCTION     : vAbsMainThread
*
* PARAMETER    : NONE
*
* RETURNVALUE  : SUCCESS - OSAL_E_NOERROR
*                FAILURE - OSAL_ERROR
*
* DESCRIPTION  :  Entry point for ABS thread spawned in @ref ABS_S32IOOpen.
*                 Is used to wait for Configuration,Error messages
*                 sent by the dispatcher via message queue.
* HISTORY      :
*--------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|-----------------------------------
* 11.July.2014 | Initial version: 1.0  | Sai Chowdary Samineni (RBEI/ECF5)
* -------------------------------------------------------------------------
**************************************************************************/

static tVoid vAbsMainThread(tVoid)
{

   trAbsConfigData *pConfigData = (trAbsConfigData*)(pu8AbsConfigBuffer);

   DEV_ABS_vTraceOut(TR_LEVEL_USER_1, "vAbsMainThread: ENTER");

   //To continuously wait for the message on the message queue.
   while( bAbsThreadRunning == TRUE )
   {

      DEV_ABS_vTraceOut(TR_LEVEL_USER_4, "vAbsMainThread: Waiting on MsgQ");
      //Wait on message queue for dispatcher messages.
      if( 0 == OSAL_s32MessageQueueWait( hldAbsMsgQueue,
                                         (tPU8)pu8AbsConfigBuffer,
                                         sizeof(pu8AbsConfigBuffer),
                                         OSAL_NULL,
                                         (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER) )
      {
         DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,
                            "vAbsMainThread: Message Queue Wait failed with error code =%u",
                            OSAL_u32ErrorCode());
      }
      //Got some message.
      //Check whether thread state is still running or its state changed to shut down.
      else if( bAbsThreadRunning == FALSE )
      {
         DEV_ABS_vTraceOut(TR_LEVEL_USER_4, "vAbsMainThread: Closing the Message Queue");
         //Close Message Queue
         if( OSAL_OK != ( OSAL_s32MessageQueueClose( hldAbsMsgQueue)))
         {
            //Error, Message Queue close failed
            DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,
                               "vAbsMainThread: Close MessageQueue FAILED, Err = %u",
                               OSAL_u32ErrorCode());
         }
         else
         {
            hldAbsMsgQueue = OSAL_NULL;
         }
      }
      //If thread is still running means received some config message.
      else
      {
         DEV_ABS_vTraceOut(TR_LEVEL_USER_4, "vAbsMainThread: Received CONFIG message");
         if (pConfigData->u8MsgType != SEN_DISP_MSG_TYPE_CONFIG)
         {
            DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,"vAbsMainThread:Received unknown message");
         }
         else
         {
            bAbsConfigEn = TRUE;
            prAbsConfigData = (trAbsConfigData*)(pu8AbsConfigBuffer);

            DEV_ABS_vTraceOut( TR_LEVEL_USER_3, "vAbsMainThread:u8MsgType =%d", prAbsConfigData->u8MsgType);
            DEV_ABS_vTraceOut( TR_LEVEL_USER_3, "vAbsMainThread:AbsDataInterval =%d", prAbsConfigData->u16AbsDataInterval);
         }
      }

   }//end of while

   DEV_ABS_vTraceOut( TR_LEVEL_USER_4, "vAbsMainThread: EXIT");
   OSAL_vThreadExit();
}
#endif

/*************************************************************************
* FUNCTION     : ABS_s32IOClose
*
* PARAMETER    : NONE
*
* RETURNVALUE  : SUCCESS - OSAL_E_NOERROR
*                FAILURE - OSAL_ERROR
*
* DESCRIPTION  : Closes ABS device.
*
* HISTORY      :
*--------------------------------------------------------------------------
* Date         |       Version        | Author & comments
*--------------------------------------------------------------------------
* 11.July.2014 | Initial version: 1.0 | Sai Chowdary Samineni (RBEI/ECF5)
* -------------------------------------------------------------------------
***************************************************************************/

tS32 ABS_s32IOClose(tVoid)
{

   tS32 s32RetVal = OSAL_E_NOERROR;
#ifdef ABS_THREAD_ACTIVE
   //Used to stop thread in conjunction with the event post below
   tU8 myTerminateMsg = 0;
#endif

   DEV_ABS_vTraceOut(TR_LEVEL_USER_4,"IOClose:ENTER");

   if( bisAbsOpen == TRUE )
   {
#ifdef ABS_THREAD_ACTIVE
      bAbsThreadRunning = FALSE;
      //Post message to ABS thread to terminate.
      if( OSAL_OK != OSAL_s32MessageQueuePost( hldAbsMsgQueue,
                                               (tCU8*)&myTerminateMsg,
                                               sizeof(tU8),
                                               OSAL_C_U32_MQUEUE_PRIORITY_HIGHEST))
      {
         s32RetVal = OSAL_ERROR;
         DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,
                            "IOClose:MsgQ post failed with error = %u",
                            OSAL_u32ErrorCode());
      }
#endif

      //Close ring buffer.
      if( OSAL_OK != SenRingBuff_s32Close(SEN_DISP_ABS_ID))
      {
         //Error, Message Queue close failed.
         s32RetVal = OSAL_ERROR;
         DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,"IOClose:Ring buffer close failed");
      }

      //Deinitialize sensor dispatcher.
      if(OSAL_OK != s32SensorDiapatcherDeInit( SEN_DISP_ABS_ID ))
      {
         s32RetVal = OSAL_ERROR;
         DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,"IOClose:Sensor dispatcher DeInit failed");
      }
      else
      {
         s32RetVal = OSAL_E_NOERROR;
         bisAbsOpen= FALSE;
         DEV_ABS_vTraceOut(TR_LEVEL_USER_4,"IOClose: Device closed successfully");   

      }
   }
   else
   {
      s32RetVal = OSAL_ERROR;
      DEV_ABS_vTraceOut(TR_LEVEL_ERRORS,"IOClose: Device is NOT opened");
   }

   DEV_ABS_vTraceOut(TR_LEVEL_USER_4,"IOClose: EXIT");

   return s32RetVal;
}

/*************************************************************************
* FUNCTION     : ABS_s32IORead 

* PARAMETER    : pBuffer, pointer to struct @ref OSAL_trAbsData
*                where the read data must be copied
*                                                      
* RETURNVALUE  :  SUCCESS - Number of bytes read
*                 FAILURE - OSAL_ERROR
*
* DESCRIPTION  :  Reads the Abs data
*
* HISTORY      :
*-------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|----------------------------------
* 11.July.2014 | Initial version: 1.0  | Sai Chowdary Samineni (RBEI/ECF5)
* ------------------------------------------------------------------------
***************************************************************************/

tS32 ABS_s32IORead( tPS8 pBuffer, tU32 u32maxbytes)
{

   tU32  u32NumberOfEntries = u32maxbytes / sizeof( OSAL_trAbsData );
   tS32  s32RetVal = OSAL_E_NOERROR;
   OSAL_trAbsData* prIOAbsData  = OSAL_NULL;
   tS32 s32Cnt = 0;

   DEV_ABS_vTraceOut( TR_LEVEL_USER_4,
                      "IORead:Requested records = %d",
                      u32NumberOfEntries);

   if( OSAL_NULL == pBuffer )
   {
       s32RetVal = OSAL_E_INVALIDVALUE;
       DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,"IORead:Got NULL buffer");
   }
   else
   {
      prIOAbsData = (OSAL_trAbsData*)((tPVoid)pBuffer);

      s32RetVal = SenRingBuff_s32Read( SEN_DISP_ABS_ID,(tPVoid)prIOAbsData,u32NumberOfEntries );

      if((tS32)OSAL_ERROR == s32RetVal)
      {
         DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,"IORead:Read from ring buffer failed" ); 
#ifdef OSAL_GEN3_SIM
         OSAL_s32ThreadWait(1000);
#endif
      }
      else if((tS32)OSAL_E_TIMEOUT == s32RetVal)
      {
         DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,
                            "IORead:Read from ring buffer failed due to time out" ); 
      }
      else
      {
         DEV_ABS_vTraceOut(TR_LEVEL_USER_4 ,"%d records read from ring buffer",s32RetVal);

         for (s32Cnt =0; s32Cnt < s32RetVal ; s32Cnt++)
         {
            Abs_vUpdateCounter( (prIOAbsData + s32Cnt) );
         }

         s32RetVal = (s32RetVal * (tS32)sizeof(OSAL_trAbsData));
      }
   }

   //Return no. of bytes written into Read buffer
   return s32RetVal;
}

/*************************************************************************
* FUNCTION     : Abs_vUpdateCounter

* PARAMETER    : prIOAbsData:pointer to Abs data for which counter 
*                            has to be updated.
*
* RETURNVALUE  : None
*
* DESCRIPTION  : Updates the ABS Counters
*
* HISTORY      :
*---------------------------------------------------------------------------
* Date         |       Version          | Author & comments
*--------------|------------------------|-----------------------------------
* 11.July.2014 | Initial version: 1.0   | Sai Chowdary Samineni (RBEI/ECF5)
* --------------------------------------------------------------------------
****************************************************************************/

static tVoid Abs_vUpdateCounter( OSAL_trAbsData* prIOAbsData )
{

   tS32 s32TicksRRDiff = 0;
   tS32 s32TicksRLDiff = 0;
   tS32 s32CurrentRRCntr = 0;
   tS32 s32CurrentRLCntr = 0;
   static tU16 u16ErrorCount = 0;

   if(OSAL_NULL == prIOAbsData)
   {
      DEV_ABS_vTraceOut(TR_LEVEL_FATAL,"Got NULL pointer to vUpdateCounter");
   }
   //Check if this is a time out record.
   else if( 0 == prIOAbsData->u16ErrorCounter )
   {
      if((OSAL_C_U8_ABS_STATUS_DATA_INVALID != prIOAbsData->u8StatusRearRight) &&
         (OSAL_C_U8_ABS_STATUS_DATA_INVALID != prIOAbsData->u8StatusRearLeft))
      {
          s32CurrentRRCntr = (tS32)prIOAbsData->u32CounterRearRight;
          s32CurrentRLCntr = (tS32)prIOAbsData->u32CounterRearLeft;
          //Don't pass the first value to application but store it for further calculations.
          if( FALSE == bFirstVal )
          {
             //If direction of wheel (RearRight/RearLeft) is unknown.
             if(( prIOAbsData->u8DirectionRearRight == OSAL_C_U8_ABS_DIR_UNKNOWN ) ||
                ( prIOAbsData->u8DirectionRearLeft  == OSAL_C_U8_ABS_DIR_UNKNOWN ))
             {
                bFirstVal = TRUE;
                u16ErrorCount++;
                prIOAbsData->u8StatusRearRight = OSAL_C_U8_ABS_STATUS_NO_INFO;
                prIOAbsData->u8StatusRearLeft = OSAL_C_U8_ABS_STATUS_NO_INFO;
                DEV_ABS_vTraceOut(TR_LEVEL_ERROR, "ABS direction received : UNKNOWN");
             }
             else
             {
                //Check the ticks counted from previous signal.
                s32TicksRRDiff = s32CurrentRRCntr - s32AbsPrevRRCntr;
                s32TicksRLDiff = s32CurrentRLCntr - s32AbsPrevRLCntr;

                u16ErrorCount = 0;

                //Corrections for overflows
                if (s32TicksRRDiff < 0)
                {
                   s32TicksRRDiff += ABS_COUNTER_RANGE + 1;
                }
                if (s32TicksRLDiff < 0)
                {
                   s32TicksRLDiff += ABS_COUNTER_RANGE + 1;
                }

                //If directions is forward add the ticks to previous value.
                if ( OSAL_C_U8_ABS_DIR_FORWARD == prIOAbsData->u8DirectionRearRight )
                {
                   DEV_ABS_vTraceOut(TR_LEVEL_USER_4, "ABS Right direction received : FORWARD");
                   u32AbsRRCntr += (tU32)s32TicksRRDiff;
                } 
                //If direction is reverse, subtract the ticks from previous value.
                else if ( OSAL_C_U8_ABS_DIR_REVERSE == prIOAbsData->u8DirectionRearRight )
                {
                   DEV_ABS_vTraceOut(TR_LEVEL_USER_4, "ABS Right direction received : REVERSE");
                   u32AbsRRCntr -= (tU32)s32TicksRRDiff;
                }

                //If directions is forward add the ticks to previous value
                if ( OSAL_C_U8_ABS_DIR_FORWARD == prIOAbsData->u8DirectionRearLeft )
                {
                   DEV_ABS_vTraceOut(TR_LEVEL_USER_4, "ABS Left direction received : FORWARD");
                   u32AbsRLCntr += (tU32)s32TicksRLDiff;
                }
                //If direction is reverse, subtract the ticks from previous value.
                else if ( OSAL_C_U8_ABS_DIR_REVERSE == prIOAbsData->u8DirectionRearLeft )
                {
                   DEV_ABS_vTraceOut(TR_LEVEL_USER_4, "ABS Left direction received : REVERSE");
                   u32AbsRLCntr -= (tU32)s32TicksRLDiff;
                }
             }
          }
          else
          {
             /* when a valid direction (FORWARD or REVERSE) is received, bFirstValue is set to FALSE and normal operation is resumed from the next cycle*/
             if(( prIOAbsData->u8DirectionRearRight != OSAL_C_U8_ABS_DIR_UNKNOWN ) && 
                ( prIOAbsData->u8DirectionRearLeft  != OSAL_C_U8_ABS_DIR_UNKNOWN ))
             {
               bFirstVal = FALSE;
             }
             /* If repeated UNKNOWN direction values are received, the error counter is updated here */
             else
             {
                DEV_ABS_vTraceOut(TR_LEVEL_ERROR, "ABS direction received : UNKNOWN");
                u16ErrorCount++;
                prIOAbsData->u8StatusRearRight = OSAL_C_U8_ABS_STATUS_NO_INFO;
                prIOAbsData->u8StatusRearLeft = OSAL_C_U8_ABS_STATUS_NO_INFO;
             }
          }
          s32AbsPrevRRCntr = s32CurrentRRCntr;
          s32AbsPrevRLCntr = s32CurrentRLCntr;
       }
       //Increment error counter if status of ABS data is Invalid.
       else
       {
          bFirstVal = TRUE;
          DEV_ABS_vTraceOut(TR_LEVEL_ERROR, "ABS Data Received is INVALID!");
          u16ErrorCount++;
          prIOAbsData->u8StatusRearRight = OSAL_C_U8_ABS_STATUS_DATA_INVALID;
          prIOAbsData->u8StatusRearLeft = OSAL_C_U8_ABS_STATUS_DATA_INVALID;
       }

       prIOAbsData->u32CounterRearRight = u32AbsRRCntr;
       prIOAbsData->u32CounterRearLeft  = u32AbsRLCntr;
       prIOAbsData->u16ErrorCounter = u16ErrorCount;

       DEV_ABS_vTraceOut( TR_LEVEL_USER_4,
                         "After updating counter:Ts %u,CRR %u,SRR %u,DRR %u,CRL %u,SRL %u,DRL %u, ERRCNTR %u",
                         prIOAbsData->u32TimeStamp,
                         prIOAbsData->u32CounterRearRight,prIOAbsData->u8StatusRearRight,prIOAbsData->u8DirectionRearRight,
                         prIOAbsData->u32CounterRearLeft, prIOAbsData->u8StatusRearLeft,prIOAbsData->u8DirectionRearLeft,
                         prIOAbsData->u16ErrorCounter );
   }   
   else
   {
      //Just ignore time out records.
      //Set status of wheels to data Internal Error.
      ++u16ErrorCount;
      prIOAbsData->u16ErrorCounter = u16ErrorCount;
      prIOAbsData->u32CounterRearRight = u32AbsRRCntr;
       prIOAbsData->u32CounterRearLeft  = u32AbsRLCntr;
      DEV_ABS_vTraceOut(TR_LEVEL_USER_4,"Timeout/Error record discarded: %d",prIOAbsData->u16ErrorCounter);
   }
}

/****************************************************************************
* FUNCTION     : ABS_s32IOControl
*
* PARAMETER    : s32fun - Function identificator
*                sArg - Argument to be passed to function
*
* RETURNVALUE  : SUCCESS - OSAL_E_NOERROR
*                FAILURE - OSAL_ERROR
*
* DESCRIPTION  : Abs driver control functions
*
* HISTORY      :
*----------------------------------------------------------------------------
* Date         |       Version         | Author & comments
*--------------|-----------------------|-------------------------------------
* 11.July.2014 | Initial version: 1.0  | Sai Chowdary Samineni (RBEI/ECF5)
* -------------|-----------------------|-------------------------------------
* 11.Dec.2015  | Version: 1.1          | Shivasharnappa Mothpalli (RBEI/ECF5)
               |                       | Removed Unused IOCTRLS(Fix for  CFG3-1622):
               |                       | OSAL_C_S32_IOCTRL_ABS_FLUSH,
               |                       | OSAL_C_S32_IOCTRL_ABS_GETCNT.
* ---------------------------------------------------------------------------
*****************************************************************************/

tS32 ABS_s32IOControl(tS32 s32fun, tLong sArg)
{ 
   tS32 s32RetVal = OSAL_E_NOERROR;
   tPS32 ps32Argument = (tPS32) sArg;

   DEV_ABS_vTraceOut( TR_LEVEL_USER_4,"ABS_s32IOControl:IOControl ENTERED" );

   switch( s32fun )
   {
      case OSAL_C_S32_IOCTRL_ABS_GET_SAMPLING_INTERVAL:
      {

         if(bAbsConfigEn)
         {
            if( OSAL_NULL != ps32Argument )
            {
               if( OSAL_NULL != prAbsConfigData )
               {
                  *ps32Argument = (tS32)( prAbsConfigData -> u16AbsDataInterval ) * 1000000;
               }
               else
               {
                  s32RetVal =  OSAL_ERROR;
               }
            }
            else
            {
               s32RetVal =  OSAL_E_INVALIDVALUE;
            }
         }
         else
         {
            s32RetVal =OSAL_E_INPROGRESS;
         }
      }
      break;

      default:
      {
         s32RetVal = OSAL_E_NOTSUPPORTED;
         DEV_ABS_vTraceOut( TR_LEVEL_ERRORS,"ABS_s32IOControl: IOControl Not supported");
      }
      break;
   }

   DEV_ABS_vTraceOut( TR_LEVEL_USER_4,"ABS_s32IOControl: IOControl EXIT");
   return s32RetVal;
}

/**************************************************************************
* FUNCTION     : DEV_ABS_vTraceOut
*
* PARAMETER    : u32Level - Trace level
*                pcFormatString - Trace string
*
* RETURNVALUE  : None
*
* DESCRIPTION  : Trace out function for ABS.
*
* HISTORY      :
*---------------------------------------------------------------------------
* Date          |       Version         | Author & comments
*---------------|-----------------------|-----------------------------------
* 11.July.2014  | Initial version: 1.0  | Sai Chowdary Samineni (RBEI/ECF5)
* --------------------------------------------------------------------------
****************************************************************************/

static tVoid DEV_ABS_vTraceOut(  TR_tenTraceLevel u32Level,const tChar *pcFormatString,... )
{
   if( FALSE != LLD_bIsTraceActive((tU32)OSAL_C_TR_CLASS_DEV_ABS, (tU32)u32Level) )
   {
      /*
      Parameter to hold the argument for a function,
      specified the format string in pcFormatString
      defined as: typedef char* va_list in stdarg.h
      */

      /*
      vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure
      */
      va_list argList;
      tS32 s32Size;

      //Buffer to hold the string to trace out
      tS8 u8Buffer[MAX_TRACE_SIZE] = { 0 };

      /*
      Position in buffer from where the format string is to be
      concatenated
      */
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /* Flush the String */
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /* Copy the String to indicate the trace is from the RTC device */

      /*
      Initialize the argList pointer to the beginning of the variable
      arguement list
      */
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
      Collect the format String's content into the remaining part of
      the Buffer
      */
      if( 0 > ( s32Size = vsnprintf( (tString)ps8Buffer,
                                     sizeof(u8Buffer),
                                     pcFormatString,
                                     argList ) ) )
      {
         return;
      }

      /* Trace out the Message to TTFis */
      LLD_vTrace( (tU32)OSAL_C_TR_CLASS_DEV_ABS, (tU32)u32Level, u8Buffer, (tU32)s32Size );   /* Send string to Trace*/

      /*
      Performs the appropiate actions to facilitate a normal return by a
      function that has used the va_list object
      */
      va_end(argList);
   }
}

#ifdef LOAD_SENSOR_SO
tS32 abs_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
   (tVoid)s32Id;
   (tVoid)szName;
   (tVoid)enAccess;
   (tVoid)pu32FD;
   (tVoid)app_id;
#ifdef OSAL_GEN3_SIM
   return OSAL_E_NOERROR;
#else
   return ABS_s32IOOpen();
#endif
}

tS32 abs_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
#ifdef OSAL_GEN3_SIM
   return OSAL_E_NOERROR;
#else
   return ABS_s32IOClose(); 
#endif
}

tS32 abs_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tLong sArg)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   return ABS_s32IOControl(s32fun, sArg); 
}

tS32 abs_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   (tVoid)ret_size;
   return ABS_s32IORead(pBuffer, u32Size);
}
#endif

/*End of file*/

