/******************************************************************************
* FILE				: Dev_abs.h
*				  
* DESCRIPTION	: This is the header file for Dev_abs.c
*				  
* AUTHOR(s) 		: Sai Chowdary Samineni (RBEI/ECF5)
*
* HISTORY           :
*------------------------------------------------------------------------
* Date      		|       Version     		  	| Author & comments
*--------------|--------------------|-------------------------------------
* 11.JULY.2014   | Initial version: 1.0      | Sai Chowdary Samineni (RBEI/ECF5)
* -----------------------------------------------------------------------
********************************************************************************/

#ifndef DEV_ABS_HEADER
#define DEV_ABS_HEADER

tS32 ABS_s32IOOpen(tVoid);
tS32 ABS_s32IOClose(tVoid);
tS32 ABS_s32IOControl(tS32 s32fun, tLong sArg);
tS32 ABS_s32IORead( tPS8 pBuffer, tU32 u32maxbytes);

#endif

/*End of file*/