/******************************************************************************
* FILE         : adc.c
*
* SW-COMPONENT : OSAL IO
*
* DESCRIPTION  : This file implements the application interfaces to access Sensor ADC
*                device for V850 hardware from IMX side.
*
* AUTHOR(s)    :  Basavaraj Nagar (RBEI/ECG4)
*
* HISTORY      :
*-----------------------------------------------------------------------------

-------------------------------------------------------------------------------------
*14,March,  2013  | Version0.00      | Basavaraj Nagar(RBEI/ECG4)
                  | Written for Linux
*10,June,  2013   | Version1.00      | Basavaraj Nagar(RBEI/ECG4)
                  | Event synchronization added,broken pipe fixed
*09,February,2014   | Version1.1      | Basavaraj Nagar(RBEI/ECG4)
                    |Lint corrections made
*21,July,2014     | Version1.2       | Arun Prabhu(RBEI/ECF5)
                  |Changes made so that functions return an OSAL error code
                   or OSAL_E_NOERROR and not OSAL_ERROR
				   
*16,0ct,2014      | Version1.3       |Nikhil Ravindran(RBEI/ECF5)
                  | Lint Fix
*07,Sept,2015     | Version1.4       | Arun Prabhu(RBEI/ECF5)
                  | Changes made to account for the removal of adc_inc_ioctl.h
                  | and bosch_ioctl.h from the ai_osal_linux vob and 
                  | use the exported kernel header files instead.
*******************************************************************************/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include <sys/ioctl.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
#include "adc.h"
#ifndef _LINUXX86_64_
  #include <linux/adc_inc_ioctl.h>
#endif
/*adc.h has u16 and u8 datatypes which are used by adc_inc_ioctl.h so,
include adc.h before adc_inc_ioctl.h to prevent build issues*/

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define DEV_ADC_SEM_NAME "DEV_ADC_SEM"
#define DEV_ADC_ADD_EVENT_NAME "fd_addition_event"
#define DEV_ADC_REM_EVENT_NAME "fd_removal_event"
#define DEV_ADC_STACK_SIZE 2048
#define OSALTHREAD_C_U32_PRIORITY_MID 60
/*TR_CLASS_DEV_SADC is redefined to TR_CLASS_DEV_ADC for consistency*/
#define OSAL_C_TR_CLASS_DEV_ADC OSAL_C_TR_CLASS_DEV_SADC
#define Removal_Event        (0x001)
#define Addition_Event       (0x008)

/*the poll file descriptors for all  adc channels available in Gen3*/
struct pollfd *fds=OSAL_NULL;

/*ThreadID allocated on thread creation*/
static OSAL_tThreadID CallbkThrID = 0;

/*the count of currently open poll files because 
it is used as index for pollFdSet while storing fds*/
static  nfds_t nfds=0;

/* the read/write ends of the pipe used to unblock the callback thread */
static tInt UnblkPipeFdSet[2] = {0};

/*Semaphore handle*/
static OSAL_tSemHandle SemHndl = OSAL_C_INVALID_HANDLE;

/*tracks the number of open callbacks*/
static tU8 CallBakLkpCntr = 0;

/*Event handles for Syncing*/
static OSAL_tEventHandle RemHdl = 0;
static OSAL_tEventHandle AddHdl = 0;

/*event differentiation flags*/
static tBool fd_add=0;
static tBool fd_rem=0;
static tBool thread_exit=0;

/************************************************************************
|function prototypes (scope: local)
|-----------------------------------------------------------------------*/
static tS32 callback_remove(intptr_t u32FD);
static tS32 reconfigure(intptr_t fd);
static tS32 ADC_Fork(uintptr_t u32FD);
static tS32 dev_ADC_SetThreshold(uintptr_t u32FD, intptr_t s32Arg);
static tS32 dev_ADC_SetCallback(uintptr_t u32FD, intptr_t s32Arg);
static tVoid dev_ADC_CallbackThread();
static tU32 dev_ADC_open(tCString id,uintptr_t* pFd);
static tS32 dev_ADC_GetResolution(uintptr_t u32FD, intptr_t s32Arg);
static tBool dev_ADC_ExistsPath(const tPChar path);

static tVoid DEV_ADC_vTraceInfo(TR_tenTraceLevel enTraceLevel,enDevADCTraceFunction enFunction,
                                tPCChar copchDescription,tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,tU32 u32Par4);

static tVoid DEV_ADC_vTraceString(TR_tenTraceLevel enTraceLevel,
                                  enDevADCTraceFunction enFunction,
                                  tPCChar copchDescription);
tU32 u32ConvertErrorCore(tS32 s32ErrCd);

/************************************************************************/
/*!
*\fn       tVoid DEV_ADC_vTraceInfo(TR_tenTraceLevel enTraceLevel,
enDevADCTraceFunction enFunction,
tPCChar copchDescription,
tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
tU32 u32Par4)
*\brief    Function to print ADC trace messges on console IO
*
*          To print trace messages on console IO ADC driver call this
*          function at nessessary places.
*
*\param    TR_tenTraceLevel [IN] - To opt trace level
*\param    enFunction [IN] - Current ADC Funtion name
*\param    copchDescription [IN] - char string which prints on console IO
*\param    u32Par1 [OUT] - Output value from the ADC funciton
*\param    u32Par1 [OUT] - Output value from the ADC funciton
*\param    u32Par1 [OUT] - Output value from the ADC funciton
*\param    u32Par1 [OUT] - Output value from the ADC funciton
*
*\return  No return value. Funciton type is void.
*
*\par History:
*25.12.2012 - Ported to Linux - Basavaraj Nagar (RBEI/ECG4)
**********************************************************************/
static tVoid DEV_ADC_vTraceInfo(TR_tenTraceLevel enTraceLevel,
                                enDevADCTraceFunction enFunction,
                                tPCChar copchDescription,
                                tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,tU32 u32Par4)
{
    tChar ch;
    tInt i;
    tU32 u32ThreadID;
    tU8 u8StringLength;

    tU8 au8Buf[3 * sizeof(tU8) + 16 * sizeof(tChar) + 5 * sizeof(tU32)];

    /* Trace Level enabled ? */
    if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_ADC,(tU32)enTraceLevel))
    {
        /* calculate the given string length */
        u8StringLength = (tU8)OSAL_u32StringLength(copchDescription);

        /*string lentgh greater than reserved space ? */
        if (u8StringLength > 16)
        {
            /* set string length to maximum allowed value */
            u8StringLength = 16;
        }
        /* fill trace buffer with desired values */
        u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
        OSAL_M_INSERT_T8(&au8Buf[0], (tU8) ADC_TRC_FN_INFO);
        OSAL_M_INSERT_T32(&au8Buf[1], u32ThreadID);
        OSAL_M_INSERT_T8(&au8Buf[5], (tU8) enFunction);

        /* copy given string into the trace buffer */
        for (i = 0; i < u8StringLength; ++i)
        {
            ch = copchDescription[i];
            OSAL_M_INSERT_T8(&au8Buf[6+i], (tU8) ch);
            if (ch == '\0') break;
        }
        /* set string end \0 */
        OSAL_M_INSERT_T8(&au8Buf[6+i], (tU8) '\0');
        /* copy par1 to par4 into the trace buffer */
        OSAL_M_INSERT_T32(&au8Buf[23], u32Par1);
        OSAL_M_INSERT_T32(&au8Buf[27], u32Par2);
        OSAL_M_INSERT_T32(&au8Buf[31], u32Par3);
        OSAL_M_INSERT_T32(&au8Buf[35], u32Par4);

        /* send adc trace to IO-console */
        LLD_vTrace((tU32)OSAL_C_TR_CLASS_DEV_ADC, (tU32)enTraceLevel,
            au8Buf, sizeof(au8Buf));
    }
}

/************************************************************************/
/*!
*\fn       tVoid DEV_ADC_vTraceString(TR_tenTraceLevel enTraceLevel,
enDevAdcTraceFunction enFunction,
tPCChar copchDescription)

*\brief    Function to print ADC string trace messages on console IO
*
*          To print trace messages on console IO ADC driver call this
*          function at nessessary places.
*
*\param    TR_tenTraceLevel [IN] - To opt trace level
*\param    enFunction [IN] - Current ADC Funtion name
*\param    copchDescription [IN] - char string which prints on console IO
*
*\return  No return value. Function type is void.
*
*\par History:
*09.08.2012 - Initial version for Linux - Basavaraj Nagar (RBEI/ECG4)
**********************************************************************/
static tVoid DEV_ADC_vTraceString(TR_tenTraceLevel enTraceLevel,
                                  enDevADCTraceFunction enFunction,
                                  tPCChar copchDescription)
{
    tU32 u32ThreadID;
    tU8 u8StringLength;

    tU8 au8TraceBuf[3 * sizeof(tU8) + 80 * sizeof(tChar) + 1 * sizeof(tU32)];

    /* Trace Level enabled ? */
    if(LLD_bIsTraceActive((tU32)OSAL_C_TR_CLASS_DEV_ADC,(tU32)enTraceLevel))
    {
        /* calculate the given string length */
        u8StringLength = (tU8)OSAL_u32StringLength(copchDescription);

        /*string lentgh greater than reserved space ? */
        if (u8StringLength > 80)
        {
            /* set string length to maximum allowed value */
            u8StringLength = 80;
        }
        /* fill trace buffer with desired values */
        u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
        OSAL_M_INSERT_T8(&au8TraceBuf[0], (tU8) ADC_TRC_FN_STRING);
        OSAL_M_INSERT_T32(&au8TraceBuf[1], u32ThreadID);
        OSAL_M_INSERT_T8(&au8TraceBuf[5], (tU8) enFunction);

        /* copy given string into the trace buffer */
        (tVoid)OSAL_szStringNCopy((tString) &au8TraceBuf[6], copchDescription,
            u8StringLength);
        /* set string end \0 */
        au8TraceBuf[u8StringLength+6] = 0;
        /* send adc trace to IO-console */
        LLD_vTrace((tU32)OSAL_C_TR_CLASS_DEV_ADC,(tU32) enTraceLevel,
            au8TraceBuf, u8StringLength+7);
    }
}

/************************************************************************/
/*! 
*\fn       tS32 DEV_ADC_s32IODeviceInit()
*\brief    Cross-process/thread initialization of DRV ADC
*
*\return   OSAL error code
*
*\par History:
* 09.08.2012 - Initial version for Linux - Basavaraj Nagar (RBEI/ECG4)
**********************************************************************/
tS32 DEV_ADC_s32IODeviceInit()
{
    /* create semaphore */
    if (OSAL_ERROR == OSAL_s32SemaphoreCreate(DEV_ADC_SEM_NAME, &SemHndl, 1))
    {
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    /* Create Event for Syncing*/

    if( OSAL_ERROR == OSAL_s32EventCreate(DEV_ADC_ADD_EVENT_NAME,&RemHdl ) )
    {
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    if( OSAL_ERROR == OSAL_s32EventCreate(DEV_ADC_REM_EVENT_NAME,&AddHdl ) )
    {
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    return (tS32)OSAL_E_NOERROR;
}

/************************************************************************/
/*!
*\fn       tS32 DEV_ADC_s32IODeviceDeinit()
*\brief    Cross-process/thread deinitialization of DRV ADC
*
*\return   OSAL error code
*
*\par History:
* 09.08.2012 - Initial version for Linux - Basavaraj Nagar(RBEI/ECG4)
**********************************************************************/
tS32 DEV_ADC_s32IODeviceDeinit()
{
    /* close semaphore */

    if (OSAL_ERROR == OSAL_s32SemaphoreClose(SemHndl))
    {
        NORMAL_M_ASSERT_ALWAYS();
        OSAL_s32SemaphoreDelete(DEV_ADC_SEM_NAME);
        return (tS32)OSAL_u32ErrorCode();
    }
    /* delete semaphore */

    if (OSAL_ERROR == OSAL_s32SemaphoreDelete(DEV_ADC_SEM_NAME))
    {
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    SemHndl = OSAL_C_INVALID_HANDLE;

    /* Close Event */
    if( OSAL_ERROR == OSAL_s32EventClose( AddHdl ) )
    {
        NORMAL_M_ASSERT_ALWAYS();
        OSAL_s32EventDelete( DEV_ADC_ADD_EVENT_NAME );
        return (tS32)OSAL_u32ErrorCode();
    }
    /* Close Event */
    if( OSAL_ERROR == OSAL_s32EventClose( RemHdl ) )
    {
        NORMAL_M_ASSERT_ALWAYS();
        OSAL_s32EventDelete( DEV_ADC_REM_EVENT_NAME );
        return (tS32)OSAL_u32ErrorCode();
    }
    /*Delete the Event */
    if( OSAL_ERROR == OSAL_s32EventDelete( DEV_ADC_ADD_EVENT_NAME ) )
    {
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    /*Delete the Event */
    if( OSAL_ERROR == OSAL_s32EventDelete( DEV_ADC_REM_EVENT_NAME ) )
    {
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    RemHdl=OSAL_C_INVALID_HANDLE;
    AddHdl=OSAL_C_INVALID_HANDLE;

    return OSAL_E_NOERROR;
}

/*****************************************************************************
* FUNCTION:     ADC_s32IOOpen()
* PARAMETER:    Channel ID
DevicePath - path of device file to be opened
u32FD - Device descriptor 
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Opens the requested ADC( sensor ADC) Sub unit. Also supports 
*               Multiple opens of the ADC channel
* HISTORY:
.......................................................................................................................................				 
25/12/12|Basavaraj Nagar| Added for Linux
******************************************************************************/
tS32 ADC_s32IOOpen(tCString szName,tS32 s32ID, uintptr_t *pu32FD)     
{
#ifndef _LINUXX86_64_
    tU32 RetVal ;

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOOpen,"enter into open function of my device",(tU32)getpid(),(tU32)s32ID,0,0);
#endif

    /* get semaphore */
    if (OSAL_ERROR == OSAL_s32SemaphoreOpen(DEV_ADC_SEM_NAME, &SemHndl))
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen,
            "smphr open failed", 0, 0, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    RetVal=dev_ADC_open(szName,pu32FD);

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOOpen,"file_descriptor= ",*pu32FD,RetVal,0,0);
#endif    

#ifdef ADC_VERBOSE_TRACE   
    DEV_ADC_vTraceString(TR_LEVEL_USER_4, enDEV_ADC_IOOpen,"SADC exit");
#endif

    return ((tS32)RetVal);
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(szName);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32FD);
    return OSAL_E_NOERROR;
#endif
}

/*****************************************************************************
* FUNCTION:     reconfigure()
* PARAMETER:    u32FD - File descriptor to be closed
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Reconfigures the pollfds accordingly and unblocks the poll.
* HISTORY:    
05/03/2013|Basavaraj Nagar(RBEI/ECG4)--written for Linux

******************************************************************************/
static tS32 reconfigure(intptr_t u32FD)
{ 
    tS32 s32ret=(tS32)OSAL_E_NOERROR;
    tU32 u32num;
    tChar buf=0;
    OSAL_tEventMask u32EventResultMask = 0;

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
        "reconfig enter",u32FD,nfds, 0, 0);
#endif

        for(u32num=0;u32num<nfds;u32num++)
        {
            //Check if the file addition request if for the same fd,if yes then skip adding fd to the list of pollfd descriptors 
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                "fd exist check",(fds+u32num)->fd, u32FD, u32num, 0);
#endif

            if((fds+u32num)->fd==u32FD)
            {
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "removing fd ",nfds, u32FD, (fds+u32num)->fd, 0);
#endif  
                nfds--;
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "mem blocks move ",nfds, 0, 0, 0);
#endif 
                /*removing the concerned fds from struct pollfd*/
                OSAL_pvMemoryCopy(fds+u32num,fds+u32num+1,((nfds-u32num)*sizeof(struct pollfd)));

#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "removing",(fds+nfds)->fd,0, nfds, sizeof(fds));
#endif
                fds = (struct pollfd *)realloc(fds,nfds*sizeof(struct pollfd));

				if(fds==NULL)
				{
#ifdef ADC_VERBOSE_TRACE
					DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
					"failed realloc",nfds,0, 0, 0);
#endif
					NORMAL_M_ASSERT_ALWAYS();
					return (tS32)OSAL_E_NOSPACE;
				}

#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "removed1",(fds+nfds)->fd,(fds+0)->fd, nfds, sizeof(fds));
#endif
                //write to pipe to reconfigure it
                fd_rem=1;
                if (write(UnblkPipeFdSet[1], &buf,1)!=1)
                {
                    tS32 s32Errorno = errno;
                    DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Reconfigure,
                        "pipe write failed", getpid(), u32FD,(tU32)s32Errorno, 0);
                    NORMAL_M_ASSERT_ALWAYS();
                    return (tS32)u32ConvertErrorCore( errno );

                }

#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "waiting for event",0,0,0, 0);
#endif

                /*Wait or the child to complete syncing with fdset*/

                if(OSAL_s32EventWait( RemHdl, Removal_Event,
                         OSAL_EN_EVENTMASK_OR,
                        OSAL_C_TIMEOUT_FOREVER  ,
                         &u32EventResultMask )== OSAL_ERROR)
                {
                    DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Reconfigure,
                        "Syncing failed", RemHdl, Removal_Event, 0, 0);
                    NORMAL_M_ASSERT_ALWAYS();
                    return (tS32)OSAL_u32ErrorCode();

                }
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "event received",0,0, 0, 0);
#endif

#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "clearing event",0,0, 0, 0);
#endif
                fd_rem=0;
                /*Clear the event */
                if(OSAL_OK != OSAL_s32EventPost( RemHdl,~(u32EventResultMask),OSAL_EN_EVENTMASK_AND ))
                {
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Reconfigure,
                    "event clear failed",0,0, 0, 0);
                NORMAL_M_ASSERT_ALWAYS();
                return (tS32)OSAL_u32ErrorCode();
#endif
                }

                /*Check if the only left out fd is that of pipe*/
                if(nfds==1)
                {
                    /*close the pipe and reset the thread*/
                    /* reset special file descriptor and delete anonymous pipe
                    /* this will cause the callback thread to exit */

                    //write to pipe to reconfigure it
                    thread_exit=1;
                    if (write(UnblkPipeFdSet[1], &buf,1)!=1)
                    {
                        tS32 s32Errorno = errno;
                        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Reconfigure,
                            "pipe write failed", getpid(), u32FD,(tU32)s32Errorno, 0);
                        NORMAL_M_ASSERT_ALWAYS();
                        return (tS32)u32ConvertErrorCore( errno );
                    }


#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "waiting for event",0,0,0, 0);
#endif

                /*Wait or the child to complete syncing with fdset*/

                if(OSAL_s32EventWait( RemHdl, Removal_Event,
                         OSAL_EN_EVENTMASK_OR,
                        OSAL_C_TIMEOUT_FOREVER  ,
                         &u32EventResultMask )== OSAL_ERROR)
                {
                    DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Reconfigure,
                        "Syncing failed", RemHdl, Removal_Event, 0, 0);
                    NORMAL_M_ASSERT_ALWAYS();
                    return (tS32)OSAL_u32ErrorCode();

                }
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "event received",0,0, 0, 0);
#endif

#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                    "clearing event",0,0, 0, 0);
#endif

                /*Clear the event */
                if(OSAL_OK != OSAL_s32EventPost( RemHdl,~(u32EventResultMask),OSAL_EN_EVENTMASK_AND ))
                {
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Reconfigure,
                    "event clear failed",0,0, 0, 0);
                NORMAL_M_ASSERT_ALWAYS();
                return (tS32)OSAL_u32ErrorCode();
#endif
                }

                      if((close(UnblkPipeFdSet[0]))<0)
                    {
                        tS32 s32Errorno = errno;
                        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Reconfigure,
                            "pipe close FAIL",(fds+0)->fd,(tU32)s32Errorno,0,0);
                        NORMAL_M_ASSERT_ALWAYS();
                        return (tS32)u32ConvertErrorCore( errno );
                    }
                    else
                    {
#ifdef ADC_VERBOSE_TRACE
                        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                            "close pipe",(fds+0)->fd,0,0,0);
#endif
                    }

                    if((close(UnblkPipeFdSet[1]))<0)
                    {
                        tS32 s32Errorno = errno;
                        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Reconfigure,
                            "pipe close FAIL",(fds+0)->fd,(tU32)s32Errorno,0,0);
                        NORMAL_M_ASSERT_ALWAYS();
                        return (tS32)u32ConvertErrorCore( errno );
                    }
                    else
                    {
#ifdef ADC_VERBOSE_TRACE
                        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
                            "pipe close",(fds+0)->fd,0,0,0);
#endif
                        /*Once the pipe has been deleted successfully,reset the nfds(no of poll file decriptore) to zero*/
                        nfds=0;
                        /* reset callback thread ID */
                        CallbkThrID = 0;
                        thread_exit=0;
                        OSAL_vMemoryFree(fds);
                        /*Wait for the child to exit */
                        OSAL_s32ThreadWait(10);
                        break;
                    }

                }

            }

        }
#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Reconfigure,
        "reconfigure exit",nfds,CallbkThrID,s32ret,0);
#endif

    return s32ret;
}

/*****************************************************************************
* FUNCTION:     callback_remove()
* PARAMETER:    No of Callback entries,file descriptor
* RETURNVALUE:  error codes

* DESCRIPTION:  removes the callback from the list of already registered callbacks.
* HISTORY:
------------------------------------------------------------------------------
05/03/12|Basavaraj Nagar| Adapted to Linux

******************************************************************************/
static tS32 callback_remove(intptr_t u32FD)
{
    tInt i;
    tS32 errVal=(tS32)OSAL_E_NOERROR;

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,"cbk_rem enter",getpid(),CallBakLkpCntr,u32FD,(callbacks+0)->fd);
#endif

    if(CallBakLkpCntr==1)
    {
        if((callbacks)->fd==u32FD)
        {
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
                "dealloc cbks",0,0,0,0);
#endif
            /* reset CallBakLkpCntr */
            CallBakLkpCntr=0;
            OSAL_vMemoryFree(callbacks);
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
                "cbk removed",CallBakLkpCntr,0,0,0);
#endif
        }
    }

    for(i=0;i<CallBakLkpCntr;i++)
    {
        if((callbacks+i)->fd==u32FD)
        {
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
                "null callback", 0,0, 0, 0);
#endif
            CallBakLkpCntr--;
            OSAL_pvMemoryCopy(callbacks+i,callbacks+i+1,((CallBakLkpCntr-i)*(sizeof(struct channel_data))));

#ifdef ADC_VERBOSE_TRACE            
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
                "removing",(callbacks+CallBakLkpCntr)->fd,0, CallBakLkpCntr, 0);
#endif
            callbacks =(struct channel_data *)realloc(callbacks,CallBakLkpCntr*sizeof(struct channel_data));

#ifdef ADC_VERBOSE_TRACE  
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
                "removed",(callbacks+CallBakLkpCntr)->fd,0, CallBakLkpCntr, 0);
#endif
        }
    }

    return errVal;
}

/*****************************************************************************
* FUNCTION:     ADC_s32IOClose()
* PARAMETER:    Channel ID
u32FD - Device descriptor
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Closes the requested ADC Sub unit.
* HISTORY:
------------------------------------------------------------------------------

25/12/12|Basavaraj Nagar| Adapted to Linux

******************************************************************************/
tS32 ADC_s32IOClose(tS32 s32ID, uintptr_t u32FD)
{
#ifndef _LINUXX86_64_
    tS32 errVal=(tS32)OSAL_E_NOERROR;

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,"enter into close function of my device",getpid(),s32ID,u32FD,0);
#endif

    if (OSAL_ERROR == OSAL_s32SemaphoreWait(SemHndl, OSAL_C_TIMEOUT_FOREVER))
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOClose,
            "smphr wait failed",0,0, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }
#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
        "removing callback",0,0, 0, 0);
#endif

    /*skip if no callback set*/
    if(CallBakLkpCntr!=0)
        errVal=callback_remove(u32FD);

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
        "removed callback",0,0, 0, 0);
#endif

    if(errVal!=(tS32)OSAL_E_NOERROR)
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
            "cbk remove failed",0,0, 0, 0);
        return (tS32)OSAL_E_UNKNOWN;
    }

    errVal= reconfigure(u32FD);

    if (OSAL_ERROR == OSAL_s32SemaphorePost(SemHndl))
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOClose,
            "smphr post failed",0,0, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    if(errVal!=(tS32)OSAL_E_NOERROR)
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
            "failed reconfiguring",0,0, 0, 0);
        return (tS32)OSAL_E_UNKNOWN;
    }
    if((close(u32FD))<0)
    {
        tInt errsv = errno;
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOClose,
            "close file failed",getpid(), errsv, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)u32ConvertErrorCore( errno );
    }
    else
    {
#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
            "Success",u32FD,0,0,0);
#endif
        u32FD=OSAL_NULL;
        return((tS32)OSAL_E_NOERROR);
    }

    return errVal;

#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
    return OSAL_E_NOERROR;
#endif
}

/*****************************************************************************
* FUNCTION:     ADC_s32IORead()
* PARAMETER:    Channel ID, buffer -to put converted data,
u32Size -no of blocks to be read ,ret_size - no of blocks
that have read.
u32FD - Device descriptor
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Reads the requested ADC Sub unit.
* HISTORY:
-----------------------------------------------------------------------------
Date:       | AUTHOR     |comments
----------------------------------------------------------------------------
25/12/12|Basavaraj Nagar| Adapted to Linux

******************************************************************************/
tS32 ADC_s32IORead(tS32 s32ID,uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
#ifndef _LINUXX86_64_
   tS32 s32Ret = ((tS32)OSAL_E_NOERROR);
   tS32 s32_BytesRead=0;

#ifdef   ADC_VERBOSE_TRACE
   DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IORead,"fd is ",u32FD,0,0,0);
#endif

   if(u32FD==OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IORead,"Invalid File descriptor passed",0,0,0,0);
      return ((tS32)OSAL_E_INVALIDVALUE);
   }

   s32_BytesRead=read(u32FD,(void *)pBuffer,u32Size);

#ifdef ADC_VERBOSE_TRACE
   DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IORead,"return value",0,s32_BytesRead,0,0);
#endif

   if(s32_BytesRead<0)
   {
      tS32 s32Errorno = errno;
      DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IORead,"Read operation Failed",0,s32_BytesRead,(tU32)s32Errorno,0);
      s32Ret=(tS32)u32ConvertErrorCore( errno );
      return s32Ret;
   }
   else
   {
      *ret_size=s32_BytesRead;
#ifdef ADC_VERBOSE_TRACE
      DEV_ADC_vTraceInfo(TR_LEVEL_USER_4,enDEV_ADC_IORead,"success bytes read",0,s32_BytesRead,0,0);
#endif
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID); 
   return s32Ret;
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pBuffer);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32Size);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ret_size);
    return OSAL_E_NOERROR;
#endif
}

/*****************************************************************************
* FUNCTION:     ADC_s32IOControl()
* PARAMETER:    Channel ID, IO Control function,
s32Arg - Argument to be passed to function.
u32FD - Device descriptor
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  ADC Control function:
OSAL_C_S32_IOCTRL_VERSION
OSAL_C_S32_IOCTRL_ADC_GET_ATTRIBUTE_DATA_INFO
OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK
OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD
OSAL_C_S32_IOCTRL_ADC_SET_LIMVIO_INT_REG
OSAL_C_S32_IOCTRL_ADC_SET_STATES_INT_MSK
OSAL_C_S32_IOCTRL_ADC_SET_LIMVIO_INT_MSK
OSAL_C_S32_IOCTRL_ADC_VC_GET_BLOCKSIZE
OSAL_C_S32_IOCTRL_ADC_GET_BLOCK_SIZE

* HISTORY:
-----------------------------------------------------------------------------
Date:         | AUTHOR      |comments
-----------------------------------------------------------------------------
25/12/2012|Basavaraj Nagar(RBEI/ECG4)--adapted to Linux
*****************************************************************************/
tS32 ADC_s32IOControl(tS32 s32ID,uintptr_t u32FD, tS32 s32Fun, intptr_t s32Arg )
{
#ifndef _LINUXX86_64_
   tS32  s32RetVal = ((tS32)OSAL_E_NOERROR);

   if(s32Arg != OSAL_NULL)
   {
      switch(s32Fun)
      {
      case OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK:
         {
            s32RetVal = dev_ADC_SetCallback(u32FD, s32Arg);
            break;
         }
      case OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD:
         {
            s32RetVal = dev_ADC_SetThreshold(u32FD, s32Arg);
            break;
         }
      case OSAL_C_S32_IOCTRL_ADC_GET_CONFIG:
         {
            s32RetVal= dev_ADC_GetResolution(u32FD,s32Arg);
            break;
         }
         /*The following IOCTRLS are not supported for Gen3*/
      case OSAL_C_S32_IOCTRL_ADC_CONFIGURE_ADC:
      case OSAL_C_S32_IOCTRL_ADC_CONFIGURE_SCAN_GROUP:
      case OSAL_C_S32_IOCTRL_ADC_GET_SCAN_GROUP_INFO:
      case OSAL_C_S32_IOCTRL_ADC_CONFIGURE_LOGICAL_CHANNEL:
      case OSAL_C_S32_IOCTRL_ADC_SET_LIMVIO_INT_REG:
      case OSAL_C_S32_IOCTRL_ADC_SET_STATES_INT_MSK:
      case OSAL_C_S32_IOCTRL_ADC_SET_LIMVIO_INT_MSK:
      case OSAL_C_S32_IOCTRL_ADC_VC_GET_BLOCKSIZE:
      case OSAL_C_S32_IOCTRL_SADC_GET_BLOCK_SIZE:
         DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_s32IOControl,
         "unsupported IOCTRL",
         (tU32)s32Fun, (tU32)s32Arg, (tU32)getpid(),((tS32)OSAL_E_NOTSUPPORTED));
         s32RetVal = ((tS32)OSAL_E_NOTSUPPORTED);
         NORMAL_M_ASSERT_ALWAYS();
         break;
      default:
         {
            s32RetVal = ((tS32)OSAL_E_WRONGFUNC);
            break;
         }
      }/* End of switch */
   }
   else
   {
      s32RetVal = ((tS32)OSAL_E_INVALIDVALUE);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   return s32RetVal;
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Fun);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Arg);
    return OSAL_E_NOERROR;
#endif
}

/*****************************************************************************
* FUNCTION:     dev_ADC_GetResolution()
* PARAMETER:    s32Arg - Structure 'OSAL_trAdcConfiguration'
to hold attribute data of ADC, u32FD - Device descriptor.
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Gets the Attribute data(Resolution) of  ADC.
* HISTORY:    
05/03/2013|Basavaraj Nagar(RBEI/ECG4)--written for Linux

******************************************************************************/
static tS32 dev_ADC_GetResolution(uintptr_t u32FD, intptr_t s32Arg)
{
#ifndef _LINUXX86_64_
    tS32 s32RetVal = (tS32)OSAL_E_NOERROR;
    tS32 errVal;
    struct dev_resolution  *padc_get_resolution;

    /*Check for Parameters*/
    if((s32Arg == OSAL_NULL)||(u32FD == OSAL_NULL))
    {
        return ((tS32)OSAL_E_INVALIDVALUE);
    }

    padc_get_resolution=(struct dev_resolution *)s32Arg;
    errVal=ioctl(u32FD,PE_DEVADC_IOCGET_CONFIG,padc_get_resolution);

    if(errVal < 0)
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_GetResolution,"ioctl unsuccessful",errVal,0, 0, 0);
        return ((tS32)OSAL_ERROR);
    }

#ifdef ADC_VERBOSE_TRACE  
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_GetResolution,"ioctl successful",errVal,padc_get_resolution->adc_resolution, 0, 0);
#endif
    return s32RetVal;
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Arg);
    return OSAL_E_NOERROR;
#endif
}

/*****************************************************************************
* FUNCTION:     ADC_SetThreshold()
* PARAMETER:    Channel ID
u32FD - Device descriptor              
s32Arg - pointer to OSAL_trAdcSetThreshold
OSAL_trAdcSetThreshold -> includes the following elements.
*               Threshold value & Comparision flag
*               and bEnable (Should an interrupt be generated for
*                                          this comparator)
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Sets Threshold for channel CH4-CH5.
Sensor ADC supports 2 channels (CH4 - CH5) to facilitate
automatic monitoring of signal.If the Signal level crosses
either the Upperlimit or Lower limit an interrupt is generated.
* HISTORY:
05/03/2013|Basavaraj Nagar(RBEI/ECG4)--written for Linux
07/09/2015|Arun Prabhu(RBEI/ECF5)--dev_threshold structure in adc_inc_ioctl.h
                                   present in ai_osal_linux vob had datamember
                                   called "u16threshold"
                                   dev_threshold structure in kernel headerfile
                                   adc_inc_ioctl.h has datamember called
                                   "threshold".Since we are going to use
                                   kernel header file ,we need to change
                                   "u16threshold" to "threshold".
******************************************************************************/
static tS32 dev_ADC_SetThreshold(uintptr_t u32FD, intptr_t s32Arg)
{
#ifndef _LINUXX86_64_
    OSAL_trAdcSetThreshold*  padcsetthreshold;
    struct dev_threshold devADCsetthreshold;
    /*parameters used for error handling*/  
    tS32 s32RetVal = (tS32)OSAL_E_NOERROR;
    tS32 errVal;

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetThreshold,
        "entry",u32FD,0,0,0);
#endif

    /*Check for Parameters*/
    if((s32Arg == OSAL_NULL)||(u32FD == OSAL_NULL))
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetThreshold,
            "invalid argument",u32FD,0,0,0);
        return ((tS32)OSAL_E_INVALIDVALUE);
    }

    /*Dereferencing the parameters*/
    padcsetthreshold = (OSAL_trAdcSetThreshold *) s32Arg;
    devADCsetthreshold.threshold =  padcsetthreshold->u16Threshold;
    devADCsetthreshold.encomparision =  (enum devadc_comparision)padcsetthreshold->enComparison;
    //devADCsetthreshold.benable      =  padcsetthreshold->bEnable;

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetThreshold,
        "dereferenced",u32FD,0,0,0);
#endif
    s32RetVal= ADC_Fork(u32FD);
    errVal=ioctl(u32FD,PE_DEVADC_IOCSET_THRESHOLD,&devADCsetthreshold);

    if(errVal < 0)
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetThreshold,"ioctl operation unsuccessful",errVal,0, 0, 0);
        return ((tS32)OSAL_ERROR);
    }
    return s32RetVal;
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Arg);
    return OSAL_E_NOERROR;
#endif
}

/*****************************************************************************
* FUNCTION:     dev_ADC_SetCallback()
* PARAMETER:    File Descriptor,
s32Arg -Argument to be passed to function(Includes callback
type and callback function).
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Allows client to register and unregister the callback function
for different ADC events.
* HISTORY:
05/03/2013|Basavaraj Nagar(RBEI/ECG4)--written for Linux

******************************************************************************/
static tS32 dev_ADC_SetCallback(uintptr_t u32FD, intptr_t s32Arg)
{
    tS32 s32RetVal =((tS32)OSAL_E_NOERROR);
    tInt i = 0;

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,"Entry",u32FD,getpid(),0,0);
#endif

    if(u32FD == OSAL_NULL)
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
            "null fd",getpid(),0, 0, 0);
        return ((tS32)OSAL_E_INVALIDVALUE);
    }

    if (OSAL_ERROR == OSAL_s32SemaphoreWait(SemHndl, OSAL_C_TIMEOUT_FOREVER))
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetCallback,
            "smphr wait failed",0,0, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
        "dereferencing cbk ",0,0, 0, 0);
#endif

    OSAL_trAdcSetCallback* padcsetcallback  = (OSAL_trAdcSetCallback *)s32Arg;
    OSAL_tenAdcCallbackType callback_type   = padcsetcallback->enCallbackType;

    if((padcsetcallback->pfnCallbackFunction == OSAL_NULL))
    {
#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
            "cbkremove ",0,0, 0, 0);
#endif
        for(i=0;i<CallBakLkpCntr;i++)
        {
            if((callbacks+i)->fd==u32FD)
            {
                (callbacks+i)->CallbackData.pfnCallbackFunction=OSAL_NULL;
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
                    "deregister",i,u32FD,CallBakLkpCntr,0);
#endif            
            }
        }
    }
    else
    {
#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
            "dereferenced successfully", 0,0, 0, 0);
#endif

        /*For Gen3, only one type of callback namely ADC_CALLBACK_THRESHOLD type is provided as of this version*/
        if((tS32)callback_type != AdcCallbackThreshold)
        {
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
                "invalid callback type", 0,0, 0, 0);
            return ((tS32)OSAL_E_INVALIDVALUE);
        }
        for(i=0;i<CallBakLkpCntr;i++)
        {
            if((callbacks+i)->fd==u32FD)
            {
                /* already registered? */
                if ((callbacks+i)->CallbackData.pfnCallbackFunction != OSAL_NULL)
                {
                    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
                        "already registered", u32FD,i, 0, 0);
                    return (tS32)OSAL_E_ALREADYEXISTS;
                }
            }
        }

#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
            "register callback",CallBakLkpCntr,0, 0, 0);
#endif

        if(CallBakLkpCntr==0)
        {

#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
                "allocating memory",CallBakLkpCntr,0, 0, 0);
#endif
            CallBakLkpCntr++;
            callbacks = (struct channel_data *)calloc(CallBakLkpCntr,sizeof(struct channel_data));
			if(callbacks==NULL)
			{
#ifdef ADC_VERBOSE_TRACE
				DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
                "failed allocating memory",CallBakLkpCntr,0, 0, 0);
#endif
				NORMAL_M_ASSERT_ALWAYS();
				return (tS32)OSAL_E_NOSPACE;
			}
            (callbacks)->fd = u32FD;
            (callbacks)->CallbackData.pfnCallbackFunction = padcsetcallback->pfnCallbackFunction;

#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
                "allocated memory",0,CallBakLkpCntr, 0, 0);
#endif
        }
        else
        {
            callbacks = (struct channel_data *)realloc(callbacks,(CallBakLkpCntr+1)*sizeof(struct channel_data));
            if(callbacks==NULL)
			{
				DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetCallback,
				"realloc failed",0,0, 0, 0);
				NORMAL_M_ASSERT_ALWAYS();
				return (tS32)OSAL_E_NOSPACE;
			}
			(callbacks+CallBakLkpCntr)->fd = u32FD;
            (callbacks+CallBakLkpCntr)->CallbackData.pfnCallbackFunction = padcsetcallback->pfnCallbackFunction;
            CallBakLkpCntr++;
        }
    }
    if (OSAL_ERROR == OSAL_s32SemaphorePost(SemHndl))
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetCallback,
            "smphr post failed",0,0, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    return s32RetVal;
}

/*****************************************************************************
* FUNCTION:     ADC_Fork(tU32 u32FD)
* PARAMETER:

* RETURNVALUE:  ret

* DESCRIPTION:  Sets the threshold of a channel. With the help of 'enComparison'
*                variable this function decides whether to set Upper limit or
*                Lower limit.
*                 
*				 responsible for spawning the thread that polls on interrupts 
*				 and also adding new channels to the set of poll file descriptors
*
* HISTORY:      
05/03/2013|Basavaraj Nagar(RBEI/ECG4)--written for Linux
******************************************************************************/
static tS32 ADC_Fork(uintptr_t u32FD)
{
    tU32 ret=OSAL_E_NOERROR;
    tChar buf = 0;
    tU32 u32num =0;
    /*used to add new fd to set of pollfds*/
    tInt pollfdreconfigure=1;   
    OSAL_tEventMask gevMask = 0;
    OSAL_trThreadAttribute CallbackThreadAttribute;


#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetThreshold,
        "ADC_Fork enter",u32FD,0, 0, 0);
#endif

    if(SemHndl==OSAL_C_INVALID_HANDLE)
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_Ext_ADC_SetThreshold,
            "smphr creation failed",0,0, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }
    /* enter critical section */
    if (OSAL_ERROR == OSAL_s32SemaphoreWait(SemHndl, OSAL_C_TIMEOUT_FOREVER))
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_Ext_ADC_SetThreshold,
            "smphr wait failed",0,0, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }

    if (CallbkThrID == 0)
    {
        nfds=2;
#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
            "allocating memory",nfds,0, 0, 0);
#endif

        fds = (struct pollfd *)calloc(nfds,sizeof(struct pollfd));
		if(fds==NULL)
		{
#ifdef ADC_VERBOSE_TRACE
			DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
            "failed allocating memory",nfds,0, 0, 0);
			NORMAL_M_ASSERT_ALWAYS();
		    return (tS32)OSAL_E_NOSPACE;
#endif
		}

        /* create anonymous pipe for unblocking poll call in callback thread */
        if (pipe(UnblkPipeFdSet) != 0)
        {
            tInt errsv = errno;
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_Ext_ADC_SetThreshold,
                "pipe create failed", getpid(), u32FD, errsv, 0);
            NORMAL_M_ASSERT_ALWAYS();
            return (tS32)u32ConvertErrorCore( errno );
        }
        (fds+0)->fd=UnblkPipeFdSet[0];
        (fds+0)->events=POLLIN;

        (fds+1)->fd=u32FD;
        (fds+1)->events=POLLRDBAND|POLLWRBAND ;

#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
            "active poll fds",fds[0].fd,fds[1].fd, 0, 0);
#endif
        CallbackThreadAttribute.u32Priority= OSALTHREAD_C_U32_PRIORITY_MID;
        CallbackThreadAttribute.s32StackSize= DEV_ADC_STACK_SIZE;
        CallbackThreadAttribute.pfEntry= (OSAL_tpfThreadEntry)dev_ADC_CallbackThread;
        CallbackThreadAttribute.szName= "DRV_ADC";

#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetThreshold,
            "about to fork",0,0, 0, 0);
#endif
        CallbkThrID=OSAL_ThreadSpawn(&CallbackThreadAttribute);

#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
            "thread id",CallbkThrID,0, 0, 0);
#endif

        if (OSAL_ERROR == CallbkThrID)
        {
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_Ext_ADC_SetThreshold,
                "spawn thread failed", CallbkThrID,0, 0, 0);
            NORMAL_M_ASSERT_ALWAYS();
            return (tS32)OSAL_E_THREAD_CREATE_FAILED;
        }
    }
    else
    {
#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
            "thread already forked ", CallbkThrID,0, 0, 0);
#endif
        for(u32num=0;u32num<nfds;u32num++)
        {
            //Check if the file addition request if for the same fd,if yes then skip adding fd to the list of pollfd descriptors 
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
                "fd exist check",(fds+u32num)->fd, u32FD, pollfdreconfigure, 0);
#endif
            if((fds+u32num)->fd==u32FD)
            {
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
                    "pfdreconf reset ",(fds+u32num)->fd, u32FD, 0, 0);
#endif  
                pollfdreconfigure=0;
                break;
            }
        }

        if(pollfdreconfigure==1)
        {

#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
                "reconfiguring",u32FD,0,0, 0);
#endif

            /*New fd addition taking place to set of poll file descriptors*/
            fds = (struct pollfd *)realloc(fds,(nfds+1)*sizeof(struct pollfd));

		if(fds==NULL)
		{
#ifdef ADC_VERBOSE_TRACE
			DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
            "failed allocating memory",nfds,0, 0, 0);
			NORMAL_M_ASSERT_ALWAYS();
			return (tS32)OSAL_E_NOSPACE;
#endif
		}
            /*Checking whether the reallocation is successful*/

            (fds+(nfds))->fd=u32FD;
            (fds+(nfds))->events =POLLRDBAND|POLLWRBAND;
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
                "added",(fds+nfds)->fd, buf, nfds, 0);
#endif
            nfds++;
            fd_add=1;
            if (write(UnblkPipeFdSet[1], &buf,1)<0)
            {
                tS32 s32Errorno = errno;
                DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_Ext_ADC_SetThreshold,
                    "pipe write failed", getpid(), u32FD,(tU32)s32Errorno, 0);
                NORMAL_M_ASSERT_ALWAYS();
                return (tS32)u32ConvertErrorCore( errno );
            }
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
                "reconfigured successfully",u32FD, 0,0, 0);
#endif

#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
                    "waiting for event",0,0,0, 0);
#endif
                /*Wait or the child to complete syncing with fdset*/

                if(OSAL_s32EventWait( AddHdl, Addition_Event,
                         OSAL_EN_EVENTMASK_OR,
                        OSAL_C_TIMEOUT_FOREVER  ,
                         &gevMask )== OSAL_ERROR)
                {
                    DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_Ext_ADC_SetThreshold,
                        "Syncing failed", getpid(), u32FD, 0, 0);
                    NORMAL_M_ASSERT_ALWAYS();
                    return (tS32)OSAL_u32ErrorCode();

                }
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
                    "event received",0,0, 0, 0);
#endif

                /*Clear the event */
                fd_add=0;
                if(OSAL_OK != OSAL_s32EventPost( AddHdl,~(gevMask),OSAL_EN_EVENTMASK_AND ))
                {
                    DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_Ext_ADC_SetThreshold,
                        "event clear failed", AddHdl, Addition_Event, 0, 0);
                    NORMAL_M_ASSERT_ALWAYS();
                    return (tS32)OSAL_u32ErrorCode();

                }

        }
    }

    if (OSAL_ERROR == OSAL_s32SemaphorePost(SemHndl))
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_Ext_ADC_SetThreshold,
            "smphr post failed",0,0, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tS32)OSAL_u32ErrorCode();
    }
#ifdef ADC_VERBOSE_TRACE

    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_Ext_ADC_SetThreshold,
        "exit",u32FD, 0,0, 0);
#endif

    return (tS32)ret;
}



/*****************************************************************************
* FUNCTION:     dev_ADC_CallbackThread()
* PARAMETER:    
* RETURNVALUE:  

* DESCRIPTION:  The Callback thread continously polls for threshold hit and when threshold hit does occur, 
it calls the callback function and alo dynamically reconfigures the pollfds as per the
events

* HISTORY: 
05/03/2013|Basavaraj Nagar(RBEI/ECG4)--written for Linux

******************************************************************************/
static tVoid dev_ADC_CallbackThread()
{
    tU32 iID,retVal = OSAL_E_NOERROR;
    tInt len = 0,i = 0,iCID = 0;
    tU32 fd=0;
    tS32 ret;
    tU32  buf;

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
        "callback thread running:",0, 0, 0, 0);
#endif

    while (1)
    {
        len = poll(fds, nfds,-1);

#ifdef ADC_VERBOSE_TRACE
        DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
            "poll(2) returned",0, nfds, 0, 0);
#endif
        if (len < 0)
        {
            tInt errsv = errno;
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                "poll(2) failed",0, errsv, 0, 0);
            retVal = u32ConvertErrorCore( errno );
            break;
        }

        if (len == 0)
        {
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                "poll(2) timeout",0, 0, 0, 0);
        }

        /* check if the poll call was unblocked via the special pipe and reconfigure the fds accordingly */
        if ((fds+0)->revents & POLLIN)
        {
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                "POLLIN",getpid(), fds[0].fd, sizeof(tS32), 0);
#endif
            ret=read((fds+0)->fd,&buf,1);

            if (ret<0)
            {
               tS32 s32Errorno = errno;
               DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                     "read pipe fail", getpid(), ret, (fds+0)->fd,(tU32)s32Errorno);
               retVal = u32ConvertErrorCore( errno );
            }
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
            		"currentfd & buf",0, buf, 0, 0);
#endif



#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
            		"posting event",(fds+0)->fd, 0, 0, 0);
#endif
            (fds+0)->revents=0;
            /*SyncEvent posted to Parent*/
            if(fd_add==1)
            {
            	if(OSAL_s32EventPost(AddHdl, Addition_Event, OSAL_EN_EVENTMASK_OR)==OSAL_ERROR)
				{
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
            		"addhdl failed posting event",AddHdl, Addition_Event, 0, 0);
#endif
				}
            }

            if(fd_rem==1)
            {
            	if(OSAL_s32EventPost(RemHdl, Removal_Event, OSAL_EN_EVENTMASK_OR)==OSAL_ERROR)
				{
#ifdef ADC_VERBOSE_TRACE
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
            		"remhdl failed posting event",RemHdl,Removal_Event, 0, 0);
#endif
				}
            }
            if(thread_exit==1)
            {
#ifdef ADC_VERBOSE_TRACE
            	DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
            			"callbk thr exit",(fds+0)->fd, 0, 0, 0);
#endif
            	break;
            }

        }
        //When the driver has fed input data to OSAL DRV, the below condition will be true
        /* check which channels have input data to read */
        for (iID = 1; iID < nfds; iID++)
        {
#ifdef ADC_VERBOSE_TRACE
        	DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
        			"fdset revents events",0, iID, fds[iID].revents, fds[iID].events);
#endif
            if ((fds+iID)->revents & (POLLRDBAND | POLLWRBAND))
            {
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                    "****poll(2) event",0, iID, 0, CallBakLkpCntr);
#endif
                iCID = CallBakLkpCntr;
                fd = (fds+iID)->fd;
                for(i = 0;i<iCID;i++)
                {
                    if(fd == ((callbacks+i)->fd))
                    {
                        /* execute callback function */
                        if ((callbacks+i)->CallbackData.pfnCallbackFunction != OSAL_NULL)
                        {
                            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                                "calling callbk function!!!",i, iID, iCID, 0);
                            (callbacks+i)->CallbackData.pfnCallbackFunction();
                        }
                        else
                        {
                            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                                "no callbk function registered!!!",0, iID, 0, 0);
                        }
                    }
                }

#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                    "revent reset",0, iID, 0, 0);
#endif
                /* reset returned events */
                (fds+iID)->revents = 0;
            }

            if ((fds+iID)->revents & (POLLNVAL))
            {
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                    "POLLNVAL enter",getpid(), fds[0].fd, sizeof(tS32), 0);
#endif
                ret=read((fds+iID)->fd,&buf,sizeof(buf));

                if (ret<0)
                {
                    tS32 s32Errorno = errno;
                    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                        "read pipe fail", getpid(), ret, (fds+0)->fd,(tU32)s32Errorno);
                    retVal = u32ConvertErrorCore( errno );
                }
#ifdef ADC_VERBOSE_TRACE
                DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
                    "currentfd & buf",0, buf, 0, 0);
#endif
                (fds+iID)->revents=0;
            }
        }
    }



    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
        "event post",0, retVal, 0, 0);

    (void)OSAL_s32EventPost(RemHdl, Removal_Event, OSAL_EN_EVENTMASK_OR);
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_CallbackThread,
        "Exit:",0, retVal, 0, 0);
    return;

}

/************************************************************************/
/*!
*\fn       tBool drv_ADC_ExistsPath(const tPChar path)

*\brief    Check if a path exists using the stat() system call
*
*\param    path - (in) path
*
*\return   OSAL error code
*
*\par History:
* 18.02.2013 - Basavaraj Nagar (RBEI/ECG-4) initial version
**********************************************************************/
static tBool dev_ADC_ExistsPath(const tPChar path)
{
    tInt len = 0;
    struct stat st;

    /* do nothing if already exported */
    len = stat(path, &st);
    if (len == 0)
    {
        return TRUE;
    }
    return FALSE;
}

/************************************************************************/
/*!
*\fn       tBool dev_ADC_open(tCString device_name,const tInt flags,tU32 * pFd)

*\brief    Checks for the existence of path specified and then 
make sure  opens the device using open() Linux system call
*
*\param    path - (in) path         
*			pFd -  pointer to file descriptr to be retained
*
*\return   OSAL error code
*
*\par History:
* 18.02.2013 - Basavaraj Nagar (RBEI/ECG-4) initial version
**********************************************************************/
static tU32 dev_ADC_open(tCString device_name,uintptr_t * pFd)
{
    tChar cFullPath[ADC_MAX_DEVNODE_PATH];
    tU32 retVal=OSAL_E_NOERROR;
    tInt len = 0;
    tS32 opened_fd;
    
#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOOpen_Subhelper,"enter dev_ADC_open  function ",getpid(),0,0,0);
#endif

    len = snprintf(cFullPath, ADC_MAX_DEVNODE_PATH, "%s/%s",
        ADC_DEV_NODE_PATH,device_name);

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceString(TR_LEVEL_USER_4,
        enDEV_ADC_IOOpen_Subhelper, cFullPath);
#endif

    if (len <= 0 || len >= ADC_MAX_DEVNODE_PATH)
    {
        tInt errsv = errno;
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen_Subhelper,
            "gen path failed", getpid(), errsv, len, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return OSAL_E_UNKNOWN;
    }

    if(!dev_ADC_ExistsPath(cFullPath))
    {
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen_Subhelper,
            "path not exist", getpid(),0, 0, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return OSAL_E_DOESNOTEXIST;
    }

#ifdef ADC_VERBOSE_TRACE
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOOpen_Subhelper,"Path exists",getpid(),0,0,0);
#endif
    opened_fd=open(cFullPath,O_RDONLY);

    if(opened_fd<0)
    {
        tInt errsv = errno;
        DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen_Subhelper,
            "open file failed",0, opened_fd, errsv, 0);
        NORMAL_M_ASSERT_ALWAYS();
        return (tU32)u32ConvertErrorCore( errno );//For LINT sake
    }

    *pFd=opened_fd;

#ifdef ADC_VERBOSE_TRACE  
    DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOOpen_Subhelper,
        "fd is", *pFd, 0,0, 0);
#endif
    return retVal;

}
