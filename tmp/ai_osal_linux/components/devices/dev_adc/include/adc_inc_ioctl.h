/******************************************************************************
*****     (C) COPYRIGHT Robert Bosch GmbH CM-DI - All Rights Reserved     *****
******************************************************************************/
/*! 
***     @file      adc_inc_ioctl.h
***
***     @brief       ioctl definition for adc_inc driver
***
***
***     @warning     
***
***     @todo        
***
*** --------------------------------------------------------------------------
\par   TECHNICAL INFORMATION:
***
\n     Operatingsystem: Linux
***
*** --------------------------------------------------------------------------
\par  VERSION HISTORY:
**  
*          14.01.2013 - File Created
**
*\n*/
/*****************************************************************************/
#ifndef ADC_INC_IOCTL_H
#define ADC_INC_IOCTL_H

#include "bosch_ioctl.h"
#include <stdbool.h>

enum devadc_comparision {
	devadc_threshold_lt = 0,
	devadc_threshold_gt,
	devadc_threshold_uk,
};

struct dev_threshold {
	u_int16_t u16threshold;
	enum devadc_comparision encomparision;
};

struct dev_resolution {
	u_int8_t adc_resolution;
};


#define PE_DEVADC_IOCSET_THRESHOLD _IOW(BOSCH_MAGIC_ADC_INC, 1\
							, struct dev_threshold)
#define PE_DEVADC_IOCGET_CONFIG    _IOR(BOSCH_MAGIC_ADC_INC, 2\
							, struct dev_resolution)

#endif /* ADC_INC_IOCTL_H_ */

