/******************************************************************************
*****     (C) COPYRIGHT Robert Bosch GmbH CM-DI - All Rights Reserved     *****
******************************************************************************/
/*! 
***     @file      drv_adc.h
***     @Authors
\n                  TMS-Dangers (mds)
\n                Fabian Zwissler, CM-DI/EAP (zw)
***
***     @brief       implementation of adc in OSAL
***
***
***     @warning     
***
***     @todo        
***
*** --------------------------------------------------------------------------
\par   TECHNICAL INFORMATION:
***
\n     Hardware       : Dragon / ADIT-Platform; Tested with Dragon Working Sample 1.1
\n     Operatingsystem: T-Kernel
***
*** --------------------------------------------------------------------------
\par  VERSION HISTORY:
**  
*    $Log: 14.10.2005 - (mds) File created
*          08.03.2006 - (zw)  Adaptions to T-Kernel(Dragon Platform)
*          25.12.2012 - Adapted for Linux - Basavaraj Nagar (RBEI/ECG4)
*          07.09.2015 - adc_inc_ioctl.h in ai_osal_linux vob has an additional 
*                       data-strucure dev_resolution which is not present in 
*                       kernel header file adc_inc_ioctl.h , so this 
*                       dev_resolution is defined here.Also u16 and u8 
*                       datatypes used in kernel header file adc_inc_ioctl.h
*                       are typedefed here,so that they are recognised in user
*                       space.- Arun Prabhu (RBEI/ECF5)
**
*\n*/
/*****************************************************************************/

#ifndef _DRV_ADC_H
#define _DRV_ADC_H

/*=============================================================================
=======                            INCLUDES                             =======
=============================================================================*/

/*=============================================================================
=======               DEFINES & MACROS FOR GENERAL PURPOSE              =======
=============================================================================*/

/*the total no of adc channels available ...see osiopublic.h*/

#define ADC_MAX_CHANNEL 255  
#define ADC_MAX_DEVNODE_PATH 128
#define ADC_DEV_NODE_PATH "/dev/adc"
#define ADC_VERBOSE_TRACE

/*=============================================================================
=======                       CONSTANTS  &  TYPES                       =======
=============================================================================*/

typedef enum 
{
	ADC_TRC_FN_INFO=0,
	ADC_TRC_FN_STRING
}enADCTraceMessages;

typedef enum 
{
	enDEV_ADC_IOOpen=0,
	enDEV_ADC_IOClose,
	enDEV_ADC_IORead,
	enDEV_ADC_IOControl,
	enDEV_ADC_SetThreshold,
	enDEV_ADC_SetCallback,
	enDEV_Ext_ADC_SetThreshold,
	enDEV_ADC_CallbackThread,
	enDEV_ADC_GetResolution,
	enDEV_ADC_IOOpen_Subhelper,
    enDEV_ADC_s32IOControl,
    enDEV_ADC_Reconfigure
} enDevADCTraceFunction;

//Since Kernel datatypes u16 and u8 are not recognised in user space
typedef u_int8_t u8;
typedef u_int16_t u16;

struct dev_resolution {
	u_int8_t adc_resolution;
};


//callback funtion for ADC DRV
typedef void (* ADC_CallbackFunction)(void);


typedef enum 
{
	/* Callbacks with this identifier will be called when a threshold interrupt 
	is received. The callback function has to be installed for a specific 
	logical channel. */
	AdcCallbackThreshold = 3
}ADC_CallbackType;


typedef struct
{
	/* In which situation should the callback function be called? */
	ADC_CallbackType enCallbackType;

	/* The function to call */
	ADC_CallbackFunction pfnCallbackFunction;

} ADC_SetCallback;

/*Below structures used for tracking files for whom callback functions have 
been activated and deregister them during application(or file descriptor)close*/

struct channel_data
{
    tU32 fd;
    ADC_SetCallback CallbackData;
};
/*The callbacks registered/unregistered*/
struct channel_data *callbacks=OSAL_NULL;

/*=============================================================================
=======                    PROTOTYPES OF PUBLIC FUNCTIONS               =======
=============================================================================*/

tS32 ADC_s32IOOpen(tCString szName,tS32 s32ID, uintptr_t *pu32FD);   
tS32 ADC_s32IOClose(tS32 s32ID, uintptr_t u32FD);
tS32 ADC_s32IOControl(tS32 s32ID,uintptr_t u32FD, tS32 s32Fun, intptr_t s32Arg);
tS32 ADC_s32IORead(tS32 s32ID, uintptr_t u32FD,tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

#endif
/* EOF */
