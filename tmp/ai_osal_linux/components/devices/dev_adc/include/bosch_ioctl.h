/******************************************************************************
*****     (C) COPYRIGHT Robert Bosch GmbH CM-DI - All Rights Reserved     *****
******************************************************************************/
/*! 
***     @file      bosch_ioctl.h
***
***     @brief       Contains common magic number for bosch drivers
***
***
***     @warning     
***
***     @todo        
***
*** --------------------------------------------------------------------------
\par   TECHNICAL INFORMATION:
***
\n     Operatingsystem: Linux
***
*** --------------------------------------------------------------------------
\par  VERSION HISTORY:
**  
*          14.01.2013 - File Created
**
*\n*/
/*****************************************************************************/



#ifndef BOSCH_IOCTL_H
#define BOSCH_IOCTL_H

#include <linux/ioctl.h>

#define BOSCH_MAGIC_OFFSET		0x88
/* the first Bosch driver that needs a magic number */
#define BOSCH_MAGIC_ADC_INC		(BOSCH_MAGIC_OFFSET + 0)

#endif /*BOSCH_IOCTL_H*/

