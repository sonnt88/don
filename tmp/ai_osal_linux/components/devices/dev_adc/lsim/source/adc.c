/******************************************************************************
* FILE         : adc.c
*
* SW-COMPONENT : OSAL IO
*
* DESCRIPTION  : This file implements the application interfaces to access ADC
*                device for LSIM
*
* AUTHOR(s)    : Chandra Sekaran Mariappan (RBEI/ECF5)
*
* HISTORY      :
*-----------------------------------------------------------------------------
*28, July, 2014   | Version 0.00      | Dharmender Suresh Chander(RBEI/EC55)
                  | Written for Linux
*13, January 2016 | Version 1.00      | Chandra Sekaran Mariappan(RBEI/ECF5)
                  | Implemented the functionalities of adc
******************************************************************************/
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <netdb.h> 
#include <time.h>
#include <pthread.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
#include "OsalConf.h"
/*adc.h has u16 and u8 datatypes which are used by adc_inc_ioctl.h so,
include adc.h before adc_inc_ioctl.h to prevent build issues*/
#include "adc.h"
#include <linux/adc_inc_ioctl.h>

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/*TR_CLASS_DEV_SADC is redefined to TR_CLASS_DEV_ADC for consistency*/
#define OSAL_C_TR_CLASS_DEV_ADC OSAL_C_TR_CLASS_DEV_SADC
#define ADC_INC_GETSAMPLE_BYTEPER_SAMPLE  (0x02)
#define PORT_OFFSET 0xC700
#define PORT_EXTENDER_ADC_PORT (0x03 | PORT_OFFSET)
#define AF_BOSCH_INC_LINUX AF_INET
#define ADC_INC_THRESHOLD_UPPER_LIMIT 0x01
#define ADC_INC_THRESHOLD_LOWER_LIMIT 0x02
#define ADC_INC_THRESHOLD_HIT 0x01
#define ADC_INC_THRESHOLD_NOHIT 0x00
#define WAITTIME 1 
#define DEV_ADC_SEM_NAME "DEV_ADC_SEM"
#define DEV_ADC_SH_NAME "ADC_INC_SM"
#define DEV_ADC_CALLBACK_SH_NAME "ADC_CALLBACK_SM"
#define DEV_ADC_LOCK_NAME "ADC_INC_Lock"
#define ADC_MAX_CHANNELS 256

/*Semaphore handle*/
static OSAL_tSemHandle SemHndl = OSAL_C_INVALID_HANDLE;

/* Global Variables */
tS32 s32Sock_FD = 0;
OSAL_tShMemHandle Sharedmemhdl = OSAL_ERROR;
OSAL_tShMemHandle Sharedmemhd2 = OSAL_ERROR;
pthread_mutex_t lock;

/*Used for pthread functionalities*/
pthread_t tid;
typedef void* (*PTHREAD_FUNC) (void *);

/************************************************************************
|function prototypes (scope: local)
|-----------------------------------------------------------------------*/
static tS32 dev_ADC_inc_handle_threshold_hit(struct adc_dev *padc, tU8 channel,
                                            tU8 upperhit, tU8 lowerhit);
static tS32 dev_ADC_inc_handle_threshold_set_response(struct adc_dev *padc,
                                               tU8 channel, const tU8 *data);
static tS32 dev_ADC_inc_cmd_status_active();
static tS32 dev_ADC_inc_getconfig();
static tS32 dev_ADC_inc_buffer_update(tU8 u8nADC, const tU8 *u8Data);
static tS32 dev_ADC_handle_rx_msg(const char *Recvbuf);
tPVoid dev_ADC_inc_rx_task(tPVoid);
static tS32 dev_ADC_socket_create();
static tS32 dev_ADC_resource_init();
static tS32 dev_ADC_sharedmem_init();
static tS32 dev_ADC_callback_sharedmem_init();
static tS32 dev_ADC_callback_remove(tU32 u32FD);
static tS32 dev_ADC_callback_execute_function(tU32 u32FD);
static tS32 dev_ADC_Readfunction(tU32 u32FD, tPS8 pBuf, tU32 u32Size);
static tS32 dev_ADC_SetCallback(tU32 u32FD, tS32 s32Arg);
static tS32 dev_ADC_GetResolution(tS32 s32Arg);
static tS32 dev_ADC_SetThreshold(tU32 u32FD, tS32 s32Arg);
static tS32 dev_ADC_inc_set_threshold(const struct dev_threshold *threshold_data,
                                     tU8 u8Ch);

/************************************************************************/
/*!
*\fn       tVoid DEV_ADC_vTraceInfo(TR_tenTraceLevel enTraceLevel,
enDevADCTraceFunction enFunction,
tPCChar copchDescription,
tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,
tU32 u32Par4)
*\brief    Function to print ADC trace messges on console IO
*
*          To print trace messages on console IO ADC driver call this
*          function at nessessary places.
*
*\param    TR_tenTraceLevel [IN] - To opt trace level
*\param    enFunction [IN] - Current ADC Funtion name
*\param    copchDescription [IN] - char string which prints on console IO
*\param    u32Par1 [OUT] - Output value from the ADC funciton
*\param    u32Par1 [OUT] - Output value from the ADC funciton
*\param    u32Par1 [OUT] - Output value from the ADC funciton
*\param    u32Par1 [OUT] - Output value from the ADC funciton
*
*\return  No return value. Funciton type is void.
*
*\par History:
*25.12.2012 - Ported to Linux - Basavaraj Nagar (RBEI/ECG4)
**********************************************************************/
static tVoid DEV_ADC_vTraceInfo(TR_tenTraceLevel enTraceLevel,
                                enDevADCTraceFunction enFunction,
                                tPCChar copchDescription,
                                tU32 u32Par1, tU32 u32Par2, tU32 u32Par3,tU32 u32Par4)
{
   tChar ch;
   tInt i;
   tU32 u32ThreadID;
   tU8 u8StringLength;
   tU8 au8Buf[3 * sizeof(tU8) + 16 * sizeof(tChar) + 5 * sizeof(tU32)];
   /* Trace Level enabled ? */
   if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_ADC,(tU32)enTraceLevel))
   {
      /* calculate the given string length */
      u8StringLength = (tU8)OSAL_u32StringLength(copchDescription);
      /*string lentgh greater than reserved space ? */
      if (u8StringLength > 16)
      {
         /* set string length to maximum allowed value */
         u8StringLength = 16;
      }
      /* fill trace buffer with desired values */
      u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
      OSAL_M_INSERT_T8(&au8Buf[0], (tU8) ADC_TRC_FN_INFO);
      OSAL_M_INSERT_T32(&au8Buf[1], u32ThreadID);
      OSAL_M_INSERT_T8(&au8Buf[5], (tU8) enFunction);
      /* copy given string into the trace buffer */
      for (i = 0; i < u8StringLength; ++i)
      {
         ch = copchDescription[i];
         OSAL_M_INSERT_T8(&au8Buf[6+i], (tU8) ch);
         if (ch == '\0') break;
      }
      /* set string end \0 */
      OSAL_M_INSERT_T8(&au8Buf[6+i], (tU8) '\0');
      /* copy par1 to par4 into the trace buffer */
      OSAL_M_INSERT_T32(&au8Buf[23], u32Par1);
      OSAL_M_INSERT_T32(&au8Buf[27], u32Par2);
      OSAL_M_INSERT_T32(&au8Buf[31], u32Par3);
      OSAL_M_INSERT_T32(&au8Buf[35], u32Par4);
      /* send adc trace to IO-console */
      LLD_vTrace((tU32)OSAL_C_TR_CLASS_DEV_ADC, (tU32)enTraceLevel,
          au8Buf, sizeof(au8Buf));
   }
}

/**************************************************************************
* Function : dev_ADC_inc_handle_threshold_hit()
* Description: This is to check whether the threshold hit is for lower limit 
               or upper limit
* Parameters: padc - Global structure
              channel - channel number
              upperhit - variable to check for upper limit
              lowerhit - variable to check for lower limit
* Return value: s8Ret, OSAL error codes
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_inc_handle_threshold_hit(struct adc_dev *padc, tU8 channel,
					tU8 upperhit, tU8 lowerhit)
{
   tS32 s32Ret = OSAL_E_NOERROR;
   if (padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Threshold_Hit,
               "padc is NULL", 0, 0, 0, 0);
      return OSAL_E_INVALIDVALUE;
   }
   if (channel >= padc->Num_channels) 
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Threshold_Hit,
               "Invalid Channel Index", 0, 0, 0, 0);
      return OSAL_E_INVALIDVALUE;
   }
   pthread_mutex_lock(&lock);
   if ((upperhit == ADC_INC_THRESHOLD_HIT) &&
      (lowerhit == ADC_INC_THRESHOLD_NOHIT))
   {
      padc->ch[channel].threshold_hit =
         ADC_INC_THRESHOLD_UPPER_LIMIT;
   }
   else if ((lowerhit == ADC_INC_THRESHOLD_HIT) &&
      (upperhit == ADC_INC_THRESHOLD_NOHIT))
   {
      padc->ch[channel].threshold_hit =
         ADC_INC_THRESHOLD_LOWER_LIMIT;
   }
   else 
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Threshold_Hit,
                 "Invalid message from remote", 0, 0, 0, 0);
      s32Ret = OSAL_E_INVALIDVALUE;
   }
   pthread_mutex_unlock(&lock);
   return s32Ret;
}

/**************************************************************************
* Function : dev_ADC_inc_handle_threshold_set_response()
* Description: This function is to set the threshold value, upper limit and 
               lower limit for a channel which is obtained from the server   
* Parameters: struct adc_dev *padc, tU8 channel, tU8 *data
              padc - Global ADC structure
              channel - ADC channel
              data - Data obtained from server
* Return value: OSAL error codes
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_inc_handle_threshold_set_response(struct adc_dev *padc,
						tU8 channel, const tU8 *data)
{
   tU16 u16Threshold = data[2];
   if (padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Threshold_Set_Response,
               "padc is NULL", 0, 0, 0, 0);
      return OSAL_E_INVALIDVALUE;
   }
   if (channel >= padc->Num_channels)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Threshold_Set_Response,
             "Invalid channel index", 0, 0, 0, 0);
      return OSAL_E_INVALIDVALUE;
   }
   u16Threshold = (tU16)((u16Threshold << 8) | data[1]);
   switch (data[0])
   {
      case ADC_INC_THRESHOLD_UPPER_LIMIT:
         pthread_mutex_lock(&lock);
         padc->ch[channel].comparision = devadc_threshold_upt;
         pthread_mutex_unlock(&lock);
         break;
      case ADC_INC_THRESHOLD_LOWER_LIMIT:
         pthread_mutex_lock(&lock);
         padc->ch[channel].comparision = devadc_threshold_lmt;
         pthread_mutex_unlock(&lock);
         break;
      default:
         break;
   }
   pthread_mutex_lock(&lock);
   padc->ch[channel].threshold = (tU8)u16Threshold;
   padc->ch[channel].set_threshold = 0x01;
   pthread_mutex_unlock(&lock);
   return OSAL_E_NOERROR;
}

/**************************************************************************
* Function : dev_ADC_inc_cmd_status_active()
* Description: This function is to check whether there is an active communication
               between the server and the client.
               This check is done by sending the ADC_INC_STATUS_ACTIVE signal 
               to the server.
* Parameters: Nil
* Return value: s8Ret
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_inc_cmd_status_active()
{
   tU8 Sendbuf[2] = {0};
   tS32 s32Ret;
   Sendbuf[0] = (tU8)SCC_PORT_EXTENDER_ADC_C_COMPONENT_STATUS_MSGID;
   Sendbuf[1] = (tU8)ADC_INC_STATUS_ACTIVE;
   s32Ret = send(padc->socket_fd, Sendbuf, sizeof(Sendbuf), 0);
   if(s32Ret < 0)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen,
               "Send failed for cmd_status_active with error : %",
                  u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   return (tS32)OSAL_E_NOERROR;
}

/**************************************************************************
* Function : dev_ADC_inc_getconfig()
* Description: This function is to get configuration from the server like
               Number of channels and the resolution of ADC.
* Parameters: Nil
* Return value: s8Ret or OSAL error codes
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_inc_getconfig()
{
   tU8 Sendbuf[1] = {0}, Recvbuf[1024] = {0};
   tS32 s32Ret;
   memset(Sendbuf,'\0', 1);
   if (padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
               "padc is NULL", 0, 0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   Sendbuf[0] = SCC_PORT_EXTENDER_ADC_C_GET_SAMPLE_MSGID;
   s32Ret = send(padc->socket_fd, Sendbuf, sizeof(Sendbuf), 0);
   if(s32Ret < 0)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
               "Send failed for get_sample with error %",
                     u32ConvertErrorCore(errno), 0, 0, 0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   s32Ret = recv(padc->socket_fd, Recvbuf, sizeof(Recvbuf), 0);
   if (s32Ret <= 0) 
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
             "Receive failed for get_sample with error %",
                     u32ConvertErrorCore(errno), 0, 0, 0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   if (Recvbuf[0] != SCC_PORT_EXTENDER_ADC_R_GET_SAMPLE_MSGID)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
            "Invalid msg received for get_sample_msg_id", 0, 0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   padc->Num_channels = Recvbuf[1];
   Sendbuf[0] = SCC_PORT_EXTENDER_ADC_C_GET_CONFIG_MSGID;
   s32Ret = send(padc->socket_fd, Sendbuf, sizeof(Sendbuf), 0);
   if(s32Ret < 0)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
            "Send failed for get_config with error %",
                  u32ConvertErrorCore(errno), 0, 0, 0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   s32Ret = recv(padc->socket_fd, Recvbuf, sizeof(Recvbuf), 0);
   if (s32Ret <= 0)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
             "Receive failed for get_config with error %", 
                  u32ConvertErrorCore(errno), 0, 0, 0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   if (Recvbuf[0] != SCC_PORT_EXTENDER_ADC_R_GET_CONFIG_MSGID)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
             "Invalid msg received for get_config", 0, 0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   if (Recvbuf[1] != ADC_INC_RESOLUTION_10BIT)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
               "Invalid Resolution obtained", 0, 0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   padc->resolution = Recvbuf[1];
   DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
               "Resolution is", padc->resolution, 0, 0, 0);
   padc->buffer = calloc(1, padc->Num_channels * ADC_INC_GETSAMPLE_BYTEPER_SAMPLE);
   if (!padc->buffer)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Getconfig,
               "Memory Allocation failed",0,0,0,0);
      return (tS32)OSAL_E_NOSPACE;
   }
   return (tS32)OSAL_E_NOERROR;
}

/**************************************************************************
* Function : dev_ADC_inc_buffer_update()
* Description: This function is to store the values obtained from the server
               to a buffer.
* Parameters: u8nADC - Number of ADC channels
              u8Data - Receive buffer from server
* Return value: OSAL error codes
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_inc_buffer_update(tU8 u8nADC, const tU8 *u8Data)
{
   tU32 u32Chn, u32Bytes;
   u32Bytes = u8nADC * ADC_INC_GETSAMPLE_BYTEPER_SAMPLE;
   if (padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Buffer_Update,
               "padc is NULL", 0, 0, 0, 0);
      return OSAL_E_INVALIDVALUE;
   }
   if (u8nADC > (padc->Num_channels))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Buffer_Update,
           "Received channel exceeds configured channels", 0, 0, 0, 0);
      return OSAL_E_INVALIDVALUE;
   }
   pthread_mutex_lock(&lock);
   memset(padc->buffer,'\0', sizeof(padc->buffer));
   memcpy(padc->buffer, u8Data, u32Bytes);
   for (u32Chn = 0; u32Chn < u8nADC; u32Chn++)
   {
      padc->ch[u32Chn].data_received = 0x01;
   }
   pthread_mutex_unlock(&lock);
   return OSAL_E_NOERROR;
}  

/**************************************************************************
* Function : dev_ADC_handle_rx_msg()
* Description: This function is to give a handle to the received data from 
               the server
* Parameters: char *Recvbuf
              Recvbuf - Buffer that stores the data obtained from the server
* Return value: s8Ret, OSAL error codes
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_handle_rx_msg(const char *Recvbuf)
{
   tS32 s32Ret = 0;
   if (padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Rx_Msg,
               "padc is NULL", 0, 0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   if ((Recvbuf[0] != SCC_PORT_EXTENDER_ADC_R_COMPONENT_STATUS_MSGID)&&
                                       (!padc->rsp_status_active))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Rx_Msg,
             "Discarding msg id", 0, 0, 0, 0);
      return s32Ret;
   }
   switch (Recvbuf[0])
   {
      case SCC_PORT_EXTENDER_ADC_R_COMPONENT_STATUS_MSGID:
         if ( Recvbuf[1] == ADC_INC_STATUS_ACTIVE )
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Handle_Rx_Msg,
              "ADC device is ready", 0, 0, 0, 0);
         }
         if (padc->init_pending)
         {
            s32Ret = dev_ADC_inc_getconfig();
            if (s32Ret != (tS32)OSAL_E_NOERROR)
            {
               DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Rx_Msg,
                 "Get config failed", 0, 0, 0, 0);
               return s32Ret;
            }
            padc->init_pending = 0;
         }
         padc->rsp_status_active = 0x01;
         pthread_cond_signal(&padc->cond3);
         break;
      case SCC_PORT_EXTENDER_ADC_R_GET_SAMPLE_MSGID:
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Handle_Rx_Msg,
               "Receive for number of channels : success", 0, 0, 0, 0);
         padc->Num_channels = Recvbuf[1];
         s32Ret = dev_ADC_inc_buffer_update(Recvbuf[1],(const char*)&Recvbuf[2]);
         if (s32Ret != (tS32)OSAL_E_NOERROR)
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Rx_Msg,
                 "Failure in buffer update", 0, 0, 0, 0);
            return s32Ret;
         } 
         pthread_cond_signal(&padc->cond1);
         break;
      case SCC_PORT_EXTENDER_ADC_R_SET_THRESHOLD_MSGID:
    	   s32Ret = dev_ADC_inc_handle_threshold_set_response(padc,
    			  Recvbuf[1],(const char*) &Recvbuf[2]);
    	   if (s32Ret != (tS32)OSAL_E_NOERROR)
    	   {
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Rx_Msg,
                "Failure in threshold_set_response", 0, 0, 0, 0);
    		   return s32Ret;
    	   }
    	   pthread_cond_signal(&padc->cond2);
         break;
      case SCC_PORT_EXTENDER_ADC_R_THRESHOLD_HIT_MSGID:
         s32Ret = dev_ADC_inc_handle_threshold_hit(padc, Recvbuf[1],
                                    Recvbuf[2], Recvbuf[3]);
         tU8 u8Ch = Recvbuf[1];
         if (s32Ret != (tS32)OSAL_E_NOERROR)
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Rx_Msg,
                "Failure in threshold_hit", 0, 0, 0, 0);
            return s32Ret;
         }
         s32Ret = dev_ADC_callback_execute_function(u8Ch);
         if (s32Ret != (tS32)OSAL_E_NOERROR)
         {
            return s32Ret;
         }
         break;
      default:
         s32Ret = (tS32)OSAL_E_INVALIDVALUE;
         DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_Handle_Rx_Msg,
                "Error in received message", 0, 0, 0, 0);
 		   return s32Ret;
   }
   return (tS32)OSAL_E_NOERROR;
}

/**************************************************************************
* Function : dev_ADC_inc_rx_task()
* Description: This is a seperate thread running especially to get the data 
               sent from the server.
* Parameters: Nil
* Return value: OSAL error codes 
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
tPVoid dev_ADC_inc_rx_task(tPVoid arg)
{
   tChar Recvbuf[1024];
   tS32 s32Ret = 0, s32RetVal = 0;
   do
   {
      s32Ret = recv(padc->socket_fd, Recvbuf, sizeof(Recvbuf), 0);
      if (s32Ret > 0)
      {
         s32RetVal = dev_ADC_handle_rx_msg((const tChar*)Recvbuf);
      }
   }while (s32Ret >= 0 && s32RetVal == (tS32)OSAL_E_NOERROR);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(arg);
   return OSAL_NULL;
}

/**************************************************************************
* Function : dev_ADC_socket_create()
* Description: Used for creation of socket so as to communicate with the server
* Parameters: Nil
* Return value: OSAL error codes 
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_socket_create()
{
   tS32 s32Ret;
   struct hostent *local;
   struct hostent *remote;
   struct sockaddr_in local_addr, remote_addr;   
   __CONST_SOCKADDR_ARG unionsockaddr;
   /* Establishing communication between adc_inc_client and the server  */
   local = gethostbyname("fake1");
   if (local == NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_socket_create,
               "Gethostbyname local failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   local_addr.sin_family = AF_INET;
   memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, (tU32)local->h_length);
   local_addr.sin_port = htons(PORT_EXTENDER_ADC_PORT); /* from inc_ports.h */
   remote = gethostbyname("fake1-local");
   if (remote == NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_socket_create,
               "Gethostbyname remote failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   remote_addr.sin_family = AF_INET;
   memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, (tU32)remote->h_length);
   remote_addr.sin_port = htons(PORT_EXTENDER_ADC_PORT); /* from inc_ports.h */
   s32Sock_FD = socket(AF_BOSCH_INC_LINUX, SOCK_STREAM, 0);
   if (-1 == s32Sock_FD)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_socket_create,
               "Failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   unionsockaddr.__sockaddr_in__ = &remote_addr;
   s32Ret = connect(s32Sock_FD,unionsockaddr, sizeof(remote_addr));
   if (s32Ret < 0)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_socket_create,
               "Connect failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      close(s32Sock_FD);
      return (tS32)u32ConvertErrorCore(errno);
   }
   return (tS32)OSAL_E_NOERROR;
}

/**************************************************************************
* Function : dev_ADC_resource_init()
* Description: Initialization of parameters in global structure adc_dev
* Parameters: Nil
* Return value: OSAL error codes 
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_resource_init()
{
   tS32 channel_index;
   if (padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_sharedmem_init,
               "padc is NULL", 0, 0, 0, 0);
      return OSAL_E_INVALIDVALUE;
   }
   (padc)->init_pending = 1;
   padc->socket_fd = s32Sock_FD;
   for(channel_index=0; channel_index<=255;channel_index++)
   {
      padc->ch[channel_index].channel_status = 0;
   }
   padc->CallBakLkpCntr = 0;
   return OSAL_E_NOERROR;
}

/**************************************************************************
* Function : dev_ADC_sharedmem_init()
* Description: Creation of shared memory for global structure
* Parameters: Nil
* Return value: OSAL error codes 
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_sharedmem_init()
{
   OSAL_tShMemHandle hshmemhandle;
   tS32 s32Retval = OSAL_OK;
   /* Creating shared memory*/
   hshmemhandle = OSAL_SharedMemoryCreate(DEV_ADC_SH_NAME, OSAL_EN_READWRITE,
                                          sizeof(struct adc_dev));
   /* Shared memory created */
   if(hshmemhandle != OSAL_ERROR)
   {
      padc = (struct adc_dev*)OSAL_pvSharedMemoryMap(hshmemhandle, OSAL_EN_READWRITE,
                                             sizeof(struct adc_dev),0);
      if(padc != NULL)
      {
         memset(padc,0,sizeof(struct adc_dev));
         Sharedmemhdl = hshmemhandle;
         if(OSAL_OK != CreateOsalLock(&padc->rADC_Lock,DEV_ADC_LOCK_NAME))
         {  
            s32Retval = OSAL_E_INVALIDVALUE;
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_sharedmem_init,
                          "Create lock failed",0,0,0,0);
         }
         /* Acquire the lock so the Initialization happens first */
         if((s32Retval == OSAL_OK) && (LockOsal(&padc->rADC_Lock) == OSAL_OK))
         {
            s32Retval = dev_ADC_resource_init();
            if(UnLockOsal(&padc->rADC_Lock) != OSAL_OK)
            {
               s32Retval = OSAL_E_INVALIDVALUE;
               DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_sharedmem_init,
                          "Unlock failed",0,0,0,0);
            }
         }
         else
         {
            s32Retval = OSAL_E_INVALIDVALUE;
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_sharedmem_init,
                          "Lock failed",0,0,0,0);
         }
         if(s32Retval == OSAL_OK)
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_sharedmem_init,
                          "Created successfully",0,0,0,0);
            s32Retval = OSAL_E_NOERROR;
         }         
      }
      else 
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_sharedmem_init,
                          "OSAL map failed",0,0,0,0);
         s32Retval = OSAL_E_INVALIDVALUE;
      }
   }
   else
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_sharedmem_init,
                          "Create failed",0,0,0,0);
      s32Retval = OSAL_E_NOSPACE;
   }
   return s32Retval;
}

/**************************************************************************
* Function : dev_ADC_callback_sharedmem_init()
* Description: Creation of shared memory for callback
* Parameters: Nil
* Return value: OSAL error codes 
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
static tS32 dev_ADC_callback_sharedmem_init()
{
   OSAL_tShMemHandle hshmemcallhandle;
   tS32 s32Retval = OSAL_E_NOERROR;
   /* Creating shared memory*/
   hshmemcallhandle = OSAL_SharedMemoryCreate(DEV_ADC_CALLBACK_SH_NAME, OSAL_EN_READWRITE,
                                          (ADC_MAX_CHANNELS*sizeof(struct channel_data)));
   /* Shared memory created */
   if(hshmemcallhandle != OSAL_ERROR)
   {
      callbacks = (struct channel_data*)OSAL_pvSharedMemoryMap(hshmemcallhandle, OSAL_EN_READWRITE,
                                             (ADC_MAX_CHANNELS*sizeof(struct channel_data)),0);
      if(callbacks != NULL)
      {
         memset(callbacks,0,sizeof(struct channel_data));
         Sharedmemhd2 = hshmemcallhandle;
      }
      else 
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_sharedmem_init,
                          "OSAL map failed for callback",0,0,0,0);
         s32Retval = OSAL_E_INVALIDVALUE;
      }
   }
   else
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_sharedmem_init,
                          "Create failed for callback",0,0,0,0);
      s32Retval = OSAL_E_NOSPACE;
   }
   return s32Retval;
}

/**************************************************************************
* Function : DEV_ADC_s32IODeviceInit()
* Description: Process/thread initialization of ADC (client)
* Parameters: Nil
* Return value: OSAL error codes 
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
***************************************************************************/
tS32 DEV_ADC_s32IODeviceInit()
{
   tS32 s32Ret;
   static struct timespec time_to_wait = {0, 0};
   PTHREAD_FUNC rx_task = dev_ADC_inc_rx_task;
   time_to_wait.tv_sec = time(NULL) + WAITTIME;
   pthread_mutex_init(&lock, NULL);
   /* create semaphore */
   if (OSAL_ERROR == OSAL_s32SemaphoreCreate(DEV_ADC_SEM_NAME, &SemHndl, 1))
   {
	   DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
                          "Error in sem create with error %",
                                 u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   pthread_mutex_lock(&lock);
   s32Ret = dev_ADC_socket_create();
   pthread_mutex_unlock(&lock);
   if(s32Ret != (tS32)OSAL_E_NOERROR)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Error in socket creation",0,0,0,0);
      return s32Ret;
   }
   pthread_mutex_lock(&lock);
   s32Ret = dev_ADC_sharedmem_init();
   pthread_mutex_unlock(&lock);
   if (s32Ret != (tS32)OSAL_E_NOERROR)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Error in shared memory creation",0,0,0,0);
      return s32Ret;
   }
   pthread_mutex_lock(&lock);
   s32Ret = dev_ADC_callback_sharedmem_init();
   pthread_mutex_unlock(&lock);
   if (s32Ret != (tS32)OSAL_E_NOERROR)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Error in shared memory creation for callback",0,0,0,0);
      return s32Ret;
   }
   pthread_mutex_lock(&lock);
   if (pthread_create(&tid, NULL, rx_task, NULL) != 0)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Thread creation failed",0,0,0,0);
      pthread_mutex_unlock(&lock);
      return (tS32)OSAL_E_THREAD_CREATE_FAILED;
   }
   else
   {
      pthread_mutex_unlock(&lock);
      pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
   }
   pthread_mutex_lock(&lock);
   s32Ret = dev_ADC_inc_cmd_status_active();
   pthread_mutex_unlock(&lock);
   if (s32Ret != (tS32)OSAL_E_NOERROR)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Cmd status failed",0,0,0,0);
      return s32Ret;
   }
   pthread_mutex_lock(&lock);
   pthread_cond_timedwait(&padc->cond3, &lock, &time_to_wait);
   pthread_mutex_unlock(&lock);
   return (tS32)OSAL_E_NOERROR;
}

/************************************************************************
* Function : DEV_ADC_s32IODeviceInit()
* Description: De-initializes the ADC(client)
* Parameters: Nil
* Return value: OSAL error codes
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
**********************************************************************/
tS32 DEV_ADC_s32IODeviceDeinit()
{
   pthread_mutex_lock(&lock);
   pthread_cancel(tid);
   pthread_mutex_unlock(&lock);
   /* close semaphore */
   if (OSAL_ERROR == OSAL_s32SemaphoreClose(SemHndl))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Semaphore close failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      OSAL_s32SemaphoreDelete(DEV_ADC_SEM_NAME);
      return (tS32)u32ConvertErrorCore(errno);
   }
   /* delete semaphore */
   if (OSAL_ERROR == OSAL_s32SemaphoreDelete(DEV_ADC_SEM_NAME))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Semaphore delete failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   SemHndl = OSAL_C_INVALID_HANDLE;
   OSAL_vMemoryFree(padc->buffer);
   padc->buffer = OSAL_NULL;
   if (s32Sock_FD)
   {
      close(s32Sock_FD);
      padc->socket_fd = 0;
   }
   if (OSAL_OK != OSAL_s32SharedMemoryUnmap(&Sharedmemhdl,sizeof(struct adc_dev)))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Shm unmap failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   if(OSAL_ERROR == OSAL_s32SharedMemoryClose(Sharedmemhdl))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Shm close failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   if (OSAL_OK != OSAL_s32SharedMemoryUnmap(&Sharedmemhd2,sizeof(struct channel_data)))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Callback Shm unmap failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   if(OSAL_ERROR == OSAL_s32SharedMemoryClose(Sharedmemhd2))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IODeviceInit,
               "Callback Shm close failed with error %",
                     u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   return OSAL_E_NOERROR; 
}

/************************************************************************
* Function : ADC_s32IOOpen()
* Description: This function opens ADC in LSIM
* Parameters: Channel ID
              DevicePath - path of device file to be opened
              u32FD - Device descriptor
* Return value: OSAL error codes 
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
**********************************************************************/
tS32 ADC_s32IOOpen(tCString szName,tS32 s32ID, tU32 *pu32FD)     
{
   tS32 s32Ch;
   s32Ch = atoi(szName+4);
   if(padc == NULL) 
   {
      /* Open the already created shared memory to access it*/
      Sharedmemhdl = OSAL_SharedMemoryOpen(DEV_ADC_SH_NAME, OSAL_EN_READWRITE);
      if(Sharedmemhdl != OSAL_ERROR)
      {
         padc = (struct adc_dev*)OSAL_pvSharedMemoryMap(Sharedmemhdl,
                     OSAL_EN_READWRITE,sizeof(struct adc_dev),0);
         if(padc == NULL)
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen,
               "Shm map failed at line : %d with error : %",
                  __LINE__, u32ConvertErrorCore(errno),0,0);
            return (tS32)u32ConvertErrorCore(errno);
         } 
      }
      else 
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen,
                  "Shm open failed",0,0,0,0);
         return (tS32)OSAL_E_INVALIDVALUE;
      }
   }
   if(callbacks == NULL) 
   {
      /* Open the already created shared memory to access it*/
      Sharedmemhd2 = OSAL_SharedMemoryOpen(DEV_ADC_CALLBACK_SH_NAME, OSAL_EN_READWRITE);
      if(Sharedmemhd2 != OSAL_ERROR)
      {
         callbacks = (struct channel_data*)OSAL_pvSharedMemoryMap(Sharedmemhd2,
                     OSAL_EN_READWRITE,ADC_MAX_CHANNELS*sizeof(struct channel_data),0);
         if(callbacks == NULL)
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen,
               "Callback Shm map failed at line : %d with error : %",
                  __LINE__, u32ConvertErrorCore(errno),0,0);
            return (tS32)u32ConvertErrorCore(errno);
         } 
      }
      else 
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen,
                  "Callback Shm open failed",0,0,0,0);
         return (tS32)OSAL_E_INVALIDVALUE;
      }
   }
   /* get semaphore */
   if (OSAL_ERROR == OSAL_s32SemaphoreOpen(DEV_ADC_SEM_NAME, &SemHndl))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen,
          "smphr open failed", 0, 0, 0, 0);
      return (tS32)OSAL_u32ErrorCode();
   }
   if ((s32Ch)&&(s32Ch < (padc->Num_channels)))
   {
      if (padc->ch[s32Ch].channel_status == 0)
      {
         padc->ch[s32Ch].channel_status = 1;
         *pu32FD = (tU32)s32Ch;
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOOpen,
         "The channel is opened successfully",0,0,0,0);
         return (tS32)OSAL_E_NOERROR;
      }
      else
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOOpen,
         "The channel is opened already",0,0,0,0);
         return (tS32)OSAL_E_ALREADYOPENED;
      }
   }
   else
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOOpen,
       "Invalid channel number",0,0,0,0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   return (tS32)OSAL_E_NOERROR;
}

/*****************************************************************************
* FUNCTION:     dev_ADC_callback_remove()
* PARAMETER:    No of Callback entries,file descriptor
* RETURNVALUE:  error codes

* DESCRIPTION:  removes the callback from the list of already registered callbacks.
* HISTORY:
------------------------------------------------------------------------------
05/03/12|Basavaraj Nagar| Adapted to Linux

******************************************************************************/
static tS32 dev_ADC_callback_remove(tU32 u32FD)
{
   tInt i;
   tS32 errVal=(tS32)OSAL_E_NOERROR;
   DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,"cbk_rem enter",
           0,0,0,0);
   if (callbacks == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_USER_4,enDEV_ADC_IOClose,"callbacks is NULL",
            0,0,0,0);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_INVALIDVALUE;
   }
   if(padc->CallBakLkpCntr==1)
   {
      if((callbacks)->fd==u32FD)
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
               "dealloc cbks",0,0,0,0);
         /* reset padc->CallBakLkpCntr */
         padc->CallBakLkpCntr=0;
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
            "cbk removed",padc->CallBakLkpCntr,0,0,0);
      }
   }
   else
   {
      for(i=0;i<padc->CallBakLkpCntr;i++)
      {
         if((callbacks+i)->fd==u32FD)
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
                  "null callback", 0,0, 0, 0);
            padc->CallBakLkpCntr--;
            OSAL_pvMemoryCopy(callbacks+i,callbacks+i+1,((padc->CallBakLkpCntr-i)*(sizeof(struct channel_data))));
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
               "removing",(callbacks+padc->CallBakLkpCntr)->fd,0, padc->CallBakLkpCntr, 0);
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
               "removed",(callbacks+padc->CallBakLkpCntr)->fd,0, padc->CallBakLkpCntr, 0);
         }
         if(i >= padc->CallBakLkpCntr)
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
               "No callback registered with this adc channel",0,0,0,0); 
         }
      }
   }
   return errVal;
}

/************************************************************************
* Function : ADC_s32IOClose()
* Description: This function opens ADC in LSIM
* Parameters: Nil* Parameters: Channel ID
              DevicePath - path of device file to be opened
              u32FD - Device descriptor
* Return value: s32Ret
* History:
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
**********************************************************************/
tS32 ADC_s32IOClose(tS32 s32ID, tU32 u32FD)
{
   tS32 s32Ret = OSAL_E_NOERROR;
   if(padc != NULL)
   {
      padc->ch[u32FD].channel_status = 0;
   }
   else
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOClose,
          "padc is NULL",0,0,0,0);
      return OSAL_E_INVALIDVALUE;
   }
   DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
             "The channel is closed successfully",0,0,0,0);
   if(padc->CallBakLkpCntr != 0)
   {
      s32Ret = dev_ADC_callback_remove(u32FD);
      if (s32Ret != (tS32)OSAL_E_NOERROR)
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_IOClose,
             "callback remove failed",0,0,0,0);
         return OSAL_E_INVALIDVALUE;
      }
   }
   if (OSAL_OK != OSAL_s32SharedMemoryUnmap(&Sharedmemhdl,sizeof(struct adc_dev)))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOClose,
               "Shm unmap failed",0,0,0,0);
      return (tS32)OSAL_u32ErrorCode();
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   return s32Ret;
}

/*****************************************************************************
* FUNCTION:     dev_ADC_callback_execute_function()
* PARAMETER:    u32FD - File descriptor
* RETURNVALUE:  OSAL error codes
* DESCRIPTION:  The Callback execute function will execute the callback for those 
                channels that are registered for callback already.
* HISTORY: 
* 13.01.2016 - Initial version for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
******************************************************************************/
static tS32 dev_ADC_callback_execute_function(tU32 u32FD)
{
   tU8 i;
   for(i = 0;i<padc->CallBakLkpCntr;i++)
   {
      /* execute callback function */
      if (((callbacks+i)->fd==u32FD) && (callbacks+i)->CallbackData.pfnCallbackFunction != OSAL_NULL)
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Callbackexec,
             "calling callbk function!!!",0,0,0,0);
         (callbacks+i)->CallbackData.pfnCallbackFunction();
      }
      else
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_Callbackexec,
             "no callbk function registered!!!",0, 0, 0, 0);
         return OSAL_E_INVALIDVALUE;
      }
   }
   return (tS32)OSAL_E_NOERROR;
}

/*****************************************************************************
* FUNCTION:     dev_ADC_Readfunction()
* PARAMETER:    pBuf - To load the values that are being read
                u32Size -no of blocks to be read ,ret_size - no of blocks
                that have read.
                u32FD - Device descriptor
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Reads the requested ADC Sub unit.
* HISTORY:
-----------------------------------------------------------------------------
Date:       | AUTHOR     |comments
----------------------------------------------------------------------------
28/07/14    | Dharmender Suresh Chander | Adapted to Linux
13/01/2016  | Chandra Sekaran Mariappan | Initial version for LSIM
******************************************************************************/
static tS32 dev_ADC_Readfunction(tU32 u32FD, tPS8 pBuf, tU32 u32Size)
{
   tS32 s32Ret;
   tU8 ch = (tU8)u32FD, Sendbuf[1] = {0};
   tU32 u32Ch_data_offset;
   static struct timespec time_to_wait = {0, 0};
   time_to_wait.tv_sec = time(NULL) + WAITTIME;
   Sendbuf[0] = SCC_PORT_EXTENDER_ADC_C_GET_SAMPLE_MSGID;
   if (padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IORead,
               "padc is NULL", 0, 0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   if (u32Size < ADC_INC_GETSAMPLE_BYTEPER_SAMPLE)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IORead,
                  "Allocate more bytes\n",0,0,0,0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   u32Ch_data_offset = ch * ADC_INC_GETSAMPLE_BYTEPER_SAMPLE;
   pthread_mutex_lock(&lock);
   padc->ch[ch].data_received = 0;
   pthread_mutex_unlock(&lock);
   s32Ret = send(padc->socket_fd, Sendbuf, sizeof(Sendbuf), 0);
   if (s32Ret < 0)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IORead,
                     "Send failed for ADC read with error %",
                           u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   pthread_mutex_lock(&lock);
   pthread_cond_timedwait(&padc->cond1, &lock, &time_to_wait);
   pthread_mutex_unlock(&lock);
   pthread_mutex_lock(&lock);
   memcpy((void *)pBuf, padc->buffer + u32Ch_data_offset, ADC_INC_GETSAMPLE_BYTEPER_SAMPLE);
   pthread_mutex_unlock(&lock);
   return (tS32)OSAL_E_NOERROR;
}

/*****************************************************************************
* FUNCTION:     ADC_s32IORead()
* PARAMETER:    Channel ID, buffer -to put converted data,
u32Size -no of blocks to be read ,ret_size - no of blocks
that have read.
u32FD - Device descriptor
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Reads the requested ADC Sub unit.
* HISTORY:
-----------------------------------------------------------------------------
Date:       | AUTHOR     |comments
----------------------------------------------------------------------------
28/07/14    | Dharmender Suresh Chander | Adapted to Linux
13/01/2016  | Chandra Sekaran Mariappan | Initial version for LSIM
******************************************************************************/
tS32 ADC_s32IORead(tS32 s32ID,tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   tS32 s32Ret;
   if(u32FD == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IORead,
                  "Invalid File descriptor passed",0,0,0,0);
      return ((tS32)OSAL_E_INVALIDVALUE);
   }
   s32Ret = dev_ADC_Readfunction(u32FD,pBuffer,u32Size);
   if (s32Ret != (tS32)OSAL_E_NOERROR)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IORead,
            "Failed",0,0,0,0);
      return ((tS32)OSAL_E_INVALIDVALUE);
   }
   else
   {
      *ret_size = ADC_INC_GETSAMPLE_BYTEPER_SAMPLE;
      DEV_ADC_vTraceInfo(TR_LEVEL_USER_4,enDEV_ADC_IORead,"Success",
                     0,0,0,0);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   return s32Ret;
}

/*****************************************************************************
* FUNCTION:     dev_ADC_SetCallback()
* PARAMETER:    File Descriptor,
s32Arg -Argument to be passed to function(Includes callback
type and callback function).
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Allows client to register and unregister the callback function
for different ADC events.
* HISTORY:
05/03/2013|Basavaraj Nagar(RBEI/ECG4)--written for Linux
******************************************************************************/
static tS32 dev_ADC_SetCallback(tU32 u32FD, tS32 s32Arg)
{
   tS32 s32RetVal =((tS32)OSAL_E_NOERROR);
   tInt i = 0;
   DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
                  "Entry",u32FD,0,0,0);
   if(s32Arg == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetCallback,
          "Arg is null",0,0, 0, 0);
      return ((tS32)OSAL_E_INVALIDVALUE);
   }
   if (padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IORead,
               "padc is NULL", 0, 0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   if (OSAL_ERROR == OSAL_s32SemaphoreWait(SemHndl, OSAL_C_TIMEOUT_FOREVER))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetCallback,
          "smphr wait failed with error %",
               u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
       "dereferencing cbk ",0,0, 0, 0);
   OSAL_trAdcSetCallback* padcsetcallback  = (OSAL_trAdcSetCallback *)s32Arg;
   OSAL_tenAdcCallbackType callback_type   = padcsetcallback->enCallbackType;
   if((padcsetcallback->pfnCallbackFunction == OSAL_NULL))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
         "cbkremove ",0,0, 0, 0);
      for(i=0;i<(padc->CallBakLkpCntr);i++)
      {
         if((callbacks+i)->fd==u32FD)
         {
            (callbacks+i)->CallbackData.pfnCallbackFunction=OSAL_NULL;
            DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
                 "deregister",0,0,0,0);
         }
      }
   }
   else
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
           "dereferenced successfully", 0,0, 0, 0);
      /*For Gen3, only one type of callback namely ADC_CALLBACK_THRESHOLD type is provided as of this version*/
      if((tS32)callback_type != AdcCallbackThreshold)
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetCallback,
              "invalid callback type", 0,0, 0, 0);
         return ((tS32)OSAL_E_INVALIDVALUE);
      }
      for(i=0;i<(padc->CallBakLkpCntr);i++)
      {
         if((callbacks+i)->fd==u32FD)
         {
            /* already registered? */
            if ((callbacks+i)->CallbackData.pfnCallbackFunction != OSAL_NULL)
            {
               DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
                    "already registered", u32FD,0, 0, 0);
               return (tS32)OSAL_E_ALREADYEXISTS;
            }
         }
      }
      DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
          "register callback",0,0, 0, 0);
      if(padc->CallBakLkpCntr==0)
      {
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
            "allocating memory",padc->CallBakLkpCntr,0, 0, 0);
         padc->CallBakLkpCntr++;
         if(callbacks==NULL)
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetCallback,
                "failed allocating memory",padc->CallBakLkpCntr,0, 0, 0);
            return (tS32)OSAL_E_NOSPACE;
         }
         (callbacks)->fd = u32FD;
         (callbacks)->CallbackData.pfnCallbackFunction = padcsetcallback->pfnCallbackFunction;
         DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetCallback,
              "allocated memory",0,padc->CallBakLkpCntr, 0, 0);
      }
      else
      {
         if(callbacks==NULL)
         {
            DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetCallback,
                  "Invalid callback",0,0, 0, 0);
            return (tS32)OSAL_E_NOSPACE;
         }
         (callbacks+padc->CallBakLkpCntr)->fd = u32FD;
         (callbacks+padc->CallBakLkpCntr)->CallbackData.pfnCallbackFunction = padcsetcallback->pfnCallbackFunction;
         padc->CallBakLkpCntr++;
      }
   }
   if (OSAL_ERROR == OSAL_s32SemaphorePost(SemHndl))
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetCallback,
           "smphr post failed",0,0, 0, 0);
      return (tS32)OSAL_u32ErrorCode();
   }
   return s32RetVal;
}
/*****************************************************************************
* FUNCTION: ADC_s32IOControl()
* PARAMETER: Channel ID, IO Control function,
             s32Arg - Argument to be passed to function.
             u32FD - Device descriptor
* RETURNVALUE: tS32 error codes
* HISTORY:
-----------------------------------------------------------------------------
Date:         | AUTHOR      |comments
-----------------------------------------------------------------------------
28/07/14    | Dharmender Suresh Chander | Adapted to Linux
13/01/2016  | Chandra Sekaran Mariappan | Initial version for LSIM
*****************************************************************************/
tS32 ADC_s32IOControl(tS32 s32ID,tU32 u32FD, tS32 s32Fun, tS32 s32Arg )
{
   tS32  s32RetVal = ((tS32)OSAL_E_NOERROR);
   if(s32Arg != OSAL_NULL)
   {
      switch(s32Fun)
      {
      case OSAL_C_S32_IOCTRL_ADC_SET_CALLBACK:
         {
            s32RetVal = dev_ADC_SetCallback(u32FD, s32Arg);
            break;
         }
      case OSAL_C_S32_IOCTRL_ADC_SET_THRESHOLD:
         {
            s32RetVal = dev_ADC_SetThreshold(u32FD, s32Arg);
            break;
         }
      case OSAL_C_S32_IOCTRL_ADC_GET_CONFIG:
         {
            s32RetVal= dev_ADC_GetResolution(s32Arg);
            break;
         }
         /*The following IOCTRLS are not supported for Gen3*/
      case OSAL_C_S32_IOCTRL_ADC_CONFIGURE_ADC:
      case OSAL_C_S32_IOCTRL_ADC_CONFIGURE_SCAN_GROUP:
      case OSAL_C_S32_IOCTRL_ADC_GET_SCAN_GROUP_INFO:
      case OSAL_C_S32_IOCTRL_ADC_CONFIGURE_LOGICAL_CHANNEL:
      case OSAL_C_S32_IOCTRL_ADC_SET_LIMVIO_INT_REG:
      case OSAL_C_S32_IOCTRL_ADC_SET_STATES_INT_MSK:
      case OSAL_C_S32_IOCTRL_ADC_SET_LIMVIO_INT_MSK:
      case OSAL_C_S32_IOCTRL_ADC_VC_GET_BLOCKSIZE:
      case OSAL_C_S32_IOCTRL_SADC_GET_BLOCK_SIZE:
         DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_IOControl,
               "unsupported IOCTRL",0,0,0,0);
         s32RetVal = ((tS32)OSAL_E_NOTSUPPORTED);
         break;
      default:
         {
            s32RetVal = ((tS32)OSAL_E_WRONGFUNC);
            break;
         }
      }/* End of switch */
   }
   else
   {
      s32RetVal = ((tS32)OSAL_E_INVALIDVALUE);
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
   return s32RetVal;
}

/*****************************************************************************
* FUNCTION: dev_ADC_GetResolution()
* PARAMETER: s32Arg - Structure 'OSAL_trAdcConfiguration'
             to hold attribute data of ADC, u32FD - Device descriptor.
* DESCRIPTION: Gets the Attribute data(Resolution) of  ADC.
* RETURNVALUE: s32RetVal
* HISTORY:    
13/01/2016  | Chandra Sekaran Mariappan | Initial version for LSIM
******************************************************************************/
static tS32 dev_ADC_GetResolution(tS32 s32Arg)
{
   tS32 s32RetVal = (tS32)OSAL_E_NOERROR;
   struct dev_resolution  *padc_get_resolution;
   if(s32Arg == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_GetResolution,
                  "Invalid argument",0,0,0,0);
      return ((tS32)OSAL_E_INVALIDVALUE);
   }
   if(padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_GetResolution,
                  "padc is NULL",0,0,0,0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   padc_get_resolution = (struct dev_resolution *)s32Arg;
   padc_get_resolution->adc_resolution = padc->resolution;
   DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_GetResolution,
               "ioctl successful",0,0,0,0);
   return s32RetVal;
}

/*****************************************************************************
* FUNCTION: dev_ADC_SetThreshold()
* PARAMETER: u32FD - Device descriptor
             s32Arg - pointer to OSAL_trAdcSetThreshold
             OSAL_trAdcSetThreshold -> includes the following elements.
             Threshold value & Comparision flag
* RETURNVALUE: tS32 error codes
* DESCRIPTION: This functions sets threshold value for a particular channel
* HISTORY:
13/01/2016  | Chandra Sekaran Mariappan | Initial version for LSIM
******************************************************************************/
static tS32 dev_ADC_SetThreshold(tU32 u32FD, tS32 s32Arg)
{
   OSAL_trAdcSetThreshold*  padcsetthreshold;
   struct dev_threshold devADCsetthreshold;
   tS32 s32RetVal;
   tU8 u8Ch = (tU8)u32FD;
   DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetThreshold,
        "entry",u32FD,0,0,0);
   if(s32Arg == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetThreshold,
            "invalid argument",u32FD,0,0,0);
      return ((tS32)OSAL_E_INVALIDVALUE);
   }
   padcsetthreshold = (OSAL_trAdcSetThreshold *) s32Arg;
   devADCsetthreshold.threshold =  padcsetthreshold->u16Threshold;
   devADCsetthreshold.encomparision = (enum devadc_comparision)padcsetthreshold->enComparison;
   DEV_ADC_vTraceInfo(TR_LEVEL_USER_4, enDEV_ADC_SetThreshold,
        "dereferenced",u32FD,0,0,0);
   s32RetVal = dev_ADC_inc_set_threshold
            ((const struct dev_threshold*)&devADCsetthreshold, u8Ch);
   if(s32RetVal != (tS32)OSAL_E_NOERROR)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetThreshold,
                     "ioctl operation unsuccessful",0,0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   return s32RetVal;
}

/*****************************************************************************
* FUNCTION: dev_ADC_inc_set_threshold()
* PARAMETER: threshold_data - Structure containing threshold value and comparision
             u8Ch - Channel
* RETURNVALUE: OSAL error codes
* DESCRIPTION: This functions sets threshold value for a particular channel
* HISTORY:
13/01/2016  | Chandra Sekaran Mariappan | Initial version for LSIM
******************************************************************************/
static tS32 dev_ADC_inc_set_threshold(const struct dev_threshold *threshold_data, tU8 u8Ch)
{
   tS32 s32Ret;
   tU8 th_lowbyte = (0x00ff & threshold_data->threshold);
   tU8 th_highbyte = ((0xff00 & threshold_data->threshold) >> 8);
   tU8 Sendbuf[5] = {0}, comparision = 0;
   static struct timespec time_to_wait = {0, 0};
   time_to_wait.tv_sec = time(NULL) + WAITTIME;
   if (padc == OSAL_NULL)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetThreshold,
               "padc is NULL", 0, 0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   if (threshold_data->encomparision == (enum devadc_comparision)devadc_threshold_lmt)
   {
      comparision = ADC_INC_THRESHOLD_LOWER_LIMIT;
   }
   else if (threshold_data->encomparision == (enum devadc_comparision)devadc_threshold_upt)
   {
      comparision = ADC_INC_THRESHOLD_UPPER_LIMIT;
   }
   else
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetThreshold,
                     "Unsuccessful",0 ,0, 0, 0);
      return (tS32)OSAL_E_INVALIDVALUE;
   }
   Sendbuf[0] = SCC_PORT_EXTENDER_ADC_C_SET_THRESHOLD_MSGID;
   Sendbuf[1] = u8Ch;
   Sendbuf[2] = comparision;
   Sendbuf[3] = th_lowbyte;
   Sendbuf[4] = th_highbyte;
   pthread_mutex_lock(&lock);
   padc->ch[u8Ch].set_threshold = 0x00;
   pthread_mutex_unlock(&lock);
   s32Ret = send(padc->socket_fd, Sendbuf, sizeof(Sendbuf), 0);
   if (s32Ret < 0)
   {
      DEV_ADC_vTraceInfo(TR_LEVEL_FATAL, enDEV_ADC_SetThreshold,
               "Send failed for cmd_status_active with error : %",
                  u32ConvertErrorCore(errno),0,0,0);
      return (tS32)u32ConvertErrorCore(errno);
   }
   pthread_mutex_lock(&lock);
   pthread_cond_timedwait(&padc->cond2, &lock, &time_to_wait);
   pthread_mutex_unlock(&lock);
   return OSAL_E_NOERROR;
}
