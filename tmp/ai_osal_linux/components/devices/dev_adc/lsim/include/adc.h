/******************************************************************************
*****     (C) COPYRIGHT Robert Bosch GmbH CM-DI - All Rights Reserved     *****
******************************************************************************/
/*! 
***     @file      drv_adc.h
***     @Authors
\n                  Dharmender Suresh Chander
***
***     @brief       implementation of adc in OSAL
***
***
***     @warning     
***
***     @todo        
*** --------------------------------------------------------------------------
\par  VERSION HISTORY:
**  
*    $Log: 28.07.2014 - Initial version
**         13.01.2016 - Adapted for LSIM - Chandra Sekaran Mariappan (RBEI/ECF5)
*\n*/
/*****************************************************************************/

#ifndef _DRV_ADC_H
#define _DRV_ADC_H

/*=============================================================================
=======                            INCLUDES                             =======
=============================================================================*/

#include "Linux_osal.h"

/*=============================================================================
=======               DEFINES & MACROS FOR GENERAL PURPOSE              =======
=============================================================================*/

/*the total no of adc channels available ...see osiopublic.h*/

#define ADC_VERBOSE_TRACE
#define SCC_PORT_EXTENDER_ADC_C_COMPONENT_STATUS_MSGID	 0x20
#define SCC_PORT_EXTENDER_ADC_R_COMPONENT_STATUS_MSGID   0x21
#define SCC_PORT_EXTENDER_ADC_R_REJECT_MSGID		 0x0B
#define SCC_PORT_EXTENDER_ADC_C_GET_SAMPLE_MSGID 	 0x40
#define SCC_PORT_EXTENDER_ADC_R_GET_SAMPLE_MSGID         0x41
#define SCC_PORT_EXTENDER_ADC_C_SET_THRESHOLD_MSGID	 0x42
#define SCC_PORT_EXTENDER_ADC_R_SET_THRESHOLD_MSGID	 0x43
#define SCC_PORT_EXTENDER_ADC_R_THRESHOLD_HIT_MSGID	 0x45
#define SCC_PORT_EXTENDER_ADC_C_GET_CONFIG_MSGID	 0x46
#define SCC_PORT_EXTENDER_ADC_R_GET_CONFIG_MSGID	 0x47
#define ADC_INC_STATUS_ACTIVE         0x01
#define ADC_INC_RESOLUTION_10BIT      0x0a

/*=============================================================================
=======                       CONSTANTS  &  TYPES                       =======
=============================================================================*/

typedef enum 
{
	ADC_TRC_FN_INFO=0,
	ADC_TRC_FN_STRING
}enADCTraceMessages;

typedef enum 
{
   enDEV_ADC_IOOpen=0,
   enDEV_ADC_IOClose,
   enDEV_ADC_IORead,
   enDEV_ADC_IOControl,
   enDEV_ADC_Handle_Threshold_Hit,
   enDEV_ADC_Handle_Threshold_Set_Response,
   enDEV_ADC_Cmd_status_active,
   enDEV_ADC_Getconfig,
   enDEV_ADC_Buffer_Update,
   enDEV_ADC_Handle_Rx_Msg,
   enDEV_ADC_Rx_task,
   enDEV_ADC_socket_create,
   enDEV_ADC_sharedmem_init,
   enDEV_ADC_IODeviceInit,
   enDEV_ADC_Callbackexec,
   enDEV_ADC_SetCallback,
   enDEV_ADC_GetResolution,
   enDEV_ADC_SetThreshold,
} enDevADCTraceFunction;

//Since Kernel datatypes u16 and u8 are not recognised in user space
typedef u_int8_t u8;
typedef u_int16_t u16;

struct dev_resolution {
	tU8 adc_resolution;
};
//callback funtion for ADC DRV
typedef void (* ADC_CallbackFunction)(void);

typedef enum 
{
	/* Callbacks with this identifier will be called when a threshold interrupt 
	is received. The callback function has to be installed for a specific 
	logical channel. */
	AdcCallbackThreshold = 3
}ADC_CallbackType;

typedef struct
{
	/* In which situation should the callback function be called? */
	ADC_CallbackType enCallbackType;
	/* The function to call */
	ADC_CallbackFunction pfnCallbackFunction;
} ADC_SetCallback;

/*Below structures used for tracking files for whom callback functions have 
been activated and deregister them during application(or file descriptor)close*/
struct channel_data
{
    tU32 fd;
    ADC_SetCallback CallbackData;
};

/*The callbacks registered/unregistered*/
struct channel_data *callbacks=OSAL_NULL;

/* Comparision value */
enum adc_comparision {
	devadc_threshold_lmt = 0,
	devadc_threshold_upt,
};

/* Structure for channel attributes */
struct channel{
   tU8 threshold;
   tU8 channel_status;
   tU8 set_threshold;
   tU8 threshold_hit;
   tU8 data_received;
   pthread_cond_t cond;
   enum adc_comparision comparision ;
};

/* Global structure adc _dev which contains seperate structure for each channels */
struct adc_dev {
   unsigned char Num_channels;
   tU8 init_pending;
   tU8 resolution;
   tU8 *buffer;
   tS32 socket_fd;
   struct channel ch[256];
   tU8 CallBakLkpCntr;
   trOsalLock rADC_Lock;
   tU8 rsp_status_active;
   pthread_cond_t cond1;   
   pthread_cond_t cond2;   
   pthread_cond_t cond3;   
};
struct adc_dev *padc = OSAL_NULL;

/*=============================================================================
=======                    PROTOTYPES OF PUBLIC FUNCTIONS               =======
=============================================================================*/

tS32 ADC_s32IOOpen(tCString szName,tS32 s32ID, tU32 *pu32FD);   
tS32 ADC_s32IOClose(tS32 s32ID, tU32 u32FD);
tS32 ADC_s32IOControl(tS32 s32ID,tU32 u32FD, tS32 s32Fun, tS32 s32Arg);
tS32 ADC_s32IORead(tS32 s32ID, tU32 u32FD,tPS8 pBuffer, tU32 u32Size, tU32 *ret_size);

#endif
/* EOF */
