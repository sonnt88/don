#ifndef DRV_BT_ASIP_PROTOCOL_H
#define DRV_BT_ASIP_PROTOCOL_H

#ifdef __cplusplus
extern "C" {
#endif
//this define enables the support for the SPP-interface
#define BT_UGZZC_ENABLED
#define BT_SPP_ENABLED

#define DRV_BT_SEM_NAME				"DrvBtSem"
#define DRV_BT_WRITE_SEM_NAME		"DrvBtWrS"
#define DRV_BT_THREAD_NAME			"DrvBtThr"

#define ASIP_ID_HOST		0xA0
#define ASIP_ID_UNIT		0xD0

#define ASIP_CHECK_ERROR_ID         0x01
#define ASIP_CHECK_ERROR_CHECKSUM   0x02
#define ASIP_CHECK_ERROR_CRC        0x04

#define ASIP_ERROR         0xFFFFFFFF
#define ASIP_INIT          0xFFFFFFF1
#define ASIP_OK            0x00000000
#define ASIP_DUN_OK        0x00000001
#define ASIP_SPP_OK			ASIP_OK

#define ASIP_ACK				0x01
#define ASIP_RTR				0x02
#define ASIP_CRC				0x08
#define ASIP_SEQRESET		0x80

#define ASIP_HEADER_SIZE			0x04
#define ASIP_CRC_SIZE				0x02
#define ASIP_PAYHEAD_SIZE			0x03
#define ASIP_PAYLOAD_DATA_SIZE 	0x40F

#define ASIP_PAYLOAD_DATA_START_POS ASIP_HEADER_SIZE+ASIP_PAYHEAD_SIZE
#define ASIP_PAYLOAD_START_POS      ASIP_HEADER_SIZE

#define MAX_SEQ_NUM			0x07
#define MAX_PAYLOAD_SIZE	ASIP_PAYLOAD_DATA_SIZE + ASIP_PAYHEAD_SIZE

#define BT_APPL_DUN_DATA_IND              0x7101
#define BT_APPL_DUN_DATA_COMP_IND         0x7102
#define BT_APPL_DUN_DATA_SEND_FC_IND      0x7103
#define BT_APPL_DUN_SEND_DATA_REQ         0x7001

#define BT_TEST_IF_FRAME							0x030a

#define BT_APPL_DEVICE_CONNECT					0x0407
#define BT_APPL_DEVICE_DISCONNECT				0x0409

#define BT_APPL_SPP_DATA_CFM          			0x6200
#define BT_APPL_SPP_CON_CONFIG_CFM    			0x6201
#define BT_APPL_SPP_BAUDRATE_SETTING_CFM		0x6202
#define BT_APPL_SPP_AUDIO_CONNECT_CFM			0x6203
#define BT_APPL_SPP_AUDIO_DISCONNECT_CFM		0x6204
#define BT_APPL_SPP_SET_UUID_CFM					0x6205

#define BT_APPL_SPP_DATA_IND						0x6300
#define BT_APPL_SPP_LINE_STATUS_IND				0x6301
#define BT_APPL_SPP_BAUDRATE_SET_RESULT_IND	0x6302
#define BT_APPL_SPP_AUDIO_CONNECT_IND			0x6303
#define BT_APPL_SPP_AUDIO_DISCONNECT_IND		0x6304

#define BT_APPL_SPP_EVTRES_SUCCESS						0x00
#define BT_APPL_SPP_EVTRES_ILLEGAL_FORMAT				0x02
#define BT_APPL_SPP_EVTRES_ILLEGAL_PARAM				0x03
#define BT_APPL_SPP_EVTRES_OPERATION_FAIL				0x04
#define BT_APPL_SPP_EVTRES_ILLEGAL_STATE				0x07
#define BT_APPL_SPP_EVTRES_SERVICE_NOT_CONNECTED	0x1F

#define BT_TX_MSG_TIMEOUT                 300
#define BT_TX_MAX_RETRANSMIT_COUNT        3

#define BT_NORMAL_MODE                    0
#define BT_TEST_MODE_CRC_FAILURE          1
#define BT_TEST_MODE_CHECKSUM_FAILURE     2
#define BT_TEST_MODE_SEQ_FAILURE          3
#define BT_TEST_MODE_LEN_FAILURE          4
#define BT_TEST_MODE_IGNORE_MSG           5
#define BT_TEST_MODE_SEQ_INIT             6
#define BT_TEST_MODE_RX_RCV_ERR           7
#define BT_TEST_MODE_RX_RCV_ACK_ERR       8
#define BT_RETRANSMIT                    10
#define BT_FORCE_SEND                    11
#define BT_RETRANSMIT_SEQ_RST            12

//==============================================================
// ASIP Header access function
//==============================================================

extern tBool _bDrvClosed;
extern OSAL_tThreadID _hDrvBtThreadID;

extern tS32 drv_bt_lld_uart_write(int comport, tU8* buf, tU16 len, tU32 timeout);
extern tS32 drv_bt_lld_uart_read(int comport, tU8* buf, tU16 len, tU32 timeout);
extern tVoid drv_bt_lld_uart_flushBuffer_Ugzzc(tVoid);

extern tBool drv_bt_SPP_UUID_query(tU8* pBuf);//, tU16 u16Len)
tVoid v_drv_bt_SPPdeviceConnect(tU16 tU16_Opcode, tU16 u16lenght, tU8* rxBuf);

//-------------------------------------------------
//	write function
//-------------------------------------------------
tVoid asipClearHeader(tU8 *buf);
tVoid asipSetHeaderID(tU8 *buf);
tVoid asipSetRtr(tU8 *buf, tU8 u8Rtr);
tVoid asipSetHeaderAck(tU8 *buf,tU8 ucAck);
tVoid asipSetHeaderPayLen(tU8 *buf,tU16 uslen);
tVoid asipSetHeaderCRC(tU8 *buf,tU8 ucCRC);
tVoid asipSetHeaderSeqNo(tU8 *buf,tU8 ucSeq);
tVoid asipSetHeaderSeqReset(tU8 *buf,tU8 ucSeqReset);
tVoid asipSetHeaderChksum(tU8 *buf);
tVoid asipClearPayload(tU8 *buf);
tVoid asipSetPayloadData( tU8 *buf , tU8 *data , tU16 uslen );
tVoid asipClearPayloadDDummy( tU8 *buf );
tVoid asipSetPayloadChannel(tU8 *buf , tU8 ch );
tVoid asipSetCRC( tU8 *buf );
tU8 asipNextSeqNo( tU8 ucSeqNo );
tU8 asicPrevSeqNo( tU8 ucSeqNo );

//-------------------------------------------------
//	read & check function
//-------------------------------------------------
tU16 asipGetPacketSize(tU8 *buf);
tU8 asipChkHeaderRemoteID( tU8 *buf);
tU8 asipGetHeaderAck(tU8 *buf );
tU16 asipGetHeaderPayLen(tU8 *buf );
tU8 asipGetHeaderCRC(tU8 *buf );
tU8 asipGetHeaderSeqNo(tU8 *buf );
tU8 asipGetHeaderSeqReset(tU8 *buf );
tU32 asipChkHeaderChksum(tU8 *buf);
tU16 asipGetDataLen(tU8 *buf);
tU8 asipGetDataChannel( tU8 *buf);
tU16 asipGetDataOpcode( tU8 *buf);
tU8 *asipGetDataAddr(tU8 *buf);
tU16 asipChkPktCRC(tU8 *buf);
tU8 asipGetRtr(tU8 *buf);

tS32 s32DrvBtAsipSendData( tU8 *txBuf, tU32 len, tU8 u8Mode);
tS32 s32DrvBtAsipRcvDataSync( tU8 *rxBuf, tU32 u32Len);
tS32 s32DrvBtAsipRcvData( tU8 *rxBuf, tU32 u32Len);
tBool bDrvBtAsipInit(tVoid);
tBool bDrvBtAsipDeInit(tVoid);
tVoid vCreateThread(tVoid);

tVoid vResetUgzzc(tBool bHciMode);
tVoid vFlushRx(tVoid);
tVoid vCheckMsgError(tU8 u8Error);

#ifdef __cplusplus
}
#endif

#endif  //DRV_BT_ASIP_PROTOCOL_H
