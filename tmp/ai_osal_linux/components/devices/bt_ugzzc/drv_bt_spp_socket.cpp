/*****************************************************************************
* Copyright (C) Bosch Car Multimedia GmbH, 2010
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
*****************************************************************************/
/*!
*\file     drv_bt_new.cpp
*\brief    This file contains the code for the DRV_BT module    
*
*\author:  Bosch Car Multimedia GmbH, CM-AI/PJ-VW32, Bosse Arndt, 
*                                       external.bosse.arndt@de.bosch.com
*\par Copyright: 
*(c) 2010 Bosch Car Multimedia GmbH
*\par History:
* Created - 12/03/12 
**********************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h> 

/* OSAL Interface */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "drv_bt_trace.h"
#include "drv_bt_asip_protocol.h"
#include "drv_bt_spp.h"
#include "drv_bt_spp_socket.h"

#ifdef BT_SPP_ENABLED
/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#ifndef DRV_BT_START_BYTE_FOR_SPP_DATA
#define DRV_BT_START_BYTE_FOR_SPP_DATA    6
#endif
#define BUFFERSIZE 500
#define SOCKET_PORT 40000

#define OSALTHREAD_C_U32_PRIORITY_MID 60
#define OSALTHREAD_C_U32_STACKSIZE_NORMAL 10000

#define drv_bt_vPrintf printf
// #define DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
#define DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
#define DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING_2
//#define DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING_IDLE
// #define DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_NOTWORKING
// #define DEBUG_VCREATETHREAD_SPP_SOCKETSERVER
/************************************************************************
| variable inclusion  (scope: global)
|-----------------------------------------------------------------------*/
extern tBool _bDrvBtTerminate;
extern tBool _bDrvBtSocketWork;

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
OSAL_tThreadID _hDrvBtThreadID_SPPSS = OSAL_ERROR;
tS32 s32DrvBt_newsockfd = 0;

tVoid dwThreadDrvBt_SPP_Socketserver( tPVoid pvArg ) {
   (tVoid) pvArg;
	
	tU32 u32_readed_bytes;
	socklen_t clilen;
	tU8 tu8_buffer[BUFFERSIZE];
	struct sockaddr_in serv_addr, cli_addr;
	struct in_addr addr;
	tS32 s32DrvBt_sockfd;
	
	s32DrvBt_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (s32DrvBt_sockfd < 0) {
		#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
		drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: ERROR at opening socket: %x\033[0m",errno);
		#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
		goto drv_bt_error;
	}
	#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
	else {
		drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: socketid: %u\033[0m\n",s32DrvBt_sockfd);
	}
	#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
	
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
	drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: serv_addr.sin_family: %u\033[0m\n",serv_addr.sin_family);
	#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
	
	if (inet_aton("127.0.0.1", &addr) == 0){
		#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
		drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: ERROR in inet_aton: '127.0.0.1' to \033[0m\n");
		#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
		goto drv_bt_error;
	}	
	serv_addr.sin_addr.s_addr = addr.s_addr;
	#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
	drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: serv_addr.sin_addr.s_addr: %u\033[0m\n",serv_addr.sin_addr.s_addr);
	#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
	serv_addr.sin_port = htons(SOCKET_PORT);
	#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
	drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: serv_addr.sin_port: %u\033[0m\n",serv_addr.sin_port);
	#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
	if (bind(s32DrvBt_sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0){
		#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
		drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: ERROR at binding: %x\033[0m",errno);
		#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
		goto drv_bt_error;
	} else {
		#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
		drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: binding worked\033[0m\n");	
		#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_STARTUP
	}
	while (!_bDrvBtTerminate){
		if (_bDrvBtSocketWork){
			#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
			drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: start listening\033[0m\n");
			#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
			listen(s32DrvBt_sockfd,3); //second argument is the size of pending connections 
			clilen = sizeof(cli_addr);
			while (!_bDrvBtTerminate && _bDrvBtSocketWork){			
				s32DrvBt_newsockfd = accept(s32DrvBt_sockfd, (struct sockaddr *) &cli_addr, &clilen);
				if (s32DrvBt_newsockfd < 0){
					#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
					drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: ERROR on acception: %x\033[0m\n",errno);
					#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
					goto drv_bt_error;
				} else {
					#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
					drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: acception worked\033[0m\n");
					#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
					bzero(tu8_buffer,BUFFERSIZE);
					u32_readed_bytes = read(s32DrvBt_newsockfd,tu8_buffer,BUFFERSIZE);
					//u32_readed_bytes = recv(s32DrvBt_newsockfd,tu8_buffer,BUFFERSIZE,MSG_WAITALL);
					if (u32_readed_bytes < 0){
						#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
						drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: ERROR reading from socket: %x\033[0m",errno);
						#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
						goto drv_bt_error;
					} else if (u32_readed_bytes > 0) {
						#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
						drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: readed bytes from socket: %u\033[0m\n",u32_readed_bytes);
						#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
						v_drv_bt_SendBtDataViaSPP(tu8_buffer, u32_readed_bytes);
						#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING_2
						drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: readed bytestream passed to the socket: \033[0m:");
						for (int i = 0; i < u32_readed_bytes ; i++ ){
							printf("\033[33m %02x\033[0m",tu8_buffer[i]);
						}
						printf("\n");
						#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING_2
					} else {
						#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING_IDLE
						drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: No data read from socket\033[0m\n");
						#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING_IDLE
						OSAL_s32ThreadWait(1000);
					}
				}
				OSAL_s32ThreadWait(1000);
				close(s32DrvBt_newsockfd);
				#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
				drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: closing socketconnection to client\033[0m\n");
				drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: stopped reading bytes\033[0m\n");
				#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
			}
		} else {
			#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_NOTWORKING
			drv_bt_vPrintf("DrvBt: \033[32mdwThreadDrvBt_SPP_Socketserver: not listening\033[0m\n");
			#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_NOTWORKING
			OSAL_s32ThreadWait(3000);
		}
	}
drv_bt_error:
	#ifdef DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
	drv_bt_vPrintf("DrvBt: \033[31mdwThreadDrvBt_SPP_Socketserver: ERROR occured Thread is exiting\033[0m\n");
	#endif //DEBUG_DWTHREADDRVBT_SPP_SOCKETSERVER_WORKING
	close(s32DrvBt_newsockfd);
	close(s32DrvBt_sockfd);
   OSAL_vThreadExit();
   return;
}

tVoid vCreateThread_SPP_Socketserver() {
	#ifdef DEBUG_VCREATETHREAD_SPP_SOCKETSERVER
	drv_bt_vPrintf("DrvBt: \033[31mvCreateThread_start_SPP_Socketserver\033[0m\n");
	#endif //DEBUG_VCREATETHREAD_SPP_SOCKETSERVER
   if (_hDrvBtThreadID_SPPSS == OSAL_ERROR) {
      OSAL_trThreadAttribute  rThAttr;

      //rThAttr.szName       = DRV_BT_THREAD_NAMESPP;
      OSAL_szStringNCopy(rThAttr.szName,DRV_BT_THREAD_NAME,sizeof(DRV_BT_THREAD_NAME));
      rThAttr.u32Priority  = OSALTHREAD_C_U32_PRIORITY_MID;
      rThAttr.s32StackSize = OSALTHREAD_C_U32_STACKSIZE_NORMAL;
      rThAttr.pfEntry      = (OSAL_tpfThreadEntry)dwThreadDrvBt_SPP_Socketserver;
      rThAttr.pvArg        = (tPVoid)NULL;

      // create thread suspended
      _hDrvBtThreadID_SPPSS = OSAL_ThreadSpawn( &rThAttr );
		#ifdef DEBUG_VCREATETHREAD_SPP_SOCKETSERVER
		drv_bt_vPrintf("DrvBt: \033[31mthread created ID:\033[0m %u\n",_hDrvBtThreadID_SPPSS);
		#endif //DEBUG_VCREATETHREAD_SPP_SOCKETSERVER
	}
	return;
}
#endif //BT_SPP_ENABLED
