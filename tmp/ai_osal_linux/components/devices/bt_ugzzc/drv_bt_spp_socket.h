/*******************************************************************************
*
* FILE:
*     drv_bt_spp.h
*
* REVISION:
*     1.0
*
* AUTHOR:
*     (c) 2012, Bosch Car Multimedia GmbH, CM-AI/PJ-VW32, Bosse Arndt, 
*                                                  extrenal.bosse.arndt@de.bosch.com
*
* CREATED:
*     01/02/2012 - Bosse Arndt
*
* DESCRIPTION:
*     This file contains all the definitions, macros, and types that 
*     are private to the drv_bt_spp.cpp. 
*
* NOTES:
*
* MODIFIED:
*
*******************************************************************************/
#ifndef DRV_BT_NEW_H
#define DRV_BT_NEW_H

#ifdef __cplusplus
extern "C" {
#endif

#define DRV_BT_THREAD_NAMESPP		"DrvBtThrSPPSS"
extern tVoid vCreateThread_SPP_Socketserver(tVoid);
extern tBool _bDrvBtTerminate;
extern tBool _bDrvBtSocketWork;
#ifdef __cplusplus
}
#endif

#endif //DRV_BT_NEW_H
