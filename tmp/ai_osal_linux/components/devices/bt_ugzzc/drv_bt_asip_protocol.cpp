/*****************************************************************************
* Copyright (C) Bosch Car Multimedia GmbH, 2010
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
*****************************************************************************/
/*!
*\file     drv_bt_asip_protocol.c
*\brief    This file contains the code for the DRV_BT module
*
*\author:  Bosch Car Multimedia GmbH, CM-AI/PJ-V32, Klaus-Peter Kollai,
*                                       Klaus-Peter.Kollai@de.bosch.com
*\par Copyright:
*(c) 2010 Bosch Car Multimedia GmbH
*\par History:
* Created - 23/07/10
* Changed BOA2HI - 02/02/12 added SPP-frames handling
**********************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

/* OSAL Interface */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "drv_bt_trace.h"
#include "drv_bt_asip_calc.h"
#include "drv_bt_asip_protocol.h"
#include "drv_bt_lld_uart.h"
#include "drv_bt_spp.h"
#include "drv_bt_spp_socket.h"

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define UART_4 0
#define OSALTHREAD_C_U32_PRIORITY_MID 60
#define OSALTHREAD_C_U32_STACKSIZE_NORMAL 10000

#define os_printf_errmem printf
#define drv_bt_vPrintf printf
//#define DEBUG_BT_SPP_ENABLED
//#define DEBUG_DWTHREADDRVBT
//#define DEBUG_VCREATETHREAD
/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable inclusion  (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

//==============================================================
// ASIP Header access function
//==============================================================

tU8 gu8AsipTxSeqNo=0;
tU8 gu8AsipRxSeqNo=0;
tU8 *gpu8AsipTxBuf = NULL;
tU32 gu32AsipTxLen=0;
tU32 gu32TxTimestamp = 0xffffffff;
tU8 *gpu8AsipRxBuf = NULL;
tU32 gu32AsipRxLen=0;
tU8 gu8OwnID=ASIP_ID_HOST;
tU8 gu8CRC=1;
tU8 gu8TxSendCounter = 0;
tU32 gu32TxMsgCount = 0;
tU32 gu32RxMsgCount = 0;
tU16 gu16RxLastOpcode = 0;
tU16 gu16TxLastOpcode = 0;
tU32 gu32NumberSkippedMsg = 0;
tU32 gu32RxMsg2AppCount = 0;
tU32 gu32RxMsg4AppCount = 0;

tBool _bUgzzcInReset = TRUE;

tU8 _u8TxBuf[ASIP_HEADER_SIZE + MAX_PAYLOAD_SIZE] = {0};
tU8 _u8RxBuf[ASIP_HEADER_SIZE + MAX_PAYLOAD_SIZE] = {0};

tU8 _au8DrvBtRxBuf[2][ASIP_HEADER_SIZE + MAX_PAYLOAD_SIZE] = {{0},{0}};
tU32 _u32RcvData = 0;
tBool _bDrvBtTerminate = FALSE;
OSAL_tThreadID _hDrvBtThreadID = OSAL_ERROR;

tBool _bDrvClosed = TRUE;
tBool _bIgnoreResetTrigger = FALSE;

tU8 u8TxTestMode = BT_NORMAL_MODE;

OSAL_tSemHandle _hDrvBtSemId = OSAL_C_INVALID_HANDLE;
OSAL_tSemHandle _hDrvBtWriteSemId = OSAL_C_INVALID_HANDLE;

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
tBool bEnterCritical(OSAL_tSemHandle hSemId, tU32 u32Timeout) {
   tBool bRet = TRUE;
   if ( OSAL_s32SemaphoreWait( hSemId, u32Timeout) != OSAL_OK ) {
      bRet = FALSE;
   }
   return bRet;
}


tBool bReleaseCritical(OSAL_tSemHandle hSemId)
{
   tBool bSemPost = FALSE;
   tS32 s32Ret = OSAL_s32SemaphorePost( hSemId );

   if ( s32Ret == OSAL_OK ) {
      bSemPost = TRUE;
   } else {
       NORMAL_M_ASSERT_ALWAYS();
   }
   return ( bSemPost );
}

/****************************************************************************/
/*!
*\fn      tVoid asipClearHeader(tU8 *buf)
*         tVoid asipClearHeader(tU8 *buf);
*         tVoid asipSetHeaderID(tU8 *buf);
*         tVoid asipSetHeaderAck(tU8 *buf,tU8 ucAck);
*         tVoid asipSetHeaderPayLen(tU8 *buf,tU16 uslen);
*         tVoid asipSetHeaderCRC(tU8 *buf,tU8 ucCRC);
*         tVoid asipSetHeaderSeqNo(tU8 *buf,tU8 ucSeq);
*         tVoid asipSetHeaderSeqReset(tU8 *buf,tU8 ucSeqReset);
*         tVoid asipSetHeaderChksum(tU8 *buf);
*         tVoid asipClearPayload(tU8 *buf);
*         tVoid asipSetPayloadData( tU8 *buf , tU8 *data , tU16 uslen );
*         tVoid asipClearPayloadDDummy( tU8 *buf );
*         tVoid asipSetPayloadChannel(tU8 *buf , tU8 ch );
*         tVoid asipSetCRC( tU8 *buf );
*         tU8 asipNextSeqNo( tU8 ucSeqNo );
*         tU8 asicPrevSeqNo( tU8 ucSeqNo );
*         tU16 asipGetPacketSize(tU8 *buf);
*         tU8 asipChkHeaderRemoteID( tU8 *buf);
*         tU8 asipGetHeaderAck(tU8 *buf );
*         tU16 asipGetHeaderPayLen(tU8 *buf );
*         tU8 asipGetHeaderCRC(tU8 *buf );
*         tU8 asipGetHeaderSeqNo(tU8 *buf );
*         tU8 asipGetHeaderSeqReset(tU8 *buf );
*         tU32 asipChkHeaderChksum(tU8 *buf);
*         tU16 asipGetDataLen(tU8 *buf);
*         tU8 asipGetDataChannel( tU8 *buf);
*         tU16 asipGetDataOpcode( tU8 *buf);
*         tU8 *asipGetDataAddr(tU8 *buf);
*         tU16 asipChkPktCRC(tU8 *buf);
*
*
*
*\brief   Helper functions for packet analysis
*
*\param
*
*\return
*
*\par History:
*
****************************************************************************/
tVoid asipClearHeader(tU8 *buf)
{
   buf[0] = 0;
   buf[1] = 0;
   buf[2] = 0;
   buf[3] = 0;
}
tVoid asipSetHeaderID(tU8 *buf)
{
   buf[0] &= 0x0f;
   buf[0] |= gu8OwnID;
}
tVoid asipSetRtr(tU8 *buf, tU8 u8Rtr)
{
   if( u8Rtr )
   {
      buf[0] |= ASIP_RTR;
   }
   else
   {
      buf[0] &= (~ASIP_RTR);
   }
}
tVoid asipSetHeaderAck(tU8 *buf,tU8 ucAck)
{
   if( ucAck )
   {
      buf[0] |= ASIP_ACK;
   }
   else
   {
      buf[0] &= (~ASIP_ACK);
   }
}
tVoid asipSetHeaderPayLen(tU8 *buf,tU16 uslen)
{
   if( uslen != 0 )uslen += ASIP_PAYHEAD_SIZE;
   if( uslen > MAX_PAYLOAD_SIZE ) uslen = MAX_PAYLOAD_SIZE;
   else if( uslen & 0x01 ) uslen++;

   buf[1] = uslen & 0xff;
   buf[2] &= 0xf8;
   buf[2] |= ( uslen >> 8 ) & 0x07;
}
tVoid asipSetHeaderCRC(tU8 *buf,tU8 ucCRC)
{
   if( ucCRC )
   {
      buf[2] |= ASIP_CRC;
   }
   else
   {
      buf[2] &= (~ASIP_CRC);
   }
}
tVoid asipSetHeaderSeqNo(tU8 *buf,tU8 ucSeq)
{
   buf[2] &= 0x8f;
   buf[2] |= (( ucSeq & 0x07) << 4 ) ;
}
tVoid asipSetHeaderSeqReset(tU8 *buf,tU8 ucSeqReset)
{
   if( ucSeqReset )
   {
      buf[2] |= ASIP_SEQRESET;
   }
   else
   {
      buf[2] &= (~ASIP_SEQRESET);
   }
}

tVoid asipSetHeaderChksum(tU8 *buf)
{
   buf[3] = drvBtAsipChksum( buf , ASIP_HEADER_SIZE - 1);
}

tVoid asipSetPayloadData( tU8 *buf , tU8 *data , tU16 uslen )
{
   if( uslen == 0 )return;
   if( uslen > MAX_PAYLOAD_SIZE - ASIP_PAYHEAD_SIZE ) uslen = MAX_PAYLOAD_SIZE - ASIP_PAYHEAD_SIZE;

   buf[4] = uslen & 0xff;
   buf[5] = (uslen >> 8 ) & 0xff;

   memcpy( &buf[7] , data , uslen );

}

tVoid asipClearPayloadDDummy( tU8 *buf )
{
   tU16 uslen;

   if( asipGetHeaderPayLen( buf ) > ASIP_PAYHEAD_SIZE )
   {
      uslen = asipGetDataLen( buf );

      if( uslen == 0 )return;
      if( uslen > MAX_PAYLOAD_SIZE - ASIP_PAYHEAD_SIZE )
      {
         uslen = MAX_PAYLOAD_SIZE - ASIP_PAYHEAD_SIZE;
         buf[4] = uslen & 0xff;
         buf[5] = (uslen >> 8 ) & 0xff;
      }
      if( ( uslen & 0x01 ) == 0x00 ) {
         buf[ ASIP_HEADER_SIZE + ASIP_PAYHEAD_SIZE + uslen ] = 0; // dummy byte
         //here we have also add DummyByte size PayloadLength count
         //buf[1]++; // Header         -> PayloadLength
         //buf[4]++; // Payload Header -> DataLength
      }
   }
}

tVoid asipSetPayloadChannel(tU8 *buf , tU8 ch )
{
   buf[6] = ch;
}

tVoid asipSetCRC( tU8 *buf )
{
   tU16 usLen;
   tU16 crcval;

   if( asipGetHeaderCRC(buf) )
   {
      usLen = asipGetPacketSize(buf) - ASIP_CRC_SIZE;

      crcval = drvBtAsipCrc( buf ,  usLen );
      buf[usLen++] = crcval & 0xff;
      buf[usLen  ] = (crcval >> 8 ) & 0xff;
   }
}

//-------------------------------------------------
//	read & check function
//-------------------------------------------------
tU16 asipGetPacketSize(tU8 *buf)
{
   tU16 usLen;
   if (((buf[0]&0xf0) == ASIP_ID_UNIT) || ((buf[0]&0xf0) == ASIP_ID_HOST) ){
      usLen = ASIP_HEADER_SIZE;
      usLen+= asipGetHeaderPayLen(buf);
      if( asipGetHeaderCRC(buf) ) usLen+= ASIP_CRC_SIZE;

      return usLen;
   }

   return 0;
}

tU8 asipChkHeaderRemoteID( tU8 *buf)
{
   tU8 ucID;

   if( gu8OwnID != ASIP_ID_HOST )	ucID = ASIP_ID_HOST;
   else							ucID = ASIP_ID_UNIT;

   return ( buf[0] & 0xf0 ) - ucID;
}
tU8 asipGetHeaderAck(tU8 *buf )
{
   return buf[0] & ASIP_ACK;

}
tU16 asipGetHeaderPayLen(tU8 *buf )
{
   tU16 usLen;
   usLen  =  (tU16)  buf[1];
   usLen += ((tU16)( buf[2] & 0x07 )) << 8 ;
   return usLen;
}
tU8 asipGetHeaderCRC(tU8 *buf )
{
   return buf[2] & ASIP_CRC;

}
tU8 asipGetHeaderSeqNo(tU8 *buf )
{
   return ((buf[2] >> 4) & 0x07);
}
tU8 asipGetHeaderSeqReset(tU8 *buf )
{
   return buf[2] & ASIP_SEQRESET;
}

tU32 asipChkHeaderChksum(tU8 *buf)
{
   return buf[3] - drvBtAsipChksum( buf , ASIP_HEADER_SIZE - 1);
}
tU16 asipGetDataLen(tU8 *buf)
{
   tU16 usLen;
   usLen  =  (tU16)buf[4];
   usLen += ((tU16)buf[5] ) << 8 ;
   return usLen;
}
tU8 asipGetDataChannel( tU8 *buf)
{
   return buf[6];
}
tU8 *asipGetDataAddr(tU8 *buf)
{
   return &buf[7];
}

tU16 asipChkPktCRC(tU8 *buf)
{
   if( asipGetHeaderCRC(buf) == 0 )return 0;	// not judge



   return drvBtAsipCrc( buf , asipGetPacketSize( buf ) );
}

tU8 asipGetRtr(tU8 *buf){
   return buf[0] & ASIP_RTR;
}

tU16 asipGetDataOpcode( tU8 *buf) {
   tU16 u16Opcode;
   u16Opcode  =  (tU16)buf[7];
   u16Opcode += ((tU16)buf[8] ) << 8 ;
   return u16Opcode;
}

tU8 asipNextSeqNo( tU8 ucSeqNo )
{
   if( ucSeqNo < MAX_SEQ_NUM )	ucSeqNo++;
   else						ucSeqNo = 0;
   return ucSeqNo;
}
tU8 asicPrevSeqNo( tU8 ucSeqNo )
{
   if( ucSeqNo != 0 )	ucSeqNo--;
   else				ucSeqNo = MAX_SEQ_NUM;
   return ucSeqNo;
}

tVoid asipSendAck(tU8 ucSeqNo, tU8 u8Rtr )
{
   tU8 txBuf[6] = {0};
   tS32 s32DataLen = 0;

   asipClearHeader( txBuf );
   asipSetHeaderID( txBuf );
   asipSetHeaderAck(txBuf , 1);
   asipSetRtr(txBuf, u8Rtr);
   asipSetHeaderPayLen( txBuf , 0 );
   asipSetHeaderCRC( txBuf , gu8CRC );
   asipSetHeaderSeqNo( txBuf , ucSeqNo );
   asipSetHeaderSeqReset( txBuf , 0 );
   asipSetHeaderChksum( txBuf );
   asipSetCRC(txBuf);

   //only for test purpose
   if (u8TxTestMode == BT_TEST_MODE_CRC_FAILURE) {
      txBuf[5]++;
   } else if (u8TxTestMode == BT_TEST_MODE_CHECKSUM_FAILURE) {
      txBuf[3]++;
   } else if (u8TxTestMode == BT_TEST_MODE_SEQ_FAILURE) {
      asipSetHeaderSeqNo( txBuf , ucSeqNo++ );
   }
   if (u8TxTestMode != BT_TEST_MODE_IGNORE_MSG) {
      drv_bt_vTraceBuf(TR_LEVEL_USER_3, TR_DRV_BT_MSG_BT_UGZZC_WRITE, txBuf, 6);
   }

   s32DataLen = drv_bt_lld_uart_write(UART_4, txBuf, 6, 1000);
   if (s32DataLen < 0) {
      //error
   }

   return;
}

/****************************************************************************/
/*!
*\fn      tS32 s32MsgComplete(tU8 *RecvBuffAddr)
*
*\brief   check if a complete packet is received correct
*         if opcode indicates DUN message transfer to NuNet
*			 if opcode indicates SPP message transfer to UUID
*
*\param   tU8 *RecvBuffAddr	: pointer to packet
*
*\return  tS32:    ASIP_OK
*                  ASIP_DUN_OK
*
*\par History:
*	Changed - 01/02/12 BOA2HI: Added the SPP section
*										turn the if else contruct to a switch case
****************************************************************************/
tS32 s32MsgComplete(tU8 *RecvBuffAddr)
{
   tS32 s32Ret=(tS32)ASIP_ERROR;
   tU8 u8CheckForFault = 0;

   if (asipChkPktCRC( RecvBuffAddr )) {
      u8CheckForFault |= ASIP_CHECK_ERROR_CRC;
   }

   //only for test purpose
   if ((u8TxTestMode == BT_TEST_MODE_RX_RCV_ACK_ERR) && (gpu8AsipTxBuf != NULL)){
      u8CheckForFault = ASIP_CHECK_ERROR_ID|ASIP_CHECK_ERROR_CHECKSUM|ASIP_CHECK_ERROR_CRC;
      u8TxTestMode = 0;
   } else if ((u8TxTestMode == BT_TEST_MODE_RX_RCV_ERR) && (gpu8AsipTxBuf == NULL)){
      u8CheckForFault = ASIP_CHECK_ERROR_ID|ASIP_CHECK_ERROR_CHECKSUM|ASIP_CHECK_ERROR_CRC;
      u8TxTestMode = 0;
   }

   if (bEnterCritical(_hDrvBtWriteSemId, OSAL_C_U32_INFINITE)) {
      if(u8CheckForFault){
         vFlushRx();
         bReleaseCritical(_hDrvBtWriteSemId);
         vCheckMsgError(u8CheckForFault);
      } else if (asipGetRtr(RecvBuffAddr)) {
         //retransmission of last TX mssage is necessary
         os_printf_errmem("DrvBt: Retransmit requested by UGZZC: opcode: 0x%04x, TX/RX count: %d/%d\n", gu16TxLastOpcode, gu32TxMsgCount, gu32RxMsgCount);
         bReleaseCritical(_hDrvBtWriteSemId);

         s32DrvBtAsipSendData( &gpu8AsipTxBuf[7], gu32AsipTxLen, BT_RETRANSMIT);
      } else if( asipGetHeaderAck(RecvBuffAddr) && gpu8AsipTxBuf)	{

         if( asipGetHeaderSeqNo( RecvBuffAddr ) == asipGetHeaderSeqNo(gpu8AsipTxBuf) ){
            gpu8AsipTxBuf = NULL;
            gu32TxTimestamp = 0xffffffff;
            gu32AsipTxLen  = 0;
            gu8TxSendCounter = 0;
         }

         bReleaseCritical(_hDrvBtWriteSemId);
      } else {
         tBool IsRestData=FALSE;
         if( asipGetHeaderSeqReset( RecvBuffAddr ) )
         {
            _bUgzzcInReset = FALSE;

            if( asipGetHeaderSeqNo( RecvBuffAddr ) == 0 )
            {
               drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO, "s32MsgComplete: Rst Seq", 0,0,0);

               gu8AsipTxSeqNo = 0x00;
               gpu8AsipTxBuf = NULL;
               gu8AsipRxSeqNo = 0x00;
               IsRestData=TRUE;
               gu32TxTimestamp = 0xffffffff;

               //g_bInited = TRUE;//dmj added
               //reinitReqFifo();
            }
         }

         if( asipGetHeaderSeqNo( RecvBuffAddr ) == gu8AsipRxSeqNo ){
            tBool bSendAck = TRUE;

            if(IsRestData) {
               s32Ret= (tS32)ASIP_INIT;
            }else{
               //check sequence ack / nb / seq rst / ...
               //here check if we are waiting for a ack

               //everything OK --> check opcode
               gu16RxLastOpcode = asipGetDataOpcode(RecvBuffAddr);

				#if (defined BT_NUNET_ENABLED || defined BT_SPP_ENABLED)
					tU16 u16DataLen;
					tU8* pu8PayloadData;
					char *sopcode[100];
					pu8PayloadData = asipGetDataAddr(RecvBuffAddr);
				#endif
					switch (gu16RxLastOpcode){
					#ifdef BT_NUNET_ENABLED
						case BT_APPL_DUN_DATA_IND:
							//parse information to nucleus net without header and CRC information

							u16DataLen  =  (tU16)pu8PayloadData[5];
							u16DataLen += ((tU16)pu8PayloadData[4] ) << 8 ;

							vDunDataReceived( &pu8PayloadData[6], u16DataLen);
							s32Ret = ASIP_DUN_OK;
						break;
						case BT_APPL_DUN_SEND_DATA_REQ:
						case BT_APPL_DUN_DATA_COMP_IND:
						case BT_APPL_DUN_DATA_SEND_FC_IND:
							s32Ret = ASIP_DUN_OK;
						break;
					#endif
					#ifdef BT_SPP_ENABLED
						//Response Messages from UGZZC
						case BT_APPL_SPP_DATA_IND:
							u16DataLen  =  (tU16)pu8PayloadData[5];
							u16DataLen += ((tU16)pu8PayloadData[4] ) << 8;
							#ifdef DEBUG_BT_SPP_ENABLED
							printf("DrvBt: \033[31mSPP_DATA_IND\033[0m detected: opcode: 0x%04x lenght: 0x%04x\n", gu16RxLastOpcode,u16DataLen);
							#endif //DEBUG_BT_SPP_ENABLED
							v_drv_bt_SPPDataReceived( gu16RxLastOpcode, &pu8PayloadData[6], u16DataLen);
							s32Ret = ASIP_SPP_OK;
						break;
						//----------------------------------------------------------------
						// Confimations from UGZZC, if the requested action is carried out
						//----------------------------------------------------------------
						case BT_APPL_SPP_DATA_CFM:
							u16DataLen  =  (tU16)pu8PayloadData[3];
							u16DataLen += ((tU16)pu8PayloadData[2] );// << 8;
							#ifdef DEBUG_BT_SPP_ENABLED
							printf("DrvBt: \033[BT_APPL_SPP_DATA_CFM\033[0m detected: opcode: 0x%04x lenght: 0x%04x\n", gu16RxLastOpcode,u16DataLen);
							#endif //DEBUG_BT_SPP_ENABLED
							v_drv_bt_SPPDataReceived( gu16RxLastOpcode, &pu8PayloadData[4], u16DataLen);
							s32Ret = ASIP_SPP_OK;
						break;
						case BT_APPL_DEVICE_CONNECT:
						case BT_APPL_DEVICE_DISCONNECT:
							u16DataLen = (tU16)pu8PayloadData[3];
							u16DataLen += (tU16)pu8PayloadData[2];
							#ifdef DEBUG_BT_SPP_ENABLED
							printf("DrvBt: \033[BT_APPL_DEVICE_(DIS)CONNECT\033[0m detected: opcode: 0x%04x lenght: 0x%04x\n", gu16RxLastOpcode,u16DataLen);
							#endif //DEBUG_BT_SPP_ENABLED
							v_drv_bt_SPPdeviceConnect( gu16RxLastOpcode, u16DataLen, &pu8PayloadData[4]);
							s32Ret = ASIP_SPP_OK;
						break;
					#endif
						default:
							//read OK -> return read
							s32Ret = ASIP_OK;
							if (&_au8DrvBtRxBuf[0][0] != RecvBuffAddr) {
								//this is the dummy buffer -> RX buffer
								bSendAck = FALSE;
								gu32NumberSkippedMsg++;
								//drv_bt_vPrintf("DrvBt: Discard message: opcode: 0x%04x, discard count: %d\n", gu16RxLastOpcode, gu32NumberSkippedMsg);
							}
					}
            }

            if (bSendAck) {
               asipSendAck( gu8AsipRxSeqNo, 0);
               gu8AsipRxSeqNo = asipNextSeqNo( gu8AsipRxSeqNo );
            }

         } else if( asipGetHeaderSeqNo( RecvBuffAddr ) == asicPrevSeqNo( gu8AsipRxSeqNo) ) {
            asipSendAck( asicPrevSeqNo( gu8AsipRxSeqNo), 0);
         }
         bReleaseCritical(_hDrvBtWriteSemId);
      }
   }

   return s32Ret;
}

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/
/****************************************************************************/
/*!
*\fn      tS32 s32DrvBtAsipSendData( tU8 *txBuf , tU16 len, tU8 u8Mode )
*
*\brief   Write buffer to UGZZC
*
*\param   tU8 *txBuf	: pointer to buffer to be send
*         tU32 len   : number of bytes to send
*
*\return  tS32:    Number of written bytes
*                  OSAL_E_UNKNOWN
*
*\par History:
*
****************************************************************************/
tS32 s32DrvBtAsipSendData( tU8 *txBuf, tU32 len, tU8 u8Mode) {

   tS32 s32WrittenBytes = 0;

   if (_bHciMode) {
      s32WrittenBytes = drv_bt_lld_uart_write(UART_4,txBuf, len, 1000);
   } else {
      if (_bUgzzcInReset){
         s32WrittenBytes = OSAL_E_BUSY;
      } else {
      if (bEnterCritical(_hDrvBtWriteSemId, OSAL_C_U32_INFINITE)) {
         if ((u8Mode == BT_RETRANSMIT) || (u8Mode == BT_RETRANSMIT_SEQ_RST) || (u8Mode == BT_FORCE_SEND) || (gpu8AsipTxBuf == NULL)) {
            tU16 u16Len=0;
            gu32AsipTxLen = len;
            gpu8AsipTxBuf = &_u8TxBuf[0];

            asipClearHeader( gpu8AsipTxBuf );

            asipSetHeaderID( gpu8AsipTxBuf );
            asipSetHeaderAck(gpu8AsipTxBuf , 0);
            asipSetHeaderPayLen( gpu8AsipTxBuf , len );
            asipSetHeaderCRC( gpu8AsipTxBuf , gu8CRC );
            asipSetHeaderSeqNo( gpu8AsipTxBuf , gu8AsipTxSeqNo );
            asipSetHeaderSeqReset( gpu8AsipTxBuf , 0 );

            if (u8Mode == BT_RETRANSMIT) {
               gu8AsipTxSeqNo = asicPrevSeqNo( gu8AsipTxSeqNo );
               asipSetHeaderSeqNo(gpu8AsipTxBuf, gu8AsipTxSeqNo);
            } else if (u8Mode == BT_RETRANSMIT_SEQ_RST) {
               gu8AsipTxSeqNo = 0;
               gu8AsipRxSeqNo = 0;
               asipSetHeaderSeqNo(gpu8AsipTxBuf, gu8AsipTxSeqNo);
               asipSetHeaderSeqReset( gpu8AsipTxBuf , 1 );
            }

            gu8AsipTxSeqNo = asipNextSeqNo( gu8AsipTxSeqNo );

            //only for test -> send wrong messages to UGZZC
            if (u8Mode == BT_TEST_MODE_SEQ_FAILURE) {
               //set wrong sequence number (gu8AsipTxSeqNo was already incremented)
               asipSetHeaderSeqNo( gpu8AsipTxBuf , gu8AsipTxSeqNo );
            } else if (u8Mode == BT_TEST_MODE_SEQ_INIT){
               gu8AsipTxSeqNo = 0;
               gu8AsipRxSeqNo = 0;
               asipSetHeaderSeqNo(gpu8AsipTxBuf, gu8AsipTxSeqNo);
               asipSetHeaderSeqReset( gpu8AsipTxBuf , 1 );
            }
            asipSetHeaderChksum( gpu8AsipTxBuf );

            asipSetPayloadChannel(gpu8AsipTxBuf, 0);
            asipSetPayloadData( gpu8AsipTxBuf , txBuf , len );
            asipClearPayloadDDummy( gpu8AsipTxBuf );
            asipSetCRC(gpu8AsipTxBuf);

            u16Len = asipGetHeaderPayLen(gpu8AsipTxBuf) + ASIP_HEADER_SIZE + ASIP_CRC_SIZE;

            gu16TxLastOpcode = asipGetDataOpcode(gpu8AsipTxBuf);
            gu8TxSendCounter++; //message send to UGZZC

            //only for test -> send wrong messages to UGZZC
            if (u8Mode == BT_TEST_MODE_CRC_FAILURE) {
               gpu8AsipTxBuf[u16Len-1]++;
            } else if (u8Mode == BT_TEST_MODE_CHECKSUM_FAILURE) {
               gpu8AsipTxBuf[3]++;
            }

            gu32TxMsgCount++;
            gu32TxTimestamp = OSAL_ClockGetElapsedTime();
            s32WrittenBytes = drv_bt_lld_uart_write(UART_4,gpu8AsipTxBuf, u16Len, 1000);

            //trace write data
            drv_bt_vTraceBuf(TR_LEVEL_USER_3, TR_DRV_BT_MSG_BT_UGZZC_WRITE, gpu8AsipTxBuf, u16Len);

            if((OSAL_E_UNKNOWN == s32WrittenBytes)||(s32WrittenBytes < 0))
            {
               gpu8AsipTxBuf = NULL;
               gu32TxTimestamp = 0xffffffff;
               drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_WRITE, TR_DRV_BT_MSG_ERROR, "ERROR!", (tS32)TR_DRV_BT_ERR_WRITE, 0, 0);
               s32WrittenBytes = OSAL_E_UNKNOWN;
            }

         } else {

            drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_WRITE, TR_DRV_BT_MSG_ERROR, "currently waiting for UGZZC acknowledge.", (tS32)TR_DRV_BT_ERR_WRITE, 0, 0);
            s32WrittenBytes = OSAL_E_BUSY;
         }
         bReleaseCritical(_hDrvBtWriteSemId);
      }
      }
   }
   return s32WrittenBytes;
}

tS32 s32ReadCompleteMsg( tU8 *rxBuf, tU32 u32Len) {

   tU8 * pu8RxBufStart = rxBuf;
   tS32 s32ReadBytes = 0;
   tU32 u32Bytes2Read = 0;
   tU8 u8CheckForFault = 0;
   tS32 s32DataLen = 0;
   tU8 au8Header[ASIP_HEADER_SIZE] = {0};
   tU32 u32Timeout = 1000; //ms


   if (gu8TxSendCounter != 0) {
      //currently waiting for ACK -> do not need to wait 1s
      u32Timeout = 200;
   }

   if (&_au8DrvBtRxBuf[0][0] != rxBuf) {
      //this is buffer to discard -> timeout is set to 50ms
      u32Timeout = 50;
   }

   //first wait for header
   s32DataLen = drv_bt_lld_uart_read(UART_4, au8Header, ASIP_HEADER_SIZE, u32Timeout);
   if (_bDrvClosed) {
      //DRV_BT already closed -> return with no data read
      return 0;
   }

   if (s32DataLen == 0) {
      tU32 u32CurrentTime = OSAL_ClockGetElapsedTime();
      if ((u32CurrentTime>(gu32TxTimestamp + BT_TX_MSG_TIMEOUT)) && (gpu8AsipTxBuf != NULL)) {
         drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_ERROR, "Timeout while waiting for ACK", (tS32)TR_DRV_BT_ERR_READ, u32CurrentTime-gu32TxTimestamp, gu8TxSendCounter);

         if (gu8TxSendCounter < BT_TX_MAX_RETRANSMIT_COUNT) {
            //try to reset sequence
            os_printf_errmem("DrvBt: UGZZC Timeout while waiting for Response -> Trigger restart sequence: opcode: 0x%04x, delay: %dms, TX/RX count: %d/%d\n", gu16TxLastOpcode, (u32CurrentTime-gu32TxTimestamp), gu32TxMsgCount, gu32RxMsgCount);
            s32DrvBtAsipSendData( &gpu8AsipTxBuf[7], gu32AsipTxLen, BT_RETRANSMIT_SEQ_RST);
         } else {
            //no response from UGZZC received -> reset module
            drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_ERROR, "Reset UGZZZ", (tS32)TR_DRV_BT_ERR_READ, u32CurrentTime, gu32TxTimestamp);
            os_printf_errmem("DrvBt: No response from UGZZC -> TX/RX opcode: 0x%04x/0x%04x, TX/RX count: %d/%d\n", gu16TxLastOpcode, gu16RxLastOpcode, gu32TxMsgCount, gu32RxMsgCount);
            if (!_bIgnoreResetTrigger) {
               vResetUgzzc(FALSE);
            } else {
               os_printf_errmem("DrvBt: UGZZC reset request ignored");
            }
         }
      }
      return OSAL_ERROR; //OSAL_E_TIMEOUT
   }

   if ((au8Header[0]&0xf0) != ASIP_ID_UNIT) {
      //unsupported header start -> flush RX buffer to be free for next UGZZC message
      drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_ERROR, "ERROR! -> header fail, flush rx bufr", (tS32)TR_DRV_BT_ERR_READ, au8Header[0], au8Header[1]);
      drv_bt_vTraceBuf(TR_LEVEL_USER_3, TR_DRV_BT_MSG_BT_UGZZC_READ, au8Header, 4);
      vFlushRx();

      return OSAL_ERROR; //OSAL_E_UNKNOWN
   }

   //copy header to receive buffer
   memcpy (rxBuf, au8Header, ASIP_HEADER_SIZE);

   s32ReadBytes = ASIP_HEADER_SIZE;
   rxBuf += ASIP_HEADER_SIZE;

   //Header is received now -> check if ok
   if (asipChkHeaderRemoteID( pu8RxBufStart )) {
      u8CheckForFault |= ASIP_CHECK_ERROR_ID;
   }
   if (asipChkHeaderChksum( pu8RxBufStart )) {
      u8CheckForFault |= ASIP_CHECK_ERROR_CHECKSUM;
   }
   if(u8CheckForFault) {
      //check failed
      vCheckMsgError(u8CheckForFault);
      vFlushRx();
   } else {
      u32Bytes2Read = asipGetPacketSize(pu8RxBufStart) - ASIP_HEADER_SIZE;
   }
   if (u32Bytes2Read > u32Len-4) {
      //buffer to small
      drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_ERROR, "ERROR! -> Buffer small ", (tS32)TR_DRV_BT_ERR_READ, s32ReadBytes, u32Len);
      return OSAL_ERROR; //OSAL_E_UNKNOWN
   }

   s32DataLen = drv_bt_lld_uart_read(UART_4, rxBuf, u32Bytes2Read, 10);
   if((OSAL_E_UNKNOWN == s32DataLen)||(s32DataLen < 0)) {
      //OS_ASSERT_NORMAL_ALWAYS();
      drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_ERROR, "ERROR! -> Datalen ", (tS32)TR_DRV_BT_ERR_READ, s32DataLen, 0);
      return OSAL_ERROR; //OSAL_E_UNKNOWN
   } else {
      //check if message is complete
      rxBuf += s32DataLen;
      s32ReadBytes += s32DataLen;

      //OK -> message complete
      drv_bt_vTraceBuf(TR_LEVEL_USER_3, TR_DRV_BT_MSG_BT_UGZZC_READ, pu8RxBufStart, s32ReadBytes);

      if (ASIP_OK != s32MsgComplete(pu8RxBufStart)) {
         s32ReadBytes = 0;
      }
      gu32RxMsgCount++;

   }
   return s32ReadBytes;
}

/****************************************************************************/
/*!
*\fn      tS32 s32DrvBtAsipRcvData( tU8 *rxBuf , tU32 len )
*
*\brief   Receive packet from UGZZC (return only if complete message is received)
*
*\param   tU8 *rxBuf	: pointer to buffer to be filled
*         tU32 len   : number of max bytes free in buffer
*
*\return  tS32:    Number of received bytes
*                  OSAL_E_UNKNOWN
*
*\par History:
*
****************************************************************************/
tS32 s32DrvBtAsipRcvData( tU8 *rxBuf, tU32 u32Len) {

   tS32 s32ReadBytes = 0;

   if (_bHciMode) {

      s32ReadBytes = drv_bt_lld_uart_read(UART_4, rxBuf, 1, 1000);
   } else {

      //check if we have already read out part of message with last message
      s32ReadBytes = s32ReadCompleteMsg(rxBuf, u32Len);
      while (s32ReadBytes == 0) {
         if (_bDrvClosed) {
            //DRV_BT already closed -> return with no data read
            return 0;
         }
         s32ReadBytes = s32ReadCompleteMsg(rxBuf, u32Len);
      } //while (!bMsgComplete) {

      if (s32ReadBytes == OSAL_ERROR) {
         s32ReadBytes = 0;
      }
   }

   return s32ReadBytes;
}

/****************************************************************************/
/*!
*\fn      tS32 s32DrvBtAsipRcvData( tU8 *rxBuf , tU32 len )
*
*\brief   Receive packet from UGZZC (return only if complete message is received)
*
*\param   tU8 *rxBuf	: pointer to buffer to be filled
*         tU32 len   : number of max bytes free in buffer
*
*\return  tS32:    Number of received bytes
*                  OSAL_E_UNKNOWN
*
*\par History:
*
****************************************************************************/
tS32 s32DrvBtAsipRcvDataSync( tU8 *rxBuf, tU32 u32Len) {
   tS32 s32ReadBytes = 0;

   if (_bHciMode) {
		s32ReadBytes = drv_bt_lld_uart_read(UART_4, rxBuf, 1, 1000);
   } else {
      if ((_u32RcvData != 0) && (u32Len >= _u32RcvData)){
         //we have already data
         OSAL_pvMemoryCopy(rxBuf, &_au8DrvBtRxBuf[0][0], _u32RcvData);
         s32ReadBytes = (tS32)_u32RcvData;
         _u32RcvData = 0;
      } else {
			//we have no data but we try again if the
         //drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO, "TEL: SemWait", 0, 0, 0);
         if (bEnterCritical(_hDrvBtSemId, 1000)) {
            //we owned the Semaphore
            //drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO, "TEL: SemRcv", _u32RcvData, 0, 0);
            if ((_u32RcvData != 0) && (u32Len >= _u32RcvData)){
					//we have received data
               OSAL_pvMemoryCopy(rxBuf, &_au8DrvBtRxBuf[0][0], _u32RcvData);
               s32ReadBytes = (tS32)_u32RcvData;
               _u32RcvData = 0;
					bReleaseCritical(_hDrvBtSemId);
            } else {
					//drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO, "TEL: noData", 0, 0, 0);
					//we have no Data to read so we release the semaphore
					bReleaseCritical(_hDrvBtSemId);
					//we have to wait for some time because the driver needs to read data to buffer
					OSAL_s32ThreadWait(100);
				}
         } else {
            //drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO, "TEL: SemNotRcv", 0, 0, 0);
            s32ReadBytes = 0;
         }
      }
      if (s32ReadBytes>0) {
         gu32RxMsg2AppCount++;
         //drv_bt_vPrintf("DrvBt: New message to app: opcode: 0x%04x, RX App count: %d, RX count: %d, discard count: %d\n", asipGetDataOpcode(rxBuf), gu32RxMsg2AppCount, gu32RxMsg4AppCount, gu32NumberSkippedMsg);
      }
   }
   return s32ReadBytes;
}

/****************************************************************************/
/*!
*\fn      tVoid vFlushRx()
*
*\brief   Flush buffer
*
*\param
*
*\return  tVoid
*
*\par History:
*
****************************************************************************/
tVoid vFlushRx(tVoid) {
   tS32 s32DataLen = 1;
   tU8 u8Buf;

   while (s32DataLen == 1) {
      //flush complete buffer and wait for a new message send by UGZZC
      s32DataLen = drv_bt_lld_uart_read(UART_4, &u8Buf, 1, 10);
   }
}

/****************************************************************************/
/*!
*\fn      tVoid vCheckMsgError(tU8 u8Error)
*
*\brief
*
*\param   u8
*
*\return  tVoid
*
*\par History:
*
****************************************************************************/
tVoid vCheckMsgError(tU8 u8Error) {
   if (u8Error&ASIP_CHECK_ERROR_ID) {
      //os_printf_errmem("DrvBt: Remote ID failure: TX/RX count: %d/%d\n", gu32TxMsgCount, gu32RxMsgCount);
   }
   if (u8Error&ASIP_CHECK_ERROR_CHECKSUM) {
      //os_printf_errmem("DrvBt: Checksum failure: TX/RX count: %d/%d\n", gu32TxMsgCount, gu32RxMsgCount);
   }
   if (u8Error&ASIP_CHECK_ERROR_CRC) {
      //os_printf_errmem("DrvBt: Crc failure: TX/RX count: %d/%d\n", gu32TxMsgCount, gu32RxMsgCount);
   }
   if (gpu8AsipTxBuf != NULL) {
      s32DrvBtAsipSendData( &gpu8AsipTxBuf[7], gu32AsipTxLen, BT_RETRANSMIT);
   }
}

/****************************************************************************/
/*!
*\fn      tVoid vResetUgzzc(tBool bHciMode)
*
*\brief   Reset UGZZC
*
*\param   tBool bHciMode: TRUE  -> restart UGZZC in HCI mode
*                         FALSE -> restart UGZZC in NORMAL mode
*
*\return  tVoid
*
*\par History:
*
****************************************************************************/
tVoid vResetUgzzc(tBool bHciMode) {

   _bUgzzcInReset = TRUE;
   _bHciMode = bHciMode;

   //init global vars
   gu8AsipTxSeqNo=0;
   gu8AsipRxSeqNo=0;
   gpu8AsipTxBuf = NULL;
   gu32AsipTxLen=0;
   gu32TxTimestamp = 0xffffffff;
   gu8TxSendCounter = 0;
   gu32TxMsgCount = 0;
   gu32RxMsgCount = 0;
   gpu8AsipRxBuf = NULL;
   gu32AsipRxLen=0;
   gu8OwnID=ASIP_ID_HOST;
   gu8CRC=1;

	#ifdef BT_SPP_ENABLED
	_bDrvBtSocketWork = FALSE;
	#endif //BT_SPP_ENABLED

	drv_bt_lld_uart_flushBuffer_Ugzzc();
   _bUgzzcInReset = FALSE;
   //OSAL_s32ThreadWait(200);
	return;
}

/*****************************************************************************
*
* FUNCTION:
*     tVoid dwThreadUgzzc( tPVoid pvArg )
*
* DESCRIPTION:
*     Thread to test read interface. Thread needed to generate message to ALPS module.
*     All acknowledges will be send in context of the read thread
*
*
* PARAMETERS:
*
* RETURNVALUE:
*     None
*
*
* HISTORY:
*
*****************************************************************************/
tVoid dwThreadDrvBt( tPVoid pvArg )
{
   tS32 s32DataLen = 0;

   (tVoid) pvArg;
   //NU_SUPERVISOR_MODE();

   while (!_bDrvBtTerminate) {
      if (_bHciMode) {
         //nothing to do in HCI mode
         OSAL_s32ThreadWait(80);
      }  else {
			#ifdef DEBUG_DWTHREADDRVBT
         drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO, "BT: SemWait", _u32RcvData, 0, 0);
			#endif
         if ((bEnterCritical(_hDrvBtSemId, 10)) && (_u32RcvData == 0)){
            //phone buffer free
				#ifdef DEBUG_DWTHREADDRVBT
            drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO, "BT: SemRcv 0", _u32RcvData, 0, 0);
				#endif
            s32DataLen = 0;
            while ((!_bHciMode) && (!_bDrvBtTerminate) && (0 >= s32DataLen)) {
               s32DataLen = s32DrvBtAsipRcvData(&_au8DrvBtRxBuf[0][0], (ASIP_HEADER_SIZE + MAX_PAYLOAD_SIZE));
               if (s32DataLen > 0) {
                  gu32RxMsg4AppCount++;
                  _u32RcvData = (tU32)s32DataLen;
               }
            }
				#ifdef DEBUG_DWTHREADDRVBT
            drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO, "BT: SemRel 0", _u32RcvData, 0, 0);
				#endif
         } else {
				#ifdef DEBUG_DWTHREADDRVBT
            drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO, "BT: SemNotRcv 1", _u32RcvData, 0, 0);
				#endif
            s32DrvBtAsipRcvData(&_au8DrvBtRxBuf[1][0], (ASIP_HEADER_SIZE + MAX_PAYLOAD_SIZE));
         }
         bReleaseCritical(_hDrvBtSemId);
      }
   }
   //NU_USER_MODE();

   OSAL_vThreadExit();
   return;
}

tVoid vCreateThread() {
	#ifdef DEBUG_VCREATETHREAD
	drv_bt_vPrintf("\033[31mDrvBt: vCreateThread\033[0m\n");
	#endif
   if (_hDrvBtThreadID == OSAL_ERROR) {
      OSAL_trThreadAttribute  rThAttr;

      /* create semaphore for protecting NotificationTable */
      (tVoid)OSAL_s32SemaphoreCreate(DRV_BT_SEM_NAME, &_hDrvBtSemId, 1);
      (tVoid)OSAL_s32SemaphoreCreate(DRV_BT_WRITE_SEM_NAME, &_hDrvBtWriteSemId, 1);


      _bDrvBtTerminate = FALSE;

      // rThAttr.szName       = DRV_BT_THREAD_NAME;
      OSAL_szStringNCopy(rThAttr.szName,DRV_BT_THREAD_NAME,sizeof(DRV_BT_THREAD_NAME));
      rThAttr.u32Priority  = OSALTHREAD_C_U32_PRIORITY_MID;
      rThAttr.s32StackSize = OSALTHREAD_C_U32_STACKSIZE_NORMAL;
      rThAttr.pfEntry      = (OSAL_tpfThreadEntry)dwThreadDrvBt;
      rThAttr.pvArg        = (tPVoid)NULL;

      // create thread suspended
      _hDrvBtThreadID = OSAL_ThreadSpawn( &rThAttr );
		#ifdef DEBUG_VCREATETHREAD
		drv_bt_vPrintf("\033[31mDrvBt: thread created ID:\033[0m %u\n",_hDrvBtThreadID);
		#endif
   }
	return;
}

tBool bDrvBtAsipInit() {
   return TRUE;
}


tBool bDrvBtAsipDeInit() {
   _bDrvBtTerminate = TRUE;
	#ifdef BT_SPP_ENABLED
	_bDrvBtSocketWork = FALSE;
	#endif //BT_SPP_ENABLED
   return TRUE;
}



