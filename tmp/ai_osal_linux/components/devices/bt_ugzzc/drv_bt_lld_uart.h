/*******************************************************************************
*
* FILE:
*     drv_bt_lld_uart.h
*
* REVISION:
*     1.0
*
* AUTHOR:
*     (c) 2012, Bosch Car Multimedia GmbH, CM-AI/PJ-VW32, Bosse Arndt, 
*                                                  extrenal.bosse.arndt@de.bosch.com
*
* CREATED:
*     01/02/2012 - Bosse Arndt
*
* DESCRIPTION:
*     This file contains all the definitions, macros, and types that 
*     are private to the drv_bt_lld_uart.cpp. 
*
* NOTES:
*
* MODIFIED:
*
*******************************************************************************/
#ifndef DRV_BT_LLD_UART_H
#define DRV_BT_LLD_UART_H

#ifdef __cplusplus
extern "C" {
#endif

tVoid drv_bt_lld_uart_port_set_rts(int state);
tVoid drv_bt_lld_uart_flushBuffer_Ugzzc(tVoid);

tBool drv_bt_lld_uart_init(tChar* strPortNb, int baudrate);
tS32 drv_bt_lld_uart_write(int comport, tU8* buf, tU16 len, tU32 timeout);
tS32 drv_bt_lld_uart_read(int comport, tU8* buf, tU16 len, tU32 timeout);
tBool drv_bt_lld_uart_close(tVoid);

#ifdef __cplusplus
}
#endif

#endif //DRV_BT_LLD_UART_H
