/*******************************************************************************
*
* FILE:
*     drv_bt_ugzzz.h
*
* REVISION:
*     1.0
*
* AUTHOR:
*     (c) 2011, Robert Bosch Car Multimedia., external.Bosse.Arndt@de.bosch.com
*
* CREATED:
*     05/12/2011
*
* DESCRIPTION:
*     This file contains all the definitions, macros, and types that 
*     are private to the drv_bt_ugzzc.c. 
*
* NOTES:
*
* MODIFIED:
*
*
*******************************************************************************/
#ifndef _DEV_BT_UGZZC_H
#define _DEV_BT_UGZZC_H
#ifdef __cplusplus
extern "C" {
#endif
tS32 BT_UGZZC_s32IODeviceInit(tVoid);
tS32 BT_UGZZC_s32IODeviceRemove(tVoid);

extern tBool drv_bt_lld_uart_init(tChar* strPortNb, int baudrate);
extern tBool drv_bt_lld_uart_close(tChar* strPortNb);
extern tVoid vResetUgzzc(tBool bHciMode);
/****************************************************************** 
|protoypes, scope global  
|---------------------------------------------------------------- */

/****************************************************************************/
/*! 
*\fn      tS32 BT_UGZZC_s32IODeviceInit(tVoid)
*
*\brief   Calls the drv_com init fn
*
*\return  	tS32: OSAL_E_NOERROR 		-> No Error
*
*\par History:  
*
****************************************************************************/
tS32 BT_UGZZC_s32IODeviceInit(tVoid);

/****************************************************************************/
/*! 
*\fn      tS32 BT_UGZZC_s32IODeviceRemove(tVoid)
*
*\brief   Calls the drv_com remove fn
*
*\return  	tS32: OSAL_E_NOERROR 		-> No Error
*
*\par History:  
*
****************************************************************************/
tS32 BT_UGZZC_s32IODeviceRemove(tVoid);

/****************************************************************************/
/*! 
*\fn      tS32 drv_bt_s32IOOpen(tS32 s32ID)
*
*\brief   Calls the drv_com Open fn 
*
*\param   tS32 s32ID	: Device identifier                      
*         
*\return  tS32: OSAL_E_NOERROR 		-> No Error
*				OSAL_E_INVALIDVALUE	-> Invalid value passed to function
*				OSAL_E_UNKNOWN		-> Functionality failed
*\par History:  
*
****************************************************************************/
tS32 BT_UGZZC_IOOpen(tVoid);

/****************************************************************************/
/*! 
*\fn      tS32 drv_bt_s32IOClose(tS32 s32ID)
*
*\brief   Calls the drv_com Close fn
*
*\param   tS32 s32ID	: Device identifier                              
*
*\return  tS32: OSAL_E_NOERROR 		-> No Error
*				OSAL_E_INVALIDVALUE	-> Invalid value passed to function
*				OSAL_E_UNKNOWN		-> Functionality failed
*\par History:  
*
****************************************************************************/
tS32 BT_UGZZC_s32IOClose(tVoid);

/****************************************************************************/
/*! 
*\fn      tS32 drv_bt_s32IOControl(tS32 s32ID, tS32 s32fun, tS32 s32arg)
*
*\brief   Calls the drv_com IOCtrl fn 
*
*\param   tS32 s32ID	:	Device identificator					            
*         tS32 s32fun	:   Function identificator                         
*         tS32 s32arg	:   Argument to be passed to function                   
*         
*\return  tS32: OSAL_E_NOERROR 		-> No Error
*				OSAL_E_INVALIDVALUE	-> Invalid value passed to function
*               OSAL_E_NOTSUPPORTED -> s32arg not supported by the fn 
*				OSAL_E_UNKNOWN		-> Functionality failed

*\par History:  
*
*****************************************************************************/
tS32 BT_UGZZC_s32IOControl(tS32 Ps32fun, tS32 Ps32arg);

/****************************************************************************/
/*! 
*\fn      tS32 drv_bt_s32IOWrite(tS32 s32ID, tPCS8 ps8Buffer, tU32 u32nbytes)
*
*\brief   Calls the drv_com write fn 
*
*\param   tS32 s32ID		:	Device identificator					        
*         ttPCS8 ps8Buffer	:   Pointer to Tx buffer                      
*         tU32 u32nbytes	:   Number of bytes to Tx                        
*         
*\return  tS32: s32Status       	-> No.of written bytes 
*				OSAL_E_INVALIDVALUE	-> Invalid value passed to function
*				OSAL_E_UNKNOWN		-> Functionality failed
 
*\par History:  
*
*****************************************************************************/
tS32 BT_UGZZC_s32IOWrite(tPCS8 Pps8Buffer, tU32 Pu32nbytes);
/****************************************************************************/
/*! 
*\fn      tS32 drv_bt_s32IORead(tS32 s32ID, tPS8 ps8Buffer, tU32 u32maxbytes)
*
*\brief   Calls the drv_com read fn 
*
*\param   tS32 s32ID		:	Device identificator					                  
*         ttPCS8 ps8Buffer	:   Pointer to Rx buffer                      
*         tU32 u32maxbytes	:   Max number of bytes to Rx                        
*         
*\return  tS32: s32Status   		-> No. of bytes read
*				OSAL_E_INVALIDVALUE	-> Invalid value passed to function
*				OSAL_E_UNKNOWN		-> Functionality failed
 
*\par History:  
*
*****************************************************************************/
tS32 BT_UGZZC_s32IORead(tPCS8 Pps8Buffer,tU32 Pu32nbytes);
#ifdef __cplusplus
}
#endif
//#else
//#error dev_bt_ugzzc.h included several times
#endif
