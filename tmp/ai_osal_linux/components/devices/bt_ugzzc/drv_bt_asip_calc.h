/*****************************************************************************
* Copyright (C) Bosch Car Multimedia GmbH, 2010
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
*****************************************************************************/
/*!
*\file     drv_bt_asip_protocol.c
*\brief    This file contains the OSAL BT specific header file    
*
*\author:  Bosch Car Multimedia GmbH, CM-AI/PJ-V32, Klaus-Peter Kollai, 
*                                       Klaus-Peter.Kollai@de.bosch.com
*\par Copyright: 
*(c) 2010 Bosch Car Multimedia GmbH
*\par History:
* Created - 23/07/10 
**********************************************************************/

#ifndef DRV_BT_ASIP_CALC_H
#define DRV_BT_ASIP_CALC_H

tU16 drvBtAsipCrc( tU8 *buf , tU16 len );
tU8 drvBtAsipChksum( tU8 * buf , tU16 len );

#endif //DRV_BT_ASIP_CALC_H
