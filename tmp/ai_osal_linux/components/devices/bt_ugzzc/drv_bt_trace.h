/*******************************************************************************
*
* FILE:
*     drv_bt_trace.h
*
* REVISION:
*     1.0
*
* AUTHOR:
*     (c) 2005, Robert Bosch India Ltd., ECM/ECM1, Vijay D, 
*                                                  vijay.d@in.bosch.com
*
* CREATED:
*     17/05/2007 - Vijay D 
*
* DESCRIPTION:
*     This file contains all the definitions, macros, and types that 
*     are private to the drv_bt_trace.c. 
*
* NOTES:
*
* MODIFIED:
*
*
*******************************************************************************/
#ifndef _DRV_BT_TRACE_H
#define _DRV_BT_TRACE_H

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
//#define BT_NUNET_ENABLED
/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/* Trace messages */
typedef enum 
{
   TR_DRV_BT_MSG_ERROR,
   TR_DRV_BT_MSG_INFO,
   TR_DRV_BT_ERR_UART_OPEN,
   TR_DRV_BT_MSG_BT_UGZZC_READ,
   TR_DRV_BT_MSG_BT_UGZZC_WRITE,
   TR_DRV_BT_MSG_BT_NET_READ,
   TR_DRV_BT_MSG_BT_NET_WRITE,
   TR_DRV_BT_MSG_BT_STRING

} tenDrvBTTraceMsg;

/* Trace errors enumeration */
typedef enum 
{
   TR_DRV_BT_ERR_NULL_BUF_INVALID_DEVICE,
   TR_DRV_BT_ERR_WRITE,
   TR_DRV_BT_ERR_READ

} tenDrvBTTraceError;

/* Trace functions enumeration */
typedef enum 
{
   TR_DRV_BT_IO_OPEN,
   TR_DRV_BT_IO_CLOSE,
   TR_DRV_BT_IOCTRL,
   TR_DRV_BT_IO_WRITE,
   TR_DRV_BT_IO_READ,
   TR_DRV_BT_STRING
} tenDrvBTTraceFunction;

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/
 extern tBool              _bHciMode;
 extern tU8                u8TxTestMode;
 extern tBool              _bIgnoreResetTrigger;

/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/

 /*****************************************************************************
*
* FUNCTION:
*     drv_com_vTraceInfo
*
* DESCRIPTION:
*     This function creates the trace message and sends it to PC
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
* HISTORY:
*
*****************************************************************************/
extern tVoid drv_bt_vTraceInfo  (TR_tenTraceLevel enTraceLevel, tenDrvBTTraceFunction enDrvBTFunction, tenDrvBTTraceMsg enDrvBTTraceMsg, tPCChar copchDescription, tS32 s32Par1, tS32 s32Par2, tS32 s32Par3);
extern tVoid drv_bt_vTraceBuf ( TR_tenTraceLevel enTraceLevel, tenDrvBTTraceMsg enDrvBTTraceMsg, tU8* pu8TxBuf, tU32 u32Len );
extern tVoid drv_bt_vPrintf(const char *pcFormat, ...);
extern tVoid drv_bt_vTraceBufNet ( TR_tenTraceLevel enTraceLevel, tenDrvBTTraceMsg enDrvBTTraceMsg, tU8 u8ModemMode, tU8* pu8Buf, tU32 u32Len );


#endif /* #ifndef _DRV_BT_TRACE_H */






