/*****************************************************************************
* Copyright (C) Bosch Car Multimedia GmbH, 2010
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
*****************************************************************************/
/*!
*\file     drv_bt_spp.cpp
*\brief    This file contains the code for the DRV_BT module    
*
*\author:  Bosch Car Multimedia GmbH, CM-AI/PJ-VW32, Bosse Arndt, 
*                                       external.bosse.arndt@de.bosch.com
*\par Copyright: 
*(c) 2010 Bosch Car Multimedia GmbH
*\par History:
* Created - 01/02/12 
**********************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>

/* OSAL Interface */
#define OSAL_S_IMPORT_INTERFACE_GENERIC		   
#include "osal_if.h"

#include "drv_bt_trace.h"
#include "drv_bt_asip_protocol.h"
#include "drv_bt_spp.h"
#include "drv_bt_spp_socket.h"

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#ifndef DRV_BT_START_BYTE_FOR_SPP_DATA
#define DRV_BT_START_BYTE_FOR_SPP_DATA 6
#define DRV_BT_PARAMLENGH 2
#endif
#define os_printf_errmem printf
#define drv_bt_vPrintf printf

#ifdef BT_SPP_ENABLED
#define SPP_FUNCTIONALITY 0x01


//debugging traces
#define DEBUG_V_DRV_BT_SPPDEVICECONNECT
// #define DEBUG_V_DRV_BT_SPPDEVICECONNECT_2
// #define DEBUG_V_DRV_BT_SENDBTDATAVIASPP
#define DEBUG_V_DRV_BT_SPPDATARECEIVED
//#define DEBUG_V_DRV_BT_SPPDATARECEIVED_2
/************************************************************************
| variable inclusion  (scope: global)
|-----------------------------------------------------------------------*/
extern tBool _bDrvBtTerminate;
extern tS32 s32DrvBt_newsockfd;
/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
tU8 caAtCommandTmpBuffer[400]={0};
tU16 u16AtTmpCounter = 0;

tU8  _u8Buf[ASIP_PAYLOAD_DATA_SIZE]={0};
tU8 cCurRcvByte = 0;

tBool _bDunSkipRequest = FALSE;
static tU32 u32LastDunNetRequest = 0xffffffff;

tU32 _u32UpStreamDataCount = 0;
tU32 _u32DownStreamDataCount = 0;
/**/
tBool _bDrvBtSocketWork=FALSE;

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/***************************************************************************
* FUNCTION
*
*    v_drv_bt_SPPdeviceConnect
*
* DESCRIPTION
*
*    Transfers NuNeT data via blutooth DUN (Dial Up Network).
*
* INPUTS
*
*    tU8 *rxBuf         :   Pointer to data received via SPP.
*
* OUTPUTS
*
*    none
*
****************************************************************************/

tVoid v_drv_bt_SPPdeviceConnect(tU16 tU16_Opcode, tU16 u16lenght, tU8* rxBuf)
{
	tU8 u8_status;
	tU8 u8_function;
	tU16 i;
	
	#ifdef DEBUG_V_DRV_BT_SPPDEVICECONNECT_2
	drv_bt_vPrintf("DrvBt: \033[31mv_drv_bt_SPPdeviceConnect:\033[0m Opcode: \033[31m %04x\033[0m Buf: ",tU16_Opcode);
	for ( i = 0 ; i < u16lenght ; i++ ){
		drv_bt_vPrintf ("%02x ",rxBuf[i]);
	}
	drv_bt_vPrintf("\n");
	#endif //DEBUG_V_DRV_BT_SPPDEVICECONNECT_2
	
	u8_status = rxBuf[0];
	u8_function = rxBuf[u16lenght-1];

	if ((u8_function & SPP_FUNCTIONALITY) == SPP_FUNCTIONALITY){
		switch (tU16_Opcode){
			case BT_APPL_DEVICE_CONNECT:
				if (u8_status == 0x00){
					#ifdef DEBUG_V_DRV_BT_SPPDEVICECONNECT
					drv_bt_vPrintf("DrvBt: \033[31mv_drv_bt_SPPdeviceConnect:\033[0m Status SPP Connect: \033[31msuccess\033[0m %02x\n",u8_status);
					#endif //DEBUG_V_DRV_BT_SPPDEVICECONNECT
					_bDrvBtSocketWork = TRUE;
				} else {
					#ifdef DEBUG_V_DRV_BT_SPPDEVICECONNECT
					drv_bt_vPrintf("DrvBt: \033[31mv_drv_bt_SPPdeviceConnect:\033[0m Status SPP Connect: \033[31mfailed\033[0m %02x\n",u8_status);
					#endif //DEBUG_V_DRV_BT_SPPDEVICECONNECT
				}
			break;
			case BT_APPL_DEVICE_DISCONNECT:
				#ifdef DEBUG_V_DRV_BT_SPPDEVICECONNECT
				drv_bt_vPrintf("DrvBt: \033[31mv_drv_bt_SPPdeviceConnect:\033[0m Status SPP Disconnect: \033[31msuccess\033[0m %02x\n",u8_status);
				#endif //DEBUG_V_DRV_BT_SPPDEVICECONNECT
				_bDrvBtSocketWork = FALSE;
				close(s32DrvBt_newsockfd);
			break;
		}
	}
	return;
}

/***************************************************************************
* FUNCTION
*
*    v_drv_bt_SendBtDataViaSPP
*
* DESCRIPTION
*
*    Transfers NuNeT data via blutooth DUN (Dial Up Network).
*
* INPUTS
*
*    tU8 *txBuf         :   Pointer to data to be written via SPP.
*    tU32 len           :   Number of data to be written
*
* OUTPUTS
*
*    none
*
****************************************************************************/
tVoid  v_drv_bt_SendBtDataViaSPP(tU8 *txBuf, tU32 len)
{

   //here we have to create SPP message and send via BT
   //set opcode
	tU8 u8Mode = BT_FORCE_SEND;
	tU8 message_buffer[550];
	tU32 i;
   tU32 u32ErrorCounter = 0;

   tU32 u32PayloadLen = len+DRV_BT_START_BYTE_FOR_SPP_DATA+DRV_BT_PARAMLENGH;
   tU32 u32SPPDataLen = u32PayloadLen-DRV_BT_PARAMLENGH;
	
	/*{0x00,0x62,
		0x00,0x13,
		0x00,0x11,
		0x7E,0x00,0x00,0x00,0x00,0x00,0x01,0x31,0xA1,0x2D,0x7C};*/
	/*	0xa0 0x14 0x48 0xfc 0x11 0x00 0x00
		0x00 0x62 
		0x00 0x13 
		0x00 0x11 
		0x7e 0x00 0x00 0x00 0x00 0x00 0x01 0x30 0xf4 0xac 0x7c
		0x07 0x6a};*/
	/* 0xd0 0x14 0x78 0x5c 0x10 0x00 0x00
		0x00 0x63
		0x0c 0x00
		0x00 0x0a
		0x7e 0x01 0x01 0x00 0x00 0x00 0x00 0xe1 0xe1 0x7c 0x00
		0x96 0x62*/

   message_buffer[0] = (tU8)(0x00ff & BT_APPL_SPP_SEND_DATA_REQ);
   message_buffer[1] = (tU8)((0xff00 & BT_APPL_SPP_SEND_DATA_REQ)>>8);

   message_buffer[2] = (tU8)(0x00ff & u32PayloadLen);
   message_buffer[3] = (tU8)((0xff00 & u32PayloadLen)>>8);

   message_buffer[4] = (tU8)((0xff00 & u32SPPDataLen)>>8);
	message_buffer[5] = (tU8)(0x00ff & u32SPPDataLen);
	
	for ( i = 0 ; i <= len ; i++)
		message_buffer[ i + DRV_BT_START_BYTE_FOR_SPP_DATA] = txBuf[i];
	#ifdef DEBUG_V_DRV_BT_SENDBTDATAVIASPP
	drv_bt_vPrintf("DrvBt: v_drv_bt_SendBtDataViaSPP: RawData: ");
	for ( i = 0; i < (DRV_BT_START_BYTE_FOR_SPP_DATA + len) ; i++ ){
		if ( i <= 1 )
			drv_bt_vPrintf("\033[32m%02x\033[0m",message_buffer[i]);
		else if ( 5 < i && i <= len+DRV_BT_START_BYTE_FOR_SPP_DATA)
			drv_bt_vPrintf("\033[33m %02x\033[0m",message_buffer[i]);
		else 
			drv_bt_vPrintf("\033[0m %02x\033[0m",message_buffer[i]);
	}
	drv_bt_vPrintf("\n");
	#endif //DEBUG_V_DRV_BT_SENDBTDATAVIASPP
	if (s32DrvBtAsipSendData((tU8 *) &message_buffer, (len+DRV_BT_START_BYTE_FOR_SPP_DATA), u8Mode) < len+DRV_BT_START_BYTE_FOR_SPP_DATA)
		drv_bt_vPrintf("DrvBt:\033[31m v_drv_bt_SendBtDataViaSPP: s32DrvBtAsipSendData: messagebytescount too low,%u \033[0m\n",len);
   //os_printf("DrvBt: v_drv_bt_SendBtDataViaSPP()-> UpStream: %d", _u32UpStreamDataCount);
	return;
}

/****************************************************************************
* FUNCTION
*
*    v_drv_bt_SPPDataReceived
*
* DESCRIPTION
*
*
* INPUTS
*
*
* OUTPUTS
*
*
****************************************************************************/
tVoid v_drv_bt_SPPDataReceived(tU16 LastOpcode, tU8* pBuf, tU16 u16Len) 
{
	tU32 u32_written_bytes;
	if (!_bDrvBtTerminate){
		if (LastOpcode == BT_APPL_SPP_DATA_IND){
			u32_written_bytes = write(s32DrvBt_newsockfd,pBuf,u16Len);
			//u32_written_bytes = send(s32DrvBt_newsockfd,pBuf,u16Len,MSG_WAITALL);
			if (u32_written_bytes < 0){
				#ifdef DEBUG_V_DRV_BT_SPPDATARECEIVED
				drv_bt_vPrintf("DrvBt: \033[31mv_drv_bt_SPPDataReceived: ERROR writing to socket\033[0m\n");
				#endif //DEBUG_V_DRV_BT_SPPDATARECEIVED
			} else {
				#ifdef DEBUG_V_DRV_BT_SPPDATARECEIVED
				drv_bt_vPrintf("DrvBt: \033[31mv_drv_bt_SPPDataReceived:\033[0m Opcode: \033[32m%04x\033[0m Buf: ",LastOpcode);
				for ( tU16 i = 0 ; i < u16Len ; i++ ){
					drv_bt_vPrintf ("\033[33m%02x \033[0m",pBuf[i]);
				}
				drv_bt_vPrintf("\n");		
				drv_bt_vPrintf("DrvBt: \033[31mv_drv_bt_SPPDataReceived: writing to socket worked 0x%x\033[0m\n",u32_written_bytes);
				#endif //DEBUG_V_DRV_BT_SPPDATARECEIVED
			}
		}
		#ifdef DEBUG_V_DRV_BT_SPPDATARECEIVED_2
		drv_bt_vPrintf("DrvBt:\033[31m v_drv_bt_SPPDataReceived: no suitable SPP data received\033[0m\n");
		#endif //DEBUG_V_DRV_BT_SPPDATARECEIVED_2
	}	
	return;
}
#ifdef NO_USE
tBool drv_bt_SPP_UUID_query(tU8* pBuf)//, tU16 u16Len)
{
	tU8 UUID[16] = {0x45,0x39,0x94,0xd5,0xd5,0x8b,0x96,0xf9,0x66,0x16,0xb3,0x7f,0x58,0x6b,0xa2,0xec};//= *pBuf;
	tU8 frame_buffer_UUID[40] = {0x05,0x62,0x00,0x24,0x00,0x22,0x03,0x45,0x39,0x94,0xd5,0xd5,0x8b,0x96,0xf9,0x66,0x16,0xb3,0x7f,0x58,0x6b,0xa2,0xec,0x03,0x45,0x39,0x94,0xd5,0xd5,0x8b,0x96,0xf9,0x66,0x16,0xb3,0x7f,0x58,0x6b,0xa2,0xec};
	tU8 frame_buffer_PDNR_STATUS_PAUSE[17] = {0x00,0x62,0x00,0x13,0x00,0x11,0x7E,0x00,0x00,0x00,0x00,0x00,0x01,0x31,0xA1,0x2D,0x7C};
	tU8 frame_buffer_PNDR_EVENT_TRACK_PLAY[17] = {0x00,0x62,0x00,0x13,0x00,0x11,0x7E,0x00,0x00,0x00,0x00,0x00,0x01,0x30,0xF4,0xAC,0x7C};
	00 62 00 13 00 11 7E 00 00 00 00 00 01 30 F4 AC 7C
	tenDrvBTTraceMsg enDrvBTTraceMsg;
	tBool status = FALSE;
	/*
		Abfolge f�r erfolgreiche SPP RFCOMM verbindung:
		1. invisible schalten ...kein Handy darf verbunden sein
		2. SPP komplett deaktivieren
		3. UUID setzen aus file setzen
		4. SPP aktivieren
		5. mit Handy verbinden (kann auch schon gepairt sein)
		6. SPP explizit nochmal connecten
		7 daten via SPP_DATA austauschen
		Die Phonecomponente sollte nicht gest�rt werden...tbv
		Trace:
		DrvBt: UGZZC RawData: a0 14 48 fc 11 00 00 00 62 00 13 00 11 7e 00 00 00 00 00 01 30 f4 ac 7c 07 6a
		DrvBt: SPP: v_drv_bt_SPP_UUID_query(): send PNDR_EVENT_TRACK_PLAY to APP
		DrvBt: dwThreadDrvBt_send_UUID wait 1s for trigger
		DrvBt: UGZZC RawData: d0 08 48 20 05 00 00 00 62 01 00 00 b7 6d
		DrvBt: SPP_DATA_CFM detected: opcode: 0x6200
		DrvBt: UGZZC RawData: d0 14 58 3c 10 00 00 00 63 0c 00 00 0a 7e 01 01 00 00 00 00 e1 e1 7c 00 48 8e
		DrvBt: SPP_DATA_IND detected: opcode: 0x6300
		DrvBt: SPP_LINT_STATUS_IND detected: opcode: 0x6300
		DrvBt: SPP_BAUDRATE_SET_RESULT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_CONNECT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_DISCONNECT_IND detected: opcode: 0x6300
		DrvBt: dwThreadDrvBt_send_UUID pass job to SPP sender
		DrvBt: UGZZC RawData: a0 14 58 0c 11 00 00 00 62 00 13 00 11 7e 00 00 00 00 00 01 30 f4 ac 7c 1e c0
		DrvBt: SPP: v_drv_bt_SPP_UUID_query(): send PNDR_EVENT_TRACK_PLAY to APP
		DrvBt: dwThreadDrvBt_send_UUID wait 1s for trigger
		DrvBt: UGZZC RawData: d1 00 58 29 16 46
		DrvBt: UGZZC RawData: d0 08 68 40 05 00 00 00 62 01 00 00 58 d0
		DrvBt: SPP_DATA_CFM detected: opcode: 0x6200
		DrvBt: UGZZC RawData: d0 14 78 5c 10 00 00 00 63 0c 00 00 0a 7e 01 01 00 00 00 00 e1 e1 7c 00 96 62
		DrvBt: SPP_DATA_IND detected: opcode: 0x6300
		DrvBt: SPP_LINT_STATUS_IND detected: opcode: 0x6300
		DrvBt: SPP_BAUDRATE_SET_RESULT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_CONNECT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_DISCONNECT_IND detected: opcode: 0x6300
		DrvBt: UGZZC RawData: d1 00 68 39 35 e0
		DrvBt: dwThreadDrvBt_send_UUID pass job to SPP sender
		DrvBt: UGZZC RawData: a0 14 78 2c 11 00 00 00 62 00 13 00 11 7e 00 00 00 00 00 01 30 f4 ac 7c 1d 9f
		DrvBt: SPP: v_drv_bt_SPP_UUID_query(): send PNDR_EVENT_TRACK_PLAY to APP
		DrvBt: dwThreadDrvBt_send_UUID wait 1s for trigger
		DrvBt: UGZZC RawData: d1 00 78 49 23 06
		DrvBt: UGZZC RawData: d0 08 18 f0 05 00 00 00 62 01 00 00 be 33
		DrvBt: SPP_DATA_CFM detected: opcode: 0x6200
		DrvBt: UGZZC RawData: d0 14 28 0c 10 00 00 00 63 0c 00 00 0a 7e 01 01 00 00 00 00 e1 e1 7c 00 19 77
		DrvBt: SPP_DATA_IND detected: opcode: 0x6300
		DrvBt: SPP_LINT_STATUS_IND detected: opcode: 0x6300
		DrvBt: SPP_BAUDRATE_SET_RESULT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_CONNECT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_DISCONNECT_IND detected: opcode: 0x6300
		DrvBt: dwThreadDrvBt_send_UUID pass job to SPP sender
		DrvBt: UGZZC RawData: a0 14 08 bc 11 00 00 00 62 00 13 00 11 7e 00 00 00 00 00 01 30 f4 ac 7c 01 d4
		DrvBt: SPP: v_drv_bt_SPP_UUID_query(): send PNDR_EVENT_TRACK_PLAY to APP
		DrvBt: dwThreadDrvBt_send_UUID wait 1s for trigger
		DrvBt: UGZZC RawData: d1 00 08 d9 6e 62
		DrvBt: UGZZC RawData: d0 08 38 10 05 00 00 00 62 01 00 00 86 4a
		DrvBt: SPP_DATA_CFM detected: opcode: 0x6200
		DrvBt: UGZZC RawData: d0 14 48 2c 10 00 00 00 63 0c 00 00 0a 7e 01 01 00 00 00 00 e1 e1 7c 00 c1 25
		DrvBt: SPP_DATA_IND detected: opcode: 0x6300
		DrvBt: SPP_LINT_STATUS_IND detected: opcode: 0x6300
		DrvBt: SPP_BAUDRATE_SET_RESULT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_CONNECT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_DISCONNECT_IND detected: opcode: 0x6300
		DrvBt: dwThreadDrvBt_send_UUID pass job to SPP sender
		DrvBt: UGZZC RawData: a0 14 28 dc 11 00 00 00 62 00 13 00 11 7e 00 00 00 00 00 01 30 f4 ac 7c df 38
		DrvBt: SPP: v_drv_bt_SPP_UUID_query(): send PNDR_EVENT_TRACK_PLAY to APP
		DrvBt: dwThreadDrvBt_send_UUID wait 1s for trigger
		DrvBt: UGZZC RawData: d0 08 68 40 05 00 00 00 62 01 00 00 58 d0
		DrvBt: SPP_DATA_CFM detected: opcode: 0x6200
		DrvBt: UGZZC RawData: d0 14 78 5c 10 00 00 00 63 0c 00 00 0a 7e 01 01 00 00 00 00 e1 e1 7c 00 96 62
		DrvBt: SPP_DATA_IND detected: opcode: 0x6300
		DrvBt: SPP_LINT_STATUS_IND detected: opcode: 0x6300
		DrvBt: SPP_BAUDRATE_SET_RESULT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_CONNECT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_DISCONNECT_IND detected: opcode: 0x6300
		DrvBt: dwThreadDrvBt_send_UUID pass job to SPP sender
		DrvBt: UGZZC RawData: a0 14 38 ec 11 00 00 00 62 00 13 00 11 7e 00 00 00 00 00 01 30 f4 ac 7c b0 4e
		DrvBt: SPP: v_drv_bt_SPP_UUID_query(): send PNDR_EVENT_TRACK_PLAY to APP
		DrvBt: dwThreadDrvBt_send_UUID wait 1s for trigger
		DrvBt: UGZZC RawData: d1 00 38 09 41 02
		DrvBt: UGZZC RawData: d0 08 08 e0 05 00 00 00 62 01 00 00 78 1e
		DrvBt: SPP_DATA_CFM detected: opcode: 0x6200
		DrvBt: UGZZC RawData: d0 14 18 fc 10 00 00 00 63 0c 00 00 0a 7e 01 01 00 00 00 00 e1 e1 7c 00 e5 5f
		DrvBt: SPP_DATA_IND detected: opcode: 0x6300
		DrvBt: SPP_LINT_STATUS_IND detected: opcode: 0x6300
		DrvBt: SPP_BAUDRATE_SET_RESULT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_CONNECT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_DISCONNECT_IND detected: opcode: 0x6300
		DrvBt: UGZZC RawData: d1 00 48 19 04 e2
		DrvBt: dwThreadDrvBt_send_UUID pass job to SPP sender
		DrvBt: UGZZC RawData: a0 14 68 1c 11 00 00 00 62 00 13 00 11 7e 00 00 00 00 00 01 30 f4 ac 7c 72 e9
		DrvBt: SPP: v_drv_bt_SPP_UUID_query(): send PNDR_EVENT_TRACK_PLAY to APP
		DrvBt: dwThreadDrvBt_send_UUID wait 1s for trigger
		DrvBt: UGZZC RawData: d0 08 48 20 05 00 00 00 62 01 00 00 b7 6d
		DrvBt: SPP_DATA_CFM detected: opcode: 0x6200
		DrvBt: UGZZC RawData: d0 14 58 3c 10 00 00 00 63 0c 00 00 0a 7e 01 01 00 00 00 00 e1 e1 7c 00 48 8e
		DrvBt: SPP_DATA_IND detected: opcode: 0x6300DrvBt: SPP_LINT_STATUS_IND detected: opcode: 0x6300
		DrvBt: SPP_BAUDRATE_SET_RESULT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_CONNECT_IND detected: opcode: 0x6300
		DrvBt: SPP_AUDIO_DISCONNECT_IND detected: opcode: 0x6300
		DrvBt: dwThreadDrvBt_send_UUID wait 1s for trigger
	*/
}


tU32 u32_drv_bt_SPPGetUpStreamCount(){
   tU32 u32Ret =  _u32UpStreamDataCount;
   os_printf("DrvBt: u32_drv_bt_SPPGetUpStreamCount()-> %d", u32Ret);
   _u32UpStreamDataCount = 0;
   return u32Ret;
}

tU32 u32_drv_bt_SPPGetDownStreamCount(){
   tU32 u32Ret =  _u32DownStreamDataCount;
   os_printf("DrvBt: u32_drv_bt_SPPGetDownStreamCount()-> %d", u32Ret);
   _u32DownStreamDataCount = 0;
   return u32Ret;
}

/*****************************************************************************
*
* FUNCTION:
*     tVoid dwThreadUgzzc( tPVoid pvArg )
*
* DESCRIPTION:
*     Thread to test read interface. Thread needed to generate message to ALPS module.
*     All acknowledges will be send in context of the read thread
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
* HISTORY:
*
*****************************************************************************/
tVoid v_drv_bt_BtSPPReadyToSend() {
   if (bNuNetMsg2Send) {
      bNuNetMsg2Send = FALSE;
      if (pUartRef->communication_mode == MDM_NETWORK_COMMUNICATION) {
         /* Add this device to the list of PPP devices that have finished
         sending a packet. */
         _ppp_tx_dev_ptr_queue [_ppp_tx_dev_ptr_queue_write++] = tModemDevice;

         /* Activate the HISR that will take care of processing the
         next packet in queue, if one is ready. */
         NU_Activate_HISR (&PPP_TX_HISR);

         /* Check for wrap of ring buffer. */

         _ppp_tx_dev_ptr_queue_write %= PPP_MAX_TX_QUEUE_PTRS;
      }
   }
}
tVoid v_drv_bt_SendViaSPP(tU16 u16Len) {

   tU32 u32MsgLen = u16Len+DRV_BT_START_BYTE_FOR_SPP_DATA;

   drv_bt_vPrintf(TR_LEVEL_USER_1, "DrvBt: New TX SPP Message with len: %d", u32MsgLen);

   _u32UpStreamDataCount += u16Len;

   //drv_bt_vTraceBufNet(TR_LEVEL_USER_3, TR_DRV_BT_MSG_BT_NET_WRITE, uart->communication_mode, u8TmpBuf, u16Len);

   //and now send via BT
   if (ASIP_PAYLOAD_DATA_SIZE < u32MsgLen) {
      NORMAL_M_ASSERT(FALSE);
   }

   if (_bSPPSkipRequest) {
      drv_bt_vPrintf(TR_LEVEL_USER_1, "DrvBt: Skip this message: %d", _bSPPSkipRequest);
      v_drv_bt_BtSPPReadyToSend();
   } else {
      v_drv_bt_SendBtDataViaSPP(&_u8Buf[0], u16Len+DRV_BT_START_BYTE_FOR_SPP_DATA);
   }
}
#endif

#endif //BT_SPP_ENABLED
/************************************************************************
|end of file drv_bt_spp.c
*/
