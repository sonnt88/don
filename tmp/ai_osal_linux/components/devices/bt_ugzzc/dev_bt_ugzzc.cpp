/*****************************************************************************
* Copyright (C) Bosch Car Multimedia GmbH, 2010
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
*****************************************************************************/
/*!
*\file     drv_bt_ugzzc.c
*\brief    This file contains the code for the DRV_BT module    
*
*\author:  Bosch Car Multimedia GmbH, CM-AI/PJ-V32, Bosse Arndt, 
*                                       external.bosse.arndt@de.bosch.com
*\par Copyright: 
*(c) 2011 Bosch Car Multimedia GmbH
*\par History:
* Created - 05/12/2011
**********************************************************************/
/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#include <ctype.h>
#include <string.h>
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#if OSAL_OS==OSAL_TENGINE
  #include "OsalmpSubSystem.h"
#endif
#include "Linux_osal.h"

#include "dev_bt_ugzzc.h"

#include "drv_bt_trace.h"
#include "drv_bt_asip_calc.h"
#include "drv_bt_asip_protocol.h"
#include "drv_bt_spp.h"
#include "drv_bt_spp_socket.h"

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define OSAL_C_S32_IOCTRL_BT_UGZZC_PARAM_ERROR 1
#define BT_UGZZC_C_S32_IO_VERSION   (tS32)(0xAA050509)
#define TR_TTFIS_DRV_BT 123
#define UART_4 0

#define drv_bt_vPrintf printf 
//#define DEBUG_UGZZC
/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/
extern OSAL_tThreadID _hDrvBtThreadID_SPPSS;
/****************************************************************************/
/*! 
*\fn      tS32 BT_UGZZC_s32IODeviceInit(tVoid)
*
*\brief   Calls the drv_com init fn
*
*\return  	tS32: OSAL_E_NOERROR 		-> No Error
*
*\par History:  
*
****************************************************************************/
tS32 BT_UGZZC_s32IODeviceInit(tVoid){
	#ifdef DEBUG_UGZZC
	drv_bt_vPrintf("DrvBt: i will start the device...make sure it doesn't make bummm\n");
	BT_UGZZC_IOOpen();
	#endif
  	tS32 s32Status = OSAL_E_NOERROR;
   TR_tenTraceChan eChan = (TR_tenTraceChan) TR_TTFIS_DRV_BT;
	drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_OPEN, TR_DRV_BT_MSG_INFO, "DrvBt: BT_UGZZC_s32IODeviceInit", 0,0,0);
	#ifdef ichhabekeineahnungwodasreferenziertwird
	drv_bt_vRegisterTraceCallBack(eChan);
	#endif
   
	//hardware UGZZC initializer
	bDrvBtAsipInit();
   return(s32Status);
}

/****************************************************************************/
/*! 
*\fn      tS32 BT_UGZZC_s32IODeviceRemove(tVoid)
*
*\brief   Calls the drv_com remove fn
*
*\return  	tS32: OSAL_E_NOERROR 		-> No Error
*
*\par History:  
*
****************************************************************************/
tS32 BT_UGZZC_s32IODeviceRemove(tVoid){
	#ifdef DEBUG_UGZZC
	drv_bt_vPrintf("DrvBt: i will close the device now...hopefully it didn't make bummm\n");
	BT_UGZZC_s32IOClose();
	#endif
	
	drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_OPEN, TR_DRV_BT_MSG_INFO, "DrvBt: BT_UGZZC_s32IODeviceRemove", 0,0,0);
	#ifdef ichhabekeineahnungwodasreferenziertwird
	drv_bt_vUnregisterTraceCallBack();
	#endif
	
   //hardware UGZZC destroyer
	bDrvBtAsipDeInit();
	return OSAL_E_NOERROR;
}

/****************************************************************************/
/*! 
*\fn      tS32 BT_UGZZC_IOOpen(tVoid)
*
*\brief   Calls the drv_com Open fn 
*
*\return  tS32: OSAL_E_NOERROR 		-> No Error
*				OSAL_E_INVALIDVALUE	-> Invalid value passed to function
*\par History:  
*
****************************************************************************/
tS32 BT_UGZZC_IOOpen(tVoid){
   tS32 s32Status = OSAL_E_INVALIDVALUE;

   drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_OPEN, TR_DRV_BT_MSG_INFO, "DrvBt: Entry_IOOpen", 0 ,0,0);

	s32Status  = OSAL_E_NOERROR;
	if( FALSE == drv_bt_lld_uart_init(UART_4, 115200) ){
		drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_OPEN, TR_DRV_BT_MSG_ERROR, "DrvBt: ERROR!", 0, 0, 0);
		s32Status = OSAL_E_UNKNOWN;
	} else {
		//set BT mode
		_bHciMode = FALSE;
		_bDrvClosed = FALSE;

		if(_hDrvBtThreadID != OSAL_ERROR){
			if (OSAL_OK != OSAL_s32ThreadResume(_hDrvBtThreadID)){
				//not an error -> thread is already running
				drv_bt_vTraceInfo(TR_LEVEL_USER_1, TR_DRV_BT_IO_CLOSE, TR_DRV_BT_MSG_ERROR, "DrvBt: Thread resume failed!", 0, 0, 0);
			}
		} else {
			vCreateThread();
		}
		#ifdef BT_SPP_ENABLED
		if(_hDrvBtThreadID_SPPSS != OSAL_ERROR){
			if (OSAL_OK != OSAL_s32ThreadResume(_hDrvBtThreadID_SPPSS)){
				//not an error -> thread is already running
				drv_bt_vTraceInfo(TR_LEVEL_USER_1, TR_DRV_BT_IO_CLOSE, TR_DRV_BT_MSG_ERROR, "DrvBt: Thread resume failed!", 0, 0, 0);
			}
		} else {
			vCreateThread_SPP_Socketserver();
		}
		#endif
	}
	//drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_OPEN, TR_DRV_BT_MSG_INFO,"DrvBt: Exit_IOOpen", 0,0,0);	

   return s32Status;
}

/****************************************************************************/
/*! 
*\fn      tS32 BT_UGZZC_s32IOClose( tVoid )
*
*\brief   Calls the drv_com Close fn
*
*
*\return  tS32: OSAL_E_NOERROR 		-> No Error
*				OSAL_E_INVALIDVALUE	-> Invalid value passed to function
*
*\par History:  
*
****************************************************************************/
tS32 BT_UGZZC_s32IOClose(tVoid){
	tS32 s32Status = OSAL_E_INVALIDVALUE;
	
	drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_CLOSE, TR_DRV_BT_MSG_INFO, "DrvBt: Entry_s32IOClose", 0,0,0);
	s32Status  = OSAL_E_NOERROR;
	_bDrvClosed = TRUE;

	if (OSAL_s32ThreadSuspend(_hDrvBtThreadID) != OSAL_OK) {
		drv_bt_vTraceInfo(TR_LEVEL_USER_1, TR_DRV_BT_IO_CLOSE, TR_DRV_BT_MSG_ERROR, "DrvBt: Thread suspend failed!", 0, 0, 0);
	}
	#ifdef BT_SPP_ENABLED
	if (OSAL_s32ThreadSuspend(_hDrvBtThreadID_SPPSS) != OSAL_OK) {
		drv_bt_vTraceInfo(TR_LEVEL_USER_1, TR_DRV_BT_IO_CLOSE, TR_DRV_BT_MSG_ERROR, "DrvBt: Thread suspend failed!", 0, 0, 0);
	}
	#endif //BT_SPP_ENABLED
	if( drv_bt_lld_uart_close(UART_4) == FALSE ){
		drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_CLOSE, TR_DRV_BT_MSG_ERROR, "DrvBt: ERROR!", 0, 0, 0);
		s32Status = OSAL_E_UNKNOWN;
	}
	//set the BT module in reset

	//drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_CLOSE, TR_DRV_BT_MSG_INFO,"DrvBt: Exit_s32IOClose", 0,0,0);
	
	return s32Status;
}

/****************************************************************************/
/*! 
*\fn      tS32 BT_UGZZC_s32IOControl(tS32 Ps32fun, tS32 Ps32arg)
*
*\brief   Calls the drv_com IOCtrl fn 
*
*\param   tS32 Ps32fun	:   Function identificator                         
*         tS32 Ps32arg	:   Argument to be passed to function                   
*         
*\return  tS32: OSAL_E_NOERROR 		-> No Error
*               OSAL_E_NOTSUPPORTED -> Ps32arg not supported by the fn
*					 OSAL_E_WRONGFUNC		-> Wrong Function
*\par History:  
*
*****************************************************************************/
tS32 BT_UGZZC_s32IOControl(tS32 Ps32fun, tS32 Ps32arg){
	tS32 s32Status = OSAL_E_NOERROR;

   drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IOCTRL, TR_DRV_BT_MSG_INFO,"DrvBt: Entry_s32IOControl", 0,0,0);

	switch (Ps32fun){
	case OSAL_C_S32_IOCTRL_VERSION:
		{
			if((tPS32)Ps32arg != NULL){
				*((tPS32)Ps32arg) = BT_UGZZC_C_S32_IO_VERSION;
			}else{
				s32Status = OSAL_C_S32_IOCTRL_BT_UGZZC_PARAM_ERROR;
			}	
		}
		break;
	case OSAL_C_S32_IOCTRL_BT_CPU_RESET_BT:
		vResetUgzzc(FALSE);
		s32Status = OSAL_E_NOERROR;
		break;
	//case OSAL_C_S32_IOCTRL_BT_ACTIVATE_DL_MODE:
	//case OSAL_C_S32_IOCTRL_BT_ACTIVATE_APP_MODE:
	//	s32Status = OSAL_E_NOTSUPPORTED;
	//	break;
	default:
		s32Status = OSAL_E_WRONGFUNC;
		break;
	}
   //drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IOCTRL, TR_DRV_BT_MSG_INFO,"DrvBt: Exit_s32IOControl", 0,0,0);

   return s32Status;
}

/****************************************************************************/
/*! 
*\fn      tS32 BT_UGZZC_s32IOWrite(tPCS8 Pps8Buffer, tU32 Pu32nbytes)
*
*\brief   Calls the drv_com write fn 
*
*\param   ttPCS8 Pps8Buffer	:   Pointer to Tx buffer                      
*         tU32 Pu32nbytes	:   Number of bytes to Tx                        
*         
*\return  tS32: s32Status       	-> No.of written bytes 
*				OSAL_E_INVALIDVALUE	-> Invalid value passed to function
*				OSAL_E_UNKNOWN		-> Functionality failed
 
*\par History:  
*
*****************************************************************************/
tS32 BT_UGZZC_s32IOWrite(tPCS8 Pps8Buffer, tU32 Pu32nbytes){
	tS32 s32Status;
	
	drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_WRITE, TR_DRV_BT_MSG_INFO,"DrvBt: Entry_s32IOWrite", 0,0,0);
	
	if(NULL == Pps8Buffer){
		drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_WRITE, TR_DRV_BT_MSG_ERROR, "DrvBt: ERROR!", (tS32)TR_DRV_BT_ERR_NULL_BUF_INVALID_DEVICE, 0, 0);
		s32Status = OSAL_E_INVALIDVALUE;
	} else {
		s32Status = s32DrvBtAsipSendData((tU8*)Pps8Buffer,Pu32nbytes, BT_NORMAL_MODE);
      if((OSAL_E_UNKNOWN == s32Status) || (OSAL_E_INVALIDVALUE == s32Status)){
			drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_WRITE, TR_DRV_BT_MSG_ERROR, "DrvBt: ERROR!", (tS32)TR_DRV_BT_ERR_WRITE, 0, 0);
		}
	}
	//drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_WRITE, TR_DRV_BT_MSG_INFO, "DrvBt: Exit_s32IOWrite", 0,0,0);
	return s32Status;
}
/****************************************************************************/
/*! 
*\fn      tS32 BT_UGZZC_s32IORead(tPCS8 Pps8Buffer,tU32 Pu32nbytes)
*
*\brief   Calls the drv_com read fn 
*
*\param   ttPCS8 Pps8Buffer	:   Pointer to Rx buffer                      
*         tU32 Pu32nbytes	:   Max number of bytes to Rx                        
*         
*\return  tS32: s32Status   		-> No. of bytes read
*				OSAL_E_INVALIDVALUE	-> Invalid value passed to function
*				OSAL_E_UNKNOWN		-> Functionality failed
 
*\par History:  
*
*****************************************************************************/
tS32 BT_UGZZC_s32IORead(tPCS8 Pps8Buffer,tU32 Pu32nbytes){
	tS32 s32Status;
	
	drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO,"DrvBt: Entry_s32IORead", 0,0,0);
	
	if(NULL == Pps8Buffer)
	{
		drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_ERROR, "DrvBt: ERROR!-> ugzzc_s32IORead 1", (tS32)TR_DRV_BT_ERR_NULL_BUF_INVALID_DEVICE, 0, 0);
		s32Status = OSAL_E_INVALIDVALUE;
	}
	else
	{
      s32Status = s32DrvBtAsipRcvDataSync((tU8*)Pps8Buffer,Pu32nbytes); 
      if((OSAL_E_UNKNOWN == s32Status) || (OSAL_E_INVALIDVALUE == s32Status))
      {
         drv_bt_vTraceInfo(TR_LEVEL_USER_3, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_ERROR, "DrvBt: ERROR!ugzzc_s32IORead 2", (tS32)TR_DRV_BT_ERR_READ, s32Status, 0);
      }
	}
	
	//drv_bt_vTraceInfo(TR_LEVEL_USER_4, TR_DRV_BT_IO_READ, TR_DRV_BT_MSG_INFO,"DrvBt: Exit_s32IORead", 0,0,0);
	
	return s32Status;
}

/* End of File dev_bt_ugzzc.c */
