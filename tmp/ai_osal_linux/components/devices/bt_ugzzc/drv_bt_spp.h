/*******************************************************************************
*
* FILE:
*     drv_bt_spp.h
*
* REVISION:
*     1.0
*
* AUTHOR:
*     (c) 2012, Bosch Car Multimedia GmbH, CM-AI/PJ-VW32, Bosse Arndt, 
*                                                  extrenal.bosse.arndt@de.bosch.com
*
* CREATED:
*     01/02/2012 - Bosse Arndt
*
* DESCRIPTION:
*     This file contains all the definitions, macros, and types that 
*     are private to the drv_bt_spp.cpp. 
*
* NOTES:
*
* MODIFIED:
*
*******************************************************************************/
#ifndef DRV_BT_SPP_H
#define DRV_BT_SPP_H

#ifdef __cplusplus
extern "C" {
#endif

#define BT_APPL_SPP_CON_BAUDRATE_2400					0x00
#define BT_APPL_SPP_CON_BAUDRATE_4800					0x01
#define BT_APPL_SPP_CON_BAUDRATE_7200					0x02
#define BT_APPL_SPP_CON_BAUDRATE_9600					0x03
#define BT_APPL_SPP_CON_BAUDRATE_19200					0x04
#define BT_APPL_SPP_CON_BAUDRATE_38400					0x05
#define BT_APPL_SPP_CON_BAUDRATE_57600					0x06
#define BT_APPL_SPP_CON_BAUDRATE_115200				0x07
#define BT_APPL_SPP_CON_BAUDRATE_230400				0x08

#define BT_APPL_SPP_CON_DATA_FORMAT_DATABITS_5 		0x00
#define BT_APPL_SPP_CON_DATA_FORMAT_DATABITS_6 		0x10
#define BT_APPL_SPP_CON_DATA_FORMAT_DATABITS_7 		0x01
#define BT_APPL_SPP_CON_DATA_FORMAT_DATABITS_8 		0x11

#define BT_APPL_SPP_CON_DATA_FORMAT_STOPBIT_1		0x0
#define BT_APPL_SPP_CON_DATA_FORMAT_STOPBIT_1_5		0x1

#define BT_APPL_SPP_CON_DATA_FORMAT_NONPARITY		0x0
#define BT_APPL_SPP_CON_DATA_FORMAT_PARITY			0x1

#define BT_APPL_SPP_CON_DATA_FORMAT_ODD_PARITY		0x00
#define BT_APPL_SPP_CON_DATA_FORMAT_MARK_PARITY		0x01
#define BT_APPL_SPP_CON_DATA_FORMAT_EVEN_PARITY		0x10
#define BT_APPL_SPP_CON_DATA_FORMAT_SPACE_PARITY	0x11

#define BT_APPL_SPP_CON_DATA_FORMAT_7_BIT				0x0

#define BT_APPL_SPP_CON_FLOW_CONTROL_XFLOWINPUT		0x01
#define BT_APPL_SPP_CON_FLOW_CONTROL_XFLOWOUTPUT	0x02
#define BT_APPL_SPP_CON_FLOW_CONTROL_RTRINPUT		0x04
#define BT_APPL_SPP_CON_FLOW_CONTROL_RTROUTPUT		0x08
#define BT_APPL_SPP_CON_FLOW_CONTROL_RTCINPUT		0x10
#define BT_APPL_SPP_CON_FLOW_CONTROL_RTCOUTPUT		0x20

#define BT_APPL_SPP_CON_FLOW_CONTROL_XONCHAR_OFF	0x00
#define BT_APPL_SPP_CON_FLOW_CONTROL_XONCHAR_ON		0x01

#define BT_APPL_SPP_CON_FLOW_CONTROL_XOFFCHAR_OFF	0x00
#define BT_APPL_SPP_CON_FLOW_CONTROL_XOFFCHAR_ON	0x01

#define BT_APPL_SPP_AUDIO_CONNECT_TYPE_NONE			0x00
#define BT_APPL_SPP_AUDIO_CONNECT_TYPE_HV1			0x01
#define BT_APPL_SPP_AUDIO_CONNECT_TYPE_HV2			0x02
#define BT_APPL_SPP_AUDIO_CONNECT_TYPE_OTHERS		0x03

#define BT_APPL_SPP_AUDIO_DISCONNECT_REASON_BY_LOCAL_DEVICE		0x01
#define BT_APPL_SPP_AUDIO_DISCONNECT_REASON_BY_REMOTE_DEVICE	0x02
#define BT_APPL_SPP_AUDIO_DISCONNECT_REASON_BY_EXEPTION			0x03
#define BT_APPL_SPP_AUDIO_DISCONNECT_REASON_BY_LINK_CLOSE		0x04

#define BT_APPL_SPP_SEND_DATA_REQ 0x6200

extern tBool _bDrvBtSocketWork;
extern tVoid v_drv_bt_SendBtDataViaSPP(tU8 *txBuf, tU32 len);
extern tVoid v_drv_bt_SPPDataReceived(tU16 LastOpcode,tU8* pBuf, tU16 u16Len);

#ifdef __cplusplus
}
#endif

#endif //DRV_BT_SPP_H
