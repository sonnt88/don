/*****************************************************************************
* Copyright (C) Bosch Car Multimedia GmbH, 2010
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
*****************************************************************************/
/*!
*\file     drv_bt_trace.c
*\brief    This file contains the code for the DRV_BT module    
*
*\author:  Bosch Car Multimedia GmbH, CM-AI/PJ-V32, Klaus-Peter Kollai, 
*                                       Klaus-Peter.Kollai@de.bosch.com
*\par Copyright: 
*(c) 2010 Bosch Car Multimedia GmbH
*\par History:
* Created - 23/07/10 
**********************************************************************/
/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "drv_bt_lld_uart.h"

/************************************************************************
| includes of interfaces from external components (scope: global)
|-----------------------------------------------------------------------*/

#include "drv_bt_trace.h"
#include "drv_bt_asip_protocol.h"

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define DRV_BT_MAX_BUF_SIZE   220

//#define debug_raw_data_drv_bt_vTraceBuf
/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/
static tBool              _bTerminate = TRUE;

tBool              _bHciMode = FALSE;

#ifdef BT_NUNET_ENABLED
extern tBool _bDunSkipRequest;
#endif
/*****************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototype (scope: module-global)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/
/*****************************************************************************
*
* FUNCTION:
*     drv_bt_vTraceInfo
*     tVoid drv_bt_vTraceBuf ( TR_tenTraceLevel enTraceLevel, tenDrvBTTraceMsg enDrvBTTraceMsg, tU8* pu8Buf, tU32 u32Len )
*     tVoid drv_bt_vTraceString ( tChar* pcSting)
*     tVoid drv_bt_vPrintf(const char *pcFormat, ...)
*     tVoid drv_bt_vTraceBufNet ( TR_tenTraceLevel enTraceLevel, tenDrvBTTraceMsg enDrvBTTraceMsg, tU8 u8ModemMode, tU8* pu8Buf, tU32 u32Len )
*
* DESCRIPTION:
*     This function creates the trace message and sends it to PC
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
* HISTORY:
*
*****************************************************************************/
tVoid drv_bt_vTraceInfo ( TR_tenTraceLevel enTraceLevel,
                          tenDrvBTTraceFunction enDrvBTFunction,
						  tenDrvBTTraceMsg enDrvBTTraceMsg,
                          tPCChar copchDescription,
                          tS32 s32Par1, 
                          tS32 s32Par2, 
                          tS32 s32Par3 
                         )
{
   //printf("UGZZC trace: %s\n",copchDescription);
	return;
}

tVoid drv_bt_vTraceBuf ( TR_tenTraceLevel enTraceLevel, tenDrvBTTraceMsg enDrvBTTraceMsg, tU8* pu8Buf, tU32 u32Len )
{
	#ifdef debug_raw_data_drv_bt_vTraceBuf
	int i,paramlen;
	paramlen = pu8Buf[9];
	paramlen << 8;
	paramlen += pu8Buf[10];
	paramlen += 10;
	if (u32Len <= 6)
		return;
	else{
		if (
				// (  pu8Buf[0] == 0xd0		//Message from UGZZC
				// || pu8Buf[0] == 0xd1		//Message from UGZZC (CFM)
				// || pu8Buf[0] == 0xa0		//Message from Host
				// || pu8Buf[0] == 0xa1		//Message from Host (CFM)
				// )
			// && (
					// pu8Buf[8] != 0x30
				// && pu8Buf[7] != 0x0a
				// )
			// && (
					// pu8Buf[8] != 0x03
				// && pu8Buf[7] != 0x36
				// )
			// && (
					(
						pu8Buf[8] == 0x62
					&& pu8Buf[7] == 0x05
					) // BT_APPL_SPP_SET_UUID
				|| (
						pu8Buf[8] == 0x63
					&& pu8Buf[7] == 0x00
					) // BT_APPL_SPP_DATA RECEIVE
				|| (
						pu8Buf[8] == 0x62
					&& pu8Buf[7] == 0x00
					)// BT_APPL_SPP_DATA SEND
				|| (
						pu8Buf[8] == 0x04
					&& pu8Buf[7] == 0x07
					)// BT_APPL_DEVICE_CONNECT
				|| (
						pu8Buf[8] == 0x04
					&& pu8Buf[7] == 0x09
					)// BT_APPL_DEVICE_DISCONNECT
				// )
		){
			printf("DrvBt: UGZZC RawData: ");
			for ( i = 0; i < u32Len ; i++ ){
				if ( i == 7 )
					printf("\033[32m%02x\033[0m",pu8Buf[i]);
				else if (i == 8)
					printf("\033[32m%02x \033[0m",pu8Buf[i]);
				else if ( 10 < i && i <= paramlen)
					printf("\033[33m%02x \033[0m",pu8Buf[i]);
				else
					printf("%02x ",pu8Buf[i]);
			}
			printf("\n");
		}
	}
	#endif
	return;
}

tVoid drv_bt_vPrintf(const char *pcFormat, ...)
{ 
	return;
}


tVoid drv_bt_vTraceBufNet ( TR_tenTraceLevel enTraceLevel, tenDrvBTTraceMsg enDrvBTTraceMsg, tU8 u8ModemMode, tU8* pu8Buf, tU32 u32Len )
{
	return;
}




