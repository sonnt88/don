/*****************************************************************************
* Copyright (C) Bosch Car Multimedia GmbH, 2010
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
*****************************************************************************/
/*!
*\file     drv_bt_asip_protocol.c
*\brief    This file contains the code for the DRV_BT module    
*
*\author:  Bosch Car Multimedia GmbH, CM-AI/PJ-V32, Bosse Arndt, 
*                                       external.bosse.arndt@de.bosch.com
*\par Copyright: 
*(c) 2010 Bosch Car Multimedia GmbH
*\par History:
* Created - 06/01/12 
**********************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

/* OSAL Interface */
#define OSAL_S_IMPORT_INTERFACE_GENERIC		   
#include "osal_if.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>

#include "drv_bt_lld_uart.h"

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
//#define TIOCMGET 0x5415
//#define TIOCMSET 0x5418
#define TIOCM_RTS 0x004

//#define drv_bt_lld_uart_port_set_rts_debug
#define drv_bt_lld_uart_flushBuffer_Ugzzc_debug
//#define drv_bt_lld_uart_init_debug
//#define drv_bt_lld_uart_write_debug
//#define drv_bt_lld_uart_read_debug

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
int pMyUart;	//fd for the opened ComPort for UART Communication with the UGGZC

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/****************************************************************************/
/*! 
*\fn      tVoid drv_bt_lld_uart_port_set_rts(int state)
*
*\brief   sets and releases the reset pin at the UGZZC
*
*\param   int state	: 0 releases the Reset
*							  1 sets the Reset
*         
*\return 
*
*\par History:  
*
****************************************************************************/
tVoid drv_bt_lld_uart_port_set_rts(int state){
   int currentState;
   ioctl(pMyUart, TIOCMGET, &currentState);
	switch (state){
		case 0:
			currentState &= ~TIOCM_RTS;
			#ifdef drv_bt_lld_uart_port_set_rts_debug
			printf("DrvBt: drv_bt_lld_uart_port_set_rts: Reset Ugzzc released\n");
			#endif
			break;
		case 1:
			currentState |= TIOCM_RTS;
			#ifdef drv_bt_lld_uart_port_set_rts_debug
			printf("DrvBt: drv_bt_lld_uart_port_set_rts: Reset Ugzzc set\n");
			#endif
			break;
	}
   ioctl(pMyUart, TIOCMSET, &currentState);
	return;
}

/****************************************************************************/
/*! 
*\fn      tVoid drv_bt_lld_uart_flushBuffer_Ugzzc(tVoid)
*
*\brief   resets the UGZZC and flushes the IObuffers from the UART
*
*\param 
*         
*\return 
*
*\par History:  
*
****************************************************************************/
tVoid drv_bt_lld_uart_flushBuffer_Ugzzc(tVoid){
   drv_bt_lld_uart_port_set_rts(1);
	OSAL_s32ThreadWait(1);				// hold reset for 1 ms
   tcflush(pMyUart, TCIOFLUSH);
	drv_bt_lld_uart_port_set_rts(0);	
	#ifdef drv_bt_lld_uart_flushBuffer_Ugzzc_debug
   printf("DrvBt: \033[31mdrv_bt_lld_uart_flushBuffer_Ugzzc: Reset Ugzzc and Buffers flushed\033[0m\n");
	#endif
	OSAL_s32ThreadWait(300);         // provide 300 ms for the UGZZC to recover from reset
	return;
}

/****************************************************************************/
/*! 
*\fn      tBool drv_bt_lld_uart_init(tChar* strPortNb, int baudrate)
*
*\brief   opens the ComPort with the UGZZC for UART communication
*
*\param 		tChar* strPortNb	: String for the DeviceName
*				int baudrate 		: Baudrate for the COnnection to the UGZZC
*         
*\return 	tBool:	TRUE successfully opened the Communication to UGZZC
*							FALSE failed to open the Communication to UGZZC
*
*\par History:  
*
****************************************************************************/
tBool drv_bt_lld_uart_init(tChar* strPortNb, int baudrate){
   // Struct for terminal IO options.
   struct termios options;
	
	pMyUart = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
	if (pMyUart < 0 )
   {
      /** Could not open the port. */
		#ifdef drv_bt_lld_uart_init_debug
      printf("DrvBt: drv_bt_lld_uart_init: Unable to open /dev/ttyUSB0\n");
		#endif
	  return FALSE;
   } else {
		#ifdef drv_bt_lld_uart_init_debug
		printf("DrvBt: drv_bt_lld_uart_init: '/dev/ttyUSB0' successfully opened\n");
		#endif
		if (fcntl(pMyUart, F_SETOWN, getpid()) > -1){
			#ifdef drv_bt_lld_uart_init_debug
			printf("DrvBt: drv_bt_lld_uart_init: '/dev/ttyUSB0' successfully owned by process\n");
			#endif
		} else {
			#ifdef drv_bt_lld_uart_init_debug
			printf("DrvBt: drv_bt_lld_uart_init: '/dev/ttyUSB0' owning by process failed\n");
			#endif
			return FALSE;
		}
		/*
		if (fcntl(pMyUart, F_SETFL, 0)> -1){ //there is everytime a discared message
		//if (fcntl(pMyUart, F_SETFL, O_SYNC)> -1){	//there is everytime a discared message
		//if (fcntl(pMyUart, F_SETFL, F_WRLCK )> -1){	//works but the upper works as well...
		//if (fcntl(pMyUart, F_SETFL, F_WRLCK | O_NDELAY)> -1){	//works but the upper works as well...
		//if (fcntl(pMyUart, F_SETFL, F_WRLCK ) > -1){	//works but the upper works as well...
		//if (fcntl(pMyUart, F_SETFL, O_NDELAY)> -1){	//works but there is a readthreat spamm in TTFis
			#ifdef drv_bt_lld_uart_init_debug
			printf("DrvBt: drv_bt_lld_uart_init: '/dev/ttyUSB0' successfully set fildes\n");
			#endif
		} else {
			#ifdef drv_bt_lld_uart_init_debug
			printf("DrvBt: drv_bt_lld_uart_init: '/dev/ttyUSB0' set fildes failed\n");
			#endif
			return FALSE;
		}*/
		/* get current port options */
		if (tcgetattr(pMyUart, &options) > -1){
			#ifdef drv_bt_lld_uart_init_debug
			printf("DrvBt: drv_bt_lld_uart_init: '/dev/ttyUSB0' successfully get options\n");
			#endif
		} else {
			#ifdef drv_bt_lld_uart_init_debug
			printf("DrvBt: drv_bt_lld_uart_init: '/dev/ttyUSB0' get options failed\n");
			#endif
			return FALSE;
		}
		/* set input and output baud rates */
		switch (baudrate){
			case 115200:
				/* B115200:	Baudrate of 115.200 kBaud*/
				cfsetispeed(&options, B115200);
				cfsetospeed(&options, B115200);
				#ifdef drv_bt_lld_uart_init_debug
				printf("DrvBt: drv_bt_lld_uart_init: Baudrate of 115.200 kBaud set!\n");
				#endif
				break;
			case 230400:
				/* B230400:	Baudrate of 230.400 kBaud*/
				cfsetispeed(&options, B230400);
				cfsetospeed(&options, B230400);
				#ifdef drv_bt_lld_uart_init_debug
				printf("DrvBt: drv_bt_lld_uart_init: Baudrate of 230.400 kBaud set!\n");
				#endif
				break;
			case 460800:
				/* B460800:	Baudrate of 460.800 kBaud*/
				cfsetispeed(&options, B460800);
				cfsetospeed(&options, B460800);
				#ifdef drv_bt_lld_uart_init_debug
				printf("DrvBt: drv_bt_lld_uart_init: Baudrate of 460.800 kBaud set!\n");
				#endif
				break;
			case 921600:
				/* B921600:	Baudrate of 921.600 kBaud*/
				cfsetispeed(&options, B921600);
				cfsetospeed(&options, B921600);
				#ifdef drv_bt_lld_uart_init_debug
				printf("DrvBt: drv_bt_lld_uart_init: Baudrate of 921.600 kBaud set!\n");
				#endif
				break;
			default:
				/* B115200:	Baudrate of 115.200 kBaud*/
				cfsetispeed(&options, B115200);
				cfsetospeed(&options, B115200);
				#ifdef drv_bt_lld_uart_init_debug
				printf("DrvBt: drv_bt_lld_uart_init: DEFAULT Baudrate of 115.200 kBaud set!\n");
				#endif
				break;
		}
		/* enable receiver and set local mode */
		/* CLOCAL:		If this bit is set, it indicates that the terminal is connected "locally" and that the modem status lines (such as carrier detect) should be ignored.
					On many systems if this bit is not set and you call open without the O_NONBLOCK flag set, open blocks until a modem connection is established.
					If this bit is not set and a modem disconnect is detected, a SIGHUP signal is sent to the controlling process group for the terminal (if it has one). Normally, this causes the process to exit; see 24. Signal Handling. Reading from the terminal after a disconnect causes an end-of-file condition, and writing causes an EIO error to be returned. The terminal device must be closed and reopened to clear the condition. 
			CSTOPB:		Set two stop bits, rather than one. 		
			CREAD:		If this bit is set, input can be read from the terminal. Otherwise, input is discarded when it arrives. 
			CS8:			This specifies eight bits per byte. 
			PARENB:		Enable parity generation on output and parity checking for input. 
			PARODD:		If set, then parity for input and output is odd; otherwise even parity is used. 
			HUPCL:		Lower modem control lines after last process closes the device (hang up). 
			CIGNORE:		If this bit is set, it says to ignore the control modes and line speed values entirely. This is only meaningful in a call to tcsetattr.
							The c_cflag member and the line speed values returned by cfgetispeed and cfgetospeed will be unaffected by the call. CIGNORE is useful 
							if you want to set all the software modes in the other members, but leave the hardware details in c_cflag unchanged. (This is how the TCSASOFT flag to tcsettattr works.)
			CRTSCTS:		(not in POSIX) Enable RTS/CTS (hardware) flow control. [requires _BSD_SOURCE or _SVID_SOURCE]
		*/
		//options.c_cflag &= ~(CSIZE | PARENB | CSTOPB );
		options.c_cflag |= ( CLOCAL | CREAD | CS8);
		/* configure for raw data transfer */
		/*
			ECHO:	Enable echo. 
			ECHOE:	Echo erase character as error-correcting backspace.
			ECHOK:	Echo KILL.
			ECHONL:	Echo NL.
			ICANON:	Canonical input (erase and kill processing). 
			IEXTEN:	Enable extended input character processing. 
			ISIG:	Enable signals.
			NOFLSH:	Disable flush after interrupt or quit. 
			TOSTOP:	Send SIGTTOU for background output. 
			XCASE:	Canonical upper/lower presentation (LEGACY).*/
		options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
		
		/*
			IGNBRK:	Ignore BREAK condition on input.
			BRKINT:	If IGNBRK is set, a BREAK is ignored. If it is not set but BRKINT is set, then a BREAK causes the input and output queues to be flushed, and if the terminal is the controlling terminal of a foreground process group, it will cause a SIGINT to be sent to this foreground process group. When neither IGNBRK nor BRKINT are set, a BREAK reads as a null byte ('\0'), except when PARMRK is set, in which case it reads as the sequence \377 \0 \0.
			IGNPAR:	Ignore framing errors and parity errors.
			PARMRK:	If IGNPAR is not set, prefix a character with a parity error or framing error with \377 \0. If neither IGNPAR nor PARMRK is set, read a character with a parity error or framing error as \0.
			INPCK:	Enable input parity checking.
			ISTRIP:	Strip off eighth bit.
			INLCR:	Translate NL to CR on input.
			IGNCR:	Ignore carriage return on input.
			ICRNL:	Translate carriage return to newline on input (unless IGNCR is set).
			IUCLC:	(not in POSIX) Map uppercase characters to lowercase on input.
			IXON:		Enable XON/XOFF flow control on output.
			IXANY:	(XSI) Typing any character will restart stopped output. (The default is to allow just the START character to restart output.)
			IXOFF:	Enable XON/XOFF flow control on input.
			IMAXBEL:	(not in POSIX) Ring bell when input queue is full. Linux does not implement this bit, and acts as if it is always set. 
			IUTF8:	(since Linux 2.6.4)(not in POSIX) Input is UTF8; this allows character-erase to be correctly performed in cooked mode. 
		*/
		options.c_iflag &= ~(IXON | IXOFF | IXANY | ICRNL | INLCR ); //disable flowcontrol

		/*
		OPOST		Enable implementation-defined output processing. The remaining c_oflag flag constants are defined in POSIX.1-2001, unless marked otherwise.
		OLCUC		(not in POSIX) Map lowercase characters to uppercase on output.
		ONLCR		(XSI) Map NL to CR-NL on output.
		OCRNL		Map CR to NL on output.
		ONOCR		Don't output CR at column 0.
		ONLRET	Don't output CR.
		OFILL		Send fill characters for a delay, rather than using a timed delay.
		OFDEL		(not in POSIX) Fill character is ASCII DEL (0177). If unset, fill character is ASCII NUL ('\0'). (Not implemented on Linux.)
		NLDLY		Newline delay mask. Values are NL0 and NL1. [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE]
		CRDLY		Carriage return delay mask. Values are CR0, CR1, CR2, or CR3. [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE]
		TABDLY	Horizontal tab delay mask. Values are TAB0, TAB1, TAB2, TAB3 (or XTABS). A value of TAB3, that is, XTABS, expands tabs to spaces (with tab stops every eight columns). [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE]
		BSDLY		Backspace delay mask. Values are BS0 or BS1. (Has never been implemented.) [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE]
		VTDLY		Vertical tab delay mask. Values are VT0 or VT1.
		FFDLY		Form feed delay mask. Values are FF0 or FF1. [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE] 
		*/
		options.c_oflag &= ~(OPOST | ONOCR | ONLRET); //disable any output data change
		
		/* configure timeout behavior: minimum number of characters = 0; timeout = 100 ms */
		// only works if *not* using O_NDELAY when opening the port
		options.c_cc[VMIN] = 0;
		options.c_cc[VTIME] = 1;

		/* set new options */
		if (tcsetattr(pMyUart, TCSANOW, &options) > -1){
			#ifdef drv_bt_lld_uart_init_debug
			printf("DrvBt: drv_bt_lld_uart_init: '/dev/ttyUSB0' successfully set options\n");
			#endif
		} else {
			#ifdef drv_bt_lld_uart_init_debug
			printf("DrvBt: drv_bt_lld_uart_init: '/dev/ttyUSB0' set options failed\n");
			#endif
			return FALSE;
		}
		//RESET
		#ifdef drv_bt_lld_uart_init_debug
		printf("DrvBt: drv_bt_lld_uart_init: ");
		#endif
		drv_bt_lld_uart_flushBuffer_Ugzzc();
	  return TRUE;
   }
}

/****************************************************************************/
/*! 
*\fn      tS32 drv_bt_lld_uart_write(int comport, tU8* buf, tU16 len, tU32 timeout)
*
*\brief   writes the param buf with the lenght param len to the opened the UART
*
*\param 		int comport		: String for the DeviceName
*				tU8* buf			: Buffer which will be written to the UART
*				tU16 len			: Bufferlengh which will be written to the UART
*				tU32 timeout	: Timeout for the writingprocess
*         
*\return 	tS32	:	Bytes written to the UART
*
*\par History:  
*
****************************************************************************/
tS32 drv_bt_lld_uart_write(int comport, tU8* buf, tU16 len, tU32 timeout){
	tS32 s32RetVal = 0;
	tS32 s32SendData;
	tU16 i = 0;
	tU32 u32ElapsedTime = 0;
	
	#ifdef drv_bt_lld_uart_write_debug
	printf("DrvBt: drv_bt_lld_uart_write: Send data via LLD: ");
	#endif
	while (i < len && (u32ElapsedTime < timeout)){
		s32SendData = write(pMyUart, &buf[i], (tU8) 1);
		if (s32SendData <=0){
			s32RetVal = 0;
         u32ElapsedTime += 20;
         OSAL_s32ThreadWait(20);
		} else {
			#ifdef drv_bt_lld_uart_write_debug
			printf("%02x ",buf[i]);
			#endif
			i++;
			s32RetVal += s32SendData;
		}
	}
	//tcdrain(pMyUart); //waiting to push out all
	#ifdef drv_bt_lld_uart_write_debug
	printf(".. %li byte(s) send \n",s32RetVal);
	#endif
	return s32RetVal;
}

/****************************************************************************/
/*! 
*\fn      tS32 drv_bt_lld_uart_read(int comport, tU8* buf, tU16 len, tU32 timeout)
*
*\brief   reads the count param len to the param buf from the opened the UART
*
*\param 		int comport		: String for the DeviceName
*				tU8* buf			: Buffer which will be read from the UART
*				tU16 len			: Bufferlengh which will be read from the UART
*				tU32 timeout	: Timeout for the readingprocess
*         
*\return 	tS32	:	Bytes read from the UART
*
*\par History:  
*
****************************************************************************/
tS32 drv_bt_lld_uart_read(int comport, tU8* buf, tU16 len, tU32 timeout){
   tS32 s32RetVal;
	tS32 s32ReceivedData = 0;
   tU32 u32ElapsedTime = 0;
	
	#ifdef drv_bt_lld_uart_read_debug
	if ( len != 1)
		printf("DrvBt: drv_bt_lld_uart_read: Read data via LLD: ");
	#endif
	while ((u32ElapsedTime < timeout) && (s32ReceivedData < len)) {
		s32RetVal = read(pMyUart,(char*)&buf[s32ReceivedData],1);
      if (s32RetVal <= 0) {
			s32RetVal = 0;
         u32ElapsedTime += 20;
         OSAL_s32ThreadWait(20);
      } else {
			s32ReceivedData += s32RetVal;
			#ifdef drv_bt_lld_uart_read_debug
			if ( len != 1 && s32ReceivedData !=0)
				printf("%02x ",buf[s32ReceivedData]);
			#endif
      }
   }
	#ifdef drv_bt_lld_uart_read_debug
	if ( len != 1 && s32ReceivedData !=0)
		printf(".. %lu byte(s) read\n",s32ReceivedData);
	#endif
   return s32ReceivedData;
}

/****************************************************************************/
/*! 
*\fn      tBool drv_bt_lld_uart_close(tVoid)
*
*\brief   closes the ComPort with the UGZZC for UART communication
*
*\param 		
*         
*\return 	tBool	:	always TRUE
*
*\par History:  
*
****************************************************************************/
tBool drv_bt_lld_uart_close(tVoid){
	close(pMyUart);
	return true;
}
