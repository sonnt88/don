/* *********************************************************************** */
/*  includes                                                               */
/* *********************************************************************** */
#include "OsalConf.h"

#include <sys/socket.h>
#include <netinet/in.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

//#define SYSTEM_S_IMPORT_INTERFACE_DEV_TOUCHSCREEN_DEF
//#include "system_pif.h"

#include "dev_touch_screen.h"

/* ************************************************************************/
/*  defines                                                               */
/* ************************************************************************/
//#define DEBUG_PRINT(...)    printf(__VA_ARGS__)
#define DEBUG_PRINT(...)

#define DEV_TOUCHSCREEN_RECV_THREAD_NAME    "TCH_SCR_IO_Thread"
#define DEV_TOUCHSCREEN_SEM_NAME            "TCH_SCR_SEM"

/* *********************************************************************** */
/*  typedefs enum                                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct                                                        */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static variables                                                       */
/* *********************************************************************** */  
static OSAL_tSemHandle          DevTouchScreenHandleSemaphore = OSAL_C_INVALID_HANDLE;
static int                      DevTouchScreenSocketFD = -1;
static int                      DevTouchScreenConnectionFD = -1;
static tS32                     hTouchShMem;

/* *********************************************************************** */
/*  global variables                                                       */
/* *********************************************************************** */
trTouchData*  pTouchData = 0;

/* *********************************************************************** */
/*  function prototypes                                                    */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static functions                                                       */
/* *********************************************************************** */

/* *********************************************************************** */
/*  global functions                                                       */
/* *********************************************************************** */

/******************************************************************************
 *FUNCTION      :DEV_TOUCHSCREEN_ReceiveThread
 *
 *DESCRIPTION   :Thread to read from the socket
 *
 *PARAMETER     :Arg    unused
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
void DEV_TOUCHSCREEN_ReceiveThread(void *Arg)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;

    (void)Arg;

    DEBUG_PRINT("%s started\n", __func__);
    if(OSAL_s32SemaphoreOpen(DEV_TOUCHSCREEN_SEM_NAME, &semhandle) == OSAL_OK)
    {
        while(DevTouchScreenSocketFD != -1)
        {
            if(DevTouchScreenConnectionFD == -1)
            {
                DevTouchScreenConnectionFD = accept(DevTouchScreenSocketFD, NULL, NULL);
                if(DevTouchScreenConnectionFD == -1)
                {
                    OSAL_s32ThreadWait(10);
                }
                else
                {
                    DEBUG_PRINT("Connected!\n");
                }
            }
            else
            {
                static const int    buffsize = 64;
                unsigned char       buff[buffsize];
                tS32                buffbytes;
                tS32                inmsg = FALSE;

                // | 0+1    | 2   | 3  | 4     | 5+6  | 7+8  |
                // | 0xF00D | Len | ID | Event | PosX | PosY |

                buffbytes = 0;
                for(; ; )
                {
                    tS32    i, numbytes;
                    tS32    msglen;
                    tS32    checkmsg;

                    numbytes = recv(DevTouchScreenConnectionFD, buff + buffbytes, buffsize - buffbytes, 0);

                    if(numbytes <= 0)
                    {
                        // recv failed for some reason - probably the connection was closed
                        DEBUG_PRINT("%s Receive failed with %i\n", __func__, numbytes);
                        if(DevTouchScreenConnectionFD != -1)
                        {
                            close(DevTouchScreenConnectionFD);
                            DevTouchScreenConnectionFD = -1;
                        }
                        break;
                    }
                    DEBUG_PRINT("%s Received: %i Bytes\n", __func__, numbytes);

                    buffbytes += numbytes;
                    for(checkmsg = TRUE; checkmsg == TRUE; )
                    {
                        checkmsg = FALSE;

                        // If there was no message start tag, check if there is one now
                        for(i = 0; inmsg == FALSE && i < buffbytes - 1; i++)
                        {
                            if(*((tU16 *)(&buff[i])) == DEV_TOUCHSCREEN_TCP_MSG_START)
                            {
                                // Shift data in buffer - discard any garbage and drop message start tag
                                memmove(buff, buff + i + sizeof(tU16), buffsize - i - sizeof(tU16));
                                buffbytes -= i + sizeof(tU16);
                                inmsg = TRUE;
                                msglen = -1;
                                DEBUG_PRINT("%s Found tag\n", __func__);
                            }
                        }

                        if(inmsg == FALSE)
                        {
                            if(buffbytes > 1)
                            {
                                // There is some garbage in the buffer
                                // Discard garbage but make sure to keep the last byte as it could be the first byte of a message start tag
                                memmove(buff, buff + buffbytes - 1, buffbytes - 1);
                                DEBUG_PRINT("%s Discarded %i bytes of garbage\n", __func__, buffbytes);
                                buffbytes = 1;
                            }
                        }
                        else
                        {
                            // There was a message start tag
                            // Now check if there is enough data available to determine the message's length
                            if(msglen < 0 && buffbytes >= 1)
                            {
                                msglen = buff[0];
                                //if(msglen < 1 || msglen > buffsize)
                                if(msglen != 7)     // For now, there is exactly one kind of message...
                                {
                                    // The length of this message is not reasonable, discard it
                                    inmsg = FALSE;
                                    msglen = -1;
                                    DEBUG_PRINT("%s Found invalid size\n", __func__);
                                }
                            }

                            // Check if there is a complete message available
                            if(inmsg == TRUE && msglen > 0 && buffbytes >= msglen)
                            {
                                // Received at least one complete message
                                OSAL_trArgTouchPoint    arg;
                                tS32                    argvalid = TRUE;

                                if(buff[1] == DEV_TOUCHSCREEN_TCP_MSG_PROPS)
                                {
                                    pTouchData->DevTouchScreenDispProp.s16x_size = *((tU16 *)&buff[2]);
                                    pTouchData->DevTouchScreenDispProp.s16y_size = *((tU16 *)&buff[4]);
                                    pTouchData->DevTouchScreenDispProp.bDualview = (buff[6] != 0) ? TRUE : FALSE;
                                    argvalid = FALSE; // Internal data - don't call callback function
                                    DEBUG_PRINT("%s changed props to <%i, %i>, DualView %s\n", __func__, (int)pTouchData->DevTouchScreenDispProp.s16x_size, (int)pTouchData->DevTouchScreenDispProp.s16y_size, pTouchData->DevTouchScreenDispProp.bDualview ? "true" : "false");
                                }
                                else if(buff[1] == DEV_TOUCHSCREEN_TCP_MSG_RESIZE)
                                {
                                    arg.rTouchPoint.u16x_co = *((tU16 *)&buff[2]);
                                    arg.rTouchPoint.u16y_co = *((tU16 *)&buff[4]);
                                    arg.enTouchState = 0xfe;
                                    memcpy(&pTouchData->DevTouchScreenState, &arg, sizeof(OSAL_trArgTouchPoint));
                                    DEBUG_PRINT("%s resized to <%i, %i>\n", __func__, (int)pTouchData->DevTouchScreenDispProp.s16x_size, (int)pTouchData->DevTouchScreenDispProp.s16y_size);
                                }
                                else if(buff[1] == 0) // Multitouch is not supported for now so ignore all but the touch with ID 0
                                {
                                    switch(buff[2])
                                    {
                                    case DEV_TOUCHSCREEN_TCP_MSG_DOWN:
                                        arg.rTouchPoint.u16x_co = *((tU16 *)&buff[3]);
                                        arg.rTouchPoint.u16y_co = *((tU16 *)&buff[5]);
                                        arg.enTouchState = PEN_DOWN;
                                        memcpy(&pTouchData->DevTouchScreenState, &arg, sizeof(OSAL_trArgTouchPoint));
                                        break;
                                    case DEV_TOUCHSCREEN_TCP_MSG_MOVE:
                                        arg.rTouchPoint.u16x_co = *((tU16 *)&buff[3]);
                                        arg.rTouchPoint.u16y_co = *((tU16 *)&buff[5]);
                                        arg.enTouchState = PEN_MOVE;
                                        if(pTouchData->DevTouchScreenState.rTouchPoint.u16x_co == arg.rTouchPoint.u16x_co &&
                                           pTouchData->DevTouchScreenState.rTouchPoint.u16y_co == arg.rTouchPoint.u16y_co)
                                            // No change -> no need to update anything
                                            argvalid = FALSE;
                                        else
                                            memcpy(&pTouchData->DevTouchScreenState, &arg, sizeof(OSAL_trArgTouchPoint));
                                        break;
                                    case DEV_TOUCHSCREEN_TCP_MSG_UP:
                                        arg.rTouchPoint.u16x_co = *((tU16 *)&buff[3]);
                                        arg.rTouchPoint.u16y_co = *((tU16 *)&buff[5]);
                                        arg.enTouchState = PEN_UP;
                                        memcpy(&pTouchData->DevTouchScreenState, &arg, sizeof(OSAL_trArgTouchPoint));
                                        break;
                                    default:
                                        argvalid = FALSE;
                                        DEBUG_PRINT("%s Unknown command %02X\n", __func__, buff[1]);
                                        break;
                                    }
                                }
                                else
                                    argvalid = FALSE;

                                // FIXME: Sanity check for values

                                if(argvalid == TRUE)
                                {
                                    // FIXME: Doesn't work! Might overwrite pTouchData->DevTouchScreenCBArgs with new data before callback is executed!
                                    if(OSAL_s32SemaphoreWait(semhandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                                    {
                                        if(pTouchData->DevTouchScreenCallback != NULL)
                                        {
                                            // Send it out, if someone is interested
                                            OSAL_trArgTouchPoint    *osal_arg = &pTouchData->DevTouchScreenCBArgs[pTouchData->DevTouchScreenCBNextArg];

                                            memcpy(osal_arg, &arg, sizeof(arg));
                                            u32ExecuteCallback(OSAL_ProcessWhoAmI(), pTouchData->DevTouchScreenCallbackProcID, pTouchData->DevTouchScreenCallback, osal_arg, sizeof(arg));

                                            if(++pTouchData->DevTouchScreenCBNextArg >= OSAL_C_DEV_TOUCHSCREEN_ARGS_MAX)
                                                pTouchData->DevTouchScreenCBNextArg = 0;
                                            DEBUG_PRINT("%s Message delivered to callback\n", __func__);
                                        }
                                        OSAL_s32SemaphorePost(semhandle);
                                    }
                                }

                                memmove(buff, buff + msglen, buffsize - msglen);
                                inmsg = FALSE;
                                buffbytes -= msglen;
                                msglen = -1;

                                checkmsg = TRUE;
                                DEBUG_PRINT("%s Message processed\n", __func__);
                            }
                        }
                    }
                }
            }
        }
        OSAL_s32SemaphoreClose(semhandle);
        OSAL_s32SemaphoreDelete(DEV_TOUCHSCREEN_SEM_NAME);
    }
    DEBUG_PRINT("%s ended\n", __func__);
}



/******************************************************************************
 *FUNCTION      :DEV_TOUCHSCREEN_s32IODeviceInit
 *
 *DESCRIPTION   :Initializes the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_TOUCHSCREEN_s32IODeviceInit(void)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;
    int             port = 3458;
    tS32            retval = OSAL_E_NOERROR;

    if(OSAL_s32SemaphoreCreate(DEV_TOUCHSCREEN_SEM_NAME, &semhandle, 1) != OSAL_OK)
    {
        retval = (tS32)OSAL_u32ErrorCode();
    }
    else
    {
        OSAL_s32SemaphoreClose(semhandle);
    }

    if(retval == OSAL_E_NOERROR)
    {
        OSAL_tIODescriptor  fd = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/LSIM/TOUCH_SCREEN", OSAL_EN_READONLY);

        // It is not a problem and not an error if access to the registry fails. Defaults will be used.
        if(fd != OSAL_ERROR)
        {
            OSAL_trIOCtrlRegistry   entry;
            tS32                    val;

            entry.pcos8Name = "PORT";
            entry.u32Size   = sizeof(val);
            entry.s32Type   = OSAL_C_S32_VALUE_S32;
            entry.ps8Value  = (tU8 *)&val;
            if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
                port = (int)(*(tS32 *)entry.ps8Value);

            entry.pcos8Name = "SIZE_X";
            entry.u32Size   = sizeof(val);
            entry.s32Type   = OSAL_C_S32_VALUE_S32;
            entry.ps8Value  = (tU8 *)&val;
            if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
                pTouchData->DevTouchScreenDispProp.s16x_size = (tS16)(*(tS32 *)entry.ps8Value);

            entry.pcos8Name = "SIZE_Y";
            entry.u32Size   = sizeof(val);
            entry.s32Type   = OSAL_C_S32_VALUE_S32;
            entry.ps8Value  = (tU8 *)&val;
            if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
                pTouchData->DevTouchScreenDispProp.s16y_size = (tS16)(*(tS32 *)entry.ps8Value);

            entry.pcos8Name = "DUAL_VIEW";
            entry.u32Size   = sizeof(val);
            entry.s32Type   = OSAL_C_S32_VALUE_S32;
            entry.ps8Value  = (tU8 *)&val;
            if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
                pTouchData->DevTouchScreenDispProp.bDualview = (*(tS32 *)entry.ps8Value) != 0;

            OSAL_s32IOClose (fd);
        }

        DevTouchScreenSocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
        if(DevTouchScreenSocketFD == -1)
        {
            DEBUG_PRINT("socket failed\n");
            retval = OSAL_ERROR;
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        int     flags = fcntl(DevTouchScreenSocketFD, F_GETFL, 0);
        fcntl(DevTouchScreenSocketFD, F_SETFL, flags | O_NONBLOCK);
    }

    if(retval == OSAL_E_NOERROR)
    {
        struct sockaddr_in      sockaddr;

        memset(&sockaddr, 0, sizeof(sockaddr));

        sockaddr.sin_family = AF_INET;
        sockaddr.sin_port = htons(port);
        sockaddr.sin_addr.s_addr = INADDR_ANY;

        if(bind(DevTouchScreenSocketFD, (struct sockaddr *)&sockaddr, sizeof(sockaddr)) == -1)
        {
            DEBUG_PRINT("bind failed\n");
            retval = OSAL_ERROR;
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        if(listen(DevTouchScreenSocketFD, 0) == -1)
        {
            DEBUG_PRINT("listen failed\n");
            retval = OSAL_ERROR;
        }
        else
        {
            DEBUG_PRINT("Listener opened for port %i\n", port);
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        OSAL_trThreadAttribute  thrdattr;

        thrdattr.szName         = DEV_TOUCHSCREEN_RECV_THREAD_NAME;
        thrdattr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        thrdattr.s32StackSize   = 4096;
        thrdattr.pfEntry        = DEV_TOUCHSCREEN_ReceiveThread;
        thrdattr.pvArg          = NULL;

        if(OSAL_ThreadSpawn((OSAL_trThreadAttribute* )&thrdattr) == OSAL_ERROR)
        {
            retval = OSAL_ERROR;
        }
    }

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_TOUCHSCREEN_s32IODeviceRemove
 *
 *DESCRIPTION   :Removes the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
void DEV_TOUCHSCREEN_s32IODeviceRemove(void)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;

    if(OSAL_s32SemaphoreOpen(DEV_TOUCHSCREEN_SEM_NAME, &semhandle) == OSAL_OK)
    {
        if(OSAL_s32SemaphoreWait(semhandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
        {
            pTouchData->DevTouchScreenCallbackProcID = 0;
            pTouchData->DevTouchScreenCallback = NULL;

            if(DevTouchScreenConnectionFD != -1)
            {
                shutdown(DevTouchScreenConnectionFD, SHUT_RDWR);
                DevTouchScreenConnectionFD = -1;
            }

            if(DevTouchScreenSocketFD != -1)
            {
                close(DevTouchScreenSocketFD);
                DevTouchScreenSocketFD = -1;
            }
            OSAL_s32SemaphorePost(semhandle);
            OSAL_s32SemaphoreClose(semhandle);
        }
    }
}



/******************************************************************************
 *FUNCTION      :DEV_TOUCHSCREEN_s32IOOpen
 *
 *DESCRIPTION   :Open the device
 *
 *PARAMETER     :enAccess   Desired access, one of
 *                              OSAL_EN_WRITEONLY
 *                              OSAL_EN_READWRITE
 *                              OSAL_EN_READONLY
 *
 *RETURNVALUE   :tS32   device handle on success or
 *                      OSAL_ERROR/OSAL_E_ALREADYOPENED in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_TOUCHSCREEN_s32IOOpen(OSAL_tenAccess enAccess)
{
    tS32    retval = OSAL_E_NOERROR;

    if((enAccess != OSAL_EN_READONLY) && (enAccess != OSAL_EN_WRITEONLY) && (enAccess != OSAL_EN_READWRITE))
    {
        retval = OSAL_ERROR;
    }
    else if(pTouchData->DevTouchScreenOpenCount > 0)
    {
        retval = OSAL_E_ALREADYOPENED;
    }
    else
    {
        if(OSAL_s32SemaphoreOpen(DEV_TOUCHSCREEN_SEM_NAME, &DevTouchScreenHandleSemaphore) != OSAL_OK)
        {
            retval = OSAL_ERROR;
        }
        else
        {
            pTouchData->DevTouchScreenOpenCount++;
        }
    }

    // FIXME: Return a handle
    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_TOUCHSCREEN_s32IOClose
 *
 *DESCRIPTION   :Close the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_TOUCHSCREEN_s32IOClose(void)
{
    tS32    retval = OSAL_E_NOERROR;

    if(pTouchData->DevTouchScreenOpenCount <= 0)
    {
        retval = OSAL_ERROR;
    }
    else
    {
        if(DevTouchScreenHandleSemaphore != OSAL_C_INVALID_HANDLE)
        {
            pTouchData->DevTouchScreenOpenCount--;
            OSAL_s32SemaphoreClose(DevTouchScreenHandleSemaphore);
            DevTouchScreenHandleSemaphore = OSAL_C_INVALID_HANDLE;
        }
    }

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_TOUCHSCREEN_s32IORead
 *
 *DESCRIPTION   :Read data from the device
 *
 *PARAMETER     :pBuffer        Buffer to receive the data
 *               u32Size        Size of buffer
 *               pu32RetSize    Pointer to variable to store the number of bytes read.
 *                              Can be NULL.
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_TOUCHSCREEN_s32IORead(tPS8 pBuffer, tU32 u32Size, tU32 *pu32RetSize)
{
    tS32    retval = OSAL_E_NOERROR;

    if(pu32RetSize != NULL) // CHECKME: Correct this way or return error?
        *pu32RetSize = 0;

    if(pBuffer == NULL || u32Size < sizeof(OSAL_trTouchPointCo))
        retval = OSAL_E_INVALIDVALUE;

    if(retval == OSAL_E_NOERROR)
    {
        if(pTouchData->DevTouchScreenState.enTouchState != PEN_UP)
        {
            memcpy(pBuffer, &pTouchData->DevTouchScreenState.rTouchPoint, sizeof(OSAL_trTouchPointCo));

            if(pu32RetSize != NULL)
                *pu32RetSize = sizeof(OSAL_trTouchPointCo);
        }
    }

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_TOUCHSCREEN_s32IOWrite
 *
 *DESCRIPTION   :Write data to the device
 *
 *PARAMETER     :pBuffer        Buffer of data to write
 *               u32Size        Size of buffer
 *               pu32RetSize    Pointer to variable to store the number of bytes written.
 *                              Can be NULL.
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_TOUCHSCREEN_s32IOWrite(tPS8 pBuffer, tU32 u32Size, tU32 *pu32RetSize)
{
    tS32    retval = OSAL_E_NOTSUPPORTED;

    if(pu32RetSize != NULL)
        *pu32RetSize = 0;

    // FIXME: Remove stub or implement, if still required

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_TOUCHSCREEN_s32IOControl
 *
 *DESCRIPTION   :Control the device
 *
 *PARAMETER     :s32Fun     Function to execute
 *               s32Arg     Argument to function, meaning depends on s32Fun
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_TOUCHSCREEN_s32IOControl(tS32 s32Fun, tS32 s32Arg)
{
    tS32    retval = OSAL_E_NOERROR;

    if(DevTouchScreenHandleSemaphore == OSAL_C_INVALID_HANDLE)
    {
        retval = OSAL_E_UNKNOWN;
    }

    if(retval == OSAL_E_NOERROR)
    {
        switch(s32Fun)
        {
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_CLEARFIFO:
            // TODO: Implement
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_GETCALINFO:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                memcpy((void *)s32Arg, &pTouchData->DevTouchScreenCalInfo, sizeof(OSAL_trTouchscreenCalInfo));
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_SETCALINFO:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                memcpy(&pTouchData->DevTouchScreenCalInfo, (void *)s32Arg, sizeof(OSAL_trTouchscreenCalInfo));
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_PUTCALPOINT:
            // TODO: Implement
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_SETCALIBRATION:
            // TODO: Implement
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_GETCALVALUES:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                memcpy((void *)s32Arg, &pTouchData->DevTouchScreenCalMatrix, sizeof(OSAL_trCalibrationMatrix));
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_PUTCALVALUES:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                memcpy(&pTouchData->DevTouchScreenCalMatrix, (void *)s32Arg, sizeof(OSAL_trCalibrationMatrix));
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_GETDISPPPROP:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                memcpy((void *)s32Arg, &pTouchData->DevTouchScreenDispProp, sizeof(OSAL_trTouchscreenDispProp));
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_SETREADTIMEOUT_MS:
            pTouchData->DevTouchScreenReadTimeout = s32Arg;
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_GETREADTIMEOUT_MS:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                s32Arg = pTouchData->DevTouchScreenReadTimeout;
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_READ_W_TMO:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                memcpy((void *)s32Arg, &pTouchData->DevTouchScreenState.rTouchPoint, sizeof(OSAL_trTouchPointCo));
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_REG_CALLBACK:
            if(OSAL_s32SemaphoreWait(DevTouchScreenHandleSemaphore, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
            {
                pTouchData->DevTouchScreenCallbackProcID = OSAL_ProcessWhoAmI();
                pTouchData->DevTouchScreenCallback = (OSAL_tpfTouchCallbackFn)s32Arg;
                OSAL_s32SemaphorePost(DevTouchScreenHandleSemaphore);
            }
            break;
        case OSAL_C_S32_IOCTL_TOUCHSCREEN_DEVICE_VERSION:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                *((tU32 *)s32Arg) = OSAL_TOUCHSCREEN_IO_CTRL_VERSION;
            break;
        default:
            retval = OSAL_E_NOTSUPPORTED;
            break;
        }
    }

    return retval;
}


#ifdef VARIANT_P_SHARED_TOUCH
tS32 touch_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
  return (tU32)(DEV_TOUCHSCREEN_s32IOOpen(enAccess));
}

tS32 touch_drv_io_close(tS32 s32ID, tU32 u32FD)
{
  return DEV_TOUCHSCREEN_s32IOClose();
}

tS32 touch_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{
  return((tS32)DEV_TOUCHSCREEN_s32IOControl(s32fun, s32arg));
}

tS32 touch_drv_io_write(tS32 s32ID, tU32 u32FD, tPCS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
  return((tS32)DEV_TOUCHSCREEN_s32IOWrite(pBuffer, u32Size, ret_size));
}

tS32 touch_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
  return((tS32)DEV_TOUCHSCREEN_s32IORead(pBuffer, u32Size, ret_size));
}

static sem_t       *inittouch_lock;
void __attribute__ ((constructor)) touch_process_attach(void)
{
   tBool bFirstAttach = FALSE;
   char buffer[100];
  
   inittouch_lock = sem_open("TOU_INIT",O_EXCL | O_CREAT, 0660, 0);
   if (inittouch_lock != SEM_FAILED)
   {
      bFirstAttach = TRUE;
       /* create shared memory at first process attach */
      hTouchShMem = shm_open("TOUCH_SHM", O_EXCL|O_RDWR|O_CREAT|O_TRUNC, OSAL_ACCESS_RIGTHS);
      if(hTouchShMem  == -1)
      {
          FATAL_M_ASSERT_ALWAYS();
      }
      if (ftruncate(hTouchShMem, sizeof(trTouchData)) == -1)
      {
         FATAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {
       inittouch_lock = sem_open("TOU_INIT", 0);
       bFirstAttach = FALSE;
       sem_wait(inittouch_lock);

       hTouchShMem = shm_open("TOUCH_SHM", O_RDWR ,0);
       if(hTouchShMem  == -1)
       {
          FATAL_M_ASSERT_ALWAYS();
       }
   }

    /* map shared memory into address space */
    pTouchData = (trTouchData*)mmap( NULL,
                      sizeof(trTouchData),
                      PROT_READ | PROT_WRITE,
                      GLOBAL_DATA_OPTION,
                      hTouchShMem,
                      0);
    if (pTouchData == MAP_FAILED)
    {
        FATAL_M_ASSERT_ALWAYS();
    }


   if(bFirstAttach)
   {
      pTouchData->DevTouchScreenOpenCount = 0;
      pTouchData->DevTouchScreenCallbackProcID = 0;
      pTouchData->DevTouchScreenCallback = NULL;
      pTouchData->DevTouchScreenState.enTouchState = PEN_UP;
      pTouchData->DevTouchScreenDispProp.s16x_size = 320;
      pTouchData->DevTouchScreenDispProp.s16y_size = 240;
      pTouchData->DevTouchScreenDispProp.bDualview = FALSE;
      DEV_TOUCHSCREEN_s32IODeviceInit();
   }
   else
   {
       NORMAL_A_ASSERT_ALWAYS();
   }
   sem_post(inittouch_lock);
}


void vOnTouchProcessDetach( void )
{
    DEV_TOUCHSCREEN_s32IODeviceRemove();
}
#endif