#ifndef DEV_TOUCHSCREEN_H
#define DEV_TOUCHSCREEN_H

/* ************************************************************************/
/*  defines (scope: global)                                               */
/* ************************************************************************/
#define DEV_TOUCHSCREEN_TCP_MSG_START   0xf00d
#define DEV_TOUCHSCREEN_TCP_MSG_DOWN    0x00
#define DEV_TOUCHSCREEN_TCP_MSG_MOVE    0x01
#define DEV_TOUCHSCREEN_TCP_MSG_UP      0x02

#define DEV_TOUCHSCREEN_TCP_MSG_RESIZE  0xfe
#define DEV_TOUCHSCREEN_TCP_MSG_PROPS   0xff

/* *********************************************************************** */
/*  typedefs enum (scope: global)                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct (scope: global)                                        */
/* *********************************************************************** */
// DEV_TOUCH_SCREEN
#define OSAL_C_DEV_TOUCHSCREEN_ARGS_MAX     4

typedef struct{
 tU8                        DevTouchScreenOpenCount;
 OSAL_tProcessID            DevTouchScreenCallbackProcID;
 OSAL_tpfTouchCallbackFn    DevTouchScreenCallback;
 tU8                        DevTouchScreenCBNextArg;
 OSAL_trArgTouchPoint       DevTouchScreenCBArgs[OSAL_C_DEV_TOUCHSCREEN_ARGS_MAX];

 OSAL_trTouchscreenDispProp DevTouchScreenDispProp;
 OSAL_trCalibrationMatrix   DevTouchScreenCalMatrix;
 OSAL_trTouchscreenCalInfo  DevTouchScreenCalInfo;
 OSAL_trArgTouchPoint       DevTouchScreenState;
 tU32                       DevTouchScreenReadTimeout;
}trTouchData;

/* *********************************************************************** */
/*  typedefs function (scope: global)                                      */
/* *********************************************************************** */

/* *********************************************************************** */
/*  function prototypes (scope: global)                                    */
/* *********************************************************************** */
tS32    DEV_TOUCHSCREEN_s32IODeviceInit(void);
void    DEV_TOUCHSCREEN_s32IODeviceRemove(void);
tS32    DEV_TOUCHSCREEN_s32IOOpen(OSAL_tenAccess enAccess);
tS32    DEV_TOUCHSCREEN_s32IOClose(void);
tS32    DEV_TOUCHSCREEN_s32IORead(tPS8 pBuffer, tU32 u32Size, tU32 *pu32RetSize);
tS32    DEV_TOUCHSCREEN_s32IOWrite(tPS8 pBuffer, tU32 u32Size, tU32 *pu32RetSize);
tS32    DEV_TOUCHSCREEN_s32IOControl(tS32 s32Fun, tS32 s32Arg);

#else //DEV_TOUCHSCREEN_H
#error dev_touchscreen.h included several times
#endif //DEV_TOUCHSCREEN_H
