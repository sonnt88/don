/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        pdd_access_nor_user.h
 *
 *  header for the PDD
 *
 * @date        2014-12-01
 *
 * @note
 *
 *  &copy; Copyright BoschSoftec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef PDD_ACCESS_NOR_USER_H
#define PDD_ACCESS_NOR_USER_H

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
/***            data stream configuration              ******************
|-----------------------------------------------------------------------*/
//location
typedef enum 
{
  PDD_LOCATION_NOR_USER, 
  PDD_LOCATION_LAST,
}tePddLocation;

/*length of the datastream name */
#define PDD_NOR_MAX_SIZE_NAME                          32
/*structure configuration*/
typedef struct
{
  tU8               u8DataStreamName[PDD_NOR_MAX_SIZE_NAME];    
  tU32              u32Version;
  tePddLocation     eLocation;
}tsPddNorUserConfig;

//datastream name 
#define PDD_NOR_USER_DATASTREAM_NAME_KDS               "kds_data" 
#define PDD_NOR_USER_DATASTREAM_NAME_CONFIG            "config_data" 
#define PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM          2 

/*configuration datstream*/
static  const tsPddNorUserConfig    vrPddNorUserConfig[PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM] =
{   /* name              */
  {PDD_NOR_USER_DATASTREAM_NAME_KDS,0x0001,PDD_LOCATION_NOR_USER},
  {PDD_NOR_USER_DATASTREAM_NAME_CONFIG,0x0001,PDD_LOCATION_NOR_USER}
};

/************************************************************************
|defines (scope: global)
|-----------------------------------------------------------------------*/
/******** configuration for nor user access *********/ 
#define PDD_NOR_USER_MIN_NUMBER_OF_SECTOR                     2
#define PDD_NOR_USER_MIN_SECTOR_SIZE                          0x20000 // PDD_NOR_USER_CLUSTER_SIZE //32k (GM: 0x20000 128k)  
#define PDD_NOR_USER_MAX_WRITE_POSITION                       (0x08)
#define PDD_NOR_USER_HEADER_SIZE_DATA_STREAM (PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM*sizeof(tsPddNorUserHeaderDataStream))

/*32k cluster (if change the size, older persistent data not valid=> erase flash)*/
#define PDD_NOR_USER_CLUSTER_SIZE                             0x8000  

//trace define
#define PDD_TRACE(u8Kind,pData,u32Len)     	
#define PDD_FATAL_M_ASSERT_ALWAYS()         

/* return value */
#define PDD_OK                                             0
#define PDD_ERROR_WRONG_PARAMETER                         -1
#define PDD_ERROR_ADMIN_INIT_NOR_USER					 -43
#define PDD_ERROR_ADMIN_SEM_CREATE_SEM                  -140
#define PDD_ERROR_ADMIN_SEM_DESTROY_SINCE_WAIT          -141
#define PDD_ERROR_ADMIN_SEM_DESTROY                     -142
#define PDD_ERROR_ADMIN_SEM_GET_VALUE                   -143
#define PDD_ERROR_ADMIN_NO_PROZESS_ATTACH               -144
#define PDD_ERROR_ADMIN_SHARED_MEM_NOT_VALID            -145
#define PDD_ERROR_ADMIN_SEM_WAIT                        -146
#define PDD_ERROR_ADMIN_SEM_POST                        -147
#define PDD_ERROR_NOR_USER_INIT_FAIL                    -170
#define PDD_ERROR_NOR_USER_LFX_WRONG_HANDLE             -171
#define	PDD_ERROR_NOR_USER_NO_VALID_SECTOR_SIZE         -172
#define PDD_ERROR_NOR_USER_NO_VALID_CHUNK_SIZE          -173
#define PDD_ERROR_NOR_USER_NO_VALID_NUMBER_OF_BLOCKS    -174
#define PDD_ERROR_NOR_USER_READ_FROM_CLUSTER            -175
#define PDD_ERROR_NOR_USER_WRITE_TO_CLUSTER             -176
#define PDD_ERROR_NOR_USER_WRONG_SIZE                   -177
#define PDD_ERROR_NOR_USER_NO_SPACE_CLUSTER_FULL        -178
#define PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED      -179
#define PDD_ERROR_NOR_USER_ERASE_FAIL                   -180
#define PDD_ERROR_NOR_USER_READ_DUMP                    -181
#define PDD_ERROR_NOR_USER_CONFIGURATION_HEADER         -182
#define PDD_ERROR_NOR_USER_WRITE_CLUSTER                -183
#define PDD_ERROR_NOR_USER_CHECKSUM                     -184    
#define PDD_ERROR_NO_BUFFER                             -220

/************************************************************************
| macros (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef enum 
{
  PDD_SEM_ACCESS_NOR_USER,  
  PDD_SEM_ACCESS_LAST
}tePddAccess;

/* header data stream*/
typedef struct
{
  tU8       u8DataStreamName[PDD_NOR_MAX_SIZE_NAME];
  tU32      u32Offset;
  tU32      u32Lenght;
  tU32      u32Count;
}tsPddNorUserHeaderDataStream;

/*structure for the driver info shared memory*/
typedef struct
{
  tU32                           u32SectorSize;
  tU32                           u32ClusterSize;
  tU32                           u32NumBlocks;
  tU32                           u32ClusterSizeData;
  tS32                           s32NumClusterPerSector;
  tS32                           s32NumCluster;  
  tU8                            u8pBufferDataOld[PDD_NOR_USER_CLUSTER_SIZE];  
  tS32                           s32LastClusterUsed;
  tU16                           u16ChunkSize;
  tU32                           u32EraseSectorCount; 
  tsPddNorUserHeaderDataStream   sHeaderDataStream[PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM];
}tsPddNorUserInfo;

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
/* pdd_admin_nor_user function */
tS32                 PDD_IsInit(tePddLocation,const char* PtsDataStreamName);
tS32                 PDD_Lock(tePddAccess);
tS32                 PDD_UnLock(tePddAccess);
tsPddNorUserInfo*    PDD_GetNorUserInfoShm(void);

/* function call from pdd_admin_nor_user */
tS32  PDD_NorUserAccessInit(void);
tS32  PDD_NorUserAccessDeInit(void);
tS32  PDD_NorUserAccessInitProcess(void);
tS32  PDD_NorUserAccessDeInitProcess(void);

/*for debug information*/
tBool PDD_bReadDataStreamConfiguration(tsPddNorUserInfo*);
tS32  PDD_NorUserAccessErase(void); 
tS32  PDD_NorUserAccessReadPartition(void *Ppu8ReadBuffer);
tS32  PDD_NorUserAccessReadActualCluster(void *Ppu8ReadBuffer);
tS32  PDD_NorUserAccessWritePartition(void *Ppu8ReadBuffer);
tS32  PDD_NorUserAccessGetActualCluster(void);
tU32  PDD_NorUserAccessGetEraseCounter(void);

//datastream access function 
tS32  PDD_NorUserAccessGetDataStreamSize(const char* PtsDataStreamName);
tS32  PDD_NorUserAccessReadDataStream(const char* PtsDataStreamName,void *Ppu8ReadBuffer,tS32 Ps32SizeReadBuffer );
tS32  PDD_NorUserAccessWriteDataStream(const char* PtsDataStreamName,void *Ppu8WriteBuffer,tS32 Ps32SizeWriteBuffer);

#ifdef __cplusplus
}
#endif
#else
#error pdd_access_nor_user.h included several times
#endif
