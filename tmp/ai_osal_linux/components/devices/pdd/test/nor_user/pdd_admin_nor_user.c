/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        pdd_admin_nor_user.c
 *
 * This file contains all functions for initialize 
 * deinitialize the pdd module
 * 
 * global function:
 * -- 
 *
 * local function:
 * -- 
 *
 * @date        2012-10-10
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */

/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>	   
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <grp.h>
#include "system_types.h"
#include "system_definition.h"
#include "pdd_access_nor_user.h"

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
/* name for semaphore*/
#define PDD_SEM_NAME_INIT_LOCK             "PDD-SemInit"
#define PDD_SEM_NAME_INIT_LOCK_IN_SHM      "/dev/shm/sem.PDD-SemInit"
/* name for shared memory*/
#define PDD_SHM_GLOBAL_NAME                "PDD-Shm"
/*access rights*/
#define PDD_ACCESS_RIGTHS                  S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/
typedef struct
{
  tS32                  s32AttachedProcesses;                 /*process attach counter*/
  sem_t                 tSemLockAccess[PDD_SEM_ACCESS_LAST];  /*semaphore for access to location file, nor, INC*/
  tBool                 vbInit[PDD_LOCATION_LAST];            /*init varibale for each location */  
  tsPddNorUserInfo      vtsPddNorUserInfo;  
}trPddShm;

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/
static sem_t    *vPdd_SemLockInit;                 /*semaphore for init create with name*/

/************************************************************************
| variable definition (scope: module-global)
|-----------------------------------------------------------------------*/
/*shared memory definition*/
intptr_t      vPdd_ShmId=-1;                            /*shared memory id */
trPddShm* vpPdd_Shm = MAP_FAILED;                   /*pointer of shared memory*/

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static void  PDD_InitAccess(void);
static void  PDD_DeInitAccess(void);
static void  PDD_CreateSemaphore(tePddAccess);
static void  PDD_DestroySemaphore(tePddAccess);
/******************************************************************************
* FUNCTION: pdd_process_attach()
*
* DESCRIPTION: Libraries should export initialization and cleanup routines 
*              using the gcc __attribute__((constructor)) 
*              and __attribute__((destructor)) function attributes. 
*              Constructor routines are executed before dlopen returns 
*              (or before main() is started if the library is loaded at load time). 
*              Destructor routines are executed before dlclose returns (
*              or after exit() or completion of main() 
*              if the library is loaded at load time).
*              This is the attach function
*
* PARAMETERS:  Void
*     
*
* RETURNS: void  
*
* HISTORY: Created by Andrea Bueter 2013 02 18
*****************************************************************************/
void __attribute__ ((constructor)) pdd_process_attach(void)
{
  tBool VbFirstAttach = TRUE;

  /* protect against initializing race, the second process has to wait until this is finished*/
  /* Linux Programmer's Manual: 
     If O_CREAT is specified in oflag, then the semaphore is created if it does not
     already exist. If both O_CREAT and O_EXCL are
     specified in oflag, then an error is returned if a semaphore with the given
     name already exists.*/
  /* try to create semaphore with name;(if success semaphore's value is set to zeror)*/
  vPdd_SemLockInit = sem_open(PDD_SEM_NAME_INIT_LOCK, O_EXCL | O_CREAT, PDD_ACCESS_RIGTHS, 0);
  if(vPdd_SemLockInit != SEM_FAILED)
  { /* no error; semaphore create => first attach */
    /* first attach */
    VbFirstAttach = TRUE;
	/* no error; semaphore create => first attach */   
    PDD_TRACE(PDD_ADMIN_FIRST_ATTACH,0,0);
    /* create shared memory at first process attach */
    vPdd_ShmId= shm_open(PDD_SHM_GLOBAL_NAME, O_EXCL|O_RDWR|O_CREAT|O_TRUNC, PDD_ACCESS_RIGTHS );
    if(ftruncate(vPdd_ShmId, sizeof(trPddShm)) == -1)
    {
      vPdd_ShmId=-1;
    }
  }
  else
  { /* open semaphore*/
    vPdd_SemLockInit= sem_open(PDD_SEM_NAME_INIT_LOCK, 0);
    /* lock semaphore*/
    sem_wait(vPdd_SemLockInit);
	/* not first attach*/
    VbFirstAttach = FALSE;
	/*trace */
    PDD_TRACE(PDD_ADMIN_NOT_FIRST_ATTACH,0,0);
    /* open shared memory*/
    vPdd_ShmId = shm_open(PDD_SHM_GLOBAL_NAME, O_RDWR ,PDD_ACCESS_RIGTHS);    
  }
  if(vPdd_ShmId!=-1)
  { /* map global shared memory into address space */
    vpPdd_Shm = (trPddShm*)mmap( NULL,sizeof(trPddShm),PROT_READ | PROT_WRITE,MAP_SHARED,vPdd_ShmId,0);
    /* if no error */
    if(vpPdd_Shm != MAP_FAILED)
    { /* if first proccess access */
      if(VbFirstAttach)
      { /*set process attach to zero*/
	    vpPdd_Shm->s32AttachedProcesses=0;
	    PDD_InitAccess();	 		
      }	
	  /*increment counter*/
      vpPdd_Shm->s32AttachedProcesses++;
	  /*trace */
      PDD_TRACE(PDD_ADMIN_ATTACH_COUNT,&vpPdd_Shm->s32AttachedProcesses,sizeof(tU32));
    }
    /* init finished, let's go! */
    sem_post(vPdd_SemLockInit);
  }
}
/******************************************************************************
* FUNCTION: pdd_process_detach()
*
* DESCRIPTION: Libraries should export initialization and cleanup routines 
*              using the gcc __attribute__((constructor)) 
*              and __attribute__((destructor)) function attributes. 
*              Constructor routines are executed before dlopen returns 
*              (or before main() is started if the library is loaded at load time). 
*              Destructor routines are executed before dlclose returns (
*              or after exit() or completion of main() 
*              if the library is loaded at load time).
*              This is the DeAttach function.
*
* PARAMETERS: Void
*     
*
* RETURNS: void 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void __attribute__ ((destructor)) pdd_process_detach(void)
{

}
/******************************************************************************
* FUNCTION: PDD_InitAccess()
*
* DESCRIPTION: init for all possible accesses 
*
* PARAMETERS: Void
*     
*
* RETURNS: void 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void  PDD_InitAccess(void)
{
  /* create semaphore for each location */
  PDD_CreateSemaphore(PDD_SEM_ACCESS_NOR_USER);
  /* set init flag for access to location*/
  vpPdd_Shm->vbInit[PDD_LOCATION_NOR_USER]=FALSE;
}
/******************************************************************************
* FUNCTION: PDD_DeInitAccess()
*
* DESCRIPTION: init for all possible accesses 
*
* PARAMETERS: Void
*     
*
* RETURNS: void 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void  PDD_DeInitAccess(void)
{
  /*deinit nor user access*/
  PDD_NorUserAccessDeInit();		 
  vpPdd_Shm->vbInit[PDD_LOCATION_NOR_USER]=FALSE;
  /*destroy semaphore, only if not blocked*/
  PDD_DestroySemaphore(PDD_SEM_ACCESS_NOR_USER);
}
/******************************************************************************
* FUNCTION: PDD_CreateSemaphore()
*
* DESCRIPTION: create and open semaphore and shared memory 
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: success
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void PDD_CreateSemaphore(tePddAccess PpAccess)
{
  sem_t* VpSemLock=&vpPdd_Shm->tSemLockAccess[PpAccess];
  if(sem_init(VpSemLock, 1/*shared*/, 1) < 0)
  {/* trace error*/        
    tS32 Vs32Result=PDD_ERROR_ADMIN_SEM_CREATE_SEM;  
	PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else
  {
    PDD_TRACE(PDD_ADMIN_SEM_INIT_SUCCESS,&PpAccess,sizeof(tePddAccess));
  }
}
/******************************************************************************
* FUNCTION: PDD_DestroySemaphore()
*
* DESCRIPTION: create and open semaphore and shared memory 
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: success
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void PDD_DestroySemaphore(tePddAccess PpAccess)
{
  tS32   Vs32Value;
  tS32   Vs32Result;
  sem_t* VpSemLock=&vpPdd_Shm->tSemLockAccess[PpAccess];

  if(sem_getvalue(VpSemLock,&Vs32Value)==0)
  { /* trace out value*/
    PDD_TRACE(PDD_ADMIN_SEM_VALUE,&Vs32Value,sizeof(tS32));	
	/* if value=0;semaphore waits => no destroy !!!*/
	if(Vs32Value==0)
	{
      Vs32Result=PDD_ERROR_ADMIN_SEM_DESTROY_SINCE_WAIT; 
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
	}
	else
	{ /*destroy semaphore*/
      if(sem_destroy(VpSemLock) < 0)
      {/* trace error*/        
        Vs32Result=PDD_ERROR_ADMIN_SEM_DESTROY; 
	    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
      }
      else
      {
        PDD_TRACE(PDD_ADMIN_SEM_DESTROY_SUCCESS,&PpAccess,sizeof(tePddAccess));
      }
	}
  }
  else
  {
    Vs32Result=PDD_ERROR_ADMIN_SEM_GET_VALUE;  
	PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
}
/******************************************************************************
* FUNCTION: PDD_IsInit()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2013 03 07
*****************************************************************************/
tS32 PDD_IsInit(tePddLocation PenLocation,const char* PtsInfoStream)
{
  tS32 Vs32Result=PDD_OK;
  /*check shared memory*/
  if(vpPdd_Shm != MAP_FAILED)
  { /*check attached proccesses*/
    if(vpPdd_Shm->s32AttachedProcesses <= 0)
	  { /*trace */
	    Vs32Result=PDD_ERROR_ADMIN_NO_PROZESS_ATTACH;  
      PDD_TRACE(PDD_ADMIN_ATTACH_COUNT,vpPdd_Shm->s32AttachedProcesses,sizeof(tU32));
	    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
	    PDD_FATAL_M_ASSERT_ALWAYS();
	  }
	  else
	  { /* init location*/
      switch(PenLocation)
	    {	    
	      case PDD_LOCATION_NOR_USER:
	      { /*call init process function for variable, which valid for process; open LFX*/
		      Vs32Result=PDD_NorUserAccessInitProcess();
		      if(Vs32Result==PDD_OK)
		      { /*check if init for all process done */
		        if(vpPdd_Shm->vbInit[PDD_LOCATION_NOR_USER]==FALSE)
		        { /*init for all process*/
		          if(PDD_NorUserAccessInit()!=PDD_OK)
		          {
	              Vs32Result=PDD_ERROR_ADMIN_INIT_NOR_USER;  
	              PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));		
	            }
		          else
		          {
		            vpPdd_Shm->vbInit[PDD_LOCATION_NOR_USER]=TRUE;
		          }
		        }
		      }		 
	      }break;		
	      default:
	      {
	        Vs32Result=PDD_ERROR_WRONG_PARAMETER;  
	      }break;
	    }/*end switch*/
	  }/*end else*/
  }/*end if*/
  else
  { /*error */
	  Vs32Result=PDD_ERROR_ADMIN_SHARED_MEM_NOT_VALID;  
    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  return(Vs32Result);
}


/******************************************************************************
* FUNCTION: PDD_Lock()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
tS32 PDD_Lock(tePddAccess PpAccess)
{
  tS32   Vs32Value;
  tS32   Vs32Result=PDD_OK;
  sem_t* VpSemLock=&vpPdd_Shm->tSemLockAccess[PpAccess];

  /* get value semaphore*/
  if(sem_getvalue(VpSemLock,&Vs32Value)==0)
  { /* trace out value*/
    PDD_TRACE(PDD_ADMIN_SEM_VALUE,&Vs32Value,sizeof(tS32));	
    /* lock semaphore*/
    if(sem_wait(VpSemLock)==0)  
	{ /* no error */
	  PDD_TRACE(PDD_ADMIN_SEM_WAIT,&PpAccess,sizeof(tePddAccess));	
    }/*end if*/
	else
  	{
      Vs32Result=PDD_ERROR_ADMIN_SEM_WAIT; 
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
	  PDD_FATAL_M_ASSERT_ALWAYS();
	}
  }
  else
  {
    Vs32Result=PDD_ERROR_ADMIN_SEM_GET_VALUE; 
    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_UnLock()
*
* DESCRIPTION: create and open semaphore and shared memory 
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: success
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2012 10 10
*****************************************************************************/
tS32 PDD_UnLock(tePddAccess PpAccess)
{ 
  tS32   Vs32Value;
  tS32   Vs32Result=0;
  sem_t* VpSemLock=&vpPdd_Shm->tSemLockAccess[PpAccess];

  /* get value semaphore*/
  if(sem_getvalue(VpSemLock,&Vs32Value)==0)
  { /* trace out value*/
    PDD_TRACE(PDD_ADMIN_SEM_VALUE,&Vs32Value,sizeof(tS32));
	/* post semaphore*/
    if(sem_post(VpSemLock)==0)  
	{ /* no error => get pointer*/
	  PDD_TRACE(PDD_ADMIN_SEM_POST,&PpAccess,sizeof(tePddAccess));	
    }/*end if*/
	else
	{
      Vs32Result=PDD_ERROR_ADMIN_SEM_POST; 
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
	  PDD_FATAL_M_ASSERT_ALWAYS();
	}
  }
  else
  {
    Vs32Result=PDD_ERROR_ADMIN_SEM_GET_VALUE;  
    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_GetNorUserNorInfoShm()
*
* DESCRIPTION: get nor user info from shared memory 
*
* RETURNS: 
*       pointer
*
* HISTORY:Created by Andrea Bueter 2012 10 10
*****************************************************************************/
tsPddNorUserInfo*  PDD_GetNorUserInfoShm(void)
{
	return(&vpPdd_Shm->vtsPddNorUserInfo);
}

#ifdef __cplusplus
}
#endif
/******************************************************************************/
/* End of File pdd_admin.c                                                    */
/******************************************************************************/
