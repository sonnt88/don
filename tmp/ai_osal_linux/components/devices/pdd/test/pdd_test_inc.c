/************************************************************************
| FILE:         pdd_test_inc
| PROJECT:      platform
| SW-COMPONENT: PDD
|------------------------------------------------------------------------------
| DESCRIPTION:  Test program for send PDD INC message
|               The main() function sends PDD messages to the socked and waits
|               then for an answer. 
|               If define PDD_TEST_SIMULATION is set, a thread is create.
|               This thread recieves the send messages and gives an answer.
|
|*****************************************************************************/
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <netinet/tcp.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "inc.h"   
#include "inc_ports.h"
#include "inc_scc_pdd.h"
#include "dgram_service.h" 
/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
/* Tests*/
//#define PDD_INC_WRITE_READ_UNTIL_128_BYTES
//#define PDD_INC_WRITE_WITHOUT_CHECK_COMPONENT_STATUS
//#define PDD_INC_WRITE_INVALID_SIZE
#define PDD_INC_WRITE_READ_6_BYTES
#define PDD_INC_TESTPOOL_1    1

/* simulation for V850 PDD messages*/
//#define PDD_TEST_SIMULATION								
/* simulation gets the TP- Marker, but doesn't interpret. Furthermore the simulation sends no TP- Marker*/
//#define PDD_TEST_SIMULATION_WITH_OUT_TP_MARKER          

#ifdef PDD_INC_WRITE_READ_UNTIL_128_BYTES
/* maximun message size for the buffer*/
#define PDD_TEST_LENGTH_DATA                   128
#define PDD_MAX_MSG_SIZE_WITH_TP     (PDD_TEST_LENGTH_DATA/*SCC_PD_NET_MAX_LENGTH_DATA_BYTES*/+4)
/* 1.Byte:       TpMarker 
   2.Byte:       MsgId
   3.+4.Byte:    Length
   1..128 Bytes: Data*/
/* for test table pdd_TestSendMsg */
/* position for legnth and mepdd_TestSendMsgssage information of one message */
#define PDD_TEST_INC_TABLE_MSG_BEGINN           1
#define PDD_TEST_INC_MSG_DATA_WRITE             4
#endif
/* position kind message */
#ifdef PDD_INC_WRITE_READ_UNTIL_128_BYTES
#define PDD_TEST_INC_TABLE_READ_MSG_BEGIN       1
#define PDD_TEST_INC_TABLE_WRITE_MSG_BEGIN      3
#define PDD_TEST_INC_TABLE_END                  5
#endif


/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/
/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/ 
#ifdef PDD_INC_WRITE_READ_UNTIL_128_BYTES
static unsigned char pdd_TestSendMsg[PDD_TEST_INC_TABLE_END][4]=
    { /* PDD_TEST_INC_TABLE_MSG_LEN, PDD_TEST_INC_TABLE_MSG_BEGINN:MsgId        ,                           */
	  {0x03                        , SCC_PD_NET_C_COMPONENT_STATUS_MSGID        ,SCC_PD_NET_APPLICATION_STATUS_ACTIVE, 0x01     },
	  {0x02                        , SCC_PD_NET_C_READ_DATA_POOL                ,PDD_INC_TESTPOOL_1, 0x00                                  },
	  {0x02                        , SCC_PD_NET_C_READ_DATA_POOL                ,PDD_INC_TESTPOOL_1, 0x00                                  },
	  /* Len                       , PDD_TEST_INC_TABLE_MSG_BEGINN:Tp           , MsgId                                      */
	  {0x03                        , SCC_PD_NET_TP_MARKER+2                     ,SCC_PD_NET_C_WRITE_DATA_POOL, PDD_INC_TESTPOOL_1  },
	  {0x03                        , SCC_PD_NET_TP_MARKER+2                     ,SCC_PD_NET_C_WRITE_DATA_POOL, PDD_INC_TESTPOOL_1   },
    };

static unsigned char pdd_TestRecvMsg[PDD_TEST_INC_TABLE_END][4]=
    { /* PDD_TEST_INC_TABLE_MSG_LEN, PDD_TEST_INC_TABLE_MSG_BEGINN:MsgId        ,                           */
	  {0x03                        , SCC_PD_NET_R_COMPONENT_STATUS_MSGID             ,SCC_PD_NET_APPLICATION_STATUS_ACTIVE, 0x01 },
	  {0x03                        , SCC_PD_NET_R_READ_DATA_POOL                     ,0x00,0x00                              },
	  {0x03                        , SCC_PD_NET_R_READ_DATA_POOL                     ,0x00,0x00                              },
	  {0x02                        , SCC_PD_NET_R_WRITE_DATA_POOL                    ,0x00,0x00                              },
	  {0x02                        , SCC_PD_NET_R_WRITE_DATA_POOL                    ,0x00,0x00                              },
    };
#endif

#ifdef PDD_INC_WRITE_READ_UNTIL_128_BYTES
static unsigned char pdd_TestBufferWrite[PDD_TEST_LENGTH_DATA]=
{
 0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
 0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,
 0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,
 0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,
 0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,
 0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,
 0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e,0x7f,
 0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f, //128
};
#endif
#ifdef PDD_TEST_SIMULATION
static unsigned char pdd_TestBufferAtShutdown[PDD_TEST_LENGTH_DATA];
static unsigned char pdd_TestBufferDirectly[PDD_TEST_LENGTH_DATA];
static unsigned char pdd_TestSizeBufferShutdown;
static unsigned char pdd_TestSizeBufferDirectly;
#endif
/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static void PDD_TestIncHexDump(const char *PpString, char *Pbuffer, int PiLen);
#ifdef PDD_INC_WRITE_READ_UNTIL_128_BYTES
static int  PDD_TestIncSendMsg(sk_dgram *,unsigned char , unsigned char );
static int  PDD_TestIncRecvMsg(sk_dgram *,unsigned char , unsigned char );
#endif
#ifdef PDD_TEST_SIMULATION
static void PDD_TestIncThread(void *arg);
static int  PDD_TestRecvStatus(unsigned char* PubBuffer,int PiLen, sk_dgram *PpDgram);
static int  PDD_TestRecvRead(unsigned char* PubBuffer,int PiLen, sk_dgram *PpDgram);
static int  PDD_TestRecvWrite(unsigned char* PubBuffer,int PiLen, sk_dgram *PpDgram);
static int  PDD_TestSendMessageReject(unsigned char PubReason, sk_dgram *PpDgram);

/******************************************************************************
* FUNCTION: PDD_TestIncThread(void *Parg)
*
* DESCRIPTION:  Thread for the simulation of the V850 PDD messages.
*               Connect to the socket and read the messages which sends to the socket
*               The messages, which are read, will be answered.
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
static void PDD_TestIncThread(void *arg)
{
   int                   ViReturnCode=0;
   unsigned char         VucBuffer[PDD_MAX_MSG_SIZE_WITH_TP];
   int                   ViSocket;
   sk_dgram              *VpDgram=NULL;
   struct hostent        *VsLocal, *VsRemote;
   struct sockaddr_in    VsLocalAddr, VsRemoteAddr;
   
   /* ----------------------  init ------------------------------------------------------*/
   /*Create socket*/
   ViSocket = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0); /* use AF_BOSCH_INC_AUTOSAR to facilitate eventual transition to TCP/IP */
   if (ViSocket == -1)
   {
      int VerrSv = errno;
	  fprintf(stderr, "R> create socket failed: %d\n", VerrSv);
   }
   else
   { /*Initialize datagram service*/
     VpDgram = dgram_init(ViSocket, DGRAM_MAX, NULL);
     if (VpDgram == NULL)
     {
        fprintf(stderr, "R> dgram_init failed\n");
     }
	 else
	 { /* Get local address and port */
	   VsLocal = gethostbyname("scc");
       VsLocalAddr.sin_family = AF_INET;
	   memmove((char *) &VsLocalAddr.sin_addr.s_addr, (char *) VsLocal->h_addr, VsLocal->h_length);      
       VsLocalAddr.sin_port = htons(PD_NET_PORT); /* from inc_ports.h */
       fprintf(stderr, "R> local %s:%d\n", inet_ntoa(VsLocalAddr.sin_addr), ntohs(VsLocalAddr.sin_port));
       /* Get remote address and port */  
	   VsRemote = gethostbyname("scc-local");
       VsRemoteAddr.sin_family = AF_INET;
	   memmove((char *) &VsRemoteAddr.sin_addr.s_addr, (char *) VsRemote->h_addr, VsRemote->h_length);     
       VsRemoteAddr.sin_port = htons(PD_NET_PORT); /* from inc_ports.h */
       fprintf(stderr, "R> remote %s:%d\n", inet_ntoa(VsRemoteAddr.sin_addr), ntohs(VsRemoteAddr.sin_port)); 

       /*Bind socket to local address/port*/
	   /*Deactivation accepted. Authorization string is authorized LINT-deactivation #364.*/
       ViReturnCode = bind(ViSocket, (struct sockaddr *) &VsLocalAddr, sizeof(VsLocalAddr));/*lint !e64*/
       if (ViReturnCode < 0)
       {
          int VerrSv = errno;
          fprintf(stderr, "R> bind failed: %d\n", VerrSv);
       }
       else
       { /* Connect to remote address/port */
	     /*Deactivation accepted. Authorization string is authorized LINT-deactivation #364.*/
         ViReturnCode = connect(ViSocket, (struct sockaddr *) &VsRemoteAddr, sizeof(VsRemoteAddr));/*lint !e64*/
         if (ViReturnCode < 0)
         {
           int VerrSv = errno;
           fprintf(stderr,"R> connect failed: %d\n", VerrSv);
         }
		 else
		 {/*init buffer and counter*/
		   memset(&pdd_TestBufferAtShutdown[0],0x00,PDD_TEST_LENGTH_DATA);
		   memset(&pdd_TestBufferDirectly[0],0x00,PDD_TEST_LENGTH_DATA);
		   pdd_TestSizeBufferShutdown=0;
		   pdd_TestSizeBufferDirectly=0;
		   //fprintf(stderr, "R> size buffer shutdown: %d\n", pdd_TestSizeBufferShutdown);
           //fprintf(stderr, "R> size buffer directly: %d\n", pdd_TestSizeBufferDirectly);
		 }
       }
	 }
   }
   /* ----------------------  read write ------------------------------------------------------*/
   if((ViReturnCode>=0)&&(VpDgram!=NULL))
   {
     do
     {
      //usleep(1000000);
	  //fprintf(stderr, "R> sleep end \n");
	  ViReturnCode=dgram_recv(VpDgram,&VucBuffer[0], PDD_MAX_MSG_SIZE_WITH_TP);
      /* check error */
      if (ViReturnCode < 0)
      {
        int VerrSv = errno;
        fprintf(stderr, "R> recv failed: %d\n", VerrSv);
      }
      else if (ViReturnCode == 0)
      {/*wrong size*/
        fprintf(stderr, "R> invalid msg size: %d\n", ViReturnCode);
        ViReturnCode=-1;
      }
      else 
      { /*trace out message */
		#ifdef PDD_TEST_SIMULATION_WITH_OUT_TP_MARKER
		if(VucBuffer[0]==SCC_PD_NET_TP_MARKER+2)
		{/*copy data*/
		  memmove(&VucBuffer[0],&VucBuffer[1],ViReturnCode);
          /*increment number*/
		  ViReturnCode--;
		}
        #endif
	    switch(VucBuffer[0])
        {
         case SCC_PD_NET_C_COMPONENT_STATUS_MSGID:
	     {
		   PDD_TestIncHexDump("R> read",&VucBuffer[0],ViReturnCode);
	       ViReturnCode=PDD_TestRecvStatus(&VucBuffer[0],ViReturnCode,VpDgram);
		 }break;
         case SCC_PD_NET_C_READ_DATA_POOL:
		   PDD_TestIncHexDump("R> read",&VucBuffer[0],ViReturnCode);	      
		   ViReturnCode=PDD_TestRecvRead(&VucBuffer[0],ViReturnCode,VpDgram);
	     break;
         case SCC_PD_NET_C_READ_DATA_POOL:
		   PDD_TestIncHexDump("R> read",&VucBuffer[0],ViReturnCode);	    
		   ViReturnCode=PDD_TestRecvRead(&VucBuffer[0],ViReturnCode,VpDgram);
	     break;
         case SCC_PD_NET_C_WRITE_DATA_POOL: 
		   PDD_TestIncHexDump("R> read",&VucBuffer[0],ViReturnCode);
		   ViReturnCode=PDD_TestRecvWrite(&VucBuffer[0],ViReturnCode,VpDgram);
         break;
         case SCC_PD_NET_C_WRITE_DATA_POOL:
		  PDD_TestIncHexDump("R> read",&VucBuffer[0],ViReturnCode);
		  ViReturnCode=PDD_TestRecvWrite(&VucBuffer[0],ViReturnCode,VpDgram);
	     break;
         default:
	     {
		   PDD_TestIncHexDump("R> read> unknown MSGID",&VucBuffer[0],ViReturnCode);
		   PDD_TestSendMessageReject(SCC_PD_NET_REJ_UNKNOWN_MESSAGE,VpDgram);
		   ViReturnCode=-3;
	     }break;
        }
      }/*end else*/
    }while(ViReturnCode>=0);
	/*------------------- Deinitialize datagram service ------------------------*/
    ViReturnCode=dgram_exit(VpDgram);
    if (ViReturnCode < 0)
    {
      int VerrSv = errno;
      fprintf(stderr, "R> dgram_exit failed: %d\n", VerrSv);
    }
    /*Close socket */
    close(ViSocket);
  }/*end if init ok*/
  fprintf(stderr, "R> test end \n");
}
/******************************************************************************
* FUNCTION: PDD_TestRecvStatus()
*
* DESCRIPTION: checks the status message and answered. 
*
* PARAMETERS:
*        PubBuffer: buffer of the read message
*        PiLen:     length of the read message
*        PpDgram:   pointer of the socket
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2013 03 13
*****************************************************************************/
static int  PDD_TestRecvStatus(unsigned char* PubBuffer,int PiLen, sk_dgram *PpDgram)
{
  int              ViReturnCode=0;
  unsigned char    VucSendBuf[2];

  /*check length*/
  if (PiLen != 2) 
  {
    fprintf(stderr, "R> invalid msg size: 0x%02X %d\n", PubBuffer[0], PiLen);
	ViReturnCode=-1;
  }
  else if (PubBuffer[1] != SCC_PD_NET_APPLICATION_STATUS_ACTIVE) 
  {
    fprintf(stderr, "R> invalid status: %d\n", PubBuffer[1]);
    ViReturnCode=-2;
  }
  else
  {
    VucSendBuf[0] = SCC_PD_NET_R_COMPONENT_STATUS_MSGID;
    VucSendBuf[1] = SCC_PD_NET_APPLICATION_STATUS_ACTIVE;
    /* send message*/
    ViReturnCode=dgram_send(PpDgram,&VucSendBuf[0],2);
    if (ViReturnCode == -1)
    {
      int VerrSv = errno;
      fprintf(stderr, "R> send failed: %d\n", VerrSv);
    }
    else
    { /*trace out message */
      PDD_TestIncHexDump("R> send",&VucSendBuf[0],2);
    }
  }
  return(ViReturnCode);
}
/******************************************************************************
* FUNCTION: PDD_TestRecvRead()
*
* DESCRIPTION: checks the message "READ_DATA_....." and answered. 
*
* PARAMETERS:
*        PubBuffer: buffer of the message
*        PiLen:     length of the message
*        PpDgram:   pointer of the socket
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2013 03 13
*****************************************************************************/
static int  PDD_TestRecvRead(unsigned char* PubBuffer, int PiLen, sk_dgram *PpDgram)
{
  int              ViReturnCode=0;
  unsigned char    VucSendBuf[PDD_MAX_MSG_SIZE_WITH_TP];
  unsigned char    VucSendSize;

  /*check length*/
  if (PiLen != 1) 
  {
    fprintf(stderr, "R> invalid msg size: 0x%02X %d\n", PubBuffer[0], PiLen);
	ViReturnCode=-1;
  }
  else
  {
    #ifdef PDD_TEST_SIMULATION_WITH_OUT_TP_MARKER
    VucSendSize=3;
	VucSendBuf[0] = PubBuffer[0]+1;
	VucSendBuf[1] = 0;
    #else
    VucSendSize=4;
	VucSendBuf[0] = SCC_PD_NET_TP_MARKER+2;
    VucSendBuf[1] = PubBuffer[0]+1;
	VucSendBuf[2] = 0;
    #endif
	if (PubBuffer[0] == SCC_PD_NET_C_READ_DATA_POOL) 
	{      
	 // fprintf(stderr, "R> size buffer shutdown: %d\n", pdd_TestSizeBufferShutdown);
	  VucSendBuf[VucSendSize-1] = pdd_TestSizeBufferShutdown;
	  memmove(&VucSendBuf[VucSendSize],&pdd_TestBufferAtShutdown[0],pdd_TestSizeBufferShutdown);
	  VucSendSize=VucSendSize+pdd_TestSizeBufferShutdown;
	}
	else 
	{
	 // fprintf(stderr, "R> size buffer directly: %d\n", pdd_TestSizeBufferDirectly);
	  VucSendBuf[VucSendSize-1] = pdd_TestSizeBufferDirectly;
	  memmove(&VucSendBuf[VucSendSize],&pdd_TestBufferDirectly[0],pdd_TestSizeBufferDirectly);
	  VucSendSize=VucSendSize+pdd_TestSizeBufferDirectly;
	}
    /* send message*/
    ViReturnCode=dgram_send(PpDgram,&VucSendBuf[0],VucSendSize);
    if (ViReturnCode == -1)
    {
      int VerrSv = errno;
      fprintf(stderr, "R> send failed: %d\n", VerrSv);
    }
    else
    { /*trace out message */
      PDD_TestIncHexDump("R> send",&VucSendBuf[0],VucSendSize);
    }
  }
  return(ViReturnCode);
}
/******************************************************************************
* FUNCTION: PDD_TestRecvWrite()
*
* DESCRIPTION: checks the message "WRITE_DATA_....." and answered. 
*
* PARAMETERS:
*        PubBuffer: buffer of the message
*        PiLen:     length of the message
*        PpDgram:   pointer of the socket
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2013 03 13
*****************************************************************************/
static int  PDD_TestRecvWrite(unsigned char* PubBuffer,int PiLen, sk_dgram *PpDgram)
{
  int              ViReturnCode=0;
  unsigned char    VucSendBuf[2];

  /*check length*/
  if (PiLen >= PDD_MAX_MSG_SIZE_WITH_TP) 
  {
    fprintf(stderr, "R> invalid msg size: 0x%02X %d\n", PubBuffer[0], PiLen);
	ViReturnCode=-1;
  }
  else if (PubBuffer[2] > PDD_TEST_LENGTH_DATA) 
  {
    fprintf(stderr, "R> invalid data size: %d\n", PubBuffer[2]);
    ViReturnCode=-2;
  }
  else
  { /*copy data into buffer*/
    if (PubBuffer[0] == SCC_PD_NET_C_WRITE_DATA_POOL) 
	{      
	  pdd_TestSizeBufferShutdown=PubBuffer[2];
	 // fprintf(stderr, "R> size buffer shutdown: %d\n", pdd_TestSizeBufferShutdown);
	  memmove(&pdd_TestBufferAtShutdown[0],&PubBuffer[3],pdd_TestSizeBufferShutdown);
	}
	else 
	{
	  pdd_TestSizeBufferDirectly=PubBuffer[2];
	 // fprintf(stderr, "R> size buffer directly: %d\n", pdd_TestSizeBufferDirectly);
	  memmove(&pdd_TestBufferDirectly[0],&PubBuffer[3],pdd_TestSizeBufferDirectly);	 
	}
	/*response save done*/
    VucSendBuf[0] = PubBuffer[0]+1;
    /* send message*/
    ViReturnCode=dgram_send(PpDgram,&VucSendBuf[0],1);
    if (ViReturnCode == -1)
    {
      int VerrSv = errno;
      fprintf(stderr, "R> send failed: %d\n", VerrSv);
    }
    else
    { /*trace out message */
      PDD_TestIncHexDump("R> send",&VucSendBuf[0],1);
    }
  }
  return(ViReturnCode);
}
/******************************************************************************
* FUNCTION: PDD_TestSendMessageReject()
*
* DESCRIPTION: send the message reject with the reason "PubReason"
*
* PARAMETERS:
*        PubReason: reason defined in inc_SCC_PD_NET.h
*        PpDgram:   pointer of the socket
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
static int PDD_TestSendMessageReject(unsigned char PubReason, sk_dgram *PpDgram)
{
  int              ViReturnCode=0;
  unsigned char    VucSendBuf[2];

  /*message*/
  VucSendBuf[0]=SCC_PD_NET_R_REJECT_MSGID;
  VucSendBuf[1]=PubReason;
  /* send message*/
  ViReturnCode=dgram_send(PpDgram,&VucSendBuf[0],2);
  if (ViReturnCode == -1)
  {
    int VerrSv = errno;
    fprintf(stderr, "R> send failed: %d\n", VerrSv);
  }
  else
  { /*trace out message */
    PDD_TestIncHexDump("R> send",&VucSendBuf[0],2);
  }
  return(ViReturnCode);
}
#endif

/******************************************************************************
* FUNCTION: PDD_TestIncHexDump()
*
* DESCRIPTION: sprint out hex dump
*
* PARAMETERS:
*      PpString: String ro print out
*      PpBuffer: Buffer to print out in hex
*      PiLen:    Lenght of the buffer to print out
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
void PDD_TestIncHexDump(const char *PpString, char *PpBuffer, int PiLen)
{
  int ViInc;
  fprintf(stdout,"%s:",PpString);
  fprintf(stdout," size: 0x%02x data:",PiLen);
  for (ViInc=0;ViInc<PiLen;ViInc++)
  {
    fprintf(stdout," 0x%02x", PpBuffer[ViInc]);
  }
  fprintf(stdout,"\n");
}
#ifdef PDD_INC_WRITE_READ_UNTIL_128_BYTES
/******************************************************************************
* FUNCTION: PDD_TestIncSendMsg()
*
* DESCRIPTION: send message from the test table to the socked
*
* PARAMETERS:
*        PpDgram:     pointer of the socket
*        PucMsgCount: message in the test table
*        PucSizeData: for write messages, lenght of test data 
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
static int PDD_TestIncSendMsg(sk_dgram *PpDgram,unsigned char PucMsgCount, unsigned char PucSizeData)
{
  int              ViReturnCode=0;
  unsigned char    VucSendBuf[PDD_MAX_MSG_SIZE_WITH_TP];
  unsigned char    VucLength=pdd_TestSendMsg[PucMsgCount][0];

  /* clear buffer */
  memset(&VucSendBuf[0],0x00,PDD_MAX_MSG_SIZE_WITH_TP);
  /*copy message*/
  memmove(&VucSendBuf[0],&pdd_TestSendMsg[PucMsgCount][PDD_TEST_INC_TABLE_MSG_BEGINN],VucLength);
  /* check if write data*/
  if(PucMsgCount>=PDD_TEST_INC_TABLE_WRITE_MSG_BEGIN)
  { /* write messages*/
    /* set VucLength */ 
	VucLength=PDD_TEST_INC_MSG_DATA_WRITE;
    /* save len data in message*/
    VucSendBuf[VucLength-1]=PucSizeData;
    /*copy data*/
    memmove(&VucSendBuf[VucLength],&pdd_TestBufferWrite[0],PucSizeData);
	VucLength+=PucSizeData;
  }
  /* send message*/
  ViReturnCode=dgram_send(PpDgram,&VucSendBuf[0],VucLength);
  if (ViReturnCode == -1)
  {
    int VerrSv = errno;
    fprintf(stderr, "C> send failed: %d\n", VerrSv);
  }
  else
  { /*trace out message */
    PDD_TestIncHexDump("C> send",&VucSendBuf[0],VucLength);
  }
  return(ViReturnCode);
}
#endif
#ifdef PDD_INC_WRITE_READ_UNTIL_128_BYTES
/******************************************************************************
* FUNCTION: PDD_TestIncRecvMsg()
*
* DESCRIPTION: recieve read message the socked and checks the message. 
*              Send the answer message recieve to the socked.
*
* PARAMETERS:
*        PpDgram:     pointer of the socket
*        PucMsgCount: message in the test table
*        PucSizeData: for read messages, lenght of data 
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
static int  PDD_TestIncRecvMsg(sk_dgram *PpDgram,unsigned char PucMsgCount, unsigned char PucSizeReadData)
{
  int              ViReturnCode=0;
  unsigned char    VucRecvBuf[PDD_MAX_MSG_SIZE_WITH_TP];
  unsigned char    VucLength=(pdd_TestRecvMsg[PucMsgCount][0]);
  unsigned char    VucMsgId;

   if((PucMsgCount>=PDD_TEST_INC_TABLE_READ_MSG_BEGIN)&&(PucMsgCount<PDD_TEST_INC_TABLE_WRITE_MSG_BEGIN))
   {
     VucLength+=PucSizeReadData-1;
   }
   do 
   { /*receive message using datagram service*/
     //usleep(1000000);
     ViReturnCode=dgram_recv(PpDgram,&VucRecvBuf[0], PDD_MAX_MSG_SIZE_WITH_TP);
   } while(ViReturnCode < 0 && errno == EAGAIN);
   /* check error */
   if (ViReturnCode < 0)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> recv failed: %d\n", VerrSv);
   }   
   else 
   { /*trace out message */
	 switch(VucRecvBuf[0])
     {
       case SCC_PD_NET_R_COMPONENT_STATUS_MSGID:
	     PDD_TestIncHexDump("C> read",&VucRecvBuf[0],VucLength);	   
	   break;
       case SCC_PD_NET_R_REJECT_MSGID:
	   {
	     PDD_TestIncHexDump("C> read> SCC_PD_NET_R_REJECT_MSGID",&VucRecvBuf[0],VucLength);
		 ViReturnCode=-2;
	   }break;
       case SCC_PD_NET_R_READ_DATA_POOL:
	     PDD_TestIncHexDump("C> read",&VucRecvBuf[0],VucLength);
	   break;
       case SCC_PD_NET_R_READ_DATA_POOL:
	     PDD_TestIncHexDump("C> read",&VucRecvBuf[0],VucLength);
	   break;
       case SCC_PD_NET_R_WRITE_DATA_POOL: 
	     PDD_TestIncHexDump("C> read",&VucRecvBuf[0],VucLength);
       break;
       case SCC_PD_NET_R_WRITE_DATA_POOL:
	     PDD_TestIncHexDump("C> read",&VucRecvBuf[0],VucLength);
	   break;
       default:
	   {
	     PDD_TestIncHexDump("C> read> unknown MSGID",&VucRecvBuf[0],VucLength);
		 ViReturnCode=-3;
	   }break;
     }	
	 /* get expected Msgid*/
     VucMsgId=pdd_TestRecvMsg[PucMsgCount][PDD_TEST_INC_TABLE_MSG_BEGINN];
	 /*check id*/
	 if(VucMsgId!=VucRecvBuf[0])
	 {
	   fprintf(stderr, "C> unexpected MSGID: %x\n", VucRecvBuf[0]);
	   ViReturnCode=-4;
	 }
   }
  return(ViReturnCode);
}
#endif
/******************************************************************************
* FUNCTION: int main(int argc, char *argv[])
*
* DESCRIPTION: start function of the proccess
*
* PARAMETERS:
*      argc: number of strings
*      argv: string table
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
int main(int argc)
{
   int                   ViSocket=argc; /*for lint*/
   int                   ViReturnCode;
   sk_dgram              *VpDgram;
   struct hostent        *VsLocal, *VsRemote;
   struct sockaddr_in    VsLocalAddr, VsRemoteAddr;
#ifdef PDD_INC_WRITE_READ_UNTIL_128_BYTES
   unsigned char         VucLoopCount;
   unsigned char         VucDataCount;
#endif

   /* ----------------------  init ------------------------------------------------------*/
   /*Create socket*/
   ViSocket = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0); /* use AF_BOSCH_INC_AUTOSAR to facilitate eventual transition to TCP/IP */
   if (ViSocket == -1)
   {
      int VerrSv = errno;
      fprintf(stderr, "C> create socket failed: %d\n", VerrSv);
      return 1;
   }
   /*Initialize datagram service*/
   VpDgram = dgram_init(ViSocket, DGRAM_MAX, NULL);
   if (VpDgram == NULL)
   {
      fprintf(stderr, "C> dgram_init failed\n");
      return 1;
   }
   /* Get local address and port */
   VsLocal = gethostbyname("scc-local");
   VsLocalAddr.sin_family = AF_INET;
   memmove((char *) &VsLocalAddr.sin_addr.s_addr, (char *) VsLocal->h_addr, VsLocal->h_length);
   VsLocalAddr.sin_port = htons(PD_NET_PORT); /* from inc_ports.h */
   fprintf(stderr, "C> local %s:%d\n", inet_ntoa(VsLocalAddr.sin_addr), ntohs(VsLocalAddr.sin_port));
   /* Get remote address and port */
   VsRemote = gethostbyname("scc");
   VsRemoteAddr.sin_family = AF_INET;
   memmove((char *) &VsRemoteAddr.sin_addr.s_addr, (char *) VsRemote->h_addr, VsRemote->h_length);
   VsRemoteAddr.sin_port = htons(PD_NET_PORT); /* from inc_ports.h */
   fprintf(stderr, "C> remote %s:%d\n", inet_ntoa(VsRemoteAddr.sin_addr), ntohs(VsRemoteAddr.sin_port)); 
   /*Bind socket to local address/port*/
   /*Deactivation accepted. Authorization string is authorized LINT-deactivation #364.*/
   ViReturnCode = bind(ViSocket, (struct sockaddr *) &VsLocalAddr, sizeof(VsLocalAddr));/*lint !e64*/
   if (ViReturnCode < 0)
   {
      int VerrSv = errno;
      fprintf(stderr, "C> bind failed: %d\n", VerrSv);
      return 1;
   }
   /* Connect to remote address/port */
   /*Deactivation accepted. Authorization string is authorized LINT-deactivation #364.*/
   ViReturnCode = connect(ViSocket, (struct sockaddr *) &VsRemoteAddr, sizeof(VsRemoteAddr));/*lint !e64*/
   if (ViReturnCode < 0)
   {
      int VerrSv = errno;
      fprintf(stderr, "C> connect failed: %d\n", VerrSv);
      return 1;
   }
   /* create thread*/
   #ifdef PDD_TEST_SIMULATION
   {
     pthread_t  Vtid; 
     if (pthread_create(&Vtid, NULL, PDD_TestIncThread, (void *) VpDgram) == -1)
       fprintf(stderr, "C> pthread_create failed\n");
	 usleep(1000000);
   }
   #endif
#ifdef PDD_INC_WRITE_READ_UNTIL_128_BYTES
   /*---------------------  write read --------------------------------------*/
   VucLoopCount=0;
   VucDataCount=0;
   do
   { /*check if write Message*/
	 if(VucLoopCount == PDD_TEST_INC_TABLE_READ_MSG_BEGIN)
	 {/*write message => count data size */
       VucDataCount++;
	 }     
	 /* send message */
     ViReturnCode=PDD_TestIncSendMsg(VpDgram,VucLoopCount,VucDataCount);
	 if(ViReturnCode>=0)
	 {/* recieve message */
	   ViReturnCode=PDD_TestIncRecvMsg(VpDgram,VucLoopCount,VucDataCount);
	 }
	 /*count loop counter*/
	 VucLoopCount++;
	 /*check end of VucLoopCount*/
	 if(VucLoopCount == PDD_TEST_INC_TABLE_END)
	 {/*write message with next size */
       VucLoopCount=PDD_TEST_INC_TABLE_READ_MSG_BEGIN;
	 }     	
   }while((ViReturnCode>=0)&&(VucDataCount<PDD_TEST_LENGTH_DATA));
#endif
#ifdef PDD_INC_WRITE_WITHOUT_CHECK_COMPONENT_STATUS
   /*------------------------- test write without send component status ---------------------------*/
   /* send message*/   
   unsigned char    VubBuffer[255];
   VubBuffer[0]=SCC_PD_NET_TP_MARKER+2;
   VubBuffer[1]=SCC_PD_NET_C_WRITE_DATA_POOL;
   VubBuffer[2]=0;
   VubBuffer[3]=10;
   memset(&VubBuffer[4],0x5a,sizeof(VubBuffer)-4);
   ViReturnCode=dgram_send(VpDgram,&VubBuffer[0],10);
   if (ViReturnCode == -1)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> send failed: %d\n", VerrSv);
   }
   else
   { /*trace out message */
     PDD_TestIncHexDump("C> send",&VubBuffer[0],ViReturnCode);
   }
   memset(&VubBuffer[0],0,sizeof(VubBuffer));
   ViReturnCode=dgram_recv(VpDgram,&VubBuffer[0],5);
   /* check error */
   if (ViReturnCode < 0)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> recv failed: %d\n", VerrSv);
   }   
   else
   {
     PDD_TestIncHexDump("C> read",&VubBuffer[0],ViReturnCode);
   }
#endif

#ifdef PDD_INC_WRITE_INVALID_SIZE
    /*------------------------- test write invalid size ---------------------------*/
   /* send message*/   
   unsigned char    VubBuffer[255];
   /*------ SCC_PD_NET_C_COMPONENT_STATUS_MSGID */
   VubBuffer[0]=SCC_PD_NET_C_COMPONENT_STATUS_MSGID;
   VubBuffer[1]=SCC_PD_NET_APPLICATION_STATUS_ACTIVE;
   VubBuffer[2]=SCC_PD_NET_VERSION;  
   ViReturnCode=dgram_send(VpDgram,&VubBuffer,3);
   if (ViReturnCode == -1)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> send failed: %d\n", VerrSv);
   }
   else
   { /*trace out message */
     PDD_TestIncHexDump("C> send",&VubBuffer[0],3);
   }
   ViReturnCode=dgram_recv(VpDgram,&VubBuffer[0],3);
   /* check error */
   if (ViReturnCode < 0)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> recv failed: %d\n", VerrSv);
   }   
   else
   {
     PDD_TestIncHexDump("C> read",&VubBuffer[0],ViReturnCode);
   }
   /*------ SCC_PD_NET_C_WRITE_DATA_POOL */
   VubBuffer[0]=SCC_PD_NET_TP_MARKER+2;
   VubBuffer[1]=SCC_PD_NET_C_WRITE_DATA_POOL;
   VubBuffer[2]=0;
   VubBuffer[3]=6;
   memset(&VubBuffer[4],0x5a,sizeof(VubBuffer)-4);
   ViReturnCode=dgram_send(VpDgram,&VubBuffer[0],255);
   if (ViReturnCode == -1)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> send failed: %d\n", VerrSv);
   }
   else
   { /*trace out message */
     PDD_TestIncHexDump("C> send",&VubBuffer[0],ViReturnCode);
   }
   memset(&VubBuffer[0],0,sizeof(VubBuffer));
   ViReturnCode=dgram_recv(VpDgram,&VubBuffer[0],5);
   /* check error */
   if (ViReturnCode < 0)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> recv failed: %d\n", VerrSv);
   }   
   else
   {
     PDD_TestIncHexDump("C> read",&VubBuffer[0],ViReturnCode);
   }
#endif
#ifdef PDD_INC_WRITE_READ_6_BYTES
    /*------------------------- PDD_INC_WRITE_READ_6_BYTES ---------------------------*/
   /* send message*/   
   unsigned char    VubBuffer[255];
   /*------ SCC_PD_NET_C_COMPONENT_STATUS_MSGID */
   VubBuffer[0]=SCC_PD_NET_C_COMPONENT_STATUS_MSGID;
   VubBuffer[1]=SCC_PD_NET_APPLICATION_STATUS_ACTIVE;
   VubBuffer[2]=SCC_PD_NET_VERSION;  
   ViReturnCode=dgram_send(VpDgram,&VubBuffer,3);
   if (ViReturnCode == -1)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> send failed: %d\n", VerrSv);
   }
   else
   { /*trace out message */
     PDD_TestIncHexDump("C> send",&VubBuffer[0],3);
   }
   ViReturnCode=dgram_recv(VpDgram,&VubBuffer[0],3);
   /* check error */
   if (ViReturnCode < 0)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> recv failed: %d\n", VerrSv);
   }   
   else
   {
     PDD_TestIncHexDump("C> read",&VubBuffer[0],ViReturnCode);
   }
   /*------ SCC_PD_NET_C_READ_DATA_POOL */
   VubBuffer[0]=SCC_PD_NET_TP_MARKER+2;
   VubBuffer[1]=SCC_PD_NET_C_WRITE_DATA_POOL;
   VubBuffer[2]=PDD_INC_TESTPOOL_1;
   VubBuffer[3]=0;
   VubBuffer[4]=6;
   VubBuffer[5]=0x12;
   VubBuffer[6]=0x34;
   VubBuffer[7]=0x56;
   VubBuffer[8]=0x78;
   VubBuffer[9]=0x9a;
   VubBuffer[10]=0xbc;
   ViReturnCode=dgram_send(VpDgram,&VubBuffer[0],11);
   if (ViReturnCode == -1)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> send failed: %d\n", VerrSv);
   }
   else
   { /*trace out message */
     PDD_TestIncHexDump("C> send",&VubBuffer[0],ViReturnCode);
   }
   memset(&VubBuffer[0],0,sizeof(VubBuffer));
   ViReturnCode=dgram_recv(VpDgram,&VubBuffer[0],2);
   /* check error */
   if (ViReturnCode < 0)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> recv failed: %d\n", VerrSv);
   }   
   else
   {
     PDD_TestIncHexDump("C> read",&VubBuffer[0],ViReturnCode);
   }
   /*---- SCC_PD_NET_C_READ_DATA_POOL ----*/
   VubBuffer[0]=SCC_PD_NET_C_READ_DATA_POOL;
   VubBuffer[1]=PDD_INC_TESTPOOL_1;
   ViReturnCode=dgram_send(VpDgram,&VubBuffer[0],2);
   if (ViReturnCode == -1)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> send failed: %d\n", VerrSv);
   }
   else
   { /*trace out message */
     PDD_TestIncHexDump("C> send",&VubBuffer[0],ViReturnCode);
   }
   memset(&VubBuffer[0],0,sizeof(VubBuffer));
   ViReturnCode=dgram_recv(VpDgram,&VubBuffer[0],10);
   /* check error */
   if (ViReturnCode < 0)
   {
     int VerrSv = errno;
     fprintf(stderr, "C> recv failed: %d\n", VerrSv);
   }   
   else
   {
     PDD_TestIncHexDump("C> read",&VubBuffer[0],ViReturnCode);
   }
#endif
   /*------------------- Deinitialize datagram service ------------------------*/
   ViReturnCode=dgram_exit(VpDgram);
   if (ViReturnCode < 0)
   {
      int VerrSv = errno;
      fprintf(stderr, "C> dgram_exit failed: %d\n", VerrSv);
      return 1;
   }
   /*Close socket */
   close(ViSocket);
   fprintf(stderr, "C> test end \n");
   return 0;
}