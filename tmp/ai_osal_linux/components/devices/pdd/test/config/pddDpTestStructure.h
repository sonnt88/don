#ifndef PDD_DP_TEST_STRUCTURE
#define PDD_DP_TEST_STRUCTURE

#define MAX_NAME1_LEN 20
#define MAX_NAME2_LEN 40

#ifdef PDD_TEST_FILE
typedef struct tsStructureString
{
	tChar name1[MAX_NAME1_LEN];
	tChar name2[MAX_NAME2_LEN];
}tsStructureString;
#endif

#endif