
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include "system_types.h"
#include "system_definition.h"
#include "pdd.h"
#include "pdd_trace.h"

/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
#define PDD_COMMAND    3
#define PDD_ARGUMENT   4

#define PDD_COMMAND_MAX_LEN   200
/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/
/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/                            

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
void vPDDTestPrintHelp(void);
void vPDDTestPrintHelpTestLevel(void);
void vPDDTestPrintHelpTestStreamOutput(void);
/******************************************************************************
* FUNCTION: int main(int argc, char *argv[])
*
* DESCRIPTION: start function of the proccess
*
* PARAMETERS:
*      argc: number of strings
*      argv: string table
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 05 06
*****************************************************************************/
int main(int argc, char *argv[])
{
  const char* VcOptString = "abcijk:lm:no:pr:s:q:";
  int         ViOpt;
	tBool       VbDone=FALSE;
	char*       VcpFilename = NULL;
  /* possible TTFIS Command mapped to options
  #define	PDD_TEST_CMD_NOR_USER_ERASE                           0x01  -a
  #define PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA               0x02  -b
  #define PDD_TEST_CMD_NOR_USER_GET_ERASE_COUNTER               0x03  -c
  #define PDD_TEST_CMD_NOR_KERNEL_ERASE                         0x04  -d
  #define PDD_TEST_CMD_NOR_KERNEL_ERASE_SECTOR                  0x05  -e
  #define PDD_TEST_CMD_NOR_KERNEL_PRINT_ACTUAL_DATA             0x06  -f
  #define PDD_TEST_CMD_NOR_KERNEL_GET_ERASE_COUNTER             0x07  -g
  #define PDD_TEST_CMD_SCC_SET_ALL_POOLS_TO_INVALID             0x08  -h
  #define PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_FILE               0x09  -i
  #define PDD_TEST_CMD_NOR_USER_SAVE_ACTUAL_CLUSTER             0x0a  -j
  #define PDD_TEST_CMD_NOR_USER_LOAD_ACTUAL_CLUSTER             0x0b  -k
  #define PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER              0x0c  -l
  #define PDD_TEST_CMD_SET_TRACE_LEVEL                          0x0d  -m
  #define PDD_TEST_CMD_CLEAR_TRACE_LEVEL                        0x0e  -n
  #define PDD_TEST_CMD_SET_TRACE_STREAM_OUTPUT                  0x0f  -o
  #define PDD_TEST_CMD_CLEAR_TRACE_STREAM_OUTPUT                0x10  -p
  #define PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA_STREAM        0x11  -r
  #define PDD_TEST_CMD_NOR_USER_SAVE_BACKUP_FILE_TO_NOR         0x12  -s
  #define PDD_TEST_CMD_DELETE_DATASTREAM                        0x13  -q

  */
  fprintf(stderr, "\n");
  fprintf(stderr, "================================================================================\n");
  fprintf(stderr, " PDD_TEST: test helper functions for pdd, v2.0.0, 13.03.2015\n");
  fprintf(stderr, "================================================================================\n");
  fprintf(stderr, "\n");

  while ((ViOpt = getopt(argc, argv, VcOptString)) != -1) 
  {
    tU8  Vu8Buffer[PDD_COMMAND_MAX_LEN];
    memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));
    switch (ViOpt) 
    {
      case 'a':
	      fprintf(stderr, "-a: erase partition nor user\n");
        fprintf(stderr, "================================================================================\n");
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_ERASE;
        pdd_vTraceCommand(&Vu8Buffer[0]);		
	      VbDone=TRUE;
      break;
	    case 'b':
	      fprintf(stderr, "-b: print actual data of partition nor user\n");
        fprintf(stderr, "================================================================================\n");
	      /*first read data; for read configuration*/
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA;
        pdd_vTraceCommand(&Vu8Buffer[0]);		
	      VbDone=TRUE;
      break;
	    case 'r':
	    {		  
	      VcpFilename = strdup(optarg);
	      fprintf(stderr, "-r: print  datastream %s of partition nor user\n",VcpFilename);			 
	      fprintf(stderr, "================================================================================\n");
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA_STREAM;	 
	      memcpy(&Vu8Buffer[PDD_ARGUMENT],VcpFilename,strlen(VcpFilename));	
        pdd_vTraceCommand(&Vu8Buffer[0]);   
	      VbDone=TRUE;
	      free(VcpFilename);
	    }break;
      case 'q':
	    {		  
        char *Vsbuf = strdup(optarg);
	      VcpFilename = strtok(Vsbuf,":");
	      if(VcpFilename!=NULL)
	      {
          char* VsLocation=strtok(NULL,"");
		      if (VsLocation!=NULL)
		      {
            tePddLocation  VeLocation = (tePddLocation)atol(VsLocation);
            VbDone=TRUE;
            switch(VeLocation)
            {
              case PDD_LOCATION_FS:
                fprintf(stderr, "-q: delete datastream '%s' from location PDD_LOCATION_FS\n",VcpFilename);			 
              break;
              case PDD_LOCATION_FS_SECURE:
                fprintf(stderr, "-q: delete datastream '%s' from location PDD_LOCATION_FS_SECURE\n",VcpFilename);			 
              break;
              case PDD_LOCATION_NOR_USER:
                fprintf(stderr, "-q: delete datastream '%s' from location PDD_LOCATION_NOR_USER\n",VcpFilename);			 
              break;
              case PDD_LOCATION_SCC:
                 fprintf(stderr, "-q: delete datastream '%s' from location PDD_LOCATION_SCC\n",VcpFilename);			 
              break;
              default:
                fprintf(stderr, "-q: invalid parameter for location '%s' %d \n",VsLocation,VeLocation);		  
                VbDone=FALSE;
              break;
            }
	          fprintf(stderr, "================================================================================\n");
            if(VbDone==TRUE)
            { //first get the size of datastream to check if the stream available
              tS32 Vs32Size=pdd_get_data_stream_size(VcpFilename,VeLocation);
              if(Vs32Size<=0)
              {               
                fprintf(stderr, "datastream '%s' not found on location '%d'\n",VcpFilename,VeLocation);			 
              }              
              else
              {              
                Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_DELETE_DATASTREAM;
		            memcpy(&Vu8Buffer[PDD_ARGUMENT],&VeLocation,sizeof(tePddLocation));
	              memcpy(&Vu8Buffer[PDD_ARGUMENT+sizeof(tePddLocation)],VcpFilename,strlen(VcpFilename));	
                pdd_vTraceCommand(&Vu8Buffer[0]);                     
              }
            }
          }
        }
        free(Vsbuf);
      }break;
	    case 's':
	    {		  
	      char *Vsbuf = strdup(optarg);
	      char *VsVersion = strtok(Vsbuf,":");
	      if(VsVersion!=NULL)
	      {
	        tU32  Vu32Version = strtol(VsVersion,NULL,16);
          VcpFilename=strtok(NULL,"");
		      if (VcpFilename!=NULL)
		      {
		        fprintf(stderr, "-s: load backupfile %s with version 0x%04x to partition nor user\n",VcpFilename,Vu32Version);
	          fprintf(stderr, "================================================================================\n");
            Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_BACKUP_FILE_TO_NOR;
		        memcpy(&Vu8Buffer[PDD_ARGUMENT],&Vu32Version,sizeof(tU32));
	          memcpy(&Vu8Buffer[PDD_ARGUMENT+sizeof(tU32)],VcpFilename,strlen(VcpFilename));	
            pdd_vTraceCommand(&Vu8Buffer[0]);   
	          VbDone=TRUE;
          }
        }
	      free(Vsbuf);
      }break;
      case 'j':
        fprintf(stderr, "-j: dump actual cluster of partition nor user to file\n");
        fprintf(stderr, "================================================================================\n");
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_ACTUAL_CLUSTER;
        pdd_vTraceCommand(&Vu8Buffer[0]);   
        VbDone=TRUE;
        break;
      case 'i':
        fprintf(stderr, "-i: dump a complete partition nor user to file\n");
        fprintf(stderr, "================================================================================\n");
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_FILE;
        pdd_vTraceCommand(&Vu8Buffer[0]);   
        VbDone=TRUE;
      break;
      case 'k':
        VcpFilename=strdup(optarg);
        fprintf(stderr, "-k: load a dump file %s for the complete partition nor user\n",VcpFilename);
        fprintf(stderr, "================================================================================\n");
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_NOR;
        memcpy(&Vu8Buffer[PDD_ARGUMENT],VcpFilename,strlen(VcpFilename));
        pdd_vTraceCommand(&Vu8Buffer[0]);   
        VbDone=TRUE;
        free(VcpFilename);
      break;
      case 'c':
        fprintf(stderr, "-c: get erase counter for partition nor user\n");
        fprintf(stderr, "================================================================================\n");
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ERASE_COUNTER;
        pdd_vTraceCommand(&Vu8Buffer[0]);   
        VbDone=TRUE;
      break;	
        case 'l':
        fprintf(stderr, "-l: get actual cluster of partition nor user to file\n");
        fprintf(stderr, "================================================================================\n");
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER;
        pdd_vTraceCommand(&Vu8Buffer[0]);
        if(Vu8Buffer[PDD_ARGUMENT]!=0xff)
          fprintf(stderr, "actual cluster: %d \n",Vu8Buffer[PDD_ARGUMENT]);
        VbDone=TRUE;
      break;
      case 'm':
      {
        tU32 Vu32Level=PDD_TRACE_DEFAULT;
        fprintf(stderr, "-m: set trace level\n");
        fprintf(stderr, "================================================================================\n");         
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_SET_TRACE_LEVEL;
        VcpFilename=strdup(optarg);
        switch(*VcpFilename)
        {
          case 'a':
	        {
	          Vu32Level|=PDD_TRACE_INTERFACE_CALL;
	        }break;
	        case 'b':
	        {
	          Vu32Level|=PDD_TRACE_ADMIN;
	        }break;
	        case 'c':
	        {
	          Vu32Level|=PDD_TRACE_PDD;
	        }break;
          case 'd':
	        {
	          Vu32Level|=PDD_TRACE_FILE;
	        }break;
	        case 'e':
	        {
	          Vu32Level|=PDD_TRACE_SCC;
	        }break;
	        case 'f':
	        {
	          Vu32Level|=PDD_TRACE_NOR_USER;
	        }break;
	        case 'g':
	        {
	          Vu32Level|=PDD_TRACE_VALIDATION;
	        }break;
	        case 'h':
	        {
	          Vu32Level|=PDD_TRACE_NOR_KERNEL;
	        }break;
	        default:
	        {
	          fprintf(stderr, "unknown level");
	        }break;
        }/*end switch*/
        if(Vu32Level!=PDD_TRACE_DEFAULT)
        {
          memcpy(&Vu8Buffer[PDD_ARGUMENT],&Vu32Level,sizeof(tU32));
          pdd_vTraceCommand(&Vu8Buffer[0]);
	        fprintf(stderr, "set trace level done\n");
          VbDone=TRUE;
        }
        free(VcpFilename);
      }break;
      case 'n':
        fprintf(stderr, "-n: clear trace level\n");
        fprintf(stderr, "================================================================================\n");
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_CLEAR_TRACE_LEVEL;
        pdd_vTraceCommand(&Vu8Buffer[0]);         
        VbDone=TRUE;
        break;
      case 'o':
      {
        tU8 Vu8StreamOutput=0xff;
        fprintf(stderr, "-o: set trace stream output\n");
        fprintf(stderr, "================================================================================\n");         
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_SET_TRACE_STREAM_OUTPUT;
        VcpFilename=strdup(optarg);
        switch(*VcpFilename)
        {
          case 'a':
	        {
	          Vu8StreamOutput=STDOUT_FILENO;
	        }break;
	        case 'b':
	        {
	          Vu8StreamOutput=STDERR_FILENO;
	        }break;	   
	        default:
	        {
	          fprintf(stderr, "unknown stream output");
	        }break;
        }/*end switch*/
        if(Vu8StreamOutput!=0xff)
        {
          Vu8Buffer[PDD_ARGUMENT]=Vu8StreamOutput;
          pdd_vTraceCommand(&Vu8Buffer[0]);
          fprintf(stderr, "set trace stream output done\n");
          VbDone=TRUE;
        }
        free(VcpFilename);
      }break;
      case 'p':
        fprintf(stderr, "-p: clear trace stream output\n");
        fprintf(stderr, "================================================================================\n");
        Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_CLEAR_TRACE_STREAM_OUTPUT;
        pdd_vTraceCommand(&Vu8Buffer[0]);         
        VbDone=TRUE;
      break;
      default: 		
        fprintf(stderr, "unknown command line option: \"-%c\"\n", optopt);
        vPDDTestPrintHelp();
      break;
    }/*end switch*/
  }/*end while*/
  if(VbDone==FALSE)
  {
    vPDDTestPrintHelp();
	}
  fprintf(stderr, "================================================================================\n");	
  _exit(0);
}
/******************************************************************************
* FUNCTION: void vPDDTestPrintHelp
*
* DESCRIPTION: printout options 
*
* PARAMETERS:
*      argc: number of strings
*      argv: string table
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
void vPDDTestPrintHelp()
{
	fprintf(stderr, "-a: erase partition nor user\n");
	fprintf(stderr, "-b: print actual data of partition nor user\n");
	fprintf(stderr, "-r: [datastream] print datastream of partition nor user\n");	
  fprintf(stderr, "-q: [datastream]:[location] delete datastream\n");	
  fprintf(stderr, "       [location '0': PDD_LOCATION_FS \n");
	fprintf(stderr, "       [location '1': PDD_LOCATION_FS_SECURE \n");
  fprintf(stderr, "       [location '2': PDD_LOCATION_NOR_USER \n");
	fprintf(stderr, "       [location '4': PDD_LOCATION_SCC \n");
	fprintf(stderr, "-s: [version]:[backupfile] load backupfile to partition nor user\n");
	fprintf(stderr, "-j: dump actual cluster of partition nor user to file\n");
	fprintf(stderr, "-i: dump a complete partition nor user to file\n");
	fprintf(stderr, "-k: [filename]: load a dump file for the complete partition nor user\n");
  fprintf(stderr, "-c: get erase counter for partition nor user\n");
	fprintf(stderr, "-l: get actual cluster of partition nor user to file\n");
	fprintf(stderr, "-m: [level] set trace level\n");
  vPDDTestPrintHelpTestLevel();
	fprintf(stderr, "-n: clear trace level\n");
	fprintf(stderr, "-o: [output] set trace stream output\n");
  vPDDTestPrintHelpTestStreamOutput();
	fprintf(stderr, "-p: clear trace stream output\n");
	
	fprintf(stderr, "================================================================================\n");
  _exit(0);
}
/******************************************************************************
* FUNCTION: void vPDDTestPrintHelpTestLevel(void)
*
* DESCRIPTION: printout options 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
void vPDDTestPrintHelpTestLevel(void)
{
	fprintf(stderr, "       [level 'a': PDD_TRACE_INTERFACE_CALL \n");
	fprintf(stderr, "       [level 'b': PDD_TRACE_ADMIN \n");
  fprintf(stderr, "       [level 'c': PDD_TRACE_PDD \n");
	fprintf(stderr, "       [level 'd': PDD_TRACE_FILE \n");
	fprintf(stderr, "       [level 'e': PDD_TRACE_SCC \n");
	fprintf(stderr, "       [level 'f': PDD_TRACE_NOR_USER \n");
	fprintf(stderr, "       [level 'g': PDD_TRACE_VALIDATION \n");
	fprintf(stderr, "       [level 'h': PDD_TRACE_NOR_KERNEL \n");
}

/******************************************************************************
* FUNCTION: void vPDDTestPrintHelpTestLevel(void)
*
* DESCRIPTION: printout options 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2015 03 12
*****************************************************************************/
void vPDDTestPrintHelpTestStreamOutput(void)
{
	fprintf(stderr, "       [level 'a': STDOUT_FILENO(1) default \n");
	fprintf(stderr, "       [level 'b': STDERR_FILENO(2) \n");    
}