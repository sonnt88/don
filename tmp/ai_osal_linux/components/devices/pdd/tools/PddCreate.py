#/**
# * @copyright    (C) 2012 - 2016 Robert Bosch GmbH.
# *               The reproduction, distribution and utilization of this file as well as the
# *               communication of its contents to others without express authorization is prohibited.
# *               Offenders will be held liable for the payment of damages.
# *               All rights reserved in the event of the grant of a patent, utility model or design.
# * @brief        This PDD creator
# * @addtogroup   PDD creator
# * @{
# */


import sys, os, re, shutil, hashlib, PddTmpl, platform,  time

from xml.dom.minidom import parse

# ##########################
# global variables
# ##########################
_swNaviPath = ""
_swBuildPath = ""

strLogInfo = ""
fileList = []
DP_BUILDENV_VOB_HOPPING = 0
DP_BUILDENV_V1_SYMLINKED = 1
DP_BUILDENV_V2_SYMLINKED = 2

buildEnv = DP_BUILDENV_VOB_HOPPING
strHashFile = ""# -------------------------#
# PDD
# -------------------------#
strPddConfigHeaderFileAccessNorUser = ""
strPddConfigHeaderFileAccessSCC = ""
strPddConfigHeaderFileConfigKDS = ""
strPddConfigHeaderFileVariantGen = ""

PddNorSccMaxSizeName=32  
PddNorUserDefaultPoolVersion="0x00000001"    
PddNorUserDefaultPoolVersionKDS="0x00000206"   

# -------------------------#
# PDD function
# -------------------------#
def removeFolder(strFolder):
    os.chmod(strFolder, 0755)
    if os.path.isdir(strFolder):
        myDirList = os.listdir(strFolder)
        for i in myDirList:
            fileName = strFolder + '/' + i
            if os.path.isdir(fileName):
                removeFolder(fileName)
            else:
                os.chmod(fileName, 0755)
                os.remove(fileName)
        os.rmdir(strFolder)
    else:
        os.remove(strFolder)

def PddGetLocationKDS(strLocation,KdsPoolNorUser,KdsPoolFileSystem,KdsPoolFileSystemSecure):
    if strLocation == "RAW_NOR":
        KdsPoolNorUser=True         
    elif strLocation == "FILE_SYSTEM":
        KdsPoolFileSystem=True                                              
    elif strLocation == "FILE_SYSTEM_SECURE":
        KdsPoolFileSystemSecure=True           
    return(KdsPoolNorUser,KdsPoolFileSystem,KdsPoolFileSystemSecure)

def PddGetSetConfigKds(strPddConfigHeaderFileConfigKDS):
    myXMLConfigDoc = parse(strFileName)
    KdsPoolNorUser = False
    KdsPoolFileSystem = False
    KdsPoolFileSystemSecure = False
    strPddKdsLocation=""
    #check if KDS location is set 
    try: 
        strLocation=myXMLConfigDoc.getElementsByTagName('locationKDS')[0].childNodes[0].nodeValue       
        (KdsPoolNorUser,KdsPoolFileSystem,KdsPoolFileSystemSecure)=PddGetLocationKDS(strLocation,KdsPoolNorUser,KdsPoolFileSystem,KdsPoolFileSystemSecure)
    except:
        strLocation=""  
    #check KDS POOL                     
    if KdsPoolNorUser == True:                                   
        strPddKdsLocation="#define PDD_KDS_LOCATION                          PDD_LOCATION_NOR_USER\n"                     
    elif KdsPoolFileSystem == True:      
        strPddKdsLocation="#define PDD_KDS_LOCATION                          PDD_LOCATION_FS\n"                            
    elif KdsPoolFileSystemSecure == True:           
        strPddKdsLocation="#define PDD_KDS_LOCATION                          PDD_LOCATION_FS_SECURE\n"       
    else:
        #trace output and exit:
        pddFatalAssert("No valid location for KDS data defined; Please set <locationKDS>FILE_SYSTEM_SECURE</locationKDS> in global configuration")
    strPddConfigHeaderFileConfigKDS+=strPddKdsLocation    
    return(strPddConfigHeaderFileConfigKDS,KdsPoolNorUser)  


def PddGetSetConfigAccessNorUser(strPddConfigHeaderFileAccessNorUser,KdsPoolNorUser,strFileConfig1,strFileConfig2):
  numberPoolNorUserAccess = 0
  numberDataPoolPoolNorUser = 0
  strPddArrayName=PddTmpl.PddTmplConfigHeaderArrayAccessNorUser
    
  #add KDS POOL                     
  #if KdsPoolNorUser == True:  06.05.2014: "kds_data" always defined; KdsPoolNorUser==FALSE; data saved in FS, but KDS entries from testmanger don't clear 
  numberPoolNorUserAccess+=1                               
  strTemp=""" "kds_data" """     
  strPddConfigHeaderFileAccessNorUser+= "#define PDD_NOR_USER_DATASTREAM_NAME_KDS              %s\n"%strTemp  
  strPddArrayName+="  {PDD_NOR_USER_DATASTREAM_NAME_KDS, %s, PDD_LOCATION_NOR_USER}"%PddNorUserDefaultPoolVersionKDS
    
  #for both files
  file = 0
  maxfile = 2
  while file < maxfile:
    file=file+1
    if file == 1:
      strFileConfig=strFileConfig1
    else:
      strFileConfig=strFileConfig2  
    # check first file 
    if os.path.isfile(strFileConfig):
      myXMLConfigDoc = parse(strFileConfig)        
      # check configuration pool  
      if myXMLConfigDoc != None:
         for node in myXMLConfigDoc.getElementsByTagName('path'):       
           strLocation = node.getAttribute('location')
           strPoolFile = node.getAttribute('file')
           strPoolName = node.getAttribute('name')           
           #check odx file
           if not strPoolFile.find(".odx-e") != -1 and (strLocation == "RAW_NOR" or strLocation == "V850"): 
               numberDataPoolPoolNorUser =numberDataPoolPoolNorUser+1                                                                   
               if numberDataPoolPoolNorUser > 30:                
                 # !! -- attention --!! : if change this max number of pools, you must change the define PDD_NOR_USER_MAX_CONFIG_NUMBER_DATASTREAM in pdd.h too.
                 # TODO: read from pdd.h !!!
                 #trace output and exit: 
                 pddFatalAssert("Only 30 datapool can use NOR-USER access. More then 30 datapools are defined in the global XML file")    
               else:
                 strTemp=""" "%s" """%strPoolName 
                 LenTemp=len(strPoolName);
                 if LenTemp >= PddNorSccMaxSizeName:
                   strError="length of the pool name:%s to long; max size: %d bytes actual size: %d bytes"%(strTemp,PddNorSccMaxSizeName-1,LenTemp)
                   pddFatalAssert(strError)    
                 else:
                   # get version
                   try:                    
                     strPoolVersion = node.getAttribute('version')  
                     if strPoolVersion == "":
                       strPoolVersion = PddNorUserDefaultPoolVersion           
                   except:
                     strPoolVersion = PddNorUserDefaultPoolVersion
                   # get enum define location 
                   strEnumLocation="PDD_LOCATION_NOR_USER"
                   if strLocation == "V850":
                     strEnumLocation="PDD_LOCATION_SCC"                                    
                   strTempUpper=strPoolName.upper()    
                   strPddConfigHeaderFileAccessNorUser+= "#define PDD_NOR_USER_DATASTREAM_NAME_%s    "%strTempUpper        
                   strPddConfigHeaderFileAccessNorUser+= "%s\n"%strTemp              
                   if (numberPoolNorUserAccess+numberDataPoolPoolNorUser)>1:
                     strPddArrayName+=",\n"   
                   strPddArrayName+="  {PDD_NOR_USER_DATASTREAM_NAME_%s, %s, %s}"%(strTempUpper,strPoolVersion,strEnumLocation)
               
  # check configuration pool    
  if numberDataPoolPoolNorUser >= 1:   
    numberPoolNorUserAccess+=numberDataPoolPoolNorUser
  #write number
  strPddConfigHeaderFileAccessNorUser+= "#define PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM          %d \n"%numberPoolNorUserAccess
  #write array                     
  strPddArrayName+="\n#endif\n};\n"
  strPddConfigHeaderFileAccessNorUser+=strPddArrayName
  return(strPddConfigHeaderFileAccessNorUser)

def PddGetSetConfigAccessNorKernel(strPddConfigHeaderFileVariantGen,strFileConfig1,strFileConfig2):
  #determine number of pools for kernel; only 1 pool possible
  #if pool exist set define in strPddConfigHeaderFileVariantGen
  numberDataPoolNorKernel = 0
   #for both files
  file = 0
  maxfile = 2
  while file < maxfile:
    file=file+1
    if file == 1:
      strFileConfig=strFileConfig1
    else:
      strFileConfig=strFileConfig2
    #for file 1 and file2
    if os.path.isfile(strFileConfig):
      myXMLConfigDoc = parse(strFileConfig)
      if myXMLConfigDoc != None:
        for node in myXMLConfigDoc.getElementsByTagName('path'):
            strLocation = node.getAttribute('location')
            if strLocation == "PDD_KERNEL":
              numberDataPoolNorKernel+=1   
             
  if numberDataPoolNorKernel > 1:
    #trace output and exit: 
    pddFatalAssert("Only 1 datapool can use NOR-KERNEL access. More then one datapool are defined in the global XML file")    
  else:  
    if numberDataPoolNorKernel > 0:
      strPddConfigHeaderFileVariantGen+="#define PDD_NOR_KERNEL_POOL_EXIST\n\n"   

  return(strPddConfigHeaderFileVariantGen)

def PddGetSetConfigAccessV850(strPddConfigHeaderFileAccessSCC,strPddConfigHeaderFileVariantGen,strFileConfig1,strFileConfig2):
  numberPoolV850Access = 0
  strPddElemName=""
  strFileConfig=""
  
  strPddArrayName=PddTmpl.PddTmplConfigHeaderArrayAccessSCCNew
  strPddDefineNumber=PddTmpl.PddTmplConfigHeaderDefineNumberSCC
  #for both files
  file = 0
  maxfile = 2
  while file < maxfile:
    file=file+1
    if file == 1:
      strFileConfig=strFileConfig1
    else:
      strFileConfig=strFileConfig2
    #for file 1 and file2
    if os.path.isfile(strFileConfig):
      myXMLConfigDoc = parse(strFileConfig)
      if myXMLConfigDoc != None:
        for node in myXMLConfigDoc.getElementsByTagName('path'):
            strLocation = node.getAttribute('location')
            if strLocation == "V850":   
                #get pool name
                strPddPoolName = node.getAttribute('name')
                LenTemp=len(strPddPoolName);
                if LenTemp >= PddNorSccMaxSizeName:
                  strError="length of the pool name:%s to long; max size: %d bytes actual size: %d bytes"%(strTemp,PddNorSccMaxSizeName-1,LenTemp)
                  pddFatalAssert(strError)    
                else:                      
                  # default port name
                  strPortName="PD_NET_PORT"                
                  #default storing type                
                  strStoringStrategie="PDD_SCC_STORE_AT_SHUTDOWN"  
                  #default string size
                  strPddPoolSize="sizeof(TPddScc_Header)"
                  #increment number
                  numberPoolV850Access+=1          
                  #get file
                  strPoolFile = node.getAttribute('file')
                  #print strPoolFile
                  if strPoolFile.find(".xml") != -1:
                      if strPoolFile.find("generated") ==-1:    
                        if strPoolFile.find("%_SWNAVIROOT%") != -1:
                          #absolute path
                          strPoolfile = strPoolFile.replace("%_SWNAVIROOT%", _swNaviPath)
                        else:
                          #relative path
                          strPoolfile = strPathToConfig + "/"+ strPoolFile
                      else:
                          strPoolfile = strPoolFile                    
                      myXmlDoc = parse(strPoolfile)
                      if myXmlDoc != None:
                          for node in myXmlDoc.getElementsByTagName('dpelement'):  # visit every node <bar />
                            #get config node
                            configNode = node.getElementsByTagName('config')
                            if None != configNode:
                              #parse all common parameter
                               strPddElemName = node.getAttribute('name')
                               strPddPoolSize+="+sizeof(TDataPoolScc_%s)"%(strPddElemName)  
                               ElemType = configNode[0].getElementsByTagName('ElementType')[0].childNodes[0].nodeValue
                               if ElemType=="PERSISTENT":                                                                    
                                 # get storing strategy                  
                                 try:
                                   elemPerselemPersStoreTypeCpp = configNode[0].getElementsByTagName('StoringType')[0].childNodes[0].nodeValue
                                 except:
                                   elemPerselemPersStoreTypeCpp ="Shutdown"    
                                 if elemPerselemPersStoreTypeCpp == "Force":
                                   strStoringStrategie="PDD_SCC_STORE_DIRECT"                             
                                 try:
                                   strPortName = configNode[0].getElementsByTagName('INC_PORT')[0].childNodes[0].nodeValue
                                 except:
                                   pass                                                    
                  #add string to table
                  if numberPoolV850Access > 1:
                    strPddArrayName+=",\n"
                  strTemp=""" "%s" """%strPddPoolName                 
                  strPddArrayName+="  {%s, %s, %s,DATAPOOL_SCC_IDENTITY_NUMBER_%s,%s,DATAPOOL_SCC_VERSION_POOL_%s}"%(strTemp,strPortName,strStoringStrategie,strPddPoolName.upper(),strPddPoolSize,strPddPoolName.upper())
         

  #add string number of access
  strPddDefineNumber+="%d"%numberPoolV850Access
  strPddConfigHeaderFileAccessSCC+=strPddDefineNumber
  strPddConfigHeaderFileAccessSCC+="\n"
  #add table
  strPddConfigHeaderFileAccessSCC+=strPddArrayName
  strPddConfigHeaderFileAccessSCC+="\n};\n "
  if numberPoolV850Access > 0:
    strPddConfigHeaderFileVariantGen+="#define PDD_SCC_POOL_EXIST\n\n"
  return(strPddConfigHeaderFileAccessSCC,strPddConfigHeaderFileVariantGen)

def PddCreateConfigHeaderFile():
  KdsPoolNorUser = False
  #write string begin
  strPddConfigHeaderFileConfigKDS=PddTmpl.PddTmplConfigHeaderBeginConfigKDS
  strPddConfigHeaderFileAccessNorUser=PddTmpl.PddTmplConfigHeaderBeginAccessNorUser
  strPddConfigHeaderFileAccessSCC=PddTmpl.PddTmplConfigHeaderBeginAccessSCC
  strPddConfigHeaderFileVariantGen=PddTmpl.PddTmplConfigHeaderBegin
  
  #get configuration
  (strPddConfigHeaderFileConfigKDS,KdsPoolNorUser)=PddGetSetConfigKds(strPddConfigHeaderFileConfigKDS)
  strPddConfigHeaderFileAccessNorUser=PddGetSetConfigAccessNorUser(strPddConfigHeaderFileAccessNorUser,KdsPoolNorUser,strFileName,strFileNameGenerate)
  (strPddConfigHeaderFileVariantGen)=PddGetSetConfigAccessNorKernel(strPddConfigHeaderFileVariantGen,strFileName,strFileNameGenerate)
  (strPddConfigHeaderFileAccessSCC,strPddConfigHeaderFileVariantGen)=PddGetSetConfigAccessV850(strPddConfigHeaderFileAccessSCC,strPddConfigHeaderFileVariantGen,strFileName,strFileNameGenerate)
                 
  #write string end
  strPddConfigHeaderFileAccessNorUser+=PddTmpl.PddTmplConfigHeaderEndAccessNorUser
  strPddConfigHeaderFileAccessSCC+=PddTmpl.PddTmplConfigHeaderEndAccessSCC
  strPddConfigHeaderFileConfigKDS+=PddTmpl.PddTmplConfigHeaderEndConfigKDS
  strPddConfigHeaderFileVariantGen+=PddTmpl.PddTmplConfigHeaderEnd

  #check path
  swGenPDDPath =   _swBuildPath + "include/"
  if not os.path.exists(swGenPDDPath):
    os.makedirs(swGenPDDPath)

  #write sting to file
  writeFileIfChanged(swGenPDDPath+ "pdd_config_nor_user.h", strPddConfigHeaderFileAccessNorUser)
  writeFileIfChanged(swGenPDDPath+ "pdd_config_scc.h", strPddConfigHeaderFileAccessSCC)
  writeFileIfChanged(swGenPDDPath+ "pdd_config_kds.h", strPddConfigHeaderFileConfigKDS)
  writeFileIfChanged(swGenPDDPath+ "pdd_variant_gen.h", strPddConfigHeaderFileVariantGen)


def pddFatalAssert(strTrace):
    global strLogInfo
    strLogInfo += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
    strLogInfo += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
    strLogInfo += "!!!!!!!!!!                                                                    !!!!!!!!!!!!\n"
    strLogInfo += "!!!!!!!!!! PDD ERROR                                                          !!!!!!!!!!!!\n"
    strLogInfo += "!!!!!!!!!!                                                                    !!!!!!!!!!!!\n"
    strLogInfo += "%s\n"%strTrace
    strLogInfo += "!!!!!!!!!!                                                                    !!!!!!!!!!!!\n"
    strLogInfo += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
    strLogInfo += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    print "!!!!!!!!!!                                                                    !!!!!!!!!!!!"
    print "!!!!!!!!!! PDD ERROR                                                          !!!!!!!!!!!!"
    print "!!!!!!!!!!                                                                    !!!!!!!!!!!!"
    print "%s"%strTrace
    print "!!!!!!!!!!                                                                    !!!!!!!!!!!!"
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    if strLogFile != "":
        logFile = open(strLogFile,'w')
        logFile.write(strLogInfo)
        logFile.close()
        #print strLogInfo
    else:
        print strLogInfo
    input("press enter to continue ")
    sys.exit()

def writeFileIfChanged(fileName, strContent):
    strOld=""
    try:
        outSrcFile = open(fileName,'r')
        strOld = outSrcFile.read()
        outSrcFile.close()
    except:
        strOld=""
    if strOld != strContent:
        outSrcFile = open(fileName,'w')
        outSrcFile.write(strContent)
        outSrcFile.close()

def copyFileIfChanged(dstFile, srcFile):
    strToCopy = ""
    try:
        outSrcFile = open(srcFile,'r')
        strToCopy = outSrcFile.read()
        outSrcFile.close()
    except:
        print "no file found to open %s."%(srcFile)
    writeFileIfChanged(dstFile, strToCopy)

def copyTreeIfChanged(rootDirDst, rootDirSrc):
    #print "Copy: " + rootDirSrc + " --> " + rootDirDst
    tFilter = (".h", ".cpp", ".xml",".trc", ".c")
    if os.path.isdir(rootDirSrc):
        if not os.path.exists(rootDirDst):
            #print "Create: " + rootDirDst
            os.makedirs(rootDirDst)
        myDirList = os.listdir(rootDirSrc)
        for i in myDirList:
            #print "Copy file : " + rootDirDst
            fileNameSrc = rootDirSrc + '/' + i
            fileNameDst = rootDirDst + '/' + i
            if os.path.isdir(fileNameSrc):
                copyTreeIfChanged(fileNameDst, fileNameSrc)
            else:
                for i in tFilter:
                    (filename, ext) = os.path.splitext(os.path.basename(fileNameSrc))
                    if ext == i:
                        #print "Copy file : " + fileNameSrc + "-->" + fileNameDst
                        copyFileIfChanged(fileNameDst, fileNameSrc)
                        break
    else:
        copyFileIfChanged(rootDirDst, rootDirSrc)


def CreateListOfDpFiles(strBaseDir, strCfgFile=None):
    global fileList
    #get all files in datapool folder
    myDirList = os.listdir(strBaseDir)
    for i in myDirList:
        fileName = strBaseDir + '/' + i
        if os.path.isdir(fileName):
            CreateListOfDpFiles(fileName)
        else:
            fileList.append(fileName)
    if strCfgFile != None:
        #add configuration file
        fileList.append(strCfgFile)

        #add all referenced files in configuration
        myConfigXmlDoc = parse(strCfgFile)
        if myConfigXmlDoc != None:
            for node in myConfigXmlDoc.getElementsByTagName('path'):
                #new pool found -> extract needed information
                strPoolFile = node.getAttribute('file')
                if strPoolFile.find("%_SWNAVIROOT%") != -1:
                    #absolute path
                    strFile = strPoolFile.replace("%_SWNAVIROOT%", _swNaviPath)
                else:
                    #relative path
                    strFile = os.path.dirname(strFileName)+"/"+strPoolFile
                #print strFile
                fileList.append(strFile)
                    

def GetHashofDir(strFileList):
    global strHashFile
    SHAhash = hashlib.sha1()
    for fileName in strFileList:
        f1 = open(fileName, 'rb')
        buf = f1.read()
        SHAhash.update(hashlib.sha1(buf).hexdigest())
        f1.close()
        if fileName.find('hash') == -1:
            strHashFile += "%s -> %s\n"%(SHAhash.hexdigest(), fileName)


# ########################################################################################################
# MAIN
# ########################################################################################################


# ##########################
# check input
# ##########################

# ##### get enviromaent ###################
try:
    _swNaviPath = os.environ["_SWNAVIROOT"]
except:
    strLogInfo += "_SWNAVIROOT not found, set default\n"
    _swNaviPath = "x:/ai_projects"

try:
    _swBuildPath = os.environ["_SWBUILDROOT"]
except:
    strLogInfo += "_SWBUILDROOT not found, set default\n"
    _swBuildPath = "y:/ai_projects"

_swGenDPConfigPath = _swBuildPath + "/generated/components/datapool/config/"
_swBuildPath += '/generated/components/pdd/'

strLogInfo += "Folder to gernerate to is %s\n" %_swBuildPath
strLogFile = ""
strBuildMode = "build"

# variable "buildEnv" is not used at all
#check view enviroment --> buildversion 1/2 symlinked or vob hopping
strPathToPDD = _swNaviPath + "/../ai_osal_linux/components/devices/pdd"
if os.path.exists(strPathToPDD) and os.path.exists(_swNaviPath + "/../di_frameworks/components"):
    # vob hopping is independend from buildversion
    buildEnv = DP_BUILDENV_VOB_HOPPING
else:
    strPathToPDD = _swNaviPath + "/components/devices/pdd"
    if os.path.exists(strPathToPDD):
        # V1 symlinked-> like CF TENGINE build
        buildEnv = DP_BUILDENV_V1_SYMLINKED
    else:
        strPathToPDD = _swNaviPath + "/linked/ai_osal_linux/devices/pdd"
        if os.path.exists(strPathToPDD):
            # V2 symlinked (extra dir "linked", no "components" dir) -> like GM
            buildEnv = DP_BUILDENV_V2_SYMLINKED

if len(sys.argv)>1:
    strFileName = sys.argv[1]
    strPathToConfig = os.path.dirname(strFileName)
if len(sys.argv)>2:
    strBuildMode = sys.argv[2]
if len(sys.argv)>3:
    strLogFile = sys.argv[3]

strFileNameGenerate=_swGenDPConfigPath+"prj_diagkds_config.xml"

swGenPDDInclude =   _swBuildPath + "include/"
swGenPDDSource  =   _swBuildPath + "source/"

#check hash
strOldHash = ""
hashName = _swBuildPath + 'genhash%s.txt'%platform.system()

if strBuildMode.lower() == 'rebuild' or strBuildMode.lower() == 'recreate' or not os.path.exists(swGenPDDInclude) or not os.path.exists(swGenPDDSource):
    strLogInfo += "Rebuild/create detected -> remove geenrated folder and invalidate hash.\n"
    if os.path.exists(_swBuildPath):
        try:
            removeFolder(_swBuildPath)
        except:
            strLogInfo += "Cannot remove generated folder.\n"

    #if not os.path.exists(_swBuildPath):
    #    os.makedirs(_swBuildPath)

try:
    oldHashFile = open(hashName, 'r')
    strOldHash = oldHashFile.read()
    oldHashFile.close()

except:
    strLogInfo += "hash log not found -> genrate first time.\n"

time.sleep(0.1)
i = 0
while i<500:
    try:
        if not os.path.exists(_swBuildPath):
            os.makedirs(_swBuildPath)
        else:
            break
    except:
        i += 1
        time.sleep(0.01)

strLogInfo += "Folder created after %fs"%(i*0.01)

if not os.path.exists(swGenPDDInclude):
    os.makedirs(swGenPDDInclude)
if not os.path.exists(swGenPDDSource):
    os.makedirs(swGenPDDSource)

#create file list of all files needed by datapool and included in HASH
CreateListOfDpFiles(strPathToPDD, strFileName)

GetHashofDir(fileList)
if strHashFile == strOldHash:
    strLogInfo += "No changes detected -> nothing to do.\n"
else:
    #something has changed -> generate new pool
    oldHashFile = open(hashName, 'w')
    oldHashFile.write(strHashFile)
    oldHashFile.close()

    if strOldHash != "":
        verifyHashFile = open(hashName + '.verify', 'w')
        verifyHashFile.write(strOldHash)
        verifyHashFile.close()

    strLogInfo +=  "Base path for config: %s\n" %strPathToConfig
    strLogInfo +=  "Load datapool config file: %s\n" %strFileName

    ##########################################################
    # copy files from clearcase folder to generated folder
    ##########################################################
    # copy xml
    try:
       copyFileIfChanged(_swBuildPath+'/pdd.trc', strPathToPDD+'/pdd.trc')
       copyFileIfChanged(_swBuildPath+'/pdd.xml', strPathToPDD+'/pdd.xml')
    except:
        strLogInfo += "Failed to copy files to generated folder\n"
    #copy the complete 'pdd/include' folder
    try:
        #print "try to copy: " + strPathToPDD + '/access'
        copyTreeIfChanged(_swBuildPath+'include', strPathToPDD+'/include')
    except:
        strLogInfo += "Failed to copy tree to generated folder\n"

    #copy the complete 'pdd/source' folder
    try:
        #print "try to copy: " + strPathToPDD + '/access'
        copyTreeIfChanged(_swBuildPath+'source', strPathToPDD+'/source')
    except:
        strLogInfo += "Failed to copy tree to generated folder\n"

    #generate PDD configuration file
    PddCreateConfigHeaderFile()
    strLogInfo +="Create configuration file pdd_config_nor_user.h \n"
    strLogInfo +="Create configuration file pdd_config_scc.h \n"
    strLogInfo +="Create configuration file pdd_config_kds.h \n"
    strLogInfo +="Create configuration file pdd_variant_gen.h \n"

if strLogFile != "":
    logFile = open(strLogFile,'w')
    logFile.write(strLogInfo)
    logFile.close()
    #print strLogInfo
else:
    print strLogInfo
