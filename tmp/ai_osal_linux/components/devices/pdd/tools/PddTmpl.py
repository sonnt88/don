# * @copyright    (C) 2012 - 2016 Robert Bosch GmbH.
# *               The reproduction, distribution and utilization of this file as well as the
# *               communication of its contents to others without express authorization is prohibited.
# *               Offenders will be held liable for the payment of damages.
# *               All rights reserved in the event of the grant of a patent, utility model or design.
# * @brief        This files includes the template for PDD creator
# * @addtogroup   PDD creator
# * @{
# */
#############################################
# begin of header configuration file nor user
PddTmplConfigHeaderBeginAccessNorUser = """/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        generated header for the configuration on the nor user access.
 * @addtogroup   PDD generated header file
 * @{
 */
#ifndef PDD_CONFIG_NOR_USER_H
#define PDD_CONFIG_NOR_USER_H

#ifdef __cplusplus
extern "C" {
#endif
/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
#ifdef PDD_UNIT_TEST
#define PDD_NOR_USER_DATASTREAM_NAME_KDS               "kds_data" 
#define PDD_NOR_USER_DATASTREAM_NAME_CONFIG            "config_data" 
#define PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM          2 
#else
"""
#############################################
# array for names
PddTmplConfigHeaderArrayAccessNorUser ="""#endif
/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
/******** for NOR User access *********/
typedef struct
{
  tU8               u8DataStreamName[PDD_NOR_MAX_SIZE_NAME];    
  tU32              u32Version;
  tePddLocation     eLocation;
}tsPddNorUserConfig;
/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/
static  const tsPddNorUserConfig    vrPddNorUserConfig[PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM] =
{   /* name              */
  #ifdef PDD_UNIT_TEST
  {PDD_NOR_USER_DATASTREAM_NAME_KDS,0x00000206,PDD_LOCATION_NOR_USER},
  {PDD_NOR_USER_DATASTREAM_NAME_CONFIG,0x00000001,PDD_LOCATION_NOR_USER}
  #else
"""
#############################################
# end of header configuration file access
PddTmplConfigHeaderEndAccessNorUser = """/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif
#else
#error pdd_config_nor_user.h included several times
#endif
"""

#############################################
# begin of header configuration file access
PddTmplConfigHeaderBeginAccessSCC = """/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        generated header for the configuration on the SCC.
 * @addtogroup   PDD generated header file
 * @{
 */
#ifndef PDD_CONFIG_SCC_H
#define PDD_CONFIG_SCC_H

#ifdef __cplusplus
extern "C" {
#endif
"""
#############################################
# array definition SCC
PddTmplConfigHeaderDefineNumberSCC ="""/************************************************************************
|defines and macros (scope: global)
|---------------------------------------------------------------------------*/
/* end table*/
#define PDD_SCC_TABLE_INFO_END          """

#############################################
# array definition SCC
PddTmplConfigHeaderArrayAccessSCC ="""/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
/******** for SCC access *********/
typedef struct
{
  tU8       u8Filename[PDD_SCC_MAX_SIZE_NAME];    
  tU32      u32PDUPort;
  tU8       u8KindSave; 
}tsPddSccConfig;

/*structure for information, which use over processes (for shared memory)*/
typedef struct
{
  tS32      s32Size;
}tsPddScc;

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/
/* SCC configuration*/
static const tsPddSccConfig    vrPddSccConfig[PDD_SCC_TABLE_INFO_END] = 
{   /* name , PORT    , kind save, kind Pool */
"""

#############################################
# array definition SCC
PddTmplConfigHeaderArrayAccessSCCNew ="""/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
/******** for SCC access *********/
typedef struct
{
  tU8       u8Filename[PDD_SCC_MAX_SIZE_NAME];    
  tU32      u32PDUPort;
  tU8       u8KindSave; 
  tU8       u8KindPool;
  tU32      u32SizeData;
  tU32      u32Version;
}tsPddSccConfig;

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/
/* SCC configuration*/
static const tsPddSccConfig    vrPddSccConfig[PDD_SCC_TABLE_INFO_END] = 
{   /* name , PORT    , kind save, kind Pool , size, version*/
"""
#############################################
# end of header configuration file SCC
PddTmplConfigHeaderEndAccessSCC = """/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif
#else
#error pdd_config_scc.h included several times
#endif
"""

#############################################
# begin of header configuration kds
PddTmplConfigHeaderBeginConfigKDS = """/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        generated header for the configuration for KDS access.
 * @addtogroup   PDD generated header file
 * @{
 */
#ifndef PDD_CONFIG_KDS_H
#define PDD_CONFIG_KDS_H

#ifdef __cplusplus
extern "C" {
#endif
/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
#ifdef OSAL_GEN3_SIM
#define PDD_KDS_LOCATION                          PDD_LOCATION_FS
#else
"""
#############################################
# end of header configuration kds
PddTmplConfigHeaderEndConfigKDS = """#endif
/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif
#else
#error pdd_config_kds.h included several times
#endif
"""

#############################################
# begin of header 
PddTmplConfigHeaderBegin = """/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        generated header for the general configuration  
 * @addtogroup   PDD generated header file
 * @{
 */
#ifndef PDD_VARIANT_GEN_H
#define PDD_VARIANT_GEN_H

#ifdef __cplusplus
extern "C" {
#endif
/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/

"""

#############################################
# end of header 
PddTmplConfigHeaderEnd = """/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif
#else
#error pdd_variant_gen.h included several times
#endif
"""


