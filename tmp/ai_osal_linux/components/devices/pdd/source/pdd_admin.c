/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        pdd_admin.c
 *
 * This file contains all functions for initialize 
 * deinitialize the pdd module
 * 
 * global function:
 * -- 
 *
 * local function:
 * -- 
 *
 * @date        2012-10-10
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */

/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <time.h>
#include <fcntl.h>	   
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <grp.h>
#include <limits.h>   
#include "system_types.h"
#include "system_definition.h"
#include "pdd_variant.h"
#include "pdd.h"
#include "pdd_config_nor_user.h"
#include "pdd_trace.h"
#include "pdd_private.h"



#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
/* name for semaphore*/
#define PDD_SEM_NAME_INIT_LOCK             "PDD-SemInit"
#define PDD_SEM_NAME_INIT_LOCK_IN_SHM      "/dev/shm/sem.PDD-SemInit"
/* name for shared memory*/
#define PDD_SHM_GLOBAL_NAME                "PDD-Shm"

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/
typedef struct
{
  tS32                  s32AttachedProcesses;                          /*process attach counter*/
  sem_t                 tSemLockAccess[PDD_SEM_ACCESS_LAST];           /*semaphore for access to location file, nor, INC*/
  tBool                 vbInit[PDD_LOCATION_LAST];                     /*init varibale for each location */
  tBool                 vbInitTraceLevel;                              /*init varibale for trace level*/
  tU32                  vu32TraceLevel;                                /*trace level*/
  tU8                   vu8TraceStreamOutput;                          /*output stream to "Standard output" or/and "Standard error output"*/
  tsPddNorUserInfo      vtsPddNorUserInfo;
  tsPddNorKernelInfo    vtsPddNorKernelInfo;
  tsPddFileInfo         vtsPddFileInfo[PDD_LOCATION_CONFIG_FS_LAST];  /*includes the configuration to each location */
  tBool                 vbPddSccReadOldDataAfterStartup;               /*flag, which indicates that old data from SCC read is done*/
  tBool                 vbPddSccSendComponentStatusMsgDone;            /*flag, which indicates that message COMPONENT_STATUS_MSG is done*/
}trPddShm;

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/
static sem_t    *vPdd_SemLockInit;                 /*semaphore for init create with name*/

/************************************************************************
| variable definition (scope: module-global)
|-----------------------------------------------------------------------*/
/*shared memory definition*/
intptr_t      vPdd_ShmId=-1;                            /*shared memory id */
trPddShm* vpPdd_Shm = MAP_FAILED;                   /*pointer of shared memory*/

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static void  PDD_CreateSemaphore(tePddAccess);
static void  PDD_DestroySemaphore(tePddAccess);
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL
static void  PDD_SavePddTraceDatastream(void);
#endif
static void  PDD_GetConfigPathPdd(tePddLocation);
/******************************************************************************
* FUNCTION: pdd_process_attach()
*
* DESCRIPTION: Libraries should export initialization and cleanup routines 
*              using the gcc __attribute__((constructor)) 
*              and __attribute__((destructor)) function attributes. 
*              Constructor routines are executed before dlopen returns 
*              (or before main() is started if the library is loaded at load time). 
*              Destructor routines are executed before dlclose returns (
*              or after exit() or completion of main() 
*              if the library is loaded at load time).
*              This is the attach function
*
* PARAMETERS:  Void
*     
*
* RETURNS: void  
*
* HISTORY: Created by Andrea Bueter 2013 02 18
*****************************************************************************/
void __attribute__ ((constructor)) pdd_process_attach(void)
{
  tBool VbFirstAttach = TRUE;

  /* protect against initializing race, the second process has to wait until this is finished*/
  /* Linux Programmer's Manual: 
     If O_CREAT is specified in oflag, then the semaphore is created if it does not
     already exist. If both O_CREAT and O_EXCL are
     specified in oflag, then an error is returned if a semaphore with the given
     name already exists.*/
  /* try to create semaphore with name;(if success semaphore's value is set to zeror)*/
  vPdd_SemLockInit = sem_open(PDD_SEM_NAME_INIT_LOCK, O_EXCL | O_CREAT, PDD_ACCESS_RIGTHS, 0);
  if(vPdd_SemLockInit != SEM_FAILED)
  { /* no error; semaphore create => first attach */
    /* first attach */
    VbFirstAttach = TRUE;
	  /* no error; semaphore create => first attach */   
    PDD_TRACE(PDD_ADMIN_FIRST_ATTACH,0,0);
    /* create shared memory at first process attach */
    vPdd_ShmId= shm_open(PDD_SHM_GLOBAL_NAME, O_EXCL|O_RDWR|O_CREAT|O_TRUNC, PDD_ACCESS_RIGTHS );
    if(ftruncate(vPdd_ShmId, sizeof(trPddShm)) == -1)
    {
      vPdd_ShmId=-1;
    }
  }
  else
  { /* open semaphore*/
    vPdd_SemLockInit= sem_open(PDD_SEM_NAME_INIT_LOCK, 0);
    /* lock semaphore*/
    sem_wait(vPdd_SemLockInit);
	  /* not first attach*/
    VbFirstAttach = FALSE;
	  /*trace */
    PDD_TRACE(PDD_ADMIN_NOT_FIRST_ATTACH,0,0);
    /* open shared memory*/
    vPdd_ShmId = shm_open(PDD_SHM_GLOBAL_NAME, O_RDWR ,PDD_ACCESS_RIGTHS);    
  }
  if(vPdd_ShmId!=-1)
  { /* map global shared memory into address space */
    vpPdd_Shm = (trPddShm*)mmap( NULL,sizeof(trPddShm),PROT_READ | PROT_WRITE,MAP_SHARED,vPdd_ShmId,0);
    /* if no error */
    if(vpPdd_Shm != MAP_FAILED)
    { /* if first proccess access */
      if(VbFirstAttach)
      { /*set access rights to Group "eco_pdd*/
	      s32PDD_ChangeGroupAccess(PDD_KIND_RES_SEM,-1,PDD_SEM_NAME_INIT_LOCK_IN_SHM);	
		    s32PDD_ChangeGroupAccess(PDD_KIND_RES_SHM,vPdd_ShmId,"vPdd_ShmId");	   		   
		    /*set process attach to zero*/
	      vpPdd_Shm->s32AttachedProcesses=0;
	      PDD_InitAccess();	 		
      }	
	    /*increment counter*/
      vpPdd_Shm->s32AttachedProcesses++;
	    /*trace */
      PDD_TRACE(PDD_ADMIN_ATTACH_COUNT,&vpPdd_Shm->s32AttachedProcesses,sizeof(tU32));
    }
    /* init finished, let's go! */
    sem_post(vPdd_SemLockInit);
  }
}
/******************************************************************************
* FUNCTION: pdd_process_detach()
*
* DESCRIPTION: Libraries should export initialization and cleanup routines 
*              using the gcc __attribute__((constructor)) 
*              and __attribute__((destructor)) function attributes. 
*              Constructor routines are executed before dlopen returns 
*              (or before main() is started if the library is loaded at load time). 
*              Destructor routines are executed before dlclose returns (
*              or after exit() or completion of main() 
*              if the library is loaded at load time).
*              This is the DeAttach function.
*
* PARAMETERS: Void
*     
*
* RETURNS: void 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void __attribute__ ((destructor)) pdd_process_detach(void)
{

}
/******************************************************************************
* FUNCTION: PDD_InitAccess()
*
* DESCRIPTION: init for all possible accesses 
*
* PARAMETERS: Void
*     
*
* RETURNS: void 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void  PDD_InitAccess(void)
{
  tePddLocationFSConfig VtsInc;
  /* create semaphore for each location */
  PDD_CreateSemaphore(PDD_SEM_ACCESS_FILE_SYSTEM);
  PDD_CreateSemaphore(PDD_SEM_ACCESS_NOR_USER);
  PDD_CreateSemaphore(PDD_SEM_ACCESS_NOR_KERNEL);
  PDD_CreateSemaphore(PDD_SEM_ACCESS_SCC);   
  /* set init flag for access to location*/
  vpPdd_Shm->vbInit[PDD_LOCATION_FS]=FALSE;
  vpPdd_Shm->vbInit[PDD_LOCATION_FS_SECURE]=FALSE;
  vpPdd_Shm->vbInit[PDD_LOCATION_NOR_USER]=FALSE;
  vpPdd_Shm->vbInit[PDD_LOCATION_NOR_KERNEL]=FALSE;
  vpPdd_Shm->vbInit[PDD_LOCATION_SCC]=FALSE; 
  vpPdd_Shm->vbInitTraceLevel=FALSE;
  vpPdd_Shm->vu32TraceLevel=PDD_TRACE_DEFAULT;
  vpPdd_Shm->vu8TraceStreamOutput=PDD_TRACE_DEFAULT_STREAM_OUTPUT;
  vpPdd_Shm->vbPddSccReadOldDataAfterStartup=FALSE;
  vpPdd_Shm->vbPddSccSendComponentStatusMsgDone=FALSE;
  /*set string path to zero */
  for(VtsInc=PDD_LOCATION_CONFIG_FS;VtsInc<PDD_LOCATION_CONFIG_FS_LAST;VtsInc++)
  {
    memset(vpPdd_Shm->vtsPddFileInfo[VtsInc].strPathName,0,PDD_FILE_MAX_PATH);
  }
}
/******************************************************************************
* FUNCTION: PDD_DeInitAccess()
*
* DESCRIPTION: init for all possible accesses 
*
* PARAMETERS: Void
*     
*
* RETURNS: void 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void  PDD_DeInitAccess(void)
{
  tePddLocationFSConfig VtsInc;
  /*deinit nor user access*/
  PDD_NorUserAccessDeInit();		
  PDD_NorKernelAccessDeInit();		
  vpPdd_Shm->vbInit[PDD_LOCATION_FS]=FALSE;
  vpPdd_Shm->vbInit[PDD_LOCATION_FS_SECURE]=FALSE;
  vpPdd_Shm->vbInit[PDD_LOCATION_NOR_USER]=FALSE;
  vpPdd_Shm->vbInit[PDD_LOCATION_NOR_KERNEL]=FALSE;
  vpPdd_Shm->vbInit[PDD_LOCATION_SCC]=FALSE; 
  vpPdd_Shm->vbInitTraceLevel=FALSE;
  vpPdd_Shm->vu32TraceLevel=PDD_TRACE_DEFAULT;
  vpPdd_Shm->vu8TraceStreamOutput=PDD_TRACE_DEFAULT_STREAM_OUTPUT;
  vpPdd_Shm->vbPddSccReadOldDataAfterStartup=FALSE;
  vpPdd_Shm->vbPddSccSendComponentStatusMsgDone=FALSE;
  /*destroy semaphore, only if not blocked*/
  PDD_DestroySemaphore(PDD_SEM_ACCESS_FILE_SYSTEM);
  PDD_DestroySemaphore(PDD_SEM_ACCESS_NOR_USER);
  PDD_DestroySemaphore(PDD_SEM_ACCESS_NOR_KERNEL);
  PDD_DestroySemaphore(PDD_SEM_ACCESS_SCC);   
  /*set string path to zero */
  for(VtsInc=PDD_LOCATION_CONFIG_FS;VtsInc<PDD_LOCATION_CONFIG_FS_LAST;VtsInc++)
  {
    memset(vpPdd_Shm->vtsPddFileInfo[VtsInc].strPathName,0,PDD_FILE_MAX_PATH);
  }
}
/******************************************************************************
* FUNCTION: PDD_CreateSemaphore()
*
* DESCRIPTION: create and open semaphore and shared memory 
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: success
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void PDD_CreateSemaphore(tePddAccess PpAccess)
{
  sem_t* VpSemLock=&vpPdd_Shm->tSemLockAccess[PpAccess];
  if(sem_init(VpSemLock, 1/*shared*/, 1) < 0)
  {/* trace error*/        
    tS32 Vs32Result=PDD_ERROR_ADMIN_SEM_CREATE_SEM;  
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else
  {
    PDD_TRACE(PDD_ADMIN_SEM_INIT_SUCCESS,&PpAccess,sizeof(tePddAccess));
  }
}
/******************************************************************************
* FUNCTION: PDD_DestroySemaphore()
*
* DESCRIPTION: create and open semaphore and shared memory 
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: success
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
void PDD_DestroySemaphore(tePddAccess PpAccess)
{
  tS32   Vs32Value;
  tS32   Vs32Result;
  sem_t* VpSemLock=&vpPdd_Shm->tSemLockAccess[PpAccess];

  if(sem_getvalue(VpSemLock,&Vs32Value)==0)
  { /* trace out value*/
    PDD_TRACE(PDD_ADMIN_SEM_VALUE,&Vs32Value,sizeof(tS32));	
	  /* if value=0;semaphore waits => no destroy !!!*/
	  if(Vs32Value==0)
	  {
      Vs32Result=PDD_ERROR_ADMIN_SEM_DESTROY_SINCE_WAIT; 
	    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
	  }
	  else
	  { /*destroy semaphore*/
      if(sem_destroy(VpSemLock) < 0)
      {/* trace error*/        
        Vs32Result=PDD_ERROR_ADMIN_SEM_DESTROY; 
	      PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
      }
      else
      {
        PDD_TRACE(PDD_ADMIN_SEM_DESTROY_SUCCESS,&PpAccess,sizeof(tePddAccess));
      }
    }
  }
  else
  {
    Vs32Result=PDD_ERROR_ADMIN_SEM_GET_VALUE;  
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
}
/******************************************************************************
* FUNCTION: PDD_IsInit()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2013 03 07
*****************************************************************************/
tS32 PDD_IsInit(tePddLocation PenLocation,const char* PtsInfoStream)
{
  tS32 Vs32Result=PDD_OK;
  /*check shared memory*/
  if(vpPdd_Shm != MAP_FAILED)
  { /*check attached proccesses*/
    if(vpPdd_Shm->s32AttachedProcesses <= 0)
	  { /*trace */
      tU8 Vu8Buffer[200];		
	    Vs32Result=PDD_ERROR_ADMIN_NO_PROZESS_ATTACH;  
      PDD_TRACE(PDD_ADMIN_ATTACH_COUNT,vpPdd_Shm->s32AttachedProcesses,sizeof(tU32));
	    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsInfoStream);
	    PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);  		
	    PDD_FATAL_M_ASSERT_ALWAYS();
	  }
	  else
	  { /* init location*/
      switch(PenLocation)
	    {
	      case PDD_LOCATION_FS: /* no break*/  
	      case PDD_LOCATION_FS_SECURE:
	      { /*check init*/
		      if(vpPdd_Shm->vbInit[PenLocation]==FALSE)
		      {          
            PDD_GetConfigPathPdd(PenLocation);
            if(PDD_FileCreatePath(PenLocation,PtsInfoStream)!=PDD_OK)
            {            
              tU8 Vu8Buffer[200];		
              Vs32Result=PDD_ERROR_ADMIN_INIT_FILE_SYSTEM;  	      	    
              snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsInfoStream);
	            PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);  			
            }
            else
            {
              vpPdd_Shm->vbInit[PenLocation]=TRUE;
            }
          }/*end if*/
        }break;
	      case PDD_LOCATION_NOR_USER:
	      { /*call init process function for variable, which valid for process; open LFX*/
		      Vs32Result=PDD_NorUserAccessInitProcess();
		      if(Vs32Result==PDD_OK)
		      { /*check if init for all process done */
		        if(vpPdd_Shm->vbInit[PDD_LOCATION_NOR_USER]==FALSE)
		        { /*init for all process*/
		          if(PDD_NorUserAccessInit()!=PDD_OK)
		          {
                tU8 Vu8Buffer[200];		
	              Vs32Result=PDD_ERROR_ADMIN_INIT_NOR_USER;  
	              snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsInfoStream);
	              PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);  			
              }
		          else
		          {
		            vpPdd_Shm->vbInit[PDD_LOCATION_NOR_USER]=TRUE;
              }
            }
          }		 
        }break;
		    case PDD_LOCATION_NOR_KERNEL:
	      { /*call init process function for variable, which valid for process; open LFX*/
		      Vs32Result=PDD_NorKernelAccessInitProcess();
		      if(Vs32Result==PDD_OK)
		      { /*check if init for all process done */
		        if(vpPdd_Shm->vbInit[PDD_LOCATION_NOR_KERNEL]==FALSE)
		        { /*init for all process*/
		          if(PDD_NorKernelAccessInit()!=PDD_OK)
		          {
                tU8 Vu8Buffer[200];		
	              Vs32Result=PDD_ERROR_ADMIN_INIT_NOR_KERNEL;  
	              snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsInfoStream);
	              PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);  	
              }
		          else
		          {
		            vpPdd_Shm->vbInit[PDD_LOCATION_NOR_KERNEL]=TRUE;
              }
            }
          }		 
        }break;
	      case PDD_LOCATION_SCC:
	      {
		      if(vpPdd_Shm->vbInit[PDD_LOCATION_SCC]==FALSE)
		      {
		        vpPdd_Shm->vbInit[PDD_LOCATION_SCC]=TRUE;		  
          }
        }break;
	      default:
	      {
	        Vs32Result=PDD_ERROR_WRONG_PARAMETER;  
        }break;
      }/*end switch*/
    }/*end else*/
  }/*end if*/
  else
  { /*error */
    tU8 Vu8Buffer[200];		
	  Vs32Result=PDD_ERROR_ADMIN_SHARED_MEM_NOT_VALID;  
    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsInfoStream);
	  PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);  
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  return(Vs32Result);
}

/******************************************************************************
* FUNCTION: vPDD_InitTraceLevel()
*
* DESCRIPTION: if stored trace level in NOR; read trace level; 
*
* PARAMETERS:
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2013 03 07
*****************************************************************************/
void  vPDD_InitTraceLevel(void)
{
  /*check shared memory*/
  if(vpPdd_Shm != MAP_FAILED)
  {
    if(vpPdd_Shm->vbInitTraceLevel==FALSE)
    { 
	    vpPdd_Shm->vu32TraceLevel=PDD_TRACE_DEFAULT;
	    vpPdd_Shm->vu8TraceStreamOutput=PDD_TRACE_DEFAULT_STREAM_OUTPUT;
	    vpPdd_Shm->vbInitTraceLevel=TRUE;
	    /* read trace level */
      #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL
	    //lock
      PDD_Lock(PDD_SEM_ACCESS_NOR_USER);
	    //is NOR init
	    tS32 Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL);
	    if(Vs32Result==PDD_OK)
	    {/*read datastream from nor*/	 
        tS32  Vs32Size=500;
	      tU8*  VpBuffer=(tU8*)malloc(Vs32Size);
		    if(VpBuffer!=NULL)
		    {
			    Vs32Size=PDD_NorUserAccessReadDataStreamEarly(PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL,(void*) VpBuffer,Vs32Size);
		      if(Vs32Size > (tS32) sizeof(tsPddFileHeader))
		      { /*check file */
		        if(PDD_ValidationCheckFile((void*)VpBuffer,Vs32Size,0x0001)==TRUE)
		        { 
			        if(pdd_helper_get_element_from_stream("TraceLevelPDD",VpBuffer,Vs32Size,&vpPdd_Shm->vu32TraceLevel,sizeof(vpPdd_Shm->vu32TraceLevel),0)!=0)
				      {//error set trace level to default
				        vpPdd_Shm->vu32TraceLevel=PDD_TRACE_DEFAULT;
				      } 
				      if(pdd_helper_get_element_from_stream("TraceStreamOutputPDD",VpBuffer,Vs32Size,&vpPdd_Shm->vu8TraceStreamOutput,sizeof(vpPdd_Shm->vu8TraceStreamOutput),0)!=0)
				      {//error set trace level to default
				        vpPdd_Shm->vu8TraceStreamOutput=PDD_TRACE_DEFAULT_STREAM_OUTPUT;
				      }
            }	     
          }
			    free(VpBuffer);
        }
      }
	    //unlock
	    PDD_UnLock(PDD_SEM_ACCESS_NOR_USER);
      #endif
	    /*trace */
      PDD_TRACE(PDD_ADMIN_TRACE_LEVEL,&vpPdd_Shm->vu32TraceLevel,sizeof(tU32));
	    PDD_TRACE(PDD_ADMIN_TRACE_STREAM_OUTPUT,&vpPdd_Shm->vu8TraceStreamOutput,sizeof(tU8));
    }
  }
}
/******************************************************************************
* FUNCTION: u32PDD_GetTraceLevel()
*
* DESCRIPTION: return  trace level
*
* PARAMETERS:
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2013 03 07
*****************************************************************************/
tU32  u32PDD_GetTraceLevel(void)
{
  if(vpPdd_Shm != MAP_FAILED)
  {
    return(vpPdd_Shm->vu32TraceLevel);
  }
  else
  {
    return(PDD_TRACE_DEFAULT);
  }
}
/******************************************************************************
* FUNCTION: vPDD_SetTraceLevel()
*
* DESCRIPTION: set  trace level
*
* PARAMETERS:
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2013 03 07
*****************************************************************************/
void  vPDD_SetTraceLevel(tU32 Pu32TraceLevel,tBool PbClear)
{
   /*check shared memory*/
   if(vpPdd_Shm != MAP_FAILED)
   { //get vpPdd_Shm-> from NOR 
     #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL
	   tS32  Vs32SizeStream=pdd_get_data_stream_size(PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL,PDD_LOCATION_NOR_USER);  
	   if(Vs32SizeStream>0)
	   {
	     tU8*  VpBuffer=(tU8*)malloc(Vs32SizeStream);	
	     if(VpBuffer!=NULL)
	     {
	       tU8   Vu8Info;
	       Vs32SizeStream=pdd_read_datastream(PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL,PDD_LOCATION_NOR_USER,VpBuffer,Vs32SizeStream,0x0001,&Vu8Info);
		     if(Vs32SizeStream > 0)
		     {
		       if(pdd_helper_get_element_from_stream("TraceStreamOutputPDD",VpBuffer,Vs32SizeStream,&vpPdd_Shm->vu8TraceStreamOutput,sizeof(vpPdd_Shm->vu8TraceStreamOutput),0)!=0)
		       {//error set vu8TraceStreamOutput to default
		         vpPdd_Shm->vu8TraceStreamOutput=PDD_TRACE_DEFAULT_STREAM_OUTPUT;
           }		 
         }
		     free(VpBuffer);
       }	  
     }
     #endif
     //change value for vpPdd_Shm->vu32TraceLevel
     if(PbClear==FALSE)
	   {
       vpPdd_Shm->vu32TraceLevel|=Pu32TraceLevel;
     }
	   else
	   {
	     vpPdd_Shm->vu32TraceLevel=PDD_TRACE_DEFAULT;
     }
	   /*save conplete datastream */
     #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL
	   PDD_SavePddTraceDatastream();
     #endif
   }
}
/******************************************************************************
* FUNCTION: u8PDD_GetTraceStreamOutput()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2015 03 03
*****************************************************************************/
tU8  u8PDD_GetTraceStreamOutput(void)
{
  if(vpPdd_Shm != MAP_FAILED)
  {
    return(vpPdd_Shm->vu8TraceStreamOutput);
  }
  else
  {
    return(PDD_TRACE_DEFAULT_STREAM_OUTPUT);
  }
}
/******************************************************************************
* FUNCTION: vPDD_SetTraceStreamOutput()
*
* DESCRIPTION: set stream output 
*
* PARAMETERS:
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2015 03 03
*****************************************************************************/
void vPDD_SetTraceStreamOutput(tU8 Pu8TraceStreamOutput,tBool PbClear)
{
  /*check shared memory*/
  if(vpPdd_Shm != MAP_FAILED)
  { //get vpPdd_Shm->vu32TraceLevel from NOR 
    #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL	  
	  tS32  Vs32SizeStream=pdd_get_data_stream_size(PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL,PDD_LOCATION_NOR_USER);  
	  if(Vs32SizeStream>0)
	  {
      fprintf(stderr, "Vs32SizeStream %d\n", Vs32SizeStream);	  
	    tU8*  VpBuffer=(tU8*)malloc(Vs32SizeStream);	
	    if(VpBuffer!=NULL)
	    {        
	      Vs32SizeStream=pdd_read_data_stream(PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL,PDD_LOCATION_NOR_USER,VpBuffer,Vs32SizeStream,0x0001);
		    if(Vs32SizeStream > 0)
		    {
		      if(pdd_helper_get_element_from_stream("TraceLevelPDD",VpBuffer,Vs32SizeStream,&vpPdd_Shm->vu32TraceLevel,sizeof(vpPdd_Shm->vu32TraceLevel),0)!=0)
		      {//error set trace level to default
		        vpPdd_Shm->vu32TraceLevel=PDD_TRACE_DEFAULT;
		      }		 
        }
		    free(VpBuffer);
      }	  
    }
    #endif
	  //change value for vpPdd_Shm->vu8TraceStreamOutput
    if(PbClear==FALSE)
    {
      vpPdd_Shm->vu8TraceStreamOutput|=Pu8TraceStreamOutput;
    }
	  else
	  {
	    vpPdd_Shm->vu8TraceStreamOutput=PDD_TRACE_DEFAULT_STREAM_OUTPUT;
    }
    /*save complete datastream */
    #ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL
	  PDD_SavePddTraceDatastream();
    #endif
  }
}
#ifdef PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL
/******************************************************************************
* FUNCTION: PDD_SavePddTraceDatastream()
*
* DESCRIPTION: save trace datastream if pool configurate
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2015 03 03
*****************************************************************************/
static void  PDD_SavePddTraceDatastream(void)
{
  /* Vpu8WriteBuffer: -- 4 bytes frame lenght -- 1 byte elemente version -- 4 bytes hash  -- elemente name -- lenght -- TraceLevel*/
  tS32  Vs32SizeStream1=4+1+4+strlen("TraceLevelPDD")+1+sizeof(tU32)+sizeof(tU32);
  tS32  Vs32SizeStream2=4+1+4+strlen("TraceStreamOutputPDD")+1+sizeof(tU32)+sizeof(tU8);
  tU8*  Vpu8WriteBuffer=malloc(Vs32SizeStream1+Vs32SizeStream2);
  if(Vpu8WriteBuffer!=NULL)
  {
    tU8*  VpBufferWriteTemp=Vpu8WriteBuffer;
	  tU32  Vu32SizeElement=sizeof(tU32);
	  memset(VpBufferWriteTemp,0,Vs32SizeStream1+Vs32SizeStream2);
	  //TraceLevelPDD
	  memcpy(VpBufferWriteTemp,(tU8*)&Vs32SizeStream1,sizeof(tS32)); //length
	  VpBufferWriteTemp+=sizeof(tS32)+1+4;                          //inc pointer after hash 
	  strncpy(VpBufferWriteTemp,(tU8*)"TraceLevelPDD",strlen("TraceLevelPDD")); //name
	  VpBufferWriteTemp+=1+strlen("TraceLevelPDD");                  //inc pointer
	  memcpy(VpBufferWriteTemp,(tU8*)&Vu32SizeElement,sizeof(tU32)); //length
    VpBufferWriteTemp+=sizeof(tU32);                               //inc pointer
	  memcpy(VpBufferWriteTemp,(tU8*)&vpPdd_Shm->vu32TraceLevel,sizeof(tU32)); 
	  VpBufferWriteTemp+=sizeof(tU32);   
	  //TraceStreamOutputPDD
	  Vu32SizeElement=sizeof(tU8);
	  memcpy(VpBufferWriteTemp,(tU8*)&Vs32SizeStream2,sizeof(tS32)); //length
	  VpBufferWriteTemp+=sizeof(tS32)+1+4;                          //inc pointer after hash 
	  strncpy(VpBufferWriteTemp,(tU8*)"TraceStreamOutputPDD",strlen("TraceStreamOutputPDD")); //name
	  VpBufferWriteTemp+=1+strlen("TraceStreamOutputPDD");                  //inc pointer
	  memcpy(VpBufferWriteTemp,(tU8*)&Vu32SizeElement,sizeof(tU32)); //length
    VpBufferWriteTemp+=sizeof(tU32);                               //inc pointer
	  memcpy(VpBufferWriteTemp,(tU8*)&vpPdd_Shm->vu8TraceStreamOutput,sizeof(tU8)); 	  
	  //write
	  pdd_write_data_stream(PDD_NOR_USER_DATASTREAM_NAME_PDDTRACELEVEL,PDD_LOCATION_NOR_USER,Vpu8WriteBuffer,Vs32SizeStream1+Vs32SizeStream2,0x0001);
	  free(Vpu8WriteBuffer);
  }
}
#endif
/******************************************************************************
* FUNCTION: PDD_Lock()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2013 03 06
*****************************************************************************/
tS32 PDD_Lock(tePddAccess PpAccess)
{
  tS32   Vs32Value;
  tS32   Vs32Result=PDD_OK;
  sem_t* VpSemLock=&vpPdd_Shm->tSemLockAccess[PpAccess];

  /* get value semaphore*/
  if(sem_getvalue(VpSemLock,&Vs32Value)==0)
  { /* trace out value*/
    PDD_TRACE(PDD_ADMIN_SEM_VALUE,&Vs32Value,sizeof(tS32));	
    /* lock semaphore*/
    if(sem_wait(VpSemLock)==0)  
	  { /* no error */
	    PDD_TRACE(PDD_ADMIN_SEM_WAIT,&PpAccess,sizeof(tePddAccess));	
    }/*end if*/
	  else
  	{
      Vs32Result=PDD_ERROR_ADMIN_SEM_WAIT; 
	    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
	    PDD_FATAL_M_ASSERT_ALWAYS();
	  }
  }
  else
  {
    Vs32Result=PDD_ERROR_ADMIN_SEM_GET_VALUE; 
    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_UnLock()
*
* DESCRIPTION: create and open semaphore and shared memory 
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: success
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2012 10 10
*****************************************************************************/
tS32 PDD_UnLock(tePddAccess PpAccess)
{ 
  tS32   Vs32Value;
  tS32   Vs32Result=0;
  sem_t* VpSemLock=&vpPdd_Shm->tSemLockAccess[PpAccess];

  /* get value semaphore*/
  if(sem_getvalue(VpSemLock,&Vs32Value)==0)
  { /* trace out value*/
    PDD_TRACE(PDD_ADMIN_SEM_VALUE,&Vs32Value,sizeof(tS32));
	  /* post semaphore*/
    if(sem_post(VpSemLock)==0)  
	  { /* no error => get pointer*/
	    PDD_TRACE(PDD_ADMIN_SEM_POST,&PpAccess,sizeof(tePddAccess));	
    }/*end if*/
	  else
	  {
      Vs32Result=PDD_ERROR_ADMIN_SEM_POST; 
	    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
	    PDD_FATAL_M_ASSERT_ALWAYS();
	  }
  }
  else
  {
    Vs32Result=PDD_ERROR_ADMIN_SEM_GET_VALUE;  
    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_GetNorUserNorInfoShm()
*
* DESCRIPTION: get nor user info from shared memory 
*
* RETURNS: 
*       pointer
*
* HISTORY:Created by Andrea Bueter 2012 10 10
*****************************************************************************/
tsPddNorUserInfo*  PDD_GetNorUserInfoShm(void)
{
	return(&vpPdd_Shm->vtsPddNorUserInfo);
}

/******************************************************************************
* FUNCTION: PDD_GetNorUserNorInfoShm()
*
* DESCRIPTION: get nor user info from shared memory 
*
* RETURNS: 
*       pointer
*
* HISTORY:Created by Andrea Bueter 2012 10 10
*****************************************************************************/
tsPddNorKernelInfo*  PDD_GetNorKernelInfoShm(void)
{
	return(&vpPdd_Shm->vtsPddNorKernelInfo);
}
/******************************************************************************
* FUNCTION: bPDD_SccGetReadOldDataAfterStartup()
*
* DESCRIPTION: get flag ReadOldDataAfterStartup from shared memory
*
* RETURNS: flag
*       
*
* HISTORY:Created by Andrea Bueter 2014 10 10
*****************************************************************************/
tBool  bPDD_SccGetReadOldDataAfterStartup(void)
{
  return(vpPdd_Shm->vbPddSccReadOldDataAfterStartup);
}
/******************************************************************************
* FUNCTION: vPDD_SccSetReadOldDataAfterStartup()
*
* DESCRIPTION: set flag ReadOldDataAfterStartup 
*
* RETURNS: void
*       
*
* HISTORY:Created by Andrea Bueter 2014 10 10
*****************************************************************************/
void  vPDD_SccSetReadOldDataAfterStartup(tBool VbFlag)
{
  vpPdd_Shm->vbPddSccReadOldDataAfterStartup=VbFlag;
}
/******************************************************************************
* FUNCTION: bPDD_SccGetFlagSendComponentStatusMsgDone()
*
* DESCRIPTION: get flag vbPddSccSendComponentStatusMsgDone from shared memory
*
* RETURNS: flag
*       
*
* HISTORY:Created by Andrea Bueter 2014 11 11
*****************************************************************************/
tBool  bPDD_SccGetFlagSendComponentStatusMsgDone(void)
{
  //return(FALSE);
  return(vpPdd_Shm->vbPddSccSendComponentStatusMsgDone);
}
/******************************************************************************
* FUNCTION: vPDD_SccSetReadOldDataAfterStartup()
*
* DESCRIPTION: set flag vbPddSccSendComponentStatusMsgDone 
*
* RETURNS: void
*       
*
* HISTORY:Created by Andrea Bueter 2014 11 11
*****************************************************************************/
void  vPDD_SccSetFlagSendComponentStatusMsgDone(tBool VbFlag)
{
  vpPdd_Shm->vbPddSccSendComponentStatusMsgDone=VbFlag;
}
/******************************************************************************
* FUNCTION: s32PDD_ChangeGroupAccess()
*
* DESCRIPTION: change group access to eco_pdd
*
* RETURNS: 
*      
*
* HISTORY:Created by Andrea Bueter 2014 07 31
*****************************************************************************/
tS32   s32PDD_ChangeGroupAccess(tePddKindRes PeKindRes,tS32 Ps32Id,const char* PStrResource)
{
  tS32   Vs32Result=PDD_OK;
  tU8    Vu8Buffer[100];		
  memset(Vu8Buffer,0,sizeof(Vu8Buffer));
  /*  The getgrnam() function returns a pointer to a structure containing the group id*/
  struct group *VtGroup=getgrnam(PDD_ACCESS_RIGTHS_GROUP_NAME);
  if(VtGroup==NULL)
  { 
    //Vs32Result=PDD_ERROR_ADMIN_GROUP_ACCESS;  
	  //snprintf(Vu8Buffer,sizeof(Vu8Buffer),"ADMIN: getgrnam() fails for resource %s",PStrResource);
  }
  else
  { /*for named semaphore and */
	  if((PeKindRes==PDD_KIND_RES_SEM)||(PeKindRes==PDD_KIND_RES_DIR))
	  {
	    if(chown(PStrResource,(uid_t) -1, VtGroup->gr_gid)==-1)
	    {	
		    Vs32Result=PDD_ERROR_ADMIN_GROUP_ACCESS;  	 
   	    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"ADMIN: chown() fails for resource %s",PStrResource);	   
      }
	    else
	    {
	      tU32 Vu32AccessRights=PDD_ACCESS_RIGTHS;
        if(PeKindRes==PDD_KIND_RES_DIR)
		      Vu32AccessRights=PDD_ACCESS_RIGTHS_DIR;
        if(chmod(PStrResource, Vu32AccessRights) == -1)
        {		
          Vs32Result=PDD_ERROR_ADMIN_GROUP_ACCESS;  	 
	        snprintf(Vu8Buffer,sizeof(Vu8Buffer),"ADMIN: chmod() fails for resource %s",PStrResource);
        }
      }
    }
	  else
	  {/* for other resource*/
	    if(fchown(Ps32Id, (uid_t) -1, VtGroup->gr_gid)==-1)
	    {	   	
		    Vs32Result=PDD_ERROR_ADMIN_GROUP_ACCESS;  	
   	    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"ADMIN: fchown() fails for resource %s",PStrResource);
      }
	    else
	    {
        if(fchmod(Ps32Id, PDD_ACCESS_RIGTHS) == -1)
        {        
          Vs32Result=PDD_ERROR_ADMIN_GROUP_ACCESS;  	         
	        snprintf(Vu8Buffer,sizeof(Vu8Buffer),"ADMIN: fchmod() fails for resource %s",PStrResource);	     
        }
      }
    }
  }
  /*trace error code and set error memory*/
  if(Vs32Result!=PDD_OK)
  {
    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
    PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
  }
  return(Vs32Result);
}

/******************************************************************************
* FUNCTION: strPDD_GetConfigPath()
*
* DESCRIPTION: return a string of the path given with PtsLocationFSConfig
*
* RETURNS: NULL => config path doesn't exist
*      
*
* HISTORY:Created by Andrea Bueter 2016 03 07
*****************************************************************************/
char*  strPDD_GetConfigPath(tePddLocationFSConfig PtsLocationFSConfig)
{
  char* VStrReturn=&vpPdd_Shm->vtsPddFileInfo[PtsLocationFSConfig].strPathName[0];
  if(strlen(VStrReturn)==0)
  {
    VStrReturn=NULL;
  }
  //fprintf(stderr, "File: strPDD_GetConfigPath: '%s' %d\n",VStrReturn,PtsLocationFSConfig);
  return(VStrReturn);
}
/******************************************************************************
* FUNCTION: PDD_GetConfigPathPdd()
*
* DESCRIPTION: open configuartion file and store the configuration
*              into shared memory      
*
* HISTORY:Created by Andrea Bueter 2016 03 08
*****************************************************************************/
static void  PDD_GetConfigPathPdd(tePddLocation PteLocation)
{
  char*                 VstrLine=NULL;
  tePddLocationFSConfig VtsLocationFSConfig;
  //struct timespec       VTimer={0,0};

  //clock_gettime(CLOCK_MONOTONIC, &VTimer);
  //fprintf(stderr, "PDD_GetConfigPathPdd: time:%d s %d ms\n",VTimer.tv_sec,VTimer.tv_nsec/1000000);

  PDD_TRACE(PDD_ADMIN_TRACE_START_GET_CONFIG_PATH,&PteLocation,sizeof(tePddLocation));  
  if((strPDD_GetConfigPath(PDD_LOCATION_CONFIG_FS)==NULL)||(strPDD_GetConfigPath(PDD_LOCATION_CONFIG_FS_SECURE)==NULL))
  {
    char* VstrConfig;
    if(PteLocation==PDD_LOCATION_FS)
    {
      VstrLine=PDD_FileReadConfigLine("PDD_PATH_FILE");
      VtsLocationFSConfig=PDD_LOCATION_CONFIG_FS;
    }
    else
    {
      VstrLine=PDD_FileReadConfigLine("PDD_PATH_SECURE_FILE");
      VtsLocationFSConfig=PDD_LOCATION_CONFIG_FS_SECURE;
    }
    VstrConfig=vpPdd_Shm->vtsPddFileInfo[VtsLocationFSConfig].strPathName;
    if(VstrLine!=NULL)
    { /*config file could be read => search string */
      char* VstrTemp=strchr(VstrLine,'/');
      if(VstrTemp!=NULL)
      {
        strncpy(VstrConfig,VstrTemp,PDD_FILE_MAX_PATH);   
        VstrTemp=strrchr(VstrConfig,'/'); //search last "/"
        if(VstrTemp!=NULL)
        {
          VstrTemp[1]=0; /*set end of string*/
        }
      }
      free(VstrLine);
    }
    else
    { /*configuration not found => make error memory entry and set to default*/    
      if(PteLocation==PDD_LOCATION_FS)
      {
        PDD_SET_ERROR_ENTRY("ADMIN: Error configuration for path 'PDD_PATH_FILE' not found!");
        strncpy(VstrConfig,PDD_FILE_DEFAULT_PATH,PDD_FILE_MAX_PATH);
      }
      else
      {
        PDD_SET_ERROR_ENTRY("ADMIN: Error configuration for path 'PDD_PATH_SECURE_FILE' not found!");
        strncpy(VstrConfig,PDD_FILE_DEFAULT_PATH_SECURE,PDD_FILE_MAX_PATH);      
      }
    }
    vpPdd_Shm->vtsPddFileInfo[VtsLocationFSConfig].strPathName[PDD_FILE_MAX_PATH-1]='\0';
    /* trace out the config path*/
    PDD_TRACE(PDD_ADMIN_TRACE_CONFIG_PATH,VstrConfig,strlen(VstrConfig)+1);  
  }
 // clock_gettime(CLOCK_MONOTONIC, &VTimer);
 // fprintf(stderr, "PDD_GetConfigPathPdd: time:%d s %d ms\n",VTimer.tv_sec,VTimer.tv_nsec/1000000);
}
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/* End of File pdd_admin.c                                                    */
/******************************************************************************/
