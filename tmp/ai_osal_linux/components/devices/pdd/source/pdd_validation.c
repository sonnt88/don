/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        pdd_validation.c
 *
 * This file contains function for the validation of a datastream
 * 
 * global function:
 * -- PDD_ValidationCheckFile:
 *         check the buffer with magic,crc32 and version
 * -- PDD_ValidationCheckScc:
 *         check the buffer with crc8 and version
 * -- PDD_ValidationGetHeaderFile:
 *         get header of the buffer with crc32
 * -- PDD_ValidationGetHeaderScc:
 *         get header of the buffer with crc8
 * -- PDD_ValidationCheckCrcEqualFile:
 *         check if the CRC of backup file and normal file are equal
 * -- PDD_ValidationCheckCrcEqualScc:
 *         check if the CRC8 of backup and normal are equal
 * -- PDD_ValidationCalcCrc32:
 *         calculate a crc32
 * -- PDD_ValidationCalcCrc8:
 *         calculate a crc8
 *
 * local function:
 *
 * @date        2013-2-22
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */

/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <limits.h>   
#include "system_types.h"

#include "pdd_variant.h"
#ifdef PDD_TESTMANGER_ACTIVE  //define is set in pdd_variant.h
#include "pdd.h"
#include "pdd_testmanager.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#else
#include "system_definition.h"
#include "pdd.h"
#include "LFX2.h"
#include "pdd_config_nor_user.h"
#include "inc.h"
#include "dgram_service.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#ifdef PDD_SCC_POOL_EXIST
#include "DataPoolSCC.h" 
#endif
#ifdef PDD_NOR_KERNEL_POOL_EXIST  
#include "DataPoolKernel.h"
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/


/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/


/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/
/*************************************************************************/
/*                                                                       */
/* CRC LOOKUP TABLE                                                      */
/* ================                                                      */
/* The following CRC lookup table was generated automagically            */
/* by the Rocksoft^tm Model CRC Algorithm Table Generation               */
/* Program V1.0 using the following model parameters:                    */
/*                                                                       */
/*    Width   : 2 bytes.                                                 */
/*    Poly    : 0x04C11DB7L                                              */
/*    Reverse : TRUE.                                                    */
/*                                                                       */
/* Die Tabelle wurde beim Generieren in High- und Low-Teil               */
/* gesplittet. Dadurch reicht ein Table-Offset aus, um beide             */
/* Teil-Werte der 32-Bit-CRC adressieren zu k�nnen.                      */
/*                                                                       */
/*************************************************************************/
static unsigned short  vgaCrcTable_High[256] =
{
  0x0000, 0x7707, 0xEE0E, 0x9909, 0x076D, 0x706A, 0xE963, 0x9E64,
  0x0EDB, 0x79DC, 0xE0D5, 0x97D2, 0x09B6, 0x7EB1, 0xE7B8, 0x90BF,
  0x1DB7, 0x6AB0, 0xF3B9, 0x84BE, 0x1ADA, 0x6DDD, 0xF4D4, 0x83D3,
  0x136C, 0x646B, 0xFD62, 0x8A65, 0x1401, 0x6306, 0xFA0F, 0x8D08,
  0x3B6E, 0x4C69, 0xD560, 0xA267, 0x3C03, 0x4B04, 0xD20D, 0xA50A,
  0x35B5, 0x42B2, 0xDBBB, 0xACBC, 0x32D8, 0x45DF, 0xDCD6, 0xABD1,
  0x26D9, 0x51DE, 0xC8D7, 0xBFD0, 0x21B4, 0x56B3, 0xCFBA, 0xB8BD,
  0x2802, 0x5F05, 0xC60C, 0xB10B, 0x2F6F, 0x5868, 0xC161, 0xB666,
  0x76DC, 0x01DB, 0x98D2, 0xEFD5, 0x71B1, 0x06B6, 0x9FBF, 0xE8B8,
  0x7807, 0x0F00, 0x9609, 0xE10E, 0x7F6A, 0x086D, 0x9164, 0xE663,
  0x6B6B, 0x1C6C, 0x8565, 0xF262, 0x6C06, 0x1B01, 0x8208, 0xF50F,
  0x65B0, 0x12B7, 0x8BBE, 0xFCB9, 0x62DD, 0x15DA, 0x8CD3, 0xFBD4,
  0x4DB2, 0x3AB5, 0xA3BC, 0xD4BB, 0x4ADF, 0x3DD8, 0xA4D1, 0xD3D6,
  0x4369, 0x346E, 0xAD67, 0xDA60, 0x4404, 0x3303, 0xAA0A, 0xDD0D,
  0x5005, 0x2702, 0xBE0B, 0xC90C, 0x5768, 0x206F, 0xB966, 0xCE61,
  0x5EDE, 0x29D9, 0xB0D0, 0xC7D7, 0x59B3, 0x2EB4, 0xB7BD, 0xC0BA,
  0xEDB8, 0x9ABF, 0x03B6, 0x74B1, 0xEAD5, 0x9DD2, 0x04DB, 0x73DC,
  0xE363, 0x9464, 0x0D6D, 0x7A6A, 0xE40E, 0x9309, 0x0A00, 0x7D07,
  0xF00F, 0x8708, 0x1E01, 0x6906, 0xF762, 0x8065, 0x196C, 0x6E6B,
  0xFED4, 0x89D3, 0x10DA, 0x67DD, 0xF9B9, 0x8EBE, 0x17B7, 0x60B0,
  0xD6D6, 0xA1D1, 0x38D8, 0x4FDF, 0xD1BB, 0xA6BC, 0x3FB5, 0x48B2,
  0xD80D, 0xAF0A, 0x3603, 0x4104, 0xDF60, 0xA867, 0x316E, 0x4669,
  0xCB61, 0xBC66, 0x256F, 0x5268, 0xCC0C, 0xBB0B, 0x2202, 0x5505,
  0xC5BA, 0xB2BD, 0x2BB4, 0x5CB3, 0xC2D7, 0xB5D0, 0x2CD9, 0x5BDE,
  0x9B64, 0xEC63, 0x756A, 0x026D, 0x9C09, 0xEB0E, 0x7207, 0x0500,
  0x95BF, 0xE2B8, 0x7BB1, 0x0CB6, 0x92D2, 0xE5D5, 0x7CDC, 0x0BDB,
  0x86D3, 0xF1D4, 0x68DD, 0x1FDA, 0x81BE, 0xF6B9, 0x6FB0, 0x18B7,
  0x8808, 0xFF0F, 0x6606, 0x1101, 0x8F65, 0xF862, 0x616B, 0x166C,
  0xA00A, 0xD70D, 0x4E04, 0x3903, 0xA767, 0xD060, 0x4969, 0x3E6E,
  0xAED1, 0xD9D6, 0x40DF, 0x37D8, 0xA9BC, 0xDEBB, 0x47B2, 0x30B5,
  0xBDBD, 0xCABA, 0x53B3, 0x24B4, 0xBAD0, 0xCDD7, 0x54DE, 0x23D9,
  0xB366, 0xC461, 0x5D68, 0x2A6F, 0xB40B, 0xC30C, 0x5A05, 0x2D02
};

static unsigned short  vgaCrcTable_Low[256] =
{
  0x0000, 0x3096, 0x612C, 0x51BA, 0xC419, 0xF48F, 0xA535, 0x95A3,
  0x8832, 0xB8A4, 0xE91E, 0xD988, 0x4C2B, 0x7CBD, 0x2D07, 0x1D91,
  0x1064, 0x20F2, 0x7148, 0x41DE, 0xD47D, 0xE4EB, 0xB551, 0x85C7,
  0x9856, 0xA8C0, 0xF97A, 0xC9EC, 0x5C4F, 0x6CD9, 0x3D63, 0x0DF5,
  0x20C8, 0x105E, 0x41E4, 0x7172, 0xE4D1, 0xD447, 0x85FD, 0xB56B,
  0xA8FA, 0x986C, 0xC9D6, 0xF940, 0x6CE3, 0x5C75, 0x0DCF, 0x3D59,
  0x30AC, 0x003A, 0x5180, 0x6116, 0xF4B5, 0xC423, 0x9599, 0xA50F,
  0xB89E, 0x8808, 0xD9B2, 0xE924, 0x7C87, 0x4C11, 0x1DAB, 0x2D3D,
  0x4190, 0x7106, 0x20BC, 0x102A, 0x8589, 0xB51F, 0xE4A5, 0xD433,
  0xC9A2, 0xF934, 0xA88E, 0x9818, 0x0DBB, 0x3D2D, 0x6C97, 0x5C01,
  0x51F4, 0x6162, 0x30D8, 0x004E, 0x95ED, 0xA57B, 0xF4C1, 0xC457,
  0xD9C6, 0xE950, 0xB8EA, 0x887C, 0x1DDF, 0x2D49, 0x7CF3, 0x4C65,
  0x6158, 0x51CE, 0x0074, 0x30E2, 0xA541, 0x95D7, 0xC46D, 0xF4FB,
  0xE96A, 0xD9FC, 0x8846, 0xB8D0, 0x2D73, 0x1DE5, 0x4C5F, 0x7CC9,
  0x713C, 0x41AA, 0x1010, 0x2086, 0xB525, 0x85B3, 0xD409, 0xE49F,
  0xF90E, 0xC998, 0x9822, 0xA8B4, 0x3D17, 0x0D81, 0x5C3B, 0x6CAD,
  0x8320, 0xB3B6, 0xE20C, 0xD29A, 0x4739, 0x77AF, 0x2615, 0x1683,
  0x0B12, 0x3B84, 0x6A3E, 0x5AA8, 0xCF0B, 0xFF9D, 0xAE27, 0x9EB1,
  0x9344, 0xA3D2, 0xF268, 0xC2FE, 0x575D, 0x67CB, 0x3671, 0x06E7,
  0x1B76, 0x2BE0, 0x7A5A, 0x4ACC, 0xDF6F, 0xEFF9, 0xBE43, 0x8ED5,
  0xA3E8, 0x937E, 0xC2C4, 0xF252, 0x67F1, 0x5767, 0x06DD, 0x364B,
  0x2BDA, 0x1B4C, 0x4AF6, 0x7A60, 0xEFC3, 0xDF55, 0x8EEF, 0xBE79,
  0xB38C, 0x831A, 0xD2A0, 0xE236, 0x7795, 0x4703, 0x16B9, 0x262F,
  0x3BBE, 0x0B28, 0x5A92, 0x6A04, 0xFFA7, 0xCF31, 0x9E8B, 0xAE1D,
  0xC2B0, 0xF226, 0xA39C, 0x930A, 0x06A9, 0x363F, 0x6785, 0x5713,
  0x4A82, 0x7A14, 0x2BAE, 0x1B38, 0x8E9B, 0xBE0D, 0xEFB7, 0xDF21,
  0xD2D4, 0xE242, 0xB3F8, 0x836E, 0x16CD, 0x265B, 0x77E1, 0x4777,
  0x5AE6, 0x6A70, 0x3BCA, 0x0B5C, 0x9EFF, 0xAE69, 0xFFD3, 0xCF45,
  0xE278, 0xD2EE, 0x8354, 0xB3C2, 0x2661, 0x16F7, 0x474D, 0x77DB,
  0x6A4A, 0x5ADC, 0x0B66, 0x3BF0, 0xAE53, 0x9EC5, 0xCF7F, 0xFFE9,
  0xF21C, 0xC28A, 0x9330, 0xA3A6, 0x3605, 0x0693, 0x5729, 0x67BF,
  0x7A2E, 0x4AB8, 0x1B02, 0x2B94, 0xBE37, 0x8EA1, 0xDF1B, 0xEF8D
};

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/

/************************************************************************/
/*                   End of CRC Lookup Table                            */
/************************************************************************/

/******************************************************************************
* FUNCTION: PDD_ValidationCheckFile()
*
* DESCRIPTION: check the file with magic,crc32 and version
*
* PARAMETERS:
*      PpBufferFile: buffer of the data with header
*      Ps32Size:     size of the buffer
*      Pu32Version:  version number of the datastream
*
* RETURNS: 
*      TRUE: file valid
*      FALSE:file invalid  
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
tBool PDD_ValidationCheckFile(void* PpBufferFile,tS32 Ps32Size, tU32 Pu32Version)
{
  tBool            VbValid=FALSE;
  tU32             Vu32NewChecksum=0;
  tS32             Vs32SizeHeader=sizeof(tsPddFileHeader);
  tU8*             Vu8pBufferFile=(tU8*)PpBufferFile;
  tsPddFileHeader* VtspHeader=(tsPddFileHeader*) PpBufferFile;
  //structure ->
  //Data[0-3]: Version
  //Data[4-7]: Magic
  //Data[8-12]:Checksum
  //Data Entry 1 ....
  /* get new checksum*/
  Vu32NewChecksum= PDD_ValidationCalcCrc32(Vu8pBufferFile+Vs32SizeHeader,Ps32Size-Vs32SizeHeader);	
  PDD_TRACE(PDD_VALIDATION_HEADER_FILE,VtspHeader,Vs32SizeHeader);
  PDD_TRACE(PDD_VALIDATION_CHECKSUM_FILE,&Vu32NewChecksum,sizeof(tU32));
  /* check file */
  if(   (VtspHeader->u32Version!=Pu32Version)
	  ||(VtspHeader->u32Magic!=PDD_C_DATA_STREAM_MAGIC)
	  ||((VtspHeader->u32Checksum!=Vu32NewChecksum)))
  {/* error no valid data*/
    tS32 Vs32Result=PDD_ERROR_VALIDATION_HEADER;  
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else
  {
    VbValid=TRUE;
  }
  return(VbValid);
}
/******************************************************************************
* FUNCTION: PDD_ValidationCheckScc()
*
* DESCRIPTION: check the buffer with crc8 and version
*
* PARAMETERS:
*      PpBufferFile: buffer of the data with header
*      Ps32Size:     size of the buffer
*      Pu32Version:  version number of the datastream
*
* RETURNS: 
*      TRUE: file valid
*      FALSE:file invalid  
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
tBool PDD_ValidationCheckScc(void* PpBufferFile,tS32 Ps32Size, tU32 Pu32Version)
{
  tBool            VbValid=FALSE;
  #ifdef PDD_SCC_POOL_EXIST
  tU32             Vu32NewChecksum=0;
  tS32             Vs32SizeHeader=sizeof(TPddScc_Header);
  tU8*             Vu8pBufferFile=(tU8*)PpBufferFile;  
  TPddScc_Header*  VtspHeader=(TPddScc_Header*) PpBufferFile;
  
  /* get new checksum*/
  Vu32NewChecksum= PDD_ValidationCalcCrc32(Vu8pBufferFile+Vs32SizeHeader,Ps32Size-Vs32SizeHeader);	
  PDD_TRACE(PDD_VALIDATION_HEADER_SCC,VtspHeader,Vs32SizeHeader);              
  PDD_TRACE(PDD_VALIDATION_CHECKSUM_SCC,&Vu32NewChecksum,sizeof(tU32));         
  /* check file */
  if(   (VtspHeader->u32VersionPool!= Pu32Version)
	  ||(VtspHeader->u32Checksum!=Vu32NewChecksum))
  {/* error no valid data*/
    tS32 Vs32Result=PDD_ERROR_VALIDATION_HEADER;  
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else
  {
    VbValid=TRUE;
  }
  #else
  /*for lint*/
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PpBufferFile); 
  PDD_PARAMETER_INTENTIONALLY_UNUSED(Ps32Size);
  PDD_PARAMETER_INTENTIONALLY_UNUSED(Pu32Version);
  #endif
  return(VbValid);
}
/******************************************************************************
* FUNCTION: PDD_ValidationCheckNorKernel()
*
* DESCRIPTION: check the file with magic,crc32 and version
*
* PARAMETERS:
*      PpBufferFile: buffer of the data with header
*      Ps32Size:     size of the buffer
*      Pu32Version:  version number of the datastream
*
* RETURNS: 
*      TRUE: file valid
*      FALSE:file invalid  
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
tBool PDD_ValidationCheckNorKernel(void* PpBuffer,tS32 Ps32Size, tU32 Pu32Version)
{
  tBool              VbValid=FALSE;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST  
  tU32               Vu32NewChecksum=0;
  tS32               Vs32SizeHeader=sizeof(TPddKernel_Header);
  tU8*               Vu8pBuffer=(tU8*)PpBuffer; 
  TPddKernel_Header* VtspHeader=(TPddKernel_Header*) PpBuffer;
  //structure ->
  //Data[0-3]: Version
  //Data[4-7]: Magic
  //Data[8-12]:Checksum
  //Data Entry 1 ....
  /* get new checksum*/
  Vu32NewChecksum= PDD_ValidationCalcCrc32(Vu8pBuffer+Vs32SizeHeader,Ps32Size-Vs32SizeHeader);	
  PDD_TRACE(PDD_VALIDATION_HEADER_NOR_KERNEL,&VtspHeader->HeaderStream,sizeof(TPddHeaderStream));
  PDD_TRACE(PDD_VALIDATION_CHECKSUM_NOR_KERNEL,&Vu32NewChecksum,sizeof(tU32));
  /* check file */
  if(   (VtspHeader->HeaderStream.Version!=Pu32Version)
	  ||(VtspHeader->HeaderStream.Checksum!=Vu32NewChecksum)
	  ||(VtspHeader->HeaderStream.ChecksumXor!=~Vu32NewChecksum)
	  ||(VtspHeader->HeaderStream.Magic!=PDD_NOR_KERNEL_MAGIC))
  {/* error no valid data*/
    tS32 Vs32Result=PDD_ERROR_VALIDATION_HEADER_NOR_KERNEL;  
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else
  {
    VbValid=TRUE;
  }  
  #else
  /*for lint*/
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PpBuffer); 
  PDD_PARAMETER_INTENTIONALLY_UNUSED(Ps32Size);
  PDD_PARAMETER_INTENTIONALLY_UNUSED(Pu32Version);
  #endif
  return(VbValid);
}
/******************************************************************************
* FUNCTION: PDD_ValidationGetHeaderFile()
*
* DESCRIPTION: get header of the buffer with crc32
*
* PARAMETERS:
*      PpBufferStream:       buffer of the data stream
*      Ps32LengthDataStream: length of the datastream
*      Pu32Version:          version number of the datastream
*      PvHeader:             pointer of the header
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
void  PDD_ValidationGetHeaderFile(tU8* PpBufferStream,tS32 Ps32LengthDataStream, tU32 Pu32Version,void* PvHeader)
{ 
  tsPddFileHeader* PtsHeader=(tsPddFileHeader*)PvHeader;
  PtsHeader->u32Version=Pu32Version;
  PtsHeader->u32Magic=PDD_C_DATA_STREAM_MAGIC;
  PtsHeader->u32Checksum=PDD_ValidationCalcCrc32(PpBufferStream,Ps32LengthDataStream);	
  PDD_TRACE(PDD_VALIDATION_HEADER_FILE,PtsHeader,sizeof(tsPddFileHeader));
}
/******************************************************************************
* FUNCTION: PDD_ValidationGetHeaderScc()
*
* DESCRIPTION: get header of the buffer with crc8
*
* PARAMETERS:
*      PpBufferStream:       buffer of the data stream
*      Ps32LengthDataStream: length of the datastream
*      Pu32Version:          version number of the datastream
*      PvHeader:             pointer of the header
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
void  PDD_ValidationGetHeaderScc(tU8* PpBufferStream,tS32 Ps32LengthDataStream,tU32 Pu32Version,void* PvHeader)
{ 
  #ifdef PDD_SCC_POOL_EXIST
  TPddScc_Header* PtsHeader=(TPddScc_Header*)PvHeader;
  PtsHeader->u32VersionPool=Pu32Version;
  PtsHeader->u32Checksum=PDD_ValidationCalcCrc32(PpBufferStream,Ps32LengthDataStream);
  PtsHeader->u32Size=(tU32) Ps32LengthDataStream;
  PDD_TRACE(PDD_VALIDATION_HEADER_SCC,PtsHeader,sizeof(TPddScc_Header));
  #else
  /*for lint*/
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PpBufferStream); 
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PvHeader);
  PDD_PARAMETER_INTENTIONALLY_UNUSED(Ps32LengthDataStream);
  PDD_PARAMETER_INTENTIONALLY_UNUSED(Pu32Version);
  #endif
}
/******************************************************************************
* FUNCTION: PDD_ValidationGetHeaderScc()
*
* DESCRIPTION: get header of the buffer with crc8
*
* PARAMETERS:
*      PpBufferStream:       buffer of the data stream
*      Ps32LengthDataStream: length of the datastream
*      Pu32Version:          version number of the datastream
*      PvHeader:             pointer of the header
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
void  PDD_ValidationGetHeaderNorKernel(tU8* PpBufferStream,tS32 Ps32LengthDataStream, tU32 Pu32Version,void* PvHeader)
{
  #ifdef PDD_NOR_KERNEL_POOL_EXIST
  tU32 Vu32NewChecksum=0;
  TPddKernel_Header* PtsHeader=(TPddKernel_Header*)PvHeader;
  Vu32NewChecksum=PDD_ValidationCalcCrc32(PpBufferStream,Ps32LengthDataStream);
  memset(&PtsHeader->HeaderValid,0xff,sizeof(TPddHeaderValid));
  PtsHeader->HeaderValid.Valid=~PDD_NOR_KERNEL_STATE_VALID;
  PtsHeader->HeaderStream.Version=Pu32Version;  
  PtsHeader->HeaderStream.Checksum=Vu32NewChecksum;
  PtsHeader->HeaderStream.ChecksumXor=~Vu32NewChecksum;
  PtsHeader->HeaderStream.Size=(tU32) Ps32LengthDataStream+sizeof(TPddKernel_Header);
  PtsHeader->HeaderStream.Magic=PDD_NOR_KERNEL_MAGIC;
  PtsHeader->HeaderStream.ChunkSize=PDD_NorKernelAccessGetChunkSize();
  PDD_TRACE(PDD_VALIDATION_HEADER_NOR_KERNEL,&PtsHeader->HeaderStream,sizeof(TPddHeaderStream));
  #else
  /*for lint*/
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PpBufferStream); 
  PDD_PARAMETER_INTENTIONALLY_UNUSED(Ps32LengthDataStream);
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PvHeader);
  PDD_PARAMETER_INTENTIONALLY_UNUSED(Pu32Version)
  #endif
}
/******************************************************************************
* FUNCTION: PDD_ValidationCheckCrcEqualFile()
*
* DESCRIPTION: check if the CRC32 of backup file and normal file are equal
*
* PARAMETERS:
*      PpAdrBufferBackup: buffer backup file
*      PpAdrBufferNormal: buffer normal file
*
* RETURNS: 
*      TRUE:  CRC are equal
*      FALSE: CRC aren't equal 
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
tBool  PDD_ValidationCheckCrcEqualFile(const void* PpAdrBufferBackup,const void* PpAdrBufferNormal)
{
  tBool            VbReturn=TRUE;
  tsPddFileHeader* VtspHeaderBackup=(tsPddFileHeader*)PpAdrBufferBackup;
  tsPddFileHeader* VtspHeaderNormal=(tsPddFileHeader*)PpAdrBufferNormal;

  /*check CRC*/
  if(VtspHeaderNormal->u32Checksum!=VtspHeaderBackup->u32Checksum)
  {
    VbReturn=FALSE;
  }
  return(VbReturn);
}
/******************************************************************************
* FUNCTION: PDD_ValidationCheckCrcEqualScc()
*
* DESCRIPTION: check if the CRC8 of backup and normal are equal
*
* PARAMETERS:
*      PpAdrBufferBackup: buffer backup
*      PpAdrBufferNormal: buffer normal
*
* RETURNS: 
*      TRUE:  CRC are equal
*      FALSE: CRC aren't equal 
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
tBool  PDD_ValidationCheckCrcEqualScc(const void* PpAdrBufferBackup,const void* PpAdrBufferNormal)
{
  tBool            VbReturn=TRUE;
  #ifdef PDD_SCC_POOL_EXIST
  TPddScc_Header*  VtspHeaderBackup=(TPddScc_Header*)PpAdrBufferBackup;
  TPddScc_Header*  VtspHeaderNormal=(TPddScc_Header*)PpAdrBufferNormal;
   /*check CRC*/
  if(VtspHeaderNormal->u32Checksum!=VtspHeaderBackup->u32Checksum)
  {
    VbReturn=FALSE;
  }
  #else
  /*for lint*/
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PpAdrBufferBackup);
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PpAdrBufferNormal);
  #endif
  return(VbReturn);
}
/******************************************************************************
* FUNCTION: PDD_ValidationCheckCrcEqualFile()
*
* DESCRIPTION: check if the CRC32 of backup file and normal file are equal
*
* PARAMETERS:
*      PpAdrBufferBackup: buffer backup file
*      PpAdrBufferNormal: buffer normal file
*
* RETURNS: 
*      TRUE:  CRC are equal
*      FALSE: CRC aren't equal 
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
tBool  PDD_ValidationCheckCrcEqualNorKernel(const void* PpAdrBufferBackup,const void* PpAdrBufferNormal)
{
  tBool            VbReturn=TRUE;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST  
  TPddKernel_Header* VtspHeaderBackup=(TPddKernel_Header*)PpAdrBufferBackup;
  TPddKernel_Header* VtspHeaderNormal=(TPddKernel_Header*)PpAdrBufferNormal;

  /*check CRC*/
  if(VtspHeaderNormal->HeaderStream.Checksum!=VtspHeaderBackup->HeaderStream.Checksum)
  {
    VbReturn=FALSE;
  }
  #else
  /*for lint*/
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PpAdrBufferBackup);
  PDD_PARAMETER_INTENTIONALLY_UNUSED(PpAdrBufferNormal);
  #endif
  return(VbReturn);
}
/******************************************************FunctionHeaderBegin******
 * FUNCTION    : PDD_ValidationCalcCrc32
 * CREATED     : 2008-08-12
 * AUTHOR      : 
 * DESCRIPTION : function calculate a crc32
 *
 * ARGUMENTS   : PpAdrBuffer :  address buffer,
 *               Pu32Len:       lenght
 *            
 * RETURN VALUE: CRC
 *              
 * NOTES       : 
 *******************************************************FunctionHeaderEnd******/
/* Used by crc computation routine crc_calc */
#define INIT_VALUE                        0xFFFFFFFFL
#define XOROUT                            0xFFFFFFFFL
#define FLASH_BYTE_INFO                   5

tU32 PDD_ValidationCalcCrc32(const tU8* PpAdrBuffer,tU32 Pu32Len)
{
  unsigned short nCrcHigh;
  unsigned short nCrcLow;
  unsigned int  nTableOffset;
  unsigned int  nCrc32 = INIT_VALUE;
  const unsigned char* pBlkStartAdr = PpAdrBuffer;

  if(Pu32Len>0)
  {
    while(Pu32Len--)
    {
      if(pBlkStartAdr + FLASH_BYTE_INFO == PpAdrBuffer)
      { /*********************************************************/
        /* mark the check byte FLASH_BYTE_INFO as 0xFF           */
        /*********************************************************/
        nTableOffset = (nCrc32 ^ 0xFF) & 0xFFL;
      }
      else
      { // Tabellen-Offset berechnen
        nTableOffset = (nCrc32 ^ *PpAdrBuffer) & 0xFFL;
      }
      // 16-Bit-High-/Low-Teil der 32-Bit-CRC
      nCrcHigh = vgaCrcTable_High[(unsigned char)nTableOffset];
      nCrcLow = vgaCrcTable_Low[(unsigned char)nTableOffset];
      // Berechnen der 32-Bit-CRC
      nCrc32 = ((((unsigned int)nCrcHigh) << 16) + nCrcLow) ^ (nCrc32 >> 8);
      // Zeiger auf Daten eins weiter
      PpAdrBuffer++;
    }/*end while*/
    // CRC muss noch XORiert werden
    nCrc32 ^= XOROUT;
  }
  /* return CRC*/
  return(nCrc32);
}
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/* End of File pdd_vaildation.c                                                          */
/******************************************************************************/


