/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        pdd_access_scc.c
 *
 * 
 * global function:
 * -- PDD_SccAccessGetDataStreamSize:
 *      get the size of the data stream. 
 * -- PDD_SccAccessReadDataStream:
 *      read the datastream
 * -- PDD_SccAccessWriteDataStream:
 *      write the datastream 
 *
 * local function:
 * -- PDD_GetPortId:
 *      check if the datstream in the configuration and get the table id
 * -- PDD_SccSendMsg:
 *      send message to the socked
 * -- PDD_SccRecvMsg:
 *      recieve message the socked
 * --PDD_SccTruncateFrame:
 *      truncate pool for one element
 * --PDD_SccCompleteFrame:
 *      complet pool for one element
 *
 * @date        2013-10-12
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>   
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <poll.h>
#include "system_types.h"
#include "system_definition.h"
#include "inc.h"
#include "dgram_service.h"
#include "inc_ports.h"
#include "inc_scc_pdd.h"
#include "LFX2.h"
#include "pdd_variant.h"
#include "pdd.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#ifdef PDD_SCC_POOL_EXIST
#include "DataPoolSCC.h" 
#endif
#include "pdd_config_scc.h"


#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
/*ID for send and recieve message */
typedef enum
{
  PDD_SCC_COMPONENT_STATUS_MSG, 
  PDD_SCC_READ_DATA_POOL,
  PDD_SCC_WRITE_DATA_POOL,
  PDD_SCC_END_MSG
}tePddSccSendMsg;

/* maximun message size for the buffer*/
#define PDD_SCC_MAX_MSG_SIZE_WITH_TP     (SCC_PD_NET_MAX_LENGTH_DATA_BYTES+5)
/* 0.Byte:       TpMarker 
   1.Byte:       msgId
   2.Byte:       poolid
   3.+4.Byte:    length of data
   5.. Bytes:    data*/

/* position for length and message beginn vu8PddSccSendMsg */
#define PDD_SCC_TABLE_MSG_LEN             0
#define PDD_SCC_TABLE_MSG_BEGINN          1

/*position msg id */
#define PDD_SCC_POS_MSG_ID                0
#define PDD_SCC_POS_MSG_STATE             1

/*postion in msg write */
//#define PDD_SCC_POS_MSG_WRITE_TP_MARKER       0
//#define PDD_SCC_POS_MSG_WRITE_ID              1
#define PDD_SCC_POS_MSG_WRITE_POOL_ID         2
#define PDD_SCC_POS_MSG_WRITE_LEN_HIGH_BYTE   3
#define PDD_SCC_POS_MSG_WRITE_LEN_LOW_BYTE    4
#define PDD_SCC_POS_MSG_WRITE_DATA            5
/*postion in msg read */
//#define PDD_SCC_POS_MSG_READ_ID              0
#define PDD_SCC_POS_MSG_READ_POOL_ID         1
//#define PDD_SCC_POS_MSG_READ_LEN_HIGH_BYTE   2
//#define PDD_SCC_POS_MSG_READ_LEN_LOW_BYTE    3
#define PDD_SCC_POS_MSG_READ_DATA            4
/* retry counter for send message */
#define PDD_SCC_MAX_RETRY_SEND_MSG           3
/* timeout */
#define PDD_SCC_TIMEOUT_SEND              1     // 1 sec

/*PSARCC21-2530: NvM sometimes needs more than 5 seconds to perform
 a write operation. Hence the receieve timeout is increased to 7s */
#define PDD_SCC_MAX_TIMEOUT_RECEIVE           7000  // 7 sec
#define PDD_SCC_MIN_TIMEOUT_RECEIVE           1000  // 1 sec

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/
typedef struct
{
  tS32       s32Socket; 
  sk_dgram  *tsDgram; 
}tsPddSccAccessInfo;
/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/
static const tU8 vu8PddSccSendMsg[PDD_SCC_END_MSG][4]=
    { /* PDD_TEST_SCC_TABLE_MSG_LEN, PDD_TEST_SCC_TABLE_MSG_BEGINN:MsgId        ,data                             */
	    {0x03                        , SCC_PD_NET_C_COMPONENT_STATUS_MSGID        ,SCC_PD_NET_APPLICATION_STATUS_ACTIVE, SCC_PD_NET_VERSION},
	    {0x02                        , SCC_PD_NET_C_READ_DATA_POOL                ,0x00                                , 0x00},	
	    {0x03                        , SCC_PD_NET_TP_MARKER+2                     ,SCC_PD_NET_C_WRITE_DATA_POOL        , 0x00},
    };
static const tU8 vu8PddSccRecvMsg[PDD_SCC_END_MSG][4]=
    { /* PDD_TEST_SCC_TABLE_MSG_LEN, PDD_TEST_SCC_TABLE_MSG_BEGINN:MsgId         */
	    {0x03                        , SCC_PD_NET_R_COMPONENT_STATUS_MSGID            ,SCC_PD_NET_APPLICATION_STATUS_ACTIVE,SCC_PD_NET_VERSION },
	    {0x04                        , SCC_PD_NET_R_READ_DATA_POOL                    ,0x00,0x00},
	    {0x02                        , SCC_PD_NET_R_WRITE_DATA_POOL                   ,0x00,0x00},
    };

static tsPddSccAccessInfo      VtsSccAccess;
/************************************************************************
| variable definition (scope: module-global)
|-----------------------------------------------------------------------*/

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static tS32	 PDD_GetPortId(const char* PtsDataStreamName);
static tS32  PDD_SccAccessOpen(tsPddSccAccessInfo*,tS32);
static tS32  PDD_SccAccessClose(tsPddSccAccessInfo*);
static tS32	 PDD_SccSendMsg(tsPddSccAccessInfo* ,tePddSccSendMsg PeSendMsg,tU8,const tU8 *PBuffer,tS32 Ps32Size);
static tS32	 PDD_SccRecvMsg(tsPddSccAccessInfo* ,tePddSccSendMsg PeRecMsg,tU8,tU8 *PBuffer,tS32 Ps32Size);

/******************************************************************************
* FUNCTION: PDD_SccAccessGetDataStreamSize()
*
* DESCRIPTION: get the size of the data stream. 
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash
*
* RETURNS: 
*      positive value: size of data stream
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 04 16
*****************************************************************************/
tS32 PDD_SccAccessGetDataStreamSize(const char* PtsDataStreamName)
{ 
  tS32                           Vs32Length=PDD_OK;
  char*                          VstrPosFolder=strrchr((char*)PtsDataStreamName,'/');
  tS32                           Vs32Port;
  const tsPddSccConfig*          VtspSccConfig;
 
#ifdef PDD_SCC_POOL_EXIST
  /*check datstream with folder */
  if(VstrPosFolder!=NULL)
  {
    PtsDataStreamName=VstrPosFolder+1;
  }
  Vs32Port=PDD_GetPortId(PtsDataStreamName);
  /* check port id*/
  if(Vs32Port < 0)
  {/*error*/
    Vs32Length=PDD_ERROR_SCC_INVALID_DATA_STREAM;
	  PDD_TRACE(PDD_ERROR,&Vs32Length,sizeof(tS32));
  }
  else
  {
    VtspSccConfig=&vrPddSccConfig[Vs32Port];
	  PDD_TRACE(PDD_SCC_SIZE_WITH_HEADER,&VtspSccConfig->u32SizeData,sizeof(tU32)); 	
	  /*check the length*/
	  if(VtspSccConfig->u32SizeData > (tS32)SCC_PD_NET_MAX_LENGTH_DATA_BYTES)
	  {
	    Vs32Length=PDD_ERROR_SCC_INVALID_SIZE_OF_STREAM;
	    PDD_TRACE(PDD_ERROR,&Vs32Length,sizeof(tS32));
	  }	
	  else
	  {
	    Vs32Length=(tS32)VtspSccConfig->u32SizeData;
	  }
  }/*end else*/
#endif
  return(Vs32Length);
}
/******************************************************************************
* FUNCTION: PDD_SccAccessReadDataStream()
*
* DESCRIPTION: read the datastream  
*
* PARAMETERS:
*      PtsDataStreamName : name of the data stream
*      Ppu8ReadBuffer    : read buffer
*      Ps32SizeReadBuffer: size read buffer
*
* RETURNS: 
*      positive value: size of data stream which read
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 04 16
*****************************************************************************/
tS32 PDD_SccAccessReadDataStream(const char* PtsDataStreamName,const void *Ppu8ReadBuffer,tS32 Ps32SizeReadBuffer)
{
  tS32                           Vs32Result=PDD_OK;
  char*                          VstrPosFolder=strrchr((char*)PtsDataStreamName,'/');
  tS32                           Vs32Port;
  const tsPddSccConfig*          VtspSccConfig;

  /*check datstream with folder */
  if(VstrPosFolder!=NULL)
  {
    PtsDataStreamName=VstrPosFolder+1;
  }
  Vs32Port=PDD_GetPortId(PtsDataStreamName);
  /* check port id*/
  if(Vs32Port < 0)
  {/*error*/
    Vs32Result=PDD_ERROR_SCC_INVALID_DATA_STREAM;
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else if((Ppu8ReadBuffer==NULL)||(Ps32SizeReadBuffer==0))
  {
	  Vs32Result=PDD_ERROR_WRONG_PARAMETER;
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else
  {	/* get pointer for port*/
	  VtspSccConfig=&vrPddSccConfig[Vs32Port];/*for kind save*/
	  /* open access */
    Vs32Result=PDD_SccAccessOpen(&VtsSccAccess,Vs32Port);
	  if(Vs32Result==PDD_OK)
	  { /* check if PDD_SCC_COMPONENT_STATUS_MSG already send*/
	    if (bPDD_SccGetFlagSendComponentStatusMsgDone()==FALSE)
	    { /* ---- send component status activ*/	 
	      Vs32Result=PDD_SccSendMsg(&VtsSccAccess,PDD_SCC_COMPONENT_STATUS_MSG,0,NULL,0);
	      if(Vs32Result==PDD_OK)
	      {/* get and check message from component*/	   
	        Vs32Result=PDD_SccRecvMsg(&VtsSccAccess,PDD_SCC_COMPONENT_STATUS_MSG,0,NULL,0);
		      if(Vs32Result==PDD_OK)
		      {
		        vPDD_SccSetFlagSendComponentStatusMsgDone(TRUE);
          }
        }
      }	  
	    /* ---- send message read */
	    if(Vs32Result==PDD_OK)
	    {/* check which kind to store*/
		    tBool            VbRetryCount=0;
		    do
		    {
		      Vs32Result=PDD_SccSendMsg(&VtsSccAccess,PDD_SCC_READ_DATA_POOL,VtspSccConfig->u8KindPool,NULL,0);
	        if(Vs32Result==PDD_OK)
	        {/*receive message*/		
		        Vs32Result=PDD_SccRecvMsg(&VtsSccAccess,PDD_SCC_READ_DATA_POOL,VtspSccConfig->u8KindPool,(tU8*)Ppu8ReadBuffer,Ps32SizeReadBuffer);			 
          }
		      VbRetryCount++;
        }while((VbRetryCount<PDD_SCC_MAX_RETRY_SEND_MSG)&&(Vs32Result<PDD_OK));
      }     
    }
	  PDD_SccAccessClose(&VtsSccAccess);
  }
  /* return*/
  return(Vs32Result);
}

/******************************************************************************
* FUNCTION: PDD_SccAccessWriteDataStream()
*
* DESCRIPTION: write the datastream 
*
* PARAMETERS:
*      PtsDataStreamName  : name of the data stream
*      Ppu8WriteBuffer    : write buffer
*      Ps32SizeWriteBuffer: size of write buffer
*
* RETURNS: 
*      positive value: size of data which is written
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 04 16
*****************************************************************************/
tS32 PDD_SccAccessWriteDataStream(const char* PtsDataStreamName,void *Ppu8WriteBuffer,tS32 Ps32SizeWriteBuffer)
{
  tS32                           Vs32Result=PDD_OK; 
  tS32                           Vs32Port;  
  char*                          VstrPosFolder=strrchr((char*)PtsDataStreamName,'/');
  const tsPddSccConfig*          VtspSccConfig;
   
  /*check datastream with folder */
  if(VstrPosFolder!=NULL)
  {
    PtsDataStreamName=VstrPosFolder+1;
  }
  Vs32Port=PDD_GetPortId(PtsDataStreamName);
  /* check port id*/
  if(Vs32Port < 0)
  {/*error*/
    Vs32Result=PDD_ERROR_SCC_INVALID_DATA_STREAM;
    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else if((Ppu8WriteBuffer==NULL)||(Ps32SizeWriteBuffer==0))
  {
    Vs32Result=PDD_ERROR_WRONG_PARAMETER;
	  PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else
  {  /* get pointer for port*/
	  VtspSccConfig=&vrPddSccConfig[Vs32Port];/*for kind save*/	 
	  /* check size; size not greater as defined in XML configuration*/
    if(Ps32SizeWriteBuffer> (tS32)VtspSccConfig->u32SizeData)
	  {
	    Vs32Result=PDD_ERROR_SCC_SEND_POOL_LENGTH_TO_GREAT;
	    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
    }
	  else
	  { /* open access */	    
      Vs32Result=PDD_SccAccessOpen(&VtsSccAccess,Vs32Port);
	    if(Vs32Result==PDD_OK)
	    { /* check if PDD_SCC_COMPONENT_STATUS_MSG already send*/
	      if (bPDD_SccGetFlagSendComponentStatusMsgDone()==FALSE)
	      {/* ---- send component status activ*/
	        Vs32Result=PDD_SccSendMsg(&VtsSccAccess,PDD_SCC_COMPONENT_STATUS_MSG,0,NULL,0);
	        if(Vs32Result==PDD_OK)
	        {/* get and check message from component*/		 
	          Vs32Result=PDD_SccRecvMsg(&VtsSccAccess,PDD_SCC_COMPONENT_STATUS_MSG,0,NULL,0);
			      if(Vs32Result==PDD_OK)
			      {
			        vPDD_SccSetFlagSendComponentStatusMsgDone(TRUE);
            }
          }
        }
	      /* ---- send message write */
	      if(Vs32Result==PDD_OK)
	      {/* check which kind to store*/
		      tBool            VbRetryCount=0;	     
		      do
		      {
		        Vs32Result=PDD_SccSendMsg(&VtsSccAccess,PDD_SCC_WRITE_DATA_POOL,VtspSccConfig->u8KindPool,Ppu8WriteBuffer,Ps32SizeWriteBuffer);
	          if(Vs32Result==PDD_OK)
	          {/*receive message*/			
		          Vs32Result=PDD_SccRecvMsg(&VtsSccAccess,PDD_SCC_WRITE_DATA_POOL,VtspSccConfig->u8KindPool,NULL,0);          
		          if(Vs32Result==PDD_OK)
		          {
		            Vs32Result=Ps32SizeWriteBuffer;
		          }/*end if success PDD_SccRecvMsg()*/
				  else if(Vs32Result == PDD_ERROR_SCC_POLL_TIMEOUT )
				  	break;
            }/*end if success PDD_SccSendMsg*/          
		        VbRetryCount++;
          }while((VbRetryCount<PDD_SCC_MAX_RETRY_SEND_MSG)&&(Vs32Result<PDD_OK));
        }/*end if success status message send and receive*/	   
      }/*end if success PDD_SccAccessOpen*/	  
	    PDD_SccAccessClose(&VtsSccAccess);
    }/*end else size OK*/
  }/*end else parameter OK*/
  if(Vs32Result<0)
  {/*error memory entry*/
    tU8 Vu8Buffer[200];		
    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"write data stream '%s' to SCC(V850) fails; error code: %d",PtsDataStreamName,Vs32Result); 
		PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
  }  
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_GetPortId()
*
* DESCRIPTION: check if the datstream in the configuration and get the table id
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*
* RETURNS: 
*      positive value: size of data stream
*      negative value: error  
*
* HISTORY:Created by Andrea Bueter 2013 04 19
*****************************************************************************/
static tS32 PDD_GetPortId(const char* PtsDataStreamName)
{
  tS32                    Vs32TableInfoCount=0;
  tS32                    Vs32TableInfoReturn=-1;
  const tsPddSccConfig*   VtspSccConfig;

  /*for each component*/
  for(Vs32TableInfoCount=0;Vs32TableInfoCount < PDD_SCC_TABLE_INFO_END;Vs32TableInfoCount++)
  {
	  VtspSccConfig=&vrPddSccConfig[Vs32TableInfoCount];
    if(memcmp(VtspSccConfig->u8Filename,PtsDataStreamName,strlen(VtspSccConfig->u8Filename))==0)
	  {
	    Vs32TableInfoReturn=Vs32TableInfoCount;
	  }
  }
  return(Vs32TableInfoReturn);
}
/******************************************************************************
* FUNCTION: PDD_SccAccessOpen();
*
* DESCRIPTION: 
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2013 04 16
*****************************************************************************/
static tS32  PDD_SccAccessOpen(tsPddSccAccessInfo* PtsAccessInfo,tS32 Ps32Port)
{ 
  tS32                    Vs32Result=PDD_OK;
  struct hostent         *VsLocal, *VsRemote;
  struct sockaddr_in      VsLocalAddr, VsRemoteAddr;
  struct timeval          VrSocketSendTimeval;
  const  tsPddSccConfig*  VtspSccConfig=&vrPddSccConfig[Ps32Port];
 
  #ifndef PDD_UNIT_TEST
  /*Create socket*/
  PtsAccessInfo->s32Socket = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM /*SOCK_NONBLOCK??*/, 0); /* use AF_BOSCH_SCC_AUTOSAR to facilitate eventual transition to TCP/IP */
  if (PtsAccessInfo->s32Socket < 0)
  { /*error */
    Vs32Result=PDD_ERROR_SCC_CALL_SOCKET;
    PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
  }
  else
  { /*Initialize datagram service*/
    PtsAccessInfo->tsDgram = dgram_init(PtsAccessInfo->s32Socket, DGRAM_MAX, NULL);
    if (PtsAccessInfo->tsDgram == NULL)
    { /*error */
      Vs32Result=PDD_ERROR_SCC_CALL_DGRAM_INIT;
      PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
    }
	  else
	  { /* Get local address and port */
      VsLocal = gethostbyname("scc-local");
	    if(VsLocal==NULL)
	    {/*error */
        Vs32Result=PDD_ERROR_SCC_CALL_GET_HOST_BY_NAME;
	      PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));		  
	    }
	    else
	    {
        VsLocalAddr.sin_family = AF_INET;
        memmove((char *) &VsLocalAddr.sin_addr.s_addr, (char *) VsLocal->h_addr, VsLocal->h_length);
        VsLocalAddr.sin_port = htons(VtspSccConfig->u32PDUPort); 
        /* Get remote address and port */
        VsRemote = gethostbyname("scc");
        VsRemoteAddr.sin_family = AF_INET;
        memmove((char *) &VsRemoteAddr.sin_addr.s_addr, (char *) VsRemote->h_addr, VsRemote->h_length);
        VsRemoteAddr.sin_port = htons(VtspSccConfig->u32PDUPort);    
		    // 24.03.2015 Set socket option "SO_SNDTIMEO" for dgram_send 
		    // Sets the timeout value specifying the amount of time that an output function blocks because flow control prevents data from being sent. 
	      VrSocketSendTimeval.tv_sec  = PDD_SCC_TIMEOUT_SEND; // 1 sec
	      VrSocketSendTimeval.tv_usec = 0;                   // 0 us
        if (setsockopt(PtsAccessInfo->s32Socket, SOL_SOCKET, SO_SNDTIMEO, (char*)&VrSocketSendTimeval, sizeof(VrSocketSendTimeval)) == -1) 
		    {
		      Vs32Result=PDD_ERROR_SCC_CALL_SOCK_OPT;
	        PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
	      }
		    else
		    { /*Bind socket to local address/port*/
		      /*Deactivation accepted. Authorization string is authorized LINT-deactivation #365.*/
          Vs32Result = bind(PtsAccessInfo->s32Socket, (struct sockaddr *) &VsLocalAddr, sizeof(VsLocalAddr)); /*lint !e64*/
          if (Vs32Result < 0)
          {
	          Vs32Result=PDD_ERROR_SCC_CALL_BIND;
	          PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
          }
	        else
	        {/* Connect to remote address/port */
		        /*Deactivation accepted. Authorization string is authorized LINT-deactivation #365.*/
            Vs32Result = connect(PtsAccessInfo->s32Socket, (struct sockaddr *) &VsRemoteAddr, sizeof(VsRemoteAddr)); /*lint !e64*/ 
            if (Vs32Result < 0)
            {
		          Vs32Result=PDD_ERROR_SCC_CALL_CONNECT;
		          PDD_TRACE(PDD_ERROR,&Vs32Result,sizeof(tS32));
            }/*end if*/	
		        else
		        {/*read possible old data from SCC once after startup (Jira: CMG3G-4853)*/
		          if(bPDD_SccGetReadOldDataAfterStartup()==FALSE)
			        {
		            struct pollfd VsFds[1];
			          tU8   Vu8RecvBuf[PDD_SCC_MAX_MSG_SIZE_WITH_TP];
			          tS32  Vs32ErrPoll;
                VsFds[0].fd=PtsAccessInfo->s32Socket;
                VsFds[0].events = POLLIN;
                Vs32ErrPoll=poll(VsFds, 1, 2 /*ms*/);
                /*check error*/
                if((Vs32ErrPoll<=0)||(VsFds[0].revents != POLLIN))
                {
			            //tU8 Vu8Buffer[80];		
		              //snprintf(Vu8Buffer,sizeof(Vu8Buffer),"PDD_SccAccessOpen: nothing to read %d\n",Vs32ErrPoll); 
		              //PDD_TRACE(PDD_INFO,Vu8Buffer,strlen(Vu8Buffer)+1);			            
                }
			          else
                {
                  if((dgram_recv(PtsAccessInfo->tsDgram,&Vu8RecvBuf[0], PDD_SCC_MAX_MSG_SIZE_WITH_TP)>0))
			            {
				            PDD_TRACE(PDD_INFO,"PDD_SccAccessOpen: read old data from socket\n",strlen("PDD_SccAccessOpen: read old data from socket\n")+1);					      
			            }			   
                }
                vPDD_SccSetReadOldDataAfterStartup(TRUE);
              }
            }
	        }/*end else bind() OK*/
        }
      }/*end else gethostname*/
    }/*end else dgram_init() OK*/
  }/*end else socket() OK*/
  #endif
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_SccClose()
*
* DESCRIPTION: 
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2013 04 16
*****************************************************************************/
static tS32  PDD_SccAccessClose(tsPddSccAccessInfo* PtsAccessInfo)
{
  tS32  Vs32Result=PDD_OK;
  if(PtsAccessInfo->tsDgram!=NULL)
  { /*exit drgram*/
    Vs32Result=dgram_exit(PtsAccessInfo->tsDgram);
	  if(Vs32Result == 0)
	  {
	    PtsAccessInfo->tsDgram=NULL;
      /*Close socket */
	    if(PtsAccessInfo->s32Socket>=0)
	    {
        close(PtsAccessInfo->s32Socket);
		    PtsAccessInfo->s32Socket=-1;
      }
    }
  }
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_SccSendMsg()
*
* DESCRIPTION: send message to the socked
*
* PARAMETERS:
*        PtspPddScc:  pointer to the configuration info of the data set
*        PeSendMsg:   kind message
*        PBuffer:     send buffer for messages with data
*        Ps32Size:    size of the data for messages with data
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
static  tS32  PDD_SccSendMsg(tsPddSccAccessInfo* PtspPddSccAccess,tePddSccSendMsg PeSendMsg,tU8 PubPoolNumber,const tU8 *PBuffer,tS32 Ps32Size)
{
  tS32  Vs32ReturnCode=PDD_OK;
  tU8   Vu8SendBuf[PDD_SCC_MAX_MSG_SIZE_WITH_TP];
  tU32  Vu32Length=vu8PddSccSendMsg[PeSendMsg][PDD_SCC_TABLE_MSG_LEN];  /*header length with poolid*/

  /* clear buffer */
  memset(&Vu8SendBuf[0],0x00,PDD_SCC_MAX_MSG_SIZE_WITH_TP);
  /*copy header message*/
  memmove(&Vu8SendBuf[0],&vu8PddSccSendMsg[PeSendMsg][PDD_SCC_TABLE_MSG_BEGINN],Vu32Length);
  /*set poolid */
  if(PeSendMsg==PDD_SCC_WRITE_DATA_POOL)
  {
    Vu8SendBuf[PDD_SCC_POS_MSG_WRITE_POOL_ID]=PubPoolNumber;
	  Vu8SendBuf[PDD_SCC_POS_MSG_WRITE_LEN_HIGH_BYTE]=(tU8) (Ps32Size>>8);
	  Vu8SendBuf[PDD_SCC_POS_MSG_WRITE_LEN_LOW_BYTE]=(tU8)(Ps32Size&0xFF);
	  Vu32Length+=2;
	  /*check length*/
	  if((Ps32Size+Vu32Length) > SCC_PD_NET_MAX_LENGTH_DATA_BYTES)
	  {
	    Vs32ReturnCode=PDD_ERROR_WRONG_PARAMETER;
	    PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));
    }
	  else
	  { /*copy data*/
      memmove(&Vu8SendBuf[PDD_SCC_POS_MSG_WRITE_DATA],PBuffer,Ps32Size); 		
	    /*store length*/
	    Vu32Length+=(tU32) Ps32Size;  
    }
  }
  else if(PeSendMsg==PDD_SCC_READ_DATA_POOL)
  {
    Vu8SendBuf[PDD_SCC_POS_MSG_READ_POOL_ID]=PubPoolNumber;
  }
  if(Vs32ReturnCode==PDD_OK)
  {/* send message*/
    PDD_TRACE(PDD_SCC_SEND_MESSAGE,&Vu8SendBuf[0],Vu32Length);
    Vs32ReturnCode=dgram_send(PtspPddSccAccess->tsDgram,&Vu8SendBuf[0],Vu32Length);
    /* trace send message*/
    PDD_TRACE(PDD_SCC_SEND_RETURN_DGRAM,&Vs32ReturnCode,sizeof(tS32));  
    if(Vs32ReturnCode < 0)
    {
      Vs32ReturnCode=PDD_ERROR_SCC_DGRAM_SEND;
      PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));
    }
    else
    {  /* no error */
      Vs32ReturnCode=PDD_OK;  
    }
  }
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: PDD_SccRecvMsg()
*
* DESCRIPTION: recieve message the socked
*
* PARAMETERS:
*        PtspPddScc:  pointer to the configuration info of the data set
*        PeRecMsg:    kind message
*        PBuffer:     recieve buffer for messages with data
*        Ps32Size:    size of the data for messages with data
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
static tS32  PDD_SccRecvMsg(tsPddSccAccessInfo* PtspPddSccAccess,tePddSccSendMsg PeRecMsg,tU8 PubPoolNumber,tU8 *PBuffer,tS32 Ps32Size)
{
  tU32  Vu32Timeout; 
  tS32  Vs32ReturnCode=PDD_OK;
  tU8   Vu8RecvBuf[PDD_SCC_MAX_MSG_SIZE_WITH_TP];
  tU32  Vu32LengthExpected=(tU32)Ps32Size+vu8PddSccRecvMsg[PeRecMsg][PDD_SCC_TABLE_MSG_LEN];
  tU8   Vu8MsgIDExpected=vu8PddSccRecvMsg[PeRecMsg][PDD_SCC_TABLE_MSG_BEGINN];
  struct pollfd VsFds[1];
  
  if( PeRecMsg == PDD_SCC_WRITE_DATA_POOL)
  	  Vu32Timeout= PDD_SCC_MAX_TIMEOUT_RECEIVE; //7 sec
  else 
  	  Vu32Timeout= PDD_SCC_MIN_TIMEOUT_RECEIVE; // 1 sec 
  	  
  
  VsFds[0].fd=PtspPddSccAccess->s32Socket;
  VsFds[0].events = POLLIN;

  PDD_TRACE(PDD_SCC_BEGINN_POLL,&Vu32Timeout,sizeof(tU32));
  Vs32ReturnCode = poll(VsFds, 1, Vu32Timeout);
  /*check error*/
  if(Vs32ReturnCode<0)
  {
	  Vs32ReturnCode=PDD_ERROR_SCC_POLL;
    PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));
  }
  else if(Vs32ReturnCode==0)
  {
	  Vs32ReturnCode=PDD_ERROR_SCC_POLL_TIMEOUT;
    PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32)); 
  }
  else if(VsFds[0].revents != POLLIN)
  {
    Vs32ReturnCode=PDD_ERROR_SCC_WRONG_EVENT;
    PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));   
  }
  else
  {
    PDD_TRACE(PDD_SCC_LENGTH_DATA_EXPECTED,&Vu32LengthExpected,sizeof(tS32));
    Vs32ReturnCode=dgram_recv(PtspPddSccAccess->tsDgram,&Vu8RecvBuf[0], Vu32LengthExpected);
	  /* check error */
    PDD_TRACE(PDD_SCC_RECV_RETURN_DGRAM,&Vs32ReturnCode,sizeof(tS32));
    if (Vs32ReturnCode < 0)
    {
      Vs32ReturnCode=PDD_ERROR_SCC_DGRAM_RECV;
      PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));   
    }
    else
    {
      if(Vu8MsgIDExpected!=Vu8RecvBuf[PDD_SCC_POS_MSG_ID])
      {/*error*/
	      PDD_TRACE(PDD_SCC_RECV_MESSAGE,&Vu8RecvBuf[0],Vs32ReturnCode);
        Vs32ReturnCode=PDD_ERROR_SCC_RECV_MSG_NOT_EXPECTED;
	      PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32)); 
        /*error memory entry => if reject version  mismatch*/             
        if((Vu8RecvBuf[PDD_SCC_POS_MSG_ID]==SCC_PD_NET_R_REJECT_MSGID)&&(Vu8RecvBuf[PDD_SCC_POS_MSG_READ_POOL_ID]==SCC_PD_NET_REJ_VERSION_MISMATCH))
        {
          tU8 Vu8Buffer[200];		
          snprintf(Vu8Buffer,sizeof(Vu8Buffer),"PDD_SccRecvMsg: version mismatch for pool number %d ",PubPoolNumber); 
		      PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
        }       
      }
      else
      {  /* trace out message */
	       PDD_TRACE(PDD_SCC_RECV_MESSAGE,&Vu8RecvBuf[0],Vs32ReturnCode);
         switch(Vu8RecvBuf[PDD_SCC_POS_MSG_ID])
         {
           case SCC_PD_NET_R_COMPONENT_STATUS_MSGID:
	         { /*check if component activ or inactiv*/
	           if(Vu8RecvBuf[PDD_SCC_POS_MSG_STATE]==SCC_PD_NET_APPLICATION_STATUS_INACTIVE)
		         {/*error*/
		           Vs32ReturnCode=PDD_ERROR_SCC_APPLICATION_INACTIVE;
	             PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));
             }
		         else
		         {
		           Vs32ReturnCode=PDD_OK;
             }
           }break;      
		       case SCC_PD_NET_R_READ_DATA_POOL:
	         {/* check correct pool*/
			       if(Vu8RecvBuf[PDD_SCC_POS_MSG_READ_POOL_ID]!=PubPoolNumber)
			       {
			         Vs32ReturnCode=PDD_ERROR_SCC_RECV_DATAPOOL_ID_NOT_EXPECTED;
	             PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));
             }
			       else
			       {/* check buffer length */
		           Vs32ReturnCode=Vs32ReturnCode-PDD_SCC_POS_MSG_READ_DATA;
			         /*the pool in V850 can be greater then the read pool, get the size from struct*/
			         /*check if read data < size of pool */
		           if(Vs32ReturnCode < Ps32Size)
		           { /*length not expected => trace*/
			           PDD_TRACE(PDD_SCC_LENGTH_DATA_RECEIVED,&Vs32ReturnCode,sizeof(tS32));
                 Vs32ReturnCode=PDD_ERROR_SCC_RECV_LENGTH_NOT_EXPECTED;				
	               PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));
		             /*error memory entry*/
				         PDD_SET_ERROR_ENTRY("SCC: lenght not expected");
               }		 
			         /*check size*/
			         if(Vs32ReturnCode>Ps32Size)
			         { /*wrong length in data*/
			           Vs32ReturnCode=PDD_ERROR_SCC_RECV_LENGTH_NOT_EXPECTED;
	               PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));
			           Vs32ReturnCode=Ps32Size;
               }
			         /*check buffer */
		           if(PBuffer!=NULL)
		           {/*copy data into buffer, then check with version and crc*/	
			           if(Vs32ReturnCode>0)
			           {
		               memmove(PBuffer,&Vu8RecvBuf[PDD_SCC_POS_MSG_READ_DATA],(tU32)Vs32ReturnCode);		  
                 }
			           else
			           { /*0 bytes read only header*/
			             Vs32ReturnCode=0;
                 }
               }
		           else
		           {
		             PDD_FATAL_M_ASSERT_ALWAYS();
               }
             }
           }break;       
		       case SCC_PD_NET_R_WRITE_DATA_POOL:
	         {/* message write data done*/
		         if(Vu8RecvBuf[PDD_SCC_POS_MSG_READ_POOL_ID]!=PubPoolNumber)
			       {
               Vs32ReturnCode=PDD_ERROR_SCC_RECV_DATAPOOL_ID_NOT_EXPECTED;
	             PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));
             }
			       else
			       {
			         Vs32ReturnCode=PDD_OK;
             }
           }break;
	         case SCC_PD_NET_R_REJECT_MSGID: /*no break*/
           default:
	         {		     
	           Vs32ReturnCode=PDD_ERROR_SCC_RECV_MSG_NOT_EXPECTED;
	           PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32)); 
             /*error memory entry => if version mismatch*/             
             if(Vu8RecvBuf[PDD_SCC_POS_MSG_READ_POOL_ID]==SCC_PD_NET_REJ_VERSION_MISMATCH)
             {
               tU8 Vu8Buffer[200];		
               snprintf(Vu8Buffer,sizeof(Vu8Buffer),"PDD_SccRecvMsg: version mismatch for pool number %d ",PubPoolNumber); 
		           PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
             }             
           }break;
         }/*end switch*/		
       }/*end else*/
    }/*end if*/
  }
  return(Vs32ReturnCode);
}
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/* End of File pdd_access_scc.c                                               */
/******************************************************************************/


