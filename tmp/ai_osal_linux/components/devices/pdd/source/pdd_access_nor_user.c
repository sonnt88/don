/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        This file contains the access to the NOR, which saves transaction safe data into NOR 
 * @addtogroup   PDD access 
 * @{
 */ 

/*
 * global function:
 * -- PDD_NorUserAccessInit:
 *       init routine for nor flash access 
 * -- PDD_NorUserAccessDeInit:
 *       deinit routine for nor flash access
 * -- PDD_NorUserAccessGetDataStreamSize:
 *       get size of data, without header 
 * -- PDD_NorUserAccessErase:
 *       erase all sector
 * -- PDD_NorUserAccessReadDataStream:
 *       function for read from nor nor 
 * -- PDD_NorUserAccessWriteDataStream:
 *       function for write to nor nor
 * -- PDD_NorUserAccessGetEraseCounter
 *       get erase counter for trace
 *
 * local function:
 * -- PDD_bFormatFlash:
 *        format a sector
 * -- PDD_bWriteToCluster:
 *        write "data" to the next free cluster
 * -- PDD_bReadFromCluster:
 *        read the data from the last valid cluster to "data"
 * -- PDD_bReadFromLastClusterValidData:
 *        read data from the last cluster with valid data
 * -- PDD_bIsFlashFormated:
 *        check if flash formated
 * -- PDD_s32FindValidCluster:
 *        find valid cluster
 * -- PDD_s32FindNextFreeCluster:
 *        find next free cluster
 * -- PDD_bIsClusterFormated:
 *        check if cluster formated
 * -- PDD_bIsClusterValid:
 *        check if cluster valid
 * -- PDD_bIsClusterValidDataWritten
 *        check if cluster has valid written data
 * -- PDD_bReadFlash:
 *        read from flash
 * -- PDD_bWriteFlash:
 *        write to flash
 * -- PDD_bEraseFlash:
 *        erase flash
 * -- PDD_u8GetCheckDataStream:
 *        check the datastream name and get the table number
 * -- PDD_bReadDataStreamConfiguration:
 *        read data configuration from flash
 */

/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>   
#include "system_types.h"
#include "pdd_variant.h"
#ifdef PDD_TESTMANGER_ACTIVE  //define is set in pdd_variant.h
#include "pdd.h"
#include "pdd_testmanager.h"
#else
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include "system_definition.h"
#include "LFX2.h"
#include "pdd.h"
#include "pdd_config_nor_user.h"
#endif
#include "pdd_private.h"
#include "pdd_trace.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
#define PDD_NOR_USER_MAX_SECTOR_ERASE_COUNTER          0xc380 //50048
#define PDD_NOR_USER_MAX_SECTOR_ERASE_COUNTER_MASK     0x7F   //128

/*magic number */
#define PDD_NOR_USER_MAGIC                0xDEAD
#define PDD_NOR_USER_MIN_CHUNK_SIZE       0x20

/*defines for state of cluster => save in header  */
#define PDD_NOR_USER_STATE_FORMAT_BEGIN     0x01
#define PDD_NOR_USER_STATE_FORMAT_END       0x02
#define PDD_NOR_USER_STATE_WRITE_BEGIN      0x04
#define PDD_NOR_USER_STATE_WRITE_END        0x08
#define PDD_NOR_USER_STATE_INVALID          0x10
#define PDD_NOR_USER_STATE_OLD_VERSION      0x20

/*define for alignement*/
#define CHIP_ALIGNMENT (vtspPddNorInfo->u16ChunkSize)
#define CHIP_ALIGNMENT_MASK (~(CHIP_ALIGNMENT-1))
#define ALIGN_TO_CHIP(x) ((x)+(CHIP_ALIGNMENT-1) & CHIP_ALIGNMENT_MASK)

/*position in the header of a cluster*/
#define PDD_NOR_USER_MAGIC_POSITION                      (0x00*CHIP_ALIGNMENT)
#define PDD_NOR_USER_STATUS_POSITION_FORMAT_BEGIN        (0x01*CHIP_ALIGNMENT)
#define PDD_NOR_USER_STATUS_POSITION_FORMAT_END          (0x02*CHIP_ALIGNMENT)
#define PDD_NOR_USER_STATUS_POSITION_WRITE_BEGIN         (0x03*CHIP_ALIGNMENT)
#define PDD_NOR_USER_STATUS_POSITION_WRITE_END           (0x04*CHIP_ALIGNMENT)
#define PDD_NOR_USER_STATUS_POSITION_INVALID             (0x05*CHIP_ALIGNMENT)
#define PDD_NOR_USER_STATUS_POSITION_OLD_VERSION         (0x06*CHIP_ALIGNMENT)
#define PDD_NOR_USER_CHECKSUM_POSITION                   (0x07*CHIP_ALIGNMENT)

/*define for header size*/
#define PDD_NOR_USER_HEADER_SIZE             (PDD_NOR_USER_MAX_WRITE_POSITION*CHIP_ALIGNMENT)

/*define for return header update*/
#define PDD_NOR_USER_UPDATE_HEADER                       1

#ifdef PDD_TESTMANGER_ACTIVE
#define PDD_NOR_USER_NUMBER_DATASTREAM_TM                   vu8NumberDataStreamTm
#define PDD_NOR_USER_HEADER_SIZE_DATA_STREAM               (vu8NumberDataStreamTm*sizeof(tsPddNorUserHeaderDataStream))
#endif


#ifdef PDD_TESTMANGER_ACTIVE
#define M_PDD_NOR_ACCESS_OPEN(name)                         PddTm_tOpen(name)
#define M_PDD_NOR_ACCESS_CLOSE(handle)                      PddTm_vClose(handle)	  
#define M_PDD_NOR_ACCESS_READ(handle,buffer,size,offset)    PddTm_s32Read(handle,buffer,size,offset)
#define M_PDD_NOR_ACCESS_WRITE(handle,buffer,size,offset)   PddTm_s32Write(handle,buffer,size,offset)
#define M_PDD_NOR_ACCESS_ERASE(handle,size,offset)          PddTm_s32Erase(handle,size,offset)
#else
#define M_PDD_NOR_ACCESS_OPEN(name)                         LFX_open(name)	  
#define M_PDD_NOR_ACCESS_CLOSE(handle)                      LFX_close(handle)	  
#define M_PDD_NOR_ACCESS_READ(handle,buffer,size,offset)    LFX_read(handle,buffer,size,offset)	
#define M_PDD_NOR_ACCESS_WRITE(handle,buffer,size,offset)   LFX_write(handle,buffer,size,offset)
#define M_PDD_NOR_ACCESS_ERASE(handle,size,offset)          LFX_erase(handle,size,offset)
#endif
/******************************************************************************* 
|typedef 
|------------------------------------------------------------------------------*/
/* structure for magic*/
typedef struct
{
  tU16  u16MagicLow;
  tU8   u8ChunkSize;    
  tU32  u32EraseSectorCount;
}tsPddNorUserHeaderMagic;

/* structure for checksum */
typedef struct
{
  tU32  u32CheckSum;
  tU32  u32CheckSumXor;
}tsPddNorUserHeaderChecksum;

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/
static tsPddNorUserInfo*          vtspPddNorInfo=NULL;

#ifdef PDD_TESTMANGER_ACTIVE
static tPddTmHandle               vthNorAccess=NULL;
static tU8                        vu8NumberDataStreamTm=0;
#else
static LFXhandle                  vthNorAccess=NULL;
#endif
/************************************************************************
| variable definition (scope: module-global)
|-----------------------------------------------------------------------*/


/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static tBool  PDD_bFormatFlash(tsPddNorUserInfo*,tS32);
static tBool  PDD_bWriteToCluster(tsPddNorUserInfo*,tU8*);
static tBool  PDD_bReadFromCluster(tsPddNorUserInfo*,void*);
static tBool  PDD_bIsFlashFormated(tsPddNorUserInfo*,tS32);
static tS32   PDD_s32FindValidCluster(tsPddNorUserInfo*);
static tS32   PDD_s32FindNextFreeCluster(tsPddNorUserInfo*);
static tBool  PDD_bIsClusterFormated(tsPddNorUserInfo*,tS32);
static tBool  PDD_bIsClusterValid(tsPddNorUserInfo*,tS32, tBool*);
static tBool  PDD_bReadFlash(tsPddNorUserInfo*,tS32,void*,tU32,tU32);
static tBool  PDD_bWriteFlash(tsPddNorUserInfo*,tS32,tU8*,tU32,tU32);
static tBool  PDD_bEraseFlash(tsPddNorUserInfo*,tS32);
static tU8    PDD_u8GetCheckDataStreamDefinedHeader(const char* ,tsPddNorUserInfo* );  
static tU8    PDD_u8GetCheckDataStreamReadHeader(char* ,tsPddNorUserHeaderDataStream* );   
static tS32   PDD_s32UpdateHeaderDataStream(tsPddNorUserInfo* ,tS32 ,tU8 );
static void   PDD_vUpdateData(tsPddNorUserInfo* ,void* );
static void   PDD_vCheckTimeEraseSector(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32SectorNum);
#ifdef PDD_TESTMANGER_ACTIVE
static tBool  PDD_bGetNumberDataStreamTmAndNameDataStream(tsPddNorUserHeaderDataStream*);
#endif
/******************************************************************************
* FUNCTION: PDD_NorUserAccessInit()
*
* DESCRIPTION: init routine for nor flash access
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
tS32 PDD_NorUserAccessInit(void)
{ 
  tU8                            Vu8Inc;
  tS32                           Vs32ErrorCode=PDD_OK;
  tsPddNorUserHeaderDataStream*  VspHeaderDataStream;
  const tsPddNorUserConfig*      VspPddNorUserConfig;
  
  if(vthNorAccess==NULL)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_USER_INIT_FAIL;
	  PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  {
    vtspPddNorInfo->u32ClusterSize=PDD_NOR_USER_CLUSTER_SIZE;        
    /*get num blocks*/
	  vtspPddNorInfo->u32NumBlocks=vthNorAccess->numBlocks;
	  PDD_TRACE(PDD_NOR_USER_DEBUG_NUMBER_BLOCKS,&vtspPddNorInfo->u32NumBlocks,sizeof(tU32));
	  if(vtspPddNorInfo->u32NumBlocks < PDD_NOR_USER_MIN_NUMBER_OF_SECTOR)
	  {
	    Vs32ErrorCode=PDD_ERROR_NOR_USER_NO_VALID_NUMBER_OF_BLOCKS;
	    PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	  }
	  else
	  { /*get sector size */
      vtspPddNorInfo->u32SectorSize=vthNorAccess->blockSize;	
      /*check sector size and if cluster size fits into one sector*/
      if(vtspPddNorInfo->u32SectorSize==0)
	    {
	      Vs32ErrorCode=PDD_ERROR_NOR_USER_NO_VALID_SECTOR_SIZE;
	      PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));;
	    }
	    else  
	    { /* trace out size */
	      tU32  Vu32SizeKilo=vtspPddNorInfo->u32SectorSize/1024;
        PDD_TRACE(PDD_NOR_USER_DEBUG_SIZE_SECTOR,&Vu32SizeKilo,sizeof(Vu32SizeKilo));      
		    if(vtspPddNorInfo->u32SectorSize < PDD_NOR_USER_MIN_SECTOR_SIZE) 
		    {
		      tU32 Vu32Factor = PDD_NOR_USER_MIN_SECTOR_SIZE / vtspPddNorInfo->u32SectorSize; 
		      vtspPddNorInfo->u32SectorSize=PDD_NOR_USER_MIN_SECTOR_SIZE;
		      vtspPddNorInfo->u32NumBlocks /= Vu32Factor;
          if(vtspPddNorInfo->u32NumBlocks < PDD_NOR_USER_MIN_NUMBER_OF_SECTOR)
	        {
		        Vs32ErrorCode=PDD_ERROR_NOR_USER_NO_VALID_NUMBER_OF_BLOCKS;
	          PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));;
	        }
		      { /*trace out new information */
		        Vu32SizeKilo=vtspPddNorInfo->u32SectorSize/1024;
            PDD_TRACE(PDD_NOR_USER_DEBUG_SIZE_SECTOR,&Vu32SizeKilo,sizeof(Vu32SizeKilo)); 
			      PDD_TRACE(PDD_NOR_USER_DEBUG_NUMBER_BLOCKS,&vtspPddNorInfo->u32NumBlocks,sizeof(tU32));
		      }
        }	
	      /*init other variable*/
        vtspPddNorInfo->s32LastClusterUsed=-1;   	
		    memset(&vtspPddNorInfo->u32EraseSectorLastTime[0],0,sizeof(tU32)*PDD_NOR_USER_MIN_NUMBER_OF_SECTOR);              
        /* get num cluster*/
        vtspPddNorInfo->s32NumClusterPerSector=((tS32)(vtspPddNorInfo->u32SectorSize/(vtspPddNorInfo->u32ClusterSize)));
        vtspPddNorInfo->s32NumCluster=vtspPddNorInfo->s32NumClusterPerSector*vtspPddNorInfo->u32NumBlocks;       		
	      /* get chunk size */
	      vtspPddNorInfo->u16ChunkSize=vthNorAccess->chunkSize;  
	      /* trace chunk size */        
        PDD_TRACE(PDD_NOR_USER_DEBUG_SIZE_CHUNK,&vtspPddNorInfo->u16ChunkSize,sizeof(vtspPddNorInfo->u16ChunkSize));
	      /*if hasECC = false and vtspPddNorInfo->u16ChunkSize < 32 set chunksize to 32 for generate a valid header*/
        if(vtspPddNorInfo->u16ChunkSize < PDD_NOR_USER_MIN_CHUNK_SIZE)
		    {
          vtspPddNorInfo->u16ChunkSize=PDD_NOR_USER_MIN_CHUNK_SIZE;
		      PDD_TRACE(PDD_NOR_USER_DEBUG_SIZE_CHUNK,&vtspPddNorInfo->u16ChunkSize,sizeof(vtspPddNorInfo->u16ChunkSize));
        }
		    /* check chunk size*/
	      if((vtspPddNorInfo->u16ChunkSize >= 0xff)||((vtspPddNorInfo->u16ChunkSize & (vtspPddNorInfo->u16ChunkSize-1))))
	      {
	        Vs32ErrorCode=PDD_ERROR_NOR_USER_NO_VALID_CHUNK_SIZE;
	        PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
        }
	      else
	      {		 
		      /* init variable, which depends on vtspPddNorInfo->u16ChunkSize*/ 
	        vtspPddNorInfo->u32ClusterSizeData=vtspPddNorInfo->u32ClusterSize - PDD_NOR_USER_HEADER_SIZE;             
          /* init buffer and clear header*/
          memset(vtspPddNorInfo->u8pBufferDataOld,0xff,vtspPddNorInfo->u32ClusterSizeData);   
		      /* get possible filename*/
		      #ifndef PDD_TESTMANGER_ACTIVE
		      memset(vtspPddNorInfo->sHeaderDataStream,0,PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM*sizeof(tsPddNorUserHeaderDataStream)); 
          for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vu8Inc++)
          #else
		      memset(vtspPddNorInfo->sHeaderDataStream,0,PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM*sizeof(tsPddNorUserHeaderDataStream)); 
		      for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM;Vu8Inc++)
          #endif
		      {/* get possible filename*/
		        VspHeaderDataStream=&vtspPddNorInfo->sHeaderDataStream[Vu8Inc];	
			      VspPddNorUserConfig=&vrPddNorUserConfig[Vu8Inc];	
            #ifdef PDD_TESTMANGER_ACTIVE
			      /*only name "kds_data" is copy to name*/
			      if(Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM_TM)
            #endif
			      memmove(VspHeaderDataStream->u8DataStreamName,VspPddNorUserConfig->u8DataStreamName,PDD_NOR_MAX_SIZE_NAME); 
			      /*offset,lenght,count is set by first application access*/	
			      VspHeaderDataStream->u32Offset=0;
	          VspHeaderDataStream->u32Lenght=0;
			      VspHeaderDataStream->u32Count=0;
          }			
		      /*trace out header */
	        PDD_TRACE(PDD_NOR_USER_DEBUG_DATA_STREAM_HEADER,&vtspPddNorInfo->sHeaderDataStream[0],PDD_NOR_USER_HEADER_SIZE_DATA_STREAM);
        }/*end else*/
      }/*end else*/
    }/*end else*/
  }/* end else*/
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessDeInit()
*
* DESCRIPTION: deinit routine for nor flash access
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
tS32 PDD_NorUserAccessDeInit(void)
{ 
  tU8                            Vu8Inc;
  tS32                           Vs32ErrorCode=PDD_OK;
  tsPddNorUserHeaderDataStream*  VspHeaderDataStream;

  if(vthNorAccess==NULL)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_USER_INIT_FAIL;
	  PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  {/* cluster size*/
    vtspPddNorInfo->u32ClusterSize=0;        
    /* num blocks*/
    vtspPddNorInfo->u32NumBlocks=0;
    /* sector size */
    vtspPddNorInfo->u32SectorSize=0;	  
    /*init other variable*/
    vtspPddNorInfo->s32LastClusterUsed=-1;         
    /* num cluster*/
    vtspPddNorInfo->s32NumClusterPerSector=0;
    vtspPddNorInfo->s32NumCluster=0;                         
    /* chunk size */
    vtspPddNorInfo->u16ChunkSize=1;
    /*u32ClusterSizeData*/ 
    vtspPddNorInfo->u32ClusterSizeData=0;     
    /* clear datastream*/
    #ifndef PDD_TESTMANGER_ACTIVE
    for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vu8Inc++)
    #else
	for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM;Vu8Inc++)
    #endif
    {/* get possible filename*/
	    VspHeaderDataStream=&vtspPddNorInfo->sHeaderDataStream[Vu8Inc];
	    memset(VspHeaderDataStream->u8DataStreamName,0x00,PDD_NOR_MAX_SIZE_NAME); 		
	    VspHeaderDataStream->u32Offset=0;
	    VspHeaderDataStream->u32Lenght=0;
      VspHeaderDataStream->u32Count=0;
    }
  }
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessInitProcess()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
tS32 PDD_NorUserAccessInitProcess(void)
{
  tS32  Vs32ErrorCode=PDD_OK;
  vtspPddNorInfo=PDD_GetNorUserInfoShm();
  if(vthNorAccess==NULL)
  { /* PDD*/
    PDD_TRACE(PDD_NOR_USER_DEBUG_LFX_OPEN,0,0);
	  vthNorAccess= M_PDD_NOR_ACCESS_OPEN("PDD_UserEarly");	  
	  if(vthNorAccess==NULL)
	  {
	    Vs32ErrorCode=PDD_ERROR_NOR_USER_LFX_WRONG_HANDLE;
	    PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	  }
  }
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessDeInitProcess()
*
* DESCRIPTION: 
*
* PARAMETERS:
*      
*
* RETURNS: 
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
tS32 PDD_NorUserAccessDeInitProcess(void)
{
  tS32  Vs32ErrorCode=PDD_OK;
  M_PDD_NOR_ACCESS_CLOSE(vthNorAccess);	
  vthNorAccess=NULL;
  vtspPddNorInfo=NULL;
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessGetDataStreamSize()
*
* DESCRIPTION: get size of data, without header
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*
* RETURNS: 
*      positive value: size of datastream with header 
*      negative value: error code 
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
tS32 PDD_NorUserAccessGetDataStreamSize(const char* PtsDataStreamName)
{ 
  tS32               Vs32Size=PDD_OK;
  tU8                VubDataStream;

  PDD_TRACE(PDD_NOR_USER_DEBUG_DATA_STREAM_NAME,PtsDataStreamName,strlen(PtsDataStreamName)+1);
  /*check stream*/   
  VubDataStream=PDD_u8GetCheckDataStreamDefinedHeader(PtsDataStreamName,vtspPddNorInfo);
  /* check if name in configuration*/
  if(VubDataStream==0xff)
  { /*error in generate header for PDD?: if defined location for NOR (Datapool or PDD), 
	  header of PDD gets the name for this stream */
    tU8  Vu8Buffer[200];		
    Vs32Size=PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED;
    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d datastream:'%s' not defined",Vs32Size,Vs32Size,PtsDataStreamName);
	  PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
    PDD_SET_ERROR_ENTRY(Vu8Buffer);
  }
  else
  { /* read data stream configuration */
    if(PDD_bReadDataStreamConfiguration(vtspPddNorInfo)==TRUE)
	  { /* set size */
	    Vs32Size=(tS32)vtspPddNorInfo->sHeaderDataStream[VubDataStream].u32Lenght;	
	  }
	  else
	  { /*configuration wrong number > PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM*/
	    Vs32Size=PDD_ERROR_NOR_USER_CONFIGURATION_HEADER;
      PDD_TRACE(PDD_ERROR,&Vs32Size,sizeof(tS32));
	    PDD_SET_ERROR_ENTRY("NOR_USER: error configuration header");
	  }
  }/*end else*/
  return(Vs32Size);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessErase()
*
* DESCRIPTION: erase all sector
*
* PARAMETERS:
*    
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
tS32 PDD_NorUserAccessErase(void)
{
  tS32               Vs32ErrorCode=PDD_OK;
  tU32               Vu32Inc;
  
  for(Vu32Inc=0; Vu32Inc < vtspPddNorInfo->u32NumBlocks; ++Vu32Inc)
  { /*erase flash => flash is not formated*/
    if(PDD_bEraseFlash(vtspPddNorInfo,(tS32)Vu32Inc)==FALSE)
	  {
	    Vs32ErrorCode=PDD_ERROR_NOR_USER_ERASE_FAIL;
	    PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	  }
  }
  if(Vs32ErrorCode==PDD_OK)
  {/*clear old buffer*/
    memset(&vtspPddNorInfo->u8pBufferDataOld[0],0xff,vtspPddNorInfo->u32ClusterSizeData);
    /*set last cluster used to unvalid*/
    vtspPddNorInfo->s32LastClusterUsed = -1;
    /*no valid header*/
    vtspPddNorInfo->sHeaderDataStream[0].u32Offset=0;
    /*set erase counter to zero*/
    vtspPddNorInfo->u32EraseSectorCount=0;
  }
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessReadPartition()
*
* DESCRIPTION: read the complete partition
*
* PARAMETERS:
*    
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2014 04 23
*****************************************************************************/
tS32 PDD_NorUserAccessReadPartition(void *PvpReadBuffer)
{
  tS32               Vs32ErrorCode=PDD_OK;

  if(PDD_bReadFlash(vtspPddNorInfo,0,PvpReadBuffer,0,PDD_NOR_USER_MIN_NUMBER_OF_SECTOR*PDD_NOR_USER_MIN_SECTOR_SIZE)==FALSE)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_USER_READ_DUMP;
  }
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessReadActualCluster()
*
* DESCRIPTION: read actual cluster
*
* PARAMETERS:
*    
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2014 05 07
*****************************************************************************/
tS32 PDD_NorUserAccessReadActualCluster(void *PvpReadBuffer)
{
  tS32               Vs32ErrorCode=PDD_OK;
  tS32               Vs32FoundCluster = -1;

  Vs32FoundCluster=PDD_s32FindValidCluster(vtspPddNorInfo);
  if(Vs32FoundCluster >= 0)
  {
    if(PDD_bReadFlash(vtspPddNorInfo,Vs32FoundCluster,PvpReadBuffer,0,PDD_NOR_USER_CLUSTER_SIZE)==FALSE)
    {
      Vs32ErrorCode=PDD_ERROR_NOR_USER_READ_DUMP;
    }
  }
  else
  {
    Vs32ErrorCode=PDD_ERROR_NOR_USER_READ_DUMP;
  }
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessWritePartition()
*
* DESCRIPTION: write data to parttion PDD_UserEarly
*
* PARAMETERS:
*      Ps32Cluster: Cluster
*    
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2014 05 07
*****************************************************************************/
tS32 PDD_NorUserAccessWritePartition(void *Ppu8WriteBuffer)
{
  tS32  Vs32ErrorCode=PDD_ERROR_NOR_USER_WRITE_CLUSTER;
  
  if((vtspPddNorInfo!=NULL)&&(Ppu8WriteBuffer!=NULL))
  {
    if(PDD_bWriteFlash(vtspPddNorInfo,0, Ppu8WriteBuffer, 0, PDD_NOR_USER_MIN_NUMBER_OF_SECTOR*PDD_NOR_USER_MIN_SECTOR_SIZE)==TRUE)
    {
      Vs32ErrorCode=PDD_OK;
    }
  }
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessGetActualCluster()
*
* DESCRIPTION: get actual cluster
*
* PARAMETERS:
*      Ps32Cluster: Cluster
*    
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2014 05 08
*****************************************************************************/
tS32 PDD_NorUserAccessGetActualCluster(void)
{
  return(PDD_s32FindValidCluster(vtspPddNorInfo));
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessReadDataStream()
*
* DESCRIPTION: function for read from nor nor
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
* RETURNS: 
*      positive value: size of read data
*      negative value: error code 
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
tS32 PDD_NorUserAccessReadDataStream(const char* PtsDataStreamName,void *PpvReadBuffer,tS32 Ps32SizeReadBuffer)
{
  tS32               Vs32ErrorCode=PDD_OK;
  void*              VvpReadBuffer=NULL;
  tU8                VubDataStream;
  tU32               Vu32Size;
  tU32               Vu32Offset;

  PDD_TRACE(PDD_NOR_USER_DEBUG_DATA_STREAM_NAME,PtsDataStreamName,strlen(PtsDataStreamName)+1);
  /*check stream*/   
  VubDataStream=PDD_u8GetCheckDataStreamDefinedHeader(PtsDataStreamName,vtspPddNorInfo);
  /* check if name in configuration*/
  if(VubDataStream==0xff)
  {/*error in generate header for PDD?: if defined location for NOR_NOR (Datapool or PDD), 
	   header of PDD gets the name for this stream */
    tU8  Vu8Buffer[200];		
    Vs32ErrorCode=PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED;
    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32ErrorCode,Vs32ErrorCode,PtsDataStreamName);
	  PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
  }
  else
  { /* read data stream configuration */
    if(PDD_bReadDataStreamConfiguration(vtspPddNorInfo)==FALSE)
	  {/*configuration wrong number > PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM*/
	    Vs32ErrorCode=PDD_ERROR_NOR_USER_CONFIGURATION_HEADER;
      PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	    PDD_SET_ERROR_ENTRY("NOR_USER: error configuration header");
	  }
	  else
	  {/*get size and offset*/
	    Vu32Size=vtspPddNorInfo->sHeaderDataStream[VubDataStream].u32Lenght;
	    Vu32Offset=vtspPddNorInfo->sHeaderDataStream[VubDataStream].u32Offset;
	    /*check length*/
	    if(Ps32SizeReadBuffer < (tS32) Vu32Size)
	    { 
        Vs32ErrorCode= PDD_ERROR_NOR_USER_WRONG_SIZE;
      }
	    else  
      { /* allocate buffer*/
        VvpReadBuffer=malloc(vtspPddNorInfo->u32ClusterSizeData);
	      /* check buffer*/
	      if(VvpReadBuffer==NULL)
	      { /*system: no space available*/
	        Vs32ErrorCode= PDD_ERROR_NO_BUFFER;
	        PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	        PDD_FATAL_M_ASSERT_ALWAYS();
	      }
	      else
	      { /* read complete cluster data*/
          if(PDD_bReadFromCluster(vtspPddNorInfo,VvpReadBuffer)==FALSE)
          {
            tU8  Vu8Buffer[200];		
            Vs32ErrorCode=PDD_ERROR_NOR_USER_READ_FROM_CLUSTER;
	          snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32ErrorCode,Vs32ErrorCode,PtsDataStreamName);
	          PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
          }
	        else
	        { /* copy data to buffer*/
            tU8* Vu8pSource=((tU8*)VvpReadBuffer)+Vu32Offset; 
            memmove(PpvReadBuffer,Vu8pSource,Vu32Size);
	          /* set size*/
	          Vs32ErrorCode=Vu32Size;
	        }
		      /* set buffer to free*/
		      free(VvpReadBuffer);
        }/*end else chechk pointer*/	    
      }/*end else*/    
    }
  }
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessWriteDataStream()
*
* DESCRIPTION: function for write to nor nor
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*
* RETURNS: 
*      positive value: PDD_OK
*      negative value: error code 
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
tS32 PDD_NorUserAccessWriteDataStream(const char* PtsDataStreamName,void *Ppu8WriteBuffer,tS32 Ps32SizeWriteBuffer)
{ 
  tS32               Vs32ErrorCode=PDD_OK;
  tU8                VubDataStream;
 
  PDD_TRACE(PDD_NOR_USER_DEBUG_DATA_STREAM_NAME,PtsDataStreamName,strlen(PtsDataStreamName)+1);
  /*check stream*/   
  VubDataStream=PDD_u8GetCheckDataStreamDefinedHeader(PtsDataStreamName,vtspPddNorInfo);
  /* check if name in configuration*/
  if(VubDataStream==0xff)
  {/*error in generate header for PDD?: if defined location for NOR_NOR (Datapool or PDD), 
	 header of PDD gets the name for this stream */
    tU8  Vu8Buffer[200];		
    Vs32ErrorCode=PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED;
    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32ErrorCode,Vs32ErrorCode,PtsDataStreamName);
	  PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
  }
  else
  { /* allocate buffer*/
    void* VpWriteBuffer=malloc(vtspPddNorInfo->u32ClusterSizeData);
    /* check buffer*/
    if(VpWriteBuffer==NULL)
    { /*system: no space available*/
	    Vs32ErrorCode= PDD_ERROR_NO_BUFFER;
	    PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
      PDD_FATAL_M_ASSERT_ALWAYS();
    }
    else
    { 
      tU32 Vu32Offset; 				
      /* read data stream configuration */
      if(PDD_bReadDataStreamConfiguration(vtspPddNorInfo)==FALSE)
	    {/*configuration wrong number > PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM*/
	      Vs32ErrorCode=PDD_ERROR_NOR_USER_CONFIGURATION_HEADER;
        PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	      PDD_SET_ERROR_ENTRY("NOR_USER: error configuration header");
      }
	    else
	    {/* copy old buffer to bufferwrite oldbuffer must be set if read cluster!! */
	      memmove(VpWriteBuffer,&vtspPddNorInfo->u8pBufferDataOld[0],vtspPddNorInfo->u32ClusterSizeData);		
	      /*update header if need */ 
	      Vs32ErrorCode=PDD_s32UpdateHeaderDataStream(vtspPddNorInfo,Ps32SizeWriteBuffer,VubDataStream);
        if(Vs32ErrorCode==PDD_NOR_USER_UPDATE_HEADER)
	      {/* update data*/
          PDD_vUpdateData(vtspPddNorInfo,VpWriteBuffer);
	        /* memmove new header into write buffer*/
	        memmove(VpWriteBuffer,&vtspPddNorInfo->sHeaderDataStream[0],PDD_NOR_USER_HEADER_SIZE_DATA_STREAM);	
	        /*set error code to OK*/
	        Vs32ErrorCode=PDD_OK;
        }
	      /*check error*/
	      if(Vs32ErrorCode==PDD_OK)
	      {/* write data into buffer */
	        tU8* Vu8pWriteBuffer=(tU8*) VpWriteBuffer;
	        Vu32Offset=vtspPddNorInfo->sHeaderDataStream[VubDataStream].u32Offset;
          memmove(Vu8pWriteBuffer+Vu32Offset,Ppu8WriteBuffer,Ps32SizeWriteBuffer);
	        /* compare old with new data */
          if(memcmp(VpWriteBuffer,&vtspPddNorInfo->u8pBufferDataOld[0], vtspPddNorInfo->u32ClusterSizeData) != 0)
          { /* increment counter: write cycle for the datastream*/
		        tsPddNorUserHeaderDataStream* VtsHeaderStream=(tsPddNorUserHeaderDataStream*)VpWriteBuffer;
            VtsHeaderStream[VubDataStream].u32Count++;
		        vtspPddNorInfo->sHeaderDataStream[VubDataStream].u32Count++;
		        /* save  buffer*/
            if((PDD_bWriteToCluster(vtspPddNorInfo,VpWriteBuffer))==FALSE)
            {/* error */
              tU8  Vu8Buffer[200];		
              Vs32ErrorCode=PDD_ERROR_NOR_USER_WRITE_TO_CLUSTER;
	            snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32ErrorCode,Vs32ErrorCode,PtsDataStreamName);
	            PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
            }
	          else
	          { /* save data into old buffer*/
	            memmove(&vtspPddNorInfo->u8pBufferDataOld[0], VpWriteBuffer,vtspPddNorInfo->u32ClusterSizeData); 
	            /*get size write */
		          Vs32ErrorCode=Ps32SizeWriteBuffer;				
            }
          }
        }
      }/*end else check configuration*/	 
	    free(VpWriteBuffer);
    }/*end else check pointer*/
  }/*end else check datastream*/
  return(Vs32ErrorCode);
}

/******************************************************************************
* FUNCTION: PDD_NorUserAccessReadDataStreamEarly()
*
* DESCRIPTION: function for read a data stream early with fast access
*              (without check of whole cluster)
*
* PARAMETERS:
*      PtsDataStreamName:  name of the data stream
*      Ppu8ReadBuffer:     read buffer
*      Ps32SizeReadBuffer: size of read buffer
*
* RETURNS: 
*      positive value: PDD_OK
*      negative value: error code 
*
* HISTORY:Created  2015 10 12
*****************************************************************************/
tS32  PDD_NorUserAccessReadDataStreamEarly(const char* PtsDataStreamName,void *PvpReadBuffer,tS32 Ps32SizeReadBuffer)
{
  tS32               Vs32ErrorCode=PDD_ERROR_NOR_USER_READ_FROM_CLUSTER;
  tU8                VubDataStream;
  tS32               Vs32FoundCluster = -1;

  PDD_TRACE(PDD_NOR_USER_DEBUG_DATA_STREAM_NAME,PtsDataStreamName,strlen(PtsDataStreamName)+1);
  /*check stream defined*/   
  VubDataStream=PDD_u8GetCheckDataStreamDefinedHeader(PtsDataStreamName,vtspPddNorInfo);
  if(VubDataStream==0xff)
  { /*error in generate header for PDD?: if defined location for NOR_NOR (Datapool or PDD), 
	  header of PDD gets the name for this stream */
    tU8  Vu8Buffer[200];		
    Vs32ErrorCode=PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED;
    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32ErrorCode,Vs32ErrorCode,PtsDataStreamName);
	  PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
  }
  else
  { /*find valid cluster*/
    Vs32FoundCluster=PDD_s32FindValidCluster(vtspPddNorInfo);
    if(Vs32FoundCluster >= 0)
    { /*read checksum*/
	    tsPddNorUserHeaderChecksum     VtsHeaderChecksum;
      memset(&VtsHeaderChecksum,0,sizeof(tsPddNorUserHeaderChecksum));
      tBool VbResult = PDD_bReadFlash(vtspPddNorInfo,Vs32FoundCluster,&VtsHeaderChecksum,PDD_NOR_USER_CHECKSUM_POSITION ,sizeof(VtsHeaderChecksum));
      if(VbResult && (VtsHeaderChecksum.u32CheckSum == (~VtsHeaderChecksum.u32CheckSumXor)))
      { //read first datastream header entry to get the size of the header
		    tsPddNorUserHeaderDataStream VsHeaderDataStreamFirst;
		    if(PDD_bReadFlash(vtspPddNorInfo,Vs32FoundCluster,&VsHeaderDataStreamFirst,PDD_NOR_USER_HEADER_SIZE,sizeof(tsPddNorUserHeaderDataStream))==TRUE)
        { /*get number of read datastreams (=first offset/size for one header)*/
		      tU8 Vu8NumberDataStreamRead=(tU8)(VsHeaderDataStreamFirst.u32Offset/(sizeof(tsPddNorUserHeaderDataStream)));
		      //determine size of header data stream
	        tS32 Vs32Size= (sizeof(tsPddNorUserHeaderDataStream)*Vu8NumberDataStreamRead); //size greater as in defined configuration  
		      /* allocate buffer*/
          void* VvpBufRead=malloc(Vs32Size);
          /* check buffer*/
          if(VvpBufRead==NULL)
          { /*system: no space available*/
            Vs32ErrorCode= PDD_ERROR_NO_BUFFER;
	          PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));	          
          }
          else
          { /* read header datastream*/
            if(PDD_bReadFlash(vtspPddNorInfo,Vs32FoundCluster,VvpBufRead,PDD_NOR_USER_HEADER_SIZE,Vs32Size)==TRUE)
            { //check stream available		
			        tsPddNorUserHeaderDataStream*  VspHeaderDataStreamRead=(tsPddNorUserHeaderDataStream*)VvpBufRead;
		          tU8                            Vu8StreamReadPos=PDD_u8GetCheckDataStreamReadHeader((char*)PtsDataStreamName,VspHeaderDataStreamRead);        
		          /*check if datastream exist*/
		          if(Vu8StreamReadPos!=0xff)
		          { //get offset 
				        tU32 Vu32OffsetRead=VspHeaderDataStreamRead[Vu8StreamReadPos].u32Offset;
				        tU32 Vu32LenghtRead=VspHeaderDataStreamRead[Vu8StreamReadPos].u32Lenght;				
				        //check lenght
				        if(Vu32LenghtRead>(tU32)Ps32SizeReadBuffer)
                  Vu32LenghtRead=(tU32)Ps32SizeReadBuffer;
	              //read offset
			          if(PDD_bReadFlash(vtspPddNorInfo,Vs32FoundCluster,PvpReadBuffer,PDD_NOR_USER_HEADER_SIZE+Vu32OffsetRead,Vu32LenghtRead)==TRUE)
				        {
				          Vs32ErrorCode=(tS32)Vu32LenghtRead;
                }
              }
            }
          }
		      free(VvpBufRead);
        }
      }
    }
  }
  if(Vs32ErrorCode < PDD_OK)   
  {
    tU8  Vu8Buffer[200];		
	  snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32ErrorCode,Vs32ErrorCode,PtsDataStreamName);
	  PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); ;
  }
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorUserAccessGetEraseCounter()
*
* DESCRIPTION: get erase sector count
*
* PARAMETERS:
*
* RETURNS: 
*      count 
*
* HISTORY:Created  2013 08 23
*****************************************************************************/
tU32 PDD_NorUserAccessGetEraseCounter(void)
{
  return(vtspPddNorInfo->u32EraseSectorCount);
}
/******************************************************************************
* FUNCTION: PDD_bFormatFlash()
*
* DESCRIPTION: format a sector
*
* PARAMETERS:
*      PtsDevInfo:     device info
*      Ps32SectorNum:  sector number
*
* RETURNS: 
*      TRUE:  success
*      FALSE: failed
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool  PDD_bFormatFlash(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32SectorNum)
{
  tBool                      VbResult = TRUE;
  tsPddNorUserHeaderMagic    VtsHeaderMagic;
  tS32                       Vs32Cluster = Ps32SectorNum*PtsDevInfo->s32NumClusterPerSector; /*get cluster*/

  /* check if flash formatted*/
  if(!PDD_bIsFlashFormated(PtsDevInfo,Ps32SectorNum))
  {
    int ViInc;
    tU8 Vu8FormatByte=0xff;
    /*for all cluster in the sector set the format begin flag */
    for(ViInc=0; ViInc < PtsDevInfo->s32NumClusterPerSector; ++ViInc)
    {
	    Vu8FormatByte = ~PDD_NOR_USER_STATE_FORMAT_BEGIN;
      /*write format begin to flash*/
      VbResult = PDD_bWriteFlash(PtsDevInfo,Vs32Cluster+ViInc,&Vu8FormatByte,PDD_NOR_USER_STATUS_POSITION_FORMAT_BEGIN,CHIP_ALIGNMENT);
    }
    /* erase sector */
    if(!PDD_bEraseFlash(PtsDevInfo,Ps32SectorNum))
    {
      VbResult = FALSE;
    }	
    /*for all cluster in the sector set the format end flag */
    for(ViInc=0; ViInc < PtsDevInfo->s32NumClusterPerSector; ++ViInc)
    {/* if nor error*/
      if(VbResult)
      { /*set magic*/
        VtsHeaderMagic.u16MagicLow=PDD_NOR_USER_MAGIC;
        VtsHeaderMagic.u8ChunkSize=(tU8)PtsDevInfo->u16ChunkSize;
        /*save counter erase and trace level*/   
		    VtsHeaderMagic.u32EraseSectorCount=PtsDevInfo->u32EraseSectorCount;				
		    PDD_TRACE(PDD_NOR_USER_DEBUG_ERASE_COUNTER,&PtsDevInfo->u32EraseSectorCount,sizeof(tU32));		
        /*write magic to flash*/
        VbResult = PDD_bWriteFlash(PtsDevInfo,Vs32Cluster+ViInc,(tU8*)&VtsHeaderMagic,PDD_NOR_USER_MAGIC_POSITION,sizeof(VtsHeaderMagic));
        /* if nor error*/
        if(VbResult)
        { /*write format end to flash*/
          Vu8FormatByte = ~PDD_NOR_USER_STATE_FORMAT_END;
          VbResult = PDD_bWriteFlash(PtsDevInfo,Vs32Cluster+ViInc,&Vu8FormatByte,PDD_NOR_USER_STATUS_POSITION_FORMAT_END,1);
        }
      }
    }/*end for*/
  }
  PDD_TRACE(PDD_NOR_USER_DEBUG_FORMAT_FLASH,&Ps32SectorNum,sizeof(Ps32SectorNum));
  PDD_TRACE(PDD_NOR_USER_DEBUG_RETURN,&VbResult,sizeof(VbResult));
  return(VbResult);
}
/******************************************************************************
* FUNCTION: PDD_bWriteToCluster()
*
* DESCRIPTION: write "data" to the next free cluster
*
* PARAMETERS:
*       PtsDevInfo   : device information
*       Ppu8Data     : pointer to the data (the size must be u32ClusterSizeData)
*
* RETURNS: 
*      TRUE:  success
*      FALSE: failed
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool  PDD_bWriteToCluster(tsPddNorUserInfo* PtsDevInfo,tU8* Ppu8Data)
{
  tS32   Vs32ClusterNum=-1;
  tU32   Vu32CrcCheckSum;
  tU8    Vu8StateValue;
  tBool  VbResult = TRUE;

  /*find next free cluster*/
  Vs32ClusterNum = PDD_s32FindNextFreeCluster(PtsDevInfo);
  /* check if cluster found*/
  if(Vs32ClusterNum < 0)
  { /* no free cluster found*/
    tS32 Vu32UnusedSector;
    /* calculate a sector that is not used by the last valid cluster */
    if(PtsDevInfo->s32LastClusterUsed >= 0)
    {/* we have a valid cluster read */
      tU32 Vu32UsedSector = ((PtsDevInfo->s32LastClusterUsed)/PtsDevInfo->s32NumClusterPerSector);
      Vu32UnusedSector = (Vu32UsedSector+1) % PtsDevInfo->u32NumBlocks;
    }
    else
    {/* we have no valid cluster, so take the first */
      Vu32UnusedSector = 0;
    }
    /* we have no free cluster, so format the first ! */
    if(PDD_bFormatFlash(PtsDevInfo,Vu32UnusedSector))
    {/* take the first cluster */      
      Vs32ClusterNum = Vu32UnusedSector * PtsDevInfo->s32NumClusterPerSector;
    }
    else
    {/* format failed */
      VbResult = FALSE;
    }
  } /* if (cluster_num < 0) */
  PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_OLD,&PtsDevInfo->s32LastClusterUsed,sizeof(PtsDevInfo->s32LastClusterUsed));
  PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_NEW,&Vs32ClusterNum,sizeof(Vs32ClusterNum));   
  /* if no error*/
  if(VbResult == TRUE)
  {/* set the old cluster to the old state */
    if(PtsDevInfo->s32LastClusterUsed >= 0 && PtsDevInfo->s32LastClusterUsed < PtsDevInfo->s32NumCluster)
    {
	    Vu8StateValue = ~PDD_NOR_USER_STATE_OLD_VERSION;     
      VbResult = PDD_bWriteFlash(PtsDevInfo,PtsDevInfo->s32LastClusterUsed, &Vu8StateValue,PDD_NOR_USER_STATUS_POSITION_OLD_VERSION, 1);       
    }
    /* calc crc */
    Vu32CrcCheckSum=PDD_ValidationCalcCrc32(Ppu8Data, PtsDevInfo->u32ClusterSizeData);
    /*write checksum*/
    if(VbResult)
    {
       tsPddNorUserHeaderChecksum VtsHeaderChecksum;
       /* put checksum and CRC to header */
       VtsHeaderChecksum.u32CheckSum = Vu32CrcCheckSum;
       VtsHeaderChecksum.u32CheckSumXor = ~Vu32CrcCheckSum;	 
       VbResult = PDD_bWriteFlash(PtsDevInfo,Vs32ClusterNum,(tU8*)&VtsHeaderChecksum,PDD_NOR_USER_CHECKSUM_POSITION,sizeof(VtsHeaderChecksum));
    }
    /*write state PDD_NOR_USER_STATE_WRITE_BEGIN */
    if(VbResult)
    {
      Vu8StateValue = ~PDD_NOR_USER_STATE_WRITE_BEGIN;     
      VbResult = PDD_bWriteFlash(PtsDevInfo,Vs32ClusterNum, &Vu8StateValue,PDD_NOR_USER_STATUS_POSITION_WRITE_BEGIN, 1); 
    }
    /* write the data stream */
    if(VbResult)
    {
      VbResult = PDD_bWriteFlash(PtsDevInfo,Vs32ClusterNum,Ppu8Data,PDD_NOR_USER_HEADER_SIZE, PtsDevInfo->u32ClusterSizeData);
    }
    /* set the new cluster to "write end" state */
    if(VbResult)
    {
	    Vu8StateValue = ~PDD_NOR_USER_STATE_WRITE_END;    
      VbResult = PDD_bWriteFlash(PtsDevInfo,Vs32ClusterNum,&Vu8StateValue, PDD_NOR_USER_STATUS_POSITION_WRITE_END, 1); 
    }
    /* if last cluster exist*/
    if(VbResult && (PtsDevInfo->s32LastClusterUsed >= 0) && (PtsDevInfo->s32LastClusterUsed < PtsDevInfo->s32NumCluster))
    {/* set it to "invalid" */
	    Vu8StateValue = ~PDD_NOR_USER_STATE_INVALID;    
      VbResult = PDD_bWriteFlash(PtsDevInfo,PtsDevInfo->s32LastClusterUsed,&Vu8StateValue, PDD_NOR_USER_STATUS_POSITION_INVALID, 1); 
    }
    if(VbResult)
    {/* set the new cluster as "last used" */
      PtsDevInfo->s32LastClusterUsed = Vs32ClusterNum;
    }   
  }
  else
  {
    VbResult = FALSE;
  }
  PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_WRITE_TO,&Vs32ClusterNum,sizeof(Vs32ClusterNum));
  PDD_TRACE(PDD_NOR_USER_DEBUG_RETURN,&VbResult,sizeof(VbResult));
  return(VbResult);
}
/******************************************************************************
* FUNCTION: PDD_bReadFromCluster()
*
* DESCRIPTION: read the data from the last valid cluster to "Ppu8Data"
*
* PARAMETERS:
*       PtsDevInfo   : device information
*       Ppu8Data     : pointer to the data (the size must be u32ClusterSizeData)
*
* RETURNS: 
*      TRUE:  success
*      FALSE: failed
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool  PDD_bReadFromCluster(tsPddNorUserInfo* PtsDevInfo,void* PvpData)
{
  tU32                           Vu32CrcCheckSum;
  tsPddNorUserHeaderChecksum     VtsHeaderChecksum;
  tBool                          VbResult = TRUE;
  tS32                           Vs32FoundCluster = -1;
  tU32                           Vu32ClusterSizeData=PtsDevInfo->u32ClusterSizeData;
   
  /*find valid cluster*/
  Vs32FoundCluster=PDD_s32FindValidCluster(PtsDevInfo);
  if(Vs32FoundCluster >= 0)
  { /*read checksum*/
    memset(&VtsHeaderChecksum,0,sizeof(tsPddNorUserHeaderChecksum));
    VbResult = PDD_bReadFlash(PtsDevInfo,Vs32FoundCluster,&VtsHeaderChecksum,PDD_NOR_USER_CHECKSUM_POSITION ,sizeof(VtsHeaderChecksum));
    if(VbResult && (VtsHeaderChecksum.u32CheckSum == (~VtsHeaderChecksum.u32CheckSumXor)))
    { /*read data from flash*/      
      VbResult = PDD_bReadFlash(PtsDevInfo,Vs32FoundCluster,PvpData, PDD_NOR_USER_HEADER_SIZE,Vu32ClusterSizeData);
      if(VbResult)
      {/* calc crc */
        Vu32CrcCheckSum=PDD_ValidationCalcCrc32(PvpData,Vu32ClusterSizeData);
        /*compare checksum*/
        VbResult = (Vu32CrcCheckSum == VtsHeaderChecksum.u32CheckSum);	
        /*checksum OK?*/
        if(VbResult==FALSE)	   
	      { //checksum error          
	        tS32 Vs32ErrorCode= PDD_ERROR_NOR_USER_CHECKSUM;         
          PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(Vs32ErrorCode)); 
		      PDD_SET_ERROR_ENTRY("NOR_USER: checksum error");
	      }
        /* the s32LastClusterUsed must be set always (It is also necessary, during checksum failure */
        /* Vs32FoundCluster must set to s32LastClusterUsed, because during next write cycle this s32LastClusterUsed should set to invalid*/
        /* see test u32PDDNorUserLoadDumpChecksumWrong */
        PtsDevInfo->s32LastClusterUsed = Vs32FoundCluster;           
        PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_LAST_USED,&PtsDevInfo->s32LastClusterUsed,sizeof(PtsDevInfo->s32LastClusterUsed));	            	     
      }
    }
    else
    {
      VbResult = FALSE;
    }
  }
  else
  {
    VbResult = FALSE;
  }
  PDD_TRACE(PDD_NOR_USER_DEBUG_RETURN,&VbResult,sizeof(VbResult));
  return(VbResult);
}

/******************************************************************************
* FUNCTION: PDD_bIsFlashFormated()
*
* DESCRIPTION: check if the sector is correct formatted
*
* PARAMETERS:
*        PtsDevInfo      : pointer of the device info   
*        Ps32SectorNum   : the number of the sector, 0 <= cluster < u32NumBlocks
*
* RETURNS: 
*      TRUE:  success
*      FALSE: failed
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool PDD_bIsFlashFormated(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32SectorNum)
{
  tS32        Vs32Inc;
  tS32        Vs32Cluster=Ps32SectorNum*PtsDevInfo->s32NumClusterPerSector; /*get cluster*/

  /*check parameter sector num */
  if(Ps32SectorNum >= (tS32)PtsDevInfo->u32NumBlocks)
  { /*sector number to great*/
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  /*set counter to zero */
  PtsDevInfo->u32EraseSectorCount=0;
  PDD_TRACE(PDD_NOR_USER_DEBUG_ERASE_COUNTER,&PtsDevInfo->u32EraseSectorCount,sizeof(tU32));
  /*for all cluster in one sector*/
  for(Vs32Inc=0; Vs32Inc < PtsDevInfo->s32NumClusterPerSector; ++Vs32Inc)
  { /*check if cluster formatted*/
    if(!PDD_bIsClusterFormated(PtsDevInfo,Vs32Cluster+Vs32Inc))
    {
      tS32 Vs32DebugsCluster=Vs32Cluster+Vs32Inc;
      PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_NOT_FORMATED,&Vs32DebugsCluster,sizeof(Vs32DebugsCluster));
      return FALSE;
    }
  }
  PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_FORMATED,&Ps32SectorNum,sizeof(Ps32SectorNum));
  return TRUE;
}
/******************************************************************************
* FUNCTION: PDD_s32FindValidCluster()
*
* DESCRIPTION: find valid cluster
*
* PARAMETERS:
*        PtsDevInfo : pointer of the device info   
*
* RETURNS: 
*      the number of the vaild cluster, -1 means no cluster is valid
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tS32   PDD_s32FindValidCluster(tsPddNorUserInfo* PtsDevInfo)
{
  tS32   Vs32FoundCluster = -1;
  tU8    Vu8FoundClusterIsNew = FALSE;
  tBool  VbIsNew=FALSE;
  tS32   Vs32Inc;
  tS32   Vs32NumCluster=PtsDevInfo->s32NumCluster;

  /* for all cluster, find a valid cluster*/
  for(Vs32Inc=0; Vs32Inc < Vs32NumCluster; ++Vs32Inc)
  {
    if(PDD_bIsClusterValid(PtsDevInfo,Vs32Inc,&VbIsNew))
    {
      if(Vs32FoundCluster >= 0)
      {/* take sector only if it is new than the found one */
        if(!Vu8FoundClusterIsNew && VbIsNew)
        {
          PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_REPLACE_OLD,&Vs32FoundCluster,sizeof(Vs32FoundCluster));
          PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_REPLACE_NEW,&Vs32Inc,sizeof(Vs32Inc));          
          Vu8FoundClusterIsNew = VbIsNew;
          Vs32FoundCluster = Vs32Inc;
        }
      }
      else
      {/* first valid sector found */
        Vu8FoundClusterIsNew = VbIsNew;
        Vs32FoundCluster = Vs32Inc;
      }
      if(Vu8FoundClusterIsNew)
      {/* more than a new cluster we cannot found */
        break;
      }
    }
  }
  PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_TO_READ,&Vs32FoundCluster,sizeof(Vs32FoundCluster));
  PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_TO_READ_STATUS,&Vu8FoundClusterIsNew,sizeof(Vu8FoundClusterIsNew));
  return(Vs32FoundCluster);
}
/******************************************************************************
* FUNCTION: PDD_s32FindNextFreeCluster()
*
* DESCRIPTION: find next free cluster
*
* PARAMETERS:
*        PtsDevInfo      : pointer of the device info   
*
* RETURNS: 
*      the number of the next cluster that is free, -1 means no cluster is free
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tS32   PDD_s32FindNextFreeCluster(tsPddNorUserInfo* PtsDevInfo)
{
  tS32 Vs32Return = PtsDevInfo->s32LastClusterUsed;
  tU32 Vu32Inc;
  tU32 Vu32NumberOfCluster = ((PtsDevInfo->s32LastClusterUsed == -1) ? (tU32)PtsDevInfo->s32NumCluster : (tU32)(PtsDevInfo->s32NumCluster-1));

  /*for each cluster*/
  for(Vu32Inc=0; Vu32Inc < Vu32NumberOfCluster; ++Vu32Inc)
  {
    Vs32Return = (Vs32Return+1)  % PtsDevInfo->s32NumCluster;
    /*check if cluster formatted*/
    if(PDD_bIsClusterFormated(PtsDevInfo,Vs32Return))
    {
      break;
    }
  }
  /* if no cluster found */
  if(Vu32Inc == Vu32NumberOfCluster)
  {
    Vs32Return = -1;
  }
  PDD_TRACE(PDD_NOR_USER_DEBUG_CLUSTER_FREE,&Vs32Return,sizeof(Vs32Return));
  return(Vs32Return);
}
/******************************************************************************
* FUNCTION: PDD_bIsClusterFormated()
*
* DESCRIPTION: check if cluster formated
*
* PARAMETERS:
*        PtsDevInfo   : pointer of the device info   
*        PtClusterNum : the number of the cluster, 0 <= cluster < s32NumCluster
*
* RETURNS: 
*      TRUE => cluster is formatted
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool  PDD_bIsClusterFormated(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32ClusterNum)
{
  tBool  VbReturn=FALSE;
  tU8    VubHeader[PDD_NOR_USER_HEADER_SIZE];

  /*read header*/
  memset(&VubHeader[0],0,PDD_NOR_USER_HEADER_SIZE);
  if(!PDD_bReadFlash(PtsDevInfo,Ps32ClusterNum ,&VubHeader[0],PDD_NOR_USER_MAGIC_POSITION,PDD_NOR_USER_HEADER_SIZE))
  {/* error read */
    VbReturn=FALSE;
  }
  else
  {
    tsPddNorUserHeaderMagic*  VtspHeaderMagic=(tsPddNorUserHeaderMagic*)(void*)&VubHeader[PDD_NOR_USER_MAGIC_POSITION];		
    /*check magic*/
    if((VtspHeaderMagic->u8ChunkSize == (tU8)CHIP_ALIGNMENT)&&(VtspHeaderMagic->u16MagicLow == PDD_NOR_USER_MAGIC))
    { /* get erase sector count and trace level*/
	    PtsDevInfo->u32EraseSectorCount=VtspHeaderMagic->u32EraseSectorCount;
	    PDD_TRACE(PDD_NOR_USER_DEBUG_ERASE_COUNTER,&PtsDevInfo->u32EraseSectorCount,sizeof(tU32));
	    /*check formated*/
      if((VubHeader[PDD_NOR_USER_STATUS_POSITION_FORMAT_BEGIN]!=(tU8)(~PDD_NOR_USER_STATE_FORMAT_BEGIN))
         &&(VubHeader[PDD_NOR_USER_STATUS_POSITION_FORMAT_END]==(tU8)(~PDD_NOR_USER_STATE_FORMAT_END))
         &&(VubHeader[PDD_NOR_USER_STATUS_POSITION_WRITE_BEGIN]!=(tU8)(~PDD_NOR_USER_STATE_WRITE_BEGIN))
         &&(VubHeader[PDD_NOR_USER_STATUS_POSITION_WRITE_END]!=(tU8)(~PDD_NOR_USER_STATE_WRITE_END))
         &&(VubHeader[PDD_NOR_USER_STATUS_POSITION_INVALID]!=(tU8)(~PDD_NOR_USER_STATE_INVALID))
         &&(VubHeader[PDD_NOR_USER_STATUS_POSITION_OLD_VERSION]!=(tU8)(~PDD_NOR_USER_STATE_OLD_VERSION)))
      {       	
		    /*cluster formated*/
		    VbReturn=TRUE;
      }
    }/*end if*/
  }/*end else*/ 
  return(VbReturn);
}
/******************************************************************************
* FUNCTION: PDD_bIsClusterValid()
*
* DESCRIPTION: check if cluster valid
*
* PARAMETERS:
*        PtsDevInfo   : pointer of the device info   
*        PtClusterNum : the number of the cluster, 0 <= cluster < s32NumCluster
*        PpbIsNew     : pointer to bool whether block has flags as "new"
*
* RETURNS: 
*      TRUE => cluster is valid
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool  PDD_bIsClusterValid(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32ClusterNum,tBool* PpbIsNew)
{
  tBool  VbReturn=FALSE;
  tU8    VubHeader[PDD_NOR_USER_HEADER_SIZE];

  /*read header*/
  memset(&VubHeader[0],0,PDD_NOR_USER_HEADER_SIZE);
  if(!PDD_bReadFlash(PtsDevInfo,Ps32ClusterNum ,&VubHeader[0],PDD_NOR_USER_MAGIC_POSITION,PDD_NOR_USER_HEADER_SIZE))
  {/* error read */
    VbReturn=FALSE;
  }
  else
  {
    tsPddNorUserHeaderMagic*  VtspHeaderMagic=(tsPddNorUserHeaderMagic*)(void*)&VubHeader[PDD_NOR_USER_MAGIC_POSITION];
	  /* get state new */
    *PpbIsNew = (VubHeader[PDD_NOR_USER_STATUS_POSITION_OLD_VERSION]&(tU8)(PDD_NOR_USER_STATE_OLD_VERSION)) != 0;
    /* if magic correct*/
    if((VtspHeaderMagic->u8ChunkSize == (tU8)CHIP_ALIGNMENT)&&(VtspHeaderMagic->u16MagicLow == PDD_NOR_USER_MAGIC))
    { /* get erase sector count*/
	    PtsDevInfo->u32EraseSectorCount=VtspHeaderMagic->u32EraseSectorCount;	 
	    PDD_TRACE(PDD_NOR_USER_DEBUG_ERASE_COUNTER,&PtsDevInfo->u32EraseSectorCount,sizeof(tU32));	
	    /*check state*/
      if((VubHeader[PDD_NOR_USER_STATUS_POSITION_FORMAT_BEGIN]!=(tU8)(~PDD_NOR_USER_STATE_FORMAT_BEGIN))
         &&(VubHeader[PDD_NOR_USER_STATUS_POSITION_FORMAT_END]==(tU8)(~PDD_NOR_USER_STATE_FORMAT_END))
         &&(VubHeader[PDD_NOR_USER_STATUS_POSITION_WRITE_BEGIN]==(tU8)(~PDD_NOR_USER_STATE_WRITE_BEGIN))
         &&(VubHeader[PDD_NOR_USER_STATUS_POSITION_WRITE_END]==(tU8)(~PDD_NOR_USER_STATE_WRITE_END))
         &&(VubHeader[PDD_NOR_USER_STATUS_POSITION_INVALID]!=(tU8)(~PDD_NOR_USER_STATE_INVALID)))
      { /*cluster valid*/
	      VbReturn=TRUE;
      }
    }
    else
    {
      VbReturn=FALSE;
    }
  }
  return(VbReturn);
}

/******************************************************************************
* FUNCTION: PDD_bReadFlash()
*
* DESCRIPTION: read from flash
*
* PARAMETERS:
*     PtsDevInfo    : pointer of the device info
*     Ps32Cluster   : the number of the cluster, 0 <= cluster < s32NumCluster
*     PvpDestAdress : address to read the byte
*     Pu32Offset    : offset in the cluster
*     Pu32Len       : byte to read
*
* RETURNS: 
*      TRUE => all OK
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool  PDD_bReadFlash(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32Cluster,void* PvpDestAdress,tU32 Pu32Offset, tU32 Pu32Len)
{
  tBool VbReturn=FALSE;
  tS32  Vs32NumCluster=PtsDevInfo->s32NumCluster;
  tU32  Vu32ClusterSize=PtsDevInfo->u32ClusterSize;

  /*check cluster range*/ 
  if(Ps32Cluster < Vs32NumCluster)
  {	
    if(M_PDD_NOR_ACCESS_READ(vthNorAccess,PvpDestAdress,Pu32Len,(tU32)Ps32Cluster * Vu32ClusterSize + Pu32Offset)==0)
	  { /*success*/
	    VbReturn=TRUE;	  
	  }
	  if(!VbReturn)
	  {
	    PDD_SET_ERROR_ENTRY("NOR_USER: read from nor fails");
	  }
  }
  return(VbReturn);
}
/******************************************************************************
* FUNCTION: PDD_bWriteFlash()
*
* DESCRIPTION: write to flash
*
* PARAMETERS:
*   PtsDevInfo      : pointer of the driver info
*   Ps32Cluster     : the number of the cluster, 0 <= cluster < s32NumCluster
*   Pu8SourceAdress : address to write from
*   Pu32Offset      : offset in the cluster
*   Pu32Len         : byte to write
*
* RETURNS: 
*      TRUE => all OK
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool PDD_bWriteFlash(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32Cluster, tU8 *Pu8SourceAdress, tU32 Pu32Offset, tU32 Pu32Len)
{
  tBool VbReturn=FALSE;

  if(Ps32Cluster < PtsDevInfo->s32NumCluster)
  {
    tU8* VubpWriteBuffer=(tU8*)malloc((ALIGN_TO_CHIP(Pu32Len)));
    if(VubpWriteBuffer==NULL)
    { /*system: no space available*/
	    tS32 Vs32ErrorCode= PDD_ERROR_NO_BUFFER;
	    PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
      PDD_FATAL_M_ASSERT_ALWAYS();
    }
    else
    {
      memset(VubpWriteBuffer,0xff,ALIGN_TO_CHIP(Pu32Len));
      memmove(VubpWriteBuffer,Pu8SourceAdress,Pu32Len);
      /*write to flash*/	  
      if(M_PDD_NOR_ACCESS_WRITE(vthNorAccess,VubpWriteBuffer,ALIGN_TO_CHIP(Pu32Len),(tU32)Ps32Cluster * PtsDevInfo->u32ClusterSize + Pu32Offset)==0)
	    {	   
	      VbReturn=TRUE;			
	    }
      free(VubpWriteBuffer);
	    /*check error */
	    if(VbReturn==FALSE)
	    { /* LFX_write failed */
	      PDD_SET_ERROR_ENTRY("NOR_USER: write to nor fails");
	    }
    }
  }
  return(VbReturn);
}
/******************************************************************************
* FUNCTION: PDD_bEraseFlash()
*
* DESCRIPTION: write to flash
*
* PARAMETERS:
*   PtsDevInfo      : pointer of the driver info
*   Ps32SectorNum     : the number of the sector, 0 <= cluster < u32NumBlocks
*
* RETURNS: 
*       TRUE => flash at sectornum is erased
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool  PDD_bEraseFlash(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32SectorNum)
{
  tBool VbReturn=FALSE;
  if(Ps32SectorNum >= (tS32) PtsDevInfo->u32NumBlocks)
  { /*sector number to great*/
    PDD_FATAL_M_ASSERT_ALWAYS();
  }

  if(M_PDD_NOR_ACCESS_ERASE(vthNorAccess, PtsDevInfo->u32SectorSize, ((tU32)Ps32SectorNum)*PtsDevInfo->u32SectorSize)==0)
  {   
    VbReturn=TRUE;
    PDD_TRACE(PDD_NOR_USER_DEBUG_ERASE_FLASH,&Ps32SectorNum,sizeof(Ps32SectorNum));
	  PDD_vCheckTimeEraseSector(PtsDevInfo,Ps32SectorNum);	
  }
  PDD_TRACE(PDD_NOR_USER_DEBUG_RETURN,&VbReturn,sizeof(VbReturn));
  if(VbReturn==FALSE)
  { /* M_PDD_NOR_ACCESS_ERASE failed */
    PDD_SET_ERROR_ENTRY("NOR_USER: erase nor fails");;
  }
  else
  {/*increment counter erase for the sector*/  
    PtsDevInfo->u32EraseSectorCount=PtsDevInfo->u32EraseSectorCount+1;
	  PDD_TRACE(PDD_NOR_USER_DEBUG_ERASE_COUNTER,&PtsDevInfo->u32EraseSectorCount,sizeof(tU32));
	  if(PtsDevInfo->u32EraseSectorCount>=PDD_NOR_USER_MAX_SECTOR_ERASE_COUNTER)
	  { /*trace out and add errory memory entry if greater PDD_NOR_USER_MAX_SECTOR_ERASE_COUNTER and multiple from 0x100*/
	    tU32 Vu32Count=PtsDevInfo->u32EraseSectorCount;
	    /*trace out only all 128 time */
	    if((Vu32Count&PDD_NOR_USER_MAX_SECTOR_ERASE_COUNTER_MASK)==0)
	    {
	      tU8 Vu8Buffer[40];		
		    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"SectorEraseCounter to high:%d",PtsDevInfo->u32EraseSectorCount); 
		    PDD_TRACE(PDD_NOR_USER,Vu8Buffer,strlen(Vu8Buffer)+1);	
		    PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
      }
    }
  }
  return(VbReturn);
}
/******************************************************************************
* FUNCTION: PDD_u8GetCheckDataStreamDefinedHeader()
*
* DESCRIPTION: check the datastream name and get the table number in the defined header
*
* PARAMETERS:
*   PtsDevInfo: pointer data stream
*
* RETURNS: 
*       table number 
*
* HISTORY:Created  2013 04 25
*****************************************************************************/
static tU8    PDD_u8GetCheckDataStreamDefinedHeader(const char* PtsDataStreamName,tsPddNorUserInfo* PtsDevInfo)
{
  tU8                            Vu8DataStream=0xff;
  tU8                            Vu8Inc;
  tsPddNorUserHeaderDataStream*  VspHeaderDataStream;
  char*                          VstrPosFolder=strrchr((char*)PtsDataStreamName,'/'); //compare without folder
 
  /*check datastream with folder */
  if(VstrPosFolder!=NULL)
  {
    PtsDataStreamName=VstrPosFolder+1;
  }
  /*trace out header */
  PDD_TRACE(PDD_NOR_USER_DEBUG_DATA_STREAM_HEADER,&PtsDevInfo->sHeaderDataStream[0],PDD_NOR_USER_HEADER_SIZE_DATA_STREAM);
  /* for each datastream*/
  #ifndef PDD_TESTMANGER_ACTIVE
  for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vu8Inc++)
  #else
  for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM;Vu8Inc++)
  #endif
  {
    VspHeaderDataStream=&PtsDevInfo->sHeaderDataStream[Vu8Inc];
    if(strncmp(VspHeaderDataStream->u8DataStreamName,PtsDataStreamName,PDD_NOR_MAX_SIZE_NAME)==0)
	  {/*filename are identical*/
	    Vu8DataStream=Vu8Inc;
	    break;
	  }			  
  }		
  return(Vu8DataStream);
}
/******************************************************************************
* FUNCTION: PDD_u8GetCheckDataStreamReadHeader()
*
* DESCRIPTION: check the datastream name and get the table number in the read header
*
* PARAMETERS:
*   PtsDevInfo: pointer data stream
*
* RETURNS: 
*       table number 
*
* HISTORY:Created  2013 04 25
*****************************************************************************/
static tU8    PDD_u8GetCheckDataStreamReadHeader(char* PtsDataStreamNameDefined,tsPddNorUserHeaderDataStream* PspHeaderDataStreamRead)
{
  tU8  Vu8Inc;   
  tU8  Vu8NumberDataStreamRead;   
  tU8  Vu8DataStream=0xff;

  /*get number of read datastreams (=first offset/size for one header)*/
  Vu8NumberDataStreamRead=(tU8)(PspHeaderDataStreamRead->u32Offset/(sizeof(tsPddNorUserHeaderDataStream)));
  /*trace out header */
  PDD_TRACE(PDD_NOR_USER_DEBUG_NUMBER_READ_DATA_STREAM,&Vu8NumberDataStreamRead,sizeof(tU8));
  /*check if size defined name !=0 */
  if(strlen(PtsDataStreamNameDefined)!=0)
  { /* for each datastream*/
    for(Vu8Inc=0;Vu8Inc<Vu8NumberDataStreamRead;Vu8Inc++)
    {
      if(memcmp(PspHeaderDataStreamRead->u8DataStreamName,PtsDataStreamNameDefined,strlen(PtsDataStreamNameDefined))==0)
	  {/*filename are identical*/
	    Vu8DataStream=Vu8Inc;
	    break;
	  }
	  PspHeaderDataStreamRead++;
	}/*end for*/
  } 
  return(Vu8DataStream);
}
/******************************************************************************
* FUNCTION: PDD_bReadDataStreamConfiguration()
*
* DESCRIPTION: read data configuration from flash
*
* PARAMETERS:
*   PtsDevInfo: pointer of the driver info
*
* RETURNS: 
*       TRUE => configuration read success 
*
* HISTORY:Created  2013 04 25
*****************************************************************************/
tBool  PDD_bReadDataStreamConfiguration(tsPddNorUserInfo* PtsDevInfo)
{
  tBool                          VbReturn=TRUE;
  tU8                            Vu8Inc;   
  tU8                            Vu8StreamReadPos;   
  tsPddNorUserHeaderDataStream*  VspHeaderDataStreamDefined=&PtsDevInfo->sHeaderDataStream[0];
  tsPddNorUserHeaderDataStream*  VspHeaderDataStreamRead;
  void*                          VvpBufRead;

  if(VspHeaderDataStreamDefined->u32Offset==0)
  { /* offset and lenght not set =>read from flash*/
    /* allocate buffer*/
    VvpBufRead=malloc(vtspPddNorInfo->u32ClusterSizeData);
    /* check buffer*/
    if(VvpBufRead==NULL)
    { /*system: no space available*/
      tS32 Vs32ErrorCode= PDD_ERROR_NO_BUFFER;
	    PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	    PDD_FATAL_M_ASSERT_ALWAYS();
    }
    else
    { /* read complete cluster data*/
      if(PDD_bReadFromCluster(PtsDevInfo,VvpBufRead)==TRUE)
      {	
	      tU32  Vu32Offset=0;
		    tU32  Vu32OffsetRead=0;
		    /*get the header of the read datastream */    	    
		    VspHeaderDataStreamRead=(tsPddNorUserHeaderDataStream*)VvpBufRead;
		    /*for each defined datastream */
		    #ifdef PDD_TESTMANGER_ACTIVE
		    if(PDD_bGetNumberDataStreamTmAndNameDataStream((tsPddNorUserHeaderDataStream*)VvpBufRead)<=0)
		    {
		      VbReturn=FALSE;
		    }
		    else
		    {
          for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_NUMBER_DATASTREAM_TM;Vu8Inc++) 
          #else		
	        for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vu8Inc++)
          #endif
		      {/*check if exist in read datastream and get the position*/		  
		        VspHeaderDataStreamDefined=&PtsDevInfo->sHeaderDataStream[Vu8Inc];
            Vu8StreamReadPos=PDD_u8GetCheckDataStreamReadHeader((char*)VspHeaderDataStreamDefined->u8DataStreamName,(tsPddNorUserHeaderDataStream*)VvpBufRead);        
		        /*check if datastream exist*/
		        if(Vu8StreamReadPos!=0xff)
		        { /*datastream exists*/
			        tU8* Vu8pBufRead=(tU8*)VvpBufRead;
			        /*get lenght, count and offset*/
			        Vu32OffsetRead=VspHeaderDataStreamRead[Vu8StreamReadPos].u32Offset;
              VspHeaderDataStreamDefined->u32Lenght=VspHeaderDataStreamRead[Vu8StreamReadPos].u32Lenght;
              VspHeaderDataStreamDefined->u32Offset=PDD_NOR_USER_HEADER_SIZE_DATA_STREAM+Vu32Offset;
              VspHeaderDataStreamDefined->u32Count=VspHeaderDataStreamRead[Vu8StreamReadPos].u32Count;			
              /*copy read data into old buffer*/
			        memmove(&PtsDevInfo->u8pBufferDataOld[VspHeaderDataStreamDefined->u32Offset],(Vu8pBufRead+Vu32OffsetRead),VspHeaderDataStreamDefined->u32Lenght);	
			        /*get new offset*/
			        Vu32Offset+=VspHeaderDataStreamDefined->u32Lenght;	
            }
		        else
		        {/*datastream doesn't exist*/		   
			        VspHeaderDataStreamDefined->u32Lenght=0;
              VspHeaderDataStreamDefined->u32Offset=PDD_NOR_USER_HEADER_SIZE_DATA_STREAM+Vu32Offset;
              VspHeaderDataStreamDefined->u32Count=0;	
            }
          }/*end for*/		
	        #ifdef PDD_TESTMANGER_ACTIVE
        }/*end else*/
        #endif
      }/*end if*/
	    else
	    { /* no data saved => set to default*/
        /* for each datastream*/
	      #ifndef PDD_TESTMANGER_ACTIVE
        for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vu8Inc++)
        #else
        vu8NumberDataStreamTm=PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM_TM;
	      for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM_TM;Vu8Inc++)
        #endif
        {	     
          VspHeaderDataStreamDefined=&PtsDevInfo->sHeaderDataStream[Vu8Inc];	  
		      VspHeaderDataStreamDefined->u32Lenght=0;
		      VspHeaderDataStreamDefined->u32Offset=PDD_NOR_USER_HEADER_SIZE_DATA_STREAM;
		      VspHeaderDataStreamDefined->u32Count=0;
        }				
      }	 
	    /* memmove new header into old buffer*/
	    memmove(&PtsDevInfo->u8pBufferDataOld[0],&PtsDevInfo->sHeaderDataStream[0],PDD_NOR_USER_HEADER_SIZE_DATA_STREAM);	
	    /* check if data new => header changed*/
	    if(memcmp(&PtsDevInfo->u8pBufferDataOld[0],VvpBufRead, vtspPddNorInfo->u32ClusterSizeData) != 0)
	    {/*save data*/
        if((PDD_bWriteToCluster(vtspPddNorInfo,PtsDevInfo->u8pBufferDataOld))==FALSE)
        {/* error */
          tS32 Vs32ErrorCode=PDD_ERROR_NOR_USER_WRITE_TO_CLUSTER;
	        PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
        }
	    }
	  }	
	  free(VvpBufRead);	
  }/*end if offset unknown*/
  PDD_TRACE(PDD_NOR_USER_DEBUG_DATA_STREAM_HEADER,&PtsDevInfo->sHeaderDataStream[0],PDD_NOR_USER_HEADER_SIZE_DATA_STREAM);
  return(VbReturn);
}

/******************************************************************************
* FUNCTION: PDD_s32UpdateHeaderDataStream()
*
* DESCRIPTION: check the datastream name and get the tabel number
*
* PARAMETERS:
*   PtsDevInfo      : pointer data stream
*
* RETURNS: 
*       PDD_NOR_USER_UPDATE_HEADER                 => update header
*       PDD_OK                                     => no update header
*       PDD_ERROR_NOR_USER_NO_SPACE_CLUSTER_FULL   => size to great
* 
*
* HISTORY:Created  2013 04 25
*****************************************************************************/
static tS32   PDD_s32UpdateHeaderDataStream(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32SizeWrite,tU8 Pu8DataStream)
{
  tS32                           Vs32ReturnCode=PDD_OK;
  tU8                            Vu8Inc;
  tU32                           Vu32Size;
  tU32                           Vu32Offset;
  tU32                           Vu32MaxSize;
  tsPddNorUserHeaderDataStream*  VspHeaderDataStream=&PtsDevInfo->sHeaderDataStream[Pu8DataStream];

  /*get size and offset from actual header */
  Vu32Size=VspHeaderDataStream->u32Lenght;	
  Vu32Offset=VspHeaderDataStream->u32Offset;	
  /* new size? => Header should be updatete */
  if(Vu32Size!=Ps32SizeWrite)
  { /*check if new lenght fit into cluster size; maxsize= (lastoffset + lastlength) + Diff(newsize-oldsize)*/
    Vu32MaxSize=(tU32)Ps32SizeWrite-Vu32Size; 
	  #ifndef PDD_TESTMANGER_ACTIVE
	  VspHeaderDataStream=&PtsDevInfo->sHeaderDataStream[PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM-1];
    #else
    VspHeaderDataStream=&PtsDevInfo->sHeaderDataStream[PDD_NOR_USER_NUMBER_DATASTREAM_TM-1];
    #endif
    Vu32MaxSize=Vu32MaxSize+VspHeaderDataStream->u32Offset+VspHeaderDataStream->u32Lenght;
	  if(Vu32MaxSize >= PtsDevInfo->u32ClusterSizeData)
	  { /* error no space cluster full*/
	    Vs32ReturnCode=PDD_ERROR_NOR_USER_NO_SPACE_CLUSTER_FULL;
	    PDD_TRACE(PDD_ERROR,&Vs32ReturnCode,sizeof(tS32));
	    //NORMAL_M_ASSERT_ALWAYS();
	  }
	  else
	  { /* change header datastream offset and lenght */
	    /* for each datastream*/
	    #ifndef PDD_TESTMANGER_ACTIVE
      for(Vu8Inc=Pu8DataStream;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vu8Inc++)
      #else
	    for(Vu8Inc=Pu8DataStream;Vu8Inc<PDD_NOR_USER_NUMBER_DATASTREAM_TM;Vu8Inc++)
      #endif
      {
        VspHeaderDataStream=&PtsDevInfo->sHeaderDataStream[Vu8Inc];
		    /* save new size*/
		    if(Vu8Inc==Pu8DataStream)
		    {
		      VspHeaderDataStream->u32Lenght=Ps32SizeWrite;
		    }
		    /*set new offset*/
		    VspHeaderDataStream->u32Offset=Vu32Offset;
		    /*determine new offset*/
		    Vu32Offset=VspHeaderDataStream->u32Offset+VspHeaderDataStream->u32Lenght;		
      }
	    Vs32ReturnCode=PDD_NOR_USER_UPDATE_HEADER;
	    /*trace out header */
	    PDD_TRACE(PDD_NOR_USER_DEBUG_DATA_STREAM_HEADER,&PtsDevInfo->sHeaderDataStream[0],PDD_NOR_USER_HEADER_SIZE_DATA_STREAM);
    }
  }
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: PDD_vUpdateData()
*
* DESCRIPTION: check the datastream name and get the tabel number
*
* PARAMETERS:
*   PtsDevInfo      : pointer data stream
*
* RETURNS: 
*       TRUE => configuration read success 
*
* HISTORY:Created  2013 04 26
*****************************************************************************/
static void  PDD_vUpdateData(tsPddNorUserInfo* PtsDevInfo,void* PvpWriteBuffer)
{
  tsPddNorUserHeaderDataStream*  VspHeaderDataStreamNew;
  tsPddNorUserHeaderDataStream*  VspHeaderDataStreamOld=(tsPddNorUserHeaderDataStream*) PvpWriteBuffer;
  tU8*                           Vu8pWriteBuffer=(tU8*) PvpWriteBuffer;
  tU8                            Vu8Inc;
  tS32                           Vs32SizeCopy=-1;
  /* for each datastream*/
  #ifndef PDD_TESTMANGER_ACTIVE
  for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vu8Inc++)
  #else
  for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_NUMBER_DATASTREAM_TM;Vu8Inc++)
  #endif
  {
    VspHeaderDataStreamNew=&PtsDevInfo->sHeaderDataStream[Vu8Inc];
	  /*if offset different*/
	  if(VspHeaderDataStreamNew->u32Offset!=VspHeaderDataStreamOld->u32Offset)
	  {/*copy data from old buffer to new buffer*/	
	    Vs32SizeCopy=VspHeaderDataStreamNew->u32Lenght;
	    if(VspHeaderDataStreamNew->u32Lenght > VspHeaderDataStreamOld->u32Lenght)
	    { 
	      Vs32SizeCopy=VspHeaderDataStreamOld->u32Lenght;
	    }	 
	    if(VspHeaderDataStreamNew->u32Offset > PtsDevInfo->u32ClusterSize)
	    {  /* error no space cluster full*/
         PDD_SET_ERROR_ENTRY("NOR_USER: no space cluster full");
	       PDD_FATAL_M_ASSERT_ALWAYS();
	    }
      if(VspHeaderDataStreamOld->u32Offset > PtsDevInfo->u32ClusterSize)
	    { /* error no space cluster full*/
        PDD_SET_ERROR_ENTRY("NOR_USER: no space cluster full");
	      PDD_FATAL_M_ASSERT_ALWAYS();
	    }
      memmove(Vu8pWriteBuffer+VspHeaderDataStreamNew->u32Offset,
              PtsDevInfo->u8pBufferDataOld+VspHeaderDataStreamOld->u32Offset,
			        Vs32SizeCopy);	 	 
	  }
	  /*increment pointer*/
	  VspHeaderDataStreamOld++;
  }/*end for*/
  if(Vs32SizeCopy!=-1)
  { 
    VspHeaderDataStreamNew=&PtsDevInfo->sHeaderDataStream[Vu8Inc-1];
	  tU32 Vu32Offset=VspHeaderDataStreamNew->u32Offset+VspHeaderDataStreamNew->u32Lenght;
	  tS32 Vs32Size=vtspPddNorInfo->u32ClusterSizeData-Vu32Offset-1;	
    memset(Vu8pWriteBuffer+Vu32Offset,0xff,Vs32Size);
  }
}
/******************************************************************************
* FUNCTION: PDD_vCheckTimeEraseSector()
*
* DESCRIPTION: For the Endurance and Retention of the Spansion NOR: 
*              It should be avoid, that one sector will be erased within 90s.
*              see document "Endurance and Retention Management and Validation"
*              This function check this timing and make an error memory entry, 
*              if the time under 90 s.
*
* PARAMETERS:
*   PtsDevInfo      : pointer data stream
*
*
* HISTORY:Created  2015 03 23
*****************************************************************************/
static void   PDD_vCheckTimeEraseSector(tsPddNorUserInfo* PtsDevInfo,tS32 Ps32SectorNum)
{
  #ifndef PDD_TESTMANGER_ACTIVE
  struct timespec VTimer={0,0};
  /*get actuel start time*/
  clock_gettime(CLOCK_MONOTONIC, &VTimer); 
  /*if sector erased since startup*/ 
  if(PtsDevInfo->u32EraseSectorLastTime[Ps32SectorNum] > 0)
  { /*check difference < 90 s => warning error error memory*/
    tU32 Vu32TimeDiff= VTimer.tv_sec-PtsDevInfo->u32EraseSectorLastTime[Ps32SectorNum];
    if(Vu32TimeDiff < PDD_NOR_LIMIT_TIME_ERASE_CYCLE)
    {/*pepare error memory entry*/
      tU8 Vu8Buffer[100];		
	    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"NOR_USER: Sector %d erased within 90s (%ds,%ds)",Ps32SectorNum,PtsDevInfo->u32EraseSectorLastTime[Ps32SectorNum], VTimer.tv_sec); 
      PDD_TRACE(PDD_NOR_USER,Vu8Buffer,strlen(Vu8Buffer)+1);	
      PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
	  }
  }
  /* save erased time for the sector into shared memory*/
  PtsDevInfo->u32EraseSectorLastTime[Ps32SectorNum]=(tU32) VTimer.tv_sec;
  #endif
}
#ifdef PDD_TESTMANGER_ACTIVE
/******************************************************************************
* FUNCTION: PDD_bGetNumberDataStreamTmAndNameDataStream()
*
* DESCRIPTION: determine number of datastream for testmanager and get datastream names
*
* PARAMETERS:
*   PspHeaderDataStreamRead      : pointerbeginn header stream
*
*
* HISTORY:Created  2014 04 28
*****************************************************************************/
static tBool   PDD_bGetNumberDataStreamTmAndNameDataStream(tsPddNorUserHeaderDataStream* PspHeaderDataStreamRead)
{
  tU8     Vu8Inc;      
  tU8     Vu8DataStreamKDS=0xff;
  tBool   VbReturn=TRUE;
 
  /*get number of read datastreams (=first offset/size for one header)*/
  vu8NumberDataStreamTm=(tU8)(PspHeaderDataStreamRead->u32Offset/(sizeof(tsPddNorUserHeaderDataStream)));
  /*trace out header */
  PDD_TRACE(PDD_NOR_USER_DEBUG_NUMBER_READ_DATA_STREAM,&Vu8NumberDataStreamRead,sizeof(tU8));
  /*get pointer defined */
  tsPddNorUserHeaderDataStream *VspHeaderDataStreamDefined;
  /* for each datastream*/
  for(Vu8Inc=0;Vu8Inc<vu8NumberDataStreamTm;Vu8Inc++)
  { /*get pointer defined */
    VspHeaderDataStreamDefined=&vtspPddNorInfo->sHeaderDataStream[Vu8Inc];
    /*copy read name to defined header*/
    memmove(VspHeaderDataStreamDefined->u8DataStreamName,PspHeaderDataStreamRead->u8DataStreamName,strlen((char*)PspHeaderDataStreamRead->u8DataStreamName));
    if(memcmp(PspHeaderDataStreamRead->u8DataStreamName,PDD_NOR_USER_DATASTREAM_NAME_KDS,strlen(PDD_NOR_USER_DATASTREAM_NAME_KDS))==0)
	  {/*filename are identical kds found*/
	    Vu8DataStreamKDS=Vu8Inc;
	  }		
	  PspHeaderDataStreamRead++;
  }		
  if(Vu8DataStreamKDS==0xff)
  {/*KDS not found increment kds datastream*/
    vu8NumberDataStreamTm++;
  }
  if(vu8NumberDataStreamTm>PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM)
  {
    VbReturn=FALSE;
  }
  return(VbReturn);
}
#endif
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/* End of File pdd_access_nor_user.c                                          */
/******************************************************************************/