/**
 * @copyright    (C) 2014 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        This file contains the access to the nor_kernel nor, which saves careful data 
 *               into the nor flash. 
 * @addtogroup   PDD access
 * @{
 */ 

 /** global function:
 * -- PDD_NorKernelAccessInit:
 *       init routine for nor_kernel flash access 
 * -- PDD_NorKernelAccessDeInit:
 *       deinit routine for nor_kernel flash access
 * -- PDD_NorKernelAccessGetDataStreamSize:
 *       get size of data, without header 
 * -- PDD_NorKernelAccessErase:
 *       erase all sector
 * -- PDD_NorKernelAccessReadDataStream:
 *       function for read from nor_kernel nor 
 * -- PDD_NorKernelAccessWriteDataStream:
 *       function for write to nor_kernel nor
 * -- PDD_NorKernelAccessGetEraseCounter
 *       get erase counter for trace     
 */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <limits.h>   
#include "system_types.h"
#include "system_definition.h"
#include "LFX2.h"
#include "pdd_variant.h"
#include "pdd.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#ifdef PDD_NOR_KERNEL_POOL_EXIST  
#include "DataPoolKernel.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
#ifdef PDD_NOR_KERNEL_POOL_EXIST  
#define PDD_NOR_KERNEL_MIN_SECTOR_SIZE         0x20000 //  0x20000 128k  
#define PDD_NOR_KERNEL_MIN_NUMBER_OF_SECTOR    2
#define PDD_NOR_KERNEL_MIN_CHUNK_SIZE          0x20
#define PDD_NOR_KERNEL_HEADER_SIZE             ((tS32)sizeof(TPddKernel_Header))

/*define for alignement*/
#define CHIP_ALIGNMENT (vtspPddNorInfo->u16ChunkSize)
#define CHIP_ALIGNMENT_MASK (~(CHIP_ALIGNMENT-1))
#define ALIGN_TO_CHIP(x) ((x)+(CHIP_ALIGNMENT-1) & CHIP_ALIGNMENT_MASK)


#define PDD_NOR_KERNEL_MAX_SECTOR_ERASE_COUNTER          0xc380 //50048
#define PDD_NOR_KERNEL_MAX_SECTOR_ERASE_COUNTER_MASK     0x7F   //128

#define PDD_NOR_KERNEL_SECTOR0               0x00
#define PDD_NOR_KERNEL_SECTOR1               0x01
#endif
/******************************************************************************* 
|typedef 
|------------------------------------------------------------------------------*/

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/
#ifdef PDD_NOR_KERNEL_POOL_EXIST  
static tsPddNorKernelInfo*        vtspPddNorInfo=NULL;
static LFXhandle                  vthLfx=NULL;
#endif

/************************************************************************
| variable definition (scope: module-global)
|-----------------------------------------------------------------------*/


/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
#ifdef PDD_NOR_KERNEL_POOL_EXIST 
static tBool  PDD_bSynchronisation(tsPddNorKernelInfo* PtsDevInfo);
static tS32   PDD_s32ReadData(tsPddNorKernelInfo* PtsDevInfo,void* Pu8pSourceAdress,tS32 Ps32Length);
static tS32   PDD_u32GetDataSizeSector(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum);
static tS32   PDD_bReadSector(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum,void* Pu8pDestAdress,tS32 Ps32Length);
static tS32   PDD_s32WriteSector(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum,void* Pu8pSourceAdress,tS32 Ps32Length);
static tBool  PDD_bReadFlash(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32Cluster,void* ,tU32 Pu32Offset, tU32 Pu32Len);
static tBool  PDD_bWriteFlash(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum, void* Pu8SourceAdress, tU32 Pu32Offset, tU32 Pu32Len);
static tBool  PDD_bEraseFlash(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum);
static void   PDD_vCheckTimeEraseSector(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum);
#endif
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessInit()
*
* DESCRIPTION: init routine for nor_kernel flash access
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
tS32 PDD_NorKernelAccessInit(void)
{ 
  tS32  Vs32ErrorCode=PDD_OK;

  #ifdef PDD_NOR_KERNEL_POOL_EXIST   
  if(vthLfx==NULL)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_INIT_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  { /*get num blocks*/
	vtspPddNorInfo->u32NumBlocks=vthLfx->numBlocks;
	PDD_TRACE(PDD_NOR_KERNEL_DEBUG_NUMBER_BLOCKS,&vtspPddNorInfo->u32NumBlocks,sizeof(tU32));
	if(vtspPddNorInfo->u32NumBlocks < PDD_NOR_KERNEL_MIN_NUMBER_OF_SECTOR)
	{
	  Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_NO_VALID_NUMBER_OF_BLOCKS;
	  PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	}
	else
	{ /*get sector size */
      vtspPddNorInfo->u32SectorSize=vthLfx->blockSize;	
      /*check sector size and if cluster size fits into one sector*/
      if(vtspPddNorInfo->u32SectorSize==0)
	  {
	    Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_NO_VALID_SECTOR_SIZE;
	    PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));;
	  }
	  else  
	  { /* trace out size */
	    tU32  Vu32SizeKilo=vtspPddNorInfo->u32SectorSize/1024;
        PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_SECTOR,&Vu32SizeKilo,sizeof(Vu32SizeKilo));   
		if(vtspPddNorInfo->u32SectorSize < PDD_NOR_KERNEL_MIN_SECTOR_SIZE) 
		{
		  tU32 Vu32Factor = PDD_NOR_KERNEL_MIN_SECTOR_SIZE / vtspPddNorInfo->u32SectorSize; 
		  vtspPddNorInfo->u32SectorSize=PDD_NOR_KERNEL_MIN_SECTOR_SIZE;
		  vtspPddNorInfo->u32NumBlocks /= Vu32Factor;
          if(vtspPddNorInfo->u32NumBlocks < PDD_NOR_KERNEL_MIN_NUMBER_OF_SECTOR)
	      {
		     Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_NO_VALID_NUMBER_OF_BLOCKS;
	         PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));;
	      }
		  { /*trace out new information */
		    Vu32SizeKilo=vtspPddNorInfo->u32SectorSize/1024;
            PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_SECTOR,&Vu32SizeKilo,sizeof(Vu32SizeKilo)); 
			PDD_TRACE(PDD_NOR_KERNEL_DEBUG_NUMBER_BLOCKS,&vtspPddNorInfo->u32NumBlocks,sizeof(tU32));
		  }
		}
	    /* get chunk size */
	    vtspPddNorInfo->u16ChunkSize=vthLfx->chunkSize;  
	    /* trace chunk size */        
        PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_CHUNK,&vtspPddNorInfo->u16ChunkSize,sizeof(vtspPddNorInfo->u16ChunkSize));
	    /*if hasECC = false and vtspPddNorInfo->u16ChunkSize < 32 set chunksize to 32 for generate a valid header*/
        if(vtspPddNorInfo->u16ChunkSize < PDD_NOR_KERNEL_MIN_CHUNK_SIZE)
		{
          vtspPddNorInfo->u16ChunkSize=PDD_NOR_KERNEL_MIN_CHUNK_SIZE;
		  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_CHUNK,&vtspPddNorInfo->u16ChunkSize,sizeof(vtspPddNorInfo->u16ChunkSize));
		}
		/* check chunk size*/
	    if(   (vtspPddNorInfo->u16ChunkSize > PDD_KERNEL_CHIP_ALIGNMENT)
			||(vtspPddNorInfo->u16ChunkSize >= 0xff)
			||((vtspPddNorInfo->u16ChunkSize & (vtspPddNorInfo->u16ChunkSize-1))))
	    {
	      Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_NO_VALID_CHUNK_SIZE;
	      PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
		  /*error memory entry */
		  if(vtspPddNorInfo->u16ChunkSize > PDD_KERNEL_CHIP_ALIGNMENT)
		  {/* define PDD_KERNEL_CHIP_ALIGNMENT update for new u16ChunkSize*/
            PDD_FATAL_M_ASSERT_ALWAYS();
		  }
	    }
	    else
	    {/* init variable */ 
		  tU8 Vu8Inc;
	      vtspPddNorInfo->u32SectorMaxSizeData=vtspPddNorInfo->u32SectorSize - PDD_NOR_KERNEL_HEADER_SIZE;             
          vtspPddNorInfo->bSyncDone=FALSE;		 
		  for(Vu8Inc=0;Vu8Inc<PDD_NOR_KERNEL_SECTOR_NUM;Vu8Inc++)
	      { /*check if size always read*/
            vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].s32SizeData=0;
            vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].bValid=FALSE;
			vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].bUpdated=FALSE;	
			vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].u32EraseSectorCount=0;
			vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].u32EraseSectorLastTime=0;
		    PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_DATA,&vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].s32SizeData,sizeof(tS32));				
		    PDD_TRACE(PDD_NOR_KERNEL_DEBUG_ERASE_COUNTER,&vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].u32EraseSectorCount,sizeof(tU32));   
	      }/*end for*/
	    }/*end else*/
      }/*end else*/
	}/*end else*/
  }/* end else*/
  #endif
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessDeInit()
*
* DESCRIPTION: deinit routine for nor_kernel flash access
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
tS32 PDD_NorKernelAccessDeInit(void)
{ 
  tS32  Vs32ErrorCode=PDD_OK;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST     
  if(vthLfx==NULL)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_INIT_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  {
    tU8 Vu8Inc;
	vtspPddNorInfo->u32SectorSize=0;	
    vtspPddNorInfo->u32SectorMaxSizeData=0;  
    vtspPddNorInfo->u32NumBlocks=0;            
    vtspPddNorInfo->u16ChunkSize=1;
	vtspPddNorInfo->bSyncDone=FALSE;	
	for(Vu8Inc=0;Vu8Inc<PDD_NOR_KERNEL_SECTOR_NUM;Vu8Inc++)
	{ /*check if size always read*/
      vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].s32SizeData=0;
      vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].bValid=FALSE;
	  vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].bUpdated=FALSE;	 
	  vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].u32EraseSectorCount=0;
	  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_DATA,&vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].s32SizeData,sizeof(tS32));	  
	  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_ERASE_COUNTER,&vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].u32EraseSectorCount,sizeof(tU32));   
	}/*end for*/
  }
  #endif
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessInitProcess()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
tS32 PDD_NorKernelAccessInitProcess(void)
{
  tS32  Vs32ErrorCode=PDD_OK;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST
  vtspPddNorInfo=PDD_GetNorKernelInfoShm();
  if(vthLfx==NULL)
  { /* PDD*/
    PDD_TRACE(PDD_NOR_KERNEL_DEBUG_LFX_OPEN,0,0);
	vthLfx= LFX_open("PDD_Kernel");	
	if(vthLfx==NULL)
	{
	  Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_LFX_OPEN_FAILED;//errno;
	  PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	}
  } 
  #endif
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessDeInitProcess()
*
* DESCRIPTION: 
*
* PARAMETERS:
*      
*
* RETURNS: 
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
tS32 PDD_NorKernelAccessDeInitProcess(void)
{
  tS32  Vs32ErrorCode=PDD_OK;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST     
  if(vthLfx==NULL)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_INIT_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  {
    LFX_close(vthLfx);	
    vthLfx=NULL;
    vtspPddNorInfo=NULL;
  }
  #endif
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessGetDataStreamSize()
*
* DESCRIPTION: get size of data, without header
*
*
* RETURNS: 
*      positive value: size of data 
*      negative value: error code 
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
tS32 PDD_NorKernelAccessGetDataStreamSize(void)
{ 
  tS32               Vs32Size=PDD_OK;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST     
  if(vthLfx==NULL)
  {
    tS32 Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_INIT_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  {
    tS32 Vs32SizeTemp=0;
	tU8  Vu8Inc;
	for(Vu8Inc=0;Vu8Inc<PDD_NOR_KERNEL_SECTOR_NUM;Vu8Inc++)
	{ /*check if size always read*/
	  Vs32SizeTemp=vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].s32SizeData;	 
	  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_DATA,&vtspPddNorInfo->sKernelSectorInfo[Vu8Inc].s32SizeData,sizeof(tS32));
      if(Vs32SizeTemp==0)
	  {/*read size*/
	    Vs32SizeTemp=PDD_u32GetDataSizeSector(vtspPddNorInfo,Vu8Inc);
	  }
	  /*compare size*/
	  if(Vs32SizeTemp>Vs32Size)
	  {
	    Vs32Size=Vs32SizeTemp;
	  }
	}/*end for*/		
  }/*end else*/
  #endif
  return(Vs32Size);
}
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessErase()
*
* DESCRIPTION: erase all sector
*
* PARAMETERS:
*    
*
* RETURNS: 
*      positive value: PDD_OK 
*      negative value: error code 
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
tS32 PDD_NorKernelAccessErase(tU8 PubSector)
{
  tS32               Vs32ErrorCode=PDD_OK;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST 
  if(vthLfx==NULL)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_INIT_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  {/*erase flash => flash is not formated*/
   PDD_bEraseFlash(vtspPddNorInfo,(tS32)PubSector);
   /*set sync to invalid*/
   vtspPddNorInfo->bSyncDone=FALSE;
   /*set sector info to init*/
   vtspPddNorInfo->sKernelSectorInfo[PubSector].s32SizeData=0;
   vtspPddNorInfo->sKernelSectorInfo[PubSector].bValid=FALSE;
   vtspPddNorInfo->sKernelSectorInfo[PubSector].bUpdated=FALSE;	
   vtspPddNorInfo->sKernelSectorInfo[PubSector].u32EraseSectorCount=0;
   PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_DATA,&vtspPddNorInfo->sKernelSectorInfo[PubSector].s32SizeData,sizeof(tS32));				
   PDD_TRACE(PDD_NOR_KERNEL_DEBUG_ERASE_COUNTER,&vtspPddNorInfo->sKernelSectorInfo[PubSector].u32EraseSectorCount,sizeof(tU32));    
  }
  #endif
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessReadDataStream()
*
* DESCRIPTION: function for read from nor_kernel nor
*
* RETURNS: 
*      positive value: size of read data
*      negative value: error code 
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
tS32 PDD_NorKernelAccessReadDataStream(void *Ppu8ReadBuffer,tS32 Ps32SizeReadBuffer)
{
  tS32               Vs32ErrorCode=PDD_OK;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST     
  if(vthLfx==NULL)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_INIT_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  { /*check parameter*/
    if((Ppu8ReadBuffer==NULL)||(((tU32)Ps32SizeReadBuffer)>vtspPddNorInfo->u32SectorSize))
	{
	  Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_READ_PARAM;
	  PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	}
	else
	{/*check if synchronsiation of sector1 and sector2 is done*/
      if (vtspPddNorInfo->bSyncDone==FALSE)
	  { /*do synchronisation*/
	    vtspPddNorInfo->bSyncDone=PDD_bSynchronisation(vtspPddNorInfo);	  
	  }
	  /* read data */
	  if (vtspPddNorInfo->bSyncDone==TRUE)
	  {
        Vs32ErrorCode=PDD_s32ReadData(vtspPddNorInfo,Ppu8ReadBuffer,Ps32SizeReadBuffer);
	  }
	  else
	  {
	    Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_SYNC_NOT_DONE;
	    PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	  }
	}
  }
  #endif
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessWriteDataStream()
*
* DESCRIPTION: function for write to nor_kernel nor
*
* PARAMETERS:
*
* RETURNS: 
*      positive value: PDD_OK
*      negative value: error code 
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
tS32 PDD_NorKernelAccessWriteDataStream(void *Ppu8WriteBuffer,tS32 Ps32SizeWriteBuffer)
{ 
  tS32               Vs32ErrorCode=PDD_OK;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST     
  tU8                Vu8Inc;
  if(vthLfx==NULL)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_INIT_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  { /*check parameter*/    
    if((Ppu8WriteBuffer==NULL)||(Ps32SizeWriteBuffer > (tS32)(sizeof(TDataPoolKernel_DpPddKernel)+sizeof(TPddKernel_Header))))
	{
	  Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_WRITE_PARAM;
	  PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	}
    else
    { /*write access: pdd.c reads the data and compare with backup; in read data synchronisation done*/
	  /*for each sector*/
      for(Vu8Inc=0;Vu8Inc<PDD_NOR_KERNEL_SECTOR_NUM;Vu8Inc++)
      {
        Vs32ErrorCode=PDD_s32WriteSector(vtspPddNorInfo,Vu8Inc,Ppu8WriteBuffer,Ps32SizeWriteBuffer);
	  }
	}
  }/*end else*/
  #endif
  return(Vs32ErrorCode);
}
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessGetEraseCounter()
*
* DESCRIPTION: get erase sector count
*
* PARAMETERS:
*
* RETURNS: 
*      count 
*
* HISTORY:Created  2014 01 20
*****************************************************************************/
tU32 PDD_NorKernelAccessGetEraseCounter(tU8 PubSector)
{
  tU32 Vu32Count=0;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST  
  if(vthLfx==NULL)
  {
    tS32 Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_INIT_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  {
    Vu32Count=vtspPddNorInfo->sKernelSectorInfo[PubSector].u32EraseSectorCount;
  }
  #else
  tS32 Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_NO_POOL_EXIST;
  PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  #endif
  return(Vu32Count);
}
/******************************************************************************
* FUNCTION: PDD_NorKernelAccessGetEraseCounter()
*
* DESCRIPTION: get erase sector count
*
* PARAMETERS:
*
* RETURNS: 
*      count 
*
* HISTORY:Created  2014 01 20
*****************************************************************************/
tU32 PDD_NorKernelAccessGetChunkSize(void)
{
  tU32 Vu32ChunkSize=1;
  #ifdef PDD_NOR_KERNEL_POOL_EXIST  
  if(vthLfx==NULL)
  {
    tS32 Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_INIT_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  {
    Vu32ChunkSize=(tU32)vtspPddNorInfo->u16ChunkSize;
  }
  #else
  tS32 Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_NO_POOL_EXIST;
  PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  #endif
  return(Vu32ChunkSize);
}
#ifdef PDD_NOR_KERNEL_POOL_EXIST 
/******************************************************************************
* FUNCTION: PDD_bSynchronisation()
*
* DESCRIPTION: synchronisate sector1 and sector 2
*
* PARAMETERS:
*   PtsDevInfo        : pointer of the driver info
*   Ps32SectorNum     : the number of the sector, 0 <= cluster < u32NumBlocks
*
* RETURNS: 
*       TRUE => flash at sectornum is erased
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
static tBool  PDD_bSynchronisation(tsPddNorKernelInfo* PtsDevInfo)
{
  tBool VbReturnCode=TRUE;
  tU8   Vu8Inc;
  void* VvpBufRead[PDD_NOR_KERNEL_SECTOR_NUM];
  tS32  Vs32SizeMax=PDD_NorKernelAccessGetDataStreamSize();
  tS32  Vs32Size;

  /*-------------------------------------------*/
  /*for each sector allocate buffer*/
  for(Vu8Inc=0;Vu8Inc<PDD_NOR_KERNEL_SECTOR_NUM;Vu8Inc++)
  { /*allocate max buffer size */
	VvpBufRead[Vu8Inc]=(void*)malloc(Vs32SizeMax);
	/* check buffer*/
    if(VvpBufRead[Vu8Inc]==NULL)
    { /*system: no space available*/
      tS32 Vs32ErrorCode= PDD_ERROR_NO_BUFFER;
      PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
      PDD_FATAL_M_ASSERT_ALWAYS();
    }  
  }
  /*-------------------------------------------*/
  /*for each sector read data and check validation*/
  for(Vu8Inc=0;Vu8Inc<PDD_NOR_KERNEL_SECTOR_NUM;Vu8Inc++)
  { /*check if size always read*/
    Vs32Size=PDD_bReadSector(PtsDevInfo,Vu8Inc,VvpBufRead[Vu8Inc],Vs32SizeMax);
    if(Vs32Size >= PDD_NOR_KERNEL_HEADER_SIZE)
    {/*check validation*/
      PtsDevInfo->sKernelSectorInfo[Vu8Inc].bValid=PDD_ValidationCheckNorKernel(VvpBufRead[Vu8Inc],Vs32Size,DATAPOOL_KERNEL_VERSION_POOL_DPPDDKERNEL);      
    }
  }/*end for*/	
  /*-------------- update-----------------------*/
  PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR0].bUpdated=FALSE;
  PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR1].bUpdated=FALSE;
  /*check both sector invalid*/
  if(   (PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR0].bValid==FALSE)
	  &&(PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR1].bValid==FALSE))
  {/*do nothing*/    
    VbReturnCode=FALSE;
  }/*check sector1 invalid and sector 2 valid*/
  else if(  (PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR0].bValid==FALSE)
	      &&(PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR1].bValid==TRUE))
  { /*write sector2 to sector1 */
    Vs32Size=PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR1].s32SizeData;
	PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_DATA,&Vs32Size,sizeof(tS32));
    if(PDD_s32WriteSector(PtsDevInfo,PDD_NOR_KERNEL_SECTOR0,VvpBufRead[PDD_NOR_KERNEL_SECTOR1],Vs32Size) == Vs32Size)
	{
	  tU8 VubSector=PDD_NOR_KERNEL_SECTOR0;
      PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR0].bUpdated=TRUE;
	  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SYNC_SECTOR,&VubSector,sizeof(tU8));
	}
	else
	{
	  VbReturnCode=FALSE;
	}
  }/*check sector2 invalid and sector 1 valid*/
  else if( (PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR0].bValid==TRUE)
	     &&(PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR1].bValid==FALSE))
  {/*write sector2 to sector1 */
    Vs32Size=PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR0].s32SizeData;
    if(PDD_s32WriteSector(PtsDevInfo,PDD_NOR_KERNEL_SECTOR1,VvpBufRead[PDD_NOR_KERNEL_SECTOR0],Vs32Size)==Vs32Size)
	{
	  tU8 VubSector=PDD_NOR_KERNEL_SECTOR1;
      PtsDevInfo->sKernelSectorInfo[VubSector].bUpdated=TRUE;
	  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SYNC_SECTOR,&VubSector,sizeof(tU8));
	}
	else
	{
	  VbReturnCode=FALSE;
	}
  }
  else
  { /* compare both valid data */
    /* if sector1 != sector2 size from sector1*/
    tU8*  Vpu8Sector1Data= (tU8*)VvpBufRead[PDD_NOR_KERNEL_SECTOR0];
	tU8*  Vpu8Sector2Data= (tU8*)VvpBufRead[PDD_NOR_KERNEL_SECTOR1];
	tS32  Vs32SizeHeader= ((tS32)sizeof(TPddKernel_Header));
    Vs32Size=PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR0].s32SizeData;
    if(memcmp(Vpu8Sector1Data+Vs32SizeHeader,Vpu8Sector2Data+Vs32SizeHeader,Vs32Size-Vs32SizeHeader)!=0)
	{/*update sector2*/
      if(PDD_s32WriteSector(PtsDevInfo,PDD_NOR_KERNEL_SECTOR1,VvpBufRead[PDD_NOR_KERNEL_SECTOR0],Vs32Size)==Vs32Size)
	  {
	    tU8 VubSector=PDD_NOR_KERNEL_SECTOR1;
        PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR1].bUpdated=TRUE;
		PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SYNC_SECTOR,&VubSector,sizeof(tU8));
	  }
	  else
	  {
        VbReturnCode=FALSE;
	  }
	}
  }  
  /*-------------------------------------------*/
  /*for each sector set buffer to  free */
  for(Vu8Inc=0;Vu8Inc<PDD_NOR_KERNEL_SECTOR_NUM;Vu8Inc++)
  { /*check if size always read*/
    free(VvpBufRead[Vu8Inc]);
  }/*end for*/
  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SECTOR_INFO,&PtsDevInfo->sKernelSectorInfo[0],(tU32)(PDD_NOR_KERNEL_SECTOR_NUM*sizeof(tsPddNorKernelSectorInfo)));
  return(VbReturnCode);
}
/******************************************************************************
* FUNCTION: PDD_s32ReadData()
*
* DESCRIPTION: read complete structure
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created  2014 01 15
*****************************************************************************/
static tS32   PDD_s32ReadData(tsPddNorKernelInfo* PtsDevInfo,void* Pu8pSourceAdress,tS32 Ps32Length)
{
  tS32  VS32Size;
  /*first read sector1*/
  VS32Size=PDD_bReadSector(PtsDevInfo,PDD_NOR_KERNEL_SECTOR0,Pu8pSourceAdress,Ps32Length);
  if(VS32Size>=0)
  {/*check validation*/
    PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR0].bValid=PDD_ValidationCheckNorKernel(Pu8pSourceAdress,VS32Size,DATAPOOL_KERNEL_VERSION_POOL_DPPDDKERNEL);	
  }
  /*if sector1 invalid*/
  if(PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR0].bValid==FALSE)
  { /*second read sector2*/
    VS32Size=PDD_bReadSector(PtsDevInfo,PDD_NOR_KERNEL_SECTOR1,Pu8pSourceAdress,Ps32Length);
	if(VS32Size>=0)
	{ /*check validation*/
	  PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR1].bValid=PDD_ValidationCheckNorKernel(Pu8pSourceAdress,VS32Size,DATAPOOL_KERNEL_VERSION_POOL_DPPDDKERNEL);
	  /*if sector2 invalid set*/
	  if(PtsDevInfo->sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR1].bValid==FALSE)
	  {/*set error code*/
        VS32Size= PDD_ERROR_NOR_KERNEL_NO_VALID_SECTOR;
	    PDD_TRACE(PDD_ERROR,&VS32Size,sizeof(tS32));
	  }
	}
  }  
  return(VS32Size);
}
/******************************************************************************
* FUNCTION: PDD_u32GetDataSizeSector()
*
* DESCRIPTION: get size information of the sector 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
static tS32   PDD_u32GetDataSizeSector(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum)
{
  tS32                 VS32Size=0;

  TPddKernel_Header    VtsHeader;
  if(PDD_bReadFlash(PtsDevInfo,Ps32SectorNum,&VtsHeader,0,PDD_NOR_KERNEL_HEADER_SIZE)==TRUE)
  { /*check validation and magic */
    if(   (VtsHeader.HeaderStream.Magic==PDD_NOR_KERNEL_MAGIC)
		&&((int)VtsHeader.HeaderValid.Valid==(~PDD_NOR_KERNEL_STATE_VALID))
		&&(VtsHeader.HeaderValid.Invalid!=(~PDD_NOR_KERNEL_STATE_INVALID)))
	{ /*save lenght from header*/
      PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].s32SizeData=VtsHeader.HeaderStream.Size;
	  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_DATA,&PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].s32SizeData,sizeof(tS32));
	  PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorCount=VtsHeader.HeaderStream.EraseSectorCount;
	  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_ERASE_COUNTER,&PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorCount,sizeof(tU32));   
      /*set lenght*/
      VS32Size=VtsHeader.HeaderStream.Size;
	}
  } 
  return(VS32Size);
}

/******************************************************************************
* FUNCTION: PDD_bReadSector()
*
* DESCRIPTION: write to flash
*
* PARAMETERS:
*   PtsDevInfo        : pointer of the driver info
*   Ps32SectorNum     : the number of the sector, 0 <= cluster < u32NumBlocks
*
* RETURNS: 
*       TRUE => flash at sectornum is erased
*
* HISTORY:Created  2014 01 15
*****************************************************************************/
static tS32  PDD_bReadSector(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum,void* PvpDestAdress,tS32 Ps32Length)
{ 
  tS32                 Vs32Size;
  TPddKernel_Header*   VtspHeader;

  /*get data*/
  if(PDD_bReadFlash(PtsDevInfo,Ps32SectorNum,PvpDestAdress,0,Ps32Length)==TRUE)
  {
    VtspHeader=(TPddKernel_Header*)PvpDestAdress;
	  /*check validation*/
    if(   (VtspHeader->HeaderStream.Magic==PDD_NOR_KERNEL_MAGIC)
		&&((int)VtspHeader->HeaderValid.Valid==(~PDD_NOR_KERNEL_STATE_VALID))
		&&(VtspHeader->HeaderValid.Invalid!=(~PDD_NOR_KERNEL_STATE_INVALID)))
	  { /*get lenght from header*/
      Vs32Size=VtspHeader->HeaderStream.Size;
      PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorCount=VtspHeader->HeaderStream.EraseSectorCount;
      PDD_TRACE(PDD_NOR_KERNEL_DEBUG_ERASE_COUNTER,&PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorCount,sizeof(tU32));   
      /*check lenght*/
      if(Vs32Size>Ps32Length)
      {/*not all data read*/
          Vs32Size=PDD_ERROR_NOR_KERNEL_READ_BUFFER_TO_SMALL;
        PDD_TRACE(PDD_ERROR,&Vs32Size,sizeof(tS32));;
      }
	  }
	  else
	  {/*no magic and validation wrong*/
      Vs32Size=PDD_ERROR_NOR_KERNEL_READ_INVALID_HEADER;
	    PDD_TRACE(PDD_ERROR,&Vs32Size,sizeof(tS32));;
	  }
  }
  else
  {
    Vs32Size=PDD_ERROR_NOR_KERNEL_READ_SECTOR_FAILS;
    PDD_TRACE(PDD_ERROR,&Vs32Size,sizeof(tS32));;
  }  
  return(Vs32Size);
}
/******************************************************************************
* FUNCTION: PDD_s32WriteSector()
*
* DESCRIPTION: write to flash
*
* PARAMETERS:
*   PtsDevInfo        : pointer of the driver info
*   Ps32SectorNum     : the number of the sector, 0 <= cluster < u32NumBlocks
*
* RETURNS: 
*       TRUE => flash at sectornum is erased
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
static tS32  PDD_s32WriteSector(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum,void* Pu8pSourceAdress,tS32 Ps32Length)
{
  tS32               Vs32ErrorCode=PDD_OK;
  tU32               Vu32Offset=sizeof(TPddHeaderValid);
  void*              VpPosWrite=(void*)(((tU8*)Pu8pSourceAdress)+Vu32Offset);
  tS32               VS32Len=Ps32Length-Vu32Offset;
  tPddKernelValid    VtsValid;
  /*write invalid flag sector */
  VtsValid=~PDD_NOR_KERNEL_STATE_INVALID;
  if(PDD_bWriteFlash(PtsDevInfo,Ps32SectorNum,(void*)&VtsValid,sizeof(tPddKernelValid),sizeof(tPddKernelValid))==FALSE)
  {
    Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_WRITE_WRITE_INVALID_FAIL;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
  }
  else
  { /*erase sector */
    if(PDD_bEraseFlash(PtsDevInfo,Ps32SectorNum)==FALSE)
    { 
      Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_WRITE_ERASE_FAIL;
      PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
    }
    else
    { /* write data*/
	  /* save u32EraseSectorCount*/
	  TPddKernel_Header*   VtspHeader=(TPddKernel_Header*) Pu8pSourceAdress;
	  VtspHeader->HeaderStream.EraseSectorCount=PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorCount;
      if(PDD_bWriteFlash(PtsDevInfo,Ps32SectorNum,VpPosWrite,Vu32Offset,VS32Len)==FALSE)
	  {
        Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_WRITE_WRITE_DATA_FAIL;
	    PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	  }
	  else
	  {/*write valid flag sector */
	    VtsValid=~PDD_NOR_KERNEL_STATE_VALID;
		if(PDD_bWriteFlash(PtsDevInfo,Ps32SectorNum,(void*)&VtsValid,0,sizeof(tPddKernelValid))==FALSE)
	    {
          Vs32ErrorCode=PDD_ERROR_NOR_KERNEL_WRITE_WRITE_IVALID_FAIL;
	      PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
	    }
		else
		{
		  Vs32ErrorCode=Ps32Length;
		  PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].s32SizeData=Ps32Length;
		  PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].bValid=TRUE;
		  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_SIZE_DATA,&PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].s32SizeData,sizeof(tS32));
		  PDD_TRACE(PDD_NOR_KERNEL_DEBUG_VALID,&PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].bValid,sizeof(tBool));
		}		
	  }
	}/*end else PDD_bEraseFlash==TRUE*/
  }
  return(Vs32ErrorCode);
}

/******************************************************************************
* FUNCTION: PDD_bReadFlash()
*
* DESCRIPTION: read from flash
*
* PARAMETERS:
*     PtsDevInfo    : pointer of the device info
*     Ps32Cluster   : the number of the cluster, 0 <= cluster < s32NumCluster
*     Pu8DestAdress : address to read the byte
*     Pu32Offset    : offset in the cluster
*     Pu32Len       : byte to read
*
* RETURNS: 
*      TRUE => all OK
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
static tBool  PDD_bReadFlash(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum,void* PvpDestAdress,tU32 Pu32Offset, tU32 Pu32Len)
{
  tBool VbReturn=FALSE;

  if(Ps32SectorNum >= (tS32) PDD_NOR_KERNEL_SECTOR_NUM)
  { /*sector number to great*/
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  if(LFX_read(vthLfx,PvpDestAdress,Pu32Len,(tU32)Ps32SectorNum * PtsDevInfo->u32SectorSize + Pu32Offset)==0)
  { /*success*/
    VbReturn=TRUE;	  
  }
  if(!VbReturn)
  { /*could not read */
	  PDD_SET_ERROR_ENTRY("NOR_KERNEL: LFX_read() fails");
  }
  return(VbReturn);  
}

/******************************************************************************
* FUNCTION: PDD_bWriteFlash()
*
* DESCRIPTION: write to flash
*
* PARAMETERS:
*   PtsDevInfo      : pointer of the driver info
*   Ps32Cluster     : the number of the cluster, 0 <= cluster < s32NumCluster
*   Pu8SourceAdress : address to write from
*   Pu32Offset      : offset in the cluster
*   Pu32Len         : byte to write
*
* RETURNS: 
*      TRUE => all OK
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static tBool PDD_bWriteFlash(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum, void* Pu8SourceAdress, tU32 Pu32Offset, tU32 Pu32Len)
{
  tBool VbReturn=FALSE;
  tU8*  VubpWriteBuffer=(tU8*)malloc((ALIGN_TO_CHIP(Pu32Len)));
  
  if(Ps32SectorNum >= (tS32) PDD_NOR_KERNEL_SECTOR_NUM)
  { /*sector number to great*/
    PDD_FATAL_M_ASSERT_ALWAYS();
  } 
  if(VubpWriteBuffer==NULL)
  { /*system: no space available*/
    tS32 Vs32ErrorCode= PDD_ERROR_NO_BUFFER;
	PDD_TRACE(PDD_ERROR,&Vs32ErrorCode,sizeof(tS32));
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  else
  {
    memset(VubpWriteBuffer,0xff,ALIGN_TO_CHIP(Pu32Len));
    memmove(VubpWriteBuffer,Pu8SourceAdress,Pu32Len);
    /*write to flash*/	  
    if(LFX_write(vthLfx,VubpWriteBuffer,ALIGN_TO_CHIP(Pu32Len),(tU32)Ps32SectorNum * PtsDevInfo->u32SectorSize + Pu32Offset)==0)
    {
	  VbReturn=TRUE;		
	}
    free(VubpWriteBuffer);		
  }
  /*check error */
  if(!VbReturn)
  { /*could not read*/
    PDD_SET_ERROR_ENTRY("NOR_KERNEL: LFX_write() fails");
  }
  return(VbReturn);
}
/******************************************************************************
* FUNCTION: PDD_bEraseFlash()
*
* DESCRIPTION: write to flash
*
* PARAMETERS:
*   PtsDevInfo        : pointer of the driver info
*   Ps32SectorNum     : the number of the sector, 0 <= cluster < u32NumBlocks
*
* RETURNS: 
*       TRUE => flash at sectornum is erased
*
* HISTORY:Created  2014 01 14
*****************************************************************************/
static tBool  PDD_bEraseFlash(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum)
{
  tBool VbReturn=FALSE;
  if(Ps32SectorNum >= (tS32) PtsDevInfo->u32NumBlocks)
  { /*sector number to great*/
    PDD_FATAL_M_ASSERT_ALWAYS();
  }
  if(LFX_erase(vthLfx, PtsDevInfo->u32SectorSize, ((tU32)Ps32SectorNum)*PtsDevInfo->u32SectorSize)==0)
  {
    VbReturn=TRUE;
    PDD_TRACE(PDD_NOR_KERNEL_DEBUG_ERASE_FLASH,&Ps32SectorNum,sizeof(Ps32SectorNum));
	PDD_vCheckTimeEraseSector(PtsDevInfo,Ps32SectorNum);	
  }
  if(VbReturn==FALSE)
  { /* LFX_erase failed */
    PDD_SET_ERROR_ENTRY("NOR_KERNEL: LFX_erase() fails");
  }
  else
  {/*increment counter erase for the sector*/  
    PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorCount += 1;
	PDD_TRACE(PDD_NOR_KERNEL_DEBUG_ERASE_COUNTER,&PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorCount,sizeof(tU32));   
	if(PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorCount>=PDD_NOR_KERNEL_MAX_SECTOR_ERASE_COUNTER)
	{ /*trace out and add errory memory entry if greater PDD_NOR_KERNEL_MAX_SECTOR_ERASE_COUNTER and multiple from 0x100*/
	  tU32 Vu32Count=PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorCount;
	  /*trace out only all 128 time */
	  if((Vu32Count&PDD_NOR_KERNEL_MAX_SECTOR_ERASE_COUNTER_MASK)==0)
	  {
	    tU8 Vu8Buffer[40];		
		snprintf(Vu8Buffer,sizeof(Vu8Buffer),"SectorEraseCounter to high:%d",Vu32Count); 
		PDD_TRACE(PDD_NOR_KERNEL,Vu8Buffer,strlen(Vu8Buffer)+1);	
		PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
	  }
    }
  }
  return(VbReturn);
}

/******************************************************************************
* FUNCTION: PDD_vCheckTimeEraseSector()
*
* DESCRIPTION: For the Endurance and Retention of the Spansion NOR: 
*              It should be avoid, that one sector will be erased within 90s.
*              see document "Endurance and Retention Management and Validation"
*              This function check this timing and make an error memory entry, 
*              if the time under 90 s.
*
* PARAMETERS:
*   PtsDevInfo      : pointer data stream
*
*
* HISTORY:Created  2015 03 23
*****************************************************************************/
static void   PDD_vCheckTimeEraseSector(tsPddNorKernelInfo* PtsDevInfo,tS32 Ps32SectorNum)
{
  struct timespec VTimer={0,0};
  /*get actuel start time*/
  clock_gettime(CLOCK_MONOTONIC, &VTimer); 
  /*if sector erased since startup*/ 
  if(PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorLastTime > 0)
  { /*check difference < 90 s => warning error error memory*/
    tU32 Vu32TimeDiff= VTimer.tv_sec-PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorLastTime;
    if(Vu32TimeDiff < PDD_NOR_LIMIT_TIME_ERASE_CYCLE)
    {/*pepare error memory entry*/
      tU8 Vu8Buffer[100];		
	  snprintf(Vu8Buffer,sizeof(Vu8Buffer),"NOR_KERNEL: Sector %d erased within 90s (%ds,%ds)",Ps32SectorNum,PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorLastTime, VTimer.tv_sec); 
      PDD_TRACE(PDD_NOR_KERNEL,Vu8Buffer,strlen(Vu8Buffer)+1);	
      PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
	}
  }
  /* save erased time for the sector into shared memory*/
  PtsDevInfo->sKernelSectorInfo[Ps32SectorNum].u32EraseSectorLastTime=(tU32) VTimer.tv_sec;
}
#endif
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/* End of File pdd_access_nor_kernel.c                                               */
/******************************************************************************/


