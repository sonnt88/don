/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        pdd_trace.c
 *
 * This file contains all functions for trace
 * 
 * global function:
 * -- 
 *
 * local function:
 * -- 
 *
 * @date        2012-10-12
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/stat.h>
#include <memory.h>
#include <limits.h>   
#include "system_types.h"
#include "system_definition.h"
#include "LFX2.h"
#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_pdd.h"
#include "dgram_service.h"
#include "mc_trace.h"
#include "pdd_variant.h"
#include "pdd.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#ifdef PDD_SCC_POOL_EXIST
#include "DataPoolSCC.h" 
#endif
#ifdef PDD_NOR_KERNEL_POOL_EXIST  
#include "DataPoolKernel.h"
#endif
#include "pdd_config_scc.h"


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
/*for TTFIS command*/
#define PDD_TEST_COMMAND                      3
#ifdef PDD_NOR_KERNEL_POOL_EXIST
#define PDD_TEST_SECTOR_NUMBER                4
#endif
#define PDD_TEST_ARGUMENT                     4

/*length for traces*/
#define PDD_TEST_LEN_DATA                    224
#define PDD_LEN_MESSAGE_SCC                   (SCC_PD_NET_MAX_LENGTH_DATA_BYTES*5)
#define PDD_TEST_LEN_LINE                     70
#define PDD_TEST_LEN_HEADER                    1
#define PDD_LEN_MESSAGE                      (PDD_TEST_LEN_DATA+PDD_TEST_LEN_HEADER)

/* **************************************************FunctionHeaderBegin** *//*
* void vPddConsoleTrace(const void* pvData, unsigned int Length,tBool PbImportant)
*
* trace out stream 
*
* @param   
*
* @return  true: init sucess    
*          false: init failed
*
* @date    2013-08-20
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void vPddConsoleTrace(const void* pvData, unsigned int Length,tBool PbImportant)
{
  tU8               Vu8StreamOutput=u8PDD_GetTraceStreamOutput();
  /*STDERR*/
  if((PbImportant==TRUE)||(Vu8StreamOutput&STDERR_FILENO))
  {
    vConsoleTrace(STDERR_FILENO,pvData,Length);
  }
  /*STDOUT*/
  if(Vu8StreamOutput&STDOUT_FILENO)
  { 
    vConsoleTrace(STDOUT_FILENO,pvData,Length);
  }
}
/* **************************************************FunctionHeaderBegin** *//*
* void pdd_vTraceCommand(char* Pu8pData)
*
* trace command from Datapool
*
* @param   
*
* @return  true: init sucess    
*          false: init failed
*
* @date    2013-08-20
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void pdd_vTraceCommand(char* Pu8pData)
{    
  switch (Pu8pData[PDD_TEST_COMMAND])
  {
  case PDD_TEST_CMD_NOR_USER_ERASE:
  {      
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_NOR_USER_ERASE");	
    if(Vs32Result==PDD_OK)
    {/* trace output begin*/
      PDD_TRACE(PDD_NOR_USER,"erase raw nor starts",strlen("erase raw nor starts")+1);
      /* erase flash*/
	    Vs32Result=PDD_NorUserAccessErase();
	    if(Vs32Result==PDD_OK)
	    {
		    PDD_TRACE(PDD_NOR_USER,"erase raw nor ends",strlen("erase raw nor ends")+1);
	    }
	    else
	    {
	      PDD_TRACE(PDD_NOR_USER,"erase raw nor failed",strlen("erase raw nor failed")+1);
      }
    }
  }break;
  case PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA:
  {   
	  tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA");	
    if(Vs32Result==PDD_OK)
    {
	    tU8                            Vu8Inc;   
	    tsPddNorUserInfo*              VtsDevInfo=PDD_GetNorUserInfoShm();
	    tsPddNorUserHeaderDataStream*  VspHeaderDataStream;
	    tU8*                           Vpu8Data;
	    tU32                           Vu32Byte;
	    tS32                           Vs32SizeHeader=sizeof(tsPddFileHeader);
	    tU8                            Vu8Buffer[PDD_LEN_MESSAGE]; 
	    tsPddFileHeader*               VpHeader;                

	    if(PDD_bReadDataStreamConfiguration(VtsDevInfo)==FALSE)
	    {
        PDD_TRACE(PDD_NOR_USER,"read configuration failed",strlen("read configuration failed")+1);
	    }
	    else
	    {
        for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vu8Inc++)
	      {
	        VspHeaderDataStream=&VtsDevInfo->sHeaderDataStream[Vu8Inc];
		      /*trace out name */	
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"name:%s",&VspHeaderDataStream->u8DataStreamName[0]); 
          PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);       
		      /*trace out offset*/
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"offset: %lu",VspHeaderDataStream->u32Offset); 
          PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
		      /*trace out size*/
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"length: %lu",VspHeaderDataStream->u32Lenght); 
          PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
		      if(VspHeaderDataStream->u32Lenght!=0)
		      { 
		        Vpu8Data=malloc(VspHeaderDataStream->u32Lenght);
		        if(Vpu8Data!=NULL)
		        {
		          Vs32Result=PDD_NorUserAccessReadDataStream(VspHeaderDataStream->u8DataStreamName,Vpu8Data,VspHeaderDataStream->u32Lenght);
			        VpHeader=(tsPddFileHeader*) ((void*)Vpu8Data);     
			        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"header: Magic/Version:0x%04x - Version/Checksum:0x%04x - Checksum/Length:0x%04x",(int)VpHeader->u32Magic,(int)VpHeader->u32Version,(int)VpHeader->u32Checksum); 
              PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);	
			        Vu8Buffer[0]='\0';
			        if(Vs32Result>Vs32SizeHeader)
			        {/*trace out data*/
		            char  VcBufValue[10]; 
	              for (Vu32Byte=Vs32SizeHeader;Vu32Byte < VspHeaderDataStream->u32Lenght;Vu32Byte++)
                {		   
                  snprintf(VcBufValue,sizeof(VcBufValue),"0x%02x ",Vpu8Data[Vu32Byte]);
			            if((strlen(VcBufValue)+strlen(Vu8Buffer)+1)< (PDD_LEN_MESSAGE-20))
                    strncat(&Vu8Buffer[0],(const char*)&VcBufValue[0],strlen(VcBufValue)+1);
				          else
				          {/*output trace*/
				            PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
				            strncpy(&Vu8Buffer[0],(const char*)&VcBufValue[0],strlen(VcBufValue)+1);
				          }
                }
			          /*output trace*/
			          PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
              }                 
			        free(Vpu8Data);
            }		  
          }
	      }
	    }
    }/*end if*/
  }break;
  case PDD_TEST_CMD_NOR_USER_GET_ERASE_COUNTER:
  { 
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_NOR_USER_GET_ERASE_COUNTER");	
    if(Vs32Result==PDD_OK)
    {
	    tU8  Vu8Buffer[PDD_LEN_MESSAGE]; 
      tU32 Vu32Count=PDD_NorUserAccessGetEraseCounter(); 
      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"erase counter: %lu",Vu32Count); 
      PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
    }
  }break;
  case PDD_TEST_CMD_NOR_KERNEL_ERASE:
  {
    #ifdef PDD_NOR_KERNEL_POOL_EXIST
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_KERNEL,"PDD_TEST_CMD_NOR_KERNEL_ERASE");
	  tU8   Vu8Inc;   
    if(Vs32Result==PDD_OK)
    {/* trace output begin*/     
	    for(Vu8Inc=0;Vu8Inc<PDD_NOR_KERNEL_SECTOR_NUM;Vu8Inc++)
	    { /* erase flash*/
	      PDD_TRACE(PDD_NOR_KERNEL,"erase raw nor starts",strlen("erase raw nor starts")+1);;
	      Vs32Result=PDD_NorKernelAccessErase(Vu8Inc);
	      if(Vs32Result==PDD_OK)
	      {
	        PDD_TRACE(PDD_NOR_KERNEL,"erase raw nor ends",strlen("erase raw nor ends")+1);
	      }
	      else
	      {
	        PDD_TRACE(PDD_NOR_KERNEL,"erase raw nor failed",strlen("erase raw nor failed")+1);
	      }
      }
    }
    #endif
  }break;
  case PDD_TEST_CMD_NOR_KERNEL_ERASE_SECTOR:
  {
    #ifdef PDD_NOR_KERNEL_POOL_EXIST
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_KERNEL,"PDD_TEST_CMD_NOR_KERNEL_ERASE_SECTOR");
	  tU8   Vu8Sector=Pu8pData[PDD_TEST_SECTOR_NUMBER];   
    if(Vs32Result==PDD_OK)
    {/* trace output begin*/
      PDD_TRACE(PDD_NOR_KERNEL,"erase raw nor starts",strlen("erase raw nor starts")+1);
	    /* erase flash*/
	    Vs32Result=PDD_NorKernelAccessErase(Vu8Sector);
	    if(Vs32Result==PDD_OK)
	    {
	      PDD_TRACE(PDD_NOR_KERNEL,"erase raw nor ends",strlen("erase raw nor ends")+1);
	    }
	    else
	    {
	      PDD_TRACE(PDD_NOR_KERNEL,"erase raw nor failed",strlen("erase raw nor failed")+1);
      }
    }
    #endif
  }break;
  case PDD_TEST_CMD_NOR_KERNEL_PRINT_ACTUAL_DATA:
  {
    #ifdef PDD_NOR_KERNEL_POOL_EXIST
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_KERNEL,"PDD_TEST_CMD_NOR_KERNEL_PRINT_ACTUAL_DATA");
	  tU8   Vu8Buffer[PDD_LEN_MESSAGE]; 
    if(Vs32Result==PDD_OK)
    { /*get size */
      Vs32Result=PDD_NorKernelAccessGetDataStreamSize();      
	    /*allocate buffer*/
	    void* VpData=malloc(Vs32Result);
	    if(VpData!=NULL)
	    { /*read data */
         Vs32Result=PDD_NorKernelAccessReadDataStream(VpData,Vs32Result);
		     if(Vs32Result>PDD_OK)
	       { /*output validation header*/
	         tU32               Vu32OffsetHeader=sizeof(TPddKernel_Header);
	         tU32               Vu32Byte;
	         TPddKernel_Header* VtsHeader=(TPddKernel_Header*)VpData;		 
	         /*output header */    
	         snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"header-Size:0x%04x-Version:0x%04x-Magic:0x%04x-ChunkSize:0x%04x-EraseSectorCount:0x%04x-CRC:0x%04x-CRCXor:0x%04x",
		                                         VtsHeader->HeaderStream.Size,VtsHeader->HeaderStream.Version,VtsHeader->HeaderStream.Magic,
										                        VtsHeader->HeaderStream.ChunkSize,VtsHeader->HeaderStream.EraseSectorCount,VtsHeader->HeaderStream.Checksum,VtsHeader->HeaderStream.ChecksumXor); 
           PDD_TRACE(PDD_NOR_KERNEL,&Vu8Buffer[0],strlen(Vu8Buffer)+1);			 
	         /* determine size of data*/
	         Vs32Result-=Vu32OffsetHeader;		
	         tU8* Vpu8Data=(tU8*)VpData;
	         if(Vs32Result>0)
	         { /*print data*/
	           char  VcBufValue[10]; 
	           Vs32Result+=Vu32OffsetHeader;		
             for (Vu32Byte=Vu32OffsetHeader;Vu32Byte < ((tU32)Vs32Result);Vu32Byte++)             
		         {		   
               snprintf(VcBufValue,sizeof(VcBufValue),"0x%02x ",Vpu8Data[Vu32Byte]);
		            if((strlen(VcBufValue)+strlen(Vu8Buffer))< (PDD_LEN_MESSAGE-20))
                    strncat(&Vu8Buffer[0],(const char*)&VcBufValue[0],strlen(VcBufValue)+1);
			          else
			          {/*output trace*/
			            PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer));
			            strncpy(&Vu8Buffer[0],(const char*)&VcBufValue[0],strlen(VcBufValue)+1);
                }
             }			 
			       /*output trace*/
			       PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
           }/*end if*/
         }/*end if*/
		     free(VpData);
      }
    }
    #endif
  }break;
  case PDD_TEST_CMD_NOR_KERNEL_GET_ERASE_COUNTER:
  {
    #ifdef PDD_NOR_KERNEL_POOL_EXIST
    tU8   Vu8Inc;   
	  tU32  Vu32Count;
	  tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_KERNEL,"PDD_TEST_CMD_NOR_KERNEL_GET_ERASE_COUNTER");
    if(Vs32Result==PDD_OK)
    {
	    tU8  Vu8Buffer[PDD_LEN_MESSAGE]; 
	    for(Vu8Inc=0;Vu8Inc<PDD_NOR_KERNEL_SECTOR_NUM;Vu8Inc++)
	    {
	      Vu32Count=PDD_NorKernelAccessGetEraseCounter(Vu8Inc); 
        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"erase counter: %lu",Vu32Count); 
        PDD_TRACE(PDD_NOR_KERNEL,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
	    }
    }
    #endif
  }break;      
  case PDD_TEST_CMD_SCC_SET_ALL_POOLS_TO_INVALID:
  { 
    tS32                    Vs32ErrorCode=PDD_OK;
    #ifdef PDD_SCC_POOL_EXIST
    PDD_Lock(PDD_SEM_ACCESS_SCC);
    Vs32ErrorCode=PDD_IsInit(PDD_LOCATION_SCC,"invalid pool");	
    if(Vs32ErrorCode==PDD_OK)
	  {
	    TPddScc_Header          VsStructHeader;
	    const tsPddSccConfig*   VtspSccConfig;
	    tU8                     VubInc;
	    tU8						          Vu8Buffer[PDD_LEN_MESSAGE]; 
     
      memset(&VsStructHeader,0x00,sizeof(VsStructHeader));
	    /*for all pools  */
	    for(VubInc=0;VubInc<PDD_SCC_TABLE_INFO_END;VubInc++)
	    {
	      VtspSccConfig=&vrPddSccConfig[VubInc];
        /*set version to invalid*/	    
	      VsStructHeader.u32VersionPool=0xffffffff;
        // first read 
        if(PDD_SccAccessReadDataStream((char*)VtspSccConfig->u8Filename,(void*)&VsStructHeader,(tS32)sizeof(VsStructHeader))>=PDD_OK)            
        { //invalid size and checksum       
          if(VsStructHeader.u32VersionPool!=0xffffffff)
          {
             VsStructHeader.u32Checksum=0xffffffff;
             VsStructHeader.u32Size=0;
             Vs32ErrorCode=PDD_SccAccessWriteDataStream((char*)VtspSccConfig->u8Filename,(void*)&VsStructHeader,(tS32)sizeof(VsStructHeader));
	           if(Vs32ErrorCode>=0)
	           {/*trace out pool set to invalid OK*/	
                snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"set SCC pool %s to invalid",(char*)VtspSccConfig->u8Filename); 
                PDD_TRACE(PDD_SCC,Vu8Buffer,strlen(Vu8Buffer)+1);
             }
	           else
	           {/*trace out pool set to invalid failed*/
	             snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"set SCC pool %s to invalid failed",(char*)VtspSccConfig->u8Filename); 
	             PDD_TRACE(PDD_SCC,Vu8Buffer,strlen(Vu8Buffer)+1);
             }
          }
          else
          {
            snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"SCC pool %s is invalid",(char*)VtspSccConfig->u8Filename); 
            PDD_TRACE(PDD_SCC,Vu8Buffer,strlen(Vu8Buffer)+1);
          }
        }
      }       
    }
    PDD_UnLock(PDD_SEM_ACCESS_SCC);	   
    #endif
  }break;
  case PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_FILE:
  {
    tBool VbSuccess=FALSE;
	  tU8	  Vu8Buffer[PDD_LEN_MESSAGE]; 
	  tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_FILE");	
    if(Vs32Result==PDD_OK)
    {
      void* VvpBuffer=malloc(PDD_NOR_USER_MIN_NUMBER_OF_SECTOR*PDD_NOR_USER_MIN_SECTOR_SIZE);
	    if(VvpBuffer!=0)
	    {	   
	      if(PDD_NorUserAccessReadPartition(VvpBuffer)==PDD_OK)
		    {			 
		      Vs32Result=PDD_IsInit(PDD_LOCATION_FS,"PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_FILE");	
		      if(Vs32Result==PDD_OK)
		      {
		        if(PDD_FileWriteDataStream("testdump/PddNorUserDump",PDD_LOCATION_FS,VvpBuffer,PDD_NOR_USER_MIN_NUMBER_OF_SECTOR*PDD_NOR_USER_MIN_SECTOR_SIZE,PDD_KIND_FILE_NORMAL,FALSE)>=0)
		        {   
			        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"dump partition nor user to file %spdd/datapool/testdump/PddNorUserDump.dat",strPDD_GetConfigPath(PDD_LOCATION_CONFIG_FS));               
	            PDD_TRACE(PDD_NOR_USER,Vu8Buffer,strlen(Vu8Buffer)+1);              
              VbSuccess=TRUE;
            }
          }
        }
        free(VvpBuffer);
      }
    }
	  if(VbSuccess==FALSE)
	  {
        PDD_TRACE(PDD_NOR_USER,"read dump fails",strlen("read dump fails")+1);
	  }
  }break;
  case PDD_TEST_CMD_NOR_USER_SAVE_ACTUAL_CLUSTER:
  {
    tBool VbSuccess=FALSE;
	  tU8	  Vu8Buffer[PDD_LEN_MESSAGE]; 
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_NOR_USER_SAVE_ACTUAL_CLUSTER");	
    if(Vs32Result==PDD_OK)
    {
      void* VvpBuffer=malloc(PDD_NOR_USER_CLUSTER_SIZE);
	    if(VvpBuffer!=0)
	    {
	      if(PDD_NorUserAccessReadActualCluster(VvpBuffer)==PDD_OK)
		    {
		      Vs32Result=PDD_IsInit(PDD_LOCATION_FS,"PDD_TEST_CMD_NOR_USER_SAVE_ACTUAL_CLUSTER");	
		      if(Vs32Result==PDD_OK)
		      {		
		        if(PDD_FileWriteDataStream("testdump/PddNorUserClusterDump",PDD_LOCATION_FILE_SYSTEM,VvpBuffer,PDD_NOR_USER_CLUSTER_SIZE,PDD_KIND_FILE_NORMAL,FALSE)>=0)
		        {
			        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"dump partition nor user to file %spdd/datapool/testdump/PddNorUserClusterDump.dat",strPDD_GetConfigPath(PDD_LOCATION_CONFIG_FS)); 
			        PDD_TRACE(PDD_NOR_USER,Vu8Buffer,strlen(Vu8Buffer)+1);	         
              VbSuccess=TRUE;
            }
          }
        }
		    free(VvpBuffer);
      }
    }
	  if(VbSuccess==FALSE)
	  {
        PDD_TRACE(PDD_NOR_USER,"read dump fails",strlen("read dump fails")+1);
	  }
  }break;
  case PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_NOR:
  {
    tBool VbSuccess=FALSE;
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_FS,"PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_NOR");	
    if(Vs32Result==PDD_OK)
	  {
	    void* VvpBuffer=malloc(PDD_NOR_USER_MIN_NUMBER_OF_SECTOR*PDD_NOR_USER_MIN_SECTOR_SIZE);
	    if(VvpBuffer!=0)
	    {
	      char*       VcpFilename = (char*)&Pu8pData[PDD_TEST_ARGUMENT];   
	      if(PDD_FileReadDataStream(VcpFilename,PDD_LOCATION_FILE_SYSTEM,VvpBuffer,PDD_NOR_USER_MIN_NUMBER_OF_SECTOR*PDD_NOR_USER_MIN_SECTOR_SIZE,PDD_KIND_FILE_NORMAL)>=0)
	      {/* write to cluster*/		
          Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_NOR");	
		      if(Vs32Result==PDD_OK)
		      {		   
		         Vs32Result=PDD_NorUserAccessErase();
	           if(Vs32Result==PDD_OK)
	           {
			         if(PDD_NorUserAccessWritePartition(VvpBuffer)==PDD_OK)
			         {
			           PDD_TRACE(PDD_NOR_USER,"save dump to NOR partition PDD_UserEarly",strlen("save dump to NOR partition PDD_UserEarly")+1);
	               VbSuccess=TRUE;
               }
             }
          }
        }
	      else
		    {       
		      PDD_TRACE(PDD_NOR_USER,"filedoes not exist",strlen("error file does not exist")+1);
        }
        free(VvpBuffer);
      }	 
    }
    if(VbSuccess==FALSE)
	  {
      PDD_TRACE(PDD_NOR_USER,"save dump to NOR partition PDD_UserErly fails",strlen("ssave dump to NOR partition PDD_UserErly fails")+1);
    }
  }break;
  case PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER:
  {
    tS32 Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER");	
    if(Vs32Result==PDD_OK)
	  {	
	    tS32 Vs32Custer=PDD_NorUserAccessGetActualCluster();
	    Pu8pData[PDD_TEST_ARGUMENT]=(tU8) Vs32Custer;
	    if(Vs32Custer>=0)
	      PDD_TRACE(PDD_NOR_USER,"actual cluster found",strlen("actual cluster found")+1);
	    else
        PDD_TRACE(PDD_NOR_USER,"no actual cluster found",strlen("no actual cluster found")+1);
    }  
  }break;
  case PDD_TEST_CMD_SET_TRACE_LEVEL:
  {
    tU32* Vu32pLevel=(tU32*)((void*) &Pu8pData[PDD_TEST_ARGUMENT]);   
	  tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_SET_TRACE_LEVEL");	
    if(Vs32Result==PDD_OK)
    {
	    vPDD_SetTraceLevel(*Vu32pLevel,FALSE);
	    {
	      tU32   Vu32TraceLevel=u32PDD_GetTraceLevel();
		    tU8    Vu8Buffer[PDD_LEN_MESSAGE]; 
	      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_Tracelevel: 0x%04x",Vu32TraceLevel); 
        vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);
      }
    }
  }break;
  case PDD_TEST_CMD_CLEAR_TRACE_LEVEL:
  {    
	  tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_CLEAR_TRACE_LEVEL");	
    if(Vs32Result==PDD_OK)
	  {	
	    vPDD_SetTraceLevel(0,TRUE);
	    {
	      tU32   Vu32TraceLevel=u32PDD_GetTraceLevel();
		    tU8    Vu8Buffer[PDD_LEN_MESSAGE]; 
	      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_Tracelevel: 0x%04x",Vu32TraceLevel); 
        vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);
      }
    }   
  }break;
  case PDD_TEST_CMD_SET_TRACE_STREAM_OUTPUT:
  { 
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_SET_TRACE_STREAM_OUTPUT");	
    if(Vs32Result==PDD_OK)
	  {	
	    vPDD_SetTraceStreamOutput(Pu8pData[PDD_TEST_ARGUMENT],FALSE);
	    {
	      tU8    Vu8StreamOutput=u8PDD_GetTraceStreamOutput();
		    tU8    Vu8Buffer[PDD_LEN_MESSAGE]; 
	      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_TraceStreamOutput: 0x%04x",Vu8StreamOutput); 
        vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);
      }
    }   
  }break;
  case PDD_TEST_CMD_CLEAR_TRACE_STREAM_OUTPUT:
  { 
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_CLEAR_TRACE_STREAM_OUTPUT");	
    if(Vs32Result==PDD_OK)
	  {	
	    vPDD_SetTraceStreamOutput(0,TRUE);
	    {
	      tU8    Vu8StreamOutput=u8PDD_GetTraceStreamOutput();
		    tU8    Vu8Buffer[PDD_LEN_MESSAGE]; 
	      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_TraceStreamOutput: 0x%04x",Vu8StreamOutput); 
        vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);       
	    }
    }   
  }break;
  case PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA_STREAM:
  {
    char* VcpFilename = (char*)&Pu8pData[PDD_TEST_ARGUMENT];   	
    tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA");	
    if(Vs32Result==PDD_OK)
    {
	    tU8                            Vu8Inc;   
	    tsPddNorUserInfo*              VtsDevInfo=PDD_GetNorUserInfoShm();
	    tsPddNorUserHeaderDataStream*  VspHeaderDataStream;
	    tU8*                           Vpu8Data;
	    tU32                           Vu32Byte;
	    tS32                           Vs32SizeHeader=sizeof(tsPddFileHeader);
	    tU8                            Vu8Buffer[PDD_LEN_MESSAGE]; 
	    tsPddFileHeader*               VpHeader;                

	    if(PDD_bReadDataStreamConfiguration(VtsDevInfo)==FALSE)
	    {
         PDD_TRACE(PDD_NOR_USER,"read configuration failed",strlen("read configuration failed")+1);
      }
	    else
	    {
	      Vs32Result=-1;
        for(Vu8Inc=0;Vu8Inc<PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vu8Inc++)
	      {
	        VspHeaderDataStream=&VtsDevInfo->sHeaderDataStream[Vu8Inc];
		      /* if name equal => output */
		      if(strncmp(&VspHeaderDataStream->u8DataStreamName[0],VcpFilename, strlen(&VspHeaderDataStream->u8DataStreamName[0]))==0)
		      { //sting found
		        /*trace out name */	
		        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"name:%s",&VspHeaderDataStream->u8DataStreamName[0]); 
            PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);       
		        /*trace out offset*/
		        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"offset: %lu",VspHeaderDataStream->u32Offset); 
            PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
		        /*trace out size*/
		        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"length: %lu",VspHeaderDataStream->u32Lenght); 
            PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
		        if(VspHeaderDataStream->u32Lenght!=0)
		        { 
		          Vpu8Data=malloc(VspHeaderDataStream->u32Lenght);
		          if(Vpu8Data!=NULL)
		          {
		            Vs32Result=PDD_NorUserAccessReadDataStream(VspHeaderDataStream->u8DataStreamName,Vpu8Data,VspHeaderDataStream->u32Lenght);
			          VpHeader=(tsPddFileHeader*) ((void*)Vpu8Data);     
			          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"header: Magic/Version:0x%04x - Version/Checksum:0x%04x - Checksum/Length:0x%04x",(int)VpHeader->u32Magic,(int)VpHeader->u32Version,(int)VpHeader->u32Checksum); 
                PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);	
			          Vu8Buffer[0]='\0';
			          if(Vs32Result>Vs32SizeHeader)
			          {/*trace out data*/
		              char  VcBufValue[10]; 
	                for (Vu32Byte=Vs32SizeHeader;Vu32Byte < VspHeaderDataStream->u32Lenght;Vu32Byte++)
                  {		   
                    snprintf(VcBufValue,sizeof(VcBufValue),"0x%02x ",Vpu8Data[Vu32Byte]);
			              if((strlen(VcBufValue)+strlen(Vu8Buffer)+1)< (PDD_LEN_MESSAGE-20))
                      strncat(&Vu8Buffer[0],(const char*)&VcBufValue[0],strlen(VcBufValue)+1);
				            else
				            {/*output trace*/
				              PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
				              strncpy(&Vu8Buffer[0],(const char*)&VcBufValue[0],strlen(VcBufValue)+1);
                    }
                  }
			            /*output trace*/
			            PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
                }                 
			          free(Vpu8Data);
              }		  
            }
          }
        }
		    if(Vs32Result==-1)
		    {
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"datastream: %s not found in partition nor user",VcpFilename); 
          PDD_TRACE(PDD_NOR_USER,&Vu8Buffer[0],strlen(Vu8Buffer)+1);
        }
      }
    }
  }break;
  case PDD_TEST_CMD_NOR_USER_SAVE_BACKUP_FILE_TO_NOR:
  { //trace init
    tU8    Vu8Buffer[PDD_LEN_MESSAGE]; 
    vPDD_InitTraceLevel();
	  //-------read backup file	
	  tePddLocation VenLocation=PDD_LOCATION_FS_SECURE;    
	  PDD_Lock(PDD_SEM_ACCESS_FILE_SYSTEM);
    tS32  Vs32Result=PDD_IsInit(VenLocation,"PDD_TEST_CMD_SAVE_BACKUP_FILE_TO_NOR");	
    if(Vs32Result==PDD_OK)
	  { //get size of backup file	
	    tU32* Vu32pVersion=(tU32*)((void*) &Pu8pData[PDD_TEST_ARGUMENT]);   
	    char* VcpFilename = (char*)&Pu8pData[PDD_TEST_ARGUMENT+sizeof(tU32)];   	
	    tS32 Vs32Size=PDD_FileGetDataStreamSizeBackup(VcpFilename,PDD_LOCATION_NOR_USER)+sizeof(tsPddFileHeader);
	    if(Vs32Size> ((tS32)sizeof(tsPddFileHeader)))
	    { //read backup file
	      tBool VbValidationBackup=FALSE;
	      tU8*  VpBufferFileBackup=(tU8*)malloc(Vs32Size);
		    if(VpBufferFileBackup!=NULL)
		    {
		      Vs32Result=M_PDD_FILE_READ_DATASTREAM(VcpFilename,PDD_LOCATION_NOR_USER,(void*)VpBufferFileBackup,Vs32Size,PDD_KIND_FILE_BACKUP);
		      if(Vs32Result > ((tS32) sizeof(tsPddFileHeader)))
		      { /*check file backup file*/  
		        VbValidationBackup=PDD_ValidationCheckFile((void*)VpBufferFileBackup,Vs32Result,*Vu32pVersion);
          }
          if(VbValidationBackup==TRUE)
		      { //--------------- write backup file
		        PDD_Lock(PDD_SEM_ACCESS_NOR_USER);
            if(PDD_IsInit(PDD_LOCATION_NOR_USER,"PDD_TEST_CMD_SAVE_BACKUP_FILE_TO_NOR")==PDD_OK)
	          {
		          Vs32Result=PDD_NorUserAccessWriteDataStream(VcpFilename,(void*)VpBufferFileBackup,Vs32Result);	
			        if(Vs32Result>=0)
			        {			
	              snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"write backup file %s to NOR",VcpFilename); 
                vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE); 
              }
			        else
			        {			    
	              snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"error write backup file %s to NOR",VcpFilename); 
                vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE); 
              }
            }
		        PDD_UnLock(PDD_SEM_ACCESS_NOR_USER);			    
          }
		      else
		      {//read no valid stream
	          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"error no valid backup file %s",VcpFilename); 
            vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE); 
          }
		      free(VpBufferFileBackup);
        }
      }
	    else
	    {
	      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"error backup not found %s",VcpFilename); 
        vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE); 
      }
    }
	  PDD_UnLock(PDD_SEM_ACCESS_FILE_SYSTEM);	
  }break;
  case PDD_TEST_CMD_DELETE_DATASTREAM:
  {
    tU8            Vu8Buffer[PDD_LEN_MESSAGE]; 
    tePddLocation* VepLocation=(tePddLocation*)((void*) &Pu8pData[PDD_TEST_ARGUMENT]);   
	  char*          VcpFilename = (char*)&Pu8pData[PDD_TEST_ARGUMENT+sizeof(tePddLocation)];   
    memset(Vu8Buffer,0,PDD_LEN_MESSAGE);    
    switch(*VepLocation)
    {
      case PDD_LOCATION_FS:        
      case PDD_LOCATION_FS_SECURE:
      {
        if(pdd_delete_data_stream(VcpFilename,*VepLocation)>=0)
        {
          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"datastream '%s' could be deleted",VcpFilename);
        }
        else
        {
          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"datastream '%s' could not be deleted",VcpFilename);
        }
      }break;
      case PDD_LOCATION_NOR_USER:
      { //-------delete backup file		    
        tS32         Vs32Result=-1;
        PDD_Lock(PDD_SEM_ACCESS_FILE_SYSTEM);
        Vs32Result=PDD_IsInit(PDD_LOCATION_FS_SECURE,VcpFilename);	     
        if(Vs32Result==PDD_OK)
	      {
          Vs32Result=PDD_FileDeleteDataStream(VcpFilename,PDD_LOCATION_NOR_USER,PDD_KIND_FILE_BACKUP);
        }
        PDD_UnLock(PDD_SEM_ACCESS_FILE_SYSTEM);	
        // ------ write size 0 to NOR 
        if(Vs32Result==PDD_OK)
        {
          PDD_Lock(PDD_SEM_ACCESS_NOR_USER);
          Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,VcpFilename);	
          if(Vs32Result==PDD_OK)
	        {
            Vs32Result=PDD_NorUserAccessWriteDataStream(VcpFilename,Vu8Buffer,0);
          }
          PDD_UnLock(PDD_SEM_ACCESS_NOR_USER);	
        }
        // ------ output
        if(Vs32Result==PDD_OK)
          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"datastream '%s' could be cleared",VcpFilename);
        else
          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"datastream '%s' could not be cleared",VcpFilename);       
      }break;
      case PDD_LOCATION_SCC:
      { // ------ write size 0 to backup in NOR     
        PDD_Lock(PDD_SEM_ACCESS_NOR_USER);
        tS32  Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,VcpFilename);	
        if(Vs32Result==PDD_OK)
	      {
          Vs32Result=PDD_NorUserAccessWriteDataStream(VcpFilename,Vu8Buffer,0);
        }
        PDD_UnLock(PDD_SEM_ACCESS_NOR_USER);	            
        // write INVALID HEADER to V850 (version nummber must be matched)
        if(Vs32Result==PDD_OK)
        {
          PDD_Lock(PDD_SEM_ACCESS_SCC);
          Vs32Result=PDD_IsInit(PDD_LOCATION_SCC,VcpFilename);	
          if(Vs32Result==PDD_OK)
	        {
            memset(Vu8Buffer,0xff,12);
            // first read 
            if(PDD_SccAccessReadDataStream(VcpFilename,Vu8Buffer,12)>=PDD_OK)            
            { //invalid size and checksum       
              if(!((Vu8Buffer[0]==0xff)&&(Vu8Buffer[1]==0xff)&&(Vu8Buffer[2]==0xff)&&(Vu8Buffer[3]==0xff)))
              {
                memset(&Vu8Buffer[4],0xff,8);
                Vs32Result=PDD_SccAccessWriteDataStream(VcpFilename,Vu8Buffer,12);              
              }
            }    
          }
          PDD_UnLock(PDD_SEM_ACCESS_SCC);	    
        }
        // ------ output
        if(Vs32Result>=PDD_OK)
          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"datastream '%s' is invalid",VcpFilename);
        else
          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"datastream '%s' could not be set to invalid",VcpFilename);       
      }break;
      default:       
      break;
    }
    vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE); 
  }break;
  default: /*for lint */
    break;
  }/*end switch*/  
}/*end function*/

#ifndef PDD_TESTMANGER_ACTIVE
/* **************************************************FunctionHeaderBegin** *//*
* static void PDD_vPuttyConsolTrace(u8 Pu8Kind, const void* Pu8pData, u8 Pu8Len) 
*
* trace output function
*
* @param   Pu8Kind:    kind of level
*          Pu8pData:   pointer of the data, which send
*          Pu8Len:     length of the data, which send
*
* @return  void    
*
* @date    2008-01-10
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void PDD_vPuttyConsolTrace(tU8 Pu8Kind, const void* Pu8pData, tU32 Pu32Len)
{
  tU8                Vu8Buffer[PDD_LEN_MESSAGE];  
  tU32               Vu32TraceLevel=u32PDD_GetTraceLevel();
  
 //fprintf(stderr, "PDD_vPuttyConsolTrace: Vu32TraceLevel %x\n",Vu32TraceLevel);

  memset(Vu8Buffer,0,PDD_LEN_MESSAGE);
  {
    if ((Pu8Kind <= PDD_LEVEL_IMPORTANT)&&((Vu32TraceLevel&PDD_TRACE_IMPORTANT)==PDD_TRACE_IMPORTANT))
    { /*check if level set*/
      tBool VbImportant=FALSE;
	    switch(Pu8Kind)
	    {
	      case PDD_ERROR:
	      {
          tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ERROR: 0x%04x: %d",*VS32ErrorCode,*VS32ErrorCode);   
          VbImportant=TRUE;
	      }break;
        case PDD_ERROR_STR:
        {
          char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ERROR: %s",VcString); 
          VbImportant=TRUE;
        }break;
		    case PDD_NOR_USER:
		    {
		      char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: %s",VcString);  
          VbImportant=TRUE; //  for nor user dump 
		    }break;
		    case PDD_NOR_KERNEL:
		    {
		      char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: %s",VcString);  
		    }break;
		    case PDD_SCC:
		    {
		      char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: %s",VcString);  
		    }break;
        case PDD_INFO:
		    {
		      char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_INFO: %s",VcString);  
		    }break;
		    default:
		    {
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: no trace defined for PDD_LEVEL_IMPORTANT");  
		    }break;
	    }/*end switch*/    
	    vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,VbImportant);
    }/*end if*/
    else if ((Pu8Kind > PDD_LEVEL_IMPORTANT)&&(Pu8Kind <= PDD_LEVEL_INTERFACE_CALL)&&((Vu32TraceLevel&PDD_TRACE_INTERFACE_CALL)==PDD_TRACE_INTERFACE_CALL))
    { /*check if level set*/
	    switch(Pu8Kind)
	    {
		    case PDD_START_DATA_STREAM_SIZE:
		    {
		      char* VcString=(char*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: start function:pdd_get_data_stream_size() for datastream %s",VcString);  
		    }break;
		    case PDD_EXIT_DATA_STREAM_SIZE:
	      {
          tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: exit  function:pdd_get_data_stream_size(): return value: 0x%04x: %d",*VS32ErrorCode,*VS32ErrorCode);  
		    }break;
		    case PDD_START_READ_DATA_STREAM:
		    {
		      char* VcString=(char*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: start function:pdd_read_data_stream() for datastream %s",VcString);  
		    }break;
		    case PDD_EXIT_READ_DATA_STREAM:
        {
          tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: exit  function:pdd_read_data_stream():  return value: 0x%04x: %d",*VS32ErrorCode,*VS32ErrorCode);  
		    }break;
		    case PDD_START_WRITE_DATA_STREAM:
		    {
		      char* VcString=(char*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: start function:pdd_write_data_stream() for datastream %s",VcString);  
		    }break;
		    case PDD_EXIT_WRITE_DATA_STREAM:
		    {
          tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: exit  function:pdd_write_data_stream():  0x%04x: %d",*VS32ErrorCode,*VS32ErrorCode);  
		    }break;
		    case PDD_START_SYNC_SCC:
		    {
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: start function:pdd_sync_scc_streams()");  
		    }break;
		    case PDD_EXIT_SYNC_SCC:
		    {
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: exit  function:pdd_sync_scc_streams()");  
		    }break;
		    case PDD_START_GET_ELEMENT_VALUE:
		    {
		      char* VcString=(char*) Pu8pData;
			    snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: start function:pdd_helper_get_element_from_stream() element:%s",VcString);  
		    }break;
		    case PDD_EXIT_GET_ELEMENT_VALUE:
		    {
		      tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: exit function:pdd_helper_get_element_from_stream():  return value: 0x%04x: %d",*VS32ErrorCode,*VS32ErrorCode);    
		    }break;
		    case PDD_START_SYNC_NOR_USER:
		    {
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: start function:pdd_sync_nor_user_streams()");  
		    }break;
		    case PDD_EXIT_SYNC_NOR_USER:
		    {
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: exit  function:pdd_sync_nor_user_streams()");  
		    }break;
		    case PDD_START_DELETE_DATA_STREAM:
		    {
			    char* VcString=(char*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: start function:pdd_delete_data_stream() for datastream %s",VcString);  
		    }break;
		    case PDD_EXIT_DELETE_DATA_STREAM:
		    {
			    tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: exit  function:pdd_delete_data_stream():  0x%04x: %d",*VS32ErrorCode,*VS32ErrorCode);  
		    }break;
		    case PDD_START_READ_DATA_STREAM_EARLY:
		    {
			    char* VcString=(char*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: start function:pdd_read_datastream_early_from_nor() for datastream %s",VcString);  
		    }break;
		    case PDD_EXIT_READ_DATA_STREAM_EARLY:
		    {
			    tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: exit  function:pdd_read_datastream_early_from_nor():  0x%04x: %d",*VS32ErrorCode,*VS32ErrorCode);  
		    }break;
	      default:
	      {
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: no trace defined for PDD_LEVEL_INTERFACE_CALL");  
	      }break;
	    }/*end switch*/  
		  vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);
    }/*end else if*/
    else if ((Pu8Kind > PDD_LEVEL_INTERFACE_CALL)&&(Pu8Kind <= PDD_LEVEL_INFO_ADMIN)&&((Vu32TraceLevel&PDD_TRACE_ADMIN)==PDD_TRACE_ADMIN))
    { /*check if level set*/
	    switch(Pu8Kind)
	    {
		    case PDD_ADMIN_FIRST_ATTACH:
			  {
			    snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: proccess first attach");  
			  }break;
		    case PDD_ADMIN_NOT_FIRST_ATTACH:
			  {
			    snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: proccess not first attach");  
			  }break;
		    case PDD_ADMIN_SEM_WAIT:
			  {
			    tU8* Vu8Sem=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: sem number %d lock",*Vu8Sem);   
			  }break;
		    case PDD_ADMIN_SEM_POST:
			  {
			    tU8* Vu8Sem=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: sem number %d post",*Vu8Sem);   
			  }break;
		    case PDD_ADMIN_SEM_VALUE:
			  {
			    tS32* VS32Value=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: sem value:%d",*VS32Value);   
			  }break;
		    case PDD_ADMIN_SEM_INIT_SUCCESS: 
			  {
			    tU8* Vu8Sem=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: sem %d init success",*Vu8Sem);   
			  }break;
		    case PDD_ADMIN_SEM_DESTROY_SUCCESS:
			  {
			    tU8* Vu8Sem=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: sem %d destroy success",*Vu8Sem);     
			  }break;
		    case PDD_ADMIN_ATTACH_COUNT:
			  {
			    tS32* VS32Value=(tS32*) Pu8pData;
			    snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: number process attach: %d ",*VS32Value);   
			  }break;
		    case PDD_ADMIN_TRACE_LEVEL:
			  {
			    tS32* VS32Value=(tS32*) Pu8pData;
			    snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: trace level: 0x%04x ",*VS32Value);   
			  }break;
		    case PDD_ADMIN_TRACE_STREAM_OUTPUT:
			  {	
			    tU8* Vu8Value=(tU8*) Pu8pData;
			    snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: trace data stream output: %d ",*Vu8Value);   
			  }break;
        case PDD_ADMIN_TRACE_START_GET_CONFIG_PATH:
        {
          tePddLocation* VtsValue=(tePddLocation*) Pu8pData;
			    snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: start function PDD_GetConfigPathPdd() for location %d ",*VtsValue); 
        }break;
        case PDD_ADMIN_TRACE_CONFIG_PATH:
			  {	
			    char* VcString=(char*) Pu8pData;
			    snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_ADMIN: config path: '%s' ",VcString);   
			  }break;
	      default:
	      {
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: no trace defined for PDD_LEVEL_INFO_ADMIN");  
	      }break;
	    }/*end switch*/    
	    vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);	 
    }/*end else if*/
    else if ((Pu8Kind > PDD_LEVEL_INFO_ADMIN)&&(Pu8Kind <= PDD_LEVEL_PDD)&&((Vu32TraceLevel&PDD_TRACE_PDD)==PDD_TRACE_PDD))
    { /*check if level set*/
	    switch(Pu8Kind)
	    {	
		    case PDD_SAVE_DATA_INDENTICAL:
		    {
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: datastream to write indentical with stored datastream ");  
		    }break;
		    case PDD_GET_ELEMENT_NAME:
		    {
		      char* VcString=(char*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: get element %s",VcString);  
		    }break;
		    case PDD_GET_ELEMENT_VERSION:
		    {
		      tU8* Vu8Version=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: version of element %d",*Vu8Version);  
		    }break;
		    case PDD_GET_ELEMENT_LENGTH:
		    {
		      tU32* Vu32Lenght=(tU32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: payload lenght of element %d",*Vu32Lenght);  
		    }break;
	      default:
	      {
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: no trace defined for PDD_LEVEL_PDD");  
	      }break;
	    }/*end switch*/    
	    vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);	  
    }/*end else if*/
    else if ((Pu8Kind > PDD_LEVEL_PDD)&&(Pu8Kind <= PDD_LEVEL_FILE)&&((Vu32TraceLevel&PDD_TRACE_FILE)==PDD_TRACE_FILE))
    {/*check if level set*/
	    switch(Pu8Kind)
	    {
	      case PDD_FILE_TRACE_PATH:
	      {
	        char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: file path:%s",VcString);  
	      }break;
	      case PDD_FILE_OPENFILE_SUCCESS:
	      {
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: file open success");  
	      }break;
	      case PDD_FILE_SIZE:
	      {
	        tU32* VU32Value=(tU32*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: data stream size: %d bytes - 0x%04x",*VU32Value,*VU32Value);  
	      }break;
	      case PDD_FILE_STAT_SUCCESS:
	      {
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: fstat success");  
	      }break;		
	      case PDD_FILE_READ_SIZE:
	      {
	        tU32* VU32Value=(tU32*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: size of bytes read from flash: %d bytes - 0x%04x",*VU32Value,*VU32Value);  
	      }break;
	      case PDD_FILE_WRITE_SIZE:
	      {
	        tU32* VU32Value=(tU32*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: size of bytes write to flash: %d bytes - 0x%04x",*VU32Value,*VU32Value);  
	      }break;
	      case PDD_FILE_CREATE_PATH_SUCCESS:
        {
	        char* VcString=(char*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: create path:%s success",VcString);  
	      }break;
	      case PDD_FILE_CREATE_PATH_ERROR:
	      {
	        char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: create path:%s failed",VcString);  
	      }break;
	      case PDD_FILE_OPENFILE_FAILS:
        {
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: file open failed");  
	      }break;
	      case PDD_FILE_TIME:
	      {
	        tU32* VU32Value=(tU32*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: mount failed - wait - actual time:%d s",*VU32Value);  
	      }break;
	      case PDD_FILE_FIND_MOUNT_POINT:
	      {
	        char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: find mount point:%s",VcString);  
	      }break;
        case PDD_FILE_INFO:
        {
          char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_FILE: %s",VcString);
        }break;
	      default:
	      {
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: no trace defined for PDD_LEVEL_FILE");  
	      }break;
	    }/*end switch*/    
		  vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);	  
    }/*end else if*/
    else if ((Pu8Kind > PDD_LEVEL_FILE)&&(Pu8Kind <= PDD_LEVEL_SCC)&&((Vu32TraceLevel&PDD_TRACE_SCC)==PDD_TRACE_SCC))
    {/*check if level set*/
		tU8*               Vu8pBufferLarge=NULL;
	    switch(Pu8Kind)
	    {
		    case PDD_SCC_RECV_MESSAGE: 
		    {
		      Vu8pBufferLarge=malloc(PDD_LEN_MESSAGE_SCC);
		      if(Vu8pBufferLarge!=NULL)
		      {
	          tU8* Vu8Value=(tU8*) Pu8pData;
		        tU8* Vu8Print=(tU8*) Vu8pBufferLarge;
		        int  VPrintCnt;
		        int  VPrintLine=0;
	          int  ViLen=0;
	          VPrintCnt=snprintf(Vu8Print,PDD_LEN_MESSAGE_SCC,"PDD_SCC: receive:");
		        while((Pu32Len>0)&&(Vu8Print<(Vu8pBufferLarge+PDD_LEN_MESSAGE_SCC)))
		        {
		          Vu8Print+=VPrintCnt;
		          VPrintLine += VPrintCnt;
		          ViLen=strlen(Vu8pBufferLarge);
		          if(VPrintLine<PDD_TEST_LEN_LINE)
		          {
		            VPrintCnt=snprintf(Vu8Print,PDD_LEN_MESSAGE_SCC-ViLen," 0x%02x",*Vu8Value);
		          }
		          else
		          {
		            VPrintCnt=snprintf(Vu8Print,PDD_LEN_MESSAGE_SCC-ViLen,"\nPDD_SCC: 0x%02x",*Vu8Value);
			          VPrintLine=0;
		          }
		          Vu8Value++;
		          Pu32Len--;
		        }
		      }
		    }break;
		    case PDD_SCC_SEND_MESSAGE: 
		    {
		      Vu8pBufferLarge=malloc(PDD_LEN_MESSAGE_SCC);
			    if(Vu8pBufferLarge!=NULL)
			    {
		        tU8* Vu8Value=(tU8*) Pu8pData;
			      tU8* Vu8Print=(tU8*) Vu8pBufferLarge;
			      int  VPrintCnt;
            int  VPrintLine=0;
			      int  ViLen=0;
		        VPrintCnt=snprintf(Vu8Print,PDD_LEN_MESSAGE_SCC,"PDD_SCC: send:");
			      while((Pu32Len>0)&&(Vu8Print<(Vu8pBufferLarge+PDD_LEN_MESSAGE_SCC)))
			      {
			        Vu8Print+=VPrintCnt;
			        VPrintLine += VPrintCnt;
			        ViLen=strlen(Vu8pBufferLarge);
			        if(VPrintLine<PDD_TEST_LEN_LINE)
			        {             
			          VPrintCnt=snprintf(Vu8Print,PDD_LEN_MESSAGE_SCC-ViLen," 0x%02x",*Vu8Value);
			        }
			        else
			        {
			          VPrintCnt=snprintf(Vu8Print,PDD_LEN_MESSAGE_SCC-ViLen,"\nPDD_SCC: 0x%02x",*Vu8Value);
				        VPrintLine=0;
			        }
			        Vu8Value++;
			        Pu32Len--;
			      }
			    }
		    }break;
		    case PDD_SCC_RECV_RETURN_DGRAM:
		    {
		      tU32* VU32Value=(tU32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: return dgram_recv(): %d",*VU32Value);  
		    }break;
		    case PDD_SCC_SEND_RETURN_DGRAM:
		    {
		      tU32* VU32Value=(tU32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: return dgram_send(): %d",*VU32Value);  
		    }break;
		    case PDD_SCC_SIZE_WITH_HEADER:
		    {
		      tU32* VU32Value=(tU32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: size data with header: %d",*VU32Value);  
		    }break;
		    case PDD_SCC_BEGINN_POLL:
		    {
		      tU32* VU32Value=(tU32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: beginn poll for event POLLIN Timeout: %d ms ",*VU32Value);  
		    }break;		 
		    case PDD_SCC_FRAME_LENGHT:
		    {
		      tU32* VU32Value=(tU32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: frame length: %d",*VU32Value);  
		    }break;
		    case PDD_SCC_ELEMENT_NAME:
		    {
			    char* VcString=(char*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: element name: %s",VcString);  
		    }break;
		    case PDD_SCC_HASH:
		    {
		      tU32* VU32Value=(tU32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: hash:%x",*VU32Value);  
		    }break;
		    case PDD_SCC_LENGTH_DATA_EXPECTED:
		    {
		      tU8* Vu8Value=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: expected data length:%d",*Vu8Value);  
		    }break;
		    case PDD_SCC_LENGTH_DATA_RECEIVED:
		    {
		      tU8* Vu8Value=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_SCC: received data length:%d",*Vu8Value);  
		    }break;
	      default:
	      {
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: no trace defined for PDD_LEVEL_SCC");  
	      }break;
	    }/*end switch*/    
		  if(Vu8pBufferLarge!=NULL)
		  {
		    vPddConsoleTrace((const void*)Vu8pBufferLarge,strlen(Vu8pBufferLarge)+1,FALSE);
		    free(Vu8pBufferLarge);
		  }
		  else
		  {
	      vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);
		  }
    }/*end else if*/
    else if ((Pu8Kind > PDD_LEVEL_SCC)&&(Pu8Kind <= PDD_LEVEL_NOR_USER)&&((Vu32TraceLevel&PDD_TRACE_NOR_USER)==PDD_TRACE_NOR_USER))
	  {/*check if level set*/
	    switch(Pu8Kind)
	    {
		    case PDD_NOR_USER_DEBUG_SIZE_SECTOR:
		    {
		      tU32* Vu32Size=(tU32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: sector size: %d k",*Vu32Size);  
		    }break;
		    case PDD_NOR_USER_DEBUG_SIZE_CHUNK:
		    {
		      tU16* Vu16Size=(tU16*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: chunk size: %d bytes",*Vu16Size);  
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_OLD:
		    {
		      tS32* Vs32Cluster=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: write to cluster: old cluster is %d",*Vs32Cluster);  
		    }break;
		    case PDD_NOR_USER_DEBUG_RETURN:
		    {
		      tBool* VbReturn=(tBool*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: return value:%d",*VbReturn);   
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_WRITE_TO:
        {
		      tS32* Vs32Cluster=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: write to cluster: %d",*Vs32Cluster);   
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_NEW:
		    {
		      tS32* Vs32Cluster=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: write to cluster: new cluster is %d",*Vs32Cluster);   
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_FREE:
		    {
		      tS32* Vs32Cluster=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: cluster %d is free",*Vs32Cluster);   
		    }break;
		    case PDD_NOR_USER_DEBUG_FORMAT_FLASH:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: format flash sector  %u",*Vs32Data);   
		    }break;
		    case PDD_NOR_USER_DEBUG_ERASE_FLASH:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: erase flash for sector %u",*Vs32Data);   
		    }break;		 
		    case PDD_NOR_USER_DEBUG_CLUSTER_NOT_FORMATED:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: cluster %u is not formated",*Vs32Data);   
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_FORMATED:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: cluster %u is formated",*Vs32Data);   
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_LAST_USED:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: read from cluster: last cluster used is %d",*Vs32Data);   
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_REPLACE_OLD:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: read from cluster: replace old cluster %d",*Vs32Data);   
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_REPLACE_NEW:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: read from cluster: with cluster %d",*Vs32Data);   
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_TO_READ:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: read from cluster: cluster to read is %d",*Vs32Data);   
		    }break;
		    case PDD_NOR_USER_DEBUG_CLUSTER_TO_READ_STATUS:
		    case PDD_NOR_USER_DEBUG_DATA_STREAM_HEADER: 
		    break;
		    case PDD_NOR_USER_DEBUG_DATA_STREAM_NAME:
        {
		      char* VcString=(char*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: name of data stream: %s",VcString);   
		    }break;
		    case PDD_NOR_USER_DEBUG_LFX_OPEN:
		    {
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: call LFX_open(""PDD_UserEarly"")");   
		    }break;
		    case PDD_NOR_USER_DEBUG_ERASE_COUNTER:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: erase counter: %d",*Vs32Data);   
		    }break;
		    case PDD_NOR_USER_DEBUG_NUMBER_READ_DATA_STREAM:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: read number data stream: %d",*Vs32Data);   
		    }break;
		    case PDD_NOR_USER_DEBUG_NUMBER_BLOCKS:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_USER: number blocks: %d",*Vs32Data);   
		    }break;
        default:
        {
          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: no trace defined for PDD_LEVEL_NOR_USER");  
        }break;
	    }/*end switch*/    
	    vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);
    }/*end else if*/
    else if ((Pu8Kind > PDD_LEVEL_NOR_USER)&&(Pu8Kind <= PDD_LEVEL_VALIDATION)&&((Vu32TraceLevel&PDD_TRACE_VALIDATION)==PDD_TRACE_VALIDATION))
    {/*check if level set*/
	    switch(Pu8Kind)
		  {
	      case PDD_VALIDATION_HEADER_FILE: /*todo*/
	      break;
	      case PDD_VALIDATION_CHECKSUM_FILE:
        {
	        tS32* Vs32Data=(tS32*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_VALID: new checksum: 0x%04x",*Vs32Data);   
	      }break;
	      case PDD_VALIDATION_HEADER_NOR_KERNEL:/*todo*/
	      break;
	      case PDD_VALIDATION_CHECKSUM_NOR_KERNEL:
	      {
	        tS32* Vs32Data=(tS32*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_VALID_NOR_KERNEL: new checksum: 0x%04x",*Vs32Data);   
	      }break;
	      case PDD_VALIDATION_HEADER_SCC:/*todo*/
	      break;
	      case PDD_VALIDATION_CHECKSUM_SCC:
	      {
	        tS32* Vs32Data=(tS32*) Pu8pData;
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_VALID_SCC: new checksum: 0x%04x",*Vs32Data);   
	      }break;
	      default:
	      {
	        snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: no trace defined for PDD_LEVEL_VALIDATION");  
	      }break;
	    }/*end switch*/    
	    vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);
    }/*end else if*/
    else if ((Pu8Kind > PDD_LEVEL_VALIDATION)&&(Pu8Kind <= PDD_LEVEL_NOR_KERNEL)&&((Vu32TraceLevel&PDD_TRACE_NOR_KERNEL)==PDD_TRACE_NOR_KERNEL))
    {/*check if level set*/
	    switch(Pu8Kind)
	    {
		    case PDD_NOR_KERNEL_DEBUG_NUMBER_BLOCKS:
		    {
		      tU8* Vu8pData=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: number blocks: %d",*Vu8pData);   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_SIZE_CHUNK:
		    {
		      tU16* Vu16pData=(tU16*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: chunk size: %u bytes",*Vu16pData);   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_SIZE_SECTOR:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: sector size: %u k",*Vs32Data);   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_LFX_OPEN:
		    {
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: all LFX_open(""PDD_Kernel"")");   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_RETURN:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: return value:%x",*Vs32Data);   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_ERASE_COUNTER:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: erase counter: %d",*Vs32Data);   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_ERASE_FLASH:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: erase flash for sector %u",*Vs32Data);   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_SIZE_DATA:
		    {
		      tS32* Vs32Data=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: s32SizeData %u",*Vs32Data);   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_VALID:
		    {
		      tU8* Vu8pData=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: bValid %u",*Vu8pData);   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_SYNC_SECTOR:
		    {
		      tU8* Vu8pData=(tU8*) Pu8pData;
		      snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD_NOR_KERNEL: sync sector %u",*Vu8pData);   
		    }break;
		    case PDD_NOR_KERNEL_DEBUG_SECTOR_INFO:
		    break;
        default:
        {
          snprintf(Vu8Buffer,PDD_LEN_MESSAGE,"PDD: no trace defined for PDD_LEVEL_NOR_KERNEL");  
        }break;
	    }/*end switch*/    
		  vPddConsoleTrace((const void*)Vu8Buffer,strlen(Vu8Buffer)+1,FALSE);	 		
    }/*end else if*/
  }/*end if check param*/
}
/* **************************************************FunctionHeaderBegin** *//*
* static void PDD_vSetErrorEntry(const tU8* Pu8pData) 
*
* trace output function
*
* @param   Pu8pData:   pointer of the data, which send
*          
* @return  void    
*
* @date    2014-01-22
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void  PDD_vSetErrorEntry(const tU8* Pu8pData)
{
  #ifndef PDD_UNIT_TEST
  tU8                Vu8Buffer[PDD_LEN_MESSAGE]; 
  Vu8Buffer[0]=PDD_NOR_ERROR_MEMORY;
  snprintf(&Vu8Buffer[1],PDD_LEN_MESSAGE-1,"PDD: %s",Pu8pData); 
  WriteErrMemEntry((int)TR_COMP_PDD,&Vu8Buffer[0],strlen(&Vu8Buffer[1])+1,0);
  #endif
}
#endif
#ifdef __cplusplus
}
#endif
