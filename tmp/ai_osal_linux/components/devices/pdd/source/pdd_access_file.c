/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        pdd_access_file.c
 *
 * This file contains all functions for access to file
 * 
 * global function:
 * -- PDD_FileCreatePath:
 *         create all nessesary paths for the file access.
 * -- PDD_FileGetDataStreamSize:
 *         PDD returns the maxsize of the data stream.
 * -- PDD_FileReadDataStream:
 *         read the file into buffer 
 * -- PDD_FileWriteDataStream:
 *         write data stream into file 
 *
 * local function:
 * -- PDD_CheckPathMount:
 *         check path mount
 * -- PDD_CheckCreatePath:
 *         create the path, if doesn't exist
 * -- PDD_GetPath
 *         get the whole path name (path with name)
 * -- PDD_CheckCreateFolderStream
 *         check if folder in stream and create
 *
 * @date        2013-02-19
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef PDD_UNIT_TEST
#define PDD_CHECK_PATH
#endif
#ifdef GEN3X86 // don't check mount with LSIM
#undef PDD_CHECK_PATH
#endif
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <memory.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <limits.h>   
#include "system_types.h"
#include "system_definition.h"
#include "pdd.h"
#include "LFX2.h"
#include "pdd_config_nor_user.h"
#include "inc.h"
#include "dgram_service.h"
#include "pdd_variant.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#ifdef PDD_SCC_POOL_EXIST
#include "DataPoolSCC.h" 
#endif

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
/*Folder*/
#define PDD_FILE_FOLDER_DATAPOOL              "datapool/" 
#define PDD_FILE_FOLDER_BACKUP                "backup/"
#define PDD_FILE_FOLDER_BACKUP_NOR            "backup/nor/" 
#define PDD_FILE_FOLDER_BACKUP_DATAPOOL       "backup/datapool/" 

/*configuration files*/
#define PDD_CONFIG_FILE_DEFAULT            "/etc/pdd/pdd-default.conf"
#define PDD_CONFIG_FILE                    "/etc/pdd/pdd.conf"

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/


/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/


/************************************************************************
| variable definition (scope: module-global)
|-----------------------------------------------------------------------*/


/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
#ifdef PDD_CHECK_PATH
static char* PDD_CheckPathMount(char* PpPath);
#endif
static tS32  PDD_CheckCreatePath(const char*,const char*,tBool);
static tS32  PDD_GetPath(const char*,tePddLocation, char*,tePddKindFile);
static tS32  PDD_CheckCreateFolderStream(const char*,const char*);
/******************************************************************************
* FUNCTION: PDD_FileCreatePath()
*
* DESCRIPTION: create all nessesary paths for the file access.  
*
* RETURNS: PDD_OK or error value
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
tS32 PDD_FileCreatePath(tePddLocation PenLocation,const char* PtsInfoStream)
{
  tS32  VS32Result=PDD_OK;
  char* VstrPathName=NULL; 
  char* Vu8Pos=NULL;
  tU32  Vu32MaxMountTime=0;

  if(PenLocation==PDD_LOCATION_FS)
  {
    VstrPathName=strPDD_GetConfigPath(PDD_LOCATION_CONFIG_FS);
    if(VstrPathName!=NULL)
    {
	    Vu8Pos=VstrPathName;
	    Vu32MaxMountTime=PDD_FILE_PATH_MAX_MOUNT_TIME;
    }
  }
  else
  {
    VstrPathName=strPDD_GetConfigPath(PDD_LOCATION_CONFIG_FS_SECURE);
	  if(VstrPathName!=NULL)
    {
	    Vu8Pos=VstrPathName;
	    Vu32MaxMountTime=PDD_FILE_PATH_SECURE_MAX_MOUNT_TIME;
    }   
  }
  if(Vu8Pos==NULL) 
  {/*error path not defined */
    tU8 Vu8Buffer[200];		
    VS32Result=PDD_ERROR_FILE_NO_FILE_PATH_DATAPOOL;	 
    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",VS32Result,VS32Result,PtsInfoStream);
	  PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);  
  }
  else
  {/*check if path mounted*/
#ifdef PDD_CHECK_PATH  
    struct timespec VTimer={0,0};
    if(VstrPathName!=NULL)
    { /*mount can take time*/
      do
	    {
        Vu8Pos=PDD_CheckPathMount(VstrPathName);
	      if(Vu8Pos==NULL)
	      { 
	        if(VTimer.tv_sec==0)
	        {
		        tU8    Vu8Buffer[200];		
            memset(Vu8Buffer,0,sizeof(Vu8Buffer));
		        if(PenLocation==PDD_LOCATION_FS)
		        {
		          snprintf(Vu8Buffer,sizeof(Vu8Buffer),"FILE: wait for partition (PDD_UserFS) to mount (%s)",PtsInfoStream);	
		          PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);  
		          fprintf(stderr,"FILE: wait for partition (PDD_UserFS) to mount (%s)\n",PtsInfoStream);
		        }
		        else
		        {		  
			        snprintf(Vu8Buffer,sizeof(Vu8Buffer),"FILE: wait for partition (PDD_UserFSEarly) to mount (%s)",PtsInfoStream);	
		          PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);  
		          fprintf(stderr,"FILE: wait for partition (PDD_UserFSEarly) to mount (%s)\n",PtsInfoStream);
            }
          }
		      /*not mounted get run time*/
          clock_gettime(CLOCK_MONOTONIC, &VTimer);
          /*trace out time*/ 
		      PDD_TRACE(PDD_FILE_TIME,&VTimer.tv_sec,sizeof(time_t));	
		      PDD_TRACE(PDD_FILE_TIME,&Vu32MaxMountTime,sizeof(time_t));	
	        /*check runtime*/
		      if(VTimer.tv_sec < (time_t)Vu32MaxMountTime)
		      { /*if not time reached sleep 200 ms*/
            usleep(200000);		
          }
        }
      }while((Vu8Pos==NULL)&&(VTimer.tv_sec < (time_t)Vu32MaxMountTime));
    }
#else
    Vu8Pos=VstrPathName;
#endif
#ifdef PDD_CHECK_PATH
	  if(Vu8Pos==NULL)
	  { 
	    tU8    Vu8Buffer[200];		
      memset(Vu8Buffer,0,sizeof(Vu8Buffer));
	    /*path not mounted return error*/
	    VS32Result=PDD_ERROR_FILE_PATH_NOT_MOUNTED;
	    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",VS32Result,VS32Result,PtsInfoStream);
	    PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
	    /*error memory entry*/
	    if(PenLocation==PDD_LOCATION_FS)
	    {
	      snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"FILE: partition not mounted(PDD_UserFS,%s)",PtsInfoStream);	
		    PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);  	  
	    }
	    else
	    {
	      snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"FILE: partition not mounted(PDD_UserFSEarly,%s)",PtsInfoStream);	
		    PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);  	  
	    }
    }
	  else
#endif
	  { 
      if(VstrPathName!=NULL)
      { /* path is mounted; mount is Vu8Pos*/	
        /* check path exist */    
        VS32Result=PDD_CheckCreatePath(VstrPathName,NULL,FALSE);
        if(VS32Result!=PDD_OK)
        {        
          char  VstrExtToMountPath[PDD_FILE_MAX_PATH];   
          char  VstrMountPath[PDD_FILE_MAX_PATH];
          strncpy(VstrMountPath,VstrPathName,strlen(VstrPathName));
          char* Vu8PosExtToMountPath=strstr(VstrPathName,Vu8Pos);
          char* Vu8PosEndMountPath=strstr(VstrMountPath,Vu8Pos);
          if((Vu8PosExtToMountPath!=NULL)&&(Vu8PosEndMountPath!=NULL))
          {
            memset(VstrExtToMountPath,0x0,PDD_FILE_MAX_PATH);
            strncpy(VstrExtToMountPath,Vu8PosExtToMountPath,strlen(Vu8PosExtToMountPath));   
            Vu8PosEndMountPath[0]='\0';     
            char *Vu8PosExt=(char*) &VstrExtToMountPath[0];
            do
            {  	            
              /*check next position of '/'*/
              Vu8PosExt=strchr((tU8*)(Vu8PosExt+1),'/');
              if(Vu8PosExt!=NULL)
              {
                Vu8PosExt[1]='\0';               
                VS32Result=PDD_CheckCreatePath(VstrMountPath,VstrExtToMountPath,TRUE);	  
              }
              strncpy(VstrExtToMountPath,Vu8PosExtToMountPath,strlen(Vu8PosExtToMountPath));
            }while((VS32Result==PDD_OK)&&(Vu8PosExt!=NULL));		           
          }
        }
      }
    }
  }
  if((VS32Result==PDD_OK)&&(VstrPathName!=NULL))
  { /*get path*/   	   
    VS32Result=PDD_CheckCreatePath(VstrPathName,PDD_FILE_FOLDER_DATAPOOL,TRUE);
    if(VS32Result==PDD_OK)
      VS32Result=PDD_CheckCreatePath(VstrPathName,PDD_FILE_FOLDER_BACKUP,TRUE);
    if(VS32Result==PDD_OK)
      VS32Result=PDD_CheckCreatePath(VstrPathName,PDD_FILE_FOLDER_BACKUP_DATAPOOL,TRUE);
    if((VS32Result==PDD_OK)&&(PenLocation==PDD_LOCATION_FS_SECURE))
    {/*only for PDD_LOCATION_FS_SECURE*/
      VS32Result=PDD_CheckCreatePath(VstrPathName,PDD_FILE_FOLDER_BACKUP_NOR,TRUE);    
    }
  }
  return(VS32Result);
}
/******************************************************************************
* FUNCTION: PDD_FileGetDataStreamSize()
*
* DESCRIPTION: PDD returns the maxsize of the data stream. 
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash
*
* RETURNS: 
*      positive value: size of data stream with header
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
tS32 PDD_FileGetDataStreamSize(const char* PtsDataStreamName,tePddLocation PenLocation)
{ 
  intptr_t               ViFile;
  tS32              Vs32Length=PDD_ERROR_FILE_NO_VALID_SIZE;
  tS32              Vs32Return;
  tePddKindFile     VeInc;
  char              VstrFileName[PDD_FILE_MAX_PATH_NAME];  

  // get length of file and backup file
  for(VeInc=PDD_KIND_FILE_NORMAL; VeInc <= PDD_KIND_FILE_BACKUP; VeInc++)
  { /* get path */	  
    Vs32Return=PDD_GetPath(PtsDataStreamName,PenLocation,&VstrFileName[0],VeInc);
    if(Vs32Return==PDD_OK)
	  {
      ViFile = open(VstrFileName, O_RDONLY);
	    /* check file handle */
      if(ViFile >= 0)
      {  
	      struct stat VFileStat;
	      PDD_TRACE(PDD_FILE_OPENFILE_SUCCESS,0,0);
	      /*get length of file*/
	      if(fstat(ViFile,&VFileStat) >= 0)
	      {/*check lenght*/
	        PDD_TRACE(PDD_FILE_STAT_SUCCESS,0,0);
	        if (VFileStat.st_size > Vs32Length) 		
		      {		          
	          Vs32Length=(tU32)VFileStat.st_size;			
          }
        }/*end else fstat OK*/
        /* close file*/ 
	      if (close(ViFile)<0) 
        {
		      tU8 Vu8Buffer[80];		
		      snprintf(Vu8Buffer,sizeof(Vu8Buffer),"PDD_FileGetDataStreamSize() close(ViFile) fails error code:%d",errno); 
		      PDD_TRACE(PDD_INFO,Vu8Buffer,strlen(Vu8Buffer)+1);	
        }  	
      }/*end else open OK*/     
	    else
	    {
	      PDD_TRACE(PDD_FILE_OPENFILE_FAILS,0,0);
	    }
	  }/*end if*/	
  }/*end for*/ 
  /*check error*/
  if((Vs32Return==PDD_ERROR_FILE_NO_FILE_PATH_SECURE)||(Vs32Return==PDD_ERROR_FILE_NO_FILE_PATH_DATAPOOL))
  { 
    Vs32Length=Vs32Return;
  }  
  return(Vs32Length);
}
/******************************************************************************
* FUNCTION: PDD_FileGetDataStreamSizeBackup()
*
* DESCRIPTION: PDD returns the maxsize of the data stream backup. 
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream backup
*
* RETURNS: 
*      positive value: size of data stream with header
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2014 02 18
*****************************************************************************/
tS32 PDD_FileGetDataStreamSizeBackup(const char* PtsDataStreamName,tePddLocation PenLocation)
{
  intptr_t               ViFile;
  tS32              Vs32Length=PDD_ERROR_FILE_NO_VALID_SIZE;
  tS32              Vs32Return;
  char              VstrFileName[PDD_FILE_MAX_PATH_NAME];  

  Vs32Return=PDD_GetPath(PtsDataStreamName,PenLocation,&VstrFileName[0],PDD_KIND_FILE_BACKUP);
  if(Vs32Return==PDD_OK)
  {
    ViFile = open(VstrFileName, O_RDONLY);
    /* check file handle */
    if(ViFile >= 0)
    {  
      struct stat VFileStat;
	    PDD_TRACE(PDD_FILE_OPENFILE_SUCCESS,0,0);
	    /*get length of file*/
	    if(fstat(ViFile,&VFileStat) >= 0)
	    {/*check lenght*/
	      PDD_TRACE(PDD_FILE_STAT_SUCCESS,0,0);
	      if (VFileStat.st_size > Vs32Length) 		
	      {		 
	        Vs32Length=(tU32)VFileStat.st_size;				
        }
      }/*end else fstat OK*/
      /* close file*/ 
	    if (close(ViFile)<0) 
	    {
	      tU8 Vu8Buffer[80];		
		    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"PDD_FileGetDataStreamSizeBackup() close(ViFile) fails error code:%d",errno); 
		    PDD_TRACE(PDD_INFO,Vu8Buffer,strlen(Vu8Buffer)+1);	
      }  	
    }/*end else open OK*/     
	  else
	  {
	    PDD_TRACE(PDD_FILE_OPENFILE_FAILS,0,0);
    }
  }/*end if*/	
  if((Vs32Return==PDD_ERROR_FILE_NO_FILE_PATH_SECURE)||(Vs32Return==PDD_ERROR_FILE_NO_FILE_PATH_DATAPOOL))
  { 
    Vs32Length=Vs32Return;
  } 
  return(Vs32Length);
}
/******************************************************************************
* FUNCTION: PDD_FileReadDataStream()
*
* DESCRIPTION: read the file into buffer 
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash
*      Ppu8ReadBuffer: the buffer for the read data
*      Pu32SizeReadBuffer: the size of the read buffer
*      PbKindFile: Kind File (normal or backup)
*
* RETURNS: 
*      positive value: size of read data 
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 02 20
*****************************************************************************/
tS32 PDD_FileReadDataStream(const char* PtsDataStreamName,tePddLocation PenLocation,void *Ppu8ReadBuffer,tS32 Ps32SizeReadBuffer,tePddKindFile PeKindFile)
{ 
  tS32     Vs32Result=PDD_OK;
  intptr_t      ViFile;
  char     VstrFileName[PDD_FILE_MAX_PATH_NAME];  

  /* get path */	  
  Vs32Result=PDD_GetPath(PtsDataStreamName,PenLocation,&VstrFileName[0],PeKindFile);
  /*check error*/
  if(Vs32Result==PDD_OK)
  { /*open file name*/
    errno = 0;
    ViFile = open(VstrFileName, O_RDONLY);
	  /* check file handle */
    if(ViFile < 0)
    { /*  error*/
      Vs32Result=PDD_ERROR_FILE_OPEN;  
	    if (errno != ENOENT)
      {
        tU8 Vu8Buffer[200];		
        snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsDataStreamName);
	      PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);  
      }
    }
	  else
	  {
      struct stat VFileStat;
      if(fstat(ViFile,&VFileStat) < 0)
	    { /* error*/
        tU8 Vu8Buffer[200];		
        Vs32Result=PDD_ERROR_FILE_FSTAT;         
        snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsDataStreamName);
	      PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);  
      }
	    else
	    { /* check length of file*/
	      if(VFileStat.st_size <= Ps32SizeReadBuffer)
	      {/*read file */
		      Vs32Result = read(ViFile,Ppu8ReadBuffer,VFileStat.st_size);		
		      PDD_TRACE(PDD_FILE_READ_SIZE,&Vs32Result,sizeof(tS32));
		      if(Vs32Result<0)
		      {
		        Vs32Result=PDD_ERROR_FILE_READ;
          }
        }/*end if*/
		    else
		    {       
          tU8 Vu8Buffer[200];		
		      Vs32Result=PDD_ERROR_FILE_READ_BUFFER_TO_SMALL;   //normal case during write access, because it is possible to write less
          snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"PDD_FileReadDataStream() read buffer to small: stream:'%s' (%d,%d)",PtsDataStreamName,Ps32SizeReadBuffer,VFileStat.st_size);
	        PDD_TRACE(PDD_FILE_INFO,Vu8Buffer,strlen(Vu8Buffer)+1); 
        }/*end else*/
      }/*end else*/
	    /* close file*/ 
	    if (close(ViFile)<0) 
	    {
	      tU8 Vu8Buffer[80];		
		    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"PDD_FileReadDataStream() close(ViFile) fails error code:%d",errno); 
		    PDD_TRACE(PDD_INFO,Vu8Buffer,strlen(Vu8Buffer)+1);	
      }  	
    }/*end else handle ok*/	
  }/*end if get path*/
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_FileWriteDataStream()
*
* DESCRIPTION: write data stream into file 
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash
*      Ppu8WriteBuffer: the buffer for the read data
*      Pu32SizeWriteBuffer: the size of the read buffer
*      PbKindFile: Kind File (normal or backup)
*
* RETURNS: 
*      positive value: size of read data 
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 02 20
*****************************************************************************/
tS32 PDD_FileWriteDataStream(const char* PtsDataStreamName,tePddLocation PenLocation,void *Ppu8WriteBuffer,tS32 Ps32SizeWriteBuffer,tePddKindFile PeKindFile,tBool PbfSync)
{ 
  tS32     Vs32Result=PDD_OK;
  tBool    VbCreate=FALSE;
  intptr_t      ViFile;
  char     VstrFileName[PDD_FILE_MAX_PATH_NAME];  

  /* get path */	  
  Vs32Result=PDD_GetPath(PtsDataStreamName,PenLocation,&VstrFileName[0],PeKindFile);
  /*check error*/
  if(Vs32Result==PDD_OK)
  { /*check stream, if folder includes */
    Vs32Result=PDD_CheckCreateFolderStream(PtsDataStreamName,VstrFileName);	
	  /*open file name without create */
    ViFile = open(VstrFileName, O_WRONLY /*| O_CREAT*/ | O_TRUNC);
    if(ViFile < 0)
    {
      VbCreate=TRUE;
      ViFile = open(VstrFileName, O_WRONLY | O_CREAT | O_TRUNC ,PDD_ACCESS_RIGTHS);
    }
	  /* check file handle */
    if(ViFile < 0)
    { /*  error*/
      tU8 Vu8Buffer[200];		
      Vs32Result=PDD_ERROR_FILE_OPEN;  
	    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsDataStreamName);
	    PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);  
    }
	  else
	  {/*write and check lenght*/
      if((write(ViFile, Ppu8WriteBuffer, Ps32SizeWriteBuffer))!=Ps32SizeWriteBuffer)
	    {/* error*/
        tU8 Vu8Buffer[200];		
        Vs32Result=PDD_ERROR_FILE_WRITE; 
		    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsDataStreamName);
	      PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
	    }
	    else
	    { /* fsync => transfers ("flushes") all modified in-core data of 
		    (i.e., modified buffer cache pages for) the file referred to by the file descriptor
		    On success, these system calls return zero. On error, -1 is returned, and errno is set appropriately. */
        if(PbfSync==TRUE)  
        { //sync only for streams, which should be stored directly
	        if(fsync(ViFile)<0)
		      {
            tU8 Vu8Buffer[200];		
		        Vs32Result=PDD_ERROR_FILE_WRITE; 
		        snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsDataStreamName);
	          PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
		      }
        }
		    /*set access rights of file to group "eco_pdd*/
        if (VbCreate==TRUE)
		      Vs32Result=s32PDD_ChangeGroupAccess(PDD_KIND_RES_FILE,ViFile,PtsDataStreamName);
		    if(Vs32Result==PDD_OK)
		    {/* write, flush and set access rihts OK*/
	        Vs32Result=Ps32SizeWriteBuffer;
	        PDD_TRACE(PDD_FILE_WRITE_SIZE,&Ps32SizeWriteBuffer,sizeof(tS32));
		    }
      }
	    if (close(ViFile)<0) 
	    {
	      tU8 Vu8Buffer[80];		
		    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"PDD_FileWriteDataStream() close(ViFile) fails error code:%d",errno); 
		    PDD_TRACE(PDD_INFO,Vu8Buffer,strlen(Vu8Buffer)+1);	
	    }  	
	  }/*end else handle OK*/
  }/*end if get path*/
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_FileDeleteDataStream()
*
* DESCRIPTION: delte data stream file 
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash*     
*      PbKindFile: Kind File (normal or backup)
*
* RETURNS: 
*      positive value: size of read data 
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 02 20
*****************************************************************************/
tS32 PDD_FileDeleteDataStream(const char* PtsDataStreamName,tePddLocation PenLocation,tePddKindFile PeKindFile)
{
  tS32     Vs32Result=PDD_OK;
  char     VstrFileName[PDD_FILE_MAX_PATH_NAME];  

  /* get path */	  
  Vs32Result=PDD_GetPath(PtsDataStreamName,PenLocation,&VstrFileName[0],PeKindFile);
  /*check error*/
  if(Vs32Result==PDD_OK)
  { /*check stream, if folder includes */
    Vs32Result=remove(&VstrFileName[0]);
	  if(Vs32Result!=0)
	  {
	    tU8 Vu8Buffer[150];		
	    Vs32Result=errno;
	    if(Vs32Result!=ENOENT)
	    {// output only if file exist but error 
	      snprintf(Vu8Buffer,sizeof(Vu8Buffer),"remove() %s ",VstrFileName); 
	      PDD_TRACE(PDD_INFO,Vu8Buffer,strlen(Vu8Buffer)+1);	
	      snprintf(Vu8Buffer,sizeof(Vu8Buffer),"remove() fails errno:%d ",errno); 
	      PDD_TRACE(PDD_INFO,Vu8Buffer,strlen(Vu8Buffer)+1);	
	    }
	  }
  }
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_FileReadConfigLine()
*
* DESCRIPTION: read the config line from file pdd configuration file
*
* PARAMETERS:
*      PstrConfigStr: config string to read
*
* RETURNS: 
*      NULL config string not found 
*      !=NULL config string
*
* HISTORY:Created by Andrea Bueter 2016 03 10
*****************************************************************************/
char* PDD_FileReadConfigLine(const char* PstrConfigStr)
{
  FILE*   VpFile;
  size_t  VtsLen = 0;
  char*   VstrLine = NULL;
  ssize_t VtsRead;
  /*first try to open the config file 'PDD_CONFIG_FILE'*/
  VpFile=fopen(PDD_CONFIG_FILE,"r");
  if(VpFile==NULL)
  {/*if config file not exist open the 'PDD_CONFIG_FILE_DEFAULT'*/
    VpFile=fopen(PDD_CONFIG_FILE_DEFAULT,"r");     
  }
  if(VpFile!=NULL)
  { /*read line until found or end*/ 
    do
    { 
      VtsRead=getline(&VstrLine, &VtsLen, VpFile);
      if(VtsRead!=-1)
      { /*check for comment as first char*/
        if (VstrLine[0] != '#')
        {        
          if(strstr(VstrLine,PstrConfigStr)!=0)
            break;
        }
      }
    }while(VtsRead!=-1);      
    fclose(VpFile);
    if(VtsRead==-1)
    {
      free(VstrLine);
      VstrLine=NULL;
    }   
    else
    { /*On success, getline() return the number of characters read, including the delimiter character, but not including the terminating null byte ('\0').  
        This value can be used to handle embedded null bytes in the line read.*/
      VstrLine[VtsRead]=0;    
    }
  }
  else
  {   
    PDD_TRACE(PDD_FILE_OPENFILE_FAILS,0,0);    
  }
  return(VstrLine);
}
#ifdef PDD_CHECK_PATH
/******************************************************************************
* FUNCTION: PDD_CheckPathMount()
*
* DESCRIPTION: check path if mount
*
* PARAMETERS:
*      PpPath: path name
*
* RETURNS: 
*        NULL: path isn't mount
*      !=NULL: mount point
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
static char* PDD_CheckPathMount(char* PpPath)
{
  tU8*  Vu8PosMointPoint=NULL;
  tU8*  Vu8Pos=NULL;
  char  VstrPathName[PDD_FILE_MAX_PATH]; 
  int   ViLen=strlen(PpPath);  
  struct stat s1,s2;
  
  /*if path to long or not a root path*/
  if((ViLen+1>=(int)PDD_FILE_MAX_PATH)||(ViLen<1)||(PpPath[0]!='/'))
  {/*error path unvalid*/
    tU8 Vu8Buffer[200];		
    tS32 Vs32Result=PDD_ERROR_FILE_PATH_INVALID; 	
	  snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d path:'%s'",Vs32Result,Vs32Result,PpPath);
	  PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
  }
  else
  { /*copy path*/
    strcpy(VstrPathName,PpPath);
    // add trailing slash if not there.
    if(VstrPathName[ViLen-1]!='/')
    {
	    VstrPathName[ViLen++]='/';
	    VstrPathName[ViLen]=0;
    }
	  /*do until valid directory*/
	  tBool VbDirFound=FALSE;
	  Vu8Pos=strrchr(VstrPathName,'/');
	  do
	  { /* get status of dir*/
	    PDD_TRACE(PDD_FILE_TRACE_PATH,VstrPathName,strlen((tU8*)VstrPathName)); 
      if(stat(VstrPathName,&s1)>=0)
	    { /* check is this a directory*/
	      if(S_ISDIR(s1.st_mode))
	      { /* is a directory*/
		      VbDirFound=TRUE;		
		      ViLen=strlen(VstrPathName); /*save the last length*/
		    }		
      }
	    /* cut off '/'*/
	    if(Vu8Pos!=NULL)
	      Vu8Pos[0]=0;
	    /* get last '/'*/
	    Vu8Pos=strrchr(VstrPathName,'/');
	    if(Vu8Pos!=NULL)
	      Vu8Pos[1]=0;		
    }while((VbDirFound==FALSE)&&(Vu8Pos!=NULL)&&(strlen(VstrPathName)>0));
	  /*check if valid dir found*/
    if(VbDirFound==FALSE)
	  {/*error path unvalid*/
      tU8 Vu8Buffer[200];		
      tS32 Vs32Result=PDD_ERROR_FILE_PATH_INVALID; 
	    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d path:'%s'",Vs32Result,Vs32Result,PpPath);
	    PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
    }
	  else
	  { /* do until different file system*/
      do
	    { /* trace out path*/
        PDD_TRACE(PDD_FILE_TRACE_PATH,VstrPathName,strlen((tU8*)VstrPathName)); 
		    if(stat(VstrPathName,&s2)>=0)
	      {
	        if( s1.st_dev == s2.st_dev )
		      { /* is same filesystem , not the mountpoint*/
            /* save s1*/
            memmove(&s1.st_dev,&s2.st_dev,sizeof(s1.st_dev));
			      ViLen=strlen(VstrPathName); /*save the last position*/
            /* cut off '/'*/
			      if(Vu8Pos!=NULL)
	            Vu8Pos[0]=0;
			      /* get last '/'*/
	          Vu8Pos=strrchr(VstrPathName,'/');
			      if(Vu8Pos!=NULL)
	            Vu8Pos[1]=0;					 
          }
		      else
		      {/*is a different filesystem. mountpoint found*/
			      Vu8PosMointPoint=(tU8*)PpPath+(tU8)ViLen;                           
            strncpy(VstrPathName,PpPath,ViLen);
            VstrPathName[ViLen]=0;   
			      PDD_TRACE(PDD_FILE_FIND_MOUNT_POINT,VstrPathName,strlen(VstrPathName)); 
          } 
        }
		    else
		    {/*error path unvalid*/
          Vu8Pos=NULL;
          tU8 Vu8Buffer[200];		
		      tS32 Vs32Result=PDD_ERROR_FILE_PATH_INVALID; 
	        snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d path:'%s'",Vs32Result,Vs32Result,PpPath);
	        PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
        }
      }while((Vu8PosMointPoint==NULL)&&(Vu8Pos!=NULL)&&(strlen(VstrPathName)>0));
    }
  }
  return((char*)Vu8PosMointPoint);
}
#endif

/******************************************************************************
* FUNCTION: PDD_CheckCreatePath()
*
* DESCRIPTION: create the path, if doesn't exist
*
* PARAMETERS:
*      PpPath: path name
*
* RETURNS: 
*      positive value: success
*      negative value: error code  
*
* HISTORY:Created by Andrea Bueter 2013 02 18
*****************************************************************************/
static tS32 PDD_CheckCreatePath(const char* PpPath,const char* PpFolder,tBool PbSetMemoryEntryIfFalse)
{
  tS32  VS32Result=PDD_OK;
  char  VstrPathName[PDD_FILE_MAX_PATH];  
  DIR   *VpDir=NULL;
  tS32  Vs32IdDir;

  memset(VstrPathName,0x0,PDD_FILE_MAX_PATH);
  strncat(VstrPathName,PpPath,PDD_FILE_MAX_PATH);
  if(PpFolder!=NULL)
  {
    int ViLen=strlen(VstrPathName);
    strncat(VstrPathName,PpFolder,PDD_FILE_MAX_PATH-ViLen);
  }
  /* check if path exist */
  VpDir=opendir(VstrPathName);
  if(VpDir==NULL)
  {/*create path, if cannot open*/  
    Vs32IdDir=mkdir(VstrPathName, PDD_ACCESS_RIGTHS_DIR);
    if (Vs32IdDir == 0)
	  { /*set access rights of dir to group "eco_pdd*/
	    VS32Result=s32PDD_ChangeGroupAccess(PDD_KIND_RES_DIR,Vs32IdDir,VstrPathName);
      if(VS32Result==PDD_OK)
	      PDD_TRACE(PDD_FILE_CREATE_PATH_SUCCESS,VstrPathName,PDD_FILE_MAX_PATH); 
    }
	  else
	  {
	    VS32Result=PDD_ERROR_FILE_PATH_CREATE;
	    PDD_TRACE(PDD_FILE_CREATE_PATH_ERROR,VstrPathName,PDD_FILE_MAX_PATH); 
      if(PbSetMemoryEntryIfFalse==TRUE)    
      { /* add error memory entry; if the folder structure of the PDD cannot create for the first time*/
        tU8 Vu8Buffer[200];		
        snprintf(Vu8Buffer,sizeof(Vu8Buffer),"PDD_FILE: path '%s' could not be created",VstrPathName); 
        PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
      }
    }
  }
  else closedir(VpDir);
  return(VS32Result);
}
/******************************************************************************
* FUNCTION: PDD_GetPath()
*
* DESCRIPTION: get the whole path name (path with name)
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash
*      PtsPath: Patch name
*      PbKindFile: Kind File (normal or backup)
*
* RETURNS: 
*      positive value: success
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 02 20
*****************************************************************************/
static tS32 PDD_GetPath(const char* PtsDataStreamName,tePddLocation PenLocation, char* PtsPath, tePddKindFile PeKindFile)
{
  tS32  VS32Result=PDD_OK;
  int   ViLen;
  /*check size PtsDataStreamName */
  if (strlen(PtsDataStreamName) >= PDD_FILE_MAX_NAME)
  {
    tU8 Vu8Buffer[200];		
    VS32Result=PDD_ERROR_WRONG_PARAMETER;
    snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d path:'%s'",VS32Result,VS32Result,PtsDataStreamName);
	  PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
  }
  else
  { /*clear buffer*/
	  memset(PtsPath,0x0,PDD_FILE_MAX_PATH_NAME);
	  /*check for which location*/
    switch(PenLocation)
    {
      case PDD_LOCATION_FS:
      { 
        char* VStrConfigPath=strPDD_GetConfigPath(PDD_LOCATION_CONFIG_FS);
        if(VStrConfigPath!=NULL)
        {
          strncat(PtsPath, VStrConfigPath, PDD_FILE_MAX_PATH_NAME);
		      if(PeKindFile==PDD_KIND_FILE_NORMAL)
		      {
		        ViLen=strlen(PtsPath);
		        strncat(PtsPath, PDD_FILE_FOLDER_DATAPOOL,PDD_FILE_MAX_PATH_NAME-ViLen);
		        ViLen=strlen(PtsPath);
            strncat(PtsPath, PtsDataStreamName,PDD_FILE_MAX_PATH_NAME-ViLen);
		        /* general extension*/
		        ViLen=strlen(PtsPath);
            strncat(PtsPath, PDD_FILE_EXT, PDD_FILE_MAX_PATH_NAME-ViLen);
          }
		      else
		      { 
	          ViLen=strlen(PtsPath);
		        strncat(PtsPath, PDD_FILE_FOLDER_BACKUP_DATAPOOL, PDD_FILE_MAX_PATH_NAME-ViLen);
		        ViLen=strlen(PtsPath);
            strncat(PtsPath, PtsDataStreamName, PDD_FILE_MAX_PATH_NAME-ViLen);
		        /* backup extension */
		        ViLen=strlen(PtsPath);
		        strncat(PtsPath, PDD_FILE_EXT_BACKUP, PDD_FILE_MAX_PATH_NAME-ViLen);
          }
        }
        else
        {
          tU8 Vu8Buffer[200];		
	        VS32Result=PDD_ERROR_FILE_NO_FILE_PATH_DATAPOOL;
		      /*Path for file not defined */
          snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d path:'%s'",VS32Result,VS32Result,PtsDataStreamName);
	        PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
        }
      }break;
      case PDD_LOCATION_FS_SECURE:
	    { 
        char* VStrConfigPath=strPDD_GetConfigPath(PDD_LOCATION_CONFIG_FS_SECURE);
        if(VStrConfigPath!=NULL)
        {
		      strncat(PtsPath, VStrConfigPath, PDD_FILE_MAX_PATH_NAME);
		      if(PeKindFile==PDD_KIND_FILE_NORMAL)
		      {
		        ViLen=strlen(PtsPath);
		        strncat(PtsPath, PDD_FILE_FOLDER_DATAPOOL, PDD_FILE_MAX_PATH_NAME-ViLen);
		        ViLen=strlen(PtsPath);
            strncat(PtsPath, PtsDataStreamName,PDD_FILE_MAX_PATH_NAME-ViLen);
		        /* general extension*/
		        ViLen=strlen(PtsPath);
            strncat(PtsPath, PDD_FILE_EXT, PDD_FILE_MAX_PATH_NAME-ViLen);
          }
		      else
		      { 
		        ViLen=strlen(PtsPath);
		        strncat(PtsPath, PDD_FILE_FOLDER_BACKUP_DATAPOOL, PDD_FILE_MAX_PATH_NAME-ViLen);
		        ViLen=strlen(PtsPath);
            strncat(PtsPath, PtsDataStreamName, PDD_FILE_MAX_PATH_NAME-ViLen);
		        /* backup extension */
	          ViLen=strlen(PtsPath);
		        strncat(PtsPath, PDD_FILE_EXT_BACKUP, PDD_FILE_MAX_PATH_NAME-ViLen);
          }
        }
        else
        {
          tU8 Vu8Buffer[200];		
	        VS32Result=PDD_ERROR_FILE_NO_FILE_PATH_SECURE;
		      /*Path for file not defined */	       
          snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d path:'%s'",VS32Result,VS32Result,PtsDataStreamName);
	        PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
        }
      }break;
      case PDD_LOCATION_NOR_USER:
	    case PDD_LOCATION_NOR_KERNEL:
	    {
		    char* VStrConfigPath=strPDD_GetConfigPath(PDD_LOCATION_CONFIG_FS_SECURE);
        if(VStrConfigPath==NULL)
        {
            tU8 Vu8Buffer[200];		
		        VS32Result=PDD_ERROR_FILE_NO_FILE_PATH_DATAPOOL;
            snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d path:'%s'",VS32Result,VS32Result,PtsDataStreamName);
	          PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
        }
        else        
        {
          strncat(PtsPath, VStrConfigPath, PDD_FILE_MAX_PATH_NAME);		              
	        ViLen=strlen(PtsPath);
	        strncat(PtsPath, PDD_FILE_FOLDER_BACKUP_NOR, PDD_FILE_MAX_PATH-ViLen);
		      ViLen=strlen(PtsPath);
          strncat(PtsPath, PtsDataStreamName, PDD_FILE_MAX_PATH-ViLen);
	        if(PeKindFile==PDD_KIND_FILE_NORMAL)
		      { /* for nor only backup is in file system */ 
            tU8 Vu8Buffer[200];		
            VS32Result=PDD_ERROR_WRONG_PARAMETER;  
		        snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",VS32Result,VS32Result,PtsDataStreamName);
	          PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1); 
		      }
		      else
		      { /* backup extension */
		        ViLen=strlen(PtsPath);
		        strncat(PtsPath, PDD_FILE_EXT_BACKUP, PDD_FILE_MAX_PATH_NAME-ViLen);
          }		
        }
      }break;
	    case PDD_LOCATION_SCC: /* no break*/
	    default:
	    {
	      VS32Result=PDD_ERROR_WRONG_PARAMETER;
		    PDD_TRACE(PDD_ERROR,&VS32Result,sizeof(tS32));
      }break;
    }/*end switch*/
  }/*end if*/
  /* trace out path*/
  PDD_TRACE(PDD_FILE_TRACE_PATH,PtsPath,strlen((tU8*)PtsPath)); 
  return(VS32Result);
}
/******************************************************************************
* FUNCTION: PDD_CheckCreateFolderStream()
*
* DESCRIPTION: check if folder in stream and create
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PstrFileName: path with file name
*
* RETURNS: 
*      positive value: success
*      negative value: error code 
*
* HISTORY:Created by Andrea Bueter 2013 02 20
*****************************************************************************/
static tS32  PDD_CheckCreateFolderStream(const char* PtsDataStreamName,const char* PstrFileName)
{
  tS32   Vs32Result=PDD_OK;
  char*  Vc8Pos=strchr((char*)PtsDataStreamName,'/'); 
  if(Vc8Pos!=NULL) 
  {/*copy path and name*/
    char VstrPath[PDD_FILE_MAX_PATH_NAME];
	  Vc8Pos=NULL;
    memset(VstrPath,0x0,PDD_FILE_MAX_PATH_NAME);
    strncat(VstrPath,PstrFileName,PDD_FILE_MAX_PATH_NAME);
	  /* search last position for '/'*/
	  Vc8Pos=strrchr(VstrPath,'/');
	  if(Vc8Pos!=NULL)
	  { /* cut file name */
	    Vc8Pos[1]=0;
	    /* check path created*/
	    Vs32Result=PDD_CheckCreatePath(VstrPath,NULL,TRUE);
	  }
  }
  return(Vs32Result);
}

#ifdef __cplusplus
}
#endif
/******************************************************************************/
/* End of File pdd_access_file.c                                              */
/******************************************************************************/
