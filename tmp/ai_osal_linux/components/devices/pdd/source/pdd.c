/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        This file contains the persistent data device 
 * @addtogroup   PDD 
 * @{
 */ 

/* global function:
 * -- pdd_get_data_stream_size:
 *         PDD returns the size of the data stream.
 * -- pdd_read_data_stream:
 *         PDD reads a data stream from the location and checks the validation.
 * -- pdd_write_data_stream:
 *         The PDD writes a new data stream with the version number, 
 *         the magic number and a checksum into the location and in a backup file.
 * -- pdd_sync_scc_streams
 *         The PDD synchronize the datastream of the SCC with the backup datastream. 
 *         Therefore the datastream reads first the size and thene trhe data
 * -- pdd_get_element_from_stream
 *         Get elementvalue from stream 
 *  
 * local function:
 * -- PDD_LockForAccess:
 *         lock sem for different location
 * -- PDD_UnLockForAccess:
 *         unlock sem for different location
 * -- PDD_GetSizeHeader:
 *         get size of header for different location
 * -- PDD_ReadNormalDataStreamFromLocation:
 *         read normal datastream from different location
 * -- PDD_WriteNormalDataStreamToLocation:
 *         write normal datastream to different location
 * -- PDD_CheckValidationDataStream:
 *         check datastream for different location
 * -- PDD_CheckBufferEqual:
 *         check the buffer is equel with the CRC
 * -- PDD_GetHeader:
 *         get the header for different location
 */
 
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <errno.h>
#include <limits.h>   
#include "pdd_variant.h"
#ifdef PDD_TESTMANGER_ACTIVE  //define is set in pdd_variant.h
#include <string.h>
#include <stdlib.h>
#include "system_types.h"
#include "pdd.h"
#include "pdd_testmanager.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#else
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "system_types.h"
#include "system_definition.h"
#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_pdd.h"
#include "dgram_service.h"
#include "LFX2.h"
#include "pdd.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#ifdef PDD_SCC_POOL_EXIST
#include "DataPoolSCC.h" 
#endif
#include "pdd_config_scc.h"
#ifdef PDD_NOR_KERNEL_POOL_EXIST  
#include "DataPoolKernel.h"
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
#ifdef PDD_TESTMANGER_ACTIVE
#define M_PDD_IS_SYNC_BACKUP_POSSIBLE(PenLocation)\
  FALSE
#define M_PDD_READ_BACKUP_DATA_STREAM_FROM_LOCATION(PtsDataStreamName,PenLocation,VpBuffer,Vs32Size)\
  PDD_OK
#define M_PDD_WRITE_BACKUP_DATASTREAM_TO_LOCATION(PtsDataStreamName,PenLocation,VpBuffer,Vs32Size,PbSync)\
  PDD_OK
#define	M_PDD_INIT_TRACE_LEVEL()
#else
#define M_PDD_IS_SYNC_BACKUP_POSSIBLE(PenLocation)\
  PDD_IsSyncBackupPossible(PenLocation) 
#define M_PDD_READ_BACKUP_DATA_STREAM_FROM_LOCATION(PtsDataStreamName,PenLocation,VpBuffer,Vs32Size)\
  PDD_ReadBackupDataStreamFromLocation(PtsDataStreamName,PenLocation,VpBuffer,Vs32Size)
#define M_PDD_WRITE_BACKUP_DATASTREAM_TO_LOCATION(PtsDataStreamName,PenLocation,VpBuffer,Vs32Size,PbSync)\
  PDD_WriteBackupDataStreamToLocation(PtsDataStreamName,PenLocation,VpBuffer,Vs32Size,PbSync)
#define	M_PDD_INIT_TRACE_LEVEL() vPDD_InitTraceLevel()
#endif


/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/


/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/


/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static void    PDD_LockForAccess(tePddLocation);
static void    PDD_UnLockForAccess(tePddLocation);
static tS32    PDD_GetSizeHeader(tePddLocation);
static tS32    PDD_ReadNormalDataStreamFromLocation(const char*,tePddLocation ,const tU8* ,tS32 ,tBool );
static tS32    PDD_WriteNormalDataStreamToLocation(const char*,tePddLocation ,const tU8* ,tS32, tBool );
static tBool   PDD_CheckValidationDataStream(tePddLocation ,const tU8* ,tS32 ,tU32 );
static tBool   PDD_CheckBufferEqual(tePddLocation ,const void* ,const void* );
static void    PDD_GetHeader(tePddLocation,tU8*,tS32,tU32,tU8*);
#ifndef PDD_TESTMANGER_ACTIVE
static tBool   PDD_IsSyncBackupPossible(tePddLocation PenLocation);
static tS32    PDD_ReadBackupDataStreamFromLocation(const char*,tePddLocation ,const tU8* ,tS32 );
static tS32    PDD_WriteBackupDataStreamToLocation(const char*,tePddLocation ,const tU8* ,tS32, tBool );
#endif
static const char* PDD_DeleteFolderFromStream(const char*);
static tS32    PDD_ReadDatastreamGeneral(const char* ,tePddLocation ,tU8 *,tS32 ,tU32 , tU8* , tBool );
/******************************************************************************
* FUNCTION: pdd_get_data_stream_size()
*
* DESCRIPTION: PDD returns the size of the data stream. 
*              (The client needs the size of the data 
*              stream to allocate a read buffer.)
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash
*
* RETURNS: 
*      positive value: size of data stream without header
*      negative value: error code 
*
* HISTORY:Created  2013 02 18
*****************************************************************************/
tS32 pdd_get_data_stream_size(const char* PtsDataStreamName,tePddLocation PenLocation)
{
  tS32  Vs32Result=PDD_OK;
  tS32  Vs32SizeHeader=PDD_GetSizeHeader(PenLocation);	
  

  if(!((PtsDataStreamName!=NULL)&&(PenLocation<PDD_LOCATION_LAST)))
  { 
    Vs32Result=PDD_ERROR_WRONG_PARAMETER;
  }
  else
  {
	  M_PDD_INIT_TRACE_LEVEL();
    PDD_TRACE(PDD_START_DATA_STREAM_SIZE,PtsDataStreamName,strlen((tU8*)PtsDataStreamName)+1); 
    PDD_LockForAccess(PenLocation);
    Vs32Result=PDD_IsInit(PenLocation,PtsDataStreamName);
    /* check if init*/
    if(Vs32Result==PDD_OK)
    {/*check parameter*/   	 
      switch(PenLocation)
	    {
	      case PDD_LOCATION_FS: /* no break*/
	      case PDD_LOCATION_FS_SECURE:
	      { /*get max size (file or backup file) */
           Vs32Result=M_PDD_FILE_GET_DATASTREAM_SIZE(PtsDataStreamName,PenLocation);
	      }break;
	      case PDD_LOCATION_NOR_USER:
	      {/*call function get size NOR*/
		      PtsDataStreamName=PDD_DeleteFolderFromStream(PtsDataStreamName);
		      Vs32Result=PDD_NorUserAccessGetDataStreamSize(PtsDataStreamName);
		      if(Vs32Result<=0)
		      {/*check lenght backup*/
            tePddLocation VenLocation=PDD_LOCATION_FS_SECURE;
            PDD_LockForAccess(VenLocation);  //PDD_SEM_ACCESS_FILE_SYSTEM for both file system FS and FS_SECURE
	          Vs32Result=PDD_IsInit(VenLocation,PtsDataStreamName);           
            if(Vs32Result==PDD_OK)
            {
              Vs32Result=M_PDD_FILE_GET_DATASTREAM_SIZE_BACKUP(PtsDataStreamName,PDD_LOCATION_NOR_USER);			                             
            }
			      PDD_UnLockForAccess(VenLocation);
          }
	      }break;
	      case PDD_LOCATION_NOR_KERNEL:
        {/*call function get size NOR*/		
	        tePddLocation VenLocation=PDD_LOCATION_FS_SECURE;      
	        tS32 Vs32LengthNorKernel=M_PDD_NORKERNEL_ACCESS_GET_DATASTREAM_SIZE();
		      tS32 Vs32LengthFile=0;
	        /*check lenght backup*/		  
	        PDD_LockForAccess(VenLocation); //PDD_SEM_ACCESS_FILE_SYSTEM for both file system FS and FS_SECURE
	        Vs32Result=PDD_IsInit(VenLocation,PtsDataStreamName);
	        if(Vs32Result==PDD_OK)
	        {	
            PtsDataStreamName=PDD_DeleteFolderFromStream(PtsDataStreamName);
            Vs32LengthFile=M_PDD_FILE_GET_DATASTREAM_SIZE_BACKUP(PtsDataStreamName,PDD_LOCATION_NOR_KERNEL);			                         
          }		 
		      PDD_UnLockForAccess(VenLocation);
		      Vs32Result=Vs32LengthNorKernel;
		      /*return max size*/
		      if(Vs32LengthFile>Vs32LengthNorKernel)
		      {
		        Vs32Result=Vs32LengthFile;
          }
        }break;
	      case PDD_LOCATION_SCC:
	      {/* call function get size V850 pool from configuration => backup should have the same lenght(if exist)!!*/
		      Vs32Result=M_PDD_SCCACCESS_GET_DATASREAM_SIZE(PtsDataStreamName);		 
        }break;
	      default:
	      {
	        Vs32Result=PDD_ERROR_WRONG_PARAMETER;  
	      }break;
      }/*end switch*/	 
    }
	  PDD_UnLockForAccess(PenLocation);
  }  
  if(Vs32Result>=0)
  { /*size without header*/
	  if(Vs32Result >= Vs32SizeHeader)
    {
	    Vs32Result=Vs32Result-Vs32SizeHeader;	
    }
	  else Vs32Result=0;
  }
  PDD_TRACE(PDD_EXIT_DATA_STREAM_SIZE,&Vs32Result,sizeof(tS32)); 
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: pdd_read_data_stream()
*
* DESCRIPTION: PDD reads a data stream from the location 
*              and checks the validation. 
*              In case of invalid data PDD reads the data 
*              from backup file and checks the validation.
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash
*      Ppu8ReadBuffer: the buffer for the read data
*      Pu32SizeReadBuffer: the size of the read buffer
*      Pu32Version: version of the datastream
*
* RETURNS: 
*      positive value: the bytes read
*      negative value: error code 
*
* HISTORY:Created  2013 02 18
*****************************************************************************/
tS32 pdd_read_data_stream(const char* PtsDataStreamName,tePddLocation PenLocation,
						  tU8 *Ppu8ReadBuffer,tS32 Ps32SizeReadBuffer,tU32 Pu32Version)
{
  tS32  Vs32Result=PDD_OK;
  tU8   Vu8Info;

  M_PDD_INIT_TRACE_LEVEL();
  if(PtsDataStreamName!=NULL)
    PDD_TRACE(PDD_START_READ_DATA_STREAM,PtsDataStreamName,strlen((tU8*)PtsDataStreamName)+1);
  Vs32Result=PDD_ReadDatastreamGeneral(PtsDataStreamName,PenLocation,Ppu8ReadBuffer,Ps32SizeReadBuffer,Pu32Version,&Vu8Info,FALSE);
  PDD_TRACE(PDD_EXIT_READ_DATA_STREAM,&Vs32Result,sizeof(tS32)); 
  return(Vs32Result);
}

tS32 pdd_read_datastream_early_from_nor(const char* PtsDataStreamName,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version,tU8* Pu8Info)
{
  tS32           Vs32Result=PDD_OK;
  
  M_PDD_INIT_TRACE_LEVEL();
  if(PtsDataStreamName!=NULL)
    PDD_TRACE(PDD_START_READ_DATA_STREAM_EARLY,PtsDataStreamName,strlen((tU8*)PtsDataStreamName)+1);  	
  tePddLocation  VenLocation=PDD_LOCATION_NOR_USER;
  Vs32Result=PDD_ReadDatastreamGeneral(PtsDataStreamName,VenLocation,Ppu8ReadBuffer,Ps32SizeReadBuffer,Pu32Version,Pu8Info,TRUE);
  PDD_TRACE(PDD_EXIT_READ_DATA_STREAM_EARLY,&Vs32Result,sizeof(tS32));  	
  return(Vs32Result);
}

tS32 pdd_read_datastream(const char* PtsDataStreamName,tePddLocation PenLocation,tU8 *Ppu8ReadBuffer,tS32 Ps32SizeReadBuffer,tU32 Pu32Version,tU8* Pu8Info)
{
  tS32           Vs32Result=PDD_OK;

  M_PDD_INIT_TRACE_LEVEL();
  if(PtsDataStreamName!=NULL)
    PDD_TRACE(PDD_START_READ_DATA_STREAM,PtsDataStreamName,strlen((tU8*)PtsDataStreamName)+1);
  Vs32Result=PDD_ReadDatastreamGeneral(PtsDataStreamName,PenLocation,Ppu8ReadBuffer,Ps32SizeReadBuffer,Pu32Version,Pu8Info,FALSE);
  PDD_TRACE(PDD_EXIT_READ_DATA_STREAM,&Vs32Result,sizeof(tS32)); 
  return(Vs32Result);
}

static tS32 PDD_ReadDatastreamGeneral(const char* PtsDataStreamName,tePddLocation PenLocation,
									                   tU8 *Ppu8ReadBuffer,tS32 Ps32SizeReadBuffer,tU32 Pu32Version,
								                     tU8* Pu8Info, tBool VbNorFastEarlyAccess)
{
  tS32  Vs32Result=PDD_OK;
  /*check parameter*/
  if(!((PtsDataStreamName!=NULL)&&(Ppu8ReadBuffer!=NULL)&&(PenLocation<PDD_LOCATION_LAST)&&(Ps32SizeReadBuffer>0)&&(Pu8Info!=NULL)))
  { 
    Vs32Result=PDD_ERROR_WRONG_PARAMETER;
  }
  else
  { /* trace out start and name */     	
    PDD_LockForAccess(PenLocation);
    Vs32Result=PDD_IsInit(PenLocation,PtsDataStreamName);
    /* check if init*/
    if(Vs32Result==PDD_OK)
    {       
      tBool VbValidationNormal=FALSE;
      tBool VbValidationBackup=FALSE;
	    tBool VbSyncBackupAccess=M_PDD_IS_SYNC_BACKUP_POSSIBLE(PenLocation); /*=> TRUE sync backup possible */
      tS32  Vs32ReadLengthFileNormal=0;
      tS32  Vs32ReadLengthFileBackup=0;  
	    tS32  Vs32SizeHeader=PDD_GetSizeHeader(PenLocation);	
	    tS32  Vs32SizeFileBuffer=Vs32SizeHeader+Ps32SizeReadBuffer;
	    tU8*  VpBufferFileNormal=(tU8*)malloc(Vs32SizeFileBuffer);
      tU8*  VpBufferFileBackup=(tU8*)malloc(Vs32SizeFileBuffer);;
	  
	    /*check buffer*/
	    if((VpBufferFileNormal==NULL)||(VpBufferFileBackup==NULL))
	    {
	      Vs32Result=PDD_ERROR_NO_MEMORY;
	    }
	    else 
	    { /*********************read and check validation of datastream ***************************************/
		    /*read normal and check validation */
	      Vs32ReadLengthFileNormal=PDD_ReadNormalDataStreamFromLocation(PtsDataStreamName,PenLocation,VpBufferFileNormal,Vs32SizeFileBuffer,VbNorFastEarlyAccess);
		    if(Vs32ReadLengthFileNormal > Vs32SizeHeader)
		    { /*check file check backup file*/
		      VbValidationNormal=PDD_CheckValidationDataStream(PenLocation,(void*)VpBufferFileNormal,Vs32ReadLengthFileNormal,Pu32Version);
	  	  }
		    /*if sync backup not possible and VbValidationNormal=true => no backup access*/
		    if(!((VbSyncBackupAccess==FALSE)&&(VbValidationNormal==TRUE)))		
		    { /*read backup file and check validation*/		
		      Vs32ReadLengthFileBackup=M_PDD_READ_BACKUP_DATA_STREAM_FROM_LOCATION(PtsDataStreamName,PenLocation,VpBufferFileBackup,Vs32SizeFileBuffer);
		      if(Vs32ReadLengthFileBackup > Vs32SizeHeader)
		      { /*check file backup file*/
		        VbValidationBackup=PDD_CheckValidationDataStream(PenLocation,(void*)VpBufferFileBackup,Vs32ReadLengthFileBackup,Pu32Version);
		      }
		      /*********************check state of validation ***************************************/
		      /*if normal file unvalid, but backup file valid => save normal file*/
		      if((VbValidationNormal==FALSE)&&(VbValidationBackup==TRUE))  
		      {/* save normal data stream */
		        PDD_WriteNormalDataStreamToLocation(PtsDataStreamName,PenLocation,VpBufferFileBackup,Vs32ReadLengthFileBackup,TRUE);
		      }
		      /*if both valid, but different */
		      if((VbValidationNormal==TRUE)&&(VbValidationBackup==TRUE))
	        {/*compare both CRC- Checksum */
		        if(PDD_CheckBufferEqual(PenLocation,VpBufferFileBackup,VpBufferFileNormal)==FALSE)		  
		        {/*save Backup file, because backup file is not equal to normal file*/
              M_PDD_WRITE_BACKUP_DATASTREAM_TO_LOCATION(PtsDataStreamName,PenLocation,VpBufferFileNormal,Vs32ReadLengthFileNormal,TRUE);
            }
          }
		      /*if normal file valid, but backup file unvalid => save backup file*/
		      if((VbValidationNormal==TRUE)&&(VbValidationBackup==FALSE))
		      {/* save backup file */
		        M_PDD_WRITE_BACKUP_DATASTREAM_TO_LOCATION(PtsDataStreamName,PenLocation,VpBufferFileNormal,Vs32ReadLengthFileNormal,TRUE);
          }
        }
	      /*check validation of both file*/
	      if((VbValidationNormal==FALSE)&&(VbValidationBackup==FALSE))
	      {
	        Vs32Result=PDD_ERROR_READ_NO_VALID_DATA_STREAM;
 	      }
	      else
	      { /*********************get result ***************************************/
	        tU8*  VpBufferFile;
		      tS32  Vs32ReadLengthFile;
		      /*check which file valid*/
          if(VbValidationNormal==TRUE)
		      { /*set pointer buffer and lenght for normal file*/
            VpBufferFile=VpBufferFileNormal;
            Vs32ReadLengthFile=Vs32ReadLengthFileNormal;
			      *Pu8Info=PDD_READ_INFO_NORMAL_FILE;
          }
		      else
		      { /*set pointer buffer and lenght for backup file*/
            VpBufferFile=VpBufferFileBackup;
            Vs32ReadLengthFile=Vs32ReadLengthFileBackup;
			      *Pu8Info=PDD_READ_INFO_BACKUP_FILE;
			      /* add error memory entry*/
			      {
			        tU8 Vu8Buffer[200];		
		          snprintf(Vu8Buffer,sizeof(Vu8Buffer),"no valid datastream for %s => read valid backup datastream",PtsDataStreamName); 
		          PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
            }
          }
		      /*check lenght of file*/
	        Vs32Result=Vs32ReadLengthFile-Vs32SizeHeader;
		      if((Vs32Result>=0)&&(Vs32Result<=Ps32SizeReadBuffer))
		      { /*check size */
		        if(Vs32Result<=Ps32SizeReadBuffer)
		        { /*copy data into buffer*/			  
		          memmove(Ppu8ReadBuffer,VpBufferFile+Vs32SizeHeader,Vs32Result);	
            }
			      else
			      {
			        Vs32Result=PDD_ERROR_READ_BUFFER_TO_SMALL;
            }
          }
		      else
		      {
		        Vs32Result=PDD_ERROR_READ_NO_VALID_DATA_STREAM;
          }	      	
        }
      }
	    free(VpBufferFileNormal);
	    free(VpBufferFileBackup);
    }
	  PDD_UnLockForAccess(PenLocation);	   
  }
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: pdd_write_data_stream()
*
* DESCRIPTION: The PDD writes a new data stream with the version number, 
*              the magic number and a checksum into the location and in a backup file. 
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash
*      Ppu8WriteBuffer: the buffer for the write data
*      Pu32SizeBytestoWrite: the size of the bytes to write
*      Pu32Version: version of the datastream
*
* RETURNS: 
*      positive value: the bytes written   
*      negative value: error code 
*
* HISTORY:Created  2013 02 18
*****************************************************************************/
tS32 pdd_write_data_stream(const char* PtsDataStreamName,tePddLocation PenLocation,
                           tU8 *Ppu8WriteBuffer, tS32 Ps32SizeBytestoWrite,tU32 Pu32Version)
{
  return(pdd_write_datastream(PtsDataStreamName,PenLocation,Ppu8WriteBuffer,Ps32SizeBytestoWrite,Pu32Version,TRUE));
}

tS32 pdd_write_datastream(const char* PtsDataStreamName,tePddLocation PenLocation,
						              tU8 *Ppu8WriteBuffer, tS32 Ps32SizeBytestoWrite,tU32 Pu32Version,tBool PbSync)
{
  tS32  Vs32Result=PDD_OK;
  /*check parameter*/
  if(!((PtsDataStreamName!=NULL)&&(Ppu8WriteBuffer!=NULL)&&(PenLocation<PDD_LOCATION_LAST)&&(Ps32SizeBytestoWrite>=0)))
  { 
    Vs32Result=PDD_ERROR_WRONG_PARAMETER;
  }
  else
  {
    M_PDD_INIT_TRACE_LEVEL();
    PDD_LockForAccess(PenLocation);  
    Vs32Result=PDD_IsInit(PenLocation,PtsDataStreamName);
    /* check if init*/
    if(Vs32Result==PDD_OK)
    {  
      tS32  Vs32ReadLengthFile=0;    
      tBool VbDataIdentical=FALSE;
	    tBool VbSyncBackupAccess=M_PDD_IS_SYNC_BACKUP_POSSIBLE(PenLocation); /*=> TRUE sync backup possible */
	    tS32  Vs32Length=0;
	    tS32  Vs32LengthBackup=0;  	
	    tS32  Vs32SizeHeader=PDD_GetSizeHeader(PenLocation);
	    tS32  Vs32SizeFileBuffer=Vs32SizeHeader+Ps32SizeBytestoWrite;
	    tU8*  VpBufferFileRead=(tU8*)malloc(Vs32SizeFileBuffer);
      tU8*  VpBufferFileWrite=(tU8*)malloc(Vs32SizeFileBuffer);
	    /* trace */
      PDD_TRACE(PDD_START_WRITE_DATA_STREAM,PtsDataStreamName,strlen((tU8*)PtsDataStreamName)+1);  	 
	    /*check buffer*/
	    if((VpBufferFileRead==NULL)||(VpBufferFileWrite==NULL))
	    {
	      Vs32Result=PDD_ERROR_NO_MEMORY;
	    }
	    else 
	    {    
        tU8 Vu8BufHeader[Vs32SizeHeader];	
	      /*get header*/
		    PDD_GetHeader(PenLocation,Ppu8WriteBuffer,Ps32SizeBytestoWrite,Pu32Version,&Vu8BufHeader[0]);
	      /*copy header*/
        memmove(VpBufferFileWrite,&Vu8BufHeader[0],Vs32SizeHeader);	
	      /*copy data stream*/
	      memmove(VpBufferFileWrite+Vs32SizeHeader,Ppu8WriteBuffer,Ps32SizeBytestoWrite);			  
		    /*read normal file for check new data*/
		    Vs32ReadLengthFile=PDD_ReadNormalDataStreamFromLocation(PtsDataStreamName,PenLocation,VpBufferFileRead,Vs32SizeFileBuffer,FALSE);
	      if(Vs32ReadLengthFile >= Vs32SizeHeader)
		    { /*check if read normal file correct*/
		      if(PDD_CheckValidationDataStream(PenLocation,(void*)VpBufferFileRead,Vs32ReadLengthFile,Pu32Version)==TRUE)
		      { /*compare data stream file, without header information*/		 
		        if(memcmp(VpBufferFileWrite+Vs32SizeHeader,VpBufferFileRead+Vs32SizeHeader,Vs32SizeFileBuffer-Vs32SizeHeader)==0)
		        { /*data are identical -> don't need to copy*/
              VbDataIdentical = TRUE;
			        PDD_TRACE(PDD_SAVE_DATA_INDENTICAL,0,0);
		        }
          }
        }
		    if(VbDataIdentical==FALSE)
		    { /* data new */
		      /* => write to file system*/		 
		      Vs32Length=PDD_WriteNormalDataStreamToLocation(PtsDataStreamName,PenLocation,VpBufferFileWrite,Vs32SizeFileBuffer,PbSync);	
		      /*if sync possible*/
		      if(VbSyncBackupAccess==TRUE)
		      { /* => write backup  */
		        Vs32LengthBackup=M_PDD_WRITE_BACKUP_DATASTREAM_TO_LOCATION(PtsDataStreamName,PenLocation,VpBufferFileWrite,Vs32SizeFileBuffer,PbSync);			  
          }
		      else
		      {/*no error for backup */
		        Vs32LengthBackup=Vs32Length;
          }
	        /*check write results*/
	        if((Vs32Length < PDD_OK)&&(Vs32LengthBackup < PDD_OK))
	        { /*no write success*/         
            // set error value
	          Vs32Result=PDD_ERROR_WRITE_NO_DATA_STREAM;
            #ifndef PDD_TESTMANGER_ACTIVE
            { /* set error memory entry for file system only if mount time of partition is past  */       
              struct timespec VTimer;	     
	            clock_gettime(CLOCK_MONOTONIC, &VTimer);             
	            if(   (PenLocation==PDD_LOCATION_NOR_USER)||(PenLocation==PDD_LOCATION_NOR_KERNEL)||(PenLocation==PDD_LOCATION_SCC)
                 ||((PenLocation==PDD_LOCATION_FS_SECURE)&&(VTimer.tv_sec > PDD_FILE_PATH_SECURE_MAX_MOUNT_TIME))
                 ||((PenLocation==PDD_LOCATION_FS)&&(VTimer.tv_sec > PDD_FILE_PATH_MAX_MOUNT_TIME)))
			        {
			          tU8 Vu8Buffer[200];		
                snprintf(Vu8Buffer,sizeof(Vu8Buffer),"datastream for %s can not be saved,time:%d s",PtsDataStreamName,VTimer.tv_sec); 
		            PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
              }
            }
            #endif
	        }
	        else if((Vs32Length >= PDD_OK)&&(Vs32LengthBackup < PDD_OK))
	        { /*no backup file  write success*/
	          Vs32Result=PDD_ERROR_WRITE_NO_DATA_STREAM_BACKUP;
	        }
	        else if((Vs32Length < PDD_OK)&&(Vs32LengthBackup >= PDD_OK))
	        {/*no normal file write sucess */
	          Vs32Result=PDD_ERROR_WRITE_NO_DATA_STREAM_NORMAL;
	        }
	        else
	        { /*no error*/
	          Vs32Result=Vs32Length-Vs32SizeHeader;
	        }
	      }/*end if*/		  
	    }/*end else no bufferelse*/
	    /*set buffer to free*/ 
	    free(VpBufferFileRead);
	    free(VpBufferFileWrite);
    }/*end else wrong parameter*/
	  PDD_UnLockForAccess(PenLocation);	 
  }/*end else not init*/ 
  PDD_TRACE(PDD_EXIT_WRITE_DATA_STREAM,&Vs32Result,sizeof(tS32)); 
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: pdd_delete_data_stream()
*
* DESCRIPTION: delete the datastream
*
* PARAMETERS:
*      PtsDataStreamName: name of the data stream
*      PenLocation: location of the data stream in flash
*
* RETURNS: PDD_OK or error code
*
* HISTORY:Created  2015 01 14
*****************************************************************************/
tS32 pdd_delete_data_stream(const char* PtsDataStreamName,tePddLocation PenLocation)
{
  tS32  Vs32Result=PDD_OK;
  /*check parameter*/
  if(!((PtsDataStreamName!=NULL)&&(PenLocation<PDD_LOCATION_LAST)))
  { 
    Vs32Result=PDD_ERROR_WRONG_PARAMETER;
  }
  else
  {
    M_PDD_INIT_TRACE_LEVEL();
	  PDD_TRACE(PDD_START_DELETE_DATA_STREAM,PtsDataStreamName,strlen((tU8*)PtsDataStreamName)+1);  	 
    if((PenLocation==PDD_LOCATION_FS)||(PenLocation==PDD_LOCATION_FS_SECURE)) 
	  {
	    PDD_LockForAccess(PenLocation);  
	    Vs32Result=PDD_IsInit(PenLocation,PtsDataStreamName);
      /* check if init*/
      if(Vs32Result==PDD_OK)
      { /* delete backup file and normal*/ 
 	      tS32 Vs32Result1=M_PDD_FILE_DELETE_DATASTREAM(PtsDataStreamName,PenLocation,PDD_KIND_FILE_NORMAL);
		    tS32 Vs32Result2=M_PDD_FILE_DELETE_DATASTREAM(PtsDataStreamName,PenLocation,PDD_KIND_FILE_BACKUP);
		    if((Vs32Result1!=PDD_OK)||(Vs32Result2!=PDD_OK))
		    {
		      Vs32Result=PDD_ERROR_DELETE_DATA_STREAM;         
		      if((Vs32Result1==ENOENT)&&(Vs32Result2==ENOENT))
		      {
		        Vs32Result=PDD_ERROR_DATA_STREAM_DOESNOTEXIST;
		      }
		      else if(  ((Vs32Result1==PDD_OK)&&(Vs32Result2==ENOENT))
			            ||((Vs32Result1==ENOENT)&&(Vs32Result2==PDD_OK)))
		      {/*only one file remove => file exist*/			
            Vs32Result=PDD_OK;
          }
        }
      }
	    PDD_UnLockForAccess(PenLocation);	   
    }
	  else
	  {/*for all other locations(PDD_LOCATION_NOR_USER,PDD_LOCATION_NOR_KERNEL,PDD_LOCATION_SCC) delete function not supported*/
      tU8 Vu8Buffer[200];		
	    Vs32Result=PDD_ERROR_NOT_SUPPORTED;
      snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"0x%04x: %d stream:'%s'",Vs32Result,Vs32Result,PtsDataStreamName);
	    PDD_TRACE(PDD_ERROR_STR,Vu8Buffer,strlen(Vu8Buffer)+1);   
    }
  } 
  PDD_TRACE(PDD_EXIT_DELETE_DATA_STREAM,&Vs32Result,sizeof(tS32)); 
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: pdd_sync_scc_streams()
*
* DESCRIPTION: The PDD synchronize the datastream of the SCC with the backup datastream. 
*              Therefore the datastream reads first the size and then the data
*
* PARAMETERS:

*
* RETURNS: 
*
* HISTORY:Created  2014 02 18
*****************************************************************************/
void pdd_sync_scc_streams(void)
{
  tU8*  VpBuffer;
  tU8   Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  #ifdef PDD_SCC_POOL_EXIST
  tS32                    Vs32Lenght;
  tS32                    Vs32TableInfoCount;
  const tsPddSccConfig*   VtspSccConfig;

  PDD_TRACE(PDD_START_SYNC_SCC,NULL,0); 
  /*for each stream*/
  for(Vs32TableInfoCount=0;Vs32TableInfoCount < PDD_SCC_TABLE_INFO_END;Vs32TableInfoCount++)
  {
	  VtspSccConfig=&vrPddSccConfig[Vs32TableInfoCount];
	  /*get size*/
	  Vs32Lenght=pdd_get_data_stream_size(VtspSccConfig->u8Filename,PDD_LOCATION_SCC);
	  if(Vs32Lenght>0)
	  {/*read data*/
      VpBuffer=(tU8*)malloc(Vs32Lenght);
	    if(VpBuffer!=NULL)
	    {
	      Vs32Lenght=pdd_read_datastream(VtspSccConfig->u8Filename,PDD_LOCATION_SCC,VpBuffer,Vs32Lenght,VtspSccConfig->u32Version,&Vu8Info);
	      if(Vs32Lenght>0)
	      {
	        if(Vu8Info==PDD_READ_INFO_BACKUP_FILE)
	        {/*output trace*/
		        tU8    Vu8Buffer[100];		
            memset(Vu8Buffer,0,sizeof(Vu8Buffer));
			      // set error memory entry
			      snprintf(Vu8Buffer,sizeof(Vu8Buffer),"pdd_sync_scc_streams: %s is synchronized with backup",VtspSccConfig->u8Filename);	 
			      PDD_TRACE(PDD_INFO,&Vu8Buffer[0],strlen(&Vu8Buffer[0])+1);
			      PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
          }
        }
		    free(VpBuffer);
      }
	    else
	    {
	      PDD_FATAL_M_ASSERT_ALWAYS();
      }
    }
  }
  PDD_TRACE(PDD_EXIT_SYNC_SCC,NULL,0); 
  #endif
}

/******************************************************************************
* FUNCTION: pdd_sync_nor_user_streams()
*
* DESCRIPTION: The PDD synchronize the datastream of the nor user with 
*              the backup datastream. 
*              Therefore the datastream reads first the size and then the data
*
* PARAMETERS:

*
* RETURNS: 
*
* HISTORY:Created  2014 09 25
*****************************************************************************/
void pdd_sync_nor_user_streams(void)
{
  #ifndef PDD_TESTMANGER_ACTIVE
  tU8*  VpBuffer;
  tU8   Vu8Info=PDD_READ_INFO_NORMAL_FILE;

  tS32                        Vs32Lenght;
  tS32                        Vs32TableInfoCount;
  const tsPddNorUserConfig*   VtspNorUserConfig;

  PDD_TRACE(PDD_START_SYNC_NOR_USER,NULL,0); 
  /*for each stream*/
  for(Vs32TableInfoCount=0;Vs32TableInfoCount < PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM;Vs32TableInfoCount++)
  {
	  VtspNorUserConfig=&vrPddNorUserConfig[Vs32TableInfoCount];
	  /*only for location NOR user*/
	  if(VtspNorUserConfig->eLocation==PDD_LOCATION_NOR_USER)
	  { /*get size*/
	    Vs32Lenght=pdd_get_data_stream_size(VtspNorUserConfig->u8DataStreamName,PDD_LOCATION_NOR_USER);
	    if(Vs32Lenght>0)
	    {/*read data*/
        VpBuffer=(tU8*)malloc(Vs32Lenght);
	      if(VpBuffer!=NULL)
	      {
	        Vs32Lenght=pdd_read_datastream(VtspNorUserConfig->u8DataStreamName,PDD_LOCATION_NOR_USER,VpBuffer,Vs32Lenght,VtspNorUserConfig->u32Version,&Vu8Info);
	        if(Vs32Lenght>0)
	        {
	          if(Vu8Info==PDD_READ_INFO_BACKUP_FILE)
	          {/*output trace*/
		          tU8    Vu8Buffer[100];		
              memset(Vu8Buffer,0,sizeof(Vu8Buffer));
			        // set error memory entry
			        snprintf(Vu8Buffer,sizeof(Vu8Buffer),"pdd_sync_nor_user_streams: %s is synchronized with backup",VtspNorUserConfig->u8DataStreamName);	 
			        PDD_TRACE(PDD_INFO,&Vu8Buffer[0],strlen(&Vu8Buffer[0])+1);
			        PDD_SET_ERROR_ENTRY(&Vu8Buffer[0]);
            }
          }
		      free(VpBuffer);
        }
	      else
	      {
	        PDD_FATAL_M_ASSERT_ALWAYS();
        }
      }
    }
  }
  PDD_TRACE(PDD_EXIT_SYNC_NOR_USER,NULL,0); 
  #endif
}

/******************************************************************************
* FUNCTION: tS32 pdd_helper_get_element_from_stream()
*
* DESCRIPTION: helper function for get elementvalue from stream 
*
* PARAMETERS:
*    PstrElementName: element name
*    PvpBufferStream: stream read with function pdd_read_datastream()
*    PtsSizeStreamBuffer: size of the stream (returnes by pdd_read_datastream())
*    PpvBufRead: read buffer for elementvalue
*    Vs32Size: size of read buffer for elementvalue
*    Pu8Version: Version of element
*
* RETURNS: success(0) or error code(<0) 
*
* HISTORY:Created  2014 06 18
*****************************************************************************/
tS32 pdd_helper_get_element_from_stream(tString PstrElementName,const void* PvpBufferStream,size_t PtsSizeStreamBuffer, void* PpvBufRead,tS32 Vs32Size,tU8 Pu8Version)
{
  tS32   Vs32ReturnCode=-1;
  char   VFirstChar=PstrElementName[0];
  
  PDD_TRACE(PDD_START_GET_ELEMENT_VALUE,PstrElementName,strlen(PstrElementName)+1); 
  /* 4 bytes     ; 1 byte         ; 4 bytes   ;strlen(name); 4 byte        ; payload lenght*/
  /* frame lenght; element version; hash value;element name; payload length; data */
  /* search pos T in string */
  void* VvpPos=memchr(PvpBufferStream,VFirstChar,PtsSizeStreamBuffer);
  void* VvpPosOld=(void*) PvpBufferStream;
  while(VvpPos!=NULL)
  { /* compare string*/
    if(strncmp(VvpPos,PstrElementName,strlen(PstrElementName))==0)
	  { /*string found*/	
	    tU8*  Vu8pVersion=( ((tU8*)VvpPos)-1-4);
	    tS32* Vs32pPayloadLength= (tS32*)((void*)(((tU8*)VvpPos)+strlen(PstrElementName)+1));  
	    PDD_TRACE(PDD_GET_ELEMENT_NAME,PstrElementName,strlen(PstrElementName));
	    PDD_TRACE(PDD_GET_ELEMENT_VERSION,Vu8pVersion,sizeof(tU8));
	    PDD_TRACE(PDD_GET_ELEMENT_LENGTH,Vs32pPayloadLength,sizeof(tS32));
	    /*check lenght and version of element*/
	    if((*Vs32pPayloadLength<=Vs32Size)&&(*Vu8pVersion==Pu8Version))
	    {/*copy data to buffer => element found*/	    
	      void* VvpPosData= (void*) (((tU8*)VvpPos)+strlen(PstrElementName)+1+4);
	      memmove(PpvBufRead,VvpPosData,*Vs32pPayloadLength); //lenght given in datastream
	      Vs32ReturnCode=0;
	    }
	    break;
	  }
	  VvpPos=((tU8*)VvpPos)+1;
    PtsSizeStreamBuffer-=((size_t)VvpPos-(size_t)VvpPosOld);;
	  VvpPosOld=VvpPos;
	  VvpPos=memchr(VvpPos,VFirstChar,PtsSizeStreamBuffer);
  }
  PDD_TRACE(PDD_EXIT_GET_ELEMENT_VALUE,&Vs32ReturnCode,sizeof(tS32)); 
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: PDD_LockForAccess()
*
* DESCRIPTION: lock sem for different location
*
* PARAMETERS:
*      PenLocation: location
*
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static void    PDD_LockForAccess(tePddLocation PenLocation)
{
   switch(PenLocation)
   {
    case PDD_LOCATION_FS: 
    case PDD_LOCATION_FS_SECURE: 
	  {
	    M_PDD_ADMIN_LOCK(PDD_SEM_ACCESS_FILE_SYSTEM);
	  }break;
    case PDD_LOCATION_NOR_USER:
    {
	    M_PDD_ADMIN_LOCK(PDD_SEM_ACCESS_NOR_USER);
    }break;
	  case PDD_LOCATION_NOR_KERNEL:
    {
	    M_PDD_ADMIN_LOCK(PDD_SEM_ACCESS_NOR_KERNEL);
    }break;
    case PDD_LOCATION_SCC:  
    { 
	    M_PDD_ADMIN_LOCK(PDD_SEM_ACCESS_SCC);	
    }break;
    default:
    {
      PDD_FATAL_M_ASSERT_ALWAYS();
    }break;
   }/*end switch*/
}
/******************************************************************************
* FUNCTION: PDD_UnLockForAccess()
*
* DESCRIPTION: unlock sem for different location
*
* PARAMETERS:
*      PenLocation: location
*
*
* HISTORY:Created  2013 04 23
*****************************************************************************/
static void    PDD_UnLockForAccess(tePddLocation PenLocation)
{
   switch(PenLocation)
   {
    case PDD_LOCATION_FS: 
    case PDD_LOCATION_FS_SECURE: 
	  {
	    M_PDD_ADMIN_UNLOCK(PDD_SEM_ACCESS_FILE_SYSTEM);
	  }break;
    case PDD_LOCATION_NOR_USER:
    {
	    M_PDD_ADMIN_UNLOCK(PDD_SEM_ACCESS_NOR_USER);
    }break;
	  case PDD_LOCATION_NOR_KERNEL:
    {
	    M_PDD_ADMIN_UNLOCK(PDD_SEM_ACCESS_NOR_KERNEL);
    }break;
    case PDD_LOCATION_SCC:  
    { 
	    M_PDD_ADMIN_UNLOCK(PDD_SEM_ACCESS_SCC);	
    }break;
    default:
    {
      PDD_FATAL_M_ASSERT_ALWAYS();
    }break;
   }/*end switch*/
}
/******************************************************************************
* FUNCTION: PDD_GetSizeHeader()
*
* DESCRIPTION: get size of header for different location
*
* PARAMETERS:
*      PenLocation: location of the data stream in flash
*
* RETURNS: 
*      size of header
* 
* HISTORY:Created  2013 02 18
*****************************************************************************/
static tS32 PDD_GetSizeHeader(tePddLocation PenLocation)
{
  tS32  Vs32SizeFileBuffer=0;
  /* get header lenght*/
  if(PenLocation==PDD_LOCATION_SCC)
  {
	  #ifdef PDD_SCC_POOL_EXIST
    Vs32SizeFileBuffer=sizeof(TPddScc_Header);
    #endif
  }
  else if(PenLocation==PDD_LOCATION_NOR_KERNEL)
  {
    #ifdef PDD_NOR_KERNEL_POOL_EXIST
    Vs32SizeFileBuffer= ((tS32)sizeof(TPddKernel_Header));
    #endif
  }
  else
  {
    Vs32SizeFileBuffer=sizeof(tsPddFileHeader);
  }
  return(Vs32SizeFileBuffer);
}
/******************************************************************************
* FUNCTION: PDD_ReadNormalDataStreamFromLocation()
*
* DESCRIPTION: read normal datastream from different location
*
* PARAMETERS:
*      PtsDataStreamName: name of datastream
*      PenLocation      : location of the datastream in flash
*      VpBuffer         : buffer for read
*      Vs32Size         : size of bytes to read
*
* RETURNS: 
*      size of read data bytes
* 
* HISTORY:Created  2013 02 18
*****************************************************************************/
static tS32 PDD_ReadNormalDataStreamFromLocation(const char* PtsDataStreamName,tePddLocation PenLocation,const tU8* VpBuffer,tS32 Vs32Size,tBool VbNorFastEarlyAccess)
{
  tS32 Vs32Result=PDD_OK;
  switch(PenLocation)
  {
    case PDD_LOCATION_FS: 
    case PDD_LOCATION_FS_SECURE: 
	  {
	    Vs32Result=M_PDD_FILE_READ_DATASTREAM(PtsDataStreamName,PenLocation,(void*)VpBuffer,Vs32Size,PDD_KIND_FILE_NORMAL);
	  }break;
    case PDD_LOCATION_NOR_USER:
    {
	    if(VbNorFastEarlyAccess==FALSE)
	      Vs32Result=PDD_NorUserAccessReadDataStream(PtsDataStreamName,(void*)VpBuffer,Vs32Size);
	    else
        Vs32Result=PDD_NorUserAccessReadDataStreamEarly(PtsDataStreamName,(void*)VpBuffer,Vs32Size);
    }break;
	  case PDD_LOCATION_NOR_KERNEL:
    {
	    Vs32Result=M_PDD_NORKERNEL_ACCESS_READ_DATASTREAM((void*)VpBuffer,Vs32Size);
    }break;
    case PDD_LOCATION_SCC:  
    { 
	    Vs32Result=M_PDD_SCCACCESS_READ_DATASTREAM(PtsDataStreamName,(void*)VpBuffer,Vs32Size);
    }break;
    default:
    {
     Vs32Result=PDD_ERROR_WRONG_PARAMETER;  
    }break;
  }/*end switch*/
  return(Vs32Result);
}

/******************************************************************************
* FUNCTION: PDD_WriteNormalDataStreamToLocation()
*
* DESCRIPTION: write normal datastream to different location
*
* PARAMETERS:
*      PtsDataStreamName: name of datastream
*      PenLocation      : location of the data stream in flash
*      VpBuffer         : write buffer
*      Vs32Size         : size of bytes to write
*
* RETURNS: 
*      size of write data bytes
* 
* HISTORY:Created  2013 02 18
*****************************************************************************/
static tS32  PDD_WriteNormalDataStreamToLocation(const char* PtsDataStreamName,tePddLocation PenLocation,const tU8* VpBuffer,tS32 Vs32Size,tBool PbfSync)
{
  tS32 Vs32Result=PDD_OK;
  switch(PenLocation)
  {
    case PDD_LOCATION_FS: 
    case PDD_LOCATION_FS_SECURE: 
	  {
	    Vs32Result=M_PDD_FILE_WRITE_DATASTREAM(PtsDataStreamName,PenLocation,(void*)VpBuffer,Vs32Size,PDD_KIND_FILE_NORMAL,PbfSync);
	  }break;
    case PDD_LOCATION_NOR_USER:
    {
	    Vs32Result=PDD_NorUserAccessWriteDataStream(PtsDataStreamName,(void*)VpBuffer,Vs32Size);
    }break;
	  case PDD_LOCATION_NOR_KERNEL:
    {
	    Vs32Result=M_PDD_NORKERNEL_ACCESS_WRITE_DATASTREAM((void*)VpBuffer,Vs32Size);
    }break;
    case PDD_LOCATION_SCC:  
    { 
	    Vs32Result=M_PDD_SCCACCESS_WRITE_DATASTREAM(PtsDataStreamName,(void*)VpBuffer,Vs32Size);
    }break;
    default:
    {
      Vs32Result=PDD_ERROR_WRONG_PARAMETER;  
    }break;
  }/*end switch*/
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_CheckValidationDataStream()
*
* DESCRIPTION: check datastream for different location
*
* PARAMETERS:
*      PenLocation      : location of the data stream
*      VpBuffer         : buffer
*      Vs32Size         : size 
*      Pu32Version      : version
*
* RETURNS: 
*      TRUE  datastream valid
*      FALSE datastream unvalid
* 
* HISTORY:Created  2013 02 18
*****************************************************************************/
static tBool PDD_CheckValidationDataStream(tePddLocation PenLocation,const tU8* VpBuffer,tS32 Vs32Size,tU32 Pu32Version)
{
  tBool VbValidation=FALSE;
  if(PenLocation==PDD_LOCATION_SCC)
  {
    VbValidation=PDD_ValidationCheckScc((void*)VpBuffer,Vs32Size,Pu32Version);
  }
  else if(PenLocation==PDD_LOCATION_NOR_KERNEL)
  {
    VbValidation=PDD_ValidationCheckNorKernel((void*)VpBuffer,Vs32Size,Pu32Version);
  }
  else
  {
    VbValidation=PDD_ValidationCheckFile((void*)VpBuffer,Vs32Size,Pu32Version);
  }
  return(VbValidation);
}
/******************************************************************************
* FUNCTION: PDD_CheckBufferEqual()
*
* DESCRIPTION: check the buffer is equel with the CRC
*
* PARAMETERS:
*      PenLocation      : location of the data stream
*      VpBuffer1        : buffer1
*      VpBuffer2        : buffer2 
*
* RETURNS: 
*      TRUE  buffer equal
*      FALSE buffer different
* 
* HISTORY:Created  2013 02 18
*****************************************************************************/
static tBool PDD_CheckBufferEqual(tePddLocation PenLocation,const void* VpBuffer1,const void* VpBuffer2)
{
  tBool VbEqual=FALSE;
  
  if(PenLocation==PDD_LOCATION_SCC)
  {
    VbEqual=PDD_ValidationCheckCrcEqualScc(VpBuffer1,VpBuffer2);
  }
  else if(PenLocation==PDD_LOCATION_NOR_KERNEL)
  {
    VbEqual=PDD_ValidationCheckCrcEqualNorKernel(VpBuffer1,VpBuffer2);
  }
  else
  {
    VbEqual=PDD_ValidationCheckCrcEqualFile(VpBuffer1,VpBuffer2);
  }
  return(VbEqual);
}
/******************************************************************************
* FUNCTION: PDD_GetHeader()
*
* DESCRIPTION: get the header for different location
*
* PARAMETERS:
*      PenLocation      : location of the data stream
*      VpBuffer         : buffer
*      Vs32Size         : size 
*      Pu32Version      : version
*      Pu8pHeader       : pointer for the header
*
* 
* HISTORY:Created  2013 02 18
*****************************************************************************/
static void  PDD_GetHeader(tePddLocation PenLocation,tU8* PubBuf,tS32 Ps32Size,tU32 Pu32Version,tU8* Pu8pHeader)
{
  if(PenLocation==PDD_LOCATION_SCC)
  {
    PDD_ValidationGetHeaderScc(PubBuf,Ps32Size,Pu32Version,(void*)Pu8pHeader);
  }
  else if(PenLocation==PDD_LOCATION_NOR_KERNEL)
  {
    PDD_ValidationGetHeaderNorKernel(PubBuf,Ps32Size,Pu32Version,(void*)Pu8pHeader);
  }
  else
  {
    PDD_ValidationGetHeaderFile(PubBuf,Ps32Size,Pu32Version,(void*)Pu8pHeader);
  } 
}
#ifndef PDD_TESTMANGER_ACTIVE
/******************************************************************************
* FUNCTION: PDD_IsEarlyKDSBackupAccess()
*
* DESCRIPTION: check if access for KDS is  for different location
*
* PARAMETERS:
*      PtsDataStreamName: stream name
*      PenLocation      : location of the data stream
*
* RETURNS: 
*      TRUE  datastream valid
*      FALSE datastream unvalid
* 
* HISTORY:Created  2014 02 18
*****************************************************************************/
static tBool PDD_IsSyncBackupPossible(tePddLocation PenLocation)
{
  tBool VbIsEarlyKDSBackupAccess=TRUE;
  /* no access location=PDD_LOCATION_NOR_USER; for datastraem "kds_data" */
  if(PenLocation==PDD_LOCATION_NOR_USER)
  {
    struct timespec VTimer;	     
	  clock_gettime(CLOCK_MONOTONIC, &VTimer);
	  /*check runtime*/
	  if(VTimer.tv_sec < PDD_FILE_PATH_SECURE_MAX_MOUNT_TIME)
	  { /* no read/write access to file systenm at early startup*/  
      VbIsEarlyKDSBackupAccess=FALSE;
	   // fprintf(stderr, "PDD_IsSyncBackupPossible: no access to backup %d\n",PDD_FILE_PATH_SECURE_MAX_MOUNT_TIME);
	  }
  }
  return(VbIsEarlyKDSBackupAccess);
}
/******************************************************************************
* FUNCTION: PDD_ReadBackupDataStreamFromLocation()
*
* DESCRIPTION: read backup datastream from different location
*
* PARAMETERS:
*      PtsDataStreamName: name of datastream
*      PenLocation      : location of the datastream in flash
*      VpBuffer         : buffer for read
*      Vs32Size         : size of bytes to read
*
* RETURNS: 
*      size of read data bytes
* 
* HISTORY:Created  2013 11 27
*****************************************************************************/
static tS32    PDD_ReadBackupDataStreamFromLocation(const char* PtsDataStreamName,tePddLocation PenLocation,const tU8* VpBuffer,tS32 Vs32Size )
{
  tS32 Vs32Result=PDD_OK;
  switch(PenLocation)
  {
    case PDD_LOCATION_FS: 
    case PDD_LOCATION_FS_SECURE: 
	  {
	    Vs32Result=M_PDD_FILE_READ_DATASTREAM(PtsDataStreamName,PenLocation,(void*)VpBuffer,Vs32Size,PDD_KIND_FILE_BACKUP);
	  }break;
	  case PDD_LOCATION_NOR_USER:
	  case PDD_LOCATION_NOR_KERNEL:
	  { 
	    tePddLocation VenLocation=PDD_LOCATION_FS_SECURE;
	    PtsDataStreamName=PDD_DeleteFolderFromStream(PtsDataStreamName);
	    /* lock additional for file access */
	    PDD_LockForAccess(VenLocation);  //PDD_SEM_ACCESS_FILE_SYSTEM for both file system FS and FS_SECURE
	    Vs32Result=PDD_IsInit(VenLocation,PtsDataStreamName);
	    if(Vs32Result==PDD_OK)
	    { 
        Vs32Result=M_PDD_FILE_READ_DATASTREAM(PtsDataStreamName,PenLocation,(void*)VpBuffer,Vs32Size,PDD_KIND_FILE_BACKUP);	            
	    }
	    PDD_UnLockForAccess(VenLocation);
	  }break;    
    case PDD_LOCATION_SCC:  
    { /* lock additional for nor access */
	    PDD_LockForAccess(PDD_LOCATION_NOR_USER);
	    Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,PtsDataStreamName);
	    if(Vs32Result==PDD_OK)
	    {	   
	      Vs32Result=PDD_NorUserAccessReadDataStream(PtsDataStreamName,(void*)VpBuffer,Vs32Size);	   
	    }
      PDD_UnLockForAccess(PDD_LOCATION_NOR_USER);
    }break;
    default:
    {
     Vs32Result=PDD_ERROR_WRONG_PARAMETER;  
    }break;
  }/*end switch*/
  return(Vs32Result);
}
/******************************************************************************
* FUNCTION: PDD_WriteBackupDataStreamToLocation()
*
* DESCRIPTION: write backup datastream to different location
*
* PARAMETERS:
*      PtsDataStreamName: name of datastream
*      PenLocation      : location of the data stream in flash
*      VpBuffer         : write buffer
*      Vs32Size         : size of bytes to write
*
* RETURNS: 
*      size of write data bytes
* 
* HISTORY:Created  2013 02 18
*****************************************************************************/
static tS32  PDD_WriteBackupDataStreamToLocation(const char* PtsDataStreamName,tePddLocation PenLocation,const tU8* VpBuffer,tS32 Vs32Size,tBool PbfSync)
{
  tS32 Vs32Result=PDD_OK;
  switch(PenLocation)
  {
    case PDD_LOCATION_FS: 
    case PDD_LOCATION_FS_SECURE: 
	  {
	    Vs32Result=M_PDD_FILE_WRITE_DATASTREAM(PtsDataStreamName,PenLocation,(void*)VpBuffer,Vs32Size,PDD_KIND_FILE_BACKUP,PbfSync);
	  }break;
	  case PDD_LOCATION_NOR_USER:
	  case PDD_LOCATION_NOR_KERNEL:
	  {
	    tePddLocation VenLocation=PDD_LOCATION_FS_SECURE;
	    PtsDataStreamName=PDD_DeleteFolderFromStream(PtsDataStreamName);
	    PDD_LockForAccess(VenLocation);  //PDD_SEM_ACCESS_FILE_SYSTEM for both file system FS and FS_SECURE
	    Vs32Result=PDD_IsInit(VenLocation,PtsDataStreamName);
	    if(Vs32Result==PDD_OK)
	    {	   
        Vs32Result=M_PDD_FILE_WRITE_DATASTREAM(PtsDataStreamName,PenLocation,(void*)VpBuffer,Vs32Size,PDD_KIND_FILE_BACKUP,PbfSync);	          
	    }
	    PDD_UnLockForAccess(VenLocation);
	  }break;
    case PDD_LOCATION_SCC:  
    { 
	    PDD_LockForAccess(PDD_LOCATION_NOR_USER);
	    Vs32Result=PDD_IsInit(PDD_LOCATION_NOR_USER,PtsDataStreamName);
	    if(Vs32Result==PDD_OK)
	    {	    
	      Vs32Result=PDD_NorUserAccessWriteDataStream(PtsDataStreamName,(void*)VpBuffer,Vs32Size);       
	    }	
	    PDD_UnLockForAccess(PDD_LOCATION_NOR_USER);
    }break;
    default:
    {
       Vs32Result=PDD_ERROR_WRONG_PARAMETER;  
    }break;
  }/*end switch*/
  return(Vs32Result);
}

#endif

/******************************************************************************
* FUNCTION: PDD_DeleteFolderFromStream()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
* 
* HISTORY:Created  2014 07 01
*****************************************************************************/
static const char* PDD_DeleteFolderFromStream(const char* PtsDataStreamName)
{
  char* VstrPosFolder=strrchr((char*)PtsDataStreamName,'/'); //compare without folder
 
  /*check datastream with folder */
  if(VstrPosFolder!=NULL)
  {
    PtsDataStreamName=VstrPosFolder+1;
  }
  return(PtsDataStreamName);
}


#ifdef __cplusplus
}
#endif
/******************************************************************************/
/* End of File pdd.c                                                          */
/******************************************************************************/