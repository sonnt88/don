#ifndef __DATAPOOL_UTEST_DPSTORE_TESTMOCKS_H
#define __DATAPOOL_UTEST_DPSTORE_TESTMOCKS_H

#include "MockDelegatee.h"

class PddMock  : public MockDelegatee<PddMock> 
{
  public:
    static inline const char *GetMockName() { return "PddMock"; }
    static PddMock &GetDelegatee() 
    {
      return MockDelegatee<PddMock>::GetDelegatee();
    }

    MOCK_METHOD2(pdd_get_data_stream_size,       tS32(const char* PtsDataStreamName,tePddLocation PtLocation));
    MOCK_METHOD5(pdd_read_data_stream,           tS32(const char* PtsDataStreamName,tePddLocation PtLocation,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version));
    MOCK_METHOD6(pdd_read_datastream,            tS32(const char* PtsDataStreamName,tePddLocation PtLocation,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version,tU8* Pu8Info));
    MOCK_METHOD5(pdd_write_data_stream,          tS32(const char* PtsDataStreamName,tePddLocation PtLocation,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version));	
    MOCK_METHOD6(pdd_write_datastream,           tS32(const char* PtsDataStreamName,tePddLocation PtLocation,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version,tBool PbfSync));	
	  MOCK_METHOD1(pdd_vTraceCommand,              void(char* Pu8pData));
 	  MOCK_METHOD0(pdd_sync_scc_streams,           void());
	  MOCK_METHOD0(pdd_sync_nor_user_streams,      void());
	  MOCK_METHOD2(pdd_delete_data_stream,         tS32(const char* PtsDataStreamName,tePddLocation PtLocation));
	  MOCK_METHOD6(pdd_helper_get_element_from_stream, tS32(tString PstrElementName,const void* PvpBufferStream,size_t PtsSizeStreamBuffer, void* PpvBufRead,tS32 Vs32Size,tU8 Pu8Version));
};

#endif     
