/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        These are the mocking functions for PDD 
 * @addtogroup   PDD access
 * @{
 */ 

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "gmock/gmock.h"

#define SYSTEM_S_IMPORT_INTERFACE_COMPLETE
  #include "system_pif.h"

#include "pdd.h"
#include "pdd_mock.h"

extern "C" 
{
  tS32 pdd_get_data_stream_size(const char* PtsDataStreamName,tePddLocation PtLocation)
  {
    return PddMock::GetDelegatee().pdd_get_data_stream_size(PtsDataStreamName,PtLocation);
  }
  tS32 pdd_read_data_stream(const char* PtsDataStreamName,tePddLocation PtLocation,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version)
  {
    return PddMock::GetDelegatee().pdd_read_data_stream(PtsDataStreamName,PtLocation,Ppu8ReadBuffer,Ps32SizeReadBuffer,Pu32Version);
  }
  tS32 pdd_read_datastream(const char* PtsDataStreamName,tePddLocation PtLocation,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version,tU8* Pu8Info)
  {
    return PddMock::GetDelegatee().pdd_read_datastream(PtsDataStreamName,PtLocation,Ppu8ReadBuffer,Ps32SizeReadBuffer,Pu32Version,Pu8Info);
  }
  tS32 pdd_write_data_stream(const char* PtsDataStreamName,tePddLocation PtLocation,tU8 *Ppu8WriteBuffer, tS32 Ps32SizeBytestoWrite,tU32 Pu32Version)
  {
    return PddMock::GetDelegatee().pdd_write_data_stream(PtsDataStreamName,PtLocation,Ppu8WriteBuffer,Ps32SizeBytestoWrite,Pu32Version);
  }  
  tS32 pdd_write_datastream(const char* PtsDataStreamName,tePddLocation PtLocation,tU8 *Ppu8WriteBuffer, tS32 Ps32SizeBytestoWrite,tU32 Pu32Version,tBool PbfSync)
  {
    return PddMock::GetDelegatee().pdd_write_datastream(PtsDataStreamName,PtLocation,Ppu8WriteBuffer,Ps32SizeBytestoWrite,Pu32Version,PbfSync);
  }  
  void pdd_vTraceCommand(char* Pu8pData)
  {
    return PddMock::GetDelegatee().pdd_vTraceCommand(Pu8pData);
  }  
  void pdd_sync_scc_streams(void)
  {
    return PddMock::GetDelegatee().pdd_sync_scc_streams();
  }  
  void pdd_sync_nor_user_streams(void)
  {
    return PddMock::GetDelegatee().pdd_sync_nor_user_streams();
  }  
  tS32 pdd_delete_data_stream(const char* PtsDataStreamName,tePddLocation PtLocation)
  {
    return PddMock::GetDelegatee().pdd_delete_data_stream(PtsDataStreamName,PtLocation);
  }  
  tS32 pdd_helper_get_element_from_stream(tString PstrElementName,const void* PvpBufferStream,size_t PtsSizeStreamBuffer, void* PpvBufRead,tS32 Vs32Size,tU8 Pu8Version)
  {
    return PddMock::GetDelegatee().pdd_helper_get_element_from_stream(PstrElementName,PvpBufferStream,PtsSizeStreamBuffer,PpvBufRead,Vs32Size,Pu8Version);
  }
};		




/************************************************************************
|end of file
|-----------------------------------------------------------------------*/

