/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        mock functions for PDD unit test
 * @addtogroup   PDD unit test
 * @{
 */

#ifndef __PDD_UTEST_MOCK_H_
#define __PDD_UTEST_MOCK_H_

/* for NOR USER ACCESS*/
#define PDD_UTEST_ACCESS_NOR_USER_BLOCK_SIZE                   0x20000
#define PDD_UTEST_ACCESS_NOR_USER_CHUNK_SIZE                   0x20
#define PDD_UTEST_ACCESS_NOR_USER_NUM_BLOCKS                   2

extern "C" {

};

class PddMockOut {
 public:
  //MOCK_METHOD3(OSAL_vAssertFunction,          void(const tChar* exp, const tChar* file, tU32 line));
  MOCK_METHOD3(dgram_init,                    sk_dgram*(int sk, int dgram_max, void *options));
  MOCK_METHOD1(dgram_exit,                    int(sk_dgram* sk));
  MOCK_METHOD3(dgram_send,                    int(sk_dgram *skd, void *ubuf, size_t ulen));
  MOCK_METHOD3(dgram_recv,                    int(sk_dgram *skd, void *ubuf, size_t ulen));
  MOCK_METHOD1(LFX_open,                      LFXhandle(const char *name));
  MOCK_METHOD1(LFX_close,                     void(LFXhandle part));
  MOCK_METHOD4(LFX_read,                      int(LFXhandle part , void *buffer , unsigned int size , unsigned int offset ));
  MOCK_METHOD4(LFX_write,                     int(LFXhandle part , const void *buffer , unsigned int size , unsigned int offset ));
  MOCK_METHOD3(LFX_erase,                     int(LFXhandle part , unsigned int size, unsigned int offset ));
  MOCK_METHOD4(vWriteToErrMem,                void(tS32 trClass, const char* pBuffer,int Len,tU32 TrcType));
  MOCK_METHOD3(poll,                          int(struct pollfd *fds, nfds_t nfds, int timeout));  
  MOCK_METHOD3(vAssertFunction,               void(const char* expr, const char* file, unsigned int line));
  MOCK_METHOD3(vConsoleTrace,                 void(int PiStdStream, const void* pvData, unsigned int Length));
};

extern PddMockOut vPddMockOutObj; 

#endif     