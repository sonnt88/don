/**
 * @copyright    (C) 2012 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        mock functions for PDD unit test
 * @addtogroup   PDD unit test
 * @{
 */
/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "gmock/gmock.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include <poll.h>
#include "dgram_service.h"
#include "LFX2.h"
#include "inc_scc_pdd.h" 
#include "pdd_mockout.h"

PddMockOut vPddMockOutObj; 

extern "C" {

//void OSAL_vAssertFunction(const tChar* exp, const tChar* file, tU32 line)
//{
//  vPddMockOutObj.OSAL_vAssertFunction(exp,file,line);
//}
/****************** dgram ******************************************/
static tU8  VubSendMesg;
static tU8  VubSizeDirectBuf;
static tU8  VubSizeShutdownBuf;
static tU8  VubDirectBuf[SCC_PD_NET_MAX_LENGTH_DATA_BYTES];
static tU8  VubShutdownBuf[SCC_PD_NET_MAX_LENGTH_DATA_BYTES];

sk_dgram* dgram_init(int sk, int dgram_max, void *options)
{  
  vPddMockOutObj.dgram_init(sk,dgram_max,options);
}
int dgram_exit(sk_dgram *skd)
{
  vPddMockOutObj.dgram_exit(skd);
  VubSizeDirectBuf=0;
  VubSizeShutdownBuf=0;
}
int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen)
{
  tU8  VubBuf[SCC_PD_NET_MAX_LENGTH_DATA_BYTES];
  tU8  VubSize;

  memcpy(&VubBuf,ubuf,ulen);
  VubSendMesg=VubBuf[0];
  if(VubSendMesg==SCC_PD_NET_TP_MARKER)
  {
    VubSendMesg=VubBuf[1];
	/* get size on position 3*/	
	VubSize=VubBuf[3];
	/* and data on position 4*/
	if(VubSendMesg==SCC_PD_NET_C_WRITE_DATA_POOL)
	{
	  VubSizeShutdownBuf=VubSize;
	  memset(VubShutdownBuf,0x00,SCC_PD_NET_MAX_LENGTH_DATA_BYTES);
	  memcpy(VubShutdownBuf,&VubBuf[4],VubSizeShutdownBuf);
	}
	else
	{
	  VubSizeDirectBuf=VubSize;
	  memset(VubDirectBuf,0x00,SCC_PD_NET_MAX_LENGTH_DATA_BYTES);
	  memcpy(VubDirectBuf,&VubBuf[4],VubSizeDirectBuf);
	}
  }
  vPddMockOutObj.dgram_send(skd,ubuf,ulen);
}
int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen)
{
  
  /*save message id*/
  tU8* VubBuf=(tU8*) ubuf;
  /*save message id*/
  memset(VubBuf,0x00,ulen);
  *VubBuf=VubSendMesg+1;
  /* if buffer read */
 if(*VubBuf==SCC_PD_NET_R_READ_DATA_POOL)
  { /* read length on position 2*/
    VubBuf++;
	VubBuf++;
    *VubBuf=(tU8)VubSizeShutdownBuf;
    /* and data on position 3*/
	VubBuf++;
    memcpy(VubBuf,VubShutdownBuf,VubSizeShutdownBuf);
  }
  else
  {/* read length on position 2*/
    VubBuf++;
	VubBuf++;
    *VubBuf=(tU8)VubSizeDirectBuf;
	/* and data on position 3*/
	VubBuf++;
    memcpy(VubBuf,VubDirectBuf,VubSizeDirectBuf);
  }
  vPddMockOutObj.dgram_recv(skd,ubuf,ulen);
}
/****************** LFX ******************************************/
LFXhandle LFX_open(const char *name)
{
  LFXpart *res;  
                         /*LFX- Part + 2* blocksize*/
  res = (LFXpart*)malloc(sizeof(LFXpart)+2*0x21000);
  memset((void*)res,0xFF,sizeof(LFXpart)+2*0x21000);
  res->name=(const char*) (res+sizeof(LFXpart)); 
  res->intID = 0;
  res->blockSize = PDD_UTEST_ACCESS_NOR_USER_BLOCK_SIZE;
  res->chunkSize = PDD_UTEST_ACCESS_NOR_USER_CHUNK_SIZE;
  res->numBlocks = PDD_UTEST_ACCESS_NOR_USER_NUM_BLOCKS;
  res->hasECC = 1;
  vPddMockOutObj.LFX_open(name);
  return res;
}
void LFX_close(LFXhandle part)
{
  vPddMockOutObj.LFX_close(part);
  free((LFXpart*)part);
}
int LFX_read( LFXhandle part , void *buffer , unsigned int size , unsigned int offset )
{
  memcpy(buffer,part->name+offset,size);
  vPddMockOutObj.LFX_read(part,buffer,size,offset);
}
int LFX_write( LFXhandle part , const void *buffer , unsigned int size , unsigned int offset )
{
  memcpy((void*)(part->name+offset),buffer,size);
  vPddMockOutObj.LFX_write(part,buffer,size,offset);
}
int LFX_erase(LFXhandle part , unsigned size , unsigned int offset )
{
  memset( (void*)(part->name+offset) , 0xFF ,  size );
  vPddMockOutObj.LFX_erase(part,size, offset);
}
void vWriteToErrMem(tS32 trClass, const char* pBuffer,int Len,tU32 TrcType)
{
  vPddMockOutObj.vWriteToErrMem(trClass,pBuffer,Len,TrcType);
}
int  poll(struct pollfd *fds, nfds_t nfds, int timeout)
{
  timeout=1;
  fds[0].revents = POLLIN;
  vPddMockOutObj.poll(fds,nfds,timeout);
 
}
void vAssertFunction(const char* expr, const char* file, unsigned int line)
{
  vPddMockOutObj.vAssertFunction(expr,file,line);
}
void vConsoleTrace(int PiStdStream, const void* pvData, unsigned int Length)
{
  vPddMockOutObj.vConsoleTrace(PiStdStream,pvData,Length);
}
};						   


/************************************************************************
|end of file
|-----------------------------------------------------------------------*/

