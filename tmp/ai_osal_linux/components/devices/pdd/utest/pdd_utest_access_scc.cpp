/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        These are the unit tests for pdd_access_scc
 * @addtogroup   PDD unit test
 * @{
 */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "dgram_service.h"
#include "pdd.h"
#include "LFX2.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#include "pdd_mockout.h"
#include "DataPoolSCC.h"

using ::testing::Return;
using ::testing::AtLeast; 
using ::testing::_;
using ::testing::ReturnArg;

/**********************  PddAccessSccTest *************************************************/
class PddAccessSccTest : public ::testing::Test 
{
public:
	PddAccessSccTest() 
	{/* You can do set-up work for each test here.*/
	 /*dgram_send returns 0 */
      EXPECT_CALL(vPddMockOutObj,dgram_send(_,_,_))		 
	     .WillRepeatedly(ReturnArg<2>());
	  EXPECT_CALL(vPddMockOutObj,dgram_recv(_,_,_))
		.WillRepeatedly(ReturnArg<2>());
	  EXPECT_CALL(vPddMockOutObj,poll(_,_,_))
		.WillRepeatedly(Return(10));
	  EXPECT_CALL(vPddMockOutObj,vConsoleTrace(_,_,_)).Times(AtLeast(0));
    }
	virtual ~PddAccessSccTest() 
	{/* You can do clean-up work that doesn't throw exceptions here.*/
	 
    }

protected:
	virtual void SetUp() 
	{ /*Code here will be called immediately after the constructor (rightbefore each test).*/
     
	}
	virtual void TearDown() 
	{ /*Code here will be called immediately after each test (right before the destructor).*/
      EXPECT_CALL(vPddMockOutObj,dgram_exit(_))
	    .WillRepeatedly(Return(1));
	}
	/* Objects declared here can be used by all tests in the test case for PddAccessSccTest.*/    
};

/**********************  PDD_SccAccessGetDataStreamSize *************************************************/
TEST_F(PddAccessSccTest, PDD_SccAccessGetDataStreamSize_InvalidDataStream_ERROR_SCC_INVALID_DATA_STREAM)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="invalid";
  Vs32Return=PDD_SccAccessGetDataStreamSize(VcDataStreamName);
  EXPECT_EQ(PDD_ERROR_SCC_INVALID_DATA_STREAM, Vs32Return);
}

/**********************  PDD_SccAccessReadDataStream *************************************************/
TEST_F(PddAccessSccTest, PDD_SccAccessReadDataStream_InvalidDataStream_ERROR_SCC_INVALID_DATA_STREAM)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="invalid";
  Vs32Return=PDD_SccAccessReadDataStream(VcDataStreamName,NULL,0);
  EXPECT_EQ(PDD_ERROR_SCC_INVALID_DATA_STREAM, Vs32Return);
}

TEST_F(PddAccessSccTest, PDD_SccAccessReadDataStream_PointerBufferNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="PddDpTest";
  Vs32Return=PDD_SccAccessReadDataStream(VcDataStreamName,NULL,12);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}

TEST_F(PddAccessSccTest, PDD_SccAccessReadDataStream_SizeReadNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="PddDpTest";
  tU8  Vu8Buf[12];
  Vs32Return=PDD_SccAccessReadDataStream(VcDataStreamName,&Vu8Buf[0],0);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}
/**********************  PDD_SccAccessWriteDataStream *************************************************/
TEST_F(PddAccessSccTest, PDD_SccAccessWriteDataStream_InvalidDataStream_ERROR_SCC_INVALID_DATA_STREAM)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="invalid";
  Vs32Return=PDD_SccAccessWriteDataStream(VcDataStreamName,NULL,0);
  EXPECT_EQ(PDD_ERROR_SCC_INVALID_DATA_STREAM, Vs32Return);
}

TEST_F(PddAccessSccTest, PDD_SccAccessWriteDataStream_PointerBufferNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="PddDpTest";
  Vs32Return=PDD_SccAccessWriteDataStream(VcDataStreamName,NULL,12);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}

TEST_F(PddAccessSccTest, PDD_SccAccessWriteDataStream_SizeWriteNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="PddDpTest";
  tU8  Vu8Buf[12];
  Vs32Return=PDD_SccAccessWriteDataStream(VcDataStreamName,&Vu8Buf[0],0);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
