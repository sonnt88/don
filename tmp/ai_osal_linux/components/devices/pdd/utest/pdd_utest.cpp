/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        These are the unit tests for pdd.c
 * @addtogroup   PDD unit test
 * @{
 */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>
#include <dirent.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "dgram_service.h"
#include "pdd.h"
#include "LFX2.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#include "pdd_mockout.h"
#include "DataPoolSCC.h"

/*define Version */
#define PDD_UTEST_VERSION          0x2020
#define PDD_UTEST_SCC_VERSION      0xaa

/*define for ACCESS FILE*/
#define PDD_UTEST_ACCESS_FILE_NAME_TEST  "utestfile"
#define PDD_UTEST_ACCESS_FILE_SIZE_TEST  500
/*size data */
#define PDD_UTEST_ACCESS_FILE_SIZE_DATA_WITHOUT_FILE_HEADER (PDD_UTEST_ACCESS_FILE_SIZE_TEST-(sizeof(tsPddFileHeader)))
#define PDD_UTEST_ACCESS_FILE_SIZE_DATA_WITHOUT_SCC_HEADER  (PDD_UTEST_ACCESS_FILE_SIZE_TEST-(sizeof(tsPddSccHeader)))

/*define for ACCESS NOR_USER*/
#define PDD_UTEST_ACCESS_NOR_USER_NAME_KDS              "kds_data"
#define PDD_UTEST_ACCESS_NOR_USER_NAME_EOL              "eol_data"
#define PDD_UTEST_ACCESS_NOR_USER_SIZE_TEST             200

/*define for ACCESS SCC*/
#define PDD_UTEST_ACCESS_SCC_VALID                 "PddDpTest" 
#define PDD_UTEST_ACCESS_SCC_SIZE_TEST             sizeof(TPddScc_PddDpTest)

using ::testing::Return;
using ::testing::Exactly; 
using ::testing::_;
using ::testing::StrEq;
using ::testing::NotNull;
using ::testing::AtLeast;
using ::testing::ReturnArg;

/**********************  PDD test  *************************************************/
class PddTest : public ::testing::Test 
{
public:
	PddTest() 
	{/* You can do set-up work for each test here.*/	 	  
	  DIR   *VpDir=NULL;
	  VpDir=opendir(PDD_FILE_DEFAULT_PATH);
    if(VpDir==NULL)
    {/*create path, if cannot open*/
     mkdir("/tmp/var/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
		 mkdir("/tmp/var/opt/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
		 mkdir("/tmp/var/opt/bosch/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
		 mkdir("/tmp/var/opt/bosch/dynamic/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
		 mkdir("/tmp/var/opt/bosch/persistent/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
	  }
	  EXPECT_CALL(vPddMockOutObj,dgram_exit(_))
	    .WillRepeatedly(Return(1));
	  EXPECT_CALL(vPddMockOutObj,dgram_send(_,_,_))
	     .WillRepeatedly(ReturnArg<2>());
	  EXPECT_CALL(vPddMockOutObj,dgram_recv(_,_,_))
		.WillRepeatedly(ReturnArg<2>());
	   EXPECT_CALL(vPddMockOutObj,LFX_open(StrEq("PDD_UserEarly")))
	    .WillRepeatedly(Return(vhLfx));
	  EXPECT_CALL(vPddMockOutObj,LFX_read(NotNull(),NotNull(),_,_))
		.WillRepeatedly(Return(0));
      EXPECT_CALL(vPddMockOutObj,LFX_write(NotNull(),NotNull(),_,_))
		.WillRepeatedly(Return(0));
      EXPECT_CALL(vPddMockOutObj,LFX_erase(NotNull(),_,_))	   
        .WillRepeatedly(Return(0));
     EXPECT_CALL(vPddMockOutObj,LFX_close(NotNull()))
		.Times(AtLeast(0));
	  EXPECT_CALL(vPddMockOutObj,poll(_,_,_))
		.WillRepeatedly(Return(10));
	  EXPECT_CALL(vPddMockOutObj,vConsoleTrace(_,_,_)).Times(AtLeast(0));
    }
	virtual ~PddTest() 
	{/* You can do clean-up work that doesn't throw exceptions here.*/
	 
    }

protected:
	virtual void SetUp() 
	{ /* Code here will be called immediately after the constructor (rightbefore each test).*/
	  PDD_IsInit(PDD_LOCATION_FS,"Test");
	  PDD_IsInit(PDD_LOCATION_FS_SECURE,"Test");
	  PDD_IsInit(PDD_LOCATION_NOR_USER,"Test");
	  PDD_IsInit(PDD_LOCATION_SCC,"Test");
	  /* -------------  create a test file --------------------*/
	  /* allocata buffer */
	  vpBufferFile=malloc(PDD_UTEST_ACCESS_FILE_SIZE_TEST);
	  /* set buffer */
	  memset(vpBufferFile,0xa5,PDD_UTEST_ACCESS_FILE_SIZE_TEST);	
	  /* save file to different location */
	  PDD_FileWriteDataStream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
	  PDD_FileWriteDataStream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
	  PDD_FileWriteDataStream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS_SECURE,vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
	  PDD_FileWriteDataStream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS_SECURE,vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
	  PDD_FileWriteDataStream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_NOR_USER,vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
    PDD_FileWriteDataStream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_SCC,vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
	}
	virtual void TearDown() 
	{ /*Code here will be called immediately after each test (right before the destructor).*/
	  PDD_DeInitAccess();	      
	  /*set buffer to free*/
	  free(vpBufferFile);	 
	}
	/* Objects declared here can be used by all tests in the test case for PddAccessNOR_USERTest.*/    
	LFXhandle vhLfx;
	void* vpBufferFile;
};

/**********************  pdd_get_data_stream_size *************************************************/
TEST_F(PddTest, pdd_get_data_stream_size_NameNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  /*get size*/
  Vs32Return=pdd_get_data_stream_size(NULL,PDD_LOCATION_FS);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest, pdd_get_data_stream_size_WrongLocation_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  /*get size*/
  Vs32Return=pdd_get_data_stream_size((char*) PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,PDD_LOCATION_LAST);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}

TEST_F(PddTest, pdd_get_data_stream_size_InvalidDataStreamFile_ERROR_FILE_NO_VALID_SIZE)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="test_invalid";
  Vs32Return=pdd_get_data_stream_size(VcDataStreamName,PDD_LOCATION_FS);
  EXPECT_EQ(PDD_ERROR_FILE_NO_VALID_SIZE, Vs32Return);
}
TEST_F(PddTest, pdd_get_data_stream_size_InvalidDataStreamNOR_USER_Assert)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="test_invalid";
  //EXPECT_CALL(vPddMockOutObj,vAssertFunction(_,_,_)).Times(AtLeast(1));	
  Vs32Return=pdd_get_data_stream_size(VcDataStreamName,PDD_LOCATION_NOR_USER);
  EXPECT_EQ(PDD_ERROR_FILE_NO_VALID_SIZE, Vs32Return);
}
TEST_F(PddTest, pdd_get_data_stream_size_InvalidDataStreamScc_ERROR_SCC_INVALID_DATA_STREAM)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="test_invalid";
  Vs32Return=pdd_get_data_stream_size(VcDataStreamName,PDD_LOCATION_SCC);
  EXPECT_EQ(PDD_ERROR_SCC_INVALID_DATA_STREAM, Vs32Return);
}
TEST_F(PddTest, pdd_get_data_stream_size_LocationFileSystem_FILE_SIZE_TEST)
{
  tS32 Vs32Return;
  Vs32Return=pdd_get_data_stream_size((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_DATA_WITHOUT_FILE_HEADER, Vs32Return);
}
TEST_F(PddTest, pdd_get_data_stream_size_LocationFileSystemSecure_FILE_SIZE_TEST)
{
  tS32 Vs32Return;
  Vs32Return=pdd_get_data_stream_size((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS_SECURE);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_DATA_WITHOUT_FILE_HEADER, Vs32Return);
}
TEST_F(PddTest, pdd_get_data_stream_size_LocationNOR_USERNorDataWritteb_Size)
{
  tS32 Vs32Return;
  tU8  VubBuffer[]={1,3,45,5,7,8,9,0,1,1,1,1,1,0,234,4,4,4,4,4,44,8,6,7};
  PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,&VubBuffer,sizeof(VubBuffer));
  Vs32Return=pdd_get_data_stream_size((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,PDD_LOCATION_NOR_USER);
  EXPECT_EQ((sizeof(VubBuffer)-sizeof(tsPddFileHeader)), Vs32Return);
}
TEST_F(PddTest, pdd_get_data_stream_size_LocationScc_Size)
{
  tS32 Vs32Return;  
  Vs32Return=pdd_get_data_stream_size((char*)PDD_UTEST_ACCESS_SCC_VALID,PDD_LOCATION_SCC);
  EXPECT_GE(Vs32Return,0);/*val1 >= val2*/
}
/**********************  pdd_read_data_stream *************************************************/
TEST_F(PddTest, pdd_read_data_stream_NameNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_read_data_stream(NULL,PDD_LOCATION_FS,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_read_data_stream_WrongLocation_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_read_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_LAST,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_read_data_stream_BufferNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_read_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)NULL,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_read_data_stream_SizeWrong_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_read_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)vpBufferFile,-1,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_read_data_stream_InvalidDataStreamFile_ERROR_READ_NO_VALID_DATA_STREAM)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="test_invalid";
  Vs32Return=pdd_read_data_stream(VcDataStreamName,PDD_LOCATION_FS,(tU8*)vpBufferFile,100,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_READ_NO_VALID_DATA_STREAM,Vs32Return);
}
TEST_F(PddTest,pdd_read_data_stream_InvalidDataStreamNOR_USER_Assert)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="test_invalid";
  //EXPECT_CALL(vPddMockOutObj,vAssertFunction(_,_,_)).Times(AtLeast(1));	
  Vs32Return=pdd_read_data_stream(VcDataStreamName,PDD_LOCATION_NOR_USER,(tU8*)vpBufferFile,100,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_READ_NO_VALID_DATA_STREAM,Vs32Return);
}

TEST_F(PddTest,pdd_read_data_stream_BackupNormalInvalidFile_ERROR_READ_NO_VALID_DATA_STREAM)
{
  tS32 Vs32Return;
  /*write data*/
  memset(vpBufferFile,0xa5,PDD_UTEST_ACCESS_FILE_SIZE_TEST);	
  Vs32Return=pdd_write_data_stream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  ASSERT_GE(Vs32Return,0);/*GE val1 > val2*/
  /*change files*/
  memset(vpBufferFile,0x22,PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  Vs32Return=PDD_FileWriteDataStream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
  ASSERT_GE(Vs32Return,0);/*GE val1 > val2*/
  memset(vpBufferFile,0x11,PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  Vs32Return=PDD_FileWriteDataStream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
  ASSERT_GE(Vs32Return,0);/*GE val1 > val2*/
  /* call read function*/
  Vs32Return=pdd_read_data_stream((char*)PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_READ_NO_VALID_DATA_STREAM,Vs32Return);
}
/**********************  pdd_write_data_stream *************************************************/
TEST_F(PddTest, pdd_write_data_stream_NameNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_write_data_stream(NULL,PDD_LOCATION_FS,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_write_data_stream_WrongLocation_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_write_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_LAST,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_write_data_stream_BufferNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_write_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)NULL,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_write_data_stream_SizeWrong_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_write_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)vpBufferFile,-1,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_write_data_stream_WrongVersion_Assert)
{
  tS32 Vs32Return;
  /*write data*/
  Vs32Return=pdd_write_data_stream((char*)PDD_UTEST_ACCESS_SCC_VALID,PDD_LOCATION_SCC,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_SCC_SIZE_TEST,PDD_UTEST_VERSION);
  EXPECT_EQ(PDD_ERROR_WRITE_NO_DATA_STREAM,Vs32Return);
}
TEST_F(PddTest,pdd_write_data_stream_TwoIndenticalStreamsForFileAccess_Size0)
{
  tS32 Vs32Return;
  memset(vpBufferFile,0x23,PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  Vs32Return=pdd_write_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  ASSERT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST,Vs32Return);
  Vs32Return=pdd_write_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  EXPECT_EQ(0,Vs32Return);
}
TEST_F(PddTest,pdd_write_data_stream_WriteReadCompareForFileAccess_DataStreamsEqual)
{
  tS32  Vs32Return;
  int   ViReturnValueMemcmp;
  void* VpReadBuffer=malloc(PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  memset(vpBufferFile,0x23,PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  Vs32Return=pdd_write_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  ASSERT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST,Vs32Return);
  Vs32Return=pdd_read_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_FS,(tU8*)VpReadBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  ASSERT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST,Vs32Return);
  ViReturnValueMemcmp=memcmp(vpBufferFile,VpReadBuffer,Vs32Return);
  EXPECT_EQ(0,ViReturnValueMemcmp);
  free(VpReadBuffer);
}

TEST_F(PddTest,pdd_write_data_stream_WriteReadCompareForNOR_USERAccess_DataStreamsEqual)
{
  tS32  Vs32Return;
  int   ViReturnValueMemcmp;
  void* VpReadBuffer=malloc(PDD_UTEST_ACCESS_NOR_USER_SIZE_TEST);
  memset(vpBufferFile,0xa5,PDD_UTEST_ACCESS_NOR_USER_SIZE_TEST);	
  Vs32Return=pdd_write_data_stream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,PDD_LOCATION_NOR_USER,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_NOR_USER_SIZE_TEST,PDD_UTEST_VERSION);
  ASSERT_EQ(PDD_UTEST_ACCESS_NOR_USER_SIZE_TEST,Vs32Return);
  Vs32Return=pdd_read_data_stream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,PDD_LOCATION_NOR_USER,(tU8*)VpReadBuffer,PDD_UTEST_ACCESS_NOR_USER_SIZE_TEST,PDD_UTEST_VERSION);
  ASSERT_EQ(PDD_UTEST_ACCESS_NOR_USER_SIZE_TEST,Vs32Return);
  ViReturnValueMemcmp=memcmp(vpBufferFile,VpReadBuffer,Vs32Return);
  EXPECT_EQ(0,ViReturnValueMemcmp);
  free(VpReadBuffer);
}
/**********************  pdd_delete_data_stream *************************************************/
TEST_F(PddTest, pdd_delete_data_stream_NameNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_delete_data_stream(NULL,PDD_LOCATION_FS);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_delete_data_stream_WrongLocation_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=pdd_delete_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_LAST);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_delete_data_stream_LocationNorUser_ERROR_NOT_SUPPORTED)
{
  tS32 Vs32Return;
  Vs32Return=pdd_delete_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_NOR_USER);
  EXPECT_EQ(PDD_ERROR_NOT_SUPPORTED,Vs32Return);
}
TEST_F(PddTest,pdd_delete_data_stream_LocationNorKernel_ERROR_NOT_SUPPORTED)
{
  tS32 Vs32Return;
  Vs32Return=pdd_delete_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_NOR_KERNEL);
  EXPECT_EQ(PDD_ERROR_NOT_SUPPORTED,Vs32Return);
}
TEST_F(PddTest,pdd_delete_data_stream_LocationSCC_ERROR_NOT_SUPPORTED)
{
  tS32 Vs32Return;
  Vs32Return=pdd_delete_data_stream((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_LOCATION_SCC);
  EXPECT_EQ(PDD_ERROR_NOT_SUPPORTED,Vs32Return);
}
TEST_F(PddTest,pdd_delete_data_stream_DataStreamNotExist_ERROR_DATA_STREAM_DOESNOTEXIST)
{
  tS32 Vs32Return;
  Vs32Return=pdd_delete_data_stream("DataStreamNotExist",PDD_LOCATION_FS);
  EXPECT_EQ(PDD_ERROR_DATA_STREAM_DOESNOTEXIST,Vs32Return);
}

TEST_F(PddTest,pdd_delete_data_stream_LocationFS_PDD_OK)
{
  tS32  Vs32Return;
  int   ViReturnValueMemcmp;
  memset(vpBufferFile,0x44,PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  //write stream
  Vs32Return=pdd_write_data_stream("deletestream1",PDD_LOCATION_FS,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  ASSERT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST,Vs32Return);
  //delete stream
  Vs32Return=pdd_delete_data_stream("deletestream1",PDD_LOCATION_FS);
  EXPECT_EQ(PDD_OK,Vs32Return);
}

TEST_F(PddTest,pdd_delete_data_stream_LocationFSSecure_PDD_OK)
{
  tS32  Vs32Return;
  int   ViReturnValueMemcmp;
  memset(vpBufferFile,0x44,PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  //write stream
  Vs32Return=pdd_write_data_stream("deletestream2",PDD_LOCATION_FS_SECURE,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION);
  ASSERT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST,Vs32Return);
  //delete stream
  Vs32Return=pdd_delete_data_stream("deletestream2",PDD_LOCATION_FS_SECURE);
  EXPECT_EQ(PDD_OK,Vs32Return);
}

/**********************  pdd_read_datastream_early_from_nor *************************************************/
TEST_F(PddTest, pdd_read_datastream_early_from_nor_NameNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  tU8  Vu8Info=0;
  Vs32Return=pdd_read_datastream_early_from_nor(NULL,(tU8*)vpBufferFile,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION,&Vu8Info);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}

TEST_F(PddTest,pdd_read_datastream_early_from_nor_BufferNull_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  tU8  Vu8Info=0;
  Vs32Return=pdd_read_datastream_early_from_nor((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,(tU8*)NULL,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_UTEST_VERSION,&Vu8Info);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}
TEST_F(PddTest,pdd_read_datastream_early_from_nor_SizeWrong_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  tU8  Vu8Info=0;
  Vs32Return=pdd_read_datastream_early_from_nor((char*) PDD_UTEST_ACCESS_FILE_NAME_TEST,(tU8*)vpBufferFile,-1,PDD_UTEST_VERSION,&Vu8Info);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER,Vs32Return);
}

TEST_F(PddTest,pdd_read_datastream_early_from_nor_InvalidDataStreamNOR_USER_Assert)
{
  tS32 Vs32Return;
  tU8  Vu8Info=0;
  char VcDataStreamName[]="test_invalid";
  //EXPECT_CALL(vPddMockOutObj,vAssertFunction(_,_,_)).Times(AtLeast(1));	
  Vs32Return=pdd_read_datastream_early_from_nor(VcDataStreamName,(tU8*)vpBufferFile,100,PDD_UTEST_VERSION,&Vu8Info);
  EXPECT_EQ(PDD_ERROR_READ_NO_VALID_DATA_STREAM,Vs32Return);
}



/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
