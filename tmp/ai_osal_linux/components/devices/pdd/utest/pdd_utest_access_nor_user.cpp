/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        These are the unit tests for pdd_access_nor_user
 * @addtogroup   PDD unit test
 * @{
 */
/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "dgram_service.h"
#include "pdd.h"
#include "LFX2.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#include "pdd_mockout.h"

#define PDD_UTEST_ACCESS_NOR_USER_NAME_KDS              "kds_data"
#define PDD_UTEST_ACCESS_NOR_USER_NAME_EOL              "config_data"

/* define sizes*/
#define PDD_UTEST_ACCESS_NOR_USER_CLUSTER_SIZE                 PDD_NOR_USER_CLUSTER_SIZE
#define PDD_UTEST_ACCESS_NOR_USER_HEADER_SIZE                 (PDD_NOR_USER_MAX_WRITE_POSITION*PDD_UTEST_ACCESS_NOR_USER_CHUNK_SIZE)
#define PDD_UTEST_ACCESS_NOR_USER_DATA_STREAM_HEADER_SIZE     (2*sizeof(tsPddNorUserHeaderDataStream))
#define PDD_UTEST_ACCESS_NOR_USER_DATA_SIZE_MAX               (PDD_NOR_USER_CLUSTER_SIZE-PDD_UTEST_ACCESS_NOR_USER_HEADER_SIZE-PDD_UTEST_ACCESS_NOR_USER_DATA_STREAM_HEADER_SIZE)-1
/* define number cluster*/
#define PDD_UTEST_ACCESS_NOR_USER_NUMBER_CLUSTER              8//TODO change(PDD_UTEST_ACCESS_NOR_USER_NUM_BLOCKS*(PDD_UTEST_ACCESS_NOR_USER_BLOCK_SIZE/PDD_UTEST_ACCESS_NOR_USER_CLUSTER_SIZE))

using ::testing::Return;
using ::testing::Exactly; 
using ::testing::_;
using ::testing::StrEq;
using ::testing::NotNull;
using ::testing::IsNull;
using ::testing::AtLeast;
using ::testing::Pointee;

/**********************  PddAccessNorUserTest *************************************************/
class PddAccessNorUserTest : public ::testing::Test 
{
public:
	PddAccessNorUserTest() 
	{/* You can do set-up work for each test here.*/
	  
    }
	virtual ~PddAccessNorUserTest() 
	{/* You can do clean-up work that doesn't throw exceptions here.*/
	 
    }

protected:
	virtual void SetUp() 
	{ 
	  /*Code here will be called immediately after the constructor (rightbefore each test).*/ 
	  EXPECT_CALL(vPddMockOutObj,LFX_open(StrEq("PDD_UserEarly")))
	    .WillRepeatedly(Return(vhLfx));
	  EXPECT_CALL(vPddMockOutObj,LFX_read(NotNull(),NotNull(),_,_))	    
		.WillRepeatedly(Return(0));
      EXPECT_CALL(vPddMockOutObj,LFX_write(NotNull(),NotNull(),_,_))
		.WillRepeatedly(Return(0));
      EXPECT_CALL(vPddMockOutObj,LFX_erase(NotNull(),_,_))
        .WillRepeatedly(Return(0));
	  EXPECT_CALL(vPddMockOutObj,LFX_close(NotNull()))
	    .Times(AtLeast(0));
	 EXPECT_CALL(vPddMockOutObj,vConsoleTrace(_,_,_)).Times(AtLeast(0));
	 PDD_NorUserAccessInitProcess();
	 PDD_NorUserAccessInit();	
	}
	virtual void TearDown() 
	{ /*Code here will be called immediately after each test (right before the destructor).*/
	  EXPECT_CALL(vPddMockOutObj,LFX_close(NotNull()))
	    .Times(AtLeast(0));
      PDD_NorUserAccessDeInitProcess();
	  PDD_NorUserAccessDeInit();
	}
	/* Objects declared here can be used by all tests in the test case for PddAccessNorUserTest.*/    
	LFXhandle vhLfx;
};
/**********************  PDD_NorUserAccessGetDataStreamSize *************************************************/
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessGetDataStreamSize_InvalidDataStream_PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="test_invalid";
  Vs32Return=PDD_NorUserAccessGetDataStreamSize(VcDataStreamName);
  EXPECT_EQ(PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED, Vs32Return);
}
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessGetDataStreamSize_NoDataWritten_Size0)
{
  tS32 Vs32Return;
  Vs32Return=PDD_NorUserAccessGetDataStreamSize((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS);
  EXPECT_EQ(0, Vs32Return);
}
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessGetDataStreamSize_NoDataWrittenReadTwice_Size0)
{
  tS32 Vs32Return;
  EXPECT_CALL(vPddMockOutObj,LFX_read(NotNull(),NotNull(),_,_))
	  .Times(Exactly(17));
  /* read first configuration(size) from buffer(flash read exacly 8 sectors => set configuartion to default*/
  PDD_NorUserAccessGetDataStreamSize((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS);
  /* read configuration done no read access nessecary */
  Vs32Return=PDD_NorUserAccessGetDataStreamSize((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS);
  EXPECT_EQ(0, Vs32Return);
}
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessGetDataStreamSize_DataWritten_SizeWrite)
{
  tS32 Vs32Return;
  tU8  VubBuffer[]={1,3,45,5,7,8,9,0,1,1,1,1,1,0,234,4,4,4,4,4,44,8,6,7};
  PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,&VubBuffer,sizeof(VubBuffer));
  Vs32Return=PDD_NorUserAccessGetDataStreamSize((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS);
  EXPECT_EQ(sizeof(VubBuffer), Vs32Return);
}
/**********************  PDD_NorUserAccessErase *************************************************/
TEST_F(PddAccessNorUserTest,PDD_NorUserAccessErase_EraseAll_OK)
{
  tS32 Vs32Return;
  Vs32Return=PDD_NorUserAccessErase();
  EXPECT_EQ(PDD_OK,Vs32Return);
}
TEST_F(PddAccessNorUserTest,PDD_NorUserAccessErase_LfxEraseError_PDD_ERROR_NOR_USER_ERASE_FAIL)
{
  tS32 Vs32Return;
  EXPECT_CALL(vPddMockOutObj,LFX_erase(NotNull(),_,_))
	    .Times(2)
		.WillRepeatedly(Return(-1)); 
  Vs32Return=PDD_NorUserAccessErase();
  EXPECT_EQ(PDD_ERROR_NOR_USER_ERASE_FAIL,Vs32Return);
}
/**********************  PDD_NorUserAccessReadDataStream *******************************************/
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessReadDataStream_InvalidDataStream_PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED)
{
  tS32 Vs32Return;
  tU8  VubBuffer[24];
  char VcDataStreamName[]="test_invalid";
  Vs32Return=PDD_NorUserAccessReadDataStream(VcDataStreamName,&VubBuffer,sizeof(VubBuffer));
  EXPECT_EQ(PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED,Vs32Return);
}
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessReadDataStream_ReadBufferToSmall_ERROR_NOR_USER_WRONG_SIZE)
{
  tS32 Vs32Return;
  tU8  VubReadBuffer[2];
  tU8  VubBuffer[]={1,3,45,5,7,8,9,0,1,1,1,1,1,0,234,4,4,4,4,4,44,8,6,7};
  PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,&VubBuffer,sizeof(VubBuffer));
  Vs32Return=PDD_NorUserAccessReadDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,&VubReadBuffer,sizeof(VubReadBuffer));
  EXPECT_EQ(PDD_ERROR_NOR_USER_WRONG_SIZE,Vs32Return);
}
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessReadDataStream_WriteReadCheckSize_SizeWrite)
{
  tS32 Vs32Return;
  tU8  VubWriteBuffer[]={1,3,45,5,7,8,9,0,1,1,1,1,1,0,234,4,4,4,4,4,44,8,6,7};
  tU8  VubReadBuffer[sizeof(VubWriteBuffer)];

  Vs32Return=PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,&VubWriteBuffer,sizeof(VubWriteBuffer));
  Vs32Return=PDD_NorUserAccessReadDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,&VubReadBuffer,sizeof(VubReadBuffer));
  EXPECT_EQ(sizeof(VubWriteBuffer),Vs32Return);
}
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessReadDataStream_WriteReadCheckData_EqualData)
{
  tS32 Vs32Return;
  int  ViReturnValueMemcmp;
  tU8  VubWriteBuffer[]={1,3,45,5,7,8,9,0,1,1,1,1,5,6,234,8,4,4,9,4,44,8,6,7};
  tU8  VubReadBuffer[sizeof(VubWriteBuffer)];

  PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,&VubWriteBuffer,sizeof(VubWriteBuffer));
  Vs32Return=PDD_NorUserAccessReadDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,&VubReadBuffer,sizeof(VubReadBuffer));
  ViReturnValueMemcmp=memcmp(VubWriteBuffer,VubReadBuffer,Vs32Return);
  EXPECT_EQ(0,ViReturnValueMemcmp);
}
/**********************  PDD_NorUserAccessWriteDataStream*******************************************/
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessWriteDataStream_InvalidDataStream_PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED)
{
  tS32 Vs32Return;
  tU8  VubBuffer[24];
  char VcDataStreamName[]="test_invalid";	
  Vs32Return=PDD_NorUserAccessWriteDataStream(VcDataStreamName,&VubBuffer,sizeof(VubBuffer));
  EXPECT_EQ(PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED,Vs32Return);
}
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessWriteDataStream_SizeMax_OK)
{
  tS32 Vs32Return;
  tS32 Vs32SizeMax=(tS32)PDD_UTEST_ACCESS_NOR_USER_DATA_SIZE_MAX;
  tU8* VubpWriteBufferEOL=(tU8*)malloc(Vs32SizeMax);
  
  Vs32Return=PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_EOL,VubpWriteBufferEOL,Vs32SizeMax);
  free(VubpWriteBufferEOL);
  EXPECT_EQ(Vs32SizeMax,Vs32Return);
}
TEST_F(PddAccessNorUserTest, PDD_NorUserAccessWriteDataStream_SizeToGreat_PDD_ERROR_NOR_USER_NO_SPACE_CLUSTER_FULL)
{
  tS32 Vs32Return;
  tS32 Vs32SizeMax=(tS32)PDD_UTEST_ACCESS_NOR_USER_DATA_SIZE_MAX+1;
  tU8* VubpWriteBufferEOL=(tU8*)malloc(Vs32SizeMax);
   
  Vs32Return=PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_EOL,&VubpWriteBufferEOL,Vs32SizeMax);
  free(VubpWriteBufferEOL);
  EXPECT_EQ(PDD_ERROR_NOR_USER_NO_SPACE_CLUSTER_FULL,Vs32Return);
}
TEST_F(PddAccessNorUserTest,PDD_NorUserAccessWriteDataStream_WriteAllCluster_Size)
{
  tS32 Vs32Return;
  tS32 Vs32SizeMax=(tS32)PDD_UTEST_ACCESS_NOR_USER_DATA_SIZE_MAX;
  tU8* VubpWriteBufferEOL=(tU8*)malloc(Vs32SizeMax);
  tU8  VubInc;
  
  for(VubInc=0;VubInc<PDD_UTEST_ACCESS_NOR_USER_NUMBER_CLUSTER;VubInc++)
  {
    memset(VubpWriteBufferEOL,VubInc+0xa7,Vs32SizeMax);
    Vs32Return=PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_EOL,VubpWriteBufferEOL,Vs32SizeMax);
  }
  free(VubpWriteBufferEOL);
  EXPECT_EQ(Vs32SizeMax,Vs32Return);
}
TEST_F(PddAccessNorUserTest,PDD_NorUserAccessWriteDataStream_WriteSameData_OK)
{
  tS32 Vs32Return;
  tS32 Vs32SizeMax=(tS32)PDD_UTEST_ACCESS_NOR_USER_DATA_SIZE_MAX;
  tU8* VubpWriteBufferEOL=(tU8*)malloc(Vs32SizeMax);
  tU8  VubInc;
  
  memset(VubpWriteBufferEOL,0x55,Vs32SizeMax);
  for(VubInc=0;VubInc<PDD_UTEST_ACCESS_NOR_USER_NUMBER_CLUSTER;VubInc++)
  {    
    Vs32Return=PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_EOL,VubpWriteBufferEOL,Vs32SizeMax);
  }
  free(VubpWriteBufferEOL); 
  EXPECT_EQ(PDD_OK,Vs32Return);
}
TEST_F(PddAccessNorUserTest,PDD_NorUserAccessWriteDataStream_WriteTwoDataStreamKdsEol_Size)
{
  tS32 Vs32Return;
  tS32 Vs32Size=200;
  tU8* VubpWriteBufferEOL=(tU8*)malloc(Vs32Size);
  tU8* VubpWriteBufferKDS=(tU8*)malloc(Vs32Size);
  
  memset(VubpWriteBufferEOL,0x55,Vs32Size);  
  memset(VubpWriteBufferEOL,0xaa,Vs32Size);  
  Vs32Return=PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_KDS,VubpWriteBufferKDS,Vs32Size);
  ASSERT_EQ(Vs32Return,Vs32Size);
  Vs32Return=PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_EOL,VubpWriteBufferEOL,Vs32Size);
  free(VubpWriteBufferEOL); 
  free(VubpWriteBufferKDS); 
  EXPECT_EQ(Vs32Size,Vs32Return);
}
TEST_F(PddAccessNorUserTest,PDD_NorUserAccessWriteDataStream_WriteReadCmpDataForAllCluster_Size)
{
  tS32 Vs32Return;
  tS32 Vs32SizeMax=(tS32)PDD_UTEST_ACCESS_NOR_USER_DATA_SIZE_MAX;
  tU8* VubpWriteBufferEOL=(tU8*)malloc(Vs32SizeMax);
  tU8* VubpReadBufferEOL=(tU8*)malloc(Vs32SizeMax);
  tU8  VubInc;
  int  ViReturnValueMemcmp;
  
  for(VubInc=0;VubInc<PDD_UTEST_ACCESS_NOR_USER_NUMBER_CLUSTER;VubInc++)
  { 
    memset(VubpWriteBufferEOL,VubInc+0x55,Vs32SizeMax);
    Vs32Return=PDD_NorUserAccessWriteDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_EOL,VubpWriteBufferEOL,Vs32SizeMax);
	ASSERT_EQ(Vs32Return,Vs32SizeMax);
	Vs32Return=PDD_NorUserAccessReadDataStream((char*)PDD_UTEST_ACCESS_NOR_USER_NAME_EOL,VubpReadBufferEOL,Vs32SizeMax);
	ASSERT_EQ(Vs32Return,Vs32SizeMax);
	ViReturnValueMemcmp=memcmp(VubpWriteBufferEOL,VubpReadBufferEOL,Vs32SizeMax);
	if(ViReturnValueMemcmp!=0) break;
  }
  free(VubpWriteBufferEOL); 
  free(VubpReadBufferEOL); 
  EXPECT_EQ(0,ViReturnValueMemcmp);
}
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
