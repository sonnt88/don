/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        These are the unit tests for pdd_access_nor_kernel
 * @addtogroup   PDD unit test
 * @{
 */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "dgram_service.h"
#include "pdd.h"
#include "LFX2.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#include "pdd_mockout.h"
#ifdef PDD_NOR_KERNEL_POOL_EXIST  
#include "DataPoolKernel.h"
#include "pdd_config_nor_kernel.h"
#endif

using ::testing::Return;
using ::testing::Exactly; 
using ::testing::_;
using ::testing::StrEq;
using ::testing::NotNull;
using ::testing::IsNull;
using ::testing::AtLeast;
using ::testing::Pointee;

/**********************  PddAccessNorKernelTest *************************************************/
class PddAccessNorKernelTest : public ::testing::Test 
{
public:
	PddAccessNorKernelTest() 
	{/* You can do set-up work for each test here.*/
	  
    }
	virtual ~PddAccessNorKernelTest() 
	{/* You can do clean-up work that doesn't throw exceptions here.*/
	 
    }

protected:
	virtual void SetUp() 
	{ /*Code here will be called immediately after the constructor (rightbefore each test).*/  	 
	  EXPECT_CALL(vPddMockOutObj,LFX_open(StrEq("PDD_Kernel")))
	    .WillRepeatedly(Return(vhLfx));
	  EXPECT_CALL(vPddMockOutObj,LFX_read(NotNull(),NotNull(),_,_))
		.WillRepeatedly(Return(0));
      EXPECT_CALL(vPddMockOutObj,LFX_write(NotNull(),NotNull(),_,_))
		.WillRepeatedly(Return(0));
      EXPECT_CALL(vPddMockOutObj,LFX_erase(NotNull(),_,_))
        .WillRepeatedly(Return(0));
	  EXPECT_CALL(vPddMockOutObj,LFX_close(NotNull()))
	    .Times(AtLeast(0));
	  EXPECT_CALL(vPddMockOutObj,vConsoleTrace(_,_,_)).Times(AtLeast(0));
	  PDD_NorKernelAccessInitProcess();
	  PDD_NorKernelAccessInit();
	}
	virtual void TearDown() 
	{ /*Code here will be called immediately after each test (right before the destructor).*/
	  EXPECT_CALL(vPddMockOutObj,LFX_close(NotNull()))
	    .Times(AtLeast(0));
	  PDD_NorKernelAccessDeInitProcess();
	  PDD_NorKernelAccessDeInit();
	}
	/* Objects declared here can be used by all tests in the test case for PddAccessNorKernelTest.*/    
	LFXhandle vhLfx;
};
#ifdef PDD_NOR_KERNEL_POOL_EXIST  
/**********************  PDD_NorKernelAccessGetDataStreamSize *************************************************/
TEST_F(PddAccessNorKernelTest, PDD_NorKernelAccessGetDataStreamSize_NoDataWritten_Size0)
{
  tS32 Vs32Return;
  
  Vs32Return=PDD_NorKernelAccessGetDataStreamSize();
  EXPECT_EQ(0, Vs32Return);
}
TEST_F(PddAccessNorKernelTest, PDD_NorKernelAccessGetDataStreamSize_DataWritten_SizeWrite)
{
  tS32                          Vs32Return;
  void*                         VvpBuffer=malloc(sizeof(TPddKernel_Header)+sizeof(TDataPoolKernel_DpPddKernel));
  tU8*                          Vu8pBuffer=(tU8*)VvpBuffer;
  TDataPoolKernel_DpPddKernel*  VtsData=(TDataPoolKernel_DpPddKernel*) (Vu8pBuffer+sizeof(TPddKernel_Header));
  memset(&VtsData->DisplayTiming.DisplayTiming[0],0xff,sizeof(TDataPoolKernel_DisplayTiming));
  PDD_NorKernelAccessWriteDataStream(VvpBuffer,(sizeof(TPddKernel_Header)+sizeof(TDataPoolKernel_DpPddKernel)));
  Vs32Return=PDD_NorKernelAccessGetDataStreamSize();
  EXPECT_EQ(sizeof(TPddKernel_Header)+sizeof(TDataPoolKernel_DpPddKernel), Vs32Return);
  free(VvpBuffer);
}
#endif
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
