/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        These are the unit tests for pdd_access_file
 * @addtogroup   PDD unit test
 * @{
 */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>
#include <dirent.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "dgram_service.h"
#include "pdd.h"
#include "LFX2.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#include "pdd_mockout.h"
#include "DataPoolSCC.h" 


#define PDD_UTEST_ACCESS_FILE_NAME_TEST  "utestfile"
#define PDD_UTEST_ACCESS_FILE_SIZE_TEST  500
#define PDD_UTEST_ACCESS_FILE_SIZE_NAME   sizeof(PDD_UTEST_ACCESS_FILE_NAME_TEST)

using ::testing::Return;
using ::testing::AtLeast; 
using ::testing::_;

/**********************  PddAccessFileTest *************************************************/
class PddAccessFileTest : public ::testing::Test 
{
public:
	PddAccessFileTest() 
	{/* You can do set-up work for each test here.*/
	  DIR   *VpDir=NULL;
	  VpDir=opendir(PDD_FILE_DEFAULT_PATH);
    if(VpDir==NULL)
    {/*create path, if cannot open*/
         mkdir("/tmp/var/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
		     mkdir("/tmp/var/opt/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
		     mkdir("/tmp/var/opt/bosch/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
		     mkdir("/tmp/var/opt/bosch/dynamic/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
		     mkdir("/tmp/var/opt/bosch/persistent/", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
	  }
	  EXPECT_CALL(vPddMockOutObj,vConsoleTrace(_,_,_)).Times(AtLeast(0));
  }
	virtual ~PddAccessFileTest() 
	{/* You can do clean-up work that doesn't throw exceptions here.*/
	 
    }

protected:
	virtual void SetUp() 
	{ /* Code here will be called immediately after the constructor (rightbefore each test).*/      
    PDD_IsInit(PDD_LOCATION_FS,"Test");
	  PDD_IsInit(PDD_LOCATION_FS_SECURE,"Test");
	  /* -------------  create a test file --------------------*/
	  /* allocata buffer */
	  vpBuffer=malloc(PDD_UTEST_ACCESS_FILE_SIZE_TEST);
	  /* set buffer */
	  memset(vpBuffer,0xa5,PDD_UTEST_ACCESS_FILE_SIZE_TEST);
	  /* set name */
      strncpy(vcDataStreamName,PDD_UTEST_ACCESS_FILE_NAME_TEST,PDD_UTEST_ACCESS_FILE_SIZE_NAME);
	  vcDataStreamName[PDD_UTEST_ACCESS_FILE_SIZE_NAME] = '\0';
	  /* save file to different location */
	  PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
	  PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
	  PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_FS_SECURE,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
	  PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_FS_SECURE,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
	  PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_NOR_USER,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
    PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_SCC,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
	}
	virtual void TearDown() 
	{ /*Code here will be called immediately after each test (right before the destructor).*/
      /*set buffer to free*/
	  free(vpBuffer);
	}
	/* Objects declared here can be used by all tests in the test case for PddAccessFileTest.*/   
	void* vpBuffer;
	char  vcDataStreamName[PDD_UTEST_ACCESS_FILE_SIZE_NAME];
};
/**********************  PDD_FileCreatePath *************************************************/
TEST_F(PddAccessFileTest, PDD_FileCreatePath_Valid_OK)
{
  tS32 Vs32Return=PDD_OK;
  Vs32Return=PDD_FileCreatePath(PDD_LOCATION_FS,"Test");
  EXPECT_EQ(PDD_OK, Vs32Return);
}
/**********************  PDD_FileGetDataStreamSize *************************************************/
TEST_F(PddAccessFileTest, PDD_FileGetDataStreamSize_LocationFileSystem_FILE_SIZE_TEST)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileGetDataStreamSize(vcDataStreamName,PDD_LOCATION_FS);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileGetDataStreamSize_LocationFileSystemSecure_FILE_SIZE_TEST)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileGetDataStreamSize(vcDataStreamName,PDD_LOCATION_FS_SECURE);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileGetDataStreamSize_LocationNorUser_FILE_SIZE_TEST)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileGetDataStreamSize(vcDataStreamName,PDD_LOCATION_NOR_USER);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileGetDataStreamSize_InvalidDataStream_ERROR_FILE_NO_VALID_SIZE)
{
  tS32 Vs32Return;
  char VcDataStreamName[]="test_invalid";
  Vs32Return=PDD_FileGetDataStreamSize(VcDataStreamName,PDD_LOCATION_FS);
  EXPECT_EQ(PDD_ERROR_FILE_NO_VALID_SIZE, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileGetDataStreamSize_DataStreamNameToGreat_ERROR_FILE_NO_VALID_SIZE)
{/*The VFS (Virtuele File system can not manage file name > 255*/
  tS32 Vs32Return;
  char VcDataStreamName[256];
  memset(&VcDataStreamName,'a',255);
  VcDataStreamName[255]=0;

  Vs32Return=PDD_FileGetDataStreamSize(VcDataStreamName,PDD_LOCATION_FS);
  EXPECT_EQ(PDD_ERROR_FILE_NO_VALID_SIZE, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileGetDataStreamSize_WrongLocation_ERROR_FILE_NO_VALID_SIZE)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileGetDataStreamSize(vcDataStreamName,PDD_LOCATION_LAST);
  EXPECT_EQ(PDD_ERROR_FILE_NO_VALID_SIZE, Vs32Return);
}
/**********************  PDD_FileReadDataStream *************************************************/
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationFileSystemKindNormal_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationFileSystemKindBackup_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationFileSystemSecureKindNormal_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_FS_SECURE,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationFileSystemSecureKindBackup_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_FS_SECURE,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationNorUserKindNormal_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_NOR_USER,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationNorUserKindBackup_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_NOR_USER,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationSccKindNormal_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_SCC,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationFileSystemKindNormal_EqualData)
{
  tS32      Vs32Return;
  void*     VpBufferRead=malloc(PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  int       ViReturnValueMemcmp;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_FS,VpBufferRead,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL);
  ViReturnValueMemcmp=memcmp(VpBufferRead,vpBuffer,Vs32Return);
  free(VpBufferRead);
  EXPECT_EQ(0, ViReturnValueMemcmp);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationFileSystemKindBackup_EqualData)
{
  tS32      Vs32Return;
  void*     VpBufferRead=malloc(PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  int       ViReturnValueMemcmp;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_FS,VpBufferRead,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP);
  ViReturnValueMemcmp=memcmp(VpBufferRead,vpBuffer,Vs32Return);
  free(VpBufferRead);
  EXPECT_EQ(0, ViReturnValueMemcmp);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationFileSystemSecureKindNormal_EqualData)
{
  tS32      Vs32Return;
  void*     VpBufferRead=malloc(PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  int       ViReturnValueMemcmp;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_FS_SECURE,VpBufferRead,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL);
  ViReturnValueMemcmp=memcmp(VpBufferRead,vpBuffer,Vs32Return);
  free(VpBufferRead);
  EXPECT_EQ(0, ViReturnValueMemcmp);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationFileSystemSecureKindBackup_EqualData)
{
  tS32      Vs32Return;
  void*     VpBufferRead=malloc(PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  int       ViReturnValueMemcmp;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_FS_SECURE,VpBufferRead,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP);
  ViReturnValueMemcmp=memcmp(VpBufferRead,vpBuffer,Vs32Return);
  free(VpBufferRead);
  EXPECT_EQ(0, ViReturnValueMemcmp);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_LocationNorUserKindBackup_EqualData)
{
  tS32      Vs32Return;
  void*     VpBufferRead=malloc(PDD_UTEST_ACCESS_FILE_SIZE_TEST);
  int       ViReturnValueMemcmp;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_NOR_USER,VpBufferRead,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP);
  ViReturnValueMemcmp=memcmp(VpBufferRead,vpBuffer,Vs32Return);
  free(VpBufferRead);
  EXPECT_EQ(0, ViReturnValueMemcmp);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_InvalidDataStream_ERROR_FILE_OPEN)
{
  tS32  Vs32Return;
  char  VcDataStreamNameInvalid[]="invalid_name";
  Vs32Return=PDD_FileReadDataStream(VcDataStreamNameInvalid,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL);
  EXPECT_EQ(PDD_ERROR_FILE_OPEN, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_ReadBufferToSmall_ERROR_FILE_READ_BUFFER_TO_SMALL)
{
  tS32  Vs32Return;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST-100,PDD_KIND_FILE_NORMAL);
  EXPECT_EQ(PDD_ERROR_FILE_READ_BUFFER_TO_SMALL, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_DataStreamNameToGreat_ERROR_WRONG_PARAMETER)
{/*The VFS (Virtuele File system can not manage file name > 255*/
  tS32 Vs32Return;
  char VcDataStreamName[256];
  memset(&VcDataStreamName,'a',255);
  VcDataStreamName[255]=0;
  Vs32Return=PDD_FileReadDataStream(VcDataStreamName,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileReadDataStream_WrongLocation_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileReadDataStream(vcDataStreamName,PDD_LOCATION_LAST,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}
/**********************  PDD_FileWriteDataStream *************************************************/
TEST_F(PddAccessFileTest, PDD_FileWriteDataStream_LocationFileSystemKindNormal_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileWriteDataStream_LocationFileSystemKindBackup_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileWriteDataStream_LocationFileSystemSecureKindNormal_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_FS_SECURE,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileWriteDataStream_LocationFileSystemSecureKindBackup_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_FS_SECURE,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileWriteDataStream_LocationNorUserKindNormal_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_NOR_USER,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileWriteDataStream_LocationNorUserKindBackup_EqualSize)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_NOR_USER,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_BACKUP,TRUE);
  EXPECT_EQ(PDD_UTEST_ACCESS_FILE_SIZE_TEST, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileWriteDataStream_LocationSccKindNormal_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_SCC,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileWriteDataStream_DataStreamNameToGreat_ERROR_WRONG_PARAMETER)
{/*The VFS (Virtuele File system can not manage file name > 255*/
  tS32 Vs32Return;
  char VcDataStreamName[256];
  memset(&VcDataStreamName,'a',255);
  VcDataStreamName[255]=0;
  Vs32Return=PDD_FileWriteDataStream(VcDataStreamName,PDD_LOCATION_FS,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}
TEST_F(PddAccessFileTest, PDD_FileWriteDataStream_WrongLocation_ERROR_WRONG_PARAMETER)
{
  tS32 Vs32Return;
  Vs32Return=PDD_FileWriteDataStream(vcDataStreamName,PDD_LOCATION_LAST,vpBuffer,PDD_UTEST_ACCESS_FILE_SIZE_TEST,PDD_KIND_FILE_NORMAL,TRUE);
  EXPECT_EQ(PDD_ERROR_WRONG_PARAMETER, Vs32Return);
}
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
