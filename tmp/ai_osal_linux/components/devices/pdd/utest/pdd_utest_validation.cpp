/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        These are the unit tests for pdd_validation
 * @addtogroup   PDD unit test
 * @{
 */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "dgram_service.h"
#include "pdd.h"
#include "LFX2.h"
#include "pdd_config_nor_user.h"
#include "pdd_private.h"
#include "pdd_trace.h"
#include "pdd_mockout.h"
#include "DataPoolSCC.h" 
#include "DataPoolKernel.h" 

/*define for buffers 32*/
#define PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32  233
#define PDD_UTEST_VALIDATION_VERSION_32          0xa5a5

/*define for buffer 8*/
#define PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8    61
#define PDD_UTEST_VALIDATION_VERSION_8            0xa5


using ::testing::Return;
using ::testing::AtLeast; 
using ::testing::_;

/**********************  PddValidationTest *************************************************/
class PddValidationTest : public ::testing::Test 
{ 
public:
	PddValidationTest() 
	{/* You can do set-up work for each test here.*/
	  EXPECT_CALL(vPddMockOutObj,vConsoleTrace(_,_,_)).Times(AtLeast(0));	 
    }
	virtual ~PddValidationTest() 
	{/* You can do clean-up work that doesn't throw exceptions here.*/
	 
    }

protected:
	virtual void SetUp() 
	{ 
	  tU8*               Vu8pBuffer;
	  tU32               Vu32Inc=0;
	  tsPddFileHeader*   VspHeader32;
	  TPddKernel_Header* VspHeaderNorKernel;
	  TPddScc_Header*    VspHeaderScc;
	  tS32               Vs32SizeHeader32=sizeof(tsPddFileHeader);
	  tS32               Vs32SizeHeaderNorKernel=sizeof(TPddKernel_Header);
	  tS32               Vs32SizeHeader8=sizeof(TPddScc_Header);

	  /*Code here will be called immediately after the constructor (rightbefore each test).*/
	  /* for buffer with crc 32*/
	  vpDataStreamTestCRC32=malloc(PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32);
	  Vu8pBuffer=(tU8*)vpDataStreamTestCRC32;
	  /* set data */
	  while(Vu32Inc<PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32)
	  {
	    *Vu8pBuffer=(tU8)Vu32Inc;
		Vu8pBuffer++;
		Vu32Inc++;
	  }
	   /* set valid header*/
	  VspHeader32=(tsPddFileHeader*)vpDataStreamTestCRC32;
	  VspHeader32->u32Magic=PDD_C_DATA_STREAM_MAGIC;
      VspHeader32->u32Version=PDD_UTEST_VALIDATION_VERSION_32;
	  VspHeader32->u32Checksum=PDD_ValidationCalcCrc32((tU8*)vpDataStreamTestCRC32+Vs32SizeHeader32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeader32);

     /* for buffer nor kernel*/
	  vpDataStreamTestNorKernel=malloc(PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32);
	  Vu8pBuffer=(tU8*)vpDataStreamTestNorKernel;
	  /* set data */
	  while(Vu32Inc<PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32)
	  {
	    *Vu8pBuffer=(tU8)Vu32Inc;
		Vu8pBuffer++;
		Vu32Inc++;
	  }
	   /* set valid header*/	   
	  VspHeaderNorKernel=(TPddKernel_Header*)vpDataStreamTestNorKernel;
	  memset(VspHeaderNorKernel,0,sizeof(TPddKernel_Header));
	  memset(&VspHeaderNorKernel->HeaderValid,0xff,sizeof(TPddHeaderValid));
      VspHeaderNorKernel->HeaderValid.Valid=~PDD_NOR_KERNEL_STATE_VALID;
      VspHeaderNorKernel->HeaderStream.Version=PDD_UTEST_VALIDATION_VERSION_32;  
      VspHeaderNorKernel->HeaderStream.Checksum=PDD_ValidationCalcCrc32((tU8*)vpDataStreamTestNorKernel+Vs32SizeHeaderNorKernel,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeaderNorKernel);
  	  VspHeaderNorKernel->HeaderStream.ChecksumXor=~VspHeaderNorKernel->HeaderStream.Checksum;
      VspHeaderNorKernel->HeaderStream.Size=(tU32) PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32;
      VspHeaderNorKernel->HeaderStream.ChunkSize=0x20;
	  VspHeaderNorKernel->HeaderStream.Magic=PDD_NOR_KERNEL_MAGIC;
	  /* for buffer with crc 8*/
	  vpDataStreamTestCRCScc=malloc(PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8);
	  Vu8pBuffer=(tU8*)vpDataStreamTestCRCScc;
	  /* set data */
	  Vu32Inc=0;
	  while(Vu32Inc<PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8)
	  {
	    *Vu8pBuffer=(tU8)Vu32Inc;
		Vu8pBuffer++;
		Vu32Inc++;
	  }
	    /* set valid header*/
	  VspHeaderScc=(TPddScc_Header*)vpDataStreamTestCRCScc;
      VspHeaderScc->u32VersionPool=PDD_UTEST_VALIDATION_VERSION_8;
	  VspHeaderScc->u32Size=PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8-Vs32SizeHeader8;
	  VspHeaderScc->u32Checksum=PDD_ValidationCalcCrc32((tU8*)vpDataStreamTestCRCScc+Vs32SizeHeader8,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8-Vs32SizeHeader8);
	 
	}
	virtual void TearDown() 
	{ /*Code here will be called immediately after each test (right before the destructor).*/
	  /*set buffer to free*/
      free(vpDataStreamTestCRC32);
	  free(vpDataStreamTestCRCScc);
	  free(vpDataStreamTestNorKernel);
	}
	/* Objects declared here can be used by all tests in the test case for PddValidationTest.*/    
	void*  vpDataStreamTestCRC32;
	void*  vpDataStreamTestCRCScc;
	void*  vpDataStreamTestNorKernel;
};

/**********************  PDD_ValidationCheckFile *************************************************/
TEST_F(PddValidationTest, PDD_ValidationCheckFile_ValidStream_TRUE)
{
	tBool VbReturn=PDD_ValidationCheckFile(vpDataStreamTestCRC32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(TRUE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckFile_InvalidVersion_FALSE)
{
	tBool VbReturn;
	tsPddFileHeader*  VspHeader32=(tsPddFileHeader*)vpDataStreamTestCRC32;
	VspHeader32->u32Version=0x1234;
	VbReturn=PDD_ValidationCheckFile(vpDataStreamTestCRC32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckFile_InvalidMagic_FALSE)
{
	tBool VbReturn;
	tsPddFileHeader*  VspHeader32=(tsPddFileHeader*)vpDataStreamTestCRC32;
	VspHeader32->u32Magic=0x1234;
	VbReturn=PDD_ValidationCheckFile(vpDataStreamTestCRC32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckFile_InvalidChecksum_FALSE)
{
	tBool VbReturn;
	tsPddFileHeader*  VspHeader32=(tsPddFileHeader*)vpDataStreamTestCRC32;
	VspHeader32->u32Checksum=VspHeader32->u32Checksum-1;
	VbReturn=PDD_ValidationCheckFile(vpDataStreamTestCRC32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckFile_InvalidData_FALSE)
{
	tBool VbReturn;
	tU8*  Vu8pBuffer=(tU8*)vpDataStreamTestCRC32;
	tS32  Vs32SizeHeader=sizeof(tsPddFileHeader);
	*(Vu8pBuffer+Vs32SizeHeader) -=1;
	VbReturn=PDD_ValidationCheckFile(vpDataStreamTestCRC32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckFile_InvalidDataZero_FALSE)
{
	tBool VbReturn;
	tU8*  Vu8pBuffer=(tU8*)vpDataStreamTestCRC32;
	tS32  Vs32SizeHeader=sizeof(tsPddFileHeader);
	memset(Vu8pBuffer+Vs32SizeHeader,0x00,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeader);
	VbReturn=PDD_ValidationCheckFile(vpDataStreamTestCRC32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckFile_InvalidData0xFF_FALSE)
{
	tBool VbReturn;
	tU8*  Vu8pBuffer=(tU8*)vpDataStreamTestCRC32;
	tS32  Vs32SizeHeader=sizeof(tsPddFileHeader);
	memset(Vu8pBuffer+Vs32SizeHeader,0xff,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeader);
	VbReturn=PDD_ValidationCheckFile(vpDataStreamTestCRC32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
/**********************  PDD_ValidationCheckNorKernel *************************************************/
TEST_F(PddValidationTest, PDD_ValidationCheckNorKernel_ValidStream_TRUE)
{
	tBool VbReturn=PDD_ValidationCheckNorKernel(vpDataStreamTestNorKernel,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(TRUE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckNorKernel_InvalidVersion_FALSE)
{
	tBool VbReturn;
	TPddKernel_Header*  VspHeader=(TPddKernel_Header*)vpDataStreamTestNorKernel;
	VspHeader->HeaderStream.Version=0x1234;
	VbReturn=PDD_ValidationCheckNorKernel(vpDataStreamTestNorKernel,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckNorKernel_InvalidMagic_FALSE)
{
	tBool VbReturn;
	TPddKernel_Header*  VspHeader32=(TPddKernel_Header*)vpDataStreamTestNorKernel;
	VspHeader32->HeaderStream.Version=0x1234;
	VbReturn=PDD_ValidationCheckNorKernel(vpDataStreamTestNorKernel,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckNorKernel_InvalidChecksum_FALSE)
{
	tBool VbReturn;
	TPddKernel_Header*  VspHeader32=(TPddKernel_Header*)vpDataStreamTestNorKernel;
	VspHeader32->HeaderStream.Checksum=VspHeader32->HeaderStream.Checksum-1;
	VbReturn=PDD_ValidationCheckNorKernel(vpDataStreamTestNorKernel,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckNorKernel_InvalidData_FALSE)
{
	tBool VbReturn;
	tU8*  Vu8pBuffer=(tU8*)vpDataStreamTestNorKernel;
	tS32  Vs32SizeHeader=sizeof(TPddKernel_Header);
	*(Vu8pBuffer+Vs32SizeHeader) -=1;
	VbReturn=PDD_ValidationCheckNorKernel(vpDataStreamTestNorKernel,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckNorKernel_InvalidDataZero_FALSE)
{
	tBool VbReturn;
	tU8*  Vu8pBuffer=(tU8*)vpDataStreamTestNorKernel;
	tS32  Vs32SizeHeader=sizeof(TPddKernel_Header);
	memset(Vu8pBuffer+Vs32SizeHeader,0x00,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeader);
	VbReturn=PDD_ValidationCheckNorKernel(vpDataStreamTestNorKernel,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PPDD_ValidationCheckNorKernel_InvalidData0xFF_FALSE)
{
	tBool VbReturn;
	tU8*  Vu8pBuffer=(tU8*)vpDataStreamTestNorKernel;
	tS32  Vs32SizeHeader=sizeof(TPddKernel_Header);
	memset(Vu8pBuffer+Vs32SizeHeader,0xff,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeader);
	VbReturn=PDD_ValidationCheckNorKernel(vpDataStreamTestNorKernel,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32,PDD_UTEST_VALIDATION_VERSION_32);
	EXPECT_EQ(FALSE, VbReturn);
}
/**********************  PDD_ValidationCheckScc *************************************************/
TEST_F(PddValidationTest, PDD_ValidationCheckScc_ValidStream_TRUE)
{
	tBool VbReturn=PDD_ValidationCheckScc(vpDataStreamTestCRCScc,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8,PDD_UTEST_VALIDATION_VERSION_8);
	EXPECT_EQ(TRUE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckScc_InvalidVersion_FALSE)
{
	tBool VbReturn;
	TPddScc_Header*  VspHeaderScc=(TPddScc_Header*)vpDataStreamTestCRCScc;
	VspHeaderScc->u32VersionPool=0x12;
	VbReturn=PDD_ValidationCheckScc(vpDataStreamTestCRCScc,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8,PDD_UTEST_VALIDATION_VERSION_8);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckScc_InvalidChecksum_FALSE)
{
	tBool VbReturn;
	TPddScc_Header*  VspHeaderScc=(TPddScc_Header*)vpDataStreamTestCRCScc;
	VspHeaderScc->u32Checksum=VspHeaderScc->u32Checksum-1;
	VbReturn=PDD_ValidationCheckScc(vpDataStreamTestCRCScc,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8,PDD_UTEST_VALIDATION_VERSION_8);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckScc_InvalidData_FALSE)
{
	tBool VbReturn;
	tU8*  Vu8pBuffer=(tU8*)vpDataStreamTestCRCScc;
	tS32  Vs32SizeHeader=sizeof(TPddScc_Header);
	*(Vu8pBuffer+Vs32SizeHeader) -=1;
	VbReturn=PDD_ValidationCheckScc(vpDataStreamTestCRCScc,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8,PDD_UTEST_VALIDATION_VERSION_8);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckScc_InvalidDataZero_FALSE)
{
	tBool VbReturn;
	tU8*  Vu8pBuffer=(tU8*)vpDataStreamTestCRCScc;
	tS32  Vs32SizeHeader=sizeof(TPddScc_Header);
	memset(Vu8pBuffer+Vs32SizeHeader,0x00,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8-Vs32SizeHeader);
	VbReturn=PDD_ValidationCheckScc(vpDataStreamTestCRCScc,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8,PDD_UTEST_VALIDATION_VERSION_8);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckScc_InvalidData0xFF_FALSE)
{
	tBool VbReturn;
	tU8*  Vu8pBuffer=(tU8*)vpDataStreamTestCRCScc;
	tS32  Vs32SizeHeader=sizeof(TPddScc_Header);
	memset(Vu8pBuffer+Vs32SizeHeader,0xff,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8-Vs32SizeHeader);
	VbReturn=PDD_ValidationCheckScc(vpDataStreamTestCRCScc,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8,PDD_UTEST_VALIDATION_VERSION_8);
	EXPECT_EQ(FALSE, VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckScc_InvalidVersion_AssertFunction)
{
	tBool VbReturn;	
	VbReturn=PDD_ValidationCheckScc(vpDataStreamTestCRCScc,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8,0xffff);	
  EXPECT_EQ(FALSE, VbReturn);
}
/**********************  PDD_ValidationGetHeaderFile *************************************************/
TEST_F(PddValidationTest, PDD_ValidationGetHeaderFile_GetKnownHeader_HeaderEqual)
{
  tS32                  Vs32SizeHeader32=sizeof(tsPddFileHeader);
  tsPddFileHeader       VtsHeader;
  tU8*                  VpBufferStream=(tU8*) vpDataStreamTestCRC32;    
  tU32                  Vu32Version=PDD_UTEST_VALIDATION_VERSION_32;
  int                   ViRetunValueMemcmp;
  
  PDD_ValidationGetHeaderFile(VpBufferStream+Vs32SizeHeader32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeader32,Vu32Version,(void*)&VtsHeader);
  ViRetunValueMemcmp=memcmp(vpDataStreamTestCRC32,&VtsHeader,Vs32SizeHeader32);
  EXPECT_EQ(ViRetunValueMemcmp,0);
}
TEST_F(PddValidationTest, PDD_ValidationGetHeaderFile_GetUnknownHeader_HeaderNotEqual)
{
  tS32                  Vs32SizeHeader32=sizeof(tsPddFileHeader);
  tsPddFileHeader       VtsHeader;
  tU8*                  VpBufferStream=(tU8*) vpDataStreamTestCRC32;    
  tU32                  Vu32Version=PDD_UTEST_VALIDATION_VERSION_32-20;
  int                   ViRetunValueMemcmp;
  
  PDD_ValidationGetHeaderFile(VpBufferStream+Vs32SizeHeader32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeader32,Vu32Version,(void*)&VtsHeader);
  ViRetunValueMemcmp=memcmp(vpDataStreamTestCRC32,&VtsHeader,Vs32SizeHeader32);
  EXPECT_NE(ViRetunValueMemcmp,0);
}
/**********************  PDD_ValidationGetHeaderNorKernel *************************************************/
TEST_F(PddValidationTest, PDD_ValidationGetHeaderNorKernel_GetUnknownHeader_HeaderNotEqual)
{
  tS32                  Vs32SizeHeader=sizeof(TPddKernel_Header);
  TPddKernel_Header     VtsHeader;
  tU8*                  VpBufferStream=(tU8*) vpDataStreamTestNorKernel;    
  tU32                  Vu32Version=PDD_UTEST_VALIDATION_VERSION_32-20;
  int                   ViRetunValueMemcmp;
  
  memset(&VtsHeader,0,sizeof(TPddKernel_Header));
  PDD_ValidationGetHeaderNorKernel(VpBufferStream+Vs32SizeHeader,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeader,Vu32Version,(void*)&VtsHeader);
  ViRetunValueMemcmp=memcmp(vpDataStreamTestNorKernel,&VtsHeader,Vs32SizeHeader);
  EXPECT_NE(ViRetunValueMemcmp,0);
}
/**********************  PDD_ValidationGetHeaderScc *************************************************/
TEST_F(PddValidationTest, PDD_ValidationGetHeaderScc_GetKnownHeader_HeaderEqual)
{
  tS32                  Vs32SizeHeader8=sizeof(TPddScc_Header);
  TPddScc_Header        VtsHeader;
  tU8*                  VpBufferStream=(tU8*) vpDataStreamTestCRCScc;    
  tU8                   Vu8Version=PDD_UTEST_VALIDATION_VERSION_8;
  int                   ViRetunValueMemcmp;
  
  PDD_ValidationGetHeaderScc(VpBufferStream+Vs32SizeHeader8,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8-Vs32SizeHeader8,Vu8Version,(void*)&VtsHeader);
  ViRetunValueMemcmp=memcmp(vpDataStreamTestCRCScc,&VtsHeader,Vs32SizeHeader8);
  EXPECT_EQ(ViRetunValueMemcmp,0);
}
TEST_F(PddValidationTest, PDD_ValidationGetHeaderScc_GetUnknownHeader_HeaderNotEqual)
{
  tS32                  Vs32SizeHeader8=sizeof(TPddScc_Header);
  TPddScc_Header        VtsHeader;
  tU8*                  VpBufferStream=(tU8*) vpDataStreamTestCRCScc;    
  tU8                   Vu8Version=PDD_UTEST_VALIDATION_VERSION_8-20;
  int                   ViRetunValueMemcmp;
  
  PDD_ValidationGetHeaderScc(VpBufferStream+Vs32SizeHeader8,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_8-Vs32SizeHeader8,Vu8Version,(void*)&VtsHeader);
  ViRetunValueMemcmp=memcmp(vpDataStreamTestCRCScc,&VtsHeader,Vs32SizeHeader8);
  EXPECT_NE(ViRetunValueMemcmp,0);
}
/**********************  PDD_ValidationCheckCrcEqualFile ****************************************************************************/
TEST_F(PddValidationTest, PDD_ValidationCheckCrcEqualFile_EqualFile_TRUE)
{
  tBool VbReturn;
  VbReturn=PDD_ValidationCheckCrcEqualFile(vpDataStreamTestCRC32,vpDataStreamTestCRC32);
  EXPECT_EQ(TRUE,VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckCrcEqualFile_NotEqualFile_FALSE)
{
  tBool VbReturn;
  VbReturn=PDD_ValidationCheckCrcEqualFile(vpDataStreamTestCRC32,vpDataStreamTestCRCScc);
  EXPECT_EQ(FALSE,VbReturn);
}
/**********************  PDD_ValidationCheckCrcEqualScc *****************************************************************************/
TEST_F(PddValidationTest, PDD_ValidationCheckCrcEqualScc_EqualFile_TRUE)
{
  tBool VbReturn;
  VbReturn=PDD_ValidationCheckCrcEqualScc(vpDataStreamTestCRCScc,vpDataStreamTestCRCScc);
  EXPECT_EQ(TRUE,VbReturn);
}
TEST_F(PddValidationTest, PDD_ValidationCheckCrcEqualScc_NotEqualFile_FALSE)
{
  tBool VbReturn;
  VbReturn=PDD_ValidationCheckCrcEqualScc(vpDataStreamTestCRC32,vpDataStreamTestCRCScc);
  EXPECT_EQ(FALSE,VbReturn);
}
/**********************  PDD_ValidationCalcCrc32 ************************************************************************************/
TEST_F(PddValidationTest, PDD_ValidationCalcCrc32_GetKnownCrc32_Equal)
{
  tU32              Vu32Crc;
  tS32              Vs32SizeHeader32=sizeof(tsPddFileHeader);
  tsPddFileHeader*  VspHeader32=(tsPddFileHeader*)vpDataStreamTestCRC32;
  Vu32Crc=PDD_ValidationCalcCrc32((tU8*)vpDataStreamTestCRC32+Vs32SizeHeader32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32-Vs32SizeHeader32);
  EXPECT_EQ(VspHeader32->u32Checksum,Vu32Crc);
}
TEST_F(PddValidationTest, PDD_ValidationCalcCrc32_GetUnKnownCrc32_NotEqual)
{
  tU32              Vu32Crc;
  tsPddFileHeader*  VspHeader32=(tsPddFileHeader*)vpDataStreamTestCRC32;
  Vu32Crc=PDD_ValidationCalcCrc32((tU8*)vpDataStreamTestCRC32,PDD_UTEST_VALIDATION_BUFFER_SIZE_CRC_32);
  EXPECT_NE(VspHeader32->u32Checksum,Vu32Crc);
}
TEST_F(PddValidationTest, PDD_ValidationCalcCrc32_LengthZero_0xFFFFFFFF)
{
  tU32              Vu32Crc;
  tsPddFileHeader*  VspHeader32=(tsPddFileHeader*)vpDataStreamTestCRC32;
  Vu32Crc=PDD_ValidationCalcCrc32((tU8*)vpDataStreamTestCRC32,0);
  EXPECT_EQ(0xFFFFFFFF,Vu32Crc);
}

/**********************  PDD_ValidationCalcCrc32 *************************************************************************************/
/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
