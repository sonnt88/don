/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        pdd.h
 *
 * header for the PDD(persistent data device). 
 *
 * @date        2012-10-10
 *
 * @note
 *
 *  &copy; Copyright BoschSoftec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef PDD_H
#define PDD_H

#ifdef __cplusplus
extern "C" {
#endif
/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
/*new feature defines for pdd */
#define PDD_FEATURE_SYNC_STREAMS_SCC_AND_NORUSER_WITH_BACKUP


/*define for max size name*/
#define PDD_SCC_MAX_SIZE_NAME          32   /*should be equal to PDD_NOR_MAX_SIZE_NAME, because backup save to NOR*/
#define PDD_NOR_MAX_SIZE_NAME          32   

#define PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM    30 //use in PddCreate.py too 

/*32k cluster (if change the size, older persistent data not valid=> erase flash)*/
#define PDD_NOR_USER_CLUSTER_SIZE                             0x8000  

/* success */
#define PDD_OK                                        0
/* error code general*/
#define PDD_ERROR_WRONG_PARAMETER                    -1
#define PDD_ERROR_NO_MEMORY                          -2
#define PDD_ERROR_NOT_SUPPORTED                      -3
/* error code pdd.c*/
#define PDD_ERROR_READ_NO_VALID_DATA_STREAM         -10
#define PDD_ERROR_WRITE_NO_DATA_STREAM              -11
#define PDD_ERROR_WRITE_NO_DATA_STREAM_BACKUP       -12   /*but, normal file is written*/
#define PDD_ERROR_WRITE_NO_DATA_STREAM_NORMAL       -13   /*but, backup is written*/
#define PDD_ERROR_READ_BUFFER_TO_SMALL              -14
#define PDD_ERROR_DELETE_DATA_STREAM                -15
#define PDD_ERROR_DATA_STREAM_DOESNOTEXIST          -16
/* error file access */
#define PDD_ERROR_FILE_NO_VALID_SIZE                -21
#define PDD_ERROR_FILE_NO_FILE_PATH_DATAPOOL        -22
#define PDD_ERROR_FILE_NO_FILE_PATH_SECURE          -23
#define PDD_ERROR_FILE_PATH_NOT_MOUNTED             -24
/*error code admin*/
#define PDD_ERROR_ADMIN_INIT_FILE_SYSTEM            -41
#define PDD_ERROR_ADMIN_INIT_SCC                    -42
#define PDD_ERROR_ADMIN_INIT_NOR_USER               -43
#define PDD_ERROR_ADMIN_INIT_NOR_KERNEL             -44
/*error SCC access*/
#define PDD_ERROR_SCC_INVALID_DATA_STREAM           -50
#define PDD_ERROR_SCC_INVALID_SIZE_OF_STREAM        -51

/*for access rights */
#define PDD_ACCESS_RIGTHS                 S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP      /*0660*/
#define PDD_ACCESS_RIGTHS_DIR             S_ISGID | S_IRWXU | S_IRWXG                /*2770*/
#define PDD_ACCESS_RIGTHS_GROUP_NAME      "eco_pdd"

/*default mount path*/
#ifdef PDD_UNIT_TEST
#define PDD_FILE_DEFAULT_PATH             "/tmp/var/opt/bosch/dynamic/pdd/" 
#define PDD_FILE_DEFAULT_PATH_SECURE      "/tmp/var/opt/bosch/persistent/pdd/" 
#else
#define PDD_FILE_DEFAULT_PATH             "/var/opt/bosch/dynamic/pdd/" 
#define PDD_FILE_DEFAULT_PATH_SECURE      "/var/opt/bosch/persistent/pdd/" 
#endif
/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef enum 
{
  PDD_LOCATION_FS,
  PDD_LOCATION_FS_SECURE,
  PDD_LOCATION_NOR_USER,
  PDD_LOCATION_NOR_KERNEL,
  PDD_LOCATION_SCC,
  PDD_LOCATION_LAST,
}tePddLocation;

/*old definition*/
#define PDD_LOCATION_RAW_NOR                        PDD_LOCATION_NOR_USER
#define PDD_LOCATION_INC							              PDD_LOCATION_SCC
#define PDD_LOCATION_FILE_SYSTEM                    PDD_LOCATION_FS
#define PDD_LOCATION_FILE_SYSTEM_SECURE_PARTITION   PDD_LOCATION_FS_SECURE

/* info read backup or actual data */
#define PDD_READ_INFO_NORMAL_FILE      0
#define PDD_READ_INFO_BACKUP_FILE      1
/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
/* functions for application access */
tS32 pdd_get_data_stream_size(const char* PtsDataStreamName,tePddLocation);
tS32 pdd_read_datastream(const char* PtsDataStreamName,tePddLocation,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version,tU8* Pu8Info);
tS32 pdd_write_data_stream(const char* PtsDataStreamName,tePddLocation,tU8 *Ppu8WriteBuffer, tS32 Ps32SizeBytestoWrite,tU32 Pu32Version);
tS32 pdd_write_datastream(const char* PtsDataStreamName,tePddLocation,tU8 *Ppu8WriteBuffer, tS32 Ps32SizeBytestoWrite,tU32 Pu32Version,tBool PbfSync);
tS32 pdd_delete_data_stream(const char* PtsDataStreamName,tePddLocation);
void pdd_sync_scc_streams(void);
void pdd_sync_nor_user_streams(void);
tS32 pdd_helper_get_element_from_stream(tString PstrElementName,const void* PvpBufferStream,size_t PtsSizeStreamBuffer, void* PpvBufRead,tS32 Vs32Size,tU8 Pu8Version);
tS32 pdd_read_datastream_early_from_nor(const char* PtsDataStreamName,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version,tU8* Pu8Info);

/*for trace command from DATAPOOL and for pdd_test_out.out; need for free osal PDD*/
void pdd_vTraceCommand(char* Pu8pData);
/*old definition*/
tS32 pdd_read_data_stream(const char* PtsDataStreamName,tePddLocation,tU8 *Ppu8ReadBuffer, tS32 Ps32SizeReadBuffer,tU32 Pu32Version);

#ifdef __cplusplus
}
#endif
#else
#error pdd.h included several times
#endif
