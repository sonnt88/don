/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        pdd_variant.h
 *
 *  header for the pdd variant handling
 *
 * @date        2014-04-25
 *
 * @note
 *
 *  &copy; Copyright BSOT GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef PDD_VARIANT_H
#define PDD_VARIANT_H

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
#ifdef CS_TESTMANAGER
#define PDD_TESTMANGER_ACTIVE
#endif

#ifndef PDD_TESTMANGER_ACTIVE 
#include "helper.h" 
#include "pdd_variant_gen.h" 
#endif

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

#else
#error PDD_variant.h included several times
#endif
