/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        private header for the PDD(persistent data device). 
 * @addtogroup   PDD 
 * @{
 */ 
#ifndef PDD_PRIVATE_H
#define PDD_PRIVATE_H

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines (scope: global)
|-----------------------------------------------------------------------*/
#define PDD_C_DATA_STREAM_MAGIC                          0xd0e1d0e1

/******** for nor user access *********/ 
#define PDD_NOR_USER_MIN_NUMBER_OF_SECTOR                2
#define PDD_NOR_USER_MIN_SECTOR_SIZE                     0x20000 // PDD_NOR_USER_CLUSTER_SIZE //32k (GM: 0x20000 128k)  
#define PDD_NOR_USER_MAX_WRITE_POSITION                  (0x08)
#ifndef PDD_TESTMANGER_ACTIVE
#define PDD_NOR_USER_HEADER_SIZE_DATA_STREAM (PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM*sizeof(tsPddNorUserHeaderDataStream))
#endif

//time limit between two erase operation of one sector
#define PDD_NOR_LIMIT_TIME_ERASE_CYCLE                   90  //second

/******** for nor kernel access *********/
#define PDD_NOR_KERNEL_SECTOR_NUM                        2    
#define PDD_NOR_KERNEL_MAGIC                             0xDEADDEAD

/* for validation*/
#define PDD_NOR_KERNEL_STATE_VALID                       0x01
#define PDD_NOR_KERNEL_STATE_INVALID                     0x02

/******** for V850 access *********/	
/*defines for u32KindSave*/
#define PDD_SCC_STORE_AT_SHUTDOWN       0
#define PDD_SCC_STORE_DIRECT            1

#ifndef PDD_TESTMANGER_ACTIVE
/******** for file access *********/	
/* max mount time*/
#define PDD_FILE_PATH_MAX_MOUNT_TIME           45  //second
#define PDD_FILE_PATH_SECURE_MAX_MOUNT_TIME     4  //second

/*max size file */
#define PDD_FILE_MAX_PATH                     PATH_MAX  
/*max size name */
#define PDD_FILE_MAX_NAME                     (NAME_MAX-(sizeof(PDD_FILE_EXT)))
/*max size file name*/
#define PDD_FILE_MAX_PATH_NAME                PATH_MAX+NAME_MAX
/*File extension*/
#define PDD_FILE_EXT                          ".dat"
/*extension for backup*/
#define PDD_FILE_EXT_BACKUP                   ".tmp"

/*file system location (PDD_LOCATION_FS and PDD_LOCATION_FS_SECURE*/
typedef enum 
{
  PDD_LOCATION_CONFIG_FS,
  PDD_LOCATION_CONFIG_FS_SECURE,
  PDD_LOCATION_CONFIG_FS_LAST,
}tePddLocationFSConfig;
#endif
/******* for lint  ************/
#define PDD_PARAMETER_INTENTIONALLY_UNUSED(x) (void)(x);

/************************************************************************
| macros (scope: global)
|-----------------------------------------------------------------------*/
/**--------- PDD_TESTMANGER_ACTIVE -----------------*/
#ifdef PDD_TESTMANGER_ACTIVE
#define M_PDD_ADMIN_LOCK(tePddAccess)\
	PDD_OK
#define M_PDD_ADMIN_UNLOCK(tePddAccess)\
	PDD_OK
/*file access function*/
#define M_PDD_FILE_GET_DATASTREAM_SIZE(PtsDataStreamName,VenLocation)\
    PDD_OK
#define M_PDD_FILE_GET_DATASTREAM_SIZE_BACKUP(PtsDataStreamName,VenLocation)\
    PDD_OK
#define M_PDD_FILE_READ_DATASTREAM(PtsDataStreamName,VenLocation,PpvReadBuffer,Ps32SizeReadBuffer,PeKindFile)\
	PDD_OK
#define M_PDD_FILE_WRITE_DATASTREAM(PtsDataStreamName,VenLocation,PpvWriteBuffer,Ps32SizeWriteBuffer,PeKindFile,PbSync)\
	PDD_OK
#define M_PDD_FILE_DELETE_DATASTREAM(PtsDataStreamName,VenLocation,PeKindFile)\
	PDD_OK
/* NOR_Kernel access function */
#define M_PDD_NORKERNEL_ACCESS_GET_DATASTREAM_SIZE()\
	PDD_OK
#define M_PDD_NORKERNEL_ACCESS_READ_DATASTREAM(PpvReadBuffer,Ps32SizeReadBuffer)\
	PDD_OK
#define M_PDD_NORKERNEL_ACCESS_WRITE_DATASTREAM(PpvWriteBuffer,Ps32SizeWriteBuffer)\
	PDD_OK
/* SCC access function */
#define M_PDD_SCCACCESS_GET_DATASREAM_SIZE(PtsDataStreamName)\
	PDD_OK
#define M_PDD_SCCACCESS_READ_DATASTREAM(PtsDataStreamName,PpvReadBuffer,Ps32SizeReadBuffer)\
	PDD_OK
#define M_PDD_SCCACCESS_WRITE_DATASTREAM(PtsDataStreamName,PpvWriteBuffer,Ps32SizeWriteBuffer)\
	PDD_OK
#else
/**--------- not PDD_TESTMANGER_ACTIVE -----------------*/
#define M_PDD_ADMIN_LOCK(tePddAccess)\
	PDD_Lock(tePddAccess)
#define M_PDD_ADMIN_UNLOCK(tePddAccess)\
	PDD_UnLock(tePddAccess)
/*file access function*/
#define M_PDD_FILE_GET_DATASTREAM_SIZE(PtsDataStreamName,VenLocation)\
	PDD_FileGetDataStreamSize(PtsDataStreamName,VenLocation)
#define M_PDD_FILE_GET_DATASTREAM_SIZE_BACKUP(PtsDataStreamName,VenLocation)\
	PDD_FileGetDataStreamSizeBackup(PtsDataStreamName,VenLocation);
#define M_PDD_FILE_READ_DATASTREAM(PtsDataStreamName,VenLocation,PpvReadBuffer,Ps32SizeReadBuffer,PeKindFile)\
	PDD_FileReadDataStream(PtsDataStreamName,VenLocation,PpvReadBuffer,Ps32SizeReadBuffer,PeKindFile);
#define M_PDD_FILE_WRITE_DATASTREAM(PtsDataStreamName,VenLocation,PpvWriteBuffer,Ps32SizeWriteBuffer,PeKindFile,PbSync)\
	PDD_FileWriteDataStream(PtsDataStreamName,VenLocation,PpvWriteBuffer,Ps32SizeWriteBuffer,PeKindFile,PbSync);
#define M_PDD_FILE_DELETE_DATASTREAM(PtsDataStreamName,VenLocation,PeKindFile)\
	PDD_FileDeleteDataStream(PtsDataStreamName,VenLocation,PeKindFile);
/* NOR_Kernel access function */
#define M_PDD_NORKERNEL_ACCESS_GET_DATASTREAM_SIZE()\
	PDD_NorKernelAccessGetDataStreamSize()
#define M_PDD_NORKERNEL_ACCESS_READ_DATASTREAM(PpvReadBuffer,Ps32SizeReadBuffer)\
	PDD_NorKernelAccessReadDataStream(PpvReadBuffer,Ps32SizeReadBuffer)
#define M_PDD_NORKERNEL_ACCESS_WRITE_DATASTREAM(PpvWriteBuffer,Ps32SizeWriteBuffer)\
	PDD_NorKernelAccessWriteDataStream(PpvWriteBuffer,Ps32SizeWriteBuffer)
/* SCC access function */
#define M_PDD_SCCACCESS_GET_DATASREAM_SIZE(PtsDataStreamName)\
	PDD_SccAccessGetDataStreamSize(PtsDataStreamName)
#define M_PDD_SCCACCESS_READ_DATASTREAM(PtsDataStreamName,PpvReadBuffer,Ps32SizeReadBuffer)\
	PDD_SccAccessReadDataStream(PtsDataStreamName,PpvReadBuffer,Ps32SizeReadBuffer);
#define M_PDD_SCCACCESS_WRITE_DATASTREAM(PtsDataStreamName,PpvWriteBuffer,Ps32SizeWriteBuffer)\
	PDD_SccAccessWriteDataStream(PtsDataStreamName,PpvWriteBuffer,Ps32SizeWriteBuffer);
#endif
/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef enum 
{
  PDD_KIND_FILE_NORMAL,
  PDD_KIND_FILE_BACKUP,
}tePddKindFile;

typedef enum 
{
  PDD_KIND_RES_SEM,
  PDD_KIND_RES_SHM,
  PDD_KIND_RES_FILE,
  PDD_KIND_RES_DIR,
}tePddKindRes;

typedef enum 
{
  PDD_SEM_ACCESS_FILE_SYSTEM,
  PDD_SEM_ACCESS_NOR_USER,
  PDD_SEM_ACCESS_NOR_KERNEL,
  PDD_SEM_ACCESS_SCC,
  PDD_SEM_ACCESS_LAST
}tePddAccess;

typedef struct
{
  tU32              u32Magic;
  tU32              u32Version;
  tU32              u32Checksum;
}tsPddFileHeader;

/* header data stream*/
typedef struct
{
  tU8       u8DataStreamName[PDD_NOR_MAX_SIZE_NAME];
  tU32      u32Offset;
  tU32      u32Lenght;
  tU32      u32Count;
}tsPddNorUserHeaderDataStream;

/*structure for the driver info shared memory*/
typedef struct
{
  tU32                           u32SectorSize;
  tU32                           u32ClusterSize;
  tU32                           u32NumBlocks;
  tU32                           u32ClusterSizeData;
  tS32                           s32NumClusterPerSector;
  tS32                           s32NumCluster;  
  tU8                            u8pBufferDataOld[PDD_NOR_USER_CLUSTER_SIZE];  
  tS32                           s32LastClusterUsed;
  tU16                           u16ChunkSize;
  tU32                           u32EraseSectorCount; 
  tU32                           u32EraseSectorLastTime[PDD_NOR_USER_MIN_NUMBER_OF_SECTOR]; 
#ifndef PDD_TESTMANGER_ACTIVE
  tsPddNorUserHeaderDataStream   sHeaderDataStream[PDD_NOR_USER_CONFIG_NUMBER_DATASTREAM];
#else
  tsPddNorUserHeaderDataStream   sHeaderDataStream[PDD_NOR_USER_CONFIG_MAX_NUMBER_DATASTREAM];
#endif
}tsPddNorUserInfo;

/* structure for one sector*/
typedef struct
{
  tS32                           s32SizeData;
  tU32                           u32EraseSectorCount;
  tBool                          bValid;
  tBool                          bUpdated;  
  tU32                           u32EraseSectorLastTime; 
}tsPddNorKernelSectorInfo;

/*structure for the driver info shared memory*/
typedef struct
{
  tU32                           u32SectorSize;
  tU32                           u32SectorMaxSizeData;
  tU32                           u32NumBlocks;  
  tU16                           u16ChunkSize;
  tBool                          bSyncDone; 
  tsPddNorKernelSectorInfo       sKernelSectorInfo[PDD_NOR_KERNEL_SECTOR_NUM];
}tsPddNorKernelInfo;

#ifndef PDD_TESTMANGER_ACTIVE
/*structure for the driver info shared memory*/
typedef struct
{
  char   strPathName[PDD_FILE_MAX_PATH];
}tsPddFileInfo;
#endif
/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
/* admin function */
void                 PDD_InitAccess(void);
void                 PDD_DeInitAccess(void);
tS32                 PDD_IsInit(tePddLocation,const char*);
tS32                 PDD_Lock(tePddAccess);
tS32                 PDD_UnLock(tePddAccess);
tsPddNorUserInfo*    PDD_GetNorUserInfoShm(void);
tsPddNorKernelInfo*  PDD_GetNorKernelInfoShm(void);
void                 vPDD_InitTraceLevel(void);
tU32                 u32PDD_GetTraceLevel(void);
void                 vPDD_SetTraceLevel(tU32,tBool);
tU8                  u8PDD_GetTraceStreamOutput(void);
void                 vPDD_SetTraceStreamOutput(tU8,tBool);
tS32                 s32PDD_ChangeGroupAccess(tePddKindRes,tS32 ,const char* );
tBool                bPDD_SccGetReadOldDataAfterStartup(void);
void                 vPDD_SccSetReadOldDataAfterStartup(tBool);
tBool                bPDD_SccGetFlagSendComponentStatusMsgDone(void);
void                 vPDD_SccSetFlagSendComponentStatusMsgDone(tBool VbFlag);
char*                strPDD_GetConfigPath(tePddLocationFSConfig);

/* file access function */
tS32  PDD_FileCreatePath(tePddLocation PenLocation,const char*);
tS32  PDD_FileGetDataStreamSize(const char* ,tePddLocation PenLocation);
tS32  PDD_FileGetDataStreamSizeBackup(const char* ,tePddLocation PenLocation);
tS32  PDD_FileReadDataStream(const char* ,tePddLocation,void*,tS32,tePddKindFile);
tS32  PDD_FileWriteDataStream(const char* ,tePddLocation,void*,tS32,tePddKindFile,tBool);
tS32  PDD_FileDeleteDataStream(const char* ,tePddLocation,tePddKindFile);
char* PDD_FileReadConfigLine(const char*);

/* SCC access function */
tS32 PDD_SccAccessGetDataStreamSize(const char*);
tS32 PDD_SccAccessReadDataStream(const char* ,const void*,tS32);
tS32 PDD_SccAccessWriteDataStream(const char* ,void*,tS32);

/* NOR_USER access function */
tS32  PDD_NorUserAccessInit(void);
tS32  PDD_NorUserAccessDeInit(void);
tS32  PDD_NorUserAccessInitProcess(void);
tS32  PDD_NorUserAccessDeInitProcess(void);
tS32  PDD_NorUserAccessGetDataStreamSize(const char* PtsDataStreamName);
tS32  PDD_NorUserAccessReadDataStream(const char* ,void*,tS32);
tS32  PDD_NorUserAccessWriteDataStream(const char* ,void*,tS32);
tBool PDD_bReadDataStreamConfiguration(tsPddNorUserInfo*);
tS32  PDD_NorUserAccessReadDataStreamEarly(const char* ,void*,tS32);
/*for commands TTFIS or pdd_test*/
tS32  PDD_NorUserAccessErase(void); 
tS32  PDD_NorUserAccessReadPartition(void *Ppu8ReadBuffer);
tS32  PDD_NorUserAccessReadActualCluster(void *Ppu8ReadBuffer);
tS32  PDD_NorUserAccessWritePartition(void *Ppu8ReadBuffer);
tS32  PDD_NorUserAccessGetActualCluster(void);
tU32  PDD_NorUserAccessGetEraseCounter(void);


/* NOR_Kernel access function */
tS32 PDD_NorKernelAccessInit(void);
tS32 PDD_NorKernelAccessDeInit(void);
tS32 PDD_NorKernelAccessInitProcess(void);
tS32 PDD_NorKernelAccessDeInitProcess(void);
tS32 PDD_NorKernelAccessGetDataStreamSize(void);
tS32 PDD_NorKernelAccessErase(tU8 PubSector); 
tS32 PDD_NorKernelAccessReadDataStream(void*,tS32);
tS32 PDD_NorKernelAccessWriteDataStream(void*,tS32);
tU32 PDD_NorKernelAccessGetEraseCounter(tU8 PubSector);
tU32 PDD_NorKernelAccessGetChunkSize(void);

/* validation */
tBool PDD_ValidationCheckFile(void*,tS32,tU32);
tBool PDD_ValidationCheckScc(void*,tS32,tU32);
tBool PDD_ValidationCheckNorKernel(void* PpBuffer,tS32 Ps32Size, tU32 Pu32Version);
void  PDD_ValidationGetHeaderFile(tU8* PpBufferStream,tS32 Ps32LengthDataStream, tU32 Pu32Version,void*);
void  PDD_ValidationGetHeaderScc(tU8* PpBufferStream,tS32 Ps32LengthDataStream, tU32 Pu32Version,void*);
void  PDD_ValidationGetHeaderNorKernel(tU8* PpBufferStream,tS32 Ps32LengthDataStream, tU32 Pu32Version,void*);
tBool PDD_ValidationCheckCrcEqualFile(const void* PpAdrBufferBackup,const void* PpAdrBufferNormal);
tBool PDD_ValidationCheckCrcEqualScc(const void* PpAdrBufferBackup,const void* PpAdrBufferNormal);
tBool PDD_ValidationCheckCrcEqualNorKernel(const void* PpAdrBufferBackup,const void* PpAdrBufferNormal);
tU32  PDD_ValidationCalcCrc32(const tU8* PpAdrBuffer,tU32 Pu32Len);

#ifdef __cplusplus
}
#endif
#else
#error pdd_private.h included several times
#endif
