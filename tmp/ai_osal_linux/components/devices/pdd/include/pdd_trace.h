/* ******************************************************FileHeaderBegin** *//**
 *
 * @file      pdd_trace.h
 *
 *  private header for the pdd driver trace information. 
 *
 * @date        2013-02-18
 *
 * @note
 *
 *  &copy; Copyright BoschSoftec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef PDD_TRACE_H
#define PDD_TRACE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <unistd.h>

/*************************************************************************/
/* define                                                                */
/*************************************************************************/
/*activate level*/
#define PDD_LEVEL_IMPORTANT                   0x1f
/*defines for PDD_vPuttyConsolTrace() */
#define PDD_LEVEL_INTERFACE_CALL              0x2f
#define PDD_LEVEL_INFO_ADMIN                  0x3f
#define PDD_LEVEL_PDD                         0x4f
#define PDD_LEVEL_FILE                        0x5f
#define PDD_LEVEL_SCC                         0x6f
#define PDD_LEVEL_NOR_USER                    0x8f
#define PDD_LEVEL_VALIDATION                  0x9f
#define PDD_LEVEL_NOR_KERNEL                  0xaf  

#define PDD_TRACE_IMPORTANT                   0x0001
#define PDD_TRACE_INTERFACE_CALL              0x0002
#define PDD_TRACE_ADMIN                       0x0004
#define PDD_TRACE_PDD                         0x0008
#define PDD_TRACE_FILE                        0x0010
#define PDD_TRACE_SCC                         0x0020
#define PDD_TRACE_NOR_USER                    0x0040
#define PDD_TRACE_VALIDATION                  0x0080
#define PDD_TRACE_NOR_KERNEL                  0x0100
#define PDD_TRACE_DEFAULT                     (PDD_TRACE_IMPORTANT)

/*activate stream output*/
#define PDD_TRACE_DEFAULT_STREAM_OUTPUT       STDERR_FILENO

/*command test*/
#define	PDD_TEST_CMD_NOR_USER_ERASE                           0x01
#define PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA               0x02
#define PDD_TEST_CMD_NOR_USER_GET_ERASE_COUNTER               0x03
#define PDD_TEST_CMD_NOR_KERNEL_ERASE                         0x04 
#define PDD_TEST_CMD_NOR_KERNEL_ERASE_SECTOR                  0x05
#define PDD_TEST_CMD_NOR_KERNEL_PRINT_ACTUAL_DATA             0x06 
#define PDD_TEST_CMD_NOR_KERNEL_GET_ERASE_COUNTER             0x07 
#define PDD_TEST_CMD_SCC_SET_ALL_POOLS_TO_INVALID             0x08
#define PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_FILE               0x09
#define PDD_TEST_CMD_NOR_USER_SAVE_ACTUAL_CLUSTER             0x0a 
#define PDD_TEST_CMD_NOR_USER_SAVE_DUMP_TO_NOR                0x0b
#define PDD_TEST_CMD_NOR_USER_GET_ACTUAL_CLUSTER              0x0c 
#define PDD_TEST_CMD_SET_TRACE_LEVEL                          0x0d
#define PDD_TEST_CMD_CLEAR_TRACE_LEVEL                        0x0e
#define PDD_TEST_CMD_SET_TRACE_STREAM_OUTPUT                  0x0f
#define PDD_TEST_CMD_CLEAR_TRACE_STREAM_OUTPUT                0x10
#define PDD_TEST_CMD_NOR_USER_PRINT_ACTUAL_DATA_STREAM        0x11  
#define PDD_TEST_CMD_NOR_USER_SAVE_BACKUP_FILE_TO_NOR         0x12  
#define PDD_TEST_CMD_DELETE_DATASTREAM                        0x13

/************************** kind traces *********************************/
/* important*/                               
#define PDD_ERROR                                  0x01
#define PDD_NOR_USER                               0x02
#define PDD_NOR_KERNEL                             0x03
#define PDD_SCC                                    0x04
#define PDD_INFO                                   0x05
#define PDD_ERROR_STR                              0x06

/* other*/
/* function call */
#define PDD_START_DATA_STREAM_SIZE            0x20
#define PDD_EXIT_DATA_STREAM_SIZE             0x21
#define PDD_START_READ_DATA_STREAM            0x22
#define PDD_EXIT_READ_DATA_STREAM             0x23
#define PDD_START_WRITE_DATA_STREAM           0x24
#define PDD_EXIT_WRITE_DATA_STREAM            0x25
#define PDD_START_SYNC_SCC                    0x26
#define PDD_EXIT_SYNC_SCC                     0x27
#define PDD_START_GET_ELEMENT_VALUE           0x28
#define PDD_EXIT_GET_ELEMENT_VALUE            0x29
#define PDD_START_SYNC_NOR_USER               0x2a
#define PDD_EXIT_SYNC_NOR_USER                0x2b
#define PDD_START_DELETE_DATA_STREAM          0x2c
#define PDD_EXIT_DELETE_DATA_STREAM           0x2d
#define PDD_START_READ_DATA_STREAM_EARLY      0x2e
#define PDD_EXIT_READ_DATA_STREAM_EARLY       0x2f

/* trace for pdd_admin.c */
#define PDD_ADMIN_FIRST_ATTACH                0x30
#define PDD_ADMIN_NOT_FIRST_ATTACH            0x31
#define PDD_ADMIN_SEM_WAIT                    0x32
#define PDD_ADMIN_SEM_POST                    0x33
#define PDD_ADMIN_SEM_VALUE                   0x34
#define PDD_ADMIN_SEM_INIT_SUCCESS            0x35   
#define PDD_ADMIN_SEM_DESTROY_SUCCESS         0x36   
#define PDD_ADMIN_ATTACH_COUNT                0x37
#define PDD_ADMIN_TRACE_LEVEL                 0x38
#define PDD_ADMIN_TRACE_STREAM_OUTPUT         0x39
#define PDD_ADMIN_TRACE_START_GET_CONFIG_PATH 0x3a
#define PDD_ADMIN_TRACE_CONFIG_PATH           0x3b

/* trace for pdd.c */
#define PDD_SAVE_DATA_INDENTICAL              0x40
#define PDD_GET_ELEMENT_NAME                  0x41
#define PDD_GET_ELEMENT_VERSION               0x42
#define PDD_GET_ELEMENT_LENGTH                0x43


/*trace for pdd_access_file.c*/
#define PDD_FILE_TRACE_PATH                   0x50
#define PDD_FILE_OPENFILE_SUCCESS             0x51
#define PDD_FILE_SIZE                         0x52
#define PDD_FILE_STAT_SUCCESS                 0x53
#define PDD_FILE_READ_SIZE                    0x54
#define PDD_FILE_WRITE_SIZE                   0x55
#define PDD_FILE_CREATE_PATH_SUCCESS          0x56
#define PDD_FILE_CREATE_PATH_ERROR            0x57
#define PDD_FILE_OPENFILE_FAILS               0x58
#define PDD_FILE_TIME                         0x59
#define PDD_FILE_FIND_MOUNT_POINT             0x5a
#define PDD_FILE_INFO                         0x5b

/*trace for pdd_access_scc.c*/
#define PDD_SCC_RECV_MESSAGE                  0x60
#define PDD_SCC_SEND_MESSAGE                  0x61
#define PDD_SCC_RECV_RETURN_DGRAM             0x62
#define PDD_SCC_SEND_RETURN_DGRAM             0x63
#define PDD_SCC_SIZE_WITH_HEADER              0x64
#define PDD_SCC_BEGINN_POLL                   0x65
#define PDD_SCC_FRAME_LENGHT                  0x67
#define PDD_SCC_ELEMENT_NAME                  0x68
#define PDD_SCC_HASH                          0x69
#define PDD_SCC_LENGTH_DATA_EXPECTED          0x6a
#define PDD_SCC_LENGTH_DATA_RECEIVED          0x6b

/*trace for pdd_access_nor_user.c*/
#define PDD_NOR_USER_DEBUG_SIZE_SECTOR             0x70
#define PDD_NOR_USER_DEBUG_SIZE_CHUNK              0x71
#define PDD_NOR_USER_DEBUG_CLUSTER_OLD             0x72
#define PDD_NOR_USER_DEBUG_RETURN                  0x73
#define PDD_NOR_USER_DEBUG_CLUSTER_WRITE_TO        0x74
#define PDD_NOR_USER_DEBUG_CLUSTER_NEW             0x75
#define PDD_NOR_USER_DEBUG_CLUSTER_FREE            0x76
#define PDD_NOR_USER_DEBUG_FORMAT_FLASH            0x77
#define PDD_NOR_USER_DEBUG_ERASE_FLASH             0x78
#define PDD_NOR_USER_DEBUG_CLUSTER_NOT_FORMATED    0x79
#define PDD_NOR_USER_DEBUG_CLUSTER_FORMATED        0x7a
#define PDD_NOR_USER_DEBUG_CLUSTER_LAST_USED       0x7b
#define PDD_NOR_USER_DEBUG_CLUSTER_REPLACE_OLD     0x7d
#define PDD_NOR_USER_DEBUG_CLUSTER_REPLACE_NEW     0x7e
#define PDD_NOR_USER_DEBUG_CLUSTER_TO_READ         0x7f
#define PDD_NOR_USER_DEBUG_CLUSTER_TO_READ_STATUS  0x80
#define PDD_NOR_USER_DEBUG_DATA_STREAM_HEADER      0x81
#define PDD_NOR_USER_DEBUG_DATA_STREAM_NAME        0x82
#define PDD_NOR_USER_DEBUG_LFX_OPEN                0x83
#define PDD_NOR_USER_DEBUG_ERASE_COUNTER           0x84
#define PDD_NOR_USER_DEBUG_NUMBER_READ_DATA_STREAM 0x85
#define PDD_NOR_USER_DEBUG_NUMBER_BLOCKS           0x86


/*trace for pdd_validation.c*/
#define PDD_VALIDATION_HEADER_FILE                 0x90
#define PDD_VALIDATION_CHECKSUM_FILE               0x91
#define PDD_VALIDATION_HEADER_NOR_KERNEL           0x92 
#define PDD_VALIDATION_CHECKSUM_NOR_KERNEL         0x93
#define PDD_VALIDATION_HEADER_SCC                  0x94
#define PDD_VALIDATION_CHECKSUM_SCC                0x95

/*trace for pdd_access_nor_kernel.c*/
#define PDD_NOR_KERNEL_DEBUG_NUMBER_BLOCKS         0xa0   
#define PDD_NOR_KERNEL_DEBUG_SIZE_CHUNK            0xa1
#define PDD_NOR_KERNEL_DEBUG_SIZE_SECTOR           0xa2
#define PDD_NOR_KERNEL_DEBUG_LFX_OPEN              0xa3
#define PDD_NOR_KERNEL_DEBUG_RETURN                0xa4
#define PDD_NOR_KERNEL_DEBUG_ERASE_COUNTER         0xa5
#define PDD_NOR_KERNEL_DEBUG_ERASE_FLASH           0xa6
#define PDD_NOR_KERNEL_DEBUG_SIZE_DATA             0xa7
#define PDD_NOR_KERNEL_DEBUG_VALID                 0xa8
#define PDD_NOR_KERNEL_DEBUG_SYNC_SECTOR           0xa9
#define PDD_NOR_KERNEL_DEBUG_SECTOR_INFO           0xaa

/*for error memory*/
#define PDD_NOR_ERROR_MEMORY                       0xC0

/*+++++++++++++++  error codes private ++++++++++++++++++++++++++++++++*/
/* error file access private */
#define PDD_ERROR_FILE_READ_BUFFER_TO_SMALL             -120
#define PDD_ERROR_FILE_OPEN                             -121
#define PDD_ERROR_FILE_FSTAT                            -122
#define PDD_ERROR_FILE_READ                             -123
#define PDD_ERROR_FILE_WRITE                            -124
#define PDD_ERROR_FILE_PATH_CREATE                      -125
#define PDD_ERROR_FILE_PATH_INVALID                     -126
#define PDD_ERROR_FILE_CLOSE                            -127
/* error validation private*/
#define PDD_ERROR_VALIDATION_HEADER                     -130
#define PDD_ERROR_VALIDATION_HEADER_NOR_KERNEL          -131  
/*error admin private*/
#define PDD_ERROR_ADMIN_SEM_CREATE_SEM                  -140
#define PDD_ERROR_ADMIN_SEM_DESTROY_SINCE_WAIT          -141
#define PDD_ERROR_ADMIN_SEM_DESTROY                     -142
#define PDD_ERROR_ADMIN_SEM_GET_VALUE                   -143
#define PDD_ERROR_ADMIN_NO_PROZESS_ATTACH               -144
#define PDD_ERROR_ADMIN_SHARED_MEM_NOT_VALID            -145
#define PDD_ERROR_ADMIN_SEM_WAIT                        -146
#define PDD_ERROR_ADMIN_SEM_POST                        -147
#define PDD_ERROR_ADMIN_GROUP_ACCESS                    -148

/*error SCC access private*/
#define PDD_ERROR_SCC_CALL_SOCKET                       -150
#define PDD_ERROR_SCC_CALL_DGRAM_INIT                   -151
#define PDD_ERROR_SCC_CALL_BIND                         -152
#define PDD_ERROR_SCC_CALL_CONNECT                      -153
#define PDD_ERROR_SCC_DGRAM_SEND                        -154
#define PDD_ERROR_SCC_DGRAM_RECV                        -155
#define PDD_ERROR_SCC_RECV_MSG_NOT_EXPECTED             -156
#define PDD_ERROR_SCC_RECV_LENGTH_NOT_EXPECTED          -157
#define PDD_ERROR_SCC_APPLICATION_INACTIVE              -158
#define PDD_ERROR_SCC_CALL_GET_HOST_BY_NAME             -159
#define PDD_ERROR_SCC_POLL                              -160
#define PDD_ERROR_SCC_POLL_TIMEOUT                      -161
#define PDD_ERROR_SCC_WRONG_EVENT                       -162
#define PDD_ERROR_SCC_ELEMENT_NEQ_DATAPOOL_NAME         -163
#define PDD_ERROR_SCC_READ_BUFFER_TO_SMALL              -164
#define PDD_ERROR_SCC_SEND_POOL_LENGTH_TO_GREAT         -165
#define PDD_ERROR_SCC_RECV_DATAPOOL_ID_NOT_EXPECTED     -166
#define PDD_ERROR_SCC_CALL_SOCK_OPT                     -167

/*nor user private*/
#define PDD_ERROR_NOR_USER_INIT_FAIL                    -170
#define PDD_ERROR_NOR_USER_LFX_WRONG_HANDLE             -171
#define	PDD_ERROR_NOR_USER_NO_VALID_SECTOR_SIZE         -172
#define PDD_ERROR_NOR_USER_NO_VALID_CHUNK_SIZE          -173
#define PDD_ERROR_NOR_USER_NO_VALID_NUMBER_OF_BLOCKS    -174
#define PDD_ERROR_NOR_USER_READ_FROM_CLUSTER            -175
#define PDD_ERROR_NOR_USER_WRITE_TO_CLUSTER             -176
#define PDD_ERROR_NOR_USER_WRONG_SIZE                   -177
#define PDD_ERROR_NOR_USER_NO_SPACE_CLUSTER_FULL        -178
#define PDD_ERROR_NOR_USER_DATA_STREAM_NOT_DEFINED      -179
#define PDD_ERROR_NOR_USER_ERASE_FAIL                   -180
#define PDD_ERROR_NOR_USER_READ_DUMP                    -181
#define PDD_ERROR_NOR_USER_CONFIGURATION_HEADER         -182
#define PDD_ERROR_NOR_USER_WRITE_CLUSTER                -183
#define PDD_ERROR_NOR_USER_CHECKSUM                     -184        

/*nor kernel private*/
#define PDD_ERROR_NOR_KERNEL_NO_VALID_CHUNK_SIZE          -190 
#define PDD_ERROR_NOR_KERNEL_NO_VALID_NUMBER_OF_BLOCKS    -191
#define	PDD_ERROR_NOR_KERNEL_NO_VALID_SECTOR_SIZE         -192
#define PDD_ERROR_NOR_KERNEL_INIT_FAIL                    -193
#define PDD_ERROR_NOR_KERNEL_LFX_OPEN_FAILED              -194
#define PDD_ERROR_NOR_KERNEL_NO_VALID_SECTOR              -195
#define PDD_ERROR_NOR_KERNEL_READ_BUFFER_TO_SMALL         -196
#define PDD_ERROR_NOR_KERNEL_READ_SECTOR_FAILS            -197
#define PDD_ERROR_NOR_KERNEL_READ_INVALID_HEADER          -198
#define PDD_ERROR_NOR_KERNEL_SYNC_NOT_DONE                -199
#define PDD_ERROR_NOR_KERNEL_WRITE_WRITE_INVALID_FAIL     -200
#define PDD_ERROR_NOR_KERNEL_WRITE_ERASE_FAIL             -201
#define PDD_ERROR_NOR_KERNEL_WRITE_WRITE_DATA_FAIL        -201
#define PDD_ERROR_NOR_KERNEL_WRITE_WRITE_IVALID_FAIL      -202 
#define PDD_ERROR_NOR_KERNEL_NO_POOL_EXIST                -203
#define PDD_ERROR_NOR_KERNEL_READ_PARAM                   -204
#define PDD_ERROR_NOR_KERNEL_WRITE_PARAM                  -205

#define PDD_ERROR_TRACE_OPEN_EM                           -210 
#define PDD_ERROR_TRACE_WRITE_EM                          -211

#define PDD_ERROR_NO_BUFFER                               -220

/**********************end kind traces **********************************/
#ifndef PDD_TESTMANGER_ACTIVE
  #define PDD_TRACE(u8Kind,pData,u32Len)     PDD_vPuttyConsolTrace(u8Kind,(void*)pData,u32Len)	
  #define PDD_FATAL_M_ASSERT_ALWAYS()        FATAL_M_NATIVE_ASSERT_ALWAYS() 
  #define PDD_SET_ERROR_ENTRY(PcpData)       PDD_vSetErrorEntry(PcpData);
#else
  #define PDD_TRACE(u8Kind,pData,u32Len)     	
  #define PDD_FATAL_M_ASSERT_ALWAYS()      
  #define PDD_SET_ERROR_ENTRY(PcpData)
#endif

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
#ifndef PDD_TESTMANGER_ACTIVE
void  PDD_vPuttyConsolTrace(tU8 Pu8Kind, const void* Pu8pData,tU32 Pu32Len); 
void  PDD_vSetErrorEntry(const tU8* Pu8pData);
#endif
#ifdef __cplusplus
}
#endif
#else
#error pdd_trace.h included several times
#endif
