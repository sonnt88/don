/************************************************************************
| FILE:         acoustic_trace.c
| PROJECT:      GEN2
| SW-COMPONENT: Acoustic driver
|------------------------------------------------------------------------
| This file is under RCS control (do not edit the following lines)
| $RCSfile: acoustic_trace.c,v $
| $Revision: 1.1.2.14 $
| $Date: 2005/08/02 14:49:39 $
|************************************************************************/
/* comment header doxygen <http://www.doxygen.org> generated documentation */
/* ******************************************************FileHeaderBegin** *//**
 * @file    acoustic_trace.c
 *
 * @brief   This file includes trace stuff for the acoustic driver.
 *
 * @author  3SOFT GmbH Erlangen
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; 2005 Blaupunkt GmbH, Hildesheim (Germany)
 *
 *//* ***************************************************FileHeaderEnd******* */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
//#include "trace_interface.h"

//#define ETG_S_IMPORT_INTERFACE_GENERIC
//#include "etg_if.h"     

#include <alsa/asoundlib.h>
#include "acoustic_trace.h"

#ifdef __cplusplus
extern "C" {
#endif


/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
//extern BOOL UTIL_trace_isActive(TraceData* data);
/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/********************************************************************/ /**
*  FUNCTION:      bIsAcousticInTraceActive
*
*  @brief         chekc if trace level is active
*  @return        TRUE, if active
************************************************************************/
tBool bIsAcousticInTraceActive(TR_tenTraceLevel enTraceLevel)
{
   return LLD_bIsTraceActive(ACOUSTICIN_TRACE_CLASS,(tS32)enTraceLevel);
}

/********************************************************************/ /**
*  FUNCTION:      bIsAcousticOutTraceActive
*
*  @brief         chekc if trace level is active
*  @return        TRUE, if active
************************************************************************/
tBool bIsAcousticOutTraceActive(TR_tenTraceLevel enTraceLevel)
{
   return LLD_bIsTraceActive(ACOUSTICOUT_TRACE_CLASS,(tS32)enTraceLevel);
}

/*****************************************************************************
*
* FUNCTION:
*     getArgList
*
* DESCRIPTION:
*     This stupid function is used for satifying L I N T
*     
*     
* PARAMETERS: *va_list pointer to a va_list
*
* RETURNVALUE:
*     va_list
*     
*
*
*****************************************************************************/
static va_list getArgList(va_list* a)
{
	(void)a;
	return *a;
}

/********************************************************************/ /**
*  FUNCTION:      uiCGET
*
*  @brief         return time in ms
*
*  @return        time in ms
*
*  HISTORY:
*
************************************************************************/
static unsigned int uiCGET(void)
{
    return (unsigned int)OSAL_ClockGetElapsedTime();
}

/********************************************************************/ /**
 *  FUNCTION:      vTraceAcousticDevError
 *
 *  @brief         Prints error trace message.
 *
 *  @param         u32Class           trace class
 *  @param         enTraceLevel       Message trace level
 *  @param         enFunction         Function where the error occured
 *  @param         u32OSALError       OSAL Error code
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return        none
 *
 *  HISTORY:
 *
 *  - 02.08.05 Bernd Schubart, 3SOFT
 *    Initial revision.
 ************************************************************************/
static tVoid vTraceAcousticDevError(tU32 u32Class,
                                    TR_tenTraceLevel enTraceLevel,
                                    enum enTraceFunction enFunction,
                                    tU32 u32OSALError,
                                    tPCChar copchDescription,
                                    tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{

    if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
    {
       tUInt uiIndex;
       tU8  au8Buf[12 * sizeof(tU8) + 7 * sizeof(tU32)]; //stack?
      //tU32 u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
       tU32 u32Time = (tU32)uiCGET();


       uiIndex = 0;
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) TRACE_ERROR);
       uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enFunction);
       uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
       uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
       uiIndex += sizeof(tU8);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32OSALError);
       uiIndex += sizeof(tU32);

       if(NULL != copchDescription)
       {
           memcpy(&au8Buf[uiIndex], copchDescription, 8);
       }
       else //if(NULL != copchDescription)
       {
           memset(&au8Buf[uiIndex], ' ', 8);
       } //else //if(NULL != copchDescription)
           
       uiIndex += 8 * sizeof(tU8);

       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
       uiIndex += sizeof(tU32);
       OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
       uiIndex += sizeof(tU32);

       // trace the stuff 
       LLD_vTrace(u32Class,
                  (tS32)enTraceLevel,
                  au8Buf, 
                  (tU32)uiIndex); //sizeof(au8Buf));
    }
}


 /********************************************************************/ /**
  *  FUNCTION:      vTraceAcousticDevInfo
  *
  *  @brief         Prints information trace message.
  *
  *  @param         u32Class           trace class
  *  @param         enTraceLevel       Message trace level
  *  @param         enFunction         Function where the error occured
  *  @param         copchDescription   Additional info (only first 7 chars are used)
  *  @param         u32Par1            Trace message parameter
  *  @param         u32Par2            Trace message parameter
  *  @param         u32Par3            Trace message parameter
  *  @param         u32Par4            Trace message parameter
  *
  *  @return   none
  *
  *  HISTORY:
  *
  *  - 02.08.05 Bernd Schubart, 3SOFT
  *    Initial revision.
  ************************************************************************/
static tVoid vTraceAcousticDevInfo(tU32 u32Class,
                                   TR_tenTraceLevel enTraceLevel,
                                   enum enTraceFunction enFunction,
                                   tPCChar copchDescription,
                                   tU32 u32Par1, tU32 u32Par2,
                                   tU32 u32Par3, tU32 u32Par4)
{

   if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
   {
      tUInt uiIndex;
      tU8 au8Buf[12 * sizeof(tU8) + 6 * sizeof(tU32)];
      //tU32 u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time = (tU32)uiCGET();


      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) TRACE_INFO);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) enFunction);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);

      if(NULL != copchDescription)
      {
          memcpy(&au8Buf[uiIndex], copchDescription, 8);
      }
      else //if(NULL != copchDescription)
      {
          memset(&au8Buf[uiIndex], ' ', 8);
      } //else //if(NULL != copchDescription)
      uiIndex += 8 * sizeof(tU8);
      
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par1);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par2);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par3);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Par4);
      uiIndex += sizeof(tU32);

      // send trace buffer
      LLD_vTrace(u32Class,
                 (tS32)enTraceLevel,
                 au8Buf, 
                 (tU32)uiIndex); //sizeof(au8Buf));
   }
}



/********************************************************************/ /**
 *  FUNCTION:      vTraceAcousticSrcError
 *
 *  @brief         Prints error trace message.
 *
 *  @param         enTraceLevel       Message trace level
 *  @param         enFunction         Function where the error occured
 *  @param         u32OSALError       OSAL Error code
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return   none
 *
 *  HISTORY:
 *
 *  - 11.10.12 Niyatha Rao, ECF5, RBEI
 *    Initial revision.
 ************************************************************************/
tVoid vTraceAcousticSrcError(TR_tenTraceLevel enTraceLevel,
                             enum enTraceFunction enFunction,
                             tU32 u32OSALError,
                             tPCChar copchDescription,
                             tU32 u32Par1, tU32 u32Par2,
                             tU32 u32Par3, tU32 u32Par4)
{
   vTraceAcousticDevError(ACOUSTICSRC_TRACE_CLASS, enTraceLevel,
                          enFunction, u32OSALError, copchDescription,
                          u32Par1, u32Par2, u32Par3, u32Par4);
}

/********************************************************************/ /**
 *  FUNCTION:      vTraceAcousticOutError
 *
 *  @brief         Prints error trace message.
 *
 *  @param         enTraceLevel       Message trace level
 *  @param         enFunction         Function where the error occured
 *  @param         u32OSALError       OSAL Error code
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return   none
 *
 *  HISTORY:
 *
 *  - 02.08.05 Bernd Schubart, 3SOFT
 *    Initial revision.
 ************************************************************************/
tVoid vTraceAcousticOutError(TR_tenTraceLevel enTraceLevel,
                             enum enTraceFunction enFunction,
                             tU32 u32OSALError,
                             tPCChar copchDescription,
                             tU32 u32Par1, tU32 u32Par2,
                             tU32 u32Par3, tU32 u32Par4)
{
   vTraceAcousticDevError(ACOUSTICOUT_TRACE_CLASS, enTraceLevel,
                          enFunction, u32OSALError, copchDescription,
                          u32Par1, u32Par2, u32Par3, u32Par4);
}


/********************************************************************/ /**
 *  FUNCTION:      vTraceAcousticInError
 *
 *  @brief         Prints error trace message.
 *
 *  @param         enTraceLevel       Message trace level
 *  @param         enFunction         Function where the error occured
 *  @param         u32OSALError       OSAL Error code
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return   none
 *
 *  HISTORY:
 *
 *  - 02.08.05 Bernd Schubart, 3SOFT
 *    Initial revision.
 ************************************************************************/
tVoid vTraceAcousticInError(TR_tenTraceLevel enTraceLevel,
                            enum enTraceFunction enFunction,
                            tU32 u32OSALError,
                            tPCChar copchDescription,
                            tU32 u32Par1, tU32 u32Par2,
                            tU32 u32Par3, tU32 u32Par4)
{
   vTraceAcousticDevError(ACOUSTICIN_TRACE_CLASS, enTraceLevel,
                          enFunction, u32OSALError, copchDescription,
                          u32Par1, u32Par2, u32Par3, u32Par4);
}

/********************************************************************/ /**
 *  FUNCTION:      vTraceAcousticECNRError
 *
 *  @brief         Prints error trace message.
 *
 *  @param         enTraceLevel       Message trace level
 *  @param         enFunction         Function where the error occured
 *  @param         u32OSALError       OSAL Error code
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return   none
 *
 *  HISTORY:
 *
 *    Initial revision.
 ************************************************************************/
tVoid vTraceAcousticECNRError(TR_tenTraceLevel enTraceLevel,
                            enum enTraceFunction enFunction,
                            tU32 u32OSALError,
                            tPCChar copchDescription,
                            tU32 u32Par1, tU32 u32Par2,
                            tU32 u32Par3, tU32 u32Par4)
{
   vTraceAcousticDevError(ACOUSTICECNR_TRACE_CLASS, enTraceLevel,
                          enFunction, u32OSALError, copchDescription,
                          u32Par1, u32Par2, u32Par3, u32Par4);
}


/********************************************************************/ /**
 *  FUNCTION:      vTraceAcousticOutInfo
 *
 *  @brief         Prints information trace message.
 *
 *  @param         enTraceLevel       Message trace level
 *  @param         enFunction         Function where the error occured
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return   none
 *
 *  HISTORY:
 *
 *  - 02.08.05 Bernd Schubart, 3SOFT
 *    Initial revision.
 ************************************************************************/
tVoid vTraceAcousticOutInfo(TR_tenTraceLevel enTraceLevel,
                            enum enTraceFunction enFunction,
                            tPCChar copchDescription,
                            tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
   vTraceAcousticDevInfo(ACOUSTICOUT_TRACE_CLASS, enTraceLevel,
                         enFunction, copchDescription,
                         u32Par1, u32Par2, u32Par3, u32Par4);
}

/********************************************************************/ /**
 *  FUNCTION:      vTraceAcousticInInfo
 *
 *  @brief         Prints information trace message.
 *
 *  @param         enTraceLevel       Message trace level
 *  @param         enFunction         Function where the error occured
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return   none
 *
 *  HISTORY:
 *
 *  - 02.08.05 Bernd Schubart, 3SOFT
 *    Initial revision.
 ************************************************************************/
tVoid vTraceAcousticInInfo(TR_tenTraceLevel enTraceLevel,
                           enum enTraceFunction enFunction,
                           tPCChar copchDescription,
                           tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
   vTraceAcousticDevInfo(ACOUSTICIN_TRACE_CLASS, enTraceLevel,
                         enFunction, copchDescription,
                         u32Par1, u32Par2, u32Par3, u32Par4);
}

/********************************************************************/ /**
 *  FUNCTION:      vTraceAcousticECNRInfo
 *
 *  @brief         Prints information trace message.
 *
 *  @param         enTraceLevel       Message trace level
 *  @param         enFunction         Function where the error occured
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return   none
 *
 *  HISTORY:
 *
 *    Initial revision.
 ************************************************************************/
tVoid vTraceAcousticECNRInfo(TR_tenTraceLevel enTraceLevel,
                           enum enTraceFunction enFunction,
                           tPCChar copchDescription,
                           tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
   vTraceAcousticDevInfo(ACOUSTICECNR_TRACE_CLASS, enTraceLevel,
                         enFunction, copchDescription,
                         u32Par1, u32Par2, u32Par3, u32Par4);
}


/********************************************************************/ /**
 *  FUNCTION:      vTraceAcousticSrcInfo
 *
 *  @brief         Prints information trace message.
 *
 *  @param         enTraceLevel       Message trace level
 *  @param         enFunction         Function where the error occured
 *  @param         copchDescription   Additional info (only first 7 chars are used)
 *  @param         u32Par1            Trace message parameter
 *  @param         u32Par2            Trace message parameter
 *  @param         u32Par3            Trace message parameter
 *  @param         u32Par4            Trace message parameter
 *
 *  @return   none
 *
 *  HISTORY:
 *
 *  - 11.10.12 Niyatha S Rao, ECF5, RBEI
 *    Initial revision.
 ************************************************************************/
tVoid vTraceAcousticSrcInfo(TR_tenTraceLevel enTraceLevel,
                            enum enTraceFunction enFunction,
                            tPCChar copchDescription,
                            tU32 u32Par1, tU32 u32Par2, tU32 u32Par3, tU32 u32Par4)
{
   vTraceAcousticDevInfo(ACOUSTICSRC_TRACE_CLASS, enTraceLevel,
                         enFunction, copchDescription,
                         u32Par1, u32Par2, u32Par3, u32Par4);
}


/*****************************************************************************
*
* FUNCTION:
*     vTraceAcousticOutPrintf
*
* DESCRIPTION:
*     This function creates the printf-style trace message
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
*
*****************************************************************************/
tVoid vTraceAcousticOutPrintf(TR_tenTraceLevel enTraceLevel,
                              const char* coszFormat,...)
{
   tU32 u32Class = ACOUSTICOUT_TRACE_CLASS;
   if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
   {
      tU8  au8Buf[256];  /*TODO: ? stack size ?*/
      tUInt uiIndex;
      long lSize;
      //tU32 u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time     = (tU32)uiCGET();
	  va_list argList;


	  uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) TRACE_PRINTF);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      
	  argList = getArgList(&argList);
	  va_start(argList, coszFormat);
      lSize = vsprintf(&au8Buf[uiIndex], coszFormat, argList);
      uiIndex += (tUInt)lSize;
      va_end(argList);
      
      // trace the stuff 
      LLD_vTrace(u32Class,
                 (tS32)enTraceLevel,
                 au8Buf, 
                 (tU32)uiIndex);
   }
   
}

/*****************************************************************************
*
* FUNCTION:
*     vTraceAcousticInPrintf
*
* DESCRIPTION:
*     This function creates the printf-style trace message
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
*
*****************************************************************************/
tVoid vTraceAcousticInPrintf(TR_tenTraceLevel enTraceLevel,
                              const char* coszFormat,...)
{
   tU32 u32Class = ACOUSTICIN_TRACE_CLASS;
   if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
   {
      va_list argList;
      tU8  au8Buf[256];  /*TODO: ? stack size ?*/
      tUInt uiIndex;
      long lSize;
      //tU32 u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time     = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) TRACE_PRINTF);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);

	  argList = getArgList(&argList);
      va_start(argList, coszFormat);
      lSize = vsprintf(&au8Buf[uiIndex], coszFormat, argList);
      uiIndex += (tUInt)lSize;
      va_end(argList);
      
      // send trace buffer
      LLD_vTrace(u32Class,
                 (tS32)enTraceLevel,
                 au8Buf, 
                 (tU32)uiIndex);
   }
   
}


/*****************************************************************************
*
* FUNCTION:
*     vTraceAcousticECNRPrintf
*
* DESCRIPTION:
*     This function creates the printf-style trace message
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
*
*****************************************************************************/
tVoid vTraceAcousticECNRPrintf(TR_tenTraceLevel enTraceLevel,
                              const char* coszFormat,...)
{
   tU32 u32Class = ACOUSTICECNR_TRACE_CLASS;
   if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
   {
      va_list argList;
      tU8  au8Buf[256];  /*TODO: ? stack size ?*/
      tUInt uiIndex;
      long lSize;
      //tU32 u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time     = (tU32)uiCGET();

      uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) TRACE_PRINTF);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);

	  argList = getArgList(&argList);
      va_start(argList, coszFormat);
      lSize = vsprintf(&au8Buf[uiIndex], coszFormat, argList);
      uiIndex += (tUInt)lSize;
      va_end(argList);
      
      // send trace buffer
      LLD_vTrace(u32Class,
                 (tS32)enTraceLevel,
                 au8Buf, 
                 (tU32)uiIndex);
   }
   
}


/*****************************************************************************
*
* FUNCTION:
*     vTraceAcousticSrcPrintf
*
* DESCRIPTION:
*     This function creates the printf-style trace message
*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None
*     
*
*
*****************************************************************************/
tVoid vTraceAcousticSrcPrintf(TR_tenTraceLevel enTraceLevel,
                              const char* coszFormat,...)
{
   tU32 u32Class = ACOUSTICSRC_TRACE_CLASS;
   if(LLD_bIsTraceActive(u32Class,(tS32)enTraceLevel) == TRUE)
   {
      tU8  au8Buf[256];  /*TODO: ? stack size ?*/
      tUInt uiIndex;
      long lSize;
      //tU32 u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
      OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
      tU32 u32Time     = (tU32)uiCGET();
	  va_list argList;


	  uiIndex = 0;
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) TRACE_PRINTF);
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T8(&au8Buf[uiIndex], (tU8) 0);  //reserved
      uiIndex += sizeof(tU8);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], (tU32)procID);
      uiIndex += sizeof(tU32);
      OSAL_M_INSERT_T32(&au8Buf[uiIndex], u32Time);
      uiIndex += sizeof(tU32);
      
	  argList = getArgList(&argList);
	  va_start(argList, coszFormat);
      lSize = vsprintf(&au8Buf[uiIndex], coszFormat, argList);
      uiIndex += (tUInt)lSize;
      va_end(argList);
      
      // trace the stuff 
      LLD_vTrace(u32Class,
                 (tS32)enTraceLevel,
                 au8Buf, 
                 (tU32)uiIndex);
   }
   
}

/********************************************************************/ /**
*  FUNCTION:      printLines
*  @brief         prints a large text with EOLs, splitted in lines
*  @return        void
************************************************************************/
static void printLines(char* pTxt, const char *pszHeader, tBool bIsAcousticIn)
{
  //print line by line
  if(pTxt)
  {
    char *pL = pTxt;
    char *pL1;
    char *pEOL;
    char szLine[200+1];
    size_t nLL = sizeof(szLine)-1;

    if(bIsAcousticIn)
      ACOUSTICIN_PRINTF_U1("[%s]",pszHeader);
    else
      ACOUSTICOUT_PRINTF_U1("[%s]",pszHeader);

    do //while(pEOL);
    {
      size_t nCopyLen;
      //pEOL = strchr(pL, '\n');
      pEOL = strchr(pL, 0x0a);
      pL1  = pL;
      if(pEOL)
      { //found EOL
        size_t nLineLen = (size_t)(pEOL - pL);
        nCopyLen = (nLineLen < nLL ? nLineLen : nLL);
        pL = &pEOL[1];
      }
      else //if(pEOL)
      { // did not found EOL, print leftover
        nCopyLen = nLL;
      } //else //if(pEOL)
      
      if(nCopyLen > 0)
      {
        memset(szLine, 0, nLL);
        strncpy(szLine, pL1, nCopyLen);
        if(strlen(szLine) > 0)
        {
          if(bIsAcousticIn)
            ACOUSTICIN_PRINTF_U1("[%s]",szLine);
          else
            ACOUSTICOUT_PRINTF_U1("[%s]",szLine);
        } //if(strlen(szLine) > 0)
      }
    }
    while(pEOL);
  }
}

/********************************************************************/ /**
*  FUNCTION:      vTraceAcousticDumpAlsa
*  @brief         prints ALSA-status to TTFIS
*  @return        void
************************************************************************/
void vTraceAcousticDumpAlsa(snd_pcm_t *pcm, tBool bIsAcousticIn)
{
  int err;
  snd_output_t *outputp = NULL;
  err = snd_output_buffer_open(&outputp);
  if(err >= 0)
  {
    if(NULL != pcm)
    {
      char *pC = NULL;
      snd_pcm_status_t *status = NULL;
      snd_pcm_status_alloca(&status); /*lint !e717*/
      snd_pcm_state_t sndState;
      size_t nValid;

      
      //dump state
      sndState = snd_pcm_state(pcm);
      if(bIsAcousticIn)
        ACOUSTICIN_PRINTF_U1("snd_pcm_state: [%d] == [%s]",
                             (int)sndState, snd_pcm_state_name(sndState));
      else
        ACOUSTICOUT_PRINTF_U1("snd_pcm_state: [%d] == [%s]",
                              (int)sndState, snd_pcm_state_name(sndState));

      //dump status
      err = snd_pcm_status(pcm, status);
      if(err >= 0)
      {
        snd_pcm_status_dump(status, outputp);
        nValid = snd_output_buffer_string(outputp, &pC);
        snd_output_flush(outputp);
        if(pC && nValid > 0)
        {
          printLines(pC, "****snd_pcm_status_dump****", bIsAcousticIn);
        } //if(pC && nValid > 0)
      } //if(err >= 0)

      //dump PCM
      snd_pcm_dump(pcm, outputp);       
      nValid = snd_output_buffer_string(outputp, &pC);
      snd_output_flush(outputp);
      if(pC && nValid > 0)
      {
        printLines(pC, "****snd_pcm_dump****", bIsAcousticIn);
      } //if(pC && nValid > 0)

      //dump setup
      snd_pcm_dump_setup(pcm, outputp);       
      nValid = snd_output_buffer_string(outputp, &pC);
      snd_output_flush(outputp);
      if(pC && nValid > 0)
      {
        printLines(pC, "****AcIn-snd_pcm_dump_setup****", bIsAcousticIn);
      } //if(pC && nValid > 0)
    } //if(NULL != pcm)
    snd_output_close(outputp);
  } //if(err  >= 0)
}


#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file 
|-----------------------------------------------------------------------*/

