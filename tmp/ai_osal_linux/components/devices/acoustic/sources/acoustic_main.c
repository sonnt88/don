/************************************************************************
| FILE:         acoustic_main.c
| PROJECT:      Gen3
| SW-COMPONENT: Acoustic driver
|------------------------------------------------------------------------
|************************************************************************/
/* ******************************************************FileHeaderBegin** *//**
* @file    acoustic_main.c
*
* @brief 
*
* @author  srt2hi
*//* ***************************************************FileHeaderEnd******* */
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"

#include "acousticout_public.h"
#include "acousticin_public.h"
#include "acousticsrc_public.h"
#include "acousticecnr_public.h"


#ifdef __cplusplus
extern "C" {
#endif


OSAL_DECL tS32 acousticout_drv_io_open(tS32 s32ID, tCString szName,
                             OSAL_tenAccess enAccess,
                             tU32 *pu32FD, tU16  app_id)
{
  (void)enAccess;
  return (tS32)ACOUSTICOUT_u32IOOpen(s32ID, szName, OSAL_EN_READONLY, pu32FD, app_id);
}

OSAL_DECL tS32 acousticout_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   return (tS32)ACOUSTICOUT_u32IOClose(s32ID, u32FD);
}

OSAL_DECL tS32 acousticout_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun,
                                tS32 s32arg)
{
   return (tS32)ACOUSTICOUT_u32IOControl(s32ID, u32FD, s32fun, s32arg);
}

OSAL_DECL tS32 acousticout_drv_io_write(tS32 s32ID, tU32 u32FD, tPCS8 pBuffer,
                              tU32 u32Size, tU32 *ret_size)
{
  (void)ret_size;
  return (tS32)ACOUSTICOUT_u32IOWrite(s32ID, u32FD, pBuffer, u32Size, NULL);
}


OSAL_DECL tS32 acousticin_drv_io_open(tS32 s32ID, tCString szName,
                            OSAL_tenAccess enAccess, tU32 *pu32FD,
                            tU16  app_id)
{
  (void)enAccess;
  return (tS32)ACOUSTICIN_u32IOOpen(s32ID,szName,OSAL_EN_READONLY,pu32FD,app_id);
}

OSAL_DECL tS32 acousticin_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   return (tS32)ACOUSTICIN_u32IOClose(s32ID, u32FD);
}

OSAL_DECL tS32 acousticin_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{
   return (tS32)ACOUSTICIN_u32IOControl(s32ID, u32FD, s32fun, s32arg);
}

OSAL_DECL tS32 acousticin_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer,
                            tU32 u32Size, tU32 *ret_size)
{
   return (tS32)ACOUSTICIN_u32IORead(s32ID,u32FD, pBuffer, u32Size, ret_size);
}

OSAL_DECL tS32 acousticsrc_drv_io_open(tS32 s32ID, tCString szName,
                             OSAL_tenAccess enAccess, tU32 *pu32FD,
                             tU16  app_id)
{
  (void)enAccess;
  return ACOUSTICSRC_s32IOOpen(s32ID,szName,OSAL_EN_READONLY, pu32FD, app_id);
}

OSAL_DECL tS32 acousticsrc_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   return ACOUSTICSRC_s32IOClose(s32ID, u32FD);
}

OSAL_DECL tS32 acousticsrc_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun,
                                tS32 s32arg)
{
   return ACOUSTICSRC_s32IOControl(s32ID, u32FD, s32fun, s32arg);
}


OSAL_DECL tS32 acousticcnr_drv_io_open(tS32 s32ID, tCString szName,
                             OSAL_tenAccess enAccess, tU32 *pu32FD,
                             tU16  app_id)
{
  (void)enAccess;
  return ACOUSTICECNR_s32IOOpen(s32ID,szName,OSAL_EN_READONLY,pu32FD,app_id);
}

OSAL_DECL tS32 acousticcnr_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   return ACOUSTICECNR_s32IOClose(s32ID, u32FD);
}

OSAL_DECL tS32 acousticcnr_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun,
                                tS32 s32arg)
{
   return ACOUSTICECNR_s32IOControl(s32ID, u32FD, s32fun, s32arg);
}

OSAL_DECL tS32 acousticcnr_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size,
                             tU32 *ret_size)
{
   return ACOUSTICECNR_s32IORead(s32ID,u32FD, pBuffer, u32Size, ret_size);
}

/*static void vTraceIOString(tCString cBuffer)
{
  tU16 u16Len;
  char au8Buf[255];

  memset((void*)au8Buf,'\0',240);
  OSAL_M_INSERT_T8(&au8Buf[0],OSAL_STRING_OUT);
  u16Len = (tU16)strlen(cBuffer);
  if(u16Len > 240) u16Len = 240;
  strncpy(&au8Buf[1],cBuffer,u16Len);
  LLD_vTrace(TR_COMP_OSALIO, (tU32)TR_LEVEL_FATAL,au8Buf,u16Len+2);
}
*/

static sem_t       *initacoustic_lock;
void __attribute__ ((constructor)) acoustic_process_attach(void)
{
   //tBool bFirstAttach = FALSE;

   initacoustic_lock = sem_open("ACOU_INIT",O_EXCL | O_CREAT, OSAL_ACCESS_RIGTHS, 0);
   if (initacoustic_lock != SEM_FAILED)
   {
      if(s32OsalGroupId)
      {
         if(chown("/dev/shm/sem.ACOU_INIT",(uid_t)-1, (gid_t)s32OsalGroupId) == -1)
         {
             vWritePrintfErrmem("acoustic_process_attach  -> chown error %d \n",errno);
         }
         if(chmod("/dev/shm/sem.ACOU_INIT", OSAL_ACCESS_RIGTHS) == -1)
         {
             vWritePrintfErrmem("acoustic_process_attach chmod -> fchmod error %d \n",errno);
         }
      }
      //bFirstAttach = TRUE;
   }
   else
   {
       initacoustic_lock = sem_open("ACOU_INIT", 0);
       //bFirstAttach = FALSE;
       sem_wait(initacoustic_lock);
   }
   /*
   if(bFirstAttach)
   {
   }
   else
   {
   }*/
   sem_post(initacoustic_lock);
}

#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file 
|-----------------------------------------------------------------------*/

