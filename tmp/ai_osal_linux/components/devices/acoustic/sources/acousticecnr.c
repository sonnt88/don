
/************************************************************************
| FILE:         acousticecnr.c
| PROJECT:      Gen3
| SW-COMPONENT: AcousticECNR driver
|------------------------------------------------------------------------
******************************************************FileHeaderBegin****
* @file    acousticecnr.c
*
* @brief   This device is used by ECNR to record the audio streams.
*
* @author  ysu1kor 
*
* @date 03/01/2013
*
* @version initial
*
* @Copyrigt note
*  (c)2014 -  RBEI - Robert Bosch Engineering and Business Solutions Limited
*
** ***************************************************FileHeaderEnd******* */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"               /**< OSAL types etc. */
#include "osalio_public.h"
#include <alsa/asoundlib.h>
#include "acousticecnr_public.h"     /**< public interface for this product */
#include "acoustic_trace.h"        /**< trace stuff for this product */


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define NUM_ARRAY_ELEMS(array)  (sizeof(array)/sizeof(array[0]))
  
/************************************************************************
| types definition (scope: local)
|-----------------------------------------------------------------------*/


/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static tU32  u32AbortStream(tS32 s32ID);
static tU32  u32DoReadOperation(tS32 s32ID, tU32 u32FD,
								OSAL_trAcousticECNRRead* prReadInfo,
								tPU32 pu32BytesRead);
static tU32  u32RcvFromAlsa(tS32 s32ID, snd_pcm_uframes_t *pFrameBuffer,
							  int iBufSizeBytes,
							  tPU32 pu32BytesRead); 
static tU32  u32InitAlsa(tS32 s32ID);
static tU32  u32UnInitAlsa(tS32 s32ID);
static tBool bIsSampleformatValid(OSAL_tenAcousticSampleFormat enSampleformat);
static tBool bIsSamplerateValid(OSAL_tAcousticSampleRate nSamplerate);
static tBool bIsChannelnumValid(tU16 u16Channelnum);
static tBool bIsBuffersizeValid(tU32 u32Buffersize);
static tVoid vResetErrorCounters (tS32 s32ID);
static tVoid vResetErrorThresholds (tS32 s32ID);

static tU32 u32AcousticECNR_IOCtrl_Version(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_Extread(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static void AcousticECNR_IOCtrl_RegNotification(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_WaitEvent(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_GetSuppSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_GetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_SetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_GetSuppSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_GetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_SetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_GetSuppChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_GetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_SetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_GetSuppBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_GetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_SetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_SetErrThr(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_Start(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_Stop(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32AcousticECNR_IOCtrl_Abort(tS32 s32ID, tU32 u32FD, tS32 s32Arg);

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/** version for returning with IOControl OSAL_C_S32_IOCTRL_VERSION */
#define ACOUSTICECNR_C_S32_IO_VERSION                (tS32)(0x00010000)
#define ACOUSTICECNR_ALSA_DEVICE_NAME_BUFFER_SIZE    64



/** play states */
typedef enum
{
  A_ECNR_EN_STATE_UNINITIALIZED = 0,  /**< driver uninitialized (==0!) */
  A_ECNR_EN_STATE_INITIALIZED   = 1,  /**< driver initialized */
  A_ECNR_EN_STATE_STOPPED       = 2,  /**< device opened but inactive */
  A_ECNR_EN_STATE_ACTIVE        = 4  /**< audio stream active */
} ACOUSTICECNR_enPlayStates;

/** file handles for the streams */
enum  ACOUSTICECNR_enFileHandles
{
    A_ECNR_EN_HANDLE_ID_NOTDEFINED = -1,
    A_ECNR_EN_HANDLE_ID_SPEECHRECO = 1 // file handle for speechreco stream
};


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/
/** error threshold data */
typedef struct
{
    /** Configured thresholds indicating when an error is signalled */
    tS32    s32XrunErrThr;       /**< threshold for xrun error */

    /** Counters indicating how many error have occurred until now */
    tS32    s32XrunErrCnt;       /**< # of xrun errors */
} trACOUSTICECNR_ErrThresholdData;

/** mechanisms for DSP communication */
typedef struct
{
  ACOUSTICECNR_enPlayStates       enPlayState;   /**< current play state */
  trACOUSTICECNR_ErrThresholdData rErrThreshold; /**< error threshold data */
  OSAL_trAcousticECNRCallbackReg  rCallback;     /**< callback data */
} trACOUSTICECNR_DrvInterface_type;


// PCM config data 
typedef struct
{
    OSAL_tAcousticSampleRate     nSampleRate;    //< current sample rate
    OSAL_tenAcousticSampleFormat enSampleFormat; //< current sample format
    tU16                         u16NumChannels; //< current number of channels
} trPCMConfigData;


// Internal status + configuration data 
typedef struct
{
	tBool                          bAlsaIsOpen;
	   /**< Alsa open flag */
	snd_pcm_t                      *pAlsaPCM;               
	   /**< pointer to handle for PCM Alsa device */
	char                     
		szAlsaDeviceName[ACOUSTICECNR_ALSA_DEVICE_NAME_BUFFER_SIZE];
	   /**< device name as configured in .asoundrc */
	snd_output_t                   *pSndOutput;             
	   /**< alsa message output handle*/
	snd_pcm_hw_params_t            *pPCM_hw_params;
	   /**< pointer to hardware config for PCM Alsa device */

    OSAL_tAcousticBuffersize       anBufferSize[OSAL_EN_ACOUSTIC_CODECLAST];
	    /**< buffer size for each codec (PCM) */
    tU32                           u32ReadTimeout;
	    /**< timeout for read operation */    
    trACOUSTICECNR_DrvInterface_type rDRVIf;
	    /**< channels & config for DSP communication */
    trPCMConfigData                rPCMConfig;
	    /**< PCM config data */
    enum ACOUSTICECNR_enFileHandles  enFileHandle;
	    /**< file handle */
} trACOUSTICECNR_StateData;


/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

static tBool ACOUSTICECNR_garbInit[ACOUSTICECNR_DEVID_LAST]
             = {FALSE};

/** internal state variables, for each stream */
static trACOUSTICECNR_StateData garStateData[ACOUSTICECNR_DEVID_LAST]
                              = {0};


/** List of PCM sample formats supported by the device */
static const OSAL_tenAcousticSampleFormat aenSupportedSampleformats[] =
{
    ACOUSTICECNR_C_EN_DEFAULT_PCM_SAMPLEFORMAT,
    OSAL_EN_ACOUSTIC_SF_S16LE      /*!< signed 16 bit, little endian */ 

};

/** List of PCM sample rates supported by the device (interval begin value) */
static const OSAL_tAcousticSampleRate anSupportedSampleratesFrom[] =
{
    ACOUSTICECNR_C_U32_DEFAULT_PCM_SAMPLERATE
};

/** List of PCM sample rates supported by the device (interval end value) */
static const OSAL_tAcousticSampleRate anSupportedSampleratesTo[] =
{
    48000
};

/** List of PCM channel numbers supported by the device */
static tCU16 au16SupportedChannelnums[] =
{
    ACOUSTICECNR_C_U8_DEFAULT_NUM_CHANNELS_SPEECHRECO,
    2
    
};

/** List of buffer sizes supported by the device */
static tCU32 au32SupportedBuffersizesPCM[] =
{
    1024,
    ACOUSTICECNR_C_U32_DEFAULT_BUFFER_SIZE_PCM,
    4096,
    8192,
   16384
};

/********************************************************************/ /**
*  FUNCTION:      iSlash2CamelCase
*
*  @brief         removes slashes from input string
*                 Add a first Letter 'A'
*                 changes Next letter after slash to capital letter
*                 Example: /dev/acoustic/speech -> AdevSpeechAcousticSpeech
*                 
*
*  @return        returns number of characters copied into Outputbuffer
*                 (without trailing '\0')
*  @retval        int
*
*  HISTORY:
************************************************************************/
static int
iSlash2CamelCase(const char *pcszSlashes,
				 char *pszOutputBuffer,
				 int iOutputBufferSize)
{
    int   iIn = 0;
    int   iOut = 0;
	tBool bMakeCapital  = FALSE;
	tBool bIsFirstSlash = TRUE; //first character after '/' leave lower case
    char  c;

	pszOutputBuffer[iOut++] = 'A';
    while('\0' != (c = pcszSlashes[iIn++])
          &&
          ((iOut+1) < iOutputBufferSize))
    {
        if('/' == c)
        {
			//letter after first slash leave in lower case
            bMakeCapital = bIsFirstSlash ? FALSE : TRUE;
			bIsFirstSlash = FALSE;
        }
        else//if('/' == c)
        {
        	if(bMakeCapital)
       		{
       			if((c >= 'a')&&(c <= 'z'))
					c = c - 0x20;
       		}
            pszOutputBuffer[iOut++] = c;
            bMakeCapital = FALSE;
        } //else//if('/' == c)
    } //while('\0' != (c = pcszSlashes[iIn++]) && !bOverflow)

    pszOutputBuffer[iOut] = '\0'; //trailing zero
    return iOut;
}


/********************************************************************/ /**
*  FUNCTION:      iConvertOsalDeviceID
*
*  @brief         converts OSAL to local IDs
*
*  @return        int local ID
*  @retval        int
*
*  HISTORY:
************************************************************************/
static tS32 s32ConvertOsalDeviceID(tS32 s32OsalDeviceID)
{
	tS32 s32ID;

	switch(s32OsalDeviceID)
	{
	case OSAL_EN_DEVID_AC_ECNR_IF_SPEECH:
		s32ID = (tS32)ACOUSTICECNR_DEVID_SPEECHRECO;
		break;
	default:
		s32ID = (tS32)ACOUSTICECNR_DEVID_SPEECHRECO;
		break;
	} //switch(s32OsalDeviceID)

	return s32ID;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICECNR_vDumpAlsaStatus
*
*  @brief         prints ALSA-status to TTFIS
*
*  @return        void
*  @retval        void
*
*  HISTORY:
************************************************************************/
static void ACOUSTICECNR_vDumpAlsaStatus(tS32 s32ID)
{
        int err;
        snd_output_t *outputp = NULL;
        
        if ((err = snd_output_buffer_open(&outputp)) < 0)
        {
          ACOUSTICECNR_PRINTF_ERRORS("ALSA buffer open error: %s",
                                   snd_strerror(err));
        }
        else //if ((err = snd_output_buffer_open(&outputp)) < 0)
        {
            snd_pcm_status_t *status = NULL;

            //snd_pcm_status_alloca(&status);
            snd_pcm_status_malloc(&status);
            
            if(NULL != garStateData[s32ID].pAlsaPCM)
            {
                if((err = snd_pcm_status(garStateData[s32ID].pAlsaPCM, status))
                    < 0)
                {
                  ACOUSTICECNR_PRINTF_ERRORS("ALSA status error: %s",
                                           snd_strerror(err));
                  //exit(0);
                }
                else
                {
                  char *pC = NULL;
                  snd_pcm_status_dump(status, outputp);				
                  snd_output_buffer_string(outputp, &pC);

                  if(pC)
                  {
                      ACOUSTICECNR_PRINTF_U3("\n****AcIn-snd_pcm_status_dump"
                                           "****:\n%s", pC);
                  } //if(pC)
                }

            } //if(NULL = garStateData[s32ID].pAlsaPCM)
            snd_output_close(outputp);
        } //else //if ((err = snd_output_buffer_open(&outputp)) < 0)
}



/********************************************************************/ /**
*  FUNCTION:      set_swparams
*
*  @brief         set soft params
*
*  @return        
*
*  HISTORY:
*
************************************************************************/
static int set_swparams(snd_pcm_t *handle, unsigned int uiSamplerate)
{
    int err;
    snd_pcm_sw_params_t *swparams;
    snd_pcm_uframes_t uframeStopTreshold;
    snd_pcm_uframes_t uframeStartTreshold;
    snd_pcm_sw_params_malloc(&swparams);

    (void)uiSamplerate;
    /* get the current swparams */
    err = snd_pcm_sw_params_current(handle, swparams);
    if (err < 0)
	{
            ACOUSTICECNR_PRINTF_ERRORS("set_swparams Unable to determine "
                                     "current swparams for playback: %s",
                                     snd_strerror(err));
    }

	//debug
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_avail_min(swparams,      &val);
        ACOUSTICECNR_PRINTF_U3("set_swparams avail_min: %u, Err %i",
                             (unsigned int)val, (int)err);
    }
	//debug
    {
        snd_pcm_uframes_t val;
        int iVal = 0;
        err = snd_pcm_sw_params_get_period_event(swparams,      &iVal);
        val = (snd_pcm_uframes_t)iVal;
        ACOUSTICECNR_PRINTF_U3("set_swparams period_event: %u, Err %i",
                             (unsigned int)val, (int)err);
    }
	//debug
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_stop_threshold(swparams, &val);
        uframeStopTreshold = val;
        ACOUSTICECNR_PRINTF_U3("set_swparams stop_threshold: %u, Err %i",
                             (unsigned int)uframeStopTreshold, err);
    }
	//debug
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_start_threshold(swparams, &val);
        uframeStartTreshold = val;
        ACOUSTICECNR_PRINTF_U3("set_swparams start_threshold: %u, Err %i",
                             (unsigned int)uframeStartTreshold, err);
    }
	//debug
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_silence_threshold(swparams, &val);
        ACOUSTICECNR_PRINTF_U3("set_swparams silence_threshold: %u, Err %i",
                             (unsigned int)val, err);
    }
	//debug
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_silence_size(swparams, &val);
        ACOUSTICECNR_PRINTF_U3("set_swparams silence_size: %u, Err %i",
                             (unsigned int)val, err);
    }

    {
        uframeStartTreshold = 1;
        err = snd_pcm_sw_params_set_start_threshold(handle, swparams,
			                                        uframeStartTreshold);
        if (err < 0)
        {
                ACOUSTICECNR_PRINTF_ERRORS("set_swparams Unable to set "
                                        "start threshold mode for playback: %s",
                                        snd_strerror(err));
        }
    }
    
    /* write the parameters to the playback device */
    err = snd_pcm_sw_params(handle, swparams);
    if (err < 0) {
            ACOUSTICECNR_PRINTF_ERRORS("set_swparams Unable to set sw params"
                                     " for playback: %s",
                                     snd_strerror(err));
    }


    /* get the current swparams */
    err = snd_pcm_sw_params_current(handle, swparams);
    if (err < 0) {
            ACOUSTICECNR_PRINTF_ERRORS("set_swparams Unable to determine"
                                     " current swparams for playback: %s",
                                     snd_strerror(err));
    }

    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_avail_min(swparams,      &val);
        ACOUSTICECNR_PRINTF_U3("set_swparams avail_min: %u, Err %i",
                             (unsigned int)val, err);
    }
    {
        snd_pcm_uframes_t val;
        int iVal = 0;
        err = snd_pcm_sw_params_get_period_event(swparams,      &iVal);
        val = (snd_pcm_uframes_t)iVal;
        ACOUSTICECNR_PRINTF_U3("set_swparams period_event: %u, Err %i",
                             (unsigned int)val, err);
    }
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_stop_threshold(swparams, &val);
        uframeStopTreshold = val;
        ACOUSTICECNR_PRINTF_U3("set_swparams stop_threshold: %u, Err %i",
                              (unsigned int)uframeStopTreshold, err);
    }
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_start_threshold(swparams, &val);
        uframeStartTreshold = val;
        ACOUSTICECNR_PRINTF_U3("set_swparams start_threshold: %u, Err %i",
                             (unsigned int)uframeStartTreshold, err);
    }
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_silence_threshold(swparams, &val);
        ACOUSTICECNR_PRINTF_U3("set_swparams silence_threshold: %u, Err %i",
                             (unsigned int)val, err);
    }
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_silence_size(swparams, &val);
        ACOUSTICECNR_PRINTF_U3("set_swparams silence_size: %u, Err %i",
                             (unsigned int)val, err);
    }
    return 0;
}


/************************************************************************
|function prototype (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
/********************************************************************/ /**
*  FUNCTION:       ACOUSTICECNR_alsaConvertFormatOsalToAlsa
*  @brief         Convert Format Osal To Alsa
*  @return        Alsa format ID
*  @retval        
*
*  HISTORY:
************************************************************************/
static snd_pcm_format_t
ACOUSTICECNR_alsaConvertFormatOsalToAlsa(OSAL_tenAcousticSampleFormat
                                                              nOsalSampleFormat)
{
  snd_pcm_format_t nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;

  switch(nOsalSampleFormat)
  {
	 case OSAL_EN_ACOUSTIC_SF_S8:    /*!< signed 8 bit */ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_S8;
	   break;
	 case OSAL_EN_ACOUSTIC_SF_S16:   /*!< signed 16 bit: CPU endian */ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_S16;
	   break;
	 case OSAL_EN_ACOUSTIC_SF_S32:   /*!< signed 32 bit: CPU endian */ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_S32;
	   break;
	 case OSAL_EN_ACOUSTIC_SF_F32:   /*!< float (IEEE 754) 32 bit: CPU endian */ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT;
	   break;
	 case OSAL_EN_ACOUSTIC_SF_S16LE: /*!< signed 16 bit: little endian */ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;
	   break;
	 case OSAL_EN_ACOUSTIC_SF_S32LE: /*!< signed 32 bit: little endian */ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_S32_LE;
	   break;
	 case OSAL_EN_ACOUSTIC_SF_F32LE: /*!< float(IEEE 754) 32bit: little endian*/ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT_LE;
	   break;
	 case OSAL_EN_ACOUSTIC_SF_S16BE:  /*!< signed 16 bit: big endian */ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_S16_BE;
	   break;
	 case OSAL_EN_ACOUSTIC_SF_S32BE: /*!< signed 32 bit, big endian */ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_S32_BE;
	   break;
  case OSAL_EN_ACOUSTIC_SF_F32BE:  /*!< float (IEEE 754) 32 bit, big endian */ 
	   nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT_BE;
	   break;
	 default:
         ACOUSTICECNR_PRINTF_ERRORS("ACOUSTICECNR_alsaConvertFormatOsalToAlsa ERROR:"
                                  " cannot convert OSAL-format (%d)",
                                  (int)nOsalSampleFormat);
	   nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;
  } //switch(nOsalSampleFormat)

  ACOUSTICECNR_PRINTF_U3("ACOUSTICECNR_alsaConvertFormatOsalToAlsa :"
                       " convert OSAL %d to Alsa-format %d",
                       (int)nOsalSampleFormat, (int)nAlsaSampleFormat);
  return nAlsaSampleFormat;
}

/********************************************************************/ /**
*  FUNCTION: ACOUSTICECNR_alsaGetBytesPerSample     
*  @brief    returns bytes per sample     
*  @return   bytes per sample     
*  @retval        
*
*  HISTORY:
************************************************************************/
static int
ACOUSTICECNR_alsaGetBytesPerSample(snd_pcm_format_t nAlsaSampleFormat)
{
  int iBPS = 1;

    switch (nAlsaSampleFormat)
    {
        case SND_PCM_FORMAT_S8:
            iBPS = 1 ; //sizeof(char);
            break;
        case SND_PCM_FORMAT_S16_BE:  /*!< signed 16 bit: big endian */ 
        case SND_PCM_FORMAT_S16_LE:
            iBPS = 2 ; //sizeof(short int);
            break;
        case SND_PCM_FORMAT_S32_BE: /*!< signed 32 bit, big endian */ 
        case SND_PCM_FORMAT_S32_LE:
            iBPS = 4; //sizeof(int);
            break;
        case SND_PCM_FORMAT_FLOAT_BE: /*!< float (IEEE 754) 32bit,big endian */ 
        case SND_PCM_FORMAT_FLOAT_LE: /*!< float (IEEE 754) 32bit:lit endian*/ 
            iBPS = 4; //sizeof(float);
            break;
        default:
            ACOUSTICECNR_PRINTF_ERRORS("ACOUSTICECNR_alsaGetBytesPerSample ERROR: "
				                     "cannot find Alsa-format (%d)",
									 (int)nAlsaSampleFormat);
            iBPS = 1; //sizeof(char);
    } //switch(nOsalSampleFormat)

  return iBPS;
}

/********************************************************************/ /**
*  FUNCTION:  ACOUSTICECNR_osalGetBytesPerSample     
*  @brief         
*  @return   Bytes per sample
*  @retval        
*
*  HISTORY:
************************************************************************/
static int
ACOUSTICECNR_osalGetBytesPerSample(OSAL_tenAcousticSampleFormat nOsalSampleFormat)
{
  int iBPS = 1;

    switch (nOsalSampleFormat)
    {
        case OSAL_EN_ACOUSTIC_SF_S8:    /*!< signed 8 bit */ 
            iBPS = 1 ; //sizeof(char);
            break;
        case OSAL_EN_ACOUSTIC_SF_S16BE:  /*!< signed 16 bit: big endian */ 
        case OSAL_EN_ACOUSTIC_SF_S16LE: /*!< signed 16 bit: little endian */ 
        case OSAL_EN_ACOUSTIC_SF_S16:
            iBPS = 2 ; //sizeof(short int);
            break;
        case OSAL_EN_ACOUSTIC_SF_S32BE: /*!< signed 32 bit, big endian */ 
        case OSAL_EN_ACOUSTIC_SF_S32LE: /*!< signed 32 bit: little endian */ 
        case OSAL_EN_ACOUSTIC_SF_S32:   /*!< signed 32 bit: CPU endian */ 
            iBPS = 4 ; //sizeof(int);
            break;
        case OSAL_EN_ACOUSTIC_SF_F32BE: /*!< float (IEEE 754) 32 bit, big end */ 
        case OSAL_EN_ACOUSTIC_SF_F32LE: /*!< float (IEEE 754) 32 bit: lit end */ 
        case OSAL_EN_ACOUSTIC_SF_F32: /*!< float (IEEE 754) 32 bit: lit end */ 
            iBPS = 4; //sizeof(float);
            break;
        default:
            ACOUSTICECNR_PRINTF_ERRORS("ACOUSTICECNR_osalGetBytesPerSample ERROR: "
				                     "cannot find OSAL-format (%d)",
									 (int)nOsalSampleFormat);
            iBPS = 1; //sizeof(char);
    } //switch(nOsalSampleFormat)

  return iBPS;
}



/********************************************************************/ /**
*  FUNCTION:      ACOUSTICECNR_s32InitPrivate
*
*  @brief         Initializes any necessary resources.
*                 Should be called at OSAL startup.
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR   on success
*
*  HISTORY:
*
*    Initial revision.
************************************************************************/
static tS32 ACOUSTICECNR_s32InitPrivate(tS32 s32ID)
{
  tS32 s32ErrorCode = (tS32)OSAL_E_NOERROR;

  ACOUSTICECNR_PRINTF_U3("ACOUSTICECNR_s32Init - START");


  if(!ACOUSTICECNR_garbInit[s32ID])
  {
    ACOUSTICECNR_garbInit[s32ID] = TRUE;
	
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_ACECNR_INIT,
					   "init", (tU32)0, (tU32)0, 0, __LINE__);
	
	
	garStateData[s32ID].enFileHandle=A_ECNR_EN_HANDLE_ID_NOTDEFINED;
	switch (s32ID)
	{
		case ACOUSTICECNR_DEVID_SPEECHRECO:
			garStateData[s32ID].enFileHandle
				          = A_ECNR_EN_HANDLE_ID_SPEECHRECO;
			break;

		default:
		break;
	}
	
	if(garStateData[s32ID].enFileHandle!=A_ECNR_EN_HANDLE_ID_NOTDEFINED)
	{
	   /* we are now in state "Initialized" */
	   garStateData[s32ID].rDRVIf.enPlayState = A_ECNR_EN_STATE_INITIALIZED;
	}
  } //if(!ACOUSTICECNR_garbInit[s32ID])

  ACOUSTICECNR_PRINTF_U3("ACOUSTICECNR_s32Init - END u32ErrorCode0x %08X",
                       (unsigned int)s32ErrorCode);
  return s32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      ACOUSTICECNR_s32DeinitPrivate
*
*  @brief         Releases any resources acquired during init
*                 Before this function is called (e.g. by Powermanagement),
*                 the device must have been closed. This cannot be done
*                 here, because then the device would still be marked as
*                 used in the dispatcher.
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR   on success
*
*  HISTORY:
*
*    Initial revision.
************************************************************************/
static tS32 ACOUSTICECNR_s32DeinitPrivate(tS32 s32ID)
{
    tS32 s32ErrorCode = (tS32)OSAL_E_NOERROR;

    if(ACOUSTICECNR_garbInit[s32ID])
    {
		ACOUSTICECNR_garbInit[s32ID] = FALSE;
        garStateData[s32ID].rDRVIf.enPlayState 
			                = A_ECNR_EN_STATE_UNINITIALIZED;
    } //if(ACOUSTICECNR_garbInit[s32ID])

    return s32ErrorCode;
}




/********************************************************************/ /**
*  FUNCTION:      ACOUSTICECNR_s32IOOpen
*
*  @brief         Opens the acousticecnr device "/dev/acousticecnr/xxx" 
*                 where xxx is the stream type to be opened.
*
*  @param         s32ID
*                   ID of the stream to open
*  @param         szName
*                   Should be OSAL_C_STRING_DEVICE_ACOUSTICECNR+"/xxx"
*  @param         enAccess
*                   File access mode (should always be OSAL_EN_WRITEONLY 
*                   for this device although value is ignored for now)
*  @param         pu32FD
*                   for storing the filehandle      
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_NOTINITIALIZED
*  @retval        OSAL_E_ALREADYOPENED
*  @retval        OSAL_E_UNKNOWN
*
*  HISTORY:
*

*    Initial revision.
************************************************************************/
tS32 
ACOUSTICECNR_s32IOOpen(tS32 s32ID,
					 tCString szName,
                     OSAL_tenAccess enAccess,
					 tPU32 pu32FD,
					 tU16 appid)
{
	int iLen;
	const char *pcszShortDeviceName;
	const char *pcszLongDeviceName;
	char szFullOsalDeviceName[ACOUSTICECNR_ALSA_DEVICE_NAME_BUFFER_SIZE];
	size_t nShortLen;
	size_t nLongLen;
	size_t nFullLen;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_ACECNR_OPEN,
						 "enter", (tU32)s32ID,
						 (tU32)enAccess,
						 pu32FD != NULL ? (tU32)(*pu32FD) : (tU32)-1,
						 (tU32)appid);


	pcszShortDeviceName = NULL == szName ? "" : szName;
	nShortLen = strlen(pcszShortDeviceName);


	// get osal base device name, append "/szName"
	switch(s32ID)
	{
	case OSAL_EN_DEVID_AC_ECNR_IF_SPEECH:
	   pcszLongDeviceName = OSAL_C_STRING_DEVICE_ACOUSTICECNR_IF_SPEECHRECO;
	break;
	default:  //fall back: s32ID is used as Index!
	   s32ID = (tS32)OSAL_EN_DEVID_AC_ECNR_IF_SPEECH;
	   pcszLongDeviceName = OSAL_C_STRING_DEVICE_ACOUSTICECNR_IF_SPEECHRECO;
	break;
	} //switch(s32ID)

	nLongLen  = strlen(pcszLongDeviceName); 
	nFullLen  = nLongLen + nShortLen + 2;  //+2: trailing zero and a '/'
	if(nFullLen <= ACOUSTICECNR_ALSA_DEVICE_NAME_BUFFER_SIZE)
	{
		strcpy(szFullOsalDeviceName,
			   pcszLongDeviceName);
		strcat(szFullOsalDeviceName,"/");
		strcat(szFullOsalDeviceName,pcszShortDeviceName);
	}
	else //if(nFullLen <= ACOUSTICECNR_ALSA_DEVICE_NAME_BUFFER_SIZE)
	{ //error buffer to short
		vTraceAcousticECNRError(TR_LEVEL_ERRORS, EN_ACECNR_OPEN, OSAL_E_UNKNOWN,
							   "2short", 
							   (tU32)ACOUSTICECNR_ALSA_DEVICE_NAME_BUFFER_SIZE,
							   (tU32)nFullLen,
							   (tU32)nLongLen,
							   (tU32)nShortLen);

		return (tS32)OSAL_E_UNKNOWN;
	} //else //if(nFullLen <= ACOUSTICECNR_ALSA_DEVICE_NAME_BUFFER_SIZE)

	s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

	ACOUSTICECNR_PRINTF_U4("ACOUSTICECNR_s32IOOpen FullOsalDeviceName <%s>\n",
						  szFullOsalDeviceName);

	ACOUSTICECNR_s32InitPrivate(s32ID); //ash

	memset(garStateData[s32ID].szAlsaDeviceName,
		   0,
		   ACOUSTICECNR_ALSA_DEVICE_NAME_BUFFER_SIZE);
	iLen = iSlash2CamelCase(szFullOsalDeviceName,
							garStateData[s32ID].szAlsaDeviceName,
							ACOUSTICECNR_ALSA_DEVICE_NAME_BUFFER_SIZE);
	ACOUSTICECNR_PRINTF_U4("ACOUSTICECNR_s32IOOpen CamelCase <%s>, Len %d\n",
						  garStateData[s32ID].szAlsaDeviceName, iLen);
	if(iLen <= 0)
	{
		vTraceAcousticECNRError(TR_LEVEL_ERRORS, EN_ACECNR_OPEN, OSAL_E_UNKNOWN,
							   "noname", 
							   (tU32)iLen,
							   (tU32)0,
							   (tU32)0,
							   (tU32)0);

		return (tS32)OSAL_E_UNKNOWN;
	} //if(iLen <= 0)


    if(s32ID<0 || s32ID >= ACOUSTICECNR_DEVID_LAST)
    {
        return (tS32)OSAL_E_INVALIDVALUE;
    }
    // If the driver is not yet initialized, the open request fails
    if(garStateData[s32ID].rDRVIf.enPlayState < A_ECNR_EN_STATE_INITIALIZED)
    {
        vTraceAcousticECNRError(TR_LEVEL_ERRORS,
                              EN_ACECNR_OPEN, OSAL_E_NOTINITIALIZED,
                              "noinit",
                              (tU32)garStateData[s32ID].rDRVIf.enPlayState,
                              0, 0, 0);
        return (tS32)OSAL_E_NOTINITIALIZED;
    }

    // If the stream is already opened, the open request fails 
    if(garStateData[s32ID].rDRVIf.enPlayState > A_ECNR_EN_STATE_INITIALIZED)
    {
        vTraceAcousticECNRError(TR_LEVEL_ERRORS,
                              EN_ACECNR_OPEN,
                              OSAL_E_ALREADYOPENED,
                              "alropen",
                              (tU32)garStateData[s32ID].rDRVIf.enPlayState,
                              0, 0, 0);
        return (tS32)OSAL_E_ALREADYOPENED;
    }

    /* Initialize internal state variables to defaults */
    garStateData[s32ID].rPCMConfig.enSampleFormat =
                                       ACOUSTICECNR_C_EN_DEFAULT_PCM_SAMPLEFORMAT;
    garStateData[s32ID].rPCMConfig.nSampleRate    = 
                                        ACOUSTICECNR_C_U32_DEFAULT_PCM_SAMPLERATE;
    garStateData[s32ID].rPCMConfig.u16NumChannels =
                                ACOUSTICECNR_C_U8_DEFAULT_NUM_CHANNELS_SPEECHRECO;
    garStateData[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM] =
                                       ACOUSTICECNR_C_U32_DEFAULT_BUFFER_SIZE_PCM;
    garStateData[s32ID].u32ReadTimeout  = ACOUSTICECNR_C_U32_DEFAULT_READ_TIMEOUT;

    vResetErrorCounters(s32ID);
    vResetErrorThresholds(s32ID);
 
    /* new state is "Stopped" */
    garStateData[s32ID].rDRVIf.enPlayState = A_ECNR_EN_STATE_STOPPED;  

    /* return filehandle */
    if(pu32FD != NULL)
    {
        *pu32FD = (tU32)garStateData[s32ID].enFileHandle; /*lint !e571 */  
    }  

    vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_ACECNR_OPEN,
                         "exitok",   (tU32)s32ID, (tU32)enAccess, 0, 0);

    return (tS32)OSAL_E_NOERROR;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICECNR_s32IOClose
*
*  @brief         Closes the acousticecnr device.
*                 When the device is reopened afterwards, all configured 
*                 parameters, registered callbacks will start with the 
*                 internal defaults again.   
*
*  @param         s32ID  stream ID
*  @param         u32FD  file handle
*
*  @return        OSAL error code
*
*  HISTORY:
*
*    Initial revision.
************************************************************************/
tS32 ACOUSTICECNR_s32IOClose(tS32 s32ID, tU32 u32FD)
{
	tU32 u32RetVal = OSAL_E_NOERROR;
	s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

    vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_ACECNR_CLOSE,
                         "enter", (tU32)s32ID, (tU32)u32FD, 0, 0);

    if(s32ID<0 || s32ID >= ACOUSTICECNR_DEVID_LAST)
    { //error
		u32RetVal = OSAL_E_INVALIDVALUE;
		vTraceAcousticECNRError(TR_LEVEL_ERRORS,
							  EN_ACECNR_CLOSE,
							  OSAL_E_INVALIDVALUE,
							  "invval",
							  (tU32)s32ID, u32FD, u32RetVal, 0);
    }
    else //if(s32ID<0 || s32ID >= ACOUSTICECNR_DEVID_LAST)
    { //OK
		// If the device is not open, the close request will fail 
		if(garStateData[s32ID].rDRVIf.enPlayState < A_ECNR_EN_STATE_STOPPED)
		{ //error
			u32RetVal = OSAL_E_DOESNOTEXIST;
			vTraceAcousticECNRError(TR_LEVEL_ERRORS,
								  EN_ACECNR_CLOSE,
								  OSAL_E_DOESNOTEXIST,
								  "noexist",
								  (tU32)s32ID, u32FD, u32RetVal, 0);
		}
		else //if(garStateData....enPlayState < A_ECNR_EN_STATE_STOPPED)
		{ //ok
			/* threads blocked in write will
			   be cancelled in the abort call below */
			/* flush buffers and clean up */
			(tVoid) u32AbortStream(s32ID);

			/*close alsa device, only if opened*/
			(tVoid) u32UnInitAlsa(s32ID);

			/* new state is "Uninitialized" */
			garStateData[s32ID].rDRVIf.enPlayState =
											  A_ECNR_EN_STATE_UNINITIALIZED; 

			vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_ACECNR_CLOSE,
								 "exitok",
								 (tU32)s32ID,
								 (tU32)u32FD,
								 u32RetVal, 0);
		} //else //if(garStateData...enPlayState < A_ECNR_EN_STATE_STOPPED)
        ACOUSTICECNR_s32DeinitPrivate(s32ID);
	} //else //if(s32ID<0 || s32ID >= ACOUSTICECNR_DEVID_LAST)

    return (tS32)u32RetVal;
}


/********************************************************************/ /**
*  FUNCTION:      ACOUSTICECNR_s32IOControl
*
*  @brief         Executes IO-Control for the driver
*
*  @param[in]     s32ID
*                   stream ID
*  @param[in]     u32FD
*                   file handle
*  @param[in]     s32Fun
*                   IO-Control to be executed
*  @param[in,out] s32Arg
*                   extra argument (depending on s32Fun)
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_INVALIDVALUE
*  @retval        OSAL_E_NOTSUPPORTED 
*
*  HISTORY:
*
*  1. Initial revision.
*  2. Modified to call each of the functionalities through a sub function rather than directly
*     This has been done to reduce the cyclomatic code complexity
************************************************************************/
tS32 ACOUSTICECNR_s32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun, tS32 s32Arg)
{
    tU32 u32ErrorCode = OSAL_E_NOERROR;

	s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

    vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_ACECNR_IOCTRL,
                         "enter",
                         (tU32)s32ID, u32FD, (tU32)s32Fun, (tU32)s32Arg);

    switch(s32Fun)
    {
	/*---------------------------------------------------------------------*/
	/* The driver version is returned                                      */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_VERSION:
        {
    		u32ErrorCode = u32AcousticECNR_IOCtrl_Version(s32ID, u32FD, s32Arg);
            break;
        }

	/*---------------------------------------------------------------------*/
	/* Reads audio data from the driver by providing a pointer to the data */
	/* (including additional control information) to the driver.           */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_EXTREAD:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_Extread(s32ID, u32FD, s32Arg);
            break;
        }

	/*---------------------------------------------------------------------*/
	/* This IOControl registers a callback function that is used for       */
	/* notification about certain events.                                  */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_REG_NOTIFICATION:
        {
			AcousticECNR_IOCtrl_RegNotification(s32ID, u32FD, s32Arg);
            break;      
        }

	/*---------------------------------------------------------------------*/
	/* Blocks until new event occurs (or until timeout happens),           */
	/* this is an alternative to registering a notification callback.      */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_WAITEVENT:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_WaitEvent(s32ID, u32FD, s32Arg);
	        break;
        }
        
	/*---------------------------------------------------------------------*/
	/* Retrieves the sample rate cap. for a specified acoustic codec.      */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_GETSUPP_SAMPLERATE:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_GetSuppSamplerate(s32ID, u32FD, s32Arg);
            break;              
        }

	/*---------------------------------------------------------------------*/
	/* Gets the sample rate for a specified acoustic codec.                */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_GETSAMPLERATE:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_GetSamplerate(s32ID, u32FD, s32Arg);
            break;              
        }

	/*---------------------------------------------------------------------*/
	/* Sets the sample rate for a specified acoustic codec.                */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_SETSAMPLERATE:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_SetSamplerate(s32ID, u32FD, s32Arg);
            break;             
        }

	/*---------------------------------------------------------------------*/
	/* Retrieves the sample format cap. for a specified acoustic codec.    */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_GETSUPP_SAMPLEFORMAT:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_GetSuppSampleformat(s32ID, u32FD, s32Arg);
            break;              
        }

	/*---------------------------------------------------------------------*/
	/* Gets the format of the samples for a specified acoustic codec.      */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_GETSAMPLEFORMAT:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_GetSampleformat(s32ID, u32FD, s32Arg);
            break;                 
        }

	/*---------------------------------------------------------------------*/
	/* Sets the format of the samples for a specified acoustic codec.      */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_SETSAMPLEFORMAT:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_SetSampleformat(s32ID, u32FD, s32Arg);
            break;              
        }

	/*---------------------------------------------------------------------*/
	/* Retrieves the channel num cap.                                      */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_GETSUPP_CHANNELS:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_GetSuppChannels(s32ID, u32FD, s32Arg);
            break;             
        }
        
	/*---------------------------------------------------------------------*/
	/* Gets the number of channels to be used for the audio stream.        */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_GETCHANNELS:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_GetChannels(s32ID, u32FD, s32Arg);
            break;             
        }

	/*---------------------------------------------------------------------*/
	/* Sets the number of channels to be used for the audio stream.        */
	/*---------------------------------------------------------------------*/     
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_SETCHANNELS:
        {			
			u32ErrorCode = u32AcousticECNR_IOCtrl_SetChannels(s32ID, u32FD, s32Arg);
            break;          
        }

	/*---------------------------------------------------------------------*/
	/* Retrieves the buffer size capabilities                              */
	/*---------------------------------------------------------------------*/      
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_GETSUPP_BUFFERSIZE:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_GetSuppBuffersize(s32ID, u32FD, 
				                                                    s32Arg);
            break;              
        }

	/*---------------------------------------------------------------------*/
	/* Gets the size of buffers to be used for the audio stream.           */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_GETBUFFERSIZE:   
        {			
			u32ErrorCode = u32AcousticECNR_IOCtrl_GetBuffersize(s32ID, u32FD, s32Arg);
            break;               
        }

	/*---------------------------------------------------------------------*/
	/* Sets the size of buffers to be used for the audio stream.           */
	/*---------------------------------------------------------------------*/     
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_SETBUFFERSIZE:
        {			
        	u32ErrorCode = u32AcousticECNR_IOCtrl_SetBuffersize(s32ID, u32FD, s32Arg);
            break;               
        }

	/*---------------------------------------------------------------------*/
	/* Sets the error thresholds for the audio stream.                     */
	/*---------------------------------------------------------------------*/     
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_SETERRTHR:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_SetErrThr(s32ID, u32FD, s32Arg);
            break; 
        }

	/*---------------------------------------------------------------------*/
	/* Starts the audio transfer. After starting the stream, the           */
	/* client can read audio data by calling the OSAL read                 */
	/* operation  continuously.                                            */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_START:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_Start(s32ID, u32FD, s32Arg);
            break;
        }

	/*---------------------------------------------------------------------*/
	/* Initiates stopping of a running audio transfer.                     */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_STOP:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_Stop(s32ID, u32FD, s32Arg);
            break;
        }
        
	/*---------------------------------------------------------------------*/
	/* Aborts (=flushs) running audio input immediately.                   */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_ABORT:
        {
			u32ErrorCode = u32AcousticECNR_IOCtrl_Abort(s32ID, u32FD, s32Arg);
            break;
        }

	/*---------------------------------------------------------------------*/
	/* Pauses running audio transfer (unsupported by frontend)             */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_PAUSE:
        {
			u32ErrorCode = OSAL_E_NOTSUPPORTED;
            break;
        }

	/*---------------------------------------------------------------------*/
	/* sets the filter coefficients for micadc                             */
	/*---------------------------------------------------------------------*/
    case OSAL_C_S32_IOCTRL_ACOUSTICECNR_SET_FILTER_COEF:
    	{
    		u32ErrorCode = OSAL_E_NOTSUPPORTED;
    		break;
    	}
	/*---------------------------------------------------------------------*/
	/* Default case: not supported argument given                          */
	/*---------------------------------------------------------------------*/
    default:
        {
            u32ErrorCode = OSAL_E_NOTSUPPORTED;
            break;               
        }
    }

    vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_ACECNR_IOCTRL,
                         "exit", (tU32)s32ID,
                         u32FD, (tU32)s32Fun, u32ErrorCode);

    return (tS32)u32ErrorCode;
}

/*************************************************************************/ /**
*  FUNCTION:      ACOUSTICECNR_s32IORead
*
*  @brief         Read audio data from the driver by providing a pointer 
*                 to a buffer. This function uses a posix style read, 
*                 therefore it acts as a wrapper to another function,
*                 "u32DoReadOperation", which supports additional information.
*                 The additional information is not used here and filled
*                 with defaults
*
*  @param[in]     s32ID
*                   stream ID
*  @param[in]     u32FD
*                   file handle
*  @param[out]     ps8Buffer
*                   Pointer to the buffer where new audio data shall be 
*                   stored by the driver
*  @param[in]     u32Size
*                   Denotes the size of the buffer content (in bytes) for 
*                   the buffer pointed to by pcs8Buffer.
*  @param[out]    pu32Read
*                   Number of bytes to read from the driver
*
*  @return        OSAL error code     
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
tS32 ACOUSTICECNR_s32IORead(tS32 s32ID,
						  tU32 u32FD,
						  tPS8 ps8Buffer,
						  tU32 u32Size,
						  tPU32 pu32Read)
{
    OSAL_trAcousticECNRRead rReadInfo;
    tU32 u32Ret;
	tU32 u32BytesRead = 0;

	s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

    vTraceAcousticECNRInfo(TR_LEVEL_USER_3, EN_ACECNR_READ,
                         "enter", (tU32)s32ID, (tU32)u32FD, u32Size, __LINE__);

    if(ps8Buffer==NULL || pu32Read ==NULL)
    {
        return (tS32)OSAL_E_INVALIDVALUE;
    }
    
    /* init ext write struct with default values for posix-style write */
    rReadInfo.nTimeout      = ACOUSTICECNR_C_U32_DEFAULT_READ_TIMEOUT;
    rReadInfo.pvBuffer      = (tPVoid)ps8Buffer;
    rReadInfo.u32BufferSize = u32Size;
    rReadInfo.u32Timestamp  = 0;
    
    /* do the read */
    u32Ret = u32DoReadOperation(s32ID, u32FD, &rReadInfo, &u32BytesRead);

    if(OSAL_E_NOERROR != u32Ret)
    {
        vTraceAcousticECNRError(TR_LEVEL_ERRORS, EN_ACECNR_READ, (tU32)u32Ret,
                              "read", (tU32)s32ID, (tU32)u32FD, 0, __LINE__);
		*pu32Read = 0;
        return (tS32)u32Ret;
    }

    *pu32Read = u32BytesRead;

    vTraceAcousticECNRInfo(TR_LEVEL_USER_3, EN_ACECNR_READ,
                         "exit", (tU32)s32ID, (tU32)u32FD,
                         rReadInfo.u32BufferSize, u32Ret);

    return (tS32)*pu32Read;
}

/*************************************************************************/ /**
*  FUNCTION:      u32DoReadOperation
*
*  @brief         worker function for reading data to the acousticecnr driver,
*                 can be used for posix-style read, as well as for the
*                 EXTREAD IOCOntrol
*
*  @param[in]     s32ID             stream ID
*  @param[in]     u32FD             file handle
*  @param[in,out] prReadInfo        additional control information
*
*  @return       Osal error code
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tU32 u32DoReadOperation(tS32 s32ID, tU32 u32FD,
							   OSAL_trAcousticECNRRead* prReadInfo,
							   tPU32 pu32BytesRead)
{
    tU32   u32Ret;

    vTraceAcousticECNRInfo(TR_LEVEL_USER_3, EN_DO_READOPERATION,
                         "enter", (tU32)s32ID,
                         (tU32)u32FD, prReadInfo->u32BufferSize, 0);

	*pu32BytesRead = 0;

    /* if we are not in state "active", no read operation is allowed */
    if (A_ECNR_EN_STATE_ACTIVE != garStateData[s32ID].rDRVIf.enPlayState)
    {
        vTraceAcousticECNRError(TR_LEVEL_ERRORS,
                              EN_DO_READOPERATION, OSAL_E_NOACCESS, "state",
                              (tU32)garStateData[s32ID].rDRVIf.enPlayState,
                              0, 0, __LINE__);

        return OSAL_E_NOACCESS;
    }

    if( (NULL == prReadInfo)
            ||
            (NULL == prReadInfo->pvBuffer) )
    {
        vTraceAcousticECNRError(TR_LEVEL_ERRORS,
                              EN_DO_READOPERATION, OSAL_E_INVALIDVALUE,
                              "nullp",
                              (tU32)garStateData[s32ID].rDRVIf.enPlayState,
                              0, 0, __LINE__);

        /* Nullpointer given */
        return OSAL_E_INVALIDVALUE;
    }

    if(prReadInfo->u32BufferSize
       <
       garStateData[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM])
    {
        vTraceAcousticECNRError(TR_LEVEL_ERRORS,
		EN_DO_READOPERATION, OSAL_E_NOSPACE,
		"size",
		(tU32)garStateData[s32ID].rDRVIf.enPlayState,prReadInfo->u32BufferSize,
		garStateData[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM],
		__LINE__);

        /* given size is too big */
        return OSAL_E_NOSPACE;
    }

    /* Now read audio buffer from Alsa */
    u32Ret = u32RcvFromAlsa(s32ID,
                              prReadInfo->pvBuffer,
                              (int)prReadInfo->u32BufferSize,
							  pu32BytesRead);
   
    if ( OSAL_E_NOERROR == u32Ret )
    {
        prReadInfo->u32BufferSize =
					garStateData[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM];
    }
    else
    {
        vTraceAcousticECNRError(TR_LEVEL_ERRORS,
					 EN_DO_READOPERATION,
					 u32Ret,
					 "driver",
					 (tU32)garStateData[s32ID].rDRVIf.enPlayState,
					 prReadInfo->u32BufferSize,
					 garStateData[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM],
					 __LINE__);
        prReadInfo->u32BufferSize = 0;        
    }

    vTraceAcousticECNRInfo(TR_LEVEL_USER_3, EN_DO_READOPERATION,"exit",
                         (tU32)s32ID,
                         (tU32)prReadInfo->nTimeout,
                         prReadInfo->u32BufferSize, u32Ret);

    return u32Ret;
}

/*************************************************************************/ /**
*  FUNCTION:      u32RcvFromAlsa
*
*  @brief         Receives an audio buffer from the DSP
*
*  @param[in]     s32ID            stream ID
*  @param[out]    pu8Buffer        buffer for read audio data
*  @param[in]     u32Size          size of provided buffer
*
*  @return        OSAL error code
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tU32 u32RcvFromAlsa(tS32 s32ID,
							 snd_pcm_uframes_t *pFrameBuffer,
							 int iBufSizeBytes,
							 tPU32 pu32BytesRead)
{
    tU32 u32RetVal = OSAL_E_NOERROR;
    int  iFramesRead;
    int  iBufSizeFrames;
	unsigned short *pusDbg;

	*pu32BytesRead = 0;


    iBufSizeFrames = iBufSizeBytes
                     /
                     (ACOUSTICECNR_osalGetBytesPerSample
                      (garStateData[s32ID].rPCMConfig.enSampleFormat)
                      *
                      garStateData[s32ID].rPCMConfig.u16NumChannels
                     );


    memset(pFrameBuffer, 0 , (unsigned int)iBufSizeBytes);

    iFramesRead = snd_pcm_readi(garStateData[s32ID].pAlsaPCM,
                               pFrameBuffer,
                               (snd_pcm_uframes_t)iBufSizeFrames);

	ACOUSTICECNR_PRINTF_U4("u32RcvFromAlsa:snd_pcm_read:"
						 "iFramesRead %d, "
						 "iBufSizeFrames %d, "
						 "enSampleFormat %d, "
						 "Channels %d, "
						 "Samplerate %d",
                         iFramesRead,
						 iBufSizeFrames,
						 (int)garStateData[s32ID].rPCMConfig.enSampleFormat,
						 (int)garStateData[s32ID].rPCMConfig.u16NumChannels,
						 (int)garStateData[s32ID].rPCMConfig.nSampleRate
						 );

	pusDbg = (unsigned short*)(void*)pFrameBuffer;
	ACOUSTICECNR_PRINTF_U4("u32RcvFromAlsa:snd_pcm_read: (hex16)"
						 "[0-7] %04X %04X %04X %04X %04X %04X %04X %04X ",
						 (unsigned int)pusDbg[0],
						 (unsigned int)pusDbg[1],
						 (unsigned int)pusDbg[2],
						 (unsigned int)pusDbg[3],
						 (unsigned int)pusDbg[4],
						 (unsigned int)pusDbg[5],
						 (unsigned int)pusDbg[6],
						 (unsigned int)pusDbg[7]
						 );


	if(iFramesRead < 0)
	{
        int err;
        snd_pcm_status_t *status;

        u32RetVal = OSAL_E_IOERROR;

		ACOUSTICECNR_PRINTF_ERRORS("u32RcvFromAlsa:ERROR snd_pcm_readi:"
						   " %d == <%s>",
						   iFramesRead, snd_strerror(iFramesRead));
        snd_pcm_status_malloc(&status);
        err = snd_pcm_status(garStateData[s32ID].pAlsaPCM, status);
        if (err < 0)
        {
            ACOUSTICECNR_PRINTF_ERRORS("u32RcvFromAlsa:ERROR "
                                     "Stream status error: %s",
                                     snd_strerror(err));
        }
        else //if (err < 0)
        {
            ACOUSTICECNR_vDumpAlsaStatus(s32ID);
        } //else //if (err < 0)

        snd_pcm_prepare(garStateData[s32ID].pAlsaPCM);
	}
	else //if(iFramesRead < 0)
	{
        *pu32BytesRead  = (tU32)iFramesRead
                        *
                        (tU32)ACOUSTICECNR_osalGetBytesPerSample
                              (garStateData[s32ID].rPCMConfig.enSampleFormat)
                        *
                        (tU32)garStateData[s32ID].rPCMConfig.u16NumChannels
                        ;

        if (iFramesRead != iBufSizeFrames)
        {

              ACOUSTICECNR_PRINTF_ERRORS("u32RcvFromAlsa:ERROR snd_pcm_readi:"
                                   " ReadFrames %d != BufferSize %u",
                                   iFramesRead, iBufSizeFrames);

        }//if (iFramesRead != iBufSizeFrames)
	} //else //if(iFramesRead < 0)
	
    return u32RetVal;
}


/*************************************************************************/ /**
*  FUNCTION:      u32InitAlsa
*
*  @brief         Initializes the Alsa system
*
*  @param         s32ID   stream ID
*
*  @return        OSAL error code
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tU32 u32InitAlsa(tS32 s32ID)
{
    int err;
    int iBpS; //BytesPerSample
    const char *pcszDevice;
    int iDirection;
    unsigned int uiSampleRate;
    unsigned int uiPeriods;
    snd_pcm_uframes_t framesBufferSize;
    snd_pcm_uframes_t framesPeriodSize;

    vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_INIT_ALSA,
                         "enter", (tU32)s32ID, 0, 0, 0);

    snd_output_stdio_attach(&garStateData[s32ID].pSndOutput, stdout, 0);
    

    /* Open PCM device for recording (capture). */
	pcszDevice = garStateData[s32ID].szAlsaDeviceName;
	err = snd_pcm_open( &garStateData[s32ID].pAlsaPCM, pcszDevice,
						 SND_PCM_STREAM_CAPTURE, 0 );
    
    if (err < 0) 
    {
      ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa:unable to open pcm device (%s): %s",
                               pcszDevice,
                               snd_strerror(err) );
       return OSAL_E_UNKNOWN;
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_malloc(&garStateData[s32ID].pPCM_hw_params);

    /* Fill it in with default values. */
    err = snd_pcm_hw_params_any( garStateData[s32ID].pAlsaPCM,
                                 garStateData[s32ID].pPCM_hw_params);
    if (err < 0)
    {
      ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa:cannot set default params (%s)",
                               snd_strerror (err));
    }


    /* Set the desired hardware parameters. */

    /* Interleaved mode */

    err = snd_pcm_hw_params_set_access(garStateData[s32ID].pAlsaPCM,
                                       garStateData[s32ID].pPCM_hw_params,
                                       SND_PCM_ACCESS_RW_INTERLEAVED );
    if (err < 0)
    {
      ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa:cannot set access type (%s)",
                                snd_strerror (err));
    }


    err = snd_pcm_hw_params_set_format(garStateData[s32ID].pAlsaPCM,
							   garStateData[s32ID].pPCM_hw_params,
							   ACOUSTICECNR_alsaConvertFormatOsalToAlsa(
							   garStateData[s32ID].rPCMConfig.enSampleFormat));
    if (err < 0)
    {
      ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa:cannot set format (%s)",
                               snd_strerror (err));
    }


    err = snd_pcm_hw_params_set_channels(garStateData[s32ID].pAlsaPCM,
								 garStateData[s32ID].pPCM_hw_params,
								 garStateData[s32ID].rPCMConfig.u16NumChannels);
    if(err < 0)
    {
      ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa:cannot set channels"
							   "(Channels:%u) (%s)",
				   (unsigned int)garStateData[s32ID].rPCMConfig.u16NumChannels,
				   snd_strerror (err));
    }

    uiSampleRate = garStateData[s32ID].rPCMConfig.nSampleRate;
    iDirection   = 0;
    err = snd_pcm_hw_params_set_rate_near(garStateData[s32ID].pAlsaPCM,
                                          garStateData[s32ID].pPCM_hw_params,
                                          &uiSampleRate, &iDirection);
    if(err < 0)
    {
      ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa:cannot "
                               "set rate_near (rate:%u) <%s>",
                               uiSampleRate,
                               snd_strerror (err));
    }
    garStateData[s32ID].rPCMConfig.nSampleRate = uiSampleRate;


    iDirection = 0;
    err = snd_pcm_hw_params_get_periods_min(garStateData[s32ID].pPCM_hw_params,
                                            &uiPeriods, &iDirection);
    if (err < 0)
    {
        ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa ERROR: "
                                 "cannot get periods_min <%s>",
                                 snd_strerror (err));
    }
    else //if (err < 0)
    {
        ACOUSTICECNR_PRINTF_U3("s32InitAlsa INFO: "
                             "get periods_min <%u> Dir <%d>",
                             uiPeriods,
                             iDirection);
    } //else //if (err < 0)

    iDirection = 0;
    err = snd_pcm_hw_params_get_periods_max(garStateData[s32ID].pPCM_hw_params,
                                            &uiPeriods,
                                            &iDirection);
    if (err < 0)
    {
        ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa ERROR: "
                                 "cannot get periods_max <%s>",
                                 snd_strerror (err));

    }
    else //if (err < 0)
    {
        ACOUSTICECNR_PRINTF_U3("s32InitAlsa INFO: "
                             "get periods_max <%u> Dir <%d>",
                             uiPeriods,
                             iDirection);
    } //else //if (err < 0)
    
    framesBufferSize = uiSampleRate;
    err = snd_pcm_hw_params_set_buffer_size_near(garStateData[s32ID].pAlsaPCM,
											 garStateData[s32ID].pPCM_hw_params,
											 &framesBufferSize);
    if (err < 0)
    {
        ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa ERROR: cannot set "
                                 "buffer size (%u frames) (%s)",  
                                 (unsigned int)framesBufferSize, 
                                 snd_strerror (err));
    }
    else //if (err < 0)
    {
        ACOUSTICECNR_PRINTF_U3("s32InitAlsa INFO: "
                             "buffer size == %u frames",
                             (unsigned int)framesBufferSize);
    } //else //if (err < 0)

    iBpS = ACOUSTICECNR_alsaGetBytesPerSample(
            ACOUSTICECNR_alsaConvertFormatOsalToAlsa(
                       garStateData[s32ID].rPCMConfig.enSampleFormat)
                                           );
    framesPeriodSize =
    (snd_pcm_uframes_t)garStateData[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM]
       /
       (snd_pcm_uframes_t)(iBpS <= 0 ? 1 : iBpS);
    iDirection = 0;
    err = snd_pcm_hw_params_set_period_size_near(garStateData[s32ID].pAlsaPCM,
											 garStateData[s32ID].pPCM_hw_params,
											 &framesPeriodSize, &iDirection);
	if(err < 0)
    {
        ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa ERROR: "
                                 "cannot set period size (%u frames) (%s)",
                                 (unsigned int)framesPeriodSize,
                                 snd_strerror(err));
    }
    else //if (err < 0)
    {
        ACOUSTICECNR_PRINTF_U3("s32InitAlsa INFO: "
                             "period size == %u frames, dir %d",
                             (unsigned int)framesPeriodSize,
                             iDirection);
    } //else //if (err < 0)

    err = snd_pcm_hw_params(garStateData[s32ID].pAlsaPCM,
                             garStateData[s32ID].pPCM_hw_params);
    if ( err < 0)
    {
       ACOUSTICECNR_PRINTF_ERRORS("s32InitAlsa:unable to set hw parameters: %s",
                                 snd_strerror(err) );
       return OSAL_E_UNKNOWN;
    } //if ( err < 0)

    snd_pcm_hw_params_free(garStateData[s32ID].pPCM_hw_params);
    garStateData[s32ID].pPCM_hw_params = NULL;

    set_swparams(garStateData[s32ID].pAlsaPCM,
                 garStateData[s32ID].rPCMConfig.nSampleRate);
    
    garStateData[s32ID].bAlsaIsOpen = TRUE;

    vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_INIT_ALSA,
                         "exit", (tU32)s32ID,
						 (tU32)garStateData[s32ID].bAlsaIsOpen, 0, 0);
    return OSAL_E_NOERROR;
}

/*************************************************************************/ /**
*  FUNCTION:      u32UnInitAlsa
*
*  @brief         UnInitializes the Alsa
*
*  @param         s32ID   stream ID
*
*  @return        OSAL error code
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tU32 u32UnInitAlsa(tS32 s32ID)
{
    tU32 u32Ret = OSAL_E_NOERROR;

    if(garStateData[s32ID].pAlsaPCM != NULL)
    {
       //ALSA CLOSE
       garStateData[s32ID].bAlsaIsOpen = FALSE;
       snd_pcm_close(garStateData[s32ID].pAlsaPCM);
       garStateData[s32ID].pAlsaPCM = NULL;
    }
    else //if(garStateData[s32ID].pAlsaPCM != NULL)
    {
       u32Ret = OSAL_E_DOESNOTEXIST;
       vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_UNINITOUTPUTDEVICE,
                         "notexist", (tU32)s32ID, 0, 0, 0);
    } //else //if(garStateData[s32ID].pAlsaPCM != NULL)

    return u32Ret;
}


/*************************************************************************/ /**
*  FUNCTION:      u32AbortStream
*
*  @brief         Aborts stream immediately
*
*  @param         s32ID              stream ID
*
*  @return        OSAL error code
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tU32 u32AbortStream(tS32 s32ID)
{   
    tU32 u32RetVal = OSAL_E_NOERROR;

    /* new state is "Stopped" */
    garStateData[s32ID].rDRVIf.enPlayState = A_ECNR_EN_STATE_STOPPED;

    return u32RetVal;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsSampleformatValid
*
*  @brief         Checks whether provided sample format value is valid
*
*  @param         enSampleformat  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tBool bIsSampleformatValid(OSAL_tenAcousticSampleFormat enSampleformat)
{
    tU8 u8Idx;
    for (u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(aenSupportedSampleformats); u8Idx++)
    {
        if (aenSupportedSampleformats[u8Idx] == enSampleformat)
        {
            return TRUE;
        }
    }

    return FALSE;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsSamplerateValid
*
*  @brief         Checks whether provided sample rate value is valid
*
*  @param         nSamplerate  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*

*    Initial revision.
*****************************************************************************/
static tBool bIsSamplerateValid(OSAL_tAcousticSampleRate nSamplerate)
{
    tU8 u8Idx;
    for (u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(anSupportedSampleratesFrom); u8Idx++)
    {
        if ((anSupportedSampleratesFrom[u8Idx] <= nSamplerate) &&
                (anSupportedSampleratesTo[u8Idx] >= nSamplerate))
        {
            return TRUE;
        }
    }

    return FALSE;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsChannelnumValid
*
*  @brief         Checks whether provided channel num value is valid
*
*  @param         u16Channelnum  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*

*    Initial revision.
*****************************************************************************/
static tBool bIsChannelnumValid(tU16 u16Channelnum)
{
    tU8 u8Idx;
    for (u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(au16SupportedChannelnums); u8Idx++)
    {
        if (au16SupportedChannelnums[u8Idx] == u16Channelnum)
        {
            return TRUE;
        }
    }

    return FALSE;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsBuffersizeValid
*
*  @brief         Checks whether provided buffer size value is valid
*
*  @param         u32Buffersize  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*

*    Initial revision.
*****************************************************************************/
static tBool bIsBuffersizeValid(tU32 u32Buffersize)
{
    tU8 u8Idx;
    for (u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM); u8Idx++)
    {
        if (au32SupportedBuffersizesPCM[u8Idx] == u32Buffersize)
        {
            return TRUE;
        }
    }

    return FALSE;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_Version
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
* Initial version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_Version(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	if (s32Arg)
	{
    	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_VERSION,
    								 "enter",
    								 (tU32)s32ID, u32FD, (tU32)s32Arg,
    								 (tU32)garStateData[s32ID].rDRVIf.enPlayState);		
	   	/* write version */
	   	*(tPS32)s32Arg = ACOUSTICECNR_C_S32_IO_VERSION;
		
    	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_VERSION,
    						 "exit", (tU32)s32ID,
    						 u32FD, (tU32)s32Arg,
    						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);		
	}
	else
	{
		vTraceAcousticECNRError(TR_LEVEL_ERRORS, EN_IOCTRL_VERSION,
					 OSAL_E_INVALIDVALUE,"invval",
					 (tU32)s32ID, u32FD,
					 (tU32)garStateData[s32ID].rDRVIf.enPlayState, 0);
	   
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_Extread
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
* Initial version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_Extread(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
   	OSAL_trAcousticECNRRead* pExtReadInfo;
   	tU32 u32BytesRead = 0;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_EXTREAD,
						 "enter", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
	pExtReadInfo = (OSAL_trAcousticECNRRead*)s32Arg;
   
   	if(NULL == pExtReadInfo)
   	{
   		/* nullpointer */
   		u32ErrorCode =  OSAL_E_INVALIDVALUE;
   	}

   	else
   	{
   	/* OK, do the read operation */
   	u32ErrorCode = u32DoReadOperation(s32ID, u32FD,
									 pExtReadInfo,
									 &u32BytesRead);
   	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_EXTREAD,
						"exit", (tU32)s32ID, u32FD,
						(tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);	
	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      AcousticECNR_IOCtrl_RegNotification
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Initial version
************************************************************************/
static void AcousticECNR_IOCtrl_RegNotification(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	OSAL_trAcousticECNRCallbackReg* pCallback;
	
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_REG_NOTIFICATION,
						 "enter", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
	pCallback = (OSAL_trAcousticECNRCallbackReg*)s32Arg;

	if ((NULL == pCallback) || (NULL == pCallback->pfEvCallback))
	{
		/* unregister callback */
	   	garStateData[s32ID].rDRVIf.rCallback.pfEvCallback = NULL;
	   	garStateData[s32ID].rDRVIf.rCallback.pvCookie	 = NULL;
	}
	else
	{		   
		/* register callback */ 
	   	garStateData[s32ID].rDRVIf.rCallback.pfEvCallback 
	   	= (OSAL_tpfAcousticECNREvCallback)pCallback->pfEvCallback;
	   	garStateData[s32ID].rDRVIf.rCallback.pvCookie 
	   	= (tPVoid)pCallback->pvCookie;
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_REG_NOTIFICATION,
						 "exit", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_WaitEvent
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
* Initial version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_WaitEvent(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	OSAL_trAcousticECNRWaitEvent* pWaitEvent;
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_WAITEVENT,
						"enter", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
								
	pWaitEvent = (OSAL_trAcousticECNRWaitEvent*)s32Arg;

	if (NULL == pWaitEvent)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_WAITEVENT,
						 "exit", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);	
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_GetSuppSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Initial version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_GetSuppSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
   	OSAL_trAcousticSampleRateCapability* prSampleRateCap;
   	tU32 u32CopyIdx;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLERATE,
						"enter", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
								
   	prSampleRateCap = (OSAL_trAcousticSampleRateCapability*)s32Arg;

	if(NULL == prSampleRateCap)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
   	}
	else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleRateCap->enCodec)
	{
		/* sample rate only configurable for PCM */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else
	{
		/* OK, get samplerate capabilities */
		for (u32CopyIdx = 0; (u32CopyIdx < prSampleRateCap->u32ElemCnt)
		&&(u32CopyIdx < NUM_ARRAY_ELEMS(anSupportedSampleratesFrom));
		u32CopyIdx++)
		{
		   prSampleRateCap->pnSamplerateFrom[u32CopyIdx] =
								   anSupportedSampleratesFrom[u32CopyIdx];
		   prSampleRateCap->pnSamplerateTo[u32CopyIdx] =
								   anSupportedSampleratesTo[u32CopyIdx];
		} //for ...
		prSampleRateCap->u32ElemCnt =
							   NUM_ARRAY_ELEMS(anSupportedSampleratesFrom);
	}
	
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLERATE,
						"exit", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
   return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_GetSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Initila version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_GetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

    OSAL_trAcousticSampleRateCfg* prSampleRateCfg;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLERATE,
						 "enter", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
    prSampleRateCfg = (OSAL_trAcousticSampleRateCfg*)s32Arg;

    if(NULL == prSampleRateCfg)
    {
        /* nullpointer */
        u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
    else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleRateCfg->enCodec)
	{
		/* sample rate only configurable for PCM */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else
	{
		/* OK, get samplerate */
		prSampleRateCfg->nSamplerate = garStateData[s32ID].rPCMConfig.nSampleRate;
	}
	
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLERATE,
								 "exit", (tU32)s32ID,
								 u32FD, (tU32)s32Arg,
								 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
   return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_SetSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Initial version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_SetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticSampleRateCfg* prSampleRateCfg;
	
    vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLERATE,
						"enter", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
	
	if(A_ECNR_EN_STATE_STOPPED != garStateData[s32ID].rDRVIf.enPlayState)
	{
   	/* stream currently running, set not allowed */
		u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
	}
	else
	{
		prSampleRateCfg = (OSAL_trAcousticSampleRateCfg*)s32Arg;
		
		if(NULL == prSampleRateCfg)
		{
			/* nullpointer */
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
		else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleRateCfg->enCodec)
		{
			/* sample rate only configurable for PCM */
			u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
		}
		else if (bIsSamplerateValid(prSampleRateCfg->nSamplerate))
		{
			/* OK, set samplerate */
			garStateData[s32ID].rPCMConfig.nSampleRate =
									   prSampleRateCfg->nSamplerate;
		}
		else
		{
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLERATE,
						"exit", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);

   return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_GetSuppSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Initial version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_GetSuppSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
   	OSAL_trAcousticSampleFormatCapability* prSampleFormatCap;
   	tU32 u32CopyIdx;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLEFORMAT, "enter", (tU32)s32ID,
						u32FD, (tU32)s32Arg, (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								
   	prSampleFormatCap = (OSAL_trAcousticSampleFormatCapability*)s32Arg;
   
	if (NULL == prSampleFormatCap)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleFormatCap->enCodec)
	{
		/* sample format only configurable for PCM */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else
	{
		/* OK, get sampleformat capabilities */
		for (u32CopyIdx = 0;(u32CopyIdx < prSampleFormatCap->u32ElemCnt) &&
		(u32CopyIdx < NUM_ARRAY_ELEMS(aenSupportedSampleformats));u32CopyIdx++)
		{
			prSampleFormatCap->penSampleformats[u32CopyIdx] =
									aenSupportedSampleformats[u32CopyIdx];
		}
		prSampleFormatCap->u32ElemCnt = NUM_ARRAY_ELEMS(aenSupportedSampleformats);
	}
	
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLEFORMAT,
						 "exit", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_GetSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Initial version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_GetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
   	OSAL_trAcousticSampleFormatCfg* prSampleFormatCfg;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLEFORMAT, "enter", (tU32)s32ID,
						 u32FD, (tU32)s32Arg, (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
   	prSampleFormatCfg = (OSAL_trAcousticSampleFormatCfg*)s32Arg;
   
	if(NULL == prSampleFormatCfg)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleFormatCfg->enCodec)
	{
		/* sample format only configurable for PCM */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else
	{
		/* OK, get sampleformat */
		prSampleFormatCfg->enSampleformat = garStateData[s32ID].rPCMConfig.enSampleFormat;
	}
	
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLEFORMAT,
						"exit", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_SetSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Initla version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_SetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
   	OSAL_trAcousticSampleFormatCfg* prSampleFormatCfg;
	
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLEFORMAT,
                                 "enter", (tU32)s32ID,
                                 u32FD, (tU32)s32Arg,
                                 (tU32)garStateData[s32ID].rDRVIf.enPlayState);   	
	
	if(A_ECNR_EN_STATE_STOPPED != garStateData[s32ID].rDRVIf.enPlayState)
	{
	   	/* stream currently running, set not allowed */
		u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
	}
	
	else
	{
		prSampleFormatCfg = (OSAL_trAcousticSampleFormatCfg*)s32Arg;
		
		if(NULL == prSampleFormatCfg)
		{
			/* nullpointer */
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
		else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleFormatCfg->enCodec)
		{
			/* sample format only configurable for PCM */
			u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
		}
		else if (bIsSampleformatValid(prSampleFormatCfg->enSampleformat))
		{
			/* OK, set sampleformat */
			garStateData[s32ID].rPCMConfig.enSampleFormat =
								   prSampleFormatCfg->enSampleformat;
		}
		else
		{
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLEFORMAT,
						"exit", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_GetSuppChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_GetSuppChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
   	tU32 u32ErrorCode = OSAL_E_NOERROR;
   	OSAL_trAcousticChannelCapability* prChanCap;
	tU32 u32CopyIdx;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_CHANNELS,
						 "enter", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
	prChanCap = (OSAL_trAcousticChannelCapability*)s32Arg;

	if(NULL == prChanCap)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{	
		/* OK, get channel num capabilities */
		for (u32CopyIdx = 0;
		(u32CopyIdx < prChanCap->u32ElemCnt) &&
		(u32CopyIdx < NUM_ARRAY_ELEMS(au16SupportedChannelnums));
		u32CopyIdx++)
		{
			prChanCap->pu32NumChannels[u32CopyIdx] =
										au16SupportedChannelnums[u32CopyIdx];
		}
		prChanCap->u32ElemCnt = NUM_ARRAY_ELEMS(au16SupportedChannelnums);
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_CHANNELS,
						 "exit", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_GetChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_GetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
   	tPU16 pu16NumChannels;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETCHANNELS,
						"enter", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
								
   	pu16NumChannels = (tPU16)s32Arg;
   
   	if(NULL == pu16NumChannels)
   	{
   		/* nullpointer */
	   	u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else
	{
		/* OK, set number of channels */
   		*pu16NumChannels = garStateData[s32ID].rPCMConfig.u16NumChannels;
	}

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETCHANNELS,
						"exit", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);	
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_SetChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_SetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
   	tU16 u16NumChannels;
   
   	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETCHANNELS,
						"enter", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
   
   	if(A_ECNR_EN_STATE_STOPPED != garStateData[s32ID].rDRVIf.enPlayState)
   	{
		/* stream currently running, setchannels not allowed */
		u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;   
	}
	else
	{
			// ACOUSTICECNR_BUGFIX
		u16NumChannels = (tU16)s32Arg;
		
		if (bIsChannelnumValid(u16NumChannels))
		{
			/* OK, set number of channels */
			garStateData[s32ID].rPCMConfig.u16NumChannels = u16NumChannels;
		}
		else
		{
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
	}

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETCHANNELS,
						"exit", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);

	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_GetSuppBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_GetSuppBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
   	OSAL_trAcousticBufferSizeCapability* prBufferSizeCap;
   	tU32 u32CopyIdx;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_BUFFERSIZE,
						"enter", (tU32)s32ID,
						u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
								
   	prBufferSizeCap = (OSAL_trAcousticBufferSizeCapability*)s32Arg;
   
	if(NULL == prBufferSizeCap)
   	{
		/* nullpointer */
	   	u32ErrorCode = OSAL_E_INVALIDVALUE;
   	}
	else if(OSAL_EN_ACOUSTIC_ENC_PCM != prBufferSizeCap->enCodec)
	{
		/* Buffer size only configurable for PCM */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else
	{
		/* OK, get channel num capabilities */
		for (u32CopyIdx = 0; (u32CopyIdx < prBufferSizeCap->u32ElemCnt) &&
		(u32CopyIdx < NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM)); u32CopyIdx++)
		{
			prBufferSizeCap->pnBuffersizes[u32CopyIdx] =
								   au32SupportedBuffersizesPCM[u32CopyIdx];
		}

		prBufferSizeCap->u32ElemCnt =
							  NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM);
	}
   	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_BUFFERSIZE,
								"exit", (tU32)s32ID,
								u32FD, (tU32)s32Arg,
								(tU32)garStateData[s32ID].rDRVIf.enPlayState);
   	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_GetBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_GetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticBufferSizeCfg* pBufferSizeCfg;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETBUFFERSIZE,
						 "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
	pBufferSizeCfg = (OSAL_trAcousticBufferSizeCfg*)s32Arg;

	if(NULL == pBufferSizeCfg)
	{
		/* nullpointer */
	    u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else if(OSAL_EN_ACOUSTIC_ENC_PCM != pBufferSizeCfg->enCodec)
	{
		/* Buffer size only configurable for PCM */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}

	else
	{
		/* OK, get buffersize */
		pBufferSizeCfg->nBuffersize =
				 garStateData[s32ID].anBufferSize[pBufferSizeCfg->enCodec];
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETBUFFERSIZE,
						 "exit", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
						 
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_SetBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_SetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticBufferSizeCfg* pBufferSizeCfg;

   	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETBUFFERSIZE,
						"enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
	   
  	if(A_ECNR_EN_STATE_STOPPED != garStateData[s32ID].rDRVIf.enPlayState)
  	{
		/* stream currently running, setbuffersize not allowed */
	   	u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
   	}
	else 
	{
		pBufferSizeCfg = (OSAL_trAcousticBufferSizeCfg*)s32Arg;
		
		if (NULL == pBufferSizeCfg)
		{
			/* nullpointer */
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
		else if (pBufferSizeCfg->enCodec != OSAL_EN_ACOUSTIC_ENC_PCM)
		{
			/* Buffer size only configurable for PCM */
			u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
		}
		else if (bIsBuffersizeValid(pBufferSizeCfg->nBuffersize))
		{
			/* OK, set buffersize */
			garStateData[s32ID].anBufferSize[pBufferSizeCfg->enCodec] =
										   pBufferSizeCfg->nBuffersize;
		}
		else
		{
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETBUFFERSIZE, 
   						"exit", (tU32)s32ID, u32FD, (tU32)s32Arg, 
   						(tU32)garStateData[s32ID].rDRVIf.enPlayState);

	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_SetErrThr
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_SetErrThr(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticErrThrCfg* pErrThrCfg;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETERRTHR,
						"enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
						(tU32)garStateData[s32ID].rDRVIf.enPlayState);
								
	pErrThrCfg = (OSAL_trAcousticErrThrCfg*)s32Arg;
   
	if(NULL == pErrThrCfg)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else if(OSAL_EN_ACOUSTIC_ERRTYPE_XRUN==pErrThrCfg->enErrType)
	{
		garStateData[s32ID].rDRVIf.rErrThreshold.s32XrunErrThr =
										  pErrThrCfg->s32Threshold;
		garStateData[s32ID].rDRVIf.rErrThreshold.s32XrunErrCnt = 0;
	}
	else
	{
		u32ErrorCode = OSAL_E_NOTSUPPORTED;
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETERRTHR,
						 "exit", (tU32)s32ID, u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);	
						 
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_Start
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
* Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_Start(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_START,
						 "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);	

	if ( A_ECNR_EN_STATE_STOPPED == garStateData[s32ID].rDRVIf.enPlayState)
	{
	 /* open alsa*/
	 if (OSAL_E_NOERROR != u32InitAlsa(s32ID))
	 {
	 	vTraceAcousticECNRError(TR_LEVEL_ERRORS, EN_IOCTRL_START,
							   OSAL_E_UNKNOWN,
							   "AlsaFail", 0, 0, 0, 0);

		u32ErrorCode = OSAL_E_UNKNOWN;
	 } 
	 else
	 {
	   switch (s32ID)
	   {
	   case ACOUSTICECNR_DEVID_SPEECHRECO:
		   {
			   /* SDS frontend does not need to be started
			   (done by client application)*/
			   garStateData[s32ID].rDRVIf.enPlayState = A_ECNR_EN_STATE_ACTIVE;
		   }
		   break;

	   default:
		   {
			   // Unsupported device, should have been caught before
			   vTraceAcousticECNRError(TR_LEVEL_ERRORS,
									  EN_IOCTRL_START,
									  OSAL_E_DOESNOTEXIST,
									  "noexist",
									  (tU32)s32ID, u32FD, 0, 0);


			   u32UnInitAlsa(s32ID);

			   u32ErrorCode = OSAL_E_UNKNOWN;
		   }
		   break;
	   }
	 }
	}
	else
	{
	   // nothing to do
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_START,
						 "exit", (tU32)s32ID, u32FD,
						 (tU32)s32Arg, (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_Stop
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*   
*    Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_Stop(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_STOP,
						 "enter", (tU32)s32ID, u32FD,
						 (tU32)s32Arg, (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
	if ( A_ECNR_EN_STATE_ACTIVE == garStateData[s32ID].rDRVIf.enPlayState)
	{
		/* close alsa, stop apps */
	 	if (OSAL_E_NOERROR != u32UnInitAlsa(s32ID))
	 	{
			u32ErrorCode = OSAL_E_UNKNOWN;
		}
	 	else
	 	{
			/* frontend does not support notification for stopping stream,
	   		therefore abort stream immediately and report AUDIOSTOPPED event*/
			u32ErrorCode = u32AbortStream(s32ID);
   
	   		if ( OSAL_E_NOERROR == u32ErrorCode )
	   		{
		   		/* Notify "audio stopped" */
		   		// notification via callback
		   		if (NULL !=	garStateData[s32ID].rDRVIf.rCallback.pfEvCallback)
		   		{
			   		garStateData[s32ID].rDRVIf.rCallback.pfEvCallback(AC_ECNR_EVAUDIOSTOPPED,
					NULL, garStateData[s32ID].rDRVIf.rCallback.pvCookie);
				}
	   		}
	 	}
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_STOP,
						 "exit", (tU32)s32ID, u32FD,
						 (tU32)s32Arg, (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticECNR_IOCtrl_Abort
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
* Inital version
************************************************************************/
static tU32 u32AcousticECNR_IOCtrl_Abort(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_ABORT,
						 "enter", (tU32)s32ID,
						 u32FD, (tU32)s32Arg,
						 (tU32)garStateData[s32ID].rDRVIf.enPlayState);
								 
	if ( A_ECNR_EN_STATE_ACTIVE == garStateData[s32ID].rDRVIf.enPlayState)
	{
		/* close alsa, stop apps */
	  	if (OSAL_E_NOERROR != u32UnInitAlsa(s32ID))
	  	{
			vTraceAcousticECNRError(TR_LEVEL_ERRORS,
								  EN_IOCTRL_ABORT,
								  OSAL_E_UNKNOWN,
								  "AlsaFail", 0, 0, 0, 0);

			u32ErrorCode = OSAL_E_UNKNOWN;
		} 
	  	else
	  	{
			u32ErrorCode = u32AbortStream(s32ID);
	  	}
	}
	vTraceAcousticECNRInfo(TR_LEVEL_USER_2, EN_IOCTRL_ABORT,
						 "exit", (tU32)s32ID, u32FD,
						 (tU32)s32Arg, (tU32)garStateData[s32ID].rDRVIf.enPlayState);   
						 
	return u32ErrorCode;
}


/*************************************************************************/ /**
*  FUNCTION:      vResetErrorCounters
*
*  @brief         Resets error counters to 0
*
*  @param         s32ID   stream ID
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tVoid vResetErrorCounters (tS32 s32ID)
{
    garStateData[s32ID].rDRVIf.rErrThreshold.s32XrunErrCnt = 0;
}

/*************************************************************************/ /**
*  FUNCTION:      vResetErrorThresholds
*
*  @brief         Resets error thresholds to 0 (disabled)
*
*  @param         s32ID   stream ID
*
*  HISTORY:
*

*    Initial revision.
*****************************************************************************/
static tVoid vResetErrorThresholds (tS32 s32ID)
{
    garStateData[s32ID].rDRVIf.rErrThreshold.s32XrunErrThr = 0;
}


#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file 
|-----------------------------------------------------------------------*/
