/************************************************************************
| FILE:         acousticsrc.c
| SW-COMPONENT: Acoustic driver
|------------------------------------------------------------------------
* @file    Acousticsrc.c
*
* @brief   This device is an audio output device whose main
*          functionality is Sample Rate Conversion of PCM streams
*
* @author  Niyatha Srivathsa Rao(ECF5/RBEI)
*
* @date	11-10-2012
*
* @version Initial Version, 1.0
*
* @note
*  &copy; 
*
* @history
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"               /**< OSAL types etc. */
#include "osalio_public.h"

#include <alsa/asoundlib.h>
#include "acousticsrc_public.h"    /**< public interface for this product */
#include "acoustic_trace.h"        /**< trace stuff for this product */
#include "acousticsrc_private.h"   /**< IOControl implementation */

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define NUM_ARRAY_ELEMS(array)  (sizeof(array)/sizeof(array[0]))

/************************************************************************
| variable definition (scope: local)
|-----------------------------------------------------------------------*/

/** List of decoders supported by the device */
static const OSAL_tenAcousticCodec aenSupportedCodecs[] =
{
	OSAL_EN_ACOUSTIC_DEC_PCM
};

/** List of PCM sample formats supported by the device */
static const OSAL_tenAcousticSampleFormat aenSupportedSampleformats[] =
{
	OSAL_EN_ACOUSTIC_SF_S16
};

/** List of PCM sample rates supported by the device (interval begin value) */
/**Niyatha: can be expanded in future **/
static const OSAL_tAcousticSampleRate anSupportedSampleratesFrom[] =
{
  16000 	
};

/** List of PCM sample rates supported by the device (interval end value) */
/**Niyatha: can be expanded in future **/
static const OSAL_tAcousticSampleRate anSupportedSampleratesTo[] =
{
  16000
};

/** List of PCM channel numbers supported by the device */
/**< mono *///Niyatha: stereo can be included in future **/
static tCU16 au16SupportedChannelnums[] =
{
    1  
};

/** List of buffer sizes of PCM codec supported by the device (in bytes)*/
static tCU32 au32SupportedBuffersizesPCM[] =
{
    1024,
    2048,
    4096,
    8192,
   16384
};

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

static tBool ACOUSTICSRC_arbInit = FALSE;

static trACOUSTICSRC_StateData arSrcStateData = {0};

static tChar	s8AcousticSRCFilename[OSAL_C_U32_MAX_PATHLENGTH] = "/dev/ramdisk/SRC_output.wav";

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static tU32 u32AcousticSrcIOCtrl_Version(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_Convert(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetFilePath(tS32 s32Arg);
static void AcousticSrcIOCtrl_RegNotification(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_WaitEvent(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetSuppDecoder(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetDecoder(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetSuppSamplerate(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetSamplerate(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_SetSamplerate(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetSuppSampleformat(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetSampleformat(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_SetSampleformat(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetSuppChannels(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetChannels(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_SetChannels(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetSuppBuffersize(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_GetBuffersize(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_SetBuffersize(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_SetErrThr(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_Start(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_Stop(tS32 s32Arg);
static tU32 u32AcousticSrcIOCtrl_Abort(tS32 s32Arg);

static tU32 u32DoSRCOperation(const OSAL_trAcousticSrcConvert* prSrcInfo,
								tPU32 pu32BytesConverted, tS32  s32TimeOut);
static tBool bIsCodecValid(OSAL_tenAcousticCodec enCodec);
static tBool bIsSampleformatValid(OSAL_tenAcousticSampleFormat enSampleformat);
static tBool bIsSamplerateValid(OSAL_tAcousticSampleRate nSamplerate);
static tBool bIsChannelnumValid(tU16 u16Channelnum);
static tBool bIsBuffersizeValid(tU32 u32Buffersize, OSAL_tenAcousticCodec enCodec);
static tU32 u32SetEvent(const trACOUSTICSRC_StateData *prStateData, const OSAL_tEventMask tWaitEventMask);
static tU32 u32ClearEvent(const trACOUSTICSRC_StateData *prStateData, const OSAL_tEventMask tWaitEventMask);
static tU32 u32InitOutputDevice(tVoid);
static tU32 u32UnInitOutputDevice(tVoid);
static tU32 u32AbortStream(tVoid);
static tU32 u32OutputDeviceStop(tVoid);
static tU32 u32OutputDeviceAbort(tVoid);
static tU32 u32OutputDataConvert(const void* pvBuffer, tU32 u32BufferSize,
								 tPU32 pu32BytesConverted, tS32  s32TimeOut);

static tU32 ACOUSTICSRC_u32InitPrivate(tVoid);
static tU32 ACOUSTICSRC_u32DeinitPrivate(tVoid);


/************************************************************************
|function implementation
|-----------------------------------------------------------------------*/

/********************************************************************/ /**
*  FUNCTION:      iSlash2CamelCase
*
*  @brief         removes slashes from input string
*                 Add a first Letter 'A'
*                 changes Next letter after slash to capital letter
*                 Example: /dev/acoustic/speech -> AdevSpeechAcousticSpeech
*                 
*
*  @return        returns number of characters copied into Outputbuffer
*                 (without trailing '\0')
*  @retval        int
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static int
iSlash2CamelCase(const char *pcszSlashes,
				 char *pszOutputBuffer,
				 int iOutputBufferSize)
{
    int   iIn = 0;
    int   iOut = 0;
	tBool bMakeCapital  = FALSE;
	tBool bIsFirstSlash = TRUE; //first character after '/' leave lower case
    char  c;

	pszOutputBuffer[iOut++] = 'A';
    while('\0' != (c = pcszSlashes[iIn++])
          &&
          ((iOut+1) < iOutputBufferSize))
    {
        if('/' == c)
        {
			//letter after first slash leave in lower case
            bMakeCapital = bIsFirstSlash ? FALSE : TRUE;
			bIsFirstSlash = FALSE;
        }
        else//if('/' == c)
        {
			pszOutputBuffer[iOut++] =
                (char)(bMakeCapital ? toupper((int)c) : c);
            bMakeCapital = FALSE;
        } //else//if('/' == c)
    } //while('\0' != (c = pcszSlashes[iIn++]) && !bOverflow)

    pszOutputBuffer[iOut] = '\0'; //trailing zero
    return iOut;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICSRC_vDumpAlsaStatus
*
*  @brief         prints ALSA-status to TTFIS
*
*  @return        void
*  @retval        void
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static void ACOUSTICSRC_vDumpAlsaStatus(tVoid)
{
	int err;
	snd_output_t *outputp = NULL;

	if ((err = snd_output_buffer_open(&outputp)) < 0)
	{
		ACOUSTICSRC_PRINTF_ERRORS("ALSA buffer open error: %s",
		snd_strerror(err));
	}
	else 
	{
		snd_pcm_status_t *status = NULL;
		snd_pcm_status_malloc(&status);

		if(NULL != arSrcStateData.pAlsaPCM)
		{
			if((err = snd_pcm_status(arSrcStateData.pAlsaPCM, status))< 0)
			{
				ACOUSTICSRC_PRINTF_ERRORS("ALSA status error: %s",
				snd_strerror(err));
			}
			else
			{
				char *pC = NULL;
				snd_pcm_status_dump(status, outputp);				
				snd_output_buffer_string(outputp, &pC);

				if(pC)
				{
					ACOUSTICSRC_PRINTF_U3("\n****AcSRC- snd_pcm_status_dump****:\n%s", pC);
				}
			}
		}
		snd_output_close(outputp);
	}
}

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/********************************************************************/ /**
*  FUNCTION:  set_swparams
*
*  @brief         set soft params
*
*  @return        0
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static int set_swparams(snd_pcm_t *handle)
{
    int err;
    snd_pcm_sw_params_t *swparams;
    snd_pcm_uframes_t uframeStartTreshold;
    snd_pcm_sw_params_malloc(&swparams);

    /* get the current swparams */
    err = snd_pcm_sw_params_current(handle, swparams);
    if (err < 0)
	{
            ACOUSTICSRC_PRINTF_ERRORS("set_swparams Unable to determine current"
                                      " swparams: %s", snd_strerror(err));
    }


	//debug
    {
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_avail_min(swparams,      &val);
        ACOUSTICSRC_PRINTF_U3("set_swparams avail_min: %d, err %d",
                              (int)val, err);
    }

	//debug
	{
        int iVal;
        err = snd_pcm_sw_params_get_period_event(swparams, &iVal);
        ACOUSTICSRC_PRINTF_U3("set_swparams period_event: %d, err %d",
                              iVal, err);
    }
    
	//debug
	{
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_stop_threshold(swparams, &val);
        //uframeStopTreshold = val;
        ACOUSTICSRC_PRINTF_U3("set_swparams stop_threshold: %u, err %d",
                              (unsigned int)val, err);
    }
    
	//debug
	{
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_start_threshold(swparams, &val);
        uframeStartTreshold = val;
        ACOUSTICSRC_PRINTF_U3("set_swparams start_threshold: %u, err %d",
                              (unsigned int)uframeStartTreshold, err);
    }

	//debug
	{
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_silence_threshold(swparams, &val);
        ACOUSTICSRC_PRINTF_U3("set_swparams silence_threshold: %u, err %d",
                              (unsigned int)val, err);
    }

	//debug
	{
        snd_pcm_uframes_t val;
        err = snd_pcm_sw_params_get_silence_size(swparams, &val);
        ACOUSTICSRC_PRINTF_U3("set_swparams silence_size: %u, err %d",
                              (unsigned int)val, err);
    }

    {             
		uframeStartTreshold = 1;
        err = snd_pcm_sw_params_set_start_threshold(handle,
                                                    swparams,
                                                    uframeStartTreshold);
        if (err < 0)
        {
                ACOUSTICSRC_PRINTF_ERRORS("set_swparams Unable to set start "
                                          "threshold: %s", snd_strerror(err));
        }
   }
    
    /* write the parameters to the playback device */
    err = snd_pcm_sw_params(handle, swparams);
    if (err < 0)
	{
      ACOUSTICSRC_PRINTF_ERRORS("set_swparams Unable to set sw params: %s",
                                snd_strerror(err));
    }

	return 0;
}


/********************************************************************/ /**
*  FUNCTION:      ACOUSTICSRC_u32InitPrivate
*
*  @brief         Initializes any necessary resources.
*   			Should be called at OSAL startup.
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR   on success
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 ACOUSTICSRC_u32InitPrivate(tVoid)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_u32InitPrivate - START");

	if(!ACOUSTICSRC_arbInit)
	{
		ACOUSTICSRC_arbInit = TRUE;

		arSrcStateData.bAlsaIsOpen = FALSE;
		arSrcStateData.pSndOutput  = NULL;

		arSrcStateData.enFileHandle = ACOUSTICSRC_EN_HANDLE_ID_SPEECH;

		/* create event field */
		{
			char szEventName[32 + 1] = "";
										
			if(OSAL_OK != OSAL_s32EventCreate(szEventName, &arSrcStateData.hOsalEvent))
			{
				tU32 u32Err = OSAL_u32ErrorCode();
				ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_u32Init- OSAL_s32EventCreate"
										  " ERROR 0x%08X",(unsigned int)u32Err);
			}
		}

        /* create close semaphore */
        {
          char szSemName[16 + 1] = "";

          if(OSAL_OK != OSAL_s32SemaphoreCreate(szSemName, &arSrcStateData.hSemClose, 1))
          {
            tU32 u32Err = OSAL_u32ErrorCode();
            ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_u32Init- OSAL_s32SemaphoreCreate"
                                      " (CLOSESem) ERROR 0x%08X", (unsigned int)u32Err);
          }
        }

		/* we are now in state "Initialized" */
		arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_INITIALIZED;
	} 

	ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_u32InitPrivate - END u32ErrorCode 0x%08X",
						  u32ErrorCode);
	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICSRC_u32DeinitPrivate
*
*  @brief         Releases any resources acquired during init.
*                 Before this function is called (e.g. by Powermanagement),
*                 the device must have been closed. This cannot be done
*                 here, because then the device would still be marked as
*                 used in the dispatcher.
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR   on success
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 ACOUSTICSRC_u32DeinitPrivate(tVoid)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_u32DeinitPrivate - START");

	if(ACOUSTICSRC_arbInit)
	{
		char szEventName[16 + 1] = "";
		ACOUSTICSRC_arbInit = FALSE;

		/* prevent uninitializing, if already uninitialized */
		if(arSrcStateData.enPlayState != ACOUSTICSRC_EN_STATE_UNINITIALIZED)
		{
			tS32 s32Ret;

			u32SetEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_CANCEL);
            OSAL_s32ThreadWait(50);
			u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ANY);

			/*Wait for acknowledge and destroy semaphore*/
			{
				char szSemName[16 + 1] = "";

				/*200 is the time out in ms*/
				(tVoid)OSAL_s32SemaphoreWait(arSrcStateData.hSemClose,200); 

				(tVoid)OSAL_s32SemaphoreClose(arSrcStateData.hSemClose);

				s32Ret = OSAL_s32SemaphoreDelete(szSemName);
				if(s32Ret != OSAL_OK)
				{
					tU32 u32Err = OSAL_u32ErrorCode();
					ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_u32Deinit - (CloseSemaphore)"
											"OSAL_s32SemaphoreDelete ERROR 0x%08X",(unsigned int)u32Err);
				}
			}

			/* close and delete event field */
			s32Ret = OSAL_s32EventClose(arSrcStateData.hOsalEvent);
			if(s32Ret != OSAL_OK)
			{
				tU32 u32Err = OSAL_u32ErrorCode();
				ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_u32Deinit - OSAL_s32EventClose ERROR 0x%08X",
										  (unsigned int)u32Err);
			}

			s32Ret = OSAL_s32EventDelete(szEventName);
			if(s32Ret != OSAL_OK)
			{
				tU32 u32Err = OSAL_u32ErrorCode();
				ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_u32Deinit - OSAL_s32EventDelete ERROR 0x%08X",
										  (unsigned int)u32Err);
			}
			/* we go to state "UnInitialized" */
			arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_UNINITIALIZED;
		} 
	}

	ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_u32DeinitPrivate - END u32ErrorCode 0x%08X",
						  u32ErrorCode);
	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICSRC_s32IOOpen
*
*  @brief         Opens the acousticsrc device "/dev/acoustic/src"
*
*  @param     	s32ID
*                   ID of the stream to open
*  @param         szName
*                   Should be OSAL_C_STRING_DEVICE_ACOUSTIC_IF_SRC
*  @param         enAccess
*                   File access mode (should always be OSAL_EN_WRITEONLY
*                   for this device although value is ignored for now)
*  @param         pu32FD
*                   for storing the filehandle
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_NOTINITIALIZED
*  @retval        OSAL_E_ALREADYOPENED
*  @retval        OSAL_E_UNKNOWN
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
tS32 ACOUSTICSRC_s32IOOpen(tS32 s32ID, tCString szName,
                           OSAL_tenAccess enAccess, tPU32 pu32FD, tU16 appid)
{
	int iLen;
	const char *pcszShortDeviceName;
	const char *pcszLongDeviceName;
	char szFullOsalDeviceName[ACOUSTICSRC_ALSA_DEVICE_NAME_BUFFER_SIZE];
	size_t nShortLen;
	size_t nLongLen;
	size_t nFullLen;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_ACSRC_OPEN, "enter", (tU32)s32ID,
				 (tU32)enAccess, pu32FD != NULL ? (tU32)(*pu32FD) : (tU32)-1,(tU32)appid);

	pcszShortDeviceName = NULL == szName ? "" : szName;
	nShortLen = strlen(pcszShortDeviceName);

	//only SRC device is supported   
	s32ID = EN_ACOUSTIC_DEVID_SRC; 
	
	// get osal base device name, append "/szName"
	pcszLongDeviceName = OSAL_C_STRING_DEVICE_ACOUSTIC_IF_SRC;

	nLongLen  = strlen(pcszLongDeviceName); 
	nFullLen  = nLongLen + nShortLen + 2;  //+2: trailing zero and a '/'
	if(nFullLen <= ACOUSTICSRC_ALSA_DEVICE_NAME_BUFFER_SIZE)
	{
		strcpy(szFullOsalDeviceName, pcszLongDeviceName);
		strcat(szFullOsalDeviceName,"/");
		strcat(szFullOsalDeviceName,pcszShortDeviceName);
	}
	else
	{ //error buffer to short
		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_ACSRC_OPEN, OSAL_E_UNKNOWN,
					  "2short", (tU32)ACOUSTICSRC_ALSA_DEVICE_NAME_BUFFER_SIZE,
					  (tU32)nFullLen, (tU32)nLongLen, (tU32)nShortLen);
		return (tS32)OSAL_E_UNKNOWN;
	}

	ACOUSTICSRC_PRINTF_U4("ACOUSTICSRC_s32IOOpen FullOsalDeviceName <%s>\n",
				 szFullOsalDeviceName);

	ACOUSTICSRC_u32InitPrivate();

	memset(arSrcStateData.szAlsaDeviceName, 0, ACOUSTICSRC_ALSA_DEVICE_NAME_BUFFER_SIZE);
	
	iLen = iSlash2CamelCase(szFullOsalDeviceName, arSrcStateData.szAlsaDeviceName,
				   ACOUSTICSRC_ALSA_DEVICE_NAME_BUFFER_SIZE);
				   
	ACOUSTICSRC_PRINTF_U4("ACOUSTICSRC_s32IOOpen CamelCase <%s>, Len %d\n",
				 arSrcStateData.szAlsaDeviceName, iLen);
	if(iLen <= 0)
	{
		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_ACSRC_OPEN, OSAL_E_UNKNOWN, "noname", 
					  (tU32)iLen, (tU32)0, (tU32)0, (tU32)0);

		return (tS32)OSAL_E_UNKNOWN;
	}    

	/* If the driver is not yet initialized, the open request fails*/
	if(arSrcStateData.enPlayState < ACOUSTICSRC_EN_STATE_INITIALIZED)
	{
		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_ACSRC_OPEN, OSAL_E_NOTINITIALIZED,
					   "noinit", (tU32)arSrcStateData.enPlayState, 0, 0, 0);
		return (tS32)OSAL_E_NOTINITIALIZED;
	}

	/* If the stream is already opened, the open request fails */
	if(arSrcStateData.enPlayState > ACOUSTICSRC_EN_STATE_INITIALIZED)
	{
		vTraceAcousticSrcError(TR_LEVEL_ERRORS,EN_ACSRC_OPEN, OSAL_E_ALREADYOPENED,
					   "alropen", (tU32)arSrcStateData.enPlayState, 0, 0, 0);
		return (tS32)OSAL_E_ALREADYOPENED;
	}

	/* Initialize internal state variables to defaults */
	arSrcStateData.rPCMConfig.enSampleFormat =
							 ACOUSTICSRC_C_EN_DEFAULT_PCM_SAMPLEFORMAT;
							 
	arSrcStateData.rPCMConfig.nSampleRate    =
							 ACOUSTICSRC_C_U32_DEFAULT_PCM_SAMPLERATE;
							 
	arSrcStateData.rPCMConfig.u16NumChannels =
						   ACOUSTICSRC_C_U8_DEFAULT_NUM_CHANNELS;
						   
	arSrcStateData.enCodec = ACOUSTICSRC_C_EN_DEFAULT_DECODER;
	
	arSrcStateData.anBufferSize = ACOUSTICSRC_C_U32_DEFAULT_BUFFER_SIZE_PCM;

	/* init Callback data */
	arSrcStateData.rCallback.pfEvCallback = NULL;
	arSrcStateData.rCallback.pvCookie     = NULL;

	/* clear all remaining notification events */
	(tVoid)u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ANY);
	
	arSrcStateData.pPCM_hw_params = NULL;
	arSrcStateData.pAlsaPCM       = NULL;
	if(OSAL_E_NOERROR != u32InitOutputDevice())
	{
		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_ACSRC_OPEN, OSAL_E_UNKNOWN,
					   "ainit", (tU32)arSrcStateData.enPlayState, (tU32)OSAL_E_UNKNOWN,
						0,0);
		return (tS32)OSAL_E_UNKNOWN;
	}

	/* new state is "Stopped" */
	arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_STOPPED;


	/* return filehandle */
	if(pu32FD != NULL)
	{
		(*pu32FD) = (tU32)arSrcStateData.enFileHandle;
	}
	
	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_ACSRC_OPEN, "exitok",
				 (tU32)s32ID, (tU32)enAccess, (tU32)OSAL_E_NOERROR, 0);

	return (tS32)OSAL_E_NOERROR;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICSRC_s32IOClose
*
*  @brief         Closes the acousticsrc device.
*                	 When the device is reopened afterwards, all configured
*                 parameters, registered callbacks will start with the
*                 internal defaults again.
*
*  @param         s32ID  stream ID
*  @param         u32FD  file handle
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_DOESNOTEXIST
*  @retval        OSAL_E_UNKNOWN
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
tS32 ACOUSTICSRC_s32IOClose(tS32 s32ID, tU32 u32FD)
{
    tU32 u32Ret = OSAL_E_NOERROR;

	s32ID = EN_ACOUSTIC_DEVID_SRC; //only SRC device is supported

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_ACSRC_CLOSE,
	                     "enter", (tU32)s32ID, (tU32)u32FD, 0, 0);
  
	/* If the device is not open, the close request will fail */
	if(arSrcStateData.enPlayState < ACOUSTICSRC_EN_STATE_STOPPED)
	{
		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_ACSRC_CLOSE, OSAL_E_DOESNOTEXIST,
		                       "noexist", (tU32)s32ID, u32FD, 0, 0);
		u32Ret = OSAL_E_DOESNOTEXIST;
	}

	/* cancel any thread blocked in WAITEVENT iocontrol */
    u32SetEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_CANCEL);
	
	/* threads blocked in write will be cancelled in the abort call below */
	/* flush buffers and clean up */
	(tVoid)u32AbortStream();

	/* new state is "Initialized" */
	arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_INITIALIZED;

	if (OSAL_E_NOERROR != u32UnInitOutputDevice() )
	{
		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_ACSRC_CLOSE, OSAL_E_UNKNOWN,
		                       "auninit", 0, 0, 0, 0);
		u32Ret = OSAL_E_UNKNOWN;
	}

    ACOUSTICSRC_u32DeinitPrivate();

    vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_ACSRC_CLOSE,
                         "exit", (tU32)s32ID, u32Ret, 0, 0);
    return (tS32)u32Ret;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICSRC_s32IOControl
*
*  @brief         Executes IO-Control for the driver
*
*  @param[in]     s32ID
*                   stream ID
*  @param[in]     u32FD
*                   file handle
*  @param[in]     s32Fun
*                   IO-Control to be executed
*  @param[in,out] s32Arg
*                   extra argument (depending on s32Fun)
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_INVALIDVALUE
*  @retval        OSAL_E_NOTSUPPORTED
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
tS32 ACOUSTICSRC_s32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun, tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	s32ID = EN_ACOUSTIC_DEVID_SRC; //only SRC device is supported

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_ACSRC_IOCTRL,
			"enter", (tU32)s32ID, u32FD, (tU32)s32Fun, (tU32)s32Arg);

	switch(s32Fun)
	{
		/*---------------------------------------------------------------------*/
		/* The driver version is returned                                      */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_VERSION:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_Version(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Writes sample rate converted audio data to the driver by providing a pointer to the data  */
		/* (including additional control information) to the driver.           */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_CONVERT:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_Convert(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/*Returns the path corresponding to the output file dump                                          */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETFILEPATH:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetFilePath(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* This IOControl registers a callback function that is used for       */
		/* notification about certain events.                                  */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_REG_NOTIFICATION:
		{
			AcousticSrcIOCtrl_RegNotification(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Blocks until new event occurs (or until timeout happens),           */
		/* this is an alternative to registering a notification callback.      */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_WAITEVENT:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_WaitEvent(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Retrieves the codec capabilities                                    */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETSUPP_DECODER:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetSuppDecoder(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Gets acoustic decoder to be used for the next audio buffer.         */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETDECODER:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetDecoder(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Retrieves the sample rate cap. for a specified acoustic codec.      */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETSUPP_SAMPLERATE:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetSuppSamplerate(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Gets the sample rate for a specified acoustic codec.                */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETSAMPLERATE:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetSamplerate(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Sets the sample rate for a specified acoustic codec.                */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_SETSAMPLERATE:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_SetSamplerate(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Retrieves the sample format cap. for a specified acoustic codec.    */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETSUPP_SAMPLEFORMAT:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetSuppSampleformat(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Gets the format of the samples for a specified acoustic codec.      */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETSAMPLEFORMAT:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetSampleformat(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Sets the format of the samples for a specified acoustic codec.      */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_SETSAMPLEFORMAT:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_SetSampleformat(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Retrieves the channel num cap.                                      */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETSUPP_CHANNELS:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetSuppChannels(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Gets the number of channels to be used for the audio stream.        */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETCHANNELS:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetChannels(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Sets the number of channels to be used for the audio stream.        */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_SETCHANNELS:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_SetChannels(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Retrieves the buffer size capabilities                              */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETSUPP_BUFFERSIZE:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetSuppBuffersize(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Gets the size of buffers to be used for the audio stream.           */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_GETBUFFERSIZE:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_GetBuffersize(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Sets the size of buffers to be used for the audio stream.           */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_SETBUFFERSIZE:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_SetBuffersize(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Sets the error thresholds for the audio stream.                     */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_SETERRTHR:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_SetErrThr(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Starts the audio transfer. After starting the stream, the           */
		/* client  is required to provide audio data by calling the OSAL write */
		/* operation  continuously.                                            */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_START:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_Start(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Initiates stopping of a running audio transfer.                     */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_STOP:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_Stop(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Aborts (=flushes) running audio output immediately.                  */
		/*---------------------------------------------------------------------*/
		case OSAL_C_S32_IOCTRL_ACOUSTICSRC_ABORT:
		{
			u32ErrorCode = u32AcousticSrcIOCtrl_Abort(s32Arg);
			break;
		}

		/*---------------------------------------------------------------------*/
		/* Default case: not supported argument given                          */
		/*---------------------------------------------------------------------*/
		default:
		{
			u32ErrorCode = OSAL_E_NOTSUPPORTED;
			break;
		}
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_ACSRC_IOCTRL, "exit",
			 (tU32)s32ID, (tU32)u32FD, (tU32)s32Fun, u32ErrorCode);

	return (tS32)u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION: ACOUSTICSRC_alsaConvertFormatOsalToAlsa     
*  @brief    Converts Osal-format To Alsa-format
*  @return   snd_pcm_format_t ALSA     
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static snd_pcm_format_t ACOUSTICSRC_alsaConvertFormatOsalToAlsa(OSAL_tenAcousticSampleFormat nOsalSampleFormat)
{
	snd_pcm_format_t nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;

	switch(nOsalSampleFormat)
	{
		case OSAL_EN_ACOUSTIC_SF_S8:    /*!< signed 8 bit */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_S8;
			break;

		case OSAL_EN_ACOUSTIC_SF_S16:   /*!< signed 16 bit: CPU endian */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_S16;
			break;

		case OSAL_EN_ACOUSTIC_SF_S32:   /*!< signed 32 bit: CPU endian */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_S32;
			break;

		case OSAL_EN_ACOUSTIC_SF_F32:   /*!< float (IEEE 754) 32 bit: CPU endian */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT;
			break;

		case OSAL_EN_ACOUSTIC_SF_S16LE: /*!< signed 16 bit: little endian */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;
			break;
		
		case OSAL_EN_ACOUSTIC_SF_S32LE: /*!< signed 32 bit: little endian */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_S32_LE;
			break;
		
		case OSAL_EN_ACOUSTIC_SF_F32LE: /*!< float (IEEE 754) 32 bit: little endian */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT_LE;
			break;

		case OSAL_EN_ACOUSTIC_SF_S16BE:  /*!< signed 16 bit: big endian */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_S16_BE;
			break;

		case OSAL_EN_ACOUSTIC_SF_S32BE: /*!< signed 32 bit, big endian */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_S32_BE;
			break;

		case OSAL_EN_ACOUSTIC_SF_F32BE:  /*!< float (IEEE 754) 32 bit, big endian */ 
			nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT_BE;
			break;

		default:
			ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConvertFormatOsalToAlsa "
					   "ERROR: cannot convert OSAL-format (%d)", (int)nOsalSampleFormat);
			nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;
	} //switch(nOsalSampleFormat)

	ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_alsaConvertFormatOsalToAlsa : convert OSAL %d to Alsa-format %d", 
							(int)nOsalSampleFormat, (int)nAlsaSampleFormat);
	return nAlsaSampleFormat;
}

/********************************************************************/ /**
*  FUNCTION: ACOUSTICSRC_osalGetBytesPerSample      
*  @brief    return used bytes per sample (!not Frame!) used by an OSAL-PCM-Format
*  @return        
*  @retval        
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static int ACOUSTICSRC_osalGetBytesPerSample(OSAL_tenAcousticSampleFormat nOsalSampleFormat)
{
	int iBPS = 1;

    switch (nOsalSampleFormat)
    {
        case OSAL_EN_ACOUSTIC_SF_S8:    /*!< signed 8 bit */ 
            iBPS = 1 ; //sizeof(char);
            break;
        case OSAL_EN_ACOUSTIC_SF_S16BE:  /*!< signed 16 bit: big endian */ 
        case OSAL_EN_ACOUSTIC_SF_S16LE: /*!< signed 16 bit: little endian */ 
        case OSAL_EN_ACOUSTIC_SF_S16:
            iBPS = 2 ; //sizeof(short int);
            break;
        case OSAL_EN_ACOUSTIC_SF_S32BE: /*!< signed 32 bit, big endian */ 
        case OSAL_EN_ACOUSTIC_SF_S32LE: /*!< signed 32 bit: little endian */ 
        case OSAL_EN_ACOUSTIC_SF_S32:   /*!< signed 32 bit: CPU endian */ 
            iBPS = 4 ; //sizeof(int);
            break;
        case OSAL_EN_ACOUSTIC_SF_F32BE:  /*!< float (IEEE 754) 32 bit, big endian */ 
        case OSAL_EN_ACOUSTIC_SF_F32LE: /*!< float (IEEE 754) 32 bit: little endian */ 
        case OSAL_EN_ACOUSTIC_SF_F32: /*!< float (IEEE 754) 32 bit: little endian */ 
            iBPS = 4; //sizeof(float);
            break;
        default:
            ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_osalGetBytesPerSample ERROR: "
                                      "cannot find OSAL-format (%d)",
                                      (int)nOsalSampleFormat);
            iBPS = 1; //sizeof(char);
    } //switch(nOsalSampleFormat)
	return iBPS;
}

/********************************************************************/ /**
*  FUNCTION: ACOUSTICSRC_alsaConfigure     
*  @brief    opens an alsa stream     
*  @return        
*  @retval    
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tBool ACOUSTICSRC_alsaConfigure(tVoid)
{
	int err;
	snd_pcm_uframes_t framesBufferSize;
	int iDirection;
	unsigned int uiSampleRate;
	unsigned int uiChannels;
	trPCMConfigData *pPCMCfg;
	unsigned int uiPeriods;

	pPCMCfg = &arSrcStateData.rPCMConfig;

	if(!arSrcStateData.bAlsaIsOpen)
	{
		ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_alsaConfigure() START");
		
		/* Allocate a hardware parameters object. */
		snd_pcm_hw_params_malloc( &arSrcStateData.pPCM_hw_params);
		
		/* Fill it in with default values. */
		err = snd_pcm_hw_params_any(arSrcStateData.pAlsaPCM, arSrcStateData.pPCM_hw_params);
		if (err < 0)
		{
			ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure ERROR:"
				   "cannot set default params (%s)", snd_strerror (err));
		}

		/* Set the desired hardware parameters. */

		/* Interleaved mode */
		err = snd_pcm_hw_params_set_access(arSrcStateData.pAlsaPCM,
						   arSrcStateData.pPCM_hw_params, SND_PCM_ACCESS_RW_INTERLEAVED );
		if (err < 0)
		{
			ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure ERROR: "
				   "cannot set access type (%s)", snd_strerror (err));
		}

		err = snd_pcm_hw_params_set_format(arSrcStateData.pAlsaPCM, arSrcStateData.pPCM_hw_params,
		ACOUSTICSRC_alsaConvertFormatOsalToAlsa(pPCMCfg->enSampleFormat));
		
		if (err < 0)
		{
			ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure ERROR: "
				   "cannot set format (%s)", snd_strerror (err));
		}
		
		uiChannels = pPCMCfg->u16NumChannels;

		err = snd_pcm_hw_params_set_channels(arSrcStateData.pAlsaPCM,
							 arSrcStateData.pPCM_hw_params, uiChannels);
		if (err < 0)
		{
			ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure ERROR: "
			"cannot set channels (Channels:%u) (%s)", uiChannels, snd_strerror (err));
		}

		uiSampleRate = pPCMCfg->nSampleRate;
		err = snd_pcm_hw_params_set_rate_near(arSrcStateData.pAlsaPCM,
		arSrcStateData.pPCM_hw_params,
		&uiSampleRate, NULL);
		if (err < 0)
		{
			ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure ERROR: "
			"cannot set rate_near (rate:%u) <%s>", uiSampleRate, snd_strerror (err));
		}
		
		pPCMCfg->nSampleRate = uiSampleRate;
		framesBufferSize = uiSampleRate; // space for one second
		err = snd_pcm_hw_params_set_buffer_size_near(arSrcStateData.pAlsaPCM,
								 arSrcStateData.pPCM_hw_params, &framesBufferSize);
		if (err < 0)
		{
			ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure ERROR: "
				"cannot set buffer size (%u frames) (%s)",
				(unsigned int)framesBufferSize, snd_strerror (err));
		}
		else
		{
			ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_alsaConfigure INFO: "
			  "buffer size == %u frames", (unsigned int)framesBufferSize);
		}

		uiPeriods = 4;
		iDirection = 0;
		err = snd_pcm_hw_params_set_periods_near(arSrcStateData.pAlsaPCM,
				arSrcStateData.pPCM_hw_params, &uiPeriods, &iDirection);
		if (err < 0)
		{
			ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure ERROR: "
				"cannot set periods %u: <%s>", uiPeriods, snd_strerror (err));
		}
		else
		{
			ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_alsaConfigure INFO: "
			  "set periods to <%u>, Dir <%d>", uiPeriods, iDirection);
			
			if(4 != uiPeriods)
			{
				iDirection = 0;
				err = snd_pcm_hw_params_get_periods_min(arSrcStateData.pPCM_hw_params,
												 &uiPeriods, &iDirection);
				if (err < 0)
				{
					ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure ERROR: "
							"cannot get periods_min <%s>", snd_strerror (err));
				}
				else
				{
					ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_alsaConfigure INFO: "
						"get periods_min <%u> Dir <%d>", uiPeriods, iDirection);
				}

				iDirection = 0;
				err = snd_pcm_hw_params_get_periods_max(arSrcStateData.pPCM_hw_params,
											&uiPeriods, &iDirection);
				if (err < 0)
				{
					ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure ERROR: "
								  "cannot get periods_max <%s>", snd_strerror (err));

				}
				else
				{
					ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_alsaConfigure INFO: "
						"get periods_max <%u> Dir <%d>", uiPeriods, iDirection);
				}
			}			
		}

		err = snd_pcm_hw_params(arSrcStateData.pAlsaPCM, arSrcStateData.pPCM_hw_params);
		if (err < 0)
		{
			ACOUSTICSRC_PRINTF_ERRORS( "ACOUSTICSRC_alsaConfigure ERROR: "
				 "unable to set hw parameters: <%s>", snd_strerror(err) );
			arSrcStateData.bAlsaIsOpen = FALSE;
		}
		else 
		{
			snd_pcm_hw_params_free(arSrcStateData.pPCM_hw_params);
			arSrcStateData.pPCM_hw_params = NULL;

			set_swparams(arSrcStateData.pAlsaPCM);
			
			ACOUSTICSRC_vDumpAlsaStatus();
			arSrcStateData.bAlsaIsOpen = TRUE;
		} 
	}
	else 
	{
		ACOUSTICSRC_PRINTF_ERRORS("ACOUSTICSRC_alsaConfigure() ALWAYS OPEN");
	} 

	ACOUSTICSRC_PRINTF_U3("ACOUSTICSRC_alsaConfigure() END - OPEN: %u",
		(unsigned int)arSrcStateData.bAlsaIsOpen);

	return arSrcStateData.bAlsaIsOpen;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_Version
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_Version(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	if ((tPS32)s32Arg != NULL)
	{
		vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_VERSION,
			"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

		/* write version */
		*(tPS32)s32Arg = ACOUSTICSRC_C_S32_IO_VERSION;

		vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_VERSION,
			"exit", 0, 0, (tU32)s32Arg,	(tU32)arSrcStateData.enPlayState);
	}
	else
	{
		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_IOCTRL_VERSION,
			OSAL_E_INVALIDVALUE, "invval", 0, 0, (tU32)arSrcStateData.enPlayState, 0);

		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_Convert
*
*  @brief         Sample converts the input PCM data and dumps it into a file as a raw file
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_Convert(tS32 s32Arg)
{
	tU32 u32ErrorCode;
	OSAL_trAcousticSrcConvert* pSrcInfo;
	tU32 u32BytesConverted = 0;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_3, EN_IOCTRL_CONVERT,
	"enter", 0, 0, (tU32)s32Arg,(tU32)arSrcStateData.enPlayState);

	pSrcInfo = (OSAL_trAcousticSrcConvert*)s32Arg;

	if(NULL == pSrcInfo)
	{
		/* nullpointer */
		return OSAL_E_INVALIDVALUE;
	}

	if((pSrcInfo->nTimeout > ACOUSTICSRC_C_U32_DEFAULT_WRITE_TIMEOUT)||
		(NULL == pSrcInfo->pvBuffer))
	{
		return OSAL_E_INVALIDVALUE;
	}

	/* OK, do the SRC operation */
	u32ErrorCode = u32DoSRCOperation(pSrcInfo,
						&u32BytesConverted,(tS32)pSrcInfo->nTimeout);

	vTraceAcousticSrcInfo(TR_LEVEL_USER_3, EN_IOCTRL_CONVERT,
					 "exit", 0, 0, (tU32)s32Arg,
					 (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      AcousticSrcIOCtrl_RegNotification
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static void AcousticSrcIOCtrl_RegNotification(tS32 s32Arg)
{
	OSAL_trAcousticSrcCallbackReg* pCallback;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_REG_NOTIFICATION,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	pCallback = (OSAL_trAcousticSrcCallbackReg*)s32Arg;

	if(NULL == pCallback)
	{
		/* unregister callback */
		arSrcStateData.rCallback.pfEvCallback = NULL;
		arSrcStateData.rCallback.pvCookie     = NULL;
	}
	else if(NULL == pCallback->pfEvCallback)
	{
		/* unregister callback */
		arSrcStateData.rCallback.pfEvCallback = NULL;
		arSrcStateData.rCallback.pvCookie     = NULL;
	}
	else
	{
		/* register callback */
		arSrcStateData.rCallback.pfEvCallback =
			(OSAL_tpfAcousticSrcEvCallback)pCallback->pfEvCallback;
		arSrcStateData.rCallback.pvCookie = (tPVoid)pCallback->pvCookie;
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_REG_NOTIFICATION,
		"exit", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_WaitEvent
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_WaitEvent(tS32 s32Arg)
{
	OSAL_trAcousticSrcWaitEvent* pWaitEvent;
	OSAL_tEventMask tWaitEventMask = 0;
	tS32 s32Ret = 0;    
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_WAITEVENT,
		 "enter", 0, 0, (tU32)s32Arg,
		 (tU32)arSrcStateData.enPlayState);

	pWaitEvent = (OSAL_trAcousticSrcWaitEvent*)s32Arg;

	if (NULL == pWaitEvent)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		(tVoid)OSAL_s32SemaphoreWait(arSrcStateData.hSemClose, OSAL_C_TIMEOUT_NOBLOCKING);

		s32Ret = OSAL_s32EventWait(arSrcStateData.hOsalEvent,
				   ACOUSTICSRC_EN_EVENT_MASK_NOTI_ANY, OSAL_EN_EVENTMASK_OR,
				   pWaitEvent->nTimeout, &tWaitEventMask);

		(tVoid)OSAL_s32SemaphorePost(arSrcStateData.hSemClose);

		if(OSAL_OK == s32Ret)
		{
			if ( tWaitEventMask & ACOUSTICSRC_EN_EVENT_MASK_NOTI_CANCEL )
			{
				/* device was closed during wait, return with cancel */
				u32ErrorCode = OSAL_E_CANCELED;
			}
			else 
			{
				if ( tWaitEventMask & ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED )
				{
					u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED);
							  pWaitEvent->enEvent = OSAL_EN_ACOUSTICSRC_EVAUDIOSTOPPED;
				}
				else if ( tWaitEventMask & ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_XRUN )
				{
					u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_XRUN);
							  pWaitEvent->enEvent = OSAL_EN_ACOUSTICSRC_EVERRTHRESHREACHED;
							  pWaitEvent->rErrThresholdInfo.enErrType = OSAL_EN_ACOUSTIC_ERRTYPE_XRUN;
							  pWaitEvent->rErrThresholdInfo.s32Threshold = 0;  /* unused */
				}
				else if ( tWaitEventMask & ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_BITSTREAM )
				{
					u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_BITSTREAM);
							  pWaitEvent->enEvent = OSAL_EN_ACOUSTICSRC_EVERRTHRESHREACHED;
							  pWaitEvent->rErrThresholdInfo.enErrType = OSAL_EN_ACOUSTIC_ERRTYPE_BITSTREAM;
							  pWaitEvent->rErrThresholdInfo.s32Threshold = 0;  /* unused */
				}
				else if ( tWaitEventMask & ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_NOVALIDDATA )
				{
					u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_NOVALIDDATA);
							  pWaitEvent->enEvent = OSAL_EN_ACOUSTICSRC_EVERRTHRESHREACHED;
							  pWaitEvent->rErrThresholdInfo.enErrType = OSAL_EN_ACOUSTIC_ERRTYPE_NOVALIDDATA;
							  pWaitEvent->rErrThresholdInfo.s32Threshold = 0;  /* unused */
				}
				else if ( tWaitEventMask & ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_WRONGFORMAT )
				{
					u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_WRONGFORMAT);
							  pWaitEvent->enEvent = OSAL_EN_ACOUSTICSRC_EVERRTHRESHREACHED;
							  pWaitEvent->rErrThresholdInfo.enErrType = OSAL_EN_ACOUSTIC_ERRTYPE_WRONGFORMAT;
							  pWaitEvent->rErrThresholdInfo.s32Threshold = 0;  /* unused */
				}
				else if ( tWaitEventMask & ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_INTERNALERR )
				{
					u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_INTERNALERR);
							  pWaitEvent->enEvent = OSAL_EN_ACOUSTICSRC_EVERRTHRESHREACHED;
							  pWaitEvent->rErrThresholdInfo.enErrType = OSAL_EN_ACOUSTIC_ERRTYPE_INTERNALERR;
							  pWaitEvent->rErrThresholdInfo.s32Threshold = 0;  /* unused */
				}
				else if ( tWaitEventMask & ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_FATAL )
				{
					u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_FATAL);
							  pWaitEvent->enEvent = OSAL_EN_ACOUSTICSRC_EVERRTHRESHREACHED;
							  pWaitEvent->rErrThresholdInfo.enErrType = OSAL_EN_ACOUSTIC_ERRTYPE_FATALERR;
							  pWaitEvent->rErrThresholdInfo.s32Threshold = 0;  /* unused */
				}
			} 
		}
		else 
		{
			if ( OSAL_E_TIMEOUT == OSAL_u32ErrorCode() )
			{
				u32ErrorCode = OSAL_E_TIMEOUT;
			}
			else
			{
				u32ErrorCode = OSAL_E_UNKNOWN;
			}
		}
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_WAITEVENT,
		 "exit", 0, 0, (tU32)0, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetSuppDecoder
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetSuppDecoder(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	OSAL_trAcousticCodecCapability* prCodecCap;
	tU32 u32CopyIdx;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_DECODER,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	prCodecCap = (OSAL_trAcousticCodecCapability*)s32Arg;

	if(NULL == prCodecCap)
	{
		/* nullpointer */
		u32ErrorCode =  OSAL_E_INVALIDVALUE;
	}
	else 
	{
		/* OK, get codec capabilities */
		for(u32CopyIdx = 0;
		   (u32CopyIdx < prCodecCap->u32ElemCnt) &&
		   (u32CopyIdx < NUM_ARRAY_ELEMS(aenSupportedCodecs));
		   u32CopyIdx++)
		{
			prCodecCap->penCodecs[u32CopyIdx] = aenSupportedCodecs[u32CopyIdx];
		}
		prCodecCap->u32ElemCnt = NUM_ARRAY_ELEMS(aenSupportedCodecs);
		prCodecCap->u32MaxCodecCnt = (tU32)ACOUSTICSRC_C_U8_MAXCODECCNT;
	} 

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_DECODER,
			"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetDecoder
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetDecoder(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	OSAL_tenAcousticCodec *penCodec;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETDECODER,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	penCodec = (OSAL_tenAcousticCodec*)s32Arg;

	if(NULL == penCodec)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		*penCodec = arSrcStateData.enCodec;
	} 


	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETDECODER,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetSuppSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetSuppSamplerate(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticSampleRateCapability* prSampleRateCap;
	tU32 u32CopyIdx;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLERATE,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	prSampleRateCap = (OSAL_trAcousticSampleRateCapability*)s32Arg;

	if(NULL == prSampleRateCap)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		if(ACOUSTICSRC_C_EN_DEFAULT_DECODER != prSampleRateCap->enCodec)
		{
			/* sample rate only configurable for PCM */
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
		else 
		{
			/* OK, get samplerate capabilities */
			for(u32CopyIdx = 0;
			(u32CopyIdx < prSampleRateCap->u32ElemCnt) &&
			(u32CopyIdx < NUM_ARRAY_ELEMS(anSupportedSampleratesFrom));
			u32CopyIdx++)
			{
              prSampleRateCap->pnSamplerateFrom[u32CopyIdx] = anSupportedSampleratesFrom[u32CopyIdx];
              prSampleRateCap->pnSamplerateTo[u32CopyIdx] = anSupportedSampleratesTo[u32CopyIdx];
           }

           prSampleRateCap->u32ElemCnt = NUM_ARRAY_ELEMS(anSupportedSampleratesFrom);
       } 
   } 

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLERATE,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetSamplerate(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticSampleRateCfg* prSampleRateCfg;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLERATE,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	prSampleRateCfg = (OSAL_trAcousticSampleRateCfg*)s32Arg;

	if(NULL == prSampleRateCfg)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		if(ACOUSTICSRC_C_EN_DEFAULT_DECODER != prSampleRateCfg->enCodec)
		{
			  /* sample rate only configurable for PCM */
			  u32ErrorCode =  OSAL_E_INVALIDVALUE;
		}
		else 
		{
			   /* OK, get samplerate */
			   prSampleRateCfg->nSamplerate = arSrcStateData.rPCMConfig.nSampleRate;
		}
	} 

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLERATE,
		 "exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_SetSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_SetSamplerate(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	OSAL_trAcousticSampleRateCfg* prSampleRateCfg;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLERATE,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	prSampleRateCfg = (OSAL_trAcousticSampleRateCfg*)s32Arg;

	if(NULL == prSampleRateCfg)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		if(ACOUSTICSRC_C_EN_DEFAULT_DECODER != prSampleRateCfg->enCodec)
		{
			/* sample rate only configurable for PCM */
			u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
		}
		/*Checks whether the passed sample rate is supported or not*/
		else if (bIsSamplerateValid(prSampleRateCfg->nSamplerate))
		{
			arSrcStateData.rPCMConfig.nSampleRate = prSampleRateCfg->nSamplerate;
		}
		else
		{
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
	} 

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLERATE,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetSuppSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetSuppSampleformat(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	OSAL_trAcousticSampleFormatCapability* prSampleFormatCap;
	tU32 u32CopyIdx;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLEFORMAT,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	prSampleFormatCap = (OSAL_trAcousticSampleFormatCapability*)s32Arg;

	if(NULL == prSampleFormatCap)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		if(ACOUSTICSRC_C_EN_DEFAULT_DECODER != prSampleFormatCap->enCodec)
		{
			/* sample format only configurable for PCM */
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
		else 
		{
			/* OK, get sampleformat capabilities */
			for (u32CopyIdx = 0;
			 (u32CopyIdx < prSampleFormatCap->u32ElemCnt) &&
			 (u32CopyIdx < NUM_ARRAY_ELEMS(aenSupportedSampleformats));
			 u32CopyIdx++)
			{
				prSampleFormatCap->penSampleformats[u32CopyIdx] = aenSupportedSampleformats[u32CopyIdx];
			}

			prSampleFormatCap->u32ElemCnt = NUM_ARRAY_ELEMS(aenSupportedSampleformats);

		} 
	} 

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLEFORMAT,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetSampleformat(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	OSAL_trAcousticSampleFormatCfg* prSampleFormatCfg;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLEFORMAT,
		"enter", 0, 0, (tU32)s32Arg,(tU32)arSrcStateData.enPlayState);

	prSampleFormatCfg = (OSAL_trAcousticSampleFormatCfg*)s32Arg;

	if(NULL == prSampleFormatCfg)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		if(ACOUSTICSRC_C_EN_DEFAULT_DECODER != prSampleFormatCfg->enCodec)
		{
			/* sample format only configurable for PCM */
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
		else 
		{
			/* OK, get sampleformat */
			prSampleFormatCfg->enSampleformat = arSrcStateData.rPCMConfig.enSampleFormat;
		}
	} 

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLEFORMAT,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_SetSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_SetSampleformat(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	OSAL_trAcousticSampleFormatCfg* prSampleFormatCfg;
	prSampleFormatCfg = (OSAL_trAcousticSampleFormatCfg*)s32Arg;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLEFORMAT,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	if(NULL == prSampleFormatCfg)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		ACOUSTICSRC_PRINTF_U3("s32AcousticSrcIOCtrl_SetSampleformat"
				   " PlayState %d codec %u SampleFmt %u",
				   (int)arSrcStateData.enPlayState,
				   (unsigned int)prSampleFormatCfg->enCodec,
				   (unsigned int)prSampleFormatCfg->enSampleformat);
				   
		if(ACOUSTICSRC_C_EN_DEFAULT_DECODER != prSampleFormatCfg->enCodec)
		{
			/* sample format only configurable for PCM */
			u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
		}
		else 
		{
			if (bIsSampleformatValid(prSampleFormatCfg->enSampleformat))
			{
				/* OK, set sampleformat */
				arSrcStateData.rPCMConfig.enSampleFormat = prSampleFormatCfg->enSampleformat;
			}
			else
			{
				u32ErrorCode = OSAL_E_INVALIDVALUE;
			}
		} 

	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLEFORMAT,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetSuppChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetSuppChannels(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticChannelCapability* prChanCap;
	tU32 u32CopyIdx;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_CHANNELS,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	prChanCap = (OSAL_trAcousticChannelCapability*)s32Arg;

	if(NULL == prChanCap)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		/* OK, get channel num capabilities */
		for(u32CopyIdx = 0;
		(u32CopyIdx < prChanCap->u32ElemCnt) &&
		(u32CopyIdx < NUM_ARRAY_ELEMS(au16SupportedChannelnums));
		u32CopyIdx++)
		{
			prChanCap->pu32NumChannels[u32CopyIdx] = au16SupportedChannelnums[u32CopyIdx];
		}
		
		prChanCap->u32ElemCnt = NUM_ARRAY_ELEMS(au16SupportedChannelnums);
	} 

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_CHANNELS,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetChannels(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	tPU16 pu16NumChannels;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETCHANNELS,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	pu16NumChannels = (tPU16)s32Arg;

	if(NULL == pu16NumChannels)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		/* OK, get number of channels */
		*pu16NumChannels = arSrcStateData.rPCMConfig.u16NumChannels;
	} 

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETCHANNELS,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_SetChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_SetChannels(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	tU16 u16NumChannels;
	u16NumChannels = (tU16)s32Arg;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETCHANNELS,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	if(ACOUSTICSRC_EN_STATE_STOPPED != arSrcStateData.enPlayState)
	{
		/* stream currently active, set not available */
		u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
	}
	else 
	{
		if (bIsChannelnumValid(u16NumChannels))
		{
			/* OK, set number of channels */
			arSrcStateData.rPCMConfig.u16NumChannels = u16NumChannels;
		}
		else
		{
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
	} 

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETCHANNELS,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetSuppBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetSuppBuffersize(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticBufferSizeCapability* prBufferSizeCap;
	tU32 u32CopyIdx;
	tPCU32 pcu32SupArray = NULL;
	tU32 u32SupArrayCnt = 0;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_BUFFERSIZE,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	prBufferSizeCap = (OSAL_trAcousticBufferSizeCapability*)s32Arg;

	if(NULL == prBufferSizeCap)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else 
	{
		switch (prBufferSizeCap->enCodec)
		{
			case OSAL_EN_ACOUSTIC_DEC_PCM:
				pcu32SupArray = au32SupportedBuffersizesPCM;
				u32SupArrayCnt = NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM);
			break;

			default:
				u32ErrorCode = OSAL_E_INVALIDVALUE;
			break;
		}

		if ( NULL != pcu32SupArray )
		{
			/* OK, get channel num capabilities */
			for(u32CopyIdx = 0;
			(u32CopyIdx < prBufferSizeCap->u32ElemCnt) &&
			(u32CopyIdx < u32SupArrayCnt); u32CopyIdx++)
			{
				prBufferSizeCap->pnBuffersizes[u32CopyIdx] = pcu32SupArray[u32CopyIdx];
			}
			prBufferSizeCap->u32ElemCnt = u32SupArrayCnt;
		}
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_BUFFERSIZE,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetBuffersize(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticBufferSizeCfg* pBufferSizeCfg;
	
	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETBUFFERSIZE,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	pBufferSizeCfg = (OSAL_trAcousticBufferSizeCfg*)s32Arg;

	if(NULL == pBufferSizeCfg)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else
	{
		/* OK, get buffersize */
		if (bIsCodecValid(pBufferSizeCfg->enCodec))
		{
			pBufferSizeCfg->nBuffersize = arSrcStateData.anBufferSize;
		}
		else
		{
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}

		ACOUSTICSRC_PRINTF_U3("s32AcousticSrcIOCtrl_GetBuffersize"
			" PlayState %d Codec %u BUffersize %u",
			(int)arSrcStateData.enPlayState, (unsigned int)pBufferSizeCfg->enCodec,
			(unsigned int)pBufferSizeCfg->nBuffersize);
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETBUFFERSIZE,
		"exit", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_SetBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_SetBuffersize(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	OSAL_trAcousticBufferSizeCfg* pBufferSizeCfg;
	pBufferSizeCfg = (OSAL_trAcousticBufferSizeCfg*)s32Arg;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETBUFFERSIZE,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	if(ACOUSTICSRC_EN_STATE_STOPPED != arSrcStateData.enPlayState)
	{
		/* stream currently running, set not allowed */
		u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
	}
	else 
	{
		if(NULL == pBufferSizeCfg)
		{
			/* nullpointer */
			u32ErrorCode = OSAL_E_INVALIDVALUE;
		}
		else 
		{
			ACOUSTICSRC_PRINTF_U3("s32AcousticSrcIOCtrl_SetBuffersize"
				" PlayState %d Codec %u BUffersize %u",
				(int)arSrcStateData.enPlayState, (unsigned int)pBufferSizeCfg->enCodec,
				(unsigned int)pBufferSizeCfg->nBuffersize);

			if(bIsCodecValid (pBufferSizeCfg->enCodec) &&
			bIsBuffersizeValid(pBufferSizeCfg->nBuffersize, pBufferSizeCfg->enCodec))
			{
				/* OK, set buffersize */
				arSrcStateData.anBufferSize = pBufferSizeCfg->nBuffersize;
			}
			else
			{
				u32ErrorCode = OSAL_E_INVALIDVALUE;
			}
		} 
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETBUFFERSIZE,
		 "exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_GetFilePath
*
*  @brief      Provides the output file's location.
*			This file will have output wav samples @ 22050Hz sampling rate   
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_GetFilePath(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	OSAL_trEX_Open_Arg* pFilePathCfg;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETFILEPATH,
		 "enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	pFilePathCfg = (OSAL_trEX_Open_Arg*)s32Arg;

	if(NULL == pFilePathCfg)
	{
		/* nullpointer */
		u32ErrorCode = OSAL_E_INVALIDVALUE;
	}
	else
	{
		pFilePathCfg->enAccess = OSAL_EN_READONLY;

		pFilePathCfg->FileName = s8AcousticSRCFilename;

	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETFILEPATH,
		"exit", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_SetErrThr
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_SetErrThr(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	
	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETERRTHR,
			  "enter", 0, 0, (tU32)s32Arg,
			  (tU32)arSrcStateData.enPlayState);

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETERRTHR,
		"exit", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_Start
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_Start(tS32 s32Arg)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_START,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	switch (arSrcStateData.enPlayState)
	{
		case ACOUSTICSRC_EN_STATE_STOPPED:
		{
			tBool bRet = ACOUSTICSRC_alsaConfigure();
			if(!bRet)
			{
				u32ErrorCode = OSAL_E_NOACCESS;
			} 
			u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ANY);
			/* new state is "Prepare" */
			arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_PREPARE;
			break;
		}

		case ACOUSTICSRC_EN_STATE_PREPARE:
		case ACOUSTICSRC_EN_STATE_ACTIVE:
		{
			/* if already in prepare state or running, there is nothing to do */
			break;
		}

		default:
		{
			vTraceAcousticSrcError(TR_LEVEL_ERRORS,  EN_IOCTRL_START, OSAL_E_UNKNOWN,
							"state",(tU32)arSrcStateData.enPlayState, 0, 0, 0);
			u32ErrorCode = OSAL_E_UNKNOWN;
			break;
		}
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_START,
		"exit", 0, 0, u32ErrorCode, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_Stop
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_Stop(tS32 s32Arg)
{
	tU32 u32ErrorCode;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_STOP,
				 "enter", 0, 0, (tU32)s32Arg,
				 (tU32)arSrcStateData.enPlayState);

	switch (arSrcStateData.enPlayState)
	{
		case ACOUSTICSRC_EN_STATE_PREPARE:
		{
			/* new state is "Stopping" (draining) */
			arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_STOPPING;
			break;
		}

		case ACOUSTICSRC_EN_STATE_ACTIVE:
		{
			/* We already sent buffers, one buffer might be blocked in
			* a write call. When the write call unblocks, the stop bit
			* will be set in it. Any further write calls will be rejected
			* after the play state is set to stopping.
			*/
			arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_STOPPING;
			break;
		}

		case ACOUSTICSRC_EN_STATE_STOPPED:
			/* nothing to do, ignore */
			break;

		default:
			break;
	}

	/* send STOP command to output device */
	u32ErrorCode = u32OutputDeviceStop();

	arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_STOPPED;
	/* Notify "audio stopped" */
	/* notification via callback */
	if (NULL != arSrcStateData.rCallback.pfEvCallback)
	{
		arSrcStateData.rCallback.pfEvCallback(OSAL_EN_ACOUSTICSRC_EVAUDIOSTOPPED,
				NULL, arSrcStateData.rCallback.pvCookie);
	}

	/* notification via event */
	{
		ACOUSTICSRC_PRINTF_U3("s32AcousticSrcIOCtrl_Stop:notify"
				" ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED");
		u32SetEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED);
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_STOP,
		"exit", u32ErrorCode, 0, 0, (tU32)arSrcStateData.enPlayState);

	return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      u32AcousticSrcIOCtrl_Abort
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
************************************************************************/
static tU32 u32AcousticSrcIOCtrl_Abort(tS32 s32Arg)
{
	tU32 u32ErrorCode;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_ABORT,
		"enter", 0, 0, (tU32)s32Arg, (tU32)arSrcStateData.enPlayState);

	u32ErrorCode = u32AbortStream();

	arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_STOPPED;
	/* Notify "audio stopped" */
	/* notification via callback */
	if (NULL != arSrcStateData.rCallback.pfEvCallback)
	{
		ACOUSTICSRC_PRINTF_U3("s32AcousticSrcIOCtrl_Abort:"
				" callback ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED");
		arSrcStateData.rCallback.pfEvCallback( OSAL_EN_ACOUSTICSRC_EVAUDIOSTOPPED,
								NULL, arSrcStateData.rCallback.pvCookie);
	}

	/* notification via event */
	{
		ACOUSTICSRC_PRINTF_U3("s32AcousticSrcIOCtrl_Abort:"
						 " notify ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED");
		u32SetEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED);
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_IOCTRL_ABORT,
				 "exit", u32ErrorCode, 0, 0, (tU32)arSrcStateData.enPlayState);
				 
	return u32ErrorCode;
}

/*************************************************************************/ /**
*  FUNCTION:      u32DoSRCOperation
*
*  @brief         worker function for performing SRC and dumping
*				the sample converted data to a file in raw format
*
*  @param         s32ID          stream ID
*  @param         u32FD          file handle
*  @param         prWriteInfo    additional control information
*  @param         pubytesWritten returns written bytes
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tU32 u32DoSRCOperation(const OSAL_trAcousticSrcConvert* prSrcInfo,
						 tPU32 pu32BytesConverted,
                         tS32  s32TimeOut)
{
	tU32 u32Ret = OSAL_E_NOERROR;

	/*validation of the buffer size that needs to be written is done here.
	Here we are checking for the range and not for the equality, as we need to
	cover the possibility of size thats needs to be written being less than the
	set buffer size, say in the case of end of stream. */

	if(prSrcInfo->u32BufferSize > arSrcStateData.anBufferSize)
	{
		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_DO_SRCOPERATION, OSAL_E_INVALIDVALUE,
		"size", (tU32)(tU32)arSrcStateData.enPlayState, prSrcInfo->u32BufferSize, 0, 0);
		u32Ret = OSAL_E_INVALIDVALUE;
	}
	else
	{
		u32Ret = u32OutputDataConvert(prSrcInfo->pvBuffer,
			prSrcInfo->u32BufferSize, pu32BytesConverted, s32TimeOut);
	}
	return u32Ret;
}


/*************************************************************************/ /**
*  FUNCTION:      u32SetEvent
*
*  @brief         Sets specified event(s)
*
*  @param         prDspIf          DSP interface
*  @param         tWaitEventMask   event(s) to be set
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tU32 u32SetEvent(const trACOUSTICSRC_StateData *prStateData,
                 const OSAL_tEventMask tWaitEventMask)
{
	tU32 u32ErrorCode = OSAL_E_NOERROR;
	tS32 s32RetVal;

	/* set event */
	s32RetVal = OSAL_s32EventPost(prStateData->hOsalEvent,
					tWaitEventMask, OSAL_EN_EVENTMASK_OR);

	if ( (tS32)OSAL_OK != s32RetVal )
	{
		u32ErrorCode = OSAL_u32ErrorCode();

		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_SET_EVENT,
							   u32ErrorCode, "evset", 0, 0, 0, 0);
	}
	return u32ErrorCode;
}

/*************************************************************************/ /**
*  FUNCTION:      u32ClearEvent
*
*  @brief         Clears specified event(s)
*
*  @param         prDspIf          DSP interface
*  @param         tWaitEventMask   event(s) to be cleared
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tU32 u32ClearEvent(const trACOUSTICSRC_StateData *prStateData, const OSAL_tEventMask tWaitEventMask)
{
    tU32 u32ErrorCode = OSAL_E_NOERROR;

	/* clear event */
	tS32 s32Ret = OSAL_s32EventPost(prStateData->hOsalEvent,
                              	  ~tWaitEventMask,
                              	  OSAL_EN_EVENTMASK_AND);

    if ((tS32)OSAL_OK != s32Ret)
	{
		u32ErrorCode = OSAL_u32ErrorCode();
		vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_CLEAR_EVENT, u32ErrorCode,
                     "evclr", 0, 0, 0, 0);
	}
	return u32ErrorCode;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsSampleformatValid
*
*  @brief         Checks whether provided sample format value is valid
*
*  @param         enSampleformat  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tBool bIsSampleformatValid(OSAL_tenAcousticSampleFormat enSampleformat)
{
	tU8 u8Idx;
	for (u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(aenSupportedSampleformats); u8Idx++)
	{
		if (aenSupportedSampleformats[u8Idx] == enSampleformat)
		{
			return TRUE;
		}
	}

	return FALSE;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsSamplerateValid
*
*  @brief         Checks whether provided sample rate value is valid
*
*  @param         nSamplerate  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tBool bIsSamplerateValid(OSAL_tAcousticSampleRate nSamplerate)
{
	tU8 u8Idx;
	for (u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(anSupportedSampleratesFrom); u8Idx++)
	{
		if ((anSupportedSampleratesFrom[u8Idx] <= nSamplerate) &&
		(anSupportedSampleratesTo[u8Idx] >= nSamplerate))
		{
			return TRUE;
		}
	}

	return FALSE;
}


/*************************************************************************/ /**
*  FUNCTION:      bIsChannelnumValid
*
*  @brief         Checks whether provided channel num value is valid
*
*  @param         u16Channelnum  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tBool bIsChannelnumValid(tU16 u16Channelnum)
{
	tU8 u8Idx;
	for (u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(au16SupportedChannelnums); u8Idx++)
	{
		if (au16SupportedChannelnums[u8Idx] == u16Channelnum)
		{
			return TRUE;
		}
	}

	return FALSE;
}


/*************************************************************************/ /**
*  FUNCTION:      bIsBuffersizeValid
*
*  @brief         Checks whether provided buffer size value is valid
*
*  @param         u32Buffersize  value to be checked
*  @param         enCodec        acoustic codec of buffer
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tBool bIsBuffersizeValid(tU32 u32Buffersize, OSAL_tenAcousticCodec enCodec)
{
	tU8 u8Idx;
	tPCU32 pcu32SupArray = NULL;
	tU32 u32SupArrayCnt = 0;

	switch (enCodec)
	{
		case OSAL_EN_ACOUSTIC_DEC_PCM:
			pcu32SupArray = au32SupportedBuffersizesPCM;
			u32SupArrayCnt = NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM);
			break;

		case OSAL_EN_ACOUSTIC_DEC_MP3:
		case OSAL_EN_ACOUSTIC_DEC_AMRWB:
		default:
			return FALSE;
	}

	for (u8Idx=0; u8Idx < u32SupArrayCnt; u8Idx++)
	{
		if (pcu32SupArray[u8Idx] == u32Buffersize)
		{
			return TRUE;
		}
	}

	return FALSE;
}

/*************************************************************************/ /**
*  FUNCTION:      u32InitOutputDevice
*
*  @brief         Opens and initializes device for audio output
*
*  @param         void
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tU32 u32InitOutputDevice(tVoid)
{
	tU32 u32Ret = OSAL_E_NOERROR;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_INITOUTPUTDEVICE,
				 "enter", 0, 0, 0, 0);

	if(arSrcStateData.pAlsaPCM == NULL)
	{
		int err;
		snd_output_stdio_attach(&arSrcStateData.pSndOutput, stdout, 0);

		err = snd_pcm_open(&arSrcStateData.pAlsaPCM, arSrcStateData.szAlsaDeviceName,
					  SND_PCM_STREAM_PLAYBACK, 0);
		if (err < 0) 
		{
			ACOUSTICSRC_PRINTF_ERRORS("u32InitOutputDevice ERROR:"
								   " unable to open pcm device <%s>: %s",
								   (const char*)arSrcStateData.szAlsaDeviceName,
								   snd_strerror(err) );
			arSrcStateData.pAlsaPCM = NULL;
			u32Ret =  OSAL_E_NOACCESS;
		} 
	}
	else
	{
		u32Ret = OSAL_E_ALREADYEXISTS;
	}

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_INITOUTPUTDEVICE,
				 "exit", u32Ret, 0, 0, 0);

	return u32Ret;
}


/*************************************************************************/ /**
*  FUNCTION:      u32UnInitOutputDevice
*
*  @brief         Closes device
*
*  @param         s32ID id
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tU32 u32UnInitOutputDevice(tVoid)
{
	tU32 u32Ret = OSAL_E_NOERROR;
	int iErr;

	if(arSrcStateData.pAlsaPCM != NULL)
	{
		/* close output device */
		//ALSA CLOSE
		arSrcStateData.bAlsaIsOpen = FALSE;
		snd_pcm_drop(arSrcStateData.pAlsaPCM);
		iErr = snd_pcm_close(arSrcStateData.pAlsaPCM);
		if(iErr < 0)
		{
			vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_UNINITOUTPUTDEVICE,
				u32Ret, "pcmclo1", (tU32)iErr,
				(tU32)arSrcStateData.pAlsaPCM, 0, 0);   
				
			ACOUSTICSRC_PRINTF_ERRORS("u32UnInitOutputDevice snd_pcm_close(%p)"
				" returns: <%s>", arSrcStateData.pAlsaPCM, snd_strerror(iErr));
		}
		else 
		{
		ACOUSTICSRC_PRINTF_U3("u32UnInitOutputDevice snd_pcm_close(%p)"
			" returns: <%s>", arSrcStateData.pAlsaPCM, snd_strerror(iErr));
		} 
		arSrcStateData.pAlsaPCM = NULL;
	}
	else
	{ 	// it's not an ERROR
		ACOUSTICSRC_PRINTF_U1("u32UnInitOutputDevice ALSA handle is NULL");
	}

	return u32Ret;
}

/*************************************************************************/ /**
*  FUNCTION:      u32AbortStream
*
*  @brief         Aborts stream immediately
*
*  @param         s32ID              stream ID
*  @param         u32FD              file handle
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tU32 u32AbortStream(tVoid)
{
	tU32 u32RetVal;

    /* send ABORT command to output device */
    u32RetVal = u32OutputDeviceAbort();
   
	/* new state is "Stopped" */
	arSrcStateData.enPlayState = ACOUSTICSRC_EN_STATE_STOPPED;

    /* Notify "audio stopped" */
    /* notification via callback */
    if (NULL != arSrcStateData.rCallback.pfEvCallback)
    {
		arSrcStateData.rCallback.pfEvCallback(OSAL_EN_ACOUSTICSRC_EVAUDIOSTOPPED,
							NULL, arSrcStateData.rCallback.pvCookie);
    }

   /* notification via event*/
   u32SetEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED);
   u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_NOTI_ANY);
    
   u32SetEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_AUDIO_CANCEL);
   u32ClearEvent(&arSrcStateData, ACOUSTICSRC_EN_EVENT_MASK_AUDIO_ANY);
 
   return u32RetVal;
}

/*************************************************************************/ /**
*  FUNCTION:      u32OutputDeviceStop
*
*  @brief         Sends the STOP command to the output device
*
*  @param         s32ID              stream ID
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tU32 u32OutputDeviceStop(tVoid)
{
    tU32 u32RetVal = OSAL_E_NOERROR;
    int iErr = 0;

    vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_OUTPUTDEVICE_STOP,
                           "enter", 0, 0, 0, 0);
    if(arSrcStateData.pAlsaPCM != NULL)
    {
		snd_pcm_state_t sndStatus;
		tBool bDoDrain;

		sndStatus = snd_pcm_state(arSrcStateData.pAlsaPCM);
		switch(sndStatus)
		{
			case SND_PCM_STATE_OPEN:
			case SND_PCM_STATE_SETUP:
			case SND_PCM_STATE_PREPARED:
			case SND_PCM_STATE_DISCONNECTED:
				bDoDrain = FALSE;
				break;
				/** Running */
			case SND_PCM_STATE_RUNNING:
			case SND_PCM_STATE_DRAINING:
			case SND_PCM_STATE_PAUSED:
			case SND_PCM_STATE_SUSPENDED:
			case SND_PCM_STATE_XRUN:
			default:
				bDoDrain = TRUE;
				break;
		}

		if(bDoDrain)
		{
			iErr = snd_pcm_drain(arSrcStateData.pAlsaPCM);
			if(iErr < 0)
			{
				u32RetVal = OSAL_E_UNKNOWN;
				vTraceAcousticSrcError(TR_LEVEL_ERRORS,
									   EN_OUTPUTDEVICE_STOP,
									   u32RetVal,
									   "pcmdrain", u32RetVal,
									   (tU32)iErr, 0, 0);
				ACOUSTICSRC_PRINTF_ERRORS("snd_pcm_drain returns: <%s>",
										  snd_strerror(iErr));
				ACOUSTICSRC_vDumpAlsaStatus();
			}
			else 
			{
				u32RetVal = OSAL_E_NOERROR;
			} 
		}
		else 
		{
		  u32RetVal = OSAL_E_NOERROR;
		} 
    }
    else 
    {
		u32RetVal = OSAL_E_NOFILEDESCRIPTOR;
    } 

    vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_OUTPUTDEVICE_STOP,
               "exit", 0, u32RetVal, (tU32)iErr, 0);
    return u32RetVal;
}

/*************************************************************************/ /**
*  FUNCTION:      u32OutputDeviceAbort
*
*  @brief         Sends the ABORT command to the output device
*
*  @param         s32ID              stream ID
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tU32 u32OutputDeviceAbort(tVoid)
{
	tU32 u32RetVal = OSAL_E_NOERROR;
	int iErr = -1;

	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_OUTPUTDEVICE_ABORT,
				  "enter", 0, 0, 0, 0);
	if(arSrcStateData.pAlsaPCM != NULL)
	{
		snd_pcm_state_t sndStatus;
		tBool bDoDrop;

		sndStatus = snd_pcm_state(arSrcStateData.pAlsaPCM);
		switch(sndStatus)
		{
			case SND_PCM_STATE_OPEN:
			case SND_PCM_STATE_SETUP:
			case SND_PCM_STATE_PREPARED:
			case SND_PCM_STATE_DISCONNECTED:
				bDoDrop = FALSE;
				break;
				
			/** Running */
			case SND_PCM_STATE_RUNNING:
			case SND_PCM_STATE_DRAINING:
			case SND_PCM_STATE_PAUSED:
			case SND_PCM_STATE_SUSPENDED:
			case SND_PCM_STATE_XRUN:
			default:
				bDoDrop = TRUE;
				break;
		} //switch(sndStatus)

		if(bDoDrop)
		{
			iErr = snd_pcm_drop(arSrcStateData.pAlsaPCM);
			if(iErr < 0)
			{
				u32RetVal = OSAL_E_UNKNOWN;
				vTraceAcousticSrcError(TR_LEVEL_ERRORS, EN_OUTPUTDEVICE_ABORT,
									  u32RetVal, "pcmdrop2",
									  u32RetVal, (tU32)iErr, (tU32)sndStatus, 0);
				ACOUSTICSRC_PRINTF_ERRORS("snd_pcm_drop returns: <%s>",
										 snd_strerror(iErr));
				ACOUSTICSRC_vDumpAlsaStatus();
			}
			else
			{
				u32RetVal = OSAL_E_NOERROR;
			}
		}
		else
		{
			u32RetVal = OSAL_E_NOERROR;
		}
	}
	else 
	{
		u32RetVal = OSAL_E_NOFILEDESCRIPTOR;
	} 
	vTraceAcousticSrcInfo(TR_LEVEL_USER_2, EN_OUTPUTDEVICE_ABORT,
		"exit", 0, u32RetVal, (tU32)iErr, 0);
		
	return u32RetVal;
}

/*************************************************************************/ /**
*  FUNCTION:      u32OutputDataConvert
*
*  @brief         Performs SRC and writes audio data to the output file in raw format
*
*  @param         s32ID              stream ID
*                 pvBuffer           buffer with data to write
*                 u32BufferSize      buffer size in bytes
*
*  @return        OSAL error code
*
*  HISTORY:
*
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
*****************************************************************************/
static tU32 u32OutputDataConvert(const void* pvBuffer,
								 tU32 u32BufferSize,
								 tPU32 pu32BytesConverted,
                                 tS32  s32TimeOut)
{
	tU32 u32RetVal = OSAL_E_NOERROR;
	int iSamplesWritten;
	int iSamples;
	int iRet;
	static int iCount = 0;

	*pu32BytesConverted = 0;

	if(arSrcStateData.pAlsaPCM != NULL)
	{
		vTraceAcousticSrcInfo(TR_LEVEL_USER_3, EN_OUTPUTDATA_CONVERT,
				 "enter", 0, 0, 0, 0);
		
		iSamples = (int)(u32BufferSize / 
		((tU32)ACOUSTICSRC_osalGetBytesPerSample(arSrcStateData.rPCMConfig.enSampleFormat)
		* (tU32)arSrcStateData.rPCMConfig.u16NumChannels));

		ACOUSTICSRC_PRINTF_U4("u32OutputDataConvert count %07d, "
				 "fmt %d, SR %d, Ch %d, BufSize %d, iSamples %d",
			iCount++, (int)arSrcStateData.rPCMConfig.enSampleFormat,
			(int)arSrcStateData.rPCMConfig.nSampleRate,
			(int)arSrcStateData.rPCMConfig.u16NumChannels,
		    (int)u32BufferSize, iSamples );

		iRet = snd_pcm_wait(arSrcStateData.pAlsaPCM, (int)s32TimeOut);
		
		if(iRet == 0)
		{ //timeout
			u32RetVal = OSAL_E_TIMEOUT;
			ACOUSTICSRC_PRINTF_ERRORS("u32OutputDataConvert"
				" ERROR snd_pcm_wait TIMEOUT after %d ms", (int)s32TimeOut);
			//DEBUG
			ACOUSTICSRC_vDumpAlsaStatus();
		}
		else if(iRet < 0)
		{ //error
			u32RetVal = OSAL_E_UNKNOWN;
			ACOUSTICSRC_PRINTF_ERRORS("u32OutputDataConvert"
				" ERROR snd_pcm_wait return %d, timeout %d ms", iRet, (int)s32TimeOut);
			//DEBUG
			ACOUSTICSRC_vDumpAlsaStatus();
		}
		else 
		{ //ready for I/O
			iSamplesWritten = snd_pcm_writei(arSrcStateData.pAlsaPCM,
							pvBuffer, (snd_pcm_uframes_t)iSamples);
			if(iSamplesWritten < 0)
			{
				int err;
				ACOUSTICSRC_PRINTF_ERRORS("u32OutputDataConvert"
									 " ERROR snd_pcm_writei <%d>==<%s>",
						iSamplesWritten, snd_strerror(iSamplesWritten));
				//DEBUG
				ACOUSTICSRC_vDumpAlsaStatus();

				err = snd_pcm_prepare(arSrcStateData.pAlsaPCM);
				if(err >= 0)
				{
					iSamplesWritten = snd_pcm_writei(arSrcStateData.pAlsaPCM,
								pvBuffer, (snd_pcm_uframes_t)iSamples);
				}
				else
				{
					u32RetVal = OSAL_E_UNKNOWN;
					ACOUSTICSRC_PRINTF_ERRORS("u32OutputDataConvert"
											 " ERROR snd_pcm_writei <%d>==<%s>",
								 iSamplesWritten, snd_strerror(iSamplesWritten));
					//DEBUG
					ACOUSTICSRC_vDumpAlsaStatus();
				}
			}

			if(iSamplesWritten >= 0)
			{
				if(iSamplesWritten != iSamples)
				{
					u32RetVal = OSAL_E_IOERROR;
					vTraceAcousticSrcError(TR_LEVEL_ERRORS,  EN_OUTPUTDATA_CONVERT,
						u32RetVal, "pcmwrite", u32RetVal, (tU32)iSamplesWritten,
						(tU32)iSamples, 0);
					//DEBUG
					ACOUSTICSRC_vDumpAlsaStatus();
				} 
				*pu32BytesConverted = (tU32)iSamplesWritten *
				   (tU32)ACOUSTICSRC_osalGetBytesPerSample(arSrcStateData.rPCMConfig.enSampleFormat)
				   * (tU32)arSrcStateData.rPCMConfig.u16NumChannels;
			} 
		} 
	}
	else 
	{
		u32RetVal = OSAL_E_NOFILEDESCRIPTOR;
	} 
	return u32RetVal;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsCodecValid
*
*  @brief         Checks whether provided codec value is valid
*
*  @param         enCodecs  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
*  - 07.09.05 Schedel Robert, 3SOFT
*    Initial revision.
*****************************************************************************/
static tBool bIsCodecValid(OSAL_tenAcousticCodec enCodecs)
{
  (void)enCodecs;
  return TRUE;
}

#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file 
|-----------------------------------------------------------------------*/

