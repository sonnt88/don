/************************************************************************
| FILE:         acousticin.c
| PROJECT:      Gen3
| SW-COMPONENT: AcousticIn driver
|------------------------------------------------------------------------*/
/* ******************************************************FileHeaderBegin** *//**
* @file    acousticin.c
* @brief   This device is a generic audio in device. Its main
*          functionality is record of PCM audio streams.
*
* @author  srt2hi BSOT. Copy of Gen2 acousticIn 
*                        with even more reduction in complexity
*
*//* ***************************************************FileHeaderEnd******* */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"               /**< OSAL types etc. */
#include "osalio_public.h"
#include <alsa/asoundlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "acousticin_public.h"     /**< public interface for this product */
#include "acoustic_trace.h"        /**< trace stuff for this product */

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| types definition (scope: local)
|-----------------------------------------------------------------------*/


/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static tU32  u32AbortStream(tS32 s32ID);
static tU32  u32DoReadOperation(tS32 s32ID, tU32 u32FD,
                                OSAL_trAcousticInRead* prReadInfo,
                                tPU32 pu32BytesRead);
static tU32  u32RcvFromAlsa(tS32 s32ID, snd_pcm_uframes_t *pFrameBuffer,
                            int iBufSizeBytes,
                            tPU32 pu32BytesRead); 
static tU32  u32InitAlsa(tS32 s32ID);
static tU32  u32UnInitAlsa(tS32 s32ID);
static tBool bIsSampleformatValid(OSAL_tenAcousticSampleFormat enSampleformat);
static tBool bIsSamplerateValid(OSAL_tAcousticSampleRate nSamplerate);
static tBool bIsChannelnumValid(tU16 u16Channelnum);
static tBool bIsBuffersizeValid(tU32 u32Buffersize);
static int iHandleXRUN(tS32 s32ID, int iRet, tBool bTraceActive);

static tU32 IOCtrl_Version(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_Extread(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static void IOCtrl_RegNotification(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_WaitEvent(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSuppSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSuppSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSuppChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSuppBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_Start(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_Stop(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_Abort(tS32 s32ID, tU32 u32FD, tS32 s32Arg);

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define NUM_ARRAY_ELEMS(array)  (sizeof(array)/sizeof(array[0]))

/** version for returning with IOControl OSAL_C_S32_IOCTRL_VERSION */
#define ACOUSTICIN_C_S32_IO_VERSION                (tS32)(0x00010000)
#define ACOUSTICIN_ALSA_DEVICE_NAME_BUFFER_SIZE    64

/** play states */
typedef enum
{
  ACOUSTICIN_EN_STATE_UNINITIALIZED = 0,  /**< driver uninitialized (==0!) */
  ACOUSTICIN_EN_STATE_INITIALIZED   = 1,  /**< driver initialized */
  ACOUSTICIN_EN_STATE_STOPPED       = 2,  /**< device opened but inactive */
  ACOUSTICIN_EN_STATE_ACTIVE        = 4  /**< audio stream active */
} ACOUSTICIN_enPlayStates;

/** file handles for the streams */
enum  ACOUSTICIN_enFileHandles
{
  ACOUSTICIN_EN_HANDLE_ID_NOTDEFINED = -1,
  ACOUSTICIN_EN_HANDLE_ID_SPEECHRECO = 1 // file handle for speechreco stream
};


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/
/** mechanisms for DSP communication */
typedef struct
{
  ACOUSTICIN_enPlayStates       enPlayState;   /**< current play state */
  OSAL_trAcousticInCallbackReg  rCallback;     /**< callback data */
} trACOUSTICIN_DrvInterface_type;


// PCM config data 
typedef struct
{
  OSAL_tAcousticSampleRate     nSampleRate;    //< current sample rate
  OSAL_tenAcousticSampleFormat enSampleFormat; //< current sample format
  tU16                         u16NumChannels; //< current number of channels
} trPCMConfigData;


// Internal status + configuration data 
typedef struct
{
  tBool                          bAlsaIsOpen;
  /**< Alsa open flag */
  snd_pcm_t                      *pAlsaPCM;               
  /**< pointer to handle for PCM Alsa device */
  char                szAlsaDeviceName[ACOUSTICIN_ALSA_DEVICE_NAME_BUFFER_SIZE];
  /**< device name as configured in .asoundrc */
  OSAL_tAcousticBuffersize       anBufferSize[OSAL_EN_ACOUSTIC_CODECLAST];
  /**< buffer size for each codec (PCM) */
  trACOUSTICIN_DrvInterface_type rDRVIf;
  /**< channels & config for DSP communication */
  trPCMConfigData                rPCMConfig;
  /**< PCM config data */
  enum ACOUSTICIN_enFileHandles  enFileHandle;
  /**< file handle */
  int                            iDebugPCMFileHandle;
  /**< file handle for writing PCM data to file*/
} trACOUSTICIN_StateData;


/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

static tBool ACOUSTICIN_garbInit[EN_ACOUSTICIN_DEVID_LAST] = {0};

/** internal state variables, for each stream */
static trACOUSTICIN_StateData garD[EN_ACOUSTICIN_DEVID_LAST] = {0};


/** List of PCM sample formats supported by the device */
static const OSAL_tenAcousticSampleFormat aenSupportedSampleformats[] =
{
  ACOUSTICIN_C_EN_DEFAULT_PCM_SAMPLEFORMAT,
  OSAL_EN_ACOUSTIC_SF_S16LE      /*!< signed 16 bit, little endian */ 
};

/** List of PCM sample rates supported by the device (interval begin value) */
static const OSAL_tAcousticSampleRate anSupportedSampleratesFrom[] =
{
  ACOUSTICIN_C_U32_DEFAULT_PCM_SAMPLERATE
};

/** List of PCM sample rates supported by the device (interval end value) */
static const OSAL_tAcousticSampleRate anSupportedSampleratesTo[] =
{
  48000
};

/** List of PCM channel numbers supported by the device */
static tCU16 au16SupportedChannelnums[] =
{
  ACOUSTICIN_C_U8_DEFAULT_NUM_CHANNELS_SPEECHRECO,
  2
};

/** List of buffer sizes supported by the device */
static tCU32 au32SupportedBuffersizesPCM[] =
{
  1024,
  ACOUSTICIN_C_U32_DEFAULT_BUFFER_SIZE_PCM,
  4096,
  8192,
  16384
};


/********************************************************************/ /**
*  FUNCTION:      vDebugSearchFuser
*  @brief         prints out prozesses which are hold open the capture devices
************************************************************************/
static void vDebugSearchFuser()
{
  FILE *fp;
  char szTxt[256], szDev[24];
  char szCommand[96];
  int iCard, iDevice;
  struct stat s;

  for(iCard = 0; iCard <= 5; iCard++)
    for(iDevice = 0; iDevice <= 5; iDevice++)
    {
      
      sprintf(szDev, "/dev/snd/pcmC%dD%dc", iCard, iDevice);
      if(stat(szDev, &s) == 0)
      {
        sprintf(szCommand, "ps ax | grep `fuser %s | awk {\"print $1\"}`",
                           szDev);

        fp = popen(szCommand, "r");
        if(fp != NULL)
        {
          memset(szTxt, 0, sizeof(szTxt));
          while (fgets(szTxt, sizeof(szTxt)-1, fp) != NULL)
          {
            ACOUSTICIN_PRINTF_ERRORS("Device [%s]", szDev);
            ACOUSTICIN_PRINTF_ERRORS(" opened by [%s]", szTxt);
          } //while (fgets(szTxt, sizeof(szTxt)-1, fp) != NULL)
          pclose(fp);
        } //if(fp != NULL)
      } //if(stat(szDev, &s) == 0)
    } //for(iDevice = 0; iDevice <= 5; iDevice++)
}

/********************************************************************/ /**
*  FUNCTION:      vDebugFileClose
*  @brief         close a file for wrting captured PCM data
************************************************************************/
static void vDebugFileClose(tS32 s32ID)
{
  if(s32ID >= EN_ACOUSTICIN_DEVID_LAST || s32ID < 0)
    return;
  
  if(garD[s32ID].iDebugPCMFileHandle == -1)
  {
    (void)close(garD[s32ID].iDebugPCMFileHandle);
    garD[s32ID].iDebugPCMFileHandle = -1;
  }
}

/********************************************************************/ /**
*  FUNCTION:      vDebugFileOpen
*  @brief         opens a file for wrting captured PCM data
************************************************************************/
static void vDebugFileOpen(tS32 s32ID)
{
  static int iCnt = 0;
  char szFile[32];

  if(!bIsAcousticInTraceActive(TR_LEVEL_USER_4))
    return;

  if(s32ID >= EN_ACOUSTICIN_DEVID_LAST || s32ID < 0)
    return;

  vDebugFileClose(s32ID);
  sprintf(szFile, "/tmp/acin%04d.pcm", iCnt++);
  garD[s32ID].iDebugPCMFileHandle = open(szFile, O_APPEND | O_RDWR | O_CREAT);
}

/********************************************************************/ /**
*  FUNCTION:      vDebugFileWrite
*  @brief         write PCM data to file
************************************************************************/
static void vDebugFileWrite(tS32 s32ID, const void *pData, size_t nLen)
{
  if(s32ID >= EN_ACOUSTICIN_DEVID_LAST || s32ID < 0)
    return;

  if(garD[s32ID].iDebugPCMFileHandle == -1)
    return;
  write(garD[s32ID].iDebugPCMFileHandle, pData, nLen);
}

/********************************************************************/ /**
*  FUNCTION:      iSlash2CamelCase
*
*  @brief         removes slashes from input string
*                 Add a first Letter 'A'
*                 changes Next letter after slash to capital letter
*                 Example: /dev/acoustic/speech -> AdevSpeechAcousticSpeech
*                 
*
*  @return        returns number of characters copied into Outputbuffer
*                 (without trailing '\0')
*  @retval        int
*
*  HISTORY:
************************************************************************/
static int iSlash2CamelCase(const char *pcszSlashes,
                            char *pszOutputBuffer,
                            int iOutputBufferSize)
{
  int   iIn = 0;
  int   iOut = 0;
  tBool bMakeCapital  = FALSE;
  tBool bIsFirstSlash = TRUE; //first character after '/' leave lower case
  char  c;

  pszOutputBuffer[iOut++] = 'A';
  while('\0' != (c = pcszSlashes[iIn++])
        &&
        ((iOut+1) < iOutputBufferSize))
  {
    if('/' == c)
    {
      //letter after first slash leave in lower case
      bMakeCapital = bIsFirstSlash ? FALSE : TRUE;
      bIsFirstSlash = FALSE;
    }
    else//if('/' == c)
    {
      pszOutputBuffer[iOut++] =
      (char)(bMakeCapital ? toupper((int)c) : c);
      bMakeCapital = FALSE;
    } //else//if('/' == c)
  } //while('\0' != (c = pcszSlashes[iIn++]) && !bOverflow)

  pszOutputBuffer[iOut] = '\0'; //trailing zero
  return iOut;
}


/********************************************************************/ /**
*  FUNCTION:      iConvertOsalDeviceID
*  @brief         converts OSAL to local IDs
*  @return        int local ID
*  @retval        int
************************************************************************/
static tS32 s32ConvertOsalDeviceID(tS32 s32OsalDeviceID)
{
  (void)s32OsalDeviceID;
  return (tS32)EN_ACOUSTICIN_DEVID_SPEECHRECO;
}



/************************************************************************
|function prototype (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
/********************************************************************/ /**
*  FUNCTION:       alsaConvertFormatOsalToAlsa
*  @brief         Convert Format Osal To Alsa
*  @return        Alsa format ID
*  @retval        
*
*  HISTORY:
************************************************************************/
static snd_pcm_format_t alsaConvertFormatOsalToAlsa(
                        OSAL_tenAcousticSampleFormat  nOsalSampleFormat)
{
  snd_pcm_format_t nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;

  switch(nOsalSampleFormat)
  {
  case OSAL_EN_ACOUSTIC_SF_S8:    /*!< signed 8 bit */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S8;
    break;
  case OSAL_EN_ACOUSTIC_SF_S16:   /*!< signed 16 bit: CPU endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S16;
    break;
  case OSAL_EN_ACOUSTIC_SF_S32:   /*!< signed 32 bit: CPU endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S32;
    break;
  case OSAL_EN_ACOUSTIC_SF_F32:   /*!< float (IEEE 754) 32 bit: CPU endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT;
    break;
  case OSAL_EN_ACOUSTIC_SF_S16LE: /*!< signed 16 bit: little endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;
    break;
  case OSAL_EN_ACOUSTIC_SF_S32LE: /*!< signed 32 bit: little endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S32_LE;
    break;
  case OSAL_EN_ACOUSTIC_SF_F32LE: /*!< float(IEEE 754) 32bit: little endian*/ 
    nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT_LE;
    break;
  case OSAL_EN_ACOUSTIC_SF_S16BE:  /*!< signed 16 bit: big endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S16_BE;
    break;
  case OSAL_EN_ACOUSTIC_SF_S32BE: /*!< signed 32 bit, big endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S32_BE;
    break;
  case OSAL_EN_ACOUSTIC_SF_F32BE:  /*!< float (IEEE 754) 32 bit, big endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT_BE;
    break;
  default:
    ACOUSTICIN_PRINTF_ERRORS("alsaConvertFormatOsalToAlsa ERROR:"
                             " cannot convert OSAL-format (%d)",
                             (int)nOsalSampleFormat);
    nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;
  } //switch(nOsalSampleFormat)

  ACOUSTICIN_PRINTF_U3("alsaConvertFormatOsalToAlsa :"
                       " convert OSAL %d to Alsa-format %d",
                       (int)nOsalSampleFormat, (int)nAlsaSampleFormat);
  return nAlsaSampleFormat;
}


/********************************************************************/ /**
*  FUNCTION:      u32InitPrivate
*
*  @brief         Initializes any necessary resources.
*                 Should be called at OSAL startup.
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR   on success
*
*  HISTORY:
*
*    Initial revision.
************************************************************************/
static tU32 u32InitPrivate(tS32 s32ID)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  ACOUSTICIN_PRINTF_U3("%s - START", __FUNCTION__);


  if(!ACOUSTICIN_garbInit[s32ID])
  {
    ACOUSTICIN_garbInit[s32ID] = TRUE;

    vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_ACIN_INIT,
                         "init", (tU32)0, (tU32)0, 0, __LINE__);


    garD[s32ID].enFileHandle=ACOUSTICIN_EN_HANDLE_ID_NOTDEFINED;
    switch(s32ID)
    {
    case EN_ACOUSTICIN_DEVID_SPEECHRECO:
      garD[s32ID].enFileHandle = ACOUSTICIN_EN_HANDLE_ID_SPEECHRECO;
      break;

    default:
      break;
    }

    if(garD[s32ID].enFileHandle!=ACOUSTICIN_EN_HANDLE_ID_NOTDEFINED)
    {
      /* we are now in state "Initialized" */
      garD[s32ID].rDRVIf.enPlayState = ACOUSTICIN_EN_STATE_INITIALIZED;
    }
  } //if(!ACOUSTICIN_garbInit[s32ID])

  ACOUSTICIN_PRINTF_U3("%s - END u32ErrorCode0x %08X", __FUNCTION__,
                       (unsigned int)u32ErrorCode);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      ACOUSTICIN_u32DeinitPrivate
*
*  @brief         Releases any resources acquired during init
*                 Before this function is called (e.g. by Powermanagement),
*                 the device must have been closed. This cannot be done
*                 here, because then the device would still be marked as
*                 used in the dispatcher.
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR   on success
*
*  HISTORY:
*
*    Initial revision.
************************************************************************/
static tU32 ACOUSTICIN_u32DeinitPrivate(tS32 s32ID)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  if(ACOUSTICIN_garbInit[s32ID])
  {
    ACOUSTICIN_garbInit[s32ID] = FALSE;
    garD[s32ID].rDRVIf.enPlayState = ACOUSTICIN_EN_STATE_UNINITIALIZED;
  } //if(ACOUSTICIN_garbInit[s32ID])

  return u32ErrorCode;
}




/********************************************************************/ /**
*  FUNCTION:      ACOUSTICIN_u32IOOpen
*
*  @brief         Opens the acousticin device "/dev/acousticin/xxx" 
*                 where xxx is the stream type to be opened.
*
*  @param         s32ID
*                   ID of the stream to open
*  @param         szName
*                   Should be OSAL_C_STRING_DEVICE_ACOUSTICIN+"/xxx"
*  @param         enAccess
*                   File access mode (should always be OSAL_EN_WRITEONLY 
*                   for this device although value is ignored for now)
*  @param         pu32FD
*                   for storing the filehandle      
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_NOTINITIALIZED
*  @retval        OSAL_E_ALREADYOPENED
*  @retval        OSAL_E_UNKNOWN
*
*  HISTORY:
*

*    Initial revision.
************************************************************************/
tU32 ACOUSTICIN_u32IOOpen(tS32 s32ID,
                          tCString szName,
                          OSAL_tenAccess enAccess,
                          tPU32 pu32FD,
                          tU16 appid)
{
  int iLen;
  const char *pcszShortDeviceName;
  const char *pcszLongDeviceName;
  char szFullOsalDeviceName[ACOUSTICIN_ALSA_DEVICE_NAME_BUFFER_SIZE];
  size_t nShortLen;
  size_t nLongLen;
  size_t nFullLen;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_ACIN_OPEN,
                       "enter", (tU32)s32ID,
                       (tU32)enAccess,
                       pu32FD != NULL ? (tU32)(*pu32FD) : (tU32)-1,
                       (tU32)appid);


  pcszShortDeviceName = NULL == szName ? "" : szName;
  nShortLen = strlen(pcszShortDeviceName);


  // get osal base device name, append "/szName"
  switch(s32ID)
  {
  case OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH:
    pcszLongDeviceName = OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO;
    break;
  default:  //fall back: s32ID is used as Index!
    s32ID = (tS32)OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH;
    pcszLongDeviceName = OSAL_C_STRING_DEVICE_ACOUSTICIN_IF_SPEECHRECO;
    break;
  } //switch(s32ID)

  nLongLen  = strlen(pcszLongDeviceName); 
  nFullLen  = nLongLen + nShortLen + 2;  //+2: trailing zero and a '/'
  if(nFullLen <= ACOUSTICIN_ALSA_DEVICE_NAME_BUFFER_SIZE)
  {
    strcpy(szFullOsalDeviceName,
           pcszLongDeviceName);
    strcat(szFullOsalDeviceName,"/");
    strcat(szFullOsalDeviceName,pcszShortDeviceName);
  }
  else //if(nFullLen <= ACOUSTICIN_ALSA_DEVICE_NAME_BUFFER_SIZE)
  {
    //error buffer to short
    vTraceAcousticInError(TR_LEVEL_ERRORS, EN_ACIN_OPEN, OSAL_E_UNKNOWN,
                          "2short", 
                          (tU32)ACOUSTICIN_ALSA_DEVICE_NAME_BUFFER_SIZE,
                          (tU32)nFullLen,
                          (tU32)nLongLen,
                          (tU32)nShortLen);

    return OSAL_E_UNKNOWN;
  } //else //if(nFullLen <= ACOUSTICIN_ALSA_DEVICE_NAME_BUFFER_SIZE)

  s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

  ACOUSTICIN_PRINTF_U4("ACOUSTICIN_u32IOOpen FullOsalDeviceName <%s>\n",
                       szFullOsalDeviceName);

  u32InitPrivate(s32ID); //ash

  memset(garD[s32ID].szAlsaDeviceName,
         0,
         ACOUSTICIN_ALSA_DEVICE_NAME_BUFFER_SIZE);
  iLen = iSlash2CamelCase(szFullOsalDeviceName,
                          garD[s32ID].szAlsaDeviceName,
                          ACOUSTICIN_ALSA_DEVICE_NAME_BUFFER_SIZE);
  ACOUSTICIN_PRINTF_U4("ACOUSTICIN_u32IOOpen CamelCase <%s>, Len %d\n",
                       garD[s32ID].szAlsaDeviceName, iLen);
  if(iLen <= 0)
  {
    vTraceAcousticInError(TR_LEVEL_ERRORS, EN_ACIN_OPEN, OSAL_E_UNKNOWN,
                          "noname", 
                          (tU32)iLen,
                          (tU32)0,
                          (tU32)0,
                          (tU32)0);

    return OSAL_E_UNKNOWN;
  } //if(iLen <= 0)


  if(s32ID<0 || s32ID >= EN_ACOUSTICIN_DEVID_LAST)
  {
    return OSAL_E_INVALIDVALUE;
  }
  // If the driver is not yet initialized, the open request fails
  if(garD[s32ID].rDRVIf.enPlayState < ACOUSTICIN_EN_STATE_INITIALIZED)
  {
    vTraceAcousticInError(TR_LEVEL_ERRORS,
                          EN_ACIN_OPEN, OSAL_E_NOTINITIALIZED,
                          "noinit",
                          (tU32)garD[s32ID].rDRVIf.enPlayState,
                          0, 0, 0);
    return OSAL_E_NOTINITIALIZED;
  }

  // If the stream is already opened, the open request fails 
  if(garD[s32ID].rDRVIf.enPlayState > ACOUSTICIN_EN_STATE_INITIALIZED)
  {
    vTraceAcousticInError(TR_LEVEL_ERRORS,
                          EN_ACIN_OPEN,
                          OSAL_E_ALREADYOPENED,
                          "alropen",
                          (tU32)garD[s32ID].rDRVIf.enPlayState,
                          0, 0, 0);
    return OSAL_E_ALREADYOPENED;
  }

  /* Initialize internal state variables to defaults */
  garD[s32ID].rPCMConfig.enSampleFormat =
                                       ACOUSTICIN_C_EN_DEFAULT_PCM_SAMPLEFORMAT;
  garD[s32ID].rPCMConfig.nSampleRate    = 
                                        ACOUSTICIN_C_U32_DEFAULT_PCM_SAMPLERATE;
  garD[s32ID].rPCMConfig.u16NumChannels =
                                ACOUSTICIN_C_U8_DEFAULT_NUM_CHANNELS_SPEECHRECO;
  garD[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM] =
                                       ACOUSTICIN_C_U32_DEFAULT_BUFFER_SIZE_PCM;
  garD[s32ID].pAlsaPCM            = NULL;
  garD[s32ID].bAlsaIsOpen         = FALSE;
  garD[s32ID].iDebugPCMFileHandle = -1;

  /* new state is "Stopped" */
  garD[s32ID].rDRVIf.enPlayState = ACOUSTICIN_EN_STATE_STOPPED;  

  /* return filehandle */
  if(pu32FD != NULL)
  {
    *pu32FD = (tU32)garD[s32ID].enFileHandle; /*lint !e571 */  
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_ACIN_OPEN,
                       "exitok",   (tU32)s32ID, (tU32)enAccess, 0, 0);

  return OSAL_E_NOERROR;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICIN_u32IOClose
*
*  @brief         Closes the acousticin device.
*                 When the device is reopened afterwards, all configured 
*                 parameters, registered callbacks will start with the 
*                 internal defaults again.   
*
*  @param         s32ID  stream ID
*  @param         u32FD  file handle
*
*  @return        OSAL error code
*
*  HISTORY:
*
*    Initial revision.
************************************************************************/
tU32 ACOUSTICIN_u32IOClose(tS32 s32ID, tU32 u32FD)
{
  tU32 u32RetVal = OSAL_E_NOERROR;
  s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_ACIN_CLOSE,
                       "enter", (tU32)s32ID, (tU32)u32FD, 0, 0);

  if(s32ID<0 || s32ID >= EN_ACOUSTICIN_DEVID_LAST)
  { //error
    u32RetVal = OSAL_E_INVALIDVALUE;
    vTraceAcousticInError(TR_LEVEL_ERRORS,
                          EN_ACIN_CLOSE,
                          OSAL_E_INVALIDVALUE,
                          "invval",
                          (tU32)s32ID, u32FD, u32RetVal, 0);
  }
  else //if(s32ID<0 || s32ID >= EN_ACOUSTICIN_DEVID_LAST)
  {
    //OK
    // If the device is not open, the close request will fail 
    if(garD[s32ID].rDRVIf.enPlayState < ACOUSTICIN_EN_STATE_STOPPED)
    { //error
      u32RetVal = OSAL_E_DOESNOTEXIST;
      vTraceAcousticInError(TR_LEVEL_ERRORS,
                            EN_ACIN_CLOSE,
                            OSAL_E_DOESNOTEXIST,
                            "noexist",
                            (tU32)s32ID, u32FD, u32RetVal, 0);
    }
    else //if(garD....enPlayState < ACOUSTICIN_EN_STATE_STOPPED)
    {
      //ok
      /* threads blocked in write will
         be cancelled in the abort call below */
      /* flush buffers and clean up */
      (tVoid) u32AbortStream(s32ID);

      /*close alsa device, only if opened*/
      (tVoid) u32UnInitAlsa(s32ID);

      /* new state is "Uninitialized" */
      garD[s32ID].rDRVIf.enPlayState =
      ACOUSTICIN_EN_STATE_UNINITIALIZED; 

      vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_ACIN_CLOSE,
                           "exitok",
                           (tU32)s32ID,
                           (tU32)u32FD,
                           u32RetVal, 0);
    } //else //if(garD...enPlayState < ACOUSTICIN_EN_STATE_STOPPED)
    ACOUSTICIN_u32DeinitPrivate(s32ID);
  } //else //if(s32ID<0 || s32ID >= EN_ACOUSTICIN_DEVID_LAST)

  vDebugFileClose(s32ID);
  return u32RetVal;
}


/********************************************************************/ /**
*  FUNCTION:      ACOUSTICIN_u32IOControl
*
*  @brief         Executes IO-Control for the driver
*
*  @param[in]     s32ID
*                   stream ID
*  @param[in]     u32FD
*                   file handle
*  @param[in]     s32Fun
*                   IO-Control to be executed
*  @param[in,out] s32Arg
*                   extra argument (depending on s32Fun)
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_INVALIDVALUE
*  @retval        OSAL_E_NOTSUPPORTED 
*
*  HISTORY:
*
*  1. Initial revision.
*  2. Modified to call each of the functionalities through a sub function
*  rather than directly
*     This has been done to reduce the cyclomatic code complexity
************************************************************************/
tU32 ACOUSTICIN_u32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_ACIN_IOCTRL,
                       "enter",
                       (tU32)s32ID, u32FD, (tU32)s32Fun, (tU32)s32Arg);

  switch(s32Fun)
  {
  /*---------------------------------------------------------------------*/
  /* The driver version is returned                                      */
  /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_VERSION:
    {
      u32ErrorCode = IOCtrl_Version(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Reads audio data from the driver by providing a pointer to the data */
    /* (including additional control information) to the driver.           */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_EXTREAD:
    {
      u32ErrorCode = IOCtrl_Extread(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* This IOControl registers a callback function that is used for       */
    /* notification about certain events.                                  */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_REG_NOTIFICATION:
    {
      IOCtrl_RegNotification(s32ID, u32FD, s32Arg);
      break;      
    }

    /*---------------------------------------------------------------------*/
    /* Blocks until new event occurs (or until timeout happens),           */
    /* this is an alternative to registering a notification callback.      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_WAITEVENT:
    {
      u32ErrorCode = IOCtrl_WaitEvent(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Retrieves the sample rate cap. for a specified acoustic codec.      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_GETSUPP_SAMPLERATE:
    {
      u32ErrorCode = IOCtrl_GetSuppSamplerate(s32ID, u32FD, s32Arg);
      break;              
    }

    /*---------------------------------------------------------------------*/
    /* Gets the sample rate for a specified acoustic codec.                */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_GETSAMPLERATE:
    {
      u32ErrorCode = IOCtrl_GetSamplerate(s32ID, u32FD, s32Arg);
      break;              
    }

    /*---------------------------------------------------------------------*/
    /* Sets the sample rate for a specified acoustic codec.                */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLERATE:
    {
      u32ErrorCode = IOCtrl_SetSamplerate(s32ID, u32FD, s32Arg);
      break;             
    }

    /*---------------------------------------------------------------------*/
    /* Retrieves the sample format cap. for a specified acoustic codec.    */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_GETSUPP_SAMPLEFORMAT:
    {
      u32ErrorCode = IOCtrl_GetSuppSampleformat(s32ID, u32FD, s32Arg);
      break;              
    }

    /*---------------------------------------------------------------------*/
    /* Gets the format of the samples for a specified acoustic codec.      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_GETSAMPLEFORMAT:
    {
      u32ErrorCode = IOCtrl_GetSampleformat(s32ID, u32FD, s32Arg);
      break;                 
    }

    /*---------------------------------------------------------------------*/
    /* Sets the format of the samples for a specified acoustic codec.      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_SETSAMPLEFORMAT:
    {
      u32ErrorCode = IOCtrl_SetSampleformat(s32ID, u32FD, s32Arg);
      break;              
    }

    /*---------------------------------------------------------------------*/
    /* Retrieves the channel num cap.                                      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_GETSUPP_CHANNELS:
    {
      u32ErrorCode = IOCtrl_GetSuppChannels(s32ID, u32FD, s32Arg);
      break;             
    }

    /*---------------------------------------------------------------------*/
    /* Gets the number of channels to be used for the audio stream.        */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_GETCHANNELS:
    {
      u32ErrorCode = IOCtrl_GetChannels(s32ID, u32FD, s32Arg);
      break;             
    }

    /*---------------------------------------------------------------------*/
    /* Sets the number of channels to be used for the audio stream.        */
    /*---------------------------------------------------------------------*/     
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_SETCHANNELS:
    {     
      u32ErrorCode = IOCtrl_SetChannels(s32ID, u32FD, s32Arg);
      break;          
    }

    /*---------------------------------------------------------------------*/
    /* Retrieves the buffer size capabilities                              */
    /*---------------------------------------------------------------------*/      
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_GETSUPP_BUFFERSIZE:
    {
      u32ErrorCode = IOCtrl_GetSuppBuffersize(s32ID, u32FD, s32Arg);
      break;              
    }

    /*---------------------------------------------------------------------*/
    /* Gets the size of buffers to be used for the audio stream.           */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_GETBUFFERSIZE:   
    {     
      u32ErrorCode = IOCtrl_GetBuffersize(s32ID, u32FD, s32Arg);
      break;               
    }

    /*---------------------------------------------------------------------*/
    /* Sets the size of buffers to be used for the audio stream.           */
    /*---------------------------------------------------------------------*/     
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_SETBUFFERSIZE:
    {     
      u32ErrorCode = IOCtrl_SetBuffersize(s32ID, u32FD, s32Arg);
      break;               
    }

    /*---------------------------------------------------------------------*/
    /* Starts the audio transfer. After starting the stream, the           */
    /* client can read audio data by calling the OSAL read                 */
    /* operation  continuously.                                            */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_START:
    {
      u32ErrorCode = IOCtrl_Start(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Initiates stopping of a running audio transfer.                     */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_STOP:
    {
      u32ErrorCode = IOCtrl_Stop(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Aborts (=flushs) running audio input immediately.                   */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_ABORT:
    {
      u32ErrorCode = IOCtrl_Abort(s32ID, u32FD, s32Arg);
      break;
    }

  case OSAL_C_S32_IOCTRL_ACOUSTICIN_PAUSE:
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_SET_FILTER_COEF:
  case OSAL_C_S32_IOCTRL_ACOUSTICIN_SETERRTHR:
  default:
    {
      ACOUSTICIN_PRINTF_FATAL("%s: IO unsupported: %d",
                               __FUNCTION__, (int)s32Fun);
      u32ErrorCode = OSAL_E_NOTSUPPORTED;
      break;               
    }
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_ACIN_IOCTRL,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Fun, u32ErrorCode);

  return u32ErrorCode;
}

/*************************************************************************/ /**
*  FUNCTION:      ACOUSTICIN_u32IORead
*
*  @brief         Read audio data from the driver by providing a pointer 
*                 to a buffer. This function uses a posix style read, 
*                 therefore it acts as a wrapper to another function,
*                 "u32DoReadOperation", which supports additional information.
*                 The additional information is not used here and filled
*                 with defaults
*
*  @param[in]     s32ID
*                   stream ID
*  @param[in]     u32FD
*                   file handle
*  @param[out]     ps8Buffer
*                   Pointer to the buffer where new audio data shall be 
*                   stored by the driver
*  @param[in]     u32Size
*                   Denotes the size of the buffer content (in bytes) for 
*                   the buffer pointed to by pcs8Buffer.
*  @param[out]    pu32Read
*                   Number of bytes to read from the driver
*
*  @return        OSAL error code     
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
tU32 ACOUSTICIN_u32IORead(tS32 s32ID,
                          tU32 u32FD,
                          tPS8 ps8Buffer,
                          tU32 u32Size,
                          tPU32 pu32Read)
{
  OSAL_trAcousticInRead rReadInfo;
  tU32 u32Ret;
  tU32 u32BytesRead = 0;

  s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

  vTraceAcousticInInfo(TR_LEVEL_USER_3, EN_ACIN_READ,
                       "enter", (tU32)s32ID, (tU32)u32FD, u32Size, __LINE__);

  if(!ps8Buffer || !pu32Read)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s: NULL Pointer ps8Buffer %p pu32Read %p",
                             __FUNCTION__,
                             ps8Buffer,
                             pu32Read);
    return OSAL_E_INVALIDVALUE;
  } //if(!ps8Buffer || !pu32Read)
  
  if(!garD[s32ID].pAlsaPCM)
  {
    *pu32Read = 0;
    ACOUSTICIN_PRINTF_ERRORS("%s: No PCM handle", __FUNCTION__);
    return OSAL_E_NOFILEDESCRIPTOR;
  } //if(!garD[s32ID].pAlsaPCM)

  /* init ext write struct with default values for posix-style write */
  rReadInfo.pvBuffer      = (tPVoid)ps8Buffer;
  rReadInfo.u32BufferSize = u32Size;
  rReadInfo.u32Timestamp  = 0;

  /* do the read */
  u32Ret = u32DoReadOperation(s32ID, u32FD, &rReadInfo, &u32BytesRead);

  if(OSAL_E_NOERROR != u32Ret)
  {
    vTraceAcousticInError(TR_LEVEL_ERRORS, EN_ACIN_READ, (tU32)u32Ret,
                          "read", (tU32)s32ID, (tU32)u32FD, 0, __LINE__);
    *pu32Read = 0;
    return u32Ret;
  }

  *pu32Read = u32BytesRead;

  vTraceAcousticInInfo(TR_LEVEL_USER_3, EN_ACIN_READ,
                       "exit", (tU32)s32ID, (tU32)u32FD,
                       rReadInfo.u32BufferSize, u32Ret);

  return *pu32Read;
}

/*************************************************************************/ /**
*  FUNCTION:      u32DoReadOperation
*
*  @brief         worker function for reading data to the acousticin driver,
*                 can be used for posix-style read, as well as for the
*                 EXTREAD IOCOntrol
*
*  @param[in]     s32ID             stream ID
*  @param[in]     u32FD             file handle
*  @param[in,out] prReadInfo        additional control information
*
*  @return       Osal error code
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tU32 u32DoReadOperation(tS32 s32ID, tU32 u32FD,
                               OSAL_trAcousticInRead* prReadInfo,
                               tPU32 pu32BytesRead)
{
  tU32   u32Ret;

  vTraceAcousticInInfo(TR_LEVEL_USER_3, EN_DO_READOPERATION,
                       "enter", (tU32)s32ID,
                       (tU32)u32FD, prReadInfo->u32BufferSize, 0);

  *pu32BytesRead = 0;

  /* if we are not in state "active", no read operation is allowed */
  if(ACOUSTICIN_EN_STATE_ACTIVE != garD[s32ID].rDRVIf.enPlayState)
  {
    vTraceAcousticInError(TR_LEVEL_ERRORS,
                          EN_DO_READOPERATION, OSAL_E_NOACCESS, "state",
                          (tU32)garD[s32ID].rDRVIf.enPlayState,
                          0, 0, __LINE__);

    return OSAL_E_NOACCESS;
  }

  if((NULL == prReadInfo)
     ||
     (NULL == prReadInfo->pvBuffer))
  {
    vTraceAcousticInError(TR_LEVEL_ERRORS,
                          EN_DO_READOPERATION, OSAL_E_INVALIDVALUE,
                          "nullp",
                          (tU32)garD[s32ID].rDRVIf.enPlayState,
                          0, 0, __LINE__);

    /* Nullpointer given */
    return OSAL_E_INVALIDVALUE;
  }

  if(prReadInfo->u32BufferSize <
     garD[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM])
  {
    vTraceAcousticInError(TR_LEVEL_ERRORS,
                EN_DO_READOPERATION, OSAL_E_NOSPACE,
                "size",
                (tU32)garD[s32ID].rDRVIf.enPlayState,prReadInfo->u32BufferSize,
                garD[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM],
                __LINE__);

    /* given size is too big */
    return OSAL_E_NOSPACE;
  }


  /* Now read audio buffer from Alsa */
  u32Ret = u32RcvFromAlsa(s32ID,
                          prReadInfo->pvBuffer,
                          (int)prReadInfo->u32BufferSize,
                          pu32BytesRead);//, prReadInfo);

  if(OSAL_E_NOERROR == u32Ret)
  {
    prReadInfo->u32BufferSize =
    garD[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM];
  }
  else
  {
    vTraceAcousticInError(TR_LEVEL_ERRORS,
                          EN_DO_READOPERATION,
                          u32Ret,
                          "driver",
                          (tU32)garD[s32ID].rDRVIf.enPlayState,
                          prReadInfo->u32BufferSize,
                          garD[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_ENC_PCM],
                          __LINE__);
    prReadInfo->u32BufferSize = 0;
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_3, EN_DO_READOPERATION,"exit",
                       (tU32)s32ID,
                       (tU32)0,
                       prReadInfo->u32BufferSize, u32Ret);

  return u32Ret;
}

/*************************************************************************/ /**
*  FUNCTION:      iHandleXRUN
*  @brief         try to recover XRUN
*  @param[in]     s32ID          stream ID
*  @param[in]     iRet           ALSA error from previous call
*  @param[in]     bTraceActive   trace turned on or off
*  @return        new ALSA error code after revocering
*****************************************************************************/
static int iHandleXRUN(tS32 s32ID, int iRet, tBool bTraceActive)
{
  // no error, return previous error code
  if(iRet >= 0)
    return iRet;

  if(bTraceActive)
  {
    ACOUSTICIN_PRINTF_U3("%s: try to handle return value: [%d]",
                         __FUNCTION__, iRet);
  }
  
  // not an error
  if(iRet == -EAGAIN)
    return 0;
  
  // error < 0, try to handle
  if(bTraceActive)
    vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, TRUE);
  
  iRet = snd_pcm_recover(garD[s32ID].pAlsaPCM, iRet, 0);
  if(iRet == 0)
  { // recover successfully
    if(bTraceActive)
    {
      ACOUSTICIN_PRINTF_U3("%s: try to restart", __FUNCTION__);
    }
    
    iRet  = snd_pcm_start(garD[s32ID].pAlsaPCM);
    if(iRet == 0)
    {
      if(bTraceActive)
      {
        ACOUSTICIN_PRINTF_U3("%s: successfully restarted", __FUNCTION__);
      }
    }
    else
    {
      ACOUSTICIN_PRINTF_ERRORS("%s: snd_pcm_start error [%d]",
                               __FUNCTION__, iRet);
      vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, TRUE);
    }
  }
  else //if(iRet == 0)
  {
    if(bTraceActive)
    {
      ACOUSTICIN_PRINTF_U3("%s: snd_pcm_recover error [%d]",
                           __FUNCTION__, iRet);
    }
  } //else //if(iRet == 0)

  return iRet;
}
/*************************************************************************/ /**
*  FUNCTION:      u32RcvFromAlsa
*  @brief         Receives an audio buffer
*  @param[in]     s32ID            stream ID
*  @param[out]    pu8Buffer        buffer for read audio data
*  @param[in]     iBufSizeBytes    size of provided buffer
*  @param[out]    pu32BytesRead    number of bytes read
*  @return        OSAL error code
*****************************************************************************/
static tU32 u32RcvFromAlsa(tS32 s32ID,
                           snd_pcm_uframes_t *pFrameBuffer,
                           int iBufSizeBytes,
                           tPU32 pu32BytesRead)
{
  tU32 u32RetVal = OSAL_E_NOERROR;
  tBool bExit = FALSE;
  snd_pcm_format_t frmt;
  snd_pcm_sframes_t sFramesToRead;
  snd_pcm_sframes_t sRet = 0;
  long lBufSizeSamples;
  tBool bTraceActive = bIsAcousticInTraceActive(TR_LEVEL_USER_1);
  
  if(!pu32BytesRead || !pFrameBuffer)
  {
    return OSAL_E_INVALIDVALUE;
  } //if(!pu32BytesRead)

  *pu32BytesRead = 0;

  sFramesToRead = snd_pcm_bytes_to_frames(garD[s32ID].pAlsaPCM,
                                           (ssize_t)iBufSizeBytes);

  lBufSizeSamples = snd_pcm_bytes_to_samples(garD[s32ID].pAlsaPCM,
                                            (ssize_t)iBufSizeBytes);

  frmt = alsaConvertFormatOsalToAlsa(garD[s32ID].rPCMConfig.enSampleFormat);
  (void)snd_pcm_format_set_silence(frmt,
                                   pFrameBuffer,
                                   (unsigned int)lBufSizeSamples); 

  if(bTraceActive)
    ACOUSTICIN_PRINTF_U3("%s: "
                         "iBufSizeBytes %d, "
                         "sFramesToRead %d, "
                         "lBufSizeSamples %d",
                         __FUNCTION__,
                         iBufSizeBytes,
                         (int)sFramesToRead,
                         (int)lBufSizeSamples);
  
  //read loop (non blocking mode)
  do //while(!bExit);
  {
    sRet = snd_pcm_readi(garD[s32ID].pAlsaPCM, pFrameBuffer,
                         (snd_pcm_uframes_t)sFramesToRead);

    if(sRet >= 0)
    { //read successfully, increment buffer pointer and decrement sample count
      ssize_t nBytesRead;
      unsigned char* puc;

      if(bTraceActive)
      {
        unsigned short *pusDbg;
        ACOUSTICIN_PRINTF_U3("%s:snd_pcm_readi:"
                             "sRet %d, "
                             "sFramesToRead %d",
                             __FUNCTION__,
                             (int)sRet,
                             (int)sFramesToRead);
  
        pusDbg = (unsigned short*)(void*)pFrameBuffer;
        ACOUSTICIN_PRINTF_U4("%s:samples (hex16)"
                             "[0-7] %04X %04X %04X %04X %04X %04X %04X %04X ",
                             __FUNCTION__,
                             (unsigned int)pusDbg[0],
                             (unsigned int)pusDbg[1],
                             (unsigned int)pusDbg[2],
                             (unsigned int)pusDbg[3],
                             (unsigned int)pusDbg[4],
                             (unsigned int)pusDbg[5],
                             (unsigned int)pusDbg[6],
                             (unsigned int)pusDbg[7]
                            );
      } //if(bTraceActive)

      // sRet contains read frames
      nBytesRead = snd_pcm_frames_to_bytes(garD[s32ID].pAlsaPCM, sRet);
      vDebugFileWrite(s32ID, pFrameBuffer, (size_t)nBytesRead);
      puc = (unsigned char*)(void*)pFrameBuffer;
      puc = &puc[nBytesRead]; // avoid pointer arithmetic
      pFrameBuffer = (snd_pcm_uframes_t *)(void*)puc; // new buffer pointer
      sFramesToRead -= sRet;  // frames to read next loop
      *pu32BytesRead += (tU32)nBytesRead;
      if(sFramesToRead <= 0)
        bExit = TRUE;
    }
    else //if(sRet >= 0)
    { //readi-error
      int  iRet = iHandleXRUN(s32ID, (int)sRet, bTraceActive);
      if(iRet < 0)
      { // unknown error or stream could not be started after recover
        u32RetVal = OSAL_E_UNKNOWN;
        bExit = TRUE;
      }
      else //if(iRet < 0)
      { //XRUN recovered or -EAGAIN
        iRet = snd_pcm_wait(garD[s32ID].pAlsaPCM, 2000);
        if(iRet == 0)
        { //timeout
          ACOUSTICIN_PRINTF_ERRORS("%s: ERROR WAIT timeout", __FUNCTION__);
          vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, TRUE);
          u32RetVal = OSAL_E_TIMEOUT;
          bExit = TRUE;
        } //if(iRet == 0)
      } //else //if(iRet < 0)
    } //else //if(sRet >= 0)
  }
  while(!bExit);
  
  if(bTraceActive)
    ACOUSTICIN_PRINTF_U3("%s: Ends with Alsa-Status: sRet [%d], " 
                         "sFramesToRead [%d], *pu32BytesRead [%u]",
                         __FUNCTION__,
                         sRet, sFramesToRead,
                         pu32BytesRead ? (unsigned int)*pu32BytesRead : 0);
  return u32RetVal;
}


/*************************************************************************/ /**
*  FUNCTION:      u32InitAlsa
*  @brief         Initializes the Alsa system
*  @param         s32ID   stream ID
*  @return        OSAL error code
*****************************************************************************/
static tU32 u32InitAlsa(tS32 s32ID)
{
  int err;
  const char *pcszDevice;
  int iDirection;
  unsigned int uiSampleRate;
  unsigned int uiPeriods;
  snd_pcm_uframes_t framesBufferSize;
  snd_pcm_uframes_t framesBufferSizeIdeal;
  snd_pcm_uframes_t framesPeriodSize;
  snd_pcm_uframes_t framesPeriodSizeIdeal;
  snd_pcm_hw_params_t *hw_params;
  snd_pcm_sw_params_t *swparams;
  snd_pcm_uframes_t uAvailMin;
  snd_pcm_uframes_t uThreshStop;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_INIT_ALSA,
                       "enter", (tU32)s32ID, 0, 0, 0);

  /* Open PCM device for recording (capture). */
  pcszDevice = garD[s32ID].szAlsaDeviceName;
  err = snd_pcm_open( &garD[s32ID].pAlsaPCM, pcszDevice,
                      SND_PCM_STREAM_CAPTURE, SND_PCM_NONBLOCK);

  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:unable to open pcm device (%s): %s",
                             __FUNCTION__,
                             pcszDevice,
                             snd_strerror(err) );
    if(err == -EBUSY)
    {
      if(bIsAcousticInTraceActive(TR_LEVEL_ERRORS))
        vDebugSearchFuser();
    } //if(err == -EBUSY)

    garD[s32ID].pAlsaPCM = NULL;
    return OSAL_E_UNKNOWN;
  } //if(err < 0)

  /* Allocate a hardware parameters object. */
  snd_pcm_hw_params_alloca(&hw_params);/*lint !e717*/

  /* Fill it in with default values. */
  err = snd_pcm_hw_params_any( garD[s32ID].pAlsaPCM,
                               hw_params);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:cannot set default params (%s)",
                             __FUNCTION__,
                             snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }

  /* Set the desired hardware parameters. */
  /* Interleaved mode */
  err = snd_pcm_hw_params_set_access(garD[s32ID].pAlsaPCM,
                                     hw_params,
                                     SND_PCM_ACCESS_RW_INTERLEAVED );
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:cannot set access type (%s)",
                             __FUNCTION__,
                             snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICIN_PRINTF_U3("%s: INFO: "
                         "access == %u",
                         __FUNCTION__,
                         (unsigned int)SND_PCM_ACCESS_RW_INTERLEAVED);
  } //else //if (err < 0)

  err = snd_pcm_hw_params_set_format(garD[s32ID].pAlsaPCM,
                                     hw_params,
                                     alsaConvertFormatOsalToAlsa(
                                       garD[s32ID].rPCMConfig.enSampleFormat));
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:cannot set format (%s)",
                             __FUNCTION__,
                             snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICIN_PRINTF_U3("%s: INFO: "
                         "format == %u",
                         __FUNCTION__,
                         (unsigned int)garD[s32ID].rPCMConfig.enSampleFormat);
  } //else //if (err < 0)

  err = snd_pcm_hw_params_set_channels(garD[s32ID].pAlsaPCM,
                             hw_params,
                             garD[s32ID].rPCMConfig.u16NumChannels);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:cannot set channels"
                   "(Channels:%u) (%s)",
                   __FUNCTION__,
                   (unsigned int)garD[s32ID].rPCMConfig.u16NumChannels,
                   snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICIN_PRINTF_U3("%s: INFO: "
                         "channels == %u",
                         __FUNCTION__,
                         (unsigned int)garD[s32ID].rPCMConfig.u16NumChannels);
  } //else //if (err < 0)

  uiSampleRate = garD[s32ID].rPCMConfig.nSampleRate;
  iDirection   = 0;
  err = snd_pcm_hw_params_set_rate_near(garD[s32ID].pAlsaPCM,
                                        hw_params,
                                        &uiSampleRate, &iDirection);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:cannot "
                             "set rate_near (rate:%u) <%s>",
                             __FUNCTION__,
                             uiSampleRate,
                             snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICIN_PRINTF_U3("%s: INFO: "
                         "rate_near == %u",
                         __FUNCTION__,
                         uiSampleRate);
  } //else //if (err < 0)
  
  garD[s32ID].rPCMConfig.nSampleRate = uiSampleRate;

  // try to set buffer to 2 seconds
  framesBufferSizeIdeal = 2 * uiSampleRate; 
  framesBufferSize = framesBufferSizeIdeal;
  err = snd_pcm_hw_params_set_buffer_size_near(garD[s32ID].pAlsaPCM,
                                               hw_params,
                                               &framesBufferSize);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s ERROR: cannot set "
                             "buffer size (%u frames) (%s)",
                             __FUNCTION__,
                             (unsigned int)framesBufferSize, 
                             snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICIN_PRINTF_U3("%s: INFO: "
                         "buffer == %u, (ideal %u)",
                         __FUNCTION__,
                         (unsigned int)framesBufferSize,
                         (unsigned int)framesBufferSizeIdeal);
  } //else //if (err < 0)

  // try to set period size to 10ms
  framesPeriodSizeIdeal = (snd_pcm_uframes_t)uiSampleRate / 100;
  framesPeriodSize = framesPeriodSizeIdeal;
  iDirection = 0;
  err = snd_pcm_hw_params_set_period_size_near(garD[s32ID].pAlsaPCM,
                                               hw_params,
                                               &framesPeriodSize, &iDirection);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s: ERROR: "
                             "cannot set period size (%u frames) (%s)",
                             __FUNCTION__,
                             (unsigned int)framesPeriodSize,
                             snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICIN_PRINTF_U3("%s: INFO: "
                         "period size == %u (ideal %u), dir %d",
                         __FUNCTION__,
                         (unsigned int)framesPeriodSize,
                         (unsigned int)framesPeriodSizeIdeal,
                         iDirection);
  } //else //if (err < 0)

  err = snd_pcm_hw_params(garD[s32ID].pAlsaPCM, hw_params);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:unable to set hw parameters: %s",
                             __FUNCTION__,
                             snd_strerror(err) );
    return OSAL_E_UNKNOWN;
  } //if ( err < 0)

  // read number of periods
  iDirection = 0;
  err = snd_pcm_hw_params_get_periods(hw_params, &uiPeriods, &iDirection);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s: ERROR: "
                             "cannot get periods) (%s)",
                             __FUNCTION__,
                             snd_strerror(err));
    if(framesPeriodSize > 0)
    {
      uiPeriods = (unsigned int)framesBufferSize /
                  (unsigned int)framesPeriodSize;
    }
    else
    {
      uiPeriods = 0;
    }
  }
  ACOUSTICIN_PRINTF_U3("%s: INFO: "
                       "periods == %u, dir %d",
                       __FUNCTION__,
                       uiPeriods,
                       iDirection);

  /*set sw parameters*/
  snd_pcm_sw_params_alloca(&swparams);/*lint !e717*/

  /* get the current swparams */
  err = snd_pcm_sw_params_current(garD[s32ID].pAlsaPCM, swparams);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:Unable to determine "
                             "current swparams for playback: %s",
                             __FUNCTION__,
                             snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }

  //set avail to period size
  uAvailMin = framesPeriodSize; // * uiPeriods;
  err = snd_pcm_sw_params_set_avail_min(garD[s32ID].pAlsaPCM,
                                        swparams, uAvailMin);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:Unable to set "
                             "avail min %u: %s",
                             __FUNCTION__,
                             (unsigned int)uAvailMin,
                             snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }

  //enable XRUN, set max threshold to number of periods * period size
  uThreshStop = framesPeriodSize * uiPeriods;
  err = snd_pcm_sw_params_set_stop_threshold(garD[s32ID].pAlsaPCM,
                                        swparams, uThreshStop);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:Unable to set "
                             "stop_treshold to %u: %s",
                             __FUNCTION__,
                             (unsigned int)uThreshStop,
                             snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }
  
  /* write the parameters to the playback device */
  err = snd_pcm_sw_params(garD[s32ID].pAlsaPCM, swparams);
  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:Unable to set sw params"
                             " for capture: %s",
                             __FUNCTION__,
                             snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }

  garD[s32ID].bAlsaIsOpen = TRUE;
  if(bIsAcousticInTraceActive(TR_LEVEL_USER_1))
    vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, TRUE);

  if(err < 0)
  {
    ACOUSTICIN_PRINTF_ERRORS("%s:Unable to start PCM"
                             " for capture: %s",
                             __FUNCTION__,
                             snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_INIT_ALSA,
                       "exit", (tU32)s32ID,
                       (tU32)garD[s32ID].bAlsaIsOpen, 0, 0);
  return OSAL_E_NOERROR;
}

/*************************************************************************/ /**
*  FUNCTION:      u32UnInitAlsa
*
*  @brief         UnInitializes the Alsa
*
*  @param         s32ID   stream ID
*
*  @return        OSAL error code
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tU32 u32UnInitAlsa(tS32 s32ID)
{
  tU32 u32Ret = OSAL_E_NOERROR;

  if(garD[s32ID].pAlsaPCM != NULL)
  {
    //ALSA CLOSE
    garD[s32ID].bAlsaIsOpen = FALSE;
    snd_pcm_close(garD[s32ID].pAlsaPCM);
    garD[s32ID].pAlsaPCM = NULL;
  }
  else //if(garD[s32ID].pAlsaPCM != NULL)
  {
    u32Ret = OSAL_E_DOESNOTEXIST;
    vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_UNINITOUTPUTDEVICE,
                         "notexist", (tU32)s32ID, 0, 0, 0);
  } //else //if(garD[s32ID].pAlsaPCM != NULL)

  return u32Ret;
}


/*************************************************************************/ /**
*  FUNCTION:      u32AbortStream
*
*  @brief         Aborts stream immediately
*
*  @param         s32ID              stream ID
*
*  @return        OSAL error code
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tU32 u32AbortStream(tS32 s32ID)
{
  tU32 u32RetVal = OSAL_E_NOERROR;

  /* new state is "Stopped" */
  garD[s32ID].rDRVIf.enPlayState = ACOUSTICIN_EN_STATE_STOPPED;

  return u32RetVal;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsSampleformatValid
*
*  @brief         Checks whether provided sample format value is valid
*
*  @param         enSampleformat  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
*    Initial revision.
*****************************************************************************/
static tBool bIsSampleformatValid(OSAL_tenAcousticSampleFormat enSampleformat)
{
  tU8 u8Idx;
  for(u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(aenSupportedSampleformats); u8Idx++)
  {
    if(aenSupportedSampleformats[u8Idx] == enSampleformat)
    {
      return TRUE;
    }
  }

  return FALSE;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsSamplerateValid
*
*  @brief         Checks whether provided sample rate value is valid
*
*  @param         nSamplerate  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*

*    Initial revision.
*****************************************************************************/
static tBool bIsSamplerateValid(OSAL_tAcousticSampleRate nSamplerate)
{
  tU8 u8Idx;
  for(u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(anSupportedSampleratesFrom); u8Idx++)
  {
    if((anSupportedSampleratesFrom[u8Idx] <= nSamplerate) &&
       (anSupportedSampleratesTo[u8Idx] >= nSamplerate))
    {
      return TRUE;
    }
  }

  return FALSE;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsChannelnumValid
*
*  @brief         Checks whether provided channel num value is valid
*
*  @param         u16Channelnum  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*

*    Initial revision.
*****************************************************************************/
static tBool bIsChannelnumValid(tU16 u16Channelnum)
{
  tU8 u8Idx;
  for(u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(au16SupportedChannelnums); u8Idx++)
  {
    if(au16SupportedChannelnums[u8Idx] == u16Channelnum)
    {
      return TRUE;
    }
  }

  return FALSE;
}

/*************************************************************************/ /**
*  FUNCTION:      bIsBuffersizeValid
*
*  @brief         Checks whether provided buffer size value is valid
*
*  @param         u32Buffersize  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*

*    Initial revision.
*****************************************************************************/
static tBool bIsBuffersizeValid(tU32 u32Buffersize)
{
  tU8 u8Idx;
  for(u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM); u8Idx++)
  {
    if(au32SupportedBuffersizesPCM[u8Idx] == u32Buffersize)
    {
      return TRUE;
    }
  }

  return FALSE;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Version
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*  functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_Version(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  if(s32Arg)
  {
    vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_VERSION,
                         "enter",
                         (tU32)s32ID, u32FD, (tU32)s32Arg,
                         (tU32)garD[s32ID].rDRVIf.enPlayState);   
    /* write version */
    *(tPS32)s32Arg = ACOUSTICIN_C_S32_IO_VERSION;

    vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_VERSION,
                         "exit", (tU32)s32ID,
                         u32FD, (tU32)s32Arg,
                         (tU32)garD[s32ID].rDRVIf.enPlayState);   
  }
  else
  {
    vTraceAcousticInError(TR_LEVEL_ERRORS, EN_IOCTRL_VERSION,
                          OSAL_E_INVALIDVALUE,"invval",
                          (tU32)s32ID, u32FD,
                          (tU32)garD[s32ID].rDRVIf.enPlayState, 0);

    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Extread
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_Extread(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticInRead* pExtReadInfo;
  tU32 u32BytesRead = 0;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_EXTREAD,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  pExtReadInfo = (OSAL_trAcousticInRead*)s32Arg;

  if(NULL == pExtReadInfo)
  {
    /* nullpointer */
    u32ErrorCode =  OSAL_E_INVALIDVALUE;
  }
  else
  {
    /* OK, do the read operation */
    u32ErrorCode = u32DoReadOperation(s32ID, u32FD,
                                      pExtReadInfo,
                                      &u32BytesRead);
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_EXTREAD,
                       "exit", (tU32)s32ID, u32FD,
                       (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);  
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_RegNotification
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*  functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static void IOCtrl_RegNotification(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  OSAL_trAcousticInCallbackReg* pCallback;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_REG_NOTIFICATION,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  pCallback = (OSAL_trAcousticInCallbackReg*)s32Arg;

  if((NULL == pCallback) || (NULL == pCallback->pfEvCallback))
  {
    /* unregister callback */
    garD[s32ID].rDRVIf.rCallback.pfEvCallback = NULL;
    garD[s32ID].rDRVIf.rCallback.pvCookie  = NULL;
  }
  else
  {
    /* register callback */ 
    garD[s32ID].rDRVIf.rCallback.pfEvCallback 
    = (OSAL_tpfAcousticInEvCallback)pCallback->pfEvCallback;
    garD[s32ID].rDRVIf.rCallback.pvCookie 
    = (tPVoid)pCallback->pvCookie;
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_REG_NOTIFICATION,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_WaitEvent
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_WaitEvent(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  OSAL_trAcousticInWaitEvent* pWaitEvent;
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_WAITEVENT,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  pWaitEvent = (OSAL_trAcousticInWaitEvent*)s32Arg;

  if(NULL == pWaitEvent)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_WAITEVENT,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState); 
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSuppSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this functionality
*  as a sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_GetSuppSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleRateCapability* prSampleRateCap;
  tU32 u32CopyIdx;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLERATE,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  prSampleRateCap = (OSAL_trAcousticSampleRateCapability*)s32Arg;

  if(NULL == prSampleRateCap)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleRateCap->enCodec)
  {
    /* sample rate only configurable for PCM */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else
  {
    /* OK, get samplerate capabilities */
    for(u32CopyIdx = 0; (u32CopyIdx < prSampleRateCap->u32ElemCnt)
       &&(u32CopyIdx < NUM_ARRAY_ELEMS(anSupportedSampleratesFrom));
       u32CopyIdx++)
    {
      prSampleRateCap->pnSamplerateFrom[u32CopyIdx] =
      anSupportedSampleratesFrom[u32CopyIdx];
      prSampleRateCap->pnSamplerateTo[u32CopyIdx] =
      anSupportedSampleratesTo[u32CopyIdx];
    } //for ...
    prSampleRateCap->u32ElemCnt =
    NUM_ARRAY_ELEMS(anSupportedSampleratesFrom);
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLERATE,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_GetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  OSAL_trAcousticSampleRateCfg* prSampleRateCfg;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLERATE,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  prSampleRateCfg = (OSAL_trAcousticSampleRateCfg*)s32Arg;

  if(NULL == prSampleRateCfg)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleRateCfg->enCodec)
  {
    /* sample rate only configurable for PCM */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else
  {
    /* OK, get samplerate */
    prSampleRateCfg->nSamplerate = garD[s32ID].rPCMConfig.nSampleRate;
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLERATE,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_SetSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_SetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleRateCfg* prSampleRateCfg;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLERATE,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  if(ACOUSTICIN_EN_STATE_STOPPED != garD[s32ID].rDRVIf.enPlayState)
  {
    /* stream currently running, set not allowed */
    u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
  }
  else
  {
    prSampleRateCfg = (OSAL_trAcousticSampleRateCfg*)s32Arg;

    if(NULL == prSampleRateCfg)
    {
      /* nullpointer */
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
    else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleRateCfg->enCodec)
    {
      /* sample rate only configurable for PCM */
      u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
    }
    else if(bIsSamplerateValid(prSampleRateCfg->nSamplerate))
    {
      /* OK, set samplerate */
      garD[s32ID].rPCMConfig.nSampleRate =
      prSampleRateCfg->nSamplerate;
    }
    else
    {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLERATE,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSuppSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_GetSuppSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleFormatCapability* prSampleFormatCap;
  tU32 u32CopyIdx;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLEFORMAT,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  prSampleFormatCap = (OSAL_trAcousticSampleFormatCapability*)s32Arg;

  if(NULL == prSampleFormatCap)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleFormatCap->enCodec)
  {
    /* sample format only configurable for PCM */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else
  {
    /* OK, get sampleformat capabilities */
    for(u32CopyIdx = 0;(u32CopyIdx < prSampleFormatCap->u32ElemCnt) &&
       (u32CopyIdx < NUM_ARRAY_ELEMS(aenSupportedSampleformats));u32CopyIdx++)
    {
      prSampleFormatCap->penSampleformats[u32CopyIdx] =
      aenSupportedSampleformats[u32CopyIdx];
    }
    prSampleFormatCap->u32ElemCnt = NUM_ARRAY_ELEMS(aenSupportedSampleformats);
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_SAMPLEFORMAT,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_GetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleFormatCfg* prSampleFormatCfg;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLEFORMAT,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  prSampleFormatCfg = (OSAL_trAcousticSampleFormatCfg*)s32Arg;

  if(NULL == prSampleFormatCfg)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleFormatCfg->enCodec)
  {
    /* sample format only configurable for PCM */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else
  {
    /* OK, get sampleformat */
    prSampleFormatCfg->enSampleformat = garD[s32ID].rPCMConfig.enSampleFormat;
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSAMPLEFORMAT,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_SetSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_SetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleFormatCfg* prSampleFormatCfg;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLEFORMAT,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);     

  if(ACOUSTICIN_EN_STATE_STOPPED != garD[s32ID].rDRVIf.enPlayState)
  {
    /* stream currently running, set not allowed */
    u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
  }

  else
  {
    prSampleFormatCfg = (OSAL_trAcousticSampleFormatCfg*)s32Arg;

    if(NULL == prSampleFormatCfg)
    {
      /* nullpointer */
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
    else if(OSAL_EN_ACOUSTIC_ENC_PCM != prSampleFormatCfg->enCodec)
    {
      /* sample format only configurable for PCM */
      u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
    }
    else if(bIsSampleformatValid(prSampleFormatCfg->enSampleformat))
    {
      /* OK, set sampleformat */
      garD[s32ID].rPCMConfig.enSampleFormat =
      prSampleFormatCfg->enSampleformat;
    }
    else
    {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETSAMPLEFORMAT,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSuppChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_GetSuppChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticChannelCapability* prChanCap;
  tU32 u32CopyIdx;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_CHANNELS,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  prChanCap = (OSAL_trAcousticChannelCapability*)s32Arg;

  if(NULL == prChanCap)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else
  {
    /* OK, get channel num capabilities */
    for(u32CopyIdx = 0;
       (u32CopyIdx < prChanCap->u32ElemCnt) &&
       (u32CopyIdx < NUM_ARRAY_ELEMS(au16SupportedChannelnums));
       u32CopyIdx++)
    {
      prChanCap->pu32NumChannels[u32CopyIdx] =
      au16SupportedChannelnums[u32CopyIdx];
    }
    prChanCap->u32ElemCnt = NUM_ARRAY_ELEMS(au16SupportedChannelnums);
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_CHANNELS,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_GetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  tPU16 pu16NumChannels;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETCHANNELS,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  pu16NumChannels = (tPU16)s32Arg;

  if(NULL == pu16NumChannels)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else
  {
    /* OK, set number of channels */
    *pu16NumChannels = garD[s32ID].rPCMConfig.u16NumChannels;
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETCHANNELS,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);  
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_SetChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_SetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  tU16 u16NumChannels;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETCHANNELS,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  if(ACOUSTICIN_EN_STATE_STOPPED != garD[s32ID].rDRVIf.enPlayState)
  {
    /* stream currently running, setchannels not allowed */
    u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;   
  }
  else
  {
    // ACOUSTICIN_BUGFIX
    u16NumChannels = (tU16)s32Arg;

    if(bIsChannelnumValid(u16NumChannels))
    {
      /* OK, set number of channels */
      garD[s32ID].rPCMConfig.u16NumChannels = u16NumChannels;
    }
    else
    {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
  }

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETCHANNELS,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSuppBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*  - 17.07.2012 Niyatha Rao(nro2kor), Included an error check for the
*   wrong codec
*
************************************************************************/
static tU32 IOCtrl_GetSuppBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticBufferSizeCapability* prBufferSizeCap;
  tU32 u32CopyIdx;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_BUFFERSIZE,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  prBufferSizeCap = (OSAL_trAcousticBufferSizeCapability*)s32Arg;

  if(NULL == prBufferSizeCap)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else if(OSAL_EN_ACOUSTIC_ENC_PCM != prBufferSizeCap->enCodec)
  {
    /* Buffer size only configurable for PCM */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else
  {
    /* OK, get channel num capabilities */
    for(u32CopyIdx = 0; (u32CopyIdx < prBufferSizeCap->u32ElemCnt) &&
       (u32CopyIdx < NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM));
       u32CopyIdx++)
    {
      prBufferSizeCap->pnBuffersizes[u32CopyIdx] =
      au32SupportedBuffersizesPCM[u32CopyIdx];
    }

    prBufferSizeCap->u32ElemCnt =
    NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM);
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETSUPP_BUFFERSIZE,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*  - 17.07.2012 Niyatha Rao(nro2kor), Included an error check for the
*   wrong codec
*
************************************************************************/
static tU32 IOCtrl_GetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticBufferSizeCfg* pBufferSizeCfg;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETBUFFERSIZE,
                       "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  pBufferSizeCfg = (OSAL_trAcousticBufferSizeCfg*)s32Arg;

  if(NULL == pBufferSizeCfg)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else if(OSAL_EN_ACOUSTIC_ENC_PCM != pBufferSizeCfg->enCodec)
  {
    /* Buffer size only configurable for PCM */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }

  else
  {
    /* OK, get buffersize */
    pBufferSizeCfg->nBuffersize =
    garD[s32ID].anBufferSize[pBufferSizeCfg->enCodec];
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_GETBUFFERSIZE,
                       "exit", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_SetBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*  - 17.07.2012 Niyatha Rao(nro2kor), Included an error check for the
*   wrong codec
*
************************************************************************/
static tU32 IOCtrl_SetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticBufferSizeCfg* pBufferSizeCfg;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETBUFFERSIZE,
                       "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  if(ACOUSTICIN_EN_STATE_STOPPED != garD[s32ID].rDRVIf.enPlayState)
  {
    /* stream currently running, setbuffersize not allowed */
    u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
  }
  else
  {
    pBufferSizeCfg = (OSAL_trAcousticBufferSizeCfg*)s32Arg;

    if(NULL == pBufferSizeCfg)
    {
      /* nullpointer */
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
    else if(pBufferSizeCfg->enCodec != OSAL_EN_ACOUSTIC_ENC_PCM)
    {
      /* Buffer size only configurable for PCM */
      u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
    }
    else if(bIsBuffersizeValid(pBufferSizeCfg->nBuffersize))
    {
      /* OK, set buffersize */
      garD[s32ID].anBufferSize[pBufferSizeCfg->enCodec] =
      pBufferSizeCfg->nBuffersize;
    }
    else
    {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_SETBUFFERSIZE, 
                       "exit", (tU32)s32ID, u32FD, (tU32)s32Arg, 
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Start
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_Start(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_START,
                       "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState); 

  vDebugFileOpen(s32ID);
  
  if(ACOUSTICIN_EN_STATE_STOPPED == garD[s32ID].rDRVIf.enPlayState)
  {
    /* open alsa*/
    if(OSAL_E_NOERROR != u32InitAlsa(s32ID))
    {
      vTraceAcousticInError(TR_LEVEL_ERRORS, EN_IOCTRL_START,
                            OSAL_E_UNKNOWN,
                            "AlsaFail", 0, 0, 0, 0);

      u32ErrorCode = OSAL_E_UNKNOWN;
    }
    else
    {
      switch(s32ID)
      {
      case EN_ACOUSTICIN_DEVID_SPEECHRECO:
        {
          /* SDS frontend does not need to be started
          (done by client application)*/
          garD[s32ID].rDRVIf.enPlayState = ACOUSTICIN_EN_STATE_ACTIVE;
        }
        break;

      default:
        {
          // Unsupported device, should have been caught before
          vTraceAcousticInError(TR_LEVEL_ERRORS,
                                EN_IOCTRL_START,
                                OSAL_E_DOESNOTEXIST,
                                "noexist",
                                (tU32)s32ID, u32FD, 0, 0);


          u32UnInitAlsa(s32ID);

          u32ErrorCode = OSAL_E_UNKNOWN;
        }
        break;
      }
    }
  }
  else
  {
    // nothing to do
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_START,
                       "exit", (tU32)s32ID, u32FD,
                       (tU32)s32Arg, (tU32)garD[s32ID].rDRVIf.enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Stop
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_Stop(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_STOP,
                       "enter", (tU32)s32ID, u32FD,
                       (tU32)s32Arg, (tU32)garD[s32ID].rDRVIf.enPlayState);

  if(ACOUSTICIN_EN_STATE_ACTIVE == garD[s32ID].rDRVIf.enPlayState)
  {
    /* close alsa, stop apps */
    if(OSAL_E_NOERROR != u32UnInitAlsa(s32ID))
    {
      u32ErrorCode = OSAL_E_UNKNOWN;
    }
    else
    {
      /* frontend does not support notification for stopping stream,
        therefore abort stream immediately and report AUDIOSTOPPED event*/
      u32ErrorCode = u32AbortStream(s32ID);

      if(OSAL_E_NOERROR == u32ErrorCode)
      {
        /* Notify "audio stopped" */
        // notification via callback
        if(NULL != garD[s32ID].rDRVIf.rCallback.pfEvCallback)
        {
          garD[s32ID].rDRVIf.rCallback.pfEvCallback(
                                 OSAL_EN_ACOUSTICIN_EVAUDIOSTOPPED,
                                 NULL,
                                 garD[s32ID].rDRVIf.rCallback.pvCookie);
        }
      }
    }
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_STOP,
                       "exit", (tU32)s32ID, u32FD,
                       (tU32)s32Arg, (tU32)garD[s32ID].rDRVIf.enPlayState);

  vDebugFileClose(s32ID);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Abort
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*
*  - 17.07.2012 Niyatha Rao(nro2kor), Modified to implement this
*   functionality as a 
*	 sub function of IOCTRL
*    This has been done to reduce the cyclomatic code complexity
*
************************************************************************/
static tU32 IOCtrl_Abort(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_ABORT,
                       "enter", (tU32)s32ID,
                       u32FD, (tU32)s32Arg,
                       (tU32)garD[s32ID].rDRVIf.enPlayState);

  if(ACOUSTICIN_EN_STATE_ACTIVE == garD[s32ID].rDRVIf.enPlayState)
  {
    /* close alsa, stop apps */
    if(OSAL_E_NOERROR != u32UnInitAlsa(s32ID))
    {
      vTraceAcousticInError(TR_LEVEL_ERRORS,
                            EN_IOCTRL_ABORT,
                            OSAL_E_UNKNOWN,
                            "AlsaFail", 0, 0, 0, 0);

      u32ErrorCode = OSAL_E_UNKNOWN;
    }
    else
    {
      u32ErrorCode = u32AbortStream(s32ID);
    }
  }
  vTraceAcousticInInfo(TR_LEVEL_USER_2, EN_IOCTRL_ABORT,
                       "exit", (tU32)s32ID, u32FD,
                       (tU32)s32Arg, (tU32)garD[s32ID].rDRVIf.enPlayState);   
  
  vDebugFileClose(s32ID);
  return u32ErrorCode;
}

#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file 
|-----------------------------------------------------------------------*/
