/************************************************************************
| FILE:         acousticout.c
| PROJECT:      Gen3
| SW-COMPONENT: Acoustic driver
|------------------------------------------------------------------------
|************************************************************************/
/* ******************************************************FileHeaderBegin** *//**
* @file    acousticout.c
*
* @brief   This device is a generic audio output device. Its main
*          functionality is playback of PCM audio streams
*
* @author  srt2hi
*
*
* @version
*           - Added fix for SUZUKI-3183 - ysu1kor
*           - Added fix for GMMY16-235 - ysu1kor
*           - Added fix for NCG3D-17942 - boc7kor
*
*//* ***************************************************FileHeaderEnd******* */

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "osalio_public.h"
#include <alsa/asoundlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include "acousticout_public.h"
#include "acoustic_trace.h"
#include "acousticout_private.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define ACOUSTICOUT_CHANGE_THREAD_PRIO
#define NUM_ARRAY_ELEMS(array)  (sizeof(array)/sizeof(array[0]))
#define ACOUSTICOUT_EMERGENCY_TIMEOUT_MS 5000



/************************************************************************
|typedefs
|-----------------------------------------------------------------------*/
typedef struct ACOUSTICOUT_fade_cache_tag
{
  snd_pcm_sframes_t fadeLen;  //Length of Fading-Buffers in samples
  snd_pcm_sframes_t size;     //Entire size of cache (einschl. Fadingbuffer)
  snd_pcm_sframes_t fill;     // current fill level of cache line
  //snd_pcm_uframes_t *pCache;
  tS16 *pCache;  //!!! TODO works only if wave is s16_le and we are little endian arch
}ACOUSTICOUT_fade_cache_type;


/************************************************************************
| variable definition (scope: local)
|-----------------------------------------------------------------------*/
#ifdef ACOUSTICOUT_FADE_IF_ABORT
  static ACOUSTICOUT_fade_cache_type fadecache; 
#endif //#ifdef ACOUSTICOUT_FADE_IF_ABORT

/** List of decoders supported by the device */
static const OSAL_tenAcousticCodec aenSupportedCodecs[] =
{
  OSAL_EN_ACOUSTIC_DEC_PCM
};

/** List of PCM sample formats supported by the device */
static const OSAL_tenAcousticSampleFormat aenSupportedSampleformats[] =
{
  OSAL_EN_ACOUSTIC_SF_S16
};

/** List of PCM sample rates supported by the device (interval begin value) */
static const OSAL_tAcousticSampleRate anSupportedSampleratesFrom[] =
{
  8000, 11025, 16000, 22050, 32000, 44100, 48000
};

/** List of PCM sample rates supported by the device (interval end value) */
static const OSAL_tAcousticSampleRate anSupportedSampleratesTo[] =
{
  8000, 11025, 16000, 22050, 32000, 44100, 48000
};

/** List of PCM channel numbers supported by the device */
static tCU16 au16SupportedChannelnums[] =
{
  1,  /**< mono */
  2   /**< stereo */
};

/** List of buffer sizes of PCM codec supported by the device (in bytes)
*/
static tCU32 au32SupportedBuffersizesPCM[] =
{
  1024,
  2048,
  4096,
  8192,
  16384
};

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/** internal state variables, for each stream */
static trACOUSTICOUT_StateData garD[EN_ACOUSTICOUT_DEVID_LAST] = {0};

static int giCanceledCounter=0;

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation
|-----------------------------------------------------------------------*/

/********************************************************************/ /**
*  FUNCTION:      vDebugSearchPCMOF
*  @brief         prints out prozesses which are hold open PCM devices
************************************************************************/
static void vDebugSearchPCMOF()
{
  FILE *fp;
  char szTxt[256], szCommand[128];

  sprintf(szCommand, "lsof | grep pcm");
  fp = popen(szCommand, "r");
  if(fp != NULL)
  {
    memset(szTxt, 0, sizeof(szTxt));
    while(fgets(szTxt, sizeof(szTxt)-1, fp) != NULL)
    {
      ACOUSTICOUT_PRINTF_ERRORS(" PCM open [%s]", szTxt);
    } //while (fgets(szTxt, sizeof(szTxt)-1, fp) != NULL)
    pclose(fp);
  } //if(fp != NULL)
}

/********************************************************************/ /**
*  FUNCTION:      vDebugFileClose
*  @brief         close a file for wrting captured PCM data
************************************************************************/
static void vDebugFileClose(tS32 s32ID)
{
  if(s32ID >= EN_ACOUSTICOUT_DEVID_LAST || s32ID < 0)
    return;

  if(garD[s32ID].iDebugPCMFileHandle == -1)
  {
    (void)close(garD[s32ID].iDebugPCMFileHandle);
    garD[s32ID].iDebugPCMFileHandle = -1;
  }
}

/********************************************************************/ /**
*  FUNCTION:      vDebugFileOpen
*  @brief         opens a file for wrting captured PCM data
************************************************************************/
static void vDebugFileOpen(tS32 s32ID)
{
  static int iCnt = 0;
  char szFile[32];

  if(!bIsAcousticOutTraceActive(TR_LEVEL_USER_4))
    return;

  if(s32ID >= EN_ACOUSTICOUT_DEVID_LAST || s32ID < 0)
    return;

  vDebugFileClose(s32ID);
  sprintf(szFile, "/tmp/acout%04d.pcm", iCnt++);
  garD[s32ID].iDebugPCMFileHandle = open(szFile, O_APPEND | O_RDWR | O_CREAT);
}

/********************************************************************/ /**
*  FUNCTION:      vDebugFileWrite
*  @brief         write PCM data to file
************************************************************************/
static void vDebugFileWrite(tS32 s32ID, const void *pData, size_t nLen)
{
  if(s32ID >= EN_ACOUSTICOUT_DEVID_LAST || s32ID < 0)
    return;

  if(garD[s32ID].iDebugPCMFileHandle == -1)
    return;
  write(garD[s32ID].iDebugPCMFileHandle, pData, nLen);
}
/********************************************************************/ /**
*  FUNCTION:      iSlash2CamelCase
*
*  @brief         removes slashes from input string
*                 Add a first Letter 'A'
*                 changes Next letter after slash to capital letter
*                 Example: /dev/acoustic/speech -> AdevSpeechAcousticSpeech
*                 
*
*  @return        returns number of characters copied into Outputbuffer
*                 (without trailing '\0')
*  @retval        int
*
*  HISTORY:
************************************************************************/
static int iSlash2CamelCase(const char *pcszSlashes,
                            char *pszOutputBuffer,
                            int iOutputBufferSize)
{
  int   iIn = 0;
  int   iOut = 0;
  tBool bMakeCapital  = FALSE;
  tBool bIsFirstSlash = TRUE; //first character after '/' leave lower case
  char  c;

  pszOutputBuffer[iOut++] = 'A';
  while('\0' != (c = pcszSlashes[iIn++])
        &&
        ((iOut+1) < iOutputBufferSize))
  {
    if('/' == c)
    {
      //letter after first slash leave in lower case
      bMakeCapital = bIsFirstSlash ? FALSE : TRUE;
      bIsFirstSlash = FALSE;
    }
    else//if('/' == c)
    {
      pszOutputBuffer[iOut++] =
      (char)(bMakeCapital ? toupper((int)c) : c);
      bMakeCapital = FALSE;
    } //else//if('/' == c)
  } //while('\0' != (c = pcszSlashes[iIn++]) && !bOverflow)

  pszOutputBuffer[iOut] = '\0'; //trailing zero
  return iOut;
}


/********************************************************************/ /**
*  FUNCTION:      iConvertOsalDeviceID
*
*  @brief         converts OSAL to local IDs
*
*  @return        int local ID
*  @retval        int
*
*  HISTORY:
************************************************************************/
static tS32 s32ConvertOsalDeviceID(tS32 s32OsalDeviceID)
{
  tS32 s32ID;

  switch(s32OsalDeviceID)
  {
  case OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH:
    s32ID = (tS32)EN_ACOUSTICOUT_DEVID_SPEECH;
    break;
  case OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC:
    s32ID = (tS32)EN_ACOUSTICOUT_DEVID_MUSIC;
    break;
  default:
    s32ID = (tS32)EN_ACOUSTICOUT_DEVID_SPEECH;
    break;
  } //switch(s32OsalDeviceID)

  return s32ID;
}

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/********************************************************************/ /**
*  FUNCTION:      u32InitPrivate
*  @brief         Initializes any necessary resources.
*                 Should be called at OSAL startup.
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR   on success
************************************************************************/
static tU32 u32InitPrivate(tS32 s32ID)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  ACOUSTICOUT_PRINTF_U2("u32InitPrivate - START");

  garD[s32ID].bAlsaIsOpen = FALSE;
  garD[s32ID].iDebugPCMFileHandle = -1;
  garD[s32ID].threadPolicy = -1;

  switch(s32ID)
  {
  case EN_ACOUSTICOUT_DEVID_SPEECH:
    {
      garD[s32ID].enFileHandle =
      ACOUSTICOUT_EN_HANDLE_ID_SPEECH;
      break;
    }
  case EN_ACOUSTICOUT_DEVID_MUSIC:
    {
      garD[s32ID].enFileHandle =
      ACOUSTICOUT_EN_HANDLE_ID_MUSIC;
      break;
    }

  default:
    break;
  } //switch (s32ID)

  /* create event field */
  {
    char szEventName[32 + 1] = "";
    OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
    (tVoid) OSAL_s32PrintFormat(szEventName,
                                ACOUSTICOUT_C_SZ_EVENT_FORMAT,
                                (unsigned int)procID,
                                (unsigned int)s32ID);
    if(OSAL_OK != OSAL_s32EventCreate(szEventName,
                                      &garD[s32ID].hOsalEvent))
    {
      tU32 u32Err = OSAL_u32ErrorCode();
      ACOUSTICOUT_PRINTF_ERRORS("ACOUSTICOUT_u32Init"
                                " - OSAL_s32EventCreate"
                                " ERROR 0x%08X",
                                (unsigned int)u32Err);
    }

  }

  /* create close semaphore */
  {
    char szSemName[16 + 1] = "";
    OSAL_tProcessID procID = OSAL_ProcessWhoAmI();
    (tVoid) OSAL_s32PrintFormat(szSemName,
                                ACOUSTICOUT_C_SZ_SEM_CLOSE_FORMAT,
                                (unsigned int)procID,
                                (unsigned int)s32ID);
    if(OSAL_OK != OSAL_s32SemaphoreCreate(szSemName,
                                          &garD[s32ID].hSemClose,
                                          1)
      )
    {
      tU32 u32Err = OSAL_u32ErrorCode();
      ACOUSTICOUT_PRINTF_ERRORS("ACOUSTICOUT_u32Init"
                                " - OSAL_s32SemaphoreCreate"
                                " (CLOSESem)"
                                " ERROR 0x%08X",
                                (unsigned int)u32Err);
    }
  }

  /* we are now in state "Initialized" */
  garD[s32ID].enPlayState            = ACOUSTICOUT_EN_STATE_INITIALIZED;
  garD[s32ID].enPlayStateBeforePause = ACOUSTICOUT_EN_STATE_INITIALIZED;

  ACOUSTICOUT_PRINTF_U2("u32InitPrivate"
                        " - END u32ErrorCode 0x%08X",
                        u32ErrorCode);
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICOUT_u32DeinitPrivat
*  @brief         Releases any resources acquired during init.
*                 Before this function is called (e.g. by Powermanagement),
*                 the device must have been closed. This cannot be done
*                 here, because then the device would still be marked as
*                 used in the dispatcher.
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR   on success
************************************************************************/
static tU32 u32DeinitPrivate(tS32 s32ID)
{
  char szEventName[16 + 1] = "";
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  ACOUSTICOUT_PRINTF_U2("u32DeinitPrivate - START");

  /* prevent uninitializing, if already uninitialized */
  if(garD[s32ID].enPlayState != ACOUSTICOUT_EN_STATE_UNINITIALIZED)
  {
    tS32 s32Ret;
    OSAL_tProcessID procID = OSAL_ProcessWhoAmI();

    u32SetEvent(&garD[s32ID],
                ACOUSTICOUT_EN_EVENT_MASK_NOTI_CANCEL);
    OSAL_s32ThreadWait(50);
    u32ClearEvent(&garD[s32ID],
                  ACOUSTICOUT_EN_EVENT_MASK_NOTI_ANY);

    //Wait for acknowledge and destroy semaphore
    {
      char szSemName[16 + 1] = "";
      (tVoid)OSAL_s32SemaphoreWait(garD[s32ID].hSemClose,
                                   200  //timeout in ms
                                  );

      (tVoid)OSAL_s32SemaphoreClose(garD[s32ID].hSemClose);
      (tVoid)OSAL_s32PrintFormat(szSemName,
                                 ACOUSTICOUT_C_SZ_SEM_CLOSE_FORMAT,
                                 (unsigned int)procID,
                                 (unsigned int)s32ID);

      s32Ret = OSAL_s32SemaphoreDelete(szSemName);
      if(s32Ret != OSAL_OK)
      {
        tU32 u32Err = OSAL_u32ErrorCode();
        ACOUSTICOUT_PRINTF_ERRORS("ACOUSTICOUT_u32Deinit - "
                                  "(CloseSemaphore) "
                                  "OSAL_s32SemaphoreDelete ERROR 0x%08X",
                                  (unsigned int)u32Err);
      }
    }

    /* close and delete event field */
    s32Ret = OSAL_s32EventClose(garD[s32ID].hOsalEvent);
    if(s32Ret != OSAL_OK)
    {
      tU32 u32Err = OSAL_u32ErrorCode();
      ACOUSTICOUT_PRINTF_ERRORS("ACOUSTICOUT_u32Deinit - "
                                "OSAL_s32EventClose ERROR 0x%08X",
                                (unsigned int)u32Err);
    }

    (tVoid)OSAL_s32PrintFormat(szEventName,
                               ACOUSTICOUT_C_SZ_EVENT_FORMAT,
                               (unsigned int)procID,
                               (unsigned int)s32ID);

    s32Ret = OSAL_s32EventDelete(szEventName);
    if(s32Ret != OSAL_OK)
    {
      tU32 u32Err = OSAL_u32ErrorCode();
      ACOUSTICOUT_PRINTF_ERRORS("ACOUSTICOUT_u32Deinit - "
                                "OSAL_s32EventDelete ERROR 0x%08X",
                                (unsigned int)u32Err);
    }
    /* we go to state "UnInitialized" */
    garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_UNINITIALIZED;
  } //if(garD[].enPlayState != ACOUSTICOUT_EN_STATE_UNINITIALIZED)

  ACOUSTICOUT_PRINTF_U2("u32DeinitPrivate"
                        " - END u32ErrorCode 0x%08X",
                        u32ErrorCode);
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICOUT_u32IOOpen
*  @brief         Opens the acousticout device "/dev/acousticout/xxx"
*                 where xxx is the stream type to be opened.
*  @param         s32ID
*                   ID of the stream to open
*  @param         szName
*                   Should be OSAL_C_STRING_DEVICE_ACOUSTICOUT+"/xxx"
*  @param         enAccess
*                   File access mode (should always be OSAL_EN_WRITEONLY
*                   for this device although value is ignored for now)
*  @param         pu32FD
*                   for storing the filehandle
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_NOTINITIALIZED
*  @retval        OSAL_E_ALREADYOPENED
*  @retval        OSAL_E_UNKNOWN
************************************************************************/
tU32 ACOUSTICOUT_u32IOOpen(tS32 s32ID, tCString szName,
                           OSAL_tenAccess enAccess, tPU32 pu32FD,
                           tU16 appid)
{
  int iLen;
  const char *pcszShortDeviceName;
  const char *pcszLongDeviceName;
  char szFullOsalDeviceName[ACOUSTICOUT_ALSA_DEVICE_NAME_BUFFER_SIZE];
  size_t nShortLen;
  size_t nLongLen;
  size_t nFullLen;


  giCanceledCounter=0;

  vTraceAcousticOutInfo(TR_LEVEL_USER_2, EN_ACOUT_OPEN,
                        "enter",
                        (tU32)s32ID,
                        (tU32)enAccess,
                        pu32FD != NULL ? (tU32)(*pu32FD) : (tU32)-1,
                        (tU32)appid);


  pcszShortDeviceName = NULL == szName ? "" : szName;
  nShortLen = strlen(pcszShortDeviceName);

  // get osal base device name, append "/szName"
  switch(s32ID)
  {
  case OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH:
    pcszLongDeviceName = OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH;
    break;
  case OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC:
    pcszLongDeviceName = OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_MUSIC;
    break;
  default:  //fall back: s32ID is used as Index!
    s32ID = (tS32)OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH;
    pcszLongDeviceName = OSAL_C_STRING_DEVICE_ACOUSTICOUT_IF_SPEECH;
    break;
  } //switch(s32ID)

  nLongLen  = strlen(pcszLongDeviceName); 
  nFullLen  = nLongLen + nShortLen + 2;  //+2: trailing zero and a '/'
  if(nFullLen <= ACOUSTICOUT_ALSA_DEVICE_NAME_BUFFER_SIZE)
  {
    strcpy(szFullOsalDeviceName, pcszLongDeviceName);
    strcat(szFullOsalDeviceName,"/");
    strcat(szFullOsalDeviceName,pcszShortDeviceName);
  }
  else //if(nFullLen <= ACOUSTICOUT_ALSA_DEVICE_NAME_BUFFER_SIZE)
  {
    //error: buffer to short
    vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_ACOUT_OPEN, OSAL_E_UNKNOWN,
                           "2short", 
                           (tU32)ACOUSTICOUT_ALSA_DEVICE_NAME_BUFFER_SIZE,
                           (tU32)nFullLen,
                           (tU32)nLongLen,
                           (tU32)nShortLen);
    return OSAL_E_UNKNOWN;
  } //else //if(nFullLen <= ACOUSTICOUT_ALSA_DEVICE_NAME_BUFFER_SIZE)

  s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

  ACOUSTICOUT_PRINTF_U2("ACOUSTICOUT_u32IOOpen FullOsalDeviceName <%s>\n",
                        szFullOsalDeviceName);

  u32InitPrivate(s32ID);

  memset(garD[s32ID].szAlsaDeviceName,
         0,
         ACOUSTICOUT_ALSA_DEVICE_NAME_BUFFER_SIZE);
  iLen = iSlash2CamelCase(szFullOsalDeviceName,
                          garD[s32ID].szAlsaDeviceName,
                          ACOUSTICOUT_ALSA_DEVICE_NAME_BUFFER_SIZE);
  ACOUSTICOUT_PRINTF_U2("ACOUSTICOUT_u32IOOpen CamelCase <%s>, Len %d\n",
                        garD[s32ID].szAlsaDeviceName, iLen);
  if(iLen <= 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s: cannot convert name: [%s]",
                         __FUNCTION__,
                         szFullOsalDeviceName);
    return OSAL_E_UNKNOWN;
  } //if(iLen <= 0)

  /* If the driver is not yet initialized, the open request fails
  (OSAL_E_NOTINITIALIZED) */
  if(garD[s32ID].enPlayState < ACOUSTICOUT_EN_STATE_INITIALIZED)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s: wrong playstate [%d]",
                             __FUNCTION__,
                             (int)garD[s32ID].enPlayState);
    return OSAL_E_NOTINITIALIZED;
  }

  /* If the stream is already opened, the open request fails
  (OSAL_E_ALREADYOPENED) */
  if(garD[s32ID].enPlayState > ACOUSTICOUT_EN_STATE_INITIALIZED)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s: already open state [%d]",
                             __FUNCTION__,
                             (int)garD[s32ID].enPlayState);
    return OSAL_E_ALREADYOPENED;
  }

  /* Initialize internal state variables to defaults */
  garD[s32ID].rPCMConfig.enSampleFormat =
                                     ACOUSTICOUT_C_EN_DEFAULT_PCM_SAMPLEFORMAT;
  garD[s32ID].rPCMConfig.nSampleRate    =
                                      ACOUSTICOUT_C_U32_DEFAULT_PCM_SAMPLERATE;

  if(s32ID == EN_ACOUSTICOUT_DEVID_SPEECH)
  {
    garD[s32ID].rPCMConfig.u16NumChannels =
                                  ACOUSTICOUT_C_U8_DEFAULT_NUM_CHANNELS_SPEECH;
  }
  else
  {
    garD[s32ID].rPCMConfig.u16NumChannels =
                                   ACOUSTICOUT_C_U8_DEFAULT_NUM_CHANNELS_MUSIC;
  }

  garD[s32ID].enCodec = ACOUSTICOUT_C_EN_DEFAULT_DECODER;

  garD[s32ID].anBufferSize[OSAL_EN_ACOUSTIC_DEC_PCM] = 
                                     ACOUSTICOUT_C_U32_DEFAULT_BUFFER_SIZE_PCM;

  garD[s32ID].u32WriteTimeout = ACOUSTICOUT_C_U32_DEFAULT_WRITE_TIMEOUT;

  /* init Callback data */
  garD[s32ID].rCallback.pfEvCallback = NULL;
  garD[s32ID].rCallback.pvCookie     = NULL;

  /* clear all remaining notification events */
  (tVoid)u32ClearEvent(&garD[s32ID], ACOUSTICOUT_EN_EVENT_MASK_NOTI_ANY);

  garD[s32ID].pAlsaPCM       = NULL;

  /* initialize hw_can_pause with default value */
  garD[s32ID].hw_can_pause = ACOUSTICOUT_C_U32_HW_CANNOT_BE_PAUSED;

  /* new state is "Stopped" */
  garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_STOPPED;

  garD[s32ID].bPreFillFirstPeriod = TRUE;

  /* return filehandle */
  if(pu32FD != NULL)
    (*pu32FD) = (tU32)garD[s32ID].enFileHandle;

#ifdef ACOUSTICOUT_FADE_IF_ABORT
  //init cache
  fadecache.pCache = NULL;
  fadecache.fill = 0;
  fadecache.size = 0;
  fadecache.fadeLen = 0;
#endif //#ifdef ACOUSTICOUT_FADE_IF_ABORT  
  
  vTraceAcousticOutInfo(TR_LEVEL_USER_2, EN_ACOUT_OPEN, "exitok",
                        (tU32)s32ID, (tU32)enAccess, OSAL_E_NOERROR, 0);
  return OSAL_E_NOERROR;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICOUT_u32IOClose
*  @brief         Closes the acousticout device.
*                 When the device is reopened afterwards, all configured
*                 parameters, registered callbacks will start with the
*                 internal defaults again.
*  @param         s32ID  stream ID
*  @param         u32FD  file handle
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_DOESNOTEXIST
*  @retval        OSAL_E_UNKNOWN
************************************************************************/
tU32 ACOUSTICOUT_u32IOClose(tS32 s32ID, tU32 u32FD)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

  vTraceAcousticOutInfo(TR_LEVEL_USER_2, EN_ACOUT_CLOSE,
                        "enter", (tU32)s32ID, (tU32)u32FD, 0, 0);

  /* If the device is not open, the close request will fail
  (OSAL_E_DOESNOTEXIST) */
  if(garD[s32ID].enPlayState < ACOUSTICOUT_EN_STATE_STOPPED)
  {
    vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_ACOUT_CLOSE, OSAL_E_DOESNOTEXIST,
                           "noexist", (tU32)s32ID, u32FD, 0, 0);
    u32Ret = OSAL_E_DOESNOTEXIST;
  }

  /* cancel any thread blocked in WAITEVENT iocontrol */
  u32SetEvent(&garD[s32ID], ACOUSTICOUT_EN_EVENT_MASK_NOTI_CANCEL);
  /* threads blocked in write will be cancelled in the abort call below */

  /* flush buffers and clean up */
#ifdef ACOUSTICOUT_USE_DRAIN_AS_ABORT
  (tVoid)u32OutputDeviceStop(s32ID);
#else //#ifdef ACOUSTICOUT_USE_DRAIN_AS_ABORT
  #ifdef ACOUSTICOUT_FADE_IF_ABORT
    if(giCanceledCounter != 0)
    {
       ACOUSTICOUT_PRINTF_U1("%s:CLOSE,  +++++++++++++++++++++++++++++++++++++++++++", __FUNCTION__);
       ACOUSTICOUT_PRINTF_U1("%s:CLOSE,  ABORT INSTEAD OF FADE giCanceledCounter [%d]", __FUNCTION__, (int)giCanceledCounter);
       ACOUSTICOUT_PRINTF_U1("%s:CLOSE,  +++++++++++++++++++++++++++++++++++++++++++", __FUNCTION__);
       (tVoid)u32AbortStream(s32ID, u32FD);
    }
    else
    {
      //vFadeCache(s32ID);
      vDrainFadeCache(s32ID);
      (tVoid)u32OutputDeviceStop(s32ID);
    }

  #else //#ifdef ACOUSTICOUT_FADE_IF_ABORT
    (tVoid)u32AbortStream(s32ID, u32FD);
  #endif //else //#ifdef ACOUSTICOUT_FADE_IF_ABORT
#endif //#else //#ifdef ACOUSTICOUT_USE_DRAIN_AS_ABORT  
  

  garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_INITIALIZED;

  if(OSAL_E_NOERROR != u32UnInitAlsa(s32ID))
  {
    vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_ACOUT_CLOSE, OSAL_E_UNKNOWN,
                           "auninit", 0, 0, 0, 0);
    u32Ret = OSAL_E_UNKNOWN;
  }
  u32DeinitPrivate(s32ID);
  vDebugFileClose(s32ID);
  vTraceAcousticOutInfo(TR_LEVEL_USER_2, EN_ACOUT_CLOSE,
                        "exit", (tU32)s32ID, u32Ret, 0, 0);
  return u32Ret;
}

/********************************************************************/ /**
*  FUNCTION:      ACOUSTICOUT_u32IOControl
*  @brief         Executes IO-Control for the driver
*  @param[in]     s32ID
*                   stream ID
*  @param[in]     u32FD
*                   file handle
*  @param[in]     s32Fun
*                   IO-Control to be executed
*  @param[in,out] s32Arg
*                   extra argument (depending on s32Fun)
*
*  @return        OSAL error code
*  @retval        OSAL_E_NOERROR
*  @retval        OSAL_E_INVALIDVALUE
*  @retval        OSAL_E_NOTSUPPORTED
************************************************************************/
tU32 ACOUSTICOUT_u32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_ACOUT_IOCTRL,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Fun, (tU32)s32Arg);

  switch(s32Fun)
  {
  /*---------------------------------------------------------------------*/
  /* The driver version is returned                                      */
  /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_VERSION:
    {
      u32ErrorCode = IOCtrl_Version(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Writes audio data to the driver by providing a pointer to the data  */
    /* (including additional control information) to the driver.           */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_EXTWRITE:
    {
      u32ErrorCode = IOCtrl_Extwrite(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* This IOControl registers a callback function that is used for       */
    /* notification about certain events.                                  */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_REG_NOTIFICATION:
    {
      IOCtrl_RegNotification(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Blocks until new event occurs (or until timeout happens),           */
    /* this is an alternative to registering a notification callback.      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_WAITEVENT:
    {
      u32ErrorCode = IOCtrl_WaitEvent(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Retrieves the codec capabilities                                    */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_DECODER:
    {
      u32ErrorCode = IOCtrl_GetSuppDecoder(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Signals an additional audio decoder which will be required after    */
    /* the currently configured codec.                                     */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_NEXTNEEDEDDECODER:
    {
      u32ErrorCode = IOCtrl_NextNeededDecoder(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Gets acoustic decoder to be used for the next audio buffer.         */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETDECODER:
    {
      u32ErrorCode = IOCtrl_GetDecoder(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Sets new acoustic decoder to be used for the next audio buffer.     */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETDECODER:
    {
      u32ErrorCode = IOCtrl_SetDecoder(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Retrieves the sample rate cap. for a specified acoustic codec.      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_SAMPLERATE:
    {
      u32ErrorCode = IOCtrl_GetSuppSamplerate(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Gets the sample rate for a specified acoustic codec.                */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSAMPLERATE:
    {
      u32ErrorCode = IOCtrl_GetSamplerate(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Sets the sample rate for a specified acoustic codec.                */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLERATE:
    {
      u32ErrorCode = IOCtrl_SetSamplerate(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Retrieves the sample format cap. for a specified acoustic codec.    */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_SAMPLEFORMAT:
    {
      u32ErrorCode = IOCtrl_GetSuppSampleformat(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Gets the format of the samples for a specified acoustic codec.      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSAMPLEFORMAT:
    {
      u32ErrorCode = IOCtrl_GetSampleformat(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Sets the format of the samples for a specified acoustic codec.      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETSAMPLEFORMAT:
    {
      u32ErrorCode = IOCtrl_SetSampleformat(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Retrieves the channel num cap.                                      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_CHANNELS:
    {
      u32ErrorCode = IOCtrl_GetSuppChannels(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Gets the number of channels to be used for the audio stream.        */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETCHANNELS:
    {
      u32ErrorCode = IOCtrl_GetChannels(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Sets the number of channels to be used for the audio stream.        */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETCHANNELS:
    {
      u32ErrorCode = IOCtrl_SetChannels(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Retrieves the buffer size capabilities                              */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETSUPP_BUFFERSIZE:
    {
      u32ErrorCode = IOCtrl_GetSuppBuffersize(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Gets the size of buffers to be used for the audio stream.           */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETBUFFERSIZE:
    {
      u32ErrorCode = IOCtrl_GetBuffersize(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Sets the size of buffers to be used for the audio stream.           */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETBUFFERSIZE:
    {
      u32ErrorCode = IOCtrl_SetBuffersize(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Starts the audio transfer. After starting the stream, the           */
    /* client  is required to provide audio data by calling the OSAL write */
    /* operation  continuously.                                            */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_START:
    {
      u32ErrorCode = IOCtrl_Start(s32ID, u32FD, s32Arg);
      if(u32ErrorCode == OSAL_E_NOERROR)
        vSetThreadPrio(s32ID);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Initiates stopping of a running audio transfer.                     */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_STOP:
    {
      u32ErrorCode = IOCtrl_Stop(s32ID, u32FD, s32Arg);
      vRestoreThreadPrio(s32ID);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Aborts (=flushes) running audio output immediately.                  */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_ABORT:
    {
      u32ErrorCode = IOCtrl_Abort(s32ID, u32FD, s32Arg);
      vRestoreThreadPrio(s32ID);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Pauses running audio transfer.                                      */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_PAUSE:
    {
      u32ErrorCode = IOCtrl_Pause(s32ID, u32FD, s32Arg);
      break;
    }

    /*---------------------------------------------------------------------*/
    /* Default case: not supported argument given                          */
    /*---------------------------------------------------------------------*/
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_GETTIME:
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETTIME:
  case OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETERRTHR:
  default:
    {
      ACOUSTICOUT_PRINTF_FATAL("%s: IO unsupported: %d",
                               __FUNCTION__, (int)s32Fun);
      u32ErrorCode = OSAL_E_NOTSUPPORTED;
      break;
    }
  }

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_ACOUT_IOCTRL,
                        "exit",
                        (tU32)s32ID,
                        u32FD,
                        (tU32)s32Fun,
                        u32ErrorCode);
  return u32ErrorCode;
}

/*************************************************************************/ /**
*  FUNCTION:      ACOUSTICOUT_u32IOWrite
*  @brief         Writes audio data to the driver by providing a pointer
*                 to the data. This function uses a posix style write,
*                 therefore it acts as a wrapper to another function,
*                 "u32DoWriteOperation", which supports additional information.
*                 The additional information is not used here and filled
*                 with defaults
*
*  @param[in]     s32ID
*                   stream ID
*  @param[in]     u32FD
*                   file handle
*  @param[in]     pcs8Buffer
*                   Pointer to the buffer where new audio data has
*                   been stored by the client
*  @param[in]     u32Size
*                   Denotes the size of the buffer content (in bytes) for
*                   the buffer pointed to by pcs8Buffer.
*  @param[out]    pu32Written
*                   Number of bytes written to the driver
*
*  @return        OSAL error code
*****************************************************************************/
tU32 ACOUSTICOUT_u32IOWrite(tS32 s32ID, tU32 u32FD, tPCS8 pcs8Buffer,
                            tU32 u32Size, tPU32 pu32Written)
{
  OSAL_trAcousticOutWrite rWriteInfo;
  tU32 u32Ret; 
  tU32 u32BytesWritten = 0;

  s32ID = s32ConvertOsalDeviceID(s32ID); //convert to local ID

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_ACOUT_WRITE,
                        "enter", (tU32)s32ID, (tU32)u32FD,
                        u32Size, (tU32)pu32Written);

  /* init ext write struct with default values for posix-style write */
  rWriteInfo.enBufStatus         = OSAL_EN_ACOUSTIC_DATA_VALID;
  rWriteInfo.nTimeout            = ACOUSTICOUT_C_U32_DEFAULT_WRITE_TIMEOUT;
  rWriteInfo.pvBuffer            = (tPVoid)pcs8Buffer;
  rWriteInfo.u32BufferSize       = u32Size;
  rWriteInfo.u32PhraseStartEvtId = 0;

  if(garD[s32ID].bPreFillFirstPeriod)
  {
    /* Fill the first period with silence */
    garD[s32ID].bPreFillFirstPeriod = FALSE;
    snd_pcm_uframes_t uP  = garD[s32ID].nPeriodSize; 
    (void)u32WriteSilence(s32ID, uP);
  } //if(garD[s32ID].bPreFillFirstPeriod)

  u32Ret = u32DoWriteOperation(s32ID, u32FD, &rWriteInfo,
                               &u32BytesWritten, 1000);

  if(OSAL_E_NOERROR != u32Ret)
  {
    vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_ACOUT_WRITE, u32Ret,
                           "write", (tU32)s32ID, (tU32)u32FD,
                           u32BytesWritten, u32Size);
  }

  if(NULL != pu32Written)
    *pu32Written = u32Size;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_ACOUT_WRITE,
                        "exit", (tU32)s32ID, (tU32)u32FD, u32Size, u32Ret);
  return u32Ret;
}



/********************************************************************/ /**
*  FUNCTION: convertFormatOsalToAlsa     
*  @brief    Converts Osal-format To Alsa-format
*  @return   snd_pcm_format_t ALSA     
************************************************************************/
static snd_pcm_format_t
convertFormatOsalToAlsa(OSAL_tenAcousticSampleFormat nOsalSampleFormat)
{
  snd_pcm_format_t nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;

  switch(nOsalSampleFormat)
  {
  case OSAL_EN_ACOUSTIC_SF_S8:    /*!< signed 8 bit */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S8;
    break;
  case OSAL_EN_ACOUSTIC_SF_S16:   /*!< signed 16 bit: CPU endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S16;
    break;
  case OSAL_EN_ACOUSTIC_SF_S32:   /*!< signed 32 bit: CPU endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S32;
    break;
  case OSAL_EN_ACOUSTIC_SF_F32:   /*!< float (IEEE 754) 32 bit: CPU endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT;
    break;
  case OSAL_EN_ACOUSTIC_SF_S16LE: /*!< signed 16 bit: little endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;
    break;
  case OSAL_EN_ACOUSTIC_SF_S32LE: /*!< signed 32 bit: little endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S32_LE;
    break;
  case OSAL_EN_ACOUSTIC_SF_F32LE: /*!< float (IEEE 754) 32 bit: little endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT_LE;
    break;
  case OSAL_EN_ACOUSTIC_SF_S16BE:  /*!< signed 16 bit: big endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S16_BE;
    break;
  case OSAL_EN_ACOUSTIC_SF_S32BE: /*!< signed 32 bit, big endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_S32_BE;
    break;
  case OSAL_EN_ACOUSTIC_SF_F32BE:  /*!< float (IEEE 754) 32 bit, big endian */ 
    nAlsaSampleFormat = SND_PCM_FORMAT_FLOAT_BE;
    break;
  default:
    ACOUSTICOUT_PRINTF_ERRORS("convertFormatOsalToAlsa "
                              "ERROR: cannot convert OSAL-format (%d)",
                              (int)nOsalSampleFormat);
    nAlsaSampleFormat = SND_PCM_FORMAT_S16_LE;
  } //switch(nOsalSampleFormat)

  ACOUSTICOUT_PRINTF_U2("convertFormatOsalToAlsa : "
                        "convert OSAL %d to Alsa-format %d",
                        (int)nOsalSampleFormat, (int)nAlsaSampleFormat);
  return nAlsaSampleFormat;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Version
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*  @return        OSAL error code
************************************************************************/
static tU32 IOCtrl_Version(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  if((tPS32)s32Arg != NULL)
  {
    vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_VERSION,
                          "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                          (tU32)garD[s32ID].enPlayState);
    /* write version */
    *(tPS32)s32Arg = ACOUSTICOUT_C_S32_IO_VERSION;
    vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_VERSION,
                          "exit", (tU32)s32ID, u32FD, (tU32)s32Arg,
                          (tU32)garD[s32ID].enPlayState);
  }
  else
  {
    vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_IOCTRL_VERSION,
                           OSAL_E_INVALIDVALUE,
                           "invval", (tU32)s32ID, u32FD,
                           (tU32)garD[s32ID].enPlayState, 0);
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Extwrite
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*  @return        OSAL error code
************************************************************************/
static tU32 IOCtrl_Extwrite(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode;
  OSAL_trAcousticOutWrite* pExtWriteInfo;
  tU32 u32BytesWritten = 0;
  (void)u32BytesWritten;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_EXTWRITE,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  pExtWriteInfo = (OSAL_trAcousticOutWrite*)s32Arg;

  if(NULL == pExtWriteInfo)
  {
    /* nullpointer */
    return OSAL_E_INVALIDVALUE;
  }

  if(garD[s32ID].bPreFillFirstPeriod)
  {
    /* Fill the first period with silence */
    garD[s32ID].bPreFillFirstPeriod = FALSE;
    snd_pcm_uframes_t uP  = garD[s32ID].nPeriodSize; 
    (void)u32WriteSilence(s32ID, uP);
  } //if(garD[s32ID].bPreFillFirstPeriod)

  /* OK, do the write operation */
  u32ErrorCode = u32DoWriteOperation(s32ID, u32FD,
                                     pExtWriteInfo,
                                     &u32BytesWritten,
                                     (tS32)pExtWriteInfo->nTimeout);

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_EXTWRITE,
                        "exit", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_RegNotification
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*  @return        OSAL error code
************************************************************************/
static void IOCtrl_RegNotification(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  OSAL_trAcousticOutCallbackReg* pCallback;

  vTraceAcousticOutInfo(TR_LEVEL_USER_2, EN_IOCTRL_REG_NOTIFICATION,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  pCallback = (OSAL_trAcousticOutCallbackReg*)s32Arg;

  if(NULL == pCallback)
  {
    /* unregister callback */
    garD[s32ID].rCallback.pfEvCallback = NULL;
    garD[s32ID].rCallback.pvCookie     = NULL;
  }
  else if(NULL == pCallback->pfEvCallback)
  {
    /* unregister callback */
    garD[s32ID].rCallback.pfEvCallback = NULL;
    garD[s32ID].rCallback.pvCookie     = NULL;
  }
  else
  {
    /* register callback */
    garD[s32ID].rCallback.pfEvCallback =
    (OSAL_tpfAcousticOutEvCallback)pCallback->pfEvCallback;
    garD[s32ID].rCallback.pvCookie =
    (tPVoid)pCallback->pvCookie;
  }

  vTraceAcousticOutInfo(TR_LEVEL_USER_2, EN_IOCTRL_REG_NOTIFICATION,
                        "exit", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_WaitEvent
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*  @return        OSAL error code
************************************************************************/
static tU32 IOCtrl_WaitEvent(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  OSAL_trAcousticOutWaitEvent* pWaitEvent;
  OSAL_tEventMask tWaitEventMask = 0;
  tS32 s32Ret = 0;    
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  vTraceAcousticOutInfo(TR_LEVEL_USER_2, EN_IOCTRL_WAITEVENT,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  pWaitEvent = (OSAL_trAcousticOutWaitEvent*)s32Arg;

  if(NULL == pWaitEvent)
  {
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if (NULL == pWaitEvent)
  {
    (tVoid)OSAL_s32SemaphoreWait(garD[s32ID].hSemClose,
                                 OSAL_C_TIMEOUT_NOBLOCKING);
    s32Ret = OSAL_s32EventWait(garD[s32ID].hOsalEvent,
                               ACOUSTICOUT_EN_EVENT_MASK_NOTI_ANY,
                               OSAL_EN_EVENTMASK_OR,
                               pWaitEvent->nTimeout,
                               &tWaitEventMask);

    (tVoid)OSAL_s32SemaphorePost(garD[s32ID].hSemClose);
    if(OSAL_OK == s32Ret)
    {
      if(tWaitEventMask & ACOUSTICOUT_EN_EVENT_MASK_NOTI_CANCEL)
      {
        /* device was closed during wait, return with cancel */
        giCanceledCounter++;
        u32ErrorCode = OSAL_E_CANCELED;
      }
      else //if ( tWaitEventMask & ACOUSTICOUT_EN_EVENT_MASK_NOTI_CANCEL )
      {
        if(tWaitEventMask & ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED)
        {
          u32ClearEvent(&garD[s32ID], ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED);
          pWaitEvent->enEvent = OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED;
        }
      } //else //if ( tWaitEventMask & ACOUSTICOUT_EN_EVENT_MASK_NOTI_CANCEL )
    }
    else //if(OSAL_OK == s32Ret)
    {
      if(OSAL_E_TIMEOUT == OSAL_u32ErrorCode())
        u32ErrorCode = OSAL_E_TIMEOUT;
      else
        u32ErrorCode = OSAL_E_UNKNOWN;
    } //else //if(OSAL_OK == s32Ret)
  } //else //if (NULL == pWaitEvent)

  vTraceAcousticOutInfo(TR_LEVEL_USER_2, EN_IOCTRL_WAITEVENT,
                        "exit", (tU32)s32ID, u32FD, (tU32)0,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSuppDecoder
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*  @return        OSAL error code
************************************************************************/
static tU32 IOCtrl_GetSuppDecoder(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticCodecCapability* prCodecCap;
  tU32 u32CopyIdx;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_DECODER,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  prCodecCap = (OSAL_trAcousticCodecCapability*)s32Arg;
  if(NULL == prCodecCap)
  {
    u32ErrorCode =  OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == prCodecCap)
  {
    /* OK, get codec capabilities */
    for(u32CopyIdx = 0;
       (u32CopyIdx < prCodecCap->u32ElemCnt) &&
       (u32CopyIdx < NUM_ARRAY_ELEMS(aenSupportedCodecs));
       u32CopyIdx++)
    {
      prCodecCap->penCodecs[u32CopyIdx] = aenSupportedCodecs[u32CopyIdx];
    }
    prCodecCap->u32ElemCnt = NUM_ARRAY_ELEMS(aenSupportedCodecs);
    prCodecCap->u32MaxCodecCnt = (tU32)ACOUSTICOUT_C_U8_MAXCODECCNT;
  } //else //if(NULL == prCodecCap)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_DECODER,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_NextNeededDecoder
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*  @return        OSAL error code
************************************************************************/
static tU32 IOCtrl_NextNeededDecoder(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOACCESS;
  OSAL_tenAcousticCodec enNeededCodec;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_NEXTNEEDEDDECODER,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  enNeededCodec = (OSAL_tenAcousticCodec)s32Arg;
  if(bIsCodecValid(enNeededCodec))
    u32ErrorCode = OSAL_E_NOERROR;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_NEXTNEEDEDDECODER,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetDecoder
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl

*  @return        OSAL error code
************************************************************************/
static tU32 IOCtrl_GetDecoder(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_tenAcousticCodec *penCodec;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETDECODER,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);
  penCodec = (OSAL_tenAcousticCodec*)s32Arg;
  if(NULL == penCodec)
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  else //if(NULL == penCodec)
    *penCodec = garD[s32ID].enCodec;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETDECODER,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_SetDecoder
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*  @return        OSAL error code
************************************************************************/
static tU32 IOCtrl_SetDecoder(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_tenAcousticCodec enCodec;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETDECODER,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);
  enCodec = (OSAL_tenAcousticCodec)s32Arg;
  if(bIsCodecValid(enCodec))
    garD[s32ID].enCodec = enCodec;
  else
    u32ErrorCode = OSAL_E_NOACCESS;  /* needed decoder not supported */

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETDECODER,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSuppSamplerate
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*  @return        OSAL error code
************************************************************************/
static tU32 IOCtrl_GetSuppSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleRateCapability* prSampleRateCap;
  tU32 u32CopyIdx;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_SAMPLERATE,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  prSampleRateCap = (OSAL_trAcousticSampleRateCapability*)s32Arg;
  if(NULL == prSampleRateCap)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == prSampleRateCap)
  {
    if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleRateCap->enCodec)
    {
      /* sample rate only configurable for PCM */
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
    else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleRateCap->enCodec)
    {
      /* OK, get samplerate capabilities */
      for(u32CopyIdx = 0;
         (u32CopyIdx < prSampleRateCap->u32ElemCnt) &&
         (u32CopyIdx < NUM_ARRAY_ELEMS(anSupportedSampleratesFrom));
         u32CopyIdx++
         )
      {
        prSampleRateCap->pnSamplerateFrom[u32CopyIdx] = anSupportedSampleratesFrom[u32CopyIdx];
        prSampleRateCap->pnSamplerateTo[u32CopyIdx] = anSupportedSampleratesTo[u32CopyIdx];
      }
      prSampleRateCap->u32ElemCnt = NUM_ARRAY_ELEMS(anSupportedSampleratesFrom);
    } //else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleRateCap->enCodec)
  } //else //if(NULL == prSampleRateCap)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_SAMPLERATE,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSamplerate
*  @brief         
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*  @return        OSAL error code
************************************************************************/
static tU32 IOCtrl_GetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleRateCfg* prSampleRateCfg;
  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSAMPLERATE,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  prSampleRateCfg = (OSAL_trAcousticSampleRateCfg*)s32Arg;
  if(NULL == prSampleRateCfg)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == prSampleRateCfg)
  {
    if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleRateCfg->enCodec)
    {
      /* sample rate only configurable for PCM */
      u32ErrorCode =  OSAL_E_INVALIDVALUE;
    }
    else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleRateCfg->enCodec)
    {
      /* OK, get samplerate */
      prSampleRateCfg->nSamplerate = garD[s32ID].rPCMConfig.nSampleRate;
    } //else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleRateCfg->enCodec)
  } //else //if(NULL == prSampleRateCfg)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSAMPLERATE,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_SetSamplerate
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
*  - 30.07.2012 Niyatha Rao (nro2kor)
*  	 Included additional error checks
************************************************************************/
static tU32 IOCtrl_SetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleRateCfg* prSampleRateCfg;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETSAMPLERATE,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  prSampleRateCfg = (OSAL_trAcousticSampleRateCfg*)s32Arg;

  if(NULL == prSampleRateCfg)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == prSampleRateCfg)
  {
    if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleRateCfg->enCodec)
    {
      /* sample rate only configurable for PCM */
      u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
    }
    /*Checks whether the passed sample rate is supported or not*/
    else if(bIsSamplerateValid(prSampleRateCfg->nSamplerate))
    {
      garD[s32ID].rPCMConfig.nSampleRate = prSampleRateCfg->nSamplerate;
    }
    else
    {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    } //else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleRateCfg->enCodec)
  } //else //if(NULL == prSampleRateCfg)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETSAMPLERATE,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSuppSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_GetSuppSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleFormatCapability* prSampleFormatCap;
  tU32 u32CopyIdx;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_SAMPLEFORMAT,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  prSampleFormatCap = (OSAL_trAcousticSampleFormatCapability*)s32Arg;
  if(NULL == prSampleFormatCap)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == prSampleFormatCap)
  {
    if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleFormatCap->enCodec)
    {
      /* sample format only configurable for PCM */
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
    else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleFormatCap->enCodec)
    {
      /* OK, get sampleformat capabilities */
      for(u32CopyIdx = 0;
         (u32CopyIdx < prSampleFormatCap->u32ElemCnt) &&
         (u32CopyIdx < NUM_ARRAY_ELEMS(aenSupportedSampleformats));
         u32CopyIdx++)
      {
        prSampleFormatCap->penSampleformats[u32CopyIdx] = aenSupportedSampleformats[u32CopyIdx];
      }
      prSampleFormatCap->u32ElemCnt = NUM_ARRAY_ELEMS(aenSupportedSampleformats);
    } //else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleFormatCap->enCodec)
  } //else //if(NULL == prSampleFormatCap)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_SAMPLEFORMAT,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_GetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleFormatCfg* prSampleFormatCfg;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSAMPLEFORMAT,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  prSampleFormatCfg = (OSAL_trAcousticSampleFormatCfg*)s32Arg;

  if(NULL == prSampleFormatCfg)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == prSampleFormatCfg)
  {
    if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleFormatCfg->enCodec)
    {
      /* sample format only configurable for PCM */
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
    else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleFormatCfg->enCodec)
    {
      /* OK, get sampleformat */
      prSampleFormatCfg->enSampleformat = garD[s32ID].rPCMConfig.enSampleFormat;
    } //else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleFormatCfg->enCodec)
  } //else //if(NULL == prSampleFormatCfg)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSAMPLEFORMAT,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_SetSampleformat
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_SetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticSampleFormatCfg* prSampleFormatCfg;
  prSampleFormatCfg = (OSAL_trAcousticSampleFormatCfg*)s32Arg;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETSAMPLEFORMAT,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  if(NULL == prSampleFormatCfg)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == prSampleFormatCfg)
  {
    ACOUSTICOUT_PRINTF_U2("s32IOCtrl_SetSampleformat"
                          " PlayState %d"
                          " codec %u"
                          " SampleFmt %u",
                          (int)garD[s32ID].enPlayState,
                          (unsigned int)prSampleFormatCfg->enCodec,
                          (unsigned int)prSampleFormatCfg->enSampleformat
                         );
    if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleFormatCfg->enCodec)
    {
      /* sample format only configurable for PCM */
      u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
    }
    else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleFormatCfg->enCodec)
    {
      if(bIsSampleformatValid(prSampleFormatCfg->enSampleformat))
      {
        /* OK, set sampleformat */
        garD[s32ID].rPCMConfig.enSampleFormat = prSampleFormatCfg->enSampleformat;
      }
      else
      {
        u32ErrorCode = OSAL_E_INVALIDVALUE;
      }
    } //else //if(OSAL_EN_ACOUSTIC_DEC_PCM != prSampleFormatCfg->enCodec)
  } //else //if(NULL == prSampleFormatCfg)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETSAMPLEFORMAT,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSuppChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_GetSuppChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticChannelCapability* prChanCap;
  tU32 u32CopyIdx;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_CHANNELS,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);
  prChanCap = (OSAL_trAcousticChannelCapability*)s32Arg;
  if(NULL == prChanCap)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == prChanCap)
  {
    /* OK, get channel num capabilities */
    for(u32CopyIdx = 0;
       (u32CopyIdx < prChanCap->u32ElemCnt) &&
       (u32CopyIdx < NUM_ARRAY_ELEMS(au16SupportedChannelnums));
       u32CopyIdx++)
    {
      prChanCap->pu32NumChannels[u32CopyIdx] = au16SupportedChannelnums[u32CopyIdx];
    }
    prChanCap->u32ElemCnt = NUM_ARRAY_ELEMS(au16SupportedChannelnums);
  } //else //if(NULL == prChanCap)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_CHANNELS,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_GetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  tPU16 pu16NumChannels;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETCHANNELS,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  pu16NumChannels = (tPU16)s32Arg;

  if(NULL == pu16NumChannels)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == pu16NumChannels)
  {
    /* OK, get number of channels */
    *pu16NumChannels = garD[s32ID].rPCMConfig.u16NumChannels;

  } //else //if(NULL == pu16NumChannels)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETCHANNELS,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_SetChannels
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_SetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  tU16 u16NumChannels;
  u16NumChannels = (tU16)s32Arg;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETCHANNELS,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  if(ACOUSTICOUT_EN_STATE_STOPPED != garD[s32ID].enPlayState)
  {
    /* stream currently active, set not available */
    u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
  }
  else //if(ACOUSTICOUT_EN_STATE_STOPPED != garD[s32ID].enPlayState)
  {
    if(bIsChannelnumValid(u16NumChannels))
    {
      /* OK, set number of channels */
      garD[s32ID].rPCMConfig.u16NumChannels = u16NumChannels;
    }
    else
    {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
  } //else //if(ACOUSTICOUT_EN_STATE_STOPPED != garD[s32ID].enPlayState)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETCHANNELS,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);

  return u32ErrorCode;
}

/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetSuppBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_GetSuppBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticBufferSizeCapability* prBufferSizeCap;
  tU32 u32CopyIdx;
  tPCU32 pcu32SupArray = NULL;
  tU32 u32SupArrayCnt = 0;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_BUFFERSIZE,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  prBufferSizeCap = (OSAL_trAcousticBufferSizeCapability*)s32Arg;

  if(NULL == prBufferSizeCap)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == prBufferSizeCap)
  {

    switch(prBufferSizeCap->enCodec)
    {
    case OSAL_EN_ACOUSTIC_DEC_PCM:
      pcu32SupArray = au32SupportedBuffersizesPCM;
      u32SupArrayCnt = NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM);
      break;

    case OSAL_EN_ACOUSTIC_DEC_MP3:
      u32ErrorCode = OSAL_E_NOTSUPPORTED;
      break;

    case OSAL_EN_ACOUSTIC_DEC_AMRWB:
      u32ErrorCode = OSAL_E_NOTSUPPORTED;
      break;

    default:
      u32ErrorCode = OSAL_E_INVALIDVALUE;
      break;
    }

    if(NULL != pcu32SupArray)
    {
      /* OK, get channel num capabilities */
      for(u32CopyIdx = 0;
         (u32CopyIdx < prBufferSizeCap->u32ElemCnt) &&
         (u32CopyIdx < u32SupArrayCnt);
         u32CopyIdx++)
      {
        prBufferSizeCap->pnBuffersizes[u32CopyIdx] = pcu32SupArray[u32CopyIdx];
      }

      prBufferSizeCap->u32ElemCnt = u32SupArrayCnt;
    }

  } //else //if(NULL == prBufferSizeCap)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETSUPP_BUFFERSIZE,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_GetBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_GetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticBufferSizeCfg* pBufferSizeCfg;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETBUFFERSIZE,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  pBufferSizeCfg = (OSAL_trAcousticBufferSizeCfg*)s32Arg;

  if(NULL == pBufferSizeCfg)
  {
    /* nullpointer */
    u32ErrorCode = OSAL_E_INVALIDVALUE;
  }
  else //if(NULL == pBufferSizeCfg)
  {
    /* OK, get buffersize */
    if(bIsCodecValid(pBufferSizeCfg->enCodec))
    {
      pBufferSizeCfg->nBuffersize = 
                           garD[s32ID].anBufferSize[pBufferSizeCfg->enCodec];
    }
    else
    {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }

    ACOUSTICOUT_PRINTF_U2("s32IOCtrl_GetBuffersize"
                          " PlayState %d"
                          " Codec %u"
                          " BUffersize %u",
                          (int)garD[s32ID].enPlayState,
                          (unsigned int)pBufferSizeCfg->enCodec,
                          (unsigned int)pBufferSizeCfg->nBuffersize
                         );
  } //else //if(NULL == pBufferSizeCfg)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_GETBUFFERSIZE,
                        "exit", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_SetBuffersize
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_SetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  OSAL_trAcousticBufferSizeCfg* pBufferSizeCfg;
  pBufferSizeCfg = (OSAL_trAcousticBufferSizeCfg*)s32Arg;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETBUFFERSIZE,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);


  if(ACOUSTICOUT_EN_STATE_STOPPED != garD[s32ID].enPlayState)
  {
    /* stream currently running, set not allowed */
    u32ErrorCode = OSAL_E_TEMP_NOT_AVAILABLE;
  }
  else //if(ACOUSTICOUT_EN_STATE_STOPPED != garD[s32ID].enPlayState)
  {
    if(NULL == pBufferSizeCfg)
    {
      /* nullpointer */
      u32ErrorCode = OSAL_E_INVALIDVALUE;
    }
    else //if(NULL == pBufferSizeCfg)
    {
      ACOUSTICOUT_PRINTF_U2("s32IOCtrl_SetBuffersize"
                            " PlayState %d"
                            " Codec %u"
                            " BUffersize %u",
                            (int)garD[s32ID].enPlayState,
                            (unsigned int)pBufferSizeCfg->enCodec,
                            (unsigned int)pBufferSizeCfg->nBuffersize
                           );

      if(bIsCodecValid (pBufferSizeCfg->enCodec) &&
         bIsBuffersizeValid(pBufferSizeCfg->nBuffersize,
                            pBufferSizeCfg->enCodec))
      {
        /* OK, set buffersize */
        garD[s32ID].anBufferSize[pBufferSizeCfg->enCodec]
                                            = pBufferSizeCfg->nBuffersize;
      }
      else
      {
        u32ErrorCode = OSAL_E_INVALIDVALUE;
      }
    } //else //if(NULL == pBufferSizeCfg)
  } //else //if(ACOUSTICOUT_EN_STATE_STOPPED != garD[s32ID].enPlayState)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_SETBUFFERSIZE,
                        "exit", (tU32)s32ID, u32FD, u32ErrorCode,
                        (tU32)garD[s32ID].enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Start
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_Start(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_START,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);
  
  vDebugFileOpen(s32ID);
  
  switch(garD[s32ID].enPlayState)
  {
  case ACOUSTICOUT_EN_STATE_PAUSED:
    {
      int iErr;
      /* restore play state before pause (either prepare or active) */
      garD[s32ID].enPlayState = garD[s32ID].enPlayStateBeforePause;

      u32ClearEvent (&garD[s32ID], ACOUSTICOUT_EN_EVENT_MASK_NOTI_ANY);

      iErr = snd_pcm_pause(garD[s32ID].pAlsaPCM, 0);
      if(iErr < 0)
      {
        u32ErrorCode = OSAL_E_UNKNOWN;
        vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_IOCTRL_START,
                               (tU32)iErr, "pcmpaus1",
                               u32ErrorCode, (tU32)iErr, 0, 0);
        ACOUSTICOUT_PRINTF_ERRORS("snd_pcm_pause returns: <%s>",
                                  snd_strerror(iErr));

        vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, FALSE);
      } //if(iErr < 0)
      break;
    }

  case ACOUSTICOUT_EN_STATE_STOPPED:
    {
      /* open alsa*/
      if(OSAL_E_NOERROR != u32InitAlsa(s32ID))
      {
        ACOUSTICOUT_PRINTF_ERRORS("%s: unable to start ALSA", __FUNCTION__);
        u32UnInitAlsa(s32ID);
        u32ErrorCode = (tS32)OSAL_E_NOACCESS;
      }

      u32ClearEvent(&garD[s32ID], ACOUSTICOUT_EN_EVENT_MASK_NOTI_ANY);

      /* new state is "Prepare" */
      garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_PREPARE;

      break;
    }

  case ACOUSTICOUT_EN_STATE_PREPARE:
  case ACOUSTICOUT_EN_STATE_ACTIVE:
    {
      /* if already in prepare state or running, there is nothing to do */
      break;
    }

  default:
    {
      vTraceAcousticOutError(TR_LEVEL_ERRORS,  EN_IOCTRL_START, OSAL_E_UNKNOWN,
                             "state",(tU32)garD[s32ID].enPlayState, 0, 0, 0);

      u32ErrorCode = OSAL_E_UNKNOWN;

      break;
    }
  }

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_START,
                        "exit", (tU32)s32ID, u32ErrorCode,
                        (tU32)0,
                        (tU32)garD[s32ID].enPlayState);

  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Stop
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_Stop(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_STOP,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);

  switch(garD[s32ID].enPlayState)
  {
  case ACOUSTICOUT_EN_STATE_PREPARE:
    {

      /* new state is "Stopping" (draining) */
      garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_STOPPING;
      break;
    }

  case ACOUSTICOUT_EN_STATE_ACTIVE:
    {
      /* We already sent buffers, one buffer might be blocked in
       * a write call. When the write call unblocks, the stop bit
       * will be set in it. Any further write calls will be rejected
       * after the play state is set to stopping.
       */
      garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_STOPPING;

      break;
    }

  case ACOUSTICOUT_EN_STATE_STOPPED:
    /* nothing to do, ignore */
    break;

  default:
    break;
  }

  /* send STOP command to output device */
#ifdef ACOUSTICOUT_FADE_IF_ABORT
   if(giCanceledCounter != 0)
   {
     ACOUSTICOUT_PRINTF_U1("%s:STOP,  ++++++++++++++++++++++++++++++++++++++++++", __FUNCTION__);
     ACOUSTICOUT_PRINTF_U1("%s:STOP,  STOP INSTEAD OF FADE giCanceledCounter [%d]", __FUNCTION__, (int)giCanceledCounter);
     ACOUSTICOUT_PRINTF_U1("%s:STOP,  ++++++++++++++++++++++++++++++++++++++++++", __FUNCTION__);
     u32ErrorCode = u32OutputDeviceStop(s32ID);
   }
   else
   {
     //vFadeCache(s32ID);
     vDrainFadeCache(s32ID);
     u32ErrorCode = u32OutputDeviceStop(s32ID);
   }


#else //#ifdef ACOUSTICOUT_FADE_IF_ABORT
  u32ErrorCode = u32OutputDeviceStop(s32ID);
#endif //else //#ifdef ACOUSTICOUT_FADE_IF_ABORT

  garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_STOPPED;
  /* Notify "audio stopped" */
  /* notification via callback */
  if(NULL != garD[s32ID].rCallback.pfEvCallback)
  {
    ACOUSTICOUT_PRINTF_U2("s32IOCtrl_Stop: callback"
                          " OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED");
    garD[s32ID].rCallback.pfEvCallback(OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED,
                                       NULL,
                                       garD[s32ID].rCallback.pvCookie);
  }

  /* notification via event */
  {
    ACOUSTICOUT_PRINTF_U2("s32IOCtrl_Stop: notify"
                          " ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED");
    u32SetEvent(&garD[s32ID], ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED);
  }

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_STOP,
                        "exit", (tU32)s32ID, u32ErrorCode,
                        (tU32)0,
                        (tU32)garD[s32ID].enPlayState);

  vDebugFileClose(s32ID);
  u32UnInitAlsa(s32ID);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Abort
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_Abort(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode;


  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_ABORT,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);


#ifdef ACOUSTICOUT_USE_DRAIN_AS_ABORT
  u32ErrorCode = u32OutputDeviceStop(s32ID);
#else //#ifdef ACOUSTICOUT_USE_DRAIN_AS_ABORT
  #ifdef ACOUSTICOUT_FADE_IF_ABORT
   if(giCanceledCounter != 0)
   {
     ACOUSTICOUT_PRINTF_U1("%s:ABORT,  +++++++++++++++++++++++++++++++++++++++++++", __FUNCTION__);
     ACOUSTICOUT_PRINTF_U1("%s:ABORT,  ABORT INSTEAD OF FADE giCanceledCounter [%d]", __FUNCTION__, (int)giCanceledCounter);
     ACOUSTICOUT_PRINTF_U1("%s:ABORT,  +++++++++++++++++++++++++++++++++++++++++++", __FUNCTION__);
     u32ErrorCode = u32AbortStream(s32ID, u32FD);
   }
   else
   {
    vFadeCache(s32ID);
    vDrainFadeCache(s32ID);
    u32ErrorCode = u32OutputDeviceStop(s32ID);
   }


  #else //#ifdef ACOUSTICOUT_FADE_IF_ABORT
    u32ErrorCode = u32AbortStream(s32ID, u32FD);
  #endif //else //#ifdef ACOUSTICOUT_FADE_IF_ABORT
#endif //#else //#ifdef ACOUSTICOUT_USE_DRAIN_AS_ABORT  

  garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_STOPPED;
  /* Notify "audio stopped" */
  /* notification via callback */
  if(NULL != garD[s32ID].rCallback.pfEvCallback)
  {
    ACOUSTICOUT_PRINTF_U2("s32IOCtrl_Abort:"
                          " callback"
                          "ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED");
    garD[s32ID].rCallback.pfEvCallback(
                                      OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED,
                                      NULL,
                                      garD[s32ID].rCallback.pvCookie);
  }

  /* notification via event */
  {
    ACOUSTICOUT_PRINTF_U2("s32IOCtrl_Abort:"
                          " notify "
                          "ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED");
    u32SetEvent(&garD[s32ID],
                ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED);
  }

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_ABORT,
                        "exit", (tU32)s32ID, u32ErrorCode,
                        (tU32)0,
                        (tU32)garD[s32ID].enPlayState);

  vDebugFileClose(s32ID);
  u32UnInitAlsa(s32ID);
  return u32ErrorCode;
}


/********************************************************************/ /**
*  FUNCTION:      IOCtrl_Pause
*
*  @brief         
*
*  @param         s32ID
*                   ID of the stream
*  @param         u32FD
*                   file handle
*  @param         s32Arg
*                   argument data for IOControl
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 11.12.2006 Bernd Schubart, Elektrobit Automotive
*    Code ported from Paramount
************************************************************************/
static tU32 IOCtrl_Pause(tS32 s32ID, tU32 u32FD, tS32 s32Arg)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  int iErr = 0;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_PAUSE,
                        "enter", (tU32)s32ID, u32FD, (tU32)s32Arg,
                        (tU32)garD[s32ID].enPlayState);


  switch(garD[s32ID].enPlayState)
  {
  case ACOUSTICOUT_EN_STATE_PREPARE:
  case ACOUSTICOUT_EN_STATE_ACTIVE:
    {

      /* new state is "Paused" */
      garD[s32ID].enPlayStateBeforePause = garD[s32ID].enPlayState;
      garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_PAUSED;


      if(garD[s32ID].hw_can_pause == ACOUSTICOUT_C_U32_HW_CAN_BE_PAUSED)
      {
        iErr = snd_pcm_pause(garD[s32ID].pAlsaPCM, 1);
        if(iErr < 0)
        {
          u32ErrorCode = OSAL_E_UNKNOWN;
          vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_IOCTRL_PAUSE,
                                 (tU32)iErr, "pcmpaus2",
                                 u32ErrorCode, (tU32)iErr, 0, 0);
          ACOUSTICOUT_PRINTF_ERRORS("snd_pcm_pause returns: <%s>",
                                    snd_strerror(iErr));

          vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, FALSE);
        } //if(iErr < 0)
      }
      else
      {
        u32ErrorCode = OSAL_E_NOTSUPPORTED;
        vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_IOCTRL_PAUSE,
                               (tU32)ACOUSTICOUT_C_U32_HW_CANNOT_BE_PAUSED, "pcmpaus2",
                               u32ErrorCode, (tU32)ACOUSTICOUT_C_U32_HW_CANNOT_BE_PAUSED, 0, 0);
        ACOUSTICOUT_PRINTF_ERRORS("hw does not support pause");

        vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, FALSE);
      }
      break;
    }

  case ACOUSTICOUT_EN_STATE_STOPPED:
  case ACOUSTICOUT_EN_STATE_PAUSED:
  default:
    /* nothing to do */
    break;
  }

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_IOCTRL_PAUSE,
                        "exit", (tU32)s32ID,
                        u32ErrorCode, (tU32)iErr,
                        (tU32)garD[s32ID].enPlayState);

  return u32ErrorCode;
}
/*************************************************************************/ /**
*  FUNCTION:      u32DoWriteOperation
*
*  @brief         worker function for writing data to the acousticout driver,
*                 can be used for posix-style write, as well as for the
*                 EXTWRITE IOCOntrol
*
*  @param         s32ID          stream ID
*  @param         u32FD          file handle
*  @param         prWriteInfo    additional control information
*  @param         pubytesWritten returns written bytes
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 07.12.06 Schubart Bernd, EBA
*    Initial revision.
*****************************************************************************/
static tU32 u32DoWriteOperation(tS32 s32ID, tU32 u32FD,
                                const OSAL_trAcousticOutWrite* prWriteInfo,
                                tPU32 pu32BytesWritten,
                                tS32  s32TimeOut)
{
  tU32 u32Ret;

  (void)u32FD;

  /* checks whether the passed pointer arg or the buffer pointer is ZERO */
  if(!prWriteInfo || !prWriteInfo->pvBuffer)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s: NULL Pointer prWriteInfo %p pvBuffer %p",
                             __FUNCTION__,
                             prWriteInfo,
                             prWriteInfo ? prWriteInfo->pvBuffer : NULL);
    return OSAL_E_INVALIDVALUE;
  }
  
  if(!garD[s32ID].pAlsaPCM)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s: No PCM handle", __FUNCTION__);
    return OSAL_E_NOFILEDESCRIPTOR;
  }

#ifdef ACOUSTICOUT_FADE_IF_ABORT
  u32Ret = u32WriteToFadeCache(s32ID, prWriteInfo->pvBuffer,
                          prWriteInfo->u32BufferSize,
                          pu32BytesWritten,
                          s32TimeOut);
#else //#ifdef ACOUSTICOUT_FADE_IF_ABORT
  u32Ret = u32WriteToAlsa(s32ID, prWriteInfo->pvBuffer,
                          prWriteInfo->u32BufferSize,
                          pu32BytesWritten,
                          s32TimeOut);
#endif //#else #ifdef ACOUSTICOUT_FADE_IF_ABORT
  return u32Ret;
}

/*************************************************************************/ /**
*  FUNCTION:      u32SetEvent
*
*  @brief         Sets specified event(s)
*
*  @param         prDspIf          DSP interface
*  @param         tWaitEventMask   event(s) to be set
*
*  @return        OSAL error code
*****************************************************************************/
static tU32 u32SetEvent(const trACOUSTICOUT_StateData *prStateData,
                        const OSAL_tEventMask tWaitEventMask)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;
  tS32 s32RetVal;

  /* set event */
  s32RetVal = OSAL_s32EventPost(prStateData->hOsalEvent,
                                tWaitEventMask,
                                OSAL_EN_EVENTMASK_OR);


  if((tS32)OSAL_OK != s32RetVal)
  {
    u32ErrorCode = OSAL_u32ErrorCode();

    vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_SET_EVENT,
                           u32ErrorCode,
                           "evset", 0, 0, 0, 0);
  }
  return u32ErrorCode;
}

/*************************************************************************/ /**
*  FUNCTION:      u32ClearEvent
*
*  @brief         Clears specified event(s)
*
*  @param         prDspIf          DSP interface
*  @param         tWaitEventMask   event(s) to be cleared
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 07.09.05 Schedel Robert, 3SOFT
*    Initial revision.
*****************************************************************************/
static tU32 u32ClearEvent(const trACOUSTICOUT_StateData *prStateData,
                          const OSAL_tEventMask tWaitEventMask)
{
  tU32 u32ErrorCode = OSAL_E_NOERROR;

  /* clear event */
  tS32 s32Ret = OSAL_s32EventPost(prStateData->hOsalEvent,
                                  ~tWaitEventMask,
                                  OSAL_EN_EVENTMASK_AND);

  if((tS32)OSAL_OK != s32Ret)
  {
    u32ErrorCode = OSAL_u32ErrorCode();
    vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_CLEAR_EVENT,
                           u32ErrorCode,
                           "evclr", 0, 0, 0, 0);
  }
  return u32ErrorCode;
}


/*************************************************************************/ /**
*  FUNCTION:      bIsCodecValid
*
*  @brief         Checks whether provided codec value is valid
*
*  @param         enCodecs  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
*  - 07.09.05 Schedel Robert, 3SOFT
*    Initial revision.
*****************************************************************************/
static tBool bIsCodecValid(OSAL_tenAcousticCodec enCodecs)
{
  tU8 u8Idx;
  for(u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(aenSupportedCodecs); u8Idx++)
  {
    if(aenSupportedCodecs[u8Idx] == enCodecs)
    {
      return TRUE;
    }
  }

  return FALSE;
}


/*************************************************************************/ /**
*  FUNCTION:      bIsSampleformatValid
*
*  @brief         Checks whether provided sample format value is valid
*
*  @param         enSampleformat  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
*  - 07.09.05 Schedel Robert, 3SOFT
*    Initial revision.
*****************************************************************************/
static tBool bIsSampleformatValid(OSAL_tenAcousticSampleFormat enSampleformat)
{
  tU8 u8Idx;
  for(u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(aenSupportedSampleformats); u8Idx++)
  {
    if(aenSupportedSampleformats[u8Idx] == enSampleformat)
    {
      return TRUE;
    }
  }

  return FALSE;
}


/*************************************************************************/ /**
*  FUNCTION:      bIsSamplerateValid
*
*  @brief         Checks whether provided sample rate value is valid
*
*  @param         nSamplerate  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
*  - 07.09.05 Schedel Robert, 3SOFT
*    Initial revision.
*****************************************************************************/
static tBool bIsSamplerateValid(OSAL_tAcousticSampleRate nSamplerate)
{
  tU8 u8Idx;
  for(u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(anSupportedSampleratesFrom); u8Idx++)
  {
    if((anSupportedSampleratesFrom[u8Idx] <= nSamplerate) &&
       (anSupportedSampleratesTo[u8Idx] >= nSamplerate))
    {
      return TRUE;
    }
  }

  return FALSE;
}


/*************************************************************************/ /**
*  FUNCTION:      bIsChannelnumValid
*
*  @brief         Checks whether provided channel num value is valid
*
*  @param         u16Channelnum  value to be checked
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
*  - 07.09.05 Schedel Robert, 3SOFT
*    Initial revision.
*****************************************************************************/
static tBool bIsChannelnumValid(tU16 u16Channelnum)
{
  tU8 u8Idx;
  for(u8Idx=0; u8Idx < NUM_ARRAY_ELEMS(au16SupportedChannelnums); u8Idx++)
  {
    if(au16SupportedChannelnums[u8Idx] == u16Channelnum)
    {
      return TRUE;
    }
  }

  return FALSE;
}


/*************************************************************************/ /**
*  FUNCTION:      bIsBuffersizeValid
*
*  @brief         Checks whether provided buffer size value is valid
*
*  @param         u32Buffersize  value to be checked
*  @param         enCodec        acoustic codec of buffer
*
*  @return        tBool
*  @retval        TRUE   value is valid
*  @retval        FALSE  value is invalid
*
*  HISTORY:
*
*  - 07.09.05 Schedel Robert, 3SOFT
*    Initial revision.
*****************************************************************************/
static tBool bIsBuffersizeValid(tU32 u32Buffersize, OSAL_tenAcousticCodec enCodec)
{
  tU8 u8Idx;
  tPCU32 pcu32SupArray = NULL;
  tU32 u32SupArrayCnt = 0;

  switch(enCodec)
  {
  case OSAL_EN_ACOUSTIC_DEC_PCM:
    pcu32SupArray = au32SupportedBuffersizesPCM;
    u32SupArrayCnt = NUM_ARRAY_ELEMS(au32SupportedBuffersizesPCM);
    break;

  case OSAL_EN_ACOUSTIC_DEC_MP3:
  case OSAL_EN_ACOUSTIC_DEC_AMRWB:
  default:
    return FALSE;
  }

  for(u8Idx=0; u8Idx < u32SupArrayCnt; u8Idx++)
  {
    if(pcu32SupArray[u8Idx] == u32Buffersize)
    {
      return TRUE;
    }
  }

  return FALSE;
}

/*************************************************************************/ /**
*  FUNCTION:      u32AbortStream
*
*  @brief         Aborts stream immediately
*
*  @param         s32ID              stream ID
*  @param         u32FD              file handle
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 09.02.2007   Schubart Bernd, Elektrobit Automotive
*    Port from Paramount
*****************************************************************************/
#ifndef ACOUSTICOUT_USE_DRAIN_AS_ABORT
static tU32 u32AbortStream(tS32 s32ID, tU32 u32FD)
{
  tU32 u32RetVal;

  (void)u32FD;

  /* send ABORT command to output device */
  u32RetVal = u32OutputDeviceAbort(s32ID);

  /* new state is "Stopped" */
  garD[s32ID].enPlayState = ACOUSTICOUT_EN_STATE_STOPPED;

  /* Notify "audio stopped" */
  /* notification via callback */
  if(NULL != garD[s32ID].rCallback.pfEvCallback)
  {
    garD[s32ID].rCallback.pfEvCallback(OSAL_EN_ACOUSTICOUT_EVAUDIOSTOPPED,
                                       NULL,
                                       garD[s32ID].rCallback.pvCookie);
  }

  /* notification via event*/
  u32SetEvent(&garD[s32ID], ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED);
  u32ClearEvent(&garD[s32ID], ACOUSTICOUT_EN_EVENT_MASK_NOTI_ANY);

  return u32RetVal;
}
#endif //#ifndef ACOUSTICOUT_USE_DRAIN_AS_ABORT

/*************************************************************************/ /**
*  FUNCTION:      u32OutputDeviceStop
*
*  @brief         Sends the STOP command to the output device
*
*  @param         s32ID              stream ID
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 01.03.07 Schubart Bernd, Elektrobit Automotive
*    Initial revision.
*****************************************************************************/
static tU32 u32OutputDeviceStop(tS32 s32ID)
{
  tU32 u32RetVal = OSAL_E_NOERROR;
  int iErr = 0;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_OUTPUTDEVICE_STOP,
                        "enter", (tU32)s32ID, 0, 0, 0);
  if(garD[s32ID].pAlsaPCM != NULL)
  {
    snd_pcm_state_t sndStatus;
    tBool bDoDrain;

    sndStatus = snd_pcm_state(garD[s32ID].pAlsaPCM);
    switch(sndStatus)
    {
    case SND_PCM_STATE_OPEN:
    case SND_PCM_STATE_SETUP:
    case SND_PCM_STATE_DISCONNECTED:
    case SND_PCM_STATE_XRUN:
    case SND_PCM_STATE_SUSPENDED:
      bDoDrain = FALSE;
      break;

    case SND_PCM_STATE_PREPARED:
    case SND_PCM_STATE_RUNNING:
    case SND_PCM_STATE_DRAINING:
    case SND_PCM_STATE_PAUSED:
    default:
      bDoDrain = TRUE;
      break;
    } //switch(sndStatus)

    if(bDoDrain)
    {
      /* Fill the last period with silence */
      snd_pcm_uframes_t uP  = garD[s32ID].nPeriodSize - 
                              garD[s32ID].nPeriodFillLevel;

#ifdef ACOUSTICOUT_FILL_MIN_SILENCE
      // fill a minimum of 4ms of silence
      unsigned int uiSilenceDurationMS = 0;
      if(garD[s32ID].uiRealSampleRate != 0)
        uiSilenceDurationMS = (1000 * uP) / garD[s32ID].uiRealSampleRate;

      if(uiSilenceDurationMS < ACOUSTICOUT_FILL_MIN_SILENCE_DURATION_MS)
        uP += garD[s32ID].nPeriodSize;
#endif //#else //#ifdef ACOUSTICOUT_FILL_MIN_SILENCE

      if(uP > 0)
        (void)u32WriteSilence(s32ID, uP);

      (void)snd_pcm_nonblock(garD[s32ID].pAlsaPCM, 0);
      iErr = snd_pcm_drain(garD[s32ID].pAlsaPCM);
      if(iErr < 0)
      {
        ACOUSTICOUT_PRINTF_ERRORS("snd_pcm_drain returns: [%d] <%s>",
                                  iErr,
                                  snd_strerror(iErr));
        vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, FALSE);
      } //if(iErr < 0)
    } //if(bDoDrain)
  }
  else //if(garD[s32ID].pAlsaPCM != NULL)
  {
    u32RetVal = OSAL_E_NOFILEDESCRIPTOR;
  } //else //if(garD[s32ID].pAlsaPCM != NULL)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_OUTPUTDEVICE_STOP,
                        "exit", (tU32)s32ID, u32RetVal,
                        (tU32)iErr, 0);
  return u32RetVal;
}

/*************************************************************************/ /**
*  FUNCTION:      u32OutputDeviceAbort
*
*  @brief         Sends the ABORT command to the output device
*
*  @param         s32ID              stream ID
*
*  @return        OSAL error code
*
*  HISTORY:
*
*  - 01.03.07 Schubart Bernd, Elektrobit Automotive
*    Initial revision.
*****************************************************************************/
static tU32 u32OutputDeviceAbort(tS32 s32ID)
{
  tU32 u32RetVal = OSAL_E_NOERROR;
  int iErr = -1;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_OUTPUTDEVICE_ABORT,
                        "enter", (tU32)s32ID, 0, 0, 0);
  if(garD[s32ID].pAlsaPCM != NULL)
  {
    snd_pcm_state_t sndStatus;
    tBool bDoDrop;

    sndStatus = snd_pcm_state(garD[s32ID].pAlsaPCM);
    switch(sndStatus)
    {
    case SND_PCM_STATE_OPEN:
    case SND_PCM_STATE_SETUP:
    case SND_PCM_STATE_PREPARED:
    case SND_PCM_STATE_DISCONNECTED:
      bDoDrop = FALSE;
      break;
      /** Running */
    case SND_PCM_STATE_RUNNING:
    case SND_PCM_STATE_DRAINING:
    case SND_PCM_STATE_PAUSED:
    case SND_PCM_STATE_SUSPENDED:
    case SND_PCM_STATE_XRUN:
    default:
      bDoDrop = TRUE;
      break;
    } //switch(sndStatus)


    if(bDoDrop)
    {
      iErr = snd_pcm_drop(garD[s32ID].pAlsaPCM);
      if(iErr < 0)
      {
        u32RetVal = OSAL_E_UNKNOWN;
        vTraceAcousticOutError(TR_LEVEL_ERRORS, EN_OUTPUTDEVICE_ABORT,
                               u32RetVal, "pcmdrop2",
                               u32RetVal, (tU32)iErr, (tU32)sndStatus, 0);
        ACOUSTICOUT_PRINTF_ERRORS("snd_pcm_drop returns: <%s>",
                                  snd_strerror(iErr));
        vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, FALSE);
      }
      else
      {
        u32RetVal = OSAL_E_NOERROR;
      }
    }
    else //if(bDoDrop)
    {
      u32RetVal = OSAL_E_NOERROR;
    } //else //if(bDoDrop)
  }
  else //if(garD[s32ID].pAlsaPCM != NULL)
  {
    u32RetVal = OSAL_E_NOFILEDESCRIPTOR;
  } //else //if(garD[s32ID].pAlsaPCM != NULL)

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_OUTPUTDEVICE_ABORT,
                        "exit", (tU32)s32ID, u32RetVal,
                        (tU32)iErr, 0);
  return u32RetVal;
}

/*************************************************************************/ /**
*  FUNCTION:      u32InitAlsa
*  @brief         Initializes the Alsa system
*  @param         s32ID   stream ID
*  @return        OSAL error code
*****************************************************************************/
static tU32 u32InitAlsa(tS32 s32ID)
{
  int err;
  const char *pcszDevice;
  int iDirection;
  unsigned int uiSampleRate;
  unsigned int uiPeriods;
  snd_pcm_uframes_t framesBufferSize;
  snd_pcm_uframes_t framesBufferSizeIdeal;
  snd_pcm_uframes_t framesPeriodSize;
  snd_pcm_uframes_t framesPeriodSizeIdeal;
  snd_pcm_hw_params_t *hw_params;
  snd_pcm_sw_params_t *swparams;
  snd_pcm_uframes_t uAvailMin;
  snd_pcm_uframes_t uThreshStop;
  snd_pcm_uframes_t uThreshStart;
  snd_pcm_format_t  format;

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_INIT_ALSA,
                        "enter", (tU32)s32ID, 0, 0, 0);

  /* Open PCM device for playback */
  pcszDevice = garD[s32ID].szAlsaDeviceName;
  
  if(garD[s32ID].bAlsaIsOpen)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s: ALSA is open yet (%s)",
                              __FUNCTION__, pcszDevice);
    return OSAL_E_NOERROR;
  } //if(garD[s32ID].bAlsaIsOpen)
  
  err = snd_pcm_open( &garD[s32ID].pAlsaPCM, pcszDevice,
                      SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK);

  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:unable to open pcm device (%s): %s",
                              __FUNCTION__,
                              pcszDevice,
                              snd_strerror(err) );
    if(err == -EBUSY)
    {
      if(bIsAcousticOutTraceActive(TR_LEVEL_ERRORS))
        vDebugSearchPCMOF();
    }
    
    garD[s32ID].pAlsaPCM = NULL;
    return OSAL_E_UNKNOWN;
  } //if(err < 0)

  /* Allocate a hardware parameters object. */
  snd_pcm_hw_params_alloca(&hw_params);/*lint !e717*/

  /* Fill it in with default values. */
  err = snd_pcm_hw_params_any( garD[s32ID].pAlsaPCM, hw_params);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:cannot set default params (%s)",
                              __FUNCTION__,
                              snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }

  /* Set the desired hardware parameters. */
  /* Interleaved mode */
  err = snd_pcm_hw_params_set_access(garD[s32ID].pAlsaPCM, hw_params,
                                     SND_PCM_ACCESS_RW_INTERLEAVED );
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:cannot set access type (%s)", __FUNCTION__,
                              snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICOUT_PRINTF_U2("%s: INFO: access == %u",
                          __FUNCTION__,
                          (unsigned int)SND_PCM_ACCESS_RW_INTERLEAVED);
  } //else //if (err < 0)

  format = convertFormatOsalToAlsa(garD[s32ID].rPCMConfig.enSampleFormat);
  err = snd_pcm_hw_params_set_format(garD[s32ID].pAlsaPCM, hw_params, format);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:cannot set format (%s)", __FUNCTION__,
                              snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICOUT_PRINTF_U2("%s: INFO: OSAL-format == %u",
                          __FUNCTION__,
                          (unsigned int)garD[s32ID].rPCMConfig.enSampleFormat);
  } //else //if (err < 0)

  err = snd_pcm_hw_params_set_channels(garD[s32ID].pAlsaPCM,
                                       hw_params,
                                       garD[s32ID].rPCMConfig.u16NumChannels);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:cannot set channels %u (%s)",
                           __FUNCTION__,
                           (unsigned int)garD[s32ID].rPCMConfig.u16NumChannels,
                           snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICOUT_PRINTF_U2("%s: INFO: channels == %u",
                          __FUNCTION__,
                          (unsigned int)garD[s32ID].rPCMConfig.u16NumChannels);
  } //else //if (err < 0)

  uiSampleRate = garD[s32ID].rPCMConfig.nSampleRate;
  iDirection   = 0;
  err = snd_pcm_hw_params_set_rate_near(garD[s32ID].pAlsaPCM,
                                        hw_params,
                                        &uiSampleRate, &iDirection);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:cannot set rate_near (rate:%u) <%s>",
                              __FUNCTION__,
                              uiSampleRate,
                              snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    garD[s32ID].uiRealSampleRate = uiSampleRate;
    ACOUSTICOUT_PRINTF_U2("%s: INFO: rate_near == %u",
                          __FUNCTION__,
                          uiSampleRate);
  } //else //if (err < 0)

  garD[s32ID].rPCMConfig.nSampleRate = uiSampleRate;

  // try to set period size to 16ms
  // as mentioned by Jens Lorenz 141021
  framesPeriodSizeIdeal = (uiSampleRate * 16) / 1000;
  
  framesPeriodSize = framesPeriodSizeIdeal;
  iDirection = 0;
  err = snd_pcm_hw_params_set_period_size_near(garD[s32ID].pAlsaPCM,
                                               hw_params,
                                               &framesPeriodSize, &iDirection);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s: ERROR: "
                              "cannot set period size (%u frames) (%s)",
                              __FUNCTION__,
                              (unsigned int)framesPeriodSize,
                              snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    garD[s32ID].nPeriodSize = framesPeriodSize;
    garD[s32ID].nPeriodFillLevel  = 0;
    ACOUSTICOUT_PRINTF_U2("%s: INFO: period size == %u (ideal %u), dir %d",
                          __FUNCTION__,
                          (unsigned int)framesPeriodSize,
                          (unsigned int)framesPeriodSizeIdeal,
                          iDirection);
  } //else //if (err < 0)

  // try to set buffer to 3 * real period size
  // as mentioned by Jens Lorenz 141021
  framesBufferSizeIdeal = garD[s32ID].nPeriodSize * 3;
 
  framesBufferSize = framesBufferSizeIdeal;
  err = snd_pcm_hw_params_set_buffer_size_near(garD[s32ID].pAlsaPCM,
                                               hw_params,
                                               &framesBufferSize);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s ERROR: cannot set "
                              "buffer size (%u frames) (%s)",
                              __FUNCTION__,
                              (unsigned int)framesBufferSize, 
                              snd_strerror (err));
    return OSAL_E_UNKNOWN;
  }
  else //if (err < 0)
  {
    ACOUSTICOUT_PRINTF_U2("%s: INFO: buffer == %u, (ideal %u)",
                          __FUNCTION__,
                          (unsigned int)framesBufferSize,
                          (unsigned int)framesBufferSizeIdeal);
  } //else //if (err < 0)
  
  err = snd_pcm_hw_params(garD[s32ID].pAlsaPCM, hw_params);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:unable to set hw parameters: %s",
                              __FUNCTION__,
                              snd_strerror(err) );
    return OSAL_E_UNKNOWN;
  } //if ( err < 0)

  
  // read number of periods
  iDirection = 0;
  uiPeriods  = 2;
  err = snd_pcm_hw_params_get_periods(hw_params, &uiPeriods, &iDirection);
  if(err < 0)
  {
    if(framesPeriodSize > 0)
    {
      uiPeriods = (unsigned int)framesBufferSize /
                  (unsigned int)framesPeriodSize;
    }
    else
    {
      uiPeriods = 0;
    }
    ACOUSTICOUT_PRINTF_U1("%s: INFO: cannot get periods: %s; calculated [%d]",
                              __FUNCTION__,
                              snd_strerror(err), uiPeriods);
  }
  ACOUSTICOUT_PRINTF_U2("%s: INFO: periods == %u, dir %d",
                        __FUNCTION__,
                        uiPeriods,
                        iDirection);

  /*set sw parameters*/
  snd_pcm_sw_params_alloca(&swparams);/*lint !e717*/

  /* get the current swparams */
  err = snd_pcm_sw_params_current(garD[s32ID].pAlsaPCM, swparams);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:Unable to determine "
                              "current swparams for playback: %s",
                              __FUNCTION__,
                              snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }

  //set avail to period size
  uAvailMin = framesPeriodSize;
  err = snd_pcm_sw_params_set_avail_min(garD[s32ID].pAlsaPCM,
                                        swparams, uAvailMin);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:Unable to set avail min %u: %s",
                              __FUNCTION__,
                              (unsigned int)uAvailMin,
                              snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }

  //enable XRUN, set stop threshold to number of periods * period size
  uThreshStop = framesPeriodSize * uiPeriods;
  err = snd_pcm_sw_params_set_stop_threshold(garD[s32ID].pAlsaPCM,
                                             swparams, uThreshStop);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:Unable to set stop_treshold to %u: %s",
                              __FUNCTION__,
                              (unsigned int)uThreshStop,
                              snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }

  //set start threshold one period before end of buffer
  uThreshStart = (uiPeriods - 1) * framesPeriodSize;
  err = snd_pcm_sw_params_set_start_threshold(garD[s32ID].pAlsaPCM,
                                             swparams, uThreshStart);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:Unable to set start_treshold to %u: %s",
                              __FUNCTION__,
                              (unsigned int)uThreshStart,
                              snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }

  /* write the parameters to the playback device */
  err = snd_pcm_sw_params(garD[s32ID].pAlsaPCM, swparams);
  if(err < 0)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:Unable to set sw params: %s",
                              __FUNCTION__,
                              snd_strerror(err));
    return OSAL_E_UNKNOWN;
  }

  garD[s32ID].bAlsaIsOpen = TRUE;
  if(bIsAcousticOutTraceActive(TR_LEVEL_USER_1))
    vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, FALSE);
  
  
#ifdef ACOUSTICOUT_FADE_IF_ABORT
  (tVoid)bInitFadeCache(s32ID);
#endif //#ifdef ACOUSTICOUT_FADE_IF_ABORT

  vTraceAcousticOutInfo(TR_LEVEL_USER_3, EN_INIT_ALSA,
                        "exit", (tU32)s32ID,
                        (tU32)garD[s32ID].bAlsaIsOpen, 0, 0);
  return OSAL_E_NOERROR;
}

/*************************************************************************/ /**
*  FUNCTION:      u32UnInitAlsa
*  @brief         UnInitializes the Alsa
*  @param         s32ID   stream ID
*  @return        OSAL error code
* 
*  HISTORY:
*
*  - 31.08.2016 Chaitanya Kumar Borah (boc7kor)
*    Added argument for ACOUSTICOUT_PRINTF_U1 
*    as a bugfix for NCG3D-17942
*****************************************************************************/
static tU32 u32UnInitAlsa(tS32 s32ID)
{
  tU32 u32Ret = OSAL_E_NOERROR;
  const char *pcszDevice;
  
  pcszDevice = garD[s32ID].szAlsaDeviceName;
  if(garD[s32ID].pAlsaPCM != NULL)
  {
    //ALSA CLOSE
    garD[s32ID].bAlsaIsOpen = FALSE;
    snd_pcm_drop(garD[s32ID].pAlsaPCM);
    snd_pcm_close(garD[s32ID].pAlsaPCM);
    garD[s32ID].pAlsaPCM = NULL;
  }
  else //if(garD[s32ID].pAlsaPCM != NULL)
  {
    //Bugfix for NCG3D-17942
    ACOUSTICOUT_PRINTF_U1("%s Device not open",pcszDevice);
  } //else //if(garD[s32ID].pAlsaPCM != NULL)

#ifdef ACOUSTICOUT_FADE_IF_ABORT
  vDeleteFadeCache(s32ID);
#endif //#ifdef ACOUSTICOUT_FADE_IF_ABORT
  
  return u32Ret;
}

/*************************************************************************/ /**
*  FUNCTION:      iHandleXRUN
*  @brief         try to recover XRUN
*  @param[in]     s32ID         stream ID
*  @param[in]     iRet          ALSA error from previous call
*  @param[in]     bTraceActive  traces active or not
*  
*  @return        new ALSA error code after revocering
*****************************************************************************/
static int iHandleXRUN(tS32 s32ID, int iRet, tBool bTraceActive)
{
  if(iRet >= 0)
    return iRet;

  // no error, return previous error code
  if(bTraceActive)
  {
    ACOUSTICOUT_PRINTF_U2("%s: try to handle return value: [%d]",
                        __FUNCTION__, iRet);
  } //if(bTraceActive)

  // not an error
  if(iRet == -EAGAIN)
  {
    usleep(2000); // avoid a bug in ALSAlib 1.0.25
    return 0;
  }

  // error < 0, try to handle XRUN
  if(bTraceActive)
    vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, FALSE);

  iRet = snd_pcm_recover(garD[s32ID].pAlsaPCM, iRet, 0);
  return iRet;
}

/*************************************************************************/ /**
*  FUNCTION:      u32WriteToAlsa
*  @brief         Writes an audio buffer
*  @param[in]     s32ID            stream ID
*  @param[out]    pvFrames         buffer for read audio data
*  @param[in]     u32BufSizeBytes  size of provided buffer
*  @param[out]    pu32BytesWritten number of bytes written
*  @param[in]     s32TimeOutMS     timeout in milliseconds
*  @return        OSAL error code
*****************************************************************************/
static tU32 u32WriteToAlsa(tS32 s32ID,
                           void *pvFrames,
                           tU32  u32BufSizeBytes,
                           tPU32 pu32BytesWritten,
                           tS32  s32TimeOutMS)
{
  int   iRet;
  int   iMinTimeoutMS;
  tU32  u32RetVal = OSAL_E_NOERROR;
  tBool bExit = FALSE;
  snd_pcm_sframes_t sFramesToWrite;
  snd_pcm_sframes_t sFramesWritten;
  snd_pcm_sframes_t sRet = 0;
  snd_pcm_uframes_t *pFrameBuffer;
  OSAL_tMSecond startMS; // start time in milliseconds
  OSAL_tMSecond diffMS;
  OSAL_tMSecond timeoutMS;
  tBool bTraceActive = bIsAcousticOutTraceActive(TR_LEVEL_USER_1);

  if(!pu32BytesWritten || !pvFrames)
  {
    return OSAL_E_INVALIDVALUE;
  } //if(!pu32BytesWritten || !pFrameBuffer)

  pFrameBuffer = (snd_pcm_uframes_t *) pvFrames;
  startMS = OSAL_ClockGetElapsedTime();
  *pu32BytesWritten = 0;
  sFramesWritten    = 0;
  sFramesToWrite = snd_pcm_bytes_to_frames(garD[s32ID].pAlsaPCM,
                                           (ssize_t)u32BufSizeBytes);
 
  if(garD[s32ID].uiRealSampleRate > 0)
    iMinTimeoutMS = (int)(sFramesToWrite * 1250) /
                    (int)garD[s32ID].uiRealSampleRate; 
  else //if(garD[s32ID].uiRealSampleRate > 0)
    iMinTimeoutMS = 1000; 

  timeoutMS = (OSAL_tMSecond)
              (s32TimeOutMS < iMinTimeoutMS ? iMinTimeoutMS : s32TimeOutMS);
  
  if(bTraceActive)
  {
    ACOUSTICOUT_PRINTF_U3("%s: "
                         "u32BufSizeBytes [%d], "
                         "sFramesToWrite [%d], "
                         "timeOutMS [%d], "
                         "iMinTimeoutMS [%d]",
                         __FUNCTION__,
                         (int)u32BufSizeBytes,
                         (int)sFramesToWrite,
                         (int)timeoutMS,
                         iMinTimeoutMS);
  } //if(bTraceActive)

  //write loop (non blocking mode)
  do //while(!bExit);
  {
    iRet = snd_pcm_wait(garD[s32ID].pAlsaPCM, (int)timeoutMS);
    diffMS =  OSAL_ClockGetElapsedTime() - startMS;
    if(diffMS > ACOUSTICOUT_EMERGENCY_TIMEOUT_MS)
    { //emergency timeout
      ACOUSTICOUT_PRINTF_ERRORS("%s: Emergency Exit [%d ms]",
                                __FUNCTION__, (int)diffMS);
      u32RetVal = OSAL_E_NOACCESS;
      bExit = TRUE;
    } //if(diffMS > ACOUSTICOUT_EMERGENCY_TIMEOUT_MS)
    
    if(iRet == 0)
    { //timeout
      if(bIsAcousticOutTraceActive(TR_LEVEL_USER_3))
        vTraceAcousticDumpAlsa(garD[s32ID].pAlsaPCM, FALSE);
      ACOUSTICOUT_PRINTF_U1("%s: snd_pcm_wait timeout "
                            " diffMS [%d], "
                            " timeoutMS [%d], "
                            "sFrmsToWrite [%d], "
                            "sFrmsWritten [%d], PeriodFillLevel [%d]",
                            __FUNCTION__,
                            (int)diffMS,
                            (int)timeoutMS,
                            (int)sFramesToWrite,
                            (int)sFramesWritten,
                            (int)garD[s32ID].nPeriodFillLevel);
      u32RetVal = OSAL_E_TIMEOUT;
      bExit     = TRUE;
    }
    else if(iRet < 0)
    {
      iRet = iHandleXRUN(s32ID, iRet, bTraceActive);
      if(iRet < 0)
      {
      ACOUSTICOUT_PRINTF_ERRORS("%s: Cannot Handle XRUN "
                            " iRet [%d], "
                            " diffMS [%d], "
                            " timeoutMS [%d], "
                            "sFrmsToWrite [%d], "
                            "sFrmsWritten [%d], PeriodFillLevel [%d]",
                            __FUNCTION__,
                            (int)iRet,
                            (int)diffMS,
                            (int)timeoutMS,
                            (int)sFramesToWrite,
                            (int)sFramesWritten,
                            (int)garD[s32ID].nPeriodFillLevel);
        giCanceledCounter++;
        u32RetVal = OSAL_E_CANCELED;
        bExit = TRUE;
      }
    } //else if(iRet < 0)

    if(!bExit && (iRet > 0))
    { // snd_pcm_wait / iHandleXRUN returns successfully
      sRet = snd_pcm_writei(garD[s32ID].pAlsaPCM, pFrameBuffer,
                           (snd_pcm_uframes_t)sFramesToWrite);
      if(sRet >= 0)
      { //write successfully, increment pointer and decrement frame count
        ssize_t nBytesWritten;
        unsigned char* puc;
        
        if(bTraceActive)
        { // additional condition only for saving resources
          ACOUSTICOUT_PRINTF_U2("%s:snd_pcm_writei: sRet %d, "
                                "sFramesToWrite %d",
                                __FUNCTION__, (int)sRet, (int)sFramesToWrite);
        }//bIsAcousticOutTraceActive
        
        timeoutMS = 2000; // if write is started, increase timeout;
                        // this minimize occurence of  partially written
                        // buffer which cannot be handled by SDS application
        // sRet contains read frames
        nBytesWritten = snd_pcm_frames_to_bytes(garD[s32ID].pAlsaPCM, sRet);
        if(bTraceActive)
          vDebugFileWrite(s32ID, pFrameBuffer, (size_t)nBytesWritten);
        puc = (unsigned char*)(void*)pFrameBuffer;
        puc = &puc[nBytesWritten]; // avoid pointer arithmetic
        pFrameBuffer = (snd_pcm_uframes_t *)(void*)puc; // new buffer pointer
        sFramesToWrite -= sRet;  // frames to write next loop
        sFramesWritten += sRet;
        if(sFramesToWrite <= 0)
          bExit = TRUE;
        *pu32BytesWritten += (tU32)nBytesWritten;
      }
      else //if(sRet >= 0)
      {
        iRet = iHandleXRUN(s32ID, (int)sRet, bTraceActive);
        if(iRet < 0)
        {
           ACOUSTICOUT_PRINTF_ERRORS("%s: Cannot Handle XRUN after Write "
                                      " iRet [%d], "
                                      " diffMS [%d], "
                                      " timeoutMS [%d], "
                                      "sFrmsToWrite [%d], "
                                      "sFrmsWritten [%d], PeriodFillLevel [%d]",
                                      __FUNCTION__,
                                      (int)iRet,
                                      (int)diffMS,
                                      (int)timeoutMS,
                                      (int)sFramesToWrite,
                                      (int)sFramesWritten,
                                      (int)garD[s32ID].nPeriodFillLevel);
          giCanceledCounter++;
          u32RetVal = OSAL_E_CANCELED;
          bExit = TRUE;
        }
      } //else if(sRet >= 0)
    } //if(!bExit)
  }
  while(!bExit);
  
  garD[s32ID].nPeriodFillLevel = ((snd_pcm_uframes_t)sFramesWritten
                                   + garD[s32ID].nPeriodFillLevel) 
                                 % garD[s32ID].nPeriodSize;

  if(bTraceActive)
  {
    ACOUSTICOUT_PRINTF_U1("%s: Ends with Alsa-Status iRet [%d], sRet [%d], " 
                       "sFrmsToWrite [%d], *pu32BytesWritten [%u], "
                       "sFrmsWritten [%d],  PeriodFillLevel [%d] frms",
                       __FUNCTION__,
                       iRet, (int)sRet, (int)sFramesToWrite,
                       (unsigned int)*pu32BytesWritten,
                       (int)sFramesWritten,
                       (int)garD[s32ID].nPeriodFillLevel);
  }//if(bTraceActive)

  if(u32RetVal != OSAL_E_NOERROR)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s: Ends with Alsa-Error [%d] OSAL 0x%08X",
                             __FUNCTION__,
                             iRet,
                             (unsigned int)u32RetVal);
  } //if(iRet < 0)
  
  return u32RetVal;
}


/*************************************************************************/ /**
*  FUNCTION:      u32WriteSilence
*  @brief         Write silence
*  @param[in]     s32ID            stream ID
*  @param[out]    uFrames          number silence frames to write
*  @return        OSAL error code
*****************************************************************************/
static tU32 u32WriteSilence(tS32 s32ID, snd_pcm_uframes_t uFrames)
{
  snd_pcm_uframes_t *pSilence;
  ssize_t            nBytes;

  nBytes = snd_pcm_frames_to_bytes(garD[s32ID].pAlsaPCM,
                                   (snd_pcm_sframes_t)uFrames);
  
  ACOUSTICOUT_PRINTF_U1("%s: [%d] bytes == [%d] frames",
                            __FUNCTION__, (int)nBytes, (int)uFrames);
  
  pSilence = OSAL_pvMemoryAllocate((size_t)nBytes);
  if(pSilence)
  {
    tU32 u32BytesWritten = 0;
    tU32 u32Ret;
    snd_pcm_format_t  format;
    long lSamples;
    lSamples = snd_pcm_bytes_to_samples(garD[s32ID].pAlsaPCM, nBytes);
    format = convertFormatOsalToAlsa(garD[s32ID].rPCMConfig.enSampleFormat);
    (void)snd_pcm_format_set_silence(format, pSilence, (unsigned int)lSamples);
    u32Ret = u32WriteToAlsa(s32ID, pSilence, (tU32)nBytes,
                             &u32BytesWritten, 2000);
    OSAL_vMemoryFree(pSilence);
    if(u32Ret != OSAL_E_NOERROR)
    {
      ACOUSTICOUT_PRINTF_ERRORS("%s:Unable to write silence: [%d] Bytes" 
                                ", Written [%d] Bytes, Err:[0x%08X]",
                                __FUNCTION__,
                                (int)nBytes,
                                (int)u32BytesWritten,
                                (unsigned int)u32Ret);
      return u32Ret;
    } //if(u32Ret != OSAL_E_NOERROR)
  }
  else //if(pSilence)
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:Unable to alloc silence", __FUNCTION__);
    return OSAL_E_NOSPACE;
  } //else //if(pSilence)

  return OSAL_E_NOERROR;
}


#ifdef ACOUSTICOUT_FADE_IF_ABORT
/*************************************************************************/ /**
*  FUNCTION:      bInitFadeCache
*  @brief         init fade cache
*  @param[in]     s32ID         stream ID
*  
*  @return        TRUE if buffer is allocated
*****************************************************************************/
static tBool bInitFadeCache(tS32 s32ID)
{
  //! ONLY MONO/S16_LE is SUPPORTED (1 channel, signed little endian 16bit)
  ssize_t sizeBytes;
    
  // der Cache ist MAX_BUFFER_SAMPLES + FADE_LEN_SAMPLES lang
  fadecache.fadeLen = (snd_pcm_sframes_t)
                     ((garD[s32ID].uiRealSampleRate  * ACOUSTICOUT_FADE_LEN_MS)
                      / 1000);
  fadecache.size = fadecache.fadeLen + ACOUSTICOUT_MAX_BUFFER_SAMPLES;
  fadecache.fill = 0;
  sizeBytes = snd_pcm_frames_to_bytes(garD[s32ID].pAlsaPCM, fadecache.size);
  fadecache.pCache = OSAL_pvMemoryAllocate((size_t)sizeBytes);

  ACOUSTICOUT_PRINTF_U1("%s: fill [%d], size [%d], fadeLen [%d]",
                        __FUNCTION__, (int)fadecache.fill, (int)fadecache.size, (int)fadecache.fadeLen);
  
  return fadecache.pCache != NULL;
}

/*************************************************************************/ /**
*  FUNCTION:      iDeleteFadeCache
*  @brief         deletes fade cache
*  @param[in]     s32ID         stream ID
*  
*  @return        void
*****************************************************************************/
static tVoid vDeleteFadeCache(tS32 s32ID)
{
  (void)s32ID;
  if(fadecache.pCache)
    OSAL_vMemoryFree(fadecache.pCache);
  fadecache.pCache = NULL;
}

/*************************************************************************/ /**
*  FUNCTION:      fadeCache
*  @brief         fades fade cache
*                 ONLY MONO/S16_LE is SUPPORTED
*                 (1 channel, signed little endian 16bit)
*  @param[in]     s32ID         stream ID
*  @return        void
*****************************************************************************/
static tVoid vFadeCache(tS32 s32ID)
{
  // formula taken from
  // http://stackoverflow.com/questions/20037947/fade-out-function-of-audio-between-samplerate-changes
  //! ONLY MONO/S16_LE is SUPPORTED (1 channel, signed little endian 16bit)
  // cpu must run in little endian mode (cache is using tS16 for s16_le sample)!
  double dFactor = 1.0;
  double dDecayTime = (double)ACOUSTICOUT_FADE_LEN_MS / 1000.0;
  double dSampleTime = garD[s32ID].uiRealSampleRate > 0 ?
                       1.0 / (double)garD[s32ID].uiRealSampleRate : 0.1;
  //double dDecayFactor = exp(- log(2) * dSampleTime / dDecayTime); // 50%
  // log(2) double dDecayFactor = exp(- 0.69314718056 * dSampleTime / dDecayTime);
  double dDecayFactor = exp(- log(10.0) * dSampleTime / dDecayTime); //10%
  snd_pcm_sframes_t i;
  double dSample;
  //TODO need correct 16 bit type / conversion for s16_le (little endian!) sample
  tS16 *ps16Cache = fadecache.pCache;
 
  ACOUSTICOUT_PRINTF_U3("%s: fill [%d], size [%d], fadeLen [%d]",
                        __FUNCTION__, (int)fadecache.fill,
                        (int)fadecache.size, (int)fadecache.fadeLen);

  for(i=0; i< fadecache.fill; i++)
  {
    dFactor *= dDecayFactor;
    dSample = ps16Cache[i];
    dSample *= dFactor;
    ps16Cache[i] = (tS16)dSample;
  }
}

/*************************************************************************/ /**
*  FUNCTION:      vDrainFadeCache
*  @brief         drain fade cache
*  @param[in]     s32ID         stream ID
*  
*  @return        void
*****************************************************************************/
static tVoid vDrainFadeCache(tS32 s32ID)
{
  ssize_t cacheFillLevelBytes;
  tU32 u32BytesWritten;
  tS32 s32TimeOutMS;

  if(!fadecache.pCache)
  {
    ACOUSTICOUT_PRINTF_U1("%s NO CACHE, Exit", __FUNCTION__);
    return;
  }

  if(garD[s32ID].uiRealSampleRate == 0)
  {
    ACOUSTICOUT_PRINTF_U1("%s NO SAMPLERATE Exit", __FUNCTION__);
    return;
  }

  s32TimeOutMS = (tS32)((fadecache.fill + (tS32)garD[s32ID].nPeriodSize)
                        * 1000 * 2)
                        / (tS32)garD[s32ID].uiRealSampleRate; // wait 2 times
  
  //WriteToALSA from cache
  cacheFillLevelBytes = snd_pcm_frames_to_bytes(garD[s32ID].pAlsaPCM,
                                                fadecache.fill);
  
  ACOUSTICOUT_PRINTF_U1("%s: fill [%d], size [%d], fadeLen [%d],"
                        "cacheFillLevelBytes [%d]",
                        __FUNCTION__, (int)fadecache.fill, (int)fadecache.size,
                        (int)fadecache.fadeLen, (int)cacheFillLevelBytes);

  (tVoid)u32WriteToAlsa(s32ID,
                        fadecache.pCache,
                        (tU32)cacheFillLevelBytes,
                        &u32BytesWritten,
                        s32TimeOutMS);

  fadecache.fill = 0;
}


/*************************************************************************/ /**
*  FUNCTION:      u32WriteToFadeCache
*  @brief         Writes an audio buffer in fade cache
*  @param[in]     s32ID            stream ID
*  @param[out]    pvFrames         buffer for read audio data
*  @param[in]     u32BufSizeBytes  size of provided buffer
*  @param[out]    pu32BytesWritten number of bytes written
*  @param[in]     s32TimeOutMS     timeout in milliseconds
*  @return        OSAL error code
*****************************************************************************/
static tU32 u32WriteToFadeCache(tS32 s32ID,
                           void *pvFrames,
                           tU32  u32BufSizeBytes,
                           tPU32 pu32BytesWritten,
                           tS32  s32TimeOutMS)
{
  snd_pcm_sframes_t frames = snd_pcm_bytes_to_frames(garD[s32ID].pAlsaPCM,
                                           (ssize_t)u32BufSizeBytes);
  tU32 u32Ret = OSAL_E_NOERROR;
  
  (void)pvFrames;

  //Copy to Cache, starting at fill level
  // todo check snd_pcm_area_copy() instead of memcpy
  memcpy(&fadecache.pCache[fadecache.fill], pvFrames, u32BufSizeBytes);
  fadecache.fill += frames;

  ACOUSTICOUT_PRINTF_U1("%s: frames [%d] fill [%d], size [%d], fadeLen [%d], " 
                        "u32BufSizeBytes [%d]",
                        __FUNCTION__, (int)frames, (int)fadecache.fill,
                        (int)fadecache.size, (int)fadecache.fadeLen,
                        (int)u32BufSizeBytes);
  
  //writing to ALSA if more than fade-len frames available
  if((fadecache.fill > fadecache.fadeLen)
    &&
    (garD[s32ID].uiRealSampleRate != 0))
  {
    ssize_t bytesFilled;
    snd_pcm_sframes_t framesWritten;
    tU32 u32BytesWritten;
    snd_pcm_sframes_t framesWriteToALSA = fadecache.fill - fadecache.fadeLen;
    ssize_t bytesWriteToALSA = snd_pcm_frames_to_bytes(garD[s32ID].pAlsaPCM,
                                                       framesWriteToALSA);


    s32TimeOutMS = (tS32)((framesWriteToALSA + (tS32)garD[s32ID].nPeriodSize)
                           * 1000 * 2)
                           / (tS32)garD[s32ID].uiRealSampleRate; // wait 2 times
    
    u32Ret = u32WriteToAlsa(s32ID,
                            fadecache.pCache,
                            (tU32)bytesWriteToALSA,
                            &u32BytesWritten,
                            s32TimeOutMS);

    framesWritten = snd_pcm_bytes_to_frames(garD[s32ID].pAlsaPCM,
                                            (ssize_t)u32BytesWritten);
    fadecache.fill -= framesWritten;
    //shift cache
    bytesFilled = snd_pcm_frames_to_bytes(garD[s32ID].pAlsaPCM,
                                          fadecache.fill);
    // todo check snd_pcm_area_copy() instead of memmove
    memmove(fadecache.pCache,
            &fadecache.pCache[framesWritten],
            (size_t)bytesFilled);
  }

  *pu32BytesWritten = u32BufSizeBytes; //simulate OK

  ACOUSTICOUT_PRINTF_U1("%s END: fill [%d], size [%d], fadeLen [%d]," 
                        " *pu32BytesWritten [%d], u32Ret 0x%08X",
                        __FUNCTION__, (int)fadecache.fill, (int)fadecache.size,
                        (int)fadecache.fadeLen, (int)*pu32BytesWritten,
                        (unsigned int)u32Ret);
  
  return u32Ret;
}
#endif //#ifdef ACOUSTICOUT_FADE_IF_ABORT

/*************************************************************************/ /**
*  FUNCTION:      vSetThreadPrio
*  @brief         set thread prio to FIFO/max
*  @param[in]     s32ID            stream ID
*  @return        void
*****************************************************************************/
static void vSetThreadPrio(tS32 s32ID)
{
  (void)s32ID;
#ifdef ACOUSTICOUT_CHANGE_THREAD_PRIO  
  pthread_t thId = pthread_self();
  int policy;
  int err;
  struct sched_param param;

  err = pthread_getschedparam(thId, &garD[s32ID].threadPolicy,
                              &garD[s32ID].threadParam); 
  if(err == 0)
  {
    pthread_getschedparam(thId, &policy, &param);
    policy = SCHED_FIFO;
    param.sched_priority = sched_get_priority_max(policy);
    (void)pthread_setschedparam(thId, policy, &param);
  }
  else //if(pthread_getschedparam
  {
    ACOUSTICOUT_PRINTF_ERRORS("%s:Unable to get schedparams: [%d]", 
                              __FUNCTION__, err);

    garD[s32ID].threadPolicy = -1;
  } //else if(pthread_getschedparam
#endif //#ifdef ACOUSTICOUT_CHANGE_THREAD_PRIO  
}

/*************************************************************************/ /**
*  FUNCTION:      vRestoreThreadPrio
*  @brief         reset previous thread prio/policy
*  @param[in]     s32ID            stream ID
*  @return        void
*****************************************************************************/
static void vRestoreThreadPrio(tS32 s32ID)
{
  (void)s32ID;
#ifdef ACOUSTICOUT_CHANGE_THREAD_PRIO  
  if(garD[s32ID].threadPolicy != -1)
  {
    pthread_t thId = pthread_self();
    pthread_setschedparam(thId, garD[s32ID].threadPolicy,
                          &garD[s32ID].threadParam);
    garD[s32ID].threadPolicy = -1;
  }
#endif //#ifdef ACOUSTICOUT_CHANGE_THREAD_PRIO  
}

#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file 
|-----------------------------------------------------------------------*/
