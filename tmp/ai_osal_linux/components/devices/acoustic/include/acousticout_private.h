/************************************************************************
| FILE:         acousticout_privat.h
| PROJECT:      Paramount
| SW-COMPONENT: Acoustic driver
|------------------------------------------------------------------------
|************************************************************************/
/* ******************************************************FileHeaderBegin** *//**
 * @file    acousticout_ioctrls.h
 *
 * @brief   This file is the header for the IOControl implementations of the
 *          acousticout driver
 *
 * @author  Elektrobit Automotive
 *
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (ACOUSTICOUT_PRIVATE_H)
   #define ACOUSTICOUT_PRIVATE_H

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/
  
#define ACOUSTICOUT_FADE_IF_ABORT
#define ACOUSTICOUT_FADE_LEN_MS 8
#define ACOUSTICOUT_MAX_BUFFER_SAMPLES  65536

//#define ACOUSTICOUT_USE_DRAIN_AS_ABORT
//#define ACOUSTICOUT_FILL_MIN_SILENCE
//#define ACOUSTICOUT_FILL_MIN_SILENCE_DURATION_MS 4


#define ACOUSTICOUT_ALSA_DEVICE_NAME_BUFFER_SIZE    64


#define ACOUSTICOUT_C_U32_HW_CANNOT_BE_PAUSED 0
#define ACOUSTICOUT_C_U32_HW_CAN_BE_PAUSED 1

/* Below macros are used while filling the incomplete period with silence */
#define ACOUT_GET_AVAIL_BUF 0
#define ACOUT_INSERT_SILENCE 1
#define ACOUT_FILL_INCOMPLETE_BUF 2
#define ACOUT_RET_ER_SILENCE 3
#define ACOUT_MAX_RETRY_COUNT 2



/* basis for event name */
/*this is a format string used by printf*/
/*entire string < 16 characters!*/
#define ACOUSTICOUT_C_SZ_EVENT_FORMAT             "AOUTE%u%u"

/*semaphore used by close device, avoids closing event while
  someone is waiting for it*/
/*this is a format string used by printf*/
/*entire string < 16 characters!*/
#define ACOUSTICOUT_C_SZ_SEM_CLOSE_FORMAT         "AOUTS%u%u"  


/* Default parameters for the device.
The default configuration in this project after opening the device is:
o PCM sample format: 16 bit signed, CPU endian (=little endian)
o PCM sample rate: 16kHz
o Preloaded acoustic codecs: PCM only
o Default acoustic decoder: PCM
o Buffer size: To be determined during integration
o Error thresholds: 0 (disabled) for all errors
o Write operation: 10s */
#define ACOUSTICOUT_C_EN_DEFAULT_PCM_SAMPLEFORMAT    OSAL_EN_ACOUSTIC_SF_S16
#define ACOUSTICOUT_C_U32_DEFAULT_PCM_SAMPLERATE     ((tU32)22050)
#define ACOUSTICOUT_C_U8_DEFAULT_NUM_CHANNELS_MUSIC  ((tU8)2)
#define ACOUSTICOUT_C_U8_DEFAULT_NUM_CHANNELS_SPEECH ((tU8)2)
#define ACOUSTICOUT_C_EN_DEFAULT_DECODER             OSAL_EN_ACOUSTIC_DEC_PCM
#define ACOUSTICOUT_C_U32_DEFAULT_BUFFER_SIZE_PCM    ((tU32)4096)
#define ACOUSTICOUT_C_U32_DEFAULT_WRITE_TIMEOUT      10000          /* 10s */


/** version for returning with IOControl OSAL_C_S32_IOCTRL_VERSION */
#define ACOUSTICOUT_C_S32_IO_VERSION   ((tS32)(0x00010001))

/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

/** PCM config data */
typedef struct
{
  OSAL_tAcousticSampleRate     nSampleRate;
  OSAL_tenAcousticSampleFormat enSampleFormat;
  tU16                         u16NumChannels;
} trPCMConfigData;

/** play states
*
* @note See design document for details to the states */
typedef enum
{
  ACOUSTICOUT_EN_STATE_UNINITIALIZED = 0,  /**< driver uninitialized */
  ACOUSTICOUT_EN_STATE_INITIALIZED   = 1,  /**< driver initialized */
  ACOUSTICOUT_EN_STATE_STOPPED       = 2,  /**< device opened but inactive */
  ACOUSTICOUT_EN_STATE_PREPARE       = 3,  /**< preparing audio stream */
  ACOUSTICOUT_EN_STATE_ACTIVE        = 4,  /**< audio stream active */
  ACOUSTICOUT_EN_STATE_PAUSED        = 5,  /**< audio stream paused */
  ACOUSTICOUT_EN_STATE_STOPPING      = 6   /**< audio stream stopping */
} ACOUSTICOUT_enPlayStates;

/** file handles for the streams */
enum ACOUSTICOUT_enFileHandles
{
  ACOUSTICOUT_EN_HANDLE_ID_SPEECH = 1, /**< file handle for speech stream */
  ACOUSTICOUT_EN_HANDLE_ID_MUSIC  = 2  /**< file handle for music stream */
};

/** Internal status + configuration data */
typedef struct
{
  tBool                           bAlsaIsOpen;
  /**< Alsa open flag */
  snd_pcm_t                      *pAlsaPCM;
  /**< pointer to handle for PCM Alsa device */
  snd_pcm_uframes_t              nPeriodSize;
  /**< real period size in frames */
  snd_pcm_uframes_t              nPeriodFillLevel;
  /**< fill level of last partially filled period*/
  unsigned int                   uiRealSampleRate;
  /**< real sample rate set by ALSA*/
  char szAlsaDeviceName[ACOUSTICOUT_ALSA_DEVICE_NAME_BUFFER_SIZE];
  /**< device name as configured in /etc/asound.project.conf */
  enum ACOUSTICOUT_enFileHandles  enFileHandle;
  /**< file handle */
  OSAL_tenAcousticCodec           enCodec;
  /**< current decoder */
  trPCMConfigData                 rPCMConfig;
  /**< PCM config data */
  OSAL_tAcousticBuffersize        anBufferSize[OSAL_EN_ACOUSTIC_CODECLAST];
  /**< buffer size for each codec (PCM) */
  tU32                            u32WriteTimeout;
  /**< timeout for write operation */
  ACOUSTICOUT_enPlayStates        enPlayState;
  /**< current play state of the stream */
  ACOUSTICOUT_enPlayStates        enPlayStateBeforePause;
  /**< play state before pause cmd */
  OSAL_tEventHandle               hOsalEvent;
  /**< handle for event field */
  OSAL_trAcousticOutCallbackReg   rCallback;
  /**< callback data */
  OSAL_tSemHandle                 hSemClose;
  /**< semaphore protecting closing of device*/
  tS32 hw_can_pause; 
  /**< to check whether the hw supports pause*/
  snd_pcm_format_t format;
  /**< sample format being set */
  snd_pcm_uframes_t period_size;
  /** < ALSA period size */
  snd_pcm_uframes_t FramesFilledInPeriod;
  /** < Frames filled in period */
  int                            iDebugPCMFileHandle;
  /**< file handle for writing PCM data to file*/
  int threadPolicy;
  struct sched_param threadParam;

  tBool                          bPreFillFirstPeriod;
} trACOUSTICOUT_StateData, *pACOUSTICOUT_StateData;

/** event masks for notification types
*
* @note These events are signalled whenever an event shall be signalled to
* the client via event loop.
* The same event group is used as for the output device events, therefore
* the values must not overlap.
*/
enum
{
  ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED =           0x00010000,
  ACOUSTICOUT_EN_EVENT_MASK_NOTI_CANCEL =                 0x00020000,
  ACOUSTICOUT_EN_EVENT_MASK_NOTI_ANY =
  (ACOUSTICOUT_EN_EVENT_MASK_NOTI_AUDIOSTOPPED |
   ACOUSTICOUT_EN_EVENT_MASK_NOTI_CANCEL)
};

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/** maximum number of parallely loaded codecs for *one* device instance
*
* For now, each device instance gets the same number of max. codec instances.
*/
#define ACOUSTICOUT_C_U8_MAXCODECCNT                 ((tU8)1)

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/

static tU32 IOCtrl_Version(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_Extwrite(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static void IOCtrl_RegNotification(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_WaitEvent(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSuppDecoder(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_NextNeededDecoder(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetDecoder(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetDecoder(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSuppSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetSamplerate(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSuppSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetSampleformat(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSuppChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetChannels(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetSuppBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetBuffersize(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_GetTime(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_SetTime(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_Start(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_Stop(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_Abort(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 IOCtrl_Pause(tS32 s32ID, tU32 u32FD, tS32 s32Arg);
static tU32 u32DoWriteOperation(tS32 s32ID, tU32 u32FD,
                                const OSAL_trAcousticOutWrite* prWriteInfo,
                                tPU32 puBytesWritten, tS32 s32TimeOut);
static tBool bIsCodecValid(OSAL_tenAcousticCodec enCodec);
static tBool bIsSampleformatValid(OSAL_tenAcousticSampleFormat enSampleformat);
static tBool bIsSamplerateValid(OSAL_tAcousticSampleRate nSamplerate);
static tBool bIsChannelnumValid(tU16 u16Channelnum);
static tBool bIsBuffersizeValid(tU32 u32Buffersize,
                                OSAL_tenAcousticCodec enCodec);
static tU32 u32SetEvent(const trACOUSTICOUT_StateData *prStateData,
                        const OSAL_tEventMask tWaitEventMask);
static tU32 u32ClearEvent(const trACOUSTICOUT_StateData *prStateData,
                          const OSAL_tEventMask tWaitEventMask);
static tU32 u32AbortStream(tS32 s32ID, tU32 u32FD);
static tU32 u32OutputDeviceStop(tS32 s32ID);
static tU32 u32OutputDeviceAbort(tS32 s32ID);

static tS32 s32OutputDeviceWrite(tS32 s32ID, tPVoid pvBuffer,
                                 tU32 u32BufferSize,
                                 tPU32 pu32BytesWritten,
                                 tS32  s32TimeOut);
static tU32 u32DoWriteOperation(tS32 s32ID, tU32 u32FD,
                                const OSAL_trAcousticOutWrite* prWriteInfo,
                                tPU32 pu32BytesWritten,
                                tS32  s32TimeOut);
static tS32 s32FillPeriodBeforeDrain(tS32 s32ID);
static tS32 s32FindAvailableBuffer(tS32 s32ID, tU32 *pSilentFrames);
static tS32 s32InsertSilenceInBuffer(tS32 s32ID,
                                     tU32 u32NumSamples,
                                     const char* pbuffer);
static tS32 s32DeviceWriteSilence(tS32 s32ID,
                                  tS32 s32SilentFrames,
                                  const char* pbuffer);
static snd_pcm_format_t convertFormatOsalToAlsa(
                        OSAL_tenAcousticSampleFormat nOsalSampleFormat);
static tU32 u32InitAlsa(tS32 s32ID);
static tU32 u32UnInitAlsa(tS32 s32ID);
static tU32 u32WriteToAlsa(tS32 s32ID,
                           void *pvFrames,
                           tU32  u32BufSizeBytes,
                           tPU32 pu32BytesWritten,
                           tS32  s32TimeOutMS);
static tU32 u32WriteSilence(tS32 s32ID, snd_pcm_uframes_t uFrames);
static void vRestoreThreadPrio(tS32 s32ID);
static void vSetThreadPrio(tS32 s32ID);

#ifdef ACOUSTICOUT_FADE_IF_ABORT
  static tBool bInitFadeCache(tS32 s32ID);
  static tVoid vDeleteFadeCache(tS32 s32ID);
  static tVoid vFadeCache(tS32 s32ID);
  static tVoid vDrainFadeCache(tS32 s32ID);
  static tU32 u32WriteToFadeCache(tS32 s32ID,
                             void *pvFrames,
                             tU32  u32BufSizeBytes,
                             tPU32 pu32BytesWritten,
                             tS32  s32TimeOutMS);
#endif //#ifdef ACOUSTICOUT_FADE_IF_ABORT

#ifdef __cplusplus
}
#endif

#else /*if !defined (ACOUSTICOUT_PRIVATE_H)*/
#error acousticout_private.h included several times
#endif /*else if !defined (ACOUSTICOUT_PRIVATE_H)*/

