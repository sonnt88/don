/************************************************************************
| FILE:         acoustic_trace.h
| PROJECT:      GEN2
| SW-COMPONENT: Acoustic driver
|------------------------------------------------------------------------
| This file is under RCS control (do not edit the following lines)
| $RCSfile: acoustic_trace.h,v $
| $Revision: 1.1.2.14 $
| $Date: 2005/08/02 14:49:39 $
|************************************************************************/
/* comment header doxygen <http://www.doxygen.org> generated documentation */
/* ******************************************************FileHeaderBegin** *//**
 * @file    acoustic_trace.h
 *
 * @brief   This file includes trace stuff for the acoustic driver.
 *
 * @author  3SOFT GmbH Erlangen
 *
 * @date
 *
 * @version
 *
 * @note
 *  &copy; 2005 Blaupunkt GmbH, Hildesheim (Germany)
 *
 *//* ***************************************************FileHeaderEnd******* */

#if !defined (ACOUSTIC_TRACE_H)
   #define ACOUSTIC_TRACE_H
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/

/*trace classes used by dev acoustic, defined in osalcore/include/ostrace.h*/
#define ACOUSTICIN_TRACE_CLASS  OSAL_C_TR_CLASS_DEV_ACOUSTICIN_LINUX
#define ACOUSTICOUT_TRACE_CLASS OSAL_C_TR_CLASS_DEV_ACOUSTICOUT_LINUX
#define ACOUSTICSRC_TRACE_CLASS OSAL_C_TR_CLASS_DEV_ACOUSTICSRC_LINUX
#define ACOUSTICECNR_TRACE_CLASS  OSAL_C_TR_CLASS_DEV_ACOUSTICECNR

    
#define ACOUSTICOUT_PRINTF_FATAL(...)\
 vTraceAcousticOutPrintf(TR_LEVEL_FATAL, __VA_ARGS__)
#define ACOUSTICOUT_PRINTF_ERRORS(...)\
 vTraceAcousticOutPrintf(TR_LEVEL_ERRORS, __VA_ARGS__)
#define ACOUSTICOUT_PRINTF_U1(...)\
 vTraceAcousticOutPrintf(TR_LEVEL_USER_1, __VA_ARGS__)
#define ACOUSTICOUT_PRINTF_U2(...)\
 vTraceAcousticOutPrintf(TR_LEVEL_USER_2, __VA_ARGS__)
#define ACOUSTICOUT_PRINTF_U3(...)\
 vTraceAcousticOutPrintf(TR_LEVEL_USER_3, __VA_ARGS__)
#define ACOUSTICOUT_PRINTF_U4(...)\
 vTraceAcousticOutPrintf(TR_LEVEL_USER_4, __VA_ARGS__)

#define ACOUSTICIN_PRINTF_FATAL(...)\
 vTraceAcousticInPrintf(TR_LEVEL_FATAL, __VA_ARGS__)
#define ACOUSTICIN_PRINTF_ERRORS(...)\
 vTraceAcousticInPrintf(TR_LEVEL_ERRORS, __VA_ARGS__)
#define ACOUSTICIN_PRINTF_U1(...)\
 vTraceAcousticInPrintf(TR_LEVEL_USER_1, __VA_ARGS__)
#define ACOUSTICIN_PRINTF_U2(...)\
 vTraceAcousticInPrintf(TR_LEVEL_USER_2, __VA_ARGS__)
#define ACOUSTICIN_PRINTF_U3(...)\
 vTraceAcousticInPrintf(TR_LEVEL_USER_3, __VA_ARGS__)
#define ACOUSTICIN_PRINTF_U4(...)\
 vTraceAcousticInPrintf(TR_LEVEL_USER_4, __VA_ARGS__)

#define ACOUSTICECNR_PRINTF_FATAL(...)\
 vTraceAcousticECNRPrintf(TR_LEVEL_FATAL, __VA_ARGS__)
#define ACOUSTICECNR_PRINTF_ERRORS(...)\
 vTraceAcousticECNRPrintf(TR_LEVEL_ERRORS, __VA_ARGS__)
#define ACOUSTICECNR_PRINTF_U1(...)\
 vTraceAcousticECNRPrintf(TR_LEVEL_USER_1, __VA_ARGS__)
#define ACOUSTICECNR_PRINTF_U2(...)\
 vTraceAcousticECNRPrintf(TR_LEVEL_USER_2, __VA_ARGS__)
#define ACOUSTICECNR_PRINTF_U3(...)\
 vTraceAcousticECNRPrintf(TR_LEVEL_USER_3, __VA_ARGS__)
#define ACOUSTICECNR_PRINTF_U4(...)\
 vTraceAcousticECNRPrintf(TR_LEVEL_USER_4, __VA_ARGS__)
    

#define ACOUSTICSRC_PRINTF_FATAL(...)\
 vTraceAcousticSrcPrintf(TR_LEVEL_FATAL, __VA_ARGS__)
#define ACOUSTICSRC_PRINTF_ERRORS(...)\
 vTraceAcousticSrcPrintf(TR_LEVEL_ERRORS, __VA_ARGS__)
#define ACOUSTICSRC_PRINTF_U1(...)\
 vTraceAcousticSrcPrintf(TR_LEVEL_USER_1, __VA_ARGS__)
#define ACOUSTICSRC_PRINTF_U2(...)\
 vTraceAcousticSrcPrintf(TR_LEVEL_USER_2, __VA_ARGS__)
#define ACOUSTICSRC_PRINTF_U3(...)\
 vTraceAcousticSrcPrintf(TR_LEVEL_USER_3, __VA_ARGS__)
#define ACOUSTICSRC_PRINTF_U4(...)\
 vTraceAcousticSrcPrintf(TR_LEVEL_USER_4, __VA_ARGS__)    

    
/** IDs for traces */
enum enTraceFunction
{
   EN_ACOUT_OPEN                    = 0,
   EN_ACOUT_CLOSE                   = 1,
   EN_ACOUT_WRITE                   = 2,
   EN_ACOUT_IOCTRL                  = 3,
   EN_IOCTRL_VERSION                = 4,
   EN_IOCTRL_EXTWRITE               = 5,
   EN_IOCTRL_REG_NOTIFICATION       = 6,
   EN_IOCTRL_WAITEVENT              = 7,
   EN_IOCTRL_GETSUPP_DECODER        = 8,
   EN_IOCTRL_NEXTNEEDEDDECODER      = 9,
   EN_IOCTRL_GETDECODER             = 10,
   EN_IOCTRL_SETDECODER             = 11,
   EN_IOCTRL_GETSUPP_SAMPLERATE     = 12,
   EN_IOCTRL_GETSAMPLERATE          = 13,
   EN_IOCTRL_SETSAMPLERATE          = 14,
   EN_IOCTRL_GETSUPP_SAMPLEFORMAT   = 15,
   EN_IOCTRL_GETSAMPLEFORMAT        = 16,
   EN_IOCTRL_SETSAMPLEFORMAT        = 17,
   EN_IOCTRL_GETSUPP_CHANNELS       = 18,
   EN_IOCTRL_GETCHANNELS            = 19,
   EN_IOCTRL_SETCHANNELS            = 20,
   EN_IOCTRL_GETSUPP_BUFFERSIZE     = 21,
   EN_IOCTRL_GETBUFFERSIZE          = 22,
   EN_IOCTRL_SETBUFFERSIZE          = 23,
   EN_IOCTRL_SETERRTHR              = 24,
   EN_IOCTRL_GETTIME                = 25,
   EN_IOCTRL_SETTIME                = 26,
   EN_IOCTRL_START                  = 27,
   EN_IOCTRL_STOP                   = 28,
   EN_IOCTRL_ABORT                  = 29,
   EN_IOCTRL_PAUSE                  = 30,
   EN_IOCTRL_SET_FILTER_COEF        = 31,
   EN_ACOUT_CREATE_UASSEMAPHORE     = 32,
   EN_INITOUTPUTDEVICE              = 33,
   EN_UNINITOUTPUTDEVICE            = 34,
   EN_OUTPUTDEVICE_STOP             = 35,
   EN_OUTPUTDEVICE_ABORT            = 36,
   EN_OUTPUTDEVICE_WRITE            = 37,
   EN_OUTPUTDEVICE_CBREG            = 38,
   EN_OUTPUTDEVICE_CBUREG           = 39,
   EN_RESET_STREAMDATA              = 40,
   EN_INIT_MULTIBUFFER              = 41,
   EN_OUTPUTDEVICE_MSG_CALLBACK     = 42,
   EN_SENDTOOUTPUTDEVICE            = 43,
   EN_MULTIBUFFER_FLUSH             = 44,
   EN_MULTIBUFFFER_SETSTOP          = 45,
   EN_WAIT_FOR_LOADED_CODEC         = 46,
   EN_LOAD_DECODER                  = 47,
   EN_UNLOAD_DECODER                = 48,
   EN_UNLOAD_UNUSED_DECODER         = 49,
   EN_GET_DECODER_STATE             = 50,
   EN_MULTIBUFFER_WRITE             = 51,
   EN_MULTIBUFFER_FLUSHABLE         = 52,
   EN_ABORT_STREAM                  = 53,
   EN_GET_AUDIO_INPUT_CHANNELHANDLE = 54,
   EN_GET_STREAMTYPE                = 55,
   EN_DO_WRITEOPERATION             = 56,
   EN_IS_CODEC_VALID                = 57,
   EN_IS_SAMPLEFORMAT_VALID         = 58,
   EN_IS_SAMPLERATE_VALID           = 59,
   EN_IS_CHANNELNUM_VALID           = 60,
   EN_IS_BUFFERSIZE_VALID           = 61,
   EN_RESET_ERRORCOUNTERS           = 62,
   EN_RESET_EERRORTHRESHOLDS        = 63,
   EN_SET_EVENT                     = 64,
   EN_CLEAR_EVENT                   = 65,
   EN_WAIT_EVENT                    = 66,
   EN_WAIT_CLEAR_EVENT              = 67,
   EN_MARKERTABLE_LOCK              = 68,
   EN_MARKERTABLE_RELEASE           = 69,
   EN_BUFFERTABLE_LOCK              = 70,
   EN_BUFFERTABLE_RELEASE           = 71,
   EN_BALLOWEDTOSEND                = 72,
   EN_ACIN_OPEN                     = 73,
   EN_ACIN_CLOSE                    = 74,
   EN_ACIN_IOCTRL                   = 75,
   EN_ACIN_READ                     = 76,
   EN_IOCTRL_EXTREAD                = 77,
   EN_DO_READOPERATION              = 78,
   EN_RCVFROMDRV                    = 79,
   EN_INIT_ALSA                     = 80,
   EN_UNINIT_ALSA                   = 81,
   EN_ACIN_INIT                     = 82,
   EN_IOCTRL_CONVERT				= 83,
   EN_IOCTRL_GETFILEPATH			= 84,
   EN_DO_SRCOPERATION				= 85,
   EN_OUTPUTDATA_CONVERT			= 86,
   EN_ACSRC_OPEN					= 87,
   EN_ACSRC_CLOSE					= 88,
   EN_ACSRC_IOCTRL					= 89,
   EN_ACECNR_OPEN                   = 125,
   EN_ACECNR_CLOSE                  = 126,
   EN_ACECNR_IOCTRL                 = 127,
   EN_ACECNR_READ                   = 128,
   EN_ACECNR_INIT					= 129
};

/** trace messages */
enum enTraceMessages
{
   TRACE_ERROR = 0,
    TRACE_INFO,
    TRACE_PRINTF
};


/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/                     

tVoid vTraceAcousticOutError(TR_tenTraceLevel enTraceLevel,
                             enum enTraceFunction enFunction, 
                             tU32 u32OSALError,
                             tPCChar copchDescription,
                             tU32 u32Par1,
                             tU32 u32Par2,
                             tU32 u32Par3,
                             tU32 u32Par4);

tVoid vTraceAcousticOutInfo(TR_tenTraceLevel enTraceLevel,
                            enum enTraceFunction enFunction,
                            tPCChar copchDescription,
                            tU32 u32Par1,
                            tU32 u32Par2,
                            tU32 u32Par3,
                            tU32 u32Par4);

tVoid vTraceAcousticOutPrintf(TR_tenTraceLevel enTraceLevel,
                              const char* coszFormat,...);

tVoid vTraceAcousticInError(TR_tenTraceLevel enTraceLevel,
                            enum enTraceFunction enFunction, 
                            tU32 u32OSALError,
                            tPCChar copchDescription,
                            tU32 u32Par1,
                            tU32 u32Par2,
                            tU32 u32Par3,
                            tU32 u32Par4);

tVoid vTraceAcousticInInfo(TR_tenTraceLevel enTraceLevel,
                           enum enTraceFunction enFunction,
                           tPCChar copchDescription,
                           tU32 u32Par1,
                           tU32 u32Par2,
                           tU32 u32Par3,
                           tU32 u32Par4); 


tVoid vTraceAcousticInPrintf(TR_tenTraceLevel enTraceLevel,
                             const char* coszFormat,...);


tVoid vTraceAcousticSrcError(TR_tenTraceLevel enTraceLevel,
                             enum enTraceFunction enFunction, 
                             tU32 u32OSALError,
                             tPCChar copchDescription,
                             tU32 u32Par1,tU32 u32Par2,tU32 u32Par3,tU32 u32Par4);


tVoid vTraceAcousticSrcInfo(TR_tenTraceLevel enTraceLevel,
                            enum enTraceFunction enFunction,
                            tPCChar copchDescription,
                            tU32 u32Par1,
                            tU32 u32Par2,
                            tU32 u32Par3,
                            tU32 u32Par4);

tVoid vTraceAcousticSrcPrintf(TR_tenTraceLevel enTraceLevel,
                              const char* coszFormat,...);

tVoid vTraceAcousticECNRError(TR_tenTraceLevel enTraceLevel,
                            enum enTraceFunction enFunction, 
                            tU32 u32OSALError,
                            tPCChar copchDescription,
                            tU32 u32Par1,
                            tU32 u32Par2,
                            tU32 u32Par3,
                            tU32 u32Par4);

tVoid vTraceAcousticECNRInfo(TR_tenTraceLevel enTraceLevel,
                           enum enTraceFunction enFunction,
                           tPCChar copchDescription,
                           tU32 u32Par1,
                           tU32 u32Par2,
                           tU32 u32Par3,
                           tU32 u32Par4); 
tVoid vTraceAcousticECNRPrintf(TR_tenTraceLevel enTraceLevel,
                             const char* coszFormat,...);


tBool bIsAcousticInTraceActive(TR_tenTraceLevel enTraceLevel);
tBool bIsAcousticOutTraceActive(TR_tenTraceLevel enTraceLevel);
void vTraceAcousticDumpAlsa(snd_pcm_t *pcm, tBool bIsAcousticIn);

#ifdef __cplusplus
}
#endif
     
#else
#error acoustic_trace.h included several times
#endif                

