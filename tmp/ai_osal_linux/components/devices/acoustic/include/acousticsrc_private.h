/************************************************************************
| FILE:         Acousticsrc_private.h
| SW-COMPONENT: AcousticSRC driver
|------------------------------------------------------------------------
/* ******************************************************FileHeaderBegin** *//**
 * @file    Acousticsrc_private.h
 *
 * @brief   This file is the header for the acousticsrc driver
 *
 * @author  Niyatha S Rao, ECF5, RBEI
 *
 * @date		11/10/2012
 *
 * @version	Intial version
 *
 * @note
 *  &copy; 
 *
 *  @history
 *
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
 *******************************************************FileHeaderEnd**********/

#if !defined (ACOUSTICSRC_PRIVATE_H)
   #define ACOUSTICSRC_PRIVATE_H
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/
#define ACOUSTICSRC_ALSA_DEVICE_NAME_BUFFER_SIZE    64


/* basis for event name */
/*this is a format string used by printf*/
/*entire string < 16 characters!*/
#define ACOUSTICSRC_C_SZ_EVENT_FORMAT             "ASRCE%u%u"

/*semaphore used by close device, avoids closing event while
  someone is waiting for it*/
/*this is a format string used by printf*/
/*entire string < 16 characters!*/
#define ACOUSTICSRC_C_SZ_SEM_CLOSE_FORMAT         "ASRCS%u%u"  


/* Default parameters for the device.
The default configuration in this project after opening the device is:
o PCM sample format: 16 bit signed, CPU endian (=little endian)
o PCM sample rate: 16kHz
o Preloaded acoustic codecs: PCM only
o Default acoustic decoder: PCM
o Buffer size: To be determined during integration
o Error thresholds: 0 (disabled) for all errors
o Write operation: 10s */
#define ACOUSTICSRC_C_EN_DEFAULT_PCM_SAMPLEFORMAT    OSAL_EN_ACOUSTIC_SF_S16
#define ACOUSTICSRC_C_U32_DEFAULT_PCM_SAMPLERATE     ((tU32)16000)
#define ACOUSTICSRC_C_U8_DEFAULT_NUM_CHANNELS ((tU8)1)
#define ACOUSTICSRC_C_EN_DEFAULT_DECODER             OSAL_EN_ACOUSTIC_DEC_PCM
#define ACOUSTICSRC_C_U32_DEFAULT_BUFFER_SIZE_PCM    ((tU32)2048)
#define ACOUSTICSRC_C_U32_DEFAULT_WRITE_TIMEOUT      10000          /* 10s */


/** version for returning with IOControl OSAL_C_S32_IOCTRL_VERSION */
#define ACOUSTICSRC_C_S32_IO_VERSION   ((tS32)(0x00010001))

#define ACOUSTICSRC_FILE_NOT_DUMPED ((tBool)(0))
#define ACOUSTICSRC_FILE_DUMPED ((tBool)(1))


/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

/** PCM config data */
typedef struct
{
   OSAL_tAcousticSampleRate     nSampleRate;    /**< current sample rate for PCM */
   OSAL_tenAcousticSampleFormat enSampleFormat; /**< current sample format for PCM */
   tU16                         u16NumChannels; /**< current number of channels for PCM */
} trPCMConfigData;

/** error threshold data */
typedef struct
{
	/** Configured thresholds indicating when an error is signalled */
	tS32    s32XrunErrThr;        /**< threshold for xrun error */
	tS32    s32BitstreamErrThr;   /**< threshold for bitstream error */
	tS32    s32NoValidDataErrThr; /**< threshold for novaliddata error */
	tS32    s32WrongFormatErrThr; /**< threshold for wrong format error */
	tS32    s32InternalErrThr;    /**< threshold for internal error */
	tS32    s32FatalErrThr;       /**< threshold for fatal error */

	/** Counters indicating how many error have occurred until now */
	tS32    s32XrunErrCnt;        /**< # of xrun errors */
	tS32    s32BitstreamErrCnt;   /**< # of bitstream errors */
	tS32    s32NoValidDataErrCnt; /**< # of novaliddata errors */
	tS32    s32WrongFormatErrCnt; /**< # of wrong format errors */
	tS32    s32InternalErrCnt;    /**< # of internal errors */
	tS32    s32FatalErrCnt;       /**< # of fatal errors */
} trACOUSTICSRC_ErrThresholdData;

/** play states
*
* @note See design document for details to the states */
typedef enum
{
	ACOUSTICSRC_EN_STATE_UNINITIALIZED = 0,  /**< driver uninitialized (should be 0) */
	ACOUSTICSRC_EN_STATE_INITIALIZED   = 1,  /**< driver initialized */
	ACOUSTICSRC_EN_STATE_STOPPED       = 2,  /**< device opened but inactive */
	ACOUSTICSRC_EN_STATE_PREPARE       = 3,  /**< preparing audio stream */
	ACOUSTICSRC_EN_STATE_ACTIVE        = 4,  /**< audio stream active */
	ACOUSTICSRC_EN_STATE_STOPPING      = 5   /**< audio stream stopping */
} ACOUSTICSRC_enPlayStates;

/** file handles for the streams */
enum ACOUSTICSRC_enFileHandles
{
	ACOUSTICSRC_EN_HANDLE_ID_SPEECH = 1 /**< file handle for SRC stream */
};


/** Internal status + configuration data */
typedef struct
{
	tBool                           bAlsaIsOpen;
	/**< Alsa open flag */
	snd_pcm_t                      *pAlsaPCM;
	/**< pointer to handle for PCM Alsa device */
	char  szAlsaDeviceName[ACOUSTICSRC_ALSA_DEVICE_NAME_BUFFER_SIZE];
	    /**< device name as configured in .asoundrc */
	    /**< device name as configured in .asoundrc, OEDT name*/
	snd_output_t                    *pSndOutput;
	/**< alsa message output handle*/
	snd_pcm_hw_params_t            *pPCM_hw_params;
	/**< pointer to hardware config for PCM Alsa device */

	enum ACOUSTICSRC_enFileHandles  enFileHandle;
	/**< file handle */
	OSAL_tenAcousticCodec           enCodec;
	/**< current decoder */
	trPCMConfigData                 rPCMConfig;
	/**< PCM config data */
	OSAL_tAcousticBuffersize        anBufferSize;
	/**< buffer size for each codec (PCM) */
	ACOUSTICSRC_enPlayStates        enPlayState;
	/**< current play state of the stream */
	OSAL_tEventHandle               hOsalEvent;
	/**< per-codec data */
	OSAL_trAcousticSrcCallbackReg   rCallback;
	/**< callback data */
	OSAL_tSemHandle                 hSemClose;
	/**< semaphore protecting closing of device*/
	
   /* end formerly rDSPIf*/
} trACOUSTICSRC_StateData, *pACOUSTICSRC_StateData;


/** event masks for output device communication */
enum
{
   ACOUSTICSRC_EN_EVENT_MASK_AUDIO_NONE      = 0x00000000,
   ACOUSTICSRC_EN_EVENT_MASK_AUDIO_PCM_ACK   = 0x00000001,
   ACOUSTICSRC_EN_EVENT_MASK_AUDIO_AMR_ACK   = 0x00000002,
   ACOUSTICSRC_EN_EVENT_MASK_AUDIO_ABORT_ACK = 0x00000004, 
   ACOUSTICSRC_EN_EVENT_MASK_AUDIO_CANCEL    = 0x00000008,

	ACOUSTICSRC_EN_EVENT_MASK_AUDIO_ANY =
	(ACOUSTICSRC_EN_EVENT_MASK_AUDIO_PCM_ACK   |
	 ACOUSTICSRC_EN_EVENT_MASK_AUDIO_AMR_ACK   |
	 ACOUSTICSRC_EN_EVENT_MASK_AUDIO_ABORT_ACK |
	 ACOUSTICSRC_EN_EVENT_MASK_AUDIO_CANCEL )
};

/** event masks for notification types
*
* @note These events are signalled whenever an event shall be signalled to
* the client via event loop.
* The same event group is used as for the output device events, therefore
* the values must not overlap.
*/
enum
{
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED =           0x00020000,
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_XRUN =         0x00100000,
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_BITSTREAM =    0x00200000,
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_NOVALIDDATA =  0x00400000,
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_WRONGFORMAT =  0x00800000,
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_INTERNALERR =  0x01000000,
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_FATAL =        0x02000000,
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_CANCEL =                 0x04000000,

	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ANY =
	(ACOUSTICSRC_EN_EVENT_MASK_NOTI_AUDIOSTOPPED |
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_XRUN |
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_BITSTREAM |
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_NOVALIDDATA |
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_WRONGFORMAT |
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_INTERNALERR |
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_ERRTHRESH_FATAL |
	ACOUSTICSRC_EN_EVENT_MASK_NOTI_CANCEL)
};


/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/** maximum number of parallely loaded codecs for *one* device instance
*
* For now, each device instance gets the same number of max. codec instances.
*/
#define ACOUSTICSRC_C_U8_MAXCODECCNT                 ((tU8)1)

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/


#ifdef __cplusplus
}
#endif
     
#else /*if !defined (ACOUSTICSRCPRIVATE_H)*/
#error acousticsrc_private.h included several times
#endif /*else if !defined (ACOUSTICSRC_PRIVATE_H)*/

