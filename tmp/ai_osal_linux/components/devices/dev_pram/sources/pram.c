/******************************************************************************
 **********                                                          **********
 *******           �        IMPLEMENTATION FILE                         *******
 **********                                                          **********
 ******************************************************************************
 ******************************************************************************
 ***** (C) COPYRIGHT Robert Bosch GmbH CM-DI/PJ ECO2 - All Rights Reserved *****
 ******************************************************************************/
/******************************************************************************/



 /******************************************************************************
 *******                           INCLUDES                             *******
 ******************************************************************************/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"

#define __INCLUDED_FROM_PRAM_DRIVER__
#include <pram.h>
#include <pram_config.h>

#ifndef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif

/******************************************************************************
 *******                             DEFINES                            *******
 ******************************************************************************/

#define SRAM_MAGIC_VALID (0xd0e1d0e1)
#define SRAM_MAGIC_VALID_INVERS (~(SRAM_MAGIC_VALID))
#define PRAM_MAGIC_VALID (SRAM_MAGIC_VALID +PRAM_VERSION_NUMBER)
#define PRAM_MAGIC_VALID_INVERS (~(PRAM_MAGIC_VALID))

#define ROUND_UP_TO_LONG(x) (((x)+3) & 0xfffffffc)

/******************************************************************************
 *******                     FORWARD DECLARATIONS                       *******
 ******************************************************************************/

/******************************************************************************
 *******                           CONSTANTS                            *******
 ******************************************************************************/

unsigned long pram_size = 0;

#define MAXIMUM_PRAM_AREA 0x200 /* 1 K */
#define PRAM_SEM_NAME     "PRAM_Semaphore"

#define ALIGN_ULONG(x) ((unsigned long)(x)&~(EXEC_PAGESIZE-1))
#define ALIGN_VOIDP(x) ((void *)(ALIGN_ULONG(x)))

//#define TRACE_PRAM
//#define ACTIVATE_CONTENT_CHECK
/******************************************************************************
 *******                        GLOBAL VARIABLES                        *******
 ******************************************************************************/
static OSAL_tSemHandle SemPRam   = OSAL_C_INVALID_HANDLE;
/* Linux driver handle */
static tS32 fdesc = -1;
static tU32 u32PrcLocalOpenCnt = 0;

static  char cBuffer[MAXIMUM_PRAM_AREA];

void vCheckSemphore(void);

/******************************************************************************
 *******                         IMPLEMENTATION                         *******
 ******************************************************************************/
static void vTracePram(char* test)
{
#ifdef TRACE_PRAM
   TraceString(test);
#else
   ((void)test);
#endif
}

static tS32 synchronizeAccess(tBool ready)
{
    // Only if there is the possibility that a taskswitch can occur, synchronination is necessary
    tS32 s32Ret = 0;
    vCheckSemphore();
    if (ready == TRUE)
    {
       vTracePram("synchronizeAccess Post ");
       if((s32Ret = OSAL_s32SemaphorePost(SemPRam)) == OSAL_ERROR)
       {
           TraceString("PRAM OSAL_s32SemaphorePost failed %d",OSAL_u32ErrorCode() );
           return synchronizeAccess(ready);
       }
       return s32Ret;
    }
    else
    {
       vTracePram("synchronizeAccess Wait ");
       if((s32Ret = OSAL_s32SemaphoreWait(SemPRam,OSAL_C_TIMEOUT_FOREVER)) == OSAL_ERROR)
       {
           TraceString("PRAM OSAL_s32SemaphoreWait failed %d",OSAL_u32ErrorCode());
           return synchronizeAccess(ready);
       }
       return s32Ret;
    }
}

tBool bWritePRAM(void* buffer, tU32 u32Size)
{
   tBool bRet = FALSE;

#ifdef OSAL_GEN3
   fdesc = open("/dev/pram", O_RDWR);
   if(fdesc != -1)
   {
      if(u32Size <= MAXIMUM_PRAM_AREA)
      {
#ifdef ACTIVATE_CONTENT_CHECK
         TraceString("PRAM bWritePRAM Size:%d",u32Size);
#endif
         lseek(fdesc,0,SEEK_SET);
         if(write(fdesc,buffer ,u32Size) > 0)
         {
             bRet = TRUE;
         }
         else
         {
            TraceString("PRAM bWritePRAM write failed Error:%d",errno);
         }
      }
      else
      {
         TraceString("PRAM bWritePRAM u32Size(%d) > MAXIMUM_PRAM_AREA:%d",
                     u32Size,MAXIMUM_PRAM_AREA);
      }
      close(fdesc);
      fdesc = -1;
   }
   else
   {
      TraceString("PRAMbWritePRAM open /dev/pram failed Error:%d",errno);
   }
#else
   if(fdesc == -1)
   {
      snprintf(cBuffer,MAXIMUM_PRAM_AREA,"bWritePRAM write failed No Sim Buffer allocated");
      TraceString(cBuffer);
   }
   else
   {
      bRet = TRUE;
   }
#endif
   return bRet;
}

tBool bReadPRAM(void* buffer, tU32 u32Size)
{
   tBool bRet = FALSE;

#ifdef OSAL_GEN3
   fdesc = open("/dev/pram",O_RDONLY);
   if(fdesc != -1)
   { 
#ifdef ACTIVATE_CONTENT_CHECK
      TraceString("PRAM bReadPRAM Size:%d",u32Size);
#endif
      lseek(fdesc,0,SEEK_SET);
      if (read(fdesc,buffer,u32Size) > 0)
      {
         bRet = TRUE;
      }
      else
      {
         TraceString("PRAM bReadPRAM read failed Error:%d",errno);
      }
      close(fdesc);
      fdesc = -1;
   }
   else
   {
      TraceString("PRAM bReadPRAM open /dev/pram failed Error:%d",errno);
   }
#else
   if(fdesc == -1)
   {
      fdesc = malloc(MAXIMUM_PRAM_AREA);
      if(buffer)
      {
         memset(buffer,0,MAXIMUM_PRAM_AREA);
      }
   }
   if(fdesc != -1)bRet = TRUE;
#endif
   return bRet;
}

static tBool bReadCheckPramContent(void* buffer, tU32 u32Size)
{
    vTracePram("pram bReadCheckPramContent");
#ifdef ACTIVATE_CONTENT_CHECK
     int i=0;
     char cReadBuffer[MAXIMUM_PRAM_AREA];
     tBool bStatus = bReadPRAM((void*)cReadBuffer,MAXIMUM_PRAM_AREA);
     for(i=0;i<MAXIMUM_PRAM_AREA;i++)
     {
       if(cReadBuffer[i] != cBuffer[i])
       {
          TraceString("PRAM Index:%d cBuffer:%c cReadBuffer:%d",i,cBuffer[i],cReadBuffer[i]);
          bStatus = FALSE;
       }
     }
     if(bStatus == TRUE)
     {
         memcpy(buffer,(void*)cReadBuffer,MAXIMUM_PRAM_AREA);
     }
#else
     return bReadPRAM(buffer,u32Size);
#endif
}

void vPrintPramEntries(int ID)
{
#ifdef ACTIVATE_CONTENT_CHECK
    PramConfigEntry* pce = &(_pram_config[ID]);
    TraceString("Entry ID:%d Size:%d Written Size:%d Adress:0x%x",
                 pce->ptr->e.id,
                 pce->ptr->e.size,
                 pce->ptr->e.written_size,
                 &(_pram_config[ID]));
#else
    ((void) ID);
#endif
}

/******************************************************************************/
/*!@fn ER pram_reset_entry(PramConfigEntry *pce)
 *
 * @param pce  entry from the configuration
 *
 * @return -
 *
 * @brief function resets the entry in PRAM to the deafults of the configuration
 *        error memory area needs special treatment
 ******************************************************************************/
static tS32 pram_reset_entry (PramConfigEntry *pce)
{
    vTracePram("pram_reset_entry");
    if((pce->id == (int)PRAM_RESET_COUNTER)||(pce->id == (int)PRAM_DNL_MAGIC))
    {
        pce->ptr->e.magic = SRAM_MAGIC_VALID;
        pce->ptr->e.magic_invers = SRAM_MAGIC_VALID_INVERS;
        pce->ptr->e.written_size = pce->size;
     //   memset(pce->ptr->data, 0, pce->size);
    }
    else
    {
        pce->ptr->e.magic = PRAM_MAGIC_VALID;
        pce->ptr->e.magic_invers = PRAM_MAGIC_VALID_INVERS;
        pce->ptr->e.written_size = 0;
    }
    if (pce->size == PRAM_SIZE_UNTIL_END)
    {
        pce->size = pram_size;
    }
    else
    {
        pce->ptr->e.size = pce->size;
    }
    pce->ptr->e.id = pce->id;
    return OSAL_OK;
}

/******************************************************************************/
/*!@fn ER pram_is_entry_valid(PramConfigEntry *pce)
 *
 * @param pce  entry from the configuration
 *
 * @return -
 *
 * @brief function returns OSAL_OK if entry is valid
 ******************************************************************************/

static tS32 pram_is_entry_valid(const PramConfigEntry *pram_config_entry)
{
    if((pram_config_entry->id == (int)PRAM_RESET_COUNTER)||(pram_config_entry->id == (int)PRAM_DNL_MAGIC))
    {
        if ((pram_config_entry->ptr->e.magic != SRAM_MAGIC_VALID)
        ||(pram_config_entry->ptr->e.magic_invers != SRAM_MAGIC_VALID_INVERS)
        ||(pram_config_entry->ptr->e.size != pram_config_entry->size)
        ||(pram_config_entry->ptr->e.id != pram_config_entry->id))
        {
            return OSAL_ERROR;
        }
    }
    else
    {
        if ((pram_config_entry->ptr->e.magic != PRAM_MAGIC_VALID)
        ||(pram_config_entry->ptr->e.magic_invers != PRAM_MAGIC_VALID_INVERS)
        ||(pram_config_entry->ptr->e.size != pram_config_entry->size)
        ||(pram_config_entry->ptr->e.id != pram_config_entry->id))
        {
            return OSAL_ERROR;
        }
    }
    return OSAL_OK;
}

/******************************************************************************/
/*!@fn INT pram_read_unsyncronized (DrvPramId id, UINT size, VP buf,
 *                                unsigned long offset, unsigned long* newOffset)
 *
 * @param id : id of the entry
 * @param size : maximum size to read
 * @param buf : pointer to read data in
 * @param offset : offset in device
 * @param newOffset : hold return value for offset after reading
 *                         (can be set to NULL to ignore)
 *
 * @return OSAL_E_CANCELED : error
 *         > 0     : size of data really read in
 *
 * @brief function reads data from the given device. This is the unsynchronized 
          version that can be used more than once in one synchronisation area.
 ******************************************************************************/
static tS32 pram_read_unsyncronized (DrvPramId id, tS32 size, void* buf, tS32 offset, tS32* newOffset)
{
    vTracePram("pram_read_unsyncronized");
    PramConfigEntry *pce;
    tS32 size_to_read;

    bReadCheckPramContent((void*)cBuffer,MAXIMUM_PRAM_AREA);
    vPrintPramEntries(id);

    pce = & (_pram_config[id]);
    if(pce->ptr->e.written_size == 0)
    {
       TraceString("PRAM %s Error:%s","pram_read_unsyncronized","OSAL_E_CANCELED");
       return OSAL_E_CANCELED;
    }
    if ((tU32)offset >= pce->ptr->e.written_size)
    {
       TraceString("PRAM %s Error:%s","pram_read_unsyncronized","OSAL_E_CANCELED");
       return OSAL_E_CANCELED;
    }
    size_to_read = MIN(((tS32)pce->ptr->e.written_size) - offset, size);
    /* size_to_read > 0, because  offset < pce->ptr->e.size */
    memcpy(buf, pce->ptr->data + offset, size_to_read);
    if (newOffset)
    {
        *newOffset = offset + size_to_read;
    }

#ifdef ACTIVATE_CONTENT_CHECK
    TraceString("Read ID:%d Size:%d offset:%d",id,size,offset);
#endif
    return size_to_read;
}

/******************************************************************************/
/*!@fn INT pram_read (DrvPramId id, UINT size, VP buf,
 *                                unsigned long offset, unsigned long* newOffset)
 *
 * @param id : id of the entry
 * @param size : maximum size to read
 * @param buf : pointer to read data in
 * @param offset : offset in device
 * @param newOffset : hold return value for offset after reading
 *                         (can be set to NULL to ignore)
 *
 * @return OSAL_E_CANCELED : error
 *         > 0     : size of data really read in
 *
 * @brief function reads data from the given device. This is the synchronized 
          version. Two calls to this are interruptable from other calls.
 ******************************************************************************/

tS32 pram_read (DrvPramId id, tU32 size, void* buf, unsigned long offset, unsigned long* newOffset)
{
    vTracePram("pram_read ");
    tS32 ret_value;
    void *paged_addr;
    unsigned char vec[1];

    paged_addr = ALIGN_VOIDP(buf);
    if(mincore(paged_addr, EXEC_PAGESIZE, vec) != 0)
    {
        TraceString("PRAM %s Error:%s","pram_read","OSAL_E_INVALIDVALUE");
        return OSAL_E_INVALIDVALUE;
    }
    if (id >= PRAM_END_OF_LIST)
    {
        TraceString("PRAM %s Error:%s","pram_read","OSAL_E_INVALIDVALUE");
        return OSAL_E_INVALIDVALUE;
    }
    if (synchronizeAccess(FALSE) != OSAL_OK)
    {
        /* No bytes could be read */
        TraceString("PRAM %s Error:%s","pram_read","OSAL_E_CANCELED");
        return OSAL_E_CANCELED;
    } 

    ret_value = pram_read_unsyncronized(id, size, buf, (tS32)offset, (tS32 *)newOffset);
    if (synchronizeAccess(TRUE) != OSAL_OK)
    {
        TraceString("PRAM %s Error:%s","pram_read","OSAL_E_CANCELED");
        return OSAL_E_CANCELED;
    }
    vPrintPramEntries(id);

    return ret_value;
}

/******************************************************************************/
/*!@fn INT pram_write (DrvPramId id, UINT size, VP buf,
 *                                unsigned long offset, unsigned long* newOffset)
 *
 * @param id : id of the entry
 * @param size : size to write
 * @param buf : pointer to write data from
 * @param offset : offset in device
 * @param newOffset : hold return value for offset after writing
 *                         (can be set to NULL to ignore)
 *
 * @return OSAL_E_CANCELED : error
 *         > 0     : size of data really written
 *
 * @brief function writes data to the given device. There is only
 *        this synchornized version.
 ******************************************************************************/
tU32 pram_write(DrvPramId id, tU32 size, void* buf, unsigned long offset, unsigned long* newOffset)
{
    vTracePram("pram_write ");
    PramConfigEntry *pce;
    int size_to_write;
    int writepos;
    void *paged_addr;
    unsigned char vec[1];

    paged_addr = ALIGN_VOIDP(buf);
    if(mincore(paged_addr, EXEC_PAGESIZE, vec) != 0)
    {
        TraceString("PRAM %s Error:%s","pram_write","OSAL_E_NOPERMISSION");
        return OSAL_E_NOPERMISSION;
    }
    if (id >= PRAM_END_OF_LIST)
    {
        TraceString("PRAM %s Error:%s","pram_write","OSAL_E_INVALIDVALUE");
        return OSAL_E_INVALIDVALUE;
    }

    vPrintPramEntries(id);

    pce = &(_pram_config[id]);
    if (offset >= pce->ptr->e.size)
    {
       TraceString("PRAM %s Error:%s","pram_write","OSAL_E_CANCELED");
       return OSAL_E_CANCELED;
    }
    size_to_write = (tS32)MIN(pce->ptr->e.size - offset, size);
    /* size_to_write > 0, because  offset < pce->ptr->e.size */
    memcpy(pce->ptr->data + offset, buf, (unsigned long)size_to_write);
    writepos = (tS32)offset + (tS32)size_to_write;
    if (newOffset)
    {
        *newOffset = (unsigned long)writepos;
    }
#ifdef ACTIVATE_CONTENT_CHECK
    TraceString("PRAM Write ID:%d size_to_write:%d offset:%d",id,size_to_write,offset);
#endif
    if (writepos > (tS32)(pce->ptr->e.written_size))
    {
       pce->ptr->e.written_size = (unsigned long)writepos;
    }
    else
    {
#ifdef ACTIVATE_CONTENT_CHECK
       TraceString("PRAM written_size:%d writepos:%d",pce->ptr->e.written_size,writepos);
#endif
    }
    bWritePRAM((void*)cBuffer,MAXIMUM_PRAM_AREA);
    return size_to_write;
}

/******************************************************************************/
/*!@fn NT pram_seek(DrvPramId id, INT position, unsigned long *new_position)
 *
 * @param id : id of the entry
 * @param position : possition to seek to
 * @param new_position : hold new position after seeking
 *                         (can be set to NULL to ignore)
 *
 * @return OSAL_E_CANCELED : error
 *         > 0     : size of data really written
 *
 * @brief function writes data to the given device. There is only
 *        this synchornized version.
 ******************************************************************************/
static tU32 pram_seek(DrvPramId id, int position, unsigned long *new_position)
{
    vTracePram("pram_seek ");
    tU32 ret_value = OSAL_E_UNKNOWN ;
    PramConfigEntry *pce;
    pce = &(_pram_config[id]);

    if (position < 0)
    {
        TraceString("PRAM pram_seek Error:%s","OSAL_E_CANCELED1");
        return OSAL_E_CANCELED;
    }
    if (pce->ptr->e.written_size < (unsigned long)position)
    {
        TraceString("PRAM pram_seek Error:%s","OSAL_E_CANCELED2");
        ret_value = OSAL_E_CANCELED;
    }
    else
    {
        if (new_position)
        {
            *new_position = (unsigned long)position;
        }
        ret_value = OSAL_E_NOERROR;
    }
    return ret_value;
}


/******************************************************************************/
/*!@fn NT pram_get_entry_by_name(const char *name)
 *
 * @param name : name of the device entry
 *
 * @return PRAM_END_OF_LIST : name not found
 *         else ID if the device entry
 *
 * @brief function writes data to the given device. There is only
 *        this synchornized version.
 ******************************************************************************/

static DrvPramId pram_get_entry_by_name(const char *name)
{
    PramConfigEntry * pram_config_entry;
    void *paged_addr;
    unsigned char vec[1];

    paged_addr = ALIGN_VOIDP(name);
    if(mincore(paged_addr, EXEC_PAGESIZE, vec) != 0)
    {
        return PRAM_END_OF_LIST;
    }
    for (pram_config_entry = _pram_config;;++pram_config_entry)
    {
        if (pram_config_entry->name == NULL)
        {
            break;
        }
        else
        {
            if (strcmp(pram_config_entry->name, name) == 0)
            {
                return(DrvPramId)(pram_config_entry->id);
            }
        }
    }
    return PRAM_END_OF_LIST;
}

/******************************************************************************/
/******************************************************************************/

void vCheckSemphore(void)
{
    // Semaphore that guards the access to the PRAM
    if(SemPRam == OSAL_C_INVALID_HANDLE)
    {
       if(OSAL_OK != OSAL_s32SemaphoreCreate(PRAM_SEM_NAME,&SemPRam,1))
       {
           if(OSAL_u32ErrorCode() == OSAL_E_ALREADYEXISTS)
           {
              if(OSAL_ERROR == OSAL_s32SemaphoreOpen( PRAM_SEM_NAME,&SemPRam ))
              {
                FATAL_M_ASSERT_ALWAYS();
              }
           }
           else
           {
              FATAL_M_ASSERT_ALWAYS();
           }
       }
    }
}

tU32 u32InitPramStruct(void)
{
   vTracePram("u32InitPramStruct start");
   PramConfigEntry * pram_config_entry;
   unsigned long offset;
   unsigned long size;
   unsigned long i;
   tU32 u32Ret = OSAL_E_NOERROR;

   /* check if local table has to be setup */
   if(_pram_config[0].ptr == NULL)
   {
 //     TraceString("Setup PRAM data for PID:%d ",getpid());
      /* setup configuration */
      offset = 0;
      for (pram_config_entry = _pram_config, i=0; ; ++pram_config_entry, ++i)
      {
         size = pram_config_entry->size;
         if (size == PRAM_SIZE_UNTIL_END)
         {
            pram_size = (MAXIMUM_PRAM_AREA - offset) - sizeof(PramEntryHeader);
            pram_config_entry->size = pram_size;
         }

         if (pram_config_entry->id != i)
         {
           TraceString("u32InitPramStruct error pram_config_entry->id:%d i:%d",pram_config_entry->id,i);
           u32Ret = OSAL_E_CANCELED;
           break;
         }
         if (offset >= MAXIMUM_PRAM_AREA)
         {
            if(i < PRAM_END_OF_LIST)
            {
               TraceString("u32InitPramStruct error MAXIMUM_PRAM_AREA offset:0x%x",offset);
               u32Ret = OSAL_E_CANCELED;
            }
            break;
         }
         if(i == (int)PRAM_RESET_COUNTER)
         {
            pram_config_entry->ptr = (PramEntry *)&cBuffer[0];
         }
         else
         {
            pram_config_entry->ptr =  (PramEntry *)(&cBuffer[0] + offset); /*lint !e826*/
         }
         if (pram_is_entry_valid(pram_config_entry) != OSAL_OK)
         {
           if (pram_reset_entry(pram_config_entry) != OSAL_OK)
           {
              TraceString("u32InitPramStruct error pram_reset_entry ");
              u32Ret = OSAL_E_CANCELED;
              break;
           }
  //         TraceString("Invalid PRAM data for ID:%d Size:%d", pram_config_entry->ptr->e.id,pram_config_entry->ptr->e.size);
         }
         if (size == PRAM_SIZE_UNTIL_END)
         {
           pram_config_entry->size = PRAM_SIZE_UNTIL_END;
           break;
         }
//#ifdef ACTIVATE_CONTENT_CHECK
         TraceString("Entry ID:%d Size:%d Written Size:%d Adress:0x%x",
                     pram_config_entry->ptr->e.id,
                     pram_config_entry->ptr->e.size,
                     pram_config_entry->ptr->e.written_size,
                     &cBuffer[0] + offset);
//#endif

         offset += ROUND_UP_TO_LONG(pram_config_entry->size) + sizeof(PramEntryHeader);
       }

       if(bWritePRAM((void*)cBuffer,MAXIMUM_PRAM_AREA) == FALSE)
       {
          TraceString("u32InitPramStruct bWritePRAM failed");
          u32Ret = OSAL_E_CANCELED;
       }
    }

    vTracePram("u32InitPramStruct end");
    return u32Ret;
}


/******************************************************************************/
/*!@fn tS32 pram_io_open (tS32 s32Id, tCString szName, OSAL_tenAccess enAccess,
 *                        tU32 *pu32FD, tU16 appid)
 *
 * @param  s32Id : id of the device (ignored)
 * @param  szName : name of the device entry
 * @param  enAccess : access mode (ignored)
 * @param  pu32FD : pointer to hold the created internal descriptor
 * @param  appid  : not used here
 * @return OSAL_E_NOERROR if all ok
 * 
 * @brief open function for OSAL
 ******************************************************************************/
tS32 pram_io_open (tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16 appid)
{
#ifndef _LINUXX86_64_
    unsigned long id;
    PramFileDescriptor *descriptor;
    vTracePram("pram_io_open start");

    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Id);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enAccess);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(appid);

    if(synchronizeAccess(FALSE) != OSAL_OK)
    {
        TraceString("pram_io_open error synchronizeAccess -> TRUE");
        return OSAL_E_CANCELED;
    }

    u32PrcLocalOpenCnt++;
    if(_pram_config[0].ptr == NULL)
    {
         if(bReadPRAM((void*)cBuffer,MAXIMUM_PRAM_AREA) == FALSE)
         {
            TraceString("PRAM %s Error:%s","pram_io_open","OSAL_E_UNKNOWN");
            (void)synchronizeAccess(TRUE);
            return OSAL_E_UNKNOWN;
         }
    }

    if(u32InitPramStruct() != OSAL_E_NOERROR)
    {
         TraceString("PRAM %s Error:%s","pram_io_open","OSAL_E_NOTINITIALIZED");
         (void)synchronizeAccess(TRUE);
         return OSAL_E_NOTINITIALIZED;
    }

    if(synchronizeAccess(TRUE) == OSAL_ERROR)
    {
       TraceString("pram_io_open error synchronizeAccess -> TRUE");
       return OSAL_E_CANCELED;
    }

    id = (unsigned long)pram_get_entry_by_name(szName);
    if (id == (unsigned long)PRAM_END_OF_LIST)
    {
        *pu32FD = 0;
        TraceString("PRAM %s Error:%s","pram_io_open","OSAL_E_DOESNOTEXIST");
        return OSAL_E_DOESNOTEXIST;
    }
    descriptor = malloc(sizeof(PramFileDescriptor));
    if(descriptor)
    {
       descriptor->id = id;
       descriptor->position = 0;
       *pu32FD = (tU32)descriptor;
       vTracePram("pram_io_open end");
       return OSAL_E_NOERROR;          
    }
    else
    {
       TraceString("PRAM %s Error:%s","pram_io_open","OSAL_E_NOSPACE");
       return OSAL_E_NOSPACE;
    }
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Id);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(szName);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enAccess);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32FD);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(appid);
    return OSAL_E_NOERROR;
#endif
}

/******************************************************************************/
/*!@fn tS32 pram_io_close (tS32 s32Id, tU32 u32FD)
 *
 * @param  s32Id : id of the device (ignored)
 * @param  szName : name of the device entry
 * @param  u32FD : the internal descriptor
 * @return OSAL_E_NOERROR if all ok
 * 
 * @brief close function for OSAL
 ******************************************************************************/
tS32 pram_io_close (tS32 s32ID, uintptr_t u32FD)
{
#ifndef _LINUXX86_64_
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    vTracePram("pram_io_close start");

    if(synchronizeAccess(FALSE) != OSAL_OK)
    {
        TraceString("pram_io_close error synchronizeAccess -> TRUE");
        return OSAL_E_CANCELED;
    }
 
    if(u32PrcLocalOpenCnt > 0)u32PrcLocalOpenCnt--;
   
    if(synchronizeAccess(TRUE) == OSAL_ERROR)
    {
       TraceString("pram_io_close error synchronizeAccess -> TRUE");
       return OSAL_E_CANCELED;
    }
 
    if(u32PrcLocalOpenCnt == 0)
    {
      OSAL_s32SemaphoreClose(SemPRam);
      SemPRam = OSAL_C_INVALID_HANDLE;
    }
    free((void *)u32FD);

    vTracePram("pram_io_close end");

    return OSAL_E_NOERROR;
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
    return OSAL_E_NOERROR;
#endif
}

/******************************************************************************/
/*!@fn tS32 pram_io_ctrl (tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32Arg)
 *
 * @param  s32Id : id of the device (ignored)
 * @param  u32FD : the internal descriptor
 * @param  s32fun : function of IO Control
 * @param  s32Arg : argument, usage depends on IOCTRL
 *             OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK             :  tS32  :position to seek to 
 *             OSAL_C_S32_IOCTRL_DEV_PRAM_TELL             :  tU32 *: pointer for the actual position
 *             OSAL_C_S32_IOCTRL_DEV_PRAM_GET_SIZE         :  tU32 *: pointer for maximum bytes to write
 *             OSAL_C_S32_IOCTRL_DEV_PRAM_GET_WRITTEN_SIZE :  tU32 *: pointer for written bytes
 *             OSAL_C_S32_IOCTRL_DEV_PRAM_CLEAR            :  void  : ignored 
 *
 * @return OSAL_E_NOERROR if all ok
 * 
 * @brief ioctrl function for OSAL
 ******************************************************************************/
tS32 pram_io_ctrl (tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32Arg)
{
#ifndef _LINUXX86_64_
    vTracePram("pram_io_ctrl ");
    tU32 *io_ctrl_tu32_arg;
    void *paged_addr;
    unsigned char vec[1];
    tU32 u32Ret = OSAL_E_NOERROR;

    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    PramFileDescriptor *descriptor;

    descriptor = (PramFileDescriptor *)u32FD;
    if (descriptor->id >= (int)PRAM_END_OF_LIST)
    {
        TraceString("PRAM %s Error:%s","pram_io_ctrl","OSAL_E_BADFILEDESCRIPTOR");
        return OSAL_E_BADFILEDESCRIPTOR;
    }

    vPrintPramEntries(descriptor->id);
    switch (s32fun)
    {
        case OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK:
             vTracePram("pram_io_ctrl OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK");
             if (pram_seek((DrvPramId)(descriptor->id), s32Arg, &(descriptor->position)) != OSAL_E_NOERROR)
             {
                TraceString("PRAM %s CTRL:%s Error:%s","pram_io_ctrl","OSAL_C_S32_IOCTRL_DEV_PRAM_SEEK","OSAL_E_INVALIDVALUE");
                u32Ret = OSAL_E_INVALIDVALUE;
             }
             break;
        case OSAL_C_S32_IOCTRL_DEV_PRAM_TELL:
             vTracePram("pram_io_ctrl OSAL_C_S32_IOCTRL_DEV_PRAM_TELL");
             /* s32Arg is a pointer */
             io_ctrl_tu32_arg = (tU32 *)s32Arg;
             paged_addr = ALIGN_VOIDP(io_ctrl_tu32_arg);
             if(mincore(paged_addr, EXEC_PAGESIZE, vec) != 0)
             {
                TraceString("PRAM %s CTRL:%s Error:%s","pram_io_ctrl","OSAL_C_S32_IOCTRL_DEV_PRAM_TELL","OSAL_E_INVALIDVALUE");
                u32Ret = OSAL_E_INVALIDVALUE;
             }
             else
             {
                *io_ctrl_tu32_arg = (tU32)(descriptor->position);
             }
             break;
        case OSAL_C_S32_IOCTRL_DEV_PRAM_GET_SIZE:
             vTracePram("pram_io_ctrl OSAL_C_S32_IOCTRL_DEV_PRAM_GET_SIZE");
             io_ctrl_tu32_arg = (tU32 *)s32Arg;
             paged_addr = ALIGN_VOIDP(io_ctrl_tu32_arg);
             if(mincore(paged_addr, EXEC_PAGESIZE, vec) != 0)
             {
                TraceString("PRAM %s CTRL:%s Error:%s","pram_io_ctrl","OSAL_C_S32_IOCTRL_DEV_PRAM_GET_SIZE","OSAL_E_INVALIDVALUE");
                u32Ret = OSAL_E_INVALIDVALUE;
             }
             else
             {
                *io_ctrl_tu32_arg = _pram_config[descriptor->id].ptr->e.size;
             }
             break;
        case OSAL_C_S32_IOCTRL_DEV_PRAM_GET_WRITTEN_SIZE:
             vTracePram("pram_io_ctrl OSAL_C_S32_IOCTRL_DEV_PRAM_GET_WRITTEN_SIZE");
             io_ctrl_tu32_arg = (tU32 *)s32Arg;
             paged_addr = ALIGN_VOIDP(io_ctrl_tu32_arg);
             if(mincore(paged_addr, EXEC_PAGESIZE, vec) != 0)
             {
                TraceString("PRAM %s CTRL:%s Error:%s","pram_io_ctrl","OSAL_C_S32_IOCTRL_DEV_PRAM_GET_WRITTEN_SIZE","OSAL_E_INVALIDVALUE");
                u32Ret = OSAL_E_INVALIDVALUE;
             }
             else
             {
                *io_ctrl_tu32_arg = _pram_config[descriptor->id].ptr->e.written_size;
             }
             break;
        case OSAL_C_S32_IOCTRL_DEV_PRAM_CLEAR:
             vTracePram("pram_io_ctrl OSAL_C_S32_IOCTRL_DEV_PRAM_CLEAR");
             if(synchronizeAccess(FALSE) != OSAL_OK)
             {
                u32Ret = OSAL_E_CANCELED;
             }
             bReadCheckPramContent((void*)cBuffer,MAXIMUM_PRAM_AREA);
             if (pram_reset_entry(&(_pram_config[descriptor->id])) != OSAL_OK)
             {
                TraceString("PRAM %s CTRL:%s Error:%s","pram_io_ctrl","OSAL_C_S32_IOCTRL_DEV_PRAM_CLEAR","OSAL_E_BUSY");
                u32Ret = OSAL_E_BUSY;
             }
             else
             {
                bWritePRAM((void*)cBuffer,MAXIMUM_PRAM_AREA);
                descriptor->position = 0;
             }
             if(synchronizeAccess(TRUE) != OSAL_OK)
             {
                u32Ret = OSAL_E_CANCELED;
             }
             break;
        default:
             TraceString("PRAM %s CTRL:%s Error:%s","pram_io_ctrl","Unknown","OSAL_E_DOESNOTEXIST");
             u32Ret = OSAL_E_DOESNOTEXIST;
             break;
    }
    vPrintPramEntries(descriptor->id);
    return u32Ret;
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32fun);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Arg);
    return OSAL_E_NOERROR;
#endif
}

/******************************************************************************/
/*!@fn tS32 pram_io_read (tS32 s32ID, tU32 u32FD, tPS8 pBuffer, 
 *                        tU32 u32Size, tU32 *u32_ret_Size)
 *
 * @param  s32Id : id of the device (ignored)
 * @param  u32FD : the internal descriptor
 * @param  pBuffer : buffer to read data in
 * @param  u32Size : maximum size to read
 * @param  u32_ret_Size : pointer to really read size
 * @return OSAL_E_NOERROR if all ok
 * 
 * @brief read function for OSAL
 ******************************************************************************/

tS32 pram_io_read (tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size)
{
#ifndef _LINUXX86_64_
    vTracePram("pram_io_read ");
    PramFileDescriptor *descriptor;
    int ret_value;

    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);

    descriptor = (PramFileDescriptor *)u32FD;
    if (descriptor->id >= (int)PRAM_END_OF_LIST)
    {
        TraceString("PRAM %s Error:%s","pram_io_read","OSAL_E_BADFILEDESCRIPTOR");
        return OSAL_E_BADFILEDESCRIPTOR;
    }

    ret_value = pram_read((DrvPramId)(descriptor->id), u32Size, 
                          pBuffer, descriptor->position, &(descriptor->position));
    switch(ret_value)
    {
       case OSAL_E_CANCELED:
            TraceString("PRAM %s Error:%s","pram_io_read","OSAL_E_CANCELED");
            break;
       case OSAL_E_INVALIDVALUE:
            TraceString("PRAM %s Error:%s","pram_io_read","OSAL_E_INVALIDVALUE");
            break;
       default:
            *u32_ret_Size = (tU32)ret_value;
            return ret_value;
            break;
    }

    return ret_value;
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pBuffer);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32Size);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32_ret_Size);
    return OSAL_E_NOERROR;
#endif
}

/******************************************************************************/
/*!@fn tS32 pram_io_write (tS32 s32ID, tU32 u32FD, tPS8 pBuffer, 
 *                        tU32 u32Size, tU32 *u32_ret_Size)
 *
 * @param  s32Id : id of the device (ignored)
 * @param  u32FD : the internal descriptor
 * @param  pBuffer : buffer to data to write
 * @param  u32Size : maximum size to write
 * @param  u32_ret_Size : pointer to really written size
 * @return OSAL_E_NOERROR if all ok
 * 
 * @brief write function for OSAL
 ******************************************************************************/

tS32 pram_io_write (tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size)
{
#ifndef _LINUXX86_64_
    vTracePram("pram_io_write");
    PramFileDescriptor *descriptor;
    int ret_value;

    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);

    descriptor = (PramFileDescriptor *)u32FD;
    if (descriptor->id >= (int)PRAM_END_OF_LIST)
    {
        TraceString("PRAM %s Error:%s","pram_io_write","OSAL_E_BADFILEDESCRIPTOR");
        return OSAL_E_BADFILEDESCRIPTOR;
    }
    if(synchronizeAccess(FALSE) != OSAL_OK)
    {
        TraceString("PRAM %s Error:%s","pram_io_write","OSAL_E_UNKNOWN");
        return OSAL_E_UNKNOWN;
    }
    bReadCheckPramContent((void*)cBuffer,MAXIMUM_PRAM_AREA);
    ret_value = pram_write((DrvPramId)(descriptor->id), u32Size,
                           (void*)pBuffer, descriptor->position, &(descriptor->position));
    *u32_ret_Size = (tU32)ret_value;
    if(synchronizeAccess(TRUE) != OSAL_OK)
    {
        return OSAL_E_UNKNOWN;
    }
    if (ret_value >= 0)
    {
#ifdef ACTIVATE_CONTENT_CHECK
      TraceString("PRAM %s Bytes:%d","pram_io_write",ret_value);
#endif
      return ret_value;
    }
    else
    {
       TraceString("PRAM %s Error:%s","pram_io_write","OSAL_E_NOSPACE");
       return OSAL_E_NOSPACE;
    }
#else
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pBuffer);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32Size);
    OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32_ret_Size);
    return OSAL_E_NOERROR;
#endif
}

