/******************************************************************************
 **********                                                          **********
 *******                        HEADER FILE                             *******
 **********                                                          **********
 ******************************************************************************
 ******************************************************************************
 ***** (C) COPYRIGHT Robert Bosch GmbH CM-DI/PJ GM3 - All Rights Reserved *****
 ******************************************************************************/
/******************************************************************************/
/*!@file    drv_sram.h
 * @author  Matthias Weise  CM-AI/PJ GM3
 *
 * @date    2008-09-11
 * Project  GM GE Architecture
 *
 * Contains the SDI driver for the SRAM.
 *****************************************************************************/
#ifndef __DRV_SRAM_H__
#define __DRV_SRAM_H__

#ifdef __cplusplus
extern "C" 
{
#endif


#define PRAM_SIZE_UNTIL_END 0


/* these structures are in permanent ram */
typedef struct {
    unsigned long magic;
    unsigned long magic_invers;
    unsigned long id;
    unsigned long size;
    unsigned long written_size;
} PramEntryHeader;

typedef struct {
    PramEntryHeader e;
    char data[1];
} PramEntry;

/* this struct is in normal memory */

typedef struct {
    const char *name;
    unsigned long id;
    unsigned long size;
    PramEntry *ptr;
} PramConfigEntry;

typedef struct {
    unsigned long id;
    unsigned long position;
} PramFileDescriptor;


#ifdef __cplusplus
}
#endif
 

#endif //  __DRV_SRAM_H__

