#ifndef __drv_pram_config_h_34545554543547575
#define __drv_pram_config_h_34545554543547575

/* count this version mumber up if you change the config !!! */
#define PRAM_VERSION_NUMBER 1

typedef enum {
    PRAM_RESET_COUNTER = 0,
    PRAM_DNL_MAGIC,
    PRAM_WAKEUP_REASON,
    PRAM_SHUTDOWN_ABORT_REASON,
    PRAM_FC_SPM,
    PRAM_RUN_STATE,
    PRAM_ORIGINAL_RESET_TYPE,
    PRAM_STARTUP_COUNTER,
    PRAM_OSAL,
    PRAM_DEV_WUP,
    PRAM_RESERVED,
    PRAM_END_OF_LIST
} DrvPramId;

#ifdef __INCLUDED_FROM_PRAM_DRIVER__

/* caution : the devices must be placed in same order like the ENUMs */
/*           this is done for sanity check while initializing        */
/* "errmem" must be always the last device !!!!                      */
/* the NULL pointer will be overwritten after init                   */

static PramConfigEntry _pram_config[]  = {
    {"reset_counter",   PRAM_RESET_COUNTER,                   4, NULL }, //   0 +   4 =   4
    {"dnl_magic",       PRAM_DNL_MAGIC,                       4, NULL }, //   4 +   4 =   8
    {"wakeup_reason",   PRAM_WAKEUP_REASON,                   8, NULL }, //   8 +   8 =  16
    {"shutdown_abort_reason",   PRAM_SHUTDOWN_ABORT_REASON,   8, NULL }, //  16 +   8 =  24
    {"fc_spm",          PRAM_FC_SPM,                        128, NULL }, //  24 + 128 = 152
    {"run_state",       PRAM_RUN_STATE,                       4, NULL }, // 152 +   4 = 156
    {"orig_reset_type", PRAM_ORIGINAL_RESET_TYPE,             4, NULL }, // 156 +   4 = 160
    {"startup_counter", PRAM_STARTUP_COUNTER,                 4, NULL }, // 160 +   4 = 164
    {"osal",            PRAM_OSAL,                           64, NULL }, // 164 +  64 = 228
    {"dev_wup",         PRAM_DEV_WUP,                        32, NULL }, // 228 +  32 = 260
    {"reserved",        PRAM_RESERVED,                      224, NULL }, // 260 + 224 = 484
    {0,                 PRAM_END_OF_LIST,                     0, NULL }
};

static tU32 u32PramOffset[] = {0,4,12,20,148,152,156,160,224,256,480};

#endif /* __INCLUDED_FROM_PRAM_DRIVER__ */

#endif

