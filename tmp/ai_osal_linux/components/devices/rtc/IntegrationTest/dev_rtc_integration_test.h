/*******************************************************************************
*
* FILE:         dev_rtc_integration_test.h
*
* SW-COMPONENT: Device RTC (Real Time Clock)
*
* PROJECT:      ADIT Gen3 Platform
*
* DESCRIPTION:  Integration tests.
*
* AUTHOR:       BSOT Andrea Bueter
*
* COPYRIGHT:    (c) 2013 Robert Bosch GmbH, Hildesheim
*
*******************************************************************************/

#ifndef _DEV_RTC_INTEGRATION_TEST_H_
#define _DEV_RTC_INTEGRATION_TEST_H_

/******************************************************************************/
#define RTC0 "/dev/rtc0"
/******************************************************************************/
/*                                                                            */
/* CLASS DECLARATION                                                          */
/*                                                                            */
/******************************************************************************/
#ifdef __cplusplus
class DevRtcEnvironment : public testing::Environment
{
  public:
    DevRtcEnvironment();
    ~DevRtcEnvironment();

    virtual tVoid SetUp();
    virtual tVoid TearDown();

  private:
    tBool bKmodCheckDriverload(const char* PpcDriverName);
    void  vStartProcessIncOutAndSendComponentStatus(tU8 Pu8Kind);
    void  vActivateGPSAntenna(void);
    void  vWriteKDSEntryCMVariantCodingGNSS(tU8 Vu8Gnss);
    tBool bCheckDriverRtcRxWorks(char* PpcDriverName);
};

/* -------------------------------------------------------------------------- */
class DevRtcFixture : public testing::Test
{
  protected:
	  int                my_ihRtcHandle;
	  DevRtcEnvironment* my_poRtcEnvironment;

	  virtual void SetUp()
	  {
		  my_ihRtcHandle = open(RTC0, O_RDWR);
	  }
	  virtual void TearDown()
	  {
		  my_ihRtcHandle = close(my_ihRtcHandle);
	  }		
  public:
    DevRtcFixture();
	  ~DevRtcFixture();
};

/* -------------------------------------------------------------------------- */
class DevRtcApplicationFixture : public testing::Test
{
  protected:
	  DevRtcEnvironment* my_poRtcEnvironment;

	  virtual void SetUp()
	  {
		
	  }
	  virtual void TearDown()
	  {

	  }		
  public:
    DevRtcApplicationFixture();
	  ~DevRtcApplicationFixture();
};

/* -------------------------------------------------------------------------- */
#endif /* __cplusplus */

#endif //_DEV_RTC_INTEGRATION_TEST_H_
