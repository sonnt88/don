/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        This file includes integration tests for RTC
 * @addtogroup   OEDT
 * @{
 */

/******************************************************************************/
/*                                                                            */
/* INCLUDES                                                                   */
/*                                                                            */
/******************************************************************************/

/* ---------------------------- Linux standard ------------------------------ */
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
/* -------------------- Persistant-Google-Regression-Test ------------------- */
#include <persigtest.h>
#include <execinfo.h>
#include <signal.h>
#include <string.h>
#include <sstream>
/* ---------------------------- device rtc     ------------------------------ */
#include <linux/rtc.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
/* ---------------------------- kmod  module   ------------------------------ */
#include <libkmod.h>
/* ---------------------------- osal types     ------------------------------ */
#include "system_types.h"
#include "system_definition.h"
#include "ostypes.h"
/* ---------------------------- datapool        ------------------------------ */
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h"
#include "dp_effd_if.h"
/* ---------------------------------- Self ---------------------------------- */
#include "dev_rtc_integration_test.h"

/******************************************************************************/
/*                                                                            */
/* DEFINES                                                                    */
/*                                                                            */
/******************************************************************************/

#define  DP_DATAPOOL_ID_RTC_INTEGRATION_TEST 0xeffd

//RTC
#ifdef DP_U32_POOL_ID_EARLYCONFIGRTCDRIVER
  #if(DP_U32_POOL_ID_EARLYCONFIGRTCDRIVER>>16 == DP_DATAPOOL_ID_RTC_INTEGRATION_TEST)
    #define EARYL_CONFIG_TEST_RTC_ACTIV    
  #endif
#endif

#define RTC_DRIVER_NAME_INC     "rtc-inc"           
#define RTC_DRIVER_NAME_RV8803  "rtc-rv8803"        
#define RTC_DRIVER_NAME_RX6110  "rtc-rx6110-i2c" 

#define RTC_READ_TIME_WAIT_FOR_NEXT_READ    5
#define RTC_READ_TIME_RETRY_COUNT           3

//for start process inc_send_out.out
#define RTC_PRIORITY_PROCESS              100
#define RTC_INVALID_PID          ((tS32)-100)

#define RTC_COMPONENT_STATUS_MSG_SENSOR     1
#define RTC_COMPONENT_STATUS_MSG_GNSS       2

#define RTC_KDS_GNSS_MOUNTED                1
#define RTC_KDS_GNSS_NOT_MOUNTED            2

//for activate GPS Antenna
#define RTC_SYS_FILE_ACTIVATE_GPS_ANTENNA  "/sys/devices/virtual/boardcfg/boardcfg/elmos52240-en-1/value"
/******************************************************************************/
/*                                                                            */
/* GLOBAL VARIABLES                                                           */
/*                                                                            */
/******************************************************************************/

static ::testing::persistence::PersistentFrameworkStateManager* g_persistentFrameworkStateManager;

static DevRtcEnvironment* g_poDevRtcEnvironment;

struct rtc_time rtc_tm;
struct rtc_time rtc_tm_read;

/******************************************************************************/
/*                                                                            */
/* PUBLIC FUNCTIONS                                                           */
/*                                                                            */
/******************************************************************************/
/*******************************************************************************
*
* Constructor for class DevRtcEnvironment.
*
*******************************************************************************/
DevRtcEnvironment::DevRtcEnvironment() 
{
	
}
/*******************************************************************************
*
* Destructor for class DevRtcEnvironment.
*
*******************************************************************************/
DevRtcEnvironment::~DevRtcEnvironment()
{
}

/*******************************************************************************
*
* Constructor for class DevRtcFixture.
*
*******************************************************************************/
DevRtcFixture::DevRtcFixture():
  my_ihRtcHandle(-1),
  my_poRtcEnvironment(g_poDevRtcEnvironment)
{
	
}

/*******************************************************************************
*
* Destructor for class DevRtcFixture.
*
*******************************************************************************/
DevRtcFixture::~DevRtcFixture()  
{
  my_poRtcEnvironment=NULL;
}

/*******************************************************************************
*
* Constructor for class DevRtcApplicationFixture.
*
*******************************************************************************/
DevRtcApplicationFixture::DevRtcApplicationFixture():
  my_poRtcEnvironment(g_poDevRtcEnvironment)
{
	
}

/*******************************************************************************
*
* Destructor for class DevRtcApplicationFixture.
*
*******************************************************************************/
DevRtcApplicationFixture::~DevRtcApplicationFixture()
{
  my_poRtcEnvironment=NULL;
}

/*******************************************************************************
*
* Main function which attaches a global test environment to be able to execute
* SetUp() and TearDown() methods at the beginning and at the end of all tests
* and which starts the execution of the tests via the RUN_ALL_TESTS() macro.
*
*******************************************************************************/
extern "C"
{
  void vStartApp(int argc, char **argv)
  {
    int myargc = 0;
    char** myargv = (char**)malloc(sizeof(char*) * argc);

    bool usePersiFeatures = false;
    bool printPersiHelp = false;

    IReader* reader = NULL;
    IWriter* writer = NULL;

    std::stringstream maxIntStringstream;
    maxIntStringstream << "--gtest_repeat=" << LONG_MAX - 1;

    std::string maxIntString;
    maxIntStringstream >> maxIntString;

    ::testing::persistence::IPersiGTestPrinter* printer = NULL;
    ::testing::TestEventListener* default_result_printer = NULL;

    for (int argIndex = 0; argIndex < argc; argIndex++)
    {
        if ( !strncmp(argv[argIndex], "--gtest_use_persi_features", 27) )
        {
            usePersiFeatures = true;
        } else
        {
            if ( !strncmp(argv[argIndex], "--help", 7) || !strncmp(argv[argIndex], "-h", 3) )
            {
                printPersiHelp = true;
            }

            myargv[myargc] = argv[argIndex];
            myargc++;
        }
    }

    if (usePersiFeatures)
    {
        for (int argIndex = 1; argIndex < myargc; argIndex++)
        {
            if ( !strncmp(myargv[argIndex], "--gtest_repeat=", 15 ) )
            {
                try {
                    std::stringstream repeatNumberAsStringStream;
                    std::string repeatNumberAsString((char*)(myargv[argIndex]+15));

                    int numberOfRepeats = 0;
                    repeatNumberAsStringStream << repeatNumberAsString;
                    repeatNumberAsStringStream >> numberOfRepeats;

                    if (numberOfRepeats < 0)
                        myargv[argIndex] = const_cast<char*>(maxIntString.c_str());

                } catch (...) {}
            }
        }
    }


    if (printPersiHelp)
    {
        fprintf(stderr, "Test Framework Behaviour:\n");
        fprintf(stderr, "  --gtest_use_persi_features\n");
        fprintf(stderr, "      Switch on persistence features.\n");
        fprintf(stderr, "\n");
    }

    if (usePersiFeatures)
    {
        //signal(SIGSEGV, SIGSEGVhandler);
        reader = IOFactory::CreateFrameworkStateReader();
        writer = IOFactory::CreateFrameworkStateWriter();

        //::testing::persistence::IPersiGTestPrinter* printer = new ::testing::persistence::PersiGTestXMLTracePrinter();
        //::testing::persistence::IPersiGTestPrinter* printer = new ::testing::persistence::PersiGTestCppUnitPrinter();

        printer = new ::testing::persistence::PrettyPersiUnitTestResultPrinter();

        ::testing::TestEventListeners& listeners = ::testing::UnitTest::GetInstance()->listeners();
        default_result_printer = listeners.Release(listeners.default_result_printer());
    
        g_persistentFrameworkStateManager =
                new ::testing::persistence::TraceablePersistentFrameworkStateManager(argc, argv, reader, writer, printer);

        listeners.Append(g_persistentFrameworkStateManager);
    }

    testing::InitGoogleMock(&myargc, myargv);

    // Ensures that the tests pass no matter what value of
    // --gmock_catch_leaked_mocks and --gmock_verbose the user specifies.
    testing::GMOCK_FLAG(catch_leaked_mocks) = true;
    testing::GMOCK_FLAG(verbose) = testing::internal::kWarningVerbosity;

    g_poDevRtcEnvironment = new DevRtcEnvironment;

    ::testing::AddGlobalTestEnvironment(g_poDevRtcEnvironment);

    /*int retVal = */RUN_ALL_TESTS();

    if (usePersiFeatures)
    {
        delete reader;
        delete writer;
        delete printer;
        delete default_result_printer;
    }
    
    free(myargv);

    OSAL_vProcessExit();
  }
} // extern "C"


/*******************************************************************************
*
* SetUp method for the entire test environment, which is executed once before
* all tests.
*
*******************************************************************************/
tVoid DevRtcEnvironment::SetUp(tVoid)
{	    
	char VcCmd[150];
	tU8  Vu8Gnss=RTC_KDS_GNSS_MOUNTED;
	tS32 Vs32ReturnCode;

  // read KDS  Vu8Gnss
	//[0x00] info not available (Information is not available. ) 
	//[0x01] mounted           (RTC_KDS_GNSS_MOUNTED)
	//[0x02] not mounted       (RTC_KDS_GNSS_NOT_MOUNTED)
	Vs32ReturnCode=DP_s32GetConfigItem("CMVariantCoding","GNSS",&Vu8Gnss,sizeof(Vu8Gnss));
	if(Vs32ReturnCode!=DP_S32_NO_ERR)
	{
	  if(Vs32ReturnCode==DP_S32_ERR_NO_ELEMENT_READ)
	  {
	    fprintf(stderr,"[----------] KDS entry for 'CMVariantCoding' 'GNSS' not saved \n");
	  }
    //try to determine, if driver RTC_DRIVER_NAME_RX6110 or RTC_DRIVER_NAME_RV8803 works
    if((bCheckDriverRtcRxWorks(RTC_DRIVER_NAME_RX6110)==TRUE)||(bCheckDriverRtcRxWorks(RTC_DRIVER_NAME_RV8803)==TRUE))
    {
      Vu8Gnss=RTC_KDS_GNSS_NOT_MOUNTED;      
    }
    //write to KDS
    vWriteKDSEntryCMVariantCodingGNSS(Vu8Gnss);           
	}
	else
	{ 
	  fprintf(stderr,"[----------] KDS entry for 'CMVariantCoding' 'GNSS' == %d \n",Vu8Gnss);
	}  
  #ifdef EARYL_CONFIG_TEST_RTC_ACTIV
  vSetInitCallback_EFFD();
  dp_tclEarlyConfigRTCDriverTrRtcDriverName           VoTrRTCDriver;
	if (Vu8Gnss==RTC_KDS_GNSS_MOUNTED)
	{//set datapool configuration for RTC_DRIVER_NAME_INC
    VoTrRTCDriver.s32SetData(RTC_DRIVER_NAME_INC);
	}
	else if(Vu8Gnss==RTC_KDS_GNSS_NOT_MOUNTED)
	{//set datapool configuration
    if (bCheckDriverRtcRxWorks(RTC_DRIVER_NAME_RV8803)==TRUE)
	    VoTrRTCDriver.s32SetData(RTC_DRIVER_NAME_RV8803);
    else
      VoTrRTCDriver.s32SetData(RTC_DRIVER_NAME_RX6110);
	}  
  #endif

	// for RTC_DRIVER_NAME_INC
	if(Vu8Gnss==RTC_KDS_GNSS_MOUNTED)
	{//activate GPS antenna 
    vActivateGPSAntenna();
	  //call process inc_send_out.out to set component status for Sensor and GNSS
	  //vStartProcessIncOutAndSendComponentStatus(RTC_COMPONENT_STATUS_MSG_SENSOR);
	  //vStartProcessIncOutAndSendComponentStatus(RTC_COMPONENT_STATUS_MSG_GNSS);
	  //sleep(1);
	}
	//system call to start early-config-rtc.service
	fprintf(stderr,"[----------] start early-config-rtc.service \n");
  memset(VcCmd,0,sizeof(VcCmd));
  snprintf(VcCmd,sizeof(VcCmd),"systemctl start early-config-rtc.service");
  system(VcCmd);
	sleep(1);
	//check which driver start and print out
  if(bKmodCheckDriverload((const char*)RTC_DRIVER_NAME_INC)==TRUE)
	{
	  fprintf(stderr,"[----------] RTC driver '%s' is started \n",RTC_DRIVER_NAME_INC);
	}
	else if(bKmodCheckDriverload((const char*)RTC_DRIVER_NAME_RX6110)==TRUE)
	{
	  fprintf(stderr,"[----------] RTC driver '%s' is started \n",RTC_DRIVER_NAME_RX6110);
	}
  else if(bKmodCheckDriverload((const char*)RTC_DRIVER_NAME_RV8803)==TRUE)
	{
	  fprintf(stderr,"[----------] RTC driver '%s' is started \n",RTC_DRIVER_NAME_RV8803);
	}
	else
	{
	  fprintf(stderr,"[----------] RTC driver isn't started   \n");
	}    
	fprintf(stderr,"\n");
  {//print out start test 
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr,"[----------] start test after %d s %d ms\n",VTimer.tv_sec,VTimer.tv_nsec/1000000);
  }
}

/*******************************************************************************
*
* TearDown method for the entire test environment, which is executed once after
* all tests.
*
*******************************************************************************/
tVoid DevRtcEnvironment::TearDown(tVoid)
{
  
}

/******************************************************************************/
/*                                                                            */
/* METHODS                                                                    */
/*                                                                            */
/******************************************************************************/

/*******************************************************************************
*
* Method which check if RTC driver load
*
*******************************************************************************/
tBool DevRtcEnvironment::bKmodCheckDriverload(const char* PpcDriverName)
{
  tBool  VbLoad=FALSE;
  struct kmod_ctx    *VsCtxDriver;
  struct kmod_module *VsModDriber;
  VsCtxDriver = kmod_new(NULL, NULL);
  if(VsCtxDriver!=NULL)
  {
    if (kmod_module_new_from_name(VsCtxDriver, PpcDriverName, &VsModDriber) >= 0)   
	  {
	    int ViKmodState;
	    ViKmodState=kmod_module_get_initstate(VsModDriber);		
	    /*check module loaded*/
	    if(ViKmodState > 0) 
	    {		   
	      VbLoad=TRUE; 
	    }
	  }
	  kmod_unref(VsCtxDriver);
  }
  return(VbLoad);
}

/*******************************************************************************
*
* Method which starts the process inc_send_out to send a component message to V850 
*
*******************************************************************************/
void DevRtcEnvironment::vStartProcessIncOutAndSendComponentStatus(tU8 Pu8Kind)
{
  OSAL_tProcessID    VprID = RTC_INVALID_PID;
  char               VcCmdLine[50];

  memset(VcCmdLine,0,sizeof(VcCmdLine));
  if(Pu8Kind==RTC_COMPONENT_STATUS_MSG_SENSOR)
  {
    snprintf(VcCmdLine,sizeof(VcCmdLine),"-p 50950 -b 50950 -r scc 20-01-01");
  }
  else if (Pu8Kind==RTC_COMPONENT_STATUS_MSG_GNSS)
  {
    snprintf(VcCmdLine,sizeof(VcCmdLine),"-p 50952 -b 50952 -r scc 20-01-01");
  }
  OSAL_trProcessAttribute	 VprAtr = {
                                     "inc_send_out.out",                                /*szName*/
					                            RTC_PRIORITY_PROCESS,	                            /*u32Priority*/								 	
					                            "/opt/bosch/base/bin/inc_send_out.out",		        /*szAppName*/
					                            VcCmdLine,                                        /*szCommandLine*/
					                            0                                                 /*szCGroup_path*/
					                          };
  VprID = OSAL_ProcessSpawn(&VprAtr);
  if (OSAL_ERROR == VprID)
  {
    fprintf(stderr,"[----------] Error Process inc_send_out.out couldn't be started \n");
  }
}
/*******************************************************************************
*
* Method which activate the GPS Antenna(write 1 to to file 
*   /sys/devices/virtual/boardcfg/boardcfg/elmos52240-en-1/value
*
*******************************************************************************/
void DevRtcEnvironment::vActivateGPSAntenna(void)
{ /*open file*/ 
  int ViFile = open(RTC_SYS_FILE_ACTIVATE_GPS_ANTENNA, O_WRONLY);
  /* check file handle */
  if(ViFile < 0)
  { /*  err*/
    fprintf(stderr,"[----------] Error function open() fails for file %s\n",RTC_SYS_FILE_ACTIVATE_GPS_ANTENNA);
  }
  else
  { /*write mode*/	  
    tS32 Vs32ReturnCode=write(ViFile,"1",strlen("1")+1);
    if(Vs32ReturnCode<0)
    {
      fprintf(stderr,"[----------] Error function write() fails for file %s\n",RTC_SYS_FILE_ACTIVATE_GPS_ANTENNA);
    }	 	
    if (close(ViFile)<0) 
    {	     
      fprintf(stderr,"[----------] Error function close() fails for file %s\n",RTC_SYS_FILE_ACTIVATE_GPS_ANTENNA);
    }  	 
  }
}

/*******************************************************************************
*
* Method which write the KDS entry "CMVariantCoding" Item "GNSS" 
*******************************************************************************/
void DevRtcEnvironment::vWriteKDSEntryCMVariantCodingGNSS(tU8 Vu8Gnss)
{
  fprintf(stderr,"[----------] KDS write entry 'CMVariantCoding' intem 'Vu8Gnss'\n");
  OSAL_tIODescriptor hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READWRITE );
  if(hDevice!=OSAL_ERROR)
  { 
    tBool bAccessOption = TRUE;
    if(OSAL_s32IOControl (hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(tS32)bAccessOption) == OSAL_ERROR)
      fprintf(stderr,"[----------] KDS write enable fails\n");
    else
    {
      tsKDSEntry rEntryInfo;
      memset(&rEntryInfo,0,sizeof(rEntryInfo));
      rEntryInfo.u16Entry = 0x0DF4;
      rEntryInfo.u16EntryLength = 32;        
      rEntryInfo.au8EntryData[14]=(Vu8Gnss<<2);// bit2-3 value 2
      /*Write the KDS Entries*/
      if(OSAL_s32IOWrite(hDevice,(tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo))==OSAL_ERROR)
        fprintf(stderr,"[----------] KDS write fails\n");
      else
      {
        fprintf(stderr,"[----------] KDS write success\n");
        OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_KDS_WRITE_BACK, 0);
      }
    }
    close(hDevice);
  }
  else
    fprintf(stderr,"[----------] KDS open fails\n");
}

/*******************************************************************************
*
* Method which checks if the driver rx6110 works. Is it possible to get a time? 
*******************************************************************************/
tBool DevRtcEnvironment::bCheckDriverRtcRxWorks(char* PpcDriverName)
{
  tBool VbReturn=FALSE;
  char  VcCmd[150];

  //unload all rtc driver
  //1) RTC_DRIVER_NAME_INC
  if(bKmodCheckDriverload((const char*)RTC_DRIVER_NAME_INC)==TRUE)
	{ //rmmod rtc-inc
    memset(VcCmd,0,sizeof(VcCmd));
    snprintf(VcCmd,sizeof(VcCmd),"rmmod %s",RTC_DRIVER_NAME_INC);
    system(VcCmd);
	  sleep(1);
  }
  //2) RTC_DRIVER_NAME_RV8803
  if(bKmodCheckDriverload((const char*)RTC_DRIVER_NAME_RV8803)==TRUE)
	{ //rmmod rtc-inc
    memset(VcCmd,0,sizeof(VcCmd));
    snprintf(VcCmd,sizeof(VcCmd),"rmmod %s",RTC_DRIVER_NAME_RV8803);
    system(VcCmd);
	  sleep(1);
  }
  //3) RTC_DRIVER_NAME_RX6110
  if(bKmodCheckDriverload((const char*)RTC_DRIVER_NAME_RX6110)==TRUE)
	{ //rmmod rtc-inc
    memset(VcCmd,0,sizeof(VcCmd));
    snprintf(VcCmd,sizeof(VcCmd),"rmmod %s",RTC_DRIVER_NAME_RX6110);
    system(VcCmd);
	  sleep(1);
  }

  // load driver
  if(bKmodCheckDriverload((const char*)PpcDriverName)==FALSE)
	{ //modprobe 
    memset(VcCmd,0,sizeof(VcCmd));
    snprintf(VcCmd,sizeof(VcCmd),"modprobe %s",PpcDriverName);
    system(VcCmd);
	  sleep(1);
  }
  if(bKmodCheckDriverload((const char*)PpcDriverName)==TRUE)
  { //open driver
    int VLocalRtcHandle = open(RTC0, O_RDWR);
    if(VLocalRtcHandle!=-1)
    {
      //for RV8803 => set time < year 200 failed
      tBool VbSmaller2000=FALSE;
      tBool VbGreater2000=FALSE;     
      //set time need for rv8803     
      rtc_tm.tm_mday  =  10;
      rtc_tm.tm_mon   =  11;
      rtc_tm.tm_year  =  99;
      rtc_tm.tm_hour  =  20;
      rtc_tm.tm_min   =  10;
      rtc_tm.tm_sec   =  25;
      if(ioctl(VLocalRtcHandle, RTC_SET_TIME, &rtc_tm)!=-1)
      {
        VbSmaller2000=TRUE;
      }    
      rtc_tm.tm_mday  =  10;
      rtc_tm.tm_mon   =  11;
      rtc_tm.tm_year  = 101;
      rtc_tm.tm_hour  =  20;
      rtc_tm.tm_min   =  10;
      rtc_tm.tm_sec   =  25;
      if(ioctl(VLocalRtcHandle, RTC_SET_TIME, &rtc_tm)!=-1)
      {
        VbGreater2000=TRUE;
      }    
      //read time only possible for rtc-inc 
      if(ioctl(VLocalRtcHandle, RTC_RD_TIME, &rtc_tm)!=-1)
      {
        VbReturn=TRUE;
      }
      //check for RTC_DRIVER_NAME_RV8803 
      if(VbReturn==TRUE)
      {
        if(strncmp(PpcDriverName,RTC_DRIVER_NAME_RV8803,strlen(RTC_DRIVER_NAME_RV8803))==0)
        {
          if((VbGreater2000==TRUE)&&(VbSmaller2000==TRUE))
            VbReturn=FALSE;
        }
      }
      close(VLocalRtcHandle);
    }
    //rmmod 
    memset(VcCmd,0,sizeof(VcCmd));
    snprintf(VcCmd,sizeof(VcCmd),"rmmod %s",PpcDriverName);
    system(VcCmd);
	  sleep(1);
  }
  return(VbReturn);
}
/******************************************************************************/
/*                                                                            */
/* TESTS            DevRtcFixture                                             */
/*                                                                            */
/******************************************************************************/
TEST_F(DevRtcFixture, Rtc_OpenDeviceTwice_EBUSY)
{
  int VLocalRtcHandle = open(RTC0, O_RDWR);
  //check error depending kind driver 
  EXPECT_EQ(VLocalRtcHandle,-1);
  EXPECT_EQ(errno, EBUSY); /* Device or resource busy */
}

TEST_F(DevRtcFixture, Rtc_CloseDeviceInvalidHandle_EBADF)
{
  int VLocalRtcHandle = -1;
  close(VLocalRtcHandle);
  EXPECT_EQ(errno, EBADF);	 /* Bad file number */
}

TEST_F(DevRtcFixture, RtcInc_SetTime_ENOPROTOOPT) 
{ //fails for RTC_DRIVER_NAME_RV8803
  //fails for RTC_DRIVER_NAME_RX6110
  int    retval;

  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  =  100; 
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( errno, ENOPROTOOPT);	
}

TEST_F(DevRtcFixture, RtcI2CRv8803_SetTimeBeforeYear2000_ERROR) 
{
  //fails for RTC_DRIVER_NAME_RV8803
  //fails for RTC_DRIVER_NAME_INC
  int    retval;

  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  =  99;//1999
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval,-1);	
}

TEST_F(DevRtcFixture, RtcI2c_SetTimeBefore2000_SUCCESS) 
{
  //fails for RTC_DRIVER_NAME_INC
  int    retval;

  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  =  100;//2000
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval,0);	
}

TEST_F(DevRtcFixture, Rtc_ReadTimeUntilSuccessRetry10_SUCCESS) 
{
  int    retval=-1;
  int    ViInc=0;
  memset(&rtc_tm,0,sizeof(rtc_time));  
  /*try to read the time max retry 10*/
  while((retval==-1)&&(ViInc<10)) 
  {
    retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
    ViInc++;
    if(retval==-1)
      sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
  }
  if(retval == -1)
  {
    fprintf(stderr, "             read time errno:%d \n", errno);	
  }
  else
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
  }
  EXPECT_GT(retval, -1);		
}

TEST_F(DevRtcFixture, RtcI2c_ReadTime_SUCCESS ) 
{
  int    retval=-1;
  memset(&rtc_tm,0,sizeof(rtc_time));  
  retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
  if(retval == -1)
  {
    fprintf(stderr, "             read time errno:%d \n", errno);	
  }
  else
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
  }
  EXPECT_GT(retval, -1);		
}

TEST_F(DevRtcFixture, RtcI2c_SetTime_SUCCESS) 
{ //fails for RTC_DRIVER_NAME_INC
  int    retval;
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 100;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm);
  if(retval == -1)
  {
    fprintf(stderr, "             set  time errno:%d \n", errno);		   
  }
  else
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);   
  }
  EXPECT_EQ(retval,0);  
}

TEST_F(DevRtcFixture, RtcI2c_SetTimeReadTime_SUCCESS) 
{ //fails for RTC_DRIVER_NAME_INC
  int    retval=-1;

  //set time
  rtc_tm.tm_mday  =   1;
  rtc_tm.tm_mon   =   9;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  11;
  rtc_tm.tm_min   =   2;
  rtc_tm.tm_sec   =   3;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm);
  if(retval == -1)
  {
    fprintf(stderr, "             set  time errno:%d \n", errno);		   
  }
  else
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
  }
  EXPECT_EQ( retval,0);
  //---- read time
  memset(&rtc_tm,0,sizeof(rtc_time));  
  /*try to read the time */
  retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
  if(retval == -1)
  {
    fprintf(stderr, "             read time errno:%d \n", errno);	
  }
  else
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);   
  }
  EXPECT_GT( retval, -1);		
  //read time >= 10 sec
  EXPECT_EQ(rtc_tm.tm_mday,1);
	EXPECT_EQ(rtc_tm.tm_mon,9);
  EXPECT_EQ(rtc_tm.tm_year,114);
	EXPECT_EQ(rtc_tm.tm_hour,11);
  EXPECT_EQ(rtc_tm.tm_min,2);
  EXPECT_GE(rtc_tm.tm_sec,3);
}

TEST_F(DevRtcFixture, RtcI2c_SetTimeWaitReadTime_SUCCESS) 
{ //fails for RTC_DRIVER_NAME_INC
  int    retval=-1;
  //set time
  rtc_tm.tm_mday  =   1;
  rtc_tm.tm_mon   =   9;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  12;
  rtc_tm.tm_min   =   0;
  rtc_tm.tm_sec   =   0;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm);
  if(retval == -1)
  {
    fprintf(stderr, "             set  time errno:%d \n", errno);		   
  }
  else
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
  }
  EXPECT_EQ( retval,0);
  //---- wait 2 sec
  sleep(2);
  //---- read time
  memset(&rtc_tm,0,sizeof(rtc_time));  
  retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
  if(retval == -1)
  {
    fprintf(stderr, "             read time errno:%d \n", errno);	
  }
  else
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
  }
  EXPECT_GT( retval, -1);		
	//read time >= 2 sec
  EXPECT_GE(rtc_tm.tm_sec,2);
}

TEST_F(DevRtcFixture, Rtc_SetTimeInvalidSec_EINVAL) 
{
  int    retval;

  //set time
  rtc_tm.tm_mday  =   1;
  rtc_tm.tm_mon   =   9;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  12;
  rtc_tm.tm_min   =   0;
  rtc_tm.tm_sec   =  60;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ( retval,-1);
  EXPECT_EQ( errno,EINVAL);    /* Invalid argument */ 
}

TEST_F(DevRtcFixture, Rtc_SetTimeInvalidMin_EINVAL) 
{
  int    retval;

  //set time
  rtc_tm.tm_mday  =   1;
  rtc_tm.tm_mon   =   9;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  12;
  rtc_tm.tm_min   =  60;
  rtc_tm.tm_sec   =   0;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ( retval,-1);
  EXPECT_EQ( errno,EINVAL);    /* Invalid argument */  
}

TEST_F(DevRtcFixture, Rtc_SetTimeInvalidHour_EINVAL) 
{
  int    retval;

  //set time
  rtc_tm.tm_mday  =   1;
  rtc_tm.tm_mon   =   9;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  24;
  rtc_tm.tm_min   =   2;
  rtc_tm.tm_sec   =   0;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ( retval,-1);
  EXPECT_EQ( errno,EINVAL);    /* Invalid argument */
}

TEST_F(DevRtcFixture, Rtc_SetTimeInvalidMonth_EINVAL) 
{
  int    retval;
  //set time
  rtc_tm.tm_mday  =   1;
  rtc_tm.tm_mon   =  12;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =   2;
  rtc_tm.tm_sec   =   0;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ( retval,-1);
  EXPECT_EQ( errno,EINVAL);    /* Invalid argument */  
}

TEST_F(DevRtcFixture, Rtc_SetTimeInvalidDay0_EINVAL) 
{
  int    retval;

  //set time
  rtc_tm.tm_mday  =   0;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =  59;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ( retval,-1);
  EXPECT_EQ( errno,EINVAL);    /* Invalid argument */ 
}

TEST_F(DevRtcFixture, Rtc_SetTimeInvalidDay32_EINVAL) 
{
  int    retval;

  //set time
  rtc_tm.tm_mday  =  32;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =  59;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ( retval,-1);
  EXPECT_EQ( errno,EINVAL);    /* Invalid argument */  
}

TEST_F(DevRtcFixture, Rtc_ReadTimeWait2sReadTime_SUCCESS) 
{
  int    retval=-1;
  int    ViInc=0;
  int    ViSec=0;
  int    ViMin=0;
  memset(&rtc_tm,0,sizeof(rtc_time));  

  /*try to read the time */
  while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
  {
    retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
	  ViInc++;
	  sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
  }

  if(retval == -1)
  {
    fprintf(stderr, "             read time errno:%d \n", errno);	
  }
  else
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
  }
  EXPECT_GT( retval, -1);		
  EXPECT_NE(rtc_tm.tm_year,0);
  ViSec=rtc_tm.tm_sec;
  ViMin=rtc_tm.tm_min;

  //wait 2 s
  sleep(2);
  
  /*try to read the time*/
  retval=-1;
  ViInc=0;
  memset(&rtc_tm,0,sizeof(rtc_time));  
  while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
  {
    retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
	  ViInc++;
    if(retval==-1)
	    sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
  }
  if(retval == -1)
  {
    fprintf(stderr, "             read time errno:%d \n", errno);	
  }
  else
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
  }
  EXPECT_GT(retval, -1);		
	EXPECT_NE(rtc_tm.tm_year,0);
	EXPECT_NE(rtc_tm.tm_sec,ViSec);
}

TEST_F(DevRtcFixture, RtcI2c_SetReadTimeCheckTransitionMin_MinChange) 
{
  int    retval;
  int    ViInc=0;

  //set time
  rtc_tm.tm_mday  =  30;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =  00;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm);  
  
  EXPECT_EQ(retval,0);
  if(retval==0)
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    //wait 2 s  
    sleep(2);
    /*try to read the time*/
    retval=-1;
    ViInc=0;
    memset(&rtc_tm,0,sizeof(rtc_time));  
    while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
    {
      retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
      ViInc++;
      sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
    }
    if(retval == -1)
    {
      fprintf(stderr, "             read time errno:%d \n", errno);	
    }
    else
    {
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      //min > 0 
      EXPECT_GT(rtc_tm.tm_min,0);
      EXPECT_GT(rtc_tm.tm_sec,0);   
    }
  }
}
TEST_F(DevRtcFixture, RtcI2c_SetReadTimeCheckTransitionHour_HourChangeTo23) 
{
  int    retval;
  int    ViInc=0;

  //set time
  rtc_tm.tm_mday  =  30;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 114;
  rtc_tm.tm_hour  =  22;
  rtc_tm.tm_min   =  59;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ(retval,0);
  if(retval==0)
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    //wait 2 s  
    sleep(2);
    /*try to read the time*/
    retval=-1;
    ViInc=0;
    memset(&rtc_tm,0,sizeof(rtc_time));  
    while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
    {
      retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
	    ViInc++;
	    sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
    }
    if(retval == -1)
    {
      fprintf(stderr, "             read time errno:%d \n", errno);	
    }
    else
    {
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      //check hour = 0
      EXPECT_EQ(rtc_tm.tm_hour,23);
      EXPECT_EQ(rtc_tm.tm_min,0);   
      EXPECT_GT(rtc_tm.tm_sec,0);   
    }
  }
}
TEST_F(DevRtcFixture, RtcI2c_SetReadTimeCheckTransitionDay_DayChangeTo2) 
{
  int    retval;
  int    ViInc=0;

  //set time
  rtc_tm.tm_mday  =  01;
  rtc_tm.tm_mon   =  02;
  rtc_tm.tm_year  = 115;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =  59;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ(retval,0);
  if(retval==0)
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    //wait 2 s  
    sleep(2);
    retval=-1;
    ViInc=0;
    memset(&rtc_tm,0,sizeof(rtc_time));  
    while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
    {
      retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
	    ViInc++;
	    sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
    }
    if(retval == -1)
    {
      fprintf(stderr, "             read time errno:%d \n", errno);	
    }
    else
    {
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      //check day = 2
      EXPECT_EQ(rtc_tm.tm_mday,2);
      EXPECT_EQ(rtc_tm.tm_hour,0);
      EXPECT_EQ(rtc_tm.tm_min,0);   
      EXPECT_GT(rtc_tm.tm_sec,0);   
    }
  }
}
TEST_F(DevRtcFixture, RtcI2c_SetReadTimeCheckTransitionMonth_MonthChangeToFeb) 
{
  int    retval;
  int    ViInc=0;

  //set time
  rtc_tm.tm_mday  =  31;
  rtc_tm.tm_mon   =  00;
  rtc_tm.tm_year  = 115;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =  59;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ(retval,0);
  if(retval==0)
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    //wait 2 s  
    sleep(2);
    /*try to read the time*/
    retval=-1;
    ViInc=0;
    memset(&rtc_tm,0,sizeof(rtc_time));  
    while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
    {
      retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
      ViInc++;
      sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
    }
    if(retval == -1)
    {
      fprintf(stderr, "             read time errno:%d \n", errno);	
    }
    else
    {
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      //check month = 1 (0..11)
      EXPECT_EQ(rtc_tm.tm_mon,1);
      EXPECT_EQ(rtc_tm.tm_mday,1);
      EXPECT_EQ(rtc_tm.tm_hour,0);
      EXPECT_EQ(rtc_tm.tm_min,0);   
      EXPECT_GT(rtc_tm.tm_sec,0);   
    }
  }
}

TEST_F(DevRtcFixture, RtcI2c_SetReadTimeCheckTransitionMonth_MonthChangeToMarch) 
{
  int    retval;
  int    ViInc=0;

  //set time
  rtc_tm.tm_mday  =  28;
  rtc_tm.tm_mon   =  01;
  rtc_tm.tm_year  = 115;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =  59;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ(retval,0);
  if(retval==0)
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    //wait 2 s  
    sleep(2);
    /*try to read the time*/
    retval=-1;
    ViInc=0;
    memset(&rtc_tm,0,sizeof(rtc_time));  
    while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
    {
      retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
	    ViInc++;
	    sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
    }
    if(retval == -1)
    {
      fprintf(stderr, "             read time errno:%d \n", errno);	
    }
    else
    {
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      //check month = 1 (0..11)
      EXPECT_EQ(rtc_tm.tm_mon,2);
      EXPECT_EQ(rtc_tm.tm_mday,1);
      EXPECT_EQ(rtc_tm.tm_hour,0);
      EXPECT_EQ(rtc_tm.tm_min,0);   
      EXPECT_GT(rtc_tm.tm_sec,0);   
    }
  }
}
TEST_F(DevRtcFixture, RtcI2c_SetReadTimeCheckTransitionMonthLeapYear_MonthChangeToMarch) 
{
  int    retval;
  int    ViInc=0;

  //set time
  rtc_tm.tm_mday  =  29;
  rtc_tm.tm_mon   =  01;
  rtc_tm.tm_year  = 116;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =  59;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ(retval,0);
  if(retval==0)
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    //wait 2 s  
    sleep(2);
    /*try to read the time*/
    retval=-1;
    ViInc=0;
    memset(&rtc_tm,0,sizeof(rtc_time));  
    while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
    {
      retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
	    ViInc++;
	    sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
    }
    if(retval == -1)
    {
      fprintf(stderr, "             read time errno:%d \n", errno);	
    }
    else
    {
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      //check month = 1 (0..11)
      EXPECT_EQ(rtc_tm.tm_mon,2);
      EXPECT_EQ(rtc_tm.tm_mday,1);
      EXPECT_EQ(rtc_tm.tm_hour,0);
      EXPECT_EQ(rtc_tm.tm_min,0);   
      EXPECT_GT(rtc_tm.tm_sec,0);   
    }
  }
}
TEST_F(DevRtcFixture, RtcI2c_SetReadTimeCheckTransitionMonth_MonthChangeToMay) 
{
  int    retval;
  int    ViInc=0;

  //set time
  rtc_tm.tm_mday  =  30;
  rtc_tm.tm_mon   =  03;
  rtc_tm.tm_year  = 116;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =  59;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ(retval,0);
  if(retval==0)
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    //wait 2 s  
    sleep(2);
    /*try to read the time*/
    retval=-1;
    ViInc=0;
    memset(&rtc_tm,0,sizeof(rtc_time));  
    while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
    {
      retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
	    ViInc++;
	    sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
    }
    if(retval == -1)
    {
      fprintf(stderr, "             read time errno:%d \n", errno);	
    }
    else
    {
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      //check month = 1 (0..11)
      EXPECT_EQ(rtc_tm.tm_mon,4);
      EXPECT_EQ(rtc_tm.tm_mday,1);
      EXPECT_EQ(rtc_tm.tm_hour,0);
      EXPECT_EQ(rtc_tm.tm_min,0);   
      EXPECT_GT(rtc_tm.tm_sec,0);   
    }
  }
}
TEST_F(DevRtcFixture, RtcI2c_SetReadTimeCheckTransitionYear_ChangeTo2017) 
{
  int    retval;
  int    ViInc=0;

  //set time
  rtc_tm.tm_mday  =  31;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 116;
  rtc_tm.tm_hour  =  23;
  rtc_tm.tm_min   =  59;
  rtc_tm.tm_sec   =  59;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ(retval,0);
  if(retval==0)
  {
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    //wait 2 s  
    sleep(2);
    /*try to read the time*/
    retval=-1;
    ViInc=0;
    memset(&rtc_tm,0,sizeof(rtc_time));  
    while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
    {
      retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
	    ViInc++;
	    sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
    }
    if(retval == -1)
    {
      fprintf(stderr, "             read time errno:%d \n", errno);	
    }
    else
    {
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      //check year 117
      EXPECT_EQ(rtc_tm.tm_year,117);
      EXPECT_EQ(rtc_tm.tm_mon,0);
      EXPECT_EQ(rtc_tm.tm_mday,1);
      EXPECT_EQ(rtc_tm.tm_hour,0);
      EXPECT_EQ(rtc_tm.tm_min,0);   
      EXPECT_GT(rtc_tm.tm_sec,0);   
    }
  }
}
TEST_F(DevRtcFixture, RtcI2cRx6110_SetReadEpochTime1970_ReadTime) 
{
  int    retval;
  int    ViInc=0;

  //set time
  rtc_tm.tm_mday  =   1;
  rtc_tm.tm_mon   =   0;
  rtc_tm.tm_year  =  70;
  rtc_tm.tm_hour  =   0;
  rtc_tm.tm_min   =   0;
  rtc_tm.tm_sec   =   0;
  retval = ioctl(my_ihRtcHandle, RTC_SET_TIME, &rtc_tm); 
  EXPECT_EQ(retval,0);

  if(retval==0)
  { 
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             set  time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    //wait 2 s  
    sleep(2);
    /*try to read the time*/
    retval=-1;
    ViInc=0;
    memset(&rtc_tm,0,sizeof(rtc_time));  
    while((retval==-1)&&(ViInc<RTC_READ_TIME_RETRY_COUNT)) 
    {
      retval = ioctl(my_ihRtcHandle, RTC_RD_TIME, &rtc_tm);
	    ViInc++;
	    sleep(RTC_READ_TIME_WAIT_FOR_NEXT_READ);
    }
    if(retval == -1)
    {
      fprintf(stderr, "             read time errno:%d \n", errno);	
    }
    else
    {
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      //check
      EXPECT_EQ(rtc_tm.tm_year,70);
      EXPECT_EQ(rtc_tm.tm_mon,0);
      EXPECT_EQ(rtc_tm.tm_mday,1);
      EXPECT_EQ(rtc_tm.tm_hour,0);
      EXPECT_EQ(rtc_tm.tm_min,0);   
      EXPECT_GT(rtc_tm.tm_sec,0);   
    }
  }
}
/******************************************************************************/
/*                                                                            */
/* TESTS            DevRtcApplicationFixture                                  */
/*                                                                            */
/******************************************************************************/
TEST_F(DevRtcApplicationFixture, Rtc_ApplicationAccess)
{
  int VLocalRtcHandle=-1;
  int ViInc=0;
  int retval=-1;
  tU8  Vu8Gnss=255;
  //call KDS
  tS32 Vs32ReturnCode=DP_s32GetConfigItem("CMVariantCoding","GNSS",&Vu8Gnss,sizeof(Vu8Gnss));
  EXPECT_GE(Vs32ReturnCode,DP_S32_NO_ERR);
  //open RTC
  VLocalRtcHandle = open(RTC0, O_RDWR);
  EXPECT_GT(VLocalRtcHandle,-1);	
  if(VLocalRtcHandle>=0)
  { //read time
    memset(&rtc_tm,0,sizeof(rtc_time));  
    /*try to read the time */
    while((retval==-1)&&(ViInc<10)) 
    { //if error try again each 5 s 
      retval = ioctl(VLocalRtcHandle, RTC_RD_TIME, &rtc_tm);
	    ViInc++;
	    sleep(5);
    }
    EXPECT_GT( retval, -1);		

    //if no error check time validation
    if(retval>-1)
    { //check time in the past
      struct timespec VTimer={0,0};
      clock_gettime(CLOCK_MONOTONIC, &VTimer);
      fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour+1,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);    
      if(rtc_tm.tm_year+1900<2015)
      { // set time
        rtc_tm.tm_mday  =   8;
        rtc_tm.tm_mon   =   9;
        rtc_tm.tm_year  = 114;
        rtc_tm.tm_hour  =  10;
        rtc_tm.tm_min   =  16;
        rtc_tm.tm_sec   =  16;
        retval = ioctl(VLocalRtcHandle, RTC_SET_TIME, &rtc_tm); 
        EXPECT_GT(retval,-1);
      }
    }
    retval=close(VLocalRtcHandle);
    EXPECT_GT(retval,-1);
  }
}

TEST_F(DevRtcApplicationFixture, Rtc_ApplicationAccessCheckTime)
{
  int VLocalRtcHandle=-1;
  int ViInc=0;
  int retval=-1;
  //open RTC
  VLocalRtcHandle = open(RTC0, O_RDWR);
  EXPECT_GT(VLocalRtcHandle,-1);
  if(VLocalRtcHandle>=0)
  {  //read time
    memset(&rtc_tm_read,0,sizeof(rtc_tm_read));  
    /*try to read the time */
    while((retval==-1)&&(ViInc<10)) 
    { //if error try again each 5 s 
      retval = ioctl(VLocalRtcHandle, RTC_RD_TIME, &rtc_tm_read);
	    ViInc++;
	    sleep(5);
    }
    EXPECT_GT(retval, -1);
    struct timespec VTimer={0,0};
    clock_gettime(CLOCK_MONOTONIC, &VTimer);
    fprintf(stderr, "             read time:  %02d.%02d.%d %02d:%02d:%02d after: %d s %d ms\n", rtc_tm.tm_mday,rtc_tm.tm_mon+1,rtc_tm.tm_year+1900,rtc_tm.tm_hour+1,rtc_tm.tm_min,rtc_tm.tm_sec,VTimer.tv_sec,VTimer.tv_nsec/1000000);
    EXPECT_GE(rtc_tm_read.tm_mday,rtc_tm.tm_mday);
    EXPECT_GE(rtc_tm_read.tm_mon,rtc_tm.tm_mon);
    EXPECT_GE(rtc_tm_read.tm_year,rtc_tm.tm_year);
    EXPECT_GE(rtc_tm_read.tm_hour,rtc_tm.tm_hour);
    //close
    retval=close(VLocalRtcHandle);
    EXPECT_GT(retval,-1);
  }
}
/******************************************************************************/
