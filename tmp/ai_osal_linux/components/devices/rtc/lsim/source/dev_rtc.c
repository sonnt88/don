/************************************************************************************
* FILE            : dev_rtc.c
*
* SW-COMPONENT    : Linux system components
*
* DESCRIPTION     : This implementation provides the RTC driver support for LSIM.
*                                      
* AUTHOR(s)       : Sudharsanan Sivagnanam (RBEI/ECF5)
*
* HISTORY         :
*------------------------------------------------------------------------------------
* Date        |       Version        | Author & comments
*-------- ----|----------------------|-----------------------------------------------
* 01.Nov.2012 |  Initial version 1.0 | Sudharsanan Sivagnanam (RBEI/ECF5)
* 26.Nov.2012 |  Updated version 1.1 | Anooj Gopi (RBEI/ECF5)
* ----------------------------------------------------------------------------------
************************************************************************************/

/***********************************************************************************
| Header file declaration
|-----------------------------------------------------------------------------------*/

#include <time.h>
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING

#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"
#include "Linux_osal.h"
#include "dev_rtc.h"

/************************************************************************
| Macro declaration (scope: global)
|-----------------------------------------------------------------------*/

#define LSIM_RTC_IO_CTRL_VERSION                 ((tU32)0x00000001)                         /* Driver version*/
#define LSIM_RTC_OFFSET_VALIDITY_FILE            OSAL_C_STRING_DEVICE_FFS2"/var/opt/bosch/dynamic/Validity.dat"   /* validity file*/
#define LSIM_RTC_TIME_INVALID                    (tU32)0x01                                 /* value of invalid validity*/
#define LSIM_RTC_GPS_TIME                        (tU32)0x02                                 /* value of valid validity*/
#define LSIM_RTC_UNKNOWN                         (tU8)0x03
/* Device open access valid mask*/
#define LSIM_RTC_ACCESS_VALID_MASK               (tS32)(~(OSAL_EN_READWRITE|OSAL_EN_WRITEONLY|OSAL_EN_READONLY))
#define LSIM_RTC_TIME_BASE 1900
#define LSIM_RTC_TIME_MIN 1985
#define LSIM_RTC_TIME_MAX 2120

/************************************************************************
| Variable declaration (scope: global)
|-----------------------------------------------------------------------*/

extern trGlobalOsalData *pOsalData;

typedef struct
{
   tU32 u32Validity;
   tS32 s32TimeOffSet;
} trRtcValidityFileData;

/************************************************************************
|function prototypes (scope: local)
|-----------------------------------------------------------------------*/
static tU32 DRV_LSIM_RTC_u32CreateValidityFile();
static tU32 DRV_LSIM_RTC_u32RtcConfig();
static tU32 DRV_LSIM_RTC_u32SetGpsTime(tS32 s32Arg);
static tU32 DRV_LSIM_RTC_u32UpdateValidityFile(trRtcValidityFileData* pData);
static tU32 DRV_LSIM_RTC_u32ReadValidityFile(trRtcValidityFileData* pData);
static tU32 CheckDataValidity(OSAL_trTimeDate  *p_date);
static tVoid DRV_LSIM_RTC_vTraceOut(  tU32 u32Level,const tChar *pcFormatString,... );

/************************************************************************
|functions Definitions
|-----------------------------------------------------------------------*/

/************************************************************************************
* FUNCTION         : RTC_u32CreateDevice
* PARAMETER        : None
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      : This function creates RTC device in LSIM
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
tU32 RTC_u32CreateDevice( tVoid )
{
   tU32 u32Ret= OSAL_OK ;

   /* Initial state of RTC time is invalid */
   pOsalData->enGpsTimeState = OSAL_RTC_INVALID;

   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"Entry:RTC_s32CreateDevice");

   /* Create validity file in flash to store the validity of RTC time*/
   if(OSAL_E_NOERROR != DRV_LSIM_RTC_u32CreateValidityFile())
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32CreateDevice Error: validity file create error");
      u32Ret = OSAL_ERROR;
   }
   else if(OSAL_E_NOERROR != DRV_LSIM_RTC_u32RtcConfig())
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32CreateDevice Error: RTC config failed");
      u32Ret = OSAL_ERROR;
   }
   else
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"RTC_s32CreateDevice:success");
   }
   return u32Ret;
}

/************************************************************************************
* FUNCTION         : DRV_LSIM_RTC_u32RtcConfig
* PARAMETER        : None
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      : This function updates validity file during system startup
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
static tU32  DRV_LSIM_RTC_u32RtcConfig()
{
   tU32 u32Ret = OSAL_E_NOERROR;
   trRtcValidityFileData rFileData = {0};

   u32Ret = DRV_LSIM_RTC_u32ReadValidityFile(&rFileData);
   if(OSAL_E_NOERROR != u32Ret)
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32RtcConfig Error: Read validity file failed");
   }
   else
   {
      switch(rFileData.u32Validity)
      {
         case LSIM_RTC_TIME_INVALID:
         {
            /* Time is never set by application */
            pOsalData->enGpsTimeState = OSAL_RTC_INVALID;
            break;
         }
         case LSIM_RTC_GPS_TIME:
         {
            /* Time is set by application once */
            pOsalData->enGpsTimeState = OSAL_RTC_EVER_SYNC;
            break;
         }
         case LSIM_RTC_UNKNOWN:
         {
            /*Update time state is unknown*/
            pOsalData->enGpsTimeState = OSAL_RTC_INVALID;
            u32Ret = OSAL_E_INVALIDVALUE; /* We never create a file with state OSAL_RTC_INVALID*/
            break;
         }
         default:
         {
            pOsalData->enGpsTimeState = OSAL_RTC_INVALID;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32RtcConfig Error:Invalid validity");
            u32Ret = OSAL_E_INVALIDVALUE;
            break;
         }
      }
   }
   return u32Ret;
}

/************************************************************************************
* FUNCTION         : DRV_LSIM_RTC_u32CreateValidityFile
* PARAMETER        : None
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      : This function creates validity file in flash device to store
*                    validity of rtc time and kernel offset.
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
static tU32 DRV_LSIM_RTC_u32CreateValidityFile()
{
   tU32 u32Ret= OSAL_E_NOERROR;
   tCString coszName = (tCString)LSIM_RTC_OFFSET_VALIDITY_FILE;
   trRtcValidityFileData rFileData = {0};
   OSAL_tIODescriptor hValidityFile = 0;

   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"Entry:DRV_LSIM_RTC_u32CreateValidityFile");

   rFileData.u32Validity = LSIM_RTC_TIME_INVALID;
   rFileData.s32TimeOffSet = 0;

   /*Try to open validity file in flash*/
   if(OSAL_ERROR != (hValidityFile = OSAL_IOOpen( coszName, OSAL_EN_READONLY )))
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32CreateValidityFile:validity file already exist");
      OSAL_s32IOClose( hValidityFile );
   }
   /*Check whether file is already created or not*/
   else if(OSAL_E_DOESNOTEXIST != OSAL_u32ErrorCode())
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32CreateValidityFile Error:validity file open error");
      u32Ret = OSAL_u32ErrorCode();
   }
   /*create validity file in flash device*/
   else if( OSAL_ERROR == ( hValidityFile = OSAL_IOCreate( coszName, OSAL_EN_READWRITE ) ))
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32CreateValidityFile Error:validity file create error");
      u32Ret = OSAL_u32ErrorCode();
   }
   else if( OSAL_ERROR == OSAL_s32IOWrite( hValidityFile,(tPCS8)&rFileData,( tS32 )sizeof(trRtcValidityFileData)))
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32CreateValidityFile Error:validity file write failed");
      u32Ret = OSAL_u32ErrorCode();
   }
   /*close validity file*/
   else if( OSAL_ERROR == OSAL_s32IOClose( hValidityFile ) )
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32CreateValidityFile Error:validity file close error");
      u32Ret = OSAL_u32ErrorCode();
   }
   else
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32CreateValidityFile: success");
   }

   return u32Ret;
}
/************************************************************************************
* FUNCTION         : RTC_u32IORemoveDevice
* PARAMETER        : None
* RETURNVALUE      : tU32 OSAL_OK
* DESCRIPTION      : This function removes RTC device in LSIM environment.
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
tU32 RTC_u32IORemoveDevice( tVoid )
{
   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"Entry:RTC_s32IORemoveDevice");

   return OSAL_OK;
}

/************************************************************************************
* FUNCTION         : RTC_u32IOOpen
* PARAMETER        : enAccess - Access Mode
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      : This function Opens RTC device in LSIM environment.
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
tU32 RTC_u32IOOpen(OSAL_tenAccess enAccess)
{
   tU32 u32Ret = OSAL_E_NOERROR ;

   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"Entry:RTC_IOOpen");

   /*Check validity of access mode*/
   if(enAccess & LSIM_RTC_ACCESS_VALID_MASK)
   {
      u32Ret = OSAL_E_INVALIDVALUE;
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_IOOpen Error:Invalid access");
   }
   else
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"RTC_IOOpen:success");
   }
   
   return u32Ret;
}

/************************************************************************************
* FUNCTION         : RTC_u32IOClose
* PARAMETER        : None
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      : This function close RTC device in LSIM environment.
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
tU32 RTC_u32IOClose(tVoid)
{
   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"Entry:RTC_s32IOClose");

   return OSAL_E_NOERROR;
}

/************************************************************************************
* FUNCTION         : RTC_s32IOControl
* PARAMETER        : None
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      : This function provides IO control function for RTC in LSIM
*                    environment.
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
tU32 RTC_u32IOControl(tS32 s32fun, tS32 s32arg)
{
   tU32 u32RetVal = OSAL_E_NOERROR;
   tPS32 pS32Arg;

   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"Entry:RTC_s32IOControl");

   if(s32arg == OSAL_NULL)
   {
       DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32IOControl Error:Invalid argument");
       u32RetVal = OSAL_E_INVALIDVALUE;
   }
   else
   {
      switch(s32fun)
      {
         case OSAL_C_S32_IOCTRL_RTC_VERSION:
         {
           /*Get driver version*/
           pS32Arg = (tPS32)s32arg;
           *pS32Arg = LSIM_RTC_IO_CTRL_VERSION;
           break;
         }
         case OSAL_C_S32_IOCTRL_RTC_SET_GPS_TIME:
         {
            /*set GPS time*/
            u32RetVal = DRV_LSIM_RTC_u32SetGpsTime(s32arg);
            if(OSAL_E_NOERROR != u32RetVal)
            {
               DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32IOControl Error:set GPS time failed");
            }
            break;
         }
         default:
         {
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32IOControl Error:Invalid IO control function");
            u32RetVal = OSAL_E_NOTSUPPORTED;
            break;
         }
      }
   }
   return u32RetVal;
}

/************************************************************************************
* FUNCTION         : RTC_s32IORead
* PARAMETER        : pBuffer to read, u32BuffSize - Size of the buffer
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      :This function reads RTC device in LSIM environment.
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
tU32 RTC_u32IORead( tPS8 pBuffer, tU32 u32BuffSize )
{
   tU32 u32Ret = OSAL_E_NOERROR;
   struct timeval rKerelTimeVal= {0};
   time_t Gpstime;
   trRtcValidityFileData rFileData = {0};
   OSAL_trRtcRead* prRtcRead  = ( tVoid* )pBuffer;
   struct tm rGpsTimeDate = {0} ;

   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"RTC_s32IORead:Entry");

   if( u32BuffSize < sizeof(OSAL_trRtcRead))
   {
     DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32IORead Error:Invalid buffer size");
     u32Ret = OSAL_E_INVALIDVALUE;
   }
   else if(NULL == pBuffer)
   {
    DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32IORead Error:Buffer is NULL");
    u32Ret = OSAL_E_INVALIDVALUE;
   }
   else if(prRtcRead->RegisterForRtcTime.registerForNotify != OSAL_RTC_NOTIFY_ON_GPS_TIME)
   {
    DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32IORead Error:Invalid register for notify");
    u32Ret = OSAL_E_INVALIDVALUE;
   }
   /* Get kernel time */
   else if(-1 == gettimeofday(&rKerelTimeVal, NULL))
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32IORead Error:gettimeofday failed");
      u32Ret = u32ConvertErrorCore( errno );
   }
   /* Read validity file */
   else if(OSAL_E_NOERROR != (u32Ret = DRV_LSIM_RTC_u32ReadValidityFile(&rFileData)))
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32IORead Error:Read validity failed");
   }
   else
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32SetGpsTime:kernal time:%d",rKerelTimeVal.tv_sec);

      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32SetGpsTime:offset:%d", rFileData.s32TimeOffSet);

      /* Calculate GPS time with kernel time and offset*/
      Gpstime = rKerelTimeVal.tv_sec + rFileData.s32TimeOffSet;

      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32SetGpsTime:GPS time:%d", Gpstime);

      /* Convert GPS time to date*/
      if(NULL == gmtime_r(&Gpstime, &rGpsTimeDate))
      {
         DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"RTC_s32IORead:gmtime failed");
         u32Ret = OSAL_E_NOTSUPPORTED;
      }
      else
      {
         /*In Linux month is calculated from 0 to 11 but in OSAL it is from 1 to 12.
         * application will give month value in terms of OSAL spec */
         rGpsTimeDate.tm_mon = rGpsTimeDate.tm_mon + 1;
         /*update argument*/
         OSAL_pvMemoryCopy((tPVoid)&prRtcRead->RtcTimes.gpsUtcTime, (tPVoid)&rGpsTimeDate,
                           (tU32)sizeof(struct tm));

         prRtcRead->RtcTimes.gpsUtcTimeState = pOsalData->enGpsTimeState;
         u32Ret = sizeof(OSAL_trRtcRead);
      }
   }

   return u32Ret;
}

/************************************************************************************
* FUNCTION         : DRV_LSIM_RTC_u32SetGpsTime
* PARAMETER        : s32Arg
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      : This function sets the gps time.
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
static tU32  DRV_LSIM_RTC_u32SetGpsTime(tS32 s32Arg)
{
   tU32 u32RetVal = OSAL_E_NOERROR;
   struct  tm TimeDate = {0};
   OSAL_trTimeDate *pGpsTimeDate = (OSAL_trTimeDate *)s32Arg;
   struct timeval rKernelTime= {0};
   trRtcValidityFileData rFileData;
   time_t OsalEpocTime;
   tS32 s32offset;

   OSAL_pvMemoryCopy((tPVoid)&TimeDate, (tPCVoid) pGpsTimeDate,
                           (tU32)sizeof(struct tm));

   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32SetGpsTime:Entry");

   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"YEAR:%d,MONTH:%d,DAY:%d,HOUR:%d,MINUTE:%d,SECOND:%d",
                          TimeDate.tm_year, TimeDate.tm_mon,TimeDate.tm_mday,TimeDate.tm_hour,
                          TimeDate.tm_min,TimeDate.tm_sec);

   if(OSAL_E_NOERROR != CheckDataValidity(pGpsTimeDate))
   {
     DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32SetGpsTime Error:invalid date value");
     u32RetVal = OSAL_E_INVALIDVALUE;
   }
   else
   {
      /*In Linux month is calculated from 0 to 11 but in OSAL it is from 1 to 12.
      * application will give month value in terms of OSAL spec*/
      TimeDate.tm_mon = TimeDate.tm_mon - 1;

      /* convert date to time */
      OsalEpocTime = mktime(&TimeDate);
      if(-1 == (int)OsalEpocTime)
      {
        DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32SetGpsTime Error:mktime failed");
        u32RetVal = u32ConvertErrorCore( errno );
      }
      /* Get the Kernel time, Note: Time zone is ignored and is obsolete */
      else if(-1 == gettimeofday(&rKernelTime, NULL))
      {
        DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32SetGpsTime Error:mktime failed");
        u32RetVal = u32ConvertErrorCore( errno );
      }
      else
      {
        DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32SetGpsTime:GPS time:%d", OsalEpocTime);

        DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32SetGpsTime:kernal time:%d", rKernelTime.tv_sec);

        /* Calculate offset value */
        s32offset = OsalEpocTime - rKernelTime.tv_sec;

        rFileData.s32TimeOffSet = s32offset;
        rFileData.u32Validity = LSIM_RTC_GPS_TIME;

        DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32SetGpsTime:offset:%d,Operator:%d",
                               rFileData.s32TimeOffSet);

        /* Update validity file*/
        u32RetVal = DRV_LSIM_RTC_u32UpdateValidityFile(&rFileData);
        if(OSAL_E_NOERROR != u32RetVal)
        {
          DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32SetGpsTime Error:Update validity file failed");
        }
      }
   }

   pOsalData->enGpsTimeState =(u32RetVal == OSAL_E_NOERROR)?OSAL_RTC_VALID:OSAL_RTC_INVALID;

   return u32RetVal;
}
/************************************************************************************
* FUNCTION         : DRV_LSIM_RTC_u32ReadValidityFile

* PARAMETER        : pData

* RETURNVALUE      : tU32 error codes

* DESCRIPTION      :This function reads validity file.
*
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
static tU32 DRV_LSIM_RTC_u32ReadValidityFile(trRtcValidityFileData* pData)
{
   tU32 u32Ret= OSAL_E_NOERROR;
   tCString coszName = (tCString)LSIM_RTC_OFFSET_VALIDITY_FILE;
   trRtcValidityFileData rReadData ={0};
   OSAL_tIODescriptor hValidityFile = 0;

   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32ReadValidityFile:Entry");

   /*open validity file*/
   if(OSAL_ERROR == (hValidityFile = OSAL_IOOpen( coszName, OSAL_EN_READONLY )))
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32ReadValidityFile Error:validity file open failed");
      u32Ret = OSAL_u32ErrorCode();
   }
   /*Read validity file*/
   else if( OSAL_ERROR == OSAL_s32IORead ( hValidityFile,(tPS8)&rReadData,( tS32 )sizeof(trRtcValidityFileData)))
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32ReadValidityFile Error:validity file Read failed");
      u32Ret = OSAL_u32ErrorCode();
   }
   /*close validity file*/
   else if( OSAL_ERROR == OSAL_s32IOClose( hValidityFile ) )
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32ReadValidityFile Error:validity file close error");
      u32Ret = OSAL_u32ErrorCode();
   }
   else
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32ReadValidityFile: success");
      pData->u32Validity = rReadData.u32Validity;
      pData->s32TimeOffSet = rReadData.s32TimeOffSet;
   }

   return u32Ret;
}
/************************************************************************************
* FUNCTION         : DRV_LSIM_RTC_u32UpdateValidityFile
* PARAMETER        : pData
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      : This function update validity and offset of rtc in validity file.
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
static tU32 DRV_LSIM_RTC_u32UpdateValidityFile(trRtcValidityFileData* pData)
{
   tU32 u32Ret= OSAL_E_NOERROR;
   tCString coszName = (tCString)LSIM_RTC_OFFSET_VALIDITY_FILE;
   OSAL_tIODescriptor hValidityFile          = 0;

   DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32UpdateValidityFile:Entry");

   /*open validity file*/
   if(OSAL_ERROR == (hValidityFile = OSAL_IOOpen( coszName, OSAL_EN_READWRITE )))
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32UpdateValidityFile Error:validity file open failed");
      u32Ret = OSAL_u32ErrorCode();
   }
   /*write validity file*/
   else if( OSAL_ERROR == OSAL_s32IOWrite ( hValidityFile,(tPCS8)pData,( tS32 )sizeof(trRtcValidityFileData)))
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32UpdateValidityFile Error:validity file write failed");
      u32Ret = OSAL_u32ErrorCode();
   }
   /*close validity file*/
   else if( OSAL_ERROR == OSAL_s32IOClose( hValidityFile ) )
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"DRV_LSIM_RTC_u32UpdateValidityFile Error:validity file close error");
      u32Ret = OSAL_u32ErrorCode();
   }
   else
   {
      DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"DRV_LSIM_RTC_u32UpdateValidityFile: success");
   }

   return u32Ret;
}

/************************************************************************************
* FUNCTION         : CheckDataValidity
* PARAMETER        : OSAL_trTimeDate  *p_date
* RETURNVALUE      : tU32 error codes
* DESCRIPTION      : This function update validity date/time.
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
static tU32 CheckDataValidity(OSAL_trTimeDate  *p_date)
{
   tS32    Ck_year = 0;
   tS32    Leap_year = 0;
   tU32 u32Ret= OSAL_E_NOERROR;

    Ck_year = p_date->s32Year + LSIM_RTC_TIME_BASE;

    if ( (Ck_year < LSIM_RTC_TIME_MIN) || (LSIM_RTC_TIME_MAX < Ck_year) )
    {
       u32Ret = OSAL_ERROR;
       DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:Invalid year");
    }
    else if ( (p_date->s32Month < 1) || (12 < p_date->s32Month) )
    {
       u32Ret = OSAL_ERROR;
       DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:Invalid month");
    }
    else if ( (p_date->s32Day < 1) || (31 < p_date->s32Day) )
    {
       u32Ret = OSAL_ERROR;
       DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:invalid day");
    }
    else
    {
      /* Check leap year */
      if ( ((Ck_year % 4 == 0) && (Ck_year % 100 != 0)) || (Ck_year % 400 == 0) )
      {
         Leap_year = 1;
      }
      else
      {
         Leap_year = 0;
      }

      /* Check month day */
      switch ( p_date->s32Month )
      {
      case 1:
         if ( (p_date->s32Day < 1) || (31 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:January invalid date");
         }
         break;
      case 2:
         if ( Leap_year == 1 )
         {
            if ( (p_date->s32Day < 1) || (29 < p_date->s32Day) )
            {
               u32Ret = OSAL_ERROR;
               DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:Leap year February invalid date");
            }
         }
         else
         {
            if ( (p_date->s32Day < 1) || (28 < p_date->s32Day) )
            {
               u32Ret = OSAL_ERROR;
               DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:February invalid date");
            }
         }
         break;
      case 3:
         if ( (p_date->s32Day < 1) || (31 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:March invalid date");
         }
         break;
      case 4:
         if ( (p_date->s32Day < 1) || (30 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:April invalid date");
         }
         break;
      case 5:
         if ( (p_date->s32Day < 1) || (31 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:May invalid date");
         }
         break;
      case 6:
         if ( (p_date->s32Day < 1) || (30 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:June invalid date");
         }
         break;
      case 7:
         if ( (p_date->s32Day < 1) || (31 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:July invalid date");
         }
         break;
      case 8:
         if ( (p_date->s32Day < 1) || (31 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:August invalid date");
         }
         break;
      case 9:
         if ( (p_date->s32Day < 1) || (30 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:September invalid date");
         }
         break;
      case 10:
         if ( (p_date->s32Day < 1) || (31 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:October invalid date");
         }
         break;
      case 11:
         if ( (p_date->s32Day < 1) || (30 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:November invalid date");
         }
         break;
      case 12:
         if ( (p_date->s32Day < 1) || (31 < p_date->s32Day) )
         {
            u32Ret = OSAL_ERROR;
            DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:December invalid date");
         }
         break;
      default:
         u32Ret = OSAL_ERROR;
      }

      if ( (p_date->s32Weekday) < 0 || 6 < (p_date->s32Weekday) )
      {
         u32Ret = OSAL_ERROR;
         DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:invalid s32Weekday");
      }
      else if ( (p_date->s32Hour < 0) || (23 < p_date->s32Hour) )
      {
         u32Ret = OSAL_ERROR;
         DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:invalid s32Hour");
      }
      else if ( (p_date->s32Minute < 0) || (59 < p_date->s32Minute) )
      {
         u32Ret = OSAL_ERROR;
         DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:invalid s32Minute");
      }
      else if ( (p_date->s32Second < 0) || (59 < p_date->s32Second) )
      {
         u32Ret = OSAL_ERROR;
         DRV_LSIM_RTC_vTraceOut(TR_LEVEL_FATAL,"CheckDataValidity Error:invalid s32Second");
      }
      else
      {
         DRV_LSIM_RTC_vTraceOut(TR_LEVEL_USER_4,"CheckDataValidity: valid date/time");
      }
    }

 return u32Ret;
}

/************************************************************************************
* FUNCTION         : DRV_LSIM_RTC_vTraceOut

* PARAMETER        : u32Level, pcFormatString

* RETURNVALUE      : tU32 error codes

* DESCRIPTION      :This function trace out LSIM RTC errors and messages.
*
* HISTORY          : 01.Nov.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
*************************************************************************************/
static tVoid DRV_LSIM_RTC_vTraceOut(  tU32 u32Level,const tChar *pcFormatString,... )
{
   if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_RTC, u32Level) != FALSE)
   {
      /*
Parameter to hold the argument for a function, specified the format
string in pcFormatString
defined as:
typedef char* va_list in stdarg.h
*/
      va_list argList = {0};
      /*
vsnprintf Returns Number of bytes Written to buffer or a negative
value in case of failure
*/
      tS32 s32Size ;
      /*
Buffer to hold the string to trace out
*/
      tS8 u8Buffer[MAX_TRACE_SIZE];
      /*
Position in buffer from where the format string is to be
concatenated
*/
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /*
Flush the String
*/
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /*
Copy the String to indicate the trace is from the RTC device
*/

      /*
Initialize the argList pointer to the beginning of the variable
arguement list
*/
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
Collect the format String's content into the remaining part of
the Buffer
*/
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,
                  sizeof(u8Buffer),
                  pcFormatString,
                  argList ) ) )
      {
         return;
      }

      /*
Trace out the Message to TTFis
*/
      LLD_vTrace( OSAL_C_TR_CLASS_DEV_RTC,
      u32Level,
      u8Buffer,
      (tU32)s32Size );   /* Send string to Trace*/
      /*
Performs the appropiate actions to facilitate a normal return by a
function that has used the va_list object
*/
      va_end(argList);
   }
}





