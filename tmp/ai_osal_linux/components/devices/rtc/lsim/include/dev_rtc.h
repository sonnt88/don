/******************************************************************************
* FILE              : dev_rtc.h
*
* SW-COMPONENT      : Linux system components
*
* DESCRIPTION       : Header file for LSIM rtc driver.
*                                      
* AUTHOR(s)         : Sudharsanan Sivagnanam (RBEI/ECF5)
*
* HISTORY           :
*-----------------------------------------------------------------------------
* Date          |       Version        | Author & comments
*-------- ------|----------------------|--------------------------------------
* 01.Nov.2012   |  Initialversion 1.0  | Sudharsanan Sivagnanam (RBEI/ECF5)
* -----------------------------------------------------------------------------
*******************************************************************************/

#ifndef DEV_RTC_H
#define DEV_RTC_H




/************************************************************************
|Data type definitions  (scope: local)
|-----------------------------------------------------------------------*/






/************************************************************************
|function prototypes (scope: local)
|-----------------------------------------------------------------------*/
OSAL_DECL tU32 RTC_u32CreateDevice(tVoid);
OSAL_DECL tU32 RTC_u32IORemoveDevice(tVoid);

OSAL_DECL tU32 RTC_u32IOOpen(OSAL_tenAccess enAccess);
OSAL_DECL tU32 RTC_u32IOClose(tVoid);

OSAL_DECL tU32 RTC_u32IOControl(tS32 s32fun, tS32 s32arg);

OSAL_DECL tU32 RTC_u32IORead( tPS8 pBuffer, tU32 maxbytes );


#endif

