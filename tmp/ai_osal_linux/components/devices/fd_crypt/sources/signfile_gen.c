#ifdef FD_CRYPT_NON_XML_SUPPORT//Obsolete Code
/*******************************************************************************
 *
 * FILE:
 *     signfile_gen.c
 *
 * REVISION:
 *     1.2
 *
 * AUTHOR:
 *     (c) 2007, Robert Bosch India Ltd., ECM/ECM-1, Divya H, 
 *                                                   divya.h@in.bosch.com
 *
 * CREATED:
 *     28/02/2007 - Divya H
 *
 * DESCRIPTION:
 *       This file contains the signature file generation code.
 *
 * NOTES:
 *
 * MODIFIED:
 *  DATE      |  AUTHOR  |         MODIFICATION
 *  27/06/07   |  Divya H |	 Modified to incorporate Crypt Extension Logic
 *  24/03/08   |  Divya H |	Modified for Crypt extension signature file handling in VW 
 *  24/10/08   | Ravindran| Ported to ADIT platform 
 *  23/02/09   | Ravindran| Root directory changed to cryptnav
                            Ported the TTFis command handling to generate signature and verify							  							  
    8/06/09   | Ravindran| Warnings Removal	
    16/06/09   | Ravindran| Warnings Removal
    24/06/09  | Ravindran| Use OSAL interface instead of LFS IO Interface
    03/08/09  | Ravindran| changes for crypt via USB
 ******************************************************************************/

/*****************************************************************
  | includes of component-internal interfaces
  | (scope: component-local)
  |--------------------------------------------------------------*/


/* OSAL Interface */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
//#include "ostrace.h"

#include "fd_crypt_private.h"
#include "fd_device_ctrl.h"
#include "fd_crypt_trace.h"
/* Header for file system ID */
//#include"prm.h"
//#include "dispatcher_defs.h"



/* Header files for BPCL */

//#include "../bpcl/bpcl.h"
//#include "../bpcl/bpcl_int.h"
//#include "../bpcl/ecc.h"


/* Private interface for TTFIS SFC related command support */
#include "signfile_gen.h"

/* ECC Public Key length */
#define ECC_PUB_KEY_LEN 		(40)

/* ECC public key entry ID in KDS */
#define KDS_ECC_PUBKEY_ENTRY	(0x0114)

/* Private key length */
#define PRIV_KEY_LEN			(20)

/* Size of signature file */
#define SIGN_FILE_SIZE			(48)

/* Flag for SF not being present */
#define SF_NOT_PRESENT			(1)
/* Name of file containing Private Key */
#define PRIVATE_KEY_FILE		"/shared/cryptnav/PRIVKEY.DAT"

/* KDS device */
#define KDS_DEVICE				"/dev/kds"

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/
typedef struct
{
  tU8 au8PrivKey[PRIV_KEY_LEN];
  tU8 au8PubKey[ECC_PUB_KEY_LEN];
  tU8 au8Signature[BPCL_ECC_SIGNATURE_SIZE];
  tU8 au8CID[CID_LEN];
}trSignFile;

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/

/* Descriptor for the trace device */
OSAL_tIODescriptor rFdTrace;

/* Handle to the signature file */
static trSignFile arSignFileHdl[FD_CRYPT_MAX_DEVICES];



/*****************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------*/


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************************
 *
 * FUNCTION:
 *    fd_crypt_vChannelMgr
 *
 * DESCRIPTION:
 *    This is the callback function for the TTFIS SFC related commands.       
 *
 * PARAMETERS:    
 *    pucData - Array of bytes passed along with the SFC command
 *
 * RETURNVALUE:
 *    None
 *
 * HISTORY:
 23/02/09   | Ravindran| Ported to ADIT PF
 *
 *****************************************************************************/
tVoid fd_crypt_vChannelMgr(const tUChar* puchData)
{
	tU8 u8MsgCode = puchData[2]; //byte 1 -> start of transmitted data
	tS32 s32RetVal = OSAL_E_NOERROR;

 	switch (u8MsgCode)
 	{
	 case CRYPT_LOAD_PRIKEY:
			s32RetVal = fd_crypt_load_prikey( 0 );
			break;
	
	 case EXT_CRYPT_WR_SF:
			s32RetVal = fd_cryptext_write_sig( 0 );
			break;
	 
	 case EXT_CRYPT_VERI_SF:
			s32RetVal = fd_cryptext_verify_sig( 0 );
			break;
	 	 	 
	 default:
			break;
	}
   	(tVoid)s32RetVal;  //To avoid lint warning

}

/*****************************************************************************
 *
 * FUNCTION:
 *    fd_crypt_load_prikey
 *
 * DESCRIPTION:
 *    This function is used to load the private key from SD Card     
 *
 * PARAMETERS:    
 *    None
 *
 * RETURNVALUE:
 *    OSAL_E_NOERROR - On success
 *	  OSAL_E_NOTSUPPORTED
 *	  OSAL_E_UNKNOWN
 *
 * HISTORY:
 * 23/02/09   | Ravindran| Root directory changed to cryptnav
 *                        added traces
 * 24/06/09  | Ravindran| Use OSAL interface instead of LFS IO Interface
 * 19/03/11   | Anooj Gopi  |  Added multi device support
 *
 *****************************************************************************/
tS32 fd_crypt_load_prikey( tS32 s32CryptDevID )
{
	tS32 s32RetVal = OSAL_ERROR;
	tS32 s32RetvalRead = OSAL_ERROR;
	tCString coszName=PRIVATE_KEY_FILE;
	OSAL_tenAccess enAccess=OSAL_EN_READONLY;
	tU32 u32PrivKeyBufAddr=(tU32)arSignFileHdl[s32CryptDevID].au8PrivKey;
	OSAL_tIODescriptor hPrivKey = 0;

		
	hPrivKey = OSAL_IOOpen( coszName, enAccess );
	if( hPrivKey != OSAL_ERROR )
	{
				
		s32RetvalRead = OSAL_s32IORead(hPrivKey, (tPS8)u32PrivKeyBufAddr, PRIV_KEY_LEN);
		s32RetVal =  OSAL_s32IOClose( hPrivKey );
	   
		if((s32RetVal !=OSAL_ERROR) && ( s32RetvalRead == (tS32)PRIV_KEY_LEN ))
		{
			//TraceIOString ("SUCCESS : Private key loaded!\n");	
			fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                              "Priv key loaded",0, 0, 0, 0 );
			
		}
		else 
		{
			//TraceIOString ("ERROR : Private key not loaded!\n");	
			fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                              "Priv key not loaded",0, 0, 0, 0 );
			s32RetVal=SIG_FILE_FAIL;
		}
	}
	else 
	{
		//TraceIOString ("ERROR : Check the presence of PRIVKEY.DAT in root directory!\n");	
		fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                              "PRIVKEY.DAT not found",0, 0, 0, 0 );
		s32RetVal=SIG_FILE_FAIL;
	}
	return (s32RetVal);
}


/*****************************************************************************
 *
 * FUNCTION:
 *    

 *
 * DESCRIPTION:
 *    This function is used to write Signature File into the SD Card 
 *	  which already has navigation data
 *
 * PARAMETERS:    
 *    None
 *
 * RETURNVALUE:
 *    OSAL_E_NOERROR - On success
 *	  SIG_FILE_FAIL	 - On failure
 *
 *
 * HISTORY:
 * 23/02/09   | Ravindran| Root directory changed to cryptnav
                         added traces   
   24/06/09  | Ravindran| Use OSAL interface instead of LFS IO Interface
   19/03/11   | Anooj Gopi  |  Added multi device support

 *****************************************************************************/
tS32 fd_cryptext_write_sig( tS32 s32CryptDevID )
{
#ifndef TSIM_OSAL
	tS32 s32RetVal=OSAL_ERROR;
	tCString coszsignfileName=EXT_CR_SF_PATH;
	OSAL_tenAccess enAccess=OSAL_EN_READWRITE; 
	tU32 u32AddrSignBuf;
	tU32 u32SignFileLen=BPCL_ECC_SIGNATURE_SIZE;
	tS32 s32Written = 0;
	tU8 u8Loop = 0;
	tU8 u8CIDLoop = 0;
	tU8 au8CID[CID_LEN] = {0};
	tBool bNoWrPr = TRUE;
	OSAL_tIODescriptor hSignFile = 0;
	OSAL_trIOCtrlCardState trIOCtrlCardState = {0};
	trSignFile *prSignFileHdl = &arSignFileHdl[s32CryptDevID];

	u32AddrSignBuf = (tU32) (prSignFileHdl->au8Signature);
	TraceIOString ("INFO :Ensure if Private key is loaded by CMD-CRYPT_LOAD_PRIKEY before this command\n");		
        /*Get the SD card info via USB interface*/             
        OSAL_pvMemorySet(&trIOCtrlCardState,0,sizeof(OSAL_trIOCtrlCardState));
	
        if ( OSAL_E_NOERROR!= fd_device_ctrl_u32UsbSdCardInfo(&trIOCtrlCardState) )
        {
              //TraceIOString ("INFO :Get Card state via USB failed\n");
              fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                              "Get CID failed",0, 0, 0, 0 );
              
        }
        else 
        {
             /*Adapt the CID pattern as expected by the signature verfication function*/
              fd_device_ctrl_vCIDPatternAdapt(trIOCtrlCardState.u8CIDRegister);
              OSAL_pvMemoryCopy( au8CID, trIOCtrlCardState.u8CIDRegister,sizeof(au8CID));
                       
         }
	
	if(bNoWrPr==TRUE)
	{
		u8CIDLoop=0;
		for(u8Loop=15;u8Loop>0;u8Loop--)
		{		  
		      (prSignFileHdl->au8CID)[u8CIDLoop++]=au8CID[u8Loop];
		}
		(prSignFileHdl->au8CID)[15]=au8CID[0];

		if(cryptext_sd_sign(prSignFileHdl->au8CID, prSignFileHdl->au8PrivKey, prSignFileHdl->au8Signature) == BPCL_OK)
		{
			/* Get path for Signature file based on Ford LS or VW board being used currently */
			
			hSignFile = OSAL_IOCreate(coszsignfileName,enAccess);

			if( hSignFile != OSAL_ERROR)
			{
						
				s32Written =  OSAL_s32IOWrite(hSignFile, (tPCS8)u32AddrSignBuf, u32SignFileLen );
				s32RetVal = OSAL_s32IOClose(hSignFile);				
		
				if((s32RetVal != OSAL_ERROR) && (s32Written==SIGN_FILE_SIZE))
				{
					//TraceIOString ("SUCCESS : Signature generated!\n");
					fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                  "Sig generated",0, 0, 0, 0 );
					return(s32RetVal);
				}
				else 
				{
					//TraceIOString ("ERROR : Signature generation failed!\n");
					fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                  "Sig generation failed",0, 0, 0, 0 );
				}
			}
			else 
			{
				//TraceIOString ("ERROR : Signature generation failed!\n");
				fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                  "Sig generation failed",0, 0, 0, 0 );
			}
		}
		else 
		{
			fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                  "Sig generation failed",0, 0, 0, 0 );
		}	
	}
	else
	{
		fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                  "card write protected",0, 0, 0, 0 );
	}
	
	return SIG_FILE_FAIL;
#endif
}

/*****************************************************************************
 *
 * FUNCTION:
 *    fd_cryptext_verify_sig
 *
 * DESCRIPTION:
 *    This function is used to verify Signature File on the SD Card 
 *	  which already has navigation data
 *
 * PARAMETERS:    
 *    None
 *
 * RETURNVALUE:
 *    OSAL_E_NOERROR - On success
 * 	  SF_NOT_PRESENT - Signature File not present
 *	  SIG_FILE_FAIL	 - On failure
 *
 *
 * HISTORY:
 23/02/09   | Ravindran| Root directory changed to cryptnav
                         added traces   
 24/06/09  | Ravindran| Use OSAL interface instead of LFS IO Interface
 19/03/11   | Anooj Gopi  |  Added multi device support
 *
 *****************************************************************************/
tS32 fd_cryptext_verify_sig( tS32 s32CryptDevID )
{
#ifndef TSIM_OSAL	
	tU8 au8Pub[ECC_PUB_KEY_LEN]=
	{
	0x5b, 0x71, 0xe6, 0x26, 0x6f, 0x6d, 0x5f, 0x28,
	0x49, 0xed, 0x89, 0x6a, 0xf1, 0x14, 0x7b, 0xdd,
	0xb7, 0x08, 0x0c, 0x28, 0xa5, 0x28, 0x9a, 0x1f,
	0xb2, 0xda, 0xed, 0x9b, 0x8d, 0x76, 0xaf, 0xe7,
	0x85, 0xb2, 0x6d, 0xd3, 0xfb, 0xe3, 0x93, 0x92
	};
	tU8 au8ReadSignature[SIGN_FILE_SIZE];
	tS32 s32RetVal = OSAL_ERROR;
	tCString coszsignfileName=EXT_CR_SF_PATH;
	tU32 u32ParamBuf1 = 0;
	tS32 s32Read = 0;
			
	OSAL_tIODescriptor rCryptKDS_Hdl = 0;
	tsKDSEntry rCryptKDS_Entry;
	tsKDSEntry* prKdsEntry=&rCryptKDS_Entry;
	tBool bNoWrPr = TRUE;
	tU8 au8CID[CID_LEN] = {0};
	tU8 u8Loop = 0;
	tU8 u8CIDLoop = 0;
	OSAL_tIODescriptor hSignFile = 0;
	OSAL_trIOCtrlCardState trIOCtrlCardState = {0};
	tChar szPath[CERT_PATH_MAX_LEN];
	trSignFile *prSignFileHdl = &arSignFileHdl[s32CryptDevID];

	u32ParamBuf1=(tU32)au8ReadSignature;
	
	
	//Read the Signature File contents present in the card		
			
	hSignFile =  OSAL_IOOpen(coszsignfileName, OSAL_EN_READONLY);
	if ( hSignFile != OSAL_ERROR)
	{
			
		s32Read = OSAL_s32IORead( hSignFile,(tPS8)u32ParamBuf1, SIGN_FILE_SIZE );
		if ( s32Read==SIGN_FILE_SIZE )
		{
				
			s32RetVal = OSAL_s32IOClose( hSignFile );			
			if ( s32RetVal != OSAL_ERROR)
			{
				//Get public key from KDS
				OSAL_pvMemorySet(prSignFileHdl->au8PubKey,0,sizeof(ECC_PUB_KEY_LEN));
				OSAL_pvMemorySet(prSignFileHdl->au8CID,0,sizeof(CID_LEN));
							
				rCryptKDS_Hdl=OSAL_IOOpen(KDS_DEVICE, OSAL_EN_READONLY);
			
				if(rCryptKDS_Hdl!=OSAL_ERROR)
				{
					OSAL_pvMemorySet(prKdsEntry,0,sizeof(rCryptKDS_Entry));

					rCryptKDS_Entry.u16Entry = KDS_ECC_PUBKEY_ENTRY;

				        rCryptKDS_Entry.u16EntryLength =  ECC_PUB_KEY_LEN ;	

					s32RetVal = OSAL_s32IORead(rCryptKDS_Hdl, 
				                              (tPS8)&rCryptKDS_Entry, 
				                              (tU32) sizeof(rCryptKDS_Entry));	

					/*Get ECC public keys from KDS*/
					if (s32RetVal != OSAL_ERROR)
					{
						OSAL_pvMemoryCopy(prSignFileHdl->au8PubKey,rCryptKDS_Entry.au8EntryData,ECC_PUB_KEY_LEN);
						fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                           "Pub key from KDS",0, 0, 0, 0 );
						//TraceIOString ("INFO : public keys from KDS!\n");
					}
					else
					{
						OSAL_pvMemoryCopy(prSignFileHdl->au8PubKey, au8Pub, ECC_PUB_KEY_LEN);
						fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                           "Pub key from Local buffer",0, 0, 0, 0 );
						//TraceIOString ("INFO : public keys from Local buffer!\n");
					}
					OSAL_s32IOClose(rCryptKDS_Hdl);
					/*Get the CID from the card*/
				
				                          			
		     			 
                                     /*Get the SD card info via USB interface*/             
                                    OSAL_pvMemorySet(&trIOCtrlCardState,0,sizeof(OSAL_trIOCtrlCardState));
		                   
		     
                             	if ( OSAL_E_NOERROR!= fd_device_ctrl_u32UsbSdCardInfo(&trIOCtrlCardState) )
                            	{
                                 		//TraceIOString ("INFO : get SD card info from USB failed!\n");
                                 		fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                           "Get CID failed",0, 0, 0, 0 );
                             	}
                             	else 
                             	{
                                 		/*Adapt the CID pattern as expected by the signature verfication function*/
                                  		fd_device_ctrl_vCIDPatternAdapt(trIOCtrlCardState.u8CIDRegister);
                                  		OSAL_pvMemoryCopy( au8CID, trIOCtrlCardState.u8CIDRegister,sizeof(au8CID));
                                   }
			     
			       
				       u8CIDLoop=0;
					for(u8Loop=15;u8Loop>0;u8Loop--)
					{		  
						au8CID[u8CIDLoop++]=(prSignFileHdl->au8CID)[u8Loop];
					}
					au8CID[15] = (prSignFileHdl->au8CID)[0];

			          /* Form the NAV_ROOT.DAT path */
			          OSAL_s32PrintFormat(szPath, "%s/%s",
			                arCryptDevPvtData[s32CryptDevID].szDevName, NAV_ROOT_PATH);

					//Verify Signature File
					if(cryptext_sd_verify(au8CID, prSignFileHdl->au8PubKey, au8ReadSignature, szPath)== 0)
					{
						//TraceIOString ("SUCCESS : Signature verification success!\n");
						fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                           "Sig verify success",0, 0, 0, 0 );
						return OSAL_E_NOERROR;
					}
					
				}//kds open
			}//close
		}//read
		else
		{
			//TraceIOString ("ERROR : Read Signature file failed!\n");
			fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                           "Read sig file failed",0, 0, 0, 0 );
							
			OSAL_s32IOClose( hSignFile );
		}
	}//open 
	else
	{
		//TraceIOString ("ERROR : Signature file not found!\n");
		fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                           "Sig file not found",0, 0, 0, 0 );
		return SF_NOT_PRESENT;
	}
   	
	
	//If verification is not successful, delete the signature file if card is not write protected.
	if(bNoWrPr==TRUE)
	{
	    	
		OSAL_s32IORemove( coszsignfileName );
	}
	
	//TraceIOString ("ERROR : Signature verification failed!\n");
	fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_IOCTRL_DEFAULT, TR_CRYPT_MSG_INFO, 
                                           "Sig verify failed",0, 0, 0, 0 );
	return SIG_FILE_FAIL;	
#endif
}



 
/*****************************************************************
| function implementation (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************************
 *
 * FUNCTION:
 *    fd_crypt_vRegisterService 
 *
 * DESCRIPTION:
 *    This function opens the trace device & registers a 
 *    callback function for the TTFIS SFC related commands.
 *    
 *
 * PARAMETERS:    
 *    None
 *
 * RETURNVALUE:
 *    None
 *
 * HISTORY:
 *
 *****************************************************************************/
tVoid fd_crypt_vRegisterService (tVoid)
{
  tS32 s32Error;
  OSAL_trIOCtrlLaunchChannel rTraceChannel;
  
  rFdTrace = OSAL_IOOpen (OSAL_C_STRING_DEVICE_TRACE, OSAL_EN_READWRITE);
  
  rTraceChannel.enTraceChannel = 0x95; //TR_TTFIS_CARD_CRYPT;
  
  rTraceChannel.pCallback = (OSAL_tpfCallback)fd_crypt_vChannelMgr;
  
  s32Error = OSAL_s32IOControl (rFdTrace, OSAL_C_S32_IOCTRL_CALLBACK_REG, (tS32) &rTraceChannel);
  (tVoid)s32Error;  
}

/*****************************************************************************
 *
 * FUNCTION:
 *    fd_crypt_vUnregisterService
 *
 * DESCRIPTION:
 *    This function unregisters the callback function for  
 *    SFC related commands & closes the trace device.
 *    
 *
 * PARAMETERS:    
 *    None
 *
 * RETURNVALUE:
 *    None
 *
 * HISTORY:
 *
 *****************************************************************************/
tVoid fd_crypt_vUnregisterService (tVoid)
{
  tS32 s32Error;
  OSAL_trIOCtrlLaunchChannel rTraceChannel;
  
  rTraceChannel.enTraceChannel = TR_TTFIS_CARD_CRYPT;
  
  rTraceChannel.pCallback = (OSAL_tpfCallback)fd_crypt_vChannelMgr;
  
  s32Error = OSAL_s32IOControl (rFdTrace, OSAL_C_S32_IOCTRL_CALLBACK_UNREG, (tS32) &rTraceChannel);
  
  OSAL_s32IOClose (rFdTrace);

  (tVoid)s32Error; 	 //To avoid Lint warning
}
#endif
