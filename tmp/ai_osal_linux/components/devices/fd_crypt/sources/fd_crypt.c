/*******************************************************************************
 *
 * FILE:
 *     fd_crypt.c
 *
 * REVISION:
 *     1.9
 *
 * AUTHOR:
 *     (c) 2007, Robert Bosch India Ltd., ECM/ECM-1, Divya H, 
 *                                                   divya.h@in.bosch.com
 *
 * CREATED:
 *     10/01/2007 - Divya H
 *
 * DESCRIPTION:
 *       This file contains the fd_crypt module code.
 *
 * NOTES:
 *
 * MODIFIED:
 *   DATE      |  AUTHOR  |         MODIFICATION
 *    27/06/07  |  Divya H |	Modified to incorporate Crypt Extension Logic
 *    11/09/07  |  Divya H |	Modified code to work without NU_Set_Current_Dir calls
 *    25/09/07  |  Divya H |	Modified for function_code:error_stage traces
 *    07/12/07  |  Divya H | Modified for MMS 160814 fix
 *    24/01/08  |  Vijay D | Modified for MMS 174264 fix
 *    24/03/08  |  Divya H |	Modified for Crypt extension signature file handling in VW
 *    31/03/08  |  Divya H |	Modified for removing hidden and write protected files
 *    28/04/08  |  Divya H |	Modified for performing navigation copy in VW (MMS 187997)
 *    19/05/08  |  Divya H |	Modified for appending huge file sizes
 *							(greater than drv_card split size for write) (MMS 187997)   
 *    27/05/08  |  Divya H | Modified for performing navigation copy in VW (MMS 187997)
 *    30/05/08  |  Divya H | Modified for performing navigation copy in VW
 * 							and enabling append through TTfis (MMS 187874)
 *    24/10/08  | Ravindran| Ported to ADIT platform
 *    02/02/09  | Ravindran| Removed code of AES encryption/decryption
 *    23/02/09  | Ravindran| Root directory changed to cryptnav.added special handling of IOCtls
 *    3/03/09   | Ravindran| SIGN_VERI_SUCCESS_SIMULATION - flag deactivated.Flags updated only
 *	                        in signature verfication
 *    8/06/09   | Ravindran| Warnings Removal
 *    16/06/09  | Ravindran| fd_crypt_file_open - return success with handle of "/dev/sda1"
 *	                        in absence of card
 *    24/06/09  | Ravindran| Use OSAL interface in fd_crypt_verify_signaturefile
 *    03/08/09  | Ravindran| changes for crypt via USB
 *    09.04.10  |Anoop Chandran |Optimisation of start up
 *                            |time by removal of redundant
 *                            |signature verfication call
 *                            |while start up  and also for thread safe, I have added semaphore
 *	                           |lock for FD_crypt signature verification
 *    22.10.10   |Gokulkrishnan N| VinFeature Porting for FORD-MFD
 *    22.10.10   |Ravindran P| Ported for Gen2 Linux PF
 *    05.04.11   |Anooj Gopi | Added multi device support
 *    01.11.11   |Madhu Kiran | prio1 lint is fixed in function bGetDeviceName
 *    09.02.12   | Anooj Gopi | KDS entry of Public key is corrected
 *    12.03.12   |Swathi Bolar| DID based implementation (INNAVPF-2544)
 *    12.09.14   |Kranthi Kiran| Lint and complier fixes
 ******************************************************************************/

/*****************************************************************
  | includes of component-internal interfaces
  | (scope: component-local)
  |--------------------------------------------------------------*/

/* OSAL Interface */
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "fd_crypt_trace.h"
#include "signfile_gen.h"
#include "fd_crypt_private.h"
#include "fetch_cert_data.h"

#include <sys/stat.h>
/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define MEDIA_DEVICE "/dev/media/"
#define MEDIA_DEVICE_SUBSTRING_COUNT 3
#define DEVICE_SUBSTRING_COUNT 2
#define FD_CRYPT_LOCK_NAME    "INIT_CRYPT"

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/* FD Crypt Device Private data structure */
trFdCryptPvtData *prFdCryptPvtData = OSAL_NULL;

/*Name of Semaphore*/
#define FD_CRYPT_SEM_NAME_MAX_LEN 20
static const tString coszFD_cryptDBSemName = "FD_CRYPT_DB_LOCK";
static const tString coszFD_cryptBPCLSemName = "FD_CRYPT_BPCL_LOCK";

OSAL_tSemHandle hFD_CryptBPCLSemaphore;
/*****************************************************************
| variable definition (scope: module-external)
|----------------------------------------------------------------*/
/*****************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------*/

extern tBool fd_crypt_verify_xml_signature( tCString coszCertPath );
extern tBool bFileExists( tCString s8CertPath );
tPVoid pvGetCryptDevDBEntry( tCString coszDevName );
tBool bSetCryptDevStateProgress( tPVoid pvDBEntry );
tBool bSetCryptDevStatus( tPVoid pvDBEntry, fd_crypt_tenError enVerifyResult );
tBool bGetCryptDevStatus( tPVoid pvDBEntry, OSAL_tenSignVerifyStatus *penVerifyStatus);
tBool bReadPublicKey(tPU8 pu8RBPublicKey);
tU32 fd_crypt_check_create(tVoid);
tS8 s8IsXmlVIN( tCString coszCertPath );

/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/


/*****************************************************************
| function implementation (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/
static int crypt_lock = 0;
/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_check_create
 *
 * DESCRIPTION:
 *     This function check whether fd_crypt create is called once for this
 *     process. If not fd_crypt_create will be called.
 *
 * PARAMETERS:
 *     None
 *
 * RETURNVALUE:
 *     OSAL_E_NOERROR - Success
 *     OSAL_E_UNKNOWN - failure
 *
 * HISTORY:
 *  24/05/2011  | Anooj Gopi | Initial version
 *
 *****************************************************************************/
tU32 fd_crypt_check_create(tVoid)
{
   tU32 u32RetVal = OSAL_E_UNKNOWN;

   crypt_lock = (int)sem_open(FD_CRYPT_LOCK_NAME,O_EXCL | O_CREAT, 0660, 0);
   if (crypt_lock == (int)SEM_FAILED)
   {
       crypt_lock = (int)sem_open(FD_CRYPT_LOCK_NAME, 0);
       if(crypt_lock == -1)
       {
          FATAL_M_ASSERT_ALWAYS();
       }
       for(;;)
       {
          if(sem_wait((sem_t *)crypt_lock) != 0)
          {
             /* check for incoming signal */
             if(errno != EINTR)
             {
                FATAL_M_ASSERT_ALWAYS();
             }
          }
          else
          {
             break;
          }
       }
   }
   if(prFdCryptPvtData == OSAL_NULL)
   {
         /* Lock the table */
         u32RetVal = fd_crypt_create();
   }
   else
   {
         /* fd_crypt create has been called once */
         u32RetVal = OSAL_E_NOERROR;
   }

   sem_post((sem_t *)crypt_lock);

   return u32RetVal;
}

/*****************************************************************************
 *
 * FUNCTION:
 *        fd_crypt_create
 *
 * DESCRIPTION:
 *        This function allocate and initialize resources needed for fd_crypt module
 *
 *
 * PARAMETERS:
 *     None
 *
 * RETURNVALUE:
 *     OSAL_E_NOERROR - Success
 *     OSAL_E_UNKNOWN - failure
 *
 * HISTORY:
 *  24/10/08  | Ravindran| Ported to ADIT platform
 *  02/02/09  | Ravindran| Removed code of AES encryption/decryption
 *  05/04/11  | Anooj Gopi | Added Multi device support
 *
 *****************************************************************************/
tU32 fd_crypt_create( tVoid )
{
   tU32 u32RetVal;
   OSAL_tProcessID hPID;
   tBool bRet = TRUE;
   tChar szSemName[FD_CRYPT_SEM_NAME_MAX_LEN+8]; /* 8 bytes extra to append PID */

   fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_CREATE, TR_CRYPT_MSG_INFO, "Enter Crypt Create", 0, 0, 0, 0);

#ifndef TSIM_OSAL

   /* Read the Process ID*/
   hPID = OSAL_ProcessWhoAmI();
   fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_CREATE, TR_CRYPT_MSG_INFO,
      "FD_crypt_create Process ID", (tS32)hPID, 0, 0, 0);

   prFdCryptPvtData = OSAL_pvMemoryAllocate( sizeof(trFdCryptPvtData));
   if(prFdCryptPvtData)
   {
      /* Clear the private data base */
      OSAL_pvMemorySet(prFdCryptPvtData, 0, sizeof(trFdCryptPvtData));

      prFdCryptPvtData->hFD_CryptDBSemaphore = OSAL_C_INVALID_HANDLE;
      prFdCryptPvtData->hFD_CryptBPCLSemaphore = OSAL_C_INVALID_HANDLE;

      /* Read public key */
      if(!bReadPublicKey(prFdCryptPvtData->au8RBPublicKey))
      {
         /* Mark the error */
         bRet = FALSE;
      }

      /* If all is good till now, create the DB semaphore */
      if( bRet )
      {
         /* Since OSAL is having named semaphores only, we have to have different name for
          * each process. Thus prepare the semaphore name by appending process id to common name */
         OSAL_s32PrintFormat(szSemName, "%s%d", coszFD_cryptDBSemName, (tInt)hPID);

         /* create semaphore to protect access to fd crypt device database */
         if( OSAL_OK != OSAL_s32SemaphoreCreate( szSemName,
               &(prFdCryptPvtData->hFD_CryptDBSemaphore), 1 ) )
         {
            fd_crypt_vTraceInfo(TR_LEVEL_FATAL, TR_CRYPT_CREATE,
                  TR_CRYPT_MSG_ERROR, "FDcrypt DB Sem Create Failed",
                  (tS32)0, 0, 0, 0);
            bRet = FALSE;
         }
      }

      /* If all is good till now, create the BPCL semaphore */
      if( bRet )
      {
         /* Since OSAL is having named semaphores only, we have to have different name for
          * each process. Thus prepare the semaphore name by appending process id to common name */
         OSAL_s32PrintFormat(szSemName, "%s%d", coszFD_cryptBPCLSemName, (tInt)hPID);

         /* Create the semaphore to protect Non-reentrant BPCL functions */
         if( OSAL_OK != OSAL_s32SemaphoreCreate( szSemName,
                             &(prFdCryptPvtData->hFD_CryptBPCLSemaphore), 1 ) )
         {
            fd_crypt_vTraceInfo(TR_LEVEL_FATAL, TR_CRYPT_CREATE,
                     TR_CRYPT_MSG_ERROR, "FDcrypt BPCL Sem Create Failed",
                     (tS32)ERROR_FD_CRYPT_UNKNOWN, 0, 0, 0);
            bRet = FALSE;
         }
      }
   }
   else
   {
      /* Mark the error */
      bRet = FALSE;
   }

   /* If we are here due to any error, clean up all the allocated resources */
   if( bRet != TRUE )
   {
      /* Free all the allocated resources */
      if(prFdCryptPvtData)
      {
         /* Delete if already created */
         if(prFdCryptPvtData->hFD_CryptDBSemaphore != OSAL_C_INVALID_HANDLE)
         {
            /* Close the semaphore handle */
            OSAL_s32SemaphoreClose( prFdCryptPvtData->hFD_CryptDBSemaphore );
            /* Delete the semaphore */
            OSAL_s32PrintFormat(szSemName, "%s%d", coszFD_cryptDBSemName, (tInt)hPID);
            OSAL_s32SemaphoreDelete(szSemName);
         }

         /* Delete the BPCL semaphore */
         if( prFdCryptPvtData->hFD_CryptBPCLSemaphore != OSAL_C_INVALID_HANDLE)
         {
            /* Close the semaphore handle */
            OSAL_s32SemaphoreClose( prFdCryptPvtData->hFD_CryptBPCLSemaphore );
            /* Delete the semaphore */
            OSAL_s32PrintFormat(szSemName, "%s%d", coszFD_cryptBPCLSemName, (tInt)hPID);
            OSAL_s32SemaphoreDelete( szSemName );
         }

         OSAL_vMemoryFree( prFdCryptPvtData );
         prFdCryptPvtData = OSAL_NULL;

         fd_crypt_vTraceInfo(TR_LEVEL_FATAL, TR_CRYPT_CREATE,
               TR_CRYPT_MSG_INFO, "FDcrypt pvt data free", 0, 0, 0, 0);
      }
   }

#else /*#ifndef TSIM_OSAL*/
   bRet = TRUE;
#endif   /*#ifndef TSIM_OSAL*/

   if( bRet == TRUE )
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_CREATE, TR_CRYPT_MSG_SUCCESS,
            "Successful create exit", 0, 0, 0, 0);
      u32RetVal = OSAL_E_NOERROR;
   }
   else
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_3, TR_CRYPT_CREATE, TR_CRYPT_MSG_ERROR,
            "Unable to create fd_crypt", 0, 0, 0, 0);
      u32RetVal = OSAL_E_UNKNOWN;
   }

   return u32RetVal;
}

/*****************************************************************************
 *
 * FUNCTION:
 *     bReadPublickey
 *
 * DESCRIPTION:
 *     This function read the public key.
 *
 * PARAMETERS:
 *     pu8RBPublicKey (O) - Pointer to public key
 *
 * RETURNVALUE:
 *     TRUE - Success
 *     FALSE - failure
 *
 * HISTORY:
 *  24/05/2011  | Anooj Gopi | Initial version
 *
 *****************************************************************************/
tBool bReadPublicKey(tPU8 pu8RBPublicKey)
{
   OSAL_tIODescriptor rCryptKDS_Hdl;
   tsKDSEntry rCryptKDS_Entry;
   tBool bRet = FALSE;
   tS32 s32RetVal;

   if(!pu8RBPublicKey)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_FATAL, TR_CRYPT_CREATE,
            TR_CRYPT_MSG_ERROR, "Invalid argument in bReadPublicKey", 0, 0, 0, 0);
   }
   /* Open KDS to Read the public key */
   else if((rCryptKDS_Hdl = OSAL_IOOpen( KDS_DEVICE, OSAL_EN_READONLY)) == OSAL_ERROR)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_FATAL, TR_CRYPT_CREATE, TR_CRYPT_MSG_ERROR,
            "Cannot open KDS device, ReadPublicKey failed!!!! ",
            (tS32)ERROR_FD_CRYPT_KDS_DEVICE_FAIL, 0, 0, 0);
   }
   else
   {
      /* Copying RB Public Key from KDS */
      OSAL_pvMemorySet( &rCryptKDS_Entry, 0, sizeof( rCryptKDS_Entry ) );

      //No dependency on device config table.Hard coding default values..
      rCryptKDS_Entry.u16Entry       =  KDS_ECC_PUBKEY_ENTRY;
      rCryptKDS_Entry.u16EntryLength =  ECC_PUB_KEY_LEN;
      /* Perform read */
      s32RetVal = OSAL_s32IORead( rCryptKDS_Hdl,
                    ( tPS8 )&rCryptKDS_Entry,
                    ( tU32 )sizeof( rCryptKDS_Entry ) );

      if ( s32RetVal != OSAL_ERROR)
      {
         OSAL_pvMemoryCopy( pu8RBPublicKey, rCryptKDS_Entry.au8EntryData,
               ECC_PUB_KEY_LEN );
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_SUCCESS,
                   "Get KDS RB public key success", 0, 0, 0, 0 );
         bRet = TRUE;
      }
      else
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
               "Using Hard-Coded RB Pub key", 0, 0, 0, 0 );
         //Different public key as for CID/VIN/DID based on encryption
         pu8RBPublicKey[0]=0xaf;
         pu8RBPublicKey[1]=0xd2;
         pu8RBPublicKey[2]=0xc6;
         pu8RBPublicKey[3]=0xa2;
         pu8RBPublicKey[4]=0x2b;
         pu8RBPublicKey[5]=0x2d;
         pu8RBPublicKey[6]=0x17;
         pu8RBPublicKey[7]=0x1d;
         pu8RBPublicKey[8]=0xea;
         pu8RBPublicKey[9]=0x36;
         pu8RBPublicKey[10]=0x17;
         pu8RBPublicKey[11]=0x33;
         pu8RBPublicKey[12]=0x1d;
         pu8RBPublicKey[13]=0x9a;
         pu8RBPublicKey[14]=0x87;
         pu8RBPublicKey[15]=0x47;
         pu8RBPublicKey[16]=0x54;
         pu8RBPublicKey[17]=0x15;
         pu8RBPublicKey[18]=0x29;
         pu8RBPublicKey[19]=0x3e;
         pu8RBPublicKey[20]=0xd6;
         pu8RBPublicKey[21]=0xda;
         pu8RBPublicKey[22]=0x39;
         pu8RBPublicKey[23]=0x11;
         pu8RBPublicKey[24]=0xb5;
         pu8RBPublicKey[25]=0x85;
         pu8RBPublicKey[26]=0x13;
         pu8RBPublicKey[27]=0x49;
         pu8RBPublicKey[28]=0xa3;
         pu8RBPublicKey[29]=0xa2;
         pu8RBPublicKey[30]=0xe7;
         pu8RBPublicKey[31]=0x90;
         pu8RBPublicKey[32]=0x74;
         pu8RBPublicKey[33]=0x62;
         pu8RBPublicKey[34]=0x1e;
         pu8RBPublicKey[35]=0x01;
         pu8RBPublicKey[36]=0x6d;
         pu8RBPublicKey[37]=0xed;
         pu8RBPublicKey[38]=0x73;
         pu8RBPublicKey[39]=0xc2;

         bRet = TRUE;
      }

      /* END: Copying RB Public Key from KDS */
      s32RetVal = OSAL_s32IOClose(rCryptKDS_Hdl);

      if(s32RetVal != OSAL_OK)
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                "OSAL_s32IOClose failed in ReadPublicKey", 0, 0, 0, 0 );
      }
   }

   return bRet;
}

 /*****************************************************************************
  *
  * FUNCTION:
  *     fd_crypt_destory
  *
  * DESCRIPTION:
  *     This function is called by devinit.c to remove the fd_crypt module.
  *
  * PARAMETERS:
  *    None
  *
  * RETURNVALUE:
  *    OSAL_E_NOERROR - Success
  *    OSAL_E_UNKNOWN - failure
  *
  * HISTORY:
  *    24/10/08  | Ravindran| Ported to ADIT platform
  *    05/04/11  | Anooj Gopi | Added Multi device support
  *****************************************************************************/
tU32 fd_crypt_destroy(tVoid)
{
   OSAL_tProcessID hPID;
   tChar szSemName[FD_CRYPT_SEM_NAME_MAX_LEN+8]; /* 8 bytes extra to append process id */
   tU32 u32Ret = OSAL_E_NOERROR;

   fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_DESTROY, TR_CRYPT_MSG_INFO, "Enter Crypt Destroy", 0, 0, 0, 0);

#ifndef TSIM_OSAL
   /* Read the Process ID*/
   hPID = OSAL_ProcessWhoAmI();
   fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_CREATE, TR_CRYPT_MSG_INFO,
      "FD_crypt_destroy Process ID", (tS32)hPID, 0, 0, 0);

   if(prFdCryptPvtData)
   {
      if( prFdCryptPvtData->hFD_CryptDBSemaphore != OSAL_C_INVALID_HANDLE )
      {
         /* Close the semaphore handle */
         OSAL_s32SemaphoreClose( prFdCryptPvtData->hFD_CryptDBSemaphore );
         prFdCryptPvtData->hFD_CryptDBSemaphore = OSAL_C_INVALID_HANDLE;

         /* Prepare the semaphore name by appending Process id to common name */
         OSAL_s32PrintFormat(szSemName, "%s%d", coszFD_cryptDBSemName, (tInt)hPID);
         OSAL_s32SemaphoreDelete( szSemName );
      }
      else
      {
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_CREATE, TR_CRYPT_MSG_ERROR,
            "Err: NULL DB Handle in FDCrypt Destroy", 0, 0, 0, 0);
         u32Ret = OSAL_E_UNKNOWN;
      }

      /* Delete BPCL semaphore */
      if( prFdCryptPvtData->hFD_CryptBPCLSemaphore != OSAL_C_INVALID_HANDLE)
      {
         /* Close the semaphore handle and delete it */
         OSAL_s32SemaphoreClose( prFdCryptPvtData->hFD_CryptBPCLSemaphore );
         prFdCryptPvtData->hFD_CryptBPCLSemaphore = OSAL_C_INVALID_HANDLE;

         /* Prepare the semaphore name by appending Process id to common name */
         OSAL_s32PrintFormat(szSemName, "%s%d", coszFD_cryptBPCLSemName, (tInt)hPID);
         OSAL_s32SemaphoreDelete( szSemName );
      }
      else
      {
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_CREATE, TR_CRYPT_MSG_ERROR,
            "Err: NULL BPCL Handle in FDCrypt Destroy", 0, 0, 0, 0);
         u32Ret = OSAL_E_UNKNOWN;
      }

      OSAL_vMemoryFree( prFdCryptPvtData );

   }
   else
   {
      u32Ret = OSAL_E_UNKNOWN;
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_CREATE, TR_CRYPT_MSG_ERROR,
         "Err: NULL Pvt Handle in FDCrypt Destroy", 0, 0, 0, 0);
   }

#endif

   return u32Ret;
}

/*****************************************************************************
 *
 * FUNCTION:
 *    fd_crypt_sdx_xml_verification
 *
 * DESCRIPTION:
 *    This function is called by fd_crypt_verify_signaturefile to verify XML based signature file.
 *
 *
 * PARAMETERS:
 *    coszCertPath  : Certificate file path.
 *
 * RETURNVALUE:
 *    FD_CRYPT_SUCCESS                      - Success
 *    ERROR_FD_CRYPT_SIGNVERI_FAILED        - Signature file verification failed
 *    ERROR_FD_CRYPT_UNKNOWN                - Unknown
 *
 *
 * HISTORY:
 *    DATE      |  AUTHOR    |         MODIFICATION
 *
 *   13/12/10   | kgr1kor    | Added a separate function to verify xml based signature file
 *   05/04/11   | Anooj Gopi | Added multi device support
 *
 *****************************************************************************/
fd_crypt_tenError fd_crypt_sdx_xml_verification( tCString coszCertPath )
{
#ifndef TSIM_OSAL
   fd_crypt_tenError enXMLResult       = ERROR_FD_CRYPT_UNKNOWN;

   fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                        "Enter Verify XML SF", 0, 0, 0, 0 );
   if( fd_crypt_verify_xml_signature( coszCertPath ) == FALSE )
   {
      enXMLResult    = ERROR_FD_CRYPT_SIGNVERI_FAILED;
   }
   else
   {
      enXMLResult    = FD_CRYPT_SUCCESS;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                           TR_CRYPT_MSG_SUCCESS, "Verify Success", 0, 0, 0, 0 );
   }
   return enXMLResult;
#else
   return FD_CRYPT_SUCCESS;
#endif   
}

#ifdef FD_CRYPT_NON_XML_SUPPORT//Obsolete Code
/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_sd_non_xml_verification
 *
 * DESCRIPTION:
 *     This function is called by fd_crypt_verify_signaturefile to verify NON-XML based signature file.
 *
 *
 * PARAMETERS:
 *      FSId:           File system ID
 *      pu8Cid:         Pointer to CID data
 *      bSignPres:      Signature File presence flag
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS                  - Success
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED    - Signature file verification failed 
 *     ERROR_FD_CRYPT_UNKNOWN            - Unknown
 * 
 *
 * HISTORY:
 *   DATE      |  AUTHOR     |         MODIFICATION
 *
 *   13/12/10  | kgr1kor     | Added a separate function to verify non_xml based signature file
 *   19/03/11  | Anooj Gopi  | Added multi crypt device support
 *****************************************************************************/
fd_crypt_tenError fd_crypt_sd_non_xml_verification( tS32 s32CryptDevID, tCString coszCertPath )
{
#ifndef TSIM_OSAL
   fd_crypt_tenError enNonXMLResult = ERROR_FD_CRYPT_UNKNOWN;
   tS32 s32RetVal                   = OSAL_ERROR;
   tU16 u16TrFnErCd                 = (FD_CRYPT_VERIFY_SF<<8);
   tU8 u8CIDLoop                    = 0;
   tU8 u8Loop                       = 0;
   tU8 au8CID[CID_LEN]              = {0}; 
   tS32 s32Read                     = 0; 
   tU32 u32AddrSignBuf;
   OSAL_tIODescriptor hSignFile     = 0;
   OSAL_tenAccess enAccess          = OSAL_EN_READONLY;
   tChar szPath[60];
   trFd_Crypt_Key *prFdCryptKeyHdl;
   trFdCryptDevPvtData *prPvtData;

   prPvtData = &arCryptDevPvtData[s32CryptDevID];
   prFdCryptKeyHdl = &(prPvtData->rFdCryptKeyHdl);
   u32AddrSignBuf = (tU32)prFdCryptKeyHdl->au8ReadSignature;
   
   hSignFile = OSAL_IOOpen(coszCertPath, enAccess);

   fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO, 
                        "Sig file open checked",0, 0, 0, 0 );
   if ( hSignFile == OSAL_ERROR )
   {
      /* No signature file present. So, no verification to do */
       fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_INFO, "SF not present-exit no veri", 
                           (tS32)FD_CRYPT_SF_NOT_PRESENT, 0, 0, 0 );
       enNonXMLResult    =  FD_CRYPT_SF_NOT_PRESENT;
	
   }
   else if(OSAL_E_NOERROR != fd_device_ctrl_u32ReadCid(prFdCryptKeyHdl->au8CID))
   {
      /* Could not read hardware CID */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                           TR_CRYPT_MSG_INFO, "Could not read hardware CID",
                           (tS32)ERROR_FD_CRYPT_CID_READ_FAIL, 0, 0, 0 );
      enNonXMLResult    =  ERROR_FD_CRYPT_CID_READ_FAIL;
   }
   else
   {
      /* SD_META.DAT - Crypt Extension Signature file exists */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Enter CryptExtSF Present", 0, 0, 0, 0 );
      /* Read the signature file */    
      s32Read = OSAL_s32IORead(hSignFile, (tPS8)u32AddrSignBuf, SIGN_FILE_SIZE);
      if ( s32Read == SIGN_FILE_SIZE )
      {
         s32RetVal = OSAL_s32IOClose( hSignFile );
         if ( s32RetVal != OSAL_ERROR )
         {
            u8CIDLoop=0;
            for(u8Loop=15;u8Loop>0;u8Loop--)
            {
               au8CID[u8CIDLoop++]=(prFdCryptKeyHdl->au8CID)[u8Loop];
            }
            au8CID[15] = (prFdCryptKeyHdl->au8CID)[0];

            /* Form the NAV_ROOT.DAT path */
            OSAL_s32PrintFormat(szPath, "%s/%s", prPvtData->szDevName, NAV_ROOT_PATH);

            /* Verify the signature file read */
            if( cryptext_sd_verify( au8CID, prFdCryptKeyHdl->au8Pub,
                                   prFdCryptKeyHdl->au8ReadSignature, szPath) == 0 )
            {
               /* Signature file successfully verified */
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                    TR_CRYPT_MSG_SUCCESS,
                                    "Success ExtSF veri exit", 0, 0, 0, 0 );
               enNonXMLResult =     FD_CRYPT_SUCCESS;
            }
            else
            {
               enNonXMLResult =     ERROR_FD_CRYPT_SIGNVERI_FAILED;
            }
         }
      }
      else
      {
         OSAL_s32IOClose( hSignFile );
         enNonXMLResult =     ERROR_FD_CRYPT_SIGNVERI_FAILED;
      }
	  
      if( enNonXMLResult == ERROR_FD_CRYPT_UNKNOWN )
      {
      
         fd_crypt_vTraceInfo( TR_LEVEL_USER_3, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "SF Verification failed",(tS32)s32RetVal, 0, 0, 0 );
         u16TrFnErCd|=6;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_3, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_FNER,
                              "fd_crypt Error: 0x",(tS32)u16TrFnErCd, 0, 0, 0 );
      }
   }
   return enNonXMLResult;
#else
   return FD_CRYPT_SUCCESS;
#endif
}
#endif

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_verify_signaturefile
 *
 * DESCRIPTION:
 *     This function verifies the signature file for the device
 *     (The device name is extracted from the file path)
 *
 * PARAMETERS:
 *     coszCertPath - Absolute path of the certificate.
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS                   - Success
 *     FD_CRYPT_SF_NOT_PRESENT            - Signature File not present
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED     - Signature file verification failed
 *     ERROR_FD_CRYPT_NOTSUPPORTED        - Database full, No more device supported
 *     ERROR_FD_CRYPT_UNKNOWN             - Unknown
 *     
 *
 * HISTORY:
 *  DATE      |  AUTHOR  |         MODIFICATION
 *  25/09/07  |  Divya H | Modified for function_code:error_stage traces
 *  24/03/08  |  Divya H | Modified for Crypt extension signature file
 *            |          | handling in VW
    24/10/08  | Ravindran| Ported to ADIT platform
    02/02/09  | Ravindran| Removed code of AES encryption/decryption
 *  13/12/10  | kgr1kor  | Re-organised the function and issue regarding critical section.
 *  05/04/11  | AnoojGopi| Added multiple crypt device support
 *****************************************************************************/
fd_crypt_tenError fd_crypt_verify_signaturefile( tCString coszCertPath )
{
   fd_crypt_tenError enResult = ERROR_FD_CRYPT_UNKNOWN;
   tPVoid pvCryptDevDBEntry = OSAL_NULL;
   tChar szDevName[FD_CRYPT_DEV_NAME_MAX_LEN];

   fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                        "Enter Verify SF", 0, 0, 0, 0 );

   /* Trace out the certificate path */
   if(coszCertPath)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                coszCertPath, 0, 0, 0, 0);
   }

#ifndef TSIM_OSAL
   /* Call fd-crypt create if not already called for this process */
   if(fd_crypt_check_create() != OSAL_E_NOERROR)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
               "fd_crypt_check_create failed", 0, 0, 0, 0);
      enResult = ERROR_FD_CRYPT_UNKNOWN;
   }
   /* Check if certificate path is valid */
   else if( !coszCertPath )
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
               "Certificate path NULL", 0, 0, 0, 0);
      enResult = FD_CRYPT_SF_NOT_PRESENT;
   }
   else if( !bFileExists(coszCertPath) )
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
               "Certificate path is invalid", 0, 0, 0, 0);
      enResult = FD_CRYPT_SF_NOT_PRESENT;
   }
   /* Extract the device name from certificate */
   else if( bGetDeviceName(szDevName, coszCertPath) == FALSE)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
               "Can't extract dev name from cert path", 0, 0, 0, 0);
      enResult = FD_CRYPT_SF_NOT_PRESENT;
   }
   /* Get the Database entry corresponding to the device */
   else if( (pvCryptDevDBEntry = pvGetCryptDevDBEntry(szDevName)) == OSAL_NULL)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
               "FD crypt: Database is full", 0, 0, 0, 0);
      enResult = ERROR_FD_CRYPT_NOTSUPPORTED;
   }
   /* Mark that certificate verification is on progress in database entry */
   else if(bSetCryptDevStateProgress( pvCryptDevDBEntry ) == FALSE)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
               "Marking progress in DB failed", 0, 0, 0, 0);
      enResult = ERROR_FD_CRYPT_UNKNOWN;
   }
   else
   {
      /* Proceed  signature verification succeeds*/
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                          "Crypt enabled-signature verification start", 0, 0, 0, 0);

#ifdef FD_CRYPT_NON_XML_SUPPORT//Obsolete Code
      tU8 au8PubCpy[ECC_PUB_KEY_LEN] = {0};
      tU8 au8RBPubCpy[ECC_PUB_KEY_LEN] = {0};

      //This is because if previously SF verification was successful, and this time
      //it is not successful, then the previous card's key data should be erased,
      //except the public key, as public key is common to all cards

      OSAL_pvMemoryCopy(au8PubCpy, prFdCryptKeyHdl->au8Pub, ECC_PUB_KEY_LEN);
      OSAL_pvMemoryCopy(au8RBPubCpy, prFdCryptKeyHdl->au8RBPubKeybuf, ECC_PUB_KEY_LEN);
      OSAL_pvMemorySet(prFdCryptKeyHdl, 0, sizeof(trFd_Crypt_Key));
      OSAL_pvMemoryCopy(prFdCryptKeyHdl->au8RBPubKeybuf, au8RBPubCpy, ECC_PUB_KEY_LEN);
      OSAL_pvMemoryCopy(prFdCryptKeyHdl->au8Pub, au8PubCpy, ECC_PUB_KEY_LEN);

      // Need to find a method to decide whether we have to go for non xml verification
      enResult = fd_crypt_sd_non_xml_verification( s32CryptDevID );
#endif

      enResult = fd_crypt_sdx_xml_verification( coszCertPath );

      /* Update the verification result for this device in database */
      if( bSetCryptDevStatus( pvCryptDevDBEntry, enResult ) == FALSE)
      {
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                  "ERR: SetCryptDevStatus failed", 0, 0, 0, 0);
         enResult = ERROR_FD_CRYPT_UNKNOWN;
      }
   }
#else /*In Tsim environment verfication passes always*/
   fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                     "Tsim environment-signature verification always success",
                     (tS32)FD_CRYPT_SUCCESS, 0, 0, 0 );
   enResult = FD_CRYPT_SUCCESS;
#endif /*#ifndef TSIM_OSAL*/

   return enResult;
}

/*****************************************************************************
 *
 * FUNCTION:
 *     FD_Crypt_vEnterCriticalSection
 *
 * DESCRIPTION                                                          
 *     Semaphore wait for device specific critical section
 *
 * PARAMETERS:
 *     None
 *
 * RETURNVALUE:
 *     TRUE                      - Success
 *     FALSE                     - Failure
 *
 * HISTORY:
 *     DATE      |  AUTHOR        |         MODIFICATION
 *    09/04/10   | Anoop Chandran |  Created 1st version
 *    05/04/11   | Anooj Gopi     |  Made locking specific to device
************************************************************************/
tBool FD_Crypt_vEnterCriticalSection( tVoid )
{
   tBool bResult = FALSE;

   if (!prFdCryptPvtData)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                 "Err: prFdCryptPvtData is NULL", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
   else if (prFdCryptPvtData->hFD_CryptDBSemaphore == OSAL_C_INVALID_HANDLE)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                 "Sem Handle invalid-EnterCritSec", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
   else if(OSAL_s32SemaphoreWait(prFdCryptPvtData->hFD_CryptDBSemaphore, OSAL_C_TIMEOUT_FOREVER ) != OSAL_OK)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
      "EnterCritSec sem take failed!!! ", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
   else
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
      "FD_Crypt_vEnterCriticalSection ", 0, 0, 0, 0);
      bResult = TRUE;
   }

   return bResult;
}

/*****************************************************************************
 *
 * FUNCTION:
 *    FD_Crypt_vLeaveCriticalSection
 *
 * DESCRIPTION                                                          
 *    Semaphore post for device specific critical section
 *
 * PARAMETERS:
 *    s32CryptDevID : Crypt Device ID. Value Range : 0 to (FD_CRYPT_MAX_DEVICES-1)
 *
 * RETURNVALUE:
 *    tVoid
 *
 * HISTORY:
 *     DATE      |  AUTHOR        |         MODIFICATION
 *    09/04/10   | Anoop Chandran |        Created 1st version
 *    05/04/11   | Anooj Gopi     |  Made locking specific to device
************************************************************************/
tVoid FD_Crypt_vLeaveCriticalSection( tVoid )
{
   if (!prFdCryptPvtData)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                 "Err: prFdCryptPvtData is NULL", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
   else if (prFdCryptPvtData->hFD_CryptDBSemaphore == OSAL_C_INVALID_HANDLE)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                 "Sem Handle invalid-EnterCritSec", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
   else
   {
      OSAL_s32SemaphorePost( prFdCryptPvtData->hFD_CryptDBSemaphore );
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
             "FD_Crypt_vLeaveCriticalSection ", 0, 0, 0, 0);
   }
   return;
}

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_get_signaturefile_type
 *
 * DESCRIPTION                                                          
 *     Gets the signature file type
 *
 * PARAMETERS:
 *     coszCertPath - Absolute path of the certificate.
 *
 * RETURNVALUE:
 *     OSAL_EN_SIGN_TYPE_UNKNOWN - Unknown certificate
 *     OSAL_EN_SIGN_XML_VIN      - VIN Based XML certificate
 *     OSAL_EN_SIGN_XML_CID      - CID Based XML certificate
 *
 * HISTORY:
 *   DATE         |  AUTHOR     |         MODIFICATION
 *   23/12/10     | Ravindran P |     Created 1st version
 *   05/04/11     | AnoojGopi   | Added multiple crypt device support
 *   19/05/11     | AnoojGopi   | Removed lock
************************************************************************/
OSAL_tenSignFileType fd_crypt_get_signaturefile_type( tCString coszCertPath )
{
   OSAL_tenSignFileType enSignFileType = OSAL_EN_SIGN_TYPE_UNKNOWN;
   tS8 s8RetVal = 0;

   /* Trace out the certificate path */
   if(coszCertPath)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                coszCertPath, 0, 0, 0, 0);
   }

   /* Call fd-crypt create if not already called for this process */
   if(fd_crypt_check_create() != OSAL_E_NOERROR)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
               "fd_crypt_check_create failed", 0, 0, 0, 0);
   }
   else if( !coszCertPath )
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                "Certificate path is NULL", 0, 0, 0, 0);
   }
   else if( !bFileExists(coszCertPath) )
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                "Certificate path is not valid", 0, 0, 0, 0);
   }
   else
   {
      s8RetVal =  s8IsXmlVIN( coszCertPath );
      if(XML_VIN == s8RetVal )
      {
         enSignFileType = OSAL_EN_SIGN_XML_VIN;
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
             " XML_VIN signature", 0, 0, 0, 0);
      }
     if(XML_DID == s8RetVal ) //DID Implementation(INNAVPF-2544)
      {
         enSignFileType = OSAL_EN_SIGN_XML_DID;
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
             " XML_DID signature", 0, 0, 0, 0);
      }
      else  if(XML_CID == s8RetVal )
      {
         enSignFileType = OSAL_EN_SIGN_XML_CID;
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
             " XML_CID signature", 0, 0, 0, 0);
      }
   }

   if(enSignFileType == OSAL_EN_SIGN_TYPE_UNKNOWN)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
	     "Unknown signature", 0, 0, 0, 0);
   }
	  
   return  enSignFileType; 
     
}

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_get_signature_verify_status
 *
 * DESCRIPTION                                                          
 *      Gets the signature verify status for the device specified
 *
 * PARAMETERS:
 *      coszDevName - Device name
 *
 * RETURNVALUE:
 *      OSAL_EN_SIGN_VERIFY_PASSED - Verification passed for this device
 *      OSAL_EN_SIGN_VERIFY_INPROGRESS  - Verification is in progress for the device
 *      OSAL_EN_SIGN_VERIFY_FAILED - Verification failed for the device
 *      OSAL_EN_SIGN_STATUS_UNKNOWN - Device in unknown error state
 *
 * HISTORY:
 *   DATE       |  AUTHOR     |         MODIFICATION
 *   23/12/10   | Ravindran P |       Created 1st version
 *   05/04/11   | Anooj Gopi  |      Added multi device support
************************************************************************/
OSAL_tenSignVerifyStatus fd_crypt_get_signature_verify_status( tCString coszDevName )
{
   OSAL_tenSignVerifyStatus enSignVerifyStatus = OSAL_EN_SIGN_STATUS_UNKNOWN;
   tPVoid pvDBEntry;

   /* Trace out device name */
   if( coszDevName )
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
            coszDevName, 0, 0, 0, 0);
   }

   /* Call fd-crypt create if not already called for this process */
   if(fd_crypt_check_create() != OSAL_E_NOERROR)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
               "fd_crypt_check_create failed", 0, 0, 0, 0);
   }
   else if( !coszDevName )
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
        "ERR: Device Name is NULL", 0, 0, 0, 0);
   }
   else if((pvDBEntry = pvGetCryptDevDBEntry(coszDevName)) == OSAL_NULL)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
        "GetCryptDevDBEntry failed", 0, 0, 0, 0);
   }
   else if( bGetCryptDevStatus( pvDBEntry, &enSignVerifyStatus) == FALSE)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
        "bGetCryptDevState failed", 0, 0, 0, 0);
   }

   return enSignVerifyStatus;
}

/**************************************************************************************
 *
 * FUNCTION:
 *     bGetDeviceName
 *
 * DESCRIPTION:
 *     This function extracts device name from the certificate path
 *
 * PARAMETERS:
 *     szDevName - (Output) Device Name (buffer of size FD_CRYPT_DEV_NAME_MAX_LEN)
 *     coszCertPath - (Input) Certificate Path
 *
 * RETURNVALUE:
 *     TRUE - Successfully extracted device name from the path
 *     FALSE - Couldn't get the device name from the path
 *
 * HISTORY:
 *     05/04/11    |   Anooj Gopi   |   Created 1st version
 *-------------------------------------------------------------------------------------
 *     02/11/11    |   Madhu Kiran  | Fixed lint and modified logic of extracting
 *                 |                | the device name to reduce cyclomatic complexity.
 *************************************************************************************/
tBool bGetDeviceName( tString szDevName, tCString coszPath )
{
   tPS8 ps8Ptr=(tPS8)coszPath;
   tU32 u32DevNameLen = 0;
   tU32 u32SubStringCnt = 0;
   tBool bStatus = FALSE;

   if(coszPath && szDevName)
   {
      /* dev/media should have three substring separated by '/'. */
      if( (tU32)OSAL_ps8StringSubString(coszPath, MEDIA_DEVICE ) == (tU32)coszPath )
      {                                                                    
         u32SubStringCnt = MEDIA_DEVICE_SUBSTRING_COUNT;
      }
      else
      {
         u32SubStringCnt = DEVICE_SUBSTRING_COUNT;
      }

      for(;u32SubStringCnt;u32SubStringCnt--)
      {
         ps8Ptr = OSAL_ps8StringSearchChar((ps8Ptr+1), '/'); //lint !e64//
         if(!ps8Ptr)
         {

            break;
         }
      }
      
      /* if coszPath has sufficient number of '/', Copy the device Name */
      if(ps8Ptr)
      {
         u32DevNameLen = (tU32)(ps8Ptr) - (tU32)(coszPath);
         if((u32DevNameLen > 0) && (u32DevNameLen < FD_CRYPT_DEV_NAME_MAX_LEN))
         {
            bStatus = TRUE;
            (tVoid)OSAL_szStringNCopy(szDevName, coszPath, u32DevNameLen);
            szDevName[u32DevNameLen] = '\0';            
         }     
      }
   }   
   return bStatus;
}

/*****************************************************************************
 *
 * FUNCTION:
 *    FD_Crypt_vEnterBPCLCriticalSec
 *
 * DESCRIPTION
 *    Semaphore wait for Non-reentrant BPCL functions
 *
 * PARAMETERS:
 *    None
 *
 * RETURNVALUE:
 *    TRUE                        - Success
 *    FALSE                     - Failure
 *
 * HISTORY:
 *   DATE      |  AUTHOR        |         MODIFICATION
 *  05/04/11   | Anooj Gopi     |      Created 1st version
************************************************************************/
tBool FD_Crypt_vEnterBPCLCriticalSec( tVoid )
{
   tBool bStatus = FALSE;

   if (!prFdCryptPvtData)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                 "Err: prFdCryptPvtData is NULL", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
   else if (prFdCryptPvtData->hFD_CryptBPCLSemaphore == OSAL_C_INVALID_HANDLE)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                 "Sem Handle invalid-EnterBPCLCritSec", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
   else if (OSAL_s32SemaphoreWait(prFdCryptPvtData->hFD_CryptBPCLSemaphore, OSAL_C_TIMEOUT_FOREVER ) == OSAL_OK)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
          "Enter BPCL CriticalSection ", 0, 0, 0, 0);
      bStatus = TRUE;
   }
   else
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
          "Enter BPCL CriticalSection failed!!! ", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }

   return bStatus;
}

/*****************************************************************************
 *
 * FUNCTION:
 *    FD_Crypt_vLeaveBPCLCriticalSec
 *
 * DESCRIPTION
 *    Semaphore wait for Non-reentrant BPCL functions
 *
 * PARAMETERS:
 *    None
 * RETURNVALUE:
 *    None
 * HISTORY:
 *    DATE      |  AUTHOR        |         MODIFICATION
 *  05/04/11    | Anooj Gopi     |     Created 1st version
************************************************************************/
tVoid FD_Crypt_vLeaveBPCLCriticalSec( tVoid )
{
   if (!prFdCryptPvtData)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                 "Err: prFdCryptPvtData is NULL", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
   else if (prFdCryptPvtData->hFD_CryptBPCLSemaphore == OSAL_C_INVALID_HANDLE)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                 "Sem Handle invalid-LeaveBPCLCritSec", (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
   if (OSAL_s32SemaphorePost( prFdCryptPvtData->hFD_CryptBPCLSemaphore ) == OSAL_OK)
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
          "Leave BPCL CriticalSection ", (tS32)0, 0, 0, 0);
   }
   else
   {
      fd_crypt_vTraceInfo(TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
          "Leave BPCL CriticalSection failed!!! ",
          (tS32)TR_CRYPT_MSG_ERROR, 0, 0, 0);
   }
}

/*****************************************************************************
 *
 * FUNCTION:
 *    pvGetCryptDevDBEntry
 *
 * DESCRIPTION:
 *    This function returns pointer to entry for the specified device name.
 *    If the entry exists for specified device name, it will be returned
 *    else free entry will be allocated and returned.
 *
 * PARAMETERS:
 *    coszDevName - Device Name
 *
 * RETURNVALUE:
 *    Pointer to entry in success (Note: pointer should not be used)
 *    OSAL_NULL on failure
 *
 * HISTORY:
 *    19/05/11    |   Anooj Gopi   |   Created 1st version
 *
 *****************************************************************************/
tPVoid pvGetCryptDevDBEntry( tCString coszDevName )
{
   trFdCryptDevDB *prEntry;
   trFdCryptDevDB *prDBEntryRet = OSAL_NULL;
   tInt iIndex;

   fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                        "Get DB entry for the device", 0, 0, 0, 0 );
   fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                        coszDevName, 0, 0, 0, 0 );

   if(coszDevName && prFdCryptPvtData)
   {
      /* Mark the device as we start the verification process */
      if ( ! FD_Crypt_vEnterCriticalSection() )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "ERR: EnterCritSec failed - GetCryptDevDBEntry", 0, 0, 0, 0 );
      }
      else
      {
         /* Start from the first entry in the database */
         prEntry = prFdCryptPvtData->arFdCryptDevDB;

         /* Check if this device name is available in database */
         for(iIndex = 0; iIndex < FD_CRYPT_MAX_DEVICES; iIndex++)
         {
            if(!OSAL_s32StringCompare(prEntry->szDevName, coszDevName))
            {
               prDBEntryRet = prEntry;
               fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                    "Valid Entry found for the device - %d", iIndex, 0, 0, 0 );
               break;
            }
            /* go to next entry */
            prEntry++;
         }

         /* If entry for this device is not available get a free entry  */
         if(!prDBEntryRet)
         {
            prEntry = prFdCryptPvtData->arFdCryptDevDB;

            fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                 "Searching for free entry in DB", 0, 0, 0, 0 );

            /* Check if free device name is available in database */
            for(iIndex = 0; iIndex < FD_CRYPT_MAX_DEVICES; iIndex++)
            {
               if(prEntry->szDevName[0] == '\0')
               {
                  /* Make sure the device name is no too lengthy for DB entry */
                  if(OSAL_u32StringLength(coszDevName) >= FD_CRYPT_DEV_NAME_MAX_LEN)
                  {
                     fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                          "Device name is too long to fit in DB", FD_CRYPT_DEV_NAME_MAX_LEN, 0, 0, 0 );
                     break;
                  }

                  /* Initialize the fields */
                  (tVoid)OSAL_szStringCopy(prEntry->szDevName, coszDevName);
                  prEntry->enGlobalVerifyState = FD_CRYPT_VERIFY_DONE;
                  prEntry->enGlobalVerifyResult = ERROR_FD_CRYPT_UNKNOWN;

                  fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                       "DB Free entry search success %d", iIndex, 0, 0, 0 );
                  prDBEntryRet = prEntry;
                  break;
               }
               /* go to next entry */
               prEntry++;
            }
         }

         FD_Crypt_vLeaveCriticalSection();
      }
   }

   if(!prDBEntryRet)
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Getting DB entry failed", 0, 0, 0, 0 );
   }

   return (tPVoid)prDBEntryRet;
}

/*****************************************************************************
 *
 * FUNCTION:
 *    bSetCryptDevStateProgress
 *
 * DESCRIPTION:
 *    This function marks the entry state to progress
 *
 * PARAMETERS:
 *    prDBEntry (I) - Pointer to database entry
 *
 * RETURNVALUE:
 *    TRUE - On success
 *    FALSE - On failure
 *
 * HISTORY:
 *    19/05/11    |   Anooj Gopi   |   Created 1st version
 *
 *****************************************************************************/
tBool bSetCryptDevStateProgress( tPVoid pvDBEntry )
{
   tBool bResult = FALSE;
   trFdCryptDevDB *prDBEntry = (trFdCryptDevDB *) pvDBEntry;

   /* Mark the device as we start the verification process */
   if ( FD_Crypt_vEnterCriticalSection() )
   {
      prDBEntry->enGlobalVerifyState = FD_CRYPT_VERIFY_IN_PROGRESS;
      bResult = TRUE;

      FD_Crypt_vLeaveCriticalSection();
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: EnterCritSec DevStateProgress", 0, 0, 0, 0 );
   }

   return bResult;
}

/*****************************************************************************
 *
 * FUNCTION:
 *    bSetCryptDevStatus
 *
 * DESCRIPTION:
 *    This function marks the entry state to verification done state and
 *    updates the entry with verification result provided as argument.
 *
 * PARAMETERS:
 *    prDBEntry (I) - Pointer to database entry
 *    enVerifyResult (I) - set the verification state
 *
 * RETURNVALUE:
 *    TRUE - On success
 *    FALSE - On failure
 *
 * HISTORY:
 *    19/05/11    |   Anooj Gopi   |   Created 1st version
 *
 *****************************************************************************/
tBool bSetCryptDevStatus( tPVoid pvDBEntry, fd_crypt_tenError enVerifyResult )
{
   tBool bResult = FALSE;
   trFdCryptDevDB *prDBEntry = (trFdCryptDevDB *) pvDBEntry;

   /* Get the lock before we update the verification result in the data base */
   if ( FD_Crypt_vEnterCriticalSection() )
   {
      /* Mark that verification process has finished for this device */
      prDBEntry->enGlobalVerifyState = FD_CRYPT_VERIFY_DONE;

      /* update the database entry structure based on the verification result */
      if( FD_CRYPT_SUCCESS == enVerifyResult )
         prDBEntry->enGlobalVerifyResult = FD_CRYPT_SUCCESS;
      else
         prDBEntry->enGlobalVerifyResult = ERROR_FD_CRYPT_SIGNVERI_FAILED;

      bResult = TRUE;
      FD_Crypt_vLeaveCriticalSection();
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: EnterCritSec failed in SetCryptDevState", (tS32)ERROR_FD_CRYPT_UNKNOWN, 0, 0, 0 );
   }

   return bResult;
}

/*****************************************************************************
 *
 * FUNCTION:
 *    bGetCryptDevStatus
 *
 * DESCRIPTION:
 *    This function returns the verification result.
 *
 * PARAMETERS:
 *    prDBEntry (I) - Pointer to database entry
 *    penVerifyStatus (O) - Verify status
 *
 * RETURNVALUE:
 *    TRUE - On success
 *    FALSE - On failure
 *
 * HISTORY:
 *    19/05/11    |   Anooj Gopi   |   Created 1st version
 *
 *****************************************************************************/
tBool bGetCryptDevStatus( tPVoid pvDBEntry, OSAL_tenSignVerifyStatus *penVerifyStatus)
{
   tBool bResult = FALSE;
   trFdCryptDevDB *prDBEntry = (trFdCryptDevDB *) pvDBEntry;

   /* Get the lock before we update the verification result in the data base */
   if ( FD_Crypt_vEnterCriticalSection() )
   {
      /* Mark that verification process has finished for this device */
      if(prDBEntry->enGlobalVerifyState == FD_CRYPT_VERIFY_IN_PROGRESS)
      {
         *penVerifyStatus = OSAL_EN_SIGN_VERIFY_INPROGRESS;
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
         "Sig verify in progress", 0, 0, 0, 0);
      }
      else if(prDBEntry->enGlobalVerifyResult == FD_CRYPT_SUCCESS)
      {
         *penVerifyStatus = OSAL_EN_SIGN_VERIFY_PASSED;
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
         "Sig verify in Passed", 0, 0, 0, 0);
      }
      else if(prDBEntry->enGlobalVerifyResult == ERROR_FD_CRYPT_SIGNVERI_FAILED)
      {
         *penVerifyStatus = OSAL_EN_SIGN_VERIFY_FAILED;
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
         "Sig verify in failed", 0, 0, 0, 0);
      }
      else
      {
         *penVerifyStatus = OSAL_EN_SIGN_STATUS_UNKNOWN;
         fd_crypt_vTraceInfo(TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
         "Sig verify status unknown", 0, 0, 0, 0);
      }

      bResult = TRUE;
      FD_Crypt_vLeaveCriticalSection();
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_1, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: EnterCritSec failed in bGetCryptDevStatus", (tS32)ERROR_FD_CRYPT_UNKNOWN, 0, 0, 0 );
   }

   return bResult;
}
