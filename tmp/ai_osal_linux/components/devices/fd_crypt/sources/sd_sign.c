#ifdef FD_CRYPT_NON_XML_SUPPORT//Obsolete Code
/*-----------------------------------------------------------------------------

  SD CARD SIGNATURE HANDLING

  Remember to set C_PLATFORM_* and C_BIG_ENDIAN in bpcl.h so that byte order
  and data types match your platform!
  24/10/08  | Ravindran| Ported to ADIT platform 
  23/02/09  | Ravindran| Root directory changed to cryptnav
  3/03/09   | Ravindran| VW/Nissan tooling used for signature generation/verfication
  8/06/09   | Ravindran| Warnings Removal
  17/06/09  | Ravindran| Warnings Removal
  24/06/09  | Ravindran| Use OSAL interface instead of LFS IO Interface

 *---------------------------------------------------------------------------*/
/* OSAL Interface */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "fd_crypt_private.h"

//#include "../bpcl/bpcl.h"
//#include "../bpcl/bpcl_int.h"
//#include "prm.h"
//#include "dispatcher.h"


#define EXT_N_BYTES				(128)


/*-----------------------------------------------------------------------------
 * Create an SD card certificate
 *
 * Input:
 *		tU8 * pCID			SD card's CID register content
 *		tU8 * pPrivKey		Private key of sd card signing application
 *
 * Output:
 *		tU8 * pSigBuffer	signature for the sd card (to be written to the card)
 *
 * Return:
 *		BPCL_OK				on success (= 0)
 *		BPCL_ERR_ECC_*		on failure (<> 0)
 *---------------------------------------------------------------------------*/

int sd_sign(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPrivKey,	// 20-bytes signer's private key
	tU8* pSigBuffer			// buffer to hold signature (>= 48bytes)
) {

	tsSHA1Context	sha_ctx;
	tU32			aHashValue[5];
	tU8				int_seed[36];
	int				i;

	// Hash CID register content
	BPCL_SHA1_Init(&sha_ctx);
	BPCL_SHA1_Update(&sha_ctx, (tU8*)pCID, CID_LEN);
	BPCL_SHA1_Finish(&sha_ctx, aHashValue);

	for(i = 0; i < 36; ++i) {
		int_seed[i] = (tU8)rnd_word(0xFF) ^ 0x9E;
	}

	return BPCL_EC_DSA_Create((tU8 *)&(aHashValue[0]), (tU8 *)pPrivKey,
								int_seed, 36, pSigBuffer);
}

/*-----------------------------------------------------------------------------
 * Verify SD card signature and - on success - derive encryption key
 * from SD card
 *
 * Input:
 *		tU8 * pCID			SD card's CID register content
 *		tU8 * pPubKey		Public key of sd card signing application (should
 *							reside in KDS)
 *		tU8 * pSig			signature read from SD card (48 bytes)
 *
 * Output:
 *		tU8 * pKeyBuffer	buffer to hold encryption key (>= 16 bytes)
 *
 * Return:
 *		0					on success (means the key in pKeyBuffer is VALID)
 *		1					on failure (means the key in pKeyBuffer must NOT
 *							be used)
 *---------------------------------------------------------------------------*/
int sd_get_cid_key(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPubKey,		// 40-bytes signer's public key
	tU8* pSig,				// signature read from SD card (48 bytes)
	tU8* pKeyBuffer			// buffer to hold encryption key (>= 16 bytes)
) {

	tsSHA1Context	sha_ctx;
	tU32			aHashValue[5];
	tU8	*			pHashPtr;
	tErrCode		err;
	int				i;

	// Hash CID register content
	BPCL_SHA1_Init(&sha_ctx);
	BPCL_SHA1_Update(&sha_ctx, (tU8*)pCID, CID_LEN);
	BPCL_SHA1_Finish(&sha_ctx, aHashValue);

	err = BPCL_EC_DSA_Verify((tU8 *)&(aHashValue[0]), (tU8*)pPubKey, pSig);
	if(err == BPCL_OK) {
		pHashPtr = (tU8 *)&aHashValue[0];
		for(i=0; i<15; ++i) {
			pKeyBuffer[i] = (pHashPtr[0] ^ pHashPtr[i+4]);
		}
		return 0;
	} else {
		return 1;
	}
}

/*-----------------------------------------------------------------------------
 * Create an SD card certificate using Crypt extension algorithm 
 *
 * Input:
 *		tU8 * pCID			SD card's CID register content
 *		tU8 * pPrivKey		Private key of sd card signing application
 *
 * Output:
 *		tU8 * pSigBuffer	signature for the sd card (to be written to the card)
 *
 * Return:
 *		BPCL_OK				on success (= 0)
 *		BPCL_ERR_ECC_*		on failure (<> 0)
   Version History:
   24/06/09  | Ravindran| Use OSAL interface instead of LFS IO Interface
 *---------------------------------------------------------------------------*/
int cryptext_sd_sign(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPrivKey,	// 20-bytes signer's private key
	tU8* pSigBuffer			// buffer to hold signature (>= 48bytes)
) 
{
	tsSHA1Context sha_ctx;
	tU32 aHashValue[5];
	tU8	au8Hash[20];
	tU8	int_seed[36];
	int	i;
	tU8 au8NavrootDat[EXT_N_BYTES] = {0};
	tS32 s32RetValRd = 0;
	tS32 s32RetValCls = 0;
	OSAL_tIODescriptor hNavRoot = 0;
    	
	/* Creating SF for Ford MFD */
			
	hNavRoot = OSAL_IOOpen( NAV_ROOT_PATH, OSAL_EN_READONLY );
	if(hNavRoot != OSAL_ERROR)
	{
    			
		s32RetValRd = OSAL_s32IORead(hNavRoot, (tPS8)au8NavrootDat, EXT_N_BYTES);
		s32RetValCls = OSAL_s32IOClose(hNavRoot);
		if((s32RetValRd == EXT_N_BYTES)&&(s32RetValCls != OSAL_ERROR))
		{
			// Hash CID register content
			BPCL_SHA1_Init(&sha_ctx);
			BPCL_SHA1_Update(&sha_ctx, (tU8*)pCID, CID_LEN);
			BPCL_SHA1_Update(&sha_ctx, au8NavrootDat, EXT_N_BYTES);
			BPCL_SHA1_Finish(&sha_ctx, aHashValue);

			for(i = 0; i < 36; ++i) 
			{
				int_seed[i] = (tU8)rnd_word(0xFF) ^ 0x9E;
			}

			// Convert result to platform independence and sign
			for(i = 0; i < 5; ++i) 
			{
				M_U32_TO_BUF(aHashValue[i], au8Hash, (4*i));
			}

			return BPCL_EC_DSA_Create(au8Hash, (tU8 *)pPrivKey,
										int_seed, 36, pSigBuffer);
		}
	}

	return 1;
}

/*-----------------------------------------------------------------------------
 * Verify SD card signature using crypt extension algorithm 
 *
 * Input:
 *		tU8 * pCID			SD card's CID register content
 *		tU8 * pPubKey		Public key of sd card signing application (should
 *							reside in KDS)
 *		tU8 * pSig			signature read from SD card (48 bytes)
 *
 * Output:
 *
 * Return:
 *		0					on success 
 *		1					on failure 
 * Version History:
   24/06/09  | Ravindran| Use OSAL interface instead of LFS IO Interface

 *---------------------------------------------------------------------------*/
int cryptext_sd_verify(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPubKey,		// 40-bytes signer's public key
	tU8* pSig,				// signature read from SD card (48 bytes)
	tCString coszNavRootPath // Navigation NAV_ROOT.DAT file path
) 
{

	tsSHA1Context	sha_ctx;
	tU32			aHashValue[5];
	tU8 			au8Hash[20];
	tErrCode		err;
	int				i;
	tU8 au8NavrootDat[EXT_N_BYTES] = {0};
	tS32 s32RetValRd = 0;
	tS32 s32RetValCls = 0;
	OSAL_tIODescriptor hNavRoot = 0;
	

   	/* Verifying Signature File for Ford */
   		
   			
   	hNavRoot = OSAL_IOOpen( coszNavRootPath, OSAL_EN_READONLY );
	if(hNavRoot != OSAL_ERROR)
	{
    		
		s32RetValRd = OSAL_s32IORead(hNavRoot, (tPS8)au8NavrootDat, EXT_N_BYTES);
		s32RetValCls = OSAL_s32IOClose(hNavRoot);
		if((s32RetValRd == EXT_N_BYTES)&&(s32RetValCls != OSAL_ERROR))
		{
			// Hash CID register content
			BPCL_SHA1_Init(&sha_ctx);
			BPCL_SHA1_Update(&sha_ctx, (tU8*)pCID, CID_LEN);
			BPCL_SHA1_Update(&sha_ctx, au8NavrootDat, EXT_N_BYTES);
			BPCL_SHA1_Finish(&sha_ctx, aHashValue);

			// Convert result to platform independence and sign
			for(i = 0; i < 5; ++i) 
			{
				M_U32_TO_BUF(aHashValue[i], au8Hash, (4*i));
			}

			err = BPCL_EC_DSA_Verify(au8Hash, (tU8*)pPubKey, pSig);
			
			if(err == BPCL_OK) 
			{
				
				return 0;	//Success
			} 
			else 
			{
				return 1;
			}
		}	  
	}
	return 1;
}
#endif
