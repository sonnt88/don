/*!\file
 * @brief Main library implementation file.
 *
 * @note For the description of these functions, see libminxml.h file.
 *
 * @author Arvind Devarajan (RBEI/ECF)
 * @date   19.Mar.2010
 * @version 1.1 Jeryn Mathew  25.May.2010  Corrections to remove an exception throwing case.
 * @version 1.2 Anooj Gopi    05.Apr.2011  Edited to add Multi crypt device support
 */
#include <stdio.h>
#include <string.h>

#include "libminxml_private.h"
#include "libminxml.h"

// History
// Anooj Gopi  05-Apr-2011 Global data is allocated dynamically to
//                           make the function reentrant
tVoid *libminxml_open( const tChar *pfile )
{
   global_t *pgbl;
   tU32 status;

   /* Allocate memory for global data structure */
   pgbl = OSAL_pvMemoryAllocate( sizeof(global_t) );
   if(pgbl)
   {
      libminxml_priv_init(pgbl);

      pgbl->pfile = libminxml_plat_fopen(pfile,"r");
   
      if (!pgbl->pfile)
      {
    	 /* Platform open failed */
	     OSAL_vMemoryFree(pgbl);
         pgbl = OSAL_NULL;
      }
      else
      {
    	 /* Platform open was success. Now lets parse the file */
         status = libminxml_priv_parse(pgbl);

         // Close the file as we no longer need it
         libminxml_plat_fclose(pgbl->pfile);

         if(status != 0 )
         {
        	 //Failed to parse
             // Cleanup the resources we allocated
        	 OSAL_vMemoryFree(pgbl);
             pgbl = OSAL_NULL;
         }
      }
   }
   
   return (tVoid *)pgbl;
}

tVoid libminxml_close(tVoid* phandle)
{
   global_t *pgbl = (global_t *)phandle;

   if (pgbl)
   {
      libminxml_priv_cleanup(pgbl);
      /* Free the memory allocated for global data structure */
      OSAL_vMemoryFree(pgbl);
   }
}

tChar *libminxml_getcontent(const tVoid *phandle, const tChar *ppath, tVoid **pplasts)
{
   element_t *pelement = NULL;
   element_t **ppelement=(element_t **)pplasts;

   if (!phandle || !pplasts)
   {
      if (pplasts) *pplasts=NULL;
      return NULL;
   }

   if (ppath)
   {
      // We have a path here. Hence, search for a node in this path
      // and return the value of that node
      pelement = libminxml_priv_find(phandle, ppath );
      if (pelement)
      {
         *pplasts=(tVoid *)pelement;
         return pelement->pcontent;
      }
   }
   else
   {
      // We do not have the path. We assume that we the caller wants the
      // next sibling of the node pointed to by the pplasts.

      // We make additional validation that the next sibling has the same
      // tag as the pplasts node.
      pelement=*ppelement;

      if (pelement)
      {
         pelement=pelement->psibling;

         if (pelement && !strcmp(pelement->pname, (*ppelement)->pname))
         {
            *pplasts=(tVoid *)pelement;
            return pelement->pcontent;
         }
      }
   }

   *pplasts=NULL;
    return NULL;
}

// History
// Jeryn Mathew  21-may-10  Resolved a potential error case that could 
//                          throw exception
rset_t *libminxml_get_rset(const tVoid *phandle, const tChar *ppath)
{
   rset_t *presult = NULL;

   if (ppath && phandle)
   {
      if( ( presult = (rset_t*)malloc( sizeof( rset_t ) ) ) != NULL )
      {
         //Capture the element
         presult->pelement = libminxml_priv_find (phandle, ppath);
         if( presult->pelement != NULL )
         {
            presult->pattr = presult->pelement->pattr;
         }
         else
         {
            //Error in finding element.
            free( presult );
            presult = NULL;
         }
      }
      else
      {
         //Error in malloc
         //return NULL
         //Here presult is still NULL, so additional code required.
      }
   }

   return presult;
}

tVoid libminxml_free_rset( rset_t *presult )
{
   free( presult );
}

tBool libminxml_validate_result_set ( const rset_t *presult)
{
   tBool bResult = TRUE;

   if( !presult->pelement )
   {
      bResult = FALSE;
   }

   return bResult;
}

tChar* libminxml_get_element_name(const rset_t* presult)
{
   if (libminxml_validate_result_set(presult))
      return presult->pelement->pname;
   else
      return NULL;
}

tChar* libminxml_get_content(const rset_t* presult)
{
   if (libminxml_validate_result_set(presult))
      return presult->pelement->pcontent;
   else
      return NULL;
}

tChar* libminxml_get_attr_name(const rset_t* presult)
{
   if (libminxml_validate_result_set(presult))
      return presult->pattr->pattr;
   else
      return NULL;
}

tChar* libminxml_get_attr_value(const rset_t* presult)
{
   if (libminxml_validate_result_set(presult))
      return presult->pattr->pattrval;
   else
      return NULL;
}

tVoid libminxml_get_next_element(rset_t* presult)
{
   if (libminxml_validate_result_set(presult))
   {
      if (presult->pelement->psibling)
         presult->pelement = presult->pelement->psibling;
      presult->pattr = presult->pelement->pattr;
   }
}

tVoid libminxml_get_next_level_element(rset_t* presult)
{
   if (libminxml_validate_result_set(presult))
   {
      if (presult->pelement->pchild)
         presult->pelement = presult->pelement->pchild;
      presult->pattr = presult->pelement->pattr;
   }
}

tVoid libminxml_get_prev_level_element(rset_t* presult)
{
   if (libminxml_validate_result_set(presult))
   {
      presult->pelement = presult->pelement->pparent;
      presult->pattr = presult->pelement->pattr;
   }
}

tChar* libminxml_get_next_content(rset_t* presult)
{
   libminxml_get_next_element(presult);

   return presult->pelement->pcontent;
}

tChar* libminxml_get_next_element_name(rset_t* presult)
{
   if (libminxml_validate_result_set(presult))
   {
      libminxml_get_next_element(presult);

      return presult->pelement->pname;
   }
   else
      return NULL;
}

tBool libminxml_get_next_attr(rset_t* presult, tChar* pattr_name, tChar* pattr_val)
{
   tBool bResult = FALSE;
   if (libminxml_validate_result_set(presult))
   {
      if (libminxml_get_next_attr_res(presult))
      {
         if( presult->pattr->pattr )
            strcpy( pattr_name, presult->pattr->pattr );
         if( presult->pattr->pattrval )
            strcpy( pattr_val, presult->pattr->pattrval );
         bResult = TRUE;
      }      
   }
   
   return bResult;
}

tBool libminxml_reset_attr_list(rset_t* presult)
{
   tBool bResult = FALSE;
   if (presult)
   {
      if( presult->pelement )
      {
         presult->pattr = presult->pelement->pattr;
         bResult = TRUE;
      }
   }
   
   return bResult;
}
