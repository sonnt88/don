/*!\file
 * @brief Functions that are not publicly exposed
 *
 * @author Arvind Devarajan (RBEI/ECF)
 * @date   19.Mar.2010
 * @history 7.Jun.2010  Jeryn Mathew (RBEI/ECF1)  CRQ update to handle possible 
 *                                                exception causing bug.
 * @history 05.Apr.2011 Anooj Gopi (RBEI/ECF1)    Done fixes for function reentrancy
 */

//#define _POSIX_C_SOURCE // Make sure we have all the POSIX symbols available
                        // from the header files that are included here
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <stdarg.h>
#include <ctype.h>

#include "libminxml.h"
#include "libminxml_private.h"

#define OPTIMISED_FIND 0 // This enables or disables the optimised search func.
                         // Currently under development.

/*!@brief Allowed NameStartChars
 * @note FIXME: libminxml limitation: We allow only till FF. Above 0xFF, though
 *       valid XML, is not allowed by libminxml
 * @note If the "end" is 0, then, we have no range. Just "start" is valid.
 */
const range startcharset[]=
{
         {'A', 'Z'},
         {'a', 'z'},
         {0xC0, 0xD6},
         {0xD8, 0xF6},
         {0xF8, 0xFF},
         {':', 0},
         {'_', 0}
};

/*!@brief Allowed NameChars (apart from the NameStartChars)
 * @note FIXME: libminxml limitation. Does not contain characters that need more
 *       than 2 bytes though they are valid XML character because libminxml
 *       does not support them.
 * @note If the "end" is 0, then, we have no range. Just "start" is valid.
 */
const range charset[]=
{
         {'-', 0},
         {'.', 0},
         {'0', '9'},
         {0xB7, 0}
};

/*!@brief Allowed white-space characters
 */
const range wsset[]=
{
   {0x20, 0},
   {0x09, 0},
   {0x0D, 0},
   {0x0A, 0}
};

/*!\fn static tChar libminxml_priv_nextchar(global_t *pgbl)
 * @brief Gets the next character; increments pgbl->curline if it gets a newline
 *
 * @param pgbl Global handle
 * @return Next character
 */
static tChar libminxml_priv_nextchar(global_t *pgbl)
{
   tChar c = libminxml_plat_fgetc(pgbl->pfile);

   if (c=='\r')
   {
      c = libminxml_plat_fgetc(pgbl->pfile); // This must be a '\n'
   }

   if (c=='\n')
   {
      pgbl->col=0;
      pgbl->curline++;
   }
   else
   {
      pgbl->col++;
   }

   return c;
}

/*!\fn static tVoid libminxml_priv_dlog(global_t *pgbl, tChar *pformat, ...)
 * @brief Logs an error message on console/file
 *
 * This function checks pgbl->curline: if this is greater than 0, it also
 * prints (additionally) the line of the XML file where the error has occured,
 * along with the column number of the character (pgbl->col).
 *
 * @param pgbl [in] Global handle
 * @param pformat [in] Format string for printf
 * @param <varargs> Arguments for "printf"
 *
 */
static tVoid libminxml_priv_dlog(const global_t *pgbl, const tChar *pformat, ...)
{
   tChar errstr[LIBMINXML_SZ_ERRSTR];
   va_list ap = {0}; // Initialized to avoid lint warning

   va_start(ap, pformat);
   
   vsprintf(errstr, pformat, ap);

   if (pgbl->curline)
   {
         sprintf(errstr, "[ERROR %lu:%lu] %s", pgbl->curline, pgbl->col, errstr);
   }
   else
   {
         sprintf(errstr, "[ERROR] %s", errstr);
   }

      va_end(ap);

      libminxml_plat_fprintf(pgbl->plog, errstr);
 }


/*!\fn static tBool libminxml_priv_isequal(tChar c1, tChar c2)
 * @brief Does a case-insensitive comparison of characters
 *
 * @param c1 [in] Character 1 to be compared
 * @param c2 [in] Character 2 to be compared
 *
 * @return TRUE if c1==c2 (case-insensitive)
 * @return FALSE if c1!=c2 (case-insensitive)
 */
static tBool libminxml_priv_isequal(tChar c1, tChar c2)
{
   if (tolower(c1) == tolower(c2))
      return TRUE;
   else
      return FALSE;
}


/*!\fn static tBool libminxml_priv_isinrange(tChar c, ctype ct)
 * @brief Checks if the character 'c' is in valid charset range
 *
 * @param c [in] Character
 * @param type [in] Character type (\see ctype)
 *
 * @return TRUE if in range
 * @return FALSE if not in range
 */
static tBool libminxml_priv_isinrange(tChar c, ctype type)
{
   const range *prange=NULL;
   tU32 i=0, len=0;

   switch(type)
   {
      case NameStartChar:
         prange=startcharset;
         len=sizeof(startcharset)/sizeof(range);
         break;

      case NameChar:
         prange=charset;
         len=sizeof(charset)/sizeof(range);
         break;

      case Whitespace:
         prange=wsset;
         len=sizeof(wsset)/sizeof(range);
         break;

      default: // Can never occur
         break;
   }

   if( prange )
   {
      for (i=0; i<len; i++)
      {
         if (prange[i].end)
         {
            if (c >= prange[i].start && c <= prange[i].end)
            {
               return TRUE;
            }
            // else, continue to search
         }
         else
         {
            if (c == prange[i].start)
            {
               return TRUE;
            }
            // else, no luck. Not in range.
         }
      }
   }

   return FALSE;
}

/*!\fn static tBool libminxml_priv_isnamechar(tChar c)
 * @brief Checks if the character is a valid element name character (XML 1.0 edition 5)
 *
 * @param c [in] Character
 *
 * @return TRUE if valid char
 * @return FALSE if no
 */
static tBool libminxml_priv_isnamechar(tChar c)
{
   return (libminxml_priv_isinrange(c, NameStartChar) || libminxml_priv_isinrange(c, NameChar));
}

/*!\fn static tBool libminxml_priv_isnamestartchar(tChar c)
 * @brief Checks if the character is a valid element name start character (XML 1.0 edition 5)
 *
 * @param c [in] Character
 *
 * @return TRUE if valid char
 * @return FALSE if no
 */
static tBool libminxml_priv_isnamestartchar(tChar c)
{
   return libminxml_priv_isinrange(c, NameStartChar);
}


/*!\fn static tBool libminxml_priv_isws(tChar c)
 * @brief Checks if 'c' is a valid XML white space (XML 1.0 edition 5)
 *
 * @param c
 *
 * @return TRUE if valid
 * @return FALSE if not
 */
static tBool libminxml_priv_isws(tChar c)
{
   return libminxml_priv_isinrange(c, Whitespace);
}

/*!\fn static parser_state libminxml_priv_decl_or_newelement(global_t *pgbl)
 * @brief Gets next state ("DECL" state or "NEWELEMENT" state)
 *
 * This function checks if the elements have a '?xml in the begining
 * of the first '<'. If we have, then, we have an XML declaration. If
 * not, we have the first element of XML.
 *
 * @param pgbl [in] Global handle
 *
 * @return Next state
 */
static parser_state libminxml_priv_decl_or_newelement(global_t *pgbl)
{
   tChar tc;
   parser_state next_state=LIBMINXML_STATE_INVALID;

   // Check if we have an XML declaration
   tc = libminxml_priv_nextchar(pgbl);

   if (tc == '?')
   {
      // We have a declaration

      // Every XML declaration should start with <?xml.....?>
      if (libminxml_priv_isequal(libminxml_priv_nextchar(pgbl), 'x') &&
          libminxml_priv_isequal(libminxml_priv_nextchar(pgbl), 'm') &&
          libminxml_priv_isequal(libminxml_priv_nextchar(pgbl), 'l'))
      {
         // We've got "<?xml" already
         // Read the rest of the chars until we get a '?'
         // We take care to end the loop assuming we'll atleast have
         // a '>' if not a '?'
         //Lint intentionally suppressed. The use of single while statement is necessary.
         while (((tc=libminxml_priv_nextchar(pgbl)) != '?') && (tc != '>') && (tc != (tChar)EOF));//lint !e722

         if (tc=='?')
         {
            // '?' should be followed by a '>'
            if (libminxml_priv_nextchar(pgbl) == '>')
            {
               next_state = LIBMINXML_STATE_DECL;
            }
            // else, we have an invalid state
         }
         // else, we have an invalid state
      }
      // else, we have an invalid state
   }
   else
   {
      // Put back tc to the stream so that it is read next
      libminxml_plat_ungetc(tc, pgbl->pfile);

      next_state = LIBMINXML_STATE_NEWELEMENT;
   }

   return next_state;
}

/*!\fn static parser_state libminxml_priv_consume_comment(global_t *pgbl)
 * @brief Consume the XML comment
 *
 * @param pgbl [in] Global handle
 * @return COMMENT if comment was "well-formed"; INVALID if not
 */
static parser_state libminxml_priv_consume_comment(global_t *pgbl)
{
   tChar tc;
   tBool bFlag = TRUE;
   parser_state next_state=LIBMINXML_STATE_COMMENT;

   while(bFlag)
   {
      // Read the comment until we get a '-'
      //Lint purposefully suppressed. Single line while loop is intentional.
      while ((tc=libminxml_priv_nextchar(pgbl)) != '-' && tc != (tChar)EOF);//lint !e722

      if (tc == '-')
      {
         // The next character must be a '-' and the one after that
         // must be a '>' to break from this loop
         if (libminxml_priv_nextchar(pgbl)=='-' && libminxml_priv_nextchar(pgbl)=='>')
         {
            // Comment ended
            bFlag = FALSE;
         }
         // else, continue
      }
      else
      {
         next_state = LIBMINXML_STATE_INVALID;
         bFlag = FALSE;
      }
   }

   return next_state;
}

/*!\fn static parser_state libminxml_priv_get_next_state(global_t *pgbl,
 *        tChar c,
 *        parser_state curstate,
 *        tU32 *pcurdepth
 *      )
 * @brief Returns the next state of the parser based on the current depth.
 *
 * This function also changes the current depth of the parser in the XML
 * tree.
 *
 * @return Next state of the parser
 */
static parser_state libminxml_priv_get_next_state(global_t *pgbl,
         tChar c,         //!< [in] Character that triggers the state change
         parser_state curstate,   //!< [in] Current state of the parser
         tU32 *pcurdepth //!< [inout] In: Current depth; Out: Next curdepth
)
{
   tChar tc;
   parser_state next_state=curstate; // We start with the next_state as
                                     // current state assuming we'll
                                     // have no transition

   // Check if we have an XML comment. This can occur anywhere
   // in the code
   if (c == '<')
   {
      if ((tc=libminxml_priv_nextchar(pgbl)) == '!')
      {
         if ((libminxml_priv_nextchar(pgbl)=='-') && (libminxml_priv_nextchar(pgbl)=='-'))
         {
            // we have a comment
            return LIBMINXML_STATE_COMMENT;
         }
         else
         {
            // We probably have some DOCTYPE or ELEMENT or some
            // other (valid) XML construct here. However, since
            // "libminxml" does not handle this, we'll transition
            // to the invalid state and come out.
            next_state = LIBMINXML_STATE_INVALID;
         }
      }
      else
      {
         // "libminxml_plat_ungetc" this character back as we'll need to read it again
         libminxml_plat_ungetc(tc, pgbl->pfile);
      }
   }

   if (curstate == LIBMINXML_STATE_INIT && (c == '<'))
   {
      // Transition to DECL state or Newelment state
      next_state = libminxml_priv_decl_or_newelement(pgbl);
   }

   else if (curstate == LIBMINXML_STATE_DECL)
   {
      if (c == '<')
      {
         next_state = LIBMINXML_STATE_NEWELEMENT;
      }
      // else, no transition
   }

   else if (curstate == LIBMINXML_STATE_NEWELEMENT &&
            libminxml_priv_isnamestartchar(c))
   {
      next_state = LIBMINXML_STATE_ELENAME;
   }

   else if (curstate == LIBMINXML_STATE_ELENAME)
   {
      if (c == '>')
      {
         next_state = LIBMINXML_STATE_ENDNEWELEMENT;
      }
      else if (libminxml_priv_isws(c))
      {
         next_state = LIBMINXML_STATE_TOKSPC;
      }
      else if (!libminxml_priv_isnamechar(c))
      {
         next_state = LIBMINXML_STATE_INVALID;
      }
      // else, no transition. This means, we remain at ELENAME until
      // we get a '>' or a ' ' or some invalid character
   }

   else if (curstate == LIBMINXML_STATE_TOKSPC)
   {
      // After TOKSPC, we'll allow only a NameStartChar (XML 1.0 edition 5)
      if (libminxml_priv_isnamestartchar(c))
      {
         next_state = LIBMINXML_STATE_ATTNAME;
      }
	  //After TOKSPC if there is a /> we have to go to ENDEND state
	  //eg:<HASH_FILE offset="47" numberofbytes="11" filename="filename1" />  
	  else if( c == '/')
	  {
	     c = libminxml_priv_nextchar(pgbl);
		 if( c == '>')
		 {
		    next_state = LIBMINXML_STATE_ENDEND;	   
		 }
		 else
		 {
		    next_state = LIBMINXML_STATE_INVALID;
         }
	     
	  }
      else
      {
         next_state = LIBMINXML_STATE_INVALID;
      }
   }

   else if (curstate == LIBMINXML_STATE_ATTNAME)
   {
      if (c == '=')
      {
         next_state = LIBMINXML_STATE_TOKEQ;
      }
      // Attribute's name must be a valid XML 1.0 edition 5 NameChar
      else if (!libminxml_priv_isnamechar(c))
      {
         next_state = LIBMINXML_STATE_INVALID;
      }
      // else, no transition. This means, we remain in ATTNAME until we get an '='
   }

   else if (curstate == LIBMINXML_STATE_TOKEQ)
   {
      // We'll allow only a " or a ' after '=' (XML 1.0 Edition 5)
      if (c=='\"' || c=='\'')
      {
         next_state = LIBMINXML_STATE_ATTVAL;
      }
      else
      {
         next_state = LIBMINXML_STATE_INVALID;
      }
   }

   else if (curstate == LIBMINXML_STATE_ATTVAL)
   {
      if (c == '\'' || c == '\"')
      {
         c = libminxml_priv_nextchar(pgbl);

         if (libminxml_priv_isws(c))
         {
            next_state = LIBMINXML_STATE_TOKSPC;
         }
         
         else if (c == '>')
         {
            next_state = LIBMINXML_STATE_ENDNEWELEMENT;
         }
         
         else
         {
            next_state = LIBMINXML_STATE_INVALID;
         }
      }
      // else, no transition.
      // FIXME: libminxml limitation - no validation of attribute's value
   }

   else if (curstate == LIBMINXML_STATE_ENDNEWELEMENT)
   {
      if (c == '<')
      {
         // We are not sure if this is the "beginning of the end" of a element
         // (which can happen if the element has no value); or a
         // new element in the next depth
         c = libminxml_priv_nextchar(pgbl);

         if (c == '/')
         {
            next_state = LIBMINXML_STATE_ENDBEG;
         }
         else
         {
            // Got a new element in the next depth
            next_state = LIBMINXML_STATE_NEWELEMENT;
            (*pcurdepth)++;

            // "libminxml_plat_ungetc" this extra character back so that it is
            // read again the next time.
            libminxml_plat_ungetc(c, pgbl->pfile);
         }
      }

      else if (!isspace(c)) // FIXME: libminxml limitation (consume first space)
      {
         next_state = LIBMINXML_STATE_CONTENT;
      }
      // else, no transition
   }

   else if (curstate == LIBMINXML_STATE_CONTENT)
   {
      if (c == '<')
      {
         // We allow only '/' as the next character.
         c = libminxml_priv_nextchar(pgbl);

         if (c=='/')
         {
            next_state = LIBMINXML_STATE_ENDBEG;
         }
         else
         {
            next_state = LIBMINXML_STATE_INVALID;
         }
      }
      // else, no transition. This means, any character other than '<',
      // we remain at TAGVAL
   }

   else if (curstate == LIBMINXML_STATE_ENDBEG)
   {
      if (c == '>')
      {
         next_state = LIBMINXML_STATE_ENDEND;
      }
      // else, no transition.
   }

   else if (curstate == LIBMINXML_STATE_ENDEND)
   {
      if (c == '<')
      {
         // We are not sure if this is the beginning of a new element
         // or end of a element of previous depth. We'll read another
         // character to know this.
         c = libminxml_priv_nextchar(pgbl);

         if (c == '/')
         {
            next_state = LIBMINXML_STATE_ENDBEG;
            (*pcurdepth)--;
         }
         else
         {
            next_state = LIBMINXML_STATE_NEWELEMENT;
            // "libminxml_plat_ungetc" this extra character back so that it is
            // read again the next time.
            libminxml_plat_ungetc(c, pgbl->pfile);
         }
      }
      // else, no transition.
   }

   else
   {
      next_state = LIBMINXML_STATE_INVALID;
   }

   return next_state;
}

/*!\fn static tVoid libminxml_priv_destroy_xmlelement(global_t *pgbl, element_t *proot)
 * @brief Releases memory of the element pelement
 *
 * @param pgbl [in] Global handle
 * @param proot [in] Element whose memory is no longer required
 */
static tVoid libminxml_priv_destroy_xmlelement(global_t *pgbl, element_t *proot)
{
   /* TODO: Recursive function to destroy all XML children and siblings! */
   //element_t *pnext_sib  = NULL;
   element_t *pcurr_element = proot;
   attr_t    *pattr      = NULL;

   if( pgbl != NULL && pcurr_element != NULL )
   {



   //DEBUG: Trace statement
   libminxml_priv_dlog(pgbl, "Entering Element: %s\n", pcurr_element->pname );

   /* Step 1: First destroy the children */
   if( pcurr_element->pchild )
      libminxml_priv_destroy_xmlelement(pgbl, pcurr_element->pchild );

   /* Step 2: Then destroy its siblings*/
   if( pcurr_element->psibling )
   {
      //save address of next sibling.
      //next_sib = pcurr_element->psibling;
      libminxml_priv_destroy_xmlelement(pgbl, pcurr_element->psibling );
   }

   /* Step 3: Finally, do self-destruct */
   //free members of this element...
   //DEBUG: Trace statement
   libminxml_priv_dlog(pgbl, "Destroying Element: %s\n", pcurr_element->pname );
   free( pcurr_element->pname );
   free( pcurr_element->pcontent );

   //...and its attributes...
   pattr = pcurr_element->pattr;
   while( pattr )
   {
      free( pattr->pattr );
      free( pattr->pattrval );
      pattr = pattr->pnext;
   }

   //..and finally the element itself
   free( pcurr_element );

   }

}

#if OPTIMISED_FIND
/*!\fn static tU32 libminxml_priv_optimise_find(global_t *pgbl)
 * @brief Creates metadata after creation of the XML tree so that "find"s are optimised.
 *
 * @param pgbl [in] Global handle
 *
 * @return 0 Success
 * @return -1 Error
 */
static tU32 libminxml_priv_optimise_find(const global_t *pgbl)
{

   // TODO: WRONG Implementation! To be corrected.
   element_t *pelement=pgbl->proot;
   tU32 i=0;

   // All we have to do now is to make sure that we have the
   // "heads" updated so that we can easily traverse through this
   pgbl->pdepth_heads =
            (element_t **)malloc(pgbl->totdepth * sizeof(element_t *));

   if (!pgbl->pdepth_heads)
   {
      libminxml_priv_cleanup(pgbl);
      return -1;
   }

   while (pelement)
   {
      pgbl->pdepth_heads[i++] = pelement;

      // Get the last sibling at this depth
      while (pelement->psibling)
      {
         pelement=pelement->psibling;
      }

      pelement=pelement->pchild;
   }

   return 0;
}
#endif

/*!\fn static helement_t libminxml_priv_create_new_xmlelement(tVoid)
 * @brief Creates a new XML element, and initialises the members.
 *
 * @return The handle for the new created element if no error
 * @return NULL on error
 */
static element_t *libminxml_priv_create_new_xmlelement(tVoid)
{
   element_t *pelement = malloc(sizeof(element_t));

   if (pelement == NULL)
      return NULL;

   /* Explicitly make members NULL to keep the code portable */
   pelement->pattr=NULL;
   pelement->pchild=NULL;
   pelement->psibling=NULL;
   pelement->pname=NULL;
   pelement->pcontent=NULL;

   return pelement;
}

/*!\fn static attr_t *libminxml_priv_create_new_attr(tVoid)
 * @brief Creates a new attribute, and initialises the members.
 *
 * @return The handle for the new created attribute if no error
 * @return NULL on error
 */
static attr_t *libminxml_priv_create_new_attr(tVoid)
{
   attr_t *pattr = malloc(sizeof(attr_t));

   if (pattr == NULL)
      return NULL;

   /* Explicitly make members NULL to keep the code portable */
   pattr->pattr=NULL;
   pattr->pattrval=NULL;
   pattr->pnext=NULL;

   return pattr;
}

/*!\fn static tChar *libminxml_priv_collect_text(tChar *ptext, tChar c, tU32 *plen)
 * @brief Allocates memory for the additional character c and returns this.
 *
 * @param ptext [in] Current pointer to the tagname/attkey memory
 * @param c     [in] Character to be added
 * @param plen  [inout] Input: Current length of the memory; returns new length.
 *
 * @return Returns the new memory location with 'c' added in that. Also
 *         appends this with a '\\0'.
 */
static tChar *libminxml_priv_collect_text(tChar *ptext, tChar c, tU32 *plen)
{
   tU32 len=*plen;

   if (ptext==NULL)
   {
      ptext=malloc(2);
      if (!ptext)
      {
         return ptext;
      }
      ptext[0]=c;
      ptext[1]='\0';
      len=2;
   }
   else
   {
      ptext=(tChar *)realloc(ptext, len+1);
      if (!ptext)
      {
         return ptext;
      }
      ptext[len-1]=c;
      ptext[len]='\0';
      len++;
   }

   *plen=len;
   return ptext;
}

/*!\fn static element_t *libminxml_priv_fetch_element(global_t *pgbl, tChar *ppath)
 * @brief Finds the XML element that corresponds to the path provided in ppath
 *
 * @param ppath [in] The path of the element to be found.
 * @return XML element.
 *
  * @history 05.Apr.2011 Anooj Gopi (RBEI/ECF1) Replaced strtok with reentrant function strtok_r.
 */
static element_t *libminxml_priv_fetch_element(const global_t *pgbl, const tChar *ppath)
{
   /* TODO: Handling error cases of strtok, because of incorrect ppath entries */
   element_t *presult;
   tChar     delim[]       = "/";
   tChar     *ptok;
   tChar *pcopypath;
   tBool     element_found = 0;
   tChar *psavepos = OSAL_NULL;

   /* Validate the arguments */
   if(!ppath)
      return (element_t*)NULL;

   /*Make a copy of ppath*/
   if( ( pcopypath = (tChar*)malloc( strlen( ppath ) + 1 ) ) == NULL )
   {
      return (element_t*)NULL;
   }
   strcpy( pcopypath, ppath );

   presult = pgbl->proot;

   /*Extract the first tag name*/
   ptok = strtok_r( pcopypath, delim, &psavepos );
   while( ptok != NULL )
   {
      /* ptok now points to the element (tag) name.
         Point the result node to the node with same tag-name.
      */
      while( !element_found )
      {
         if( presult )
         {
            if( strcmp( presult->pname, ptok ) == 0 )
               element_found = 1;
            else
               presult = presult->psibling;                
         }
         else
         {
            presult = NULL;
            break;
         }
      }

      /* Go deeper down the tree.
         Point the result node to the same.
      */
      if( ( ptok = strtok_r( NULL, delim, &psavepos ) ) != NULL )
      {
         if( presult )
            presult = presult->pchild;
         element_found = 0;
      }
   }

   free( pcopypath );

   return presult;
}

/* --------- Functions used only within libminxml ----- */

/*!\fn tVoid libminxml_priv_init(global_t *pgbl)
 * @brief Initialises the parser.
 *
 * This function does nothing but initing global variables to NULL.
 * This method is preferred instead of a simple memset() to 0
 * because it is a portable code across all processor architectures.
 *
 * @param pgbl [in] Global handle
 */
tVoid libminxml_priv_init(global_t *pgbl)
{
#ifdef __CYGWIN__
   pgbl->pfile = NULL;
#else 
   pgbl->pfile = 0;
#endif
   pgbl->proot = NULL;
   pgbl->col = 0;
   pgbl->curline = 0;
   libminxml_plat_loginit(&pgbl->plog);
}

/*!\fn tVoid libminxml_priv_cleanup(global_t *pgbl)
 * @brief Cleans up the entire memory allocated for this XML file
 *
 * @param pgbl [in] Global handle
 */
tVoid libminxml_priv_cleanup(global_t *pgbl)
{
   libminxml_priv_destroy_xmlelement(pgbl, pgbl->proot);
}


/*!\fn tU32 libminxml_priv_parse(global_t *pgbl)
 * @brief Parser engine
 *
 * @param pgbl [in] Global handle
 *
 * @return 0  on Success
 * @return -1 on error
 */
tU32 libminxml_priv_parse(global_t *pgbl)
{
   tChar c; // Character read from the stream (XML file)
   element_t *ptempelement=NULL; // New element that is created
   attr_t *pattr=NULL; // Currently operating attribute
   attr_t *ptempattr=NULL; // New attribute that is created
   tChar *ptext=NULL; // Points to the current tagname/attribute key/val
   tU32 txtlen=0; // Length of the above
   parser_state prev_state; // State of the parser before the state transition
   parser_state curstate = LIBMINXML_STATE_INIT; // Initial state of the parser
   tU32 prev_depth; // Operating depth of the parser before the state transition
   tU32 curdepth = 0; // Current depth in the file
   element_t *pelement=NULL; // Currently operating element

   while (((c=libminxml_priv_nextchar(pgbl)) != (tChar)EOF) &&
            !(curstate==LIBMINXML_STATE_INVALID))
   {
      prev_depth=curdepth; // Get current depth before transitioning
      prev_state = curstate; // Get the current state before transitioning
      curstate = libminxml_priv_get_next_state(pgbl, c, curstate, &curdepth);

      switch (curstate)
      {
         case LIBMINXML_STATE_COMMENT:
            // Just consume this comment
            curstate = libminxml_priv_consume_comment(pgbl);

            // Restore the state of the parser to the state
            // it was in before the comment began. This way, we'll make
            // sure that the comment has not changed the parser state diagram
            // much.
            if (curstate != LIBMINXML_STATE_INVALID)
            {
               curstate = prev_state;
            }
            // else, we get into the invalid state, anyways
            break;

         case LIBMINXML_STATE_NEWELEMENT:
            // We've got a new XML element
            ptempelement = libminxml_priv_create_new_xmlelement();
            pattr=NULL;

            if (!ptempelement)
            {
               libminxml_priv_cleanup(pgbl);
               return (tU32)-1;
            }

            if (prev_state==LIBMINXML_STATE_DECL)
            {
               // Save the new element as first element in this depth
               pgbl->proot=ptempelement;
               pelement=ptempelement; // This becomes the currently operating element
               curdepth=1;
            }
            else
            {
               if ((prev_depth == curdepth) && (pelement!=NULL))
               {
                  // We've got a sibling for the current element
                  pelement->psibling=ptempelement;

                  // Parent of this element is the same as the parent of the
                  // currently operating element
                  ptempelement->pparent=pelement->pparent;

                  // Update the currently operating element
                  pelement=ptempelement;
               }
               else
               {
                  if (pelement)
                  {
                     // We've got a child for the current element
                     pelement->pchild=ptempelement;
                     ptempelement->pparent=pelement;
                     pelement=ptempelement; // This now becomes the currently operating element
                  }
               }
            }
            break;

         case LIBMINXML_STATE_ELENAME:
            // Collect the name of the tag in ptext
            ptext = libminxml_priv_collect_text(ptext, c, &txtlen);

            if (!ptext)
            {
               libminxml_priv_cleanup(pgbl);
               return (tU32)-1;
            }

            break;

         case LIBMINXML_STATE_TOKSPC:
         case LIBMINXML_STATE_ENDNEWELEMENT:
            if (prev_state == LIBMINXML_STATE_ELENAME)
            {
               if (pelement)
               {
                  /* Element name has just ended */
                  pelement->pname=ptext;
               }
            }
            else if (prev_state == LIBMINXML_STATE_ATTVAL)
            {
               if (pattr)
               {
                  // Attribute's value has just ended
                  pattr->pattrval=ptext;
                  pattr->pnext=NULL; // No more attributes
               }
            }
            ptext=NULL;
            txtlen=0;
            break;

         case LIBMINXML_STATE_ATTNAME:
            if (prev_state == LIBMINXML_STATE_TOKSPC)
            {
               // Create space for the new attribute
               if (pattr)
               {
                  // Attribute already exists. Create another one and
                  // make this as the "next" of the existing attribute
                   ptempattr=libminxml_priv_create_new_attr();

                   if (!ptempattr)
                     {
                        libminxml_priv_cleanup(pgbl);
                        return (tU32)-1;
                     }
                   else
                   {
                      pattr->pnext = ptempattr;
                      pattr = ptempattr;
                   }
               }
               else
               {
                  /* New attribute is getting created in this XML element */
                  pattr=libminxml_priv_create_new_attr();

                  if (!pattr)
                  {
                     libminxml_priv_cleanup(pgbl);
                     return (tU32)-1;
                  }
                  else
                  {
                     if (pelement)
                     {
                        pelement->pattr=pattr;
                     }
                  }
               }
            }
            // Collect the name of the attribute's name in ptext
            ptext = libminxml_priv_collect_text(ptext, c, &txtlen);

            if (!ptext)
            {
               libminxml_priv_cleanup(pgbl);
               return (tU32)-1;
            }

            break;

         case LIBMINXML_STATE_TOKEQ:
            if (pattr)
            {
               /* Attribute's key has just ended */
               pattr->pattr=ptext;
            }
            ptext=NULL;
            txtlen=0;
            break;

         case LIBMINXML_STATE_ATTVAL:
            if (prev_state == LIBMINXML_STATE_ATTVAL)
            {
               // Collect the name of the attribute's value in ptext
               ptext = libminxml_priv_collect_text(ptext, c, &txtlen);

               if (!ptext)
               {
                  libminxml_priv_cleanup(pgbl);
                  return (tU32)-1;
               }
            }
            // else, we have an ' or a " here - and we ignore them

            break;

         case LIBMINXML_STATE_CONTENT:
            // Collect the name of the element's content in ptext
            ptext = libminxml_priv_collect_text(ptext, c, &txtlen);

            if (!ptext)
            {
               libminxml_priv_cleanup(pgbl);
               return (tU32)-1;
            }

            break;

         case LIBMINXML_STATE_ENDBEG:

            if (prev_state == LIBMINXML_STATE_CONTENT)
            {
               if (pelement)
               {
                  // Tag's value is obtained
                  pelement->pcontent=ptext;
               }
               ptext=NULL;
               txtlen=0;
            }
            else if (prev_state == LIBMINXML_STATE_ENDEND)
            {
                  // This means, the previous depth XML tag
                  // has just been closed.
                  // <person>
                  //    <name> NAME </name>
                  // </person>
                  //  ^
                  //  |
                  // Here, ENDBEG is preceded by an ENDEND.
                  // In this case, the currently operating element becomes the
                  // parent of the currently operating element
                  if (pelement)
                     pelement=pelement->pparent;
            }
            else if (prev_state == LIBMINXML_STATE_ENDBEG)
            {
               // We'll now collect the name of the tag that is
               // being closed
               // Collect the name of the attribute's key in ptext
               ptext = libminxml_priv_collect_text(ptext, c, &txtlen);

               if (!ptext)
               {
                  libminxml_priv_cleanup(pgbl);
                  return (tU32)-1;
               }
            }
            // else do nothing
            break;

         case LIBMINXML_STATE_ENDEND:

            if (prev_state != LIBMINXML_STATE_ENDEND)
            {
               // If the prev_state is TOKSPC, then this point of code represents
               // a single tag entry <Tag_Name ATTR="ATTR_VAL" /> .
               // At this point, ptext last held the value ATTR_VAL and has
               // already assigned it to attr variable and ptext contents has been
               // cleared. Thus a strcmp would throw an exception because ptext 
               // is NULL. Hence, the below if-condition is placed as a protection.
               if(prev_state != LIBMINXML_STATE_TOKSPC)
               {
                  if (pelement && ptext) //lint removal
                  {
                     // Check if we have the end tag of the last started tag
                     if (strcmp(pelement->pname, ptext))
                     {
                        // ERROR
                        libminxml_priv_cleanup(pgbl);
                        return (tU32)-1;
                     }
                  }
               }

               ptext = NULL;
               txtlen = 0;

               // We've come out of all tags; ideally, we have no "currently
               // operating element" - i.e., pelement should have been ideally made
               // NULL. However, if we have a sibling for this "last closed
               // element, we'll need this pelement intact to attach a sibling
               // for this element. Hence, we'll intensionally not make
               // this as NULL.
            }
            // else we do nothing

            break;

         case LIBMINXML_STATE_INVALID:
         case LIBMINXML_STATE_INIT:
         case LIBMINXML_STATE_DECL:
         case LIBMINXML_STATE_MAX:
         default:
            // Other states, we do not do anything.
            break;
      }
   }

   if (curstate != LIBMINXML_STATE_INVALID)
   {
      // If we are here, we have successfully built an XML tree.
      // Now, create metadata for optimising "find"s.
#if OPTIMISED_FIND
      libminxml_priv_optimise_find(pgbl);
#endif
      return 0;
   }
   else
   {
      // Tough luck
      return (tU32)-1;
   }
}

/*!\fn element_t *libminxml_priv_find(global_t *pgbl, tChar *ppath)
 * @brief Finds the XML element that corresponds to the path provided in ppath
 *
 * @param pgbl [in] Global handle
 * @param ppath [in] The path of the element to be found.
 * @return XML element.
 */
element_t *libminxml_priv_find(const global_t *pgbl, const tChar *ppath)
{
   return libminxml_priv_fetch_element( pgbl, ppath );
}

/*!\fn tBool libminxml_get_next_attr_res (rset_t* presult)
 * @brief Forwards the result set to the next attribute set.
 *
 * @param presult [in] Handle returned by libminxml_get_rset().
 * @return TRUE if there were any more result sets. Else, FALSE.
 * @history 07.Jun.2010 Jeryn M. (RBEI/ECF1) Resolve possible exception causing 
 *                                           error.
 */
tBool libminxml_get_next_attr_res(rset_t* presult)
{
   tBool bResult = FALSE;
   //If Input is not null and RHS value of assignment operation is not NULL.
   if( presult->pattr && presult->pattr->pnext)
   {
      presult->pattr = presult->pattr->pnext;
      bResult = TRUE;
   }
   
   return bResult;
}
