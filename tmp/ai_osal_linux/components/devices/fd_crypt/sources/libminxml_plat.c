/*!\file
 * @brief Platform dependent implementation of the libminxml library
 *
 * @author Arvind Devarajan (RBEI/ECF)
 * @date   26.Mar.2010
 * @history  
 * 21.Jun.2010  v1.1   XML-Cache mode implemented for performance.
 * 30.Noc.2010  v1.2   Trace Message for Empty xml file | kgr1kor
 * 05.Apr.2011  v1.3   Introduced new handle structure for cache mode to
 *                      make functions reentrant | Anooj Gopi (RBEI/ECF1)
 */

//#define _POSIX_C_SOURCE // Make sure we have all the POSIX symbols available
                        // from the header files that are included here  rav8kor commented
#include <stdio.h>
#include "libminxml_plat.h"

/*!\fn inline hFile libminxml_plat_fopen(tChar *pfile, void *pplat)
 *
 * @param pfile [in] File name
 * @param pplat [in] Platform specific "open" parameters (like mode, etc.)
 * @return File handle
 *
 * @history
 * 05-Apr-2011 | Anooj Gopi | Function is made reentrant to add multi device support.
 */
hFile libminxml_plat_fopen(const tChar *pfile, tVoid *pplat)
{
#ifdef __CYGWIN__
   {
      tChar *pmode = pplat;

      return fopen(pfile, pmode);
   }
#elif defined(__XML_CACHE__)
   {
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( pplat );
      hFile pFileHandle;
      tBool bResult = FALSE;

      /* Allocate memory for Handle Structure */
      pFileHandle = OSAL_pvMemoryAllocate(sizeof(tXmlFileHandle));
      if( pFileHandle )
      {
         //Init the data-structure.
         pFileHandle->hXmlFile       = 0;
         pFileHandle->s32DataLength  = 0;
         pFileHandle->s32DataPointer = 0;
         pFileHandle->ps8XMLData     = OSAL_NULL;

         //Open File and store file-descriptor.
         if( ( pFileHandle->hXmlFile = OSAL_IOOpen( pfile,
                                               OSAL_EN_READONLY ) ) == OSAL_ERROR )
         {
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                 TR_CRYPT_MSG_INFO, "xmlfile open failed",
                                 0, 0, 0, 0 );

         }
         //Fetch Full Filelength
         else if( ( pFileHandle->s32DataLength =
                     OSALUTIL_s32FGetSize( pFileHandle->hXmlFile ) ) == OSAL_ERROR )
         {
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                                 TR_CRYPT_MSG_INFO, "xmlfile filelen failed",
                                 0, 0, 0, 0 );
         }
         //Print trace message for empty file.
         else if( pFileHandle->s32DataLength == 0 )
         {
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                 TR_CRYPT_MSG_INFO, "xmlfile is EMPTY!",
                                 0, 0, 0, 0 );
         }
         //Malloc full size
         else if( ( pFileHandle->ps8XMLData =
             (tPS8)OSAL_pvMemoryAllocate( (tU32)pFileHandle->s32DataLength ) ) == OSAL_NULL )
         {
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                 TR_CRYPT_MSG_INFO, "xmlfile malloc failed",
                                 0, 0, 0, 0 );
         }
         //Read entire file into buffer.
         else if( OSAL_s32IORead( pFileHandle->hXmlFile, pFileHandle->ps8XMLData,
                         (tU32)pFileHandle->s32DataLength ) != pFileHandle->s32DataLength )
         {
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                 TR_CRYPT_MSG_INFO, "xmlfile read failed",
                                 0, 0, 0, 0 );
         }
         else
         {
        	bResult = TRUE;
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                  TR_CRYPT_MSG_INFO, "Platform open success",
                                  0, 0, 0, 0 );
         }
      }
      else
      {
          fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                               TR_CRYPT_MSG_ERROR, "Memory allocation for File handle failed",
                               0, 0, 0, 0 );
      }

      /* If we are here due to any error, clean all allocated resources */
      if(bResult != TRUE)
      {
         if(pFileHandle)
         {
            if(pFileHandle->ps8XMLData)
               OSAL_vMemoryFree( pFileHandle->ps8XMLData );

            if(pFileHandle->hXmlFile)
               OSAL_s32IOClose( pFileHandle->hXmlFile );

            OSAL_vMemoryFree(pFileHandle);
            pFileHandle = OSAL_NULL;
        }
      }

      return pFileHandle;
   }
#else
   {
      OSAL_tenAccess enAccess = OSAL_EN_READONLY;
      tChar *paccess = (tChar *)pplat;

      if( paccess[0] == 'r' ) //FIXME: handle all conditions
      {
         enAccess = OSAL_EN_READONLY;
         
         if( paccess[1] == 'w' || paccess[1] == '+' )
         {
            enAccess = OSAL_EN_READWRITE;
         }
      }
      else if( paccess[0] == 'w' )
      {
         enAccess = OSAL_EN_WRITEONLY;
      }
      
      /* NOTE: the return type is OSAL_tIODescriptor */
      return OSAL_IOOpen(pfile, enAccess);
   }
#endif
}

/*!\fn inline tChar libminxml_plat_fgetc(hFile pfile)
 * @brief Read a character from the file pfile
 *
 * @param pfile [in] File handle from where the character should be read
 * @return Return the character that's read
 */
tChar libminxml_plat_fgetc( hFile pfile )
{
#ifdef __CYGWIN__
   {
      return fgetc(pfile);
   }
#elif defined(__XML_CACHE__)
   {
      tChar cByte = '\0';

      /* Get character at current position.
       * If Buffer indices lie between 0 and filelen-1,
       * fetch the correct value from buffer. 
      */
      if( pfile->s32DataPointer < pfile->s32DataLength )
      {
         cByte = (tChar)pfile->ps8XMLData[pfile->s32DataPointer];
         /* Increment pointer to next character.
          * So increment index till filelen. So, when index goes beyond buffer-len,
          * the byte returned will be EOF.
         */
         if( (pfile->s32DataPointer+1) <= pfile->s32DataLength )
         {
            pfile->s32DataPointer += 1;
         }
      }
      // File-pointer beyond buffer-len. Return EOF.
      else
      {
         cByte = (tChar)-1;
      }
      return cByte;
   }
#else
   {
      tS8  cByte     = '\0';
      tS32 s32Result = OSAL_ERROR;

      /* NOTE: the return type is OSAL_tIODescriptor */
      s32Result = OSAL_s32IORead(pfile, &cByte, 1);
      //Error Case
      if (s32Result < 1)
      {
         return (tChar)-1;
      }
      //Success case
      else
      {
         return (tChar)cByte;
      }
   }
#endif
}

/*!\fn inline void libminxml_plat_ungetc(tChar c, hFile pfile)
 * @brief Platform-specific "ungetc"
 *
 * @param c [in] Character to be "ungetc"'d
 * @param pfile [in] File handle from where the character should be read
 */
void libminxml_plat_ungetc( tChar c, hFile pfile )
{
#ifdef __CYGWIN__
   {
      ungetc(c, pfile);
   }
#elif defined(__XML_CACHE__)
   {
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(c);

      //Decrement file pointer. Minimum allowed value is 0.
      if( pfile->s32DataPointer - 1 >= 0 )
      {
         pfile->s32DataPointer -= 1;
      }
   }
#else
   {
      OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(c);
      OSALUTIL_s32FSeek(pfile, -1, OSAL_C_S32_SEEK_CUR);
   }
#endif
}


/*!\fn inline void libminxml_plat_fprintf(void *pplat, char *pstr)
 * @brief Log-printf
 *
 * If pctx is NULL, then, logging happens on console (platform dependent).
 *
 * @param pplat [in] Platform specific logging stream
 * @param pstr [in] String to be logged
 */
void libminxml_plat_fprintf( tVoid *pplat, const char *pstr )
{
#ifdef __CYGWIN__
   {
      hFile *pfile = (hFile*)pplat;

      if (!pplat)
         printf(pstr);
      else
      {
         fprintf(pfile, pstr);
      }
   }
#else
   /* Under development */ 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( pplat ); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( pstr ); 
   /*{
      hFile *pfile  = (hFile*)(pplat);
      if(pplat)
      {
         OSALUTIL_s32FPrintf(pfile, pstr);
      }
   }*/
#endif
}

/*!\fn inline void libminxml_plat_fclose(hFile pfile)
 *
 * @param pfile [in] File to be closed
 */
void libminxml_plat_fclose( hFile pfile )
{
#ifdef __CYGWIN__
   {
      fclose(pfile);
   }
#elif defined(__XML_CACHE__)

   //Free allocated Memory
   if( OSAL_s32IOClose( pfile->hXmlFile ) == OSAL_ERROR )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_INFO, "xmlfile close failed", 
                           0, 0, 0, 0 );
   }

   if( pfile->ps8XMLData != OSAL_NULL )
   {
      OSAL_vMemoryFree( pfile->ps8XMLData );
   }

   /* Free the memory allocated for Handle */
   OSAL_vMemoryFree(pfile);

#else
   {
      OSAL_s32IOClose(pfile);
   }
#endif
}

/*!\fn inline tU32 libminxml_plat_loginit(void **ppplat)
 * @brief Platform specific logging initialization
 *
 * @param ppplat [in] Platform-specific logging stream to be inited
 * @return 0 - Success
 * @return -1 - Error
 */
tU32 libminxml_plat_loginit( void **ppplat )
{
#ifdef __CYGWIN__
   {
      *ppplat = NULL;

      return 0;
   }
#else
   {
      *ppplat = NULL;
      
      return 0;
   }
#endif
}
