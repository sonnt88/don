#if 0
#include "bpcl.h"

#include <sys/types.h>
#include <sys/timeb.h>

#include <io.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

// Private key of signing application - THIS MUST NOT
// BE DISTRIBUTED !

#define  PATH_PRIVKEY	"C:\\SDCERT_PRIVKEY.DAT"

// Public key of signing application - THIS NEEDS TO
// BE STORED IN ANY DEVICE'S CONFIGURATION MEMORY (KDS)

#define  PATH_PUBKEY	"C:\\SDCERT_PUBKEY.DAT"

// Path to certificate - should be "/dev/card/CRDSHARE.DAT"
// in a navigation system

#define  PATH_CERT		"C:\\CRDSHARE.DAT"

// Sample CID data - must be replaced with real CID (read from
// SD card using CMD10)

static tU8 pSampleCID[16] = { 
	0x21, 0x83, 0x5A, 0xC5, 0xEB, 0xC0, 0x51, 0x8A,
	0x38, 0x43, 0x92, 0x09, 0xF9, 0x3A, 0xE5, 0xDF
};

// Prototypes for sd_sign.c - better move to an appropriate header file

extern int sd_sign(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPrivKey,	// 20-bytes signer's private key
	tU8* pSigBuffer			// buffer to hold signature (>= 48bytes)
);

extern int sd_get_cid_key(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPubKey,		// 40-bytes signer's public key
	tU8* pSig,				// signature read from SD card (48 bytes)
	tU8* pKeyBuffer			// buffer to hold encryption key (>= 16 bytes)
);

int main(void) {

	int fd;

	tU8	aPrivateKey[BPCL_ECC_PRIVATE_KEY_SIZE];
	tU8	aPublicKey[BPCL_ECC_PUBLIC_KEY_SIZE];
	tU8	aSignature[BPCL_ECC_SIGNATURE_SIZE];
	tU8	aKeyBuffer[16];


	//
	//  Application 1: Create SD card certificate
	//
	fd = _open(PATH_PRIVKEY,_O_RDONLY|_O_BINARY);
	if(_read(fd,aPrivateKey,BPCL_ECC_PRIVATE_KEY_SIZE)
		!= BPCL_ECC_PRIVATE_KEY_SIZE) {
		fprintf(stderr, "Error reading private key\n");
		return 1;
	}
	_close(fd);

	if(sd_sign(pSampleCID, aPrivateKey, aSignature) == BPCL_OK) {
		fd = _open(PATH_CERT,_O_CREAT|_O_RDWR|_O_BINARY);
		if(_write(fd, aSignature, BPCL_ECC_SIGNATURE_SIZE)
			!= BPCL_ECC_SIGNATURE_SIZE) {
			fprintf(stderr, "Error writing signature file\n");
			return 1;
		}
		_close(fd);
		fprintf(stdout, "Signature file successfully written\n");
	} else {
		fprintf(stderr, "Error generating signature\n");
	}


	//
	//  Application 2: Verify SD card certificate
	//
	fd = _open(PATH_PUBKEY,_O_RDONLY|_O_BINARY);
	if(_read(fd,aPublicKey,BPCL_ECC_PUBLIC_KEY_SIZE)
		!= BPCL_ECC_PUBLIC_KEY_SIZE) {
		fprintf(stderr, "Error reading public key\n");
		return 1;
	}
	_close(fd);

	fd = _open(PATH_CERT,_O_RDONLY|_O_BINARY);
	if(_read(fd,aSignature,BPCL_ECC_SIGNATURE_SIZE)
		!= BPCL_ECC_SIGNATURE_SIZE) {
		fprintf(stderr, "Error reading signature file\n");
		return 1;
	}
	_close(fd);

	if(sd_get_cid_key(pSampleCID,aPublicKey,aSignature,aKeyBuffer)
		== 0) {
		fprintf(stdout, "Signature file successfully verified\n");
	} else {
		fprintf(stderr, "Error verifying signature file\n");
		return 1;
	}

	return 0;
}
#endif
