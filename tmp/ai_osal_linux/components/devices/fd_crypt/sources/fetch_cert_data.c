/*!
* \file  fetch_cert_data.c
* \brief
*     Fetches the required data from the XML tree parsed by the libminxml parser
*     and stores its locally in the correct format and type.
*
* \par AUTHOR
*         Jeryn Mathew (RBEI/ECF1)
* \par COPYRIGHT
*        (c) 2010 Robert Bosch Engineering and Business Solutions Ltd.
*
* \date
*  26-03-10   v1.0   Jeryn Mathew (RBEI/ECF1)      Initial Revision
*  26-04-10   v1.1   Jeryn Mathew (RBEI/ECF1)      1st phase of CRQs.
*  25-05-10   v1.2   Jeryn Mathew (RBEI/ECF1)      New update for VIN-XML crypt.
*  07-06-10   v1.3   Jeryn Mathew (RBEI/ECF1)      Resolve known issues and fix 
*                                                  large-file handling.
*  07-06-10   v1.4   Jeryn Mathew (RBEI/ECF1)      Error handling for -ve offset
*                                                  and numofbytes
*  22-10-10   v1.5   Gokulkrishnan N               VinFeature Porting for FORD-MFD
*  05-04-11   v1.6   Anooj Gopi   (RBEI/ECF1)      Added multi device support
*  12.03.14   v1.7   Swathi Bolar(RBEI/ECF5)       DID based implementation(INNAVPF-2544)
*/

#include "libminxml.h"
#include "fetch_cert_data.h"
#include "fd_crypt_private.h"

/*!
* \fn static tU16 u16CountEntries( tPChar pcTreePath )
*  @brief Counts the number of elements with the same name as given in the 
*         argument at the appropriate depth.
*
*  @param  hHandle [in] - Fetch layer handle
*  @param  tPChar pcTreePath [in] - The Path to the XML node, where info lies
*  @return Returns the number of entries with the same element name.
*
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant.
*/
static tU16 u16CountEntries( tcFetchHandle hHandle, tPCChar pcTreePath )
{
   tU16   u16Result = 0;
   tPVoid plasts    = OSAL_NULL;

   if(hHandle)
   {
      if ( pcTreePath != OSAL_NULL )
      {
         libminxml_getcontent( hHandle->hXMLCert, pcTreePath, &plasts );

         while( plasts != OSAL_NULL )
         {
            u16Result++;

            libminxml_getcontent( hHandle->hXMLCert, OSAL_NULL, &plasts );
         }
      }
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - VerCertLT ", 0, 0, 0, 0 );
   }

   return u16Result;
}

/*!
* \fn static tBool bGetAttrVal( tPChar pcTreePath, tPChar pcAttrName, tPChar pcAttrVal )
*  @brief Fetches the Attribute's value corresponding to the attribute's name 
*         (given as argument) present at the result-set node.
*  
*  @param  tPChar pResultSet [in] - Result set corresponding to the node in XML tree
*  @param  tPChar pcAttrName [in] - Name of the attribute
*  @param  tPChar pcAttrVal [out] - Buffer for storing the value of the attribute
*  @return Returns TRUE if successful, else FALSE
*  
*  History:
*  25 - May - 2010 | Jeryn Mathew | Corrections for new update for fd-crypt-vin
*  07 - Jun - 2010 | Jeryn Mathew | Corrected char buffer length, for memset
*/
static tBool bGetAttrVal( rset_t* pResultSet, tPCChar pcAttrName, tPChar pcAttrVal )
{
   tBool    bResult       = FALSE;
   tPChar   pTempAttrName = OSAL_NULL;
   tPChar   pTempAttrVal  = OSAL_NULL;
   tChar    acAttrName[255] = {0};
   tChar    acAttrVal[255] = {0};
   tBool    bBreakCond    = FALSE;
   
   /* Protector code */
   if( pcAttrName == OSAL_NULL || pcAttrVal == OSAL_NULL )
   {
      bResult = FALSE;
   }
   /* Check if result set is valid */
   else if( libminxml_validate_result_set( pResultSet ) )
   {
      /* point to the top of the attribute list in the elements */
      if( libminxml_reset_attr_list( pResultSet ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "Attributes reset failed", 0, 0, 0, 0 );
      }
      /* Fetch the Attribute name */
      else if( ( pTempAttrName = libminxml_get_attr_name( pResultSet ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "get_attr_name() failed", 0, 0, 0, 0 );
      }
      else
      {
         if( OSAL_s32StringCompare( pTempAttrName, pcAttrName ) != 0 )
         {
            bBreakCond = TRUE;
            while( bBreakCond )
            {
               // Check whether Attr-names match
               if( OSAL_s32StringCompare( acAttrName, pcAttrName ) == 0 )
               {
                  //Copy the attr-val into buffer, if its not NULL.
                  pTempAttrVal = libminxml_get_attr_value( pResultSet );
                  if( pTempAttrVal )
                  {
                     //Success.
                     bResult = TRUE;                  
                     (tVoid)OSAL_szStringCopy( pcAttrVal,pTempAttrVal );
                  }
                  break;
               }
               
               OSAL_pvMemorySet( acAttrName, 0, 255 );
               OSAL_pvMemorySet( acAttrVal, 0, 255 );
               /*New parameter acAttrVal is introduced to fix 
                obtaining wrong value of pcAttrVal*/
               bBreakCond = libminxml_get_next_attr( pResultSet, acAttrName, acAttrVal );

            }
         }
         else
         {
            //Perform strcpy only if source is NOT null!
            pTempAttrVal = libminxml_get_attr_value( pResultSet );
            if( pTempAttrVal )
            {
               bResult = TRUE;
               (tVoid)OSAL_szStringCopy( pcAttrVal, pTempAttrVal );
            }            
         }
      }
   }
   else
   {
      //Result Set Validation failed
      bResult = FALSE;
   }
      
   return bResult;
}

/*!
* \fn static tBool bGetArray( tPChar pcTreePath, tU8 **ppu8Array, tU32 u32ArrayLength )
*  @brief Fetches the contents of the element (which is an array) for the given 
*         tree-path (given as argument).
*  
*  @param  tS32 s32CryptDevID  [in] - Device ID (offset to private data structure)
*  @param  tPChar pcTreePath   [in]  - The Path to the XML node, where info lies
*  @param  tU8 **ppu8Array     [out] - Buffer to store the data
*  @param  tU32 u32ArrayLength [in]  - Length of the buffer.
*  @return Returns TRUE if successful, else FALSE
*/
static tBool bGetArray( tcFetchHandle hHandle, tPCChar pcTreePath, tU8 **ppu8Array, tU32 u32ArrayLength )
{
   tChar*  pTok       = OSAL_NULL;
   tChar   delim[]    = " ,\r\n\t";
   tVoid*  plasts     = OSAL_NULL;
   tChar*  pContent   = OSAL_NULL;
   tU32    u32Index   = 0;
   tBool   bResult    = TRUE;
   tU32    u16ArrElem = 0;
   tPChar  pcTempVal  = OSAL_NULL;
   tS32    s32Value   = 0; // For removing warnings
   tPChar  pcSavePtr;

   /* Validate the arguments */
   if( !hHandle || pcTreePath == OSAL_NULL || u32ArrayLength == 0 )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "ERR: Invalid arg for bGetArray", 0, 0, 0, 0 );
   }
   /* Allocate the memory for array */
   else if( ( *ppu8Array = (tPU8)OSAL_pvMemoryAllocate(
                                 u32ArrayLength * sizeof( tU8 ) ) ) == OSAL_NULL )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "malloc() error", 0, 0, 0, 0 );
   }
   else if( ( OSAL_pvMemorySet( *ppu8Array, 0,
                                ( u32ArrayLength * sizeof( tU8 ) ) ) ) == OSAL_NULL )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "memset() error", 0, 0, 0, 0 );
   }
   /* Get the Contents of the element */
   else if( ( pContent = libminxml_getcontent( hHandle->hXMLCert, pcTreePath,
                                                &plasts ) ) == OSAL_NULL )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "libminxml_getcontent() error", 0, 0, 0, 0 );
   }
   else
   {
      /* Parse the contents and fill the array */
      if( ( pcTempVal = (tPChar)OSAL_pvMemoryAllocate( 
                  sizeof(tChar) * ( strlen( pContent ) + 1 ) ) ) != OSAL_NULL )
      {
         (tVoid)OSAL_szStringCopy( pcTempVal, pContent );
         
         /* Since OSAL doesn't have equivalent function for strtok_r, using direct libc call */
         pTok = (tChar*) strtok_r( pcTempVal, delim, &pcSavePtr );
         for( u32Index = 0; u32Index < u32ArrayLength && pTok != OSAL_NULL; u32Index++ )
         {
            if( OSAL_s32ScanFormat( pTok, "%x", &s32Value ) <= 0 )         
            {
               bResult = FALSE;
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                                    TR_CRYPT_MSG_INFO, "sscanf() error", 
                                    0, 0, 0, 0 );
               break;
            }
            u16ArrElem = (tU16)s32Value; // For revmoving warnings
            (*ppu8Array)[u32Index] = (tU8)u16ArrElem;
            // Get the next token (array value)
            /* Since OSAL doesn't have equivalent function for strtok_r, using direct libc call */
            pTok = (tChar*) strtok_r( OSAL_NULL, delim, &pcSavePtr );
         }
         OSAL_vMemoryFree( pcTempVal );
      }
      else
      {
         //Alloc failed.
         bResult = FALSE;
      }
   }

   return bResult;
}

/*!
* \fn static tBool bFetchLifetime()
*  @brief Fetches the Lifetime values from the XML certificate
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  19-Mar-2011 | Anooj Gopi | Added multi device support
*  19-May-2011 | Anooj Gopi | Added Handle to make it reentrant
*/
static tBool bFetchLifetime( tFetchHandle hHandle )
{
   tBool   bResult        = TRUE;
   tChar   cDelim         = '\0';
   rset_t  *pResultSet    = OSAL_NULL;

   if(hHandle)
   {
      /* Prepare result set */
      if( ( pResultSet = libminxml_get_rset( hHandle->hXMLCert,
                                             PARTITION_CERT_LIFETIME ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchLifetime: get_rset error", 0, 0, 0, 0 );
      }
      /* Fetch the Content from the Tree*/
      else if( bGetAttrVal( pResultSet, ATTR_LIFETIME_START,
            hHandle->acLifetimeStart ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchLifetime: bGetAttrVal(start) error", 0, 0, 0, 0 );
      }
      /* Parse and store result */
      else if( ( OSAL_s32ScanFormat( hHandle->acLifetimeStart, "%d%c%d%c%d%c%d%c%d%c%d",
                                     &(hHandle->rLifeTimeStart).s32Year,
                                     &cDelim, &(hHandle->rLifeTimeStart).s32Month,
                                     &cDelim, &(hHandle->rLifeTimeStart).s32Day,
                                     &cDelim, &(hHandle->rLifeTimeStart).s32Hour,
                                     &cDelim, &(hHandle->rLifeTimeStart).s32Minute,
                                     &cDelim, &(hHandle->rLifeTimeStart).s32Second ) ) < 11 )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchLifetime: sscanf(start) error", 0, 0, 0, 0 );
      }

      /* Update to the XML feature. Null value for lifetime-end means "forever" */
      /* Fetch the Content from the Tree*/
      else if( bGetAttrVal( pResultSet, ATTR_LIFETIME_END, hHandle->acLifetimeEnd ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchLifetime: Lifetime end forever", 0, 0, 0, 0 );
      }
      /* Parse and store result */
      else if( ( OSAL_s32ScanFormat( hHandle->acLifetimeEnd, "%d%c%d%c%d%c%d%c%d%c%d",
                                     &(hHandle->rLifeTime).s32Year,
                                     &cDelim, &(hHandle->rLifeTime).s32Month,
                                     &cDelim, &(hHandle->rLifeTime).s32Day,
                                     &cDelim, &(hHandle->rLifeTime).s32Hour,
                                     &cDelim, &(hHandle->rLifeTime).s32Minute,
                                     &cDelim, &(hHandle->rLifeTime).s32Second ) ) < 11 )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchLifetime: sscanf(end) error", 0, 0, 0, 0 );
      }

      /* Free the result set */
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - FetchLT ", 0, 0, 0, 0 );
   }
   
   return bResult;
}

/*!
* \fn static tBool bFetchRevocTimestamp()
*  @brief Fetches the timestamp associated to the revocation list
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make it reentrant
*/
static tBool bFetchRevocTimestamp( tFetchHandle hHandle )
{
   tBool   bResult        = TRUE;
   tChar   acAttrName[10] = ATTR_REVOC_TIMESTAMP;
   tChar   cDelim         = '\0';
   rset_t  *pResultSet    = OSAL_NULL;

   if(hHandle)
   {
      /* Prepare result set */
      if( ( pResultSet = libminxml_get_rset( hHandle->hXMLCert, REVOC_TIMESTAMP ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRevocTS: get_rset() error", 0, 0, 0, 0 );
      }
      /* Fetch the Content from the Tree*/
      else if( bGetAttrVal( pResultSet, acAttrName, hHandle->acRevocTimestamp ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRevocTS: bGetAttrVal() error", 0, 0, 0, 0 );
      }
      /* Parse and store result */
      else if( ( OSAL_s32ScanFormat( hHandle->acRevocTimestamp, "%d%c%d%c%d%c%d%c%d%c%d",
                                     &(hHandle->rRevocTimestamp).s32Year,
                                     &cDelim, &(hHandle->rRevocTimestamp).s32Month,
                                     &cDelim, &(hHandle->rRevocTimestamp).s32Day,
                                     &cDelim, &(hHandle->rRevocTimestamp).s32Hour,
                                     &cDelim, &(hHandle->rRevocTimestamp).s32Minute,
                                     &cDelim, &(hHandle->rRevocTimestamp).s32Second ) ) < 11 )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRevocTS: sscanf error", 0, 0, 0, 0 );
      }

      /* Free the result set */
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - FetchRevTS ", 0, 0, 0, 0 );
   }
      
   return bResult;
}

/*!
* \fn static tBool bFetchRevocationList()
*  @brief Fetches the List of revoked keys from the XML certificate
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*  
*  History:
*  07-Jun-2010 | Jeryn Mathew | Corrections for new update for fd-crypt-vin
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
static tBool bFetchRevocationList( tFetchHandle hHandle )
{
   tChar*  pTok         = OSAL_NULL;
   tChar   delim[]      = " ,\r\n\t";
   tVoid*  plasts       = OSAL_NULL;
   tChar*  pContent     = OSAL_NULL;
   tInt    iRow         = 0;
   tInt    iCol         = 0;
   rset_t  *pResultSet  = OSAL_NULL;
   tChar   acAttrVal[255] = {0};
   tS32    s32KeyLength = 0;
   tBool   bResult      = TRUE;
   tPChar  pcTempVal    = OSAL_NULL;
   tS32    s32TempVal   = 0;
   tPChar  pcSavePtr;

   if(hHandle)
   {
      /* Update for the vin-xml: NULL revocation list is NOT an error */
      /* Prepare result set */
      if( ( pResultSet = libminxml_get_rset( hHandle->hXMLCert, REVOC_LIST_KEY ) ) == OSAL_NULL )
      {
         hHandle->bIsRevocListPresent = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRL: No Revoked Keys", 0, 0, 0, 0 );
      }
      /* Get the Key Length */
      else if( ( bResult = bGetAttrVal( pResultSet, "length", acAttrVal ) ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRL: bGetAttrVal(length) error", 0, 0, 0, 0 );
      }
      /* Parse and Store Value */
      else if( OSAL_s32ScanFormat( acAttrVal, "%d", &(s32KeyLength) ) < 1 )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRL: sscanf(length) error", 0, 0, 0, 0 );
      }
      /* Get the Revocation List Length*/
      else if( ( (hHandle->rRevocList).u16ListLength = u16CountEntries(
                                                       hHandle, REVOC_LIST_KEY ) ) == 0 )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRL: u16CountEntries()==0 error", 0, 0, 0, 0 );
      }
      /* Prepare the Revocation List data-structure */
      else if( ( (hHandle->rRevocList).pList = ( trArray* )OSAL_pvMemoryAllocate(
                        (hHandle->rRevocList).u16ListLength * sizeof( trArray ) ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRL: malloc() list error", 0, 0, 0, 0 );
      }
      /* Fetch the first revoked key */
      else if( ( pContent = libminxml_getcontent( hHandle->hXMLCert, REVOC_LIST_KEY,
                                                  &plasts ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRL: getcontent( ) error", 0, 0, 0, 0 );
      }
      else
      {
         if( hHandle->bIsRevocListPresent )
         {
            /* Iterate till end of list in tree */
            for( iRow = 0; iRow < (hHandle->rRevocList).u16ListLength; iRow++ )
            {
               /* Prepare space for the key to be read */
               (hHandle->rRevocList).pList[iRow].u32KeyLength = (tU32)s32KeyLength;
               if( ( (hHandle->rRevocList).pList[iRow].pu8Array = (tPU8)OSAL_pvMemoryAllocate(
                     (hHandle->rRevocList).pList[iRow].u32KeyLength * sizeof( tU8 ) ) ) == OSAL_NULL )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_INFO,
                                       "FetchRL: malloc() key array error", 0, 0, 0, 0 );
                  break;
               }

               /* Clear the buffer */
               if( ( OSAL_pvMemorySet( (hHandle->rRevocList).pList[iRow].pu8Array, 0,
                   ( (hHandle->rRevocList).pList[iRow].u32KeyLength * sizeof( tU8 ) ) ) ) == OSAL_NULL )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_INFO,
                                       "FetchRL: memset() key array error", 0, 0, 0, 0 );
                  break;
               }

               /* Parse the Contents of the element and fetch the array data*/
               if( ( pcTempVal = (tPChar)OSAL_pvMemoryAllocate(
                                          strlen( pContent ) + 1 ) ) != OSAL_NULL )
               {
                  (tVoid)OSAL_szStringCopy( pcTempVal, pContent );

                  /* Since OSAL doesn't have equivalent function for strtok_r, using direct libc call */
                  pTok = (tChar*)strtok_r( pcTempVal, delim, &pcSavePtr );
                  for( iCol = 0; iCol < s32KeyLength && pTok != OSAL_NULL ; iCol++ )
                  {
                     OSAL_s32ScanFormat( pTok, "%x", &s32TempVal );
                     (hHandle->rRevocList).pList[iRow].pu8Array[iCol] = (tU8)s32TempVal;
                     /* Since OSAL doesn't have equivalent function for strtok_r, using direct libc call */
                     pTok = (tChar*)strtok_r( OSAL_NULL, delim, &pcSavePtr );
                  }
                  OSAL_vMemoryFree( pcTempVal );

                  /* Fetch next Content */
                  if( ( pContent = libminxml_getcontent( hHandle->hXMLCert,
                                             OSAL_NULL, &plasts ) ) == OSAL_NULL )
                  {
                     if ( iRow < (hHandle->rRevocList).u16ListLength - 1)
                        bResult = FALSE;
                     break;
                  }
               }
               else
               {
                  //Alloc Failed
                  bResult = FALSE;
               }
            }
         }
      }

      /* Free the result set */
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - FetchRevList ", 0, 0, 0, 0 );
   }

   return bResult;
}

/*!
* \fn static tBool bFetchRevocationSignature()
*  @brief Fetches the Revocation signature from the XML certificate
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  07-Jun-2010 | Jeryn Mathew | Change to U32 storage.
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
static tBool bFetchRevocationSignature( tFetchHandle hHandle )
{
   tBool  bResult        = TRUE;
   tChar  acAttrVal[255] = {0};
   tS32   s32KeyLength   = 0;
   rset_t *pResultSet    = OSAL_NULL;

   if(hHandle)
   {
      /* Prepare result set */
      if( ( pResultSet = libminxml_get_rset( hHandle->hXMLCert, REVOC_SIGNATURE ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRS: rset() error", 0, 0, 0, 0 );
      }
      /* Get the Signature Length */
      else if( ( bResult = bGetAttrVal( pResultSet, "length", acAttrVal ) ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRS: bGetAttrVal(length) error", 0, 0, 0, 0 );
      }
      /* Parse and Store Value */
      else if( OSAL_s32ScanFormat( acAttrVal, "%d", &s32KeyLength ) < 1 )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRS: sscanf(length) error", 0, 0, 0, 0 );
      }
      /* Get the Signature */
      else if( ( bResult = bGetArray( hHandle, REVOC_SIGNATURE, &((hHandle->rRevocSign).pu8Array),
                                      (tU16)s32KeyLength ) ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchRS: bGetArray(REVOC_SIGNATURE) error", 0, 0, 0, 0 );
      }

      (hHandle->rRevocSign).u32KeyLength = (tU32)s32KeyLength;

      /* Free the result set */
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - FetchRevSign ", 0, 0, 0, 0 );
   }
   //Return the result
   return bResult;
}

/*!
* \fn static tBool bFetchVIN()
*  @brief Fetches the VIN from Partition 0 from the XML certificate
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
static tBool bFetchVIN( tFetchHandle hHandle )
{
   tBool   bResult    = TRUE;
   rset_t* pResultSet = OSAL_NULL;
   
   if(hHandle)
   {
      /* Create a result set */
      if( ( pResultSet = libminxml_get_rset( hHandle->hXMLCert, PARTITION_ID ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchVIN: get_rset(PARTITION_ID) error", 0, 0, 0, 0 );
      }
      /* Note: a NULL VIN is NOT an incorrect VIN */
      else if( bGetAttrVal( pResultSet, ATTR_VIN, hHandle->acVIN ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchVIN: No VIN", 0, 0, 0, 0 );
      }

      // Free result set
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - bFetchVIN ", 0, 0, 0, 0 );
   }

   return bResult;
}

/*!
* \fn static tBool bFetchDID()
*  @brief Fetches the VIN from Partition 0 from the XML certificate
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  12-Mar-2014 | Swathi Bolar | DID based implementation(INNAVPF-2544)
*/
static tBool bFetchDID( tFetchHandle hHandle )
{
   tBool   bResult    = TRUE;
   rset_t* pResultSet = OSAL_NULL;
   
   if(hHandle)
   { 
      /* Create a result set */
      if( ( pResultSet = libminxml_get_rset( hHandle->hXMLCert, PARTITION_ID ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchDID: get_rset(PARTITION_ID) error", 0, 0, 0, 0 );
      }
      /* Note: a NULL DID is NOT an incorrect DID */
      else if( bGetAttrVal( pResultSet, ATTR_DID, hHandle->acDID ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchDID: No DID", 0, 0, 0, 0 );
      }
      else
      {
        fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              hHandle->acDID, 0,0, 0, 0 );}

      // Free result set
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - bFetchDID ", 0, 0, 0, 0 );
   }

   return bResult;
}

#if 0
/*!
* \fn static tBool bFetchCID()
*  @brief Fetches the CID from Partition 0 from the XML certificate
*
*  @param  None
*  @return Returns TRUE if successful, FALSE if failed
*/
static tBool bFetchCID()
{
   tBool   bResult     = TRUE;
   rset_t* pResultSet  = OSAL_NULL;

   /* Create a result set */
   if( ( pResultSet = libminxml_get_rset( prFPvtData->hXMLCert, PARTITION_ID ) ) == OSAL_NULL )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "get_rset(PARTITION_ID) error", 0, 0, 0, 0 );
   }
   else if( bGetAttrVal( pResultSet, ATTR_CID, (tChar *)prFPvtData->au8CID ) == FALSE )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "bGetAttrVal(ATTR_CID) error", 0, 0, 0, 0 );
   }

   // Free result set
   if( pResultSet )
      libminxml_free_rset( pResultSet );

   return bResult;
}
#endif

/*!
* \fn static tBool bFetchOperatorKey()
*  @brief Fetches the Operator Key from Partition 0 from the XML certificate
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  07-Jun-2010 | Jeryn Mathew | Change to U32 storage. 
*  19-May-2011 | Anooj Gopi   | Added Handle to make the function reentrant
*/
static tBool bFetchOperatorKey( tFetchHandle hHandle )
{
   tBool  bResult       = TRUE;
   tChar  acAttrVal[10] = {0};   
   tS32   s32KeyLength  = 0;
   rset_t *pResultSet   = OSAL_NULL;

   if(hHandle)
   {
      /* Prepare result set */
      if( ( pResultSet = libminxml_get_rset( hHandle->hXMLCert, PART_OPERATOR_KEY ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchOpKey: get_rset() error", 0, 0, 0, 0 );
      }
      /* Get the Operator Key Length */
      else if( ( bResult = bGetAttrVal( pResultSet, "length", acAttrVal ) ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchOpKey: bGetAttrVal(length) error", 0, 0, 0, 0 );
      }
      /* Parse and Store Value */
      else if( ( OSAL_s32ScanFormat( acAttrVal, "%d", &s32KeyLength ) ) < 1 )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchOpKey: sscanf(Oper-Key-len) error", 0, 0, 0, 0 );
      }
      /* Get the Operator Key */
      else if( ( bResult = bGetArray( hHandle, PART_OPERATOR_KEY, &((hHandle->rOperatorKey).pu8Array),
                                      (tU16)s32KeyLength ) ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchOpKey: bGetArray(PART_OPER_KEY) error", 0, 0, 0, 0 );
      }

      /* Save the length of the key. Even if there is error */
      (hHandle->rOperatorKey).u32KeyLength = (tU32)s32KeyLength;

      /* Free the result set */
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - FetchOpKey ", 0, 0, 0, 0 );
   }
   //Return the result
   return bResult;
}

/*!
* \fn static tBool bFetchOperatorSignature()
*  @brief Fetches the Operator Key Signature from Partition 0 from the 
*         XML certificate
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  07-Jun-2010 | Jeryn Mathew | Change to U32 storage. 
*  19-May-2011 | Anooj Gopi   | Added Handle to make the function reentrant
*/
static tBool bFetchOperatorSignature( tFetchHandle hHandle )
{
   tBool  bResult       = TRUE;
   tChar  acAttrVal[10] = {0};
   tS32   s32KeyLength  = 0;
   rset_t *pResultSet   = OSAL_NULL;

   if(hHandle)
   {
      /* Prepare result set */
      if( ( pResultSet = libminxml_get_rset( hHandle->hXMLCert,
                                             PART_OPERATOR_SIGN_DATA ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchOpSig: get_rset() error", 0, 0, 0, 0 );
      }
      /* Get the Operator Signature Length */
      else if( ( bResult = bGetAttrVal( pResultSet, "length", acAttrVal ) ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchOpSig: bGetAttrVal(length) error", 0, 0, 0, 0 );
      }
      /* Parse and Store Value */
      else if( ( OSAL_s32ScanFormat( acAttrVal, "%d", &s32KeyLength ) ) < 1 )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchOpSig: sscanf(length) error", 0, 0, 0, 0 );
      }
      /* Get the Signature */
      else if( ( bResult = bGetArray( hHandle,
                                      PART_OPERATOR_SIGN_DATA,
                                      &((hHandle->rOperatorSignature).pu8Array),
                                      (tU16)s32KeyLength ) ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchOpSig: bGetArray() error", 0, 0, 0, 0 );
      }

      /* Save the length of the key. Even if there is error. */
      (hHandle->rOperatorSignature).u32KeyLength = (tU32)s32KeyLength;

      /* Free the result set */
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - FetchOpSign", 0, 0, 0, 0 );
   }
   //Return the result
   return bResult;
}

/*!
* \fn static tBool bFetchPartitionSignature()
*  @brief Fetches the Signature data from Partition 0 of the 
*         XML certificate
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  07-Jun-2010 | Jeryn Mathew | Change to U32 storage. 
*  19-May-2011 | Anooj Gopi   | Added Handle to make the function reentrant
*/
static tBool bFetchPartitionSignature( tFetchHandle hHandle )
{
   tBool  bResult       = TRUE;   
   tChar  acAttrVal[10] = {0};
   tS32   s32KeyLength  = 0;
   rset_t *pResultSet   = OSAL_NULL;

   if(hHandle)
   {
      /* Prepare result set */
      if( ( pResultSet = libminxml_get_rset( hHandle->hXMLCert,
                                             PARTITION_SIGNATURE_DATA ) ) == OSAL_NULL )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchPS: get_rset() error", 0, 0, 0, 0 );
      }
      /* Get the Partition's Signature Key Length */
      else if( ( bResult = bGetAttrVal( pResultSet, "length", acAttrVal ) ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchPS: bGetAttrVal(length) error", 0, 0, 0, 0 );
      }
      /* Parse and Store Value */
      else if( ( OSAL_s32ScanFormat( acAttrVal, "%d", &s32KeyLength ) ) < 1 )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchPS: sscanf(length) error", 0, 0, 0, 0 );
      }
      /* Get the Signature */
      else if( ( bResult = bGetArray( hHandle,
                                    PARTITION_SIGNATURE_DATA,
                                    &((hHandle->rPartitionSignature).pu8Array),
                                    (tU16)s32KeyLength ) ) == FALSE )
      {
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "FetchPS: bGetArray() error", 0, 0, 0, 0 );
      }

      /* Save the length of the key. Even if there is error. */
      (hHandle->rPartitionSignature).u32KeyLength = (tU32)s32KeyLength;

      /* Free the result set */
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - FetchPartSign ", 0, 0, 0, 0 );
   }
   //Return the result
   return bResult;
}

/*!
* \fn static tBool bGetFilelength( const tChar* pFilename, tPU32 pu32Filelength )
*  @brief  Returns the file-length of the given file.
*  
*  @param  tChar* pFilename [in]      - Path to the file.
*  @param  tPU16 pu32Filelength [out] - location to store file-Length
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  07-Jun-2010 | Jeryn Mathew | Change to U32 storage. 
*/
static tBool bGetFilelength( const tChar* pFilename, tPU32 pu32Filelength )
{
   tBool              bResult      = TRUE;
   tS32               s32FileSize  = OSAL_ERROR;
   OSAL_tIODescriptor hHashFile;
   char NewName[256];

   memset(NewName,0,256);
   if(!strncmp("/dev/cryptcard/CRYPTNAV/",pFilename,strlen("/dev/cryptcard/CRYPTNAV/")))
   {
      /*wrong path*/
      strncpy(NewName,"/dev/cryptcard/",strlen("/dev/cryptcard/"));
      strcat(NewName,pFilename+strlen("/dev/cryptcard/CRYPTNAV/"));
   }
   else
   {
      strncpy(NewName,pFilename,strlen(pFilename));
   }

   if( ( hHashFile = OSAL_IOOpen( NewName, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file_open error", 0, 0, 0, 0 );
   }
   else if( ( s32FileSize = OSALUTIL_s32FGetSize( hHashFile ) ) == OSAL_ERROR )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file FGetSize() error", 0, 0, 0, 0 );
   }
   else if( OSAL_s32IOClose( hHashFile ) == OSAL_ERROR )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file close() error", 0, 0, 0, 0 );
   }
   
   if ( s32FileSize != OSAL_ERROR )
      *pu32Filelength = (tU32)s32FileSize;
   
   return bResult;
}

/*!
* \fn static tBool bReadFromFile( const tChar* pFilename, tU32 u32Offset, tU32 u32DataLen, tPS8 ps8Buffer )
*  @brief Reads the requested data from file and stores in buffer provided
*  
*  @param  tChar* pFilename [in] - Path to the file.
*  @param  tU32 u32Offset [in]   - Offset of the File, from where the reads 
*                                  should begin
*  @param  tU32 u32DataLen [in]  - Num of bytes to read
*  @param  tPS8 ps8Buffer [out]  - Buffer to store the result in.
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  07-Jun-2010 | Jeryn Mathew | Change to U32 storage. 
*/
static tBool bReadFromFile( const tChar* pFilename, tU32 u32Offset, tU32 u32DataLen, tPS8 ps8Buffer )
{
   tBool              bResult   = TRUE;
   OSAL_tIODescriptor hHashFile;
   char NewName[256];

   memset(NewName,0,256);
   if(!strncmp("/dev/cryptcard/CRYPTNAV/",pFilename,strlen("/dev/cryptcard/CRYPTNAV/")))
   {
      /*wrong path*/
      strncpy(NewName,"/dev/cryptcard/",strlen("/dev/cryptcard/"));
      strcat(NewName,pFilename+strlen("/dev/cryptcard/CRYPTNAV/"));
   }
   else
   {
      strncpy(NewName,pFilename,strlen(pFilename));
   }

   if( ( hHashFile = OSAL_IOOpen( NewName, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file open() error", 0, 0, 0, 0 );
   }
   else if( ( OSALUTIL_s32FSeek( hHashFile,
                                 (tS32)u32Offset,
                                 OSAL_C_S32_SEEK_SET ) ) == OSAL_ERROR  )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file seek() error", 0, 0, 0, 0 );
   }
   else if( ( OSAL_s32IORead( hHashFile, ps8Buffer, u32DataLen ) ) < 
                                                              (tS32)u32DataLen )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file read() error", 0, 0, 0, 0 );
   }
   else if( OSAL_s32IOClose( hHashFile ) == OSAL_ERROR )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file close() error", 0, 0, 0, 0 );
   }

   return bResult;
}

/*!
* \fn static tBool bFetchFileData()
*  @brief Fetches the various data from file to be hashed, and store it.
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*
*  History:
*  07-Jun-2010 | Jeryn Mathew | Change to U32 storage. 
*  21-Jun-2010 | Jeryn Mathew | Error handling if offset/numofbytes are -ve.
*  19-Mar-2011 | Anooj Gopi   | Added multi device support
*  19-May-2011 | Anooj Gopi   | Added Handle to make the function reentrant
*/
static tBool bFetchFileData( tFetchHandle hHandle )
{
   tBool       bResult         = TRUE;
   rset_t*     pResultSet      = OSAL_NULL;
   tU32        u32Offset       = 0;
   tChar       acFilename[255] = {0};
   tU16        u16Index        = 0;   
   tChar       acAttrVal[255]  = {0};
   tS32        s32TempVal      = 0;
   tU32        u32Filelength   = 0;
   tU16        u16Loop         = 0;

   if(hHandle)
   {
      pResultSet = libminxml_get_rset( hHandle->hXMLCert, PARTITION_FILEDATA_FILELIST );
      if( pResultSet )
      {
         /* Prepare data structure */
         (hHandle->rFileList).u16ListLength = u16CountEntries( hHandle, PARTITION_FILEDATA_FILELIST );
         if( ( (hHandle->rFileList).pFileList = (trFileData*)OSAL_pvMemoryAllocate(
                        (hHandle->rFileList).u16ListLength * sizeof(trFileData) ) ) == OSAL_NULL )
         {
            bResult = FALSE;
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                                 TR_CRYPT_MSG_ERROR, "FetchFD: malloc(list) error",
                                 0, 0, 0, 0 );
         }

         /* Clear buffer before proceeding */
         for( u16Index = 0; u16Index < (hHandle->rFileList).u16ListLength; u16Index++ )
         {
            OSAL_pvMemorySet( (hHandle->rFileList).pFileList[u16Index].acFilename, 0, 255 );
            (hHandle->rFileList).pFileList[u16Index].u32NumOfBytes       = 0;
            (hHandle->rFileList).pFileList[u16Index].u32Offset           = 0;
            (hHandle->rFileList).pFileList[u16Index].rArray.pu8Array     = OSAL_NULL; //Extremely important
            (hHandle->rFileList).pFileList[u16Index].rArray.u32KeyLength = 0;
         }
         u16Index = 0;

         /* Repeat till no more file_entries are present */
         while( u16Index < hHandle->rFileList.u16ListLength )
         {
            /* Get Offset */
            if( bGetAttrVal( pResultSet, ATTR_FILEDATA_OFFSET, acAttrVal ) == FALSE )
            {
               bResult = FALSE;
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                                    TR_CRYPT_MSG_ERROR, 
                                    "FetchFD: bGetAttrVal(FILE_OFFSET) error", 0, 0, 0, 0 );
               break;
            }
            else
            {
               if( ( OSAL_s32ScanFormat( acAttrVal, "%d", &s32TempVal ) ) < 1 )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_ERROR,
                                       "FetchFD: sscanf(FILE_OFFSET) error", 0, 0, 0, 0 );
                  break;
               }

               /*Error handling: If XML file had -ve values, throw error and quit*/
               if( s32TempVal < 0 )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_ERROR, "FetchFD: Offset is -ve. Error.",
                                       0, 0, 0, 0 );
                  break;
               }

               u32Offset = (tU32)s32TempVal;
               (hHandle->rFileList).pFileList[u16Index].u32Offset = (tU32)s32TempVal;
               s32TempVal = 0;

               if( ( OSAL_pvMemorySet( (tPChar)acAttrVal, 0,
                                       strlen( acAttrVal ) ) ) == OSAL_NULL )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_ERROR,
                                       "FetchFD: memset(FILE_OFFSET) error", 0, 0, 0, 0 );
                  break;
               }
            }
            
            /* Get Num of bytes to read */
            if( bGetAttrVal( pResultSet, ATTR_FILEDATA_LEN, acAttrVal ) == FALSE )
            {
               bResult = FALSE;
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                                    TR_CRYPT_MSG_ERROR,
                                    "FetchFD: bGetAttrVal(FILE_LEN) error", 0, 0, 0, 0 );
               break;
            }
            else
            {
               if( ( OSAL_s32ScanFormat( acAttrVal, "%d", &s32TempVal ) ) < 1 )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_ERROR,
                                       "FetchFD: sscanf(FILE_LEN) error", 0, 0, 0, 0 );
                  break;
               }

               /*Error handling: If XML file had -ve values, throw error and quit*/
               if( s32TempVal < 0 )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_ERROR,
                                       "FetchFD: NumofBytes is -ve. Error.", 0, 0, 0, 0 );
                  break;
               }

               //Save the num of bytes to read
               (hHandle->rFileList).pFileList[u16Index].rArray.u32KeyLength = (tU32)s32TempVal;
               (hHandle->rFileList).pFileList[u16Index].u32NumOfBytes = (tU32)s32TempVal;
               s32TempVal = 0;

               if( ( OSAL_pvMemorySet( (tPChar)acAttrVal, 0,
                                        strlen( acAttrVal ) ) ) == OSAL_NULL )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_ERROR,
                                       "FetchFD: memset(FILE_LEN) error", 0, 0, 0, 0 );
                  break;
               }
            }
            
            /* Get filename */
            if( bGetAttrVal( pResultSet, ATTR_FILEDATA_FILENAME, acAttrVal ) == FALSE )
            {
               bResult = FALSE;
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                                    TR_CRYPT_MSG_ERROR,
                                    "FetchFD: bGetAttrVal(FILENAME) error", 0, 0, 0, 0 );
               break;
            }
            else
            {
               //Clear the filename buffer.
               if( ( OSAL_pvMemorySet( (tPChar)acFilename, 0,
                                        strlen( acFilename ) ) ) == OSAL_NULL )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_ERROR,
                                       "FetchFD: memset(FILENAME) error", 0, 0, 0, 0 );
                  break;
               }
               // Prefix with the path of the card.
               (tVoid)OSAL_szStringCopy( acFilename, hHandle->acDevName );

               // Concatenate the filename to the card-path
               if( ( OSAL_s32ScanFormat( acAttrVal, "%s",
                                       (acFilename + strlen(hHandle->acDevName) ) ) < 1 ) )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_ERROR,
                                       "FetchFD: sscanf(FILENAME) error", 0, 0, 0, 0 );
                  break;
               }

               // Correct for '\' in file-name. Change it to '/'
               for( u16Loop = 0; u16Loop < strlen( acFilename ); u16Loop++ )
               {
                  if( acFilename[u16Loop] == '\\' )
                  {
                     acFilename[u16Loop] = '/';
                  }
               }

               // Save the path. Not the full path, but the path as per XML certificate
               (tVoid)OSAL_szStringCopy( (hHandle->rFileList).pFileList[u16Index].acFilename,
                                          acAttrVal );

               if( ( OSAL_pvMemorySet( (tPChar)acAttrVal, 0,
                                        strlen( acAttrVal ) ) ) == OSAL_NULL )
               {
                  bResult = FALSE;
                  fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                       TR_CRYPT_MSG_ERROR,
                                       "FetchFD: memset(0) error", 0, 0, 0, 0 );
                  break;
               }
            }
            
            /* Special Case: if NumOfBytes = 0, then read whole file.*/
            if( (hHandle->rFileList).pFileList[u16Index].rArray.u32KeyLength == 0 )
            {
               if( bGetFilelength( acFilename, &u32Filelength ) )
               {
                  (hHandle->rFileList).pFileList[u16Index].rArray.u32KeyLength = u32Filelength - u32Offset;
               }
               else
               {
                  bResult = FALSE;
                  break;
               }
            }
            
            /* Allocate buffer to read from file */
            if( ( (hHandle->rFileList).pFileList[u16Index].rArray.pu8Array =
                  (tPU8)OSAL_pvMemoryAllocate( (hHandle->rFileList).pFileList[u16Index].rArray.u32KeyLength ) ) == OSAL_NULL )
            {
               bResult = FALSE;
               break;
            }
            /* Clear buffer */
            if( ( OSAL_pvMemorySet( (hHandle->rFileList).pFileList[u16Index].rArray.pu8Array,
                                    0,
                                    (hHandle->rFileList).pFileList[u16Index].rArray.u32KeyLength )
                                   ) == OSAL_NULL )
            {
               bResult = FALSE;
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                                    TR_CRYPT_MSG_ERROR,
                                    "FetchFD: memset(pu8array) error", 0, 0, 0, 0 );
               break;
            }
            /* Read the data from file. */
            else if( ( bReadFromFile( acFilename, u32Offset,
                               (hHandle->rFileList).pFileList[u16Index].rArray.u32KeyLength,
                               (tPS8)(hHandle->rFileList).pFileList[u16Index].rArray.pu8Array
                                    ) ) == FALSE )
            {
               bResult = FALSE;
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                                    TR_CRYPT_MSG_ERROR,
                                    "FetchFD: read from file error", 0, 0, 0, 0 );
               break;
            }
            
            /* Get the next element along the same branch */
            libminxml_get_next_element( pResultSet );
            
            /* Goto Next Row */
            u16Index++;
         }
      }
      else
      {
         //Result set is invalid
         bResult = FALSE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "FetchFD: get_rset(PARTITION_FILEDATA_FILELIST) error", 0, 0, 0, 0 );
      }

      //Free the result set.
      if( pResultSet )
         libminxml_free_rset( pResultSet );
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - FetchFileData ", 0, 0, 0, 0 );
      bResult = FALSE;
   }

   return bResult;
}

/*!
* \fn tBool bInitFetch( tPCChar pcCertPath )
*  @brief  Creates the XML tree in memory by parsing the XML file.
*  
*  @param  : tPChar pcCertPath - Path to the XML Certificate.
*  @return : - Handle on success
*            - OSAL_NULL on failure
*  History:
*  18-Mar-2011 | Anooj Gopi | Edited to Add multi device support
*  19-May-2011 | Anooj Gopi | Added Handle to make it reentrant
*/
tFetchHandle bInitFetch( tCString coszCertPath )
{
   tBool bResult = TRUE;
   tFetchHandle hHandle;

   hHandle = OSAL_pvMemoryAllocate( sizeof(*hHandle));

   if(hHandle)
   {
      /* Clear the handle memory allocated */
      OSAL_pvMemorySet( (tPVoid) hHandle, 0, sizeof(*hHandle));

      /* Extract the device name from certificate */
      if(!bGetDeviceName(hHandle->acDevName, coszCertPath))
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "InitFetch: GetDeviceName failed", 0, 0, 0, 0 );
         bResult = FALSE;
      }

      if( ( hHandle->hXMLCert  = libminxml_open( coszCertPath ) ) == OSAL_NULL )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "InitFetch: libminxml_open error", 0, 0, 0, 0 );
         bResult = FALSE;
      }

      //1. Revocation List
      (hHandle->rRevocList).pList             = OSAL_NULL;
      //2. Revocation Signature
      (hHandle->rRevocSign).pu8Array          = OSAL_NULL;
      //3. Operator Key
      (hHandle->rOperatorKey).pu8Array        = OSAL_NULL;
      //4. Operator Signature
      (hHandle->rOperatorSignature).pu8Array  = OSAL_NULL;
      //5. Partition Signature
      (hHandle->rPartitionSignature).pu8Array = OSAL_NULL;
      //6. File List
      (hHandle->rFileList).pFileList = OSAL_NULL;
      //7. Lifetime
      //8. Strings
      //
      hHandle->bIsRevocListPresent = TRUE;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "InitFetch: malloc for handle failed", 0, 0, 0, 0 );
      bResult = FALSE;
   }
   
   /* Check whether we got here with any error */
   if(!bResult)
   {
      if( hHandle )
      {
         /* Free all the allocated resources */
         if(hHandle->hXMLCert)
            libminxml_close(hHandle->hXMLCert);

         OSAL_vMemoryFree( hHandle );
         hHandle = OSAL_NULL;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                             "InitFetch: Freeing memory for handle", 0, 0, 0, 0 );
      }
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_SUCCESS,
                           "InitFetch Succeeded", 0, 0, 0, 0 );
   }

   return hHandle;
}

/*!
* \fn tBool bPerformFetch()
*  @brief  Performs the fetch operations by reading all the relevant content 
*          from the Tree.
*  
*  @param  hHandle - Fetch layer handle
*  @return Returns TRUE if successful, FALSE if failed
*/
tBool bPerformFetch( tFetchHandle hHandle )
{
   tBool bResult = FALSE;
   tenSignVerifyTrace enFetchStatus = SIGN_VER_XML_FETCH_NO_ERROR;

   if(!hHandle)
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle in PerfFetch", 0, 0, 0, 0 );
   }
   else
   {
      /* Fetch the Certificate Lifetime */
      if( bFetchLifetime( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Lifetime fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_LIFETIME;
      }
      /* Fetch the revocation list timestamp */
      else if( bFetchRevocTimestamp( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Revoc. timestamp fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_REVOC_TS;
      }
      /* Fetch the revocation list */
      else if( bFetchRevocationList( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Revoc. list fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_REVOC_LIST;
      }
      /* Fetch the revocation signature */
      else if( bFetchRevocationSignature( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Revocation Signature Fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_REVOC_SIGN;
      }
      /* Fetch the Operator Key */
      else if( bFetchOperatorKey( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Operator Key Fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_OP_KEY;
      }
      /* Fetch the Operator signature */
      else if( bFetchOperatorSignature( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Operator Key Signature Fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_OP_SIGN;
      }
      /* Fetch the VIN from Certificate */
      else if( bFetchVIN( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "VIN fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_VIN;
      }
        /* Fetch the DID from Certificate */
      else if( bFetchDID( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "DID fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_DID;
      }
      /* Fetch the Partition signature */
      else if( bFetchPartitionSignature( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Partition Signature Fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_PART_SIGN;
      }
      /* Fetch the File Data */
      else if( bFetchFileData( hHandle ) == FALSE )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "FileData Fetch failed", 0, 0, 0, 0 );
         enFetchStatus = ERROR_SIGN_VER_FETCH_FILE_DATA;
      }
      else
      {
         /* Mark that perform fetch succeeded */
         enFetchStatus = SIGN_VER_XML_FETCH_NO_ERROR;
         bResult = TRUE;
      }

      /* Close the XML parser */
      libminxml_close( hHandle->hXMLCert );
   }
   
   if( bResult )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "PerformFetch succeeded", 0, 0, 0, 0 );
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "PerformFetch failed", 0, 0, 0, 0 );
   }

   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enFetchStatus);
   return bResult;
}

/*!
* \fn tVoid vCloseFetch()
*  @brief  Frees the Memory Allocated for each data.
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return None
*
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
tVoid vCloseFetch( tFetchHandle hHandle )
{
   tU16 u16Row = 0;

   if(hHandle)
   {
      /* Free the Memory of all data structures */
      if( (hHandle->rRevocSign).pu8Array != OSAL_NULL )
      {
         OSAL_vMemoryFree( (hHandle->rRevocSign).pu8Array );
         (hHandle->rRevocSign).pu8Array = OSAL_NULL;
      }

      if( (hHandle->rOperatorKey).pu8Array != OSAL_NULL )
      {
         OSAL_vMemoryFree( (hHandle->rOperatorKey).pu8Array );
         (hHandle->rOperatorKey).pu8Array = OSAL_NULL;
      }
   
      if( (hHandle->rOperatorSignature.pu8Array) != OSAL_NULL )
      {
         OSAL_vMemoryFree( (hHandle->rOperatorSignature).pu8Array );
         (hHandle->rOperatorSignature).pu8Array = OSAL_NULL;
      }

      if( (hHandle->rPartitionSignature).pu8Array != OSAL_NULL )
      {
         OSAL_vMemoryFree( (hHandle->rPartitionSignature).pu8Array );
         (hHandle->rPartitionSignature).pu8Array = OSAL_NULL;
      }
   
      for( u16Row = 0; u16Row < hHandle->rRevocList.u16ListLength; u16Row++ )
      {
         if( (hHandle->rRevocList).pList[u16Row].pu8Array != OSAL_NULL )
         {
            OSAL_vMemoryFree( (hHandle->rRevocList).pList[u16Row].pu8Array );
            (hHandle->rRevocList).pList[u16Row].pu8Array = OSAL_NULL;
         }
      }
      if( hHandle->rRevocList.pList != OSAL_NULL )
      {
         OSAL_vMemoryFree( (hHandle->rRevocList).pList );
         (hHandle->rRevocList).pList = OSAL_NULL;
      }

      for( u16Row = 0; u16Row < (hHandle->rFileList).u16ListLength; u16Row++ )
      {
         if( (hHandle->rFileList).pFileList[u16Row].rArray.pu8Array != OSAL_NULL )
         {
            OSAL_vMemoryFree( (hHandle->rFileList).pFileList[u16Row].rArray.pu8Array );
            (hHandle->rFileList).pFileList[u16Row].rArray.pu8Array = OSAL_NULL;
         }
      }
      if( (hHandle->rFileList).pFileList != OSAL_NULL )
      {
         OSAL_vMemoryFree( (hHandle->rFileList).pFileList );
         (hHandle->rFileList).pFileList = OSAL_NULL;
      }

      OSAL_vMemoryFree( hHandle );
   }
   
   return;
}

/*!
* \fn OSAL_trTimeDate trGetCertificateLifetime()
*  @brief  Returns Partition "end" timestamp value
*  
*  @param  hHandle [in] - Fetch layer handle
*          prTimeDate [out] - Partition "end" time stamp value
*  @return TRUE - success, FAIL - failed
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
tBool trGetCertificateLifetime( tcFetchHandle hHandle, OSAL_trTimeDate *prTimeDate )
{
   tBool bResult;

   if( hHandle )
   {
      *prTimeDate = hHandle->rLifeTime;
      bResult = TRUE;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetCertLT ", 0, 0, 0, 0 );
      bResult = FALSE;
   }

   return bResult;
}

/*!
* \fn OSAL_trTimeDate trGetCertLifetimeStart()
*  @brief  Returns Partition "start" timestamp value
*  
*  @param  hHandle [in] - Fetch layer handle
*          prTimeDate [out] - Partition "start" time stamp value
*  @return TRUE - success, FAIL - failed
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
tBool trGetCertLifetimeStart( tcFetchHandle hHandle, OSAL_trTimeDate *prTimeDate )
{
   tBool bResult;

   if( hHandle )
   {
      *prTimeDate = hHandle->rLifeTimeStart;
      bResult = TRUE;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetCertLTS ", 0, 0, 0, 0 );
      bResult = FALSE;
   }

   return bResult;
}

/*!
* \fn OSAL_trTimeDate trGetRevocTimestamp()
*  @brief  Returns revocation timestamp value
*  
*  @param  hHandle [in] - Fetch layer handle
*          prTimeDate [out] - Partition "start" timestamp value
*  @return TRUE - success, FAIL - failed
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
tBool trGetRevocTimestamp( tcFetchHandle hHandle, OSAL_trTimeDate *prTimeDate )
{
   tBool bResult;

   if( hHandle )
   {
      *prTimeDate = hHandle->rRevocTimestamp;
      bResult = TRUE;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetRevocTS ", 0, 0, 0, 0 );
      bResult = FALSE;
   }

   return bResult;
}

/*!
* \fn trList* trGetRevocationList()
*  @brief  Returns pointer to revocation list.
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to revocation time stamp value on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
trList* trGetRevocationList( tFetchHandle hHandle )
{
   trList* prRevocList = OSAL_NULL;

   if(hHandle)
   {
      if( hHandle->bIsRevocListPresent )
         prRevocList = &(hHandle->rRevocList);
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetRevoList ", 0, 0, 0, 0 );
   }

   return prRevocList;
}

/*!
* \fn trArray* trGetRevocationSignature()
*  @brief  Returns pointer to revocation signature.
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to revocation signature on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
trArray* trGetRevocationSignature( tFetchHandle hHandle )
{
   trArray* prRevocSign = OSAL_NULL;

   if(hHandle)
   {
      prRevocSign = &(hHandle->rRevocSign);
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetRevoSign ", 0, 0, 0, 0 );
   }

   return prRevocSign;
}

/*!
* \fn trArray* trGetOperatorKey()
*  @brief  Returns pointer to Operator Key
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to Operator Key on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
trArray* trGetOperatorKey( tFetchHandle hHandle )
{
   trArray* prOperatorKey;

   if(hHandle)
   {
      prOperatorKey = &(hHandle->rOperatorKey);
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetOperKey ", 0, 0, 0, 0 );
      prOperatorKey = OSAL_NULL;
   }

   return prOperatorKey;
}

/*!
* \fn trArray* trGetOperatorSignature()
*  @brief  Returns pointer to Operator Signature
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to Operator Signature on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
trArray* trGetOperatorSignature( tFetchHandle hHandle )
{
   trArray* prOperatorSign;

   if(hHandle)
   {
      prOperatorSign = &(hHandle->rOperatorSignature);
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetOperSig ", 0, 0, 0, 0 );
      prOperatorSign = OSAL_NULL;
   }

   return prOperatorSign;
}

/*!
* \fn trArray* trGetPartSignature()
*  @brief  Returns pointer to Partition Signature
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to Partition Signature on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
trArray* trGetPartSignature( tFetchHandle hHandle )
{
   trArray* prPartitionSignature;

   if(hHandle)
   {
      prPartitionSignature = &(hHandle->rPartitionSignature);
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetPartSig ", 0, 0, 0, 0 );
      prPartitionSignature = OSAL_NULL;
   }

   return prPartitionSignature;
}

/*!
* \fn trFileList* trGetFileData()
*  @brief  Returns pointer to File Data of partition
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to File Data of partition on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
trFileList* trGetFileData( tFetchHandle hHandle )
{
   trFileList* prFileList;

   if(hHandle)
   {
      prFileList = &(hHandle->rFileList);
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetFileData ", 0, 0, 0, 0 );
      prFileList = OSAL_NULL;
   }

   return prFileList;
}

/*!
* \fn tPChar pcGetRevocTimestamp()
*  @brief  Returns pointer to Revoc Timestamp
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to Revoc Time stamp on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
tPChar pcGetRevocTimestamp( tFetchHandle hHandle )
{
   tPChar pcRevocTimestamp;

   if(hHandle)
   {
      pcRevocTimestamp = hHandle->acRevocTimestamp;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle -GetRevocTS ", 0, 0, 0, 0 );
      pcRevocTimestamp = OSAL_NULL;
   }

   return pcRevocTimestamp;
}

/*!
* \fn tPChar pcGetVIN()
*  @brief  Returns pointer to VIN from XML tree
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to VIN from XML tree on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
tPChar pcGetVIN( tFetchHandle hHandle )
{
   tPChar pcVIN;

   if(hHandle)
   {
      pcVIN = hHandle->acVIN;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - GetVIN ", 0, 0, 0, 0 );
      pcVIN = OSAL_NULL;
   }
   return pcVIN;
}
/*!
* \fn tPChar pcGetDID()
*  @brief  Returns pointer to DID from XML tree
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to DID from XML tree on success
*          OSAL_NULL on failure
*
*  History:
*  12-Mar-2014 | Swathi Bolar | DID based implementation(INNAVPF-2544)
*/
tPChar pcGetDID( tFetchHandle hHandle )
{
   tPChar pcDID;

   if(hHandle)
   {
      pcDID = hHandle->acDID;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - GetDID ", 0, 0, 0, 0 );
      pcDID = OSAL_NULL;
   }
   return pcDID;
}

/*!
* \fn tPU8 pu8GetCID()
*  @brief  Returns pointer to CID from XML tree
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to CID from XML tree on success
*          OSAL_NULL on failure
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
tPU8 pu8GetCID( tFetchHandle hHandle )
{
   tPU8 pu8CID;

   if(hHandle)
   {
      pu8CID = hHandle->au8CID;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - GetCID ", 0, 0, 0, 0 );
      pu8CID = OSAL_NULL;
   }

   return pu8CID;
}

/*!
* \fn tPChar pcGetLifetimeStart()
*  @brief  Returns pointer to Lifetime "start" value
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to Lifetime "start" value on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
tPChar pcGetLifetimeStart( tFetchHandle hHandle )
{
   tPChar pcLifetimeStart;

   if(hHandle)
   {
      pcLifetimeStart = hHandle->acLifetimeStart;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - GetLTStart ", 0, 0, 0, 0 );
      pcLifetimeStart = OSAL_NULL;
   }

   return pcLifetimeStart;
}

/*!
* \fn tPChar pcGetLifetimeEnd()
*  @brief  Returns pointer to Lifetime "end" value
*  
*  @param  hHandle [in] - Fetch layer handle
*  @return Pointer to Lifetime "end" value on success
*          OSAL_NULL on failure
*
*  History:
*  19-May-2011 | Anooj Gopi | Added Handle to make the function reentrant
*/
tPChar pcGetLifetimeEnd( tFetchHandle hHandle )
{
   tPChar pcLifetimeEnd;

   if(hHandle)
   {
      pcLifetimeEnd = hHandle->acLifetimeEnd;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - GetLTEnd ", 0, 0, 0, 0 );
      pcLifetimeEnd = OSAL_NULL;
   }

   return pcLifetimeEnd;
}

 
