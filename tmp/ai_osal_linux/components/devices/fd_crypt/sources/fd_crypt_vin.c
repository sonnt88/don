/*!
* \file  fd_crypt_vin.c
* \brief
*     The new Cryptcard verification procedure involving the use of VIN, as proposed
*  by GMGE for GMGE MY11.5 (NGA) project.
*
* \par AUTHOR
*         Jeryn Mathew (RBEI/ECF1)
* \par COPYRIGHT
*        (c) 2010 Robert Bosch Engineering and Business Solutions Ltd.
*
* \date
*  22-03-10   v1.0   Jeryn Mathew (RBEI/ECF1)   Initial Revision
*  26-04-10   v1.1   Jeryn Mathew (RBEI/ECF1)   New CRQs updated. Support for
*                                               CID, security fix for revoc-list,
*                                               hashing approach finalized.
*  25-05-10   v1.2   Jeryn Mathew (RBEI/ECF1)   New updates for VIN-XML feature.
*                                               Handling empty tags/attr.
*  07-06-10   v1.3   Jeryn Mathew (RBEI/ECF1)   Resolve known issues and fix 
*                                               large-file handling.
*  21-06-10   v1.4   Jeryn Mathew (RBEI/ECF1)   Added performance and debug traces 
*                                               Enabled VIN-Check feature.
*  14-07-10   v1.5   Jeryn Mathew (RBEI/ECF1)   Enabled certificate lifetime
*                                               check feature
*  22.10.10   v1.6   Gokulkrishnan N            VinFeature Porting for FORD-MFD
*  23.12.10   v1.7   Ravindran P      Additional functions added for Gen2
*  05.04.11   v1.8   Anooj Gopi (RBEI/ECF1)     Added multi device support
*  26.04.13   v1.9  Selvan Krishnan( (RBEI/ECF5) Trcae information changed.
*  12.03.14   v1.10  Swathi Bolar(RBEI/ECF5)    DID based implementation(INNAVPF-2544)
*  13.05.14   v1.11  Swathi Bolar(RBEI/ECF5)    DID changes for Suzuki(INNAVPF-2830)
*/

/*****************************************************************
  | includes of component-internal interfaces
  | (scope: component-local)
  |--------------------------------------------------------------*/

/* OSAL Interface */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "osioctrl.h"
//#include "prm.h"
//#include "dispatcher_defs.h"
#include "fd_crypt_trace.h"
#include "signfile_gen.h"
#include "fd_crypt_private.h"
#include "fetch_cert_data.h"


#include <sys/stat.h>

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
/* States the Boolean return values */
#ifndef TRUE
   #define TRUE                   1
#endif
#ifndef FALSE
   #define FALSE                  0
#endif
/* Crypt Signature File path for Ford MFD */
//#define XML_CERT_PATH		 "/shared/cryptnav/DATA/DATA/MISC/SDX_META.DAT"
/* Length of ECC public key */
#ifndef ECC_PUB_KEY_LEN
   #define ECC_PUB_KEY_LEN 		  (40)
#endif
/* Entry number of ECC public key in KDS */

/* Location of Revoc List in Flash Mem. */
#define REVOC_LIST_PATH         "/dev/ffs/REVOC_LIST.DAT"

/* Development feature. If target does not have correct VIN, comment line below */
//#define VIN_IN_KDS              1
/* Length of VIN entry */
#define VIN_ENTRY_LEN 		     (40)
#ifdef VIN_IN_KDS
   /* Entry number of VIN in KDS */
   #define VIN_ENTRY	              (0x0114)
#endif
/* Development feature. If target does not have correct DID, comment line below */
#define DID_IN_KDS              1
/*(INNAVPF-2830)The length and Entry for DID in KDS modified as per Suzuki request */
/* Length of DID entry */
#define DID_ENTRY_LEN 		     (17)
#ifdef DID_IN_KDS
   /* Entry number of DID in KDS */
   #define DID_ENTRY	             (0x0D62)
#endif

/* Development feature. The target uses GPS time to compare against the 
   certificate's UTC time. If the GPS time is invalid, then the verification
   will fail. Uncomment this line, for actual release.
*/
#define ENABLE_CERT_LIFETIME_CHECK  0 //commented for Gen2
#ifdef ENABLE_CERT_LIFETIME_CHECK
   /* Base value for the offset provided by RTC clock */
   #define OSAL_TIME_BASE_YEAR          1900
   /* Default offset provided for initializing buffer - 2008 */
   #define RTC_OSAL_OFFSET_DEFAULT_YEAR 108
#endif


//t_CryptVINPathHeader_Setting* pvinHeaderData;



/* Debug traces for hashing data. To enable uncomment below #define line */
//#define _DEBUG_TRACE_

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-external)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------*/
#ifdef ENABLE_CERT_LIFETIME_CHECK
static tBool   trReadRTCTime( OSAL_trTimeDate* pCurrTimeDate );
static tBool   bVerifyCertLifetime( tcFetchHandle hHandle, tU8 u8PartitionNum );
static OSAL_trTimeDate trGetCertLifetime( tcFetchHandle hHandle, tU8 u8PartitionNum );
#endif
static tBool   bVerifyRevocTimestamp( tcFetchHandle hHandle, tU8 u8PartitionNum );
static tS8     s8CompareValues( const OSAL_trTimeDate* rTD1, 
                                const OSAL_trTimeDate* rTD2 );
static tPU8    pu8GetRevocSignature( tFetchHandle hHandle, tPU32 pu32SignatureLength );
static tBool   bReadRBPubKey(tFetchHandle hHandle, tPU8 pu8RBPubKey);
static tBool   bVerifyRevocSignature( tFetchHandle hHandle );
static tBool   bCopyRevocList( tFetchHandle hHandle );
static tBool   bPerformRevocListCopy( tFetchHandle hHandle );
static tPU8    pu8GetOperatorKey( tFetchHandle hHandle, tU8 u8PartitionNum, tPU32 pu32KeyLength );
static tPU8    pu8GetOperatorSignature( tFetchHandle hHandle, tU8 u8PartitionNum,
                                        tPU32 pu32SignatureLength );
static tBool   bVerifyOperatorSignature( tFetchHandle hHandle, tU8 u8PartitionNum );
static tBool   bIsKeyRevoked( tPCU8 pu8Key );
//#ifdef VIN_IN_KDS
static tBool   bReadVINfromKDS( tFetchHandle hHandle, tPU8 pu8VIN );
static tBool   bReadDIDfromKDS( tFetchHandle hHandle, tPU8 pu8DID );
static tBool   bCompareVIN( tFetchHandle hHandle );
static tBool   bCompareDID( tFetchHandle hHandle );
//#endif
static tPU8    pu8GetPartitionSignature( tFetchHandle hHandle, tU8 u8PartitionNum,
                                         tPU32 pu32PartSignLength );
static tPU8    pu8CreatePartitionHash( tFetchHandle hHandle, tU8 u8PartitionNum );
static tBool   bVerifyPartSignature( tFetchHandle hHandle, tU8 u8PartitionNum );

tBool   bFileExists( tCString s8CertPath );
tBool fd_crypt_verify_xml_signature( tCString coszCertPath );


/*****************************************************************
| function implementation (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function implementation (scope: global)
|----------------------------------------------------------------*/


// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Checks for the presence of certificate file (SDX_META.DAT or SD_META.DAT)
//!  and returns a TRUE or FALSE
//!
//! \return
//!   TRUE  - File exist.
//!   FALSE - File does not exist.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//******************************************************************************
tBool bFileExists
(
   //! (I) Path of the certificate file.
   tCString s8CertPath
)
{
   tenSignVerifyTrace enFileSt = SIGN_VER_FILE_EXIST_NO_ERROR;
   tBool bResult = TRUE;
   OSAL_tIODescriptor hCertFile;
   
   if( ( hCertFile = OSAL_IOOpen( s8CertPath, OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      bResult = FALSE;
	   enFileSt = ERROR_SIGN_VER_FILE_OPEN;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file open error", 0, 0, 0, 0 );
   }
   else if( (OSAL_s32IOClose( hCertFile )) == OSAL_ERROR )
   {
      bResult = FALSE;
	   enFileSt = ERROR_SIGN_VER_FILE_CLOSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file close error", 0, 0, 0, 0 );
   }   
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enFileSt);
   return bResult;
}
#ifdef ENABLE_CERT_LIFETIME_CHECK
// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Fetches the Lifetime value of the certificate, and returns it.
//!
//! \return
//!   OSAL_trTimeDate rResult - UTC value of Lifetime of certificate.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the function reentrant.
//******************************************************************************
static OSAL_trTimeDate trGetCertLifetime( tcFetchHandle hHandle, tU8 u8PartitionNum )
{
   OSAL_trTimeDate rTimeDate = {0};

   // The partition number will only be used in the new phase development.
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( u8PartitionNum );
   trGetCertificateLifetime( hHandle, &rTimeDate );

   return rTimeDate;
}

// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Reads the RTC device and fetches the current date-time based on the GPS.
//!  This is needed for verifying the validity of certificate lifetime.
//!
//! \return
//!   OSAL_trTimeDate rResult - UTC value of current date-time.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//******************************************************************************
static tBool trReadRTCTime( OSAL_trTimeDate* pCurrTimeDate )
{
   tBool bResult = TRUE;
   OSAL_trTimeDate rTimeDate = {0};
   OSAL_tIODescriptor hDevice = 0;
   //default initialization for RTC devices
   OSAL_trRtcRead rRTCRead ={
                             {0,0},
                             {
                               {0,0,0,1,1,RTC_OSAL_OFFSET_DEFAULT_YEAR,0,0,0},
                               OSAL_RTC_INVALID,
                               {0,0,0,1,1,RTC_OSAL_OFFSET_DEFAULT_YEAR,0,0,0},
                               OSAL_RTC_INVALID,
                               {0,0,0,1,1,RTC_OSAL_OFFSET_DEFAULT_YEAR,0,0,0},
                               OSAL_RTC_INVALID
                             }
                            };
   
   /* Read GPS time */
   rRTCRead.RegisterForRtcTime.updateRate = 0;
   rRTCRead.RegisterForRtcTime.registerForNotify = OSAL_RTC_NOTIFY_ON_GPS_TIME;

   //Verify the argument
   if( pCurrTimeDate == OSAL_NULL )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "input arg null error", 0, 0, 0, 0 );
   }
   //Open the device
   else if( ( hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_RTC,
                                OSAL_EN_READWRITE ) ) == OSAL_ERROR )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "rtc dev open error", 0, 0, 0, 0 );
   }
   //read the RTC value.
   else if( (tU32)OSAL_s32IORead( hDevice, 
                                  (tS8 *)&rRTCRead, 
                                  sizeof( rRTCRead ) ) != sizeof( rRTCRead ) )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "rtc dev read error", 0, 0, 0, 0 );
   }
   //close the device
   else if( OSAL_s32IOClose( hDevice ) == OSAL_ERROR )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "rtc dev close error", 0, 0, 0, 0 );
   }
   //check if the time is valid
   else if( rRTCRead.RtcTimes.gpsUtcTimeState == OSAL_RTC_INVALID )
   {
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "rtc time invalid", 0, 0, 0, 0 );
   }
   else
   {
      rTimeDate = rRTCRead.RtcTimes.gpsUtcTime;
      rTimeDate.s32Year += OSAL_TIME_BASE_YEAR;

      //copy the result
      *pCurrTimeDate = rTimeDate;
   }

   return bResult;
}
#endif
// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Compares 2 Date-Time values and returns if they are same, greater or lesser.
//!
//! \return
//!   0 - Same values
//!   1 - TD1 > TD2
//!  -1 - TD1 < TD2
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//******************************************************************************
static tS8 s8CompareValues
(
   //! (I) First Date Time value
   const OSAL_trTimeDate* rTD1,
   //! (I) Second Date Time value
   const OSAL_trTimeDate* rTD2
)
{
   tS8  s8Result          = 0; //Default State: Timestamps are same.
   tU8  u8Index;
   tS32 as32TimeStamp1[6] = {0};
   tS32 as32TimeStamp2[6] = {0};

   /* The TimeStamp arrays will hold the OSAL_trTimeDate in the following format
    * [Year, Month, Day, Hour, Minute, Second]
    *   0      1     2    3      4       5
    */

   if( rTD1 != OSAL_NULL )
   {
      as32TimeStamp1[0] = rTD1->s32Year;
      as32TimeStamp1[1] = rTD1->s32Month;
      as32TimeStamp1[2] = rTD1->s32Day;
      as32TimeStamp1[3] = rTD1->s32Hour;
      as32TimeStamp1[4] = rTD1->s32Minute;
      as32TimeStamp1[5] = rTD1->s32Second;
   }

   if( rTD2 != OSAL_NULL )
   {
      as32TimeStamp2[0] = rTD2->s32Year;
      as32TimeStamp2[1] = rTD2->s32Month;
      as32TimeStamp2[2] = rTD2->s32Day;
      as32TimeStamp2[3] = rTD2->s32Hour;
      as32TimeStamp2[4] = rTD2->s32Minute;
      as32TimeStamp2[5] = rTD2->s32Second;
   }

   for( u8Index = 0; u8Index < 6; u8Index++ )
   {
      if( as32TimeStamp1[u8Index] > as32TimeStamp2[u8Index] )
      {
         s8Result = 1;
         break;
      }
      else if( as32TimeStamp1[u8Index] < as32TimeStamp2[u8Index] )
      {
         s8Result = -1;
         break;
      }
   }

   return s8Result;
}

// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Fetches the revocation signature data from the certificate file.
//!
//! \return
//!   tPU8 pu8RevocSignData - Signature data
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  07 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Change to U32 storage.
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//******************************************************************************
static tPU8 pu8GetRevocSignature
(
   //! (I) Fd Crypt Fetch Handle
   tFetchHandle hHandle,
   //! (O) Length of the data
   tPU32 pu32SignatureLength
)
{
   tPU8     pu8RevocSign = OSAL_NULL;
   trArray  *pSignPtr;

   /* Read the entries */
   pSignPtr = trGetRevocationSignature( hHandle );

   if(pSignPtr && pu32SignatureLength)
   {
      pu8RevocSign = pSignPtr->pu8Array;

      /* Return the pointer and the length */
      *pu32SignatureLength = pSignPtr->u32KeyLength;
   }

   if(!pu8RevocSign)
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "ERR GetRevocSignature failed", 0, 0, 0, 0 );
   }

   return pu8RevocSign;
}

// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Read the RB Public Key from KDS. Key length will be known and hard-coded
//!
//! \return
//!   RB Public Key. tPU8 Type
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle
//******************************************************************************
static tBool bReadRBPubKey(tFetchHandle hHandle, tPU8 pu8RBPubKey)
{
   tBool bResult = FALSE;

   if(prFdCryptPvtData)
   {
      /* Public key is common for all device, copy the key from global buffer */
      OSAL_pvMemoryCopy( pu8RBPubKey, prFdCryptPvtData->au8RBPublicKey, ECC_PUB_KEY_LEN );
      bResult = TRUE;
   }

   /* Handle will be useful when we need more than one public key */
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(hHandle);
   return bResult;
}

// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Verifies the Revocation Signature specified in certificate file.
//!  Signature data from Revocation list is composed of complete 1-D array of
//!  keys, signed against a RB private key.
//!
//! \return
//!   TRUE  - Verification Success
//!   FALSE - Verification Failed
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  24 - May - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Support for empty revoc list
//  07 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Change to U32 storage.
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//******************************************************************************
static tBool bVerifyRevocSignature( tFetchHandle hHandle )
{
   tBool   bResult             = TRUE;
   tPU8    pu8SignatureData;
   tU32    u32SignatureLength  = 0;
   tU16    u16Row              = 0;
   trList* prRevocList         = OSAL_NULL;
   tU8     au8Timestamp[TIMESTAMP_LEN-1] = {0};
   tPChar  pcTimestamp         = OSAL_NULL;
   tU32    au32HashValue[5]    = {0};
   tU8     au8HashVal[20]      = {0};
   tsSHA1Context  sha_ctx;
   tenSignVerifyTrace enVerRSStatus = SIGN_VER_REVOC_SIGN_NO_ERROR;
   tS32 s32RetVal = 0;
   tU8 au8RBPubKey[ECC_PUB_KEY_LEN] = {0};

   /* Get Signature Data */
   if( ( pu8SignatureData = pu8GetRevocSignature( hHandle,
                                         &u32SignatureLength ) ) == OSAL_NULL )
   {
      //Reading signature data failed.
      bResult = FALSE;
      enVerRSStatus = ERROR_SIGN_VER_REVOC_SIGN_READ;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "GetRevocSign() error", 0, 0, 0, 0 );
   }
   /* Get the RB Public Key */
   else if( bReadRBPubKey( hHandle, au8RBPubKey ) == FALSE )
   {
      /* Error fetching RB public Key */
      bResult = FALSE;
      enVerRSStatus = ERROR_SIGN_VER_RB_PUBKEY_READ;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "ReadRBPubKey() error", 0, 0, 0, 0 );
   }
   /* Get the Revoked List Timestamp */
   else if( ( pcTimestamp = pcGetRevocTimestamp( hHandle ) ) == OSAL_NULL )
   {
      /* Error fetching revocation List timestamp */
      bResult = FALSE;
      enVerRSStatus = ERROR_SIGN_VER_REVOC_LIST_TS_FETCH;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "GetRevocTimestamp() error", 0, 0, 0, 0 );
   }
   else
   {
      /* Update for vin-xml feature: NULL revoc list is NOT an error. */
      /* Get revocation list. */
      if( ( prRevocList = trGetRevocationList( hHandle ) ) == OSAL_NULL )
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "Empty Revoc List", 0, 0, 0, 0 );
      }

      //Prepare the time stamp for hashing
      //This step copies the time stamp value WITHOUT the null-character
      for( u16Row = 0; u16Row < TIMESTAMP_LEN-1; u16Row++ )
      {
         au8Timestamp[u16Row] = (tU8)pcTimestamp[u16Row];
      }
      
      /* Lock the non-reentrant BPCL function */
      FD_Crypt_vEnterBPCLCriticalSec();

      // Prepare Hash
      BPCL_SHA1_Init( &sha_ctx );
      
      //Hash the time stamp.
      BPCL_SHA1_Update( &sha_ctx, au8Timestamp, TIMESTAMP_LEN-1 );
      
      //Hash Revoked Keys Data
      if ( prRevocList )
      {
         for( u16Row = 0; u16Row < prRevocList->u16ListLength; u16Row++ )
         {
            BPCL_SHA1_Update( &sha_ctx, 
                              (prRevocList->pList)[u16Row].pu8Array,
                              (prRevocList->pList)[u16Row].u32KeyLength );
         }
      }

      //Prepare the hash value
      BPCL_SHA1_Finish( &sha_ctx, au32HashValue );

      // Convert result to platform independence and sign
      for( u16Row = 0; u16Row < 5; ++u16Row )
      {
         M_U32_TO_BUF(au32HashValue[u16Row], au8HashVal, (4*u16Row));
      }

      /* Conduct verification */
      s32RetVal = (tS32) BPCL_EC_DSA_Verify( au8HashVal, au8RBPubKey,
                                    pu8SignatureData );

      /* Release BPCL Lock */
      FD_Crypt_vLeaveBPCLCriticalSec();

      if( s32RetVal ==  BPCL_OK )
      {
         //Verification passed.
         bResult = TRUE;
      }
      else
      {
         //Verification failed
         bResult = FALSE;
         enVerRSStatus = ERROR_SIGN_VER_REVOC_SIGN_HASH_COMPARE;
      }

   }

   if( bResult )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Verify Revoc Signature passes", 0, 0, 0, 0 );
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Verify Revoc Signature failed", 0, 0, 0, 0 );
   }

   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enVerRSStatus);
   /* All Done. Clear Memory and return result */
   return bResult;
}

// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Copies the revocation list to internal memory. FFS --> NOR0/NAND0.
//!  Format: each line is 1 key, starting byte is the length in bytes, 
//!  subsequent bytes is the key.
//!  Current implementation: Copy the whole thing into a file and place 
//!  flash (FFS)
//!  Format of Data in Flash:
//!           [TIMESTAMP][\n]
//!           [Key-Len][Key Array][\n]
//!           [Key-Len][Key Array][\n]...
//!
//! \return
//!   TRUE  - Copy Success
//!   FALSE - Copy Failed. Unexpected Error.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  24 - May - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Support for empty revoc list
//  07 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Change to U32 storage
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//******************************************************************************
static tBool bCopyRevocList( tFetchHandle hHandle )
{
   tBool                bResult         = TRUE;
   OSAL_tIODescriptor   hRevocFile      = 0;
   tU16                 u16Row          = 0;
   tS32                 s32KeyLength    = 0; //To resolve Lint
   tS8                  s8KeyLen        = 0; 
   tS8                  s8NewLineChar   = (tU8)('\n');
   trList               *prRevocList;
   tChar                *pcTimestamp    = OSAL_NULL;

   /* Update for vin-xml feature: NULL revoc list is NOT an error. */
   /* Get the Revoc List */
   if( ( prRevocList = trGetRevocationList( hHandle ) ) == OSAL_NULL )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "No Copy for empty revoc list", 0, 0, 0, 0 );
   }
   /* Open/Create the File */
   else if( ( hRevocFile = OSAL_IOCreate( REVOC_LIST_PATH, OSAL_EN_READWRITE ) )
                                                  == (tS32)OSAL_E_NOTSUPPORTED )
   {
      /* Cannot open revoc list file in flash mem */
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file create() error", 0, 0, 0, 0 );
   }
   /* Get the Timestamp */
   else if( ( pcTimestamp = pcGetRevocTimestamp( hHandle ) ) == OSAL_NULL )
   {
      /* Error fetching revocation list timestamp */
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "GetRevocTimestamp() error", 0, 0, 0, 0 );
   }
   /* Write the Timestamp */
   else if( OSAL_s32IOWrite( hRevocFile, (tS8*)pcTimestamp, 
                             TIMESTAMP_LEN ) != TIMESTAMP_LEN )  
   {
      /* Error fetching revocation list timestamp */
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file write timestamp error", 0, 0, 0, 0 );
   }
   /* Write newline */
   else if( OSAL_s32IOWrite( hRevocFile, &s8NewLineChar, 1 ) != 1 )
   {
      /* Error Writing Newline char into file. */
      bResult = FALSE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "file write NewLineChar error", 0, 0, 0, 0 );
   }
   /* Write the revocation list into file */
   else
   {
      s8KeyLen     = (tS8)prRevocList->pList[0].u32KeyLength;
      s32KeyLength = s8KeyLen; //To resolve Lint

      for( u16Row = 0; u16Row < prRevocList->u16ListLength; u16Row++ )
      {
         /* Write the key length */
         if( OSAL_s32IOWrite( hRevocFile, &s8KeyLen, 1 ) != 1 )
         {
            /* Error Writing Key-Length into file. */
            bResult = FALSE;
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                 "file write keylen error", 0, 0, 0, 0 );
            break;
         }
         /* Write the key into the file */
         else if( OSAL_s32IOWrite( hRevocFile,
                                   (tS8*)prRevocList->pList[u16Row].pu8Array,
                                   (tU32)s32KeyLength ) != s32KeyLength )
         {
            /* Error Writing Key into file. */
            bResult = FALSE;
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                 "file write key error", 0, 0, 0, 0 );
            break;
         }
         /* Write newline */
         else if( OSAL_s32IOWrite( hRevocFile, &s8NewLineChar, 1 ) != 1 )
         {
            /* Error Writing End-Of-Key into file. */
            bResult = FALSE;
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                 "file write NewLineChar error", 0, 0, 0, 0 );
            break;
         }
      }
   }
   
   /* Close the file */
   if( hRevocFile != 0 )
      OSAL_s32IOClose( hRevocFile ); 
   return bResult;
}

// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Performs the revocation-list copy operation. Logic is as follows:
//!  1. If no Revoc_list is found in NOR0, copy the revoc-list.
//!  2. If found, compare timestamp of revoc-list in NOR0 and certificate.
//!  3. If NOR0 timestamp older than certificate one, replace OLD revoclist (NOR0)
//!     with new one from the certificate. Else, do nothing.
//!
//! \return
//!   TRUE  - Copy Success
//!   FALSE - Copy Failed. Unexpected Error.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//******************************************************************************
static tBool bPerformRevocListCopy( tFetchHandle hHandle )
{
   OSAL_tIODescriptor  hRevocFile         = 0;
   tBool  bResult                         = TRUE;
   tChar  acFlashTimestamp[TIMESTAMP_LEN] = {0};
   tPChar pcCertTimestamp                 = OSAL_NULL;
   tS32   as32FlashTime[6]                = {0};
   tS32   as32CertTime[6]                 = {0};
   tU8    u8Row                           = 0;
   tChar  cDummy                          = (tChar)0;
   tenSignVerifyTrace enVerRSStatus = SIGN_VER_REVOC_LIST_COPY_NO_ERROR;
   
   // Check if file exists.
   if( bFileExists( REVOC_LIST_PATH ) == FALSE )
   {
      // No revoc list found in NOR0. ==> First time.
      // Perform copy
      bResult = bCopyRevocList( hHandle );
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                            "Copying revoc list", 0, 0, 0, 0 );
   }
   else
   {
      /* Revoc list exists. Fetch the Timestamp from NOR0 */
      if( ( hRevocFile = OSAL_IOOpen( REVOC_LIST_PATH, 
                                      OSAL_EN_READONLY ) ) == OSAL_ERROR )
      {
         // Cannot Open file.
         bResult = FALSE;
         enVerRSStatus = ERROR_SIGN_VER_RL_OPEN_FILE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "file open revoc list error", 0, 0, 0, 0 );
      }
      else if( OSAL_s32IORead( hRevocFile, (tS8 *)acFlashTimestamp, 
                               TIMESTAMP_LEN )  < TIMESTAMP_LEN )
      {
         // Error in reading.
         bResult = FALSE;
         enVerRSStatus = ERROR_SIGN_VER_RL_READ_FILE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "file read timestamp error", 0, 0, 0, 0 );
      }
      else if( OSAL_s32IOClose( hRevocFile ) == OSAL_ERROR )
      {
         // Error in closing.
         bResult = FALSE;
         enVerRSStatus = ERROR_SIGN_VER_RL_CLOSE_FILE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "file close error", 0, 0, 0, 0 );
      }
      else if( ( pcCertTimestamp = pcGetRevocTimestamp( hHandle ) ) == OSAL_NULL )
      {
         // Error in reading timestamp.
         bResult = FALSE;
         enVerRSStatus = ERROR_SIGN_VER_RL_READ_TS;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "GetRevocTimestamp error", 0, 0, 0, 0 );
      }
      else
      {
         OSAL_s32ScanFormat( acFlashTimestamp, "%d%c%d%c%d%c%d%c%d%c%d",
                             &(as32FlashTime[0]), &cDummy, 
                             &(as32FlashTime[1]), &cDummy,
                             &(as32FlashTime[2]), &cDummy, 
                             &(as32FlashTime[3]), &cDummy,
                             &(as32FlashTime[4]), &cDummy, 
                             &(as32FlashTime[5]) );
         OSAL_s32ScanFormat( pcCertTimestamp,  "%d%c%d%c%d%c%d%c%d%c%d",
                             &(as32CertTime[0]), &cDummy, 
                             &(as32CertTime[1]), &cDummy,
                             &(as32CertTime[2]), &cDummy, 
                             &(as32CertTime[3]), &cDummy,
                             &(as32CertTime[4]), &cDummy, 
                             &(as32CertTime[5]) );
         // Compare the timestamps
         for( u8Row = 0; u8Row < 6; u8Row++ )
         {
            if( as32FlashTime[u8Row] < as32CertTime[u8Row] )
            {
               // The Certificate file is newer.
               // So copy it. But first, remove the old file.
               OSAL_s32IORemove( REVOC_LIST_PATH );
               bResult = bCopyRevocList( hHandle );
               break;
            }
            // ELSE, Certificate file is older or same time
            // So, do nothing.
         }
      }
   }
   
   if( bResult )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Perform Revoc List Copy passes", 0, 0, 0, 0 );
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Perform Revoc List Copy failed", 0, 0, 0, 0 );
   }

   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enVerRSStatus);
   return bResult;
}

// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Returns the Operator Key from the partition, by parsing the certificate
//!  file.
//!
//! \return
//!   An array containing the Operator Key
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//******************************************************************************
static tPU8 pu8GetOperatorKey
(
   //! (I) Fd Crypt Fetch Handle
   tFetchHandle hHandle,
   //! (I) The Partition where the Operator Key is stored.
   tU8 u8PartitionNum,
   //! (O) Length of the Key.
   tPU32 pu32KeyLength
)
{
   tPU8    pu8OperatorKey = OSAL_NULL;
   trArray *pKeyPtr       = OSAL_NULL;

   // The partition number will only be used in the new phase development.
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( u8PartitionNum );
   
   /* Check the arguments before we continue */
   if(hHandle && pu32KeyLength)
   {

      pKeyPtr = trGetOperatorKey( hHandle );

      if(pKeyPtr)
      {
         pu8OperatorKey = pKeyPtr->pu8Array;
         *pu32KeyLength = pKeyPtr->u32KeyLength;
      }
      else
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "trGetOperatorKey Failed ", 0, 0, 0, 0 );
      }
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Invalid argument pu8GetOperatorKey() ",
                           (tS32) hHandle, (tS32) pu32KeyLength, 0, 0 );
   }

   return pu8OperatorKey;
}

// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Returns the Operator Signature from the partition, by parsing the
//!  certificate file.
//!
//! \return
//!   An array containing the Operator Key
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  07 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Change to U32 storage
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//******************************************************************************
static tPU8 pu8GetOperatorSignature
(
   //! (I) Fd Crypt Fetch Handle
   tFetchHandle hHandle,
   //! (I) The Partition where the Operator Signature is stored.
   tU8 u8PartitionNum,
   //! (O) Length of the signature data
   tPU32 pu32SignatureLength
)
{
   tPU8    pu8OperatorSignature = OSAL_NULL;
   trArray *pSignPtr;

   // The partition number will only be used in the new phase development.
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( u8PartitionNum );
   
   pSignPtr = trGetOperatorSignature( hHandle );

   if(pSignPtr)
   {
      pu8OperatorSignature = pSignPtr->pu8Array;
      *pu32SignatureLength = pSignPtr->u32KeyLength;
   }

   return pu8OperatorSignature;
}

// ***************** F U N C T I O N  H E A D E R *****************************
//
//  DESCRIPTION:
//
//! \brief
//!  Verify if the Operator's Key in the partition is authentic.
//!  Signature data is verified with ECC algo using RB public key and resulting
//!  data is compared to the Operator Key.
//!
//! \return
//!   TRUE  - Verification Success
//!   FALSE - Verification Failed.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  07 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Change to U32 storage
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//******************************************************************************
static tBool bVerifyOperatorSignature
(
   //! (I) Fd Crypt Fetch Handle
   tFetchHandle hHandle,
   //! (I) The Partition for which the verification is done.
   tU8 u8PartitionNum
)
{
   tBool bResult             = FALSE;
   tPU8  pu8OperatorKey;
   tPU8  pu8OperatorSignData = OSAL_NULL;
   tU32  u32OperKeyLength    = 0;
   tU32  u32OperSignLength   = 0;
   tU16  u16Row              = 0;
   tU32  au32HashValue[5]    = {0};
   tU8   au8HashVal[20]      = {0};
   tsSHA1Context  sha_ctx;
   tenSignVerifyTrace enVerOpSignStatus = SIGN_VER_OP_SIGN_NO_ERROR;
   tU8 au8RBPubKey[ECC_PUB_KEY_LEN] = {0};
   tS32 s32ErrorCode;
   
   /* Get Operator Pub Key. remember to free the data. */
   if( ( pu8OperatorKey = pu8GetOperatorKey( hHandle, u8PartitionNum,
                                             &u32OperKeyLength ) ) == OSAL_NULL )
   {
      //Reading Operator Pub Key failed.
      enVerOpSignStatus = ERROR_SIGN_VER_OPSIGN_READ_OP_PUBKEY;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "GetOperatorKey error", 0, 0, 0, 0 );
   }
   /* Get the RB Public Key */
   else if( bReadRBPubKey( hHandle, au8RBPubKey ) == FALSE )
   {
      /* Error fetching RB public Key */
      enVerOpSignStatus = ERROR_SIGN_VER_OPSIGN_FETCH_RB_PUBKEY;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "ReadRBPubKey error", 0, 0, 0, 0 );
   }
   /* Get Operator Signature data. Remember to free memory after use */
   else if( ( pu8OperatorSignData = pu8GetOperatorSignature( hHandle,
                  u8PartitionNum, &u32OperSignLength ) ) == OSAL_NULL )
   {
      /* Error fetching Operator Signature data */
      enVerOpSignStatus = ERROR_SIGN_VER_OPSIGN_FETCH_OP_SIGN;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "GetOperSign error", 0, 0, 0, 0 );
   }
   /* Begin Verify Operation */
   else
   {
      /* Lock the non-reentrant BPCL function */
      FD_Crypt_vEnterBPCLCriticalSec();

      // Prepare Hash
      BPCL_SHA1_Init( &sha_ctx );
      
      //Hash the Operator Key
      BPCL_SHA1_Update( &sha_ctx, pu8OperatorKey, u32OperKeyLength );
      
      //Prepare the hash value
      BPCL_SHA1_Finish( &sha_ctx, au32HashValue );

      // Convert result to platform independence and sign
      for( u16Row = 0; u16Row < 5; ++u16Row )
      {
         M_U32_TO_BUF(au32HashValue[u16Row], au8HashVal, (4*u16Row));
      }

      /* Conduct verification */
      s32ErrorCode = (tS32) BPCL_EC_DSA_Verify( au8HashVal, au8RBPubKey, pu8OperatorSignData );

      /* Release BPCL Lock */
      FD_Crypt_vLeaveBPCLCriticalSec();

      if(s32ErrorCode ==  BPCL_OK )
      {
         //Verification passed.
         bResult = TRUE;
      }
      else
      {
         //Verification failed
         enVerOpSignStatus = ERROR_SIGN_VER_OPSIGN_VERIFY;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "Oper Sign Verify failed", 0, 0, 0, 0 );
      }
   }

   if( bResult )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Verify Operator Signature passes", 0, 0, 0, 0 );
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Verify Operator Signature failed", 0, 0, 0, 0 );
   }

   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enVerOpSignStatus);
   /* All Done. Clear Memory and return result */
   return bResult;
}

//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  Check's if the Operator's key is a revoked one or not.
//!  In this implementation, the check is done against the Revocation list
//!  present in the Revocation list in memory.
//!
//! \return
//!   TRUE  - Key has been revoked
//!   FALSE - Key is not revoked.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//*****************************************************************************
static tBool bIsKeyRevoked
(
   //! (I) The Operator Key to be verified if revoked.
   tPCU8 pu8Key
)
{
   OSAL_tIODescriptor  hRevocFile;
   tBool               bResult       = TRUE;
   tBool               bAllocDone    = FALSE;
   tBool               bCompareRes   = FALSE;
   tS32                s32BytesRead;
   tPU8                pu8RevokedKey = OSAL_NULL;
   tS8                 s8KeySize     = 0;
   tS32                s32KeyLen     = 0; // To resolve lint.
   tS8                 s8Delimiter   = 0;
   tU8                 u8Index       = 0;
   tChar               acTimeStamp[TIMESTAMP_LEN+1] = {0};
   tenSignVerifyTrace enKeyRevocStatus = SIGN_VER_CHECK_REVOCKEY_NO_ERROR;

   if( ( hRevocFile = OSAL_IOOpen( REVOC_LIST_PATH,
                                   OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      /* Error opening Revocation file */
      bResult = FALSE;
      enKeyRevocStatus = ERROR_SIGN_VER_REVOC_FILE_OPEN;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "open revoc file error", 0, 0, 0, 0 );
   }
   else
   {
      /* Repeat these steps until there is a match or 
         there are no more keys in the REVOC_LIST.DAT */      
      
      if( OSAL_s32IORead( hRevocFile, (tS8 *)acTimeStamp, 
                          TIMESTAMP_LEN ) != TIMESTAMP_LEN )
      {
         /* Error while reading from file */
         bResult = FALSE;
         enKeyRevocStatus = ERROR_SIGN_VER_REVOC_FILE_READ_TS;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "read revoc timestamp error", 0, 0, 0, 0 );
      }
      else if( OSAL_s32IORead( hRevocFile, &s8Delimiter, 1 ) != 1 )
      {
         /* Error while reading from file */
         bResult = FALSE;
         enKeyRevocStatus = ERROR_SIGN_VER_REVOC_FILE_READ_DELIMITER;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "read revoc delimiter error", 0, 0, 0, 0 );
      }
      
      do
      {
         /* Read Key Length Size */
         s32BytesRead = OSAL_s32IORead( hRevocFile, &s8KeySize, 1 );
         if( s32BytesRead != 1 )
         {
            /* Error while reading from file */
            bResult = FALSE;
            enKeyRevocStatus = ERROR_SIGN_VER_REVOC_FILE_READ_KEYSIZE;
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                 "read revoc KeySize error", 0, 0, 0, 0 );
            break;
         }
         
         // resolve lint
         s32KeyLen = (tS32)s8KeySize;
         
         /* Allocate Memory for key read */
         if( !bAllocDone )
         {
            if( ( pu8RevokedKey = (tPU8)OSAL_pvMemoryAllocate( (tU32)s32KeyLen ) ) == OSAL_NULL )
            {
               /* Error while allocating memory from file */
               bResult = FALSE;
               enKeyRevocStatus = ERROR_SIGN_VER_REVOC_KEY_READ_MEMALLOC;
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                    "malloc error", 0, 0, 0, 0 );
               break;
            }
            else
            {
               bAllocDone = TRUE;
            }
         }
         
         /* Read the Key */
         s32BytesRead = OSAL_s32IORead( hRevocFile,
                                        (tS8*)pu8RevokedKey,
                                        (tU32)s32KeyLen );
         if( s32BytesRead != s32KeyLen )
         {
            /* Error while reading Revoked Key from file */
            bResult = FALSE;
            enKeyRevocStatus = ERROR_SIGN_VER_REVOC_READ_KEY;
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                 "read key error", 0, 0, 0, 0 );
            break;
         }
         /* Check if key revoked */
         else
         {
            if( pu8RevokedKey && pu8Key)
            {
               for( u8Index = 0; u8Index < (tU8)s8KeySize; u8Index++ )
               {
                  if( pu8RevokedKey[u8Index] != pu8Key[u8Index] )
                  {
                     /* Key's are not the same */
                     bCompareRes = FALSE;
                     break;
                  }
                  else
                  {
                     bCompareRes = TRUE;
                  }
               }
            }
            /* Read the '\n' char from the file */
            s32BytesRead = OSAL_s32IORead( hRevocFile, &s8Delimiter, 1 );
            if( s8Delimiter != (tS8)'\n' )
            {
               bResult = FALSE;
               break;
            }
         }
      } while( bCompareRes == FALSE && s32BytesRead > 0 );

      if( bCompareRes == TRUE )
      {
         bResult = TRUE;
      }
      else
      {
         bResult = FALSE;
         enKeyRevocStatus = ERROR_SIGN_VER_REVOC_KEY_REVOKED;
         
      }
   }

   /* All Done. Close file and release memory */
   if( hRevocFile != OSAL_ERROR )
      OSAL_s32IOClose( hRevocFile );

   if( pu8RevokedKey != OSAL_NULL )
   {
      OSAL_vMemoryFree( pu8RevokedKey );
   }
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enKeyRevocStatus);
   return bResult;
}
//#ifdef VIN_IN_KDS  
//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  Read the VIN from KDS
//!
//! \return
//!   VIN from the KDS. tPU8 type.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  21 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Added GM Support for VIN,
//                  |                              | read from LearnedVIN in NAND0.
//  30 - Nov - 2010 |  Gokulkrishnan N (RBEI/ECF1) | VinFeature Porting for FORD-MFD with KDS Update
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//*****************************************************************************
static tBool bReadVINfromKDS( tFetchHandle hHandle, tPU8 pu8VIN )
{
   tBool bRet = TRUE;
   tString strCryptVinPath;
   tU32 u32VinHeaderSeek;
   tU32 u32VinLength;
   /* This will be removed if DTT Entries are available for other Products:*/
   strCryptVinPath = CRYPT_VIN_PATH_GMGE;
   u32VinHeaderSeek = VIN_HEADER_SEEK_GMGE;
   u32VinLength = VIN_LEN_GMGE;


#ifdef VIN_IN_KDS   
   OSAL_tIODescriptor rVINKDS_Hdl                  = 0;
   tsKDSEntry         rVINKDS_Entry;
   tS32               s32RetVal                    = 0;   
   tsKDSEntry*        prKdsEntry                   = &rVINKDS_Entry;
   
   /* Open KDS device */
   rVINKDS_Hdl = OSAL_IOOpen( KDS_DEVICE, OSAL_EN_READONLY );

   if( rVINKDS_Hdl != OSAL_ERROR )
   {
      /* Clear the KDS Entry structure */
      OSAL_pvMemorySet( prKdsEntry, 0, sizeof( rVINKDS_Entry ) );

      /* Read from KDS at the VIN Entry location */
      rVINKDS_Entry.u16Entry       =  VIN_ENTRY;
      rVINKDS_Entry.u16EntryLength =  VIN_ENTRY_LEN ;
      s32RetVal = OSAL_s32IORead( rVINKDS_Hdl,
                                 ( tPS8 )&rVINKDS_Entry,
                                 ( tU32 )sizeof( rVINKDS_Entry ) );
      /* Read Success*/
      if ( s32RetVal != OSAL_ERROR)
      {
         /* Copy to buffer */
         OSAL_pvMemoryCopy( pu8VIN, rVINKDS_Entry.au8EntryData, VIN_ENTRY_LEN );
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_SUCCESS, "Get VIN from KDS success", 
                              0, 0, 0, 0 );
      }
      else
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_ERROR, "Get VIN from KDS failed", 
                              0, 0, 0, 0 );
      }

      /* All Done. Close KDS and return status */
      s32RetVal = OSAL_s32IOClose( rVINKDS_Hdl );
      if( s32RetVal == OSAL_OK )
      {
         bRet = TRUE;
      }
      else
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_ERROR, "Error in closing KDS", 
                              0, 0, 0, 0 );
         bRet = FALSE;
      }
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_ERROR, "Error in opening KDS", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }
#else
   /* For GM: The VIN value is available in NAND0 FS as LearnedVIN */
   /* For MFD: The VIN value is available in NOR0 FS as LearnedVIN */
   OSAL_tIODescriptor hFile;

   /*Clear memory*/
   OSAL_pvMemorySet( pu8VIN, 0, VIN_ENTRY_LEN );
   
   if( ( hFile = OSAL_IOOpen( strCryptVinPath , OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_INFO, "Error opening LearnedVIN", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }

   else if( ( OSALUTIL_s32FSeek( hFile, (tS32)u32VinHeaderSeek , OSAL_C_S32_SEEK_SET ) ) == OSAL_ERROR )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_ERROR, "Error seeking to LearnedVIN", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }

   else if( ( OSAL_s32IORead( hFile, (tPS8)pu8VIN, u32VinLength ) ) < (tS32)u32VinLength)
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_ERROR, "Error reading LearnedVIN", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }
   else if( OSAL_s32IOClose( hFile ) == OSAL_ERROR )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_ERROR, "Error closing LearnedVIN", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }
   
#endif
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(hHandle);
   return bRet;
}
 //#ifdef DID_IN_KDS  
//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  Read the DID from KDS
//!
//! \return
//!   DID from the KDS. tPU8 type.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  12 - Mar - 2014 |    Swathi Bolar (RBEI/ECF5)  |   Initial Version

//*****************************************************************************
static tBool bReadDIDfromKDS( tFetchHandle hHandle, tPU8 pu8DID )
{
   tBool bRet = TRUE;
#ifndef DID_IN_KDS
   tString strCryptDIDPath;
   tU32 u32DIDHeaderSeek;
   tU32 u32DIDLength;
   /* This will be removed if DTT Entries are available for other Products: */

   strCryptDIDPath = CRYPT_DID_PATH;
   u32DIDHeaderSeek = DID_HEADER_SEEK;
   u32DIDLength = DID_LEN_FFS;
#endif

#ifdef DID_IN_KDS  
   OSAL_tIODescriptor rDIDKDS_Hdl;
   tsKDSEntry         rDIDKDS_Entry;
   tS32               s32RetVal                    = 0;   
   tsKDSEntry*        prKdsEntry                   = &rDIDKDS_Entry;
   
   /* Open KDS device */
   rDIDKDS_Hdl = OSAL_IOOpen( KDS_DEVICE, OSAL_EN_READONLY );

   if( rDIDKDS_Hdl != OSAL_ERROR )
   {
      /* Clear the KDS Entry structure */
      OSAL_pvMemorySet( prKdsEntry, 0, sizeof( rDIDKDS_Entry ) );

      /* Read from KDS at the VIN Entry location */
      rDIDKDS_Entry.u16Entry       =  DID_ENTRY;
      rDIDKDS_Entry.u16EntryLength =  DID_ENTRY_LEN ;
      s32RetVal = OSAL_s32IORead( rDIDKDS_Hdl,
                                 ( tPS8 )&rDIDKDS_Entry,
                                 ( tU32 )sizeof( rDIDKDS_Entry ) );
      /* Read Success*/
      if ( s32RetVal != OSAL_ERROR)
      {
         /* Copy to buffer */
         OSAL_pvMemoryCopy( pu8DID, rDIDKDS_Entry.au8EntryData, DID_ENTRY_LEN );
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_SUCCESS, "Get DID from KDS success", 
                              0, 0, 0, 0 );
      }
      else
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_ERROR, "Get DID from KDS failed", 
                              0, 0, 0, 0 );
      }

      /* All Done. Close KDS and return status */
      s32RetVal = OSAL_s32IOClose( rDIDKDS_Hdl );
      if( s32RetVal == OSAL_OK )
      {
         bRet = TRUE;
      }
      else
      {
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_ERROR, "Error in closing KDS", 
                              0, 0, 0, 0 );
         bRet = FALSE;
      }
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_ERROR, "Error in opening KDS", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }
#else

   /* The DID support if the value is available in NOR0 FS as LearnedDID */
   OSAL_tIODescriptor hFile;

   /*Clear memory*/
   OSAL_pvMemorySet( pu8DID, 0,DID_ENTRY_LEN );
   
   if( ( hFile = OSAL_IOOpen( strCryptDIDPath , OSAL_EN_READONLY ) ) == OSAL_ERROR )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_INFO, "Error opening LearnedDID", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }

   else if( ( OSALUTIL_s32FSeek( hFile, (tS32)u32DIDHeaderSeek , OSAL_C_S32_SEEK_SET ) ) == OSAL_ERROR )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_ERROR, "Error seeking to LearnedDID", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }

   else if( ( OSAL_s32IORead( hFile, (tPS8)pu8DID, u32DIDLength ) ) < (tS32)u32DIDLength)
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_ERROR, "Error reading LearnedDID", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }
   else if( OSAL_s32IOClose( hFile ) == OSAL_ERROR )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_ERROR, "Error closing LearnedDID", 
                           0, 0, 0, 0 );
      bRet = FALSE;
   }
   
#endif
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(hHandle);
   return bRet;
}
//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  Compares the VIN entries from the KDS and certificate file.
//!
//! \return
//!  TRUE  --> Same VIN
//!  FALSE --> Different VIN
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  21 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Added GM Support for VIN,
//                  |                              | read from LearnedVIN in NAND0.
//  21 -July - 2010 |    anc2hi                    | update for lack of VIN.
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//*****************************************************************************
static tBool bCompareVIN( tFetchHandle hHandle )
{
   tBool  bResult       = TRUE;
   tU32   u32StartIndex = 0;
   tPU8   pu8VIN1;
   tenSignVerifyTrace enVINCompareStatus = SIGN_VER_COMPARE_VIN_NO_ERROR;
   tU8    au8VIN2[VIN_ENTRY_LEN] = {0};
   
   /* Get VIN from Certificate */
   if( ( pu8VIN1 = (tU8 *)pcGetVIN( hHandle ) ) == OSAL_NULL )
   {
      bResult = FALSE;
      enVINCompareStatus = ERROR_SIGN_VER_NO_XML_VIN;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "pcGetVIN Failed ", 0, 0, 0, 0 );
   }
   else if(*pu8VIN1 == 0) // Lack of VIN is not error. It means that the system is using CID instead
   {
      bResult = TRUE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Not a VIN based sig to compare VIN", 0, 0, 0, 0 );
   }
   /* Get VIN from HW */
   else if( bReadVINfromKDS( hHandle, au8VIN2) == FALSE)
   {
      bResult = FALSE;
      enVINCompareStatus = ERROR_SIGN_VER_NO_FS_VIN;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Read VIN from KDS Failed ", 0, 0, 0, 0 );
   }
   /* Perform comparison */
   else
   {
  	   /* For MFD: 17 charecters are compared */
      if( OSAL_u32StringLength( (tCString)pu8VIN1 ) >= OSAL_u32StringLength( (tCString)au8VIN2 ) )
      {
         /* If the lengths are same, compare entire string. 
            Else, compare the last 'n' characters.
         */
         u32StartIndex = OSAL_u32StringLength( (tCString)pu8VIN1 ) - 
                         OSAL_u32StringLength( (tCString)au8VIN2 );
         
         if( OSAL_s32MemoryCompare( ( pu8VIN1 + u32StartIndex ), au8VIN2,
                              OSAL_u32StringLength( (tCString)au8VIN2 ) ) != 0 )
         {
            bResult = FALSE;
            enVINCompareStatus = ERROR_SIGN_VER_DIFF_VIN;
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                                 "Comparing VIN failed", 0, 0, 0, 0 );
         }
         else
         {
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                 "Comparing VIN passed", 0, 0, 0, 0 );
         }
      }
      else
      {
         /* Here, XML File VIN is < HW VIN. So return False */
         bResult = FALSE;
         enVINCompareStatus = ERROR_SIGN_VER_INSUFF_XML_VINDATA;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Invalid VIN (short) in certificate", 0, 0, 0, 0 );
      }
   }

   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enVINCompareStatus);
   return bResult;
}
//#endif

//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  Compares the DID entries from the KDS and certificate file.
//!
//! \return
//!  TRUE  --> Same DID
//!  FALSE --> Different DID
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  12 - Mar - 2014 |    Swathi Bolar (RBEI/ECF5)  |   Initial Version
//*****************************************************************************
static tBool bCompareDID( tFetchHandle hHandle )
{
   tBool  bResult       = TRUE;
   tU32   u32StartIndex = 0;
   tPU8   pu8DID1;
   tenSignVerifyTrace enDIDCompareStatus = SIGN_VER_COMPARE_DID_NO_ERROR;
   tU8    au8DID2[DID_ENTRY_LEN] = {0};
   
   /* Get DID from Certificate */
   if( ( pu8DID1 = (tU8 *)pcGetDID( hHandle ) ) == OSAL_NULL )
   {
      bResult = FALSE;
      enDIDCompareStatus = ERROR_SIGN_VER_NO_XML_DID;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "pcGetDID Failed ", 0, 0, 0, 0 );
   }
   else if(*pu8DID1 == 0) // Lack of DID is not error. It means that the system is using CID instead
   {
      bResult = TRUE;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Not a DID based sig to compare DID", *pu8DID1, 0, 0, 0 );

   }
   /* Get DID from HW */
   else if( bReadDIDfromKDS( hHandle, au8DID2) == FALSE)
   {
      bResult = FALSE;
      enDIDCompareStatus = ERROR_SIGN_VER_NO_FS_DID;
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Read DID from KDS Failed ", 0, 0, 0, 0 );
   }
   /* Perform comparison */
   else
   {

      if( OSAL_u32StringLength( (tCString)pu8DID1 ) >= OSAL_u32StringLength( (tCString)au8DID2 ) )
      {
         /* If the lengths are same, compare entire string. 
            Else, compare the last 'n' characters.
         */
         u32StartIndex = OSAL_u32StringLength( (tCString)pu8DID1 ) - 
                         OSAL_u32StringLength( (tCString)au8DID2 );
         
         if( OSAL_s32MemoryCompare( ( pu8DID1 + u32StartIndex ), au8DID2,
                              OSAL_u32StringLength( (tCString)au8DID2 ) ) != 0 )
         {
            bResult = FALSE;
            enDIDCompareStatus = ERROR_SIGN_VER_DIFF_DID;
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                                 "Comparing DID failed", 0, 0, 0, 0 );
         }
         else
         {
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                                 "Comparing DID passed", 0, 0, 0, 0 );
         }
      }
      else
      {
         /* Here, XML File DID is < HW DID. So return False */
         bResult = FALSE;
         enDIDCompareStatus = ERROR_SIGN_VER_INSUFF_XML_DID;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Invalid DID (short) in certificate", 0, 0, 0, 0 );
      }
   }

   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enDIDCompareStatus);
   return bResult;
}
//#endif



//********************** F U N C T I O N  H E A D E R *************************
//
//!  Fetches the partition's Signature data.
//!
//! \return
//!  Array of partition signature data.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  07 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Change to U32 storage
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//*****************************************************************************
static tPU8 pu8GetPartitionSignature
(
   //! (I) Fd Crypt Fetch Handle
   tFetchHandle hHandle,
   //! (I) Partition where the signature data is kept.
   tU8 u8PartitionNum,
   //! (O) Length of the signature data
   tPU32 pu32PartSignLength
)
{
   tPU8 pu8PartSignature = OSAL_NULL;
   trArray *pSignPtr;

   // The partition number will only be used in the new phase development.
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( u8PartitionNum );
   
   pSignPtr = trGetPartSignature( hHandle );

   if(pSignPtr)
   {
      pu8PartSignature    = pSignPtr->pu8Array;
      *pu32PartSignLength = pSignPtr->u32KeyLength;
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "trGetPartSignature Failed ", 0, 0, 0, 0 );
   }

   return pu8PartSignature;
}

//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  Prepares the hash-data for given partition.
//!  Inputs: Device ID, Partition Number.
//!          (Logical inputs)File-Data, VIN, Lifetime Value
//!
//! \return
//!  Array of partition's hash-value
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  26 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   2nd phase CRQ from GMGE
//  07 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Change to U32 storage
//  21 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Added Debug traces
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//  12 - Mar - 2014 |    Swathi Bolar (RBEI/ECF5)  | DID Support Added(INNAVPF-2544)
//*****************************************************************************
static tPU8 pu8CreatePartitionHash
(
   //! (I) Fd Crypt Fetch Handle
   tFetchHandle hHandle,
   //! (I) The partition for which the hash is being prepared
   tU8 u8PartitionNum
)
{
   tsSHA1Context  sha_ctx;
   static tU8     au8HashValue[20] = {0};
   tU32           au32HashValue[5] = {0};
   tPU8           pu8VIN           = OSAL_NULL;
   tPU8           pu8DID           = OSAL_NULL;
   tU8            au8CID[16]       = {0};
   tChar          acCIDStr[33]     = {0};
   tChar          acHexStr[3]      = {0};
   tU8            u8Index          = 0;
   tPU8           pu8Result        = OSAL_NULL;
   trFileList*    prFileList       = OSAL_NULL;
   tU8            au8Offset[4]     = {0};
   tU8            au8NumBytes[4]   = {0};
   tPChar         pcLifetimeStart  = OSAL_NULL;
   tPChar         pcLifetimeStop   = OSAL_NULL;
   tU32           u32Offset        = 0;
   tU32           u32Numbytes      = 0;
   tU8            au8HWCID[CID_LEN]= {0};
   OSAL_tIODescriptor devicehandle;

   // The partition number will only be used in the new phase development.
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( u8PartitionNum );
   
   if(!hHandle)
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - CreatePartitionHash", 0, 0, 0, 0 );
   }
   else if( ( pu8VIN = (tU8*)pcGetVIN( hHandle ) ) == OSAL_NULL )
   {
      //error reading from KDS
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "VIN null", 0, 0, 0, 0 );
   }
   else if( ( pu8DID = (tU8*)pcGetDID( hHandle ) ) == OSAL_NULL )
   {
      //error reading from KDS
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "DID null", 0, 0, 0, 0 );
   }
   else if( ( prFileList = trGetFileData( hHandle ) ) == OSAL_NULL )
   {
      //Error fetching File Data
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "GetFileData() error", 0, 0, 0, 0 );
   }
   else if( (pcLifetimeStart = pcGetLifetimeStart( hHandle )) == OSAL_NULL )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "GetLifetimeStart() error ", 0, 0, 0, 0 );
   }
   else if( (pcLifetimeStop  = pcGetLifetimeEnd( hHandle )) == OSAL_NULL)
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "GetLifetimeEnd() error ", 0, 0, 0, 0 );
   }
   else
   {
      //Clear Buffer
      OSAL_pvMemorySet( au8HashValue, 0, sizeof( au8HashValue ) );

      /* Begin Prepare of Hash */
      BPCL_SHA1_Init( &sha_ctx );
      

      if( OSAL_u32StringLength( (tChar *)pu8VIN ) != 0 )
      {
        //VIN is present. Hash empty CID value
        BPCL_SHA1_Update( &sha_ctx, OSAL_NULL, 0 );
        // Hash VIN content
        BPCL_SHA1_Update( &sha_ctx, (tU8*)pu8VIN, 
        OSAL_u32StringLength( (tChar *)pu8VIN ) );
#ifdef _DEBUG_TRACE_ 
        fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
           TR_CRYPT_MSG_STRING, (tPChar)pu8VIN, 0, 0, 0, 0 );                  
#endif
      }
      else if( OSAL_u32StringLength( (tChar *)pu8DID ) != 0 )
      {
        //DID is present. Hash empty CID value
        BPCL_SHA1_Update( &sha_ctx, OSAL_NULL, 0 );
        // Hash DID content
        BPCL_SHA1_Update( &sha_ctx, (tU8*)pu8DID, 
        OSAL_u32StringLength( (tChar *)pu8DID ) );
#ifdef _DEBUG_TRACE_ 
        fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
           TR_CRYPT_MSG_STRING, (tPChar)pu8DID, 0, 0, 0, 0 );				   
#endif
      }

     else
      { 
         //VIN-DID is empty. Hash actual CID String by fetching it from the device
         devicehandle = OSAL_IOOpen( hHandle->acDevName, OSAL_EN_READWRITE);
         if(devicehandle != OSAL_ERROR)
         {
            if(OSAL_s32IOControl(devicehandle, OSAL_C_S32_IOCTRL_FIOGET_CID,(tS32)au8HWCID) != OSAL_OK)
            {
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                              TR_CRYPT_MSG_ERROR, "CID fetch failed",
                              0, 0, 0, 0 );
            }
            else
            {
               //Flip the CID array
               for( u8Index = 0; u8Index < CID_LEN; u8Index++ )
               {
                  au8CID[u8Index] = au8HWCID[(CID_LEN-1)-u8Index];
               }
                     
               //Prepare String
               for( u8Index = 0; u8Index < CID_LEN; u8Index++ )
               {
                  OSAL_s32PrintFormat( acHexStr, "%2x", au8CID[u8Index] );
                  acCIDStr[ 2*u8Index ]     = (tChar)OSAL_s32ToUpper(
                                            ((acHexStr[0]==' ')?'0':acHexStr[0]));
                  acCIDStr[ 2*u8Index + 1 ] = (tChar)OSAL_s32ToUpper( acHexStr[1] );
               }
            
               //Display string
#ifdef _DEBUG_TRACE_
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                 TR_CRYPT_MSG_STRING, acCIDStr, 0, 0, 0, 0 );
                                 
               fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
                                 TR_CRYPT_MSG_STRING, (acCIDStr+28), 0, 0, 0, 0 );
#endif
               BPCL_SHA1_Update( &sha_ctx, (tU8*)acCIDStr, 2*CID_LEN );
            }
         }
         else
         {
            fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF,
            		          TR_CRYPT_MSG_ERROR, "Couldn't open device to fetch the CID",
                              0, 0, 0, 0 );
         }
      }
      // Hash Lifetime Start content
      BPCL_SHA1_Update( &sha_ctx, (tU8*)pcLifetimeStart, 
                        OSAL_u32StringLength( pcLifetimeStart ) );
#ifdef _DEBUG_TRACE_       
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_STRING, pcLifetimeStart, 0, 0, 0, 0 );
#endif      
      // Hash Lifetime End content
      BPCL_SHA1_Update( &sha_ctx, (tU8*)pcLifetimeStop, 
                        OSAL_u32StringLength( pcLifetimeStop ) );
#ifdef _DEBUG_TRACE_                           
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                           TR_CRYPT_MSG_STRING, pcLifetimeStop, 0, 0, 0, 0 );
#endif      
      // Hash File Data
      for( u8Index = 0; u8Index < prFileList->u16ListLength; u8Index++ )
      {
         // Hash offset
         u32Offset    = prFileList->pFileList[u8Index].u32Offset;
         au8Offset[0] = (tU8)((u32Offset >> 0)&0xFF);
         au8Offset[1] = (tU8)((u32Offset >> 8)&0xFF);
         au8Offset[2] = (tU8)((u32Offset >> 16)&0xFF);
         au8Offset[3] = (tU8)((u32Offset >> 24)&0xFF);
         BPCL_SHA1_Update( &sha_ctx, (tU8*)au8Offset, 4 );

#ifdef _DEBUG_TRACE_
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_INFO, "au8Offset", 
                              au8Offset[0], au8Offset[1], au8Offset[2], 
                              au8Offset[3] );
#endif
         // Hash num of bytes
         u32Numbytes    = prFileList->pFileList[u8Index].u32NumOfBytes;
         au8NumBytes[0] = (tU8)((u32Numbytes >> 0)&0xFF);
         au8NumBytes[1] = (tU8)((u32Numbytes >> 8)&0xFF);
         au8NumBytes[2] = (tU8)((u32Numbytes >> 16)&0xFF);
         au8NumBytes[3] = (tU8)((u32Numbytes >> 24)&0xFF);
         BPCL_SHA1_Update( &sha_ctx, (tU8*)au8NumBytes, 4 );

#ifdef _DEBUG_TRACE_
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_INFO, "au8NumBytes", 
                              au8NumBytes[0], au8NumBytes[1], au8NumBytes[2], 
                              au8NumBytes[3] );
#endif
         // Hash filename
         BPCL_SHA1_Update( &sha_ctx, 
            (tU8*)prFileList->pFileList[u8Index].acFilename, 
            OSAL_u32StringLength( prFileList->pFileList[u8Index].acFilename ) );

#ifdef _DEBUG_TRACE_
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_INFO, prFileList->pFileList[u8Index].acFilename, 
                              0, 0, 0, 0 );
#endif
         // Hash Raw Data
         BPCL_SHA1_Update( &sha_ctx, 
                           prFileList->pFileList[u8Index].rArray.pu8Array, 
                           prFileList->pFileList[u8Index].rArray.u32KeyLength );
#ifdef _DEBUG_TRACE_
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, 
                              TR_CRYPT_MSG_INFO, "File Data Length", 
                        (tS32)prFileList->pFileList[u8Index].rArray.u32KeyLength, 
                              0, 0, 0 );
#endif
      }
      //Prepare the hash value
      BPCL_SHA1_Finish( &sha_ctx, au32HashValue );

      // Convert result to platform independence and sign
      for( u8Index = 0; u8Index < 5; ++u8Index )
      {
         M_U32_TO_BUF(au32HashValue[u8Index], au8HashValue, (4*u8Index));
      }

      pu8Result = au8HashValue;
   }

   return pu8Result;
}

//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  Verifies the partition signature;
//!  Inputs required: Device ID, Partition Number.
//!        (Logical inputs) Part_Signature from Certificate, Hash value, Operator Key
//!
//! \return
//!  TRUE  - Verification success.
//!  FALSE - Verification failed.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  07 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Change to U32 storage
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//*****************************************************************************
static tBool bVerifyPartSignature
(
   //! (I) Fd Crypt Fetch Handle
   tFetchHandle hHandle,
   //! (I) The partition under verification
   tU8 u8PartitionNum
)
{
   tBool bResult           = FALSE;
   tPU8  pu8PartSignature;
   tPU8  pu8HashValue      = OSAL_NULL;
   tPU8  pu8OperatorKey    = OSAL_NULL;
   tU32  u32PartSignLength = 0;
   tU32  u32KeyLength      = 0;
   tenSignVerifyTrace enVerPartSignStatus = SIGN_VER_PART_SIGN_NO_ERROR;
   
   /* Read Partition Signature from Certificate */
   if( ( pu8PartSignature = pu8GetPartitionSignature( hHandle, u8PartitionNum,
                                           &u32PartSignLength ) ) == OSAL_NULL )
   {
      /* Error while reading signature */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Error reading Signature", 0, 0, 0, 0 );
      enVerPartSignStatus = ERROR_SIGN_VER_PSIGN_READ_PARTSIGN;
   }
   /* Get Operator Key */
   else if( ( pu8OperatorKey = pu8GetOperatorKey( hHandle, u8PartitionNum,
                                                &u32KeyLength ) ) == OSAL_NULL )
   {
      /* Error while reading Operator Key */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Error reading Operator Key", 0, 0, 0, 0 );
      enVerPartSignStatus = ERROR_SIGN_VER_PSIGN_READ_OPKEY;
   }
   else
   {
      /* Lock the non-reentrant BPCL function */
      FD_Crypt_vEnterBPCLCriticalSec();

      /* Create Hash */
      if( ( pu8HashValue = pu8CreatePartitionHash( hHandle, u8PartitionNum ) ) == OSAL_NULL )
      {
         /* Error while creating Hash-values */
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "Error creating hash", 0, 0, 0, 0 );
         enVerPartSignStatus = ERROR_SIGN_VER_PSIGN_CREATE_HASH;
      }
      /* Do verification */
      else if( BPCL_OK != (tS32) BPCL_EC_DSA_Verify( pu8HashValue, pu8OperatorKey, pu8PartSignature ))
      {
         //Verification failed
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                                 "Verification for partition failed", 0, 0, 0, 0 );
         enVerPartSignStatus = ERROR_SIGN_VER_PSIGN_VERIFY;
      }
      else
      {
            bResult = TRUE;
      }

      /* Release BPCL Lock */
      FD_Crypt_vLeaveBPCLCriticalSec();
   }

   if(bResult)
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_SUCCESS,
                              "VerifyPartSignature succeeded ", 0, 0, 0, 0 );
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                              "VerifyPartSignature failed", 0, 0, 0, 0 );
   }

   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enVerPartSignStatus);
   return bResult;
}

#ifdef ENABLE_CERT_LIFETIME_CHECK
//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  Verifies the lifetime of the certificate.
//!  Inputs required: Device ID, Partition Number
//!
//! \return
//!  TRUE  - Certificate lifetime verified.
//!  FALSE - Certificate lifetime Verification failed.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  09 - Jul - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Enabled lifetime check 
//                  |                              | feature and support code.
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//*****************************************************************************
static tBool bVerifyCertLifetime
(
   //! (I) Fd Crypt Fetch Handle
   tcFetchHandle hHandle,
   //! (I) The partition under verification
   tU8 u8PartitionNum 
)
{
   OSAL_trTimeDate rCertLifetime = {0};
   OSAL_trTimeDate rRTCTimeDate  = {0};
   tBool           bResult       = FALSE;
   tenSignVerifyTrace enVerifyCLTStatus = ERROR_SIGN_VER_CERT_LIFETIME_EXPIRED;

   if(!hHandle)
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "ERR: Null Handle - VerCertLT ", 0, 0, 0, 0 );
   }
   else
   {
      /* Get the Time-stamp */
      rCertLifetime = trGetCertLifetime( hHandle, u8PartitionNum );

      /* Feature Update. TimeDate structure returning 0 means lifetime string is
         null; this means lifetime forever. */
      // Default value of the certificate is 0
      if( rCertLifetime.s32Year   == 0 &&
          rCertLifetime.s32Month  == 0 &&
          rCertLifetime.s32Day    == 0 &&
          rCertLifetime.s32Hour   == 0 &&
          rCertLifetime.s32Minute == 0 &&
          rCertLifetime.s32Second == 0 )
      {
         bResult = TRUE;
         enVerifyCLTStatus = SIGN_VER_CERT_LIFETIME_NO_ERROR;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "lifetime forever", 0, 0, 0, 0 );
      }
      /* Verify RTC Time */
      else if( trReadRTCTime( &rRTCTimeDate ) == FALSE )
      {
         enVerifyCLTStatus = ERROR_SIGN_VER_CERT_LT_READ_RTC;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "ReadRTCTime() error", 0, 0, 0, 0 );
      }
      /* Compare the time-stamp */
      else if( s8CompareValues( &rCertLifetime, &rRTCTimeDate ) < 0 )
      {
         enVerifyCLTStatus = ERROR_SIGN_VER_CERT_LT_COMPARE;
         fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                              "Verify cert life error", 0, 0, 0, 0 );
      }
      else
      {
         enVerifyCLTStatus = SIGN_VER_CERT_LIFETIME_NO_ERROR;
         bResult = TRUE;
      }
   }
   
   if( bResult )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Cert. lifetime check passes", 0, 0, 0, 0 );
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Cert. lifetime check failed", 0, 0, 0, 0 );
   }

   /* Return result */
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enVerifyCLTStatus);
   return bResult;
}
#endif
//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  Security Fix. Compares the revocation timestamp to the partition lifetime
//!  If the revocation timestamp is equal to or newer than partition lifetime,
//!  then return success else failed.
//!  Inputs required: Fetch handle, Partition Number
//!
//! \return
//!  TRUE  - Revocation timestamp >= Certificate lifetime.
//!  FALSE - Revocation timestamp < Certificate lifetime.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  26 - Apr - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added Handle to make the
//                                                 | function reentrant
//*****************************************************************************
static tBool bVerifyRevocTimestamp
(
   //! (I) Fd Crypt Fetch Handle
   tcFetchHandle hHandle,
   //! (I) The partition under verification
   tU8 u8PartitionNum 
)
{
   OSAL_trTimeDate rRevocTimestamp = {0};
   OSAL_trTimeDate rPartLifetime   = {0};
   tBool           bResult         = FALSE;
   tenSignVerifyTrace enVerRTSStatus = ERROR_SIGN_VER_REVOC_TS_COMPARE;
   /* Get the Time-stamp */
   if( !trGetRevocTimestamp( hHandle, &rRevocTimestamp ) )
   {
      
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Getting RevocTS Failed ", 0, 0, 0, 0 );
   }
   else if (!trGetCertLifetimeStart( hHandle, &rPartLifetime ))
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Getting Cert LifeTime start failed ", 0, 0, 0, 0 );
   }
   /* Compare the time-stamp */
   else if( s8CompareValues( &rRevocTimestamp, &rPartLifetime ) >= 0 )
   {
      bResult = TRUE;
      enVerRTSStatus = SIGN_VER_REVOC_TS_NO_ERROR;
   }
   
   if( bResult )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "Verify Revoc Timestamp passes", 0, 0, 0, 0 );
   }
   else
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Verify Revoc Timestamp failed", 0, 0, 0, 0 );
   }

   // The partition number will only be used in the new phase development.
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( u8PartitionNum );
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED( enVerRTSStatus );
   /* Return result */
   return bResult;
}
//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  The main function responsible for conducting signature verification on
//!  partition 0 of the XML certificate, by following a sequence of verification
//!  steps to resolve this.
//!  Inputs required: coszCertPath - certificate path
//!
//! \return
//!  TRUE  - XML certificate and Partition 0 signature verified.
//!  FALSE - XML certificate and Partition 0 signature Verification failed.
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  22 - Mar - 2010 |    Jeryn Mathew (RBEI/ECF1)  |   Initial Version
//  07 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Change to U32 storage
//  21 - Jun - 2010 |    Jeryn Mathew (RBEI/ECF1)  | Performance traces.
//  05 - Apr - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   (RBEI/ECF1)  | Fetch layer is made reentrant
//  12 - Mar - 2014 |    Swathi Bolar (RBEI/ECF1)  | DID Support was added INNAVPF-2544
//*****************************************************************************
tBool fd_crypt_verify_xml_signature( tCString coszCertPath )
{
   tBool bResult      = TRUE;
   tenSignVerifyTrace enVerifyStatus = SIGN_VER_NO_ERROR;
   tU32  u32KeyLength = 0;
   tFetchHandle hHandle;

#ifdef _DEBUG_TRACE_    
   /* Measuring Performance */
   OSAL_tMSecond u32Total          = 0;
   OSAL_tMSecond u32XMLParseTime   = 0;
   OSAL_tMSecond u32FetchTime      = 0;
   OSAL_tMSecond u32TempTime       = 0;
   OSAL_tMSecond u32PartVerifyTime = 0;
   OSAL_tMSecond u32Start          = OSAL_ClockGetElapsedTime();
#endif
   
   // This step parses the XML file and prepare the XML tree with the data.
   hHandle = bInitFetch( coszCertPath );
   if( hHandle == OSAL_NULL )
   {
      //XML Parsing failed
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "XML Parsing error", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_INITFETCH_XML_PARSE;
   }
#ifdef _DEBUG_TRACE_    
   else if( (tS32)( u32XMLParseTime = ( OSAL_ClockGetElapsedTime() - u32Start ) ) < 0 )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Clock-Time Error", 0, 0, 0, 0 );
   }
#endif
   // This step fetches the relevant data from the XML tree and stores it locally.
   else if( bPerformFetch( hHandle ) == FALSE )
   {
      //Fetch operation failed
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Saving Data to memory failed", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_XML_FETCH_DATA;
   }
#ifdef ENABLE_CERT_LIFETIME_CHECK   
   /* Step 2: Check if Certificate lifetime is >= to current date-time */
   else if( ( bVerifyCertLifetime( hHandle, 0 ) == FALSE ) )
   {
      //Certificate Expired
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Certificate expired", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_CERT_LIFETIME_EXPIRED;
   }
#endif
#ifdef _DEBUG_TRACE_ 
   else if( (tS32)( u32FetchTime = ( OSAL_ClockGetElapsedTime() - u32XMLParseTime ) - u32Start ) < 0 )
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Clock-Time Error", 0, 0, 0, 0 );
   }
#endif   
   /* Step 2: Check if Revoc Timestamp >= Certificate lifetime  */
   else if( bVerifyRevocTimestamp( hHandle, 0 ) == FALSE )
   {
      //Timestamp check failed.
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Revoc Timestamp older than partition 0.", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_REVOC_TS_OLD;
   }
   /* Step 3: Verify if revocation signature is valid */
   else if( bVerifyRevocSignature( hHandle ) == FALSE )
   {
      /* Error in revoc signature */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Revoc. list verify failed", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_REVOC_SIGN_VERIFY;
   }
   /* Step 4: Copy Revocation List */
   else if( bPerformRevocListCopy( hHandle ) == FALSE )
   {
      /* Error while copying revoc list */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Revoc. list copy failed", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_REVOC_LIST_COPY;
   }
   /* Step 5: Check if key is revoked */
   else if( bIsKeyRevoked( pu8GetOperatorKey( hHandle, 0, &u32KeyLength ) ) == TRUE )
   {
      /* Error checking partition Key */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Part. 0 Operator key is revoked.", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_OPKEY_REVOKED;
   }   
   /* Step 6: Verify Operator Signature for partition 0 */
   else if( bVerifyOperatorSignature( hHandle, 0 ) == FALSE )
   {
      /* Error in operator signature */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Part. 0 Operator signature failed", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_OP_SIGN;
   }
//#ifdef VIN_IN_KDS   
   /* Step 7: Check if VIN's are the same */ 
   else if( bCompareVIN( hHandle ) == FALSE )
   {
      /* Error while Comparing VIN-values */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "VIN's do not match", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_COMPARE_VIN;
   }
//#endif
//#ifdef DID_IN_KDS   
   /* Step 7: Check if DID's are the same */ 
   else if( bCompareDID( hHandle ) == FALSE )
   {
      /* Error while Comparing DID-values */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                           "DID's do not match", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_COMPARE_DID;
   }
//#endif

#ifdef _DEBUG_TRACE_ 
   else if( (tS32)( u32TempTime = OSAL_ClockGetElapsedTime() ) < 0 ) 
   {
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Clock-Time Error", 0, 0, 0, 0 );
   }
#endif
    /* Step 8: Perform verification on Partition 0 Signature */
   else if( bVerifyPartSignature( hHandle, 0 )  == FALSE )
   {
      /* Error while creating Hash-values or verify operation */
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                           "Partition 0 signature verify failed.", 0, 0, 0, 0 );
      bResult = FALSE;
      enVerifyStatus = ERROR_SIGN_VER_PART_SIGN;
   }
#ifdef _DEBUG_TRACE_    
   u32PartVerifyTime = OSAL_ClockGetElapsedTime() - u32TempTime;
#endif   

   /* Clear memory allocated for all locally stored data */
   if( hHandle )
   {
      vCloseFetch( hHandle );
   }

#ifdef _DEBUG_TRACE_ 
   u32Total = OSAL_ClockGetElapsedTime() - u32Start;
   fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                        "Total Time Taken", (tS32)u32Total, (tS32)u32XMLParseTime, 
                        (tS32)u32FetchTime, (tS32)u32PartVerifyTime );
#endif                        
   
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enVerifyStatus);
   /* Return result */
   return bResult;
}

//********************** F U N C T I O N  H E A D E R *************************
//
//  DESCRIPTION:
//
//! \brief
//!  This function checks if the signature is based on CID or VIN by parsing the XML certificate 
//!  Inputs required: coszCertPath - Certificate path
//!
//! \return
//!  -1   - Parsing error
//!  XML_CID - CID based signature in XML certificate
//!  XML_VIN  - VIN  based signature in XML certificate
//
//  HISTORY:
//       Date       |            Author            |        MODIFICATION
// ----------------------------------------------------------------------------
//  23 - Dec - 2010 |    Ravindran P  |   Initial Version
//  05 - Apr - 2011 |    Anooj Gopi   | Added multi device support
//  19 - May - 2011 |    Anooj Gopi   | Adapted to changes in fetch layer
//  12 - Mar - 2014 |    Swathi Bolar | DID Support was added INNAVPF-2544
//*****************************************************************************
tS8 s8IsXmlVIN( tCString coszCertPath )
{
   tS8 s8RetVal = 0;
   tPU8 pu8VIN = OSAL_NULL;
   tPU8 pu8DID = OSAL_NULL;
   tFetchHandle hHandle;

   hHandle = bInitFetch( coszCertPath );
   if(  hHandle == OSAL_NULL )
   {
      //XML Parsing failed
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                        "XML Parsing error", 0, 0, 0, 0 );
      s8RetVal = -1;
   }
   // This step fetches the relevant data from the XML tree and stores it locally.
   else if( bPerformFetch( hHandle ) == FALSE )
   {
      //Fetch operation failed
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_ERROR,
                        "Saving Data to memory failed", 0, 0, 0, 0 );
      s8RetVal = -1;
   }
   else if( ( pu8VIN = (tU8*)pcGetVIN( hHandle ) ) == OSAL_NULL )
   {
      //error reading from KDS
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                     "VIN null", 0, 0, 0, 0 );
      s8RetVal = -1;
   }
    else if( ( pu8DID = (tU8*)pcGetDID( hHandle ) ) == OSAL_NULL )
   {
      //error reading from KDS
      fd_crypt_vTraceInfo( TR_LEVEL_USER_4, TR_CRYPT_VERIFY_SF, TR_CRYPT_MSG_INFO,
                     "DID null", 0, 0, 0, 0 );
      s8RetVal = -1;
   }
   else
   {
      if( OSAL_u32StringLength( (tChar *)pu8VIN ) != 0 )
      {
           s8RetVal = XML_VIN; //XML VIN
      }
      else if( OSAL_u32StringLength( (tChar *)pu8DID ) != 0 )
      {
            s8RetVal = XML_DID;//XML DID
      }
     else
    {
        s8RetVal = XML_CID; //XML CID
    }
   }

   if( hHandle )
   {
      vCloseFetch( hHandle );
   }

   return s8RetVal;
}
