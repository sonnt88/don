/*******************************************************************************
*
* FILE:
*     fd_crypt_trace.h
*
* REVISION:
*     1.1
*
* AUTHOR:
*     (c) 2007, Robert Bosch India Ltd., ECM/ECM1, Divya H, 
*                                                  Divya.H@in.bosch.com
*
* CREATED:
*     09/04/2007 - Divya H 
*
* DESCRIPTION:
*     This file contains all the definitions, macros, and types that 
*     are private to the fd_crypt_trace.c
*
* NOTES:
*
* MODIFIED:
*   DATE      |  AUTHOR  |         MODIFICATION
*   25/09/07  |  Divya H |	Modified for function_code:error_stage traces
    24/10/08  | Ravindran| Ported to ADIT platform 							   
    8/06/09   | Ravindran| Warnings Removal 							   
*   22.10.10  |Gokulkrishnan N| VinFeature Porting for FORD-MFD
*   12/03/14  |Swathi Bolar   | DID based implementation (INNAVPF-2544)
*******************************************************************************/
#ifndef _FD_CRYPT_TRACE_H
#define _FD_CRYPT_TRACE_H
#define  FD_CRYPT_TRACE_CLASS TR_CLASS_FD_CRYPT

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

/* Trace messages */  
typedef enum 
{
   TR_CRYPT_MSG_SUCCESS,
   TR_CRYPT_MSG_ERROR,
   TR_CRYPT_MSG_INFO,
   TR_CRYPT_MSG_STRING,
   TR_CRYPT_MSG_FNER

} tenFdCryptTraceMsg;

/* Trace functions enumeration */		 
typedef enum 			
{
   TR_CRYPT_CREATE_HEADER,
   TR_CRYPT_ENCRYPT,
   TR_CRYPT_DECRYPT,
   TR_CRYPT_CREATE,
   TR_CRYPT_DESTROY,
   TR_CRYPT_FILE_CREATE,
   TR_CRYPT_FILE_REMOVE,
   TR_CRYPT_FILE_OPEN,
   TR_CRYPT_FILE_CLOSE,
   TR_CRYPT_FILE_READ,
   TR_CRYPT_FILE_WRITE,
   TR_CRYPT_VERIFY_SF,
   TR_CRYPT_IS_CRYPT_HANDLE,
   TR_CRYPT_IOCTRL,
   TR_CRYPT_IOCTRL_FIOWHERE,
   TR_CRYPT_IOCTRL_FIOSEEK,
   TR_CRYPT_IOCTRL_FIONREAD,
   TR_CRYPT_IOCTRL_DISK_ABORT,
   TR_CRYPT_HANDLE_ENC_READ,
   TR_CRYPT_HANDLE_ENC_WRITE,
   TR_CRYPT_IOCTRL_DEFAULT
} tenFdCryptTraceFunction;

typedef enum
{

	SIGN_VER_NO_ERROR,

	/* fd_crypt File exist check */
	SIGN_VER_FILE_EXIST_NO_ERROR,
	ERROR_SIGN_VER_FILE_OPEN,
	ERROR_SIGN_VER_FILE_CLOSE,
	ERROR_SIGN_VER_FILE_EXIST_SF,



	/* InitFetch */
	SIGN_VER_INITFETCH_NO_ERROR,
	ERROR_SIGN_VER_OPEN_LIBMINXML,
	ERROR_SIGN_VER_INITFETCH_XML_PARSE,

	/* Perform Fetch */
	SIGN_VER_XML_FETCH_NO_ERROR,
	ERROR_SIGN_VER_FETCH_LIFETIME,
	ERROR_SIGN_VER_FETCH_REVOC_TS,
	ERROR_SIGN_VER_FETCH_REVOC_LIST,
	ERROR_SIGN_VER_FETCH_REVOC_SIGN,
	ERROR_SIGN_VER_FETCH_OP_KEY,
	ERROR_SIGN_VER_FETCH_OP_SIGN,
	ERROR_SIGN_VER_FETCH_VIN,
	ERROR_SIGN_VER_FETCH_PART_SIGN,
	ERROR_SIGN_VER_FETCH_FILE_DATA,
	ERROR_SIGN_VER_XML_FETCH_DATA,
	ERROR_SIGN_VER_FETCH_DID,

    /* Verify Cert Life Time */
	SIGN_VER_CERT_LIFETIME_NO_ERROR,
	ERROR_SIGN_VER_CERT_LT_READ_RTC,
	ERROR_SIGN_VER_CERT_LT_COMPARE,
	ERROR_SIGN_VER_CERT_LIFETIME_EXPIRED,

	/* Verify Revoc Time Stamp */
	SIGN_VER_REVOC_TS_NO_ERROR,
	ERROR_SIGN_VER_REVOC_TS_COMPARE,
	ERROR_SIGN_VER_REVOC_TS_OLD,

	/*bVerifyRevocSignature*/
	SIGN_VER_REVOC_SIGN_NO_ERROR,
	ERROR_SIGN_VER_REVOC_SIGN_READ,
	ERROR_SIGN_VER_RB_PUBKEY_READ,
	ERROR_SIGN_VER_REVOC_LIST_TS_FETCH,
	ERROR_SIGN_VER_REVOC_SIGN_HASH_COMPARE,
	ERROR_SIGN_VER_REVOC_SIGN_VERIFY,

	/*bPerformRevocListCopy*/
	SIGN_VER_REVOC_LIST_COPY_NO_ERROR,
	ERROR_SIGN_VER_RL_OPEN_FILE,
	ERROR_SIGN_VER_RL_READ_FILE,
	ERROR_SIGN_VER_RL_CLOSE_FILE,
	ERROR_SIGN_VER_RL_READ_TS,
	ERROR_SIGN_VER_REVOC_LIST_COPY,

	/*bIsKeyRevoked*/
	SIGN_VER_CHECK_REVOCKEY_NO_ERROR,
	ERROR_SIGN_VER_REVOC_FILE_OPEN,
	ERROR_SIGN_VER_REVOC_FILE_READ_TS,
	ERROR_SIGN_VER_REVOC_FILE_READ_DELIMITER,
	ERROR_SIGN_VER_REVOC_FILE_READ_KEYSIZE,
	ERROR_SIGN_VER_REVOC_KEY_READ_MEMALLOC,
	ERROR_SIGN_VER_REVOC_READ_KEY,
	ERROR_SIGN_VER_REVOC_KEY_REVOKED,
	ERROR_SIGN_VER_OPKEY_REVOKED,

	/*bVerifyOperatorSignature*/
	SIGN_VER_OP_SIGN_NO_ERROR,
	ERROR_SIGN_VER_OPSIGN_READ_OP_PUBKEY,
	ERROR_SIGN_VER_OPSIGN_FETCH_RB_PUBKEY,
	ERROR_SIGN_VER_OPSIGN_FETCH_OP_SIGN,
	ERROR_SIGN_VER_OPSIGN_VERIFY,
	ERROR_SIGN_VER_OP_SIGN,

	/* bCompareVIN */
	SIGN_VER_COMPARE_VIN_NO_ERROR,
	ERROR_SIGN_VER_NO_XML_VIN,
	ERROR_SIGN_VER_NO_FS_VIN,
	ERROR_SIGN_VER_DIFF_VIN,
	ERROR_SIGN_VER_INSUFF_XML_VINDATA,
	ERROR_SIGN_VER_COMPARE_VIN,

	/* bVerifyPartSignature*/
	SIGN_VER_PART_SIGN_NO_ERROR,
	ERROR_SIGN_VER_PSIGN_CREATE_HASH,
	ERROR_SIGN_VER_PSIGN_READ_PARTSIGN,
	ERROR_SIGN_VER_PSIGN_READ_OPKEY,
	ERROR_SIGN_VER_PSIGN_VERIFY,
	ERROR_SIGN_VER_PART_SIGN,
    
    /* bCompareDID */
    SIGN_VER_COMPARE_DID_NO_ERROR,
    ERROR_SIGN_VER_NO_XML_DID,
    ERROR_SIGN_VER_NO_FS_DID,
    ERROR_SIGN_VER_DIFF_DID,
    ERROR_SIGN_VER_INSUFF_XML_DID,
    ERROR_SIGN_VER_COMPARE_DID,

	
} tenSignVerifyTrace;
/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/
 
/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************************
*
* FUNCTION:
*     fd_crypt_vTraceInfo
*
* DESCRIPTION:
*     This function creates the trace message and sends it to PC*     
*     
* PARAMETERS:
*
* RETURNVALUE:
*     None	       
*
* HISTORY:
*
*****************************************************************************/
tVoid fd_crypt_vTraceInfo(TR_tenTraceLevel enTraceLevel,
                         tenFdCryptTraceFunction enFunction,
						 tenFdCryptTraceMsg enLldMmcTraceMsg,
                         tPCChar copchDescription,
                         tS32 s32Par1, 
                         tS32 s32Par2, 
                         tS32 s32Par3, 
                         tS32 s32Par4);


#endif /* #ifndef _FD_CRYPT_TRACE_H */ 
