/*!\file
 * @brief Private header file included only inside the library.
 *
 * @author Arvind Devarajan (RBEI/ECF)
 * @date   19.Mar.2010
 */

#include "libminxml_plat.h"

#ifndef LIBMINXML_PRIVATE_H_
#define LIBMINXML_PRIVATE_H_

/*!\def LIBMINXML_CHAR_SPACE
 * @brief Defines the "space" character according to XML 1.0 Edition 5
 */
#define LIBMINXML_CHAR_SPACE 0x20

/*!\def LIBMINXML_SZ_ERRSTR
 * @brief Maximum size of an error string
 */
#define LIBMINXML_SZ_ERRSTR 100

/*!\enum state
 * @brief Parser states
 *
 * The parser goes through these states while parsing the XML file.
 * Each of these states have a "state function" that handles what
 * needs to be done in that state.
 */
enum state
{
	LIBMINXML_STATE_INVALID,      //!< Invalid state
	LIBMINXML_STATE_INIT,         //!< Initial state
	LIBMINXML_STATE_DECL,         //!< XML Declaration obtained
	LIBMINXML_STATE_NEWELEMENT,   //!< Token '\<' obtained
	LIBMINXML_STATE_ELENAME,      //!< Element's name started
	LIBMINXML_STATE_TOKSPC,       //!< Space obtained between \< and \>
	LIBMINXML_STATE_ATTNAME,      //!< Attribute's name started
	LIBMINXML_STATE_TOKEQ,        //!< Token '=' obtained
	LIBMINXML_STATE_ATTVAL,       //!< Attribute value started
	LIBMINXML_STATE_ENDNEWELEMENT,//!< Token '\>' obtained after new element start
	LIBMINXML_STATE_CONTENT,      //!< Element's content started
	LIBMINXML_STATE_ENDBEG,       //!< Element's content ended. Beginning closure ("</" obtained).
	LIBMINXML_STATE_ENDEND,       //!< Ended closure ('>' obtained)
	LIBMINXML_STATE_COMMENT,      //!< Comment has just begun
	LIBMINXML_STATE_MAX           //!< Maximum number of states of parser
};

/*!\typedef parser_state
 * \copydoc state
 */
typedef enum state parser_state;

/*!\enum ct
 * @brief Character types as mentioned in the XML 1.0 (fifth edition) specs
 */
enum ct
{
   NameStartChar,//!< NameStartChar
   NameChar,     //!< NameChar
   Whitespace    //!< Whitespace
};

/*!\typedef ctype
 * \copydoc ct
 */
typedef enum ct ctype;

/*!\struct char_range
 * @brief ASCII range of characters used for validation in function isinrange()
 */
struct char_range
{
   tU32 start; //!< Start of the character range
   tU32 end; //!< End of the character range
};

/*!\typedef range
 * \copydoc char_range
 */
typedef struct char_range range;


/*!\struct attr
 * @brief XML element attribute's name and its value.
 *
 * For example, an XML element defined like this:
 *
 * \code
 * <myelement attr1=val1 attr2=val2>
 * \endcode
 *
 * will have the attribute list as:
 *
 * \code
 * pattr="attr1"                    +----> pattr="attr2"
 * pattrval="val1"                  |      pattrval="val2"
 * pnext -----------------Points to-+      pnext=NULL
 * \endcode
 */
struct attr
{
	tChar *pattr;   //!< Name of the attribute
	tChar *pattrval; //!< Value of the attribute
	struct attr *pnext; //!< Next attribute belonging to the same XML element
};

/*!\typedef attr_t
 * \copydoc attr
 */
typedef struct attr attr_t;

/*!\struct element
 * @brief XML element, its attributes, and it's contents.
 *
 * This structure is the core of this XML parser. It contains a single
 * XML element, and its attributes. Also, it contains a content if the XML
 * element was bounded within a "content" string; else, this is NULL.
 *
 * For example, consider this structure:
 * \code
 * <person attr1=val1>
 *     <name> NAME </name>
 *     <age> AGE </age>
 * </person>
 * \endcode
 *
 * In the above case, there are three XML elements "person," "name," and
 * "age". The structure for "person" element would be:
 *
 * \code
 * pname="person"
 * pattr=<pointer to a list of attr_t>
 * pcontent=NULL // Since we have no content for this element
 * child=<pointer to "name" element below>
 * sibling=NULL  // Since we have no other XML element at this depth
 * \endcode
 *
 * For "name," we'll have:
 *
 * \code
 * pname="name"                   +--> pname="age"
 * pattr=NULL                     |    pattr=NULL // No attributes
 * pcontent="NAME"                |    pcontent="AGE"
 * pchild=NULL                    |    pchild=NULL // No next depth
 * psibling ---- Points to -------+    psibling=NULL // No more elements at
 *                                                   // the same depth
 * \endcode
 */
struct element
{
	tChar *pname; //!< Name of the XML element
   attr_t *pattr;  //!< Pointer to the attributes of this XML element.
	tChar *pcontent; //!< Content of the element if present; NULL otherwise.
	struct element *pchild; //!< XML element at the next depth as this
	struct element *psibling; //!< XML element at the same depth as this
	struct element *pparent; //!< XML element of the parent
};

/*!\typedef element_t
 * \copydoc element
 */
typedef struct element element_t;

/*!\struct _result_set
 * @brief System result set that is returned to calling function.
 *
 * This contains a pointer to the active element and its attribute.
 * It is used by the higher level API's to returns its various
 * properties to the user.
 */
struct _result_set
{
   element_t *pelement;
   attr_t *pattr;
};

/*!\struct global
 * @brief System global handle that is returned to the caller
 *
 * This contains various global variables that the parser needs from
 * time to time for working.
 */
struct global
{
   hFile pfile; //!< File handle of the XML file
	element_t *proot; //!< Pointer to the root XML element of the file (pointer to first element)
	tU32 curline; //!< Current line number of the XML file that is parsed
	tU32 col; //!< Column number of the current character
	tVoid *plog; //!< Logging context
};

/*!\typedef global_t
 * \copydoc global
 */
typedef struct global global_t;

/* Functions used only within libminxml */
tVoid libminxml_priv_init(global_t *pgbl);
tVoid libminxml_priv_cleanup(global_t *pgbl);
tU32 libminxml_priv_parse(global_t *pgbl);
element_t *libminxml_priv_find(const global_t *pgbl, const tChar *ppath);

#endif /* LIBMINXML_PRIVATE_H_ */
