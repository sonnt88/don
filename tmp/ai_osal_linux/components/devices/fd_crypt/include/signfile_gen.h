#ifdef FD_CRYPT_NON_XML_SUPPORT//Obsolete Code
/*******************************************************************************
 *
 * FILE:
 *     signfile_gen.h
 *
 * REVISION:
 *     1.0
 *
 * AUTHOR:
 *     (c) 2007, Robert Bosch India Ltd., ECM/ECM-1, Divya H, 
 *                                                   divya.h@in.bosch.com
 *
 * CREATED:
 *     28/02/2007 - Divya H
 *
 * DESCRIPTION:
 *       This file contains the signature file generation code.
 *
 * NOTES:
 *
 * MODIFIED:
 *   DATE      |  AUTHOR  |         MODIFICATION
 *			   |		  |
 ******************************************************************************/
#ifndef _SIGNFILE_GEN_H
#define _SIGNFILE_GEN_H

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#define I_SF_1		0x1101 /*register service reached*/
#define I_SF_2		0x1102 /*unregister service reached*/

#define E_SF_1		0x2202	/*reg failed*/
#define E_SF_2		0x2203	/*null buf got*/
#define E_SF_3		0x2204	/*unreg failed*/

#define CRYPT_LOAD_PRIKEY	0x01
#define CRYPT_WRITE_SF		0x02
#define CRYPT_VERIFY_SF		0x03
#define CRYPT_KDS_WRITE		0x04
#define EXT_CRYPT_WR_SF		0x05
#define EXT_CRYPT_VERI_SF	0x06

#define SIG_FILE_FAIL 		0x88


/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: module-local)
|----------------------------------------------------------------*/

/* External functions */
extern int sd_sign(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPrivKey,	// 20-bytes signer's private key
	tU8* pSigBuffer			// buffer to hold signature (>= 48bytes)
);
extern int sd_get_cid_key(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPubKey,		// 40-bytes signer's public key
	tU8* pSig,				// signature read from SD card (48 bytes)
	tU8* pKeyBuffer			// buffer to hold encryption key (>= 16 bytes)
);

extern int cryptext_sd_sign(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPrivKey,	// 20-bytes signer's private key
	tU8* pSigBuffer			// buffer to hold signature (>= 48bytes)
);

extern int cryptext_sd_verify(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPubKey,		// 40-bytes signer's public key
	tU8* pSig,				// signature read from SD card (48 bytes)
	tCString coszNavRootPath // Navigation NAV_ROOT.DAT file path
);


tS32 fd_crypt_load_prikey( tS32 s32CryptDevID );
tS32 fd_cryptext_write_sig( tS32 s32CryptDevID );
tS32 fd_cryptext_verify_sig( tS32 s32CryptDevID );
tVoid fd_crypt_vRegisterService (tVoid);
tVoid fd_crypt_vUnregisterService (tVoid);
tVoid fd_crypt_vChannelMgr(const tUChar* puchData);

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /*ifdef _SIGNFILE_GEN_H*/
#endif
