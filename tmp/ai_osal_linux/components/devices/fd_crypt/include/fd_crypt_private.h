/*******************************************************************************
 *
 * FILE:
 *     fd_crypt_private.h
 *
 * REVISION:
 *     1.3
 *
 * AUTHOR:
 *     (c) 2007, Robert Bosch India Ltd., ECM/ECM-1, Divya H, 
 *                                                   divya.h@in.bosch.com
 *
 * CREATED:
 *     25/01/2007 - Divya H
 *
 * DESCRIPTION:
 *     This file contains the private functions & data elements 
 *     for the module FD_CRYPT
 *
 * NOTES:
 *
 * MODIFIED:
 *    24/03/08  |  Divya H    |Modified for Crypt extension signature file handling in VW
 *    28/03/08  |  Divya H    |Modified for file remove functionality for VW
 *    27/05/08  |  Divya H    | Modified for performing navigation copy in VW (MMS 187997)
 *    23/02/09  | Ravindran P | Root directory changed to cryptnav
 *    8/06/09   | Ravindran P | Warnings Removal
 *    22/10/10  |Gokulkrishnan N| VinFeature Porting for FORD-MFD
 *    05/04/11  | Anooj Gopi  | Added multi device support
 *    09/02/12  | Anooj Gopi  | KDS_ECC_PUBKEY_ENTRY value is corrected
 ******************************************************************************/

#ifndef _FD_CRYPT_PRIVATE_H
#define _FD_CRYPT_PRIVATE_H

/*****************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|----------------------------------------------------------------*/
/* FD CRYPT public interface */
#include "fd_crypt.h"
#include "fetch_cert_data.h"

/* Crypt Library interfaces */

#include "bpcl.h"
#include "bpcl_int.h"
#include "ecc.h"

/* external driver interfaces */
#include "system_kds_def.h"

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

//Used to comment out obsolete code for NONXML certificate verification
//#define FD_CRYPT_NON_XML_SUPPORT //NOTE: Code Not compiled and tested

/* Number of encrypted bytes in a file */
#define FD_CRYPT_NBYTES 		(32)
/* Length of AES key */
#define AES_KEY_LEN 			(16)
/* Size of Signature file */
#define SIGN_FILE_SIZE			(48)
/* Length of ECC public key */
#define ECC_PUB_KEY_LEN 		(40)

//#define CID_LEN             (16)//defined in fetch_cert_data.h
/* Entry number of ECC public key in KDS */
#define KDS_ECC_PUBKEY_ENTRY M_KDS_ENTRY(KDS_TARGET_SECURITY, KDS_TYPE_ECC_PUBLIC_KEY);

#define FD_CRYPT_MAX_DEVICES 8

#if FD_CRYPT_NON_XML_SUPPORT
/* Crypt Signature File path for Ford MFD */
#define EXT_CR_SF_PATH		"cryptnav/DATA/DATA/MISC/SD_META.DAT"

/* nav_root.dat path */
#define NAV_ROOT_PATH	  	"cryptnav/DATA/CONNECT/RNW/NAV_ROOT.DAT"
#endif

/* KDS Device */
#define KDS_DEVICE				"/dev/kds"

#if FD_CRYPT_NON_XML_SUPPORT//Obsolete. Used for Non-XML certificate
/* Default crypt routing directory name's length */
#define CRYPT_FOLDER_NM_LEN		(9)
/* Crypt device specific folder name for default crypt access routing */
#define CRYPT_FOLDER			"cryptnav"
/* Concatenation string while using cryptcard directory */
#define CONCAT_STRING_FOLDER	"cryptnav/"

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/

typedef struct
{
	//signfile
	 tU8 au8Pub[ECC_PUB_KEY_LEN]; //Obsolete. Used for Non-XML certificate
	 tU8 au8ReadSignature[BPCL_ECC_SIGNATURE_SIZE];
	 tU8 au8CID[CID_LEN];
	 tU8 au8RBPubKeybuf[ECC_PUB_KEY_LEN];
	 tBool bCryptExtSFVeriSuc;
}trFd_Crypt_Key;
#endif

typedef struct
{
   tChar szDevName[FD_CRYPT_DEV_NAME_MAX_LEN];
   fd_crypt_tenVerifyState enGlobalVerifyState;
   fd_crypt_tenError enGlobalVerifyResult;
} trFdCryptDevDB;

typedef struct
{
   trFdCryptDevDB arFdCryptDevDB[FD_CRYPT_MAX_DEVICES];
   OSAL_tSemHandle hFD_CryptDBSemaphore; /* Lock for the database */
   OSAL_tSemHandle hFD_CryptBPCLSemaphore; /* Lock for the database */
   tU8 au8RBPublicKey[ECC_PUB_KEY_LEN]; /* Pointer to public key  */
} trFdCryptPvtData;

extern trFdCryptPvtData *prFdCryptPvtData;

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/
/*Not needed as function definition is part of product.Not removed for reference */
#if 0
extern int sd_get_cid_key(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPubKey,		// 40-bytes signer's public key
	tU8* pSig,				// signature read from SD card (48 bytes)
	tU8* pKeyBuffer			// buffer to hold encryption key (>= 16 bytes)
);

extern int cryptext_sd_verify(
	const tU8* pCID,		// 16-bytes CID register content
	const tU8* pPubKey,		// 40-bytes signer's public key
	tU8* pSig				// signature read from SD card (48 bytes)
	tCString coszNavRootPath // Navigation NAV_ROOT.DAT file path
);
#endif

/*****************************************************************************
 *
 * FUNCTION:
 *    fd_crypt_sdx_xml_verification
 *
 * DESCRIPTION:
 *    This function is called by fd_crypt_verify_signaturefile to verify XML based signature file.
 *
 *
 * PARAMETERS:
 *    coszCertPath  : Certificate file path.
 *
 * RETURNVALUE:
 *    FD_CRYPT_SUCCESS                      - Success
 *    ERROR_FD_CRYPT_SIGNVERI_FAILED        - Signature file verification failed
 *    ERROR_FD_CRYPT_UNKNOWN                - Unknown
 *
 *
 * HISTORY:
 *    DATE      |  AUTHOR    |         MODIFICATION
 *
 *   13/12/10   | kgr1kor    | Added a separate function to verify xml based signature file
 *   05/04/11   | Anooj Gopi | Added multi device support
 *
 *****************************************************************************/
fd_crypt_tenError fd_crypt_sdx_xml_verification( tCString coszCertPath );

#ifdef FD_CRYPT_NON_XML_SUPPORT//Obsolete Code
/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_sd_non_xml_verification
 *
 * DESCRIPTION:
 *     This function is called by fd_crypt_verify_signaturefile to verify NON-XML based signature file.
 *
 *
 * PARAMETERS:
 *    s32CryptDevID:    Crypt Device ID
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS                      - Success
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED        - Signature file verification failed 
 *     ERROR_FD_CRYPT_UNKNOWN                - Unknown
 * 
 *
 * HISTORY:
 *   DATE      |  AUTHOR  |         MODIFICATION
 *
 *   13/12/10  | kgr1kor  | Added a separate function to verify non_xml based signature file
 *   19/03/11  | Anooj Gopi  |  Added multi device support
 *****************************************************************************/
fd_crypt_tenError fd_crypt_sd_non_xml_verification( tS32 s32CryptDevID );
#endif

/*****************************************************************************
 *
 * FUNCTION:
 *     FD_Crypt_vEnterCriticalSection
 *
 * DESCRIPTION                                                          
 *     Semaphore wait for device specific critical section
 *
 * PARAMETERS:
 *     None
 *
 * RETURNVALUE:
 *     TRUE                      - Success
 *     FALSE                     - Failure
 *
 * HISTORY:
 *     DATE      |  AUTHOR        |         MODIFICATION
 *    09/04/10   | Anoop Chandran |  Created 1st version
 *    05/04/11   | Anooj Gopi     |  Made locking specific to device
************************************************************************/
tBool FD_Crypt_vEnterCriticalSection( tVoid );

/*****************************************************************************
 *
 * FUNCTION:
 *    FD_Crypt_vLeaveCriticalSection
 *
 * DESCRIPTION                                                          
 *    Semaphore post for device specific critical section
 *
 * PARAMETERS:
 *
 * RETURNVALUE:
 *	 tVoid
 *
 * HISTORY:
 *     DATE      |  AUTHOR        |         MODIFICATION
 *	 09/04/10   | Anoop Chandran |	   Created 1st version 
 *    05/04/11   | Anooj Gopi     |  Made locking specific to device
************************************************************************/
tVoid FD_Crypt_vLeaveCriticalSection( tVoid );

/*****************************************************************************
 *
 * FUNCTION:
 *    FD_Crypt_vEnterBPCLCriticalSec
 *
 * DESCRIPTION
 *    Semaphore wait for Non-reentrant BPCL functions
 *
 * PARAMETERS:
 *    None
 *
 * RETURNVALUE:
 *    TRUE                        - Success
 *    FALSE                     - Failure
 *
 * HISTORY:
 *   DATE      |  AUTHOR        |         MODIFICATION
 *  05/04/11   | Anooj Gopi     |      Created 1st version
************************************************************************/
tBool FD_Crypt_vEnterBPCLCriticalSec( tVoid );

/*****************************************************************************
 *
 * FUNCTION:
 *    FD_Crypt_vLeaveBPCLCriticalSec
 *
 * DESCRIPTION
 *    Semaphore wait for Non-reentrant BPCL functions
 *
 * PARAMETERS:
 *    None
 * RETURNVALUE:
 *    None
 * HISTORY:
 *    DATE      |  AUTHOR        |         MODIFICATION
 *  05/04/11    | Anooj Gopi     |     Created 1st version
************************************************************************/
tVoid FD_Crypt_vLeaveBPCLCriticalSec( tVoid );
 
/*****************************************************************************
 *
 * FUNCTION:
 *     bGetDeviceName
 *
 * DESCRIPTION:
 *     This function extracts device name from the certificate path
 *
 * PARAMETERS:
 *     szDevName - (Output) Device Name (buffer of size FD_CRYPT_DEV_NAME_MAX_LEN)
 *     coszCertPath - (Input) Certificate Path
 *
 * RETURNVALUE:
 *     TRUE - Successfully extracted device name from the path
 *     FALSE - Couldn't get the device name from the path
 *
 * HISTORY:
 *     05/04/11    |   Anooj Gopi   |   Created 1st version
 *
 *****************************************************************************/
tBool bGetDeviceName( tString szDevName, tCString coszPath );

#endif /* #ifndef _FD_CRYPT_PRIVATE_H */
