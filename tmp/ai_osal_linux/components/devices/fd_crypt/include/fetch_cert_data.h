
#ifndef _FETCH_CERT_DATA_H
#define _FETCH_CERT_DATA_H

#include "fd_crypt_trace.h"

#define TIMESTAMP_LEN               20

#define REVOC_TIMESTAMP             "/RBCM_SIGNATURE/REVOKED_KEYS"
#define ATTR_REVOC_TIMESTAMP        "timestamp"
#define REVOC_LIST_KEY              "/RBCM_SIGNATURE/REVOKED_KEYS/LIST/KEY"
#define REVOC_SIGNATURE             "/RBCM_SIGNATURE/REVOKED_KEYS/BOSCH_SIGNATURE"

#define PART_OPERATOR_KEY           "/RBCM_SIGNATURE/PARTITION/OPERATOR_PUBLIC_KEY/KEY"
#define PART_OPERATOR_SIGN_DATA     "/RBCM_SIGNATURE/PARTITION/OPERATOR_PUBLIC_KEY/BOSCH_SIGNATURE"

#define PARTITION_SIGNATURE_DATA    "/RBCM_SIGNATURE/PARTITION/OPERATOR_SIGNATURE"

#define PARTITION_ID                "/RBCM_SIGNATURE/PARTITION/ID"
#define ATTR_VIN                    "vin"
#define VIN_LEN                     40
#define ATTR_DID                   "did"
/*Max DID Length is set as 40 , but the actual DID length is 17*/
#define DID_LEN                     40 
#define ATTR_CID                    "cid"
#ifndef CID_LEN
   #define CID_LEN                  16
#endif

#define PARTITION_CERT_LIFETIME     "/RBCM_SIGNATURE/PARTITION/LIFETIME"
#define ATTR_LIFETIME_START         "start"
#define ATTR_LIFETIME_END           "end"

#define PARTITION_FILEDATA_FILELIST "/RBCM_SIGNATURE/PARTITION/HASH_FILE_LIST/HASH_FILE"
#define ATTR_FILEDATA_LEN           "numberofbytes"
#define ATTR_FILEDATA_OFFSET        "offset"
#define ATTR_FILEDATA_FILENAME      "filename"

struct _array
{
	tPU8 pu8Array;
	tU32 u32KeyLength;
};

typedef struct _array trArray;

struct _list
{
	trArray* pList;
	tU16 u16ListLength;
};

typedef struct _list trList;

struct _file_data
{
   trArray rArray;
   tChar   acFilename[255];
   tU32    u32Offset;
   tU32    u32NumOfBytes;
};

typedef struct _file_data trFileData;

struct _file_data_list
{
   trFileData* pFileList;
   tU16        u16ListLength;
};

typedef struct _file_data_list trFileList;

/* Path for LearnedVIN file */
//#define CRYPT_VIN_PATH			"/dev/nor0/LearnedVIN"

#define CRYPT_VIN_PATH_GMGE "/dev/ffs/LearnedVIN"
#define VIN_HEADER_SEEK_GMGE (1)
#define VIN_LEN_GMGE		 (16)

#define CRYPT_DID_PATH "/dev/ffs/LearnedDID"
#define DID_HEADER_SEEK (1)
#define DID_LEN_FFS         (16)
#define XML_DID   (2)
#define XML_VIN   (1)
#define XML_CID   (0)



typedef struct
{
	tString 	strCryptVinPathSet;
	tU32 		u32VinHeaderSeekValue;
	tU32		u32VinLengthValue;
    tU16		u16VinKDSRBPubKeyEntry;
    tPU8        pu8KDSRBPubKeyData;
}t_CryptVINPathHeader_Setting;

#define FD_CRYPT_DEV_NAME_MAX_LEN ( (20) + 64 )

typedef struct
{
   /* Default Variables for Fetch operation */
   tVoid*          hXMLCert;
   trList          rRevocList;
   trArray         rRevocSign;
   trArray         rOperatorKey;
   trArray         rOperatorSignature;
   trArray         rPartitionSignature;
   trFileList      rFileList;
   OSAL_trTimeDate rLifeTimeStart;
   OSAL_trTimeDate rLifeTime;
   OSAL_trTimeDate rRevocTimestamp;
   tChar           acRevocTimestamp[TIMESTAMP_LEN];
   tChar           acVIN[VIN_LEN+1];
   tChar           acLifetimeStart[TIMESTAMP_LEN];
   tChar           acLifetimeEnd[TIMESTAMP_LEN];
   tU8             au8CID[2*CID_LEN + 1];
   tBool           bIsRevocListPresent;
   tChar           acDevName[FD_CRYPT_DEV_NAME_MAX_LEN];
   tChar           acDID[DID_LEN+1];
} trCryptDevFetchPvtData;

typedef trCryptDevFetchPvtData *tFetchHandle;
typedef const trCryptDevFetchPvtData *tcFetchHandle;

trFileList*      trGetFileData( tFetchHandle hHandle );
trArray*         trGetPartSignature( tFetchHandle hHandle );
trArray*         trGetOperatorSignature( tFetchHandle hHandle );
trArray*         trGetOperatorKey( tFetchHandle hHandle );
trArray*         trGetRevocationSignature( tFetchHandle hHandle );
trList*          trGetRevocationList( tFetchHandle hHandle );
tBool trGetCertificateLifetime( tcFetchHandle hHandle, OSAL_trTimeDate *prCertLifeTime );
tBool trGetCertLifetimeStart( tcFetchHandle hHandle, OSAL_trTimeDate *prTimeDate );
tBool trGetRevocTimestamp( tcFetchHandle hHandle, OSAL_trTimeDate *prTimeDate );
tPChar           pcGetRevocTimestamp( tFetchHandle hHandle );
tPChar           pcGetVIN( tFetchHandle hHandle );
tPU8             pu8GetCID( tFetchHandle hHandle );
tPChar           pcGetLifetimeStart( tFetchHandle hHandle );
tPChar           pcGetLifetimeEnd( tFetchHandle hHandle );
tVoid            vCloseFetch( tFetchHandle hHandle );
tBool            bPerformFetch( tFetchHandle hHandle );
tFetchHandle     bInitFetch( tCString coszCertPath );
tBool            bIsXmlVIN( tFetchHandle hHandle );
tBool            bIsXmlDID( tFetchHandle hHandle );
tPChar           pcGetDID( tFetchHandle hHandle );

#endif //_FETCH_CERT_DATA_H
