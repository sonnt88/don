/*!\file
 * @brief Platform specific header file
 *
 * @author Arvind Devarajan (RBEI/ECF)
 * @date   26.Mar.2010
 * @history  
 * 21.Jun.2010  v1.1   XML-Cache mode implemented for performance.
 * 05.Apr.2011  v1.2   Introduced new handle structure to make functions reentrant | Anooj Gopi (RBEI/ECF1)
 */

#ifndef LIBMINXML_PLAT_H_
#define LIBMINXML_PLAT_H_

#define __XML_CACHE__

#ifdef __CYGWIN__
typedef char tChar;
typedef unsigned char tU8;
typedef int tS32;
typedef unsigned int tU32;
typedef FILE *hFile;
typedef void tVoid;

/*!\enum boolval
 * @brief For boolean values
 */
enum boolval
{
   FALSE,
   TRUE
};

/*!\typedef bool
 * \copydoc boolval
 */
typedef enum boolval tBool;

#else

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "fd_crypt_trace.h"

#ifdef __XML_CACHE__
typedef struct
{
   tPS8 ps8XMLData;
   tS32 s32DataLength;
   tS32 s32DataPointer;
   OSAL_tIODescriptor hXmlFile;
} tXmlFileHandle;

typedef tXmlFileHandle *hFile;

#else
typedef OSAL_tIODescriptor hFile
#endif

#endif

/* Function prototypes */
hFile libminxml_plat_fopen(const tChar *pfile, tVoid *pplat);
void libminxml_plat_fprintf(tVoid *pplat, const char *str);
tChar libminxml_plat_fgetc(hFile pfile);
void libminxml_plat_ungetc(tChar c, hFile pfile);
void libminxml_plat_fclose(hFile pfile);
tU32 libminxml_plat_loginit(void **ppplat);

#endif /* LIBMINXML_PLAT_H_ */
