/*!\file
 * @brief Public .h file to be included by components needing to use
 *         libminxml.
 *
 * @author Arvind Devarajan (RBEI/ECF)
 * @date 19.Mar.2010
 *
 * @mainpage Minimalistic XML parser library
 * libminxml is an "minimalistic" XML parser library written completely
 * in ANSI C. Since this is a "minimalistic" XML parser, we have certain
 * limitations:
 *
 * @li It does not contain any validation of XML DTDs
 * @li Does only a basic sanity check on the input XML file.
 * @li Does not have any Unicode support - i.e., it assumes that the
 * XML file that it parses contains only 8-bit ASCII characters, and
 * discards the xml declaration (if present) at the beginning of the file.
 * @li Also, it considers any other (valid) XML syntax like <!DOCTYPE, <!ELEMENT,
 * etc. as invalid, and actually errors out!
 *
 * Apart from the above, it almost conforms to XML Version 1.0 (fifth edition)
 * specifications found at: http://www.w3.org/TR/2008/REC-xml-20081126/
 *
 * @note The "examples" tab in the doxygen generated documentation
 *       shows an example of the usage of this library.
 *
 * @version 09.Apr.2010:Jeryn Mathew (RBEI/ECF1)   : Added new high-level APIs
 * @version 26.Mar.2010:Jeryn Mathew (RBEI/ECF1)   : Added "find element" code
 * @version 26.Mar.2010:Jeryn Mathew (RBEI/ECF1)   : Added "destroy element" code
 * @version 19.Mar.2010:Arvind Devarajan (RBEI/ECF): Created initial version
 * @version 19.Mar.2010:Jeryn Mathew (RBEI/ECF1)   : Added function to reset
 *                                                  attr-list pointer in result-set.
 */

#include "libminxml_plat.h"

#ifndef LIBMINXML_H_
#define LIBMINXML_H_

/*!\typedef rset_t
 * \copydoc _result_set
 */
typedef struct _result_set rset_t;

/* ------------ LibMinXML APIs ------------*/
/*!\fn tVoid *libminxml_open(const tChar *pfile);
 * @brief Open an XML file, and return the XML handle
 * @param pfile [in] Name of the file to open.
 * @return Handle of XML.
 */
tVoid *libminxml_open(const tChar *pfile);

/*!\fn tVoid libminxml_close(tVoid* phandle)
 * @brief Close the XML file, and release all resources.
 * @param phandle [in] Handle returned by libminxml_open().
 */
tVoid libminxml_close(tVoid* phandle);

/*!\fn tChar *libminxml_getcontent(const tVoid *phandle, const tChar *ppath, tVoid **pplasts)
 * @brief Gets the content of the XML element pointed to by ppath.
 *
 * If ppath==NULL, it works like strtok() - it just gets the next element
 * with the same tag name as the last call, which lies at the same path
 * as the last call too; and returns the value of this element.
 *
 * @param phandle [in] Handle returned by libminxml_open().
 * @param ppath [in] Path for the XML element.
 * @param pplasts [inout] Used internally by this function (caller should send
 *                        the address of a pointer where this function will store
 *                        its internal state).
 * @return Content of this element (if found)
 * @return NULL if no element found at this path
 * @example example_usage.c
 *
 * Suppose we have an XML file as follows:
 *
 * @code
 * <person>
 *      <name> NAME </name>
 *      <education>
 *          <ug> UG1 </ug>
 *          <ug> UG2 </ug>
 *     </education>
 * </person>
 * @endcode
 *
 * Then, to get the content of the "pg" in "education," use:
 * @code
 * char *pug = libminxml_getcontent(phandle, "/person/education/ug");
 * @endcode
 *
 * "pug" after this call gives "UG". The complete code for this is given
 * here:
 */
tChar *libminxml_getcontent(const tVoid *phandle, const tChar *ppath, tVoid **pplasts);

/*!\fn rset_t *libminxml_get_rset(const tVoid *phandle, const tChar *ppath)
 * @brief Prepares a result set for iterating through the XML tree.
 *
 * @param phandle [in] Handle returned by libminxml_open().
 * @param ppath [in] Path for the XML element.
 * @return A result set of active node. The internal structure can only be
 *         accessed by other public APIs.
 * @return NULL if no element found at this path
 */
rset_t *libminxml_get_rset(const tVoid *phandle, const tChar *ppath);

/*!\fn tChar* libminxml_get_element_name(const rset_t* presult)
 * @brief Returns the name of the active element pointed by the result set
 *
 * @param presult [inout] Handle returned by libminxml_get_rset().
 * @return Name of the element. A character pointer that is NULL terminated.
 * @return NULL if no element found at this path
 */
tChar* libminxml_get_element_name(const rset_t* presult);

/*!\fn tChar* libminxml_get_content(const rset_t* presult)
 * @brief Returns the content of the active element pointed by the result set.
 *
 * If the return is NULL, this could represent either the end of the particular
 * branch, or it could mean there are children (leaves) to this element.
 *
 * @param presult [inout] Handle returned by libminxml_get_rset().
 * @return Content of the element
 * @return NULL if no element found at this path
 */
tChar* libminxml_get_content(const rset_t* presult);

/*!\fn tChar* libminxml_get_attr_name(const rset_t* presult)
 * @brief Returns the name of the attribute to the active element
 *        pointed by the result set.
 *
 * @param presult [inout] Handle returned by libminxml_get_rset().
 * @return Name of the attribute
 * @return NULL if no more attributes are found at this path
 */
tChar* libminxml_get_attr_name(const rset_t* presult);

/*!\fn tChar* libminxml_get_attr_value(const rset_t* presult)
 * @brief Returns the value of the attribute to the active element
 *        pointed by the result set.
 *
 * @param presult [inout] Handle returned by libminxml_get_rset().
 * @return Value of the attribute
 * @return NULL if no more attributes are found at this path
 */
tChar* libminxml_get_attr_value(const rset_t* presult);

/*!\fn tVoid libminxml_get_next_element(rset_t* presult)
 * @brief Updates the result set by pointing to the next element
 *        along the same branch.
 *
 * Suppose we have an XML file as follows:
 *
 * @code
 * <person>
 *      <name> NAME </name>
 *      <education>
 *          <ug> UG1 </ug>
 *          <ug> UG2 </ug>
 *     </education>
 * </person>
 * @endcode
 *
 * If result set pointed to "/person/name", this function will point
 * the result set to "/person/education".
 *
 * @param presult [inout] Handle returned by libminxml_get_rset().
 * @return None
 */
tVoid libminxml_get_next_element(rset_t* presult);

/*!\fn tVoid libminxml_get_next_level_element(rset_t* presult)
 * @brief Updates the result set by pointing to the first element
 *        in the next branch.
 *
 * Suppose we have an XML file as follows:
 *
 * @code
 * <person>
 *      <name> NAME </name>
 *      <education>
 *          <ug> UG1 </ug>
 *          <ug> UG2 </ug>
 *     </education>
 * </person>
 * @endcode
 *
 * If result set pointed to "/person/education", this function will point
 * the result set to "/person/education/ug".
 *
 * @param presult [inout] Handle returned by libminxml_get_rset().
 * @return None
 */
tVoid libminxml_get_next_level_element (rset_t* presult);

/*!\fn tVoid libminxml_free_rset( rset_t *presult )
 * @brief Frees the result set created by libminxml_get_rset()
 *
 * @param presult [in] Handle returned by libminxml_get_rset().
 * @return None
 */
tVoid libminxml_free_rset (rset_t *presult);

/*!\fn tBool libminxml_validate_result_set ( const rset_t *presult )
 * @brief Validates if the contents of rset points to valid locations
 *
 * @param presult [in] Handle returned by libminxml_get_rset().
 * @return TRUE if validated. Else FALSE;
 */
tBool libminxml_validate_result_set (const rset_t *presult);

/*!\fn tChar* libminxml_get_next_content (rset_t* presult)
 * @brief Returns the content of the element pointed by the result set.
 *
 * @param presult [in] Handle returned by libminxml_get_rset().
 * @return The contents of the element.
 */
tChar* libminxml_get_next_content (rset_t* presult);

/*!\fn tChar* libminxml_get_next_element_name (rset_t* presult)
 * @brief Returns the name of the next element pointed by the result set.
 *
 * @param presult [in] Handle returned by libminxml_get_rset().
 * @return The name of the next element.
 */
tChar* libminxml_get_next_element_name (rset_t* presult);

/*!\fn tBool libminxml_get_next_attr_res (rset_t* presult)
 * @brief Forwards the result set to the next attribute set.
 *
 * @param presult [in] Handle returned by libminxml_get_rset().
 * @return TRUE if there were any more result sets. Else, FALSE.
 */
tBool libminxml_get_next_attr_res (rset_t* presult);

/*!\fn tBool libminxml_get_next_attr (rset_t* presult, tChar* pattr_name, tChar* pattr_val)
 * @brief Forwards the result set to the next attribute set and returns the 
 *        attribute name and value.
 *
 * @param presult [in] Handle returned by libminxml_get_rset().
 * @param pattr_name [out] Buffer to hold the name of next attribute.
 * @param pattr_val [out] Buffer to hold the value of next attribute. 
 * @return TRUE if there were any more attr result sets. Else, FALSE.
 */
tBool libminxml_get_next_attr (rset_t* presult, tChar* pattr_name, tChar* pattr_val);

/*!\fn tVoid libminxml_get_prev_level_element(rset_t* presult)
 * @brief Goes one step backward in the iterative process. That is, the result
 *        set now points to the parent of the current element.
 *
 * @param presult [in] Handle returned by libminxml_get_rset().
 * @return None.
 */
tVoid libminxml_get_prev_level_element(rset_t* presult);

/*!\fn tBool libminxml_reset_attr_list(rset_t* presult)
 * @brief Reset the attribute pointer in result set to the top of the list.
 *
 * @param presult [in] Handle returned by libminxml_get_rset().
 * @return None.
 */
tBool libminxml_reset_attr_list(rset_t* presult);

#endif /* LIBMINXML_H_ */
