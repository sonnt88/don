/************************************************************************************************
* 
* \file          sharedlibentry.c
* \brief         
*
* \project       IMX6 (ADIT GEN3)
*
* \authors       
*
* COPYRIGHT      (c) 2014 Bosch CarMultimedia GmbH
*
* HISTORY      :
*----------------------------------------------------------------------------------------------------
* Date         |        Modification                            | Author & comments
*--------------|------------------------------------------------|------------------------------------
* 12.Aug.2014  |  Include ostrace.h                             |  Madhu Sudhan Swargam (RBEI/ECF5)
* 30.Aug.2015      Provided support for GPIO in LSIM            |  Rahana V S ( RBEI/ECF5)
* 27.Dec.2015      Prio2 Lint fix CFG3-1648	                |  Jeshwanth Kumar N K (RBEI/ECF5)
|-----------------------------------------------------------------------------------------------------*/
/* OSAL Interface */
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"

extern tS32 DEV_ADC_s32IODeviceInit(void);
//extern tS32 DEV_ADC_s32IODeviceDeinit(void);
extern tS32 ADC_s32IOOpen(tCString szName,tS32 s32ID, uintptr_t *pu32FD);   
extern tS32 ADC_s32IOClose(tS32 s32ID, uintptr_t u32FD);
extern tS32 ADC_s32IOControl(tS32 s32ID,uintptr_t u32FD, tS32 s32Fun, intptr_t s32Arg);
extern tS32 ADC_s32IORead(tS32 s32ID, uintptr_t u32FD,tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 DEV_GPIO_s32IODeviceInit(void);
//extern tS32 DEV_GPIO_s32IODeviceDeinit(void);
extern tS32 GPIO_IOOpen(void);
extern tS32 GPIO_s32IOClose(void);
extern tS32 GPIO_s32IOControl(tS32 s32Fun, intptr_t s32Arg);

extern tS32 pram_io_open (tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16 appid);
extern tS32 pram_io_close (tS32 s32ID, uintptr_t u32FD);
extern tS32 pram_io_ctrl (tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32Arg);
extern tS32 pram_io_read (tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size);
extern tS32 pram_io_write (tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size);


OSAL_DECL tS32 gpio_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(app_id);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Id);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(szName);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enAccess);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32FD);
#ifdef _LINUXX86_64_
   return OSAL_E_NOTSUPPORTED;
#else
   return GPIO_IOOpen();
#endif
}

OSAL_DECL tS32 gpio_drv_io_close(tS32 s32ID, uintptr_t u32FD)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
#ifdef _LINUXX86_64_
   return OSAL_E_NOTSUPPORTED;
#else
  return GPIO_s32IOClose();
#endif
}

OSAL_DECL tS32 gpio_drv_io_control(tS32 s32ID, intptr_t u32FD, tS32 s32fun, intptr_t s32arg)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
#ifdef _LINUXX86_64_
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32fun);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32arg);
   return OSAL_E_NOTSUPPORTED;
#else
   return GPIO_s32IOControl(s32fun, s32arg); 
#endif
}

OSAL_DECL tS32 adc_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enAccess);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(app_id);
#if (defined (OSAL_LINUX_X86) || defined (_LINUXX86_64_))
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Id);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(szName);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32FD);
   return OSAL_E_NOTSUPPORTED;
#else
   return ADC_s32IOOpen(szName,s32Id, pu32FD);
#endif
}

OSAL_DECL tS32 OSAL_DECL adc_drv_io_close(tS32 s32ID, uintptr_t u32FD)
{
#if (defined (OSAL_LINUX_X86) || defined (_LINUXX86_64_))
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
   return OSAL_E_NOTSUPPORTED;
#else
   return ADC_s32IOClose(s32ID, u32FD);
#endif
}

OSAL_DECL tS32 adc_drv_io_control(tS32 s32ID, intptr_t u32FD, tS32 s32fun, intptr_t s32arg)
{
#if (defined (OSAL_LINUX_X86) || defined (_LINUXX86_64_))
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32fun);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32arg);
   return OSAL_E_NOTSUPPORTED;
#else
   return ADC_s32IOControl(s32ID,u32FD, s32fun, s32arg );
#endif
}

OSAL_DECL tS32 adc_drv_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
#if (defined (OSAL_LINUX_X86) || defined (_LINUXX86_64_))
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pBuffer);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32Size);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ret_size);
   return OSAL_E_NOTSUPPORTED;
#else
   return ADC_s32IORead(s32ID,u32FD, pBuffer, u32Size, ret_size);
#endif
}

OSAL_DECL tS32 pram_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, intptr_t *pu32FD, tU16  app_id)
{
#ifdef _LINUXX86_64_
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Id);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(szName);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enAccess);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32FD);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(app_id);
   return OSAL_E_NOTSUPPORTED;
#else
   return pram_io_open (s32Id, szName, enAccess, pu32FD, app_id);
#endif
}

OSAL_DECL tS32 pram_drv_io_close(tS32 s32ID, uintptr_t u32FD)
{
#ifdef _LINUXX86_64_
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
   return OSAL_E_NOTSUPPORTED;
#else
   return pram_io_close(s32ID, u32FD); 
#endif
}

OSAL_DECL tS32 pram_drv_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg)
{
#if (defined (OSAL_LINUX_X86) || defined (_LINUXX86_64_))
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32fun);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32arg);
   return OSAL_E_NOTSUPPORTED;
#else
    return pram_io_ctrl(s32ID, u32FD, s32fun, s32arg);
#endif
}

OSAL_DECL tS32 pram_drv_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
#if (defined (OSAL_LINUX_X86) || defined (_LINUXX86_64_))
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pBuffer);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32Size);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ret_size);
   return OSAL_E_NOTSUPPORTED;
#else
    return pram_io_write (s32ID, u32FD, pBuffer, u32Size, ret_size);
#endif
}

OSAL_DECL tS32 pram_drv_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
#if (defined (OSAL_LINUX_X86) || defined (_LINUXX86_64_))
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pBuffer);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32Size);
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ret_size);
   return OSAL_E_NOTSUPPORTED;
#else
   return  pram_io_read (s32ID, u32FD, pBuffer, u32Size, ret_size);
#endif
}


//target location for DownloadLogWriter.ini(marker file)to disable "write downloadlog2usb on authorized unlock only".
#define DOWNLOAGLOG_VOLATILE_PATH "/tmp/DownloadLogWriter.ini"
#define DOWNLOAGLOG_PERMANENT_PATH "/var/opt/bosch/dynamic/DownloadLogWriter.ini"

//download log is available in files update.log and downloadpipe..
#define UPDATE_LOG   "/dev/ffs2/persistent/swupdate/update.log"
#define DOWNLOADPIPE "/dev/ffs2/persistent/downloadPipe"

/*****************************************************************************
*
* FUNCTION:    s32TraceUpdateLog
*
* DESCRIPTION: Trace out download log (update.log) to usb .
               Target location :"/dev/ffs2/persistent/swupdate/update.log"
*
* PARAMETER:   OSAL_tIODescriptor fd

* RETURNVALUE: tS32 s32Return
*              OSAL_OK = Success
*              OSAL_ERROR = Failed
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | SJA3KOR
* --.--.--  | ----------------                       | -----
*****************************************************************************/
tS32 s32TraceUpdateLog(OSAL_tIODescriptor fd)
{
	tS32 s32Return = OSAL_OK;
	tU32 u32ErrorCode	= OSAL_E_NOERROR;
	OSAL_tIODescriptor file_desc ;
	OSAL_tenAccess enAccess = OSAL_EN_READONLY;
	tS8 *ps8ReadBuffer = OSAL_NULL ;
	tS32 s32BytesRead = 0;
	tS32 s32BytesWritten = 0;
	tS32 file_size = 0;
	
	/*Open update.log(download log file) file*/
	file_desc = OSAL_IOOpen ( ( tCString )UPDATE_LOG, enAccess );
	if( ( file_desc != OSAL_ERROR )&&( file_desc != OSAL_NULL ))
	{
		file_size = OSALUTIL_s32FGetSize( file_desc );
		if(file_size != OSAL_ERROR)
		{
			ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( (tU32)file_size + 1);
			if ( ps8ReadBuffer == OSAL_NULL )
			{
				u32ErrorCode = OSAL_E_NOSPACE;
			}
			else
			{
				s32BytesRead = OSAL_s32IORead ( file_desc, ps8ReadBuffer,
										(tU32)file_size );
				if ( ( s32BytesRead ==	( tS32 )OSAL_ERROR ) || ( s32BytesRead !=  ( tS32 )file_size ) )
				{
					u32ErrorCode = OSAL_u32ErrorCode();
				
				}
				else
				{
					s32BytesWritten = OSAL_s32IOWrite ( fd , ps8ReadBuffer, (tU32)file_size );
					if ( ( s32BytesWritten == ( tS32 )OSAL_ERROR ) ||( s32BytesWritten != ( tS32 )file_size )  )
					{
						u32ErrorCode = OSAL_u32ErrorCode();
					}
				}
				
			     OSAL_vMemoryFree( ps8ReadBuffer );
				 ps8ReadBuffer = OSAL_NULL;
				
			}
		}
		else
		{
			u32ErrorCode = OSAL_E_UNKNOWN;
		}	
		if ( OSAL_s32IOClose ( file_desc ) == OSAL_ERROR )
		{
			u32ErrorCode = OSAL_u32ErrorCode();
		}
	}
	else
	{
		u32ErrorCode = OSAL_u32ErrorCode();
	}
	if(u32ErrorCode != OSAL_E_NOERROR)
	{
		char cBuffer[100] = {0};
		s32Return = OSAL_ERROR;
		OSAL_M_INSERT_T8(&cBuffer[0],OSAL_STRING_OUT);
		snprintf(&cBuffer[1],99,"downloadlog2usb:Write Download Log(update.log) to USB is Failed Errorcode : %d",u32ErrorCode);
		LLD_vTrace(TR_COMP_OSALCORE, (tU32)TR_LEVEL_FATAL,cBuffer,strlen(&cBuffer[1])+1);
		vWriteToErrMem((tS32)TR_COMP_OSALCORE,(char*)&cBuffer[1],(int)strlen(cBuffer),OSAL_STRING_OUT);
	}
    
	return s32Return;
}

/*****************************************************************************
*
* FUNCTION:    s32TraceDownpipeLog
*
* DESCRIPTION: Trace out download log (downloadpipe) to usb .
               Target location :"/dev/ffs2/persistent/downloadPipe"
*
* PARAMETER:   OSAL_tIODescriptor fd

* RETURNVALUE: tS32 s32Return
*              OSAL_OK = Success
*              OSAL_ERROR = Failed
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | SJA3KOR
* --.--.--  | ----------------                       | -----
*****************************************************************************/
tS32 s32TraceDownpipeLog(OSAL_tIODescriptor fd)
{
    tS32 s32Return = OSAL_OK;
    tU32 u32ErrorCode   = OSAL_E_NOERROR;
    OSAL_tIODescriptor file_desc;
    OSAL_tenAccess enAccess = OSAL_EN_READONLY;
    tS8 *ps8ReadBuffer = OSAL_NULL ;
    tS32 s32BytesRead = 0;
    tS32 s32BytesWritten = 0;
    tS32 file_size = 0;
    
    /*Open downloadPipe file*/
    file_desc = OSAL_IOOpen ( ( tCString )DOWNLOADPIPE, enAccess );
    if (( file_desc != OSAL_ERROR )&&( file_desc != OSAL_NULL ))
    {
        file_size = OSALUTIL_s32FGetSize( file_desc );
        if(file_size != OSAL_ERROR)
        {
           
            ps8ReadBuffer = ( tS8 * ) OSAL_pvMemoryAllocate ( (tU32)file_size + 1 );
            if ( ps8ReadBuffer == OSAL_NULL )
            {
                u32ErrorCode = OSAL_E_NOSPACE;
            }
            else
            {
                s32BytesRead = OSAL_s32IORead ( file_desc, ps8ReadBuffer,
                                        (tU32)file_size );
                if ( ( s32BytesRead ==  ( tS32 )OSAL_ERROR ) || ( s32BytesRead !=  ( tS32 )file_size ) )
                {
                    u32ErrorCode = OSAL_u32ErrorCode();
                
                }
                else
                {
                    s32BytesWritten = OSAL_s32IOWrite ( fd , ps8ReadBuffer, (tU32)file_size );
                    if ( ( s32BytesWritten == ( tS32 )OSAL_ERROR ) ||( s32BytesWritten != ( tS32 )file_size )  )
                    {
                        u32ErrorCode = OSAL_u32ErrorCode();
                    }
                }
                
                OSAL_vMemoryFree( ps8ReadBuffer );
                ps8ReadBuffer = OSAL_NULL;
               
            }
        }
        else
        {
            u32ErrorCode = OSAL_E_UNKNOWN;
        }   
        if ( OSAL_s32IOClose ( file_desc ) == OSAL_ERROR )
        {
            u32ErrorCode = OSAL_u32ErrorCode();
        }
    }
    if(u32ErrorCode != OSAL_E_NOERROR)
    {
        char cBuffer[100] = {0};
        s32Return = OSAL_ERROR;
        OSAL_M_INSERT_T8(&cBuffer[0],OSAL_STRING_OUT);
        snprintf(&cBuffer[1],99,"downloadlog2usb:Write Download Log(downloadpipe) to USB is Failed Errorcode : %d",u32ErrorCode);
        LLD_vTrace(TR_COMP_OSALCORE, (tU32)TR_LEVEL_FATAL,cBuffer,strlen(&cBuffer[1])+1);
        vWriteToErrMem((tS32)TR_COMP_OSALCORE,(char*)&cBuffer[1],(int)strlen(cBuffer),OSAL_STRING_OUT);
    }
    return s32Return;
}



/*****************************************************************************
*
* FUNCTION:    vWriteDownloadlogToMassstorage
*
* DESCRIPTION: Trace out download log (downloadpipe and update.log) to usb .
                    
* PARAMETER:   const char* Path

* RETURNVALUE: none

* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | SJA3KOR
* --.--.--  | ----------------                       | -----
*****************************************************************************/
void vWriteDownloadlogToMassstorage(const char* Path)
{
  char cFilepath[100];
  char cFileNr[4];
  char szCommand[100];
  int i;
  OSAL_tIODescriptor fd = 0;
     /* check for running download copy */
  if(pOsalData->fddownloadlog == 0)
  {
/*#ifdef AM_BIN
      const char* options = "rw";
      if(automounter_api_remount_partition_by_mountpoint(Path,options,ReqId,remount_done_callback) != RESULT_OK)
      {
          TraceString("Remount for Download trace failed");
      }
      else
      {
          do{
              OSAL_s32ThreadWait(200);
          }while(bCallbackDone == FALSE);

          TraceString("Remount for Download trace done");
          /*reset for next request *
          bCallbackDone = FALSE;
          if(request_result == RESULT_OK)
          {
             TraceString("Remount for Download trace successful");
          }
      }
#else*/
      /* make media writable */
     OSAL_szStringCopy(szCommand,"mount -o remount,rw ");
     OSAL_szStringConcat(szCommand,Path);
     OSAL_szStringConcat(szCommand, "\n");
     system(szCommand);
//#endif
   
        /* check for older files on media*/
       for(i=1;i<100;i++)
       { 
         cFilepath[0] = '\0';
         OSAL_szStringCopy(cFilepath,Path);
         OSAL_szStringConcat(cFilepath,"/update");
         sprintf(cFileNr,"%d",i);
         OSAL_szStringConcat(cFilepath,&cFileNr[0]);
         OSAL_szStringConcat(cFilepath,".log");
         if((pOsalData->fddownloadlog = OSAL_IOOpen(&cFilepath[0], OSAL_EN_READWRITE)) == OSAL_ERROR)
         {
            /* create file(updatei.log) inside usb for coping download log */
            pOsalData->fddownloadlog = OSAL_IOCreate(&cFilepath[0], OSAL_EN_READWRITE );
            break;
         }
         else
         {
            OSAL_s32IOClose(pOsalData->fddownloadlog);
         }
       }/* end for... */
   
       /* check for valid file handle */
       if(pOsalData->fddownloadlog != OSAL_ERROR)
       {
         //copy update.log from target to usb if available..
         if(s32TraceUpdateLog(pOsalData->fddownloadlog) == OSAL_ERROR)
         {
            TraceString("downloadlog2usb : Copy Update.log is finished with error\n");
         }
         else
         {
            TraceString("downloadlog2usb : Copy Update.log done !!!");
         }
         /*Check the download pipe is present in the target rootfilesystem.
           In GM it is available whereas in suzuki it is not available */
         if((fd = OSAL_IOOpen ( ( tCString )DOWNLOADPIPE, OSAL_EN_READONLY))!= OSAL_ERROR)
         {
          OSAL_s32IOClose(fd);
          for(i=1;i<100;i++)
          { 
            cFilepath[0] = '\0';
            OSAL_szStringCopy(cFilepath,Path);
            OSAL_szStringConcat(cFilepath,"/downloadpipe");
            sprintf(cFileNr,"%d",i);
            OSAL_szStringConcat(cFilepath,&cFileNr[0]);
            OSAL_szStringConcat(cFilepath,".log");
            if((fd = OSAL_IOOpen(&cFilepath[0], OSAL_EN_READWRITE)) == OSAL_ERROR)
            {
            /* create file downloadpipe.log inside usb for coping download log */
              fd= OSAL_IOCreate(&cFilepath[0], OSAL_EN_READWRITE );
              break;
            }
            else
            {
             OSAL_s32IOClose(fd);
            }
          } 
          if(fd != OSAL_ERROR)
          {
              //copy downloadpipe from target to usb..
              if(s32TraceDownpipeLog(fd) == OSAL_ERROR)
              {
                  TraceString("downloadlog2usb : copy downloadpipe is finished with error\n");
              }
              else
              {
                  TraceString("downloadlog2usb : Copy downloadPipe done !!!");
              }
          }
        }
   
         /* ensure that data are in sync */
         OSAL_szStringCopy(szCommand,"sync");
         OSAL_szStringConcat(szCommand, "\n");
         system(szCommand);
   
         OSAL_s32ThreadWait(3000);
   
/*#ifdef AM_BIN
       if(automounter_api_remount_partition_by_mountpoint(Path,"r",ReqId,remount_done_callback) != RESULT_OK)
       {
          TraceString("Remount after Download trace failed");
       }
#else*/
        OSAL_szStringCopy(szCommand,"mount -o remount,ro ");
        OSAL_szStringConcat(szCommand,Path);
        OSAL_szStringConcat(szCommand, "\n");
        system(szCommand);
//#endif
   }
       OSAL_s32IOClose(pOsalData->fddownloadlog);
       pOsalData->fddownloadlog = 0;
 }
}



tS32 s32InvestigateMedia(tCString String)
{
      char PathBuffer[256];
      OSAL_tIODescriptor fd,file_desc;

      /*Feature :"Write Download Log to USB"*/
      strcpy(&PathBuffer[0],"/dev/media/");
      strcat(PathBuffer,String);
      strcat(PathBuffer,"/downloadlog.ini");
      if((fd = OSAL_IOOpen(PathBuffer,OSAL_EN_READONLY)) != OSAL_ERROR )
      {
               OSAL_s32IOClose(fd);
               /*check for DownloadLogWriter.ini(marker file).If it is available then dont allow to 
                 write download log to usb.It is gen3 security feature.File DownloadLogWriter.ini(marker file)
               is created by ALD for disabling the feature "write downloadlog2usb"*/
               if(((file_desc = open(DOWNLOAGLOG_VOLATILE_PATH,O_RDONLY))== -1)&&
                  ((file_desc = open(DOWNLOAGLOG_PERMANENT_PATH,O_RDONLY))== -1))
               {
                  vWriteDownloadlogToMassstorage(PathBuffer);
               }
               else
               {
                   /*File is present.It means feature is disabled by ALD*/ 
                   close(file_desc);
               }
      }
      return 0;
}

static sem_t       *initbase_lock;
void __attribute__ ((constructor)) base_process_attach(void)
{
   tBool bFirstAttach = FALSE;

   initbase_lock = sem_open("BASE_INIT",O_EXCL | O_CREAT, 0660, 0);
   if (initbase_lock != SEM_FAILED)
   {
      if(s32OsalGroupId)
      {
         if(chown("/dev/shm/sem.BASE_INIT",(uid_t)-1,s32OsalGroupId) == -1)
         {
             vWritePrintfErrmem("BASE_INIT -> chown error %d \n",errno);
         }
         if(chmod("/dev/shm/sem.BASE_INIT", OSAL_ACCESS_RIGTHS) == -1)
         {
            vWritePrintfErrmem("BASE_INIT -> fchmod error %d \n",errno);
         }
      }

      bFirstAttach = TRUE;
   }
   else
   {
      initbase_lock = sem_open("BASE_INIT", 0);
      bFirstAttach = FALSE;
      sem_wait(initbase_lock);
   }
   if(bFirstAttach)
   {

     if(DEV_GPIO_s32IODeviceInit() != (tS32)OSAL_E_NOERROR)
      {
      }
      if(DEV_ADC_s32IODeviceInit() != (tS32)OSAL_E_NOERROR)
      {
      }
   }
   sem_post(initbase_lock);
}

void __attribute__((destructor)) base_process_detach(void)
{
}
