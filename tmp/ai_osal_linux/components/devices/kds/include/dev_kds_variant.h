/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_kds_variant.h
 *
 *  header for the kds variant handling
 *
 * @date        2014-04-24
 *
 * @note
 *
 *  &copy; Copyright BSOT GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef DEV_KDS_VARIANT_H
#define DEV_KDS_VARIANT_H

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
#ifdef CS_TESTMANAGER
#define KDS_TESTMANGER_ACTIVE
#else
#ifdef LOAD_KDS_SO 
#define KDS_PUTTY_CONSOL_TRACE_ACTIVE 
#else
#define KDS_TTFIS_CONSOL_TRACE_ACTIVE           /*KDS_TTFIS_CONSOL_TRACE_ACTIVE active if no shared library*/
#endif
#endif
/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

#else
#error dev_kds_variant.h included several times
#endif
