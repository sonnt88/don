/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_kds_trace.h
 *
 *  private header for the KDS driver trace information. 
 *
 * @date        2012-12-03
 *
 * @note
 *
 *  &copy; Copyright BoschSoftec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef DEV_KDS_TRACE_H
#define DEV_KDS_TRACE_H

/*************************************************************************/
/* define                                                                */
/*************************************************************************/
#define KDS_LEVEL_IMPORTANT                   0x1f
#ifdef KDS_PUTTY_CONSOL_TRACE_ACTIVE 
/*defines for KDS_vPuttyConsolTrace() */
//#define KDS_LEVEL_INTERFACE_CALL              0x2f
//#define KDS_LEVEL_INFO_GENERALLY              0x3f
//#define KDS_LEVEL_INFO_SHARED_KDS             0x4f
#endif

/* assert*/
#ifdef KDS_TTFIS_CONSOL_TRACE_ACTIVE
#define KDS_NORMAL_ASSERT(bAssertion)  	  {if(!(bAssertion)){NORMAL_M_ASSERT_ALWAYS(); return FALSE; }}
#define KDS_NORMAL_ASSERT_NO_CONDITION()  NORMAL_M_ASSERT_ALWAYS()
#define KDS_FATAL_ASSERT()				  FATAL_M_ASSERT_ALWAYS()
#elif defined KDS_PUTTY_CONSOL_TRACE_ACTIVE
#define KDS_NORMAL_ASSERT(bAssertion)  	 {if(!(bAssertion)){NORMAL_M_NATIVE_ASSERT_ALWAYS(); return FALSE; }}
#define KDS_NORMAL_ASSERT_NO_CONDITION() NORMAL_M_NATIVE_ASSERT_ALWAYS()
#define KDS_FATAL_ASSERT()				 FATAL_M_NATIVE_ASSERT_ALWAYS()
#else
#define KDS_NORMAL_ASSERT(bAssertion)	
#define KDS_NORMAL_ASSERT_NO_CONDITION() 
#define KDS_FATAL_ASSERT()				
#endif


/************************** kind traces *********************************/
/* important*/
#define KDS_ERROR                             0x01
#define KDS_ERROR_READ                        0x02
#define KDS_ERROR_SEM                         0x03
#define KDS_ERROR_MMAP                        0x04
#define KDS_INFO_NO_KDS_DATA                  0x05


/* other*/
#define KDS_START_FUNCTION_INIT               0x20
#define KDS_EXIT_FUNCTION_INIT                0x21
#define KDS_START_FUNCTION_REMOVE             0x22    
#define KDS_EXIT_FUNCTION_REMOVE              0x23
#define KDS_START_FUNCTION_OPEN               0x24
#define KDS_EXIT_FUNCTION_OPEN                0x25
#define KDS_START_FUNCTION_CLOSE              0x26
#define KDS_EXIT_FUNCTION_CLOSE               0x27
#define KDS_START_FUNCTION_CONTROL            0x28
#define KDS_EXIT_FUNCTION_CONTROL             0x29
#define KDS_START_FUNCTION_READ               0x2a
#define KDS_EXIT_FUNCTION_READ                0x2b
#define KDS_START_FUNCTION_WRITE              0x2c
#define KDS_EXIT_FUNCTION_WRITE               0x2d

#define KDS_NUMBER_OF_ENTRIES                 0x30  
#define KDS_ENTRY                             0x31
#define KDS_DATA_SIZE_WRITE_TO_FLASH          0x32
#define KDS_MAX_DATA_SIZE_WRITE_TO_FLASH      0x33
#define KDS_READ_MAX_FILE_SIZE                0x34
#define KDS_READ_DATA_SIZE                    0x35

#define KDS_ATTACH_COUNT                      0x40  
#define KDS_SEM_VALUE                         0x41
#define KDS_SEM_WAIT                          0x42
#define KDS_SEM_POST                          0x43

/*for error memory*/
#define KDS_NOR_ERROR_MEMORY                  0xC0
/**********************end kind traces **********************************/

#ifdef KDS_TTFIS_CONSOL_TRACE_ACTIVE
  #define KDS_TRACE(u8Kind,pData,u8Len)   KDS_vTTFISConsolTrace(u8Kind,(tU8*)pData,u8Len)  
#elif defined KDS_PUTTY_CONSOL_TRACE_ACTIVE
  #define KDS_TRACE(u8Kind,pData,u8Len)   KDS_vPuttyConsolTrace(u8Kind,(void*)pData,u8Len)  
#else
  #define KDS_TRACE(u8Kind,pData,u8Len)
#endif

#ifdef KDS_TESTMANGER_ACTIVE
#define KDS_SET_ERROR_ENTRY(PcpData)
#else
#define KDS_SET_ERROR_ENTRY(PcpData)     KDS_vSetErrorEntry(PcpData);
#endif

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
#ifdef KDS_TTFIS_CONSOL_TRACE_ACTIVE
void KDS_vTTFISConsolTrace(tU8 Pu8Kind, const tU8* Pu8pData, tU8 Pu8Len);
#elif defined KDS_PUTTY_CONSOL_TRACE_ACTIVE
void KDS_vPuttyConsolTrace(tU8 Pu8Kind, const void* Pu8pData, tU8 Pu8Len);
#endif
#ifndef KDS_TESTMANGER_ACTIVE
void KDS_vSetErrorEntry(const tChar* PcpData);
#endif
#else
#error dev_kds_trace.h included several times
#endif

