/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_kds.h
 *
 *  header for the KDS driver. The dev_kds driver 
 *  is a an osal driver. 
 *
 * @date        2010-08-18
 *
 * @note
 *
 *  &copy; Copyright TMS GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef DEV_KDS_H
#define DEV_KDS_H

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
tS32 KDS_s32IODeviceInit(tVoid);
tS32 KDS_s32IODeviceRemove(tVoid);
tS32 KDS_IOOpen(tVoid);
tS32 KDS_s32IOClose(tVoid);
tS32 KDS_s32IOControl(tS32 s32fun, intptr_t s32arg);
tS32 KDS_s32IOWrite(tPCS8 ps8Buffer, tU32 u32nbytes);
tS32 KDS_s32IORead(tPCS8 ps8Buffer, tU32 u32nbytes);

#else
#error dev_kds.h included several times
#endif
