/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_kds_privat.h
 *
 *  private header for the KDS driver. 
 *
 * @date        2010-08-18
 *
 * @note
 *
 *  &copy; Copyright TMS GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef DEV_KDS_PRIVATE_H
#define DEV_KDS_PRIVATE_H

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
/*version number of driver*/
#define KDS_C_S32_IO_VERSION                      0x00000206

/*magic number*/
#define KDS_C_SHARED_MEMORY_MAGIC_TENGINE         0xac
#define KDS_C_SHARED_MEMORY_MAGIC_LINUX           0xca
/*maximal entrys*/
#define KDS_C_MAX_ENTRIES                         1000  

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef enum 
{
  KDS_KIND_RES_SEM,
  KDS_KIND_RES_SHM,
}teKDSKindRes;


typedef struct
{
  tU16              u16StateReadFlashData;
  tU32              u32KDSNumberOfEntries;
#ifndef KDS_TESTMANGER_ACTIVE
  sem_t             tSemLockAccess;                            //semaphore for access to this shared memory
  tS32              s32AttachedProcesses;                      //attach count
#endif
  tBool             vbValidSharedMemory;                       //check if shared memory OK 
  tBool             vbWriteEnable;                             //write enable flag
  tsKDSEntry        sEntry[KDS_C_MAX_ENTRIES];  
}tsKDSSharedMemory;

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/


/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
tS32  s32KDSReadData(tsKDSSharedMemory* PsSharedMemory);
tS32  s32KDSWriteData(tsKDSSharedMemory* PsSharedMemory);
#ifndef KDS_TESTMANGER_ACTIVE 
tS32  s32KDSChangeGroupAccess(teKDSKindRes PeKindRes,tS32 Ps32Id,const char* PStrResource);
#endif
#else
#error dev_kds_private.h included several times
#endif
