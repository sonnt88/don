
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include "system_types.h"
#include "system_definition.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "dev_kds.h"

/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
#define KDS_TEST_ENTRY_ID       0x1234
#define KDS_TEST_ENTRY_LENGTH   10

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/
/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/                            

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
void vKDSTestPrintHelp(void);
void vKDSTestPrintBuffer(tU8* VubpBuffer,tU32 Pu32Lenght);
void vKDSTestReadEntry(void);
void vKDSTestWriteEntry(void);
void vKDSTestWriteReadEntry(void);

/******************************************************************************
* FUNCTION: int main(int argc, char *argv[])
*
* DESCRIPTION: start function of the proccess
*
* PARAMETERS:
*      argc: number of strings
*      argv: string table
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 05 06
*****************************************************************************/
int main(int argc, char *argv[])
{
	const char* VcOptString = "rwt";
    int         ViOpt;
	tBool       VbDone=FALSE;

  fprintf(stderr, "\n");
  fprintf(stderr, "================================================================================\n");
  fprintf(stderr, " KDS_TEST: test helper functions for kds, v1.0.0, 2014\n");
  fprintf(stderr, "================================================================================\n");
  fprintf(stderr, "\n");

    while ((ViOpt = getopt(argc, argv, VcOptString)) != -1) 
    {
      switch (ViOpt) 
	  {
        case 'r':
		  fprintf(stderr, "-r: read test entry\n");
          fprintf(stderr, "================================================================================\n");    
		  vKDSTestReadEntry();
		  VbDone=TRUE;
        break;
		case 'w':
		  fprintf(stderr, "-w: write test entry and store this to NOR\n");
          fprintf(stderr, "================================================================================\n");
		  vKDSTestWriteEntry();
		  VbDone=TRUE;
        break;
        case 't':
		  fprintf(stderr, "-t: first write, then read test entry\n");
          fprintf(stderr, "================================================================================\n");
		  vKDSTestWriteReadEntry();
		  VbDone=TRUE;
        break;	   
        default: 		
	      fprintf(stderr, "unknown command line option: \"-%c\"\n", optopt);
		  vKDSTestPrintHelp();
	  }/*end switch*/
    }/*end while*/
	if(VbDone==FALSE)
	{
	  vKDSTestPrintHelp();
	}
	fprintf(stderr, "================================================================================\n");
	sleep(100);
	_exit(0);
}
/******************************************************************************
* FUNCTION: void vKDSTestPrintHelp
*
* DESCRIPTION: printout options 
*
* PARAMETERS:
*      argc: number of strings
*      argv: string table
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
void vKDSTestPrintHelp()
{
	fprintf(stderr, "-r: read test entry\n");
	fprintf(stderr, "-w: write test entry\n");
	fprintf(stderr, "-t: read write test entry\n");	
	fprintf(stderr, "================================================================================\n");
    _exit(0);
}
/******************************************************************************
* FUNCTION: void vKDSTestPrintBuffer
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 09 24
*****************************************************************************/
void vKDSTestPrintBuffer(tU8* VubpBuffer,tU32 Pu32Lenght)
{
  tU32 Vu32Inc;  
  for(Vu32Inc=0;Vu32Inc<Pu32Lenght;Vu32Inc++)
  {
    fprintf(stderr, "%d ",VubpBuffer[Vu32Inc]);
  }
  fprintf(stderr, "\n ");
}

/******************************************************************************
* FUNCTION: void vKDSTestReadEntry
*
* DESCRIPTION: read an entry from KDS
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 09 12
*****************************************************************************/
void vKDSTestReadEntry(void)
{
  tS32 Vs32Return;
  Vs32Return=KDS_IOOpen();
  if(Vs32Return!=OSAL_E_NOERROR)
  {
    fprintf(stderr, "vKDSTestReadEntry: KDS_IOOpen() fails %x \n",Vs32Return);
  }
  else
  {
    tsKDSEntry rEntryInfo;
	rEntryInfo.u16Entry = KDS_TEST_ENTRY_ID;
	rEntryInfo.u16EntryLength = KDS_TEST_ENTRY_LENGTH;
	memset(rEntryInfo.au8EntryData,0, sizeof(rEntryInfo.au8EntryData));
    Vs32Return=KDS_s32IORead((tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo));
    if(Vs32Return!=(tS32)sizeof(rEntryInfo))
    {
      fprintf(stderr, "vKDSTestReadEntry: KDS_s32IORead() fails %x\n",Vs32Return);
    }
	else
	{
	  fprintf(stderr, "vKDSTestReadEntry: u16Entry: %x \n",rEntryInfo.u16Entry);
      fprintf(stderr, "vKDSTestReadEntry: u16EntryLength: %d \n",rEntryInfo.u16EntryLength);
      fprintf(stderr, "vKDSTestReadEntry: au8EntryData: ");
	  vKDSTestPrintBuffer(rEntryInfo.au8EntryData,(tU32)rEntryInfo.u16EntryLength);	  
	}
    KDS_s32IOClose();	
  }
}
/******************************************************************************
* FUNCTION: void vKDSTestWriteEntry
*
* DESCRIPTION: write an entry to KDS and saved it to NOR
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 09 24
*****************************************************************************/
void vKDSTestWriteEntry(void)
{
  tS32 Vs32Return;
  Vs32Return=KDS_IOOpen();
  if(Vs32Return!=OSAL_E_NOERROR)
  {
    fprintf(stderr, "vKDSTestWriteEntry: KDS_IOOpen() fails %x \n",Vs32Return);
  }
  else
  { /*write enable*/
	tBool VbAccessOption = 1;
	Vs32Return=KDS_s32IOControl(OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(tS32)VbAccessOption);
    if(Vs32Return != OSAL_E_NOERROR)
	{
	  fprintf(stderr, "vKDSTestWriteEntry: KDS_s32IOControl(OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE) fails %x \n",Vs32Return);
	}
	else
	{ /*write*/	  
	  tsKDSEntry rEntryInfo;
	  rEntryInfo.u16Entry = KDS_TEST_ENTRY_ID;
	  rEntryInfo.u16EntryLength = KDS_TEST_ENTRY_LENGTH;
	  memset(rEntryInfo.au8EntryData,0, sizeof(rEntryInfo.au8EntryData));
	  memset(rEntryInfo.au8EntryData,56, KDS_TEST_ENTRY_LENGTH);
      Vs32Return=KDS_s32IOWrite((tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo));
      if(Vs32Return!=(tS32)sizeof(rEntryInfo))
      {
        fprintf(stderr, "vKDSTestWriteEntry: KDS_s32IOWrite() fails %x\n",Vs32Return);
      }
	  else
	  { /* save to NOR*/
	    Vs32Return=KDS_s32IOControl(OSAL_C_S32_IOCTRL_KDS_WRITE_BACK,0); 
		if(Vs32Return != OSAL_E_NOERROR)
	    {
	      fprintf(stderr, "vKDSTestWriteEntry: KDS_s32IOControl(OSAL_C_S32_IOCTRL_KDS_WRITE_BACK) fails %x \n",Vs32Return);
	    }
		else
		{
		  fprintf(stderr, "vKDSTestReadEntry: u16Entry: %x \n",rEntryInfo.u16Entry);
          fprintf(stderr, "vKDSTestReadEntry: u16EntryLength: %d \n",rEntryInfo.u16EntryLength);
          fprintf(stderr, "vKDSTestReadEntry: au8EntryData: ");
	      vKDSTestPrintBuffer(rEntryInfo.au8EntryData,rEntryInfo.u16EntryLength);	  
		}
	  }
	}
    KDS_s32IOClose();	
  }
}
/******************************************************************************
* FUNCTION: void vKDSTestWriteReadEntry
*
* DESCRIPTION: write an entry to KDS and read it back
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 09 24
*****************************************************************************/
void vKDSTestWriteReadEntry(void)
{
  tS32 Vs32Return;
  Vs32Return=KDS_IOOpen();
  if(Vs32Return!=OSAL_E_NOERROR)
  {
    fprintf(stderr, "vKDSTestWriteReadEntry: KDS_IOOpen() fails %x \n",Vs32Return);
  }
  else
  { /*write enable*/
	tBool VbAccessOption = 1;
	Vs32Return=KDS_s32IOControl(OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE,(tS32)VbAccessOption);
    if(Vs32Return != OSAL_E_NOERROR)
	{
	  fprintf(stderr, "vKDSTestWriteReadEntry: KDS_s32IOControl(OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE) fails %x \n",Vs32Return);
	}
	else
	{ /*write*/	  
	  tsKDSEntry rEntryInfo;
	  rEntryInfo.u16Entry = KDS_TEST_ENTRY_ID;
	  rEntryInfo.u16EntryLength = KDS_TEST_ENTRY_LENGTH;
	  memset(rEntryInfo.au8EntryData,0, sizeof(rEntryInfo.au8EntryData));
	  memset(rEntryInfo.au8EntryData,22, KDS_TEST_ENTRY_LENGTH);
      Vs32Return=KDS_s32IOWrite((tS8 *)&rEntryInfo,(tS32)sizeof(rEntryInfo));
      if(Vs32Return!=(tS32)sizeof(rEntryInfo))
      {
        fprintf(stderr, "vKDSTestWriteReadEntry: KDS_s32IOWrite() fails %x\n",Vs32Return);
      }
	  else
	  { /* save to NOR*/
	    Vs32Return=KDS_s32IOControl(OSAL_C_S32_IOCTRL_KDS_WRITE_BACK,0); 
		if(Vs32Return != OSAL_E_NOERROR)
	    {
	      fprintf(stderr, "vKDSTestWriteReadEntry: KDS_s32IOControl(OSAL_C_S32_IOCTRL_KDS_WRITE_BACK) fails %x \n",Vs32Return);
	    }
		else
		{
		  tsKDSEntry rEntryInfoRead;
		  fprintf(stderr, "vKDSTestWriteReadEntry: ---- write entry ----\n");
		  fprintf(stderr, "vKDSTestWriteReadEntry: u16Entry: %x \n",rEntryInfo.u16Entry);
          fprintf(stderr, "vKDSTestWriteReadEntry: u16EntryLength: %d \n",rEntryInfo.u16EntryLength);
          fprintf(stderr, "vKDSTestWriteReadEntry: au8EntryData: ");
	      vKDSTestPrintBuffer(rEntryInfo.au8EntryData,rEntryInfo.u16EntryLength);	  
		  /* read back*/	     
	      rEntryInfoRead.u16Entry = KDS_TEST_ENTRY_ID;
	      rEntryInfoRead.u16EntryLength = KDS_TEST_ENTRY_LENGTH;
	      memset(rEntryInfoRead.au8EntryData,0, sizeof(rEntryInfoRead.au8EntryData));
          Vs32Return=KDS_s32IORead((tS8 *)&rEntryInfoRead,(tS32)sizeof(rEntryInfoRead));
          if(Vs32Return!=(tS32)sizeof(rEntryInfoRead))
          {
            fprintf(stderr, "vKDSTestWriteReadEntry: KDS_s32IORead() fails %x\n",Vs32Return);
          }
	      else
	      {
		    fprintf(stderr, "vKDSTestWriteReadEntry: ---- read entry ----\n");
	        fprintf(stderr, "vKDSTestWriteReadEntry: u16Entry: %x \n",rEntryInfoRead.u16Entry);
            fprintf(stderr, "vKDSTestWriteReadEntry: u16EntryLength: %d \n",rEntryInfoRead.u16EntryLength);
            fprintf(stderr, "vKDSTestWriteReadEntry: au8EntryData: ");
	        vKDSTestPrintBuffer(rEntryInfoRead.au8EntryData,(tU32)rEntryInfoRead.u16EntryLength);	  
	      }
		}
	  }
	}
    KDS_s32IOClose();	
  }
}