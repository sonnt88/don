/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_kds_pdd_access.c
 *
 * This file contains special function for the file access for the linux side
 * 
 * global function:
 * --s32KDSWriteData:          write the data from the shared memory into files
 * --s32KDSReadData:           call function s32KDSReadFromFile() for read data
 * --s32KDSCheckValidReadData: check validation of read data
 * 
 * @date        2013-03-05
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include "dev_kds_variant.h"
#ifdef KDS_TESTMANGER_ACTIVE  //define is set in dev_kds_variant.h
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "system_types.h"
#include "dev_kds_osal_interface.h"
#else
#include <grp.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <memory.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdio.h>
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#ifdef KDS_PUTTY_CONSOL_TRACE_ACTIVE  
#include "helper.h" 
#endif
#include "pdd_config_kds.h"
#endif

#include "dev_kds.h"
#include "dev_kds_private.h"
#include "dev_kds_trace.h"
#include "pdd.h"

/************************************************************************ 
|defines and macros 
|-----------------------------------------------------------------------*/
#define KDS_C_ENTRY_MAGIC                         0xfab
#define KDS_PDD_DATASTREAM_NAME                  "kds_data"

#ifdef OSAL_GEN3_SIM 
  #if PDD_KDS_LOCATION==PDD_LOCATION_NOR_USER
    #define PDD_KDS_LOCATION  PDD_LOCATION_FS
  #endif
#else
  #ifdef KDS_TESTMANGER_ACTIVE
    #define PDD_KDS_LOCATION  PDD_LOCATION_NOR_USER
  #else
    #if PDD_KDS_LOCATION==PDD_LOCATION_NOR_USER
      #define PDD_EARLY_ACCESS
    #endif
  #endif
#endif

/************************************************************************ 
|typedefs and struct defs
|-----------------------------------------------------------------------*/
typedef struct
{
  tU16              u16Magic;
  tU16              u16Entry;
  tU16              u16Length;
  tU16              u16Flags;
}tsKDSEntryHeader;

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/
/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static tU32 u32KDSWriteToRamBuffer (      tU8* PpFile,const void *PpData, const tU32 Pu32SizeFile, const tU32 Pu32SizeData,tU32* Ppu32Offset);
static tU32 u32KDSReadFromRamBuffer(const tU8* PpFile,      void *PpData, const tU32 Pu32SizeFile, const tU32 Pu32SizeData,tU32* Ppu32Offset);
/************************************************************************
* FUNCTIONS                                                             *
*      static  tU32 u32KDSWriteToRamBuffer()                            *
*                                                                       *
* DESCRIPTION                                                           *
*      Write data to the RAM buffer                                     *
*                                                                       *
*  HISTORY                                                              *
*      Created by Andrea B�ter(Bosch SoftTec GmbH)  21.12.2012          *
************************************************************************/
static  tU32 u32KDSWriteToRamBuffer(tU8* PpFile,const void *PpData, const tU32 Pu32SizeFile, const tU32 Pu32SizeData,tU32* Ppu32Offset)
{  /*check length and buffer*/
  if((*Ppu32Offset + Pu32SizeData <= Pu32SizeFile)&&(PpFile!=NULL)&&(PpData!=NULL))
  {
    memmove(&PpFile[*Ppu32Offset], PpData, Pu32SizeData);
    *Ppu32Offset += Pu32SizeData;
    return Pu32SizeData;
  }
  else
  {/* buffer too short ?*/  
    KDS_NORMAL_ASSERT(*Ppu32Offset + Pu32SizeData < Pu32SizeFile);
    return 0;
  }
}
/************************************************************************
* FUNCTIONS                                                             *
*      static tU32 u32KDSReadFromRamBuffer()                            *
*                                                                       *
* DESCRIPTION                                                           *
*      Read data from the RAM                                           *
*                                                                       *
*  HISTORY                                                              *
*      Created by Andrea B�ter(Bosch SoftTec GmbH)  21.12.2012          *
************************************************************************/
static tU32 u32KDSReadFromRamBuffer(const tU8* PpFile,void *PpData,const tU32 Pu32SizeFile, const tU32 Pu32SizeData,tU32* Ppu32Offset)
{/*check length and buffer*/
  if((*Ppu32Offset + Pu32SizeData <= Pu32SizeFile)&&(PpFile!=NULL)&&(PpData!=NULL))
  {
    memmove(PpData,&PpFile[*Ppu32Offset],Pu32SizeData);
    *Ppu32Offset += Pu32SizeData;
    return 1;
  }
  else
  {
    return 0;
  }
}
/************************************************************************
* FUNCTIONS                                                             *
*      s32KDSWriteData                                                  *
*                                                                       *
* DESCRIPTION                                                           *
*      Write the data from the shared memory into a RAM buffer          *
*      and call PDD for save data                                       *
* INPUTS                                                                *
*      Pointer to shared memory                                         *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
*                                                                       *
* HISTORY                                                               *
*      Created by Andrea B�ter(Bosch SoftTec GmbH)  21.12.2012          *
************************************************************************/
tS32  s32KDSWriteData(tsKDSSharedMemory* PsSharedMemory)
{
  tS32                Vs32Result=OSAL_E_NOERROR;
  tU32                Vu32Size;
  tU32                Vu32EntryNumber=0;
  tU32                Vu32Offset=0;
  tU32                Vu32MaxSize= PsSharedMemory->u32KDSNumberOfEntries*(sizeof(tsKDSEntryHeader)+KDS_MAX_ENTRY_LENGTH);  
  tsKDSEntry*         VpsEntry;
  tsKDSEntryHeader    VtsEntryHeader;
  tU8*                Vpu8DataStreamBuffer;

  /*trace out max data size to save*/
  KDS_TRACE(KDS_MAX_DATA_SIZE_WRITE_TO_FLASH,&Vu32MaxSize,sizeof(tU32));   
  /*allocate buffer */
  Vpu8DataStreamBuffer = (tU8*) malloc(Vu32MaxSize);
  /*check pointer*/
  if(Vpu8DataStreamBuffer==NULL)
  {/* error*/
    Vs32Result=OSAL_E_NOSPACE; 
    KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));
	  KDS_FATAL_ASSERT();
  }
  else
  {/* init buffer*/
    memset(Vpu8DataStreamBuffer,0x00,Vu32MaxSize);
    /*trace out number of entries*/
    KDS_TRACE(KDS_NUMBER_OF_ENTRIES,&PsSharedMemory->u32KDSNumberOfEntries,sizeof(tU32));   
    /* for all entries  */
    for(Vu32EntryNumber=0;Vu32EntryNumber<PsSharedMemory->u32KDSNumberOfEntries;Vu32EntryNumber++)
    {/* get entry */
      VpsEntry=&PsSharedMemory->sEntry[Vu32EntryNumber];
      /*check length*/
      KDS_NORMAL_ASSERT(KDS_MAX_ENTRY_LENGTH >= VpsEntry->u16EntryLength); //lint !e429  /* abr: if assert free buffer not important*/
	    /*trace out entry */
	    KDS_TRACE(KDS_ENTRY,VpsEntry,(sizeof(tsKDSEntry)-KDS_MAX_ENTRY_LENGTH)+VpsEntry->u16EntryLength);
      /* write header entry*/
      VtsEntryHeader.u16Magic = KDS_C_ENTRY_MAGIC;
	    VtsEntryHeader.u16Entry = VpsEntry->u16Entry;
	    VtsEntryHeader.u16Flags = VpsEntry->u16EntryFlags;
      VtsEntryHeader.u16Length= VpsEntry->u16EntryLength;
      Vu32Size = u32KDSWriteToRamBuffer(Vpu8DataStreamBuffer,&VtsEntryHeader,Vu32MaxSize,sizeof(VtsEntryHeader),&Vu32Offset);
      if(Vu32Size ==(int)sizeof(VtsEntryHeader))
      { /* write data*/
        Vu32Size = u32KDSWriteToRamBuffer(Vpu8DataStreamBuffer,VpsEntry->au8EntryData,Vu32MaxSize,VpsEntry->u16EntryLength,&Vu32Offset);
        if(Vu32Size != VpsEntry->u16EntryLength)
          break;
      }
      else
        break;           
    }/*end for*/       
	  /*trace out data size to save*/
	  KDS_TRACE(KDS_DATA_SIZE_WRITE_TO_FLASH,&Vu32Offset,sizeof(tU32));   
    /* save data into file and backup file*/
    tS32 Vs32ResultPdd=pdd_write_data_stream(KDS_PDD_DATASTREAM_NAME,PDD_KDS_LOCATION,Vpu8DataStreamBuffer,Vu32Offset,KDS_C_S32_IO_VERSION);
	  if(Vs32ResultPdd >= PDD_OK)
	  { /*check if all data saved*/
      if(Vu32EntryNumber!=PsSharedMemory->u32KDSNumberOfEntries)
      {/*no all data saved*/
        Vs32Result=OSAL_C_S32_IOCTRL_KDS_FULL;
	      KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));
		    KDS_NORMAL_ASSERT_NO_CONDITION();
      }
    }
	  else
	  { /*error: could not flush KDS data*/
      tU8    Vu8Buffer[200];		
      memset(Vu8Buffer,0,sizeof(Vu8Buffer));
      snprintf(Vu8Buffer,sizeof(Vu8Buffer)-1,"pdd_write_data_stream fails(): error code: %d ",Vs32ResultPdd);	      
		  KDS_SET_ERROR_ENTRY(&Vu8Buffer[0]);  	 
	    Vs32Result=OSAL_C_S32_IOCTRL_KDS_WRITE_ERROR;
	    KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));
	    KDS_NORMAL_ASSERT_NO_CONDITION();
    }
	  free(Vpu8DataStreamBuffer);
  }
  return(Vs32Result);
}

/************************************************************************
* FUNCTIONS                                                             *
*      s32KDSReadData                                                   *
*                                                                       *
* DESCRIPTION                                                           *
*      call function s32KDSReadFromFile() for read data into ram buffer *
*      and copy from the ram buffer into the shared memory              *
* INPUTS                                                                *
*      Pointer to shared memory                                         *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
*                                                                       *
* HISTORY                                                               *
*      Created by Andrea B�ter(Bosch SoftTec GmbH)   21.12.2012         *
*      changed by Andrea B�ter | read early from NOR 16.11.2015         *
************************************************************************/
tS32  s32KDSReadData(tsKDSSharedMemory* PsSharedMemory)
{
  tS32       Vs32Result=OSAL_E_NOERROR;
  tS32       Vs32LenghtDataStream;

  /*state data no valid data read*/
  PsSharedMemory->u16StateReadFlashData=0xffff;  //no data read  
  /* get size lenght of datastream */  
  #ifdef PDD_EARLY_ACCESS
  Vs32LenghtDataStream=PDD_NOR_USER_CLUSTER_SIZE; 
  #else
  Vs32LenghtDataStream=pdd_get_data_stream_size(KDS_PDD_DATASTREAM_NAME,PDD_KDS_LOCATION);	
  /*check lenght*/
  if(Vs32LenghtDataStream<=0)
  { /*error: => nor KDS found */    
    Vs32Result=OSAL_C_S32_IOCTRL_KDS_NOT_FOUND;   
    KDS_TRACE(KDS_INFO_NO_KDS_DATA,&Vs32Result,sizeof(tS32));	
	  /*error memory entry*/
	  KDS_SET_ERROR_ENTRY("s32KDSReadData: no kds data found");
  }
  else
  #endif
  {/* allocate buffer */
    tU8* Vpu8DataStreamBuffer = (tU8*) malloc(Vs32LenghtDataStream);    
    /* trace out data stream size */
	  KDS_TRACE(KDS_READ_MAX_FILE_SIZE,&Vs32LenghtDataStream,sizeof(Vs32LenghtDataStream));
	  /*check pointer*/
    if (Vpu8DataStreamBuffer==NULL)
	  { /* trace error*/		
      Vs32Result=OSAL_E_NOSPACE;   
	    KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));
	    KDS_FATAL_ASSERT();
	  }
	  else
	  {
	    tU8 Vu8Info=PDD_READ_INFO_NORMAL_FILE;
      #ifdef PDD_EARLY_ACCESS
	    Vs32Result=pdd_read_datastream_early_from_nor(KDS_PDD_DATASTREAM_NAME,Vpu8DataStreamBuffer,Vs32LenghtDataStream,KDS_C_S32_IO_VERSION,&Vu8Info);
      #else
      Vs32Result=pdd_read_datastream(KDS_PDD_DATASTREAM_NAME,PDD_KDS_LOCATION,Vpu8DataStreamBuffer,Vs32LenghtDataStream,KDS_C_S32_IO_VERSION,&Vu8Info);
      #endif
	    KDS_TRACE(KDS_READ_DATA_SIZE,&Vs32Result,sizeof(Vs32Result));	  
	    /*check return value*/
	    if(Vs32Result<=0)
	    {
	      Vs32Result=OSAL_C_S32_IOCTRL_KDS_NOT_FOUND;   
        KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));	    
		    /* error memory entry*/
		    KDS_SET_ERROR_ENTRY("s32KDSReadData: no valid kds data read from PDD");
	    }
	    else	 
	    {
	      tBool              VbLastEntry=FALSE;   
	      tsKDSEntryHeader   VtsEntryHeader;
	      tU32               Vu32Offset=0;

		    if(Vu8Info==PDD_READ_INFO_NORMAL_FILE)
		    {/*set state read data to valid */	
          PsSharedMemory->u16StateReadFlashData=M_KDS_INFO_READ_FLASH_DATA_ACTUAL; 
		    }
		    else
		    {/*PDD_READ_INFO_BACKUP_FILE*/
          PsSharedMemory->u16StateReadFlashData=M_KDS_INFO_READ_FLASH_DATA_BACKUP_FILE; 
		    }
        /* while not last entry*/
        while(VbLastEntry==FALSE)
        { /* read header from buffer */
          if(u32KDSReadFromRamBuffer(Vpu8DataStreamBuffer,&VtsEntryHeader,(tU32)Vs32Result,sizeof(tsKDSEntryHeader),&Vu32Offset)==0)
	        {
	  	      VbLastEntry=TRUE;			
		      }
		      else
		      { /* check magic*/
            if(VtsEntryHeader.u16Magic != KDS_C_ENTRY_MAGIC)
            {/*no valid entry more; end loop*/
			        tU8 Vu8TraceBuf[20];
              VbLastEntry=TRUE;
			        snprintf((tU8*)&Vu8TraceBuf[0],sizeof(Vu8TraceBuf),"entry magic:%d",VtsEntryHeader.u16Magic);
		          KDS_TRACE(KDS_ERROR_READ,&Vu8TraceBuf[0],strlen(Vu8TraceBuf)+1);			
            }
            else
            {/*check length*/
              if(VtsEntryHeader.u16Length <= KDS_MAX_ENTRY_LENGTH)
              {/* init entry */
			          tsKDSEntry*   VpsEntrySharedMemory;
                VpsEntrySharedMemory=&PsSharedMemory->sEntry[PsSharedMemory->u32KDSNumberOfEntries];
                memset(VpsEntrySharedMemory,0x00,sizeof(tsKDSEntry));	         
			          /*get data*/
                if(u32KDSReadFromRamBuffer(Vpu8DataStreamBuffer,&VpsEntrySharedMemory->au8EntryData[0],(tU32)Vs32Result,VtsEntryHeader.u16Length,&Vu32Offset)==0)
                {
				          tU8 Vu8TraceBuf[50];
                  VbLastEntry=TRUE;
				          snprintf((tU8*)&Vu8TraceBuf[0],sizeof(Vu8TraceBuf),"read file to short -- size file: %d bytes",Vs32Result);
		              KDS_TRACE(KDS_ERROR_READ,&Vu8TraceBuf[0],strlen(Vu8TraceBuf)+1);	
				          free(Vpu8DataStreamBuffer);
				          KDS_NORMAL_ASSERT_NO_CONDITION();
                }
			          else
			          { /*set entry*/
			            VpsEntrySharedMemory->u16Entry=VtsEntryHeader.u16Entry;
                  /*set entry flags*/
			            VpsEntrySharedMemory->u16EntryFlags=VtsEntryHeader.u16Flags;
			            /*set entry length*/
			            VpsEntrySharedMemory->u16EntryLength=VtsEntryHeader.u16Length;
				          ++PsSharedMemory->u32KDSNumberOfEntries;    				  
				          /*trace out entry */
				          KDS_TRACE(KDS_ENTRY,VpsEntrySharedMemory,(sizeof(tsKDSEntry)-KDS_MAX_ENTRY_LENGTH)+VtsEntryHeader.u16Length);
                }/*end else read data OK*/
              }/*end if lenght OK*/
		          else
              {/* data entry to great; data not saved: normal assert*/
                tU8 Vu8TraceBuf[50];
                VbLastEntry=TRUE;
				        snprintf((tU8*)&Vu8TraceBuf[0],sizeof(Vu8TraceBuf),"entry lenght of data to great - lenght:%d",VtsEntryHeader.u16Length);
		            KDS_TRACE(KDS_ERROR_READ,&Vu8TraceBuf[0],strlen(Vu8TraceBuf)+1);	
				        free(Vpu8DataStreamBuffer);
                KDS_NORMAL_ASSERT(VtsEntryHeader.u16Length <= KDS_MAX_ENTRY_LENGTH); 
              }
            }/*end else magic OK*/
          }/*end else read header OK*/		
        }/*end while*/  
      }/*end if else read from file*/	  
      /*buffer free*/
      free(Vpu8DataStreamBuffer);
    }	
  }
  /*trace out number of entries*/
  KDS_TRACE(KDS_NUMBER_OF_ENTRIES,&PsSharedMemory->u32KDSNumberOfEntries,sizeof(tU32));    
  /*set to no error; pdd waits until access to nor or file system available; */
  Vs32Result=OSAL_E_NOERROR;
  return(Vs32Result);
}  
#ifndef KDS_TESTMANGER_ACTIVE 
/******************************************************************************
* FUNCTION: s32KDSChangeGroupAccess()
*
* DESCRIPTION: change group access to eco_pdd
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2012 10 10
*****************************************************************************/
tS32  s32KDSChangeGroupAccess(teKDSKindRes PeKindRes,tS32 Ps32Id,const char* PStrResource)
{
  tS32   Vs32Result=OSAL_E_NOERROR;
  tU8    Vu8Buffer[40];	

  memset(Vu8Buffer,0,sizeof(Vu8Buffer));
  /*  The getgrnam() function returns a pointer to a structure containing the group id*/
  struct group *VtGroup=getgrnam(PDD_ACCESS_RIGTHS_GROUP_NAME);
  if(VtGroup==NULL)
  { 
    //Vs32Result=OSAL_E_UNKNOWN;  
	  //snprintf(Vu8Buffer,sizeof(Vu8Buffer),"getgrnam() fails for resource %s",PStrResource);
  }
  else
  { /*for named semaphore*/
	  if(PeKindRes==KDS_KIND_RES_SEM)
	  {
	    if(chown(PStrResource,(uid_t) -1, VtGroup->gr_gid)==-1)
	    {	
		    Vs32Result=OSAL_E_UNKNOWN;  	 
   	    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"chown() fails for resource %s",PStrResource);
      }
	    else
	    {
        if(chmod(PStrResource, PDD_ACCESS_RIGTHS) == -1)
        {		
          Vs32Result=OSAL_E_UNKNOWN;  	 
	        snprintf(Vu8Buffer,sizeof(Vu8Buffer),"chmod() fails for resource %s",PStrResource);
        }
      }
    }
	  else
	  {/* for other resource*/
	    if(fchown(Ps32Id, (uid_t) -1, VtGroup->gr_gid)==-1)
	    {	   	
		    Vs32Result=OSAL_E_UNKNOWN;  	
   	    snprintf(Vu8Buffer,sizeof(Vu8Buffer),"fchown() fails for resource %s",PStrResource);
      }
	    else
	    {
        if(fchmod(Ps32Id, PDD_ACCESS_RIGTHS) == -1)
        {        
          Vs32Result=OSAL_E_UNKNOWN;  	         
	        snprintf(Vu8Buffer,sizeof(Vu8Buffer),"fchmod() fails for resource %s",PStrResource);	     
        }
      }
    }
  }
  /*trace error code and set error memory*/
  if(Vs32Result!=OSAL_E_NOERROR)
  {
    KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));
    KDS_vSetErrorEntry(&Vu8Buffer[0]);
  }
  return(Vs32Result);
}
#endif
/******************************************************************************/
/* End of File dev_kds_file_access.c                                          */
/******************************************************************************/