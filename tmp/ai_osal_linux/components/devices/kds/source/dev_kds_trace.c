/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_kds_trace.c
 *
 * This file contains the function for trace output
 * 
 * global function:
 * -- KDS_vTrace
 *
 * @date        2012-12-21
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#include <ctype.h>
#include <string.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <unistd.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"  		           // use for tsKDSEntry in kds_private.h

#include "dev_kds_variant.h"
#include "dev_kds_trace.h"
#include "dev_kds_private.h"

#ifdef KDS_TTFIS_CONSOL_TRACE_ACTIVE   //define set in dev_kds_variant
#include "OsalConf.h"
#define TRACE_S_IMPORT_INTERFACE_TYPES
#include "trace_if.h"
#include "Linux_osal.h"
#else
#include "helper.h" 
#endif
/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#ifdef KDS_TTFIS_CONSOL_TRACE_ACTIVE
#define KDS_TRACE_DATA_BYTE_KIND_INFO   0
#endif

/*length for traces*/
#define KDS_TTFIS_LEN_DATA                    224
#define KDS_TTFIS_LEN_HEADER                    1
#define KDS_LEN_MESSAGE                 (KDS_TTFIS_LEN_DATA+KDS_TTFIS_LEN_HEADER)

#ifdef KDS_TTFIS_CONSOL_TRACE_ACTIVE
/* **************************************************FunctionHeaderBegin** *//*
* static void KDS_vTTFISConsolTrace(u8 Pu8Kind, u8* Pu8pData, u8 Pu8Len) 
*
* trace output function
*
* @param   Pu8Kind:    kind of level
*          Pu8pData:   pointer of the data, which send
*          Pu8Len:     length of the data, which send
*
* @return  void    
*
* @date    2012-12-21
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void KDS_vTTFISConsolTrace(tU8 Pu8Kind, const tU8* Pu8pData, tU8 Pu8Len)
{
  tU8                Vu8LenHeader=KDS_TTFIS_LEN_HEADER; 
  tU8                Vu8Buffer[KDS_LEN_MESSAGE]; 
  TR_tenTraceLevel   VtrLevel;

  /* set kind */
  Vu8Buffer[KDS_TRACE_DATA_BYTE_KIND_INFO] = Pu8Kind;
  /* kind error  */
  if (Pu8Kind <= KDS_LEVEL_IMPORTANT)
  {
    VtrLevel=TR_LEVEL_FATAL;
  }/*end if*/
  else
  {
    VtrLevel=TR_LEVEL_USER_4; 
  }/*end else*/
  /* check param len and data*/
  if ((Pu8pData!=NULL)&&((Pu8Len+Vu8LenHeader) <= sizeof(Vu8Buffer)))
  { /* set error information*/
    memmove(&Vu8Buffer[Vu8LenHeader],(void*)Pu8pData,Pu8Len);
  }/*end if*/
  else Pu8Len=0;
  /*trace*/
  TR_core_uwTraceOut(Pu8Len + Vu8LenHeader, TR_TTFIS_KDS, VtrLevel, Vu8Buffer);  
}/*end function*/
#else
/* **************************************************FunctionHeaderBegin** *//*
* static void KDS_vPuttyConsolTrace(u8 Pu8Kind, u8* Pu8pData, u8 Pu8Len) 
*
* trace output function
*
* @param   Pu8Kind:    kind of level
*          Pu8pData:   pointer of the data, which send
*          Pu8Len:     length of the data, which send
*
* @return  void    
*
* @date    2012-12-21
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void KDS_vPuttyConsolTrace(tU8 Pu8Kind, const void* Pu8pData, tU8 Pu8Len)
{ 
  tU8    Vu8Buffer[KDS_LEN_MESSAGE]; 

  memset(Vu8Buffer,0,KDS_LEN_MESSAGE);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(Pu8Len); 
    if (Pu8Kind <= KDS_LEVEL_IMPORTANT)
    {
      switch(Pu8Kind)
	  {
	    case KDS_ERROR:
	    {
		  if(Pu8pData!=NULL)
		  {
            tS32* VS32ErrorCode=(tS32*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS_ERROR: 0x%04x: %d",*VS32ErrorCode,*VS32ErrorCode);   
		  }
	    }break;
	    case KDS_ERROR_READ:
	    {
		  if(Pu8pData!=NULL)
		  {
		    char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS_ERROR_READ: %s",VcString);   
		  }
	    }break;		
	    case KDS_ERROR_SEM:
	    {
		  if(Pu8pData!=NULL)
		  {
            char* VcString=(char*) Pu8pData;
	        snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS_ERROR_SEM: %s",VcString);  
		  }
	    }break;
		case KDS_ERROR_MMAP:
		{
		  if(Pu8pData!=NULL)
		  {
		    char* VcString=(char*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS_ERROR_MMAP: %s",VcString);  
		  }
		}break;
		case KDS_INFO_NO_KDS_DATA:
		{
		  if(Pu8pData!=NULL)
		  {
		    tS32* VS32ErrorCode=(tS32*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: no kds data found:%d",*VS32ErrorCode,*VS32ErrorCode);  
		  }
		}break;
		default:
		{
	      snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: no trace defined for KDS_LEVEL_IMPORTANT");  
		}break;	
	  }	
      vConsoleTrace(STDERR_FILENO,(const void*)Vu8Buffer,strlen(Vu8Buffer)+1); 
    }
	#ifdef KDS_LEVEL_INTERFACE_CALL
	else if(Pu8Kind <= KDS_LEVEL_INTERFACE_CALL)
	{
		switch(Pu8Kind)
		{
		  case KDS_START_FUNCTION_INIT:
		  {		
			  snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: start function:KDS_s32IODeviceInit()");   
		  }break;
		  case KDS_EXIT_FUNCTION_INIT:
		  {
		    if(Pu8pData!=NULL)
		    {
		      tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: exit  function:KDS_s32IODeviceInit(): return value: %d",*VS32ErrorCode);   		    
			}
		  }break;
		  case KDS_START_FUNCTION_REMOVE:   
		  {		
			 snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: start function:KDS_s32IODeviceRemove()");   
		  }break;
		  case KDS_EXIT_FUNCTION_REMOVE:
		  {
		    if(Pu8pData!=NULL)
		    {
		      tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: exit  function:KDS_s32IODeviceRemove(): return value: %d",*VS32ErrorCode);   		    
			}
		  }break;
		  case KDS_START_FUNCTION_OPEN:
		  {		
			 snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: start function:KDS_IOOpen()");   
		  }break;
		  case KDS_EXIT_FUNCTION_OPEN:
		  {
		    if(Pu8pData!=NULL)
		    {
		      tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: exit  function:KDS_IOOpen(): return value: %d",*VS32ErrorCode);   		    
			}
		  }break;
		  case KDS_START_FUNCTION_CLOSE:
		  {		
			 snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: start function:KDS_s32IOClose()");   
		  }break;
		  case KDS_EXIT_FUNCTION_CLOSE:
		  {
		    if(Pu8pData!=NULL)
		    {
		      tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: exit  function:KDS_s32IOClose(): return value: %d",*VS32ErrorCode);   		    
			}
		  }break;
		  case KDS_START_FUNCTION_CONTROL:
		  {		
			 snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: start function:KDS_s32IOControl()");   
		  }break;
		  case KDS_EXIT_FUNCTION_CONTROL:
		  {
		    if(Pu8pData!=NULL)
		    {
		      tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: exit  function:KDS_s32IOControl(): return value: %d",*VS32ErrorCode);   		    
			}
		  }break;
		  case KDS_START_FUNCTION_READ:
		  {		
			 snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: start function:KDS_s32IORead()");   
		  }break;
		  case KDS_EXIT_FUNCTION_READ:
		  {
		    if(Pu8pData!=NULL)
		    {
		      tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: exit  function:KDS_s32IORead(): return value: %d",*VS32ErrorCode);   		    
			}
		  }break;
		  case KDS_START_FUNCTION_WRITE:
		  {		
			 snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: start function:KDS_s32IOWrite()");   
		  }break;
		  case KDS_EXIT_FUNCTION_WRITE:
		  {
		    if(Pu8pData!=NULL)
		    {
		      tS32* VS32ErrorCode=(tS32*) Pu8pData;
		      snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: exit  function:KDS_s32IOWrite(): return value: %d",*VS32ErrorCode);   		    
			}
		  }break;
		  default:
		  {
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: no trace defined for KDS_LEVEL_INTERFACE_CALL");  
		  }
		}           
		vConsoleTrace(STDOUT_FILENO,(const void*)Vu8Buffer,strlen(Vu8Buffer)+1); 
	}
    #endif
    #ifdef KDS_LEVEL_INFO_GENERALLY
	else if(Pu8Kind <= KDS_LEVEL_INFO_GENERALLY)
	{
	  switch(Pu8Kind)
	  {
	    case KDS_NUMBER_OF_ENTRIES:  
	    {
		  if(Pu8pData!=NULL)
		  {
		    tU32* Vu32Number=(tU32*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: number of entries: %d",*Vu32Number);  		 
		  }
	    }break;
	    case KDS_ENTRY:    
	    {
		  if(Pu8pData!=NULL)
		  {
		    tsKDSEntry*  VpsEntry=(tsKDSEntry*)Pu8pData;
		    int          ViInc=0;
		    char         VcBufValue[10]; 
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: u16Entry: 0x%02x  u16EntryLength:%d  u16EntryFlags:%d\n",VpsEntry->u16Entry,VpsEntry->u16EntryLength,VpsEntry->u16EntryFlags);  		 
  	        for (ViInc=0;ViInc<VpsEntry->u16EntryLength;ViInc++)
            {		   
              snprintf(VcBufValue,sizeof(VcBufValue),"0x%02x ",VpsEntry->au8EntryData[ViInc]);
			  if((strlen(VcBufValue)+strlen(Vu8Buffer)+1)< KDS_LEN_MESSAGE)
                strncat(&Vu8Buffer[0],(const char*)&VcBufValue[0],strlen(VcBufValue)+1);
            }
		  }
	    }break;
	    case KDS_DATA_SIZE_WRITE_TO_FLASH: 
	    {
		  if(Pu8pData!=NULL)
		  {
		    tU32* Vu32Value=(tU32*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: data bytes write to flash: %d",*Vu32Value);  
		  }
	    }break;
	    case KDS_MAX_DATA_SIZE_WRITE_TO_FLASH:     
	    {
		  if(Pu8pData!=NULL)
		  {
		    tU32* Vu32Value=(tU32*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: max data bytes write to flash: %d",*Vu32Value);  
		  }
	    }break;
	    case KDS_READ_MAX_FILE_SIZE:    
	    {
		  if(Pu8pData!=NULL)
		  {
		    tU32* Vu32Value=(tU32*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: function read data: max file size: %d bytes",*Vu32Value);  
		  }
	    }break;
	    case KDS_READ_DATA_SIZE:    
	    {
		  if(Pu8pData!=NULL)
		  {
		    tU32* Vu32Value=(tU32*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: function read data: data size: %d bytes",*Vu32Value);  
		  }
	    }break;
	    default:
	    {
		  snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: no trace defined for KDS_LEVEL_INFO_GENERALLY");  
	    }break;
	  }/*end switch*/
	  vConsoleTrace(STDOUT_FILENO,(const void*)Vu8Buffer,strlen(Vu8Buffer)+1); 
	}/*end if else*/
    #endif
	#ifdef KDS_LEVEL_INFO_SHARED_KDS
	else if(Pu8Kind <= KDS_LEVEL_INFO_SHARED_KDS)
	{
      switch(Pu8Kind)
	  {
	    case KDS_ATTACH_COUNT:
		{
		  if(Pu8pData!=NULL)
		  {
		    tU32* Vu32Value=(tU32*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: number process attach: %d",*Vu32Value);  
		  }
		}break;
		case KDS_SEM_VALUE:
        {
		  if(Pu8pData!=NULL)
		  {
		    tS32* Vs32Value=(tS32*) Pu8pData;
		    snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: sem value:%d",*Vs32Value);  
		  }
		}break;
		case KDS_SEM_WAIT:
		{
		  snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: sem lock");  
		}break;
		case KDS_SEM_POST:
		{
		  snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: sem unlock");  
		}break;
        default:
	    {
		  snprintf(Vu8Buffer,KDS_LEN_MESSAGE,"KDS: no trace defined for KDS_LEVEL_INFO_SHARED_KDS");  
	    }break;
      }/*end switch*/
	  vConsoleTrace(STDOUT_FILENO,(const void*)Vu8Buffer,strlen(Vu8Buffer)+1); 
	}
    #endif
}
#endif
/* **************************************************FunctionHeaderBegin** *//*
* void KDS_vSetErrorEntry(u8* Pu8pData, u8 Pu8Len) 
*
* trace output function
*
* @param   Pu8pData:   pointer of the data, which send
*          Pu8Len:     length of the data, which send
*
* @return  void    
*
* @date    2014-02-24
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void KDS_vSetErrorEntry(const tChar* PpData)
{
  tU8                Vu8Buffer[KDS_LEN_MESSAGE]; 
  Vu8Buffer[0]=KDS_NOR_ERROR_MEMORY;
  snprintf(&Vu8Buffer[1],KDS_LEN_MESSAGE-1,"KDS:%s",PpData); 
  WriteErrMemEntry((int)TR_TTFIS_KDS,&Vu8Buffer[0],strlen(&Vu8Buffer[1])+1,0);
}

