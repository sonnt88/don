/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_kds.c
 *
 * This file contains /dev/kds (Konfigurationsdatenspeicher) 
 * 
 * global function:
 * -- KDS_s32IODeviceInit
 *      Create the KDS-Device and make a ram-copy of the actual keys
 * -- KDS_s32IODeviceRemove
 *      Destroy the ram-copy of the actual valid KDS-Entries
 * -- KDS_IOOpen
 *      return true
 * -- KDS_s32IOClose
  *      return true
 * -- KDS_s32IOControl
 *       Call a control function 
 * -- KDS_s32IOWrite
 *       write a entry data to RAM buffer
 * -- KDS_s32IORead
 *      read a entry data from RAM buffer 
 *
 * local function:
 * -- psKDSLock
 *       lock shared memory and get pointer of shared memory
 * -- vKDSUnLock
 *       unlock semaphore
 * -- vKDSClearData
 *       clear all data, which saved in RAM buffer 
 * -- psKDSGetNextEntry
 *       get next KDS entry
 * -- u32KDSGetEntry
 *       get entry of an entry number
 * -- s32KDSRemoveEntry
 *        remove entry
 * -- vKDSAddEntry
 *        add entry
 *
 * @date        2010-08-09
 *
 * @note
 *
 *  &copy; Copyright BSOT GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */

/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include "dev_kds_variant.h"
#ifdef KDS_TESTMANGER_ACTIVE  //define is set in dev_kds_variant.h
#include <string.h>
#include "system_types.h"
#include "dev_kds_osal_interface.h"
#else
#include <ctype.h>
#include <string.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#ifdef KDS_PUTTY_CONSOL_TRACE_ACTIVE  
#include "helper.h" 
#endif
#endif
#include "dev_kds.h"
#include "dev_kds_private.h"
#include "dev_kds_trace.h"
#include "pdd.h"

/************************************************************************ 
|defines and macros 
|-----------------------------------------------------------------------*/
#ifndef KDS_TESTMANGER_ACTIVE
#define KDS_NAME_SHARED_MEMORY	"KDS_SHAREDMEM"
#endif
/************************************************************************ 
|typedefs and struct defs
|-----------------------------------------------------------------------*/


/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/
#ifndef KDS_TESTMANGER_ACTIVE
static sem_t*                vpKDS_SemLockInit;          /*id for process init*/
static tsKDSSharedMemory*    vpKDSSharedMemory = NULL;   /*pointer of shared memory*/
static tS32                  vs32KDSShmId = 0;           /*shared memory id*/
#else
static tsKDSSharedMemory     vtKDSRamMirror;
static tsKDSSharedMemory*    vpKDSSharedMemory = &vtKDSRamMirror;   /*pointer of memory*/
#endif

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
#ifndef KDS_TESTMANGER_ACTIVE
static		 tS32				s32KDSShmInit(tBool VbFirstAttach);
#endif
static       tS32               s32KDSIsInit(tsKDSSharedMemory* PtsSharedMemory);
static       tsKDSSharedMemory* psKDSLock(void);
static       void               vKDSUnLock(tsKDSSharedMemory* PtsShMemKDS);
static       void               vKDSClearData(tsKDSSharedMemory* PtsShMemKDS);
static const tsKDSEntry*        psKDSGetNextEntry(tsKDSSharedMemory*,tU16);
static       tU32               u32KDSGetEntry(tsKDSSharedMemory*,tU16 Pu16Entry);
static       tS32               s32KDSRemoveEntry(tsKDSSharedMemory*,tU16 Pu16Entry);
static       void               vKDSAddEntry(tsKDSSharedMemory*,const tsKDSEntry *PspKdsEntry,tU32);

#ifdef LOAD_KDS_SO
tS32 kds_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Id); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(szName); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enAccess); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32FD); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(app_id); 
   return KDS_IOOpen();
}

tS32 kds_drv_io_close(tS32 s32ID, tU32 u32FD)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD); 
   return KDS_s32IOClose(); 
}

tS32 kds_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, intptr_t s32arg)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD); 
   return KDS_s32IOControl(s32fun, s32arg); 
}

tS32 kds_drv_io_write(tS32 s32ID, tU32 u32FD, tPCS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ret_size); 
   return KDS_s32IOWrite(pBuffer, u32Size);
}

tS32 kds_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD); 
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(ret_size); 
   return KDS_s32IORead(pBuffer, u32Size);
}
void __attribute__ ((constructor)) kds_process_attach(void)
{
  KDS_s32IODeviceInit();
}

void vOnProcessDetach( void )
{
  KDS_s32IODeviceRemove();
}

#endif
/************************************************************************
* FUNCTIONS                                                             *
*      KDS_s32IODeviceInit                                              *
*                                                                       *
* DESCRIPTION                                                           *
*      Create the KDS-Device and make a ram-copy of the actual keys     *
*      This will be done, because there are blocking problems if the    *
*      flash is busy.                                                   *
*                                                                       *
* INPUTS                                                                *
*      tVoid                                                            *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
*                                                                       *
*  HISTORY                                                              *
************************************************************************/
tS32 KDS_s32IODeviceInit(tVoid)
{
  tS32    Vs32Result=OSAL_E_NOERROR;
   
  /* TRACE */
  KDS_TRACE(KDS_START_FUNCTION_INIT,0,0);
  #ifndef KDS_TESTMANGER_ACTIVE
  /*create sem, if not failed first attach*/
  vpKDS_SemLockInit = sem_open("KDS_INIT",O_EXCL | O_CREAT, PDD_ACCESS_RIGTHS, 0);
  if (vpKDS_SemLockInit != SEM_FAILED)
  { /*process first attach*/
	  Vs32Result=s32KDSShmInit(TRUE); /*get pointer vpKDSSharedMemory*/
	  if(Vs32Result==(tS32)OSAL_E_NOERROR)
	  { /*create semaphore and save id to shared memory*/
      sem_t* VpSemLock=&vpKDSSharedMemory->tSemLockAccess;
      if(sem_init(VpSemLock, 1/*shared*/, 1) < 0)
      {
	      KDS_TRACE(KDS_ERROR_SEM,"create",strlen("create")+1);
	      KDS_FATAL_ASSERT();
	    }	  			
	    /*init shared variables  */
      vpKDSSharedMemory->vbValidSharedMemory=FALSE;      //=> set to true if data of shared memory OK
	    vpKDSSharedMemory->s32AttachedProcesses=0;	  
	    /*set access rights to Group "eco_pdd*/
	    #ifndef KDS_TESTMANGER_ACTIVE 
      s32KDSChangeGroupAccess(KDS_KIND_RES_SEM,-1,"/dev/shm/sem.KDS_INIT");
      #endif
	  }
  }
  else
  { /* not first attach*/
	  /* open semaphore*/
    vpKDS_SemLockInit= sem_open("KDS_INIT", 0);
    if(vpKDS_SemLockInit!= SEM_FAILED)
    { /* lock semaphore*/
      sem_wait(vpKDS_SemLockInit);     	 
       /*get pointer shared memory*/
      Vs32Result=s32KDSShmInit(FALSE); /*get pointer vpKDSSharedMemory*/
    }
    else
    { //error semaphore should be created
      KDS_NORMAL_ASSERT_NO_CONDITION();
      Vs32Result=OSAL_E_UNKNOWN;
      /*error memory entry*/
      tU8 Vu8Buffer[100];
      snprintf(Vu8Buffer,100,"s32IODeviceInit: function sem_open() fails; errno:%d",errno); 
	    KDS_SET_ERROR_ENTRY(Vu8Buffer);
    }   
  } 
  /*for all process*/
  if(Vs32Result==(tS32)OSAL_E_NOERROR)
  {  
	  tsKDSSharedMemory* VsShMemKDS=psKDSLock();
    if(VsShMemKDS==NULL)
    {
      Vs32Result=OSAL_E_NOACCESS;  
    }
    else
	  { /*increment process counter*/
      VsShMemKDS->s32AttachedProcesses++;
      KDS_TRACE(KDS_ATTACH_COUNT,&VsShMemKDS->s32AttachedProcesses,sizeof(tU32));    
	    /* check if trace and reload done*/
	    s32KDSIsInit(VsShMemKDS);    
	    /*unLock*/
	    vKDSUnLock(VsShMemKDS);
	  } 
  }
  if(vpKDS_SemLockInit!= SEM_FAILED)
    sem_post(vpKDS_SemLockInit);
  #else  
  s32KDSIsInit(&vtKDSRamMirror);   
  #endif
  /* TRACE */
  KDS_TRACE(KDS_EXIT_FUNCTION_INIT,&Vs32Result,sizeof(tS32));

  return(Vs32Result);
}
/************************************************************************
* FUNCTIONS                                                             *
*       KDS_s32IODeviceRemove                                           *
*                                                                       *
* DESCRIPTION                                                           *
*      Destroy the ram-copy of the actual valid KDS-Entries             *
*                                                                       *
* INPUTS                                                                *
*      tVoid                                                            *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
************************************************************************/
tS32 KDS_s32IODeviceRemove(tVoid)
{
  tS32       Vs32Result=OSAL_E_NOERROR;
  tsKDSSharedMemory* VsShMemKDS=psKDSLock();
  /*trace function start*/
  KDS_TRACE(KDS_START_FUNCTION_REMOVE,0,0);
  /*if shared memory valid */
  if(VsShMemKDS==NULL)
  {
    Vs32Result=OSAL_C_S32_IOCTRL_KDS_NOT_FOUND;
  }
  else
  { /*if shared memory valid */
     #ifndef KDS_TESTMANGER_ACTIVE
    /*decrement counter*/
    VsShMemKDS->s32AttachedProcesses--;
	  KDS_TRACE(KDS_ATTACH_COUNT,&VsShMemKDS->s32AttachedProcesses,sizeof(tU32));
	  /*all process closed */
	  if(VsShMemKDS->s32AttachedProcesses==0)
	  { /* write data to flash*/
      s32KDSWriteData(VsShMemKDS); 	  
	  }
	  /*wait semaphore */
    if(vpKDS_SemLockInit!= SEM_FAILED)
	    sem_wait(vpKDS_SemLockInit);
	  /*unLock*/
	  vKDSUnLock(VsShMemKDS);
	  /* if last process: delete sem*/
    if(VsShMemKDS->s32AttachedProcesses==0)
	  {/*delete sem*/
	    sem_destroy(&VsShMemKDS->tSemLockAccess);
	  }
	  /*unmap memory from process*/
	  if(munmap(vpKDSSharedMemory,sizeof(tsKDSSharedMemory)) == -1)
    {
      KDS_TRACE(KDS_ERROR_MMAP,"munmap",strlen("munmap")+1);
      KDS_FATAL_ASSERT();
    }
	  vpKDSSharedMemory = NULL;
    /* if last process: delete shared memory*/
    if(VsShMemKDS->s32AttachedProcesses==0)
	  {/*delete shm*/
	    shm_unlink(KDS_NAME_SHARED_MEMORY);
	    vs32KDSShmId=-1;	  
	  }
	  /*post semaphore*/
    if(vpKDS_SemLockInit!= SEM_FAILED)
	    sem_post(vpKDS_SemLockInit);	 
	  #else	
    s32KDSWriteData(VsShMemKDS);
	  memset(VsShMemKDS,0,sizeof(tsKDSSharedMemory)); //clear all infos
    #endif
  }/*end if*/
  /* trace exit function*/
  KDS_TRACE(KDS_EXIT_FUNCTION_REMOVE,&Vs32Result,sizeof(tS32));
  return(Vs32Result); 
}
/************************************************************************
* FUNCTIONS                                                             *
*      KDS_IOOpen                                                       *
*                                                                       *
* DESCRIPTION                                                           *
*     return true                                                       *
*                                                                       *
* INPUTS                                                                *
*      tVoid                                                            *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
************************************************************************/
tS32 KDS_IOOpen(tVoid)
{
  tS32  Vs32Result=OSAL_E_NOERROR;
  /* trace function start*/
  KDS_TRACE(KDS_START_FUNCTION_OPEN,0,0);
  
#ifndef LOAD_KDS_SO  //if no shared memory function KDS_s32IODeviceInit is called once
  /*check if pointer vpKDSSharedMemory get for process*/
  if(vpKDSSharedMemory == 0)
  {
    Vs32Result=KDS_s32IODeviceInit();   
  }
  if(Vs32Result==OSAL_E_NOERROR)
#endif
  { /* lock and get memory*/
    tsKDSSharedMemory* VsShMemKDS=psKDSLock();
    if(VsShMemKDS==NULL)
    {
      Vs32Result=OSAL_E_NOACCESS;  
    }
    else
    { /*check reload done*/
	    Vs32Result=s32KDSIsInit(VsShMemKDS); 
	    vKDSUnLock(VsShMemKDS);
    }   
  }
  /* trace exit function*/
  KDS_TRACE(KDS_EXIT_FUNCTION_OPEN,&Vs32Result,sizeof(tS32));
  return(Vs32Result);
}
/************************************************************************
* FUNCTIONS                                                             *
*      KDS_s32IOClose                                                   *
*                                                                       *
* DESCRIPTION                                                           *
*     return true                                                       *
*                                                                       *               
* INPUTS                                                                *
*      tVoid                                                            *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error code                                                 *
************************************************************************/
tS32 KDS_s32IOClose( tVoid )
{
  tS32  Vs32Result=OSAL_E_NOERROR;
  /*trace function start*/
  KDS_TRACE(KDS_START_FUNCTION_CLOSE,0,0);  
 
  /* trace exit function*/
  KDS_TRACE(KDS_EXIT_FUNCTION_CLOSE,&Vs32Result,sizeof(tS32));
  return(Vs32Result);
}

/************************************************************************
* FUNCTIONS                                                             *
*      KDS_s32IOControl                                                 *
*                                                                       *
* DESCRIPTION                                                           *
*  Call a control function                                              *
*  Following controls are supported and Ps32arg has to be of the        *
*  described type:                                                      *
*                                                                       *
*  Control (Ps32fun)                          Type of Ps32arg           *
*  OSAL_C_S32_IOCTRL_VERSION                 tPS32                      *
*  OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE        tBool                      *
*  OSAL_C_S32_IOCTRL_KDS_GET_ENTRY           tKdsEntry                  *
*  OSAL_C_S32_IOCTRL_KDS_GET_NEXT_ID         tPU16                      *
*  OSAL_C_S32_IOCTRL_KDS_GET_PREV_ID         tPU16                      *
*  OSAL_C_S32_IOCTRL_KDS_CHECK               tsKDSCheckResult*          *
*  OSAL_C_S32_IOCTRL_KDS_INIT                ----                       *
*  OSAL_C_S32_IOCTRL_KDS_INVALIDATE_ENTRY    tU16                       *
*  OSAL_C_S32_IOCTRL_KDS_CLEAR               ----                       *
*                                                                       *
* INPUTS                                                                *
*       tS32 Ps32fun:              Function identificator               *
*       tS32 Ps32arg:              Argument to be passed to function    *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error code                                                 *
* HISTORY                                                               *
*                                                                       *
************************************************************************/
tS32 KDS_s32IOControl(tS32 Ps32fun, intptr_t Ps32arg)
{
  tS32 Vs32RetVal   = OSAL_E_NOERROR;

  /*trace function start*/
  KDS_TRACE(KDS_START_FUNCTION_CONTROL,&Ps32fun,sizeof(tS32));
  /* lock and get memory*/
  tsKDSSharedMemory* VsShMemKDS=psKDSLock();
  if(VsShMemKDS==NULL)
  {
    Vs32RetVal=OSAL_C_S32_IOCTRL_KDS_NOT_FOUND;  
  }
  else
  {
    switch(Ps32fun)
    {
    case OSAL_C_S32_IOCTRL_VERSION :
      if(Ps32arg == 0)
      {
        Vs32RetVal = OSAL_C_S32_IOCTRL_KDS_PARAM_ERROR;
      }
      else
      {
        *(tPS32)Ps32arg = KDS_C_S32_IO_VERSION;
      }
      break;
    case OSAL_C_S32_IOCTRL_KDS_WRITE_ENABLE :
      if(Ps32arg == TRUE)
      {
        VsShMemKDS->vbWriteEnable = TRUE;
      }
      else if(Ps32arg == FALSE)
      {
        VsShMemKDS->vbWriteEnable = FALSE;
      }
      else
      {
        Vs32RetVal = OSAL_ERROR;
      }
      break;
    case OSAL_C_S32_IOCTRL_KDS_GET_NEXT_ID:
      if(Ps32arg == 0)
      {
        Vs32RetVal = OSAL_C_S32_IOCTRL_KDS_PARAM_ERROR;
      }
      else
      {/*This function can be used to read the KDS sequencial. If you want to read
          all entries, you may not know, which Entries are stored. So you can
          request the Entry-Id following (have the next bigger Id-Value) to the last readed Id*/

        tU16              VidKDS = (tU16) (*(tU16*)Ps32arg);
        const tsKDSEntry *VtsFoundEntry=psKDSGetNextEntry(VsShMemKDS,VidKDS);
        if(VtsFoundEntry)
        {
          *(tU16 *)Ps32arg = VtsFoundEntry->u16Entry;
        }
        else
        {
          *(tU16 *)Ps32arg = 0xffff;
          Vs32RetVal = OSAL_C_S32_IOCTRL_KDS_NOT_IN_LIST;              
        }
      }
      break;
    case OSAL_C_S32_IOCTRL_KDS_GET_PREV_ID:
      Vs32RetVal = ((tS32)OSAL_E_NOTSUPPORTED);
      break;
    case OSAL_C_S32_IOCTRL_KDS_CHECK:
      if(Ps32arg == 0)
      {
        Vs32RetVal = OSAL_C_S32_IOCTRL_KDS_PARAM_ERROR;
      }
      else
      {
        tsKDS_Info *info;
        info = (tsKDS_Info *)Ps32arg;
        info->s32EntryError = 0;
        info->u16IdWithError = 0;
        info->u32KDS_Size = KDS_C_MAX_ENTRIES * KDS_MAX_ENTRY_LENGTH;
        info->u32KDS_UsedSize = ((tU32)VsShMemKDS->u32KDSNumberOfEntries) * KDS_MAX_ENTRY_LENGTH;
        info->u32NumberOfActiveEntries = (tU32)VsShMemKDS->u32KDSNumberOfEntries;
        info->u32NumberOfEntries = KDS_C_MAX_ENTRIES;
        info->u16StateReadFlashData=VsShMemKDS->u16StateReadFlashData;
      }
      break;
    case OSAL_C_S32_IOCTRL_KDS_REMAIN:
      if(Ps32arg == 0)
      {
        Vs32RetVal = OSAL_C_S32_IOCTRL_KDS_PARAM_ERROR;
      }
      else
      {
        tU32* Vpu32RemainSize;
        tU32  Vu32RemainSize;

        Vpu32RemainSize = (tU32*)Ps32arg;
        Vu32RemainSize = (KDS_C_MAX_ENTRIES - VsShMemKDS->u32KDSNumberOfEntries) * sizeof(tsKDSEntry); 
        *Vpu32RemainSize = Vu32RemainSize;
      }
      break;
    case OSAL_C_S32_IOCTRL_KDS_INIT:
      if(!VsShMemKDS->vbWriteEnable)
      {
        Vs32RetVal = ((tS32)OSAL_E_NOPERMISSION);
      }
      break;
    case OSAL_C_S32_IOCTRL_KDS_INVALIDATE_ENTRY:
      if(VsShMemKDS->vbWriteEnable)
      {
        Vs32RetVal=s32KDSRemoveEntry(VsShMemKDS,(tU16)Ps32arg);       
      }
      else
      {
        Vs32RetVal = ((tS32)OSAL_E_NOPERMISSION);
      }
      break;
    case OSAL_C_S32_IOCTRL_KDS_CLEAR:
      if(VsShMemKDS->vbWriteEnable)
      {
        vKDSClearData(VsShMemKDS);
	      /*write data to flash*/
        s32KDSWriteData(VsShMemKDS);
      }
      else
      {
        Vs32RetVal = ((tS32)OSAL_E_NOPERMISSION);
      }
      break;
    case OSAL_C_S32_IOCTRL_KDS_WRITE_BACK:
	   /*write data to flash*/
       Vs32RetVal=s32KDSWriteData(VsShMemKDS);
      break;
    default:
      Vs32RetVal = OSAL_E_WRONGFUNC;
      break;
    }
    vKDSUnLock(VsShMemKDS);
  }
  /* trace exit function*/
  KDS_TRACE(KDS_EXIT_FUNCTION_CONTROL,&Vs32RetVal,sizeof(tS32));
  return Vs32RetVal;
}
/************************************************************************
* FUNCTIONS                                                             *
*      KDS_s32IOWrite                                                   *
*                                                                       *
* DESCRIPTION                                                           *
*    write a entry data to RAM buffer                                   *
*                                                                       *
* INPUTS                                                                *
*       tPCS32 Pps32Buffer:      Pointer to data buffer                 *
*       tU32   Pu32nbytes:       Number of bytes in buffer              *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error code                                                 *
*                                                                       *
************************************************************************/
tS32 KDS_s32IOWrite(tPCS8 Pps8Buffer, tU32 Pu32nbytes)
{
  tsKDSEntry*         VpsKDSEntry;
  const tsKDSEntry*   VpsKDSoldEntry;
  tU32                Vu32KDSEntryNumber;
  tS32                Vs32Result=OSAL_E_NOERROR;

  /*trace function start*/
  KDS_TRACE(KDS_START_FUNCTION_WRITE,0,0);
  tsKDSSharedMemory* VsShMemKDS=psKDSLock();
  if(VsShMemKDS==NULL)
  {
    Vs32Result=OSAL_C_S32_IOCTRL_KDS_NOT_FOUND;  
  }
  else
  {
    VpsKDSEntry = (tsKDSEntry*)Pps8Buffer;  //lint !e826
    /*trace out entry */
    KDS_TRACE(KDS_ENTRY,VpsKDSEntry,(((tU8)sizeof(tsKDSEntry)-KDS_MAX_ENTRY_LENGTH))+(tU8)VpsKDSEntry->u16EntryLength);
    /*check parameter*/
    if((Pps8Buffer == NULL) || (VpsKDSEntry->u16EntryLength >= KDS_MAX_ENTRY_LENGTH)
       || (VpsKDSEntry->u16Entry == 0xffff) 
       || (VpsKDSEntry->u16EntryLength > Pu32nbytes))
    {
      Vs32Result = OSAL_C_S32_IOCTRL_KDS_PARAM_ERROR;
	  KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));
    }
    else
    { /* check if write enable*/
      if(VsShMemKDS->vbWriteEnable)
      { /*get old entry*/
        Vu32KDSEntryNumber = u32KDSGetEntry(VsShMemKDS,VpsKDSEntry->u16Entry);      
        /* check KDS full*/
        if(VsShMemKDS->u32KDSNumberOfEntries == KDS_C_MAX_ENTRIES)
        {/* check if the entry exists */
          if(Vu32KDSEntryNumber >= VsShMemKDS->u32KDSNumberOfEntries)
          {/* if not, no new one can be created */
            Vs32Result = OSAL_C_S32_IOCTRL_KDS_FULL;
		    KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));
          }
        }
        /* error ?*/
        if(Vs32Result == (tS32)OSAL_E_NOERROR)
        {/* check if old entry exists and write protected*/
          if(Vu32KDSEntryNumber < VsShMemKDS->u32KDSNumberOfEntries)
          { /*get old entry */
            VpsKDSoldEntry=&VsShMemKDS->sEntry[Vu32KDSEntryNumber];
            /*check if it is write protected*/
            if(VpsKDSoldEntry->u16EntryFlags & M_KDS_ENTRY_FLAG_WRITE_PROTECTED)
            {
              Vs32Result = ((tS32)OSAL_C_S32_IOCTRL_KDS_WRITE_PROTECTED);
			        KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));
            }
          }
          /* error ?*/
          if(Vs32Result == (tS32)OSAL_E_NOERROR)
          {
            vKDSAddEntry(VsShMemKDS,VpsKDSEntry,Vu32KDSEntryNumber);
          }
        }
      }
      else
      {
        Vs32Result = ((tS32)OSAL_E_NOPERMISSION);
	    KDS_TRACE(KDS_ERROR,&Vs32Result,sizeof(tS32));
      }
    }
    vKDSUnLock(VsShMemKDS); 
    if(Vs32Result==(tS32)OSAL_E_NOERROR)
    {
      Vs32Result=(tS32)Pu32nbytes;
    }
  }
  /* trace exit function*/
  KDS_TRACE(KDS_EXIT_FUNCTION_WRITE,&Vs32Result,sizeof(tS32));
  return(Vs32Result);
}
/************************************************************************
* FUNCTIONS                                                             *
*      KDS_s32IORead                                                    *
*                                                                       *
* DESCRIPTION                                                           *
*    read a entry data from RAM buffer                                  *
*                                                                       *
* INPUTS                                                                *
*       tPCS32 Pps8Buffer:      Pointer to data buffer                  *
*       tU32   Pu32nbytes:      Number of bytes in buffer               *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error code                                                 *
************************************************************************/
tS32 KDS_s32IORead(tPCS8 Pps8Buffer,tU32 Pu32nbytes)
{
  tsKDSEntry*       VpsKDSEntry;
  tU32              Vu32KDSEntryNumber;
  const tsKDSEntry* VpsKDSEntryFromList;
  tS32              Vs32Result =OSAL_E_NOERROR;

  /*trace function start*/
  KDS_TRACE(KDS_START_FUNCTION_READ,0,0);
  /*lock shared memory*/
  tsKDSSharedMemory* VsShMemKDS=psKDSLock();
  if(VsShMemKDS==NULL)
  {
    Vs32Result=OSAL_C_S32_IOCTRL_KDS_NOT_FOUND;  
  }
  else
  { /*check parameter*/
    if((Pps8Buffer == NULL) || (Pu32nbytes == 0))
    {
      Vs32Result = OSAL_C_S32_IOCTRL_KDS_PARAM_ERROR;
    }
    else
    {
      VpsKDSEntry = (tsKDSEntry*)Pps8Buffer;  //lint !e826
      Vu32KDSEntryNumber=u32KDSGetEntry(VsShMemKDS,VpsKDSEntry->u16Entry);
      if(Vu32KDSEntryNumber < VsShMemKDS->u32KDSNumberOfEntries)
      {
        VpsKDSEntryFromList=&VsShMemKDS->sEntry[Vu32KDSEntryNumber];
        *VpsKDSEntry = *VpsKDSEntryFromList;
      }
      else
      {
        Vs32Result = OSAL_C_S32_IOCTRL_KDS_NOT_IN_LIST;
        VpsKDSEntry->u16Entry = 0xffff;
      }
    }
    vKDSUnLock(VsShMemKDS);
    if(Vs32Result==(tS32)OSAL_E_NOERROR)
    {
      Vs32Result=(tS32)Pu32nbytes;
    }
  }
  /* trace exit function*/
  KDS_TRACE(KDS_EXIT_FUNCTION_READ,&Vs32Result,sizeof(tS32));
  return(Vs32Result);
}
#ifndef KDS_TESTMANGER_ACTIVE
/* **************************************************FunctionHeaderBegin** *//**
*
*  static  tS32    s32KDSShmInit(tBool VbFirstAttach)
*   
*  This function init the the shared memory pointer vpKDSSharedMemory 
*  For first attach the shared memory should be create.
*  For all processes the map function should be called
*
* @param   tBool VbFirstAttach: check for first attach 
*
* @return  tS32: Error Code                  
*
* @date    2010-02-02
*
* @note      
*
*//* ***********************************************FunctionHeaderEnd******* */
static  tS32    s32KDSShmInit(tBool VbFirstAttach)
{
  tS32    Vs32ReturnCode=OSAL_E_NOERROR;
  if(VbFirstAttach)
  { /* create shared memory at first process attach */
    vs32KDSShmId = shm_open(KDS_NAME_SHARED_MEMORY, O_EXCL|O_RDWR|O_CREAT|O_TRUNC, PDD_ACCESS_RIGTHS );
	  if(ftruncate(vs32KDSShmId, sizeof(tsKDSSharedMemory)) == -1)
    {
      vs32KDSShmId=-1;	  
    }    
	  #ifndef KDS_TESTMANGER_ACTIVE 
    Vs32ReturnCode=s32KDSChangeGroupAccess(KDS_KIND_RES_SHM,vs32KDSShmId,"vs32KDSShmId");
    #endif
  }
  else
  { /* open shared memory*/
    vs32KDSShmId = shm_open(KDS_NAME_SHARED_MEMORY, O_RDWR ,0);    
  }
  if(vs32KDSShmId!=-1)
  { /* map global shared memory into address space */
    vpKDSSharedMemory = (tsKDSSharedMemory*)mmap( 0,sizeof(tsKDSSharedMemory),PROT_READ | PROT_WRITE,MAP_SHARED,vs32KDSShmId,0);
    if (vpKDSSharedMemory == MAP_FAILED)
    {
	     KDS_TRACE(KDS_ERROR_MMAP,"mmap",strlen("mmap")+1);
       KDS_FATAL_ASSERT();
    }	
  }
  else
  {
    Vs32ReturnCode=OSAL_E_UNKNOWN;
  }
  return(Vs32ReturnCode);
}
#endif
/* **************************************************FunctionHeaderBegin** *//**
*
*  static  tS32    s32KDSIsInit(tsKDSSharedMemory* PtsSharedMemory)
*   
*  This function checks if initialisiation for trace and shared memory done 
*
* @param   PtsSharedMemory: shared memory 
*
* @return  tS32: Error Code                  
*
* @date    2010-02-02
*
* @note      
*
*//* ***********************************************FunctionHeaderEnd******* */
static  tS32    s32KDSIsInit(tsKDSSharedMemory* PtsSharedMemory)
{
  tS32    Vs32Result=OSAL_E_NOERROR;  
  /* reload from flash */
  if(PtsSharedMemory->vbValidSharedMemory==FALSE)
  {	 
    Vs32Result=s32KDSReadData(PtsSharedMemory);
	if(Vs32Result==(tS32)OSAL_E_NOERROR)
	{
	  PtsSharedMemory->vbValidSharedMemory=TRUE;	
	}
  }
  return(Vs32Result);
}
/***********************************************************************
 * static       tsKDSSharedMemory*        psKDSLock(void)
 *    	
 * This function gives a pointer to the start of the shared memory and 
 * locks the memory with the internal mutex. 
 *
 * @return  
 *    tsKDSSharedMemory*  pointer of shared memory
 *
 * @date    2010-08-09
 *
 * @note
 *
 **********************************************************************/
static tsKDSSharedMemory*  psKDSLock(void)
{
  tsKDSSharedMemory*    VtspShMem=NULL;

  if (vpKDSSharedMemory!=0)
  {
    VtspShMem=vpKDSSharedMemory;
    #ifndef KDS_TESTMANGER_ACTIVE
	tS32  Vs32Value;
    /* get value semaphore*/
    if(sem_getvalue(&VtspShMem->tSemLockAccess,&Vs32Value)==0)
    { /* trace out value*/
      KDS_TRACE(KDS_SEM_VALUE,&Vs32Value,sizeof(tS32));	
      /* lock semaphore*/
      if(sem_wait(&VtspShMem->tSemLockAccess)==0)  
	  { /* no error */
	    KDS_TRACE(KDS_SEM_WAIT,0,0);	
      }/*end if*/
	  else
  	  {
	    KDS_TRACE(KDS_ERROR_SEM,"wait",strlen("wait")+1);
	    KDS_FATAL_ASSERT();
	  }
    }
    else
    {
      KDS_TRACE(KDS_ERROR_SEM,"get value",strlen("get value")+1);
      KDS_FATAL_ASSERT();
    }
    #else
	if(VtspShMem->vbValidSharedMemory==FALSE)
	{
	  VtspShMem=NULL;
	}
    #endif
  }
  return(VtspShMem);
}
/***********************************************************************
 * static void vKDSUnLock(tsKDSSharedMemory* PtsShMemKDS)
 *    	
 * This function releases a shared memory pointer and unlocks the internal mutex.
 *
 * @parameter
 *     PtsShMemKDS  Pointer to shared memory 
 *   
 * @date    2010-08-09
 *
 * @note
 *
 **********************************************************************/
static void vKDSUnLock(tsKDSSharedMemory* PtsShMemKDS)
{
  if(PtsShMemKDS!=NULL)
  {
    #ifndef KDS_TESTMANGER_ACTIVE
    tS32   Vs32Value;
    sem_t* VpSemLock=&PtsShMemKDS->tSemLockAccess;
    /* get value semaphore*/
    if(sem_getvalue(VpSemLock,&Vs32Value)==0)
    { /* trace out value*/
      KDS_TRACE(KDS_SEM_VALUE,&Vs32Value,sizeof(tS32));
	  /* post semaphore*/
      if(sem_post(VpSemLock)==0)  
	  {
	    KDS_TRACE(KDS_SEM_POST,0,0);	
      }/*end if*/
	  else
	  {
	    KDS_TRACE(KDS_ERROR_SEM,"post",strlen("post")+1);
	    KDS_FATAL_ASSERT();
	  }
    }
    else
    {    
      KDS_TRACE(KDS_ERROR_SEM,"get value",strlen("get value")+1);
      KDS_FATAL_ASSERT();
    }
    #endif
  }
}

/***********************************************************************
 * static void vKDSClearData(tsKDSSharedMemory* PtsShMemKDS)
 *    	
 * clear all data, which saved in RAM buffer 
 *
 * @parameter
 *     PtsShMemKDS  Pointer to shared memory 
 *   
 * @date    2010-08-09
 *
 * @note
 *
 **********************************************************************/
static void vKDSClearData(tsKDSSharedMemory* PtsShMemKDS)
{
  /*set number of entry to zero*/
  PtsShMemKDS->u32KDSNumberOfEntries = 0;
  /* memset entries to zero */
  memset(PtsShMemKDS->sEntry,0x00,sizeof(PtsShMemKDS->sEntry));
}
/***********************************************************************
 * static const tsKDSEntry*    psKDSGetNextEntry(tsKDSSharedMemory* PtsShMemKDS,tU16 Pu16Entry)
 *    	
 * get next KDS entry
 * This function can be used to read the KDS sequencial. If you want to read
 * all entries, you may not know, which Entries are stored. So you can
 * request the Entry-Id following (have the next bigger Id-Value) to the last readed Id
 *
 * @parameter
 *     PtsShMemKDS  Pointer to shared memory 
 *     Pu16Entry    entry number 
 *   
 * @return  
 *    tsKDSEntry*  pointer of next entry
 *
 * @date    2010-08-09
 *
 * @note
 *
 **********************************************************************/
static const tsKDSEntry*    psKDSGetNextEntry(tsKDSSharedMemory* PtsShMemKDS,tU16 Pu16Entry)
{
  tU32          Vu32EntryNumber;
  tU32          Vu32EntryNumberNextEntry=0;
  tU32          Vu32EntryNext=0xffff;
  tsKDSEntry*   VpsEntry;

  /* for all entries  */
  for(Vu32EntryNumber=0;Vu32EntryNumber<PtsShMemKDS->u32KDSNumberOfEntries;Vu32EntryNumber++)
  { /* get entry */
    VpsEntry=&PtsShMemKDS->sEntry[Vu32EntryNumber];
    /* If the Entry have an Id, bigger to Pu16Entry */
    if(VpsEntry->u16Entry > Pu16Entry)
    { /* check if the founf entry is smaller then last found entry */
      if(VpsEntry->u16Entry < Vu32EntryNext)
      { /* save the smaller entry*/
        Vu32EntryNext=VpsEntry->u16Entry;
        Vu32EntryNumberNextEntry=Vu32EntryNumber;
      }
    }
  }/*end for*/
  /*an next entry found ? */
  if(Vu32EntryNext!=0xffff)
  { /*next entry found*/
    VpsEntry=&PtsShMemKDS->sEntry[Vu32EntryNumberNextEntry];
  }
  else
  {/*no next entry found; exist given entry?*/    
    Vu32EntryNumber=u32KDSGetEntry(PtsShMemKDS,Pu16Entry);
    if(Vu32EntryNumber==PtsShMemKDS->u32KDSNumberOfEntries)
    { /*given entry not in list*/
      VpsEntry=NULL;
    }
    else
    { /*set given entry*/
      VpsEntry=&PtsShMemKDS->sEntry[Vu32EntryNumber];
    }
  }
  return(VpsEntry);
}
/***********************************************************************
 * static tU32 u32KDSGetEntry(tsKDSSharedMemory* PtsShMemKDS,tU16 Pu16Entry)
 *    	
 * get entry of an entry number
 *   
 * @parameter
 *     PtsShMemKDS  Pointer to shared memory 
 *     Pu16Entry   entry number     
 * 
 * @return   
 *    entry number
 *
 * @date    2010-08-09
 *
 * @note
 *
 **********************************************************************/
static tU32 u32KDSGetEntry(tsKDSSharedMemory* PtsShMemKDS,tU16 Pu16Entry)
{
  tU32          Vu32EntryNumber;
  tsKDSEntry*   VpsEntry;

  /* for all entries  */
  for(Vu32EntryNumber=0;Vu32EntryNumber<PtsShMemKDS->u32KDSNumberOfEntries;Vu32EntryNumber++)
  {  /* get entry */
    VpsEntry=&PtsShMemKDS->sEntry[Vu32EntryNumber];
    if(Pu16Entry==VpsEntry->u16Entry)
    {
      break;
    }
  }
  return(Vu32EntryNumber);
}

/***********************************************************************
 * static  tS32    s32KDSRemoveEntry(tsKDSSharedMemory* PtsShMemKDS,tU16 Pu16Entry)
 *    	
 * remove entry
 *
 * @parameter
 *     PtsShMemKDS  Pointer to shared memory 
 *     Pu16Entry   entry number   
 *   
 * @return  
 *     tS32: Error code      
 *
 * @date    2010-08-09
 *
 * @note
 *
 **********************************************************************/
static  tS32    s32KDSRemoveEntry(tsKDSSharedMemory* PtsShMemKDS,tU16 Pu16Entry)
{
  tS32          Vs32Result =OSAL_E_NOERROR;
  tU32          Vu32KDSEntryNumber;
  tsKDSEntry*   VpsEntry;

  /*get entry number*/
  Vu32KDSEntryNumber = u32KDSGetEntry(PtsShMemKDS,Pu16Entry);
  /* exist entry?*/
  if(Vu32KDSEntryNumber >= PtsShMemKDS->u32KDSNumberOfEntries)
  {
    Vs32Result = OSAL_C_S32_IOCTRL_KDS_NOT_IN_LIST;
  }
  else
  { /*get entry */
    VpsEntry=&PtsShMemKDS->sEntry[Vu32KDSEntryNumber];
    /* check write protected*/
    if(0 != (VpsEntry->u16EntryFlags & M_KDS_ENTRY_FLAG_WRITE_PROTECTED))
    {
      Vs32Result = ((tS32)OSAL_C_S32_IOCTRL_KDS_WRITE_PROTECTED);
    }
    else
    { /* delete entry: copy all entrys a entry forwards */
      tU32  Vu32KDSEntryNumberRemain=PtsShMemKDS->u32KDSNumberOfEntries-Vu32KDSEntryNumber;
      if(Vu32KDSEntryNumberRemain!=0)
      {
        memmove(VpsEntry,&PtsShMemKDS->sEntry[Vu32KDSEntryNumber+1],Vu32KDSEntryNumberRemain*sizeof(tsKDSEntry));
      }
      /*delete last entry*/
      memset(&PtsShMemKDS->sEntry[PtsShMemKDS->u32KDSNumberOfEntries],0x00,sizeof(tsKDSEntry));
      /*increment number of entries*/
      --PtsShMemKDS->u32KDSNumberOfEntries;     
    }
  }
  return(Vs32Result);
}

/***********************************************************************
 * static       void        vKDSAddEntry(const tsKDSEntry *PspKdsEntry,tU32 Pu32EntryNumber)
 *    	
 * add entry
 *   
 * @parameter
 *     PtsShMemKDS  Pointer to shared memory 
 *     PspKdsEntry  Pointer of add entry
 *     Pu16Entry   entry number
 *
 * @date    2010-08-09
 *
 * @note
 *
 **********************************************************************/
static       void        vKDSAddEntry(tsKDSSharedMemory* PtsShMemKDS,const tsKDSEntry *PspKdsEntry,tU32 Pu32EntryNumber)
{
  tsKDSEntry*   VpsEntry;

  if(Pu32EntryNumber>=PtsShMemKDS->u32KDSNumberOfEntries)
  {
    VpsEntry=&PtsShMemKDS->sEntry[PtsShMemKDS->u32KDSNumberOfEntries];
    memmove(VpsEntry,PspKdsEntry,sizeof(tsKDSEntry));
    ++PtsShMemKDS->u32KDSNumberOfEntries;
  }
  else
  {/* save actual entry data */
    VpsEntry=&PtsShMemKDS->sEntry[Pu32EntryNumber];
    memmove(VpsEntry,PspKdsEntry,sizeof(tsKDSEntry));
  }
}


/******************************************************************************/
/* End of File dev_kds.c                                                     */
/******************************************************************************/


