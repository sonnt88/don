/******************************************************************************
 *FILE         : osal_KDS_TestFuncs.h
 *
 *SW-COMPONENT : Regression Test framework 
 *
 *DESCRIPTION  : This file contains function decleration for the KDS device.
 *
 *AUTHOR       : Rohit C R
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 
 *
 *****************************************************************************/
#ifndef OEDT_KDS_TESTFUNCS_HEADER
#define OEDT_KDS_TESTFUNCS_HEADER

#include <persigtest.h>
#include <ResultOutput/traceprintf.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h" 
#define SYSTEM_S_IMPORT_INTERFACE_KDS_DEF
#include "system_pif.h"
//#include "oedt_helper_funcs.h"
#endif
