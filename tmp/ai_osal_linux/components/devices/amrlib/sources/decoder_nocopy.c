/*
 *===================================================================
 *  3GPP AMR Wideband Floating-point Speech Codec
 *===================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "typedef.h"
#include "dec_if.h"

#include <string.h>
#define AMRWB_MAGIC_NUMBER "#!AMR-WB\n"

#include "amr_if.h"

#if defined(VASCO_OS_WINCE) || defined(VASCO_OS_LINUX)
extern unsigned long OSAL_ClockGetElapsedTime();
#elif defined(VASCO_OS_WINNT)
#include <Windows.h>
#endif

/*
 * DECODER.C
 *
 * Main program of the AMR WB ACELP wideband decoder.
 *
 *    Usage : decoder bitstream_file synth_file
 *
 *    Format for bitstream_file:
 *        Described in TS26.201
 *
 *    Format for synth_file:
 *      Synthesis is written to a binary file of 16 bits data.
 *
 */

extern const UWord8 block_size[];

UWord32 iDecodeAMR(Word8* en_data, Word8* dec_data, UWord32 size)
{
#if defined(VASCO_OS_WINCE) || defined(VASCO_OS_LINUX)
   unsigned long clk_start, clk_end;
#elif defined(VASCO_OS_WINNT)
   BOOL have_perf;
   LARGE_INTEGER perf_freq;
   LARGE_INTEGER perf_cnt0;
   LARGE_INTEGER perf_cnt1;
#endif
   int idx_file = 0;
//   Word16 synth[L_FRAME16k];              /* Buffer for speech @ 16kHz             */
//   UWord8 serial[NB_SERIAL_MAX];
   Word16 mode;
//   Word32 frame;

   //const Word8* pSample;
   UWord32 offset;

   void *st;

#if defined(VASCO_OS_WINCE) || defined(VASCO_OS_LINUX)
   clk_start=OSAL_ClockGetElapsedTime();
#elif defined(VASCO_OS_WINNT)
   have_perf = QueryPerformanceFrequency(&perf_freq);
   if (have_perf) QueryPerformanceCounter(&perf_cnt0);
#endif

   /*
   * Initialization of decoder
   */
   st = D_IF_init();

   offset = strlen(AMRWB_MAGIC_NUMBER);
   if ((size <= offset) || strncmp(en_data, AMRWB_MAGIC_NUMBER, offset))
   {
      return 0;
   }
   //pSample = &en_data[offset];

   /*
   * Loop for each "L_FRAME" speech data
   */

//   frame = 0;
//   while (fread(serial, sizeof (UWord8), 1, f_serial ) > 0)
   while (offset < size)
   {
//      serial[0] = en_data[offset++];

//      mode = (Word16)((serial[0] >> 3) & 0x0F);
////      fread(&serial[1], sizeof (UWord8), block_size[mode] - 1, f_serial );
//      memcpy(&serial[1], &en_data[offset], (block_size[mode]-1)*sizeof(UWord8));
//      offset += (block_size[mode]-1)*sizeof(UWord8);

//      frame++;

      mode = (Word16)((en_data[offset] >> 3) & 0x0F);
      D_IF_decode( st, &en_data[offset], (Word16*)(&dec_data[idx_file]), _good_frame);
//      D_IF_decode( st, serial, synth, _good_frame);

//      memcpy(&dec_data[idx_file], synth, L_FRAME16k*sizeof(Word16));
      offset += block_size[mode]*sizeof(UWord8);
      idx_file += L_FRAME16k*sizeof(Word16);
//      fwrite(synth, sizeof(Word16), L_FRAME16k, f_synth);
//      fflush(f_synth);
   }

   D_IF_exit(st);

#if defined (VASCO_OS_WINCE) || defined(VASCO_OS_LINUX)
   clk_end=OSAL_ClockGetElapsedTime();
   {
      FILE* log=fopen("\\amrdec.txt", "a");
      if (log)
      {
         fprintf(log, "iDecodeAMR() took %ldms (decoded %d bytes into %d samples)\n",
            clk_end - clk_start, size, idx_file);
         fclose(log);
      }
   }
#elif defined(VASCO_OS_WINNT)
   if (have_perf) QueryPerformanceCounter(&perf_cnt1);
   if (have_perf)
   {
      FILE* log=fopen("D:\\Daten\\OSALBench\\amrdec.txt", "a");
      if (log)
      {
         double f = perf_freq.QuadPart;
         double t = perf_cnt1.QuadPart - perf_cnt0.QuadPart;

         fprintf(log, "iDecodeAMR() took %ldms (decoded %d bytes into %d samples)\n",
            (unsigned long)(1000*t/f), size, idx_file);
         fclose(log);
      }
   }
#endif
   return idx_file;
}

#if 0
int main(int argc, char *argv[])
{
    FILE *f_serial;                        /* File of serial bits for transmission  */
    FILE *f_synth;                         /* File of speech data                   */

    Word16 synth[L_FRAME16k];              /* Buffer for speech @ 16kHz             */
    UWord8 serial[NB_SERIAL_MAX];
    Word16 mode;
    Word32 frame;

#ifndef IF2
	char magic[16];
#endif
    void *st;


    fprintf(stderr, "\n");
	   fprintf(stderr, "===================================================================\n");
	   fprintf(stderr, " 3GPP AMR-WB Floating-point Speech Decoder, v6.0.0, Dec 14, 2004\n");
	   fprintf(stderr, "===================================================================\n");
   fprintf(stderr, "\n");

    /*
     * Read passed arguments and open in/out files
     */
    if (argc != 3)
    {
        fprintf(stderr, "Usage : decoder  bitstream_file  synth_file\n");
        fprintf(stderr, "\n");
        fprintf(stderr, "Format for bitstream_file:\n");
#ifdef IF2
		fprintf(stderr, "  Described in TS26.201.\n");
#else
		fprintf(stderr, "  Described in RFC 3267 (Sections 5.1 and 5.3).\n");
#endif
        fprintf(stderr, "\n");
        fprintf(stderr, "Format for synth_file:\n");
        fprintf(stderr, "  Synthesis is written to a binary file of 16 bits data.\n");
        fprintf(stderr, "\n");
        exit(0);
    }

    /* Open file for synthesis and packed serial stream */
    if ((f_serial = fopen(argv[1], "rb")) == NULL)
    {
        fprintf(stderr, "Input file '%s' does not exist !!\n", argv[1]);
        exit(0);
    }
    else
    {
        fprintf(stderr, "Input bitstream file:   %s\n", argv[1]);
    }

    if ((f_synth = fopen(argv[2], "wb")) == NULL)
    {
        fprintf(stderr, "Cannot open file '%s' !!\n", argv[2]);
        exit(0);
    }
    else
    {
        fprintf(stderr, "Synthesis speech file:   %s\n", argv[2]);
    }

    /*
     * Initialization of decoder
     */
    st = D_IF_init();

#ifndef IF2
   /* read magic number */
   fread(magic, sizeof(char), strlen(AMRWB_MAGIC_NUMBER), f_serial);

   /* verify magic number */
   if (strncmp(magic, AMRWB_MAGIC_NUMBER, strlen(AMRWB_MAGIC_NUMBER)))
   {
	   fprintf(stderr, "%s%s\n", "Invalid magic number: ", magic);
	   fclose(f_serial);
	   fclose(f_synth);
	   exit(0);
   }
#endif

    /*
     * Loop for each "L_FRAME" speech data
     */
    fprintf(stderr, "\n --- Running ---\n");

    frame = 0;
    while (fread(serial, sizeof (UWord8), 1, f_serial ) > 0)
    {
#ifdef IF2
       mode = (Word16)(serial[0] >> 4);
#else
	   mode = (Word16)((serial[0] >> 3) & 0x0F);
#endif
	   fread(&serial[1], sizeof (UWord8), block_size[mode] - 1, f_serial );

	   frame++;

	   fprintf(stderr, " Decoding frame: %ld\r", frame);

	   D_IF_decode( st, serial, synth, _good_frame);

	   fwrite(synth, sizeof(Word16), L_FRAME16k, f_synth);
	   fflush(f_synth);
    }

    D_IF_exit(st);

    fclose(f_serial);
    fclose(f_synth);

    return 0;
}
#endif
