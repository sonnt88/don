/*
 *===================================================================
 *  3GPP AMR Wideband Floating-point Speech Codec
 *===================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "typedef.h"
#include "dec_if.h"

#include <string.h>
#define AMRWB_MAGIC_NUMBER "#!AMR-WB\n"

#include "amr_if.h"

/*
 * DECODER.C
 *
 * Main program of the AMR WB ACELP wideband decoder.
 *
 *    Usage : decoder bitstream_file synth_file
 *
 *    Format for bitstream_file:
 *        Described in TS26.201
 *
 *    Format for synth_file:
 *      Synthesis is written to a binary file of 16 bits data.
 *
 */

extern const UWord8 block_size[];

UWord32 iDecodeAMR(Word8* en_data, Word8* dec_data, UWord32 size)
{
   int idx_file = 0;
   Word16 synth[L_FRAME16k];              /* Buffer for speech @ 16kHz             */
   UWord8 serial[NB_SERIAL_MAX];
   Word16 mode;

   UWord32 offset;

   void *st;

   /*
   * Initialization of decoder
   */
   st = D_IF_init();

   offset = strlen(AMRWB_MAGIC_NUMBER);
   if ((size <= offset) || strncmp(en_data, AMRWB_MAGIC_NUMBER, offset))
   {
      return 0;
   }

   /*
   * Loop for each "L_FRAME" speech data
   */

   while (offset < size)
   {
      serial[0] = en_data[offset++];

      mode = (Word16)((serial[0] >> 3) & 0x0F);
      memcpy(&serial[1], &en_data[offset], (block_size[mode]-1)*sizeof(UWord8));
      offset += (block_size[mode]-1)*sizeof(UWord8);

      D_IF_decode( st, serial, synth, _good_frame);

      memcpy(&dec_data[idx_file], synth, L_FRAME16k*sizeof(Word16));
      idx_file += L_FRAME16k*sizeof(Word16);
   }

   D_IF_exit(st);

   return idx_file;
}
