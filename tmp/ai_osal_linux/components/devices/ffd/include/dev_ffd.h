/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_ffd.h
 *
 *  header for the FFD driver. The dev_ffd driver 
 *  is a an osal driver. 
 *
 * @date        2010-11-25
 *
 * @note
 *
 *  &copy; Copyright TMS GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef DEV_FFD_H
#define DEV_FFD_H

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
tS32 DEV_FFD_s32IODeviceInit(tVoid);
tS32 DEV_FFD_s32IODeviceRemove(tVoid);
tS32 DEV_FFD_IOOpen(OSAL_tenAccess PenAccess);
tS32 DEV_FFD_s32IOClose(void);
tS32 DEV_FFD_s32IORead(tU32 Pu32Id,tPS8 PpBuffer, tU32 Pu32Size, tU32 *PpRetSize);
tS32 DEV_FFD_s32IOWrite(tU32 Pu32Id,tPS8 PpBuffer, tU32 Pu32Size, tU32 *PpRetSize);
tS32 DEV_FFD_s32IOControl(tU32 Pu32Id,tS32 Ps32fun,tS32 Ps32arg);

#else
#error dev_ffd.h included several times
#endif
