/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_ffd_privat.h
 *
 *  private header for the FFD driver. 
 *
 * @date        2010-11-18
 *
 * @note
 *
 *  &copy; Copyright TMS GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef DEV_FFD_PRIVATE_H
#define DEV_FFD_PRIVATE_H

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
/* size of header*/
#define M_FFD_SIZE_HEADER(NumberEntry)    ((tU32)sizeof(tsFFDHeaderVersion)+(((tU32) sizeof(tsFFDHeaderEntry)) * ((tU32)NumberEntry)))
/* size of data */
#define FFD_SIZE_DATA_MAX                       FLASHBLOCK_CLUSTER_SIZE_FFD

/*magic number for flash */
#define FFD_FLASH_MAGIC_NR                      0x5a6b7c8d
/* default value for the mask, which block are write*/
#define FFD_FLASH_MASK_SAVED_DATA_SETS          0

/*define lenght name */
#define FFD_FLASH_NAME_LENGTH                   16

/*define max number of data sets*/
#define FFD_DATA_SET_MAX_ENTRY                  32

/*define file*/
//#define FFD_FILE_NAME_BACKUP_FILE   "/var/opt/bosch/dynamic/ffs/ffd_data.bin"
#define FFD_FILE_NAME_BACKUP_FILE      "/var/opt/bosch/persistent/ffd_data.bin"

/*for access rights */
#define FFD_ACCESS_RIGTHS                 S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP  //0660
#define FFD_ACCESS_RIGTHS_GROUP_NAME      "eco_osal"

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef enum 
{
  FFD_KIND_RES_SEM,
  FFD_KIND_RES_SHM,
  FFD_KIND_RES_FILE,
}teFFDKindRes;
/* structure for check valid entrys for each project*/
typedef struct
{
  unsigned char bValid;  
  unsigned char ubPos;
}tsFFDEntryValid;

/*structure for the header used locally for version information*/
typedef struct
{
  tU32                    u32StoredVersion;
  tU32                    u32Magic; 
  tU32                    u32MaskSavedDataSets;
}tsFFDHeaderVersion;

/* structure for file entry*/
typedef struct
{
  tU8                   u8Filename[FFD_FLASH_NAME_LENGTH];  
  tU32                  u32Offset;
  tU32                  u32Size;
  tU32                  u32Version;
}tsFFDHeaderEntry;

/*structure for data */
typedef struct
{
  tsFFDHeaderVersion     rHeaderVersion;
  tsFFDHeaderEntry       raEntry[FFD_DATA_SET_MAX_ENTRYS]; 
}tsFFDHeader;

/*structure for data */
typedef union /*struct*/
{
  tsFFDHeader            rHeader;
  tU8                    u8Data[FFD_SIZE_DATA_MAX]; 
}tuFFDData;

/*structure for shared memory */
typedef struct
{
  tU32                 u32FFDMaxSizeData;
  tU8                  u8NumberEntry;
  tsFFDEntryValid      sFFDValidEntry[FFD_DATA_SET_MAX_ENTRYS];
  tU16                 u16StateReadFlashData;                     //state of read data (normal or backup)
  tS32                 s32AttachedProcesses;                      //attach count
  tBool                vbFFDTraceRegState;                        //check if trace register ok   
  tBool                vbFFDValidSharedMemory;                    //check if shared memory OK 
  tBool                bReloadData;                               //check if valid data read from flash
  sem_t                tSemLockAccess;                            //semaphore for access to this shared memory
  tuFFDData            uActualData;                               //actuel configuration data (header+data)
}tsFFDSharedMemory;

/*config data*/
typedef struct
{
  tsFFDEntryValid*     pFFDValidEntry;  
  tsFFDHeader*         pFFDDefaultHeader;
  tU8                  u8FFDNumberEntry; 
}tsFFDConfig;
/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
tsFFDSharedMemory* psFFDLock(void);
void               vFFDUnLock(tsFFDSharedMemory* PtsShMemFFD);
tsFFDConfig*       psFFDGetConfigPointer(void);
tS32               s32FFDReloadDataFromFile(tsFFDSharedMemory* PsSharedMemory);
tS32               s32FFDSaveDataToFile(tsFFDSharedMemory* PsSharedMemory);
tS32               s32FFDChangeGroupAccess(teFFDKindRes,tS32 ,const char* );
#else
#error dev_ffd_private.h included several times
#endif






