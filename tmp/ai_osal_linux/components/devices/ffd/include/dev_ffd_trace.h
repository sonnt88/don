/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_ffd_trace.h
 *
 *  private header for the FFD driver trace information. 
 *
 * @date        2010-11-18
 *
 * @note
 *
 *  &copy; Copyright TMS GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef DEV_FFD_TRACE_H
#define DEV_FFD_TRACE_H

/*************************************************************************/
/* define                                                                */
/*************************************************************************/
/*command ttfis*/
#define FFD_ATTR_ERASE_FLASH_BLOCK_CMD                 0x01
#define FFD_ATTR_GET_ACTUAL_DATA_MIRROR_ALL            0x02
#define FFD_ATTR_GET_ACTUAL_DATA_MIRROR_SET            0x03
#define FFD_ATTR_GET_ACTUAL_DATA_MIRROR_ALL_STRUCTURE  0x04
#define FFD_ATTR_GET_FLASH_DATA_VERSION_CMD            0x05
#define FFD_ATTR_GET_MASK_FOR_SAVED_DATASETS           0x06
#define FFD_ATTR_SHUTDOWN                              0x07
#define FFD_ATTR_GET_SIZE                              0x08
#define FFD_ATTR_CHANGE_VERSION_NUMBER                 0x09
#define FFD_ATTR_BACKUP_FILE_SAVE                      0x0a
#define FFD_ATTR_BACKUP_FILE_LOAD                      0x0b
#define FFD_ATTR_BACKUP_FILE_OUTPUT                    0x0c

/************************** kind traces *********************************/
/* important*/
#define FFD_FILE_ERASE_FLASH_BLOCK_START      0x01
#define FFD_FILE_ERASE_FLASH_BLOCK_END        0x02
#define FFD_FLASH_DATA_VERSION_TRACE          0x03
#define FFD_ACTUAL_DATA_MIRROR_ALL            0x04
#define FFD_ACTUAL_DATA_MIRROR_SET            0x05
#define FFD_GET_MASK_DATASET                  0x06
#define FFD_ACTUAL_DATA_SET_NAME              0x07
#define FFD_GET_DATA_SIZE                     0x08
#define FFD_GET_MAGIC                         0x09
#define FFD_ACTUAL_DATA_SET_OFFSET            0x0a
#define FFD_ACTUAL_DATA_SET_SIZE              0x0b
#define FFD_GET_HEADER_SIZE                   0x0c
#define FFD_GET_SIZE                          0x0d
#define FFD_READ_ERROR                        0x10
#define FFD_CMD_NOT_SUPPORTED_FOR_LINUX       0x11 
#define FFD_BACKUP_FILE_SAVE_DONE             0x12 
#define FFD_BACKUP_FILE_SAVE_ERROR            0x13 
#define FFD_BACKUP_FILE_LOAD_DONE             0x14 
#define FFD_BACKUP_FILE_LOAD_ERROR            0x15 
#define FFD_BACKUP_FILE_OUTPUT_ERROR          0x16 
#define FFD_VERSION_DIFFERENT                 0x17
#define FFD_BOARD_NAME                        0x18
#define FFD_ERROR_SEM_WAIT                    0x19
#define FFD_ERROR_SEM_GET_VALUE               0x1a
#define FFD_ERROR_SEM_POST                    0x1b
#define FFD_ERROR_SEM_CREATE                  0x1c
#define FFD_ERROR_MMAP                        0x1d
#define FFD_LEVEL_IMPORTANT                   0x1f
/* other*/
#define FFD_START_FUNCTION_CREATE             0x20
#define FFD_START_FUNCTION_DESTROY            0x21
#define FFD_START_FUNCTION_OPEN               0x22
#define FFD_START_FUNCTION_CLOSE              0x23
#define FFD_START_FUNCTION_WRITE              0x24
#define FFD_START_FUNCTION_READ               0x25
#define FFD_START_FUNCTION_CONTROL            0x26  
#define FFD_START_FUNCTION_RELOAD_FILE        0x27
#define FFD_START_FUNCTION_SAVE_FILE          0x28
#define FFD_RETURN_FUNCTION                   0x30  
#define FFD_ERROR_TRACE_CHANNEL_REGISTER      0x31
#define FFD_ERROR_TRACE_CHANNEL_UNREGISTER    0x32
#define FFD_WRITE_DATA_SET                    0x33
#define FFD_READ_DATA_SET                     0x34
#define FFD_FILE_POS_BEGIN                    0x35
#define FFD_FILE_POS_END                      0x36
#define FFD_MASK_DATASET                      0x40
#define FFD_SAVE_FILE                         0x41
#define FFD_RELOAD_FILE                       0x42
#define FFD_FILE_INDENTICAL                   0x43
#define FFD_SEM_WAIT                          0x50
#define FFD_SEM_VALUE                         0x51
#define FFD_SEM_POST                          0x52
#define FFD_ATTACH_COUNT                      0x53

/**********************end kind traces **********************************/

#define FFD_TRACE(u8Kind,pData,u8Len) \
  FFD_vTrace(u8Kind,pData,u8Len)  
#define FFD_TRACE_MORE_INFO(u8Kind,Data1,Data2) \
  FFD_vTraceMoreInfo(u8Kind,Data1,Data2)  

/* for assert call function  */
#define FFD_FATAL_ASSERT()				FATAL_M_ASSERT(0);
#define FFD_NORMAL_ASSERT()             NORMAL_M_ASSERT(0);

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
void   FFD_vTraceCommand(const tU8* Pu8pData);
void   FFD_vTrace(tU8 Pu8Kind, const tU8* Pu8pData, tU8 Pu8Len);
void   FFD_vTraceMoreInfo(tU8 Pu8Kind,tU32 Pu32Data1,tU32 Pu32Data2);
#else
#error dev_ffd_trace.h included several times
#endif

