/**********************************************************FileHeaderBegin*****
 *
 * FILE:
 *     dev_ffd_file_config.c
 *
 * REVISION:
 *
 * AUTHOR:
 *     Andrea B�ter , Bosch Softec GmbH, Hildesheim
 *
 * CREATED:
 *     15.03.2011
 *
 * DESCRIPTION:
 *     This file contains the various configurations in dependency of different
 *     boards needed for FFD.
 * 
 * NOTES:
 *
 ***********************************************************FileHeaderEnd*****/
/************************************************************************ 
| includes 
|-----------------------------------------------------------------------*/
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"
#include "flashblocksize.h"
#include "dev_ffd_private.h"
#include "dev_ffd_trace.h"
/************************************************************************ 
|defines and macros 
|-----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*/
/* Update 1: if change entry incremet version number                    */
/*----------------------------------------------------------------------*/

/************************* GM *******************************************/
/* count this number up on _every change of aFFD_Data_Default! 
(Attention: first 4 bits indicate kind of flash!!)  */
#define FFD_FLASH_DATA_VERSION_NUMBER_GM      0x0017
#define FFD_FLASH_KIND_GM                     0x2000
#define FFD_FLASH_DATA_VERSION_GM             (FFD_FLASH_DATA_VERSION_NUMBER_GM|FFD_FLASH_KIND_GM)

/************************ VOLVO ******************************************/
/* count this number up on _every change of aFFD_Data_Default! 
(Attention: first 4 bits indicate kind of flash!!)  */
#define FFD_FLASH_DATA_VERSION_NUMBER_VOLVO   0x0002
#define FFD_FLASH_KIND_VOLVO                  0x4000
#define FFD_FLASH_DATA_VERSION_VOLVO         (FFD_FLASH_DATA_VERSION_NUMBER_VOLVO|FFD_FLASH_KIND_VOLVO)

/************************ LCN2_KAI ******************************************/
/* count this number up on _every change of aFFD_Data_Default! 
(Attention: first 4 bits indicate kind of flash!!)  */
#define FFD_FLASH_DATA_VERSION_NUMBER_LCN2_KAI   0x0001
#define FFD_FLASH_KIND_LCN2_KAI                  0x8000
#define FFD_FLASH_DATA_VERSION_LCN2_KAI         (FFD_FLASH_DATA_VERSION_NUMBER_LCN2_KAI|FFD_FLASH_KIND_LCN2_KAI)

/*----------------------------------------------------------------------*/
/* Update  1: END (version number)                                      */
/*----------------------------------------------------------------------*/

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/

/*----------------------------------------------------------------------*/
/* Update  2: set valid entry for each project                          */
/*----------------------------------------------------------------------*/
/* a new data set must be added in system_ffd_def.h */

/************************* GM *******************************************/
/*table for look up, which entrys for which project valid */
static tsFFDEntryValid vrFFDValidEntry_GM[FFD_DATA_SET_MAX_ENTRYS] =
{ /*                                          element, pos */
  {/*EN_FFD_DATA_DIR*/                      TRUE,0},
  {/*EN_FFD_DATA_SET_EOL_SYSTEM*/           TRUE,1}, 
  {/*EN_FFD_DATA_SET_EOL_DISPLAY*/          TRUE,2}, 
  {/*EN_FFD_DATA_SET_EOL_BLUETOOTH*/        TRUE,3}, 
  {/*EN_FFD_DATA_SET_EOL_NAV_SYSTEM*/       TRUE,4}, 
  {/*EN_FFD_DATA_SET_EOL_NAV_ICON*/         TRUE,5}, 
  {/*EN_FFD_DATA_SET_EOL_BRAND*/            TRUE,6}, 
  {/*EN_FFD_DATA_SET_EOL_COUNTRY*/          TRUE,7}, 
  {/*EN_FFD_DATA_SET_EOL_SPEECH_REC*/       TRUE,8}, 
  {/*EN_FFD_DATA_SET_EOL_HF_TUNING*/        TRUE,9}, 
  {/*EN_FFD_DATA_SET_EOL_RVC*/              TRUE,10}, 
  {/*EN_FFD_DATA_SET_CSM*/                  TRUE,11}, 
  {/*EN_FFD_DATA_SET_SPM*/                  TRUE,12}, 
  {/*EN_FFD_DATA_SET_OEDT*/                 TRUE,13}, 
  {/*EN_FFD_DATA_SET_OSAL_ASSERT_MODE*/     TRUE,14}, 
  {/*EN_FFD_DATA_SET_TRACE_CFG*/            TRUE,15}, 
  {/*EN_FFD_DATA_SET_TRACE_STARTUP*/        TRUE,16},
  {/*EN_FFD_DATA_SET_EDC_STATE*/            TRUE,17},
  {/*EN_FFD_DATA_SET_LIN_MODE*/             TRUE,18},
  {/*EN_FFD_DATA_SET_LVDS_MODE*/            TRUE,19},
  {/*EN_FFD_DATA_SET_DOWNLOAD_DATE*/        TRUE,20},
  {/*EN_FFD_DATA_SET_VERSION*/              TRUE,21},
  {/*EN_FFD_DATA_SET_MOST*/                 TRUE,22},
  {/*EN_FFD_DATA_SET_MAX_MOST*/             TRUE,23},
  {/*EN_FFD_DATA_SET_RVC*/                  TRUE,24},
  {/*EN_FFD_DATA_SET_THM*/                  TRUE,25},
  {/*EN_FFD_DATA_SET_LAST*/                 TRUE,26},  
};
#define  FFD_DATA_SET_LAST_ENTRY_GM         27  /* the max number of data is 32 */

/*********************** VOLVO ******************************************/
/*table for look up, which entrys for which project valid */
static tsFFDEntryValid vrFFDValidEntry_VOLVO[FFD_DATA_SET_MAX_ENTRYS] =
{ /*                                       element, pos */
  {/*EN_FFD_DATA_DIR*/                      TRUE,0},
  {/*EN_FFD_DATA_SET_EOL_SYSTEM*/           FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_EOL_DISPLAY*/          FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_EOL_BLUETOOTH*/        FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_EOL_NAV_SYSTEM*/       FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_EOL_NAV_ICON*/         FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_EOL_BRAND*/            FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_EOL_COUNTRY*/          FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_EOL_SPEECH_REC*/       FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_EOL_HF_TUNING*/        FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_EOL_RVC*/              FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_CSM*/                  TRUE,1}, 
  {/*EN_FFD_DATA_SET_SPM*/                  TRUE,2}, 
  {/*EN_FFD_DATA_SET_OEDT*/                 TRUE,3}, 
  {/*EN_FFD_DATA_SET_OSAL_ASSERT_MODE*/     TRUE,4}, 
  {/*EN_FFD_DATA_SET_TRACE_CFG*/            TRUE,5}, 
  {/*EN_FFD_DATA_SET_TRACE_STARTUP*/        TRUE,6},
  {/*EN_FFD_DATA_SET_EDC_STATE*/            TRUE,7},
  {/*EN_FFD_DATA_SET_LIN_MODE*/             FALSE,0xff},
  {/*EN_FFD_DATA_SET_LVDS_MODE*/            TRUE,10},
  {/*EN_FFD_DATA_SET_DOWNLOAD_DATE*/        TRUE,8},
  {/*EN_FFD_DATA_SET_VERSION*/              TRUE,9},
  {/*EN_FFD_DATA_SET_MOST*/                 FALSE,0xff},
  {/*EN_FFD_DATA_SET_MAX_MOST*/             FALSE,0xff}, 
  {/*EN_FFD_DATA_SET_RVC*/                  FALSE,0xff},
  {/*EN_FFD_DATA_SET_THM*/                  FALSE,0xff},
  {/*EN_FFD_DATA_SET_LAST*/                 TRUE,11},  
};
#define  FFD_DATA_SET_LAST_ENTRY_VOLVO      12  /* the max number of data is 32 */

/************************* GM *******************************************/
/*table for look up, which entrys for which project valid */
static tsFFDEntryValid vrFFDValidEntry_LCN2_KAI[FFD_DATA_SET_MAX_ENTRYS] =
{ /*                                          element, pos */
  {/*EN_FFD_DATA_DIR*/                      TRUE,0},
  {/*EN_FFD_DATA_SET_EOL_SYSTEM*/           TRUE,1}, 
  {/*EN_FFD_DATA_SET_EOL_DISPLAY*/          TRUE,2}, 
  {/*EN_FFD_DATA_SET_EOL_BLUETOOTH*/        TRUE,3}, 
  {/*EN_FFD_DATA_SET_EOL_NAV_SYSTEM*/       TRUE,4}, 
  {/*EN_FFD_DATA_SET_EOL_NAV_ICON*/         TRUE,5}, 
  {/*EN_FFD_DATA_SET_EOL_BRAND*/            TRUE,6}, 
  {/*EN_FFD_DATA_SET_EOL_COUNTRY*/          TRUE,7}, 
  {/*EN_FFD_DATA_SET_EOL_SPEECH_REC*/       TRUE,8}, 
  {/*EN_FFD_DATA_SET_EOL_HF_TUNING*/        TRUE,9}, 
  {/*EN_FFD_DATA_SET_EOL_RVC*/              TRUE,10}, 
  {/*EN_FFD_DATA_SET_CSM*/                  TRUE,11}, 
  {/*EN_FFD_DATA_SET_SPM*/                  TRUE,12}, 
  {/*EN_FFD_DATA_SET_OEDT*/                 TRUE,13}, 
  {/*EN_FFD_DATA_SET_OSAL_ASSERT_MODE*/     TRUE,14}, 
  {/*EN_FFD_DATA_SET_TRACE_CFG*/            TRUE,15}, 
  {/*EN_FFD_DATA_SET_TRACE_STARTUP*/        TRUE,16},
  {/*EN_FFD_DATA_SET_EDC_STATE*/            TRUE,17},
  {/*EN_FFD_DATA_SET_LIN_MODE*/             TRUE,18},
  {/*EN_FFD_DATA_SET_LVDS_MODE*/            TRUE,19},
  {/*EN_FFD_DATA_SET_DOWNLOAD_DATE*/        TRUE,20},
  {/*EN_FFD_DATA_SET_VERSION*/              TRUE,21},
  {/*EN_FFD_DATA_SET_MOST*/                 TRUE,22},
  {/*EN_FFD_DATA_SET_MAX_MOST*/             TRUE,23},
  {/*EN_FFD_DATA_SET_RVC*/                  TRUE,24},
  {/*EN_FFD_DATA_SET_THM*/                  TRUE,25},
  {/*EN_FFD_DATA_SET_LAST*/                 TRUE,26},  
};
#define  FFD_DATA_SET_LAST_ENTRY_LCN2_KAI         27  /* the max number of data is 32 */


/*----------------------------------------------------------------------*/
/* Update  2: END   (valid entry)                                       */
/*----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*/
/* Update  3:    set default header for each project                    */
/*----------------------------------------------------------------------*/
/************************* GM *******************************************/
/*default data */
static tsFFDHeader         vrFFDDefaultHeader_GM = 
{
  {
    FFD_FLASH_DATA_VERSION_GM,             //    uwStoredVersion;
    FFD_FLASH_MAGIC_NR,                    //    uwMagic;
    FFD_FLASH_MASK_SAVED_DATA_SETS,        //    uwMaskSavedDataSets
  }, {//FFD_DATA_BASE_OFFSET  12 + (number_set*24)
    /* name ,           offset, number of bytes,      version */
    {"__DIR__",             0,                0,      0x0001},   
    {"EOL.System",          0,             1500,      0x0008},
    {"EOL.Display",      1500,              500,      0x0008},
    {"EOL.Bluetooth",    2000,              300,      0x0008},
    {"EOL.NavSystem",    2300,              500,      0x0008},
    {"EOL.NavIcon",      2800,              100,      0x0008},
    {"EOL.Brand",        2900,              600,      0x0008},
    {"EOL.Country",      3500,             2000,      0x0008},
    {"EOL.SpeechRec",    5500,             8210,      0x0008},
    {"EOL.HFTuning",    13710,             3000,      0x0008},
    {"EOL.RVC",         16710,              200,      0x0008},
    {"CSM",             16910,              128,      0x0001},
    {"SPM",             17038,              128,      0x0001},
    {"OEDT",            17166,              128,      0x0001},
    {"OSAL.AssertMode", 17294,                4,      0x0001},
    {"Trace.Config",    17298,              256,      0x0002},
    {"Startup.Param",   17554,              132,      0x0001},
    {"EDC.State",       17686,              192,      0x0001},
    {"LIN.Mode",        17878,                4,      0x0001},
    {"LVDS.Mode",       17882,                6,      0x0001},
    {"Download.Date",   17888,                4,      0x0001},
    {"Version.String",  17892,              400,      0x0001},
    {"MOST",            18292,             1024,      0x0001},
    {"Max.MOST",        19316,              256,      0x0001},
    {"RVC",             19572,                4,      0x0001},
    {"THM",             19576,               24,      0x0001},
    {"__END__",         19600,                0,      0x0001}
  }  
}; 

/*********************** VOLVO ******************************************/
/*default data */
static tsFFDHeader         vrFFDDefaultHeader_VOLVO = 
{
  {
    FFD_FLASH_DATA_VERSION_VOLVO,          //    uwStoredVersion;
    FFD_FLASH_MAGIC_NR,                    //    uwMagic;
    FFD_FLASH_MASK_SAVED_DATA_SETS,        //    uwMaskSavedDataSets
  }, {//FFD_DATA_BASE_OFFSET  12 + (number_set*24)
    /* name ,           offset, number of bytes,      version 0x1000:VOLVO*/
    {"__DIR__",             0,                0,      0x1001},   
    {"CSM",                 0,              128,      0x1001},
    {"SPM",               128,              128,      0x1001},
    {"OEDT",              256,              128,      0x1001},    
    {"OSAL.AssertMode",   384,                4,      0x1001},   
    {"Trace.Config",      388,               32,      0x1001},
    {"Startup.Param",     420,              132,      0x1001},
    {"EDC.State",         552,              192,      0x1001},
    {"Download.Date",     744,                4,      0x1001},
    {"Version.String",    748,              400,      0x1001}, 
    {"LVDS.Mode",        3236,                6,      0x1001},
    {"__END__",          3242,                0,      0x1001},
	{" ",                   0,                0,         0x0},/*for lint*/
    {" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
    {" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0},/*for lint*/
    {" ",                   0,                0,         0x0},/*for lint*/
	{" ",                   0,                0,         0x0}/*for lint*/
  }  
}; 

/*********************** LCN_KAI TODO clarify ******************************************/
/*default data */
static tsFFDHeader         vrFFDDefaultHeader_LCN2_KAI = 
{
  {
    FFD_FLASH_DATA_VERSION_LCN2_KAI,       //    uwStoredVersion;
    FFD_FLASH_MAGIC_NR,                    //    uwMagic;
    FFD_FLASH_MASK_SAVED_DATA_SETS,        //    uwMaskSavedDataSets
  }, {//FFD_DATA_BASE_OFFSET  12 + (number_set*24)
    /* name ,           offset, number of bytes,      version */
    {"__DIR__",         0,                    0,      0x0001},
    {"EOL.System",      0,                  1500,     0x0004},    
    {"EOL.Display",     1500,                500,     0x0004},    
    {"EOL.Bluetooth",   2000,                300,     0x0004},    
    {"EOL.NavSystem",   2300,                500,     0x0004},    
    {"EOL.NavIcon",     2800,                100,     0x0004},    
    {"EOL.Brand",       2900,                500,     0x0004},    
    {"EOL.Country",     3400,                400,     0x0004},    
    {"EOL.SpeechRec",   3800,               8000,     0x0004},    
    {"EOL.HFTuning",    11800,              3000,     0x0004},    
    {"EOL.RVC",         14800,               200,     0x0004},    
    {"CSM",             15000,               128,     0x0001},
    {"SPM",             15128,               128,     0x0001},
    {"OEDT",            15256,               128,     0x0001},    
    {"OSAL.AssertMode", 15384,                 4,     0x0001},   
    {"Trace.Config",    15388,               256,     0x0002},
    {"Startup.Param",   15644,               132,     0x0001},
    {"EDC.State",       15776,               192,     0x0001},
    {"LIN.Mode",        15968,                 4,     0x0001},
    {"LVDS.Mode",       15972,                 6,     0x0001},
    {"Download.Date",   15978,                 4,     0x0001},
    {"Version.String",  15982,               400,     0x0001},
    {"MOST",            16382,              1024,     0x0001},
    {"Max.MOST",        17406,               256,     0x0001},
    {"RVC",             17662,                 4,     0x0001},
    {"THM",             17666,                24,     0x0001},
    {"__END__",         17690,                 0,     0x0001}  
  }  
}; 

/*----------------------------------------------------------------------*/
/* Update  3: END   (default header)                                    */
/*----------------------------------------------------------------------*/
/*********************** FFD-GM *******************************************/
/* save config data in a structure*/
tsFFDConfig  vrFFDConfigData_GM=
{
  &vrFFDValidEntry_GM[0],                             /* pFFDValidEntry*/
  &vrFFDDefaultHeader_GM,                             /* pFFDDefaultHeader*/
  FFD_DATA_SET_LAST_ENTRY_GM,                         /* u8FFDNumberEntry*/
};

/******************** FFD-VOLVO ******************************************/
/* save config data in a structure*/
tsFFDConfig  vrFFDConfigData_VOLVO=
{
  &vrFFDValidEntry_VOLVO[0],                             /* pFFDValidEntry*/
  &vrFFDDefaultHeader_VOLVO,                             /* pFFDDefaultHeader*/
  FFD_DATA_SET_LAST_ENTRY_VOLVO,                         /* u8FFDNumberEntry*/
};

/******************** FFD-LCN2_KAI ******************************************/
/* save config data in a structure*/
tsFFDConfig  vrFFDConfigData_LCN2_KAI=
{
  &vrFFDValidEntry_LCN2_KAI[0],                             /* pFFDValidEntry*/
  &vrFFDDefaultHeader_LCN2_KAI,                             /* pFFDDefaultHeader*/
  FFD_DATA_SET_LAST_ENTRY_LCN2_KAI,                         /* u8FFDNumberEntry*/
};

/************************************************************************
* FUNCTIONS                                                             *
*      tsFFDConfig* psFFDGetConfigPointer(void)                         *
*                                                                       *
* DESCRIPTION                                                           *
*      get config pointer for different projects                        *
* INPUTS                                                                *
*      Pointer to shared memory                                         *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
************************************************************************/
tsFFDConfig* psFFDGetConfigPointer(void)
{
  tsFFDConfig  *VtspConfig=&vrFFDConfigData_GM;  /* set to default*/

  /* bta2hi 08.09.2014: registry boardcfg not available => remove access; only GM use FFD*/
#if 0 
  /* read from reg */
  OSAL_tIODescriptor  VtsFileHandle = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/BOARDCFG", OSAL_EN_READWRITE);
  if(VtsFileHandle != OSAL_ERROR)
  {
    OSAL_trIOCtrlRegistry   VtsRegEntry;
    tU8                     Vp8Buffer[30];
   
    VtsRegEntry.pcos8Name = (tS8*) "BOARD_NAME";
    VtsRegEntry.u32Size   = sizeof(Vp8Buffer);
    VtsRegEntry.ps8Value  = (tPU8) Vp8Buffer;
    VtsRegEntry.s32Type   = OSAL_C_S32_VALUE_STRING;

    if(OSAL_s32IOControl(VtsFileHandle, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&VtsRegEntry) != OSAL_ERROR)
    {/* trace out name*/
     FFD_TRACE(FFD_BOARD_NAME,Vp8Buffer,sizeof(Vp8Buffer));
     /*compare the first letter */
     if(memcmp(&Vp8Buffer[0],"GM NG",5)==0)
     {
       VtspConfig=&vrFFDConfigData_GM;
       /*trace out version */
       FFD_TRACE(FFD_FLASH_DATA_VERSION_TRACE,(tU8*)&VtspConfig->pFFDDefaultHeader->rHeaderVersion.u32StoredVersion,sizeof(tU32));
     }
     else if(memcmp(Vp8Buffer,"LCN2 KAI",8)==0)
     {
       VtspConfig=&vrFFDConfigData_LCN2_KAI;
       /*trace out version */
       FFD_TRACE(FFD_FLASH_DATA_VERSION_TRACE,(tU8*)&VtspConfig->pFFDDefaultHeader->rHeaderVersion.u32StoredVersion,sizeof(tU32));
     }
     else if(memcmp(Vp8Buffer,"ICM MCA",7)==0)
     {
       VtspConfig=&vrFFDConfigData_VOLVO;
       /*trace out version */
       FFD_TRACE(FFD_FLASH_DATA_VERSION_TRACE,(tU8*)&VtspConfig->pFFDDefaultHeader->rHeaderVersion.u32StoredVersion,sizeof(tU32));    
     }    
    }    
    OSAL_s32IOClose(VtsFileHandle);
  } 
#endif
  return(VtspConfig);
}

