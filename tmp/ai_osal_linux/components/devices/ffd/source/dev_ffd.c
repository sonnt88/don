/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_ffd.c
 *
 * In this file the osal driver for the FFD (Fast- Flash- driver) is implemented.
 *
 * global function:
 * --DEV_FFD_s32IODeviceInit:
 *     Create the FFD-Device and make a ram-copy into the shared memory
 * --DEV_FFD_s32IODeviceRemove:
 *     for T-Engine save data to flash; then remove the osal shared 
 *     memory and the osal mnessage queue      
 * --DEV_FFD_IOOpen:      
 *     This function open the interface 
 * --DEV_FFD_s32IOClose:  
 *      This function close the interface
 * --DEV_FFD_s32IORead:
 *      This function read data from the osal shared memory. 
 * --DEV_FFD_s32IOWrite:
 *      This function writes data to the osal shared memory    
 * --DEV_FFD_s32IOControl: 
 *      This function work on the control options for the FFD driver.
 *
 * local function:
 * --psFFDLock: 
 *      This function gives a pointer to the start of the shared memory and 
 *      locks the memory with the internal mutex. 
 * --vFFDUnLock:
 *      This function releases a shared memory pointer and unlocks the internal mutex.
 *
 * @date        2010-02-02
 *
 * @note
 *
 *  &copy; Copyright TMS GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#include <semaphore.h>
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"

#include "trace_interface.h"
#include "flashblocksize.h"
#include "dev_ffd.h"
#include "dev_ffd_private.h"
#include "dev_ffd_trace.h"
/* ************************************************************************/
/*  defines                                                               */
/* ************************************************************************/

/* *********************************************************************** */
/*  typedefs enum                                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct                                                        */
/* *********************************************************************** */

/* *********************************************************************** */
/*  variable                                                               */
/* *********************************************************************** */  
/* *********************************************************************** */
/*  static variable                                                        */
/* *********************************************************************** */  
static tU32                 vu32FFDFilePos[FFD_DATA_SET_MAX_ENTRYS];   
static sem_t               *vpFFD_SemLockInit;          /*id for process init*/
static tS32                 vs32FFDShmId = 0;           /*shared memory id*/
static tsFFDSharedMemory*   vpFFDSharedMemory = NULL;   /*pointer of shared memory*/
/* *********************************************************************** */
/*  static  functions                                                      */
/* *********************************************************************** */
static  tS32    s32FFDSeek(tU8 Pu8DataSet,tU32 Pu32Pos,tsFFDSharedMemory* PtsSharedMemory);
static  tS32    s32FFDShmInit(tBool VbFirstAttach);
static  tS32    s32FFDIsInit(tsFFDSharedMemory* PtsSharedMemory);

#ifdef LOAD_FFD_SO                   // load EOL device as seperate library

tS32 ffd_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{/*satisfy lint*/
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Id); 
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(szName); 
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32FD); 
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(app_id); 
  return (tS32)DEV_FFD_IOOpen(enAccess);
}

tS32 ffd_drv_io_close(tS32 s32ID, tU32 u32FD)
{ /*satisfy lint*/
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID); 
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD); 
  return ((tS32)DEV_FFD_s32IOClose());
}

tS32 ffd_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg)
{/*satisfy lint*/
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD); 
  return((tS32)DEV_FFD_s32IOControl((tU32)s32ID,s32fun,s32arg));
}

tS32 ffd_drv_io_write(tS32 s32ID, tU32 u32FD, tPCS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{ /*satisfy lint*/
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD); 
  return((tS32)DEV_FFD_s32IOWrite( (tU32)s32ID, (tPS8)pBuffer, u32Size, ret_size) );
}

tS32 ffd_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{/*satisfy lint*/
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD); 
  return((tS32)(tS32)DEV_FFD_s32IORead((tU32)s32ID,pBuffer,u32Size,ret_size));
}

#endif

/************************************************************************
* FUNCTIONS                                                             *
*      DEV_FFD_s32IODeviceInit                                          *
*                                                                       *
* DESCRIPTION                                                           *
*      Create the FFD-Device and make a ram-copy into the shared memory *
*      This will be done, because there are blocking problems if the    *
*      flash is busy.                                                   *
*                                                                       *
* INPUTS                                                                *
*      tVoid                                                            *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
************************************************************************/
tS32 DEV_FFD_s32IODeviceInit(tVoid)
{
  tS32    Vs32Result=OSAL_E_NOERROR;
  
  /* TRACE */
  FFD_TRACE(FFD_START_FUNCTION_CREATE,0,0);
  /*create sem, if not failed first attach*/
  vpFFD_SemLockInit = sem_open("FFD_INIT",O_EXCL | O_CREAT, FFD_ACCESS_RIGTHS, 0);
  if (vpFFD_SemLockInit != SEM_FAILED)
  { /*process first attach*/
	Vs32Result=s32FFDShmInit(TRUE); /*get pointer vpFFDSharedMemory*/
	if(Vs32Result==OSAL_E_NOERROR)
	{ /*create semaphore and save id to shared memory*/
      sem_t* VpSemLock=&vpFFDSharedMemory->tSemLockAccess;
      if(sem_init(VpSemLock, 1/*shared*/, 1) < 0)
      {
	    FFD_TRACE(FFD_ERROR_SEM_CREATE,0,0);
	    FATAL_M_ASSERT_ALWAYS();
	  }	
	  /*set access rights "eco_osal*/
      Vs32Result=s32FFDChangeGroupAccess(FFD_KIND_RES_SEM,-1,"/dev/shm/sem.FFD_INIT");
	  /*init shared variables  */
      vpFFDSharedMemory->vbFFDValidSharedMemory=FALSE;      //=> set to true if data of shared memory OK
	  vpFFDSharedMemory->vbFFDTraceRegState=FALSE;          //=> set to true if register trace available
	  vpFFDSharedMemory->s32AttachedProcesses=0;	  
	}
  }
  else
  { /* not first attach*/
	/* open semaphore*/
    vpFFD_SemLockInit= sem_open("FFD_INIT", 0);
    /* lock semaphore*/
    sem_wait(vpFFD_SemLockInit);
	/*get pointer shared memory*/
	Vs32Result=s32FFDShmInit(FALSE); /*get pointer vpFFDSharedMemory*/
  }
  /*for all process*/
  if(Vs32Result==OSAL_E_NOERROR)
  {  
	tsFFDSharedMemory* VsShMemFFD=psFFDLock();
    if(VsShMemFFD==NULL)
    {
      Vs32Result=OSAL_E_NOACCESS;  
    }
    else
	{ /*increment process counter*/
      VsShMemFFD->s32AttachedProcesses++;
      FFD_TRACE(FFD_ATTACH_COUNT,(tU8*)&VsShMemFFD->s32AttachedProcesses,sizeof(tU32));
      /* init file using */    
      memset(&vu32FFDFilePos[0],0,sizeof(vu32FFDFilePos[FFD_DATA_SET_MAX_ENTRYS]));   
	  /* check if trace and reload done*/
	  s32FFDIsInit(VsShMemFFD);    
	  /*unLock*/
	  vFFDUnLock(VsShMemFFD);
	}
  }
  sem_post(vpFFD_SemLockInit);
  /* TRACE */
  FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&Vs32Result,sizeof(tS32));
  return(Vs32Result);
}

/************************************************************************
* FUNCTIONS                                                             *
*       DEV_FFD_s32IODeviceRemove                                       *
*                                                                       *
* DESCRIPTION                                                           *
*      for T-Engine save data to flash; then remove the osal shared     * 
*      memory and the osal mnessage queue                               *
*                                                                       *
* INPUTS                                                                *
*      tVoid                                                            *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
************************************************************************/
tS32 DEV_FFD_s32IODeviceRemove(tVoid)
{
  tS32       Vs32Result=OSAL_E_NOERROR;
  /* TRACE */
  FFD_TRACE(FFD_START_FUNCTION_DESTROY,0,0);
  tsFFDSharedMemory* VsShMemFFD=psFFDLock();
  /*if shared memory valid */
  if(VsShMemFFD==NULL)
  {
    Vs32Result=OSAL_E_NOACCESS;
  }
  else
  { /*if shared memory valid */
    /*decrement counter*/
    VsShMemFFD->s32AttachedProcesses--;
	FFD_TRACE(FFD_ATTACH_COUNT,(tU8*)&VsShMemFFD->s32AttachedProcesses,sizeof(tU32));
	/*all process close */
	if(VsShMemFFD->s32AttachedProcesses==0)
	{/*unregister trace channel*/
      if(TR_chan_acess_bUnRegChan(TR_TTFIS_FFD_LI,(TRACE_CALLBACK)(FFD_vTraceCommand))==FALSE)
      {/*trace channel cannot unregister*/
        FFD_TRACE(FFD_ERROR_TRACE_CHANNEL_UNREGISTER,0,0);
      }/*end if*/
	  else
	  {
	    VsShMemFFD->vbFFDTraceRegState=FALSE;
	  }
	}	
	/*unLock*/
	vFFDUnLock(VsShMemFFD);
  }/*end if*/
  /* TRACE */
  FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&Vs32Result,sizeof(tS32));
  return(Vs32Result);
}
/* **************************************************FunctionHeaderBegin** *//**
*
*  tS32 DEV_FFD_IOOpen(OSAL_tenAccess PenAccess) 
*   
*  This function open the interface 
*
*  @param   PenAccess
*             --OSAL_EN_WRITEONLY
*             --OSAL_EN_READWRITE
*             --OSAL_EN_READONLY
*
* @return  >=0 success
*          OSAL_E_INVALIDVALUE: no valid paramter
*          OSAL_E_UNKNOWN:      driver cannot open
*
* @date    2010-02-02
*
* @note      
*
*//* ***********************************************FunctionHeaderEnd******* */
tS32 DEV_FFD_IOOpen(OSAL_tenAccess PenAccess)  
{
  tS32    Vs32ReturnCode=OSAL_E_NOERROR;
  /* TRACE */
  FFD_TRACE(FFD_START_FUNCTION_OPEN,0,0);
  /* check if channel valid */
  if((PenAccess==OSAL_EN_READONLY)||(PenAccess==OSAL_EN_WRITEONLY)||(PenAccess==OSAL_EN_READWRITE))
  {
#ifndef LOAD_FFD_SO  //if no shared memory function DEV_FFD_s32IODeviceInit is called once
    /*check if pointer vpFFDSharedMemory get for process*/
    if(vpFFDSharedMemory == 0)
    {
      Vs32ReturnCode=DEV_FFD_s32IODeviceInit();   
    }
    if(Vs32ReturnCode==OSAL_E_NOERROR)
#endif
    { /* lock and get memory*/
      tsFFDSharedMemory* VsShMemFFD=psFFDLock();
      if(VsShMemFFD==NULL)
      {
        Vs32ReturnCode=OSAL_E_NOACCESS;  
      }
      else
      { /*check trace register and reload done*/
	    Vs32ReturnCode=s32FFDIsInit(VsShMemFFD); 
	    vFFDUnLock(VsShMemFFD);
      }   
    }
  }
  else
  {
    Vs32ReturnCode = OSAL_E_INVALIDVALUE; 
  }
  FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&Vs32ReturnCode,sizeof(tS32));
  return(Vs32ReturnCode);
}/*end function*/
/* **************************************************FunctionHeaderBegin** *//**
*
*  tS32 DEV_FFD_s32IOClose()
*   
*  This function close the interface 
*
* @param   void 
*
* @return  tS32: Error Code                  
*
* @date    2010-02-02
*
* @note      
*
*//* ***********************************************FunctionHeaderEnd******* */
tS32 DEV_FFD_s32IOClose(void)
{
  tS32    Vs32ReturnCode=OSAL_E_NOERROR;
  /* TRACE */
  FFD_TRACE(FFD_START_FUNCTION_CLOSE,0,0);

  /* TRACE */
  FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&Vs32ReturnCode,sizeof(tS32));
  return(Vs32ReturnCode);
}/*end function*/

/* **************************************************FunctionHeaderBegin** *//**
*
*  tS32 DEV_FFD_s32IORead(tU32 Pu32Id, tPS8 PpBuffer, tU32 Pu32Size, tU32 *PpRetSize)
*   
*  This function reads data from the osal shared memory
*
* @param   Pu32Id:    Id       
*          PpBuffer:  buffer for the data to read and the data set
*          Pu32Size:  size to read      
*          PpRetSize: size read return    
*
* @return  tS32: Error Code    
*
* @date    2010-02-02
*
* @note      
*
*//* ***********************************************FunctionHeaderEnd******* */
tS32 DEV_FFD_s32IORead(tU32 Pu32Id, tPS8 PpBuffer, tU32 Pu32Size, tU32 *PpRetSize)
{
  tS32                      Vs32ReturnCode=OSAL_E_NOERROR;
  OSAL_trFFDDeviceInfo*     VtrpFFD=(void*) PpBuffer;
  tBool                     VbFilePos=FALSE;
  tU8                       Vu8DataSet;

  /*satisfy lint*/
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(Pu32Id); 
  /* TRACE */
  FFD_TRACE(FFD_START_FUNCTION_READ,0,0);
  /* lock and get memory*/
  tsFFDSharedMemory* VsShMemFFD=psFFDLock();
  if(VsShMemFFD==NULL)
  {
    Vs32ReturnCode=OSAL_E_NOACCESS;  
  }
  else if(VtrpFFD==NULL)
  {/*check parameter*/
    Vs32ReturnCode= OSAL_E_INVALIDVALUE;  
  }
  else
  { /*check file pos*/
    if(VtrpFFD->u8DataSet & 0x80)
    {
      VtrpFFD->u8DataSet&=~0x80;
      VbFilePos=TRUE;    
    }
    /* get/check valid data set for the project */
    if(VsShMemFFD->sFFDValidEntry[VtrpFFD->u8DataSet].bValid==FALSE)
    {/* error*/
      Vs32ReturnCode= OSAL_E_INVALIDVALUE;  
    }
    else
    { /*set data set*/
      Vu8DataSet=VsShMemFFD->sFFDValidEntry[VtrpFFD->u8DataSet].ubPos;
      /* TRACE */
      FFD_TRACE_MORE_INFO(FFD_READ_DATA_SET,Vu8DataSet,Pu32Size);
      /* check if valid data reload from flash or if data set wite*/
      if(VsShMemFFD->bReloadData==TRUE)
      {/* check parameter  */
        if((Vu8DataSet <  VsShMemFFD->u8NumberEntry)&&(VtrpFFD->pvArg!=NULL))
        { /* trace mask*/
          FFD_TRACE(FFD_MASK_DATASET,(tU8*)&VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32MaskSavedDataSets,sizeof(tU32));
          /* check if valid data in the data set*/
          if(VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32MaskSavedDataSets & (1UL << Vu8DataSet))
          {
            tU32                Vu32DataSetSize=VsShMemFFD->uActualData.rHeader.raEntry[Vu8DataSet].u32Size;
            tU32                Vu32DataSetOffset=VsShMemFFD->uActualData.rHeader.raEntry[Vu8DataSet].u32Offset+M_FFD_SIZE_HEADER(VsShMemFFD->u8NumberEntry);
            tU32                Vu32BytesToEnd;
            tU32                Vu32Count;

            /* if write with no file position*/
            if(VbFilePos==FALSE)
            {/* set file position to zero */
              vu32FFDFilePos[Vu8DataSet]=0;
            }/*end if*/
            /* set file position*/
            if(vu32FFDFilePos[Vu8DataSet] > Vu32DataSetSize)
            {
              vu32FFDFilePos[Vu8DataSet] = Vu32DataSetSize;
            }/*end if*/
            /* get space to end and the count to read*/
            Vu32BytesToEnd = Vu32DataSetSize - vu32FFDFilePos[Vu8DataSet];
            Vu32Count = (Pu32Size >= Vu32BytesToEnd) ? Vu32BytesToEnd :  Pu32Size;
            if(Vu32DataSetOffset + Vu32Count >= VsShMemFFD->u32FFDMaxSizeData)
            {
              Vs32ReturnCode=OSAL_E_NOSPACE;   
              FFD_FATAL_ASSERT();
            }/*end if*/
            else
            { /* TRACE */
              FFD_TRACE_MORE_INFO(FFD_FILE_POS_BEGIN,vu32FFDFilePos[Vu8DataSet],Vu32Count);
              /* check count*/
              if(Vu32Count!=0)
              {/* copy data into buffer*/
                memmove(VtrpFFD->pvArg,&VsShMemFFD->uActualData.u8Data[Vu32DataSetOffset+vu32FFDFilePos[Vu8DataSet]],Vu32Count);
                vu32FFDFilePos[Vu8DataSet] += Vu32Count;
                *PpRetSize=Vu32Count;        
              }/*end if*/
              /* TRACE */
              FFD_TRACE(FFD_FILE_POS_END,(tU8*)&vu32FFDFilePos[Vu8DataSet],sizeof(tU32));
            }/*end else*/      
          }/*end if check if valid data*/
          else
          {/*no valid data for this data set writes into the flash => no access for read  */
            Vs32ReturnCode=OSAL_E_NOACCESS;     
          }/*end else*/
        }/*end if check parameter*/
        else
        {
          Vs32ReturnCode= OSAL_E_INVALIDVALUE;  
        }/*end else wrong parameter*/
      }/*end if*/
      else
      {/*no valid data read from flash; vaild data doesn't exist*/
        Vs32ReturnCode=OSAL_E_DOESNOTEXIST;    
      }/*end else*/
    }
  }   
  if(Vs32ReturnCode == OSAL_E_NOERROR)
  {
    Vs32ReturnCode=*PpRetSize;
    FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&Vs32ReturnCode,sizeof(tS32));
  }
  else
  {
    tU8 Vu8TraceData[sizeof(Vs32ReturnCode)+sizeof(VtrpFFD->u8DataSet)];
    /*save Ver and PiStart into buffer*/
    memmove(&Vu8TraceData[0],&Vs32ReturnCode,sizeof(Vs32ReturnCode));
    if(VtrpFFD)
    {
      memmove(&Vu8TraceData[sizeof(Vs32ReturnCode)],&VtrpFFD->u8DataSet,sizeof(VtrpFFD->u8DataSet));
    }
    /*trace out read error and error memory */
    FFD_TRACE(FFD_READ_ERROR,(tU8*)&Vu8TraceData[0],sizeof(Vs32ReturnCode)+sizeof(VtrpFFD->u8DataSet));
  }
  /*unlock*/
  vFFDUnLock(VsShMemFFD);
  return(Vs32ReturnCode);
}/*end function*/

/* **************************************************FunctionHeaderBegin** *//**
*
*  tS32 DEV_FFD_s32IOWrite(tU32 Pu32Id,tPS8 PpBuffer, tU32 Pu32Size, tU32 *PpRetSize)
*   
*  This function writes data to the osal shared memory
*
* @param      Pu32Id:    Id       
*             PpBuffer:  buffer of the data to write and the data set
*             Pu32Size:  size to write      
*             PpRetSize: size write return
*
* @return  tS32: Error Code  
*
* @date    2010-02-02
*
* @note      
*
*//* ***********************************************FunctionHeaderEnd******* */
tS32 DEV_FFD_s32IOWrite(tU32 Pu32Id,tPS8 PpBuffer, tU32 Pu32Size, tU32 *PpRetSize)
{
  tS32                      Vs32ReturnCode=OSAL_E_NOERROR;
  OSAL_trFFDDeviceInfo*     VtrpFFD=(void*) PpBuffer;
  tBool                     VbFilePos=FALSE;
  tU8                       Vu8DataSet;

  /*satisfy lint*/
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(Pu32Id); 
  /* TRACE */
  FFD_TRACE(FFD_START_FUNCTION_WRITE,0,0);
  /* lock and get memory*/
  tsFFDSharedMemory* VsShMemFFD=psFFDLock();
  /*check pointer*/
  if(VsShMemFFD==NULL)
  {
    Vs32ReturnCode=OSAL_E_NOACCESS;  
  }
  else if(VtrpFFD==NULL)
  { /*check parameter*/
    Vs32ReturnCode= OSAL_E_INVALIDVALUE;  
  }
  else
  { /*check file pos*/
    if(VtrpFFD->u8DataSet & 0x80)
    {
      VtrpFFD->u8DataSet&=~0x80;
      VbFilePos=TRUE;    
    }
    /* get/check valid data set for the project */
    if(VsShMemFFD->sFFDValidEntry[VtrpFFD->u8DataSet].bValid==FALSE)
    {/* error*/
      Vs32ReturnCode= OSAL_E_INVALIDVALUE;  
    }
    else
    { /* TRACE */
      FFD_TRACE_MORE_INFO(FFD_WRITE_DATA_SET, VtrpFFD->u8DataSet, Pu32Size);
      /*set data set*/
      Vu8DataSet=VsShMemFFD->sFFDValidEntry[VtrpFFD->u8DataSet].ubPos;     
      /* check parameter */
      if((Vu8DataSet < VsShMemFFD->u8NumberEntry)&&(VtrpFFD->pvArg!=NULL))
      {  /*parameter OK*/  
        tU32  Vu32DataSetSize=VsShMemFFD->uActualData.rHeader.raEntry[Vu8DataSet].u32Size;
        tU32  Vu32DataSetOffset=VsShMemFFD->uActualData.rHeader.raEntry[Vu8DataSet].u32Offset+M_FFD_SIZE_HEADER(VsShMemFFD->u8NumberEntry);
        tU32  Vu32BytesToEnd;
        tU32  Vu32Count;

        /* if write with no file position*/
        if(VbFilePos==FALSE)
        {/* set file position to zero */
          vu32FFDFilePos[Vu8DataSet]=0;
        }/*end if*/
        /* set file position if position over size */
        if(vu32FFDFilePos[Vu8DataSet] > Vu32DataSetSize)
        {
          vu32FFDFilePos[Vu8DataSet] = Vu32DataSetSize;
        }/*end if*/
        /* get space to end and the count to read*/
        Vu32BytesToEnd = Vu32DataSetSize - vu32FFDFilePos[Vu8DataSet];
        Vu32Count = (Pu32Size >= Vu32BytesToEnd) ? Vu32BytesToEnd :  Pu32Size; 
        if(Vu32DataSetOffset + Vu32Count >= VsShMemFFD->u32FFDMaxSizeData)
        {
          Vs32ReturnCode=OSAL_E_NOSPACE;   
          FFD_FATAL_ASSERT();
        }/*end if*/
        else
        { /* TRACE */
          FFD_TRACE_MORE_INFO(FFD_FILE_POS_BEGIN,vu32FFDFilePos[Vu8DataSet],Vu32Count);
          /* check count*/
          if(Vu32Count!=0)
          {/* copy data into buffer*/
            memmove(&VsShMemFFD->uActualData.u8Data[Vu32DataSetOffset+vu32FFDFilePos[Vu8DataSet]],VtrpFFD->pvArg,Vu32Count);
            vu32FFDFilePos[Vu8DataSet] += Vu32Count;     
            *PpRetSize=Vu32Count;
            Vs32ReturnCode=Vu32Count;
            /* set mask for saved data set */
            VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32MaskSavedDataSets |= (1UL << Vu8DataSet);
            /* TRACE mask */
            FFD_TRACE(FFD_MASK_DATASET,(tU8*)&VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32MaskSavedDataSets,sizeof(tU32));
          }/*end if*/
          /* TRACE */
          FFD_TRACE(FFD_FILE_POS_END,(tU8*)&vu32FFDFilePos[Vu8DataSet],sizeof(tU32));
        }/*end else*/      
      }/*end if check parameter*/
      else
      {
        Vs32ReturnCode= OSAL_E_INVALIDVALUE;   
      }/*end else wrong parameter*/
    }
  }
  /*unlock*/
  vFFDUnLock(VsShMemFFD);
  /* TRACE */
  FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&Vs32ReturnCode,sizeof(tS32));
  return(Vs32ReturnCode);
}/*end function*/
/* **************************************************FunctionHeaderBegin** *//**
*
*  tS32 DEV_FFD_s32IOControl(tU32 Pu32Id,tS32 Ps32fun,tS32 Ps32arg)
*   
*  This function work on the control options for the FFD driver.
*
* @param    Pu32Id: Id       
*           Ps32fun: function
*           Ps32arg: info for device
*
* @return   tS32: Error Code 
*
* @date    2010-02-02
*
* @note      
*
*//* ***********************************************FunctionHeaderEnd******* */
tS32 DEV_FFD_s32IOControl(tU32 Pu32Id,tS32 Ps32fun,tS32 Ps32arg)
{
  tS32                  VtS32ReturnCode=OSAL_E_NOERROR;
  OSAL_trFFDDeviceInfo* VtrpFFD=(OSAL_trFFDDeviceInfo*) Ps32arg;

  /*satisfy lint*/
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(Pu32Id); 
  /* TRACE */
  FFD_TRACE(FFD_START_FUNCTION_CONTROL,0,0);
  /* lock and get memory*/
  tsFFDSharedMemory* VsShMemFFD=psFFDLock();
  if(VsShMemFFD==NULL)
  {
    VtS32ReturnCode=OSAL_E_NOACCESS;  
  }
  else if(VtrpFFD!=NULL)
  {/*check parameter*/
    if(VtrpFFD->u8DataSet < (tU8) EN_FFD_DATA_SET_LAST)
    {
      tU8 Vu8DataSet=VsShMemFFD->sFFDValidEntry[VtrpFFD->u8DataSet].ubPos;
      if(Vu8DataSet <  VsShMemFFD->u8NumberEntry)
      {
        switch(Ps32fun)
        {
        case OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW:
          {      
            VtS32ReturnCode=s32FFDSaveDataToFile(VsShMemFFD); 
          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_RELOAD:
          {
            if(Vu8DataSet==EN_FFD_DATA_SET_SPM)
            {
              VtS32ReturnCode=s32FFDReloadDataFromFile(VsShMemFFD);            
            }
            else
            {
              VtS32ReturnCode=OSAL_E_NOPERMISSION;  
            }
          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_SET_FLASHBLOCK_RAW_DATA:
          { 

            VtS32ReturnCode=OSAL_E_NOTSUPPORTED;         

          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAW_DATA:
          {   

            VtS32ReturnCode=OSAL_E_NOTSUPPORTED;         

          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_GET_FLASHBLOCK_RAWSIZE:
          {   

            VtS32ReturnCode=OSAL_E_NOTSUPPORTED;         
   
          }break;    
        case OSAL_C_S32_IOCTRL_DEV_FFD_GET_TOTAL_FLASH_SIZE:
          {
            if(VtrpFFD->pvArg!=NULL)
            {
              tU32 *Vu32pSize=VtrpFFD->pvArg;          
              *Vu32pSize = VsShMemFFD->u32FFDMaxSizeData;
            }/*end if*/
            else
            {
              VtS32ReturnCode= OSAL_E_INVALIDVALUE;   
            }/*end else*/
          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_SEEK:
          { 
            if(VtrpFFD->pvArg!=NULL)
            {
              tU32 *Vu32Pos=(tU32*)VtrpFFD->pvArg;  
              VtS32ReturnCode = s32FFDSeek(Vu8DataSet,*Vu32Pos,VsShMemFFD);           
            }/*end if*/
            else
            {
              VtS32ReturnCode= OSAL_E_INVALIDVALUE;   
            }/*end else*/ 
          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_GET_DATA_SET_SIZE:
          {        
            if(VtrpFFD->pvArg!=NULL)
            {
              tU32 *Vu32Size=(tU32*)VtrpFFD->pvArg;  
              *Vu32Size=VsShMemFFD->uActualData.rHeader.raEntry[Vu8DataSet].u32Size;   
            }/*end if*/
            else
            {
              VtS32ReturnCode= OSAL_E_INVALIDVALUE;   
            }/*end else*/ 
          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_GET_ROM_DATA_VERSION:
          {
            if(VtrpFFD->pvArg!=NULL)
            {
              tU32 *Vu32pVersion=(tU32*)VtrpFFD->pvArg; 
              *Vu32pVersion = (tU32)VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32StoredVersion;  
            }/*end if*/
            else
            {
              VtS32ReturnCode= OSAL_E_INVALIDVALUE;   
            }/*end else*/            
          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_SAVE_BACKUP_FILE:
          {
            VtS32ReturnCode=s32FFDSaveDataToFile(VsShMemFFD);
          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_LOAD_BACKUP_FILE:
          {
            VtS32ReturnCode=s32FFDReloadDataFromFile(VsShMemFFD);
          }break;
        case OSAL_C_S32_IOCTRL_DEV_FFD_GET_INFO_READ_DATA:
          {
            if(VtrpFFD->pvArg!=NULL)
            {
              tU16* Vu16pStateReadFlashData=(tU16*)VtrpFFD->pvArg; 
              *Vu16pStateReadFlashData = (tU32)VsShMemFFD->u16StateReadFlashData;  
            }/*end if*/
            else
            {
              VtS32ReturnCode= OSAL_E_INVALIDVALUE;   
            }/*end else*/    
          }break;
        default:
          VtS32ReturnCode= OSAL_E_INVALIDVALUE;   
          break;
        }/*end switch*/
      }
      else
      {
        VtS32ReturnCode=OSAL_E_INVALIDVALUE;
      }
    }
    else
    {
      VtS32ReturnCode=OSAL_E_INVALIDVALUE;
    }
  }
  else
  {
    VtS32ReturnCode=OSAL_E_INVALIDVALUE;
  }
  /* unlock */
  vFFDUnLock(VsShMemFFD);
  /* TRACE */
  FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&VtS32ReturnCode,sizeof(tS32));
  return(VtS32ReturnCode);
}/*end function*/

/***********************************************************************
 * tsFFDSharedMemory*        psFFDLock(void)
 *    	
 * This function gives a pointer to the start of the shared memory and 
 * locks the memory with the internal mutex. 
 *
 * @return  
 *    tsFFDSharedMemory*  pointer of shared memory
 *
 * @date    2010-08-09
 *
 * @note
 *
 **********************************************************************/
tsFFDSharedMemory*  psFFDLock(void)
{
  tS32                  Vs32Value;
  tsFFDSharedMemory*    VtspShMem=NULL;

  if (vpFFDSharedMemory!=0)
  {
    VtspShMem=vpFFDSharedMemory;
    /* get value semaphore*/
    if(sem_getvalue(&VtspShMem->tSemLockAccess,&Vs32Value)==0)
    { /* trace out value*/
      FFD_TRACE(FFD_SEM_VALUE,(tU8*)&Vs32Value,sizeof(tS32));	
      /* lock semaphore*/
      if(sem_wait(&VtspShMem->tSemLockAccess)==0)  
	  { /* no error */
	    FFD_TRACE(FFD_SEM_WAIT,0,0);	
      }/*end if*/
	  else
  	  {
	    FFD_TRACE(FFD_ERROR_SEM_WAIT,0,0);
	    FATAL_M_ASSERT_ALWAYS();
	  }
    }
    else
    {
      FFD_TRACE(FFD_ERROR_SEM_GET_VALUE,0,0);
      FATAL_M_ASSERT_ALWAYS();
    }
  }
  return(VtspShMem);
}
/***********************************************************************
 * static void vFFDUnLock(tsFFDSharedMemory* PtsShMemFFD)
 *    	
 * This function releases a shared memory pointer and unlocks the internal mutex.
 *
 * @parameter
 *     PtsShMemFFD  Pointer to shared memory 
 *   
 * @date    2010-08-09
 *
 * @note
 *
 **********************************************************************/
void vFFDUnLock(tsFFDSharedMemory* PtsShMemFFD)
{
  tS32   Vs32Value;

  if(PtsShMemFFD!=NULL)
  {
    sem_t* VpSemLock=&PtsShMemFFD->tSemLockAccess;
    /* get value semaphore*/
    if(sem_getvalue(VpSemLock,&Vs32Value)==0)
    { /* trace out value*/
      FFD_TRACE(FFD_SEM_VALUE,(tU8*)&Vs32Value,sizeof(tS32));
	  /* post semaphore*/
      if(sem_post(VpSemLock)==0)  
	  {
	    FFD_TRACE(FFD_SEM_POST,0,0);	
      }/*end if*/
	  else
	  {
	    FFD_TRACE(FFD_ERROR_SEM_POST,0,0);
	    FATAL_M_ASSERT_ALWAYS();
	  }
    }
    else
    {    
      FFD_TRACE(FFD_ERROR_SEM_GET_VALUE,0,0);
      FATAL_M_ASSERT_ALWAYS();
    }
  }
}
/***********************************************************************
 * static  tS32   s32FFDSeek(tU8   Pu8DataSet,tU32 Pu32Pos)
 * 
 * set file position to data set  			
 *  
 * @Param: PiDataSet   	 data set		
 * @Param: PuwPos        new file position
 *
 * @return  error code  defined in errno.h  
 *
 * @date    2007-10-23
 *
 * @note
 *
 **********************************************************************/
static  tS32    s32FFDSeek(tU8 Pu8DataSet,tU32 Pu32Pos,tsFFDSharedMemory* PtsSharedMemory)
{
  tS32      VtS32ReturnCode=OSAL_E_NOERROR;

  /*check position*/
  if(Pu32Pos >= PtsSharedMemory->uActualData.rHeader.raEntry[Pu8DataSet].u32Size)
  {
    VtS32ReturnCode=OSAL_E_INVALIDVALUE;  
  }/*end if*/
  else
  {
    vu32FFDFilePos[Pu8DataSet] = Pu32Pos;
  }/*end else*/

  /* TRACE */
  FFD_TRACE(FFD_FILE_POS_END,(tU8*)&vu32FFDFilePos[Pu8DataSet],sizeof(tU32));
  return(VtS32ReturnCode);
}/*end function*/

/* **************************************************FunctionHeaderBegin** *//**
*
*  static  tS32    s32FFDShmInit(tBool VbFirstAttach)
*   
*  This function init the the shared memory pointer vpFFDSharedMemory 
*  For first attach the shared memory should be create.
*  For all processes the map function should be called
*
* @param   tBool VbFirstAttach: check for first attach 
*
* @return  tS32: Error Code                  
*
* @date    2010-02-02
*
* @note      
*
*//* ***********************************************FunctionHeaderEnd******* */
static  tS32    s32FFDShmInit(tBool VbFirstAttach)
{
  tS32    Vs32ReturnCode=OSAL_E_NOERROR;
  if(VbFirstAttach)
  { /* create shared memory at first process attach */
    vs32FFDShmId = shm_open("FFD_SHAREDMEM", O_EXCL|O_RDWR|O_CREAT|O_TRUNC, FFD_ACCESS_RIGTHS );
	if(ftruncate(vs32FFDShmId, sizeof(tsFFDSharedMemory)) == -1)
    {
      vs32FFDShmId=-1;	  
    }    
  }
  else
  { /* open shared memory*/
    vs32FFDShmId = shm_open("FFD_SHAREDMEM", O_RDWR ,0);    
  }
  if(vs32FFDShmId!=-1)
  { /* map global shared memory into address space */
    vpFFDSharedMemory = (tsFFDSharedMemory*)mmap( 0,sizeof(tsFFDSharedMemory),PROT_READ | PROT_WRITE,GLOBAL_DATA_OPTION,vs32FFDShmId,0);
    if (vpFFDSharedMemory == MAP_FAILED)
    {
	   FFD_TRACE(FFD_ERROR_MMAP,0,0);
       FATAL_M_ASSERT_ALWAYS();
    }	
	else
	{
	  if(VbFirstAttach)
      { /*set access rights vs32FFDShmId to Group "eco_osal*/
		s32FFDChangeGroupAccess(FFD_KIND_RES_SHM,vs32FFDShmId,"vs32FFDShmId");	   
	  }
	}
  }
  else
  {
    Vs32ReturnCode=OSAL_E_UNKNOWN;
  }
  return(Vs32ReturnCode);
}

/* **************************************************FunctionHeaderBegin** *//**
*
*  static  tS32    s32FFDIsInit(tsFFDSharedMemory* PtsSharedMemory)
*   
*  This function checks if initialisiation for trace and shared memory done 
*
* @param   PtsSharedMemory: shared memory 
*
* @return  tS32: Error Code                  
*
* @date    2010-02-02
*
* @note      
*
*//* ***********************************************FunctionHeaderEnd******* */
static  tS32    s32FFDIsInit(tsFFDSharedMemory* PtsSharedMemory)
{
  tS32    Vs32Result=OSAL_E_NOERROR;
  /*check trace register*/
  if(PtsSharedMemory->vbFFDTraceRegState==FALSE)
  {
    PtsSharedMemory->vbFFDTraceRegState=TR_chan_acess_bRegChan(TR_TTFIS_FFD_LI,(TRACE_CALLBACK)(FFD_vTraceCommand));
    if(PtsSharedMemory->vbFFDTraceRegState==FALSE)
    {/*trace channel cannot register*/
      FFD_TRACE(FFD_ERROR_TRACE_CHANNEL_REGISTER,0,0);
    }/*end if*/
  }
  /* reload from flash */
  if(PtsSharedMemory->vbFFDValidSharedMemory==FALSE)
  {	 
    Vs32Result=s32FFDReloadDataFromFile(PtsSharedMemory);
	if(Vs32Result==(tS32)OSAL_E_NOERROR)
	{
	  PtsSharedMemory->vbFFDValidSharedMemory=TRUE;	
	}
  }
  return(Vs32Result);
}
/* End of File dev_ffd.c                                                      */







