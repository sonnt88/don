/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        dev_ffd_access.c
 *
 * In this file the osal driver for the FFD (Fast- Flash- driver) for file access is implemented.
 *
 * global function:
 *
 * local function:
 *
 * @date        2010-02-02
 *
 * @note
 *
 *  &copy; Copyright TMS GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#include <grp.h>
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"
#include "trace_interface.h"
#include "flashblocksize.h"
#include "dev_ffd_private.h"
#include "dev_ffd_trace.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <memory.h>

/* ************************************************************************/
/*  defines                                                               */
/* ************************************************************************/
#define FFD_FILE_EXT_BACKUP                       ".tmp"
#define FFD_FILE_NAME_MAX_LENGTH                  (sizeof(FFD_FILE_NAME_BACKUP_FILE)+sizeof(FFD_FILE_EXT_BACKUP))

/* *********************************************************************** */
/*  typedefs enum                                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct                                                        */
/* *********************************************************************** */

/* *********************************************************************** */
/*  variable                                                               */
/* *********************************************************************** */  

/* *********************************************************************** */
/*  static variable                                                        */
/* *********************************************************************** */  


/* *********************************************************************** */
/*  static  functions                                                      */
/* *********************************************************************** */
static tS32 vFFDRestoreDataToSharedMemory(tuFFDData* PtspActualData,
                                          const tuFFDData* PtspFlashData,
                                          tsFFDConfig*  PtspConfig);

/* *********************************************************************** */
/* Osal- Interface*/
/* *********************************************************************** */


/* *********************************************************************** */
/*  variable                                                        */
/* *********************************************************************** */  

/************************************************************************ 
|defines and macros 
|-----------------------------------------------------------------------*/

/************************************************************************
* FUNCTIONS                                                             *
*      s32FFDReloadDataFromFile                                         *
*                                                                       *
* DESCRIPTION                                                           *
*      Function to read from file representing the flash into the shared* 
*      memory.                                                          *
* INPUTS                                                                *
*      Pointer to shared memory                                         *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
* REMARK: if access to file system not possible,because file            *
*         system not mounted, functions returns an error.               *
*         open fails=> no read and write possible                       *
*         TODO: check mount point
************************************************************************/
tS32  s32FFDReloadDataFromFile(tsFFDSharedMemory* PsSharedMemory)
{
  tS32                  VtS32ReturnCode=OSAL_E_NOERROR;   
  tuFFDData            *VtspActualData=&PsSharedMemory->uActualData;
  tsFFDConfig*          VtspConfig=psFFDGetConfigPointer();
  tChar                 VstrFileName[FFD_FILE_NAME_MAX_LENGTH] = {0};  
  tU8                   Vu8Inc=0;
    
  FFD_TRACE(FFD_START_FUNCTION_RELOAD_FILE,0,0);
  /* copy file path and name*/
  strncat(VstrFileName, FFD_FILE_NAME_BACKUP_FILE, FFD_FILE_NAME_MAX_LENGTH);
  /* set reload data to false*/
  PsSharedMemory->bReloadData=FALSE;  
  /*set size */
  PsSharedMemory->u32FFDMaxSizeData = FFD_SIZE_DATA_MAX; /* max size */
  /* init actual */
  memset(VtspActualData,(int)0xffffffff,PsSharedMemory->u32FFDMaxSizeData);  
  /* set header, because no default header set before*/
  memmove(VtspActualData,VtspConfig->pFFDDefaultHeader,M_FFD_SIZE_HEADER(VtspConfig->u8FFDNumberEntry));
  /* for file and backup file*/
  while((PsSharedMemory->bReloadData==FALSE)&&(Vu8Inc<2))
  {     
    tuFFDData            *VtspFileData=(tuFFDData*)malloc(PsSharedMemory->u32FFDMaxSizeData);	
    int                   ViBackupFile = open(VstrFileName,O_RDONLY);

    /* check pointer and the file */
    if((VtspFileData==NULL)||(ViBackupFile < 0))
    { /*error*/ 
      VtS32ReturnCode=OSAL_E_NOACCESS;
    }
    else
    { /* read data */   
      int   ViSize = read(ViBackupFile,VtspFileData,PsSharedMemory->u32FFDMaxSizeData);   
      /* read success*/
      if((ViSize!=PsSharedMemory->u32FFDMaxSizeData))
      { /*error*/
        VtS32ReturnCode=OSAL_E_UNKNOWN;
      }
      else
      { /* init actual data*/
        memset(VtspActualData,(int)0xffffffff,PsSharedMemory->u32FFDMaxSizeData);  
        /* set actual directory*/
        memmove(VtspActualData,VtspConfig->pFFDDefaultHeader,M_FFD_SIZE_HEADER(VtspConfig->u8FFDNumberEntry));
        /* correct magic number */
        if(VtspFileData->rHeader.rHeaderVersion.u32Magic == FFD_FLASH_MAGIC_NR)
        { /* if version number correct*/
          if(VtspFileData->rHeader.rHeaderVersion.u32StoredVersion == VtspConfig->pFFDDefaultHeader->rHeaderVersion.u32StoredVersion)
          {/* if version matches, just copy everything*/
            memmove(VtspActualData,VtspFileData,PsSharedMemory->u32FFDMaxSizeData);     
            /* set flag to true*/
            PsSharedMemory->bReloadData=TRUE; 
			/* TRACE */
            FFD_TRACE(FFD_RELOAD_FILE,(tU8*)&VstrFileName,FFD_FILE_NAME_MAX_LENGTH);
          }/*end if*/
          else
          { /* restore data only, if the same board configuration (e.g. old GM new LCN KAI)*/
            if(  (VtspActualData->rHeader.rHeaderVersion.u32StoredVersion & 0xf000)
               ==(VtspFileData->rHeader.rHeaderVersion.u32StoredVersion & 0xf000))
            { /*restore flash data */
              if(vFFDRestoreDataToSharedMemory(VtspActualData,VtspFileData,VtspConfig)!=OSAL_E_NOERROR)
              { /*error*/
                VtS32ReturnCode=OSAL_E_UNKNOWN;
              }
              else
              { /* set flag to true*/
                PsSharedMemory->bReloadData=TRUE;    		       				
                FFD_TRACE(FFD_RELOAD_FILE,(tU8*)&VstrFileName,FFD_FILE_NAME_MAX_LENGTH);
				PsSharedMemory->u16StateReadFlashData=(M_FFD_INFO_READ_FLASH_DATA_ACTUAL+Vu8Inc);
              }
            }
          }/*end else*/        
        }/*end if */
        else
        { /*error*/
          VtS32ReturnCode=OSAL_E_UNKNOWN;
        }
      }/*end else*/
	  close(ViBackupFile);       
    }/*end else*/         
    free(VtspFileData); 
	Vu8Inc++;
	if(PsSharedMemory->bReloadData==FALSE)
	{/*next open backup file */
      strncat(VstrFileName,FFD_FILE_EXT_BACKUP,FFD_FILE_NAME_MAX_LENGTH-strlen(FFD_FILE_NAME_BACKUP_FILE));  
	}
  }/*end while*/
  /* init shared memory */
  {/* copy valid data set to shared memory*/
    tU32 VuwMaxEntrys=FFD_DATA_SET_MAX_ENTRYS;
    memmove(&PsSharedMemory->sFFDValidEntry[0],&VtspConfig->pFFDValidEntry[0],VuwMaxEntrys * (sizeof(tsFFDEntryValid)));
    /* copy number of entrys to shared memory*/
    PsSharedMemory->u8NumberEntry=VtspConfig->u8FFDNumberEntry;  
    /*save data to file*/
    s32FFDSaveDataToFile(PsSharedMemory);
  }    
  /* TRACE */
  FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&VtS32ReturnCode,sizeof(tS32));
  return(VtS32ReturnCode);
}
/************************************************************************
* FUNCTIONS                                                             *
* tS32  s32FFDSaveDataToFile(tsFFDSharedMemory* PsSharedMemory)         *
*                                                                       *
* DESCRIPTION                                                           *
*      Copy the contents of the shared memory into the file emulating   *
*       the file .                                                      *
*                                                                       *
*                                                                       *
* INPUTS                                                                *
*      Pointer to shared memory                                         *
*                                                                       *
* OUTPUTS                                                               *
*      tS32: Error Code                                                 *
************************************************************************/
tS32  s32FFDSaveDataToFile(tsFFDSharedMemory* PsSharedMemory)
{
  tS32    VtS32ReturnCode=OSAL_E_NOERROR;
  int     ViBackupFile;
  tBool   VbDataIdentical = FALSE;
  tChar   VstrFileName[FFD_FILE_NAME_MAX_LENGTH] = {0};  
  int     ViFile;

  FFD_TRACE(FFD_START_FUNCTION_SAVE_FILE,0,0);
  /* copy file path and name for file*/
  strncat(VstrFileName, FFD_FILE_NAME_BACKUP_FILE, FFD_FILE_NAME_MAX_LENGTH);

  /* open file for read for compare data with shared memory*/
  /* if data not identical, save file */
  ViFile = open(VstrFileName, O_RDONLY);
  if(ViFile >= 0)
  {
    tuFFDData *VtspFileData=(tuFFDData*)malloc(PsSharedMemory->u32FFDMaxSizeData);

    /* check pointer */
    if(VtspFileData == NULL)
    { /*error*/
      VtS32ReturnCode = OSAL_E_NOSPACE;
    }
    else
    { /* read data */   
      int   ViSize = read(ViFile,VtspFileData,PsSharedMemory->u32FFDMaxSizeData);   
      /* read success*/
      if(ViSize!=PsSharedMemory->u32FFDMaxSizeData)
      { /*error*/
        VtS32ReturnCode=OSAL_E_UNKNOWN;
      }
      else
	  {
	    if(memcmp(VtspFileData,&PsSharedMemory->uActualData,ViSize)==0)
		{/*data are identical -> don't need to copy*/
          VbDataIdentical = TRUE;	
		  FFD_TRACE(FFD_FILE_INDENTICAL,0,0);
        }/*end if compare*/
      }/*end else*/

      free(VtspFileData);
    }/*end else*/
    close(ViFile);
  }

  if(VbDataIdentical==FALSE)
  {/* open file for write*/
    ViBackupFile = open(VstrFileName, O_WRONLY | O_CREAT | O_TRUNC, FFD_ACCESS_RIGTHS);
    if(ViBackupFile >= 0)
    { /*Write shared memory content to File*/
      int ViSize=write(ViBackupFile,&PsSharedMemory->uActualData,PsSharedMemory->u32FFDMaxSizeData); 
      if(ViSize!=PsSharedMemory->u32FFDMaxSizeData)
      {
        VtS32ReturnCode=OSAL_ERROR;		
      }	  
	  else
	  {/* fsync => transfers ("flushes") all modified in-core data of 
		   (i.e., modified buffer cache pages for) the file referred to by the file descriptor*/
	    if(fsync(ViBackupFile))
		{
		  VtS32ReturnCode=OSAL_ERROR;
		}
		else
		{ /*set access rights of file to group "eco_osal*/
		  VtS32ReturnCode=s32FFDChangeGroupAccess(FFD_KIND_RES_FILE,ViFile,VstrFileName);
		  if(VtS32ReturnCode==OSAL_E_NOERROR)
		  {/* write and flush OK => Trace*/
		    FFD_TRACE(FFD_SAVE_FILE,(tU8*)&VstrFileName,FFD_FILE_NAME_MAX_LENGTH);	    
		  }
		}        
	  }
      close(ViBackupFile);       
    }
    else
    {
      VtS32ReturnCode=OSAL_E_NOACCESS;
    }
    /* copy extention for backup file */
    strncat(VstrFileName,FFD_FILE_EXT_BACKUP,FFD_FILE_NAME_MAX_LENGTH-strlen(FFD_FILE_NAME_BACKUP_FILE));  
    /* save data to backup file */
    ViBackupFile = open(VstrFileName, O_WRONLY | O_CREAT | O_TRUNC, FFD_ACCESS_RIGTHS);
    if(ViBackupFile >= 0)
    { /*Write shared memory content to File*/
      int ViSize=write(ViBackupFile,&PsSharedMemory->uActualData,PsSharedMemory->u32FFDMaxSizeData); 
      if(ViSize!=PsSharedMemory->u32FFDMaxSizeData)
      {
        VtS32ReturnCode=OSAL_ERROR;
      }
	  else
	  {/* fsync => transfers ("flushes") all modified in-core data of 
		   (i.e., modified buffer cache pages for) the file referred to by the file descriptor*/
	    if(fsync(ViBackupFile))
		{
		  VtS32ReturnCode=OSAL_ERROR;
		}
		else
		{ /*set access rights of file to group "eco_osal*/
		  VtS32ReturnCode=s32FFDChangeGroupAccess(FFD_KIND_RES_FILE,ViFile,VstrFileName);
		  if(VtS32ReturnCode==OSAL_E_NOERROR)
		  {/* write and flush OK => Trace*/
		    FFD_TRACE(FFD_SAVE_FILE,(tU8*)&VstrFileName,FFD_FILE_NAME_MAX_LENGTH);	    
		  }
		}           
	  }
      close(ViBackupFile);       
    }
    else
    {
      VtS32ReturnCode=OSAL_E_NOACCESS;
    }
  }/*end if not identical*/
   /* TRACE */
  FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&VtS32ReturnCode,sizeof(tS32));
  return(VtS32ReturnCode);
}

/***********************************************************************
 * static tS32 vFFDReloadDataToSharedMemory(tsFFDSharedMemory* PsSharedMemory)
 *
 * reload data to shared memory
 *   
 * @paramter:
 *    PsSharedMemory: pointer of the shared memory 
 *
 * @return  tS32: Error Code  
 *
 * @date    2010-11-19
 *
 * @note
 *
 **********************************************************************/
static tS32 vFFDRestoreDataToSharedMemory(tuFFDData* PtspActualData,const tuFFDData* PtspFlashData,tsFFDConfig* PtspConfig)
{
  tS32               VtS32ReturnCode=OSAL_E_NOERROR;
  tU32               VuwSet;          
  tS32               VS32OffsetNewDataSet=0;
  tsFFDHeaderEntry   VtsActualDataSet;
  tsFFDHeaderEntry   VtsFlashDataSet;

  /* output trace version different*/
  FFD_vTraceMoreInfo(FFD_VERSION_DIFFERENT,
                     PtspActualData->rHeader.rHeaderVersion.u32StoredVersion,
                     PtspFlashData->rHeader.rHeaderVersion.u32StoredVersion);
  /* abr: 17.06.2011 check header, if new data set added,
     the new structure does not fit the length of the new haeder.
     on offset has to be calculated*/
  VtsActualDataSet=PtspActualData->rHeader.raEntry[PtspConfig->u8FFDNumberEntry-1];
  for(VuwSet=0; VuwSet < FFD_DATA_SET_MAX_ENTRY; VuwSet++)
  {
    VtsFlashDataSet=PtspFlashData->rHeader.raEntry[VuwSet]; /*lint !e661 !e662*/ 
    /*check name* "__END__"*/
    if(memcmp(&VtsActualDataSet.u8Filename[0],&VtsFlashDataSet.u8Filename[0],FFD_FLASH_NAME_LENGTH)==0)
    { /* end position in flash found*/
      /* calc offset the size of the new data header */   
      VS32OffsetNewDataSet = ((((PtspConfig->u8FFDNumberEntry-1))-VuwSet)*sizeof(tsFFDHeaderEntry)); 
      break;
    }/*end if*/
  }/*end for*/
  /* check if end position found*/
  if(VuwSet == FFD_DATA_SET_MAX_ENTRY)
  {/* end position not found flash not valid; set reload data to false */
    VtS32ReturnCode=OSAL_E_UNKNOWN;  
  }/*end if*/
  else
  {
    tU32   VuwSizeMin;
    /*check Version of each data set*/
    for(VuwSet=0; VuwSet < PtspConfig->u8FFDNumberEntry; VuwSet++)
    {
      VtsActualDataSet=PtspActualData->rHeader.raEntry[VuwSet];
      VtsFlashDataSet=PtspFlashData->rHeader.raEntry[VuwSet];
      /*check version*/
      if((VtsActualDataSet.u32Version==VtsFlashDataSet.u32Version)
         &&(PtspFlashData->rHeader.rHeaderVersion.u32MaskSavedDataSets & (1UL << VuwSet)))
      { /* set data mask to valid */      
        PtspActualData->rHeader.rHeaderVersion.u32MaskSavedDataSets |= (1UL << VuwSet);             
        /*check size, if different copy the min size*/
        if(VtsFlashDataSet.u32Size > VtsActualDataSet.u32Size)
        {
          VuwSizeMin=VtsActualDataSet.u32Size;
        }
        else
        {
          VuwSizeMin=VtsFlashDataSet.u32Size;
        }
        /*save flash data from set to actual data*/
        /*increment or decrement offset of flash data;if new data set added, the new structure does not fit the length of the new haeder*/
        memmove(&PtspActualData->u8Data[VtsActualDataSet.u32Offset+M_FFD_SIZE_HEADER(PtspConfig->u8FFDNumberEntry)],
               (&PtspFlashData->u8Data[VtsFlashDataSet.u32Offset+M_FFD_SIZE_HEADER(PtspConfig->u8FFDNumberEntry)])-VS32OffsetNewDataSet,
               VuwSizeMin);
      }
    }/*end for*/  
  }/*end else*/
  /*output mask for saved data set */
  FFD_TRACE(FFD_MASK_DATASET,(tU8*)&PtspActualData->rHeader.rHeaderVersion.u32MaskSavedDataSets,sizeof(tU32));

  return(VtS32ReturnCode);
}

/******************************************************************************
* FUNCTION: s32FFDChangeGroupAccess()
*
* DESCRIPTION: change group access to eco_osal
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 31 07
*****************************************************************************/
tS32  s32FFDChangeGroupAccess(teFFDKindRes PeKindRes,tS32 Ps32Id,const char* PStrResource)
{
  tS32   Vs32Result=OSAL_E_NOERROR;
  /*  The getgrnam() function returns a pointer to a structure containing the group id*/
  struct group *VtGroup=getgrnam(FFD_ACCESS_RIGTHS_GROUP_NAME);
  if(VtGroup==NULL)
  { 
    //Vs32Result=OSAL_E_UNKNOWN;  
	//snprintf(Vu8Buffer,sizeof(Vu8Buffer),"getgrnam() fails for resource %s",PStrResource);
  }
  else
  { /*for named semaphore*/
	if(PeKindRes==FFD_KIND_RES_SEM)
	{
	  if(chown(PStrResource, (uid_t) -1, VtGroup->gr_gid)==-1)
	  {	
		Vs32Result=OSAL_E_UNKNOWN;  	 	   
	  }
	  else
	  {
        if(chmod(PStrResource, FFD_ACCESS_RIGTHS) == -1)
        {		
          Vs32Result=OSAL_E_UNKNOWN;  	 	  
        }
	  }
	}
	else
	{/* for other resource*/
	  if(fchown(Ps32Id, (uid_t) -1, VtGroup->gr_gid)==-1)
	  {	   	
		Vs32Result=OSAL_E_UNKNOWN;  	   	   
	  }
	  else
	  {
        if(fchmod(Ps32Id, FFD_ACCESS_RIGTHS) == -1)
        {        
          Vs32Result=OSAL_E_UNKNOWN;  	         	       
        }
	  }
	}
  }
  /*trace error code and set error memory*/
  if(Vs32Result!=OSAL_E_NOERROR)
  {
    FFD_TRACE(FFD_RETURN_FUNCTION,(tU8*)&Vs32Result,sizeof(tS32));
  }
  return(Vs32Result);
}
/* End of File dev_ffd_file_access.c             */







