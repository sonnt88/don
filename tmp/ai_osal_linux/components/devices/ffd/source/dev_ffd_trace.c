
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
//#include <tk/tkernel.h>
#include <ctype.h>
#include <string.h>
#include <fcntl.h>
#include <memory.h>
#include <unistd.h>

#define SYSTEM_S_IMPORT_INTERFACE_FFD_DEF
#include "system_pif.h"

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "flashblocksize.h"
#include "dev_ffd.h"
#include "dev_ffd_trace.h"
#include "dev_ffd_private.h"
#include "trace_interface.h"

/* *********************************************************************** */
/*  define                                                                 */
/* *********************************************************************** */
/*for ttfis command*/
#define FFD_TTFIS_COMMAND                      3
#define FFD_TTFIS_DATA_SET                     4

/*length for traces*/
#define FFD_TTFIS_LEN_DATA                    224
#define FFD_TTFIS_LEN_HEADER                    1
#define FFD_TTFIS_LEN_MESSAGE                 (FFD_TTFIS_LEN_DATA+FFD_TTFIS_LEN_HEADER)

#define FFD_TRACE_DATA_BYTE_KIND_INFO   0
/* *********************************************************************** */
/*  variable                                                               */
/* *********************************************************************** */

/* *********************************************************************** */
/*  function                                                               */
/* *********************************************************************** */

/* **************************************************FunctionHeaderBegin** *//**
*  LOCAL void   FFD_vTraceCommand(u8* Pu8pData)
*
*  This function is the callback function for the commands of the TTFIS.
*
* @param   Pu8pData:   pointer of the data given by ttfis
*                      byte 0: number
*                      byte 1: CompId (0x00)
*                      byte 2: ClassId(0xde)
*                      byte 3: kind function
*                      byte 4: data set information
*
* @return  void
*
* @date    2008-01-10
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void   FFD_vTraceCommand(const tU8* Pu8pData)
{    
  DEV_FFD_IOOpen(OSAL_EN_READWRITE);
  switch (Pu8pData[FFD_TTFIS_COMMAND])
  {
  case FFD_ATTR_ERASE_FLASH_BLOCK_CMD:
    {
      FFD_TRACE(FFD_CMD_NOT_SUPPORTED_FOR_LINUX,0,0);
    }break;
  case FFD_ATTR_GET_ACTUAL_DATA_MIRROR_ALL:
    { 
      tU32               Vu32Byte;
      tsFFDSharedMemory* VsShMemFFD=psFFDLock();                     /* lock and get memory*/
      tU32               Vu32LenData=VsShMemFFD->u32FFDMaxSizeData;
      tU32               Vu32OffsetHeader=M_FFD_SIZE_HEADER(VsShMemFFD->u8NumberEntry);
      /* for all FFD_TTFIS_LEN_DATA bytes a trace*/
      for (Vu32Byte=Vu32OffsetHeader;Vu32Byte < Vu32LenData;Vu32Byte+=FFD_TTFIS_LEN_DATA)
      {
        if (Vu32LenData-Vu32Byte > FFD_TTFIS_LEN_DATA)
        {
          FFD_TRACE(FFD_ACTUAL_DATA_MIRROR_ALL,(tU8*)&VsShMemFFD->uActualData.u8Data[Vu32Byte],FFD_TTFIS_LEN_DATA);
        }/*end if*/
        else
        {
          FFD_TRACE(FFD_ACTUAL_DATA_MIRROR_ALL,(tU8*)&VsShMemFFD->uActualData.u8Data[Vu32Byte],(tU8)(Vu32LenData-Vu32Byte));
        }/*end else*/
      }/*end for*/
      vFFDUnLock(VsShMemFFD); 
    }break;
  case FFD_ATTR_GET_ACTUAL_DATA_MIRROR_SET:
    { 
      /* lock and get memory*/
      tsFFDSharedMemory*  VsShMemFFD=psFFDLock(); 
      tU8                 Vu8DataSet= VsShMemFFD->sFFDValidEntry[Pu8pData[FFD_TTFIS_DATA_SET]].ubPos;  
      /*check data set*/
      if (Vu8DataSet < VsShMemFFD->u8NumberEntry)
      {/*get size*/      
        tU32  Vu32Size=VsShMemFFD->uActualData.rHeader.raEntry[Vu8DataSet].u32Size;
        tU32  Vu32Offset=VsShMemFFD->uActualData.rHeader.raEntry[Vu8DataSet].u32Offset+M_FFD_SIZE_HEADER(VsShMemFFD->u8NumberEntry);
        tU32  Vu32Byte;
        /*trace out name*/
        FFD_TRACE(FFD_ACTUAL_DATA_SET_NAME,&VsShMemFFD->uActualData.rHeader.raEntry[Vu8DataSet].u8Filename[0],sizeof(VsShMemFFD->uActualData.rHeader.raEntry[Vu8DataSet].u8Filename));
        /* for all FFD_TTFIS_LEN_DATA bytes a trace*/
        for (Vu32Byte=0;(Vu32Byte < Vu32Size); Vu32Byte+=FFD_TTFIS_LEN_DATA)
        {
          if (Vu32Size-Vu32Byte > FFD_TTFIS_LEN_DATA)
          {
            FFD_TRACE(FFD_ACTUAL_DATA_MIRROR_SET,(tU8*)&VsShMemFFD->uActualData.u8Data[Vu32Offset+Vu32Byte],FFD_TTFIS_LEN_DATA);
          }/*end if*/
          else
          {
            FFD_TRACE(FFD_ACTUAL_DATA_MIRROR_SET,(tU8*)&VsShMemFFD->uActualData.u8Data[Vu32Offset+Vu32Byte],(tU8)((Vu32Size-Vu32Byte)));
          }/*end else*/
        }/*end for*/
      }/*end if*/
      vFFDUnLock(VsShMemFFD); 
    }break;
  case FFD_ATTR_GET_ACTUAL_DATA_MIRROR_ALL_STRUCTURE:
    {
      tU8   Vu8Count;
      /* lock and get memory*/
      tsFFDSharedMemory* VsShMemFFD=psFFDLock(); 
      /*trace uwStoredVersion*/
      FFD_TRACE(FFD_FLASH_DATA_VERSION_TRACE,(tU8*)&VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32StoredVersion,sizeof(tU32));    
      /* trace uwMagic*/
      FFD_TRACE(FFD_GET_MAGIC,(tU8*)&VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32Magic,sizeof(tU32));
      /* trace u32MaskSavedDataSets*/
      FFD_TRACE(FFD_GET_MASK_DATASET,(tU8*)&VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32MaskSavedDataSets,sizeof(tU32));
      /* trace out raEntry*/
      for (Vu8Count=0;Vu8Count < VsShMemFFD->u8NumberEntry;Vu8Count++)
      {
        tU32 Vu32Size=VsShMemFFD->uActualData.rHeader.raEntry[Vu8Count].u32Size;
        tU32 Vu32Offset=VsShMemFFD->uActualData.rHeader.raEntry[Vu8Count].u32Offset;
        tU32 Vu32Byte;
        /*name*/
        FFD_TRACE(FFD_ACTUAL_DATA_SET_NAME,(tU8*)&VsShMemFFD->uActualData.rHeader.raEntry[Vu8Count].u8Filename[0],sizeof(VsShMemFFD->uActualData.rHeader.raEntry[Vu8Count].u8Filename));
        /*offset*/
        FFD_TRACE(FFD_ACTUAL_DATA_SET_OFFSET,(tU8*)&Vu32Offset,sizeof(tU32));
        /*size*/
        FFD_TRACE(FFD_ACTUAL_DATA_SET_SIZE,(tU8*)&Vu32Size,sizeof(tU32));
        /*data*/
        Vu32Offset=Vu32Offset+M_FFD_SIZE_HEADER(VsShMemFFD->u8NumberEntry);
        if (Vu32Size!=0)
        {/* for all FFD_TTFIS_LEN_DATA bytes a trace*/
          for (Vu32Byte=0;(Vu32Byte < Vu32Size); Vu32Byte+=FFD_TTFIS_LEN_DATA)
          {
            if (Vu32Size-Vu32Byte > FFD_TTFIS_LEN_DATA)
            {
              FFD_TRACE(FFD_ACTUAL_DATA_MIRROR_SET,(tU8*)&VsShMemFFD->uActualData.u8Data[Vu32Offset+Vu32Byte],FFD_TTFIS_LEN_DATA);
            }/*end if*/
            else
            {
              FFD_TRACE(FFD_ACTUAL_DATA_MIRROR_SET,(tU8*)&VsShMemFFD->uActualData.u8Data[Vu32Offset+Vu32Byte],(tU8)((Vu32Size-Vu32Byte)));
            }/*end else*/
          }/*end for*/
        }/*end if*/
      }/*end for*/   
      vFFDUnLock(VsShMemFFD); 
    }break;
  case FFD_ATTR_GET_FLASH_DATA_VERSION_CMD:
    {/* lock and get memory*/
      tsFFDSharedMemory* VsShMemFFD=psFFDLock(); 
      FFD_TRACE(FFD_FLASH_DATA_VERSION_TRACE,(tU8*)&VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32StoredVersion,sizeof(tU32));      
      vFFDUnLock(VsShMemFFD); 
    }break;
  case FFD_ATTR_GET_MASK_FOR_SAVED_DATASETS:
    { /* lock and get memory*/
      tsFFDSharedMemory* VsShMemFFD=psFFDLock(); 
      /* TRACE mask */
      FFD_TRACE(FFD_GET_MASK_DATASET,(tU8*)&VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32MaskSavedDataSets,sizeof(tU32));
      vFFDUnLock(VsShMemFFD); 
    }break;
  case FFD_ATTR_SHUTDOWN:
    {     
      DEV_FFD_s32IODeviceRemove();
    }break;
  case FFD_ATTR_GET_SIZE:
    {/* lock and get memory*/
      tsFFDSharedMemory*    VsShMemFFD=psFFDLock(); 
      tU32 Pu32Size=VsShMemFFD->u32FFDMaxSizeData;
      tU32 Pu32SizeHeader= (tU32) M_FFD_SIZE_HEADER(VsShMemFFD->u8NumberEntry);
      tU32 Pu32SizeData=Pu32Size-Pu32SizeHeader;
      /*trace*/
      FFD_TRACE(FFD_GET_SIZE,(tU8*)&Pu32Size,sizeof(tU32));
      FFD_TRACE(FFD_GET_HEADER_SIZE,(tU8*)&Pu32SizeHeader,sizeof(tU32));
      FFD_TRACE(FFD_GET_DATA_SIZE,(tU8*)&Pu32SizeData,sizeof(tU32));
      vFFDUnLock(VsShMemFFD); 
    }break;
  case FFD_ATTR_CHANGE_VERSION_NUMBER:
    {/* lock and get memory*/
      tsFFDSharedMemory*    VsShMemFFD=psFFDLock(); 
      tU32                  Vu32Version = VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32StoredVersion;   
      OSAL_trFFDDeviceInfo  VtrFFD;
      if ((Vu32Version&0x0FFF) < 0x0fff)
      {
        VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32StoredVersion=Vu32Version+1;
      }
      vFFDUnLock(VsShMemFFD); 
      VtrFFD.pvArg=NULL;
      VtrFFD.u8DataSet=(tU8)EN_FFD_DATA_SET_SPM;
      DEV_FFD_s32IOControl(1,OSAL_C_S32_IOCTRL_DEV_FFD_SAVENOW,(tS32)&VtrFFD);
      FFD_TRACE(FFD_FLASH_DATA_VERSION_TRACE,(tU8*)&VsShMemFFD->uActualData.rHeader.rHeaderVersion.u32StoredVersion,sizeof(tU32));    
    }break;
  case FFD_ATTR_BACKUP_FILE_SAVE:
    {
      tsFFDSharedMemory*    VsShMemFFD=psFFDLock();
      if (s32FFDSaveDataToFile(VsShMemFFD)==(tS32)OSAL_E_NOERROR)
      {
        FFD_TRACE(FFD_BACKUP_FILE_SAVE_DONE,0,0);
      }
      else
      {
        FFD_TRACE(FFD_BACKUP_FILE_SAVE_ERROR,0,0);
      }       
      vFFDUnLock(VsShMemFFD); 
    }break;
  case FFD_ATTR_BACKUP_FILE_LOAD:
    {
      tsFFDSharedMemory*    VsShMemFFD=psFFDLock();
      if (s32FFDReloadDataFromFile(VsShMemFFD)==(tS32)OSAL_E_NOERROR)
      {
        FFD_TRACE(FFD_BACKUP_FILE_LOAD_DONE,0,0);
      }
      else
      {
        FFD_TRACE(FFD_BACKUP_FILE_LOAD_ERROR,0,0);
      }      
      vFFDUnLock(VsShMemFFD);               
    }break;
  case FFD_ATTR_BACKUP_FILE_OUTPUT:
    {
      tU8                Vu8Count;
      tsFFDSharedMemory* VsShMemFFD=psFFDLock(); 
      int                ViBackupFile = open(FFD_FILE_NAME_BACKUP_FILE,O_RDONLY);
      tuFFDData          *VtspFileData=(tuFFDData*)malloc(VsShMemFFD->u32FFDMaxSizeData);

      /* check pointer and the file */
      if ((VtspFileData==NULL)||(ViBackupFile < 0))
      { /*error TRACE */
        FFD_TRACE(FFD_BACKUP_FILE_OUTPUT_ERROR,0,0);
      }
      else
      { /* read data */   
        int   ViSize = read(ViBackupFile,VtspFileData,VsShMemFFD->u32FFDMaxSizeData);   
        /* read success*/
        if (ViSize!=(int)VsShMemFFD->u32FFDMaxSizeData)
        { /*error Trace*/
          FFD_TRACE(FFD_BACKUP_FILE_OUTPUT_ERROR,0,0);
        }
        else
        { /*trace uwStoredVersion*/
          FFD_TRACE(FFD_FLASH_DATA_VERSION_TRACE,(tU8*)&VtspFileData->rHeader.rHeaderVersion.u32StoredVersion,sizeof(tU32));    
          /* trace uwMagic*/
          FFD_TRACE(FFD_GET_MAGIC,(tU8*)&VtspFileData->rHeader.rHeaderVersion.u32Magic,sizeof(tU32));
          /* trace u32MaskSavedDataSets*/
          FFD_TRACE(FFD_GET_MASK_DATASET,(tU8*)&VtspFileData->rHeader.rHeaderVersion.u32MaskSavedDataSets,sizeof(tU32));
          /* trace out raEntry*/
          for (Vu8Count=0;Vu8Count < VsShMemFFD->u8NumberEntry;Vu8Count++)
          {
            tU32 Vu32Size=VtspFileData->rHeader.raEntry[Vu8Count].u32Size;
            tU32 Vu32Offset=VtspFileData->rHeader.raEntry[Vu8Count].u32Offset;
            tU32 Vu32Byte;
            /*name*/
            FFD_TRACE(FFD_ACTUAL_DATA_SET_NAME,(tU8*)&VtspFileData->rHeader.raEntry[Vu8Count].u8Filename[0],sizeof(VtspFileData->rHeader.raEntry[Vu8Count].u8Filename));
            /*offset*/
            FFD_TRACE(FFD_ACTUAL_DATA_SET_OFFSET,(tU8*)&Vu32Offset,sizeof(tU32));
            /*size*/
            FFD_TRACE(FFD_ACTUAL_DATA_SET_SIZE,(tU8*)&Vu32Size,sizeof(tU32));
            /*data*/
            Vu32Offset=Vu32Offset+M_FFD_SIZE_HEADER(VsShMemFFD->u8NumberEntry);
            if (Vu32Size!=0)
            {/* for all FFD_TTFIS_LEN_DATA bytes a trace*/
              for (Vu32Byte=0;(Vu32Byte < Vu32Size); Vu32Byte+=FFD_TTFIS_LEN_DATA)
              {
                if (Vu32Size-Vu32Byte > FFD_TTFIS_LEN_DATA)
                {
                  FFD_TRACE(FFD_ACTUAL_DATA_MIRROR_SET,(tU8*)&VtspFileData->u8Data[Vu32Offset+Vu32Byte],FFD_TTFIS_LEN_DATA);
                }/*end if*/
                else
                {
                  FFD_TRACE(FFD_ACTUAL_DATA_MIRROR_SET,(tU8*)&VtspFileData->u8Data[Vu32Offset+Vu32Byte],(tU8)((Vu32Size-Vu32Byte)));
                }/*end else*/
              }/*end for*/
            }/*end if*/
          }/*end for*/   
        }/*end else*/
      }/*end else*/
      close(ViBackupFile);       
      free(VtspFileData);     
      vFFDUnLock(VsShMemFFD);       
    }break;
  default: /*for lint */
    break;
  }/*end switch*/  
  DEV_FFD_s32IOClose();  
}/*end function*/

/* **************************************************FunctionHeaderBegin** *//*
* static void FFD_vTrace(u8 Pu8Kind, u8* Pu8pData, u8 Pu8Len) 
*
* trace output function
*
* @param   Pu8Kind:    kind of level
*          Pu8pData:   pointer of the data, which send
*          Pu8Len:     length of the data, which send
*
* @return  void    
*
* @date    2008-01-10
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void FFD_vTrace(tU8 Pu8Kind, const tU8* Pu8pData, tU8 Pu8Len)
{
  tU8                Vu8LenHeader=FFD_TTFIS_LEN_HEADER; 
  tU8                Vu8Buffer[FFD_TTFIS_LEN_MESSAGE]; 
  TR_tenTraceLevel   VtrLevel;

  /* set kind */
  Vu8Buffer[FFD_TRACE_DATA_BYTE_KIND_INFO] = Pu8Kind;
  /* kind error  */
  if (Pu8Kind <= FFD_LEVEL_IMPORTANT)
  {
    VtrLevel=TR_LEVEL_FATAL;
  }/*end if*/
  else
  {
    VtrLevel=TR_LEVEL_USER_4; 
  }/*end else*/
  /* check param len and data*/
  if ((Pu8pData!=NULL)&&((Pu8Len+Vu8LenHeader) <= sizeof(Vu8Buffer)))
  { /* set error information*/
    memmove(&Vu8Buffer[Vu8LenHeader],(void*)Pu8pData,Pu8Len);
  }/*end if*/
  else Pu8Len=0;
  /*trace*/
  TR_core_uwTraceOut(Pu8Len + Vu8LenHeader, (int)TR_CLASS_LLD_FFD, VtrLevel, Vu8Buffer);  
}/*end function*/


/* **************************************************FunctionHeaderBegin** *//*
* static void FFD_vTraceMoreInfo(u8 Pu8Kind, u8* Pu8pData, u8 Pu8Len) 
*
* trace out two integer value
*
* @param   Pu8Kind:    kind of level
*          PiData1:    data1
*          PiData2:    data2
*
* @date    2008-01-10
*
* @date    2007-07-03
*
* @note
*      
*//* ***********************************************FunctionHeaderEnd******* */
void FFD_vTraceMoreInfo(tU8 Pu8Kind,tU32 Pu32Data1,tU32 Pu32Data2)
{
  tU8  Vu8Len=0;            
  tU8  Vu8Buffer[2*sizeof(tU32)]; 

  memmove(&Vu8Buffer[Vu8Len],&Pu32Data1,sizeof(tU32));
  Vu8Len+=sizeof(tU32);
  memmove(&Vu8Buffer[Vu8Len],&Pu32Data2,sizeof(tU32));
  Vu8Len+=sizeof(tU32);
  FFD_vTrace(Pu8Kind,(tU8*)&Vu8Buffer[0],Vu8Len);
}/*end function*/




