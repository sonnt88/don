#ifndef __AMR_IF_H__
#define __AMR_IF_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "../amrlib/include/typedef.h"
#include "../amrlib/include/dec_if.h"

#define AMRWB_MAGIC_NUMBER "#!AMR-WB\n"

extern const UWord8 block_size[];
UWord32 iDecodeAMR( Word8* en_data, Word8* dec_data, UWord32 size );

#ifdef __cplusplus
}
#endif

#endif //__AMR_IF_H__
