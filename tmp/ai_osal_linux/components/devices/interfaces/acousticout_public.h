
/************************************************************************
| $Id: acousticout.h
|************************************************************************
| FILE:         acousticout.h
| PROJECT:      Paramount
| SW-COMPONENT: Acoustic driver
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header for acousticout.c
|                
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH, Hildesheim (Germany)
| HISTORY:
| Date      | Author / Modification
| --.--.--  | ----------------------------------------
| 07.07.05  | Schubart Bernd, 3SOFT: Initial Revision
|
|************************************************************************
| This file is under RCS control (do not edit the following lines)
| $RCSfile:
| $Revision:  
| $Date:  
|************************************************************************/

#if !defined (ACOUSTICOUT_PUBLIC_HEADER)
   #define ACOUSTICOUT_PUBLIC_HEADER
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/

/** \brief IDs of all devices implemented by the dev_acousticout interface */
enum {
   EN_ACOUSTICOUT_DEVID_SPEECH = 0,     /*!< id of "speech" device */
   EN_ACOUSTICOUT_DEVID_MUSIC = 1,      /*!< id of "music" device */
   EN_ACOUSTICOUT_DEVID_LAST            /*!< last id (used for loops) */
};

/** \brief OEDT testmode iID, ACOUSTICOUT_trTestModeCfg */


/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/
/** \brief OEDT test mode configuration -
used with OSAL_C_S32_IOCTRL_ACOUSTICOUT_SETTIME*/
typedef struct { 
    int   iSizeOfStruct;
    tBool bTestModeIsOn;
    tBool bMono2StereoUpmixIsOn;
} ACOUSTICOUT_trTestModeCfg; 

/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/
tU32 ACOUSTICOUT_u32IOOpen(tS32 s32ID, tCString szName,
								  OSAL_tenAccess enAccess, tPU32 pu32FD,
								  tU16 appid); 
tU32 ACOUSTICOUT_u32IOClose(tS32 s32ID, tU32 u32FD);
tU32 ACOUSTICOUT_u32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun,
									 tS32 s32Arg);
tU32 ACOUSTICOUT_u32IOWrite(tS32 s32ID, tU32 u32FD, tPCS8 pcs8Buffer,
								   tU32 u32Size, tPU32 pu32Written); 


#ifdef __cplusplus
}
#endif
     
#else //#if !defined (ACOUSTICOUT_PUBLIC_HEADER)
#error acousticout_public.h included several times
#endif //#else //#if !defined (ACOUSTICOUT_PUBLIC_HEADER)

