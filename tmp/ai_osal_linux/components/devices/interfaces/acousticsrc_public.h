/************************************************************************
| FILE:         Acousticsrc_public.h
| SW-COMPONENT: AcousticSRC driver
|------------------------------------------------------------------------
/* ******************************************************FileHeaderBegin** *//**
 * @file    Acousticsrc_public.h
 *
 * @brief   This file is the header for the acousticsrc driver
 *
 * @author  Niyatha S Rao, ECF5, RBEI
 *
 * @date		11/10/2012
 *
 * @version	Intial version
 *
 * @note
 *  &copy; 
 *
 *  @history
 *
| Date        | Author / Modification
| --.--.--  | ----------------------------------------
| 11.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
 ******************************************************FileHeaderEnd**********/

#if !defined (ACOUSTICSRC_PUBLIC_HEADER)
   #define ACOUSTICSRC_PUBLIC_HEADER
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/

/** \brief IDs of all devices implemented by the dev_acousticout interface */
enum {
   EN_ACOUSTIC_DEVID_SRC = 0,      /*!< id of "Sample Rate Converter" device */
   EN_ACOUSTIC_DEVID_LAST            /*!< last id (used for loops) */
};

/** \brief OEDT testmode iID, ACOUSTICOUT_trTestModeCfg */


/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/
tS32 ACOUSTICSRC_s32IOOpen(tS32 s32ID, tCString szName,
								  OSAL_tenAccess enAccess, tPU32 pu32FD, tU16 appid); 
tS32 ACOUSTICSRC_s32IOClose(tS32 s32ID, tU32 u32FD);
tS32 ACOUSTICSRC_s32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun,
									 tS32 s32Arg);

#ifdef __cplusplus
}
#endif
     
#else 
#error acousticsrc_public.h included several times
#endif 

