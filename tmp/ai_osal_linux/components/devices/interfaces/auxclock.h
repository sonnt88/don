/************************************************************************
| $Id: auxclock.h,v 1.2 2002/10/21 11:23:48 kos2hi Exp $
|************************************************************************
| FILE:         Auxclock.h
| PROJECT:      BMW L6
| SW-COMPONENT: OSAL IO DEVICE
|------------------------------------------------------------------------
| DESCRIPTION:  Header file for OSAL auxclock device API
|                 
|                
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2002 STMicroelectronics, Agrate (ITALY)
| HISTORY:      
| Date      | Modification               | Author
| --.--.--  | ----------------           | -------, -----
| 20.09.02  | Initial revision           | L.Colombo,----
|
|************************************************************************
| This file is under RCS control (do not edit the following lines)
| $RCSfile: auxclock.h,v $
| $Revision:   1.1  $
| $Date:   Dec 17 2002 00:37:22  $
|************************************************************************/

#if !defined (AUXCLOCK_HEADER)
   #define AUXCLOCK_HEADER
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 
     
#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/
#ifdef EXTERN
  #undef EXTERN
#endif

#ifdef AUXCLOCK_MAIN
 #define EXTERN
#else
 #define EXTERN extern
#endif

     
/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/
/************************************************************************
    AUXCLOCK Device Driver API (see AUXCLOCK.c for function description) 
*************************************************************************/
EXTERN tS32 AUXCLOCK_s32IOOpen(tVoid);   
EXTERN tS32 AUXCLOCK_s32IOClose(tVoid); 
EXTERN tS32 AUXCLOCK_s32IOControl(tS32 s32fun, tS32 s32arg);
EXTERN tS32 AUXCLOCK_s32IORead(OSAL_trIOCtrlAuxClockTicks* prIOAuxData, tU32 u32maxbytes);



#ifdef __cplusplus
}
#endif
     
#else
#error auxclock.h included several times
#endif 
