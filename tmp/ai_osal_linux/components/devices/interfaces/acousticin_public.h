/************************************************************************
| $Id: acousticin.h
|************************************************************************
| FILE:         acousticin.h
| PROJECT:      Paramount
| SW-COMPONENT: Acoustic driver
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header for acousticin.c
|                
|------------------------------------------------------------------------
  | COPYRIGHT:    (c) 2009 Blaupunkt GmbH, Hildesheim (Germany)
| HISTORY:
| Date      | Author / Modification
| --.--.--  | ----------------------------------------*/

#if !defined (ACOUSTICIN_PUBLIC_HEADER)
   #define ACOUSTICIN_PUBLIC_HEADER
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/

/** \brief IDs of all devices implemented by the dev_acousticout interface */
enum {
   EN_ACOUSTICIN_DEVID_SPEECHRECO = 0, /*!< id of "speechreco" stream */
   EN_ACOUSTICIN_DEVID_LAST            /*!< last id (used for loops) */
};

/* Default parameters for the device.
The default configuration in this project after opening the device is:
o PCM sample format: 16 bit signed, CPU endian (=little endian)
o PCM sample rate: 16kHz
o Buffer size: To be determined during integration
o Error thresholds: 0 (disabled) for all errors
o Read operation: Timeout 10s */
#define ACOUSTICIN_C_EN_DEFAULT_PCM_SAMPLEFORMAT         OSAL_EN_ACOUSTIC_SF_S16
#define ACOUSTICIN_C_U32_DEFAULT_PCM_SAMPLERATE          ((tU32)16000)
#define ACOUSTICIN_C_U8_DEFAULT_NUM_CHANNELS_SPEECHRECO  ((tU8)1)
#define ACOUSTICIN_C_U32_DEFAULT_BUFFER_SIZE_PCM         ((tU32)2048)  // 576*2) // TODO: take this from header file!


/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/
tU32 ACOUSTICIN_u32IOOpen(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess,
                          tPU32 pu32FD, tU16 appid ); 
tU32 ACOUSTICIN_u32IOClose(tS32 s32ID, tU32 u32FD);
tU32 ACOUSTICIN_u32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun,
                             tS32 s32Arg);
tU32 ACOUSTICIN_u32IORead(tS32 s32ID, tU32 u32FD, tPS8 ps8Buffer, tU32 u32Size,
                          tPU32 pu32Read); 

#ifdef __cplusplus
}
#endif
     
#else //#ifdef ACOUSTICIN_PUBLIC_HEADER
#error acousticin_public.h included several times
#endif //#else //#ifdef ACOUSTICIN_PUBLIC_HEADER

