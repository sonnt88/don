/************************************************************************
| $Id: acousticecnr_public.h
|************************************************************************
| FILE:         acousticecnr_public.h
| PROJECT:      GEN3
| SW-COMPONENT: Acoustic driver
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header for acousticecnr.c
|                
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2009 Blaupunkt GmbH, Hildesheim (Germany)
| HISTORY:
| Date      | Author / Modification
| --.--.--  | ----------------------------------------*/

#if !defined (ACOUSTICECNR_PUBLIC_HEADER)
   #define ACOUSTICECNR_PUBLIC_HEADER
     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 


#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/

/** \brief IDs of all devices implemented by the dev_acousticout interface */
enum {
   ACOUSTICECNR_DEVID_SPEECHRECO = 0, /*!< id of "speechreco" stream */
   ACOUSTICECNR_DEVID_LAST            /*!< last id (used for loops) */
};

/* Default parameters for the device.
The default configuration in this project after opening the device is:
o PCM sample format: 16 bit signed, CPU endian (=little endian)
o PCM sample rate: 16kHz
o Buffer size: To be determined during integration
o Error thresholds: 0 (disabled) for all errors
o Read operation: Timeout 10s */
#define ACOUSTICECNR_C_EN_DEFAULT_PCM_SAMPLEFORMAT         OSAL_EN_ACOUSTIC_SF_S16
#define ACOUSTICECNR_C_U32_DEFAULT_PCM_SAMPLERATE          ((tU32)16000)
#define ACOUSTICECNR_C_U8_DEFAULT_NUM_CHANNELS_SPEECHRECO  ((tU8)1)
#define ACOUSTICECNR_C_U32_DEFAULT_BUFFER_SIZE_PCM         ((tU32)2048)
#define ACOUSTICECNR_C_U32_DEFAULT_READ_TIMEOUT            10000


/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/
tS32 ACOUSTICECNR_s32IOOpen(tS32 s32ID, tCString szName,
                            OSAL_tenAccess enAccess, tPU32 pu32FD,
                            tU16 appid ); 
tS32 ACOUSTICECNR_s32IOClose(tS32 s32ID, tU32 u32FD);
tS32 ACOUSTICECNR_s32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun,
                               tS32 s32Arg);
tS32 ACOUSTICECNR_s32IORead(tS32 s32ID, tU32 u32FD, tPS8 ps8Buffer,
                            tU32 u32Size, tPU32 pu32Read); 

#ifdef __cplusplus
}
#endif
     
#else //#ifdef ACOUSTICECNR_PUBLIC_HEADER
#error acousticecnr_public.h included several times
#endif //#else //#ifdef ACOUSTICECNR_PUBLIC_HEADER
