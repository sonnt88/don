/* *********************************************************************** */
/*  includes                                                               */
/* *********************************************************************** */
#include "OsalConf.h"

#include <sys/socket.h>
#include <netinet/in.h>

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "dev_swc.h"

/* ************************************************************************/
/*  defines                                                               */
/* ************************************************************************/
//#define DEBUG_PRINT(...)    printf(__VA_ARGS__)
#define DEBUG_PRINT(...)

#define DEV_SWC_RECV_THREAD_NAME    "SWC_IO_Thread"
#define DEV_SWC_SEM_NAME            "SWC_SEM"

/* *********************************************************************** */
/*  typedefs enum                                                          */
/* *********************************************************************** */

/* *********************************************************************** */
/*  typedefs struct                                                        */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static variables                                                       */
/* *********************************************************************** */  
static OSAL_tSemHandle          DevSWCHandleSemaphore = OSAL_C_INVALID_HANDLE;
static int                      DevSWCSocketFD = -1;
static int                      DevSWCConnectionFD = -1;

/* *********************************************************************** */
/*  global variables                                                       */
/* *********************************************************************** */
extern trGlobalOsalData *pOsalData;

/* *********************************************************************** */
/*  function prototypes                                                    */
/* *********************************************************************** */

/* *********************************************************************** */
/*  static functions                                                       */
/* *********************************************************************** */

/* *********************************************************************** */
/*  global functions                                                       */
/* *********************************************************************** */

/******************************************************************************
 *FUNCTION      :DEV_SWC_ReceiveThread
 *
 *DESCRIPTION   :Thread to read from the socket
 *
 *PARAMETER     :Arg    unused
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
void DEV_SWC_ReceiveThread(void *Arg)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;

    (void)Arg;

    DEBUG_PRINT("%s started\n", __func__);
    if(OSAL_s32SemaphoreOpen(DEV_SWC_SEM_NAME, &semhandle) == OSAL_OK)
    {
        while(DevSWCSocketFD != -1)
        {
            if(DevSWCConnectionFD == -1)
            {
                DevSWCConnectionFD = accept(DevSWCSocketFD, NULL, NULL);
                if(DevSWCConnectionFD == -1)
                {
                    OSAL_s32ThreadWait(10);
                }
                else
                {
                    DEBUG_PRINT("Connected!\n");
                }
            }
            else
            {
                static const int    buffsize = 64;
                unsigned char       buff[buffsize];
                tS32                buffbytes;
                tS32                inmsg = FALSE;

                // | 0+1    | 2   | 3     | 4+5+6+7 |
                // | 0xF00D | Len | State | Code    |

                buffbytes = 0;
                for(; ; )
                {
                    tS32    i, numbytes;
                    tS32    msglen;
                    tS32    checkmsg;

                    numbytes = recv(DevSWCConnectionFD, buff + buffbytes, buffsize - buffbytes, 0);

                    if(numbytes <= 0)
                    {
                        // recv failed for some reason - probably the connection was closed
                        DEBUG_PRINT("%s Receive failed with %i\n", __func__, numbytes);
                        if(DevSWCConnectionFD != -1)
                        {
                            close(DevSWCConnectionFD);
                            DevSWCConnectionFD = -1;
                        }
                        break;
                    }
                    DEBUG_PRINT("%s Received: %i Bytes\n", __func__, numbytes);

                    buffbytes += numbytes;
                    for(checkmsg = TRUE; checkmsg == TRUE; )
                    {
                        checkmsg = FALSE;

                        // If there was no message start tag, check if there is one now
                        for(i = 0; inmsg == FALSE && i < buffbytes - 1; i++)
                        {
                            if(*((tU16 *)(&buff[i])) == DEV_SWC_TCP_MSG_START)
                            {
                                // Shift data in buffer - discard any garbage and drop message start tag
                                memmove(buff, buff + i + sizeof(tU16), buffsize - i - sizeof(tU16));
                                buffbytes -= i + sizeof(tU16);
                                inmsg = TRUE;
                                msglen = -1;
                                DEBUG_PRINT("%s Found tag\n", __func__);
                            }
                        }

                        if(inmsg == FALSE)
                        {
                            if(buffbytes > 1)
                            {
                                // There is some garbage in the buffer
                                // Discard garbage but make sure to keep the last byte as it could be the first byte of a message start tag
                                memmove(buff, buff + buffbytes - 1, buffbytes - 1);
                                DEBUG_PRINT("%s Discarded %i bytes of garbage\n", __func__, buffbytes);
                                buffbytes = 1;
                            }
                        }
                        else
                        {
                            // There was a message start tag
                            // Now check if there is enough data available to determine the message's length
                            if(msglen < 0 && buffbytes >= 1)
                            {
                                msglen = buff[0];
                                //if(msglen < 1 || msglen > buffsize)
                                if(msglen != 6)     // For now, there is exactly one kind of message...
                                {
                                    // The length of this message is not reasonable, discard it
                                    inmsg = FALSE;
                                    msglen = -1;
                                    DEBUG_PRINT("%s Found invalid size\n", __func__);
                                }
                            }

                            // Check if there is a complete message available
                            if(inmsg == TRUE && msglen > 0 && buffbytes >= msglen)
                            {
                                // Received at least one complete message
                                OSAL_trSWCKeyCodeInfo   arg;
                                tS32                    argvalid = TRUE;

                                switch(buff[1])
                                {
                                case DEV_SWC_TCP_CMD_PRESS:
                                    // FIXME: Not safe! Might overwrite pOsalData->DevSWCCBArgs with new data before callback is executed!
                                    if(pOsalData->DevSWCKeyState != 0)
                                    {
                                        // There is already a key down and SWC supports only one key at a time
                                        // So send a release message for it first
                                        if(OSAL_s32SemaphoreWait(semhandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                                        {
                                            if(pOsalData->DevSWCCallback != NULL)
                                            {
                                                // Send it out, if someone is interested
                                                OSAL_trSWCKeyCodeInfo    *osal_arg = &pOsalData->DevSWCCBArgs[pOsalData->DevSWCCBNextArg];

                                                osal_arg->u32KeyCode = pOsalData->DevSWCKeyState;
                                                osal_arg->enKeyState = OSAL_EN_SWC_KEY_RELEASED;
                                                u32ExecuteCallback(OSAL_ProcessWhoAmI(), pOsalData->DevSWCCallbackProcID, pOsalData->DevSWCCallback, osal_arg, sizeof(arg));

                                                if(++pOsalData->DevSWCCBNextArg > OSAL_C_DEV_SWC_ARGS_MAX)
                                                    pOsalData->DevSWCCBNextArg = 0;
                                            }
                                            OSAL_s32SemaphorePost(semhandle);
                                        }
                                        DEBUG_PRINT("%s sent a release before press\n", __func__);
                                        pOsalData->DevSWCKeyState = 0;
                                    }
                                    arg.u32KeyCode = *((tU32 *)&buff[2]);
                                    arg.enKeyState = OSAL_EN_SWC_KEY_PRESSED;
                                    pOsalData->DevSWCKeyState |= arg.u32KeyCode;
                                    break;
                                case DEV_SWC_TCP_CMD_RELEASE:
                                    arg.u32KeyCode = *((tU32 *)&buff[2]);
                                    arg.enKeyState = OSAL_EN_SWC_KEY_RELEASED;
                                    pOsalData->DevSWCKeyState &= ~arg.u32KeyCode;
                                    break;
                                default:
                                    argvalid = FALSE;
                                    DEBUG_PRINT("%s Unknown command %02X\n", __func__, buff[1]);
                                    break;
                                }

                                // FIXME: Sanity check for values

                                if(argvalid == TRUE)
                                {
                                    // FIXME: Not safe! Might overwrite pOsalData->DevSWCCBArgs with new data before callback is executed!
                                    if(OSAL_s32SemaphoreWait(semhandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
                                    {
                                        if(pOsalData->DevSWCCallback != NULL)
                                        {
                                            // Send it out, if someone is interested
                                            OSAL_trSWCKeyCodeInfo    *osal_arg = &pOsalData->DevSWCCBArgs[pOsalData->DevSWCCBNextArg];

                                            memcpy(osal_arg, &arg, sizeof(arg));
                                            u32ExecuteCallback(OSAL_ProcessWhoAmI(), pOsalData->DevSWCCallbackProcID, pOsalData->DevSWCCallback, osal_arg, sizeof(arg));

                                            if(++pOsalData->DevSWCCBNextArg >= OSAL_C_DEV_SWC_ARGS_MAX)
                                                pOsalData->DevSWCCBNextArg = 0;
                                            DEBUG_PRINT("%s Message delivered to callback\n", __func__);
                                        }
                                        OSAL_s32SemaphorePost(semhandle);
                                    }
                                }

                                memmove(buff, buff + msglen, buffsize - msglen);
                                inmsg = FALSE;
                                buffbytes -= msglen;
                                msglen = -1;

                                checkmsg = TRUE;
                                DEBUG_PRINT("%s Message processed\n", __func__);
                            }
                        }
                    }
                }
            }
        }
        OSAL_s32SemaphoreClose(semhandle);
        OSAL_s32SemaphoreDelete(DEV_SWC_SEM_NAME);
    }
    DEBUG_PRINT("%s ended\n", __func__);
}



/******************************************************************************
 *FUNCTION      :DEV_SWC_s32IODeviceInit
 *
 *DESCRIPTION   :Initializes the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_SWC_s32IODeviceInit(void)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;
    int             port = 3459;
    tS32            retval = OSAL_E_NOERROR;

    if(OSAL_s32SemaphoreCreate(DEV_SWC_SEM_NAME, &semhandle, 1) != OSAL_OK)
    {
        retval = (tS32)OSAL_u32ErrorCode();
    }
    else
    {
        OSAL_s32SemaphoreClose(semhandle);
    }

    if(retval == OSAL_E_NOERROR)
    {
        OSAL_tIODescriptor  fd = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/LSIM/SWC", OSAL_EN_READONLY);

        // It is not a problem and not an error if access to the registry fails. Defaults will be used.
        if(fd != OSAL_ERROR)
        {
            OSAL_trIOCtrlRegistry   entry;
            tS32                    val;

            entry.pcos8Name = "PORT";
            entry.u32Size   = sizeof(val);
            entry.s32Type   = OSAL_C_S32_VALUE_S32;
            entry.ps8Value  = (tU8 *)&val;
            if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGGETVALUE, (tS32)&entry) != OSAL_ERROR)
                port = (int)(*(tS32 *)entry.ps8Value);

            OSAL_s32IOClose (fd);
        }

        DevSWCSocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
        if(DevSWCSocketFD == -1)
        {
            DEBUG_PRINT("socket failed\n");
            retval = OSAL_ERROR;
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        int     flags = fcntl(DevSWCSocketFD, F_GETFL, 0);
        fcntl(DevSWCSocketFD, F_SETFL, flags | O_NONBLOCK);
    }

    if(retval == OSAL_E_NOERROR)
    {
        struct sockaddr_in      sockaddr;

        memset(&sockaddr, 0, sizeof(sockaddr));

        sockaddr.sin_family = AF_INET;
        sockaddr.sin_port = htons(port);
        sockaddr.sin_addr.s_addr = INADDR_ANY;

        if(bind(DevSWCSocketFD, (struct sockaddr *)&sockaddr, sizeof(sockaddr)) == -1)
        {
            DEBUG_PRINT("bind failed\n");
            retval = OSAL_ERROR;
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        if(listen(DevSWCSocketFD, 0) == -1)
        {
            DEBUG_PRINT("listen failed\n");
            retval = OSAL_ERROR;
        }
        else
        {
            DEBUG_PRINT("Listener opened for port %i\n", port);
        }
    }

    if(retval == OSAL_E_NOERROR)
    {
        OSAL_trThreadAttribute  thrdattr;

        thrdattr.szName         = DEV_SWC_RECV_THREAD_NAME;
        thrdattr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
        thrdattr.s32StackSize   = 4096;
        thrdattr.pfEntry        = DEV_SWC_ReceiveThread;
        thrdattr.pvArg          = NULL;

        if(OSAL_ThreadSpawn((OSAL_trThreadAttribute* )&thrdattr) == OSAL_ERROR)
        {
            retval = OSAL_ERROR;
        }
    }

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_SWC_s32IODeviceRemove
 *
 *DESCRIPTION   :Removes the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :none
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
void DEV_SWC_s32IODeviceRemove(void)
{
    OSAL_tSemHandle semhandle = OSAL_C_INVALID_HANDLE;

    if(OSAL_s32SemaphoreOpen(DEV_SWC_SEM_NAME, &semhandle) == OSAL_OK)
    {
        if(OSAL_s32SemaphoreWait(semhandle, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
        {
            pOsalData->DevSWCCallbackProcID = 0;
            pOsalData->DevSWCCallback = NULL;

            if(DevSWCConnectionFD != -1)
            {
                shutdown(DevSWCConnectionFD, SHUT_RDWR);
                DevSWCConnectionFD = -1;
            }

            if(DevSWCSocketFD != -1)
            {
                close(DevSWCSocketFD);
                DevSWCSocketFD = -1;
            }
            OSAL_s32SemaphorePost(semhandle);
            OSAL_s32SemaphoreClose(semhandle);
        }
    }
}



/******************************************************************************
 *FUNCTION      :DEV_SWC_s32IOOpen
 *
 *DESCRIPTION   :Open the device
 *
 *PARAMETER     :enAccess   Desired access, one of
 *                              OSAL_EN_WRITEONLY
 *                              OSAL_EN_READWRITE
 *                              OSAL_EN_READONLY
 *
 *RETURNVALUE   :tS32   device handle on success or
 *                      OSAL_ERROR/OSAL_E_ALREADYOPENED in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_SWC_s32IOOpen(OSAL_tenAccess enAccess)
{
    tS32    retval = OSAL_E_NOERROR;

    if((enAccess != OSAL_EN_READONLY) && (enAccess != OSAL_EN_WRITEONLY) && (enAccess != OSAL_EN_READWRITE))
    {
        retval = OSAL_ERROR;
    }
    else if(pOsalData->DevSWCOpenCount > 0)
    {
        retval = OSAL_E_ALREADYOPENED;
    }
    else
    {
        if(OSAL_s32SemaphoreOpen(DEV_SWC_SEM_NAME, &DevSWCHandleSemaphore) != OSAL_OK)
        {
            retval = OSAL_ERROR;
        }
        else
        {
            pOsalData->DevSWCOpenCount++;
        }
    }

    // FIXME: Return a handle
    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_SWC_s32IOClose
 *
 *DESCRIPTION   :Close the device
 *
 *PARAMETER     :none
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_SWC_s32IOClose(void)
{
    tS32    retval = OSAL_E_NOERROR;

    if(pOsalData->DevSWCOpenCount <= 0)
    {
        retval = OSAL_ERROR;
    }
    else
    {
        if(DevSWCHandleSemaphore != OSAL_C_INVALID_HANDLE)
        {
            pOsalData->DevSWCOpenCount--;
            OSAL_s32SemaphoreClose(DevSWCHandleSemaphore);
            DevSWCHandleSemaphore = OSAL_C_INVALID_HANDLE;
        }
    }

    return retval;
}



/******************************************************************************
 *FUNCTION      :DEV_SWC_s32IOControl
 *
 *DESCRIPTION   :Control the device
 *
 *PARAMETER     :s32Fun     Function to execute
 *               s32Arg     Argument to function, meaning depends on s32Fun
 *
 *RETURNVALUE   :tS32   OSAL_E_NOERROR on success or
 *                      OSAL_ERROR in case of an error
 *
 *HISTORY:      :Created by FAN4HI 2012 01 05
 *****************************************************************************/
tS32 DEV_SWC_s32IOControl(tS32 s32Fun, tS32 s32Arg)
{
    tS32    retval = OSAL_E_NOERROR;

    if(DevSWCHandleSemaphore == OSAL_C_INVALID_HANDLE)
    {
        retval = OSAL_E_UNKNOWN;
    }

    if(retval == OSAL_E_NOERROR)
    {
        switch(s32Fun)
        {
        case OSAL_C_S32_IOCTRL_SWC_REGISTER_CALLBACK:
            if(OSAL_s32SemaphoreWait(DevSWCHandleSemaphore, OSAL_C_TIMEOUT_FOREVER) == OSAL_OK)
            {
                pOsalData->DevSWCCallbackProcID = OSAL_ProcessWhoAmI();
                pOsalData->DevSWCCallback = (OSAL_tpfSWCCallbackFn)s32Arg;
                OSAL_s32SemaphorePost(DevSWCHandleSemaphore);
            }
            break;
        case OSAL_C_S32_IOCTRL_SWC_GET_KEY_CODE:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                *((tU32 *)s32Arg) = pOsalData->DevSWCKeyState;
            break;
        case OSAL_C_S32_IOCTRL_DEV_SWC_DEVICE_VERSION:
            if(s32Arg == 0)
                retval = OSAL_E_INVALIDVALUE;
            else
                *((tU32 *)s32Arg) = OSAL_SWC_IO_CTRL_VERSION;
            break;
        default:
            retval = OSAL_E_NOTSUPPORTED;
            break;
        }
    }

    return retval;
}
