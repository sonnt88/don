/************************************************************************************************
* 
* \file          dev_adr3ctrl.c
* \brief         This file implements the application interfaces to access the ADR3CTRL.
*
* \project       IMX6 (ADIT GEN3)
*
* \authors       swm2kor
*
* COPYRIGHT      (c) 2013 Bosch CarMultimedia GmbH
*
* HISTORY      :
*-------------------------------------------------------------------------------------------------------------------------
* Date         |        Modification                            | Author & comments
*--------------|------------------------------------------------|---------------------------------------------------------
*              |  Download Interface                            |  Thomae Matthias (CM-AI/PJ-CF31)
* 25.Aug.2013  |  ADR interface initial Version                 |  Madhu Sudhan Swargam (RBEI/ECF5)
* 24.Dec.2013  |  Reset Concurrency Update                      |  Madhu Sudhan Swargam (RBEI/ECF5)
* 13.Jan.2013  |  DEADINIT state update for SPI MOD             |  Madhu Sudhan Swargam (RBEI/ECF5)
* 03.Mar.2014  |  Moving teDevAdr3_State to dev_adr3ctrl.h file |  Madhu Sudhan Swargam (RBEI/ECF5)
* 20.May.2014  |  To activate ADR3CTRL_SCC Comm                 |  Madhu Sudhan Swargam (RBEI/ECF5)
* 05.Jun.2014  |  To ignore ADR3_SCC messages till Component    |
*              |  status is received                            |  Madhu Sudhan Swargam (RBEI/ECF5)
* 04.Aug.2014  |  To support for LSIM                           |  Madhu Sudhan Swargam (RBEI/ECF5)
* 07.Aug.2014  |  ADR3ctrl resource close at the close operation|  Madhu Sudhan Swargam (RBEI/ECF5)
* 18.Mar.2015  |  ADR3Ctrl open update CFG3-1132                |  Madhu Sudhan Swargam (RBEI/ECF5)
* 04.Aug.2015  |  Named gpio is introduced, if not available    |
*              |  default SPI2_req GPIO value is 24             |  Madhu Sudhan Swargam (RBEI/ECF5)
* 01.Jan.2016  |  Add check in reset request in download mode   |  Madhu Sudhan Swargam (RBEI/ECF5)
* 12.Feb.2016  |  Fix for CMG3GB-2930.                          |
*              |  Clear reg gpio state after dwn-reset release  |  Madhu Sudhan Swargam (RBEI/ECF5)
* 14.Mar.2016  |  gethostbyname() is not multi thread safe use  |
*              |  getaddrinfo()                                 |  Madhu Sudhan Swargam (RBEI/ECF5)
* 26.May.2015  | PSARCCB-8674                                   |
*              | gpio poll event is cleared before every Write  |  Madhu Sudhan Swargam (RBEI/ECF5)
* 26.May.2015  | PSARCCB-10095: Trace level change                                |
*              | Null exception handling in adr_process_attach                    |  Madhu Sudhan Swargam (RBEI/ECF5)
* 26.May.2015  | PSARCCB-10252: request ADR version a second time forces a reset  |
*              | Clean semaphore and shared Memory                                |  Madhu Sudhan Swargam (RBEI/ECF5)
* 18.Nov.2016  | Remove error handling during DEAD and ALIVE states for same state|  Madhu Sudhan Swargam (RBEI/ECF5)
* --------------------------------------------------------------------------------------------------------------------
|---------------------------------------------------------------------------------------------------------------------*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <poll.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "ostrace.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "OsalConf.h"
#include "osansi.h"
#include "dev_adr3ctrl.h"
#include <netdb.h>
#include "inc_ports.h"
#include "inc.h"
#include "dgram_service.h"
#include "inc_scc_adr3ctrl.h"
#include "Linux_osal.h"
/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/* ------------------------------------------------------------------------- */


/* ADR3Ctrl Resource*/
//#define DEV_ADR3CTRL_V850INTERFACE_INACTIVE       // Definition will activate the usage of GPIO Port extender instead of ADR3_SCC Comm
#define DEV_ADR3CTRL_SH_NAME                       ("ADR3Ctrl_SM")
#define DEV_ADR3CTRL_LOCK_NAME                     ("ADR3Ctrl_Lock")
/* Dev_ADR3Ctrl Linux Handling Thread*/
#define DEV_ADR3CTRL_LI_HNDL_THRD_NM               ("ADR3Ctrl_LinuxHandleThrd")
#define DEV_ADR3CTRL_LI_HNDL_THRD_PRIOR            (OSAL_C_U32_THREAD_PRIORITY_HIGHEST)
#define DEV_ADR3CTRL_LI_HNDL_THRD_STCKSZ           (2048)
#define DEV_ADR3CTRL_LI_HNDLNG_MQ_NM               ("ADR3Ctrl_MQ")
#define DEV_ADR3CTRL_LI_HNDL_UNKNWN_MSG            0X00000000ul
#define DEV_ADR3CTRL_LI_HNDL_RST_ACT_NOR_MSG       0x00000001ul
#define DEV_ADR3CTRL_LI_HNDL_RST_REL_NOR_MSG       0x00000002ul
#define DEV_ADR3CTRL_LI_HNDL_RST_ACT_DWNLD_MSG     0x00000004ul
#define DEV_ADR3CTRL_LI_HNDL_RST_REL_DWNLD_MSG     0x00000008ul
#define DEV_ADR3CTRL_LI_HNDL_ADR3SCC_ACTV_MSG      0x00000010ul
#define DEV_ADR3CTRL_LI_HNDL_ADR3SCC_INACTV_MSG    0x00000020ul
#define DEV_ADR3CTRL_LI_HNDL_THREAD_EXIT           0x00000040ul

/* Thread Attributes and Resources Used*/
#define DEV_ADR3CTRL_SCC_DATA_SEND_THRD_NM         ("ADR3Ctrl_SCC_DataSendThread")
#define DEV_ADR3CTRL_SCC_DATA_SEND_THRD_PRIOR      (OSAL_C_U32_THREAD_PRIORITY_HIGHEST)
#define DEV_ADR3CTRL_SCC_DATA_SEND_THRD_STCKSZ     (2048)

#define DEV_ADR3_SCC_DATA_SEND_MQ_NAME             ("ADR3Ctrl_SCC_DataSend_MQ")
#define DEV_ADR3CTRL_SCC_DATASEND_RESET_MSG        0x00000002ul
#define DEV_ADR3CTRL_SCC_DATASEND_SPIMOD_MSG       0x00000004ul
#define DEV_ADR3CTRL_SCC_DATASEND_NORMALMOD_MSG    0x00000008ul
#define DEV_ADR3CTRL_SCC_DATASEND_THREAD_EXIT      0x00000010ul
/* ADR3 SCC Thread Priority Atributes*/
#define DEV_ADR3CTRL_SCC_DATA_READ_THRD_NM         ("ADR3Ctrl_SCC_DataReadThread")
#define DEV_ADR3CTRL_SCC_DATA_READ_THRD_PRIOR      (OSAL_C_U32_THREAD_PRIORITY_HIGHEST)
#define DEV_ADR3CTRL_SCC_DATA_READ_THRD_STCKSZ     (2048)


#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
//#define DEV_ADR3CTRL_FAKE_DEVICE_SIMULATION
/* Socket connection Host names used for INC Communication */
#ifdef DEV_ADR3CTRL_FAKE_DEVICE_SIMULATION
#define SCC_INC_NODE_LOCAL_NAME                    "fake0-local" 
#define SCC_INC_NODE_NAME                          "fake0" 
#else
#define SCC_INC_NODE_LOCAL_NAME                    "scc-local" 
#define SCC_INC_NODE_NAME                          "scc" 
#endif 
#define ADR3_SCC_COMM_BUFFR_LENGHT                 3  

#endif


#define DEV_ADR3CTRL_MAX_DEVC_HNDLR         10
 /* Definitions for Download */
//#define ADR3CTRL_VERBOSE_TRACE
//#define ADR3CTRL_TRACE_STDERR /* trace to stderr instead of TTFIS */

#define ADR3CTRL_TRACE_MAX_CHARS                   150
#define ADR_IFNAME_DEVICE_TREE_PATH                "/proc/device-tree/board-configuration/inc/adr3/interface-name"
#define SPI_DEVICE_PATH                            "/dev/spidev_adr3"
#ifdef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
#define ADR_RESET_GPIO_VALUE_PATH                  "/sys/devices/virtual/boardcfg/boardcfg/embeddedradio-gpio-adr-reset/value"
#define ADR_BOOTSEL0_GPIO_VALUE_PATH               "/sys/devices/virtual/boardcfg/boardcfg/embeddedradio-gpio-adr-boot-select0/value"
#define ADR_BOOTSEL1_GPIO_VALUE_PATH               "/sys/devices/virtual/boardcfg/boardcfg/embeddedradio-gpio-adr-boot-select1/value"
#define U32_MAX_DECIMALS                           11 /* max nof decimals of tU32 + 1 */
#endif

#define ADR_REQ_GPIO_VALUE_PATH     "/sys/devices/virtual/boardcfg/boardcfg/embeddedradio-gpio-adr-req/value"
#define ADR_REQ_GPIO_EDGE_PATH      "/sys/devices/virtual/boardcfg/boardcfg/embeddedradio-gpio-adr-req/edge"
#define CMD_REQ_GPIO_EXPORT         "echo -n embeddedradio-gpio-adr-req > /sys/devices/virtual/boardcfg/boardcfg/export_named"
#define CMD_REQ_GPIO_UNEXPORT       "echo -n embeddedradio-gpio-adr-req > /sys/devices/virtual/boardcfg/boardcfg/unexport_named"
#define ADR_REG_NAMED_GPIO_PATH     "/proc/device-tree/board-configuration/exported-gpios/embeddedradio-gpio-adr-req/export-named"

#define ADR_REQ_GPIO_VALUE_PATH_DEFAULT    "/sys/class/gpio/gpio24/value"       
#define ADR_REQ_GPIO_EDGE_PATH_DEFAULT     "/sys/class/gpio/gpio24/edge"        
#define CMD_REQ_GPIO_EXPORT_DEFAULT        "echo 24 > /sys/class/gpio/export"   
#define CMD_REQ_GPIO_UNEXPORT_DEFAULT      "echo 24 > /sys/class/gpio/unexport" 

#define CMD_SPIDEV_START                           "/sbin/modprobe -r -q spidev; /sbin/modprobe spidev bufsiz=20480"
#define CMD_SPIDEV_STOP                            "/sbin/modprobe -r spidev"
//#define ADR3CTRL_HW_GPIO_VALUE_PATH                "/sys/devices/virtual/boardcfg/boardcfg/embeddedradio-gpio-hw-mute/value"

#define INT_MAX_DECIMALS                           10 /* max nof decimals of tInt + 1 */
#define C_MAX_STATUS_LENGTH                        1000
#define ADR3CTRL_SCC_INIT_RESPONSE_TIMEOUT         1000
/*************************************************************************
* Driver specific structure definition
*-----------------------------------------------------------------------*/

/**ADR3 State Machine Events*/
typedef enum
{
   ADR3_ST_EVNT_ACTIVE,            // Triggered at start-up for ADR3_STATUS_ACTIVE response form ADR3_SCC
   ADR3_ST_EVNT_INACTIVE,          // Triggered at start-up for ADR3_STATUS_INACTIVE response form ADR3_SCC
   ADR3_ST_EVNT_RESET_ACTIVE,      // Triggered for ADR3_SCC response ADR3_RESET_STATUS_ACTIVE Normal and Download mode
   ADR3_ST_EVNT_SPI_RESET_REL,     // Triggered for ADR3_SCC response ADR3_RESET_STATUS_RELEASE Download mode
   ADR3_ST_EVNT_NRML_RESET_REL,      // Triggered for ADR3_SCC response ADR3_RESET_STATUS_RELEASE Normal mode
   ADR3_ST_EVNT_IOCNTRL_DEAD_INIT
   
}teDevADR3Ctrl_SMEvent;

/** Resources Used for ADR3 interface*/
typedef enum
{
   ADR3CTRL_RESOURCE_RELEASE_LI_HNDL_TX_MQ,
   ADR3CTRL_RESOURCE_RELEASE_SCC_SOCKET,
   ADR3CTRL_RESOURCE_RELEASE_SCC_DGRAM,
   ADR3CTRL_RESOURCE_RELEASE_SH_MEM
}teDevAdr3_Resources;

#ifdef ADR3CTRL_TRACE_STDERR
#define ADR3CTRL_TRACE(level, fmt, args...)\
   fprintf(stderr, "%s: " fmt "\n", __func__, ## args)
#else
#define ADR3CTRL_TRACE(level, fmt, args...)\
   DEV_ADR3CTRL_vTraceString(level, "%s: " fmt, __func__, ## args)
#endif

typedef enum
{
   BOOT_MODE_NORMAL = 0,
   BOOT_MODE_SPI = 1
} enAdrBootMode;

typedef void (*adr3_tVPCallBackFunc) (tU32 u32Data);

typedef struct
{
  tBool fOpen;        /**< TRUE -> device is open, FALSE -> device is closed */
  adr3_tVPCallBackFunc  pvStateCallback; /* Hold the CallBack Information of the Application */
  OSAL_tProcessID pid;    /* Holds the CallBacks Process ID */
}trDevAdr3_InstanceState;

typedef struct 
{
trDevAdr3_InstanceState    rDevAdr3_InstanceState[DEV_ADR3CTRL_MAX_DEVC_HNDLR];  /* Used to store the App Specific Data  */
teDevAdr3_State            eDevAdr3_State;                            /*Used to store the Current State of ADR3 state machine */
tU32                       u32ADR3ReadTimeout;                            /* Used for ADR3 READ time-out*/ 
trOsalLock                 rADR3Ctrl_Lock;
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
tBool                      bDevAdr3Ctrl_LxHndlTxActv;		   /* Variable used to Keep the ADR3 Worker Thread Alive*/
tBool                      bDevAdr3Ctrl_SCC_DataReadTxActv;           /* Variable used to Keep the SCC data read Thread Alive*/
tS32                       s32DevADR3CtrlSCC_INCSocketFD;                 /* Socket used for ADR3_SCC Communication */
sk_dgram                   *hDevADR3CtrlSCC_dgram;               /* Dgram struct used for ADR3_SCC Communication */ 
tBool                      bDevAdr3Ctrl_SCC_CompntActv;
tBool                      bDrvRRsurceInit;
#endif
tU32                       bDevAdr3Ctrl_SCCDataSendTxActv;
/* Used to handle  how many process are using the adr3ctrl services 
   Open count within a multiple process                            */
tU32                       u32ADR3MultiProcOpenCounter;
/* Pipe used for thread shut down for data-read thread*/
tInt                       UnblkPipeFdSet[2];
/* To know whether adr3ctrl_scc responded to component status*/
tBool                      bSCC_Comm_initOkay;
/* To get the driver initialization result*/
tBool                      bLinuxDriverInit;
/* To Store download process information */
tChar                      cDwnldProcNm[256];
/* To track the first process who spawn the driver threads */
OSAL_tProcessID            adr3firstProcPid;
tBool                      bSpiGpioActv;
} trADR3CtrlData;
/*************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/
static struct pollfd PollFdSet[1] = {0};
static int SpiDevFd = 0;
static int WriteCnt = 0;
/* To get the status of Process Initialization */
tBool                      bDevAdr3_Init_Active = FALSE;
static trADR3CtrlData* pADR3CtrlData;
/* Handler is SPecific to Each process as the MQ is used in Different Process Context*/
/* Event used for State change Request post */
OSAL_tMQueueHandle          hDevAdr3CtrlSCCDataSend_MQ = OSAL_C_INVALID_HANDLE;
OSAL_tShMemHandle           hDevAdr3CtrlShrdMemryHndl = OSAL_ERROR;
/* Message queue used for handling Socket read thread and ADR3Ctrl thread */
OSAL_tMQueueHandle          hDevAdr3_SCC_DataReadMQHndlr = OSAL_C_INVALID_HANDLE;           
/* Variable is used to Check whether the Resource is already initialized or not*/         
enAdrBootMode               enPreviousMode;
/*************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
static tVoid DEV_ADR3CTRL_vExcuteCallback(teDevAdr3_State adr3PresentState);
static tVoid DEV_ADR3CTRL_vStateMachine(teDevADR3Ctrl_SMEvent machine_event);
static tVoid DEV_ADR3CTRL_vReleaseResource(teDevAdr3_Resources u32Resource);
static tU32  DEV_ADR3CTRL_u32ADR3CommDown(tVoid);
static tU32  DEV_ADR3CTRL_u32ADR3CommUp(tVoid);
static tVoid DEV_ADR3CTRL_vHndlADR3Active(tVoid);
static tVoid DEV_ADR3CTRL_vHndlADR3InActive(tVoid);
static tVoid DEV_ADR3CTRL_vHndlResetActveNormal(tVoid);
static tVoid DEV_ADR3CTRL_vHndlResetRelNormal(tVoid);
static tVoid DEV_ADR3CTRL_vHndlResetActveDwnld(tVoid);
static tVoid DEV_ADR3CTRL_vHndlResetRelDwnld(tVoid);
static tVoid DEV_ADR3CTRL_vLiHndlngThread(tPVoid pvArg);
static tVoid DEV_ADR3CTRL_SCCDataSendThread(tPVoid pvArg);
static tBool DEV_ADR3CTRL_bLinuxResource_Init(tVoid);
static tS32  DEV_ADR3CTRL_s32SCC_CommSocketSetup(tVoid);
static tU32  DEV_ADR3CTRL_u32GetMsgPostCommand(char cMsgID,char cStatus,char cMode);
static tVoid DEV_ADR3CTRL_vSCC_DataReadThread(tPVoid pvArg);
static tBool DEV_ADR3CTRL_bSCC_Comm_Init(tVoid);
static tBool DEV_ADR3CTRL_bCheckIntialSequence(tVoid);
static tBool DEV_ADR3CTRL_bOpenOperations(tVoid);
static tBool DEV_ADR3CTRL_bProcResurcInit(tVoid);
static tBool DEV_ADR3CTRL_bData_ResourceInit(tVoid);
static tVoid DEV_ADR3CTRL_vExcute_SCC_MSG(tU32 u32MsgRequest);
static tU32 DEV_ADR3CTRL_exitDnl(tVoid);
static tU32 DEV_ADR3CTRL_initDnl(tVoid);
static tU32 DEV_ADR3CTRL_setNetDevState(tBool activate);
tVoid DEV_ADR3CTRL_vTraceString(TR_tenTraceLevel enTraceLevel,tPCChar pchFormat, ...);
#ifdef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
static tU32 DEV_ADR3CTRL_fileOpenWriteU32Close(const tPChar path,const tU32 value);
static tU32 DEV_ADR3CTRL_resetADR(enAdrBootMode mode);
static tBool DEV_ADR3CTRL_bGpioPortextndrDevInit(tVoid);
#endif
static tS32 DEV_ADR3CTRL_s32ResetIOCntrlHndl();
static tS32 DEV_ADR3CTRL_s32SPIModIOCntrlHndl();
static tS32 DEV_ADR3CTRL_s32NORModIOCntrlHndl();
static tVoid DEV_ADR3CTRL_vProcResurcClose(tBool bMqResource_delete);
static tU32 DEV_ADR3CTRL_fileClose(const tInt fd);
static tBool DEV_ADR3CTRL_bDrvrRsurceInitChk(tVoid);
static tVoid DEV_ADR3CTRL_vLxHndlThreadExit(tVoid);
static tVoid DEV_ADR3CTRL_vDataSendThreadExit(tVoid);
static tVoid DEV_ADR3CTRL_vDataReadThreadExit(tVoid);
static tVoid DEV_ADR3CTRL_vDriverThreadCleanup(tVoid);
static tVoid DEV_ADR3CTRL_vCloseResource(tVoid);
static tBool DEV_ADR3CTRL_DriverCntrlThreadspawn(tVoid);
static tU32 DEV_ADR3CTRL_systemCall(const tPChar cmd);
static tU32 DEV_ADR3CTRL_fileReadInt(const tInt fd,tPInt pValue);
static tU32 DEV_ADR3CTRL_fileReadStr(const tInt fd,const tInt bufSize,
                                     tPChar pBuf,tPInt pReadBytes);
static tU32 DEV_ADR3CTRL_fileWriteStr(const tInt fd,const tPCChar pStr);
static tU32 DEV_ADR3CTRL_fileOpenReadStrClose(const tPChar path,const tInt bufSize,
                                              tPChar pBuf,tPInt pReadBytes);
static tU32 DEV_ADR3CTRL_fileOpenWriteStrClose(const tPChar path,const tPCChar pStr);
static tU32 DEV_ADR3CTRL_readNetDevName(tPChar ifname,tInt bufSize);
static tBool DEV_ADR3CTRL_bDriverInit(tVoid);
/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vExcuteCallback
*
* PARAMETER    : teDevAdr3_State adr3PresentState 
*
* RETURNVALUE  : Void
*                
*
* DESCRIPTION  :  Execute the registered and open(i.e Valid ) CallBacks only with current ADR3 state
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vExcuteCallback(teDevAdr3_State adr3PresentState)
{
   tU32 u32count;
   trDevAdr3_InstanceState *prInstanceState = NULL;
   tOsalMbxMsg rMsg;
   OSAL_tMQueueHandle  Mq;
   OSAL_tProcessID idCurrPID = OSAL_ProcessWhoAmI();
   /* Execute callback for all ADR3 registered Devices  */
   for(u32count=0;u32count<DEV_ADR3CTRL_MAX_DEVC_HNDLR;u32count++)
   {
      prInstanceState = &(pADR3CtrlData->rDevAdr3_InstanceState[ u32count]);
      /*  Check for all valid CallBacks that are to be executed 
      The application which close the /dev/adr3ctrl can't receive  
      CallBack even though they are registered for call back       */
      if((prInstanceState->fOpen == TRUE) 
          && 
         (prInstanceState->pvStateCallback != NULL)
        )
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                   "CallBack Execution for Proc ID=%d,ADR3 State:=%d",
                                   prInstanceState->pid,
                                   (tS32)adr3PresentState
                                  );
         /* Check for the Valid PID */ 
         if(prInstanceState->pid != idCurrPID) 
         {
            /* send callback to main callback handler message queue for dispatching */
            rMsg.ru32ParmCBMsg.rHead.Cmd    = (tU32)MBX_CB_U32PARM;
            rMsg.ru32ParmCBMsg.u32Pid       = (tU32)prInstanceState->pid;
            rMsg.ru32ParmCBMsg.u32CallFun   = (tU32)prInstanceState->pvStateCallback;
            rMsg.ru32ParmCBMsg.u32Param1    = (tU32)adr3PresentState;
            /* Get the Message Queue Handler for the Respective Process */
            Mq = GetPrcLocalMsgQHandle((tU32)prInstanceState->pid); 
            if((Mq != 0) && (prInstanceState->pvStateCallback != NULL))
            {
               /* Send the CallBack information for execution*/
               if(OSAL_ERROR == OSAL_s32MessageQueuePost(Mq,
                                                         (tPCU8)&rMsg,
                                                         sizeof(tOsalMbxMsg),
                                                         0
                                                        )
                  )
               {
                  DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                            "MessageQueuePost Failed line %lu with error =%u",
                                            __LINE__,
                                            OSAL_u32ErrorCode()
                                           );
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            else
            {
               NORMAL_M_ASSERT_ALWAYS();
            }
         }
         else
         {
            /* CallBack execution for the Same Process ID*/
            prInstanceState->pvStateCallback((tU32)adr3PresentState);
         }       
      }
   }
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vStateMachine
*
* PARAMETER    : teDevADR3Ctrl_SMEvent machine_event
*
* RETURNVALUE  : Void
*                
*
* DESCRIPTION  : Handles the State machine of ADR3
*                 1) Receives the state machine event from ADR3 Thread
*                 2) Change the State according to the machine_event and current ADR3 state
*                 3) Call Backs are executed after State change
* HISTORY      :
*------------------------------------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* 18.Nov.2016  |  Remove Normal assert during DEAD and ALIVE state | Madhu Sudhan Swargam
* -----------------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static void DEV_ADR3CTRL_vStateMachine(teDevADR3Ctrl_SMEvent machine_event)
{
   teDevAdr3_State   eCurr_DevAdr3_State = ADR3CTRL_ADR3_STATE_INVALID;  
   switch(pADR3CtrlData->eDevAdr3_State)
   {
      case ADR3CTRL_ADR3_STATE_INIT:
      {   
      /* Change the state Machine to dead or alive depending on ADR3_SCC response  system start-up */
      /*  Assumption: There are no DNL state change request from Application at start-up */
         if(ADR3_ST_EVNT_NRML_RESET_REL == machine_event)     
         {
            eCurr_DevAdr3_State = ADR3CTRL_ADR3_STATE_ALIVE;
         }
         else if(ADR3_ST_EVNT_RESET_ACTIVE == machine_event)
         {
         /*Reset active occurred in the system start-up  */
            eCurr_DevAdr3_State = ADR3CTRL_ADR3_STATE_DEAD; 
         }
         else
         {
          /* Others state change are not expected */
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "Unexpected %d state event change when system is in %d state",
                                       machine_event,
                                       ADR3CTRL_ADR3_STATE_INIT
                                     );
            NORMAL_M_ASSERT_ALWAYS();
         }
         break;
      }
      case ADR3CTRL_ADR3_STATE_DEAD:
      {
         if(ADR3_ST_EVNT_NRML_RESET_REL == machine_event)
         {
         /*  Occurs in Normal Mode Reset Release,system enters into State Active*/
            eCurr_DevAdr3_State = ADR3CTRL_ADR3_STATE_ALIVE;
         }
         else if(ADR3_ST_EVNT_SPI_RESET_REL == machine_event)
         {
         /* Occurs in DNL Mode Reset Release,system enters into State DNL */
            eCurr_DevAdr3_State = ADR3CTRL_ADR3_STATE_DNL;
         }
         else if(ADR3_ST_EVNT_RESET_ACTIVE == machine_event)
         {
            /* Two Reset active are expected from adr3ctrl_scc */
            /* Ignore this event */
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                      "Received Reset active during Dead state"
                                      );
         }
         else
         {
            /* Others state change are not expected */
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "Unexpected %d state event change when system is in %d state",
                                       machine_event,
                                       ADR3CTRL_ADR3_STATE_DEAD
                                     );
            NORMAL_M_ASSERT_ALWAYS();
         }
         break;
      }
      case ADR3CTRL_ADR3_STATE_ALIVE:
      {
         if(ADR3_ST_EVNT_RESET_ACTIVE == machine_event)
         { 
         /* Occurs in Reset active response in normal mode 
            System enters into Dead state                  */
            eCurr_DevAdr3_State = ADR3CTRL_ADR3_STATE_DEAD;            
         }
         else if(ADR3_ST_EVNT_IOCNTRL_DEAD_INIT == machine_event)
         { 
            eCurr_DevAdr3_State = ADR3CTRL_ADR3_STATE_DEADINIT;            
         }
         else if(ADR3_ST_EVNT_NRML_RESET_REL == machine_event)
         {
            /* Two state active are expected from adr3ctrl_scc */
            /* Ignore this event */
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                      "Received Reset rel during active state"
                                     );
         }
         else
         {
           /* Others state change are not expected */
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "Unexpected %d state event change when system is in %d state",
                                       machine_event,
                                       ADR3CTRL_ADR3_STATE_ALIVE
                                     );
            NORMAL_M_ASSERT_ALWAYS();
         }
         break;
      }
      case ADR3CTRL_ADR3_STATE_DEADINIT:
      {
         if(ADR3_ST_EVNT_RESET_ACTIVE == machine_event)
         {
            /* Occurs in Reset active response in normal mode 
         System enters into Dead state                  */
            eCurr_DevAdr3_State = ADR3CTRL_ADR3_STATE_DEAD;   
         }
         else
         {
           /* Others state change are not expected */
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "Unexpected %d state event change when system is in %d state",
                                       machine_event,
                                       ADR3CTRL_ADR3_STATE_DEADINIT
                                     );
            NORMAL_M_ASSERT_ALWAYS();
         }
         break;
      }
      case ADR3CTRL_ADR3_STATE_DNL:
      {
         if(ADR3_ST_EVNT_RESET_ACTIVE == machine_event)
         {
         /* Occurs in Reset active response in Download mode 
         System enters into Dead state                 */
            eCurr_DevAdr3_State = ADR3CTRL_ADR3_STATE_DEAD; 
         }
         else
         {
           /* Others state change are not expected */
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "Unexpected %d state event change when system is in %d state",
                                       machine_event,
                                       ADR3CTRL_ADR3_STATE_DNL
                                     );
            NORMAL_M_ASSERT_ALWAYS();
         }
         break;
      }
      default:
         NORMAL_M_ASSERT_ALWAYS();
         break;
   }
   if((ADR3CTRL_ADR3_STATE_INVALID != eCurr_DevAdr3_State)
      &&
      (ADR3CTRL_ADR3_STATE_DEADINIT != eCurr_DevAdr3_State)
     )
   {
      pADR3CtrlData->eDevAdr3_State=eCurr_DevAdr3_State;
      DEV_ADR3CTRL_vExcuteCallback(pADR3CtrlData->eDevAdr3_State);
   }
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vReleaseResource
*
* PARAMETER    : teDevAdr3_Resources u32Resource
*
* RETURNVALUE  : void
*                
*
* DESCRIPTION  : Handles the ADR3 Resources removal
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vReleaseResource(teDevAdr3_Resources u32Resource)
{
   switch (u32Resource)
   {
      /* Release ADR3 SCC Socket*/
      case ADR3CTRL_RESOURCE_RELEASE_SCC_SOCKET :
      {
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
         /* Check for the Valid Handler */
         if(OSAL_ERROR !=  pADR3CtrlData->s32DevADR3CtrlSCC_INCSocketFD)  
         {
            if(0 != close(pADR3CtrlData->s32DevADR3CtrlSCC_INCSocketFD))
            {
               tInt errsv = errno;
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                         "ADR3CTRL-SCC socket close Failed with Error =%d",
                                         errsv
                                        );
            }
            else 
            {
               pADR3CtrlData->s32DevADR3CtrlSCC_INCSocketFD = OSAL_ERROR;
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                         "ADR3CTRL-SCC socket closed successfully"
                                        );
            }
         }
#endif         
      break;
      }
      case ADR3CTRL_RESOURCE_RELEASE_SCC_DGRAM :
      {
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE       
       /* Check for the Valid Handler */
         if(OSAL_NULL !=  pADR3CtrlData->hDevADR3CtrlSCC_dgram)
         {
            if(dgram_exit(pADR3CtrlData->hDevADR3CtrlSCC_dgram) < 0)
            {
               tInt errsv = errno;
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                         "dgram exit Failed with Error =%d",
                                         errsv
                                        );
            }
            else 
            {
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,"SCC_dgram exit successful");
               pADR3CtrlData->hDevADR3CtrlSCC_dgram = OSAL_NULL;
            }
         }
#endif
         break;
      }
      /*Release resource DataReadMQ*/
      case ADR3CTRL_RESOURCE_RELEASE_LI_HNDL_TX_MQ:
      {
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE        
        /* Check for the Valid Handler */
         if(hDevAdr3_SCC_DataReadMQHndlr != OSAL_C_INVALID_HANDLE)
         {
            if(OSAL_OK != OSAL_s32MessageQueueClose(hDevAdr3_SCC_DataReadMQHndlr))
            {
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                         "MQ close Failed with Error =%d",
                                         OSAL_u32ErrorCode() 
                                        );
            }
            else if(OSAL_OK != OSAL_s32MessageQueueDelete(DEV_ADR3CTRL_LI_HNDLNG_MQ_NM))
            {
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                         "MQ Delete Failed with Error =%d",
                                         OSAL_u32ErrorCode() 
                                        );
               hDevAdr3_SCC_DataReadMQHndlr = OSAL_C_INVALID_HANDLE;
            }
            else
            {
               hDevAdr3_SCC_DataReadMQHndlr = OSAL_C_INVALID_HANDLE;;
            }
         }
#endif
         break;
      }
      case ADR3CTRL_RESOURCE_RELEASE_SH_MEM:
      {
         if(OSAL_OK != OSAL_s32SharedMemoryDelete(DEV_ADR3CTRL_SH_NAME))
         {
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "SHMem delete Failed with Error =%d",
                                      OSAL_u32ErrorCode() 
                                     );
            NORMAL_M_ASSERT_ALWAYS();
         }
         break;
      }  
      default :
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,"default:%d @ line %d",
                                    u32Resource,__LINE__);
         break;
      }
   }
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_u32ADR3CommDown
*
* PARAMETER    : void 
*
* RETURNVALUE  : tU32
*                
*
* DESCRIPTION  : To bring INC-ADR network down
*                Network down will purge all the message to adr3 chip
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 DEV_ADR3CTRL_u32ADR3CommDown(tVoid)
{
   return DEV_ADR3CTRL_setNetDevState(0);
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_u32ADR3CommUp
*
* PARAMETER    : void 
*
* RETURNVALUE  : tU32
*                
*
* DESCRIPTION  :  To bring INC-ADR3 network Up
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32 DEV_ADR3CTRL_u32ADR3CommUp(tVoid)
{
   return DEV_ADR3CTRL_setNetDevState(1);
}

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vHndlADR3Active
*
* PARAMETER    : void 
*
* RETURNVALUE  : void
*                
*
* DESCRIPTION  : Handles the ADR3 Active response form the V850 
*                 1) Changes the state Machine to ALIVE State
*                 2) Callback execution
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vHndlADR3Active(tVoid)
{
   /* Make the State machine to ALIVE  */
   DEV_ADR3CTRL_vStateMachine(ADR3_ST_EVNT_ACTIVE); 
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vHndlADR3InActive
*
* PARAMETER    : void 
*
* RETURNVALUE  : void
*                
*
* DESCRIPTION  : Handles the ADR3 Active response form the V850 
*                 1) Changes the state Machine to DEAD State
*                 2) Callback execution
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vHndlADR3InActive(tVoid)
{
   /* Make the State machine to DEAD  */
   DEV_ADR3CTRL_vStateMachine(ADR3_ST_EVNT_INACTIVE); 
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vHndlResetActveNormal
*
* PARAMETER    : void 
*
* RETURNVALUE  : void
*                
*
* DESCRIPTION  : Handles the Reset active normal response form the V850 
*                 1) Changes the state Machine to Dead State
*                 2) Callback execution
*                 3) Send RESET RELEASE NORMAL to ADR3_SCC
*                 Download initialization done by DEV_ADR3CTRL_initDnl()
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vHndlResetActveNormal(tVoid)
{
   /* Make the State machine to Dead */
   DEV_ADR3CTRL_vStateMachine(ADR3_ST_EVNT_RESET_ACTIVE); 
 /* Shut down the inc-adr3 network i.e. stopping the communication with ADR3 Chip*/
   if(DEV_ADR3CTRL_u32ADR3CommDown() == OSAL_E_NOERROR){
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
   char sendbuf[ADR3_SCC_COMM_BUFFR_LENGHT]={0};
   /* send command to V850 as ADR3 Reset release */
   sendbuf[0] = SCC_ADR3CTRL_C_SET_RESET;
   sendbuf[1] = ADR3_RESET_STATUS_RELEASE;
   sendbuf[2] = ADR3_MODE_NORMAL;
   if(ADR3_SCC_COMM_BUFFR_LENGHT != dgram_send(pADR3CtrlData->hDevADR3CtrlSCC_dgram,
                                               sendbuf,
                                               ADR3_SCC_COMM_BUFFR_LENGHT
                                              )
      )
   {
      tInt errsv = errno;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "dgram_send Failed with Error =%d",
                                errsv 
                               );
      NORMAL_M_ASSERT_ALWAYS();
   }
#endif
   }
   else
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
}

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vHndlResetRelNormal
*
* PARAMETER    : void 
*
* RETURNVALUE  : void
*                
*
* DESCRIPTION  : Handles the Reset active normal response form the V850 
*                 1) Changes the state Machine to ACTIVE State
*                 2) Callback execution
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vHndlResetRelNormal(tVoid)
{
   /*  Start the communication with ADR3 */
   if(DEV_ADR3CTRL_u32ADR3CommUp() == OSAL_E_NOERROR)
   {
      enPreviousMode = BOOT_MODE_NORMAL;
      /* Make the State machine to ALIVE  */
      DEV_ADR3CTRL_vStateMachine(ADR3_ST_EVNT_NRML_RESET_REL); 
   }
   else
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
}

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vHndlResetActveDwnld
*
* PARAMETER    : void 
*
* RETURNVALUE  : void
*                
*
* DESCRIPTION  : Handles the Reset active Download response form the V850 
*                 1) Changes the state Machine to Dead State
*                 2) Callback execution
*                 3) ifconfig down the ADR3(Purges all the mesage in queue to ADR3 chip)
*                 4) Send RESET RELEASE DOWNLOAD to ADR3_SCC
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification              | Author & comments
*--------------|----------------------------------|-----------------------------------
* 27.Aug.2013  |  Initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vHndlResetActveDwnld(tVoid)
{
   /* Make the State machine to Dead */
   DEV_ADR3CTRL_vStateMachine(ADR3_ST_EVNT_RESET_ACTIVE); 
   
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
   char sendbuf[ADR3_SCC_COMM_BUFFR_LENGHT]={0};
   /* send command to V850 as ADR3 Reset release */
   sendbuf[0] = SCC_ADR3CTRL_C_SET_RESET;
   sendbuf[1] = ADR3_RESET_STATUS_RELEASE;
   sendbuf[2] = ADR3_MODE_DOWNLOAD;
   if(ADR3_SCC_COMM_BUFFR_LENGHT != dgram_send(pADR3CtrlData->hDevADR3CtrlSCC_dgram,
                                               sendbuf,
                                               ADR3_SCC_COMM_BUFFR_LENGHT
                                              )
      )
   {
      tInt errsv = errno;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "dgram_send failed with Error =%d",
                                errsv 
                               );
      NORMAL_M_ASSERT_ALWAYS();
   }
#endif
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vHndlResetRelDwnld
*
* PARAMETER    : void 
*
* RETURNVALUE  : void
*                
*
* DESCRIPTION  : Handles the Reset active Download response form the V850 
*                 1) ifconfig Up the ADR3(All communication with ADR3 STARTs)
*                 2) Changes the state Machine to Download State
*                 3) Callback execution
* HISTORY      :
*---------------------------------------------------------------------------------------------
* Date         |        Modification                      | Author & comments
*--------------|------------------------------------------|-----------------------------------
* 27.Aug.2013  |  Initial  Modification                   |  Madhu Sudhan Swargam (RBEI/ECF5)
* 10.Feb.2016  |  Clear reg gpio state after reset release|  Madhu Sudhan Swargam (RBEI/ECF5)
* 26.May.2016  |  PSARCCB-8674: clear gpio is moved to write | Madhu Sudhan Swargam (RBEI/ECF5)
* --------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vHndlResetRelDwnld(tVoid)
{
    enPreviousMode = BOOT_MODE_SPI;
    
   /* Make the State machine to DWNLD */
    DEV_ADR3CTRL_vStateMachine(ADR3_ST_EVNT_SPI_RESET_REL);  
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_SCCDataSendThread
*
* PARAMETER    : NONE
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  :  Thread is responsible for sending Data to ADR3Ctrl_SCC.
*                 DGRAM send from different process trough IOControl is not supported as the SOcket is Porcess specific.
*                 The IOControl send the MQ, based on the Msg the data is send to ADR3Ctrl_SCC
*
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_SCCDataSendThread(tPVoid pvArg)
{
   (tVoid) pvArg; 
   tU32 u32MsgRequest;
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
   char sendbuf[ADR3_SCC_COMM_BUFFR_LENGHT]={0};
   int errsv ;
#else
    tU32 u32retVal;
#endif
   pADR3CtrlData->bDevAdr3Ctrl_SCCDataSendTxActv = TRUE;
   while(pADR3CtrlData && (pADR3CtrlData->bDevAdr3Ctrl_SCCDataSendTxActv == TRUE))   
   {
      /* Waiting for msg from IOControl*/
       if(0 <  OSAL_s32MessageQueueWait(hDevAdr3CtrlSCCDataSend_MQ,
                                        (tPU8)&u32MsgRequest,
                                        sizeof(tU32),
                                        OSAL_NULL,
                                        (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER
                                       )
      )
      {
         if((DEV_ADR3CTRL_SCC_DATASEND_RESET_MSG == u32MsgRequest)
            && 
            (ADR3CTRL_ADR3_STATE_DEADINIT != pADR3CtrlData->eDevAdr3_State)
           )
         {
            if(ADR3CTRL_ADR3_STATE_DNL != pADR3CtrlData->eDevAdr3_State)
            {
               DEV_ADR3CTRL_vStateMachine(ADR3_ST_EVNT_IOCNTRL_DEAD_INIT);
            }
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
            if(pADR3CtrlData && (pADR3CtrlData->hDevADR3CtrlSCC_dgram != OSAL_NULL))
            {
               sendbuf[0] = SCC_ADR3CTRL_C_SET_RESET;
               sendbuf[1] = ADR3_RESET_STATUS_ACTIVE;
               if(BOOT_MODE_NORMAL == enPreviousMode)
               {
                  sendbuf[2] = ADR3_MODE_NORMAL;
               }
               else if(BOOT_MODE_SPI == enPreviousMode)
               {
                  sendbuf[2] = ADR3_MODE_DOWNLOAD;
               }
               //sendbuf[2] = ADR3_MODE_NORMAL;
               if(ADR3_SCC_COMM_BUFFR_LENGHT != dgram_send(pADR3CtrlData->hDevADR3CtrlSCC_dgram,
                                                           sendbuf,
                                                           ADR3_SCC_COMM_BUFFR_LENGHT
                                                          )
                 )
               {
                  errsv = errno;
                  DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                            "Dgram send Failed at line %d with Error =%d",
                                            __LINE__,
                                            errsv
                                           );
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "SCC data send for Reset Request=%d,%d,%d",
                                      sendbuf[0],
                                      sendbuf[1],
                                      sendbuf[2]
                                     );
#else
            /*TODO: Hardware mute and Bringing ADR3 Communication down is yet to Implement*/
            /* Change the State machine to Dead State with CallBack*/
            if(BOOT_MODE_NORMAL == enPreviousMode)
            {
               DEV_ADR3CTRL_vHndlResetActveNormal();
            }
            else if(BOOT_MODE_SPI == enPreviousMode)
            {
               DEV_ADR3CTRL_vHndlResetActveDwnld();
            }
            /* Reset the ADR3ctrl its previous state  */
            if(OSAL_E_NOERROR == (u32retVal = DEV_ADR3CTRL_resetADR(enPreviousMode)))
            {  
               if(BOOT_MODE_NORMAL == enPreviousMode)
               {
                  DEV_ADR3CTRL_vHndlResetRelNormal();
               }
               else if(BOOT_MODE_SPI == enPreviousMode)
               {
                  DEV_ADR3CTRL_vHndlResetRelDwnld();
               }
            /*TODO: Hardware mute and Bringing ADR3 Communication up is yet to Implement*/
            }
            else
            {
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                         "ResetADR Failed at line %d with Error =%d",
                                         __LINE__,
                                         u32retVal
                                        );
               NORMAL_M_ASSERT_ALWAYS();
            } 
#endif
         }
         else if(DEV_ADR3CTRL_SCC_DATASEND_SPIMOD_MSG == u32MsgRequest)
         {
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
            if(pADR3CtrlData && (pADR3CtrlData->hDevADR3CtrlSCC_dgram != OSAL_NULL))
            {
               sendbuf[0] = SCC_ADR3CTRL_C_SET_RESET;
               sendbuf[1] = ADR3_RESET_STATUS_ACTIVE;
               sendbuf[2] = ADR3_MODE_DOWNLOAD;
               if(ADR3_SCC_COMM_BUFFR_LENGHT 
                  != 
                  dgram_send(pADR3CtrlData->hDevADR3CtrlSCC_dgram,
                             sendbuf,
                             ADR3_SCC_COMM_BUFFR_LENGHT
                            )
                  )
               {
                  errsv = errno;
                  DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                            "Dgram send failed with error %d",
                                            errsv
                                           );
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "SCC data send for SPI Mode Request=%d,%d,%d",
                                      sendbuf[0],
                                      sendbuf[1],
                                      sendbuf[2]
                                     );
#else
            /*TODO: Hardware mute and Bringing ADR3 Communication down is yet to Implement*/
            DEV_ADR3CTRL_vHndlResetActveDwnld();
            if(OSAL_E_NOERROR == (u32retVal = DEV_ADR3CTRL_resetADR(BOOT_MODE_SPI)))
            {
               //enPreviousMode = BOOT_MODE_SPI;
               DEV_ADR3CTRL_vHndlResetRelDwnld();
            
            }
            else
            {
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                         "ResetADR Failed at line %d with Error =%d",
                                         __LINE__,
                                         u32retVal
                                        );
            }
#endif
         }
         else if(DEV_ADR3CTRL_SCC_DATASEND_NORMALMOD_MSG == u32MsgRequest)
         {
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
           if(pADR3CtrlData && (pADR3CtrlData->hDevADR3CtrlSCC_dgram != OSAL_NULL))
            {
               sendbuf[0] = SCC_ADR3CTRL_C_SET_RESET;
               sendbuf[1] = ADR3_RESET_STATUS_ACTIVE;
               sendbuf[2] = ADR3_MODE_NORMAL;
               if(ADR3_SCC_COMM_BUFFR_LENGHT 
                  != 
                  dgram_send(pADR3CtrlData->hDevADR3CtrlSCC_dgram,
                             sendbuf,
                             ADR3_SCC_COMM_BUFFR_LENGHT
                            )
                 )
               {
                  errsv = errno;
                  DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                            "Dgram send Failed at line %d with Error =%d",
                                            __LINE__,
                                            errsv
                                           );
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "SCC data send for Reset Request=%d,%d,%d",
                                      sendbuf[0],
                                      sendbuf[1],
                                      sendbuf[2]
                                     );
#else
            DEV_ADR3CTRL_vHndlResetActveNormal();
            if(OSAL_E_NOERROR == (u32retVal = DEV_ADR3CTRL_resetADR(BOOT_MODE_NORMAL)))
            {
               //enPreviousMode = BOOT_MODE_NORMAL;
               DEV_ADR3CTRL_vHndlResetRelNormal();
               /*TODO: Hardware mute and Bringing ADR3 Communication UP is yet to Implement*/
            }
            else
            {
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                         "ResetADR Failed at line %d with Error =%d",
                                         __LINE__,
                                         u32retVal
                                        );
            }
#endif
         } 
         else if(DEV_ADR3CTRL_SCC_DATASEND_THREAD_EXIT == u32MsgRequest)
         {  
            pADR3CtrlData->bDevAdr3Ctrl_SCCDataSendTxActv = FALSE;
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,"Received DATASEND_THREAD_EXIT ");
         }
         u32MsgRequest = 0;
      }
      else
      {  
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "MessageQueueWait Failed line %lu with error =%u",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   /* Exit the Thread*/
   OSAL_vThreadExit();
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vExcute_SCC_MSG
*
* PARAMETER    : NONE
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  :  Acts on the state machine based on the ADR3CTRL_SCC message
* HISTORY      :
*-------------------------------------------------------------------------------------------
* Date         |        Modification                      | Author & comments
*--------------|------------------------------------------|-----------------------------------
* 05.Jun.2014  |  Initial  Modification                   |  Madhu Sudhan Swargam (RBEI/ECF5)
* ---------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vExcute_SCC_MSG(tU32 u32MsgRequest)
{
   switch(u32MsgRequest)
   {
      /* Reset Active Normal mode response from socket read thread*/
      case DEV_ADR3CTRL_LI_HNDL_RST_ACT_NOR_MSG:
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                   "Reset Active normal is received from ADR3_scc"
                                  );
         DEV_ADR3CTRL_vHndlResetActveNormal();
      }
      break;
      /* Reset Release Normal mode response from socket read thread*/
      case DEV_ADR3CTRL_LI_HNDL_RST_REL_NOR_MSG:
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                   "Reset release normal is received from ADR3_scc"
                                  );
         DEV_ADR3CTRL_vHndlResetRelNormal();
      }
      break;
      /* Reset Active Download mode response from socket read thread*/       
      case DEV_ADR3CTRL_LI_HNDL_RST_ACT_DWNLD_MSG:
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                   "Reset Active download is received from ADR3_scc"
                                  );
         DEV_ADR3CTRL_vHndlResetActveDwnld();
      }
      break;
      /* Reset Release Download mode response from socket read thread*/
      case DEV_ADR3CTRL_LI_HNDL_RST_REL_DWNLD_MSG:
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                   "Reset Release Download is received from ADR3_scc"
                                  );
         DEV_ADR3CTRL_vHndlResetRelDwnld(); 
      }
      break;
      /* ADR3 state active response from socket read thread*/
      case DEV_ADR3CTRL_LI_HNDL_ADR3SCC_ACTV_MSG:
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                   "ADR3 ACTIVE is received from ADR3_scc"
                                  );
         DEV_ADR3CTRL_vHndlADR3Active();  
      }
      break;
       /* ADR3 state inactive response from socket read thread*/
      case DEV_ADR3CTRL_LI_HNDL_ADR3SCC_INACTV_MSG:
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                   "Active INACTIVE is received from ADR3_scc"
                                  );
         DEV_ADR3CTRL_vHndlADR3InActive();
      }
      break;
     case DEV_ADR3CTRL_LI_HNDL_THREAD_EXIT:
      {
         //ignore
      }
      break;
      default :
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "Unexpected Msg received %d",
                                   u32MsgRequest
                                  );
         NORMAL_M_ASSERT_ALWAYS();                      
      }            
   }
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vLiHndlngThread
*
* PARAMETER    : NONE
*
* RETURNVALUE  : NONE
*
* DESCRIPTION  :  Thread is responsible for callback execution and send commands to V850 if require.
*                 This thread waits from ADR3_SCC socket read thread MQ post.
*                 Once Msg received,change the state machine and execute callback and  send response to v850 
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification                   | Author & comments
*--------------|---------------------------------------|-----------------------------------
* 27.Aug.2013  |  Initial  Modification                |  Madhu Sudhan Swargam (RBEI/ECF5)
* 07.Aug.2014  |  Thread shut-down message Handling    |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vLiHndlngThread(tPVoid pvArg)
{
   (tVoid) pvArg;
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
   tU32 u32MsgRequest;
   pADR3CtrlData->bDevAdr3Ctrl_LxHndlTxActv = TRUE;
   while(pADR3CtrlData && (pADR3CtrlData->bDevAdr3Ctrl_LxHndlTxActv == TRUE))
   {
       if(0 <  OSAL_s32MessageQueueWait(hDevAdr3_SCC_DataReadMQHndlr,
                                        (tPU8)&u32MsgRequest,
                                        sizeof(tU32),
                                        OSAL_NULL,
                                        (OSAL_tMSecond)OSAL_C_TIMEOUT_FOREVER
                                       )
         )
      {  
         /* ADR3CTRL_SCC Component Status is received */
         if(TRUE == pADR3CtrlData->bDevAdr3Ctrl_SCC_CompntActv)
         {
            DEV_ADR3CTRL_vExcute_SCC_MSG(u32MsgRequest);
         }
         if(DEV_ADR3CTRL_LI_HNDL_THREAD_EXIT == u32MsgRequest)
         {
               pADR3CtrlData->bDevAdr3Ctrl_LxHndlTxActv = FALSE;
         }
      }
      else
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "MessageQueueWait Failed line %lu with error =%u",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         NORMAL_M_ASSERT_ALWAYS();
      }
   } /* pADR3CtrlData->bDevAdr3Ctrl_LxHndlTxActv Made false at Driver last close */
   /* Exit the Thread*/
#endif
   DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "Received LI_HNDL_THREAD_EXIT "
                                     );

   OSAL_vThreadExit();
}


/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_bLinuxResource_Init
*
* PARAMETER    : NONE
*
* RETURNVALUE  : OSAL_OK  on success
*                OSAL_ERROR on Failure
*
* DESCRIPTION  :   Creates the resource for ADR3Ctrl_linux handling
*                 1) Create Message queue for SCC data Send thread and data receive thread
*
* HISTORY      :
*-------------------------------------------------------------------------------
* Date         |        Modification                         |    Author 
*--------------|---------------------------------------------|------------------
* 27.Aug.2013  |  initial  Modification                       |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool DEV_ADR3CTRL_bLinuxResource_Init(tVoid)
{
   tBool bLinux_initOkay = FALSE;
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
   if (OSAL_OK != OSAL_s32MessageQueueCreate(DEV_ADR3CTRL_LI_HNDLNG_MQ_NM, 10, 
                                             sizeof(tU32), 
                                             OSAL_EN_READWRITE, 
                                             &hDevAdr3_SCC_DataReadMQHndlr
                                             )
      )
   {
       DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                 "SCC_DataReadMQ create Failed at line %d with Error =%d",
                                 __LINE__,
                                 OSAL_u32ErrorCode()
                                );
   }
   else if (OSAL_OK != OSAL_s32MessageQueueCreate(DEV_ADR3_SCC_DATA_SEND_MQ_NAME, 
                                                  10, 
                                                  sizeof(tU32), 
                                                  OSAL_EN_READWRITE, 
                                                  &hDevAdr3CtrlSCCDataSend_MQ
                                                 )
           )
   {
       DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                 "CCDataSend_MQ create Failed at line %d with Error =%d",
                                 __LINE__,
                                 OSAL_u32ErrorCode()
                                );
   }
   else
   {
       bLinux_initOkay = TRUE;
       DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                 "ADR3Ctrl_LINUX Resource init success"
                                );
   }
#endif
   return bLinux_initOkay;
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_DriverCntrlThreadspawn
*
* PARAMETER    : tVoid
*
* RETURNVALUE  : Returns FALSE on error case else TRUE
*                
*
* DESCRIPTION  : spawns the Thread for Linux handling and SCC Data Send Thread
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool DEV_ADR3CTRL_DriverCntrlThreadspawn(tVoid)
{
   OSAL_trThreadAttribute rThreadAttr;
   tBool bDriverCntrl_ThreadspawnActv = FALSE;
   /*Update thread attributes*/
   rThreadAttr.szName        = DEV_ADR3CTRL_LI_HNDL_THRD_NM;
   rThreadAttr.u32Priority   = DEV_ADR3CTRL_LI_HNDL_THRD_PRIOR;
   rThreadAttr.s32StackSize  = DEV_ADR3CTRL_LI_HNDL_THRD_STCKSZ;
   rThreadAttr.pfEntry       = DEV_ADR3CTRL_vLiHndlngThread;
   rThreadAttr.pvArg         = OSAL_NULL;
   /* This thread will be waiting for data from ADR3Linux state change 
   event notification.   */
   if(OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr))
   {
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "vLiHndlngThread Failed at line %d with Error =%d",
                                __LINE__,
                                OSAL_u32ErrorCode()
                               );
      DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_LI_HNDL_TX_MQ);
   }
   else
   {
      rThreadAttr.szName        = DEV_ADR3CTRL_SCC_DATA_SEND_THRD_NM;
      rThreadAttr.u32Priority   = DEV_ADR3CTRL_SCC_DATA_SEND_THRD_PRIOR;
      rThreadAttr.s32StackSize  = DEV_ADR3CTRL_SCC_DATA_SEND_THRD_STCKSZ;
      rThreadAttr.pfEntry       = DEV_ADR3CTRL_SCCDataSendThread;
      rThreadAttr.pvArg         = OSAL_NULL;
      if(OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr))
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "SCCDataSendThread Failed at line %d with Error =%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_LI_HNDL_TX_MQ);
      }
      else
      {
         bDriverCntrl_ThreadspawnActv= TRUE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,"ADR3Ctrl Threads Successfully Spawned");
      }
   }
   return(bDriverCntrl_ThreadspawnActv);
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_s32SCC_CommSocketSetup
*
* PARAMETER    : tVoid
*
* RETURNVALUE  : Returns OSAL_ERROR on error case else zero
*                
*
* DESCRIPTION  : Creates the Socket communication with ADR3_SCC
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification               | Author & comments
*--------------|-----------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification            |  Madhu Sudhan Swargam (RBEI/ECF5)
* 14.Mar.2016  |  Use getaddrinfo instaed of 
*              | gethostbyname(This is not MT safe)|  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 DEV_ADR3CTRL_s32SCC_CommSocketSetup(tVoid)
{
   tS32 s32RetVal = 0;
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
   struct addrinfo hints, *socAddr_local = NULL,*socAddr_remote = NULL;
   int ret;
   char hstSCCLunID[20]={0};
   
   sprintf (hstSCCLunID,"%d",ADR3CTRL_PORT);
   memset(&hints, 0, sizeof(hints));
   hints.ai_family = AF_INET;
   ret = getaddrinfo(SCC_INC_NODE_NAME, hstSCCLunID, &hints, &socAddr_remote);
   if(0 != ret)
   {
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
      "scc get adddfrinfo returned null LINE=%d errno %d",
      __LINE__,
      ret
      );
      s32RetVal = -1;
   }
   else 
   {
      if(NULL == socAddr_remote)
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
         "scc get addrinfo socAddr_remote update failed"
         );
         s32RetVal = -1;
      }
   }
   memset(&hints, 0, sizeof(hints));
   hints.ai_family = AF_INET;
   ret = getaddrinfo(SCC_INC_NODE_LOCAL_NAME, hstSCCLunID,
                     &hints, &socAddr_local);
   if(0 != ret)
   {
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
        "scc get adddfrinfo returned null LINE=%d errno %d",
         __LINE__,
         ret
         );
      s32RetVal = -1;
   }
   else
   {    
      if(NULL == socAddr_local)
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
         "scc get addrinfo socAddr_local update failed"
         );
         s32RetVal = -1;
      }
   }
   if(s32RetVal != -1)
   {
      /* Creating the Socket Communication*/
      pADR3CtrlData->s32DevADR3CtrlSCC_INCSocketFD = socket(AF_BOSCH_INC_AUTOSAR,(tInt)SOCK_STREAM,0);
      if(pADR3CtrlData->s32DevADR3CtrlSCC_INCSocketFD < 0)
      {
         tInt errsv = errno;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "Socket system call failed errno=%d",
                                   errsv 
                                  );
         s32RetVal = -1;
      }
      else
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,"Socket Creation Successful");
      }
   }
   /*  Dgram initialization to send and recive the commands */
   if((s32RetVal != -1) 
      &&
      (NULL == (pADR3CtrlData->hDevADR3CtrlSCC_dgram = dgram_init(pADR3CtrlData->s32DevADR3CtrlSCC_INCSocketFD,
                                                             DGRAM_MAX,
                                                             NULL
                                                            )
               )
      )
     )
   {
      tInt errsv = errno;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "Dgram Init failed errno=%d",
                                errsv
                              );
      s32RetVal = -1;
      DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SCC_SOCKET);
   }
   /* Bind to local address */
   if((s32RetVal != -1) && (NULL != socAddr_local))
   {
      s32RetVal = bind(pADR3CtrlData->s32DevADR3CtrlSCC_INCSocketFD,
                       socAddr_local->ai_addr,/*lint !e64 */
                       socAddr_local->ai_addrlen
                      );
      if(s32RetVal != 0)
      {        
         tInt errsv = errno;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "Bind to socket failed errno=%d",
                                   errsv
                                  );
         DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SCC_DGRAM);
         DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SCC_SOCKET);    
         s32RetVal = -1;
      }
      else
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,"Bind is  Successful");
         freeaddrinfo(socAddr_local); 
      }
   }   
   if((s32RetVal != -1) && (NULL != socAddr_remote))
   {
   /* Connect to remote scc address */
      s32RetVal = connect(pADR3CtrlData->s32DevADR3CtrlSCC_INCSocketFD,
                          socAddr_remote->ai_addr,/*lint !e64 */
                          socAddr_remote->ai_addrlen
                         );          
      if(s32RetVal == 0)
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                   "Successfully Connected with ADR3_SCC "
                                  );
         s32RetVal = OSAL_OK;
         freeaddrinfo(socAddr_remote); 
      }
      else
      {
         tInt errsv = errno;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "Connect Failed errno=%d",
                                   errsv 
                                  );
         DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SCC_DGRAM);
         DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SCC_SOCKET);
         s32RetVal = -1;
      }
   }
   if(s32RetVal != OSAL_OK)
   {
      s32RetVal = OSAL_ERROR;
   }
#endif
   return s32RetVal;
}

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_u32GetMsgPostCommand
*
* PARAMETER    : tVoid
*
* RETURNVALUE  : Returns DEV_ADR3CTRL_LI_HNDL_UNKNWN_MSG on error case
*
*
* DESCRIPTION  : Based on the MsgID,status and Mode decides which Msg post has to made to ADR3 thread
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tU32  DEV_ADR3CTRL_u32GetMsgPostCommand(char cMsgID,char cStatus,char cMode)
{
   tU32 u32MsgCommand = DEV_ADR3CTRL_LI_HNDL_UNKNWN_MSG;
   /* Command response for ADR3 component Status  */
   if(SCC_ADR3CTRL_R_COMPONENT_STATUS == cMsgID)
   {
      if((ADR3_STATUS_ACTIVE == cStatus) && (ADR3_FIRST_VERSION == cMode))
      {
      /* ADR3 is up and  ready for Communication*/
         u32MsgCommand = DEV_ADR3CTRL_LI_HNDL_ADR3SCC_ACTV_MSG;
      }
      else if((ADR3_STATUS_INACTIVE == cStatus) && (ADR3_FIRST_VERSION == cMode))
      {
      /* ADR3 is not ready for Communication*/      	
         u32MsgCommand = DEV_ADR3CTRL_LI_HNDL_ADR3SCC_INACTV_MSG;
      }
   }
   /* Command response for ADR3 component reset */
   if((u32MsgCommand == DEV_ADR3CTRL_LI_HNDL_UNKNWN_MSG) 
      && 
      (SCC_ADR3CTRL_R_SET_RESET == cMsgID)
     )
   {
      if((ADR3_RESET_STATUS_ACTIVE == cStatus) && (ADR3_MODE_NORMAL == cMode))
      {
      /* Response as the Reset is active in normal mode */
        u32MsgCommand = DEV_ADR3CTRL_LI_HNDL_RST_ACT_NOR_MSG;
      }
      else if((ADR3_RESET_STATUS_RELEASE == cStatus) && (ADR3_MODE_NORMAL == cMode))
      {
      /* Response as the Reset is released in normal mode */
        u32MsgCommand = DEV_ADR3CTRL_LI_HNDL_RST_REL_NOR_MSG;
      }
      else if((ADR3_RESET_STATUS_ACTIVE == cStatus) && (ADR3_MODE_DOWNLOAD == cMode))
      {
      /* Response as the Reset is Active in Dwld mode */
        u32MsgCommand = DEV_ADR3CTRL_LI_HNDL_RST_ACT_DWNLD_MSG;
      }
      else if((ADR3_RESET_STATUS_RELEASE == cStatus) && (ADR3_MODE_DOWNLOAD == cMode))
      {
      /* Response as the Reset is Active in Dwld mode */
        u32MsgCommand = DEV_ADR3CTRL_LI_HNDL_RST_REL_DWNLD_MSG;
      }
   }
   if((u32MsgCommand == DEV_ADR3CTRL_LI_HNDL_UNKNWN_MSG) 
      && 
      (SCC_ADR3CTRL_R_REJECT == cMsgID)
     )
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
   return u32MsgCommand;
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vSCC_DataReadThread
*
* PARAMETER    : tPVoid pvArg
*
* RETURNVALUE  : Void
*                
*
* DESCRIPTION  : Read the response form ADR3_SCC
*                Decide the operation to performe and post the
*                Operation event to ADR3 thread
* HISTORY      :
*------------------------------------------------------------------------------------
* Date         |        Modification                         | Author & comments
*--------------|---------------------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification                      |  Madhu Sudhan Swargam (RBEI/ECF5)
* 07.Aug.2014  |  Thread shut-down message handling          |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vSCC_DataReadThread(tPVoid pvArg)
{
   (tVoid)pvArg;
   char sendbuf[ADR3_SCC_COMM_BUFFR_LENGHT]={0};
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
   sk_dgram* pdgram = pADR3CtrlData->hDevADR3CtrlSCC_dgram;
   tS32 s32pfd = pdgram->sk;
   tS32 s32rc = -1;
   tU32 u32MsgPostCommand;
   struct pollfd fds[2];
   tS32 s32timeout = -1;
   char buffer[ADR3_SCC_COMM_BUFFR_LENGHT];
   memset(buffer,0,sizeof(buffer));
   while(pADR3CtrlData && pADR3CtrlData->bDevAdr3Ctrl_SCC_DataReadTxActv)
   {
      fds[0].fd = s32pfd;
      fds[0].events = POLLIN;
      fds[0].revents =0;
      fds[1].fd = pADR3CtrlData->UnblkPipeFdSet[0];
      fds[1].events = POLLIN;
      fds[1].revents =0;
      s32rc = poll(fds,2,s32timeout);
      if(s32rc < 0)
      {
         tInt errsv = errno;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,"Poll Failed with %d error",errsv);
         NORMAL_M_ASSERT_ALWAYS();
         if(EINTR != errsv)
         {  
            pADR3CtrlData->bDevAdr3Ctrl_SCC_DataReadTxActv = FALSE;
            NORMAL_M_ASSERT_ALWAYS();
            break;
         }
         else
         {
            continue;
         }
      }
      if(s32rc == 0)
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,"Poll Timed out");
         continue;
      }
      if(fds[0].revents & POLLIN)
      {
         /* receives the command form ADR3_SCC */
         s32rc = dgram_recv(pdgram,buffer,sizeof(buffer));
         if(s32rc > 0)
         {
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                      "ADR3 SCC Bytes Received =%d: %d %d %d",
                                      s32rc,buffer[0],buffer[1],buffer[2]
                                     );
            u32MsgPostCommand = DEV_ADR3CTRL_u32GetMsgPostCommand(buffer[0],
                                                                 buffer[1],
                                                                 buffer[2]
                                                                );
            if((u32MsgPostCommand != DEV_ADR3CTRL_LI_HNDL_UNKNWN_MSG) && pADR3CtrlData)
            {
               if(OSAL_OK != OSAL_s32MessageQueuePost(hDevAdr3_SCC_DataReadMQHndlr,
                                                      (tPU8)&u32MsgPostCommand,
                                                      sizeof(tU32),       
                                                      0
                                                     )
                  )
               {
                  DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                            "OSAL_s32MessageQueuePost Failed line %lu with error =%u",
                                            __LINE__,
                                            OSAL_u32ErrorCode()
                                           );
                  NORMAL_M_ASSERT_ALWAYS();
               } 
            }
            else
            {
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                         "Unknow command form ADR3_SCC MsgID = %x,status =%x,Mode =%x",
                                         buffer[0],
                                         buffer[1],
                                         buffer[2] 
                                        );
            }
         }
         else
         {
            tInt errsv = errno;
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "dgram_recv Failed at line %d with Error =%d",
                                      __LINE__,
                                      errsv
                                     );
            NORMAL_M_ASSERT_ALWAYS();
         }
         memset(buffer,0,sizeof(buffer));
      }
      if(fds[1].revents & POLLIN)
      {
         pADR3CtrlData->bDevAdr3Ctrl_SCC_DataReadTxActv = FALSE;
         break;
      }
   }/* pADR3CtrlData->bDevAdr3Ctrl_LxHndlTxActv Made false at DRIVER LAST CLOSE */
#endif

   /* send command to V850 as to ignore IMx  */
   sendbuf[0] = SCC_ADR3CTRL_C_COMPONENT_STATUS;
   sendbuf[1] = ADR3_STATUS_INACTIVE;
   sendbuf[2] = ADR3_FIRST_VERSION;
   if(ADR3_SCC_COMM_BUFFR_LENGHT != dgram_send(pADR3CtrlData->hDevADR3CtrlSCC_dgram,
                                               sendbuf,
                                               ADR3_SCC_COMM_BUFFR_LENGHT
                                              )
      )
   {
      tInt errsv = errno;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "dgram_send Failed with Error =%d",
                                errsv 
                               );
      NORMAL_M_ASSERT_ALWAYS();
   }
   pADR3CtrlData->bSCC_Comm_initOkay = FALSE;
   DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SCC_DGRAM);  
   DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SCC_SOCKET); 
   fds[0].fd  = 0;
   fds[1].fd  = 0;
   DEV_ADR3CTRL_fileClose(pADR3CtrlData->UnblkPipeFdSet[0]);
   DEV_ADR3CTRL_fileClose(pADR3CtrlData->UnblkPipeFdSet[1]); 
   OSAL_vThreadExit();
}

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_bSCC_Comm_Init
*
* PARAMETER    : void
*
* RETURNVALUE  : tBool 
*                
*
* DESCRIPTION  : initializes the ADR3_SCC Communication
*                 1) Creates the Socket Communication
*                 2) initialize the Thread which listen to ADR3_SCC responses
* HISTORY      :
*---------------------------------------------------------------------------------------------------
* Date         |        Modification                               | Author & comments
*--------------|---------------------------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification                            |  Madhu Sudhan Swargam (RBEI/ECF5)
* 07.Aug.2014  |  Pipe creation for thread shut-down handling      |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool DEV_ADR3CTRL_bSCC_Comm_Init(tVoid)
{
   tBool bSCC_Comm_initOkay = FALSE;
   OSAL_trThreadAttribute rThreadAttr={0};
   /* Do socket set up. Just connect,no transactions */
   if(OSAL_ERROR ==  DEV_ADR3CTRL_s32SCC_CommSocketSetup())
   {
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,"DEV_ADR3CTRL_s32SCC_CommSocketSetup Failed");
   }
   else
   {
   /* create anonymous pipe for unblocking poll call in DataReadThread */
      if (pipe(pADR3CtrlData->UnblkPipeFdSet) != 0)
      {
         tInt errsv = errno;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,"pipe create failed %d",errsv);
         NORMAL_M_ASSERT_ALWAYS();
      }
      else 
      {
         pADR3CtrlData->bSCC_Comm_initOkay = TRUE;
         pADR3CtrlData->bDevAdr3Ctrl_SCC_DataReadTxActv = TRUE;
         rThreadAttr.szName        = DEV_ADR3CTRL_SCC_DATA_READ_THRD_NM;
         rThreadAttr.u32Priority   = DEV_ADR3CTRL_SCC_DATA_READ_THRD_PRIOR;
         rThreadAttr.s32StackSize  = DEV_ADR3CTRL_SCC_DATA_READ_THRD_STCKSZ;
         rThreadAttr.pfEntry       = DEV_ADR3CTRL_vSCC_DataReadThread;
         rThreadAttr.pvArg         = OSAL_NULL;
         /* This thread will be waiting for the response from ADR3Ctrl_SCC. */
         if(OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr))
         {
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                       "Thread spawn at line %d with Error =%d",
                                       __LINE__,
                                       OSAL_u32ErrorCode()
                                     );
            DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SCC_DGRAM);
            DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SCC_SOCKET);
            pADR3CtrlData->bSCC_Comm_initOkay = FALSE;
            pADR3CtrlData->bDevAdr3Ctrl_SCC_DataReadTxActv = FALSE;
         }
         else
         {
            bSCC_Comm_initOkay= TRUE;
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                       "ADR3Ctrl SCC Comm initialization success."
                                     );
         }
      }
  }
   return bSCC_Comm_initOkay;
}

tVoid DEV_ADR3CTRL_vTraceString(TR_tenTraceLevel enTraceLevel,
                                tPCChar pchFormat, ...)
{
   tU32 u32ThreadID;
   va_list argList = {0};
   va_start(argList, pchFormat);
   tInt noChar = 0;

   tU8 au8TraceBuf[ADR3CTRL_TRACE_MAX_CHARS + 5];

   /* Trace Level enabled ? */
   if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_DEV_ADR3CTRL, (tU32)enTraceLevel))
   {
      /* fill trace buffer with desired values */
      u32ThreadID = (tU32) OSAL_ThreadWhoAmI();
      OSAL_M_INSERT_T8(&au8TraceBuf[0], 0);
      OSAL_M_INSERT_T32(&au8TraceBuf[1], u32ThreadID);

      /* copy given string into the trace buffer */
      noChar = vsnprintf(&au8TraceBuf[5], ADR3CTRL_TRACE_MAX_CHARS,
            pchFormat, argList);
      va_end(argList);

      if (noChar >= ADR3CTRL_TRACE_MAX_CHARS)
        noChar = ADR3CTRL_TRACE_MAX_CHARS - 1;



      /* send gpio trace to IO-console */
      LLD_vTrace(OSAL_C_TR_CLASS_DEV_ADR3CTRL, (tU32)enTraceLevel,
            au8TraceBuf, (tU32)noChar+5);
      }
}

static tU32 DEV_ADR3CTRL_systemCall(const tPChar cmd)
{
   ADR3CTRL_TRACE(TR_LEVEL_USER_1, "%s", cmd);

   /* TODO: system() return value is always -1
    * since SIGCHLD is caught in osalproc.cpp */
   system(cmd);

   return OSAL_E_NOERROR;
}

static tU32 DEV_ADR3CTRL_fileOpen(const tPChar path,
                                  const tInt   flags,
                                  tPInt        pFd)
{
   tU32 retVal = OSAL_E_NOERROR;

   *pFd = open(path, flags);
   if (*pFd < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "open %s failed: %d", path, errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   return retVal;
}

static tU32 DEV_ADR3CTRL_fileClose(const tInt fd)
{
   if (close(fd) < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "close failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   return OSAL_E_NOERROR;
}

static tU32 DEV_ADR3CTRL_fileReadInt(const tInt fd,
                                     tPInt      pValue)
{
   tInt len;
   tChar cValue[INT_MAX_DECIMALS];

   if (fd < 0)
   {
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "Invalid file descriptor:%d",fd);
      return OSAL_E_UNKNOWN;
   }
   else
   {
      len = read(fd, cValue, INT_MAX_DECIMALS);
   }
   if (len < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "read failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   *pValue = atoi(cValue);

   return OSAL_E_NOERROR;
}

static tU32 DEV_ADR3CTRL_fileReadStr(const tInt fd,
                                     const tInt bufSize,
                                     tPChar     pBuf,
                                     tPInt      pReadBytes)
{
   tInt len;

   len = read(fd, pBuf, (tUInt)bufSize);
   if (len < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "read failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   *pReadBytes = len;

   return OSAL_E_NOERROR;
}
#ifdef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
static tU32 DEV_ADR3CTRL_fileWriteU32(const tInt fd,
                                      const tU32 value)
{
   tInt len ;
   tChar cID[U32_MAX_DECIMALS];

   len = snprintf(cID, U32_MAX_DECIMALS, "%lu", value);
   if (len <= 0 || len >= U32_MAX_DECIMALS)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "create str failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   len = write(fd, cID, strlen(cID));
   if (len <= 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "write failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   return OSAL_E_NOERROR;
}
#endif
static tU32 DEV_ADR3CTRL_fileWriteStr(const tInt   fd,
                                      const tPCChar pStr)
{
   tInt len;

   len = write(fd, pStr, strlen(pStr));
   if (len <= 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "write failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   return OSAL_E_NOERROR;
}

static tU32 DEV_ADR3CTRL_fileOpenReadStrClose(const tPChar path,
                                              const tInt bufSize,
                                              tPChar     pBuf,
                                              tPInt      pReadBytes)
{
   tU32 retVal;
   tInt fd;

   retVal = DEV_ADR3CTRL_fileOpen(path, O_RDONLY, &fd);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   retVal = DEV_ADR3CTRL_fileReadStr(fd, bufSize, pBuf, pReadBytes);
   if (retVal != OSAL_E_NOERROR) {
      DEV_ADR3CTRL_fileClose(fd);
      return retVal;
   }

   return DEV_ADR3CTRL_fileClose(fd);
}
#ifdef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
static tU32 DEV_ADR3CTRL_fileOpenWriteU32Close(const tPChar path,
                                               const tU32 value)
{
   tU32 retVal;
   tInt fd;

   retVal = DEV_ADR3CTRL_fileOpen(path, O_RDWR, &fd);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   retVal = DEV_ADR3CTRL_fileWriteU32(fd, value);
   if (retVal != OSAL_E_NOERROR) {
      DEV_ADR3CTRL_fileClose(fd);
      return retVal;
   }

   return DEV_ADR3CTRL_fileClose(fd);
}
#endif
static tU32 DEV_ADR3CTRL_fileOpenWriteStrClose(const tPChar path,
                                               const tPCChar pStr)
{
   tU32 retVal;
   tInt fd;

   retVal = DEV_ADR3CTRL_fileOpen(path, O_RDWR, &fd);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   retVal = DEV_ADR3CTRL_fileWriteStr(fd, pStr);
   if (retVal != OSAL_E_NOERROR) {
      DEV_ADR3CTRL_fileClose(fd);
      return retVal;
   }

   return DEV_ADR3CTRL_fileClose(fd);
}
#ifdef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
static tU32 DEV_ADR3CTRL_resetADR(enAdrBootMode mode)
{
   tU32 retVal = OSAL_E_NOERROR;

   if (enPreviousMode == BOOT_MODE_NORMAL && mode == BOOT_MODE_SPI)
      retVal = DEV_ADR3CTRL_initDnl();
   if (enPreviousMode == BOOT_MODE_SPI    && mode == BOOT_MODE_NORMAL)
      retVal = DEV_ADR3CTRL_exitDnl();
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   /* activate reset */
   retVal = DEV_ADR3CTRL_fileOpenWriteU32Close(ADR_RESET_GPIO_VALUE_PATH, 0);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   /* set boot mode */
   if (mode == BOOT_MODE_SPI) {
      retVal = DEV_ADR3CTRL_fileOpenWriteU32Close(ADR_BOOTSEL0_GPIO_VALUE_PATH, 1);
   } else {
      retVal = DEV_ADR3CTRL_fileOpenWriteU32Close(ADR_BOOTSEL0_GPIO_VALUE_PATH, 0);
   }
   if (retVal != OSAL_E_NOERROR)
      return retVal;
   retVal = DEV_ADR3CTRL_fileOpenWriteU32Close(ADR_BOOTSEL1_GPIO_VALUE_PATH, 0);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   /* ADR needs to be at least 5 msec in reset */
   usleep(5000);

   /* enable request trigger edge */
   if (enPreviousMode == BOOT_MODE_NORMAL && mode == BOOT_MODE_SPI)
   {
      if(TRUE == pADR3CtrlData->bSpiGpioActv)
      {
         retVal = DEV_ADR3CTRL_fileOpenWriteStrClose(ADR_REQ_GPIO_EDGE_PATH, "falling");
      }
      else
      {
         retVal = DEV_ADR3CTRL_fileOpenWriteStrClose(ADR_REQ_GPIO_EDGE_PATH_DEFAULT, "falling");
      }
      if (retVal != OSAL_E_NOERROR)
      return retVal;
   }
   /* release reset */
   retVal = DEV_ADR3CTRL_fileOpenWriteU32Close(ADR_RESET_GPIO_VALUE_PATH, 1);

   return retVal;
}
#endif

static tU32 DEV_ADR3CTRL_readNetDevName(tPChar ifname,
                                        tInt   bufSize)
{
   tU32 retVal;
   tInt readBytes;

   retVal = DEV_ADR3CTRL_fileOpenReadStrClose(ADR_IFNAME_DEVICE_TREE_PATH,
         bufSize, ifname, &readBytes);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   ADR3CTRL_TRACE(TR_LEVEL_USER_1, "netdev interface name: %s", ifname);

   return OSAL_E_NOERROR;
}

static tU32 DEV_ADR3CTRL_setNetDevState(tBool activate)
{
   tU32 retVal;
   int fd;
   struct ifreq ifr;
   char ifname[IFNAMSIZ];

   retVal = DEV_ADR3CTRL_readNetDevName(ifname, IFNAMSIZ);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   fd = socket(AF_BOSCH_INC_ADR, (tInt)SOCK_STREAM, 0);
   if (fd == -1)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "create socket failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   strncpy(ifr.ifr_name, ifname, IFNAMSIZ);

   if (ioctl(fd, SIOCGIFFLAGS, &ifr) == -1)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "ioctl SIOCGIFFLAGS on %s failed: %d",
            ifr.ifr_name, errsv);
      NORMAL_M_ASSERT_ALWAYS();
      close(fd);
      return OSAL_E_UNKNOWN;
   }

   if (activate) {
      if (ifr.ifr_flags & IFF_UP) {
         ADR3CTRL_TRACE(TR_LEVEL_USER_1, "netdev %s already up", ifname);
         close(fd);
         return OSAL_E_NOERROR;
      }
      ifr.ifr_flags |= IFF_UP;
      ADR3CTRL_TRACE(TR_LEVEL_USER_1, "netdev %s up, IFFLAGS: 0x%X",
            ifname, ifr.ifr_flags);
   } else {
      if (!(ifr.ifr_flags & IFF_UP)) {
         ADR3CTRL_TRACE(TR_LEVEL_USER_1, "netdev %s already down", ifname);
         close(fd);
         return OSAL_E_NOERROR;
      }
      ifr.ifr_flags &= ~IFF_UP;
      ADR3CTRL_TRACE(TR_LEVEL_USER_1, "netdev %s down, IFFLAGS: 0x%X",
            ifname, ifr.ifr_flags);
   }

   if (ioctl(fd, SIOCSIFFLAGS, &ifr) == -1)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "ioctl SIOCSIFFLAGS on %s failed: %d",
            ifr.ifr_name, errsv);
      NORMAL_M_ASSERT_ALWAYS();
      close(fd);
      return OSAL_E_UNKNOWN;
   }

   close(fd);

   return OSAL_E_NOERROR;
}

static tU32 DEV_ADR3CTRL_initDnl()
{
   tU32 retVal;
   tS8 dummyByte = 0;
   tInt len;

   ADR3CTRL_TRACE(TR_LEVEL_USER_1, "PID: %d", getpid());

   retVal = DEV_ADR3CTRL_setNetDevState(0);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   if(TRUE == pADR3CtrlData->bSpiGpioActv)
   {
      retVal = DEV_ADR3CTRL_systemCall(CMD_REQ_GPIO_EXPORT);
   }
   else
   {
       retVal = DEV_ADR3CTRL_systemCall(CMD_REQ_GPIO_EXPORT_DEFAULT);
   }
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   retVal = DEV_ADR3CTRL_systemCall(CMD_SPIDEV_START);
   if (retVal != OSAL_E_NOERROR)
      return retVal;
   if(TRUE == pADR3CtrlData->bSpiGpioActv)
   {
      PollFdSet[0].fd = open(ADR_REQ_GPIO_VALUE_PATH, O_RDWR);
   }
   else
   {
      PollFdSet[0].fd = open(ADR_REQ_GPIO_VALUE_PATH_DEFAULT, O_RDWR);
   }
   if (PollFdSet[0].fd < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "open %s failed: %d", 
                     (pADR3CtrlData->bSpiGpioActv)? ADR_REQ_GPIO_VALUE_PATH : ADR_REQ_GPIO_VALUE_PATH_DEFAULT, 
                     errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   PollFdSet[0].events = POLLPRI;

   /* enable request trigger edge */
   if(TRUE == pADR3CtrlData->bSpiGpioActv)
   {
      retVal = DEV_ADR3CTRL_fileOpenWriteStrClose(ADR_REQ_GPIO_EDGE_PATH, "falling");
   }
   else
   {
       retVal = DEV_ADR3CTRL_fileOpenWriteStrClose(ADR_REQ_GPIO_EDGE_PATH_DEFAULT,
                                                   "falling");
   }
   if (retVal != OSAL_E_NOERROR)
      return retVal;
      
   SpiDevFd = open(SPI_DEVICE_PATH, O_RDWR);
   if (SpiDevFd < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "open %s failed: %d", SPI_DEVICE_PATH, errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   /* write dummy byte to clear communication channel
    * TODO: analyze why this is necessary */
   len = write(SpiDevFd, &dummyByte, 1);
   if (len <= 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "write failed: %d %d", errsv, len);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   return OSAL_E_NOERROR;
}

static tU32 DEV_ADR3CTRL_exitDnl()
{
   tU32 retVal;

   ADR3CTRL_TRACE(TR_LEVEL_USER_1, "");

   if (close(PollFdSet[0].fd) < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "close file failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   if (close(SpiDevFd) < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "close file failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   retVal = DEV_ADR3CTRL_systemCall(CMD_SPIDEV_STOP);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   if(TRUE == pADR3CtrlData->bSpiGpioActv)
   {
      retVal = DEV_ADR3CTRL_systemCall(CMD_REQ_GPIO_UNEXPORT);
   }
   else
   {
      retVal = DEV_ADR3CTRL_systemCall(CMD_REQ_GPIO_UNEXPORT_DEFAULT);
   }
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   retVal = DEV_ADR3CTRL_setNetDevState(1);
   if (retVal != OSAL_E_NOERROR)
      return retVal;

   return OSAL_E_NOERROR;
}

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_bCheckIntialSequence
*
* PARAMETER    : tVoid
*
* RETURNVALUE  : 
*                
*
* DESCRIPTION  : Receive the initial sequence form ADR3Ctrl_SCC
*                Maximum time-out is 1 second. 
*                
* HISTORY      :
*--------------------------------------------------------------------------------------------------
* Date         |        Modification                            | Author & comments
*--------------|------------------------------------------------|----------------------------------
* 27.Aug.2013  |  initial  Modification                         |  Madhu Sudhan Swargam (RBEI/ECF5)
* 05.Jun.2014  |  To ignore ADR3_SCC messages till Component    |
*              |  status is received  time-out of 1 sec         |  Madhu Sudhan Swargam (RBEI/ECF5)
* --------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool DEV_ADR3CTRL_bCheckIntialSequence(tVoid)
{
   tBool bRetVal = FALSE;
   tBool bwhilewait = TRUE;
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
   tU32  u32MsgRequest = 0;
   OSAL_tMSecond mSCC_CompStatRspnTO = (OSAL_ClockGetElapsedTime() + 1000);
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   OSAL_tMSecond mCurrentTime;
   /*
     Wait till the ADR3CTRL_SCC Component status is received.
     Then react on the immediate ADR3 working mode send by ADR3CTRL_SCC
      */
   while(bwhilewait == TRUE)
   {
      if(0 < OSAL_s32MessageQueueWaitMonotonic(hDevAdr3_SCC_DataReadMQHndlr,
                                      (tPU8)&u32MsgRequest,
                                      sizeof(tU32),
                                      OSAL_NULL,
                                      (OSAL_tMSecond)ADR3CTRL_SCC_INIT_RESPONSE_TIMEOUT
                                     )
         )
      {  
           /* ADR3Ctrl_SCC is Active */
         if(DEV_ADR3CTRL_LI_HNDL_ADR3SCC_ACTV_MSG == u32MsgRequest)
         {
            //wait for next Reset Active State Continue while loop for waiting
            pADR3CtrlData->bDevAdr3Ctrl_SCC_CompntActv = TRUE;
            bRetVal = TRUE;
            bwhilewait = FALSE;
         }
         else if(DEV_ADR3CTRL_LI_HNDL_ADR3SCC_INACTV_MSG == u32MsgRequest)
         {
            bwhilewait = FALSE;
         }
      }
      else
      {
         u32ErrorCode = OSAL_u32ErrorCode();
      }
      if(OSAL_E_NOERROR != u32ErrorCode)
      {
         if(OSAL_E_TIMEOUT == u32ErrorCode)
         {  
            bwhilewait = FALSE;
         }
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "MsgQue wait Failed at line %d with Error =%d",
                                   __LINE__,
                                   u32ErrorCode
                                  );
         NORMAL_M_ASSERT_ALWAYS();                                   
      }
      mCurrentTime = OSAL_ClockGetElapsedTime();
      if(mCurrentTime >= mSCC_CompStatRspnTO)  
      {
         bwhilewait = FALSE;
      }
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                "Initial Sequence Received %u",
                                u32MsgRequest
                               );
   }
#endif
   return (bRetVal);
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_s32ResurThdInit
*
* PARAMETER    : tVoid
*
* RETURNVALUE  : 
*                
*
* DESCRIPTION  : initializes the ADR3 Linux functionality
*                initialize the ADR3 SCC Communication 
*                Check the ADR3Ctrl System Start-up Communication
* HISTORY      :
*------------------------------------------------------------------------------------------
* Date         |        Modification                    | Author & comments
*--------------|----------------------------------------|-----------------------------------
* 27.Aug.2013  |  initial  Modification                 |  Madhu Sudhan Swargam (RBEI/ECF5)
* 07.Aug.2014  |  Process dependent resource handling   |  Madhu Sudhan Swargam (RBEI/ECF5)
* -----------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 DEV_ADR3CTRL_s32ResurThdInit(tVoid)
{
   tS32 s32RetVal = OSAL_E_NOERROR; 
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
   tBool bInitOkay = FALSE;
   char sendbuf[ADR3_SCC_COMM_BUFFR_LENGHT]={0};
   /***  initialize the ADR3 Control data structures and resources     ***/
   DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,"entered DEV_ADR3CTRL_s32ResurThdInit  ");

   /***   initialize the SCC communication to read the ADR3_SCC responses    ***/
   if(FALSE == DEV_ADR3CTRL_bLinuxResource_Init())
   {
      bInitOkay = FALSE;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,"ADR3_LINUX init Failed");
   }
   else if(FALSE == DEV_ADR3CTRL_bSCC_Comm_Init())
   {
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,"IOCreate not successful ");
   }
   else
   {
      bInitOkay = TRUE;
   }
   if(bInitOkay == TRUE)
   {
   /* To get the present status of the ADR3_SCC */
      sendbuf[0] = SCC_ADR3CTRL_C_COMPONENT_STATUS;
      sendbuf[1] = ADR3_STATUS_ACTIVE;
      sendbuf[2] = ADR3_FIRST_VERSION;
      if(ADR3_SCC_COMM_BUFFR_LENGHT != dgram_send(pADR3CtrlData->hDevADR3CtrlSCC_dgram,
                                                  sendbuf,
                                                  ADR3_SCC_COMM_BUFFR_LENGHT
                                                 )
        )
      {
         bInitOkay = FALSE;
         NORMAL_M_ASSERT_ALWAYS();
      }
   }  
   if(bInitOkay == TRUE)
   {
    /* Check the initial Commands form ADR3Ctrl SCC*/
      bInitOkay = DEV_ADR3CTRL_bCheckIntialSequence(); 
   }
   if(bInitOkay == TRUE)
   {
      bInitOkay = DEV_ADR3CTRL_DriverCntrlThreadspawn();
   }
   if(bInitOkay != TRUE)
   {
      s32RetVal=OSAL_E_NOTINITIALIZED;
   }
   else
   {
      /* Wait is added so the Driver thread will serve the message from ADR3CTRL reset
         status from V850 and first Open will be always Successful*/
      pADR3CtrlData->bDrvRRsurceInit= TRUE;
      OSAL_s32ThreadWait(300);
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "ADR3Ctrl Driver initialization successfully"
                               );
   }
#else
   if(TRUE != DEV_ADR3CTRL_bGpioPortextndrDevInit())
   {
      s32RetVal=OSAL_E_NOTINITIALIZED;
   }
   /* Change the state machine to Alive*/
   /* By default the ADR3CTrl is Assumed Up and Running*/
   DEV_ADR3CTRL_vStateMachine(ADR3_ST_EVNT_NRML_RESET_REL); 
   DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                              "ADR3Ctrl GPIO port extender initialized "
                             );
#endif
   return(s32RetVal);
}
#ifdef DEV_ADR3CTRL_V850INTERFACE_INACTIVE
static tBool DEV_ADR3CTRL_bGpioPortextndrDevInit()
{
   OSAL_trThreadAttribute rThreadAttr;
   tBool bGPIOPortExtndrInitActv = FALSE;
   if (OSAL_OK != OSAL_s32MessageQueueCreate(DEV_ADR3_SCC_DATA_SEND_MQ_NAME, 10, 
                                                  sizeof(tU32), 
                                                  OSAL_EN_READWRITE, 
                                                  &hDevAdr3CtrlSCCDataSend_MQ
                                                 )
           )
   {
       DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                 "MQ create Failed at line %d with Error =%d",
                                 __LINE__,
                                 OSAL_u32ErrorCode()
                                );
   }
   else
   {  
      rThreadAttr.szName        = DEV_ADR3CTRL_SCC_DATA_SEND_THRD_NM;
      rThreadAttr.u32Priority   = DEV_ADR3CTRL_SCC_DATA_SEND_THRD_PRIOR;
      rThreadAttr.s32StackSize  = DEV_ADR3CTRL_SCC_DATA_SEND_THRD_STCKSZ;
      rThreadAttr.pfEntry       = DEV_ADR3CTRL_SCCDataSendThread;
      rThreadAttr.pvArg         = OSAL_NULL;
      if(OSAL_ERROR == OSAL_ThreadSpawn(&rThreadAttr))
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "SCCDataSendThread Failed at line %d with Error =%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
      }
      else
      {
         bGPIOPortExtndrInitActv= TRUE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,"ADR3Ctrl Threads Successfully Spawned");
      }
   }
   return(bGPIOPortExtndrInitActv);
}
#endif
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_bDrvrRsurceInitChk()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tBool
*                
*
* DESCRIPTION  :   Checks if the threads are alive else the threads are spawned 
* HISTORY      :
*-------------------------------------------------------------------------------------------------------------
* Date         |        Modification                                        | Author & comments
*--------------|------------------------------------------------------------|---------------------------------
* 06.Aug.2014  |  Initial Version                                           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -------------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool DEV_ADR3CTRL_bDrvrRsurceInitChk(tVoid)
{
   tBool bRetVal = TRUE;
   if(FALSE == pADR3CtrlData->bDrvRRsurceInit)
   {
      if((tS32)OSAL_E_NOERROR != DEV_ADR3CTRL_s32ResurThdInit()) 
      {
         bRetVal = FALSE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,"ADR3Ctrl DEVINIT FAILED ");
         NORMAL_M_ASSERT_ALWAYS(); 
      }
   }
   return bRetVal;
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vLxHndlThreadExit()
*
* PARAMETER    :  tVoid
*
* RETURNVALUE  :  tVoid 
*                
*
* DESCRIPTION  :  shut-down the thread by sending DEV_ADR3CTRL_LI_HNDL_THREAD_EXIT message
*                 
* HISTORY      :
*-------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-------------------------------------
* 06.Aug.2014  |  Initial Version                |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vLxHndlThreadExit(tVoid)
{
   tU32 u32MsgPostCommand = DEV_ADR3CTRL_LI_HNDL_THREAD_EXIT;
   if(OSAL_OK != OSAL_s32MessageQueuePost(hDevAdr3_SCC_DataReadMQHndlr,
                                          (tPU8)&u32MsgPostCommand,
                                          sizeof(tU32),       
                                          0
                                         )
     )
   {
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "MQ post Failed line %lu with error =%u",
                                __LINE__,
                                OSAL_u32ErrorCode()
                               );
      NORMAL_M_ASSERT_ALWAYS();
   }
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vDataSendThreadExit()
*
* PARAMETER    :  tVoid
*
* RETURNVALUE  :  tVoid 
*                
*
* DESCRIPTION  :  shut-down the data send thread by sending DEV_ADR3CTRL_SCC_DATASEND_THREAD_EXIT message
*                 
* HISTORY      :
*-------------------------------------------------------------------------------------------------------------
* Date         |        Modification                                        | Author & comments
*--------------|------------------------------------------------------------|---------------------------------
* 06.Aug.2014  |  Initial Version                                           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -------------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vDataSendThreadExit(tVoid)
{
   tU32 u32MsgPostCommand;
   if(hDevAdr3CtrlSCCDataSend_MQ != OSAL_C_INVALID_HANDLE)
   {
      u32MsgPostCommand = DEV_ADR3CTRL_SCC_DATASEND_THREAD_EXIT;
      if(OSAL_OK 
         != 
         OSAL_s32MessageQueuePost(hDevAdr3CtrlSCCDataSend_MQ,
                                  (tPU8)&u32MsgPostCommand,
                                  sizeof(tU32),
                                  0
                                 )
       )
      {         
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "MQ post Failed line %lu with error =%u",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vDataReadThreadExit()
*
* PARAMETER    :  tVoid
*
* RETURNVALUE  :  tVoid 
*                
*
* DESCRIPTION  :  shut-down the data read thread by triggering the poll call in data read thread
*                 
* HISTORY      :
*-------------------------------------------------------------------------------------------------------------
* Date         |        Modification                                        | Author & comments
*--------------|------------------------------------------------------------|---------------------------------
* 06.Aug.2014  |  Initial Version                                           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -------------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vDataReadThreadExit(tVoid)
{
   tChar buf = 0;
   if (write(pADR3CtrlData->UnblkPipeFdSet[1], &buf, 1) != 1)
   {
      tInt errsv = errno;
      NORMAL_M_ASSERT_ALWAYS();
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "pipe write failed %d at line =%d",
                                errsv,
                                __LINE__
                               );
   }   
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vProcResurcClose()
*
* PARAMETER    :  tVoid
*
* RETURNVALUE  :  tVoid 
*                
*
* DESCRIPTION  :  Close the resource specific to process
*                 
* HISTORY      :
*-------------------------------------------------------------------------------------------------------------
* Date         |        Modification                                        | Author & comments
*--------------|------------------------------------------------------------|---------------------------------
* 06.Aug.2014  |  Initial Version                                           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -------------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vProcResurcClose(tBool bMqResource_delete)
{
   tBool bRsurcRemovd = TRUE;
   if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
   {
      if(OSAL_OK != OSAL_s32MessageQueueClose(hDevAdr3CtrlSCCDataSend_MQ))
      {  
         bRsurcRemovd = FALSE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "MsgQueueClose Failed at line=%d,Er=%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  ); 
         NORMAL_M_ASSERT_ALWAYS(); 
        
      }
      else
      {
         hDevAdr3CtrlSCCDataSend_MQ = OSAL_C_INVALID_HANDLE;
      }
      
      if(OSAL_OK != OSAL_s32MessageQueueClose(hDevAdr3_SCC_DataReadMQHndlr))
      {  
         bRsurcRemovd = FALSE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "MsgQueueClose Failed at line=%d,Er=%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  ); 
         NORMAL_M_ASSERT_ALWAYS(); 
        
      }
      else
      {
         hDevAdr3_SCC_DataReadMQHndlr = OSAL_C_INVALID_HANDLE;
      }
      if(TRUE == bMqResource_delete)
      {
         if(OSAL_OK != OSAL_s32MessageQueueDelete(DEV_ADR3CTRL_LI_HNDLNG_MQ_NM))
         {
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "MQ Delete Failed with Error =%d",
                                      OSAL_u32ErrorCode() 
                                     );
            NORMAL_M_ASSERT_ALWAYS();
            hDevAdr3_SCC_DataReadMQHndlr = OSAL_C_INVALID_HANDLE;
         }
          if(OSAL_OK != OSAL_s32MessageQueueDelete(DEV_ADR3_SCC_DATA_SEND_MQ_NAME))
         {
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "MQ Delete Failed with Error =%d",
                                      OSAL_u32ErrorCode() 
                                     );
            NORMAL_M_ASSERT_ALWAYS();
            hDevAdr3CtrlSCCDataSend_MQ = OSAL_C_INVALID_HANDLE;
         }
         pADR3CtrlData->bDrvRRsurceInit= FALSE;
      }
      if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
      {
         bRsurcRemovd = FALSE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "UnLockOsal Failed at line=%d,Er=%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );  
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {  
      bRsurcRemovd = FALSE;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "LockOsal Failed at line=%d,Er=%d",
                                __LINE__,
                                OSAL_u32ErrorCode()
                               );  
      NORMAL_M_ASSERT_ALWAYS();
   }
   if(TRUE == bRsurcRemovd)
   {
     if(CloseOsalLock(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
      {
         bRsurcRemovd = FALSE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "CloseOsalLock Failed at line=%d,Er=%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         NORMAL_M_ASSERT_ALWAYS();   
      }
   }
      /* Check if all the resources are closed then only close the shared memory*/
   if(TRUE == bRsurcRemovd)
   {
      if (OSAL_OK != OSAL_s32SharedMemoryUnmap(&hDevAdr3CtrlShrdMemryHndl,sizeof(trADR3CtrlData)))
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "SM unmap Failed at line=%d,Er=%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         NORMAL_M_ASSERT_ALWAYS(); 
      }
      else if(OSAL_ERROR == OSAL_s32SharedMemoryClose(hDevAdr3CtrlShrdMemryHndl))
      {
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "SM Close Failed at line=%d,Er=%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         NORMAL_M_ASSERT_ALWAYS(); 
      }
      else
      {
         hDevAdr3CtrlShrdMemryHndl = OSAL_ERROR; 
      }      
   }  
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vDriverThreadCleanup()
*
* PARAMETER    :  tVoid
*
* RETURNVALUE  :  tVoid 
*                
*
* DESCRIPTION  :  Shut-down the threads used by ADR3CTRL driver
*                 
* HISTORY      :
*-------------------------------------------------------------------------------------------------------------
* Date         |        Modification                                        | Author & comments
*--------------|------------------------------------------------------------|---------------------------------
* 06.Aug.2014  |  Initial Version                                           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -------------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vDriverThreadCleanup(tVoid)
{
  //check the thread variables before shut-down
  DEV_ADR3CTRL_vLxHndlThreadExit();
  DEV_ADR3CTRL_vDataSendThreadExit();
  DEV_ADR3CTRL_vDataReadThreadExit();
  /* Wait till the thread exit its entries
     The time is approximate value, expecting all threads shut within 200 milli Seconds*/
  OSAL_s32ThreadWait(200);
  /* revert back the state machine to init state  as the 
      system doesn't handle any adr3 state machine     */
  /* TODO shut down of adr3ctrl threads will stop the communication with v850
      High risk of adr3ctrl_scc to wait for adr3ctrl_linux response but the threads are shut-down 
      this will add adr3ctrl_scc to wait for 10 seconds(approximate value)*/
   pADR3CtrlData->eDevAdr3_State = ADR3CTRL_ADR3_STATE_INIT;
   DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                             "Driver thread clean-up is done");

}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_vCloseResource()
*
* PARAMETER    :  tVoid
*
* RETURNVALUE  :  tVoid 
*                
*
* DESCRIPTION  :  Close the adr3 resource for the process 
*                 Shut-down the  threads if the close resource is for last process close
* HISTORY      :
*-------------------------------------------------------------------------------------------------------------
* Date         |        Modification                                        | Author & comments
*--------------|------------------------------------------------------------|---------------------------------
* 06.Aug.2014  |  Initial Version                                           |  Madhu Sudhan Swargam (RBEI/ECF5)
* -------------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tVoid DEV_ADR3CTRL_vCloseResource(tVoid)
{
   OSAL_tProcessID pid = OSAL_ProcessWhoAmI();
   char cBuffer[ADR3CTRL_TRACE_MAX_CHARS]={0};
   OSAL_trProcessControlBlock prPcb={0};
   tBool bMqResource_delete = FALSE;
   if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
   {
       /*  em entry for first process close even other process are using adr3 resources*/
      if((pid == pADR3CtrlData->adr3firstProcPid) 
         && 
         (pADR3CtrlData->u32ADR3MultiProcOpenCounter > (tU32)0)
        )
      {
         bMqResource_delete= TRUE;
         (tVoid)OSAL_s32ProcessControlBlock(pid,&prPcb);
         (void)snprintf(cBuffer,
                        sizeof(cBuffer),
                        "ADR First Process with name =%.15s "
                        "and Id =%d exit before last close",
                         prPcb.szName,
                         pid
                        );
         vWriteToErrMem((tS32)TR_COMP_OSALCORE,
                        (char*)&cBuffer[0],
                        (tS32)strlen(cBuffer),
                        OSAL_STRING_OUT
                       );
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,&cBuffer[0]);
         NORMAL_M_ASSERT_ALWAYS();
      }
      /* Check for last multi process close 
         The process who created the threads is detached this is error case.
         Clean the resource as the process will exit with out clearing the osal data*/
      if((TRUE == bMqResource_delete)
         || 
         (pADR3CtrlData->u32ADR3MultiProcOpenCounter == 0)
        )
      {
         /*last process close imply no one in the system are interested in the adr3 driver
           Shut down the threads for last process close is required as this process may exit 
           Leaving thread entries in osal */
         DEV_ADR3CTRL_vDriverThreadCleanup();
         //remove the first process entry
         pADR3CtrlData->adr3firstProcPid= 0;  
         bMqResource_delete = TRUE;
      }
      if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      /*last close in the process*/
      DEV_ADR3CTRL_vProcResurcClose(bMqResource_delete);
      /* Delete shared memory for last close */
      if(TRUE == bMqResource_delete)
      {
         DEV_ADR3CTRL_vReleaseResource(ADR3CTRL_RESOURCE_RELEASE_SH_MEM);
         sem_unlink("ADR_INIT");
      }
   }     
}  

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_bData_ResourceInit()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tBool TRUE or FALSE
*                
*
* DESCRIPTION  :   initialize the data and Does the Driver initialization
* HISTORY      :
*--------------------------------------------------------------------------------------------------------------
* Date         |        Modification                                        | Author & comments
*--------------|------------------------------------------------------------|----------------------------------
* 02.Nov.2013  |  Implemented Open interface                                |  Madhu Sudhan Swargam (RBEI/ECF5)
* 07.Aug.2014  |  data initialization for resource closing                  |  Madhu Sudhan Swargam (RBEI/ECF5)
* 04.Aug.2015  |  SPI23_REQ named GPIO is added                             |  Madhu Sudhan Swargam (RBEI/ECF5)
* 01.Jan.2016  |  Initialize Download process information  storing variable |  Madhu Sudhan Swargam (RBEI/ECF5)
* --------------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool DEV_ADR3CTRL_bData_ResourceInit(tVoid)
{    
   tBool bRetVal = TRUE;
   tS32 count;
   int SpiGpiofd;
   for(count =0; count < DEV_ADR3CTRL_MAX_DEVC_HNDLR; count++) 
   {
      pADR3CtrlData->rDevAdr3_InstanceState[count].fOpen=FALSE;
      pADR3CtrlData->rDevAdr3_InstanceState[count].pvStateCallback=NULL;
      pADR3CtrlData->rDevAdr3_InstanceState[count].pid=0;
   }
   memset(pADR3CtrlData->cDwnldProcNm,0,sizeof(pADR3CtrlData->cDwnldProcNm));
   pADR3CtrlData->eDevAdr3_State = ADR3CTRL_ADR3_STATE_INIT;
   pADR3CtrlData->bDevAdr3Ctrl_SCC_CompntActv = FALSE ;
   /* by default read time-out is Time-out forever */   
   pADR3CtrlData->u32ADR3ReadTimeout = OSAL_C_TIMEOUT_FOREVER; 
   /* To make ADR3 thread Alive */
#ifndef DEV_ADR3CTRL_V850INTERFACE_INACTIVE   
   pADR3CtrlData->bDevAdr3Ctrl_LxHndlTxActv = TRUE;  
   hDevAdr3_SCC_DataReadMQHndlr = OSAL_C_INVALID_HANDLE ;   
   /* To make ADR3_SCC socket read thread Alive  */
   pADR3CtrlData->bDevAdr3Ctrl_SCC_DataReadTxActv = TRUE; 
   pADR3CtrlData->s32DevADR3CtrlSCC_INCSocketFD = OSAL_NULL;      
   pADR3CtrlData->hDevADR3CtrlSCC_dgram = OSAL_NULL ;
   pADR3CtrlData->bSCC_Comm_initOkay = FALSE;
   pADR3CtrlData->bLinuxDriverInit = FALSE;
   pADR3CtrlData->u32ADR3MultiProcOpenCounter = 0;
   pADR3CtrlData->bDrvRRsurceInit= FALSE;   
#endif
   /* check whether named gpio is available. 
      If there no named GPIO "embeddedradio-gpio-adr-req" default spi_req gpio value is 24*/
   SpiGpiofd = open(ADR_REG_NAMED_GPIO_PATH, O_RDONLY);
   if(-1 == SpiGpiofd)
   {
     pADR3CtrlData->bSpiGpioActv = FALSE; 
   }
   else
   {
     pADR3CtrlData->bSpiGpioActv = TRUE;
     close(SpiGpiofd);
   }  
   /* Data used for ADR3Ctrl Socket Data Sending Thread */
   pADR3CtrlData->bDevAdr3Ctrl_SCCDataSendTxActv = TRUE;
   /* The initial State is assumed as Normal Mode*/
   enPreviousMode = BOOT_MODE_NORMAL;
      /* Only resource initialization*/
   if((tS32)OSAL_E_NOERROR != DEV_ADR3CTRL_s32ResurThdInit()) 
   {
      bRetVal = FALSE;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,"ADR3Ctrl DEVINIT FAILED ");
      NORMAL_M_ASSERT_ALWAYS(); 
   }
   return bRetVal;  
}

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_bProcResurcInit()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tBool TRUE or FALSE
*                
*
* DESCRIPTION  :   initialize the Process resource for Lock and IOControl usage
* HISTORY      :
*-------------------------------------------------------------------------------------------------------------
* Date         |        Modification                                        | Author & comments
*--------------|------------------------------------------------------------|---------------------------------
* 02.Nov.2013  |  Implemented Open interface                                |  Madhu Sudhan Swargam (RBEI/ECF5)
* 06.Aug.2014  |  Initialize the process dependent resources                |  Madhu Sudhan Swargam (RBEI/ECF5)
* -------------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool DEV_ADR3CTRL_bProcResurcInit(tVoid)
{
   tBool bRetVal = TRUE;
   hDevAdr3CtrlShrdMemryHndl = OSAL_SharedMemoryOpen(DEV_ADR3CTRL_SH_NAME,OSAL_EN_READWRITE);
   if(OSAL_ERROR == hDevAdr3CtrlShrdMemryHndl)
   {
      bRetVal = FALSE;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "SM Open Failed at line %d with Error =%d",
                                __LINE__,
                                OSAL_u32ErrorCode()
                               );
      NORMAL_M_ASSERT_ALWAYS(); 
   }
   else if (NULL !=(pADR3CtrlData = (trADR3CtrlData*)OSAL_pvSharedMemoryMap(hDevAdr3CtrlShrdMemryHndl,
                                                                            OSAL_EN_READWRITE,
                                                                            sizeof(trADR3CtrlData),
                                                                            0
                                                                           )
                     )
           )
   {
      if(OpenOsalLock(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
      {
         bRetVal = FALSE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "OpenOsalLock Failed at line %d with Error =%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         NORMAL_M_ASSERT_ALWAYS();   
      }
      /**Take the lock so the resource over write or unnecessary use before creation will be avoided*/
      if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
      {
         if(FALSE == DEV_ADR3CTRL_bDrvrRsurceInitChk())
         {
            bRetVal = FALSE;
         }
         if((OSAL_C_INVALID_HANDLE != hDevAdr3CtrlSCCDataSend_MQ) 
            && 
            (OSAL_C_INVALID_HANDLE != hDevAdr3_SCC_DataReadMQHndlr)
           )
         {
             DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL," ALREADY opened");
         }
         else if(OSAL_OK != OSAL_s32MessageQueueOpen(DEV_ADR3_SCC_DATA_SEND_MQ_NAME,
                                                 OSAL_EN_READWRITE,
                                                 &hDevAdr3CtrlSCCDataSend_MQ
                                                 )
           )
         {  
            bRetVal = FALSE;
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "MQ Open Failed at line %d with Error =%d",
                                      __LINE__,
                                      OSAL_u32ErrorCode()
                                     ); 
            NORMAL_M_ASSERT_ALWAYS(); 
         }
         else if(OSAL_OK != OSAL_s32MessageQueueOpen(DEV_ADR3CTRL_LI_HNDLNG_MQ_NM,
                                                     OSAL_EN_READWRITE,
                                                     &hDevAdr3_SCC_DataReadMQHndlr
                                                    )
           )
         {      
            bRetVal = FALSE;
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "MQ Open Failed at line %d with Error =%d",
                                      __LINE__,
                                      OSAL_u32ErrorCode()
                                     ); 
            NORMAL_M_ASSERT_ALWAYS(); 
         }
         if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
         {
            bRetVal = FALSE;
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "UnLock Failed at line %d with Error =%d",
                                      __LINE__,
                                      OSAL_u32ErrorCode()
                                     );  
            NORMAL_M_ASSERT_ALWAYS();
         }
      }
      else
      {
         bRetVal = FALSE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "Lock Failed at line %d with Error =%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   if (NULL == pADR3CtrlData)
   {
      bRetVal = FALSE;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "shMemMap Failed at line %d with Error =%d",
                                __LINE__,
                                OSAL_u32ErrorCode()
                               );
      NORMAL_M_ASSERT_ALWAYS();
   }
   return bRetVal;  
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_bOpenOperations()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tBool 
*                
*
* DESCRIPTION  :  opens the resource if the resources are not opened 
*                 Close the opened resources if the adr3scc component does not 
*                 respond or if driver initialization fails 
* HISTORY      :
*-------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-------------------------------------
* 02.Nov.2013  |  Implemented Open interface     |  Madhu Sudhan Swargam (RBEI/ECF5)
* 06.Aug.2014  |  close resource in error case   |  Madhu Sudhan Swargam (RBEI/ECF5)
* 18.Mar.2015  |  ADR3Ctrl open update CFG3-1132 |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool DEV_ADR3CTRL_bOpenOperations(tVoid)
{
   tBool bRetVal = TRUE; 
  /* Check if the process and global data are initialized*/
   if(TRUE == bDevAdr3_Init_Active)
   {
      /* Send true only if adr3_scc response for component status with in  1 sec in bCheckIntialSequence() */
      /* Also check variable for proper driver initialization  in adr_process_attach()*/
      if((FALSE == pADR3CtrlData->bDevAdr3Ctrl_SCC_CompntActv)  
          || 
         (TRUE != pADR3CtrlData->bLinuxDriverInit) 
        )
      {
         bRetVal = FALSE;
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {
      /* Driver initialization failed in process attach */
      bRetVal = FALSE;
   }
   return bRetVal;
}

/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_s32IOOpen()
*
* PARAMETER:    s32Id Channel ID
*               szName, enAccess, *pu32FD, app_id 
*
* RETURNVALUE  :  tS32 OSAL_E_NOERROR in functionality achievement
*                
*
* DESCRIPTION  :  Open the resources for the driver.
*                 Check for free handler and assign it to file descriptor
* HISTORY      :
*-------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-------------------------------------
* 12.Aug.2013  | Initial Version                 |  Smruti Sanjay Sali(RBEI/ECF5) 
* 06.Aug.2014  | Close the resource in error case|  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 DEV_ADR3CTRL_s32IOOpen(tS32 s32Id, 
                            tCString szName, 
                            OSAL_tenAccess enAccess, 
                            tU32 *pu32FD, 
                            tU16  app_id
                            )
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32Count;
   tBool bFound = FALSE;
   tBool bADR3CtrlFirstOpen ;
   /* initialize the adr3ctrl resources*/
   bADR3CtrlFirstOpen = DEV_ADR3CTRL_bOpenOperations();
   if(bADR3CtrlFirstOpen != TRUE)
   {
      s32RetVal = OSAL_E_NOTINITIALIZED;
   }
      /* Check the Access Only READWRITE is Allowed */
   if((s32RetVal == (tS32)OSAL_E_NOERROR) && (OSAL_EN_READWRITE != enAccess))
   {
      s32RetVal = OSAL_E_NOACCESS;   
   }
   if((s32RetVal == (tS32)OSAL_E_NOERROR) && (pu32FD != NULL))
   {
      if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
      {
         /* search for all devices */
         for(u32Count = 0;u32Count < (tU32)DEV_ADR3CTRL_MAX_DEVC_HNDLR;u32Count++)  
         {
            /* check for open flag */
            if(pADR3CtrlData->rDevAdr3_InstanceState[ u32Count].fOpen == FALSE)    
            {
               pADR3CtrlData->rDevAdr3_InstanceState[ u32Count].fOpen = TRUE;
               pADR3CtrlData->rDevAdr3_InstanceState[ u32Count].pid = OSAL_ProcessWhoAmI();
               /* store device number */
               *pu32FD = u32Count;      
               /* used to find the max opens in the driver open call*/
               bFound = TRUE;
               /* device found,leave loop */
               break;              
            }
         }
          /* Lock acquired in First open function is released*/
         if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
         {
            NORMAL_M_ASSERT_ALWAYS();
         }
      }
     
   }
   else if(pu32FD == NULL)
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
   }
   /* return an error code if no device is found */
   if((s32RetVal == (tS32)OSAL_E_NOERROR) && (bFound == FALSE))           
   {
      s32RetVal = OSAL_E_MAXFILES; /* return error code */
   }
   /* check if callback handler task for this process already exists */  
   s32StartCbHdrTask(s32FindProcEntry(OSAL_ProcessWhoAmI()));
   DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                             "Open s32Id=%x szName=%s,enAccess=%x,"
                             "*pu32FD=%x,app_id=%x RetVal=%x",
                             s32Id,
                             szName,
                             enAccess,
                             (pu32FD == NULL)? 0xFFFF : *pu32FD,
                             app_id,
                             s32RetVal
                           );
   return(s32RetVal);
}/*lint !e715 !e818 */
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_s32IOClose()
*
* PARAMETER    :  s32ID, u32FD
*
* RETURNVALUE  :  tS32 error codes
*                
*
* DESCRIPTION  :  Open the resources for the driver.
*                 Check for free handler and assign it to file descriptor
* HISTORY      :
*-------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-------------------------------------
* 12.Aug.2013  | Initial Version                 |  Smruti Sanjay Sali(RBEI/ECF5) 
* 06.Aug.2014  | Close the resources             |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 DEV_ADR3CTRL_s32IOClose(tS32 s32ID, tU32 u32FD)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   if(u32FD < DEV_ADR3CTRL_MAX_DEVC_HNDLR)
   {
      if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
      {
         if(pADR3CtrlData->rDevAdr3_InstanceState[u32FD].fOpen == TRUE) /* check for open flag */
         {
         /* Stops the callback execution*/
            pADR3CtrlData->rDevAdr3_InstanceState[u32FD].fOpen = FALSE;
         }
         else      /* not open */
         {
            s32RetVal = OSAL_E_BADFILEDESCRIPTOR; /* return error code */
         }
         if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
         {
            NORMAL_M_ASSERT_ALWAYS();
         }
      }
   }
   else
   {
      s32RetVal = OSAL_E_UNKNOWN; /* return error code */
   }
   DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                             "Close  s32ID=%x u32FD=%x  RetVal=%x",
                             s32ID,
                             u32FD,
                             s32RetVal
                            );
   return(s32RetVal);
}/*lint !e715 */
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_s32ResetIOCntrlHndl()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tS32 OSAL_E_NOERRORin functionality achievement
*                
*
* DESCRIPTION  :   Reset the ADR3CTRL moves the present mode to DEAD and revert back to 
                   previous mode of Operation
* HISTORY      :
*-------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-------------------------------------
* 24.Dec.2013  |  Initial Version                |  Madhu Sudhan Swargam (RBEI/ECF5)
* 01.Jan.2016  |  Add check during reset request |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 DEV_ADR3CTRL_s32ResetIOCntrlHndl()
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32MsgPostCommand;
   OSAL_trProcessControlBlock prPcb;
   tBool bProcessResetReq = FALSE;
   if(OSAL_ERROR == OSAL_s32ProcessControlBlock(OSAL_ProcessWhoAmI(), &prPcb))
    {
        s32RetVal = OSAL_E_UNKNOWN;
        NORMAL_M_ASSERT_ALWAYS();
        DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
        "OSAL_s32ProcessControlBlock line %lu with error =%u",
        __LINE__,
        OSAL_u32ErrorCode()
        );
    }
   /* Does Reset Only if the State Machine is in Active State or DNL state */
   /* If ADR3STATE is in DEAD state then the  ADR3CTRL Driver assumes the reset is in progress 
   If ADR3STATE is in DEADINIT state then the driver assumes the reset is in Progress. 
   In both the cases the reset return OSAL_E_NOERROR assuming the reset is in progress*/    
    /* Process request if driver is in active state*/
    if(((tS32)OSAL_E_NOERROR == s32RetVal)
       &&
       (pADR3CtrlData->eDevAdr3_State == ADR3CTRL_ADR3_STATE_ALIVE)
      )
    {
        bProcessResetReq = TRUE;
    }
    /* In Download mode Process request only if the reset request 
       is from the process who triggered Download mode */
    else if(((tS32)OSAL_E_NOERROR == s32RetVal)
            &&
            (pADR3CtrlData->eDevAdr3_State == ADR3CTRL_ADR3_STATE_DNL)
            && 
            (0 == strcmp(prPcb.szName, pADR3CtrlData->cDwnldProcNm))
           )
    {
        bProcessResetReq = TRUE;
    }

   if(TRUE == bProcessResetReq) 
    { 
      if(hDevAdr3CtrlSCCDataSend_MQ != OSAL_C_INVALID_HANDLE)
      {
         u32MsgPostCommand = DEV_ADR3CTRL_SCC_DATASEND_RESET_MSG;
         if(OSAL_OK 
            != 
            OSAL_s32MessageQueuePost(hDevAdr3CtrlSCCDataSend_MQ,
                                     (tPU8)&u32MsgPostCommand,
                                     sizeof(tU32),
                                     0
                                    )
           )
         {
            s32RetVal = OSAL_E_UNKNOWN;
            NORMAL_M_ASSERT_ALWAYS();
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "MsgQueu Post post Fail line %lu with err =%u",
                                      __LINE__,
                                      OSAL_u32ErrorCode()
                                     );
         }
      }
      else
      {  
         s32RetVal = OSAL_E_UNKNOWN;
      }
   }
   else
   {
      s32RetVal = OSAL_E_UNKNOWN;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                "Reset not handled state =%d, Process=%s, Download Process=%s",
                                 pADR3CtrlData->eDevAdr3_State,
                                 prPcb.szName,
                                 pADR3CtrlData->cDwnldProcNm
                               );
   }
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_s32SPIModIOCntrlHndl()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tS32 OSAL_E_NOERRORin functionality achievement
*                
*
* DESCRIPTION  :   Changes the ADR3CTRL to SPI Download mode
* HISTORY      :
*-------------------------------------------------------------------------------------------------------
* Date         |        Modification                                | Author & comments
*--------------|----------------------------------------------------|-----------------------------------
* 24.Dec.2013  |  Initial Version                                   |  Madhu Sudhan Swargam (RBEI/ECF5)
* 01.Jan.2016  |  Update download process information variable      |  Madhu Sudhan Swargam (RBEI/ECF5)
* -------------------------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 DEV_ADR3CTRL_s32SPIModIOCntrlHndl()
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32MsgPostCommand;
   OSAL_trProcessControlBlock prPcb;
   /* SPI Mode is of high priority for the System it is changed to SPI mode 
   irrespective of the ADR3 State*/
   if(pADR3CtrlData->eDevAdr3_State != ADR3CTRL_ADR3_STATE_DNL) 
   {
      if((hDevAdr3CtrlSCCDataSend_MQ != OSAL_C_INVALID_HANDLE)
         &&
         (OSAL_E_NOERROR == DEV_ADR3CTRL_initDnl()
         )
        )
      {
         u32MsgPostCommand = DEV_ADR3CTRL_SCC_DATASEND_SPIMOD_MSG;
         if(OSAL_OK 
            != 
            OSAL_s32MessageQueuePost(hDevAdr3CtrlSCCDataSend_MQ,
                                     (tPU8)&u32MsgPostCommand,
                                     sizeof(tU32),
                                     0
                                    )
           )
         {
            s32RetVal = OSAL_E_UNKNOWN;
            NORMAL_M_ASSERT_ALWAYS();
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "MQ post Failed line %lu with error =%u",
                                      __LINE__,
                                      OSAL_u32ErrorCode()
                                     );
         }
         else
         {
             if(OSAL_ERROR == OSAL_s32ProcessControlBlock(OSAL_ProcessWhoAmI(), &prPcb))
             {
                s32RetVal = OSAL_E_UNKNOWN;
                NORMAL_M_ASSERT_ALWAYS();
                DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                "OSAL_s32ProcessControlBlock line %lu with error =%u",
                __LINE__,
                OSAL_u32ErrorCode()
                );
             }
             else
             {
                /* Update download process name used during reset request */
                strncpy(pADR3CtrlData->cDwnldProcNm, prPcb.szName,sizeof(pADR3CtrlData->cDwnldProcNm));
                DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                                          "Download triggered by process =%s",
                                          pADR3CtrlData->cDwnldProcNm
                                         );
             }
         }
      }
      else
      {
         s32RetVal = OSAL_E_UNKNOWN;
      }
   } 
   return s32RetVal;
}
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_s32NORModIOCntrlHndl()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tS32 OSAL_E_NOERRORin functionality achievement
*                
*
* DESCRIPTION  :   Changes the ADR3CTRL to normal mode
* HISTORY      :
*--------------------------------------------------------------------------------------------------------------
* Date         |        Modification                                        | Author & comments
*--------------|------------------------------------------------------------|----------------------------------
* 24.Dec.2013  |  Initial Version                                           |  Madhu Sudhan Swargam (RBEI/ECF5)
* 01.Jan.2016  |  Update Download process information  storing variable     |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tS32 DEV_ADR3CTRL_s32NORModIOCntrlHndl()
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   tU32 u32MsgPostCommand;
   /* Normal mode requires no concurrency
      If the ADR3CTRL state is in DNL mode the mode is changed to ALIVE
      Else the ICONTROL returns invalid value error                    */
   if(pADR3CtrlData->eDevAdr3_State == ADR3CTRL_ADR3_STATE_DNL) 
   {
      if((hDevAdr3CtrlSCCDataSend_MQ != OSAL_C_INVALID_HANDLE)
         &&
         (OSAL_E_NOERROR == DEV_ADR3CTRL_exitDnl())
        )
      {
         u32MsgPostCommand = DEV_ADR3CTRL_SCC_DATASEND_NORMALMOD_MSG;
         if(OSAL_OK 
            != 
            OSAL_s32MessageQueuePost(hDevAdr3CtrlSCCDataSend_MQ,
                                     (tPU8)&u32MsgPostCommand,
                                     sizeof(tU32),       
                                     0
                                    )
           )
         {
            s32RetVal = OSAL_E_UNKNOWN;
            NORMAL_M_ASSERT_ALWAYS();
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "MQ post Failed line %lu with error =%u",
                                      __LINE__,
                                      OSAL_u32ErrorCode()
                                     );
         }
         else
         {
            memset(pADR3CtrlData->cDwnldProcNm,0,sizeof(pADR3CtrlData->cDwnldProcNm));
         }
      }
      else
      {
         s32RetVal = OSAL_E_UNKNOWN; 
      }
   }
   else
   {
      s32RetVal = OSAL_E_INVALIDVALUE;
   } 
   return s32RetVal; 
}
/*****************************************************************************
* FUNCTION:   DEV_ADR3CTRL_s32IOControl()
* PARAMETER:    s32ID
                u32FD   
                s32Fun
                s32Arg - Argument to be passed to function.
               
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  SPI Control function:
             OSAL_C_S32_IOCTRL_ADR3CTRL_REGISTER_RESET_CALLBACK
             OSAL_C_S32_IOCTRL_ADR3CTRL_RESET_ADR3
             OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_SPI
             OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_NORMAL
             OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BLOCKINGMODE_READ_TIMEOUT
* HISTORY:   12.08.2013   | Initial Implementation |  Smruti Sanjay Sali(RBEI/ECF5)
*            24.12.2013   | Reset Concurrency      |  Madhu Sudhan Swargam (RBEI/ECF5)
******************************************************************************/
tS32 DEV_ADR3CTRL_s32IOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun, tS32 s32Arg)
{
   tS32 s32RetVal = OSAL_E_NOERROR;
   trDevAdr3_InstanceState *prInstanceState = NULL;
   if(u32FD < DEV_ADR3CTRL_MAX_DEVC_HNDLR)
   {
      if(pADR3CtrlData->rDevAdr3_InstanceState[ u32FD].fOpen == TRUE)  /* check for open flag */
      {
         prInstanceState = &(pADR3CtrlData->rDevAdr3_InstanceState[ u32FD]);
         s32RetVal = OSAL_E_NOERROR;
      }
      else  /* not open */
      {
      s32RetVal = OSAL_E_DOESNOTEXIST; /* return error code */
      }
   }
   else
   {
      s32RetVal = OSAL_E_DOESNOTEXIST; /* return error code */
   }

   if(s32RetVal == (tS32)OSAL_E_NOERROR)
   {
      switch(s32Fun)
      {
         case OSAL_C_S32_IOCTRL_ADR3CTRL_REGISTER_RESET_CALLBACK :
         {   
            if(prInstanceState != NULL)
            {
               if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
               {
               /* Update the CallBack information */
                  prInstanceState->pvStateCallback = (adr3_tVPCallBackFunc) s32Arg; 
                  /* ensure callback handling could happened for this process */
                  (void)s32StartCbHdrTask(s32FindProcEntry(OSAL_ProcessWhoAmI()));
                  if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
                  {
                     NORMAL_M_ASSERT_ALWAYS();
                  }
               }
               if(prInstanceState->pvStateCallback != NULL)
               { 
                  prInstanceState->pvStateCallback((tU32)pADR3CtrlData->eDevAdr3_State);
               }
            }
            break;
         }
         case OSAL_C_S32_IOCTRL_ADR3CTRL_RESET_ADR3 :
         {
            if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
            {
               s32RetVal = DEV_ADR3CTRL_s32ResetIOCntrlHndl();
               if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
               {
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            break;            
         }
         case OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_SPI :
         { 
            if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
            {
               s32RetVal = DEV_ADR3CTRL_s32SPIModIOCntrlHndl();
               if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
               {
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            break;
         }
         case OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_NORMAL :
         {
            if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
            {
               s32RetVal = DEV_ADR3CTRL_s32NORModIOCntrlHndl();
               if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
               {
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            break;
         }
         case OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BLOCKINGMODE_READ_TIMEOUT:
         {
            if(LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK)
            {
               pADR3CtrlData->u32ADR3ReadTimeout = (tU32)s32Arg;
               if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
               {
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            break;
         }
         default : 
            s32RetVal = OSAL_E_INVALIDVALUE; /* return error code */
         break;
      }
   }
   DEV_ADR3CTRL_vTraceString(TR_LEVEL_USER_4,
                             "IOControl:s32ID=%x u32FD=%x s32Fun=%x RetVal=%x",
                             s32ID,
                             u32FD,
                             s32Fun,
                             s32RetVal
                           );
  return(s32RetVal);
}/*lint !e715 */


/*****************************************************************************
* FUNCTION:   DEV_ADR3CTRL_s32IORead()
* PARAMETER:    s32ID
                u32FD Channel ID, pBuffer -to put received data,
                pBuffer
                u32Size -no of bytes to be read ,ret_size - no of bytes that have read.
                ret_size
                
* RETURNVALUE:  tS32 error codes

* DESCRIPTION:  Reads the data from requested SPI Channel.
* HISTORY:      12.08.2013   | Initial Implementation |  Smruti Sanjay Sali(RBEI/ECF5)
******************************************************************************/
tS32 DEV_ADR3CTRL_s32IORead(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
   tS32 s32RetVal;
   tInt len;
   tInt reqVal;
   tU8 abLength[2]={0,0};
   tU16 wLength;
   tU8 abData[C_MAX_STATUS_LENGTH+2];

#ifdef ADR3CTRL_VERBOSE_TRACE
   ADR3CTRL_TRACE(TR_LEVEL_FATAL, "u32Size=%d", u32Size);
#endif

   /*
    * TODO: required to unblock the poll call on reset?
    */
   PollFdSet[0].revents = 0;
   len = poll(PollFdSet, 1, (tInt)pADR3CtrlData->u32ADR3ReadTimeout);

   if (len < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "poll(2) failed: %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   if (len == 0)
   {
      ADR3CTRL_TRACE(TR_LEVEL_USER_1, "poll(2) timeout");
      return 0;
   }

   if (PollFdSet[0].revents & POLLPRI) {
#ifdef ADR3CTRL_VERBOSE_TRACE
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "POLLPRI");
#endif

      len = lseek(PollFdSet[0].fd, 0, SEEK_SET);
      if (len < 0)
      {
         int errsv = errno;
         ADR3CTRL_TRACE(TR_LEVEL_FATAL, "seek error %d", errsv);
         NORMAL_M_ASSERT_ALWAYS();
         return OSAL_E_UNKNOWN;
      }

      len = (tInt)DEV_ADR3CTRL_fileReadInt(PollFdSet[0].fd, &reqVal);
      if (len != (tInt)OSAL_E_NOERROR)
      {
         return len;
      }
#ifdef ADR3CTRL_VERBOSE_TRACE
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "reqValue: %d", reqVal);
#endif
   }

   /*
    * read frame size
    */
   len = read(SpiDevFd, abLength, 2);
   if (len <= 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "read failed: %d %d", errsv, len);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   wLength = (abLength[0]<<8) | abLength[1];
#ifdef ADR3CTRL_VERBOSE_TRACE
   ADR3CTRL_TRACE(TR_LEVEL_FATAL, "wLength: %d", wLength);
#endif

   if (wLength == 0) {
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "wLength is 0");
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   if (wLength > C_MAX_STATUS_LENGTH) {
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "wLength > C_MAX_STATUS_LENGTH");
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "wLength %d abLength[0] %d abLength[1] %d",
            wLength, abLength[0], abLength[1]);
      wLength = C_MAX_STATUS_LENGTH;
   }

   /*
    * read actual data
    */
   len = read(SpiDevFd, abData, wLength-2);
   if (len <= 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "read failed: %d %d", errsv, len);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }

   /* limit data length to application buffer size */
   if (wLength > u32Size) {
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "wLength > u32Size");
      wLength = (tU16)u32Size;
   }

   pBuffer[0] = (tS8)abLength[0];
   pBuffer[1] = (tS8)abLength[1];
   memcpy(&(pBuffer[2]), abData, wLength-2);

#ifdef ADR3CTRL_VERBOSE_TRACE
   fprintf(stderr, "read buffer:");
   for (i = 0; i < wLength; i++)
      fprintf(stderr, " %02x", (tU8)pBuffer[i]);
   fprintf(stderr, "\n");
#endif

   s32RetVal = wLength;

   return(s32RetVal);
}/*lint !e715 !e818 */

/**************************************************************************************************************
* FUNCTION:     DEV_ADR3CTRL_s32IOWrite()
* PARAMETER:    s32ID
                u32FD - Channel ID, 
                pcs8Buffer - Buffer, 
                u32Size - No of bytes.
                ret_size
                
* RETURNVALUE:  tS32 error codes
   
* DESCRIPTION:   Writes Data to SPI Channel.
* HISTORY:      12.08.2013   | Initial Implementation |  Smruti Sanjay Sali(RBEI/ECF5)
*               26.05.2016   | PSARCCB-8674 Clear poll event before every write | Madhu Sudhan Swargam (RBEI/ECF5)
*********************************************************************************************************************/

tS32 DEV_ADR3CTRL_s32IOWrite(tS32 s32ID, tU32 u32FD, tPCS8 pcs8Buffer, tU32 u32Size, tU32 *ret_size)
{
  tS32 s32RetVal;
  tInt len;
  tInt reqVal;

  if (WriteCnt % 100 == 0)
     ADR3CTRL_TRACE(TR_LEVEL_USER_1, "size=%d cnt=%d", u32Size, WriteCnt);
  WriteCnt++;
/**
PSARCCB-8674
Analysis:
      In download sequence adr3 is pulled to download mode,
the GPIO poll events are cleared.Download enabling software is written
to adr3, and then the adr3 will respond for downloading the new software.
When there is a read data available then the gpio poll event is triggered 
by adr3.


   The behaviour of adr3 is unknown until the adr3 download enabling software 
is written to adr3.This imply there might be some triggered on the GPIO line
during the initial start-up and while writing the download enabling software.
In the sequence,we might get some false triggering for first poll() call.


Changes:
   The poll event is cleared before write to the adr3, so the false triggered 
is cleared before the actual poll event occurred. 
***/
   /* Clear the request line poll event before every write*/
   len = lseek(PollFdSet[0].fd, 0, SEEK_SET);
   if (len < 0)
   {
      int errsv = errno;
      ADR3CTRL_TRACE(TR_LEVEL_FATAL, "seek error %d", errsv);
      NORMAL_M_ASSERT_ALWAYS();
      return OSAL_E_UNKNOWN;
   }
   len = (tInt)DEV_ADR3CTRL_fileReadInt(PollFdSet[0].fd, &reqVal);
   if (len != (tInt)OSAL_E_NOERROR)
   {
      return len;
   }
#ifdef ADR3CTRL_VERBOSE_TRACE
  if (u32Size < 100) {
     fprintf(stderr, "write buffer:");
     for (i = 0; i < u32Size; i++)
        fprintf(stderr, " %02x", (tU8)pcs8Buffer[i]);
     fprintf(stderr, "\n");
  }
#endif

  len = write(SpiDevFd, pcs8Buffer, u32Size);
  if (len <= 0)
  {
     int errsv = errno;
     ADR3CTRL_TRACE(TR_LEVEL_FATAL, "write failed: %d %d", errsv, len);
     NORMAL_M_ASSERT_ALWAYS();
     return OSAL_E_UNKNOWN;
  }
#ifdef ADR3CTRL_VERBOSE_TRACE
  ADR3CTRL_TRACE(TR_LEVEL_FATAL, "written: %d", len);
#endif

  s32RetVal = len;

  return(s32RetVal);
}/*lint !e715 !e818 */
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_bDriverInit()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tVoid
*                
*
* DESCRIPTION  :
*                First process in the power cycle will initialize the driver shared resources 
*                and spawn threads to handle the state machine
*                Preceding process will open the resources, if required initialize the threads
*                Shared resource will be alive throughout the power cycle
*                 
*   
* HISTORY      :
*-------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-------------------------------------
* 06.Aug.2014  |  Initial Version                |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
static tBool DEV_ADR3CTRL_bDriverInit(tVoid)
{
   tBool bRetVal = TRUE;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   OSAL_tShMemHandle hshmemhandle;
   /* create shared memory to shared data and state machine information between multiple process*/
   hshmemhandle = OSAL_SharedMemoryCreate(DEV_ADR3CTRL_SH_NAME, 
                                          OSAL_EN_READWRITE, 
                                          sizeof(trADR3CtrlData)
                                         );
   /* Shared memory created */
   if(OSAL_ERROR != hshmemhandle)
   {
      pADR3CtrlData = (trADR3CtrlData*)OSAL_pvSharedMemoryMap(hshmemhandle,
                                                              OSAL_EN_READWRITE,
                                                              sizeof(trADR3CtrlData),
                                                               0
                                                              );
      
      if(NULL != pADR3CtrlData)
      {
         hDevAdr3CtrlShrdMemryHndl = hshmemhandle;
         memset(pADR3CtrlData,0,sizeof(trADR3CtrlData));
         if(OSAL_OK != CreateOsalLock(&pADR3CtrlData->rADR3Ctrl_Lock,
                                      DEV_ADR3CTRL_LOCK_NAME
                                     )
           )
         {
            bRetVal = FALSE;
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "CreLock Failed at line=%d with Err =%d",
                                      __LINE__,
                                      OSAL_u32ErrorCode()
                                     );
            NORMAL_M_ASSERT_ALWAYS();   
         }
         /* Acquire the lock so the Initialization happens first */
         if((bRetVal == TRUE) && (LockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) == OSAL_OK))
         {
            bRetVal = DEV_ADR3CTRL_bData_ResourceInit();
            if(UnLockOsal(&pADR3CtrlData->rADR3Ctrl_Lock) != OSAL_OK)
            {
               bRetVal = FALSE;
               DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                         "unLock Failed at line %d with Err =%d",
                                         __LINE__,
                                         OSAL_u32ErrorCode()
                                        );
               NORMAL_M_ASSERT_ALWAYS();
            }
         }
         else
         {
            bRetVal = FALSE;
            DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                      "Lock Failed at line %d with Err =%d",
                                      __LINE__,
                                      OSAL_u32ErrorCode()
                                     );
            NORMAL_M_ASSERT_ALWAYS();
         }
         if(TRUE == bRetVal)
         {
         /*Variable is used by  driver open to check whether adr3 resource are initialized properly or not*/
            pADR3CtrlData->bLinuxDriverInit = TRUE;
         }         
      }
      else 
      {
         bRetVal = FALSE;
         DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                                   "Osalmemmap Failed at line %d with Err =%d",
                                   __LINE__,
                                   OSAL_u32ErrorCode()
                                  );
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {
      u32ErrorCode = OSAL_u32ErrorCode();
   }
   
   if((OSAL_E_ALREADYEXISTS != u32ErrorCode) && (u32ErrorCode != OSAL_E_NOERROR))
   {
      bRetVal = FALSE;
      DEV_ADR3CTRL_vTraceString(TR_LEVEL_FATAL,
                          "shm create failed at line %d with Err =%d",
                          __LINE__,
                          u32ErrorCode
                         );
      NORMAL_M_ASSERT_ALWAYS(); 
   }
   return bRetVal;
}

#ifdef LOAD_ADR3CTRL_SO
/*********************************************************************************************************************
* FUNCTION     : adr_process_attach()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tVoid
*                
*
* DESCRIPTION  : The attach is called when the process first call the library functions
*   
* HISTORY      :
*-------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-------------------------------------
* 06.Aug.2014  |  Initial Version                |  Madhu Sudhan Swargam (RBEI/ECF5)
* 27.Jun.2016  |  NULL exception handling        |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
static sem_t* initadr_lock;
void __attribute__ ((constructor)) adr_process_attach(void)
{
#ifndef OSAL_GEN3_SIM 
   //No data is initialized , Init part happens in the first open from the process.
   //Driver is initialized  when the First process does the open
   tBool bSemwait_Actv = TRUE;
   initadr_lock = sem_open("ADR_INIT",O_EXCL | O_CREAT, OSAL_ACCESS_RIGTHS, 0);
   if (initadr_lock != SEM_FAILED)
   {
      if(s32OsalGroupId)
      {
         if(chmod("/dev/shm/sem.ADR_INIT", OSAL_ACCESS_RIGTHS) == -1)
         {
            vWritePrintfErrmem("ADR_INIT  chmod -> chmod error %d \n",errno);
         }
         if(chown("/dev/shm/sem.ADR_INIT",(uid_t)-1,(tU32)s32OsalGroupId) == -1)
         {
            vWritePrintfErrmem("ADR_INIT  -> chown error %d \n",errno);
         }
      }
      bDevAdr3_Init_Active = DEV_ADR3CTRL_bDriverInit();
   }
   else
   {
      if (errno != EEXIST)
      {
         vWritePrintfErrmem("ADR_INIT sem_open EEXIST error %d : ret val %d \n",errno,initadr_lock);
         NORMAL_M_ASSERT_ALWAYS();
         bSemwait_Actv = FALSE;
      }

      initadr_lock= sem_open("ADR_INIT", 0);
      if (initadr_lock == SEM_FAILED)
      {
         vWritePrintfErrmem("ADR_INIT sem_open error %d : ret val %d \n",errno,initadr_lock);
         NORMAL_M_ASSERT_ALWAYS();
         bSemwait_Actv = FALSE;
      }
      while(TRUE == bSemwait_Actv)
      {
         /* wait until subsystem is installed */
         if(sem_wait(initadr_lock) != 0)
         {
         /* check for incoming signal */
            if(errno != EINTR)
            {
               FATAL_M_ASSERT_ALWAYS();
            }
         }
         else
         {
            bDevAdr3_Init_Active = DEV_ADR3CTRL_bProcResurcInit();
            bSemwait_Actv = FALSE;
         }
      }
   }
   if(TRUE == bDevAdr3_Init_Active)
   {
      /* open count within a multiple process*/
      pADR3CtrlData->u32ADR3MultiProcOpenCounter++;
      if(pADR3CtrlData->u32ADR3MultiProcOpenCounter == 1)
      {
      /* Update the information of first process who initialize the threads */
         pADR3CtrlData->adr3firstProcPid = OSAL_ProcessWhoAmI();
      }
   }
   if(SEM_FAILED != initadr_lock)
   {
      sem_post(initadr_lock); 
      sem_close(initadr_lock);
   }
#endif
}
/*********************************************************************************************************************
* FUNCTION     : vOnAdrProcessDetach()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tVoid
*                
*
* DESCRIPTION  : Close the resources which are opened by adr_process_attach function
*                 
*   
* HISTORY      :
*-------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-------------------------------------
* 09.Sep.2014  |  Initial Version                |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
void __attribute__ ((destructor))vOnAdrProcessDetach(void)
{
#ifndef OSAL_GEN3_SIM 
   if(0 < pADR3CtrlData->u32ADR3MultiProcOpenCounter)
               pADR3CtrlData->u32ADR3MultiProcOpenCounter--;
   DEV_ADR3CTRL_vCloseResource();
#endif
}

#ifdef OSAL_GEN3_SIM
/*********************************************************************************************************************
* FUNCTION     : DEV_ADR3CTRL_s32LSIMIOControl()
*
* PARAMETER    :  
*
* RETURNVALUE  :  tS32 OSAL_E_NOERROR in all cases                
*
* DESCRIPTION  :   Process for REGISTER_RESET_CALLBACK with callback execution
* HISTORY      :
*-------------------------------------------------------------------------------------
* Date         |        Modification             | Author & comments
*--------------|---------------------------------|-------------------------------------
* 04.Aug.2014  |  Initial Version                |  Madhu Sudhan Swargam (RBEI/ECF5)
* ------------------------------------------------------------------------------------
*********************************************************************************************************************/
tS32 DEV_ADR3CTRL_s32LSIMIOControl(tS32 s32ID, tU32 u32FD, tS32 s32Fun, tS32 s32Arg)
{
   (tVoid)s32ID;
   (tVoid)u32FD;
   tS32 s32RetVal = OSAL_E_NOERROR;
   adr3_tVPCallBackFunc  pvStateCallback;
   if((s32Arg != (tS32)NULL) 
      && 
      (OSAL_C_S32_IOCTRL_ADR3CTRL_REGISTER_RESET_CALLBACK == s32Fun)
     )
   {
      pvStateCallback = (adr3_tVPCallBackFunc)s32Arg;
      pvStateCallback((tU32)ADR3CTRL_ADR3_STATE_ALIVE);
      s32RetVal = OSAL_E_NOERROR;
   }
   return s32RetVal;
}
#endif

tS32 adr3ctrl_drv_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id)
{
#ifdef OSAL_GEN3_SIM
   (tVoid)s32Id;
   (tVoid)szName;
   (tVoid)enAccess;
   (tVoid)pu32FD;
   (tVoid)app_id;
   return OSAL_E_NOERROR;
#else
   return DEV_ADR3CTRL_s32IOOpen(s32Id, szName, enAccess, pu32FD, app_id);
#endif
}

tS32 adr3ctrl_drv_io_close(tS32 s32ID, tU32 u32FD)
{
#ifdef OSAL_GEN3_SIM
   (tVoid)s32ID;
   (tVoid)u32FD;
   return OSAL_E_NOERROR;
#else
   return DEV_ADR3CTRL_s32IOClose(s32ID, u32FD);
#endif    
}

tS32 adr3ctrl_drv_io_control(tS32 s32ID, tU32 u32FD, tS32 s32Fun, tS32 s32Arg)
{
#ifdef OSAL_GEN3_SIM
   return DEV_ADR3CTRL_s32LSIMIOControl(s32ID, u32FD, s32Fun, s32Arg);
#else
   return DEV_ADR3CTRL_s32IOControl(s32ID, u32FD, s32Fun, s32Arg);
#endif
}

tS32 adr3ctrl_drv_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size)
{
#ifdef OSAL_GEN3_SIM 
   (tVoid)s32ID;
   (tVoid)u32FD;
   (tVoid)u32Size;
   (tVoid)pBuffer;
   (tVoid)ret_size;
   return OSAL_E_NOERROR;
#else
   return DEV_ADR3CTRL_s32IORead(s32ID, u32FD, pBuffer, u32Size, ret_size);
#endif   
}

tS32 adr3ctrl_drv_io_write(tS32 s32ID, tU32 u32FD, tPCS8 pcs8Buffer, tU32 u32Size, tU32 *ret_size)
{
#ifdef OSAL_GEN3_SIM 
   (tVoid)s32ID;
   (tVoid)u32FD;
   (tVoid)pcs8Buffer;
   (tVoid)u32Size;
   (tVoid)ret_size;
   return OSAL_E_NOERROR;
#else
   return DEV_ADR3CTRL_s32IOWrite(s32ID, u32FD, pcs8Buffer, u32Size, ret_size);
#endif   
}

#endif

