/*********************************************************************************************************************
* FILE         : dev_adr3ctrl.h
*
* DESCRIPTION  : This is the header file corresponding todev_adr3ctrl.c
*
* AUTHOR(s)    : Smruti Sanjay Sali(RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------------
* Date         |       Version               | Author & comments
*--------------|-----------------------------|---------------------------
* 12.08.2013   | Initial version: 1.0        | Smruti Sanjay Sali(RBEI/ECF5)
* 03.09.2013   | Added ADR3 init function    | Madhu Sudhan Swargam (RBEI/ECF5) 
* 03.Mar.2014  | Adding teDevAdr3_State enum | Madhu Sudhan Swargam (RBEI/ECF5) 
* ------------------------------------------------------------------------------
*********************************************************************************************************************/
#ifndef DEV_ADR3_HEADER
#define DEV_ADR3_HEADER

/* ADR3 States */
typedef enum 
{
   ADR3CTRL_ADR3_STATE_INIT       = 0,
   ADR3CTRL_ADR3_STATE_DEAD       = 1,
   ADR3CTRL_ADR3_STATE_ALIVE      = 2,
   ADR3CTRL_ADR3_STATE_DNL        = 3, 
   ADR3CTRL_ADR3_STATE_INVALID    = 4,
   ADR3CTRL_ADR3_STATE_DEADINIT   = 5
}teDevAdr3_State;


tS32 DEV_ADR3CTRL_s32IOOpen( tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, tU32 *pu32FD, tU16  app_id);
tS32 DEV_ADR3CTRL_s32IOClose( tS32 s32ID, tU32 u32FD);
tS32 DEV_ADR3CTRL_s32IOControl( tS32 s32ID, tU32 u32FD, tS32 s32fun, tS32 s32arg);
tS32 DEV_ADR3CTRL_s32IORead( tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, tU32 *ret_size);
tS32 DEV_ADR3CTRL_s32IOWrite( tS32 s32ID, tU32 u32FD, tPCS8 pcs8Buffer, tU32 u32Size, tU32 *ret_size);
tS32 DEV_ADR3CTRL_s32IODeviceInit( tVoid);

#endif