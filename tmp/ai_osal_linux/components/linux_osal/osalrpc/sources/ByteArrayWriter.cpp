#include "string.h"
#include "ByteArrayWriter.h"
#include "string"



ByteArrayWriter::ByteArrayWriter (unsigned char *pucByteArray, unsigned int u32ArraySize)
: m_pucByteArray (pucByteArray)
, m_u32ArraySize (u32ArraySize)
, m_u32Pos (0)
{
}

unsigned int ByteArrayWriter::u32GetActualDataSize()
{
   return m_u32Pos;
}


bool ByteArrayWriter::bWriteU32   (unsigned int u32Value)
{
   return bWriteRaw ((unsigned char *) &u32Value, sizeof(unsigned int));
}

bool ByteArrayWriter::bWriteS32   (int s32Value)
{
   return bWriteRaw ((unsigned char *) &s32Value, sizeof(int));
}

bool ByteArrayWriter::bWriteString (const char *pcString)
{
   return bWriteRaw ((unsigned char *) pcString, strlen (pcString) + 1);
}


bool ByteArrayWriter::bWriteRaw (unsigned char *data, unsigned int size)
{
   /*
    *  Groessencheck:
    */
   bool bSuccess = false;
   if ( (m_u32Pos + size) <= m_u32ArraySize )
   {
      memcpy (m_pucByteArray+m_u32Pos, data, size);
      m_u32Pos += size;
      bSuccess = true;
   }
   return bSuccess;
}

