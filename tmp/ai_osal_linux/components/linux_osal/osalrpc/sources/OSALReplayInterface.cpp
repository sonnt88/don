// OSALInterface.cpp: implementation of the OSALInterface class.
//
//////////////////////////////////////////////////////////////////////

#include "fstream"
#include "socket.h"

#include "OSALInterface.h"
#include "OSALReplayInterface.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

OSALReplayInterface::OSALReplayInterface()
{
   szReplayFileName = 0;
}

OSALReplayInterface::OSALReplayInterface(const char* coszReplayFileName)
{
   if (coszReplayFileName)
   {
      szReplayFileName = new char[strlen(coszReplayFileName)+1];
      strcpy(szReplayFileName,coszReplayFileName);
   }
}

OSALReplayInterface::~OSALReplayInterface()
{
    if (szReplayFileName != NULL)
        delete[] szReplayFileName;
}

bool OSALReplayInterface::bInit(bool bEnableLog)
{
   oLogStream.open(szReplayFileName);
   return oLogStream.good();
}

bool OSALReplayInterface::bEnd()
{
   if (oLogStream)
      oLogStream.close();
   return true;
}

int OSALReplayInterface::s32MsgQueueWait(unsigned long u32Handle, unsigned long u32BufSize, unsigned long u32TimeOut, unsigned char*pu8MsgBuf,unsigned long &u32Prio)
{
   static unsigned int u32MsgCounter = 0;
   unsigned int u32MsgLength;
   if (oLogStream.good() && (enRunMode != EN_STOP_MODE))
   {
      char szLine[256];

      do 
      {
         do 
         {
            oLogStream.getline(szLine,255);
         } while (oLogStream.good() && (strstr(szLine,"incoming message")==0));
         if (oLogStream.good())
            sscanf(strchr(szLine,'=') + 2,"%d",&u32MsgLength);
      } while (oLogStream.good() && (enRunMode == EN_STEP_MODE) && (u32MsgLength == 0));

      if (enRunMode == EN_STEP_MODE)
         enRunMode = EN_STOP_MODE;

      unsigned int u32BytesRemaining = u32MsgLength;

      unsigned char* pu8MsgPtr = pu8MsgBuf;
      while ((u32BytesRemaining > 0) && oLogStream.good())
      {
         oLogStream.getline(szLine,255);
         szLine[strlen(szLine)+1] = '\0'; // be sure that end is properly found !
         char* ps8Start = szLine;
         while (isxdigit(*ps8Start))
         {
            unsigned char u8Byte = strtol(ps8Start,0,16);
            *(pu8MsgPtr++) = u8Byte;
            ps8Start += 3;
            u32BytesRemaining--;

         }
      }
      u32MsgCounter++;
   } 
   else 
   {
      u32MsgLength = 0;
   }
   return u32MsgLength;
}

int OSALReplayInterface::s32MsgQueuePost(unsigned long u32Handle, unsigned long u32MsgSize, unsigned char* ps8Msg, unsigned long u32Prio)
{
   return SUCCESS;
}

int OSALReplayInterface::s32MsgPoolClose()
{
   return SUCCESS;
}

int OSALReplayInterface::s32MsgPoolOpen()
{
   return SUCCESS;
}

int OSALReplayInterface::s32MsgQueueClose(long s32Handle)
{
   return SUCCESS;
}

int OSALReplayInterface::s32MsgQueueOpen(const char* szQueueName,unsigned long u32Access,unsigned long& u32Handle)
{
   return SUCCESS;
}

int OSALReplayInterface::s32MsgQueueCreate(const char* szQueueName,unsigned long s32MaxMsgs, unsigned long s32MaxLength, unsigned long u32Access, unsigned long& u32Handle)
{
   return SUCCESS;
}

int OSALReplayInterface::s32MsgQueueDelete(const char* szQueueName)
{
   return SUCCESS;
}

void OSALReplayInterface::vSetRunMode(tenRunMode enRunMode)
{
   this->enRunMode = enRunMode;
}

OSALReplayInterface::tenRunMode OSALReplayInterface::enGetRunMode()
{
   return enRunMode;
}



int OSALReplayInterface::hSharedMemoryCreate   ( const char* szName,
                                                 int access, 
                                                 unsigned int u32Size)
{
   return -1;
}


int OSALReplayInterface::hSharedMemoryOpen     (const char* szName, int access)
{
   return -1;
}

int OSALReplayInterface::s32SharedMemoryClose  (int handle)
{
   return -1;
}

int OSALReplayInterface::s32SharedMemoryDelete (const char *szName)
{
   return -1;
}


void *OSALReplayInterface::pvSharedMemoryMap     ( int handle, 
                                                   int access, 
                                                   unsigned int length, 
                                                   unsigned int offset  )
{
   return NULL;
}


int OSALReplayInterface::s32SharedMemoryUnmap    (void * sharedMemory, unsigned int size)
{
   return -1;
}


int OSALReplayInterface::s32MsgPoolCreate(unsigned long u32PoolSize)
{
   return -1;
}


int OSALReplayInterface::s32MsgPoolDelete()
{
   return -1;
}


int OSALReplayInterface::s32IOOpen(unsigned long u32Access, const char* szFileName)
{
   return -1;
}

int OSALReplayInterface::s32IOCreate(unsigned long u32Access, const char* szFileName)
{
   return -1;
}

int OSALReplayInterface::s32IOClose(long s32Descriptor)
{
   return -1;
}

int OSALReplayInterface::s32IOCtrl(long s32Descriptor, long s32Fun, long s32Size, unsigned char *ps8Arg)
{
   return -1;
}

int OSALReplayInterface::s32IOCtrl(long s32Descriptor, long s32Fun, long s32Arg)
{
   return -1;
}


int OSALReplayInterface::s32IORemove(const char* szFileName)
{
   return -1;
}


int OSALReplayInterface::s32IORead(long s32Descriptor, unsigned long u32BufSize, unsigned char* pu8Buf)
{
   return -1;
}

int OSALReplayInterface::s32IOWrite(long s32Descriptor, unsigned long u32Size, unsigned char* pu8Buf)
{
   return -1;
}


int OSALReplayInterface::s32IOEngFct (const char *device, long s32Fun, unsigned char *ps8Arg, long s32ArgSize)
{
   return -1;
}

int OSALReplayInterface::s32IOFakeExclAccess (const char *device)
{
   return -1;
}

int OSALReplayInterface::s32IOReleaseFakedExclAccess (const char *device)
{
   return -1;
}


/* This function only works with osal_pure/NT and application started with MiniSPM! */
int OSALReplayInterface::s32MiniSPMInitiateShutdown()
{
   return -1;
}

int OSALReplayInterface::s32FSeek(long fd,int offset, int origin)
{
    return -1;
}

int OSALReplayInterface::s32FTell(long fd)
{
    return -1;
}

int OSALReplayInterface::s32FGetpos(long fd,int *ptr)
{
    return -1;
}

int OSALReplayInterface::s32FSetpos(long fd,const int *ptr)
{
    return -1;
}

int OSALReplayInterface::s32FGetSize(long fd)
{
    return -1;
}

int OSALReplayInterface::s32CreateDir(long fd,const char* szDirectory)
{
    return -1;
}

int OSALReplayInterface::s32RemoveDir(long fd,const char* szDirectory)
{
    return -1;
}

intptr_t OSALReplayInterface::prOpenDir(const char* szDirectory)
{
    return -1;
}

intptr_t OSALReplayInterface::prReadDir(long pDir)
{
    return -1;
}

int OSALReplayInterface::s32CloseDir(long pDir)
{
    return -1;
}
