#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "OSALInterface.h"
#include "OSALDirectInterface.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

OSALDirectInterface::OSALDirectInterface()
{
}


OSALDirectInterface::~OSALDirectInterface()
{
}

int OSALDirectInterface::s32MsgQueueCreate( const char* szQueueName,
                                           unsigned long u32MaxMsgs, 
                                           unsigned long u32MaxLength, 
                                           unsigned long u32Access, 
                                           unsigned long& u32Handle )
{
    return OSAL_s32MessageQueueCreate( szQueueName,
        u32MaxMsgs,
        u32MaxLength,
        (OSAL_tenAccess) u32Access,
        (OSAL_tMQueueHandle *)&u32Handle );
}

int OSALDirectInterface::s32MsgQueueDelete(const char* szQueueName)
{
    return OSAL_s32MessageQueueDelete(szQueueName);
}

int OSALDirectInterface::s32MsgQueueOpen(const char* szQueueName,
                                         unsigned long u32Access,
                                         unsigned long& u32Handle)
{
    return OSAL_s32MessageQueueOpen( szQueueName, 
        (OSAL_tenAccess) u32Access, 
        (OSAL_tMQueueHandle *)&u32Handle );
}

int OSALDirectInterface::s32MsgQueueClose(long s32Handle)
{
    return OSAL_s32MessageQueueClose(s32Handle);
}

int OSALDirectInterface::s32MsgPoolOpen()
{
    return OSAL_s32MessagePoolOpen();
}

int OSALDirectInterface::s32MsgPoolClose()
{
    return OSAL_s32MessagePoolClose();
}

int OSALDirectInterface::s32MsgQueuePost( unsigned long u32Handle,
                                         unsigned long u32MsgSize, 
                                         unsigned char *pu8Msg, 
                                         unsigned long u32Prio)
{
#if 1
    tS32 s32RetVal = 0;
    OSAL_trMessage message;
    // create message in message pool
    s32RetVal = OSAL_s32MessageCreate(&message, u32MsgSize , OSAL_EN_MEMORY_SHARED);
    if (s32RetVal == OSAL_OK)
    {
        // Adresse bestimmen
        tPU8 pointer = OSAL_pu8MessageContentGet(message, OSAL_EN_READWRITE);
        // Nachricht kopieren
        memcpy(pointer, pu8Msg, u32MsgSize);
        // OSAL_s32MessageQueuePost aufrufen
        s32RetVal = OSAL_s32MessageQueuePost(u32Handle, (tPCU8) &message, sizeof(OSAL_trMessage), u32Prio);
    }
    return s32RetVal;
#else
    return OSAL_s32MessageQueuePost(u32Handle, pu8Msg, u32MsgSize, u32Prio);
#endif
}

int OSALDirectInterface::s32MsgQueueWait( unsigned long u32Handle, 
                                         unsigned long u32BufSize, 
                                         unsigned long u32TimeOut, 
                                         unsigned char *pu8MsgBuf,
                                         unsigned long &u32Prio )
{
#if 0
    return OSAL_s32MessageQueueWait( u32Handle,
        pu8MsgBuf,
        u32BufSize,
        &u32Prio,
        u32TimeOut );
#else
    // Gr��e speichern
    OSAL_trMessage message;
    tS32 s32BytesReceived = 0;
    // OSAL_s32MessageQueueWait aufrufen
    tS32 s32RetVal = OSAL_s32MessageQueueWait( u32Handle,
        (tU8*)&message,
        sizeof(OSAL_trMessage),
        (tPU32)&u32Prio,
        u32TimeOut );
    //  if message was received
    if (s32RetVal > 0)
    {
        // determine size of message data
        unsigned int u32Size = OSAL_u32GetMessageSize(message);
        // is buffer big enough?
        if (u32Size < u32BufSize)
        {
            tPU8 pointer = OSAL_pu8MessageContentGet(message, OSAL_EN_READWRITE);
            // save msg size
            s32BytesReceived = u32Size;            
            // copy message data
            memcpy(pu8MsgBuf, pointer, u32Size);
            // delete message
            OSAL_s32MessageDelete(message);
        }
    }
    return s32BytesReceived;
#endif
}

bool OSALDirectInterface::bInit(bool bEnableLog)
{
    return true;
}

bool OSALDirectInterface::bEnd()
{
    return true;
}



int  OSALDirectInterface::hSharedMemoryCreate   (const char* szName, int access, unsigned int u32Size)
{
    return -1;
}

int  OSALDirectInterface::hSharedMemoryOpen     (const char* szName, int access)
{
    return -1;
}

int  OSALDirectInterface::s32SharedMemoryClose  (int handle)
{
    return -1;
}

int  OSALDirectInterface::s32SharedMemoryDelete (const char *szName)
{
    return -1;
}

void *OSALDirectInterface::pvSharedMemoryMap     ( int handle, 
                                                  int access, 
                                                  unsigned int length, 
                                                  unsigned int offset  )
{
    return 0;
}

int OSALDirectInterface::s32SharedMemoryUnmap    (void * sharedMemory, unsigned int size)
{
    return -1;
}



int OSALDirectInterface::s32IOOpen(unsigned long u32Access, const char* szFileName)
{
    return -1;
}

int OSALDirectInterface::s32IOCreate(unsigned long u32Access, const char* szFileName)
{
    return -1;
}

int OSALDirectInterface::s32IOClose(long s32Descriptor)
{
    return -1;
}

int OSALDirectInterface::s32IOCtrl(long s32Descriptor, long s32Fun, long s32Size, unsigned char *ps8Arg)
{
    return -1;
}

int OSALDirectInterface::s32IOCtrl(long s32Descriptor, long s32Fun, long s32Arg)
{
    return -1;
}

int OSALDirectInterface::s32IORemove(const char* szFileName)
{
    return -1;
}

int OSALDirectInterface::s32IORead(long s32Descriptor, unsigned long u32BufSize, unsigned char* pu8Buf)
{
    return -1;
}

int OSALDirectInterface::s32IOWrite(long s32Descriptor, unsigned long u32Size, unsigned char* pu8Buf)
{
    return -1;
}

int OSALDirectInterface::s32MsgPoolCreate(unsigned long u32PoolSize)
{
    return -1;
}

int OSALDirectInterface::s32MsgPoolDelete()
{
    return -1;
}

int OSALDirectInterface::s32IOEngFct ( const char *device, 
                                      long s32Fun, 
                                      unsigned char *ps8Arg, 
                                      long s32ArgSize)
{
    return -1;
}

int OSALDirectInterface::s32IOFakeExclAccess (const char *device)
{
    return -1;
}

int OSALDirectInterface::s32IOReleaseFakedExclAccess (const char *device)
{
    return -1;
}

int OSALDirectInterface::s32MiniSPMInitiateShutdown()
{
    return -1;
}

int OSALDirectInterface::s32FSeek(intptr_t fd,int offset, int origin)
{
    return OSALUTIL_s32FSeek((OSAL_tIODescriptor)fd, offset, origin);
}

int OSALDirectInterface::s32FTell(intptr_t fd)
{
    return OSALUTIL_s32FTell((OSAL_tIODescriptor)fd);
}

int OSALDirectInterface::s32FGetpos(intptr_t fd,intptr_t *ptr)
{
    return OSALUTIL_s32FGetpos((OSAL_tIODescriptor) fd, ptr);
}

int OSALDirectInterface::s32FSetpos(intptr_t fd,const intptr_t *ptr)
{
    return OSALUTIL_s32FSetpos((OSAL_tIODescriptor) fd, (intptr_t*) ptr);
}

int OSALDirectInterface::s32FGetSize(intptr_t fd)
{
    return OSALUTIL_s32FGetSize((OSAL_tIODescriptor)fd);
}

int OSALDirectInterface::s32CreateDir(intptr_t fd,const char* szDirectory)
{
    return OSALUTIL_s32CreateDir((OSAL_tIODescriptor)fd, (tCString) szDirectory);
}

int OSALDirectInterface::s32RemoveDir(intptr_t fd,const char* szDirectory)
{
    return (int)OSALUTIL_s32RemoveDir((OSAL_tIODescriptor)fd, (tCString) szDirectory);
}

intptr_t OSALDirectInterface::prOpenDir(const char* szDirectory)
{
    return (intptr_t)OSALUTIL_prOpenDir((tCString)szDirectory);
}

intptr_t OSALDirectInterface::prReadDir(intptr_t pDir)
{
    return (intptr_t)OSALUTIL_prReadDir((OSAL_trIOCtrlDir*)pDir);
}

int OSALDirectInterface::s32CloseDir(intptr_t pDir)
{
    return OSALUTIL_s32CloseDir((OSAL_trIOCtrlDir*)pDir);
}
