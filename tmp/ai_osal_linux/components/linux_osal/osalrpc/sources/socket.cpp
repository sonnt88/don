

#ifdef VASCO_OS_WINNT
   #include "winsock.h"
   #include "stdlib.h"
   #include "Winbase.h"
   #include "stdio.h"

   typedef int socklen_t;

#elif defined VASCO_OS_LINUX
   #include <netinet/in.h>
   #include <netdb.h>
   #include <sys/socket.h>
   #include <arpa/inet.h>
   #include <unistd.h>

   #include <stdlib.h>
   #include <stdio.h>
   #include <string.h>
   #include <memory.h>

   #define INVALID_SOCKET -1
   #define FALSE 0
   #define TRUE 1
   #define closesocket close
#endif

#include "socket.h"


#ifdef VASCO_OS_WINNT
// used for WSAStartup and WSACleanup 
int Socket::counter=0;
#endif

bool isClientAvailable (const char *client)
{
   if (gethostbyname (client))
      return true;
   else 
      return false;
}

Socket::Socket ()
{
    msgsock = INVALID_SOCKET;
    port = -1;
    m_u32TimeOutValue = 0;

#ifdef VASCO_OS_WINNT
    WSADATA wsaData;
    if (!counter)	
        WSAStartup(0x0101, &wsaData);
    counter++;
#endif
}

Socket::~Socket ()
{
    if (msgsock != INVALID_SOCKET) {
        closesocket (msgsock);
    }

#ifdef VASCO_OS_WINNT
    counter--;
    if (!counter)	
        WSACleanup();
#endif
}

int Socket::Send (char *pcbuffer, int nlength)
{
    assertion (nlength < MAX_MESSAGE_SIZE);
    socket_message message;
    message.nsize = nlength;
    memcpy (message.pcbuffer, pcbuffer, nlength);

    if (msgsock == INVALID_SOCKET || 
        send (msgsock,(char*) &message, nlength+4, 0) != nlength+4) {
            return ERROR_SOCKET_SEND;
    }
    return SUCCESS;
}

int Socket::Send (char *pcbuffer)
{
    int nlength = strlen (pcbuffer);
    if (msgsock == INVALID_SOCKET || send (msgsock, pcbuffer, nlength, 0) != nlength) {
        return ERROR_SOCKET_SEND;
    }
    return SUCCESS;
}

int Socket::Receive (char *pcbuffer, int nlength)
{
    int nSize;
    int nRecvSize;

    if (msgsock == INVALID_SOCKET || (recv (msgsock, (char*) &nSize, 4, 0)) <= 0) {
        return ERROR_SOCKET_RECV;
    } 

    assertion (nSize <= nlength);

    if ((nRecvSize = recv (msgsock, pcbuffer, nSize, 0)) != nSize) {
        return ERROR_SOCKET_RECV;
    }
    assertion (nRecvSize == nSize);

    return nSize;
}

int Socket::Receive (char *pcbuffer)
{
    int nSize = 0;
    char temp;

    do {
        if (msgsock == INVALID_SOCKET || (recv (msgsock, &temp, 1, 0)) <= 0) {
            return ERROR_SOCKET_RECV;
        }
        pcbuffer[nSize] = temp;
        nSize++;
    }
    while (temp != '\n' && temp != '\0');

    pcbuffer[nSize] = '\0'; 
    return nSize;
}


// implementation of server socket class

SimpleServer::SimpleServer ()
{
    nsock = INVALID_SOCKET;
}

int SimpleServer::Create (int nport)
{
   socklen_t length;

    struct sockaddr_in server;

    nsock = socket(AF_INET, SOCK_STREAM,0);
    if (nsock == INVALID_SOCKET) {
        return ERROR_SOCKET_OPEN;
    }

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = nport;
    if (bind(nsock,(struct sockaddr*)&server, sizeof(server))<0) {
        return ERROR_SOCKET_BIN;
    }

    length = sizeof server;
    if (getsockname(nsock, (struct sockaddr*)&server,&length)<0) {
        return ERROR_SOCKET_NAME;
    }

    listen(nsock, 5);

    port = ntohs(server.sin_port);

    return port;
}

int SimpleServer::Wait ()
{
    if (nsock == INVALID_SOCKET){
        return ERROR_SOCKET_ACCEPT;
    }
    msgsock = accept (nsock, (struct sockaddr *)0, (socklen_t*) 0);
    if (msgsock == INVALID_SOCKET){
        return ERROR_SOCKET_ACCEPT;
    }
    return SUCCESS;
}



// implementation of the client socket class
SimpleClient::SimpleClient ()
{
}

int SimpleClient::Create (int nportNbr, char *pchostname)
{
    struct sockaddr_in server;
    struct hostent *hp;
    char computerName[256];
    port = nportNbr;

    msgsock = socket(AF_INET, SOCK_STREAM,0);
    if (msgsock == INVALID_SOCKET) {
        return ERROR_SOCKET_OPEN;
    }

#ifdef VASCO_OS_WINNT
    if (!pchostname || pchostname[0] == '\0') {
        unsigned long number = 256;
        GetComputerName (computerName, &number);
    }
    else
        strcpy (computerName, pchostname);
#elif defined VASCO_OS_LINUX
    strcpy (computerName, "localhost");
#endif

    hp = gethostbyname (computerName);
    server.sin_family = AF_INET;

    if (hp == NULL) {
        return ERROR_SOCKET_HOST;
    }

    memcpy((char *) &server.sin_addr, (char *) hp -> h_addr, hp -> h_length);
    server.sin_port = htons(port);

    if (connect (msgsock, (struct sockaddr *) &server, sizeof (server)) < 0) {
        return ERROR_SOCKET_CONNECT;
    }
    return SUCCESS;
}

void assertion(bool condition) {
    if (!condition)
        exit(-1);
}

int Socket::RawSend(char *pcbuffer, int length)
{
    if (msgsock == INVALID_SOCKET || 
        send (msgsock,pcbuffer, length, 0) != length) {
            return ERROR_SOCKET_SEND;
    }
    return SUCCESS;
}

int Socket::RawReceive(char *pcbuffer, int length)
{
    int nSize = length;
    int nRecvSize;
    if ((nRecvSize = recv (msgsock, pcbuffer, nSize, 0)) <= 0) {
        return ERROR_SOCKET_RECV;
    }
    return nRecvSize;
}

int Socket::TimedReceive( char* pcbuffer, int length, unsigned long ReceiveTimeOut )
{
#ifdef VASCO_OS_WINNT
    int nRecvdSize = 0;
    int nPartRecvSize = 0;
    unsigned long u32LocalTimeOut = ( ReceiveTimeOut == 0) ? m_u32TimeOutValue : ReceiveTimeOut;

    // Timeout behaviour not required; Should freeze!
    if ( msgsock != -1)
    {
        if ( u32LocalTimeOut == 0 )
        {
            while ( nRecvdSize < length )
            {
                // Drain what is there on the socket buffer
                if ( (nPartRecvSize = recv (msgsock, (pcbuffer + nRecvdSize), (length - nRecvdSize), 0)) <= 0 )
                {
                    return ERROR_SOCKET_RECV;
                }
                else
                {
                    nRecvdSize += nPartRecvSize;
                }
            }
        }
        else  // Timeout behaviour required
        {
            while ( nRecvdSize < length )
            {
                if (m_hTimerStartEvent) SetEvent(m_hTimerStartEvent);
                if (m_hTimerEvent) ResetEvent(m_hTimerEvent);
                if (m_hCallbackEvent) ResetEvent(m_hCallbackEvent);

                if ( (nPartRecvSize = recv (msgsock, (pcbuffer + nRecvdSize), (length - nRecvdSize), 0)) <= 0 )
                {
                    return ERROR_SOCKET_RECV;
                }
                else
                {
                    nRecvdSize += nPartRecvSize;
                }
                if (m_hTimerStartEvent) ResetEvent(m_hTimerStartEvent);
                if (m_hTimerEvent) SetEvent(m_hTimerEvent);
                if (m_hCallbackEvent) SetEvent(m_hCallbackEvent);
            }
        }
    }
    else
        nRecvdSize = -1;
    return nRecvdSize;
#elif defined VASCO_OS_LINUX
   return -1;
#endif
}


int Socket::s32TimerInit(Socket_tpfTimeoutCallback pCallback, void *pvArg,unsigned long timeOut )
{
#ifdef VASCO_OS_WINNT
    m_hTimerStartEvent =CreateEvent(NULL,TRUE,FALSE,NULL);
    m_hTimerEvent=CreateEvent(NULL,FALSE,FALSE,NULL);
    m_hCallbackEvent=CreateEvent(NULL,FALSE,FALSE,NULL);
    m_pfTimeoutCallback = pCallback;
    m_pvArgLst = pvArg;
    m_u32TimeOutValue = timeOut;
    if (m_hTimerStartEvent && m_hTimerEvent && m_hCallbackEvent)
    {
        m_hSockTimerThread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)SocketTimerProc,this,0,&m_dwThreadID);
        if ( m_hSockTimerThread )
            return true;
    }
    return false;
#else
   return false;
#endif
}

void SocketTimerProc(void * pArg)
{
#ifdef VASCO_OS_WINNT
    Socket *pSocket = (Socket *)pArg;

    unsigned long dwWaitEvRet,dwTimerEvRet,dwWaitMultRet;
    void *arHandle[2]={0};
    bool bRetry = true;

    if(pSocket)
    {
        while( bRetry )
        {
            dwWaitEvRet = WaitForSingleObject( pSocket->m_hTimerStartEvent, INFINITE );
            if ( dwWaitEvRet == WAIT_OBJECT_0 )
            {
                dwTimerEvRet = WaitForSingleObject( pSocket->m_hTimerEvent, pSocket->m_u32TimeOutValue );
                switch( dwTimerEvRet )
                {
                case WAIT_OBJECT_0:
                    {
                        ResetEvent( pSocket->m_hTimerStartEvent );
                        ResetEvent( pSocket->m_hTimerEvent );
                    }
                    break;

                case WAIT_TIMEOUT:
                    {
                        ResetEvent( pSocket->m_hCallbackEvent );
                        pSocket->m_hCallbackThread = CreateThread( NULL,                                // SecurityAttributes,
                                                                    0,                                   // StackSize,
                                                                    (LPTHREAD_START_ROUTINE)pSocket->m_pfTimeoutCallback, // StartFunction,
                                                                    pSocket->m_pvArgLst,                 // ThreadParameter,
                                                                    0,                                   // CreationFlags,
                                                                    &pSocket->m_dwCallbackThreadID       // ThreadId
                                                                    );

                        arHandle[0] = pSocket->m_hCallbackEvent;
                        arHandle[1] = pSocket->m_hCallbackThread;
                        dwWaitMultRet = WaitForMultipleObjects( 2, arHandle, FALSE,INFINITE );

                        switch( dwWaitMultRet )
                        {
                        case WAIT_OBJECT_0:
                            {
                                TerminateThread( pSocket->m_hCallbackThread, 0 );
                            }
                            break;

                        case WAIT_OBJECT_0 + 1:
                            {
                                tenTimeoutOptions enCallbackRet;
                                GetExitCodeThread( pSocket->m_hCallbackThread, (unsigned long*)&enCallbackRet );
                                switch( enCallbackRet )
                                {
                                case ABORT:
                                    send(pSocket->msgsock,"Terminate",0,0);// To unblock the blocking receive call once
                                    closesocket(pSocket->msgsock);
                                    pSocket->msgsock = -1;
                                    bRetry = false;
                                    break;

                                case RETRY:
                                    ResetEvent( pSocket->m_hTimerEvent );
                                    break;

                                case CANCEL:
                                    bRetry = false;
                                    break;

                                default:
                                    break;
                                };
                            }
                            break;

                        default:
                            break;
                        };
                    }
                    break;

                default:
                    break;
                };
            }
        }
    }
    CloseHandle(pSocket->m_hTimerStartEvent);
    CloseHandle(pSocket->m_hTimerEvent);
    CloseHandle(pSocket->m_hCallbackEvent);
    CloseHandle(pSocket->m_hCallbackThread);
    pSocket->m_hTimerStartEvent = pSocket->m_hTimerEvent = pSocket->m_hCallbackEvent = pSocket->m_hCallbackThread = NULL;
    pSocket->vClearSocketTimeout();
#endif
}

void Socket::vClearSocketTimeout()
{
    m_pfTimeoutCallback = NULL;
    m_pvArgLst = NULL;
    m_u32TimeOutValue = 0;
}


