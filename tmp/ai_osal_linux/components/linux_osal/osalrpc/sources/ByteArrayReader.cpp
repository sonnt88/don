#include "string.h"
#include "ByteArrayReader.h"
#include "string"


ByteArrayReader::ByteArrayReader (unsigned char *pucByteArray, unsigned int u32ArraySize)
: m_pucByteArray (pucByteArray)
, m_u32ArraySize (u32ArraySize)
, m_u32Pos (0)
{
}

unsigned int ByteArrayReader::u32GetBytesLeft()
{
   return m_u32ArraySize - m_u32Pos;
}

bool ByteArrayReader::bReadU32 (unsigned int *u32Value)
{
   return bReadRaw ((unsigned char *) u32Value, 4);
}

bool ByteArrayReader::bReadS32 (int *s32Value)
{
   return bReadRaw ((unsigned char *) s32Value, 4);
}

unsigned int ByteArrayReader::bPeekUpcomingStringLength ()
{
   int i=0;
   while (i+m_u32Pos < m_u32ArraySize)
   {
      if (m_pucByteArray[i+m_u32Pos] == '\0')
      {
         // Terminierung gefunden!
         break;
      }
      ++i;
   }

   if ( !(i+m_u32Pos < m_u32ArraySize) )
   {
      // Keine Null-Terminierung gefunden...
      i = 0;
   }

   return i;
}

bool ByteArrayReader::bReadString (char *data)
{
   bool bSuccess = false;
   int size = bPeekUpcomingStringLength();
   if (size != 0)
   {
      strcpy (data, (const char *) (m_pucByteArray+m_u32Pos));
      m_u32Pos += size;
      bSuccess = true;
   }

   return bSuccess;
}



bool ByteArrayReader::bReadRaw (unsigned char *data, unsigned int size)
{
   /*
    *  Groessencheck
    */
   bool bSuccess = false;
   
   if (m_u32Pos + size <= m_u32ArraySize)
   {
      memcpy (data, m_pucByteArray + m_u32Pos, size);
      m_u32Pos += size;
      bSuccess = true;
   }

   return bSuccess;
}
