#ifndef OSALRPC_IF_HEADER
#define OSALRPC_IF_HEADER

#ifdef OSALRPC_INTERFACE_CLIENT
   #ifndef OSALRPC_ALREADY_INTERFACE_CLIENT
   #define OSALRPC_ALREADY_INTERFACE_CLIENT
      #define OSALRPC_INTERFACE_COMMANDCODES
      #include "osalrpcserver_if.h"
      #include "include/OSALInterface.h"
      #include "include/OSALDirectInterface.h"
      #include "linux/include/OSALSocketInterface.h"
      #ifdef OSALRPC_USE_REPLAY_INTERFACE
         #include "linux/include/OSALReplayInterface.h"
      #endif //OSALRPC_USE_REPLAY_INTERFACE
   #endif //OSALRPC_ALREADY_INTERFACE_CLIENT
#endif //OSALRPC_INTERFACE_CLIENT

#endif //OSALRPC_IF_HEADER
