// OSALInterface.h: interface for the OSALInterface class.

#ifndef __OSAL_INTERFACE_H__
#define __OSAL_INTERFACE_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define OSALRPC_E_OSALAPI_TIMEOUT   (unsigned int)466964

class OSALInterface  
{
public:
    OSALInterface();
    virtual ~OSALInterface();
    virtual bool bInit(bool bEnableLog) = 0;
    virtual bool bEnd() = 0;

    virtual int s32MsgQueueWait(unsigned long u32Handle, unsigned long u32BufSize, unsigned long u32TimeOut, unsigned char*pu8MsgBuf,unsigned long &u32Prio) = 0;
    virtual int s32MsgQueuePost(unsigned long u32Handle, unsigned long u32MsgSize, unsigned char* ps8Msg, unsigned long u32Prio) = 0;
    virtual int s32MsgQueueClose(long s32Handle) = 0;
    virtual int s32MsgQueueOpen(const char* szQueueName,unsigned long u32Access,unsigned long& u32Handle) = 0;
    virtual int s32MsgQueueCreate(const char* szQueueName,unsigned long s32MaxMsgs, unsigned long s32MaxLength, unsigned long u32Access, unsigned long& u32Handle) = 0;
    virtual int s32MsgQueueDelete(const char* szQueueName) = 0;

    virtual int s32MsgPoolCreate(unsigned long u32PoolSize) = 0;
    virtual int s32MsgPoolDelete() = 0;
    virtual int s32MsgPoolClose() = 0;
    virtual int s32MsgPoolOpen() = 0;

    virtual int  hSharedMemoryCreate   (const char* szName, int access, unsigned int u32Size) = 0;
    virtual int  hSharedMemoryOpen     (const char* szName, int access) = 0;
    virtual int  s32SharedMemoryClose  (int handle) = 0;
    virtual int  s32SharedMemoryDelete (const char *szName) = 0;

    virtual void *pvSharedMemoryMap     ( int handle, 
        int access, 
        unsigned int length, 
        unsigned int offset  ) = 0;

    virtual int s32SharedMemoryUnmap    (void * sharedMemory, unsigned int size) = 0;

    virtual int s32IOOpen(unsigned long u32Access, const char* szFileName) = 0;
    virtual int s32IOCreate(unsigned long u32Access, const char* szFileName) = 0;
    virtual int s32IOClose(long s32Descriptor) = 0;
    virtual int s32IOCtrl(long s32Descriptor, long s32Fun, long s32Size, unsigned char *ps8Arg) = 0;
    virtual int s32IOCtrl(long s32Descriptor, long s32Fun, long s32Arg) = 0;
    virtual int s32IORemove(const char* szFileName) = 0;
    virtual int s32IORead(long s32Descriptor, unsigned long u32BufSize, unsigned char* pu8Buf) = 0;
    virtual int s32IOWrite(long s32Descriptor, unsigned long u32Size, unsigned char* pu8Buf) = 0;

    /* OSAL-IO-Funktionen - Debug-Funktionen nur fuer die Bench und osal_pure / NT! */
    virtual int s32IOEngFct (const char *device, long s32Fun, unsigned char *ps8Arg, long s32ArgSize) = 0;
    virtual int s32IOFakeExclAccess         (const char *device) = 0;
    virtual int s32IOReleaseFakedExclAccess (const char *device) = 0;

    /* This function only works with osal_pure/NT and application started with MiniSPM! */
    virtual int s32MiniSPMInitiateShutdown() = 0;

    /* OSAL Util-IO Funktionen */
    virtual int s32FSeek(long fd,int offset, int origin) = 0;
    virtual int s32FTell(long fd) = 0;
    virtual int s32FGetpos(long fd,int *ptr) = 0;
    virtual int s32FSetpos(long fd,const int *ptr) = 0;
    virtual int s32FGetSize(long fd) = 0;
    virtual int s32CreateDir(long fd,const char* szDirectory) = 0;
    virtual int s32RemoveDir(long fd,const char* szDirectory) = 0;
    virtual intptr_t prOpenDir(const char* szDirectory) = 0;
    virtual intptr_t prReadDir(long pDir) = 0;
    virtual int s32CloseDir(long pDir) = 0;

protected:
    unsigned int u32LastErrorCode;
};

#endif // __OSAL_INTERFACE_H__
