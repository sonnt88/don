


#ifndef BYTE_ARRAY_READER_H
#define BYTE_ARRAY_READER_H


class ByteArrayReader
{
   private:
      unsigned char *m_pucByteArray;
      unsigned int  m_u32ArraySize;

      /** 
       *  aktuelle Schreibposition relativ zum Anfang des Arrays 
       *  Wertebereich: 0...u32ArraySize
       */
      unsigned int m_u32Pos;
   public:
      ByteArrayReader (unsigned char *pucByteArray, unsigned int u32ArraySize);
      unsigned int u32GetBytesLeft();

      bool bReadU32 (unsigned int *u32Value);
      bool bReadS32 (int *s32Value);
      
      /** 
       *  Angenommen, jetzt kommt ein NULL-Terminierter String
       *  im Datenstrom - mit dieser Funktion kann man
       *  seinen Laenge bestimmen!
       *
       *  @return Laenge des Strings incl. Null-Byte am Ende
       *          oder '0', falls kein NULL-terminierter
       *          String gefunden wurde!
       */
      unsigned int bPeekUpcomingStringLength ();

      /** 
       *  liest einen String ein. Die Laenge sollte vorher mit
       *  der Funktion bPeekUpcomingStringLength() abgefragt werden,
       *  damit in "data" genuegend Platz zur Verfuegung steht!
       */
      bool bReadString (char *data);

      bool bReadRaw (unsigned char *data, unsigned int size);
};



#endif      // BYTE_ARRAY_READER_H
