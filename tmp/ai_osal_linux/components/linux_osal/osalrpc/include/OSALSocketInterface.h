
// OSALSocketInterface.h: interface for the OSALSocketInterface class.

#ifndef __OSAL_SOCKET_INTERFACE_H__
#define __OSAL_SOCKET_INTERFACE_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <pthread.h>
#include <map>
#include <string>
#include "socket.h"

class OSALSocketInterface : public OSALInterface  
{
public:
    OSALSocketInterface();
    OSALSocketInterface(int s32SocketNo,const char* coszHostName);
    virtual ~OSALSocketInterface();
    virtual bool bInit(bool bEnableLog = false);
    virtual bool bEnd();

    virtual int s32MsgQueueWait(unsigned long u32Handle, unsigned long u32BufSize, unsigned long u32TimeOut, unsigned char*pu8MsgBuf,unsigned long &u32Prio);
    virtual int s32MsgQueuePost(unsigned long u32Handle, unsigned long u32MsgSize, unsigned char* ps8Msg, unsigned long u32Prio);
    virtual int s32MsgQueueClose(long s32Handle);
    virtual int s32MsgQueueOpen(const char* szQueueName,unsigned long u32Access,unsigned long& u32Handle);
    virtual int s32MsgQueueCreate(const char* szQueueName,unsigned long s32MaxMsgs,unsigned long s32MaxLength, unsigned long u32Access, unsigned long& u32Handle);
    virtual int s32MsgQueueDelete(const char* szQueueName);

    virtual int s32MsgPoolClose();
    virtual int s32MsgPoolOpen();
    virtual int s32MsgPoolCreate(unsigned long u32PoolSize);
    virtual int s32MsgPoolDelete();

    virtual int hSharedMemoryCreate   (const char* szName, int access, unsigned int u32Size);
    virtual int hSharedMemoryOpen     (const char* szName, int access);
    virtual int s32SharedMemoryClose  (int handle);
    virtual int s32SharedMemoryDelete (const char *szName);

    virtual void *pvSharedMemoryMap     ( int handle, 
        int access, 
        unsigned int length, 
        unsigned int offset  );

    virtual int s32SharedMemoryUnmap    (void * sharedMemory, unsigned int size);

    virtual int s32IOOpen(unsigned long u32Access, const char* szFileName);
    virtual int s32IOCreate(unsigned long u32Access, const char* szFileName);
    virtual int s32IOClose(long s32Descriptor);
    virtual int s32IOCtrl(long s32Descriptor, long s32Fun, long s32Size, unsigned char *ps8Arg);
    virtual int s32IOCtrl(long s32Descriptor, long s32Fun, long s32Arg);
    virtual int s32IORemove(const char* szFileName);
    virtual int s32IORead(long s32Descriptor, unsigned long u32BufSize, unsigned char* pu8Buf);
    virtual int s32IOWrite(long s32Descriptor, unsigned long u32Size, unsigned char* pu8Buf);

    /* OSAL-IO-Funktionen - Debug-Funktionen nur fuer die Bench und osal_pure / NT! */
    virtual int s32IOEngFct (const char *device, long s32Fun, unsigned char *ps8Arg, long s32ArgSize);
    virtual int s32IOFakeExclAccess         (const char *device);
    virtual int s32IOReleaseFakedExclAccess (const char *device);

    /* This function only works with osal_pure/NT and application started with MiniSPM! */
    virtual int s32MiniSPMInitiateShutdown();

    /* This function is used to register for timeout callback */
    /* u32TmOutValue value should be specified in milliseconds */
    virtual int s32SetTimeoutEventHandler( Socket_tpfTimeoutCallback pCallback, void *pvArg, unsigned long u32TmOutValue );

    /* OSAL Util-IO Funktionen */
    virtual int s32FSeek(long fd,int offset, int origin);
    virtual int s32FTell(long fd);
    virtual int s32FGetpos(long fd,int *ptr);
    virtual int s32FSetpos(long fd,const int *ptr);
    virtual int s32FGetSize(long fd);
    virtual int s32CreateDir(long fd,const char* szDirectory);
    virtual int s32RemoveDir(long fd,const char* szDirectory);
    virtual intptr_t prOpenDir(const char* szDirectory);
    virtual intptr_t prReadDir(long pDir);
    virtual int s32CloseDir(long pDir);

private:

    const int m_cu32Times;  // Divide by
    const int m_cu32Factor; // Receive timeout factor
    char* szHostName;
    FILE* log;
    int s32SocketNumber;
    SimpleClient* clientSocket;

    pthread_mutex_t mPostMutex;
    pthread_mutex_t mRecieveMutex;

    /** time out vars **/
    bool m_bTimeOutFlag;

    std::map<unsigned int, std::string> *m_poQueueNameMap;
    std::map<void *, int> *m_poSharedMemoryMap;
    int s32MsgQueueTimedWait(unsigned long u32Handle, unsigned long u32BufSize, 
                             unsigned long u32TimeOut, unsigned char *pu8MsgBuf,
                             unsigned long &u32Prio, unsigned long u32RecvTimeout = 0);

};

#endif // __OSAL_SOCKET_INTERFACE_H__
