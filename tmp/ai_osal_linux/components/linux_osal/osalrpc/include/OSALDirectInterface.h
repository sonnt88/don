

#ifndef __OSAL_DIRECT_INTERFACE_H__
#define __OSAL_DIRECT_INTERFACE_H__

// #define MAX_MESSAGE_SIZE 1024*10
#define SUCCESS 0


/**
*  This class is to be used as a direct interface to an OSAL-Application.
*  
*  It can be used to link OSAL_IF-Based applications together with
*  an OSAL_PURE-based Application to get the communication not
*  via socket or an other external line but directly by
*  calling a OSAL-Function.
* 
*  Espacially this will be used to get the WinCE-HMI running on
*  PDA connected to navigation running on PDA.
*
*  @author CM-DI/ESP4 - Tobias Steinmann
*  @date   2005-11-01
*/
class OSALDirectInterface : public OSALInterface  
{
public:
    OSALDirectInterface();
    virtual ~OSALDirectInterface();
    virtual bool bInit(bool bEnableLog);
    virtual bool bEnd();

    virtual int s32MsgQueueWait(unsigned long u32Handle, unsigned long u32BufSize, unsigned long u32TimeOut, unsigned char*pu8MsgBuf,unsigned long &u32Prio);
    virtual int s32MsgQueuePost(unsigned long u32Handle, unsigned long u32MsgSize, unsigned char* ps8Msg, unsigned long u32Prio);
    virtual int s32MsgQueueClose(long s32Handle);
    virtual int s32MsgQueueOpen(const char* szQueueName,unsigned long u32Access,unsigned long& u32Handle);
    virtual int s32MsgQueueCreate(const char* szQueueName,unsigned long s32MaxMsgs,unsigned long s32MaxLength, unsigned long u32Access, unsigned long& u32Handle);
    virtual int s32MsgQueueDelete(const char* szQueueName);

    virtual int s32MsgPoolCreate(unsigned long u32PoolSize);
    virtual int s32MsgPoolDelete();
    virtual int s32MsgPoolClose();
    virtual int s32MsgPoolOpen();

    virtual int  hSharedMemoryCreate   (const char* szName, int access, unsigned int u32Size);
    virtual int  hSharedMemoryOpen     (const char* szName, int access);
    virtual int  s32SharedMemoryClose  (int handle);
    virtual int  s32SharedMemoryDelete (const char *szName);

    virtual void *pvSharedMemoryMap     ( int handle, 
        int access, 
        unsigned int length, 
        unsigned int offset  );

    virtual int s32SharedMemoryUnmap    (void * sharedMemory, unsigned int size);


    virtual int s32IOOpen(unsigned long u32Access, const char* szFileName);
    virtual int s32IOCreate(unsigned long u32Access, const char* szFileName);
    virtual int s32IOClose(long s32Descriptor);
    virtual int s32IOCtrl(long s32Descriptor, long s32Fun, long s32Size, unsigned char *ps8Arg);
    virtual int s32IOCtrl(long s32Descriptor, long s32Fun, long s32Arg);
    virtual int s32IORemove(const char* szFileName);
    virtual int s32IORead(long s32Descriptor, unsigned long u32BufSize, unsigned char* pu8Buf);
    virtual int s32IOWrite(long s32Descriptor, unsigned long u32Size, unsigned char* pu8Buf);

    /* OSAL-IO-Funktionen */
    virtual int s32IOEngFct (const char *device, long s32Fun, unsigned char *ps8Arg, long s32ArgSize);
    virtual int s32IOFakeExclAccess         (const char *device);
    virtual int s32IOReleaseFakedExclAccess (const char *device);

    virtual int s32MiniSPMInitiateShutdown();

    /* OSAL Util-IO Funktionen */
    virtual int s32FSeek(intptr_t fd,int offset, int origin);
    virtual int s32FTell(intptr_t fd);
    virtual int s32FGetpos(intptr_t fd,intptr_t *ptr);
    virtual int s32FSetpos(intptr_t fd,const intptr_t *ptr);
    virtual int s32FGetSize(intptr_t fd);
    virtual int s32CreateDir(intptr_t fd,const char* szDirectory);
    virtual int s32RemoveDir(intptr_t fd,const char* szDirectory);
    virtual intptr_t prOpenDir(const char* szDirectory);
    virtual intptr_t prReadDir(intptr_t pDir);
    virtual int s32CloseDir(intptr_t pDir);

};

#endif // __OSAL_DIRECT_INTERFACE_H__
