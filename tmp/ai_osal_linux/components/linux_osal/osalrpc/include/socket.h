#ifndef __SOCKET_H
#define __SOCKET_H

#define MAX_MESSAGE_SIZE 512*1024

#define SUCCESS 0
#define ERROR_SOCKET_SEND -1
#define ERROR_SOCKET_RECV -2
#define ERROR_SOCKET_OPEN -3
#define ERROR_SOCKET_BIN -4
#define ERROR_SOCKET_NAME -5
#define ERROR_SOCKET_ACCEPT -6
#define ERROR_SOCKET_HOST -7
#define ERROR_SOCKET_CONNECT -8
#define ERROR_SOCKET_RECV_TIMEOUT -9


bool isClientAvailable (const char *client);

typedef enum {
    NONE   = 0,
    ABORT  = 1,
    RETRY  = 2,
    CANCEL = 3
} tenTimeoutOptions;

typedef tenTimeoutOptions (*Socket_tpfTimeoutCallback) ( void* );

// struct of a socket message, combines message length and message buffer
struct socket_message {
    // length of message
    int nsize;
    // buffer for message content
    char pcbuffer[MAX_MESSAGE_SIZE];
};

/*
Abstract base class for socket connections, uses blocking sockets.
*/
class Socket
{
public:
    int RawReceive(char* pcbuffer, int length);
    int RawSend(char *pcbuffer,int length);
    int TimedReceive(char* pcbuffer, int length, unsigned long ReceiveTimeOut = 0);
    int s32TimerInit(Socket_tpfTimeoutCallback pCallback, void *pvArg,unsigned long timeOut );
    void vClearSocketTimeout();
    Socket ();
    virtual ~Socket ();

    virtual int Send (char *pcbuffer, int nlength);
    virtual int Send (char *pcbuffer);

    virtual int Receive (char *pcbuffer, int nlength);
    virtual int Receive (char *pcbuffer);
    friend void SocketTimerProc(void *pArg);
protected:

    int msgsock;
    int port;
    unsigned long m_u32TimeOutValue; //in milliseconds
    // reference counter of existing socket connections
    static int counter;
    void *m_hSockTimerThread;
    unsigned long m_dwThreadID;
    void *m_hTimerStartEvent;
    void *m_hTimerEvent;
    void *m_hCallbackEvent;
    void *m_hCallbackThread;
    unsigned long m_dwCallbackThreadID;
    Socket_tpfTimeoutCallback m_pfTimeoutCallback;
    void *m_pvArgLst;

}; 

class SimpleServer: public Socket
{
public:
    SimpleServer ();

    int Create (int nport = 0);
    virtual int Wait ();

protected:

    int nsock;

};

class SimpleClient: public Socket
{
public:

    SimpleClient ();

    int Create (int nport, char *pchostname = NULL);

};

void assertion(bool condition);
#endif
