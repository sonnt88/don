


#ifndef BYTE_ARRAY_WRITER_H
#define BYTE_ARRAY_WRITER_H


class ByteArrayWriter
{
   private:
      unsigned char *m_pucByteArray;
      unsigned int  m_u32ArraySize;

      /** 
       *  aktuelle Schreibposition relativ zum Anfang des Arrays 
       *  Wertebereich: 0...u32ArraySize
       */
      unsigned int m_u32Pos;
   public:
      ByteArrayWriter (unsigned char *pucByteArray, unsigned int u32ArraySize);
      unsigned int u32GetActualDataSize();

      bool bWriteU32 (unsigned int u32Value);
      bool bWriteS32 (int s32Value);
      
      /** Writes NULL-Terminated String */
      bool bWriteString (const char *pcString);

      bool bWriteRaw (unsigned char *data, unsigned int size);

};



#endif      // BYTE_ARRAY_WRITER_H
