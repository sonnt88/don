// OSALSocketInterface.cpp: implementation of the OSALSocketInterface class.
//
//////////////////////////////////////////////////////////////////////
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#pragma warning( disable : 4786 )

#ifdef VASCO_OS_WINNT
   #include "string.h"
   #include "stdio.h"
   #include "memory.h"
   #include "assert.h"
   #include "socket.h"
   #define NOMINMAX 
   #include "windows.h"
#elif defined VASCO_OS_LINUX
   #include <netinet/in.h>
   #include <netdb.h>
   #include <sys/socket.h>
   #include <arpa/inet.h>

   #include <stdlib.h>
   #include <stdio.h>
   #include <string.h>
   #include <memory.h>
   #include <cstdio>

   #define INVALID_SOCKET -1
   #define FALSE 0
   #define TRUE 1
   #define MAX_PATH 256
#define sprintf_s snprintf

#include <time.h>
#include <sys/time.h>

static void Sleep (unsigned int msec)
{
  struct timespec r;

  unsigned int sec = (msec) / 1000;
  unsigned int nsec = (msec - sec*1000) * 1000 * 1000;

  r.tv_sec  = sec;
  r.tv_nsec = nsec;

  nanosleep (&r, NULL);
}

#endif


#include "OSALInterface.h"
#include "OSALSocketInterface.h"


#include "ByteArrayWriter.h"
#include "ByteArrayReader.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

OSALSocketInterface::OSALSocketInterface() : m_cu32Times(4), m_cu32Factor(2)
{
    s32SocketNumber = -1;
    clientSocket = 0;
    m_bTimeOutFlag = FALSE;
    szHostName = NULL;
    log = 0 ;

    m_poQueueNameMap = new std::map<unsigned int, std::string>();
    m_poSharedMemoryMap = new std::map<void *, int>();
#ifdef VASCO_OS_WINNT
	m_pPostCS = (void*)new CRITICAL_SECTION;
    m_pReceiveCS = (void*)new CRITICAL_SECTION;
    InitializeCriticalSection((CRITICAL_SECTION*)m_pPostCS);
    InitializeCriticalSection((CRITICAL_SECTION*)m_pReceiveCS);
#endif
#ifdef VASCO_OS_LINUX
    pthread_mutex_init(&mPostMutex,NULL);
    pthread_mutex_init(&mRecieveMutex,NULL);
#endif
}

OSALSocketInterface::OSALSocketInterface(int s32SocketNo,const char* coszHostName) : m_cu32Times(4), m_cu32Factor(2)
{
    s32SocketNumber = s32SocketNo;
    szHostName = new char[strlen(coszHostName)+1];
    if (szHostName != NULL)
    {
        strcpy(szHostName,coszHostName);
    }
    clientSocket = 0;
    m_bTimeOutFlag = FALSE;
    log = 0 ;

    m_poQueueNameMap = new std::map<unsigned int, std::string>();
    m_poSharedMemoryMap = new std::map<void *, int>();
#ifdef VASCO_OS_WINNT
    m_pPostCS = (void*)new CRITICAL_SECTION;
    m_pReceiveCS = (void*)new CRITICAL_SECTION;
    InitializeCriticalSection((CRITICAL_SECTION*)m_pPostCS);
    InitializeCriticalSection((CRITICAL_SECTION*)m_pReceiveCS);
#endif
#ifdef VASCO_OS_LINUX
    pthread_mutex_init(&mPostMutex,NULL);
    pthread_mutex_init(&mRecieveMutex,NULL);
#endif
}

OSALSocketInterface::~OSALSocketInterface()
{
    if (szHostName != NULL)
    {
        delete[] szHostName;
        szHostName = NULL;
    }
    log = 0 ;
    m_poQueueNameMap->clear();
    m_poSharedMemoryMap->clear();

    delete m_poQueueNameMap;
    m_poQueueNameMap = NULL;
    delete m_poSharedMemoryMap;
    m_poSharedMemoryMap = NULL;
}


int OSALSocketInterface::s32SetTimeoutEventHandler( Socket_tpfTimeoutCallback pCallback, void *pvArg, unsigned long u32TmOutValue )
{
    int s32RetVal = 0;
    if( pCallback == NULL ) // NULL means unregister the callback
    {
        m_bTimeOutFlag = FALSE;
        clientSocket->vClearSocketTimeout();
    }
    else
    {
        if ( u32TmOutValue != 0 && clientSocket->s32TimerInit(pCallback,pvArg,u32TmOutValue))
        {
            m_bTimeOutFlag = TRUE;
        }
        else 
            s32RetVal = -1; // Callback Register won't happen if Timeout value is zero
    }
    return s32RetVal;
}

int OSALSocketInterface::s32MsgQueueCreate(const char* szQueueName,unsigned long u32MaxMsgs, 
                                           unsigned long u32MaxLength, unsigned long u32Access, 
                                           unsigned long& u32Handle)
{
    int s32MsgQueueCreateCmd = 39;
    int s32ReturnValue;
    char* sendBuf = new char[17+strlen(szQueueName)]; // 1 int, 3 uint + 1 string
    if ((sendBuf != 0) && (clientSocket != 0)) {
        memcpy(sendBuf,&s32MsgQueueCreateCmd,4);
        memcpy(sendBuf+4,szQueueName,strlen(szQueueName));
        sendBuf[4+strlen(szQueueName)] = 0;
        memcpy(sendBuf+5+strlen(szQueueName),&u32MaxMsgs,4);
        memcpy(sendBuf+9+strlen(szQueueName),&u32MaxLength,4);
        memcpy(sendBuf+13+strlen(szQueueName),&u32Access,4);
        clientSocket->RawSend(sendBuf,17+strlen(szQueueName));
        char recvBuf[12];

        int s32MsgLength = clientSocket->TimedReceive(recvBuf, 12 );
        if (s32MsgLength == 12) {
            memcpy(&u32LastErrorCode,recvBuf,4);
            memcpy(&s32ReturnValue,recvBuf+4,4);
            memcpy(&u32Handle,recvBuf+8,4);
        } else {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
        delete[] sendBuf;
    } else {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32MsgQueueDelete(const char* szQueueName)
{
    int s32MsgQueueDeleteCmd = 42;
    int s32ReturnValue;
    char* sendBuf = new char[5+strlen(szQueueName)];
    if ((sendBuf != 0) && (clientSocket != 0)) {
        memcpy(sendBuf,&s32MsgQueueDeleteCmd,4);
        memcpy(sendBuf+4,szQueueName,strlen(szQueueName));
        sendBuf[4+strlen(szQueueName)] = 0;
        clientSocket->RawSend(sendBuf,5+strlen(szQueueName));
        char recvBuf[8];
        int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
        if (s32MsgLength == 8) {
            memcpy(&u32LastErrorCode,recvBuf,4);
            memcpy(&s32ReturnValue,recvBuf+4,4);
        } else {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
        delete[] sendBuf;
    } else {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32MsgQueueOpen(const char* szQueueName,
                                         unsigned long u32Access,
                                         unsigned long& u32Handle)
{
    int s32MsgQueueOpenCmd = 40;
    int s32ReturnValue;
    char* sendBuf = new char[9+strlen(szQueueName)];
    if ((sendBuf != 0) && (clientSocket != 0)) {
        memcpy(sendBuf,&s32MsgQueueOpenCmd,4);
        memcpy(sendBuf+4,szQueueName,strlen(szQueueName));
        sendBuf[4+strlen(szQueueName)] = 0;
        memcpy(sendBuf+5+strlen(szQueueName),&u32Access,4);
        clientSocket->RawSend(sendBuf,9+strlen(szQueueName));
        char recvBuf[12];
        int s32MsgLength = clientSocket->TimedReceive(recvBuf,12);
        if (s32MsgLength == 12) {
            memcpy(&u32LastErrorCode,recvBuf,4);
            memcpy(&s32ReturnValue,recvBuf+4,4);
            // check this change (J.Bruns 23.11.06)
            if (s32ReturnValue == 0)
            {
                memcpy(&u32Handle,recvBuf+8,4);
                (*m_poQueueNameMap)[u32Handle] = std::string(szQueueName);
            }
        } else {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
        delete[] sendBuf;
    } else {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32MsgQueueClose(long s32Handle)
{
    int s32MsgQueueCloseCmd = 41;
    int s32ReturnValue;
    char sendBuf[8];
    if ((sendBuf != 0) && (clientSocket != 0)) {
        memcpy(sendBuf,&s32MsgQueueCloseCmd,4);
        memcpy(sendBuf+4,&s32Handle,4);
        clientSocket->RawSend(sendBuf,8);
        char recvBuf[8];
        int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
        if (s32MsgLength == 8) {
            memcpy(&u32LastErrorCode,recvBuf,4);
            memcpy(&s32ReturnValue,recvBuf+4,4);
        } else {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }

    } else {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32MsgPoolOpen()
{
    int s32MsgPoolOpenCmd = 1001;
    int s32ReturnValue;
    if (clientSocket != 0) {
        clientSocket->RawSend((char*)&s32MsgPoolOpenCmd,4);
        char recvBuf[8];
        int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
        if (s32MsgLength == 8) {
            memcpy(&u32LastErrorCode,recvBuf,4);
            memcpy(&s32ReturnValue,recvBuf+4,4);
        } else {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    }
    else {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32MsgPoolClose()
{
    int s32MsgPoolCloseCmd = 1002;
    int s32ReturnValue;
    if (clientSocket != 0) {
        clientSocket->RawSend((char*)&s32MsgPoolCloseCmd,4);
        char recvBuf[8];
        int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
        if (s32MsgLength == 8) {
            memcpy(&u32LastErrorCode,recvBuf,4);
            memcpy(&s32ReturnValue,recvBuf+4,4);
        } else {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    }
    else {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32MsgQueuePost(unsigned long u32Handle,
                                         unsigned long u32MsgSize, 
                                         unsigned char *pu8Msg, 
                                         unsigned long u32Prio)
{
   int s32MsgQueuePostCmd = 1010;
   int s32ReturnValue;
   unsigned char* sendBuf = new unsigned char[16+u32MsgSize];
   if ((sendBuf != 0) && (clientSocket != 0)) {
      memcpy(sendBuf,&s32MsgQueuePostCmd,4);
      memcpy(sendBuf+4,&u32Handle,4);
      memcpy(sendBuf+8,&u32MsgSize,4);
      memcpy(sendBuf+12,pu8Msg,u32MsgSize);
      memcpy(sendBuf+12+u32MsgSize,&u32Prio,4);
      // print for debugging
      if (log)
      {
         fprintf(log,"<MSG_SEND channel=\"%s\"> <!-- outgoing message length = %d -->\n",(*m_poQueueNameMap)[u32Handle].c_str(),u32MsgSize);
         for (unsigned int i = 0; i < u32MsgSize; i++) {
            fprintf(log,"%02X%c",pu8Msg[i] & 0xFF,(((i%16)==15)?'\n':' '));
         }
         fprintf(log,"\n</MSG_SEND>\n");
         fflush(log);
      }
#ifdef VASCO_OS_WINNT
      EnterCriticalSection((CRITICAL_SECTION*)m_pPostCS);
#endif
#ifdef VASCO_OS_LINUX
      pthread_mutex_lock(&mPostMutex);
#endif
      clientSocket->RawSend(reinterpret_cast<char*>(sendBuf),16+u32MsgSize);
      Sleep(10);
      unsigned char recvBuf[8];
      int s32MsgLength = clientSocket->TimedReceive(reinterpret_cast<char*>(recvBuf),8);
      if (s32MsgLength == 8) {
         memcpy(&u32LastErrorCode,recvBuf,4);
         memcpy(&s32ReturnValue,recvBuf+4,4);
      } else {
         s32ReturnValue = -1;
         u32LastErrorCode = -1;
      }
#ifdef VASCO_OS_WINNT
      LeaveCriticalSection((CRITICAL_SECTION*)m_pPostCS);
#endif
#ifdef VASCO_OS_LINUX
      pthread_mutex_unlock(&mPostMutex);
#endif
   } else {
      s32ReturnValue = -1;
      u32LastErrorCode = -1;
   }
   delete[] sendBuf;
   return s32ReturnValue;
}

int OSALSocketInterface::s32MsgQueueWait(unsigned long u32Handle, unsigned long u32BufSize, 
                                         unsigned long u32TimeOut, unsigned char *pu8MsgBuf,
                                         unsigned long &u32Prio)
{
    int s32ReturnValue = -1;

    // Finite timeout for MQ wait
    // Socket timeout handler registered
    if ( m_bTimeOutFlag )
    {
        unsigned long u32SplitTimeout = u32TimeOut / m_cu32Times;         // milliseconds
        unsigned long u32RecvTimeout  = u32SplitTimeout * m_cu32Factor;   // milliseconds
        unsigned long u32TotalTimeout = u32SplitTimeout;                  // milliseconds

        bool bSendAgain = TRUE;

        // Send sequence of requests to server with split timeout
        while ( (u32TotalTimeout <= u32TimeOut ) && bSendAgain )
        {
            s32ReturnValue = s32MsgQueueTimedWait( u32Handle, u32BufSize, u32SplitTimeout, pu8MsgBuf, u32Prio, u32RecvTimeout );
            if (( s32ReturnValue == 0 )&&(u32LastErrorCode == OSALRPC_E_OSALAPI_TIMEOUT)) 
            {
                u32TotalTimeout += u32SplitTimeout;
            }
            else // s32MsgQueueTimedWait returned a message
            {
                bSendAgain = FALSE;
            }
        }
    }
    else  // Socket timeout handler not registered; Should freeze!
    {
        s32ReturnValue = s32MsgQueueTimedWait( u32Handle, u32BufSize, u32TimeOut, pu8MsgBuf, u32Prio );
    }

    return s32ReturnValue;
}

int OSALSocketInterface::s32MsgQueueTimedWait(unsigned long u32Handle, unsigned long u32BufSize, 
                                              unsigned long u32TimeOut, unsigned char *pu8MsgBuf,
                                              unsigned long &u32Prio, unsigned long u32RecvTimeout)
{
   int s32MsgQueueWaitCmd = 1011;
   int s32ReturnValue;
   char sendBuf[16];   
   if (clientSocket != 0) {
      memcpy(sendBuf,&s32MsgQueueWaitCmd,4);
      memcpy(sendBuf+4,&u32Handle,4);
      memcpy(sendBuf+8,&u32BufSize,4);
      memcpy(sendBuf+12,&u32TimeOut,4);
#ifdef VASCO_OS_WINNT
      EnterCriticalSection((CRITICAL_SECTION*)m_pReceiveCS);
#endif
#ifdef VASCO_OS_LINUX
      pthread_mutex_lock(&mPostMutex);
#endif
      clientSocket->RawSend(sendBuf,16);
      Sleep(10);
      char recvBuf[8];
      int s32MsgLength = clientSocket->TimedReceive( recvBuf, sizeof(recvBuf),u32RecvTimeout );
     if ( s32MsgLength == 8 )
        {
            memcpy( &u32LastErrorCode, recvBuf, 4 );
            memcpy( &s32ReturnValue, recvBuf + 4, 4 );

            char *pcReceiveBuffer = NULL;

			if(s32ReturnValue > u32BufSize)
			{
				//SSA8KOR : MMS31773
				s32ReturnValue = -3;
				u32LastErrorCode = -1;
			}
			else if ( s32ReturnValue > 0 ) // Contains non-zero size message
            {
                pcReceiveBuffer = new char[s32ReturnValue + 12 - s32MsgLength];
                while ( s32MsgLength < ( 12 + s32ReturnValue ) )
                {
                    int s32TmpMsgLength = clientSocket->TimedReceive( pcReceiveBuffer ,
                        s32ReturnValue + 12 - s32MsgLength,u32RecvTimeout);
                    if (s32TmpMsgLength > 0)
                    {
                        s32MsgLength += s32TmpMsgLength;
                    } 
                    else 
                    {
                        if (s32TmpMsgLength < 0)
                        {
                            u32LastErrorCode = -5;
                            delete [] pcReceiveBuffer;
#ifdef VASCO_OS_WINNT
                            LeaveCriticalSection((CRITICAL_SECTION*)m_pReceiveCS);
#endif
#ifdef VASCO_OS_LINUX
                            pthread_mutex_unlock(&mPostMutex);
#endif
                            return -5;
                        }
                    }
                }

                if ( s32MsgLength == ( 12 + s32ReturnValue ) ) // Got the Full Message 
                {
                    memcpy( pu8MsgBuf , pcReceiveBuffer, s32ReturnValue );
                    memcpy( &u32Prio , pcReceiveBuffer + s32ReturnValue, 4);
                    // print for debugging
                    if (log)
                    {
                        fprintf(log,"<MSG_RECV> <!-- incoming message length = %d -->\n",s32ReturnValue);
                        for (int i = 0; i < s32ReturnValue; i++) {
                            fprintf(log,"%02X%c",pu8MsgBuf[i] & 0xFF,(((i%16)==15)?'\n':' '));
                        }
                        fprintf(log,"\n</MSG_RECV>\n");
                        fflush(log);
                    }
                }
                else
                {
                    s32ReturnValue = -2;
                    u32LastErrorCode = -1;
                }
                delete [] pcReceiveBuffer;
            }
            else // zero size message. consume priority also
            {
                char recvBuf[4];
                int s32MsgLength = clientSocket->TimedReceive( recvBuf, sizeof(recvBuf),u32RecvTimeout);
                if ( s32MsgLength == 4 )
                {
                    memcpy( &u32Prio , recvBuf, 4 );
                }
                else
                {
                    s32ReturnValue = -3;
                    u32LastErrorCode = -1;
                }
            }
        }
        else
        {
            s32ReturnValue = -3;
            u32LastErrorCode = -1;
        }
#ifdef VASCO_OS_WINNT
     LeaveCriticalSection((CRITICAL_SECTION*)m_pReceiveCS);
#endif
#ifdef VASCO_OS_LINUX
     pthread_mutex_unlock(&mPostMutex);
#endif
    }
    else
    {
        s32ReturnValue = -4;
        u32LastErrorCode = -1;
    }

    return s32ReturnValue;

}

bool OSALSocketInterface::bInit(bool bEnableLog)
{
   static int iIFCounter = 0;
   iIFCounter++;
   bool bResult = false;
   clientSocket = new SimpleClient();
   if ( clientSocket != NULL )
   {
       log = 0;
       if (bEnableLog)
       {
           char szLogFileName[MAX_PATH] = { '\0' };
           sprintf_s( szLogFileName, MAX_PATH, "SocketTrace%u.log", iIFCounter );
           log = fopen( szLogFileName,"w" );
           if (log == 0) 
           {
               delete clientSocket;
               clientSocket = 0;
               return false;
           }
       }   

       if (s32SocketNumber > 0) 
       {
           if (clientSocket->Create(s32SocketNumber,szHostName) == SUCCESS) 
           {
               bResult = true;
           }
           else
           {
               if (log)
               {
                   fclose(log);
                   log = 0;
               }               
               delete clientSocket;
               clientSocket = 0;
           }
       }
   }
   return bResult;   

}

bool OSALSocketInterface::bEnd()
{
    if (log)
    {
        fclose(log);
        log = 0;
    }
    if (clientSocket != 0) {
        delete clientSocket;
        clientSocket = 0;
    }
    return true;
}




/** @return OSAL_tShMemHandle */
int OSALSocketInterface::hSharedMemoryCreate ( const char* szName, 
                                              int access, 
                                              unsigned int u32Size )
{
    int retVal = -1;
    int systemErrorCode = 0;

    char buffer[1024] = { 0 };
    int nameLength = strlen(szName) + 1;
    // functionID:
    *((unsigned int *)buffer) = 0x2d;
    // string:
    strcpy(buffer + 4, szName);
    // access rights:
    *((int *)(buffer + 4 + nameLength)) = access;
    // mem block size
    *((unsigned int *) (buffer + 8 + nameLength) ) = u32Size;
    if (clientSocket != NULL)
    {
        clientSocket->RawSend(buffer,12+nameLength);
        char recvBuf[12] = {0};
        int s32MsgLength = clientSocket->TimedReceive(recvBuf,12);
        if (s32MsgLength >= 12) 
        {
            if ( *((unsigned int*) recvBuf) == 0x802d)
            {
                retVal          = *((unsigned int*) (recvBuf + 4));
                systemErrorCode = *((unsigned int*) (recvBuf + 8));
            }
        }
        else
        {
            return 0;
        }
    }

    return retVal;
}


int OSALSocketInterface::hSharedMemoryOpen (const char* szName, int access)
{
    int retVal = -1;
    int systemErrorCode = 0;

    char buffer[1024] = { 0 };
    // functionID:
    *((unsigned int *)buffer) = 0x2e;
    // msg length:
    int nameLength = strlen(szName) + 1;
    // string:
    strcpy(buffer + 4, szName);
    // access rights:
    *((int *)(buffer + 4 + nameLength)) = access;
    if (clientSocket != NULL)
    {
        clientSocket->RawSend(buffer,8+nameLength);
        //      Sleep(10);
        char recvBuf[12] = {0};

        int s32MsgLength = clientSocket->TimedReceive(recvBuf,12);
        if (s32MsgLength >= 12) 
        {
            if ( *((unsigned int*) recvBuf) == 0x802e)
            {
                retVal          = *((unsigned int*) (recvBuf + 4));
                systemErrorCode = *((unsigned int*) (recvBuf + 8));
            }
        }
        else
        {
            return 0;
        }
    }

    return retVal;
}

int OSALSocketInterface::s32SharedMemoryClose (int handle)
{
    int retVal = -1;
    int systemErrorCode = 0;

    char buffer[1024] = { 0 };
    // functionID:
    *((unsigned int *)buffer) = 0x2f;
    // handle:
    *((int *)(buffer+4)) = handle;
    if (clientSocket != NULL)
    {
        clientSocket->RawSend(buffer,8);
        char recvBuf[12] = {0};

        int s32MsgLength = clientSocket->TimedReceive(recvBuf,12);
        if (s32MsgLength >= 12) 
        {
            if ( *((unsigned int*) recvBuf) == 0x802f)
            {
                retVal          = *((unsigned int*) (recvBuf + 4));
                systemErrorCode = *((unsigned int*) (recvBuf + 8));
            }
        }
        else
        {
            return 0;
        }
    }
    return retVal;
}

int OSALSocketInterface::s32SharedMemoryDelete (const char *szName)
{
    int retVal = -1;
    int systemErrorCode = 0;

    char buffer[1024] = { 0 };

    // functionID:
    *((unsigned int *)buffer) = 0x30;
    // msg length:
    int nameLength = strlen(szName) + 1;
    // string:
    strcpy(buffer + 4, szName);
    if (clientSocket != NULL)
    {
        clientSocket->RawSend(buffer,4+nameLength);
        char recvBuf[12] = {0};

        int s32MsgLength = clientSocket->TimedReceive(recvBuf,12);
        if (s32MsgLength >= 12) 
        {
            if ( *((unsigned int*) recvBuf) == 0x8030)
            {
                retVal          = *((unsigned int*) (recvBuf + 4));
                systemErrorCode = *((unsigned int*) (recvBuf + 8));
            }
        }
        else
        {
            return 0;
        }
    }
    return retVal;
}

void *OSALSocketInterface::pvSharedMemoryMap  ( int handle, 
                                               int access, 
                                               unsigned int length, 
                                               unsigned int offset  )
{
    void *sharedMemoryBlock = NULL;
    int systemErrorCode = 0;

    char buffer[1024] = { 0 };

    // functionID:
    *((unsigned int *)buffer) = 0x31;
    // handle:
    *((int *)(buffer+4)) = handle;
    // access:
    *((int *)(buffer+8)) = access;
    // length:
    *((int *)(buffer+12)) = length;
    // offset:
    *((int *)(buffer+16)) = offset;

    if (clientSocket != NULL)
    {
        clientSocket->RawSend(buffer, 20);
        char *recvBuf = new char[16 + length];

        int s32MsgLength = clientSocket->TimedReceive(recvBuf,16 + length);
        if (s32MsgLength >= 12) 
        {
            if ( *((unsigned int*) recvBuf) == 0x8031)
            {
                unsigned int memBlockId = *((unsigned int *) (recvBuf + 4));
                unsigned int returnedSize = *((unsigned int *) (recvBuf + 8));
                systemErrorCode            = *((unsigned int *) (recvBuf + 12 + returnedSize));

                if (memBlockId != 0 && returnedSize == length)
                {
                    sharedMemoryBlock = (void *) new char[length];
                    memcpy(sharedMemoryBlock, recvBuf + 12, length);
                    (*m_poSharedMemoryMap)[sharedMemoryBlock] = memBlockId;
                }
                else
                {
                    // Assert gibt es hier leider nicht...
                    *((int*)0x00000000) = 5;
                }
            }
        }
        else
        {
            return 0;
        }
        delete[] recvBuf;
    }

    return sharedMemoryBlock;
}

int OSALSocketInterface::s32SharedMemoryUnmap ( void * sharedMemory, 
                                               unsigned int size    )
{
    int retVal = -1;
    int systemErrorCode = 0;

    char buffer[1024] = { 0 };

    // functionID:
    *((unsigned int *)buffer) = 0x32;

    // lookup mapped pointer to shared memory area
    std::map<void *, int> ::const_iterator shmIter;

    shmIter = m_poSharedMemoryMap->find(sharedMemory);

    if ( shmIter != m_poSharedMemoryMap->end() )
       *((int *)(buffer+4)) = (int) shmIter->second;  // shm block found
    else
       return retVal;   // shm block not found; unmapped already?

    // handle:
    *((int *)(buffer+8)) = size;

    if (clientSocket != NULL)
    {
        clientSocket->RawSend(buffer,12);
        //      Sleep(10);
        char recvBuf[12] = {0};

        int s32MsgLength = clientSocket->TimedReceive(recvBuf,12);
        if (s32MsgLength >= 12) 
        {
            if ( *((unsigned int*) recvBuf) == 0x8032)
            {
                retVal          = *((unsigned int*) (recvBuf + 4));
                systemErrorCode = *((unsigned int*) (recvBuf + 8));
            }
        }
        else
        {
            return 0;
        }
    }

    if (retVal == 0 /* OSAL_OK */)
    {
        m_poSharedMemoryMap->erase(sharedMemory);
        delete[] (char*)sharedMemory;
    }

    return retVal;
}


int OSALSocketInterface::s32MsgPoolCreate(unsigned long u32PoolSize)
{
    int s32MsgQueueCloseCmd = 1000;
    int s32ReturnValue;
    char sendBuf[8];
    if (sendBuf != 0) {
        memcpy(sendBuf,&s32MsgQueueCloseCmd,4);
        memcpy(sendBuf+4,&u32PoolSize,4);
        clientSocket->RawSend(sendBuf,8);
        char recvBuf[8];
        int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
        if (s32MsgLength == 8) {
            memcpy(&u32LastErrorCode,recvBuf,4);
            memcpy(&s32ReturnValue,recvBuf+4,4);
        } else {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    } else {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32MsgPoolDelete()
{
    int s32MsgPoolCloseCmd = 1003;
    int s32ReturnValue;
    clientSocket->RawSend((char*)&s32MsgPoolCloseCmd,4);
    char recvBuf[8];
    int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
    if (s32MsgLength == 8) {
        memcpy(&u32LastErrorCode,recvBuf,4);
        memcpy(&s32ReturnValue,recvBuf+4,4);
    } else {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32IOOpen(unsigned long u32Access, const char* szFileName)
{
    int s32Cmd = 6;
    int s32ReturnValue;
    int s32FileNameLength = strlen(szFileName);
    if (clientSocket != 0)
    {
        char* sendBuf = new char[8+s32FileNameLength+1]; // 2 uint + 1 string
        if (sendBuf != 0) 
        {
            memcpy(sendBuf,&s32Cmd, 4);
            memcpy(sendBuf+4,&u32Access, 4);
            memcpy(sendBuf+8,szFileName,s32FileNameLength);
            sendBuf[8+s32FileNameLength] = 0;
            clientSocket->RawSend(sendBuf,8+s32FileNameLength+1);
            char recvBuf[8];
            int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
            if (s32MsgLength == 8) 
            {
                memcpy(&u32LastErrorCode,recvBuf,4);
                memcpy(&s32ReturnValue,recvBuf+4,4);
            } 
            else 
            {
                s32ReturnValue = -1;
                u32LastErrorCode = -1;
            }
            delete[] sendBuf;
        } 
        else 
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    }
    else
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32IOCreate(unsigned long u32Access, const char* szFileName)
{
    int s32Cmd = 9;
    int s32ReturnValue;
    int s32FileNameLength = strlen(szFileName);
    if (clientSocket != 0)
    {
        char* sendBuf = new char[8+s32FileNameLength+1]; // 2 uint + 1 string
        if (sendBuf != 0) 
        {
            memcpy(sendBuf,&s32Cmd, 4);
            memcpy(sendBuf+4,&u32Access, 4);
            memcpy(sendBuf+8,szFileName,s32FileNameLength);
            sendBuf[8+s32FileNameLength] = 0;
            clientSocket->RawSend(sendBuf,8+s32FileNameLength+1);
            char recvBuf[8];
            int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
            if (s32MsgLength == 8) 
            {
                memcpy(&u32LastErrorCode,recvBuf,4);
                memcpy(&s32ReturnValue,recvBuf+4,4);
            } 
            else 
            {
                s32ReturnValue = -1;
                u32LastErrorCode = -1;
            }
            delete[] sendBuf;
        } 
        else 
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    }
    else
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32IOClose(long s32Descriptor)
{
    int s32Cmd = 8;
    int s32ReturnValue;
    char sendBuf[8]; // 1 uint + 1 int 
    if (clientSocket != 0)
    {
        memcpy(sendBuf,&s32Cmd, 4);
        memcpy(sendBuf+4, &s32Descriptor, 4);
        clientSocket->RawSend(sendBuf,8);
        char recvBuf[8];
        int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
        if (s32MsgLength == 8) 
        {
            memcpy(&u32LastErrorCode,recvBuf,4);
            memcpy(&s32ReturnValue,recvBuf+4,4);
        } 
        else 
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    } 
    else 
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32IOCtrl(long s32Descriptor, long s32Fun, long s32Size, unsigned char *ps8Arg)
{
    int s32Cmd = 1110;
    int s32ReturnValue;
    if (clientSocket != 0) 
    {
        char *buf = new char[4+4+4+4+s32Size];
        if (buf != 0)
        {
            memcpy(buf,&s32Cmd,4);
            memcpy(buf+4,&s32Descriptor,4);
            memcpy(buf+8,&s32Fun,4);
            memcpy(buf+12,&s32Size,4);
            memcpy(buf+16,ps8Arg,s32Size);
            clientSocket->RawSend(buf, 16+s32Size);
            int s32MsgLength = clientSocket->TimedReceive(buf,12+s32Size);
            memcpy(&u32LastErrorCode,buf,4);
            memcpy(&s32ReturnValue,buf+4,4);
            memcpy(ps8Arg, buf+12, s32Size);
            delete [] buf;
        }
        else
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    } 
    else 
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32IOCtrl(long s32Descriptor, long s32Fun, long s32Arg)
{
    int s32Cmd = 11;
    int s32ReturnValue;
    if (clientSocket != 0) 
    {
        char buf[4+4+4+4];

        memcpy(buf,&s32Cmd,4);
        memcpy(buf+4,&s32Descriptor,4);
        memcpy(buf+8,&s32Fun,4);
        memcpy(buf+12,&s32Arg,4);
        clientSocket->RawSend(buf, 16);
        int s32MsgLength = clientSocket->TimedReceive(buf,8);
        memcpy(&u32LastErrorCode,buf,4);
        memcpy(&s32ReturnValue,buf+4,4);
    } 
    else 
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}

int OSALSocketInterface::s32IORemove(const char* szFileName)
{
    int s32Cmd = 10;
    int s32ReturnValue;
    int s32FileNameLength = strlen(szFileName);
    if (clientSocket != 0)
    {
        char* sendBuf = new char[4+s32FileNameLength+1]; // 1 uint + 1 string
        if (sendBuf != 0) 
        {
            memcpy(sendBuf,&s32Cmd, 4);
            memcpy(sendBuf+4,szFileName,s32FileNameLength);
            sendBuf[4+s32FileNameLength] = 0;
            clientSocket->RawSend(sendBuf,4+s32FileNameLength+1);
            char recvBuf[8];
            int s32MsgLength = clientSocket->TimedReceive(recvBuf,8);
            if (s32MsgLength == 8) 
            {
                memcpy(&u32LastErrorCode,recvBuf,4);
                memcpy(&s32ReturnValue,recvBuf+4,4);
            } 
            else 
            {
                s32ReturnValue = -1;
                u32LastErrorCode = -1;
            }
            delete[] sendBuf;
        } 
        else 
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    }
    else
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}


int OSALSocketInterface::s32IORead(long s32Descriptor, unsigned long u32BufSize, unsigned char* pu8Buf)
{
   int s32Cmd = 12;
   int s32ReturnValue;
   if (clientSocket != 0) 
   {
      char *buf = new char[4+u32BufSize];
      if (buf != 0)
      {
         // send command
         memcpy(buf,&s32Cmd,4);
         memcpy(buf+4,&s32Descriptor,4);
         memcpy(buf+8,&u32BufSize,4);
         clientSocket->RawSend(buf, 12);
         Sleep(10);

         int s32MsgLength = 0;
         int s32TempRead = 0;
         // read error code
         clientSocket->RawReceive((char*)&u32LastErrorCode, 4);

         // read size
         clientSocket->RawReceive((char*)&s32TempRead,4);

         //read data
         s32ReturnValue = 0;
         if (s32TempRead > 0)
         {
            do
            {
               s32TempRead -= s32MsgLength;
               s32MsgLength = clientSocket->RawReceive(&buf[s32ReturnValue],s32TempRead);
               s32ReturnValue += s32MsgLength;
            }
            while(s32MsgLength < s32TempRead);
         }

         if (s32ReturnValue != 0)
            memcpy(pu8Buf, buf, s32ReturnValue);
         else
            s32ReturnValue = -1;
         delete [] buf;
      }
      else
      {
         s32ReturnValue = -1;
         u32LastErrorCode = -1;
      }
   } 
   else 
   {
      s32ReturnValue = -1;
      u32LastErrorCode = -1;
   }
   return s32ReturnValue;

}

int OSALSocketInterface::s32IOWrite(long s32Descriptor, unsigned long u32Size, unsigned char* pu8Buf)
{
    int s32Cmd = 7;
    int s32ReturnValue;
    if (clientSocket != 0) 
    {
        char *buf = new char[4+4+4+u32Size];
        if (buf != 0)
        {
            memcpy(buf,&s32Cmd,4);
            memcpy(buf+4,&s32Descriptor,4);
            memcpy(buf+8,&u32Size,4);
            memcpy(buf+12, pu8Buf, u32Size);
            clientSocket->RawSend(buf, 12+u32Size);
            int s32MsgLength = clientSocket->TimedReceive(buf,8);
            memcpy(&u32LastErrorCode,buf,4);
            memcpy(&s32ReturnValue,buf+4,4);
            delete [] buf;
        }
        else
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    } 
    else 
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }
    return s32ReturnValue;
}


int OSALSocketInterface::s32IOEngFct ( const char *device, 
                                      long s32Fun, 
                                      unsigned char *ps8Arg, 
                                      long s32ArgSize )
{
    unsigned int u32Cmd = 0x35;     // IO-Engineering-Fct
    int s32ReturnValue = -1;

    if (clientSocket != 0) 
    {
        int reqSize = 4 + (strlen(device) + 1) + 4 + 4 + s32ArgSize;

        unsigned char *buf = new unsigned char[reqSize];
        if (buf != 0)
        {
            ByteArrayWriter writer (buf, reqSize);
            writer.bWriteU32(u32Cmd);
            writer.bWriteString(device);
            writer.bWriteS32(s32Fun);
            writer.bWriteS32(s32ArgSize);
            writer.bWriteRaw(ps8Arg, s32ArgSize);

            clientSocket->RawSend((char*) buf, writer.u32GetActualDataSize());
            int s32MsgLength = clientSocket->TimedReceive((char*) buf, 16 + s32ArgSize);
            ByteArrayReader reader (buf, s32MsgLength);
            reader.bReadU32 (&u32Cmd);
            reader.bReadU32 (&u32LastErrorCode);
            reader.bReadS32 (&s32ReturnValue);
            int retArgSize;
            reader.bReadS32 (&retArgSize);
            reader.bReadRaw (ps8Arg, retArgSize);
            delete [] buf;
        }
        else
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    } 
    else 
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }

    return s32ReturnValue;
}


int OSALSocketInterface::s32IOFakeExclAccess (const char *device)
{
    unsigned int u32Cmd = 0x36;     // IO-Engineering-Fct
    int s32ReturnValue = -1;

    if (clientSocket != 0) 
    {
        int reqSize = 4 + (strlen(device) + 1);

        unsigned char *buf = new unsigned char[reqSize];
        if (buf != 0)
        {
            ByteArrayWriter writer (buf, reqSize);
            writer.bWriteU32(u32Cmd);
            writer.bWriteString(device);

            clientSocket->RawSend((char*) buf, writer.u32GetActualDataSize());
            int s32MsgLength = clientSocket->TimedReceive((char*) buf, 12);
            ByteArrayReader reader (buf, s32MsgLength);
            reader.bReadU32 (&u32Cmd);
            reader.bReadU32 (&u32LastErrorCode);
            reader.bReadS32 (&s32ReturnValue);

            delete [] buf;
        }
        else
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    } 
    else 
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }

    return s32ReturnValue;
}

int OSALSocketInterface::s32IOReleaseFakedExclAccess (const char *device)
{
    unsigned int u32Cmd = 0x37;     // IO-Engineering-Fct
    int s32ReturnValue = -1;

    if (clientSocket != 0) 
    {
        int reqSize = 4 + (strlen(device) + 1);

        unsigned char *buf = new unsigned char[reqSize];
        if (buf != 0)
        {
            ByteArrayWriter writer (buf, reqSize);
            writer.bWriteU32(u32Cmd);
            writer.bWriteString(device);

            clientSocket->RawSend((char*) buf, writer.u32GetActualDataSize());
            int s32MsgLength = clientSocket->TimedReceive((char*) buf, reqSize);
            ByteArrayReader reader (buf, s32MsgLength);
            reader.bReadU32 (&u32Cmd);
            reader.bReadU32 (&u32LastErrorCode);
            reader.bReadS32 (&s32ReturnValue);

            delete [] buf;
        }
        else
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    } 
    else 
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }

    return s32ReturnValue;
}

int OSALSocketInterface::s32MiniSPMInitiateShutdown()
{
    unsigned int u32Cmd = 0x38;     // MiniSPM-Shutdown Request
    int s32ReturnValue = -1;

    if (clientSocket != 0) 
    {
        int reqSize = 4;

        unsigned char *buf = new unsigned char[reqSize];
        if (buf != 0)
        {
            ByteArrayWriter writer (buf, reqSize);
            writer.bWriteU32(u32Cmd);

            clientSocket->RawSend((char*) buf, writer.u32GetActualDataSize());
            int s32MsgLength = clientSocket->TimedReceive((char*) buf, reqSize);
            ByteArrayReader reader (buf, s32MsgLength);
            reader.bReadU32 (&u32Cmd);
            reader.bReadS32 (&s32ReturnValue);
            u32LastErrorCode = -1;

            delete [] buf;
        }
        else
        {
            s32ReturnValue = -1;
            u32LastErrorCode = -1;
        }
    } 
    else 
    {
        s32ReturnValue = -1;
        u32LastErrorCode = -1;
    }

    return s32ReturnValue;
}

int OSALSocketInterface::s32FSeek(long fd,int offset, int origin)
{
    int curPos;
    int endPos;
    if (origin == 0)
    {
        return s32IOCtrl(fd, ((long)1002), (long)offset);
    }
    else if (origin == 1)
    {
        if (-1 == s32IOCtrl(fd, ((int)1001), sizeof(int), (unsigned char *) &curPos))
            return -1;
        offset += curPos;
        return s32IOCtrl(fd, ((int)1002), offset);
    }
    else if (origin == 2)
    {
        if (-1 == s32IOCtrl(fd, ((int)1004),sizeof(int), (unsigned char *) &endPos))
            return -1;
        if (-1 == s32IOCtrl(fd, ((int)1001), sizeof(int), (unsigned char *) &curPos))
            return -1;
        offset = curPos + endPos + offset;
        return s32IOCtrl(fd, ((int)1002), offset);
    }
    else
        return -1;
}

int OSALSocketInterface::s32FTell(long fd)
{
    int curPos;
    if (-1 == s32IOCtrl(fd, ((int)1001),sizeof(int), (unsigned char *) &curPos))
        return -1;
    else
        return curPos;
}

int OSALSocketInterface::s32FGetpos(long fd,int *ptr)
{
    return s32IOCtrl(fd, ((int)1001),sizeof(int), (unsigned char *) ptr);
}

int OSALSocketInterface::s32FSetpos(long fd,const int *ptr)
{
    return s32IOCtrl(fd, ((int)1002), *ptr);
}

int OSALSocketInterface::s32FGetSize(long fd)
{
    int front;
    int back;
    if (-1 == s32IOCtrl(fd, ((int)1001),sizeof(int), (unsigned char *) &front))
        return -1;
    if (-1 == s32IOCtrl(fd, ((int)1004),sizeof(int), (unsigned char *) &back))
        return -1;
    return front+back;
}

int OSALSocketInterface::s32CreateDir(long fd,const char* szDirectory)
{
    return s32IOCtrl(fd, ((int)1005),256, (unsigned char *) szDirectory);
}

int OSALSocketInterface::s32RemoveDir(long fd,const char* szDirectory)
{
    return s32IOCtrl(fd, ((int)1006), 256, (unsigned char *) szDirectory);
}

intptr_t OSALSocketInterface::prOpenDir(const char* szDirectory)
{
    int s32Cookie=0;
    int s32Return=-1;

    char * Buffer = new char[sizeof(int) + sizeof(int) + 256];// Allocate memory for OSAL_trIOCtrlDir
    if (Buffer == NULL)
        return 0;

    s32Return = s32IOOpen(0x0004,szDirectory); // Open with READWRITE access
    if (s32Return== -1)
    {
        free(Buffer);
        return 0;
    }
    memcpy(Buffer,&s32Return,sizeof(int)); // Copy the file descriptor 
    memcpy(Buffer+sizeof(int),&s32Cookie,sizeof(int));// Copy the s32Cookie
    return (intptr_t)Buffer;
}

intptr_t OSALSocketInterface::prReadDir (long pDir)
{
    char * Buffer =(char*)pDir;
    int fd =-1;
    int pDirent=-1;

    memcpy(&fd,Buffer,sizeof(int));
    if (s32IOCtrl((long)fd, ((long)1003),sizeof(int) + sizeof(int) + 256,(unsigned char*)pDir)== -1)
    {
        return 0;
    }
    else
    {
        pDirent = pDir + sizeof(int) + sizeof(int); // Pointer to the Dirent structure 
        return pDirent;
    }
}

int OSALSocketInterface::s32CloseDir (long pDir)
{
    char * Buffer = (char*) pDir;
    int fd=-1;

    memcpy(&fd,Buffer,sizeof(int));
    s32IOClose((long)fd);
    delete Buffer;

    return 0;
}

