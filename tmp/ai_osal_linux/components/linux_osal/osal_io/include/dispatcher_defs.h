/************************************************************************
| FILE:         Dispatcher_defs.h
| PROJECT:      BMW_L6
| SW-COMPONENT: Dispatcher
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for file system device drivers
|               inclusion
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH
| HISTORY:      
| Date      | Modification               | Author
| 17.01.06  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----

|************************************************************************/
#if !defined (DISPATCHER_DEFS_HEADER)
   #define DISPATCHER_DEFS_HEADER

#include "dispatcher.h"

/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/

/* Definitions for asynchronous I/O */
#define C_MAX_JOBS       3
#define C_INVALID_JOB    0xFFFFFFFF
#define C_EVENT_SHUTDOWN_ACK(n) (1UL << (n))


#define  IS_DEVICE      -1
#define  IS_DIR         -2


/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

#define PRM_ILLEGAL_INDEX -1

#define OSAL_DESCRIPTOR_MAGIC         0xD0E1D0E1

typedef struct
{
   tU32 tID;                      /* osal I/O descriptor of this file  */
   OSAL_tenFSDevID   tDevType;    /* descriptor of target FS component */
   tVoid            *tNext;       /* pointer to next list element      */
   tU16              u16AppID;    /* PRM new */
   tUInt             nJobQueue;   /* AsyncIO new */
} OSAL_tFileDescriptor;

tBool OSAL_bInitAsyncIO( tVoid );
tS32  FSFileRelink( OSAL_tFileDescriptor* pFD );
OSAL_tFileDescriptor* FSFileUnlink( tU32 u32ID );


typedef struct FsTable
{
   OSAL_tFileDescriptor  *tTempEl;
   OSAL_tFileDescriptor  *tPrevEl;
} StrCurList;

typedef struct
{
  tU16 u16ExAppID;  /*PRM new*/
  tU16 u16AppID;    /*PRM new*/
} OSAL_ExclusiveDriveHandling;

typedef enum
{
   OSAL_EN_DEV_MULTIPLE_USE,
   OSAL_EN_DEV_NOT_USED,
   OSAL_EN_DEV_USED
}OSAL_tenDevAccess;

typedef tS32 (*tOpenPtr) (tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16 appid);
typedef tS32 (*tCreatePtr) (tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16 appid);
typedef tS32 (*tClosePtr) (tS32 s32ID, uintptr_t u32FD);
typedef tS32 (*tIOControlPtr) (tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32Arg);
typedef tS32 (*tReadPtr) (tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size);
typedef tS32 (*tWritePtr) (tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size);
typedef tS32 (*tRemovePtr) (tS32 s32ID, tCString szName);

typedef struct
{
  tU32                 magic;
  struct OsalDevices*  device_ptr;
  tU16                 u16AppID;    /* PRM new */
  tUInt                nJobQueue;   /* AsyncIO new */
  uintptr_t                 local_ptr;
  tU16                 u16FileSystem;
  tU16                 u16PrmIndex;
  OSAL_trAsyncControl* pAIO;
  tU32                 u32ID;
  tS32                 s32Pid;
} OsalDeviceDescriptor;


/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/


extern tCString Drive[];
/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
OSAL_tFileDescriptor *FSReturnLast  ( tVoid );

OSAL_tenFSDevID OSAL_get_drive_id( OSAL_tIODescriptor descriptor );
tU16 OSAL_get_exapp_id( OSAL_tIODescriptor descriptor );
tBool OSAL_test_and_set_inuse_descriptor( OSAL_tIODescriptor descriptor );
tS32 OSAL_s32IOControl_plain( tCString name, uintptr_t local_ptr, tS32 fun, intptr_t arg );
tS32 s32OSAL_get_device_id_and_filename_by_Path( tString *name );


#else
#error dipatcher_defs.h included several times
#endif 

