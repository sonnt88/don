/************************************************************************
| FILE:         osalio_public.h
| PROJECT:      BMW_L6
| SW-COMPONENT: Dispatcher
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for file system device drivers
|               inclusion
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Bosch GmbH
| HISTORY:      
| Date        | Modification              					| Author
| 03.10.05  | Initial revision          						| MRK2HI
| 11.10.12  | Included a new OSAL device AcousticSRC	| NRO2KOR
| --.--.--  | ------------------------------------| -------, -----

|************************************************************************/
#if !defined (DISPATCHERPUBLIC_HEADER)
   #define DISPATCHERPUBLIC_HEADER

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef enum
{
    OSAL_EN_DEVID_AUXCLK,
    OSAL_EN_DEVID_ODO,
    OSAL_EN_DEVID_GYRO,
    OSAL_EN_DEVID_ADC0, 
    OSAL_EN_DEVID_AUDIOOUT,
    OSAL_EN_DEVID_AUDIOIN,
    OSAL_EN_DEVID_KDS,
    OSAL_EN_DEVID_FFD,
    OSAL_EN_DEVID_ERRMEM,
    OSAL_EN_DEVID_TRACE,
    OSAL_EN_DEVID_CDCTRL,
    OSAL_EN_DEVID_CDAUDIO,
    OSAL_EN_DEVID_GPS,
    OSAL_EN_DEVID_GPIO,
    OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH,
    OSAL_EN_DEVID_CHENC,
    OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC,
    OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH,
    OSAL_EN_DEVID_DIAG_EOL,
    OSAL_EN_DEVID_TOUCHSCREEN,
    OSAL_EN_DEVID_ABS,
    OSAL_EN_DEVID_VOLT,
    OSAL_EN_DEVID_ADAS,
    OSAL_EN_DEVID_PRM,
    OSAL_EN_DEVID_USBPWR,
    OSAL_EN_DEVID_WUP,
    OSAL_EN_DEVID_ACOUSTIC_IF_SRC,
    OSAL_EN_DEVID_ACC,
    OSAL_EN_DEVID_ADR3CTRL,
    OSAL_EN_DEVID_GNSS,
    OSAL_EN_DEVID_AC_ECNR_IF_SPEECH,
    OSAL_EN_DEVID_PRAM,
    OSAL_EN_DEVID_AARS_DAB,
    OSAL_EN_DEVID_AARS_SSI32,
    OSAL_EN_DEVID_AARS_MTD,
    OSAL_EN_DEVID_AARS_AMFM,
    OSAL_EN_DEVID_LAST
 }OSAL_tenNfsDevId;

 typedef enum
 {
   OSAL_EN_DEVID_FFS_FFS,     // internal
   OSAL_EN_DEVID_FFS_FFS2,    // internal
   OSAL_EN_DEVID_FFS_FFS3,    // internal
   OSAL_EN_DEVID_FFS_FFS4,    // internal
   OSAL_EN_DEVID_RAMDISK,     // internal
   OSAL_EN_DEVID_REGISTRY,    // internal
   OSAL_EN_DEVID_ROOT,        // internal
   OSAL_EN_DEVID_CRYPTNAVROOT,// internal
   OSAL_EN_DEVID_CRYPTNAV,    // internal
   OSAL_EN_DEVID_NAVDB,       // internal
   OSAL_EN_DEVID_DEV_MEDIA,   // internal
   OSAL_EN_DEVID_DVD,         // add internal devices before !!!
   OSAL_EN_DEVID_CARD,
   OSAL_EN_DEVID_CRYPT_CARD,
   OSAL_EN_DEVID_LOCAL,
   OSAL_EN_DEVID_DATA,
   OSAL_EN_DEVID_FSLAST
 }OSAL_tenFSDevID;


tU32 u32ConvertError(tS32 s32ErrCd);



 #ifdef __cplusplus
}
#endif

#else
#error osalio_public.h included several times
#endif
