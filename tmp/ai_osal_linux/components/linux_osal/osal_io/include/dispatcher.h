/************************************************************************
| FILE:         Dispatcher.h
| PROJECT:      BMW_L6
| SW-COMPONENT: Dispatcher
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for file system device drivers
|               inclusion
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH
| HISTORY:      
| Date      | Modification                   | Author
| 03.10.05  | Initial revision               | MRK2HI
| 03.04.13  | To provide fix to NIKAI-4347   |
|           | Fix the Path Max Name Lenght   | SWM2KOR
| --.--.--  | ----------------               | -------, -----

|************************************************************************/
#if !defined (DISPATCHER_HEADER)
   #define DISPATCHER_HEADER

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include<dispatcher_device.h>
#include<osalio_public.h>

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/

#define OSAL_MAX_DEVNAME_LEN            25

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/


/* GGS2HI: list nodes for async i/o */
typedef enum
{
   OSAL_EN_ASYNC_PENDING,
   OSAL_EN_ASYNC_WORKING,
   OSAL_EN_ASYNC_CANCELLED,
   OSAL_EN_ASYNC_DONE,
   OSAL_EN_ASYNC_SHUTDOWN
} OSAL_tenAsyncState;

typedef struct OSAL_rAsyncNode
{
   OSAL_trAsyncControl*    prAsyncControl;
   OSAL_tenAccess          enAccess;
   OSAL_tenAsyncState      enState;
   tU32                    u32Priority;
   tUInt                   nJobQueue;
   struct OSAL_rAsyncNode* prNext;
} OSAL_trAsyncNode;


typedef tPVoid             OSAL_tpParam;


typedef struct OSAL_trDirent
{
  struct OSAL_trDirent *tpNext;
  tS8                  s8Name [OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET]; //To update the size for handling App folder name upto 256 apart osal mapping
} OSAL_trDirent; 


typedef struct OSAL_tfdIO
{
   tChar                  szName[OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET]; //To update the size for handling App folder name upto 256 apart osal mapping
   tChar                  szFileName[OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET];//To update the size for handling App folder name upto 256 apart osal mapping
   OSAL_tenAccess         enAccess;
   OSAL_tIODescriptor     fd;
   tU32                   u32Size;
   tS32                    offset;
   struct OSAL_trDirent   *tpDirent;
   struct OSAL_trDirent   *tpDirentTable;
   tS32                    DispId;
} OSAL_tfdIO;


/* defines for traces */
#define EN_FS_DISPATCHER   0x00
#define EN_NFS_DISPATCHER  0x80	

enum
{
   EN_IOOPEN_RESULT,
   EN_IOCLOSE_RESULT,
   EN_IOCONTROL_RESULT,
   EN_IOCREATE_RESULT,
   EN_IOREMOVE_RESULT,
   EN_IOWRITE_RESULT,
   EN_IOREAD_RESULT,
   EN_IOASYNC_READ_RESULT,
   EN_IOASYNC_WRITE_RESULT,
   EN_IOASYNC_RETURN_RESULT,
   EN_IOASYNC_CANCEL_RESULT,
   EN_IOASYNC_ERROR_RESULT,
   EN_IOCOPYFILE_RESULT,
   EN_IORMDIR_RESULT,
   EN_IOCOPYDIR_RESULT
};


enum
{
   EN_CTRL_FIOWHERE,
   EN_CTRL_FIOSEEK,
   EN_CTRL_FIOTOTALSIZE,
   EN_CTRL_FIOFREESIZE,
   EN_CTRL_FIONREAD,
   EN_CTRL_FIOMKDIR,
   EN_CTRL_FIORMDIR,
   EN_CTRL_RELOAD,
   EN_CTRL_CREATE,
   EN_CTRL_READDIR,
   EN_CTRL_FIOCHKDSK,
   EN_CTRL_FIOOPENDIR,
   EN_CTRL_READDIREXT,
   EN_CTRL_FIORMRECURSIVE,
   EN_CTRL_FIORENAME,
   EN_CTRL_FIOFORMAT,
   EN_CTRL_FIODIRCOPY,
   EN_CTRL_FIORMRECURSIVECANCEL,
   EN_CTRL_FIOPREPAREEJECT,
   EN_CTRL_READDIREXT2,
   EN_CTRL_FIOSAVENOW,
   EN_CTRL_FIOIPOD_ACTIVATE_USB_VBUS,
   EN_CTRL_FIOIPOD_DEACTIVATE_USB_VBUS,
   EN_CTRL_FIOUSBH_GET_DEVICEINFO,
   EN_CTRL_FIOUSBH_GET_HUBDEVICEINFO,
   EN_CTRL_FIOGETMOUNTPOINTINFO,
   EN_CTRL_FIOSETMOUNTPOINTACCESS,
   EN_CTRL_FIOGETMOUNTPOINTACCESS,
   EN_CTRL_FIOFLUSH,
   EN_CTRL_FIOCARD_STATE,
   EN_CTRL_FIOGET_REAL_PATH,
   EN_CTRL_FIOGET_CID,
   EN_CTRL_FIOTRIGGER_SIGN_VERIFY,
   EN_CTRL_FIOCRYPT_VERIFY_STATUS,
   EN_CTRL_FIOGET_SIGNATURE_TYPE,
   EN_CTRL_eMMC_REFRESH_START,
   EN_CTRL_eMMC_REFRESH_STOP ,
   EN_CTRL_eMMC_GET_REFRESH_STATE,
   EN_CTRL_FIOCHMOD,
   EN_CTRL_FIOCHOWN
};

 /************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

tBool IsNfsDev(OSAL_tIODescriptor fd);
tBool GetFsDevID( tCString szName, OSAL_tenFSDevID *tID );
tBool IsAFile( tU32 u32fd, OSAL_tenFSDevID *pDevID );
tS32  GetNucleusLocName(tCString coszName, tString szLocName,tCString lDev);
tCString GetOsalDeviceName( OSAL_tIODescriptor prIoH );

#ifdef __cplusplus
}
#endif

#else
#error dispatcher.h included several times
#endif
