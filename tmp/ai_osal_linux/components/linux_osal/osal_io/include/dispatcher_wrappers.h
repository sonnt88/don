/************************************************************************
| FILE:         Dispatcher.h
| PROJECT:      platform
| SW-COMPONENT: Dispatcher
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for file system device drivers  
|               inclusion
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH
| HISTORY:      
| Date      | Modification               | Author
| 17.01.06  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----

|************************************************************************/
#if !defined (DISPATCHER_WRAPPERS_HEADER)
   #define DISPATCHER_WRAPPERS_HEADER


/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
#define DISPATCHER_STATIC static

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

 /************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/


/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/

tU32 u32LoadSharedObject(void* pFunc,char* szName)
{
   tU32 u32Ret = OSAL_E_NOERROR;
   /* check again to avoid double loading */
   if(pFunc == NULL)
   {
      if(s32LoadModul(szName) == OSAL_ERROR)
      {
         u32Ret = OSAL_E_NOTSUPPORTED;
      }
   }
   return u32Ret;
}


/*****************************************************************************************
 *                                                                                       *
 *                                       wrappers                                        *
 *                                                                                       *
 *****************************************************************************************/

/*****************************************************************************************
 *****************************************************************************************
 *****************************************************************************************
 * Non File System Devices
 *****************************************************************************************
 *****************************************************************************************
 *****************************************************************************************/
/*****************************************************************************************
 * /dev/trace
 *****************************************************************************************/
tS32 wrapper_trace_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   return OSAL_E_NOERROR;
}

tS32 wrapper_trace_io_close(tS32 s32ID, uintptr_t u32FD)
{
   return OSAL_E_NOERROR;
}

tS32 wrapper_trace_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg)
{
   return((tS32)TRACE_s32IOControl(OSAL_C_U32_TRACE_OUTPUT, s32fun, s32arg));
}

tS32 wrapper_trace_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   return TRACE_s32IOWrite(s32ID,(const OSAL_trIOWriteTrace*)pBuffer,u32Size);
}

tS32 wrapper_trace_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   return((tU32)OSAL_E_NOTSUPPORTED);
}


/*****************************************************************************************
 * /dev/kds
 *****************************************************************************************/
tS32 wrapper_kds_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pKdsOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pKdsOpen,pOsalData->rLibrary[EN_SHARED_KDS].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pKdsOpen)
   {
       return pKdsOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
      return((tU32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_kds_io_close(tS32 s32ID, uintptr_t  u32FD)
{    
   if(pKdsClose != NULL)
   {
      return pKdsClose(s32ID, u32FD); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}
tS32 wrapper_kds_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pKdsIOControl != NULL)
   {
      return pKdsIOControl(s32ID,u32FD,s32fun, s32arg); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}

tS32 wrapper_kds_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{    
   if(pKdsWrite != NULL)
   {
      return pKdsWrite(s32ID, u32FD, pBuffer, u32Size, ret_size); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}

tS32 wrapper_kds_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{  
   if(pKdsRead != NULL)
   {
      return pKdsRead(s32ID, u32FD, pBuffer, u32Size, ret_size); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}




/*****************************************************************************************
 * /dev/pram
 *****************************************************************************************/
tS32 wrapper_pram_io_open (tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16 appid)
{
   if(pPramOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pPramOpen,pOsalData->rLibrary[EN_SHARED_BASE].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pPramOpen)
   {
       return pPramOpen(s32Id, szName, enAccess, pu32FD, appid);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_pram_io_close (tS32 s32ID, uintptr_t  u32FD)
{
   if(pPramClose != NULL)
   {
      return pPramClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_pram_io_ctrl(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32Arg)
{
   if(pPramIOControl != NULL)
   {
      return pPramIOControl(s32ID, u32FD, s32fun, s32Arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_pram_io_read (tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size)
{
   if(pPramRead != NULL)
   {
      return pPramRead(s32ID, u32FD, pBuffer, u32Size, u32_ret_Size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_pram_io_write (tS32 s32ID, uintptr_t  u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size)
{
   if(pPramWrite != NULL)
   {
      return pPramWrite(s32ID, u32FD, pBuffer, u32Size, u32_ret_Size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}


/*****************************************************************************************
 * /dev/acousticout
 *****************************************************************************************/
tS32 wrapper_acousticout_open(tS32 s32ID,
                         tCString szName,
                         OSAL_tenAccess enAccess,
                         uintptr_t *pu32FD,
                         tU16  app_id)
{
   if(pAcOutOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAcOutOpen,pOsalData->rLibrary[EN_SHARED_ACOUSTIC].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAcOutOpen)
   {
       return pAcOutOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_acousticout_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAcOutClose != NULL)
   {
      return pAcOutClose(s32ID, u32FD); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}
tS32 wrapper_acousticout_ioctl(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pAcOutIOControl != NULL)
   {
      return pAcOutIOControl(s32ID,u32FD,s32fun, s32arg); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}
tS32 wrapper_acousticout_write(tS32 s32ID,
                                                 uintptr_t  u32FD,
                                                 tPCS8 pBuffer,
                                                 tU32 u32Size,
                                                 uintptr_t *ret_size)
{
   if(pAcOutWrite != NULL)
   {
      return pAcOutWrite(s32ID, u32FD, pBuffer, u32Size, ret_size); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}

/*****************************************************************************************
 * /dev/acousticin
 *****************************************************************************************/
tS32 
wrapper_acousticin_open(tS32 s32ID,
                        tCString szName,
                        OSAL_tenAccess enAccess,
                        uintptr_t *pu32FD,
                        tU16  app_id)
{
   if(pAcInOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAcInOpen,pOsalData->rLibrary[EN_SHARED_ACOUSTIC].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAcInOpen)
   {
       return pAcInOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_acousticin_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAcInClose != NULL)
   {
      return pAcInClose(s32ID, u32FD); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}
tS32 wrapper_acousticin_ioctl(tS32 s32ID, uintptr_t  u32FD,tS32 s32fun, intptr_t s32arg)
{
   if(pAcInIOControl != NULL)
   {
      return pAcInIOControl(s32ID,u32FD,s32fun, s32arg); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}
tS32 wrapper_acousticin_read(tS32 s32ID, uintptr_t  u32FD,tPS8 ps8Buffer, tU32 u32Size,uintptr_t *pRet_size)
{
   if(pAcInRead != NULL)
   {
      return pAcInRead(s32ID, u32FD, ps8Buffer, u32Size, pRet_size); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}

/*****************************************************************************************
 * /dev/acousticsrc
 *****************************************************************************************/
tS32
wrapper_acousticsrc_open(tS32 s32ID,
                         tCString szName,
                         OSAL_tenAccess enAccess,
                         uintptr_t *pu32FD,
                         tU16  app_id)
{
   if(pAcSrcOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAcSrcOpen,pOsalData->rLibrary[EN_SHARED_ACOUSTIC].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
          return OSAL_E_NOTSUPPORTED;
      }
   }
   if(pAcSrcOpen)
   {
       return pAcSrcOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_acousticsrc_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAcSrcClose != NULL)
   {
      return pAcSrcClose(s32ID, u32FD); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}
tS32 wrapper_acousticsrc_ioctl(tS32 s32ID, uintptr_t  u32FD,tS32 s32fun, intptr_t s32arg)
{
   if(pAcSrcIOControl != NULL)
   {
      return pAcSrcIOControl(s32ID,u32FD,s32fun, s32arg); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}

/*****************************************************************************************
 * /dev/acousticecnr
 *****************************************************************************************/
tS32 
wrapper_acousticecnr_open(tS32 s32ID,
                        tCString szName,
                        OSAL_tenAccess enAccess,
                        uintptr_t *pu32FD,
                        tU16  app_id)
{
   if(pAcCnrOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAcCnrOpen,pOsalData->rLibrary[EN_SHARED_ACOUSTIC].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAcCnrOpen)
   {
       return pAcCnrOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}
tS32 wrapper_acousticecnr_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAcCnrClose != NULL)
   {
      return pAcCnrClose(s32ID, u32FD); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}
tS32 wrapper_acousticecnr_ioctl(tS32 s32ID, uintptr_t  u32FD,
                                                tS32 s32fun, intptr_t s32arg)
{
   if(pAcCnrIOControl != NULL)
   {
      return pAcCnrIOControl(s32ID,u32FD,s32fun, s32arg); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}
tS32 wrapper_acousticecnr_read(tS32 s32ID, uintptr_t  u32FD,
                                               tPS8 ps8Buffer, tU32 u32Size,
                                               uintptr_t *pRet_size)
{
   if(pAcCnrRead != NULL)
   {
      return pAcCnrRead(s32ID, u32FD, ps8Buffer, u32Size, pRet_size); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}


/*****************************************************************************************
 * /dev/auxclock
*****************************************************************************************/
tS32 wrapper_auxclock_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pAuxOpen == NULL)
   {
      if(s32LoadModul(pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames) == OSAL_ERROR)
      {
          NORMAL_M_ASSERT_ALWAYS();
          return((tU32)OSAL_E_NOTSUPPORTED);
      }
      else
      {
         return pAuxOpen(s32ID, szName, enAccess, pu32FD,app_id);
      }
   }
   else
   {
      return pAuxOpen(s32ID, szName, enAccess, pu32FD,app_id);
   }
}

tS32 wrapper_auxclock_io_close(tS32 s32ID, uintptr_t  u32FD)
{
  if(pAuxClose == NULL)
  {
     if(s32LoadModul(pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames) == OSAL_ERROR)
     {
         NORMAL_M_ASSERT_ALWAYS();
         return((tU32)OSAL_E_NOTSUPPORTED);
     }
     else
     {
        return pAuxClose(s32ID, u32FD );
     }
  }
  else
  {
     return pAuxClose(s32ID, u32FD );
  }
}

tS32 wrapper_auxclock_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
    if(pAuxIOControl == NULL)
    {
       if(s32LoadModul(pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames) == OSAL_ERROR)
       {
           NORMAL_M_ASSERT_ALWAYS();
           return((tU32)OSAL_E_NOTSUPPORTED);
       }
       else
       {
          return pAuxIOControl(s32ID, u32FD, s32fun, s32arg );
       }
    }
    else
    {
       return pAuxIOControl(s32ID, u32FD, s32fun, s32arg );
    }
}

tS32 wrapper_auxclock_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
     if(pAuxRead != NULL)
     {
        return pAuxRead(s32ID, u32FD, pBuffer, u32Size, ret_size);
     }
     else
     {
        return((tS32)OSAL_E_NOTSUPPORTED);
     }
}

/*****************************************************************************************
 * /dev/diag/eol
 *****************************************************************************************/
tS32 wrapper_diag_eol_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pEolOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pEolOpen,pOsalData->rLibrary[EN_SHARED_EOL].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pEolOpen)
   {
       return pEolOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_diag_eol_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pEolClose == NULL)
   {
      return((tS32)OSAL_E_DOESNOTEXIST);
   }
   else
   {
       return pEolClose(s32ID,u32FD);
   }
}

tS32 wrapper_diag_eol_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pEolIOControl == NULL)
   {
      return((tS32)OSAL_E_DOESNOTEXIST);
   }
   else
   {
       return pEolIOControl(s32ID, u32FD, s32fun, s32arg);
   }
}

tS32 wrapper_diag_eol_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pEolWrite == NULL)
   {
      return((tS32)OSAL_E_DOESNOTEXIST);
   }
   else
   {
       return pEolWrite(s32ID, u32FD, pBuffer, u32Size, ret_size);
   }
}

tS32 wrapper_diag_eol_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pEolRead == NULL)
   {
      return((tS32)OSAL_E_DOESNOTEXIST);
   }
   else
   {
       return pEolRead(s32ID, u32FD, pBuffer, u32Size, ret_size);
   }
}


/*****************************************************************************************
 * /dev_ffd
 *****************************************************************************************/
tS32 wrapper_ffd_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pFfdOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pFfdOpen,pOsalData->rLibrary[EN_SHARED_EOL].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pFfdOpen)
   {
       return pFfdOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_ffd_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pFfdClose == NULL)
   {
      return((tS32)OSAL_E_DOESNOTEXIST);
   }
   else
   {
       return pFfdClose(s32ID,u32FD);
   }
}

tS32 wrapper_ffd_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pFfdRead == NULL)
   {
      return((tS32)OSAL_E_DOESNOTEXIST);
   }
   else
   {
      return pFfdRead(s32ID, u32FD, pBuffer, u32Size, ret_size);
   }
}

tS32 wrapper_ffd_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pcs8Buffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pFfdWrite == NULL)
   {
      return((tS32)OSAL_E_DOESNOTEXIST);
   }
   else
   {
       return pFfdWrite(s32ID, u32FD, pcs8Buffer, u32Size, ret_size);
   }
}

tS32 wrapper_ffd_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pFfdIOControl == NULL)
   {
      return((tS32)OSAL_E_DOESNOTEXIST);
   }
   else
   {
       return pFfdIOControl(s32ID, u32FD, s32fun, s32arg);
   }
}



tS32 wrapper_ERRMEM_S32IOOpen(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   return ERRMEM_S32IOOpen(s32ID, szName, enAccess, pu32FD, app_id);
}

tS32 wrapper_ERRMEM_S32IOClose(tS32 s32ID, uintptr_t  u32FD)
{
    return ERRMEM_S32IOClose(s32ID, u32FD);
}

tS32 wrapper_ERRMEM_s32IORead(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
    return ERRMEM_s32IORead( s32ID, u32FD, pBuffer, u32Size, ret_size);
}

tS32 wrapper_ERRMEM_s32IOWrite(tS32 s32ID, uintptr_t  u32FD, tPCS8 pcs8Buffer, tU32 u32Size, uintptr_t *ret_size)
{
    return ERRMEM_s32IOWrite(s32ID, u32FD, pcs8Buffer, u32Size, ret_size);
}

tS32 wrapper_ERRMEM_s32IOControl(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   return ERRMEM_s32IOControl(s32ID, u32FD, s32fun, s32arg);
}






/*****************************************************************************************
 * /dev_touch_screen
 *****************************************************************************************/
tS32 wrapper_touchscreen_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pTouchOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pTouchOpen,pOsalData->rLibrary[EN_SHARED_TOUCH].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pTouchOpen)
   {
       return pTouchOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_touchscreen_io_close(tS32 s32ID, uintptr_t  u32FD)
{
    if(pTouchClose != NULL)
   {
      return pTouchClose(s32ID, u32FD); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}

tS32 wrapper_touchscreen_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pTouchRead != NULL)
   {
      return pTouchRead(s32ID, u32FD, pBuffer, u32Size, ret_size); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}

tS32 wrapper_touchscreen_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pTouchWrite != NULL)
   {
      return pTouchWrite(s32ID, u32FD, pBuffer, u32Size, ret_size); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}

tS32 wrapper_touchscreen_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
  if(pTouchIOControl != NULL)
   {
      return pTouchIOControl(s32ID,u32FD,s32fun, s32arg); 
   }
   else
   {
      return((tU32)OSAL_E_DOESNOTEXIST);
   }
}


/*****************************************************************************************
 * /dev_cdctrl
 *****************************************************************************************/
tS32 wrapper_cdctrl_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pCdCtrlOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pCdCtrlOpen,pOsalData->rLibrary[EN_SHARED_OD].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pCdCtrlOpen)
   {
       return pCdCtrlOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_cdctrl_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pCdCtrlClose != NULL)
   {
      return pCdCtrlClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_cdctrl_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pCdCtrlIOControl != NULL)
   {
      return pCdCtrlIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

/*****************************************************************************************
 * /dev_cdaudio
 *****************************************************************************************/
tS32 wrapper_cdaudio_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pCdAudioOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pCdAudioOpen,pOsalData->rLibrary[EN_SHARED_OD].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pCdAudioOpen)
   {
       return pCdAudioOpen(s32ID, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_cdaudio_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pCdAudioClose != NULL)
   {
         return pCdAudioClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_cdaudio_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pCdAudioIOControl != NULL)
   {
       return pCdAudioIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

/*****************************************************************************************
 * /dev/gps
 *****************************************************************************************/
tS32 wrapper_gps_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pGpsOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pGpsOpen,pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pGpsOpen)
   {
       return pGpsOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_gps_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pGpsClose != NULL)
   {
      return pGpsClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_gps_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pGpsIOControl != NULL)
   {
      return pGpsIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_gps_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pGpsRead != NULL)
   {
      return pGpsRead(s32ID, u32FD, pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

/*****************************************************************************************
 * /dev/gnss
 *****************************************************************************************/
tS32 wrapper_gnss_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pGnssOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pGnssOpen,pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pGnssOpen)
   {
       return pGnssOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_gnss_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pGnssClose != NULL)
   {
      return pGnssClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_gnss_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pGnssIOControl != NULL)
   {
      return pGnssIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_gnss_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pGnssRead != NULL)
   {
      return pGnssRead(s32ID, u32FD, pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_gnss_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pcsBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pGnssWrite != NULL)
   {
      return pGnssWrite(s32ID, u32FD, pcsBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

/*****************************************************************************************
 * /dev/gpio
 *****************************************************************************************/
tS32 wrapper_gpio_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pGpioOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pGpioOpen,pOsalData->rLibrary[EN_SHARED_BASE].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pGpioOpen)
   {
       return pGpioOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_gpio_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pGpioClose != NULL)
   {
      return pGpioClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_gpio_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pGpioIOControl != NULL)
   {
      return pGpioIOControl(s32ID, u32FD,s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
/*****************************************************************************************
 * /dev/adc
 *****************************************************************************************/
tS32 wrapper_adc_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pAdcOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAdcOpen,pOsalData->rLibrary[EN_SHARED_BASE].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAdcOpen)
   {
       return pAdcOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_adc_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAdcClose != NULL)
   {
      return pAdcClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_adc_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   tS32 s32RetVal = 0;
   if(pAdcRead != NULL)
   {
         s32RetVal =pAdcRead(s32ID, u32FD,pBuffer, u32Size, ret_size);
         if( s32RetVal == OSAL_E_NOERROR)
         {
            s32RetVal = *ret_size;
         }
         return s32RetVal;
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_adc_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pAdcIOControl != NULL)
   {
      return pAdcIOControl(s32ID, u32FD,  s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}



tS32 wrapper_adr3_ctrl_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pAdr3CtrlOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAdr3CtrlOpen,pOsalData->rLibrary[EN_SHARED_ADR3].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAdr3CtrlOpen)
   {
       return pAdr3CtrlOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_adr3_ctrl_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAdr3CtrlClose != NULL)
   {
         return pAdr3CtrlClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_adr3_ctrl_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pAdr3CtrlIOControl != NULL)
   {
         return pAdr3CtrlIOControl(s32ID, u32FD,s32fun,s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_adr3_ctrl_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAdr3CtrlWrite != NULL)
   {
      return pAdr3CtrlWrite(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_adr3_ctrl_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAdr3CtrlRead != NULL)
   {
      return pAdr3CtrlRead(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_dab_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pAaRSDABOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAaRSDABOpen,pOsalData->rLibrary[EN_SHARED_AARS_DAB].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAaRSDABOpen)
   {
       return pAaRSDABOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_aars_dab_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAaRSDABClose != NULL)
   {
      return pAaRSDABClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_aars_dab_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pAaRSDABIOControl != NULL)
   {
         return pAaRSDABIOControl(s32ID, u32FD,s32fun,s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_dab_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAaRSDABWrite != NULL)
   {
      return pAaRSDABWrite(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_dab_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAaRSDABRead != NULL)
   {
      return pAaRSDABRead(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_ssi32_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pAaRSSSI32Open == NULL)
   {
      if(u32LoadSharedObject((void*)pAaRSSSI32Open,pOsalData->rLibrary[EN_SHARED_AARS_SSI32].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAaRSSSI32Open)
   {
       return pAaRSSSI32Open(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_ssi32_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAaRSSSI32Close != NULL)
   {
         return pAaRSSSI32Close(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_aars_ssi32_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pAaRSSSI32IOControl != NULL)
   {
         return pAaRSSSI32IOControl(s32ID, u32FD,s32fun,s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_ssi32_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAaRSSSI32Write != NULL)
   {
      return pAaRSSSI32Write(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_ssi32_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAaRSSSI32Read != NULL)
   {
      return pAaRSSSI32Read(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_mtd_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pAaRSMTDOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAaRSMTDOpen,pOsalData->rLibrary[EN_SHARED_AARS_MTD].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAaRSMTDOpen)
   {
       return pAaRSMTDOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_mtd_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAaRSMTDClose != NULL)
   {
         return pAaRSMTDClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_aars_mtd_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pAaRSMTDIOControl != NULL)
   {
         return pAaRSMTDIOControl(s32ID, u32FD,s32fun,s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_mtd_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAaRSMTDWrite != NULL)
   {
      return pAaRSMTDWrite(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_mtd_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAaRSMTDRead != NULL)
   {
      return pAaRSMTDRead(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_amfm_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pAaRSAMFMOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAaRSAMFMOpen,pOsalData->rLibrary[EN_SHARED_AARS_AMFM].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAaRSAMFMOpen)
   {
       return pAaRSAMFMOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_amfm_io_close(tS32 s32ID, tU32 u32FD)
{
   if(pAaRSAMFMClose != NULL)
   {
         return pAaRSAMFMClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}
tS32 wrapper_aars_amfm_io_control(tS32 s32ID, tU32 u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pAaRSAMFMIOControl != NULL)
   {
         return pAaRSAMFMIOControl(s32ID, u32FD,s32fun,s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_amfm_io_write(tS32 s32ID, tU32 u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAaRSAMFMWrite != NULL)
   {
      return pAaRSAMFMWrite(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_aars_amfm_io_read(tS32 s32ID, tU32 u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAaRSAMFMRead != NULL)
   {
      return pAaRSAMFMRead(s32ID, u32FD,pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

/*********************************************************************************
 *                                                                               *
 *                              driver with filesystem                           *
 *                                                                               *
 *********************************************************************************/
DISPATCHER_STATIC tS32 wrapper_fs_io_create(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   uintptr_t u32RetAddr = s32ID;
  
   if(s32ID == OSAL_EN_DEVID_CRYPT_CARD)
   {
      if     (pOsalData->u16CardMediaType1 == OSAL_C_U16_MEDIA_EJECTED)   u32ErrorCode = OSAL_E_MEDIA_NOT_AVAILABLE;
      else if(pOsalData->u16CardMediaType1 == OSAL_C_U16_INCORRECT_MEDIA) u32ErrorCode = OSAL_E_NOPERMISSION; 
      else if(pOsalData->bWriteProtected)                                 u32ErrorCode = OSAL_E_WRITE_PROTECTED;
      else if(pOsalData->enAccessSdCard < OSAL_EN_WRITEONLY)              u32ErrorCode = OSAL_E_NOACCESS;
      else;
   }
   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      if( (u32ErrorCode = LFS_u32IOCreate( (tCString)szName,
                                           (OSAL_tenAccess)enAccess,
                                           s32ID,
                                           &u32RetAddr)) == OSAL_E_NOERROR )
      {
            *pu32FD = u32RetAddr;  /* return the pointer of the file descriptor structure */
      }
   }
   return u32ErrorCode;
}


DISPATCHER_STATIC tS32 wrapper_fs_io_remove (tS32 s32ID, tCString szName)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
  
   if(s32ID == OSAL_EN_DEVID_CRYPT_CARD)
   {
      if     (pOsalData->u16CardMediaType1 == OSAL_C_U16_MEDIA_EJECTED)   u32ErrorCode = OSAL_E_MEDIA_NOT_AVAILABLE;
      else if(pOsalData->u16CardMediaType1 == OSAL_C_U16_INCORRECT_MEDIA) u32ErrorCode = OSAL_E_NOPERMISSION; 
      else if(pOsalData->bWriteProtected)                                 u32ErrorCode = OSAL_E_WRITE_PROTECTED;
      else if(pOsalData->enAccessSdCard < OSAL_EN_WRITEONLY)              u32ErrorCode = OSAL_E_NOACCESS;
      else;
   }
   if(u32ErrorCode == OSAL_E_NOERROR)  
   {
      u32ErrorCode = LFS_u32IORemove((tCString)szName, s32ID);
   }
   return u32ErrorCode;
}

DISPATCHER_STATIC tS32 wrapper_fs_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   uintptr_t u32RetAddr = s32ID;
   if(s32ID == OSAL_EN_DEVID_CRYPT_CARD)
   { 
      if     (pOsalData->u16CardMediaType1 == OSAL_C_U16_MEDIA_EJECTED)    u32ErrorCode = OSAL_E_MEDIA_NOT_AVAILABLE;
      //else if(pOsalData->u16CardMediaType1 == OSAL_C_U16_INCORRECT_MEDIA) u32ErrorCode = OSAL_E_NOPERMISSION; 
      else if(pOsalData->enAccessSdCard < OSAL_EN_READONLY)             u32ErrorCode = OSAL_E_NOACCESS;
   }
   if(u32ErrorCode == OSAL_E_NOERROR)  
   {
     if( (u32ErrorCode = LFS_u32IOOpen( szName,
                                        enAccess,
                                        s32ID,
                                        &u32RetAddr)) == OSAL_E_NOERROR )
     {
           *pu32FD = u32RetAddr;  /* return the pointer of the file descriptor structure */
     }
   }
  return u32ErrorCode;
}

DISPATCHER_STATIC tS32 wrapper_fs_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   if(s32ID == OSAL_EN_DEVID_CRYPT_CARD)
   { 
      if( pOsalData->enAccessSdCard < OSAL_EN_READONLY )             u32ErrorCode = OSAL_E_NOACCESS;
      // if( pOsalData->enAccessUsb[0] < OSAL_EN_READONLY )             u32ErrorCode = OSAL_E_NOACCESS;
      // else if ( pOsalData->u16SdaMediaType == OSAL_C_U16_INCORRECT_MEDIA ) u32ErrorCode = OSAL_E_NOPERMISSION; 
   }
   if(u32ErrorCode == OSAL_E_NOERROR)  
   {
         u32ErrorCode = LFS_u32IOClose(u32FD, s32ID);
   }
   return u32ErrorCode;
}

DISPATCHER_STATIC tS32 wrapper_fs_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR; 
   if(s32ID == OSAL_EN_DEVID_CRYPT_CARD)
   { 
      if     (pOsalData->u16CardMediaType1 == OSAL_C_U16_MEDIA_EJECTED)   u32ErrorCode = OSAL_E_MEDIA_NOT_AVAILABLE;
      else if(pOsalData->u16CardMediaType1 == OSAL_C_U16_INCORRECT_MEDIA) u32ErrorCode = OSAL_E_NOACCESS; 
      if((u32ErrorCode == OSAL_E_NOERROR)&&
         ((s32fun == OSAL_C_S32_IOCTRL_FIOMKDIR) || (s32fun == OSAL_C_S32_IOCTRL_FIORMDIR) ||
         (s32fun == OSAL_C_S32_IOCTRL_FIORENAME) || (s32fun == OSAL_C_S32_IOCTRL_FIOATTRIBSET) ||
         (s32fun == OSAL_C_S32_IOCTRL_FIORMRECURSIVE) || (s32fun == OSAL_C_S32_IOCTRL_FIOCOPYDIR) ||
         (s32fun == OSAL_C_S32_IOCTRL_FIOFLUSH)||(s32fun == OSAL_C_S32_IOCTRL_FIORMRECURSIVE_CANCEL)||
         (s32fun == OSAL_C_S32_IOCTRL_FIODISKFORMAT)))
      {
         if(pOsalData->bWriteProtected)u32ErrorCode = OSAL_E_WRITE_PROTECTED;
      }
      else if(pOsalData->enAccessSdCard < OSAL_EN_READONLY)
      {
         if(!bAccessAllowed(s32fun,pOsalData->enAccessSdCard))  u32ErrorCode = OSAL_E_NOACCESS;
      }
   }
   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      u32ErrorCode = LFS_u32IOControl( u32FD, s32fun, s32arg, s32ID );
   }
   return u32ErrorCode;
}

DISPATCHER_STATIC tS32 wrapper_fs_io_write(tS32 s32ID, uintptr_t  u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   *ret_size = s32ID;
   if(s32ID == OSAL_EN_DEVID_CRYPT_CARD)
   { 
       if     (pOsalData->u16CardMediaType1 == OSAL_C_U16_MEDIA_EJECTED)   u32ErrorCode = OSAL_E_MEDIA_NOT_AVAILABLE;
       else if(pOsalData->bWriteProtected)                               u32ErrorCode = OSAL_E_WRITE_PROTECTED;
       else if(pOsalData->enAccessSdCard < OSAL_EN_WRITEONLY)            u32ErrorCode = OSAL_E_NOACCESS;
   }
   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      u32ErrorCode = LFS_u32IOWrite(u32FD, (uintptr_t)pBuffer, u32Size, (intptr_t *)ret_size);
   }
   return u32ErrorCode;
}

DISPATCHER_STATIC tS32 wrapper_fs_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   *ret_size = s32ID;
   if(s32ID == OSAL_EN_DEVID_CRYPT_CARD)
   { 
       if     (pOsalData->u16CardMediaType1 == OSAL_C_U16_MEDIA_EJECTED)   u32ErrorCode = OSAL_E_MEDIA_NOT_AVAILABLE;
       else if(pOsalData->u16CardMediaType1 == OSAL_C_U16_INCORRECT_MEDIA) u32ErrorCode = OSAL_E_NOPERMISSION; 
       else if(pOsalData->enAccessSdCard < OSAL_EN_READONLY)               u32ErrorCode = OSAL_E_NOACCESS;
   }
   if(u32ErrorCode == OSAL_E_NOERROR)
   {
      u32ErrorCode = LFS_u32IORead(u32FD,(uintptr_t)pBuffer, u32Size, (intptr_t *)ret_size);
   }
   return u32ErrorCode;
}


/*****************************************************************************************
 * /dev/registry
 *****************************************************************************************/
DISPATCHER_STATIC tS32 wrapper_registry_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
  tU32 u32ErrorCode = OSAL_E_UNKNOWN;
  uintptr_t u32RetAddr;

  if(enAccess >= 64) enAccess = OSAL_EN_READWRITE;
  if ( (u32ErrorCode = REGISTRY_u32IOOpen((tCString)szName,
                                          (OSAL_tenAccess)enAccess,
                                          Drive[OSAL_EN_DEVID_REGISTRY],
                                          &u32RetAddr)) == OSAL_E_NOERROR )
  {
      *pu32FD = u32RetAddr;  /* return the pointer of the
                                file descriptor structure */
  }
  return u32ErrorCode;
}

DISPATCHER_STATIC tS32 wrapper_registry_io_close(tS32 s32ID, uintptr_t  u32FD)
{
  return ((tS32)REGISTRY_u32IOClose(u32FD));
}

DISPATCHER_STATIC tS32 wrapper_registry_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
  return((tS32)REGISTRY_u32IOControl(u32FD, s32fun, s32arg));
}

DISPATCHER_STATIC tS32 wrapper_registry_io_create(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
  tU32 u32ErrorCode = OSAL_E_UNKNOWN;
  uintptr_t u32RetAddr;

  if(enAccess >= 64) enAccess = OSAL_EN_READWRITE;
  if ( (u32ErrorCode = REGISTRY_u32IOCreate(szName,
                                            enAccess,
                                            Drive[OSAL_EN_DEVID_REGISTRY],
                                            &u32RetAddr)) == OSAL_E_NOERROR )
  {
      *pu32FD = u32RetAddr;  /* return the pointer of the
                                file descriptor structure */
  }
  return u32ErrorCode;
}

DISPATCHER_STATIC tS32 wrapper_registry_io_remove (tS32 s32ID, tCString szName)
{
  return REGISTRY_u32IORemove((tCString)szName, Drive[OSAL_EN_DEVID_REGISTRY]);
}

tS32 wrapper_prm_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pPrmFunc == NULL)
   {
      if(u32LoadSharedObject((void*)pPrmFunc,pOsalData->rLibrary[EN_SHARED_OSAL2].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   return ((tS32) OSAL_E_NOERROR);
}

tS32 wrapper_prm_io_close(tS32 s32ID, uintptr_t  u32FD)
{
  return ((tS32) OSAL_E_NOERROR);
}

tS32 wrapper_prm_io_ioctl(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, tS32 s32arg)
{
  /* dummy */
  return ((tS32) OSAL_E_NOERROR);
}

/*****************************************************************************************
 * /dev/odometer
 *****************************************************************************************/
tS32 wrapper_odometer_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pOdoOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pOdoOpen,pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pOdoOpen)
   {
      return pOdoOpen(s32Id, szName, enAccess, pu32FD,app_id);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_odometer_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pOdoClose != NULL)
   {
      return pOdoClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_odometer_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pOdoIOControl != NULL)
   {
      return pOdoIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_odometer_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pOdoRead != NULL)
   {
      return pOdoRead(s32ID, u32FD, pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

/*****************************************************************************************
 * /dev/abs
 *****************************************************************************************/
tS32 wrapper_abs_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pAbsOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAbsOpen,pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAbsOpen)
   {
      return pAbsOpen(s32Id, szName, enAccess, pu32FD,app_id);
   }
   else
   {
     return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_abs_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAbsClose != NULL)
   {
      return pAbsClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_abs_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pAbsIOControl != NULL)
   {
      return pAbsIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_abs_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAbsRead != NULL)
   {
      return pAbsRead(s32ID, u32FD, pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

/*****************************************************************************************
 * /dev/gyro
 *****************************************************************************************/
tS32 wrapper_gyro_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pGyroOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pGyroOpen,pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pGyroOpen)
   {
      return pGyroOpen(s32Id, szName, enAccess, pu32FD,app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_gyro_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pGyroClose != NULL)
   {
      return pGyroClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_gyro_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pGyroIOControl != NULL)
   {
      return pGyroIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_gyro_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pGyroRead != NULL)
   {
      return pGyroRead(s32ID, u32FD, pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}


/*****************************************************************************************
 * /dev/acc
 *****************************************************************************************/
tS32 wrapper_acc_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pAccOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pAccOpen,pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pAccOpen)
   {
      return pAccOpen(s32Id, szName, enAccess, pu32FD,app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_acc_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pAccClose != NULL)
   {
      return pAccClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_acc_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pAccIOControl != NULL)
   {
      return pAccIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_acc_io_read(tS32 s32ID, uintptr_t  u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size)
{
   if(pAccRead != NULL)
   {
      return pAccRead(s32ID, u32FD, pBuffer, u32Size, ret_size);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

/*****************************************************************************************
 * /dev/wup
 *****************************************************************************************/
tS32 wrapper_wup_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pWupOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pWupOpen,pOsalData->rLibrary[EN_SHARED_WUP].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pWupOpen)
   {
     return pWupOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_wup_io_close(tS32 s32ID, uintptr_t  u32FD)
{
   if(pWupClose != NULL)
   {
      return pWupClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_wup_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
   if(pWupIOControl != NULL)
   {
      return pWupIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

/*****************************************************************************************
 * /dev/volt
 *****************************************************************************************/
tS32 wrapper_volt_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
   if(pVoltOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pVoltOpen,pOsalData->rLibrary[EN_SHARED_VOLT].cLibraryNames) == OSAL_E_NOTSUPPORTED)
      {
        NORMAL_M_ASSERT_ALWAYS();
        return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pVoltOpen)
   {
      return pVoltOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
       return((tS32)OSAL_E_NOTSUPPORTED);  
   }
}

tS32 wrapper_volt_io_close(tS32 s32ID, uintptr_t  u32FD)
{
  if(pVoltClose != NULL)
  {
     return pVoltClose(s32ID, u32FD);
  }
  else
  {
     return((tS32)OSAL_E_NOTSUPPORTED);
  }
}

tS32 wrapper_volt_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
  if(pVoltIOControl != NULL)
  {
     return pVoltIOControl(s32ID, u32FD, s32fun, s32arg);
  }
  else
  {
     return((tS32)OSAL_E_NOTSUPPORTED);
  }
}

/*****************************************************************************************
 * /dev/chenc
 *****************************************************************************************/
tS32 wrapper_ChEnc_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32Id);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(szName);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(enAccess);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pu32FD);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(app_id);

   if(pChEncOpen == NULL)
   {
      if(u32LoadSharedObject((void*)pChEncOpen,pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames)== OSAL_E_NOTSUPPORTED)
      {
          NORMAL_M_ASSERT_ALWAYS();
          return((tU32)OSAL_E_NOTSUPPORTED);
      }
   }
   if(pChEncOpen)
   {
     return pChEncOpen(s32Id, szName, enAccess, pu32FD, app_id);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_ChEnc_io_close(tS32 s32ID, uintptr_t u32FD)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);

   if(pChEncClose != NULL)
   {
      return pChEncClose(s32ID, u32FD);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

tS32 wrapper_ChEnc_io_control(tS32 s32ID, uintptr_t  u32FD, tS32 s32fun, intptr_t s32arg)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(s32ID);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32FD);

   if(pChEncIOControl != NULL)
   {
      return pChEncIOControl(s32ID, u32FD, s32fun, s32arg);
   }
   else
   {
      return((tS32)OSAL_E_NOTSUPPORTED);
   }
}

#else
#error dipatcher_wrappers.h included several times
#endif 
