/************************************************************************
| FILE:         osfile.h
| PROJECT:      BMW_L6
| SW-COMPONENT: Dispatcher
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for file system device drivers
|               inclusion
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|************************************************************************/

#if !defined (OSFILE_HEADER)
   #define OSFILE_HEADER

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/


/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/

#define  IS_DEVICE            -1
#define  IS_DIR               -2
#define  IS_RESTRICTED_DEVICE -3

#define C_DEV_CARD         "card1"
#define C_DEV_CRYPTCARD    "crypt1"
#define C_DEV_RAMDISK      "ramdisk"

#define C_DEV_FFS_FFS      "ffs"
#define C_DEV_FFS_FFS2     "ffs2"
#define C_DEV_FFS_FFS3     "ffs3"
#define C_DEV_FFS_FFS4     "ffs4"

#define C_DEV_DVD          "dvd"
#define C_DEV_REGISTRY     "DUMMY1"
#define C_DEV_CRYPT_USB    C_DEV_USBA

#define C_DEV_ROOT         "root"
#define C_DEV_CRYPTNAVROOT "ncr"
#define C_DEV_CRYPTNAV     "ncn"
#define C_DEV_NAVDB        "ndb"
#define C_DEV_DEVMEDIA     "exd"
#define C_DEV_LOCAL        "loc"
#define C_DEV_DATA         "dat"



/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
tU32 LFS_u32IORead (uintptr_t u32fd, uintptr_t pBuffer, tU32 u32MaxLength, intptr_t *s32Read);
tU32 LFS_u32IOWrite (uintptr_t u32fd, uintptr_t pBuffer, tU32 u32Length,intptr_t *ps32Written);
tU32 LFS_u32IOControl (uintptr_t u32fd, tU32 fun, intptr_t arg, tS32 s32DeviceId );
tU32 LFS_u32IORemove (tCString coszName,  tS32 s32DeviceId );
tU32 LFS_u32IOCreate (tCString coszName,OSAL_tenAccess enAccess, tS32 s32DeviceId,uintptr_t *pu32Addr);
tU32 LFS_u32IOClose (uintptr_t u32fd, tS32 s32DeviceId);
tU32 LFS_u32IOOpen (tCString coszName,OSAL_tenAccess enAccess,tS32 s32DeviceId, uintptr_t *u32Addr);
tU32 u32GetUsbHostDeviceInfo(OSAL_trUsbDeviceInfo* pDevInfo,tU32 Chan);
void vStartErrmemWriteTask(tU32 u32Val);

#ifdef __cplusplus
}
#endif

#else
#error osfile.h included several times
#endif
