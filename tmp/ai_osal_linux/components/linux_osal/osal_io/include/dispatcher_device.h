/************************************************************************
| FILE:         Dispatcher_device.h
| PROJECT:      GMGE
| SW-COMPONENT: Dispatcher
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for file system device drivers
|               inclusion
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2008 Blaupunkt GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.03.08  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----

|************************************************************************/
#if !defined (DISPATCHER_DEVICE_HEADER)
   #define DISPATCHER_DEVICE_HEADER

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"


/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/


/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/* function prototypes for Dispatcher */
tS32 TRACE_s32IOControl(tS32 s32DevId, tS32 s32fun, intptr_t s32arg);
tS32 TRACE_s32IOWrite( tS32 s32DevId,const OSAL_trIOWriteTrace *prIOTrData, tU32 u32nbytes);
tS32 s32LoadModul(const char* szModuleName);
tBool bAccessAllowed(tS32 s32Fun, OSAL_tenAccess enAccess);

tS32 RegistryInit(void);
tU32 REGISTRY_u32IOCreate(tCString coszName,OSAL_tenAccess enAccess,
                          tCString Drive,uintptr_t *pu32Addr);
tU32 REGISTRY_u32IOOpen(tCString coszName,OSAL_tenAccess enAccess,
                        tCString Drive,uintptr_t *pu32Addr);
tU32 REGISTRY_u32IOControl( uintptr_t u32fd, tS32 fun, intptr_t arg);
tU32 REGISTRY_u32IOClose  ( uintptr_t u32fd);
tU32 REGISTRY_u32IORemove(tCString coszName,tCString Drive);

tS32 ERRMEM_S32IOOpen(tS32 s32Id, tCString szName,OSAL_tenAccess enAccess, uintptr_t *pu32FD,tU16 app_id);
tS32 ERRMEM_S32IOClose(tS32 s32ID, uintptr_t u32FD);
tS32 ERRMEM_s32IORead(tS32 s32ID, uintptr_t u32FD, tPS8 buffer,tU32 size, uintptr_t *ret_size);
tS32 ERRMEM_s32IOWrite(tS32 s32ID, uintptr_t u32FD, tPCS8 buffer,tU32 size, uintptr_t *ret_size);
tS32 ERRMEM_s32IOControl(tS32 s32ID, uintptr_t u32fd, tS32 io_func, intptr_t param);

extern drv_io_open      pGpioOpen;
extern drv_io_close     pGpioClose;
extern drv_io_control   pGpioIOControl;

extern drv_io_open      pAdcOpen;
extern drv_io_close     pAdcClose;
extern drv_io_control   pAdcIOControl;
extern drv_io_read      pAdcRead;

extern drv_io_open      pPramOpen;
extern drv_io_close     pPramClose;
extern drv_io_control   pPramIOControl;
extern drv_io_read      pPramRead;
extern drv_io_write     pPramWrite;

extern crypt_sign       pMediaFunc;

extern drv_io_open      pAdr3CtrlOpen;
extern drv_io_close     pAdr3CtrlClose;
extern drv_io_control   pAdr3CtrlIOControl;
extern drv_io_read      pAdr3CtrlRead;
extern drv_io_write     pAdr3CtrlWrite;

extern drv_io_open      pAaRSDABOpen;
extern drv_io_close     pAaRSDABClose;
extern drv_io_control   pAaRSDABIOControl;
extern drv_io_read      pAaRSDABRead;
extern drv_io_write     pAaRSDABWrite;

extern drv_io_open      pAaRSSSI32Open;
extern drv_io_close     pAaRSSSI32Close;
extern drv_io_control   pAaRSSSI32IOControl;
extern drv_io_read      pAaRSSSI32Read;
extern drv_io_write     pAaRSSSI32Write;

extern drv_io_open      pAaRSMTDOpen;
extern drv_io_close     pAaRSMTDClose;
extern drv_io_control   pAaRSMTDIOControl;
extern drv_io_read      pAaRSMTDRead;
extern drv_io_write     pAaRSMTDWrite;

extern drv_io_open      pAaRSAMFMOpen;
extern drv_io_close     pAaRSAMFMClose;
extern drv_io_control   pAaRSAMFMIOControl;
extern drv_io_read      pAaRSAMFMRead;
extern drv_io_write     pAaRSAMFMWrite;

extern drv_io_open      pKdsOpen;
extern drv_io_close     pKdsClose;
extern drv_io_control   pKdsIOControl;
extern drv_io_read      pKdsRead;
extern drv_io_write     pKdsWrite;

extern drv_io_open      pTouchOpen;
extern drv_io_close     pTouchClose;
extern drv_io_control   pTouchIOControl;
extern drv_io_read      pTouchRead;
extern drv_io_write     pTouchWrite;

extern drv_io_open      pFfdOpen;
extern drv_io_close     pFfdClose;
extern drv_io_control   pFfdIOControl;
extern drv_io_read      pFfdRead;
extern drv_io_write     pFfdWrite;

extern drv_io_open      pEolOpen;
extern drv_io_close     pEolClose;
extern drv_io_control   pEolIOControl;
extern drv_io_read      pEolRead;
extern drv_io_write     pEolWrite;

extern drv_io_open      pWupOpen;
extern drv_io_close     pWupClose;
extern drv_io_control   pWupIOControl;

extern drv_io_open      pVoltOpen;
extern drv_io_close     pVoltClose;
extern drv_io_control   pVoltIOControl;

extern drv_io_open      pCdCtrlOpen;
extern drv_io_close     pCdCtrlClose;
extern drv_io_control   pCdCtrlIOControl;

extern drv_io_open      pCdAudioOpen;
extern drv_io_close     pCdAudioClose;
extern drv_io_control   pCdAudioIOControl;

extern drv_io_open      pGpsOpen;
extern drv_io_close     pGpsClose;
extern drv_io_control   pGpsIOControl;
extern drv_io_read      pGpsRead;

extern drv_io_open      pGnssOpen;
extern drv_io_close     pGnssClose;
extern drv_io_control   pGnssIOControl;
extern drv_io_read      pGnssRead;
extern drv_io_write     pGnssWrite;

extern drv_io_open      pOdoOpen;
extern drv_io_close     pOdoClose;
extern drv_io_control   pOdoIOControl;
extern drv_io_read      pOdoRead;

extern drv_io_open      pGyroOpen;
extern drv_io_close     pGyroClose;
extern drv_io_control   pGyroIOControl;
extern drv_io_read      pGyroRead;

extern drv_io_open      pAbsOpen;
extern drv_io_close     pAbsClose;
extern drv_io_control   pAbsIOControl;
extern drv_io_read      pAbsRead;

extern drv_io_open      pAccOpen;
extern drv_io_close     pAccClose;
extern drv_io_control   pAccIOControl;
extern drv_io_read      pAccRead;

extern drv_io_open      pChEncOpen;
extern drv_io_close     pChEncClose;
extern drv_io_control   pChEncIOControl;

extern drv_io_open      pAuxOpen;
extern drv_io_close     pAuxClose;
extern drv_io_control   pAuxIOControl;
extern drv_io_read      pAuxRead;

extern drv_io_open      pAcOutOpen;
extern drv_io_close     pAcOutClose;
extern drv_io_control   pAcOutIOControl;
extern drv_io_write     pAcOutWrite;

extern drv_io_open      pAcInOpen;
extern drv_io_close     pAcInClose;
extern drv_io_control   pAcInIOControl;
extern drv_io_read      pAcInRead;

extern drv_io_open      pAcSrcOpen;
extern drv_io_close     pAcSrcClose;
extern drv_io_control   pAcSrcIOControl;

extern drv_io_open      pAcCnrOpen;
extern drv_io_close     pAcCnrClose;
extern drv_io_control   pAcCnrIOControl;
extern drv_io_read      pAcCnrRead;

#ifdef __cplusplus
}
#endif

#else
#error dispatcher_device.h included several times
#endif
