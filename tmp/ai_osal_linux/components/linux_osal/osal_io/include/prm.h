/************************************************************************
| FILE:         prm.h
| PROJECT:      Platform
| SW-COMPONENT: OSAL IO
|------------------------------------------------------------------------
| DESCRIPTION:  This is the source for the prm
|
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| 09.10.12  | Updated PrmMsg structure   | SWM2KOR
| 20.03.13  | Fix for NIKAI 3163         | SWM2KOR
| 08.12.16  | New PRM Signal             | AHM5KOR
|           | SIGNAL_PARTINFO added.     |
| --.--.--  | ----------------           | -------, -----
|
|************************************************************************
|
|************************************************************************/

#if !defined (PRM_HEADER)
   #define PRM_HEADER

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 
     
#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: global) 
|-----------------------------------------------------------------------*/
// define for recognizing medias data type (navi, mp3, flash)
//#define VARIANT_S_FTR_ENABLE_PRM_MEDIADATATYPE

#define AUTOMOUNT1  "/tmp/.automount"
#define AUTOMOUNT2  "/tmp/.automounter/device_db"

//#ifdef VARIANT_S_FTR_ENABLE_PRM_MEDIADATATYPE
// Settings for flag
#define C_U32_EVENT_START_DATAMEDIARECOG  ( ( tU32 ) 0x00000001 )
//#endif //VARIANT_S_FTR_ENABLE_PRM_MEDIADATATYPE

#define PRM_MEDIARECOG_C_TRACELEVEL1  1 //TR_LEVEL_ERRORS
#define PRM_MEDIARECOG_C_TRACELEVEL2  4 //TR_LEVEL_COMPONENT
#define PRM_MEDIARECOG_C_TRACELEVEL3  8 //TR_LEVEL_USER_4

#define PRM_EXEC_CALLBACK_INFO   0x25
#define PRM_CALLBACK_INFO        0x26

#define C_U16_CMD_TIMEOUT        0x0100
#define C_U16_DEFECT_COM         0x0200
#define C_U16_READ_ERR           0x0400
#define C_U16_TEMP_READ_ERR      0x0800
#define C_U16_DEVICE_READY_BIT   0x1000

#define C_MAX_APPLICATIONS    16
#define C_MAX_RESOURCES       4

/*Macro*/
#define M_SET_BIT(dest,bit)    (dest |= bit)
#define M_CLEAR_BIT(dest, bit) (dest &= (~bit))
#define M_CHECK_BIT(dest, bit) (dest & bit)

#define ERROR_TMO_MS     45000

#define LINUX_PRM_REC_MQ   "LI_PRM_REC_MQ"

#define M_FIRST_ENCLOSES_SECOND(first, second)   (first==(first|(first^second)))
//#define NO_MEDIA_MASK  (OSAL_C_U16_MEDIA_EJECTED | OSAL_C_U16_INCORRECT_MEDIA | OSAL_C_U16_UNKNOWN_MEDIA | OSAL_C_U16_DATA_MEDIA_UNKNOWN)
//#define PRM_M_ELEMENTS(array)  (sizeof(array)/sizeof(array[0]))

#define PRM_TRACE_CLASS    ((tU32)TR_CLASS_DRV_PRM)

#define PRM_TRACE_LEVEL    TR_LEVEL_COMPONENT

/* Defines for Trace output. Must be synchonised with prm_table.trc */
#define C_U8_TRACE_vSetCdErrorToPrc             1
#define C_U8_TRACE_vSendSignal                  2
//#define C_U8_TRACE_vSendSignal_FuncPointer    3
#define C_U8_TRACE_vSendResSignal               4
#define C_U8_TRACE_vRemoveAllExclAccess         5
#define C_U8_TRACE_u32RelExclusiveAccess        6
#define C_U8_TRACE_u32RelExclusiveAccess_exit   7
#define C_U8_TRACE_u32RegisterForNotification   8
#define C_U8_TRACE_u32RegisterForNotification_exit    9
#define C_U8_TRACE_u32UnRegisterForNotification       10
#define C_U8_TRACE_u32UnRegisterForNotification_exit  11
#define C_U8_TRACE_prm_vUpdateStatus            12
//#define C_U8_TRACE_prm_vInit                  13
#define C_U8_TRACE_prm_vCheck                   14
#define C_U8_TRACE_prm_bIsAPrmFun               15
#define C_U8_TRACE_prm_u32Prm                   16
#define C_U8_TRACE_prm_u32Prm_exit              17
#define C_U8_TRACE_prm_u32Prm_error_exit        18
#define C_U8_TRACE_Error                        19
#define C_U8_TRACE_u32GetExclusiveAccess        20
#define C_U8_TRACE_u32GetExclusiveAccess_exit   21
#define C_U8_TRACE_prm_uDev_Msg                 22
#define C_U8_TRACE_prm_usbpwr_Msg               23

#define C_U8_TRACE_register_odrmfi              30
#define C_U8_TRACE_register_SED                 31
#define C_U8_TRACE_unknown_callback             32
#define C_U8_TRACE_prm_callback_execute         33

#ifdef PRM_LAST_MODE
#define C_U8_TRACE_prm_get_last_mode_data       34
#define C_U8_TRACE_prm_set_last_mode_data       35
#endif

/*PRM stata: bit set if exclusive mode*/
// #define C_U32_PRC_CDAUDIO_BIT     0x00000002
// #define C_U32_PRC_CDROM_BIT       0x00000004
// #define C_U32_PRC_CACHEDCDFS_BIT  0x00000008

#define C_U32_PRC_CDCTRL_BIT      0x00000001
#define C_U32_PRC_USBMS_BIT       0x00000002
#define C_U32_PRC_USBMS2_BIT      0x00000004
#define C_U32_PRC_CARD_BIT        0x00000008
#define C_U32_PRC_CRYPTCARD_BIT   0x00000010
#define C_U32_PRC_CRYPTCARD2_BIT  0x00000020
#define C_U32_PRC_CRYPTNAV_BIT    0x00000040
#define C_U32_PRC_DEV_MEDIA_BIT   0x00000080
#define C_U32_PRC_DEV_USBPWR_BIT  0x00000100

/* Physical stata: see ATAPI PRM/PRC for OSAL 5.1*/
/* Bit values for pu8status are located in lower 8 bit of devicestatus */
/* Start of status bits for pu8Status. With first 8 bits no changes are possible! */
#define C_U8_MEDIA_INSIDE_BIT      0x01
#define C_U8_DEVICE_NOT_OK_BIT     0x02
#define C_U8_MEDIA_READY_BIT       0x04
#define C_U8_EXCLUSIVE_MODE_BIT    0x08
#define C_U8_HIGH_TEMPERATURE_BIT  0x10

#ifdef PRM_REPORT_DISC_FAILURES
#define C_U8_TOC_AVAIL_BIT         0x20
#define C_U8_DISC_ERROR_TOC_BIT    0x40
#define C_U8_DISC_ERROR_BIT        0x80
#endif
/* End of status bits for pu8Status. With first 8 bits no changes are possible! */

#define C_U16_CMD_TIMEOUT          0x0100
#define C_U16_DEFECT_COM           0x0200
#define C_U16_READ_ERR             0x0400
#define C_U16_TEMP_READ_ERR        0x0800
#define C_U16_DEVICE_READY_BIT     0x1000


#define SDC_DEFAULT_ACCESS   OSAL_EN_READWRITE
#define USB_DEFAULT_ACCESS   OSAL_EN_READWRITE


/* CD_MEDIA_STATE class */
#define CDDVD_READY                       0x00000001
#define CDDVD_NOT_READY                   0x00000002
#define CDDVD_ALMOST_READY                0x00000004

/* CDDVD_MEDIA_CHANGE class */
#define CDDVD_MEDIA_EJECTED               0x00000008
#define CDDVD_MEDIA_INCORRECT             0x00000010
#define CDDVD_MEDIA_DATA                  0x00000020
#define CDDVD_MEDIA_AUDIO                 0x00000040
#define CDDVD_MEDIA_UNKNOWN               0x00000080

/* CDVD_MEDIA_READY class */
#define CDDVD_DEVICE_READY                0x00000100
#define CDDVD_DEVICE_NOT_READY            0x00000200

/* CDDVD_TOTAL_FAILURE class */
#define CDDVD_DEVICE_OK                   0x00000400
#define CDDVD_DEVICE_FAIL                 0x00000800

/* CDDVD_LOW_VOLTAGE class */
#define CDDVD_LOW_VOLTAGE                 0x00001000
#define CDDVD_NRM_VOLTAGE                 0x00002000

/* CDDVD_OVR_TEMP class */
#define CDDVD_OVR_TEMP                    0x00004000
#define CDDVD_NRM_TEMP                    0x00008000

/* CDDVD_DEFECT class */
#define CDDVD_COM_FAIL                    0x00010000
#define CDDVD_LOADING_FAIL                0x00020000
#define CDDVD_READ_ERROR                  0x00040000
#define CDDVD_READ_TIMEOUT                0x00080000
#define CDDVD_ABNORMAL_WRITE_TERMINATION  0x00180000

/* Eject key notification */
#define CDDVD_EJECTKEYDOWN                0x00100000
#define CDDVD_EJECTKEYUP                  0x00200000

#define CDDVD_COM_OK                      0x00400000
#define CDDVD_READ_OK                     0x00800000
#define CDDVD_TEMP_ERR                    0x01000000
#define CDDVD_CMD_TIMEOUT_OK              0x02000000
#define CDDVD_CMD_TIMEOUT                 0x04000000

/* Play info notification */
#define CDDVD_PLAY_INFO                   0x08000000


/*used by TNCD*/
#define CDDVD_NOTIFY_END_REACHED          0x10000000
#define CDDVD_NOTIFY_HAVE_MP3             0x20000000
#define CDDVD_NOTIFY_ID3_AVAIL            0x40000000
#define CDDVD_NOTIFY_QUIT                 0x80000000
//#define CDDVD_RESERVED                  0x80000006



/* Temperatures */
#define CDDVD_50_DEGREE_TEMPERATURE    0x32
#define CDDVD_BELOW_50_RETURN_VALUE    0x00
#define CDDVD_90_DEGREE_TEMPERATURE    0x5a
#define CDDVD_ABOVE_90_RETURN_VALUE    0x5b
#define CDDVD_UNKNOWN_TEMPERATURE       156      // -100  Grad:256 - 100 = 156


#define CID_LEN         (16)
#define SIGN_VERIFICATION_SUCCESS 0x00000100
#define SIGN_VERIFICATION_FAILURE 0x00000010

/************************************************************************ 
|typedefs and struct defs (scope: global) 
|-----------------------------------------------------------------------*/

typedef struct PrmMsg
{
   tS32  s32ResID;
   tU32  u32EvSignal;
   tU32  u32Val;
   tU16  u16MediaType;
   tU16  U16Reserved; // to make a structure word alligned
   tU8   au8CertPath[32];
   tU8   au8UUID[PRM_SIZE_OF_UUID_MSG];   // uuid string of device
   
}trPrmMsg;

typedef struct 
{
    tU32 PortMask;
    tU32 ValueMask;
}usb_port_control;

typedef enum {
   RES_ID_INVALID,
   RES_ID_SD_CARD,
   RES_ID_VOLTAGE,
   RES_ID_DEVMEDIA,
   RES_ID_SYSINFO,
   RES_ID_TRIGGER_OVC_STATUS,
   RES_ID_USB_PORT_STATE,
   RES_ID_USB_PORT_POWER,
   RES_ID_LAST
}ePrmResId;

typedef enum {
   SIGNAL_INVALID,
   SIGNAL_VOLTAGE,
   SIGNAL_MOUNT,
   SIGNAL_UNMOUNT,
   SIGNAL_SD_CARD,
   SIGNAL_SYSINFO,
   SIGNAL_SYS_STATINFO,
   SIGNAL_SD_ACTIVATE,
   SIGNAL_REMOUNT,
   SIGNAL_PARTINFO,
   SIGNAL_LAST
}ePrmSignal;

typedef enum {
   MEDIUM_SD,
   MEDIUM_CRYPTNAV,
   MEDIUM_DEVMEDIA,
   MEDIUM_USBPWR,
   MEDIUM_LAST,
}MediumType;


#define PRM_SYS_NOTIFY      MEDIUM_LAST
#define PRM_SYSSTAT_NOTIFY  MEDIUM_LAST+1


typedef enum {
   MEDIUM_EJECTED,
   MEDIUM_INSERTED,
   MEDIUM_UNKNOWN
}MediumState;

/*Data types for exclusive access management*/
typedef struct ExAccList
{
   tU16                u16AppID;
   struct ExAccList    *pNext;
}trExAccList;


typedef struct PrcCtrlTabEntry
{
   tChar                device_name[ OSAL_C_U32_MAX_PATHLENGTH ];
   OSAL_tenFSDevID      enDriveID;
   tU32                 u32PrcBit;
   trExAccList          *prExAccListRoot;
}trPrcCtrlTabEntry;


/*Data types for callbacks management*/
typedef struct CallbackTabEntry
{
   OSALCALLBACKFUNC     cbCryptCardFun1;
   OSALCALLBACKFUNC     cbCryptnavFun;
   OSALCALLBACKFUNCEXT2 cbDevMediaFun;
   OSALCALLBACKFUNCEXT2 cbUsbPwrFun;

   tU16              u16NotifyRegCryptCard; // Holdes Cryptcard  Notification types
   tU32              u32ResourceMaskCryptCard;
   tU16              u16NotifyRegCryptnav;
   tU32              u32ResourceMaskCryptnav;
   tU16              u16NotifyRegDevMedia;
   tU32              u32ResourceMaskDevMedia;
   tU16              u16NotifyRegUsbPwr;
   tU32              u32ResourceMaskUsbPwr;
  
   tS32              hCallerTaskID;
   tS32              hCallerProcID;

}trCallbackTabEntry;



typedef enum
{
   PRM_EN_PRCID_CRYPTCARD,
   PRM_EN_PRCID_CRYPTNAV,
   PRM_EN_PRCID_DEV_MEDIA,
   PRM_EN_PRCID_USBPWR,
   PRM_EN_PRCID_LAST
}tenPrcID;

typedef struct ResourceTableEntry
{
   tCString  ResourceName;
   tU32      u32ResourceMask;
   tenPrcID  aPrcList[ PRM_EN_PRCID_LAST ];
}trResourceTableEntry;


// will be shifted to osal interface
typedef enum
{
   SIG_UNDEF = 0x00,
   SIG_FALSE = 0x01,
   SIG_TRUE  = 0x02
}tenSignalValue;

typedef struct
{
   tU8  u8PortNr;
   tU8  u8OC;
   tU8  u8UV;
   tU8  u8PPON;

   tU32 u32OCStartTime;
   tU32 u32OCEndTime;
   tU32 u32UVStartTime;
   tU32 u32UVEndTime;
   tU32 u32PPONStartTime;
   tU32 u32PPONEndTime;      // 28 bytes
}UsbPortState;


/************************************************************************ 
| variable declaration (scope: global) 
|-----------------------------------------------------------------------*/
extern tU32 u32PrmUSBPortCount;

/************************************************************************ 
|function prototypes (scope: global) 
|-----------------------------------------------------------------------*/

// extern tBool bPrmInitMediaRecog(tVoid);
extern tVoid prm_vUpdateStatus(tU32 signals);
extern tVoid prm_vInit(tVoid);
extern tVoid prm_vCheck(tVoid);
extern tBool prm_bIsAPrmFun(tS32);
extern void  vPrmTtfisTrace(tU8 u8Level, const char *pcFormat, ...);
extern void prm_InitLibUsbConnectionData( void );
extern OSAL_tThreadID prm_CreateLibUsbConnectionTask( void );
extern void prm_vDeleteLibUsbConnectionTask( OSAL_tThreadID ThId );
extern tU32 Prm_GetUsbPortState(UsbPortState* rPortState );
extern tU32 prmGetPRMUSBPortCount(tS32 s32arg);
#if !defined (LSIM) && !defined (OSAL_GEN3_SIM)
	extern tU32 prmUSBPower_control(tS32 arg);
#endif

#ifdef __cplusplus
}
#endif

#else
#error prm.h included several times
#endif 
