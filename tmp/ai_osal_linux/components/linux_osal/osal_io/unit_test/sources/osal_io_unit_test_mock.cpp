/*****************************************************************************
| FILE:         osal_io_unit_test_mock.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL IO UNIT TEST
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the mocking functions for osal io unit test
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | Carsten Resch
| 31.03.15  | Ported to osalio unit test | SWM2KOR
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "osal_io_unit_test_mock.h"


using namespace std;


OsalIOUnitTestMock osal_io_mock; 


extern "C" {


static OSAL_IO* g_pOSAL_IO = NULL;


void OSAL_IO::SetUp(void)
{

}

void OSAL_IO::TearDown(void)
{
}

tS32 main(int argc, char** argv) 
{
   g_pOSAL_IO = new OSAL_IO;

   if(g_pOSAL_IO)
   {
      ::testing::InitGoogleTest(&argc, argv);
 
      ::testing::AddGlobalTestEnvironment(g_pOSAL_IO);

      int retVal = RUN_ALL_TESTS();
   }
   OSAL_vProcessExit();
}

} // extern "C"

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
