/*****************************************************************************
| FILE:         osal_io_unit_test_helper.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL IO UNIT TEST
|-----------------------------------------------------------------------------
| DESCRIPTION:  For osal io unit test
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 30.03.15  | Initial revision           | SWM2KOR
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "osal_io_unit_test_mock.h"

using namespace std;


/*****************************************************************************
* FUNCTION     :  OSALIO_UnitTest_HelperPrintf( )
* PARAMETER    :  Device name
* RETURNVALUE  :  
* DESCRIPTION  :  UNIT Test Trace function to trace unit test traces
* HISTORY      :  Ported from oedt SWM2KOR on Mar 30, 2015
*                     
******************************************************************************/
tVoid OSALIO_UnitTest_HelperPrintf(tU8 u8_trace_level,const char* cBuffer,...)
{
  tU16 u16Len;
  char cBuf[OSAL_C_U32_MAX_PATHLENGTH];
  OSAL_tVarArgList argList; /*lint -e530 */
  cBuf[0]=OSAL_STRING_OUT;
  OSAL_VarArgStart(argList, cBuffer);  //lint !e1055 !e64 !e516 !e530 !e534 !e416 !e662 !e1773  
  u16Len = OSALUTIL_s32SaveVarNPrintFormat(&cBuf[1], OSAL_C_U32_MAX_PATHLENGTH-1, cBuffer, argList); //lint !e530
  OSAL_VarArgEnd(argList);
  LLD_vTrace(TR_COMP_OSALTEST, u8_trace_level,cBuf,u16Len+1);
}

  
  
  
  
  