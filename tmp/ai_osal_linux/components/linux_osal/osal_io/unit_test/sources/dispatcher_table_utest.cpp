/*****************************************************************************
| FILE:         Osfile_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL IO
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for Dispatcher_table.c
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 01.04.16  | Compiler warning fix       | VEW5KOR 
|              for CFG3-1772 
| 31.07.15  | Initial revision           | Amit Bhardwaj
| 07.10.16  | fix for CFG3-2047 Unittest | vew5kor
|           |  OSAL IO tests are Blocked |
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif
#include "gmock/gmock.h"

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"
#include "osfile.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "dispatcher_utest.h"

#define MAX_BYTES 200
#define DEV_ROOT "/dev/root"
#define SUBDIR_NAME  "NewDir"
#define SIZE 250
#define OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(x) (void)(x);

#ifndef OSAL_C_STRING_DEVICE_FFS4
#define OSAL_C_STRING_DEVICE_FFS4  "/dev/ffs4"
#endif 

class DispatchertableTest : public ::testing::Test
{
   protected:
   DispatchertableTest() 
   {
      mkdir("/var/opt/bosch/Test", S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO );
      OSAL_IOCreate("/dev/ffs/abc.txt" , OSAL_EN_READWRITE);
	  #ifndef GEN3ARM
	  OSAL_IOCreate("/dev/root/abc.txt" , OSAL_EN_READWRITE);
	  #endif
   }
   virtual ~DispatchertableTest() 
   {
   }

};

class Dispatcher_helper_test: public testing::TestWithParam<tCString>
{
   virtual void SetUp(void)
   {}
   virtual void TearDown(void)
   {}
};
class dispatcher_helper_positive_test: public Dispatcher_helper_test{};
class dispatcher_helper_negative_test_with_invalid_access: public Dispatcher_helper_test{};
class dispatcher_helper_negative_test_with_invalid_name: public Dispatcher_helper_test{};
class dispatcher_helper_device_openclose_positive_test: public Dispatcher_helper_test{};

#ifndef GEN3ARM
INSTANTIATE_TEST_CASE_P(InstantiationName, dispatcher_helper_positive_test, testing::Values(DEV_ROOT"/abc.txt" ,OSAL_C_STRING_DEVICE_FFS"/abc.txt",OSAL_C_STRING_DEVICE_FFS2"/abc.txt", OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_C_STRING_DEVICE_FFS4"/abc.txt"));
#else
INSTANTIATE_TEST_CASE_P(InstantiationName, dispatcher_helper_positive_test, testing::Values(OSAL_C_STRING_DEVICE_FFS"/abc.txt",OSAL_C_STRING_DEVICE_FFS2"/abc.txt", OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_C_STRING_DEVICE_FFS4"/abc.txt"));
#endif
INSTANTIATE_TEST_CASE_P(InstantiationName, dispatcher_helper_negative_test_with_invalid_access, testing::Values(DEV_ROOT"/abc.txt"));

INSTANTIATE_TEST_CASE_P(InstantiationName, dispatcher_helper_negative_test_with_invalid_name, testing::Values(DEV_ROOT"/xyz.txt",OSAL_C_STRING_DEVICE_FFS"/xyz.txt",OSAL_C_STRING_DEVICE_FFS2"/xyz.txt", OSAL_C_STRING_DEVICE_FFS3"/xyz.txt", OSAL_C_STRING_DEVICE_FFS4"/xyz.txt"));

INSTANTIATE_TEST_CASE_P(InstantiationName, dispatcher_helper_device_openclose_positive_test, testing::Values(DEV_ROOT ,OSAL_C_STRING_DEVICE_FFS ,OSAL_C_STRING_DEVICE_FFS2 , OSAL_C_STRING_DEVICE_FFS3 , OSAL_C_STRING_DEVICE_FFS4 ));

/********************* OSAL_IOOpenCloseFile_positiveTest *********************************** */

TEST_F(DispatchertableTest, Osal_double_open_tests)
{
   OSAL_tIODescriptor fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR, fd);	
    /* Try to open an already opened file*/
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR, fd);
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(fd));
}
TEST_P(dispatcher_helper_positive_test, Openclosefilepositivetest)
{
   OSAL_tIODescriptor fd =  OSAL_IOOpen(GetParam(), OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR, fd);
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(fd));
}

/********************* OSAL_s32IOOpenClosedevice_PositiveTest *********************************** */

TEST_P(dispatcher_helper_device_openclose_positive_test , OSAL_s32IOOpenCloseDevice_PositiveTest )
{
   OSAL_tIODescriptor fd = OSAL_IOOpen( GetParam(), OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR,fd); 
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(fd));
}

/********************* OSAL_s32IOOpenFile_NegativeTest_invalid_access *********************************** */
#ifndef GEN3ARM
TEST_P(dispatcher_helper_negative_test_with_invalid_access , OSAL_s32IOOpenFile_NegativeTest_with_invalid_access )
{
   OSAL_tIODescriptor fd ;
   /* /dev/root and /dev/FFS3 can be opened only in READ_ONLY mode */
   fd = OSAL_IOOpen(GetParam(), OSAL_EN_READWRITE);
   EXPECT_EQ(OSAL_ERROR,fd);
   EXPECT_NE(OSAL_OK, OSAL_s32IOClose(fd));
   /* Open with Invalid Access should return error */
   fd = OSAL_IOOpen(GetParam(), (OSAL_tenAccess)0);
   EXPECT_EQ(OSAL_ERROR,fd);
   EXPECT_NE(OSAL_OK, OSAL_s32IOClose(fd));
 
}
#endif
/********************* OSAL_s32IOOpenCloseFile_NegativeTest_invalidname *********************************** */

TEST_P(dispatcher_helper_negative_test_with_invalid_name , OSAL_s32IOOpenFile_NegativeTest_with_invalid_name )
{
   OSAL_tIODescriptor fd ;
   /* Invalid path should return error */
   fd = OSAL_IOOpen(GetParam(), OSAL_EN_READONLY);
   EXPECT_EQ(OSAL_ERROR,fd);
   EXPECT_NE(OSAL_OK, OSAL_s32IOClose(fd));  
}

/********************* OSAL_s32IOOpenClosedevice_NegativeTest *********************************** */

TEST_F(DispatchertableTest , OSAL_s32IOOpenCloseDevice_NegativeTest )
{
   OSAL_tIODescriptor fd ; 
   /* Open with Invalid device name will return error */
   fd = OSAL_IOOpen("DEV_ABC", OSAL_EN_READONLY);
   EXPECT_EQ(OSAL_ERROR,fd);
   /* Try to call open without device name */
   fd = OSAL_IOOpen("", OSAL_EN_READONLY);
   EXPECT_EQ(OSAL_ERROR,fd);
}

/********************* OSAL_s32IOWriteTest *********************************** */

TEST_F(DispatchertableTest , OSAL_s32IOWriteTest )
{
   OSAL_tIODescriptor fd;
   tS32 u32errorcode; 
   signed char* buffer = (signed char*)malloc(SIZE);
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, fd);
   u32errorcode = OSAL_s32IOWrite(fd, buffer, MAX_BYTES);
   EXPECT_EQ(MAX_BYTES, u32errorcode);
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(fd));
   free(buffer);

}
/********************* OSAL_s32IOReadTest *********************************** */

TEST_F(DispatchertableTest , OSAL_s32IOReadTest )
{
   OSAL_tIODescriptor fd;
   tS32 u32errorcode = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(u32errorcode);
   signed char* buffer = (signed char*)malloc(SIZE);
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, fd);  
   u32errorcode = OSAL_s32IORead(fd, buffer, (tU32)MAX_BYTES);
   EXPECT_EQ(OSAL_OK , u32errorcode);
   /* Check for the function TraceIOString */
   TraceIOString("TraceIOString\n");
   u32errorcode = OSAL_s32IOClose(fd);
   EXPECT_EQ(OSAL_OK, u32errorcode);
   free(buffer);
}

/* ********************OSAL_s32IOControlTest*********************************** */

/* Test the IOControl OSAL_C_S32_IOCTRL_FIOWHERE to get position from beginning of file */
TEST_F(DispatchertableTest, OSAL_s32IOControlGetPositionOfFileTest)
{
   OSAL_tIODescriptor hFile ;
   tS32 s32FilePos = 0;
   tS32 u32errorcode;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(s32FilePos);
   /* Open the file */
   hFile = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hFile); 
   EXPECT_EQ(OSAL_OK , OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOWHERE,(intptr_t)&s32FilePos ));
   u32errorcode = OSAL_s32IOClose(hFile);
   EXPECT_EQ(OSAL_OK, u32errorcode);
   hFile = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hFile);
   EXPECT_NE(OSAL_OK , OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOWHERE,0 ));
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hFile));
}

/* Test the IOControl OSAL_C_S32_IOCTRL_FIOSEEK */
TEST_F(DispatchertableTest, OSAL_s32IOControlSetPosTestPathTest)
{
   OSAL_tIODescriptor hFile;
   tS32 u32errorcode ;
   tS32 s32Postoset = 10;
   /* Open the file */
   hFile = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hFile);
   u32errorcode = OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOSEEK,(intptr_t)s32Postoset );
   EXPECT_EQ(OSAL_OK , u32errorcode);
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hFile));
}

/* Test the IOControl OSAL_C_S32_IOCTRL_FIOTOTALSIZE */
TEST_F(DispatchertableTest, OSAL_s32IOControlTestTotalFileSize)
{
   OSAL_tIODescriptor hFile;
   OSAL_trIOCtrlDeviceSize rSize = {0};
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(rSize);
   /* Open the file */
   hFile = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hFile); 
   rSize.u32High = 0;
   rSize.u32Low  = 0;
   EXPECT_EQ(OSAL_OK , OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOTOTALSIZE,( intptr_t )& rSize ));
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hFile));
}

/* Test the IOControl OSAL_C_S32_IOCTRL_FIOFREESIZE */
TEST_F(DispatchertableTest, OSAL_s32IOControlTestFileFreeSize)
{
   OSAL_tIODescriptor hFile ;
   OSAL_trIOCtrlDeviceSize rSize = {0};
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(rSize);
   /* Open the file */
   hFile = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hFile); 
   rSize.u32High = 0;
   rSize.u32Low  = 0;
   EXPECT_EQ(OSAL_OK , OSAL_s32IOControl ( hFile, OSAL_C_S32_IOCTRL_FIOFREESIZE,( intptr_t ) & rSize ));
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hFile));
}

/* Test the IOControl OSAL_C_S32_IOCTRL_FIONREAD */
TEST_F(DispatchertableTest, OSAL_s32IOControlTestFionread)
{
   OSAL_tIODescriptor hFile;
   tS32 s32FilePos = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(s32FilePos);
   /* Open the file */
   hFile = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/abc.txt", OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hFile);
   EXPECT_EQ(OSAL_OK , OSAL_s32IOControl ( hFile,OSAL_C_S32_IOCTRL_FIONREAD ,(intptr_t)&s32FilePos ));
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hFile));
}

/* Test the IOControl OSAL_C_S32_IOCTRL_FIOREADDIR */
TEST_F(DispatchertableTest, OSAL_s32IOControlFIOReaddirTest)
{
   OSAL_tIODescriptor hDir1;
   OSAL_trIOCtrlDir sDirCtrl = {0};
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(sDirCtrl);
   hDir1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3 , OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hDir1);
   sDirCtrl.fd        = hDir1;
   sDirCtrl.s32Cookie = 0;
   EXPECT_EQ(OSAL_OK , OSAL_s32IOControl ( hDir1, OSAL_C_S32_IOCTRL_FIOREADDIR, (intptr_t)&sDirCtrl) );
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hDir1));
}

 /* Test the IOControl OSAL_C_S32_IOCTRL_FIOMKDIR */
TEST_F(DispatchertableTest, OSAL_s32IOControlFIOMakedirTest)
{
   OSAL_tIODescriptor hDir1;
   OSAL_trIOCtrlDirent rDirent = {""};
   ( tVoid )OSAL_szStringCopy ( ( tString )( rDirent.s8Name ),( tCString )(SUBDIR_NAME) );
   hDir1 = OSAL_IOOpen("/dev/ffs2/" , OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hDir1);
   EXPECT_EQ(OSAL_OK , OSAL_s32IOControl( hDir1, OSAL_C_S32_IOCTRL_FIOMKDIR ,(intptr_t)&rDirent) );
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hDir1));    
}

 /* Test the IOControl OSAL_C_S32_IOCTRL_FIORMDIR */
TEST_F(DispatchertableTest, OSAL_s32IOControlFIORemovedirTest)
{
   OSAL_tIODescriptor hDir1;
   OSAL_trIOCtrlDirent rDirent = {""};
   ( tVoid )OSAL_szStringCopy ( ( tString )( rDirent.s8Name ),( tCString )(SUBDIR_NAME) );
   hDir1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_FFS2"/" , OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hDir1);
   EXPECT_EQ(OSAL_OK , OSAL_s32IOControl( hDir1, OSAL_C_S32_IOCTRL_FIORMDIR ,(intptr_t)&rDirent) );
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hDir1));
}

/* Test the IOControl OSAL_C_S32_IOCTRL_FIORENAME */
TEST_F(DispatchertableTest, OSAL_s32IOControlRenameFilePathTest)
{
   OSAL_tIODescriptor hFile;
   OSAL_tIODescriptor hDir1;
   tChar* arrFileName[2] = {OSAL_NULL};
   tChar szFilePath [50] = {'\0'};
   tChar szReFilePath [50] = {'\0'};
   /*File path name*/
   ( tVoid )OSAL_szStringCopy( szFilePath, OSAL_C_STRING_DEVICE_FFS2 );
   ( tVoid )OSAL_szStringConcat( szFilePath, "/File1.txt" );
   /*Renamed File path */
   ( tVoid )OSAL_szStringCopy( szReFilePath, OSAL_C_STRING_DEVICE_FFS2 );
   ( tVoid )OSAL_szStringConcat( szReFilePath, "/File2.txt" );
   arrFileName[0] = const_cast<tChar*>("File1.txt");      /* old name*/
   arrFileName[1] = const_cast<tChar*>("File2.txt");     /* new name */
   hFile = OSAL_IOCreate ( ( tCString )szFilePath, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   hDir1 = OSAL_IOOpen("/dev/ffs2/" , OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, hDir1);      
   EXPECT_EQ(OSAL_OK, OSAL_s32IOControl( hDir1, OSAL_C_S32_IOCTRL_FIORENAME, ( intptr_t )arrFileName));
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hDir1));
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(hFile));
}

/* Test the IOControl OSAL_C_S32_IOCTRL_FIOCOPYDIR */
TEST_F(DispatchertableTest, OSAL_s32IOControlCopyDirTest)
{  
   OSAL_tIODescriptor fd;
   tChar* arg[2]={OSAL_NULL};
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS, OSAL_EN_READWRITE);
   /* Create source and destination directories */
   EXPECT_NE(OSAL_ERROR, OSALUTIL_s32CreateDir(fd, "Source_Dir"));
   EXPECT_NE(OSAL_ERROR, OSALUTIL_s32CreateDir(fd, "Destination_Dir"));
   /*Create files in the source directory */
   EXPECT_NE(OSAL_ERROR, OSAL_IOCreate("/dev/ffs/Source_Dir/File1.txt" , OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR, OSAL_IOCreate("/dev/ffs/Source_Dir/File2.txt" , OSAL_EN_READWRITE));
   arg[0] =  const_cast<tChar*>(OSAL_C_STRING_DEVICE_FFS"/Source_Dir") ;
   arg[1] =  const_cast<tChar*>(OSAL_C_STRING_DEVICE_FFS"/Destination_Dir") ;
   EXPECT_NE( OSAL_ERROR ,OSAL_s32IOControl(fd ,OSAL_C_S32_IOCTRL_FIOCOPYDIR, (intptr_t)arg));
   EXPECT_NE( OSAL_ERROR, OSAL_s32IORemove("/dev/ffs/Source_Dir/File1.txt"));
   EXPECT_NE( OSAL_ERROR, OSAL_s32IORemove("/dev/ffs/Source_Dir/File2.txt"));
   EXPECT_NE( OSAL_ERROR, OSAL_s32IORemove("/dev/ffs/Destination_Dir/File1.txt"));
   EXPECT_NE( OSAL_ERROR, OSAL_s32IORemove("/dev/ffs/Destination_Dir/File2.txt"));
   EXPECT_NE( OSAL_ERROR, OSALUTIL_s32RemoveDir(fd, "Source_Dir"));
   EXPECT_NE( OSAL_ERROR, OSALUTIL_s32RemoveDir(fd, "Destination_Dir"));   
}

/* Test the IOControl OSAL_C_S32_IOCTRL_FIOGET_REAL_PATH */
TEST_F(DispatchertableTest , OSAL_s32IOControlFIOGetRealPath)
{
   OSAL_tIODescriptor fd ;
   char path[320] = {0};
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(path);
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS, OSAL_EN_READWRITE);
   EXPECT_EQ(OSAL_OK, OSAL_s32IOControl( fd, OSAL_C_S32_IOCTRL_FIOGET_REAL_PATH, (intptr_t)&path[0]));
}

/* Test the IOControl OSAL_C_S32_IOCTRL_FIORMRECURSIVE */
TEST_F(DispatchertableTest , OSAL_s32IOControlRmrecursive)
{
   OSAL_tIODescriptor hDevice ;
   OSAL_tIODescriptor hFile1 ;
   OSAL_tIODescriptor hFile2;
   hDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS2, OSAL_EN_READWRITE);
   /* Create a directory in OSAL_C_STRING_DEVICE_FFS2 */
   EXPECT_NE(OSAL_ERROR, OSALUTIL_s32CreateDir(hDevice, "New_Dir"));
   /*Create few files in the directory */
   EXPECT_NE(OSAL_ERROR, (hFile1 = OSAL_IOCreate("/dev/ffs2/New_Dir/File1.txt", OSAL_EN_READWRITE)));
   EXPECT_NE(OSAL_ERROR, (hFile2 = OSAL_IOCreate("/dev/ffs2/New_Dir/File2.txt", OSAL_EN_READWRITE)));
   /* Create one more directory in the same path */
   EXPECT_NE(OSAL_ERROR, OSALUTIL_s32CreateDir(hDevice, "New_Dir/New_Dir2" ));
   /* Close the opened files */
   EXPECT_EQ(OSAL_OK , OSAL_s32IOClose(hFile1));
   EXPECT_EQ(OSAL_OK , OSAL_s32IOClose(hFile2));
   EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_FIORMRECURSIVE, (intptr_t) "New_Dir" )); 
}

/* IOControl OSAL_C_S32_IOCTRL_FIOREADDIREXT test */
TEST_F(DispatchertableTest , OSAL_s32IOControlReadDirExt)
{
   OSAL_tenAccess enAccess = OSAL_EN_ACCESS_DIR;
   OSAL_tIODescriptor hFile; 
   OSAL_trIOCtrlExtDir s32Stat;
   OSAL_trIOCtrlExtDirent pDir;
   s32Stat.s32Cookie = 0;
   s32Stat.u32NbrOfEntries = 1;            /*No of entries to be read,say 1 */                          
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(s32Stat);   
   pDir.enFileFlags = (OSAL_tenFileFlags)0;
   OSAL_pvMemorySet(pDir.s8Name,'\0',sizeof(pDir.s8Name));   
   pDir.u32FileSize = 0;
   s32Stat.pDirent = &pDir;
   hFile = OSAL_IOOpen ("/dev/ffs2/",enAccess );
   EXPECT_NE(OSAL_ERROR, hFile);
   EXPECT_EQ( OSAL_OK , OSAL_s32IOControl(hFile, OSAL_C_S32_IOCTRL_FIOREADDIREXT, (intptr_t)&s32Stat));
   EXPECT_EQ(OSAL_OK , OSAL_s32IOClose(hFile));
}

/* Trace device open and close test  */
TEST_F(DispatchertableTest , Openandclosedevtrace)
{
   OSAL_tIODescriptor fd ;
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE, OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR, fd);
   EXPECT_NE(OSAL_ERROR, OSAL_s32IOClose (fd));
}

/* Test to read from the Trace device */
TEST_F(DispatchertableTest, traceDevicereadtest)
{
   OSAL_tIODescriptor fd ;
   tS32 u32errorcode;
   signed char* buffer = (signed char*)malloc(SIZE);
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE, OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, fd);  
   u32errorcode = OSAL_s32IORead(fd, buffer, MAX_BYTES);
   EXPECT_EQ(OSAL_ERROR, u32errorcode);
   TraceIOString("Dev Trace Read Test\n");
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(fd));
   free(buffer);
}

/* Test to write to the Trace device */
TEST_F(DispatchertableTest , traceDevicewritetest )
{
   OSAL_tIODescriptor fd;
   signed char* buffer = (signed char*)malloc(250);
   fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_TRACE, OSAL_EN_READWRITE);
   EXPECT_NE(OSAL_ERROR, fd);
   OSAL_s32IOWrite(fd, buffer, MAX_BYTES);
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(fd));
   free(buffer);
}

/* Test the Trace device open,close,remove and create functions */
TEST_F(DispatchertableTest , traceDeviceunusedFunctest )
{
   tS32 s32devid = 0;
   EXPECT_EQ(OSAL_E_NOERROR, TRACE_s32IOCreate(s32devid));
   EXPECT_EQ(OSAL_E_NOERROR, TRACE_s32IOOpen());
   EXPECT_EQ(OSAL_E_NOERROR, TRACE_s32IORemove(s32devid));
   EXPECT_EQ(OSAL_E_NOERROR, TRACE_s32IOClose());
}

/* Test the read operation on proc file system */
TEST_F(DispatchertableTest , ProcFileSystemReadtest )
{
   OSAL_tIODescriptor fd;
   tS32 u32errorcode = 0;
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSE(u32errorcode);
   signed char* buffer = (signed char*)malloc(SIZE);
   fd = OSAL_IOOpen("/dev/root/proc/filesystems", OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR, fd);  
   u32errorcode = OSAL_s32IORead(fd, buffer, (tU32)MAX_BYTES);
   EXPECT_NE(OSAL_ERROR , u32errorcode);
   u32errorcode = OSAL_s32IORead(fd, buffer, 10);
   EXPECT_NE(OSAL_ERROR , u32errorcode);
   u32errorcode = OSAL_s32IOClose(fd);
   EXPECT_EQ(OSAL_OK, u32errorcode);
   free(buffer);
}
/* EOF */

