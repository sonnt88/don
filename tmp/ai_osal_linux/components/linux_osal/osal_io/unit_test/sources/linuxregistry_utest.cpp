/*****************************************************************************
| FILE:         linuxregistry_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL IO
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for linux registry implementation
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2015 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 02.02.16  | Implementation of linuxregistry unittest | Vinutha Eshwar Shantha
| 03.01.15  | Initial revision           | Klaus-Hinrich Meyer
| 07.10.16  | fix for CFG3-2047 Unittest | vew5kor
|           |  OSAL IO tests are Blocked |
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "osal_io_unit_test_mock.h"
#include "linuxregistry_utest.h"

using namespace std;

#define REGISTRY_VERSION 0x00000100
#define MAXVALUE 12

#define OSAL_C_STRING_DEVICE_REGISTRY_KEY          "/dev/registry/Test"           //RBIN
#define OSAL_C_STRING_DEVICE_REGISTRY_KEY1          "/dev/registry/Test1"           //RBIN
#define OEDT_REGISTRY_INVALID_ACCESS_MODE                            25                      //RBIN
#define REG_INVAL_PARAM                             OSAL_C_INVALID_HANDLE            /* dont use any number , it must be OSAL_C_... */

extern tVoid OSALIO_UnitTest_HelperPrintf(tU8 u8_trace_level,const char* cBuffer,...);

class linuxregOSAL_tenAccess_test: public testing::TestWithParam<OSAL_tenAccess>
{
   virtual void SetUp(void)
   {      
   }
   virtual void TearDown(void)
   {
   }
};

class DIFFMODES_READWRITE: public linuxregOSAL_tenAccess_test{};
class DIFFMODES_NOACCESS: public linuxregOSAL_tenAccess_test{};

INSTANTIATE_TEST_CASE_P(dataset2, DIFFMODES_READWRITE, testing::Values(OSAL_EN_READONLY,OSAL_EN_READWRITE,OSAL_EN_WRITEONLY));
INSTANTIATE_TEST_CASE_P(dataset2, DIFFMODES_NOACCESS, testing::Values(OSAL_EN_APPEND,OSAL_EN_TEXT,OSAL_EN_BINARY,OSAL_EN_READSTREAM,OSAL_EN_ACCESS_DEVICE,OSAL_EN_ACCESS_DIR));

/*********************************************************************/
/* Test Cases for OSAL Registry Device                               */
/*********************************************************************/

TEST(OsalIoRegistryTest, u32RegDevOpenClose)
{
   OSAL_tIODescriptor hRegDevice = 0;
   /* Open the device in read-write mode */
   EXPECT_NE(OSAL_ERROR,(hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY ,OSAL_EN_READWRITE)));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}


TEST(OsalIoRegistryTest, u32RegDevOpenCloseDiffModes)
{
   OSAL_tIODescriptor hRegDevice = 0;
   /* Open the device in read-write mode */
   EXPECT_NE(OSAL_ERROR,(hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY ,OSAL_EN_READWRITE)));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
   /* Open the device in read-only mode */
   EXPECT_NE(OSAL_ERROR,(hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY ,OSAL_EN_READONLY)));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
   /* Open the device in read-only mode */
   EXPECT_NE(OSAL_ERROR,(hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY ,OSAL_EN_WRITEONLY)));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}
   
TEST(OsalIoRegistryTest, u32RegDevOpenCloseInvalParam)
{
   OSAL_tIODescriptor hRegDevice = 0;
   EXPECT_EQ(OSAL_ERROR,(hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY ,(OSAL_tenAccess)OEDT_REGISTRY_INVALID_ACCESS_MODE)));
   /* Close device with invalid parameter */
   hRegDevice = (OSAL_tIODescriptor)REG_INVAL_PARAM;
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegCreateKey)
{
   OSAL_tIODescriptor  hRegDevice1 = 0;
   OSAL_tIODescriptor  hRegDevice2 = 0;
   /* Open  the device in readwrite mode */
   EXPECT_NE(OSAL_ERROR,(hRegDevice1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY ,OSAL_EN_READWRITE)));
   /*  Create the Key in readwrite mode*/
   EXPECT_NE(OSAL_ERROR,hRegDevice2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Close the key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice2));
   /* Close the device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice1));
   /*Remove the Key*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
}

TEST(OsalIoRegistryTest, u32RegOpenCloseKey)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey = 0;
   /* Open the registry device */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen (OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /*  Create the Key in readwrite mode*/
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /*Remove the Key*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close registry device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegGetVersion)
{
   OSAL_tIODescriptor hRegDevice = 0;
   uintptr_t s32VersionInfo = 0;
   /* Open the device in readonly mode */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READONLY)); 
   /* Get device version */	
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegDevice,OSAL_C_S32_IOCTRL_VERSION,(uintptr_t)&s32VersionInfo));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegSetGetValue)
{
   OSAL_tIODescriptor 	 hRegDevice1 = 0;
   OSAL_tIODescriptor 	 hRegDevice2 = 0;
   OSAL_trIOCtrlRegistry hReg ;
   tS8   pBuffer[30];
   /* Open the device in readwrite  mode */
   EXPECT_NE(OSAL_ERROR,hRegDevice1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /*Create the Key*/
   EXPECT_NE(OSAL_ERROR,hRegDevice2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /*Set the value */
   hReg.pcos8Name =  (tPCS8)"REGISTRY";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  13;
   hReg.ps8Value  =  (tPU8)"FunctionCheck";
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl( hRegDevice2,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&hReg));
   hReg.pcos8Name =  (tPCS8)"REGISTRY";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  13;
   hReg.ps8Value  =  (tPU8)pBuffer;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl( hRegDevice2,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&hReg));
   /*Set the value */
   hReg.pcos8Name =  (tPCS8)"REGISTRY";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  0;
   hReg.ps8Value  =  NULL;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegDevice2, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (uintptr_t)&hReg));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice2));
   /* Remove the key in the registry*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /*Close the device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice1));
}

TEST(OsalIoRegistryTest, u32RegWriteValue)
{
   OSAL_tIODescriptor 	 hRegDevice = 0;
   OSAL_tIODescriptor hRegKey = 0;
   OSAL_trIOCtrlRegistryValue sValue;

   /* Open the device in readwrite  mode */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY, OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY, OSAL_EN_READWRITE));
   /* Write the value */
   memcpy(sValue.s8Name, (const void*)"REG", 3);
   memcpy(sValue.s8Value, (const void*)"WriteValue", 10);
   sValue.s32Type   =  OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl( hRegKey,OSAL_C_S32_IOCTRL_REGWRITEVALUE, (uintptr_t)&sValue));
 
   memcpy(sValue.s8Name, (const void*)"REG", 3);
   memcpy(sValue.s8Value, (const void*)"0", 0);
   sValue.s32Type   =  OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGDELETEVALUE, (uintptr_t)&sValue));
   /* Close the key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Remove the key in the registry*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /*Close the device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}


TEST(OsalIoRegistryTest, u32RegSetQueryValue)
{
   OSAL_tIODescriptor 	 hRegDevice = 0;
   OSAL_tIODescriptor 	 hRegKey = 0;
   OSAL_trIOCtrlRegistry hReg;
   tS8   pBuffer[30];

   /* Open the device in readwrite  mode */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /*Create the Key*/
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Write the value */
   hReg.pcos8Name =  (tPCS8)"REG1";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  10;
   hReg.ps8Value  =  (tPU8)"WriteCheck";
   /* Set the value*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegKey,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&hReg));
   hReg.pcos8Name =  (tPCS8)"REG1";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  10;
   hReg.ps8Value  =  (tPU8)pBuffer;
   /* Query  the  value*/ 
   hReg.pcos8Name =  (tPCS8)"REG1";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  0;
   hReg.ps8Value  =  NULL;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl( hRegKey,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(uintptr_t)&hReg));
   /* Close key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /*Remove the key in the registry */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /*Close the device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegRemoveValue)
{
   OSAL_tIODescriptor 	 hRegDevice = 0;
   OSAL_tIODescriptor 	 hRegKey = 0;
   OSAL_trIOCtrlRegistry hReg;
	  
   /* Open the device in readwrite  mode */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /*Create the Key*/
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   hReg.pcos8Name =  (tPCS8)"REG123";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  10;
   hReg.ps8Value  =  (tPU8)"WriteCheck";
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegKey,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&hReg));
   hReg.pcos8Name =  (tPCS8)"REG123";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  10;
   hReg.ps8Value  =  NULL;			
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegKey,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(uintptr_t)&hReg));
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOControl(hRegKey,OSAL_C_S32_IOCTRL_REGQUERYVALUE,(uintptr_t)&hReg));
   /* Close the key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Remove the key in the registry*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close Device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegDevReOpen)
{
   OSAL_tIODescriptor hRegDevice1 = 0;	
   OSAL_tIODescriptor hRegDevice2 = 0;
   /* Open Registry Device */
   EXPECT_NE(OSAL_ERROR,hRegDevice1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /* Re-open Registry Device */
   EXPECT_NE(OSAL_ERROR,hRegDevice2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /* Close the re-opened handle */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice2));
   /* Close the device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice1));
}

TEST(OsalIoRegistryTest, u32RegDevCloseAlreadyClosed)
{
   OSAL_tIODescriptor hRegDevice = 0;
   /* Open Registry Device */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /* Close Device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
   /* Close device already closed */
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegCreateMultipleValues)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey = 0;
   OSAL_trIOCtrlRegistry hReg;
   tU32 u32len = 11;
   tS8   pBuffer[30];
   /* Open Registry Device */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /* Create a Key */		
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Set value 1 */
   hReg.pcos8Name =  (tPCS8)"VAL1";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.ps8Value  =  (tPU8)"WriteCheck1";
   hReg.u32Size   =  OSAL_u32StringLength(hReg.ps8Value);
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl( hRegKey,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&hReg));                
   hReg.pcos8Name =  (tPCS8)"VAL1";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  sizeof(pBuffer);
   hReg.ps8Value  =  (tPU8)pBuffer;
   EXPECT_EQ(OSAL_OK, OSAL_s32IOControl(hRegKey,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&hReg));
   EXPECT_EQ(OSAL_OK,OSAL_s32StringNCompare((tS8 *)hReg.ps8Value,(tS8 *)"WriteCheck1",u32len));
   /* Set value 2 */
   hReg.pcos8Name =  (tPCS8)"VAL2";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.ps8Value  =  (tPU8)"WriteCheck2";
   hReg.u32Size   =  OSAL_u32StringLength(hReg.ps8Value);
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl( hRegKey,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&hReg));
   hReg.pcos8Name =  (tPCS8)"VAL2";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  sizeof(pBuffer);
   hReg.ps8Value  =  (tPU8)pBuffer;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl( hRegKey,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&hReg));
   EXPECT_EQ(OSAL_OK,OSAL_s32StringNCompare((tS8 *)hReg.ps8Value,(tS8 *)"WriteCheck2",u32len));
   hReg.pcos8Name =  (tPCS8)"VAL1";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.ps8Value  =  NULL;
   hReg.u32Size   =  0;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegKey,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(uintptr_t)&hReg));
   hReg.pcos8Name =  (tPCS8)"VAL2";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.ps8Value  =  NULL;
   hReg.u32Size   =  0;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(uintptr_t)&hReg));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Remove Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close Registry device*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegCreateMultipleKeys)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey1 = 0;
   OSAL_tIODescriptor hRegKey2 = 0;
   /* Open Registry Device */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /* Create a Key */		
   EXPECT_NE(OSAL_ERROR,hRegKey1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Create another Key */		
   EXPECT_NE(OSAL_ERROR,hRegKey2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY1,OSAL_EN_READWRITE));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey2));
   /* Remove Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY1));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey1));
   /* Remove Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close Registry device*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}


TEST(OsalIoRegistryTest, u32RegReOpenKey)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey1 = 0;
   OSAL_tIODescriptor hRegKey2 = 0;
   /* Open Registry Device */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   /* Create a Key and hence open */		
   EXPECT_NE(OSAL_ERROR,hRegKey1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Re-open key */
   EXPECT_NE(OSAL_ERROR,hRegKey2 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey2));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey1));
   /* Remove Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close Registry device*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegCloseKeyAlreadyClosed)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey = 0;

   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Attempt to close key which is already closed */
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IOClose(hRegKey));
   /* Remove Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close Registry device*/
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}


TEST(OsalIoRegistryTest, u32RegDeleteKeyWithValues)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey = 0;
   OSAL_trIOCtrlRegistry hReg;
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Set value 1 */
   hReg.pcos8Name =  (tPCS8)"VAL1";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.ps8Value  =  (tPU8)"WriteCheck1";
   hReg.u32Size   =  OSAL_u32StringLength(hReg.ps8Value);
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl( hRegKey,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&hReg));
   /* Delete key with valid values */
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /*Set the value */
   hReg.pcos8Name =  (tPCS8)"VAL1";
   hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   hReg.u32Size   =  0;
   hReg.ps8Value  =  NULL;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (uintptr_t)&hReg));
   /* Close key */	
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Delete key with valid values */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close device */	
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegOpenKeyDiffAccessModes)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey = 0;
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Close key opened by default */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Open key in READWRITE mode */
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Close key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Open key in READWRITE mode */
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READONLY));
   /* Close key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Open key in READWRITE mode */
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_WRITEONLY));
   /* Close key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Delete key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close device */	
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegWriteNumericValue)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey = 0;
   OSAL_trIOCtrlRegistry hReg;
   tU8 u8NumData= 12;
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Set value */
   hReg.pcos8Name =  (tPCS8)"VALUE";
   hReg.s32Type   =  OSAL_C_S32_VALUE_S32;
   hReg.ps8Value  =  &u8NumData;
   hReg.u32Size   =  2;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl( hRegKey,OSAL_C_S32_IOCTRL_REGSETVALUE, (uintptr_t)&hReg));                 
   hReg.pcos8Name =  (tPCS8)"VALUE";
   hReg.s32Type   =  OSAL_C_S32_VALUE_S32;
   hReg.u32Size   =  0;
   hReg.ps8Value  =  NULL;
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hRegKey, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (uintptr_t)&hReg));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Delete key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}

TEST(OsalIoRegistryTest, u32RegDelKeyAlreadyRemoved)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey = 0;
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Delete key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Attempt to delete a key already deleted. 
      Diver comment says that, remove will success for no existing Key  */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}


TEST(OsalIoRegistryTest, u32RegDelKeyStructure)
{
   OSAL_tIODescriptor hRegDevice = 0;
   OSAL_tIODescriptor hRegKey = 0;
   OSAL_tIODescriptor hRegKey1 = 0;
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY,OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hRegKey1 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY_KEY"/Test1",OSAL_EN_READWRITE));
   /* Delete key */
   EXPECT_EQ(OSAL_ERROR,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey1));
   /* Delete key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY"/Test1"));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey));
   /* Delete key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close Key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
} 


TEST(OsalIoRegistryTest, u32REGISTRYBasic1Test)
{
   tBool bContinue = TRUE;
   OSAL_tIODescriptor  hFd1;
   OSAL_tIODescriptor  hFd2;
   OSAL_trIOCtrlDir 	  sDirCtrl;

   EXPECT_NE(OSAL_ERROR,hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hFd2 = OSAL_IOCreate("/dev/registry/Bosch", OSAL_EN_READWRITE));
   sDirCtrl.fd        = hFd1;
   sDirCtrl.s32Cookie = 0;

    do
    {
      EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_FIOREADDIR, (uintptr_t)&sDirCtrl));
      if(strncmp((const char*)sDirCtrl.dirent.s8Name, "Bosch", 5) == 0)
      {
         bContinue = FALSE;
      }
    }
    while(bContinue == TRUE);

    EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hFd2));
    EXPECT_EQ(OSAL_OK,OSAL_s32IORemove("/dev/registry/Bosch"));
    EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hFd1));
}


TEST(OsalIoRegistryTest, u32REGISTRYBasic2Test)
{
   tBool bContinue = TRUE;
   OSAL_tIODescriptor    hFd1;
   OSAL_trIOCtrlRegistry sIOCtrlRegistry;
   OSAL_trIOCtrlDir      sValueInfo;
   tS8   pBuffer[30];

   EXPECT_NE(OSAL_ERROR,hFd1 = OSAL_IOCreate("/dev/registry/tralala", OSAL_EN_READWRITE));
   memset(pBuffer, 0, 30);
   sIOCtrlRegistry.pcos8Name = (tPCS8)"Test";
   sIOCtrlRegistry.u32Size   = 26;
   sIOCtrlRegistry.s32Type   = OSAL_C_S32_VALUE_STRING;
   sIOCtrlRegistry.ps8Value  = (tPU8)"abcdefghijklmnopqrstuvwxyz";

   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGSETVALUE, (uintptr_t)&sIOCtrlRegistry));
   sValueInfo.fd        = hFd1;
   sValueInfo.s32Cookie = 0;

   do
   {
        EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGENUMVALUE, (uintptr_t)&sValueInfo));
        if(strncmp((const char*)sValueInfo.dirent.s8Name, "Test", 4) == 0)
        {
          bContinue = FALSE;
        }
   }while((sValueInfo.dirent.s8Name != OSAL_NULL) && (bContinue == TRUE));    

   sIOCtrlRegistry.pcos8Name = (tPS8)"Test";
   sIOCtrlRegistry.u32Size   = 30;
   sIOCtrlRegistry.s32Type   = OSAL_C_S32_VALUE_STRING;
   sIOCtrlRegistry.ps8Value  = (tPU8)pBuffer;

   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGGETVALUE, (uintptr_t)&sIOCtrlRegistry));
   EXPECT_EQ(0,strncmp((const char*)pBuffer, "abcdefghijklmnopqrstuvwxyz", 26));

   sIOCtrlRegistry.pcos8Name = (tPS8)"Test";
   sIOCtrlRegistry.u32Size   = 0;
   sIOCtrlRegistry.s32Type   = OSAL_C_S32_VALUE_STRING;
   sIOCtrlRegistry.ps8Value  = NULL;

   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGREMOVEVALUE, (uintptr_t)&sIOCtrlRegistry));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hFd1));
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove("/dev/registry/tralala"));
}

TEST(OsalIoRegistryTest, u32REGISTRYBasic3Test)
{
   OSAL_tIODescriptor         hFd1;
   OSAL_trIOCtrlRegistryValue sValue;
   tS8                        pBuffer[9];
   EXPECT_NE(OSAL_ERROR,hFd1 = OSAL_IOCreate("/dev/registry/tralala", OSAL_EN_READWRITE));
   sValue.s32Type = OSAL_C_S32_VALUE_STRING;
   memcpy(sValue.s8Name,  (const void*)"blub", 5);
   memcpy(sValue.s8Value, (const void*)"zyxwvuts", 9);
   memset(pBuffer, 0, 9);

   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGWRITEVALUE, (uintptr_t)&sValue));
   sValue.s32Type = OSAL_C_S32_VALUE_STRING;
   memcpy(sValue.s8Name, (const void*)"blub", 5);
   memset(sValue.s8Value, 0, sizeof(sValue.s8Value));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGREADVALUE, (uintptr_t)&sValue));
   EXPECT_EQ(0,strncmp((const char*)sValue.s8Value, "zyxwvuts", 8));
   sValue.s32Type = OSAL_C_S32_VALUE_STRING;
   memcpy(sValue.s8Name, (const void*)"blub", 5);
   memset(sValue.s8Value, 0, sizeof(sValue.s8Value));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGDELETEVALUE, (uintptr_t)&sValue));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hFd1));
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove("/dev/registry/blabla"));
}

TEST(OsalIoRegistryTest, u32RegSearchKey)
{
   tBool bContinue = TRUE;
   OSAL_tIODescriptor  hFd1 = 0;
   OSAL_tIODescriptor  hFd2 = 0;
   OSAL_trIOCtrlDir sDirCtrl = {0};

   EXPECT_NE(OSAL_ERROR,hFd1 = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, OSAL_EN_READWRITE));
   EXPECT_NE(OSAL_ERROR,hFd2 = OSAL_IOCreate(OSAL_C_STRING_DEVICE_REGISTRY"/Bosch", OSAL_EN_READWRITE));
   sDirCtrl.fd = hFd1;
   sDirCtrl.s32Cookie = 0;

    do
    {
         EXPECT_EQ(OSAL_OK,OSAL_s32IOControl(hFd1, OSAL_C_S32_IOCTRL_REGREADDIR, (uintptr_t)&sDirCtrl));
         if(strncmp((const char*)sDirCtrl.dirent.s8Name, "Bosch", 5) == 0)
         {
            bContinue = FALSE;
         }
    }while(bContinue == TRUE);

   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hFd2));
   EXPECT_EQ(OSAL_OK, OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY"/Bosch"));
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hFd1));
}


TEST(OsalIoRegistryTest, u32RegOpenAsDir)
{
   OSAL_tIODescriptor  	hRegDevice = 0;
   OSAL_tIODescriptor  	hRegKey = 0;
   OSAL_trIOCtrlDir*  	pDir = NULL;

   /* Open the device in read-write mode */
   EXPECT_NE(OSAL_ERROR,hRegDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_REGISTRY ,OSAL_EN_READWRITE));
   /* Create the Key in readwrite mode */
   EXPECT_NE(OSAL_ERROR,hRegKey = OSAL_IOCreate( OSAL_C_STRING_DEVICE_REGISTRY_KEY,OSAL_EN_READWRITE));
   /* Open reg like an directory */
   EXPECT_NE(0,(uintptr_t)(pDir = OSALUTIL_prOpenDir(OSAL_C_STRING_DEVICE_REGISTRY_KEY)));
   /* close reg directory */
   EXPECT_EQ(OSAL_OK,OSALUTIL_s32CloseDir(pDir));
   pDir = OSAL_NULL;
   /* Close the key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegKey ));
   /* Remove the key */
   EXPECT_EQ(OSAL_OK,OSAL_s32IORemove(OSAL_C_STRING_DEVICE_REGISTRY_KEY));
   /* Close the device */
   EXPECT_EQ(OSAL_OK,OSAL_s32IOClose(hRegDevice));
}  
     
/****************************************************************************/
/* Test  case  for  s32RegistryConnect */
/****************************************************************************/
TEST(s32RegistryConnecttest, s32RegistryConnecttest_Success)
{  
   EXPECT_EQ(OSAL_OK, s32RegistryConnect_wrapper());
}

/****************************************************************************/
                      /* Test  case  for REG_u32IOClose */
/****************************************************************************/
TEST(REG_u32IOClosetest, REG_u32IOClosetest_AlreadyExistingKeyNameSuccess)
{
   uintptr_t u32Fd =0;

   EXPECT_EQ(OSAL_E_ALREADYEXISTS, REG_u32IOCreate_wrapper("LOCAL_MACHINE",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REG_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_BUSY, REG_u32IORemove_wrapper("LOCAL_MACHINE"));
}

TEST(REG_u32IOClosetest, REG_u32IOClosetest_NonExistingKeyNameSuccess)
{
   uintptr_t u32Fd =0;
   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fd)); 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("ROBERTBOSCH"));
}

/****************************************************************************/
                      /* Test  case  for REGISTRY_u32IOClose */
/****************************************************************************/
TEST(REGISTRY_u32IOClosetest, REGISTRY_u32IOClosetest_AlreadyExistingKeyNameSuccess)
{
   uintptr_t u32Fd =0;
      
   EXPECT_EQ(OSAL_E_ALREADYEXISTS, REGISTRY_u32IOCreate_wrapper("LOCAL_MACHINE",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REGISTRY_u32IOClose_wrapper(u32Fd)); 
   EXPECT_EQ(OSAL_E_BUSY, REGISTRY_u32IORemove_wrapper("LOCAL_MACHINE",NULL));
}

TEST(REGISTRY_u32IOClosetest, REGISTRY_u32IOClosetest_NonExistingKeyNameSuccess)
{
   uintptr_t u32Fd =0;
   
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fd)); 
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("ROBERTBOSCH",NULL));
}

/****************************************************************************/
                      /*Test  case  for REG_u32IORemove */
/****************************************************************************/
TEST(REG_u32IORemovetest, REG_u32IORemovetest_AlreadyExistingKeyNameSuccess)
{  
   EXPECT_EQ(OSAL_E_BUSY, REG_u32IORemove_wrapper("LOCAL_MACHINE"));
}

TEST(REG_u32IORemovetest, REG_u32IORemovetest_NonExistingKeyNameSuccess)
{
   uintptr_t u32Fd =0;
   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fd)); 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("ROBERTBOSCH"));
}

TEST(REG_u32IORemovetest, REG_u32IORemovetest_NULLSuccess)
{
   uintptr_t u32Fd =0;

   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper(" ",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fd)); 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper(" "));
}
   
/*Test for NOTSUPPORTED Removal */
TEST(REG_u32IORemovetest, REG_u32IORemovetest_NotSupported)
{
   EXPECT_EQ(OSAL_E_NOTSUPPORTED, REG_u32IORemove_wrapper(OSAL_NULL));
}

/****************************************************************************/
                      /*Test  cases  for REGISTRY_u32IORemove */
/****************************************************************************/
TEST(REGISTRY_u32IORemovetest, REGISTRY_u32IORemovetest_AlreadyExistingKeyNameSuccess)
{
   uintptr_t u32Fd =0;
      
   EXPECT_EQ(OSAL_E_ALREADYEXISTS, REGISTRY_u32IOCreate_wrapper("LOCAL_MACHINE",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REGISTRY_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_BUSY, REGISTRY_u32IORemove_wrapper("LOCAL_MACHINE",NULL));
}

TEST(REGISTRY_u32IORemovetest, REGISTRY_u32IORemovetest_NonExistingKeyNameSuccess)
{
   uintptr_t u32Fd =0;
   
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fd)); 
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("ROBERTBOSCH",NULL));
}

TEST(REGISTRY_u32IORemovetest, REGISTRY_u32IORemovetest_NULLSuccess)
{
   uintptr_t u32Fd =0;

   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper(" ",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fd)); 
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper(" ",NULL));
}
 
/*Test for NOTSUPPORTED Removal */
TEST(REGISTRY_u32IORemovetest, REGISTRY_u32IORemovetest_NotSupported)
{
   EXPECT_EQ(OSAL_E_NOTSUPPORTED, REGISTRY_u32IORemove_wrapper(OSAL_NULL,NULL));
}

/****************************************************************************/
                     /*Test  cases for REG_u32IOCreate and REGISTRY_u32IOCreate*/
/****************************************************************************/
TEST_P(DIFFMODES_READWRITE, REGnREGISTRY_u32IOCreatetest_DiffModes_ExistingKeyName)
{
   uintptr_t u32Fd = 0;

   /*Test  for key creation in different modes  for already existing key name*/
   EXPECT_EQ(OSAL_E_ALREADYEXISTS, REG_u32IOCreate_wrapper("LOCAL_MACHINE",GetParam(),&u32Fd));
   EXPECT_EQ(OSAL_E_ALREADYEXISTS, REGISTRY_u32IOCreate_wrapper("LOCAL_MACHINE",GetParam(),NULL,&u32Fd));
}    

TEST_P(DIFFMODES_NOACCESS, REGnREGISTRY_u32IOCreatetest_DiffModes_ExistingKeyNameNoaccess)
{
   uintptr_t u32Fd = 0;

   /*Test  for key creation in different modes  for already existing key name*/
   EXPECT_EQ(OSAL_E_NOACCESS, REG_u32IOCreate_wrapper("LOCAL_MACHINE",GetParam(),&u32Fd));
   EXPECT_EQ(OSAL_E_NOACCESS, REGISTRY_u32IOCreate_wrapper("LOCAL_MACHINE",GetParam(),NULL,&u32Fd));
}   

TEST_P(DIFFMODES_READWRITE, REGnREGISTRY_u32IOCreatetest_DiffModes_NonExistingKeyName)
{
   uintptr_t u32Fd = 0;

   /*Test  for key creation in different modes for non-existing key name*/
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("ROBERTBOSCH",GetParam(),&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("ROBERTBOSCH"));

   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("ROBERTBOSCH",GetParam(),NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("ROBERTBOSCH",NULL));
}

TEST_P(DIFFMODES_NOACCESS, REGnREGISTRY_u32IOCreatetest_DiffModes_NonExistingKeyNameNoaccess)
{
   uintptr_t u32Fd = 0;

   /*Test  for key creation in different modes for non-existing key name*/
   EXPECT_EQ(OSAL_E_NOACCESS, REG_u32IOCreate_wrapper("ROBERTBOSCH",GetParam(),&u32Fd));
   EXPECT_EQ(OSAL_E_NOACCESS, REGISTRY_u32IOCreate_wrapper("ROBERTBOSCH",GetParam(),NULL,&u32Fd));

}   

TEST(REG_u32IOCreatetest, REG_u32IOCreatetest_NULL)
{
   uintptr_t u32Fd = 0;

   /*test  for NULL */
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper(" ",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fd)); 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper(" "));
}

TEST(REG_u32IOCreatetest, REG_u32IOCreatetest_DiffModes_DiffKeyName)
{
   uintptr_t u32Fd = 0;
   
   /*test for different key names*/
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("UNITTEST",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("UNITTEST"));
   u32Fd = 0;
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REG_u32IOCreate_wrapper("UNITTEST/REGISTRY",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REG_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("UNITTEST/REGISTRY"));
   u32Fd = 0;
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REG_u32IOCreate_wrapper("UNITTEST/REGISTRY/REGISTRY1",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REG_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("UNITTEST/REGISTRY/REGISTRY1"));
   u32Fd = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("123",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("123"));
   u32Fd = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",OSAL_EN_READONLY,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"));
}

TEST(REGISTRY_u32IOCreatetest, REGISTRY_u32IOCreatetest_NULL)
{
   uintptr_t u32Fd = 0;

   /*test  for NULL */
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper(" ",OSAL_EN_READONLY,NULL,&u32Fd));	
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fd)); 
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper(" ",NULL));
}


TEST(REGISTRY_u32IOCreatetest, REGISTRY_u32IOCreatetest_DiffModes_DiffKeyName)
{
   uintptr_t u32Fd = 0;
   
   /*test for different key names*/
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("UNITTEST",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("UNITTEST",NULL));
   u32Fd = 0;
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REGISTRY_u32IOCreate_wrapper("UNITTEST/REGISTRY",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REGISTRY_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("UNITTEST/REGISTRY",NULL));
   u32Fd = 0;
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REGISTRY_u32IOCreate_wrapper("UNITTEST/REGISTRY/REGISTRY1",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REGISTRY_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("UNITTEST/REGISTRY/REGISTRY1",NULL));
   u32Fd = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("123",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("123",NULL));
   u32Fd = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",OSAL_EN_READONLY,NULL,&u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fd));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",NULL));
}

/****************************************************************************/
                     /*Test  cases for  REG_u32IOOpen and REGISTRY_u32IOOpen*/
/****************************************************************************/
TEST_P( DIFFMODES_NOACCESS,  REGnREGISTRY_u32IOOpentest_DiffModes_ExistingKeyName)
{
   uintptr_t u32Fdro = 0,u32Fdwo = 0,u32Fdrw = 0,u32Fd = 0;

   /*Test  for open key  in different modes  for already existing key name*/
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper("LOCAL_MACHINE",OSAL_EN_READONLY,&u32Fdro));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper("LOCAL_MACHINE",OSAL_EN_WRITEONLY,&u32Fdwo));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper("LOCAL_MACHINE",OSAL_EN_READWRITE,&u32Fdrw));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdro));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdwo));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdrw));
   EXPECT_EQ(OSAL_E_BUSY, REG_u32IORemove_wrapper("LOCAL_MACHINE"));

   EXPECT_EQ(OSAL_E_NOACCESS, REG_u32IOOpen_wrapper("LOCAL_MACHINE",GetParam(),&u32Fd));

   u32Fdro = 0,u32Fdwo = 0,u32Fdrw = 0,u32Fd = 0;
   
   /*Test  for open key  in different modes  for already existing key name*/
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper("LOCAL_MACHINE",OSAL_EN_READONLY,NULL,&u32Fdro));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper("LOCAL_MACHINE",OSAL_EN_WRITEONLY,NULL,&u32Fdwo));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper("LOCAL_MACHINE",OSAL_EN_READWRITE,NULL,&u32Fdrw));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdro));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdwo));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdrw));
   EXPECT_EQ(OSAL_E_BUSY, REGISTRY_u32IORemove_wrapper("LOCAL_MACHINE",NULL));

   EXPECT_EQ(OSAL_E_NOACCESS, REGISTRY_u32IOOpen_wrapper("LOCAL_MACHINE",GetParam(),NULL,&u32Fd));
   
}

TEST_P(DIFFMODES_NOACCESS, REGnREGISTRY_u32IOOpentest_DiffModes_NonExistingKeyName)
{
   uintptr_t u32Fdcr = 0,u32Fdop = 0;

   /*Test  for open  key  in different modes for non-existing key name*/
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("ROBERTBOSCH"));

   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_WRITEONLY,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper("ROBERTBOSCH",OSAL_EN_WRITEONLY,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("ROBERTBOSCH"));

   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READWRITE,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper("ROBERTBOSCH",OSAL_EN_READWRITE,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("ROBERTBOSCH"));

   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOACCESS,REG_u32IOOpen_wrapper("ROBERT_BOSCH",GetParam(),&u32Fdop));   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("ROBERTBOSCH"));

   u32Fdcr = 0,u32Fdop = 0;

   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("ROBERTBOSCH",NULL));

   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_WRITEONLY,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper("ROBERTBOSCH",OSAL_EN_WRITEONLY,NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("ROBERTBOSCH",NULL));

   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READWRITE,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper("ROBERTBOSCH",OSAL_EN_READWRITE,NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("ROBERTBOSCH",NULL));
   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("ROBERTBOSCH",OSAL_EN_READONLY,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOACCESS, REGISTRY_u32IOOpen_wrapper("ROBERT_BOSCH",GetParam(),NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("ROBERTBOSCH",NULL));
}

TEST(REG_u32IOOpentest, REG_u32IOOpentest_NULL)
{
   uintptr_t u32Fdcr = 0,u32Fdop = 0;

   /*test  for NULL */
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper(" ",OSAL_EN_READONLY,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper(" ",OSAL_EN_READONLY,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdop)); 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper(" "));
}

TEST(REG_u32IOOpentest, REG_u32IOOpentest_DiffModes_DiffKeyNames)
{
   uintptr_t u32Fdcr = 0,u32Fdop = 0;
   
   /*test for different key names*/
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("UNITTEST",OSAL_EN_READONLY,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper("UNITTEST",OSAL_EN_READONLY,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("UNITTEST"));
   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REG_u32IOCreate_wrapper("UNITTEST/REGISTRY",OSAL_EN_READONLY,&u32Fdcr));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REG_u32IOOpen_wrapper("UNITTEST/REGISTRY",OSAL_EN_READONLY,&u32Fdop));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REG_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("UNITTEST/REGISTRY"));
   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REG_u32IOCreate_wrapper("UNITTEST/REGISTRY/REGISTRY1",OSAL_EN_READONLY,&u32Fdcr));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REG_u32IOOpen_wrapper("UNITTEST/REGISTRY/REGISTRY1",OSAL_EN_READONLY,&u32Fdop));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REG_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("UNITTEST/REGISTRY/REGISTRY1"));
   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("123",OSAL_EN_READONLY,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper("123",OSAL_EN_READONLY,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("123"));
   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",OSAL_EN_READONLY,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOOpen_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",OSAL_EN_READONLY,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"));
}

TEST(REGISTRY_u32IOOpentest, REGISTRY_u32IOOpentest_NULL)
{
   uintptr_t u32Fdcr = 0,u32Fdop = 0;

   /*test  for NULL */
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper(" ",OSAL_EN_READONLY,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper(" ",OSAL_EN_READONLY,NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdop)); 
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper(" ",NULL));
}

TEST(REGISTRY_u32IOOpentest, REGISTRY_u32IOOpentest_DiffModes_DiffKeyNames)
{
   uintptr_t u32Fdcr = 0,u32Fdop = 0;
   
   /*test for different key names*/
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("UNITTEST",OSAL_EN_READONLY,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper("UNITTEST",OSAL_EN_READONLY,NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("UNITTEST",NULL));
   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REGISTRY_u32IOCreate_wrapper("UNITTEST/REGISTRY",OSAL_EN_READONLY,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REGISTRY_u32IOOpen_wrapper("UNITTEST/REGISTRY",OSAL_EN_READONLY,NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REGISTRY_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("UNITTEST/REGISTRY",NULL));
   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REGISTRY_u32IOCreate_wrapper("UNITTEST/REGISTRY/REGISTRY1",OSAL_EN_READONLY,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_DOESNOTEXIST, REGISTRY_u32IOOpen_wrapper("UNITTEST/REGISTRY/REGISTRY1",OSAL_EN_READONLY,NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REGISTRY_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("UNITTEST/REGISTRY/REGISTRY1",NULL));
   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("123",OSAL_EN_READONLY,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper("123",OSAL_EN_READONLY,NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("123",NULL));
   
   u32Fdcr = 0,u32Fdop = 0;
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOCreate_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",OSAL_EN_READONLY,NULL,&u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOOpen_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",OSAL_EN_READONLY,NULL,&u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOClose_wrapper(u32Fdop));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IORemove_wrapper("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",NULL));
}

/****************************************************************************/
                      /* Test  case  for bLockRegistry */
/****************************************************************************/
TEST(bLockRegistrytest, bLockRegistrytest_SUCCESS)
{  
   EXPECT_EQ(TRUE, bLockRegistry_wrapper());   
}

/****************************************************************************/
                      /* Test  case  for bUnLockRegistry */
/****************************************************************************/
TEST(bUnLockRegistrytest, bUnLockRegistrytest_SUCCESS)
{  
   EXPECT_EQ(TRUE, bUnLockRegistry_wrapper());
}

/****************************************************************************/
/* Test  case  for  REG_u32IOControl  OSAL_C_S32_IOCTRL_VERSION*/
/****************************************************************************/
TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_VERSION, REG_u32IOControl_OSAL_C_S32_IOCTRL_VERSION_Success)
{
   uintptr_t u32fd = 0;
   intptr_t s32VersionInfo = 0;

   /* Get device version */	
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOControl_wrapper(u32fd,OSAL_C_S32_IOCTRL_VERSION,(intptr_t)&s32VersionInfo));
   
   if(s32VersionInfo != REGISTRY_VERSION)
   {
      tS32 ret = OSAL_E_NOERROR;      
      OSALIO_UnitTest_HelperPrintf(TR_LEVEL_FATAL,"s32VersionInfo  = %x",s32VersionInfo);
      EXPECT_EQ(OSAL_ERROR, ret); 
   }
} 

/****************************************************************************/
/* Test  case  for  REG_u32IOControl  OSAL_C_S32_IOCTRL_BUILD_REG*/
/***************************************************************************/
#ifndef GEN3ARM
TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_BUILD_REG, REG_u32IOControl_OSAL_C_S32_IOCTRL_BUILD_REG_Success)
{
   uintptr_t u32fd = 0;
   char name[25] = "/dev/local/osal.reg";

   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOControl_wrapper(u32fd,OSAL_C_S32_IOCTRL_BUILD_REG,(intptr_t)&name[0]));
} 
#endif

TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_BUILD_REG, REG_u32IOControl_OSAL_C_S32_IOCTRL_BUILD_REG_Failure)
{
   uintptr_t u32fd = 0;
   char name[50] = "/dev/root/opt/bosch/base/registry/osal.reg";
   
   EXPECT_EQ(OSAL_E_UNKNOWN, REG_u32IOControl_wrapper(u32fd,OSAL_C_S32_IOCTRL_BUILD_REG,(intptr_t)&name[0]));
}

/****************************************************************************/
                      /* Test  case  for vShowStructSizes */
/****************************************************************************/
#ifdef SHOW_SIZES
TEST(vShowStructSizes, vShowStructSizes_Success)
{
   vShowStructSizes_wrapper();
}
#endif

/****************************************************************************************/
 /* Test  case  for  REGISTRY_u32IOControl  OSAL_C_S32_IOCTRL_REGISTRYLOCKandUNLOCK*/
/****************************************************************************************/
TEST(REGISTRY_u32IOControl_OSAL_C_S32_IOCTRL_REGISTRYLOCKandUNLOCK, REGISTRY_u32IOControl_OSAL_C_S32_IOCTRL_REGISTRYLOCKandUNLOCK_Success)
{
   uintptr_t u32fd = 0;
   intptr_t arg = 0;

   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOControl_wrapper(u32fd,OSAL_C_S32_IOCTRL_REGISTRYLOCK,(intptr_t)&arg));
   EXPECT_EQ(OSAL_E_NOERROR, REGISTRY_u32IOControl_wrapper(u32fd,OSAL_C_S32_IOCTRL_REGISTRYUNLOCK,(intptr_t)&arg));
}

/****************************************************************************/
            /* Test  case  for  REGISTRY_u32IOControl  DEFAULT*/
/****************************************************************************/
TEST(REGISTRY_u32IOControl_DEFAULT, REGISTRY_u32IOControl_DEFAULT_Success)
{
   uintptr_t u32fd = 0;
   intptr_t arg = 0;

   EXPECT_EQ(OSAL_E_WRONGFUNC, REGISTRY_u32IOControl_wrapper(u32fd,100,(intptr_t)&arg));
}

/****************************************************************************/
		/* Test  case  for  REG_u32IOControl  OSAL_C_S32_IOCTRL_REGSETVALUE */
/****************************************************************************/
TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_STRING, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_STRING_Success)
{
   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistry rReg = {0};
 
   /* set a string as value*/
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_READWRITE,&u32Fdcr));

   rReg.pcos8Name = (tPCS8 )"xxxxx";
   rReg.ps8Value  = (tU8*)"yyyyy";
   rReg.u32Size   = 5;
   rReg.s32Type   = OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg));
   
   rReg.pcos8Name = (tPCS8)"xxxxx";
   rReg.s32Type   = OSAL_C_S32_VALUE_STRING;
   rReg.u32Size   = 0;
   rReg.ps8Value  = NULL;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(intptr_t)&rReg));

   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr)); 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
}

TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_INT, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_INT_Success)
{
   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistry rReg;
   tS32  U8Value = 10;

   /* set a integer as value*/
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_READWRITE,&u32Fdcr));

   rReg.pcos8Name = (tPCS8 )"AAAAA";
   rReg.ps8Value  = (tU8*)&U8Value;
   rReg.u32Size   = 2;
   rReg.s32Type   = OSAL_C_S32_VALUE_S32;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg));

   rReg.pcos8Name = (tPCS8)"AAAAA";
   rReg.s32Type   = OSAL_C_S32_VALUE_S32;
   rReg.u32Size   = 0;
   rReg.ps8Value  = NULL;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(intptr_t)&rReg));
 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
}


TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_OSAL_EN_READONLY, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_OSAL_EN_READONLY_Success)
{
   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistry rReg;
   
   /*To check setting a value fails, if access is OSAL_EN_READONLY*/
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_READONLY,&u32Fdcr));

   rReg.pcos8Name = (tPCS8 )"AAAAA";
   rReg.ps8Value  = (tU8*)"BBBBB";
   rReg.u32Size   = 5;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_E_NOACCESS,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg)); 
   
   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
}

#define REGVALUE_APPID                          "APPID"
#define REGVALUE_SERVICEID                      "SERVICEID"

TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_REGVALUE_APPID, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_REGVALUE_APPID_Success)
{
   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistry rReg = {0};
 
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_READWRITE,&u32Fdcr));

   rReg.pcos8Name = (tPCS8 )REGVALUE_APPID;
   rReg.ps8Value  = (tU8*)"yyyyy";
   rReg.u32Size   = 5;
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg));
   
   rReg.pcos8Name =  (tPCS8)REGVALUE_APPID;
   rReg.s32Type   = OSAL_C_S32_VALUE_S32;
   rReg.u32Size   =  0;
   rReg.ps8Value  =  NULL;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(intptr_t)&rReg));
   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr)); 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
}


TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_REGVALUE_SERVICEID, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGSETVALUE_REGVALUE_SERVICEID_Success)
{
   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistry rReg = {0};
 
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_READWRITE,&u32Fdcr));

   rReg.pcos8Name = (tPCS8 )REGVALUE_SERVICEID;
   rReg.ps8Value  = (tU8*)"yyyyy";
   rReg.u32Size   = 5;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg));
   
   rReg.pcos8Name =  (tPCS8)REGVALUE_SERVICEID;
   rReg.s32Type   = OSAL_C_S32_VALUE_STRING;
   rReg.u32Size   =  0;
   rReg.ps8Value  =  NULL;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(intptr_t)&rReg));
 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr)); 
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
}

/****************************************************************************/
  /* Test  case  for  REG_u32IOControl  OSAL_C_S32_IOCTRL_REGGETVALUE */
/****************************************************************************/
TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGGETVALUE_STRING, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGGETVALUE_STRING_Success)
{
   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistry rReg;
   tS8   pBuffer[30] = {0};
 
   /*Get a string value in buffer*/
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_READWRITE,&u32Fdcr));

   rReg.pcos8Name = (tPCS8)"AAAAA";
   rReg.ps8Value  = (tU8*)"BBBBB";
   rReg.u32Size   = 6;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg));
 
   rReg.pcos8Name =  (tPCS8 )"AAAAA"; 
   rReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   rReg.u32Size   =  6;
   rReg.ps8Value  =  (tPU8)pBuffer;		
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGGETVALUE,(intptr_t)&rReg));	

   #ifdef DEBUG_UTEST
   memcpy(pBuffer,"SSSSS",6);
   #endif
   if(memcmp(pBuffer, "BBBBB",6) != 0)
   {
      tS32 ret = OSAL_E_NOERROR;      
      OSALIO_UnitTest_HelperPrintf(TR_LEVEL_FATAL,"GETVALUE  STRING TEST = %s",pBuffer);
      EXPECT_EQ(OSAL_ERROR, ret); 
   }

   rReg.pcos8Name =  (tPCS8)"AAAAA";
   rReg.s32Type   = OSAL_C_S32_VALUE_STRING;
   rReg.u32Size   =  0;
   rReg.ps8Value  =  NULL;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(intptr_t)&rReg));
   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
}

TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGGETVALUE_INT, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGGETVALUE_INT_Success)
{
   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistry rReg;
   tS32  u32RegistryValue = 10;
   tS32 U32Getval = 0;
   
   /*get an integer value*/
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_READWRITE,&u32Fdcr));

   rReg.pcos8Name = (tPCS8)"AAAAA";
   rReg.ps8Value = (tPU8)&u32RegistryValue;
   rReg.u32Size	= sizeof( tS32 );
   rReg.s32Type = OSAL_C_S32_VALUE_S32;

   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg));
   
   rReg.pcos8Name = (tPCS8 )"AAAAA"; 
   rReg.ps8Value  =(tPU8)&U32Getval;
   rReg.u32Size	=sizeof(tS32);
   rReg.s32Type	 =OSAL_C_S32_VALUE_S32;
   
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGGETVALUE,(intptr_t)&rReg));

   #ifdef DEBUG_UTEST
   U32Getval = 67;
   #endif
   if(U32Getval != u32RegistryValue ) 
   {
      tS32 ret = OSAL_E_NOERROR;      
      OSALIO_UnitTest_HelperPrintf(TR_LEVEL_FATAL,"GETVALUE INT TEST = %d",U32Getval);
      EXPECT_EQ(OSAL_ERROR, ret); 
   }

   rReg.pcos8Name =  (tPCS8)"AAAAA";
   rReg.s32Type   = OSAL_C_S32_VALUE_S32;
   rReg.u32Size   =  0;
   rReg.ps8Value  =  NULL;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(intptr_t)&rReg));

   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
  
}

TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGGETVALUE_OSAL_EN_WRITEONLY, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGGETVALUE_OSAL_EN_WRITEONLY_Success)
{
   uintptr_t u32Fdcr = 0,temp=0;
   OSAL_trIOCtrlRegistry rReg;
   tS8   pBuffer[30] = {0};
   
   /*To check getting a value fails, if access is OSAL_EN_WRITEONLY*/
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_WRITEONLY,&u32Fdcr));

   temp = u32Fdcr;
   
   rReg.pcos8Name = (tPCS8 )"AAAAA";
   rReg.ps8Value  = (tU8*)"BBBBB";
   rReg.u32Size   = 5;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGSETVALUE,(intptr_t)&rReg)); 


   rReg.pcos8Name =  (tPCS8 )"AAAAA"; 
   rReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
   rReg.u32Size   =  5;
   rReg.ps8Value  =  (tPU8)pBuffer;
   EXPECT_EQ(OSAL_E_NOACCESS,REG_u32IOControl_wrapper(temp,OSAL_C_S32_IOCTRL_REGGETVALUE,(intptr_t)&rReg));				

   rReg.pcos8Name =  (tPCS8)"AAAAA";
   rReg.s32Type   = OSAL_C_S32_VALUE_S32;
   rReg.u32Size   =  0;
   rReg.ps8Value  =  NULL;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGREMOVEVALUE,(intptr_t)&rReg));
   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
}


/****************************************************************************/
/* Test  case  for  REG_u32IOControl  OSAL_C_S32_IOCTRL_REGWRITEVALUE_STRING*/
/****************************************************************************/
TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGWRITEVALUE_STRING, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGWRITEVALUE_STRING_Success)
{

   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistryValue sValue;

   /*test to write a string value*/
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_WRITEONLY,&u32Fdcr));
   
   memcpy(sValue.s8Name, (const void*)"REG", 3);
   memcpy(sValue.s8Value, (const void*)"WriteValue", 10);
   sValue.s32Type   =  OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGWRITEVALUE,(intptr_t)&sValue));

   memcpy(sValue.s8Name, (const void*)"REG", 3);
   memcpy(sValue.s8Value, (const void*)"0", 0);
   sValue.s32Type   =  OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGDELETEVALUE,(intptr_t)&sValue));
   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
}

/****************************************************************************/
   /* Test  case  for  REG_u32IOControl  OSAL_C_S32_IOCTRL_REGWRITEVALUE INT*/
/****************************************************************************/
TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGWRITEVALUE_INT, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGWRITEVALUE_INT_Success)
{

   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistryValue sValue;
   uintptr_t  u32RegistryValue = 10;

   /*test to write a integer value*/
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_WRITEONLY,&u32Fdcr));
   
   memcpy(sValue.s8Name, (const void*)"REG", 3);
   memcpy(sValue.s8Value, (const void*)&u32RegistryValue, sizeof( u32RegistryValue ));
   sValue.s32Type   =  OSAL_C_S32_VALUE_S32;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGWRITEVALUE,(intptr_t)&sValue));

   memcpy(sValue.s8Name, (const void*)"REG", 3);
   memcpy(sValue.s8Value, 0, 0);
   sValue.s32Type   =  OSAL_C_S32_VALUE_S32;
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGDELETEVALUE,(intptr_t)&sValue));
   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG"));
}

/************************************************************************************/
 /* Test  case  for  REG_u32IOControl  OSAL_C_S32_IOCTRL_REGWRITEVALUE NOACCESS*/
/************************************************************************************/
TEST(REG_u32IOControl_OSAL_C_S32_IOCTRL_REGWRITEVALUE_OSAL_EN_READONLY, REG_u32IOControl_OSAL_C_S32_IOCTRL_REGWRITEVALUE_OSAL_EN_READONLY_Success)
{
   uintptr_t u32Fdcr = 0;
   OSAL_trIOCtrlRegistryValue sValue;

   /*To check writing a value fails, if access is OSAL_EN_READONLY*/
   EXPECT_EQ(OSAL_E_NOERROR,REG_u32IOCreate_wrapper("TESTREG",OSAL_EN_READONLY,&u32Fdcr));

   memcpy(sValue.s8Name, (const void*)"REG", 3);
   memcpy(sValue.s8Value, (const void*)"WriteValue", 10);
   sValue.s32Type   =  OSAL_C_S32_VALUE_STRING;
   EXPECT_EQ(OSAL_E_NOACCESS,REG_u32IOControl_wrapper(u32Fdcr,OSAL_C_S32_IOCTRL_REGWRITEVALUE,(intptr_t)&sValue));
   
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IOClose_wrapper(u32Fdcr));
   EXPECT_EQ(OSAL_E_NOERROR, REG_u32IORemove_wrapper("TESTREG")); 
}

/****************************************************************************/
 /* Test  case  for  s32RegistryMemUnlink SUCCESS*/
/****************************************************************************/
TEST(s32RegistryMemUnlinkTest,s32RegistryMemUnlinkTest_Success)
{
   EXPECT_EQ(OSAL_OK, s32RegistryMemUnlink_wrapper()); 
}

/****************************************************************************/
 /* Test  case  for  u32GetUsedRegistrySize SUCCESS*/
/****************************************************************************/
TEST(u32GetUsedRegistrySizeTest,u32GetUsedRegistrySizeTest_Success)
{
   tU32 U32UsedRegistrySize = 0;
  
   U32UsedRegistrySize   = u32GetUsedRegistrySize_wrapper();
   if(U32UsedRegistrySize <= REGISTRY_MEM_SIZE)
   {  
      int ret = OSAL_OK;
      EXPECT_EQ(OSAL_OK, ret); 
   }

   if(U32UsedRegistrySize > REGISTRY_MEM_SIZE)
   {
      int ret = OSAL_OK;
      OSALIO_UnitTest_HelperPrintf(TR_LEVEL_FATAL,"UsedRegistrySize  %d EXCEEDS maximum Registry Size %d",U32UsedRegistrySize,REGISTRY_MEM_SIZE);
      EXPECT_EQ(OSAL_ERROR, ret); 
   }
}

