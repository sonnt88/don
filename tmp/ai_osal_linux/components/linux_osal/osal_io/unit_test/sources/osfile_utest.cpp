/*****************************************************************************
| FILE:         Osfile_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL IO
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for Osfile.c
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 01.04.16  | Compiler warning fix       | VEW5KOR 
|              for CFG3-1772
| 31.07.15  | Initial revision           | Amit Bhardwaj
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif

#include "gmock/gmock.h"

#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"
#include "osfile.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "dispatcher_utest.h"

#define BUFF_SIZE 250
#define TRACE_SOURCE "Tracebuffupdate"

         
/**********************  Osfile test  *************************************************/

class Osfiletest : public ::testing::Test
{
  protected:
   virtual void SetUp()
   {
      OSAL_IOCreate("/dev/ffs/abc.txt" , OSAL_EN_READWRITE);
      OSAL_IOCreate("/dev/ffs2/abc.txt" , OSAL_EN_READWRITE);
   }
   virtual void TearDown() 
   {}

};

/**************************** Tracebufferupdatetest *******************************/

TEST_F(Osfiletest , Tracebuffer_updatetest)
{
   tU8 au8Buf[BUFF_SIZE]={0};
   tU32 len = strlen(TRACE_SOURCE);
   EXPECT_EQ(len, u32TraceBuf_Update(&au8Buf[0], TRACE_SOURCE, (tU32)(len+10)));  
   EXPECT_EQ(len, u32TraceBuf_Update(&au8Buf[0], TRACE_SOURCE, (tU32)(len+16)));
}

/*************************** sGetOptionStringtest ***********************************/

TEST_F(Osfiletest , sGetOptionStringtest)
{
   char* szReturn;
   szReturn = sGetOptionString(OSAL_EN_READONLY);
   EXPECT_EQ("OSAL_EN_READONLY", szReturn);  
   szReturn = sGetOptionString(OSAL_EN_WRITEONLY);
   EXPECT_EQ("OSAL_EN_WRITEONLY", szReturn);  
   szReturn = sGetOptionString(OSAL_EN_READWRITE);
   EXPECT_EQ("OSAL_EN_READWRITE", szReturn);  
   szReturn = sGetOptionString(OSAL_EN_APPEND);
   EXPECT_EQ("OSAL_EN_APPEND", szReturn);  
   szReturn = sGetOptionString(OSAL_EN_TEXT);
   EXPECT_EQ("OSAL_EN_TEXT", szReturn); 
   szReturn = sGetOptionString(OSAL_EN_BINARY);
   EXPECT_EQ("OSAL_EN_BINARY", szReturn);
}

/*************************** LFS_u32IOOpenDevicetest ****************************************/

TEST_F(Osfiletest , LFS_u32IOOpenDevicetest)
{
   uintptr_t ret = (uintptr_t)OSAL_EN_DEVID_ROOT; 
   tU32 u32ErrorCode = LFS_u32IOOpen( "/",OSAL_EN_READONLY,(tS32)OSAL_EN_DEVID_ROOT,&ret);
   EXPECT_EQ(OSAL_E_NOERROR, u32ErrorCode);
   ret = (uintptr_t)OSAL_EN_DEVID_FFS_FFS;
   u32ErrorCode = LFS_u32IOOpen( "",OSAL_EN_READONLY,(tS32)OSAL_EN_DEVID_FFS_FFS,&ret);
   EXPECT_EQ(OSAL_E_NOERROR, u32ErrorCode);
   ret = (uintptr_t)OSAL_EN_DEVID_FFS_FFS2;
   u32ErrorCode = LFS_u32IOOpen( "",OSAL_EN_READONLY,(tS32)OSAL_EN_DEVID_FFS_FFS2,&ret);
   EXPECT_EQ(OSAL_E_NOERROR, u32ErrorCode);
   ret = (uintptr_t)OSAL_EN_DEVID_FFS_FFS3;
   u32ErrorCode = LFS_u32IOOpen( "",OSAL_EN_READONLY,(tS32)OSAL_EN_DEVID_FFS_FFS3,&ret);
   EXPECT_EQ(OSAL_E_NOERROR, u32ErrorCode);
   ret = (uintptr_t)OSAL_EN_DEVID_FFS_FFS4;
   u32ErrorCode = LFS_u32IOOpen( "",OSAL_EN_READONLY,(tS32)OSAL_EN_DEVID_FFS_FFS4,&ret);
   EXPECT_EQ(OSAL_E_NOERROR, u32ErrorCode);
}

/*************************** LFS_u32IOOpenFiletest ****************************************/

TEST_F(Osfiletest , LFS_u32IOOpenFiletest)
{
   uintptr_t ret = (uintptr_t)OSAL_EN_DEVID_FFS_FFS;
   tU32 u32ErrorCode = LFS_u32IOOpen( "abc.txt",OSAL_EN_READONLY,(tS32)OSAL_EN_DEVID_FFS_FFS, &ret);
   EXPECT_EQ(OSAL_E_NOERROR, u32ErrorCode);
   ret = (uintptr_t)OSAL_EN_DEVID_FFS_FFS2;
   u32ErrorCode = LFS_u32IOOpen( "abc.txt",OSAL_EN_READONLY,(tS32)OSAL_EN_DEVID_FFS_FFS2, &ret);
   EXPECT_EQ(OSAL_E_NOERROR, u32ErrorCode);
   ret = (uintptr_t)OSAL_EN_DEVID_FFS_FFS3;
   u32ErrorCode = LFS_u32IOOpen( "abc.txt",OSAL_EN_READONLY,(tS32)OSAL_EN_DEVID_FFS_FFS3, &ret);
   EXPECT_EQ(OSAL_E_NOERROR, u32ErrorCode);
   ret = (uintptr_t)OSAL_EN_DEVID_FFS_FFS4;
   u32ErrorCode = LFS_u32IOOpen( "abc.txt",OSAL_EN_READONLY,(tS32)OSAL_EN_DEVID_FFS_FFS4, &ret);
   EXPECT_EQ(OSAL_E_NOERROR, u32ErrorCode);
}

/*************************** LFS_u32IOCloseFiletest ****************************************/

TEST_F(Osfiletest , LFS_u32IOCloseFiletest)
{
   OSAL_tIODescriptor fd = OSAL_IOOpen( "/dev/ffs/abc.txt",OSAL_EN_READONLY);
   EXPECT_NE(OSAL_ERROR, fd);   
   EXPECT_EQ(OSAL_OK, OSAL_s32IOClose(fd));
}

/* Test the function s32GetFileSize with wrong parameters */
TEST_F(Osfiletest, GetfilesizeNegativeTest)
{
   /* Open a non existing file */
   OSAL_tIODescriptor fd  = OSAL_IOOpen(OSAL_C_STRING_DEVICE_FFS3"/dfg.txt", OSAL_EN_READONLY);
   EXPECT_EQ(OSAL_ERROR, fd);
   /* Check the size it will return error */
   EXPECT_EQ(OSAL_ERROR, (s32GetFileSize(fd)));
}

/* Test the function bAccessAllowed with positive and negative arguments*/
TEST_F(Osfiletest, bAccessAllowedTest)
{
   /* Test with positive arguments */
   EXPECT_EQ(FALSE, bAccessAllowed(OSAL_C_S32_IOCTRL_FIOMKDIR, OSAL_EN_READONLY));
   EXPECT_EQ(FALSE, bAccessAllowed(OSAL_C_S32_IOCTRL_FIOTOTALSIZE, (OSAL_tenAccess)0));
   /* Test with negative arguments */
   EXPECT_EQ(TRUE, bAccessAllowed(OSAL_C_S32_IOCTRL_FIOWHERE, OSAL_EN_WRITEONLY));
}
