/*****************************************************************************
| FILE:         osal_aio_utest.cpp.
| PROJECT:      platform
| SW-COMPONENT: OSAL IO
|-----------------------------------------------------------------------------
| DESCRIPTION:  This file contains unit test cases for osal_aio.c
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 01.05.16  | Initial revision           | Amit Bhardwaj
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif
#include <gtest/gtest-spi.h>
#include "gmock/gmock.h"
using namespace std;
using namespace testing;
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "dispatcher_utest.h"
#include "Linux_osal.h"
#include "ostrace.h"
#include "osfile.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define DEV_RAMDISK_FILE "/dev/ramdisk/abc.txt"
#define DATA_READ_SIZE 50
#define DATA_WRITE_SIZE 10
#define THREAD_WAIT 3000
tS32 bSuccess = 0;
         
/*********************************************************************/
/*                 Osal_aio.c file Tests                             */
/*********************************************************************/

class Osalaiotest : public Test
{
  protected:
   OSAL_tIODescriptor hFile ;
   OSAL_tIODescriptor hFile2 ;
   tS8 *ps8ReadBuffer ;
   tS8 *ps8WriteBuffer;
   Osalaiotest()
   {
      hFile2 = 0;
      hFile = 0;
      ps8ReadBuffer = NULL;
      ps8WriteBuffer = NULL;
   }
   virtual void SetUp()
   {
      hFile = 0;
      tCS8 DataToWrite[45] = "Read the Data Asynchronously "; 
      hFile2 = OSAL_IOCreate(DEV_RAMDISK_FILE, OSAL_EN_READWRITE);
      OSAL_s32IOWrite( hFile2, DataToWrite,40 ); 
      ps8ReadBuffer = new tS8[DATA_READ_SIZE + 1];
      ps8WriteBuffer = new tS8[DATA_WRITE_SIZE + 1];
   }
   virtual void TearDown()
   {
      OSAL_s32IOClose(hFile2);
      OSAL_s32IORemove(( tCString )DEV_RAMDISK_FILE);
      delete[]ps8ReadBuffer;
      ps8ReadBuffer = NULL;
      delete[]ps8WriteBuffer;
      ps8WriteBuffer = NULL;
   }
   ~Osalaiotest()
   {
      if (NULL != ps8ReadBuffer)
      {
      delete[]ps8ReadBuffer;
      }
      if (NULL != ps8WriteBuffer)
      {
      delete[]ps8WriteBuffer;
      }
   }
};

tVoid vFSAsyncCallback ( OSAL_trAsyncControl *prAsyncCtrl )
{
   (void)prAsyncCtrl;
   bSuccess += 10;
   TraceString("callback\n");
}

/*********************************************************************/
/* Test Cases for OSAL_s32IOReadAsync API                            */
/*********************************************************************/

TEST_F(Osalaiotest, OsalAsyncReadTestwithValidControlStruct )
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8ReadBuffer;
   rAsyncCtrl.u32Length = DATA_READ_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   EXPECT_NE( OSAL_ERROR , OSAL_s32IOReadAsync ( &rAsyncCtrl ));
   OSAL_s32ThreadWait(THREAD_WAIT);
   EXPECT_EQ(10, bSuccess);
   EXPECT_NE(OSAL_ERROR, OSAL_s32IOClose (hFile));   
}

TEST_F(Osalaiotest, OsalAsyncReadTestwithInvalidControlStruct )
{
   EXPECT_EQ( OSAL_ERROR , OSAL_s32IOReadAsync (OSAL_NULL));
   EXPECT_EQ( OSAL_E_DOESNOTEXIST, OSAL_u32ErrorCode());
}

TEST_F(Osalaiotest, OsalAsyncReadTestwithInvalidFileDescriptor )
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = 0;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8ReadBuffer;
   rAsyncCtrl.u32Length = DATA_READ_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   EXPECT_EQ( OSAL_ERROR , OSAL_s32IOReadAsync (&rAsyncCtrl));
   EXPECT_EQ( OSAL_E_BADFILEDESCRIPTOR, OSAL_u32ErrorCode());   
}

TEST_F(Osalaiotest, OsalAsyncReadTestwithInvalidCallback )
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8ReadBuffer;
   rAsyncCtrl.u32Length = DATA_READ_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )0;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   EXPECT_EQ( OSAL_ERROR , OSAL_s32IOReadAsync (&rAsyncCtrl));
   EXPECT_EQ( OSAL_E_DOESNOTEXIST, OSAL_u32ErrorCode());
}

/*********************************************************************/
/* Test Cases for OSAL_s32IOWriteAsync API                            */
/*********************************************************************/

TEST_F(Osalaiotest, OsalAsyncWriteTest )
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure */
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   EXPECT_NE( OSAL_ERROR , OSAL_s32IOWriteAsync ( &rAsyncCtrl ));
   OSAL_s32ThreadWait(THREAD_WAIT); /* Wait for write operation to get completed */
   EXPECT_EQ(20, bSuccess);
   EXPECT_NE(OSAL_ERROR, OSAL_s32IOClose (hFile));   
}

TEST_F(Osalaiotest, OsalAsyncWriteTestwithInvalidCallback )
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )0;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   EXPECT_EQ( OSAL_ERROR , OSAL_s32IOWriteAsync ( &rAsyncCtrl ));
   EXPECT_EQ( OSAL_E_DOESNOTEXIST, OSAL_u32ErrorCode());
   EXPECT_NE(OSAL_ERROR, OSAL_s32IOClose (hFile));   
}

TEST_F(Osalaiotest, OsalAsyncWriteTestwithInvalidControlStructure )
{
   EXPECT_EQ( OSAL_ERROR , OSAL_s32IOWriteAsync ( OSAL_NULL)); 
   EXPECT_EQ( OSAL_E_DOESNOTEXIST, OSAL_u32ErrorCode());
}

/*********************************************************************/
/* Test Cases for OSAL_u32IOErrorAsync API                           */
/*********************************************************************/

TEST_F(Osalaiotest, OsalAsyncIOErrortestwithinvalidcontrolstruct)
{
   /*ErrorAsync function will return error with control structure as NULL */
   EXPECT_EQ(OSAL_E_INVALIDVALUE, OSAL_u32IOErrorAsync(NULL));
}

TEST_F(Osalaiotest, OsalAsyncIOErrortestwithinvalidfiledescriptor)
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = 0;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   /*ErrorAsync function will return error with invalid file descriptor */
   EXPECT_EQ(OSAL_E_BADFILEDESCRIPTOR, OSAL_u32IOErrorAsync(&rAsyncCtrl));
}

TEST_F(Osalaiotest, OsalAsyncIOErrortestwithvalidcontrolstruct)
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   /*ErrorAsync function will return error with control structure as NULL */
   EXPECT_NE(OSAL_ERROR, OSAL_u32IOErrorAsync(&rAsyncCtrl));
}

/*********************************************************************/
/* Test Cases for OSAL_s32IOReturnAsync API                          */
/*********************************************************************/

TEST_F(Osalaiotest, OsalAsyncIOReturntestwithinvalidControlstruct)
{
   /*ReturnAsync function will return error with control structure as NULL */
   EXPECT_EQ(OSAL_E_INVALIDVALUE, OSAL_s32IOReturnAsync(OSAL_NULL));
}

TEST_F(Osalaiotest, OsalAsyncIOReturntestwithValidControlStruct)
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   EXPECT_NE(OSAL_ERROR, OSAL_s32IOReturnAsync(&rAsyncCtrl));
}

TEST_F(Osalaiotest, OsalAsyncIOReturntestwithInvalidFiledescriptor)
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = 0;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   EXPECT_NE(OSAL_ERROR, OSAL_s32IOReturnAsync(&rAsyncCtrl));
}

/*********************************************************************/
/* Test Cases for OSAL_s32IOCancelAsync API                          */
/*********************************************************************/

TEST_F(Osalaiotest, OsalAsyncIOCanceltestwithInvalidControlStructure )
{
   OSAL_trAsyncControl rAsyncCtrl;
   rAsyncCtrl.id = 0;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   /* OsalAsyncCancel will return error with invalid control structure*/
   EXPECT_EQ(OSAL_ERROR, OSAL_s32IOCancelAsync(rAsyncCtrl.id ,&rAsyncCtrl ));
}

TEST_F(Osalaiotest, OsalAsyncIOCanceltestwithinvalidFileDescriptor )
{
   OSAL_trAsyncControl rAsyncCtrl;
    /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   EXPECT_EQ(OSAL_ERROR, OSAL_s32IOCancelAsync( 0 ,&rAsyncCtrl ));
}

TEST_F(Osalaiotest, OsalAsyncIOCanceltestwithValidFileDescriptor )
{
   OSAL_trAsyncControl rAsyncCtrl;
    /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   EXPECT_EQ(OSAL_C_S32_AIO_ALLDONE, OSAL_s32IOCancelAsync( rAsyncCtrl.id  ,&rAsyncCtrl ));
}

TEST_F(Osalaiotest, OsalAsyncIOCanceltestwithValidFileDescriptorWithAsyncRead)
{
   OSAL_trAsyncControl rAsyncCtrl;
   /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8ReadBuffer;
   rAsyncCtrl.u32Length = DATA_READ_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   rAsyncCtrl.pvArg = &rAsyncCtrl;
   EXPECT_NE( OSAL_ERROR , OSAL_s32IOReadAsync (&rAsyncCtrl));
   OSAL_s32ThreadWait(THREAD_WAIT);
   EXPECT_EQ(OSAL_C_S32_AIO_ALLDONE, OSAL_s32IOCancelAsync( rAsyncCtrl.id  ,&rAsyncCtrl ));
}

TEST_F(Osalaiotest, OsalAsyncIOCanceltestwithValidFileDescriptorWithAsyncWrite)
{
   OSAL_trAsyncControl rAsyncCtrl;
    /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Fill the asynchronous control structure*/
   rAsyncCtrl.id = hFile;
   rAsyncCtrl.s32Offset = 0;
   rAsyncCtrl.pvBuffer = ps8WriteBuffer;
   rAsyncCtrl.u32Length = DATA_WRITE_SIZE;
   rAsyncCtrl.pCallBack = ( OSAL_tpfCallback )vFSAsyncCallback;
   EXPECT_NE( OSAL_ERROR , OSAL_s32IOWriteAsync ( &rAsyncCtrl ));
   OSAL_s32ThreadWait(THREAD_WAIT);
   EXPECT_EQ(OSAL_C_S32_AIO_ALLDONE, OSAL_s32IOCancelAsync( rAsyncCtrl.id  ,&rAsyncCtrl ));


}

/*********************************************************************/
/*       Test Cases for OsalAsyncIOShutdowntest API                  */
/*********************************************************************/

TEST_F(Osalaiotest, OsalAsyncIOShutdowntest )
{
	EXPECT_EQ(OSAL_OK, s32ShutdownAsyncTask());
}

/*********************************************************************/
/*       Test Cases for IsNfsDev API                                 */
/*********************************************************************/

TEST_F(Osalaiotest, OsalIsNfsDevcheckforDevicewithFileSystem )
{
    /*Open the File */
   hFile = OSAL_IOOpen("/dev/ffs", OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   /*Check if the given file descriptor is from non-file system.*/
   EXPECT_NE(TRUE, IsNfsDev(hFile));

}

TEST_F(Osalaiotest, OsalIsNfsDevcheckwithValidFiledescriptorforDevwithFilesystem )
{
   /*Open the File */
   hFile = OSAL_IOOpen(DEV_RAMDISK_FILE, OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   OSAL_s32IOClose(hFile);
   EXPECT_NE(TRUE, IsNfsDev(hFile));

}

TEST_F(Osalaiotest, OsalIsNfsDevcheckwithValidFiledescriptorforDevwithoutFilesystem )
{
   /*Open the File */
   hFile = OSAL_IOOpen("/dev/trace", OSAL_EN_READWRITE );
   EXPECT_NE(OSAL_ERROR, hFile);
   OSAL_s32IOClose(hFile);
   /*Check if the given file descriptor is from non-file system.*/
   EXPECT_NE(TRUE, IsNfsDev(hFile));

}
/*********************************************************************/
/*       Test Cases for s32EnterErrMem API                           */
/*********************************************************************/

TEST_F(Osalaiotest, Osals32EnterErrMemCheckwithValidParameter )
{
   char cErrMemBuffer[100];
   snprintf(&cErrMemBuffer[0],100,"Valid ErrmemEntry for Task:%d",(int)OSAL_ThreadWhoAmI());
   /* /dev/errmem will fail for unit test */
   EXPECT_EQ(OSAL_ERROR, s32EnterErrMem(cErrMemBuffer));
}
