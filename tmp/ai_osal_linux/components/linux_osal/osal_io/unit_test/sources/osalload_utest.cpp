/*****************************************************************************
| FILE:         Osfile_utest.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL IO
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the unit test for Dispatcher_table.c
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 01.04.16  | Compiler warning fix       | VEW5KOR 
|              for CFG3-1772
| 31.07.15  | Initial revision           | Amit Bhardwaj
| 07.10.16  | fix for CFG3-2047 Unittest | vew5kor
|           |  OSAL IO tests are Blocked |
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#ifdef Gen3ArmMake
#include "persigtest.h"
#else
#include "gtest/gtest.h"
#endif
#include "gmock/gmock.h"

#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"
#include "ostrace.h"
#include "osfile.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



class Osalload_helper_test: public testing::TestWithParam<tCString>
{
   virtual void SetUp(void)
   {  
   }
   virtual void TearDown(void)
   {
   }

};

class Osalload_helper_open_negative_test: public Osalload_helper_test{};

INSTANTIATE_TEST_CASE_P(dataset1, Osalload_helper_open_negative_test, testing::Values(OSAL_C_STRING_DEVICE_KDS,OSAL_C_STRING_DEVICE_CDCTRL, OSAL_C_STRING_DEVICE_SPEECH, 
                        OSAL_C_STRING_DEVICE_GPS, OSAL_C_STRING_DEVICE_GNSS, OSAL_C_STRING_DEVICE_GPIO,OSAL_C_STRING_DEVICE_RTC, OSAL_C_STRING_DEVICE_ADC, 
                        OSAL_C_STRING_DEVICE_ABS,OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_C_STRING_DEVICE_GYRO, OSAL_C_STRING_DEVICE_CHENC, OSAL_C_STRING_DEVICE_ODOMETER, 
                        OSAL_C_STRING_DEVICE_VOLT,OSAL_C_STRING_DEVICE_AUXILIARY_CLOCK, OSAL_C_STRING_DEVICE_PRAM, OSAL_C_STRING_DEVICE_ADR3CTRL, OSAL_C_STRING_DEVICE_CDAUDIO,
                        OSAL_C_STRING_DEVICE_FFD, OSAL_C_STRING_DEVICE_WUP));

#ifndef GEN3ARM						
TEST_P(Osalload_helper_open_negative_test, Opendevices_without_filesystem)
{
 /* Open the devices */
 OSAL_tIODescriptor fd = OSAL_IOOpen(GetParam(), OSAL_EN_READONLY);
 EXPECT_EQ(OSAL_ERROR, fd);
 EXPECT_EQ(OSAL_ERROR, OSAL_s32IOClose(fd));
}
#endif
/* TBD */



