#ifdef __cplusplus
extern "C" {
#endif
tString sGetOptionString(OSAL_tenAccess enAccess);
tU32 u32TraceBuf_Update(tU8 *szDest, tCString szSource, tU32 u32szDestUnusedSize);
tS32 TRACE_s32IOOpen(tVoid);
tS32 TRACE_s32IOClose(tVoid);
tS32 TRACE_s32IORemove( tS32 s32DevId );
tS32 TRACE_s32IOCreate( tS32 s32DevId );
tS32 s32ShutdownAsyncTask(void);
#ifdef __cplusplus
}

#endif

