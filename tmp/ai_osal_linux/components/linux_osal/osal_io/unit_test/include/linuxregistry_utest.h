#ifndef LINUXREGISTRY_UTEST_H 
#define LINUXREGISTRY_UTEST_H

#ifdef __cplusplus
extern "C" {
#endif

extern tU32 REG_u32IOClose(uintptr_t u32fd);
extern tU32 REGISTRY_u32IOClose( uintptr_t u32fd);

extern tU32 REG_u32IORemove(tCString coszName);
extern tU32 REGISTRY_u32IORemove(tCString coszName, tCString szDrive);

extern tU32 REG_u32IOCreate(tCString coszName, OSAL_tenAccess enAccess, uintptr_t *pu32RetAddr);
extern tU32 REGISTRY_u32IOCreate(tCString coszName, OSAL_tenAccess enAccess, tCString szDrive, uintptr_t *pu32RetAddr);

extern tU32 REG_u32IOOpen(tCString coszName, OSAL_tenAccess enAccess, uintptr_t *pu32RetAddr);
extern tU32 REGISTRY_u32IOOpen(tCString coszName, OSAL_tenAccess enAccess, tCString szDrive, uintptr_t* pu32RetAddr);

extern tU32 REG_u32IOControl(uintptr_t u32fd, tS32 fun, intptr_t arg);
extern tU32 REGISTRY_u32IOControl( uintptr_t u32fd, tS32 fun, intptr_t arg);

extern tS32 s32RegistryInit(void);
extern tS32 s32RegistryDeInit(void);

extern tBool bLockRegistry(void);
extern tBool bUnLockRegistry(void);

extern tU32 u32GetUsedRegistrySize(void);
extern void vFreeMemory(void* pMem);
extern void* pvGetMemory(tU32 u32Size);

extern tS32 s32RegistryConnect(void);
extern tS32 s32RegistryMemUnlink(void);
extern tBool bBuildupRegistryLockless(char* pcPtr,tU32 u32Size);
extern tBool bCreateRegFromFile(char* szFileName);
extern tBool bBuildErgReg(char* szFileName,tU32 u32Size,tU8* pu8Data);
extern tBool bGetEnvData(tU32* pu32Size,char* szFileName);
extern tBool bCreateRegFromCryptedFile(char* szFileName);
extern tBool bFindErgKeyVal(OSAL_trErgReq* prErgReqeust, tU32 u32Size,tU8* pu8Data);
extern tBool bValueOfCryptedFile(OSAL_trErgReq* prErgReqeust);
#ifdef SHOW_SIZES
extern void vShowStructSizes(void);
#endif

tU32 REG_u32IOClose_wrapper(uintptr_t u32fd)
{
   return REG_u32IOClose(u32fd);
}
tU32 REGISTRY_u32IOClose_wrapper(uintptr_t u32fd)
{
   return REGISTRY_u32IOClose(u32fd);
}
tU32 REG_u32IORemove_wrapper(tCString coszName)	
{
   return REG_u32IORemove(coszName);
}

tU32 REGISTRY_u32IORemove_wrapper(tCString coszName, tCString szDrive)
{
   return REGISTRY_u32IORemove(coszName,szDrive);
}

tU32 REG_u32IOCreate_wrapper(tCString coszName, OSAL_tenAccess enAccess, uintptr_t *pu32RetAddr)
{
   return REG_u32IOCreate(coszName,enAccess,pu32RetAddr);
}
tU32 REGISTRY_u32IOCreate_wrapper(tCString coszName, OSAL_tenAccess enAccess, tCString szDrive, uintptr_t *pu32RetAddr)
{
   return REGISTRY_u32IOCreate(coszName,enAccess,szDrive,pu32RetAddr);
}

tU32 REG_u32IOOpen_wrapper(tCString coszName, OSAL_tenAccess enAccess, uintptr_t *pu32RetAddr)
{
   return REG_u32IOOpen(coszName,enAccess,pu32RetAddr);
}

tU32 REGISTRY_u32IOOpen_wrapper(tCString coszName, OSAL_tenAccess enAccess, tCString szDrive, uintptr_t* pu32RetAddr)
{
   return REGISTRY_u32IOOpen(coszName,enAccess,szDrive,pu32RetAddr);
}

tU32 REG_u32IOControl_wrapper(uintptr_t u32fd, tS32 fun, intptr_t arg)
{
   return REG_u32IOControl(u32fd,fun,arg);
}

tU32 REGISTRY_u32IOControl_wrapper( uintptr_t u32fd, tS32 fun, intptr_t arg)
{
   return REGISTRY_u32IOControl(u32fd,fun,arg);
}

tS32 s32RegistryInit_wrapper(void)
{
   return s32RegistryInit();
}

tS32 s32RegistryDeInit_wrapper(void)
{
   return s32RegistryDeInit();
}
tBool bLockRegistry_wrapper(void)
{  
   return bLockRegistry();
}

tBool bUnLockRegistry_wrapper(void)
{
   return bUnLockRegistry();
}

tU32 u32GetUsedRegistrySize_wrapper(void)
{
   return u32GetUsedRegistrySize();
}

void vFreeMemory_wrapper(void* pMem)
{
   return vFreeMemory(pMem);
}

void* pvGetMemory_wrapper(tU32 u32Size)
{
   return pvGetMemory(u32Size);
}
tS32 s32RegistryConnect_wrapper(void)
{
   return s32RegistryConnect();
}

tS32 s32RegistryMemUnlink_wrapper(void)
{
   return s32RegistryMemUnlink();
}

tBool bBuildupRegistryLockless_wrapper(char* pcPtr,tU32 u32Size)
{
   return bBuildupRegistryLockless(pcPtr,u32Size);
}
tBool bCreateRegFromFile_wrapper(char* szFileName)
{
   return bCreateRegFromFile(szFileName);
}

tBool bBuildErgReg_wrapper(char* szFileName,tU32 u32Size,tU8* pu8Data)
{
   return bBuildErgReg(szFileName,u32Size,pu8Data);
}

tBool bGetEnvData_wrapper(tU32* pu32Size,char* szFileName)
{
   return bGetEnvData(pu32Size,szFileName);
}

tBool bCreateRegFromCryptedFile_wrapper(char* szFileName)
{
   return bCreateRegFromCryptedFile(szFileName);
}

tBool bFindErgKeyVal_wrapper(OSAL_trErgReq* prErgReqeust, tU32 u32Size,tU8* pu8Data)
{
   return bFindErgKeyVal( prErgReqeust,  u32Size,pu8Data);
}

tBool bValueOfCryptedFile_wrapper(OSAL_trErgReq* prErgReqeust)
{
   return bValueOfCryptedFile(prErgReqeust);
}

#ifdef SHOW_SIZES
void vShowStructSizes_wrapper()
{
   return vShowStructSizes();
}
#endif
#ifdef __cplusplus
}
#endif

#endif

