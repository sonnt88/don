/******************************************************************************
 * FILE				:	osal_io_unit_test_mock.h
 *
 * SW-COMPONENT		:	OSAL IO UNIT TEST 
 *
 * DESCRIPTION		:	This file contains the prototype and some Macros that 
 *						will be used in the file oedt_FS_TestFuncs.c   
 *               		for the FileSystem device.
 *                          
 * AUTHOR(s)		:	Sriranjan U (RBEI/ECM1), Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * Date				|							         |	Author & comments
 * --.--.--			|	Initial revision		      | 	------------
 * 31.03.15       |  Ported from OEDT           |  SWM2KOR
 *------------------------------------------------------------------------------*/

#ifndef __OSAL_IO_UNIT_TEST_MOCK_H_
#define __OSAL_IO_UNIT_TEST_MOCK_H_



class OsalIOUnitTestMock {
 public:
};   

class OSAL_IO : public testing::Environment
{
	public:
		OSAL_IO() {}
		virtual ~OSAL_IO() {}

	protected:
		virtual void SetUp();
		virtual void TearDown();
};


#endif     // __OSAL_IO_UNIT_TEST_MOCK_H_
