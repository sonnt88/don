/*****************************************************************************
| FILE:            osfile.c
| PROJECT:        platform
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL File System  
|                    accesses.
|-----------------------------------------------------------------------------
| COPYRIGHT:     (c) 2011 Robert Bosch GmbH
| HISTORY:        
| Date        | Modification                    | Author
| 03.10.05  | Initial revision              | MRK2HI
| --.--.--  | ----------------              | -------, -----
| 17.03.09  |Made Changes in LFS_u32IOClose()    | ANC3KOR
| 29.07.09  |Changes for cryptcard via USB        | rav8kor
| 14.01.11  | additional IOctls for Gen2 cryptcard        | rav8kor
| 13.03.12  | Added support to read CID for card2         | agn2cob
| 19.11.12  | fix for nikai-435                                 | gur6kor
| 30.01.13  | fix for nikai-2141 "/dev/card" open failure issue ( if card doesn't contain navi data)  | krs4cob
| 08.02.13  | To provide Fix to GMNGA-54319                 | SWM2KOR
| 26.02.13  | Removed Unsed IO control                        | SWM2KOR
| 26.02.13  | Fix for LSIM oedt failure                      |sja3kor
| 07.03.13  | fix for CFAGII-509 "/dev/cryptcard" open failure issue  | krs4cob
| 16.03.13  |    Added support for triggering sign verifcation from T-engine side for NIKAI-3713      |krs4cob
| 27.03.12  | Fix for NIAKI-4156 :Malformes file name strings            | SWM2KOR
| 03.04.13  | Fix for NIAKI-4347, To update the Path Name Buffers to  |
|              | 256(path lenght) + 64 (Internal osal mapping Max lenght)|
|              | u32TraceBuf_Update is added to update the trace buffer  | SWM2KOR
| 22.04.13  | Fix for NIKAI-4247  "USB - OSAL_s32IOControl always reads from the start" issue  | krs4cob
| 20.05.14  |FIx for SUZUKI Ticket CFG3-606 OSAL /dev/media does not support |
|              |          GEN2 IOCTL OSAL_C_S32_IOCTRL_CARD_STATE                              |pmh5kor
| 20.06.14  | FIx for SUZUKI Ticket CFG3-742 Improvement to make the IOCTLs |
|              | OSAL_C_S32_IOCTRL_CARD_STATE and OSAL_C_S32_IOCTRL_FIOGET_CID |
|              | thread safe                                                                    |pmh5kor
| 25.08.14  | FIx for Ticket CFG3-606 Implementation of      |
|              | OSAL_C_S32_IOCTRL_CARD_STATE IOCTL for LSIM    |pmh5kor
| 30.05.16  | Suppressed offset check in LFS_u32IORead for proc file system | Ahm5kor(RBEI/ECF5)
| --.--.--  | ----------------                                        |-------, -----

|****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#define _GNU_SOURCE
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"
#include "osfile.h"
#include "fd_crypt.h"
#include "fd_device_ctrl.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

  
#ifdef __cplusplus
extern "C"
{
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define OSAL_CNTRL_VERSION  0x100
#define OSAL_EN_ACCESS_GET_PERMISSION(a)          ((OSAL_tenAccess)(a & 0x0007))


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

    
/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
        
/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/* Compare with OSAL_tenFSDevID (dispatcher.h). The */
/* drive letter assignment is defined in osalio_public.h    */
tCString Drive[ OSAL_EN_DEVID_FSLAST ] = { 
                            C_DEV_FFS_FFS,        // ffs         internal
                            C_DEV_FFS_FFS2,      // ffs2        internal
                            C_DEV_FFS_FFS3,      // ffs3        internal
                            C_DEV_FFS_FFS4,      // ffs4        internal
                            C_DEV_RAMDISK,        // ramdisk    internal
                            C_DEV_REGISTRY,      // dummy1     internal
                            C_DEV_ROOT,            // root        internal
                            C_DEV_CRYPTNAVROOT, // ncr         internal
                            C_DEV_CRYPTNAV,      // ncn crytnav internal
                            C_DEV_NAVDB,          // ndb crytnav internal
                            C_DEV_DEVMEDIA,      // exd general external usb device (internal!)
                            C_DEV_DVD,             // dvd0 must be the first external!
                            C_DEV_CARD,            // card1
                            C_DEV_CRYPTCARD,     // cryptcard
                            C_DEV_LOCAL,          // local dir
                            C_DEV_DATA,            // DATA
                         };




#if defined (GEN3X86)
#define OSAL_U64_LSIM_CARD_SIZE ((tU64)1024)
#endif


/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static tS32 s32FillDirentTable(OSAL_tfdIO *fd);
static tVoid vFreeDirentTable(OSAL_tfdIO *fd);
static tS32 s32HandleDirRecursive(const char* pvBuffer, tU16 u16Operation, tU32* pu32Val);


/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

enum
{
    IndexFfs,
    IndexFfs2,
    IndexFfs3,
    IndexFfs4,
    IndexRamDisk1,
    IndexDvd0,
    IndexUdaSdCrypt,
    IndexCryptNavRoot,
    IndexCryptNav,
    IndexNavDb,
    IndexRoot,
    IndexDevMedia,
    IndexLocal,
    IndexData,
    IndexLast
};

// change MAX_MOUNTPOINT_STRING_SIZE in OsalConf.h, if necessary!!
static char MountPoint[IndexLast][ MAX_MOUNTPOINT_STRING_SIZE ] =
{ //1234567890123456789012345678901234567890  max 40
    "/var/opt/bosch/dynamic/ffs/",        // "/dev/ffs"
    "/var/opt/bosch/",                    // "/dev/ffs2"
    "/var/opt/bosch/dynamic/ffs/",        // "/dev/ffs3"
    "/var/opt/bosch/",                    // "/dev/ffs4"
    "/tmp/",
    "/cdrom",                             // "/dvd0/", actual mapping from "/dev/cdrom"
    "/shared/cryptnav/CRYPTNAV/",         // cryptnav  via usb card interface
    "/var/opt/bosch/navdata/",            // internal uncrypted nav data in flash (direct access to the usbms device)
    "/var/opt/bosch/navdata/cryptnav/",   // internal crypted nav data in flash
    "/var/opt/bosch/navdb/",              // internal navi data base
    "/",                                  // linux root filesystem
    "/dev/media/",                        // general usb root device. devices are subdirectories
    " ",                                  //  local
    " "
};

void vReplaceMountPointTable(void)
{
    if(getcwd(&MountPoint[IndexLocal][0], MAX_MOUNTPOINT_STRING_SIZE) == NULL)
    {
      TraceString("s32GetRealPath getcwd failed %d",errno);
    }
    else
    {
        TraceString("s32GetRealPath cwd %s",&MountPoint[IndexLocal][0]);
        (void)OSAL_szStringConcat( &MountPoint[IndexLocal][0],"/");
    }
    strncpy(&MountPoint[IndexFfs][0],"bosch/dynamic/ffs/",MAX_MOUNTPOINT_STRING_SIZE);
    strncpy(&MountPoint[IndexFfs2][0],"bosch/",MAX_MOUNTPOINT_STRING_SIZE);
    strncpy(&MountPoint[IndexFfs3][0],"bosch/dynamic/ffs/",MAX_MOUNTPOINT_STRING_SIZE);
    strncpy(&MountPoint[IndexFfs4][0],"bosch/",MAX_MOUNTPOINT_STRING_SIZE);
    strncpy(&MountPoint[IndexCryptNavRoot][0],"bosch/navdata/",MAX_MOUNTPOINT_STRING_SIZE);
    strncpy(&MountPoint[IndexCryptNav][0],"bosch/navdata/cryptnav/",MAX_MOUNTPOINT_STRING_SIZE);
    strncpy(&MountPoint[IndexNavDb][0],"bosch/navdb/",MAX_MOUNTPOINT_STRING_SIZE);
}

void vPrepareOsalFileSystemDevices(void)
{
    if(pOsalData->u32FsToLocal)
    {
      vReplaceMountPointTable();
    }
    if(pOsalData->szNavDatPath[0])
    {
        strncpy(&MountPoint[IndexData][0],pOsalData->szNavDatPath,MAX_MOUNTPOINT_STRING_SIZE);
    }
}

/*------------------------------------------------------------------------
ROUTINE NAME : u32CardDeviceName

PARAMETERS    : p8deviceName

RETURNVALUE  : u32error

DESCRIPTION  : Provided new API interface for getting the drive name dynamically

MODIFIED:

DATE         |  AUTHOR                         |MODIFICATION

02.05.13    |Anoop Chandran (RBEI/ECF5) | Initial verision 
 ------------------------------------------------------------------------*/

tU32 u32CardDeviceName(tU8 *p8deviceName)/* lint -e752 accessed from Crypt device */
{
    tU32 u32error= 1;
    DIR *dirp;
    struct dirent *dirinfo;
    if (OSAL_NULL != p8deviceName)
    {
        if(pOsalData->u32AmVersion == 2)
        {
            dirp = opendir("/tmp/.automounter/device_db/device_dev_mmcblk0");
            if( NULL != dirp )
            {
                strncpy((char*)&p8deviceName[0],(char*)"mmcblk0",sizeof("mmcblk0"));
                closedir(dirp);
            }
        }
        else
        {
            dirp = opendir(AUTOMOUNT1);
            if( NULL != dirp )
            {
                 for (\
                          dirinfo = readdir(dirp);\
                          ( 'm' != dirinfo->d_name[0]);\
                            dirinfo = readdir(dirp)\
                        )
                  {
                  }
                  strncpy((char*)&p8deviceName[0],(char*)dirinfo->d_name,strlen(dirinfo->d_name)-2);
                  p8deviceName[strlen(dirinfo->d_name)-2] = 0;
                  if(dirinfo->d_name[strlen(dirinfo->d_name)-2] != 'p')
                  {
                        NORMAL_M_ASSERT_ALWAYS();  
                  }
                  closedir(dirp);
                  u32error = 0;
            }
        }
    }
    return u32error;
}

/******************************************************************************************************
*
* Functions for Device Remapping
*
******************************************************************************************************/


/******************************************************************************
 * FUNCTION:        GetRealPath
 *
 * DESCRIPTION:    This is a Internal function, which extract/convert the OSAL 
 *                     file system path to LFS path
 *
 * PARAMETER:      pointer to OSAL path
 *                     pointer to LFS path
 *
 * RETURNVALUE:    s32ReturnValue
 *                      it is the function return value:
 *                      - OSAL_OK for success;
 *                      - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 20.08.02  |    Initial revision                            | MRK2HI
 *****************************************************************************/
static tS32 s32GetRealPath( const char* pFile, char* pRealPath, tS32 s32DeviceId )
{
    tS32  s32Return = OSAL_OK;

    switch( s32DeviceId )
    {
        case OSAL_EN_DEVID_FFS_FFS:  // /dev/ffs
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexFfs][0] );
            break;
        case OSAL_EN_DEVID_FFS_FFS2: // /dev/ffs2
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexFfs2][0] );
            break;
        case OSAL_EN_DEVID_FFS_FFS3: // /dev/ffs3
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexFfs3][0] );
            break;
        case OSAL_EN_DEVID_FFS_FFS4: // /dev/ffs4
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexFfs4][0] );
            break;
        case OSAL_EN_DEVID_DVD:        // dvd0
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexDvd0][0] ); 
            break;
        case OSAL_EN_DEVID_RAMDISK:  // ramdisk
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexRamDisk1][0] );
            break;
        case OSAL_EN_DEVID_LOCAL:  // 
                if(MountPoint[IndexLocal][0] == ' ')
                {
                    if(getcwd(&MountPoint[IndexLocal][0], MAX_MOUNTPOINT_STRING_SIZE) == NULL)
                    {
                        TraceString("s32GetRealPath getcwd failed %d",errno);
                    }
                    else
                    {
                        (void)OSAL_szStringConcat( &MountPoint[IndexLocal][0],"/");
                    }
                }
                (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexLocal][0] );
            break;
        case OSAL_EN_DEVID_CRYPT_CARD:  // card1
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexUdaSdCrypt][0] );
            break;
        case OSAL_EN_DEVID_ROOT: // lroot linux root file system
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexRoot][0] ); 
            break;
        case OSAL_EN_DEVID_CRYPTNAVROOT: // cryptnav (ncr)
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexCryptNavRoot][0] );
            break;            
        case OSAL_EN_DEVID_CRYPTNAV: // cryptnav (ncn)
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexCryptNav][0] );
            break;
        case OSAL_EN_DEVID_NAVDB:     // navdb (ndb)
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexNavDb][0] );
            break;
        case OSAL_EN_DEVID_DEV_MEDIA:     // external usb root device (exd)
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexDevMedia][0] );
            break;            
        case OSAL_EN_DEVID_DATA:  // /dev/data
            (void)OSAL_szStringCopy( (tString)pRealPath, &MountPoint[IndexData][0] );
            break;
        default:
            *pRealPath = '\0';
            s32Return = OSAL_ERROR;
            NORMAL_M_ASSERT_ALWAYS();
            break;
    }
    if(s32Return == OSAL_OK )
    {
        (void)OSAL_szStringConcat( pRealPath, pFile );
    }
    return s32Return;
}


#define  HALFSIZE(X)  ((X/2)-5)         //Providing Half Size along with 5 buffer size as empty
#define  TRACE_FILENAME_FILLER  "..."
#define  TRACE_FILENAME_FILLER_SIZE sizeof(TRACE_FILENAME_FILLER)
/******************************************************************************
 * FUNCTION:        u32TraceBuf_Update
 *
 * DESCRIPTION:    This function update the buffer for traces. If the path name 
 *                     is greater trace buffer then the path is printed in 
 *                     LonpathN...NmaePrint.txt format. This provides the full 
 *                     information of the Path used for IO operations.
 *                     It prints string starting HALFSIZE characters and last HALFSIZE characters
 *
 * PARAMETER:
 *    tU8 *              szDest  :    Trace Buffer
 *    tCString          szSource:    PathName
 *    tU32                u32szDestUnusedSize: Remaining size of Trace Buffer that can be used.
 * RETURNVALUE:    Send the lenght upto which the PathName is updated.
 *
 *
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 03.04.13  |    Initial revision                            | SWM2KOR
 ******************************************************************************/
tU32 u32TraceBuf_Update(tU8 *szDest, tCString szSource, tU32 u32szDestUnusedSize)
{
    tU32 slen=(tU32) strlen(szSource);
    if(slen >= u32szDestUnusedSize )
    {
        tU32 u32lenght =HALFSIZE(u32szDestUnusedSize);
        memcpy (szDest,szSource,u32lenght);
        memcpy(&szDest[u32lenght+1],TRACE_FILENAME_FILLER,TRACE_FILENAME_FILLER_SIZE);
        memcpy (&szDest[u32lenght+1+TRACE_FILENAME_FILLER_SIZE],szSource+slen-u32lenght,u32lenght);
        szDest[(2*u32lenght)+1+TRACE_FILENAME_FILLER_SIZE]='\0';
        slen = (2*u32lenght)+1+TRACE_FILENAME_FILLER_SIZE;
    }
    else
    {
          memcpy (szDest,szSource,slen);
          szDest[slen+1]='\0';
    }
    return slen;
}
/******************************************************************************************************
*
* end of Functions for Device Remapping
*
******************************************************************************************************/


    
/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

tString sGetOptionString(OSAL_tenAccess enAccess)
{
    tString szReturn;
    switch(enAccess)
    {
        case OSAL_EN_READONLY:
              szReturn = "OSAL_EN_READONLY";
             break;
        case OSAL_EN_WRITEONLY:
              szReturn = "OSAL_EN_WRITEONLY";
             break;
        case OSAL_EN_READWRITE:
              szReturn = "OSAL_EN_READWRITE";
             break;
        case OSAL_EN_APPEND:
              szReturn = "OSAL_EN_APPEND";
             break;
        case OSAL_EN_TEXT:
              szReturn = "OSAL_EN_TEXT";
             break;
        case OSAL_EN_BINARY:
              szReturn = "OSAL_EN_BINARY";
             break;
        default:
              szReturn = "Undefined";
             break;
    }
    return szReturn;
}

/******************************************************************************
 * FUNCTION:        s32GetFileSize(tS32 fd)
 *
 * DESCRIPTION:    This function return the size of the file referenced by fd.
 *
 * PARAMETER:
 *
 *      tS32         fd:     file handle;
 *
 * RETURNVALUE:    u32RetValue 
 *                          OSAL_ERROR
 *                          otherwise file size
 *                      
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 20.08.15  | correct initial revision copied from    | ket2hi
 *              | osaldbg.cpp                                     |
 ******************************************************************************/
tS32 s32GetFileSize(tS32 fd)
{
    tS32 s32Error = OSAL_ERROR;
    tS32 s32CurrentPos = lseek(fd, 0, SEEK_CUR);

    if (OSAL_ERROR != s32CurrentPos)
    {
        s32Error = lseek(fd, 0, SEEK_END);
        if (OSAL_ERROR != s32Error)
        {
            s32CurrentPos = lseek(fd, s32CurrentPos, SEEK_SET);
            if (OSAL_ERROR == s32CurrentPos)
            {
                s32Error = OSAL_ERROR;
            }
        }
        else
        {
             s32Error = OSAL_ERROR;
             TraceString("lseek(fd, 0, SEEK_END) error %d",errno);
         }
    }
    else
    {
          TraceString("lseek(fd, 0, SEEK_CUR) error %d",errno);
    }
    return s32Error;
}

/******************************************************************************
 * FUNCTION:        LFS_u32IOOpen
 *
 * DESCRIPTION:    This function opens a data file in a drive with the
 *                     specified options. In case the file does not exist,
 *                     OSAL_NULL is returned.
 *
 * PARAMETER:
 *
 *    tCString          szName:     file name;
 *    OSAL_tenAccess  enAccess:  options.
 *
 * RETURNVALUE:     pu32Addr: 
 *                          > including the file handle if everything goes right
 *                          > OSAL_ERROR otherwise.
 *                      
 *                      u32ReturnValue:
 *                          > OSAL_E_NOERROR
 *                          > OSAL_E_NOTSUPPORTED
 *
 *
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 26.02.04  |    Initial revision                            | CM-DI/EES4 S.Kropp
 * 03.04.13  | To provide fix to NIKAI-4347 , Update  |
 *              | the path name buffer                         | SWM2KOR
 ******************************************************************************/
tU32 LFS_u32IOOpen( tCString coszName, OSAL_tenAccess enAccess,
                          tS32 s32DeviceId,  uintptr_t *u32Addr )
{
    
    tS32 s32LowLevelOperation = OSAL_ERROR;
    tU32 u32ReturnValue         = OSAL_E_NOERROR;  
    tS32 lFd = 0;
    DIR* pDir = NULL;
    OSAL_tfdIO* ptr;
    struct stat tr_stat_data;
    tString szObjName  = OSAL_NULL;
    tString szObj        = OSAL_NULL;
    unsigned short u16OpenMode = 0;
    tU16 EnAccessMode  = (tU16)enAccess & 0x0007;
    tU16 EnAccessBin    = (tU16)enAccess & 0x0030;
    tCString coszDrive = Drive[ s32DeviceId ];
    char cNameBuffer[sizeof(tChar)*(OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET)]={0}; //To update the size for handling App folder name upto 256 apart osal Mapping

    if (coszName == NULL)
    {
        coszName = "";
    }
    if( (ptr = (OSAL_tfdIO*)OSAL_pvMemPoolFixSizeGetBlockOfPool(&FileMemPoolHandle)) != OSAL_NULL )
    {
        if( *coszName == 0 )  /* Is the device */
        {  
            if( ((tU32)enAccess & 0xFFFFFE00 ) != 0 ) // check all possible access mode bits
            {
                u32ReturnValue = OSAL_E_INVALIDVALUE;
            }
            else
            {
                /* OSAL_EN_READONLY          =0x0001,
                    OSAL_EN_WRITEONLY         =0x0002,
                    OSAL_EN_READWRITE         =0x0004,
                    OSAL_EN_APPEND             =0x0008,
                    OSAL_EN_TEXT                =0x0010,
                    OSAL_EN_BINARY             =0x0020,
                    OSAL_EN_READSTREAM        =0x0040
                    OSAL_EN_ACCESS_DEVICE    =0x0080,
                    OSAL_EN_ACCESS_DIR        =0x0100 */
            
                lFd         = IS_DEVICE; /* -1*/
                szObjName = (tString)coszDrive;
                coszName  = "";
                enAccess  = OSAL_EN_ACCESS_DEVICE;
                s32LowLevelOperation = OSAL_OK;
            }
        }
        else    /* In this case the object can be either a file or directory*/
        {
            szObj = cNameBuffer;
            szObjName = szObj;
            if( s32GetRealPath( (char*)coszName, szObjName, s32DeviceId ) == OSAL_OK )
            {
               if ( EnAccessBin == ((tU16)OSAL_EN_TEXT | (tU16)OSAL_EN_BINARY) )
               {
                  /* incompatible accessmode */
                  EnAccessMode = OSAL_NULL;
               }
               /* try open directory first to allow open to procfile system directory*/ 
               pDir = opendir( szObjName );
               if(pDir)
               {
                  enAccess =  OSAL_EN_ACCESS_DIR | enAccess;
                  lFd = IS_DIR;
                  s32LowLevelOperation = OSAL_OK;
                  u32ReturnValue = OSAL_E_NOERROR; 
               } 
               else
               {
                  lFd = -1;
                  /*******************
                   open the object
                  *******************/
                  switch( EnAccessMode )
                  { 
                     case OSAL_EN_READONLY:
                          if(*u32Addr == OSAL_EN_DEVID_DVD)
                          {
                             u16OpenMode = S_IRUSR | S_IXUSR | S_IROTH | S_IXOTH;
                             EnAccessMode = OSAL_EN_READONLY;
                          }
                          else
                          {
                             u16OpenMode = OSAL_FILE_ACCESS_RIGTHS;
                          }
                          if( (lFd = (OSAL_tIODescriptor)open( szObjName, O_RDONLY|O_CLOEXEC, u16OpenMode)) < 0 )
                          { 
                             if(errno == EINTR)
                             {
                                vWritePrintfErrmem("open failed EINTR PID:%d \n",getpid());
                             }
                          }
                         break;
                     case OSAL_EN_WRITEONLY:
                          if(*u32Addr == OSAL_EN_DEVID_DVD)
                          {
                             lFd = 0x00FF;
                             lFd = -1;
                          }
                          else
                          {
                             if((lFd = (OSAL_tIODescriptor)open(szObjName, O_WRONLY|O_CLOEXEC, OSAL_FILE_ACCESS_RIGTHS)) < 0)
                             { 
                                if(errno == EINTR)
                                {
                                   vWritePrintfErrmem("open failed EINTR PID:%d",getpid());
                                }
                             }
                          }
                         break;
                     case OSAL_EN_READWRITE:
                          if(*u32Addr == OSAL_EN_DEVID_DVD)
                          {
                             u16OpenMode = S_IRUSR | S_IXUSR | S_IROTH | S_IXOTH;
                             EnAccessMode = OSAL_EN_READONLY;
                          }
                          else
                          {
                             u16OpenMode = OSAL_FILE_ACCESS_RIGTHS;
                          }
                          if(EnAccessMode == OSAL_EN_READONLY)
                          {
                             lFd = (OSAL_tIODescriptor)open( szObjName, O_RDONLY|O_CLOEXEC, u16OpenMode );
                             if( lFd < 0 )
                             {
                                if(errno == EINTR)
                                {
                                   vWritePrintfErrmem("open failed EINTR PID:%d \n",getpid());
                                }
                             }
                          }
                          else
                          {
                             lFd = (OSAL_tIODescriptor)open( szObjName, O_RDWR|O_CLOEXEC, u16OpenMode );
                             if( lFd < 0 )
                             { 
                                if(errno == EINTR)
                                {
                                   vWritePrintfErrmem("open failed EINTR PID:%d \n",getpid());
                                }
                             }
                          }
                         break;
                     default:
                          lFd = -1;
                          u32ReturnValue = OSAL_E_NOTSUPPORTED;
                         break;
                  }/* switch( EnAccessMode ) */

                  if( lFd != -1 ) /* in this case the object is a file */
                  {
                     s32LowLevelOperation = OSAL_OK;
                  }
                  else
                  {
                     u32ReturnValue = u32ConvertErrorCore( errno );
                  }//if( lFd != -1)
               }//opendir
            }//if( s32GetRealPath( (char*)coszName, szObjName, s32DeviceId ) == OSAL_OK )
        }//if(szObj != OSAL_NULL )
    }//if( ptr )
    else
    {
       u32ReturnValue = OSAL_E_NOSPACE;
    }
        
    /* Allocate the Memory Structure & The return pointer */
    if((s32LowLevelOperation == OSAL_OK )&&(ptr))
    {                    
       if(lFd == IS_DIR)
       {
          ((OSAL_tfdIO*)ptr)->fd = (uintptr_t)pDir; 
          ((OSAL_tfdIO*)ptr)->u32Size    = 0xffffffff;
       }
       else if (lFd == IS_DEVICE)
       {
          ((OSAL_tfdIO*)ptr)->fd = lFd;
          ((OSAL_tfdIO*)ptr)->u32Size    = 0xefffffff;
       }
       else
       {
          ((OSAL_tfdIO*)ptr)->fd = lFd;
          if(fstat(lFd,&tr_stat_data) == 0)
          {
             ((OSAL_tfdIO*)ptr)->u32Size = (tU32)tr_stat_data.st_size;
          }
          else
          {
             ((OSAL_tfdIO*)ptr)->u32Size = 0;
              u32ReturnValue = OSAL_E_ACCESS_FAILED;
              NORMAL_M_ASSERT_ALWAYS();
          }
       }
       if(szObjName) (void)OSAL_szStringCopy((tString)ptr->szFileName, szObjName);
       else ptr->szFileName[0] = 0;
       (void)OSAL_szStringCopy((tString)ptr->szName, coszName);
       ((OSAL_tfdIO*)ptr)->offset    = 0;
       ((OSAL_tfdIO*)ptr)->enAccess = enAccess;
       ((OSAL_tfdIO*)ptr)->tpDirentTable = NULL;
       ((OSAL_tfdIO*)ptr)->tpDirent = ((OSAL_tfdIO*)ptr)->tpDirentTable;
       ((OSAL_tfdIO*)ptr)->DispId = s32DeviceId;
    }
    else
    {
       if( OSAL_s32MemPoolFixSizeRelBlockOfPool(&FileMemPoolHandle,(void*)ptr) == OSAL_ERROR )
       {
       }
       ptr = OSAL_NULL;
       // only if origin reason not known, set a default reason
       // Modified by Jentsch Roland
       if( u32ReturnValue == OSAL_E_NOERROR )
       {
          u32ReturnValue = OSAL_E_NOTSUPPORTED;
       }
    }
    
    *u32Addr = (uintptr_t)ptr;

    pOsalData->bTraceAll = LLD_bIsTraceActive( TR_COMP_OSALIO, TR_LEVEL_USER_4 );
    if((pOsalData->bTraceAll)||(u32ReturnValue != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
    {
#ifdef SHORT_TRACE_OUTPUT
        tU32 slen  = 16;
        tU8 au8Buf[MAX_TRACE_SIZE]={0};
        
        OSAL_M_INSERT_T8 (&au8Buf[0], EN_FS_DISPATCHER | EN_IOOPEN_RESULT);
        OSAL_M_INSERT_T32(&au8Buf[1], u32ReturnValue);
        OSAL_M_INSERT_T16(&au8Buf[5], enAccess);
        OSAL_M_INSERT_T32(&au8Buf[7], (uintptr_t)ptr);
        if( !ptr )
        {
            ptr = (OSAL_tfdIO*)OSAL_ERROR;
            if (coszDrive != NULL)
            {
                // add drivename to trace
                slen = u32TraceBuf_Update(&au8Buf[11], coszDrive, MAX_TRACE_SIZE - 11  );//Trace buffer update according to the buffer name length
                // add filename to trace
                // Modified by Jentsch Roland
                au8Buf[11 + slen + 1] = '/';
                slen += u32TraceBuf_Update(&au8Buf[11 + slen + 2], coszName, MAX_TRACE_SIZE - (11+slen+2)); //Trace buffer update according to the buffer name length
            }
            else
            {
                memcpy (&au8Buf[11], "UNKNOWN", 7);
                au8Buf[18] ='\0';
                slen = 8;
            }
        }
        else
        {
            slen = u32TraceBuf_Update(&au8Buf[11], ptr->szFileName, MAX_TRACE_SIZE - 11 );
        }
        if((pOsalData->bTraceAll)||(u32OsalSTrace & 0x00000001))LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], slen+12);
        else LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_ERRORS, &au8Buf[0], slen+12);
#else
        tU8 au8Buf[MAX_TRACE_SIZE+1]={0};
        tString szOption = sGetOptionString(enAccess);
        au8Buf[0] = OSAL_STRING_OUT;
        if(ptr)
        {
            snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOOPEN    : Result=0x%x Mode=%s, h=0x%" PRIxPTR " %s",
                        (unsigned int)u32ReturnValue,szOption,(uintptr_t)ptr,ptr->szFileName);
        }
        else
        {
            snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOOPEN    : Result=0x%x Mode=%s, h=0x%" PRIxPTR " %s",
                        (unsigned int)u32ReturnValue,szOption,(uintptr_t)0,coszName);
        }
        if((pOsalData->bTraceAll)||(u32OsalSTrace & 0x00000001))LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0],strlen((char*)(&au8Buf[1]))+1);
        else LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_ERRORS, &au8Buf[0], strlen((char*)(&au8Buf[1]))+1);
#endif
    }

    return(u32ReturnValue);
}

static tU32 u32CheckFileHandle( tU32 u32fd) 
{
    if((u32fd <= FileMemPoolHandle.u32memstart)&&(u32fd >= FileMemPoolHandle.u32memend))
    {
        trOSAL_MPF* pPtr = (trOSAL_MPF*)FileMemPoolHandle.pMem;
        if(pPtr->u32ErrCnt == 0)
        {
            return OSAL_E_INVALIDVALUE;
        }
        else
        {
             /* possible that Handle Memory was allocated via malloc */
             if(( u32fd == 0)||((tU32)u32fd == OSAL_C_INVALID_HANDLE))
             {
                  return OSAL_E_INVALIDVALUE;
             }
        }
    }
    return OSAL_E_NOERROR;
}


/******************************************************************************
 * FUNCTION:        LFS_s32IOClose
 *
 * DESCRIPTION:    This function closes a data file on a drive with the
 *                     specified options. In case the file does not exist, an error
 *                     is returned.
 *
 * PARAMETER:
 *
 *    OSAL_tIODescriptor fd    descriptor of file;
 *
 * RETURNVALUE:    u32ReturnValue
 *                      > OSAL_E_NOERROR if everything goes right
 *                      > OSAL_E_NOTSUPPORTED
 *                      > OSAL_E_UNKNOWN
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 26.02.04  |    Initial revision                            | CM-DI/EES4 S.Kropp
 * 04.03.13  | To provide fix to NIKAI-4347 , Update  |
 *              | the Trace format                              | SWM2KOR
 ******************************************************************************/
tU32 LFS_u32IOClose( uintptr_t u32fd, tS32 s32DeviceId )
{
    tU32 u32ReturnValue = u32CheckFileHandle(u32fd);
    tS32 s32Status = 1;
    ((tVoid)s32DeviceId); // satisfy lint

    if(u32ReturnValue == OSAL_E_NOERROR)
    {
        if( ((OSAL_tfdIO*)u32fd)->fd != 0 )
        {
            // prove for dev open
            if( (((OSAL_tfdIO*)u32fd)->fd == IS_DEVICE) || (((OSAL_tfdIO*)u32fd)->fd == IS_RESTRICTED_DEVICE))
            {
              s32Status = 0;
            }
            else 
            {
                // prove for dir open
                if((((OSAL_tfdIO*)u32fd)->enAccess & OSAL_EN_ACCESS_DIR) == OSAL_EN_ACCESS_DIR)
                {
                         // close dir
                      s32Status = closedir((DIR*)((OSAL_tfdIO*)u32fd)->fd);
                      vFreeDirentTable((OSAL_tfdIO*)u32fd);
                }
                else
                {
                    /* look for created pseudo file handle for directory 
                    without following readdir operation */
                    s32Status = close(((OSAL_tfdIO*)u32fd)->fd);
                }
            }
            /* check for good close or low voltage occured */
            if(s32Status != OSAL_OK)
            {
                u32ReturnValue = u32ConvertErrorCore(errno);
            }
        }
    }
    else
    {
        u32ReturnValue = OSAL_E_UNKNOWN;
    }
    
    if( (pOsalData->bTraceAll) || (u32ReturnValue != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
    {
#ifdef SHORT_TRACE_OUTPUT
        tU8 au8Buf[MAX_TRACE_SIZE]={0};
        tU32 slen = 0;
         
        OSAL_M_INSERT_T8 (&au8Buf[0], EN_FS_DISPATCHER | EN_IOCLOSE_RESULT);
        OSAL_M_INSERT_T32(&au8Buf[1], u32ReturnValue);
        OSAL_M_INSERT_T32(&au8Buf[5], u32fd);
        au8Buf[9] = 0;
        if(u32fd != (tU32)NULL)
        {
            if(((OSAL_tfdIO*)u32fd)->szFileName[0] != 0)
            {
                slen = u32TraceBuf_Update(&au8Buf[9], ((OSAL_tfdIO*)u32fd)->szFileName, MAX_TRACE_SIZE - 9 );
            }
        }
        LLD_vTrace( TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], slen + 10 );
#else
        tU8 au8Buf[MAX_TRACE_SIZE+1]={0};
        au8Buf[0] = OSAL_STRING_OUT;
        if((u32fd)&&(((OSAL_tfdIO*)u32fd)->szFileName[0] != 0))
        {
            snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOCLOSE  : Result=0x%x h=0x%" PRIxPTR " %s",
                        (unsigned int)u32ReturnValue,(uintptr_t)u32fd,((OSAL_tfdIO*)u32fd)->szFileName);
        }
        else
        {
            if(u32fd)
            {
                snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOCLOSE  : Result=0x%x h=0x%" PRIxPTR " %s",
                            (unsigned int)u32ReturnValue,(uintptr_t)u32fd,((OSAL_tfdIO*)u32fd)->szFileName);
            }
            else
            {
                snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOCLOSE  : Result=0x%x h=0x%" PRIxPTR " %s",
                            (unsigned int)u32ReturnValue,(uintptr_t)u32fd,"Unknown");
            }
        }
        LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], strlen((char*)(&au8Buf[1]))+1);
#endif
    }
    
    if((u32ReturnValue == OSAL_E_NOERROR )/* satisfy lint ->*/&&(u32fd))
    {
            ((OSAL_tfdIO*)u32fd)->fd = 0;
            ((OSAL_tfdIO*)u32fd)->u32Size    = 0xffffffff;
            ((OSAL_tfdIO*)u32fd)->offset = OSAL_ERROR;
            ((OSAL_tfdIO*)u32fd)->enAccess = (OSAL_tenAccess)0;
            if(OSAL_s32MemPoolFixSizeRelBlockOfPool(&FileMemPoolHandle,
                                                                 (void*)u32fd) == OSAL_ERROR )
            {
            }
            u32fd = 0;
    }

    return(u32ReturnValue);
}


/******************************************************************************
 * FUNCTION:        LFS_u32IOCreate
 *
 * DESCRIPTION:    This function creates a data file in a drive with the
 *                     specified options. In case the file does not exist,
 *                     an error is returned.
 *
 * PARAMETER:
 *
 *    tCString          coszName:  file name;
 *    OSAL_tenAccess  enAccess:  options.
 *
 * RETURNVALUE:     pu32Addr: 
 *                          > including the file handle if everything goes right
 *                          > OSAL_ERROR otherwise.
 *                      
 *                      u32ReturnValue:
 *                          > OSAL_E_NOERROR
 *                          > OSAL_E_NOTSUPPORTED
 *
 *
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 26.02.04  |    Initial revision                            | CM-DI/EES4 S.Kropp
 * 07.09.12  |    Updated the functionality so that it | sja3kor 
                      can trace out the name of file which 
                      is exceeding the limit of trace buffer  
 * 04.03.13  | To provide fix to NIKAI-4347 , Update  |
 *              | the path name buffer                         | SWM2KOR                      
 ******************************************************************************/
tU32 LFS_u32IOCreate( tCString coszName, OSAL_tenAccess enAccess,
                             tS32 s32DeviceId, uintptr_t *pu32Addr )
{
    tS32 s32LowLevelOperation = OSAL_ERROR;
    tU32 u32ReturnValue         = OSAL_E_NOERROR;
    tS32 lFd = -1;
    OSAL_tfdIO * ptr;
    tString szObjName  = OSAL_NULL;
    tString szObj        = OSAL_NULL;
#ifdef SHORT_TRACE_OUTPUT
    tCString coszDrive = Drive[s32DeviceId];
#endif

    tU16 EnAccessMode  = (tU16)enAccess & 0x0007;
    tU16 EnAccessBin    = (tU16)enAccess & 0x0030;
    char cNameBuffer[sizeof(tChar)*(OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET)]={0}; //To update the size for handling App folder name upto 256 apart osal Mapping

    if( (ptr = (OSAL_tfdIO*)OSAL_pvMemPoolFixSizeGetBlockOfPool(&FileMemPoolHandle)) != OSAL_NULL )
    {
        if( coszName == NULL )  /* Is the device */
        {
            u32ReturnValue = OSAL_E_NOTSUPPORTED;
        }
        else    /* In this case the object can be either a file or directory*/
        {
            szObj = cNameBuffer;
            {
                szObjName = szObj;
                if( s32GetRealPath( (char*)coszName, szObjName, s32DeviceId ) == OSAL_OK )
                {
                    // if( szObjName )    pointer is never changed within the function s32GetRealPath
                    {
                        if((tU16)EnAccessBin == (tU16)((int)OSAL_EN_TEXT | (int)OSAL_EN_BINARY))
                        {
                            /* incompatible accessmode */
                            EnAccessMode = OSAL_NULL;
                        }                          
                        /* navigation application need create of existing files */
                        /* prove for exiting file */
                 /*      if((lFd = open(szObjName, O_RDWR, OSAL_EN_READWRITE)) >= 0)
                        {
                                 TraceString("Existing file %s created by Task %d",szObjName,OSAL_ThreadWhoAmI());
                                 close(lFd);
                                 lFd = -1;
                        }*/
                        /*******************
                        open the object
                        *******************/
                        switch(EnAccessMode)
                        { 
                            case OSAL_EN_READONLY:
                            {
                                if((lFd = (OSAL_tIODescriptor)open(szObjName, O_CREAT|O_RDONLY|O_TRUNC|O_CLOEXEC/*|(O_APPEND & EnAccessApp*)*/, OSAL_ACCESS_RIGTHS_RO)) < 0)
                                { 
                                    u32ReturnValue = u32ConvertErrorCore(errno);
                                }
                            }
                            break;
                            case OSAL_EN_WRITEONLY:
                            {
                                if((lFd = (OSAL_tIODescriptor)open(szObjName, O_CREAT|O_WRONLY|O_TRUNC|O_CLOEXEC /*|(O_APPEND & EnAccessApp*)*/,OSAL_ACCESS_RIGTHS_WO)) < 0)
                                { 
                                    u32ReturnValue = u32ConvertErrorCore( errno );
                                }
                            }
                            break;
                            case OSAL_EN_READWRITE:
                            {
                                if((lFd = (OSAL_tIODescriptor)open(szObjName, O_CREAT | O_RDWR | O_TRUNC|O_CLOEXEC /*|(O_APPEND & EnAccessApp*)*/,OSAL_ACCESS_RIGTHS_RW)) < 0)
                                { 
                                    u32ReturnValue = u32ConvertErrorCore(errno);
                                }
                            }
                            break;
                            case OSAL_EN_APPEND:
                            case OSAL_EN_TEXT:
                            case OSAL_EN_BINARY:
                            default:
                                lFd = -1;
                                u32ReturnValue = OSAL_E_NOTSUPPORTED;
                                break;
                        }  // switch(EnAccessMode)
                    } // if(szObjName)
                        
                    if( lFd >= 0 ) /* in this case the object must be only a file */
                    {
                        s32LowLevelOperation = OSAL_OK;
                    }
                }
                else
                {
                    u32ReturnValue = OSAL_E_NOFILEDESCRIPTOR;
                }
            }
            /* Allocate the Memory Structure & The return pointer */
            if( (s32LowLevelOperation == OSAL_OK )/* satisfy lint -> */)
            {
                ((OSAL_tfdIO*)ptr)->fd = lFd;
                (void)OSAL_szStringCopy((tString)ptr->szFileName, szObjName);
                (void)OSAL_szStringCopy((tString)ptr->szName, coszName);
                ((OSAL_tfdIO*)ptr)->u32Size    = 0;
                ((OSAL_tfdIO*)ptr)->offset    = 0;
                ((OSAL_tfdIO*)ptr)->enAccess = enAccess;
                ((OSAL_tfdIO*)ptr)->tpDirentTable = NULL;
                ((OSAL_tfdIO*)ptr)->tpDirent = ((OSAL_tfdIO*)ptr)->tpDirentTable;
                ((OSAL_tfdIO*)ptr)->DispId = s32DeviceId;
            }
            else
            {
                if( OSAL_s32MemPoolFixSizeRelBlockOfPool(&FileMemPoolHandle, 
                                                                      (void*)ptr) == OSAL_ERROR )
                {         
                }
                // OSAL_vMemoryFree((OSAL_tfdIO*)ptr);
                ptr = OSAL_NULL;
            }
        }
    }
    else
    {
        u32ReturnValue = OSAL_E_NOTSUPPORTED;
    }
    
    if (szObj != NULL)
    {
//      OSAL_vMemoryFree( (tString)szObj );
    }
    
    *pu32Addr = (uintptr_t)ptr;

    pOsalData->bTraceAll = LLD_bIsTraceActive(TR_COMP_OSALIO, TR_LEVEL_USER_4);
    if( (pOsalData->bTraceAll) || (u32ReturnValue != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
    {
#ifdef SHORT_TRACE_OUTPUT
        tU32 slen = 14;
        tU8  au8Buf[MAX_TRACE_SIZE]={0};
        
        OSAL_M_INSERT_T8 (&au8Buf[0], EN_FS_DISPATCHER | EN_IOCREATE_RESULT);
        OSAL_M_INSERT_T32(&au8Buf[1], u32ReturnValue);
        OSAL_M_INSERT_T16(&au8Buf[5], enAccess);
        if((!ptr) || (!szObjName)) 
        {
            ptr = (OSAL_tfdIO*)OSAL_ERROR;
            if (coszDrive != NULL)
            {
              /*Here is the code to print the file path in case of errorsja3kor*/
              slen  = u32TraceBuf_Update(&au8Buf[11], coszDrive, MAX_TRACE_SIZE - 11); //Trace buffer update according to the buffer name length
              au8Buf[11 + slen + 1] = '/';
              slen += u32TraceBuf_Update(&au8Buf[11 + slen + 2], coszName, MAX_TRACE_SIZE - (11+slen+2) );  //Trace buffer update according to the buffer name length 
            }
            else
            {
                memcpy (&au8Buf[11], "UNKNOWN", 7);
                au8Buf[18] ='\0'; 
            }
        }
        else
        {
            //if the szObjName is not null then tracing out szObjName.
            slen = u32TraceBuf_Update(&au8Buf[11] , szObjName, MAX_TRACE_SIZE - 11 );          
            
        }
        OSAL_M_INSERT_T32(&au8Buf[7], (tU32)ptr);
        LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], slen + 12);
#else
        tU8 au8Buf[MAX_TRACE_SIZE+1]={0};
        tString szOption = sGetOptionString(enAccess);
        au8Buf[0] = OSAL_STRING_OUT;
        if(ptr)
        {

            snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOCREATE : Result=0x%x Mode=%s h=0x%" PRIxPTR " %s",
                        (unsigned int)u32ReturnValue,szOption,(uintptr_t)ptr,ptr->szFileName);
        }
        else
        {
            snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOCREATE : Result=0x%x Mode=%s h=0x%" PRIxPTR " %s",
                        (unsigned int)u32ReturnValue,szOption,(uintptr_t)0,coszName);
        }
        LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], strlen((char*)&au8Buf[1])+1);
#endif
    }
    return(u32ReturnValue);
}


/*****************************************************************************
 * FUNCTION:        LFS_IORemove
 *
 * DESCRIPTION:    This function removes a data file with the specified options.
 *                     In case the file does not exist, an error is returned.
 *
 * PARAMETER:
 *
 *    OSAL_tIODescriptor fd    descriptor of file;
 *
 * RETURNVALUE:    u32ReturnValue
 *                      > OSAL_E_NOERROR if everything goes right;
 *                      > OSAL_E_NOTSUPPORTED otherwise.
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 26.02.04  |    Initial revision                            | CM-DI/EES4 S.Kropp
 * 04.03.13  | To provide fix to NIKAI-4347 , Update  |
 *              | the path name buffer                         | SWM2KOR  
 ******************************************************************************/
tU32 LFS_u32IORemove(tCString coszName, tS32 s32DeviceId)
{
    tU32 u32ReturnValue = OSAL_E_NOERROR;
    tString szObjName    = OSAL_NULL;
    tS32 s32Status;
    DIR* pDir = NULL;
    char cNameBuffer[sizeof(tChar)*( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET)]; //To update the size for handling App folder name upto 256 apart osal Mapping

    /* non e' un file */
    if( coszName != NULL )
    {
        szObjName = cNameBuffer;
            if( s32GetRealPath( (char*)coszName, szObjName, s32DeviceId ) == OSAL_OK )
            {
                /* check if it is a directory */
                if((pDir = opendir((char*)szObjName)) != NULL)
                {
                    closedir(pDir);
                    s32Status = rmdir( (char*)szObjName );
                }
                else
                {
                    s32Status = remove( (char*)szObjName );
                }
                if( s32Status != 0 )
                {
                    u32ReturnValue = u32ConvertErrorCore( errno );
                }
            }
            else
            {
                u32ReturnValue = OSAL_E_NOTSUPPORTED;
            }
        //      OSAL_vMemoryFree((tString)szObjName); /*free object after trace usage */
    }
    else
    {
        u32ReturnValue = OSAL_E_NOTSUPPORTED;
    }

    if( (pOsalData->bTraceAll) || (u32ReturnValue != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
    {
#ifdef SHORT_TRACE_OUTPUT
        tU32 slen = 7;
        tU8 au8Buf[MAX_TRACE_SIZE]={0};
        
        OSAL_M_INSERT_T8( &au8Buf[0], EN_FS_DISPATCHER | EN_IOREMOVE_RESULT);
        OSAL_M_INSERT_T32(&au8Buf[1], u32ReturnValue);
        if (szObjName != NULL)
        {
            slen = u32TraceBuf_Update(&au8Buf[5] , szObjName, MAX_TRACE_SIZE - 5 );
        }
        else
        {
            memcpy (&au8Buf[5], "UNKNOWN", 7);
            au8Buf[12] ='\0';
        }
        LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], slen + 9);
#else
        tU8 au8Buf[MAX_TRACE_SIZE+1]={0};
        au8Buf[0] = OSAL_STRING_OUT;
        if(szObjName)
        {
            snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOREMOVE : Result=0x%x %s",
                        u32ReturnValue,szObjName);
        }
        else
        {
            snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOREMOVE : Result=0x%x %s",
                        u32ReturnValue,"UNKNOWN");
        }
        LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], strlen((char*)&au8Buf[1])+1);
#endif
    }    

    if( szObjName )
    {
//        OSAL_vMemoryFree( (tString)szObjName );
    }

    return(u32ReturnValue);
}



tS32 s32GetDirContent(const char* pvBuffer, char* pBuffer, const struct dirent* pDirEnt, tU32* pu32Val)
{
    tS32 s32RetValue = OSAL_OK;
    tS32 s32CurrentPos;
    tU32 Len, Len2;
    
    if(pu32Val)
    {
        DIR* pDir = NULL;
        tS32 fd;
        
        Len = strlen( (const char*)pvBuffer );
        memcpy( (void*)pBuffer, pvBuffer, Len );
        *(pBuffer+Len) = '/';
        Len2 = strlen(pDirEnt->d_name);
        memcpy((void*)(pBuffer + Len + 1), pDirEnt->d_name, Len2 );
        *(pBuffer + Len + 1 + Len2) = '\0';
        /* step 1: prove for directory */
        if((pDir = opendir(pDirEnt->d_name)) != NULL)
        {
            s32RetValue = s32HandleDirRecursive((char*)pBuffer, 1, pu32Val);
            closedir(pDir);
        }
        else
        {
            fd = open( pDirEnt->d_name, O_RDONLY, 0 ); 
            if( ( s32CurrentPos = lseek( fd, 0, SEEK_CUR ) ) < 0 )
            {
                // error
                s32RetValue = OSAL_ERROR;
            }
            else
            {
                if(( s32RetValue = lseek( fd, 0, SEEK_END )) < 0 )
                {
                    // error
                    s32RetValue = OSAL_ERROR;
                }
                else
                {
                    *pu32Val += (tU32)(s32RetValue - s32CurrentPos);
                    if( lseek( fd, s32CurrentPos, SEEK_SET ) < 0)
                    {
                        // error
                    }
                }
            }
            close(fd);
        }
    }
    return s32RetValue;
}


tS32 s32RemoveFile(const char* pvBuffer, char* pBuffer, const struct dirent* pDirEnt)
{
    tU32 Len, Len2;
    tS32 s32RetValue = OSAL_E_NOERROR;
    
    if( remove( (char*)pDirEnt->d_name ) == 0 )
    {
    }
    else
    {                 
        Len = strlen((const char*)pvBuffer);
        memcpy((void*)pBuffer,pvBuffer,Len);
        *(pBuffer+Len) = '/';
        Len2 = strlen(pDirEnt->d_name);
        memcpy( (void*)(pBuffer + Len + 1), pDirEnt->d_name, Len2 );
        *(pBuffer+Len+1+Len2) = '\0';
        s32HandleDirRecursive( (char*)pBuffer, 0, NULL );
        remove( (char*)&pBuffer[0] );
    }
    /* check for abort recursive delete */
    if(pOsalData->bStopRecursiveDelete == TRUE)
    {
        s32RetValue = OSAL_E_CANCELED;
    }
    return s32RetValue;
}

/******************************************************************************
 * FUNCTION:        s32HandleDirRecursive
 *
 * DESCRIPTION:    This function removes recursively a directory with content 
 *                     from the file system.
 *
 * PARAMETER:
 *
 *          void*    directory path;
 *
 * RETURNVALUE:    u32ErrorCode
 *                      > OSAL_E_NOERROR if everything goes right
 *                      > OSAL_E_NOTSUPPORTED if not supported by driver 
 *                      > OSAL_E_UNKNOWN otherwise.
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 02.10.07  |    Initial revision                            | CM-DI/CF32 MRK2HI
 * 07.02.13  |    To provide Fix to GMNGA-54319          | SWM2KOR
 * 26.02.13  |    Fix for LSIM.(setting up proper error| sja3kor 
                 |     code)  
 ******************************************************************************/
tS32 s32HandleDirRecursive(const char* pvBuffer, tU16 u16Operation, tU32* pu32Val) 
{
    tU32 u32ErrorCode = OSAL_E_NOERROR;
    struct dirent*    pDirEnt=NULL;
    DIR* dir;
    char* pBuffer = (char*)malloc(256);
    char* pRootDevice1 = "/nand0";
    char* pRootDevice2 = "/nand1";
    char* pRootDevice3 = "/nor0";
    char* pRootDevice4 = "/ram1";
    char* pRootDevice5 = "/uda1";
    char* pRootDevice6 = "/sda1";
    tBool bRetry = TRUE;

    //declare variables only for TSIM

    if(pBuffer)
    {
        while(bRetry)
        {
            bRetry = FALSE;
            /* special handling for DVD end */
            dir = opendir((const char*)pvBuffer);
            if(dir)
            {
                pDirEnt = readdir((DIR*)dir);
                while(pDirEnt)
                {
                    if(pDirEnt->d_name[0] != '.')
                    {
                        if(u16Operation == 0)
                        {
                            u32ErrorCode = (tU32)s32RemoveFile( pvBuffer, pBuffer, pDirEnt); 
                            if(u32ErrorCode == OSAL_E_CANCELED)        
                            break;
                        }
                        if(u16Operation == 1)
                        {
                            if(s32GetDirContent(pvBuffer, pBuffer, pDirEnt, pu32Val) == OSAL_ERROR)
                            {
                            }
                        }
                    }
                    else 
                    {
                        /* invalid directory*/
                    } 
                    pDirEnt = readdir(dir);
                    /* special handling for voltage sensitive devices */
                    if(pDirEnt == NULL)
                    {
                    }
                }/* end while */
                closedir((DIR*) dir);
                dir = NULL;
            }/* end opendir */
            else
            {
                u32ErrorCode = u32ConvertErrorCore(errno);
            }
        }
    }
    else
    {
        u32ErrorCode = OSAL_E_NOSPACE;
    }
    free( pBuffer );

    if((pOsalData->bStopRecursiveDelete == FALSE) && (u16Operation == 0) )
    {
        /* delete empty directory */
        if(     (OSAL_s32StringCompare( (char*)pvBuffer, (tCString)pRootDevice1 ) != 0)/*lint -e530 */
             && (OSAL_s32StringCompare( (char*)pvBuffer, (tCString)pRootDevice2 ) != 0)/*lint -e530 */
             && (OSAL_s32StringCompare( (char*)pvBuffer, (tCString)pRootDevice3 ) != 0)/*lint -e530 */
             && (OSAL_s32StringCompare( (char*)pvBuffer, (tCString)pRootDevice4 ) != 0)/*lint -e530 */
             && (OSAL_s32StringCompare( (char*)pvBuffer, (tCString)pRootDevice5 ) != 0)/*lint -e530 */
             && (OSAL_s32StringCompare( (char*)pvBuffer, (tCString)pRootDevice6 ) != 0))/*lint -e530 */
        {
            if( remove((char*) pvBuffer) < 0 )
            {
                u32ErrorCode = u32ConvertErrorCore(errno);
            }
        }
    }

    if( u32ErrorCode == OSAL_E_NOERROR )
    {
        tU32 slen = 14;
        tU8  au8Buf[MAX_TRACE_SIZE]={0};
        
        OSAL_M_INSERT_T8 (&au8Buf[0], EN_FS_DISPATCHER | EN_IORMDIR_RESULT);
        OSAL_M_INSERT_T32(&au8Buf[1], u32ErrorCode);
        if( pvBuffer )
        {
            slen = u32TraceBuf_Update(&au8Buf[5], pvBuffer, MAX_TRACE_SIZE - 5 );//Trace buffer update according to the buffer name length
        }
        LLD_vTrace( TR_COMP_OSALIO, TR_LEVEL_ERRORS, &au8Buf[0], slen + 5 );
    }
    return (tS32)u32ErrorCode;
}


/******************************************************************************
 * FUNCTION:        s32FileCopy
 *
 * DESCRIPTION:    This function copy a file from source to destination 
 *
 * PARAMETER:
 *
 *          tString    source file 
 *          tString    destination file
 *
 * RETURNVALUE:    OSAL error code
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 02.10.07  |    Initial revision                            | CM-DI/CF32 MRK2HI
 ******************************************************************************/
tS32 s32FileCopy(const char* strsrc, const char* strdest)
{
    tS32 s32RetValue = OSAL_OK,s32CurrentPos;
    tS32 fd_in_file;
    tS32 fd_out_file;
    int size = 0;
    tU16 u16OpenMode = 0;
    char* pBuffer = NULL;
    DIR* pDir = NULL;
    tU32 u32ErrorCode = OSAL_E_NOERROR;

    /* step 1: prove for directory */
    pDir = opendir(strsrc);
    if(pDir)
    {
        closedir(pDir);
        return OSAL_E_NOACCESS;
    }

    /* step 2: open source file and create destination file */
    fd_in_file = open(strsrc, O_RDONLY, u16OpenMode);
    if(fd_in_file < 0)
    {
        u32ErrorCode = u32ConvertErrorCore(errno);
    }
    fd_out_file = open( strdest, O_CREAT | O_RDWR | O_TRUNC,S_IRUSR|S_IWUSR );
    if(fd_out_file < 0) 
    {
        u32ErrorCode = u32ConvertErrorCore(errno);
    }
    close(fd_out_file);
    
    /* step 3: determine file size */
    /*  struct statvfs sta;
    sta.f_bsize = 0;
    sta.f_frsize = 0;
    sta.f_blocks = 0;
    sta.f_bfree = 0;
    sta.f_bavail = 0;
    sta.f_files = 0;
    sta.f_ffree = 0;
    sta.f_favail = 0;
    sta.f_fsid = 0;
    sta.f_flag = 0;
    sta.f_namemax = 0;

    if(fstatvfs(fd_in_file,&sta) < OSAL_OK)
    {
        u32ErrorCode = u32ConvertErrorCore(errno);
    }
    size = sta.f_bsize; */

    if(u32ErrorCode == OSAL_E_NOERROR)
    {
        if((s32CurrentPos = lseek( fd_in_file, 0, SEEK_SET )) < 0)
        {
            u32ErrorCode = u32ConvertErrorCore(errno);
        }
        else
        {
            if((s32RetValue = lseek(fd_in_file, 0, SEEK_END)) < 0)
            {
                u32ErrorCode = u32ConvertErrorCore(errno);
            }
            else
            {
                size = s32RetValue - s32CurrentPos;
                if(lseek(fd_in_file, 0, SEEK_SET) < 0)
                {
                    u32ErrorCode = u32ConvertErrorCore(errno);
                }
            }
        }
    }

    /* step 4: read source file and write to destination file */
    if((u32ErrorCode == OSAL_E_NOERROR)&&(size > 0))
    {
        int BytesToRead = 0;
        int written = 0;

        if(FILECOPY_BUFFERSIZE > size) 
        {
            pBuffer  = (char*)OSAL_pvMemoryAllocate((tU32)size);
        }
        else
        {
            pBuffer  = (char*)OSAL_pvMemoryAllocate(FILECOPY_BUFFERSIZE);
        }
        if(pBuffer)
        {
#ifndef FILE_SEEK_ACTIV
            fd_out_file = open(strdest, O_APPEND|O_RDWR , S_IRUSR|S_IWUSR);
#endif
            while(size)
            {
                BytesToRead = size-written;  
                if(BytesToRead > FILECOPY_BUFFERSIZE) BytesToRead = FILECOPY_BUFFERSIZE;

#ifdef FILE_SEEK_ACTIV
                fd_out_file = open( strdest, O_APPEND|O_RDWR , S_IRUSR|S_IWUSR);
#endif
                if(fd_out_file >= 0)  
                {
#ifdef FILE_SEEK_ACTIV
                    if( lseek( fd_in_file, written, SEEK_SET) >= 0 )
                    {
#endif
                        s32RetValue = read( fd_in_file,pBuffer,(tU32)BytesToRead);  
                        if(s32RetValue == BytesToRead) 
                        {
#ifdef FILE_SEEK_ACTIV
                            if(lseek( fd_out_file, written, SEEK_SET ) >= 0 )
                            {
#endif
                                s32RetValue = write( fd_out_file, (void*)pBuffer, (tU32)BytesToRead );
                                if(s32RetValue != BytesToRead) 
                                {
                                    u32ErrorCode = u32ConvertErrorCore(errno);
                                }
#ifdef FILE_SEEK_ACTIV
                            }
                            else
                            { 
                                u32ErrorCode = u32ConvertErrorCore(errno);
                            }
#endif
                        }
                        else
                        {
                            u32ErrorCode = u32ConvertErrorCore(errno);
                        }
#ifdef FILE_SEEK_ACTIV
                    }
                    else
                    {
                        u32ErrorCode = u32ConvertErrorCore(errno);
                    }
                    close(fd_out_file);
#endif
                } 
                else
                {
                    u32ErrorCode = u32ConvertErrorCore(errno);
                }
                if(u32ErrorCode != OSAL_E_NOERROR)
                {
                    OSAL_vMemoryFree(pBuffer);
                    pBuffer = NULL;
                    break;
                }
                written += BytesToRead;
                if(written >= size)
                {
                    OSAL_vMemoryFree(pBuffer);
                    pBuffer = NULL;
                    break;
                }
            }
            s32RetValue = OSAL_OK;
            OSAL_vMemoryFree(pBuffer);
            pBuffer = NULL;
        }
        else
        {
            /* allocation problem */
            u32ErrorCode = OSAL_E_NOSPACE;
        }
    }
 
    close(fd_in_file); 
    close(fd_out_file);

    /* step 5: generate trace in error case */
    if(u32ErrorCode != OSAL_E_NOERROR) 
    {
        tU32 slen = 14;
        tU8 au8Buf[MAX_TRACE_SIZE]={0};
        
        OSAL_M_INSERT_T8 (&au8Buf[0], EN_FS_DISPATCHER | EN_IOCOPYFILE_RESULT);
        OSAL_M_INSERT_T32(&au8Buf[1], u32ErrorCode);
        if(strsrc) 
        {
            slen = u32TraceBuf_Update(&au8Buf[5], strsrc, MAX_TRACE_SIZE - 5 ); //Trace buffer update according to the buffer name length
        }
        LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_ERRORS, au8Buf, slen + 5);
    }

    return (tS32)u32ErrorCode;
}



/******************************************************************************
 * FUNCTION:        u32CopyDirFiles
 *
 * DESCRIPTION:    This function copies recursively a directory from source to 
 *                     destination
 *
 * PARAMETER:
 *
 *          char*    source directory 
 *          char*    destination directory
 *
 * RETURNVALUE:    Number of copied files
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 02.10.07  |    Initial revision                            | CM-DI/CF32 MRK2HI
 * 04.03.13  | To provide fix to NIKAI-4347 , Update  |
 *              | the path name buffer                         | SWM2KOR  
 ******************************************************************************/
tU32 u32CopyDirFiles( const char* cSrcDir, const char* cDestDir )
{
    tU32 u32ErrorCode = OSAL_E_NOERROR; 
    struct dirent* pDirEnt = NULL;
    DIR* dir;
    char* pcNameBufferSrc  = (char*)malloc(OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET); //To update the size for handling App folder name upto 256 apart osal Mapping
    char* pcNameBufferDest = (char*)malloc(OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET); //To update the size for handling App folder name upto 256 apart osal Mapping

    if((cSrcDir) && (cDestDir))
    {
        if(pcNameBufferSrc && pcNameBufferDest)
        {
                dir = opendir( cSrcDir );
                if(dir)
                {
                    pDirEnt = readdir( (DIR*)dir );  // return NULL = error
                    while( pDirEnt != NULL )
                    {
                            if(pDirEnt->d_name[0] != '.')
                            {
                                memset( pcNameBufferSrc,  '\0',OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                                memset( pcNameBufferDest, '\0',OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);

                                strcat( pcNameBufferSrc, cSrcDir );
                                strcat( pcNameBufferSrc, "/" );
                                strcat( pcNameBufferSrc, pDirEnt->d_name );
                  
                                strcat( pcNameBufferDest, cDestDir );
                                strcat( pcNameBufferDest, "/" );
                                strcat( pcNameBufferDest, pDirEnt->d_name );

                                /* remove file if existing */
                                remove( (char*)pcNameBufferDest );
                                if( s32FileCopy( pcNameBufferSrc, pcNameBufferDest ) == (tS32)OSAL_E_NOACCESS )
                                {
                                    /* it's an subdirectory */
                                    mkdir( pcNameBufferDest,OSAL_FILE_ACCESS_RIGTHS);
                                    /* try to copy subdirectory */
                                    u32ErrorCode = u32CopyDirFiles( pcNameBufferSrc, pcNameBufferDest );
                                }
                        }
                        pDirEnt = readdir( dir );
                    }//while
                    closedir(dir);
                }
                else
                {
                    u32ErrorCode = u32ConvertErrorCore(errno);
                }
        }
        else
        {
            u32ErrorCode = OSAL_E_NOSPACE;
        }
    }
    else
    {
        u32ErrorCode = OSAL_E_INVALIDVALUE;
    }

    free(pcNameBufferSrc);
    free(pcNameBufferDest);

    if(u32ErrorCode != OSAL_E_NOERROR) 
    {
        tU32 slen = 14;
        tU8 au8Buf[MAX_TRACE_SIZE]={0};
        
        OSAL_M_INSERT_T8 (&au8Buf[0], EN_FS_DISPATCHER | EN_IOCOPYDIR_RESULT);
        OSAL_M_INSERT_T32(&au8Buf[1], u32ErrorCode);
        if(cSrcDir) 
        {
            slen = u32TraceBuf_Update(&au8Buf[5], cSrcDir, MAX_TRACE_SIZE - 5 );  //Trace buffer update according to the buffer name length
        }
        LLD_vTrace( TR_COMP_OSALIO, TR_LEVEL_ERRORS, &au8Buf[0], slen + 5 );
    }
    return u32ErrorCode;
}


/******************************************************************************
 * FUNCTION:        u32ReadExtDir
 *
 * DESCRIPTION:    This function removes recursively a directory with content 
 *                     from the file system.
 *
 * PARAMETER:
 *
 *          u32fd    descriptor of file;
 *
 * RETURNVALUE:    u32ErrorCode
 *                      > OSAL_E_NOERROR if everything goes right
 *                      > OSAL_E_NOTSUPPORTED if not supported by driver 
 *                      > OSAL_E_UNKNOWN otherwise.
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 02.10.07  |    Initial revision                            | CM-DI/CF32 MRK2HI
 * 27.03.13  | To intialize the s8Name before memcopy | SWM2KOR
 * 04.03.13  | To provide fix to NIKAI-4347 , Update  |
 *              | the path name buffer                         | SWM2KOR  
 ******************************************************************************/
tU32 u32ReadExtDir(const char* cDirPath , OSAL_trIOCtrlExtDir *vextdir, tS32 s32Len)
{
    tU32 u32ErrorCode = OSAL_E_NOERROR; 
    tU32 u32Entry = 0;
    struct dirent* pDirEnt = NULL;
    DIR* dir = NULL;
    char cBuffer[OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET] = {'\0'}; //To update the size for handling App folder name upto 256 apart osal Mapping
    struct stat rStat;

    memset( &cBuffer[0], 0, sizeof(cBuffer) );

    if(vextdir->u32NbrOfEntries ) 
    {
        dir = opendir(cDirPath);
        if(!dir)
        {
            u32ErrorCode = u32ConvertErrorCore(errno);
        }
        else
        {
            /* skip entries which are already read */
            if(vextdir->s32Cookie > 0)
            {
                seekdir((DIR*)dir,vextdir->s32Cookie);
            }

            pDirEnt = readdir((DIR*)dir);
            while(pDirEnt)
            {         
               /* prove for unecessary entries "." & ".." but accept .xyz */
                if(((pDirEnt->d_name[0] == '.' )&&(pDirEnt->d_name[1] == '.'))
                 ||((pDirEnt->d_name[0] == '.' )&&(pDirEnt->d_name[1] == '\0')))
                {
                }
                else
                {
                    /* copy name into list element */
                    // memset to zero, if memeset is not used,twice or more usage of same file path or vextdir variable the for IOCONTROL will result in some unmwanted symbols
                    memset(vextdir->pDirent[u32Entry].s8Name,0,sizeof(vextdir->pDirent[u32Entry].s8Name));
                    strncpy( (char*)vextdir->pDirent[u32Entry].s8Name, pDirEnt->d_name, strlen(pDirEnt->d_name) );
      /*            if( pDirEnt->d_type == DT_DIR )
                    {
                            vextdir->pDirent[u32Entry].u32FileSize = 0;
                            vextdir->pDirent[u32Entry].enFileFlags = OSAL_EN_DIRECTORY;                        
                    }
                    else*/
                    {
                       cBuffer[0]= '\0';
                       strncpy(&cBuffer[0], cDirPath,(size_t)s32Len ); 
                       if( cBuffer[s32Len] != '/' )  // add a missing slash
                       {
                           cBuffer[s32Len] = '/';
                       }
                       strcat( cBuffer, pDirEnt->d_name );
//                            vextdir->pDirent[u32Entry].enFileFlags = OSAL_EN_FILE;
//                            vextdir->pDirent[u32Entry].u32FileSize = 1024; /* dummy value */
    /*
    dev_t      st_dev      ID of device containing file
    ino_t      st_ino      file serial number
    mode_t     st_mode     mode of file (see below)
    nlink_t    st_nlink    number of links to the file
    uid_t      st_uid      user ID of file
    gid_t      st_gid      group ID of file
    dev_t      st_rdev     device ID (if file is character or block special)
    off_t      st_size     file size in bytes (if file is a regular file)
    time_t     st_atime    time of last access
    time_t     st_mtime    time of last data modification
    time_t     st_ctime    time of last status change
    blksize_t st_blksize a filesystem-specific preferred I/O block size for
                                this object.  In some filesystem types, this may
                                vary from file to file
    blkcnt_t  st_blocks  number of blocks allocated for this object
    */
                       if(stat(cBuffer, &rStat) != -1)
                       {
                          if(rStat.st_mode & S_IFDIR) // check again
                          {
                             vextdir->pDirent[u32Entry].u32FileSize = 0;
                             vextdir->pDirent[u32Entry].enFileFlags = OSAL_EN_DIRECTORY;
                          }
                          else if( S_ISREG( rStat.st_mode ) ) // regular file?
                          { 
                             vextdir->pDirent[u32Entry].u32FileSize = (tU32)rStat.st_size;
                             vextdir->pDirent[u32Entry].enFileFlags = OSAL_EN_FILE;
                          } 
                          else
                          {
                             vextdir->pDirent[u32Entry].enFileFlags = (OSAL_tenFileFlags)(OSAL_EN_DIRECTORY + 1);  // invalid
                          }
                       }
                    }
                    memset(&cBuffer[0],0,sizeof(cBuffer));
                    u32Entry++;
                }//if(pDirEnt->d_name[0] != '.')
                if( u32Entry < vextdir->u32NbrOfEntries )
                {
                    pDirEnt = readdir(dir);
		        }
                else
                {
                   if( u32Entry == 0 ) 
                   {
                      u32ErrorCode    = OSAL_E_DOESNOTEXIST;
                   }
                   break;
                }
            } // end while

            /* remember read entries */
            vextdir->s32Cookie = telldir((DIR*)dir);

  //         rewinddir(dir);
            closedir(dir);
        }//if(pDirEnt) 
    }//if(vextdir->u32NbrOfEntries ) 
    /* Store read entries */
    vextdir->u32NbrEntries = u32Entry;
    return u32ErrorCode;
}


/******************************************************************************
 * FUNCTION:        u32ReadExtDir
 *
 * DESCRIPTION:    This function removes recursively a directory with content 
 *                     from the file system.
 *
 * PARAMETER:
 *
 *          u32fd    descriptor of file;
 *
 * RETURNVALUE:    u32ErrorCode
 *                      > OSAL_E_NOERROR if everything goes right
 *                      > OSAL_E_NOTSUPPORTED if not supported by driver 
 *                      > OSAL_E_UNKNOWN otherwise.
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 02.10.07  |    Initial revision                            | CM-DI/CF32 MRK2HI
 * 27.03.13  | To intialize the s8Name before memcopy | SWM2KOR
 * 04.03.13  | To provide fix to NIKAI-4347 , Update  |
 *              | the path name buffer                         | SWM2KOR 
 * 22.04.13  | Cookies is handled to skip the entries which are aleady read  | krs4cob
 ******************************************************************************/
tU32 u32ReadExtDir2(const char* cDirPath , OSAL_trIOCtrlExt2Dir *vextdir, tS32 s32Len)
{ 
    tU32 u32ErrorCode = OSAL_E_NOERROR; 
    tU32 u32Entry = 0;
    struct dirent* pDirEnt = NULL;
    DIR* dir = NULL;
    char cBuffer[OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET] = {'\0'}; //To update the size for handling App folder name upto 256 apart osal Mapping
    struct stat rStat;
    struct tm *newtime;

    memset( &cBuffer[0], 0, sizeof(cBuffer) );

    if(vextdir->u32NbrOfEntries ) 
    {
        dir = opendir(cDirPath);
        if(!dir)
        {
            u32ErrorCode = u32ConvertErrorCore(errno);
        }
        else
        {
            /* skip entries which are already read */
            if(vextdir->s32Cookie > 0)
            {
                seekdir((DIR*)dir,vextdir->s32Cookie);
            }

            pDirEnt = readdir((DIR*)dir);
            while(pDirEnt)
            {         
                /* prove for unecessary entries "." & ".." but accept .xyz */
                if(((pDirEnt->d_name[0] == '.' )&&(pDirEnt->d_name[1] == '.'))
                 ||((pDirEnt->d_name[0] == '.' )&&(pDirEnt->d_name[1] == '\0')))
                {
                }
                else
                {
                     /* copy name into list element */
                     // memset to zero, if memeset is not used,twice or more usage of same file path or vextdir variable the for IOCONTROL will result in some unmwanted symbols
                     memset(vextdir->pDirent[u32Entry].s8Name,0,sizeof(vextdir->pDirent[u32Entry].s8Name));
                     strncpy( (char*)vextdir->pDirent[u32Entry].s8Name, pDirEnt->d_name, strlen(pDirEnt->d_name) );
      /*             if( pDirEnt->d_type == DT_DIR )
                     {
                          vextdir->pDirent[u32Entry].u32FileSize = 0;
                            vextdir->pDirent[u32Entry].enFileFlags = OSAL_EN_DIRECTORY;                        
                     }
                     else*/
                     {
                            cBuffer[0]= '\0';
                            strncpy(&cBuffer[0], cDirPath,(size_t)s32Len ); 
                            if( cBuffer[s32Len] != '/' )  // add a missing slash
                            {
                                cBuffer[s32Len] = '/';
                            }
                            strcat( cBuffer, pDirEnt->d_name );
//                            vextdir->pDirent[u32Entry].enFileFlags = OSAL_EN_FILE;
//                            vextdir->pDirent[u32Entry].u32FileSize = 1024; /* dummy value */
    /*
    dev_t      st_dev      ID of device containing file
    ino_t      st_ino      file serial number
    mode_t     st_mode     mode of file (see below)
    nlink_t    st_nlink    number of links to the file
    uid_t      st_uid      user ID of file
    gid_t      st_gid      group ID of file
    dev_t      st_rdev     device ID (if file is character or block special)
    off_t      st_size     file size in bytes (if file is a regular file)
    time_t     st_atime    time of last access
    time_t     st_mtime    time of last data modification
    time_t     st_ctime    time of last status change
    blksize_t st_blksize a filesystem-specific preferred I/O block size for
                                this object.  In some filesystem types, this may
                                vary from file to file
    blkcnt_t  st_blocks  number of blocks allocated for this object
    */
                            if(stat(cBuffer, &rStat) != -1)
                            {
                                if(rStat.st_mode & S_IFDIR) // check again
                                {
                                    vextdir->pDirent[u32Entry].u32FileSize = 0;
                                    vextdir->pDirent[u32Entry].enFileFlags = OSAL_EN_DIRECTORY;
                                }
                                else if( S_ISREG( rStat.st_mode ) ) // regular file?
                                { 
                                    vextdir->pDirent[u32Entry].u32FileSize = (tU32)rStat.st_size;
                                    vextdir->pDirent[u32Entry].enFileFlags = OSAL_EN_FILE;
                                } 
                                else
                                {
                                    vextdir->pDirent[u32Entry].enFileFlags = (OSAL_tenFileFlags)(OSAL_EN_DIRECTORY + 1);  // invalid
                                }
                            }
                            vextdir->pDirent[u32Entry].u8FileAttributes  = (tU8)rStat.st_mode; 
                            newtime = localtime( &rStat.st_mtime ); /* Convert to local time. */
                            vextdir->pDirent[u32Entry].trFileDateAndTime.u8Second = (tU8)newtime->tm_sec;
                            vextdir->pDirent[u32Entry].trFileDateAndTime.u8Minute = (tU8)newtime->tm_min;
                            vextdir->pDirent[u32Entry].trFileDateAndTime.u8Hour    = (tU8)newtime->tm_hour;
                            vextdir->pDirent[u32Entry].trFileDateAndTime.u8Day     = (tU8)newtime->tm_mday;
                            vextdir->pDirent[u32Entry].trFileDateAndTime.u8Month  = (tU8)newtime->tm_mon  + 1;
                            vextdir->pDirent[u32Entry].trFileDateAndTime.s16Year  = (tS16)newtime->tm_year+ 1900;                                    
                     }
                     memset(&cBuffer[0],0,sizeof(cBuffer));
                     u32Entry++;
                }//if(pDirEnt->d_name[0] != '.')
                if( u32Entry < vextdir->u32NbrOfEntries )
                {
                    pDirEnt = readdir(dir);
                }
                else
                {
                   if( u32Entry == 0 ) 
                   {
                      u32ErrorCode    = OSAL_E_DOESNOTEXIST;
                   }
                   break;
                }
            } // end while

            /* remember read entries */
            vextdir->s32Cookie = telldir((DIR*)dir);

  //         rewinddir(dir);
            closedir(dir);
        }//if(pDirEnt) 
    }//if(vextdir->u32NbrOfEntries ) 
    /* Store read entries */
    vextdir->u32NbrEntries = u32Entry;
    return u32ErrorCode;
}




tBool bAccessAllowed(tS32 s32Fun, OSAL_tenAccess enAccess)
{
    tBool bRet = TRUE;
    
    switch(s32Fun)
    {
        case OSAL_C_S32_IOCTRL_FIOWHERE:
        case OSAL_C_S32_IOCTRL_FIOSEEK:
        case OSAL_C_S32_IOCTRL_FIOTOTALSIZE:
        case OSAL_C_S32_IOCTRL_FIOFREESIZE:
        case OSAL_C_S32_IOCTRL_FIONREAD:
        case OSAL_C_S32_IOCTRL_FIOREADDIR:
        case OSAL_C_S32_IOCTRL_FIOREADDIREXT:
        case OSAL_C_S32_IOCTRL_FIOREADDIREXT2:
        case OSAL_C_S32_IOCTRL_FIOOPENDIR:        
        case OSAL_C_S32_IOCTRL_FIORMRECURSIVE_CANCEL:
        case OSAL_C_S32_IOCTRL_FIOPREPAREEJECT:
        case OSAL_C_S32_IOCTRL_FIOGETMOUNTPOINTINFO:
        case OSAL_C_S32_IOCTRL_FIOSETMOUNTPOINTACCESS:
        case OSAL_C_S32_IOCTRL_FIOGETMOUNTPOINTACCESS:
        case OSAL_C_S32_IOCTRL_VERSION:
        case OSAL_C_S32_IOCTRL_IPOD_ACTIVATE_USB_VBUS:             
        case OSAL_C_S32_IOCTRL_IPOD_DEACTIVATE_USB_VBUS:          
        case OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO:
        case OSAL_C_S32_IOCTRL_USBH_GETHUBDEVICEINFO:
        case OSAL_C_S32_IOCTRL_FIOCHKDSK:         
        case OSAL_C_S32_IOCTRL_FIOSETSTREAMMODE:
        case OSAL_C_S32_IOCTRL_FFS_SAVENOW:
            if(OSAL_EN_ACCESS_GET_PERMISSION(enAccess) < OSAL_EN_READONLY)
            {
                bRet = FALSE;
            }
            break;  
        case OSAL_C_S32_IOCTRL_FIODISKFORMAT:
        case OSAL_C_S32_IOCTRL_FIOMKDIR:
        case OSAL_C_S32_IOCTRL_FIORMDIR:
        case OSAL_C_S32_IOCTRL_FIORENAME:
        case OSAL_C_S32_IOCTRL_FIORMRECURSIVE:
        case OSAL_C_S32_IOCTRL_FIOCOPYDIR:
            if(OSAL_EN_ACCESS_GET_PERMISSION(enAccess) < OSAL_EN_WRITEONLY)
            {            
                bRet = FALSE;
            }
            break;
        default:
            break;
    }
    return(bRet);
}


tString sGetCtrlString(tS32 s32Control)
{
    tString szCtrl;
    switch(s32Control)
    {
        case OSAL_C_S32_IOCTRL_FIOWHERE:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOWHERE";
             break;
        case OSAL_C_S32_IOCTRL_FIOSEEK:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOSEEK";
             break;
        case OSAL_C_S32_IOCTRL_FIOTOTALSIZE:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOTOTALSIZE";
             break;
        case OSAL_C_S32_IOCTRL_FIOFREESIZE:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOFREESIZE";
             break;
        case OSAL_C_S32_IOCTRL_FIONREAD:
              szCtrl = "OSAL_C_S32_IOCTRL_FIONREAD";
             break;
        case OSAL_C_S32_IOCTRL_FIOREADDIR:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOREADDIR";
             break;
        case OSAL_C_S32_IOCTRL_FIOREADDIREXT:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOREADDIREXT";
             break;
        case OSAL_C_S32_IOCTRL_FIOREADDIREXT2:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOREADDIREXT2";
             break;
        case OSAL_C_S32_IOCTRL_FIOOPENDIR:        
              szCtrl = "OSAL_C_S32_IOCTRL_FIOOPENDIR";
             break;
        case OSAL_C_S32_IOCTRL_FIORMRECURSIVE_CANCEL:
              szCtrl = "OSAL_C_S32_IOCTRL_FIORMRECURSIVE_CANCEL";
             break;
        case OSAL_C_S32_IOCTRL_FIOPREPAREEJECT:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOPREPAREEJECT";
             break;
        case OSAL_C_S32_IOCTRL_FIOGETMOUNTPOINTINFO:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOGETMOUNTPOINTINFO";
             break;
        case OSAL_C_S32_IOCTRL_FIOSETMOUNTPOINTACCESS:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOSETMOUNTPOINTACCESS";
             break;
        case OSAL_C_S32_IOCTRL_FIOGETMOUNTPOINTACCESS:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOGETMOUNTPOINTACCESS";
             break;
        case OSAL_C_S32_IOCTRL_VERSION:
              szCtrl = "OSAL_C_S32_IOCTRL_VERSION";
             break;
        case OSAL_C_S32_IOCTRL_IPOD_ACTIVATE_USB_VBUS:             
              szCtrl = "OSAL_C_S32_IOCTRL_IPOD_ACTIVATE_USB_VBUS";
             break;
        case OSAL_C_S32_IOCTRL_IPOD_DEACTIVATE_USB_VBUS:          
              szCtrl = "OSAL_C_S32_IOCTRL_IPOD_DEACTIVATE_USB_VBUS";
             break;
        case OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO:
              szCtrl = "OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO";
             break;
        case OSAL_C_S32_IOCTRL_USBH_GETHUBDEVICEINFO:
              szCtrl = "OSAL_C_S32_IOCTRL_USBH_GETHUBDEVICEINFO";
             break;
        case OSAL_C_S32_IOCTRL_FIOCHKDSK:         
              szCtrl = "OSAL_C_S32_IOCTRL_FIOCHKDSK";
             break;
        case OSAL_C_S32_IOCTRL_FIOSETSTREAMMODE:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOSETSTREAMMODE";
             break;
        case OSAL_C_S32_IOCTRL_FFS_SAVENOW:
                szCtrl = "OSAL_C_S32_IOCTRL_FFS_SAVENOW";
             break;
        case OSAL_C_S32_IOCTRL_FIODISKFORMAT:
              szCtrl = "OSAL_C_S32_IOCTRL_FIODISKFORMAT";
             break;
        case OSAL_C_S32_IOCTRL_FIOMKDIR:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOMKDIR";
             break;
        case OSAL_C_S32_IOCTRL_FIORMDIR:
              szCtrl = "OSAL_C_S32_IOCTRL_FIORMDIR";
             break;
        case OSAL_C_S32_IOCTRL_FIORENAME:
              szCtrl = "OSAL_C_S32_IOCTRL_FIORENAME";
             break;
        case OSAL_C_S32_IOCTRL_FIORMRECURSIVE:
              szCtrl = "OSAL_C_S32_IOCTRL_FIORMRECURSIVE";
             break;
        case OSAL_C_S32_IOCTRL_FIOCOPYDIR:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOCOPYDIR";
             break;
        case OSAL_C_S32_IOCTRL_FIOFLUSH:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOWHERE";
             break;
        case OSAL_C_S32_IOCTRL_CARD_STATE:
              szCtrl = "OSAL_C_S32_IOCTRL_CARD_STATE";
             break;
        case OSAL_C_S32_IOCTRL_FIOGET_REAL_PATH:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOGET_REAL_PATH";
             break;
        case OSAL_C_S32_IOCTRL_FIOGET_CID:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOGET_CID";
             break;
        case OSAL_C_S32_IOCTRL_FIOTRIGGER_SIGN_VERIFY:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOTRIGGER_SIGN_VERIFY";
             break;
        case OSAL_C_S32_IOCTRL_FIOCRYPT_VERIFY_STATUS:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOCRYPT_VERIFY_STATUS";
             break;
        case OSAL_C_S32_IOCTRL_FIOGET_SIGNATURE_TYPE:
              szCtrl = "OSAL_C_S32_IOCTRL_FIOGET_SIGNATURE_TYPE";
             break;
        default:
              szCtrl = "Undefined";
             break;
    }
    return szCtrl;
}

void vTraceIoCtrl(tU32 u32ErrorCode, tS32 s32Result, tCString coszDrive, tU32 u32fd, tS32 s32Control)
{
    tU32 u32Level = TR_LEVEL_DATA;
#ifdef SHORT_TRACE_OUTPUT
    tU8  au8Buf[ MAX_TRACE_SIZE ];
    tS32 slen;

    OSAL_M_INSERT_T8( &au8Buf[0], EN_FS_DISPATCHER | EN_IOCONTROL_RESULT );
    if( u32ErrorCode == OSAL_E_NOERROR )
    {
        OSAL_M_INSERT_T32( &au8Buf[1], (tU32)s32Result );
    }
    else
    {
        OSAL_M_INSERT_T32( &au8Buf[1], u32ErrorCode );
    }
    OSAL_M_INSERT_T8(  &au8Buf[5], s32Control );
    OSAL_M_INSERT_T32( &au8Buf[6], u32fd );
    
    slen = (tS32)strlen(coszDrive);
    if( slen > (MAX_TRACE_SIZE - 11) )
    {
        slen = MAX_TRACE_SIZE - 11;
    }
    memcpy ( &au8Buf[10], coszDrive, (tU32)slen + 1);
    au8Buf[MAX_TRACE_SIZE - 1] = 0;
    
    switch(s32Control)
    {
    case EN_CTRL_READDIR:
          if(u32ErrorCode == OSAL_E_NOTSUPPORTED)u32Level = TR_LEVEL_ERRORS;/* normal when end of dir is reached */
         break;
    case EN_CTRL_FIOMKDIR:
          if(u32ErrorCode == OSAL_E_ALREADYEXISTS)u32Level = TR_LEVEL_ERRORS;/* often happens always in startup */
         break;
    case EN_CTRL_FIOSEEK: /* ugly way from SDS to recognize directories */
         if(u32ErrorCode == OSAL_E_IOERROR)u32Level = TR_LEVEL_ERRORS;
         break;
    case EN_CTRL_FIOWHERE:/* ugly way from SDS to recognize directories */
         if(u32ErrorCode == OSAL_E_IOERROR)u32Level = TR_LEVEL_ERRORS;
         break;
    default:
          u32Level = TR_LEVEL_FATAL;
         break;
    }
    LLD_vTrace( TR_COMP_OSALIO, u32Level, au8Buf, slen + 11 );

#else
    tU8 au8Buf[MAX_TRACE_SIZE+1]={0};
    tString szCtrl = sGetCtrlString(s32Control);
    au8Buf[0] = OSAL_STRING_OUT;
    if(u32ErrorCode == OSAL_E_NOERROR)
    {
        snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOCONTROL: Result=0x%x Ctrl:%s  h=0x%" PRIxPTR "  %s",
                    s32Result,szCtrl,(uintptr_t)u32fd,coszDrive);
    }
    else
    {
        snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOCONTROL: Result=0x%x Ctrl:%s  h=0x%" PRIxPTR "  %s",
                    (unsigned int)u32ErrorCode,szCtrl,(uintptr_t)u32fd,coszDrive);
    }
    switch(s32Control)
    {
    case EN_CTRL_READDIR:
          if(u32ErrorCode == OSAL_E_NOTSUPPORTED)u32Level = TR_LEVEL_ERRORS;
         break;
    case EN_CTRL_FIOMKDIR:
          if(u32ErrorCode == OSAL_E_ALREADYEXISTS)u32Level = TR_LEVEL_ERRORS;
         break;
    default:
          u32Level = TR_LEVEL_FATAL;
         break;
    }
    LLD_vTrace(TR_COMP_OSALIO, u32Level, &au8Buf[0], strlen((char*)&au8Buf[1])+1);
#endif

}



tBool bMapFileSystems( char* pRealPath, const char* pOrigPath )
{
    tS32  s32NewDevId;
    tS32  s32Id;
    tBool bRet = TRUE;
    
    if( (!pRealPath) || (!pOrigPath) )
    {
        bRet = FALSE;
    }
    else
    {        
        s32Id = s32OSAL_get_device_id_and_filename_by_Path( (tString *)(&pOrigPath) );
        if( pOrigPath == NULL ) // is set to NULL, if original path is the device, only
        {
            pOrigPath = "";
        }
        if( s32Id < OSAL_EN_DEVID_DVD )  // all devices < DVD are internal devices!
        {
            s32NewDevId = s32Id;
        }
        else if(s32Id == OSAL_EN_DEVID_DVD)
        {
            s32NewDevId = OSAL_EN_DEVID_DVD; 
        }
        else
        {
            s32NewDevId = s32Id;
        }
        if( s32NewDevId != OSAL_ERROR )
        {
            if( s32GetRealPath( pOrigPath, pRealPath, s32NewDevId ) == OSAL_ERROR )
            { 
                bRet = FALSE;
            }
        }
        else
        {
            bRet = FALSE;
        }
    }
    return bRet;
}


extern crypt_ctrl         pSDCardInfo;
extern crypt_ctrl         pReadCid;
extern crypt_sign         pVerify_signaturefile;
extern crypt_sign         pGet_signature_verify_status;
extern crypt_sign         pGet_signaturefile_type;



tU32 u32CardIOControl( uintptr_t u32fd, tU32 fun, uintptr_t arg, tS32 s32DeviceId )
{
    tU32 u32ErrorCode    = OSAL_E_NOERROR;
    tCString coszDrive = Drive[ s32DeviceId ];
    tS32 s32count;
    tS32 s32DeviceIdx = -1;
#if defined (GEN3X86)
    char fPath[OSAL_C_U32_MAX_PATHLENGTH]={0};
    FILE *fp;
    tU8 i;
#endif

    if(pSDCardInfo == NULL)
    {
        if(u32LoadSharedObject(pSDCardInfo,pOsalData->rLibrary[EN_CRYPTMODUL].cLibraryNames) != OSAL_E_NOERROR)
        {
             NORMAL_M_ASSERT_ALWAYS();
             return((tU32)OSAL_E_NOTSUPPORTED);
        }
    }

    tS32 s32Val = (tS32)fun;
    switch( s32Val )
    {
        /* -------------------------------------------------------- */
        /*                                Card device specific                  */
        /* -------------------------------------------------------- */ 
        case OSAL_C_S32_IOCTRL_CARD_STATE:            /* Get information about the inserted card */
        {
          u32ErrorCode = OSAL_E_NOERROR;
          OSAL_trIOCtrlCardState* prIOCtrlCardState = (OSAL_trIOCtrlCardState*)arg;
          //search for the device for which the OSAL_C_S32_IOCTRL_CARD_STATE is being called
            if((prIOCtrlCardState->szDevicName[0])!= 0)
            {
                for(s32count=0;s32count<=PRM_MAX_USB_DEVICES;s32count++)
                {
                     if( (pOsalData->u8DeviceUUID[s32count][0] != 0) && (strcmp((char*)&pOsalData->u8DeviceUUID[s32count][0],&prIOCtrlCardState->szDevicName[0])==0) )
                     {
#if !defined (GEN3X86)
                          //update s32DeviceIdx with the device index for which the ioctl is being called
                          s32DeviceIdx = s32count; 
                          u32ErrorCode = pSDCardInfo( (void*)prIOCtrlCardState, s32DeviceIdx);
#else
                          /*Checking for file named SDCARD to distinguish between USB and SDCARD connected to LSIM*/
                          strncpy(fPath,"/media/",7);
                          strncat(fPath,&prIOCtrlCardState->szDevicName[0],strlen(prIOCtrlCardState->szDevicName));
                          strncat(fPath,"/SDCARD",7);

                          fp = fopen(fPath,"r");
                          if(fp == NULL)
                          {
                                TraceString("LSIM :Device is not SDCARD!!!");
                                u32ErrorCode = OSAL_E_NOTSUPPORTED;
                          }
                          else
                          {
                                /* Dummy values returned for this ioctl if the Device is SDCARD */
                                prIOCtrlCardState->bHW_WriteProtected = 0;
                                prIOCtrlCardState->bSW_WriteProtected = 0;
                                prIOCtrlCardState->bMounted = 1;
                                prIOCtrlCardState->u16UncleanUnmountCnt = 0;
                                prIOCtrlCardState->u16UncleanWriteCnt = 0;
                                prIOCtrlCardState->u16UncleanReadCnt = 0;
                                prIOCtrlCardState->u8ManufactureId = 2;
                                prIOCtrlCardState->u32SerialNumber = 0XFFFFFFFF;
                                prIOCtrlCardState->u8SDCardSpecVersion = 0;
                                for(i = 0;i < 16; i++)
                                {
                                     prIOCtrlCardState->u8CIDRegister[i] = i;
                                }
                                prIOCtrlCardState->u64CardSize = OSAL_U64_LSIM_CARD_SIZE; 
                                fclose(fp);
                          }
#endif
                          break;
                     }
                
                }
                if(s32count > PRM_MAX_USB_DEVICES )
                {
                     u32ErrorCode = OSAL_E_DOESNOTEXIST;  //the device is unmounted/ejected.
                }
          }
          else
          {
                u32ErrorCode = OSAL_E_INVALIDVALUE; //application didnt pass valid UUID.
          }
          //if( pOsalData->s32UsbIdxForCard != -1 )
          //{
          // todo 0: "/dev/sda"; 1: "/dev/sdb"
          // todo and transfer the string to the fdcrypt function
          //}

            /*Previously the card is via USB hub only,  but now we can get the card
              info via USB hub interface as well as direct SD card interface.
              So, to make a general function to get the card info, the function name has changed*/
            
            if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
            {
                vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIOCARD_STATE );
            }
            break;
        }
        case OSAL_C_S32_IOCTRL_FIOGET_CID:
        {
            /* This IO Control can return following status */
            /* :--- IO Control status --:------------------------------------------- */
            /* : OSAL_E_NOERROR            : CID Read successfully                             */
            /* : OSAL_E_IOERROR            : Error while reading CID from the device     */
            /* : OSAL_E_INVALIDVALUE     : Invalid IO Control argument                     */

            tPU8 pu8HWCID = (tPU8) arg;
            u32ErrorCode = OSAL_E_NOTSUPPORTED;
          
             /* Read CID is only supported for /dev/card and /dev/card2 device */
            if(( s32DeviceId == OSAL_EN_DEVID_CRYPT_CARD)||(s32DeviceId == (tS32)OSAL_EN_DEVID_DEV_MEDIA))
             {
                /* Check whether the argument is valid */
                if(pu8HWCID)
                {
                     /* Get the CID from device */
                     /* pass pOsalData->s32UsbIdxForCard as one of the argument to function pReadCid */
                     u32ErrorCode = pReadCid( (void*)pu8HWCID, pOsalData->s32UsbIdxForCard  );
                }
                else
                {
                    u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
            }
            if((pOsalData->bTraceAll)||(u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
            {
                 vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIOGET_CID );
            }
            break;
        }
        case OSAL_C_S32_IOCTRL_TRIGGER_SIGN_VERIFY:
        case OSAL_C_S32_IOCTRL_FIOTRIGGER_SIGN_VERIFY:
        {
            /* This IO Control can return following status */
            /* :---- IO Control status ---:---- Verification status ----:------------------------------------- */
            /* : OSAL_E_NOERROR              : OSAL_EN_SIGN_VERIFY_PASSED  : Certificate verification is success  */
            /* : OSAL_E_DECRYPTION_FAILED : OSAL_EN_SIGN_VERIFY_FAILED  : Certificate verification is failed    */
            /* : OSAL_E_NOTSUPPORTED        : Not set                            : The device is not supported             */
            /* : OSAL_E_INVALIDVALUE        : Not set                            : Invalid IO Control argument             */
            /* : OSAL_E_NOSPACE              : OSAL_EN_SIGN_VERIFY_FAILED  : memory Allocation failed                 */
            /* : OSAL_E_IOERROR              : OSAL_EN_SIGN_VERIFY_FAILED  : Certificate file not present            */

            OSAL_tenSignVerifyStatus enStatus = OSAL_EN_SIGN_VERIFY_FAILED;
            OSAL_trTrigSigVerifyArg *prTrigSigVerifyArg;
            fd_crypt_tenError  enVerifyStat;
            u32ErrorCode = OSAL_E_NOTSUPPORTED;
            tCString coszDevName = OSAL_NULL;
            tString szCertPath = OSAL_NULL;
            tS32 s32CertPathLen;

            /* Following code is done temporarily for adapting to fd_crypt changes */
            /* Get the Device Name */
            if(s32DeviceId == OSAL_EN_DEVID_CRYPT_CARD)
                coszDevName = OSAL_C_STRING_DEVICE_CRYPTCARD;
            else if (s32DeviceId == OSAL_EN_DEVID_CRYPTNAV)
                coszDevName = OSAL_C_STRING_DEVICE_CRYPTNAV;
            else if (s32DeviceId == OSAL_EN_DEVID_CRYPTNAVROOT)
                coszDevName = OSAL_C_STRING_DEVICE_CRYPTNAVROOT;
            else if ( s32DeviceId == OSAL_EN_DEVID_DEV_MEDIA )
            {
                coszDevName = ((OSAL_tfdIO*)u32fd)->szFileName; // ->szFileName (/dev/media/E615-F654) ->szName (E615-F654)
            }

            if(coszDevName)
            {
                prTrigSigVerifyArg = (OSAL_trTrigSigVerifyArg *) arg;

                /* Check the arguments before we continue */
                if(prTrigSigVerifyArg && prTrigSigVerifyArg->coszCertPath)
                {
                    /* Allocate memory for absolute certificate path */
                    s32CertPathLen = OSAL_u32StringLength(coszDevName);
                    s32CertPathLen += OSAL_u32StringLength(prTrigSigVerifyArg->coszCertPath);
                    s32CertPathLen += 2; /* Space for null termination and missing '/'*/

                    szCertPath = OSAL_pvMemoryAllocate(s32CertPathLen);

                    if( szCertPath )
                    {
                        /* Prepare the absolute certificate path */
                        OSAL_szStringCopy(szCertPath, coszDevName);
                        if(*prTrigSigVerifyArg->coszCertPath != '/')
                        {
                          OSAL_szStringConcat(szCertPath, "/");
                        }
                        OSAL_szStringConcat(szCertPath, prTrigSigVerifyArg->coszCertPath);

                        enVerifyStat = (fd_crypt_tenError)pVerify_signaturefile( szCertPath );

                        if(enVerifyStat == FD_CRYPT_SUCCESS)
                        {
                            /* Signature is verified successfully */
                            enStatus = OSAL_EN_SIGN_VERIFY_PASSED;
                            u32ErrorCode  =  OSAL_E_NOERROR;
                        }
                        else if(enVerifyStat == FD_CRYPT_SF_NOT_PRESENT)
                        {
                            /* Certificate file path is not valid */
                            u32ErrorCode  =  OSAL_E_IOERROR;
                        }
                        else if(enVerifyStat == ERROR_FD_CRYPT_NOTSUPPORTED)
                        {
                            /* The device containing the certificate file, is not supported by fd_crypt */
                            u32ErrorCode = OSAL_E_NOTSUPPORTED;
                        }
                        else
                        {
                            /* Signature verification failed */
                            u32ErrorCode  =  OSAL_E_DECRYPTION_FAILED;
                        }

                    }
                    else
                    {
                        u32ErrorCode = OSAL_E_NOSPACE;
                    }
      
                    /* Return the status */
                    prTrigSigVerifyArg->enStatus = enStatus;
                }
                else
                {
                    u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
            }
            else
            {
                u32ErrorCode = OSAL_E_NOTSUPPORTED;
            }
            if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
            {
                 if(szCertPath)
                 {
                      vTraceIoCtrl( u32ErrorCode, enStatus , szCertPath, u32fd, EN_CTRL_FIOTRIGGER_SIGN_VERIFY);
                 }
                 else
                 {
                      vTraceIoCtrl( u32ErrorCode, enStatus , coszDevName, u32fd, EN_CTRL_FIOTRIGGER_SIGN_VERIFY);
                 }
            }
            /* Free the memory allocated for file path */
            if(szCertPath)OSAL_vMemoryFree( szCertPath );

            break;
        }
        case OSAL_C_S32_IOCTRL_CRYPT_VERIFY_STATUS:
        case OSAL_C_S32_IOCTRL_FIOCRYPT_VERIFY_STATUS:
        {
            /* This IO Control can return following status */
            /* :--- IO Control status --:---- Verification status -------:------------------------------------- */
            /* : OSAL_E_NOERROR            : OSAL_EN_SIGN_VERIFY_PASSED      : Certificate verification is success  */
            /* : OSAL_E_NOERROR            : OSAL_EN_SIGN_VERIFY_FAILED      : Certificate verification is failed    */
            /* : OSAL_E_NOERROR            : OSAL_EN_SIGN_VERIFY_INPROGRESS : Certificate verification in progress */
            /* : OSAL_E_NOERROR            : OSAL_EN_SIGN_STATUS_UNKNOWN     : Verification in unknown error state  */
            /* : OSAL_E_NOTSUPPORTED     : Not set                                : The device is not supported             */
            /* : OSAL_E_INVALIDVALUE     : Not set                                : Invalid IO Control argument             */

            OSAL_tenSignVerifyStatus enStatus = OSAL_EN_SIGN_VERIFY_FAILED;
            u32ErrorCode    = OSAL_E_NOTSUPPORTED;
            tCString coszDevName = OSAL_NULL;

            /* Following code is done temporarily for adapting to fd_crypt changes */
            /* Get the Device Name */
            if(s32DeviceId == OSAL_EN_DEVID_CRYPT_CARD)
                coszDevName = OSAL_C_STRING_DEVICE_CRYPTCARD;
            else if (s32DeviceId == OSAL_EN_DEVID_CRYPTNAV)
                coszDevName = OSAL_C_STRING_DEVICE_CRYPTNAV;
            else if (s32DeviceId == OSAL_EN_DEVID_CRYPTNAVROOT)
                coszDevName = OSAL_C_STRING_DEVICE_CRYPTNAVROOT;
            else if ( s32DeviceId == OSAL_EN_DEVID_DEV_MEDIA )
            {
                coszDevName = ((OSAL_tfdIO*)u32fd)->szFileName; // ->szFileName (/dev/media/E615-F654) ->szName (E615-F654)
            }


            if(coszDevName)
            {
                if( arg )
                {
                    enStatus = (OSAL_tenSignVerifyStatus)pGet_signature_verify_status( coszDevName );
                    u32ErrorCode = OSAL_E_NOERROR;
                    *(intptr_t*)arg     = (intptr_t)enStatus;
                }
                else
                {
                    u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
            }
            else
            {
                u32ErrorCode = OSAL_E_NOTSUPPORTED;
            }
            if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
            {
                vTraceIoCtrl( u32ErrorCode, enStatus, coszDrive, u32fd, EN_CTRL_FIOCRYPT_VERIFY_STATUS);    
            }
            break;
        }
        case OSAL_C_S32_IOCTRL_GET_SIGNATURE_TYPE:
        case OSAL_C_S32_IOCTRL_FIOGET_SIGNATURE_TYPE:
        {
            /* This IO Control can return following status */
            /* :---- IO Control status ---:-------- File Type -------------- */
            /* : OSAL_E_NOERROR              : OSAL_EN_SIGN_XML_VIN                 */
            /* : OSAL_E_NOERROR              : OSAL_EN_SIGN_XML_CID                 */
            /* : OSAL_E_NOERROR              : OSAL_EN_SIGN_XML_DID                 */
            /* : OSAL_E_IOERROR              : OSAL_EN_SIGN_TYPE_UNKNOWN          */
            /* : OSAL_E_NOSPACE              : OSAL_EN_SIGN_TYPE_UNKNOWN          */
            /* : OSAL_E_NOTSUPPORTED        : Not set                                  */
            /* : OSAL_E_INVALIDVALUE        : Not set                                  */

            OSAL_tenSignFileType enFileType = OSAL_EN_SIGN_TYPE_UNKNOWN;
            OSAL_trGetSigTypeArg *prGetSigTypeArg;
            tCString coszDevName = OSAL_NULL;
            tString szCertPath = OSAL_NULL;
            tS32 s32CertPathLen;

            /* Following code is done temporarily for adapting to fd_crypt changes */
            /* Get the Device Name */
            if(s32DeviceId == OSAL_EN_DEVID_CRYPT_CARD)
                coszDevName = OSAL_C_STRING_DEVICE_CRYPTCARD;
            else if (s32DeviceId == OSAL_EN_DEVID_CRYPTNAV)
                coszDevName = OSAL_C_STRING_DEVICE_CRYPTNAV;
            else if (s32DeviceId == OSAL_EN_DEVID_CRYPTNAVROOT)
                coszDevName = OSAL_C_STRING_DEVICE_CRYPTNAVROOT;
            else if ( s32DeviceId == OSAL_EN_DEVID_DEV_MEDIA )
            {
                coszDevName = ((OSAL_tfdIO*)u32fd)->szFileName; // ->szFileName (/dev/media/E615-F654) ->szName (E615-F654)
            }

            if(coszDevName)
            {
                /* Check the argument before we continue */
                prGetSigTypeArg = (OSAL_trGetSigTypeArg *) arg;

                /* Check the arguments before we continue */
                if( prGetSigTypeArg && prGetSigTypeArg->coszCertPath )
                {
                    /* Allocate memory for absolute certificate path */
                    s32CertPathLen = OSAL_u32StringLength(coszDevName);
                    s32CertPathLen += OSAL_u32StringLength(prGetSigTypeArg->coszCertPath);
                    s32CertPathLen += 2; /* Space for null termination and missing '/'*/

                    szCertPath = OSAL_pvMemoryAllocate(s32CertPathLen);

                    if( szCertPath )
                    {
                        /* Prepare the absolute certificate path */
                        OSAL_szStringCopy(szCertPath, coszDevName);
                        if(*prGetSigTypeArg->coszCertPath != '/')
                        {
                                  OSAL_szStringConcat(szCertPath, "/");
                        }
                        OSAL_szStringConcat(szCertPath, prGetSigTypeArg->coszCertPath);
                        /* Get the signature type */
                        enFileType = (OSAL_tenSignFileType)pGet_signaturefile_type( szCertPath );

                        if(enFileType != OSAL_EN_SIGN_TYPE_UNKNOWN)
                            u32ErrorCode = OSAL_E_NOERROR;
                        else
                            u32ErrorCode = OSAL_E_IOERROR;

                    }
                    else
                    {
                        u32ErrorCode = OSAL_E_NOSPACE;
                    }

                    /* Pass the file type to application */
                    prGetSigTypeArg->enSigType = enFileType;
                }
                else
                {
                    u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
            }
            else
            {
                u32ErrorCode = OSAL_E_NOTSUPPORTED;
            }
            if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
            {
                 vTraceIoCtrl( u32ErrorCode, enFileType, szCertPath, u32fd, EN_CTRL_FIOGET_SIGNATURE_TYPE);    
            }
            /* Free the memory allocated for file path */
            if(szCertPath)OSAL_vMemoryFree( szCertPath );
           break;
        }
        /* -------------------------------------------------------- */
        /*                                DEFAULT                                    */
        /* -------------------------------------------------------- */ 
        default:
            u32ErrorCode = OSAL_E_NOTSUPPORTED;
            break;
    }
    return u32ErrorCode;
}


/******************************************************************************
 * FUNCTION:        LFS_s32IOControl
 *
 * DESCRIPTION:    This function calls a control function of a drive.
 *                     A parameter can be transferred to this driver dependent
 *                     function. This function implement a function for query
 *                     of the version by specification of OSAL_IOCTRL_VERSION.
 *
 * PARAMETER:
 *
 *          u32fd    descriptor of file;
 *
 * RETURNVALUE:    u32ErrorCode
 *                      > OSAL_E_NOERROR if everything goes right
 *                      > OSAL_E_NOTSUPPORTED if not supported by driver 
 *                      > OSAL_E_UNKNOWN otherwise.
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 26.02.04  |    Initial revision                            | CM-DI/EES4 S.Kropp
 * 04.03.13  | To provide fix to NIKAI-4347 , Update  |
 *              | the path name buffer                         | SWM2KOR  
 ******************************************************************************/

tU32 LFS_u32IOControl( uintptr_t u32fd, tU32 fun, intptr_t arg, tS32 s32DeviceId )
{
    intptr_t s32ReturnValue = OSAL_ERROR;
    tU32 u32ErrorCode    = u32CheckFileHandle(u32fd);
    tS32 s32CurrentPos;
    OSAL_trIOCtrlDeviceSize* trSize;
    tCString coszDrive = Drive[ s32DeviceId ];
    tS32 s32Val = (tS32)fun;

    if(u32ErrorCode == OSAL_E_NOERROR)
    {
    switch( s32Val )
    {
/* ========================================================================= */
/*                                                                                                    */
/*                    Controls generally for all File System Drives                    */
/*                                                                                                    */
/* ========================================================================= */

    /* -------------------------------------------------------- */
    /*                  OSAL_C_S32_IOCTRL_FIOWHERE                        */
    /* -------------------------------------------------------- */
        case OSAL_C_S32_IOCTRL_FIOWHERE:
            {
                off_t off = 0; 
                
                if( (arg) && (((OSAL_tfdIO*)u32fd)->fd != 0) )
                { 
                    //s32ReturnValue = ((OSAL_tfdIO*)u32fd)->offset;
                    s32ReturnValue = lseek( ((OSAL_tfdIO*)u32fd)->fd, off, SEEK_CUR );
                    if( s32ReturnValue != -1 )
                    {
#ifdef FORCE_ARG_32BIT
                        *(tS32*)arg = (tS32)s32ReturnValue;
#else
                        *(intptr_t*)arg = s32ReturnValue;
#endif
                    }
                    else
                    {
                        if(((OSAL_tfdIO*)u32fd)->u32Size == 0xffffffff)
                        {
                            if(telldir((DIR*)((OSAL_tfdIO*)u32fd)->fd) != -1)
                            {
                                 u32ErrorCode = OSAL_E_IOERROR;
                            }
                        }
                        if(u32ErrorCode == OSAL_E_NOERROR)u32ErrorCode = u32ConvertErrorCore( errno );
                    }
                }
                else
                {
                    u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {    
                    vTraceIoCtrl( u32ErrorCode, s32ReturnValue, coszDrive, u32fd, EN_CTRL_FIOWHERE );         
                }
            }
            break;
        
    /* -------------------------------------------------------- */
    /*                  OSAL_C_S32_IOCTRL_FIOSEEK                         */
    /* -------------------------------------------------------- */        
        case OSAL_C_S32_IOCTRL_FIOSEEK:
        /* addional parameter check, because lseek does no parameter check in order to
        support 4GB Files  
        arg < 0 results in -1 OSAL error value OSAL_E_INVALIDVALUE is set
        arg > file size is OK file pointer is set to end of file          */
            if((intptr_t)arg < 0)
            {
                u32ErrorCode    = OSAL_E_INVALIDVALUE;
            }
            else
            {
                if(((OSAL_tfdIO*)u32fd)->fd != 0)
                {
#ifdef FORCE_ARG_32BIT
                    tU32 u32Offset = (tU32)arg;
#else
                    uintptr_t u32Offset = (uintptr_t)arg;
#endif
                    if(u32Offset > ((OSAL_tfdIO*)u32fd)->u32Size)
                    {
                         u32Offset = ((OSAL_tfdIO*)u32fd)->u32Size;
                    }
                    s32ReturnValue = lseek( ((OSAL_tfdIO*)u32fd)->fd, (intptr_t)u32Offset, SEEK_SET );
                    if( s32ReturnValue == -1 )
                    {
                         struct stat tr_stat_data;
                         int n_fstat_ret = fstat(  ((OSAL_tfdIO*)u32fd)->fd, &tr_stat_data );
                         if(n_fstat_ret == 0)
                         {
                             if(S_ISDIR( tr_stat_data.st_mode ))
                             {
                                  u32ErrorCode = OSAL_E_IOERROR;
                             }
                        }
                        if(u32ErrorCode == OSAL_E_NOERROR)u32ErrorCode = u32ConvertErrorCore( errno );
                    }
                    else
                    {
                        ((OSAL_tfdIO*)u32fd)->offset = s32ReturnValue;
                    }
                }
                else
                {
                    u32ErrorCode    = OSAL_E_INVALIDVALUE;
                }
            }
            if((pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
            {    
                vTraceIoCtrl( u32ErrorCode, s32ReturnValue, coszDrive, u32fd, EN_CTRL_FIOSEEK );    
            }
            break;

    /* -------------------------------------------------------- */
    /*                  OSAL_C_S32_IOCTRL_FIOTOTALSIZE                  */
    /* -------------------------------------------------------- */  
        case OSAL_C_S32_IOCTRL_FIOTOTALSIZE:
            {
                unsigned long long int u64Size = 0;
                struct statvfs rsta = {0};
                char TempBuf[50]; // todo check max size and or use malloc
                                
                if( s32GetRealPath( "", TempBuf, s32DeviceId ) == OSAL_ERROR )
                {
                    u32ErrorCode = OSAL_E_NOTSUPPORTED;
                }
                else
                {
                    if( statvfs( TempBuf, &rsta ) < 0 )
                    {
                        u32ErrorCode = u32ConvertErrorCore( errno );
                    }
                }
                if( u32ErrorCode == OSAL_E_NOERROR )
                {
                  u64Size = (unsigned long long int)((unsigned long long int)rsta.f_frsize*(unsigned long long int)rsta.f_blocks);
                  trSize  = (OSAL_trIOCtrlDeviceSize*)arg;
                  trSize->u32High = (u64Size >> 32);
                  trSize->u32Low  = (u64Size & 0xFFFFFFFF);
                }
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {  
                    vTraceIoCtrl(u32ErrorCode, (tS32)u64Size, coszDrive, u32fd, EN_CTRL_FIOTOTALSIZE);      
                }
             }
            break;

    /* -------------------------------------------------------- */
    /*                  OSAL_C_S32_IOCTRL_FIOFREESIZE                    */
    /* -------------------------------------------------------- */
        case OSAL_C_S32_IOCTRL_FIOFREESIZE:
            {
                unsigned long long int u64Size = 0;
                struct statvfs rsta = {0};
                char TempBuf[50]; // todo check max size and or use malloc

                if( s32GetRealPath( "", TempBuf, s32DeviceId ) == OSAL_ERROR )
                {
                    u32ErrorCode = OSAL_E_NOTSUPPORTED;
                }
                else
                {
                    if( statvfs( TempBuf, &rsta ) < 0 )
                    {
                        u32ErrorCode = u32ConvertErrorCore( errno );
                    }
                }
                if( u32ErrorCode == OSAL_E_NOERROR )
                {
                  u64Size = (unsigned long long int)((unsigned long long int)rsta.f_frsize*(unsigned long long int)rsta.f_bfree);
                  trSize  = (OSAL_trIOCtrlDeviceSize*)arg;
                  trSize->u32High = (u64Size >> 32);
                  trSize->u32Low  = (u64Size & 0xFFFFFFFF);
                }
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {    
                    vTraceIoCtrl( u32ErrorCode, s32ReturnValue, coszDrive, u32fd, EN_CTRL_FIOFREESIZE );      
                }
            }
            break;      

    /* -------------------------------------------------------- */
    /*                  OSAL_C_S32_IOCTRL_FIONREAD                        */
    /* -------------------------------------------------------- */         
        case OSAL_C_S32_IOCTRL_FIONREAD:
            {
                if( (((OSAL_tfdIO*)u32fd)->enAccess  < OSAL_EN_ACCESS_DIR)
                     && (((OSAL_tfdIO*)u32fd)->fd != 0) )
                {
                    if((s32CurrentPos = lseek( ((OSAL_tfdIO*)u32fd)->fd, 0, SEEK_CUR) ) < OSAL_OK)
                    {
                        u32ErrorCode = u32ConvertErrorCore(errno);
                    }
                    else
                    {
                        if((s32ReturnValue = lseek( ((OSAL_tfdIO*)u32fd)->fd, 0, SEEK_END) ) < OSAL_OK)
                        {
                            u32ErrorCode = u32ConvertErrorCore( errno );
                        }
                        else
                        {
                            s32ReturnValue = s32ReturnValue - s32CurrentPos;
                            if((lseek( ((OSAL_tfdIO*)u32fd)->fd, s32CurrentPos, SEEK_SET) ) < OSAL_OK)
                            {
                                u32ErrorCode    = u32ConvertErrorCore( errno );
                            }
                        }
                    }
                    if((s32ReturnValue >= 0 ) && (u32ErrorCode == OSAL_E_NOERROR))
                    {
                        if(arg)
                        {
#ifdef FORCE_ARG_32BIT
                            *(tS32*)arg = s32ReturnValue;
#else
                            *(intptr_t*)arg = s32ReturnValue;
#endif
                        }
                    }
                    else
                    {
                        if(arg) 
                        {
                            *(tS32*)arg = 0;
                        }
                    }
                }
                else
                {
                    u32ErrorCode = OSAL_E_NOFILEDESCRIPTOR;
                }
                /* trace the stuff */
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {    
                    vTraceIoCtrl(u32ErrorCode, s32ReturnValue, coszDrive, u32fd, EN_CTRL_FIONREAD);     
                }
            }
            break;

    /* -------------------------------------------------------- */
    /*                  OSAL_C_S32_IOCTRL_FIOREADDIR                     */
    /* -------------------------------------------------------- */         
        case OSAL_C_S32_IOCTRL_FIOREADDIR:
            {
                if((( ((OSAL_tfdIO*)u32fd)->enAccess & OSAL_EN_ACCESS_DIR)  != OSAL_EN_ACCESS_DIR ) && ( ((OSAL_tfdIO*)u32fd)->fd != IS_DEVICE ))
                {
                    u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
                else
                {
                    s32ReturnValue = OSAL_OK;
                    if( (((OSAL_tfdIO*)u32fd)->fd != 0)
                         && (OSAL_EN_ACCESS_GET_PERMISSION(((OSAL_tfdIO*)u32fd)->enAccess) != OSAL_EN_WRITEONLY)
                         && (((OSAL_tfdIO*)u32fd)->fd != IS_RESTRICTED_DEVICE) )
                    {
                        if (((OSAL_tfdIO*)u32fd)->tpDirent == ((OSAL_tfdIO*)u32fd)->tpDirentTable)
                        {
                            /* fill dirent table */
                            s32ReturnValue = s32FillDirentTable( (OSAL_tfdIO*)u32fd );
                        }
                        if (s32ReturnValue == OSAL_OK)
                        {
                            /* Set dirent pointer onto next table element */
                            if (((OSAL_tfdIO*)u32fd)->tpDirent != NULL)
                            {
                                strcpy((tString)((OSAL_trIOCtrlDir *)arg)->dirent.s8Name, (tString)((OSAL_tfdIO*)u32fd)->tpDirent->s8Name);
                                ((OSAL_tfdIO*)u32fd)->tpDirent = ((OSAL_tfdIO*)u32fd)->tpDirent->tpNext;  
                            }
                            else
                            {
                                /* free dirent table */
                                vFreeDirentTable( (OSAL_tfdIO*)u32fd );
                                u32ErrorCode = OSAL_E_NOTSUPPORTED;
                            }
                        }
                        else
                        {
                            u32ErrorCode = u32ConvertErrorCore(errno);
                        }
                    }
                    else
                    {
                        u32ErrorCode = OSAL_E_NOTSUPPORTED;
                    }
                }  
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {    
                    vTraceIoCtrl(u32ErrorCode, s32ReturnValue, coszDrive, u32fd, EN_CTRL_READDIR);    
                }
            }
            break;

        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIOREADDIREXT                 */
        /* -------------------------------------------------------- */
        case OSAL_C_S32_IOCTRL_FIOREADDIREXT:
            {
                OSAL_tfdIO *fd = (OSAL_tfdIO*)u32fd;
                tS32 s32Len;
                char* pRealPath = malloc( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET ); //To update the size for handling App folder name upto 256 apart osal Mapping
                char* pDirName  = malloc( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET );//To update the size for handling App folder name upto 256 apart osal Mapping
                if((( fd->enAccess & OSAL_EN_ACCESS_DIR)     != OSAL_EN_ACCESS_DIR ) && ( fd->fd != IS_DEVICE ))
                {
                    u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
                else
                {
                    if((fd->fd != 0) && (OSAL_EN_ACCESS_GET_PERMISSION(fd->enAccess) != OSAL_EN_WRITEONLY) )

                    {
                        OSAL_trIOCtrlExtDir *vextdir = (OSAL_trIOCtrlExtDir*)arg;
                      
                        if( (pRealPath != NULL) && (vextdir != NULL) && (pDirName != NULL) )
                        {
                            memset( pRealPath, 0,OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                            memset( pDirName,  0,OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                            if( fd->fd == IS_DEVICE ) // -1: device?
                            {
                                if( s32GetRealPath( "", pRealPath, s32DeviceId ) != OSAL_OK )
                                {
                                }
                            }
                            else
                            {
                                OSAL_szStringCopy( pRealPath, fd->szFileName );
                            }
                            s32Len = (tS32)strlen( pRealPath );
                            if (s32Len != 0)
                            {
                                u32ReadExtDir(pRealPath, vextdir, s32Len);
                            }
                            else
                            {
                                u32ErrorCode = OSAL_E_NOSPACE;
                            }
                        }
                        else
                        {
                            u32ErrorCode = OSAL_E_NOSPACE;
                        }
                    } 
                    else
                    {
                        u32ErrorCode = OSAL_E_NOTSUPPORTED;          
                    }
                }
                
                if( pRealPath )
                {
                    free( pRealPath );
                }
                if(pDirName)
                {
                    free( pDirName );
                }
 
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {
                    vTraceIoCtrl( u32ErrorCode, s32ReturnValue, coszDrive, u32fd, EN_CTRL_READDIREXT );    
                }            
            }
            break;

        case OSAL_C_S32_IOCTRL_FIOREADDIREXT2:
            {
                OSAL_tfdIO *fd = (OSAL_tfdIO*)u32fd;
                tS32 s32Len;
                char* pRealPath = malloc(OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET); //To update the size for handling App folder name upto 256 apart osal Mapping
                char* pDirName  = malloc(OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET); //To update the size for handling App folder name upto 256 apart osal Mapping
                if((( fd->enAccess & OSAL_EN_ACCESS_DIR)     != OSAL_EN_ACCESS_DIR ) && ( fd->fd != IS_DEVICE ))
                {
                    u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
                else
                {
                    if((fd->fd != 0) && (OSAL_EN_ACCESS_GET_PERMISSION(fd->enAccess) != OSAL_EN_WRITEONLY) )
                    {
                        OSAL_trIOCtrlExt2Dir *vextdir = (OSAL_trIOCtrlExt2Dir*)arg;
                      
                        if( (pRealPath != NULL) && (vextdir != NULL) && (pDirName != NULL) )
                        {
                            memset( (void*)pRealPath, 0,OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                            memset( (void*)pDirName,  0,OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                            if( fd->fd == IS_DEVICE ) // -1: device?
                            {
                                if( s32GetRealPath( "", pRealPath, s32DeviceId ) != OSAL_OK )
                                {
                                }
                            }
                            else
                            {
                                OSAL_szStringCopy( pRealPath, fd->szFileName );
                            }
                            s32Len = (tS32)strlen( pRealPath );
                            if (s32Len != 0)
                            {
                                u32ReadExtDir2( pRealPath, vextdir, s32Len );
                            }
                            else
                            {
                                u32ErrorCode = OSAL_E_NOSPACE;
                            }
                        }
                        else
                        {
                            u32ErrorCode    = OSAL_E_NOSPACE;
                        }
                    } 
                    else
                    {
                        u32ErrorCode    = OSAL_E_NOTSUPPORTED;          
                    }
                }

                if(pRealPath)
                {
                    free(pRealPath);
                }
                if(pDirName)
                {
                    free(pDirName);
                }
                if((pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {  
                    vTraceIoCtrl( u32ErrorCode, s32ReturnValue, coszDrive, u32fd, EN_CTRL_READDIREXT2);    
                }
            }
            break;

    /* -------------------------------------------------------- */
    /*                  OSAL_C_S32_IOCTRL_FIOMKDIR                        */
    /* -------------------------------------------------------- */         
        case OSAL_C_S32_IOCTRL_FIOMKDIR:
            {
                char* cName  = (char*)arg;
                char* pRealPath = malloc(OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);//To update the size for handling App folder name upto 256 apart osal Mapping
                OSAL_tfdIO *fd = (OSAL_tfdIO*)u32fd;
                tU32 len;
              
                if( pRealPath != NULL )
                {
                    memset( (void*)pRealPath, 0,OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                    if( ( fd->fd != 0)
                         &&( OSAL_EN_ACCESS_GET_PERMISSION(fd->enAccess) != OSAL_EN_READONLY) 
                         &&( fd->fd != IS_RESTRICTED_DEVICE) )
                    {
                        if( fd->fd == IS_DEVICE ) // -1: device?
                        {
                            if( s32GetRealPath( "", pRealPath, s32DeviceId ) != OSAL_OK )
                            {
                            }
                        }
                        else
                        {
                            OSAL_szStringCopy( pRealPath, fd->szFileName );
                        }
                        len = strlen( pRealPath );
                        if( pRealPath[len - 1] != '/' )  // check for ending slash, before adding the path
                        {
                            (void)OSAL_szStringConcat( pRealPath, "/" );
                        }
                        OSAL_szStringConcat( pRealPath, cName );
                        s32ReturnValue = mkdir( pRealPath, OSAL_FILE_ACCESS_RIGTHS);
                        if( s32ReturnValue != 0 )
                        {
                            u32ErrorCode    = u32ConvertErrorCore(errno);
                        }
                    }
                    else
                    {    
                        u32ErrorCode    = OSAL_E_NOACCESS;
                    }
                }
                else
                {
                    u32ErrorCode = OSAL_E_NOSPACE;
                }
                if(pRealPath)
                {
                    free(pRealPath);
                }
                if((pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {  
                    vTraceIoCtrl( u32ErrorCode, s32ReturnValue, coszDrive, u32fd, EN_CTRL_FIOMKDIR);    
                }            
            }
            break;
        
    /* -------------------------------------------------------- */
    /*                  OSAL_C_S32_IOCTRL_FIOOPENDIR                     */
    /* -------------------------------------------------------- */ 
        case OSAL_C_S32_IOCTRL_FIOOPENDIR:        
            {
                char* cName  = (char*)arg;
                char* pRealPath = malloc( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET );//To update the size for handling App folder name upto 256 apart osal Mapping
                OSAL_tfdIO *fd = (OSAL_tfdIO*)u32fd;
                tU32 len;
              
                if( pRealPath != NULL )
                {
                    if( fd->fd == IS_DEVICE ) // -1: device?
                    {
                        if( s32GetRealPath( "", pRealPath, s32DeviceId ) != OSAL_OK )
                        {
                        }
                    }
                    else
                    {
                        OSAL_szStringCopy( pRealPath, fd->szFileName );
                    }
                    len = strlen( pRealPath );
                    if( pRealPath[len - 1] != '/' )
                    {
                        (void)OSAL_szStringConcat( pRealPath, "/" );
                    }
                    OSAL_szStringConcat( pRealPath, cName );
                    if( len != 0 ) // todo add a real check
                    {
                        fd->fd = (uintptr_t)opendir( pRealPath );
                    }
                    else        
                    {
                        u32ErrorCode = OSAL_E_DOESNOTEXIST;
                    }
                    if(((OSAL_tfdIO*)u32fd)->fd != 0)
                    {
                        (void)OSAL_szStringCopy( (tString)((OSAL_tfdIO*)u32fd)->szName, pRealPath );
                        ((OSAL_tfdIO*)u32fd)->u32Size    = 0xffffffff;
                        ((OSAL_tfdIO*)u32fd)->offset    = 0;
                        // ((OSAL_tfdIO*)u32fd)->enAccess = OSAL_EN_READWRITE;
                        ((OSAL_tfdIO*)u32fd)->enAccess = OSAL_EN_ACCESS_DIR;
                        ((OSAL_tfdIO*)u32fd)->tpDirentTable = NULL;
                        ((OSAL_tfdIO*)u32fd)->tpDirent = ((OSAL_tfdIO*)u32fd)->tpDirentTable;
                        ((OSAL_tfdIO*)u32fd)->DispId = s32DeviceId;
                        // s32ReturnValue = 0;
                    }
                    else 
                    {
                        u32ErrorCode = u32ConvertErrorCore( errno );
                    }
                }
                else
                {
                    u32ErrorCode = OSAL_E_NOSPACE;
                }
                if( pRealPath )
                {
                    free( pRealPath );
                }
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {  
                    vTraceIoCtrl(u32ErrorCode,0,coszDrive,u32fd,EN_CTRL_FIOOPENDIR);     
                }            
            }
            break;

    /* -------------------------------------------------------- */
    /*                  OSAL_C_S32_IOCTRL_FIORMDIR                        */
    /* -------------------------------------------------------- */ 
        case OSAL_C_S32_IOCTRL_FIORMDIR:
            {
                char* cName  = (char*)arg;
                char* pRealPath = (char*)malloc( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET ); //To update the size for handling App folder name upto 256 apart osal Mapping
                OSAL_tfdIO *fd = (OSAL_tfdIO*)u32fd;
                tU32 len;
                
                if( pRealPath != NULL )
                {
                    memset( pRealPath, 0,OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                    if( (((OSAL_tfdIO*)u32fd)->fd != 0 )
                         &&(OSAL_EN_ACCESS_GET_PERMISSION(fd->enAccess) != OSAL_EN_READONLY) 
                         &&( (((OSAL_tfdIO*)u32fd)->fd != IS_RESTRICTED_DEVICE )) )
                    {
                        if( fd->fd == IS_DEVICE ) // -1: device?
                        {
                            if( s32GetRealPath( "", pRealPath, s32DeviceId ) != OSAL_OK )
                            {
                            }
                        }
                        else
                        {
                            OSAL_szStringCopy( pRealPath, fd->szFileName );
                        }
                        len = strlen( pRealPath );
                        if( pRealPath[len - 1] != '/' )
                        {
                            (void)OSAL_szStringConcat( pRealPath, "/" );
                        }
                        OSAL_szStringConcat( pRealPath, cName );
                      
                        s32ReturnValue = rmdir( pRealPath );
                      
                        if( s32ReturnValue != 0 )
                        {
                            u32ErrorCode = u32ConvertErrorCore(errno);
                        }
                    }
                    else
                    {
                        u32ErrorCode    = OSAL_E_NOACCESS;
                    }
                }
                else
                {
                    u32ErrorCode = OSAL_E_NOSPACE;
                }
                if( pRealPath )
                {
                    free( pRealPath );
                }
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {  
                    vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIORMDIR );      
                }
            }
            break;

        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIORENAME                      */
        /* -------------------------------------------------------- */
        case OSAL_C_S32_IOCTRL_FIORENAME:
            {
                tCString  coszNameOld, coszNameNew;
                char* pRealPath1 = malloc( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET  );//To update the size for handling App folder name upto 256 apart osal Mapping
                char* pRealPath2 = malloc( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET  );//To update the size for handling App folder name upto 256 apart osal Mapping
                OSAL_tfdIO *fd = (OSAL_tfdIO*)u32fd;
                tU32 len;
              
                if( (pRealPath1 != NULL) && (pRealPath2 != NULL) )
                {
                    memset( pRealPath1, 0,OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                    memset( pRealPath2, 0,OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                    coszNameOld = ((tString*)arg)[0]; /* "filename" */
                    coszNameNew = ((tString*)arg)[1]; /* "filename" */
                    /* check filehandle and access-mode */
                    if( (((OSAL_tfdIO*)u32fd)->fd != 0)
                         &&(OSAL_EN_ACCESS_GET_PERMISSION(fd->enAccess) != OSAL_EN_READONLY) 
                         &&( (((OSAL_tfdIO*)u32fd)->fd != IS_RESTRICTED_DEVICE)) )                                                                                                                 
                    {
                        if( fd->fd == IS_DEVICE ) // -1: device?
                        {
                            if( s32GetRealPath( "", pRealPath1, s32DeviceId ) != OSAL_OK )
                            {
                                u32ErrorCode = OSAL_E_DOESNOTEXIST;
                            }
                        }
                        else
                        {
                            OSAL_szStringCopy( pRealPath1, fd->szFileName );
                        }
                        len = strlen( pRealPath1 );
                        if( pRealPath1[len - 1] != '/' )
                        {
                            (void)OSAL_szStringConcat( pRealPath1, "/" );
                        }
                        OSAL_szStringConcat( pRealPath1, coszNameOld );

                        if( fd->fd == IS_DEVICE ) // -1: device?
                        {
                            if( s32GetRealPath( "", pRealPath2, s32DeviceId ) != OSAL_OK )
                            {
                                u32ErrorCode = OSAL_E_DOESNOTEXIST;
                            }
                        }
                        else
                        {
                            OSAL_szStringCopy( pRealPath2, fd->szFileName );
                        }
                        len = strlen( pRealPath2 );
                        if( pRealPath2[len - 1] != '/' )
                        {
                            (void)OSAL_szStringConcat( pRealPath2, "/" );
                        }
                        OSAL_szStringConcat( pRealPath2, coszNameNew );
                        
                        /** now call rename of LFS */
                        if( u32ErrorCode != OSAL_E_DOESNOTEXIST )
                        { 
                            s32ReturnValue = rename( pRealPath1, pRealPath2 );
                            if( s32ReturnValue != 0 )
                            {
                                u32ErrorCode = u32ConvertErrorCore( errno );
                            }
                        }
                    }  
                    else
                    {
                        u32ErrorCode = OSAL_E_NOACCESS;
                    }
                }
                else
                {
                    u32ErrorCode = OSAL_E_NOSPACE;
                }
                if( pRealPath1 )
                {
                    free( pRealPath1 );
                }
                if( pRealPath2 )
                {
                    free( pRealPath2 );
                }
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {
                    vTraceIoCtrl(u32ErrorCode,0,coszDrive,u32fd,EN_CTRL_FIORENAME);      
                }
            }
            break;

        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIORMRECURSIVE                */
        /* -------------------------------------------------------- */
        case OSAL_C_S32_IOCTRL_FIORMRECURSIVE:
            {
                char* cRemoveDirName = (char*)arg;
                char* pRealPath = malloc( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET  ); //To update the size for handling App folder name upto 256 apart osal Mapping
                OSAL_tfdIO *fd  = (OSAL_tfdIO*)u32fd;
                tU32 len;
              
                pOsalData->bStopRecursiveDelete = FALSE;
                if( pRealPath != NULL )
                {
                    memset(pRealPath, 0, OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET);
                    if( (((OSAL_tfdIO*)u32fd)->fd != 0 )
                         &&(OSAL_EN_ACCESS_GET_PERMISSION(fd->enAccess) != OSAL_EN_READONLY) 
                         && ( (((OSAL_tfdIO*)u32fd)->fd != IS_RESTRICTED_DEVICE )) )
                    {
                        if( fd->fd == IS_DEVICE ) // -1: device?
                        {
                            if( s32GetRealPath( "", pRealPath, s32DeviceId ) != OSAL_OK )
                            {
                                u32ErrorCode = OSAL_E_DOESNOTEXIST;
                            }
                        }
                        else
                        {
                            OSAL_szStringCopy( pRealPath, fd->szFileName );
                        }
                        len = strlen( pRealPath );
                        if( pRealPath[len - 1] != '/' )
                        {
                            (void)OSAL_szStringConcat( pRealPath, "/" );
                        }
                        OSAL_szStringConcat( pRealPath, cRemoveDirName );
                        
                        /* remove directory structure */
                        if( u32ErrorCode != OSAL_E_DOESNOTEXIST ) 
                        {
                            u32ErrorCode = (tU32)s32HandleDirRecursive(pRealPath, 0, NULL);
                        }
                    }
                    else
                    {
                      u32ErrorCode = OSAL_E_NOACCESS;
                    }
                }
                else
                {
                    u32ErrorCode = OSAL_E_NOSPACE;
                }
                if( pRealPath )
                {
                    free( pRealPath );
                }
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {
                    vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIORMRECURSIVE );      
                }
            }
            break;
             
        case OSAL_C_S32_IOCTRL_FIORMRECURSIVE+20:
            {
              tCString coszDirName = (char*)arg;
              tU32 u32Size;
              
              u32ErrorCode = (tU32)s32HandleDirRecursive( coszDirName, 1, &u32Size );
              if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
              {
                    vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIORMRECURSIVE );      
              }
            }
            break;

        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIORMRECURSIVE_CANCEL                */
        /* -------------------------------------------------------- */
        case OSAL_C_S32_IOCTRL_FIORMRECURSIVE_CANCEL:
            u32ErrorCode = OSAL_E_NOERROR;
            pOsalData->bStopRecursiveDelete = TRUE;
            if((pOsalData->bTraceAll )||(u32OsalSTrace & 0x00000001))
            {
                vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIORMRECURSIVECANCEL );  
            }
            break;

        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIODISKFORMAT                 */
        /* -------------------------------------------------------- */         
        case OSAL_C_S32_IOCTRL_FIODISKFORMAT:
                u32ErrorCode = OSAL_E_NOTSUPPORTED;
             //  if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR) )
                {
                    vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIOFORMAT );        
                }
            break;
        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIO_CHMOD                      */
        /* -------------------------------------------------------- */         
        case OSAL_C_S32_IOCTRL_FIO_CHMOD:
              if( (arg) && (((OSAL_tfdIO*)u32fd)->fd != 0) )
              {
#ifdef FORCE_ARG_32BIT
                  if(fchmod(((OSAL_tfdIO*)u32fd)->fd,*((tU32*)arg)) != 0)
#else
                  if(fchmod(((OSAL_tfdIO*)u32fd)->fd,*((uintptr_t*)arg)) != 0)
#endif
                  {
                        u32ErrorCode = u32ConvertErrorCore( errno );
                  }
                  if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                  {
                      vTraceIoCtrl( u32ErrorCode, *((tU32*)arg), coszDrive, u32fd, EN_CTRL_FIOCHMOD);
                  }
              }
             break;
        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIO_CHOWN                      */
        /* -------------------------------------------------------- */         
        case OSAL_C_S32_IOCTRL_FIO_CHOWN:
              if( (arg) && (((OSAL_tfdIO*)u32fd)->fd != 0) )
              {
                  tU32 u32Owner = ((tU32*)arg)[0];
                  tU32 u32Group = ((tU32*)arg)[1];
                  if(fchown(((OSAL_tfdIO*)u32fd)->fd,u32Owner,u32Group) != 0)
                  {
                        u32ErrorCode = u32ConvertErrorCore( errno );
                  }
                  if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                  {
                      vTraceIoCtrl( u32ErrorCode,u32Owner, coszDrive, u32fd, EN_CTRL_FIOCHOWN);
                  }
             }
             break;
        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIOGET_REAL_PATH             */
        /* -------------------------------------------------------- */
        case OSAL_C_S32_IOCTRL_FIOGET_REAL_PATH:
            {
                OSAL_trOsalRealPath* pInfo = (OSAL_trOsalRealPath*)arg;
                OSAL_tfdIO *fd = (OSAL_tfdIO*)u32fd;
                
                if( pInfo == NULL )
                {
                    u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
                else
                {
                    if( fd->fd == IS_DEVICE ) // -1: device?
                    {
                        if( s32GetRealPath( "", (char*)pInfo, s32DeviceId ) != OSAL_OK )
                        {
                            u32ErrorCode = OSAL_E_NOTSUPPORTED; // not supported for this ID
                        }
                    }
                    else
                    {
                        strncpy( (char*)pInfo, fd->szFileName, sizeof(OSAL_trOsalRealPath) - 1 );
                        pInfo->szRealPath[sizeof(OSAL_trOsalRealPath) - 1] = 0;    // terminate always. can happen, if string size is to small
                    }
                }
                
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR )||(u32OsalSTrace & 0x00000001))
                {
                    if( u32ErrorCode == OSAL_E_NOERROR )
                    {
                        vTraceIoCtrl( u32ErrorCode, s32DeviceId, (char*)pInfo, u32fd, EN_CTRL_FIOGET_REAL_PATH );
                    }
                    else
                    {
                        vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIOGET_REAL_PATH );
                    }
                }
            }
            break;




        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIOCOPYDIR                 */
        /* -------------------------------------------------------- */
        case OSAL_C_S32_IOCTRL_FIOCOPYDIR:
            {
                tCString coszName, coszNameNew;
                /*Allocate the memory for source and destination path*/
                char* pRealPath1 = malloc( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET  );//To update the size for handling App folder name upto 256 apart osal Mapping
                char* pRealPath2 = malloc( OSAL_C_U32_MAX_PATHLENGTH + OSAL_C_U32_OS_PATHLENGTH_OFFSET  );//To update the size for handling App folder name upto 256 apart osal Mapping
                
                coszName     = ((tString*)arg)[0];
                coszNameNew = ((tString*)arg)[1];
         
                if( (pRealPath1 != NULL) && (pRealPath2 != NULL)
                     && (coszName != NULL) && (coszNameNew != NULL) )
                {
                    if( bMapFileSystems( pRealPath1, (char*)coszName ) != TRUE )
                    {
                        u32ErrorCode = OSAL_E_DOESNOTEXIST;
                    }
                    if( bMapFileSystems( pRealPath2, (char*)coszNameNew ) != TRUE )
                    {
                        u32ErrorCode = OSAL_E_DOESNOTEXIST;
                    }
                    if( u32ErrorCode != OSAL_E_DOESNOTEXIST )
                    {    
                        /* create destination directory */
                        mkdir( pRealPath2, (OSAL_FILE_ACCESS_RIGTHS) );
                        u32ErrorCode = u32CopyDirFiles( pRealPath1, pRealPath2 );
                    }
                }
                else
                {
                    u32ErrorCode = OSAL_E_NOSPACE;
                }
                if( pRealPath1 )
                {
                    free( pRealPath1 );
                }
                if( pRealPath2 )
                {
                    free( pRealPath2 );
                }
                if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
                {
                    vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIODIRCOPY );    
                }
            }
            break;
        case OSAL_C_S32_IOCTRL_FIOFLUSH:
              if(fsync(((OSAL_tfdIO*)u32fd)->fd) == -1)
              {
                  u32ErrorCode = u32ConvertErrorCore( errno );
              }
              else
              {
                    u32ErrorCode = OSAL_E_NOERROR;
              }
              if(( pOsalData->bTraceAll )|| (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
            {
                vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIOFLUSH );    
            }
            break;
        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_VERSION                         */
        /* -------------------------------------------------------- */         
        case OSAL_C_S32_IOCTRL_VERSION:
            if( arg )
            {
                *(intptr_t*)arg  = OSAL_CNTRL_VERSION;
                u32ErrorCode = OSAL_E_NOERROR;
            }
            else
            {
                u32ErrorCode = OSAL_E_INVALIDVALUE;
            }
            break;
            
        case OSAL_C_S32_IOCTRL_USBH_GETDEVICEINFO:
            u32ErrorCode = OSAL_E_NOTSUPPORTED;
            break;
            
        case OSAL_C_S32_IOCTRL_USBH_GETHUBDEVICEINFO:
            u32ErrorCode = OSAL_E_NOTSUPPORTED;
            vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIOUSBH_GET_HUBDEVICEINFO );    
            break;
        /* -------------------------------------------------------- */
        /*                  OSAL_C_S32_IOCTRL_FIOCHKDSK                        */
        /* -------------------------------------------------------- */
        case OSAL_C_S32_IOCTRL_FIOCHKDSK:         
            {
                /*checkfilesys returns 0 for success.Hard coded just to avoid 
                OSAL error codes in fatcheckdisk or introducing new error codes
                to osfile.c*/
                u32ErrorCode = OSAL_E_NOTSUPPORTED;
                vTraceIoCtrl( u32ErrorCode, 0, coszDrive, u32fd, EN_CTRL_FIOCHKDSK );    
            }
            break;
/* ========================================================================= */
/*                                                                                                    */
/*                        Tolerated IOCtrls to the day                                      */
/*                                                                                                    */
/* ========================================================================= */

        case OSAL_C_S32_IOCTRL_FIOSETSTREAMMODE:
              u32ErrorCode = OSAL_E_NOTSUPPORTED;
            break;
             
        case OSAL_C_S32_IOCTRL_FFS_SAVENOW:
                NORMAL_M_ASSERT_ALWAYS();
                u32ErrorCode = OSAL_E_NOTSUPPORTED;
            break;
            
        /* -------------------------------------------------------- */
        /*                                DEFAULT                                    */
        /* -------------------------------------------------------- */ 
        default:
            u32ErrorCode = u32CardIOControl( u32fd, fun, arg, s32DeviceId );
            break;
     }
     }
     else
     {
        u32ErrorCode = OSAL_E_BADFILEDESCRIPTOR;
         vTraceIoCtrl( u32ErrorCode, 0, coszDrive, 0xffffffff, fun );    
     }
     return( u32ErrorCode );
}


/******************************************************************************
 * FUNCTION:        LFS_s32IOWrite
 *
 * DESCRIPTION:    This function writes a number of bytes from a data file
 *                     on RAMDISK device.
 *
 * PARAMETER:
 *
 *    tU32            u32fd:          low level descriptor of file;
 *    tPCS8          pcos8Buffer:  input buffer;
 *    tU32            u32Lenght:     size of the buffer in bytes.
 *
 * RETURNVALUE:    u32ErrorCode
 *                      > OSAL_E_NOERROR if everything goes right;
 *                      > OSAL_E_NOTSUPPORTED otherwise.
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 26.02.04  |    Initial revision                            | CM-DI/EES4 S.Kropp
 ************************* *****************************************************/
tU32 LFS_u32IOWrite( uintptr_t u32fd, uintptr_t pBuffer, tU32 u32Length, intptr_t *ps32Written )
{
    tPCS8 pcos8Buffer  = (tPCS8)pBuffer;
    tU32  u32ErrorCode = u32CheckFileHandle(u32fd);
#ifdef DLY_RW_SEGMENT
    tS32  s32DeviceId  = *ps32Written;
#endif

    if((u32ErrorCode == OSAL_E_NOERROR)
         && (OSAL_EN_ACCESS_GET_PERMISSION(((OSAL_tfdIO*)u32fd)->enAccess) != OSAL_EN_READONLY) )
    {
#ifdef READWRITE_SEGMENTED
        if( s32ID == OSAL_EN_DEVID_FFS3 )
        { 
            tU32 u32Written = 0;
            tU32 u32_bytes_to_write = 0;
            
            do
            {
                if( (u32Length - u32Written) > READ_BUFFER_LENGTH )
                {  u32_bytes_to_write = READ_BUFFER_LENGTH; }
                else
                {  u32_bytes_to_write = u32Length - u32Written;    }
                if( (*ps32Written = write( ((OSAL_tfdIO*)u32fd)->fd, pcos8Buffer + u32Written, (tS32)READ_BUFFER_LENGTH )) < 0)
                {
                    /*Write error */
                    u32ErrorCode = u32ConvertErrorCore( errno );
                    break;
                }
                else
                {
                    u32Written += *ps32Written;
                    ((OSAL_tfdIO*)u32fd)->u32Size += *ps32Written;
                }
            }while( u32Written < u32Length );
            *ps32Written = u32Written;
        }
        else
#endif
        {
            if ((*ps32Written = write( ((OSAL_tfdIO*)u32fd)->fd, pcos8Buffer, u32Length )) != (tS32)u32Length )
            { 
                /* Write error */
                if(-1 == *ps32Written )
                {
                    u32ErrorCode = u32ConvertErrorCore( errno );
                }
                else  // short write happens to a file: no space left
                {
                    u32ErrorCode = u32ConvertErrorCore( ENOSPC );
                    ((OSAL_tfdIO*)u32fd)->u32Size += *ps32Written;
                }
            }
            else
            {
                ((OSAL_tfdIO*)u32fd)->u32Size += *ps32Written;
            }         
        } 
    }
    else
    {
        u32ErrorCode = OSAL_E_NOTSUPPORTED;
    }

    if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
    {
#ifdef SHORT_TRACE_OUTPUT
        tU8 au8Buf[17];
        
        OSAL_M_INSERT_T8 ( &au8Buf[0],  EN_FS_DISPATCHER | EN_IOWRITE_RESULT );
        OSAL_M_INSERT_T32( &au8Buf[1],  u32ErrorCode );
        OSAL_M_INSERT_T32( &au8Buf[5],  u32fd );
        OSAL_M_INSERT_T32( &au8Buf[9],  (tU32)*ps32Written );
        OSAL_M_INSERT_T32( &au8Buf[13], u32Length );
        LLD_vTrace( TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], sizeof(au8Buf) );
#else
        tU8 au8Buf[MAX_TRACE_SIZE+1]={0};
        au8Buf[0] = OSAL_STRING_OUT;
        snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOWRITE  : Result=0x%x h=0x%" PRIxPTR " Bytes=%d/%d",
                    (unsigned int)u32ErrorCode ,(uintptr_t)u32fd,(int)*ps32Written,(int)u32Length);
        LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], strlen((char*)&au8Buf[1])+1);
#endif
    }    
    return( u32ErrorCode );
}


/******************************************************************************
 * FUNCTION:        LFS_s32IORead
 *
 * DESCRIPTION:    This function reads a number of bytes from a data file
 *                     within a drive device.
 *
 * PARAMETER:
 *
 *    tU32            u32fd:            low level descriptor of file;
 *    tPS8            ps8Buffer:      input buffer;
 *    tU32            u32MaxLenght:  maximal size of the buffer in bytes.
 *
 * RETURNVALUE:    u32ErrorCode
 *                      > OSAL_E_NOERROR if everything goes right;
 *                      > OSAL_E_NOTSUPPORTED otherwise.
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 26.02.04  |    Initial revision                            | CM-DI/EES4 S.Kropp
 ******************************************************************************/
tU32 LFS_u32IORead( uintptr_t u32fd, uintptr_t pBuffer, tU32 u32MaxLength, intptr_t *ps32Read )
{
    tU32 u32ErrorCode = u32CheckFileHandle(u32fd);
    tPS8 pos8Buffer    = (tPS8)pBuffer;
    tS32 s32DeviceId  = *ps32Read;
    tS32 ret = -1;

    if((u32ErrorCode == OSAL_E_NOERROR)
         && (OSAL_EN_ACCESS_GET_PERMISSION(((OSAL_tfdIO*)u32fd)->enAccess) != OSAL_EN_WRITEONLY) )
    {
    
#ifdef READWRITE_SEGMENTED
        if( s32DeviceId == OSAL_EN_DEVID_FFS3 )
        { 
            tU32 u32Read = 0;
            tU32 u32_bytes_to_read = 0;
            
            do
            {
                if((u32MaxLength - u32Read) > READ_BUFFER_LENGTH)
                {  
                    u32_bytes_to_read = READ_BUFFER_LENGTH; 
                }
                else
                {  
                    u32_bytes_to_read = u32MaxLength - u32Read;    
                }
                if ((*ps32Read = read( ((OSAL_tfdIO*)u32fd)->fd, pos8Buffer+u32Read, READ_BUFFER_LENGTH )) < 0)
                {
                    u32ErrorCode = u32ConvertErrorCore( errno );
                    break;
                }
                else
                {
                    u32Read += *ps32Read;
                    ((OSAL_tfdIO*)u32FD)->offset += *ps32Read;
                }
            }while( u32Read < u32MaxLength );
            *ps32Read = u32Read;
        } 
        else
#endif
        {
            if (((OSAL_tfdIO*)u32fd)->DispId == OSAL_EN_DEVID_ROOT )
            {
                ret = strncmp(((OSAL_tfdIO*)u32fd)->szFileName, "/proc/", 6);
            }
            /* Since size of the proc file system is always zero, so check is added to suppress the offset check */
            if(((tU32)((OSAL_tfdIO*)u32fd)->offset > ((OSAL_tfdIO*)u32fd)->u32Size) && (ret != 0) )
            {
                 u32ErrorCode = OSAL_E_ACCESS_FAILED;
            }
            else
            {
            if( (*ps32Read = read( ((OSAL_tfdIO*)u32fd)->fd, pos8Buffer, u32MaxLength )) < 0 )
            {
                u32ErrorCode = u32ConvertErrorCore( errno );
                if((s32DeviceId == OSAL_EN_DEVID_DVD))
                {
                    if(pOsalData->u16DvdMediaType == OSAL_C_U16_MEDIA_EJECTED)
                    {
                        u32ErrorCode = OSAL_E_MEDIA_NOT_AVAILABLE;
                    }
                }
                else if((s32DeviceId == OSAL_EN_DEVID_CARD) || (s32DeviceId == OSAL_EN_DEVID_CRYPT_CARD))
                {
                    if(pOsalData->bRealCardIf)
                    {
                        if((pOsalData->u16SdCardMediaType == OSAL_C_U16_MEDIA_EJECTED) && (pOsalData->u16CardMediaType1 == OSAL_C_U16_MEDIA_EJECTED))
                        {
                          u32ErrorCode = OSAL_E_MEDIA_NOT_AVAILABLE;
                        }
                    }
                    else
                    {
                        if(pOsalData->u16CardMediaType1 == OSAL_C_U16_MEDIA_EJECTED)
                        {
                          u32ErrorCode = OSAL_E_MEDIA_NOT_AVAILABLE;
                        }
                    }
                }                    
            }
            else
            {
                ((OSAL_tfdIO*)u32fd)->offset += *ps32Read;
            }
            }
        }
    }
    else
    {
        u32ErrorCode = OSAL_E_NOTSUPPORTED;
    }

    if( (pOsalData->bTraceAll) || (u32ErrorCode != OSAL_E_NOERROR)||(u32OsalSTrace & 0x00000001))
    {
#ifdef SHORT_TRACE_OUTPUT
        tU8 au8Buf[17];
            
        OSAL_M_INSERT_T8 (&au8Buf[0], EN_FS_DISPATCHER | EN_IOREAD_RESULT);
        OSAL_M_INSERT_T32(&au8Buf[1], u32ErrorCode);
        OSAL_M_INSERT_T32(&au8Buf[5], u32fd);
        OSAL_M_INSERT_T32(&au8Buf[9], (tU32)*ps32Read);
        OSAL_M_INSERT_T32(&au8Buf[13], u32MaxLength);
        LLD_vTrace( TR_COMP_OSALIO, TR_LEVEL_FATAL, &au8Buf[0], sizeof(au8Buf) );
#else
        tU8 au8Buf[MAX_TRACE_SIZE+1]={0};
        au8Buf[0] = OSAL_STRING_OUT;
        snprintf((char*)&au8Buf[1],MAX_TRACE_SIZE,"IOREAD    : Result=0x%x h=0x%" PRIxPTR "  Bytes=%d/%d",
                    (unsigned int)u32ErrorCode ,(uintptr_t)u32fd,(int     )*ps32Read,(int)u32MaxLength);
        LLD_vTrace(TR_COMP_OSALIO, TR_LEVEL_ERRORS, &au8Buf[0], strlen((char*)&au8Buf[1])+1);
#endif
    }    
    return( u32ErrorCode );
}


/******************************************************************************
 * FUNCTION:        s32FillDirentTable
 *
 * DESCRIPTION:    This function reads reads all entries in a directory
 *                     that matches a pattern and fills the dirent table.
 *
 * PARAMETER:
 *
 *
 * RETURNVALUE:    s32ReturnValue
 *                      > OSAL_OK if everything goes right;
 *                      > OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 03.09.03  |    Initial revision                            | CM-CR/EES4, S.Kropp
 ******************************************************************************/
tS32 s32FillDirentTable( OSAL_tfdIO *fdesc )
{
    tS32                s32Ret = OSAL_E_NOERROR;
    OSAL_trDirent*  ptr = NULL;
    OSAL_trDirent*  prev_ptr = NULL;
    struct dirent*  pDirEnt = NULL;
    char* pcNameBuffer = NULL;
    DIR* dir = NULL;
    tBool bDirHasToBeCreated = FALSE;
    tBool bRetry = TRUE;

    while( bRetry )
    {
        bRetry = FALSE;
        pcNameBuffer = NULL;

        /* prove special case read mount point */
        if( (((OSAL_tfdIO*)fdesc)->fd == IS_DEVICE) || (((OSAL_tfdIO*)fdesc)->fd == IS_RESTRICTED_DEVICE))
        {
            bDirHasToBeCreated = TRUE;
        }
        else
        {
                    dir = (DIR*)((OSAL_tfdIO*)fdesc)->fd;
                    pDirEnt = readdir( (DIR*)dir );  
                    /* pseudo file handle was created at open function,
                    because / at string end was missing */
                    if(pDirEnt == OSAL_NULL)
                    {
                         /* check for close of old file descriptor */
                         if((((OSAL_tfdIO*)fdesc)->enAccess & OSAL_EN_ACCESS_DIR) == OSAL_EN_ACCESS_DIR)                         
                         {
                              /* Fix for NIKAI2-7993 Closedir should not be executed in IOControl
                                  It will be closed by Application using OSAL_IOClose()*/
                              // closedir((DIR*)((OSAL_tfdIO*)fdesc)->fd);
                              vFreeDirentTable((OSAL_tfdIO*)fdesc);
                              prev_ptr = OSAL_NULL;
                              ptr        = OSAL_NULL;
                         }
                    }
        }
        if( bDirHasToBeCreated )
        {
            /* create a new directory handle if the device is IS_DEVICE else original path(example :"/shared/cryptnav/CRYPTNAV/DATA/DATA") 
            will be replaced by Device path("/shared/cryptnav/CRYPTNAV/") only */
            pcNameBuffer = (char*)malloc( 256 );
            if(pcNameBuffer)
            {
                // getrealname            
                if( s32GetRealPath( "", pcNameBuffer, fdesc->DispId ) != OSAL_OK )
                {
                }
             
                if((dir = opendir( (char*)pcNameBuffer )) == 0)
                {
                    s32Ret = (tS32)u32ConvertErrorCore( errno );
                    // ((OSAL_tfdIO*)fdesc)->fd = 0; don't touch the fd, a second try is not possible, if changing the value
                }
                else
                {
                    ((OSAL_tfdIO*)fdesc)->enAccess = OSAL_EN_ACCESS_DIR;
                    ((OSAL_tfdIO*)fdesc)->fd = (uintptr_t)dir;
                    // if changing the handle to a dir handle, save the path in szFilename
                    strcpy( fdesc->szFileName, pcNameBuffer );
                    pDirEnt = readdir( (DIR*)dir );  
                }
                free(pcNameBuffer);
                pcNameBuffer = 0;
            }
            else
            {    
                s32Ret = OSAL_E_NOSPACE;
            }
        } 

        /* prove for valid handle value */
        if(s32Ret == (tS32)OSAL_E_NOERROR)
        {
            /* if allready read to this descriptor free the existing structures */
            if(fdesc->tpDirentTable || fdesc->tpDirent)
            {
                vFreeDirentTable( fdesc );
              /* ket2hi 26.07.08
                * if directroy entries are freed the structure to create the next entry (prev_ptr)  for this  former valid chained list
                * has been set to Null otherwise access to freed memory is possible (see instruction prev_ptr->tpNext = ptr below)
                * In case of low voltage the chained listc " tpDirentTable" for a directory is build up newly. 
                * Therefore the structure prev_ptr has to be initialized with NULL
                * Important: The initialization of pDirEnt has to be considered in oder to avoid to access old directory entries
                */
                prev_ptr = OSAL_NULL;
                ptr      = OSAL_NULL;
            }
            while( pDirEnt )
            {        
                /* prove for unecessary entries */
                /* pDirEnt already proved */
                if(pDirEnt->d_name[0] != '.')
                {
                    /* allocate memory for entry name */
                    if((ptr = (OSAL_trDirent*)malloc( sizeof(OSAL_trDirent) )) == NULL)
                    {
                        s32Ret = OSAL_E_UNKNOWN;
                        break;
                    }
                    if (fdesc->tpDirentTable == NULL)
                    {
                        fdesc->tpDirentTable = ptr;
                        fdesc->tpDirent = fdesc->tpDirentTable;
                    }
                    if (prev_ptr != NULL)
                    {
                        prev_ptr->tpNext = ptr;
                    }
                    /* copy name into list element */
                    /* pDirEnt already proved */
                    strcpy( (tString)ptr->s8Name, pDirEnt->d_name );
                    ptr->tpNext = NULL;
                    prev_ptr = ptr;
                }
                pDirEnt = readdir(dir);
    
            } // end while
            if( dir != 0 )
            {
                rewinddir( dir );
            }
        }// if(s32Ret == OSAL_E_NOERROR)
    }
 
    if(s32Ret == (tS32)OSAL_E_NOERROR)
    {
        s32Ret = OSAL_OK;
    }
    else
    {
        OSAL_vSetErrorCode( (tU32)s32Ret );        /* Set error code */
        s32Ret = OSAL_ERROR;
    }
    return( s32Ret );
}


/******************************************************************************
 * FUNCTION:        vFreeDirentTable
 *
 * DESCRIPTION:    All elements of dirent table will be freed.
 *
 * PARAMETER:
 *
 *
 * RETURNVALUE:    none
 *
 * HISTORY:
 * Date        |    Modification                                 | Authors
 * 03.09.03  |    Initial revision                            | CM-CR/EES4, S.Kropp
 ******************************************************************************/
tVoid vFreeDirentTable( OSAL_tfdIO *fd )
{
    OSAL_trDirent  *ptr;
    OSAL_trDirent  *next_ptr = NULL;

    ptr = fd->tpDirentTable;
    while(ptr != NULL)
    {
        next_ptr = ptr->tpNext;
        free( (void*)ptr );
        ptr = next_ptr;
    }
    fd->tpDirentTable = NULL;
    fd->tpDirent = fd->tpDirentTable;
}



#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file osfile.c
|-----------------------------------------------------------------------*/
