/************************************************************************
| FILE:         Devinit.c
| PROJECT:      BMW_L6
| SW-COMPONENT: OSAL IO
|------------------------------------------------------------------------
| DESCRIPTION:  This is the source for OSAL IO devices initialization
|
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| 13.02.2013| NIKAI-1970, Registry       | asa5cob
|           | rereival TTFIs command
|           | blocks LSIM           
| 18.03.2013| NIKAI - 4010               | asa5cob    
| 09.04.2013| NIKAI - 3061               | sgo1cob    
| 09.04.2014| LSIM-Gen3 CD Audio and
|           | control feature enabled but  
|           | Loaded as shared object    | sgo1cob
|************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"



#ifdef __cplusplus
extern "C" {
#endif


/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/*************************************************************************
|function prototype inclusion (scope: module-local)
|-----------------------------------------------------------------------*/


/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/*  to be aligned with eSharedLib*/
static tCString  szLibNames[EN_SHARED_LAST]={"libshared_kds_so.so","libsensordrv_so.so", "libshared_cd_so.so","libshared_wup_so.so",
                                          "libshared_volt_so.so", "libshared_eollib_so.so","libshared_adr3ctrl_so.so","libcryptmodul_so.so",
                                          "libshared_acoustic_so.so","libosal_addon_so.so","libshared_touch_screen_so.so","libbasedrv_so.so",
                                          "libshared_aarsdab_so.so","libshared_aarsssi32_so.so","libshared_aarsmtd_so.so","libshared_aarsamfm_so.so"};


/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
void vSetupRegistry(void);
void LLD_vRegTraceCallback( TR_tenTraceChan enTraceChannel, OSAL_tpfCallback pfCallback );

/************************************************************************
|function prototype (scope: module-global)
|-----------------------------------------------------------------------*/
extern dbg_func pIoDbgFunc;


extern void HandleMaxErrMemFilesize(int u32Size);
extern void vSetTraceForAllDeviceIo(char cVal, tS32 s32Pid);
extern tBool bCreateRegFromFileNativ(char* szFileName);

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

void TraceIOString(tCString cBuffer)
{
  TraceString(cBuffer);
}


/*****************************************************************************
 * helper date functions for vTraceErrmem
 *****************************************************************************/

/*****************************************************************************
*
* FUNCTION:    calc_seconds
*
* DESCRIPTION: calculate seconds of given time       
*
* PARAMETER:   OSAL_trTimeDate *t
*
* RETURNVALUE: value of "t" in seconds;
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
//#define TT_TRACE_CLASS (TR_COMP_OSALCORE + 0x01) //TR_COMP_TT

void vIoCallbackHandler(void* pvBuffer )
{
   if(pIoDbgFunc == NULL)
   {
      if(u32LoadSharedObject(pIoDbgFunc,pOsalData->rLibrary[EN_SHARED_OSAL2].cLibraryNames) != OSAL_E_NOERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   if(pIoDbgFunc)
   {
      pIoDbgFunc(pvBuffer );
   }
}

/*****************************************************************************
*
* FUNCTION:    vRegisterSysCallback
*
* DESCRIPTION: Register OSAL Callback
*
* PARAMETER:   none
*
* RETURNVALUE: none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vRegisterOsalIO_Callback(void)
{
  LLD_vRegTraceCallback((TR_tenTraceChan)TR_TTFIS_LINUX_OSALIO,(OSAL_tpfCallback)vIoCallbackHandler);
}

/*****************************************************************************
*
* FUNCTION:    vUnregisterSysCallback
*
* DESCRIPTION: Unregister OSAL Callback
*
* PARAMETER:   none
*
* RETURNVALUE: none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.10.05  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vUnregisterOsalIO_Callback(void)
{
 /*   LaunchChannel  oTraceChannel;

    oTraceChannel.enTraceChannel = TR_TTFIS_OSALIO;
    oTraceChannel.p_Callback = (OSAL_tpfCallback)vOsalIoCallbackHandler;*/
}

extern tU32 REG_u32IOCreate(tCString coszName, OSAL_tenAccess enAccess, uintptr_t *pu32RetAddr);
extern tU32 REG_u32IOOpen(tCString coszName, OSAL_tenAccess enAccess,uintptr_t* pu32RetAddr);
extern tU32 REG_u32IOClose(uintptr_t u32fd);
extern tU32 REG_u32IOControl(uintptr_t u32fd, tS32 fun, uintptr_t arg);


void vSetDefaultRegValues( uintptr_t u32Fd)
{
   tS32 s32Val;
   OSAL_trIOCtrlRegistry rReg;

#ifdef AES_ENCRYPTION_AVAILABLE
   s32Val = 0x00000001;//1= active
#else
   s32Val = 0x00000000;//0= Not active
#endif
   rReg.pcos8Name = (tS8*)"AES_ENCRYPTION";
   rReg.ps8Value  = (tU8*)&s32Val;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   (void)REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&rReg);

   s32Val = 0x00000001;//0=Not active,1 Nice Level, 2Prio
   rReg.pcos8Name = (tS8*)"LINUX_PRIO";
   rReg.ps8Value  = (tU8*)&s32Val;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   (void)REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&rReg);

   s32Val = OSAL_MSG_POOL_SIZE;//default size
   rReg.pcos8Name = (tS8*)"MAXPOOLSIZE";
   rReg.ps8Value  = (tU8*)&s32Val;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   (void)REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&rReg);


   s32Val = (tS32)0xffffffff;//default unlimited, 0 kernel configuration is used , other value configured by project
   rReg.pcos8Name = (tS8*)"MQ_LIMIT";
   rReg.ps8Value  = (tU8*)&s32Val;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   (void)REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&rReg);

   s32Val = 0x1;//default uppercase workaround supported
   rReg.pcos8Name = (tS8*)"UPPER_CASE";
   rReg.ps8Value  = (tU8*)&s32Val;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   (void)REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&rReg);

   s32Val = 0x0;//default LD_LIBRARY_PATH should be used
   rReg.pcos8Name = (tS8*)"NO_LD_LIBRARY_PATH";
   rReg.ps8Value  = (tU8*)&s32Val;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   (void)REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"TIM_RESOLUTION";
   rReg.ps8Value  = (tU8*)&pOsalData->u32TimerResolution;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   (void)REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t)&rReg);
}

void vPrepareLocalFileSystem(void)
{
   char Path[256];
   int Len;
   int Ret;
   int Val = TRUE;
   if(getcwd(&Path[0],256) == NULL)
   {
     TraceString("vPrepareFileSystem getcwd failed %d",errno);
   }
   else
   {
      TraceString("vPrepareFileSystem cwd %s",&Path[0]);
   }
   Len = strlen(Path);
   strncat(&Path[Len],"/bosch",256);
   Ret = mkdir(Path,OSAL_VOLATILE_OSAL_ACCESS_RIGTHS);
   if(Ret == -1)
   {
      if(errno == EEXIST)
      {
        Val = TRUE;
      } 
      else
      {
         Val = FALSE;
         TraceString("Create Directory failed %d",errno);
      }
   }
   else
   {
      Val = TRUE;
      if(chmod(Path, OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
      {
         vWritePrintfErrmem("vPrepareLocalFileSystem -> %s chmod error %d \n",Path,errno);
      }
   }
   if(Val == TRUE)
   {
      memset(&Path[Len],0,256-Len);
      strncpy(&Path[Len],"/bosch/dynamic",strlen("/bosch/dynamic"));
      if(mkdir(Path,OSAL_VOLATILE_OSAL_ACCESS_RIGTHS) == 0)
      {
         if(chmod(Path, OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
         {     vWritePrintfErrmem("vPrepareLocalFileSystem -> %s chmod error %d \n",Path,errno);   }
      }
      memset(&Path[Len],0,256-Len);
      strncpy(&Path[Len],"/bosch/dynamic/ffs",strlen("/bosch/dynamic/ffs"));
      if(mkdir(Path,OSAL_VOLATILE_OSAL_ACCESS_RIGTHS) == 0)
      {
         if(chmod(Path, OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
         {     vWritePrintfErrmem("vPrepareLocalFileSystem -> %s chmod error %d \n",Path,errno);   }
      }
      memset(&Path[Len],0,256-Len);
      strncpy(&Path[Len],"/bosch/navdata",strlen("/bosch/navdata"));
      if(mkdir(Path,OSAL_VOLATILE_OSAL_ACCESS_RIGTHS) == 0)
      {
         if(chmod(Path, OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
         {     vWritePrintfErrmem("vPrepareLocalFileSystem -> %s chmod error %d \n",Path,errno);   }
      }
      memset(&Path[Len],0,256-Len);
      strncpy(&Path[Len],"/bosch/navdb",strlen("/bosch/navdb"));
      if(mkdir(Path,OSAL_VOLATILE_OSAL_ACCESS_RIGTHS) == 0)
      {
         if(chmod(Path, OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
         {     vWritePrintfErrmem("vPrepareLocalFileSystem -> %s chmod error %d \n",Path,errno);   }
      }
   }
}

void vSetOsalDebugRegValues(tU32 u32Fd)
{
   OSAL_trIOCtrlRegistry rReg;
   tS32 s32Val;

   rReg.pcos8Name = (tCS8*)"OSAL_TIM_CHECK";
   rReg.ps8Value  = (tU8*)&pOsalData->CheckTimerApp[0];
   rReg.u32Size   = OSAL_C_U32_MAX_NAMELENGTH;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   pOsalData->u32MqResource = (tU32)RLIM_INFINITY;
   rReg.pcos8Name = (tCS8*)"MQ_LIMIT";
   rReg.ps8Value  = (tPU8)&pOsalData->u32MqResource;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   if(pOsalData->u32MqResource == 0xffffffff)pOsalData->u32MqResource = (tU32)RLIM_INFINITY;

   /* check for supervison OSAL objects*/
   rReg.u32Size   = OSAL_C_U32_MAX_NAMELENGTH;
   rReg.s32Type   = OSAL_C_S32_VALUE_STRING;
   rReg.pcos8Name = (tCS8*)"MSG_QUEUE";
   rReg.ps8Value  = (tU8*)&pOsalData->szMqName;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"OSAL_EVENT";
   rReg.ps8Value  = (tU8*)&pOsalData->szEventName;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"OSAL_SEM";
   rReg.ps8Value  = (tU8*)&pOsalData->szSemName;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"OSAL_MTX";
   rReg.ps8Value  = (tU8*)&pOsalData->szMtxName;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"OSAL_SEM_CHECK";
   rReg.ps8Value  = (tU8*)&pOsalData->szCheckSemName;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"OSAL_SHARED_MEM";
   rReg.ps8Value  = (tU8*)&pOsalData->szShMemName;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"OSAL_STRACE_PRC";
   rReg.ps8Value  = (tU8*)&pOsalData->szSTraceProcess;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   
   pOsalData->u32CheckSemTmo = 3000;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type   = OSAL_C_S32_VALUE_S32;
   rReg.pcos8Name = (tCS8*)"OSAL_SEM_CHECK_TMO";
   rReg.ps8Value  = (tU8*)&pOsalData->u32CheckSemTmo;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"OSAL_STRACE_FILTER";
   rReg.ps8Value  = (tU8*)&pOsalData->u32STraceLevel;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
    
   s32Val = 0;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type   = OSAL_C_S32_VALUE_S32;
   rReg.pcos8Name = (tCS8*)"DUMP_MSG_POOL";
   rReg.ps8Value  = (tU8*)&s32Val;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   if(s32Val & 0x01)
   {
      pOsalData->rMsgPoolStruct.u32InvestigatePool = 1;
   }
   if(s32Val & 0x10)
   {
      pOsalData->rMsgPoolStruct.bLogPidTid = TRUE;
   }

   rReg.pcos8Name = (tCS8*)"TRACE_DEV";
   rReg.ps8Value  = (tU8*)&pOsalData->szDevTrace;
   rReg.u32Size   = 64;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg)==OSAL_E_NOERROR)
   {  
      if(!strncmp(pOsalData->szDevTrace,"all",strlen("all")))
      {
         vSetTraceForAllDeviceIo(1,0);
      }
      else
      {
         tS32 s32Dummy = 0;
         vSetDevTrace((char*)&s32Dummy,(tCString)pOsalData->szDevTrace);
      }
   }

 

   if(!strcmp(pOsalData->szSemName,"all"))
   {
      pOsalData->bSemTaceAll = TRUE;
      memset(pOsalData->szSemName,0,OSAL_C_U32_MAX_NAMELENGTH);
   }
   if(!strcmp(pOsalData->szMtxName,"all"))
   {
      pOsalData->bMutexTaceAll = TRUE;
      memset(pOsalData->szMtxName,0,OSAL_C_U32_MAX_NAMELENGTH);
   }
   if(!strcmp(pOsalData->szMqName,"all"))
   {
      pOsalData->bMqTaceAll = TRUE;
      memset(pOsalData->szMqName,0,OSAL_C_U32_MAX_NAMELENGTH);
   }
   if(!strcmp(pOsalData->szEventName,"all"))
   {
      pOsalData->bEvTaceAll = TRUE;
      memset(pOsalData->szEventName,0,OSAL_C_U32_MAX_NAMELENGTH);
   }
   if(!strcmp(pOsalData->szShMemName,"all"))
   {
     pOsalData->bShMemTaceAll = TRUE;
     /* some shared memories already created */
     trSharedMemoryElement *pCurrent = pShMemEl;
     while(pCurrent < &pShMemEl[pOsalData->SharedMemoryTable.u32UsedEntries])
     {
        pCurrent->bIsMapped =  pOsalData->bShMemTaceAll;
        pCurrent++;
     }
     memset(pOsalData->szShMemName,0,OSAL_C_U32_MAX_NAMELENGTH);
   }
}

void vSetOsalTraceRegValues(tU32 u32Fd)
{
   OSAL_trIOCtrlRegistry rReg;
   char Buffer[OSAL_C_U32_MAX_NAMELENGTH];
   int i;
   char szPathName[256];
   
   rReg.pcos8Name = (tCS8*)"OSAL_TRACE";
   rReg.ps8Value  = (tU8*)&pOsalData->u32OsalTrace;
   rReg.u32Size   = 256;
   rReg.s32Type  = OSAL_C_S32_VALUE_S32;
   if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg)==OSAL_E_NOERROR)
   {
      TraceString("General Trace level %d active",pOsalData->u32TraceLevel);
   }
   
   rReg.pcos8Name = (tCS8*)"TRACE_LOG";
   rReg.ps8Value  = (tU8*)&pOsalData->szPathTraceLog;
   rReg.u32Size   = 256;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg)==OSAL_E_NOERROR)
   {
      if(strstr(pOsalData->szPathTraceLog,".pro"))
      {
         pOsalData->u32WritToFile = 2;
      }
      else
      {
         pOsalData->u32WritToFile = 1;
      }
      TraceString("Log Trace to file %s ",pOsalData->szPathTraceLog);
   }

   rReg.pcos8Name = (tCS8*)"TRACE_LEVEL";
   rReg.ps8Value  = (tU8*)&pOsalData->u32TraceLevel;
   rReg.u32Size   = 256;
   rReg.s32Type  = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   TraceString("General Trace level %d active",pOsalData->u32TraceLevel);

   rReg.pcos8Name = (tCS8*)"TRACE_FS_ACCESS";
   rReg.ps8Value  = (tU8*)&pOsalData->bTraceAll;
   rReg.u32Size   = 256;
   rReg.s32Type  = OSAL_C_S32_VALUE_S32;
   if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg)==OSAL_E_NOERROR)
   {
      if(pOsalData->bTraceAll)TraceString("Trace Active for File System Access");
   }

   for(i=0;i<50;i++)
   {
      snprintf(szPathName,100,"Trace%d",i+1);
      rReg.pcos8Name = (tCS8*)szPathName;
      rReg.ps8Value  = (tU8*)&Buffer[0];
      rReg.u32Size   = OSAL_C_U32_MAX_NAMELENGTH;
      rReg.s32Type = OSAL_C_S32_VALUE_STRING;
      if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg) != OSAL_E_NOERROR)
      {
         break;
      }
      else
      {
        char *pTemp = (char*)OSAL_ps8StringSearchChar((tString)Buffer,',' );/*lint !e505 */
        if(pTemp)
        {
        *pTemp = 0;
        pTemp++;
        pOsalData->u32Class[i] = atoi(Buffer);
        pOsalData->u32Level[i] = atoi(pTemp);
        pOsalData->u32NrOfClass++;
        TraceString("Trace Class %d on Level %d Active",pOsalData->u32Class[i],pOsalData->u32Level[i] );
      }
        else
        {
           TraceString("Wrong Traces Class Entry %s",Buffer);
           break;
        }
      }
   }
}


void vSetOsalPoolSizeRegValues(tU32 u32Fd)
{
   OSAL_trIOCtrlRegistry rReg;
 
   rReg.pcos8Name = (tCS8*)"DEVICE_DES";
   rReg.ps8Value  = (tU8*)&pOsalData->u32DeviceHandles;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"FILE_DES";
   rReg.ps8Value  = (tU8*)&pOsalData->u32FileHandles;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"MQ_DES";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MqHandles;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"EV_DES";
   rReg.ps8Value  = (tU8*)&pOsalData->u32EventHandles;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"SEM_DES";
   rReg.ps8Value  = (tU8*)&pOsalData->u32SemHandles;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   
   rReg.pcos8Name = (tCS8*)"SEM_MAX";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MaxNrSemaphoreElements;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"MUTEX_MAX";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MaxNrMutexElements;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"EVENT_MAX";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MaxNrEventElements;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"SHMEM_MAX";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MaxNrSharedMemElements;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"TIMER_MAX";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MaxNrTimerElements;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"TASK_MAX";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MaxNrThreadElements;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   pOsalData->u32OffsetNodeElements = pOsalData->u32MaxNrThreadElements + 1;
   rReg.pcos8Name = (tCS8*)"MSGQ_MAX";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MaxNrMqElements;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   rReg.pcos8Name = (tCS8*)"PROC_MAX";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MaxNrProcElements;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   
   if(pOsalData->u32MaxNrProcElements > MAX_LINUX_PROC)pOsalData->u32MaxNrProcElements = MAX_LINUX_PROC;
}


void vSetOsalRegValues(tU32 u32Fd)
{
   tS32 i,s32Val = 0;
   OSAL_trIOCtrlRegistry rReg;
   char szPathName[256];

   rReg.pcos8Name = (tCS8*)"NO_LD_LIBRARY_PATH";
   rReg.ps8Value  = (tU8*)&s32Val;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   if(s32Val)
   {
      rReg.pcos8Name = (tCS8*)"SHARED_PATH";
      rReg.ps8Value  = (tU8*)szPathName;
      rReg.u32Size   = 256;
      rReg.s32Type = OSAL_C_S32_VALUE_STRING;
      if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg) == OSAL_E_NOERROR)
      {
         for(i=0;i<EN_SHARED_LAST;i++)
         {
            OSAL_szStringCopy(pOsalData->rLibrary[i].cLibraryNames,szPathName);
         }
      }
      else
      {
         for(i=0;i<EN_SHARED_LAST;i++)
         {
            OSAL_szStringCopy(pOsalData->rLibrary[i].cLibraryNames,OSAL_SHARED_LIB_PATH);
         }
      }
      for(i=0;i<EN_SHARED_LAST;i++)
      {
         OSAL_szStringConcat(pOsalData->rLibrary[i].cLibraryNames,szLibNames[i]);
      }
   }

   rReg.pcos8Name = (tCS8*)"USE_OSAL_TIME";
   rReg.ps8Value  = (tU8*)&pOsalData->u32UseOsalTime;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg) == OSAL_E_NOERROR)
   {
      if(pOsalData->u32UseOsalTime)TraceString("OSAL uses intern time instead of Linux Time");
   }
   rReg.pcos8Name = (tCS8*)"UPPER_CASE";
   rReg.ps8Value  = (tU8*)&pOsalData->u32UpperCase;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"MAXPOOLSIZE";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MsgPoolSize;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"ASSERT_MODE";
   rReg.ps8Value  = (tU8*)&pOsalData->u32AssertMode;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
 
   rReg.pcos8Name = (tCS8*)"USE_LIBUNWIND";
   rReg.ps8Value  = (tU8*)&pOsalData->u32UseLibunwind;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"LINUX_PRIO";
   rReg.ps8Value  = (tPU8)&pOsalData->u32Prio;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"LINUX_CGROUP";
   rReg.ps8Value  = (tPU8)&pOsalData->u32CGroup;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
   s32Val = 0;
   rReg.pcos8Name = (tCS8*)"ERRMEM_SIZE";
   rReg.ps8Value  = (tPU8)&s32Val;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   HandleMaxErrMemFilesize(s32Val);
#endif

   rReg.pcos8Name = (tCS8*)"CRYPTCARD_APP";
   rReg.ps8Value  =  (tPU8)&pOsalData->u32PrmAppId;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
 
   rReg.pcos8Name = (tCS8*)"AM_VERSION";
   rReg.ps8Value  = (tPU8)&pOsalData->u32AmVersion;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"TIMER_SIGNAL";
   rReg.ps8Value  = (tPU8)&pOsalData->u32TimSignal;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"EXC_HDR";
   rReg.ps8Value  = (tPU8)&pOsalData->u32ExcStatus;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"OSAL_MAIN";
   rReg.ps8Value  = (tU8*)&pOsalData->OsalMainApp[0];
   rReg.u32Size   = OSAL_C_U32_MAX_NAMELENGTH;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg) != OSAL_E_NOERROR)
   {
      strncpy(&pOsalData->OsalMainApp[0],"procbase",strlen("procbase"));
   }

   rReg.pcos8Name = (tCS8*)"CLK_MONO";
   rReg.ps8Value  = (tU8*)&pOsalData->u32ClkMonotonic;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"MQ_POST_TMO";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MqPostTmo;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"TIM_RESOLUTION";
   rReg.ps8Value  = (tU8*)&pOsalData->u32TimerResolution;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"NO_MQ_BLOCK_ENTRY";
   rReg.ps8Value  = (tU8*)&pOsalData->u32NoMqBlockEntry;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"CATCH_SIGCHILD";
   rReg.ps8Value  = (tU8*)&pOsalData->u32CatchSigchild;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   s32Val = 0;
   rReg.pcos8Name = (tCS8*)"LOG_ERROR";
   rReg.ps8Value  = (tU8*)&s32Val;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg) == OSAL_E_NOERROR)
   {
      pOsalData->bLogError = s32Val;
   }

   rReg.pcos8Name = (tCS8*)"REC_OSAL_LOCK";
   rReg.ps8Value  = (tU8*)&pOsalData->u32RecLockAccess;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"ALLOW_EXIT";
   rReg.ps8Value  = (tU8*)&pOsalData->u32AllowExit;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"VARIABLE_PATH";
   rReg.ps8Value  = (tU8*)&pOsalData->szNavDatPath;
   rReg.u32Size   = MAX_MOUNTPOINT_STRING_SIZE;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg)==OSAL_E_NOERROR)
   {
       TraceString("/dev/data points to %s",pOsalData->szNavDatPath);
   }

   rReg.pcos8Name = (tCS8*)"ERRMEM_DUMP_PATH";
   rReg.ps8Value  = (tU8*)&pOsalData->szErrMemDumpPath;
   rReg.u32Size   = MAX_MOUNTPOINT_STRING_SIZE;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);

   rReg.pcos8Name = (tCS8*)"FS_TO_LOCAL";
   rReg.ps8Value  = (tU8*)&pOsalData->u32FsToLocal;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   if(pOsalData->u32FsToLocal == 1)
   {
      TraceString("Buildup local file system");
      vPrepareLocalFileSystem();
   }

   rReg.pcos8Name = (tCS8*)"MMAP_MSG_SIZE";
   rReg.ps8Value  = (tU8*)&pOsalData->u32MmapMsgSize;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
   
   rReg.pcos8Name = (tCS8*)"MIN_DYN_MMAP_MSG_SIZE";
   rReg.ps8Value  = (tU8*)&pOsalData->u32DynMmapMsgSize;
   rReg.u32Size   = sizeof(tS32);
   rReg.s32Type = OSAL_C_S32_VALUE_S32;
   REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
  
   /* set system wide default exception handler behaviour if nonone has defined defined it for the specific process i*/
   if(s32ExcHdrStatus == 0)s32ExcHdrStatus = pOsalData->u32ExcStatus;
   for(i=0;i<MAX_BOSCH_CONF_APP;i++)
   {
      snprintf(szPathName,100,"EXCHDR_APP%d",i+1);
      rReg.pcos8Name = (tCS8*)szPathName;
      rReg.ps8Value  = (tU8*)&pOsalData->rBoschExcHdrApps[i].szAppName[0];
      rReg.u32Size   = OSAL_C_U32_MAX_NAMELENGTH;
      rReg.s32Type = OSAL_C_S32_VALUE_STRING;
      if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg) != OSAL_E_NOERROR)break;
   }

   for(i=0;i<MAX_BOSCH_CONF_APP;i++)
   {
      snprintf(szPathName,100,"TIM_APP%d",i+1);
      rReg.pcos8Name = (tCS8*)szPathName;
      rReg.ps8Value  = (tU8*)&pOsalData->rNoSigTimerApp[i].szAppName[0];
      rReg.u32Size   = OSAL_C_U32_MAX_NAMELENGTH;
      rReg.s32Type = OSAL_C_S32_VALUE_STRING;
      if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg) != OSAL_E_NOERROR)break;
   }

   for(i=0;i<MAX_BOSCH_CONF_APP;i++)
   {
      snprintf(szPathName,100,"EXIT_APP%d",i+1);
      rReg.pcos8Name = (tCS8*)szPathName;
      rReg.ps8Value  = (tU8*)&pOsalData->rExitApp[i].szAppName[0];
      rReg.u32Size   = OSAL_C_U32_MAX_NAMELENGTH;
      rReg.s32Type = OSAL_C_S32_VALUE_STRING;
      if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg) != OSAL_E_NOERROR)break;
   }

   for(i=0;i<MAX_BOSCH_CONF_APP;i++)
   {
      snprintf(szPathName,100,"RT_APP%d",i+1);
      rReg.pcos8Name = (tCS8*)szPathName;
      rReg.ps8Value  = (tU8*)&pOsalData->rRtApp[i].szAppName[0];
      rReg.u32Size   = OSAL_C_U32_MAX_NAMELENGTH;
      rReg.s32Type = OSAL_C_S32_VALUE_STRING;
      if(REG_u32IOControl(u32Fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg) != OSAL_E_NOERROR)break;
   }
}


void vCheckRegOverride(void)
{
   /* check if osal.reg exist to overide values */
   uintptr_t u32Fd = 0;

   /* check if OSAL registry is located in /opt/bosch/base/registry */
   /* update with project specific changes */
   char Path1[64] = "/opt/bosch/base/registry/osal.reg";
   char Path2[10] = "osal.reg";
   if(bCreateRegFromFileNativ(&Path1[0]) == TRUE)
   {
      TraceString("osal.reg taken from absolute path %s via PID:%d",Path1,getpid());
      u32Fd = 1;
   }
   else if(bCreateRegFromFileNativ(&Path2[0]) == TRUE)
   {
      TraceString("osal.reg taken from local pwd %s via PID:%d",Path2,getpid());
      u32Fd = 1;
   }
   if(u32Fd == 1)
   {
      pOsalData->u32ConfigCheckPid = getpid();
      // already locked by loading semaphore
      if(REG_u32IOOpen("LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL",OSAL_EN_READWRITE,&u32Fd) == OSAL_E_NOERROR)
      {
        vSetOsalRegValues(u32Fd);
        REG_u32IOClose(u32Fd);
      }
      else
      {
         TraceString("Open LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL failed"); 
      }
  
      if(REG_u32IOOpen("LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/POOLSIZES",OSAL_EN_READWRITE,&u32Fd) == OSAL_E_NOERROR)
      {
         vSetOsalPoolSizeRegValues(u32Fd);
         REG_u32IOClose(u32Fd);
      }
      if(REG_u32IOOpen("LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/INTERNAL_TRACE",OSAL_EN_READWRITE,&u32Fd) == OSAL_E_NOERROR)
      {
         vSetOsalTraceRegValues(u32Fd);
         REG_u32IOClose(u32Fd);
      }
      if(REG_u32IOOpen("LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL/DEBUG",OSAL_EN_READWRITE,&u32Fd) == OSAL_E_NOERROR)
      {
         vSetOsalDebugRegValues(u32Fd);
         REG_u32IOClose(u32Fd);
      }
   }
   else
   {
      strncpy(&pOsalData->OsalMainApp[0],"procbase",strlen("procbase"));
   }
}

void vSetupRegistry(void)
{
   uintptr_t u32Fd = 0;
   int i;
    
   //  already locked by loading semaphore
   if(REG_u32IOCreate("LOCAL_MACHINE",OSAL_EN_READWRITE,&u32Fd) != OSAL_E_NOERROR)
   {  TraceString("vSetupRegistry LOCAL_MACHINE failed %d",errno);       }
   else
   {  REG_u32IOClose(u32Fd);   }
   if(REG_u32IOCreate("LOCAL_MACHINE/SOFTWARE",OSAL_EN_READWRITE,&u32Fd) != OSAL_E_NOERROR)
   {   TraceString("vSetupRegistry SOFTWARE failed");   }
   else
   {  REG_u32IOClose(u32Fd);   }
   if(REG_u32IOCreate("LOCAL_MACHINE/SOFTWARE/BLAUPUNKT",OSAL_EN_READWRITE,&u32Fd) != OSAL_E_NOERROR)
   {   TraceString("vSetupRegistry BLAUPUNKT failed");   }
   else
   {  REG_u32IOClose(u32Fd);   }
   if(REG_u32IOCreate("LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS",OSAL_EN_READWRITE,&u32Fd) != OSAL_E_NOERROR)
   {   TraceString("vSetupRegistry VERSIONS failed");   }
   else
   {  REG_u32IOClose(u32Fd);   }
   if(REG_u32IOCreate("LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL",OSAL_EN_READWRITE,&u32Fd) != OSAL_E_NOERROR)
   {   TraceString("vSetupRegistry OSAL failed");   }
   else
   {  REG_u32IOClose(u32Fd);   }

   if(REG_u32IOOpen("LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL",OSAL_EN_READWRITE,&u32Fd) == OSAL_E_NOERROR)
   {
       /*set default values */
       vSetDefaultRegValues(u32Fd);
       for(i=0;i<EN_SHARED_LAST;i++)
       {
          OSAL_szStringConcat(pOsalData->rLibrary[i].cLibraryNames,szLibNames[i]);
       }
       pOsalData->u32ExcStatus = OSAL_EXCHDR_STATUS;
       pOsalData->u32PrmAppId  = 0xffffffff; /*set default value*/
       pOsalData->u32AmVersion = 2;
       REG_u32IOClose(u32Fd);
   }
   else
   {
       TraceString("vSetupRegistry REG_u32IOOpen LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL failed Error:%d", OSAL_u32ErrorCode()); 
   }

    /* override existing values if osal.reg exists */
    vCheckRegOverride();

    if(pOsalData->u32MsgPoolSize == 0)
    {
       pOsalData->u32MsgPoolSize = OSAL_MSG_POOL_SIZE;
    }
    HandleMaxErrMemFilesize(0);
}

#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file Devinit.c
|-----------------------------------------------------------------------*/
