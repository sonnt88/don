/******************************************************************************
| FILE:         osal_aio.c
| PROJECT:      GMGE 
| SW-COMPONENT: OSAL IO
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL 
|               (Operating System Abstraction Layer) Asynchronous IO -Functions.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 12.02.08  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define AIO_TRACE_LEVEL TR_LEVEL_COMPONENT

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

struct OsalDevices {
    const char*   name;                         /* name of the device */
    /*lint -esym(754,OsalDevices::ptr_s32IOOpen) OsalDevices::ptr_s32IOOpen is not referenced PQM_authorized_435*/       
    const tOpenPtr      ptr_s32IOOpen;          /* pointer to open function */
    /*lint -esym(754,OsalDevices::ptr_s32IOClose) OsalDevices::ptr_s32IOClose is not referenced PQM_authorized_435*/       
    const tClosePtr     ptr_s32IOClose;         /* pointer to close function */
    /*lint -esym(754,OsalDevices::ptr_s32IOCreate) OsalDevices::ptr_s32IOCreate is not referenced PQM_authorized_435*/        
    const tCreatePtr    ptr_s32IOCreate;        /* pointer to create function */
    /*lint -esym(754,OsalDevices::ptr_s32IORemove) OsalDevices::ptr_s32IORemove is not referenced PQM_authorized_435*/        
    const tRemovePtr    ptr_s32IORemove;        /* pointer to remove function */
    /*lint -esym(754,OsalDevices::ptr_s32IOControl) OsalDevices::ptr_s32IOControl is not referenced PQM_authorized_435*/        
    const tIOControlPtr ptr_s32IOControl;       /* pointer to control function */
    /*lint -esym(754,OsalDevices::ptr_s32IORead) OsalDevices::ptr_s32IORead is not referenced PQM_authorized_435*/        
    const tReadPtr      ptr_s32IORead;          /* pointer to read function */
    /*lint -esym(754,OsalDevices::ptr_s32IOWrite) OsalDevices::ptr_s32IOWrite is not referenced PQM_authorized_435*/        
    const tWritePtr     ptr_s32IOWrite;         /* pointer to write function */
    /*lint -esym(754,OsalDevices::device_local_id) OsalDevices::device_local_id is not referenced PQM_authorized_435*/        
    const tU32          device_local_id;        /* device id (internally used for the driver */
    /*lint -esym(754,OsalDevices::bFileSystem) OsalDevices::bFileSystem is not referenced PQM_authorized_435*/        
    const tBool         bFileSystem;            /* TRUE -> is a file system */
    /*lint -esym(754,OsalDevices::ExclusiveAccess) OsalDevices::ExclusiveAccess is not referenced PQM_authorized_435*/        
    OSAL_tenDevAccess   ExclusiveAccess;        /* exclusive access variable */
    /*lint -esym(754,OsalDevices::nJobQueue) OsalDevices::nJobQueue is not referenced PQM_authorized_435*/        
    tU32 nJobQueue;                             /* jobqueue number for no filesystems */
    /*lint -esym(754,OsalDevices::prm_index) OsalDevices::prm_index is not referenced PQM_authorized_435*/        
    tS32 prm_index;                             /* prm index for exclusive access */
 }; /*lint !e612 PQM_authorized_435*/


/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

static OSAL_trAsyncNode*    AIO_aprJobList[C_MAX_JOBS] = {NULL,NULL,NULL};
static OSAL_tThreadID       AIO_ahThread[C_MAX_JOBS]   = {0,0,0};
static OSAL_trAsyncNode     AIO_arShutdownNode[C_MAX_JOBS];
static tU32                 AIO_nNextJob = 0;
static tBool                bAsyncIoAvailable = FALSE;
static pthread_once_t AIO_is_initialized = PTHREAD_ONCE_INIT;

tS32                        AIO_aJobFlg[C_MAX_JOBS];
tS32                        AIO_JobTrigger;
tBool bAIO = FALSE;

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
char cErrMemBuffer[100];



/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
static tS32 OSAL_Async_s32IOStart(OSAL_trAsyncControl* prAIO, OSAL_tenAccess enAccess);
static tS32 OSAL_Async_s32IOCancel( OSAL_tIODescriptor fd, OSAL_trAsyncControl const * prAIO );
//static OSAL_tenAsyncState OSAL_ASync_enGetState( OSAL_trAsyncControl const * prAIO );
static tVoid vIOAsyncThread(tPVoid pvArg);
static tBool bEnqueueNode(tUInt nJobQueue, OSAL_trAsyncNode* prNode);
void OSAL_vShutdownAsyncIO(void);



//static tU32 u32CheckDescriptor( OsalDeviceDescriptor * fd )
//{
//   if( ( fd == NULL ) || ( (tU32)fd == OSAL_C_INVALID_HANDLE ) )
//   {                                 /* fd contains above defines, not good code, but no assert */
//      return OSAL_E_INVALIDVALUE;
//   }
//
//   if( OSAL_DESCRIPTOR_MAGIC != fd->magic )
//   {                                         /* fd hande is not open */
//      return OSAL_E_BADFILEDESCRIPTOR;
//   }
//   return OSAL_E_NOERROR;
//}


tS32 s32EnterErrMem(const char* pBuffer)
{
  OSAL_tIODescriptor hErrMem;
  char cBuffer[ERRMEM_MAX_ENTRY_LENGTH];
  tS32 s32Len;
  cBuffer[0] = (tChar) 0;//(TR_COMP_OSALCORE & 0xFF);
  cBuffer[1] = (tChar) (TR_COMP_OSALCORE >> 8) & 0xFF;
  cBuffer[2] = (tChar) OSAL_STRING_OUT;
  s32Len = (tS32)strlen(pBuffer);
  if(s32Len > (ERRMEM_MAX_ENTRY_LENGTH-4)) s32Len = ERRMEM_MAX_ENTRY_LENGTH-4;
  memcpy(&cBuffer[3],pBuffer,(tU32)s32Len);
  cBuffer[ERRMEM_MAX_ENTRY_LENGTH-1] = 0;

  hErrMem = OSAL_IOOpen( OSAL_C_STRING_DEVICE_ERRMEM, OSAL_EN_WRITEONLY);
  if(hErrMem != OSAL_ERROR)
  {
      trErrmemEntry  rErrmemEntry = {0};
      rErrmemEntry.u16Entry = TR_COMP_OSALCORE;
      (tVoid) OSAL_s32ClockGetTime(&rErrmemEntry.rEntryTime);
      rErrmemEntry.eEntryType = eErrmemEntryNormal;
      rErrmemEntry.u16EntryLength = 100;
      (tVoid) OSAL_pvMemoryCopy(rErrmemEntry.au8EntryData, cBuffer,ERRMEM_MAX_ENTRY_LENGTH);
      if(OSAL_s32IOWrite(hErrMem, (tPCS8)&rErrmemEntry, sizeof(rErrmemEntry)) == OSAL_OK)
      {
         s32Len = OSAL_OK;
      }
        else
      {
           s32Len = OSAL_ERROR;
      }
      OSAL_s32IOClose(hErrMem);
  }
  else
  {
     s32Len = OSAL_ERROR;
  }
  return s32Len;
}




/* Async IO */

/******************************************************************************
 * FUNCTION:     s32PrepAsyncTask
 *
 * DESCRIPTION:  Start of asynchronous I/O part
 *
 * PARAMETER:    -
 *
 * RETURNVALUE:  Error Code
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 28.09.05  |   Initial revision                   | MRK2HI
 *****************************************************************************/
void vPrepAsyncTask()
   {
     if(!OSAL_bInitAsyncIO())
    {
         FATAL_M_ASSERT_ALWAYS();
   }
     else
     {
         bAIO = TRUE;
     }
}

/******************************************************************************
 * FUNCTION:     s32ShutdownAsyncTask
 *
 * DESCRIPTION:  Shutdown of asynchronous I/O part
 *
 * PARAMETER:    -
 *
 * RETURNVALUE:  Error Code
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 28.09.05  |   Initial revision                   | MRK2HI
 *****************************************************************************/
tS32 s32ShutdownAsyncTask(void)
{
   tS32 s32RetVal= OSAL_OK;
   if (bAsyncIoAvailable)
   {
      OSAL_vShutdownAsyncIO();
      CloseOsalLock(&gpOsalProcDat->rAsyncIOLock);
      DeleteOsalLock(&gpOsalProcDat->rAsyncIOLock);
   }
   return s32RetVal;
}

/******************************************************************************
 * FUNCTION:     bLockAsync
 *
 * DESCRIPTION:  Lock access to the asynchronous I/O data structures.
 *
 * PARAMETER:    -
 *
 * RETURNVALUE:  TRUE on success, FALSE on failure
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 22.09.03  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tBool bLockAsync(void)
{
   if(LockOsal(&gpOsalProcDat->rAsyncIOLock) == OSAL_OK)
   {
     return TRUE;
   }
   else
   {
     return FALSE;
   }
}

/******************************************************************************
 * FUNCTION:     vUnlockAsync
 *
 * DESCRIPTION:  Release the lock around the asynchronous I/O data structures.
 *
 * PARAMETER:    -
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 22.09.03  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static void vUnlockAsync(void)
{
   UnLockOsal(&gpOsalProcDat->rAsyncIOLock);
}

/******************************************************************************
 * FUNCTION:     prAllocAsyncNode
 *
 * DESCRIPTION:  Allocate a new OSAL_trAsyncNode and initialize it for
 *               further use.
 *
 * PARAMETER:    prAIO:    (->I)
 *                  Pointer to the AsyncControl block for the job.
 *
 *               enAccess: (I)
 *                  Indicates whether the job is to be set up for
 *                  read or write access.
 *
 * RETURNVALUE:  An initialized OSAL_trAsyncNode, or NULL if there was
 *               not enough free memory.
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 22.09.03  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static OSAL_trAsyncNode* prAllocAsyncNode(OSAL_trAsyncControl* prAIO, OSAL_tenAccess enAccess)
{
   OSAL_trAsyncNode* prNode = (OSAL_trAsyncNode*)OSAL_pvMemoryAllocate( sizeof(OSAL_trAsyncNode) );

   if( prNode != NULL )
   {
#ifdef OSAL_AIO_PRIORITY_CONTROL
      OSAL_trThreadControlBlock rThread;
#endif
      prNode->prAsyncControl = prAIO;
      prNode->enAccess       = enAccess;
      prNode->enState        = OSAL_EN_ASYNC_PENDING;
      prNode->nJobQueue      = C_INVALID_JOB;
      prNode->prNext         = NULL;
#ifdef ENTER_ASYNC_IN_DD
     /* enter prAIO in device descriptor */
      ((OsalDeviceDescriptor*)prNode->prAsyncControl->id)->pAIO = prAIO;
#endif


#ifdef OSAL_AIO_PRIORITY_CONTROL
     /* The priority of the job is set to the priority of
       * the calling thread, if possible */
      if( OSAL_s32ThreadControlBlock( OSAL_ThreadWhoAmI(), &rThread) == OSAL_OK )
      {
         prNode->u32Priority = rThread.u32CurrentPriority;
      }
      else
      {
         prNode->u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
      }
#endif

      /* ERROR (IN PROGRESS) as long as we are not finished */
      prAIO->s32Status = OSAL_ERROR;
   }
   return prNode;
}

/******************************************************************************
 * FUNCTION:     vFreeAsyncNode
 *
 * DESCRIPTION:  Delete an OSAL_trAsyncNode.
 *               The node must not be enqueued any more in any list!
 *
 * PARAMETER:    prNode: (->I)
 *                  The node that is to be freed.
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 22.09.03  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tVoid vFreeAsyncNode(OSAL_trAsyncNode* prNode)
{
#ifdef ENTER_ASYNC_IN_DD
   if((OsalDeviceDescriptor*)prNode->prAsyncControl->id != 0)
   {
     ((OsalDeviceDescriptor*)prNode->prAsyncControl->id)->pAIO = NULL;
   }
#endif
   OSAL_vMemoryFree(prNode);
}

/******************************************************************************
 * FUNCTION:     OSAL_bInitAsyncIO
 *
 * DESCRIPTION:  Set up the asynchronous IO facility: start the async IO
 *               threads and set up the necessary variables.
 *
 * PARAMETER:    -
 *
 * RETURNVALUE:  TRUE on success, FALSE on failure
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 18.09.05  |   Initial revision                   | MRK2HI
 *****************************************************************************/
tBool OSAL_bInitAsyncIO(tVoid)
{
   uintptr_t nIdx;
   tBool bSuccess = FALSE;
   tChar szName[32];
   tS32 s32Pid = OSAL_ProcessWhoAmI();

   OSAL_s32PrintFormat(szName, "ASYNC%d", s32Pid);
   if(CreateOsalLock(&gpOsalProcDat->rAsyncIOLock, szName) != OSAL_OK)
   {   FATAL_M_ASSERT_ALWAYS();   }


   AIO_nNextJob = 0;

   for (nIdx = 0; nIdx < C_MAX_JOBS; ++nIdx)
   {
      AIO_aprJobList[nIdx] = NULL;
      AIO_ahThread[nIdx] = OSAL_ERROR;

      AIO_arShutdownNode[nIdx].u32Priority = 0;
      AIO_arShutdownNode[nIdx].enState = OSAL_EN_ASYNC_SHUTDOWN;
      AIO_arShutdownNode[nIdx].prAsyncControl = NULL;
      AIO_arShutdownNode[nIdx].prNext = NULL;
   }

   OSAL_s32PrintFormat(szName, "AIOJOBTRIG%d",s32Pid);
   if((AIO_JobTrigger = CreSyncObj(&szName[0],0)) != OSAL_ERROR)
   {
      bSuccess = TRUE;

      for (nIdx = 0; nIdx < C_MAX_JOBS; ++nIdx)
      {
         OSAL_trThreadAttribute    rAttr;

         OSAL_s32PrintFormat(szName, "AIO_%d_%d", (tU32)nIdx, s32Pid);
         if((AIO_aJobFlg[nIdx] = CreSyncObj(&szName[0],0)) == OSAL_ERROR)
         {
            bSuccess = FALSE;
            NORMAL_M_ASSERT_ALWAYS();
         }
         else
         {
            rAttr.szName       = szName;
            rAttr.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
            rAttr.s32StackSize = 10240;
            rAttr.pfEntry      = vIOAsyncThread;
            rAttr.pvArg        = (tPVoid)nIdx;

            /* create a new thread for the I/O operation */
            AIO_ahThread[nIdx] = OSAL_ThreadSpawn(&rAttr);
            if (AIO_ahThread[nIdx] == OSAL_ERROR)
            {
               bSuccess = FALSE;
               NORMAL_M_ASSERT_ALWAYS();
            }
         }
      }
   }
   return bSuccess;
}

/******************************************************************************
 * FUNCTION:     OSAL_vShutdownAsyncIO
 *
 * DESCRIPTION:  Free all resources acuired with OSAL_bInitAsyncIO.
 *
 * PARAMETER:    -
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 18.09.03  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
void OSAL_vShutdownAsyncIO(void)
{
   tU32 nJob;
  /* Trigger shutdown of the job threads by enqueuing a special
    * shutdown job */
   if (bLockAsync() == TRUE)
   {
      for (nJob = 0; nJob < C_MAX_JOBS; ++nJob)
      {
         bEnqueueNode(nJob, &AIO_arShutdownNode[nJob]);
      }
      vUnlockAsync();
   }

   for (nJob = 0; nJob < C_MAX_JOBS; ++nJob)
   {
      if (AIO_ahThread[nJob] != OSAL_ERROR)
      {
      }
   }
   if(WaiSyncObj(AIO_JobTrigger,100) != OSAL_E_NOERROR)
   {
     NORMAL_M_ASSERT_ALWAYS();
   }
   for (nJob = 0; nJob < C_MAX_JOBS; ++nJob)
   {
         if (AIO_ahThread[nJob] != OSAL_ERROR)
         {
            OSAL_s32ThreadDelete(AIO_ahThread[nJob]);
         }
		 if(DelSyncObj(AIO_aJobFlg[nJob]) != OSAL_E_NOERROR)
		 {
		    NORMAL_M_ASSERT_ALWAYS();
		 }
   }
}

/************************************************************************
*                                                                       *
* FUNCTIONS                                                             *
*                                                                       *
*      IsNfsDev                                                         *
*                                                                       *
* DESCRIPTION                                                           *
*      checks if the given file descriptor is from a non-file system    *
*                                                                       *
*                                                                       *
* INPUTS                                                                *
*                                                                       *
*      OSAL_tIODescriptor fd                                            *
*                                                                       *
* OUTPUTS                                                               *
*                                                                       *
*      TRUE if the file descriptor is not from a file system            *
*                                                                       *
************************************************************************/
tBool IsNfsDev(OSAL_tIODescriptor fd)
{
  OsalDeviceDescriptor *fd_local;
  fd_local = (OsalDeviceDescriptor *)fd;
  if (fd_local->device_ptr)
  {
    return !(fd_local->device_ptr->bFileSystem);
  }
  else
  {
    return FALSE;
  }
}

/******************************************************************************
 * FUNCTION:     vSetJobQueue
 *
 * DESCRIPTION:  Set the job queue identifier for file descriptor to a
 *               given value.
 *
 * PARAMETER:    fd:        (I)
 *                  The file descriptor whose job queue identifier is to
 *                  be set.
 *
 *               nJobQueue: (I)
 *                  The value that is to be stored in the file descriptor.
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 18.09.03  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
tVoid vSetJobQueue(OSAL_tIODescriptor fd, tUInt nJobQueue)
{
  OsalDeviceDescriptor *fd_local;

  fd_local = (OsalDeviceDescriptor *)fd;

   if (OSAL_DESCRIPTOR_MAGIC != fd_local->magic)
   {
      return;
   }
   if (IsNfsDev(fd))
   {
      ((OsalDeviceDescriptor*)fd)->device_ptr->nJobQueue = nJobQueue;
   }
   else
   {
      ((OsalDeviceDescriptor*)fd)->nJobQueue = nJobQueue;
   }
}


/******************************************************************************
 * FUNCTION:     nFindJobQueue
 *
 * DESCRIPTION:  Search for a job queue that can be used by the given file
 *               descriptor.
 *
 *               If possible, an empty queue is selected.
 *
 * PARAMETER:    fd:     (I)
 *                  The file descriptor where a job node is to be enqueued.
 *
 * RETURNVALUE:  The number of the selected job queue, or C_INVALID_JOB in
 *               case of an error.
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 18.09.03  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
tUInt nFindJobQueue(OSAL_tIODescriptor fd)
{
  OsalDeviceDescriptor *fd_local;
  tUInt nQueue = C_INVALID_JOB;
  
  fd_local = (OsalDeviceDescriptor *)fd;
  if (OSAL_DESCRIPTOR_MAGIC != fd_local->magic)
  {
      return C_INVALID_JOB;
  }
  if (IsNfsDev(fd))
  {
      nQueue = ((OsalDeviceDescriptor*)fd)->device_ptr->nJobQueue;
  }
  else
  {
      nQueue = ((OsalDeviceDescriptor*)fd)->nJobQueue;
  }

   /* If this FD has no job queue yet, find a free queue */
  if (nQueue == C_INVALID_JOB)
  {
      tU32 nNextQueue = AIO_nNextJob;
      tBool bFound = FALSE;
      do
      {
         if (AIO_aprJobList[nNextQueue] == NULL)
         {
            bFound = TRUE;
         }
         else
         {
            nNextQueue = (nNextQueue + 1) % C_MAX_JOBS;
         }
      }
      while ((bFound == FALSE) && (nNextQueue != AIO_nNextJob));

      nQueue = nNextQueue;
      vSetJobQueue(fd, nQueue);
      AIO_nNextJob = (nNextQueue + 1) % C_MAX_JOBS;
  }

   return nQueue;
}


/******************************************************************************
 * FUNCTION:     prFindNode
 *
 * DESCRIPTION:  Searches for a given asynchronous I/O control block and
 *               returns a pointer to its job node as well as to the job
 *               node preceeding it in the queue.
 *
 * PARAMETER:    prAIO:   (->I)
 *                  The control block that is to be found. The search is
 *                  done on the file descriptor contained within the block.
 *
 *               pprPrev: (->O)
 *                  Pointer to a variable that will receive a pointer to
 *                  the node preceeding the found node.
 *
 *                  This parameter may be set to NULL, if the caller is not
 *                  interested in this information.
 *
 * RETURNVALUE:  A pointer to the found job node, or NULL if there is no
 *               job node for the control block yet.
 *
 *               If a job node has been found and 'pprPrev' is != NULL,
 *               a pointer to the job node preceeding the found one is stored
 *               in 'pprPrev'. If the found job node is at the head of the
 *               queue, 'pprPrev' will be set to NULL.
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 25.11.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static OSAL_trAsyncNode* prFindNode( const OSAL_trAsyncControl* prAIO, OSAL_trAsyncNode** pprPrev )
{
   OSAL_trAsyncNode* prCur = NULL;
   OSAL_trAsyncNode* prDummy;
   tUInt nQueue = nFindJobQueue(prAIO->id);

   if (pprPrev == NULL)
   {
      pprPrev = &prDummy;
   }

   if(nQueue != C_INVALID_JOB)
   {
      prCur = AIO_aprJobList[nQueue];
   }

   *pprPrev = NULL;

   while( (prCur != NULL) && (prCur->prAsyncControl != prAIO) )
   {
      *pprPrev = prCur;
      prCur = prCur->prNext;
   }
   return(prCur);
}

/******************************************************************************
 * FUNCTION:     prGetNextNode
 *
 * DESCRIPTION:  Returns the next node that is enqueued for asynchronous
 *               I/O at a given file descriptor.
 *
 * PARAMETER:    fd:     (I)
 *                  File descriptor whose asynchronous I/O queue is to be
 *                  checked.
 *
 *               prPrev: (->I)
 *                  Pointer to the starting point (usually the result of
 *                  a previous call to prGetNextNode()).
 *
 *                  If NULL, the search starts at the head of the job
 *                  list.
 *
 * RETURNVALUE:  A pointer to the next enqueued node, or NULL if there is
 *               no more node within the queue.
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 25.11.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static OSAL_trAsyncNode* prGetNextNode( OSAL_tIODescriptor fd, const OSAL_trAsyncNode* prPrev )
{
   OSAL_trAsyncNode*  prNode = NULL;

   if (prPrev == NULL)
   {
      tUInt nQueue = nFindJobQueue(fd);

      if (nQueue != C_INVALID_JOB)
      {
         prPrev = AIO_aprJobList[nQueue];
      }
   }

   if (prPrev != NULL)
   {
      prNode = prPrev->prNext;
      while ((prNode != NULL) && (prNode->prAsyncControl != NULL) &&
             (prNode->prAsyncControl->id != fd))
      {
         prNode = prNode->prNext;
      }
   }

   return(prNode);
}

/******************************************************************************
 * FUNCTION:     bEnqueueNode
 *
 * DESCRIPTION:  Enqueue a node for an asynchronous I/O job into the job
 *               queue.
 *
 *               The queue is sorted with descending priority. The new node
 *               is added after all nodes with higher or equal priority.
 *
 * PARAMETER:    nJobQueue: (I)
 *                  The queue for the job.
 *
 *               prNode:     (->I)
 *                  The job node that is to be enqueued.
 *
 * RETURNVALUE:  TRUE if everything went well, FALSE in case of an error.
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 25.11.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tBool bEnqueueNode( tUInt nJobQueue, OSAL_trAsyncNode* prNode )
{
   tBool bSuccess = FALSE;

   if (nJobQueue < C_MAX_JOBS)
   {
      /* If the queue is empty, just add it at the top */
      if (AIO_aprJobList[nJobQueue] == NULL)
      {
         AIO_aprJobList[nJobQueue] = prNode;
      }
      else
      {
         OSAL_trAsyncNode* prCur  = AIO_aprJobList[nJobQueue];
         OSAL_trAsyncNode* prPrev = NULL;

         /* Search for the first node with lower priority (thread priorities:
          * the higher the numeric value, the lower the priority), or till
          * the end of the queue
          */
         while ((prCur != NULL) && (prCur->u32Priority <= prNode->u32Priority))
         {
            prPrev = prCur;
            prCur  = prCur->prNext;
         }

         /* Link the node into the queue */
         if( prPrev == NULL )
         {
            prNode->prNext = AIO_aprJobList[nJobQueue];
            AIO_aprJobList[nJobQueue] = prNode;
         }
         else
         {
            prNode->prNext = prPrev->prNext;
            prPrev->prNext = prNode;
         }
      }
      prNode->nJobQueue = nJobQueue;

      /* Another job for the job thread */
	  RelSyncObj(AIO_aJobFlg[nJobQueue]);
      bSuccess = TRUE;
   }
   return bSuccess;
}

/******************************************************************************
* FUNCTION:     vUnlinkNode
 *
 * DESCRIPTION:  Remove the job node corresponding to an asynchronous I/O
 *               control block from the queue.
 *
 * PARAMETER:    prAIO: (->I)
 *                  The control block whose job node ist to be unlinked.
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 25.11.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tVoid vUnlinkNode( OSAL_trAsyncNode* prNode )
{
   OSAL_trAsyncNode* prCur = AIO_aprJobList[prNode->nJobQueue];

   if( prCur == prNode )
   {
         AIO_aprJobList[prNode->nJobQueue] = prNode->prNext;
   }
   else
   {
      OSAL_trAsyncNode* prPrev = prNode;

      while( (prCur != NULL) && (prCur != prNode) )
      {
         prPrev = prCur;
         prCur = prCur->prNext;
      }
      if( prCur != NULL )
      {
         prPrev->prNext = prNode->prNext;
      }
   }

   if( prGetNextNode( prNode->prAsyncControl->id, prNode ) == NULL )
   {
      vSetJobQueue( prNode->prAsyncControl->id, C_INVALID_JOB );
   }
}

/******************************************************************************
 * FUNCTION:     s32CancelAsync
 *
 * DESCRIPTION:  Tries to cancel an asynchronous I/O job.
 *
 *               If a job is currently in the WORKING state, i.e. currently
 *               within a Read() or Write(), this job cannot be cancelled.
 *
 *               If a job has successfully been cancelled, the status and
 *               error data within its control block gets updated accordingly,
 *               the job node gets removed from the list of jobs and its
 *               memory is freed.
 *
 * PARAMETER:    prNode:   (->I)
 *                  The job node of the job that is to be cancelled.
 *
 * RETURNVALUE:  - OSAL_C_S32_AIO_ALLDONE if the job had already been finished
 *               - OSAL_C_S32_AIO_CANCELED if the job has been cancelled
 *               - OSAL_C_S32_AIO_NOTCANCELED otherwise
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 25.11.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tS32 s32CancelAsync(OSAL_trAsyncNode* prNode)
{
   tS32 s32Status = OSAL_C_S32_AIO_ALLDONE;

   if( prNode != NULL )
   {
      /* If the job is currently active, i.e. within a Read() or Write(),
       * it cannot be cancelled. */
      if( prNode->enState == OSAL_EN_ASYNC_WORKING )
      {
         s32Status = OSAL_C_S32_AIO_NOTCANCELED;
      }
      else if( prNode->enState != OSAL_EN_ASYNC_DONE )
      {
           prNode->prAsyncControl->u32ErrorCode = OSAL_E_CANCELED;
           prNode->prAsyncControl->s32Status    = OSAL_ERROR;
           prNode->enState = OSAL_EN_ASYNC_CANCELLED;
           s32Status = OSAL_C_S32_AIO_CANCELED;

         vUnlinkNode( prNode );
         vFreeAsyncNode( prNode );
     }
   }
   return(s32Status);
}

/******************************************************************************
 * FUNCTION:     prGetNextJob
 *
 * DESCRIPTION:  Return the next queued job from the given job queue.
 *
 * PARAMETER:    nJobQueue: (I)
 *                  ID of the job queue (0 <= nJobQueue < C_MAX_JOBS).
 *
 * RETURNVALUE:  The next job in the queue, or NULL if there are no jobs in
 *               the queue.
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 18.09.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static OSAL_trAsyncNode* prGetNextJob(tUInt nJobQueue)
{
   OSAL_trAsyncNode* prJob = NULL;

   if (nJobQueue < C_MAX_JOBS)
   {
      prJob = AIO_aprJobList[nJobQueue];

      if( prJob != NULL )
      {
         if( prJob->enState == OSAL_EN_ASYNC_SHUTDOWN )
         {  // set to next job if this was an shutdown node
            AIO_aprJobList[nJobQueue] = prJob->prNext;
         }
      }
   }
   return prJob;
}


/** 
 *  This function is to be called inside a LockedAsync-Block.
 *  It does pre-work before execution HandleAsyncJob.
 *
 *  @return return-value has to be used as input-param for
 *          vHandleAsyncJob
 *  @date   2006-09-22
 */
static OSAL_tenAsyncState enPreHandleAsyncJob( OSAL_trAsyncNode* prNode )
{
#ifdef OSAL_AIO_PRIORITY_CONTROL

   OSAL_s32ThreadPriority( OSAL_ThreadWhoAmI(), prNode->u32Priority );

#endif

   if( prNode->enState == OSAL_EN_ASYNC_PENDING )
   {
      prNode->enState = OSAL_EN_ASYNC_WORKING;
      OSAL_vSetErrorCode( OSAL_E_NOERROR );
   }
   else
   {
      OSAL_vSetErrorCode( OSAL_E_UNKNOWN );
   }

   return prNode->enState;
}

void OSAL_copy_device_name(const OsalDeviceDescriptor *fd,char* Buffer)
{
   struct OsalDevices* device     = fd->device_ptr;
   strncpy(Buffer,device->name,strlen(device->name));
}


/******************************************************************************
 * FUNCTION:     vHandleAsyncJob
 *
 * DESCRIPTION:  Execute an actual asyncronous I/O job.
 *
 * PARAMETER:    prNode: (->I)
 *                  Pointer to the asynchronous I/O job node whose job is
 *                  to be executed.
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 18.09.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tVoid vHandleAsyncJob( OSAL_trAsyncNode* prNode, OSAL_tenAsyncState enCurState )
{
   OSAL_trAsyncControl* prAIO    = prNode->prAsyncControl;
   OSAL_tIODescriptor   fd       = prAIO->id;
   tS32 s32Ret                   = OSAL_ERROR;
   tS32 s32Offset                = 0;
   tBool              bNfs = FALSE;
   char sName[255] = "";

   if( enCurState == OSAL_EN_ASYNC_WORKING )
   {
      s32Offset = prAIO->s32Offset;

      if( OSAL_NULL != (OSAL_tfdIO*)fd)// && OSAL_NULL != (char*)((OSAL_tfdIO*)fd)->szName && strlen(((OSAL_tfdIO*)fd)->szName) < 255)
      {
        //check whether we have no file system device or a file system device
        bNfs = IsNfsDev(fd);
        if(bNfs == TRUE)
        {
           OSAL_copy_device_name((OsalDeviceDescriptor *)fd,&sName[0]);
        }
        else
        {
           OSAL_tIODescriptor fdesc = (OSAL_tIODescriptor)(((OsalDeviceDescriptor*)fd)->local_ptr);
           strncpy(sName, "/dev",strlen("/dev"));
           sName[4] = '\0';
           strncat(sName,
                   (char*)((OSAL_tfdIO*)fdesc)->szFileName ,
                   strlen(((OSAL_tfdIO*)fdesc)->szFileName) + 1);
        }
      }

      /* execute read or write function */
      switch( prNode->enAccess )
      { 
         case OSAL_EN_READONLY:
         {
          if(fd != OSAL_ERROR)
          {
             OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_FIOSEEK, s32Offset);
             s32Ret = OSAL_s32IORead(fd,(tPS8)prAIO->pvBuffer, prAIO->u32Length);
          }
         break;

         case OSAL_EN_WRITEONLY:
         {
           if(fd != OSAL_ERROR)
           {
             OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_FIOSEEK, s32Offset);
             s32Ret = OSAL_s32IOWrite(fd,(tPCS8)prAIO->pvBuffer, prAIO->u32Length);
           }
         }
         break;

       case OSAL_EN_ACCESS_DEVICE:
       case OSAL_EN_ACCESS_DIR:
       case OSAL_EN_APPEND:
       case OSAL_EN_BINARY:
       case OSAL_EN_READSTREAM:
       case OSAL_EN_READWRITE:
       case OSAL_EN_TEXT:
       default:
         {
            OSAL_vSetErrorCode(OSAL_E_NOTSUPPORTED);
         }
         break;
		 }
	  }
   } //if( enCurState == OSAL_EN_ASYNC_WORKING )

   if (bLockAsync() == TRUE)
   {
      enCurState = prNode->enState;
      prNode->enState = OSAL_EN_ASYNC_DONE;
          
      /* different possibilities for setting the status */
      prAIO->s32Status    = s32Ret;
      prAIO->u32ErrorCode = OSAL_u32ErrorCode();
 
      if( enCurState == OSAL_EN_ASYNC_WORKING )
      {
         /* get the new offset */
         s32Offset = 0;
         if( OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_FIOWHERE,(intptr_t)&s32Offset) == OSAL_OK )
         {
            prAIO->s32Offset = s32Offset;
         }
      }
      /* remove node from list */
      vUnlinkNode( prNode );
      vUnlockAsync();
   }
   //if(OSAL_s32IOClose(fdFM) != OSAL_OK)
   {
    /* error OSAL_s32IOClose */
   }
   /* call callback outside of critical section */
   if( prAIO->pCallBack != NULL )
   {
       (*prAIO->pCallBack)((tPVoid)prAIO->pvArg);
   }// end if (prAIO->pCallBack != NULL)
   else
   {
     s32EnterErrMem("AIO Callback cannot accomplished");
   }

}

/******************************************************************************
 * FUNCTION:     vIOAsynchThread
 *
 * DESCRIPTION:  The thread function that does the asynchronous I/O.
 *
 *               Once started, the thread works on the queue of I/O jobs
 *               related to a given file descriptor.
 *
 *               When the last I/O job has finished, the thread terminates.
 *
 * PARAMETER:    pvArg: (->I)
 *                  Pointer to the asynchronous I/O job node that has been
 *                  used to start the thread.
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 25.11.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
tVoid vIOAsyncThread( tPVoid pvArg )
{
   uintptr_t nMyID = (uintptr_t)pvArg;
   tBool bRunning = TRUE;  
             
   while (bRunning == TRUE)
   {
   	  if(WaiSyncObj(AIO_aJobFlg[nMyID],OSAL_C_TIMEOUT_FOREVER) ==OSAL_E_NOERROR)
      {
         OSAL_trAsyncNode* prNode = NULL;
         if( bLockAsync() == TRUE )        // the related Unlock is done in  vHandleAsyncJob
         {
            prNode = prGetNextJob(nMyID);
         }
         if (prNode != NULL)
         {
            if (prNode->enState == OSAL_EN_ASYNC_SHUTDOWN)
            {
               bRunning = FALSE;
               vUnlockAsync();
            }
            else
            {
               OSAL_tenAsyncState enCurState = enPreHandleAsyncJob(prNode);
               vUnlockAsync();
               /* do async IO Job Handling */
               vHandleAsyncJob(prNode, enCurState );
               vFreeAsyncNode(prNode);
            } 
         }
      }
      else
      {
         bRunning = FALSE;
      }
#ifdef OSAL_AIO_PRIORITY_CONTROL
      // Reset priority of worker-thread to normal
      OSAL_s32ThreadPriority( OSAL_ThreadWhoAmI(), OSAL_C_U32_THREAD_PRIORITY_NORMAL );
#endif
   }

   /* Cancel any outstanding jobs */
   if (bLockAsync() == TRUE)
   {
      tBool bLocked = TRUE;
      OSAL_trAsyncNode* prNode = prGetNextJob( nMyID );

      /* At this point, the queue contains only PENDING and CANCELLED jobs */
      while ( (prNode != NULL) && (bLocked == TRUE) )
      {
        tBool bCallback = FALSE;
           OSAL_trAsyncControl* prAIO = prNode->prAsyncControl;
           if (prNode->enState == OSAL_EN_ASYNC_PENDING)
           {
               s32CancelAsync(prNode);
               bCallback = TRUE;
           }
           if ((bCallback == TRUE)&&(prAIO != NULL)&&(prAIO->pCallBack != NULL))
           {
               /* call callback outside of critical section */
               vUnlockAsync();
               bLocked = FALSE;
               (*prAIO->pCallBack)((tPVoid)prAIO->pvArg);
           }//if ((bCallback == TRUE)&&(prAIO != NULL)&&(prAIO->pCallBack != NULL))
        if( bLocked == FALSE )
        {
           bLocked = bLockAsync();
        } 
        if( bLocked == TRUE )
        { 
            prNode = prGetNextJob( nMyID );
        } 
      }// end while
      if( bLocked == TRUE )
      { 
         vUnlockAsync();
      } 
   }  
   RelSyncObj(AIO_JobTrigger);
}

/******************************************************************************
 * FUNCTION:     OSAL_Async_s32IOStart
 *
 * DESCRIPTION:  Initialize asynchronous I/O for a given control block.
 *
 * PARAMETER:    prAIO:    (->I)
 *                  The control block that describes the asynchronous I/O
 *                  that is to be done.
 *
 *               enAccess: (I)
 *                  Describes the kind of asynchronous I/O:
 *                   - OSAL_EN_READONLY for asynchronous read
 *                   - OSAL_EN_WRITEONLY for asynchronous write
 *
 * RETURNVALUE:  OSAL_OK on success, OSAL_ERROR on error with the following
 *               error codes:
 *                - OSAL_E_INVALIDVALUE if 'prAIO' is NULL
 *                - OSAL_E_BADFILEDESCRIPTOR if the 'prAIO->id' is invalid
 *                - OSAL_E_INPROGRESS if 'prAIO' is already in use
 *                - OSAL_E_NOSPACE if no job node could be allocated
 *                - any error code OSAL_s32ThreadSpawn() can create
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 25.11.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
tS32 OSAL_Async_s32IOStart( OSAL_trAsyncControl* prAIO, OSAL_tenAccess enAccess )
{
   tS32 s32Res   = OSAL_OK;
   tU32 u32Error = OSAL_E_NOERROR;

   if(!bAsyncIoAvailable)
   {
      pthread_once(&AIO_is_initialized,&vPrepAsyncTask);
     /* if(s32PrepAsyncTask() != OSAL_OK)
      {
         FATAL_M_ASSERT_ALWAYS();
      }*/
      bAsyncIoAvailable = TRUE;
   }

   /* check parameters */
   if( prAIO != (OSAL_trAsyncControl*)NULL )
   {
      OSAL_tIODescriptor fd = prAIO->id;

      if( (fd != OSAL_ERROR) && (fd != 0) )
      {
         OSAL_trAsyncNode* prNode;

         if (bLockAsync() == TRUE)
         {
            /* check if the async structure is already in use */
            prNode = prFindNode(prAIO, NULL);

            if( prNode == NULL ) /* async control structure not yet in use */
            {
               /* create a new async control node */
               prNode = prAllocAsyncNode(prAIO, enAccess);

               if( prNode != NULL )
               {
                  if (bEnqueueNode(nFindJobQueue(fd), prNode) == FALSE)
                  {
                     vFreeAsyncNode(prNode);
                     u32Error = OSAL_E_UNKNOWN;
                  }
               }
               else
               {
                  u32Error = OSAL_E_NOSPACE;
               }
            }
            else
            {
               u32Error = OSAL_E_INPROGRESS;
            }
            vUnlockAsync();
         }
      }
      else
      {
         u32Error = OSAL_E_BADFILEDESCRIPTOR;
      }
   }
   else
   {
      u32Error = OSAL_E_INVALIDVALUE;
   }

   if( u32Error != OSAL_E_NOERROR )
   {
      s32Res = OSAL_ERROR;
      OSAL_vSetErrorCode(u32Error);
   }
   return(s32Res);
}


/******************************************************************************
 * FUNCTION:     OSAL_Async_s32IOCancel
 *
 * DESCRIPTION:  Cancel an asynchronous I/O job, or all jobs for a given
 *               file descriptor.
 *
 * PARAMETER:    fd:     (I)
 *                  File descriptor for which an asynchronous I/O job is to
 *                  be cancelled.
 *
 *               prAIO:  (->I)
 *                  The control block of the job that is to be cancelled, or
 *                  NULL if all jobs for the file descriptor are to be
 *                  cancelled.
 *
 * RETURNVALUE:  - OSAL_C_AIO_ALLDONE if there had been no job to cancel
 *               - OSAL_C_AIO_CANCELED if all jobs had been cancelled
 *                    successfully
 *               - OSAL_C_AIO_NOTCANCELED if cancellation of any job
 *                    has failed (if this is returned when all jobs for a
 *                    given file descriptor are cancelled, some of the jobs
 *                    may have been cancelled nevertheless; check with
 *                    OSAL_u32IOErrorAsync() for each control block)
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 25.11.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
tS32 OSAL_Async_s32IOCancel( OSAL_tIODescriptor fd, OSAL_trAsyncControl const * prAIO )
{
   OSAL_trAsyncNode * prNode = NULL;
   tS32 s32Ret = OSAL_C_S32_AIO_ALLDONE;

   if (bLockAsync() == TRUE)
   {
      if( prAIO == NULL )   /* cancel all async i/o via fd */
      {
         prNode = prGetNextNode(fd, NULL);

         while( prNode != NULL )
         {
            OSAL_trAsyncNode* prNextNode = prGetNextNode(fd, prNode);
            tS32 s32Intermediate = s32CancelAsync(prNode);

            if( s32Ret != OSAL_C_S32_AIO_NOTCANCELED )
            {
               s32Ret = s32Intermediate;
            }
            prNode = prNextNode;
         }
      }
      else /* cancel i/o for one async control block */
      {
         prNode = prFindNode(prAIO, NULL);

         if( prNode != NULL )
         {
            s32Ret = s32CancelAsync(prNode);
         }
      }
      vUnlockAsync();
   }
   return(s32Ret);
}

/******************************************************************************
 * FUNCTION:     OSAL_ASync_enGetAsyncState
 *
 * DESCRIPTION:  Return the current state of a given asynchronous I/O control
 *               block.
 *
 * PARAMETER:    prAIO: (->I)
 *                  The control block whose state is to be determined.
 *
 * RETURNVALUE:  The state of the control block. If the node is currently
 *               not enqueued for an asynchronous I/O job, its state is
 *               reported as DONE.
 *
 * HISTORY:
 * Date      |   Modification                       | Authors
 * 25.11.02  |   Initial revision                   | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
OSAL_tenAsyncState OSAL_ASync_enGetState( OSAL_trAsyncControl const * prAIO )
{
   OSAL_tenAsyncState enState = OSAL_EN_ASYNC_DONE;

   if (bLockAsync() == TRUE)
   {
      OSAL_trAsyncNode* prNode = prFindNode(prAIO, NULL);
      if (prNode != NULL)
      {
         enState = prNode->enState;
      }
      vUnlockAsync();
   }
   return(enState);
}

/******************************************************************************
* FUNCTION:     vTraceOpenAsyncOrders
*
* DESCRIPTION:  trace pending or working async ordes into the error memory
*
* PARAMETER:    
*
* RETURNVALUE:  
*
* HISTORY:
* Date      |   Modification                       | Authors
* 24.01.07  |   Initial revision                   | CM-DI/PJ-CF14 Storre
*****************************************************************************/
#ifdef TRACE_ASYNCIO
void vTraceOpenAsyncOrders( void )
{
   tUInt    nJob;
   OSAL_trAsyncNode* AIO_aprJobPointer;

   if( bLockAsync() == TRUE )
   {
      for( nJob = 0; nJob < C_MAX_JOBS; ++nJob )
      {
         AIO_aprJobPointer = AIO_aprJobList[nJob];

         while( AIO_aprJobPointer != NULL )
         {
           TraceString("AsyncIO Info: Open order while shutdown: ThreadID= %08X",
                   AIO_aprJobPointer->prAsyncControl->s32CallerThreadId );
           TraceString("AsyncIO Info: Open order while shutdown: Device= %s",
                   GetOsalDeviceName((OSAL_tIODescriptor)AIO_aprJobPointer->prAsyncControl->id ) );
           AIO_aprJobPointer = AIO_aprJobPointer->prNext;
         }
      }
      vUnlockAsync();
   }
}
#endif


void vGetThreadName(tChar* pu8Buf,const OSAL_trAsyncControl* prAIO)
{
   (void)pu8Buf;
   (void)prAIO;
   // todo implement this functionality
}

   /************************************************************************
   *                                                                       *
   * FUNCTION:      OSAL_s32IOWriteAsync                                   *
   *                                                                       *
   * DESCRIPTION:   --15.2.8 Asynchronous Write to a File.                 *
   *                                                                       *
   * PARAMETERS:    OSAL_trAsyncControl* prAIO (->IO)                      *
   *                   async control structure                             *
   *                                                                       *
   * RETURN TYPE:   tS32: OSAL_OK or OSAL_ERROR                            *
   *                                                                       *
   ************************************************************************/
   tS32 OSAL_s32IOWriteAsync(OSAL_trAsyncControl* prAIO)
   {
      tS32 s32Ret = OSAL_OK;
     if(prAIO == OSAL_NULL)
     {
        //*** ERROR
          s32Ret = OSAL_ERROR;	  
     }
     else
     {
        if((prAIO->s32CallerThreadId = OSAL_ThreadWhoAmI()) == OSAL_ERROR)
        {
             s32Ret = OSAL_ERROR;	  
        }
        /* ensure that callback can be called */
        if(prAIO->pCallBack == NULL)
        {
           s32Ret = OSAL_ERROR;
           snprintf(&cErrMemBuffer[0],100,"OSAL_s32IOWriteAsync of Task:%d with Invalid Callback",
           (int)OSAL_ThreadWhoAmI());
           s32EnterErrMem(cErrMemBuffer);
        }
     }

     if(s32Ret == OSAL_ERROR)
     {
        //*** ERROR
         OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);      /* Set error code */
          return(OSAL_ERROR);                           /* Return OSAL ERROR */
     }

      if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_ASYNC_IO,AIO_TRACE_LEVEL) == TRUE )
      {
         tU8 au8Buf[45];
         au8Buf[0] = 0x20;
         if(prAIO) 
         {
            vGetThreadName( (tChar*)&au8Buf[1], prAIO );
       //     strncpy( (char*)&au8Buf[9], (char*)((OSAL_tfdIO*)prAIO->id)->szFileName, 20 );
            OSAL_M_INSERT_T32( &au8Buf[29], (uintptr_t)prAIO->pvBuffer );
            OSAL_M_INSERT_T32( &au8Buf[33], prAIO->u32Length );
            OSAL_M_INSERT_T32( &au8Buf[37], (tU32)prAIO->id );
			LLD_vTrace(OSAL_C_TR_CLASS_ASYNC_IO,AIO_TRACE_LEVEL,au8Buf,41);
        }
        else
        {
          memset(&au8Buf[1],0,40);
        }
      }
      return(OSAL_Async_s32IOStart(prAIO, OSAL_EN_WRITEONLY));
   }

   /************************************************************************
   *                                                                       *
   * FUNCTION:      OSAL_s32IOReadAsync                                    *
   *                                                                       *
   * DESCRIPTION:   --15.2.9 Asynchronous Read From a File.                *
   *                                                                       *
   * PARAMETERS:    OSAL_trAsyncControl* prAIO (->IO)                      *
   *                   async control structure                             *
   *                                                                       *
   * RETURN TYPE:   tS32: OSAL_OK or OSAL_ERROR                            *
   *                                                                       *
   ************************************************************************/
   tS32 OSAL_s32IOReadAsync(OSAL_trAsyncControl* prAIO)
   {
      tS32 s32Ret = OSAL_OK;
     if(prAIO == OSAL_NULL)
     {
        //*** ERROR
          s32Ret = OSAL_ERROR;	  
     }
     else
     {
        if((prAIO->s32CallerThreadId = OSAL_ThreadWhoAmI()) == OSAL_ERROR)
        {
             s32Ret = OSAL_ERROR;	  
        }
        /* ensure that callback can be called */
        if(prAIO->pCallBack == NULL)
        {
           s32Ret = OSAL_ERROR;
             snprintf(&cErrMemBuffer[0],100,"OSAL_s32IOReadAsync of Task:%d with Invalid Callback",
             (int)OSAL_ThreadWhoAmI());
             s32EnterErrMem(cErrMemBuffer);
        }
     }
     if(s32Ret ==OSAL_ERROR)
     {
        //*** ERROR
            OSAL_vSetErrorCode(OSAL_E_DOESNOTEXIST);      /* Set error code */
             return(OSAL_ERROR);                           /* Return OSAL ERROR */
     }
     else
     {
       if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_ASYNC_IO,AIO_TRACE_LEVEL) == TRUE )
       {
         tU8 au8Buf[45];
         au8Buf[0] = 0x21;
         if(prAIO)
         {
           vGetThreadName( (tChar*)&au8Buf[1], prAIO );
       //    strncpy( (char*)&au8Buf[9], (char*)((OSAL_tfdIO*)prAIO->id)->szFileName, 20 );
           OSAL_M_INSERT_T32( &au8Buf[29], (uintptr_t)prAIO->pvBuffer );
           OSAL_M_INSERT_T32( &au8Buf[33], prAIO->u32Length );
           OSAL_M_INSERT_T32( &au8Buf[37], (tU32)prAIO->id );
	    	LLD_vTrace(OSAL_C_TR_CLASS_ASYNC_IO,AIO_TRACE_LEVEL,au8Buf,41);
         }
         else
         {
            memset(&au8Buf[1],0,40);
         }
       }
       return(OSAL_Async_s32IOStart(prAIO, OSAL_EN_READONLY));
     }
   }

   /************************************************************************
   *                                                                       *
   * FUNCTION:      OSAL_s32IOCancelAsync                                  *
   *                                                                       *
   * DESCRIPTION:   --15.2.10 Cancel an Asynchronous Operation.            *
   *                   when the parameter prAIO = NULL, cancel all async   *
   *                   ops via fd                                          *
   *                                                                       *
   * PARAMETERS:    OSAL_tIODescriptor fd (I)                              *
   *                   I/O handle                                          *
   *                OSAL_trAsyncControl* prAIO (->IO)                      *
   *                   pointer to async control structure (may be NULL)    *
   *                                                                       *
   * RETURN TYPE:   tS32: OSAL_C_S32_AIO_ALLDONE  (operation already done) *
   *                      OSAL_C_S32_AIO_CANCELED (io operation canceled)  *
   *                      OSAL_C_S32_AIO_NOTCANCELED (not supported)       *
   *                      OSAL_ERROR (invalid parameters)                  *
   *                                                                       *
   ************************************************************************/
  /*lint -save -e{818}*/
   tS32 OSAL_s32IOCancelAsync(OSAL_tIODescriptor fd, OSAL_trAsyncControl* prAIO)
 /*lint -restore */    
   {
     tS32 s32Ret = OSAL_OK;
     /* fd is always != 0 and prAIO can be 0 ! */
     if( (fd == 0) || (fd == OSAL_ERROR ) )
    {
         s32Ret = OSAL_ERROR;
    }
    if(prAIO != NULL) 
    {
        if(prAIO->id != fd)
      {
              s32Ret = OSAL_ERROR;
      }
      if((prAIO->s32CallerThreadId = OSAL_ThreadWhoAmI()) == OSAL_ERROR)
      {
              s32Ret = OSAL_ERROR;
      }
    }
    if(s32Ret == OSAL_ERROR)
    {
       return OSAL_ERROR;
    }
    else
    {
       s32Ret = OSAL_Async_s32IOCancel(fd, prAIO);
       if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_ASYNC_IO,AIO_TRACE_LEVEL) == TRUE )
       {
         tU8 au8Buf[45];
         au8Buf[0] = 0x22;
         if(prAIO) 
         {
            vGetThreadName( (tChar*)&au8Buf[1], prAIO );
//            strncpy( (char*)&au8Buf[9], (char*)((OSAL_tfdIO*)prAIO->id)->szFileName, 20 );
            OSAL_M_INSERT_T32( &au8Buf[29], (tU32)s32Ret );
            OSAL_M_INSERT_T32( &au8Buf[33], (tU32)prAIO->id );
			LLD_vTrace(OSAL_C_TR_CLASS_ASYNC_IO,AIO_TRACE_LEVEL,au8Buf,37);
         }
         else
         {
            memset(&au8Buf[1],0,36);
         }
       }
       return s32Ret;
     }
   }

   /************************************************************************
   *                                                                       *
   * FUNCTION:      OSAL_u32IOErrorAsync                                   *
   *                                                                       *
   * DESCRIPTION:   --15.2.11 Get Error Code.                              *
   *                                                                       *
   * PARAMETERS:    OSAL_trAsyncControl* prAIO (->I)                       *
   *                   async control structure                             *
   *                                                                       *
   * RETURN TYPE:   tS32: error code of async operation                    *
   *                                                                       *
   ************************************************************************/
  /*lint -save -e{818}*/
   tU32 OSAL_u32IOErrorAsync(OSAL_trAsyncControl* prAIO)/* no change in OSAL API */
  /*lint -restore */    
  {
      tU32 u32Error = OSAL_E_INVALIDVALUE;
      if( prAIO != NULL )
      {
         if( (prAIO->id == 0) || (prAIO->id == OSAL_ERROR) )
         {
            u32Error = OSAL_E_BADFILEDESCRIPTOR;
         }
         else
         {

/*            OSAL_tenAsyncState enState = OSAL_ASync_enGetState(prAIO);
            switch( enState )
            {
               case OSAL_EN_ASYNC_PENDING:
               case OSAL_EN_ASYNC_WORKING:
                  u32Error = OSAL_E_INPROGRESS;
                  break;
               case OSAL_EN_ASYNC_CANCELLED:
                  u32Error = OSAL_E_CANCELED;
                  break;
               case OSAL_EN_ASYNC_DONE:
                  u32Error = prAIO->u32ErrorCode;
                  break;
              case OSAL_EN_ASYNC_SHUTDOWN:
                  * no break *
               default:
                  u32Error = OSAL_E_UNKNOWN;
                  break;
            }*/
            /* is job in progress , Cancel is not used*/
            if(prAIO->s32Status != OSAL_ERROR)
            {  
               u32Error = prAIO->u32ErrorCode;
            }  
            else
            {  
              u32Error = OSAL_E_INPROGRESS;
            }  
         }
      }
      return(u32Error);
   }

   /************************************************************************
   *                                                                       *
   * FUNCTION:      OSAL_s32IOReturnAsync                                  *
   *                                                                       *
   * DESCRIPTION:   --15.2.12 Get Return Value.                            *
   *                                                                       *
   * PARAMETERS:    OSAL_trAsyncControl* prAIO (->I)                       *
   *                   async control structure                             *
   *                                                                       *
   * RETURN TYPE:   tS32: async status                                     *
   *                                                                       *
   ************************************************************************/
 /*lint -save -e{818}*/
    tS32 OSAL_s32IOReturnAsync(OSAL_trAsyncControl* prAIO)  // no change for lint in OSAL API */
 /*lint -restore */    
   {
      tS32 s32Return = OSAL_E_INVALIDVALUE;

      if( prAIO != NULL )
      {
         s32Return = prAIO->s32Status;
      }
      return(s32Return);
   }

#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file osal_aio.c
|-----------------------------------------------------------------------*/
