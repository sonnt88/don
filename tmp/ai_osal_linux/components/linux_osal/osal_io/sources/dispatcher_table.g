struct OsalDevices {
        const char*   name;                         /* name of the device */
        const tOpenPtr      ptr_s32IOOpen;          /* pointer to open function */
        const tClosePtr     ptr_s32IOClose;         /* pointer to close function */
        const tCreatePtr    ptr_s32IOCreate;        /* pointer to create function */
        const tRemovePtr    ptr_s32IORemove;        /* pointer to remove function */
        const tIOControlPtr ptr_s32IOControl;       /* pointer to control function */
        const tReadPtr      ptr_s32IORead;          /* pointer to read function */
        const tWritePtr     ptr_s32IOWrite;         /* pointer to write function */
        const tU32          device_local_id;        /* device id (internally used for the driver */
        const tBool         bFileSystem;            /* TRUE -> is a file system */
        OSAL_tenDevAccess   ExclusiveAccess;        /* exclusive access variable */
        tU32 nJobQueue;                             /* jobqueue number for no filesystems */
        tS32 prm_index;                             /* prm index for exclusive access */
};

/*********************************************************************************/
/*                                                                               */
/*                             driver without filesystem                         */
/*                                                                               */
/*********************************************************************************/
{
  "/dev/trace",                         /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_TRACE,                  /*  device id */
  FALSE,                                /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
 }


{
  "/dev/errmem",                        /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ERRMEM,                 /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/kds",                           /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_KDS,                    /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}


{
  "/dev/acousticout/speech",            /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH,  /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/acousticout/music",             /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC,   /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/acousticin/speech",             /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH,   /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/acousticecnr/speech",           /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_AC_ECNR_IF_SPEECH, /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/acoustic/src",           		/*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ACOUSTIC_IF_SRC,        /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}
{
  "/dev/auxclock",                      /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_AUXCLK,                 /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}
{
  "/dev/diag/eol",                      /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_DIAG_EOL,               /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/prm",                           /*  name */ 
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_PRM,                    /*  device id */
  FALSE,                                /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}


{
  "/dev/ffd",                           /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_FFD,                    /*  device id */
  FALSE,                               /*  no filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/pram",                          /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_PRAM,                   /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}


{
  "/dev/touchscreen",                   /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_TOUCHSCREEN,            /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}


{
  "/dev/cdctrl",              	        /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_CDCTRL,      	        /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  OSAL_EN_DEVID_CDCTRL                 /*  index to the prm table */
}


{
  "/dev/cdaudio",                       /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_CDAUDIO,                /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  OSAL_EN_DEVID_CDAUDIO                /*  index to the prm table */
}


{
  "/dev/gps",                           /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_GPS,                    /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/gpio",                          /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_GPIO,                   /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  OSAL_EN_DEV_MULTIPLE_USE exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/adc",                           /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ADC0,                   /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
   "/dev/volt",                           /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
   OSAL_EN_DEVID_VOLT,                    /*  device id */
   FALSE,                                /*  no filesystem */
   OSAL_EN_DEV_MULTIPLE_USE,                 /*  exclusive access status */
   C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
   PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/odometer",                      /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ODO,                    /*  device id */
  FALSE         ,                       /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/abs",                           /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ABS,                    /*  device id */
  FALSE         ,                       /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/gyro",                          /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_GYRO,                   /*  device id */
  FALSE         ,                       /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}


{
  "/dev/acc",                          /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ACC,                   /*  device id */
  FALSE         ,                       /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}


{
   "/dev/wup",                           /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
   OSAL_EN_DEVID_WUP,                    /*  device id */
   FALSE,                                /*  no filesystem */
   OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
   C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
   PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
   "/dev/chenc",                         /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
   OSAL_EN_DEVID_CHENC,                  /*  device id */
   FALSE,                                /*  no filesystem */
   OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
   C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
   PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/adr3ctrl",                      /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_ADR3CTRL,               /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/gnss",                           /*  name */
  NULL,                                 /*  open  */
  NULL,                                 /*  close */
  NULL,                                 /*  create */
  NULL,                                 /*  remove */
  NULL,                                 /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_GNSS,                    /*  device id */
  FALSE,                                /*  no filesystem */
  OSAL_EN_DEV_NOT_USED,                 /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
   "/dev/aars/dab",                      /*  name */
  NULL,                                  /*  open  */
  NULL,                                  /*  close */
  NULL,                                  /*  create */
  NULL,                                  /*  remove */
  NULL,                                  /*  IOControl */
  NULL,                                  /*  read */
  NULL,                                  /*  write */
   OSAL_EN_DEVID_AARS_DAB,               /*  device id */
   FALSE,                                /*  no filesystem */
   OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
   C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
   PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
   "/dev/aars/ssi32",                    /*  name */
  NULL,                                  /*  open  */
  NULL,                                  /*  close */
  NULL,                                  /*  create */
  NULL,                                  /*  remove */
  NULL,                                  /*  IOControl */
  NULL,                                  /*  read */
  NULL,                                  /*  write */
   OSAL_EN_DEVID_AARS_SSI32,             /*  device id */
   FALSE,                                /*  no filesystem */
   OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
   C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
   PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
   "/dev/aars/mtd",                      /*  name */
  NULL,                                  /*  open  */
  NULL,                                  /*  close */
  NULL,                                  /*  create */
  NULL,                                  /*  remove */
  NULL,                                  /*  IOControl */
  NULL,                                  /*  read */
  NULL,                                  /*  write */
   OSAL_EN_DEVID_AARS_MTD,               /*  device id */
   FALSE,                                /*  no filesystem */
   OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
   C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
   PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
   "/dev/aars/amfm",                     /*  name */
  NULL,                                  /*  open  */
  NULL,                                  /*  close */
  NULL,                                  /*  create */
  NULL,                                  /*  remove */
  NULL,                                  /*  IOControl */
  NULL,                                  /*  read */
  NULL,                                  /*  write */
   OSAL_EN_DEVID_AARS_AMFM,              /*  device id */
   FALSE,                                /*  no filesystem */
   OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
   C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
   PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

/*********************************************************************************/
/*                                                                               */
/*                              driver with filesystem                           */
/*                                                                               */
/*********************************************************************************/

{
  "/dev/ramdisk",                       /*  name */ 
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_RAMDISK,                /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}


{
  "/dev/data",                           /*  name */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_DATA,                   /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/ffs",                           /*  name */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_FFS_FFS,                /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/ffs2",                          /*  name */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_FFS_FFS2,               /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/ffs3",                          /*  name */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_FFS_FFS3,               /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/ffs4",                          /*  name */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_FFS_FFS4,               /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/cryptcard",                     /*  name */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_CRYPT_CARD,             /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  OSAL_EN_DEVID_CRYPT_CARD             /*  index to the prm table */
}


{
  "/dev/cryptnav",                      /*  name */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_CRYPTNAV,               /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  OSAL_EN_DEVID_CRYPTNAV               /*  index to the prm table */
}

{
  "/dev/cryptnavroot",                  /*  name */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_CRYPTNAVROOT,           /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  OSAL_EN_DEVID_CRYPTNAVROOT           /*  index to the prm table */
}

{
  "/dev/navdb",                         /*  name */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_NAVDB,                  /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}


{
  "/dev/root",                          /*  name: Linux root as readonly device */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_ROOT,                   /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/media",                         /*  name:  */
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_DEV_MEDIA,              /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  OSAL_EN_DEVID_DEV_MEDIA              /*  index to the prm table */
}

{
  "/dev/local",                          /*  name */ 
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_LOCAL,                  /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  OSAL_EN_DEVID_CARD                 /*  index to the prm table */
}

{
  "/dev/registry",                      /*  name */
  wrapper_registry_io_open,             /*  open  */
  wrapper_registry_io_close,            /*  close */
  wrapper_registry_io_create,           /*  create */
  wrapper_registry_io_remove,           /*  remove */
  wrapper_registry_io_control,          /*  IOControl */
  NULL,                                 /*  read */
  NULL,                                 /*  write */
  OSAL_EN_DEVID_REGISTRY,               /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

{
  "/dev/cdrom",                         /*  name */ 
  wrapper_fs_io_open,                   /*  open  */
  wrapper_fs_io_close,                  /*  close */
  wrapper_fs_io_create,                 /*  create */
  wrapper_fs_io_remove,                 /*  remove */
  wrapper_fs_io_control,                /*  IOControl */
  wrapper_fs_io_read,                   /*  read */
  wrapper_fs_io_write,                  /*  write */
  OSAL_EN_DEVID_DVD,                    /*  device id */
  TRUE,                                 /*  filesystem */
  OSAL_EN_DEV_MULTIPLE_USE,             /*  exclusive access status */
  C_INVALID_JOB,                        /*  jobqueue number (always C_INVALID_JOB) */
  PRM_ILLEGAL_INDEX                     /*  index to the prm table */
}

