/************************************************************************
| FILE:         prm_usbpower.c
| PROJECT:      platform
| SW-COMPONENT: OSAL IO
|------------------------------------------------------------------------
|------------------------------------------------------------------------
| DESCRIPTION:  This is the source for the PRM (Physical Resource Manager)
|                 
|                
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2011 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 20.07.11  | Initial revision           | SEL2hi
| 24.01.13  | Added code to work         | dhs2cob
|           |     for LSIM               | 
| 05.03.13  | USB OC Handling issue fix  | kgr1kor
| 02.06.14    | Gen3: Adapt PRM LIBUSB      | kgr1kor
| 15.07.15  | CFG3-1339 : Fix to OSAL    | Ahm5kor(RBEI/ECF5)
|           | errorcode for USBP IOCTL   |
| --.--.--  | ----------------           | -------, -----
|************************************************************************/

     
/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 

/* Linux osal Header */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"
#include "ostrace.h"

#ifdef __cplusplus
extern "C" {
#endif


#if !defined (LSIM) &&  !defined (OSAL_GEN3_SIM) /*TARGET CODE GEN3*/
#ifdef PRM_LIBUSB_CONNECTION

#ifndef __USE_GNU
#define __USE_GNU
#endif
#include <sys/socket.h>

/* Defined in latest toolchain include but not in imx-staging */
/* Remove once staging header is patched*/
#define USB_MAXCHILDREN         (31)

#include <libusb-1.0/libusb.h>
#include <linux/netlink.h>
#include <linux/usb/ch9.h>
#include <linux/usb/ch11.h>


/************************************************************************ 
| externals from other modules
|-----------------------------------------------------------------------*/
extern OSAL_tMQueueHandle prm_hPrmMQ;


/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static void vPrmLibUsbConnectionTask( int status );
static void vSendMsgToPrmRecTask(UsbPortState const *PortState );


/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define USBPWR_TRACE_DEBUG TR_LEVEL_USER_4
//#define USBPWR_TRACE_INFO TR_LEVEL_USER_3

#define USB_STATUS_SIZE       4
#define USB_TIMEOUT           500
#define USB_HUB_DESC_SIZE     1024
#define HOTPLUG_BUFFER_SIZE   1024
#define OBJECT_SIZE           512
#define UEVENT_ACTION         "change"
#define RECVBUFSIZE (HOTPLUG_BUFFER_SIZE + OBJECT_SIZE)

/************************************************************************
|typedefs, enums and structs (scope: module-local)
|-----------------------------------------------------------------------*/

struct usb_status
{
   tU16 u16Status;
   tU16 u16Change;
};


typedef struct
{
    tU8 u8HubPortID;
    libusb_device_handle *udh;
    tU8 u8DevAddr;
    tU8 u8BusNum;
}trUsbPort;

enum event_type
{
   UEV_OVERCURRENT = 0,
   UEV_UNDERVOLTAGE,
   UEV_UNKNOWN    /*lint -e749 */
};

struct uevent
{
   void *next;
   tU32 PortID;
   tU32 oc_state;
   tU32 u32Time;
   enum event_type type;
   tU8  uv_activ;
};

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
static struct uevent     *uevqhp, *uevqtp;
static pthread_mutex_t  uevq_lock, *uevq_lockp = &uevq_lock;
static pthread_mutex_t     uevc_lock, *uevc_lockp = &uevc_lock;
static pthread_cond_t    uev_cond,  *uev_condp  = &uev_cond;
tU32 u32PrmUSBPortCount = 0;
tBool bPrmLibUSBInitDone = FALSE;
static struct     libusb_context       *lusbctx;
trUsbPort rUsbPortData[PRM_MAX_USB_DEVICES];
int InitUsbForPrm(void);

static int sock = -1;
static int Debuglevel  = 3;

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static int    GetUsbDeviceList( libusb_context **pctx);
static int     GetUsbDeviceType( tU32 u32DevIndex, libusb_device * const *DevList);
static int  InfoUsbHub(  libusb_device_handle *dev_handle, tU8 u8BusNum, tU8 u8DevAddr);
static int     StatusUsbHub(  libusb_device_handle *dev_handle );
static int  StatusUsbPort( unsigned PortNum,  struct usb_status *pStat);
static void ReleaseUsbHub( libusb_context **pctx);

static tS32 Prm_libusb_interface(unsigned int PortNum ,
        uint8_t request_type, uint8_t bRequest, uint16_t wValue, uint16_t wIndex,
        unsigned char *data, uint16_t wLength, unsigned int timeout);

static int  GetKernelmsg (int sock, char *buf, unsigned int bufsize, unsigned int* bufpos);
static int    UvEvent_Socket_Open( void );
static void    service_queue( void );
static void    UvEvent_queue_thread( void );

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/
void* pUsbLibHandle = NULL;

typedef int (*usb_init)(libusb_context **ctx);
typedef void (*usb_exit)(libusb_context *ctx);
typedef void (*usb_set_debug)(libusb_context *ctx, int level);
typedef int (*usb_open)(libusb_device *dev, libusb_device_handle **handle);
typedef void (*usb_close)(libusb_device_handle *dev_handle);
typedef ssize_t (*usb_get_device_list)(libusb_context *ctx,    libusb_device ***list);
typedef void (*usb_free_device_list)(libusb_device **list,int unref_devices);
typedef uint8_t (*usb_get_device_address)(libusb_device *dev);
typedef uint8_t (*usb_get_bus_number)(libusb_device *dev);
typedef uint8_t (*usb_get_port_number)(libusb_device *dev);
typedef int (*usb_get_device_descriptor)(libusb_device *dev,struct libusb_device_descriptor *desc);
typedef int (*usb_control_transfer)(libusb_device_handle *dev_handle,uint8_t request_type, uint8_t bRequest, uint16_t wValue, uint16_t wIndex,
                                       unsigned char *data, uint16_t wLength, unsigned int timeout);

typedef int (*GetFunktionPtr)(void* pModuleHandle); 

 usb_init                plibusb_init= NULL;
 usb_exit                plibusb_exit= NULL;
 usb_set_debug           plibusb_set_debug= NULL;
 usb_open                plibusb_open= NULL;
 usb_close               plibusb_close= NULL;
 usb_get_device_list     plibusb_get_device_list= NULL;
 usb_free_device_list    plibusb_free_device_list= NULL;
 usb_get_device_address  plibusb_get_device_address= NULL;
 usb_get_bus_number      plibusb_get_bus_number= NULL;
 usb_get_port_number     plibusb_get_port_number= NULL;
 usb_get_device_descriptor plibusb_get_device_descriptor= NULL;
 usb_control_transfer    plibusb_control_transfer= NULL;

tS32 GetUsbFunktionPtr(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_OK;

   if((plibusb_init                = (usb_init)(dlsym(pModuleHandle, (const char*)"libusb_init"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_init failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_exit                = (usb_exit)(dlsym(pModuleHandle, (const char*)"libusb_exit"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_exit failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_set_debug                = (usb_set_debug)(dlsym(pModuleHandle, (const char*)"libusb_set_debug"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_set_debug failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_open                = (usb_open)(dlsym(pModuleHandle, (const char*)"libusb_open"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_open failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_close               = (usb_close)(dlsym(pModuleHandle, (const char*)"libusb_close"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_close failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_get_device_list     = (usb_get_device_list)(dlsym(pModuleHandle, (const char*)"libusb_get_device_list"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_get_device_list failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_free_device_list    = (usb_free_device_list)(dlsym(pModuleHandle, (const char*)"libusb_free_device_list"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_free_device_list failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_get_device_address  = (usb_get_device_address)(dlsym(pModuleHandle, (const char*)"libusb_get_device_address"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_get_device_address failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_get_bus_number      = (usb_get_bus_number)(dlsym(pModuleHandle, (const char*)"libusb_get_bus_number"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_get_bus_number failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_get_port_number     = (usb_get_port_number)(dlsym(pModuleHandle, (const char*)"libusb_get_port_number"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_get_port_number failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_get_device_descriptor     = (usb_get_device_descriptor)(dlsym(pModuleHandle, (const char*)"libusb_get_device_descriptor"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_get_device_descriptor failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }
   if((plibusb_control_transfer     = (usb_control_transfer)(dlsym(pModuleHandle, (const char*)"libusb_control_transfer"))) == NULL)/*lint !e611 */
   {
       TraceString("dlsym libusb_control_transfer failed Error:%s",dlerror());s32Ret = OSAL_ERROR;
   }

   if(s32Ret == OSAL_ERROR)
   {
     plibusb_init= NULL;
     plibusb_exit= NULL;
     plibusb_set_debug= NULL;
     plibusb_open= NULL;
     plibusb_close= NULL;
     plibusb_get_device_list= NULL;
     plibusb_free_device_list= NULL;
     plibusb_get_device_address= NULL;
     plibusb_get_bus_number= NULL;
     plibusb_get_port_number= NULL;
     plibusb_get_device_descriptor= NULL;
     plibusb_control_transfer= NULL;
   }
   return s32Ret;
}


void* LoadModul(const char* szModuleName,GetFunktionPtr pFunc)
{
//   tS32 s32Ret = OSAL_ERROR;
   tS32 s32Pid =  OSAL_ProcessWhoAmI();
   char *error;
   void* pModuleHandle = NULL;

   dlerror();    /* Clear any existing error */

   if(LLD_bIsTraceActive(TR_COMP_OSALCORE,TR_LEVEL_COMPONENT))
   {
      TraceString("Load Driver %s PID:%d %s",szModuleName,s32Pid,prProcDat[s32FindProcEntry(s32Pid)].pu8CommandLine);
   }

   if(pUsbLibHandle == NULL)
   {
      pModuleHandle = dlopen(szModuleName, RTLD_NOW );
      if(pModuleHandle == NULL)
      {
         if ((error = dlerror()) != NULL)  
         { 
           TraceString("PID:%d Error:%s when loading %s Driver!!!",s32Pid,error,szModuleName);
           vWritePrintfErrmem("PID:%d Error:%s when loading %s Driver!!! \n",s32Pid,error,szModuleName);
         }
         else
         {
            TraceString("PID:%d Error:? when loading %s Driver!!!",s32Pid,szModuleName);
            vWritePrintfErrmem("PID:%d Error:? when loading %s Driver!!! \n",s32Pid,szModuleName);
         }
      }
      else
      {
         s32Pid = pFunc(pModuleHandle); 
         if(s32Pid == OSAL_ERROR)
         {
            dlclose(pModuleHandle);
            pModuleHandle = OSAL_NULL;
         }
      }
   }
   return pModuleHandle;
}


/*------------------------------------------------------------------------
ROUTINE NAME : prm_InitLibUsbConnectionData

PARAMETERS   :

RETURNVALUE  :

DESCRIPTION  : Initialises the libusb data for each process in its own space

COMMENTS     :

------------------------------------------------------------------------*/
void prm_InitLibUsbConnectionData( void )
{
    tU32 u32temp;

    /* check if init already done by another instance during wait */
    if( bPrmLibUSBInitDone == FALSE )
    {
       if(pUsbLibHandle == NULL)
       {
         InitUsbForPrm();
       }
       if(!(GetUsbDeviceList(&lusbctx) > 0))
       {
          vPrmTtfisTrace( TR_LEVEL_FATAL, "USB device open AND INFO failed");
       }
       bPrmLibUSBInitDone = TRUE;

       if(pOsalData->u32PrmUSBGlobalPortCount==0)
            pOsalData->u32PrmUSBGlobalPortCount=u32PrmUSBPortCount; //Update from first process
      
	  for (u32temp = 0; u32temp < u32PrmUSBPortCount; u32temp++ )
	  {
               Prm_GetUsbPortState( &pOsalData->prm_rPortState[u32temp] );
               TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBPORTID: Port (%d): HANDLE = 0x%08X:PORTNUM=%d BUSNUM = %d DEVADDR = %d",
                           u32temp, rUsbPortData[u32temp].udh, rUsbPortData[u32temp].u8HubPortID,
                           rUsbPortData[u32temp].u8BusNum,  rUsbPortData[u32temp].u8DevAddr);
         }
    }

   /* if(pOsalData->u32PrmUSBGlobalPortCount != u32PrmUSBPortCount) //check later
        TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Port Count Mismatch Global:%d Process Local:%d",pOsalData->u32PrmUSBGlobalPortCount, u32PrmUSBPortCount );*/


    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_DATA, "Total Number for Ports = %d", pOsalData->u32PrmUSBGlobalPortCount );

}


/*------------------------------------------------------------------------
ROUTINE NAME : prm_CreateLibUsbConnectionTask

PARAMETERS   :

RETURNVALUE  : OSAL ThreadId

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

OSAL_tThreadID prm_CreateLibUsbConnectionTask( void )
{
   OSAL_trThreadAttribute  attr;
   OSAL_tThreadID ThId;

   /* start PRM task */
   attr.szName = "PRM_LIBUSB";
   attr.u32Priority  = OSALIO_PRIORITY_THREAD_PRM_LIBUSB_TA;
   attr.s32StackSize = minStackSize;
   attr.pfEntry = (OSAL_tpfThreadEntry)vPrmLibUsbConnectionTask;
   attr.pvArg   = NULL;
   ThId = OSAL_ThreadSpawn( &attr );
   if( ThId == OSAL_ERROR )
   {
      FATAL_M_ASSERT_ALWAYS();
   }
   return( ThId );

}


/*------------------------------------------------------------------------
ROUTINE NAME : prm_vDeleteLibUsbConnectionTask

PARAMETERS   : ThreadId            ( I ) = Application ID

RETURNVALUE  :

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

void prm_vDeleteLibUsbConnectionTask( OSAL_tThreadID ThId )
{
   if( OSAL_ERROR == OSAL_s32ThreadDelete( ThId ) )
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
}


/*------------------------------------------------------------------------
ROUTINE NAME : Prm_libusb_interface

PARAMETERS   :

RETURNVALUE  : error code

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static tS32 Prm_libusb_interface(unsigned int AppPortNum ,
        uint8_t request_type, uint8_t bRequest, uint16_t wValue, uint16_t wIndex,
        unsigned char *data, uint16_t wLength, unsigned int timeout)
{
    libusb_context *libusbctx = lusbctx;
    libusb_device **Devicelist;
    libusb_device * cdev;
    libusb_device_handle *dev_handle;
    tU8 u8BusNum, u8DevAddr, u8PortNum;
    tS32 NumDevs, i, found_dev = 0;
    tS32 ReturnValue = OSAL_E_NOERROR;

    if(plibusb_get_device_list == NULL) return OSAL_E_NOTSUPPORTED;
    //if(!libusb_init(&libusbctx))
    {
        NumDevs = plibusb_get_device_list(libusbctx,&Devicelist);

        if(NumDevs < 0)
        {
            vPrmTtfisTrace( TR_LEVEL_FATAL, "libusb_get_device_list returns: %d ",NumDevs);
        }
        else
        {
            TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "libusb_get_device_list returns NumDevices: %d ",NumDevs);

            vPrmTtfisTrace( TR_LEVEL_FATAL, "\nWANTED: AppPortNum:%d u8BusNum:%d u8DevAddr:%d u8HubPortID:%d ",
                        AppPortNum, rUsbPortData[AppPortNum -1].u8BusNum, rUsbPortData[AppPortNum -1].u8DevAddr, rUsbPortData[AppPortNum -1].u8HubPortID);


            for(i=0; (i < NumDevs) && (found_dev == 0) ; i++)
            {
                cdev=Devicelist[i];
                u8BusNum    = plibusb_get_bus_number(cdev);
                u8DevAddr    = plibusb_get_device_address(cdev);
                u8PortNum    = plibusb_get_port_number(cdev);
                TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, " [%d] CDEV:%p BUSNUM: %d DEVADDR: %d PORTNUM: %d ", i, cdev, u8BusNum, u8DevAddr, u8PortNum);

                if( (u8BusNum == rUsbPortData[AppPortNum -1].u8BusNum) && u8DevAddr == rUsbPortData[AppPortNum -1].u8DevAddr)
                {
                    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "found!! Device for AppPortNum : %d ",AppPortNum);
                    found_dev= 1;

                    ReturnValue = plibusb_open(cdev, &dev_handle);

                    if(!ReturnValue)
                    {
                       TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : libusb_control_transfer(%d): HDL=0x%08X TYP=0x%X REQ=0x%X VAL=0x%X  PORTID=0x%X BUF=0x%08X LEN=%d TO=%d", __func__, AppPortNum,
                               dev_handle, request_type,  bRequest, wValue, wIndex, data, wLength, timeout);

                       ReturnValue = plibusb_control_transfer( dev_handle, request_type,  bRequest, wValue, wIndex, data, wLength, timeout );

                       TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "libusb_control_transfer returned  : %d ",ReturnValue);

                       plibusb_close(dev_handle);
                    }

                }
            }
            plibusb_free_device_list(Devicelist, 1);
        }

    //    libusb_exit(libusbctx);

        if(found_dev == 0)
        {
            vPrmTtfisTrace( TR_LEVEL_FATAL, "USB Device for AppPortNum %d not available anymore ",AppPortNum);
            ReturnValue = OSAL_E_INVALIDVALUE;
        }

    }
    return ReturnValue;

}

/*------------------------------------------------------------------------
ROUTINE NAME : Prm_GetUsbPortState

PARAMETERS   : UsbPortState with Port number            ( I ) = Application ID

RETURNVALUE  : error code

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

tU32 Prm_GetUsbPortState( UsbPortState* rPortState )
{
    struct usb_status Stat;
    tU32 ret = OSAL_E_NOERROR;
    tU8 u8Port;
    if(!rPortState)
        return (tU32)OSAL_E_INVALIDVALUE;
    else
    {
        u8Port = rPortState->u8PortNr;
    }

    memset( rPortState, 0, sizeof(UsbPortState));
    rPortState->u8PortNr = u8Port;
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Prm_GetUsbPortState: PORT %d ",u8Port);

    if(((tU32)u8Port == 0) ||((tU32)u8Port > u32PrmUSBPortCount))
        ret = OSAL_E_INVALIDVALUE;
    else
    {
        ret = (tU32)StatusUsbPort( (tU32)u8Port, &Stat );
        if( ret == OSAL_E_NOERROR)
        {
            unsigned int portstate = 0;
            if(Stat.u16Status & USB_PORT_STAT_OVERCURRENT)
             portstate |= 0x4;

            if(Stat.u16Change & USB_PORT_STAT_C_OVERCURRENT)
             portstate |= 0x2;

            if(Stat.u16Status & USB_PORT_STAT_POWER)
             portstate |= 0x1;

            switch(portstate)
            {
                 case 0:
                    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Prm_GetUsbPortState: PORT %u OFF / DISABLED - OTHER REASON ", (tU32)u8Port);
                    rPortState->u8PPON = (tU8)SIG_FALSE;
                    rPortState->u32PPONEndTime = OSAL_ClockGetElapsedTime();
                     break;

                 case 1:
                    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Prm_GetUsbPortState: PORT %u ON - NORMAL CONDITION ", (tU32)u8Port);
                    rPortState->u8OC = (tU8)SIG_FALSE;
                    rPortState->u8PPON = (tU8)SIG_TRUE;
                    rPortState->u32PPONStartTime = OSAL_ClockGetElapsedTime();
                    break;

                 case 2:
                    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Prm_GetUsbPortState: PORT %u OC START | PP switched OFF due to OC by HC/HCD ", (tU32)u8Port);
                    rPortState->u8OC = (tU8)SIG_TRUE;
                    rPortState->u32OCStartTime = OSAL_ClockGetElapsedTime();
                    rPortState->u8PPON = (tU8)SIG_FALSE;
                    rPortState->u32PPONEndTime = rPortState->u32OCStartTime;
                    break;

                 case 3:
                    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Prm_GetUsbPortState: PORT %u OC END  | PP switched ON after OC", (tU32)u8Port);
                    rPortState->u8OC = (tU8)SIG_FALSE;
                    rPortState->u32OCEndTime = OSAL_ClockGetElapsedTime();
                    rPortState->u8PPON = (tU8)SIG_TRUE;
                    rPortState->u32PPONStartTime = rPortState->u32OCEndTime;
                    break;

                 case 4:
                 case 5:
                 case 6:
                 case 7:
                    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Prm_GetUsbPortState: PORT %u OC ACTIVE | OCI line HIGH shows active OverCurrent", (tU32)u8Port);
                    rPortState->u8OC = (tU8)SIG_TRUE;
                    rPortState->u32OCStartTime = OSAL_ClockGetElapsedTime();
                    rPortState->u8PPON = (tU8)SIG_FALSE;
                    rPortState->u32PPONEndTime = rPortState->u32OCStartTime;
                    break;
                 default:
                    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Prm_GetUsbPortState:  <<<<<<<<<<<<<< !UNEXPECTED STATE! >>>>>>>>>>>");
                     break;
            }
        }
    }

    return ret;
}

/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

/*------------------------------------------------------------------------
ROUTINE NAME : vSendMsgToPrmRecTask

PARAMETERS   :

RETURNVALUE  : void

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static void vSendMsgToPrmRecTask(UsbPortState const *PortState )
{

   trPrmMsg rMsg;

   rMsg.s32ResID    = (tS32)RES_ID_VOLTAGE;
   rMsg.u32EvSignal = (tU32)SIGNAL_VOLTAGE;
   rMsg.u32Val      = 0;
   rMsg.au8UUID[0]  = 0;

   memcpy( rMsg.au8UUID,  PortState, sizeof(UsbPortState) );

   if(( rMsg.s32ResID != (tS32)RES_ID_INVALID ) && (rMsg.u32EvSignal != (tU32)SIGNAL_INVALID) && (rMsg.u32Val < PRM_MAX_USB_DEVICES))
   {
      (void)OSAL_s32MessageQueuePost( prm_hPrmMQ, (tPCU8)&rMsg, sizeof(trPrmMsg), 0 );
   }
}

/*------------------------------------------------------------------------
ROUTINE NAME : GetKernelmsg

PARAMETERS   :     uev_osck: socket to listen-to
                buf: buffer to store the uevent
                bufsize: size of the buffer
                bufpos: pointer to store the start of uevent data.

RETURNVALUE  : Number of bytes received or Error

DESCRIPTION  : This function is used to receive and filter the libusb
                uevent messages from the kernel

COMMENTS     :

------------------------------------------------------------------------*/

static int GetKernelmsg (int uev_sock, char *buf, unsigned int bufsize, unsigned int *bufpos)
{
    struct iovec iov;
    char cred_msg[ CMSG_SPACE( sizeof( struct ucred) ) ];
    struct msghdr  smsg;
    struct cmsghdr *cmsg;
    struct ucred   *cred;
    int nlbytesreceived;

    memset( buf, 0, bufsize);
    iov.iov_base = buf;
    iov.iov_len = bufsize;

    memset( &smsg, 0, sizeof( struct msghdr ) );
    smsg.msg_iov = &iov;
    smsg.msg_iovlen = 1;
    smsg.msg_control = cred_msg;
    smsg.msg_controllen = sizeof( cred_msg );

    nlbytesreceived = recvmsg( uev_sock, &smsg, 0 );
    //TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "\n\n$START>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    //TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "message received with size %d bytes", nlbytesreceived );
    if( nlbytesreceived == 0 )
    {
       return -1;
    }
    if( nlbytesreceived < 0 )
    {
       TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "illegal message received %i", -errno );
       return -1;
    }

    cmsg = CMSG_FIRSTHDR( &smsg );
    if( (cmsg == NULL) || ( cmsg->cmsg_type != SCM_CREDENTIALS) )
    {
       TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "no sender credentials received, message ignored" );
       return -1;
    }
    cred = (struct ucred *)((void*)CMSG_DATA(cmsg));
    if( cred->uid != 0 )
    {
       TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "sender uid=%d, message ignored", cred->uid );
       return -1;
    }

    if (memcmp(buf, "libudev", strlen("libudev")) == 0) {
        TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "received message with libudev string" );
        return -1; /*only events with kernel msghdr supported.*/
    } else {
        TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "received kernel message with header" );
          *bufpos = strlen( buf ) + 1;
          if( ( *bufpos < sizeof("a@/d")) || ( *bufpos >= bufsize ) )
          {
             TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "invalid message lenght" );
             return -1;
          }
          if( (strstr( buf, "@/" )) == NULL )
          {
             TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "unrecognized message header" );
             return -1;
          }
    }

        //TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBP_OSAL: nlbytesreceived\t:0x%X", (unsigned int)nlbytesreceived);
        //TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBP_OSAL: buf            \t:0x%X", (unsigned int)buf);
        //TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBP_OSAL: bufpos         \t:0x%X", (unsigned int)*bufpos);

        return nlbytesreceived;
}



/*------------------------------------------------------------------------
ROUTINE NAME : vPrmLibUsbConnectionTask

PARAMETERS   :

RETURNVALUE  : void

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static void vPrmLibUsbConnectionTask( int status )
{
   struct sched_param param;
   int sched_policy;
   char *buf;
   tU32 u32temp = 0;

   (void)status;  // lint

   // todo change priority
   pthread_getschedparam( pthread_self(), &sched_policy, &param);
   param.sched_priority = 30;
   sched_policy = SCHED_FIFO;  // SCHED_RR, SCHED_OTHER
   if((pthread_setschedparam( pthread_self(), sched_policy, &param))!= 0 )
   {
      pOsalData->bPrmUsbPwrActiv = FALSE;
      vPrmTtfisTrace( TR_LEVEL_FATAL, "failed to set msg tread priority" );
   }

   uevqhp = NULL;
   uevqtp = NULL;

   pthread_mutex_init( uevq_lockp, NULL );
   pthread_mutex_init( uevc_lockp, NULL );
   pthread_cond_init(  uev_condp,  NULL );

   if( pOsalData->bPrmUsbPwrActiv == TRUE )
   {
      OSAL_trThreadAttribute  attr;
      OSAL_tThreadID ThId;

      /* start PRM task */
      attr.szName = "PRM_LIBUSBQ";
      attr.u32Priority  = OSALIO_PRIORITY_THREAD_PRM_LIBUSBQ_TA;
      attr.s32StackSize = minStackSize;
      attr.pfEntry = (OSAL_tpfThreadEntry)UvEvent_queue_thread;
      attr.pvArg   = NULL;
      ThId = OSAL_ThreadSpawn( &attr );
      if( ThId == OSAL_ERROR )
      {
         FATAL_M_ASSERT_ALWAYS();
      }
      else
      {
          TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBP_OSAL: Spawned PRM_LIBUSBQ UEVQ HLDR with Thread ID: %d (0x%X)", (unsigned int)ThId, (unsigned int)ThId);
      }

      sock = UvEvent_Socket_Open();
   }
   if( sock < 0 )
   {
      pOsalData->bPrmUsbPwrActiv = FALSE;
      vPrmTtfisTrace( TR_LEVEL_FATAL, "failed to open the socket, error= %d", sock );
   }

   buf = malloc(RECVBUFSIZE);

   if(!buf)
   {
       vPrmTtfisTrace( TR_LEVEL_FATAL, "No Memory alloc RECVBUFSIZE failed");
       FATAL_M_ASSERT_ALWAYS();
   }
   else
   {
       TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "message loop %s started",(pOsalData->bPrmUsbPwrActiv)?"":"not");
    while(pOsalData->bPrmUsbPwrActiv && buf)
    {
        unsigned int bufpos;
        int bytesreceived;
        int BusNumber = -1, DeviceNumber = -1;
        int PortID_current = -1, ocstate = -1;
        char *prm_usb_hci_path = NULL;
        char *tpos;
        struct uevent  *uev;

        bytesreceived = GetKernelmsg(sock, buf, RECVBUFSIZE, &bufpos);
        if(bytesreceived < 0)
            continue;
        if(bytesreceived > (tS32)(RECVBUFSIZE-1))
            bytesreceived = (tS32)RECVBUFSIZE-1;

        //strncpy(buf+bytesreceived,'\0',1); /*Terminate incomplete buf data also*/
        *(buf+bytesreceived) = '\0'; /*Terminate incomplete buf data also*/

        if( strncmp( buf, UEVENT_ACTION, strlen(UEVENT_ACTION) ) )   // filter: action must be "change"
        {
         // TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBP_OSAL: This is not a change event !!IGNORE!!");
         continue;                                 // throw away message
        }
        TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBP_OSAL: 'change' event received\n");

        vPrmTtfisTrace(USBPWR_TRACE_DEBUG, "=> %s",buf);
        for( tpos=buf+bufpos; tpos<buf+bytesreceived; tpos+=(strlen(tpos)+1))
        {
            vPrmTtfisTrace(USBPWR_TRACE_DEBUG, " > %s",tpos);

            if ((tpos <= (buf + bytesreceived - strlen("DEVPATH="))) && strstr(tpos,"DEVPATH=")) /*Debug information purpose*/
                prm_usb_hci_path = tpos + strlen("DEVPATH=");

            if ((tpos <= (buf + bytesreceived - strlen("BUSNUM="))) && strstr(tpos,"BUSNUM="))
                BusNumber = atoi(tpos+strlen("BUSNUM="));

            if ((tpos <= (buf + bytesreceived - strlen("DEVNUM="))) && strstr(tpos,"DEVNUM="))
                DeviceNumber = atoi(tpos+strlen("DEVNUM="));

            if ((tpos <= (buf + bytesreceived - strlen("OVERCURRENT="))) && strstr(tpos,"OVERCURRENT="))
                ocstate = atoi(tpos+strlen("OVERCURRENT="));
        }
        TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "\n");

        TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBP_OSAL:prm_usb_hci_path = %s | BusNumber = %d | DeviceNumber = %d | OCSTATE = %d [%s]",
                prm_usb_hci_path, BusNumber, DeviceNumber, ocstate, ocstate?"ACTIVE OC":"NO OC");

        if((prm_usb_hci_path == NULL) || (BusNumber == -1)
                || (DeviceNumber == -1) || (ocstate == -1))
        {
            TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBP_OSAL: Lost Event !! Insufficient Data !! ", prm_usb_hci_path);
            continue;
        }

        for (u32temp = 0; u32temp < u32PrmUSBPortCount;u32temp++)
        {
            if((rUsbPortData[u32temp].u8BusNum == BusNumber) && (rUsbPortData[u32temp].u8DevAddr == DeviceNumber))
                PortID_current = (tS32)(u32temp+1);
        }

        if (PortID_current == -1)
        {
            TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "USBP_OSAL: Lost Event !! Not listed for USBPOWERCTL DEVNUM=%d DEVADDR=%d !! ",
                    BusNumber, DeviceNumber);
            continue;
        }

        uev = malloc(sizeof(struct uevent));

        if( uev == NULL )
        {
         vPrmTtfisTrace( TR_LEVEL_FATAL, "lost event ENOMEM" );
         continue;
        }

        /* initialise event to post to PRM */
        uev->PortID     = (tU32)PortID_current;
        uev->oc_state    = (tU32)ocstate;
        uev->u32Time    = OSAL_ClockGetElapsedTime();
        uev->type        = UEV_OVERCURRENT;
        uev->uv_activ    = 0;

        TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "msgloop: message queued 0x%08x,PORT %d OC_STATE:%s Time: %ld", uev, uev->PortID,(uev->oc_state)?"START":"END", uev->u32Time);

        pthread_mutex_lock( uevq_lockp );   // queue into chain list
        if( uevqtp != NULL )
        {
          uevqtp->next = uev;
        }
        else
        {
          uevqhp = uev;  // set rx to tx, if empty
        }
        uevqtp = uev;
        uev->next = NULL;
        pthread_mutex_unlock( uevq_lockp );

        pthread_mutex_lock( uevc_lockp );
        pthread_cond_signal( uev_condp );
        pthread_mutex_unlock( uevc_lockp );

    } // while( pOsalData->bPrmUsbPwrActiv )
    free(buf);
   }

   pthread_mutex_destroy( uevq_lockp );
   pthread_mutex_destroy( uevc_lockp );
   pthread_cond_destroy(  uev_condp  );

   if( sock >= 0 )
   {
      close( sock );
   }

}

/*------------------------------------------------------------------------
ROUTINE NAME : StatusUsbHub

PARAMETERS   :

RETURNVALUE  : OK: 0; Error: -EFAULT

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static int StatusUsbHub( libusb_device_handle *dev_handle )
{
   int     ReturnValue;
   uint8_t buf[USB_STATUS_SIZE];
   struct  usb_status *pStat;

   if(plibusb_control_transfer)
   {
       ReturnValue = plibusb_control_transfer( dev_handle, (tU8)LIBUSB_ENDPOINT_IN | USB_RT_HUB,  (tU8)LIBUSB_REQUEST_GET_STATUS,
                                              0, 0, buf, USB_STATUS_SIZE, USB_TIMEOUT );
       if( ReturnValue != USB_STATUS_SIZE )
       {
           return -EFAULT;
       }
       pStat = (struct usb_status *)buf;

       TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Status(): HUB: UDH: 0x%08X HUB(%04x:%04x) STATUS:%s %s CHANGE: %s %s",
               dev_handle, pStat->u16Change, pStat->u16Status,
               (pStat->u16Status & 0x02) ? "HUB_STATUS_OVERCURRENT" : "",
               (pStat->u16Status & 0x01) ? "HUB_STATUS_LOCAL_POWER" : "",
               (pStat->u16Change & 0x02) ? "HUB_CHANGE_OVERCURRENT" : "",
               (pStat->u16Change & 0x01) ? "HUB_CHANGE_LOCAL_POWER" : "" );

       return 0;
   }
   else
   {
       return -1;
   }
}


/*------------------------------------------------------------------------
ROUTINE NAME : StatusUsbPort

PARAMETERS   : PortNumber (Unique ID visible to application)

RETURNVALUE  : OSAL_E_NOERROR or Error Code

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static int StatusUsbPort( unsigned int PortNum, struct usb_status *pStat )
{
   int  ReturnValue;
   unsigned short int     PortID;
   struct libusb_device_handle *dev_handle;
   uint8_t buf[USB_STATUS_SIZE];

   if((PortNum == 0) ||( PortNum > u32PrmUSBPortCount ) || pStat == NULL )
   {
       vPrmTtfisTrace( TR_LEVEL_FATAL, "%s : status requested with invalid Parameters PortNumber=%d, buffer=0x%08X", __func__, PortNum, pStat );
       return OSAL_E_INVALIDVALUE;
   }

   if(plibusb_control_transfer == NULL)
   {
       vPrmTtfisTrace( TR_LEVEL_FATAL, "%s : failed plibusb_control_transfer function missing for LIBUSB_REQUEST_GET_STATUS", __func__);
       ReturnValue = OSAL_E_NOTSUPPORTED;
   }
   else
   {

       PortID = rUsbPortData[PortNum-1].u8HubPortID;
       dev_handle = rUsbPortData[PortNum-1].udh;

       if( bPrmLibUSBInitDone==FALSE)
       {
           TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : libusb_control_transfer(%d): HDL=0x%08X TYP=0x%X REQ=0x%X VAL=0x%X  PORTID=0x%X BUF=0x%08X LEN=%d TO=%d", __func__, PortNum,
                           dev_handle, (tU8)LIBUSB_ENDPOINT_IN | USB_RT_PORT, LIBUSB_REQUEST_GET_STATUS,
                           0, PortID, (unsigned char *)pStat, 2, USB_TIMEOUT  );

           ReturnValue = plibusb_control_transfer( dev_handle, (tU8)LIBUSB_ENDPOINT_IN | USB_RT_PORT,  (tU8)LIBUSB_REQUEST_GET_STATUS,
                                                   0, PortID, (unsigned char *)buf, USB_STATUS_SIZE, USB_TIMEOUT );
       }
       else
       {
           ReturnValue = Prm_libusb_interface( PortNum, (tU8)LIBUSB_ENDPOINT_IN | USB_RT_PORT,  (tU8)LIBUSB_REQUEST_GET_STATUS,
                                                     0, PortID, (unsigned char *)buf, USB_STATUS_SIZE, USB_TIMEOUT );
       }

       if( ReturnValue != USB_STATUS_SIZE )
       {
          vPrmTtfisTrace( TR_LEVEL_FATAL, "failed to read hub udh %08x port %u status, %d", dev_handle, PortID, ReturnValue );

          /*Return Value: ReturnValue is of no use for OSAL application
              on success, the number of bytes actually transferred
              LIBUSB_ERROR_TIMEOUT if the transfer timed out
              LIBUSB_ERROR_PIPE if the control request was not supported by the device
              LIBUSB_ERROR_NO_DEVICE if the device has been disconnected
              another LIBUSB_ERROR code on other failures */
          ReturnValue = OSAL_E_UNKNOWN;
       }
       else
       {
           ReturnValue = OSAL_E_NOERROR;
           pStat->u16Change = ((struct usb_status *)buf)->u16Change;
           pStat->u16Status = ((struct usb_status *)buf)->u16Status;
    
           if((!(pStat->u16Change)) && (!(pStat->u16Status)))
           {
               vPrmTtfisTrace( TR_LEVEL_FATAL, "PortStatus(): HUB UDH:%08X PORT %u switched off", dev_handle, PortNum );
           }
           else
           {
               TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "PortStatus(): UDH:0x%08X PORT %u STATUS(%04X:%04X) = %s %s %s %s %s %s %s %s %s %s",
                               dev_handle, PortNum, pStat->u16Change, pStat->u16Status,
                               (pStat->u16Status & USB_PORT_STAT_INDICATOR) ? "STAT_INDICATOR" : "",
                               (pStat->u16Status & USB_PORT_STAT_TEST) ? "STAT_TEST" : "",
                               (pStat->u16Status & USB_PORT_STAT_HIGH_SPEED) ? "STAT_HIGH_SPEED" : "",
                               (pStat->u16Status & USB_PORT_STAT_LOW_SPEED) ? "STAT_LOW_SPEED" : "",
                               (pStat->u16Status & USB_PORT_STAT_POWER) ? "STAT_POWER" : "",
                               (pStat->u16Status & USB_PORT_STAT_RESET) ? "STAT_RESET" : "",
                               (pStat->u16Status & USB_PORT_STAT_OVERCURRENT) ? "STAT_OVERCURRENT" : "",
                               (pStat->u16Status & USB_PORT_STAT_SUSPEND) ? "STAT_SUSPEND" : "",
                               (pStat->u16Status & USB_PORT_STAT_ENABLE) ? "STAT_ENABLE" : "",
                               (pStat->u16Status & USB_PORT_STAT_CONNECTION) ? "STAT_CONNECTION" : "");

              TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "PortStatus(): UDH:0x%08X PORT %u CHANGE(%04X:%04X) = %s %s %s %s %s",
                              dev_handle, PortNum, pStat->u16Change, pStat->u16Status,
                              (pStat->u16Change & USB_PORT_STAT_C_RESET) ? "C_RESET" : "",
                              (pStat->u16Change & USB_PORT_STAT_C_OVERCURRENT) ? "C_OVERCURRENT" : "",
                              (pStat->u16Change & USB_PORT_STAT_C_SUSPEND) ? "C_SUSPEND" : "",
                              (pStat->u16Change & USB_PORT_STAT_C_ENABLE) ? "C_ENABLE" : "",
                              (pStat->u16Change & USB_PORT_STAT_C_CONNECTION) ? "C_CONNECTION" : "" );

           }
       }

   }

   return ReturnValue;
}


/*------------------------------------------------------------------------
ROUTINE NAME : InfoUsbHub

PARAMETERS   :

RETURNVALUE  : OK: 0 else error

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static int InfoUsbHub(  libusb_device_handle *dev_handle, tU8 u8BusNum, tU8 u8DevAddr )
{
   tU8     i;
   int ReturnValue;
   uint8_t buf[ USB_HUB_DESC_SIZE ];
   struct usb_hub_descriptor *hub_desc;
   struct usb_status Stat;
   tU8 hub_lpsm;
   tU8 hub_ocpm;
   if(plibusb_control_transfer)
   {
   ReturnValue = plibusb_control_transfer( dev_handle, (tU8)LIBUSB_ENDPOINT_IN | USB_RT_HUB,
                                                                   (tU8)LIBUSB_REQUEST_GET_DESCRIPTOR,  (tU8)LIBUSB_DT_HUB << 8,
                                          0, buf, sizeof( buf ), USB_TIMEOUT );
   }
   else
   {
      ReturnValue = -1;
   }
   if( ReturnValue <= 0 )
   {
      return -EFAULT;
   }
   hub_desc = (struct usb_hub_descriptor *)buf;
   hub_lpsm = hub_desc->wHubCharacteristics & HUB_CHAR_LPSM;
   hub_ocpm = hub_desc->wHubCharacteristics & HUB_CHAR_OCPM;

   TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "\n");
   TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X bDescLength      %d;",__func__, dev_handle,hub_desc->bDescLength );
   TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X bDescriptorType  %d;",__func__, dev_handle,hub_desc->bDescriptorType);
   TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X bNbrPorts          %d;",__func__, dev_handle,hub_desc->bNbrPorts);

   switch(hub_lpsm)
   {
      case HUB_CHAR_COMMON_LPSM:
         TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X wHubCharacteristics[0] LPSM: ganged ",__func__ , dev_handle);
         break;
      case HUB_CHAR_INDV_PORT_LPSM:
         TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X wHubCharacteristics[0] LPSM: individual",__func__ , dev_handle);
         break;
      case HUB_CHAR_NO_LPSM:
      default:
         TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X wHubCharacteristics[0] LPSM: none",__func__ , dev_handle);
         break;
   }
   switch(hub_ocpm)
   {
      case HUB_CHAR_COMMON_OCPM:
         TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X wHubCharacteristics[1] OCPM: global",__func__, dev_handle);
         break;
      case HUB_CHAR_INDV_PORT_OCPM:
         TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X wHubCharacteristics[1] OCPM: individual ",__func__, dev_handle );
         break;
      case HUB_CHAR_NO_OCPM:
      default:
         TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X wHubCharacteristics[1] OCPM: none",__func__ , dev_handle);
         break;
   }

   TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X bPwrOn2PwrGood      %d;",__func__, dev_handle, hub_desc->bPwrOn2PwrGood);
   TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s : UDH: 0x%08X bHubContrCurrent %d;",__func__,  dev_handle,hub_desc->bHubContrCurrent);

#ifndef SUPPORT_GANGED_CONTROL
   if(!((hub_lpsm == 1) && (hub_ocpm == 8)))
   {
       vPrmTtfisTrace( TR_LEVEL_FATAL, "%s : UDH: 0x%08X GANGED PORT CONTROL Not Supported    !Device Dropped!;",__func__, dev_handle);
       return -EFAULT;
   }
   else
#endif

    if( (ReturnValue = StatusUsbHub( dev_handle )) !=0 )
    {
        vPrmTtfisTrace( TR_LEVEL_FATAL, "StatusUsbHub FAILED with %d",ReturnValue );
        return ReturnValue;
    }

   for( i=1; i<= hub_desc->bNbrPorts; i++ )
   {
       u32PrmUSBPortCount++;
       rUsbPortData[u32PrmUSBPortCount-1].u8HubPortID = i;
       rUsbPortData[u32PrmUSBPortCount-1].udh = dev_handle;
       rUsbPortData[u32PrmUSBPortCount-1].u8BusNum = u8BusNum;
       rUsbPortData[u32PrmUSBPortCount-1].u8DevAddr = u8DevAddr;
       StatusUsbPort(u32PrmUSBPortCount, &Stat ); //Last added PortNumber
   }

   return 0;
}

/*------------------------------------------------------------------------
ROUTINE NAME : prmUSBPower_control

PARAMETERS   :

RETURNVALUE  : OK: 0

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

tU32 prmUSBPower_control(tS32 arg)
{
   tU32 PortNum = 1;
   usb_port_control *pcontrol = (usb_port_control *)arg;
   tU32 ret = OSAL_E_NOERROR;
   TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Power(): control: PMASK:0x%08X VMASK:0x%08X",pcontrol->PortMask, pcontrol->ValueMask );
   while( (pcontrol->PortMask != 0) && (PortNum > 0 ) && (PortNum <= u32PrmUSBPortCount))
   {

      if( pcontrol->PortMask & 0x1 )
      {
         TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Power(): control for Port %d in PMASK:0x%08X VMASK:0x%08X", PortNum, pcontrol->PortMask, pcontrol->ValueMask );
         tU32 u32Tmp = pcontrol->ValueMask & 0x1;
         UsbPortState rPortState;
         rPortState.u8PortNr = (tU8)PortNum;
         if(OSAL_E_NOERROR != ( ret = Prm_GetUsbPortState(&rPortState)))
         {
             TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Power(): control failed with err 0x%08x",ret );
         }
         else
         {
             if( rPortState.u8OC == (tU8)SIG_TRUE )
             {
                 ret = OSAL_E_BUSY;
             }
             else if(((rPortState.u8PPON == (tU8)SIG_TRUE ) && u32Tmp )|| ((rPortState.u8PPON == (tU8)SIG_FALSE) && !u32Tmp)) /*Success b4 try:already in requested state!*/
             {
                 TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Power(): HUB: 0x%X PORT %u already %s",rUsbPortData[PortNum-1].udh , PortNum,  u32Tmp ? "ON" : "OFF" );
             }
             else
          {
                 TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "Power(): HUB: 0x%X PORT %u %s",rUsbPortData[PortNum-1].udh , PortNum,  u32Tmp ? "ON" : "OFF" );

                 ret = Prm_libusb_interface( PortNum,    // to find dev_handle
                         (tU8)LIBUSB_ENDPOINT_OUT | USB_RT_PORT,   // bmRequestType
                                  u32Tmp ? (tU8)LIBUSB_REQUEST_SET_FEATURE : (tU8)LIBUSB_REQUEST_CLEAR_FEATURE, // bRequest
                                  USB_PORT_FEAT_POWER,  // wValue
                                  rUsbPortData[PortNum-1].u8HubPortID,           // wIndex
                                  NULL,            // data
                                  0,               // wLength
                                  USB_TIMEOUT );   // timeout

#if 0
                 libusb_control_transfer( rUsbPortData[PortNum-1].udh,    // dev_handle
                         (tU8)LIBUSB_ENDPOINT_OUT | USB_RT_PORT,   // bmRequestType
                                  u32Tmp ? (tU8)LIBUSB_REQUEST_SET_FEATURE : (tU8)LIBUSB_REQUEST_CLEAR_FEATURE, // bRequest
                                  USB_PORT_FEAT_POWER,  // wValue
                                  rUsbPortData[PortNum-1].u8HubPortID,           // wIndex
                                  NULL,            // data
                                  0,               // wLength
                                  USB_TIMEOUT );   // timeout
#endif
            if( ret != 0 )
            {
                  vPrmTtfisTrace( TR_LEVEL_FATAL, "failed to control port %u ret=%d", PortNum, ret);
                  /*Return Value: libusb ReturnValue is of no use for OSAL application.
                     On success, the number of bytes actually transferred */
                  ret = OSAL_E_UNKNOWN;
            }
            else
            {
               ret = OSAL_E_NOERROR;
            }

          }
         }
      }

      pcontrol->PortMask  >>= 1;
      pcontrol->ValueMask >>= 1;
      PortNum++;
   }
   return ret;
}

/*------------------------------------------------------------------------
ROUTINE NAME : GetUsbDeviceType

PARAMETERS   :

RETURNVALUE  : OK: 0 else error

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/
static int GetUsbDeviceType( tU32 u32DevIndex, libusb_device * const * DevList)
{
    int ReturnValue;
    tU8 u8DevAddr;
    tU8 u8BusNum;
    libusb_device *curdev;
    struct libusb_device_descriptor desc;
    struct libusb_device_handle *udh_current;
    if(!plibusb_open)return -1;

    curdev = DevList[u32DevIndex];
    u8BusNum     = plibusb_get_bus_number( curdev );
    u8DevAddr     = plibusb_get_device_address( curdev );

    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "\n%s: DEVINDEX [%d]=0x%X BusNum=%d, DevNum[Addr]=%d",__func__,u32DevIndex,(unsigned int)curdev, u8BusNum,u8DevAddr);

    if( (ReturnValue = plibusb_open( curdev, &udh_current)) != 0 )
    {
        vPrmTtfisTrace( TR_LEVEL_FATAL, "%s: DEVINDEX [%d] libusb_open failed with %d",__func__,u32DevIndex,ReturnValue);
        return ReturnValue;
    }


    if( (ReturnValue = plibusb_get_device_descriptor( curdev, &desc )) != 0 )
    {
        vPrmTtfisTrace( TR_LEVEL_FATAL, "%s: DEVINDEX [%d] libusb_get_device_descriptor failed with %d",__func__,u32DevIndex,ReturnValue);
        plibusb_close(udh_current);
        return ReturnValue;
    }
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s: DEVINDEX [%d]  BusNum=%d, DevNum[Addr]=%d curdev=0x%X LIBUSB_CLASS:%d ",
            __func__,u32DevIndex,u8BusNum,u8DevAddr,curdev,(tU8)desc.bDeviceClass);

    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] bLength            0x%08X ",__func__,u32DevIndex ,(tU8)(desc.bLength));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] bDescriptorType    0x%08X ",__func__,u32DevIndex ,(tU8)(desc.bDescriptorType ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] bDeviceClass       0x%08X ",__func__,u32DevIndex ,(tU8)(desc.bDeviceClass ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] bDeviceSubClass    0x%08X ",__func__,u32DevIndex ,(tU8)(desc.bDeviceSubClass ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] bDeviceProtocol    0x%08X ",__func__,u32DevIndex ,(tU8)(desc.bDeviceProtocol ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] bMaxPacketSize0    0x%08X ",__func__,u32DevIndex ,(tU8)(desc.bMaxPacketSize0 ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] idVendor           0x%08X ",__func__,u32DevIndex ,(tU16)(desc.idVendor ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] idProduct          0x%08X ",__func__,u32DevIndex ,(tU16)(desc.idProduct ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] iManufacturer      0x%08X ",__func__,u32DevIndex ,(tU8)(desc.iManufacturer ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] iProduct           0x%08X ",__func__,u32DevIndex ,(tU8)(desc.iProduct ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] iSerialNumber      0x%08X ",__func__,u32DevIndex ,(tU8)(desc.iSerialNumber ));
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL,"%s: DEVINDEX [%d] bNumConfigurations 0x%08X ",__func__,u32DevIndex ,(tU8)(desc.bNumConfigurations ));


    if( desc.bDeviceClass != (tU8)LIBUSB_CLASS_HUB )
    {
        plibusb_close(udh_current);
        return ReturnValue;
    }
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s: DEVINDEX [%d] !!!FOUND!!! LIBUSB_CLASS_HUB @ BusNum=%d, DevNum[Addr]=%d dev=0x%08X LIBUSB_CLASS:0x%08X ",
            __func__,u32DevIndex,u8BusNum,u8DevAddr,curdev,(tU8)desc.bDeviceClass);

#ifndef SUPPORT_EXT_HUBS
    if(desc.idProduct != 2) // 1 = USB2.0 Hub | 2 = EHCI Host Controller
    {
        vPrmTtfisTrace( TR_LEVEL_FATAL, "External Hub Operations Not supported !Device Dropped! %d",ReturnValue );
        plibusb_close(udh_current);
        return ReturnValue;
    }
#endif

    if( (ReturnValue = InfoUsbHub(udh_current, u8BusNum, u8DevAddr)) != 0 )
    {
        vPrmTtfisTrace( TR_LEVEL_FATAL, "InfoUsbHub FAILED with %d",ReturnValue );
        plibusb_close(udh_current);
        return ReturnValue;
    }
    plibusb_close(udh_current);
    return ReturnValue;
}


int InitUsb(libusb_context ** pctx)
{
    if(pUsbLibHandle == NULL)pUsbLibHandle = LoadModul("/lib/libusb-1.0.so",GetUsbFunktionPtr);

    if(!plibusb_init)return -1;
    int Ret = plibusb_init( pctx );
    if( Debuglevel > 1 )
    {
       plibusb_set_debug( *pctx, 3 );
    }
    return Ret;
}

int InitUsbForPrm(void)
{
   return InitUsb(&lusbctx);
}

void DeInitUsb(libusb_context ** pctx)
{
    ReleaseUsbHub( pctx );
}

void DeInitUsbForPrm(void)
{
   DeInitUsb(&lusbctx);
}


/*------------------------------------------------------------------------
ROUTINE NAME : GetUsbDeviceList

PARAMETERS   :

RETURNVALUE  : OK: 0 else error

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static int GetUsbDeviceList(libusb_context ** pctx)
{
    int ReturnValue;
    unsigned int DevIndex;
    unsigned int NumDevs;
    libusb_device **DevList;

/*    ReturnValue = libusb_init( pctx );
    if( ReturnValue < 0 )
    {
      ReleaseUsbHub( pctx );
      vPrmTtfisTrace( TR_LEVEL_FATAL, "%s: USB hub initialization failed with err=%d",__func__,ReturnValue);
      return ReturnValue;
    }
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s: USB INIT SUCCESS CTX:0x%X",__func__,*pctx );
    if( Debuglevel > 1 )
    {
      libusb_set_debug( *pctx, 3 );
    }*/
    if(plibusb_get_device_list == NULL) return -1;
    ReturnValue = plibusb_get_device_list( *pctx, &DevList );
    if( ReturnValue < 0 )
    {
    //  ReleaseUsbHub( pctx );
      vPrmTtfisTrace( TR_LEVEL_FATAL, "%s: failed to get USB hub device list",__func__);
      return ReturnValue;
    }
    if( ReturnValue == 0 )
    {
      vPrmTtfisTrace( TR_LEVEL_FATAL, "%s: error no devices on the hub list",__func__ );
      plibusb_free_device_list( DevList, 1 );
      ReturnValue = -ENODEV;
    //  ReleaseUsbHub( pctx );
      return ReturnValue;
    }
    NumDevs = (unsigned int)ReturnValue;
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s: libusb_get_device_list: Number of Devices:%d",__func__, NumDevs );

    for( DevIndex = 0; DevIndex < NumDevs; DevIndex++ )
        GetUsbDeviceType(DevIndex, (libusb_device * const *)DevList);

    plibusb_free_device_list( DevList, 1 );
//    ReleaseUsbHub( pctx );
    return ReturnValue;
}
/*------------------------------------------------------------------------
ROUTINE NAME : prmGetPRMUSBPortCount(tS32 s32arg)

PARAMETERS   : Address of the variable to store port count

RETURNVALUE  : OK: 0; else : error code

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/
tU32 prmGetPRMUSBPortCount(tS32 s32arg)
{
    TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "%s: s32arg = 0x%X u32PortCount=%d",__func__,s32arg,pOsalData->u32PrmUSBGlobalPortCount);
    if(s32arg)
    {
        *(tS32*)(s32arg) = (tS32)pOsalData->u32PrmUSBGlobalPortCount;
        return OSAL_E_NOERROR;
    }
    return OSAL_E_INVALIDVALUE;
}

/*------------------------------------------------------------------------
ROUTINE NAME : ReleaseUsbHub

PARAMETERS   :

RETURNVALUE  : void

DESCRIPTION  : Called during shutdown

COMMENTS     :

------------------------------------------------------------------------*/
static void ReleaseUsbHub( libusb_context **pctx )
{

    vPrmTtfisTrace( TR_LEVEL_FATAL, "ReleaseUsbHub called with pctx == %p ", pctx );

    if( pctx!= NULL )
   {
       if(*pctx != NULL)
       {
           if(plibusb_exit)plibusb_exit( *pctx );
           *pctx = NULL;
       }
       else
       {
              vPrmTtfisTrace( TR_LEVEL_FATAL, "ReleaseUsbHub *pctx == NULL " );
              return;
       }
   }
    else
    {
        vPrmTtfisTrace( TR_LEVEL_FATAL, "ReleaseUsbHub INVALID pctx == NULL " );
    }


}

/*------------------------------------------------------------------------
ROUTINE NAME : UvEvent_Socket_Open

PARAMETERS   :

RETURNVALUE  : OK: socketFd; negativ value: error

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static int UvEvent_Socket_Open( void )
{
   int ReturnValue;
   int _sock;
   struct sockaddr socke;
   struct sockaddr_nl *snl;
   int rcvbufsz = 128 * 1024;
   int rcvsz = 0;
   int rcvszsz = sizeof( rcvsz );
   unsigned int *prcvszsz = (unsigned int *)&rcvszsz;
   const int feature_on = 1;

   snl = (struct sockaddr_nl*)(void*) &socke;
   memset( snl, 0, sizeof( struct sockaddr_nl ));
   snl->nl_family = AF_NETLINK;
   snl->nl_pid    = 0; /*(tU32)getpid();Let kernel assign the PortID for netlink socket*/
   snl->nl_groups = 0x01;

   _sock = socket( PF_NETLINK, (int)SOCK_DGRAM, NETLINK_KOBJECT_UEVENT );
   TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "socket returned : %d (pid=%d)", _sock, snl->nl_pid );

   if( _sock >= 0 )
   {
      TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "prepare socket connection to read events from kernel" );
      ReturnValue = setsockopt( _sock, SOL_SOCKET, SO_RCVBUF, &rcvbufsz, sizeof( rcvbufsz ) );
      if( ReturnValue < 0 )
      {
         close( _sock );
         vPrmTtfisTrace( TR_LEVEL_FATAL, "error setting receive buffer size for socket" );
         return -EFAULT;
      }
      TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "setsockopt socket : %d", _sock );
      ReturnValue = getsockopt( _sock, SOL_SOCKET, SO_RCVBUF, &rcvsz, prcvszsz );
      if( ReturnValue < 0 )
      {
         close( _sock );
         vPrmTtfisTrace( TR_LEVEL_FATAL, "error getting receive buffer size for socket" );
         return -EFAULT;
      }
      TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "getsockopt socket : %d", _sock );

      ReturnValue = bind(_sock, &socke, sizeof(struct sockaddr_nl)); /*lint !e64*/
      if( ReturnValue < 0 )
      {
         close( _sock );
         vPrmTtfisTrace( TR_LEVEL_FATAL, "bind failed, exit %i %i", ReturnValue, errno );
         return -EFAULT;
      }
      TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "bind socket : %d", _sock );
   }
   ReturnValue = setsockopt( _sock, SOL_SOCKET, SO_PASSCRED, &feature_on, sizeof( feature_on) );
   if( ReturnValue < 0 )
   {
      close( _sock );
      vPrmTtfisTrace( TR_LEVEL_FATAL, "error setting sender credentials" );
      return -EFAULT;
   }

   TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "socket returned : %d (pid=%d)", _sock, snl->nl_pid );
   return _sock;
}


/*------------------------------------------------------------------------
ROUTINE NAME : service_queue

PARAMETERS   :

RETURNVALUE  : void

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static void service_queue( void )
{
   int empty;
   struct uevent *uev;
   static UsbPortState rPortState = {0};

   do
   {
      /* --- get new msg from queue */
      pthread_mutex_lock( uevq_lockp );
      empty = (uevqhp == NULL);
      if( ! empty )
      {
         uev = uevqhp;
         uevqhp = uev->next;
         if( uevqtp == uev )
         {
            uevqtp = uev->next;
         }
         pthread_mutex_unlock( uevq_lockp );
         /* --- new msg from queue availaable */

         TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "ServiceQ(): servicing uevent 0x%08X typ: %d from %10d ms", uev, uev->type, uev->u32Time );

         switch(uev->oc_state) {
              case 0://OC end
                  TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "ServiceQ():OCC = OC_END PORT ON - NORMAL CONDITION");

                  rPortState.u8PortNr = (tU8)uev->PortID;

                 rPortState.u8OC = (tU8)SIG_FALSE;
                //rPortState.u32OCStartTime;
                rPortState.u32OCEndTime = uev->u32Time;

                //rPortState.u8UV;
                //rPortState.u32UVStartTime;
                //rPortState.u32UVEndTime;

                rPortState.u8PPON = (tU8)SIG_TRUE;
                rPortState.u32PPONStartTime = uev->u32Time;
                rPortState.u32PPONEndTime = 0;

                vSendMsgToPrmRecTask(&rPortState);

                break;

              case 1://OC Start
                TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "ServiceQ():OCC=OC_START ACTIVE OverCurrent Condition");

                rPortState.u8PortNr = (tU8)uev->PortID;

                rPortState.u8OC = (tU8)SIG_TRUE;
                rPortState.u32OCStartTime = uev->u32Time;
                rPortState.u32OCEndTime = 0;

                //rPortState.u8UV;
                //rPortState.u32UVStartTime;
                //rPortState.u32UVEndTime;

                rPortState.u8PPON = (tU8)SIG_FALSE;
                rPortState.u32PPONStartTime = 0;
                rPortState.u32PPONEndTime = uev->u32Time;

                vSendMsgToPrmRecTask(&rPortState);
                 break;


              default:
                 TraceStringClassLevel( OSAL_C_TR_CLASS_DEV_USBPOWER,TR_LEVEL_FATAL, "ServiceQ(): <<<<<<<<<<<<<< !UNEXPECTED STATE! >>>>>>>>>>>");
                  break;
         }

         free( uev );
      }
      else
      {
         pthread_mutex_unlock( uevq_lockp );
      }
   }while( !empty );
}


/*------------------------------------------------------------------------
ROUTINE NAME : UvEvent_queue_thread

PARAMETERS   : void

RETURNVALUE  : void

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

/* thread for event processing */
static void UvEvent_queue_thread( void )
{
   int ReturnValue;
   int sched_policy;
   struct sched_param param;

   // todo change priority
   ReturnValue = pthread_getschedparam( pthread_self(), &sched_policy, &param);
   if( ReturnValue != 0 )
         vPrmTtfisTrace( TR_LEVEL_FATAL, "failed to get ev tread priority, error= %d", ReturnValue );

   param.sched_priority = 30;
   sched_policy = SCHED_FIFO;  // SCHED_RR, SCHED_OTHER
   ReturnValue = pthread_setschedparam( pthread_self(), sched_policy, &param);
   if( ReturnValue != 0 )
   {
      pOsalData->bPrmUsbPwrActiv = FALSE;
      vPrmTtfisTrace( TR_LEVEL_FATAL, "failed to set ev tread priority, error= %d", ReturnValue );
   }

   while( pOsalData->bPrmUsbPwrActiv == TRUE )
   {
      pthread_mutex_lock( uevc_lockp );
      pthread_cond_wait( uev_condp, uevc_lockp );
      pthread_mutex_unlock (uevc_lockp );
      service_queue();
   }
}

/****************************************************************************
* FUNCTION    : vUSBPWRCallbackHandler
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is handling USB power callback
* SYNTAX      : tVoid vUSBPWRCallbackHandler(void* pvBuffer )
* ARGUMENTS   : pvBuffer   command via trace
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
tVoid vUSBPWRCallbackHandler(void* pvBuffer )
{
  ((void)pvBuffer);
}

#endif

#else //#if !defined (LSIM) &&  !defined (OSAL_GEN3_SIM) //Until massive Cleanup
/************************************************************************ 
| externals from other modules
|-----------------------------------------------------------------------*/

extern OSAL_tMQueueHandle prm_hPrmMQ;


/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/

#ifdef PRM_LIBUSB_CONNECTION

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
static void vPrmLibUsbConnectionTask( int status );
static void vSendMsgToPrmRecTask( tU8 u8Port, UsbPortState const *PortState );
#define USBPWR_TRACE_LEVEL TR_LEVEL_USER_1

/************************************************************************
|function prototype (scope: module-Global)
|-----------------------------------------------------------------------*/
tVoid vUSBPWRCallbackHandler(void* pvBuffer );

#define USB_PWR_MSG_SIZE                           50
#define MSG_QUEUE_NAME                             "USBPWR_MSGQUE_LSIM"
#define USBPWR_MQ_NORMAL_PRIORITY                  5
#define USBPWR_MQ_HIGH_PRIORITY                    0
#define USBPWR_CMD_SHUTDOWN                        99

enum
{
   CMD_USB_UV_ALLACTIVE = 1,
   CMD_USB_UV_ALLINACTIVE,
   CMD_USB_UV_ACTIVE,
   CMD_USB_UV_INACTIVE,
   CMD_USB_OC_ACTIVE,
   CMD_USB_OC_INACTIVE,
   CMD_USB_SIG_UNDEF
};

enum
{
   USB_PWR_UV = 1,
   USB_PWR_OC,
   USB_PWR_UNDEF
};

typedef struct Prm_UsbPwrMsg
{
   tU8  u8NtrkDspr;
   tU8  u8PortNr;
   tU8  u8Activ;
   tU8  u8OC_UV_UNDEF;
}trPrm_UsbPwrMsg;

OSAL_tMQueueHandle  hUSBPWRMq;


/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/


/*------------------------------------------------------------------------
ROUTINE NAME : prm_CreateLibUsbConnectionTask

PARAMETERS   :

RETURNVALUE  : OSAL ThreadId

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

OSAL_tThreadID prm_CreateLibUsbConnectionTask( void )
{
#if !defined (LSIM) && !defined (OSAL_GEN3_SIM)
   OSAL_trThreadAttribute  attr;
   OSAL_tThreadID ThId;

   /* start PRM task */
   attr.szName = "PRM_LIBUSB";
   attr.u32Priority  = OSALIO_PRIORITY_THREAD_PRM_LIBUSB_TA;
   attr.s32StackSize = minStackSize;
   attr.pfEntry = (OSAL_tpfThreadEntry)vPrmLibUsbConnectionTask;
   attr.pvArg   = NULL;
   ThId = OSAL_ThreadSpawn( &attr );
   if( ThId == OSAL_ERROR )
   {
      FATAL_M_ASSERT_ALWAYS();
   }
   return( ThId );

#else /*LSIM*/
   /*This function creates a driver message queue and task thread*/
   OSAL_trThreadAttribute  attr;
   OSAL_tThreadID ThId;
   tS32 s32retValue = OSAL_OK;
   tU8 u8Count;

   /*Initialize the port number in the structure*/
   for(u8Count = 0; u8Count < MAX_USB_PORTS; u8Count++)
   {
      pOsalData->prm_rPortState[u8Count].u8PortNr = u8Count + 1;
   }
   if(OSAL_s32MessageQueueCreate( MSG_QUEUE_NAME, USB_PWR_MSG_SIZE, sizeof(trPrm_UsbPwrMsg),
          OSAL_EN_READWRITE, &hUSBPWRMq) == OSAL_ERROR )
   {
      s32retValue = OSAL_ERROR;
      vPrmTtfisTrace((tU32)TR_LEVEL_USER_3,"Messagequeue creation failed in prm_CreateLibUsbConnectionTask");
   }
   if(s32retValue == OSAL_OK)
   {
      /* start PRM task */
      attr.szName = "PRM_LIBUSB";
      attr.u32Priority  = OSALIO_PRIORITY_THREAD_PRM_LIBUSB_TA;
      attr.s32StackSize = minStackSize;
      attr.pfEntry = (OSAL_tpfThreadEntry)vPrmLibUsbConnectionTask;
      attr.pvArg   = NULL;
      ThId = OSAL_ThreadSpawn( &attr );
      if( ThId == OSAL_ERROR )
      {
         s32retValue = OSAL_ERROR;
         FATAL_M_ASSERT_ALWAYS();
      }
      else
      {
         s32retValue = ThId;
      }
   }
   if(s32retValue == OSAL_ERROR)
   {
      if(OSAL_C_INVALID_HANDLE != hUSBPWRMq)
      {
         if(OSAL_OK != OSAL_s32MessageQueueClose( hUSBPWRMq ))
         {
            vPrmTtfisTrace( (tU32)TR_LEVEL_USER_3,"Message close failed in prm_CreateLibUsbConnectionTask" );
         }
         /*Delete the message queue*/
         if(OSAL_OK != OSAL_s32MessageQueueDelete( MSG_QUEUE_NAME ))
         {
            vPrmTtfisTrace( (tU32)TR_LEVEL_USER_3,"Message delete failed in prm_CreateLibUsbConnectionTask" );
         }
      }
   }
   else
   {
      vPrmTtfisTrace( (tU32)TR_LEVEL_USER_3,"prm_CreateLibUsbConnectionTask successfully initialized" );
   }
   return( s32retValue );
#endif
}


/*------------------------------------------------------------------------
ROUTINE NAME : prm_vDeleteLibUsbConnectionTask

PARAMETERS   : ThreadId            ( I ) = Application ID

RETURNVALUE  :

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

void prm_vDeleteLibUsbConnectionTask( OSAL_tThreadID ThId )
{
#if !defined (LSIM) && !defined (OSAL_GEN3_SIM)
   if( OSAL_ERROR == OSAL_s32ThreadDelete( ThId ) )
   {
      NORMAL_M_ASSERT_ALWAYS();
   }

#else /*LSIM*/

   /*This function post a shutdown command to this driver messageQueue*/
   trPrm_UsbPwrMsg rMsg;
   pOsalData->bPrmUsbPwrActiv = FALSE;

   rMsg.u8OC_UV_UNDEF = USBPWR_CMD_SHUTDOWN;

   (void)ThId;
   if(OSAL_s32MessageQueuePost(hUSBPWRMq,(tPCU8)&rMsg,sizeof(trPrm_UsbPwrMsg),USBPWR_MQ_HIGH_PRIORITY) != OSAL_OK)
   {
      vPrmTtfisTrace((tU32)TR_LEVEL_USER_3,"Message queue post failed in prm_vDeleteLibUsbConnectionTask");
   }
#endif
}



/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/


/*------------------------------------------------------------------------
ROUTINE NAME : Prm_GetUsbPortState

PARAMETERS   : Port number            ( I ) = Application ID

RETURNVALUE  : Pointer to the portstate variable

DESCRIPTION  :

COMMENTS     : called from prm_table.c only

------------------------------------------------------------------------*/

tU32 Prm_GetUsbPortState(UsbPortState* rPortState)
{

   tS32 s32RetVal = (tS32)OSAL_NULL;

    tU8 u8Port = 0;
    if(!rPortState)
        return (tU32)OSAL_ERROR;
    else
    {
        u8Port = rPortState->u8PortNr;
    }
    vPrmTtfisTrace( USBPWR_TRACE_LEVEL, "Prm_GetUsbPortState: PORT %d ",u8Port);
    if((u8Port > 0)&&(u8Port <= pOsalData->u32PrmUSBGlobalPortCount))
    {
        memcpy(&pOsalData->prm_rPortState[u8Port - 1], rPortState, sizeof(UsbPortState));
        return s32RetVal;
    }
    else
    {
        vPrmTtfisTrace((tU32)TR_LEVEL_USER_3,"Prm_GetUsbPortState:Unknown port number:%d",u8Port);
        return (tU32)OSAL_ERROR;
    }
}

/*------------------------------------------------------------------------
ROUTINE NAME : prmGetPRMUSBPortCount(tS32 s32arg)

PARAMETERS   :

RETURNVALUE  : OK: 0; else : error code

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/
tU32 prmGetPRMUSBPortCount(tS32 s32arg)
{
    vPrmTtfisTrace( TR_LEVEL_FATAL, "%s: s32arg = 0x%X u32PortCount=%d",__func__,s32arg,(tS32)pOsalData->u32PrmUSBGlobalPortCount);
    if(s32arg)
    {
        *(tS32*)(s32arg) = (tS32)pOsalData->u32PrmUSBGlobalPortCount;
        vPrmTtfisTrace( TR_LEVEL_FATAL, "%s: *(tS32*)(s32arg) = 0x%X u32PortCount=%d",__func__,*(tS32*)(s32arg),(tS32)pOsalData->u32PrmUSBGlobalPortCount);
        return 0;
    }
    return -1;
}

/*------------------------------------------------------------------------
ROUTINE NAME : vSendMsgToPrmRecTask

PARAMETERS   :

RETURNVALUE  : void

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static void vSendMsgToPrmRecTask( tU8 u8Port, UsbPortState const *PortState )
{
   static UsbPortState rPortState[MAX_USB_PORTS] = {0};   // static: used for old state

   UsbPortState * pOldPortState;
   trPrmMsg rMsg;

   rMsg.s32ResID    = (tS32)RES_ID_VOLTAGE;
   rMsg.u32EvSignal = (tU32)SIGNAL_VOLTAGE;
   rMsg.u32Val      = 0;
   rMsg.au8UUID[0]  = 0;

   /*This function checks for state change and posts message to PRM*/
   pOldPortState = &rPortState[u8Port - 1];

   /*check for state change*/
   if(((PortState->u8UV == (tU8)SIG_UNDEF ) && (PortState->u8OC  == (tU8)SIG_UNDEF )) ||
       ((pOldPortState->u8UV   != PortState->u8UV)   ||
       (pOldPortState->u8OC   != PortState->u8OC)   ||
       (pOldPortState->u8PPON != PortState->u8PPON)))
   {
      memcpy( rMsg.au8UUID,  PortState, sizeof(UsbPortState) );
      memcpy( pOldPortState, PortState, sizeof(UsbPortState) );

      if(( rMsg.s32ResID != (tS32)RES_ID_INVALID ) &&
         (rMsg.u32EvSignal != (tU32)SIGNAL_INVALID) &&
         (rMsg.u32Val < PRM_MAX_USB_DEVICES))
      {
         (void)OSAL_s32MessageQueuePost( prm_hPrmMQ, (tPCU8)&rMsg, sizeof(trPrmMsg), 0 );
      }
   }
   else
   {
      vPrmTtfisTrace((tU32)TR_LEVEL_FATAL,"No change in state");
      return;  // no change in state, don't report
   }

}

/*------------------------------------------------------------------------
ROUTINE NAME : vPrmLibUsbConnectionTask

PARAMETERS   :

RETURNVALUE  : void

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/

static void vPrmLibUsbConnectionTask( int status )
{
   /*This function updated the data structure according to the
      UV/OC/UNDEF signal received in message queue and sends
      to PRM by posting message queue.*/
   tS32 s32retValue;
   trPrm_UsbPwrMsg rMsg;
   tU32 u32Time;
   tU8 u8Count;

   (void)status;  // lint

   while(pOsalData->bPrmUsbPwrActiv)
   {
      /*Wait for the message */
      s32retValue = OSAL_s32MessageQueueWait( hUSBPWRMq, (tPU8)&rMsg, sizeof(trPrm_UsbPwrMsg),
                                          OSAL_NULL, OSAL_C_TIMEOUT_FOREVER );
      if( s32retValue != OSAL_ERROR )
      {
         /* Every time this timer is done to ensure that timer notification is done as earliest */
         vPrmTtfisTrace((tU32)TR_LEVEL_USER_3,"vPrmLibUsbConnectionTask:Received Message");

         /*check for current/voltage/undefined data updation*/
         switch(rMsg.u8OC_UV_UNDEF)
         {
            case USB_PWR_UV:
               {
                  /*check whether all ports should be active/inactive or specific port should be active/inactive for undervoltage*/
                  switch(rMsg.u8Activ)
                  {
                     case CMD_USB_UV_ALLACTIVE:/*all ports undervoltage should be active*/
                        {
                           for(u8Count = 0; u8Count < pOsalData->u32PrmUSBGlobalPortCount; u8Count++)
                           {
                              u32Time  = OSAL_ClockGetElapsedTime();
                              pOsalData->prm_rPortState[u8Count].u32UVStartTime   = u32Time;
                              pOsalData->prm_rPortState[u8Count].u32PPONStartTime = u32Time;
                              pOsalData->prm_rPortState[u8Count].u32UVEndTime     = 0;
                              pOsalData->prm_rPortState[u8Count].u32PPONEndTime   = 0;
                              pOsalData->prm_rPortState[u8Count].u8PPON           = (tU8)SIG_FALSE;
                              pOsalData->prm_rPortState[u8Count].u8UV             = (tU8)SIG_TRUE;
                           }
                           for(u8Count = 0; u8Count < pOsalData->u32PrmUSBGlobalPortCount; u8Count++)
                           {
                              vSendMsgToPrmRecTask(u8Count + 1, & pOsalData->prm_rPortState[u8Count] );
                           }
                        }
                     break;
                     case CMD_USB_UV_ALLINACTIVE:/*all port undervoltage should be inactive*/
                        {
                           for(u8Count = 0; u8Count < pOsalData->u32PrmUSBGlobalPortCount; u8Count++)
                           {
                              u32Time  = OSAL_ClockGetElapsedTime();
                              pOsalData->prm_rPortState[u8Count].u32UVEndTime   = u32Time;
                              pOsalData->prm_rPortState[u8Count].u8UV           = (tU8)SIG_FALSE;
                           }
                           for(u8Count = 0; u8Count < pOsalData->u32PrmUSBGlobalPortCount; u8Count++)
                           {
                              vSendMsgToPrmRecTask(u8Count + 1, & pOsalData->prm_rPortState[u8Count] );
                           }

                        }
                     break;
                     case CMD_USB_UV_ACTIVE:/*spcific port undervoltage should be active*/
                        {
                           if(rMsg.u8PortNr > pOsalData->u32PrmUSBGlobalPortCount || !rMsg.u8PortNr)
                           {
                              vPrmTtfisTrace((tU32)TR_LEVEL_FATAL,"vPrmLibUsbConnectionTask:\
                                             CMD_USB_UV_ACTIVE:Unknown port number:%d",rMsg.u8PortNr);
                           }
                           else
                           {
                              u32Time  = OSAL_ClockGetElapsedTime();
                              pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32UVStartTime   = u32Time;
                              pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32PPONStartTime = u32Time;
                              pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32UVEndTime     = 0;
                              pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32PPONEndTime   = 0;
                              pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8PPON           = (tU8)SIG_FALSE;
                              pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8UV             = (tU8)SIG_TRUE;
                              vSendMsgToPrmRecTask(rMsg.u8PortNr, & pOsalData->prm_rPortState[rMsg.u8PortNr - 1] );
                           }

                        }
                     break;
                     case CMD_USB_UV_INACTIVE:
                        {
                           if(rMsg.u8PortNr > pOsalData->u32PrmUSBGlobalPortCount || !rMsg.u8PortNr)
                           {
                              vPrmTtfisTrace((tU32)TR_LEVEL_FATAL,"vPrmLibUsbConnectionTask:\
                                             CMD_USB_UV_INACTIVE:Unknown port number:%d",rMsg.u8PortNr);
                           }
                           else
                           {
                              u32Time  = OSAL_ClockGetElapsedTime();
                              pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32UVEndTime   = u32Time;
                              pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8UV           = (tU8)SIG_FALSE;
                              vSendMsgToPrmRecTask(rMsg.u8PortNr, & pOsalData->prm_rPortState[rMsg.u8PortNr - 1] );
                           }

                        }
                     break;
                     default:
                        {
                           vPrmTtfisTrace((tU32)TR_LEVEL_FATAL,"vPrmLibUsbConnectionTask:default:\
                                          Unknown parameter u8Activ :%d",rMsg.u8Activ);
                        }
                     break;
                  }
               }
            break;
            case USB_PWR_OC:/*overcurrent*/
               {
                  if(rMsg.u8Activ == CMD_USB_OC_ACTIVE)/*Active overcurrent for specific port*/
                  {
                     if(rMsg.u8PortNr > pOsalData->u32PrmUSBGlobalPortCount || !rMsg.u8PortNr)
                     {
                        vPrmTtfisTrace((tU32)TR_LEVEL_FATAL,"vPrmLibUsbConnectionTask:\
                                       CMD_USB_OC_ACTIVE:Unknown port number:%d",rMsg.u8PortNr);
                     }
                     else
                     {
                        u32Time  = OSAL_ClockGetElapsedTime();
                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32OCStartTime   = u32Time;
                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32PPONStartTime = u32Time;
                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32OCEndTime     = 0;
                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32PPONEndTime   = 0;
                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8PPON           = (tU8)SIG_FALSE;
                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8OC             = (tU8)SIG_TRUE;
                        vSendMsgToPrmRecTask(rMsg.u8PortNr, & pOsalData->prm_rPortState[rMsg.u8PortNr - 1] );
                     }

                  }
                  else if(rMsg.u8Activ == CMD_USB_OC_INACTIVE)/*InActive overcurrent for specific port*/
                  {
                     if(rMsg.u8PortNr > pOsalData->u32PrmUSBGlobalPortCount || !rMsg.u8PortNr)
                     {
                        vPrmTtfisTrace((tU32)TR_LEVEL_FATAL,"vPrmLibUsbConnectionTask:\
                                       CMD_USB_OC_INACTIVE:Unknown port number:%d",rMsg.u8PortNr);
                     }
                     else
                     {
                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8PPON = (tU8)SIG_TRUE;
                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32PPONEndTime = OSAL_ClockGetElapsedTime();
                        vSendMsgToPrmRecTask(rMsg.u8PortNr, & pOsalData->prm_rPortState[rMsg.u8PortNr - 1] );

                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32OCEndTime = OSAL_ClockGetElapsedTime();;
                        pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8OC         = (tU8)SIG_FALSE;
                        vSendMsgToPrmRecTask(rMsg.u8PortNr, & pOsalData->prm_rPortState[rMsg.u8PortNr - 1] );
                     }
                  }
               }
            break;
            case USB_PWR_UNDEF:/*Undefined for specific port*/
               {
                  if(rMsg.u8PortNr > pOsalData->u32PrmUSBGlobalPortCount || !rMsg.u8PortNr)
                  {
                     vPrmTtfisTrace((tU32)TR_LEVEL_FATAL,"vPrmLibUsbConnectionTask:\
                                    USB_PWR_UNDEF:Unknown port number:%d",rMsg.u8PortNr);
                  }
                  else
                  {
                     pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32UVStartTime   = (tU32)SIG_UNDEF;
                     pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32OCStartTime   = (tU32)SIG_UNDEF;
                     pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32OCEndTime     = (tU32)SIG_UNDEF;
                     pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32PPONStartTime = (tU32)SIG_UNDEF;
                     pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32UVEndTime       = (tU32)SIG_UNDEF;
                     pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u32PPONEndTime = (tU32)SIG_UNDEF;
                     pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8PPON           = (tU8)SIG_UNDEF;
                     pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8UV             = (tU8)SIG_UNDEF;
                     pOsalData->prm_rPortState[rMsg.u8PortNr - 1].u8OC             = (tU8)SIG_UNDEF;
                     vSendMsgToPrmRecTask(rMsg.u8PortNr, & pOsalData->prm_rPortState[rMsg.u8PortNr - 1] );
                  }
               }
            break;
            case USBPWR_CMD_SHUTDOWN:
               {
                  vPrmTtfisTrace((tU32)TR_LEVEL_USER_3,"vPrmLibUsbConnectionTask:\
                                 u8OC_UV_UNDEF:Shutdown USB driver");
               }
            break;
            default:
               {
                  vPrmTtfisTrace((tU32)TR_LEVEL_USER_3,"vPrmLibUsbConnectionTask:\
                                 u8OC_UV_UNDEF:Unknown number:%d",rMsg.u8OC_UV_UNDEF);
               }
            break;
         }
      }
   }
   /*close the message queue*/
   if(OSAL_C_INVALID_HANDLE != hUSBPWRMq)
   {
      if(OSAL_OK != OSAL_s32MessageQueueClose( hUSBPWRMq ))
      {
         vPrmTtfisTrace((tU32)TR_LEVEL_USER_3,"Message close failed in vPrmLibUsbConnectionTask");
      }
      /*Delete the message queue*/
      if(OSAL_OK != OSAL_s32MessageQueueDelete( MSG_QUEUE_NAME ))
      {
         vPrmTtfisTrace((tU32)TR_LEVEL_USER_3,"Message delete failed in vPrmLibUsbConnectionTask");
      }
   }
}

/****************************************************************************
* FUNCTION    : vUSBPWRCallbackHandler
* CREATED     :
* AUTHOR      : Suresh Dhandapani(RBEI/ECF5)
* DESCRIPTION : This function is handling USB power callback
* SYNTAX      : tVoid vUSBPWRCallbackHandler(void* pvBuffer )
* ARGUMENTS   : pvBuffer   command via trace
*
* RETURN VALUE: tVoid
* NOTES       :   -
****************************************************************************/
tVoid vUSBPWRCallbackHandler(void* pvBuffer )
{
   tPU8 pu8Buffer;
   /*Initiaize value "0" to message structure*/
   trPrm_UsbPwrMsg rMsg = {OSAL_NULL,OSAL_NULL,OSAL_NULL,OSAL_NULL};

   (void)rMsg.u8NtrkDspr; /*lint*/
   pu8Buffer = (tPU8)pvBuffer;
   rMsg.u8PortNr = (tU8)pu8Buffer[3];
   
   switch ((tS32)pu8Buffer[2])
   {
      case OSAL_USB_UV_ALLACTIVE:
         {
            rMsg.u8Activ = CMD_USB_UV_ALLACTIVE;
            rMsg.u8OC_UV_UNDEF = USB_PWR_UV;
         }
      break;
      case OSAL_USB_UV_ALLINACTIVE:
         {
            rMsg.u8Activ = CMD_USB_UV_ALLINACTIVE;
            rMsg.u8OC_UV_UNDEF = USB_PWR_UV;
         }         
      break;
      case OSAL_USB_UV_ACTIVE:
         {
            rMsg.u8Activ = CMD_USB_UV_ACTIVE;
            rMsg.u8OC_UV_UNDEF = USB_PWR_UV;
         }
      break;
      case OSAL_USB_UV_INACTIVE:
         {
            rMsg.u8Activ = CMD_USB_UV_INACTIVE;
            rMsg.u8OC_UV_UNDEF = USB_PWR_UV;
         }
      break;
      case OSAL_USB_OC_ACTIVE:
         {
            rMsg.u8Activ = CMD_USB_OC_ACTIVE;
            rMsg.u8OC_UV_UNDEF = USB_PWR_OC;
         }
      break;
      case OSAL_USB_OC_INACTIVE:
         {
            rMsg.u8Activ = CMD_USB_OC_INACTIVE;
            rMsg.u8OC_UV_UNDEF = USB_PWR_OC;
         }
      break;
      case OSAL_USB_SIG_UNDEF:
         {
            rMsg.u8Activ = CMD_USB_SIG_UNDEF;
            rMsg.u8OC_UV_UNDEF = USB_PWR_UNDEF;
         }
      break;
      default: // do nothing
         vPrmTtfisTrace((tU32)TR_LEVEL_FATAL, "vUSBPWRCallbackHandler:error not matched to value:%d",pu8Buffer[2] );
      break;
   }
   if(rMsg.u8Activ != 0)/*whether to do post or not by checking command value is updated or not*/
   {
      if(OSAL_s32MessageQueuePost(hUSBPWRMq,(tPCU8)&rMsg,sizeof(trPrm_UsbPwrMsg),USBPWR_MQ_NORMAL_PRIORITY) == OSAL_ERROR)
      {
         vPrmTtfisTrace((tU32)TR_LEVEL_USER_3,"MessageQueue post failed in vUSBPWRCallbackHandler:u8OC_UV_UNDEF:%d",rMsg.u8OC_UV_UNDEF);
      }
   }
}
#endif //PRM_LIBUSB_CONNECTION
tVoid vUSBPWRCallbackHandler(void* pvBuffer ){}
#endif //ENABLE_LSIM_CODE

#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file prm_usbpower.c
|-----------------------------------------------------------------------*/
