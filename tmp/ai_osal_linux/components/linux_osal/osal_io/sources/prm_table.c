
/************************************************************************
| FILE:         prm_table.c
| PROJECT:      platform
| SW-COMPONENT: OSAL IO
|------------------------------------------------------------------------
|------------------------------------------------------------------------
| DESCRIPTION:  This is the source for the PRM (Physical Resource Manager)
|                 
|                
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2011 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----
| 06.06.12  | New mount point for SDcard | sja3kor 
|             for SDCARD reader. 
| 09.10.12  | Updated medai types for    |
|           |SD_Card with    Newly added |
|           | PrmMsg Stucture Member     | SWM2KOR
|  --.--.-- | ----------------           | -------, -----    
| 15.10.12  | Provided fix for MMS NIKAI-| sja3kor
            | 147.SD card unmounts on USB|
            | Eject.                     |
| --.--.--  | ----------------           | -------, -----  
|23.10.12   |   Fix for NIKAI-434.       | sja3kor 
|07.01.13   |   Fix for USBMS open failure issue | krs4cob
|08.01.13   |   Fix for NULL  UUID is passed to vPrmSignal instead of valid UUID of SDcard  |krs4cob
|09.01.13   |   Fix for NULL  UUID is passed to vPrmSignal instead of valid UUID of USB       |krs4cob
|01.02.13   |   Fix for GET_CARD_STATE ioctl is not working for the non navi cards             |krs4cob
|20.03.13   |   FIX for NIKAI-3163 DT after update can not be recognized                       | SWM2KOR
| 16.03.13  |   Added support for triggering sign verifcation from T-engine side for NIKAI-3713     |krs4cob
|02.05.13   |   Fix for NIKAI-734 provided new API interface for getting the drive name dynamically |krs4cob
|30.07.13   |   Added Fake notification for Audio CD at startup                                     |rmm2hi
|20.05.14   |   Fix for Suzuki CFG3-606 OSAL /dev/media does not support GEN2                       |
|           |   IOCTL OSAL_C_S32_IOCTRL_CARD_STATE                                                  |pmh5kor
|03.06.14   |   Enable PRMLIB USB POWER for Gen3, lint fix                        |kgr1kor
|03.06.2014 |   Fix for SUZUKI-10989 PRM Registry update                                            | SWM2KOR
|27.08.2014 |   Fix for SUZUKI-15825 PRM Notifications for SD-CARD                                  | SWM2KOR 
|19.12.2014 |   Initialize the PRM handling messages INNAVPF-3336                                   | vew5kor
|************************************************************************/


/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/ 

/* TENGINE Header */
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "Linux_osal.h"

#include "ostrace.h"

#include "osfile.h"

#include "fd_crypt.h"
#include "fd_device_ctrl.h"
#include <dirent.h>

#ifdef __cplusplus
extern "C" {
#endif
/* This global variable will update the partition info when requested by the PRM IOControl */
tCString Part_Value = "NULL";

extern void RegisterForMedia(void);
extern void vEraseErrmem(tS32 s32Val);
#ifdef PRM_LIBUSB_CONNECTION
extern void DeInitUsbForPrm(void);
extern int InitUsbForPrm(void);
#endif

/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/
//#define USBDEBUG_CONSOLE_TRACE /*Enable for USBPOWER Testing*/
#ifdef AM_BIN
#include "automounter_types.h"
#include "automounter_api.h"
#include "automounter_api_ctrl.h"
#include "automounter_api_info.h"
#include "automounter_api_events.h"
#endif

#include <sys/inotify.h>
#include <unistd.h>

#define FAILED  -1


#define SEARCH_SLASH(ptr) \
  if (!(*ptr)) \
  { \
    ptr = NULL; \
  } \
  else \
  { \
    do \
    {\
      ++ptr; \
    } while ((*ptr) && (*ptr != '/')); \
  }



/************************************************************************ 
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/


/************************************************************************ 
| variable definition (scope: global) 
|-----------------------------------------------------------------------*/


/************************************************************************ 
| variable definition (scope: module-local) 
|-----------------------------------------------------------------------*/

OSAL_tMQueueHandle prm_hPrmMQ = 0;

/* RESOURCE TABLE */

/*
  To add a new resource:
  -#define resource name in file osioctrl.h
  -Add the resouce in this table
  -Update C_MAX_RESOURCES in prm.h file
*/
static const trResourceTableEntry  coarResourceTable[ C_MAX_RESOURCES ] = 
{
   /* Resource Cryptcard */
   { 
      OSAL_C_STRING_DEVICE_CRYPTCARD,
      (C_U32_PRC_CRYPTCARD_BIT),
      {
         PRM_EN_PRCID_CRYPTCARD,
         PRM_EN_PRCID_LAST,
         PRM_EN_PRCID_LAST,
         PRM_EN_PRCID_LAST
      }
   },
   /* Resource dev/media */
   { 
      "/dev/media",
      (C_U32_PRC_DEV_MEDIA_BIT),
      {
         PRM_EN_PRCID_DEV_MEDIA,
         PRM_EN_PRCID_LAST,
         PRM_EN_PRCID_LAST,
         PRM_EN_PRCID_LAST
      }
   },

   /* Resource Cryptnav */
   { 
      // todo OSAL_C_STRING_DEVICE_CRYPTNAV,
      OSAL_C_STRING_DEVICE_CRYPTNAV,
      (C_U32_PRC_CRYPTNAV_BIT),
      {
         PRM_EN_PRCID_CRYPTNAV,
         PRM_EN_PRCID_LAST,
         PRM_EN_PRCID_LAST,
         PRM_EN_PRCID_LAST
      }
   },

   /* Resource dev/media */
   { 
      "/dev/usbpower",
      (C_U32_PRC_DEV_USBPWR_BIT),
      {
         PRM_EN_PRCID_USBPWR,
         PRM_EN_PRCID_LAST,
         PRM_EN_PRCID_LAST,
         PRM_EN_PRCID_LAST
      }
   }
   
};

static tBool bPrmActive = FALSE;
static OSAL_tThreadID PrmRecThreadID = OSAL_ERROR;

/************************************************************************ 
|function prototype (scope: module-local) 
|-----------------------------------------------------------------------*/
static void vPrmSignal( tU16 u16Event, tU8 u8MediumType, tU8 au8String[] );
static OSAL_tThreadID CreateUDevConnectionTask( void );
static void PRM_vRecTaskEntry( void );
static trResourceTableEntry *prSearchResByName( tCString coszResourceName );
static void vExcutePrmCallback( OSAL_tProcessID pid, OSAL_tThreadID tid, tU32 u32CallbackParameter,
                                tU32 u32Idx, tU8 u8MediumType, tU8 au8String[] );
static tVoid vSendSignal( tU16 u16Notification, tU16 u16Status, tU8 u8ResType, tU8 au8String[] );
//static tVoid vSendResSignal( tU16 u16Notification, tU16 u16Status, tU32 u32ResMask );
static tU32 u32RegisterForNotification( tU16             u16AppID,
                                        tCString         coszResourceName,
                                        tU16             u16NotificationType,
                                        OSALCALLBACKFUNC pCallback,
                                        tU32             *pu32Data,
                                        tU8              *pu8Status,
                                        tBool            bTwoParameter
                                      );
static tU32 u32UnRegisterForNotification( tU16     u16AppID,
                                          tCString coszResourceName,
                                          tU16     u16NotificationType
                                        );
static tU16 u16EvaluateSdType( tU32 tu32CryptId, tU32 u32PhysId );



/************************************************************************ 
|function implementation (scope: module-global) 
|-----------------------------------------------------------------------*/
//tU32 u32CardDeviceName(tU8 *p8deviceName);
tS32 s32TraceErrmem(tBool bExtended,OSAL_tIODescriptor fd);

/*****************************************************************************/
/* Local functions implementation                                           */
/*****************************************************************************/
sem_t* sem_init_prm = SEM_FAILED;

void vCheckPrm(void)
{
   if(bPrmActive == FALSE)
   {
     sem_init_prm = sem_open(OSAL2_NAME_LOCK,O_EXCL | O_CREAT, OSAL_ACCESS_RIGTHS, 0);
     if (sem_init_prm != SEM_FAILED)
     {
        ChangeSemRights("/dev/shm/sem."OSAL2_NAME_LOCK,"BASE_INIT");
        prm_vInit();
        /* wait for installing PRM tasks */
        OSAL_s32ThreadWait(100);
        bPrmActive = TRUE;
     }
     else
     {
        if (errno != EEXIST)
        {
            vWritePrintfErrmem("BASE_INIT sem_open errno != EEXIST error %d \n",errno);
            NORMAL_M_ASSERT_ALWAYS();
        }
            
        sem_init_prm= sem_open(OSAL2_NAME_LOCK, 0);
        if (sem_init_prm == SEM_FAILED)
        {
            vWritePrintfErrmem("BASE_INIT sem_open error %d : ret val %d \n",errno,sem_init_prm);
            NORMAL_M_ASSERT_ALWAYS();
        }
        else
        {
            /* wait until PRM subsystem is installed */
            u32SemWait(sem_init_prm);
        }
     }
     if(sem_init_prm != SEM_FAILED)
     {
         sem_post(sem_init_prm);
     }
   }
}


/******************************************************************************
 * FUNCTION:      vPrmSignal
 *
 * DESCRIPTION:   sets internal resource specific flag and signals registered applications
 *                
 *
 * PARAMETER:     [i] tU16  Event information
 *                [i] tU8   Media type information (real port)
 *                 
 *               
 * RETURNVALUE:   none
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                              | Authors
 *  04.02.01 |   Initial revision                          | MRK2HI
 *  20.03.13 |   Providing Seperate implementation  for    |
 *           |   Sd_Card and CryptCard                     | SWM2KOR
 *****************************************************************************/
static void vPrmSignal( tU16 u16Event, tU8 u8MediumType, tU8 au8String[] )
{
   tBool bSecondSignal = FALSE;
   tU8   au8Buf[6];
   tU16  tmp;
     
   OSAL_M_INSERT_T8( &au8Buf[0], PRM_CALLBACK_INFO );
   
   switch (u16Event)
   {
      case MEDIUM_EJECTED:
         // this is the DATA MEDIUM ejected part...
         if(u8MediumType == MEDIUM_SD)
         {
            // The status bit for Cryptcard is cleared
            M_CLEAR_BIT( pOsalData->u8DeviceCryptCardStatus, C_U8_MEDIA_READY_BIT );
            M_CLEAR_BIT( pOsalData->u8DeviceCryptCardStatus, C_U8_MEDIA_INSIDE_BIT );
            pOsalData->u16CardMediaType1 = OSAL_C_U16_MEDIA_EJECTED;
            OSAL_M_INSERT_T16( &au8Buf[1], pOsalData->u16CardMediaType1);
         }
         else if(u8MediumType == MEDIUM_DEVMEDIA)
         {
            if( pOsalData->u8DeviceDevMediaCount > 0 )
            {
               pOsalData->u8DeviceDevMediaCount--;
            }
            else
            {
               NORMAL_M_ASSERT_ALWAYS();
            }
            tmp = OSAL_C_U16_MEDIA_EJECTED;
            OSAL_M_INSERT_T16( &au8Buf[1], tmp );  // lint
         }         
         else
         {
            // normalassert?
         }
         
         // send not ready signal first
         vSendSignal( OSAL_C_U16_NOTI_MEDIA_STATE,  OSAL_C_U16_MEDIA_NOT_READY, u8MediumType, au8String );            
         // send eject info
         vSendSignal( OSAL_C_U16_NOTI_MEDIA_CHANGE, OSAL_C_U16_MEDIA_EJECTED,   u8MediumType, au8String );
         break;
         
      case MEDIUM_INSERTED:
         // this is the DATA MEDIUM inserted part...
            // signal media type
         if(u8MediumType == MEDIUM_SD) //Crypt CARD
         {
            // The status bit for Cryptcard is updated
            vSendSignal( OSAL_C_U16_NOTI_MEDIA_CHANGE, pOsalData->u16CardMediaType1, u8MediumType, au8String );
            if(pOsalData->u32PrmAppId == 0xffffffff)
            {
               M_SET_BIT( pOsalData->u8DeviceCryptCardStatus, C_U8_MEDIA_READY_BIT );
            }
            M_SET_BIT( pOsalData->u8DeviceCryptCardStatus, C_U8_MEDIA_INSIDE_BIT );
            OSAL_M_INSERT_T16( &au8Buf[1], pOsalData->u16CardMediaType1 );
            if(pOsalData->u16CardMediaType1 != OSAL_C_U16_UNKNOWN_MEDIA) bSecondSignal = TRUE;
         }
         else if(u8MediumType == MEDIUM_DEVMEDIA)
         {
            if( pOsalData->u8DeviceDevMediaCount <= (PRM_MAX_USB_DEVICES+1) )
            {
               pOsalData->u8DeviceDevMediaCount++;
            }
            else
            {
               NORMAL_M_ASSERT_ALWAYS();
            }
            vSendSignal( OSAL_C_U16_NOTI_MEDIA_CHANGE, OSAL_C_U16_DATA_MEDIA, u8MediumType, au8String );                
            tmp = OSAL_C_U16_DATA_MEDIA;
            OSAL_M_INSERT_T16( &au8Buf[1], tmp );  // lint
            bSecondSignal = TRUE;   // second signal, always. We don't look at the type of medium.
         }
         else
         {
            // normalassert?
         }
        
         if( bSecondSignal )
         {
         
            vSendSignal( OSAL_C_U16_NOTI_MEDIA_STATE, OSAL_C_U16_MEDIA_READY, u8MediumType, au8String );
         }
         break;
         
      default:
         // can't happen
         // u8MediumType = MEDIUM_UNKNOWN;
         break;
   }
   LLD_vTrace( PRM_TRACE_CLASS, TR_LEVEL_DATA, au8Buf, 3);
}


/******************************************************************************
 * FUNCTION:      vCreateUDevConnection
 *
 * DESCRIPTION:   Task for handling IO Event messages for media type recognition
 *                and ffs formatting progress
 *
 * PARAMETER:     none
 *                 
 *               
 * RETURNVALUE:   none
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 *  04.12.01 |   Initial revision                     | MRK2HI
 *****************************************************************************/


   // old messages from udev:
   // UdevMsg : inserted /dev/sda1 available at /dev/media/F438-8D74 with cryptnav
   // UdevMsg : removed /dev/sda1 with cryptnav
   // UdevMsg : removed /dev/sda 

   // UdevMsg : inserted /dev/sda1 available at /dev/media/F438-8D74 
   // UdevMsg : removed /dev/sda1 
   // UdevMsg : removed /dev/sda 
  
  /******************************************************************************
 * FUNCTION:      EvaluateNewMount
 *
 * DESCRIPTION:   This function Evaluates New mount point for cryptnav devices 
                  such as CDROM ,SDCARD,USB etc.  
 *
 * PARAMETER:     1. tBool bMounted
                  2. tBool bCryptDev
 *                3. char *device_name
                  4. char MountPoint[] 
 *               
 * RETURNVALUE:   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 06.06.2012|   New mount point for SD card for CARD | sja3kor 
                 reader. 
 * 09.10.12  |   Updated media types for SD_Card with |SWM2KOR
 *           |   Newly added PrmMsg Stucture Member   | 
 * 15.10.12  |   Provided Fix For MMS NIKAI-147       | sja3kor 
             |   Removed rMsg.u32Val = 0 because it | 
             |   causes USB eject as a SD-CARD eject  |
 * 19.12.14  |  Initialize rMsg                       | vew5kor    
 *****************************************************************************/
void EvaluateNewMount1( tBool bMounted, tBool bCryptDev, char *device_name, char MountPoint[] )
{
   trPrmMsg rMsg = {0};
   char *uuidpos;
   int RetVal;
   tBool bRealSdIf = FALSE;
   static tS32 s32Cryptcard  = -1;

   rMsg.s32ResID    = RES_ID_INVALID;
   rMsg.u32EvSignal = SIGNAL_INVALID;
   rMsg.u32Val      = (tU32)(-1);
   rMsg.au8UUID[0]  = 0;
   
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
   {
      tU8  auBuf[200];
   
      auBuf[0] = C_U8_TRACE_prm_uDev_Msg;
      strncpy((char*)&auBuf[1],(char*)MountPoint, 198 );
      auBuf[199] = 0;
      LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL,auBuf, strlen((char*)auBuf) );
   }

   if( bMounted == TRUE )
   {
      rMsg.u32EvSignal = SIGNAL_MOUNT;
   }
   else
   {
      rMsg.u32EvSignal = SIGNAL_UNMOUNT;
   }

   // analyse the device name. The format must be "sda", "sdb", "sda1", "sdb1", ...
   if(device_name[0] == 's' && device_name[1] == 'd')
   {
      rMsg.s32ResID = RES_ID_DEVMEDIA; 
      rMsg.u32Val = device_name[2] - 'a'; // val = 0,1,2,..
      if(rMsg.u32Val >= PRM_MAX_USB_DEVICES)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
 
   if(!strncmp(device_name,"mmcblk1",6)) /*lint !e530 */
   {
      rMsg.s32ResID = RES_ID_DEVMEDIA; 
      rMsg.u32Val = 8;
      bRealSdIf = TRUE;
   } 

   if( bMounted == TRUE )  // uuid is send with a mount, only!
   {
      // uuid is located behind the /dev/media/ string
      uuidpos = strstr( MountPoint, "/dev/media/" );
      if( uuidpos != NULL )
      {
         // only uuid string -> move pointer behind 3 slashes
         SEARCH_SLASH(uuidpos);
         if( uuidpos != NULL )
         {
            SEARCH_SLASH(uuidpos);
            if( uuidpos != NULL )
            {
               ++uuidpos;
            }
         }
         if( uuidpos != NULL )
         {
            strncpy((char*)rMsg.au8UUID,(char*)uuidpos, PRM_SIZE_OF_UUID_MSG-1 );
            rMsg.au8UUID[PRM_SIZE_OF_UUID_MSG-1] = 0;
          }
         else
         {
            // device without an UUID, this should not happen
            NORMAL_M_ASSERT_ALWAYS();
         }
      }
      else
      {
         // no /dev/media -> wrong udev scripting?
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   
   // cryptcard is overwriting the normal mass storage mount
   if( bCryptDev == TRUE )   // can be true only, if a mount has happend
   {
      if(bRealSdIf == TRUE)
      {
            // cryptcard is overwriting the normal mass storage mount
            s32Cryptcard  = rMsg.u32Val;
            rMsg.s32ResID = RES_ID_SD_CARD;  // overwrite s32ResID with card id, usb? is saved in u32Val
            // create symlink to /
            remove( "/tmp/cryptnav");    
            RetVal = symlink( MountPoint, "/tmp/cryptnav" );                
            if( RetVal == -1 )
            {
               RetVal = errno;
               NORMAL_M_ASSERT_ALWAYS();
            }
      }
      else
      {
         if( rMsg.u32Val < PRM_MAX_USB_DEVICES )   // todo check -1 value (0xff...)
         {
            if( s32Cryptcard == -1 )
            {
               // create symlink to /
               remove( "/tmp/cryptnav");                    
               RetVal = symlink( MountPoint, "/tmp/cryptnav" );
               if( RetVal == -1 )
               {
                  RetVal = errno;
                  NORMAL_M_ASSERT_ALWAYS();
               }
               s32Cryptcard  = rMsg.u32Val;
               rMsg.s32ResID = RES_ID_SD_CARD;// overwrite s32ResID with card id, usb? is saved in u32Val / *krs4cob */
            }
            else
            {
               // we don't support more than two cryptcards
            }
         }
         else
         {
             rMsg.s32ResID = RES_ID_INVALID;
             NORMAL_M_ASSERT_ALWAYS();
         }
      }
   }

   if( bMounted == FALSE ) // check if an cryptcard unmount happened
   {
         if( s32Cryptcard == rMsg.u32Val )
         {
            s32Cryptcard  = -1;
            rMsg.s32ResID = RES_ID_SD_CARD;  // overwrite s32ResID with card id, usb? is saved in u32Val
         }
         else
         {
            rMsg.s32ResID = RES_ID_DEVMEDIA; 
         }
   }

   if(( rMsg.s32ResID != RES_ID_INVALID ) && (rMsg.u32EvSignal != SIGNAL_INVALID))
   {
      (void)OSAL_s32MessageQueuePost( prm_hPrmMQ, (tPCU8)&rMsg, sizeof(trPrmMsg), 0 );
   }
}

void EvaluateNewMount2( tBool bMounted, tBool bCryptDev, char *device_name, char MountPoint[] )
{
   trPrmMsg rMsg = {0};
   int RetVal;
   char* pStr = NULL;
   rMsg.s32ResID    = RES_ID_INVALID;
   rMsg.u32EvSignal = SIGNAL_INVALID;
   rMsg.u32Val      = (tU32)(-1);
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
   {
      tU8  auBuf[200];
      auBuf[0] = C_U8_TRACE_prm_uDev_Msg;
      strncpy( (char*)&auBuf[1],(char*)MountPoint, 198 );
      auBuf[199] = 0;
      LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL,auBuf, strlen((char*)auBuf) );
   }
   if( bMounted == TRUE )
   {
      rMsg.u32EvSignal = SIGNAL_MOUNT;
   }
   else
   {
      rMsg.u32EvSignal = SIGNAL_UNMOUNT;
   }
   /* try to find SD card at real SD card IF */
   pStr = strstr(device_name,"mmcblk0");
   // analyse the device name. The format must be "sda", "sdb", "sda1", "sdb1", ...
   RetVal = strlen(device_name);
   rMsg.s32ResID = RES_ID_DEVMEDIA; 
   if(pStr)
   {
      rMsg.u32Val = PRM_MAX_USB_DEVICES;
   }
   else
   {
#ifdef AM_BIN
      /*  device_name -> /dev/sda1 */
      rMsg.u32Val = device_name[7] - 'a'; // val = 0,1,2,..
#else
      rMsg.u32Val = device_name[RetVal-1] - 'a'; // val = 0,1,2,..
#endif
   }
   if( bMounted == FALSE ) // check if an cryptcard unmount happened
   {
      /* do we have real SD card IF unmount ? */
      if(pStr)
      {
         /* check for cryptcard */
         if(pOsalData->u32CryptcardId  != (tU32)-1)
         {
            rMsg.u32Val = PRM_MAX_USB_DEVICES - 1 ;
         }
      }
   }
   // cryptcard is overwriting the normal mass storage mount
   if( bCryptDev == TRUE )   // can be true only, if a mount has happend
   {
         if( rMsg.u32Val < PRM_MAX_USB_DEVICES )   // todo check -1 value (0xff...)
         {
               // create symlink to /
               remove( "/tmp/cryptnav");
               RetVal = symlink( MountPoint, "/tmp/cryptnav" );
               if( RetVal == -1 )
               {
                  RetVal = errno;
                  NORMAL_M_ASSERT_ALWAYS();
               }
               pOsalData->u32CryptcardId  = rMsg.u32Val;
               rMsg.s32ResID = RES_ID_SD_CARD;// overwrite s32ResID with card id, usb? is saved in u32Val / *krs4cob */
         }
         else
         {
            pStr = strstr(MountPoint,"mmcblk0");
            if(pStr != NULL)
            {
               // create symlink to /
               remove( "/tmp/cryptnav");                    
               RetVal = symlink( MountPoint, "/tmp/cryptnav" );
               if( RetVal == -1 )
               {
                  RetVal = errno;
                  NORMAL_M_ASSERT_ALWAYS();
               }
               rMsg.u32Val = 8; // index for sd card
               pOsalData->u32CryptcardId  = rMsg.u32Val;
               rMsg.s32ResID = RES_ID_SD_CARD;// overwrite s32ResID with card id, usb? is saved in u32Val / *krs4cob */
            }
            else
            {
                rMsg.s32ResID = RES_ID_INVALID;
                NORMAL_M_ASSERT_ALWAYS();
            }
         }
   }
    if( bMounted == TRUE )  // uuid is send with a mount, only!
    {
        //copy the uuid to rMsg.au8UUID whenever any device is mounted
        strncpy((char*)rMsg.au8UUID,(char*)&MountPoint[7], PRM_SIZE_OF_UUID_MSG-1 );
        rMsg.au8UUID[PRM_SIZE_OF_UUID_MSG-1] = 0;
        char szName[256];
        int fd = -1;
        if((fd = open("/dev/media",O_RDWR)) == -1)
        {
           if(mkdir("/dev/media",S_IFDIR | OSAL_ACCESS_RIGTHS) == 0)
           {
              struct group *pGroup = getgrnam(MEDIA_GROUP_NAME);
              if(pGroup==NULL)
              {
                  vWritePrintfErrmem("PRM getgrnam -> error %d \n",errno);
              }
              else
              {
                 if(chown("/dev/media",(uid_t)-1,pGroup->gr_gid) == -1)
                 {
                     vWritePrintfErrmem("PRM -> /dev/media chown error %d \n",errno);
                 }
                 if(chmod("/dev/media", OSAL_MEDIA_ACCESS_RIGTHS) == -1)
                 {
                    vWritePrintfErrmem("PRM -> /dev/media chmod error %d \n",errno);
                 }
              }
           }
        }
        else
        {
            close(fd);
        }
        snprintf(szName,256,"/dev/media/%s",&MountPoint[7]);
        remove(szName);
        RetVal = symlink( MountPoint, szName );
        if( RetVal == -1 )
        {
            RetVal = errno;
            NORMAL_M_ASSERT_ALWAYS();
        }
    }
   if( bMounted == FALSE ) // check if an cryptcard unmount happened
   {
         if( pOsalData->u32CryptcardId == rMsg.u32Val )
         {
            pOsalData->u32CryptcardId  = (tU32)-1;
            rMsg.s32ResID = RES_ID_SD_CARD;  // overwrite s32ResID with card id, usb? is saved in u32Val
         }
         else
         {
            rMsg.s32ResID = RES_ID_DEVMEDIA; 
         }
   }
   if(( rMsg.s32ResID != RES_ID_INVALID ) && (rMsg.u32EvSignal != SIGNAL_INVALID))
   {
      (void)OSAL_s32MessageQueuePost( prm_hPrmMQ, (tPCU8)&rMsg, sizeof(trPrmMsg), 0 );
   }
}

void handle_mount1( char *device_name )
{
   int   device_file, crypt_dir_fd;
   char  fullname[OSAL_C_U32_MAX_PATHLENGTH];
   char  mnt[ 255 ] = {0};
   tBool bCryptdev = FALSE;    

   strcpy(fullname, AUTOMOUNT1"/");
   strncat(fullname, device_name, sizeof(fullname)-sizeof(AUTOMOUNT1"/")-1);

   device_file = open(fullname, O_RDONLY );
   if( FAILED != device_file )
   {
      ssize_t size;

      size = read( device_file, mnt, sizeof(mnt) );
      if(( FAILED != size ) && ( size <= (11 + PRM_SIZE_OF_UUID_MSG) )) // we are supporting uuids with 64 chars + "/dev/media/" string
      {
         strcat( mnt, "/CRYPTNAV/" );  // add the cryptnav dir to the string
         crypt_dir_fd = open( mnt , O_RDONLY);
         mnt[size] = 0;                // remove the cryptnav string
         if( crypt_dir_fd >= 0 )       // cryptnav directory in root filesystem
         {
            bCryptdev = TRUE;
            close( crypt_dir_fd );
         }
         EvaluateNewMount1( TRUE, bCryptdev, device_name, mnt );
      }
      // else USB device could already be removed, possible no real error
      close( device_file );
   }
   // else could already be removed, possible no real error
}


void handle_mount2( char *device_name )
{
   int   device_file, crypt_dir_fd;
   char  fullname[256];
   char  devicename[256];
   char  mnt[ 255 ] = {0};
   char  Content[1024] = {0};
   tBool bCryptdev = FALSE;    
   char  cBuffer[200];
   tBool bSuccess = FALSE;
   int size;
   DIR   *pDevEntry ;
   struct dirent *pPartition = NULL;
   strcpy(devicename, AUTOMOUNT2"/");
   strncat(devicename, device_name, sizeof(fullname)-sizeof(AUTOMOUNT2"/")-1);

   // device_name = device_dev_sda.info
   // search in device_dev_sda/partition_dev_sda1.info

   /* Step 1 find new directory for device */
   { 
         // device_name = device_dev_sda.info
         // search in device_dev_sda/partition_dev_sda1.info
         size = strlen(devicename);
         devicename[size-5] = 0;
      // open /tmp/.automounter/device_db/device_dev_sda
      pDevEntry = opendir(devicename);
      
         if(pDevEntry)
         {
            // read /tmp/.automounter/device_db/device_dev_sda/partition_dev_sda1.info
            /* read partition */
            do
            {
               pPartition = readdir( pDevEntry );
               if( NULL != pPartition )
               {
                  if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
                  {
                     snprintf(cBuffer,200,"PRM readdir entry %s ",pPartition->d_name);
                     TraceString(cBuffer);
                  }
                  if(pPartition->d_name[0] != '.')
                  {
                     if((!memcmp("partition_dev_sd",(char*)pPartition->d_name,strlen("partition_dev_sd")))
                      ||(!memcmp("partition_dev_mmc",(char*)pPartition->d_name,strlen("partition_dev_mmc"))))
                     {
                        bSuccess = TRUE;
                        /* support of first valid partion */
                        break;
                     }
                  }
               }
            }while( NULL != pPartition );
            (void)closedir( pDevEntry );
         }
    
   }

   /* Step 2 find mount point info for device */
   if(bSuccess == TRUE)
   {
      bSuccess = FALSE;
      strcpy(fullname, devicename);
      strcat(fullname,"/");
      if(pPartition)strcat(fullname,(char*)&pPartition->d_name[0]);

      device_file = open(fullname, O_RDONLY );
      if( FAILED != device_file )
      {
        if((size = s32GetFileSize(device_file)) >0)
        {
           size = read(device_file, Content, size);
           if(!size)
           {
                snprintf(cBuffer,200,"PRM search PRM MOUNT_POINT= in %s failed size error",fullname);
                TraceString(cBuffer);
           }
           else
           {
              int i,offset = 0;
              while(1)
              {
                if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
                {
                   snprintf(cBuffer,200,"PRM search PRM MOUNT_POINT= in %s ",fullname);
                   TraceString(cBuffer);
                }
                if(!strncmp("MOUNT_POINT=",Content+offset,12))
                {
                   strncpy(mnt,Content+offset+12,64);
                   for (i = 0; i<64 ;i++)
                   {
                     if((*(mnt+i) == /*EOF*/0xff) || (*(mnt+i) == 0x0a/*EOL*/))break;
                   }
                   mnt[i]= 0;
                   size = strlen(mnt);
                   if(size >= PRM_SIZE_OF_UUID_MSG)
                   {
                      snprintf(cBuffer,200,"PRM path length error %s ",mnt);
                      TraceString(cBuffer);
                   }
                   bSuccess = TRUE;
                   break;
                }
                /* search next line if available*/
                for (i = 0; i<64 ;i++)
                {
                  if((*(Content+offset+i) == /*EOF*/0xff) || (*(Content+offset+i) == 0x0a/*EOL*/)) 
                  {
                     i++;
                     offset += i;
                     break;
                  }
                }
                if(*(Content+offset+i-1) == /*EOF*/0xff)break;
              }
           }
        }
        close(device_file);
      }
      else
      {
         snprintf(cBuffer,200,"PRM open %s failed",fullname);
         TraceString(cBuffer);
      }
   }
 
   /* Step 3 open device root directory */
   if(bSuccess == TRUE)
   {
      size = strlen(mnt);
      device_file = open(mnt, O_RDONLY );
      if( FAILED != device_file )
      {
         strcat( mnt, "/CRYPTNAV/" );  // add the cryptnav dir to the string
         crypt_dir_fd = open( mnt , O_RDONLY);
         mnt[size] = 0;                // remove the cryptnav string
         if( crypt_dir_fd >= 0 )       // cryptnav directory in root filesystem
         {
            bCryptdev = TRUE;
            close( crypt_dir_fd );
         }
         if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
         {
             snprintf(cBuffer,200,"PRM EvaluateNewMount bCryptdev:%d Dev:%s Mnt:%s",bCryptdev,devicename,mnt);
             TraceString(cBuffer);
         }
         EvaluateNewMount2( TRUE, bCryptdev, devicename, mnt );
      }
      else
      {
        // else USB device could already be removed, possible no real error
      }
      close( device_file );
   }
}

void handle_umount( char *device_name )
{
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
   {
       char  cBuffer[200];
       snprintf(cBuffer,200,"PRM handle_umount for %s ",device_name);
       TraceString(cBuffer);
   }
   if(pOsalData->u32AmVersion == 2)
   {
      EvaluateNewMount2( FALSE, FALSE, device_name, "" );
   }
   else
   {
      EvaluateNewMount1( FALSE, FALSE, device_name, "" );
   }
}

int scan_existing_mounts( void )
{
   DIR   *automount = NULL;
   int    ret       = EXIT_FAILURE;

   if(pOsalData->u32AmVersion == 2)
   {
      automount = opendir( AUTOMOUNT2 );
   }
   else
   {
      automount = opendir( AUTOMOUNT1 );
   }

   if( NULL == automount)
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
   else
   {
      struct dirent *device = NULL;

      errno = 0;
      do
      {
         device = readdir( automount );
         if( NULL != device )
         {

            if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
            {
                char cBuffer[200];
               snprintf(cBuffer,200,"PRM scan_existing_mounts %s",device->d_name);
               TraceString(cBuffer);
            }
            if(pOsalData->u32AmVersion == 2)
            {
                handle_mount2( device->d_name );
            }
            else
            {
               handle_mount1( device->d_name );
            }
         }
      }while( NULL != device );
      (void)closedir( automount );
      ret = EXIT_SUCCESS;
   }
   return ret;
}

void watch_mounts( int fd )
{
   char  buf[ sizeof(struct inotify_event) + NAME_MAX ] = {0};

   struct inotify_event *event = (struct inotify_event*)&buf;
   ssize_t               size  = 0;

   while( pOsalData->bPrmRecTaskActiv )
   {
      size = read( fd, event, sizeof( buf ) );
      if(pOsalData->bPrmRecTaskActiv ==FALSE)
      {
         TraceString("Stop Inotify Supervision");
         break;
      }
      if( FAILED != size )
      {
         if( (event->mask & IN_MODIFY) )
         {
            if(pOsalData->u32AmVersion == 2)
            {
               handle_mount2( event->name );
            }
            else
            {
               handle_mount1( event->name );
            }
         }
         else if( (event->mask & IN_DELETE) )
         {
            handle_umount( event->name );
         }
         else
         {
            TraceString("Unhandled Mount Event");
         }
      }
   }
 
   if(pOsalData->u32AmVersion == 2)
   {
      remove(AUTOMOUNT2"/exit.txt");
   }
   else
   {
      remove(AUTOMOUNT1"/exit.txt");
   }
 
}


#ifdef AM_BIN
static int ReqId = -1;
static int bConnectionLost = FALSE;

void vCheckConnetionLost(void)
{
    if(bConnectionLost == TRUE)
    {
//       while(bConnectionLost == TRUE)
       {
           automounter_api_state_t state = automounter_api_get_state();
           error_code_t error = automounter_api_try_connect();
           TraceString("Automounter State %d",state);
           if ((error == RESULT_OK)
            ||(error == RESULT_DAEMON_NOT_RUNNING))
           {
               TraceString("connection_lost reconnected");
               bConnectionLost = FALSE;
           }
           else
           {
               TraceString("connection_lost reconnection failed Error:%d",error);
               OSAL_s32ThreadWait(1000);
           }
       }
    }
}


void establish_connection_success(void)
{
    TraceString("connection_established_success");
    if(automounter_api_get_snapshot(SNAPSHOT_MOUNTED_PARTITIONS_ONLY, &ReqId) != RESULT_OK)
    {
        TraceString("PRM Error automounter_api_get_snapshot -> failed");
        vWritePrintfErrmem("PRM Error automounter_api_get_snapshot -> failed \n");
    }
}
void establish_connection_failure(void)
{
    TraceString("connection_established_failure");
}
void connection_lost()
{
    TraceString("connection_lost");
    bConnectionLost = TRUE;
    vCheckConnetionLost();
}


static void print_part_info(const partition_info_t *part_info,const device_info_t *device_info)
{
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
   {
    TraceString("\tInterface ID: %s",part_info->interface_id);
    TraceString("\tState: %s",automounter_api_get_partition_state_string(part_info->state));
    TraceString("\tIdentifier: %s",part_info->identifier);
    TraceString("\tMountpoint: %s",part_info->mount_point);
    TraceString("\tMount source: %s",part_info->mount_src);
    TraceString("\tFilesystem: %s",part_info->mount_fs);
    TraceString("\tMounted writable: %d",part_info->mounted_writable);
    TraceString("\tPartition number: %d",part_info->partition_no);
    if (part_info->state==PARTITION_UNSUPPORTED)
    {
        TraceString("\tUnsupported reason: %s",automounter_api_get_partition_unsupported_reason_string(part_info->unsupported_reason));
    }
    if (part_info->error != RESULT_OK)
    {
        TraceString("\tError Code: %d",part_info->error);
    }
    TraceString("\tParent device: %s",device_info->interface_id);
    TraceString("\t\tState: %s",automounter_api_get_device_state_string(device_info->state));
    TraceString("\t\tIdentifier: %s",device_info->identifier);
    TraceString("\t\tDetected partitions: %d",device_info->detected_partition_cnt);
    TraceString("\t\tDevice handler ID: %s",device_info->device_handler_id);
    TraceString("\t\tDevice type: %s",device_info->device_type);
    TraceString("---------------------------------------------------------------------------------");
   }
}


void device_detected(const device_info_t *device_info)
{
    partition_info_t part_info;
    memset(&part_info,0,sizeof(part_info));
    print_part_info(&part_info,device_info);
}
void device_nomedia(const device_info_t *device_info)
{
    partition_info_t part_info;
    memset(&part_info,0,sizeof(part_info));
    print_part_info(&part_info,device_info);
}
void device_automounting(const device_info_t *device_info)
{
    partition_info_t part_info;
    memset(&part_info,0,sizeof(part_info));
    print_part_info(&part_info,device_info);
}
void device_automounted(const device_info_t *device_info)
{
    partition_info_t part_info;
    memset(&part_info,0,sizeof(part_info));
    print_part_info(&part_info,device_info);
}
void device_unmounting(const device_info_t *device_info)
{
    partition_info_t part_info;
    memset(&part_info,0,sizeof(part_info));
    print_part_info(&part_info,device_info);
}
void device_unmounted(const device_info_t *device_info)
{
    partition_info_t part_info;
    memset(&part_info,0,sizeof(part_info));
    print_part_info(&part_info,device_info);
}
void device_invalid(const device_info_t *device_info)
{
    partition_info_t part_info;
    memset(&part_info,0,sizeof(part_info));
    print_part_info(&part_info,device_info);
}

#define MAX_CONNECTED_DEV  4

typedef struct{
    char sPartIdentifier[64];
    tU32 u32Status;
}trDetectedPartition;

enum {
EN_NODEVICE_DETECTED = 0,
EN_DEVICE_DETECTED,
EN_PARTITION_MOUNTED,
EN_PARTITION_UNMOUNTED,
EN_PARTITION_REMOUNT
};

static trDetectedPartition   rDetectedPartition[MAX_CONNECTED_DEV];

tS32 s32StoreNewPartition(const partition_info_t *partition_info)
{
   tS32 s32Ret = -1;
   int i;
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
   {
      TraceString("bStoreNewPartition %s",partition_info->identifier);
   }

   for(i=0;i<MAX_CONNECTED_DEV;i++)
   {
       if(rDetectedPartition[i].sPartIdentifier[0] == 0)
       {
          if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
          {
             TraceString("bStoreNewPartition %s on position %d",partition_info->identifier,i);
          }
          strncpy(rDetectedPartition[i].sPartIdentifier,partition_info->identifier,strlen(partition_info->identifier));
          s32Ret = i;
          break;
       }
   }
   return s32Ret;
}

tS32 s32FindPartition(const char *partition_name)
{
   tS32 s32Ret = -1;
   int i;
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
   {
      TraceString("bFindPartition %s",partition_name);
   }
   if(partition_name)
   {
     for(i=0;i<MAX_CONNECTED_DEV;i++)
     {
        if(rDetectedPartition[i].sPartIdentifier[0] != 0)
        {
           if(!strncmp(rDetectedPartition[i].sPartIdentifier,partition_name,strlen(partition_name)))
           {
              if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
              {
                 TraceString("bFindPartition %s on position %d",partition_name,i);
              }
              s32Ret = i;
              break;
           }
        }
     }
   }
   return s32Ret;
}

void partition_detected(const partition_info_t *partition_info, const device_info_t *device_info)
{
    int i;
    if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
    {
       TraceString("partition_detected %s",partition_info->identifier);
    }
    
    i = s32FindPartition(partition_info->identifier);
    if(i == -1)
    {
       i = s32StoreNewPartition(partition_info);
       if(i == -1)
       {
         TraceString("cannot store partition %s ",partition_info->identifier);
       }
       else
       {
         rDetectedPartition[i].u32Status = EN_DEVICE_DETECTED;
       }
    }
    print_part_info(partition_info,device_info);
}

void partition_unsupported(const partition_info_t *partition_info, const device_info_t *device_info)
{
    if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
    {
       TraceString("partition_mounted %s",partition_info->identifier);
    }
    print_part_info(partition_info,device_info);
}
void partition_mounting(const partition_info_t *partition_info, const device_info_t *device_info)
{
    if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
    {
       TraceString("partition_mounted %s",partition_info->identifier);
    }
    print_part_info(partition_info,device_info);
}


void handle_mount(const partition_info_t *partition_info, const device_info_t *device_info)
{
    char  mnt[ 255 ] = {0};
    int device_file = -1;
    int crypt_dir_fd = -1;
    int size;
    tBool bCryptdev = FALSE;
    char cBuffer[200];
    ((void)device_info);
    size = strlen(partition_info->mount_point);
    strcpy(mnt,partition_info->mount_point);
    device_file = open(mnt, O_RDONLY );
    if( FAILED != device_file )
    {
      strcat( mnt, "/CRYPTNAV/" );  // add the cryptnav dir to the string
      crypt_dir_fd = open( mnt , O_RDONLY);
      mnt[size] = 0;                // remove the cryptnav string
      if( crypt_dir_fd >= 0 )       // cryptnav directory in root filesystem
      {
         bCryptdev = TRUE;
         close( crypt_dir_fd );
      }
      if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
      {
          snprintf(cBuffer,200,"PRM EvaluateNewMount bCryptdev:%d Dev:%s Mnt:%s",bCryptdev,partition_info->interface_id,mnt);
          TraceString(cBuffer);
      }
      close(device_file);
      EvaluateNewMount2( TRUE, bCryptdev,(char*)partition_info->interface_id, mnt );
    }
}


void partition_mounted(const partition_info_t *partition_info, const device_info_t *device_info)
{
    int i;
    ((void)device_info);
    if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_4 ))
    {
       TraceString("partition_mounted %s",partition_info->identifier);
    }
    i = s32FindPartition(partition_info->identifier);
    if(rDetectedPartition[i].u32Status != EN_PARTITION_MOUNTED)
    {
        if(rDetectedPartition[i].u32Status == EN_PARTITION_REMOUNT)
        {
          TraceString("partition_mounted %s remounting detected",partition_info->identifier);
        }
        else
        {
           handle_mount(partition_info,device_info);
        }
        rDetectedPartition[i].u32Status = EN_PARTITION_MOUNTED;
    }
    print_part_info(partition_info,device_info);
}

void partition_mount_err(const partition_info_t *partition_info, const device_info_t *device_info)
{
    print_part_info(partition_info,device_info);
}
void partition_remounting(const partition_info_t *partition_info, const device_info_t *device_info)
{
    int i;
    i = s32FindPartition(partition_info->identifier);
    if(i != -1)
    {
       rDetectedPartition[i].u32Status = EN_PARTITION_REMOUNT;
    }
    TraceString("partition_remounting %s",partition_info->identifier);
    print_part_info(partition_info,device_info);
}
void partition_unmounting(const partition_info_t *partition_info, const device_info_t *device_info)
{
    TraceString("partition_unmounting %s",partition_info->identifier);
    print_part_info(partition_info,device_info);
}
void partition_unmounted(const partition_info_t *partition_info, const device_info_t *device_info)
{
    int i;
    TraceString("partition_unmounted %s",partition_info->identifier);
    print_part_info(partition_info,device_info);
    EvaluateNewMount2( FALSE, FALSE,(char*)partition_info->interface_id, "" );
    i = s32FindPartition(partition_info->identifier);
    if(i != -1)
    {
       /* clean entry */
       rDetectedPartition[i].u32Status = EN_PARTITION_UNMOUNTED;
       memset(rDetectedPartition[i].sPartIdentifier,0,64);
    }
}
void partition_invalid(const partition_info_t *partition_info, const device_info_t *device_info)
{
    int i;
    TraceString("partition_invalid %s",partition_info->identifier);
    print_part_info(partition_info,device_info);

    EvaluateNewMount2( FALSE, FALSE,(char*)partition_info->interface_id, "" );
    i = s32FindPartition(partition_info->identifier);
    if(i != -1)
    {
       /* clean entry */
       rDetectedPartition[i].u32Status = EN_PARTITION_UNMOUNTED;
       memset(rDetectedPartition[i].sPartIdentifier,0,64);
    }
}
void update_device_info(const device_info_t *device_info, int request_id)
{
   ((void)request_id);
   ((void)device_info);
    TraceString("update_device_info");
}
void update_partition_info(const partition_info_t *partition_info,const device_info_t *device_info, int request_id)
{
   ((void)request_id);
   handle_mount(partition_info,device_info);
}

void snapshot_complete(int request_id)
{
   ((void)request_id);
    TraceString("snapshot_complete");
}

static automounter_api_callbacks_t  prm_rFunction =
{
     .on_establish_connection_success = &establish_connection_success,
     .on_establish_connection_failure = &establish_connection_failure,
     .on_connection_lost              = &connection_lost,
     .on_device_detected              = &device_detected,
     .on_device_nomedia               = &device_nomedia,
     .on_device_automounting          = &device_automounting,
     .on_device_automounted           = &device_automounted,
     .on_device_unmounting            = &device_unmounting,
     .on_device_unmounted             = &device_unmounted,
     .on_device_invalid               = &device_invalid,
     .on_partition_detected           = &partition_detected,
     .on_partition_unsupported        = &partition_unsupported,
     .on_partition_mounting           = &partition_mounting,
     .on_partition_mounted            = &partition_mounted,
     .on_partition_mount_err          = &partition_mount_err,
     .on_partition_remounting         = &partition_remounting,
     .on_partition_unmounting         = &partition_unmounting,
     .on_partition_unmounted          = &partition_unmounted,
     .on_partition_invalid            = &partition_invalid,
     .on_update_device_info           = &update_device_info,
     .on_update_partition_info        = &update_partition_info,
     .on_snapshot_complete            = &snapshot_complete,
};

static void vPrmAmHandleConnection(void)
{
     int           am_fd   = automounter_api_get_pollfd();
     struct pollfd poll_fd = {.fd = am_fd, .events = POLLIN,.revents = POLLOUT};
     if (-1 != am_fd)
     {
          while (TRUE)
          {
               int rc = poll(&poll_fd, 1, -1);
               if (rc > 0)
               {
                    if (poll_fd.revents == POLLIN)
                    {
                         automounter_api_dispatch_event();
                    }
                    else
                    {
                         TraceString("automounter: unexpected event");
                    }
               }
               else
               {
                    if (EINTR != errno)
                    {
                         TraceString("automounter: poll failed");
                    }
               }
          }
     }
     else
     {
          vWritePrintfErrmem("PRM Error automounter_api_get_pollfd -> %d \n",
                             am_fd);
     }
}

static void vPrmAmConnectionTask( int status )
{
     ((void)status);

     error_code_t error = automounter_api_init("OSAL_PRM", LOGGER_LEVEL_ERROR,
                                               (bool)true);

     TraceString(automounter_api_get_version_string());

     if (RESULT_OK != error)
     {
          vWritePrintfErrmem("PRM Error automounter_api_init -> %d \n", error);
     }
     else
     {
          automounter_api_register_callbacks(&prm_rFunction);
          error = automounter_api_try_connect();
          if ((error == RESULT_OK)
              ||
              (error == RESULT_DAEMON_NOT_RUNNING))
          {
               vPrmAmHandleConnection();
               automounter_api_disconnect();

          }
          else
          {
               vWritePrintfErrmem("PRM Error automounter_api_try_connect -> %d \n",
                                  error);
          }
          automounter_api_deinit();
     }
     TraceString("vPrmAmConnectionTask end");
}
#else
static void vPrmINotifyConnectionTask( int status )
{
   int fd  = inotify_init();
   int ret = EXIT_FAILURE;
   int wd = 0;
   ((tVoid)status); // satisfy lint

   if(pOsalData->u32AmVersion != 2)
   {
      // Returnvalue not checked, because mkdir might fail, if udev already created the directory
      mkdir( AUTOMOUNT1, OSAL_FILE_ACCESS_RIGTHS );
   }
   
   if( FAILED == fd )
   {
      TraceString("Failed to initialise");
     // perror("Failed to initialise");
      NORMAL_M_ASSERT_ALWAYS();
   }
   else
   {
      if(pOsalData->u32AmVersion == 2)
      {
         mkdir("/tmp/trace", OSAL_FILE_ACCESS_RIGTHS );
         wd = inotify_add_watch( fd, AUTOMOUNT2, IN_DELETE | IN_MODIFY );
      }
      else
      {
         wd = inotify_add_watch( fd, AUTOMOUNT1, IN_DELETE | IN_MODIFY );
      }
      if( FAILED == wd )
      {
         TraceString("Failed to initialise watch on");
        // perror("Failed to initialise watch on " AUTOMOUNT2);
         NORMAL_M_ASSERT_ALWAYS();
      }
      else
      {
         ret = scan_existing_mounts();
         if( EXIT_SUCCESS == ret )
         {
            watch_mounts( fd );
         }
      }
   }
   TraceString("PRM vPrmINotifyConnectionTask ends");
}
#endif


static OSAL_tThreadID CreateUDevConnectionTask( void )
{
   OSAL_trThreadAttribute  attr;
   OSAL_tThreadID ThId;
   
   /* start PRM task */
   attr.u32Priority  = 37; // todo use define from header x
   attr.s32StackSize = minStackSize;
#ifdef AM_BIN
   attr.szName = "PRM_AM_CONNECTION";
   attr.pfEntry = (OSAL_tpfThreadEntry)vPrmAmConnectionTask;
#else
   attr.szName = "PRM_UDEV_CONNECTION";
   attr.pfEntry = (OSAL_tpfThreadEntry)vPrmINotifyConnectionTask;
#endif
   attr.pvArg   = NULL;
   ThId = OSAL_ThreadSpawn( &attr );
   if( ThId == OSAL_ERROR )
   {
      FATAL_M_ASSERT_ALWAYS();
   }
   return( ThId );
} 


void vExecuteSystemStatInfo(tU32 u32Param)
{
   tOsalMbxMsg rMsg;
   OSAL_tMQueueHandle Mq = 0;
   tS32 i = 0;

   for(i=0;i<5;i++)
   {
      if((pOsalData->rPrmSysStatus[i].pNotifyCallback))
      {
         if(pOsalData->rPrmSysStatus[i].s32NotifyPrcId != OSAL_ProcessWhoAmI()) // execute callback of other process
         {
            // send callback to main callback handler message queue for dispatching
            rMsg.rPrmMsg.u32Pid     = pOsalData->rPrmSysStatus[i].s32NotifyPrcId;
            rMsg.rPrmMsg.u32CallFun = (uintptr_t)pOsalData->rPrmSysStatus[i].pNotifyCallback;
            rMsg.rPrmMsg.u32Param1  = u32Param;
            rMsg.rPrmMsg.u32Param2  = PRM_SYSSTAT_NOTIFY;
            rMsg.rPrmMsg.rHead.Cmd  = MBX_CB_PRM;
            rMsg.rPrmMsg.au8UUID[0] = 0;
            Mq = GetPrcLocalMsgQHandle(pOsalData->rPrmSysStatus[i].s32NotifyPrcId);
            if(Mq != 0)
            {
               if( OSAL_s32MessageQueuePost(Mq, (tPCU8)&rMsg, sizeof(tOsalMbxMsg), 0 ) == OSAL_ERROR )
               {
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
         }
         else
         {
             pOsalData->rPrmSysStatus[i].pNotifyCallback( &u32Param );
         }
      }
   }
}

void vExecuteSystemInfo(tU32 u32Param,tU32 u32Val)
{
   tOsalMbxMsg rMsg = {{0}};
   OSAL_tMQueueHandle Mq = 0;

   if(u32Val != 0)
   {
       vExecuteSystemStatInfo(u32Param);
   }
   else
   {
      if(pOsalData->pSysNotifyCallback)
      {
      if(pOsalData->s32SysNotifyPrcId != OSAL_ProcessWhoAmI()) // execute callback of other process
      {
           // send callback to main callback handler message queue for dispatching
           rMsg.rPrmMsg.u32Pid     = pOsalData->s32SysNotifyPrcId;
           rMsg.rPrmMsg.u32CallFun = (uintptr_t)pOsalData->pSysNotifyCallback;
           rMsg.rPrmMsg.u32Param1  = u32Param;
           rMsg.rPrmMsg.u32Param2  = PRM_SYS_NOTIFY;
           rMsg.rPrmMsg.rHead.Cmd  = MBX_CB_PRM;
           rMsg.rPrmMsg.au8UUID[0] = 0;
           Mq = GetPrcLocalMsgQHandle(pOsalData->s32SysNotifyPrcId);
         }
         else
         {
            pOsalData->pSysNotifyCallback( &u32Param );
         }
         if(Mq != 0)
         {
            if( OSAL_s32MessageQueuePost( Mq, (tPCU8)&rMsg, sizeof(tOsalMbxMsg), 0 ) == OSAL_ERROR )
            {
               NORMAL_M_ASSERT_ALWAYS();
            }
         }
      }
   }
}


void vPostSdCardTrigger(void)
{
   trPrmMsg rMsg;
   OSAL_tMQueueHandle hPrm;
   rMsg.s32ResID    = RES_ID_SD_CARD;
   rMsg.u32EvSignal = SIGNAL_SD_ACTIVATE;
   rMsg.u32Val      = (tU32)(-1);

   if( OSAL_s32MessageQueueOpen( LINUX_PRM_REC_MQ, OSAL_EN_READWRITE, &hPrm ) != OSAL_ERROR )
   {
      if(OSAL_s32MessageQueuePost(hPrm,(tPCU8)&rMsg,sizeof(rMsg),0)  == OSAL_ERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      OSAL_s32MessageQueueClose(hPrm );
   }
}


tU32 u32PostRemountTrigger(const char* Path,const char* cOption)
{
   trPrmMsg rMsg;
   OSAL_tMQueueHandle hPrm;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   OSAL_tEventHandle hEvent;
   OSAL_tEventMask u32EventResultMask = 0;
   memset(&rMsg,0,sizeof(trPrmMsg));
   rMsg.s32ResID    = RES_ID_DEVMEDIA;
   rMsg.u32EvSignal = SIGNAL_REMOUNT;
   rMsg.u32Val      = (tU32)(-1);

    if(OSAL_s32EventCreate("Remount_Event",&hEvent) == OSAL_ERROR)
   {
       TraceString("PRM OSAL_s32EventCreate Remount_Event failed OSAL_E_BUSY");
       u32ErrorCode = OSAL_E_BUSY;
   }
   else
   {
      if(strlen(Path) > PRM_SIZE_OF_UUID_MSG)
      {
          TraceString("PRM Len of Path %s too large",strlen(Path));
          u32ErrorCode = OSAL_E_NAMETOOLONG;
          OSAL_s32EventClose(hEvent);
          OSAL_s32EventDelete("Remount_Event");
     }
      else
      {
         strncpy((char*)rMsg.au8UUID,Path,strlen(Path));
         strncpy((char*)rMsg.au8CertPath,cOption,strlen(cOption));
         if( OSAL_s32MessageQueueOpen( LINUX_PRM_REC_MQ, OSAL_EN_READWRITE, &hPrm ) != OSAL_ERROR )
         {
           if(OSAL_s32MessageQueuePost(hPrm,(tPCU8)&rMsg,sizeof(rMsg),0)  == OSAL_ERROR)
           {
              NORMAL_M_ASSERT_ALWAYS();
           }
           OSAL_s32MessageQueueClose(hPrm );
         }
         OSAL_s32EventWait(hEvent,0x111,OSAL_EN_EVENTMASK_OR,10000,&u32EventResultMask);
         OSAL_s32EventClose(hEvent);
         OSAL_s32EventDelete("Remount_Event");
         if(u32EventResultMask & 0x1)
         {
             u32ErrorCode = OSAL_E_NOERROR;
         }
         else
         {
             TraceString("PRM wrong u32EventResultMask");
             u32ErrorCode = OSAL_E_UNKNOWN;
         }
      }
   }
   return u32ErrorCode;
}

/* This function will create the event Find partition and post the Prm message with the requested path*/

tU32 u32FindFolderpartitioninfo(tCString Path)
{
    trPrmMsg rMsg;
    OSAL_tMQueueHandle hEvent;
    OSAL_tMQueueHandle hPrm;
    tU32 u32ErrorCode = OSAL_E_NOERROR;
    OSAL_tEventMask u32EventResultMask = 0;
    memset(&rMsg,0,sizeof(trPrmMsg));
    rMsg.s32ResID    = RES_ID_DEVMEDIA;
    rMsg.u32EvSignal = SIGNAL_PARTINFO;
    rMsg.u32Val      = (tU32)(-1);
    
    if(OSAL_s32EventCreate("Find_Partition",&hEvent) == OSAL_ERROR)
    {
        TraceString("PRM OSAL_s32EventCreate Find_Partition failed OSAL_E_BUSY");
        u32ErrorCode = OSAL_E_BUSY;
    }
    else
    {
        if(strlen(Path) > PRM_SIZE_OF_UUID_MSG)
        {
            TraceString("PRM Length of Path %s too large\n",strlen(Path));
            u32ErrorCode = OSAL_E_NAMETOOLONG;
            OSAL_s32EventClose(hEvent);
            OSAL_s32EventDelete("Find_Partition");
        }
        else
        {
            strncpy((char*)rMsg.au8UUID,Path,strlen(Path));
            if( OSAL_s32MessageQueueOpen( LINUX_PRM_REC_MQ, OSAL_EN_READWRITE, &hPrm ) != OSAL_ERROR )
            {
                if(OSAL_s32MessageQueuePost(hPrm,(tPCU8)&rMsg,sizeof(rMsg),0)  == OSAL_ERROR)
                {
                    NORMAL_M_ASSERT_ALWAYS();
                }
                OSAL_s32MessageQueueClose(hPrm );
            }
            OSAL_s32EventWait(hEvent,0x111,OSAL_EN_EVENTMASK_OR,10000,&u32EventResultMask);
            OSAL_s32EventClose(hEvent);
            OSAL_s32EventDelete("Find_Partition");
            if(u32EventResultMask & 0x1)
            {
                u32ErrorCode = OSAL_E_NOERROR;
            }
            else
            {
                TraceString("PRM wrong u32EventResultMask");
                u32ErrorCode = OSAL_E_UNKNOWN;
            }
        }
    }

    return u32ErrorCode;
}


tU32 u32PostUsbPortTrigger(tS32 s32Port)
{
   trPrmMsg rMsg;
   OSAL_tMQueueHandle hPrm;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   OSAL_tEventHandle hEvent;
   OSAL_tEventMask u32EventResultMask = 0;
   memset(&rMsg,0,sizeof(trPrmMsg));
   rMsg.s32ResID  = RES_ID_USB_PORT_STATE;
   rMsg.u32Val    = s32Port;
   snprintf((char*)rMsg.au8UUID,OSAL_C_U32_MAX_NAMELENGTH,"USB_PortTriggerEvent_%d", OSAL_ThreadWhoAmI());
   
   if(OSAL_s32EventCreate((char*)rMsg.au8UUID,&hEvent) == OSAL_ERROR)
   {
       TraceString("PRM OSAL_s32EventCreate %s failed OSAL_E_BUSY",rMsg.au8UUID);
       u32ErrorCode = OSAL_E_BUSY;
   }
   else
   {
      if( OSAL_s32MessageQueueOpen( LINUX_PRM_REC_MQ, OSAL_EN_READWRITE, &hPrm ) != OSAL_ERROR )
      {
         if(OSAL_s32MessageQueuePost(hPrm,(tPCU8)&rMsg,sizeof(rMsg),0)  == OSAL_ERROR)
         {
            NORMAL_M_ASSERT_ALWAYS();
         }
         OSAL_s32MessageQueueClose(hPrm );
      }
      OSAL_s32EventWait(hEvent,0x111,OSAL_EN_EVENTMASK_OR,3000,&u32EventResultMask);
      OSAL_s32EventClose(hEvent);
      OSAL_s32EventDelete((char*)rMsg.au8UUID);
      if(u32EventResultMask & 0x1)
      {
         u32ErrorCode = OSAL_E_NOERROR;
      }
      else
      {
         TraceString("PRM wrong u32EventResultMask");
         u32ErrorCode = OSAL_E_UNKNOWN;
      }
   }
   return u32ErrorCode;
}


tU32 u32PostUsbPowerTrigger(usb_port_control* pDat)
{
   trPrmMsg rMsg;
   OSAL_tMQueueHandle hPrm;
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   OSAL_tEventHandle hEvent;
   OSAL_tEventMask u32EventResultMask = 0;
   snprintf((char*)rMsg.au8UUID,OSAL_C_U32_MAX_NAMELENGTH,"USB_PowerTriggerEvent_%d", OSAL_ThreadWhoAmI());
   memset(&rMsg,0,sizeof(trPrmMsg));
   rMsg.s32ResID    = RES_ID_USB_PORT_POWER;
   memcpy(&rMsg.u32Val,pDat,sizeof(usb_port_control)); /*lint !e419 */

   if(OSAL_s32EventCreate((char*)rMsg.au8UUID,&hEvent) == OSAL_ERROR)
   {
       TraceString("PRM OSAL_s32EventCreate %s failed OSAL_E_BUSY",rMsg.au8UUID);
       u32ErrorCode = OSAL_E_BUSY;
   }
   else
   {
      if( OSAL_s32MessageQueueOpen( LINUX_PRM_REC_MQ, OSAL_EN_READWRITE, &hPrm ) != OSAL_ERROR )
      {
         if(OSAL_s32MessageQueuePost(hPrm,(tPCU8)&rMsg,sizeof(rMsg),0)  == OSAL_ERROR)
         {
            NORMAL_M_ASSERT_ALWAYS();
         }
         OSAL_s32MessageQueueClose(hPrm );
      }
      OSAL_s32EventWait(hEvent,0x111,OSAL_EN_EVENTMASK_OR,3000,&u32EventResultMask);
      OSAL_s32EventClose(hEvent);
      OSAL_s32EventDelete((char*)rMsg.au8UUID);
      if(u32EventResultMask & 0x1)
      {
         u32ErrorCode = OSAL_E_NOERROR;
      }
      else
      {
         TraceString("PRM wrong u32EventResultMask");
         u32ErrorCode = OSAL_E_UNKNOWN;
      }
   }
   return u32ErrorCode;
}

/******************************************************************************
 * FUNCTION:      vRecTaskEntry
 *
 * DESCRIPTION:   Task for handling IO Event messages for media type recognition
 *                and ffs formatting progress
 *
 * PARAMETER:     none
 *                 
 *               
 * RETURNVALUE:   none
 *
 *   none
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 *  04.12.01 |   Initial revision                     | MRK2HI
 *  26.09.12 |   Feature PRM_LIBUSB_CONNECTION defined| sja3kor
             |   for OSAL_GEN2 section in case of     | 
             |   prm_low_voltage.                     | 
 *  20.05.14 |Changes made to send UUID to            |
 *           |applications for SD_CARD mount & unmount|pmh5kor
 * 19.12.14  | Initialize rMsg                        | vew5kor
 *****************************************************************************/
static void PRM_vRecTaskEntry( void )
{
   tU8  auBuf[10];
   trPrmMsg rMsg = {0};
   tU32 u32Signal = MEDIUM_UNKNOWN;
   tU16 u16Medium = OSAL_C_U16_UNKNOWN_MEDIA;
   OSAL_tThreadID PrmUdevThreadID;
   tBool bSecondStep = FALSE;
   char Buffer[200];
#ifdef PRM_LIBUSB_CONNECTION
   OSAL_tThreadID PrmLibUsbThreadID;
#endif

#ifdef AM_BIN
   memset(rDetectedPartition,0,sizeof(trDetectedPartition));
#endif
   if( OSAL_s32MessageQueueCreate( LINUX_PRM_REC_MQ,
                                   20,
                                   sizeof(trPrmMsg),
                                   OSAL_EN_READWRITE,
                                   &prm_hPrmMQ ) == OSAL_ERROR )
   {
       if(OSAL_u32ErrorCode() == OSAL_E_ALREADYEXISTS)
       {
          if( OSAL_s32MessageQueueOpen( LINUX_PRM_REC_MQ, OSAL_EN_READWRITE, &prm_hPrmMQ ) == OSAL_ERROR )
          {
              FATAL_M_ASSERT_ALWAYS();
          }
       }
       else
       {
         FATAL_M_ASSERT_ALWAYS();
       }
   }
   if(prm_hPrmMQ != 0)
   {
#if !defined (OSAL_GEN3_SIM) && defined PRM_LIBUSB_CONNECTION
      if(InitUsbForPrm() < 0)
      {
         TraceString("PRM Init USB for Power failed %d",errno);
         NORMAL_M_ASSERT_ALWAYS();
      }
      else
      {
          prm_InitLibUsbConnectionData();
          PrmLibUsbThreadID = prm_CreateLibUsbConnectionTask();
      }
#endif
      PrmUdevThreadID = CreateUDevConnectionTask();


      RegisterForMedia();

      /* start processing PRM messages */
      while( pOsalData->bPrmRecTaskActiv )
      {
         if( OSAL_s32MessageQueueWait( prm_hPrmMQ, (unsigned char*)&rMsg, sizeof(trPrmMsg), 0, OSAL_C_TIMEOUT_FOREVER ) == 0 )
         {
            // = OSAL_u32ErrorCode();
            NORMAL_M_ASSERT_ALWAYS();
         }
         else
         {
            if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
            {
               auBuf[0] = PRM_TASK_START;
               LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, auBuf, 1 );
            }
            // assume we have a usb message, if not, u32Signel and u16Medium is not evaluated
            // Initialise the media type as data media type in the OSAL data structure       
            bSecondStep = TRUE;
            if( rMsg.u32EvSignal == SIGNAL_SYSINFO )
            {
               vExecuteSystemInfo( rMsg.u32Val,0 );
               bSecondStep = FALSE;
            }
            else if( rMsg.u32EvSignal == SIGNAL_SYS_STATINFO )
            {
               vExecuteSystemInfo( rMsg.u32Val,1 );
               bSecondStep = FALSE;
            }
            else if( rMsg.u32EvSignal == SIGNAL_REMOUNT )
            {
               OSAL_tEventHandle hEvent;
               TraceString("PRM SIGNAL_REMOUNT %s %s",rMsg.au8UUID,rMsg.au8CertPath);
               if(OSAL_s32EventOpen("Remount_Event",&hEvent) != OSAL_ERROR)
               {
                 if(bRemount((char*)rMsg.au8UUID,(char*)rMsg.au8CertPath))
                 {
                    TraceString("PRM REMOUNT succeeded");
                    OSAL_s32EventPost(hEvent,0x1,OSAL_EN_EVENTMASK_OR);
                 }
                 else
                 {
                    TraceString("PRM REMOUNT failed");
                    OSAL_s32EventPost(hEvent,0x10,OSAL_EN_EVENTMASK_OR);
                 }
                 OSAL_s32EventClose(hEvent);
               }
               else
               {
                    TraceString("PRM Open REMOUNT Event failed");
               }
               bSecondStep = FALSE;
            }
            else if( rMsg.u32EvSignal == SIGNAL_MOUNT )
            {
               u32Signal = MEDIUM_INSERTED;
               u16Medium = OSAL_C_U16_DATA_MEDIA;
            }
            else if( rMsg.u32EvSignal == SIGNAL_PARTINFO)
            {
                OSAL_tEventHandle hEvent;
                Part_Value = "NONE";
                tCString getValue;
                TraceString("PRM SIGNAL_PARTITION %s \n",rMsg.au8UUID);
                if(OSAL_s32EventOpen("Find_Partition",&hEvent) != OSAL_ERROR)
                {
                    if( "INVALID" != (getValue = bGetPartitionInfo((char*)rMsg.au8UUID)))
                    {
                        Part_Value = getValue;
                        TraceString("PRM Find_Partition Info succeeded \n");
                        OSAL_s32EventPost(hEvent,0x1,OSAL_EN_EVENTMASK_OR);
                    }
                    else
                    {
                        TraceString("PRM Find_Partition info failed");
                        OSAL_s32EventPost(hEvent,0x10,OSAL_EN_EVENTMASK_OR);
                    }
                    OSAL_s32EventClose(hEvent);
                }
                else
                {
                    TraceString("PRM Find_Partition Event failed");
                }
                bSecondStep = FALSE;
            }
            else
            {
               u32Signal = MEDIUM_EJECTED;
               u16Medium = OSAL_C_U16_MEDIA_EJECTED;
            }

            if(bSecondStep) // check range of supported devices 0: sda, 1: sdb, ...
            {
               if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
               {
                  TraceString("ResID:%d Status:%d u32Val:%d UUID:%s",rMsg.s32ResID,rMsg.u32EvSignal,rMsg.u32Val,rMsg.au8UUID);
               }
               switch( rMsg.s32ResID )
               {
                  case RES_ID_DEVMEDIA:
                       if( rMsg.u32EvSignal == SIGNAL_MOUNT )
                       {
                          if(rMsg.u32Val < PRM_MAX_USB_DEVICES ) // check range of supported devices 0: sda, 1: sdb, ...
                          {
                             if( rMsg.au8UUID[0] != 0 )
                             {
                                   pOsalData->u16UsbMediaType[rMsg.u32Val] = u16Medium; // todo still neccessary?
                                   strncpy((char*)&((*pOsalData).u8DeviceUUID[ rMsg.u32Val ][0]),(char*)rMsg.au8UUID, sizeof(rMsg.au8UUID) - 1);
                                   vPrmSignal( u32Signal, MEDIUM_DEVMEDIA, rMsg.au8UUID );
                             }
                             else  // devmedia without a uuid?
                             {
                                   vWritePrintfErrmem("mount of devmedia Res:%d Val:%d without a UUID \n",rMsg.s32ResID,rMsg.u32Val);
                             }
                          }  // no unmount with uuid possible, only a device message is received for an unmounted
                          else  // devmedia without a uuid?
                          {
                             /* SD card from real SD card IF without Navi Data */
                             if((rMsg.u32Val == PRM_MAX_USB_DEVICES )&&(rMsg.au8UUID[0] != 0 ))
                             {
                                 pOsalData->u16SdCardType = u16Medium; 
                                 pOsalData->u16UsbMediaType[rMsg.u32Val] = u16Medium; // todo still neccessary?
                                 strncpy((char*)&((*pOsalData).u8DeviceUUID[ rMsg.u32Val ][0]),(char*)rMsg.au8UUID, sizeof(rMsg.au8UUID) - 1);
                                 vPrmSignal( u32Signal, MEDIUM_DEVMEDIA, rMsg.au8UUID );
                             }
                             else
                             {
                                 vWritePrintfErrmem("mount of devmedia real SD card IF Res:%d Val:%d without a UUID \n",rMsg.s32ResID,rMsg.u32Val);
                             }
                          }
                       }
                       else
                       {
                          // + case RES_ID_DEVMEDIA: in case of unmount, only the device name is available
                          // if a uuid is stored for the device sd? (at mount process), then /dev/media is the correct ResID
                          if(rMsg.u32Val < PRM_MAX_USB_DEVICES ) // check range of supported devices 0: sda, 1: sdb, ...
                          {
                             if( pOsalData->u8DeviceUUID[rMsg.u32Val][0] != 0 )
                             {
                                rMsg.s32ResID = RES_ID_DEVMEDIA; // switch to devmedia, uuid was send at mount event
                                strncpy((char*)rMsg.au8UUID, (char*)&((*pOsalData).u8DeviceUUID[rMsg.u32Val][0]), sizeof(rMsg.au8UUID) - 1);
                                pOsalData->u16UsbMediaType[rMsg.u32Val] = u16Medium; // todo still neccessary?
                                vPrmSignal( u32Signal, MEDIUM_DEVMEDIA, rMsg.au8UUID );
                                pOsalData->u8DeviceUUID[rMsg.u32Val][0] = 0;
                             }
                             else
                             {
                                   vWritePrintfErrmem("unmount of devmedia Res:%d Val:%d without a UUID \n",rMsg.s32ResID,rMsg.u32Val);
                             }
                          }
                          else
                          {
                              /* pOsalData->u8DeviceUUID is updated at insert of sd card, 
                                       so pOsalData->u8DeviceUUID is used to check UUID   */
                             if((rMsg.u32Val == PRM_MAX_USB_DEVICES )&&(pOsalData->u8DeviceUUID[rMsg.u32Val][0] != 0 ))
                             {
                                rMsg.s32ResID = RES_ID_DEVMEDIA; // switch to devmedia, uuid was send at mount event
                                strncpy((char*)rMsg.au8UUID,(char*)&((*pOsalData).u8DeviceUUID[rMsg.u32Val][0]), sizeof(rMsg.au8UUID) - 1);
                                pOsalData->u16SdCardType = u16Medium; 
                                vPrmSignal( u32Signal, MEDIUM_DEVMEDIA, rMsg.au8UUID );
                                pOsalData->u8DeviceUUID[rMsg.u32Val][0] = 0;
                             }
                             else
                             {
                                 snprintf(Buffer,200,"unmount of devmedia real SD card IF Res:%d Val:%d without a UUID",rMsg.s32ResID,(int)rMsg.u32Val);
                                 vWriteToErrMem((TR_tenTraceClass)TR_COMP_OSALCORE,(char*)&Buffer[0],strlen(&Buffer[0]),OSAL_STRING_OUT);
                             }
                          }
                        }
                      break;
                   case RES_ID_SD_CARD:
                        switch(rMsg.u32EvSignal)
                        {
                           case SIGNAL_MOUNT:
                                if(rMsg.u32Val < PRM_MAX_USB_DEVICES ) // check range of supported devices 0: sda, 1: sdb, ...
                                {
                                    pOsalData->s32UsbIdxForCard = rMsg.u32Val; // connected to sd? which is stored in u32Val
                                    // evaluate media and cal signature verfication during every reinsertion
                                    // Storre: only on devices with a cryptnav directory
                                    pOsalData->u16CardMediaType1 = u16EvaluateSdType(0,rMsg.u32Val);
                                    u16Medium = pOsalData->u16CardMediaType1; // change typ 
                                    //store the uuid in pOsalData for sd_card.
                                    strncpy((char*)&((*pOsalData).u8DeviceUUID[ rMsg.u32Val ][0]),(char*)rMsg.au8UUID, sizeof(rMsg.au8UUID) - 1);
                                    //since everything is under /dev/media interface uuid needs to be sent to applications
                                    vPrmSignal( MEDIUM_INSERTED, MEDIUM_SD, rMsg.au8UUID); 
                                }
                                else
                                {
                                   vWritePrintfErrmem("mount of devcryptcard Res:%d Val:%d with wrong Index \n",rMsg.s32ResID,rMsg.u32Val);
                                }
                                rMsg.s32ResID = RES_ID_DEVMEDIA;
                                (void)OSAL_s32MessageQueuePost( prm_hPrmMQ, (tPCU8)&rMsg, sizeof(trPrmMsg), 0 );
                            break;
                           case SIGNAL_UNMOUNT:
                                if(rMsg.u32Val < PRM_MAX_USB_DEVICES ) // check range of supported devices 0: sda, 1: sdb, ...
                                {
                                    pOsalData->s32UsbIdxForCard = -1;
                                    pOsalData->u16CardMediaType1 = OSAL_C_U16_MEDIA_EJECTED;
                                    /* set media type for TE*/
                                    rMsg.u16MediaType = pOsalData->u16CardMediaType1; // change typ 
                                    strncpy((char*)rMsg.au8UUID, (char*)&((*pOsalData).u8DeviceUUID[rMsg.u32Val][0]), sizeof(rMsg.au8UUID) - 1);
                                    vPrmSignal( MEDIUM_EJECTED, MEDIUM_SD, rMsg.au8UUID );
                                  //  pOsalData->u8DeviceUUID[rMsg.u32Val][0] = 0; -> because trigger for dev/media follows
                                }
                                else
                                { 
                                   vWritePrintfErrmem("mount of devcryptcard Res:%d Val:%d with wrong Index \n",rMsg.s32ResID,rMsg.u32Val);
                                }
                                rMsg.s32ResID = RES_ID_DEVMEDIA;
                                (void)OSAL_s32MessageQueuePost( prm_hPrmMQ, (tPCU8)&rMsg, sizeof(trPrmMsg), 0 );
                                break;
                            case SIGNAL_SD_ACTIVATE:
                                /* check for UNMoUNT was happened again*/
                                if(pOsalData->u16CardMediaType1 != OSAL_C_U16_MEDIA_EJECTED)
                                {
                                   M_SET_BIT( pOsalData->u8DeviceCryptCardStatus, C_U8_MEDIA_READY_BIT );
                                   /* media type is already set at SIGNAL_MOUNT*/
                                   vPrmSignal( MEDIUM_INSERTED, MEDIUM_SD, NULL );
                                }
                                else
                                {
                                   TraceString("SIGNAL_SD_ACTIVATE Card already ejected again");
                                }
                               break;
                          default:
                                TraceString("Unknown Signal received");
                              break;
                        }
                       break;
                  case RES_ID_LAST:    // signal to terminate the task
                      {
                        int fd;
                        pOsalData->bPrmRecTaskActiv = FALSE;
                        if(pOsalData->u32AmVersion == 2)
                        {
                           fd = open(AUTOMOUNT2"/exit.txt", O_CREAT|O_RDWR|O_TRUNC|O_CLOEXEC,OSAL_FILE_ACCESS_RIGTHS);
                        }
                        else
                        {
                           fd = open(AUTOMOUNT1"/exit.txt", O_CREAT|O_RDWR|O_TRUNC|O_CLOEXEC,OSAL_FILE_ACCESS_RIGTHS);
                        }
                        write(fd,(char*)"exit",4);
                        close(fd);
                        OSAL_s32ThreadJoin(PrmUdevThreadID,1000);
                      }
                     break;
                  case RES_ID_VOLTAGE: // use for usbpwr?
                     if( rMsg.u32EvSignal == SIGNAL_VOLTAGE )  // or UV or OC
                     {
                        tU8 buf[200];
                        UsbPortState *rPortState;

                        rPortState = (UsbPortState *)rMsg.au8UUID;

                        sprintf( (char*)buf, "PWR: Port: %d OC:%d; UV:%d; PP:%d; S:%u; E:%u; S:%u; E:%u; S:%u; E:%u\n", 
                                 rPortState->u8PortNr, rPortState->u8OC, rPortState->u8UV, rPortState->u8PPON,
                                 rPortState->u32OCStartTime, rPortState->u32OCEndTime,
                                 rPortState->u32UVStartTime, rPortState->u32UVEndTime,
                                 rPortState->u32PPONStartTime, rPortState->u32PPONEndTime
                                 );
                        vPrmTtfisTrace( TR_LEVEL_COMPONENT, "%s", buf );

                        vSendSignal( OSAL_C_U16_NOTI_LOW_POW, OSAL_C_U16_DEFECT_POWER_STATUS_OK, MEDIUM_USBPWR, rMsg.au8UUID );
                     }
                     else
                     {
                        NORMAL_M_ASSERT_ALWAYS();        // unmount with uuid not possible
                     }
                     break;
#ifdef PRM_LIBUSB_CONNECTION
                  case RES_ID_TRIGGER_OVC_STATUS: 
                      {
                        tU32 i;
                        if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_DATA ) )
                        {
                           for(i=0;i< pOsalData->u32PrmUSBGlobalPortCount;i++)
                           {
                              TraceString("RES_ID_TRIGGER_OVC_STATUS PWR: Port: %d OC:%d; UV:%d; PP:%d; S:%u; E:%u; S:%u; E:%u; S:%u; E:%u\n", 
                                          pOsalData->prm_rPortState[i].u8PortNr, pOsalData->prm_rPortState[i].u8OC, pOsalData->prm_rPortState[i].u8UV, pOsalData->prm_rPortState[i].u8PPON,
                                          pOsalData->prm_rPortState[i].u32OCStartTime, pOsalData->prm_rPortState[i].u32OCEndTime,
                                          pOsalData->prm_rPortState[i].u32UVStartTime, pOsalData->prm_rPortState[i].u32UVEndTime,
                                          pOsalData->prm_rPortState[i].u32PPONStartTime, pOsalData->prm_rPortState[i].u32PPONEndTime);
                           }
                        }
                        for(i=0;i<u32PrmUSBPortCount;i++)
                        {
                            vSendSignal( OSAL_C_U16_NOTI_LOW_POW, OSAL_C_U16_DEFECT_POWER_STATUS_OK, MEDIUM_USBPWR, ((tU8*)&pOsalData->prm_rPortState[i]));
                        }
                      }
                     break;
                  case RES_ID_USB_PORT_STATE: 	
                      {
                         OSAL_tEventHandle hEvent;
                         if(OSAL_s32EventOpen(rMsg.au8UUID,&hEvent) != OSAL_ERROR)
                         {
                            tU32 Mask = 0x1;
                            if(pOsalData->prm_rPortState[rMsg.u32Val-1].u8PortNr < 1)
                            {
                                TraceString(" RES_ID_USB_PORT_STATE wrong Index received ");
                            }
                            if(Prm_GetUsbPortState(&pOsalData->prm_rPortState[rMsg.u32Val-1] ) != OSAL_E_NOERROR)
                            {
                               Mask = 0x10;
                            }
                            if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_DATA ))
                            {
                               TraceString("RES_ID_USB_PORT_STATE PWR: Port: %d OC:%d; UV:%d; PP:%d\n", 
                                           pOsalData->prm_rPortState[rMsg.u32Val-1].u8PortNr, pOsalData->prm_rPortState[rMsg.u32Val-1].u8OC, 
                                           pOsalData->prm_rPortState[rMsg.u32Val-1].u8UV,pOsalData->prm_rPortState[rMsg.u32Val-1].u8PPON);
                            }
                            OSAL_s32EventPost(hEvent,0x1,OSAL_EN_EVENTMASK_OR);
                            OSAL_s32EventClose(hEvent);
                         }
                      }
                     break;
                  case RES_ID_USB_PORT_POWER: 				 
                      {				  
                         OSAL_tEventHandle hEvent;
                         if(OSAL_s32EventOpen(rMsg.au8UUID,&hEvent) != OSAL_ERROR)
                         {
                            tU32 Mask = 0x1;
                            if(prmUSBPower_control((tS32)&rMsg.u32Val) != OSAL_E_NOERROR)
                            {
                               Mask = 0x10;
                            }
                          //  TraceString("PRM USB_PORT_POWER Post Mask 0x%x",Mask);
                            OSAL_s32EventPost(hEvent,0x1,OSAL_EN_EVENTMASK_OR);
                            OSAL_s32EventClose(hEvent);
                         }
                      }
                     break;
#endif
                  default:
                     auBuf[0] = C_U8_TRACE_unknown_callback;
                     LLD_vTrace( PRM_TRACE_CLASS, TR_LEVEL_FATAL, auBuf, 1 );
                     break;
               }

              // todo remove a second eject if( rMsg.au8UUID[0] != 0 ) neccessary?
            } // if( rMsg.u32Val < PRM_MAX_USB_DEVICES )

            if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
            {
               auBuf[0] = PRM_TASK_END;
               LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, auBuf, 1);
            }
         }
      } // while( pOsalData->bPrmRecTaskActiv )

#ifdef PRM_LIBUSB_CONNECTION
     // prm_vDeleteLibUsbConnectionTask( PrmLibUsbThreadID );
     DeInitUsbForPrm();
#endif
      OSAL_s32MessageQueueClose( prm_hPrmMQ );
      OSAL_s32MessageQueueDelete( "LI_PRM_REC_MQ" );
   }


   TraceString("PRM Main Task terminates");
}


/*------------------------------------------------------------------------
ROUTINE NAME : prSearchResByName

PARAMETERS   : coszResourceName ( I ) = Resource Name

RETURNVALUE  : Pointer to the resource table entry

DESCRIPTION  :

COMMENTS     :

------------------------------------------------------------------------*/
static trResourceTableEntry *prSearchResByName( tCString coszResourceName )
{
   tInt nResNum;
   trResourceTableEntry *prRetVal = OSAL_NULL;

   for( nResNum = 0; nResNum < C_MAX_RESOURCES; nResNum++ )
   {
      if( !OSAL_s32StringCompare( coszResourceName, coarResourceTable[nResNum].ResourceName ) ) /*lint !e530 */
      {
         prRetVal = (trResourceTableEntry *)&coarResourceTable[nResNum];      /* Resource found! */
         break;
      }
   }
   return( prRetVal );
}



void TraceCallbackexcute(OSAL_tProcessID pid, OSAL_tThreadID tid,tU8 u8MediumType)
{
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_DATA ) )
   {
      tU8 buf[10];

      if(u8MediumType < MEDIUM_LAST)
      { 
      buf[0] = C_U8_TRACE_prm_callback_execute;
      buf[1] = u8MediumType;
      OSAL_M_INSERT_T32( &buf[2], (tU32)tid );
      OSAL_M_INSERT_T32( &buf[6], (tU32)pid );
      }
      else
      {
          if(u8MediumType == PRM_SYS_NOTIFY)
          {
              buf[0] = 0x24;
          }
          if(u8MediumType == PRM_SYSSTAT_NOTIFY)
          {
              buf[0] = 0x25;
          }
          OSAL_M_INSERT_T32( &buf[2], (tU32)tid );
          OSAL_M_INSERT_T32( &buf[6], (tU32)pid );
      }
      LLD_vTrace( PRM_TRACE_CLASS, TR_LEVEL_DATA, buf, 10 );
   }
}


/*------------------------------------------------------------------------
ROUTINE NAME : vExcutePrmCallback

PARAMETERS   : 

RETURNVALUE  :

DESCRIPTION  : 

COMMENTS     :

 ------------------------------------------------------------------------*/
static void vExcutePrmCallback( OSAL_tProcessID pid, OSAL_tThreadID tid, tU32 u32CallbackParameter,
                                tU32 u32Idx, tU8 u8MediumType, tU8 au8String[] )
{
   OSALCALLBACKFUNC     CallFunc  = NULL;
   OSALCALLBACKFUNCEXT2 CallFunc2 = NULL;

   switch( u8MediumType )
   {
      case MEDIUM_SD:
         if(pOsalData->arCallbackTable[u32Idx].cbCryptCardFun1)
         {
            CallFunc = pOsalData->arCallbackTable[u32Idx].cbCryptCardFun1;
         }
         break;
      case MEDIUM_DEVMEDIA:
         CallFunc2  = pOsalData->arCallbackTable[u32Idx].cbDevMediaFun;
         CallFunc   = (OSALCALLBACKFUNC)CallFunc2;
         break;
      case MEDIUM_USBPWR:
         CallFunc2  = pOsalData->arCallbackTable[u32Idx].cbUsbPwrFun;
         CallFunc   = (OSALCALLBACKFUNC)CallFunc2;
         break;
      default:
         break;
   }


   if( pid != OSAL_ProcessWhoAmI() ) // execute callback of other process
   {
      if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_DATA ) )
      {
         TraceString("Forward CB for PID:%d TID:%d Resource:%d ",pid,tid,u8MediumType);
      }
      // send callback to main callback handler message queue for dispatching
      tOsalMbxMsg rMsg;
      OSAL_tMQueueHandle Mq;

      rMsg.rPrmMsg.u32Pid     = pid;
      rMsg.rPrmMsg.u32CallFun = (uintptr_t)CallFunc;
      rMsg.rPrmMsg.u32Param1  = u32CallbackParameter;
      rMsg.rPrmMsg.u32Param2  = u8MediumType;
      if(( CallFunc2 != NULL ) && ( au8String != NULL ))
      {
         rMsg.rPrmMsg.rHead.Cmd  = MBX_CB_PRM2;
         memcpy( rMsg.rPrmMsg.au8UUID, au8String, sizeof(rMsg.rPrmMsg.au8UUID) );
      }
      else
      {
         rMsg.rPrmMsg.rHead.Cmd  = MBX_CB_PRM;
         rMsg.rPrmMsg.au8UUID[0] = 0;
      }

      Mq = GetPrcLocalMsgQHandle( pid );
      if(( Mq != 0 ) && ( CallFunc != NULL ))
      {
         if( OSAL_s32MessageQueuePost(Mq, (tPCU8)&rMsg, sizeof(tOsalMbxMsg), 0 ) == OSAL_ERROR )
         {
            NORMAL_M_ASSERT_ALWAYS();
         }
      }
      else
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
   }
   else
   {
      if( CallFunc2 != NULL )
      {
         CallFunc2( &u32CallbackParameter, au8String );
         TraceCallbackexcute(pid, tid,u8MediumType);
      }
      else if( CallFunc != NULL )
      {
         CallFunc( &u32CallbackParameter );
         TraceCallbackexcute(pid, tid,u8MediumType);
      }
   }
}


/*------------------------------------------------------------------------
ROUTINE NAME : vSendSignal

PARAMETERS   : u16Notification ( I ) = Notification to send
               u16Status       ( I ) = Current Status

RETURNVALUE  :

DESCRIPTION  : Sends a signal to registered applications

COMMENTS     :
HISTORY      :
Date         |   Modification                            | Authors
23.10.12     |   Fix for NIKAI-434.                      | sja3kor 
             |   Added MEDIUM_SD_NOCRYPT to send the     | 
             |   signal to application in case for data  |
             |   card.                                   |
20.03.13     |   Fix for NIKAI 3163                      | SWM2KOR
             |   Seperate notification registers are     |
             |   used for SD_CARD and CRYPTCARD          |
------------------------------------------------------------------------*/
static tVoid vSendSignal( tU16 u16Notification, tU16 u16Status, tU8 u8ResType, tU8 au8String[] )
{
   tU8  cnt;
   tU32 u32CallbackParameter;
   
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
   {
      tU8 buf[6];
      
      buf[0] = C_U8_TRACE_vSendSignal;
      buf[1] = (tU8)u8ResType;
      OSAL_M_INSERT_T16(&buf[2], u16Notification );
      OSAL_M_INSERT_T16(&buf[4], u16Status );
      LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, buf, 6 );
   }
   u32CallbackParameter  = ((tU32)((tU32)u16Notification << 16) | (u16Status));
 
   for( cnt = 0; cnt < C_MAX_APPLICATIONS; cnt++ )
   {
      switch( u8ResType )
      {
       case MEDIUM_SD:
         if( u16Notification & pOsalData->arCallbackTable[cnt].u16NotifyRegCryptCard )
         {
            // Callback function for Notfication found
            if(pOsalData->u32PrmAppId != 0xffffffff)
            {
               /* check for eject*/
               if(((u16Notification == OSAL_C_U16_NOTI_MEDIA_STATE)&&(u16Status == OSAL_C_U16_MEDIA_NOT_READY))
                ||((u16Notification == OSAL_C_U16_NOTI_MEDIA_CHANGE)&&(u16Status == OSAL_C_U16_MEDIA_EJECTED)))
               {     
                        vExcutePrmCallback( pOsalData->arCallbackTable[cnt].hCallerProcID, pOsalData->arCallbackTable[cnt].hCallerTaskID,
                                            u32CallbackParameter, cnt, MEDIUM_SD, au8String );
               }         
               else
               {
                  if(M_CHECK_BIT(pOsalData->u8DeviceCryptCardStatus, C_U8_MEDIA_READY_BIT ))
                  {
                     if(cnt != pOsalData->u32PrmAppId)
                     {
                        vExcutePrmCallback( pOsalData->arCallbackTable[cnt].hCallerProcID, pOsalData->arCallbackTable[cnt].hCallerTaskID,
                                            u32CallbackParameter, cnt, MEDIUM_SD, au8String );
                     }
                  }
                  else
                  {
                     if(cnt == pOsalData->u32PrmAppId)
                     {
                        vExcutePrmCallback( pOsalData->arCallbackTable[cnt].hCallerProcID, pOsalData->arCallbackTable[cnt].hCallerTaskID,
                                            u32CallbackParameter, cnt, MEDIUM_SD, au8String );
                     }
                  }
               }
            }
            else
            {
               if( u16Status == OSAL_C_U16_INCORRECT_MEDIA )
               {
                  u32CallbackParameter = ((tU32)u16Notification << 16) | (pOsalData->u16CardMediaType1);
               }
               vExcutePrmCallback( pOsalData->arCallbackTable[cnt].hCallerProcID, pOsalData->arCallbackTable[cnt].hCallerTaskID,
                                   u32CallbackParameter, cnt, MEDIUM_SD, au8String );
            }
         }
         break;
        case MEDIUM_CRYPTNAV:
         if( u16Notification & pOsalData->arCallbackTable[cnt].u16NotifyRegCryptnav )
         {
            // Callback function for Notfication found
            if( pOsalData->arCallbackTable[cnt].cbCryptnavFun != NULL )
            {
               vExcutePrmCallback( pOsalData->arCallbackTable[cnt].hCallerProcID, pOsalData->arCallbackTable[cnt].hCallerTaskID,
                                   u32CallbackParameter, cnt, MEDIUM_CRYPTNAV, au8String );    
            }
         }
         break;
      case MEDIUM_DEVMEDIA:
        if ( u16Notification & pOsalData->arCallbackTable[cnt].u16NotifyRegDevMedia )
         {
            // Callback function for Notfication found
            if( pOsalData->arCallbackTable[cnt].cbDevMediaFun != NULL )
            {
               vExcutePrmCallback( pOsalData->arCallbackTable[cnt].hCallerProcID, pOsalData->arCallbackTable[cnt].hCallerTaskID,
                                   u32CallbackParameter, cnt, MEDIUM_DEVMEDIA, au8String );
            }
         }
         break;
       case MEDIUM_USBPWR:
         if ( u16Notification & pOsalData->arCallbackTable[cnt].u16NotifyRegUsbPwr )
         {
            // Callback function for Notfication found
            if( pOsalData->arCallbackTable[cnt].cbUsbPwrFun != NULL )
            {
               vExcutePrmCallback( pOsalData->arCallbackTable[cnt].hCallerProcID, pOsalData->arCallbackTable[cnt].hCallerTaskID,
                                   u32CallbackParameter, cnt, MEDIUM_USBPWR, au8String );
            }
         }
         break;
       default:
         break;
      }
   }
}


/*------------------------------------------------------------------------
ROUTINE NAME : vSendResSignal

PARAMETERS   : u16Notification ( I ) = Notification to send
               u16Status       ( I ) = Current Status 
               u16ResMask      ( I ) = Resource mask

RETURNVALUE  :

DESCRIPTION  : Send a resource specific signal (i.e. a signal that applies
               only if the particular combination notification+resource
               is registered). Is called from exclusiv code, only!

COMMENTS     : For now only Mode Change notification is of this type,
               so the check for the notification type is not needed
HISTORY      :
Date         |   Modification                            | Authors
20.03.13     |   Fix for NIKAI 3163                      | SWM2KOR
             |   Seperate notification registers are     |
             |   used for SD_CARD and CRYPTCARD          |
------------------------------------------------------------------------*/
/*static tVoid vSendResSignal( tU16 u16Notification, tU16 u16Status, tU32 u32ResMask )
{
   tU32 par;
   tU8  cnt;

   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
   {
      tU8 buf[10];

      buf[0] = C_U8_TRACE_vSendResSignal;
      buf[1] = (tU8)0;
      OSAL_M_INSERT_T16(&buf[2], u16Notification);
      OSAL_M_INSERT_T16(&buf[4], u16Status);
      OSAL_M_INSERT_T32(&buf[6], u32ResMask);
      LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, buf, 10 );
   }

   par = ((tU32)u16Notification << 16) | (u16Status);
   
   for( cnt = 0; cnt < C_MAX_APPLICATIONS; cnt++ )
   {
      if(u32ResMask & pOsalData->arCallbackTable[cnt].u32ResourceMaskCryptCard)
      {  
         if( M_FIRST_ENCLOSES_SECOND(u32ResMask, pOsalData->arCallbackTable[cnt].u32ResourceMaskCryptCard) )
         {
            // Callback function for Notfication found
            if(pOsalData->arCallbackTable[cnt].cbCryptCardFun1 != NULL)
            {
               vExcutePrmCallback( pOsalData->arCallbackTable[cnt].hCallerProcID, pOsalData->arCallbackTable[cnt].hCallerTaskID,
                                   par, cnt, MEDIUM_SD, NULL );
            }
         }
      }
      else;
   }
}*/



/*------------------------------------------------------------------------
ROUTINE NAME : u32RegisterForNotification

PARAMETERS   : u16AppID            ( I ) = Application ID
               coszResourceName    ( I ) = Resouce Name
               u16NotificationType ( I ) = Notification type 
               pCallback           ( I ) = Callback for the event  
               pu16Data            (   ) = Parameter of the callback function


RETURNVALUE  : OSAL Error Code

DESCRIPTION  :

COMMENTS     :

HISTORY      :
    13/11/09  | anc3kor  | Added check for OSAL_C_U16_DATA_MEDIA_NAVI 
    26/09/12  | sja3kor  | Feature PRM_LIBUSB_CONNECTION defined
                           for OSAL_GEN2 section in case of prm_low_voltage. 
    20/03/13  | swm2kor  | Fix for NIKAI-3163 
                           Seperate Notification implementations for 
                           SD_CARD(/dev/card) and CRYPTCARD(/dev/cryptcard)
    30/07/13  | rmm2hi   | /dev/cdrom is no more used in GEN3. It is replaced with
                           /dev/cdctrl.
    03/06/14  | swm2kor  | To update the registration for u32PrmAppId update for cryptcard
                           Fix for SUZUKI-10989 
---------------------------------------------------------------------------------------------*/
static tU32 u32RegisterForNotification( tU16             u16AppID,
                                        tCString         coszResourceName,
                                        tU16             u16NotificationType,
                                        OSALCALLBACKFUNC pCallback,
                                        tU32             *pu32Data,
                                        tU8              *pu8Status,
                                        tBool            bTwoParameter
                                      )
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trCallbackTabEntry   *prNotyData = OSAL_NULL;
   trResourceTableEntry *prResource;
   tU32 u32Par    = 0;
   tU16 u16Medium = 0;
   OSALCALLBACKFUNCEXT2 pCallback2 = (OSALCALLBACKFUNCEXT2)pCallback;
   tBool bCryptCard = FALSE;

   ((tVoid)pu32Data); // satisfy lint

   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
   {
      tU8 buf[50];
      u32Par = (tU32)pCallback;

      buf[0] = C_U8_TRACE_u32RegisterForNotification;
      OSAL_M_INSERT_T16( &buf[1], u16AppID );
      OSAL_M_INSERT_T16( &buf[3], u16NotificationType );
      OSAL_M_INSERT_T32( &buf[5], u32Par);
      strncpy((char*) &buf[9], coszResourceName, 40 );
      LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, buf, 40 );
   }
   prResource = prSearchResByName( coszResourceName );

   *pu8Status = 0;

   /*REGISTER APPLICATION CALLBACK*/
   /*CHECK PARAMETERS*/
   if( (pCallback) && (prResource) && (u16AppID < C_MAX_APPLICATIONS) )
   {
      /* check if callback handler task for this process already exists */  
      tS32 s32PidEntry = s32FindProcEntry(OSAL_ProcessWhoAmI());
      if(s32PidEntry == OSAL_ERROR)
      {
         NORMAL_M_ASSERT_ALWAYS();
      }
      else
      {
         /* check if callback handler task for this process already exists */  
         s32StartCbHdrTask(s32PidEntry);
      }
/*LOCK CALLBACK TABLE*/
      if( LockOsal( &pOsalData->PrmTableLock ) == OSAL_OK )
      {
         /* -> prove of u16AppID >= C_MAX_APPLICATIONS already accomplised */
         prNotyData = &pOsalData->arCallbackTable[u16AppID];

         if( bTwoParameter == TRUE )   // only supported at /dev/media
         {
            if( !OSAL_s32StringCompare( "/dev/media", coszResourceName ))  /*lint !e530 */
            {
               if((prNotyData->cbDevMediaFun == OSAL_NULL) || (prNotyData->cbDevMediaFun == (OSALCALLBACKFUNCEXT2)pCallback))
               {
                  prNotyData->u16NotifyRegDevMedia |= u16NotificationType;
                  prNotyData->cbDevMediaFun         = (OSALCALLBACKFUNCEXT2)pCallback;
               }            
               prNotyData->hCallerTaskID = OSAL_ThreadWhoAmI();
               prNotyData->hCallerProcID = OSAL_ProcessWhoAmI();
               
               if( pOsalData->u8DeviceDevMediaCount > 0 )
               {
                  *pu8Status = C_U8_MEDIA_READY_BIT | C_U8_MEDIA_INSIDE_BIT;
               }
               else
               {
                  *pu8Status = 0;   // no device recognized
               }
               /* Notification is resource dependant type */
               if( u16NotificationType & OSAL_C_U16_NOTI_MODE_CHANGE )
               {
                  M_SET_BIT( prNotyData->u32ResourceMaskDevMedia, prResource->u32ResourceMask );
               }
               u16Medium = MEDIUM_DEVMEDIA;
            }
            else if( !OSAL_s32StringCompare( "/dev/usbpower", coszResourceName )) /*lint !e530 */
            {
               if((prNotyData->cbUsbPwrFun == OSAL_NULL) || (prNotyData->cbUsbPwrFun == (OSALCALLBACKFUNCEXT2)pCallback))
               {
                  prNotyData->u16NotifyRegUsbPwr |= u16NotificationType;
                  prNotyData->cbUsbPwrFun         = (OSALCALLBACKFUNCEXT2)pCallback;
               }            
               prNotyData->hCallerTaskID = OSAL_ThreadWhoAmI();
               prNotyData->hCallerProcID = OSAL_ProcessWhoAmI();
               
               *pu8Status = 0;   // no device recognized

               /* Notification is resource dependant type */
               if( u16NotificationType & OSAL_C_U16_NOTI_MODE_CHANGE )
               {
                  M_SET_BIT( prNotyData->u32ResourceMaskUsbPwr, prResource->u32ResourceMask );
               }
               u16Medium = MEDIUM_USBPWR;
            }
            else
            {
               u32ErrorCode = OSAL_E_NOTSUPPORTED;
            }
         }
         else if(  ( !OSAL_s32StringCompare( OSAL_C_STRING_DEVICE_CRYPTCARD, coszResourceName )) ) /*lint !e530 */
         {
            bCryptCard = TRUE;
            if( (prNotyData->cbCryptCardFun1 == OSAL_NULL) || (prNotyData->cbCryptCardFun1 == pCallback) )
            {
               prNotyData->u16NotifyRegCryptCard |= u16NotificationType;
               prNotyData->cbCryptCardFun1 = pCallback;
               prNotyData->hCallerTaskID = OSAL_ThreadWhoAmI();
               prNotyData->hCallerProcID = OSAL_ProcessWhoAmI();
               //Update Media Type,only if crypt card is inserted else provide as ejected
               if( M_CHECK_BIT( pOsalData->u8DeviceCryptCardStatus, C_U8_MEDIA_READY_BIT )) 
               {
                  pOsalData->u16CardMediaType1 = u16EvaluateSdType( 0, (tU32)pOsalData->s32UsbIdxForCard );  
               }
               else if(pOsalData->u32PrmAppId != 0xffffffff)
               {
                  if(( u16AppID == pOsalData->u32PrmAppId ) 
                     &&( M_CHECK_BIT( pOsalData->u8DeviceCryptCardStatus , C_U8_MEDIA_INSIDE_BIT )))
                  {
                     pOsalData->u16CardMediaType1 = u16EvaluateSdType( 0, (tU32)pOsalData->s32UsbIdxForCard );  
                     /* Inform only to u32PrmAppId that  Media is ready*/
                     *pu8Status |= C_U8_MEDIA_READY_BIT; 
                  }
               }
               else
               {
                  pOsalData->u16CardMediaType1 = OSAL_C_U16_MEDIA_EJECTED;
               }
               *pu8Status |= pOsalData->u8DeviceCryptCardStatus;
               /* Notification is resource dependant type */
               if( u16NotificationType & OSAL_C_U16_NOTI_MODE_CHANGE )
               {
                  M_SET_BIT( prNotyData->u32ResourceMaskCryptCard, prResource->u32ResourceMask );
               }
               u16Medium = MEDIUM_SD;
            }
         }
         else if( !OSAL_s32StringCompare( OSAL_C_STRING_DEVICE_CRYPTNAV, coszResourceName ) ) /*lint !e530 */
         {
            if((prNotyData->cbCryptnavFun == OSAL_NULL) || (prNotyData->cbCryptnavFun == pCallback))
            {  
               prNotyData->u16NotifyRegCryptnav |= u16NotificationType;
               prNotyData->cbCryptnavFun = pCallback;
               prNotyData->hCallerTaskID = OSAL_ThreadWhoAmI ();
               prNotyData->hCallerProcID = OSAL_ProcessWhoAmI();
            }
            else
            {
               /* Wrong notification callback*/
               u32ErrorCode = OSAL_E_UNKNOWN;
            }
            *pu8Status |= pOsalData->u8DeviceCryptnavStatus;
            /* Notification is resource dependant type */
            if(u16NotificationType & OSAL_C_U16_NOTI_MODE_CHANGE)
            {
               M_SET_BIT( prNotyData->u32ResourceMaskCryptnav, prResource->u32ResourceMask );
            }
            u16Medium = MEDIUM_CRYPTNAV;
         }
         else
         {
            u32ErrorCode = OSAL_E_NOTSUPPORTED;
            NORMAL_M_ASSERT_ALWAYS();
         }
         /*UNLOCK CALLBACK TABLE*/   
         UnLockOsal( &pOsalData->PrmTableLock );
      }
      else
      {
         u32ErrorCode = OSAL_E_UNKNOWN;
      }

      if( u32ErrorCode == OSAL_E_NOERROR )
      {
         tU16  u16Status   = 0;
         tU16 u16MedType = OSAL_C_U16_DEVICE_NOT_READY;
         
         if(u16Medium == (tU16)MEDIUM_SD)
         {
            u16Status   = *pu8Status;
            u16MedType = pOsalData->u16CardMediaType1;
         }
         else if(u16Medium == (tU16)MEDIUM_DEVMEDIA)
         {
            NORMAL_M_ASSERT( bTwoParameter == TRUE  );
            bTwoParameter = TRUE;
         }   
         else if(u16Medium == (tU16)MEDIUM_CRYPTNAV)
         {
            u16Status   = pOsalData->u8DeviceCryptnavStatus;
            u16MedType = pOsalData->u16CryptnavMediaType;
         }
         else if(u16Medium == (tU16)MEDIUM_USBPWR)
         {
            NORMAL_M_ASSERT( bTwoParameter == TRUE  );
            bTwoParameter = TRUE;
            u16MedType = OSAL_C_U16_DEFECT_POWER_STATUS_OK; // todo use ??? see under case RES_ID_VOLTAGE: 
         }
         else
         {
            u32ErrorCode = OSAL_E_INVALIDVALUE;
            NORMAL_M_ASSERT_ALWAYS();
         }

         if( u32ErrorCode == OSAL_E_NOERROR )
         {
            if(( u16NotificationType & OSAL_C_U16_NOTI_DEVICE_READY ) && (bTwoParameter == FALSE))
            {
               if( M_CHECK_BIT( u16Status, C_U16_DEVICE_READY_BIT ))
               {
                  u32Par = (OSAL_C_U16_NOTI_DEVICE_READY << 16) | OSAL_C_U16_DEVICE_READY;
               }
               else
               {
                  u32Par = (OSAL_C_U16_NOTI_DEVICE_READY << 16) | OSAL_C_U16_DEVICE_NOT_READY;
               }
                  if((pOsalData->u32PrmAppId != 0xffffffff)&&(bCryptCard == TRUE))
                  {
                      if(u16AppID == pOsalData->u32PrmAppId )
                      {
                         pCallback( &u32Par );
                      }
                  }
                  else
                  {
                     pCallback( &u32Par );
                  }
            }
            
            // callback with two parameter is for several devices with dev/media only!
            if( bTwoParameter == TRUE )
            {
               tU8 u8Loop;
               
               if( u16Medium == MEDIUM_DEVMEDIA )
               {
                  u16MedType = OSAL_C_U16_DATA_MEDIA; // we 
                  u16Status   = C_U8_MEDIA_READY_BIT;
                  
                  // loop via all possible devices
                  for( u8Loop = 0; u8Loop <= PRM_MAX_USB_DEVICES; u8Loop++ )
                  {
                     if( pOsalData->u8DeviceUUID[u8Loop][0] != 0 )   // device available
                     {
                        if( u16NotificationType & OSAL_C_U16_NOTI_MEDIA_CHANGE )
                        {
                           u32Par = (OSAL_C_U16_NOTI_MEDIA_CHANGE << 16) | u16MedType;
                           pCallback2( &u32Par, &((*pOsalData).u8DeviceUUID[ u8Loop ][0]) );
                        }
                      
                        if( u16NotificationType & OSAL_C_U16_NOTI_MEDIA_STATE )
                        {
                           if(M_CHECK_BIT( u16Status, C_U8_MEDIA_READY_BIT ))
                           {
                              u32Par = (OSAL_C_U16_NOTI_MEDIA_STATE << 16) | OSAL_C_U16_MEDIA_READY;
                           }
                           else
                           {
                              u32Par = (OSAL_C_U16_NOTI_MEDIA_STATE << 16) | OSAL_C_U16_MEDIA_NOT_READY;
                           }
                           pCallback2( &u32Par, &((*pOsalData).u8DeviceUUID[ u8Loop ][0]) );
                        }
                     }
                  }  // for
               }
               else if( u16Medium == MEDIUM_USBPWR )
               {
                  if( u16NotificationType & OSAL_C_U16_NOTI_LOW_POW )
                  {
#ifdef PRM_LIBUSB_CONNECTION
//                     UsbPortState LocalCopy;
//                     tS32 PortCount = 1;
                     tS32 i;

                     u32Par = (OSAL_C_U16_NOTI_LOW_POW << 16) | u16MedType;
#if !defined (OSAL_GEN3_SIM)
//                     prm_InitLibUsbConnectionData(); //Init for late processes
#endif

                  /*   if((prmGetPRMUSBPortCount((tS32)&PortCount)) == OSAL_E_NOERROR)
                     {
                         for(i=1;i<=PortCount;i++)
                         {
                             LocalCopy.u8PortNr=i;
                             Prm_GetUsbPortState( &LocalCopy );
                             pCallback2( &u32Par, (tU8 *)&LocalCopy );
                         }
                     }*/
                   //  else
                     {
                         OSAL_tMQueueHandle hPrmMQ = 0;
                         i=0;
                         while(OSAL_s32MessageQueueOpen( LINUX_PRM_REC_MQ, OSAL_EN_READWRITE, &hPrmMQ ) == OSAL_ERROR )
                         {
                              i++;
                              if(i==10)break;
                              OSAL_s32ThreadWait(100);
                         }
                         /* TRIGGER PRM Entry task , because access to USB not allowed due security reason*/
                         if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
                         {
                            TraceString("TRIGGER PRM Entry task , because access to USB not allowed due security reason");
                         }
                         trPrmMsg rMsg;
                         memset(&rMsg,0,sizeof(trPrmMsg));
                         rMsg.s32ResID    = (tS32)RES_ID_TRIGGER_OVC_STATUS;
                         (void)OSAL_s32MessageQueuePost( hPrmMQ, (tPCU8)&rMsg, sizeof(trPrmMsg), 0 );
                         OSAL_s32MessageQueueClose(hPrmMQ);
                     }
#endif
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_INVALIDVALUE;
                  NORMAL_M_ASSERT_ALWAYS();
               }
            }
            else
            {
               if( u16NotificationType & OSAL_C_U16_NOTI_MEDIA_CHANGE )
               {
                  u32Par = (OSAL_C_U16_NOTI_MEDIA_CHANGE << 16) | u16MedType;
                  if((pOsalData->u32PrmAppId != 0xffffffff)&&(bCryptCard == TRUE))
                  {
                      if(u16AppID == pOsalData->u32PrmAppId )
                      {
                         pCallback( &u32Par );
                      }
                  }
                  else
                  {
                         pCallback( &u32Par );
                  }
               }
             
               if( u16NotificationType & OSAL_C_U16_NOTI_MEDIA_STATE )
               {
                  if(M_CHECK_BIT( u16Status, C_U8_MEDIA_READY_BIT ))
                  {
                     u32Par = (OSAL_C_U16_NOTI_MEDIA_STATE << 16) | OSAL_C_U16_MEDIA_READY;
                  }
                  else
                  {
                     u32Par = (OSAL_C_U16_NOTI_MEDIA_STATE << 16) | OSAL_C_U16_MEDIA_NOT_READY;
                  }
                  if((pOsalData->u32PrmAppId != 0xffffffff)&&(bCryptCard == TRUE))
                  {
                      if(u16AppID == pOsalData->u32PrmAppId )
                      {
                         pCallback( &u32Par );
                      }
                  }
                  else
                  {
                     pCallback( &u32Par );
                  }
               }
            
 /*              if( u16NotificationType & OSAL_C_U16_NOTI_TOTAL_FAILURE )
               {
                  if(M_CHECK_BIT( u16Status, C_U8_DEVICE_NOT_OK_BIT ))
                  {
                     u32Par = (OSAL_C_U16_NOTI_TOTAL_FAILURE << 16) | OSAL_C_U16_DEVICE_FAIL;
                  }
                  else
                  {
                     u32Par = (OSAL_C_U16_NOTI_TOTAL_FAILURE << 16) | OSAL_C_U16_DEVICE_OK;
                  }
                  pCallback( &u32Par );
               }

               if(u16NotificationType & OSAL_C_U16_NOTI_DVD_OVR_TEMP)
               {
                  if(M_CHECK_BIT( u16Status, C_U8_HIGH_TEMPERATURE_BIT ))
                  {
                     u32Par = (OSAL_C_U16_NOTI_DVD_OVR_TEMP << 16) | OSAL_C_U16_OVER_TEMPERATURE;
                  }
                  else
                  {
                     u32Par = (OSAL_C_U16_NOTI_DVD_OVR_TEMP << 16) | OSAL_C_U16_NORMAL_TEMPERATURE;
                  }
                  pCallback( &u32Par );
               }

               if(u16NotificationType & OSAL_C_U16_NOTI_DEFECT)
               {
                  if( M_CHECK_BIT( u16Status, C_U16_TEMP_READ_ERR ))
                  {
                     u32Par = (OSAL_C_U16_NOTI_DEFECT << 16) | OSAL_C_U16_DEFECT_TEMP_READ;
                     pCallback( &u32Par );
                  }

               }
*/
               if(u16NotificationType & OSAL_C_U16_NOTI_MODE_CHANGE)
               {
                  /*if(M_CHECK_BIT((*pu8Status), C_U8_EXCLUSIVE_MODE_BIT))
                  {
                     trExAccList *prExAccEntry;
                     *
                      If status is exclusive means that 
                      all the prc in the resource are in exclusive mode,
                      then read the application that locks the first prc
                     *
                     prExAccEntry = pOsalData->arPrcCtrlTable[prResource->aPrcList[0]].prExAccListRoot;
    
                     if(prExAccEntry != OSAL_NULL)
                     {
                        u32Par = (OSAL_C_U16_NOTI_MODE_CHANGE << 16) | (prExAccEntry->u16AppID);
                     }
                  }
                  else*/
                  {
                     u32Par = (OSAL_C_U16_NOTI_MODE_CHANGE << 16) | OSAL_C_U16_INVALID_APPID;
                  }
                  pCallback( &u32Par );
               }
            }  // end if btwoparameter
         }
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
   {
      tU8 buf[12];

      buf[0] = C_U8_TRACE_u32RegisterForNotification_exit;
      OSAL_M_INSERT_T8 ( &buf[1], *pu8Status );
      OSAL_M_INSERT_T16( &buf[2], u16AppID );
      OSAL_M_INSERT_T32( &buf[4], u32ErrorCode );
      OSAL_M_INSERT_T32( &buf[8], u32Par);
      LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, buf, 12 );
   }
   return( u32ErrorCode );
}



/*------------------------------------------------------------------------
ROUTINE NAME : s32UnRegisterForNotification

PARAMETERS   : u16AppID            ( I ) = Application ID
               coszResourceName    ( I ) = Resouce Name
               u16NotificationType ( I ) = Notification type

RETURNVALUE  : OSAL Error Code

DESCRIPTION  :

COMMENTS     :
20/03/13  | swm2kor      | Fix for NIKAI-3163 
                           Seperate Notification implementations for 
                           SD_CARD(/dev/card) and CRYPTCARD(/dev/cryptcard)
------------------------------------------------------------------------*/
static tU32 u32UnRegisterForNotification( tU16     u16AppID,
                                          tCString coszResourceName,
                                          tU16     u16NotificationType
                                        )
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;
   trCallbackTabEntry   *prNotyData = NULL;
   trResourceTableEntry *prResource;    

   /*
    * Possible interface change - now do nothing, when the resource name is invalid.
    * Before the support for multiple drives the function removed the notification
    * even if the resource name was invalid. This is not possible anymore, because
    * we need the drive ID from the resource.
    */

   if(LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ))
   {
      tU8 buf[45];

      buf[0] = C_U8_TRACE_u32UnRegisterForNotification;
      OSAL_M_INSERT_T16( &buf[1], u16AppID );
      OSAL_M_INSERT_T16( &buf[3], u16NotificationType );
      strncpy((char*) &buf[5], coszResourceName, 39 );
      LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, buf, 45 );
   }

   prResource = prSearchResByName( coszResourceName );
   /* CHECK PARAMETERS */
   if((u16AppID < C_MAX_APPLICATIONS) && ( prResource != OSAL_NULL ))
   {
      //   OSAL_tenFSDevID enDriveID = 0;

      /* LOCK CALLBACK TABLE */
      if( LockOsal( &pOsalData->PrmTableLock ) == OSAL_OK)
      {
         prNotyData = &pOsalData->arCallbackTable[u16AppID];


         if( !OSAL_s32StringCompare( "/dev/media", coszResourceName ) ) /*lint !e530 */
         {
            M_CLEAR_BIT( prNotyData->u16NotifyRegDevMedia,u16NotificationType );
            /*Notification is resource dependant type*/
            if(u16NotificationType & OSAL_C_U16_NOTI_MODE_CHANGE)
            {
               M_CLEAR_BIT( prNotyData->u32ResourceMaskDevMedia, prResource->u32ResourceMask );
               /* Reset Callback registration if nedded for other resources */
               if(prNotyData->u32ResourceMaskDevMedia != 0)
               {
                  M_SET_BIT(prNotyData->u16NotifyRegDevMedia, OSAL_C_U16_NOTI_MODE_CHANGE);
               }
            }
            if( prNotyData->u16NotifyRegDevMedia == 0x0000 )
            {
               prNotyData->cbDevMediaFun = OSAL_NULL;
               prNotyData->hCallerTaskID = OSAL_ThreadWhoAmI();
               prNotyData->hCallerProcID = OSAL_ProcessWhoAmI(); // todo: ok to change procid?
            }
           }
         else if( !OSAL_s32StringCompare( OSAL_C_STRING_DEVICE_CRYPTCARD, coszResourceName ) ) /*lint !e530 */
         {
            M_CLEAR_BIT( prNotyData->u16NotifyRegCryptCard,u16NotificationType );
            /*Notification is resource dependant type*/
            if(u16NotificationType & OSAL_C_U16_NOTI_MODE_CHANGE)
            {
               M_CLEAR_BIT( prNotyData->u32ResourceMaskCryptCard, prResource->u32ResourceMask );
            }
            if( prNotyData->u16NotifyRegCryptCard == 0x0000 )
            {
               prNotyData->cbCryptCardFun1 = OSAL_NULL;
               prNotyData->hCallerTaskID = OSAL_ThreadWhoAmI();
               prNotyData->hCallerProcID = OSAL_ProcessWhoAmI(); // todo: ok to change procid?
            }
         }
         else if( !OSAL_s32StringCompare( OSAL_C_STRING_DEVICE_CRYPTNAV, coszResourceName ) ) /*lint !e530 */
         {
            M_CLEAR_BIT( prNotyData->u16NotifyRegCryptnav,u16NotificationType );
            /*Notification is resource dependant type*/
            if(u16NotificationType & OSAL_C_U16_NOTI_MODE_CHANGE)
            {
               M_CLEAR_BIT( prNotyData->u32ResourceMaskCryptnav, prResource->u32ResourceMask );

               /* Reset Callback registration if nedded for other resources */
               if(prNotyData->u32ResourceMaskCryptnav != 0)
               {
                  M_SET_BIT( prNotyData->u16NotifyRegCryptnav, OSAL_C_U16_NOTI_MODE_CHANGE );
               }
            }
            if( prNotyData->u16NotifyRegCryptnav == 0x0000 )
            {
               prNotyData->cbCryptnavFun = OSAL_NULL;
               prNotyData->hCallerTaskID = OSAL_ThreadWhoAmI();
               prNotyData->hCallerProcID = OSAL_ProcessWhoAmI(); // todo: ok to change procid?
            }
         }
        else if( !OSAL_s32StringCompare( "/dev/usbpower", coszResourceName ) ) /*lint !e530 */
         {

            M_CLEAR_BIT( prNotyData->u16NotifyRegUsbPwr,u16NotificationType );
            /* Notification is resource dependant type */
            if( u16NotificationType & OSAL_C_U16_NOTI_MODE_CHANGE )
            {
               M_CLEAR_BIT( prNotyData->u32ResourceMaskUsbPwr, prResource->u32ResourceMask );
               /* Reset Callback registration if nedded for other resources */
               if(prNotyData->u32ResourceMaskUsbPwr != 0)
               {
                  M_SET_BIT(prNotyData->u16NotifyRegUsbPwr, OSAL_C_U16_NOTI_MODE_CHANGE);
               }
            }
            if( prNotyData->u16NotifyRegUsbPwr == 0x0000 )
            {
               prNotyData->cbUsbPwrFun = OSAL_NULL;
               prNotyData->hCallerTaskID = OSAL_ThreadWhoAmI();
               prNotyData->hCallerProcID = OSAL_ProcessWhoAmI(); // todo: ok to change procid?
            }
         }
         else
         {
            NORMAL_M_ASSERT_ALWAYS();
         }
      }
      /*UNLOCK CALLBACK TABLE*/   
      UnLockOsal(&pOsalData->PrmTableLock);
   }
   else
   {
      u32ErrorCode = OSAL_E_INVALIDVALUE;
   }

   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
   {
      tU8 buf[7];
      
      buf[0] = C_U8_TRACE_u32UnRegisterForNotification_exit;
      OSAL_M_INSERT_T16( &buf[1], u16AppID );
      OSAL_M_INSERT_T32( &buf[3], u32ErrorCode );
      LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, buf, 7);
   }

   return(u32ErrorCode);
}



/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/


/*------------------------------------------------------------------------
ROUTINE NAME : prm_vInit

PARAMETERS   :

RETURNVALUE  :

DESCRIPTION  : Initialization function for prm data structures

COMMENTS     : called from Devinit.c from function vInitOsalIO()
HISTORY      :
Date         |   Modification                            | Authors
20.03.13     |   Fix for NIKAI 3163                      | SWM2KOR
             |   Seperate notification registers are     |
             |   used for SD_CARD and CRYPTCARD          |
 ------------------------------------------------------------------------*/ 
tVoid prm_vInit()
{
   tU8 i;
 
 /*  for( i = 0; i < PRM_EN_PRCID_LAST; i++ )
   {
      pOsalData->arPrcCtrlTable[i].prExAccListRoot = OSAL_NULL;
      pOsalData->arPrcCtrlTable[i].device_name[OSAL_C_U32_MAX_PATHLENGTH-1] = 0;
   }
   * Get used PRC address *
   strncpy(pOsalData->arPrcCtrlTable[PRM_EN_PRCID_CRYPTCARD].device_name, OSAL_C_STRING_DEVICE_CRYPTCARD, OSAL_C_U32_MAX_PATHLENGTH-1);
   pOsalData->arPrcCtrlTable[PRM_EN_PRCID_CRYPTCARD].enDriveID   = OSAL_EN_DEVID_CRYPT_CARD;
   pOsalData->arPrcCtrlTable[PRM_EN_PRCID_CRYPTCARD].u32PrcBit   = C_U32_PRC_CRYPTCARD_BIT;

   strncpy(pOsalData->arPrcCtrlTable[PRM_EN_PRCID_CRYPTNAV].device_name, OSAL_C_STRING_DEVICE_CRYPTNAV, OSAL_C_U32_MAX_PATHLENGTH-1);
   pOsalData->arPrcCtrlTable[PRM_EN_PRCID_CRYPTNAV].enDriveID   = OSAL_EN_DEVID_CRYPTNAV;
   pOsalData->arPrcCtrlTable[PRM_EN_PRCID_CRYPTNAV].u32PrcBit   = C_U32_PRC_CRYPTNAV_BIT;

   strncpy(pOsalData->arPrcCtrlTable[PRM_EN_PRCID_DEV_MEDIA].device_name, "/dev/media", OSAL_C_U32_MAX_PATHLENGTH-1);
   pOsalData->arPrcCtrlTable[PRM_EN_PRCID_DEV_MEDIA].enDriveID   = OSAL_EN_DEVID_DEV_MEDIA;
   pOsalData->arPrcCtrlTable[PRM_EN_PRCID_DEV_MEDIA].u32PrcBit   = C_U32_PRC_DEV_MEDIA_BIT;
    
   strncpy(pOsalData->arPrcCtrlTable[PRM_EN_PRCID_USBPWR].device_name, "/dev/usbpower", OSAL_C_U32_MAX_PATHLENGTH-1);
   pOsalData->arPrcCtrlTable[PRM_EN_PRCID_USBPWR].enDriveID   = (OSAL_tenFSDevID)OSAL_EN_DEVID_USBPWR;
   pOsalData->arPrcCtrlTable[PRM_EN_PRCID_USBPWR].u32PrcBit   = C_U32_PRC_DEV_USBPWR_BIT;*/

   for( i = 0; i < C_MAX_APPLICATIONS; i++ )
   {
      pOsalData->arCallbackTable[i].cbCryptCardFun1         = OSAL_NULL;
      pOsalData->arCallbackTable[i].u16NotifyRegCryptCard   = 0x0000;
      pOsalData->arCallbackTable[i].u32ResourceMaskCryptCard = 0x00000000;
      pOsalData->arCallbackTable[i].cbCryptnavFun           = OSAL_NULL;
      pOsalData->arCallbackTable[i].u16NotifyRegCryptnav    = 0x0000;
      pOsalData->arCallbackTable[i].u32ResourceMaskCryptnav = 0x00000000;
      pOsalData->arCallbackTable[i].cbDevMediaFun           = OSAL_NULL;
      pOsalData->arCallbackTable[i].u16NotifyRegDevMedia    = 0x0000;
      pOsalData->arCallbackTable[i].u32ResourceMaskDevMedia = 0x00000000;
      
      pOsalData->arCallbackTable[i].hCallerTaskID       = 0;
      pOsalData->arCallbackTable[i].hCallerProcID       = 0;
   }

   /* OS semaphore for PRM table access control creation */
   //pOsalData->u32PrcStatus = 0x00000000; // PRM stata: bit set if prc in exclusive mode


   //pOsalData->bCryptEnabled    = 1;// This value is inittialized later in Devinit.c
   
   pOsalData->s32UsbIdxForCard = -1; // init with -1 for not connected   
   pOsalData->u16CardMediaType1  = OSAL_C_U16_MEDIA_EJECTED;
   pOsalData->u16SdCardMediaType = OSAL_C_U16_MEDIA_EJECTED;
   
   pOsalData->u16SdCardType = OSAL_C_U16_MEDIA_EJECTED;
   
   pOsalData->u8DeviceCryptnavStatus = C_U8_MEDIA_INSIDE_BIT | C_U8_MEDIA_READY_BIT;
   pOsalData->u16CryptnavMediaType   = OSAL_C_U16_DATA_MEDIA_NAVI;
   pOsalData->u32CryptcardId  = (tU32)-1;

   pOsalData->u8DeviceDevMediaCount = 0;

}

void vStartPrmTasks(void)
{
  /* start PRM task */
   OSAL_trThreadAttribute  attr;
   attr.szName = "PRM_RECOGNITION";
   attr.u32Priority  = 37; // todo use define from header x
   attr.s32StackSize = minStackSize;
   attr.pfEntry = (OSAL_tpfThreadEntry)PRM_vRecTaskEntry;
   attr.pvArg   = NULL;
   if( PrmRecThreadID != OSAL_ERROR )
   {
      FATAL_M_ASSERT_ALWAYS(); // second initialisation of the prm is not allowed
   }
   PrmRecThreadID = OSAL_ThreadSpawn( &attr );
   if( PrmRecThreadID == OSAL_ERROR )
   {
      FATAL_M_ASSERT_ALWAYS();
   }
   pOsalData->s32PrmMainPrcId = PrmRecThreadID;
}

/*------------------------------------------------------------------------
ROUTINE NAME : prm_bIsAPrmFun 

PARAMETERS   : s32Fun   ( I ) = OSAL IOCTRL function

RETURNVALUE  : TRUE if s32Fun is a PRM function


DESCRIPTION  : Checks function type

COMMENTS     : This function is only called from the dispatcher IOCTRL function

------------------------------------------------------------------------*/
tBool prm_bIsAPrmFun( tS32 s32Fun )
{
   tU8 u8RetVal = FALSE;
   if(  ( s32Fun <= OSAL_C_S32_IOCTRL_PRM_MAX_VALUE )
     && ( s32Fun >= OSAL_C_S32_IOCTRL_PRM_MIN_VALUE ) )
   {
      u8RetVal = TRUE;
   }
   if( pOsalData->bTraceLevel4Active != FALSE )
   {
      tU8 buf[6];

      buf[0] = C_U8_TRACE_prm_bIsAPrmFun;
      OSAL_M_INSERT_T32( &buf[1], (tU32)s32Fun );
      OSAL_M_INSERT_T8 ( &buf[5], u8RetVal );
      LLD_vTrace( PRM_TRACE_CLASS, TR_LEVEL_DATA, buf, 6 );
   }
   return( (tBool)u8RetVal );
}



/*------------------------------------------------------------------------
ROUTINE NAME : prm_s32Prm

PARAMETERS   : rDevId        ( I )= Device identificator
               s32Fun        ( I )= Function identificator
               s32Arg        ( I )= Argument to be passed to function

RETURNVALUE  : OSAL Error code

DESCRIPTION  :

COMMENTS     : This function is only called from the dispatcher IOCTRL function

------------------------------------------------------------------------*/
tU32 prm_u32Prm(OSAL_tIODescriptor rDevId, tS32 s32Fun, intptr_t s32Arg)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;

//   OSAL_trAccessData     *prAccessData;
   OSAL_trNotifyData     *prNotifyData;
   OSAL_trNotifyDataExt2 *prNotifyData2;
   OSAL_trNotifyDataSystem* prNotifyData3;
   OSAL_trRelNotifyData  *prRelNotifyData;
//   OSAL_trEX_Open_Arg    *prEXOpenArg;
   OSAL_trRemountData *prRemountData;
   Part_info *Partitiondata;
   tS32 i;
   tU32 u32DriveID;
   tU8  u8ResType  = 0xff;
//   tenPrcID enPrc  = PRM_EN_PRCID_LAST;

   vCheckPrm();
   /* get the drive number */
   u32DriveID = (tU32)OSAL_get_drive_id( rDevId );   // return value is the PRM_INDEX in dispatcher_table.g
   
   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
   {
      tU8 buf[10];

      buf[0] = C_U8_TRACE_prm_u32Prm;
      OSAL_M_INSERT_T8 ( &buf[1], u8ResType);
      OSAL_M_INSERT_T32( &buf[2], (tU32)s32Fun );
      OSAL_M_INSERT_T32( &buf[6], (tU32)s32Arg );
      LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, buf, 10 );
   }


   // s32Arg must be checked again because of lint is stupid
   if( s32Arg != 0 )
   {
      switch(s32Fun)
      {
         case OSAL_C_S32_IOCTRL_PRM_GETMEDIAINFO:
         case OSAL_C_S32_IOCTRL_PRM_GETDEVICEINFO:
         case OSAL_C_S32_IOCTRL_PRM_GETDISKTYPE:
         case OSAL_C_S32_IOCTRL_PRM_EJECTLOCK:
            /* prepare the variables according to drive ID */
            switch( u32DriveID )
            {
               case OSAL_EN_DEVID_CRYPT_CARD:
//                  enPrc = PRM_EN_PRCID_CRYPTCARD;
                  u8ResType = MEDIUM_SD;
                  break;
               case OSAL_EN_DEVID_DEV_MEDIA:
//                  enPrc = PRM_EN_PRCID_DEV_MEDIA;
                  u8ResType = MEDIUM_DEVMEDIA;
                  break;
               case OSAL_EN_DEVID_CRYPTNAV:
//                  enPrc = PRM_EN_PRCID_CRYPTNAV;
                  u8ResType = MEDIUM_CRYPTNAV;
                  break;
               default:
                  u32ErrorCode = OSAL_E_INVALIDVALUE;
                  u8ResType    = (tU8)-1;
                  break;
            }
            u32ErrorCode = OSAL_E_INVALIDVALUE;
            break;
            
         // for this ioctrl we do not need the variable values from enPrc and u8ResType
            
         case OSAL_C_S32_IOCTRL_GET_EXCLUSIVE_ACCESS:
             if(pOsalData->s32MainOsalPrcPid == OSAL_ProcessWhoAmI())
             {
                 vStartPrmTasks();
             }
            u32ErrorCode = OSAL_E_NOTSUPPORTED;
            break;
         case OSAL_C_S32_IOCTRL_REL_EXCLUSIVE_ACCESS:
            u32ErrorCode = OSAL_E_NOTSUPPORTED;
            break;
         case OSAL_C_S32_IOCTRL_PRM_ACTIVATE_SIGNAL:
            vPostSdCardTrigger();
            break;
         case OSAL_C_S32_IOCTRL_REG_NOTIFICATION:
            prNotifyData = (OSAL_trNotifyData *)s32Arg;
            u32ErrorCode = u32RegisterForNotification( prNotifyData->u16AppID,
                                                       prNotifyData->ResourceName,
                                                       prNotifyData->u16NotificationType,
                                                       prNotifyData->pCallback,
                                                       prNotifyData->pu32Data,
                                                       &(prNotifyData->u8Status),
                                                       FALSE );
            break;
         case OSAL_C_S32_IOCTRL_UNREG_NOTIFICATION:
         case OSAL_C_S32_IOCTRL_PRM_UNREG_NOTIFICATION_EXT2:
            prRelNotifyData = (OSAL_trRelNotifyData *)s32Arg;
            u32ErrorCode = u32UnRegisterForNotification( prRelNotifyData->u16AppID,
                                                         prRelNotifyData->ResourceName,
                                                         prRelNotifyData->u16NotificationType );
            break;
         case OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2:        
            prNotifyData2 = (OSAL_trNotifyDataExt2 *)s32Arg;
            u32ErrorCode  = u32RegisterForNotification( prNotifyData2->u16AppID,
                                                        prNotifyData2->ResourceName,
                                                        prNotifyData2->u16NotificationType,
                                                        (OSALCALLBACKFUNC)prNotifyData2->pCallbackExt2,
                                                        prNotifyData2->pu32Data,
                                                        &(prNotifyData2->u8Status),
                                                        TRUE );
           break;
         case OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_INFO:
            prNotifyData3 = (OSAL_trNotifyDataSystem *)s32Arg;
            if(pOsalData->pSysNotifyCallback == NULL)
            {
               if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_DATA ) )
               {
                  TraceString("PRM OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_INFO for PID:%d succeeded for ID:%d ",getpid(),pOsalData->s32SysNotifyPrcId);
               }
               pOsalData->s32SysNotifyPrcId  = OSAL_ProcessWhoAmI();
               pOsalData->pSysNotifyCallback = prNotifyData3->pCallback;
               pOsalData->pSysNotifyu32Data  = prNotifyData3->pu32Data;
            }
            else
            {
               TraceString("PRM OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_INFO for PID:%d failed",getpid());
               TraceString("PRM OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_INFO was alredy done for PID:%d",pOsalData->s32SysNotifyPrcId);
              u32ErrorCode  = OSAL_E_BUSY;
            }
           break;
#ifdef PRM_LIBUSB_CONNECTION
         case OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTCOUNT:
             u32ErrorCode = prmGetPRMUSBPortCount(s32Arg);
             if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_DATA ) )
             {
                 TraceString("OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTCOUNT returns %d ",*(tS32*)s32Arg);
             }
             break;
         case OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE:
              {				  
                UsbPortState* prPortStat =  (UsbPortState*)s32Arg;
 //               TraceString("OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE Start Port: %d !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ", prPortStat->u8PortNr);
                if((prPortStat->u8PortNr >= 1)&&(prPortStat->u8PortNr <= pOsalData->u32PrmUSBGlobalPortCount))
                {
                   u32ErrorCode = u32PostUsbPortTrigger(prPortStat->u8PortNr);
                   if((u32ErrorCode == OSAL_E_NOERROR)&&(prPortStat->u8PortNr >= 1))
                   {
                      memcpy(prPortStat,&pOsalData->prm_rPortState[prPortStat->u8PortNr-1],sizeof(UsbPortState));
                      if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_DATA ) )
                      {
                         TraceString("OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE returns PWR: Port: %d OC:%d; UV:%d; PP:%d", 
                                      prPortStat->u8PortNr, prPortStat->u8OC,prPortStat->u8UV,prPortStat->u8PPON);
                      }
                   }
                   else
                   {
                      u32ErrorCode = OSAL_E_UNKNOWN;
                   }
                }
                else
                {
                   u32ErrorCode = OSAL_E_INVALIDVALUE;
                }
//                TraceString("OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE End Port: %d !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", prPortStat->u8PortNr);
              }
             break;
         case OSAL_C_S32_IOCTRL_PRM_USBPOWER_SET_PORTPOWER:
//             TraceString("OSAL_C_S32_IOCTRL_PRM_USBPOWER_SET_PORTPOWER Start !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");
             u32ErrorCode = u32PostUsbPowerTrigger((usb_port_control*)s32Arg);
//             TraceString("OSAL_C_S32_IOCTRL_PRM_USBPOWER_SET_PORTPOWER End  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");
             break;
#endif /*PRM_LIBUSB_CONNECTION*/
         case OSAL_C_S32_IOCTRL_PRM_UNREG_SYSTEM_INFO:
            pOsalData->s32SysNotifyPrcId  = 0;
            pOsalData->pSysNotifyCallback = NULL;
            pOsalData->pSysNotifyu32Data  = NULL;
            break;


         case OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_STAT_INFO:
            prNotifyData3 = (OSAL_trNotifyDataSystem *)s32Arg;
            for(i=0;i<5;i++)
            {
              if(pOsalData->rPrmSysStatus[i].pNotifyCallback == NULL)
              {
                  pOsalData->rPrmSysStatus[i].pNotifyCallback = prNotifyData3->pCallback;
                  pOsalData->rPrmSysStatus[i].s32NotifyPrcId  = OSAL_ProcessWhoAmI();
                  pOsalData->rPrmSysStatus[i].pNotifyu32Data  = prNotifyData3->pu32Data;
                  break;
              }
            }
            if(i == 5)
            {
               TraceString("PRM OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_STAT_INFO for PID:%d failed",getpid());
               u32ErrorCode  = OSAL_E_BUSY;
            }
           break;

         case OSAL_C_S32_IOCTRL_PRM_UNREG_SYSTEM_STAT_INFO:
            for(i=0;i<5;i++)
            {
               if(pOsalData->rPrmSysStatus[i].s32NotifyPrcId  == OSAL_ProcessWhoAmI())
               {
                  pOsalData->rPrmSysStatus[i].pNotifyCallback = NULL;
                  pOsalData->rPrmSysStatus[i].s32NotifyPrcId  = 0;
                  pOsalData->rPrmSysStatus[i].pNotifyu32Data  = NULL;
                  break;
               }
            }
            break;
         case OSAL_C_S32_IOCTRL_EX_OPEN_FILE:
             u32ErrorCode = OSAL_E_NOTSUPPORTED;
            break;
         case OSAL_C_S32_IOCTRL_PRM_REMOUNT:
              prRemountData =  (OSAL_trRemountData *)s32Arg;
              TraceString("PRM OSAL_C_S32_IOCTRL_PRM_REMOUNT %s with option %s",prRemountData->szPath,prRemountData->szOption);
              u32ErrorCode = u32PostRemountTrigger(prRemountData->szPath,prRemountData->szOption);
            break;
         case OSAL_C_S32_IOCTRL_PRM_TRIGGER_ERRMEM_TTFIS:
              s32TraceErrmem(FALSE,0);
            break;
         case OSAL_C_S32_IOCTRL_PRM_TRIGGER_DEL_ERRMEM:
              vEraseErrmem(0);
            break;
         case OSAL_C_S32_IOCTRL_PRM_PARTITION_INFO:
              Partitiondata = (Part_info*)s32Arg;
              TraceString("PRM OSAL_C_S32_IOCTRL_PRM_PARTITION_INFO %s \n",Partitiondata->path);
              /* Update the partition Info for requested folder location */
              u32ErrorCode = u32FindFolderpartitioninfo(Partitiondata->path);
              if(u32ErrorCode == OSAL_E_NOERROR)
              {
                    Partitiondata->sRet = Part_Value;
              }
            break;
         default:
            break;
      } /* switch (s32Fun) */
   }  /* if( u32ErrorCode == OSAL_E_NOERROR ) */

   if( u32ErrorCode == OSAL_E_INVALIDVALUE ) // This error is traced with level fatal
   {
      if( LLD_bIsTraceActive( PRM_TRACE_CLASS, PRM_TRACE_LEVEL ) )
      {
         tU8 buf[14];
         
         buf[0] = C_U8_TRACE_prm_u32Prm_error_exit;
         OSAL_M_INSERT_T8 ( &buf[1], (tU8)u32DriveID );
         OSAL_M_INSERT_T32( &buf[2], (tU32)s32Fun );
         OSAL_M_INSERT_T32( &buf[6], (tU32)s32Arg );
         OSAL_M_INSERT_T32( &buf[10], u32ErrorCode );
         LLD_vTrace( PRM_TRACE_CLASS, PRM_TRACE_LEVEL, buf, 14 );
      }
   }

   if( LLD_bIsTraceActive( PRM_TRACE_CLASS, TR_LEVEL_USER_2 ))
   {
      tU8 buf[5];
      
      buf[0] = C_U8_TRACE_prm_u32Prm_exit; // 16
      OSAL_M_INSERT_T32( &buf[1], u32ErrorCode );
      LLD_vTrace( PRM_TRACE_CLASS, TR_LEVEL_USER_2, buf, 5 );
   }

   return(u32ErrorCode);
}


/********************************************************************/ /**
  FUNCTION       vPrmTtfisTrace
  SCOPE          module local

  @param         UB: Level to trace
  @param         char*: arguments list a la printf
  @return        none

  @brief         ttfis tracing (maximum 255 chars) 

  @note
  @pre           none
  @post          none
*************************************************************************/
void vPrmTtfisTrace(tU8 u8Level, const char *pcFormat, ...)
{ 
   va_list argList;  //lint !e530 ID 6 va_list is an makro which can't be initialized. ID: permitted

   if(LLD_bIsTraceActive( PRM_TRACE_CLASS, u8Level ))
   {
      char pcBuffer[255];
      char *buf = &pcBuffer[1];
      int noChar;

      pcBuffer[0] = C_U8_TRACE_prm_usbpwr_Msg;

      va_start( argList, pcFormat );   //lint !e530 ID 6 va_list is an makro which can't be initialized. ID: permitted
      noChar = vsnprintf( buf, 253, pcFormat, argList ); //lint !e530 ID 6 va_list is an makro which can't be initialized. ID: permitted
      va_end( argList );               //lint !e530 ID 6 va_list is an makro which can't be initialized. ID: permitted
      pcBuffer[255 - 1]    = '\0';  //last buffer entry
      pcBuffer[noChar + 1] = '\0';  //end of the real msg
      LLD_vTrace( PRM_TRACE_CLASS, (TR_tenTraceLevel)u8Level, pcBuffer, (tS32)strlen( pcBuffer ));
   }
} 



/*------------------------------------------------------------------------
ROUTINE NAME : 

PARAMETERS   : 

RETURNVALUE  : 

DESCRIPTION  :

COMMENTS     : This function is only called from osfile.c

 ------------------------------------------------------------------------*/
static tU16 u16EvaluateSdType( tU32 tu32CryptId, tU32 u32PhysId )
{
   tS32 s32file;
   tU16 u16Type = OSAL_C_U16_MEDIA_EJECTED;
   char *SymlinkCard1;
   char *SymlinkCard2;
   char *DeviceCard = NULL;
  // tU8  u8DeviceName[50];
   //check for medium.cfg 
   if( tu32CryptId == 0 )
   {
      SymlinkCard1 = "/shared/cryptnav/CRYPTNAV/MEDIUM.CFG";
      SymlinkCard2 = "/shared/cryptnav/CRYPTNAV/ROOT.NDS";
   }
   else
   {
      return OSAL_C_U16_MEDIA_EJECTED;
   }

   if( u32PhysId == 0 )
   {
      DeviceCard = "/dev/sda";
   }
   else if( u32PhysId == 1 )
   {
      DeviceCard = "/dev/sdb";
   }
   else if( u32PhysId == 2 )
   {
      DeviceCard = "/dev/sdc";
   }
   else if( u32PhysId == 3 )
   {
      DeviceCard = "/dev/sdd";
   }
   else if( u32PhysId == 4 )
   {
      DeviceCard = "/dev/sde";
   }
   else if( u32PhysId == 5 )
   {
      DeviceCard = "/dev/sdf";
   }
   else if( u32PhysId == 6 )
   {
      DeviceCard = "/dev/sdg";
   }
   else if( u32PhysId == 7 )
   {
      DeviceCard = "/dev/sdh";
   }
   else if( u32PhysId == 8 )
   {
   //  strncpy(u8DeviceName,"/dev/",strlen("/dev/"));
   //  u32CardDeviceName(&u8DeviceName[strlen("/dev/")]);
   //  DeviceCard = (char *)u8DeviceName;
   }
   else
   {
      return OSAL_C_U16_MEDIA_EJECTED;
   }
   
   if((s32file = open( SymlinkCard1, O_RDONLY,S_IRUSR | S_IXUSR | S_IROTH | S_IXOTH)) >= 0)
   {     
      u16Type = OSAL_C_U16_DATA_MEDIA_NAVI;
      close(s32file);
   }
   else if((s32file = open( SymlinkCard2, O_RDONLY,S_IRUSR | S_IXUSR | S_IROTH | S_IXOTH)) >= 0)
   {     
      u16Type = OSAL_C_U16_DATA_MEDIA_NAVI;
      close(s32file);
   }
   else if( (s32file = open( DeviceCard , O_RDONLY | O_NONBLOCK )) >= 0 )//check for SD card device mount point 
   {  
      u16Type = OSAL_C_U16_DATA_MEDIA;
      close(s32file);                                
   }
   else 
   { 
     if(DeviceCard == NULL)
     {
        u16Type = OSAL_C_U16_INCORRECT_MEDIA; 
     }
     else
     {
        u16Type = OSAL_C_U16_MEDIA_EJECTED; 
     }
   }
   return u16Type;
}

#ifdef __cplusplus
}
#endif
/************************************************************************ 
|end of file prm_table.c
|-----------------------------------------------------------------------*/
