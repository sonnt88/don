/*****************************************************************************
| $Id: registry.c,v
| $Revision:   1.10  $
| $Date:   Apr 13 2004 09:06:16  $
|*****************************************************************************
| FILE:         registry.c
| PROJECT:      ELeNa
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is a  implementation file for the OSAL
|               (Operating System Abstraction Layer) input-output functions
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2001 Tecne s.r.l., Cagliari (ITALY)
| HISTORY:      
| Date      | Modification               | Author
| 15.04.02  | Initial revision           | -------, -----
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************
| This file is under RCS control (do not edit the following lines)
| $RCSfile: registry.c,v $
|*****************************************************************************/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#define SYSTEM_S_IMPORT_INTERFACE_REGISTRY
#include "osal_if.h"


#include "Linux_osal.h"
#include <ostrace.h>


/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/
#define REGISTRY_VERSION 0x00000100
#define REG_BASEPOSITION        "LOCAL_MACHINE/"

              /* With GM Version 11.11S001 312 entries were used */
              /* Consider 25 % headroom --> 400 */
#define REGISTRY_MAX_LOOKUP_NUMBER 400

#define REGISTRY_MAX_LOOKUP_SIZE (REGISTRY_MAX_LOOKUP_NUMBER * sizeof(trRegMap))
#define REGISTRY_START_VALUE 0xFFFF
#define REGISTRY_DELETED_ENTRY 0xFFFF

#define REGISTRY_SUM_SIZE (REGISTRY_MEM_SIZE + sizeof(trRegMapAdmin) + REGISTRY_MAX_LOOKUP_SIZE)

#ifndef OSAL_C_S32_IOCTRL_REGLOOKUPAPPPATH
#define OSAL_C_S32_IOCTRL_REGLOOKUPAPPPATH (OSAL_C_S32_IOCTRL_REGDELETEVALUE + 1)
#endif
#ifndef OSAL_C_S32_IOCTRL_REGLOOKUPSRVPATH
#define OSAL_C_S32_IOCTRL_REGLOOKUPSRVPATH (OSAL_C_S32_IOCTRL_REGDELETEVALUE + 2)
#endif

//#define SHOW_SIZES
#ifdef SHOW_SIZES
#define REGISTRY_SITE "LINUX: "
#endif

#define REGISTRY_TRACE_ENABLED

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/
typedef struct
{
 tU32  u32Size;
 void* pNext;
}trMemAdMin;

typedef struct
{
  tU32  u32UsedSize;
  void* prLastAdMin;
}trAddress;

/******************************************************************************
 * STRUCT:      trRegValue
 *
 * DESCRIPTION: Elements of this structure describe values within the
 *              registry.
 *
 *              The values form a singly linked list that is attached to
 *              the corresponding registry key.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig 
 *****************************************************************************/
typedef struct _trRegValue
{
   tU32 u32szNameOffset;          /* The name of this value */
   tS32 s32Type;               /* Type of the value */
   union
   {
      tS32 s32NumValue;        /* Value of numeric type */
      tU32 u32StringValueOffset;  /* Value of string type */
   } uData;
   tU32 u32NextOffset; /* Link to the next value */
} trRegValue;

/******************************************************************************
 * STRUCT:      trRegKey
 *
 * DESCRIPTION: A key of the registry.  The keys are organized hierarchically.
 *
 *              Each key maintains a singly linked list of subkeys of the
 *              next hierarchy level, and a singly linked list of value
 *              elements.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig 
 *****************************************************************************/
typedef struct _trRegKey
{
   tU32 u32szNameOffset;               /* The name of the key */
   tU32 u32ParentKeyOffset;   /* The parent within the key hierarchy */
   tU32 u32NextKeyOffset;     /* The next key in the parents chain */
   tU32 u32FirstSubKeyOffset; /* This keys chain of subkeys */
   tU32 u32FirstValueOffset;  /* The list of values for this key */
   tU32 u32OpenCount;  /* Number of times this key has been opened */
} trRegKey;

typedef struct _trRegMap
{
   tString coszName[OSAL_C_U32_MAX_PATHLENGTH]; /* The name of the key */
   tS32    nId;                                 /* The Id of the key */
   tU32    nNextId;                             /* The next key in the parents chain */
} trRegMap;

typedef struct _trRegMapAdmin
{
   tU32  u32LookupCount; // count in sharedmem
   tU32  u32FirstAppMapElement; // offset in sharedmem to first element
   tU32  u32FirstSrvMapElement; // offset in sharedmem to first element
   tU32  u32LastAppMapElement; // offset in sharedmem to first element
   tU32  u32LastSrvMapElement; // offset in sharedmem to first element
} trRegMapAdmin;

/******************************************************************************
 * STRUCT:      trRegIO
 *
 * DESCRIPTION: A registry IO descriptor.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig 
 *****************************************************************************/
typedef struct
{
   OSAL_tenAccess enAccess; /* Access rights of this IO descriptor */
   trRegKey*      prKey;    /* The key referenced by this IO descriptor */
} trRegIO;

/******************************************************************************
 * STRUCT:      trPathPartInfo
 *
 * DESCRIPTION: This helper structure is used to extract single parts of a
 *              path name.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig 
 *****************************************************************************/
typedef struct
{
   tChar szBuf[OSAL_C_U32_MAX_PATHLENGTH]; /* Contains a pathname */
   tU32 u32BufLen;                         /* Length of the path name */
   tChar* pcNextPos;                       /* Points to the next path part */
} trPathPartInfo;


#ifdef __cplusplus
extern "C" {
#endif 


/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
/* added to satisfy linker */
tU32 u32dummy = 0;
extern aes_encrypt      pEncryptAesFile;

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/
/* The RegistryLock semaphore locks access to all the registry functions */

/* The registry root key */
static trRegKey* prRegistry = NULL;
/* Module-local buffer for pathname operations (saves stack) */
static trPathPartInfo rPathPart;

static tVoid*          pvRegMem     = NULL;
static trMemAdMin*     prLastAdMin  = NULL;
static tVoid*          pvLookupMem  = NULL;
static trRegMapAdmin*  prMapAdmin   = NULL; // offset in sharedmem

static tS32 s32LockedByTid = 0;

static OSAL_tShMemHandle hRegShMemHandle = 0;


tU32 REG_u32IOControl(uintptr_t u32fd, tS32 fun, intptr_t arg);


/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/
#ifdef REGISTRY_TRACE_ENABLED
static void TraceOut(TR_tenTraceLevel u32Level, const char *format, ...)
{
   char buf[OSAL_C_U32_MAX_PATHLENGTH];
   va_list ap;

   buf[0] = 0xf1;
   OSAL_VarArgStart(ap, format);/*lint -e718*/
   (tVoid)vsprintf(buf+1, format, ap);
   OSAL_VarArgEnd(ap);

   LLD_vTrace(TR_COMP_OSALIO, (tU32)u32Level, buf, 1+strlen(buf+1));   
}
#else
#define TraceOut(...)
#endif

static trRegMap* pMapindex2Pointer(tU32 n)
{
   if ((pvLookupMem != NULL) && (n != REGISTRY_START_VALUE))
   {
      trRegMap* prKey = (trRegMap*)pvLookupMem;
      return  &prKey[n];
   }
   else
   {
      return NULL;
   }
}

#ifdef SHOW_SIZES
void vShowStructSizes(void)
{
    TraceString(REGISTRY_SITE);
    TraceString(REGISTRY_SITE"Size of trRegValue:%u",sizeof(trRegValue));
    TraceString(REGISTRY_SITE"Size of trRegKey:%u",sizeof(trRegKey));
    TraceString(REGISTRY_SITE"Size of trRegIO:%u",sizeof(trRegIO));
    TraceString(REGISTRY_SITE"Size of trPathPartInfo:%u",sizeof(trPathPartInfo));
    TraceString(REGISTRY_SITE"Size of REGISTRY_SUM_SIZE:%d \n",REGISTRY_SUM_SIZE);
}
#endif

tBool bLockRegistry(void)
{
   tBool bRet = TRUE;
   if(LockOsal(&pOsalData->RegObjLock) != OSAL_OK) bRet = FALSE;
   return bRet;
}

tBool bUnLockRegistry(void)
{
   tBool bRet = TRUE;
   if (UnLockOsal(&pOsalData->RegObjLock) != OSAL_OK) bRet = FALSE;
   return bRet;
}


void* pvGetMemory(tU32 u32Size)
{

    prLastAdMin = (trMemAdMin*)((trAddress*)pvRegMem)->prLastAdMin;
    tU32 u32Temp;
    
    /* determine return adress */
    void* pMem = (void*)((char*)pvRegMem+((trAddress*)pvRegMem)->u32UsedSize);

//#ifdef 4_BYTEALIGNMENT
    /* ensure 4Byte alignment */
    u32Temp = u32Size%4;
    if(u32Temp)
    {
       u32Size = u32Size + 4 - u32Temp;
    }
    if((((trAddress*)pvRegMem)->u32UsedSize + u32Size + sizeof(trMemAdMin)) > REGISTRY_MEM_SIZE)
    {
       FATAL_M_ASSERT_ALWAYS();
    }
//#endif

    /* calculate new  u32UsedSize */
    ((trAddress*)pvRegMem)->u32UsedSize +=(sizeof(trMemAdMin) + u32Size);
    prLastAdMin = (trMemAdMin*)pMem;

    /* set new  admin data */
    ((trMemAdMin*)pMem)->u32Size = u32Size;
    ((trMemAdMin*)pMem)->pNext   = prLastAdMin;
    ((trAddress*)pvRegMem)->prLastAdMin = prLastAdMin;
    return (void*)((char*)pMem+sizeof(trMemAdMin));

}

void vFreeMemory(void* pMem)
{
  ((void)pMem);
  
   pMem = (void*)((uintptr_t)pMem - sizeof(trMemAdMin));
   pOsalData->u32LostRegBytes += ((trMemAdMin*)pMem)->u32Size;
  // TraceString("OSALIO Registry PID:%d Lost %d bytes ",OSAL_ProcessWhoAmI(),((trMemAdMin*)pMem)->u32Size);

 /*  trMemAdMin* pMemOwnAdMin  = (trMemAdMin*)(pMem - sizeof(trMemAdMin));
      trMemAdMin* pMemNextAdMin = pMemOwnAdMin + pMemOwnAdMin->u32Size + sizeof(trMemAdMin);
      pMemNextAdMin->pNext = pMemOwnAdMin->pNext;
      pMemOwnAdMin->pNext = NULL;
      memset(pMem,'X',pMemOwnAdMin->u32Size);*/


}

OSAL_DECL tU32 u32GetUsedRegistrySize(void)
{
   return ((trAddress*)pvRegMem)->u32UsedSize;
}


/******************************************************************************
 * FUNCTION:     s32StrICmp
 *
 * DESCRIPTION:  Compare two strings case insensitively.
 *
 * PARAMETER:    coszStr1:  (->I)
 *               coszStr2:  (->I)
 *                  Two strings that are to be compared.
 *
 * RETURNVALUE:  0  if both strings compare equal
 *               <0 if szStr1 is less than szStr2
 *               >0 if szStr1 is greater than szStr2
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 07.15.03  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
#define s32ToUpper( x ) (( x < 'a') || ( x > 'z') ? x : ( (x - 'a') + 'A' ))

static tS32 s32StrICmp( tCString coszStr1, tCString coszStr2 )
{
   tS32 s32Char1;
   tS32 s32Char2;
   tS32 s32Diff;

   s32Char1 = s32ToUpper( *coszStr1 );
   s32Char2 = s32ToUpper( *coszStr2 );
   s32Diff  = s32Char1 - s32Char2;
   coszStr1++;
   coszStr2++;

   while( (s32Diff == 0) && (s32Char1 != 0) && (s32Char2 != 0) )
   {
      s32Char1 = s32ToUpper( *coszStr1 );
      s32Char2 = s32ToUpper( *coszStr2 );
      coszStr1++;
      coszStr2++;
      s32Diff  = s32Char1 - s32Char2;
   }
   return s32Diff;
}

/******************************************************************************
 * FUNCTION:     pAddKey
 *
 * DESCRIPTION:  Allocate and add a registry key into the lookup table.
 *
 * PARAMETER:    coszName:  (->I)
 *                  The name of the registry key that is to be created.
 *
 * RETURNVALUE:  A registry key with the given name, or OSAL_NULL if an
 *               error occured (not enough memory).
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 22.06.11  |   Initial revision                     | TMS Jentsch
 *****************************************************************************/

static tU32 MapLookupAlloc()
{
   tU32 n = REGISTRY_START_VALUE;
   if (prMapAdmin && ((prMapAdmin->u32LookupCount) < REGISTRY_MAX_LOOKUP_NUMBER))
   {
      n = prMapAdmin->u32LookupCount;
      prMapAdmin->u32LookupCount++;
      #ifdef SHOW_SIZES
      TraceOut(TR_LEVEL_SYSTEM_MIN, "LookupCount = %d", prMapAdmin->u32LookupCount);
      #endif
   }
   else
   {
     TraceOut(TR_LEVEL_FATAL, "Registry size to small LookupCount = %d", prMapAdmin->u32LookupCount);
     NORMAL_M_ASSERT_ALWAYS();
   }
   return n;
}

static tU32 pAdd2Map(tU32 LastUsedIndex, tCString coszName, tS32 nID)
{
   /* The memory for the key and the name are allocated as one single
    * chunk to prevent memory fragmentation */

  tU32 n = MapLookupAlloc();

  if (n != REGISTRY_START_VALUE)
  {
     trRegMap* prKey = pMapindex2Pointer(n);
     (void)OSAL_szStringNCopy(prKey->coszName, coszName, OSAL_C_U32_MAX_PATHLENGTH);
     prKey->nId = nID;
     prKey->nNextId = REGISTRY_START_VALUE;
     if (LastUsedIndex != REGISTRY_START_VALUE)
     {
        trRegMap* ptrMapKey = pMapindex2Pointer(LastUsedIndex);
        if(ptrMapKey)
        {
          ptrMapKey->nNextId = n;
        }
        else
        {
          NORMAL_M_ASSERT_ALWAYS();
        }
     }
  }
  return n;
}

static tVoid vRemoveFromMap(tU32 FirstUsedIndex,  tS32 nID)
{
   trRegMap* prKeyMap = pMapindex2Pointer(FirstUsedIndex);

   while (prKeyMap != NULL)
   {
      if (prKeyMap->nId == nID)
      {
         prKeyMap->nId = REGISTRY_DELETED_ENTRY;
         TraceOut(TR_LEVEL_SYSTEM_MIN, "Remove: Id 0x%04x (%s) from Lookup table", nID, prKeyMap->coszName);
      }
      prKeyMap = pMapindex2Pointer(prKeyMap->nNextId);
   }
}



/******************************************************************************
 * FUNCTION:     prAllocKey
 *
 * DESCRIPTION:  Allocate a registry key with a given name.
 *
 * PARAMETER:    coszName:  (->I)
 *                  The name of the registry key that is to be created.
 *
 * RETURNVALUE:  A registry key with the given name, or OSAL_NULL if an
 *               error occured (not enough memory).
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static trRegKey* prAllocKey(tCString coszName)
{
  trRegKey* prKey;
  tU32 nLen = OSAL_u32StringLength(coszName) + 1;
  tU32 nSize = (nLen*sizeof(tChar)) + sizeof(trRegKey);

   /* The memory for the key and the name are allocated as one single
    * chunk to prevent memory fragmentation */
  prKey = (trRegKey*)pvGetMemory(nSize);
  if (prKey != OSAL_NULL)
  {
      tString szName = (tString)(&prKey[1]);
      (void)OSAL_szStringCopy(szName, coszName);
      prKey->u32szNameOffset      = (uintptr_t)(&prKey[1]) - (uintptr_t)prKey;
      prKey->u32ParentKeyOffset   = 0;
      prKey->u32NextKeyOffset     = 0;
      prKey->u32FirstSubKeyOffset = 0;
      prKey->u32FirstValueOffset  = 0;
      prKey->u32OpenCount  = 0;
  }
  return prKey;
}

/******************************************************************************
 * FUNCTION:     bDeleteKey
 *
 * DESCRIPTION:  Delete a registry key.
 *
 *               A registry key can only be deleted if it is currently not
 *               open and if it has neither values nor subkey attached to it.
 *
 *               This function takes care to correctly adjust the links
 *               of the remaining keys.
 *
 * PARAMETER:    prKey:  (->I)
 *                  Pointer to the registry key that is to be deleted.
 *
 * RETURNVALUE:  TRUE on success, FALSE otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tBool bDeleteKey(trRegKey* prKey)
{
   tBool bSuccess = FALSE;
   trRegKey* prTempKey = NULL;

   if (prKey != OSAL_NULL)
   {
      /* The key must not have subkeys or values attached, and it must not
       * be open */
      if ((prKey->u32FirstSubKeyOffset == 0) &&
          (prKey->u32FirstValueOffset == 0) &&
          (prKey->u32OpenCount == 0))
      {
         if (prKey->u32ParentKeyOffset != 0)
         {
            /* find the key preceeding this one */
            trRegKey* prPrev = OSAL_NULL;
            trRegKey* prCur = (trRegKey*)((uintptr_t)prKey - prKey->u32ParentKeyOffset);
            prCur = (trRegKey*)((uintptr_t)prCur + prCur->u32FirstSubKeyOffset);
            
            while ((prCur != prKey) 
                 &&(prCur != OSAL_NULL)
                 &&(prCur->u32NextKeyOffset != 0))
            {
               prPrev = prCur;
               prCur = (trRegKey*)((uintptr_t)prCur + prCur->u32NextKeyOffset);
            }
            
            /* adjust the necessary links */
            if (prPrev == OSAL_NULL)
            {
              prTempKey = (trRegKey*)((uintptr_t)prKey - prKey->u32ParentKeyOffset);
              if(prKey->u32NextKeyOffset)
              {
                 prTempKey->u32FirstSubKeyOffset = prTempKey->u32FirstSubKeyOffset + prKey->u32NextKeyOffset;
              }
              else
              {
                 prTempKey->u32FirstSubKeyOffset = 0;
              }
            }
            else
            {
                if(prKey->u32NextKeyOffset)
                {
                   prPrev->u32NextKeyOffset = prPrev->u32NextKeyOffset + prKey->u32NextKeyOffset;
                }
                else
                {
                   prPrev->u32NextKeyOffset = 0;
                }
            }
         }
         vFreeMemory(prKey);
         bSuccess = TRUE;
      }
   }
   else
   {
      bSuccess = TRUE;
   }
   return bSuccess;
}

/******************************************************************************
 * FUNCTION:     prAllocNumericValue
 *
 * DESCRIPTION:  Allocate a numeric registry value with a given name and data.
 *
 * PARAMETER:    coszName:  (->I)
 *                  The name of the registry value that is to be created.
 *
 *               s32Data:   (I)
 *                  The data that is associated with the value.
 *
 * RETURNVALUE:  A registry value with the given name, or OSAL_NULL if an
 *               error occured (not enough memory).
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static trRegValue* prAllocNumericValue(tCString coszName, tS32 s32Data)
{
   tU32 u32NameLen = OSAL_u32StringLength(coszName) + 1;
   tU32 u32Size = sizeof(trRegValue) + (u32NameLen * sizeof(tChar));
   
   /* The memory for the value and the name are allocated as one single
    * chunk to prevent memory fragmentation */

   trRegValue* prValue = (trRegValue*)pvGetMemory(u32Size);
   if(prValue)
   {
      tString szName = (tString)(&prValue[1]);
      (void)OSAL_szStringCopy(szName, coszName);
      
      prValue->u32szNameOffset = (uintptr_t)(&prValue[1]) - (uintptr_t)prValue;
      prValue->s32Type = OSAL_C_S32_VALUE_S32;
      prValue->uData.s32NumValue = s32Data;
      prValue->u32NextOffset = 0;
   }
   return prValue;
}

/******************************************************************************
 * FUNCTION:     prAllocStringValue
 *
 * DESCRIPTION:  Allocate a string registry value with a given name and data.
 *
 * PARAMETER:    coszName:  (->I)
 *                  The name of the registry value that is to be created.
 *
 *               coszData:  (->I)
 *                  The data that is associated with the value.
 *
 * RETURNVALUE:  A registry value with the given name, or OSAL_NULL if an
 *               error occured (not enough memory).
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static trRegValue* prAllocStringValue(tCString coszName, tCString coszData)
{
   tU32 u32NameLen = OSAL_u32StringLength(coszName) + 1;
   tU32 u32DataLen = OSAL_u32StringLength(coszData) + 1;
   tU32 u32Size = sizeof(trRegValue) + ((u32NameLen + u32DataLen) * sizeof(tChar));
   
   /* The memory for the value, its name and its data are allocated as one
    * single chunk to prevent memory fragmentation */
   trRegValue* prValue;

   prValue = (trRegValue*)pvGetMemory(u32Size);
   if(prValue)
   {
      tString szName = (tString)(&prValue[1]);
      tString szData = &(szName[u32NameLen]);
      
      (void)OSAL_szStringCopy(szName, coszName);
      (void)OSAL_szStringCopy(szData, coszData);
      
      prValue->u32szNameOffset = (uintptr_t)(&prValue[1]) - (uintptr_t)prValue;
      prValue->s32Type = OSAL_C_S32_VALUE_STRING;
      prValue->uData.u32StringValueOffset = (uintptr_t)(szData) - (uintptr_t)prValue;
      prValue->u32NextOffset = 0;
   }
   return prValue;
}

/******************************************************************************
 * FUNCTION:     vDeleteValue
 *
 * DESCRIPTION:  Delete a registry value.
 *
 *               This function takes care to correctly adjust the links
 *               of the remaining values.
 *
 * PARAMETER:    prValue:  (->I)
 *                  Pointer to the registry value that is to be deleted.
 *
 *               prOwner:  (->I)
 *                  Pointer to the registry key this value belongs to.
 *                  If the value does not belong to any registry key,
 *                  this parameter can be set to OSAL_NULL. In this case
 *                  nothing will be re-linked.
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tVoid vDeleteValue(trRegValue* prValue, trRegKey* prOwner)
{
   if (prValue != OSAL_NULL)
   {
      if (prOwner != OSAL_NULL)
      {
         /* find the key preceeding this one */
         trRegValue* prPrev = OSAL_NULL;
         trRegValue* prCur = (trRegValue*)((uintptr_t)prOwner + prOwner->u32FirstValueOffset);
         
         while ((prCur != prValue) 
             && (prCur != NULL)
             && (prCur->u32NextOffset != 0))
         {
            prPrev = prCur;
            prCur = (trRegValue*)((uintptr_t)prOwner + prCur->u32NextOffset);
         }
         
         /* adjust the necessary links */
         if (prPrev == OSAL_NULL)
         {
            if(prValue->u32NextOffset)
            {
               prOwner->u32FirstValueOffset = prValue->u32NextOffset;
            }
            else
            {
               prOwner->u32FirstValueOffset = 0;
            }
         }
         else
         {
            if(prValue->u32NextOffset)
            {
                prPrev->u32NextOffset = /* prPrev->u32NextOffset + */prValue->u32NextOffset;
            }
            else
            {
               prPrev->u32NextOffset = 0;
            }
         }
      }

      // remove entries in lookup tables
      if ((prValue->s32Type == OSAL_C_S32_VALUE_S32) && (s32StrICmp((tCString)(prValue + prValue->u32szNameOffset), REGVALUE_APPID) == 0))
      {
         vRemoveFromMap(prMapAdmin->u32FirstAppMapElement, prValue->uData.s32NumValue);
      } 
      else if ((prValue->s32Type == OSAL_C_S32_VALUE_S32) && (s32StrICmp((tCString)(prValue + prValue->u32szNameOffset), REGVALUE_SERVICEID) == 0))
      {
         vRemoveFromMap(prMapAdmin->u32FirstSrvMapElement, prValue->uData.s32NumValue);
      }

      vFreeMemory(prValue);
   }
}

/******************************************************************************
 * FUNCTION:     vReplaceValue
 *
 * DESCRIPTION:  Replace a registry value with a new one.
 *
 *               The old value will be deleted.
 *
 * PARAMETER:    prOldValue:  (->I)
 *                  Pointer to the registry value that is to be replaced.
 *
 *               prNewValue:  (->I)
 *                  Pointer to the registry value that should replace the
 *                  old value.
 *                  This value must not be linked to any key yet!
 *
 *               prOwner:  (->I)
 *                  Pointer to the registry key the old value belongs to.
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tVoid vReplaceValue(trRegValue* prOldValue, trRegValue* prNewValue, trRegKey* prOwner)
{
   if ((prOldValue != OSAL_NULL) && (prNewValue != OSAL_NULL) &&
       (prOwner != OSAL_NULL))
   {
      /* find the key preceeding the old one */
      trRegValue* prPrev = OSAL_NULL;
      trRegValue* prCur = (trRegValue*)((uintptr_t)prOwner + prOwner->u32FirstValueOffset);

      while ((prCur != prOldValue) 
          && (prCur != NULL)
          && (prCur->u32NextOffset != 0))
      {
         prPrev = prCur;
         prCur  = (trRegValue*)((uintptr_t)prOwner + (uintptr_t)prCur->u32NextOffset);
      }

      if (prPrev == OSAL_NULL)
      {
         prOwner->u32FirstValueOffset = (uintptr_t)prNewValue - (uintptr_t)prOwner;
      }
      else
      {
         prPrev->u32NextOffset = (uintptr_t)prNewValue - (uintptr_t)prOwner;
      }
      prNewValue->u32NextOffset = prOldValue->u32NextOffset;

     vFreeMemory(prOldValue);
   }
}

/******************************************************************************
 * FUNCTION:     vAddKey
 *
 * DESCRIPTION:  Add a key to another keys list of subkeys.
 *
 * PARAMETER:    prParentKey:  (->I)
 *                  Pointer to the key to whose list of subkeys the new key
 *                  is to be added.
 *
 *               prSubKey:     (->I)
 *                  Pointer to the key that ist to be added.
 *                  This key must not be linked to any key yet!
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tVoid vAddKey(trRegKey* prParentKey, trRegKey* prSubKey)
{
   trRegKey* prCur;
   prSubKey->u32ParentKeyOffset = (uintptr_t)prSubKey - (uintptr_t)prParentKey;
   if (prParentKey->u32FirstSubKeyOffset == 0)
   {
       prParentKey->u32FirstSubKeyOffset = (uintptr_t)prSubKey - (uintptr_t)prParentKey;
   }
   else
   {
      prCur = (trRegKey*)((uintptr_t)prParentKey + (uintptr_t)prParentKey->u32FirstSubKeyOffset);
      while (prCur->u32NextKeyOffset != 0)
      {
         prCur = (trRegKey*)((uintptr_t)prCur + (uintptr_t)prCur->u32NextKeyOffset);
      }
      prCur->u32NextKeyOffset = (uintptr_t)(prSubKey) - (uintptr_t)prCur;
   }
}

/******************************************************************************
 * FUNCTION:     vAddValue
 *
 * DESCRIPTION:  Add a value to the value list of a key.
 *
 * PARAMETER:    prKey:    (->I)
 *                  Pointer to the key to which the value is to be added.
 *
 *               prValue:  (->I)
 *                  Pointer to the value that ist to be added.
 *                  This value must not be linked to any key yet!
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tVoid vAddValue(trRegKey* prKey, trRegValue* prValue)
{
   prValue->u32NextOffset = prKey->u32FirstValueOffset;
   prKey->u32FirstValueOffset = (uintptr_t)prValue - (uintptr_t)prKey;
}

/******************************************************************************
 * FUNCTION:     vPathPartInit
 *
 * DESCRIPTION:  Initialise a trPathPartInfo structure for subsequent calls to
 *               szNextPathPart.
 *
 * PARAMETER:    prInfo:    (->I)
 *                  Pointer to the trPathPartInfo structure that is to be
 *                  initialised.
 *
 *               coszName:  (->I)
 *                  Path name that is to be used to initialise the
 *                  structure.
 *
 * RETURNVALUE:  -
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tVoid vPathPartInit(trPathPartInfo* prInfo, tCString coszName)
{
   if (coszName == OSAL_NULL)
   {
      coszName = "";
   }
   (void)OSAL_szStringCopy(prInfo->szBuf, coszName);
   prInfo->u32BufLen = OSAL_u32StringLength(coszName);
   prInfo->pcNextPos = prInfo->szBuf;
}

/******************************************************************************
 * FUNCTION:     szNextPathPart
 *
 * DESCRIPTION:  Retrieve the next part of a file path from the trPathPartInfo
 *               structure.
 *
 *               Path parts are separated from each other by "/" or "\"
 *               characters.
 *
 *               Before this function is called for the first time for a
 *               given path, the trPathPartInfo structure has to be
 *               initialised with a call to vPathPartInit().
 *
 *               After the last of the path name components has been returned,
 *               subsequent calls will return OSAL_NULL.
 *
 * PARAMETER:    prInfo:    (->I)
 *                  Pointer to the trPathPartInfo structure to determine the
 *                  next pathname part.
 *
 *
 * RETURNVALUE:  Either a pointer to the next part of the path name, or
 *               OSAL_NULL if all parts have already been returned.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tCString szNextPathPart(trPathPartInfo* prInfo)
{
   tChar* pcPos = prInfo->pcNextPos;
   tCString szRet = (tCString)pcPos;
   
   if (pcPos != &(prInfo->szBuf[prInfo->u32BufLen]))
   {
      while ((*pcPos != '\0') && (*pcPos != '/') && (*pcPos != '\\'))
      {
         ++pcPos;
      }
      
      if ((*pcPos == '/') || (*pcPos == '\\'))
      {
         *pcPos = '\0';
         ++pcPos;
      }
      
      prInfo->pcNextPos = pcPos;
   }
   else
   {
      szRet = NULL;
   }
   return szRet;
}

/******************************************************************************
 * FUNCTION:     prGetKeyByName
 *
 * DESCRIPTION:  Search a subkey of a key by name.
 *
 *               Only the immediate children of the key are searched.
 *
 * PARAMETER:    prRoot:    (->I)
 *                  Pointer to the key whose subkeys are to be searched.
 *
 *               coszName:  (->I)
 *                  The name of the subkey that is to be found.
 *
 * RETURNVALUE:  A pointer to the corresponding subkey, or OSAL_NULL if no
 *               such subkey exists.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static trRegKey* prGetKeyByName(const trRegKey* prRoot, tCString coszName)
{
   trRegKey* prCur = (trRegKey*)((uintptr_t)prRoot + prRoot->u32FirstSubKeyOffset);
   
   if(prRoot->u32FirstSubKeyOffset == 0)
   {
       return OSAL_NULL;
   }

   while ((prCur->u32NextKeyOffset != 0) &&
          (s32StrICmp(coszName, (tCString)((uintptr_t)prCur + prCur->u32szNameOffset)) != 0))
   {
     prCur = (trRegKey*)((uintptr_t)prCur + (uintptr_t)prCur->u32NextKeyOffset);
   }
   if((s32StrICmp(coszName, (tCString)((uintptr_t)prCur + prCur->u32szNameOffset)) != 0))prCur = OSAL_NULL;
   return prCur;
}

/******************************************************************************
 * FUNCTION:     prGetKeyByPath
 *
 * DESCRIPTION:  Search a subkey of a key by path name.
 *
 *               The subkey hierarchy is descended following the parts of
 *               the path name.
 *
 * PARAMETER:    prRoot:    (->I)
 *                  Pointer to the key whose subkeys are to be searched.
 *
 *               coszName:  (->I)
 *                  The path to the subkey that is to be found.
 *
 * RETURNVALUE:  A pointer to the corresponding subkey, or OSAL_NULL if no
 *               such subkey exists.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static trRegKey* prGetKeyByPath(trRegKey* prRoot, tCString coszName)
{
   trRegKey* prCur = prRoot;
   tCString coszKeyName;

   vPathPartInit(&rPathPart, coszName);
   coszKeyName = szNextPathPart(&rPathPart);
  
   if(coszKeyName == OSAL_NULL)prCur = OSAL_NULL;

   while ((prCur != OSAL_NULL) && (coszKeyName != OSAL_NULL))
   {
      prCur = prGetKeyByName(prCur, coszKeyName);
      coszKeyName = szNextPathPart(&rPathPart);
   }

   return prCur;
}

/******************************************************************************
 * FUNCTION:     prGetValueByName
 *
 * DESCRIPTION:  Search a value of a key by name.
 *
 * PARAMETER:    prRoot:    (->I)
 *                  Pointer to the key whose values are to be searched.
 *
 *               coszName:  (->I)
 *                  The name of the value that is to be found.
 *
 * RETURNVALUE:  A pointer to the corresponding value, or OSAL_NULL if no
 *               such value exists.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static trRegValue* prGetValueByName(const trRegKey* prRoot, tCString coszName)
{
   trRegValue* prCur = (trRegValue*)((uintptr_t)prRoot + prRoot->u32FirstValueOffset);
 
   if (prCur == (trRegValue*)prRoot)/*lint !e1773 */
   {
     if((s32StrICmp(coszName, (tCString)((uintptr_t)prCur + prCur->u32szNameOffset))) == 0) return prCur;
   }

   if(prRoot->u32FirstValueOffset == 0) return OSAL_NULL;

   while ((prCur->u32NextOffset != 0) &&
          (s32StrICmp(coszName, (tCString)((uintptr_t)prCur + prCur->u32szNameOffset)) != 0))
   {
      prCur = (trRegValue*)((uintptr_t)prRoot + prCur->u32NextOffset);
   }
   if((s32StrICmp(coszName, (tCString)((uintptr_t)prCur + prCur->u32szNameOffset)) != 0))prCur = OSAL_NULL;
   return prCur;
}


/******************************************************************************
 * FUNCTION:     u32SetValue
 *
 * DESCRIPTION:  Set a value of the registry.
 *
 * PARAMETER:    prKey:     (->I)
 *                  Pointer to the key where the value is to be set.
 *
 *               coszName:  (->I)
 *                  The name of the value that is to be set.
 *
 *               pu8Value:  (->I)
 *                  The data for the value
 *
 *               s32Type:   (I)
 *                  The type of the value
 *
 *               enAccess:  (I)
 *                  Access mode of the key.
 *
 * RETURNVALUE:  An OSAL error code describing the success or failure of
 *               the operation.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 11.07.03  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tU32 u32SetValue(trRegKey* prKey, tCString coszName, tU8* pu8Value, tS32 s32Type, OSAL_tenAccess enAccess)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;

   if ((enAccess == OSAL_EN_READWRITE) || (enAccess == OSAL_EN_WRITEONLY))
   {
      trRegValue* prVal = OSAL_NULL;
            
      u32ErrorCode = OSAL_E_NOSPACE;

      if (s32Type == OSAL_C_S32_VALUE_S32)
      {
         tVoid* ps32Temp = pu8Value;

         prVal = prAllocNumericValue(coszName, *(tS32 *)ps32Temp);
      }
      else if (s32Type == OSAL_C_S32_VALUE_STRING)
      {
         prVal = prAllocStringValue(coszName, (tCString)(pu8Value));
      }
      else
      {
         u32ErrorCode = OSAL_E_INVALIDVALUE;
      }

      if (prVal != OSAL_NULL)
      {
         tBool bApp = FALSE;
         tBool bSrv = FALSE;
         trRegValue* prOldVal = prGetValueByName(prKey, coszName);

         if (prOldVal != OSAL_NULL)
         {
            if(LLD_bIsTraceActive(TR_COMP_OSALIO,TR_LEVEL_WARNING))
            {
               TraceString("OSALIO Registry PID:%d vReplaceValue %s",OSAL_ProcessWhoAmI(),coszName);
            }
            vReplaceValue(prOldVal, prVal, prKey);
         }
         else
         {
            vAddValue(prKey, prVal);
         }

         // fill lookup tables
         if ((s32Type == OSAL_C_S32_VALUE_S32) && (s32StrICmp(coszName, REGVALUE_APPID) == 0))
         {
            bApp = TRUE;
         } 
         else if ((s32Type == OSAL_C_S32_VALUE_S32) && (s32StrICmp(coszName, REGVALUE_SERVICEID) == 0))
         {
            bSrv = TRUE;
         }

         if ((bApp == TRUE) || (bSrv == TRUE))
         {
            tChar szBuf[OSAL_C_U32_MAX_PATHLENGTH] = ""; /* Contains a pathname */
            tChar szTempBuf[OSAL_C_U32_MAX_PATHLENGTH] = ""; /* Contains a pathname */
            tChar* pszBuf = szBuf;
            tChar* pszTempBuf = szTempBuf;
            tChar* pszTempXBuf;
            tBool bFirst = TRUE;

            // calculate complete path
            while ((prKey != NULL) && (prKey != prRegistry))
            {
               pszTempBuf[0] = 0;
               if (bFirst)
               {
                  bFirst = FALSE;
                  (tVoid)OSAL_szStringNConcat(pszBuf, (tCString)((uintptr_t)prKey + prKey->u32szNameOffset), OSAL_C_U32_MAX_PATHLENGTH-1);
               }
               else
               {
                  (tVoid)OSAL_szStringNConcat(pszTempBuf, (tCString)((uintptr_t)prKey + prKey->u32szNameOffset), OSAL_C_U32_MAX_PATHLENGTH-1);
                  (tVoid)OSAL_szStringNConcat(pszTempBuf, "/", OSAL_C_U32_MAX_PATHLENGTH-1);
                  (tVoid)OSAL_szStringNConcat(pszTempBuf, pszBuf, OSAL_C_U32_MAX_PATHLENGTH-1);
                  pszTempXBuf = pszBuf;
                  pszBuf = pszTempBuf;
                  pszTempBuf = pszTempXBuf;
               }

               prKey = (trRegKey*)((uintptr_t)prKey - prKey->u32ParentKeyOffset);
            }
            if (bApp == TRUE)
            {
               tU32 n;
               // TraceOut(TR_LEVEL_SYSTEM_MIN, "set AppId %d in %s %d", prVal->uData.s32NumValue, pszBuf, REGISTRY_SUM_SIZE);

               n = pAdd2Map(prMapAdmin->u32LastAppMapElement, pszBuf, prVal->uData.s32NumValue);
               if (prMapAdmin->u32FirstAppMapElement == REGISTRY_START_VALUE)
               {
                  prMapAdmin->u32FirstAppMapElement = n;
               }
               prMapAdmin->u32LastAppMapElement = n;
            }
            else
            {
               tU32 n;
               // TraceOut(TR_LEVEL_SYSTEM_MIN, "set SrvId %d in %s (%d)", prVal->uData.s32NumValue, pszBuf, REGISTRY_SUM_SIZE);

               n = pAdd2Map(prMapAdmin->u32LastSrvMapElement, pszBuf, prVal->uData.s32NumValue);
               if (prMapAdmin->u32FirstSrvMapElement == REGISTRY_START_VALUE)
               {
                  prMapAdmin->u32FirstSrvMapElement = n;
               }
               prMapAdmin->u32LastSrvMapElement = n;
            }
         }
         u32ErrorCode = OSAL_E_NOERROR;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_NOACCESS;
   }
   return u32ErrorCode;
}

/******************************************************************************
 * FUNCTION:     u32GetValue
 *
 * DESCRIPTION:  Get a value of the registry.
 *
 * PARAMETER:    prKey:     (->I)
 *                  Pointer to the key where the value is located.
 *
 *               coszName:  (->I)
 *                  The name of the value that is to be queried.
 *
 *               pu8Value:  (->O)
 *                  This buffer will receive the data of the value.
 *
 *               u32Size:   (I)
 *                  The size of the buffer.
 *
 *               ps32Type:  (->I)
 *                  On successful execution, this variable will contain the
 *                  type of the value data.
 *
 *               enAccess:  (I)
 *                  Access mode of the key.
 *
 * RETURNVALUE:  An OSAL error code describing the success or failure of
 *               the operation.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 11.07.03  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
static tU32 u32GetValue(const trRegKey* prKey, tCString coszName, tU8* pu8Value,
            tU32 u32Size, tS32* ps32Type, OSAL_tenAccess enAccess)
{
   tU32 u32ErrorCode = OSAL_E_NOERROR;

   if ((enAccess == OSAL_EN_READWRITE) || (enAccess == OSAL_EN_READONLY))
   {
       trRegValue* prVal = prGetValueByName(prKey, coszName);

       if (prVal != OSAL_NULL)
       {
          if (prVal->s32Type == OSAL_C_S32_VALUE_S32)
          {
             if (u32Size >= sizeof(tS32))
             {
               tVoid* ps32Temp;
               ps32Temp = pu8Value;
               *ps32Type = OSAL_C_S32_VALUE_S32;
               *(tS32 *)ps32Temp = prVal->uData.s32NumValue;
             }
             else
             {
                u32ErrorCode = OSAL_E_NOSPACE;
             }
          }
          else if (prVal->s32Type == OSAL_C_S32_VALUE_STRING)
          {
             if ((u32Size > 0)&&(prVal->uData.u32StringValueOffset != 0))
             {
                *ps32Type = OSAL_C_S32_VALUE_STRING;
                (void)OSAL_szStringNCopy((tString)pu8Value,
                                         (tCString)((uintptr_t)prVal + prVal->uData.u32StringValueOffset),
                                         u32Size);
                pu8Value[u32Size-1] = '\0';
             }
             else
             {
                u32ErrorCode = OSAL_E_NOSPACE;
             }
          }
          else
          {
             u32ErrorCode = OSAL_E_INVALIDVALUE;
          }
       }
       else
       {
          u32ErrorCode = OSAL_E_DOESNOTEXIST;
       }
   }
   else
   {
      u32ErrorCode = OSAL_E_NOACCESS;
   }
   return u32ErrorCode;
}

/************************************************************************
|function implementation (scope: global)
|-----------------------------------------------------------------------*/

/******************************************************************************
 * FUNCTION:      RegistryInit()
 *
 * DESCRIPTION:   This is a Internal function 
 *                
 *                This function initialize the OS level of the Registry
 *                device.
 * PARAMETER:
 *
 *                nil
 *
 * RETURNVALUE:   s32ErrorCode
 *                 it is the function return value: 
 *                 - OSAL_OK
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig 
 *****************************************************************************/
tS32 s32RegistryInit(tVoid)
{
   tS32 s32ReturnValue = OSAL_ERROR;
   char *buffer = NULL;
   pOsalData->u32RegistryMemSize = REGISTRY_SUM_SIZE;
   
   /* export OSAL_REGISTRY_SIZE=650000   */
   buffer = getenv("OSAL_REGISTRY_SIZE");
   if (buffer)
   {
      errno = 0;
      pOsalData->u32RegistryMemSize = strtol(buffer, NULL, 0);
      if(errno)
      {
            pOsalData->u32RegistryMemSize = REGISTRY_SUM_SIZE;
      }
      TraceString("Set OSAL Registry size to %d Bytes",pOsalData->u32RegistryMemSize);
   }
   pOsalData->u32RegistryLookUpMemSize = (sizeof(trRegMapAdmin) + REGISTRY_MAX_LOOKUP_SIZE);
 
   hRegShMemHandle = shm_open("/OSAL-Registry", O_EXCL|O_RDWR|O_CREAT, OSAL_ACCESS_RIGTHS);
   if(hRegShMemHandle  == (OSAL_tShMemHandle)-1)
   {
      abort();
   }
   if(s32OsalGroupId)
   {
      /* adapt rights for shared memory */
      if(fchown(hRegShMemHandle,(uid_t)-1,s32OsalGroupId) == -1)
      {
         vWritePrintfErrmem("s32RegistryInit shm_open -> fchown error %d \n",errno);
      }
      // umask (022) is overwriting the permissions on creation, so we have to set it again
      if(fchmod(hRegShMemHandle, OSAL_ACCESS_RIGTHS) == -1)
      {
         vWritePrintfErrmem("s32RegistryInit shm_open -> fchmod error %d \n",errno);
      }
   }
   if (ftruncate(hRegShMemHandle, pOsalData->u32RegistryMemSize) == -1)
   {
      abort();
   }
   /* map shared memory into address space */
   pvRegMem = mmap(0x00000000L,pOsalData->u32RegistryMemSize, PROT_READ | PROT_WRITE,GLOBAL_DATA_OPTION,hRegShMemHandle,0);
   if(pvRegMem != NULL)
   {
      ((trAddress*)pvRegMem)->u32UsedSize = sizeof(trAddress);
      ((trAddress*)pvRegMem)->prLastAdMin = 0;
      prMapAdmin  = (trRegMapAdmin*)((uintptr_t)pvRegMem + REGISTRY_MEM_SIZE);/*lint -e124*/
      prMapAdmin->u32FirstAppMapElement = REGISTRY_START_VALUE;
      prMapAdmin->u32FirstSrvMapElement = REGISTRY_START_VALUE;
      prMapAdmin->u32LastAppMapElement  = REGISTRY_START_VALUE;
      prMapAdmin->u32LastSrvMapElement  = REGISTRY_START_VALUE;
      prMapAdmin->u32LookupCount        = 0;
      pvLookupMem = (void*)((uintptr_t)pvRegMem + REGISTRY_MEM_SIZE + sizeof(trRegMapAdmin));/*lint -e124*/
      prRegistry = prAllocKey("registry");
   }
   else
   {
      FATAL_M_ASSERT_ALWAYS();
   }
   if (prRegistry != OSAL_NULL)
   {
         s32ReturnValue = OSAL_OK;
   }
   return s32ReturnValue;
}

tS32 s32RegistryConnect(tVoid)
{
   if(pvRegMem == 0)
   {
      tS32 s32ReturnValue = OSAL_ERROR;
      if((hRegShMemHandle = shm_open("/OSAL-Registry", O_RDWR ,0))  == (OSAL_tShMemHandle)-1)
      {
          FATAL_M_ASSERT_ALWAYS();
      }
      /* map shared memory into address space */
      pvRegMem = mmap(0x00000000L,pOsalData->u32RegistryMemSize, PROT_READ | PROT_WRITE,GLOBAL_DATA_OPTION,hRegShMemHandle,0);
      if(pvRegMem != NULL)
      {
         prRegistry = (trRegKey*)((uintptr_t)pvRegMem +sizeof(trAddress) + sizeof(trMemAdMin));
         pvLookupMem = (void*)((uintptr_t)pvRegMem + REGISTRY_MEM_SIZE + sizeof(trRegMapAdmin));/*lint -e124*/
         prMapAdmin  = (trRegMapAdmin*)((uintptr_t)pvRegMem + REGISTRY_MEM_SIZE);/*lint -e124*/
         s32ReturnValue = OSAL_OK;
      }
      else
      {
         FATAL_M_ASSERT_ALWAYS();
      }
      return s32ReturnValue;
   }
   else
   {
      return OSAL_OK;
   }
}

/******************************************************************************
 * FUNCTION:      RegistryDeInit()
 *
 * DESCRIPTION:   This is a Internal function 
 *                
 *                This function deinitialize the OS level of the Registry
 *                device.
 * PARAMETER:
 *
 *                nil
 *
 * RETURNVALUE:   s32ErrorCode
 *                 it is the function return value: 
 *                 - OSAL_OK
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig 
 *****************************************************************************/
tS32 s32RegistryDeInit(void)
{
  tS32 s32ReturnValue = OSAL_OK;
  if(pvRegMem != 0)
  {
    s32ReturnValue = munmap(pvRegMem,pOsalData->u32RegistryMemSize);
    s32ReturnValue = close(hRegShMemHandle);
  }
  return s32ReturnValue;
}

tS32 s32RegistryMemUnlink(void)
{
    return shm_unlink("/OSAL-Registry");
}


/******************************************************************************
 * FUNCTION:      REGISTRY_u32IOClose
 *
 * DESCRIPTION:   This function closes a key and release the handle. 
 *                In case the key does not exist, an error is returned.
 *                ...
 * PARAMETER:     
 *
 *   OSAL_tIODescriptor fd   descriptor of file;
 *
 * RETURNVALUE:   u32ErrorCode
 *                 it is the function return value: 
 *                 - OSAL_E_NOERROR if everything goes right;
 *                  - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig
 *****************************************************************************/
tU32 REG_u32IOClose(uintptr_t u32fd)
{
   tU32  u32ErrorCode = OSAL_E_NOERROR;
   trRegIO* pIO;
   if (!u32fd)
   {
      u32ErrorCode = OSAL_E_BADFILEDESCRIPTOR;
   }
   else
   {
     pIO = (trRegIO*)u32fd;

     if (pIO->prKey->u32OpenCount > 0)
     {
         pIO->prKey->u32OpenCount--;
     }
     // Changed by Resch to malloc
     // vFreeMemory((tPVoid)u32fd);
     free((tPVoid)u32fd);
   }
   return(u32ErrorCode);
}

tU32 REGISTRY_u32IOClose(uintptr_t u32fd)
{
   tU32  u32ErrorCode = OSAL_E_NOERROR;
   if(bLockRegistry() == TRUE)
   {
      u32ErrorCode = REG_u32IOClose(u32fd);
     NORMAL_M_ASSERT(bUnLockRegistry());
   }
   else
   {
     NORMAL_M_ASSERT_ALWAYS();
   }
   return(u32ErrorCode);
}



/******************************************************************************
 * FUNCTION:      REGISTRY_u32IOCreate
 *
 * DESCRIPTION:   This function creates a KEY on REGISTRY with
 *                the specified options.
 *                In case the key name  does not exist, a key is created.
 *
 * PARAMETER:     
 *
 *   tCString        coszName:    file name;
 *   OSAL_tenAccess  enAccess:  options.
 *
 * RETURNVALUE:   ReturnValue
 *                 it is the function return value:
 *                 - OSAL_tIODescriptor if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 *
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig 
 *****************************************************************************/
tU32 REG_u32IOCreate(tCString coszName, OSAL_tenAccess enAccess, uintptr_t *pu32RetAddr)
{
   tU32  u32ErrorCode = OSAL_E_UNKNOWN;
   if( coszName != NULL)
   {
         if ((enAccess == OSAL_EN_READWRITE) ||
             (enAccess == OSAL_EN_READONLY) ||
             (enAccess == OSAL_EN_WRITEONLY))
         {
            trRegKey* prKey = prGetKeyByPath(prRegistry, coszName);

            /* Key does not exist yet */
            if (prKey == OSAL_NULL)
            {
               tChar szBuf[OSAL_C_U32_MAX_PATHLENGTH];
               tUInt nLen = OSAL_u32StringLength(coszName);
               tUInt nPos = nLen;

               /* Split the path name of the new key into the path name of the
                * parent key and "file" name of the new key.
                * Afterwards, the path name of the parent key begins at buf[0],
                * and the name of the new key begins at buf[nPos]. */
               (void)OSAL_szStringCopy(szBuf, coszName);
               while ((nPos > 0) && (szBuf[nPos] != '/') && (szBuf[nPos] != '\\'))
               {
                  --nPos;
               }

               if (nPos > 0)
               {
                  szBuf[nPos] = '\0';
                  ++nPos;
               }

               if (nPos < nLen)
               {
                  if (nPos > 0)
                  {
                     prKey = prGetKeyByPath(prRegistry, szBuf);
                  }
                  else
                  {
                     prKey = prRegistry;
                  }
                  
                  /* parent key does exist */
                  if (prKey != OSAL_NULL)
                  {
                     trRegKey* prNewKey = prAllocKey(&szBuf[nPos]);
                     if (prNewKey != OSAL_NULL)
                     {
                        // Changed by Resch to malloc
                        // trRegIO* prIO = (trRegIO*)pvGetMemory(sizeof(trRegIO));
                        trRegIO* prIO = (trRegIO*) malloc(sizeof(trRegIO));
                        if (prIO != OSAL_NULL)
                        {
                           u32ErrorCode = OSAL_E_NOERROR;
                           vAddKey(prKey, prNewKey);
                           prNewKey->u32OpenCount++;
                           prIO->enAccess = enAccess;
                           prIO->prKey = prNewKey;

                           *pu32RetAddr = (uintptr_t)prIO;
                        }
                        else
                        {
                           u32ErrorCode = OSAL_E_NOFILEDESCRIPTOR;
                           vFreeMemory(prNewKey);
                        }
                     }
                     else
                     {
                        u32ErrorCode = OSAL_E_NOSPACE;
                     }
                  }
                  else
                  {
                     u32ErrorCode = OSAL_E_DOESNOTEXIST;
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_UNKNOWN;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_ALREADYEXISTS;
            }
         }
         else
         {
            u32ErrorCode = OSAL_E_NOACCESS;
         }
   }
   else
   {
      u32ErrorCode = OSAL_E_ALREADYEXISTS;
   }

   return(u32ErrorCode);
}


tU32 REGISTRY_u32IOCreate(tCString coszName, OSAL_tenAccess enAccess, tCString szDrive, uintptr_t *pu32RetAddr)
{
   tU32  u32ErrorCode = OSAL_E_UNKNOWN;

   /* added to satisfy linker */
   (tVoid)(szDrive); /* satisfy Lint */
   if(bLockRegistry() == TRUE)
   {
      u32ErrorCode = REG_u32IOCreate(coszName, enAccess, pu32RetAddr);
      NORMAL_M_ASSERT(bUnLockRegistry());
   }
   else
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
    
   return(u32ErrorCode);
}



/******************************************************************************
 * FUNCTION:      REGISTRY_u32IOOpen
 *
 * DESCRIPTION:   This function opens a KEY on REGISTRY with 
 *                the specified options. 
 *                In case the key name does not exist, an error is reported.
 *
 * PARAMETER:     
 *
 *   tCString        coszName:    file name;
 *   OSAL_tenAccess  enAccess:  options.
 *
 * RETURNVALUE:   ReturnValue
 *                 it is the function return value: 
 *                 - OSAL_tIODescriptor if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 *
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 14.11.02  |   Initial revision                     | CM-CR/EES4-Grosse Pawig 
 *****************************************************************************/
tU32 REG_u32IOOpen(tCString coszName, OSAL_tenAccess enAccess,uintptr_t* pu32RetAddr)
{
   tU32  u32ErrorCode = OSAL_E_UNKNOWN;
   if ((enAccess == OSAL_EN_READWRITE) ||
       (enAccess == OSAL_EN_READONLY) ||
       (enAccess == OSAL_EN_WRITEONLY))
   {
      trRegKey* prKey = OSAL_NULL;

      if( coszName == OSAL_NULL)
      {
         prKey = prRegistry;
      }
      else
      {
         prKey = prGetKeyByPath(prRegistry, coszName);
      }
      if ((prKey != OSAL_NULL)&&(prKey->u32szNameOffset != 0))
      {
         // Changed by Resch to malloc
         // trRegIO* prIO = (trRegIO*)pvGetMemory(sizeof(trRegIO));
         trRegIO* prIO = (trRegIO*) malloc(sizeof(trRegIO));
         if (prIO != OSAL_NULL)
         {
            prIO->enAccess = enAccess;
            prIO->prKey    = prKey;

            prKey->u32OpenCount++;

            *pu32RetAddr = (uintptr_t)prIO;
            u32ErrorCode = OSAL_E_NOERROR;
         }
         else
         {
            u32ErrorCode = OSAL_E_NOFILEDESCRIPTOR;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_DOESNOTEXIST;
      }
   }
   else
   {
      u32ErrorCode = OSAL_E_NOACCESS;
   }
   return u32ErrorCode;
}

tU32 REGISTRY_u32IOOpen(tCString coszName, OSAL_tenAccess enAccess, tCString szDrive, uintptr_t* pu32RetAddr)
{
   tU32  u32ErrorCode = OSAL_E_UNKNOWN;

   (tVoid)(szDrive); /* satisfy Lint */

   if(bLockRegistry() == TRUE)
   {
      if(s32RegistryConnect() == OSAL_OK)
      {
         u32ErrorCode = REG_u32IOOpen(coszName, enAccess,pu32RetAddr);
      }
      NORMAL_M_ASSERT(bUnLockRegistry());
   }
   else
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
   return(u32ErrorCode);
}


tBool bBuildupRegistryLockless(char* pcPtr,tU32 u32Size)
{
    OSAL_trIOCtrlRegistry registry;
    OSAL_tIODescriptor fd = 0;
    tBool bRet = TRUE;
    tChar acPathBuffer[256];  // used for the keys found in the registry file
    tChar *acReadBuf = pcPtr;  // the pointer to the file buffer
    tS32  s32FileSize = u32Size;        // size of the registry file
    tU32  u32PathStart = 0;       // length of the base path used ever and ever again
    tU32  u32State;           // state of the parser

    tU32  u32Value = 0;           // numerical buffer
    tChar* ptr = NULL;               // pointer to path
    tChar* ptrName = NULL;           // pointer to name
    tChar* ptrStringValue = NULL;    // pointer to string value

    OSAL_szStringCopy(acPathBuffer, REG_BASEPOSITION);
    u32PathStart = OSAL_u32StringLength(acPathBuffer); // End of default path, start appending here

    u32State = 0; // start looking for terminal symbols.
    while (s32FileSize > 0)
    {
        tChar c = *acReadBuf++; // get the next character from file buffer

        switch(u32State)
        {
        case 1: // comment mode
            if (c == 0x0a) // line end symbol found?
            {
                u32State = 0; // goto new terminal symbol acceptance mode
            }
            break;

        case 2: // key extraction mode, skip HKEY_LOCAL_MACHINE
            if (c == '\\') // skip 
            {
                u32State = 6; // key extraction mode start
            }
            break;
        case 6: // key extraction mode, gather symbols
            /* for missing ] at end of entry*/
            if (c == '\n')
            {
                TraceString("Registry file corrupt, please check %s",acPathBuffer);
                vWritePrintfErrmem("Registry file corrupt, please check %s\n",acPathBuffer);
                return FALSE;
            }
            if (c == ']') // end of key found?
            {
                if(ptr)*ptr = 0; // set an end to the buffer
                else NORMAL_M_ASSERT_ALWAYS();
                if(REG_u32IOCreate(acPathBuffer,OSAL_EN_READWRITE,(uintptr_t*)&fd) == OSAL_E_NOERROR)
//                fd = OSAL_IOCreate(acPathBuffer, OSAL_EN_READWRITE);
//                if (fd != OSAL_ERROR)
                {
                    REG_u32IOClose(fd);
//                    OSAL_s32IOClose(fd);
                }
                u32State = 0; // back to terminal symbol recognition
            }
            else
            {
                // store a new character in the buffer
                // change all backslashes to slashes
                if(ptr)*ptr++ = c == '\\' ? '/' : c;
                else NORMAL_M_ASSERT_ALWAYS();
            }
            break;

        case 3: // name extraction mode
            if (c == '\"') // end of name found?
            {
                acReadBuf[-1] = 0; // set an ending to the name
                u32State = 7; // search for values
            }
            break;
        case 7: // search for terminal symbol " or : to extract string or numerical value
            if (c == '\"') // start string evaluation
            {
                u32State = 4;
                ptrStringValue = acReadBuf;
            }
            else if (c == ':') // start numerical evaluation
            {
                u32State = 5;
                u32Value = 0; // initialize numerical value
            }
            break;
        case 4: // string value extraction mode
            if (c == '\"')
            {
                acReadBuf[-1] = 0; // set an ending to the stringvalue
                if(REG_u32IOOpen(acPathBuffer, OSAL_EN_READWRITE,(uintptr_t*)&fd) != OSAL_E_NOERROR) return FALSE;
                //fd = OSAL_IOOpen(acPathBuffer, OSAL_EN_READWRITE);
                //if(fd == OSAL_ERROR) return FALSE;

                registry.s32Type = OSAL_C_S32_VALUE_STRING;
                registry.pcos8Name = (tPCS8)ptrName;
                registry.ps8Value = (tPU8) ptrStringValue; // Prima, ps8Value ist in Wirklichkeit ein tPU8
                if(ptrStringValue)registry.u32Size = acReadBuf - ptrStringValue -1;
                else NORMAL_M_ASSERT_ALWAYS();
                if(REG_u32IOControl(fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t) &registry) != OSAL_E_NOERROR)
//                if (OSAL_ERROR == OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32) &registry))
                {
                     bRet = FALSE;
                }
//                OSAL_s32IOClose(fd);
                REG_u32IOClose(fd);
                u32State = 0; // start checking for terminal symbols
            }
            break;
        case 5: // numerical value extraction mode
            if (!OSAL_s32IsHexDigit(c)) // value ends here
            {
                // end of number detected
                if(REG_u32IOOpen(acPathBuffer, OSAL_EN_READWRITE,(uintptr_t*)&fd) != OSAL_E_NOERROR) return FALSE;
//                fd = OSAL_IOOpen(acPathBuffer, OSAL_EN_READWRITE);
//                if(fd == OSAL_ERROR) return FALSE;

                registry.s32Type = OSAL_C_S32_VALUE_S32;
                registry.pcos8Name = (tPCS8)ptrName;
                registry.ps8Value = (tPU8) &u32Value; // Prima, ps8Value ist in Wirklichkeit ein tPU8
                registry.u32Size = sizeof(u32Value);
                if(REG_u32IOControl(fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t) &registry) != OSAL_E_NOERROR)
//                if (OSAL_OK == OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32) &registry))
                {
                    bRet = FALSE;
                }
                REG_u32IOClose(fd);
//                OSAL_s32IOClose(fd);
                u32State = 0; // start checking for terminal symbols
            }
            else
            {
                // make place for the next digit
                u32Value <<= 4;
                // we know here that a hex digit is available so
                // we use a simplified range check here
                u32Value |= (c <= '9') ? (c - '0') :
                            (c <= 'F') ? (c + 10-'A') :
                            (c + 10-'a');
            }
            break;
        case 0: // in no state: detect next terminal symbol
        default:
            switch(c)
            {
            case ';':
                u32State = 1; // switch comment mode on
                break;
            case '[':
                u32State = 2; // key evaluation path mode
                acPathBuffer[u32PathStart] = 0; // set up a new path for key evaluation
                ptr = &acPathBuffer[u32PathStart]; // set ptr to path buffer
                break;
            case '\"':
                u32State = 3; // name evaluation mode
                ptrName = acReadBuf; // store start of name
                break;
            default:
                // no terminal symbol recognized, search for next
                break;
            }
        }
        --s32FileSize;          // decrement the amount of characters to read
    }

    // it can be that a numerical values directly ends with the last character in the file
    // check this here and put it the numerical value into the registry
    if (u32State == 5)
    {
       if(REG_u32IOOpen(acPathBuffer, OSAL_EN_READWRITE,(uintptr_t*)&fd) != OSAL_E_NOERROR) return FALSE;
//       fd = OSAL_IOOpen(acPathBuffer, OSAL_EN_READWRITE);
//       if(fd == OSAL_ERROR) return FALSE;

       registry.s32Type = OSAL_C_S32_VALUE_S32;
       registry.pcos8Name = (tPCS8)ptrName;
       registry.ps8Value = (tPU8) &u32Value; // Prima, ps8Value ist in Wirklichkeit ein tPU8
       registry.u32Size = sizeof(u32Value);
       if(REG_u32IOControl(fd,OSAL_C_S32_IOCTRL_REGSETVALUE,(uintptr_t) &registry) != OSAL_E_NOERROR)
//       if (OSAL_OK == OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_REGSETVALUE, (tS32) &registry))
       {
          bRet = FALSE;
       }
       REG_u32IOClose(fd);
//       OSAL_s32IOClose(fd);
    }

   return bRet;
}


tBool bCreateRegFromFile(char* szFileName)
{
   tU32 u32Size = 0;
   tS8* pcReadBuf = 0;
   tBool bRet = TRUE;
   uintptr_t s32Ret = 0;
   OSAL_tIODescriptor hDevice = OSAL_IOOpen(szFileName, OSAL_EN_READONLY);
   if(hDevice == OSAL_ERROR)
   {
  //    TraceString("Registry -> OSAL_IOOpen %s failed errno:%d\n",szFileName,errno);
      if(errno == EPERM)
      {
         TraceString("Registry -> OSAL_IOOpen %s failed  -> EPERM errno:%d\n",szFileName,errno);
         vWritePrintfErrmem("Registry -> OSAL_IOOpen %s failed -> EPERM errno:%d\n",szFileName,errno);
      }
      bRet = FALSE;
   }
   else
   {
      s32Ret = ((OsalDeviceDescriptor*)hDevice)->local_ptr;
      u32Size = ((OSAL_tfdIO*)s32Ret)->u32Size;
      
//      TraceString("Registry %s with size %u",((OSAL_tfdIO*)s32Ret)->szName,u32Size);
      if(u32Size == 0xffffffff)
      {
          TraceString("Registry Error bCreateRegFromFile done for directory %s",((OSAL_tfdIO*)s32Ret)->szName);
          bRet = FALSE;
      }
      else if(u32Size == 0xefffffff)
      {
          TraceString("Registry Error bCreateRegFromFile done for device %s",((OSAL_tfdIO*)s32Ret)->szName);
          bRet = FALSE;
      }
      else
      {
         s32Ret = 0;
         if(OSAL_s32IOControl(hDevice,OSAL_C_S32_IOCTRL_FIOSEEK,s32Ret) == OSAL_ERROR)
         {
            NORMAL_M_ASSERT_ALWAYS();
            vWritePrintfErrmem("Registry -> bCreateRegFromFile errno:%d\n",errno);
         }
         pcReadBuf = (tS8 *)malloc(u32Size + 1);
         if(pcReadBuf == NULL)
         {
            bRet = FALSE;
         }
         else
         {
            if(OSAL_s32IORead( hDevice,pcReadBuf,u32Size) == OSAL_ERROR)
            { 
               NORMAL_M_ASSERT_ALWAYS();
               bRet = FALSE;
            }
            else
            {
                pcReadBuf[u32Size] = (tS8)EOF;
                bBuildupRegistryLockless((char*)pcReadBuf, u32Size);
            }
            free(pcReadBuf);
         }
      }
      OSAL_s32IOClose(hDevice);
   }
   return bRet;
}

tBool bCreateRegFromFileNativ(char* szFileName)
{
   tU32 u32Size = 0;
   tS8* pcReadBuf = 0;
   tBool bRet = FALSE;
   int fd = open(szFileName,  O_RDONLY|O_CLOEXEC, OSAL_FILE_ACCESS_RIGTHS);
   if(fd == -1)
   {
      TraceString("Registry -> open %s failed errno:%d\n",szFileName,errno);
      if(errno == EPERM)
      {
         TraceString("Registry -> open %s failed  -> EPERM errno:%d\n",szFileName,errno);
         vWritePrintfErrmem("Registry -> open %s failed -> EPERM errno:%d\n",szFileName,errno);
      }
   }
   else
   {
      struct stat rStat;
      if(fstat(fd,&rStat) != -1)
      {
         u32Size = rStat.st_size;
         pcReadBuf = (tS8 *)malloc(u32Size + 1);
         if(pcReadBuf)
         {
            if(read(fd,pcReadBuf,u32Size) == OSAL_ERROR)
            { 
               NORMAL_M_ASSERT_ALWAYS();
            }
            else
            {
                pcReadBuf[u32Size] = (tS8)EOF;
                bBuildupRegistryLockless((char*)pcReadBuf, u32Size);
                bRet = TRUE;
            }
            free(pcReadBuf);
        }
      }
      close(fd);
   }
   return bRet;
}


tBool bBuildErgReg(char* szFileName,tU32 u32Size,tU8* pu8Data)
{
   u32Size = pEncryptAesFile(szFileName,pu8Data,u32Size);
   if(u32Size > 0)
   {
       return bBuildupRegistryLockless((char*)pu8Data, u32Size);
   }
   else
   {
       NORMAL_M_ASSERT_ALWAYS();
       return FALSE;
   }
}

tBool bGetEnvData(tU32* pu32Size,char* szFileName)
{
   OSAL_tIODescriptor hDevice = OSAL_IOOpen(szFileName, OSAL_EN_READONLY);
   *pu32Size = OSALUTIL_s32FGetSize(hDevice);
   OSAL_s32IOClose ( hDevice );
   if(*pu32Size == 0)
   {
       NORMAL_M_ASSERT_ALWAYS();
       return FALSE;
   }
   if(pEncryptAesFile == NULL)
   {
      if(u32LoadSharedObject((void*)pEncryptAesFile,pOsalData->rLibrary[EN_CRYPTMODUL].cLibraryNames) != OSAL_E_NOERROR)/*lint !e611 */ /*otherwise linker warning */
      {
          NORMAL_M_ASSERT_ALWAYS();
          return FALSE;
      }
   }
   return TRUE;
}

tBool bCreateRegFromCryptedFile(char* szFileName)
{
   tBool bRet = FALSE;
   tU32 u32Size = 0;
   tPU8 pu8Data = NULL; // Buffer for decoded data

   if(bGetEnvData(&u32Size,szFileName))
   {
 //     if(u32Size)
      {
         u32Size = u32Size+64;
         pu8Data = (tPU8)malloc(u32Size);
         if(pu8Data)
         {
            bRet = bBuildErgReg(szFileName,u32Size,pu8Data);
            free(pu8Data);
         }
      }
   }
   return bRet;
}

tBool bFindErgKeyVal(OSAL_trErgReq* prErgReqeust, tU32 u32Size,tU8* pu8Data)
{
   tBool bRet = FALSE;
   u32Size = pEncryptAesFile(prErgReqeust->cErgFilePath,pu8Data,u32Size);
   if(u32Size > 0)
   {
      //"DATE"="2014-07-10 09:18:56"
      char *pTemp;
      char *pTemp2;
      /* skip dokumentation*/
      pTemp = strstr((char*)pu8Data,"HKEY_LOCAL_MACHINE");
      /* now loo k for requested key*/
      if(pTemp)
      {
          pTemp = strstr(pTemp,prErgReqeust->cKeyName);
      }
      if(pTemp != NULL)
      {
         /* key found , find value for key*/
         for(;;)
         {
            pTemp++;
            if(*pTemp == '=')
            {
              pTemp++;
              break; // =
            }
         }
         /* check for type*/
         if(*pTemp == 'd')
         {
            *(pTemp+9) = 0;
            prErgReqeust->u32ResultValue = (tU32)atoi(pTemp+5);
            prErgReqeust->u32ResultType = OSAL_C_S32_VALUE_S32;
            bRet = TRUE;
         }
         else if(*pTemp == '"')
         {
             pTemp2 = pTemp;
             for(;;)
             {
                pTemp2++;
                if(*pTemp2 == '"')
                {
                   pTemp2++;
                   *pTemp2 = 0;
                   break;
                }
             }
             strncpy(prErgReqeust->cResultString,pTemp,OSAL_C_U32_MAX_NAMELENGTH);
             prErgReqeust->u32ResultType = OSAL_C_S32_VALUE_STRING;
             bRet = TRUE;
         }
         else
         {
              prErgReqeust->u32ResultType = 0xffffffff;
         }
       }
       else
       {
          TraceString("Searched Registry Entry not found");
       }
   }
   else
   {
       NORMAL_M_ASSERT_ALWAYS();
   }
   return bRet;
}

tBool bValueOfCryptedFile(OSAL_trErgReq* prErgReqeust)
{
   tBool bRet = FALSE;
   tU32 u32Size = 0;
   tPU8 pu8Data = NULL; // Buffer for decoded data

   if(bGetEnvData(&u32Size,prErgReqeust->cErgFilePath))
   {
  //    if(u32Size)
      {
         u32Size = u32Size+64;
         pu8Data = (tPU8)malloc(u32Size);
         if(pu8Data)
         {
            bRet = bFindErgKeyVal(prErgReqeust,u32Size,pu8Data);
            free(pu8Data);
         }
      }
   }
   return bRet;
}


/******************************************************************************
 * FUNCTION:      REGISTRY_u32IOControl
 *
 * DESCRIPTION:  ToolSet of ioRoutines.
 * -FUNCTION:      IOCONTROL_Version
 *
 * -DESCRIPTION:  This function gets version of this device.
 * -FUNCTION:      IOCONTROL_SetValue
 *
 * -DESCRIPTION:   This function sets a Value on REGISTRY with 
 *                the specified options.
 *                In case the file does not exist, an error is returned.
 *
 * -FUNCTION:      IOCONTROL_GetValue
 *
 * -DESCRIPTION:  This function gets the payload of a Value on REGISTRY with 
 *                the specified options.
 *                In case the file does not exist, an error is returned.
 * -FUNCTION:      IOCONTROL_RegEnumValue
 *
 * -DESCRIPTION:  This function gets lists all the Values on a Key.
 * -FUNCTION:      IOCONTROL_RegReadDir
 *
 * -DESCRIPTION:  This function gets lists all the SubKeys on a Key.
 * -FUNCTION:      IOCONTROL_RemoveValue
 *
 * -DESCRIPTION:  This function gets version.
 *
 * PARAMETER:
 *
 *   OSAL_tIODescriptor u32fd   descriptor of file;
 *   tS32               fun     specific action;
 *   tS32               arg     pointer to tRegIO data transport structure 
 *
 * RETURNVALUE:   u32ErrorCode
 *                 it is the function return value: 
 *                 - OSAL_E_NOERROR if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 10.06.02  |   Initial revision                     |  dc
 * 26.07.02  |   Prefinal revision                    |  dc
 * 14.11.02  |   Adapted to non-FS operation          | CM-CR/EES4-Grosse Pawig 
 *****************************************************************************/
tU32 REG_u32IOControl(uintptr_t u32fd, tS32 fun, intptr_t arg)
{
   trRegIO* prIO = (trRegIO*)u32fd;
   tU32 u32ErrorCode = OSAL_E_UNKNOWN;
     switch(fun)
      {
      case OSAL_C_S32_IOCTRL_BUILD_REG:
         {
            char* pcName = (char*)arg;
            if(bCreateRegFromFile(pcName))
            {
                u32ErrorCode = OSAL_E_NOERROR;
            }
         }
         break;
      case OSAL_C_S32_IOCTRL_BUILD_ERG:
         {
            char* pcName = (char*)arg;
            if(bCreateRegFromCryptedFile(pcName))
            {
                u32ErrorCode = OSAL_E_NOERROR;
            }
         }
         break;
      case OSAL_C_S32_IOCTRL_GET_ERGVAL:
         {
            OSAL_trErgReq* prErgReq = (OSAL_trErgReq*)arg;
            if(bValueOfCryptedFile(prErgReq))
            {
                u32ErrorCode = OSAL_E_NOERROR;
            }
         }
         break;

/************************** IOCONTROL_SetValue *******************************/

         case OSAL_C_S32_IOCTRL_REGSETVALUE:
         {
            OSAL_trIOCtrlRegistry* prReg = (OSAL_trIOCtrlRegistry*)arg;

            u32ErrorCode = u32SetValue(prIO->prKey,
                                       (tCString)prReg->pcos8Name,
                                       prReg->ps8Value,
                                       prReg->s32Type,
                                       prIO->enAccess);
         }
         break;

/************************** IOCONTROL_GetValue *******************************/

         case OSAL_C_S32_IOCTRL_REGGETVALUE:
         case OSAL_C_S32_IOCTRL_REGQUERYVALUE:
         {
            OSAL_trIOCtrlRegistry* prReg = (OSAL_trIOCtrlRegistry*)arg;

            u32ErrorCode = u32GetValue(prIO->prKey,
                                       (tCString)prReg->pcos8Name,
                                       prReg->ps8Value,
                                       prReg->u32Size,
                                       &prReg->s32Type,
                                       prIO->enAccess);
         }
         break;



/************************** IOCONTROL_WriteValue *****************************/

         case OSAL_C_S32_IOCTRL_REGWRITEVALUE:
         {
            OSAL_trIOCtrlRegistryValue* prReg =
               (OSAL_trIOCtrlRegistryValue*)arg;

            u32ErrorCode = u32SetValue(prIO->prKey,
                                       (tCString)&(prReg->s8Name[0]),
                                       (tU8*)&prReg->s8Value[0],
                                       prReg->s32Type,
                                       prIO->enAccess);
         }
         break;


/************************** IOCONTROL_ReadValue ******************************/

         case OSAL_C_S32_IOCTRL_REGREADVALUE:
         {
            OSAL_trIOCtrlRegistryValue* prReg =
               (OSAL_trIOCtrlRegistryValue*)arg;

            u32ErrorCode = u32GetValue(prIO->prKey,
                                       (tCString)&(prReg->s8Name[0]),
                                       (tU8*)&prReg->s8Value[0],
                                       sizeof(prReg->s8Value),
                                       &prReg->s32Type,
                                       prIO->enAccess);
         }
         break;


/************************** IOCONTROL_ENUMValue ******************************/

         case OSAL_C_S32_IOCTRL_REGENUMVALUE:
         {
            if ((prIO->enAccess == OSAL_EN_READWRITE) ||
                (prIO->enAccess == OSAL_EN_READONLY))
            {
               OSAL_trIOCtrlDir* prDir = (OSAL_trIOCtrlDir*)arg;
               trRegValue* prValue = NULL;
               if (prDir != OSAL_NULL)
               {
                  u32ErrorCode = OSAL_E_UNKNOWN;

                  if (prDir->s32Cookie == 0)
                  {
                    if(prIO->prKey->u32FirstValueOffset)
                    {
                        prValue =  (trRegValue*)((uintptr_t)prIO->prKey + prIO->prKey->u32FirstValueOffset);
                        // trRegValue* prValue = prIO->prKey->prFirstValue;
                        if ((prValue != OSAL_NULL)&&(prValue->u32szNameOffset != 0))
                        {
                            (void)OSAL_szStringCopy((tString)prDir->dirent.s8Name,(tCString)((uintptr_t)prValue + prValue->u32szNameOffset));
                            prDir->s32Cookie = 1;
                            u32ErrorCode = OSAL_E_NOERROR;
                        } /* if (prValue != OSAL_NULL){ */
                    }
                  }
                  else
                  {
                     prValue = prGetValueByName(prIO->prKey,
                                                (tString)prDir->dirent.s8Name);
                  //   trRegValue* prValue = prGetValueByName(prIO->prKey, (tString)prDir->dirent.s8Name);
                     if ((prValue != OSAL_NULL) && (prValue->u32NextOffset != 0))
                     {
                         prValue = (trRegValue*)((uintptr_t)prIO->prKey + (uintptr_t)prValue->u32NextOffset);
                         if(prValue->u32szNameOffset != 0)
                         {
                            (void)OSAL_szStringCopy((tString)prDir->dirent.s8Name,(tCString)((uintptr_t)prValue + prValue->u32szNameOffset));
                     //     (void)OSAL_szStringCopy((tString)prDir->dirent.s8Name,prValue->prNext->coszName);
                            u32ErrorCode = OSAL_E_NOERROR;
                         }
                     }
                  }
               }
               else
               {
                  u32ErrorCode = OSAL_E_INVALIDVALUE;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_NOACCESS;
            }
         }
         break;



/************************** IOCONTROL_REGREADDIR *****************************/

         case OSAL_C_S32_IOCTRL_REGREADDIR:
         case OSAL_C_S32_IOCTRL_FIOREADDIR:
         {
            if ((prIO->enAccess == OSAL_EN_READWRITE) ||
                (prIO->enAccess == OSAL_EN_READONLY))
            {
               trRegKey* prKey = 0;
               OSAL_trIOCtrlDir* prDir = (OSAL_trIOCtrlDir*)arg;
               if (prDir != OSAL_NULL)
               {
                  u32ErrorCode = OSAL_E_UNKNOWN;

                  if((prDir->s32Cookie == 0)&&(prIO->prKey->u32FirstSubKeyOffset != 0))
                  {
                        prKey = (trRegKey*)((uintptr_t)prIO->prKey + prIO->prKey->u32FirstSubKeyOffset);
                        (void)OSAL_szStringCopy((tString)prDir->dirent.s8Name,(tCString)((uintptr_t)prKey + prKey->u32szNameOffset));
                        prDir->s32Cookie = 1;
                        u32ErrorCode = OSAL_E_NOERROR;
                  }
                  else
                  {
                        prKey = prGetKeyByName(prIO->prKey,
                                               (tString)prDir->dirent.s8Name);

                        if ((prKey != OSAL_NULL) && (prKey->u32NextKeyOffset != 0))
                        {
                            prKey = (trRegKey*)((uintptr_t)prKey + prKey->u32NextKeyOffset);
                            (void)OSAL_szStringCopy((tString)prDir->dirent.s8Name,(tCString)((uintptr_t)prKey + prKey->u32szNameOffset));
                            u32ErrorCode = OSAL_E_NOERROR;
                        }
                   }
               }
               else
               {
                  u32ErrorCode = OSAL_E_INVALIDVALUE;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_NOACCESS;
            }
         }
         break;



/************************** IOCONTROL_RemoveValue ****************************/

         case OSAL_C_S32_IOCTRL_REGREMOVEVALUE:
         {
            if ((prIO->enAccess == OSAL_EN_READWRITE) ||
                (prIO->enAccess == OSAL_EN_WRITEONLY))
            {
               OSAL_trIOCtrlRegistry* prReg = (OSAL_trIOCtrlRegistry*)arg;
               trRegValue* prVal = prGetValueByName(prIO->prKey,
                                                    (tCString)prReg->pcos8Name);
               if ((prVal != OSAL_NULL)&&(prVal->u32szNameOffset != 0))
               {
                  vDeleteValue(prVal, prIO->prKey);
                  u32ErrorCode = OSAL_E_NOERROR;
               }
               else
               {
                  u32ErrorCode = OSAL_E_DOESNOTEXIST;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_NOACCESS;
            }
         }
         break;



/************************** IOCONTROL_DeleteValue ****************************/

         case OSAL_C_S32_IOCTRL_REGDELETEVALUE:
         {
            if ((prIO->enAccess == OSAL_EN_READWRITE) ||
                (prIO->enAccess == OSAL_EN_WRITEONLY))
            {
               OSAL_trIOCtrlRegistryValue* prReg = (OSAL_trIOCtrlRegistryValue*)arg;
               trRegValue* prVal = prGetValueByName(prIO->prKey, (tCString)&(prReg->s8Name[0]));

               if ((prVal != OSAL_NULL)&&(prVal->u32szNameOffset != 0))
               {
                  vDeleteValue(prVal, prIO->prKey);
                  u32ErrorCode = OSAL_E_NOERROR;
               }
               else
               {
                  u32ErrorCode = OSAL_E_DOESNOTEXIST;
               }
            }
            else
            {
               u32ErrorCode = OSAL_E_NOACCESS;
            }
         }
         break;

         case OSAL_C_S32_IOCTRL_REGLOOKUPAPPPATH:
         {
            if (prMapAdmin)
            {
               if ((prIO->enAccess == OSAL_EN_READWRITE) ||
                   (prIO->enAccess == OSAL_EN_READONLY))
               {
                  //TraceOut(TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_REGLOOKUPAPPPATH: enter");
                  //OSAL_s32ThreadWait(1000);
                  
                  trRegMap* prKey = pMapindex2Pointer(prMapAdmin->u32FirstAppMapElement);
                  OSAL_trIOCtrlRegistryValue* prReg = (OSAL_trIOCtrlRegistryValue*)arg;
 
                  u32ErrorCode = OSAL_E_DOESNOTEXIST;
   
                  while (prKey != NULL)
                  {
                     //TraceOut(TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_REGLOOKUPAPPPATH: loop");
                     //OSAL_s32ThreadWait(100);
                     
                     if (prKey->nId == prReg->s32Type)
                     {
                        TraceOut(TR_LEVEL_SYSTEM_MIN, "OSAL_C_S32_IOCTRL_REGLOOKUPAPPPATH: found App ID 0x%04x = %s", prKey->nId, prKey->coszName);
                        (void)OSAL_szStringNCopy(prReg->s8Name, prKey->coszName, OSAL_C_U32_MAX_PATHLENGTH);
                        u32ErrorCode = OSAL_E_NOERROR;
                        break;
                     }
                     prKey = pMapindex2Pointer(prKey->nNextId);
                  }
   
                  //TraceOut(TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_REGLOOKUPAPPPATH: after loop");
                  //OSAL_s32ThreadWait(100);

                  // error case debug output
                  if (u32ErrorCode != OSAL_E_NOERROR)
                  {
                     prKey = pMapindex2Pointer(prMapAdmin->u32FirstAppMapElement);
                     TraceOut(TR_LEVEL_SYSTEM_MIN, "##### App 0x%04x not found #### -> Trace List for debug purposes", prReg->s32Type);

                     if (prKey == NULL)
                     {
                        TraceOut(TR_LEVEL_SYSTEM_MIN, "##### Map is empty %d ####", prReg->s32Type);
                     }

                     while (prKey != NULL)
                     {
                        TraceOut(TR_LEVEL_SYSTEM_MIN, "##### App ID 0x%04x -> %s #####", prKey->nId, prKey->coszName);
                        prKey = pMapindex2Pointer(prKey->nNextId);
                     }
                  }

                  //TraceOut(TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_REGLOOKUPAPPPATH: leave");
                  //OSAL_s32ThreadWait(1000);
               }
               else
               {
                  TraceOut(TR_LEVEL_FATAL, "Illegal open access flags for handle, while call of OSAL_C_S32_IOCTRL_REGLOOKUPAPPPATH");
                  u32ErrorCode = OSAL_E_NOACCESS;
               }
            }
            else
            {
               // no additional shared mem -> functionality not supported
               u32ErrorCode = OSAL_E_WRONGFUNC;
            }
         }
         break;

         case OSAL_C_S32_IOCTRL_REGLOOKUPSRVPATH:
         {
            if (prMapAdmin)
            {
               if ((prIO->enAccess == OSAL_EN_READWRITE) ||
                   (prIO->enAccess == OSAL_EN_READONLY))
               {
                  OSAL_trIOCtrlRegistryValue* prReg = (OSAL_trIOCtrlRegistryValue*)arg;
                  trRegMap* prKeyMap = pMapindex2Pointer(prMapAdmin->u32FirstSrvMapElement);

                  //TraceOut(TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_REGLOOKUPSRVPATH: enter");
                  //OSAL_s32ThreadWait(1000);

                  u32ErrorCode = OSAL_E_DOESNOTEXIST;
   
                  while (prKeyMap != NULL)
                  {
                     //TraceOut(TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_REGLOOKUPSRVPATH: loop");
                     //OSAL_s32ThreadWait(100);
                     
                     if (prKeyMap->nId == prReg->s32Type)
                     {
                        TraceOut(TR_LEVEL_SYSTEM_MIN, "OSAL_C_S32_IOCTRL_REGLOOKUPSRVPATH: found SrvId 0x%04x for Srv %s", prKeyMap->nId, prKeyMap->coszName);
                        (void)OSAL_szStringNCopy(prReg->s8Name, prKeyMap->coszName, OSAL_C_U32_MAX_PATHLENGTH);
                        break;
                     }
                     prKeyMap = pMapindex2Pointer(prKeyMap->nNextId);
                  }

                  //TraceOut(TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_REGLOOKUPSRVPATH: after loop");
                  //OSAL_s32ThreadWait(1000);
                  
                  if (prKeyMap != NULL)
                  {
                        trRegKey* prKey = prGetKeyByPath(prRegistry, (tCString)(prReg->s8Name)  );
                        
                        if (prKey) // one directory up
                           prKey = (trRegKey*)((uintptr_t)prKey - prKey->u32ParentKeyOffset);
                        
                        if (prKey) // again one directory up
                           prKey = (trRegKey*)((uintptr_t)prKey - prKey->u32ParentKeyOffset);
                        
                        if (prKey)
                        {
                           trRegValue* prAppVal = prGetValueByName(prKey, (tCString)"APPID");
                           if (prAppVal != NULL)
                           {
                              //TraceOut(TR_LEVEL_FATAL, " appval %p", prAppVal);
                              //OSAL_s32ThreadWait(100);
                              
                              TraceOut(TR_LEVEL_SYSTEM_MIN, " app Id 0x%04x for service 0x%04x found %s", prAppVal->uData.s32NumValue, prReg->s32Type, prAppVal + prAppVal->u32szNameOffset);
                              prReg->s32Type = prAppVal->uData.s32NumValue;
                              u32ErrorCode = OSAL_E_NOERROR;
                           }
                           else
                           {
                              TraceOut(TR_LEVEL_ERRORS, " app Id for service 0x%04x not found", prReg->s32Type);
                           }
                        }
                        else
                        {
                           TraceOut(TR_LEVEL_ERRORS, " parent Key for for service 0x%04x not found", prReg->s32Type);
                        }
                  }
                  else
                  {
                     TraceOut(TR_LEVEL_ERRORS, " Srv Id for service 0x%04x not found", prReg->s32Type);
                  }
                  
                  //TraceOut(TR_LEVEL_FATAL, "OSAL_C_S32_IOCTRL_REGLOOKUPSRVPATH: leave");
                  //OSAL_s32ThreadWait(1000);
               }
               else
               {
                  TraceOut(TR_LEVEL_FATAL, "Illegal open access flags for handle, while call of OSAL_C_S32_IOCTRL_REGLOOKUPSRVPATH");
                  u32ErrorCode = OSAL_E_NOACCESS;
               }
            }
            else
            {
               // no additional shared mem -> functionality not supported
               u32ErrorCode = OSAL_E_WRONGFUNC;
            }
         }
         break;

/************************** IOCONTROL_VERSION ********************************/

         case OSAL_C_S32_IOCTRL_VERSION:
         {
            *(uintptr_t*)arg = REGISTRY_VERSION;
            u32ErrorCode = OSAL_E_NOERROR;
         }
         break;
   
         default:
            u32ErrorCode = OSAL_E_WRONGFUNC;
         break;
      }
   return(u32ErrorCode);
}

tU32 REGISTRY_u32IOControl( uintptr_t u32fd, tS32 fun, intptr_t arg)
{
   tU32 u32ErrorCode = OSAL_E_UNKNOWN;

     switch(fun)
     {
        case OSAL_C_S32_IOCTRL_REGISTRYLOCK:
             if(!bLockRegistry())
             {
                 u32ErrorCode = u32ConvertErrorCore(errno);
             }
             else
             {
                 s32LockedByTid = OSAL_ThreadWhoAmI();
                 u32ErrorCode = OSAL_E_NOERROR;
             }
            break;
        case OSAL_C_S32_IOCTRL_REGISTRYUNLOCK:
             if(s32LockedByTid == OSAL_ThreadWhoAmI())
             {
                if(!bUnLockRegistry())
                {
                   u32ErrorCode = u32ConvertErrorCore(errno);
                }
                else
                {
                   s32LockedByTid = 0;
                   u32ErrorCode = OSAL_E_NOERROR;
                }
             }
             else
             {
                u32ErrorCode = OSAL_E_NOACCESS;
             }
            break;
        default:
             if(bLockRegistry() == TRUE)
             {
                u32ErrorCode = REG_u32IOControl(u32fd, fun, arg);
                NORMAL_M_ASSERT(bUnLockRegistry());
             }
             else
             {
                NORMAL_M_ASSERT_ALWAYS();
             }
            break;
     }
     return(u32ErrorCode);
}



/******************************************************************************
 * FUNCTION:      REGISTRY_u32IORemove
 *
 * DESCRIPTION:   This function removes a Key  with the specified options. 
 *                In case the Key does not exist, an error is returned.
 *
 * PARAMETER:
 *
 *   tCString     coszName   name of the key to remove (absolute path)
 *
 * RETURNVALUE:   u32ErrorCode
 *                 it is the function return value: 
 *                 - OSAL_E_NOERROR if everything goes right;
 *                 - OSAL_ERROR otherwise.
 *
 * HISTORY:
 * Date      |   Modification                         | Authors
 * 15.04.02  |   Initial revision                     |  
 *****************************************************************************/
tU32 REG_u32IORemove(tCString coszName)
{
   tU32 u32ErrorCode = OSAL_E_UNKNOWN;
      if (coszName != OSAL_NULL)
      {
         trRegKey* prKey = prGetKeyByPath(prRegistry, coszName);
         if ((prKey != OSAL_NULL)&&(prKey->u32szNameOffset))
         {
            if (bDeleteKey(prKey) == TRUE)
            {
               u32ErrorCode = OSAL_E_NOERROR;
            }
            else
            {
               u32ErrorCode = OSAL_E_BUSY;
            }
         }
         else
         {
            /* If the key does not exist, we report success (since the
             * caller does not want the key to exist any more). */
            u32ErrorCode = OSAL_E_NOERROR;
         }
      }
      else
      {
         u32ErrorCode = OSAL_E_NOTSUPPORTED;
      }
      return u32ErrorCode;
}

tU32 REGISTRY_u32IORemove(tCString coszName, tCString szDrive)
{
   tU32 u32ErrorCode = OSAL_E_UNKNOWN;

   (tVoid)(szDrive); /* Satisfy Lint */

   if(bLockRegistry() == TRUE)
   {
      u32ErrorCode = REG_u32IORemove(coszName);
      NORMAL_M_ASSERT(bUnLockRegistry());
   }
   else
   {
      NORMAL_M_ASSERT_ALWAYS();
   }
   return(u32ErrorCode);
}


#ifdef __cplusplus
}
#endif

/******************************************************************************
|end of file registry.c
|-----------------------------------------------------------------------*/


