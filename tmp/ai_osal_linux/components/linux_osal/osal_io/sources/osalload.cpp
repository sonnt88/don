/*****************************************************************************
| FILE:         osalload.cpp
| PROJECT:      platform
| SW-COMPONENT: OSAL IO
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL driver loading
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 17.07.13  | Initial revision           | MRK2HI
| 12.08.14  | Update trace for s32LoadModul   | SWM2KOR
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"
#include <dlfcn.h>
#include "ostrace.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
|defines and macros (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
|typedefs (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
drv_io_open      pGpioOpen       = NULL;
drv_io_close     pGpioClose      = NULL;
drv_io_control   pGpioIOControl  = NULL;

drv_io_open      pAdcOpen        = NULL;
drv_io_close     pAdcClose       = NULL;
drv_io_control   pAdcIOControl   = NULL;
drv_io_read      pAdcRead        = NULL;

drv_io_open      pPramOpen       = NULL;
drv_io_close     pPramClose      = NULL;
drv_io_control   pPramIOControl  = NULL;
drv_io_read      pPramRead       = NULL;
drv_io_write     pPramWrite      = NULL;

OSAL_DECL crypt_sign       pMediaFunc      = NULL;

drv_io_open        pKdsOpen      = NULL;
drv_io_close       pKdsClose     = NULL;
drv_io_control     pKdsIOControl = NULL;
drv_io_read        pKdsRead      = NULL;
drv_io_write       pKdsWrite     = NULL;

drv_io_open      pGpsOpen       = NULL;
drv_io_close     pGpsClose      = NULL;
drv_io_control   pGpsIOControl  = NULL;
drv_io_read      pGpsRead       = NULL;

drv_io_open      pGnssOpen      = NULL;
drv_io_close     pGnssClose     = NULL;
drv_io_control   pGnssIOControl = NULL;
drv_io_read      pGnssRead      = NULL;
drv_io_write     pGnssWrite     = NULL;

drv_io_open      pOdoOpen       = NULL;
drv_io_close     pOdoClose      = NULL;
drv_io_control   pOdoIOControl  = NULL;
drv_io_read      pOdoRead       = NULL;

drv_io_open      pGyroOpen      = NULL;
drv_io_close     pGyroClose     = NULL;
drv_io_control   pGyroIOControl = NULL;
drv_io_read      pGyroRead      = NULL;

drv_io_open      pAbsOpen      = NULL;
drv_io_close     pAbsClose     = NULL;
drv_io_control   pAbsIOControl = NULL;
drv_io_read      pAbsRead      = NULL;

drv_io_open      pAccOpen       = NULL;
drv_io_close     pAccClose      = NULL;
drv_io_control   pAccIOControl  = NULL;
drv_io_read      pAccRead       = NULL;

drv_io_open      pChEncOpen     = NULL;
drv_io_close     pChEncClose    = NULL;
drv_io_control   pChEncIOControl= NULL;

drv_io_open      pAuxOpen       = NULL;
drv_io_close     pAuxClose      = NULL;
drv_io_control   pAuxIOControl  = NULL;
drv_io_read      pAuxRead       = NULL;

drv_io_open      pCdCtrlOpen       = NULL;
drv_io_close     pCdCtrlClose      = NULL;
drv_io_control   pCdCtrlIOControl  = NULL;

drv_io_open      pCdAudioOpen       = NULL;
drv_io_close     pCdAudioClose      = NULL;
drv_io_control   pCdAudioIOControl  = NULL;

drv_io_open      pWupOpen       = NULL;
drv_io_close     pWupClose      = NULL;
drv_io_control   pWupIOControl  = NULL;

drv_io_open      pVoltOpen       = NULL;
drv_io_close     pVoltClose      = NULL;
drv_io_control   pVoltIOControl  = NULL;


drv_io_open      pEolOpen       = NULL;
drv_io_close     pEolClose      = NULL;
drv_io_control   pEolIOControl  = NULL;
drv_io_read      pEolRead       = NULL;
drv_io_write     pEolWrite      = NULL;

drv_io_open      pFfdOpen       = NULL;
drv_io_close     pFfdClose      = NULL;
drv_io_control   pFfdIOControl  = NULL;
drv_io_read      pFfdRead       = NULL;
drv_io_write     pFfdWrite      = NULL;


drv_io_open      pAdr3CtrlOpen       = NULL;
drv_io_close     pAdr3CtrlClose      = NULL;
drv_io_control   pAdr3CtrlIOControl  = NULL;
drv_io_read      pAdr3CtrlRead       = NULL;
drv_io_write     pAdr3CtrlWrite      = NULL;

drv_io_open      pAaRSDABOpen       = NULL;
drv_io_close     pAaRSDABClose      = NULL;
drv_io_control   pAaRSDABIOControl  = NULL;
drv_io_read      pAaRSDABRead       = NULL;
drv_io_write     pAaRSDABWrite      = NULL;

drv_io_open      pAaRSSSI32Open       = NULL;
drv_io_close     pAaRSSSI32Close      = NULL;
drv_io_control   pAaRSSSI32IOControl  = NULL;
drv_io_read      pAaRSSSI32Read       = NULL;
drv_io_write     pAaRSSSI32Write      = NULL;

drv_io_open      pAaRSMTDOpen         = NULL;
drv_io_close     pAaRSMTDClose        = NULL;
drv_io_control   pAaRSMTDIOControl    = NULL;
drv_io_read      pAaRSMTDRead         = NULL;
drv_io_write     pAaRSMTDWrite        = NULL;

drv_io_open      pAaRSAMFMOpen         = NULL;
drv_io_close     pAaRSAMFMClose        = NULL;
drv_io_control   pAaRSAMFMIOControl    = NULL;
drv_io_read      pAaRSAMFMRead         = NULL;
drv_io_write     pAaRSAMFMWrite        = NULL;

crypt_ctrl       pSDCardInfo     = NULL;
crypt_ctrl       pReadCid        = NULL;
crypt_sign       pVerify_signaturefile       = NULL;
crypt_sign       pGet_signature_verify_status= NULL;
crypt_sign       pGet_signaturefile_type     = NULL;
aes_encrypt      pEncryptAesFile = NULL;


drv_io_open      pAcOutOpen      = NULL;
drv_io_close     pAcOutClose     = NULL;
drv_io_control   pAcOutIOControl = NULL;
drv_io_write     pAcOutWrite     = NULL;

drv_io_open      pAcInOpen       = NULL;
drv_io_close     pAcInClose      = NULL;
drv_io_control   pAcInIOControl  = NULL;
drv_io_read      pAcInRead       = NULL;

drv_io_open      pAcSrcOpen      = NULL;
drv_io_close     pAcSrcClose     = NULL;
drv_io_control   pAcSrcIOControl = NULL;

drv_io_open      pAcCnrOpen      = NULL;
drv_io_close     pAcCnrClose     = NULL;
drv_io_control   pAcCnrIOControl = NULL;
drv_io_read      pAcCnrRead      = NULL;

dbg_func         pCoreDbgFunc    = NULL;
dbg_func         pIoDbgFunc      = NULL;
u32PrmFunc       pPrmFunc        = NULL;
CbExc            pCbExc          = NULL;
PrcTskInfo       pPrcTskInfo     = NULL;

drv_io_open      pTouchOpen      = NULL;
drv_io_close     pTouchClose     = NULL;
drv_io_control   pTouchIOControl = NULL;
drv_io_read      pTouchRead      = NULL;
drv_io_write     pTouchWrite     = NULL;

drv_io_open      pPRamOpen       = NULL;
drv_io_close     pPRamClose      = NULL;
drv_io_control   pPRamIOControl  = NULL;
drv_io_read      pPRamRead       = NULL;
drv_io_write     pPRamWrite      = NULL;


trOsalDrvFunc rOsalDrvFuncTable[OSAL_EN_DEVID_LAST];

GetFunctionPointer  pGetFuncPtr[EN_SHARED_LAST];

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
extern tS32 wrapper_prm_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_prm_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_prm_io_ioctl(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_odometer_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_odometer_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_odometer_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_odometer_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_gyro_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_gyro_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_gyro_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_gyro_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_abs_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_abs_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_abs_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_abs_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_acc_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_acc_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_acc_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_acc_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_wup_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_wup_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_wup_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_trace_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_trace_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_trace_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_trace_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_trace_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_kds_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_kds_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_kds_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_kds_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_kds_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_acousticout_open(tS32 s32ID, tCString szName,OSAL_tenAccess enAccess, uintptr_t *pu32FD,tU16  app_id);
extern tS32 wrapper_acousticout_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_acousticout_ioctl(tS32 s32ID, uintptr_t u32FD,tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_acousticout_write(tS32 s32ID,uintptr_t u32FD, tPCS8 pBuffer,tU32 u32Size,uintptr_t *ret_size);

extern tS32 wrapper_acousticin_open(tS32 s32ID,tCString szName, OSAL_tenAccess enAccess,uintptr_t *pu32FD,tU16  app_id);
extern tS32 wrapper_acousticin_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_acousticin_ioctl(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_acousticin_read(tS32 s32ID, uintptr_t u32FD, tPS8 ps8Buffer, tU32 u32Size,uintptr_t *pRet_size);

extern tS32 wrapper_acousticecnr_open(tS32 s32ID,tCString szName, OSAL_tenAccess enAccess,uintptr_t *pu32FD,tU16  app_id);
extern tS32 wrapper_acousticecnr_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_acousticecnr_ioctl(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_acousticecnr_read(tS32 s32ID, uintptr_t u32FD, tPS8 ps8Buffer, tU32 u32Size,uintptr_t *pRet_size);


extern tS32 wrapper_acousticsrc_open(tS32 s32ID, tCString szName,OSAL_tenAccess enAccess,uintptr_t *pu32FD,tU16  app_id);
extern tS32 wrapper_acousticsrc_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_acousticsrc_ioctl(tS32 s32ID, uintptr_t u32FD,tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_auxclock_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_auxclock_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_auxclock_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_auxclock_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_diag_eol_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_diag_eol_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_diag_eol_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_diag_eol_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_diag_eol_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_ffd_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_ffd_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_ffd_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_ffd_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pcs8Buffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_ffd_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_ERRMEM_S32IOOpen(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_ERRMEM_S32IOClose(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_ERRMEM_s32IORead(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_ERRMEM_s32IOWrite(tS32 s32ID, uintptr_t u32FD, tPCS8 pcs8Buffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_ERRMEM_s32IOControl(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_touchscreen_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_touchscreen_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_touchscreen_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *pu32RetSize);
extern tS32 wrapper_touchscreen_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *pu32RetSize);
extern tS32 wrapper_touchscreen_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);


extern tS32 wrapper_cdctrl_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_cdctrl_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_cdctrl_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_cdaudio_io_open(tS32 s32ID, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_cdaudio_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_cdaudio_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_gps_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_gps_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_gps_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_gps_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_gnss_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_gnss_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_gnss_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_gnss_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_gnss_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pcsBuffer, tU32 u32Size, uintptr_t *ret_size);

extern tS32 wrapper_gpio_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_gpio_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_gpio_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_adc_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_adc_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_adc_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_adc_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_volt_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_volt_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_volt_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_ChEnc_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_ChEnc_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_ChEnc_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);

extern tS32 wrapper_adr3_ctrl_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_adr3_ctrl_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_adr3_ctrl_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_adr3_ctrl_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_adr3_ctrl_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *pu32RetSize);

extern tS32 wrapper_aars_dab_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_aars_dab_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_aars_dab_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_aars_dab_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_aars_dab_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *pu32RetSize);

extern tS32 wrapper_aars_ssi32_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_aars_ssi32_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_aars_ssi32_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_aars_ssi32_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_aars_ssi32_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *pu32RetSize);

extern tS32 wrapper_aars_mtd_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_aars_mtd_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_aars_mtd_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_aars_mtd_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_aars_mtd_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *pu32RetSize);

extern tS32 wrapper_aars_amfm_io_open(tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16  app_id);
extern tS32 wrapper_aars_amfm_io_close(tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_aars_amfm_io_control(tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32arg);
extern tS32 wrapper_aars_amfm_io_read(tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *ret_size);
extern tS32 wrapper_aars_amfm_io_write(tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *pu32RetSize);

extern tS32 wrapper_pram_io_open (tS32 s32Id, tCString szName, OSAL_tenAccess enAccess, uintptr_t *pu32FD, tU16 appid);
extern tS32 wrapper_pram_io_close (tS32 s32ID, uintptr_t u32FD);
extern tS32 wrapper_pram_io_ctrl (tS32 s32ID, uintptr_t u32FD, tS32 s32fun, intptr_t s32Arg);
extern tS32 wrapper_pram_io_read (tS32 s32ID, uintptr_t u32FD, tPS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size);
extern tS32 wrapper_pram_io_write (tS32 s32ID, uintptr_t u32FD, tPCS8 pBuffer, tU32 u32Size, uintptr_t *u32_ret_Size);


/************************************************************************
|function implementation (scope: module-local)
|-----------------------------------------------------------------------*/

tS32 s32GetOsal2FunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;

   pCoreDbgFunc   = (dbg_func)(dlsym(pModuleHandle, (const char*)"vSysCallbackHandler"));/*lint !e611 */
   pIoDbgFunc     = (dbg_func)(dlsym(pModuleHandle, (const char*)"vOsalIoCallbackHandler"));/*lint !e611 */
   pPrmFunc       = (u32PrmFunc)(dlsym(pModuleHandle, (const char*)"prm_u32Prm"));/*lint !e611 */
   pCbExc         = (CbExc)(dlsym(pModuleHandle, (const char*)"TraceCallbackexcute"));/*lint !e611 */
   pPrcTskInfo    = (PrcTskInfo)(dlsym(pModuleHandle, (const char*)"vProcTaskInfo"));/*lint !e611 */

   if(pPrcTskInfo == NULL)
   {
      TraceString("Not all function pointers for OSAL2 found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }

   }

   if((pCoreDbgFunc == NULL )||(pIoDbgFunc == NULL )||(pPrmFunc == NULL )||(pCbExc == NULL )||(pPrcTskInfo == NULL))
   {
      TraceString("Not all function pointers for OSAL2 found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 s32GetBaseDrvFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_OK;
   char *error;

   pGpioOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"gpio_drv_io_open"));/*lint !e611 */
   pGpioClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"gpio_drv_io_close"));/*lint !e611 */
   pGpioIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"gpio_drv_io_control"));/*lint !e611 */

   pAdcOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"adc_drv_io_open"));/*lint !e611 */
   pAdcClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"adc_drv_io_close"));/*lint !e611 */
   pAdcIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"adc_drv_io_control"));/*lint !e611 */
   pAdcRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"adc_drv_io_read"));/*lint !e611 */
 
   pPramOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"pram_drv_io_open"));/*lint !e611 */
   pPramClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"pram_drv_io_close"));/*lint !e611 */
   pPramIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"pram_drv_io_control"));/*lint !e611 */
   pPramRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"pram_drv_io_read"));/*lint !e611 */
   pPramWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"pram_drv_io_write"));/*lint !e611 */

   pMediaFunc     = (crypt_sign)(dlsym(pModuleHandle, (const char*)"s32InvestigateMedia"));/*lint !e611 */

   if((pGpioOpen == NULL )||(pGpioClose == NULL )||(pGpioIOControl == NULL ))
   {
      TraceString("Not all function pointers for GPIO found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
      s32Ret = OSAL_ERROR;
   }
   if((pAdcOpen == NULL )||(pAdcClose == NULL )||(pAdcIOControl == NULL )||(pAdcRead == NULL ))
   {
      TraceString("Not all function pointers for ADC found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
      s32Ret = OSAL_ERROR;
   }

   if((pPramClose == NULL )||(pPramClose == NULL )||(pPramIOControl == NULL )||(pPramRead == NULL )||(pPramWrite == NULL ))
   {
      TraceString("Not all function pointers for PRAM found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
      s32Ret = OSAL_ERROR;
   }


   return s32Ret;
}

tS32 s32GetTouchFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;

   pTouchOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"touch_drv_io_open"));/*lint !e611 */
   pTouchClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"touch_drv_io_close"));/*lint !e611 */
   pTouchIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"touch_drv_io_control"));/*lint !e611 */
   pTouchRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"touch_drv_io_read"));/*lint !e611 */
   pTouchWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"touch_drv_io_write"));/*lint !e611 */

   if((pTouchOpen == NULL )||(pTouchClose == NULL )||(pTouchIOControl == NULL )
    ||(pTouchRead == NULL )||(pTouchWrite == NULL ))
   {
      TraceString("Not all function pointers for Touch found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}


tS32 s32GetKdsFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;

   pKdsOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"kds_drv_io_open"));/*lint !e611 */
   pKdsClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"kds_drv_io_close"));/*lint !e611 */
   pKdsIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"kds_drv_io_control"));/*lint !e611 */
   pKdsRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"kds_drv_io_read"));/*lint !e611 */
   pKdsWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"kds_drv_io_write"));/*lint !e611 */

   if((pKdsOpen == NULL )||(pKdsClose == NULL )||(pKdsIOControl == NULL )
    ||(pKdsRead == NULL )||(pKdsWrite == NULL ))
   {
      TraceString("Not all function pointers for KDS found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}


tS32 s32GetEolFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_OK;
   char *error;

   pEolOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"eol_drv_io_open"));/*lint !e611 */
   pEolClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"eol_drv_io_close"));/*lint !e611 */
   pEolIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"eol_drv_io_control"));/*lint !e611 */
   pEolRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"eol_drv_io_read"));/*lint !e611 */
   pEolWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"eol_drv_io_write"));/*lint !e611 */
   if((pEolOpen == NULL )||(pEolClose == NULL )||(pEolIOControl == NULL )
    ||(pEolRead == NULL )||(pEolWrite == NULL ))
   {
      TraceString("Not all function pointers for EOL found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
      s32Ret = OSAL_ERROR;
   }

   pFfdOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"ffd_drv_io_open"));/*lint !e611 */
   pFfdClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"ffd_drv_io_close"));/*lint !e611 */
   pFfdIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"ffd_drv_io_control"));/*lint !e611 */
   pFfdRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"ffd_drv_io_read"));/*lint !e611 */
   pFfdWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"ffd_drv_io_write"));/*lint !e611 */
   if((pFfdOpen == NULL )||(pFfdClose == NULL )||(pFfdIOControl == NULL )
    ||(pFfdRead == NULL )||(pFfdWrite == NULL ))
   {
      TraceString("Not all function pointers for FFD found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
      s32Ret = OSAL_ERROR;
   }

   return s32Ret;
}

tS32 s32GetWupFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;

   pWupOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"wup_drv_io_open"));/*lint !e611 */;
   pWupClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"wup_drv_io_close"));/*lint !e611 */
   pWupIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"wup_drv_io_control"));/*lint !e611 */

   if ((pWupOpen == NULL )||(pWupClose == NULL )||(pWupIOControl == NULL ))
   {
      TraceString("Not all function pointers for WUP found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 s32GetVoltFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;

   pVoltOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"volt_drv_io_open"));/*lint !e611 */;
   pVoltClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"volt_drv_io_close"));/*lint !e611 */
   pVoltIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"volt_drv_io_control"));/*lint !e611 */

   if((pVoltOpen == NULL )||(pVoltClose == NULL )||(pVoltIOControl == NULL ))
   {
      TraceString("Not all function pointers for VOLT found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 s32GetOdFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;

   pCdCtrlOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"cdctrl_drv_io_open"));/*lint !e611 */;
   pCdCtrlClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"cdctrl_drv_io_close"));/*lint !e611 */
   pCdCtrlIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"cdctrl_drv_io_control"));/*lint !e611 */

   pCdAudioOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"cdaudio_drv_io_open"));/*lint !e611 */;
   pCdAudioClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"cdaudio_drv_io_close"));/*lint !e611 */
   pCdAudioIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"cdaudio_drv_io_control"));/*lint !e611 */

   if((pCdCtrlOpen == NULL )||(pCdCtrlClose == NULL )||(pCdCtrlIOControl == NULL )
    ||(pCdAudioOpen == NULL )||(pCdAudioClose == NULL )||(pCdAudioIOControl == NULL ))
   {
      TraceString("Not all function pointers for CD found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      pOsalData->bCdActive = TRUE;
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 s32GetSensorFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;

   pGpsOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"gnss_drv_io_open"));/*lint !e611 */
   pGpsClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"gnss_drv_io_close"));/*lint !e611 */
   pGpsIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"gps_drv_io_control"));/*lint !e611 */
   pGpsRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"gps_drv_io_read"));/*lint !e611 */
   if((pGpsOpen == NULL )||(pGpsClose == NULL )||(pGpsIOControl == NULL )||(pGpsRead == NULL ))
   {
      s32Ret = OSAL_ERROR;
       TraceString("Not all function pointers for GPS found 0x%x 0x%x 0x%x 0x%x ",
                           pGpsOpen,pGpsClose,pGpsIOControl,pGpsRead);
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }

   pGnssOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"gnss_drv_io_open"));/*lint !e611 */
   pGnssClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"gnss_drv_io_close"));/*lint !e611 */
   pGnssIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"gnss_drv_io_control"));/*lint !e611 */
   pGnssRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"gnss_drv_io_read"));/*lint !e611 */
   pGnssWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"gnss_drv_io_write"));/*lint !e611 */
   if((pGnssOpen == NULL )||(pGnssClose == NULL )||(pGnssIOControl == NULL )||(pGnssRead == NULL )|| (NULL == pGnssWrite))
   {
      s32Ret = OSAL_ERROR;
      TraceString("Not all function pointers for GNSS found 0x%x 0x%x 0x%x 0x%x 0x%x",
                  pGnssOpen,pGnssClose,pGnssIOControl,pGnssRead,pGnssWrite);
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }

   pOdoOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"odo_drv_io_open"));/*lint !e611 */
   pOdoClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"odo_drv_io_close"));/*lint !e611 */
   pOdoIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"odo_drv_io_control"));/*lint !e611 */
   pOdoRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"odo_drv_io_read"));/*lint !e611 */
   if((pOdoOpen == NULL )||(pOdoClose == NULL )||(pOdoIOControl == NULL )||(pOdoRead == NULL ))
   {
      s32Ret = OSAL_ERROR;
      TraceString("Not all function pointers for ODO found 0x%x 0x%x 0x%x 0x%x ",
                  pOdoOpen,pOdoClose,pOdoIOControl,pOdoRead);
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }

   pGyroOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"gyro_drv_io_open"));/*lint !e611 */
   pGyroClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"gyro_drv_io_close"));/*lint !e611 */
   pGyroIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"gyro_drv_io_control"));/*lint !e611 */
   pGyroRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"gyro_drv_io_read"));/*lint !e611 */
   if((pGyroOpen == NULL )||(pGyroClose == NULL )||(pGyroIOControl == NULL )||(pGyroRead == NULL ))
   {
      s32Ret = OSAL_ERROR;
      TraceString("Not all function pointers for Gyro found 0x%x 0x%x 0x%x 0x%x ",
                  pGyroOpen,pGyroClose,pGyroIOControl,pGyroRead);
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }

   pAccOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"acc_drv_io_open"));/*lint !e611 */
   pAccClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"acc_drv_io_close"));/*lint !e611 */
   pAccIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"acc_drv_io_control"));/*lint !e611 */
   pAccRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"acc_drv_io_read"));/*lint !e611 */
   if((pAccOpen == NULL )||(pAccClose == NULL )||(pAccIOControl == NULL )||(pAccRead == NULL ))
   {
      s32Ret = OSAL_ERROR;
      TraceString("Not all function pointers for ACC found 0x%x 0x%x 0x%x 0x%x ",
                  pAccOpen,pAccClose,pAccIOControl,pAccRead);
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }

   pChEncOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"chenc_drv_io_open"));/*lint !e611 */
   pChEncClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"chenc_drv_io_close"));/*lint !e611 */
   pChEncIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"chenc_drv_io_control"));/*lint !e611 */
   if((pChEncOpen == NULL )||(pChEncClose == NULL )||(pChEncIOControl == NULL ))
   {
      s32Ret = OSAL_ERROR;
      TraceString("Not all function pointers for Chenc found 0x%x 0x%x 0x%x",
                  pChEncOpen,pChEncClose,pChEncIOControl);
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   
   pAuxOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"aux_drv_io_open"));/*lint !e611 */
   pAuxClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"aux_drv_io_close"));/*lint !e611 */
   pAuxIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"aux_drv_io_control"));/*lint !e611 */
   pAuxRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"aux_drv_io_read"));/*lint !e611 */
   if((pAuxOpen == NULL )||(pAuxClose == NULL )||(pAuxIOControl == NULL )||(pAuxRead == NULL ))
   {
      s32Ret = OSAL_ERROR;
      TraceString("Not all function pointers for Aux clock found 0x%x 0x%x 0x%x 0x%x ",
                pAuxOpen,pAuxClose,pAuxIOControl,pAuxRead );
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
    s32Ret = OSAL_OK;
   }


   pAbsOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"abs_drv_io_open"));/*lint !e611 */
   pAbsClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"abs_drv_io_close"));/*lint !e611 */
   pAbsIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"abs_drv_io_control"));/*lint !e611 */
   pAbsRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"abs_drv_io_read"));/*lint !e611 */
   if((pAbsOpen == NULL )||(pAbsClose == NULL )||(pAbsIOControl == NULL )||(pAbsRead == NULL ))
   {
      s32Ret = OSAL_ERROR;
      TraceString("Not all function pointers for ABS found 0x%x 0x%x 0x%x 0x%x ",
                  pAbsOpen,pAbsClose,pAbsIOControl,pAbsRead);
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }

   return s32Ret;
}

tS32 s32GetAcousticFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_OK;
   char *error;

   pAcOutOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"acousticout_drv_io_open"));/*lint !e611 */
   pAcOutClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"acousticout_drv_io_close"));/*lint !e611 */
   pAcOutIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"acousticout_drv_io_control"));/*lint !e611 */
   pAcOutWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"acousticout_drv_io_write"));/*lint !e611 */

   if((pAcOutOpen == NULL )||(pAcOutClose == NULL )
    ||(pAcOutIOControl == NULL )||(pAcOutWrite == NULL ))
   {
      TraceString("Not all function pointers for Acoustic Out found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
      s32Ret = OSAL_ERROR;
   }

   pAcInOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"acousticin_drv_io_open"));/*lint !e611 */
   pAcInClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"acousticin_drv_io_close"));/*lint !e611 */
   pAcInIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"acousticin_drv_io_control"));/*lint !e611 */
   pAcInRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"acousticin_drv_io_read"));/*lint !e611 */
   if((pAcInOpen == NULL )||(pAcInClose == NULL )
    ||(pAcInIOControl == NULL )||(pAcInRead == NULL ))
   {
      TraceString("Not all function pointers for Acoustic In found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
      s32Ret = OSAL_ERROR;
   }

   pAcSrcOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"acousticsrc_drv_io_open"));/*lint !e611 */
   pAcSrcClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"acousticsrc_drv_io_close"));/*lint !e611 */
   pAcSrcIOControl  = (drv_io_control)(dlsym(pModuleHandle, (const char*)"acousticsrc_drv_io_control"));/*lint !e611 */
   if((pAcSrcOpen == NULL )||(pAcSrcClose == NULL )||(pAcSrcIOControl == NULL ))
   {
      TraceString("Not all function pointers for Acoustic Src found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
      s32Ret = OSAL_ERROR;
   }

   pAcCnrOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"acousticcnr_drv_io_open"));/*lint !e611 */
   pAcCnrClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"acousticcnr_drv_io_close"));/*lint !e611 */
   pAcCnrIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"acousticcnr_drv_io_control"));/*lint !e611 */
   pAcCnrRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"acousticcnr_drv_io_read"));/*lint !e611 */
   if((pAcCnrOpen == NULL )||(pAcCnrClose == NULL )
    ||(pAcCnrIOControl == NULL )||(pAcCnrRead == NULL ))
   {
      TraceString("Not all function pointers for Acoustic CNR found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
      s32Ret = OSAL_ERROR;
   }

   return s32Ret;
}

tS32 s32GetAdr3CtrlFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;

   pAdr3CtrlOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"adr3ctrl_drv_io_open"));/*lint !e611 */
   pAdr3CtrlClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"adr3ctrl_drv_io_close"));/*lint !e611 */
   pAdr3CtrlIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"adr3ctrl_drv_io_control"));/*lint !e611 */
   pAdr3CtrlRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"adr3ctrl_drv_io_read"));/*lint !e611 */
   pAdr3CtrlWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"adr3ctrl_drv_io_write"));/*lint !e611 */

   if((pAdr3CtrlOpen == NULL )||(pAdr3CtrlClose == NULL )||(pAdr3CtrlIOControl == NULL )
    ||(pAdr3CtrlRead == NULL )||(pAdr3CtrlWrite == NULL ))
   {
      TraceString("Not all function pointers for ADR3Ctrl found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 s32GetAaRSDABFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;
   char buffer[100];

   pAaRSDABOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"aars_dab_drv_io_open"));/*lint !e611 */
   pAaRSDABClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"aars_dab_drv_io_close"));/*lint !e611 */
   pAaRSDABIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"aars_dab_drv_io_control"));/*lint !e611 */
   pAaRSDABRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"aars_dab_drv_io_read"));/*lint !e611 */
   pAaRSDABWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"aars_dab_drv_io_write"));/*lint !e611 */

   if((pAaRSDABOpen == NULL )||(pAaRSDABClose == NULL )||(pAaRSDABIOControl == NULL )
    ||(pAaRSDABRead == NULL )||(pAaRSDABWrite == NULL ))
   {
      TraceString("Not all function pointers for AaRSDAB found");
      error = dlerror();
      if (error != NULL)
      {
         snprintf(buffer,100,"dlsym failed Error:%d",errno);
         TraceString(buffer);
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 s32GetAaRSSSI32FunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;
   char buffer[100];

   pAaRSSSI32Open      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"aars_ssi32_drv_io_open"));/*lint !e611 */
   pAaRSSSI32Close     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"aars_ssi32_drv_io_close"));/*lint !e611 */
   pAaRSSSI32IOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"aars_ssi32_drv_io_control"));/*lint !e611 */
   pAaRSSSI32Read      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"aars_ssi32_drv_io_read"));/*lint !e611 */
   pAaRSSSI32Write     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"aars_ssi32_drv_io_write"));/*lint !e611 */

   if((pAaRSSSI32Open == NULL )||(pAaRSSSI32Close == NULL )||(pAaRSSSI32IOControl == NULL )
    ||(pAaRSSSI32Read == NULL )||(pAaRSSSI32Write == NULL ))
   {
      TraceString("Not all function pointers for AaRSSSI32 found");
      error = dlerror();
      if (error != NULL)
      {
         snprintf(buffer,100,"dlsym failed Error:%d",errno);
         TraceString(buffer);
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 s32GetAaRSMTDFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;
   char buffer[100];

   pAaRSMTDOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"aars_mtd_drv_io_open"));/*lint !e611 */
   pAaRSMTDClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"aars_mtd_drv_io_close"));/*lint !e611 */
   pAaRSMTDIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"aars_mtd_drv_io_control"));/*lint !e611 */
   pAaRSMTDRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"aars_mtd_drv_io_read"));/*lint !e611 */
   pAaRSMTDWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"aars_mtd_drv_io_write"));/*lint !e611 */

   if((pAaRSMTDOpen == NULL )||(pAaRSMTDClose == NULL )||(pAaRSMTDIOControl == NULL )
    ||(pAaRSMTDRead == NULL )||(pAaRSMTDWrite == NULL ))
   {
      TraceString("Not all function pointers for AaRSMTD found");
      error = dlerror();
      if (error != NULL)
      {
         snprintf(buffer,100,"dlsym failed Error:%d",errno);
         TraceString(buffer);
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 s32GetAaRSAMFMFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;
   char buffer[100];

   pAaRSAMFMOpen      = (drv_io_open)(dlsym(pModuleHandle, (const char*)"aars_amfm_drv_io_open"));/*lint !e611 */
   pAaRSAMFMClose     = (drv_io_close)(dlsym(pModuleHandle, (const char*)"aars_amfm_drv_io_close"));/*lint !e611 */
   pAaRSAMFMIOControl = (drv_io_control)(dlsym(pModuleHandle, (const char*)"aars_amfm_drv_io_control"));/*lint !e611 */
   pAaRSAMFMRead      = (drv_io_read)(dlsym(pModuleHandle, (const char*)"aars_amfm_drv_io_read"));/*lint !e611 */
   pAaRSAMFMWrite     = (drv_io_write)(dlsym(pModuleHandle, (const char*)"aars_amfm_drv_io_write"));/*lint !e611 */

   if((pAaRSAMFMOpen == NULL )||(pAaRSAMFMClose == NULL )||(pAaRSAMFMIOControl == NULL )
    ||(pAaRSAMFMRead == NULL )||(pAaRSAMFMWrite == NULL ))
   {
      TraceString("Not all function pointers for AaRSAMFM found");
      error = dlerror();
      if (error != NULL)
      {
         snprintf(buffer,100,"dlsym failed Error:%d",errno);
         TraceString(buffer);
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}

tS32 s32GetCryptModulFunctionPointer(void* pModuleHandle)
{
   tS32 s32Ret = OSAL_ERROR;
   char *error;

#ifdef AES_ENCRYPTION_AVAILABLE
   pEncryptAesFile = (aes_encrypt)(dlsym(pModuleHandle, (const char*)"crypt_read_aes_file"));/*lint !e611 */;
   if(pEncryptAesFile == NULL)
   {
      TraceString("AES function pointer in CryptModul not found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
#endif

   pSDCardInfo           = (crypt_ctrl)(dlsym(pModuleHandle, (const char*)"crypt_ctrl_u32SDCardInfo"));/*lint !e611 */;
   pReadCid              = (crypt_ctrl)(dlsym(pModuleHandle, (const char*)"crypt_ctrl_u32ReadCid"));/*lint !e611 */;
 
   pVerify_signaturefile        = (crypt_sign)(dlsym(pModuleHandle, (const char*)"sign_verify_signaturefile"));/*lint !e611 */;
   pGet_signature_verify_status = (crypt_sign)(dlsym(pModuleHandle, (const char*)"sign_get_signature_verify_status"));/*lint !e611 */;
   pGet_signaturefile_type      = (crypt_sign)(dlsym(pModuleHandle, (const char*)"sign_get_signaturefile_type"));/*lint !e611 */;

   if((pSDCardInfo == NULL )||(pReadCid == NULL )||(pVerify_signaturefile == NULL )
    ||(pGet_signature_verify_status == NULL )||(pGet_signature_verify_status == NULL ))
   {
      TraceString("Not all function pointers for CryptModul found");
      error = dlerror();
      if (error != NULL)
      {
         TraceString("dlsym failed Errno:%d %s",errno,dlerror());
      }
   }
   else
   {
      s32Ret = OSAL_OK;
   }
   return s32Ret;
}




static uintptr_t ModuleHandle[EN_SHARED_LAST] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void vUnLoadModulsForExit(void)
{
  tS32 s32Count= 0;
  for(s32Count= 0; s32Count < EN_SHARED_LAST;s32Count++)
  {
     /* shared_eol should not be unloaded because there is a hard dependency to the shared datapool, 
        which is also linked directly to many processes */
     if((ModuleHandle[s32Count])&&(s32Count != EN_SHARED_EOL))
     {
    	if( (void*)ModuleHandle[s32Count] == (void*)1 )
    	{
    		/* NOP, because module was loaded with OSAL_s32DefineModul */
    	}
    	else if(dlclose((void*)ModuleHandle[s32Count]) != 0)
        {
           TraceString("dlclose for DriverID %d failed with %s",s32Count,dlerror());
        }
        else
        {
           TraceString("dlclose for DriverID %d succeeded ",s32Count);
        }
     }
  }
}

/*****************************************************************************
*
* FUNCTION:    OSAL_s32DefineModul
*
* DESCRIPTION: Define a OSAL device module, by given device id and function pointer
*
* PARAMETER:
*
* RETURNVALUE:
*****************************************************************************/
tVoid OSAL_s32DefineModul(uint h, tPVoid pOpen, tPVoid pClose, tPVoid pIOControl, tPVoid pRead, tPVoid pWrite)
{
	switch( h )
	{
	case EN_SHARED_AARS_DAB:
	    ModuleHandle[h] = 1; /* mark handle as loaded */
	    pAaRSDABOpen      = (drv_io_open)pOpen; /*lint !e611 */
	    pAaRSDABClose     = (drv_io_close)pClose;/*lint !e611 */
	    pAaRSDABIOControl = (drv_io_control)pIOControl;/*lint !e611 */
	    pAaRSDABRead      = (drv_io_read)pRead;/*lint !e611 */
	    pAaRSDABWrite     = (drv_io_write)pWrite;/*lint !e611 */
		break;
	case EN_SHARED_AARS_SSI32:
	    ModuleHandle[h] = 1; /* mark handle as loaded */
	    pAaRSSSI32Open      = (drv_io_open)pOpen;/*lint !e611 */
	    pAaRSSSI32Close     = (drv_io_close)pClose;/*lint !e611 */
	    pAaRSSSI32IOControl = (drv_io_control)pIOControl;/*lint !e611 */
	    pAaRSSSI32Read      = (drv_io_read)pRead;/*lint !e611 */
	    pAaRSSSI32Write     = (drv_io_write)pWrite;/*lint !e611 */
		break;
	case EN_SHARED_AARS_MTD:
	    ModuleHandle[h] = 1; /* mark handle as loaded */
	    pAaRSMTDOpen      = (drv_io_open)pOpen;/*lint !e611 */
	    pAaRSMTDClose     = (drv_io_close)pClose;/*lint !e611 */
	    pAaRSMTDIOControl = (drv_io_control)pIOControl;/*lint !e611 */
	    pAaRSMTDRead      = (drv_io_read)pRead;/*lint !e611 */
	    pAaRSMTDWrite     = (drv_io_write)pWrite;/*lint !e611 */
		break;
	case EN_SHARED_AARS_AMFM:
	    ModuleHandle[h] = 1; /* mark handle as loaded */
	    pAaRSAMFMOpen      = (drv_io_open)pOpen;/*lint !e611 */
	    pAaRSAMFMClose     = (drv_io_close)pClose;/*lint !e611 */
	    pAaRSAMFMIOControl = (drv_io_control)pIOControl;/*lint !e611 */
	    pAaRSAMFMRead      = (drv_io_read)pRead;/*lint !e611 */
	    pAaRSAMFMWrite     = (drv_io_write)pWrite;/*lint !e611 */
		break;
    default:
	    break;
		
	}
}

tS32 s32UnLoadModul(const char* szModuleName)
{
    tS32 s32Ret = OSAL_ERROR;

    if(strstr(szModuleName,"libshared_kds"))
    {
        if(ModuleHandle[EN_SHARED_KDS])
        {
           pKdsOpen      = NULL;
           pKdsClose     = NULL;
           pKdsIOControl = NULL;
           pKdsRead      = NULL;
           pKdsWrite     = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_KDS]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libsensordrv"))
    {
        if(ModuleHandle[EN_SHARED_SENSOR])
        {
           pGpsOpen       = NULL;
           pGpsClose      = NULL;
           pGpsIOControl  = NULL;
           pGpsRead       = NULL;
           pGnssOpen      = NULL;
           pGnssClose     = NULL;
           pGnssIOControl = NULL;
           pGnssRead      = NULL;
           pGnssWrite     = NULL;
           pOdoOpen       = NULL;
           pOdoClose      = NULL;
           pOdoIOControl  = NULL;
           pOdoRead       = NULL;
           pGyroOpen      = NULL;
           pGyroClose     = NULL;
           pGyroIOControl = NULL;
           pGyroRead      = NULL;
           pAccOpen       = NULL;
           pAccClose      = NULL;
           pAccIOControl  = NULL;
           pAccRead       = NULL;
           pChEncOpen     = NULL;
           pChEncClose    = NULL;
           pChEncIOControl= NULL;
           pAuxOpen       = NULL; 
           pAuxClose      = NULL; 
           pAuxIOControl  = NULL; 
           pAuxRead       = NULL; 
           pAbsOpen       = NULL;
           pAbsClose      = NULL;
           pAbsIOControl  = NULL;
           pAbsRead       = NULL;   
           if(dlclose((void*)ModuleHandle[EN_SHARED_SENSOR]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libbasedrv"))
    {
        if(ModuleHandle[EN_SHARED_BASE])
        {
           pGpioOpen       = NULL;
           pGpioClose      = NULL;
           pGpioIOControl  = NULL;
           pAdcOpen        = NULL;
           pAdcClose       = NULL;
           pAdcIOControl   = NULL;
           pAdcRead        = NULL;
           pPramOpen       = NULL;
           pPramClose      = NULL;
           pPramIOControl  = NULL;
           pPramRead       = NULL;
           pPramWrite      = NULL;
           pMediaFunc      = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_BASE]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libshared_cd"))
    {
        if(ModuleHandle[EN_SHARED_OD])
        {
           pCdCtrlOpen       = NULL;
           pCdCtrlClose      = NULL;
           pCdCtrlIOControl  = NULL;
           pCdAudioOpen       = NULL;
           pCdAudioClose      = NULL;
           pCdAudioIOControl  = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_OD]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libshared_wup"))
    {
        if(ModuleHandle[EN_SHARED_WUP])
        {
           pWupOpen       = NULL;
           pWupClose      = NULL;
           pWupIOControl  = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_WUP]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libshared_volt"))
    {
        if(ModuleHandle[EN_SHARED_VOLT])
        {
           pVoltOpen       = NULL;
           pVoltClose      = NULL;
           pVoltIOControl  = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_VOLT]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libshared_eol"))
    {
        if(ModuleHandle[EN_SHARED_EOL])
        {
           pEolOpen       = NULL;
           pEolClose      = NULL;
           pEolIOControl  = NULL;
           pEolRead       = NULL;
           pEolWrite      = NULL;
           pFfdOpen       = NULL;
           pFfdClose      = NULL;
           pFfdIOControl  = NULL;
           pFfdRead       = NULL;
           pFfdWrite      = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_EOL]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libshared_adr3ctrl"))
    {
        if(ModuleHandle[EN_SHARED_ADR3])
        {
           pAdr3CtrlOpen       = NULL;
           pAdr3CtrlClose      = NULL;
           pAdr3CtrlIOControl  = NULL;
           pAdr3CtrlRead       = NULL;
           pAdr3CtrlWrite      = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_ADR3]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libcrypt_modul"))
    {
        if(ModuleHandle[EN_CRYPTMODUL])
        {
           pSDCardInfo     = NULL;
           pReadCid        = NULL;
           pVerify_signaturefile       = NULL;
           pGet_signature_verify_status= NULL;
           pGet_signaturefile_type     = NULL;
           pEncryptAesFile = NULL;
           if(dlclose((void*)ModuleHandle[EN_CRYPTMODUL]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libshared_acoustic"))
    {
        if(ModuleHandle[EN_SHARED_ACOUSTIC])
        {
           pAcOutOpen      = NULL;
           pAcOutClose     = NULL;
           pAcOutIOControl = NULL;
           pAcOutWrite     = NULL;
           pAcInOpen       = NULL;
           pAcInClose      = NULL;
           pAcInIOControl  = NULL;
           pAcInRead       = NULL;
           pAcSrcOpen      = NULL;
           pAcSrcClose     = NULL;
           pAcSrcIOControl = NULL;
           pAcCnrOpen      = NULL;
           pAcCnrClose     = NULL;
           pAcCnrIOControl = NULL;
           pAcCnrRead      = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_ACOUSTIC]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libosal_addon"))
    {
        if(ModuleHandle[EN_SHARED_OSAL2])
        {
           pCoreDbgFunc    = NULL;
           pIoDbgFunc      = NULL;
           pPrmFunc        = NULL;
           pCbExc          = NULL;
           pPrcTskInfo     = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_OSAL2]) != -1)
           {
             s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libshared_touch"))
    {
        if(ModuleHandle[EN_SHARED_TOUCH])
        {
           pTouchOpen      = NULL;
           pTouchClose     = NULL;
           pTouchIOControl = NULL;
           pTouchRead      = NULL;
           pTouchWrite     = NULL;
        }
        if(dlclose((void*)ModuleHandle[EN_SHARED_TOUCH]) != -1)
        {
           s32Ret = OSAL_OK;
        }
    }
    else if(strstr(szModuleName,"libshared_aars_dab"))
    {
        if(ModuleHandle[EN_SHARED_AARS_DAB])
        {
           pAaRSDABOpen       = NULL;
           pAaRSDABClose      = NULL;
           pAaRSDABIOControl  = NULL;
           pAaRSDABRead       = NULL;
           pAaRSDABWrite      = NULL;
           if(dlclose((void*)ModuleHandle[EN_SHARED_AARS_DAB]) != -1)
           {
              s32Ret = OSAL_OK;
           }
        }
    }
    else if(strstr(szModuleName,"libshared_aars_ssi32"))
    {
            if(ModuleHandle[EN_SHARED_AARS_SSI32])
            {
               pAaRSSSI32Open       = NULL;
               pAaRSSSI32Close      = NULL;
               pAaRSSSI32IOControl  = NULL;
               pAaRSSSI32Read       = NULL;
               pAaRSSSI32Write      = NULL;
               if(dlclose((void*)ModuleHandle[EN_SHARED_AARS_SSI32]) != -1)
               {
                 s32Ret = OSAL_OK;
               }
            }
    }
    else if(strstr(szModuleName,"libshared_aars_mtd"))
    {
            if(ModuleHandle[EN_SHARED_AARS_MTD])
            {
               pAaRSMTDOpen       = NULL;
               pAaRSMTDClose      = NULL;
               pAaRSMTDIOControl  = NULL;
               pAaRSMTDRead       = NULL;
               pAaRSMTDWrite      = NULL;
               if(dlclose((void*)ModuleHandle[EN_SHARED_AARS_MTD]) != -1)
               {
                 s32Ret = OSAL_OK;
               }
            }
    }
    else if(strstr(szModuleName,"libshared_aars_amfm"))
    {
            if(ModuleHandle[EN_SHARED_AARS_AMFM])
            {
               pAaRSAMFMOpen       = NULL;
               pAaRSAMFMClose      = NULL;
               pAaRSAMFMIOControl  = NULL;
               pAaRSAMFMRead       = NULL;
               pAaRSAMFMWrite      = NULL;
               if(dlclose((void*)ModuleHandle[EN_SHARED_AARS_AMFM]) != -1)
               {
                 s32Ret = OSAL_OK;
               }
            }
    }
    else
    {
       s32Ret = OSAL_ERROR;
    }
    return s32Ret;
}


void vSetBasicFuncPointer(void)
{
   pGetFuncPtr[EN_SHARED_KDS]    = &s32GetKdsFunctionPointer;
   pGetFuncPtr[EN_SHARED_SENSOR] = &s32GetSensorFunctionPointer;
   pGetFuncPtr[EN_SHARED_OD]     = &s32GetOdFunctionPointer;
   pGetFuncPtr[EN_SHARED_WUP]    = &s32GetWupFunctionPointer;
   pGetFuncPtr[EN_SHARED_VOLT]   = &s32GetVoltFunctionPointer;
   pGetFuncPtr[EN_SHARED_EOL]    = &s32GetEolFunctionPointer;
   pGetFuncPtr[EN_SHARED_ADR3]   = &s32GetAdr3CtrlFunctionPointer;
   pGetFuncPtr[EN_CRYPTMODUL]     = &s32GetCryptModulFunctionPointer;
   pGetFuncPtr[EN_SHARED_ACOUSTIC] = &s32GetAcousticFunctionPointer;
   pGetFuncPtr[EN_SHARED_OSAL2]  = &s32GetOsal2FunctionPointer;
   pGetFuncPtr[EN_SHARED_TOUCH]  = &s32GetTouchFunctionPointer;
   pGetFuncPtr[EN_SHARED_BASE]   = &s32GetBaseDrvFunctionPointer;
   pGetFuncPtr[EN_SHARED_AARS_DAB]   = &s32GetAaRSDABFunctionPointer;
   pGetFuncPtr[EN_SHARED_AARS_SSI32] = &s32GetAaRSSSI32FunctionPointer;
   pGetFuncPtr[EN_SHARED_AARS_MTD]   = &s32GetAaRSMTDFunctionPointer;
   pGetFuncPtr[EN_SHARED_AARS_AMFM]   = &s32GetAaRSAMFMFunctionPointer;


   /* Enter OSAL internal implemented device pointer for Trace and PRM */
   rOsalDrvFuncTable[OSAL_EN_DEVID_TRACE].pFuncOpn = &wrapper_trace_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_TRACE].pFuncCls = &wrapper_trace_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_TRACE].pFuncCtl = &wrapper_trace_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_TRACE].pFuncWri = &wrapper_trace_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_TRACE].pFuncRea = &wrapper_trace_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ERRMEM].pFuncOpn = &wrapper_ERRMEM_S32IOOpen;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ERRMEM].pFuncCls = &wrapper_ERRMEM_S32IOClose;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ERRMEM].pFuncCtl = &wrapper_ERRMEM_s32IOControl;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ERRMEM].pFuncWri = &wrapper_ERRMEM_s32IOWrite;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ERRMEM].pFuncRea = &wrapper_ERRMEM_s32IORead;

   rOsalDrvFuncTable[OSAL_EN_DEVID_KDS].pFuncOpn = &wrapper_kds_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_KDS].pFuncCls = &wrapper_kds_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_KDS].pFuncCtl = &wrapper_kds_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_KDS].pFuncWri = &wrapper_kds_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_KDS].pFuncRea = &wrapper_kds_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH].pFuncOpn = &wrapper_acousticout_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH].pFuncCls = &wrapper_acousticout_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH].pFuncCtl = &wrapper_acousticout_ioctl;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH].pFuncWri = &wrapper_acousticout_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH].pFuncRea = NULL;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC].pFuncOpn = &wrapper_acousticout_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC].pFuncCls = &wrapper_acousticout_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC].pFuncCtl = &wrapper_acousticout_ioctl;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC].pFuncWri = &wrapper_acousticout_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC].pFuncRea = NULL;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH].pFuncOpn = &wrapper_acousticin_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH].pFuncCls = &wrapper_acousticin_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH].pFuncCtl = &wrapper_acousticin_ioctl;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH].pFuncRea = &wrapper_acousticin_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_AC_ECNR_IF_SPEECH].pFuncOpn = &wrapper_acousticecnr_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AC_ECNR_IF_SPEECH].pFuncCls = &wrapper_acousticecnr_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AC_ECNR_IF_SPEECH].pFuncCtl = &wrapper_acousticecnr_ioctl;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AC_ECNR_IF_SPEECH].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AC_ECNR_IF_SPEECH].pFuncRea = &wrapper_acousticecnr_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTIC_IF_SRC].pFuncOpn = &wrapper_acousticsrc_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTIC_IF_SRC].pFuncCls = &wrapper_acousticsrc_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTIC_IF_SRC].pFuncCtl = &wrapper_acousticsrc_ioctl;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTIC_IF_SRC].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACOUSTIC_IF_SRC].pFuncRea = NULL;

   rOsalDrvFuncTable[OSAL_EN_DEVID_AUXCLK].pFuncOpn = &wrapper_auxclock_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AUXCLK].pFuncCls = &wrapper_auxclock_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AUXCLK].pFuncCtl = &wrapper_auxclock_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AUXCLK].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AUXCLK].pFuncRea = &wrapper_auxclock_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_DIAG_EOL].pFuncOpn = &wrapper_diag_eol_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_DIAG_EOL].pFuncCls = &wrapper_diag_eol_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_DIAG_EOL].pFuncCtl = &wrapper_diag_eol_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_DIAG_EOL].pFuncWri = &wrapper_diag_eol_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_DIAG_EOL].pFuncRea = &wrapper_diag_eol_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_PRM].pFuncOpn = &wrapper_prm_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_PRM].pFuncCls = &wrapper_prm_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_PRM].pFuncCtl = &wrapper_prm_io_ioctl;
   rOsalDrvFuncTable[OSAL_EN_DEVID_PRM].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_PRM].pFuncRea = NULL;


   rOsalDrvFuncTable[OSAL_EN_DEVID_FFD].pFuncOpn = &wrapper_ffd_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_FFD].pFuncCls = &wrapper_ffd_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_FFD].pFuncCtl = &wrapper_ffd_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_FFD].pFuncWri = &wrapper_ffd_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_FFD].pFuncRea = &wrapper_ffd_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_TOUCHSCREEN].pFuncOpn = &wrapper_touchscreen_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_TOUCHSCREEN].pFuncCls = &wrapper_touchscreen_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_TOUCHSCREEN].pFuncCtl = &wrapper_touchscreen_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_TOUCHSCREEN].pFuncWri = &wrapper_touchscreen_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_TOUCHSCREEN].pFuncRea = &wrapper_touchscreen_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_CDCTRL].pFuncOpn = &wrapper_cdctrl_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CDCTRL].pFuncCls = &wrapper_cdctrl_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CDCTRL].pFuncCtl = &wrapper_cdctrl_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CDCTRL].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CDCTRL].pFuncRea = NULL;

   rOsalDrvFuncTable[OSAL_EN_DEVID_CDAUDIO].pFuncOpn = &wrapper_cdaudio_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CDAUDIO].pFuncCls = &wrapper_cdaudio_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CDAUDIO].pFuncCtl = &wrapper_cdaudio_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CDAUDIO].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CDAUDIO].pFuncRea = NULL;

   rOsalDrvFuncTable[OSAL_EN_DEVID_GPS].pFuncOpn = &wrapper_gps_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GPS].pFuncCls = &wrapper_gps_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GPS].pFuncCtl = &wrapper_gps_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GPS].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GPS].pFuncRea = &wrapper_gps_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_GNSS].pFuncOpn = &wrapper_gnss_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GNSS].pFuncCls = &wrapper_gnss_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GNSS].pFuncCtl = &wrapper_gnss_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GNSS].pFuncWri = &wrapper_gnss_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GNSS].pFuncRea = &wrapper_gnss_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_GPIO].pFuncOpn = &wrapper_gpio_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GPIO].pFuncCls = &wrapper_gpio_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GPIO].pFuncCtl = &wrapper_gpio_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GPIO].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GPIO].pFuncRea = NULL;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ADC0].pFuncOpn = &wrapper_adc_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ADC0].pFuncCls = &wrapper_adc_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ADC0].pFuncCtl = &wrapper_adc_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ADC0].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ADC0].pFuncRea = &wrapper_adc_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_VOLT].pFuncOpn = &wrapper_volt_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_VOLT].pFuncCls = &wrapper_volt_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_VOLT].pFuncCtl = &wrapper_volt_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_VOLT].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_VOLT].pFuncRea = NULL;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ODO].pFuncOpn = &wrapper_odometer_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ODO].pFuncCls = &wrapper_odometer_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ODO].pFuncCtl = &wrapper_odometer_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ODO].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ODO].pFuncRea = &wrapper_odometer_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_GYRO].pFuncOpn = &wrapper_gyro_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GYRO].pFuncCls = &wrapper_gyro_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GYRO].pFuncCtl = &wrapper_gyro_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GYRO].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_GYRO].pFuncRea = &wrapper_gyro_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ABS].pFuncOpn = &wrapper_abs_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ABS].pFuncCls = &wrapper_abs_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ABS].pFuncCtl = &wrapper_abs_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ABS].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ABS].pFuncRea = &wrapper_abs_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ACC].pFuncOpn = &wrapper_acc_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACC].pFuncCls = &wrapper_acc_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACC].pFuncCtl = &wrapper_acc_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACC].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ACC].pFuncRea = &wrapper_acc_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_WUP].pFuncOpn = &wrapper_wup_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_WUP].pFuncCls = &wrapper_wup_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_WUP].pFuncCtl = &wrapper_wup_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_WUP].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_WUP].pFuncRea = NULL;

   rOsalDrvFuncTable[OSAL_EN_DEVID_CHENC].pFuncOpn = &wrapper_ChEnc_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CHENC].pFuncCls = &wrapper_ChEnc_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CHENC].pFuncCtl = &wrapper_ChEnc_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CHENC].pFuncWri = NULL;
   rOsalDrvFuncTable[OSAL_EN_DEVID_CHENC].pFuncRea = NULL;

   rOsalDrvFuncTable[OSAL_EN_DEVID_ADR3CTRL].pFuncOpn = &wrapper_adr3_ctrl_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ADR3CTRL].pFuncCls = &wrapper_adr3_ctrl_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ADR3CTRL].pFuncCtl = &wrapper_adr3_ctrl_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ADR3CTRL].pFuncWri = &wrapper_adr3_ctrl_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_ADR3CTRL].pFuncRea = &wrapper_adr3_ctrl_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_PRAM].pFuncOpn = &wrapper_pram_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_PRAM].pFuncCls = &wrapper_pram_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_PRAM].pFuncCtl = &wrapper_pram_io_ctrl;
   rOsalDrvFuncTable[OSAL_EN_DEVID_PRAM].pFuncWri = &wrapper_pram_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_PRAM].pFuncRea = &wrapper_pram_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_DAB].pFuncOpn = &wrapper_aars_dab_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_DAB].pFuncCls = &wrapper_aars_dab_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_DAB].pFuncCtl = &wrapper_aars_dab_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_DAB].pFuncWri = &wrapper_aars_dab_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_DAB].pFuncRea = &wrapper_aars_dab_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_SSI32].pFuncOpn = &wrapper_aars_ssi32_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_SSI32].pFuncCls = &wrapper_aars_ssi32_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_SSI32].pFuncCtl = &wrapper_aars_ssi32_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_SSI32].pFuncWri = &wrapper_aars_ssi32_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_SSI32].pFuncRea = &wrapper_aars_ssi32_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_MTD].pFuncOpn = &wrapper_aars_mtd_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_MTD].pFuncCls = &wrapper_aars_mtd_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_MTD].pFuncCtl = &wrapper_aars_mtd_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_MTD].pFuncWri = &wrapper_aars_mtd_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_MTD].pFuncRea = &wrapper_aars_mtd_io_read;

   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_AMFM].pFuncOpn = &wrapper_aars_amfm_io_open;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_AMFM].pFuncCls = &wrapper_aars_amfm_io_close;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_AMFM].pFuncCtl = &wrapper_aars_amfm_io_control;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_AMFM].pFuncWri = &wrapper_aars_amfm_io_write;
   rOsalDrvFuncTable[OSAL_EN_DEVID_AARS_AMFM].pFuncRea = &wrapper_aars_amfm_io_read;
}

tS32 s32CheckLoadedModuls(const char* szModuleName)
{
   /* against double loading */
   if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_BASE].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_BASE] == 0)return EN_SHARED_BASE;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_WUP].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_WUP] == 0)return EN_SHARED_WUP;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_VOLT].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_VOLT] == 0)return EN_SHARED_VOLT;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_KDS].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_KDS] == 0)return EN_SHARED_KDS;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_EOL].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_EOL] == 0)return EN_SHARED_EOL;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_SENSOR].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_SENSOR] == 0)return EN_SHARED_SENSOR;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_OD].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_OD] == 0)return EN_SHARED_OD;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_ADR3].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_ADR3] == 0)return EN_SHARED_ADR3;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_ACOUSTIC].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_ACOUSTIC] == 0)return EN_SHARED_ACOUSTIC;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_CRYPTMODUL].cLibraryNames))
   {
     if(ModuleHandle[EN_CRYPTMODUL] == 0)return EN_CRYPTMODUL;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_OSAL2].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_OSAL2] == 0)return EN_SHARED_OSAL2;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_TOUCH].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_TOUCH] == 0)return EN_SHARED_TOUCH;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_AARS_DAB].cLibraryNames))
     {
     if(ModuleHandle[EN_SHARED_AARS_DAB] == 0)return EN_SHARED_AARS_DAB;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_AARS_SSI32].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_AARS_SSI32] == 0)return EN_SHARED_AARS_SSI32;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_AARS_MTD].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_AARS_MTD] == 0)return EN_SHARED_AARS_MTD;
   }
   else if(!strcmp(szModuleName,pOsalData->rLibrary[EN_SHARED_AARS_AMFM].cLibraryNames))
   {
     if(ModuleHandle[EN_SHARED_AARS_AMFM] == 0)return EN_SHARED_AARS_AMFM;
   }
   return OSAL_ERROR;
}


/*****************************************************************************
*
* FUNCTION:    s32LoadModul
*
* DESCRIPTION: This function loads the KDS shared library and set the function pointer
*
* PARAMETER:   Module Name
*
* RETURNVALUE: s32ReturnValue
*                 it is the function return value:
*                 - OSAL_OK if everything goes right;
*                 - OSAL_ERROR otherwise.
* HISTORY:
* Date      |   Modification                         | Authors
* 17.07.13  | Initial revision                       | MRK2HI
* 12.08.14  | Update trace for s32LoadModul          | SWM2KOR
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
tS32 s32LoadModul(const char* szModuleName)
{
   tS32 s32Ret = OSAL_ERROR;
   tS32 s32Pid =  OSAL_ProcessWhoAmI();
   char *error;
   void* pModuleHandle = NULL;
   static void* pAsoundHandle = NULL;
   static void* pUdevHandle = NULL;

   if(u32PrcExitStep > 0)return OSAL_ERROR;

   dlerror();    /* Clear any existing error */

   if(LLD_bIsTraceActive(TR_COMP_OSALCORE,TR_LEVEL_COMPONENT)||(u32OsalSTrace & 0x00000002))
   {
      TraceString("Load Driver %s PID:%d %s",szModuleName,s32Pid,prProcDat[s32FindProcEntry(s32Pid)].pu8CommandLine);
   }

   s32Ret = s32CheckLoadedModuls(szModuleName);
   if(s32Ret == OSAL_ERROR)
   {
      TraceString("PID:%d %s already loaded !",s32Pid,szModuleName);
      return OSAL_OK;
   }

   if(strstr(szModuleName,"shared_acoustic"))
   {
      if(LLD_bIsTraceActive(TR_COMP_OSALCORE,TR_LEVEL_COMPONENT)||(u32OsalSTrace & 0x00000002))
      {
          TraceString("Loading %s by PID:%d !!!!!!!!!!!!!","/usr/lib/libasound.so",getpid());
      }
      if((pAsoundHandle = dlopen("/usr/lib/libasound.so", RTLD_NOW | RTLD_GLOBAL)) == NULL)
      {
          TraceString("Loading %s by PID:%d failed !!!!!!!!!!!!!","/usr/lib/libasound.so",getpid());
      }
   }
   else if(strstr(szModuleName,"shared_cd"))
   {
      if(LLD_bIsTraceActive(TR_COMP_OSALCORE,TR_LEVEL_COMPONENT)||(u32OsalSTrace & 0x00000002))
      {
         TraceString("Loading %s by PID:%d !!!!!!!!!!!!!","/lib/libudev.so",getpid());
      }
      if((pUdevHandle = dlopen("/lib/libudev.so", RTLD_NOW | RTLD_GLOBAL)) == NULL)
      {
          TraceString("Loading %s by PID:%d failed !!!!!!!!!!!!!","/lib/libudev.so",getpid());
      }
   }
   
   pModuleHandle = dlopen(szModuleName, RTLD_NOW );
   if(pModuleHandle == NULL)
   {
      if ((error = dlerror()) != NULL)  
      {
        TraceString("PID:%d Error:%s when loading %s Driver!!!",s32Pid,error,szModuleName);
        vWritePrintfErrmem("PID:%d Error:%s when loading %s Driver!!! \n",s32Pid,error,szModuleName);
      }
      else
      {
         TraceString("PID:%d Error:? when loading %s Driver!!!",s32Pid,szModuleName);
         vWritePrintfErrmem("PID:%d Error:? when loading %s Driver!!! \n",s32Pid,szModuleName);
      }
      s32Ret = OSAL_ERROR;
   }
   else
   {
       s32Pid = pGetFuncPtr[s32Ret](pModuleHandle); 
       if(s32Pid == OSAL_ERROR)
       {
          dlclose(pModuleHandle);
          ModuleHandle[s32Ret] = OSAL_NULL;
       }
       else
       {
          ModuleHandle[s32Ret] = (uintptr_t)pModuleHandle;
       }
   }
   return s32Ret;
}

#ifdef __cplusplus
}
#endif

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
