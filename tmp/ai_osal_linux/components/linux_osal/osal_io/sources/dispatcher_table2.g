
struct OsalDevices {
        const char*   name;                         /* name of the device */
        const tOpenPtr      ptr_s32IOOpen;          /* pointer to open function */
        const tClosePtr     ptr_s32IOClose;         /* pointer to close function */
        const tCreatePtr    ptr_s32IOCreate;        /* pointer to create function */
        const tRemovePtr    ptr_s32IORemove;        /* pointer to remove function */
        const tIOControlPtr ptr_s32IOControl;       /* pointer to control function */
        const tReadPtr      ptr_s32IORead;          /* pointer to read function */
        const tWritePtr     ptr_s32IOWrite;         /* pointer to write function */
        const tU32          device_local_id;        /* device id (internally used for the driver */
        const tBool         bFileSystem;            /* TRUE -> is a file system */
        OSAL_tenDevAccess   ExclusiveAccess;        /* exclusive access variable */
        tU32 nJobQueue;                             /* jobqueue number for no filesystems */
        tS32 prm_index;                             /* prm index for exclusive access */
};

%%
"/dev/trace",              NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_TRACE,                  FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/errmem",             NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_ERRMEM,                 FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/kds",                NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_KDS,                    FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/acousticout/speech", NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_ACOUSTICOUT_IF_SPEECH,  FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/acousticout/music",  NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_ACOUSTICOUT_IF_MUSIC,   FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/acousticin/speech",  NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_ACOUSTICIN_IF_SPEECH,   FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/acousticecnr/speech",NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_AC_ECNR_IF_SPEECH,      FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/acoustic/src",       NULL, NULL, NULL, NULL,  NULL, NULL, NULL, OSAL_EN_DEVID_ACOUSTIC_IF_SRC,       FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/auxclock",           NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_AUXCLK,                 FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/diag/eol",           NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_DIAG_EOL,               FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/prm",                NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_PRM,                    FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/ffd",                NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_FFD,                    FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/pram",               NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_PRAM,                   FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/cdctrl",             NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_CDCTRL,                 FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, OSAL_EN_DEVID_CDCTRL  
"/dev/cdaudio",            NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_CDAUDIO,                FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, OSAL_EN_DEVID_CDAUDIO  
"/dev/gps",                NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_GPS,                    FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/odometer",           NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_ODO,                    FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/abs",                NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_ABS,                    FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/acc",                NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_ACC,                    FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/gyro",               NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_GYRO,                   FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/chenc",              NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_CHENC,                  FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/gnss",               NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_GNSS,                   FALSE,  OSAL_EN_DEV_NOT_USED,     C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/gpio",               NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_GPIO,                   FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/adc",                NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_ADC0,                   FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/volt",               NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_VOLT,                   FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/wup",                NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_WUP,                    FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/adr3ctrl",           NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_ADR3CTRL,               FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/aars/dab",           NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_AARS_DAB,               FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/aars/ssi32",         NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_AARS_SSI32,             FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/aars/mtd",           NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_AARS_MTD,               FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/aars/amfm",          NULL, NULL, NULL, NULL,  NULL, NULL, NULL,OSAL_EN_DEVID_AARS_AMFM,               FALSE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/ramdisk",   wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_RAMDISK,   TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/data",      wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_DATA,      TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/ffs",       wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_FFS_FFS,   TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/ffs2",      wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_FFS_FFS2,  TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/ffs3",      wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_FFS_FFS3,  TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/ffs4",      wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_FFS_FFS4,  TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/cryptcard", wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_CRYPT_CARD,TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/cryptnav",  wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_CRYPTNAV,  TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/cryptnavroot",wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_CRYPTNAVROOT,TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/navdb",     wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_NAVDB,     TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/root",      wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_ROOT,      TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/media",     wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_DEV_MEDIA, TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/local",     wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_LOCAL,     TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/cdrom",     wrapper_fs_io_open, wrapper_fs_io_close, wrapper_fs_io_create, wrapper_fs_io_remove,wrapper_fs_io_control, wrapper_fs_io_read, wrapper_fs_io_write ,OSAL_EN_DEVID_DVD,       TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
"/dev/registry",  wrapper_registry_io_open, wrapper_registry_io_close, wrapper_registry_io_create, wrapper_registry_io_remove,wrapper_registry_io_control, NULL, NULL ,OSAL_EN_DEVID_REGISTRY,     TRUE,  OSAL_EN_DEV_MULTIPLE_USE, C_INVALID_JOB, PRM_ILLEGAL_INDEX  
%%