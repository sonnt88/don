/*
 * SequencialBlockReader.h
 *
 *  Created on: Jan 19, 2012
 *      Author: oto4hi
 */

#ifndef SEQUENCIALBLOCKREADER_H_
#define SEQUENCIALBLOCKREADER_H_

#include "Interfaces/IReader.h"

class SequencialBlockReader : public IReader {
public:
	SequencialBlockReader();
	virtual void* read(unsigned int& Length);
	virtual ~SequencialBlockReader();
};

#endif /* SEQUENCIALBLOCKREADER_H_ */
