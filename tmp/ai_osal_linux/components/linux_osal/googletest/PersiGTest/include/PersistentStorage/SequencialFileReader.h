/*
 * SequencialFileReader.h
 *
 *  Created on: Nov 23, 2011
 *      Author: oto4hi
 */

#ifndef SEQUENCIALFILEREADER_H_
#define SEQUENCIALFILEREADER_H_

#include "Interfaces/IReader.h"

class SequencialFileReader : public IReader
{
private:
	char* path;
public:
	SequencialFileReader(const char* Path);
	virtual void* read(unsigned int& Length);
	virtual ~SequencialFileReader();
};


#endif /* SEQUENCIALFILEREADER_H_ */
