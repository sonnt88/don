/*
 * SequencialFileWriter.h
 *
 *  Created on: Nov 23, 2011
 *      Author: oto4hi
 */

#ifndef SEQUENCIALFILEWRITER_H_
#define SEQUENCIALFILEWRITER_H_

#include "Interfaces/IWriter.h"

class SequencialFileWriter : public IWriter
{
private:
	char* path;
public:
	SequencialFileWriter(const char* Path);
	virtual void write(void* Buffer, unsigned int Length);
	virtual void remove();
	virtual ~SequencialFileWriter();
};

#endif /* SEQUENCIALFILEWRITER_H_ */
