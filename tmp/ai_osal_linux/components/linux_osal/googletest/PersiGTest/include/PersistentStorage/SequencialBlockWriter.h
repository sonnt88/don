/*
 * SequencialBlockWriter.h
 *
 *  Created on: Jan 19, 2012
 *      Author: oto4hi
 */

#ifndef SEQUENCIALBLOCKWRITER_H_
#define SEQUENCIALBLOCKWRITER_H_

#include "Interfaces/IWriter.h"

class SequencialBlockWriter : public IWriter {
public:
	SequencialBlockWriter();
	virtual void write(void* Buffer, unsigned int Length);
	virtual void remove();
	virtual ~SequencialBlockWriter();
};

#endif /* SEQUENCIALBLOCKWRITER_H_ */
