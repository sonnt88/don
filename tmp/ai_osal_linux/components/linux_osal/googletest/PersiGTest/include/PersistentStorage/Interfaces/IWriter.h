/*
 * IWriter.h
 *
 *  Created on: Dec 12, 2011
 *      Author: oto4hi
 */

#ifndef IWRITER_H_
#define IWRITER_H_

#include <PersistentStorage/IOException.h>

class IWriter
{
public:
	virtual void write(void* pBuffer, unsigned int Length)
	{
		throw new IOException("Error: try to call IWrite::write() (pure interface function).");
	}

	virtual void remove()
	{
		throw new IOException("Error: try to call IWrite::erase() (pure interface function).");
	}

	virtual ~IWriter() {}
};

#endif /* IWRITER_H_ */
