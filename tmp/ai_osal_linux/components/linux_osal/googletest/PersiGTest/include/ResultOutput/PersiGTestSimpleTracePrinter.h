#ifndef PERSIGTESTSIMPLETRACEPRINTER_H
#define PERSIGTESTSIMPLETRACEPRINTER_H

#include "traceprintf.h"
#include "IPersiGTestPrinter.h"

namespace testing {
	namespace persistence {

		class PersiGTestSimpleTracePrinter : public ::testing::persistence::IPersiGTestPrinter {
		  public:
			virtual void OnTestStart(const ::testing::TestInfo& test_info)
			{
			  TracePrintf(TR_LEVEL_FATAL, "Starting test %s.%s", test_info.test_case_name(), test_info.name());
			}

			virtual void OnTestPartResult(const ::testing::TestPartResult& test_part_result)
			{
			  if (test_part_result.failed())
				TracePrintf(TR_LEVEL_FATAL, "FAILURE: %s:%d\n%s",
				  test_part_result.file_name(),
				  test_part_result.line_number(),
				  test_part_result.summary() );
			}

			virtual void OnTestEnd(const ::testing::TestInfo& test_info)
			{
			  if (test_info.result()->Passed())
				TracePrintf(TR_LEVEL_FATAL, "%s.%s: PASSED", test_info.test_case_name(), test_info.name());
			  else
				TracePrintf(TR_LEVEL_FATAL, "%s.%s: FAILED", test_info.test_case_name(), test_info.name());
			}

			void OnTestIterationEnd(const ::testing::UnitTest& unit_test, int iteration, ::testing::persistence::PersistentFrameworkState* state)
			{
				unsigned int failedTestCount = state->GetFailedTestCount();
				unsigned int passedTestCount = state->GetPassedTestCount();
				unsigned int totalTestCount = unit_test.total_test_count();

				TracePrintf(TR_LEVEL_FATAL, "test summary: TOTAL: %d, PASSED: %d, FAILED: %d, DISABLED: %d",
					totalTestCount, passedTestCount, failedTestCount, totalTestCount - (passedTestCount + failedTestCount));
			}
		};

	}
}

#endif //PERSIGTESTSIMPLETRACEPRINTER_H

