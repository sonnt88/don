/*
 * gtest-FrameworkStateWriter.h
 *
 *  Created on: Nov 22, 2011
 *      Author: oto4hi
 */

#ifndef GTEST_FRAMEWORKSTATEWRITER_H_
#define GTEST_FRAMEWORKSTATEWRITER_H_

#include <gtest/gtest.h>
#include "PersistentFrameworkState.h"

#define GTEST_IMPLEMENTATION_ 1
#include <src/gtest-internal-inl.h>

namespace testing {
	namespace persistence {

		class PersistentFrameworkStateManager : public ::testing::TestEventListener {
		private:
			void restoreInternalGTestState();
			bool firstTestAfterRestore;
		protected:
			PersistentFrameworkState* state;
			bool isFirstTestAfterRestore() { return this->firstTestAfterRestore; }
		public:
			PersistentFrameworkStateManager(int argc, char** argv, IReader* Reader, IWriter* Writer);
			~PersistentFrameworkStateManager();

			/* needed to restore the test framework settings of the first run */
			int GetArgC() { return this->state->GetArgC(); }

			/* needed to restore the test framework settings of the first run */
			char** GetArgV() { return this->state->GetArgV(); }

			/* needed to restore the test framework settings of the first run */
			void SetArgs(int argc, char** argv);

			virtual void OnTestProgramStart(const ::testing::UnitTest& unit_test);
			virtual void OnTestIterationStart(const ::testing::UnitTest& unit_test, int iteration);
			virtual void OnEnvironmentsSetUpStart(const ::testing::UnitTest& unit_test);
			virtual void OnEnvironmentsSetUpEnd(const ::testing::UnitTest& unit_test);
			virtual void OnTestCaseStart(const ::testing::TestCase& test_case);
			virtual void OnTestStart(const ::testing::TestInfo& test_info);
			virtual void OnTestPartResult(const ::testing::TestPartResult& test_part_result);
			virtual void OnTestEnd(const ::testing::TestInfo& test_info);
			virtual void OnTestCaseEnd(const ::testing::TestCase& test_case);
			virtual void OnEnvironmentsTearDownStart(const ::testing::UnitTest& unit_test);
			virtual void OnEnvironmentsTearDownEnd(const ::testing::UnitTest& unit_test);
			virtual void OnTestIterationEnd(const ::testing::UnitTest& unit_test, int iteration);
			virtual void OnTestProgramEnd(const ::testing::UnitTest& unit_test);

			void ResetTest();
			void BreakAllTests();
		};
	};
};

#endif /* GTEST_FRAMEWORKSTATEWRITER_H_ */
