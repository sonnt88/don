/*
 * PersistentFrameworkState.h
 *
 *  Created on: Nov 23, 2011
 *      Author: oto4hi
 */

#ifndef PERSISTENTFRAMEWORKSTATE_H_
#define PERSISTENTFRAMEWORKSTATE_H_

#include <PersistentStorage/Interfaces/IReader.h>
#include <PersistentStorage/Interfaces/IWriter.h>

#include <PersistentStateModels/Interfaces/IPersistentState.h>


#include <vector>

namespace testing {
	namespace persistence {

		typedef enum {
			SHOULD_RUN,
			SHOULD_NOT_RUN,
			FAILED,
			PASSED
		} TESTEXECSTATE;

		class PersistentFrameworkState : public IPersistentState{
		private:
			IReader* reader;
			IWriter* writer;

			int argc;
			char** argv;

			unsigned int currentTestCase;
			unsigned int currentTest;

			int iterationCount;
			unsigned int currentIteration;

			std::vector< std::vector<TESTEXECSTATE>* > testResults;
			int randomSeed;

			unsigned int getSerializedSize();
			void readStringFromBuffer(char** pDest, void* pBuffer, unsigned int bufferLength, unsigned int& offset);
			void writeToBuffer(void* pBuffer, void* pSrc, unsigned int contentSize, unsigned int bufferLength, unsigned int& offset);
			void readFromBuffer(void* pDest, void* pBuffer, unsigned int contentSize, unsigned int bufferLength, unsigned int& offset);
			void clearArgs();
			void clearTestResults();
			unsigned int countTestResultsWithValue(unsigned TestCaseIndex, TESTEXECSTATE Value);

		public:
			PersistentFrameworkState(IReader* Reader, IWriter* Writer);
			PersistentFrameworkState(const PersistentFrameworkState& s);
			~PersistentFrameworkState();

			bool IsStructurallyEquivalent(const PersistentFrameworkState& s);

			void Init();
			virtual void Persist();
			virtual bool Restore();
			void Remove();
			void Clear();

			void DimTestCases(unsigned int NumOfTestCases);
			void DimTests(unsigned int TestCaseIndex, unsigned int NumOfTests);
			void SetResult(TESTEXECSTATE Result);
			void SetResult(unsigned int TestCaseIndex, unsigned int TestIndex, TESTEXECSTATE Result);
			TESTEXECSTATE GetResult(unsigned int TestCaseIndex, unsigned int TestIndex);
			void SetArgs(int argc, char** argv);

			unsigned int GetTestCaseCount();

			// count tests for a single test case
			unsigned int GetTestCount(unsigned TestCaseIndex);
			unsigned int GetPassedTestCount(unsigned TestCaseIndex);
			unsigned int GetFailedTestCount(unsigned TestCaseIndex);

			// count all tests
			unsigned int GetTestCount();
			unsigned int GetPassedTestCount();
			unsigned int GetFailedTestCount();

			// simple getters
			int GetArgC() { return this->argc; }
			char** GetArgV() { return this->argv; }
			int GetCurrentTestCase() { return this->currentTestCase; }
			int GetCurrentTest() { return this->currentTest; }
			int GetIterationCount() { return this->iterationCount; }
			int GetCurrentIteration() { return this->currentIteration; }
			int GetRandomSeed() { return this->randomSeed; }

			// simple setters
			void SetCurrentTestCase(int CurrentTestCase) { this->currentTestCase = CurrentTestCase; }
			void SetCurrentTest(int CurrentTest) { this->currentTest = CurrentTest; }
			void SetIterationCount(int IterationCount) { this->iterationCount = IterationCount; }
			void SetRandomSeed(int RandomSeed) { this->randomSeed = RandomSeed; }
			void NextIteration();
		};

	};
};

#endif /* PERSISTENTFRAMEWORKSTATE_H_ */
