/*
 * TestEventListenerTraceDecorator.h
 *
 *  Created on: Dec 9, 2011
 *      Author: oto4hi
 */

#ifndef TESTEVENTLISTENERTRACEDECORATOR_H_
#define TESTEVENTLISTENERTRACEDECORATOR_H_

#include <ResultOutput/IPersiGTestPrinter.h>
#include <PersistentStateModels/FrameworkState/PersistentFrameworkStateManager.h>

namespace testing {
	namespace persistence {
		class TraceablePersistentFrameworkStateManager : public PersistentFrameworkStateManager{
		private:
			IPersiGTestPrinter* printer;
		public:
			TraceablePersistentFrameworkStateManager(int argc, char** argv, IReader* Reader, IWriter* Writer, IPersiGTestPrinter* Printer);
			virtual ~TraceablePersistentFrameworkStateManager();
			virtual void OnTestProgramStart(const UnitTest& unit_test);
			virtual void OnTestIterationStart(const UnitTest& unit_test, int iteration);
			virtual void OnEnvironmentsSetUpStart(const UnitTest& unit_test);
			virtual void OnEnvironmentsSetUpEnd(const UnitTest& unit_test);
			virtual void OnTestCaseStart(const TestCase& test_case);
			virtual void OnTestStart(const TestInfo& test_info);
			virtual void OnTestPartResult(const TestPartResult& test_part_result);
			virtual void OnTestEnd(const TestInfo& test_info);
			virtual void OnTestCaseEnd(const TestCase& test_case);
			virtual void OnEnvironmentsTearDownStart(const UnitTest& unit_test);
			virtual void OnEnvironmentsTearDownEnd(const UnitTest& unit_test);
			virtual void OnTestIterationEnd(const UnitTest& unit_test, int iteration);
			virtual void OnTestProgramEnd(const UnitTest& unit_test);
		};
	}
}


#endif /* TESTEVENTLISTENERTRACEDECORATOR_H_ */
