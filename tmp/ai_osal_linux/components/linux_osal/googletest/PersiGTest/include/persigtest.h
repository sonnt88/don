/*
 * persigtest.h
 *
 *  Created on: Feb 15, 2012
 *      Author: oto4hi
 */

#ifndef PERSIGTEST_H_
#define PERSIGTEST_H_

#include <PersistentStateModels/TestState/PersistentTestState.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <ResultOutput/traceprintf.h>

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif //_MSC_VER

#define HELLO() TracePrintf(TR_LEVEL_FATAL, "I am \"%s\".", __PRETTY_FUNCTION__)
#define BREAK_TEST() _BREAK_TEST(HasFailure())

void _BREAK_TEST(bool hasFailure);

#endif /* PERSIGTEST_H_ */
