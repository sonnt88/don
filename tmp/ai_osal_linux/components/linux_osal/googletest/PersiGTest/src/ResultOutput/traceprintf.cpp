#ifdef OSAL_OS

#include <ResultOutput/traceprintf.h>

tVoid vTracePrintf(TR_tenTraceLevel TraceLevel, tPCChar pchFormat, va_list argList)
{
	tChar pcBuffer[1024];
	tU32 messageLength = 0;

	pcBuffer[0] = 0x1a;
	pcBuffer[1] = 0x02;

	messageLength = (tU32)vsnprintf(pcBuffer+2, 1022, pchFormat, argList);
	pcBuffer[messageLength+2] = '\0';
	pcBuffer[1023] = '\0';

#ifndef TSIM_OSAL
	LLD_vTrace(TR_COMP_OSALTEST, TraceLevel ,pcBuffer, messageLength+2);
#endif
	return;
}

tVoid TracePrintf(TR_tenTraceLevel TraceLevel, tPCChar pchFormat, ...)
{
	va_list argList;
	va_start(argList, pchFormat);

	vTracePrintf(TraceLevel, pchFormat, argList);

	va_end(argList);
}

#endif

