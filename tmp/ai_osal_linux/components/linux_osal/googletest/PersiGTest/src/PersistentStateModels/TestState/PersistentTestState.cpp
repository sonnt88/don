/*
 * PersistentTestState.cpp
 *
 *  Created on: Dec 19, 2011
 *      Author: oto4hi
 */

#include <PersistentStateModels/TestState/PersistentTestState.h>
#include <PersistentStorage/IOFactory.h>

using namespace ::testing::persistence;

void PersistentTestState::clear()
{
	if (pBuffer) {
		free(pBuffer);
		pBuffer = NULL;
		bufferLength = 0;
	}
}

void PersistentTestState::Persist()
{
	if (!pBuffer || bufferLength == 0)
		throw new IOException("Error: nothing to persist.");

	writer->write(this->pBuffer, this->bufferLength);
}

bool PersistentTestState::Restore()
{
	void* pBuffer = NULL;
	unsigned int length = 0;

	pBuffer = reader->read(length);

	if (pBuffer)
	{
		this->bufferLength = length;
		this->pBuffer = pBuffer;

		writer->remove();

		return true;
	}

	return false;
}

PersistentTestState::PersistentTestState(IReader* Reader, IWriter* Writer)
{
	if (!Reader)
		throw new testing::persistence::PersistentStateException("Error: Reader is NULL.");

	if (!Writer)
		throw new testing::persistence::PersistentStateException("Error: Writer is NULL");

	this->reader = Reader;
	this->writer = Writer;

	this->pBuffer = NULL;
	this->bufferLength = 0;
}

void PersistentTestState::SetBuffer(void* pBuffer, unsigned int Length)
{
	clear();
	this->pBuffer = malloc( Length );
	if (!this->pBuffer)
		throw new std::bad_alloc();

	memcpy(this->pBuffer, pBuffer, Length);
	this->bufferLength = Length;
}

void* PersistentTestState::GetBuffer(unsigned int& Length)
{
	if (!this->pBuffer || this->bufferLength == 0)
	{
		Length = 0;
		return NULL;
	}

	Length = this->bufferLength;
	return this->pBuffer;
}

PersistentTestState::~PersistentTestState()
{
	clear();
}

void PERSIST(void* pBuffer, unsigned int length)
{
	IReader* reader = IOFactory::CreateTestStateReader();
	IWriter* writer = IOFactory::CreateTestStateWriter();
	testing::persistence::PersistentTestState* persistentState = new testing::persistence::PersistentTestState(reader, writer);

	persistentState->SetBuffer(pBuffer, length);
	persistentState->Persist();

	delete persistentState;
	delete reader;
	delete writer;
}

bool RESTORE(void*& pBuffer, unsigned int& length)
{
	bool retVal = false;
	IReader* reader = IOFactory::CreateTestStateReader();
	IWriter* writer = IOFactory::CreateTestStateWriter();
	testing::persistence::PersistentTestState* persistentState = new testing::persistence::PersistentTestState(reader, writer);

	if (persistentState->Restore())
	{
		persistentState->GetBuffer(length);
		pBuffer = malloc (length);
		memcpy(pBuffer, persistentState->GetBuffer(length), length);
		retVal = true;
	}

	delete persistentState;
	delete reader;
	delete writer;

	return retVal;
}
