/*
 * PersistentFrameworkStateException.cpp
 *
 *  Created on: Dec 8, 2011
 *      Author: oto4hi
 */

#include <iostream>
#include <PersistentStateModels/PersistentStateException.h>

using namespace testing::persistence;

PersistentStateException::PersistentStateException(const char* what) {
	// TODO Auto-generated constructor stub
	this->_what = what;
	std::cout << what << "\n";
}

const char* PersistentStateException::what() const throw()
{
	return this->_what;
}

