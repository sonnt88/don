/*
 * PersistentFrameworkState.cpp
 *
 *  Created on: Dec 8, 2011
 *      Author: oto4hi
 */

#include <PersistentStateModels/FrameworkState/PersistentFrameworkState.h>
#include <gtest/gtest.h>
#include <stdlib.h>
#include <string.h>

using namespace ::testing;
using namespace ::testing::persistence;

PersistentFrameworkState::PersistentFrameworkState(IReader* Reader, IWriter* Writer)
{
	this->reader = Reader;
	this->writer = Writer;

	this->argc = 0;
	this->argv = NULL;

	this->currentTestCase = 0;
	this->currentTest = 0;

	this->currentIteration = 0;

	this->randomSeed = 0;
}

PersistentFrameworkState::PersistentFrameworkState(const PersistentFrameworkState& s)
{
	this->reader = s.reader;
	this->writer = s.writer;

	this->argc = 0;
	this->argv = NULL;

	this->SetArgs(s.argc, s.argv);

	this->currentTestCase = s.currentTestCase;
	this->currentTest = s.currentTest;

	this->currentIteration = s.currentIteration;
	this->DimTestCases(s.testResults.size());

	this->randomSeed = s.randomSeed;

	for ( unsigned int index = 0; index < this->testResults.size(); index++)
	{
		this->DimTests(index, s.testResults[index]->size());
	}

	for ( unsigned int testCaseIndex = 0; testCaseIndex < this->testResults.size(); testCaseIndex++)
	{
		for ( unsigned int testIndex = 0; testIndex < this->testResults[testCaseIndex]->size(); testIndex++)
		{
			this->testResults[testCaseIndex][testIndex] = s.testResults[testCaseIndex][testIndex];
		}
	}
}

PersistentFrameworkState::~PersistentFrameworkState()
{
	this->Clear();
}

bool PersistentFrameworkState::IsStructurallyEquivalent(const PersistentFrameworkState& s)
{
	if (this->argc != s.argc)
		return false;

	for (int index = 1; index < this->argc; index++)
	{
		if (strcmp(this->argv[index], s.argv[index]) != 0)
			return false;
	}

	if (this->testResults.size() != s.testResults.size())
		return false;

	for ( unsigned int testCaseIndex = 0; testCaseIndex < this->testResults.size(); testCaseIndex++)
	{
		if (this->testResults[testCaseIndex] == NULL || s.testResults[testCaseIndex] == NULL)
			return false;
		if (this->testResults[testCaseIndex]->size() != s.testResults[testCaseIndex]->size())
			return false;
	}

	return true;
}

unsigned int PersistentFrameworkState::getSerializedSize()
{
	unsigned int serializedSize = 0;

	//argc
	serializedSize += sizeof(this->argc);

	//argv
	for (int i = 0; i < this->argc; i++)
		serializedSize += strlen(argv[i]) + 1;

	//number of test cases
	serializedSize += sizeof(int);


	for (unsigned int testCaseIndex = 0; testCaseIndex < this->testResults.size(); testCaseIndex++)
	{
		//number of tests per test case
		serializedSize += sizeof(int);
		for (unsigned int testIndex = 0; testIndex < this->testResults[testCaseIndex]->size(); testIndex++)
		{
			//the test result itself
			serializedSize += sizeof(TESTEXECSTATE);
		}
	}

	serializedSize += sizeof(this->currentTestCase);
	serializedSize += sizeof(this->currentTest);
	serializedSize += sizeof(this->iterationCount);
	serializedSize += sizeof(this->currentIteration);
	serializedSize += sizeof(this->randomSeed);

	return serializedSize;
}

void PersistentFrameworkState::writeToBuffer(void* pBuffer, void* pSrc, unsigned int contentSize, unsigned int bufferLength, unsigned int& offset)
{
	if (offset + contentSize > bufferLength)
			throw new PersistentStateException("Error: attempt to write behind the buffer threshold.");

	memcpy((char*)pBuffer + offset, pSrc, contentSize);
	offset += contentSize;
}

void PersistentFrameworkState::readStringFromBuffer(char** pDest, void* pBuffer, unsigned int bufferLength, unsigned int& offset)
{
	unsigned int stringLength = strlen(((char*)pBuffer + offset)) + 1;

	if (offset + stringLength > bufferLength)
		throw new PersistentStateException("Error: attempt to read behind the buffer threshold. Are you possibly trying to read a non-zero terminated string?");

	*pDest = (char*)malloc(stringLength);
	memcpy(*pDest, ((char*)pBuffer+offset), stringLength);

	offset += stringLength;
}

void PersistentFrameworkState::readFromBuffer(void* pDest, void* pBuffer, unsigned int contentSize, unsigned int bufferLength, unsigned int& offset)
{
	if (offset + contentSize > bufferLength)
		throw new PersistentStateException("Error: attempt to read behind the buffer threshold.");

	memcpy(pDest, (char*)pBuffer + offset, contentSize);
	offset += contentSize;
}

void PersistentFrameworkState::Persist()
{
	unsigned int serializedSize = getSerializedSize();
	void* pBuffer = malloc(serializedSize);
	unsigned int offset = 0;

	// serialize argc and argv
	writeToBuffer(pBuffer, (void*)&argc, sizeof(argc), serializedSize, offset);
	for (int i = 0; i < this->argc; i++)
		writeToBuffer(pBuffer, (void*)argv[i], strlen(argv[i]) + 1, serializedSize, offset);

	// serialize the number of testcases
	unsigned int sizeValue = this->testResults.size();
	writeToBuffer(pBuffer, (void*)&sizeValue, sizeof(sizeValue), serializedSize, offset);

	// serialize the number of tests per testcase
	for (unsigned int testCaseIndex = 0; testCaseIndex < this->testResults.size(); testCaseIndex++)
	{
		sizeValue = this->testResults[testCaseIndex]->size();
		writeToBuffer(pBuffer, (void*)&sizeValue, sizeof(sizeValue), serializedSize, offset);
	}

	// serialize each test result
	for (unsigned int testCaseIndex = 0; testCaseIndex < this->testResults.size(); testCaseIndex++)
	{
		for (unsigned int testIndex = 0; testIndex < this->testResults[testCaseIndex]->size(); testIndex++)
		{
			TESTEXECSTATE execState = this->testResults[testCaseIndex]->at(testIndex);
			writeToBuffer(pBuffer, (void*)&execState, sizeof(TESTEXECSTATE), serializedSize, offset);
		}
	}

	// serialize the current test position
	writeToBuffer(pBuffer, (void*)&(this->currentTestCase), sizeof(this->currentTestCase), serializedSize, offset);
	writeToBuffer(pBuffer, (void*)&(this->currentTest), sizeof(this->currentTest), serializedSize, offset);
	writeToBuffer(pBuffer, (void*)&(this->iterationCount), sizeof(this->iterationCount), serializedSize, offset);
	writeToBuffer(pBuffer, (void*)&(this->currentIteration), sizeof(this->currentIteration), serializedSize, offset);
	writeToBuffer(pBuffer, (void*)&(this->randomSeed), sizeof(this->randomSeed), serializedSize, offset);

	this->writer->write(pBuffer, serializedSize);

	free(pBuffer);
}

void PersistentFrameworkState::Init()
{
	::testing::UnitTest* unit_test = ::testing::UnitTest::GetInstance();

	this->DimTestCases(unit_test->total_test_case_count());
	for (int testCaseIndex = 0; testCaseIndex < unit_test->total_test_case_count(); testCaseIndex++)
	{
		this->DimTests(testCaseIndex, unit_test->GetTestCase(testCaseIndex)->total_test_count());

		const ::testing::TestCase* testCase = unit_test->GetTestCase(testCaseIndex);
		for (int testIndex = 0; testIndex < testCase->total_test_count(); testIndex++)
			this->SetResult(testCaseIndex, testIndex, (testCase->GetTestInfo(testIndex)->should_run()) ? SHOULD_RUN : SHOULD_NOT_RUN);
	}

	this->SetIterationCount(GTEST_FLAG(repeat));

}

bool PersistentFrameworkState::Restore()
{
	unsigned int bufferSize = 0;
	void* pBuffer = this->reader->read(bufferSize);

	if (!pBuffer)
		return false;

	this->Clear();
	unsigned int offset = 0;

	// restore argc and argv
	readFromBuffer((void*)&(this->argc), pBuffer, sizeof(this->argc), bufferSize, offset);
	this->argv = (char**) malloc (this->argc * sizeof(char*));
	for (int i = 0; i < this->argc; i++)
		readStringFromBuffer(&(this->argv[i]), pBuffer, bufferSize, offset);

	// restore all testcase entries
	unsigned int numberOfTestCases;
	readFromBuffer((void*)&numberOfTestCases, pBuffer, sizeof(numberOfTestCases), bufferSize, offset);
	this->DimTestCases(numberOfTestCases);

	// restore all test entries
	for (unsigned int testCaseIndex = 0; testCaseIndex < numberOfTestCases; testCaseIndex++)
	{
		unsigned int numberOfTests;
		readFromBuffer((void*)&numberOfTests, pBuffer, sizeof(numberOfTests), bufferSize, offset);
		this->DimTests(testCaseIndex, numberOfTests);
	}

	// restore the content of all test entries
	for (unsigned int testCaseIndex = 0; testCaseIndex < this->testResults.size(); testCaseIndex++)
	{
		for (unsigned int testIndex = 0; testIndex < this->testResults[testCaseIndex]->size(); testIndex++)
		{
			TESTEXECSTATE execState;
			readFromBuffer((void*)&execState, pBuffer, sizeof(execState), bufferSize, offset);
			this->testResults[testCaseIndex]->at(testIndex) = execState;
		}
	}

	// restore the current test position
	readFromBuffer((void*)&(this->currentTestCase), pBuffer, sizeof(this->currentTestCase), bufferSize, offset);
	readFromBuffer((void*)&(this->currentTest), pBuffer, sizeof(this->currentTest), bufferSize, offset);
	readFromBuffer((void*)&(this->iterationCount), pBuffer, sizeof(this->iterationCount), bufferSize, offset);
	readFromBuffer((void*)&(this->currentIteration), pBuffer, sizeof(this->currentIteration), bufferSize, offset);
	readFromBuffer((void*)&(this->randomSeed), pBuffer, sizeof(this->randomSeed), bufferSize, offset);

	free(pBuffer);

	return true;
}

void PersistentFrameworkState::clearArgs()
{
	if (this->argv)
	{
		for (int i = 0; i < this->argc; i++)
			free(this->argv[i]);
		free(this->argv);
		this->argv = NULL;
	}
	this->argc = 0;
}

void PersistentFrameworkState::clearTestResults()
{
	for (unsigned int testCaseIndex = 0; testCaseIndex < this->testResults.size(); testCaseIndex++)
	{
		this->testResults[testCaseIndex]->clear();
		delete this->testResults[testCaseIndex];
	}

	this->testResults.clear();

	this->currentTestCase = 0;
	this->currentTest = 0;
	this->iterationCount = 0;
	this->currentIteration = 0;
}

void PersistentFrameworkState::Clear()
{
    clearArgs();
    clearTestResults();
}

void PersistentFrameworkState::Remove()
{
	this->writer->remove();
}

void PersistentFrameworkState::SetArgs(int argc, char** argv)
{
	clearArgs();

	this->argc = argc;
	this->argv = (char**) malloc (sizeof(char*) * this->argc);

	for (int i = 0; i < this->argc; i++)
	{
		int argSize = strlen(argv[i]) + 1;
		this->argv[i] = (char*) malloc (argSize);
		memcpy(this->argv[i], argv[i], argSize);
	}
}

void PersistentFrameworkState::DimTestCases(unsigned int NumOfTestCases)
{
	if (!testResults.empty())
		throw new PersistentStateException("Error: trying to dimension the test case storage which has already been done.");

	for ( unsigned int index = 0; index < NumOfTestCases; index++)
	{
		testResults.push_back( new std::vector<TESTEXECSTATE> );
	}
}

void PersistentFrameworkState::DimTests(unsigned int TestCaseIndex, unsigned int NumOfTests)
{
	if (TestCaseIndex >= testResults.size())
		throw new PersistentStateException("Error: test case index does not exist.");

	if (!testResults[TestCaseIndex]->empty())
		throw new PersistentStateException("Error: trying to dimension the test result storage which has already been done.");

	for ( unsigned int index = 0; index < NumOfTests; index++ )
		testResults[TestCaseIndex]->push_back( SHOULD_RUN );
}

unsigned int PersistentFrameworkState::GetTestCaseCount()
{
	return this->testResults.size();
}

unsigned int PersistentFrameworkState::GetTestCount(unsigned int TestCaseIndex)
{
	if (this->testResults.size() <= TestCaseIndex)
		throw new PersistentStateException("Error: TestCaseIndex does not exist.");

	return this->testResults[TestCaseIndex]->size();
}

unsigned int PersistentFrameworkState::countTestResultsWithValue(unsigned int TestCaseIndex, TESTEXECSTATE Value)
{
	unsigned int count = 0;
	for (unsigned int index = 0; index < testResults[TestCaseIndex]->size(); index++)
	{
		if ( testResults[TestCaseIndex]->at(index) == Value )
			count++;
	}

	return count;
}

unsigned int PersistentFrameworkState::GetTestCount()
{
	unsigned int count = 0;
	for (unsigned int index = 0; index < this->testResults.size(); index++)
	{
		count += GetTestCount(index);
	}

	return count;
}

unsigned int PersistentFrameworkState::GetPassedTestCount()
{
	unsigned int count = 0;
	for (unsigned int index = 0; index < this->testResults.size(); index++)
	{
		count += GetPassedTestCount(index);
	}

	return count;
}

unsigned int PersistentFrameworkState::GetFailedTestCount()
{
	unsigned int count = 0;
	for (unsigned int index = 0; index < this->testResults.size(); index++)
	{
		count += GetFailedTestCount(index);
	}

	return count;
}

unsigned int PersistentFrameworkState::GetPassedTestCount(unsigned TestCaseIndex)
{
	if (this->testResults.size() <= TestCaseIndex)
			throw new PersistentStateException("Error: TestCaseIndex does not exist.");

	return countTestResultsWithValue(TestCaseIndex, PASSED);
}

unsigned int PersistentFrameworkState::GetFailedTestCount(unsigned TestCaseIndex)
{
	if (this->testResults.size() <= TestCaseIndex)
		throw new PersistentStateException("Error: TestCaseIndex does not exist.");

	return countTestResultsWithValue(TestCaseIndex, FAILED);
}

void PersistentFrameworkState::SetResult(TESTEXECSTATE Result)
{
	testResults[this->currentTestCase]->at(this->currentTest) = Result;
}

void PersistentFrameworkState::SetResult(unsigned int TestCaseIndex, unsigned int TestIndex, TESTEXECSTATE Result)
{
	if (this->testResults.size() <= TestCaseIndex)
			throw new PersistentStateException("Error: TestCaseIndex does not exist.");

	if (this->testResults[TestCaseIndex]->size() <= TestIndex)
			throw new PersistentStateException("Error: TestIndex does not exist.");

	testResults[TestCaseIndex]->at(TestIndex) = Result;
}

TESTEXECSTATE PersistentFrameworkState::GetResult(unsigned int TestCaseIndex, unsigned int TestIndex)
{
	if (this->testResults.size() <= TestCaseIndex)
		throw new PersistentStateException("Error: TestCaseIndex does not exist.");

	if (this->testResults[TestCaseIndex]->size() <= TestIndex)
		throw new PersistentStateException("Error: TestIndex does not exist.");

	return testResults[TestCaseIndex]->at(TestIndex);
}

void PersistentFrameworkState::NextIteration()
{
	if ((int)(this->currentIteration) >= this->iterationCount)
		throw new PersistentStateException("Error: Behind last iteration.");

	this->currentIteration++;

	for (unsigned int testCaseIndex = 0; testCaseIndex < testResults.size(); testCaseIndex++)
	{
		for (unsigned int testIndex = 0; testIndex < testResults[testCaseIndex]->size(); testIndex++)
		{
			if (testResults[testCaseIndex]->at(testIndex) != SHOULD_NOT_RUN)
				testResults[testCaseIndex]->at(testIndex) = SHOULD_RUN;
		}
	}
}
