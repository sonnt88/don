/*
 * SequencialFileReader.cpp
 *
 *  Created on: Dec 8, 2011
 *      Author: oto4hi
 */

#include <PersistentStorage/SequencialFileReader.h>
#include <PersistentStorage/IOException.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>

SequencialFileReader::SequencialFileReader(const char* Path)
{
	if (!Path)
		throw new IOException("Path cannot be NULL.");

	unsigned int len = strlen(Path) + 1;

	this->path = (char*)malloc(len);
	memcpy(this->path, Path, len);
}

SequencialFileReader::~SequencialFileReader()
{
	if (this->path)
		free(this->path);
}

void* SequencialFileReader::read(unsigned int& Length)
{
	std::ifstream file(this->path, std::ios::in|std::ios::binary|std::ios::ate);
	void* pBuffer = NULL;

	if (file.is_open())
	{
		Length = (int) file.tellg();
		pBuffer = malloc(Length);
		file.seekg(0, std::ios::beg);
		file.read((char*)pBuffer, Length);
		file.close();
	}

	return pBuffer;
}
