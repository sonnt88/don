/*
 * IOFactory.cpp
 *
 *  Created on: Jan 19, 2012
 *      Author: oto4hi
 */

#include <PersistentStorage/IOFactory.h>
#include <gtest/gtest.h>

#if defined(OSAL_OS) && OSAL_OS==OSAL_TENGINE
#include <PersistentStorage/SequencialBlockReader.h>
#include <PersistentStorage/SequencialBlockWriter.h>

IReader* IOFactory::CreateTestStateReader()
{
	return new SequencialBlockReader();
}

IWriter* IOFactory::CreateTestStateWriter()
{
	return new SequencialBlockWriter();
}

IReader* IOFactory::CreateFrameworkStateReader()
{
	return new SequencialBlockReader();
}

IWriter* IOFactory::CreateFrameworkStateWriter()
{
	return new SequencialBlockWriter();
}

#else

#include <PersistentStorage/SequencialFileReader.h>
#include <PersistentStorage/SequencialFileWriter.h>

IReader* IOFactory::CreateTestStateReader()
{
	char* filename = GetCurrentTestStateFilename();
	SequencialFileReader* reader = new SequencialFileReader(filename);
	free(filename);
	return reader;
}

IWriter* IOFactory::CreateTestStateWriter()
{
	char* filename = GetCurrentTestStateFilename();
	SequencialFileWriter* writer = new SequencialFileWriter(filename);
	free(filename);
	return writer;
}

IReader* IOFactory::CreateFrameworkStateReader()
{
	return new SequencialFileReader("FrameworkState.bin");
}

IWriter* IOFactory::CreateFrameworkStateWriter()
{
	return new SequencialFileWriter("FrameworkState.bin");
}

#endif

char* IOFactory::GetCurrentTestStateFilename()
{
	::testing::UnitTest* unitTest = ::testing::UnitTest::GetInstance();
	const char* testCaseName = unitTest->current_test_case()->name();
	const char* testName = unitTest->current_test_info()->name();
	unsigned int testCaseNameLength = strlen(testCaseName);
	unsigned int testNameLength = strlen(testName);

	char* filename = (char*) malloc (testCaseNameLength + testNameLength + 12);

	if (!filename)
		throw new std::bad_alloc();

	sprintf(filename, "%s_%s_state.bin", testCaseName, testName);

	return filename;
}

