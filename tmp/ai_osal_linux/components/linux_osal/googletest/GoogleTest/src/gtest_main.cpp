// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <iostream>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <PersistentStorage/IOFactory.h>
#include <PersistentStateModels/FrameworkState/TraceablePersistentFrameworkStateManager.h>
#include <ResultOutput/PersiGTestSimpleTracePrinter.h>
#include <ResultOutput/PersiGTestXMLTracePrinter.h>

::testing::persistence::PersistentFrameworkStateManager* persistentFrameworkStateManager;

#ifdef OSAL_OS 

#if OSAL_OS==OSAL_TENGINE
extern void anchorfunc(void);
#endif

extern "C"{
  void vStartApp(int argc, char **argv);
}

#endif

#if defined(OSAL_OS) && OSAL_OS==OSAL_TENGINE
void vStartApp(int argc, char **argv)
{
  anchorfunc();
#else
GTEST_API_ int main(int argc, char **argv)
{
#endif
  IReader* reader = IOFactory::CreateFrameworkStateReader();
  IWriter* writer = IOFactory::CreateFrameworkStateWriter();
  ::testing::persistence::IPersiGTestPrinter* printer = new ::testing::persistence::PersiGTestXMLTracePrinter();

  ::testing::TestEventListeners& listeners = ::testing::UnitTest::GetInstance()->listeners();
  ::testing::TestEventListener* default_result_printer = listeners.Release(listeners.default_result_printer());

  persistentFrameworkStateManager =
		  new ::testing::persistence::TraceablePersistentFrameworkStateManager(argc, argv, reader, writer, printer);

  listeners.Append(persistentFrameworkStateManager);

  testing::InitGoogleMock(&argc, argv);

  // Ensures that the tests pass no matter what value of
  // --gmock_catch_leaked_mocks and --gmock_verbose the user specifies.
  testing::GMOCK_FLAG(catch_leaked_mocks) = true;
  testing::GMOCK_FLAG(verbose) = testing::internal::kWarningVerbosity;

  int retVal = RUN_ALL_TESTS();

  delete reader;
  delete writer;
  delete printer;
  delete default_result_printer;

#ifdef OSAL_OS
    _exit(0);
#else
    return retVal;
#endif
}

