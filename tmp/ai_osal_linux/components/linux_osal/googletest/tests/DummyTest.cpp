#include <persigtest.h>

TEST(DummyTests, DISABLED_Test0)
{
    HELLO();
	EXPECT_EQ(1, 1);
}

TEST(DummyTests, Test1)
{
    HELLO();
	EXPECT_EQ(1, 1);
}

TEST(DummyTests, Test2)
{
    HELLO();
	EXPECT_EQ(1, 1);

	int value = 4;

	void* pBuffer;
	unsigned int length = sizeof(int);

	if (RESTORE(pBuffer, length))
	{
		value = *((int*)pBuffer);
	}

    TracePrintf(TR_LEVEL_FATAL, "The persistent value is now %d", value);
	value++;

	if (value < 8)
		PERSIST(&value, sizeof(int));

	//_exit(0);
}

TEST(DummyTests, Test3)
{
    HELLO();
	EXPECT_EQ(1, 1);
}

TEST(DummyTests, Test4)
{
    HELLO();
	EXPECT_EQ(0, 1);
}

TEST(DummyTests, Test5)
{
    HELLO();
	EXPECT_EQ(1, 1);
}
