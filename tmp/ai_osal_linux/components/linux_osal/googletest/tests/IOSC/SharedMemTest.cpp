/*
 * ShardMemTest.cpp
 *
 *  Created on: Feb 16, 2012
 *      Author: oto4hi
 */

#include "IOSCTests.h"
#include <math.h>

#define IOSC_TEST_MEM_ID	IOSC_MEM_ID(2011)
#define IOSC_TEST_MEM_ID2	IOSC_MEM_ID(2012)

#ifdef OSAL_OS
#if OSAL_OS == OSAL_LINUX
#define iosc_shared_free(ptr) iosc_shared_mem_free(ptr)
#define iosc_shared_malloc(size) iosc_shared_malloc_with_id((size),IOSC_TEST_MEM_ID, NULL)
#endif
#endif //OSAL_OS

class ShardMemAllocIOSC : public ::testing::Test {
	protected:
	bool AllocAndFree(tU32 u32Size);
	void SharedMemAllocSingleTest(tU32 u32NumOfIterations, tU32 MemoryBaseSize, const char* sMeasurementUnit);
};

bool ShardMemAllocIOSC::AllocAndFree(tU32 u32Size)
{
	bool result;
	tPU32 pU32TestPtr = (tPU32)(iosc_shared_malloc(u32Size));

	EXPECT_FALSE(pU32TestPtr == NULL);
	result = (pU32TestPtr != NULL);

	if (pU32TestPtr)
	{
		result = (iosc_shared_free(pU32TestPtr) == IOSC_OK);
		EXPECT_TRUE(result);
	}

	return result;
}

void ShardMemAllocIOSC::SharedMemAllocSingleTest(tU32 u32NumOfIterations, tU32 MemoryBaseSize, const char* sMeasurementUnit)
{
  tU32 i;

  for (i = 0; i < u32NumOfIterations; i++)
  {
    if (!AllocAndFree ( MemoryBaseSize * (tU32)pow((double)2, (int)i) ) )
    	TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) allocation %d %s of shared memory failed.", (tU32)pow((double)2, (int)i), sMeasurementUnit);
    else
    	TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) successfully allocated and freed %d %s of shared memory.", (tU32)pow((double)2, (int)i), sMeasurementUnit);
  }
}

TEST_F(ShardMemAllocIOSC, AllocWithGrowingSizes)
{
  // Tests with 1 to 32kB
	SharedMemAllocSingleTest(6, 1024, "kB");

  // Tests with 1 to 32MB
	SharedMemAllocSingleTest(6, 1024 * 1024, "MB");
}

class ShardMemFreeIOSC : public ::testing::Test {
	protected:
	bool AllocAndFree(tPU32* ppU32TestPtr);
	bool DoubleFree(tPU32* ppU32TestPtr);
	bool PerformFreeOnNULLPointer();
	void SharedMemAllocSingleTest(tU32 u32NumOfIterations, tU32 MemoryBaseSize, const char* sMeasurementUnit);
};


bool ShardMemFreeIOSC::AllocAndFree(tPU32* ppU32TestPtr)
{
	bool result = false;
	tPU32 pU32TestPtr = *ppU32TestPtr;
	pU32TestPtr = (tPU32)iosc_shared_malloc(1024*1024*8);

	EXPECT_FALSE(pU32TestPtr == NULL);

	if (pU32TestPtr)
	{
		result = ( IOSC_OK == iosc_shared_free(pU32TestPtr) );
		EXPECT_TRUE( result );
	}

	return result;
}

bool ShardMemFreeIOSC::DoubleFree(tPU32* ppU32TestPtr)
{
	bool result = (IOSC_BADARG == iosc_shared_free(*ppU32TestPtr));
	EXPECT_TRUE(result);
	return result;
}

bool ShardMemFreeIOSC::PerformFreeOnNULLPointer()
{
  bool result = (IOSC_OK != iosc_shared_free(NULL));
  EXPECT_TRUE(result);
  return result;
}

TEST_F(ShardMemFreeIOSC, Free)
{
	tPU32 pU32TestPtr = NULL;

	// try to free a NULL pointer
	if (!PerformFreeOnNULLPointer())
		TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) try free on NULL pointer: FAILED");

	// allocate and free 8MB of shared memory
	if (!AllocAndFree(&pU32TestPtr))
		TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc and free 8MB: FAILED");

	// try a another free on an already freed pointer
	if (!DoubleFree(&pU32TestPtr))
		TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) double free 8MB: FAILED");

	// again alloc and free should work
	if (!AllocAndFree(&pU32TestPtr))
		TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) realloc and free 8MB: FAILED");
}

class AllocWithIdSharedMemIOSC : public ::testing::Test {
	protected:
	tPU32 MallocWithId(tBool bShouldAlreadyExist, tBool* pbExistenceExpectationIsTrue, tS32 s32MemID);
	void MallocWithIdSingleStep(tPU32* vSharedMemPointers, tU32 u32CurrentElement, tS32 s32MemID);
	void FreeSharedMemory(tPU32* vSharedMemPointers, tU32 NumOfElements);
};

tPU32 AllocWithIdSharedMemIOSC::MallocWithId(tBool bShouldAlreadyExist, tBool* pbExistenceExpectationIsTrue, tS32 s32MemID)
{
  tPU32 pU32TestPtr = NULL;
  tS32 s32MemAlreadyExists;

  tU32 SharedMemSize = 1024*1024*8;

  pU32TestPtr = (tPU32)(iosc_shared_malloc_with_id((SharedMemSize), s32MemID, &s32MemAlreadyExists));

  if (( bShouldAlreadyExist && (s32MemAlreadyExists == 1) ) || (!bShouldAlreadyExist && (s32MemAlreadyExists == 0) ))
    *pbExistenceExpectationIsTrue = TRUE;
  else
    *pbExistenceExpectationIsTrue = FALSE;

  return pU32TestPtr;
}

void AllocWithIdSharedMemIOSC::MallocWithIdSingleStep(tPU32* vSharedMemPointers, tU32 u32CurrentElement, tS32 s32MemID)
{
	tBool bShouldAlreadyExist = (u32CurrentElement != 0);
	tBool bExistanceExpectationIsTrue;

	tPU32 pU32TestPtr = MallocWithId(bShouldAlreadyExist, &bExistanceExpectationIsTrue, s32MemID);
	vSharedMemPointers[u32CurrentElement] = pU32TestPtr;

	EXPECT_TRUE( pU32TestPtr != NULL );

	if (!pU32TestPtr)
		TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc with id 8MB: NULL pointer returned: FAILED");
	else {
		EXPECT_TRUE(bExistanceExpectationIsTrue);
		if (!bExistanceExpectationIsTrue)
		{
			if (bShouldAlreadyExist)
				TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc with id 8MB: shared memory was not expected to exist but already existed: FAILED", u32CurrentElement);
			else
				TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc with id 8MB: shared memory was expected to exist but did not exist: FAILED", u32CurrentElement);
		}
#ifdef OSAL_OS
#if OSAL_OS == OSAL_TENGINE
		else
		{
			if ( (u32CurrentElement > 0) && (vSharedMemPointers[0] != pU32TestPtr) )
			{
				ADD_FAILURE();
				TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) alloc with id 8MB (realloc=%d): shared memory pointers are not equal: FAILED", (tU32) bShouldAlreadyExist);
			}
		}
#endif
#else
#error unknown OSAL configuration
#endif
	}
}

void AllocWithIdSharedMemIOSC::FreeSharedMemory(tPU32* vSharedMemPointers, tU32 NumOfElements)
{
	bool result = true;
	tU32 currentElement = NumOfElements;
	tPU32 pU32TestPtr;

	while(currentElement > 0)
	{
		currentElement--;
		pU32TestPtr = vSharedMemPointers[currentElement];
		if (pU32TestPtr)
			if (iosc_shared_free(pU32TestPtr) != IOSC_OK)
			{
				ADD_FAILURE();
				result = false;
			}

	}

	if (!result)
		TracePrintf(TR_LEVEL_FATAL, "IOSC (shared mem) free shared memory: FAILED");
}

TEST_F(AllocWithIdSharedMemIOSC, AllocWithIdMultipleTimes)
{
  tPU32 vSharedMemPointers[5];
  tPU32 vSharedMemPointers2[1];
  tU32 i;

  // get the shared memory segment 5 times. The pointer that is returned should alway be the
  // same and IOSC is expected to be aware of an already existing shared memory segment.
  for (i = 0; i < 5; i++)
    MallocWithIdSingleStep(vSharedMemPointers, i, IOSC_TEST_MEM_ID);

  // free all shared memory pointers
  FreeSharedMemory(vSharedMemPointers, 5);

  // try to reallocate the shared memory segment -> the segment is expected as non-existing now
  MallocWithIdSingleStep((tPU32*)&vSharedMemPointers, 0, IOSC_TEST_MEM_ID);

  // try to allocated another shared memory segment just to see if two different shared memory segements can exist next to another
  MallocWithIdSingleStep((tPU32*)&vSharedMemPointers2, 0, IOSC_TEST_MEM_ID2);

  // free the first shared memory segment again
  FreeSharedMemory(vSharedMemPointers, 1);

  // free the second shared memory segment again
  FreeSharedMemory(vSharedMemPointers2, 1);
}


