#include "IOSCTests.h"

#define TEST_RINGBUFFER IOSC_RING_ID(2011)

TEST(RingbufferIOSC, Create)
{
	IoscRingbuffer ringbuffer = IOSC_RINGBUFFER_INVALID;
	IoscRingbuffer ringbuffer1 = IOSC_RINGBUFFER_INVALID;
	
	ringbuffer = iosc_create_ringbuffer(TEST_RINGBUFFER, 256);
	EXPECT_NE(IOSC_RINGBUFFER_INVALID, ringbuffer);

	if (ringbuffer == IOSC_RINGBUFFER_INVALID)
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: create first ringbuffer: FAILED");
		FAIL();
	}

	ringbuffer1 = iosc_create_ringbuffer(TEST_RINGBUFFER, 256);
	EXPECT_NE(IOSC_RINGBUFFER_INVALID, ringbuffer1);

	if (ringbuffer1 == IOSC_RINGBUFFER_INVALID)
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: create second ringbuffer: FAILED");
		iosc_destroy_ringbuffer(ringbuffer);
		FAIL();
	}

	EXPECT_EQ( ringbuffer, ringbuffer1);
	if (ringbuffer != ringbuffer1)
	{
    TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: check if ringbuffers are equal: FAILED");
		iosc_destroy_ringbuffer(ringbuffer1);
    iosc_destroy_ringbuffer(ringbuffer);
    FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_ringbuffer(ringbuffer) );
	if ( HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: destroy first ringbuffer: FAILED");
		iosc_destroy_ringbuffer(ringbuffer1);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_destroy_ringbuffer(ringbuffer1));
	if ( HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) create ring buffer: destroy second ringbuffer: FAILED");
		FAIL();
	}
}

TEST(RingbufferIOSC, DoubleDestroy)
{
	IoscRingbuffer ringbuffer = IOSC_RINGBUFFER_INVALID;	
	ringbuffer = iosc_create_ringbuffer(TEST_RINGBUFFER, 256);

	EXPECT_NE(IOSC_RINGBUFFER_INVALID, ringbuffer);
	if ( HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) try double free: create ringbuffer: FAILED");
		FAIL();
	}

  EXPECT_EQ( IOSC_OK, iosc_destroy_ringbuffer(ringbuffer) );
	if (  HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) try double free: destroy ringbuffer: FAILED");
		FAIL();
	}

  EXPECT_EQ( IOSC_BADARG, iosc_destroy_ringbuffer(ringbuffer) );
	if ( HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) try double free: second destroy on ringbuffer: FAILED");
		FAIL();
	}
}

TEST(RingbufferIOSC, SendAndReceive)
{
	IoscRingbuffer ringbuffer = IOSC_RINGBUFFER_INVALID;

	char test_snd_buffer[256] = {"TEST BUFF 12345678901234678910234569854721"};
	char test_rcv_buffer[256] = {""};
	unsigned int string_size = strlen(reinterpret_cast<char*>(test_snd_buffer));

	ringbuffer = iosc_create_ringbuffer(TEST_RINGBUFFER, 256);
	EXPECT_NE(IOSC_RINGBUFFER_INVALID, ringbuffer);

	if ( HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: create ringbuffer FAILED");
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_sendto_ringbuffer( ringbuffer, string_size, (unsigned char*)test_snd_buffer, IOSC_TIMEOUT_100 ) );
	if ( HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: send to ringbuffer FAILED");
		iosc_destroy_ringbuffer(ringbuffer);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_recfrom_ringbuffer(ringbuffer, string_size, (unsigned char*)test_rcv_buffer, IOSC_TIMEOUT_100 ) );
	if ( HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: receive from ringbuffer: FAILED");
		iosc_destroy_ringbuffer(ringbuffer);
		FAIL();
	}

    EXPECT_EQ(0, strcmp(test_snd_buffer, test_rcv_buffer ) );
	if ( HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: check for consistency of sent and received buffer: FAILED");
		iosc_destroy_ringbuffer(ringbuffer);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_ringbuffer(ringbuffer) );
	if ( HasNonfatalFailure() )
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (ringbuffer) send and receive: destroy ringbuffer: FAILED");
		FAIL();
	}
}

