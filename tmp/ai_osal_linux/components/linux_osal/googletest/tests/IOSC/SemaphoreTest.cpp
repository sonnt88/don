/*
 * SemaphoreTest.cpp
 *
 *  Created on: Feb 17, 2012
 *      Author: oto4hi
 */

#include "IOSCTests.h"

#define TEST_SEMAPHORE     IOSC_SEM_ID(2011)
#define IOSC_TEST_MAX_SEM  7000

class SemaphoreIOSC : public ::testing::Test {
protected:
	void DestroySemaphores(IoscSemaphore* Semaphores, tU32 u32Count);
	IoscSemaphore* CreateSemaphores(tU32 u32Count, tBool bDifferentIDs);
	void ObtainSemaphores(IoscSemaphore* Semaphores, tU32 u32Count, tBool ShouldBeObtainable);
};


void SemaphoreIOSC::DestroySemaphores(IoscSemaphore* Semaphores, tU32 u32Count)
{
	for (int i = 0; i < u32Count; i++)
	{
		if (Semaphores[i] != IOSC_SEMAPHORE_INVALID)
		{
			if (iosc_destroy_semaphore(Semaphores[i]) != IOSC_OK)
				ADD_FAILURE();
		}
	}
}

IoscSemaphore* SemaphoreIOSC::CreateSemaphores(tU32 u32Count, tBool bDifferentIDs)
{
	tU32 i;
	tS32 s32SemID;
	tS32 s32SemInitValue;
	const char* sInsertString;

	s32SemInitValue = (bDifferentIDs) ? 1 : u32Count;
	sInsertString = (bDifferentIDs) ? "different" : "equal";

	IoscSemaphore *Semaphores = (IoscSemaphore*)malloc (u32Count * sizeof (IoscSemaphore));
	EXPECT_TRUE(Semaphores != NULL);
	if (!Semaphores)
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) create semaphores with %s IDs: memory allocation for semaphores: %s", sInsertString, "FAILED");
		return NULL;
	}

	for (i = 0; i < u32Count; i++)
	{
		Semaphores[i] = IOSC_SEMAPHORE_INVALID;
	}

	for (i = 0; i < u32Count; i++)
	{
		s32SemID = (bDifferentIDs) ? TEST_SEMAPHORE + i : TEST_SEMAPHORE;
		Semaphores[i] = iosc_create_semaphore(s32SemID, s32SemInitValue);
		if (Semaphores[i] == IOSC_SEMAPHORE_INVALID)
		{
			TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) create semaphores with %s IDs: create semaphore no. %d:", i, "FAILED");
			DestroySemaphores(Semaphores, u32Count);
			free(Semaphores);
			ADD_FAILURE();
			return NULL;
		}
	}

	return Semaphores;
}

void SemaphoreIOSC::ObtainSemaphores(IoscSemaphore* Semaphores, tU32 u32Count, tBool ShouldBeObtainable)
{
	tU32 i;
	const char* sInsertString;
	tBool bSuccess = TRUE;
	tS32 s32ExpectedReturnValue;

	s32ExpectedReturnValue = (ShouldBeObtainable) ? IOSC_OK : IOSC_BADARG;
	sInsertString = (ShouldBeObtainable) ? "valid" : "invalid";

	for (i = 0; i < u32Count; i++)
	{
		if (iosc_obtain_semaphore(Semaphores[i], 1, IOSC_TIMEOUT_100) != s32ExpectedReturnValue)
			bSuccess = FALSE;
	}
	if (!bSuccess)
	{
		ADD_FAILURE();
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain %s semaphores: %s", sInsertString, "FAILED");
	}
}

TEST_F(SemaphoreIOSC, CreateWithDiffIDsAndObtainAfterDestroy)
{
	IoscSemaphore* Semaphores = CreateSemaphores(IOSC_TEST_MAX_SEM, FALSE);
	if (Semaphores)
	{
		DestroySemaphores(Semaphores, IOSC_TEST_MAX_SEM);
		ObtainSemaphores(Semaphores, IOSC_TEST_MAX_SEM, FALSE);
		free(Semaphores);
	}
}

TEST_F(SemaphoreIOSC, CreateWithEqIDsAndObtainAfterDestroy)
{
	IoscSemaphore* Semaphores = CreateSemaphores(IOSC_TEST_MAX_SEM, TRUE);
	if (Semaphores)
	{
		DestroySemaphores(Semaphores, IOSC_TEST_MAX_SEM );
		ObtainSemaphores(Semaphores, IOSC_TEST_MAX_SEM, FALSE);
		free(Semaphores);
	}
}

TEST_F(SemaphoreIOSC, CreateWithDiffIDsAndObtainBeforeDestroy)
{
	IoscSemaphore* Semaphores = CreateSemaphores(IOSC_TEST_MAX_SEM, FALSE);
	if (Semaphores)
	{
		ObtainSemaphores(Semaphores, IOSC_TEST_MAX_SEM, TRUE);
		DestroySemaphores(Semaphores, IOSC_TEST_MAX_SEM);
		free(Semaphores);
	}
}

TEST_F(SemaphoreIOSC, CreateWithEqIDsAndObtainBeforeDestroy)
{
	IoscSemaphore* Semaphores = CreateSemaphores(IOSC_TEST_MAX_SEM, TRUE);
	if (Semaphores)
	{
		ObtainSemaphores(Semaphores, IOSC_TEST_MAX_SEM, TRUE);
		DestroySemaphores(Semaphores, IOSC_TEST_MAX_SEM);
		free(Semaphores);
	}
}

TEST_F(SemaphoreIOSC, TryObtainWithValueNull)
{
	IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE , 0);
	EXPECT_NE( IOSC_SEMAPHORE_INVALID, semaphore);
	if (semaphore == IOSC_SEMAPHORE_INVALID)
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with value null: create semaphore: %s", "FAILED");
		FAIL();
	}

	EXPECT_EQ(IOSC_TIMEOUT, iosc_obtain_semaphore(semaphore, 10, IOSC_TIMEOUT_100));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with value null: obtain semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_semaphore(semaphore) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with value null: destroy semaphore: %s", "FAILED");
		FAIL();
	}
}

TEST_F(SemaphoreIOSC, TryObtainWithNegativeValue)
{
	IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE , 0);
	EXPECT_NE( IOSC_SEMAPHORE_INVALID, semaphore);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with negative argument: create semaphore: %s", "FAILED");
		FAIL();
	}

	EXPECT_EQ( IOSC_BADARG, iosc_obtain_semaphore(semaphore, -10, IOSC_TIMEOUT_100) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with negative argument: obtain semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_semaphore(semaphore) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain semaphore with negative argument: destroy semaphore: %s", "FAILED");
		FAIL();
	}
}

TEST_F(SemaphoreIOSC, TryDoubleDestroy)
{
	IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE , 0);
	EXPECT_NE( IOSC_SEMAPHORE_INVALID, semaphore );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try double destroy semaphore: create semaphore: %s", "FAILED");
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_semaphore(semaphore));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try double destroy semaphore: destroy semaphore: %s", "FAILED");
		FAIL();
	}

	EXPECT_EQ( IOSC_BADARG, iosc_destroy_semaphore(semaphore) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try double destroy semaphore: second destroy did not return IOSC_BADARG: %s", "FAILED");
		FAIL();
	}
}

TEST_F(SemaphoreIOSC, TryReleaseAfterDestroy)
{
	IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE , 0);
	EXPECT_NE( IOSC_SEMAPHORE_INVALID, semaphore );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try release on destroyed semaphore: create semaphore: %s", "FAILED");
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_semaphore(semaphore) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try release on destroyed semaphore: destroy semaphore: %s", "FAILED");
		FAIL();
	}

	EXPECT_EQ( IOSC_BADARG, iosc_release_semaphore(semaphore, 10) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) try release on destroyed semaphore: release semaphore: %s", "FAILED");
		FAIL();
	}
}

/*****************************************************************************
* FUNCTION		:  vTestSemObtain()
* PARAMETER		:  trfpSemIf -- IOSC Fn interface pointer
* RETURNVALUE	:  none
* DESCRIPTION	:  Test case 26 w.r.to Semaphore obtain
* HISTORY		:  Created by Sriranjan U (RBEI CM-AI/PJ-CF32) on 05 Mar, 2011
******************************************************************************/
TEST_F(SemaphoreIOSC, ObtainAndRelease)
{
	IoscSemaphore semaphore = iosc_create_semaphore(TEST_SEMAPHORE, 10);
	EXPECT_NE( IOSC_SEMAPHORE_INVALID, semaphore );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: create semaphore: %s", "FAILED");
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_obtain_semaphore(semaphore, 10, IOSC_TIMEOUT_FOREVER) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: obtain semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_release_semaphore(semaphore, 10) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: release semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_release_semaphore(semaphore, 10) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: second release semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_obtain_semaphore(semaphore, 10, IOSC_TIMEOUT_FOREVER) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: second obtain semaphore: %s", "FAILED");
		iosc_destroy_semaphore(semaphore);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_semaphore(semaphore) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (semaphore) obtain and release semaphore: destroy semaphore: %s", "FAILED");
		FAIL();
	}
}

