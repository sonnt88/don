/*
 * EventTest.cpp
 *
 *  Created on: Feb 16, 2012
 *      Author: oto4hi
 */

#include "IOSCTests.h"

#define TEST_EVENT_ID 		IOSC_EVENT_ID (2011)

#define IOSC_TEST_MAX_EVENT       7000
#define IOSC_TEST_EVENT_MASK   		0xAAAAAAAA
#define IOSC_TEST_EVENT_NULL_MASK	0x00000000
#define IOSC_TEST_EVENT_MASK_OR   0x00000002


TEST(EventIOSC, CreateAndDestroy)
{
	IoscEvent event1 = IOSC_EVENT_INVALID;
	IoscEvent event2 = IOSC_EVENT_INVALID;

	event1 = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, event1);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: create event1: FAILED");
		FAIL();
	}

	event2 = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, event2);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: create event2: FAILED");
		iosc_destroy_event(event1);
		FAIL();
	}

	EXPECT_EQ(event1, event2);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: check consistency of the two created event handles: FAILED");
		iosc_destroy_event(event1);
		iosc_destroy_event(event2);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_destroy_event(event1));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: destroy event1: FAILED");
		iosc_destroy_event(event2);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_destroy_event(event2));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) create and destroy: destroy event2: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, DoubleDestroy)
{
	IoscEvent hEvent = IOSC_EVENT_INVALID;

	hEvent = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, hEvent);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) double destroy event: create ewvent: FAILED");
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_destroy_event(hEvent));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) double destroy event: destroy event: FAILED");
		FAIL();
	}

	EXPECT_EQ(IOSC_BADARG, iosc_destroy_event(hEvent));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) double destroy event: second destroy on event handle: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, SetEventAndWaitForEvent_AND)
{
	IoscEvent hevent = IOSC_EVENT_INVALID;
	unsigned long mask_got = 0;

	hevent = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, hevent);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (complete bit matching): create event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event: set event (complete bit matching): FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got,IOSC_EVENT_AND, IOSC_TIMEOUT_100));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (complete bit matching): wait for event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_TEST_EVENT_MASK, mask_got);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (complete bit matching): check event bitmask: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_event(hevent));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (complete bit matching): destroy event: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, SetEventAndWaitForEvent_OR)
{

	IoscEvent hevent = IOSC_EVENT_INVALID;
	unsigned long mask_got = 0;

	hevent = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, hevent);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): create event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK_OR) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): set event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got,IOSC_EVENT_OR, IOSC_TIMEOUT_100) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): wait for event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_TEST_EVENT_MASK_OR, mask_got );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): check event bitmask: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_TIMEOUT, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK << 1, &mask_got,IOSC_EVENT_OR, IOSC_TIMEOUT_100) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): wait for event with wrong bitmask: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_event(hevent) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/wait for event (single bit matching): destroy event: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, SetEventAndWaitForEvent_OR2)
{
	IoscEvent hevent = IOSC_EVENT_INVALID;
	unsigned long mask_got = 0;

	hevent = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, hevent);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for bit shifted event mask (2 bits): create event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for bit shifted event mask (2 bits): set event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK << 2, &mask_got, IOSC_EVENT_OR, IOSC_TIMEOUT_100));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for bit shifted event mask (2 bits): wait for event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_event(hevent) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for bit shifted event mask (2 bits): destroy event: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, Clear)
{
	IoscEvent hevent = IOSC_EVENT_INVALID;
	unsigned long mask_got = 0;

	hevent = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, hevent);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: create event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: set event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_clear_event(hevent, IOSC_TEST_EVENT_MASK));
	EXPECT_EQ(IOSC_OK, iosc_clear_event(hevent, IOSC_TEST_EVENT_MASK));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: clear event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	mask_got = 0;

	EXPECT_EQ( IOSC_TIMEOUT, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got, IOSC_EVENT_AND, IOSC_TIMEOUT_100) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: wait for event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( 0, mask_got);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: check if returned bitmask is untouched: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_event(hevent));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) set event/clear event: destroy event: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, WaitWithClearFlag)
{
	IoscEvent hevent = IOSC_EVENT_INVALID;
	unsigned long mask_got = 0;

	hevent = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, hevent);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: create event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: set event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(IOSC_OK, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got, IOSC_EVENT_AND | IOSC_EVENT_CLEAR, IOSC_TIMEOUT_100) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: wait for event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_TEST_EVENT_MASK, mask_got );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: check event bitmask: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	mask_got = 0;

	EXPECT_EQ(IOSC_TIMEOUT, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_MASK, &mask_got, IOSC_EVENT_AND, IOSC_TIMEOUT_100));
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: wait for null event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ(0, mask_got);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: check if returned bitmask is untouched: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_event(hevent) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) wait for event with clear flag: destroy event: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, ClearBadArg)
{
	IoscEvent hevent = IOSC_EVENT_INVALID;
	hevent = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, hevent);

	if (hevent == IOSC_EVENT_INVALID)
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try clear event with bad argument (mask=0): create event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_BADARG, iosc_clear_event(hevent, IOSC_TEST_EVENT_NULL_MASK) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try clear event with bad argument (mask=0): clear event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_event(hevent) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try clear event with bad argument (mask=0): destroy event: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, WaitBadArg)
{
	IoscEvent hevent = IOSC_EVENT_INVALID;
	unsigned long mask_got = 0;

	hevent = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, hevent);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try wait for event with bad argument (mask=0): create event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_BADARG, iosc_wait_for_event(hevent, IOSC_TEST_EVENT_NULL_MASK, &mask_got, IOSC_EVENT_OR | IOSC_EVENT_CLEAR, IOSC_TIMEOUT_100) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try wait for event with bad argument (mask=0): wait for event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_event(hevent) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try wait for event with bad argument (mask=0): destroy event: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, PerformSetAfterDestroy)
{
	IoscEvent hevent = IOSC_EVENT_INVALID;
	hevent = iosc_create_event(TEST_EVENT_ID);

	EXPECT_NE(IOSC_EVENT_INVALID, hevent);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'set' on a destroyed event: create event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_event(hevent) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'set' on a destroyed event: destroy event: FAILED");
		FAIL();
	}

	EXPECT_EQ( IOSC_BADARG, iosc_set_event(hevent, IOSC_TEST_EVENT_MASK) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'set' on a destroyed event: set event: FAILED");
		FAIL();
	}
}

TEST(EventIOSC, PerformClearAfterDestroy)
{
	IoscEvent hevent = IOSC_EVENT_INVALID;

	hevent = iosc_create_event(TEST_EVENT_ID);
	EXPECT_NE(IOSC_EVENT_INVALID, hevent);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'clear' on a destroyed event: create event: FAILED");
		iosc_destroy_event(hevent);
		FAIL();
	}

	EXPECT_EQ( IOSC_OK, iosc_destroy_event(hevent) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'clear' on a destroyed event: destroy event: FAILED");
		FAIL();
	}

	EXPECT_EQ( IOSC_BADARG, iosc_clear_event(hevent, IOSC_TEST_EVENT_MASK) );
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try to perform 'clear' on a destroyed event: set event: FAILED");
		FAIL();
	}
}

class CreateAndDestroyEventIOSC : public ::testing::Test {
	protected:
	void CreateAndDestroyEvents(tU32 u32Length, tBool bDifferentIDs);
};

void CreateAndDestroyEventIOSC::CreateAndDestroyEvents(tU32 u32Length, tBool bDifferentIDs)
{

	tU32 Cnt = 0;

	tU32 u32DifferentIDCoeff = (bDifferentIDs) ? 1 : 0;
	const char* sInsertString = (bDifferentIDs) ? "different IDs" : "single ID";

	tBool bSuccess = TRUE;

	IoscEvent* hEvents = (IoscEvent*) malloc ( sizeof (IoscEvent) * u32Length );

	EXPECT_FALSE(hEvents == NULL);
	if (HasNonfatalFailure())
	{
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try to create the max number of event handles (%s): allocate memory for event handles: %s", sInsertString, "FAILED");
		return;
	}

	for (Cnt = 0; Cnt < u32Length; Cnt++)
		hEvents[Cnt]= iosc_create_event(TEST_EVENT_ID + (u32DifferentIDCoeff * Cnt) );

	for (Cnt = 0; Cnt < u32Length; Cnt++)
	{
		if ( hEvents[Cnt] == IOSC_EVENT_INVALID)
			bSuccess = FALSE;

		if ( hEvents[Cnt] != hEvents[u32Length - 1] && !bDifferentIDs)
			bSuccess = FALSE;

		if (iosc_destroy_event(hEvents[Cnt]) != IOSC_OK)
			bSuccess = FALSE;
	}

	EXPECT_TRUE(bSuccess);

	if (!bSuccess)
		TracePrintf(TR_LEVEL_FATAL, "IOSC (event) try to create the max number of event handles (%s): FAILED", sInsertString);

	free(hEvents);
}

TEST_F(CreateAndDestroyEventIOSC, CreateMaxNumOfHandlesForOneEvent)
{
	CreateAndDestroyEvents(IOSC_TEST_MAX_EVENT, FALSE);
	if (HasNonfatalFailure())
		FAIL();
}

TEST_F(CreateAndDestroyEventIOSC, CreateMaxNumOfHandlesForDiffEvents)
{
	CreateAndDestroyEvents(IOSC_TEST_MAX_EVENT, TRUE);
	if (HasNonfatalFailure())
		FAIL();
}
