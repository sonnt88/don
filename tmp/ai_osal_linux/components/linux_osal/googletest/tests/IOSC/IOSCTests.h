/*
 * IOSCTests.h
 *
 *  Created on: Feb 16, 2012
 *      Author: oto4hi
 */

#ifndef IOSCTESTS_H_
#define IOSCTESTS_H_

#include <persigtest.h>

extern "C" {
#if OSAL_OS == OSAL_LINUX
#include <iosc_userspace_lib.h>
#endif

#if OSAL_OS == OSAL_TENGINE
#include <iosc.h>
#endif
}

#define IOSC_TIMEOUT_100 100

#endif /* IOSCTESTS_H_ */
