#include <persigtest.h>

#if OSAL_OS==OSAL_TENGINE

// do not change this function: this is a rvct linker workaround
void anchorfunc(void)
{
  return;
}

//Dummy Tests: just for experiments with PersiGTest
#include "DummyTest.cpp"

//IOSC
#include "IOSC/EventTest.cpp"
#include "IOSC/RingBufferTest.cpp"
#include "IOSC/SemaphoreTest.cpp"
#include "IOSC/SharedMemTest.cpp"

/*
//gtest selftests
#include "selftests/gtest-filepath_test.cpp"
#include "selftests/gtest-linked_ptr_test.cpp"
#include "selftests/gtest-message_test.cpp"
#include "selftests/gtest-options_test.cpp"
#include "selftests/gtest-port_test.cpp"
#include "selftests/gtest_pred_impl_unittest.cpp"
#include "selftests/gtest_prod_test.cpp"
#include "selftests/gtest-test-part_test.cpp"
#include "selftests/gtest-typed-test2_test.cpp"
#include "selftests/gtest-typed-test_test.cpp"
#include "selftests/gtest_unittest.cpp"
#include "selftests/production.cpp"

//gmock selftests
#include "selftests/GoogleMock/gmock-actions_test.cpp"
#include "selftests/GoogleMock/gmock-cardinalities_test.cpp"
#include "selftests/GoogleMock/gmock-generated-actions_test.cpp"
#include "selftests/GoogleMock/gmock-generated-function-mockers_test.cpp"
#include "selftests/GoogleMock/gmock-generated-internal-utils_test.cpp"
#include "selftests/GoogleMock/gmock-generated-matchers_test.cpp"
#include "selftests/GoogleMock/gmock-internal-utils_test.cpp"
#include "selftests/GoogleMock/gmock-matchers_test.cpp"
#include "selftests/GoogleMock/gmock-more-actions_test.cpp"
#include "selftests/GoogleMock/gmock-nice-strict_test.cpp"
#include "selftests/GoogleMock/gmock-port_test.cpp"
#include "selftests/GoogleMock/gmock-spec-builders_test.cpp"
#include "selftests/GoogleMock/gmock_test.cpp"
*/

#endif //OSAL_OS==OSAL_TENGINE
