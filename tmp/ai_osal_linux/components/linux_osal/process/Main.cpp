
#include <semaphore.h>
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "osal_public.h"

#include "errorlogger.h"
#include "errorlog_if.h"
#include "osalrpc_if.h"
#include "socketconnection.h"
#include "socketgroup.h"


tS32 WOS_s32IOEngineeringFct   ( const char *device, tS32 fun, tS32 arg ){ return OSAL_ERROR; }

#ifdef __cplusplus
extern "C" {
#endif


// Dummy Function for OSAL_PURE Compatibility
void  vTerminateCcaApplications(void){};

void tclSocketThread::ThreadEntry( void* pvArg )
{
   tclSocketThread *ptr = (tclSocketThread*) pvArg;
  (ptr->m_pThreadEntry)(ptr->m_pvArg);
}

bool tclSocketThread::bStartThread(fpThreadEntry ThreadFunction,void* ThreadArg)
{
    char Name[32];
   m_pThreadEntry = ThreadFunction;
   m_pvArg = ThreadArg;
   OSAL_trThreadAttribute  attr;
   snprintf(Name,32,"Test%d",OSAL_ClockGetElapsedTime());
   attr.szName = (tString)Name;/*lint !e1773 */  /*otherwise linker warning */
   attr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   attr.s32StackSize = 4096;
   attr.pfEntry = (OSAL_tpfThreadEntry)ThreadFunction;
   attr.pvArg = ThreadArg;
   if(OSAL_ThreadSpawn(&attr) != OSAL_ERROR)return TRUE;
   else return FALSE;
}


OSAL_tclErrorLogger *pel;
tclSocketGroup *g_poSocketGroup;
char  szIpAddress[32];

int main(int argc,char** argv)
{
   OSAL_tIODescriptor s32DevDesc = 0;
   tS32 s32Port = 3000;
   OSAL_trIOCtrlRegistry hReg;
   int opt,msecs = OSAL_C_TIMEOUT_FOREVER;
 
   /* check for timeout option ./osalprc.out -t 1000 */
   while ((opt = getopt (argc, argv, "t:")) != -1)
   {
      switch (opt)
      {
         case 't':
              msecs = atoi(optarg);
            break;
         default:
               TraceString("Usage: %s [-t msecs] [-n] name",argv[0]);
            break;
      }
   }
   TraceString("Process will stay alive for %d msec",msecs);
 
   /* check for socket configuration */
   if((s32DevDesc = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY"/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL",OSAL_EN_READWRITE)) != OSAL_ERROR )
   {
      hReg.pcos8Name =  (tPCS8)"SOCKET";
      hReg.s32Type   =  OSAL_C_S32_VALUE_STRING;
      hReg.ps8Value  =  (tPU8)szIpAddress;
      hReg.u32Size   =  32;
      if(OSAL_s32IOControl(s32DevDesc,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&hReg) == OSAL_OK)
      {
        hReg.pcos8Name =  (tPCS8)"PORT";
        hReg.s32Type   =  OSAL_C_S32_VALUE_S32;
        hReg.ps8Value  =  (tPU8)&s32Port;
        hReg.u32Size   =  sizeof(tU32);
        if(OSAL_s32IOControl(s32DevDesc,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&hReg) == OSAL_OK)
	{
           TraceString("Start Socket Server for Port %d ",s32Port);
           pel = new OSAL_tclErrorLogger();
           if(pel)
           {
              g_poSocketGroup = new tclSocketGroup( pel );
              if(g_poSocketGroup)
              {
                 g_poSocketGroup->vSocketServerGroupInit(s32Port);
              }
              else
              {
                 TraceString("Cannot instantiate tclSocketGroup object ");
                 delete pel;
              }
           }
           else
           {
              TraceString("Cannot instantiate Error Logger object ");
           }
        }
      }
      OSAL_s32IOClose(s32DevDesc);
   }

   OSAL_s32ThreadWait(msecs);

   OSAL_vProcessExit();
}


#ifdef __cplusplus
}
#endif

/************************************************************************
|end of fileMAin.cpp
|-----------------------------------------------------------------------*/
