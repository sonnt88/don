/*****************************************************************************
| FILE:         osalgawprocess.c
| PROJECT:      PF NGA
| SW-COMPONENT: OSAL CORE
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) Startup-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 12.10.10  | Initial revision           | OSC2HI
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#define SCD_S_IMPORT_INTERFACE_GENERIC
#include "scd_if.h"

#include "el_gaw_App.h"

#define PROC_SHUTDOWN_EVENT_NAME "GAW_SHATDOWN"

OSAL_tEventHandle  hEvShutdown = 0;
OSAL_tEventMask    hEvRequest  = 0x00000001;

static gaw_tclApp* pApp=NULL;

template <class CCA_Class> CCA_Class *pCreateCcaApp(CCA_Class *, const char *, int);
#define CCA_CREATE_APP(CCA_ClassName, poCCA_Class, ccaId) ( pCreateCcaApp<CCA_ClassName>(poCCA_Class, #CCA_ClassName, ccaId))
template <class CCA_Class> CCA_Class *vDestroyCcaApp(CCA_Class *, const char *);
#define CCA_DESTROY_APP(CCA_ClassName, poCCA_Class) (vDestroyCcaApp<CCA_ClassName>(poCCA_Class, #CCA_ClassName))

#ifdef __cplusplus
extern "C" {
#endif


OSAL_DECL tS32 main(int argc,char** argv) 
{
   int opt,msecs = OSAL_C_TIMEOUT_FOREVER;
 
   /* check for timeout option ./osalprc.out -t 1000 */
   while ((opt = getopt (argc, argv, "t:")) != -1)
   {
      switch (opt)
      {
         case 't':
              msecs = atoi(optarg);
            break;
         default:
               TraceString("Usage: %s [-t msecs] [-n] name",argv[0]);
            break;
      }
   }
   TraceString("Process will stay alive for %d msec",msecs);

   if (OSAL_s32EventCreate(PROC_SHUTDOWN_EVENT_NAME, &hEvShutdown) == OSAL_OK)
   {
 //     pApp = CCA_CREATE_APP(gaw_tclApp, pApp, CCA_C_U16_APP_GATEWAY);
       scd_init();
	   
       pApp = OSAL_NEW gaw_tclApp();
	   pApp->bInitInstance(0,CCA_C_U16_APP_GATEWAY);
       OSAL_s32EventWait(hEvShutdown, hEvRequest, OSAL_EN_EVENTMASK_OR, msecs, &hEvRequest);
       OSAL_s32EventClose(hEvShutdown);
       OSAL_s32EventDelete(PROC_SHUTDOWN_EVENT_NAME);
 
       pApp->vDeinitInstance ();
   }
   OSAL_vProcessExit();
   return 0;
}
#ifdef __cplusplus
}
#endif
