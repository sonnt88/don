
#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"

#include "ostrace.h"

#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif



#define EVENT_NAME "PRM_Test"
#define PATTERN_ERRMEMINI   0x00000001
#define PATTERN_DNL_SUCCESS 0x00000002
#define PATTERN_DNL_FAILED  0x00000004


static tU32 u32SysInfoData = 0;
static char PathBuffer[MAX_MOUNTPOINT_STRING_SIZE];


void vMediaNotifyHandler( tU32 *pu32ModeChangeInfo, tU8 au8String[] )
{
    tU16 u16Status = OSAL_C_U16_MEDIA_EJECTED;
	tS32 Len = 0;
    OSAL_tEventHandle hEvent = OSAL_C_INVALID_HANDLE;	
	if(pu32ModeChangeInfo)u16Status = LOWORD(*pu32ModeChangeInfo);
	
    if((u16Status != OSAL_C_U16_MEDIA_EJECTED))
    {
        TraceString("vMediaNotifyHandler with Status:%d received Check Content ",u16Status);
 
        OSAL_tIODescriptor fd;
        strcpy(&PathBuffer[0],"/dev/media/");
        strcat(PathBuffer,(const char*)au8String); 
        TraceString("vMediaNotifyHandler Path:%s",PathBuffer);
        Len = strlen(PathBuffer);
        strcat(PathBuffer,"/errmem.ini");          /* add specific file name in the root of the medium */
        if((fd = OSAL_IOOpen(PathBuffer,OSAL_EN_READONLY)) != OSAL_ERROR )
        {
           if(OSAL_s32EventOpen(EVENT_NAME, &hEvent) == OSAL_OK)
		   {
              OSAL_s32EventPost(hEvent,PATTERN_ERRMEMINI, OSAL_EN_EVENTMASK_OR);
			  OSAL_s32EventClose(hEvent);
		   }
           OSAL_s32IOClose(fd);
		}
		PathBuffer[Len] = '\0';
    }
}

#ifdef PRM_LIBUSB_CONNECTION
#define OEDT_USBPWR_OCACT_MASK      0x00001000
#define OEDT_USBPWR_OCINACT_MASK    0x00002000
#define OEDT_SIG_TRUE               2
#define OEDT_SIG_FALSE              1
#define OEDT_SIG_UNDEF              0

typedef struct
{
   tU8  u8PortNr;
   tU8  u8OC;
   tU8  u8UV;
   tU8  u8PPON;

   tU32 u32OCStartTime;
   tU32 u32OCEndTime;
   tU32 u32UVStartTime;
   tU32 u32UVEndTime;
   tU32 u32PPONStartTime;
   tU32 u32PPONEndTime;
}OEDTUsbPortState;


void vUSBPWR_OC_Callback(tPCU32 pu32ModeChangeInfo, void* pVoid)
{

   OEDTUsbPortState *pPortState = (OEDTUsbPortState *)pVoid;
   tU8 *u8states[] = {(tU8 *)"UNDEF", (tU8 *)"INACTIVE", (tU8 *)"ACTIVE"};
   TraceString("ENTER vUSBPWR_OC_Callback 0x%08x",*pu32ModeChangeInfo);
   if(pPortState)
   {
         TraceString("trPortState->u8PortNr: \t%-10d  ",pPortState->u8PortNr);
         TraceString("trPortState->u8OC:     \t%-10s  [StartTime=%-10d   EndTime=%-10d]  ",u8states[pPortState->u8OC],  pPortState->u32OCStartTime,  pPortState->u32OCEndTime);
         TraceString("trPortState->u8UV:     \t%-10s  [StartTime=%-10d   EndTime=%-10d]  ",u8states[pPortState->u8UV],  pPortState->u32UVStartTime,  pPortState->u32UVEndTime);
         TraceString("trPortState->u8PPON:   \t%-10s  [StartTime=%-10d   EndTime=%-10d]  ",u8states[pPortState->u8PPON],pPortState->u32PPONStartTime,pPortState->u32PPONEndTime);
   }

   /*check for whether event notification should be performed or not*/
      //Lint fix for Warning 613: Possible use of null pointer 'pPortState' in left argument to operator '->'
      if((pPortState !=NULL) && (pPortState->u8OC == (tU8)OEDT_SIG_TRUE))
      {
         TraceString("vUSBPWR_OC_Callback OEDT_USBPWR_OCACT_MASK: 0x%08x",(tU32)OEDT_USBPWR_OCACT_MASK);
      }
      //Lint fix for Warning 613: Possible use of null pointer 'pPortState' in left argument to operator '->'
      else if((pPortState !=NULL) && (pPortState->u8OC == (tU8)OEDT_SIG_FALSE))
      {
		 TraceString("vUSBPWR_OC_Callback OEDT_USBPWR_OCINACT_MASK: 0x%08x",(tU32)OEDT_USBPWR_OCINACT_MASK);
      }
      else
      { 
        TraceString("vUSBPWR_OC_Callback UnDefined ev OC state 0x%08x",*pu32ModeChangeInfo);
      }
}

void vUSB_PWR_CTRL_test(void)
{
   tS32 s32FunRetVal = OSAL_OK;
   tS32 s32Mask = 1;
   tU8 u8Count;
   tS32 PortCount = -1;
   UsbPortState rPortState = {0};
   OSAL_tIODescriptor hDevice;

   usb_port_control cmask[2] = {{1,0}, //OFF
										{1,1}};//ON
   usb_port_control cmask_temp;

	hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY );
    if(hDevice != OSAL_ERROR)
	{
		s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTCOUNT, (intptr_t)&PortCount);
      
      //Supress lint info 774: Boolean within 'if' always evaluates to False 
     //lint -e774 
		if((s32FunRetVal == OSAL_OK)&&(PortCount > 0))
		{
	            TraceString("OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTCOUNT returns %d",PortCount);
     
         //Supress Warning info 681: Loop is not entered
         //lint -e681
			for(u8Count = 1; u8Count <=PortCount; u8Count++)
			{

				rPortState.u8PortNr = u8Count;

				TraceString("PRM_USBPOWER_CTRL of PORT %d: START\n",u8Count);


					 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE, (intptr_t)&rPortState);
					 if(s32FunRetVal == OSAL_OK)
						 TraceString("OK: PRM_USBPOWER_GET_PORTSTATE INITIAL PORTSTATE of %d is %s",u8Count,(rPortState.u8PPON == 0x2)?"ON":"OFF");
					 else
						 TraceString("ERROR: PRM_USBPOWER_GET_PORTSTATE of %d ERROR:0x%X",u8Count,s32FunRetVal);

				 OSAL_s32ThreadWait(3000);

				 if(rPortState.u8PPON == 0x2) //IF PP ON true, switch OFF port else power ON first
					 s32Mask = 0; //OFF
				 else
					 s32Mask = 1;//ON

				 cmask_temp.PortMask = cmask[s32Mask].PortMask << (u8Count-1);
				 cmask_temp.ValueMask = cmask[s32Mask].ValueMask << (u8Count-1);

					 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_SET_PORTPOWER, (intptr_t)&cmask_temp);
					 if(s32FunRetVal == OSAL_E_NOERROR)
						  TraceString("OK: PRM_USBPOWER_SET_PORTPOWER of %d to %s",u8Count,s32Mask?"ON":"OFF");
					 else
						  TraceString("ERROR: PRM_USBPOWER_SET_PORTPOWER of %d ERROR:0x%X",u8Count,s32FunRetVal );

				 OSAL_s32ThreadWait(3000);

				 rPortState.u8PortNr = u8Count;
					 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE, (intptr_t)&rPortState);
					 if(s32FunRetVal == OSAL_OK)
						  TraceString("OK: PRM_USBPOWER_GET_PORTSTATE of %d is %s",u8Count,(rPortState.u8PPON == 0x2)?"ON":"OFF");
					 else
						 TraceString("ERROR: PRM_USBPOWER_GET_PORTSTATE of %d ERROR:0x%X",u8Count,s32FunRetVal);

					 s32Mask = (s32Mask+1)%2; /*Toggle State*/
					 cmask_temp.PortMask = cmask[s32Mask].PortMask << (u8Count-1);
					 cmask_temp.ValueMask = cmask[s32Mask].ValueMask << (u8Count-1);

				 OSAL_s32ThreadWait(3000);

					 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_SET_PORTPOWER, (intptr_t)&cmask_temp);
					 if(s32FunRetVal == OSAL_E_NOERROR)
						  TraceString("OK: PRM_USBPOWER_SET_PORTPOWER of %d  to %s  ",u8Count, s32Mask?"ON":"OFF");
					 else
						  TraceString("ERROR: PRM_USBPOWER_SET_PORTPOWER of %d ERROR:0x%X",u8Count,s32FunRetVal );

				 OSAL_s32ThreadWait(3000);

				 rPortState.u8PortNr = u8Count;
				 s32FunRetVal = OSAL_s32IOControl(hDevice, OSAL_C_S32_IOCTRL_PRM_USBPOWER_GET_PORTSTATE, (intptr_t)&rPortState);

					 if(s32FunRetVal == OSAL_OK)
						  TraceString("OK: PRM_USBPOWER_GET_PORTSTATE  of %d is %s",u8Count,(rPortState.u8PPON == 0x2)?"ON":"OFF");
					 else
						  TraceString("ERROR: PRM_USBPOWER_GET_PORTSTATE of %d ERROR:0x%X",u8Count,s32FunRetVal);


					  TraceString("PRM_USBPOWER_CTRL of PORT %d: END\n",u8Count);
			}
		}
		else
			  TraceString("ERROR: PRM_USBPOWER_GET_PORTCOUNT with %d , ERR:0x%X",PortCount ,s32FunRetVal);
		OSAL_s32IOClose( hDevice );
	}
	else
	{
	 TraceString("ERROR: OSAL_IOOpen for USB_Power");
	}
}
#endif

void RegisterForDevMedia(void)
{
    OSAL_tIODescriptor fd;
    OSAL_trNotifyDataExt2 rNotify2;

    // register callback function
    fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY);
    if( fd != OSAL_ERROR )
    {
	    TraceString("RegisterForDevMedia");
        rNotify2.u16AppID = OSAL_C_U16_DAPI_APPID;
        rNotify2.ResourceName = "/dev/media";
        rNotify2.u16NotificationType = OSAL_C_U16_NOTI_MEDIA_CHANGE;
        rNotify2.pCallbackExt2 = vMediaNotifyHandler;

        if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2, (uintptr_t)&rNotify2) == OSAL_ERROR)
        {
             NORMAL_M_ASSERT_ALWAYS();
        }
#ifdef PRM_LIBUSB_CONNECTION
        OSAL_trNotifyDataExt2 rNotifyData;
	    rNotifyData.pu32Data            = 0;
        rNotifyData.ResourceName        = "/dev/usbpower" ;
        rNotifyData.u16AppID            = OSAL_C_U16_DAPI_APPID ;
        rNotifyData.u8Status            = 0;
        rNotifyData.u16NotificationType = OSAL_C_U16_NOTI_LOW_POW;
        rNotifyData.pCallbackExt2       = (OSALCALLBACKFUNCEXT2)vUSBPWR_OC_Callback;
        if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_REG_NOTIFICATION_EXT2, (uintptr_t)&rNotifyData) == OSAL_ERROR)
        {
             NORMAL_M_ASSERT_ALWAYS();
        }
#endif
        OSAL_s32IOClose(fd);
    }
}

void vSysInfoNotiHandler( tU32 *pu32SysInfo)
{
    OSAL_tEventHandle hEvent = OSAL_C_INVALID_HANDLE;	
   switch(*pu32SysInfo)
   {
	   case WRITE_ERRMEM_START:
	        TraceString("vSysInfoNotiHandler received WRITE_ERRMEM_START");
	   break;
	   case WRITE_ERRMEM_SUCCESS:
	        TraceString("vSysInfoNotiHandler received WRITE_ERRMEM_SUCCESS");
            if(OSAL_s32EventOpen(EVENT_NAME, &hEvent) == OSAL_OK)
            {
              OSAL_s32EventPost(hEvent,PATTERN_DNL_SUCCESS, OSAL_EN_EVENTMASK_OR);
			  OSAL_s32EventClose(hEvent);
		    }
	   break;
	   case WRITE_ERRMEM_FAILED:
	        TraceString("vSysInfoNotiHandler received WRITE_ERRMEM_FAILED");
            if(OSAL_s32EventOpen(EVENT_NAME, &hEvent) == OSAL_OK)
            {
              OSAL_s32EventPost(hEvent,PATTERN_DNL_FAILED, OSAL_EN_EVENTMASK_OR);
			  OSAL_s32EventClose(hEvent);
		    }
	   break;
	   case ERROR_MOUNT_FAILED:
	        TraceString("vSysInfoNotiHandler received ERROR_MOUNT_FAILED");
	   break;
	   default:
	        TraceString("vSysInfoNotiHandler unknown status received %d",*pu32SysInfo);
	   break; 
   }
}
void RegisterForSystemInfo(void)
{
	tS32 OSAL_s32EventCreate(tCString coszName, OSAL_tEventHandle* phEvent);
    OSAL_tIODescriptor fd;
    OSAL_trNotifyDataSystem rNotifyData;

    // register callback function
    fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY);
    if( fd != OSAL_ERROR )
    {
       TraceString("RegisterForSystemInfo");
       rNotifyData.u16AppID   = OSAL_C_U16_DAPI_APPID;
       rNotifyData.pCallback = vSysInfoNotiHandler;
       rNotifyData.pu32Data  = &u32SysInfoData;
       if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_REG_SYSTEM_INFO, (uintptr_t)&rNotifyData) == OSAL_ERROR)
       {
             NORMAL_M_ASSERT_ALWAYS();
       }
       OSAL_s32IOClose(fd);
    }
}

void vWriterToUsbStick(void)
{
     OSAL_tIODescriptor fd,fdprm;
     char FileBuffer[256];
     fdprm = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY);
     if( fdprm != OSAL_ERROR )
     {
         OSAL_trRemountData rRemountData;
         rRemountData.u16AppID = OSAL_C_U16_DAPI_APPID;                /* ID of the application                    */
         rRemountData.szPath    = PathBuffer;
         rRemountData.szOption = "rw";         
         if(OSAL_s32IOControl(fdprm, OSAL_C_S32_IOCTRL_PRM_REMOUNT, (uintptr_t)&rRemountData) != OSAL_ERROR)
         {
            strcpy(&FileBuffer[0],&PathBuffer[0]);
            strcat(FileBuffer,"/navtest.txt");

            TraceString("OSAL_IOCreate %s",FileBuffer);
            fd  = OSAL_IOCreate(&FileBuffer[0], OSAL_EN_READWRITE );
            if(fd != OSAL_ERROR )
            {
                TraceString("OSAL_s32IOWrite %s","Dummy");
                OSAL_s32IOWrite(fd,(tPCS8)"Dummy",5);
                OSAL_s32IOClose(fd);
            }

            rRemountData.szOption = "r";         
            if(OSAL_s32IOControl(fdprm, OSAL_C_S32_IOCTRL_PRM_REMOUNT, (uintptr_t)&rRemountData) != OSAL_ERROR)
            {
            }
         }
         OSAL_s32IOClose(fdprm);
     }
}


void vNotification(void* pArg)
{
   OSAL_tEventMask Mask;
   OSAL_tIODescriptor fd,fd_file_src,fd_file_dest,Ret;
   OSAL_trRemountData rRemountData;
   char Name[OSAL_C_U32_MAX_PATHLENGTH];
   OSAL_tEventHandle hEvent = OSAL_C_INVALID_HANDLE;	
   
   TraceString("First Test !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

   tS32 Size = OSAL_ERROR;
   OSAL_trIOCtrlRegistry rReg;
   char szErrPathName[OSAL_C_U32_MAX_PATHLENGTH];
   rReg.pcos8Name = (tCS8*)"ERRMEM_DUMP_PATH";
   rReg.ps8Value  = (tU8*)&szErrPathName;
   rReg.u32Size   = OSAL_C_U32_MAX_PATHLENGTH;
   rReg.s32Type = OSAL_C_S32_VALUE_STRING;
   if((fd = OSAL_IOOpen("/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS/OSAL" ,OSAL_EN_READONLY)) != OSAL_ERROR )
   {
      Size = OSAL_s32IOControl(fd,OSAL_C_S32_IOCTRL_REGGETVALUE,(uintptr_t)&rReg);
      TraceString("vNotification OSAL_C_S32_IOCTRL_REGGETVALUE success %d",Size);
      OSAL_s32IOClose(fd);
   }
   
   if(OSAL_s32EventCreate(EVENT_NAME,&hEvent) == OSAL_OK)
   {
      RegisterForDevMedia(); 
      RegisterForSystemInfo();
   
      /*for finished download*/
      OSAL_s32EventWait(hEvent,PATTERN_ERRMEMINI, OSAL_EN_EVENTMASK_OR,1000000,NULL);
      (void)OSAL_s32EventPost(hEvent, ~PATTERN_ERRMEMINI, OSAL_EN_EVENTMASK_AND);
      TraceString("vNotification PATTERN_ERRMEMINI received %d",PATTERN_ERRMEMINI);

      OSAL_s32EventWait(hEvent,PATTERN_DNL_SUCCESS|PATTERN_DNL_FAILED, OSAL_EN_EVENTMASK_OR,1000000,&Mask);
      (void)OSAL_s32EventPost(hEvent, ~Mask, OSAL_EN_EVENTMASK_AND);
      TraceString("vNotification ERRMEM DNL finished with Status received %d",Mask);
	  
      if(szErrPathName[0] != 0)
      {
        strcpy(&Name[0],&szErrPathName[0]);
        Size = strlen(Name);
        strcat(Name,"/Errmem100.pro");
        TraceString("vNotification open %s ",Name);
        if((fd_file_src = OSAL_IOOpen(Name,OSAL_EN_READONLY)) != OSAL_ERROR )
        {
           char* pBuffer = NULL;
           Size = OSALUTIL_s32FGetSize(fd_file_src);
	       if(Size)pBuffer = (char*)malloc(Size);
	       if(pBuffer)
	       {
              Ret = OSAL_s32IORead(fd_file_src,(tPS8)pBuffer,Size);
              TraceString("vNotification read %d Bytes from %s ",Ret,Name);
		   }
           OSAL_s32IOClose(fd_file_src);
           OSAL_s32IORemove(Name);
		   
           if((fd = OSAL_IOOpen(OSAL_C_STRING_DEVICE_PRM, OSAL_EN_READONLY))!= OSAL_ERROR )
           {
              rRemountData.u16AppID = OSAL_C_U16_DAPI_APPID;                /* ID of the application                    */
              rRemountData.szPath    = PathBuffer;
              rRemountData.szOption = "rw";         
              if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_REMOUNT, (uintptr_t)&rRemountData) != OSAL_ERROR)
              {
                 strcpy(&Name[0],&PathBuffer[0]);
                 strcat(Name,"/Errmem100.pro");
			     TraceString("vNotification create %s",Name);
                 if((fd_file_dest = OSAL_IOCreate(Name,OSAL_EN_READWRITE)) != OSAL_ERROR)
                 {
                    Ret = OSAL_s32IOWrite(fd_file_dest,(tPCS8)pBuffer,Size);
                    TraceString("vNotification write %d Bytes to %s ",Ret,Name);
                    OSAL_s32IOClose(fd_file_dest);
                 }
                 else
                 {
                    TraceString("vNotification create %s failed errno:%d",Name,errno);				 
                 }
              } 
              rRemountData.szOption = "r";         
              if(OSAL_s32IOControl(fd, OSAL_C_S32_IOCTRL_PRM_REMOUNT, (uintptr_t)&rRemountData) != OSAL_ERROR)
              {
                 TraceString("OSAL_C_S32_IOCTRL_PRM_REMOUNT ro failed");
              }		   
              OSAL_s32IOClose(fd);
           }
           if(pBuffer)free(pBuffer);
        }
     }
     else
     {
        /* Download finished default */
     }
     OSAL_s32EventClose(hEvent);
     OSAL_s32EventDelete(EVENT_NAME); 
   }
   
   OSAL_s32ThreadWait(5000);
   
   TraceString("Second Test !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
   vWriterToUsbStick();
}





void StartTestThread(void)
{
   OSAL_trThreadAttribute  attr;
   attr.szName = (tString)"Writer";/*lint !e1773 */  /*otherwise linker warning */
   attr.u32Priority = 100;
   attr.s32StackSize = 1024*128;
   attr.pfEntry = (OSAL_tpfThreadEntry)vNotification;
   attr.pvArg = NULL;
   if(OSAL_ThreadSpawn(&attr) != OSAL_ERROR)
   {
   }

}




int main(int argc,char** argv)
{
   int opt,msecs = OSAL_C_TIMEOUT_FOREVER;
 
   /* check for timeout option ./osalprc.out -t 1000 */
   while ((opt = getopt (argc, argv, "t:")) != -1)
   {
      switch (opt)
      {
         case 't':
              msecs = atoi(optarg);
            break;
         case 'p':
#ifdef PRM_LIBUSB_CONNECTION
         void vUSB_PWR_CTRL_test();
#endif		
            break;	
         default:
               TraceString("Usage: %s [-t msecs] [-n] name",argv[0]);
            break;
      }
   }
   
   StartTestThread();   
   
   TraceString("Process will stay alive for %d msec",msecs);
 
   OSAL_s32ThreadWait(msecs);

   OSAL_vProcessExit();
}


#ifdef __cplusplus
}
#endif

/************************************************************************
|end of fileMAin.cpp
|-----------------------------------------------------------------------*/
