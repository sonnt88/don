/******************************************************************************
* FILE:         pure_osal_errorlogger.cpp
*
* PROJECT:      OSAL
*
* SW-COMPONENT: OSAL
*
*-----------------------------------------------------------------------------
* DESCRIPTION:  This is the sourcefile for the implementation of the class
*               OSAL_tclErrorLogger.
*-----------------------------------------------------------------------------
* COPYRIGHT:    (c) 2006 RBIN / EDI2
* HISTORY:      
* Date      | Author   | Modification
* 02.01.06  | Ananya M | Initial version 
*****************************************************************************/


/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#if (OSAL_OS == OSAL_WINCE)
#include "pure_osal_OSIO.h"
#elif (OSAL_OS == OSAL_LINUX)
//#include "pure_osal_OSIO.h"
#include <unistd.h>
#include <string>
#endif

#include "errorlogger.h"

extern OSAL_tclErrorLogger *pel; //For logging error messages

/******************************************************************************
class description:   OSAL_tclErrorLogger is a class to allow logging of error 
messages primarily for testing and debugging purposes.
All messages are written to a log file whose path is defined
in the registry. All messages whose error levels are 
numerically greater than the set error level in the registry 
are not logged.

There is also an option to add filename and line number
while logging. Use ADDINFO or ADDNOINFO.

Messages can be either formatted ASCII strings or 
WCHAR strings

Log File is an ASCII file.

NOTE: OSAL_tclErrorLogger has native implementation in CPP
******************************************************************************/

/******************************************************************************
*  FUNCTION:      OSAL_tclErrorLogger 
*------------------------------------------------------------------------------  
*  DESCRIPTION:   Constructor for the class OSAL_tclErrorLogger obtains the Log
*                 File Name and Path from the Registry, Creates a default file 
*                 if there are no Registry Entries
*------------------------------------------------------------------------------   
*  PARAMETERS:    NONE
*------------------------------------------------------------------------------       
*  RETURN TYPE:   NONE.
*------------------------------------------------------------------------------
*  HISTORY:
*------------------------------------------------------------------------
*  Date      |   Modification               | Author
*------------|------------------------------|-----------------------------
*  02.01.06  |   Initial revision           | Ananya Muddukrishna RBIN\EDI2
*         
******************************************************************************/

OSAL_tclErrorLogger :: OSAL_tclErrorLogger() : OSAL_tclGlobalObjectInterface()
{
   RegErrorLevel = 8;
   hLF = NULL;
}

/******************************************************************************
*   FUNCTION:      ~OSAL_tclErrorLogger 
*------------------------------------------------------------------------------
*   DESCRIPTION:   Destructor for the class OSAL_tclErrorLogger, Closes the 
*                  Semaphore and File Handle.
*------------------------------------------------------------------------------
*   PARAMETERS:    NONE.
*------------------------------------------------------------------------------
*   RETURN TYPE:   NONE.
*------------------------------------------------------------------------------
*   HISTORY:
*------------------------------------------------------------------------------
*  Date      |   Modification               | Author
*------------|------------------------------|-----------------------------
*  02.01.06  |   Initial revision           | Ananya Muddukrishna RBIN\EDI2
*         
******************************************************************************/

OSAL_tclErrorLogger :: ~OSAL_tclErrorLogger()
{
    hLF = NULL;
    this->bUninitialize();
}

/******************************************************************************
*   FUNCTION:      OSAL_LogError 
*------------------------------------------------------------------------------
*   DESCRIPTION:   To log error messages in ASCII string format
*                 into log file. Variable argument function similar to printf
*------------------------------------------------------------------------------
*   PARAMETERS:    const char* error_file [I]
*                        name of the file having this function call
*                  int error_line [I]
*                        number of the line having this function call
*                  OSAL_ERRORLEVEL eErrorLevel [I]
*                       enum variable to take error type
*                 const char* ErrorString [I]
*                       formatted error message string
*                 ... [I]
*                 variable argument list
*------------------------------------------------------------------------------
*   RETURN TYPE:   signed int
*                 returns 0 on success, -1 on failure
*
*------------------------------------------------------------------------
*  Date      |   Modification               | Author
*------------|------------------------------|-----------------------------
*  02.01.06  |   Initial revision           | Ananya Muddukrishna RBIN\EDI2
*         
******************************************************************************/
//lint -e{40,10,1055}
tInt OSAL_tclErrorLogger::OSAL_LogError( tPCChar error_file, tInt error_line, 
                                       OSAL_ERRORLEVEL eErrorLevel, 
                                       tPCChar ErrorString, ... )
{
   OSAL_tVarArgList VArgList; 
   OSAL_VarArgStart(VArgList, ErrorString);
   return OSAL_LogErrorInt(error_file, error_line, eErrorLevel, ErrorString, VArgList);
}

tInt OSAL_tclErrorLogger::OSAL_LogErrorInt( tPCChar error_file, tInt error_line, 
                                            OSAL_ERRORLEVEL eErrorLevel, 
                                            tPCChar ErrorString, OSAL_tVarArgList VArgList )
{
   if(m_bInitialized == false)
   {
      return -1;
   }

   if ( ( tU32 ) eErrorLevel > RegErrorLevel || ErrorString == NULL)
   {
      return -1;
   }

   tChar FormatBuffer[EL_REG_BUFFER_SIZE] = {0};
   OSALUTIL_s32SaveVarNPrintFormat(FormatBuffer, EL_REG_BUFFER_SIZE - 2 , ErrorString, VArgList);

   if( !(0 == error_line && NULL == error_file) )
   {
      tChar InfoBuffer[EL_REG_BUFFER_SIZE] = {0};

      OSALUTIL_s32SaveNPrintFormat(InfoBuffer,
                                   EL_REG_BUFFER_SIZE - 2,
                                   "%s (%d) : %s : %s", 
                                   OSAL_GetErrorFileName(error_file), 
                                   error_line,
                                   ERRORLEVELNAME[(tInt)eErrorLevel - 1],
                                   FormatBuffer);

      OSALUTIL_szSaveStringNCopy(FormatBuffer,InfoBuffer,EL_REG_BUFFER_SIZE - 2 );
   }

   OSALUTIL_szSaveStringNConcat((tChar*)FormatBuffer, "\n" , 2 );
   oErrLogSem.s32Wait( OSAL_C_TIMEOUT_FOREVER );
   if( NULL != hLF ) // added for lint corrections
   {
      fwrite( FormatBuffer, sizeof(tChar), strlen(FormatBuffer), hLF);
      fflush(hLF);
   }
   oErrLogSem.s32Post();
   OSAL_VarArgEnd(VArgList);//lint !e40 !e10 !e1055
   return 0;
}

/******************************************************************************
*   FUNCTION:      OSAL_GetErrorFileName 
*------------------------------------------------------------------------------
*   DESCRIPTION:   To extract the errorfile name from a given errorfile path.
*------------------------------------------------------------------------------
*   PARAMETERS:    const char* ErrorFilePath [I] : errorfile path
*------------------------------------------------------------------------------
*   RETURN TYPE:   const char* : returns errorfile name
*------------------------------------------------------------------------------
*   HISTORY:
*------------------------------------------------------------------------------
*  Date      |   Modification               | Author
*------------|------------------------------|----------------------------------
*  02.01.06  |   Initial revision           | Ananya Muddukrishna RBIN\EDI2
*         
******************************************************************************/

tPCChar OSAL_tclErrorLogger::OSAL_GetErrorFileName( tPCChar ErrorFilePath )
{
   tPChar ErrorFileName = 0;
   tPChar temp = 0;
   tPChar lintTemp = 0; // added for lint errors corrections
   const tChar Slash = '/'; // For compatibality with Linux - Backslash replaced with front slash


   temp = (tChar*)strchr(ErrorFilePath, Slash);
   if(NULL==temp)
   {
      return ErrorFilePath;
   }


   while( NULL != temp )
   {
      ErrorFileName = temp;
      lintTemp = strchr( ++temp, Slash ); // added for lint errors corrections
      temp = lintTemp;
   }

   return ++ErrorFileName;
}

/******************************************************************************
*   FUNCTION:      bInitialize
*------------------------------------------------------------------------------
*   DESCRIPTION:   Intialize the object.
*------------------------------------------------------------------------------
*   PARAMETERS:    None
*------------------------------------------------------------------------------
*   RETURN TYPE:   True if successful otherwise false
*------------------------------------------------------------------------------
*   HISTORY:
*------------------------------------------------------------------------------
*  Date      |   Modification               | Author
*------------|------------------------------|----------------------------------
*  13.06.08  |   Initial revision           | Kailash Tandel
*         
******************************************************************************/
tBool OSAL_tclErrorLogger::bInitialize()
{
   if(m_bInitialized == FALSE)
   {
      tU8 aszLogFileName[EL_REG_BUFFER_SIZE] = {0};
      RegErrorLevel = 8;

      tU32 dwBufSize = EL_REG_BUFFER_SIZE;
      tU32 dwRELSize = sizeof(tU32);

      char tmp[512];
      char *path = getcwd(tmp, 512);
      if (path != NULL)
      {
         std::string oTmp = path;
         oTmp += "/";
         oTmp += DEFAULTLOGFILENAME;
         strcpy ((char *) aszLogFileName, oTmp.c_str());
      }
      else
      {
         strcpy ((char *) aszLogFileName, DEFAULTLOGFILENAME);
      }
      hLF = fopen((tPCChar)aszLogFileName, "w+");
      if (NULL != hLF)  
      {
         OSAL_trTimeDate rSysTime;
         OSAL_s32ClockGetTime(&rSysTime);

         tChar aszSysTime[ EL_REG_BUFFER_SIZE ] = { 0 };
         sprintf( ( tChar * )aszSysTime,//142 - 138 = 4
            "Log Creation Time (dd/mm/yy): %d/%d/%d @ %d:%d:%d\n", 
            rSysTime.s32Day,    rSysTime.s32Month, 
            rSysTime.s32Year,   rSysTime.s32Hour,
            rSysTime.s32Minute, rSysTime.s32Second);

         fwrite(aszSysTime, sizeof(tChar), strlen(aszSysTime), hLF);
         fflush(hLF);

         oErrLogSem.s32Create("ErrLogSem",1);
         m_bInitialized = TRUE;
      }
      else
      {
         printf("ERROR: Could not open error-log for writing (\"%s\")\n", aszLogFileName);
      }
   }
   return m_bInitialized;
}

/******************************************************************************
*   FUNCTION:      bUninitialize
*------------------------------------------------------------------------------
*   DESCRIPTION:   Unintialize the object.
*------------------------------------------------------------------------------
*   PARAMETERS:    None
*------------------------------------------------------------------------------
*   RETURN TYPE:   True if successful otherwise false
*------------------------------------------------------------------------------
*   HISTORY:
*------------------------------------------------------------------------------
*  Date      |   Modification               | Author
*------------|------------------------------|----------------------------------
*  13.06.08  |   Initial revision           | Kailash Tandel
*         
******************************************************************************/
tBool OSAL_tclErrorLogger::bUninitialize()
{
   if(m_bInitialized)
   {
      oErrLogSem.s32Close();
      if( NULL != hLF ) // added for lint corrections
      {
         fclose( hLF );
      }
      m_bInitialized = FALSE;
   }
   return TRUE;
}

/******************************************************************************
*   FUNCTION:      vLog
*------------------------------------------------------------------------------
*   DESCRIPTION:   Overridden method of IELog interface
*------------------------------------------------------------------------------
*   PARAMETERS:    None
*------------------------------------------------------------------------------
*   RETURN TYPE:   None
*------------------------------------------------------------------------------
*   HISTORY:
*------------------------------------------------------------------------------
*  Date      |   Modification               | Author
*------------|------------------------------|----------------------------------
*  13.06.08  |   Initial revision           | Soj Thomas
******************************************************************************/
//lint -e{40,10,1055}
tVoid OSAL_tclErrorLogger::vLog( tPCChar error_file, tInt error_line, ten_ErrorLevel level, tCString coszMessage, ... )
{
   OSAL_tVarArgList argList; 
   OSAL_VarArgStart(argList, coszMessage);
   OSAL_LogErrorInt( error_file, error_line, (OSAL_ERRORLEVEL)level, coszMessage, argList );
}
