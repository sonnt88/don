/******************************************************************************
* FILE:          pure_osal_errorlogger.h
*
* PROJECT:       OSAL
*
* SW-COMPONENT:  OSAL
*-----------------------------------------------------------------------------
* DESCRIPTION:   This is the headerfile for the implementation of the class
*                OSAL_tclErrorLogger.
*-----------------------------------------------------------------------------
* COPYRIGHT:    (c) 2006 RBIN / EDI2
* HISTORY:      
*-----------------------------------------------------------------------------
* Date      | Author              | Modification
*-----------------------------------------------------------------------------
* 02.01.06  | Ananya Muddukrishna | Initial version 
*****************************************************************************/


/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#ifndef __OSAL_ERRORLOGGER_H__
#define __OSAL_ERRORLOGGER_H__

#define OSAL_S_IMPORT_INTERFACE_TYPES
#include "osal_if.h"

#include "errorlog_if.h"

/******************************************************************************
class description:   OSAL_tclErrorLogger is a class to allow logging of error 
messages primarily for testing and debugging purposes.
All messages are written to a log file whose path is defined
in the registry. All messages whose error levels are 
numerically greater than the set error level in the registry 
are not logged.

There is also an option to add filename and line number
while logging. Use ADDINFO or ADDNOINFO.

Messages can be either formatted ASCII strings or 
WCHAR strings

Log File is an ASCII file.

NOTE: OSAL_tclErrorLogger has native implementation in CPP
*****************************************************************************/


/******************************************************************************
| defines (scope: global)
|----------------------------------------------------------------------------*/

//Name of global dll errorlogger object
#define ELOBJECTNAME pel

//Undefine this to disable error logging completely
#define PERMIT_ERROR_LOG

//Macro for adding errorfile and errorline information while logging 
#define ADDINFO __FILE__, __LINE__

//Macro for disabling errorfilename and errorline information while logging
#define ADDNOINFO NULL, 0

//A convenient name substitution
#define ELog ELOBJECTNAME->OSAL_LogError

/******************************************************************************
| constants (scope: global)
|----------------------------------------------------------------------------*/
tPCChar const       DEFAULTLOGFILENAME   = "/var/opt/bosch/dynamic/osal_log.txt";

const tInt          EL_REG_BUFFER_SIZE   = 255;
tPCChar const       ERRORLEVELNAME[5]    = {
    "E_FATAL", 
    "E_ERROR", 
    "E_WARNING",   
    "E_MESSAGE",   
    "E_DETAILED"
};    

/******************************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------------------*/

#ifdef PERMIT_ERROR_LOG

typedef enum 
{ 
    E_FATAL     = 1,  //Fatal error messages
    E_ERROR     = 2,  //General error messages
    E_WARNING   = 3,  //Warning messages
    E_MESSAGE   = 4,  //General messages
    E_DETAILED  = 5   //All error messages
} 
OSAL_ERRORLEVEL;

#else

typedef enum 
{ 
    E_FATAL     = 0,  //Fatal error messages
    E_ERROR     = 0,  //General error messages
    E_WARNING   = 0,  //Warning messages
    E_MESSAGE   = 0,  //General messages
    E_DETAILED  = 0   //All error messages
} 
OSAL_ERRORLEVEL;

#endif   //~PERMIT_ERROR_LOG

class OSAL_tclGlobalObjectInterface {
protected:
   tBool m_bInitialized;
public:
   OSAL_tclGlobalObjectInterface() : m_bInitialized(FALSE) {}
   virtual tBool bInitialize() = 0;
   virtual tBool bUninitialize() = 0;
};

class OSAL_tclErrorLogger : public OSAL_tclGlobalObjectInterface, public IELog
{
public:
    /******************************************************************************
    *  FUNCTION:      OSAL_tclErrorLogger 
    *------------------------------------------------------------------------------  
    *  DESCRIPTION:   Constructor for the class OSAL_tclErrorLogger obtains the Log
    *                 File Name and Path from the Registry, Creates a default file 
    *                 if there are no Registry Entries
    *------------------------------------------------------------------------------   
    *  PARAMETERS:    NONE
    *------------------------------------------------------------------------------       
    *  RETURN TYPE:   NONE.
    *------------------------------------------------------------------------------
    *  HISTORY:
    *------------------------------------------------------------------------
    *  Date      |   Modification               | Author
    *------------|------------------------------|-----------------------------
    *  02.01.06  |   Initial revision           | Ananya Muddukrishna RBIN\EDI2
    *         
    ******************************************************************************/
    OSAL_tclErrorLogger( );

    /******************************************************************************
    *   FUNCTION:      ~OSAL_tclErrorLogger 
    *------------------------------------------------------------------------------
    *   DESCRIPTION:   Destructor for the class OSAL_tclErrorLogger, Closes the 
    *                  Semaphore and File Handle.
    *------------------------------------------------------------------------------
    *   PARAMETERS:    NONE.
    *------------------------------------------------------------------------------
    *   RETURN TYPE:   NONE.
    *------------------------------------------------------------------------------
    *   HISTORY:
    *------------------------------------------------------------------------------
    *  Date      |   Modification               | Author
    *------------|------------------------------|-----------------------------
    *  02.01.06  |   Initial revision           | Ananya Muddukrishna RBIN\EDI2
    *         
    ******************************************************************************/

    ~OSAL_tclErrorLogger( );

    /******************************************************************************
    *   FUNCTION:      OSAL_LogError [OVERRIDDEN]
    *------------------------------------------------------------------------------
    *   DESCRIPTION:   To log error messages in ASCII string format
    *                 into log file. Variable argument function similar to printf
    *------------------------------------------------------------------------------
    *   PARAMETERS:    const char* ERROR_FILE [I]
    *                        name of the file having this function call
    *                  int ERROR_LINE [I]
    *                        number of the line having this function call
    *                  OSAL_ERRORLEVEL eErrorLevel [I]
    *                       enum variable to take error type
    *                 const char* ErrorString [I]
    *                       formatted error message string
    *                 ... [I]
    *                 variable argument list
    *------------------------------------------------------------------------------
    *   RETURN TYPE:   signed int
    *                 returns 0 on success, -1 on failure
    *
    *------------------------------------------------------------------------
    *  Date      |   Modification               | Author
    *------------|------------------------------|-----------------------------
    *  02.01.06  |   Initial revision           | Ananya Muddukrishna RBIN\EDI2
    *         
    ******************************************************************************/

    tInt OSAL_LogError( tPCChar ERROR_FILE, tInt ERROR_LINE, 
        OSAL_ERRORLEVEL eErrorLevel, 
        tPCChar ErrorString, ... );

private:

    //Error Level (EL) read from WinCE Registry
    tU32 RegErrorLevel;

    //Log File (LF) Handle
    FILE* hLF;

    //Semaphore for LF access control
    OSAL_tclSemaphore oErrLogSem;

    //Method to get errorfile name from errorfile path
    tPCChar OSAL_GetErrorFileName( tPCChar ErrorFilePath );

    // Implement the virtual functions of OSAL_tclGlobalObjectInterface interface
    tBool bInitialize();
    tBool bUninitialize();

    tInt OSAL_LogErrorInt( tPCChar error_file, tInt error_line, 
                           OSAL_ERRORLEVEL eErrorLevel, 
                           tPCChar ErrorString, OSAL_tVarArgList VArgList );
public:
   tVoid vLog( tPCChar error_file, tInt error_line,ten_ErrorLevel level, tCString coszMessage, ... );
};


#endif //~__OSAL_PURE_ERRORLOGGER_H__

