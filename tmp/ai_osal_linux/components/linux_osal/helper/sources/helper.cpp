/*****************************************************************************
| FILE:         osalcallback.c
| PROJECT:      Platform
| SW-COMPONENT: OSAL
|-----------------------------------------------------------------------------
| DESCRIPTION:  This is the implementation file for the OSAL
|               (Operating System Abstraction Layer) IOSC Connection-Functions.
|
|-----------------------------------------------------------------------------
| COPYRIGHT:    (c) 2010 Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 03.10.05  | Initial revision           | MRK2HI
| 28.02.13  | Fixed lint warnings.       | mdh4cob
| 11.11.13  | Adding new CallBack for u32|
|           | parameter as the Argument  | SWM2KOR 
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

/************************************************************************ 
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/
#include <execinfo.h>
#include <string.h>
#include "errno.h"
#include "fcntl.h"
#include "sys/types.h"
#include "sys/time.h"
#include "sys/timeb.h"
#include "sys/stat.h"
#include "stdlib.h"
#include "unistd.h"
#include "dirent.h"
#include "stdio.h"
#include "sys/mman.h"
#include "sys/statvfs.h"
#include "sys/ioctl.h"
#include "sys/resource.h"
#include <sys/syscall.h>
#include <sys/reboot.h>

#include "OsalConf.h"

#define TRACE_S_IMPORT_INTERFACE_TYPES
#include "trace_if.h"

#define EXCEPTION_HANDLER_REBOOT_FILE "/opt/bosch/disable_reset.txt"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************ 
|defines and macros (scope: module-local) 
|-----------------------------------------------------------------------*/

#define MIN(x,y) ((x)<(y)?(x):(y))

// #define ASSERT_BUFFER_SIZE    MAX_TRACE_SIZE // errmmem buffer is smaller!

// ERRMEM_MAX_ENTRY_LENGTH (=128) - header for trace
#define ASSERT_BUFFER_SIZE    200

// Line 1
#define ASSERT_BUFFER_MODE_LINE1_SIZE  (0x03)
#define ASSERT_BUFFER_EXPR_LINE1_SIZE  (ASSERT_BUFFER_SIZE - ASSERT_BUFFER_MODE_LINE1_SIZE - 1)

// Line 2
#define ASSERT_PROC_NAME_SIZE   16
#define ASSERT_THREAD_NAME_SIZE 16
#define ASSERT_LINE_NUMBER_SIZE  4
#define ASSERT_BUFFER_EXPR_LINE2_SIZE (ASSERT_BUFFER_SIZE - 1 - ASSERT_PROC_NAME_SIZE - \
                                       ASSERT_THREAD_NAME_SIZE - ASSERT_LINE_NUMBER_SIZE - 1)


#define ASSERT_INITIATOR_OSAL       0x00

#define MAX_ENTRY_LENGTH        256


#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
#define ERRMEM_FILE "/var/opt/bosch/dynamic/errmem"
#else
#define ERRMEM_FILE "/dev/errmem"
#endif


#define INSERT_T32(buf, Long) \
    (buf)[0] = (unsigned char) (((Long) >> 24) & 0xFF); \
    (buf)[1] = (unsigned char) (((Long) >> 16) & 0xFF); \
    (buf)[2] = (unsigned char) (((Long) >>  8) & 0xFF); \
    (buf)[3] = (unsigned char) (((Long)      ) & 0xFF);


/************************************************************************ 
|typedefs (scope: module-local) 
|-----------------------------------------------------------------------*/

/************************************************************************
| variable definition (scope: module-local)
|-----------------------------------------------------------------------*/

/************************************************************************ 
| variable definition (scope: global)
|-----------------------------------------------------------------------*/
static char vAssert_buf[ ASSERT_BUFFER_SIZE ];
static int fderrmem = -1;

/************************************************************************
|function prototype (scope: module-local)
|-----------------------------------------------------------------------*/
#undef EXC_ARM
#ifdef EXC_ARM

static int check_reboot(void)
{
   static int should_be_rebooted_flag = 0xdead;

   if(should_be_rebooted_flag == 0xdead)
   {
      struct stat buf;
      should_be_rebooted_flag =
      stat(EXCEPTION_HANDLER_REBOOT_FILE, &buf) != 0;	
   }
   return should_be_rebooted_flag;
}
#endif

void  intern_reboot(void)
{
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
   sync();   /* be sure that everything is on disk */
#endif

#ifdef EXC_ARM
  if(check_reboot()) 
  {
     fprintf(stderr, "rebooting (to prevent: touch %s\n", EXCEPTION_HANDLER_REBOOT_FILE);
     reboot(RB_AUTOBOOT);
  } 
  else
  {
     const char msg[] = "reboot disabled due " EXCEPTION_HANDLER_REBOOT_FILE "\n";
     write_fd(fderrmem, msg, strlen(msg));
  }
#endif
}

int WaitMillSeconds(unsigned int msec)
{
  int Ret = 0;
  struct timespec tim = {0,0};
  struct timespec remain;
  if(!clock_gettime(CLOCK_REALTIME, &tim))
  {
     while(1)
     {
        tim.tv_sec = msec / 1000 ;
        tim.tv_nsec = (msec%1000)*1000000;
        if(tim.tv_nsec >= 1000000000)
        {
           tim.tv_sec += 1;
           tim.tv_nsec = tim.tv_nsec - 1000000000;
        }
        Ret = nanosleep(&tim, &remain);
        if(Ret == 0)
        {
           break;
        }
        else if(errno != EINTR)
        {
           break;
        }
        /* We are here because of signal. lets try again. */
     }
  }
  return Ret;
}


/*****************************************************************************
*
* FUNCTION:    vConsoleTrace
*
* DESCRIPTION: This function is the trace to putty console implementation
*
* PARAMETER: StdStream: STDOUT_FILENO	1	Standard output.  
*                       STDERR_FILENO	2	Standard error output. 
*
* RETURNVALUE:   None
*
* HISTORY:
* Date      |   Modification                         | Authors
* 21.02.14  | Initial revision                       | MRK2HI
* 02.03.15  | add parameter PiStdStream              | bueter
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vConsoleTrace(int PiStdStream, const void* pvData, unsigned int Length)
{
	if((PiStdStream==STDOUT_FILENO)||(PiStdStream==STDERR_FILENO))
	{
      write(PiStdStream, pvData,Length);
      write(PiStdStream, "\n",1);
	}
}

/*****************************************************************************
*
* FUNCTION:    WriteErrMemEntry
*
* DESCRIPTION: This function is the osal free error memory write operation
*
* PARAMETER:     int          Trace Class
*                const char*  Data buffer
*                int          length of data buffer
*                unsigned int Trace Type
*
* RETURNVALUE:   int error code
*
* HISTORY:
* Date      |   Modification                         | Authors
* 03.09.11  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
int WriteErrMemEntry(int trClass, const char* pBuffer,int Len,unsigned int TrcType/*OSAL_STRING_OUT for string*/)  
{
  int Ret = -1;
  char cOutPut[MAX_ENTRY_LENGTH];
  char Buffer[100];
  ((void)TrcType);
  cOutPut[0] = 0;
  cOutPut[1] = (char) (trClass & 0xFF);
  cOutPut[2] = (char) (trClass >> 8) & 0xFF;

  if(fderrmem == -1)
  {  
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
     fderrmem = open(ERRMEM_FILE, O_RDWR | O_CREAT | O_APPEND,0);
#else
     fderrmem = open(ERRMEM_FILE, O_WRONLY);
#endif
  }

  if(fderrmem != -1)
  {
     if (Len > MAX_ENTRY_LENGTH - 3)Len = MAX_ENTRY_LENGTH-3;

     memcpy(&cOutPut[3], pBuffer,(unsigned int)Len);
     if ((Ret = write(fderrmem, cOutPut,Len+3)) != (Len+3))
     {
        Ret = errno;
        snprintf(Buffer,100,"Write ErrMem Error %d",Ret);
        vConsoleTrace(STDOUT_FILENO,Buffer, strlen(Buffer) );
     }
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
     close(fderrmem);
     fderrmem = -1;
#endif
  }
  else
  {
     snprintf(Buffer,100,"Open ErrMem Error %d",Ret);
     vConsoleTrace(STDOUT_FILENO,Buffer, strlen(Buffer) );
  }
  return errno;
}

/*****************************************************************************
*
* FUNCTION:    trace_callstack
*
* DESCRIPTION: This function is the callbackhandler task for initial process
*
* PARAMETER:     tBool  flag for writing to errmem
*
* RETURNVALUE:   none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 21.02.14  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void trace_callstack(char bWriteCallstackToErrmem)
{
   #define MAX_DEEP 10

   int  i, CallStackDeep;
   void *buffer[MAX_DEEP];
   char **strings;

   CallStackDeep = backtrace( buffer, MAX_DEEP );
   strings       = backtrace_symbols( buffer, CallStackDeep );

   if( strings == NULL )
   {
      vAssert_buf[0] = 0x07;
      sprintf((char*)(vAssert_buf+1), "backtrace error");
      vConsoleTrace(STDOUT_FILENO,vAssert_buf, strlen(vAssert_buf) );
      if( bWriteCallstackToErrmem)
      {
         WriteErrMemEntry( TR_CLASS_ASSERT, vAssert_buf, strlen(vAssert_buf), 0 );
      }
   }
   else
   {
      for( i = 1; i < CallStackDeep; i++ ) // 1: we don't want to see the function call of OSAL_trace_callstack()
      {
         memset( vAssert_buf, 0, ASSERT_BUFFER_SIZE );
         vAssert_buf[0] = 0x07;

         strncpy( vAssert_buf+1, strings[i], ASSERT_BUFFER_SIZE-2 );
         vAssert_buf[ ASSERT_BUFFER_SIZE-1 ] = 0;
         vConsoleTrace(STDOUT_FILENO,vAssert_buf, strlen(vAssert_buf) );
         if( bWriteCallstackToErrmem)
         {
            WriteErrMemEntry( TR_CLASS_ASSERT, vAssert_buf, strlen(vAssert_buf), 0 );
         }
      }
   }
   free(strings);
}

/*****************************************************************************
*
* FUNCTION:     vAssert_Trace
*
* DESCRIPTION: This function is the callbackhandler task for initial process
*
* PARAMETER:   const char 
*              const char 
*              unsigned int
*              char
*
* RETURNVALUE: none
*
* HISTORY:
* Date      |   Modification                         | Authors
* 21.02.14  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
static void vAssert_Trace(const char *expr, const char *file, unsigned int line,  char initiator)
{
  unsigned int buflen = 0;
  unsigned int exprlen = strlen(expr);

  /* if assert is followed by reset */
  
  /* first line: ASSERT OSAL mode=NORMAL  behave=TASK_NOACTION, expr=[ALWAYS] */
  memset(vAssert_buf, 0, ASSERT_BUFFER_SIZE);
  vAssert_buf[0] = initiator;
  vAssert_buf[1] = (char)((line & 0x80000000) != 0);
  // vAssert_buf[2] = (char)pOsalData->eAssertMode;
  vAssert_buf[2] = 0; // use assert mode coresponding to the line (high bit) coding and debug/release mode
  buflen += ASSERT_BUFFER_MODE_LINE1_SIZE;
  
  strncpy( &vAssert_buf[buflen], expr, MIN(ASSERT_BUFFER_EXPR_LINE1_SIZE, exprlen) );
  buflen += ASSERT_BUFFER_EXPR_LINE1_SIZE;
  
  vAssert_buf[ASSERT_BUFFER_SIZE - 1] = 0;

  vConsoleTrace(STDOUT_FILENO,vAssert_buf, buflen );
  WriteErrMemEntry( TR_CLASS_ASSERT, vAssert_buf, buflen, 0 );

  /* second line: ASSERT Line=302, File=[x:/di_tengine_os/products/TEngine_driver/spm/tengi.... */
  memset(vAssert_buf, 0, ASSERT_BUFFER_SIZE);
  vAssert_buf[0] = 0x06; // 6 instead of 5 for proc ans task
  buflen = 1;

  // process name  16 Bytes
  (void) snprintf( &vAssert_buf[buflen], ASSERT_PROC_NAME_SIZE, "PID:%d", getpid() );
 // (void) snprintf( &vAssert_buf[buflen], ASSERT_PROC_NAME_SIZE, "%s", gpOsalProcDat->pu8AppName );
  buflen += ASSERT_PROC_NAME_SIZE;

  int tid = syscall(SYS_gettid);
  // task name  16 Bytes
  // if( bGetThreadNameForTID(&vAssert_buf[buflen], ASSERT_THREAD_NAME_SIZE, tid ) == FALSE )
   {
      (void) snprintf(&vAssert_buf[buflen], ASSERT_THREAD_NAME_SIZE, "TID=%d", tid);
   }   
  buflen += ASSERT_THREAD_NAME_SIZE;

  INSERT_T32(&vAssert_buf[buflen], (line & (~0x80000000)));
  buflen  += ASSERT_LINE_NUMBER_SIZE;


  exprlen = strlen(file);
  if( exprlen >= ASSERT_BUFFER_EXPR_LINE2_SIZE )
  {
      file += exprlen - ASSERT_BUFFER_EXPR_LINE2_SIZE + 1;
  }
  strncpy( &vAssert_buf[buflen], file, MIN(ASSERT_BUFFER_EXPR_LINE2_SIZE - 1, exprlen) );
  buflen += ASSERT_BUFFER_EXPR_LINE2_SIZE;
  
  vConsoleTrace(STDOUT_FILENO,vAssert_buf, buflen);
  WriteErrMemEntry( TR_CLASS_ASSERT, vAssert_buf, buflen, 0);

  // if  (0 == (line & 0x80000000))
  {
      /* bWriteCallstackToErrmem = TRUE */
      /* The callstacks should always be writtet to error memory */
      /* Independently if the ASSERT is a NORMAL or FATAL */
      trace_callstack(1);
  }
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
  sync();
#endif
}



/*****************************************************************************
*
* FUNCTION:    OSAL_vAssertFunction
*
* DESCRIPTION: This function is for Assertion handling
*
* PARAMETER:   const char *expr, 
*              const char *file, 
*              tU32 line, 
*
* RETURNVALUE: none.
* HISTORY:
* Date      |   Modification                         | Authors
* 21.02.14  | Initial revision                       | MRK2HI
* --.--.--  | ----------------                       | -----
*
*****************************************************************************/
void vAssertFunction(const char* expr, const char* file, unsigned int line)
{
   // Assemble trace message
   vAssert_Trace(expr, file, line, ASSERT_INITIATOR_OSAL);

   if((line & 0x80000000) == 0)
   {
       vConsoleTrace(STDOUT_FILENO,"RESET via Assert!!!",19);
       WaitMillSeconds(1000);
       intern_reboot();
   }
}

#ifdef __cplusplus
}
#endif
/************************************************************************
|end of file osalansi.c
|-----------------------------------------------------------------------*/

