/************************************************************************
| FILE:         helper.h
| PROJECT:      platform
| SW-COMPONENT: 
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for helper library definitions
|
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2014  Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
* 21.02.14  | Initial revision           | MRK2HI
| --.--.--  | ----------------           | -------, -----

|************************************************************************/
#if !defined (HELPER_HEADER)
   #define HELPER_HEADER

/************************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************
| feature configuration
| (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
  // Handling of error equal in Debug and Release 
     #define FATAL_M_NATIVE_ASSERT(_INT_)  ((_INT_) ? (void)(0) : (vAssertFunction(_INT_, __FILE__, (__LINE__)&(~0x80000000))) )
     // Use it for NORMAL asserts, which should suspend task or system
     // in debug version but resuming task operation in release version
     #define NORMAL_M_NATIVE_ASSERT(_INT_) ((_INT_) ? (void)(0) : (vAssertFunction(_INT_, __FILE__, (__LINE__)|0x80000000)) )

     // Use it for FATAL asserts, which should occur as an obligation
     #define FATAL_M_NATIVE_ASSERT_ALWAYS() (vAssertFunction("ALWAYS", __FILE__, (__LINE__)&(~0x80000000)))

     // Use it for NORMAL asserts, which should occur as an obligation
     #define NORMAL_M_NATIVE_ASSERT_ALWAYS() (vAssertFunction("ALWAYS", __FILE__, (__LINE__)|0x80000000))

/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/


/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/

/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
void vAssertFunction(const char* expr, const char* file, unsigned int line);

int WriteErrMemEntry(int trClass, const char* pBuffer,int Len,unsigned int TrcType);

void vConsoleTrace(int PiStdStream, const void* pvData, unsigned int Length);


#ifdef __cplusplus
}
#endif

#else
#error helper.h included several times
#endif
