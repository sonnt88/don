/******************************************************************************
 *FILE         : Adr3_standaloneapp.c
 *
 *SW-COMPONENT :  
 *
 *DESCRIPTION  : Reads out ADR3 Flash memory from address range ( 0x00000000 to  0x00400000)
 *               with 512 bytes at one time.
 *               
 *AUTHOR       : Amit Bhardwaj (RBEI/ECF5)
 *
 *COPYRIGHT    : (c) 2012, Robert Bosch Engineering and Business Solutions India Limited.
 *
 *HISTORY      : 23-12-2014 . Initial Version
 *
 *****************************************************************************/
 
 
/*****************************************************************
* Helper Defines and Macros (scope: module-local)
*******************************************************************/
#define SPM_EV_SHUTDOWN_NAME  "FLASH_READ_APP_SHUTDOWN"
#define OEDT_FFS3_WRITEFILE1               "/dev/ffs3/ADR3_Flashmem.bin"
#define ADR3_READFLASHMEM_EVENTNAME        "Adr3_READFLASHMEM_EV"
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#define BUFF_LENGTH         516
#define BYTESREADSIZE      (4*1024*1024)
#define MESSAGE_LENGTH      12
#define MAX_BYTES_READ      512
#define READING_FINE        0x41
#define FLASHER_FILE       "./xl_flasher662.bin"

#include "osal_if.h"
#include "oedt_Types.h"
#include "oedt_ADR3Ctrl_TestFuncs.h"
#include "dev_adr3ctrl.h"

/*****************************************************************
* variable definition (scope: global)
******************************************************************/

OSAL_tEventHandle CallBackEventHndlr = OSAL_C_INVALID_HANDLE;
OSAL_tMSecond Starttime = 0;
tBool bEventPostMechansmActive = FALSE;
static int writecnt = 0;  /* To show the progress while reading ADR Flash memory */
OSAL_tEventHandle hEvShutdown = 0;
OSAL_tEventMask    hevRequest  = 0x00000001;


/*Function Definitions*/
/***************************************************************************************
* FUNCTION    :  u32ADR3StateWait
*
* PARAMETER   :  
*                  OSAL_tEventHandle EventHndlr
*                  tU32 dwEventMask
*                  tU32 u32LineNumbr 
* RETURNVALUE :    tU32, valid event  on success  or Invalid event value in case of error
*
* DESCRIPTION :  Wait for the EventHndlr and send the event received 
*               
*
* HISTORY     :   Created by Amit Bhardwaj(RBEI/ECF5)  AHM5KOR   :  23-12-2014
*
*******************************************************************************************/
static tU32 u32ADR3StateWait(OSAL_tEventHandle EventHndlr,tU32 dwEventMask,OSAL_tenEventMaskFlag fgMaskoption,tU32 u32LineNumbr)
{
   tU32 hEvRequest ;
   if(((OSAL_tEventHandle)OSAL_C_INVALID_HANDLE != EventHndlr) && (0 != dwEventMask))
   {
      if(OSAL_OK == OSAL_s32EventWait(EventHndlr,
                                       dwEventMask, 
                                       fgMaskoption, 
                                       ADR3CTRL_RESPONSE_WAIT_TIME, 
                                       &hEvRequest
                                      )
         )
      {
         /* Clear the Event */
         if(OSAL_OK != OSAL_s32EventPost(EventHndlr, 
                                           ~hEvRequest, 
                                           OSAL_EN_EVENTMASK_AND 
                                          ) 
            )
         {
            printf(" Line Number is %lu",u32LineNumbr);
            hEvRequest = 134217735;
         } 
         
      }
      else
      {
         hEvRequest = 67108877;
      }
   }
   else
   {
      hEvRequest = 33554449;
   }
   
   return (hEvRequest);
}


/***************************************************************************************
* FUNCTION    :  vAdr3ctrlCallback
*
* PARAMETER   :  
*                  tU32 u32State
*
* RETURNVALUE :    tVoid
*
* DESCRIPTION :  CallBack execution for ADR3 state change
*                Post the event for the received State from Dev_ADR3
*               
*
* HISTORY     :   Created by Amit Bhardwaj (RBEI/ECF5)  23-12-2014
*
*******************************************************************************************/
static tVoid vAdr3ctrlCallback(tU32 u32State)
{
   tU32  tU32EventCommand = ADR3CALLBACK_INVALIDSTATE;
   if(u32State == (tU32)ADR3CTRL_ADR3_STATE_ALIVE)
   {
      /* event post ADR3 ALIVE state */
      tU32EventCommand = ADR3CALLBACK_ALIVESTATE; 
   }
   else if(u32State == (tU32)ADR3CTRL_ADR3_STATE_DEAD)
   {
      /* event post ADR3 DEAD state */
      tU32EventCommand = ADR3CALLBACK_DEADSTATE;
   }
   else if(u32State == (tU32)ADR3CTRL_ADR3_STATE_DNL)
   {
      /* event post ADR3 DNL state */
      tU32EventCommand = ADR3CALLBACK_DNLSTATE ;
   }
   else if(u32State == (tU32)ADR3CTRL_ADR3_STATE_INIT)
   {
      /* event post ADR3 INIT state */
      tU32EventCommand = ADR3CALLBACK_INITSTATE ;
   }
   else
   {
     
   }
   if(bEventPostMechansmActive == TRUE)
   {
      if(OSAL_C_INVALID_HANDLE == CallBackEventHndlr)  
      {
         
      }
      else if(OSAL_OK != OSAL_s32EventPost(CallBackEventHndlr, 
                                           tU32EventCommand, 
                                           OSAL_EN_EVENTMASK_OR 
                                          ) 
              )
      {
          
      }     
   }
}

/***************************************************************************************
* FUNCTION    :  u32ADR3CtrlIOCntrlWait
*
* PARAMETER   :  
*                  OSAL_tIODescriptor hDevHandle
                   tU32 u32IOCONTROL
*                  tU32 u32ADR3State
* RETURNVALUE :    tVoid
*
* DESCRIPTION :  CallBack execution for ADR3 state change
*                Post the event for the received State from Dev_ADR3
*               
*
* HISTORY     :  Created by Amit Bhardwaj (RBEI/ECF5)  23-12-2014
*
*******************************************************************************************/
tU32 u32ADR3CtrlIOCntrlWait(OSAL_tIODescriptor hDevHandle,tU32 u32IOCONTROL,tU32 u32ADR3State)
{  
   tU32   u32RetVal = 0;
   tU32 hEvRequest;
   tU32 dwEventMask ;
   if(OSAL_OK 
      == 
      OSAL_s32IOControl(hDevHandle,
                        (tS32)u32IOCONTROL,
                        (tS32)NULL 
                        )
     )
   {
      if(ADR3CTRL_NORMALMODE == u32ADR3State)
      {            
         dwEventMask = ADR3CALLBACK_ALIVESTATE | ADR3CALLBACK_DEADSTATE;
      }
      else
      {
         dwEventMask = ADR3CALLBACK_DNLSTATE | ADR3CALLBACK_DEADSTATE;
      }

      /* Wait for callback from ADR3*/
      hEvRequest = u32ADR3StateWait(CallBackEventHndlr,
                                    dwEventMask,
                                    OSAL_EN_EVENTMASK_AND,
                                    __LINE__
                                    );
      if(hEvRequest >= ADR3CTRL_STATEWAIT_MIN_ERROR)
      {
         /* Adding the wait error value with return Value*/
         u32RetVal += hEvRequest;
      }
      else if(dwEventMask & hEvRequest) 
      {
         // Expected behaviour
      
      // Wait for 2 seconds as the ADR3 takes 1.5 seconds to come up after reset
         OSAL_s32ThreadWait(2500);
      }
      else
      {  
         u32RetVal += 16777223;
      }
   }  
   else
   {
      
      u32RetVal += 8388621;
   }
   return u32RetVal;
}


/***********************************************************************************
* FUNCTION    :  u32EventClose_Delete
*
* PARAMETER   :  
*                  tCString EventName
*                  OSAL_tEventHandle EventHndlr
*                  tU32 u32LineNumbr 
* RETURNVALUE :    tU32, "OSAL_OK" on success  or "OSAL_ERROR" value in case of error
*
* DESCRIPTION :  Close the event and delete the event
*               
*
* HISTORY     :   Created by AMIT BHARDWAJ (RBEI/ECF5)  23-12-2014
*
*************************************************************************************/
static tS32 u32EventClose_Delete(tCString EventName,
                                 OSAL_tEventHandle *EventHndlr,
                                 tU32 u32LineNumbr
                                )
{
   tS32 s32Retval = OSAL_OK;
   if((EventName != NULL) && (*EventHndlr != OSAL_C_INVALID_HANDLE))
   {
      if(OSAL_OK != OSAL_s32EventClose(*EventHndlr))
      {
      
         s32Retval = OSAL_ERROR;
      }
      else if(OSAL_OK != OSAL_s32EventDelete(EventName ))
      {
         *EventHndlr = OSAL_C_INVALID_HANDLE;
         s32Retval = OSAL_ERROR;
      }
      else
      {
         /** Event closed and Deleted  sucessfully*/
         *EventHndlr = OSAL_C_INVALID_HANDLE;
      }
   }
   else
   {
      printf(" Line Number - %lu \n",u32LineNumbr);
      s32Retval = OSAL_ERROR;
   }
   return (s32Retval);
}

/***********************************************************************************
* FUNCTION    :  u32ADR3Ctrlopen_RgstrtCallBack
*
* PARAMETER   :  
*                  OSAL_tIODescriptor hDevHandle
* RETURNVALUE :    tU32,"OSAL_OK" on success  or "OSAL_ERROR" value in case of error
*
* DESCRIPTION :   Open ADR3CTRL and Register for CallBack
*               
*
* HISTORY     :   Created by Amit Bhardwaj (RBEI/ECF5)  23-12-2014
*
*************************************************************************************/
tU32 u32ADR3Ctrlopen_RgstrtCallBack(OSAL_tIODescriptor *hpDevHandle)
{
   tU32  u32RetVal = 0;
   /** ADR3CTRL Driver Open*/
   *hpDevHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_ADR3CTRL,OSAL_EN_READWRITE);
   if((*hpDevHandle) != OSAL_ERROR)
   {
      if(OSAL_OK 
         != 
         OSAL_s32IOControl(*hpDevHandle,
                           OSAL_C_S32_IOCTRL_ADR3CTRL_REGISTER_RESET_CALLBACK,
                           (tS32)vAdr3ctrlCallback 
                          )
        )
      {
         u32RetVal += 4096;
      }
   }
   else
   {
      u32RetVal += 8192;
   }
   return u32RetVal;
}

/***********************************************************************************
* FUNCTION    :  u32CheckSumModCalc
*
* PARAMETER   :  const tU8  *pcu8Buffer, tU16 u16Len
*       
* RETURNVALUE :  tU32
*
* DESCRIPTION :  Function to Calculate checksum %256.
*               
*
* HISTORY     :  Created by Amit Bhardwaj
*
*************************************************************************************/

tU32 u32CheckSumModCalc(const tU8  *pcu8Buffer, tU16 u16Len)
{
   tU32 u32CheckSum = 0;
   tS32 s32Count;

   for( s32Count = 0; s32Count < u16Len; s32Count++)
   {
      u32CheckSum += pcu8Buffer[ s32Count];
   }
   return (tU8)u32CheckSum % 256;
}

/***********************************************************************************
* FUNCTION    :  vReadFlashmem
*
* PARAMETER   :  
*       
* RETURNVALUE :  tU32, "OSAL_OK" on success  or "OSAL_ERROR" value in case of error
*
* DESCRIPTION :  Read out ADR3 Flash Memory.
*               
*
* HISTORY     :  Created by Amit Bhardwaj
*
*************************************************************************************/

tU32 vReadFlashmem(OSAL_tIODescriptor hDevHandle ,tU32 u32Readdatasize)
{
   tU32 u32retval = 0;
   OSAL_tMSecond EndTime   = 0;
   OSAL_tMSecond Timetaken = 0;
   OSAL_tIODescriptor hFile;
   tU32 s32Loop;
   tU32 rtCheckSum;
   tU8  commandrecieved[BUFF_LENGTH+4]={0};
   tU32 readdatacount;
   tU8  readflashCommand[] = { 0x00,0x0c,0x48,0x00,0x00,0x00,0x00,0x00,0x00,0x02,0x00,0x00 }; /* Message sent to ADR3 to read 512 bytes of Flash memory .*/
   unsigned int Curraddress = 0x00000000;
   char *ptr = ( char *)&Curraddress;
   
   readdatacount = ( u32Readdatasize/MAX_BYTES_READ );  
   hFile = OSAL_IOCreate( OEDT_FFS3_WRITEFILE1, OSAL_EN_READWRITE );
   if (hFile)
   {
      for (s32Loop=0;s32Loop<readdatacount;s32Loop++)
      {
         if(writecnt % MAX_BYTES_READ == 0)
         {
            fprintf(stderr, ".");
            fflush(stderr);
         }
         writecnt++;
         readflashCommand[3] = ptr[3];
         readflashCommand[4] = ptr[2];
         readflashCommand[5] = ptr[1];
         readflashCommand[6] = ptr[0];
         readflashCommand[MESSAGE_LENGTH - 1] = (tU8)u32CheckSumModCalc(&readflashCommand[0], (MESSAGE_LENGTH-1));

         /* Write the Read message to ADR3 */            
         if ( -1 == OSAL_s32IOWrite(hDevHandle,(signed char*)readflashCommand,12) )
         {
            u32retval += 2;
            printf("Sending Message to Read ADR3 is failed, u32retval - %lu and error code is - %lu \n",u32retval,OSAL_u32ErrorCode());
         }
         /* Read the response of ADR3 */       
         if ( -1 == OSAL_s32IORead(hDevHandle,(signed char *)commandrecieved,BUFF_LENGTH))
         {
            u32retval += 4;
            printf(" Reading from ADR3 is failing \n");
            OSAL_s32IOClose ( hFile );
            return u32retval;
         }
         else
         {
            rtCheckSum = u32CheckSumModCalc(&commandrecieved[0], (BUFF_LENGTH-1));
            if ((rtCheckSum == commandrecieved[BUFF_LENGTH -1]) && (commandrecieved[2] == READING_FINE))
            {
               if(OSAL_ERROR == OSAL_s32IOWrite ( hFile, (signed char*)(&commandrecieved[3]), MAX_BYTES_READ ))
               {
                  u32retval += 8;
                  printf(" Writing Read data to the file is Failed and u32retval is %lu and address location is - %lu \n",u32retval,(s32Loop*MAX_BYTES_READ));
               }
            }
            else
            {
               u32retval += 16;
               printf(" Reading is not fine at address location - %lu \n",(s32Loop*MAX_BYTES_READ));
               printf(" Trying to read ADR3 Flash again...\n");
               OSAL_s32IOClose ( hFile );
               return u32retval;
            }
         }
         Curraddress += 0x00000200;
      }
      if ( OSAL_s32IOClose ( hFile ) == OSAL_ERROR )
      {
        u32retval += 32;
        printf(" File Close is Failed and u32retval - %lu\n",u32retval);
      }
      EndTime = OSAL_ClockGetElapsedTime();
      if( EndTime > Starttime )
      {
         Timetaken =    EndTime - Starttime;	
      }
      printf(" Time taken is - %lu secs.\n",(Timetaken/1000));    
   }
   else
   {
      printf(" File create Failed \n");
      u32retval += 64;
   }
   return u32retval;  
}

/***********************************************************************************
* FUNCTION    :  u32ADR3ReadoutStdaloneApp
*
* PARAMETER   :  tU8 * pu8FileName and tU32 bytesReadSize.
*       
* RETURNVALUE :  tU32, "OSAL_OK" on success  or "OSAL_ERROR" value in case of error
*
* DESCRIPTION :  
*               
*
* HISTORY     :  Created by Amit Bhardwaj
*
*************************************************************************************/

tU32 u32ADR3ReadoutStdaloneApp(const tU8 * pu8FileName , tU32 bytesReadSize)
{
   OSAL_tEventHandle hADR3ReadoutStdApp; 
   OSAL_tIODescriptor hDevHandle ;
   tU32 u32RetVal = 0;
   tU8 *pu8Buff = NULL;
   tU32 u32Len = 0;   
   FILE *fp1= NULL;
   tU32 file_len = 0; 
   tU32 u32Statechange = 0;
   tS32 s32count = 0;
   tS32 flag = 0;
  
   /*Create Event to handle the ADR3 callback Changes*/
   
   if (OSAL_OK != OSAL_s32EventCreate(ADR3_READFLASHMEM_EVENTNAME,&hADR3ReadoutStdApp ))
   {     
      u32RetVal = 1;
   }

   if ((0 == u32RetVal) && (0 == (u32RetVal += u32ADR3Ctrlopen_RgstrtCallBack(&hDevHandle))))
   {
      /* Allow the permission for call Back to send the event */      
      
      bEventPostMechansmActive = TRUE;
      CallBackEventHndlr = hADR3ReadoutStdApp; 

      /* Change the ADR3 state to Download mode using IOControl  and wait till the callback execution*/
      
      u32RetVal = u32ADR3CtrlIOCntrlWait(hDevHandle,OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_SPI, ADR3CTRL_SPIMODE);  
      if ( 0 == u32RetVal)
      {
         if (pu8FileName == NULL)
         {
            u32RetVal += 2;
         }
         else
         {
            fp1 = fopen(pu8FileName,"r"); /*open xl_flasher662.bin*/
            if (fp1 == NULL)
            {
               u32RetVal += 4;
            }
            else
            {
               if (fseek(fp1, 0L, SEEK_END) != 0)
               {
                  u32RetVal += 8;
                  fclose(fp1);
                  fp1 = NULL;
               }
               else
               {
                  file_len = (tU32)ftell(fp1);
               }
            }
         }
         
         if (!u32RetVal && fp1)
         {
            if (fseek(fp1, 0L, SEEK_SET) != 0)
            {
              u32RetVal += 16;
              fclose(fp1);
              fp1 = NULL;
            }

            else if(file_len == 0)
            {
              u32RetVal += 32;
              fclose(fp1);
              fp1 = NULL;
            }
         } 
         if (!u32RetVal && fp1)
         {
            pu8Buff = (tU8 *)malloc(file_len);
            if (pu8Buff == NULL)
            { 
               u32RetVal = +64 ;
               fclose(fp1);
            }
            else
            {
               u32Len = fread(pu8Buff,1,file_len,fp1);
               fclose(fp1);
               if ( u32Len != file_len )
               {
                  u32RetVal += u32ADR3CtrlIOCntrlWait(hDevHandle,OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_NORMAL,ADR3CTRL_NORMALMODE);
                  free(pu8Buff);
               }

               else
               {
                  u32Len = (tU32)OSAL_s32IOWrite(hDevHandle,(signed char*)pu8Buff,file_len);
                  if ( u32Len != file_len)
                  {
                     u32RetVal +=128;
                  }
                  else
                  {
                  }
                  OSAL_s32ThreadWait(50); /*min 50ms delay is required*/
                  free(pu8Buff);
                  Starttime = OSAL_ClockGetElapsedTime();
                  if( 0 == vReadFlashmem(hDevHandle , bytesReadSize))
                  {
                     system("mount -o remount ,rw /media/AAM_dev_sda1");
                     system("cp /var/opt/bosch/dynamic/ffs/ADR3_Flashmem.bin /media/AAM_dev_sda1 ");
                     system("sync");
                     system("mount -o remount ,ro /media/AAM_dev_sda1");
                  }
                  else
                  {
                     do{
                           if ( 0 == vReadFlashmem(hDevHandle , bytesReadSize))
                           {
                              flag = 1;
                              system("mount -o remount ,rw /media/AAM_dev_sda1");
                              system("cp /var/opt/bosch/dynamic/ffs/ADR3_Flashmem.bin /media/AAM_dev_sda1 ");
                              system("sync");
                              system("mount -o remount ,ro /media/AAM_dev_sda1");
                              break;
                           }
                           s32count= s32count + 1;       
                        }while(s32count < 5);
                     if(!flag)
                     {
                     u32RetVal += 256;
                     }
                  }
                  u32Statechange = u32ADR3CtrlIOCntrlWait(hDevHandle,OSAL_C_S32_IOCTRL_ADR3CTRL_SET_BOOTMODE_NORMAL,ADR3CTRL_NORMALMODE);
                  if ( 0 != u32Statechange)
                  {
                     printf( " Normal mode set is failed and u32Statechange - %lu \n",u32Statechange );
                  }
               }            
            }
         }   
      }
      else
      {
         printf("Bootmode set is failed \n");
      }

      /* Remove the permissions from CallBack to send the event*/
      bEventPostMechansmActive = FALSE;
      CallBackEventHndlr = OSAL_C_INVALID_HANDLE;

      /* Close and delete the Event*/
      if (OSAL_OK != u32EventClose_Delete(ADR3_READFLASHMEM_EVENTNAME,&hADR3ReadoutStdApp,__LINE__))
      {
         u32RetVal += 512;
      }

      /* ADR3 close*/
      if (OSAL_s32IOClose(hDevHandle) == OSAL_ERROR)
      {
         u32RetVal += 1024;
      }  

   }
   return(u32RetVal);
}

/***********************************************************************************
* FUNCTION    :  vStartApp   [process entry function]
*
* PARAMETER   :  None
*       
* RETURNVALUE :  tU32, "OSAL_OK" on success  or "OSAL_ERROR" value in case of error
*
* DESCRIPTION :  Generic test function of standalone application.
*               
*

* HISTORY     :  Created by Amit Bhardwaj
*
*************************************************************************************/

tU32 vStartApp(tVoid)
{
      tU32 u32Ret;
      u32Ret = u32ADR3ReadoutStdaloneApp( FLASHER_FILE, BYTESREADSIZE);
      if( 0 == u32Ret)
      {
         printf(" Read is successful \n");
      }
      else
      {
         printf(" Unable to Read the data \n");
      }
      if (OSAL_s32EventCreate(SPM_EV_SHUTDOWN_NAME, &hEvShutdown) == OSAL_ERROR)
      {
         printf("Creation of shutdown event failed");
         u32Ret +=2 ;
      }
      else
      { 
         OSAL_s32EventWait(hEvShutdown, hevRequest, OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, &hevRequest);
         OSAL_s32EventClose(hEvShutdown);
         OSAL_s32EventDelete(SPM_EV_SHUTDOWN_NAME);
      } 
      return u32Ret;
}
