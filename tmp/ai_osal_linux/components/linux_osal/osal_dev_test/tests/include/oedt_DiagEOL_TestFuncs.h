/******************************************************************************
 *FILE         : oedt_DiagEOL_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_DiagEOL_TestFuncs.c for the  
 *               Diag EOL device for the GM-GE hardware.
 *
 *AUTHOR       : Shilpa Bhat (RBIN/EDI3) 
 *
 *HISTORY      : 26 Feb, 2008 ver 1.0  RBIN/ECM1- Shilpa Bhat
*******************************************************************************/

#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	
#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>

#ifndef OEDT_DiagEOL_TESTFUNCS_HEADER
#define OEDT_DiagEOL_TESTFUNCS_HEADER

/* Macro definition used in the code */
#define OSAL_C_S32_IOCTRL_DIAGEOL_INVAL_IOCTL 	-1
#define OSAL_C_STRING_INVALID_ACCESS_MODE       0xffffffff


/* Function prototypes of functions used in file oedt_DiagEOL_TestFuns.c */
tU32 u32DiagEOLDevOpenClose(tVoid);
tU32 u32DiagEOLDevReOpen(tVoid);
tU32 u32DiagEOLDevCloseAlreadyClosed(tVoid);
tU32 u32DiagEOLDevCloseInvalParam(tVoid);
tU32 u32DiagEOLDevOpenCloseDiffAccessMode(tVoid);
tU32 u32DiagEOLDevOpenCloseInvalAccessMode(tVoid);
tU32 u32DiagEOLDisplayInterfaceReadOneByte(tVoid);
tU32 u32DiagEOLDisplayInterfaceReadInvalBuffer(tVoid);
tU32 u32DiagEOLDisplayInterfaceReadWholeBlock(tVoid);
tU32 u32DiagEOLDisplayInterfaceReadBlockInvalidSize(tVoid);
tU32 u32DiagEOLDisplayInterfaceWriteWholeBlock(tVoid);
tU32 u32DiagEOLDisplayInterfaceWriteInvalBuffer(tVoid);
tU32 u32DiagEOLBluetoothReadOneByte(tVoid);
tU32 u32DiagEOLBluetoothReadInvalBuffer(tVoid);
tU32 u32DiagEOLBluetoothReadWholeBlock(tVoid);
tU32 u32DiagEOLBluetoothReadBlockInvalidSize(tVoid);
tU32 u32DiagEOLBluetoothWriteWholeBlock(tVoid);
tU32 u32DiagEOLBluetoothWriteInvalBuffer(tVoid);
tU32 u32DiagEOLNavigationSystemReadOneByte(tVoid);
tU32 u32DiagEOLNavigationSystemReadInvalBuffer(tVoid);
tU32 u32DiagEOLNavigationSystemReadWholeBlock(tVoid);
tU32 u32DiagEOLNavigationSystemReadBlockInvalidSize(tVoid);
tU32 u32DiagEOLNavigationSystemWriteWholeBlock(tVoid);
tU32 u32DiagEOLNavigationSystemWriteInvalBuffer(tVoid);
tU32 u32DiagEOLNavigationIconReadOneByte(tVoid);
tU32 u32DiagEOLNavigationIconReadInvalBuffer(tVoid);
tU32 u32DiagEOLNavigationIconReadWholeBlock(tVoid);
tU32 u32DiagEOLNavigationIconReadBlockInvalidSize(tVoid);
tU32 u32DiagEOLNavigationIconWriteWholeBlock(tVoid);
tU32 u32DiagEOLNavigationIconWriteInvalBuffer(tVoid);
tU32 u32DiagEOLHfTuningReadOneByte(tVoid);
tU32 u32DiagEOLHfTuningReadInvalBuffer(tVoid);
tU32 u32DiagEOLHfTuningReadWholeBlock(tVoid);
tU32 u32DiagEOLHfTuningReadBlockInvalidSize(tVoid);
tU32 u32DiagEOLHfTuningWriteWholeBlock(tVoid);
tU32 u32DiagEOLHfTuningWriteInvalBuffer(tVoid);
tU32 u32DiagEOLSYSTEMReadOneByte(tVoid);
tU32 u32DiagEOLSYSTEMReadInvalBuffer(tVoid);
tU32 u32DiagEOLSYSTEMReadWholeBlock(tVoid);
tU32 u32DiagEOLSYSTEMReadBlockInvalidSize(tVoid);
tU32 u32DiagEOLSYSTEMWriteWholeBlock(tVoid);
tU32 u32DiagEOLSYSTEMWriteInvalBuffer(tVoid);
tU32 u32DiagEOLCOUNTRYReadOneByte(tVoid);
tU32 u32DiagEOLCOUNTRYReadInvalBuffer(tVoid);
tU32 u32DiagEOLCOUNTRYReadWholeBlock(tVoid);
tU32 u32DiagEOLCOUNTRYReadBlockInvalidSize(tVoid);
tU32 u32DiagEOLCOUNTRYWriteWholeBlock(tVoid);
tU32 u32DiagEOLCOUNTRYWriteInvalBuffer(tVoid);
tU32 u32DiagEOLBRANDReadOneByte(tVoid);
tU32 u32DiagEOLBRANDReadInvalBuffer(tVoid);
tU32 u32DiagEOLBRANDReadWholeBlock(tVoid);
tU32 u32DiagEOLBRANDReadBlockInvalidSize(tVoid);
tU32 u32DiagEOLBRANDWriteWholeBlock(tVoid);
tU32 u32DiagEOLBRANDWriteInvalBuffer(tVoid);
tU32 u32DiagEOLRVCReadOneByte(tVoid);
tU32 u32DiagEOLRVCReadInvalBuffer(tVoid);
tU32 u32DiagEOLRVCReadWholeBlock(tVoid);
tU32 u32DiagEOLRVCReadBlockInvalidSize(tVoid);
tU32 u32DiagEOLRVCWriteWholeBlock(tVoid);
tU32 u32DiagEOLRVCWriteInvalBuffer(tVoid);
tU32 u32DiagEOLGetModuleIdentifier(tVoid);
tU32 u32DiagEOLGetModuleIdentifierInvalParam(tVoid);

#endif
