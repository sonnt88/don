/******************************************************************************
 *FILE         : odt_Acoustic_TestFuncs.h
 *
 *SW-COMPONENT : ODT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for the acoustic device.
 *                also the array of function pointer for this device has been 
 *                initialised.
 *
 *AUTHOR       : Bernd Schubart, 3SOFT
 *
 *COPYRIGHT    : (c) 2005 Blaupunkt Werke GmbH
 *
 *HISTORY:       25.07.05  3SOFT-Schubart
 *               Initial Revision.
 *
 *               26.04.07  RBIN/ECM-Soj Thomas
 *               Added function prototypes and includes
 *****************************************************************************/

#ifndef OEDT_ACOUSTICOUT_TESTFUNCS_HEADER
#define OEDT_ACOUSTICOUT_TESTFUNCS_HEADER

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"



/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

#define OSALTEST_PND_USE_NO_AUDIOROUTER

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
// OEDT Functions
tU32 OEDT_ACOUSTICOUT_T000(void);
tU32 OEDT_ACOUSTICOUT_T001(void);
tU32 OEDT_ACOUSTICOUT_T002(void);
tU32 OEDT_ACOUSTICOUT_T003(void);
tU32 OEDT_ACOUSTICOUT_T004(void);
tU32 OEDT_ACOUSTICOUT_T005(void);
tU32 OEDT_ACOUSTICOUT_T006(void);
tU32 OEDT_ACOUSTICOUT_T007(void);
tU32 OEDT_ACOUSTICOUT_T008(void);
tU32 OEDT_ACOUSTICOUT_T009(void);
tU32 OEDT_ACOUSTICOUT_T010(void);
tU32 OEDT_ACOUSTICOUT_T011(void);

tU32 OEDT_ACOUSTICOUT_LONG_T001(void);
tU32 OEDT_ACOUSTICOUT_LONG_T002(void);
tU32 OEDT_ACOUSTICOUT_LONG_T003(void);
tU32 OEDT_ACOUSTICOUT_LONG_T004(void);


tU32 u32AcousticoutEmptyStartStopAbortTest(tVoid);
tU32 u32AcousticoutStartStopAbortTest(tVoid);
tU32 u32AcousticoutAMRMarkerTest(tVoid);
tU32 u32AcousticoutPCMBasicTest(tVoid);
tU32 u32AcousticoutPCMCfgTest(tVoid);
tU32 u32AcousticoutPCMMarkerTest(tVoid);
tU32 u32AcousticoutAMRBasicTest(tVoid);
tU32 u32AcousticoutAMRFormatErrorTest(tVoid);
tU32 u32AcousticoutAMRDataErrorTest(tVoid);
tU32 u32AcousticoutConcatTest(tVoid);
tU32 u32AcousticoutConcurrentTest(tVoid);

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/


#endif

