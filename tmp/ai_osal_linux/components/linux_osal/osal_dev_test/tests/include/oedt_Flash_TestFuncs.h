/******************************************************************************
|FILE         : oedt_Flash_TestFuncs.h
|SW-COMPONENT : OEDT_FrmWrk 
|DESCRIPTION  : This file contains header includes ,function declarations and 
|            	macros for Flash device.
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          | 				          | Author	& comments
| --.--.--      | Initial revision        | -------, -----
| 31 July 2006   |  version 1.0            |  Anoop Krishnan (RBIN/ECM1)
|------------------------------------------------------------------------------
|26 Dec 2006    | Version  1.1 		      | Added and updated test cases			   
|				|                         |Haribabu Sannapaneni(RBIN/ECM1)
| *******************************************************************************
|  23 Jan 2007  | Version 1.2			  | Modified Flash Address
|				|						  |	Kishore Kumar.R (RBIN/ECM1)
|******************************************************************************
| 14 May 2007	|  Version 1.3			  |Added prototypes for new cases and 
|				|						  |some defines that are required for them.
|				|						  |Also updated count for OEDT_FLASH_NUMOFBYTES_PREDEFWRITE 
|				|						  | Rakesh Dhanya (RBIN/EDI3)
|******************************************************************************
| 15 Feb 2008	|  Version 1.4			  	|Added new macro  OSAL_C_STRING_DRAGON
|				|						  	| macro value of INVALID_PARAMETER
|				|							| change from -1 to OSAL_NULL
|******************************************************************************
|03 March 2008 	|    Version 1.5		   	|Added new test case										  	| macro value of INVALID_PARAMETER	  L
|				|							| u32DevFlashReadWrtRU
|				|							| u32DevFlashReadWrtRV
|				|							| u32WriteFlashWithInvalAddr
|				|							| u32ReadFlashWithInvalAddr
|*****************************************************************************
|******************************************************************************
|28 Oct 2008 	|    Version 1.6 |		   	|Added new test case and macros								
|				|							| Anoop Chandran(RBEI/ECM1)
|				|							
|				|							
|*****************************************************************************
|25 Mar 2009 	|    Version 1.7 |		   	|Removed lint and compiler warnings							
|				|							|Sainath Kalpuri (RBEI/ECF1)
|				|							
|				|							
|*****************************************************************************
|14 April 2009 	|    Version 1.8 		   	|relpace the 
|				|				 			|OEDT_FLASH_RU_END_OFFSET with 
|				|				 			|OEDT_FLASH_RAW_END_OFFSET							
|				|				 			|Anoop Chandran (RBEI/ECF1)
|*****************************************************************************
|29  march 2010| version 1.9 	 	        |Anoop Chandran (RBEI/ECF1)
|			   |			 	            |update the flash start offset
|              |                            | for GMGE 
|*****************************************************************************/

#ifndef OEDT_FLASH_TESTFUNCS_HEADER
#define OEDT_FLASH_TESTFUNCS_HEADER

#define OSAL_S_IMPORT_INTERFACE_GENERIC

/******************************************************************
* Header files inclusion                                          *
******************************************************************/

#include "osal_if.h"


/******************************************************************
* defines and macros (scope: module-local)                        * 
******************************************************************/

//#define OSAL_C_TRACELEVEL1  TR_LEVEL_ERRORS

#define OSAL_C_STRING_INVALID_DEVICE  "/dev/Invalid"
#define OSAL_C_STRING_FLASH_DEVICE  "/dev/flash"
#define INVALID_HANDLE -1
#define INVALID_PARAMETER OSAL_NULL
#define INVALID_ACCESS_MODE -1
#define OSAL_C_S32_IOCTRL_FLASH_INVALID -1
#define OEDT_FLASH_NUMOFBLOCKS_TOERASE 1 
#define OEDT_FLASH_NUMOFREGIONS 1
#define OSAL_C_STRING_DRAGON      "/dev/host/DEBUG/Dragon_GMGED05_Mid.bin"
#define OEDT_FLASH_HELPER_FILE_NAME_NAND0               "/dev/nand0/paralleltest4.txt"
#define OEDT_FLASH_HELPER_FILE_NAME_NOR0               "/dev/nor0/paralleltest4.txt"

//#define OEDT_FLASH_RU_END_OFFSET 9830399
#define OEDT_FLASH_RAW_END_OFFSET 67108864
#define OEDT_FLASH_NUMOFBYTES_TOREAD_WRT 1024
#define OEDT_FLASH_RV_START_OFFSET 62914560 
#define OEDT_FLASH_RU_GMGE_START_OFFSET 4587520 //start address of IDFLASHBOLCK
#define OEDT_FLASH_RU_MFD_START_OFFSET 0 
#define OEDT_FLASH_JLR_START_OFFSET 2097152 //ROW5 start address
#define OEDT_FLASH_ICM_START_OFFSET 65536
#define OEDT_FLASH_INVAL_START_OFFSET (OEDT_FLASH_RAW_END_OFFSET + 1024)
#define OEDT_FLASH_STARTADDRESS_WRITETO 0x00000000 


#if 0
 /* To be changed according to the driver - Anoop */
#define OEDT_FLASH_DEVSIZE (131072*58)
#define OEDT_FLASH_SECTORSIZE  0x20000

#define OEDT_FLASH_NUMOFBYTES_PREDEFWRITE 3072 
#define OEDT_FLASH_ADDRESS_READFROM 0x00000000 
#define OEDT_FLASH_ADDRESS_WRITETO  0x007270e0 //0x0073f000			// 7.5 MB	  ,Flash area(nor o)
#define OEDT_FLASH_ADDRESS_READ  0x007270e0//0x0073f000
#define OEDT_FLASH_ADDRESS_WRITETO_INVAL   0x007a1200			    // 8MB, KDS area	- Kishore
#define  OEDT_FLASH_INVAL_NOR_ADDRESS_WRITETO 0x009a0000
#define OEDT_FLASH_INVAL_NAND_ADDRESS_WRITETO 0xa0000000
#define  ZERO_PARAMETER                 0

#endif



/******************************************************************
* Function prtotypes                                              *
******************************************************************/

tU32 u32DevFlashOpenClose(void);
tU32 u32DevFlashOpenCloseDiffModes(void);
tU32 u32DevFlashOpenInvalParam(void);
tU32 u32DevFlashOpenMulti(void);
tU32 u32DevFlashReClose(void);
tU32 u32DevFlashReadWrtRU(void);
tU32 u32DevFlashReadWrtRV(void);
tU32 u32DevFlashReadInvalParam(void);
tU32 u32DevFlashWrite(void);
tU32 u32DevFlashWriteInvalParam(void);
tU32 u32DevFlashPredefinedWrite(void);
tU32 u32DevFlashWriteReadCombine(void);
tU32 u32DevFlashErase(void);
tU32 u32DevFlashEraseInvalidParam(void);
tU32 u32DevFlashGetRegionInfo(void);
tU32 u32DevFlashGetRegionInfoInvalParam(void);
tU32 u32DevFlashGetVersion(void);
tU32 u32DevFlashGetVersionInvalParam(void);
tU32 u32DevFlashGetInfo(void);
tU32 u32DevFlashGetInfoInvalParam (void);
tU32 u32DevFlashInvalIOCtlMacro(void);
tU32 u32WriteFlashWithInvalAddr(void);
tU32 u32ReadFlashWithInvalAddr(void);
tU32 u32DevFlashHeavyParallelAccess_nor0(void); 
tU32 u32DevFlashHeavyParallelAccess_nand0(void); 
#endif
/* EOF */
