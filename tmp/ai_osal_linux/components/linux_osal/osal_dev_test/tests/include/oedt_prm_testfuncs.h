/******************************************************************************
 * FILE				:	oedt_PRM_TestFuncs.h
 *
 * SW-COMPONENT	:	OEDT_FrmWrk 
 *
 * DESCRIPTION		:	This file has declarations for the PRM test cases for the CDROM/DVD, 
 *                	SDCard 
 *                          
 * AUTHOR(s)		:	Sriranjan U (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * Date				|							|	Author & comments
 * --.--.--			|	Initial revision	|	------------
 * 17 Dec, 2008	|	version 1.0			| Sriranjan U (RBEI/ECM1)
 * 11 Mar, 2009   | version 1.1         | Sainath Kalpuri (RBEI/ECF1)
                                        | Two new OEDTs "u32PRM_NaviDataMedia_event"
										  and "u32PRM_DataMedia_event" have been added
 * 18 Mar, 2009   | version 1.2         | Sainath Kalpuri (RBEI/ECF1)
 										  New OEDT "u32PRM_NaviDataMedia_event_TEST" 
                                          has been added
 * 14 Oct, 2011   | version 1.3         | Anooj Gopi (RBEI/ECF1)
                                          Added Tests for /dev/media devices
 * 24 Jan, 2013   | version 1.4         | Suresh Dhandapani (RBEI/ECF5)
                                          Added Tests for LSIM /dev/usbpower
 * 02 Jun, 2014   | version 1.5         | Gokulkrishnan N (RBEI/ECF5)
                                          Added Tests for Gen3 /dev/usbpower
  * 5 August, 2014 |version 1.8               |Padma Kudari(RBEI/ECF5)
                                          Modified OEDTs to fix CFG3-697.
 *  24 April, 2015 |version 1.9         | Deepak Kumar (RBEI/ECF5)
                                          Added missing OEDTS for PRM
-------------------------------------------------------------------------------*/
#ifndef OEDT_PRM_TESTFUNCS_HEADER
#define OEDT_PRM_TESTFUNCS_HEADER

tU32 u32PRM_DrvOpenClose(void);
tU32 u32PRM_CallbackHandler(void);
tU32 u32PRM_CDVD_inserted_event(tVoid);
tU32 u32PRM_CARD_inserted_event(tVoid);
tU32 u32PRM_NaviDataMedia_event(tVoid);
tU32 u32PRM_DataMedia_event(tVoid);
tU32 u32PRM_NaviDataMedia_event_TEST(tVoid);
tU32 u32PRM_DevMedia_event(tVoid);
tU32 u32PRM_GetStatus(tVoid);
tU32 u32CARD_RegisterforprmNotification(tVoid);
tU32 u32PRM_IOCTRL_REG_SYSTEM_INFO(tVoid);
tU32 u32PRM_IOCTRL_TRIG_ERRMEM_TTF(tVoid);
tU32 u32PRM_IOCTRL_TRIG_DEL_ERRMEM(tVoid);
tU32 u32PRM_IOCTRL_ACTIVATE_SIGNAL(tVoid);
tU32 u32PRM_IOCTRL_REMOUNT(tVoid);
tU32 u32PRM_TEST_max_registry(tVoid);

#if defined (LSIM) || defined (OSAL_GEN3_SIM)
tU32 u32USB_PWR_UV_test(void);
tU32 u32USB_PWR_OC_test(void);
tU32 u32USB_PWR_UNDEF_test(void);
#else
tU32 u32USB_PWR_CTRL_test(void);
tU32 u32USB_PWR_CTRL_Portstate(void);
tU32 u32USB_PWR_CTRL_P1ON(void);
tU32 u32USB_PWR_CTRL_P1OFF(void);
tU32 u32USB_PWR_CTRL_P2ON(void);
tU32 u32USB_PWR_CTRL_P2OFF(void);
#endif
tU32 u32USB_PWR_OC_test(void);

tU32 u32SDCARD_RegisterforprmNotification(tVoid);
#endif /* OEDT_PRM_TESTFUNCS_HEADER */

