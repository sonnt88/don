/************************************************************************
| FILE:         oedt_InputInc_TestFuncs.h
| PROJECT:      platform
| SW-COMPONENT: OEDT_FrmWrk
|------------------------------------------------------------------------------
| DESCRIPTION:  Regression tests for INPUT_INC driver.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 16.05.13  | Initial version            | tma2hi
| 27.06.13  | Add support for protocol v2| tma2hi
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#include "osal_if.h"

#ifndef OEDT_INPUT_INC_TESTFUNCS_HEADER
#define OEDT_INPUT_INC_TESTFUNCS_HEADER

tU32 u32InputIncMultipleDevicesV1(tVoid);
tU32 u32InputIncMultipleDevicesV2(tVoid);
tU32 u32InputIncManyEventsV1(tVoid);
tU32 u32InputIncManyEventsV2(tVoid);
tU32 u32InputIncManyCodesV1(tVoid);
tU32 u32InputIncManyCodesV2(tVoid);
tU32 u32InputIncExtendedKeysV1(tVoid);
tU32 u32InputIncExtendedKeysV2(tVoid);
tU32 u32InputIncMultiTouchTypeAV1(tVoid);
tU32 u32InputIncMultiTouchTypeAV2(tVoid);
tU32 u32InputIncMultiTouchTypeBV1(tVoid);
tU32 u32InputIncMultiTouchTypeBV2(tVoid);
tU32 u32InputIncNonFatalProtoViolationsV1(tVoid);
tU32 u32InputIncNonFatalProtoViolationsV2(tVoid);

#endif
