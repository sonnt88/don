/******************************************************************************
 *FILE         : oedt_Gyro_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk
 *
 *DESCRIPTION  : This file has the function declaration for gyro test cases.
 *               Ref:SW Test specification 0.3 draft
 *
 *AUTHOR       : Ravindran P(CM-DI/ESA)
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 11.05.06 .Initial version
               : 02.06.10 2 New OEDTs have been added
                 24.08.10 - Sainath K (RBEI/ECF1)- New functions realted to "gyrocheckgyrovalueseq"
                 for additiona 2D gyro and tu32Gyrobasicread has been added
                 21.03.13 - Swathi.B (RBEI/ECF5)- New functions related to "gyroopenclose", 
                 "gyroreopen", "Gyroclosealreadyclosed" for Gen3 has been added
                 21.03.13 - Sanjay (RBEI/ECF5) - New funstion related to "multithreadbasicread"
                 for Gen3 has been added
 *
 *****************************************************************************/

#ifndef OEDT_GYRO_TESTFUNCS_HEADER
#define OEDT_GYRO_TESTFUNCS_HEADER



/*Test case function prototypes*/
tU32 u32GyroReadSeq(void);
tU32 u32GyroCheckTimeOutIllegalReadSeq(void);
tU32 u32GyroCheckGyroValuesSeq_r(void);
tU32 u32GyroCheckGyroValuesSeq_s(void);
tU32 u32GyroCheckGyroValuesSeq_t(void);
tU32 u32GyroInterfaceCheckSeq(void);
tU32 u32GyroReadPassingNullBuffer(tVoid);
tU32 u32GyroIOCTRLPassingNullBuffer(tVoid);
tU32 u32GyroBasicRead(tVoid);
tU32 u32MultiThreadBasicRead(tVoid);
tU32 u32GyroTemperature(tVoid);
tU32 u32GyroOpenClose(tVoid);
tU32 u32GyroReOpen(tVoid); 
tU32 u32GyroCloseAlreadyClosed(tVoid); 
tU32 u32GyroOedt_u32ValuesPlot (tVoid);
tU32 u32GyroSelfTest(tVoid);
tU32 ACCOedt_u32ValuesPlot(tVoid);
tU32 u32GyroHwInfo(tVoid);
tU32 u32OpenCloseRepeatedly(tVoid);
tU32 u32GyroGetCycleTime(tVoid);

#endif
