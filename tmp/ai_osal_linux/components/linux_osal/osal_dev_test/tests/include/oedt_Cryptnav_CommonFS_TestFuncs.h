/******************************************************************************
 * FILE         : oedt_Cryptnav_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file contains function declarations for the cryptnav.
 *            
 * AUTHOR(s)    : Anooj Gopi (RBEI/ECF1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                  | Author & comments
 * --.--.--       | Initial revision | ------------
 * 19 Apr, 2011   | version 1.0      | Anooj Gopi (RBEI/ECF1)
 *                |                  | Initial version
 *****************************************************************************/
#ifndef OEDT_CRYPTNAV_COMMON_FS_TESTFUNCS_HEADER
#define OEDT_CRYPTNAV_COMMON_FS_TESTFUNCS_HEADER

#define SWITCH_OEDT_CRYPTNAV
#ifdef SWITCH_OEDT_CRYPTNAV
#define OEDTTEST_C_STRING_DEVICE_CRYPTNAV "/dev/cryptnav"
#else
#define OEDTTEST_C_STRING_DEVICE_CRYPTNAV OSAL_C_STRING_DEVICE_CRYPTNAV
#endif

#define OEDT_C_STRING_CRYPTNAV_DIR1         OEDTTEST_C_STRING_DEVICE_CRYPTNAV"/cfg"
#define OEDT_C_STRING_CRYPTNAV_NONEXST      OEDTTEST_C_STRING_DEVICE_CRYPTNAV"/Invalid"
#define OEDT_C_STRING_FILE_INVPATH_CRYPTNAV OEDTTEST_C_STRING_DEVICE_CRYPTNAV"/Dummydir/Dummy.txt"
#define OEDT_C_STRING_CRYPTNAV_DAT_FILE	    OEDTTEST_C_STRING_DEVICE_CRYPTNAV"/data/data/misc/content.dat"
#define OEDT_C_STRING_DEVICE_CRYPTNAV_ROOT  OEDTTEST_C_STRING_DEVICE_CRYPTNAV"/"

/* Enable file system tests for cryptnav device */
#define OEDT_CRYPTNAV_FS_TEST_ENABLE

tU32 u32Cryptnav_CommonFSOpenClosedevice(tVoid );
tU32 u32Cryptnav_CommonFSOpendevInvalParm(tVoid );
tU32 u32Cryptnav_CommonFSReOpendev(tVoid );
tU32 u32Cryptnav_CommonFSOpendevDiffModes(tVoid );
tU32 u32Cryptnav_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32Cryptnav_CommonFSOpenClosedir(tVoid );
tU32 u32Cryptnav_CommonFSOpendirInvalid(tVoid );
tU32 u32Cryptnav_CommonFSGetDirInfo(tVoid );
tU32 u32Cryptnav_CommonFSOpenDirDiffModes(tVoid );
tU32 u32Cryptnav_CommonFSReOpenDir(tVoid );
tU32 u32Cryptnav_CommonFSFileCreateDel(tVoid );
tU32 u32Cryptnav_CommonFSFileOpenClose(tVoid );
tU32 u32Cryptnav_CommonFSFileOpenInvalPath(tVoid );
tU32 u32Cryptnav_CommonFSFileOpenInvalParam(tVoid );
tU32 u32Cryptnav_CommonFSFileReOpen(tVoid );
tU32 u32Cryptnav_CommonFSFileRead(tVoid );
tU32 u32Cryptnav_CommonFSGetPosFrmBOF(tVoid );
tU32 u32Cryptnav_CommonFSGetPosFrmEOF(tVoid );
tU32 u32Cryptnav_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32Cryptnav_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32Cryptnav_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32Cryptnav_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32Cryptnav_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32Cryptnav_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32Cryptnav_CommonFSGetFileCRC(tVoid );
tU32 u32Cryptnav_CommonFSReadAsync(tVoid);
tU32 u32Cryptnav_CommonFSLargeReadAsync(tVoid);
tU32 u32Cryptnav_CommonFSFileMultipleHandles(tVoid);
tU32 u32Cryptnav_CommonFSFileOpenCloseNonExstng(tVoid);
tU32 u32Cryptnav_CommonFSSetFilePosDiffOff(tVoid);
tU32 u32Cryptnav_CommonFSFileOpenDiffModes(tVoid);
tU32 u32Cryptnav_CommonFSFileReadAccessCheck(tVoid);
tU32 u32Cryptnav_CommonFSFileReadSubDir(tVoid);
tU32 u32Cryptnav_CommonFSFileThroughPutFile(tVoid);
tU32 u32Cryptnav_CommonFSLargeFileRead(tVoid);
tU32 u32Cryptnav_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32Cryptnav_CommonFSReadMultiThread(tVoid);
tU32 u32Cryptnav_CommonFSReadDirExt(tVoid);
tU32 u32Cryptnav_CommonFSReadDirExt2(tVoid);
tU32 u32Cryptnav_CommonFSReadDirExtPartByPart(tVoid);
tU32 u32Cryptnav_CommonFSReadDirExt2PartByPart(tVoid);

#endif //OEDT_CRYPTNAV_COMMON_FS_TESTFUNCS_HEADER
