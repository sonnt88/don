/******************************************************************************
**********                                                           **********
*******                      DECLARATION FILE                           *******
**********                                                           **********
******************************************************************************/
/******************************************************************************
*****   (C) COPYRIGHT Robert Bosch GmbH CM-DI/EAA - All Rights Reserved   *****
******************************************************************************/
/*!				 
***     @file        CCAMessageLoop.h
***     @ingroup     
***     $Workfile:   
***     @Created_by 
\nt                  Matthias Weise CM-DI/PJ GM3
***
***     @date        2007-10-16
***     $Project     GM GE Architecture
***
***     @brief       Test classes for CCA message loop performance tests
***                                 
*** --------------------------------------------------------------------------
\par   TECHNICAL INFORMATION:
***
*** --------------------------------------------------------------------------
\par  VERSION HISTORY:
*** 13-April-2009 | Lint and Compiler warnings removed | Jeryn Mathew (RBEI/ECF1)
*** 05-05-2009 | Lint and Compiler warnings removed | Anoop Chandran(RBEI/ECF1)
*\n*/

/*****************************************************************************/

#ifndef CCAMESSAGELOOP_H_
#define CCAMESSAGELOOP_H_

/*=============================================================================
  =======                           INCLUDES  						    =======
  =============================================================================*/

/*------  standard includes -------*/
#define AIL_S_IMPORT_INTERFACE_GENERIC
#include "ail_if.h"

/*------  project includes -------*/
#include "oedt_ComTest.h"

/*=============================================================================
  =======                       CLASS DECLARATION						 ======
  =============================================================================*/

/*---------------------------------------------------------------------------*/
/*     CLASS: CCATestmessage
 */
/*!    @brief    This class implements a test message class for the CCA message
 *               test. It is derived from the service data message and allows
 *               creation of message with different byte buffers sizes
 */
/*---------------------------------------------------------------------------*/
class CCATestmessage : public amt_tclServiceData
{
	public:
	CCATestmessage();
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: CCATestmessage
	 */
	/*!    @brief    Constructor of the class. The constructor updates the internal
	 *               srructure, alloacates the memory and copies the data from the 
	 *               given buffer
	 * 	   @param 	 pMsgBuffer   Pointer to buffer from which the message shall be filled
	 * 	   @param 	 u32MsgSize   Size of the given buffer
	 * 	   @param 	 bAllocateMsg Indicates if the memory shall be allocated (true by default)
	 */
	/*---------------------------------------------------------------------------*/
	CCATestmessage(tPCU8 pMsgBuffer, tU32 u32MsgSize, tBool bAllocateMsg = TRUE) 
		: amt_tclServiceData()
	{
	  vAddDynMsgSize(u32MsgSize);
      if(bAllocateMsg)
      {
         if(bAllocateMessage() == TRUE)
         {
            vSetSize(u32GetDynMsgSize());
         }
      }
	  memcpy(pu8GetSharedMemBase() + AMT_C_U32_BASEMSG_ABSMSGSIZE + AMT_C_U32_SVCDATA_RELMSGSIZE, pMsgBuffer, u32MsgSize);
	}
	
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: CCATestmessage
	 */
	/*!    @brief    Constructor that construct the object from a base message
	 * 	   @param 	 poBaseMessage Base message to construct this object from
	 */
	/*---------------------------------------------------------------------------*/
	CCATestmessage(amt_tclBaseMessage* poBaseMessage)
    	: amt_tclServiceData(poBaseMessage)
	{
		vSetDynMsgSize(u32GetSize());
	}
	
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: operator =
	 */
	/*!    @brief    Assignment operator which is delegated to the base class
	 * 	   @param 	 roRight Object that shall be assigned to this object
	 */
	/*---------------------------------------------------------------------------*/
	CCATestmessage& operator = (const CCATestmessage& roRight)
	{
	  if ( this != &roRight )
	  {
		 amt_tclServiceData::operator=(roRight);
	  }
	  return *this;
	};

	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: operator ==
	 */
	/*!    @brief    Comparison operator which is delegated to the base class
	 * 	   @param 	 roRight Object that shall be compared to this object
	 */
	/*---------------------------------------------------------------------------*/
	tBool operator == (const CCATestmessage& roRight) const
	{ //lint !e1511: Member hides non-virtual member
	  return amt_tclServiceData::operator==(roRight);
	};

	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: operator !=
	 */
	/*!    @brief    Comparison operator which is solved by negation of the == operator
	 * 	   @param 	 roRight Object that shall be compared to this object
	 */
	/*---------------------------------------------------------------------------*/
	tBool operator != (const CCATestmessage& roRight) const
	{ //lint !e1511: Member hides non-virtual member
	  return !((*this) == roRight);
	};

	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: getPayloadSize
	 */
	/*!    @brief    Returns the size of the data transported by this message
 	 *     @return   Size of the payload data (including identificytion byte)
	 */
	/*---------------------------------------------------------------------------*/
	tU32 getPayloadSize(tVoid) const 
	{
		return u32GetDynMsgSize() - (AMT_C_U32_BASEMSG_ABSMSGSIZE + AMT_C_U32_SVCDATA_RELMSGSIZE); 
	}
};
  
/*---------------------------------------------------------------------------*/
/*     CLASS: CCAMessageLoop
 */
/*!    @brief    This class implements a test CCA application that measures the
 *               message throughput of an CCA application with different message
 *               numbers and sizes
 */
/*---------------------------------------------------------------------------*/
class CCAMessageLoop : public ail_tclOneThreadAppInterface
{
	public:
	CCAMessageLoop();
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: CCAMessageLoop
	 */
	/*!    @brief    Constructor of the class. The constructor initializes the
	 *               members of the class with the given values and default 
	 *               values
	 */
	/*---------------------------------------------------------------------------*/
	CCAMessageLoop(const tChar szTaskName[9], tU16 u16AppID, tU16 u16PartnerAppID, tU16 u16ServiceID);
	
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: ~CCAMessageLoop
	 */
	/*!    @brief    Virtual descructor of the class
	 */
	/*---------------------------------------------------------------------------*/
	virtual ~CCAMessageLoop();
	
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: setMaxQueueMessage
	 */
	/*!    @brief    Sets the maximum number of messages stored in the queue
     * 	   @param 	 u32MaxMessages Maximum number of messages stored in the queue
	 */
	/*---------------------------------------------------------------------------*/
	tVoid setMaxQueueMessage(tU32 u32MaxMessages) { mu32MaxMessages = u32MaxMessages; } 
	
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: setWatchdogInterval
	 */
	/*!    @brief    Set the interval for the watchdog timer 
     * 	   @param 	 u32WatchdogInterval Watchdog timer interval in ms
	 */
	/*---------------------------------------------------------------------------*/
	tVoid setWatchdogInterval(tU32 u32WatchdogInterval) { mu32WatchdogInterval = u32WatchdogInterval; }
	
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: vStart
	 */
	/*!    @brief    Starts the CCA message handling thread. After the creation
	 *               of the object this method starts the standard setup sequence
	 *               of a CCA application
 	 *     @return   true if starting returned no errors, false otherwise
	 */
	/*---------------------------------------------------------------------------*/
	tBool vStart(tVoid);
		
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: vOnNewMessage
	 */
	/*!    @brief    This method is called when a new message arrives and is   
   	 *               overridden by this class. This method handles the test and 
	 *               and the synchronization 
     * 	   @param 	 poMessage Received message
	 */
	/*---------------------------------------------------------------------------*/
	virtual tVoid vOnNewMessage (amt_tclBaseMessage* poMessage);
	
	protected:
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: bSetupRegistry
	 */
	/*!    @brief    Sets up the registry with the task specific values. The 
	 *               method calls bSetupRegistryBase at start. 
 	 *     @return   true if setting worked, false otherwise
	 */
	/*---------------------------------------------------------------------------*/
	tBool bSetupRegistry(tVoid);

	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: bSetupRegistryBase
	 */
	/*!    @brief    Sets up the base part in the registry up to the process. The
	 *               methods checks if the path was already created and returns
	 *               true in that case. Therefore it can be called several times
 	 *     @return   true if setting worked, false otherwise
	 */
	/*---------------------------------------------------------------------------*/
	tBool bSetupRegistryBase(tVoid)const;

	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: setMaxQueueMessage
	 */
	/*!    @brief    Sends a test message from an internal buffer. 
     * 	   @param 	 size Size of the test message to send
     * 	   @param 	 type type (FunctionID) of the message ti send
 	 *     @return   true if setting worked, false otherwise
	 */
	/*---------------------------------------------------------------------------*/	
	tBool bSendTestMessage(tU32 size, tU16 type);
	private:
	/*---------------------------------------------------------------------------*/
	/*     FUNCTION: vAppEntry
	 */
	/*!    @brief    This method is executed at the start of the CCA thread. It 
	 *               contains the loop that waits for messages and dispatches them.
	 *               It is overrriden to set the state of the component and do 
	 *               some initialization.
	 */
	/*---------------------------------------------------------------------------*/
	virtual tVoid vAppEntry ();

	// Name of the process the CCA is running in. Therefore it is static for all
	// CCA threads
	static const tChar* smszProcessName;
	// Name of this task. May be 8 characters long at maximum
	tChar mszTaskName[9];	
	// Service ID of this CCA application
	tU16 mu16ServiceID;
	// Maxmimum mailbox size
	tU32 mu32MaxMailboxSize;
	// Maximum queue size of the CCA thread
	tU32 mu32MaxMessages;
	// The time interval for the watchdog
	tU32 mu32WatchdogInterval;
	// The thread priority of the CCA thread
	tU32 mu32ThreadPriority;
	// The stacksize for the CCA thread
	tU32 mu32ThreadStacksize;
	// Application ID of the communication partner 
	tU16 mu16PartnerAppID;
	// Buffer for messages sent during test
	tU8  mpcMessageBuffer[MAX_MSG_LENGTH];
	// Index in Array of sizes, that is currently tested
	tU32 mu32CurrentSizeCount;
	// Index in Array of message counts, that is currently tested
	tU32 mu32CurrentNumberCount;
	// Number of messages which are currently still to send
	tU32 mu32MessagesToSend;
	// The time a test was stated
	OSAL_tMSecond mStartTime;
	// Buffer for printout of test results
	tChar mszSPrintfBuffer[256];
	// Indicates if registration message was already sent
	tBool mbPartnerRegistered;
};
#endif /*CCAMESSAGELOOP_H_*/
