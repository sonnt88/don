/******************************************************************************
 *FILE         : oedt_pwm_testfunc.h
 *
 *SW-COMPONENT : OEDT_FrmWrk
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that
                 will be used in the file oedt_pwm_testfunc.c
 *
  *AUTHOR(s)    :  Anoop Chandran (RBIN/ECF1)
 *HISTORY      :
                    7-7-2008 Initial  Version v1.0
                    Haribabu Sannapaneni (RBEI/ECF1)
                    25-03-2010 Added defines for oedt_pwm_testfunc.c Version 1.1

*******************************************************************************/


/* Macro definition used in the code */


 /* Function prototypes of functions used
                in file oedt_PWM_TestFuns.c */
/*Test cases*/

#define OSAL_C_PWM_INVALID_ACCESS_MODE              -1
#define OSAL_C_S32_IOCTRL_INVAL_FUNC_PWM            -1000
#define OEDT_PWM_INVAL_PARAM                        0
#define OEDT_PWM_DUTYCYCLE_MINVAL                   0
#define OEDT_PWM_DUTYCYCLE_MIDVAL                   32767
#define OEDT_PWM_DUTYCYCLE_MAXVAL                   65535
#define OEDT_PWM_DUTYCYCLE_INVAL_MIN                -1
#define OEDT_PWM_DUTYCYCLE_INVAL_MAX                65536
#define OEDT_PWM_FREQUENCY              200

#ifndef OEDT_PWM_TESTFUNCS_HEADER
#define OEDT_PWM_TESTFUNCS_HEADER
/*Test cases*/

tU32 u32PWMDevOpenClose(const tS8 * );                      //TU_OEDT_PWM_001
tU32 u32PWMDevOpenCloseInvalParam(const tS8 * );            //TU_OEDT_PWM_002
tU32 u32PWMDevMultipleOpen(const tS8 * );                   //TU_OEDT_PWM_003
tU32 u32PWMDevCloseAlreadyClosed(const tS8 * );         //TU_OEDT_PWM_004
tU32 u32PWMGetVersion(const tS8 * );                        //TU_OEDT_PWM_005
tU32 u32PWMGetVersionInvalParam(const tS8 * );              //TU_OEDT_PWM_006
tU32 u32PWMSetPWMInRange( const tS8 * ,tS32,tS32,tS32,tS32 );                   //TU_OEDT_PWM_007
tU32 u32PWMSetPWMOutRange(const tS8 *,tS32,tS32,tS32 );                 //TU_OEDT_PWM_008
tU32 u32PWMGetPWMData(const tS8 * );                        //TU_OEDT_PWM_009
tU32 u32PWMGetPWMInvalParam(const tS8 * );                  //TU_OEDT_PWM_010
tU32 u32PWMSetGetPWM(const tS8 *,tS32,tS32 );                       //TU_OEDT_PWM_011
tU32 u32PWMInvalidSetGetPWM(const tS8 *,tS32,tS32,tS32 );                   //TU_OEDT_PWM_012
tU32 u32PWMInvalFuncParamToIOCtrl(const tS8 * );            //TU_OEDT_PWM_013
tU32 u32PWMMultSetPWM(const tS8 *,tS32,tS32 );                      //TU_OEDT_PWM_014
tU32 u32PWMIOControlsAfterClose(const tS8 * );              //TU_OEDT_PWM_015
tU32 u32PWMChanelOpenClose(tVoid);
tU32 u32PWMChannelMultipleOpen(tVoid);
tU32 u32PWMChannelCloseAlreadyClosed(tVoid);
tU32 u32PWMChannelGetVersion(tVoid);
tU32 u32PWMChannelGetVersionInvalParam(tVoid);
tU32 u32PWMChannelSetPWMInRange( tVoid );
tU32 u32PWMChannelSetPWMOutRange(tVoid);
tU32 u32PWMChannelGetPWMData(tVoid);
tU32 u32PWMChannelGetPWMInvalParam(tVoid);
tU32 u32PWMChannelSetGetPWM(tVoid);
tU32 u32PWMChannelInvalidSetGetPWM(tVoid);
tU32 u32PWMChannelInvalFuncParamToIOCtrl(tVoid);
#endif
