/******************************************************************************
 *FILE         : oedt_RFS_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file declares test functions for the RFS "file system".
 *            
 *AUTHOR       : pwaechtler
 *
 *****************************************************************************/
#ifndef OEDT_RFS_TESTFUNCS_HEADER
#define OEDT_RFS_TESTFUNCS_HEADER


tU32 OEDT_RFS_TESTDIR_available(void);

#endif
