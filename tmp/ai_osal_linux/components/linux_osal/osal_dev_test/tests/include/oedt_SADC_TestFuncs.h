/******************************************************************************
 *FILE         : oedt_SADC_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_SADC_TestFuncs.c for the  
 *               ADC device for the GM-GE hardware.
 *
 *AUTHOR       : Haribabu Sannapaneni (RBEI/ECM1) 
 *
 *HISTORY      : Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
				 version 0.1
				 version 0.2 Haribabu Sannapaneni (RBEI/ECM1) on 2,July,2008
				             Added new testcase proto types.
				            
*******************************************************************************/

#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	
#include <basic.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>

#ifndef OEDT_SADC_TESTFUNCS_HEADER
#define OEDT_SADC_TESTFUNCS_HEADER

/* Macro definition used in the code */

typedef enum {
  DN_SADC_INFO = -200        /* High-speed units information */
} SADC_data_no;


enum
{
  LIMVIO_INT_REG = 0x8,
  STATES_INT_MSK = 0x10,
  LIMVIO_INT_MSK = 0x14,
  LIMVIO_CMP_REG_CH4 = 0x20,
  LIMVIO_CMP_REG_CH5 = 0x24,
  LIMVIO_CMP_REG_CH6 = 0x28,
  LIMVIO_CMP_REG_CH7 = 0x2c
};

/* Function prototypes of functions used in file oedt_SADC_TestFuns.c */

tU32 u32SADCHighSpeedUnitOpenClose(tVoid);
tU32 u32SADCLowSpeedUnitOpenClose(tVoid);
tU32 u32SADCChanReOpen(tVoid);
tU32 u32SADCChanCloseAlreadyClosed(tVoid);
tU32 u32SADCHighSpeedUnitRead(tVoid );
tU32 u32SADCLowSpeedUnitRead(tVoid );
tU32 u32SADCSetThreshold(tVoid);
tU32 u32SADCGetVersion(tVoid);
tU32 u32SADCGetAttributeData(tVoid);
tU32 u32SADCSetRegisters(tVoid);
tU32 u32SADCOpenCloseWithAlias(tVoid);
tU32 u32SADCSetThresholdCallbackForCH4(tVoid);
tU32 u32SADCSetThresholdCallbackForCH5(tVoid);
tU32 u32SADCSetThresholdCallbackForCH6(tVoid);
tU32 u32SADCSetThresholdCallbackForCH7(tVoid);
tU32 u32SADCHighSpeedEOCCallBack(tVoid);
tU32 u32SADCLowSpeedEOCCallBack(tVoid);
tU32 u32SADCSetSingleThresholdCallbackCH7(tVoid);


#endif

