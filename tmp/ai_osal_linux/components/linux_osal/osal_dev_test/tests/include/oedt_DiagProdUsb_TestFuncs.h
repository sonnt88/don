/**********************************************************FileHeaderBegin******
 *
 * FILE:        odt_DiagProdUsb_TestFuncs.h
 *
 * CREATED:     2005-12-06
 *
 * AUTHOR:
 *
 * DESCRIPTION: -
 *
 * NOTES: -
 *
 * COPYRIGHT:  (c) 2005 Technik & Marketing Service GmbH
 *
 **********************************************************FileHeaderEnd*******/

/*****************************************************************************
 *
 * search for this KEYWORDS to jump over long histories:
 *
 * CONSTANTS - TYPES - MACROS - VARIABLES - FUNCTIONS - PUBLIC - PRIVATE
 *
 ******************************************************************************/

/******************************************************************************
 * LOG:
 *
 * $Log: $
 ******************************************************************************/


#ifndef __ODT_DIAGPRODUSB_TESTFUNCS_H__
#define __ODT_DIAGPRODUSB_TESTFUNCS_H__


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/



/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/



/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/



/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/



/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/
tU32 u32DiagProdUsbTest(tVoid);



#endif  /* #ifndef __ODT_DIAGPRODUSB_TESTFUNCS_H__                          */

/* End of File odt_DiagProdUsb_TestFuncs.h                                  */

