/******************************************************************************
|FILE         : oedt_RegistryTest.h
|SW-COMPONENT : OEDT_FrmWrk 
|DESCRIPTION  : This file contains definitions for the OEDT registry test
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          | 				          | Author	& comments
| --.--.--      | Initial revision        | -------, -----
| 30 May 2011   | version 1.0             | Martin Langer, initial version
|------------------------------------------------------------------------------
|*****************************************************************************/

#ifndef OEDT_REGISTRYTEST_H
#define OEDT_REGISTRYTEST_H

/******************************************************************
 * Includes                                                       *
 ******************************************************************/
#include "OsalConf.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "Linux_osal.h"
#include "reg_if.h"
#include "scd_if.h"
#include "ostrace.h"

/******************************************************************
 * Defines                                                       *
 ******************************************************************/

 #define MAXVALUE 12
 
/******************************************************************
 * Function prototypes                                            *
 ******************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

extern tU32 u32RegBlockCreateAndRemove(void);
extern tU32 u32RegUseInvalidKey(void);

#ifdef __cplusplus
}
#endif

#endif // OEDT_REGISTRYTEST_H
