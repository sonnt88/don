/******************************************************************************
 *FILE         : oedt_Acousticecnr_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for the acoustic device.
 *                also the array of function pointer for this device has been 
 *                initialised.
 *
 *AUTHOR       : Suryachand Yellamraju (RBEI/ECF5)
 *
 *COPYRIGHT    : (c)2014 -  RBEI - Robert Bosch Engineering and Business Solutions Limited
 *
 *HISTORY:       03/01/2013
 *               Initial Revision.
 *
 *****************************************************************************/

#ifndef OEDT_ACOUSTICECNR_TESTFUNCS_HEADER
#define OEDT_ACOUSTICECNR_TESTFUNCS_HEADER

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

#define OSALTEST_PND_USE_NO_AUDIOROUTER

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
// OEDT Functions
tU32 OEDT_ACOUSTICECNR_T000(void);
tU32 OEDT_ACOUSTICECNR_T001(void);
tU32 OEDT_ACOUSTICECNR_T002(void);
tU32 OEDT_ACOUSTICECNR_T003(void);
tU32 OEDT_ACOUSTICECNR_T004(void);
tU32 OEDT_ACOUSTICECNR_T005(void);
tU32 OEDT_ACOUSTICECNR_T006(void);
tU32 OEDT_ACOUSTICECNR_T007(void);
tU32 OEDT_ACOUSTICECNR_T008(void);
tU32 OEDT_ACOUSTICECNR_T009(void);

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/


#endif


