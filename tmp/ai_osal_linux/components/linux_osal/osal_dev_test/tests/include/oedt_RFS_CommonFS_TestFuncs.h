/******************************************************************************
 * FILE				: oedt_RFS_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT	: OEDT_FrmWrk 
 *
 * DESCRIPTION		: This file defines the macros and prototypes for the 
 *					  filesystem test cases for the RFS
 *                          
 * AUTHOR(s)		:  pwaechtler
 *
 * HISTORY			: copied from FFS2 files
 * ------------------------------------------------------------------------------*/

#ifndef OEDT_RFS_COMMON_FS_TESTFUNCS_HEADER
#define OEDT_RFS_COMMON_FS_TESTFUNCS_HEADER


#if 1 //#ifdef SWITCH_OEDT_RFS
#define OEDTTEST_C_STRING_DEVICE_RFS "/dev/rfs"
#else
#define OEDTTEST_C_STRING_DEVICE_RFS OSAL_C_STRING_DEVICE_RFS
#endif

#define OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR    OEDTTEST_C_STRING_DEVICE_RFS"/tmp"

#define OEDT_C_STRING_DEVICE_RFS_TESTDIR        OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR
#define OEDT_C_STRING_RFS_DIR1                  OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/NewDir"
#define OEDT_C_STRING_RFS_NONEXST               OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/Invalid"
#define OEDT_C_STRING_RFS_FILE1                 OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/FILEFIRST.txt"
#define SUBDIR_PATH_RFS	                        OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/NewDir"
#define SUBDIR_PATH2_RFS	                    OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/NewDir/NewDir2"
#define OEDT_C_STRING_RFS_DIR_INV_NAME          OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/*@#**"
#define OEDT_C_STRING_RFS_DIR_INV_PATH          OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/MYFOLD1/MYFOLD2/MYFOLD3"
#define	CREAT_FILE_PATH_RFS                     OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/Testf1.txt"  
#define OSAL_TEXT_FILE_FIRST_RFS                OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/file1.txt"

#define OEDT_C_STRING_FILE_INVPATH_RFS          OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/Dummydir/Dummy.txt"

#define OEDT_RFS_COMNFILE                       OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/common.txt"
#define OEDT_RFS_WRITEFILE                      OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/WriteFile.txt"

#define FILE12_RECR2_RFS                        OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/File_Dir/File_Dir2/File12_test.txt"
#define FILE11_RFS                              OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/File_Dir/File11.txt"
#define FILE12_RFS                              OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/File_Dir/File12.txt"
#define FILE13_RFS                              OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/File_Source/File13.txt"
#define FILE14_RFS                              OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/File_Source/File14.txt"

#define OEDT_RFS_CFS_DUMMYFILE                  OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/Dummy.txt"
#define OEDT_RFS_CFS_TESTFILE                   OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/Test.txt"
#define OEDT_RFS_CFS_INVALIDFILE                OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/DIR/Test.txt"
#define OEDT_RFS_CFS_UNICODEFILE                OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/��汪�.txt"
#define OEDT_RFS_CFS_INVALCHAR_FILE             OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"//*/</>>.txt"
#define OEDT_RFS_CFS_IOCTRL_SAVENOW_FILE_SYNC	OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/ioctrl_save_now_RFS_sync.txt"
#define OEDT_RFS_CFS_IOCTRL_SAVENOW_FILE_ASYNC	OEDTTEST_C_STRING_DEVICE_RFS_TESTDIR"/ioctrl_save_now_RFS_async.txt"
	 											  
/* Function Prototypes for the functions defined in Oedt_FileSystem_TestFuncs.c */
tU32 u32RFS_CommonFSOpenClosedevice(tVoid );
tU32 u32RFS_CommonFSOpendevInvalParm(tVoid );
tU32 u32RFS_CommonFSReOpendev(tVoid );
tU32 u32RFS_CommonFSOpendevDiffModes(tVoid );
tU32 u32RFS_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32RFS_CommonFSOpenClosedir(tVoid );
tU32 u32RFS_CommonFSOpendirInvalid(tVoid );
tU32 u32RFS_CommonFSCreateDelDir(tVoid );
tU32 u32RFS_CommonFSCreateDelSubDir(tVoid );
tU32 u32RFS_CommonFSCreateDirInvalName(tVoid );
tU32 u32RFS_CommonFSRmNonExstngDir(tVoid );
tU32 u32RFS_CommonFSRmDirUsingIOCTRL(tVoid );
tU32 u32RFS_CommonFSGetDirInfo(tVoid );
tU32 u32RFS_CommonFSOpenDirDiffModes(tVoid );
tU32 u32RFS_CommonFSReOpenDir(tVoid );
tU32 u32RFS_CommonFSDirParallelAccess(tVoid);
tU32 u32RFS_CommonFSCreateDirMultiTimes(tVoid ); 
tU32 u32RFS_CommonFSCreateRemDirInvalPath(tVoid );
tU32 u32RFS_CommonFSCreateRmNonEmptyDir(tVoid );
tU32 u32RFS_CommonFSCopyDir(tVoid );
tU32 u32RFS_CommonFSMultiCreateDir(tVoid );
tU32 u32RFS_CommonFSCreateSubDir(tVoid);
tU32 u32RFS_CommonFSDelInvDir(tVoid);
tU32 u32RFS_CommonFSCopyDirRec(tVoid );
tU32 u32RFS_CommonFSRemoveDir(tVoid);
tU32 u32RFS_CommonFSFileCreateDel(tVoid );
tU32 u32RFS_CommonFSFileOpenClose(tVoid );
tU32 u32RFS_CommonFSFileOpenInvalPath(tVoid );
tU32 u32RFS_CommonFSFileOpenInvalParam(tVoid );
tU32 u32RFS_CommonFSFileReOpen(tVoid );
tU32 u32RFS_CommonFSFileRead(tVoid );
tU32 u32RFS_CommonFSFileWrite(tVoid );
tU32 u32RFS_CommonFSGetPosFrmBOF(tVoid );
tU32 u32RFS_CommonFSGetPosFrmEOF(tVoid );
tU32 u32RFS_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32RFS_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32RFS_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32RFS_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32RFS_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32RFS_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32RFS_CommonFSGetFileCRC(tVoid );
tU32 u32RFS_CommonFSReadAsync(tVoid);
tU32 u32RFS_CommonFSLargeReadAsync(tVoid);
tU32 u32RFS_CommonFSWriteAsync(tVoid);
tU32 u32RFS_CommonFSLargeWriteAsync(tVoid);
tU32 u32RFS_CommonFSWriteOddBuffer(tVoid);
tU32 u32RFS_CommonFSFileMultipleHandles(tVoid);
tU32 u32RFS_CommonFSWriteFileWithInvalidSize(tVoid);
tU32 u32RFS_CommonFSWriteFileInvalidBuffer(tVoid);
tU32 u32RFS_CommonFSWriteFileStepByStep(tVoid);
tU32 u32RFS_CommonFSGetFreeSpace (tVoid);
tU32 u32RFS_CommonFSGetTotalSpace (tVoid);
tU32 u32RFS_CommonFSFileOpenCloseNonExstng(tVoid);
tU32 u32RFS_CommonFSFileDelWithoutClose(tVoid);
tU32 u32RFS_CommonFSSetFilePosDiffOff(tVoid);
tU32 u32RFS_CommonFSCreateFileMultiTimes(tVoid);
tU32 u32RFS_CommonFSFileCreateUnicodeName(tVoid);
tU32 u32RFS_CommonFSFileCreateInvalName(tVoid);
tU32 u32RFS_CommonFSFileCreateLongName(tVoid);
tU32 u32RFS_CommonFSFileCreateDiffModes(tVoid);
tU32 u32RFS_CommonFSFileCreateInvalPath(tVoid);
tU32 u32RFS_CommonFSStringSearch(tVoid);
tU32 u32RFS_CommonFSFileOpenDiffModes(tVoid);
tU32 u32RFS_CommonFSFileReadAccessCheck(tVoid);
tU32 u32RFS_CommonFSFileWriteAccessCheck(tVoid);
tU32 u32RFS_CommonFSFileReadSubDir(tVoid);
tU32 u32RFS_CommonFSFileWriteSubDir(tVoid);
tU32 u32RFS_CommonFSReadWriteTimeMeasureof1KbFile(tVoid);
tU32 u32RFS_CommonFSReadWriteTimeMeasureof10KbFile(tVoid);
tU32 u32RFS_CommonFSReadWriteTimeMeasureof100KbFile(tVoid);
tU32 u32RFS_CommonFSReadWriteTimeMeasureof1MBFile(tVoid);
tU32 u32RFS_CommonFSReadWriteTimeMeasureof1KbFilefor1000times(tVoid);
tU32 u32RFS_CommonFSReadWriteTimeMeasureof10KbFilefor1000times(tVoid);
tU32 u32RFS_CommonFSReadWriteTimeMeasureof100KbFilefor100times(tVoid);
tU32 u32RFS_CommonFSReadWriteTimeMeasureof1MBFilefor16times(tVoid);
tU32 u32RFS_CommonFSRenameFile(tVoid);
tU32 u32RFS_CommonFSWriteFrmBOF(tVoid);
tU32 u32RFS_CommonFSWriteFrmEOF(tVoid);
tU32 u32RFS_CommonFSLargeFileRead(tVoid);
tU32 u32RFS_CommonFSLargeFileWrite(tVoid);
tU32 u32RFS_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32RFS_CommonFSWriteMultiThread(tVoid);
tU32 u32RFS_CommonFSWriteReadMultiThread(tVoid);
tU32 u32RFS_CommonFSReadMultiThread(tVoid);
tU32 u32RFS_CommonFSFormat(tVoid);
tU32 u32RFS_CommonFSFileGetExt(tVoid);
tU32 u32RFS_CommonFSFileGetExt2(tVoid);
tU32 u32RFS_CommonFSSaveNowIOCTRL_SyncWrite(tVoid);
tU32 u32RFS_CommonFSSaveNowIOCTRL_AsynWrite(tVoid);
tU32 u32RFS_CommonFileOpenCloseMultipleTime( tVoid );
tU32 u32RFS_CommonFileOpenCloseMultipleTimeRandom1( tVoid );
tU32 u32RFS_CommonFileOpenCloseMultipleTimeRandom2( tVoid );
tU32 u32RFS_CommonFileLongFileOpenCloseMultipleTime(tVoid);
tU32 u32RFS_CommonFileLongFileOpenCloseMultipleTimeRandom1(tVoid);
tU32 u32RFS_CommonFileLongFileOpenCloseMultipleTimeRandom2(tVoid);
tU32 u32RFS_CommonFileOpenCloseMultipleTimeMultDir(tVoid);

#endif //OEDT_RFS_COMMON_FS_TESTFUNCS_HEADER

