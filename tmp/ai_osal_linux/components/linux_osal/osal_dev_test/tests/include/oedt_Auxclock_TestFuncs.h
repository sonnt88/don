/******************************************************************************
 *FILE         : oedt_Auxclock_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file has the function declarations for aux clock test cases 
 *               Ref:SW Test specification 0.1 draft
 *               
 *AUTHOR       :Ravindran P(CM-DI/ESA)
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 06.04.06 .Initial version
 *
 *****************************************************************************/
#ifndef OEDT_AUXCLOCK_TESTFUNCS_HEADER
#define OEDT_AUXCLOCK_TESTFUNCS_HEADER

/*Test cases*/

tU32 tu32AuxclockCheckTimeStampValiditySeq( void );
//tU32 tu32AuxclockPosixTimeCheck(void);
tU32 tu32AuxclockOpenCloseDev(void);
/*Test cases*/

#endif


