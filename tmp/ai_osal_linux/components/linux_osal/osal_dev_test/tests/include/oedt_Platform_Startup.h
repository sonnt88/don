
/******************************************************************************
 * FILE         : oedt_Platform_Startup.c
 *
 * SW-COMPONENT : OEDT_FrmWrk
 *
 * DESCRIPTION  : This file implements the platform startup measurement test 
 *
 * AUTHOR(s)    :  Dainius Ramanauskas (CM-AI/PJ-CF31)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                       | Author & comments
 * --.--.--       | Initial revision      | ------------
 * 25 Mai, 2011    | version 1.0           | Dainius Ramanauskas (CM-AI/PJ-CF31)
  -----------------------------------------------------------------------------*/
#ifndef OEDT_PLATFROM_STARTUP_HEADER
#define OEDT_PLATFROM_STARTUP_HEADER


tU32 u32StartupTimePlatform(tVoid );

#endif //OEDT_PLATFROM_STARTUP_HEADER


