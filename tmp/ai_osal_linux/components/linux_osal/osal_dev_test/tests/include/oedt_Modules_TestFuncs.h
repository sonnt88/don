
#ifndef OEDT_MODULES_TESTFUNCS_H
#define OEDT_MODULES_TESTFUNCS_H

#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "osal_if.h"


struct module_info {
	tCString module_name;
	tBool    module_loaded;
};

/******************************************************************
* Function prototypes                                             *
******************************************************************/

tU32 u32ModuleLoadCheck(void);


#endif // OEDT_MODULES_TESTFUNCS_H
