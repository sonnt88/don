/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        This file implements the individual test cases ( OEDT testcases)
 *				       for PDD
 * @addtogroup   PDD unit test
 * @{
 */
#ifndef OEDT_PDD_TESTFUNCS_HEADER
#define OEDT_PDD_TESTFUNCS_HEADER


#if defined(GEN3ARM) 
tU32 u32PDDNorUserReadEarlyBufferGreat(void);
tU32 u32PDDNorUserReadEarlyBufferSmall(void);
tU32 u32PDDNorUserReadEarlyDelStreamAndNor(void);
tU32 u32PDDNorUserReadSize(void);
tU32 u32PDDNorUserReadWrite(void);
tU32 u32PDDNorUserWriteLengthToGreat(void);
tU32 u32PDDNorUserWriteReadMorePools(void);
tU32 u32PDDNorUserCheckEqualCluster(void);
tU32 u32PDDNorUserLoadDumpOK(void);
tU32 u32PDDNorUserLoadDumpMagicWrong(void);
tU32 u32PDDNorUserLoadDumpWriteInterrupt(void);
tU32 u32PDDNorUserLoadDumpChecksumWrong(void);
tU32 u32PDDNorUserWriteTwoStreams(void);
tU32 u32PDDSccReadDataSize(void);
tU32 u32PDDSccReadWrite(void);
tU32 u32PDDSccReadLengthToGreat(void);
tU32 u32PDDSccWriteLengthToGreat(void);
tU32 u32PDDNorKernelWrite(void);
tU32 u32PDDNorKernelReadDataSize(void);
tU32 u32PDDNorKernelReadWrite(void);
tU32 u32PDDNorKernelWriteReadCompare(void);
tU32 u32PDDNorKernelReadWrongVersion(void);
tU32 u32PDDNorKernelReadLengthToGreat(void);
tU32 u32PDDNorKernelWriteLengthToGreat(void);
tU32 u32PDDSccWriteCheckBackup(void);
tU32 u32PDDSccWriteCheckSync(void);
tU32 u32PDDSccWriteDestroyCheckSync(void);
tU32 u32PDDNorUserWriteCheckBackup(void);
tU32 u32PDDNorUserWriteCheckSync(void);
tU32 u32PDDNorUserWriteEraseNorCheckSync(void);
tU32 u32PDDDeleteDataFs(void);
tU32 u32PDDDeleteDataFsSecure(void);
tU32 u32PDDDeleteDataCheckParamString(void);
tU32 u32PDDDeleteDataCheckParamLocation(void);
tU32 u32PDDDeleteDataTwice(void);
tU32 u32PDDDeleteDataBackupNotExist(void);
tU32 u32PDDDeleteDataNormalNotExist(void);
#endif

#endif
