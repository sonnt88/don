/******************************************************************************
| FILE			:	oedt_Audio_LoopBackTestFuncs.h
| PROJECT		:      ADIT GEN 2
| SW-COMPONENT	: 	OEDT AUDIO LOOPBACK 
|------------------------------------------------------------------------------
| DESCRIPTION	:	This file contains audio loopback test declaration
|------------------------------------------------------------------------------
| COPYRIGHT		:	(c) 2011 Bosch
| HISTORY		:      
|------------------------------------------------------------------------------
| Date 		| 	Modification			|	Author
|------------------------------------------------------------------------------
| 11.05.2011  	| Initial revision			| 	SUR2HI
| --.--.--  	| ----------------           | ------------
|*****************************************************************************/


#ifndef OEDT_AUDIO_LOOPBACK_TESTFUNCS_HEADER
#define OEDT_AUDIO_LOOPBACK_TESTFUNCS_HEADER

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

// OEDT Functions
tU32 OEDT_AUDIO_LOOPBACK_T001(void);
#endif

