/******************************************************************************
 * FILE         : oedt_CARD_TestFuncs.h
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file implements the individual test cases for the USB  
 *                device for GM-GE hardware.
 *              
 * AUTHOR(s)    :  Anoop Chandran (RBEI/ECM1) 10/10/2008
 * HISTORY      :  Taken form Paramount platform 
******************************FileHeaderEnd**********************************/


#define OEDT_DRV_CARD_TEST_PASSED 0
/*#define OEDT_DRV_CARD_TEST_FAILED 1*/

#define CARD_MOUNT    1
#define CARD_UNMOUNT  0


tU32 u32GeneralTestInLoop(void);
tS32 u32CardDetectcheck(void);
tS32 u32CardCreateRemove(void);
tS32 u32CardMultCreate(void);
tS32 u32CardRemoveWOCreate(void);
tS32 u32CardOpenClose(void);
tS32 u32CardFileCreateRemove(void);
tS32 u32CardFileOpenClose(void);
tS32 u32CardFileMultipleOpen(void);
tS32 u32CardFileWriteRead(void);
tS32 u32CardIOCtrl_MountError(void);
tS32 u32CardFormat(void);
tS32 u32CardCheckDisk(void);
tS32 u32CardFileParallelOpen(void);
tS32 u32CardFileParallelClose(void);
tS32 u32CardFileParallelWrite(void);
tS32 u32CardFileParallelRead(void);
tS32 u32CardFileParallelAccess(void);
tS32 u32CardFileParallelReadWrite(void);
tS32 u32CardFileReadWrite_Performance(void);
tS32 u32CardFileParallelReadFormat(void);
tS32 u32CardIOCtrl_CARD_MOUNT(void);
tU32 u32CardCheckdisk(void);
tU32 u32CardIOCtrl(void);
tU32 u32CardPerformanceAsyncIO(void);



