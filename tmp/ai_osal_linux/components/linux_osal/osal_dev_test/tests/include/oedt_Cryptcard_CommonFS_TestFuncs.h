/******************************************************************************
 * FILE         : oedt_Cryptcard_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file contains function declarations for the CARD device.
 *            
 * AUTHOR(s)    : Anooj Gopi (RBEI/ECF1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                  | Author & comments
 * --.--.--       | Initial revision | ------------
 * 19 Apr, 2011   | version 1.0      | Anooj Gopi (RBEI/ECF1)
 *                |                  | Initial version ported from gen1
 *****************************************************************************/
#ifndef OEDT_CRYPTCARD_COMMON_FS_TESTFUNCS_HEADER
#define OEDT_CRYPTCARD_COMMON_FS_TESTFUNCS_HEADER

#if 1

#define SWITCH_OEDT_CRYPTCARD
#ifdef SWITCH_OEDT_CRYPTCARD
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD "/dev/cryptcard"
#else
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD OSAL_C_STRING_DEVICE_CRYPTCARD
#endif

#define OEDT_C_STRING_CRYPTCARD_DIR1         OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/cfg"
#define OEDT_C_STRING_CRYPTCARD_NONEXST      OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/Invalid"
#define OEDT_C_STRING_FILE_INVPATH_CRYPTCARD OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/Dummydir/Dummy.txt"
#define OEDT_C_STRING_CRYPTCARD_DAT_FILE     OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/data/data/misc/content.dat"
#define OEDT_C_STRING_DEVICE_CRYPTCARD_ROOT  OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/"

#else
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD "/dev/card"
#define OEDT_C_STRING_CRYPTCARD_DIR1         OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/cryptnav""/cfg"
#define OEDT_C_STRING_CRYPTCARD_NONEXST      OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/cryptnav""/Invalid"
#define OEDT_C_STRING_FILE_INVPATH_CRYPTCARD OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/cryptnav""/Dummydir/Dummy.txt"
#define OEDT_C_STRING_CRYPTCARD_DAT_FILE     OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/cryptnav""/data/data/misc/content.dat"
#define OEDT_C_STRING_DEVICE_CRYPTCARD_ROOT  OEDTTEST_C_STRING_DEVICE_CRYPTCARD"/cryptnav""/"
#endif

/* Function Prototypes for the functions defined in Oedt_FileSystem_TestFuncs.c */
tU32 u32Cryptcard_CommonFSOpenClosedevice(tVoid );
tU32 u32Cryptcard_CommonFSOpendevInvalParm(tVoid );
tU32 u32Cryptcard_CommonFSReOpendev(tVoid );
tU32 u32Cryptcard_CommonFSOpendevDiffModes(tVoid );
tU32 u32Cryptcard_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32Cryptcard_CommonFSOpenClosedir(tVoid );
tU32 u32Cryptcard_CommonFSOpendirInvalid(tVoid );
tU32 u32Cryptcard_CommonFSGetDirInfo(tVoid );
tU32 u32Cryptcard_CommonFSOpenDirDiffModes(tVoid );
tU32 u32Cryptcard_CommonFSReOpenDir(tVoid );
tU32 u32Cryptcard_CommonFSFileCreateDel(tVoid );
tU32 u32Cryptcard_CommonFSFileOpenClose(tVoid );
tU32 u32Cryptcard_CommonFSFileOpenInvalPath(tVoid );
tU32 u32Cryptcard_CommonFSFileOpenInvalParam(tVoid );
tU32 u32Cryptcard_CommonFSFileReOpen(tVoid );
tU32 u32Cryptcard_CommonFSFileRead(tVoid );
tU32 u32Cryptcard_CommonFSGetPosFrmBOF(tVoid );
tU32 u32Cryptcard_CommonFSGetPosFrmEOF(tVoid );
tU32 u32Cryptcard_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32Cryptcard_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32Cryptcard_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32Cryptcard_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32Cryptcard_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32Cryptcard_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32Cryptcard_CommonFSGetFileCRC(tVoid );
tU32 u32Cryptcard_CommonFSReadAsync(tVoid);
tU32 u32Cryptcard_CommonFSLargeReadAsync(tVoid);
tU32 u32Cryptcard_CommonFSFileMultipleHandles(tVoid);
tU32 u32Cryptcard_CommonFSFileOpenCloseNonExstng(tVoid);
tU32 u32Cryptcard_CommonFSSetFilePosDiffOff(tVoid);
tU32 u32Cryptcard_CommonFSFileOpenDiffModes(tVoid);
tU32 u32Cryptcard_CommonFSFileReadAccessCheck(tVoid);
tU32 u32Cryptcard_CommonFSFileReadSubDir(tVoid);
tU32 u32Cryptcard_CommonFSFileThroughPutFile(tVoid);
tU32 u32Cryptcard_CommonFSLargeFileRead(tVoid);
tU32 u32Cryptcard_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32Cryptcard_CommonFSReadMultiThread(tVoid);
tU32 u32Cryptcard_CommonFSReadDirExt(tVoid);
tU32 u32Cryptcard_CommonFSReadDirExt2(tVoid);
tU32 u32Cryptcard_CommonFSReadDirExtPartByPart(tVoid);
tU32 u32Cryptcard_CommonFSReadDirExt2PartByPart(tVoid);

#endif //OEDT_Cryptcard_COMMON_FS_TESTFUNCS_HEADER
