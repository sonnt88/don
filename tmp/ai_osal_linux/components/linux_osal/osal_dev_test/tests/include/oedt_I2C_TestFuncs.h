/******************************************************************************
 *FILE         : oedt_I2C_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : Function declarations for the TDA7564,M24C64 device.
 *            
 *AUTHOR       : Narasimha Prasad Palasani(RBIN/ECM1)
 *
 *------------------------------------------------------------------------------
 * HISTORY:
 *______________________________________________________________________________
 * Date          | 				           | Author	& comments
 * --.--.--      | Initial revision        | -------, -----
 *02 June 2006   |  version 1.0            |Narasimha Prasad Palasani(RBIN/ECM1)
 *------------------------------------------------------------------------------
 *			     |	                      |
 *08 June 2006   |  version 2.0           |Narasimha Prasad Palasani(RBIN/ECM1)
 *   		     |                        |Updated after review comments
 *
 *------------------------------------------------------------------------------
 *			     |	                      |
 *23 Aug 2006    |  version 2.1           |Narasimha Prasad Palasani(RBIN/ECM1)
 *   		     |                        |ISP1301 defines removed and 
 *               |                        |M24C64 defines added
 *-----------------------------------------------------------------------------
 *05 Oct 2006    |	version 2.2           |Rakesh Dhanya (RBIN/ECM1)
 *               |						  |Error code values changed.
 *______________________________________________________________________________
 *06 Oct 2006    |	version 2.3           |Haribabu Sannapaneni (RBIN/ECM1)
 *               |						  |New test cases are added to improve
                 |                        |  TCA coverage.
 *______________________________________________________________________________
 *2nd Feb 2007   |	version 2.4           |Sandeep.M.Joshi (RBIN/ECM1)
 *               |						  |Added the prototype for the 
 *										  |new test case u32I2CGetVersion()
 *______________________________________________________________________________
 *25 June 2007   |	version 2.5           |Kishore Kumar.R (RBIN/EDI3)
 *               |						  |Added the prototype for the 
 *				 |						  |test cases  TU_OEDT_I2C_027,028,029,
 				 |						  |	030.And also defines used .
 *______________________________________________________________________________
 *27 Jan 2009    |	version 2.6           |Haribabu S(RBEI/ECF1)
 *               |						  |Added the prototype for the 
 *				 |						  |test case  TU_OEDT_I2C_033
 *______________________________________________________________________________
 *25 Mar 2009    |	version 2.7           |Sainath Kalpuri S(RBEI/ECF1)
 *               |						  |Removed Lint and compiler warnings 
 *				 |						  |
 				 |						  |	
*______________________________________________________________________________

 *22 Apr 2009    |	version 2.8           |Sainath Kalpuri S(RBEI/ECF1)
 *               |						  |Removed Lint and compiler warnings 
 *				 |						  |
 				 |						  |	

 ******************************************************************************/

#ifndef OEDT_I2C_TESTFUNCS_HEADER
#define OEDT_I2C_TESTFUNCS_HEADER


tU32 u32I2CTDA7564AutoTest(tVoid);
tU32 u32I2CMultiDevOpen(tVoid);
tU32 u32I2COpenDevDiffModes(tVoid);
tU32 u32I2CDevOpenInvalParam(tVoid);
//tU32 u32I2CDevOpenNullParam(tVoid);
tU32 u32I2CDevCloseInvalParam(tVoid);
tU32 u32I2CDevReClose(tVoid) ;
tU32 u32I2CWriteDataInvalParam(tVoid);
tU32 u32I2CWriteDataAfterDevClose(tVoid ) ;
tU32 u32I2CReadDataInvalParam(tVoid );
tU32 u32I2CReadDataAfterDevClose(tVoid) ;
tU32 u32I2CSetWriteBreakTimeInvalParam(tVoid);
tU32 u32I2CSetWriteBreakTimeAfterDevClose(tVoid);
tU32 u32I2CSetReadBreakTimeInvalParam(tVoid);
tU32 u32I2CSetReadBreakTimeAfterDevClose(tVoid);
tU32 u32I2CSetClkSpeedInvalParam(tVoid);
tU32 u32I2CSetClkSpeedAfterDevClose(tVoid);
tU32 u32I2CGetVersion(tVoid);
tU32 u32I2CGetVersionInvalParam(tVoid);
tU32 u32I2CGetVersionAfterDevClose(tVoid) ;
tU32 u32I2CCallbackInvalParam(tVoid);
tU32 u32I2CCallbackAfterDevClose(tVoid);
tU32 u32I2CIOCTRLWithInvalParam(tVoid);
//tU32 u32I2CReOpenDevice (tVoid);
//tU32 u32I2COpendeviceWithInvalChanlId (tVoid);
//tU32 u32I2CClosedeviceWithInvalChanlId (tVoid);
tU32 u32I2CGetCallbackPointer(tVoid);
tU32  u32I2CGetChannelState(tVoid);
//tU32  u32I2C_IOCTRLWithInvalParam(tVoid);
tU32 u32I2CIOWriteAndReadTDAWithInvalParam(tVoid);
tU32 u32I2CIOWriteWithInvalChanlId(tVoid);
tU32 u32I2CTDAOffsetDetection(tVoid);
tU32 u32I2CSetWriteReadBreakTimeInval(tVoid);
tU32 u32I2CSetClkSpeedZero(tVoid);
tU32 u32I2CTDACurrentSensorActivte(tVoid);
tU32 u32I2CTDAWriteMoreBytes(tVoid);

/*M24C64 define */
#define M24C64_NUM_REG       22

#define M24C64_VENDORID      0x04cc
#define M24C64_PRODUCTID     0x1301
#define M24C64_VERSIONID     0x0210


/*registername*/
#define M24C64_VENDOR_ID							    0x00
#define M24C64_PRODUCT_ID								0x02
#define M24C64_VERSION_ID								0x14
#define M24C64_MODE_CTRL1_SET						    0x04
#define M24C64_MODE_CTRL1_CLR 						    0x05
#define M24C64_MODE_CTRL2_SET						    0x12
#define M24C64_MODE_CTRL2_CLR						    0x13
#define M24C64_OTG_CTRL_SET							0x06
#define M24C64_OTG_CTLR_CLR 							0x07
#define M24C64_OTG_STATUS								0x10
#define M24C64_INT_SOURCE								0x08
#define M24C64_INT_LATCH_SET							0x0a
#define M24C64_INT_LATCH_CLR							0x0b
#define M24C64_INT_ENABLE_LOW_SET				        0x0c
#define M24C64_INT_ENABLE_LOW_CLR			        	0x0d
#define M24C64_INT_ENABLE_HIGH_SET			        	0x0e
#define M24C64_INT_ENABLE_HIGH_CLR				        0x0f


/******************************************************************/
/* defines and macros (scope: module-local)                       */
/******************************************************************/
/* version string */
#define I2C_C_S32_IO_VERSION   0x0100  /*read v01.00 */

/*define the error codes */
#define OEDT_I2C_E_VERSION_WRONG_DRV_I2C                 0004
#define OEDT_I2C_E_VERSION_FAILS                         0008
#define OEDT_I2C_E_OPEN_FAILS                            0002
#define OEDT_I2C_E_OPEN_FAILS_NOT_SUPPORTED              0001
#define OEDT_I2C_E_WRITE_FAILS                           0050
#define OEDT_I2C_E_READ_FAILS                            0016
#define OEDT_I2C_E_WRITE_FAILS_TIMEOUT                   0500
#define OEDT_I2C_E_READ_FAILS_TIMEOUT                    0400
#define OEDT_I2C_E_READ_DATA_WRONG                       8888
#define OEDT_I2C_E_SET_CLOCKSPEED_FAILS                  0075
#define OEDT_I2C_E_SET_TIMEOUT_READ_FAILS                1400
#define OEDT_I2C_E_SET_TIMEOUT_WRITE_FAILS               0700
#define OEDT_I2C_E_CLOSE_FAILS                           0200
#define OEDT_I2C_TDA7564_WRITE_CALLBACK_FAILS			 0020
#define OEDT_I2C_TDA7564_READ_CALLBACK_FAILS			 0100
#define OEDT_I2C_M24C64_WRITE_CALLBACK_FAILS			 3333
#define OEDT_I2C_M24C64_READ_CALLBACK_FAILS			     5555
//#define OSAL_C_S32_IOCTRL_INVALID_DEVICE                "dev/invalid"
#define I2C_CALLBACK_ID_READ     0001
#define I2C_CALLBACK_ID_WRITE    0002
#define I2C_CALLBACK_ID_NO	     0000
#define I2C_INVALID_PARAM        -1
#define OSAL_C_S32_IOCTRL_I2C_INVALID -1				/*TDA defines*/
/* Num Register TDA7564 Instructions Byte*/
#define NUM_REG_TDA7564_IB 2
/* Num Register TDA7564 Instructions Byte*/
#define NUM_REG_TDA7564_DB 4
#define INVALID_NUM_REG_TDA7564_IB -1

/*Instruction to TDA 7564 -used in  test case TU_OEDT_I2C_027*/
#define OFFSET_ENABLE_1 0x60
#define OFFSET_ENABLE_2 0x10

/*Instruction to TDA 7564 -used in  test case TU_OEDT_I2C_029*/
#define	CURRENTSENSOR_STANDBY_1 0x00
#define	CURRENTSENSOR_STANDBY_2 0x14

#define CLK_SPEED 0	 /* Used in test case TU_OEDT_I2C_028*/
#define BYTES 10	 /* Used in test case TU_OEDT_I2C_030*/

#define MASK_1 0x80	 /*Used in test cases TU_OEDT_I2C_027,029,030 */
#define MASK_2 0x40

#define OEDT_I2C_INIT_VAL 0x0000
#define INVALID_VAL -1
/* Num Register ADV1 Instructions Byte*/
#define NUM_REG_ADV1_IB 2
/* Num Register ADV1 Data Byte*/
#define NUM_REG_ADV1_DB 2

/* Num Register ADV2 Instructions Byte*/
#define NUM_REG_ADV2_IB 2
/* Num Register ADV1 Data Byte*/
#define NUM_REG_ADV2_DB 2
#define NUM_REG_FPGA_IB 8



/*define testmode TDA */
typedef enum
{
  I2C_IC_TEST_MODE_TDA_READ=1,
  I2C_IC_TEST_MODE_TDA_MUTE,
  I2C_IC_TEST_MODE_TDA_DEMUTE
}OEDT_I2C_Testmode;

/*****************************************************************/
/*  static variable                                              */
/*****************************************************************/

/*array for the data to read*/
/*sak9kor - Below mentioned variable declaration is made as dead code to remove the compiler 
            warnings.code which is using these variables is a dead code,
            hence making these declarations as dead code */
#if 0
static tU8          I2C_u8RxDataArr[M24C64_NUM_REG];
static tU8          I2C_u8RxSubAddrArr[M24C64_NUM_REG];
#endif


/*****************************************************************/
/* static functions                                              */
/*****************************************************************/
/*sak9kor - Below mentioned fucntion prototype is made as dead code to remove the compiler 
            warnings. Since the below mentioned function is a callback function,
            code which registers this function as a callback is a dead code,
            hence making this prototype as dead code */
#if 0
static tU32 u32I2CCallbackM24C64(tU16  u16CallbackId);
#endif


/*sak9kor - Below mentioned fucntion prototypes made as dead code to remove the compiler 
            warnings. Since the below mentioned functions body are under dead code,
			so the prototypes are also made as dead code */

/*functions for test M24C64 */
#if 0
static tU32  OEDT_I2C_FTestM24C64Automatic(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);
static tU32  OEDT_I2C_FReadAndCompareIdM24C64(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);
static tU32  OEDT_I2C_FWriteReadOneByteM24C64(OSAL_tIODescriptor PI2cDevice,tU32 Pu32Ret);
static tU32  OEDT_I2C_FReadByteM24C64(OSAL_tIODescriptor PI2cDevice,tU8 Pu8SubAddr,tU8 Pu8Number,tU32 Pu32Ret);
static tU32  OEDT_I2C_FWriteRegisterAddrM24C64(OSAL_tIODescriptor PI2cDevice,tU8 Pu8Addr,tU32 Pu32Ret);
static tU32  OEDT_I2C_FWriteOneByteM24C64(OSAL_tIODescriptor PI2cDevice,tU8 Pu8SubAddr,tU8 Pu8Data,tU32 Pu32Ret);
#endif

/*functions for test ADV1, ADV2 and FPGA  */

tU32 u32I2CADV1AutoTest(tVoid);
tU32 u32I2CADV2AutoTest(tVoid);
tU32 u32I2CFPGAAutoTest(tVoid);


 #endif
 
/* EOF */
 
