/******************************************************************************
 * FILE				:	oedt_FS_TestFuncs.h
 *
 * SW-COMPONENT		:	OEDT_FrmWrk 
 *
 * DESCRIPTION		:	This file contains the prototype and some Macros that 
 *						will be used in the file oedt_FS_TestFuncs.c   
 *               		for the FileSystem device.
 *                          
 * AUTHOR(s)		:	Sriranjan U (RBEI/ECM1), Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * Date				|							|	Author & comments
 * --.--.--			|	Initial revision		|	------------
 * 2 Dec, 2008		|	version 1.0				|	Sriranjan U (RBEI/ECM1)
 *					|							|	Shilpa Bhat(RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 13 Jan, 2009     |   version 1.1     	    |   Shilpa Bhat(RBEI/ECM1)
 ----------------------------------------------------------------------------------
 * 25 Mar, 2009	  | Version 1.2         | Vijay D (RBEI/ECF1) 
 *-----------------------------------------------------------------------------
 * 25 /03/ 2009   | version 1.3    	| Lint Remove Anoop Chandran(RBEI/ECF1)
  *-----------------------------------------------------------------------------
 * 21 /07/ 2009   | version 1.4    	| OEDT for NOR Sriranjan U(RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 10 /09/ 2009   | version 1.5    	|  Commenting the Test cases which uses 
 *				  |					|  OSAL_C_S32_IOCTRL_FIOOPENDIR
 *				  |					|  Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 16 Mar, 2010   | version 1.6           |  OEDT to measure the open time
 *                |                       |  Anoop Chandran
 *-----------------------------------------------------------------------------
 * 02 June, 2010  | version 1.7           |  changed u32FSCopyDirFS1FS2() to
 *                |                       |   u32FSCopyDirTest()
 *                |                       |  by Anoop Chandran
 *-----------------------------------------------------------------------------
 * 19 April, 2011 | version 2.2           |  Added following functions from gen1
 *                |                       |  u32FSReadDirExtPartByPart
 *                |                       |  u32FSReadDirExt2PartByPart
 *                |                       |  Anooj Gopi (RBEI/ECF1)
 * 26 Feb , 2013  | Version 2.3           | Removed u32FSPrepareEject()
 *                |                       | Under Unsed OSAL IO control Removal
 *                |                       |  By SWM2KOR
 ------------------------------------------------------------------------------*/

#ifndef OEDT_FS_TESTFUNCS_HEADER
#define OEDT_FS_TESTFUNCS_HEADER

#include "oedt_testing_macros.h"

#define OSAL_C_TRACELEVEL1  TR_LEVEL_ERRORS

#define OEDT_C_STRING_DEVICE_INVAL "/dev/Invalid"
#define OEDT_INVALID_ACCESS_MODE -100

#define SUBDIR_NAME 		"NewDir"
#define SUBDIR_NAME_INVAL 	"/Inval"
#define SUBDIR_NAME2 		"NewDir2"
#define SUBDIR_NAME3 		"NewDir3"

#define OEDT_DIR_NAME 		"/NewDir1"
#define OEDT_TEST_FILE      "abcdefghij"
#define OEDT_TEST_LONG_FILE "abcdefghijklmnopqrstuvwxyzabcdefghij"
#define OEDT_FILE_TYPE      ".txt"
#define OEDT_FS_BASE_COUNT  1000
#define OEDT_CD_COPY_DIR_TEST_FILE "/00_cd_test_firstfile.dat"

#define OEDT_MAX_TOTAL_DIR_PATH_LEN 		260
#define OEDT_CDROM_NEGATIVE_POS_TO_SET 		-75
#define OEDT_CDROM_BYTES_TO_READ 			20
#define OEDT_CDROM_OFFSET_BEYOND_END 		559
#define OEDT_FFS1_MAX_FILE_PATH_LEN    		260 
#define OEDT_FFS1_MAX_TOTAL_DIR_PATH_LEN	260	

#define FS_THREAD_STACK_SIZE 	(1024)
#define FS_EVENT_WAIT_TIMEOUT 	(110000)


#define FILE_OPER_SUCCESS		0
#define FILE_CREATE_FAIL		100
#define FILE_REMOVE_FAIL		200
#define COMMON_FILE_ERROR		150

#define OEDT_CDROM_ZERO_PARAMETER 0
#define OEDT_FS_SIZE_OF_DATA_FILE 5242880

#define FS_SKIP_COUNT 		20
#define FS_BYTES_TO_READ_N 	5


#define EVENT_POST_THREAD_1 (0x00000001) 
#define EVENT_POST_THREAD_2 (0x00000002)
#define EVENT_POST_THREAD_2_STOP_WAIT (0x00000004)
#define EVENT_POST_THREAD_1_STOP_WAIT (0x00000005)
#define EVENT_POST_DIR_RECURSIVE_START (0x00000006)
#define EVENT_POST_DIR_RECURSIVE_STOP (0x00000007)
#define EVENT_POST_CREAT_FILE (0x00000008)

#define FS_NO_IOP 		(8)
#define DIR1		"File_Source"
#define DIR			"File_Dir"
#define DIR_RECR1	"File_Dir2" 

#define OEDT_FS_FILE_PATH_LEN 					44
#define CFS_FILE_NAME_MAX_LEN_WITHOUT_DEV_NAME  65 /*Should be 250 but probem in remove */
#define FILE_NAME_MAX_LEN                       255
#define FILE_NAME_MAX_LEN_EXCEEDED              256
#define FILE_NAME_MAX_LEN_EXCEEDED_1            257

#define OEDT_CFST_MB_SIZE                      		((tU32)1048576)	
#define OEDT_CFST_256KB_SIZE                      	((tU32)262144)	
#define OEDT_CFST_1KB_SIZE                      	((tU32)1024)	
#define OEDT_CFST_10KB_SIZE                      	((tU32)10240)	
#define OEDT_CFST_100KB_SIZE                      	((tU32)102400)	
#define OEDT_CFST_10000		                      	((tU32)10000)	
#define OEDT_CFST_1000		                      	((tU32)1000)	
#define OEDT_CFST_100		                      	((tU32)100)	

#define FS_THREAD1               		        0x0001
#define FS_THREAD2                         		0x0002
#define EVENT_NAME 								"FS_ThrOp1"
#define EVENT_NAME1 							"FS_ThrOp2"
#define CDROM_BYTES_TO_READ_TPUT 				1048576
#define WRITE_SIZE								((tU32)16777216)
#define CFST_FILESYSTEM_NO_IOP 		(8)

typedef struct 
{
	tString szDevName;
	OSAL_tEventMask mask;
	OSAL_tIODescriptor ioHandle;
	tU32 u32Ret;
	
}FSThreadParam;

	 											  
/* Function Prototypes for the functions defined in Oedt_FS_TestFuncs.c */
tU32 u32FSOpenClosedevice(const tS8 * dev_name );
tU32 u32FSOpendevInvalParm(const tS8 *  dev_name );
tU32 u32FSReOpendev(const tS8 *  dev_name );
tU32 u32FSOpendevDiffModes(const tS8 *  dev_name );
tU32 u32FSClosedevAlreadyClosed(const tS8 *  dev_name );
tU32 u32FSOpenClosedir(const tS8 *  dev_name );
tU32 u32FSOpendirInvalid(const tPS8 * dev_name );
tU32 u32FSCreateDelDir(const tS8 *  dev_name );
tU32 u32FSCreateDelSubDir(const tS8 *  dev_name );
tU32 u32FSCreateDirInvalName(const tPS8 * dev_name );
tU32 u32FSRmNonExstngDir(const tS8 *  dev_name );
tU32 u32FSRmDirUsingIOCTRL(const tPS8 *  dev_name );
tU32 u32FSGetDirInfo(const tS8 * dev_name, tBool bCreateRemove);
tU32 u32FSOpenDirDiffModes(const tPS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSReOpenDir(const tPS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSDirParallelAccess(const tS8 *  dev_name);
tU32 u32FSCreateDirMultiTimes(const tPS8 *  dev_name, tBool bCreateRemove); 
tU32 u32FSCreateRemDirInvalPath(const tPS8 *  dev_name);
tU32 u32FSCreateRmNonEmptyDir(const tPS8 *  dev_name, tBool bCreateRemove );
tU32 u32FSCopyDir(const tPS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSMultiCreateDir(const tS8 * dev_name, tBool bCreateRemove );
tU32 u32FSCreateSubDir(const tPS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSDelInvDir(const tPS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSCopyDirRec(const tPS8 *  dev_name);
tU32 u32FSRemoveDir(const tPS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSFileCreateDel(const tS8 * dev_name );
tU32 u32FSFileOpenClose(const tS8 * dev_name, tBool bCreateRemove );
tU32 u32FSFileOpenInvalPath(const tS8 * dev_name );
tU32 u32FSFileOpenInvalParam(const tS8 * dev_name,  tBool bCreateRemove);
tU32 u32FSFileReOpen(const tS8 * dev_name, tBool bCreateRemove );
tU32 u32FSFileRead(const tS8 * dev_name, tBool bCreateRemove);
tU32 u32FSFileWrite(const tS8 * dev_name, tBool bCreateRemove );
tU32 u32FSGetPosFrmBOF(const tS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSGetPosFrmEOF(const tS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSFileReadNegOffsetFrmBOF(const tS8 *  dev_name, tBool bCreateRemove );
tU32 u32FSFileReadOffsetBeyondEOF(const tS8 *  dev_name, tBool bCreateRemove );
tU32 u32FSFileReadOffsetFrmBOF(const tS8 *  dev_name, tBool bCreateRemove );
tU32 u32FSFileReadOffsetFrmEOF(const tS8 *  dev_name, tBool bCreateRemove );
tU32 u32FSFileReadEveryNthByteFrmBOF(const tS8 *  dev_name, tBool bCreateRemove );
tU32 u32FSFileReadEveryNthByteFrmEOF(const tS8 *  dev_name, tBool bCreateRemove );
tU32 u32FSGetFileCRC(const tS8 *  dev_name, tBool bCreateRemove);
tU32 u32FSReadAsync(const tS8 *, tBool);
tU32 u32FSLargeReadAsync(const tS8 * , tBool);
tU32 u32FSWriteAsync(const tS8 * , tBool);
tU32 u32FSLargeWriteAsync(const tS8 * , tBool);
tU32 u32FSWriteOddBuffer(const tS8 * , tBool);
tU32 u32FSWriteFileWithInvalidSize(const tS8 * , tBool);
tU32 u32FSWriteFileInvalidBuffer(const tS8 * , tBool);
tU32 u32FSWriteFileStepByStep(const tS8 * , tBool);
tU32 u32FSGetFreeSpace (const tS8 *);
tU32 u32FSGetTotalSpace (const tS8 * );
tU32 u32FSFileOpenCloseNonExstng(const tS8 *);
tU32 u32FSFileDelWithoutClose(const tS8 *);
tU32 u32FSSetFilePosDiffOff(const tS8 * , tBool);
tU32 u32FSCreateFileMultiTimes(const tS8 * , tBool);
tU32 u32FSFileCreateUnicodeName(const tS8 * );
tU32 u32FSFileCreateInvalName(const tS8 *);
tU32 u32FSFileCreateLongName(const tS8 *);
tU32 u32FSFileCreateDiffModes(const tS8 *);
tU32 u32FSFileCreateInvalPath(const tS8 *);
tU32 u32FSStringSearch(const tS8 * , tBool);
tU32 u32FSFileOpenDiffModes(const tS8 * ps8file_name, tBool bCreateRemove);
tU32 u32FSFileReadAccessCheck(const tS8 *, tBool);
tU32 u32FSFileWriteAccessCheck(const tS8 *  , tBool);
tU32 u32FSFileReadSubDir(const tS8 * );
tU32 u32FSFileWriteSubDir(const tS8 * );
tU32 u32FSTimeMeasureof1KbFile(const tS8 * );
tU32 u32FSTimeMeasureof10KbFile(const tS8 * );
tU32 u32FSTimeMeasureof100KbFile(const tS8 * );
tU32 u32FSTimeMeasureof1MBFile(const tS8 * );
tU32 u32FSTimeMeasureof1KbFilefor1000times(const tS8 * );
tU32 u32FSTimeMeasureof10KbFilefor1000times(const tS8 * );
tU32 u32FSTimeMeasureof100KbFilefor100times(const tS8 * );
tU32 u32FSTimeMeasureof1MBFilefor16times(const tS8 * , const tS8 * );
tU32 u32FSFileThroughPutFirstFile(const tS8 * );
tU32 u32FSFileThroughPutSecondFile(const tS8 * );
tU32 u32FSFileThroughPutMP3File(const tS8 * );
tU32 u32FSRenameFile(const tS8 * );
tU32 u32FSWriteFrmBOF(const tS8 * , tBool);
tU32 u32FSWriteFrmEOF(const tS8 * , tBool);
tU32 u32FSLargeFileRead(const tS8 * , tBool);
tU32 u32FSLargeFileWrite(const tS8 * );
tU32 u32FSOpenCloseMultiThread(const tS8 * );
tU32 u32FSWriteMultiThread(const tS8 * );
tU32 u32FSWriteReadMultiThread(const tS8 * );
tU32 u32FSReadMultiThread(const tS8 * );
tU32 u32FSFormat(const tS8 * );
tU32 u32FSCheckDisk(const tS8 * );
tU32 u32FSRW_Performance_Diff_BlockSize(const tS8 *  ps8file_name);
tU32 u32FSFileGetExt(const tS8 *  strDevName);
tU32 u32FSFileGetExt2(const tS8 *  strDevName);

tU32 u32FFSCommonFSSaveNowIOCTRL_SyncWrite(const tS8* strDevName);
tU32 u32FFSCommonFSSaveNowIOCTRL_AsynWrite(const tS8* strDevName);

tU32 u32FSReadWriteHugeData (const tS8 * ps8file_name, tChar cData);
tU32 u32FSWriteHugeDataAbort(const tS8 * strDevName);
tU32 u32FSReadHugeDataAbort(const tS8 * strDevName);
tU32 u32FSGetFileSize(const tS8 * strDevName);
tU32 u32FSReadDirValidate(const tS8 * ps8_name);
tU32 u32FSRmRecursiveCancel(const tS8* dev_name);
tU32 u32FSIoctrlGetMountPointInfo(const tS8 * dev_name);
tU32 u32FSCreateManyThreadDeleteMain(const tS8 * dev_name);
tU32 u32FSCreateManyDeleteInThread(const tS8 * dev_name);
tU32 u32FSOpenManyCloseInThread(const tS8 * dev_name);
tU32 u32FSOpenInThreadCloseMain(const tS8 * dev_name);
tU32 u32FSWriteMainReadInThread(const tS8 * dev_name);
tU32 u32FSWriteThreadReadInMain(const tS8 * dev_name);
tU32 u32FSFileAccInDiffThread(const tS8 * dev_name);
tU32 u32FSCreateTestDataFile( const tS8* , const tS8* , tS32 , const tS8*  );
tU32 u32FSRemoveTestDataFile( const tS8*  );
tU32 u32FSFileOpenCloseMultipleTime( const tS8*, tBool, tS32, tS32, const tS8* );
tU32 u32FSFileOpenCloseMultipleTimeRandom1( const tS8*, tBool, tS32, tS32, const tS8* );
tU32 u32FSFileOpenCloseMultipleTimeRandom2( const tS8*, tBool, tS32, tS32, const tS8* );
tU32 u32FSCopyDirTest( const tPS8* , tBool  );
tU32 u32FSReadDirExtPartByPart( const tS8 * strDevName );
tU32 u32FSReadDirExt2PartByPart( const tS8 * strDevName );

tVoid vFSStressTest(const tPS8 strDevName, OEDT_TESTSTATE* pteststate);

#endif


