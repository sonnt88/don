/******************************************************************************
 *FILE         : oedt_PWM_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_PWM_TestFuncs.c for the  
 *               Timer device for the GM-GE hardware.
 *
 *AUTHOR       : Anoop Chandran (RBIN/ECM1) 
 *
 *HISTORY      : 22-Dec-2008 Initial  Version v1.0 - Anoop Chandran
*******************************************************************************/


/* Macro definition used in the code */

 
 /* Function prototypes of functions used 
				in file oedt_PWM_TestFuns.c */
/*Test cases*/

#ifndef __OEDT_PWM_FAN_TESTFUNCS_HEADER_H_
#define __OEDT_PWM_FAN_TESTFUNCS_HEADER_H_

#define OSAL_C_PWM_INVALID_ACCESS_MODE           	-1
#define OEDT_PWM_INVAL_PARAM				 0
#define OEDT_PWM_DUTYCYCLE_MINVAL			 0
#define OEDT_PWM_DUTYCYCLE_MIDVAL		     32767
#define OEDT_PWM_DUTYCYCLE_MAXVAL		     65535
#define OEDT_PWM_DUTYCYCLE_INVAL_MIN 			-1
#define OEDT_PWM_DUTYCYCLE_INVAL_MAX 		     65536
#define OSAL_C_S32_IOCTRL_INVAL_FUNC_PWM	     -1000
/*Test cases*/

tU32 u32PWMFanDevOpenClose(tVoid);						//TU_OEDT_PWM_001
tU32 u32PWMFanDevOpenCloseInvalParam(tVoid);			//TU_OEDT_PWM_002
tU32 u32PWMFanDevMultipleOpen(tVoid);					//TU_OEDT_PWM_003
tU32 u32PWMFanDevCloseAlreadyClosed(tVoid);			//TU_OEDT_PWM_004
tU32 u32PWMFanGetVersion(tVoid);						//TU_OEDT_PWM_005
tU32 u32PWMFanGetVersionInvalParam(tVoid);				//TU_OEDT_PWM_006
tU32 u32PWMFanSetPWMInRange( tVoid );					//TU_OEDT_PWM_007
tU32 u32PWMFanSetPWMOutRange(tVoid);					//TU_OEDT_PWM_008
tU32 u32PWMFanGetPWMData(tVoid);						//TU_OEDT_PWM_009
tU32 u32PWMFanGetPWMInvalParam(tVoid);					//TU_OEDT_PWM_010
tU32 u32PWMFanSetGetPWM(tVoid);						//TU_OEDT_PWM_011
tU32 u32PWMFanInvalidSetGetPWM(tVoid);					//TU_OEDT_PWM_012
tU32 u32PWMFanInvalFuncParamToIOCtrl(tVoid);			//TU_OEDT_PWM_013
tU32 u32PWMFanMultSetPWM(tVoid);						//TU_OEDT_PWM_014
tU32 u32PWMFanIOControlsAfterClose(tVoid);				//TU_OEDT_PWM_015
tU32 u32PWMFanSetGetInval(tVoid);						//TU_OEDT_PWM_016

#endif
