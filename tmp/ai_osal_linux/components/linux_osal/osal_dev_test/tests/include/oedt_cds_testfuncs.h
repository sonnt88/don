/**********************************************************FileHeaderBegin******
 *
 * FILE:        oedt_cds_testfuncs.h
 *
 * CREATED:     2005-03-08
 *
 * AUTHOR:      Ulrich Schulz / TMS
 *
 * DESCRIPTION:
 *      OEDT tests for the memory  management system
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 *            Modified by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
              Version - 1.1

 **********************************************************FileHeaderEnd*******/
/* TENGINE Header */
#ifndef OEDT_CDS_TESTFUNC_HEADER
#define OEDT_CDS_TESTFUNC_HEADER

tU32 u32CdsTest1(void);


#endif //OEDT_CDS_TESTFUNC_HEADER


