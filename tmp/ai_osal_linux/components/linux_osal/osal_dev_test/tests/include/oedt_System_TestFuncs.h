#ifndef _OEDT_SYSTEM_TEST_FUNCS_H_
#define _OEDT_SYSTEM_TEST_FUNCS_H_
/*!
 *\file     oedt_SystemTestFuncs.h
 *\ref      
 *\brief    defines all oedt system test-cases
 *              
 *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH
 *\author		CM-DI/PJ-GM32 - Resch
 *\version:
 * CVS Log: 
 * $Log: oedt_SystemTestFuncs.h
 * \bugs      
 * \warning   
 **********************************************************************/


/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/
tU32 u32_OEDT_SystemTest1_ThreadSpawn(void);
tU32 u32_OEDT_SystemTest2_ThreadSpawnWithoutDelete(void);
tU32 u32_OEDT_SystemTest3_ThreadSpawnWithoutExit(void);
tU32 u32_OEDT_SystemTest4_KillRunningThreads(void);
tU32 u32_OEDT_SystemTest5_StartMultipleThreads(void);

#else/* #ifndef _OEDT_SYSTEM_TEST_FUNCS_H_ */
#error "oedt_System_TestFuncs.h multiple included"
#endif/* #ifndef _OEDT_SYSTEM_TEST_FUNCS_H_ */
