/**********************************************************************************
 *FILE         : oedt_ADR3Ctrl_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmmeWork
 *
 *DESCRIPTION  : This Is The Header File Of oedt_ADR3Ctrl_TestFuncs.c
 *               
 *AUTHOR       : Madhu Sudhan Swargam (RBEI/ECF5)
 *
 *COPYRIGHT    : (C) COPYRIGHT RBEI - All Rights Reserved 
 * HISTORY      :
 *------------------------------------------------------------------------------------
 * Date         |        Modification               | Author & comments
 *--------------|-----------------------------------|-----------------------------------
 * 27.Aug.2013  |  Intial  Modification             |  Madhu Sudhan Swargam (RBEI/ECF5)
 * 24.Dec.2013  |  Response wait Time for events    |
 *              | or messages is made for 4 seconds |  Madhu Sudhan Swargam (RBEI/ECF5)
 * 13.Jan.2013  |  OEDT ADDITION                    |  Madhu Sudhan Swargam (RBEI/ECF5)
 * 03.Mar.2014  | To remove ADR3CTRl state enum def |  Madhu Sudhan Swargam (RBEI/ECF5)
 * 14.Mar.2014  | To remove Lint and Trace Update   |  Madhu Sudhan Swargam (RBEI/ECF5)
 * -----------------------------------------------------------------------------------
 *************************************************************************************/
#ifndef OEDT_ADR3CTRL_TESTFUNC_HEADER
#define OEDT_ADR3CTRL_TESTFUNC_HEADER

#define MQ_ADR3CTRL_MP_OPEN_CNCRNCY              1
#define MQ_ADR3CTRL_MP_CLOSE_CNCRNCY             2
#define MQ_ADR3CTRL_MP_CBREG_CNCRNCY             3
#define MQ_ADR3CTRL_MP_NOR_RST_CNCRNCY           4
#define MQ_ADR3CTRL_MP_SPI_RST_CNCRNCY           5
#define MQ_ADR3CTRL_MP_CONTROL_MAX_MSG           10
#define MQ_ADR3CTRL_MP_CONTROL_MAX_LEN           sizeof(tU32)
#define MQ_ADR3CTRL_MP1_RESPONSE_NAME            "MQ_P1ADR3CTRL_RSPNS"
#define MQ_ADR3CTRL_MP2_RESPONSE_NAME            "MQ_P2ADR3CTRL_RSPNS"
#define MQ_ADR3CTRL_MP1_OPEN_DEVICE_NAME         "MQ_P1ADR3CTRL_OPEN"
#define MQ_ADR3CTRL_MP2_OPEN_DEVICE_NAME         "MQ_P2ADR3CTRL_OPEN"
#define MQ_ADR3CTRL_MP1_CLOSE_DEVICE_NAME        "MQ_P1ADR3CTRL_CLOSE"
#define MQ_ADR3CTRL_MP2_CLOSE_DEVICE_NAME        "MQ_P2ADR3CTRL_CLOSE"
#define MQ_ADR3CTRL_MP1_REG_CB_NAME              "MQ_P1ADR3CTRL_REGCB"
#define MQ_ADR3CTRL_MP2_REG_CB_NAME              "MQ_P2ADR3CTRL_REGCB"
#define MQ_ADR3CTRl_MP1_CB_REGSTER               0x00000001ul
#define MQ_ADR3CTRl_MP1_RESET_OBSERVED           0x00000002ul
#define MQ_ADR3CTRL_MP1_PROCESS_EXIT             0x00000004ul
#define MQ_ADR3CTRl_MP2_CB_REGSTER               0x00000008ul
#define MQ_ADR3CTRl_MP2_RESET_OBSERVED           0x00000010ul
#define MQ_ADR3CTRL_MP2_PROCESS_EXIT             0x00000020ul
#define MQ_ADR3CTRl_MP1_OPEN1                    0x004F504E31
#define MQ_ADR3CTRl_MP2_OPEN2                    0x004F504E32
#define MQ_ADR3CTRl_MP1_CLSE1                    0x00434C5331 
#define MQ_ADR3CTRl_MP2_CLSE2                    0x00434C5332
#define MQ_ADR3CTRl_MP1_REG1                     0x0052454731
#define MQ_ADR3CTRl_MP2_REG2                     0x0052454732
#define MQ_ADR3CTRl_MP1_NRST1                    0x004E525331
#define MQ_ADR3CTRl_MP2_NRST2                    0x004E525332
#define MQ_ADR3CTRl_MP1_DRST1                    0x0044525331
#define MQ_ADR3CTRl_MP2_DRST2                    0x0044525332
/** Events Used for CallBack changes handling */
#define ADR3EVENT_UNKNOWERROR                   0x00000001ul
#define ADR3CALLBACK_INVALIDSTATE               0x00000002ul
#define ADR3CALLBACK_ALIVESTATE                 0x00000004ul
#define ADR3CALLBACK_DEADSTATE                  0x00000008ul
#define ADR3CALLBACK_DNLSTATE                   0x00000010ul
#define ADR3CALLBACK_INITSTATE                  0x00000020ul
#define ADR3CTRL_STATEWAIT_MIN_ERROR            1000
#define ADR3CTRL_RESPONSE_WAIT_TIME             5000 // five seconds
#define ADR3CTRL_MAXDEVICES                     10
#define ADR3CTRL_NORMALMODE                     1
#define ADR3CTRL_SPIMODE                        2
#define TRACE_BUF_SIZE                          90
/*ADR3CTRL OEDT Test Functions Declarations*/
tU32  u32OedtADR3CtrlBasiOpenClose(tVoid);
tU32  u32OedtADR3CtrlCallBackRegister(tVoid);
tU32  u32OedtADR3CtrlResetCheck(tVoid);
tU32  u32OedtADR3CtrlSPI_NOR_ModeCheck(tVoid);
tU32  u32ADR3CtrlMultiProcessTest(tVoid);
tU32  u32OedtADR3CtrlReadonlyOpen(tVoid);
tU32  u32OedtADR3CtrlWriteonlyOpen(tVoid);
tU32  u32OedtADR3CtrlMaxOpenCheckOpen(tVoid);
tU32  u32OedtADR3CtrlDoubleClose(tVoid);
tU32  u32OedtADR3CtrlInvalidIoControl(tVoid);
tU32  u32ADR3CtrlMPCloseTest(tVoid);
tU32  u32ADR3CtrlMPOpenTest(tVoid);
tU32  u32ADR3CtrlMPRegistrCBTest(tVoid);
tU32  u32ADR3CtrlMPResetSPITest(tVoid);
tU32  u32OedtADR3CtrlSPIModResetCheck(tVoid);
//EOF
#endif // OEDT_ADR3CTRL_TESTFUNC_HEADER