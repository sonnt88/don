																				/******************************************************************************
 *FILE         : oedt_Host_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file has the function declaration for Host test cases. 
 *                              
 *AUTHOR       : Martin Langer (CM-AI/PJ-CF33)
 *
 *COPYRIGHT    : (c) 2011  Bosch Car Multimedia
 *
 *HISTORY      : 22.09.11  Initial version
 *
 *****************************************************************************/


#ifndef OEDT_HOST_TESTFUNCS_HEADER
#define OEDT_HOST_TESTFUNCS_HEADER

/* Test case function prototypes */
tU32 u32HostOpenDev(void);

#endif
