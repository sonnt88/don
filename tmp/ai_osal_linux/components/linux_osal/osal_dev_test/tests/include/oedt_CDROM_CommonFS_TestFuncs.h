/******************************************************************************
 * FILE				: oedt_CDROM_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT		: OEDT_FrmWrk 
 *
 * DESCRIPTION		: This file defines the macros and prototypes for the 
 *					  filesystem test cases for the CDROM  
 *                          
 * AUTHOR(s)		:  Sriranjan U (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * 	Date			|							|	Author & comments
 * --.--.--			|	Initial revision		|	------------
 * 01 Dec, 2008		|	version 1.0				|	Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Mar,2009      |	version 1.1				|	Shilpa Bhat (RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 24 Feb, 2010     | version 1.2	    	    |  Updated the Device name 
 *				    |						    |  Anoop Chandran (RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 02 June, 2010   | version 1.3     |  Added new OEDT u32CDROM_CommonFSCopyDirToFFS1()
 *                |                  |  Anoop Chandran
 * ------------------------------------------------------------------------------*/
#ifndef OEDT_CDROM_COMMON_FS_TESTFUNCS_HEADER
#define OEDT_CDROM_COMMON_FS_TESTFUNCS_HEADER
#ifdef SWITCH_OEDT_CDROM
#define OEDTTEST_C_STRING_DEVICE_CDROM "/dev/cdrom"
#else
#define OEDTTEST_C_STRING_DEVICE_CDROM OSAL_C_STRING_DEVICE_CDROM
#endif
#define OEDT_C_STRING_CDROM_DIR1 OEDTTEST_C_STRING_DEVICE_CDROM"/TestCD/DreamTheatre"
#define OEDT_C_STRING_CDROM_NONEXST OEDTTEST_C_STRING_DEVICE_CDROM"/Invalid"
#define OEDT_C_STRING_FILE_INVPATH_CDROM OEDTTEST_C_STRING_DEVICE_CDROM"/Dummydir/Dummy.txt"
#define OEDT_COPY_DIR_PATH OEDTTEST_C_STRING_DEVICE_CDROM"/TestCD/00_first_folder"
//#define OEDT_CDROM_COMNFILE OEDTTEST_C_STRING_DEVICE_CDROM"/common.txt"
#define	CREAT_FILE_PATH_CDROM	OEDTTEST_C_STRING_DEVICE_CDROM"/Testf1.txt" 
#define CDROM_TEXT_FILE			OEDTTEST_C_STRING_DEVICE_CDROM"/TestCD/00_first_folder/00_cd_test_firstfile.dat"
#define CDROM_TEXT_LAST			OEDTTEST_C_STRING_DEVICE_CDROM"/TestCD/ZZ_last/last_00/last_01/last_02/last_03/last_04/last_06/99_cd_test_lastfile.dat"
#define CDROM_MP3_FILE			OEDTTEST_C_STRING_DEVICE_CDROM"/TestCD/Creed/Weathered/01-Creed-bullets.mp3"

#define OEDT_C_STRING_DEVICE_CDROM_ROOT OEDTTEST_C_STRING_DEVICE_CDROM"/"
	 											  
/* Function Prototypes for the functions defined in Oedt_FileSystem_TestFuncs.c */
tU32 u32CDROM_CommonFSOpenClosedevice(tVoid );
tU32 u32CDROM_CommonFSOpendevInvalParm(tVoid );
tU32 u32CDROM_CommonFSReOpendev(tVoid );
tU32 u32CDROM_CommonFSOpendevDiffModes(tVoid );
tU32 u32CDROM_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32CDROM_CommonFSOpenClosedir(tVoid );
tU32 u32CDROM_CommonFSOpendirInvalid(tVoid );
tU32 u32CDROM_CommonFSCreateDelDir(tVoid );
tU32 u32CDROM_CommonFSGetDirInfo(tVoid );
tU32 u32CDROM_CommonFSOpenDirDiffModes(tVoid );
tU32 u32CDROM_CommonFSReOpenDir(tVoid );
tU32 u32CDROM_CommonFSFileCreateDel(tVoid );
tU32 u32CDROM_CommonFSFileOpenClose(tVoid );
tU32 u32CDROM_CommonFSFileOpenInvalPath(tVoid );
tU32 u32CDROM_CommonFSFileOpenInvalParam(tVoid );
tU32 u32CDROM_CommonFSFileReOpen(tVoid );
tU32 u32CDROM_CommonFSFileRead(tVoid );
tU32 u32CDROM_CommonFSGetPosFrmBOF(tVoid );
tU32 u32CDROM_CommonFSGetPosFrmEOF(tVoid );
tU32 u32CDROM_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32CDROM_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32CDROM_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32CDROM_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32CDROM_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32CDROM_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32CDROM_CommonFSGetFileCRC(tVoid );
tU32 u32CDROM_CommonFSReadAsync(tVoid);
tU32 u32CDROM_CommonFSLargeReadAsync(tVoid);
tU32 u32CDROM_CommonFSFileMultipleHandles(tVoid);
tU32 u32CDROM_CommonFSFileOpenCloseNonExstng(tVoid);
tU32 u32CDROM_CommonFSSetFilePosDiffOff(tVoid);
tU32 u32CDROM_CommonFSFileOpenDiffModes(tVoid);
tU32 u32CDROM_CommonFSFileReadAccessCheck(tVoid);
tU32 u32CDROM_CommonFSFileReadSubDir(tVoid);
tU32 u32CDROM_CommonFSFileThroughPutFirstFile(tVoid);
tU32 u32CDROM_CommonFSFileThroughPutSecondFile(tVoid);
tU32 u32CDROM_CommonFSFileThroughPutMP3File(tVoid);
tU32 u32CDROM_CommonFSLargeFileRead(tVoid);
tU32 u32CDROM_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32CDROM_CommonFSReadMultiThread(tVoid);
tU32 u32CDROM_CommonFSCopyDirToFFS1(tVoid);

#endif //OEDT_CDROM_COMMON_FS_TESTFUNCS_HEADER
