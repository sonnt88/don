/******************************************************************************
 *FILE         : oedt_flashblock_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file has the function declaration for fast flash driver test cases. 
 *               
 *AUTHOR       : Andrea Bueter
 *
 *COPYRIGHT    : (c) 2007 TMS GmbH
 *
 *HISTORY      : 05.07.10 .Initial version
 *
 *****************************************************************************/

#ifndef OEDT_FLASHBLOCK_TESTFUNCS_HEADER
#define OEDT_FLASHBLOCK_TESTFUNCS_HEADER

tU32 u32FlashblockOpenClose(void);
tU32 u32FlashblockOpenWithInvalidParam(void);
tU32 u32FlashblockCloseWithInvalidParam(void);
tU32 u32FlashblockOpenCloseSeveralTimes(void);
tU32 u32FlashblockWriteRead_FFD(void);
tU32 u32FlashblockWriteRead_KDS(void);
tU32 u32FlashblockWriteReadInvalidParam(void);
tU32 u32FlashblockWriteReadIfClose(void);
tU32 u32FlashblockGetSizeData_FFD(void);
tU32 u32FlashblockGetSizeData_KDS(void);;
tU32 u32FlashblockGetSizeDataInvalidParam(void);
tU32 u32FlashblockGetSizeIfClose(void);
tU32 u32FlashblockErase_FFD(void);
tU32 u32FlashblockErase_KDS(void);
tU32 u32FlashblockEraseInvalidParam(void);
tU32 u32FlashblockEraseIfClose(void);

#endif



