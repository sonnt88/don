/************************************************************************
| FILE:         oedt_IncComm_TestFuncs.h
| PROJECT:      platform
| SW-COMPONENT: OEDT_FrmWrk
|------------------------------------------------------------------------------
| DESCRIPTION:  Regression tests for INPUT_INC driver.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 14.08.14  | Initial version            | shg7kor 
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#include "osal_if.h"

#ifndef OEDT_INC_COMM_TESTFUNCS_HEADER
#define OEDT_INC_COMM_TESTFUNCS_HEADER 

tU32 u32IncCommSccV1(tVoid);
tU32 u32IncCommSccV2(tVoid);
tU32 u32IncCommSccV3(tVoid);
tU32 u32IncCommSccV4(tVoid);
tU32 u32IncCommSccV5(tVoid);
tU32 u32IncCommSccV6(tVoid);
tU32 u32IncCommSccV7(tVoid);
tU32 u32IncCommSccV8(tVoid);
tU32 u32IncCommSccV9(tVoid);
tU32 u32IncCommSccV10(tVoid);
tU32 u32IncCommSccV11(tVoid);
tU32 u32IncCommSccV12(tVoid);
tU32 u32IncCommSccV13(tVoid);
tU32 u32IncCommSccV14(tVoid);
tU32 u32IncCommSccV15(tVoid);
tU32 u32IncCommSccV16(tVoid);
tU32 u32IncCommSccV17(tVoid);
tU32 u32IncCommSccV18(tVoid);
tU32 u32IncCommSccV19(tVoid);
tU32 u32IncCommSccV20(tVoid);
tU32 u32IncCommSccV21(tVoid);
tU32 u32IncCommSccV22(tVoid);
tU32 u32IncCommSccV23(tVoid);
tU32 u32IncCommSccV24(tVoid);
tU32 u32IncCommSccV25(tVoid);
tU32 u32IncCommAdrV1(tVoid);
tU32 u32IncCommAdrV2(tVoid);
tU32 u32IncCommAdrV3(tVoid);
tU32 u32IncCommAdrV4(tVoid);
tU32 u32IncCommAdrV5(tVoid);
tU32 u32IncPerformanceScc(tVoid);
tU32 u32IncPerformanceAdr(tVoid);

#endif
