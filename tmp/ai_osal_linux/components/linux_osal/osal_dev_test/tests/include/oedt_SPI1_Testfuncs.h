/******************************************************************************
 *FILE         : oedt_SPI_Testfuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function declarations for the SPI device.
 *            
 *AUTHOR       : Haribabu Sannapaneni(RBIN/ECM1)
 *
 *HISTORY      : Jun 02 2006  Version 1.0 
 *               - Initial Revision.
 *			   : Oct 03 2006  Version 1.1
 *               - Added more test cases by Anoop Krishnan ( RBIN - ECM1 ) 
 *             : November 6, 2006  Version 1.2
 *				 -  New test cases written based on comments from Mr.Diesner 
 *                  Tinoy Mathews(RBIN/ECM1)
 *			   :December 28, 2006 Version  1.3
                   - Added new test case prototypes

 *			   :May 3rd,2007 Version  1.4,
                -Prototypes of functions which make use of macro OSAL_C_S32_IOCTRL_SPI_SPI_SYNC_START  
				have been removed
				   - Kishore Kumar. R
			   :May 10th,2007 Version 1.5,
			    - Added Prototype for GPIO Dev removal.

			   :Sept 25, 2007, Version 1.6,
				- Added INIT_VAL Macro, Updated Prototypes for 
				  TunerICLockUnlock cases 
				   - Tinoy Mathews
			   :June 18, 2008 , version 1.7,
			   - Commented testcase u32SPI1_RadioUnit_ADR_TranceiveTunerIC as its not
			   implemented in driver
			   : March 25, 2009, version 1.8
			   - Removed lint and compiler warnings by Sainath Kalpuri (RBEI/ECF1)
			   : Apr 22, 2009, version 1.9
			   - Removed lint and compiler warnings by Sainath Kalpuri (RBEI/ECF1)
 ******************************************************************************/
#ifndef OEDT_SPI_TESTFUNCS_H
#define OEDT_SPI_TESTFUNCS_H

#define OEDT_SPI1_KERNEL_MODE   0  /* change to "1" if you are testing in Kernel mode */ 
#define INVAL_SIZE			   -1
#define ZERO_SIZE				0
#define OEDT_SPI1_INVAL_PARAM  -1
#define OEDT_SPI1_INIT_VAL      0x0000


/*Non test(Support) Functions*/
tVoid Callback(UW dintno_ui32);
tU32  CloseDeviceGPIOSPI1(const OSAL_tIODescriptor *hDeviceGPIO, const OSAL_tIODescriptor *hDeviceSPI1);
tBool CallbackCheck(OSAL_tEventHandle handleEvent, OSAL_tEventMask  maskEvent, OSAL_tMSecond timout);
tBool bCreEvent(OSAL_tEventHandle* phandleEvent,tCString coszName);
tU32 CS_to_Output( OSAL_tIODescriptor *hDeviceGPIO );
tU32 Reset_to_Output( OSAL_tIODescriptor *hDeviceGPIO );
tU32 Request_to_Input( OSAL_tIODescriptor *hDeviceGPIO );
tU32 GPIO_DevCreate (void);
tU32 GPIO_DevRemove(void);


/*Open device functions*/
tU32 u32SPI1_RadioUnit_ADR_OpenDevice(void);
tU32 u32SPI1_RadioUnit_ADR_OpenDevInvalParam(void);
tU32 u32SPI1_RadioUnit_ADR_ReOpenDev(void);
tU32 u32SPI1_RadioUnit_ADR_OpenCloseDevDiffModes(void);

/*Close device functions*/
tU32 u32SPI1_RadioUnit_ADR_CloseDevice(void);
tU32 u32SPI1_RadioUnit_ADR_CloseDevInvalParam(void);
tU32 u32SPI1_RadioUnit_ADR_CloseDevAlreadyClosed(void);

/*Control functions*/
tU32 u32SPI1_RadioUnit_ADR_GetVersion(void);
tU32 u32SPI1_RadioUnit_ADR_LockUnlockForTunerICs(void);
tU32 u32SPI1_RadioUnit_ADR_UnlockForTunerICsInval(void);
//tU32 u32SPI1_RadioUnit_ADR_TranceiveTunerIC(void);

#if 0/*Not Implemented in driver*/
tU32 u32SPI1_RadioUnit_ADR_GetPayloadWritebufferMaxSize(void);
tU32 u32SPI1_RadioUnit_ADR_GetPayloadReadbufferMaxSize(void);
tU32 u32SPI1_RadioUnit_ADR_SwitchModeGetEventMaskAndHandle(void);
tU32 u32SPI1_RadioUnit_ADR_ModePlainRead(void);
#endif/*Not Implemented in driver*/

tU32 u32SPI1_RadioUnit_ADR_SetAdvancedMode(void);

/*Negative test cases*/
tU32 u32SPI1_RadioUnit_ADR_Read_InvalSize(void);
tU32 u32SPI1_RadioUnit_ADR_Read_InvalDev(void);
tU32 u32SPI1_RadioUnit_ADR_Write_InvalSize(void);
tU32 u32SPI1_RadioUnit_ADR_Write_InvalDev(void);
tU32 u32SPI1_RadioUnit_ADR_WriteGreat(void);	           //28
tU32 u32SPI1_RadioUnit_ADR_ReadGreat(void);			   //29	

/*Positive Read/Write test cases*/
tU32 u32SPI1_RadioUnit_ADR_Read(void);
tU32 u32SPI1_RadioUnit_ADR_Write(void);

#if OEDT_SPI1_KERNEL_MODE
/*SPI - ADR test cases*/
tU32 u32_Set_CS_Reset_Request_to_IO_mode_ADR( tVoid );
tU32 u32Open_Connection2ADR(tVoid);
#endif

/* open close test functions for SPI CAP*/
tU32 u32SpiCap_DrvOpenClose(void);
tU32 u32SpiCap_DrvOpenInvalParam(void); 
tU32 u32SpiCap_DrvOpenNullParam(void); 
tU32 u32SpiCap_DrvCloseInvalParam(void);
tU32 u32SpiCap_DrvReOpen(void); 
tU32 u32SpiCap_DrvReClose(void);
tU32 u32SpiCap_DrvSetCallback(void);
tU32 u32SpiCap_DrvSetCallbackInvalParam(void);
tU32 u32SpiCap_DrvSetCallbackAfterDevClose(void);
tU32 u32SpiCap_DrvTransceiveNoCallback(void);
tU32 u32SpiCap_DrvTransceiveNoCallbackInvalParam(void);
tU32 u32SpiCap_DrvTransceiveNoCallbackAfterDevClose(void);
tU32 u32SpiCap_DrvTransceiveNoCallbackCheckRetry(void);
tU32 u32SpiCap_DrvTransceiveCallback(void);



#endif	  /* OEDT_SPI_TESTFUNCS_H */
