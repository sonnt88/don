/**********************************************************FileHeaderBegin******
 *
 * FILE:        oedt_priority_sealing.h
 *
 * CREATED:     2005-11-19
 *
 * AUTHOR:      Ulrich Schulz / TMS
 *              Matthias Thoemel / Blaupunkt
 *
 * DESCRIPTION:
 *      OEDT tests for priority inversion problem and solution
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: Blaupunkt
 *HISTORY      :Initial Revision.
 *      		Ported by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
                for ADIT platform
				Version - 1.1

 **********************************************************FileHeaderEnd*******/
/* TENGINE Header */
#ifndef OEDT_PRIORITY_CEILING_TESTFUNC_HEADER
#define OEDT_PRIORITY_CEILING_TESTFUNC_HEADER

tU32 oedt_priority_ceiling(void);
tS32 oedt_prio_change_test(void);

#endif //OEDT_MEMORY_TESTFUNC_HEADER


