/******************************************************************************
 *FILE         : oedt_Audio_TestFuncs
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for testiing the
 *                    audio 
 *
 *AUTHOR       : Bernd Schubart, 3SOFT
 *
 *COPYRIGHT    : (c) 2005 Blaupunkt Werke GmbH
 *
 *HISTORY:       25.07.05  3SOFT-Schubart
 *               Initial Revision.
 *
 *****************************************************************************/

#ifndef OEDT_AUDIO_TESTFUNCS_H_
#define OEDT_AUDIO_TESTFUNCS_H_

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

#define OEDT_STRING_DEVICE_ACOUSTICIN      "/dev/acousticin"
#define OEDT_STRING_DEVICE_ACOUSTICOUT     "/dev/acousticout"

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/
tU32 u32_oedt_audio_test_wav_out_codec(tVoid);
tU32 u32_oedt_audio_test_mp3_out_codec(tVoid);
tU32 u32_oedt_audio_test_voice_out_codec(tVoid);
tU32 u32_oedt_audio_cyrrus_setup(tVoid);
tU32 u32_oedt_audio_test_open_close_devs(void);
#endif /* OEDT_AUDIO_TESTFUNCS_H_ */

