#ifndef OEDT_FS_DEV_TESTFUNCS_HEADER
#define OEDT_FS_DEV_TESTFUNCS_HEADER


#define OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS		    "/dev/root/var/opt/bosch"
#define OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS2		  "/dev/root/var/opt/bosch"
#define OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS3		  "/dev/root/var/opt/bosch/dynamic/ffs"
#define OEDTTEST_FS_SYSTEM_DEVICE_FFS_FFS4		  "/dev/root/var/opt/bosch"
#define OEDTTEST_FS_SYSTEM_DEVICE_CARD			    "/dev/root/shared/cryptnav"
#define OEDTTEST_FS_SYSTEM_DEVICE_CRYPT_CARD	  "/dev/root/shared/cryptnav/cryptnav"
#define OEDTTEST_FS_SYSTEM_DEVICE_DEV_MEDIA		  "/dev/root/dev/media"
#define OEDTTEST_FS_SYSTEM_DEVICE_CRYPTNAVROOT	"/dev/root/var/opt/bosch/navdata"
#define OEDTTEST_FS_SYSTEM_DEVICE_CRYPTNAV		  "/dev/root/var/opt/bosch/navdata/cryptnav"


/* testcases */
tU32 u32fs_dev_check_system_devices(tVoid);

#endif
