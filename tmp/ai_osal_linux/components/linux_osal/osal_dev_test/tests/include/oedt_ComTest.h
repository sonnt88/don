/******************************************************************************
|FILE         : oedt_ComTest.h
|SW-COMPONENT : OEDT_FrmWrk 
|DESCRIPTION  : This file contains definitions for the OEDT communcation test
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          | 				          | Author	& comments
| --.--.--      | Initial revision        | -------, -----
| 17 Oct 2008   |  version 1.0            | Matthias Weise, initial version
|------------------------------------------------------------------------------
|*****************************************************************************/

#ifndef OEDT_COMTEST_H
#define OEDT_COMTEST_H

/******************************************************************
 * Includes                                                       *
 ******************************************************************/

/******************************************************************
 * Defines                                                       *
 ******************************************************************/
#define THREAD_STACKSIZE                 100000
#define THREAD_PRITORITY                 80

#define CCA_C_U16_APP_CT_MEASURE         (tU16)0x0200
#define CCA_C_U16_APP_CT_ECHO            (tU16)0x0201
#define CCA_C_U16_SRV_COMTEST            (tU16)0x0300

#define CCA_C_U16_FCT_INVALID            (tU16)0x0000
#define CCA_C_U16_FCT_START              (tU16)0x0001
#define CCA_C_U16_FCT_END                (tU16)0x0002
#define CCA_C_U16_FCT_DATA               (tU16)0x0003
#define CCA_C_U16_FCT_ECHO               (tU16)0x0004

#define CSFW_COMTEST_C_U16_API_CODE      (tU16)0x0200
#define CSFW_COMTEST_ECHO_C_U16_API_CODE (tU16)0x0201
#define CSFW_COMTEST_C_U16_OID_TEST      (tU8)0x30

#define CSFW_OPTYPE_INVALID              (tU8)0x00
#define CSFW_OPTYPE_START                (tU8)0x01
#define CSFW_OPTYPE_END                  (tU8)0x02
#define CSFW_OPTYPE_DATA                 (tU8)0x03
#define CSFW_OPTYPE_ECHO                 (tU8)0x04

#define MAX_MSG_LENGTH                   4096
#define NO_OF_MSGSIZES                   12
#define NO_OF_MSGCOUNT                   3
#define MAX_MSG                          10
#define MSG_PRIO                         2

#define START_MSG                        0xFE
#define END_MSG                          0xFF

// Event mask for "test finished" signal
extern OSAL_tEventMask    hEvMask;
// Handle for "test finished" signal
extern OSAL_tEventHandle  hEvPerfTest;

// Number of messages to send in one test loop
extern const tU32 u32NrOfMsg[];
// Size of message to be sent in one test loop
extern const tU32 u32SizeOfMsg[];
// Option constant for bGetPerfCounter
extern tU32 perfCounterOption;


/******************************************************************
 * Function prtotypes                                             *
 ******************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

extern tBool bGetPerfCounter(tU64* pu64Counter,tU32* pu32Option);
extern tU32 u32ComPerfTest_OSALMailboxPerf(void);
extern tU32 u32ComPerfTest_MoccaPerf(void);
extern tU32 u32ComPerfTest_CALSimplePerf(void);
extern tU32 u32ComPerfTest_CALReliablelePerf(void);
extern tU32 u32ComPerfTest_CFWPerf(void);
extern tU32 u32ComPerfTest_CCAPerf(void);
extern tU32 u32ComPerfTest_CSFWPerf(void);

#ifdef __cplusplus
}
#endif

#endif // OEDT_COMTEST_H
