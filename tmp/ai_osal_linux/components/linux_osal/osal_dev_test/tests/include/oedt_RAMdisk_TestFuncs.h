/******************************************************************************
| FILE:			oedt_RAMdisk_TestFuncs.h
| PROJECT:      TE_Plattform_INT_XX  (RBIN Sub Project ID: 70705_T_OEDT)
| SW-COMPONENT: RAMDisk
|------------------------------------------------------------------------------
| DESCRIPTION:  This is the header file for OEDT Test for RAMDisk
|  
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          | 				               | Author	& comments
| --.--.--      | Initial revision             | -------, -----
|20 April 2006  |  version 1.0                 |Haribabu Sannapaneni(RBIN/ECM1)
|23 Oct 2007    |  version 1.1                  |Updated by Pankaj Kumar (RBIN/EDI3)
|               |                              |Added new test cases
|				|							   |
|19 Nov 2007    |  version 1.2                 |Updated by Anoop Chandran (RBIN/EDI3)
|               |                              |Removed test cases which using
|				|							   |invalid handle 
|				|							   |from previous version ver 1.1
|               |                              |
|10, Jan, 2008  |  version 1.3				   |Haribabu Sannapaneni(RBIN/ECM1)
|               |                              |Added new test case:
|				|							   |File name with Unicode
|               |                              |charecters.
|               |                              |
|14,Feb 2008    |  version 1.4                 |Haribabu Sannapaneni(RBIN/ECM1)
|               |                              |Removed some Invalid test cases.
|               |                              |
|               |                              |
|19,Mar 2009    |  version 1.5				   |Shilpa Bhat (RBIN/ECF1)
|               |                              |Lint Removal, Updated Generate_File_Name
|			    |						       |filename to RAMDisk_Generate_File_Name
|------------------------------------------------------------------------------
|*****************************************************************************/

#ifndef OEDT_RAMDISK_TESTFUNCS_HEADER
#define OEDT_RAMDISK_TESTFUNCS_HEADER

/* RAMdisk related Test functions prototypes*/

/*Helper Functions*/
extern tU32 u32RAMDiskCreateCommonFile(void);
extern tU32 u32RAMDiskRemoveCommonFile(void);
extern tVoid RAMDisk_Generate_File_Name(tChar* ptr,tU32 len,tU32 limit);
extern tU32 OedtRamdiskCreateEvent( OSAL_tEventHandle * hEventHandle );
/*Helper Functions*/

extern tU32 u32RAMDiskOpenDevice(void);
extern tU32 u32RAMDiskReOpenDev(void);
extern tU32 u32RAMDiskOpenCloseDevDiffModes(void);
extern tU32 u32RAMDiskCloseDevice(void);
extern tU32 u32RAMDiskCloseDevInvalParam(void);
extern tU32 u32RAMDiskOpenDir(void);
extern tU32 u32RAMDiskOpenNonExstngDir(void);
extern tU32 u32RAMDiskOpenDirDiffModes(void);
extern tU32 u32RAMDiskReOpenDir(void);
extern tU32 u32RAMDiskCloseDir(void);
extern tU32 u32RAMDiskCloseInvalDir(void);
extern tU32 u32RAMDiskMkDir(void);
extern tU32 u32RAMDiskMultiMkDir(void);
extern tU32 u32RAMDiskMkDirMultiTimes(void);
extern tU32 u32RAMDiskMkDirInvalName(void);
extern tU32 u32RAMDiskMkDirInvalPath(void);
extern tU32 u32RAMDiskRmDir(void);
extern tU32 u32RAMDiskRmNonExstngDir(void);
extern tU32 u32RAMDiskRmNonEmptyDir(void);
extern tU32 u32RAMDiskRmFileUsingRmDir(void);
extern tU32 u32RAMDiskGetDirInfo(void);
extern tU32 u32RAMDiskGetDirInfoInvalParam(void);
extern tU32 u32RAMDiskFileCreate(void);
extern tU32 u32RAMDiskFileCreateDiffModes(void);
extern tU32 u32RAMDiskMultiFileCreation(void);
extern tU32 u32RAMDiskFileCreateInvalName(void);
extern tU32 u32RAMDiskFileCreateInvalPath(void);
extern tU32 u32RAMDiskFileCreateLongName(void);
extern tU32 u32RAMDiskFileCreateDiffExtn(void);
extern tU32 u32RAMDiskFileCrtOpnClseNoExtn(void);
extern tU32 u32RAMDiskFileCreateInvalExtn(void);
extern tU32 u32RAMDiskFileDel(void);
extern tU32 u32RAMDiskFileDelNonExstng(void);
extern tU32 u32RAMDiskFileDelDiffExtn(void);
extern tU32 u32RAMDiskFileDelWithoutClose(void);
extern tU32 u32RAMDiskFileOpen(void);
extern tU32 u32RAMDiskFilOpenNonExstng(void);
extern tU32 u32RAMDiskFileOpenInvalPath(void);
extern tU32 u32RAMDiskFileOpenInvalName(void);
extern tU32 u32RAMDiskFileOpenDiffModes(void);
extern tU32 u32RAMDiskFileOpenDiffExtn(void);
extern tU32 u32RAMDiskFileClose(void);
extern tU32 u32RAMDiskFileCloseNonExstng(void);
extern tU32 u32RAMDiskFileCloseDiffExtns(void);
extern tU32 u32RAMDiskFileRead(void);
extern tU32 u32RAMDiskFileReadAccessCheck(void);
extern tU32 u32RAMDiskFileReadSubDir(void);
extern tU32 u32RAMDiskLargeFileRead(void);
extern tU32 u32RAMDiskFileWrite(void);
extern tU32 u32RAMDiskFileWriteAccessCheck(void);
extern tU32 u32RAMDiskLargeFileWrite(void);
extern tU32 u32RAMDiskFileWriteSubDir(void);
extern tU32 u32RAMDiskFileWritePredefined(void);
extern tU32 u32RAMDiskGetPosFrmBOF(void);
extern tU32 u32RAMDiskGetPosFrmBOFNonExstng(void);
extern tU32 u32RAMDiskGetPosFrmEOF(void);
extern tU32 u32RAMDiskGetPosFrmEOFNonExstng(void);
extern tU32 u32RAMDiskSetPosDiffOff(void);
extern tU32 u32RAMDiskSetPosNonExstng(void);
extern tU32 u32RAMDiskReadFrmOffFrmBOF(void);
extern tU32 u32RAMDiskReadBeyondEOF(void);
extern tU32 u32RAMDiskReadFrmOffFrmEOF(void);
extern tU32 u32RAMDiskReadFrmEvryNFrmBOF(void);
extern tU32 u32RAMDiskReadFrmEvryNFrmEOF(void);
extern tU32 u32RAMDiskWriteFrmOff(void);
extern tU32 u32RAMDiskWriteFrmEOF(void);
extern tU32 u32RAMDiskStringSearch(void);
extern tU32 u32RAMDiskGetFileCRC(void);
extern tU32 u32RAMDiskReadAsync(void);
extern tU32 u32RAMDiskLargeReadAsync(void);
extern tU32 u32RAMDiskWriteAsync(void);
extern tU32 u32RAMDiskLargeWriteAsync(void);
extern tU32 u32RAMDiskRenameFile(void); /*Start of  new IO Control*/
extern tU32 u32RAMDiskRemoveDir(void);
extern tU32 u32RAMDiskCopyDir(void);
extern tU32 u32RAMDiskFileUnicodeName(void); /*End  of  new IO Control*/
extern tU32 u32RAMDiskMultipleHandles(void);
extern tU32 u32RAMDiskMultipleFileCreation(void);
extern tU32 u32RAMDiskWriteOddBuffer(void);
extern tU32 u32RAMDiskFilePtrTest(void);
extern tU32 u32RAMDiskWriteWithInvalidSize(void);
extern tU32 u32RAMDiskWriteInvalidBuffer(void);
extern tU32 u32RAMDiskWriteStepByStep(void);
extern tU32 u32RAMDiskGetFreeSpace(void);
extern tU32 u32RAMDiskGetTotalSpace(void);
extern tU32 u32TestRamdiskPerformance(tVoid);
tU32  u32RAMDiskWriteFile2Threads(tVoid);


#endif 


/* end of file */

