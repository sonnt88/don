/******************************************************************************
 *FILE         : oedt_GPS_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file has the function delcalartions for test functionsses in the GPS 
 *               device for OEDT mode
 *               
 *               
 *
 *AUTHOR       : Ravindran P (CM-DI/ESA)
 *
 *COPYRIGHT    : (c) 2006 Blaupunkt GmbH
 *
 *HISTORY      :  Initial version
 *
 *****************************************************************************/

#ifndef OEDT_GPS_TESTFUNCS_HEADER
#define OEDT_GPS_TESTFUNCS_HEADER
/*****************************************************************
| function decalaration (scope: Local) 
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/


/*Test cases*/

tU32 u32GpsBasic_OpenReadDevice( void ); 
tU32 u32GpsInterfaceCheck( void );
tU32 u32GpsDurationTimeFix( void);
tU32 u32GpsDuration2DFix_ColdStart( void);

tU32  u32GpsDuration3DFix_ColdStart( void );
tU32  u32GpsDuration2DFix_WarmStart( void );
tU32  u32GpsDuration3DFix_WarmStart( void );


tU32  u32GpsRegressionTest(  void );
tU32  u32GpsGetPersistentDataForWamBoot( void );
tU32  u32GpsGetPinToggleFrontendTest( void );
tU32  u32GpsProductionTest( void );
tU32  u32TestCriticalVoltageColdStart( void );
tU32 u32TestCriticalVoltageWarmStart( void );
tU32 u32Check2DFixAfterSetHints( void ); 
tU32 u32GpsDeviceOpenClosetest(void);
tU32 u32GpsDeviceAntennaStatustest(void);
/*Test cases*/


#endif


