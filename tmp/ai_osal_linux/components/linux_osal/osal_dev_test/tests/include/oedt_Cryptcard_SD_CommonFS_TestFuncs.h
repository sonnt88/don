/******************************************************************************
 * FILE         : oedt_Cryptcard_SD_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file contains function declarations for the CARD device.
 *            
 * AUTHOR(s)    : Martin Langer (CM-AI/PJ-CF33)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                  | Author & comments
 * --.--.--       | Initial revision | ------------
 * 06 Jul, 2012   | version 1.0      | Martin Langer (CM-AI/PJ-CF33)
 *****************************************************************************/
#ifndef OEDT_CRYPTCARD_SD_COMMON_FS_TESTFUNCS_HEADER
#define OEDT_CRYPTCARD_SD_COMMON_FS_TESTFUNCS_HEADER

#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD  "/dev/cryptcard"
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD2 "/dev/cryptcard2"


/* Function Prototypes for the functions defined in Oedt_FileSystem_TestFuncs.c */
tU32 u32Cryptcard_SD_CommonFSOpenClosedevice(tVoid );
tU32 u32Cryptcard_SD_CommonFSOpendevInvalParm(tVoid );
tU32 u32Cryptcard_SD_CommonFSReOpendev(tVoid );
tU32 u32Cryptcard_SD_CommonFSOpendevDiffModes(tVoid );
tU32 u32Cryptcard_SD_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32Cryptcard_SD_CommonFSOpenClosedir(tVoid );
tU32 u32Cryptcard_SD_CommonFSOpendirInvalid(tVoid );
tU32 u32Cryptcard_SD_CommonFSGetDirInfo(tVoid );
tU32 u32Cryptcard_SD_CommonFSOpenDirDiffModes(tVoid );
tU32 u32Cryptcard_SD_CommonFSReOpenDir(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileCreateDel(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileOpenClose(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileOpenInvalPath(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileOpenInvalParam(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileReOpen(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileRead(tVoid );
tU32 u32Cryptcard_SD_CommonFSGetPosFrmBOF(tVoid );
tU32 u32Cryptcard_SD_CommonFSGetPosFrmEOF(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32Cryptcard_SD_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32Cryptcard_SD_CommonFSGetFileCRC(tVoid );
tU32 u32Cryptcard_SD_CommonFSReadAsync(tVoid);
tU32 u32Cryptcard_SD_CommonFSLargeReadAsync(tVoid);
tU32 u32Cryptcard_SD_CommonFSFileMultipleHandles(tVoid);
tU32 u32Cryptcard_SD_CommonFSFileOpenCloseNonExstng(tVoid);
tU32 u32Cryptcard_SD_CommonFSSetFilePosDiffOff(tVoid);
tU32 u32Cryptcard_SD_CommonFSFileOpenDiffModes(tVoid);
tU32 u32Cryptcard_SD_CommonFSFileReadAccessCheck(tVoid);
tU32 u32Cryptcard_SD_CommonFSFileReadSubDir(tVoid);
tU32 u32Cryptcard_SD_CommonFSFileThroughPutFile(tVoid);
tU32 u32Cryptcard_SD_CommonFSLargeFileRead(tVoid);
tU32 u32Cryptcard_SD_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32Cryptcard_SD_CommonFSReadMultiThread(tVoid);
tU32 u32Cryptcard_SD_CommonFSReadDirExt(tVoid);
tU32 u32Cryptcard_SD_CommonFSReadDirExt2(tVoid);
tU32 u32Cryptcard_SD_CommonFSReadDirExtPartByPart(tVoid);
tU32 u32Cryptcard_SD_CommonFSReadDirExt2PartByPart(tVoid);

#endif //OEDT_CRYPTCARD_SD_COMMON_FS_TESTFUNCS_HEADER
