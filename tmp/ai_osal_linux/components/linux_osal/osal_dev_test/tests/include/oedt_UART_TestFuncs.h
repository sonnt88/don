/******************************************************************************
 *FILE         : oedt_UART_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_UART_TestFuncs.c for the  
 *               UART device for the GM-GE hardware.
 *
 *AUTHOR       : Haribabu Sannapaneni (RBEI/ECM1) 
 *
 *HISTORY      : Created by Haribabu Sannapaneni (RBEI/ECM1) on 22, August,2008
				 version 0.1
				            
*******************************************************************************/

#ifndef OEDT_UART_TESTFUNCS_HEADER
#define OEDT_UART_TESTFUNCS_HEADER
#include "osal_if.h"

/* Function prototypes of functions used in file oedt_UART_TestFuns.c */

tU32 u32UARTChannelOpenClose(tVoid);
tU32 u32UARTChannelReOpen(tVoid);
tU32 u32UARTChanCloseAlreadyClosed(tVoid);
tU32 u32UARTChannelOpenCloseWithAlias(tVoid);
tU32 u32UARTGetVersion(tVoid);
tU32 u32UARTSetAndGetRsModeSettings(tVoid);
tU32 u32UARTSetAndGetRsFlowControl(tVoid);
tU32 u32UARTSetSendAndReceiveTimeOut(tVoid);
tU32 u32UARTBreakTransmission(tVoid);
tU32 u32UARTChannle1WriteRead(tVoid);
tU32 u32UARTChannleWriteReadWithDiffBaud(tVoid);

#endif
