/******************************************************************************
 *FILE         : oedt_GNSS_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmmeWork
 *
 *DESCRIPTION  : This Is The Header File Of oedt_GNSS_TestFuncs.c
 *               
 *AUTHOR       : Sanjay G(RBEI/ECF5)
 *
 *COPYRIGHT    : (C) COPYRIGHT RBEI - All Rights Reserved 
 *------------------------------------------------------------------------------
 *HISTORY      :|   Author      | Version  |     Date
 *              |   Sanjay G    |  1.0     |  30/April/2013
                |   Deepak      |  1.1     |  20/Feb/2015 
 -------------------------------------------------------------------------------
********************************************************************************/


/*GNSS Proxy OEDT Test Functions Declarations*/

tU32  u32OedtGNSSProxyBasiOpenClose(tVoid);
tU32  u32OedtGNSSProxyReOpen(tVoid);
tU32  u32OedtGNSSProxyReClose(tVoid);
tU32  u32OedtGNSSProxyBasicRead(tVoid);
tU32  u32OedtGNSSProxyReadByPassingNullBufffer(tVoid);
tU32  u32OedtGNSSProxyIOCntInterfaceCheck(tVoid);
tU32  u32OedtGNSSProxyIOCntChkInvPar(tVoid);
tU32  u32OedtGNSSProxyReOpen(tVoid);
tU32  GnssOedt_u32GetNmeaMsgList(tVoid);
tU32  GnssOedt_u32Teseo2FwUpdate(tVoid);
tU32  GnssOedt_s32TesFwUpdateStub(tVoid);
tU32  u32OedtGNSSProxySetEpoch(tVoid);
tU32  GNSSOedt_u32SatInterChk( tVoid );
tU32  GNSSOedt_u32DiagSatInterChk( tVoid );
tU32  GNSSOedt_u32SatCfgBlk200( tVoid );
tU32  GNSSOedt_u32SatCfgBlk227( tVoid );
tU32  GNSSOedt_u32SatCfgBlk200227( tVoid );
tU32  GNSSOedt_u32ChkSatCfgBlk227( tVoid );
tU32  GNSSOedt_u32ChkSatCfgBlk200227( tVoid );
tU32  u32OedtGNSSFwOpenCloseContinuously( tVoid );
tU32  u32OedtGNSSProxyInfiniteRead(tVoid);
tU32  u32OedtGNSSProxyMultipleIOOpenIOClose(tVoid);
tU32  u32OedtGNSSProxyGetEpoch(tVoid);

tU32 GnssOedt_u32Teseo3FwUpdate(tVoid);

/* GPS proxy test cases */
tU32  u32OedtGPSProxyBasiOpenClose(tVoid);
tU32  u32OedtGPSProxyBasicRead(tVoid);
tU32  u32OedtGPSProxyReOpen(tVoid);
tU32  u32OedtGPSProxyReClose(tVoid);
tU32 GnssOedt_s32TesGetCrc(tVoid);

//EOF
