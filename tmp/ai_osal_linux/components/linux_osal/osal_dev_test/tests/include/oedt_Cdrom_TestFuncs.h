/******************************************************************************
 *FILE         : oedt_Cdrom_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_Cdrom_TestFuncs.c for the  
 *               Cdrom device for the low variant hardware of GM-GE.
 *
 *AUTHOR       : Rakesh Dhanya (RBIN/EDI3) 
 *
 *HISTORY      : 25.05.07 ver 1.0  RBIN/EDI3- Rakesh Dhanya
 *		 08 June-2007 ver 1.1 - updates made based on review
										 comments given by Mr Resch Carsten
 *                06.03.08 ver 1.2  CF32- Lars Tracht    renamed to oedt_Cdrom_TestFuncs.h
 *
 *        30.07.2008 ver 1.3 - Shilpa Bhat (BRBEI/ECM1)
 *        Removed cases u32CDROMClosedevInvalParm, u32CDROMCloseDirInvalParm,
 *        u32CDROMGetInfoInvalParm, u32CDROMFileCloseInvParm , 
 *		  u32CDROMGetVersionNumberAfterDevClose, u32CDROMGetVolumeLabelAfterDevClose
 *        u32CDROMOpendevNullParm, u32CDROMOpenDirWithNULL, u32CDROMFileOpenWithNULLParam,
 *        u32CDROMDirReClose, u32CDROMSetReadErrorStatergyNormal,
 *        u32CDROMSetReadErrorStatergyStreaming 
 *        Added cases u32CDROMReadAsync, u32CDROMFileReadInvaildCount
 *        29.09.2008 ver 1.4 - Shilpa Bhat (BRBEI/ECM1)
 *        Removed testcases u32CDROMGetDirInfo, u32CDROMFirstFileRead, u32CDROMLastFileRead,
 *        u32CDROMFirstMP3Read, u32CDROMGetVolumeLabel, u32CDROMGetVolumeLabelInvalParm , 
 *        u32CDROMGetMediaInfo, u32CDROMGetMediaInfoInvalParam		        
 *        
 *        24.03.2009 ver 1.5 - Shilpa Bhat (RBEI/ECF1)
 *        Lint Removal
 *
  *****************************************************************************/
 #include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	
#include <basic.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif /*#ifndef TSIM_OSAL*/
#include <string.h>

#ifndef OEDT_CDROM_TESTFUNCS_HEADER
#define OEDT_CDROM_TESTFUNCS_HEADER

tU32 DirRead (OSAL_trIOCtrlDir* hDir);
tU32 CompareName(tPS8 Name);

/* Function Prototypes for the functions defined in Oedt_Cdrom_TestFuncs.c */
tU32 u32CDROMOpenClosedevice(void);
tU32 u32CDROMOpendevInvalParm(void);
tU32 u32CDROMOpendevDiffModes(void);
tU32 u32CDROMReOpendev(void);
tU32 u32CDROMClosedevAlreadyClosed(void);
tU32 u32CDROMOpenClosedir(void);
tU32 u32CDROMOpendirInvalid(void);
tU32 u32CDROMOpendirDiffModes(void);
tU32 u32CDROMOpendirwithFile(void);
tU32 u32CDROMReOpenDir(void);
tU32 u32CDROMCreateDir(void);
tU32 u32CDROMDelDir(void);
tU32 u32CDROMFileOpenClose(void);
tU32 u32CDROMFileOpenInvalParam(void);
tU32 u32CDROMFileOpenInvalPath(void);
tU32 u32CDROMFileOpenDiffModes(void);
tU32 u32CDROMFileOpenCloseDiffExtns(void);
tU32 u32CDROMFileReOpen(void);
tU32 u32CDROMFileReClose(void);
tU32 u32CDROMFileCreate(void);
tU32 u32CDROMFileRemove(void);
tU32 u32CDROMFileGetPosFrmBOF(void);
tU32 u32CDROMFileGetPosFrmBOFInvalParm(void);
tU32 u32CDROMFileGetPosFrmBOFAfterClosing(void);
tU32 u32CDROMFileGetPosFrmEOF(void);
tU32 u32CDROMFileGetPosFrmEOFInvalParm(void);
tU32 u32CDROMFileGetPosFrmEOFAfterClosing(void);
tU32 u32CDROMFileSetPosFrmBOF(void);
tU32 u32CDROMFileSetPosFrmBOFInvalParam(void);
tU32 u32CDROMFileSetPosFrmBOFAfterClosing(void);
tU32 u32CDROMFileReadInJumps(void);
tU32 u32CDROMFileReadWithInvalParm(void);
tU32 u32CDROMFileReadLargeData(void);
tU32 u32CDROMFileReadNegOffsetFrmBOF(void);
tU32 u32CDROMFileReadOffsetFrmEOF(void);
tU32 u32CDROMFileReadOffsetBeyondEOF(void);
tU32 u32CDROMFileReadEveryNthByteFrmBOF(void);
tU32 u32CDROMFileReadEveryNthByteFrmEOF(void);
tU32 u32CDROMFileReadLongFileName(void);
tU32 u32CDROMFileThroughPutFirstFile(void);
tU32 u32CDROMFileThroughPutLastFile(void);
tU32 u32CDROMFileThroughPutMP3File(void);
tU32 u32CDROMGetVersionNumber(void);
tU32 u32CDROMGetVersionNumberInvalParm(void);
tU32 u32CDROMInvalFuncParm2IOCTL(void);
tU32 u32CDROMReversedMedia(void);
tU32 u32CDROMInvalidMedia(void);
tU32 u32CDROMReadAsync(tVoid);
tU32 u32CDROMFileReadInvalidCount(tVoid);
#endif





