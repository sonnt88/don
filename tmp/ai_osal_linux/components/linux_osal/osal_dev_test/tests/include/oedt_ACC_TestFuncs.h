/******************************************************************************
 *FILE         : oedt_ACC_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_ACC_TestFuncs.c for the  
 *               ACC device.
 *
 *AUTHOR       : Haribabu Sannapaneni (RBEI/ECF1) 
 *
 *HISTORY      : Created by Haribabu Sannapaneni (RBEI/ECF1) on 9, March,2010
				 version 1.0
				 
				 
*******************************************************************************/


#ifndef OEDT_ACC_TESTFUNCS_HEADER
#define OEDT_ACC_TESTFUNCS_HEADER
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#define     DEFAULT_TIMEOUT                 2

/* Function prototypes of functions used in file oedt_ACC_TestFuns.c */

tU32 u32ACCDevOpenClose(tVoid);
tU32 u32ACCDevReOpen(tVoid); 
tU32 u32ACCDevCloseAlreadyClosed(tVoid); 
tU32 u32ACCDevRead(tVoid);
tU32 u32ACCDevInterfaceCheckSeq(tVoid);
tU32 u32ACCDevReadpassingnullbuffer(tVoid);
tU32 u32ACCDevIOCTRLpassingnullbuffer(tVoid);
tU32 u32ACCDevReadSeq(tVoid);
tU32 u32AccSelfTest(tVoid);
tU32 u32AccHwInfo(tVoid);
tU32 tu32AccOpnClsReg(tVoid);
tU32 u32AccCheckACCValuesSeq_x(tVoid);
tU32 u32AccCheckACCValuesSeq_y(tVoid);
tU32 u32AccCheckACCValuesSeq_z(tVoid);
tU32 u32AccGetCycleTime(tVoid) ;
#endif

