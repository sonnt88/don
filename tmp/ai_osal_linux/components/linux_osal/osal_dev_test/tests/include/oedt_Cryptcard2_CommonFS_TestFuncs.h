/******************************************************************************
 * FILE         : oedt_Cryptcard2_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT : OEDT_FrmWrk 
 *
 * DESCRIPTION  : This file contains function declarations for the cryptcard2.
 *            
 * AUTHOR(s)    : Anooj Gopi (RBEI/ECF1)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |                  | Author & comments
 * --.--.--       | Initial revision | ------------
 * 19 Apr, 2011   | version 1.0      | Anooj Gopi (RBEI/ECF1)
 *                |                  | Initial version
 *****************************************************************************/
#ifndef OEDT_CRYPTCARD2_COMMON_FS_TESTFUNCS_HEADER
#define OEDT_CRYPTCARD2_COMMON_FS_TESTFUNCS_HEADER

#define SWITCH_OEDT_CRYPTCARD2
#ifdef SWITCH_OEDT_CRYPTCARD2
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD2 "/dev/cryptcard2"
#else
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD2 OSAL_C_STRING_DEVICE_CRYPTCARD2
#endif

#define OEDT_C_STRING_CRYPTCARD2_DIR1         OEDTTEST_C_STRING_DEVICE_CRYPTCARD2"/cfg"
#define OEDT_C_STRING_CRYPTCARD2_NONEXST      OEDTTEST_C_STRING_DEVICE_CRYPTCARD2"/Invalid"
#define OEDT_C_STRING_FILE_INVPATH_CRYPTCARD2 OEDTTEST_C_STRING_DEVICE_CRYPTCARD2"/Dummydir/Dummy.txt"
#define OEDT_C_STRING_CRYPTCARD2_DAT_FILE     OEDTTEST_C_STRING_DEVICE_CRYPTCARD2"/data/data/misc/content.dat"
#define OEDT_C_STRING_DEVICE_CRYPTCARD2_ROOT  OEDTTEST_C_STRING_DEVICE_CRYPTCARD2"/"

tU32 u32Cryptcard2_CommonFSOpenClosedevice(tVoid );
tU32 u32Cryptcard2_CommonFSOpendevInvalParm(tVoid );
tU32 u32Cryptcard2_CommonFSReOpendev(tVoid );
tU32 u32Cryptcard2_CommonFSOpendevDiffModes(tVoid );
tU32 u32Cryptcard2_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32Cryptcard2_CommonFSOpenClosedir(tVoid );
tU32 u32Cryptcard2_CommonFSOpendirInvalid(tVoid );
tU32 u32Cryptcard2_CommonFSGetDirInfo(tVoid );
tU32 u32Cryptcard2_CommonFSOpenDirDiffModes(tVoid );
tU32 u32Cryptcard2_CommonFSReOpenDir(tVoid );
tU32 u32Cryptcard2_CommonFSFileCreateDel(tVoid );
tU32 u32Cryptcard2_CommonFSFileOpenClose(tVoid );
tU32 u32Cryptcard2_CommonFSFileOpenInvalPath(tVoid );
tU32 u32Cryptcard2_CommonFSFileOpenInvalParam(tVoid );
tU32 u32Cryptcard2_CommonFSFileReOpen(tVoid );
tU32 u32Cryptcard2_CommonFSFileRead(tVoid );
tU32 u32Cryptcard2_CommonFSGetPosFrmBOF(tVoid );
tU32 u32Cryptcard2_CommonFSGetPosFrmEOF(tVoid );
tU32 u32Cryptcard2_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32Cryptcard2_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32Cryptcard2_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32Cryptcard2_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32Cryptcard2_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32Cryptcard2_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32Cryptcard2_CommonFSGetFileCRC(tVoid );
tU32 u32Cryptcard2_CommonFSReadAsync(tVoid);
tU32 u32Cryptcard2_CommonFSLargeReadAsync(tVoid);
tU32 u32Cryptcard2_CommonFSFileMultipleHandles(tVoid);
tU32 u32Cryptcard2_CommonFSFileOpenCloseNonExstng(tVoid);
tU32 u32Cryptcard2_CommonFSSetFilePosDiffOff(tVoid);
tU32 u32Cryptcard2_CommonFSFileOpenDiffModes(tVoid);
tU32 u32Cryptcard2_CommonFSFileReadAccessCheck(tVoid);
tU32 u32Cryptcard2_CommonFSFileReadSubDir(tVoid);
tU32 u32Cryptcard2_CommonFSFileThroughPutFile(tVoid);
tU32 u32Cryptcard2_CommonFSLargeFileRead(tVoid);
tU32 u32Cryptcard2_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32Cryptcard2_CommonFSReadMultiThread(tVoid);
tU32 u32Cryptcard2_CommonFSReadDirExt(tVoid);
tU32 u32Cryptcard2_CommonFSReadDirExt2(tVoid);
tU32 u32Cryptcard2_CommonFSReadDirExtPartByPart(tVoid);
tU32 u32Cryptcard2_CommonFSReadDirExt2PartByPart(tVoid);

#endif //OEDT_CRYPTCARD2_COMMON_FS_TESTFUNCS_HEADER
