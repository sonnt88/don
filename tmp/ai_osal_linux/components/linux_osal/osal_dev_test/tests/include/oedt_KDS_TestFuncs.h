/******************************************************************************
 *FILE         : oedt_KDS_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for the KDS device.
 *
 *AUTHOR       : Venkateswara.N
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : 19.06.06 ver 1.0  RBIN/ECM1- Venkateswara.N
 *               Initial Revision.
 *
 *****************************************************************************/
#ifndef OEDT_KDS_TESTFUNCS_HEADER
#define OEDT_KDS_TESTFUNCS_HEADER

tU32 u32KDSDevOpen(void);
tU32 u32KDSDevOpenInvalParm(void);
tU32 u32KDSDevReOpen(void);
tU32 u32KDSDevOpenDiffModes(void);
tU32 u32KDSDevClose(void);
tU32 u32KDSDevCloseInvalParm(void);
tU32 u32KDSDevReClose(void);
tU32 u32KDSEnaDisAccessright(void);
tU32 u32KDSEnaDisAccessrightWithInvalParm(void);
tU32 u32KDSGetNextEntryID(void);
tU32 u32KDSGetNextEntryIDWithInvalParm(void);
tU32 u32KDSGetDevInfo(void);
tU32 u32KDSGetDevInfoWithInvalParm(void);
tU32 u32KDSDevInit(void);
tU32 u32KDSDeleteEntryID(void);
tU32 u32KDSDeleteEntryIDWithInvalID(void);
tU32 u32KDSReadEntry(void);
tU32 u32KDSReadEntryWithInvalID(void);
tU32 u32KDSReadEntryWithInvalParm(void);
tU32 u32KDSWriteEntry(void);
tU32 u32KDSWriteEntryWithInvalID(void);
tU32 u32KDSWriteEntryWithInvalParam(void);
tU32 u32KDSWriteReadCombine(void);
tU32 u32KDSGetDevVersion(void);
tU32 u32KDSGetDevVersionWithInvalParm(void);
tU32 u32KDSInvalMacroToIOCTL(void);
tU32 u32KDSClearAllData(void);
tU32 u32KDSGetDevVersionAfterDevclose(void);
tU32 u32KDSReWriteProtected(void);
tU32 u32KDSWriteProtectedDel(void);
tU32 u32KDSWriteEntryGreater(void);
tU32 u32KDSReadcheck(void);
tU32 u32KDSGetRemainingSize(void);
tU32 u32KDSGetRemainingSizeInval(void);
tU32 u32KDSWriteWithoutWEnable (void);
tU32 u32KDSClearWithoutWEnable(void);
tU32 u32KDSOverWrite(void);
tU32 u32KDSWriteFull(void);
tU32 u32KDSWriteFullAndBackToFlash(void);
tU32 u32KDSOpenMax(void) ;
tU32 u32KDSWriteBackToFlash(void);

#endif
