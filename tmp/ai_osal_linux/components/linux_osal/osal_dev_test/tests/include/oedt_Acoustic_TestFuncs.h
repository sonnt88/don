/******************************************************************************
 *FILE         : odt_Acoustic_TestFuncs.h
 *
 *SW-COMPONENT : ODT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for the acoustic device.
 *                also the array of function pointer for this device has been 
 *                initialised.
 *
 *AUTHOR       : Bernd Schubart, 3SOFT
 *
 *COPYRIGHT    : (c) 2005 Blaupunkt Werke GmbH
 *
 *HISTORY:       25.07.05  3SOFT-Schubart
 *               Initial Revision.
 *
 *****************************************************************************/

#ifndef ODT_ACOUSTIC_TESTFUNCS_HEADER
#define ODT_ACOUSTIC_TESTFUNCS_HEADER

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

tU32 u32AcousticoutPCMBasicTest(tVoid);
tU32 u32AcousticoutPCMCfgTest(tVoid);
tU32 u32AcousticoutPCMPlayModeTest(tVoid);
tU32 u32AcousticoutPCMMarkerTest(tVoid);
tU32 u32AcousticoutPCMStartStopAbortTest(tVoid);

#endif

