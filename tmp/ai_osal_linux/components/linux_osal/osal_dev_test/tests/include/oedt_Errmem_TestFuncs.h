/******************************************************************************
|FILE         : oedt_Errmem_TestFuncs.h
|SW-COMPONENT : OEDT_FrmWrk 
|DESCRIPTION  : This file contains header includes ,function declarations and 
|            	macros for ERRMEM device.
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          | 				          | Author	& comments
| --.--.--      | Initial revision        | -------, -----
|16 June 2006   |  version 1.0            |Narasimha Prasad Palasani(RBIN/ECM1)
|------------------------------------------------------------------------------
|			    |	                      |
|23 June 2006   |  version 2.0            |Narasimha Prasad Palasani(RBIN/ECM1)
|   		    |                         |Updated after review comments
|______________________________________________________________________________
|				|						  |
|28 Nov 2006	|  version 3.0			  |	Rakesh Dhanya (RBIN/ECM1)
|				|						  |	Inclusion of new test case.
______________________________________________________________________________
|				|						  |
|06 Dec 2006	|  version 3.1			  |	Rakesh Dhanya (RBIN/ECM1)
|				|						  |	Inclusion of new test cases.
|______________________________________________________________________________
|
|				|						  |
|25 June 2007	|  version 3.2			  |	Rakesh Dhanya (RBIN/ECM1)
|				|						  |	Inclusion of new test cases.
|______________________________________________________________________________
|
|				|						  |
|25 June 2007	|  version 3.3			  |	Anoop Chandran (RBIN/EDI3)
|______________________________________________________________________________
|
|				|						  |
|25 June 2007	|  version 3.4			  |	Anoop Chandran (RBIN/ECM1)
|				|						  |	Added new macro 
|				|						  |C_MAX_NOTIFIERS_ARRAY_SIZE
|				|						  |MAX_ENTRIES_TO_WRITE.
|______________________________________________________________________________
|
|				|						  |
|25 Mar 2009	|  version 3.5			  |	Sainath Kalpuri (RBEI/ECF1)
											Removed lint and compiler warnings
|______________________________________________________________________________
|
|				|						  |
|22 Apr 2009	|  version 3.6			  |	Sainath Kalpuri (RBEI/ECF1)
											Removed lint and compiler warnings
|______________________________________________________________________________
|
|				|						  |
|06 May 2009	|  version 3.7	  |	Sriranjan U (RBEI/ECF1)
|											Added new test case
|*****************************************************************************/

#ifndef OEDT_ERRMEM_TESTFUNCS_H
#define OEDT_ERRMEM_TESTFUNCS_H

#define OSAL_S_IMPORT_INTERFACE_GENERIC

/******************************************************************
* Header files inclusion                                          *
******************************************************************/

#include "osal_if.h"
#include "osioctrl.h" // anoop
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <time.h>
#include <string.h>
//#include <basic.h>
//#include <tk/tkernel.h>

//#include "../../TEngine_driver/LFX/tengine/saphire/lld_LFX/lld_LFX.h"
//#include "../../TEngine_driver/errormem/tengine/saphire/dev_errmem/lld_errmem.h"
/******************************************************************
* defines and macros (scope: module-local)                        * 
******************************************************************/

/* to be uncommented while testing in kernel mode :- Anoop Chandran*/
//#define ERRMEM_KERNEL

//#define OSAL_C_TRACELEVEL1  TR_LEVEL_ERRORS
#define OSAL_C_S32_IOCTRL_INVALID_DEVICE "dev/invalid" 
#define ERRMEM_INVALID_PARAM  	 0
#define C_MAX_NOTIFIERS_ARRAY_SIZE   6
#define C_MAX_NOTIFIERS              5
#define ERRMEM_FATAL_TEST			 0
#define ERR_MEM_ENTRY_DATA_START_OFFSET 14
#define ERR_MEM_MAGIC_INVALID_ENTRY	  0xABCD
//#define ERR_MEM_FLASH_START_OFFSET 0
//#define ERR_MEM_FLASH_END_OFFSET   0x60000
//#define ERR_MEM_FLASH_BLOCK_SIZE   0x20000
#define ERR_MEM_VERSION_NUMBER_OFFSET 8
#define DEV_ERRMEM_INVAL_PARAMOUNT_VERSION	((tU32)0x01234567)
#define ERR_MEM_MAGIC_BLOCK_INVALID 0xDCBAABCD
#define  OEDT_ERRMEM_INVAL_NOR_ADDRESS_WRITETO 0x009a0000
#define OEDT_ERRMEM_INVAL_NAND_ADDRESS_WRITETO 0xa0000000
#define  OEDT_ERRMEM_INVAL_NOR_ADDRESS_READFROM 0x009a0010
#define OEDT_ERRMEM_INVAL_NAND_ADDRESS_READFROM 0xa0000010
#define ERR_MEM_FLASH_START_OFFSET 0
#define ERR_MEM_FLASH_END_OFFSET   0x60000
#define ERR_MEM_FLASH_BLOCK_SIZE   0x20000
#define INVALID_ERRMEM_BLOCK 5
#define ERR_MEM_MAGIC_FULL_OFFSET 4
#define ERR_MEM_MAGIC_BLOCK_VALID 0x01111969
#define DEV_ERRMEM_PARAMOUNT_VERSION ((tU32)0x00000001)
#define ERR_MEM_END_HEADER_OFFSET 12
#define ZERO_PARAMETER 0
#define NUMBER_OF_ERRMEM_BLOCKS 3
#define ERRMEM_HEADER_MAGIC_LENGTH 4

/*macro change from ERRMEM_MAX_ENTRY_LENGTH 
to ERR_MEM_LONG_ENTRY_SIZE.
ERR_MEM_LONG_ENTRY_SIZE will avail 
in osiocrtl.h which may update in future.
by Anoop Chandran(RBIN/EDI3)  */
#define ERR_MEM_LONG_ENTRY_SIZE (ERRMEM_MAX_ENTRY_LENGTH+10) 

/* MAX size of Errmem is 393216 
(According to this macro "ERR_MEM_FLASH_END_OFFSET in dev_errmemc.c")
max size of one entry will be [ERRMEM_MAX_ENTRY_LENGTH (256)(in osioctrl.h) 
+ ERR_MEM_ENTRY_DATA_START_OFFSET(14)(in dev_errmemc.c)	]
so max entry will be 1456*/

#define MAX_ENTRIES_TO_WRITE 1456 
#define ERRMEM_MAX_OPEN_INSTANCE 256
#define MAX_WRITES 8
#define COOKIE_VALUE 0x12345678
#define THREAD_WAIT_TIME 100
#define ERRMEM_TEST_ID 0x03
#define LENGTH_OF_ENTRY 128
#define THREAD_PRIORITY 130
#define THREAD_STACK_SIZE 4096
#define BLOCK_ONE 1

//static tBool LFM_FLASH_INIT_OK=FALSE;

#define ERR_MEM_GET_LONG_ENTRY(x) (ERR_MEM_OFFSET_READ(x,0) + (ERR_MEM_OFFSET_READ(x,1) << 8) \
        + (ERR_MEM_OFFSET_READ(x,2) << 16) + (ERR_MEM_OFFSET_READ(x,3) << 24))
#define WRAPPER_FLASH_TEST_BlockErase(x) ((tS32)((LFM_FlashEraseBlock(IDERRMEM, (x))) ? OSAL_E_NOERROR : OSAL_E_IOERROR))
#define WRAPPER_FLASH_Read(x,y,z) ((tS32)((LFM_FlashRead(IDERRMEM, (y),(x),(z))) ? OSAL_E_NOERROR : OSAL_E_IOERROR))
#define WRAPPER_FLASH_Write(x,y,z) ((tS32)((LFM_FlashWrite(IDERRMEM ,(y),(x),(z))) ? OSAL_E_NOERROR : OSAL_E_IOERROR))
#define ERR_MEM_OFFSET(x,y) (((tU8 *)(x))+(y))
#define ERR_MEM_OFFSET_READ(x,y) (tU32)(*(ERR_MEM_OFFSET(x,y)))
#define WRAPPER_FLASH_BlockErase(x) ((tS32)(((FALSE)?LFM_FlashEraseBlock(IDERRMEM, (x)) : 1) ? OSAL_E_NOERROR : OSAL_E_IOERROR))
#define ERR_MEM_BLOCK_ADDR(x) (ERR_MEM_FLASH_START_OFFSET + (x)*ERR_MEM_FLASH_BLOCK_SIZE)

tBool LFM_FlashInit(void);
tBool LFM_FlashUninit(void);

typedef int ID;

typedef enum
{
 FATAL_ENTRY_TYPE, 			 
 NORMAL_ENTRY_TYPE,	
 INFO_ENTRY_TYPE,
 UNDEFINED_ENTRY_TYPE		 
}entrytype;

struct ref_param
{
	tU32 *RetVal;
	OSAL_tIODescriptor hDevPtr;
	ID ParTsk;
};

typedef enum
{
   /* Common Errors */
   EM_Result_Ok = 0,            /* Not Error Detected */
   EM_Result_Common_Error,      /* Unknown Error Detected */
   EM_Result_Parameter_Error,   /* There is an Error in the calling Parameters */
   EM_Result_Not_Initialised,   /* ERRMEM isn't initialised */
   EM_Result_Not_In_List,       /* Entry not found in ERRMEM */
   EM_Result_Buffer_To_Small,   /* Entry can't be copied in Buffer */
   EM_Result_Write_Error,       /* The Writefunction returned an Error */
   EM_Result_Full,              /* There is no space left in ERRMEM */
   EM_Result_Clear_Error,       /* The Clearfunction returned an Error */
   EM_Result_Check_Error       /* There is an Error in the ERRMEM. */
} ten_EM_Result;

typedef enum {
  IDFLASH, IDERRMEM, IDKDS, IDFLASHBLOCK, IDEND
} tLFXID;

tU32 ERR_MEM_BLOCK_ADDR_TEST(tU32 BlockAddr);
tBool LFM_FlashRead(tLFXID ID, tU32 dwLogicalFlashAdr, tU8 * pBuffer, tU32 dwLength );
tBool LFM_FlashWrite(tLFXID ID, tU32 dwLogicalFlashAdr, tU8 * pBuffer, tU32 dwLength);
tBool LFM_FlashEraseBlock(tLFXID ID, tU32 dwLogicalBlockNo);
tU32 ErrmemCompressWrite(tU32 number, const OSAL_tIODescriptor *hDevice);
trErrmemEntry* ErrmemWriteSetup(entrytype type);
ten_EM_Result fEM_Write(trErrmemEntry rEM_Entry); 

/******************************************************************
* Function prtotypes                                              *
******************************************************************/

tU32 u32ErrMemWrite(void);
tU32 u32ErrMemCheckNormal(void);
tU32 u32ErrMemCheckInvalParam(void);
tU32 u32ErrMemClear(void);
tU32 u32ErrMemClearInvalParam(void);
tU32 u32ErrMemRegUnregCallback(void);
tU32 u32ErrMemRegUnregCallbackInvalParam(void);
tU32 u32ErrMemVer(void);
tU32 u32ErrMemVerInvalParam(void);
tU32 vErrorMemTest(void);
tU32 u32ErrMemRegUnregMaxCallback(void);
tU32 u32ErrMemCheckMaxEntries(void);
tU32 u32ErrUnregWithoutReg(void);
tU32 u32ErrMemGetReadAndWrTime(void);
tU32 u32ErrMemCheckCorrectWriteRead(void);
tU32 u32ErrMemDevOpenClose(void);


#ifdef USE_IN_KERNEL_MODE
tU32 u32ErrMemWriteInvalWithinNORFLASH(void);
tU32 u32ErrMemReadInvalWithinNORFLASH(void);
tU32 u32ErrMemWriteInvalHeaderToErrMem(void);
tU32 u32ErrMemWriteWithInvalEntryHeader(void);
tU32 u32ErrMemEraseWithInvalBlock(void);
tU32 u32ErrMemGetBlkInfoInvalid(void);
tU32 u32ErrMemMultiThreadAccess(void);
tU32 u32ErrMemSwitchOffWhileClear(void);
tU32 CheckERRMEMAfterReset(void);
tU32 u32ErrMemHdrChkAfterInit(void);
tU32 u32ErrMemCheckMaxEntrylength(void);
#endif


#endif
