#ifndef _OEDT_FLASH_HELPER_FUNCS_H_
#define _OEDT_FLASH_HELPER_FUNCS_H_
/*!
 *\file     oedt_Flash_HelperFuncs.h
 *\ref      
 *\brief    defines all oedt audio test-cases
 *              
 *COPYRIGHT: 	(c) 1996 - 2000 Blaupunkt Werke GmbH
 *\author		CM-DI/PJ-GM32 - Resch
 *\version:
 * CVS Log: 
 * $Log: oedt_Flash_HelperFuncs.h
 * \bugs      
 * \warning
 * History  -  Sainath Kalpuri (RBEI/ECF1) - Removed lint and compiler
               warnings   
 **********************************************************************/


/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
typedef enum _tag_flashHelperReturn{
    OEDT_FLASH_HELPER_NO_ERROR = 0,
    OEDT_FLASH_HELPER_GENERIC_ERROR = -1
    
}enFlashHelperReturn;
/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/
tS32 s32_start_all_working_tsk(tU8 *);

#else/* #ifndef _OEDT_FLASH_HELPER_FUNCS_H_ */
#error "oedt_Flash_helperFuncs.h multiple included"
#endif/* #ifndef _OEDT_FLASH_HELPER_FUNCS_H_ */
