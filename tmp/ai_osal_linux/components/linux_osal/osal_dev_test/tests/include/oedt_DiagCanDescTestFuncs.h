/******************************************************************************
 *FILE         : oedt_DiagCanDesc_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file has the function declaration for can desc driver test cases. 
 *               
 *AUTHOR       : Andrea Bueter
 *
 *COPYRIGHT    : (c) 2008 TMS GmbH
 *
 *HISTORY      : 02.06.08 .Initial version
 *
 *****************************************************************************/

#ifndef OEDT_CAN_DESC_TESTFUNCS_HEADER
#define OEDT_CAN_DESC_TESTFUNCS_HEADER


/* Open/Close Testcases*/
tU32 u32CanDescLldOpenClose(void);
tU32 u32CanDescLldOpenInvalParam(void);
tU32 u32CanDescLldOpenNullParam(void);
tU32 u32CanDescLldCloseInvalParam(void);
tU32 u32CanDescLldReOpen(void);
tU32 u32CanDescLldReClose(void);
/* DRV_CANDESC_REGISTER_CLIENT */
tU32 u32CanDescLldRegisterClient(void);
tU32 u32CanDescLldRegisterClientInvalidParam(void);
tU32 u32CanDescLldRegisterClientAfterDevClose(void);
/* DRV_CANDESC_UNREGISTER_CLIENT*/
tU32 u32CanDescLldUnRegisterClient(void);
tU32 u32CanDescLldUnRegisterClientInvalidParam(void);
tU32 u32CanDescLldUnRegisterClientAfterDevClose(void);
/* DRV_CANDESC_WRITE_CSM_TRIGGER */
tU32 u32CanDescLldWriteCSMTrigger(void);
tU32 u32CanDescLldWriteCSMTriggerInvalidParam(void);
tU32 u32CanDescLldWriteCSMTriggerAfterDevClose(void);
/* DRV_CANDESC_WRITE_CSM_RESPONSE */
tU32 u32CanDescLldWriteCSMResponse(void);
tU32 u32CanDescLldWriteCSMResponseInvalidParam(void);
tU32 u32CanDescLldWriteCSMResponseAfterDevClose(void);
/* DRV_CANDESC_WRITE_UUDT_FRAME */
tU32 u32CanDescLldWriteUUDTFrame(void);
tU32 u32CanDescLldWriteUUDTFrameInvalidParam(void);
tU32 u32CanDescLldWriteUUDTFrameeAfterDevClose(void);
/* DRV_CANDESC_CSM_EVENTID */
tU32 u32CanDescLldWriteEventID(void);
tU32 u32CanDescLldWriteEventIDInvalidParam(void);
tU32 u32CanDescLldWriteEventIDAfterDevClose(void);
/* DRV_CANDESC_WRITE_CSM_ERROR */
tU32 u32CanDescLldWriteCSMError(void);
tU32 u32CanDescLldWriteCSMerrorInvalidParam(void);
tU32 u32CanDescLldWriteCSMErrorAfterDevClose(void);


#endif


