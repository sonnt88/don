/**
 * @copyright    (C) 2013 - 2016 Robert Bosch GmbH.
 *               The reproduction, distribution and utilization of this file as well as the
 *               communication of its contents to others without express authorization is prohibited.
 *               Offenders will be held liable for the payment of damages.
 *               All rights reserved in the event of the grant of a patent, utility model or design.
 * @brief        This file contains the header for the test functions of the
 *               Datapool
 * @addtogroup   OEDT
 * @{
 */

#ifndef OEDT_DATAPOOL_TESTFUNCS_HEADER
#define OEDT_DATAPOOL_TESTFUNCS_HEADER
/*********************************************************************************/
/*                         define                                                */
/*********************************************************************************/
/* Macro definition used in the code */
#define MQ_DP_OEDT_PROCESS_NAME       "MQ_OEDT0"
#define MQ_DP_TEST_PROCESS_NAME       "MQ_TEST0"
#define MQ_DP_OEDT_PROCESS2_NAME      "MQ_OEDT1"
#define MQ_DP_TEST_PROCESS2_NAME      "MQ_TEST1"
#define MQ_DP_OEDT_PROCESS3_NAME      "MQ_OEDT2"
#define MQ_DP_TEST_PROCESS3_NAME      "MQ_TEST2"
#define MQ_DP_OEDT_PROCESS4_NAME      "MQ_OEDT3"
#define MQ_DP_TEST_PROCESS4_NAME      "MQ_TEST3"
#define MQ_DP_OEDT_PROCESS5_NAME      "MQ_OEDT4"
#define MQ_DP_TEST_PROCESS5_NAME      "MQ_TEST4"
#define MQ_DP_OEDT_PROCESS6_NAME      "MQ_OEDT5"
#define MQ_DP_TEST_PROCESS6_NAME      "MQ_TEST5"

#define DP_TEST_VALUE_WRITE_GET_UNDEF_ELEMENT_SHARED   (tU32)0x55AA55AA

#ifdef DP_U32_POOL_ID_PDDDPTESTKDSDEFAULT
#ifdef DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE
#ifdef DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE
#define OEDT_DATAPOOL_KDS_DEFAULT_TEST
#endif
#endif
#endif

#ifdef DP_U32_POOL_ID_DPENDUSERMODE
#ifdef DP_U32_POOL_ID_PDDDPTESTKDSDEFAULTBANK
#ifdef DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE
#ifdef DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE
#define OEDT_DATAPOOL_KDS_DEFAULT_TEST_BANK
#endif
#endif
#endif
#endif

#ifdef OEDT_DATAPOOL_KDS_DEFAULT_TEST
tU8  Vu8RecordKdsDefault[DP_U8_KDSLEN_CMVARIANTCODING_COMPLETE]={0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
                                                                 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
tU8 Vu8RecordKdsDefaultSensor[DP_U8_KDSLEN_SENSORCONFIGGYRO_COMPLETE];
#endif


/*********************************************************************************/
/*                          typedef                                             */
/*********************************************************************************/
 typedef enum 
 {
   eCmdInit,
   eCmdDelete,
   eCmdSet,
   eCmdGet,
   eCmdAccessDeleteElement,
   eCmdInitDeleteElement,
   eCmdSramAccessMultiThreads, 
   eCmdProcess1End,
   eCmdLock,
   eCmdUnlock,
   eCmdSetUser,
   eCmdSetUserWithLock,  
   eCmdProcess2End,
   eCmdGetDefKDSRecord,
   eCmdGetDefKDSRecordNoInit,
   eCmdGetDefKDSItemGNSS,
   eCmdGetDefKDSItemGNSSNoInit,
   eCmdGetDefKDSWrongItem,
   eCmdGetDefKDSu32,
   eCmdGetDefKDSu16,
   eCmdGetDefKDSstr,
   eCmdGetDefKDSstrNoInit,
   eCmdConfigSetUserDpEndUserNotStored,
   eCmdConfigSetBankDpEndUserNotStored,
   eCmdConfigSetDefaultDpEndUserNotStored,
   eCmdConfigCopyUserDpEndUserNotStored,
   eCmdProcess3End,
   eCmdProcess4End,
   eCmdGetConfigItemWithoutDatapool,
   eCmdProcess5End,
   eCmdGetRegistryWithoutDatapool,
   eCmdProcess6End,
   eCmdResponse
 }ECmdTypes;

 typedef struct TPoolMsg
 {
   ECmdTypes  eCmd;
   union 
   {
     tU32  u32ErrorCode;         
	   tU8   u8EndUser;         
     struct 
	   {
       tChar       strElement[200];
       tChar       strPool[100];
	   }tShared;
   }u;
 }TPoolMsg;

 #if defined(GEN3ARM) 
/*********************************************************************************/
/*                          function                                             */
/*********************************************************************************/
tU32 u32DPSetGetElementWithoutClassAccess(void);
tU32 u32DPGetDefaultValueTrue(void);
tU32 u32DPGetDefaultValueAndState(void);
tU32 u32DPGetChangedValueAndState(void);
tU32 u32DPSetConfig(void);
tU32 u32DPGetSetCodeDataAndSetConfig(void);
tU32 u32DPAddNotificationAndSetConfig(void);
tU32 u32DPSetPoolV850(void);
tU32 u32DPSetGetTestValueU8(void);
tU32 u32DPSetGetTestArrayGreat4k(void);
tU32 u32DPSetGetTestArrayGreat2k(void);
tU32 u32DPSetGetTestStructureArray(void);
tU32 u32DPSetToDefaultTestValueU8(void);
tU32 u32DPEarlyConfigDisplaySet(void);
tU32 u32DPEarlyConfigDisplayGet(void);
tU32 u32DPEarlyConfigCSCSetGet(void);
tU32 u32DPSetDefaultValueCheckIfStored(void);
// test for SRAM access
tU32 u32DPSetGetSRAMAccess(void);
tU32 u32DPSetGetSRAMAccessWriteAll(void);
tU32 u32DPSetGetSRAMAccessMultiThread(void);
// test for KDS default entry
tU32 u32DPGetDefKDSRecordNoKdsEntryStored(void);
tU32 u32DPGetDefKDSRecordNoInitNoKdsEntryStored(void);
tU32 u32DPGetDefKDSItemNoKdsEntryStored(void);
tU32 u32DPGetDefKDSItemNoInitNoKdsEntryStored(void);
tU32 u32DPGetDefKDSWrongItemNoKdsEntryStored(void);  //20
tU32 u32DPGetDefKDSItemtU32NoKdsEntryStored(void);
tU32 u32DPGetDefKDSItemtU16NoKdsEntryStored(void);
tU32 u32DPGetDefKDSItemtStringNoKdsEntryStored(void);
tU32 u32DPGetDefKDSItemtStringNoInitNoKdsEntryStored(void);
tU32 u32DPGetDefKDSRecord(void);
tU32 u32DPGetDefKDSRecordNoInit(void);
tU32 u32DPGetDefKDSItem(void);
tU32 u32DPGetDefKDSItemNoInit(void);
tU32 u32DPGetDefKDSWrongItem(void);
tU32 u32DPGetDefKDSItemtU32(void);
tU32 u32DPGetDefKDSItemtU16(void);
tU32 u32DPGetDefKDSItemtString(void);
tU32 u32DPGetDefKDSItemtStringNoInit(void);
tU32 u32DPGetDefKDSBankElementsNoKdsEntryStored(void);
tU32 u32DPGetDefKDSBankElementsStored(void);
tU32 u32DPSetDefKDSToDefault(void);
tU32 u32DPLoadDefaultBankToUserAndStore(void);
//test for undefined elements
tU32 u32DPSetUndefElementCheckParameter(void);
tU32 u32DPGetUndefElementCheckParameter(void);
tU32 u32DPInitUndefElementCheckParameter(void);
tU32 u32DPDeleteUndefElementCheckParameter(void);//40
tU32 u32DPSetUndefElement(void);
tU32 u32DPGetUndefElementUnknown(void);
tU32 u32DPDeleteUndefElementUnknown(void);
tU32 u32DPSetGetUndefElement(void);
tU32 u32DPSetDeleteGetUndefElement(void);
tU32 u32DPInitGetUndefElement(void);
tU32 u32DPInitSetGetUndefElement(void);
tU32 u32DPInit20UndefElements(void);
tU32 u32DPInitUndefElementNewProperty(void);
tU32 u32DPInitUndefElementSetDefaultProperty(void);
tU32 u32DPWriteInitUndefElement(void);						    //	=> test together
tU32 u32DPReadInitUndefElementAfterReboot(void);			//	=> test together after reboot
tU32 u32DPInitSetGetUndefElementShared(void);
tU32 u32DPOtherProcessInitUndefElementShared(void);
tU32 u32DPOtherProcessDeleteUndefElementShared(void);
tU32 u32DPOtherProcessSetUndefElementShared(void);
tU32 u32DPOtherProcessGetUndefElementShared(void);
tU32 u32DPOtherProcessAccessDelUndefElementShared(void);
tU32 u32DPOtherProcessInitDelUndefElementShared(void);
//test for end user feature 
tU32 u32DPEndUserLockUnlock(void);  //60
tU32 u32DPEndUserSetGetElementEndUserIfLocked(void);
tU32 u32DPEndUserSetGetElementEndUserAfterUnLocked(void);
tU32 u32DPEndUserSetGetElementNoEndUserIfLocked(void);
tU32 u32DPEndUserLock2ModesUnlock(void);					
tU32 u32DPEndUserSetEndUserReadNewDefaultElement(void);		
tU32 u32DPEndUserSaveNewEndUser1AndSetAllElements(void);    //	=> test together
tU32 u32DPEndUserGetCmpAllNewEndUser1AndSetToDefault(void); //	=> test together after reboot
tU32 u32DPEndUserSetNoCurrentUserToDefault(void);
tU32 u32DPEndUserSetAllPoolsToFactoryDefault(void);
tU32 u32DPEndUserSetAllPoolsToUserDefault(void);
tU32 u32DPEndUserGetNoCurrentEndUserValue(void);
tU32 u32DPEndUserOtherProcessSetEndUser(void);
tU32 u32DPEndUserOtherProcessLockSetEndUserUnlock(void);
tU32 u32DPEndUserCopyToCurrentUserNotAllowed(void);
tU32 u32DPEndUserCopyFromCurrentUser(void);
tU32 u32DPEndUserCopyFromFileNotCurrentUser(void);
tU32 u32DPEndUserCopySwitchEndUser(void);
tU32 u32DPEndUserCopySetDefaultCopyReadDefault(void);
//test for end user feature banking
tU32 u32DPEndUserBankLoadBankToCurrentEndUser(void); 
tU32 u32DPEndUserBankSaveCurrentEndUserToBank(void); 
tU32 u32DPEndUserBankSetValueSaveToBankLoadSavedBank(void);
tU32 u32DPEndUserBankGetBank(void);
tU32 u32DPEndUserBankSetEndGreatUserLoadBank(void);
tU32 u32DPEndUserBankSetBankDefault(void);
tU32 u32DPEndUserBankSetAllBanksDefault(void);
tU32 u32DPEndUserBankSetEndUserDefault(void); //81
tU32 u32DPEndUserBankSetUserDefault(void);
tU32 u32DPEndUserSwitchbackBankAction(void);
tU32 u32DPEndUserSwitchbackUserDefault(void);
tU32 u32DPEndUserBankLoadSwitchUser(void);
tU32 u32DPEndUserBankSetBankDefaultGetSwitchEndUser(void);
// test default for bank and user
tU32 u32DPSetDefault_TefUser(void);
tU32 u32DPSetDefault_EndUserCurrent(void);
tU32 u32DPSetDefault_EndUserNotCurrent(void);
tU32 u32DPSetUserDefault_CurrentUser1(void);
tU32 u32DPSetUserDefault_CurrentUser3(void);
// test default for elements with secure
tU32 u32DPSetBankDefaultWithElemSecure(void);
tU32 u32DPSetEndUserDefaultWithElemSecure(void);
tU32 u32DPSetUserDefaultWithElemSecure(void);
//test for kds interface over Datapool DP_KDS_INTERFACE_2
tU32 u32DPs32GetConfigItemWrongParamString(void);
tU32 u32DPs32GetConfigItemBufferNull(void);
tU32 u32DPs32GetConfigItemWrongSize(void);
tU32 u32DPs32GetConfigItemCompleteWriteGetCmp(void);
tU32 u32DPs32GetConfigItemArrayWriteGetCmp(void);
tU32 u32DPs32GetConfigItemByteWriteGetCmp(void);
tU32 u32DPs32GetConfigItemFromTestProcessProcdatapool5(void);
tU32 u32DPs32GetRegistryFromTestProcessProcdatapool6(void);
//test function for s32StorePoolNow
tU32 u32DPs32StorePoolNow(void);
tU32 u32DPs32StorePoolNowWrongAccess(void);
//test for DpEndUserNotStored
tU32 u32DPChangeConfigSetUserDpEndUserNotStored(void);
tU32 u32DPChangeConfigSetBankDpEndUserNotStored(void);
tU32 u32DPChangeConfigSetDefaultDpEndUserNotStored(void);
tU32 u32DPChangeConfigCopyUserDpEndUserNotStored(void);
/*test process exit and lock/unlock */
tU32 u32DPProcessExitWithOsalExitFunction(void);  
tU32 u32DPProcessExitWithoutOsalExitFunction(void);
#endif
#endif
