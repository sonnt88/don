#ifndef IOSC_SHAREDMEM_TESTCASES_H_
#define IOSC_SHAREDMEM_TESTCASES_H_

#include "iosc_testcases_global.h"

#define IOSC_TEST_MEM_ID	IOSC_MEM_ID(2011)
#define IOSC_TEST_MEM_ID2	IOSC_MEM_ID(2012)

#ifdef OSAL_OS
#if (OSAL_OS == OSAL_LINUX)
#define iosc_shared_free(ptr) iosc_shared_mem_free(ptr)
#define iosc_shared_malloc(size) iosc_shared_malloc_with_id((size),IOSC_TEST_MEM_ID, NULL)
#endif
#endif //OSAL_OS

OEDT_TEST_SIMPLE_DECLARATION(SharedMemAllocWithGrowingSizes);
OEDT_TEST_SIMPLE_DECLARATION(SharedMemFree);
OEDT_TEST_SIMPLE_DECLARATION(SharedMemAllocWithIdMultipleTimes);

#endif //IOSC_SHAREDMEM_TESTCASES_H_

