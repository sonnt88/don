/******************************************************************************
 *FILE         : oedt_Registry_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk(osal_dev_test)
 *
 *DESCRIPTION  : This file contains function prototypes and macros that 
 				 will be used in the file oedt_Registry_TestFuncs.c for the  
 *               REGISTRY  device for the GM-GE hardware.
 *
 *AUTHOR       : Pankaj Kumar (RBIN/EDI3) 

 *COPYRIGHT    : (c) 2007 Robert Bosch India Limited (RBIN)

 *HISTORY      : 11 Oct, 2007 ver 0.1  RBIN/EDI3- Pankaj Kumar
 				 12 Oct , 2007 ver 0.2  RBIN/EDI3 -Pankaj Kumar
				 4 Apr, 2008 ver 0.3 RBEI/ECM1 - Shilpa Bhat
				 30 May, 2008 ver 0.4 RBEI/ECM1 - Shilpa Bhat
                 29-Sept-2008 v0.5 RBEI/ECM1 - Shilpa Bhat 
                 Removed testcases u32RegGetAllSubkeyNames, u32RegGetValueNames,
                 u32RegGetValueInfo, u32RegGetVersionInvalParam
				 25-03-2009 v0.6 RBEI/ECF1 - Sainath Kalpuri
                 Removed lint and compiler warnings
                 28-April-2009 v0.7 - Anoop Chandran (RBEI/ECF1)
                  update OEDT_REGISTRY_INVALID_ACCESS_MODE
*******************************************************************************/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"	                                                          //RBIN

#ifndef OEDT_REGISTRY_TESTFUNCS_HEADER
#define OEDT_REGISTRY_TESTFUNCS_HEADER

#define OSAL_C_STRING_DEVICE_REGISTRY_KEY          "/dev/registry/Test"           //RBIN
#define OSAL_C_STRING_DEVICE_REGISTRY_KEY1          "/dev/registry/Test1"           //RBIN
#define OEDT_REGISTRY_INVALID_ACCESS_MODE                            25                      //RBIN
// #define REG_INVAL_PARAM                                0xffff	                      //RBIN
#define REG_INVAL_PARAM                             OSAL_C_INVALID_HANDLE            /* dont use any number , it must be OSAL_C_... */

/*Start of Test case function prototypes (RBIN)*/

tU32 u32RegDevOpenClose(void);
tU32 u32RegDevOpenCloseDiffModes(void);
tU32 u32RegDevOpenCloseInvalParam(void);
tU32 u32RegCreateKey(void);
tU32 u32RegOpenCloseKey(void);
tU32 u32RegGetVersion(void);
tU32 u32RegSetGetValue(void);
tU32 u32RegWriteValue(void);
tU32 u32RegSetQueryValue(void);
tU32 u32RegRemoveValue(void);
tU32 u32RegDevReOpen(void);
tU32 u32RegDevCloseAlreadyClosed(tVoid);
tU32 u32RegCreateMultipleValues(tVoid);
tU32 u32RegCreateMultipleKeys(tVoid);
tU32 u32RegReOpenKey(tVoid);
tU32 u32RegCloseKeyAlreadyClosed(tVoid);
tU32 u32RegDeleteKeyWithValues(tVoid);
tU32 u32RegOpenKeyDiffAccessModes(tVoid);
tU32 u32RegWriteNumericValue(tVoid);
tU32 u32RegDelKeyAlreadyRemoved(tVoid);
tU32 u32RegDelKeyStructure(tVoid);
tU32 u32REGISTRYBasic1Test(void);
tU32 u32REGISTRYBasic2Test(void);
tU32 u32REGISTRYBasic3Test(void);
tU32 u32RegSearchKey(void);
tU32 u32RegOpenAsDir(void);

/*End of Test case function prototypes (RBIN)*/

#endif

