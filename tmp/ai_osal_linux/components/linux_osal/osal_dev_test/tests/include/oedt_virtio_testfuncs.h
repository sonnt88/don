/************************************************************************
 * @file: oedt_virtio_testfuncs.h
 *
 *
 * oedt component test for virtio driver
 * 
 * @component: virtio
 *
 * @author: Andreas Pape
 *
 * @copyright: (c) 2003 - 2010 ADIT Corporation
 *
 * @see <related items>
 * 
 * @history
 * <history item>
 *
 * ASSUMPTION(s): 
 ***********************************************************************/

#ifndef _OEDT_VIRTIO_TEST_H_
#define _OEDT_VIRTIO_TEST_H_

/* oedt interfaces for virtio testfunctions*/
tU32 u32virtio_test1(void);
tU32 u32virtio_test2(void);
tU32 u32virtio_test3(void);
tU32 u32virtio_test4(void);
tU32 u32virtio_test5(void);
tU32 u32virtio_test6(void);


#endif /* _OEDT_VIRTIO_TEST_H */
