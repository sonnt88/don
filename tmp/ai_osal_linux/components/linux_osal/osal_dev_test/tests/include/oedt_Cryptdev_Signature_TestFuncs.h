/******************************************************************
*
* FILE:
*     oedt_Cryptdev_Signature_TestFuncs.h
*
* REVISION:
*     1.1
*
* AUTHOR:
*     (c) 2005, RBEI, ECF1 Jeryn M.
*
* CREATED:
*     29/12/2010 - Jeryn M.
*
* DESCRIPTION:
*      This file contains signature verification testcases for
*      crypt devices.
*
* NOTES:
*
* MODIFIED:
* 29-12-2010 | jev1kor    | Initial Version
* 05-04-2011 | Anooj Gopi | Added test cases multiple crypt devices
* 19.10.2011 | Sudharsanan Sivagnanam (RBEI/ECF1) | Added test cases for Noncrypt devices
* 12-03-2014 | Swathi Bolar (RBEI/ECF5)           | Added test cases to update data to KDS
* 16-03-2015 | Deepak Kumar (RBEI/ECF5)           | lint fixes for CFG3-1001
****************************************************************/
#ifndef __OEDT_CRYPTDEV_SIGNATURE_TESTFUNCS_H__
#define __OEDT_CRYPTDEV_SIGNATURE_TESTFUNCS_H__

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "oedt_helper_funcs.h"
#include "oedt_FS_TestFuncs.h"


#define SWITCH_OEDT_CRYPTCARD
#define OEDT_CRYPTNAV_TEST_ENABLE

#ifdef SWITCH_OEDT_CRYPTCARD
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD   "/dev/cryptcard"
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD2  "/dev/cryptcard2"
#define OEDTTEST_C_STRING_DEVICE_CRYPTNAV    "/dev/cryptnav"

/*for non crypt devices*/
#define  OEDTTEST_C_STRING_DEVICE_CARD     		"/dev/card"  
#define  OEDTTEST_C_STRING_DEVICE_CARD2   		"/dev/card2"  
#define  OEDTTEST_C_STRING_DEVICE_CRYPTNAVROOT  "/dev/cryptnavroot"  

#else
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD   OSAL_C_STRING_DEVICE_CRYPTCARD
#define OEDTTEST_C_STRING_DEVICE_CRYPTCARD2  OSAL_C_STRING_DEVICE_CRYPTCARD2
#define OEDTTEST_C_STRING_DEVICE_CRYPTNAV     "/dev/cryptnav"

/*for non crypt devices*/
#define  OEDTTEST_C_STRING_DEVICE_CARD       		OSAL_C_STRING_DEVICE_CARD 
#define  OEDTTEST_C_STRING_DEVICE_CARD2       		"/dev/card2" 
#define  OEDTTEST_C_STRING_DEVICE_CRYPTNAVROOT    	"/dev/cryptnavroot" 

#endif

/* Relative path of signature files */
#define OEDTTEST_C_STRING_CERT_INVALID_PATH_CRYPTCARD 	"INVALID.DAT"
#define OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD   		"DATA/DATA/MISC/SDX_META.DAT"
#define OEDTTEST_C_STRING_CERT_PATH_CRYPTCARD2  		"DATA/DATA/MISC/SDX_META.DAT"
#define OEDTTEST_C_STRING_CERT_PATH_CRYPTNAV    		"DATA/DATA/MISC/SDX_META.DAT"
#define OEDTTEST_C_STRING_CERT_PATH_CARD      			"cryptnav/DATA/DATA/MISC/SDX_META.DAT"
#define OEDTTEST_C_STRING_CERT_PATH_CARD2	            "cryptnav/DATA/DATA/MISC/SDX_META.DAT"
#define OEDTTEST_C_STRING_CERT_PATH_CRYPTNAVROOT      "cryptnav/DATA/DATA/MISC/SDX_META.DAT"
#define OEDTTEST_C_STRING_CERT_PATH_USB					"cryptnav/DATA/DATA/MISC/SDX_META.DAT"
/* Data to be stored in Kds for the test to run successfully */
#define OEDTTEST_C_STRING_VALID_VIN_DATA                 "WBWBOSCH12H181012"
#define OEDTTEST_C_STRING_INVALID_VIN_DATA               "WBWERROR12H181012" 
#define OEDTTEST_C_STRING_VALID_DID_DATA                 "WBWBOSCH12H181012" 
#define OEDTTEST_C_STRING_INVALID_DID_DATA               "WBWERROR12H181012"

/* Development feature. If target does not have correct VIN, comment line below */
//#define VIN_IN_KDS              1
#ifdef VIN_IN_KDS
/* Length of VIN entry */
#define VIN_ENTRY_LEN 		     (40)
   /* Entry number of VIN in KDS */
   #define VIN_ENTRY	              (0x0114)
#endif
/* Development feature. If target does not have correct DID, comment line below */
#define DID_IN_KDS              1
#ifdef DID_IN_KDS
/* Length of DID entry */
#define DID_ENTRY_LEN 		     (40)
   /* Entry number of DID in KDS */
   #define DID_ENTRY	             (0x0162)
#endif

// OEDT Test functions for Signature Verification
tU32 u32Cryptcard_IOCTRL_TriggerSignVerify(tVoid);
tU32 u32Cryptcard_IOCTRL_SignVerifyStatus(tVoid);
tU32 u32Cryptcard_IOCTRL_SignFileType(tVoid);
tU32 u32Cryptcard_InvalidCertPath_TriggerSignVerify( tVoid );
tU32 u32Cryptcard_InvalidCertPath_GetSignFileType(tVoid);                
/* Cryptcard2 OEDT Test fuctions */
tU32 u32Cryptcard2_IOCTRL_TriggerSignVerify(tVoid);
tU32 u32Cryptcard2_IOCTRL_SignVerifyStatus(tVoid);
tU32 u32Cryptcard2_IOCTRL_SignFileType(tVoid);
/* Cryptnav OEDT Test fuctions */
tU32 u32Cryptnav_IOCTRL_TriggerSignVerify(tVoid);
tU32 u32Cryptnav_IOCTRL_SignVerifyStatus(tVoid);
tU32 u32Cryptnav_IOCTRL_SignFileType(tVoid);
/* Card OEDT Test fuctions */
tU32 u32Noncrypt_dev1_IOCTRL_SignVerify(tVoid);
tU32 u32Noncrypt_dev1_IOCTRL_VerifyStatus(tVoid);
tU32 u32Noncrypt_dev1_IOCTRL_FileType(tVoid);

/* USBMS OEDT Test fuctions */
tU32 u32Noncrypt_dev2_IOCTRL_SignVerify(tVoid);
tU32 u32Noncrypt_dev2_IOCTRL_VerifyStatus(tVoid);
tU32 u32Noncrypt_dev2_IOCTRL_FileType(tVoid);

/* Cryptnavroot OEDT Test fuctions */
tU32 u32Noncrypt_dev3_IOCTRL_SignVerify(tVoid);
tU32 u32Noncrypt_dev3_IOCTRL_VerifyStatus(tVoid);
tU32 u32Noncrypt_dev3_IOCTRL_FileType(tVoid);

/* Multi thread sign verify */
tU32 u32MultiThread_Signverify( tVoid );
tU32 u32MultiThread_SignverifyStatus( tVoid );
tU32 u32MultiThread_SignType( tVoid );
/* Multi function with same device*/
tU32 u32Cryptcard_Multifunction( tVoid );
tU32 u32Card_Multifunction( tVoid );
/* Multi device with same function*/
tU32 u32Multidev_Signverify( tVoid );
tU32 u32Multidev_SignverifyStatus( tVoid );
tU32 u32Multidev_SignfileType( tVoid );
/* Multi device with Multi function*/
tU32 u32Multidev_Multifunction( tVoid );

tU32 u32SDCard_IOCTRL_TriggerSignVerify(tVoid);
tU32 u32SDcard_RegisterForNotification (tVoid );
tVoid vData_Media_SDCARD (tPCU32 pu32MediaChangeInfo, tU8 au8String[]);
tU32 u32SDCard_IOCTRL_SignVerifyStatus(tVoid);
tU32 u32SDCard_IOCTRL_SignFileType(tVoid);
tU32 u32SDCard_InvalidCertPath_TriggerSignVerify(tVoid);
tU32 u32SDCard_InvalidCertPath_GetSignFileType(tVoid);
tU32 u32SDcard_Multifunction( tVoid );


tU32 u32USB_IOCTRL_TriggerSignVerify(tVoid);
tU32 u32USB_RegisterForNotification (tVoid );
tVoid vData_Media_USB (tPCU32 pu32MediaChangeInfo, tU8 au8String[]);
tU32 u32USB_IOCTRL_SignVerifyStatus(tVoid);
tU32 u32USB_IOCTRL_SignFileType(tVoid);
tU32 u32WriteVINValuetoKDS(void);
tU32 u32WriteDIDValuetoKDS(tVoid);
tU32 u32WriteErrorVINtoKDS(tVoid);
tU32 u32WriteErrorDIDtoKDS(tVoid);

#endif //__OEDT_CRYPTDEV_SIGNATURE_TESTFUNCS_H__
