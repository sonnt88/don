/******************************************************************************
 * FILE				: oedt_RAMDISK_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT		: OEDT_FrmWrk 
 *
 * DESCRIPTION		: This file defines the Macros and prototypes used by the 
 *					  file system test cases for the RAMDISK  
 *                          
 * AUTHOR(s)		:  Sriranjan U (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * 	Date			|							|	Author & comments
 * --.--.--			|	Initial revision		|	------------
 * 01 Dec, 2008		|	version 1.0				|	Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 13 Jan, 2009     |   version 1.1             |   Shilpa Bhat(RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Mar, 2009	    |   version 1.2		        |   Lint Removal
 *			        |						    |   Shilpa Bhat(RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 10 Sep, 2009  	| 	version 1.3  		  	|  Commenting the Test cases which uses 
 *					|							|  OSAL_C_S32_IOCTRL_FIOOPENDIR
 *					|							|  Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Feb, 2010     | version 1.4               |  Updated the Device name 
 *				    |			                |  Anoop Chandran (RBEI/ECF1)
 * ------------------------------------------------------------------------------*/

#ifndef OEDT_RAMDISK_COMMON_FS_TESTFUNCS_HEADER
#define OEDT_RAMDISK_COMMON_FS_TESTFUNCS_HEADER
#ifdef SWITCH_OEDT_RAMDISK
#define OEDTTEST_C_STRING_DEVICE_RAMDISK "/dev/ram1"
#else
#define OEDTTEST_C_STRING_DEVICE_RAMDISK OSAL_C_STRING_DEVICE_RAMDISK
#endif

#define OEDT_C_STRING_RAMDISK_CFS_DIR1 OEDTTEST_C_STRING_DEVICE_RAMDISK"/NewDir"
#define OEDT_C_STRING_RAMDISK_CFS_NONEXST OEDTTEST_C_STRING_DEVICE_RAMDISK"/Invalid"
#define OEDT_C_STRING_RAMDISK_CFS_FILE1 OEDTTEST_C_STRING_DEVICE_RAMDISK"/FILEFIRST.txt"
#define SUBDIR_PATH_RAMDISK_CFS	OEDTTEST_C_STRING_DEVICE_RAMDISK"/NewDir"
#define SUBDIR_PATH2_RAMDISK_CFS	OEDTTEST_C_STRING_DEVICE_RAMDISK"/NewDir/NewDir2"
#define OEDT_C_STRING_RAMDISK_CFS_DIR_INV_NAME OEDTTEST_C_STRING_DEVICE_RAMDISK"/*@#**"
#define OEDT_C_STRING_RAMDISK_CFS_DIR_INV_PATH OEDTTEST_C_STRING_DEVICE_RAMDISK"/MYFOLD1/MYFOLD2/MYFOLD3"
#define	CREAT_FILE_PATH_RAMDISK_CFS OEDTTEST_C_STRING_DEVICE_RAMDISK"/Testf1.txt"  
#define OSAL_TEXT_FILE_FIRST_RAMDISK_CFS OEDTTEST_C_STRING_DEVICE_RAMDISK"/file1.txt"

#define OEDT_C_STRING_FILE_INVPATH_RAMDISK_CFS OEDTTEST_C_STRING_DEVICE_RAMDISK"/Dummydir/Dummy.txt"

#define OEDT_RAMDISK_COMNFILE_CFS OEDTTEST_C_STRING_DEVICE_RAMDISK"/common.txt"

#define OEDT_RAMDISK_CFS_WRITEFILE OEDTTEST_C_STRING_DEVICE_RAMDISK"/WriteFile.txt"

#define FILE12_RECR2_RAMDISK_CFS OEDTTEST_C_STRING_DEVICE_RAMDISK"/File_Dir/File_Dir2/File12_test.txt"
#define FILE11_RAMDISK_CFS OEDTTEST_C_STRING_DEVICE_RAMDISK"/File_Dir/File11.txt"
#define FILE12_RAMDISK_CFS OEDTTEST_C_STRING_DEVICE_RAMDISK"/File_Dir/File12.txt"

#define OEDT_C_STRING_DEVICE_RAMDISK_CFS_ROOT OEDTTEST_C_STRING_DEVICE_RAMDISK"/"

#define FILE13_RAMDISK_CFS     OEDTTEST_C_STRING_DEVICE_RAMDISK"/File_Source/File13.txt"
#define FILE14_RAMDISK_CFS     OEDTTEST_C_STRING_DEVICE_RAMDISK"/File_Source/File14.txt"

#define OEDT_RAMDISK_CFS_DUMMYFILE				OEDTTEST_C_STRING_DEVICE_RAMDISK"/Dummy.txt"
#define OEDT_RAMDISK_CFS_TESTFILE				OEDTTEST_C_STRING_DEVICE_RAMDISK"/Test.txt"
#define OEDT_RAMDISK_CFS_INVALIDFILE			OEDTTEST_C_STRING_DEVICE_RAMDISK"/DIR/Test.txt"
#define OEDT_RAMDISK_CFS_UNICODEFILE		  	OEDTTEST_C_STRING_DEVICE_RAMDISK"/��汪�.txt"
#define OEDT_RAMDISK_CFS_INVALCHAR_FILE			OEDTTEST_C_STRING_DEVICE_RAMDISK"//*/</>>.txt"

	 											  
/* Function Prototypes for the functions defined in Oedt_FileSystem_TestFuncs.c */
tU32 u32RAMDISK_CommonFSOpenClosedevice(tVoid );
tU32 u32RAMDISK_CommonFSOpendevInvalParm(tVoid );
tU32 u32RAMDISK_CommonFSReOpendev(tVoid );
tU32 u32RAMDISK_CommonFSOpendevDiffModes(tVoid );
tU32 u32RAMDISK_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32RAMDISK_CommonFSOpenClosedir(tVoid );
tU32 u32RAMDISK_CommonFSOpendirInvalid(tVoid );
tU32 u32RAMDISK_CommonFSCreateDelDir(tVoid );
tU32 u32RAMDISK_CommonFSCreateDelSubDir(tVoid );
tU32 u32RAMDISK_CommonFSCreateDirInvalName(tVoid );
tU32 u32RAMDISK_CommonFSRmNonExstngDir(tVoid );
tU32 u32RAMDISK_CommonFSRmDirUsingIOCTRL(tVoid );
tU32 u32RAMDISK_CommonFSGetDirInfo(tVoid );
tU32 u32RAMDISK_CommonFSOpenDirDiffModes(tVoid );
tU32 u32RAMDISK_CommonFSReOpenDir(tVoid );
tU32 u32RAMDISK_CommonFSDirParallelAccess(tVoid);
tU32 u32RAMDISK_CommonFSCreateDirMultiTimes(tVoid ); 
tU32 u32RAMDISK_CommonFSCreateRemDirInvalPath(tVoid );
tU32 u32RAMDISK_CommonFSCreateRmNonEmptyDir(tVoid );
tU32 u32RAMDISK_CommonFSCopyDir(tVoid );
tU32 u32RAMDISK_CommonFSMultiCreateDir(tVoid );
tU32 u32RAMDISK_CommonFSCreateSubDir(tVoid);
tU32 u32RAMDISK_CommonFSDelInvDir(tVoid);
tU32 u32RAMDISK_CommonFSCopyDirRec(tVoid );
tU32 u32RAMDISK_CommonFSRemoveDir(tVoid);
tU32 u32RAMDISK_CommonFSFileCreateDel(tVoid );
tU32 u32RAMDISK_CommonFSFileOpenClose(tVoid );
tU32 u32RAMDISK_CommonFSFileOpenInvalPath(tVoid );
tU32 u32RAMDISK_CommonFSFileOpenInvalParam(tVoid );
tU32 u32RAMDISK_CommonFSFileReOpen(tVoid );
tU32 u32RAMDISK_CommonFSFileRead(tVoid );
tU32 u32RAMDISK_CommonFSFileWrite(tVoid );
tU32 u32RAMDISK_CommonFSGetPosFrmBOF(tVoid );
tU32 u32RAMDISK_CommonFSGetPosFrmEOF(tVoid );
tU32 u32RAMDISK_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32RAMDISK_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32RAMDISK_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32RAMDISK_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32RAMDISK_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32RAMDISK_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32RAMDISK_CommonFSGetFileCRC(tVoid );
tU32 u32RAMDISK_CommonFSReadAsync(tVoid);
tU32 u32RAMDISK_CommonFSLargeReadAsync(tVoid);
tU32 u32RAMDISK_CommonFSWriteAsync(tVoid);
tU32 u32RAMDISK_CommonFSLargeWriteAsync(tVoid);
tU32 u32RAMDISK_CommonFSWriteOddBuffer(tVoid);
tU32 u32RAMDISK_CommonFSFileMultipleHandles(tVoid);
tU32 u32RAMDISK_CommonFSWriteFileWithInvalidSize(tVoid);
tU32 u32RAMDISK_CommonFSWriteFileInvalidBuffer(tVoid);
tU32 u32RAMDISK_CommonFSWriteFileStepByStep(tVoid);
tU32 u32RAMDISK_CommonFSGetFreeSpace (tVoid);
tU32 u32RAMDISK_CommonFSGetTotalSpace (tVoid);
tU32 u32RAMDISK_CommonFSFileOpenCloseNonExstng(tVoid);
tU32 u32RAMDISK_CommonFSFileDelWithoutClose(tVoid);
tU32 u32RAMDISK_CommonFSSetFilePosDiffOff(tVoid);
tU32 u32RAMDISK_CommonFSCreateFileMultiTimes(tVoid);
tU32 u32RAMDISK_CommonFSFileCreateUnicodeName(tVoid);
tU32 u32RAMDISK_CommonFSFileCreateInvalName(tVoid);
tU32 u32RAMDISK_CommonFSFileCreateLongName(tVoid);
tU32 u32RAMDISK_CommonFSFileCreateDiffModes(tVoid);
tU32 u32RAMDISK_CommonFSFileCreateInvalPath(tVoid);
tU32 u32RAMDISK_CommonFSStringSearch(tVoid);
tU32 u32RAMDISK_CommonFSFileOpenDiffModes(tVoid);
tU32 u32RAMDISK_CommonFSFileReadAccessCheck(tVoid);
tU32 u32RAMDISK_CommonFSFileWriteAccessCheck(tVoid);
tU32 u32RAMDISK_CommonFSFileReadSubDir(tVoid);
tU32 u32RAMDISK_CommonFSFileWriteSubDir(tVoid);
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFile(tVoid);
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFile(tVoid);
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFile(tVoid);
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFile(tVoid);
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof1KbFilefor1000times(tVoid);
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof10KbFilefor1000times(tVoid);
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof100KbFilefor100times(tVoid);
tU32 u32RAMDISK_CommonFSReadWriteTimeMeasureof1MBFilefor16times(tVoid);
tU32 u32RAMDISK_CommonFSRenameFile(tVoid);
tU32 u32RAMDISK_CommonFSWriteFrmBOF(tVoid);
tU32 u32RAMDISK_CommonFSWriteFrmEOF(tVoid);
tU32 u32RAMDISK_CommonFSLargeFileRead(tVoid);
tU32 u32RAMDISK_CommonFSLargeFileWrite(tVoid);
tU32 u32RAMDISK_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32RAMDISK_CommonFSWriteMultiThread(tVoid);
tU32 u32RAMDISK_CommonFSWriteReadMultiThread(tVoid);
tU32 u32RAMDISK_CommonFSReadMultiThread(tVoid);
tU32 u32RAMDISK_CommonFSFileGetExt(tVoid);
tU32 u32RAMDISK_CommonFSFileGetExt2(tVoid);


#endif //OEDT_RAMDISK_COMMON_FS_TESTFUNCS_HEADER

