#ifndef OEDT_FS_TESTFUNCS_HEADER
#define OEDT_FS_TESTFUNCS_HEADER

#define ERROR_MULTIPLIER 1000
#define CDROM_ERROR		1000
#define FFS1_ERROR		2000
#define FFS2_ERROR		3000
#define FFS3_ERROR		4000
#define RAMDISK_ERROR	5000
#define USBH_ERROR		6000
#define SDCARD_ERROR	7000

#define OEDTTEST_C_STRING_DEVICE_FFS1 "/dev/ffs1"
#define OEDTTEST_C_STRING_DEVICE_FFS2 "/dev/ffs2"
#define OEDTTEST_C_STRING_DEVICE_FFS3 "/dev/ffs3"


#define OEDT_C_STRING_DEVICE_INVAL "/dev/Invalid"
#define OEDT_INVALID_ACCESS_MODE -100
#define OEDT_C_STRING_CD_DIR1 OSAL_C_STRING_DEVICE_CDROM"/TestCD/DreamTheatre"
#define OEDT_C_STRING_FFS1_DIR1 OEDTTEST_C_STRING_DEVICE_FFS1"/NewDir"
#define OEDT_C_STRING_FFS2_DIR1 OEDTTEST_C_STRING_DEVICE_FFS2"/NewDir"
#define OEDT_C_STRING_RAMDISK_DIR1 OSAL_C_STRING_DEVICE_RAMDISK"/NewDir"
#define OEDT_C_STRING_SDCARD_DIR1 OSAL_C_STRING_DEVICE_CARD"/NewDir"
#define OEDT_C_STRING_USB_DIR1 OSAL_C_STRING_DEVICE_USB"/NewDir"

#define OEDT_C_STRING_CD_NONEXST OSAL_C_STRING_DEVICE_CDROM"/Invalid"
#define OEDT_C_STRING_FFS1_NONEXST OEDTTEST_C_STRING_DEVICE_FFS1"/Invalid"
#define OEDT_C_STRING_FFS2_NONEXST OEDTTEST_C_STRING_DEVICE_FFS2"/Invalid"
#define OEDT_C_STRING_RAMDISK_NONEXST OSAL_C_STRING_DEVICE_RAMDISK"/Invalid"
#define OEDT_C_STRING_SDCARD_NONEXST OSAL_C_STRING_DEVICE_CARD"/Invalid"
#define OEDT_C_STRING_USB_NONEXST OSAL_C_STRING_DEVICE_USB"/Invalid"
#define SUBDIR_NAME_INVAL "/Inval"

#define OEDT_C_STRING_FFS1_FILE1 OEDTTEST_C_STRING_DEVICE_FFS1"/FILEFIRST.txt"
#define OEDT_C_STRING_FFS2_FILE1 OEDTTEST_C_STRING_DEVICE_FFS2"/FILEFIRST.txt"
#define OEDT_C_STRING_RAMDISK_FILE1 OSAL_C_STRING_DEVICE_RAMDISK"/FILEFIRST.txt"
#define OEDT_C_STRING_SDCARD_FILE1 OSAL_C_STRING_DEVICE_CARD"/FILEFIRST.txt"
#define OEDT_C_STRING_USB_FILE1 OSAL_C_STRING_DEVICE_USB"/FILEFIRST.txt"

#define SUBDIR_CREAT_CDROM_PATH OSAL_C_STRING_DEVICE_CDROM"/TestCD/Creed/Weathered"

#define OEDT_C_STRING_CDROM_DIR2 SUBDIR_CREAT_CDROM_PATH"/NewDir"
#define SUBDIR_NAME "NewDir"
#define SUBDIR_PATH_FFS1	OEDTTEST_C_STRING_DEVICE_FFS1"/NewDir"
#define SUBDIR_PATH_FFS2	OEDTTEST_C_STRING_DEVICE_FFS2"/NewDir"
#define SUBDIR_PATH_RAMDISK	OSAL_C_STRING_DEVICE_RAMDISK"/NewDir"
#define SUBDIR_PATH_SDCARD	OSAL_C_STRING_DEVICE_CARD"/NewDir"
#define SUBDIR_PATH_USB	OSAL_C_STRING_DEVICE_USB"/NewDir"

#define SUBDIR_NAME2 "NewDir2"
#define SUBDIR_PATH2_FFS1	SUBDIR_PATH_FFS1"/NewDir2"
#define SUBDIR_PATH2_FFS2	SUBDIR_PATH_FFS2"/NewDir2"
#define SUBDIR_PATH2_RAMDISK	SUBDIR_PATH_RAMDISK"/NewDir2"
#define SUBDIR_PATH2_SDCARD	SUBDIR_PATH_SDCARD"/NewDir2"
#define SUBDIR_PATH2_USB	SUBDIR_PATH_USB"/NewDir2"

#define SUBDIR_NAME3 "NewDir3"

#define OEDT_MAX_TOTAL_DIR_PATH_LEN 260

#define OEDT_C_STRING_FFS1_DIR_INV_NAME OEDTTEST_C_STRING_DEVICE_FFS1"/*@#**"
#define OEDT_C_STRING_FFS2_DIR_INV_NAME OEDTTEST_C_STRING_DEVICE_FFS2"/*@#**"
#define OEDT_C_STRING_RAMDISK_DIR_INV_NAME OSAL_C_STRING_DEVICE_RAMDISK"/*@#**"
#define OEDT_C_STRING_SDCARD_DIR_INV_NAME OSAL_C_STRING_DEVICE_CARD"/*@#**"
#define OEDT_C_STRING_USB_DIR_INV_NAME OSAL_C_STRING_DEVICE_USB"/*@#**"

#define OEDT_C_STRING_FFS1_DIR_INV_PATH OEDTTEST_C_STRING_DEVICE_FFS1"/MYFOLD1/MYFOLD2/MYFOLD3"
#define OEDT_C_STRING_FFS2_DIR_INV_PATH OEDTTEST_C_STRING_DEVICE_FFS2"/MYFOLD1/MYFOLD2/MYFOLD3"
#define OEDT_C_STRING_RAMDISK_DIR_INV_PATH OSAL_C_STRING_DEVICE_RAMDISK"/MYFOLD1/MYFOLD2/MYFOLD3"
#define OEDT_C_STRING_SDCARD_DIR_INV_PATH OSAL_C_STRING_DEVICE_CARD"/MYFOLD1/MYFOLD2/MYFOLD3"
#define OEDT_C_STRING_USB_DIR_INV_PATH OSAL_C_STRING_DEVICE_USB"/MYFOLD1/MYFOLD2/MYFOLD3" 

#define OEDT_C_STRING_CD_DIR_CRE_PAR OSAL_C_STRING_DEVICE_CDROM"/TestCD/Creed/Weathered"
#define CREAT_FILE_PATH_CDROM  OSAL_C_STRING_DEVICE_CDROM"/TestCD/Creed/Weathered/NewFile"
#define	CREAT_FILE_PATH_FFS1 OEDTTEST_C_STRING_DEVICE_FFS1"/Testf1.txt"
#define	CREAT_FILE_PATH_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/Testf1.txt"  
#define	CREAT_FILE_PATH_RAMDISK OSAL_C_STRING_DEVICE_RAMDISK"/Testf1.txt" 
#define	CREAT_FILE_PATH_SDCARD OSAL_C_STRING_DEVICE_CARD"/Testf1.txt" 
#define	CREAT_FILE_PATH_USB OSAL_C_STRING_DEVICE_USB"/Testf1.txt" 

#define OSAL_TEXT_FILE_FIRST_CDROM OSAL_C_STRING_DEVICE_CDROM"/TestCD/00_first_folder/00_cd_test_firstfile.dat"
#define OSAL_TEXT_FILE_FIRST_FFS1 OEDTTEST_C_STRING_DEVICE_FFS1"/file1.txt"
#define OSAL_TEXT_FILE_FIRST_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/file1.txt"
#define OSAL_TEXT_FILE_FIRST_RAMDISK OSAL_C_STRING_DEVICE_RAMDISK"/file1.txt"
#define OSAL_TEXT_FILE_FIRST_SDCARD OSAL_C_STRING_DEVICE_CARD"/file1.txt"
#define OSAL_TEXT_FILE_FIRST_USB OSAL_C_STRING_DEVICE_USB"/file1.txt"

#define OEDT_C_STRING_FILE_INVPATH_CDROM OSAL_C_STRING_DEVICE_CDROM"/InvDir/Dummy.TXT"
#define OEDT_C_STRING_FILE_INVPATH_FFS1 OEDTTEST_C_STRING_DEVICE_FFS1"/Dummydir/Dummy.txt"
#define OEDT_C_STRING_FILE_INVPATH_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/Dummydir/Dummy.txt"
#define OEDT_C_STRING_FILE_INVPATH_RAMDISK OSAL_C_STRING_DEVICE_RAMDISK"/Dummydir/Dummy.txt"
#define OEDT_C_STRING_FILE_INVPATH_SDCARD OSAL_C_STRING_DEVICE_CARD"/Dummydir/Dummy.txt"
#define OEDT_C_STRING_FILE_INVPATH_USB OSAL_C_STRING_DEVICE_USB"/Dummydir/Dummy.txt"

#define OEDT_FFS1_COMNFILE OEDTTEST_C_STRING_DEVICE_FFS1"/common.txt"
#define OEDT_FFS2_COMNFILE OEDTTEST_C_STRING_DEVICE_FFS2"/common.txt"
#define OEDT_RAMDISK_COMNFILE OSAL_C_STRING_DEVICE_RAMDISK"/common.txt"
#define OEDT_SDCARD_COMNFILE OSAL_C_STRING_DEVICE_CARD"/common.txt"
#define OEDT_USB_COMNFILE OSAL_C_STRING_DEVICE_USB"/common.txt"

#define OEDT_CDROM_WRITEFILE OSAL_C_STRING_DEVICE_CDROM"/WriteFile.txt"
#define OEDT_FFS1_WRITEFILE OEDTTEST_C_STRING_DEVICE_FFS1"/WriteFile.txt"
#define OEDT_FFS2_WRITEFILE OEDTTEST_C_STRING_DEVICE_FFS2"/WriteFile.txt"
#define OEDT_RAMDISK_WRITEFILE OSAL_C_STRING_DEVICE_RAMDISK"/WriteFile.txt"
#define OEDT_SDCARD_WRITEFILE OSAL_C_STRING_DEVICE_CARD"/WriteFile.txt"
#define OEDT_USB_WRITEFILE OSAL_C_STRING_DEVICE_USB"/WriteFile.txt"

#define OEDT_CDROM_NEGATIVE_POS_TO_SET -75
#define OEDT_CDROM_BYTES_TO_READ 20
#define OEDT_CDROM_OFFSET_BEYOND_END 559

#define DIR			"File_Dir"
#define DIR_RECR1	"File_Dir2" 

#define FILE12_RECR2_FFS1 OEDTTEST_C_STRING_DEVICE_FFS1"/"DIR"/"DIR_RECR1"/File12_test.txt"
#define FILE11_FFS1 OEDTTEST_C_STRING_DEVICE_FFS1"/"DIR"/File11.txt"
#define FILE12_FFS1 OEDTTEST_C_STRING_DEVICE_FFS1"/"DIR"/File12.txt"

#define FILE12_RECR2_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/"DIR"/"DIR_RECR1"/File12_test.txt"
#define FILE11_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/"DIR"/File11.txt"
#define FILE12_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/"DIR"/File12.txt"

#define FILE12_RECR2_RAMDISK OSAL_C_STRING_DEVICE_RAMDISK"/"DIR"/"DIR_RECR1"/File12_test.txt"
#define FILE11_RAMDISK OSAL_C_STRING_DEVICE_RAMDISK"/"DIR"/File11.txt"
#define FILE12_RAMDISK OSAL_C_STRING_DEVICE_RAMDISK"/"DIR"/File12.txt"

#define FILE12_RECR2_SDCARD OSAL_C_STRING_DEVICE_CARD"/"DIR"/"DIR_RECR1"/File12_test.txt"
#define FILE11_SDCARD OSAL_C_STRING_DEVICE_CARD"/"DIR"/File11.txt"
#define FILE12_SDCARD OSAL_C_STRING_DEVICE_CARD"/"DIR"/File12.txt"

#define FILE12_RECR2_USB OSAL_C_STRING_DEVICE_USB"/"DIR"/"DIR_RECR1"/File12_test.txt"
#define FILE11_USB OSAL_C_STRING_DEVICE_USB"/"DIR"/File11.txt"
#define FILE12_USB OSAL_C_STRING_DEVICE_USB"/"DIR"/File12.txt"

#define OEDT_FFS1_MAX_FILE_PATH_LEN         260 
#define OEDT_FFS1_MAX_TOTAL_DIR_PATH_LEN    260	
#define OEDT_CDROM_CREAT_DIR_PATH OSAL_C_STRING_DEVICE_CDROM"/TestCD/Creed/Weathered/NewDir"

#define OEDT_C_STRING_DEVICE_FFS1_ROOT OEDTTEST_C_STRING_DEVICE_FFS1"/"
#define OEDT_C_STRING_DEVICE_FFS2_ROOT OEDTTEST_C_STRING_DEVICE_FFS2"/"
#define OEDT_C_STRING_DEVICE_RAMDISK_ROOT OSAL_C_STRING_DEVICE_RAMDISK"/"
#define OEDT_C_STRING_DEVICE_CARD_ROOT OSAL_C_STRING_DEVICE_CARD"/"
#define OEDT_C_STRING_DEVICE_USB_ROOT OSAL_C_STRING_DEVICE_USB"/"

#define DIR1		"File_Source"

#define FILESYSTEM_THREAD_STACK_SIZE (1024)
#define FILESYSTEM_EVENT_WAIT (0x00000003)
#define FILESYSTEM_EVENT_WAIT_TIMEOUT (110000)

#define FILE13_FFS1		OEDTTEST_C_STRING_DEVICE_FFS1"/"DIR1"/File13.txt"
#define FILE13_FFS2     OEDTTEST_C_STRING_DEVICE_FFS2"/"DIR1"/File13.txt"
#define FILE13_RAMDISK  OSAL_C_STRING_DEVICE_RAMDISK"/"DIR1"/File13.txt"
#define FILE13_SDCARD   OSAL_C_STRING_DEVICE_CARD"/"DIR1"/File13.txt"
#define FILE13_USB      OSAL_C_STRING_DEVICE_USB"/"DIR1"/File13.txt"

#define FILE14_FFS1		OEDTTEST_C_STRING_DEVICE_FFS1"/"DIR1"/File14.txt"
#define FILE14_FFS2     OEDTTEST_C_STRING_DEVICE_FFS2"/"DIR1"/File14.txt"
#define FILE14_RAMDISK  OSAL_C_STRING_DEVICE_RAMDISK"/"DIR1"/File14.txt"
#define FILE14_SDCARD   OSAL_C_STRING_DEVICE_CARD"/"DIR1"/File14.txt"
#define FILE14_USB      OSAL_C_STRING_DEVICE_USB"/"DIR1"/File14.txt"

#define FILE_OPER_SUCCESS		0
#define FILE_CREATE_FAIL		100
#define FILE_REMOVE_FAIL		200
#define COMMON_FILE_ERROR		150

#define OEDT_CDROM_ZERO_PARAMETER 0
#define OEDT_FILESYSTEM_SIZE_OF_DATA_FILE 5242880

#define FILESYSTEM_SKIP_COUNT 20
#define FILESYSTEM_BYTES_TO_READ_N 5

#define NO_OF_DEVICE 7
#define FFS3_POS 3

#define EVENT_POST_THREAD_1 (0x00000001) 
#define EVENT_POST_THREAD_2 (0x00000002)
#define EVENT_POST_THREAD_2_STOP_WAIT (0x00000004)
#define EVENT_POST_THREAD_1_STOP_WAIT (0x00000005)


	 											  
/* Function Prototypes for the functions defined in Oedt_FileSystem_TestFuncs.c */
tU32 u32FileSystemOpenClosedevice(void );
tU32 u32FileSystemOpendevInvalParm(void );
tU32 u32FileSystemReOpendev(void );
tU32 u32FileSystemOpendevDiffModes(void );
tU32 u32FileSystemClosedevAlreadyClosed(void );
tU32 u32FileSystemOpenClosedir(void );
tU32 u32FileSystemOpendirInvalid(void );
tU32 u32FileSystemCreateDelDir(void );
tU32 u32FileSystemCreateDelSubDir(void );
tU32 u32FileSystemCreateDirInvalName(void );
tU32 u32FileSystemRmNonExstngDir(void );
tU32 u32FileSystemRmDirUsingIOCTRL(void );
tU32 u32FileSystemGetDirInfo(void );
tU32 u32FileSystemOpenDirDiffModes(void );
tU32 u32FileSystemReOpenDir(void );
tU32 u32FileSystemDirParallelAccess(void);
tU32 u32FileSystemCreateDirMultiTimes(void ); 
tU32 u32FileSystemCreateRemDirInvalPath(void );
tU32 u32FileSystemCreateRmNonEmptyDir(void );
tU32 u32FileSystemCopyDir(void );
tU32 u32FileSystemMultiCreateDir(void );
tU32 u32FileSystemCreateSubDir(void);
tU32 u32FileSystemDelInvDir(void);
tU32 u32FileSystemCopyDirRec(void );
tU32 u32FileSystemRemoveDir(void);
tU32 u32FileSystemFileCreateDel(void );
tU32 u32FileSystemFileOpenClose(void );
tU32 u32FileSystemFileOpenInvalPath(void );
tU32 u32FileSystemFileOpenInvalParam(void );
tU32 u32FileSystemFileReOpen(void );
tU32 u32FileSystemFileRead(void );
tU32 u32FileSystemFileWrite(void );
tU32 u32FileSystemGetPosFrmBOF(void );
tU32 u32FileSystemGetPosFrmEOF(void );
tU32 u32FileSystemFileReadNegOffsetFrmBOF(void );
tU32 u32FileSystemFileReadOffsetBeyondEOF(void );
tU32 u32FileSystemFileReadOffsetFrmBOF(void );
tU32 u32FileSystemFileReadOffsetFrmEOF(void );
tU32 u32FileSystemFileReadEveryNthByteFrmBOF(void );
tU32 u32FileSystemFileReadEveryNthByteFrmEOF(void );
tU32 u32FileSystemGetFileCRC(void );

tU32 u32FileSystemReadAsync(tVoid);
tU32 u32FileSystemLargeReadAsync(tVoid);
tU32 u32FileSystemWriteAsync(tVoid);
tU32 u32FileSystemLargeWriteAsync(tVoid);
tU32 u32FileSystemWriteOddBuffer(tVoid);
tU32 u32FileSystemFileMultipleHandles(tVoid);
tU32 u32FileSystemWriteFileWithInvalidSize(tVoid);
tU32 u32FileSystemWriteFileInvalidBuffer(tVoid);
tU32 u32FileSystemWriteFileStepByStep(tVoid);
tU32 u32FileSystemGetFreeSpace (tVoid);
tU32 u32FileSystemGetTotalSpace (tVoid);
tU32 u32FileSystemFileOpenCloseNonExstng(tVoid);
tU32 u32FileSystemFileDelWithoutClose(tVoid);
tU32 u32FileSystemSetFilePosDiffOff(tVoid);
tU32 u32FileSystemCreateFileMultiTimes(void);
tU32 u32FileSystemFileCreateUnicodeName(tVoid);
tU32 u32FileSystemFileCreateInvalName(tVoid);
tU32 u32FileSystemFileCreateLongName(tVoid);
tU32 u32FileSystemFileCreateDiffModes(tVoid);
tU32 u32FileSystemFileCreateInvalPath(tVoid);
tU32 u32FileSystemStringSearch(tVoid);
tU32 u32FileSystemFileOpenDiffModes(tVoid);
tU32 u32FileSystemFileReadAccessCheck(tVoid);
tU32 u32FileSystemFileWriteAccessCheck(tVoid);
tU32 u32FileSystemFileReadSubDir(tVoid);
tU32 u32FileSystemFileWriteSubDir(tVoid);
tU32 u32FileSystemReadWriteTimeMeasureof1KbFile(tVoid);
tU32 u32FileSystemReadWriteTimeMeasureof10KbFile(tVoid);
tU32 u32FileSystemReadWriteTimeMeasureof100KbFile(tVoid);
tU32 u32FileSystemReadWriteTimeMeasureof1MBFile(tVoid);
tU32 u32FileSystemReadWriteTimeMeasureof1KbFilefor10000times(tVoid);
tU32 u32FileSystemReadWriteTimeMeasureof10KbFilefor1000times(tVoid);
tU32 u32FileSystemReadWriteTimeMeasureof100KbFilefor100times(tVoid);
tU32 u32FileSystemReadWriteTimeMeasureof1MBFilefor16times(tVoid);
tU32 u32FileSystemFileThroughPutFirstFile(tVoid);
tU32 u32FileSystemFileThroughPutSecondFile(tVoid);
tU32 u32FileSystemFileThroughPutMP3File(tVoid);
tU32 u32FileSystemRenameFile(tVoid);
tU32 u32FileSystemWriteFrmBOF(tVoid);
tU32 u32FileSystemWriteFrmEOF(tVoid);
tU32 u32FileSystemLargeFileRead(tVoid);
tU32 u32FileSystemLargeFileWrite(tVoid);
tU32 u32FileSystemOpenCloseMultiThread(tVoid);
tU32 u32FileSystemWriteMultiThread(tVoid);
tU32 u32FileSystemWriteReadMultiThread(tVoid);
tU32 u32FileSystemReadMultiThread(tVoid);
tU32 u32FileSystemFormat(tVoid);
tU32 u32FileSystemCheckDisk(tVoid);

#endif
