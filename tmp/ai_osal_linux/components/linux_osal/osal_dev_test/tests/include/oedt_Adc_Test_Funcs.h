/******************************************************************************
 *FILE         : oedt_ADC_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_ADC_TestFuncs.c for the  
 *               ADC device for the Gen3 hardware.
 *
 *AUTHOR       : Haribabu Sannapaneni (RBEI/ECM1) 
 *
 *HISTORY      : Created by Haribabu Sannapaneni (RBEI/ECM1) on 4, April,2008
				 version 0.1
				 version 0.2 Haribabu Sannapaneni (RBEI/ECM1) on 2,July,2008
				             Added new testcase proto types.
                 Ported to Linux by Basavaraj Nagar(RBEI/ECG4)
				            
*******************************************************************************/

#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	
#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>

#ifndef OEDT_ADC_TESTFUNCS_HEADER
#define OEDT_ADC_TESTFUNCS_HEADER

/* Macro definition used in the code */
/* Macro definition used in the code */
#define MQ_ADC_MP_CONTROL_MAX_MSG     10
#define MQ_ADC_MP_CONTROL_MAX_LEN     sizeof(tU32)
#define MQ_ADC_MP_RESPONSE_NAME       "MQ_ADC_RSPNS"

/* Function prototypes of functions used in file oedt_ADC_TestFuns.c */

tU32 OpenCloseADCSubUnit(tVoid);
tU32 u32ADCChanReOpen(tVoid);
tU32 u32ADCChanCloseAlreadyClosed(tVoid);
tU32 u32ADCRead();
tU32 u32ADCSetSingleThresholdCallbackCH5(tVoid);
tU32 u32ADCGetResolution(tVoid);
tU32 u32ADClogicalDualthreadRead(tVoid);
tU32 u32ADCUnsupportedIoctrlCheck(tVoid);
static tVoid Parallel_ADC_Open_Thread_1(tPVoid pvThread1Arg);
static tVoid Parallel_ADC_Open_Thread_2(tPVoid pvThread2Arg);
tU32 u32ADCMultiProcessTest(tVoid);


#endif

