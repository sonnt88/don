/******************************************************************************
 *FILE         : oedt_FFD_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file has the function declaration for fast flash driver test cases. 
 *               
 *AUTHOR       : Andrea Bueter
 *
 *COPYRIGHT    : (c) 2007 TMS GmbH
 *
 *HISTORY      : 24.10.07 .Initial version
 *
 *****************************************************************************/

#ifndef OEDT_FFD_TESTFUNCS_HEADER
#define OEDT_FFD_TESTFUNCS_HEADER

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* !!!-----------------------DEV-FFD --------------------------!!! */
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
tU32 u32FFDDevOpenClose(void);
tU32 u32FFDDevOpenWithInvalidParam(void);
tU32 u32FFDDevCloseWithInvalidParam(void);
tU32 u32FFDDevOpenCloseSeveralTimes(void);
tU32 u32FFDDevWriteRead(void);
tU32 u32FFDDevWriteReadInvalidParam(void);
tU32 u32FFDDevGetRawSize(void);
tU32 u32FFDDevGetRawSizeInvalidParam(void);
tU32 u32FFDDevGetRawData(void);
tU32 u32FFDDevGetRawDataInvalidParam(void);
tU32 u32FFDDevSetRawData(void);
tU32 u32FFDDevSetRawDataInvalidParam(void);
tU32 u32FFDDevSaveNow(void);
tU32 u32FFDDevSaveNowInvalidParam(void);
tU32 u32FFDDevReload(void);
tU32 u32FFDDevReloadInvalidParam(void);
tU32 u32FFDDevSaveReloadCompare(void);
tU32 u32FFDDevSeek(void);
tU32 u32FFDDevSeekInvalidParam(void);
tU32 u32FFDDevWriteReadWithSeek(void);
tU32 u32FFDDevGetRomDataVersion(void);
tU32 u32FFDDevGetRomDataVersionInvalidParam(void);
tU32 u32FFDDevGetTotolFlashSize(void);
tU32 u32FFDDevGetTotolFlashSizeInvalidParam(void);
tU32 u32FFDDevGetDataSetSize(void);
tU32 u32FFDDevGetDataSetSizeInvalidParam(void);
tU32 u32FFDDevSaveCompleteFlashArea(void);
tU32 u32FFDDevReadSaveAssertMode(void);
tU32 u32FFDDevSaveBackupFile(void);
tU32 u32FFDDevSaveBackupFileInvalidParam(void);
tU32 u32FFDDevLoadBackupFile(void);
tU32 u32FFDDevLoadBackupFileInvalidParam(void);
tU32 u32FFDDevGetInfoReadData(void);
tU32 u32FFDDevGetInfoReadDataInvalidParam(void);
#if defined(LSIM) || defined(GEN3X86)
tU32 u32FFDDevLSIMWriteBeforeReboot(void);
tU32 u32FFDDevLSIMReadAfterReboot(void);
#endif
#endif


