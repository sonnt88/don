/******************************************************************************
 *FILE         : oedt_usbdnl_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk(osal_dev_test)
 *
 *DESCRIPTION  : This file contains function prototypes and macros that 
 				 will be used in the file oedt_usbdnl_TestFuncs.c 

 *AUTHOR       : Pankaj Kumar (RBIN/EDI3) 

 *COPYRIGHT    : (c) 2007 Robert Bosch India Limited (RBIN)

 *HISTORY      : 		10-Dec-2007 -Initial Version - Pankaj Kumar(RBIN/EDI3)
						27-05-2008 update by-Anoop Chandran (RBEI/ECM1)
						Added new test csae
				  1.2 version ,31-01-2009 - Added new test case TU_OEDT_USBDNL_029 
				  Anoop Chandran(RBEI/ECF1)
*******************************************************************************/

#ifndef OEDT_USBDNL_TESTFUNCS_HEADER
#define OEDT_USBDNL_TESTFUNCS_HEADER


tU32 u32usbdnlOEDT_DeviceOpenClose(void);			//TU_OEDT_USBDNL_001
tU32 U32usbdnlOEDT_DeviceMultipleOpen(void);		//TU_OEDT_USBDNL_002
tU32 U32usbdnlOEDT_DeviceMultipleClose(void);		//TU_OEDT_USBDNL_003
tU32 U32usbdnlOEDT_DeviceOpenCloseLoop(void);		//TU_OEDT_USBDNL_004
tU32 U32usbdnlOEDT_FileOpen(void);					//TU_OEDT_USBDNL_005
tU32 U32usbdnlOEDT_FileMultipleOpen(void);	    	//TU_OEDT_USBDNL_006
tU32 U32usbdnlOEDT_FileClose(void);					//TU_OEDT_USBDNL_007
tU32 U32usbdnlOEDT_FileMultipleClose(void);			//TU_OEDT_USBDNL_008
tU32 U32usbdnlOEDT_FileOpenCloseLoop(void);			//TU_OEDT_USBDNL_009
tU32 U32usbdnlOEDT_Read0Byte(void);					//TU_OEDT_USBDNL_010
tU32 U32usbdnlOEDT_Read1Byte(void);					//TU_OEDT_USBDNL_011
tU32 U32usbdnlOEDT_Read63Bytes(void);				//TU_OEDT_USBDNL_012
tU32 U32usbdnlOEDT_Read64Bytes(void);				//TU_OEDT_USBDNL_013
tU32 U32usbdnlOEDT_Read65Bytes(void);				//TU_OEDT_USBDNL_014
tU32 U32usbdnlOEDT_Read4KBytes(void);				//TU_OEDT_USBDNL_015
tU32 U32usbdnlOEDT_Read8KBytes(void);				//TU_OEDT_USBDNL_016
tU32 U32usbdnlOEDT_Read1MBytes(void);				//TU_OEDT_USBDNL_017
tU32 U32usbdnlOEDT_SeekReadTest(void);				//TU_OEDT_USBDNL_018
tU32 U32usbdnlOEDT_SeekReadMultipleFiles(void);		//TU_OEDT_USBDNL_019
tU32 U32usbdnlOEDT_IOCTRL_Version(void);			//TU_OEDT_USBDNL_020
tU32 U32usbdnlOEDT_IOCTRL_LinkStatusCheck(void);	//TU_OEDT_USBDNL_021
tU32 U32usbdnlOEDT_IOCTRL_LinkStatusWait(void);		//TU_OEDT_USBDNL_022
tU32 U32usbdnlOEDT_IOCTRL_ReadBreakTimeSet(void);	//TU_OEDT_USBDNL_023
tU32 U32usbdnlOEDT_IOCTRL_FileRelatedTest(void);   	//TU_OEDT_USBDNL_024
tU32 U32usbdnlOEDT_IOCTRL_usbBoardID(void);		  	//TU_OEDT_USBDNL_025
tU32 U32usbdnlOEDT_IOCTRL_usbRawData(void);		  	//TU_OEDT_USBDNL_026
tU32 U32usbdnlOEDT_IOCTRL_InterfaceInvalPara(void);	//TU_OEDT_USBDNL_027
tU32 U32usbdnlOEDT_ReadLargeFileInChunks(void);		//TU_OEDT_USBDNL_028
tU32 U32usbdnlOEDT_IOCTRL_Fseek_FIONREAD(void);				//TU_OEDT_USBDNL_029

#endif  /* #ifndef USBDNL_OEDTFUNCTIONS_H */

