/******************************************************************************
 *FILE         : odt_Cdctrl_TestFuncs.h
 *
 *SW-COMPONENT : ODT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for the Cdctrl device.
 *                also the array of function pointer for this device has been 
 *                initialised.
 *
 *AUTHOR       : Pemmaiah BD
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : Initial  Version
 *               15.05.06 ver  1.0  RBIN/ECM1-Narasimha Prasad Palasani
 *               OEDT-CD-Ctrl Test functions Prototypes
 *               23.05.06 ver 1.1 	RBIN/ECM1-Narasimha Prasad Palasani
 *               prototypes are updated		 
 *****************************************************************************/
#ifndef OEDT_CDCTRL_TESTFUNCS_HEADER
#define OEDT_CDCTRL_TESTFUNCS_HEADER


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/
tBool bReleaseCdctrlResource(tVoid* pvThrdID);

/*****************************************************************
| function prototypes (scope: local)
|----------------------------------------------------------------*/
/* CDCtrl prototype declarations   */

tU32 u32CDCtrlDevOpen(void);
tU32 u32CDCtrlOpenDevDiffModes(void);
tU32 u32CDCtrlDevOpenInvalParam(void);
tU32 u32CDCtrlDevReOpen(void);
tU32 u32CDCtrlDevOpenNullParam(void);
tU32 u32CDCtrlDevClose(void);
tU32 u32CDCtrlDevCloseInvalParam(void);
tU32 u32CDCtrlDevReClose(void);
tU32 u32CDCtrlLoadMedia(void);
tU32 u32CDCtrlLoadMediaInvalParam(void);
tU32 u32CDCtrlLoadMediaAfterDevClose(void);
tU32 u32CDCtrlMotorOn(void);
tU32 u32CDCtrlMotorOnInvalParam(void);
tU32 u32CDCtrlMotorOnAfterDevClose(void);
tU32 u32CDCtrlMotorOff(void);
tU32 u32CDCtrlMotorOffInvalParam(void);
tU32 u32CDCtrlMotorOffAfterDevClose(void);
tU32 u32CDCtrlLoadStat(void);
tU32 u32CDCtrlLoadStatInvalParam(void);
tU32 u32CDCtrlLoadStatAfterDevClose(void);
tU32 u32CDCtrlGetDriveTemp(void);
tU32 u32CDCtrlGetDriveTempInvalParam(void);
tU32 u32CDCtrlGetDriveTempAfterDevClose(void);
tU32 u32CDCtrlGetMediaInfo(void);
tU32 u32CDCtrlGetMediaInfoInvalParam(void);
tU32 u32CDCtrlGetMediaInfoAfterDevClose(void);
tU32 u32CDCtrlGetCDInfo(void);
tU32 u32CDCtrlGetCDInfoInvalParam(void);
tU32 u32CDCtrlGetCDInfoAfterDevClose(void);
tU32 u32CDCtrlGetTrackInfo(void);
tU32 u32CDCtrlGetTrackInfoInvalParam(void);
tU32 u32CDCtrlGetTrackInfoAfterDevClose(void);
tU32 u32CDCtrlGetErrInfo(void);
tU32 u32CDCtrlGetErrInfoInvalParam(void);
tU32 u32CDCtrlGetErrInfoAfterDevClose(void);
tU32 u32CDCtrlRawDataRead(void);
tU32 u32CDCtrlRawDataReadInvalidParam(void);
tU32 u32CDCtrlRawDataReadAfterDevClose(void);
tU32 u32CDCtrlMSFRead(void);
tU32 u32CDCtrlMSFReadInvalidParam(void);
tU32 u32CDCtrlMSFReadAfterDevClose(void);
tU32 u32CDCtrlGetVer(void);
tU32 u32CDCtrlGetVerInvalParam(void);
tU32 u32CDCtrlGetVerAfterDevClose(void);
tU32 u32CDCtrlGetDVDInfo(void);
tU32 u32CDCtrlGetDVDInfoInvalParam(void);
tU32 u32CDCtrlGetDVDInfoAfterDevClose(void);
tU32 u32CDCtrlGetDevInfo(void);
tU32 u32CDCtrGetDevInfoInvalParam(void);
tU32 u32CDCtrlGetDevInfoAfterDevClose(void);
tU32 u32CDCtrlGetDiskInfo(void);
tU32 u32CDCtrlGetDiskInfoInvalParam(void);
tU32 u32CDCtrlGetDiskInfoAfterDevClose(void);
tU32 u32CDCtrlRegUnregCallBackDiagMedChange(void);
tU32 u32CDCtrlRegUnregCallBackDiagTotFailure(void);
tU32 u32CDCtrlRegUnregCallBackDiagModeChange(void);
tU32 u32CDCtrlRegUnregCallBackDiagMedState(void);
tU32 u32CDCtrlRegUnregCallBackDiagDvdOvrTemp(void);
tU32 u32CDCtrlRegUnregCallBackDiagNotiAll(void);
tU32 u32CDCtrlRegUnregCallBackDownMedChange(void);
tU32 u32CDCtrlRegUnregCallBackDownTotFailure(void);
tU32 u32CDCtrlRegUnregCallBackDownModeChange(void);
tU32 u32CDCtrlRegUnregCallBackDownMedState(void);
tU32 u32CDCtrlRegUnregCallBackDownDvdOvrTemp(void);
tU32 u32CDCtrlRegUnregCallBackDownNotiAll(void);
tU32 u32CDCtrlRegUnregCallBackDapiMedState(void);
tU32 u32CDCtrlRegUnregCallBackDapiTotFailure(void);
tU32 u32CDCtrlRegUnregCallBackDapiModeChange(void);
tU32 u32CDCtrlRegUnregCallBackDapiMedChange(void);
tU32 u32CDCtrlRegUnregCallBackDapiDvdOvrTemp(void);
tU32 u32CDCtrlRegUnregCallBackDapiNotiAll(void);
tU32 u32CDCtrlRegUnregCallBackOtherMedChange(void);
tU32 u32CDCtrlRegUnregCallBackOtherTotFailure(void);
tU32 u32CDCtrlRegUnregCallBackOtherModeChange(void);
tU32 u32CDCtrlRegUnregCallBackOtherMedState(void);
tU32 u32CDCtrlRegUnregCallBackOtherDvdOvrTemp(void);
tU32 u32CDCtrlRegUnregCallBackOtherNotiAll(void);
tU32 u32CDCtrlRegUnregCallBackInvalMedChange(void);
tU32 u32CDCtrlRegUnregCallBackInvalTotFailure(void);
tU32 u32CDCtrlRegUnregCallBackInvalModeChange(void);
tU32 u32CDCtrlRegUnregCallBackInvalMedState(void);
tU32 u32CDCtrlRegUnregCallBackInvalDvdOvrTemp(void);
tU32 u32CDCtrlRegUnregCallBackInvalNotiAll(void);
tU32 u32CDCtrlEjectMedia(void);
tU32 u32CDCtrlEjectMediaInvalParam(void);
tU32 u32CDCtrlEjectMedAfterDevClose(void);
tU32 u32CDCtrlEjectLockInvalParam(void);
tU32 u32CDCtrlEjectLockAfterDevClose(void);
tU32 u32CDCtrlEjectLock(void);
tU32 u32CDCtrlEjectUnlock(void);
tU32 u32CDCtrlMultiDevOpen(void);
tU32 u32CDCtrlCopyProtection(void);
tU32 u32CDCtrlPowerOffInvalParam(void);
tU32 u32CDCtrlPowerOffAfterDevClose(void);
tU32 u32CDCtrlPowerOff(void);
tU32 u32CDCtrlGetDeviceVersion(void);
tU32 u32CDCtrlGetDeviceVersionInvalParam(void);
tU32 u32CDCtrlGetDeviceVersionAfterDevClose(void);


#endif
