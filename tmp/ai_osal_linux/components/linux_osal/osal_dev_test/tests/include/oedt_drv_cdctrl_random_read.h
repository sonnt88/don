/*!
 * \file odt_drv_cdctrl_random_read.h
 *
 * \brief OEDT test for drv_cdctrl
 *
 * \author CM-DI/ESP Schepers
 *
 * \par COPYRIGHT (c) 2006 Blaupunkt GmbH
 *
 * \date 2006-08-23
 */
#ifndef OEDT_DRV_CDCTRL_RANDOM_READ_HEADER
#define OEDT_DRV_CDCTRL_RANDOM_READ_HEADER

extern tU32 u32TestCdCtrl0RandomRead(void);
extern tU32 u32TestCdCtrl0RandomReadWithVerify(void);
extern tU32 u32TestCdCtrl0RecordedRead(void);
extern tU32 u32TestCdCtrl0SequentialRead(void);
extern tU32 u32TestCdCtrl0CompleteRead(void);
extern tU32 u32TestCdCtrl1RandomRead(void);
extern tU32 u32TestCdCtrl1RandomReadWithVerify(void);
extern tU32 u32TestCdCtrl1SequentialRead(void);
extern tU32 u32TestCdCtrl1CompleteRead(void);
extern tU32 u32TestFilesystemRecordedRead(void);


#endif //OEDT_DRV_CDCTRL_RANDOM_READ_HEADER


