/******************************************************************************
*FILE         : oedt_rtc_TestFuncs.h
*
*SW-COMPONENT : OEDT_FrmWrk 
*
*DESCRIPTION  : This file has the function declarations for aux clock test cases 
*               Ref:SW Test specification 0.1 draft
*               
*AUTHOR       :Ravindran P(CM-DI/EAP)
*
*COPYRIGHT    : (c) 2006 Blaupunkt Werke GmbH
*
*HISTORY      : 
                        26.05.10. v1.2
                        26.06.08 .v1.1
*                       - tny1kor 
*                       11.10.06 .Initial version
*                       - Kranthi Kiran (RBEI/ECF5)
*                       08.02.2014 Lint Fix
*****************************************************************************/
#ifndef OEDT_RTC_TESTFUNCS_HEADER
#define OEDT_RTC_TESTFUNCS_HEADER
#include "oedt_helper_funcs.h"

#define MQ_RTC_MP_RESPONSE_NAME       "MQ_RTC_RSPNS"
#define MQ_RTC_MP_CONTROL_MAX_MSG     10
#define MQ_RTC_MP_CONTROL_MAX_LEN     sizeof(tU32)

#undef INVALID_PID
#define INVALID_PID          ((tS32)-100)


#define RTC_DEVICE_OPEN_ERROR                 35
#define RTC_DEVICE_CLOSE_ERROR                36
#define RTC_DEVICE_READ_ERROR                 37
#define RTC_DEVICE_WRITE_ERROR                38
#define RTC_DEVICE_TIME_VALID_ERROR           39
#define RTC_DEVICE_TIME_WRITE_READ_BACK_ERROR 40
#define RTC_DEVICE_MULTIPLE_DEVICE_ERROR      41
#define RTC_DEVICE_TIME_EQUAL_ERROR           42
#define RTC_DEVICE_TIME_ADVANCE_ERROR         43
#define RTC_DEVICE_OFFSET_CALCULATION_ERROR   44
#define RTC_DEVICE_RDS_POSITIVE_OFFSET_ERROR  45
#define RTC_DEVICE_RDS_NEGATIVE_OFFSET_ERROR  46
#define RTC_DEVICE_BASE_POSITIVE_OFFSET_ERROR 47
#define RTC_DEVICE_BASE_NEGATIVE_OFFSET_ERROR 48
#define RTC_DEVICE_ILLEGAL_TIME_ERROR         49
#define RTC_DEVICE_ILLEGAL_UPDATE_RATE_ERROR  50
#define RTC_DEVICE_GET_VERSION_ERROR          51
#define RTC_DEVICE_TIME_STATE_ERROR           52
#define RTC_DEVICE_OFFSET_RECALCULATION_ERROR 53
#define RTC_DEVICE_TIME_ROLLOVER_ERROR        54
#define RTC_DEVICE_SET_GPS_TIME_ERROR         55
#define RTC_DEVICE_SET_RDS_TIME_ERROR         56
#define RTC_DEVICE_SET_BASE_TIME_ERROR        57
#define RTC_DEVICE_UPDATE_RATE_ERROR          58
#define RTC_DEVICE_GET_TIME_VALIDITY_ERROR    59
#define RTC_DEVICE_GET_CONFIG_INFO_ERROR      60
#define RTC_DEVICE_GET_RTC_TIME_ERROR         61

#define  TEST_THREAD_STACK_SIZE           (8*1024)
#define RTC_MAX_YRS_PRODUCTION            102     
#define RTC_OSAL_OFFSET_DEFAULT_YEAR      110

#define PRIORITY                          100


#define RTC_MAX_UPDATE_RATE  60 /*Max update rate in seconds for IO Read*/
#define RTC_DEFAULT_UPDATE_RATE 1 /*Default update rate in seconds for IO Read*/

static tU8 aRtcErr_oedt[] = {
   RTC_DEVICE_OPEN_ERROR ,//0
   RTC_DEVICE_CLOSE_ERROR ,//1 
   RTC_DEVICE_READ_ERROR,  //2
   RTC_DEVICE_WRITE_ERROR, //3
   RTC_DEVICE_TIME_VALID_ERROR ,//4
   RTC_DEVICE_TIME_WRITE_READ_BACK_ERROR ,//5
   RTC_DEVICE_MULTIPLE_DEVICE_ERROR,//6
   RTC_DEVICE_TIME_EQUAL_ERROR,//7
   RTC_DEVICE_TIME_ADVANCE_ERROR,//8
   RTC_DEVICE_OFFSET_CALCULATION_ERROR,//9
   RTC_DEVICE_RDS_POSITIVE_OFFSET_ERROR,//10
   RTC_DEVICE_RDS_NEGATIVE_OFFSET_ERROR,//11
   RTC_DEVICE_BASE_POSITIVE_OFFSET_ERROR,//12
   RTC_DEVICE_BASE_NEGATIVE_OFFSET_ERROR,//13
   RTC_DEVICE_ILLEGAL_TIME_ERROR,//14
   RTC_DEVICE_ILLEGAL_UPDATE_RATE_ERROR,//15
   RTC_DEVICE_GET_VERSION_ERROR,//16
   RTC_DEVICE_TIME_STATE_ERROR,//17
   RTC_DEVICE_TIME_ROLLOVER_ERROR,//18
   RTC_DEVICE_SET_GPS_TIME_ERROR,//19
   RTC_DEVICE_SET_RDS_TIME_ERROR,//20
   RTC_DEVICE_SET_BASE_TIME_ERROR,//21
   RTC_DEVICE_UPDATE_RATE_ERROR,//22

};

static OSAL_trTimeDate rRTCIllegalTimes[5] =
{
   { 72 ,108 ,36 ,46 ,82 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0}, 
   { 1 ,105 ,34 ,42 ,96 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   { 3 ,3 ,38 ,56 ,78 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0}, 
   { 5 ,5 ,3 ,60 ,45 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0},
   { 7 ,7 ,4 ,5 ,78 ,RTC_OSAL_OFFSET_DEFAULT_YEAR,0, 0, 0} 
};           


static OSAL_trTimeDate rRTCDateTimeSetGps = {30, 30, 12, 31, 01 , RTC_OSAL_OFFSET_DEFAULT_YEAR, 0, 0, 0};


/*Data Types*/

tU32 tu32Gen2RTCMulOpenClose( void );
tU32 tu32Gen2RTCMultipleRead( void );
tU32 tu32Gen2RTCSetGpsTime( void );
tU32 tu32Gen2RTCGpsTimePersistance( void );
tU32 tu32Gen2RTCSetGpsTimeRegressionSet( void );
tU32 tu32Gen2RTCGpsTimeRegressionRead( void );
tU32 tu32Gen2RTCGpsTimeSetReadStressTest( void );
tU32 tu32Gen2RTCGpsTimeAfterBatteryOut( void );
tU32 tu32Gen2RTCSetGpsTimeIllegalRegressionSet( void );
tU32 tu32Gen2RTCNonExistentTime( void );
tU32 tu32Gen2RTCReadVersion( void );
tU32 tu32Gen2RTCReadVersionInval( void );
tU32 tu32Gen2RTCReadInval( void );
tU32 tu32Gen2RTCReadWithoutOpen( void );
tU32 tu32Gen2RTCReadExceedMax( void );
tU32 tu32Gen2RTCSetGPSTimeNoOpen( void );
tU32 tu32Gen2RTCOpenCloseDiffContext( void );
tU32 tu32Gen2RTCGpsTimeStateValid( void );
tU32 tu32Gen2RTCGpsTimeStateEverSync( void );

tU32 u32MultiThread_OpenClose( tVoid );
tU32 u32MultiThread_SetGpsTime( tVoid );
tU32 u32MultiThread_GetVersion( tVoid );
tU32 u32MultiThread_RtcIORead( tVoid );
tU32 u32MultiThread_MultiFunctionTest( tVoid );

//tU32 u32RTCMultiProcessTest(tVoid);




#endif

