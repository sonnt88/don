#ifndef OEDT_IOSC_MQ_COMMON_H
#define OEDT_IOSC_MQ_COMMON_H

#include <oedt_testing_macros.h>

#define MQ_OSALTEST_TaskPri 101
#define TEST1 	1
#define TEST2 	2
#define TEST3 	3
#define TEST4 	4
#define TEST5 	5
#define TEST6   6

#define MQ_THR_NAME_1 				"MQ_Thread_1"
#define MQ_THR_NAME_2 				"MQ_Thread_2"
#define SEM1NAME					"BASE_SEM1"
#define SEM2NAME					"BASE_SEM2"
#define MQ_TR_STACK_SIZE			2048
#define IOSC_TEST_MQ_CHANNEL		"IOSC_TEST_MQ"
#define MQ_OSALTEST_TaskPri 		101
#define MQ_OSALTEST_TaskStack 		(8 * 1024)

tS32 vCreDelMQSequence1(void);

tS32 vCreDelMQSequence2(void);

tS32 vCreDelMQSequence3(void);

tS32 vCreDelMQSequence4(void);

tS32 vPostRecSequence1(void);

tS32 vPostRecSequence2(void);

tVoid vOsalIoscMQTestTsk2(tPVoid pvArg);

#endif //OEDT_IOSC_MQ_COMMON_H

