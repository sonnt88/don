/**********************************************************FileHeaderBegin******
 *
 * FILE:        oedt_memory_testfuncs.h
 *
 * CREATED:     2005-03-08
 *
 * AUTHOR:      Ulrich Schulz / TMS
 *
 * DESCRIPTION:
 *      OEDT tests for the memory  management system
 *
 * NOTES:
 *      -
 *
 * COPYRIGHT: TMS GmbH Hildesheim. All Rights reserved!
 * HISTORY    Ported by Haribabu Sannapaneni(RBEI/ECM1) on 13, October,2008
              for ADIT platform
			  Version - 1.1
 **********************************************************FileHeaderEnd*******/
/* TENGINE Header */
#ifndef OEDT_STACK_TESTFUNC_HEADER
#define OEDT_STACK_TESTFUNC_HEADER

tU32 vStackOverFlowTest(tVoid);

#endif //OEDT_MEMORY_TESTFUNC_HEADER


