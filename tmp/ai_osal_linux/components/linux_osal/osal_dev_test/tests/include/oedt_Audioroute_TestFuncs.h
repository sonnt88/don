/******************************************************************************
*FILE         : oedt_Audioroute_TestFuncs.h
*
*SW-COMPONENT : OEDT_FrmWrk
*
*DESCRIPTION  : This file is the header file for Audio Route Testing Functions.
*
*AUTHOR       : Kranthi Kiran (RBEI/ECF5)
*
*HISTORY:      
*------------------------------------------------------------------------
* Date        |       Version       | Author & comments
*------------ |---------------------|--------------------------------------
* 19 Aug,2015 |    Initial Version  | Kranthi Kiran(RBEI/ECF5)
*--------------------------------------------------------------------------
* 04 Mar,2016 | In order to add the OEDTs to LSIM some modifications have been done.
*************************************************************************************/

#ifndef OEDT_AUDIOROUTE_TESTFUNCS_H
#define OEDT_AUDIOROUTE_TESTFUNCS_H

#include <stdio.h>
#include <alsa/asoundlib.h>
#include "osal_if.h"

typedef struct
{
  char *DeviceName;
  int Channels;
  unsigned int SampleRate;
  char * FilePath;
  tU32 RetValue;
}OsalAlsaParameter;
static void vWriteToAlsa(tPVoid pvData );
static void vReadFromAlsa(tPVoid pvData);
static int iHandleXRUN(snd_pcm_t* AlsaPCM,int iRet);

/*Macros for Filepaths */
#define OEDT_FILEPATH_PLAYBACK_STEREO_DEVICE "/etc/PCMFiles/playback_stereo.pcm"
#define OEDT_FILEPATH_PLAYBACK_MONO_DEVICE "/etc/PCMFiles/playback_mono.pcm"
#define OEDT_FILEPATH_CAPTURE_DEVICE "/etc/PCMFiles/capture.pcm"

/* Macros For PCM devices */
#define OEDT_PCM_DEVICE_AdevEnt1Out             "AdevEnt1Out"
#define OEDT_PCM_DEVICE_AdevAcousticoutSpeech   "AdevAcousticoutSpeech"
#define OEDT_PCM_DEVICE_AdevMPOut               "AdevMPOut"
#define OEDT_PCM_DEVICE_AdevBTAudio             "AdevBTAudio"
#define OEDT_PCM_DEVICE_AdevAudioSampler1Out    "AdevAudioSampler1Out"
#define OEDT_PCM_DEVICE_AdevAudioSampler2Out    "AdevAudioSampler2Out"
#define OEDT_PCM_DEVICE_AdevAcousticinSpeech    "AdevAcousticinSpeech"
#define OEDT_PCM_DEVICE_AdevVoiceOut            "AdevVoiceOut"         
#define OEDT_PCM_DEVICE_AdevMicro12AmpRef       "AdevMicro12AmpRef"
#define OEDT_PCM_DEVICE_AdevMicro1In            "AdevMicro1In"
#define OEDT_PCM_DEVICE_AdevAudioInfoMonoOut    "AdevAudioInfoMonoOut"
#define OEDT_PCM_DEVICE_AdevAudioSampleStereo   "AdevAudioSampleStereo"
#define OEDT_PCM_DEVICE_AdevEsaiClockOnOff      "AdevEsaiClockOnOff"

/* Macros for Sample Rate */
#define OEDT_SAMPLERATE_MONO ((tU32)22050)
#define OEDT_SAMPLERATE_STEREO ((tU32)48000)

/* Number of channels */
#define MONO 1
#define STEREO 2
#define MULTICHANNEL 3
 tU32 OEDT_AUDIO_ROUTE_TEST_00(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_01(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_02(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_03(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_04(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_05(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_06(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_07(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_08(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_09(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_11(void);
 #ifndef GEN3X86
 tU32 OEDT_AUDIO_ROUTE_TEST_10(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_12(void);
 tU32 OEDT_AUDIO_ROUTE_TEST_13(void);
 #endif
 tU32 OEDT_AUDIO_ROUTE_TEST_14(void);
 #endif
 