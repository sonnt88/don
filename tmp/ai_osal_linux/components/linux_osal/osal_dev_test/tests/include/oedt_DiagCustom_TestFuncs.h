/******************************************************************************
 *FILE         : oedt_DiagCustom_TestFuncs.h
 *
 *
 *SW-COMPONENT : OEDT_FrameWork 
 *
 *
 *DESCRIPTION  : This file has all defines for the source of
 *               the OEDT_DEV.
 *               
 *AUTHOR       : Anoop chandran(RBIN/EDI3)
 *
 *
 *COPYRIGHT    : 2007, Robert Bosch India Limited
 *
 *HISTORY      : Version 1.0 , 09- 11- 2007
               : Version 1.1 , 25-03-2009
			     Sainath Kalpuri (RBEI/ECF1) - Removed lint and compiler
				 warnings
 *					
 *
 *****************************************************************************/

/*****************************************************************
| includes: 
|   1)system- and project- includes
|   2)needed interfaces from external components
|...3)internal and external interfaces from this component
|----------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define OSAL_C_TRACELEVEL1  TR_LEVEL_USER1



/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/
#ifndef OEDT_DIAGCUSTOM_TESTFUNCS_HEADER
#define OEDT_DIAGCUSTOM_TESTFUNCS_HEADER

#define OEDT_DIAGC_STRING_INVALID_ACCESS_MODE   		-1
#define OEDT_DIAGC_ZERO 								0
#define OEDT_DIAGC_WRITE_DATA							"Data to DIAGC"
#define OEDT_DIAGC_ECX_BUFF_LEN							10
#define BUFFSIZE										50
#define OEDT_DIAGC_SECOND_10							10000
#define OEDT_DIAGC_OFFSET_MS							2000
#define OEDT_DIAGC_CALLBACK								1
#define OEDT_DIAGC_CSMERROR								2
#define OEDT_DIAGC_WRITE								3	
#define OEDT_DIAGC_WAIT_TIME							10000
#define OEDT_DIAGC_BUFF_SIZE							255							
/*****************************************************************
| function prototype (scope: module-global)
|----------------------------------------------------------------*/


/*Test cases*/
tU32 u32DiagCustomOpenClose( tVoid );					//TU_OEDT_DiagCustom_000
tU32 u32DiagCustomOpenInval( tVoid );					//TU_OEDT_DiagCustom_001
tU32 u32DiagCustomOpenWithOutClose( tVoid );			//TU_OEDT_DiagCustom_002
tU32 u32DiagCustomDevCloseAlreadyClosed( tVoid );		//TU_OEDT_DiagCustom_003
tU32 u32DiagCustomWrite( tVoid );						//TU_OEDT_DiagCustom_004
tU32 u32DiagCustomWriteExcBuffLen( tVoid );				//TU_OEDT_DiagCustom_005
tU32 u32DiagCustomWriteBuffNULL( tVoid );				//TU_OEDT_DiagCustom_006
tU32 u32DiagCustomCSMErrReg(tVoid);						//TU_OEDT_DiagCustom_007


#endif
/************************************************************************
|end of file oedt_DiagCustom_TestFuncs.h
|-----------------------------------------------------------------------*/


#if 0
/**********************************************************FileHeaderBegin******
 *
 * FILE:        odt_DiagCustom_TestFuncs.h
 *
 * CREATED:     2006-01-25
 *
 * AUTHOR:
 *
 * DESCRIPTION: -
 *
 * NOTES: -
 *
 * COPYRIGHT:  (c) 2005 Technik & Marketing Service GmbH
 *
 **********************************************************FileHeaderEnd*******/

/*****************************************************************************
 *
 * search for this KEYWORDS to jump over long histories:
 *
 * CONSTANTS - TYPES - MACROS - VARIABLES - FUNCTIONS - PUBLIC - PRIVATE
 *
 ******************************************************************************/

/******************************************************************************
 * LOG:
 *
 * $Log: $
 ******************************************************************************/


#ifndef __ODT_DIAGCUSTOM_TESTFUNCS_H__
#define __ODT_DIAGCUSTOM_TESTFUNCS_H__


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/



/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/



/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/



/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/



/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/
tU32 u32DiagCustomTest(tVoid);



#endif  /* #ifndef __ODT_DIAGCUSTOM_TESTFUNCS_H__                          */

/* End of File odt_DiagCustom_TestFuncs.h                                  */

#endif
