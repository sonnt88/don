/******************************************************************************
 *FILE         : oedt_CARD_CommonFS_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function declarations for the CARD device.
 *            
  AUTHOR(s)    : Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY     :
 *-----------------------------------------------------------------------------
 * Date           | 		     		| Author & comments
 * --.--.--       | Initial revision    | ------------
 * 2 Dec, 2008    | version 1.0         | Shilpa Bhat(RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 13 Jan, 2009   | version 1.1     	| Shilpa Bhat(RBEI/ECM1)
   25 mar, 2009   | version 1.2     	| Rav8kor - removal of lint warnings
 *-----------------------------------------------------------------------------
 * 10 Sep, 2009   | version 1.3	    	|  Commenting the Test cases which uses 
 *				  |						|  OSAL_C_S32_IOCTRL_FIOOPENDIR
 *				  |						|  Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Feb, 2010   | version 1.4	    	|  Updated the Device name 
 *				  |						|  Anoop Chandran (RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 28 May, 2010   | version 1.5	    	|  Added new OEDT for CARD STATE
 *                |                     |u32CARD_CommonF_CardState()
 *                |                     |  anc2hi
 * 28 May, 2010   | version 1.5	    	| Removed u32CARD_CommonFSPrepareEject()
 *                |                     | Under unused Iocontrol removal 
 *                |                     |  By SWM2KOR
 *****************************************************************************/
#ifndef OEDT_CARD_CommonFST_TESTFUNCS_HEADER
#define OEDT_CARD_CommonFS_TESTFUNCS_HEADER

#ifdef SWITCH_OEDT_CARD
#define OEDTTEST_C_STRING_DEVICE_CARD "/dev/nor0"
#else
#define OEDTTEST_C_STRING_DEVICE_CARD OSAL_C_STRING_DEVICE_CARD 
#endif

#define OEDT_C_STRING_CARD_DIR1 OEDTTEST_C_STRING_DEVICE_CARD"/NewDir"
#define OEDT_C_STRING_CARD_NONEXST OEDTTEST_C_STRING_DEVICE_CARD"/Invalid"
#define OEDT_C_STRING_CARD_FILE1 OEDTTEST_C_STRING_DEVICE_CARD"/FILEFIRST.txt"
#define SUBDIR_PATH_CARD	OEDTTEST_C_STRING_DEVICE_CARD"/NewDir"
#define SUBDIR_PATH2_CARD	OEDTTEST_C_STRING_DEVICE_CARD"/NewDir/NewDir2"
#define OEDT_C_STRING_CARD_DIR_INV_NAME OEDTTEST_C_STRING_DEVICE_CARD"/*@#**"
#define OEDT_C_STRING_CARD_DIR_INV_PATH OEDTTEST_C_STRING_DEVICE_CARD"/MYFOLD1/MYFOLD2/MYFOLD3"
#define	CREAT_FILE_PATH_CARD OEDTTEST_C_STRING_DEVICE_CARD"/Testf1.txt"  
#define OSAL_TEXT_FILE_FIRST_CARD OEDTTEST_C_STRING_DEVICE_CARD"/file1.txt"

#define OEDT_C_STRING_FILE_INVPATH_CARD OEDTTEST_C_STRING_DEVICE_CARD"/Dummydir/Dummy.txt"

#define OEDT_CARD_COMNFILE OEDTTEST_C_STRING_DEVICE_CARD"/common.txt"

#define OEDT_CARD_WRITEFILE OEDTTEST_C_STRING_DEVICE_CARD"/WriteFile.txt"

#define FILE12_RECR2_CARD OEDTTEST_C_STRING_DEVICE_CARD"/File_Dir/File_Dir2/File12_test.txt"
#define FILE11_CARD OEDTTEST_C_STRING_DEVICE_CARD"/File_Dir/File11.txt"
#define FILE12_CARD OEDTTEST_C_STRING_DEVICE_CARD"/File_Dir/File12.txt"

#define OEDT_C_STRING_DEVICE_CARD_ROOT_CFS OEDTTEST_C_STRING_DEVICE_CARD"/"

#define FILE13_CARD     OEDTTEST_C_STRING_DEVICE_CARD"/File_Source/File13.txt"
#define FILE14_CARD     OEDTTEST_C_STRING_DEVICE_CARD"/File_Source/File14.txt"

tU32 u32CARD_CommonFSOpenClosedevice(tVoid );
tU32 u32CARD_CommonFSOpendevInvalParm(tVoid );
tU32 u32CARD_CommonFSReOpendev(tVoid );
tU32 u32CARD_CommonFSOpendevDiffModes(tVoid );
tU32 u32CARD_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32CARD_CommonFSOpenClosedir(tVoid );
tU32 u32CARD_CommonFSOpendirInvalid(tVoid );
tU32 u32CARD_CommonFSCreateDelDir(tVoid );
tU32 u32CARD_CommonFSCreateDelSubDir(tVoid );
tU32 u32CARD_CommonFSCreateDirInvalName(tVoid );
tU32 u32CARD_CommonFSRmNonExstngDir(tVoid );
tU32 u32CARD_CommonFSRmDirUsingIOCTRL(tVoid );
tU32 u32CARD_CommonFSGetDirInfo(tVoid );
tU32 u32CARD_CommonFSOpenDirDiffModes(tVoid );
tU32 u32CARD_CommonFSReOpenDir(tVoid );
tU32 u32CARD_CommonFSDirParallelAccess(tVoid);
tU32 u32CARD_CommonFSCreateDirMultiTimes(tVoid ); 
tU32 u32CARD_CommonFSCreateRemDirInvalPath(tVoid );
tU32 u32CARD_CommonFSCreateRmNonEmptyDir(tVoid );
tU32 u32CARD_CommonFSCopyDir(tVoid );
tU32 u32CARD_CommonFSMultiCreateDir(tVoid );
tU32 u32CARD_CommonFSCreateSubDir(tVoid);
tU32 u32CARD_CommonFSDelInvDir(tVoid);
tU32 u32CARD_CommonFSCopyDirRec(tVoid );
tU32 u32CARD_CommonFSRemoveDir(tVoid);
tU32 u32CARD_CommonFSFileCreateDel(tVoid );
tU32 u32CARD_CommonFSFileOpenClose(tVoid );
tU32 u32CARD_CommonFSFileOpenInvalPath(tVoid );
tU32 u32CARD_CommonFSFileOpenInvalParam(tVoid );
tU32 u32CARD_CommonFSFileReOpen(tVoid );
tU32 u32CARD_CommonFSFileRead(tVoid );
tU32 u32CARD_CommonFSFileWrite(tVoid );
tU32 u32CARD_CommonFSGetPosFrmBOF(tVoid );
tU32 u32CARD_CommonFSGetPosFrmEOF(tVoid );
tU32 u32CARD_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32CARD_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32CARD_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32CARD_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32CARD_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32CARD_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32CARD_CommonFSGetFileCRC(tVoid );
tU32 u32CARD_CommonFSReadAsync(tVoid);						//TU_OEDT_CARD_CFS_042
tU32 u32CARD_CommonFSLargeReadAsync(tVoid);					//TU_OEDT_CARD_CFS_043
tU32 u32CARD_CommonFSWriteAsync(tVoid);						//TU_OEDT_CARD_CFS_044
tU32 u32CARD_CommonFSLargeWriteAsync(tVoid);					//TU_OEDT_CARD_CFS_045
tU32 u32CARD_CommonFSWriteOddBuffer(tVoid);					//TU_OEDT_CARD_CFS_046
tU32 u32CARD_CommonFSWriteFileWithInvalidSize(tVoid);		//TU_OEDT_CARD_CFS_048
tU32 u32CARD_CommonFSWriteFileInvalidBuffer(tVoid);			//TU_OEDT_CARD_CFS_049
tU32 u32CARD_CommonFSWriteFileStepByStep(tVoid);				//TU_OEDT_CARD_CFS_050
tU32 u32CARD_CommonFSGetFreeSpace(tVoid);					//TU_OEDT_CARD_CFS_051
tU32 u32CARD_CommonFSGetTotalSpace(tVoid);					//TU_OEDT_CARD_CFS_052
tU32 u32CARD_CommonFSFileOpenCloseNonExstng(tVoid);			//TU_OEDT_CARD_CFS_053
tU32 u32CARD_CommonFSFileDelWithoutClose(tVoid);					//TU_OEDT_CARD_CFS_054
tU32 u32CARD_CommonFSSetFilePosDiffOff(tVoid);				//TU_OEDT_CARD_CFS_055
tU32 u32CARD_CommonFSCreateFileMultiTimes(tVoid);			//TU_OEDT_CARD_CFS_056
tU32 u32CARD_CommonFSFileCreateUnicodeName(tVoid);			//TU_OEDT_CARD_CFS_057
tU32 u32CARD_CommonFSFileCreateInvalName(tVoid);					//TU_OEDT_CARD_CFS_058
tU32 u32CARD_CommonFSFileCreateLongName(tVoid);				//TU_OEDT_CARD_CFS_059
tU32 u32CARD_CommonFSFileCreateDiffModes(tVoid);				//TU_OEDT_CARD_CFS_060
tU32 u32CARD_CommonFSFileCreateInvalPath(tVoid);				//TU_OEDT_CARD_CFS_061
tU32 u32CARD_CommonFSFileStringSearch(tVoid);				//TU_OEDT_CARD_CFS_062
tU32 u32CARD_CommonFSFileOpenDiffModes(tVoid);
tU32 u32CARD_CommonFSFileReadAccessCheck(tVoid);
tU32 u32CARD_CommonFSFileWriteAccessCheck(tVoid);
tU32 u32CARD_CommonFSFileReadSubDir(tVoid);
tU32 u32CARD_CommonFSFileWriteSubDir(tVoid);
tU32 u32CARD_CommonFSTimeMeasureof1KbFile(tVoid);
tU32 u32CARD_CommonFSTimeMeasureof10KbFile(tVoid);
tU32 u32CARD_CommonFSTimeMeasureof100KbFile(tVoid);
tU32 u32CARD_CommonFSTimeMeasureof1MBbFile(tVoid);
tU32 u32CARD_CommonFSTimeMeasureof1KbFilefor1000times(tVoid);
tU32 u32CARD_CommonFSTimeMeasureof10KbFilefor1000times(tVoid);
tU32 u32CARD_CommonFSTimeMeasureof100KbFilefor100times(tVoid);
tU32 u32CARD_CommonFSTimeMeasureof1MBFilefor16times(tVoid);
tU32 u32CARD_CommonFSRenameFile(tVoid);
tU32 u32CARD_CommonFSWriteFrmBOF(tVoid);
tU32 u32CARD_CommonFSWriteFrmEOF(tVoid);
tU32 u32CARD_CommonFSLargeFileRead(tVoid);
tU32 u32CARD_CommonFSLargeFileWrite(tVoid);
tU32 u32CARD_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32CARD_CommonFSWriteMultiThread(tVoid);
tU32 u32CARD_CommonFSWriteReadMultiThread(tVoid);
tU32 u32CARD_CommonFSReadMultiThread(tVoid);
tU32 u32CARD_CommonFSFormat(tVoid);
tU32 u32CARD_CommonFSCheckDisk(tVoid);
tU32 u32CARD_CommonFSPerformance_Diff_BlockSize(tVoid);
tU32 u32CARD_CommonFSFileGetExt(tVoid);
tU32 u32CARD_CommonFSFileGetExt2(tVoid);

tU32 u32CARD_CommonFS_CardState(tVoid);

#endif
