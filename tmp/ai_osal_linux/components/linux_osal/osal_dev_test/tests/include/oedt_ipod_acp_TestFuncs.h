/*******************************************************FileHeaderBegin*****
*
* FILE:
*     oedt_ipod_acp_TestFuncs.h
*
* REVISION:
*	   1.0
* AUTHOR:
*     (c) 2008, RBEI, ECM1 Hemantha Kumar A
*                                               
*
* CREATED:
*     06/05/2008 - Hemanth
*
* DESCRIPTION:
*      This file contains all the declaration for oedt ipod tests.
*
* NOTES:
*
* MODIFIED:
* 08-10-08 | hka2kor | removed few test cases and renamed function names.
* 22-04-09 | sak9kor | removed lint and compiler warnings
* 04.05.09 | jev1kor | changed return type for AutoTester support
*
* $Log$
*
*********************************************************FileHeaderEnd******/

#ifndef IPOD_ACP_OEDTFUNCTIONS_H
#define IPOD_ACP_OEDTFUNCTIONS_H

#ifndef TSIM_OSAL

/* Transmission Mode */
typedef enum
{
    DN_I2CSETUP    = -200,            /* setup the I2C port */
    DN_I2CTRANSFER = -201,            /* start a combined transaction */
    DN_I2CSNDTMO   = -202,            /* send time out */
    DN_I2CRCVTMO   = -203             /* receive time out */
} I2C_DataNo;

/* Slave Address */
typedef UW I2C_slave_addr;
/* if <  2^8, then  7bit address and the address is (value)         */
/* if >= 2^8, then 10bit address and the address is (value >> 8)    */

/* Transmission Mode */
typedef enum
{
    I2C_SLAVE   = 0,            /* slave mode   */
    I2C_MASTER  = 1             /* master mode  */
} I2C_transmission_mode;

/* Clock Settings */
typedef enum
{
    I2C_CLOCK_STANDARD  = 0,    /* standard mode    */
    I2C_CLOCK_FAST      = 1     /* fast mode        */
} I2C_clock;

/* I2C setup infomation struct */
typedef struct _I2C_info
{
    I2C_slave_addr          slave_addr;
    I2C_transmission_mode   transmission_mode;  /*  Transmission Mode   */
    I2C_clock               clock;              /*  Clock Seting        */
} I2C_info;

#define IPOD_POS0   0
#define IPOD_POS1   1
#define IPOD_POS2   2
#define IPOD_POS3   3
#define IPOD_POS4   4
#define IPOD_POS5   5
#define IPOD_POS6   6
#define IPOD_POS7   7
#define IPOD_POS8   8
#define IPOD_POS9   9
#define IPOD_POS10 10
#define IPOD_POS11 11
#define IPOD_POS12 12
#define IPOD_POS13 13
#define IPOD_POS14 14
#define IPOD_POS15 15
#define IPOD_POS16 16
#define IPOD_POS17 17
#define IPOD_POS18 18
#define IPOD_POS19 19
#define IPOD_POS20 20

#define IPOD_SHIFT_2			 2
#define IPOD_SHIFT_3			 3
#define IPOD_SHIFT_4			 4
#define IPOD_SHIFT_8			 8
#define IPOD_SHIFT_16			 16
#define IPOD_SHIFT_24		 	 24
#define IPOD_SHIFT_32			 32

#define IPOD_U32_LOW_MASK               0x000000FF
#define IPOD_U32_LOW_0_MASK             0xFFFFFF00

#define IPOD_MAX_TRY_COUNT              10
#define IPOD_USB_MAX_TRY_COUNT          10
#define IPOD_CP_READY_MAX_TEST_COUNT    20
#define IPOD_TIME_OUT                   1000

#define IPOD_DEVCONF_MAX_LEN            20
#define IICDEVICE                       "IICDEVICE"

#define IPOD_WAIT_FOR_CP_TIME           10
#define IPOD_DELAY_10MS                 10
#define IPOD_DELAY_20MS                 20
#define IPOD_DELAY_30MS                 30
#define IPOD_DELAY_50MS                 50
#define IPOD_DELAY_100MS                100
#define IPOD_DELAY_500MS                500
#define IPOD_DELAY_1000MS               1000

#define IPOD_AUTH_CP_WRITE_ADDR         0x20
#define IPOD_AUTH_CP_READ_ADDR          0x21
#define IPOD_AUTH_CP_SELFTEST_ADDR      0x40

#define IPOD_AUTH_CP_SELFTEST_SIZE      2 /* includes write address byte */
#define IPOD_AUTH_CP_DEVICE_ID_LEN      4
#define IPOD_AUTH_CP_VERSION_LEN        1
#define IPOD_AUTH_CP_CERTIFICATE_DATA_REGISTER_LEN    2
#define IPOD_AUTH_CP_CERTIFICATE_LEN    128

#define IPOD_AUTH_CP_FW_MAJ_VER_ADR     0x00
#define IPOD_AUTH_CP_FW_MIN_VER_ADR     0x01
#define IPOD_AUTH_CP_PROT_MAJ_VER_ADR   0x02
#define IPOD_AUTH_CP_PROT_MIN_VER_ADR   0x03
#define IPOD_AUTH_CP_DEVICE_ID_ADR      0x04
//#define IPOD_AUTH_CP_CHALLENGE_ADR      0x20
//#define IPOD_AUTH_CP_CHALLENGE_DATA_REGISTER_ADR      0x21
#define IPOD_AUTH_CP_CERTIFICATE_ADR    0x30

#define IPOD_OK             0
#define IPOD_ERROR          1

//#define OEDT_IPOD_ACP_CERT_MAX_LEN	 945
//#define OEDT_IPOD_ACP_NACK_MAX_RETRY_CNT 20

/* Test functions */
tU32 u32IPODACPAccessWhileSignatureGeneration(tVoid);
tU32 u32IPODACPGetCertificate(tVoid);
tU32 u32IPODACPGetDeviceID(tVoid);
tU32 u32IPODACPGetFirmwareVersion(tVoid);
tU32 u32IPODACPGetProtocolVersion(tVoid);
tU32 u32IPODACPGetSignature(tVoid);
tU32 u32IPODACPResetWhileSignatureGenerationn(tVoid);
tU32 u32IPODACPSelftest(tVoid);

/* Control functions */
tS32     iPodACPSelftest (void);
tS32     iPodInitACP(void);
UW      iPodIsACPReady(void);
#endif /*#ifndef TSIM_OSAL*/

#endif  /* #ifndef IPOD_ACP_OEDTFUNCTIONS_H */
