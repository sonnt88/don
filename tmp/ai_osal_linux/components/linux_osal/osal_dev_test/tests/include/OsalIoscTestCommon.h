#ifndef OEDT_IOSC_TEST_COMMON_H
#define OEDT_IOSC_TEST_COMMON_H
#include <oedt_testing_macros.h>

#define IOSC_TEST_MQ_DATA_CHANNEL "IOSC_TEST_MQ_DATA"
#define IOSC_TEST_MQ_ACK_CHANNEL "IOSC_TEST_MQ_ACK"
#define IOSC_TEST_MQ_MAX_MSGNUM 20
#define IOSC_TEST_MQ_MAX_MSGLEN 20
#define IOSC_TEST_MQ_PRIO 2

#define IOSC_TEST_S_ACK "ACK"
#define IOSC_TEST_S_OP_LEAVE "leave"
#define IOSC_TEST_S_OP_CREATE "create"
#define IOSC_TEST_S_OP_OPEN "open"
#define IOSC_TEST_S_OP_READ "read"
#define IOSC_TEST_S_OP_WRITE "write"
#define IOSC_TEST_S_OP_CLOSE "close"
#define IOSC_TEST_S_OP_DELETE "delete"
#define IOSC_TEST_S_SET_HALF_SIZE "half"
#define IOSC_TEST_S_SET_DOUBLE_SIZE "double"

typedef enum IOSC_TEST_OPERATION {
  IOSC_TEST_ACK,
  IOSC_TEST_OP_LEAVE,
  IOSC_TEST_OP_CREATE,
  IOSC_TEST_OP_OPEN,
  IOSC_TEST_OP_READ,
  IOSC_TEST_OP_WRITE,
  IOSC_TEST_OP_CLOSE,
  IOSC_TEST_OP_DELETE,
  IOSC_TEST_SET_HALF_SIZE,
  IOSC_TEST_SET_DOUBLE_SIZE,
} IOSC_TEST_OPERATION;


OSAL_tMQueueHandle CreateMessageQueue( tCString szMQName, OSAL_tenAccess mode, OEDT_TESTSTATE* state );
OSAL_tMQueueHandle OpenMessageQueue( tCString szMQName, OSAL_tenAccess mode, OEDT_TESTSTATE* state);
tVoid ReceiveMessage(OSAL_tMQueueHandle hMessageQueue, tU8* pu8Buffer, OEDT_TESTSTATE* state);
tS32 TranslateMessage(tCString szMessage, OEDT_TESTSTATE* state);
tBool DispatchMessage(tS32 s32Message, OEDT_TESTSTATE* state);
tBool PostMessage(OSAL_tMQueueHandle hMessageQueue, tU8* pu8Message, OEDT_TESTSTATE* state);
tVoid CloseMessageQueue(OSAL_tMQueueHandle hMessageQueue, OEDT_TESTSTATE* state);
tVoid DeleteMessageQueue( tCString szMQName, OEDT_TESTSTATE* state);
#endif //OEDT_IOSC_TEST_COMMON_H
