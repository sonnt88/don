/******************************************************************************
 *FILE         : oedt_USB_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes that 
                 will be used in the file oedt_Chenc_TestFuncs.c, 
                 for the GM-GE hardware.
 *
 *AUTHOR       : Anoop Chandran (RBEI/ECM1)) 
 *
 *HISTORY      : 30/06/2008 ver 0.1 Anoop Chandran (RBEI/ECM1)
 *--------------------------------------------------------------------------------
                 19/07/2013 ver 1.0 Ramachandra Madhu Kiran (CM-AI/PJ-CF35)
                                    Modified test cases for GEN3 China encoder.
 *--------------------------------------------------------------------------------
*******************************************************************************/

#ifndef OEDT_CHENC_TESTFUNCS_HEADER
#define OEDT_CHENC_TESTFUNCS_HEADER

/* Function prototypes of functions used in file oedt_Chenc_TestFuns.c */
tU32 u32ChencOpenCloseDevice(tVoid);
tU32 u32ChencOpenDeviceInvalParam(tVoid);
tU32 u32ChencMultOpenDevice(tVoid);
tU32 u32ChencOpenDeviceDiffModes(tVoid);
tU32 u32ChenDevCloseAlreadyClosed(tVoid);
tU32 u32ChenDevEncodePosInfo(tVoid);
tU32 u32ChenDevEncodeChkDisFeature(tVoid);
#endif 

/******************* End of File *********************/

