																				/******************************************************************************
 *FILE         : oedt_Odometer_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file has the function declaration for Odo test cases. 
 *                              
 *AUTHOR       : Ravindran P(CM-DI/EAP)
 *
 *COPYRIGHT    : (c) 2006 Blaupunkt Werke GmbH
 *
 *HISTORY      : 26.06.06 .Initial version
 *               21.03.2013. Added new test cases
 *
 *****************************************************************************/


#ifndef OEDT_ODO_TESTFUNCS_HEADER
#define OEDT_ODO_TESTFUNCS_HEADER

/*Test case function prototypes*/
tU32 u32OdoOpenDev(void);
tU32 tu32OdoBasicReadSeq(void);
tU32 tu32OdoReadAndFlushSeq(void);
tU32 tu32OdoInterfaceCheckSeq(void);
tU32 tu32OdoCheckReadLargeNoOfRecords(void);
tU32 tu32OdoCheckReadMoreThanBufferLength(void);
/*tU32 tu32OdoSetIllegalTimeOutCheckContentSeq(void); */	 /*Not needed as time out taken care by event handler*/
/*tU32 tu32OdoCheckTimeOut(void);*/	 /*Not needed as time out taken care by event handler*/

tU32 tu32OdoBasicRead(void);
tU32 tu32OdoReOpen(void);
tU32 tu32OdoCloseAlreadyClosed(void);
tU32 tu32OdoReadpassingnullbuffer(void);
tU32 tu32OdoTimeoutRead(void);
tU32 u32OdoGetCycleTime(tVoid);


/* OEDTs for Stub tests */
tU32 tu32OdoStubTestReadDirection(void);



#endif
