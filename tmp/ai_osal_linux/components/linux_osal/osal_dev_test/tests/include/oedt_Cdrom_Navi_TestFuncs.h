/******************************************************************************
 *FILE         : oedt_Cdrom_Navi_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for the Cdrom device.
 *
 *AUTHOR       : Venkateswara.N
 *
 *COPYRIGHT    : (c) 2003 Blaupunkt Werke GmbH
 *
 *HISTORY      : DVD used for testing - dvd0-EUR07.3NT_FM7.2S_N1210M8T8_07 [v2_oS]
 *             
 *               15.05.06 ver 1.0  RBIN/ECM2- Venkateswara.N
 *               Initial Revision.
 *            	 Created a seperate file for OEDT-CD-Ctrl Test functions
 *				 Prototypes
 *				 ver 1.1 
 *				 Updated by Shilpa Bhat(RBIN/EDI3) 11 July, 2007
 *				 ver 1.2
 *				 Updated by Tinoy Mathews(RBIN/EDI3) Sept 27, 2007
 *               06.03.08 ver 1.3
 *               renamed to oedt_Cdrom_Navi_TestFuncs.h
 *				 ver 1.4
 *				 Updated by Anoop Chandran(RBIN/EDI3) Nov 19, 2007
 *               Removed 8 test cases which using invalid handle
 *				 from previous version ver 1.2
 *               ver 1.5
 *               Removed case u32CDROMFileGetInfo() and 
 *               u32CDROMFileGetInfoInvalParm()                
 *               ver 1.6
 *          	 Updated by Shilpa Bhat (RBIN/ECM1) on 30/07/2008
 *		    	 Removed cases u32CDROMNaviOpenDirNonExist, u32CDROMNaviFileOpenNonExist,
 *          	 u32CDROMNaviCloseDevInvalParm, u32CDROMNaviOpendevNullParam,
 *          	 u32CDROMNaviCloseDirInvalParm, u32CDROMNaviGetInfoInvalParm,
 *          	 u32CDROMNaviFileCloseInvParm, u32CDROMNaviFileGetPosInvalParm,
 *          	 u32CDROMNaviFileGetPosFrmEOFInvalParm, u32CDROMNaviFileSetPosInvalParm,
 *          	 u32CDROMNaviFileReadWithInvalParm, u32CDROMNaviGetVersionNumberInvalParm
 *          	 u32CDROMNaviGetVolumeLabelInvalParm, u32CDROMNaviOpenDirWithNULL, 
 *          	 u32CDROMNaviFileOpenWithNULLParam, u32CDROMNaviDirReClose
 *               u32CDROMNaviLowVoltTest, u32CDROMNaviMaxFileOpen, u32CDROMNaviFileOpenGrtrMax
 *          	 Added new cases u32CDROMReadAsync, u32CDROMFileReClose
 *        
 *               ver 1.7 
 *               Updated by Shilpa Bhat (RBEI/ECF1) on Mar 24, 2009
 *               Lint Removal
 *							 
*****************************************************************************/
#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	
#include <basic.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif 
#include <string.h>

#ifndef OEDT_CDROM_NAVI_TESTFUNCS_HEADER
#define OEDT_CDROM_NAVI_TESTFUNCS_HEADER

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

tU32 u32CDROMNaviOpenDevice(void);
tU32 u32CDROMNaviOpenDevInvalParm(void);
tU32 u32CDROMNaviReOpenDev(void);
tU32 u32CDROMNaviOpenDevWriteOnly(void);
tU32 u32CDROMNaviCloseDevice(void);
tU32 u32CDROMNaviOpenDir(void);
tU32 u32CDROMNaviOpenDirWithFile(void) ;
tU32 u32CDROMNaviReOpenDir(void);
tU32 u32CDROMNaviOpenDirWriteAccess( void);
tU32 u32CDROMNaviCloseDir(void);
tU32 u32CDROMNaviGetDirInfo(void);
tU32 u32CDROMNaviFileOpen(void);
tU32 u32CDROMNaviFileOpenInvalPath(void);
tU32 u32CDROMNaviFileOpenDiffModes(void);
tU32 u32CDROMNaviFileOpenDiffExtns(void);
tU32 u32CDROMNaviFileOpenWithDirName(void);
tU32 u32CDROMNaviFileReOpen(void);
tU32 u32CDROMNaviFileClose(void);
tU32 u32CDROMNaviFileCloseDiffExtns(void);
tU32 u32CDROMNaviFileCloseWithDirName(void);
tU32 u32CDROMNaviFileGetPosFrmBOF(void)	;
tU32 u32CDROMNaviFileGetPosFrmEOF(void);
tU32 u32CDROMNaviFileSetPos(void);
tU32 u32CDROMNaviFileSetPosBeyondEOF(void);
tU32 u32CDROMNaviFileRead(void);
tU32 u32CDROMNaviFileReadSubDir(void);
tU32 u32CDROMNaviFileReadLargeData(void);
tU32 u32CDROMNaviFileReadOffset(void);
tU32 u32CDROMNaviFileReadOffsetFrmEOF(void);
tU32 u32CDROMNaviFileReadOffsetBeyondEOF(void);
tU32 u32CDROMNaviFileReadEveryNthByteFrmBOF(void);
tU32 u32CDROMNaviFileReadEvreyNthByteFrmEOF(void);
tU32 u32CDROMNaviGetVersionNumber(void);
tU32 u32CDROMNaviGetVolumeLabel(void);
tU32 u32CDROMNaviGetMediaInfo(void);
tU32 u32CDROMNaviInvalFuncParm2IOCTL(void);
tU32 u32CDROMNaviInvalidMedia(void);
tU32 u32CDROMNaviWithReverseCD();
tU32 u32CDROMNaviCreateDir(void);
tU32 u32CDROMNaviDelDir(void);
tU32 u32CDROMNaviOpenMaxPathLength(void);
tU32 u32CDROMNaviFileCreate(void);
tU32 u32CDROMNaviFileRemove(void);
tU32 u32CDROMNaviFileSetPosFrmBOF(void);
tU32 u32CDROMNaviFirstFileRead(void);
tU32 u32CDROMNaviLastFileRead(void);
tU32 u32CDROMNaviFileReadInJumps(void);
tU32 u32CDROMNaviFileReadNegOffsetFrmBOF(void);
tU32 u32CDROMNaviFileThroughPutFirstFile(void);
tU32 u32CDROMNaviFileThroughPutLastFile(void);
tU32 u32CDROMNaviFileReadInvaildCount(void);
tU32 u32CDROMNaviFileReadToInvaildBuffer(void);
tU32 u32CDROMNaviGetMediaInfoInvalParam(void);
tU32 u32CDROMNaviReadAsync(tVoid);
tU32 u32CDROMNaviFileReClose(tVoid);
#endif
