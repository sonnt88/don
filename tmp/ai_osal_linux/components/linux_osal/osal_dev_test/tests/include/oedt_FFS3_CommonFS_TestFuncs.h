/******************************************************************************
 * FILE				  : oedt_FFS3_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT	: OEDT_FrmWrk 
 *
 * DESCRIPTION	: This file defines the macros and prototypes for the 
 *					      filesystem test cases for the FFS3
 *                          
 * AUTHOR(s)		:  Sriranjan U (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * 	Date			  |					        |	Author & comments
 * --.--.--			|	Initial revision|	------------
 * 17 Jan, 2012	|	version 1.0			|	Martin Langer CM-AI/PJ-CF33
 * 16 Mar, 2015   |  version 1.1       |  Deepak Kumar (RBEI/ECF5)
 * 05 Feb,2016    | version 1.2        |  Chakitha Saraswathi (RBEI/ECF5)
 *-----------------------------------------------------------------------------
 */


#ifndef OEDT_FFS3_COMMON_FS_TESTFUNCS_HEADER
#define OEDT_FFS3_COMMON_FS_TESTFUNCS_HEADER

//#undef OEDTTEST_C_STRING_DEVICE_FFS3
//#ifdef SWITCH_OEDT_FFS3
//#define OEDTTEST_C_STRING_DEVICE_FFS3 "/dev/nand0"
//#else
#define OEDTTEST_C_STRING_DEVICE_FFS3 OSAL_C_STRING_DEVICE_FFS3
#define OEDTTEST_C_STRING_DEVICE_DYNAMIC "/dev/ffs2/dynamic"
// #endif

#undef OEDT_C_STRING_FFS3_DIR1 
#undef OEDT_C_STRING_FFS3_NONEXST 
#undef OEDT_C_STRING_FFS3_FILE1
#undef SUBDIR_PATH_FFS3	
#undef SUBDIR_PATH2_FFS3   
#undef OEDT_C_STRING_FFS3_DIR_INV_NAME 
#undef OEDT_C_STRING_FFS3_DIR_INV_PATH 
#undef	CREAT_FILE_PATH_FFS3 
#undef OSAL_TEXT_FILE_FIRST_FFS3 
#undef OEDT_C_STRING_FILE_INVPATH_FFS3 
#undef OEDT_FFS3_COMNFILE 
#undef OEDT_FFS3_WRITEFILE 
#undef FILE12_RECR2_FFS3 
#undef FILE11_FFS3 
#undef FILE12_FFS3 

#undef OEDT_C_STRING_DEVICE_FFS3_ROOT 

#undef FILE13_FFS3    
#undef FILE14_FFS3    

#undef OEDT_FFS3_CFS_DUMMYFILE			   
#undef OEDT_FFS3_CFS_TESTFILE			   
#undef OEDT_FFS3_CFS_INVALIDFILE		   
#undef OEDT_FFS3_CFS_UNICODEFILE		   
#undef OEDT_FFS3_CFS_INVALCHAR_FILE		
#undef OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_SYNC  
#undef OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_ASYNC 

#define OEDT_C_STRING_FFS3_DIR1 OEDTTEST_C_STRING_DEVICE_FFS3"/NewDir"
#define OEDT_C_STRING_FFS3_NONEXST OEDTTEST_C_STRING_DEVICE_FFS3"/Invalid"
#define OEDT_C_STRING_FFS3_FILE1 OEDTTEST_C_STRING_DEVICE_FFS3"/FILEFIRST.txt"
#define SUBDIR_PATH_FFS3	OEDTTEST_C_STRING_DEVICE_FFS3"/NewDir"
#define SUBDIR_PATH2_FFS3	OEDTTEST_C_STRING_DEVICE_FFS3"/NewDir/NewDir2"
#define OEDT_C_STRING_FFS3_DIR_INV_NAME OEDTTEST_C_STRING_DEVICE_FFS3"/*@#**"
#define OEDT_C_STRING_FFS3_DIR_INV_PATH OEDTTEST_C_STRING_DEVICE_FFS3"/MYFOLD1/MYFOLD2/MYFOLD3"
#define	CREAT_FILE_PATH_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/Testf1.txt"  
#define OSAL_TEXT_FILE_FIRST_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/file1.txt"

#define OEDT_C_STRING_FILE_INVPATH_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/Dummydir/Dummy.txt"

#define OEDT_FFS3_COMNFILE OEDTTEST_C_STRING_DEVICE_FFS3"/common.txt"

#define OEDT_FFS3_WRITEFILE OEDTTEST_C_STRING_DEVICE_FFS3"/WriteFile.txt"

#define FILE12_RECR2_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/File_Dir/File_Dir2/File12_test.txt"
#define FILE11_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/File_Dir/File11.txt"
#define FILE12_FFS3 OEDTTEST_C_STRING_DEVICE_FFS3"/File_Dir/File12.txt"

#define OEDT_C_STRING_DEVICE_FFS3_ROOT OEDTTEST_C_STRING_DEVICE_FFS3"/"

#define FILE13_FFS3     OEDTTEST_C_STRING_DEVICE_FFS3"/File_Source/File13.txt"
#define FILE14_FFS3     OEDTTEST_C_STRING_DEVICE_FFS3"/File_Source/File14.txt"

#define OEDT_FFS3_CFS_DUMMYFILE				OEDTTEST_C_STRING_DEVICE_FFS3"/Dummy.txt"
#define OEDT_FFS3_CFS_TESTFILE				OEDTTEST_C_STRING_DEVICE_FFS3"/Test.txt"
#define OEDT_FFS3_CFS_INVALIDFILE			OEDTTEST_C_STRING_DEVICE_FFS3"/DIR/Test.txt"
#define OEDT_FFS3_CFS_UNICODEFILE		  	OEDTTEST_C_STRING_DEVICE_FFS3"/¶«æ±ª¶.txt"
#define OEDT_FFS3_CFS_INVALCHAR_FILE		OEDTTEST_C_STRING_DEVICE_FFS3"//*/</>>.txt"
#define OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_SYNC	\
        OEDTTEST_C_STRING_DEVICE_FFS3"/ioctrl_save_now_FFS3_sync.txt"
#define OEDT_FFS3_CFS_IOCTRL_SAVENOW_FILE_ASYNC	\
        OEDTTEST_C_STRING_DEVICE_FFS3"/ioctrl_save_now_FFS3_async.txt"
	 											  
/* Function Prototypes for the functions defined in Oedt_FileSystem_TestFuncs.c */
tU32 u32FFS3_CommonFSOpenClosedevice(tVoid );
tU32 u32FFS3_CommonFSOpendevInvalParm(tVoid );
tU32 u32FFS3_CommonFSReOpendev(tVoid );
tU32 u32FFS3_CommonFSOpendevDiffModes(tVoid );
tU32 u32FFS3_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32FFS3_CommonFSOpenClosedir(tVoid );
tU32 u32FFS3_CommonFSOpendirInvalid(tVoid );
tU32 u32FFS3_CommonFSCreateDelDir(tVoid );
tU32 u32FFS3_CommonFSCreateDelSubDir(tVoid );
tU32 u32FFS3_CommonFSCreateDirInvalName(tVoid );
tU32 u32FFS3_CommonFSRmNonExstngDir(tVoid );
tU32 u32FFS3_CommonFSRmDirUsingIOCTRL(tVoid );
tU32 u32FFS3_CommonFSGetDirInfo(tVoid );
tU32 u32FFS3_CommonFSOpenDirDiffModes(tVoid );
tU32 u32FFS3_CommonFSReOpenDir(tVoid );
tU32 u32FFS3_CommonFSDirParallelAccess(tVoid);
tU32 u32FFS3_CommonFSCreateDirMultiTimes(tVoid ); 
tU32 u32FFS3_CommonFSCreateRemDirInvalPath(tVoid );
tU32 u32FFS3_CommonFSCreateRmNonEmptyDir(tVoid );
tU32 u32FFS3_CommonFSCopyDir(tVoid );
tU32 u32FFS3_CommonFSMultiCreateDir(tVoid );
tU32 u32FFS3_CommonFSCreateSubDir(tVoid);
tU32 u32FFS3_CommonFSDelInvDir(tVoid);
tU32 u32FFS3_CommonFSCopyDirRec(tVoid );
tU32 u32FFS3_CommonFSRemoveDir(tVoid);
tU32 u32FFS3_CommonFSFileCreateDel(tVoid );
tU32 u32FFS3_CommonFSFileOpenClose(tVoid );
tU32 u32FFS3_CommonFSFileOpenInvalPath(tVoid );
tU32 u32FFS3_CommonFSFileOpenInvalParam(tVoid );
tU32 u32FFS3_CommonFSFileReOpen(tVoid );
tU32 u32FFS3_CommonFSFileRead(tVoid );
tU32 u32FFS3_CommonFSFileWrite(tVoid );
tU32 u32FFS3_CommonFSGetPosFrmBOF(tVoid );
tU32 u32FFS3_CommonFSGetPosFrmEOF(tVoid );
tU32 u32FFS3_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32FFS3_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32FFS3_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32FFS3_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32FFS3_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32FFS3_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32FFS3_CommonFSGetFileCRC(tVoid );
tU32 u32FFS3_CommonFSReadAsync(tVoid);
tU32 u32FFS3_CommonFSLargeReadAsync(tVoid);
tU32 u32FFS3_CommonFSWriteAsync(tVoid);
tU32 u32FFS3_CommonFSLargeWriteAsync(tVoid);
tU32 u32FFS3_CommonFSWriteOddBuffer(tVoid);
tU32 u32FFS3_CommonFSFileMultipleHandles(tVoid); //new
tU32 u32FFS3_CommonFSWriteFileWithInvalidSize(tVoid);
tU32 u32FFS3_CommonFSWriteFileInvalidBuffer(tVoid);
tU32 u32FFS3_CommonFSWriteFileStepByStep(tVoid);
tU32 u32FFS3_CommonFSGetFreeSpace (tVoid);
tU32 u32FFS3_CommonFSGetTotalSpace (tVoid);
tU32 u32FFS3_CommonFSFileOpenCloseNonExstng(tVoid);
tU32 u32FFS3_CommonFSFileDelWithoutClose(tVoid);
tU32 u32FFS3_CommonFSSetFilePosDiffOff(tVoid);
tU32 u32FFS3_CommonFSCreateFileMultiTimes(tVoid);
tU32 u32FFS3_CommonFSFileCreateUnicodeName(tVoid);
tU32 u32FFS3_CommonFSFileCreateInvalName(tVoid);
tU32 u32FFS3_CommonFSFileCreateLongName(tVoid);
tU32 u32FFS3_CommonFSFileCreateDiffModes(tVoid);
tU32 u32FFS3_CommonFSFileCreateInvalPath(tVoid);
tU32 u32FFS3_CommonFSStringSearch(tVoid);
tU32 u32FFS3_CommonFSFileOpenDiffModes(tVoid);
tU32 u32FFS3_CommonFSFileReadAccessCheck(tVoid);
tU32 u32FFS3_CommonFSFileWriteAccessCheck(tVoid);
tU32 u32FFS3_CommonFSFileReadSubDir(tVoid);
tU32 u32FFS3_CommonFSFileWriteSubDir(tVoid);
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof1KbFile(tVoid);
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof10KbFile(tVoid);
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof100KbFile(tVoid);
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof1MBFile(tVoid);
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof1KbFilefor1000times(tVoid);
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof10KbFilefor1000times(tVoid);
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof100KbFilefor100times(tVoid);
tU32 u32FFS3_CommonFSReadWriteTimeMeasureof1MBFilefor16times(tVoid);
tU32 u32FFS3_CommonFSRenameFile(tVoid);
tU32 u32FFS3_CommonFSWriteFrmBOF(tVoid);
tU32 u32FFS3_CommonFSWriteFrmEOF(tVoid);
tU32 u32FFS3_CommonFSLargeFileRead(tVoid);
tU32 u32FFS3_CommonFSLargeFileWrite(tVoid);
tU32 u32FFS3_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32FFS3_CommonFSWriteMultiThread(tVoid);
tU32 u32FFS3_CommonFSWriteReadMultiThread(tVoid);
tU32 u32FFS3_CommonFSReadMultiThread(tVoid);
tU32 u32FFS3_CommonFSFormat(tVoid);
tU32 u32FFS3_CommonFSFileGetExt(tVoid);
tU32 u32FFS3_CommonFSFileGetExt2(tVoid);
tU32 u32FFS3_CommonFSSaveNowIOCTRL_SyncWrite(tVoid);
tU32 u32FFS3_CommonFSSaveNowIOCTRL_AsynWrite(tVoid);
tU32 u32FFS3_CommonFileOpenCloseMultipleTime( tVoid );
tU32 u32FFS3_CommonFileOpenCloseMultipleTimeRandom1( tVoid );
tU32 u32FFS3_CommonFileOpenCloseMultipleTimeRandom2( tVoid );
tU32 u32FFS3_CommonFileLongFileOpenCloseMultipleTime(tVoid);
tU32 u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom1(tVoid);
tU32 u32FFS3_CommonFileLongFileOpenCloseMultipleTimeRandom2(tVoid);
tU32 u32FFS3_CommonFileOpenCloseMultipleTimeMultDir(tVoid);
tU32 u32FFS3_CommonFSGetFileSize(tVoid);
tU32 u32FFS3_CommonFSReadDirValidate(tVoid);
tU32 u32FFS3_CommonFSRmRecursiveCancel(tVoid);
tU32 u32FFS3_CommonFSCreateManyThreadDeleteMain(tVoid);
tU32 u32FFS3_CommonFSCreateManyDeleteInThread(tVoid);
tU32 u32FFS3_CommonFSOpenManyCloseInThread(tVoid);
tU32 u32FFS3_CommonFSOpenInThreadCloseMain(tVoid);
tU32 u32FFS3_CommonFSWriteMainReadInThread(tVoid);
tU32 u32FFS3_CommonFSWriteThreadReadInMain(tVoid);
tU32 u32FFS3_CommonFSFileAccInDiffThread(tVoid);
tU32 u32FFS3_CommonFSReadDirExtPartByPart(tVoid);
tU32 u32FFS3_CommonFSReadDirExt2PartByPart(tVoid);
tU32 u32FFS3_CommonFSCopyDirTest(tVoid);
tU32 u32FFS3_CommonFSReadWriteHugeData(tVoid);
tU32 u32FFS3_CommonFSRW_Performance_Diff_BlockSize(tVoid);

tU32 FFS3_CommonFSStressTest(tVoid);

#endif //OEDT_FFS3_COMMON_FS_TESTFUNCS_HEADER

