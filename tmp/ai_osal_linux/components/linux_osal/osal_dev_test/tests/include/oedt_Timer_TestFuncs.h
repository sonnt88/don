/******************************************************************************
 *FILE         : oedt_Timer_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_Timer_TestFuncs.c for the  
 *               Timer device for the GM-GE hardware.
 *
 *AUTHOR       : Shilpa Bhat (RBIN/EDI3) 
 *
 *HISTORY      : 26 Sept, 2007 ver 0.1  RBIN/EDI3- Shilpa Bhat
				 27 Sept, 2007 ver 0.2  RBIN/EDI3 -Shilpa Bhat
				 Updated the names of functions as per fixed standard
				 8 April, 2008 ver 0.3 RBIN/ECM1 - Tinoy Mathews
				 Added prototypes for Timer Channels 0,1,2,3
              13-April-2009| lint warnings removed | Jeryn Mathew (RBEI/ECF1)
*******************************************************************************/

#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	
#include <basic.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>

#ifndef OEDT_TIMER_TESTFUNCS_HEADER
#define OEDT_TIMER_TESTFUNCS_HEADER

/* Macro definition used in the code */
#define PWM_DUTYCYCLE_MINVAL	0
#define PWM_DUTYCYCLE_MIDVAL	50
#define PWM_DUTYCYCLE_MAXVAL	100
#define PWM_DUTYCYCLE_INVAL		101

#ifndef OSAL_C_S32_IOCTRL_INVAL_FUNC
   #define OSAL_C_S32_IOCTRL_INVAL_FUNC 	   -1000
#endif
#ifndef OSAL_C_STRING_INVALID_ACCESS_MODE
   #define OSAL_C_STRING_INVALID_ACCESS_MODE   -1
#endif
#define OSAL_C_STRING_INVALID_DEVICE_TIMER	   -1
#define TIMER_INVAL_PARAM					   -1

#define TIMER_FAN_SUPPORT       0
#define NO_OF_TIMER_CHANNELS    4 /*4 Timer Channels*/  
#define PATH_LENGTH             15
#define SRAND_MAX               (tU32)65535

#define TIMER0					0
#define TIMER1					1
#define TIMER2					2
#define TIMER3					3

#define CPU_EXCEPTION           0

#if TIMER_FAN_SUPPORT
/* Function prototypes of functions used in file oedt_Timer_TestFuns.c */
tU32 u32TimerDevOpenClose(tVoid);
tU32 u32TimerDevOpenCloseInvalParam(tVoid);
tU32 u32TimerDevCloseAlreadyClosed(tVoid);
tU32 u32TimerDevOpenCloseDiffModes(tVoid);
tU32 u32TimerDevOpenCloseInvalAccessMode(tVoid);
tU32 u32TimerGetVersion(tVoid);
tU32 u32TimerGetVersionInvalParam(tVoid);
tU32 u32TimerSetPWMMinRange(tVoid);
tU32 u32TimerSetPWMMidRange(tVoid);
tU32 u32TimerSetPWMMaxRange(tVoid);
tU32 u32TimerSetPWMInval(tVoid);
tU32 u32TimerGetPWM(tVoid);
tU32 u32TimerGetPWMInvalParam(tVoid);
tU32 u32TimerSetGetPWM(tVoid);
tU32 u32TimerInvalFuncParamToIOCtrl(tVoid);

/* Function prototypes of existing functions of Timer Test Code in osal_dev_test */
tU32 u32TestTimer1(tVoid);
tU32 u32TestTimer2(tVoid);
tU32 u32TestTimer3(tVoid);
#endif

/*Cases on Timer channels 0,1,2,3*/
tS32 s32CalcDutyCycRand( tVoid );
tU32 u32GenericTimerOpenClose( tU8 u8Channel,OSAL_tenAccess enAccess );
tU32 u32GenericTimerReOpen( tU8 u8Channel,OSAL_tenAccess enAccess );
tU32 u32GenericTimerReClose( tU8 u8Channel,OSAL_tenAccess enAccess );
tU32 u32GenericTimerVersionQuery( tU8 u8Channel,OSAL_tenAccess enAccess,tS32* pVersion );
tU32 u32GenericTimerGetPWM( tU8 u8Channel,OSAL_tenAccess enAccess,tS32* pDutyCycle );
tU32 u32GenericTimerSetPWM( tU8 u8Channel,OSAL_tenAccess enAccess );

tU32 u32OpenCloseValidTimer0( tVoid );			/*1*/
tU32 u32OpenCloseValidTimer1( tVoid );			/*2*/
tU32 u32OpenCloseValidTimer2( tVoid );			/*3*/
tU32 u32OpenCloseValidTimer3( tVoid );			/*4*/
tU32 u32OpenCloseInvalAccessTimer0( tVoid );	/*5*/
tU32 u32OpenCloseInvalAccessTimer1( tVoid );	/*6*/
tU32 u32OpenCloseInvalAccessTimer2( tVoid );	/*7*/
tU32 u32OpenCloseInvalAccessTimer3( tVoid );	/*8*/
tU32 u32ReOpenTimer0( tVoid );					/*9*/
tU32 u32ReOpenTimer1( tVoid );					/*10*/
tU32 u32ReOpenTimer2( tVoid );					/*11*/
tU32 u32ReOpenTimer3( tVoid );					/*12*/
tU32 u32ReCloseTimer0( tVoid );					/*13*/
tU32 u32ReCloseTimer1( tVoid );					/*14*/
tU32 u32ReCloseTimer2( tVoid );					/*15*/
tU32 u32ReCloseTimer3( tVoid );					/*16*/
tU32 u32GetVersionTimer0( tVoid );				/*17*/
tU32 u32GetVersionTimer1( tVoid );				/*18*/
tU32 u32GetVersionTimer2( tVoid );				/*19*/
tU32 u32GetVersionTimer3( tVoid );				/*20*/
tU32 u32GetPWMTimer0( tVoid );					/*21*/
tU32 u32GetPWMTimer1( tVoid );					/*22*/
tU32 u32GetPWMTimer2( tVoid );					/*23*/
tU32 u32GetPWMTimer3( tVoid );					/*24*/
tU32 u32SetPWMTimer0( tVoid );					/*25*/
tU32 u32SetPWMTimer1( tVoid );					/*26*/
tU32 u32SetPWMTimer2( tVoid );					/*27*/
tU32 u32SetPWMTimer3( tVoid );					/*28*/

#endif

