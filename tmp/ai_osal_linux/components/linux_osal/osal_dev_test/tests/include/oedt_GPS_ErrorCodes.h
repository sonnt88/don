/******************************************************************************
 *FILE         : odt_GPS_ErrorCodes.h
 *
 *SW-COMPONENT : ODT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for the GPS device.
 *                also the array of function pointer for this device has been 
 *                initialised.
 *
 *AUTHOR       : Ravindran P (RBIN CM-DI/ESA)
 *
 *COPYRIGHT    : (c) 2005 Blaupunkt GmbH
 *
 *HISTORY      : 2005-05-13 - Initial version
 *
 *****************************************************************************/


#ifndef ODT_GPS_TESTFUNCS_HEADER
#define ODT_GPS_TESTFUNCS_HEADER



#define DEFAULT_CLK_DRIFT			(-210)

/*Specific error codes with values assigned corresponding to the array value in TRT file*/
#define		OSAL_C_S32_GPS_ERROR_UNKNOWN								36
#define		OSAL_DEVICE_OPEN_ERROR  								   		37	
#define		OSAL_GPS_START_ERROR                                                           		38
#define		OSAL_GPS_IOREAD_ERROR                                                           		39
#define		OSAL_GPS_IOCTRL_READ_RECORD_ERROR                                		40
#define		OSAL_C_S32_IOCTRL_VERSION_ERROR                                       		41
#define		OSAL_C_S32_IOCTL_GPS_SET_STARTUP_MODE_ERROR               		42        			
#define		OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_SIZE_ERROR    		43
#define		OSAL_C_S32_IOCTL_GPS_SET_INTERNAL_DATA_ERROR          		44
#define 		OSAL_C_S32_IOCTL_GPS_SET_DEFAULT_CLOCK_DRIFT_ERROR		45
#define		OSAL_C_S32_IOCTL_GPS_GET_CLOCK_DRIFT_ERROR                 		46
#define 		OSAL_C_S32_IOCTL_GPS_SET_CURRENT_TIME_ERROR               		47
#define		OSAL_C_S32_IOCTL_GPS_GET_ANTENNA_ERROR                        		48
#define	       OSAL_C_S32_IOCTL_GPS_SET_ANTENNA_ERROR                         		49
#define 		OSAL_C_S32_IOCTL_GPS_GET_NO_RECORDS_AVAILABLE_ERROR 		50
#define		OSAL_C_S32_IOCTL_GPS_GET_RECORD_FIELDS_ERROR              		51
#define		OSAL_C_S32_IOCTL_GPS_GET_INTERNAL_DATA_ERROR                       52
#define		OSAL_GPS_STOP_ERROR                                                           		53
#define		OSAL_DEVICE_CLOSE_ERROR  								   		54	
#define		OSAL_C_S32_IOCTL_GPS_SET_RECORD_FIELDS_ERROR				55
#define		OSAL_C_S32_IOCTL_GPS_READ_RECORD_ERROR					56



/*Specific error codes with values assigned corresponding to the array value in TRT file*/
#define		OSAL_GPS_IOCTRL_TIME_OUT_ERROR								35








/*****************************************************************
| defines  - Error Codes  -Ravi to fill codes
|----------------------------------------------------------------*/
/*Specific error codes with values assigned corresponding to the array value in TRT file*/







/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
   

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/


/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

#endif
