/******************************************************************************
 * FILE				: oedt_FFS2_CommonFS_TestFuncs.h
 *
 * SW-COMPONENT	: OEDT_FrmWrk 
 *
 * DESCRIPTION		: This file defines the macros and prototypes for the 
 *					  filesystem test cases for the FFS2  
 *                          
 * AUTHOR(s)		:  Sriranjan U (RBEI/ECM1)
 *
 * HISTORY			:
 *-----------------------------------------------------------------------------
 * 	Date			|					|	Author & comments
 * --.--.--			|	Initial revision|	------------
 * 01 Dec, 2008	|	version 1.0			|	Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 13 Jan, 2009     |   version 1.1     |   Shilpa Bhat(RBEI/ECM1)
   *-----------------------------------------------------------------------------
* 25 Mar, 2009	    |   Version 1.2     | Vijay D (RBEI/ECF1) 
 *                  |                   | Added osal save_now Ioctrl test case
-------------------------------------------------------------------------------------
  * 25 /03/ 2009  	| version 1.2    	| Lint Remove Anoop Chandran(RBEI/ECF1)
------------------------------------------------------------------------------------
  * 14 /04/ 2009 	| version 1.3    	| Lint Remove Anoop Chandran(RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 10 /09/ 2009   	| version 1.4    	|  Commenting the Test cases which uses 
 *				  	|					|  OSAL_C_S32_IOCTRL_FIOOPENDIR
 *				  	|					|  Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 16 Mar, 2010   | version 1.9           |  OEDT to measure the open time
 *                |                       |  Anoop Chandran
 * ------------------------------------------------------------------------------*/

#ifndef OEDT_FFS2_COMMON_FS_TESTFUNCS_HEADER
#define OEDT_FFS2_COMMON_FS_TESTFUNCS_HEADER

#undef OEDTTEST_C_STRING_DEVICE_FFS2
#ifdef SWITCH_OEDT_FFS2
#define OEDTTEST_C_STRING_DEVICE_FFS2 "/dev/nand0"
#else
#define OEDTTEST_C_STRING_DEVICE_FFS2 OSAL_C_STRING_DEVICE_FFS2
 #endif

#undef OEDT_C_STRING_FFS2_DIR1 
#undef OEDT_C_STRING_FFS2_NONEXST 
#undef OEDT_C_STRING_FFS2_FILE1
#undef SUBDIR_PATH_FFS2	
#undef SUBDIR_PATH2_FFS2   
#undef OEDT_C_STRING_FFS2_DIR_INV_NAME 
#undef OEDT_C_STRING_FFS2_DIR_INV_PATH 
#undef	CREAT_FILE_PATH_FFS2 
#undef OSAL_TEXT_FILE_FIRST_FFS2 
#undef OEDT_C_STRING_FILE_INVPATH_FFS2 
#undef OEDT_FFS2_COMNFILE 
#undef OEDT_FFS2_WRITEFILE 
#undef FILE12_RECR2_FFS2 
#undef FILE11_FFS2 
#undef FILE12_FFS2 

#undef OEDT_C_STRING_DEVICE_FFS2_ROOT 

#undef FILE13_FFS2    
#undef FILE14_FFS2    

#undef OEDT_FFS2_CFS_DUMMYFILE			   
#undef OEDT_FFS2_CFS_TESTFILE			   
#undef OEDT_FFS2_CFS_INVALIDFILE		   
#undef OEDT_FFS2_CFS_UNICODEFILE		   
#undef OEDT_FFS2_CFS_INVALCHAR_FILE		
#undef OEDT_FFS2_CFS_IOCTRL_SAVENOW_FILE_SYNC  
#undef OEDT_FFS2_CFS_IOCTRL_SAVENOW_FILE_ASYNC 

#define OEDT_C_STRING_FFS2_DIR1 OEDTTEST_C_STRING_DEVICE_FFS2"/NewDir"
#define OEDT_C_STRING_FFS2_NONEXST OEDTTEST_C_STRING_DEVICE_FFS2"/Invalid"
#define OEDT_C_STRING_FFS2_FILE1 OEDTTEST_C_STRING_DEVICE_FFS2"/FILEFIRST.txt"
#define SUBDIR_PATH_FFS2	OEDTTEST_C_STRING_DEVICE_FFS2"/NewDir"
#define SUBDIR_PATH2_FFS2	OEDTTEST_C_STRING_DEVICE_FFS2"/NewDir/NewDir2"
#define OEDT_C_STRING_FFS2_DIR_INV_NAME OEDTTEST_C_STRING_DEVICE_FFS2"/*@#**"
#define OEDT_C_STRING_FFS2_DIR_INV_PATH OEDTTEST_C_STRING_DEVICE_FFS2"/MYFOLD1/MYFOLD2/MYFOLD3"
#define	CREAT_FILE_PATH_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/Testf1.txt"  
#define OSAL_TEXT_FILE_FIRST_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/file1.txt"

#define OEDT_C_STRING_FILE_INVPATH_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/Dummydir/Dummy.txt"

#define OEDT_FFS2_COMNFILE OEDTTEST_C_STRING_DEVICE_FFS2"/common.txt"

#define OEDT_FFS2_WRITEFILE OEDTTEST_C_STRING_DEVICE_FFS2"/WriteFile.txt"

#define FILE12_RECR2_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/File_Dir/File_Dir2/File12_test.txt"
#define FILE11_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/File_Dir/File11.txt"
#define FILE12_FFS2 OEDTTEST_C_STRING_DEVICE_FFS2"/File_Dir/File12.txt"

#define OEDT_C_STRING_DEVICE_FFS2_ROOT OEDTTEST_C_STRING_DEVICE_FFS2"/"

#define FILE13_FFS2     OEDTTEST_C_STRING_DEVICE_FFS2"/File_Source/File13.txt"
#define FILE14_FFS2     OEDTTEST_C_STRING_DEVICE_FFS2"/File_Source/File14.txt"

#define OEDT_FFS2_CFS_DUMMYFILE				OEDTTEST_C_STRING_DEVICE_FFS2"/Dummy.txt"
#define OEDT_FFS2_CFS_TESTFILE				OEDTTEST_C_STRING_DEVICE_FFS2"/Test.txt"
#define OEDT_FFS2_CFS_INVALIDFILE			OEDTTEST_C_STRING_DEVICE_FFS2"/DIR/Test.txt"
#define OEDT_FFS2_CFS_UNICODEFILE		  	OEDTTEST_C_STRING_DEVICE_FFS2"/��汪�.txt"
#define OEDT_FFS2_CFS_INVALCHAR_FILE		OEDTTEST_C_STRING_DEVICE_FFS2"//*/</>>.txt"
#define OEDT_FFS2_CFS_IOCTRL_SAVENOW_FILE_SYNC	\
        OEDTTEST_C_STRING_DEVICE_FFS2"/ioctrl_save_now_FFS2_sync.txt"
#define OEDT_FFS2_CFS_IOCTRL_SAVENOW_FILE_ASYNC	\
        OEDTTEST_C_STRING_DEVICE_FFS2"/ioctrl_save_now_FFS2_async.txt"
	 											  
/* Function Prototypes for the functions defined in Oedt_FileSystem_TestFuncs.c */
tU32 u32FFS2_CommonFSOpenClosedevice(tVoid );
tU32 u32FFS2_CommonFSOpendevInvalParm(tVoid );
tU32 u32FFS2_CommonFSReOpendev(tVoid );
tU32 u32FFS2_CommonFSOpendevDiffModes(tVoid );
tU32 u32FFS2_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32FFS2_CommonFSOpenClosedir(tVoid );
tU32 u32FFS2_CommonFSOpendirInvalid(tVoid );
tU32 u32FFS2_CommonFSCreateDelDir(tVoid );
tU32 u32FFS2_CommonFSCreateDelSubDir(tVoid );
tU32 u32FFS2_CommonFSCreateDirInvalName(tVoid );
tU32 u32FFS2_CommonFSRmNonExstngDir(tVoid );
tU32 u32FFS2_CommonFSRmDirUsingIOCTRL(tVoid );
tU32 u32FFS2_CommonFSGetDirInfo(tVoid );
tU32 u32FFS2_CommonFSOpenDirDiffModes(tVoid );
tU32 u32FFS2_CommonFSReOpenDir(tVoid );
tU32 u32FFS2_CommonFSDirParallelAccess(tVoid);
tU32 u32FFS2_CommonFSCreateDirMultiTimes(tVoid ); 
tU32 u32FFS2_CommonFSCreateRemDirInvalPath(tVoid );
tU32 u32FFS2_CommonFSCreateRmNonEmptyDir(tVoid );
tU32 u32FFS2_CommonFSCopyDir(tVoid );
tU32 u32FFS2_CommonFSMultiCreateDir(tVoid );
tU32 u32FFS2_CommonFSCreateSubDir(tVoid);
tU32 u32FFS2_CommonFSDelInvDir(tVoid);
tU32 u32FFS2_CommonFSCopyDirRec(tVoid );
tU32 u32FFS2_CommonFSRemoveDir(tVoid);
tU32 u32FFS2_CommonFSFileCreateDel(tVoid );
tU32 u32FFS2_CommonFSFileOpenClose(tVoid );
tU32 u32FFS2_CommonFSFileOpenInvalPath(tVoid );
tU32 u32FFS2_CommonFSFileOpenInvalParam(tVoid );
tU32 u32FFS2_CommonFSFileReOpen(tVoid );
tU32 u32FFS2_CommonFSFileRead(tVoid );
tU32 u32FFS2_CommonFSFileWrite(tVoid );
tU32 u32FFS2_CommonFSGetPosFrmBOF(tVoid );
tU32 u32FFS2_CommonFSGetPosFrmEOF(tVoid );
tU32 u32FFS2_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32FFS2_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32FFS2_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32FFS2_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32FFS2_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32FFS2_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32FFS2_CommonFSGetFileCRC(tVoid );
tU32 u32FFS2_CommonFSReadAsync(tVoid);
tU32 u32FFS2_CommonFSLargeReadAsync(tVoid);
tU32 u32FFS2_CommonFSWriteAsync(tVoid);
tU32 u32FFS2_CommonFSLargeWriteAsync(tVoid);
tU32 u32FFS2_CommonFSWriteOddBuffer(tVoid);
tU32 u32FFS2_CommonFSFileMultipleHandles(tVoid);
tU32 u32FFS2_CommonFSWriteFileWithInvalidSize(tVoid);
tU32 u32FFS2_CommonFSWriteFileInvalidBuffer(tVoid);
tU32 u32FFS2_CommonFSWriteFileStepByStep(tVoid);
tU32 u32FFS2_CommonFSGetFreeSpace (tVoid);
tU32 u32FFS2_CommonFSGetTotalSpace (tVoid);
tU32 u32FFS2_CommonFSFileOpenCloseNonExstng(tVoid);
tU32 u32FFS2_CommonFSFileDelWithoutClose(tVoid);
tU32 u32FFS2_CommonFSSetFilePosDiffOff(tVoid);
tU32 u32FFS2_CommonFSCreateFileMultiTimes(tVoid);
tU32 u32FFS2_CommonFSFileCreateUnicodeName(tVoid);
tU32 u32FFS2_CommonFSFileCreateInvalName(tVoid);
tU32 u32FFS2_CommonFSFileCreateLongName(tVoid);
tU32 u32FFS2_CommonFSFileCreateDiffModes(tVoid);
tU32 u32FFS2_CommonFSFileCreateInvalPath(tVoid);
tU32 u32FFS2_CommonFSStringSearch(tVoid);
tU32 u32FFS2_CommonFSFileOpenDiffModes(tVoid);
tU32 u32FFS2_CommonFSFileReadAccessCheck(tVoid);
tU32 u32FFS2_CommonFSFileWriteAccessCheck(tVoid);
tU32 u32FFS2_CommonFSFileReadSubDir(tVoid);
tU32 u32FFS2_CommonFSFileWriteSubDir(tVoid);
tU32 u32FFS2_CommonFSReadWriteTimeMeasureof1KbFile(tVoid);
tU32 u32FFS2_CommonFSReadWriteTimeMeasureof10KbFile(tVoid);
tU32 u32FFS2_CommonFSReadWriteTimeMeasureof100KbFile(tVoid);
tU32 u32FFS2_CommonFSReadWriteTimeMeasureof1MBFile(tVoid);
tU32 u32FFS2_CommonFSReadWriteTimeMeasureof1KbFilefor1000times(tVoid);
tU32 u32FFS2_CommonFSReadWriteTimeMeasureof10KbFilefor1000times(tVoid);
tU32 u32FFS2_CommonFSReadWriteTimeMeasureof100KbFilefor100times(tVoid);
tU32 u32FFS2_CommonFSReadWriteTimeMeasureof1MBFilefor16times(tVoid);
tU32 u32FFS2_CommonFSRenameFile(tVoid);
tU32 u32FFS2_CommonFSWriteFrmBOF(tVoid);
tU32 u32FFS2_CommonFSWriteFrmEOF(tVoid);
tU32 u32FFS2_CommonFSLargeFileRead(tVoid);
tU32 u32FFS2_CommonFSLargeFileWrite(tVoid);
tU32 u32FFS2_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32FFS2_CommonFSWriteMultiThread(tVoid);
tU32 u32FFS2_CommonFSWriteReadMultiThread(tVoid);
tU32 u32FFS2_CommonFSReadMultiThread(tVoid);
tU32 u32FFS2_CommonFSFormat(tVoid);
tU32 u32FFS2_CommonFSFileGetExt(tVoid);
tU32 u32FFS2_CommonFSFileGetExt2(tVoid);
tU32 u32FFS2_CommonFSSaveNowIOCTRL_SyncWrite(tVoid);
tU32 u32FFS2_CommonFSSaveNowIOCTRL_AsynWrite(tVoid);
tU32 u32FFS2_CommonFileOpenCloseMultipleTime( tVoid );
tU32 u32FFS2_CommonFileOpenCloseMultipleTimeRandom1( tVoid );
tU32 u32FFS2_CommonFileOpenCloseMultipleTimeRandom2( tVoid );
tU32 u32FFS2_CommonFileLongFileOpenCloseMultipleTime(tVoid);
tU32 u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom1(tVoid);
tU32 u32FFS2_CommonFileLongFileOpenCloseMultipleTimeRandom2(tVoid);
tU32 u32FFS2_CommonFileOpenCloseMultipleTimeMultDir(tVoid);
tU32 u32FFS2_CommonFSGetFileSize(tVoid);
tU32 u32FFS2_CommonFSReadDirValidate(tVoid);
tU32 u32FFS2_CommonFSRmRecursiveCancel(tVoid);
tU32 u32FFS2_CommonFSCreateManyThreadDeleteMain(tVoid);
tU32 u32FFS2_CommonFSCreateManyDeleteInThread(tVoid);
tU32 u32FFS2_CommonFSOpenManyCloseInThread(tVoid);
tU32 u32FFS2_CommonFSOpenInThreadCloseMain(tVoid);
tU32 u32FFS2_CommonFSWriteMainReadInThread(tVoid);
tU32 u32FFS2_CommonFSWriteThreadReadInMain(tVoid);
tU32 u32FFS2_CommonFSFileAccInDiffThread(tVoid);
tU32 u32FFS2_CommonFSReadDirExtPartByPart(tVoid);
tU32 u32FFS2_CommonFSReadDirExt2PartByPart(tVoid);
tU32 u32FFS2_CommonFSCopyDirTest(tVoid);
tU32 u32FFS2_CommonFSReadWriteHugeData(tVoid);
tU32 u32FFS2_CommonFSRW_Performance_Diff_BlockSize(tVoid);

#endif //OEDT_FFS2_COMMON_FS_TESTFUNCS_HEADER

