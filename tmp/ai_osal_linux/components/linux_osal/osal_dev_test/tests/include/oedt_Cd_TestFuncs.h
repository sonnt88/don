/******************************************************************************
 *FILE         : oedt_Cd_TestFuncs.h
 *
 *SW-COMPONENT : ODT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for test of 
 *               DEV_Cdaudio + dev_cdctrl device.
 *
 *AUTHOR       : srt2hi
 *
 *COPYRIGHT    : (c) 2012 BSOT
 *
 *****************************************************************************/

#ifndef OEDT_CD_TESTFUNCS_HEADER
#define OEDT_CD_TESTFUNCS_HEADER


/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: local)
|----------------------------------------------------------------*/
/* prototype declarations   */



//111215 - GEN2 - NISSAN LCN2KAI
tU32 OEDT_CD_TPRINT_NOCD(void);
tU32 OEDT_CD_TPRINT_MASCA01(void);
tU32 OEDT_CD_TPRINT_MASCA02(void);
tU32 OEDT_CD_TPRINT_MASCA03(void);
tU32 OEDT_CD_T000(void);
tU32 OEDT_CD_T001(void);
tU32 OEDT_CD_T002(void);
tU32 OEDT_CD_T003(void);

tU32 OEDT_CD_T010_inslot(void);
tU32 OEDT_CD_T010_nocd(void);
tU32 OEDT_CD_T011(void);
tU32 OEDT_CD_T012_masca01(void);
tU32 OEDT_CD_T012_masca02(void);
tU32 OEDT_CD_T012_masca03(void);
tU32 OEDT_CD_T012_masca03_1(void);
tU32 OEDT_CD_T012_masca04(void);
tU32 OEDT_CD_T012_masca05(void);
tU32 OEDT_CD_T013(void);

tU32 OEDT_CD_T050(void);
tU32 OEDT_CD_T100(void);
tU32 OEDT_CD_T101(void);
tU32 OEDT_CD_T102(void);
tU32 OEDT_CD_T150(void);
tU32 OEDT_CD_T151(void);
tU32 OEDT_CD_T152(void);
tU32 OEDT_CD_T153(void);
tU32 OEDT_CD_T154(void);
tU32 OEDT_CD_T155(void);
tU32 OEDT_CD_T156(void);
tU32 OEDT_CD_T157(void);
tU32 OEDT_CD_T158_masca05(void);
tU32 OEDT_CD_T159(void);

tU32 OEDT_CD_T255(void);

#endif // OEDT_CD_TESTFUNCS_HEADER

