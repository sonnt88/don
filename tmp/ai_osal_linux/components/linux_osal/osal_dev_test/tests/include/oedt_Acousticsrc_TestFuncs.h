/******************************************************************************
 *FILE         : oedt_Acousticsrc_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function declaration for the acoustic device AcousticSRC.
 *
 *AUTHOR       : Niyatha S Rao, ECF5, RBEI
 *
 *HISTORY	   :       
| Date      | Author / Modification
| --.--.--  | ----------------------------------------
| 16.10.12  | Niyatha S Rao, ECF5, RBEI : Initial Revision, 1.0
 *****************************************************************************/

#ifndef OEDT_ACOUSTICSRC_TESTFUNCS_HEADER
#define OEDT_ACOUSTICSRC_TESTFUNCS_HEADER

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/


/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
// OEDT Functions
tU32 OEDT_ACOUSTICSRC_T000(void);
tU32 OEDT_ACOUSTICSRC_T001(void);

/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

#endif

