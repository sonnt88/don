/******************************************************************************
 *FILE         : odt_Acousticin_TestFuncs.h
 *
 *SW-COMPONENT : ODT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function decleration for the acoustic device.
 *                also the array of function pointer for this device has been 
 *                initialised.
 *
 *AUTHOR       : Bernd Schubart, 3SOFT
 *
 *COPYRIGHT    : (c) 2005 Blaupunkt Werke GmbH
 *
 *HISTORY:       25.07.05  3SOFT-Schubart
 *               Initial Revision.
 *
 *               26.04.07  RBIN/ECM-Soj Thomas
 *               Added function prototypes and includes
 *		     07.01.2013 RBEI/ECF5 - Suryachand
 *		     Added test cases for MIC-ADC
 *****************************************************************************/

#ifndef OEDT_ACOUSTICIN_TESTFUNCS_HEADER
#define OEDT_ACOUSTICIN_TESTFUNCS_HEADER

/*****************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

//#include "oedt_Types.h"

/*****************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------*/

#define OSALTEST_PND_USE_NO_AUDIOROUTER

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/
// OEDT Functions
tU32 OEDT_ACOUSTICIN_T000(void);
tU32 OEDT_ACOUSTICIN_T001(void);
tU32 OEDT_ACOUSTICIN_T002(void);
tU32 OEDT_ACOUSTICIN_T003(void);
tU32 OEDT_ACOUSTICIN_T004(void);
tU32 OEDT_ACOUSTICIN_T005(void);
tU32 OEDT_ACOUSTICIN_T006(void);
tU32 OEDT_ACOUSTICIN_T007(void);
tU32 OEDT_ACOUSTICIN_T008(void);
tU32 OEDT_ACOUSTICIN_T009(void);


/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/


#endif

