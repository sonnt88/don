/******************************************************************************
 *FILE         : oedt_Abs_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file has the function declaration for Abs test cases. 
 *                              
 *AUTHOR       : Sai Chowdary Samineni (RBEI/ECF5)
 *
 *COPYRIGHT    : 
 *
 *HISTORY      : 22.07.14  Sai Chowdary Samineni (RBEI/ECF5)
 *
 *****************************************************************************/


#ifndef OEDT_ABS_TESTFUNCS_HEADER
#define OEDT_ABS_TESTFUNCS_HEADER

//Test case function prototypes
tU32 u32AbsOpenDev(void);
tU32 tu32AbsBasicRead(void);
tU32 tu32AbsBasicReadSeq(void);
tU32 tu32AbsInterfaceCheckSeq(void);
tU32 tu32AbsReOpen(void);
tU32 tu32AbsCloseAlreadyClosed(tVoid);
tU32 tu32AbsReadpassingnullbuffer(void);
tU32 u32AbsStatusCheck(tVoid);
tU32 u32AbsGetCycleTime(tVoid);

/* OEDTs for Stub tests */
tU32 tu32AbsStubTestReadDirection(void);



#endif
