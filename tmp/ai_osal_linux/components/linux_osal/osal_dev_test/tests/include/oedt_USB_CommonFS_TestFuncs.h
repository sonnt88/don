/******************************************************************************
 *FILE         : oedt_USB_CommonFS_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function declarations for the USB device.
 *            
  AUTHOR(s)    : Shilpa Bhat (RBEI/ECM1)
 *
 * HISTORY     :
 *-----------------------------------------------------------------------------
 * Date           | 		     		| Author & comments
 * --.--.--       | Initial revision    | ------------
 * 2 Dec, 2008    | version 1.0         | Shilpa Bhat(RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 13 Jan, 2009   | version 1.1     | Shilpa Bhat(RBEI/ECM1)
 ------------------------------------------------------------------------------
 * 24 Mar, 2009	  | version 1.2		| Lint Removal
 *			      |					| Update by Shilpa Bhat(RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 20 May, 2009	  | version 1.3		| Addes new OEDTs TU_OEDT_USB_CFS_091,92,93
 *			      |					| Update by Anoop Chandran(RBEI/ECF1)
 *-----------------------------------------------------------------------------
 * 10 Sep, 2009   | version 1.4    	|  Commenting the Test cases which uses 
 *				  |					|  OSAL_C_S32_IOCTRL_FIOOPENDIR
 *				  |					|  Sriranjan U (RBEI/ECM1)
 *-----------------------------------------------------------------------------
 * 24 Feb, 2010     | version 1.5   |  Updated the Device name 
 *				    |			    |  Anoop Chandran (RBEI/ECF1)
 * 26 Feb, 2013     | version 1.6   |  Removed u32USB_CommonFSPrepareEject()
 *				    |			    |  for OSAL Unsed IO control Removal
 *				    |			    |  By SWM2KOR
 *****************************************************************************/
#ifndef OEDT_USB_CommonFST_TESTFUNCS_HEADER
#define OEDT_USB_CommonFS_TESTFUNCS_HEADER

#define OEDT_C_STRING_USB_CFS_DIR1 OEDTTEST_C_STRING_DEVICE_USB"/NewDir"
#define OEDT_C_STRING_USB_CFS_NONEXST OEDTTEST_C_STRING_DEVICE_USB"/Invalid"
#define OEDT_C_STRING_USB_CFS_FILE1 OEDTTEST_C_STRING_DEVICE_USB"/FILEFIRST.txt"
#define SUBDIR_PATH_USB_CFS	OEDTTEST_C_STRING_DEVICE_USB"/NewDir"
#define SUBDIR_PATH2_USB_CFS	OEDTTEST_C_STRING_DEVICE_USB"/NewDir/NewDir2"
#define OEDT_C_STRING_USB_CFS_DIR_INV_NAME OEDTTEST_C_STRING_DEVICE_USB"/*@#**"
#define OEDT_C_STRING_USB_CFS_DIR_INV_PATH OEDTTEST_C_STRING_DEVICE_USB"/MYFOLD1/MYFOLD2/MYFOLD3"
#define	CREAT_FILE_PATH_USB_CFS OEDTTEST_C_STRING_DEVICE_USB"/Testf1.txt"  
#define OSAL_TEXT_FILE_FIRST_USB_CFS OEDTTEST_C_STRING_DEVICE_USB"/file1.txt"

#define OEDT_C_STRING_FILE_INVPATH_USB_CFS OEDTTEST_C_STRING_DEVICE_USB"/Dummydir/Dummy.txt"

#define OEDT_USB_CFS_COMNFILE OEDTTEST_C_STRING_DEVICE_USB"/common.txt"

#define OEDT_USB_CFS_WRITEFILE OEDTTEST_C_STRING_DEVICE_USB"/WriteFile.txt"

#define FILE12_RECR2_USB_CFS OEDTTEST_C_STRING_DEVICE_USB"/File_Dir/File_Dir2/File12_test.txt"
#define FILE11_USB_CFS OEDTTEST_C_STRING_DEVICE_USB"/File_Dir/File11.txt"
#define FILE12_USB_CFS OEDTTEST_C_STRING_DEVICE_USB"/File_Dir/File12.txt"

#define OEDT_C_STRING_DEVICE_USB_CFS_ROOT OEDTTEST_C_STRING_DEVICE_USB"/"

#define FILE13_USB_CFS     OEDTTEST_C_STRING_DEVICE_USB"/File_Source/File13.txt"
#define FILE14_USB_CFS     OEDTTEST_C_STRING_DEVICE_USB"/File_Source/File14.txt"
#undef  DEBUG_MODE
#define DEBUG_MODE               0


tU32 u32USB_CommonFSOpenClosedevice(tVoid );
tU32 u32USB_CommonFSOpendevInvalParm(tVoid );
tU32 u32USB_CommonFSReOpendev(tVoid );
tU32 u32USB_CommonFSOpendevDiffModes(tVoid );
tU32 u32USB_CommonFSClosedevAlreadyClosed(tVoid );
tU32 u32USB_CommonFSOpenClosedir(tVoid );
tU32 u32USB_CommonFSOpendirInvalid(tVoid );
tU32 u32USB_CommonFSCreateDelDir(tVoid );
tU32 u32USB_CommonFSCreateDelSubDir(tVoid );
tU32 u32USB_CommonFSCreateDirInvalName(tVoid );
tU32 u32USB_CommonFSRmNonExstngDir(tVoid );
tU32 u32USB_CommonFSRmDirUsingIOCTRL(tVoid );
tU32 u32USB_CommonFSGetDirInfo(tVoid );
tU32 u32USB_CommonFSOpenDirDiffModes(tVoid );
tU32 u32USB_CommonFSReOpenDir(tVoid );
tU32 u32USB_CommonFSDirParallelAccess(tVoid);
tU32 u32USB_CommonFSCreateDirMultiTimes(tVoid ); 
tU32 u32USB_CommonFSCreateRemDirInvalPath(tVoid );
tU32 u32USB_CommonFSCreateRmNonEmptyDir(tVoid );
tU32 u32USB_CommonFSCopyDir(tVoid );
tU32 u32USB_CommonFSMultiCreateDir(tVoid );
tU32 u32USB_CommonFSCreateSubDir(tVoid);
tU32 u32USB_CommonFSDelInvDir(tVoid);
tU32 u32USB_CommonFSCopyDirRec(tVoid );
tU32 u32USB_CommonFSRemoveDir(tVoid);
tU32 u32USB_CommonFSFileCreateDel(tVoid );
tU32 u32USB_CommonFSFileOpenClose(tVoid );
tU32 u32USB_CommonFSFileOpenInvalPath(tVoid );
tU32 u32USB_CommonFSFileOpenInvalParam(tVoid );
tU32 u32USB_CommonFSFileReOpen(tVoid );
tU32 u32USB_CommonFSFileRead(tVoid );
tU32 u32USB_CommonFSFileWrite(tVoid );
tU32 u32USB_CommonFSGetPosFrmBOF(tVoid );
tU32 u32USB_CommonFSGetPosFrmEOF(tVoid );
tU32 u32USB_CommonFSFileReadNegOffsetFrmBOF(tVoid );
tU32 u32USB_CommonFSFileReadOffsetBeyondEOF(tVoid );
tU32 u32USB_CommonFSFileReadOffsetFrmBOF(tVoid );
tU32 u32USB_CommonFSFileReadOffsetFrmEOF(tVoid );
tU32 u32USB_CommonFSFileReadEveryNthByteFrmBOF(tVoid );
tU32 u32USB_CommonFSFileReadEveryNthByteFrmEOF(tVoid );
tU32 u32USB_CommonFSGetFileCRC(tVoid );
tU32 u32USB_CommonFSReadAsync(tVoid);						//TU_OEDT_USB_CFS_042
tU32 u32USB_CommonFSLargeReadAsync(tVoid);					//TU_OEDT_USB_CFS_043
tU32 u32USB_CommonFSWriteAsync(tVoid);						//TU_OEDT_USB_CFS_044
tU32 u32USB_CommonFSLargeWriteAsync(tVoid);					//TU_OEDT_USB_CFS_045
tU32 u32USB_CommonFSWriteOddBuffer(tVoid);					//TU_OEDT_USB_CFS_046
tU32 u32USB_CommonFSWriteFileWithInvalidSize(tVoid);		//TU_OEDT_USB_CFS_048
tU32 u32USB_CommonFSWriteFileInvalidBuffer(tVoid);			//TU_OEDT_USB_CFS_049
tU32 u32USB_CommonFSWriteFileStepByStep(tVoid);				//TU_OEDT_USB_CFS_050
tU32 u32USB_CommonFSGetFreeSpace(tVoid);					//TU_OEDT_USB_CFS_051
tU32 u32USB_CommonFSGetTotalSpace(tVoid);					//TU_OEDT_USB_CFS_052
tU32 u32USB_CommonFSFileOpenCloseNonExstng(tVoid);			//TU_OEDT_USB_CFS_053
tU32 u32USB_CommonFSFileDelWithoutClose(tVoid);					//TU_OEDT_USB_CFS_054
tU32 u32USB_CommonFSSetFilePosDiffOff(tVoid);				//TU_OEDT_USB_CFS_055
tU32 u32USB_CommonFSCreateFileMultiTimes(tVoid);			//TU_OEDT_USB_CFS_056
tU32 u32USB_CommonFSFileCreateUnicodeName(tVoid);			//TU_OEDT_USB_CFS_057
tU32 u32USB_CommonFSFileCreateInvalName(tVoid);					//TU_OEDT_USB_CFS_058
tU32 u32USB_CommonFSFileCreateLongName(tVoid);				//TU_OEDT_USB_CFS_059
tU32 u32USB_CommonFSFileCreateDiffModes(tVoid);				//TU_OEDT_USB_CFS_060
tU32 u32USB_CommonFSFileCreateInvalPath(tVoid);				//TU_OEDT_USB_CFS_061
tU32 u32USB_CommonFSFileStringSearch(tVoid);				//TU_OEDT_USB_CFS_062
tU32 u32USB_CommonFSFileOpenDiffModes(tVoid);
tU32 u32USB_CommonFSFileReadAccessCheck(tVoid);
tU32 u32USB_CommonFSFileWriteAccessCheck(tVoid);
tU32 u32USB_CommonFSFileReadSubDir(tVoid);
tU32 u32USB_CommonFSFileWriteSubDir(tVoid);
tU32 u32USB_CommonFSTimeMeasureof1KbFile(tVoid);
tU32 u32USB_CommonFSTimeMeasureof10KbFile(tVoid);
tU32 u32USB_CommonFSTimeMeasureof100KbFile(tVoid);
tU32 u32USB_CommonFSTimeMeasureof1MBbFile(tVoid);
tU32 u32USB_CommonFSTimeMeasureof1KbFilefor1000times(tVoid);
tU32 u32USB_CommonFSTimeMeasureof10KbFilefor1000times(tVoid);
tU32 u32USB_CommonFSTimeMeasureof100KbFilefor100times(tVoid);
tU32 u32USB_CommonFSTimeMeasureof1MBFilefor16times(tVoid);
tU32 u32USB_CommonFSRenameFile(tVoid);
tU32 u32USB_CommonFSWriteFrmBOF(tVoid);
tU32 u32USB_CommonFSWriteFrmEOF(tVoid);
tU32 u32USB_CommonFSLargeFileRead(tVoid);
tU32 u32USB_CommonFSLargeFileWrite(tVoid);
tU32 u32USB_CommonFSOpenCloseMultiThread(tVoid);
tU32 u32USB_CommonFSWriteMultiThread(tVoid);
tU32 u32USB_CommonFSWriteReadMultiThread(tVoid);
tU32 u32USB_CommonFSReadMultiThread(tVoid);
tU32 u32USB_CommonFSFormat(tVoid);
tU32 u32USB_CommonFSCheckDisk(tVoid);
tU32 u32USB_CommonFSPerformance_Diff_BlockSize(tVoid);
tU32 u32USB_CommonFSFileGetExt(tVoid);
tU32 u32USB_CommonFSFileGetExt2(tVoid);

tU32 u32USB_GetDeviceInfo(tVoid);
tU32 u32USB_GetDeviceInfoInval(tVoid);
tU32 u32USB_IPOD_Activate_Deactivate_USB_VBUS(tVoid);


#endif
