/******************************************************************************
 *FILE         : oedt_Illumination_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_Illumination_TestFuncs.c for the  
 *               Timer device for the GM-GE hardware.
 *
 *AUTHOR       : Anoop Chandran (RBIN/EDI3) 
 *
 *HISTORY      : 
 					v 1.3, 04-02-2009 
					Added new test case
					TU_OEDT_ILLUMINATION_019
 				
 					03-oct-2007 Initial  Version v1.0 - Anoop Chandran
*******************************************************************************/
#define ILLU_PWM_DUTYCYCLE_MINVAL	0
#define ILLU_PWM_DUTYCYCLE_MIDVAL	32767
#define ILLU_PWM_DUTYCYCLE_MAXVAL	65535
#define PWM_DUTYCYCLE_INVAL_MIN -1
#define PWM_DUTYCYCLE_INVAL_MAX 65536

#define OSAL_C_S32_IOCTRL_INVAL_ILLU_FUNC 	        	-1000
#define OSAL_C_STRING_INVALID_DEVICE_ILLUMINATION	-1
#define OSAL_C_STRING_ILLU_INVALID_ACCESS_MODE           -1
#define ILLUMINATION_INVAL_PARAM					0

 
#ifndef OEDT_ILLUMINATION_TESTFUNCS_HEADER
#define OEDT_ILLUMINATION_TESTFUNCS_HEADER

/* Macro definition used in the code */

 
 /* Function prototypes of functions used 
				in file oedt_Illumination _TestFuns.c */
/*Test cases*/

tU32 u32IlluminationDevOpenClose(tVoid);
tU32 u32IlluminationDevOpenCloseInvalParam(tVoid);
tU32 u32IlluminationDevOpenWithNULLParam(tVoid);
tU32 u32IlluminationDevMultipleOpen(tVoid);
tU32 u32IlluminationDevCloseAlreadyClosed(tVoid);
tU32 u32IlluminationDevCloseInvalHandle(tVoid);
tU32 u32IlluminationGetVersion(tVoid);
tU32 u32IlluminationGetVersionInvalParam(tVoid);
tU32 u32IlluminationrSetPWMInRange( tVoid );
tU32 u32lluminationrSetPWMOutRange(tVoid);
tU32 u32IlluminationGetPWM(tVoid);
tU32 u32IlluminationGetPWMInvalParam(tVoid);
tU32 u32lluminationSetGetPWM(tVoid);
tU32 u32lluminationInvalidSetGetPWM(tVoid);
tU32 u32lluminationInvalFuncParamToIOCtrl(tVoid);
tU32 u32IlluminationrMultSetPWM(tVoid);
tU32 u32IlluminationIOControlsAfterClose(tVoid);
tU32 u32lluminationGetPWMAfterClose(tVoid);
tU32 u32lluminationSetGetPWMInval(tVoid);
tU32 u32lluminationSetBackLightLCD(tVoid);


/*Test cases*/
#endif
