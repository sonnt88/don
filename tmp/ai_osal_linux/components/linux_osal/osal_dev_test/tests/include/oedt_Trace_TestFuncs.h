/******************************************************************************
 *FILE         : oedt_Trace_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_Trace_TestFuncs.c for the  
 *               Trace device for the GM-GE hardware.
 *
 *AUTHOR       : Shilpa Bhat (RBIN/EDI3) 
 *
 *HISTORY      : 21 Nov, 2007 ver 0.1 RBIN/EDI3- Shilpa Bhat
 *               03 Dec, 2007 ver 0.2 RBIN/EDI3- Shilpa Bhat
 *               02 Apr, 2009 ver 0.3 RBIN/ECF1- Shilpa Bhat  
*******************************************************************************/

#include "osal_if.h"
#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	
#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif
#include <string.h>

#ifndef OEDT_TRACE_TESTFUNCS_HEADER
#define OEDT_TRACE_TESTFUNCS_HEADER

/* Macro definition used in the code */
#define OEDT_TRACE_MAX_LENGTH			20
#define BYTES_TO_WRITE		10
#define MULTIPLE_DEV_OPEN		255			
#define OSAL_C_S32_OEDTTRACE_IOCTRL_INVAL_FUNC 		-1000
#define OSAL_C_STRING_OEDTTRACE_INVALID_ACCESS_MODE   -1
#define TRACE_INVAL_PARAM			-1

#define  TRIP_FILE_REC     OSAL_C_STRING_DEVICE_RAMDISK"/TripFileRec.txt"
#define  TRIP_FILE_REPLAY  OSAL_C_STRING_DEVICE_RAMDISK"/TripFileReplay.txt"

/* Function prototypes of functions used in file oedt_Trace_TestFuns.c */
tVoid RegCallback(tVoid);
tVoid UnRegCallback(tVoid);
tU32 u32TraceDevOpenClose(tVoid);
tU32 u32TraceDevOpenCloseInvalParam(tVoid);
tU32 u32TraceDevCloseAlreadyClosed(tVoid);
tU32 u32TraceDevReOpen(tVoid);
tU32 u32TraceDevOpenCloseDiffModes(tVoid);
tU32 u32TraceDevOpenCloseInvalAccessMode(tVoid);
tU32 u32TraceDevRead(tVoid);
tU32 u32TraceDevWrite(tVoid);
tU32 u32TraceGetVersion(tVoid);
tU32 u32TraceCheckActive(tVoid);
tU32 u32TraceCallbackRegUnreg(tVoid);
tU32 u32TraceSendBinaryData(tVoid);
tU32 u32TraceGetOutputChan(tVoid);
tU32 u32TraceSetOutputChanUSB(tVoid);
tU32 u32TraceSetOutputChanUART(tVoid);
tU32 u32TraceTripFuncCheck(tVoid);
tU32 u32TraceInvalIOCtrlParam(tVoid);
tU32 u32TraceGetVersionNumberInvalParam(tVoid);
tU32 u32TraceGetVersionNumberAfterDevClose(tVoid);
tU32 u32TraceDeviceOpenMultipleTimes(tVoid);
tU32 u32TraceWriteInvalSize(tVoid);
tU32 u32TraceWriteInvalBuffer(tVoid);

#endif

