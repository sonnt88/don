/******************************************************************************
 * FILE         : oedt_SWC_TestFuncs.h
 *
 * SW-COMPONENT : OEDT_FrmWrk
 *
 * DESCRIPTION  : This file implements the individual test cases for the SWC
 *                device in LSIM side(LCN2KAI).
 * AUTHOR(s)    : Manikandan Dhanasekaran (RBEI/ECF5)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date                | 	Initial revision       | Author & comments
 *-----------------------------------------------------------------------------
 * 24,January, 2013      |        1.0              | Manikandan Dhanasekaran (RBEI/ECF5)
 *                                                 | File is modified for enabling 
                                                     the SWC test cases in LSIM side.
 *-----------------------------------------------------------------------------
 *******************************************************************************/

#ifndef OEDT_SWC_TESTFUNCS_HEADER
#define OEDT_SWC_TESTFUNCS_HEADER

#define OEDT_DRV_SWC_TEST_PASSED 0
#define OEDT_DRV_SWC_TEST_FAILED 1

typedef struct
{
   tU32 u32Keycode;
   tU32 u32KeyEvent;
}oedt_trkeyCodeEventinfo;

typedef oedt_trkeyCodeEventinfo oedt_trkeypressinfo;
typedef oedt_trkeyCodeEventinfo oedt_trkeyreleaseinfo;

typedef struct
{
   tU16 swc_u16noofkeypress;
   tU16 swc_u16noofkeyrelease;
   oedt_trkeypressinfo * rkeypressinfo;
   oedt_trkeyreleaseinfo * rkeyreleaseinfo;
   tBool bIoCtrlTest;
}troedt_keyseqinfo;

tU32 u32SteeringWheelDevOpen(tVoid);
tU32 u32SteeringWheelDev_MultiOpen(tVoid);
tU32 u32SteeringWheelDevclose(tVoid);
tU32 u32SteeringWheelDev_Multiclose(tVoid);
tU32 u32SteeringWheelDevIOControl_GetVersion(tVoid);
tU32 u32SteeringWheelDevIOControl_SetCallBack(tVoid);

tU32 u32SteeringWheelKEY1Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey1(tVoid);
tU32 u32SteeringWheelKEY1Release_Test(tVoid);
tU32 u32SteeringWheelKEY1PressRelease_Test(tVoid);

tU32 u32SteeringWheelKEY2Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey2(tVoid);
tU32 u32SteeringWheelKEY2Release_Test(tVoid);
tU32 u32SteeringWheelKEY2PressRelease_Test(tVoid);

tU32 u32SteeringWheelKEY3Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey3(tVoid);
tU32 u32SteeringWheelKEY3Release_Test(tVoid);
tU32 u32SteeringWheelKEY3PressRelease_Test(tVoid);

tU32 u32SteeringWheelKEY4Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey4(tVoid);
tU32 u32SteeringWheelKEY4Release_Test(tVoid);
tU32 u32SteeringWheelKEY4PressRelease_Test(tVoid);

tU32 u32SteeringWheelKEY5Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey5(tVoid);
tU32 u32SteeringWheelKEY5Release_Test(tVoid);
tU32 u32SteeringWheelKEY5PressRelease_Test(tVoid);

tU32 u32SteeringWheelKEY6Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey6(tVoid);
tU32 u32SteeringWheelKEY6Release_Test(tVoid);
tU32 u32SteeringWheelKEY6PressRelease_Test(tVoid);

tU32 u32SteeringWheelKEY7Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey7(tVoid);
tU32 u32SteeringWheelKEY7Release_Test(tVoid);
tU32 u32SteeringWheelKEY7PressRelease_Test(tVoid);

/*The following functions are commented, as the LSIM SWC image contains only 7 Keys at present. These can be used in future.
/*
tU32 u32SteeringWheelKEY8Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey8(tVoid);
tU32 u32SteeringWheelKEY8Release_Test(tVoid);
tU32 u32SteeringWheelKEY8PressRelease_Test(tVoid);

tU32 u32SteeringWheelKEY9Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey9(tVoid);
tU32 u32SteeringWheelKEY9Release_Test(tVoid);
tU32 u32SteeringWheelKEY9PressRelease_Test(tVoid);

tU32 u32SteeringWheelKEY10Press_Test(tVoid);
tU32 u32SteeringWheelDevIOControl_GetKey10(tVoid);
tU32 u32SteeringWheelKEY10Release_Test(tVoid);
tU32 u32SteeringWheelKEY10PressRelease_Test(tVoid);
*/

tU32 u32SteeringWheelKEY2KEY1Press_Test(tVoid);
tU32 u32SteeringWheelKEY3KEY1Press_Test(tVoid);
tU32 u32SteeringWheelKEY4KEY1Press_Test(tVoid);
tU32 u32SteeringWheelKEY3KEY2Press_Test(tVoid);
tU32 u32SteeringWheelKEY4KEY2Press_Test(tVoid);
tU32 u32SteeringWheelKEY4KEY3Press_Test(tVoid);
tU32 u32SteeringWheelKEY1KEY2Press_Test(tVoid);
tU32 u32SteeringWheelKEY1KEY3Press_Test(tVoid);
tU32 u32SteeringWheelKEY2KEY3Press_Test(tVoid);


tU32 u32SteeringWheelKEY3KEY2KEY1Press_Test(tVoid);
tU32 u32SteeringWheelKEY4KEY3KEY2Press_Test(tVoid);
tU32 u32SteeringWheelKEY4KEY3KEY1Press_Test(tVoid);
tU32 u32SteeringWheelKEY4KEY2KEY1Press_Test(tVoid);

tU32 u32SteeringWheelKEY4KEY3KEY2KEY1Press_Test(tVoid);

#endif
