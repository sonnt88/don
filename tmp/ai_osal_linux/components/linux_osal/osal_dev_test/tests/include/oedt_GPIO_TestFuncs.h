/******************************************************************************
 *FILE         : oedt_GPIO_TestFuncs.h
 *
 *SW-COMPONENT : OEDT_FrmWrk 
 *
 *DESCRIPTION  : This file contains function prototypes and some Macros that 
 				 will be used in the file oedt_GPIO_TestFuncs.c for the  
 *               GPIO device for the GM-GE hardware.
 *
 *AUTHOR       : Shilpa Bhat (RBIN/EDI3) 
 *
 *HISTORY      : 03 Oct, 2007 ver 0.1  RBIN/EDI3- Shilpa Bhat
 *               21 Mar, 2004 ver 0.2 RBIN/ECM1 - Shilpa Bhat
 *               28 Apr, 2008 ver 0.3 RBEI/ECM1 - Shilpa Bhat
 *               07 May, 2008 ver 0.4 RBEI/ECM1 - Shilpa Bhat
 *               23 May, 2008 ver 0.5 RBEI/ECM1 - Shilpa Bhat
  *              29 Sept, 2008 ver 0.6 RBEI/ECM1 - Shilpa Bhat
  *              29 Oct, 2008 ver 0.7 RBEI/ECM1 - Anoop Chandran
 *               11 Nov, 2008 ver 0.8 RBEI/ECM1 - Shilpa Bhat  
 *               24 Mar, 2009 ver 0.9 RBEI/ECF1 - Shilpa Bhat
 *               14 Aug, 2012 ver 1.0 CM-AI/PJ-CF31 - Matthias Thomae
*******************************************************************************/

#include "osal_if.h"

#include "osdevice.h"
#include <stdio.h>
#include <stdlib.h>	
#include <sys/stat.h>
#include <fcntl.h>
#ifndef TSIM_OSAL
#include <unistd.h>
#endif 
#include <string.h>

#ifndef OEDT_GPIO_TESTFUNCS_HEADER
#define OEDT_GPIO_TESTFUNCS_HEADER

/* Macro definition used in the code */
#define MQ_GPIO_MP_CONTROL_MAX_MSG     10
#define MQ_GPIO_MP_CONTROL_MAX_LEN     sizeof(tU32)
#define MQ_GPIO_MP_RESPONSE_NAME       "MQ_GPIO_RSPNS"

/* Function prototypes of functions used in file oedt_GPIO_TestFuns.c */
tU32 u32GPIODevOpenClose(tVoid);
tU32 u32GPIODevCloseAlreadyClosed(tVoid);
tU32 u32GPIODevReOpen(tVoid);
tU32 u32GPIODevOpenCloseDiffModes(tVoid);
tU32 u32GPIODevOpenCloseInvalAccessMode(tVoid);
tU32 u32GPIODevRead(tVoid);
tU32 u32GPIODevWrite(tVoid);
tU32 u32GPIOGetVersion(tVoid);
tU32 u32GPIOSetInput(tVoid);
tU32 u32GPIOSetInputRemote(tVoid);
tU32 u32GPIOSetOutput(tVoid);
tU32 u32GPIOSetOutputRemote(tVoid);
tU32 u32GPIOSetRemoveCallback(tVoid);
tU32 u32GPIOSetTrigEdgeHIGHIntEnable(tVoid);
tU32 u32GPIOSetTrigEdgeHIGHIntEnableRemote(tVoid);
tU32 u32GPIOSetTrigEdgeLOWIntEnable(tVoid);
tU32 u32GPIOSetTrigEdgeLOWIntEnableRemote(tVoid);
tU32 u32GPIOSetStateLOW(tVoid);
tU32 u32GPIOSetStateHIGH(tVoid);
tU32 u32GPIOGetState(tVoid);
tU32 u32GPIOSetStateInactive(tVoid);
tU32 u32GPIOSetStateInactiveRemote(tVoid);
tU32 u32GPIOSetStateActive(tVoid);
tU32 u32GPIOSetStateActiveRemote(tVoid);
tU32 u32GPIOIsStateActive(tVoid);
tU32 u32GPIOIsStateActiveRemote(tVoid);
tU32 u32GPIOSetOutputHIGH(tVoid);
tU32 u32GPIOSetOutputLOW(tVoid);
tU32 u32GPIOSetOutputHIGHLOW(tVoid);
tU32 u32GPIOSetOutputActive(tVoid);
tU32 u32GPIOSetOutputActiveRemote(tVoid);
tU32 u32GPIOSetOutputInactive(tVoid);
tU32 u32GPIOSetOutputInactiveRemote(tVoid);
tU32 u32GPIOSetOutputActiveInactive(tVoid);
tU32 u32GPIOSetOutputActiveInactiveRemote(tVoid);
tU32 u32GPIOSetOutputInInputMode(tVoid);
tU32 u32GPIOSetOutputInInputModeRemote(tVoid);
tU32 u32GPIOInvalFuncParamToIOCtrl(tVoid);
tU32 u32GPIOMultiThreadTest(tVoid);
tU32 u32GPIOMultiThreadTestRemote(tVoid);
tU32 u32GPIOTwoInputPins(tVoid);
tU32 u32GPIOTwoInputPinsRemote(tVoid);
tU32 u32GPIOMultiProcessTest(tVoid);
tU32 u32GPIOMultiProcessTestRemote(tVoid);
tU32 u32GPIOCallbackTest(tVoid);
tU32 u32GPIOCallbackRegOutputMode(tVoid);
tU32 u32GPIOCallbackRegUnReg(tVoid);
tU32 u32GPIOThreadCallbackReg(tVoid);
tU32 u32GPIO_OSAL_test(tVoid);
tU32 u32GPIOActiveStateTest(tVoid);

#endif
