/******************************************************************************
*FILE         : oedt_SPM_TestFuncs.h
*
*SW-COMPONENT : OEDT_FrmWrk 
*
*DESCRIPTION  : This file has the function delcalartions for test functionsses in the SPM 
*               device for OEDT mode
*               
*               
*
*AUTHOR       : 
*
*COPYRIGHT    : (c) 2006 Blaupunkt GmbH
*
*HISTORY      :  Initial version
*
*****************************************************************************/

#ifndef OEDT_SPM_TESTFUNCS_HEADER
#define OEDT_SPM_TESTFUNCS_HEADER
/*****************************************************************
| function decalaration (scope: Local) 
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------*/


/*Test cases*/

tU32 u32SPM_OpenCloseDevs(void);
tU32 u32SPM_StartOverVoltageListener(void);
tU32 u32SPM_StopOverVoltageListener(void);
tU32 u32SPM_StartUserVoltageListener(void);
tU32 u32SPM_StopUserVoltageListener(void);
tU32 u32SPM_Volt_CreateRemoveNotifications(void);
tU32 u32SPM_Volt_BoardVoltage(void);
tU32 u32SPM_Volt_UserVoltageRange(void);
tU32 u32SPM_Volt_BoardCurrentScale(void);
tU32 u32SPM_Volt_SysLevelHistory(void);
tU32 u32SPM_Volt_UserLevelHistory(void);
tU32 u32SPM_Volt_VoltageLevel(void);
tU32 u32SPM_Volt_ReadBoardCurrent(void);
tU32 u32SPM_StartCVMLowVoltageListener(void);
tU32 u32SPM_StopCVMLowVoltageListener(void);
tU32 u32SPM_Volt_IOCTRL_Verify(void);

struct device
{
   tCString dev_name;
}; 


/*Test cases*/


#endif


