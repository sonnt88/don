
/******************************************************************************
| FILE:			oedt_SPI1_TestFuncs.c

| PROJECT:      TE_Plattform_INT_XX  (RBIN Sub Project ID: 70705_T_OEDT)
| SW-COMPONENT: OEDT FWK
|------------------------------------------------------------------------------
| DESCRIPTION:  This file implements the individual test cases for the SPI1 
                device
|               The corresponding test specs:  TU_OEDT_SPI1_TestSpec.xls
                                     version:  2.5
|------------------------------------------------------------------------------
| HISTORY:
|______________________________________________________________________________
| Date          | 				               | Author	& comments
| --.--.--      | Initial revision             | -------, -----
|2 June,2006    |  version 1.0                 |Haribabu Sannapaneni(RBIN/ECM1)
|------------------------------------------------ -----------------------------
|			    |	                           |
|6 June,2006    |  version 2.0                 |Haribabu Sannapaneni(RBIN/ECM1)
|   		    |                              | Updated after review comments
|------------------------------------------------ -----------------------------
|			    |	                           |
|3 Oct,2006     |  version 2.1                 | Anoop Krishnan(RBIN/ECM1)
|   		    |                              | Added more test cases after
| 				|							   | TCA
|------------------------------------------------ -----------------------------
|			    |	                           |
|6 nov,2006     |  version 2.2                 |Tinoy Mathews
|   		    |                              |Updated based on test spec from 
|				|                              |Mr.Andreas Diesner
|
|------------------------------------------------ -----------------------------
|			    |	                           |
|5 Jan,2007     |  version 2.3                 |Kishore Kumar.R
|   		    |                              |Added  and updated 
|				|							   |		new test cases  										
|------------------------------------------------------------------------------
|			    |	                           |
|19 Jan,2007    |  version 2.4                 |Kishore Kumar.R
|   		    |                              | Updated  test cases  TU_OEDT_SPI1_021,022,027,028.
|				|							   |   ( Alphabet String was replaced by numbers( digits)  )
        |							   |		TU_OEDT_SPI1_031.		 										
|------------------------------------------------------------------------------
|			    |	                           |
|07 Feb ,2007   |  version 2.5                 | Rakesh Dhanya 
|   		    |                              | Updated cases TU_OEDT_SPI1_025,026
|				|							   | Commented async write and read  
        |							   | cases (TU_OEDT_SPI1028,029 as in v2.4)								 										
|_____________________________________________________________________________
|				|
|12 March,07	|  Version 2.6				   | Kishore Kumar.R	
|				|							   |   Updated test cases according to
|				|							   |comments in MMS TICKETS 125832,125833 ,128977.
|				|							   |(Asynchronous operations not supported,names of functions changed)
------------------------------------------------------------------------------------------------------------------*
| 3 May,2007    |							   |Kishore Kumar. R
|				|  Version 2.7				   |  Test cases TU_OEDT_SPI1_026- 029  has been commented based
|				|							   |  MMS reply (Ticket no-132705).As, the macro
|				|							   |  OSAL_C_S32_IOCTRL_SPI_SPI_SYNC_START  is not used in GM project.
==================================================================================================================
| 10 May,2007	|							   |Tinoy Mathews(RBIN/EDI3)
|				| Version 2.8                  |Test cases TU_OEDT_SPI1_022,023
|				|							   |
|				|							   |
==================================================================================================================
|				|							   |
|25 Sept,2007	| Version 2.9                  |Tinoy Mathews(RBIN/EDI3) 
|				|							   |Updated cases TU_OEDT_SPI1_002,004,009,019,020
|				|							   |
==================================================================================================================
|18 June, 2008  | version 3.0				   | Shilpa Bhat (RBEI/ECM1)
|               |							   | Updated case u32SPI1_RadioUnit_ADR_TranceiveTunerIC
|				|							   |
==================================================================================================================
|27 Jan, 2009  | version 3.1				   | Anoop Chandran(RBEI/ECF1)
|               |							   | test case TU_OEDT_SPI1_011and 012 commented
|					|						   | Updated Test case TU_OEDT_SPI1_023and 024 
|					|						   |
=================================================================================================================
==================================================================================================================
|25 Mar, 2009  | version 3.2				   | Sainath Kalpuri(RBEI/ECF1)
|               |							   | Removed Lint and compiler warnings 
|					|						   |
==================================================================================================================
|22 Apr, 2009  | version 3.3				   | Sainath Kalpuri(RBEI/ECF1)
|               |							   | Removed Lint and compiler warnings 
|					|						   |
=================================================================================================================*/


#define OSAL_S_IMPORT_INTERFACE_GENERIC

#include "osal_if.h"
#include "osioctrl.h"

#include "lld_spi.h"
#include "lld_deviceconfig.h"
#include "drv_gpio.h"

#include "oedt_SPI1_Testfuncs.h"

#include "oedt_local_dtt.h"
/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/
//#define OSAL_C_TRACELEVEL1  	TR_LEVEL_ERRORS
//#define OEDT_TRUE                             0
//#define OEDT_FALSE                            1	
//#define OEDT_ORDER_OF_KILOBYTES				  1

/******************************************************************************
| global variables
|----------------------------------------------------------------------------*/
OSAL_tEventHandle  tHandleEvent_ADRRequestLine_GLOB = OSAL_C_INVALID_HANDLE;
OSAL_tEventMask    tEventMask_ADRRequestLine_GLOB   = 0x00000001;
tBool bRequestLineSet = FALSE;

/*event*/
OSAL_tEventHandle  tHandleEvent_CAPRequestLine = OSAL_C_INVALID_HANDLE;
OSAL_tEventMask    tEventMask_CAPRequestLine   = 0x00000001;
/*thread*/
static OSAL_trThreadAttribute   tThreadAttributeReadKey_CAP;
static OSAL_tThreadID           tIDThreadReadKey_CAP  = OSAL_ERROR;
/******************************************************************************
| function implementation (scope: module-local)
| Please note : All functions using low level driver calls have been enclosed
|               in the OEDT_SPI1_KERNEL_MODE macro.
----------------------------------------------------------------------------*/


#if OEDT_SPI1_KERNEL_MODE
/*Callback function*/
tVoid Callback(UW dintno_ui32)
{
  ClearInt (dintno_ui32);
  bRequestLineSet = TRUE;

}
#endif

/*Check if callback was called*/
tBool CallbackCheck(OSAL_tEventHandle handleEvent, OSAL_tEventMask  maskEvent, OSAL_tMSecond timout)
{
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(handleEvent);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(maskEvent);
  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(timout);
  OSAL_s32ThreadWait(50);
  if(bRequestLineSet == FALSE)
  {
    return FALSE;
  }

  bRequestLineSet = FALSE;
  return TRUE;
}

/*Close SPI and GPIO devices*/
tU32 CloseDeviceGPIOSPI1(const OSAL_tIODescriptor *hDeviceGPIO, const OSAL_tIODescriptor *hDeviceSPI1 )
{
  tU32 u32Ret = 0;

  if(OSAL_s32IOClose( *hDeviceGPIO ) == OSAL_ERROR)
  {
    u32Ret = 100;
  }
  if(OSAL_s32IOClose( *hDeviceSPI1 ) == OSAL_ERROR)
  {
    u32Ret +=1000;
  }
  return u32Ret;
}

/*Event Creation*/
tBool bCreEvent(OSAL_tEventHandle* phandleEvent,tCString coszName)
{
  if(OSAL_C_INVALID_HANDLE != *phandleEvent)
  {
    /*Open event if already existing*/
    if(OSAL_s32EventOpen(coszName,phandleEvent) != OSAL_OK)
      return FALSE;
  }
  else
  {
    if(OSAL_s32EventCreate(coszName,phandleEvent)!= OSAL_OK)
      return FALSE;
  }
  return TRUE;
}
/*delete event*/
tBool  bDelEvent(OSAL_tEventHandle* PpEventHandle,tCString coszName)
{/*close Event */
  if(OSAL_OK == OSAL_s32EventClose(*PpEventHandle))
  {
    if(OSAL_OK != OSAL_s32EventDelete(coszName))
    {
      return FALSE;
    }/*end if*/
  }
  else
  {
    return FALSE;
  }
  *PpEventHandle=OSAL_C_INVALID_HANDLE;
  return TRUE;
}/*end function*/

/*create thread*/
tBool  bOpenThread(OSAL_trThreadAttribute PAttributte,OSAL_tThreadID *PpWorkerId)
{  
  PAttributte.u32Priority  = OSAL_C_U32_THREAD_PRIORITY_NORMAL; //HIGHEST;	
  PAttributte.pvArg        = NULL;
  PAttributte.s32StackSize = 1024;

  *PpWorkerId = OSAL_ThreadSpawn(&PAttributte);
  if(*PpWorkerId == OSAL_ERROR)
  {
    return FALSE;
  }
  return(TRUE);
}/*end function*/

/*close thread*/
tBool bCloseThread(OSAL_tThreadID PpWorkerId)
{
  OSAL_trThreadControlBlock VTcb;
  do //while((VTcb.enStatus != OSAL_EN_THREAD_READY))
  {
    if(OSAL_s32ThreadControlBlock(PpWorkerId, &VTcb) != OSAL_OK)
    {
      VTcb.enStatus = OSAL_EN_THREAD_READY;
    }/*end if*/
    else
    {
      OSAL_s32ThreadWait((10));  /*ms*/
    }/*end else*/
  }while((VTcb.enStatus != OSAL_EN_THREAD_READY)); 
  if(OSAL_s32ThreadDelete(PpWorkerId)== OSAL_ERROR)
  {
    //return FALSE;
  }/*end if*/
  return(TRUE);
}/*end fucntion*/

#if OEDT_SPI1_KERNEL_MODE
/*Chip Select to output mode*/
tU32 CS_to_Output( OSAL_tIODescriptor *hDeviceGPIO )
{

  /*Pin descriptor*/
  OSAL_tGPIODevID idADRChipSelectGPIO;

  idADRChipSelectGPIO = DTT_GET_SPI_CS_ID_FROM_DEVICEID(
                                                       DTT_getDeviceId( DTT_EMBEDDEDRADIO_SPI_ADR_SPI1_CS )
                                                       );
  /*Try Setting GPIO 
  DTT_EMBEDDEDRADIO_SPI_ADR_SPI1_CS to output mode*/
  if(OSAL_s32IOControl(
                      *hDeviceGPIO,
                      OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,
                      (tS32)idADRChipSelectGPIO 
                      ) == OSAL_ERROR 
    )
    return 1;
  else
    return 0;
}

/*Reset to output mode*/
tU32 Reset_to_Output( OSAL_tIODescriptor *hDeviceGPIO )
{

  /*Pin descriptor*/
  OSAL_tGPIODevID idADRResetGPIO;

  idADRResetGPIO = DTT_GET_SPI_CS_ID_FROM_DEVICEID(
                                                  DTT_getDeviceId( DTT_EMBEDDEDRADIO_GPIO_ADR_RESET )
                                                  );
  /*Try Setting GPIO 
  DTT_EMBEDDEDRADIO_GPIO_ADR_RESET to output mode 
  Reset pin of the ADR*/
  if(OSAL_s32IOControl(
                      *hDeviceGPIO,
                      OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT,
                      (tS32)idADRResetGPIO
                      ) == OSAL_ERROR 
    )
    return 1;
  else
    return 0;

}

/*Request to input mode*/
tU32 Request_to_Input( OSAL_tIODescriptor *hDeviceGPIO )
{
  /*Pin descriptor*/
  OSAL_tGPIODevID idADRRequestGPIO;

  idADRRequestGPIO = DTT_GET_SPI_CS_ID_FROM_DEVICEID(
                                                    DTT_getDeviceId( DTT_EMBEDDEDRADIO_GPIO_ADR_REQ )
                                                    ); 
  /*Try Setting GPIO 
  DTT_EMBEDDEDRADIO_GPIO_ADR_REQUEST to input mode 
  Request pin of the ADR*/
  if(OSAL_s32IOControl(
                      *hDeviceGPIO,
                      OSAL_C_32_IOCTRL_GPIO_SET_INPUT,
                      (tS32)idADRRequestGPIO
                      ) == OSAL_ERROR 
    )
    return 1;
  else
    return 0;
}
#endif

/*GPIO Device Creation*/
tU32 GPIO_DevCreate (void)
{ 
  if(OSAL_IOCreate(
                  OSAL_C_STRING_DEVICE_GPIO, 
                  OSAL_EN_READWRITE ) != OSAL_ERROR)
    return 0;
  else
    return 1;
}

/*GPIO Device Removal*/
tU32 GPIO_DevRemove(void)
{
  if(OSAL_s32IORemove(OSAL_C_STRING_DEVICE_GPIO) != OSAL_ERROR)
    return 0;
  else
    return 1;
}

/******************************************************************************
* FUNCTION:     u32SPI1_RadioUnit_ADR_OpenDevice() 
* DESCRIPTION:  Opens the device.
* PARAMETER:    None.            
* TEST CASE:    TU_OEDT_SPI1_001.
* RETURNVALUE:  0-Success/>0 on Failure.
* HISTORY:      Created by Tinoy Mathews(RBIN/ECM1). 
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_OpenDevice(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );

  /*Check if opening has been successful*/
  if(hDevice == OSAL_ERROR)
  {
    return 1;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      return 2;       
    }
  }
  return 0;
} 

/*****************************************************************************
* FUNCTION:	    u32SPI1_RadioUnit_ADR_OpenDevInvalParam( )
* PARAMETER:    none.
* RETURNVALUE:  0-Success/>0 on Failure.
* TEST CASE:    TU_OEDT_SPI1_002.
* DESCRIPTION:  Device open with invalid parameter/handle (should fail).
* HISTORY:		Created by Tinoy Mathews(RBIN/ECM1).  
        Updated by Tinoy Mathews(RBIN/EDI3) on Sept 25th 2007
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_OpenDevInvalParam(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = (OSAL_tenAccess)OEDT_SPI1_INVAL_PARAM;
  tU32 u32Ret = 0;

  /*Open the SPI1 device*/    
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );

  /*Check if opening succeeds*/
  if(hDevice != OSAL_ERROR)
  {
    u32Ret = 1;

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 2;
    }
  }
  return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32SPI1_RadioUnit_ADR_ReOpenDev()
* PARAMETER:    none.
* RETURNVALUE:  0-Success/>0 on Failure.
* TEST CASE:    TU_OEDT_SPI1_003.
* DESCRIPTION:  Try to open the device which is already opened.
* HISTORY:		Created by Tinoy Mathews(RBIN/ECM1).
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_ReOpenDev(void)
{
  OSAL_tIODescriptor hDevice1 = 0,hDevice2 = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  /*Open the SPI1 device*/     
  hDevice1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );

  /*Check if opening has been successful*/
  if(hDevice1 == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Reopen the SPI1 device*/
    hDevice2 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );

    /*Check if device is reopened*/
    if(hDevice2 != OSAL_ERROR)
    {
      u32Ret = 2;

      /*Restore SPI1 device state*/
      if(OSAL_s32IOClose ( hDevice2 ) == OSAL_ERROR)
      {
        u32Ret += 10;
      }
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice1 )== OSAL_ERROR)
    {
      u32Ret += 100;
    }
  }
  return u32Ret;

}

/*****************************************************************************
* FUNCTION:		u32SPI1_RadioUnit_ADR_OpenCloseDevDiffModes()
* PARAMETER:    none.
* RETURNVALUE:  0-Success/>0 on Failure.
* TEST CASE:    TU_OEDT_SPI1_004.
* DESCRIPTION:  Try to open the device in different access modes
*				(Read only,Write only,Read write).
* HISTORY:		Created by Tinoy Mathews(RBIN/ECM1).   
        Updated by Tinoy Mathews(RBIN/EDI3) on Sept 25, 2007 
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_OpenCloseDevDiffModes(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = (OSAL_tenAccess)OEDT_SPI1_INIT_VAL;
  tU32 u32Ret = 0;

  /* In read only mode */
  enAccess = OSAL_EN_READONLY;

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
  }


  /* In read write mode */
  enAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret += 10;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 20;
    }
  }


  /* In write only mode */
  enAccess = OSAL_EN_WRITEONLY;

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret += 100;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 200;
    }
  }

  return u32Ret;
}

/*****************************************************************************
* FUNCTION:		u32SPI1_RadioUnit_ADR_CloseDevice( )
* PARAMETER:    none.
* RETURNVALUE:  0-Success/>0 on Failure.
* TEST CASE:    TU_OEDT_SPI1_005.
* DESCRIPTION:  Close the SPI device.
* HISTORY:		Created by Tinoy Mathews. 
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_CloseDevice(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/  
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    return 1;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      return 2;
    }
  }
  return 0;
}

/******************************************************************************
* FUNCTION:		u32SPI1_RadioUnit_ADR_CloseDevInvalParam()
* PARAMETER:    none.
* RETURNVALUE:  0-Success/>0 on Failure.
* TEST CASE:    TU_OEDT_SPI1_006.
* DESCRIPTION:  Close the device with invalid parameters (should fail).  
* HISTORY:		Created by Tinoy Mathews.  
*******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_CloseDevInvalParam(void)
{
  OSAL_tIODescriptor hDevice_inval = -1;

  /*Restore SPI1 device state*/
  if(OSAL_s32IOClose ( hDevice_inval ) != OSAL_ERROR)
  {
    return 1;
  }
  return 0;
}

/******************************************************************************
* FUNCTION:	    u32SPI1_RadioUnit_ADR_CloseDevAlreadyClosed().
* PARAMETER:    none.
* RETURNVALUE:  0-Success/>0 on Failure.
* TEST CASE:    TU_OEDT_SPI1_007.
* DESCRIPTION:  Try to close an already closed device.
* HISTORY:		Created by Tinoy Mathews.
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_CloseDevAlreadyClosed(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/     
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    return 1;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      return 2;
    }
    else
    {
      /*Reclose the already closed device*/
      if(OSAL_s32IOClose ( hDevice ) != OSAL_ERROR)
      {
        return 3;
      }
    }

  }
  return 0;

}

/******************************************************************************
*FUNCTION:    u32SPI1_RadioUnit_ADR_GetVersion()    
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_008.
*DESCRIPTION: Gets the SPI1 device's version number.   
*HISTORY:     Created by Tinoy Mathews.
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_GetVersion(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tChar szVersionBuffer [5]= {0}; 

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Get Version information*/
    if(OSAL_s32IOControl (
                         hDevice,
                         OSAL_C_S32_IOCTRL_VERSION,
                         (tS32)szVersionBuffer
                         ) == OSAL_ERROR
      )
    {
      u32Ret = 2;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}
#if 0	/*The IOCtls used for Testing are removed*/
/******************************************************************************	
*FUNCTION:    u32SPI1_RadioUnit_ADR_LockUnlockForTunerICs()
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_009.
*DESCRIPTION: Lock for TunerICs and later Unlock for Tuner ICs
*HISTORY:     Created by Tinoy Mathews.
        Updated by Tinoy Mathews(RBIN/EDI3) on Sept 25, 2007
******************************************************************************/
tU32  u32SPI1_RadioUnit_ADR_LockUnlockForTunerICs(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tChar Buffer [10]= {0}; 

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Lock for tuner ICs*/
    if(
      OSAL_s32IOControl (
                        hDevice,
                        OSAL_C_S32_IOCTRL_SPI_LOCK_FOR_TUNERICS,
                        (tS32)Buffer
                        ) == OSAL_ERROR
      )
    {
      u32Ret = 2;
    }
    /*Unlock for tuner ICs*/
    if(
      OSAL_s32IOControl (
                        hDevice,
                        OSAL_C_S32_IOCTRL_SPI_UNLOCK_FOR_TUNERICS,
                        (tS32)Buffer
                        ) == OSAL_ERROR
      )
    {
      u32Ret = 10;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
} 

/******************************************************************************	
*FUNCTION:    u32SPI1_RadioUnit_ADR_UnlockForTunerICsInval( )    
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_010.
*DESCRIPTION: Unlock for TunerICs.
*HISTORY:     Created by Tinoy Mathews.
******************************************************************************/
tU32  u32SPI1_RadioUnit_ADR_UnlockForTunerICsInval(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tChar Buffer [10]= {0}; 

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Unlock for tuner ICs*/
    if(
      OSAL_s32IOControl (
                        hDevice,
                        OSAL_C_S32_IOCTRL_SPI_UNLOCK_FOR_TUNERICS,
                        (tS32)Buffer
                        )!= OSAL_ERROR
      )
    {
      u32Ret = 2;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
} 
#endif
#if 0	  //case commented as the config data not updated
/******************************************************************************	
*FUNCTION:    u32SPI1_RadioUnit_ADR_TranceiveTunerIC()
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_011.
*DESCRIPTION: Send data e.g. to the DCR and
              collect a response which occurs on the MISO line.
*HISTORY:     Created by Tinoy Mathews.
*             Modified by Shilpa Bhat (RBEI/ECM1) on 18 June, 2008
******************************************************************************/
tU32  u32SPI1_RadioUnit_ADR_TranceiveTunerIC(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trSpiTransceive_TunerICs oedt_rSPI_if_trTransceiveTunerIC;

  tU8 u8TxBuffer[2];
  tU8 u8RxBuffer[2];
  tU8 u8byte1 = 2;
  tU8 u8byte2 = 2;

  oedt_rSPI_if_trTransceiveTunerIC.pu8RxData = u8RxBuffer;
  oedt_rSPI_if_trTransceiveTunerIC.pu8TxData = u8TxBuffer;
  *(oedt_rSPI_if_trTransceiveTunerIC.pu8TxData + 0) = u8byte1;
  *(oedt_rSPI_if_trTransceiveTunerIC.pu8TxData + 1) = u8byte2;
  oedt_rSPI_if_trTransceiveTunerIC.u32Length = 2;
  oedt_rSPI_if_trTransceiveTunerIC.bDriverUsesChipSelect = FALSE; 

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_DAB, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Transceive tuner ICs*/
    if(OSAL_s32IOControl (
                         hDevice,
                         OSAL_C_S32_IOCTRL_SPI_TRANSCEIVE_TUNERIC,
                         (tS32)&oedt_rSPI_if_trTransceiveTunerIC
                         ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 4;
    }
  }

  return u32Ret;
}
#endif

#if 0
/*Not Implemented in driver*/
/******************************************************************************
*FUNCTION:    u32SPI1_RadioUnit_ADR_GetPayloadWritebufferMaxSize()   
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_012.
*DESCRIPTION: Get max size of buffer which could be used in context of 
              OSAL_IOWrite command. 
*HISTORY:     Created by Tinoy Mathews.
******************************************************************************/
tU32  u32SPI1_RadioUnit_ADR_GetPayloadWritebufferMaxSize(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tChar MaxSzBuffer [10] = {0};

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Get payloadwritebuffer maximum size*/
    if(
      OSAL_s32IOControl (
                        hDevice, 
                        OSAL_C_S32_IOCTRL_DRVSPI_GET_PAYLOADWRITEBUFFER_MAXSIZE,
                        (tS32)MaxSzBuffer
                        ) == OSAL_ERROR
      )
    {
      u32Ret = 2;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}  

/******************************************************************************	
*FUNCTION:    u32SPI1_RadioUnit_ADR_GetPayloadReadbufferMaxSize()         
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_013.
*DESCRIPTION: Get max size of buffer which could be used in context of 
              OSAL_IORead command. 
*HISTORY:     Created by Tinoy Mathews
******************************************************************************/
tU32  u32SPI1_RadioUnit_ADR_GetPayloadReadbufferMaxSize(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tChar MaxSzBuffer [10] = {0};

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Get payloadreadbuffer maximum size*/
    if(
      OSAL_s32IOControl (
                        hDevice,  
                        OSAL_C_S32_IOCTRL_DRVSPI_GET_PAYLOADREADBUFFER_MAXSIZE,
                        (tS32)MaxSzBuffer
                        ) == OSAL_ERROR
      )
    {
      u32Ret = 2;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}

/******************************************************************************	
*FUNCTION:    u32SPI1_RadioUnit_ADR_SwitchModeGetEventMaskAndHandle()
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_014.
*DESCRIPTION: Switch mode of driver to call back event mode and get event mask
              and event handle.
*HISTORY:     Created by Tinoy Mathews.
*******************************************************************************/
tU32  u32SPI1_RadioUnit_ADR_SwitchModeGetEventMaskAndHandle(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tChar EvntMskHdleBuffer [10] = {0};

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Switch mode of driver to call back event mode and get event mask
      and event handle*/
    if(
      OSAL_s32IOControl (
                        hDevice,    
                        OSAL_C_S32_IOCTRL_DRVSPI_MODE_CALLBACKEVENT_GET_EVENTMASKANDHANDLE,
                        (tS32)EvntMskHdleBuffer
                        ) == OSAL_ERROR
      )
    {
      u32Ret = 2;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}  

/******************************************************************************	
*FUNCTION:    u32SPI1_RadioUnit_ADR_ModePlainRead()    
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_015.
*DESCRIPTION: Blocking mode.
*HISTORY:     Created by Tinoy Mathews.
******************************************************************************/
tU32  u32SPI1_RadioUnit_ADR_ModePlainRead(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tChar Buffer [10]= {0};

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Blocking mode*/
    if(
      OSAL_s32IOControl (
                        hDevice,  
                        OSAL_C_S32_IOCTRL_DRVSPI_MODE_PLAIN_READ,         
                        (tS32)Buffer
                        ) == OSAL_ERROR
      )
    {
      u32Ret = 2;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
} 
#endif/*Not implemented in the driver*/

/******************************************************************************	
*FUNCTION:    u32SPI1_RadioUnit_ADR_SetAdvancedMode()    
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_016.
*DESCRIPTION: Set Advanced Mode.
*HISTORY:     Created by Tinoy Mathews.
******************************************************************************/
tU32  u32SPI1_RadioUnit_ADR_SetAdvancedMode(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tChar Buffer [10]= {0}; 

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Set Advanced mode*/
    if(
      OSAL_s32IOControl (
                        hDevice,
                        OSAL_C_S32_IOCTRL_SPI_SET_ADVANCED_MODE,
                        (tS32)Buffer
                        ) == OSAL_ERROR
      )
    {
      u32Ret = 2;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 10;
    }
  }
  return u32Ret;
}  

/******************************************************************************
* FUNCTION:    u32SPI1_RadioUnit_ADR_Read_InvalSize()
* PARAMETER:   none.
* RETURNVALUE: 0-Success/>0 on Failure.
* TEST CASE:   TU_OEDT_SPI1_017.           
* DESCRIPTION: Try reading synchronously 0/<0 bytes(should fail). (Asynchronous read is not supported, refer mms 125833-Kishore).
* HISTORY:     Created by Tinoy Mathews.
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_Read_InvalSize(void) 
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trSpiRxDataBuffer rRxBuffer;

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Read synchronously zero bytes*/
    if(OSAL_s32IORead (
                      hDevice,
                      (tPS8)&rRxBuffer,
                      (tU32)ZERO_SIZE) != OSAL_ERROR)
    {
      u32Ret = 10; 
    }


    /*Read synchronously negative bytes*/
    /* This section of code has been commented based on MMS reply 128977.*/
    /*  if(OSAL_s32IORead (
                 hDevice,
                         (tPS8)&rRxBuffer,
                         (tU32)INVAL_SIZE) != OSAL_ERROR)
      {
          u32Ret += 100; 
      }
      */


    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret += 1000;
    }
  }
  return u32Ret;
}

/******************************************************************************	
*FUNCTION:    u32SPI1_RadioUnit_ADR_Read_InvalDev()    
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_018.
*DESCRIPTION: Try reading synchronously without opening the device (Asynchronous read is not supported, refer mms 125833-Kishore).

*HISTORY:     Created by Tinoy Mathews.
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_Read_InvalDev(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  OSAL_trSpiRxDataBuffer rRxBuffer;    

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {
      /*Read synchronously from an invalid device*/
      if(OSAL_s32IORead (
                        hDevice,
                        (tPS8)&rRxBuffer,
                        sizeof(rRxBuffer)) != OSAL_ERROR)
      {
        u32Ret = 3;
      }
    }
  }
  return u32Ret;

}
/******************************************************************************
*FUNCTION:    u32SPI1_RadioUnit_ADR_Write_InvalSize()      
*PARAMETER:   none.
* RETURNVALUE: 0-Success/>0 on Failure.
* TEST CASE:   TU_OEDT_SPI1_019.    
*DESCRIPTION: Synchronously writes invalid number of bytes of 
              data to the device. 	(Asynchronous write  is not supported, refer mms 125832-Kishore).
        
*HISTORY:     Created by Tinoy Mathews(RBIN/ECM1).              
              Modified by Kishore Kumar.R             
        Updated by Tinoy Mathews(RBIN/EDI3) on Sept 25, 2007
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_Write_InvalSize(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trSpiTxDataBuffer rTxBuffer;
  tChar DataToWrite[] = {10,11};
  rTxBuffer.u8BufferLength = 128;

  /*Reserving memory block in heap memory*/
  rTxBuffer.pu8DataBuffer = 
  (tU8 *) OSAL_pvMemoryAllocate ( rTxBuffer.u8BufferLength +1);
  if(rTxBuffer.pu8DataBuffer == OSAL_NULL)
  {
    u32Ret = 1; 
  }

  /*Strcpy*/
  (tVoid)OSAL_szStringCopy(  rTxBuffer.pu8DataBuffer,DataToWrite );


  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 3;
  }
  else
  {

    /*Try writing into buffer synchronously of an invalid size*/
    if(OSAL_s32IOWrite (
                       hDevice,
                       (tPS8)&rTxBuffer,
                       (tU32)INVAL_SIZE) != OSAL_ERROR)
    {
      u32Ret = 10;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret += 100;
    }

  }

  if(rTxBuffer.pu8DataBuffer != OSAL_NULL)
  {
    /*Release memory block from heap*/
    OSAL_vMemoryFree ( rTxBuffer.pu8DataBuffer );
  }
  return u32Ret;
}

/******************************************************************************
*FUNCTION:    u32SPI1_RadioUnit_ADR_Write_InvalDev()      
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_020. 
*DESCRIPTION: Synchronously writes the specified number of bytes of 
              given data to an non existent device.(Asynchronous write is not supported, refer mms 125832-Kishore).
 
*HISTORY:     Created by Tinoy Mathews(RBIN/ECM1).              
         Modified by Kishore Kumar.R               
         Updated by Tinoy Mathews(RBIN/EDI3) on Sept 25, 2007              
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_Write_InvalDev(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trSpiTxDataBuffer rTxBuffer;
  tS8 DataToWrite[] = {10,06};
  rTxBuffer.u8BufferLength = 2;

  /*Reserving memory block in heap memory*/
  rTxBuffer.pu8DataBuffer = 
  (tU8 *) OSAL_pvMemoryAllocate ( rTxBuffer.u8BufferLength +1 );
  if(rTxBuffer.pu8DataBuffer == OSAL_NULL)
  {
    u32Ret = 1;
  }

  /*Strcpy*/
  (tVoid)OSAL_szStringCopy(  rTxBuffer.pu8DataBuffer,DataToWrite );


  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 3;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret = 10;
    }
    else
    {
      /*Write synchronously to invalid device i.e after closing the device*/
      if(OSAL_s32IOWrite (
                         hDevice,
                         (tPS8)&rTxBuffer,
                         sizeof(rTxBuffer)) != OSAL_ERROR)
      {
        u32Ret = 100;
      }

    }

  }

  if(rTxBuffer.pu8DataBuffer != OSAL_NULL)
  {
    /*Release memory block from heap*/
    OSAL_vMemoryFree ( rTxBuffer.pu8DataBuffer );
  }
  return u32Ret;
}   


/******************************************************************************
*FUNCTION:     u32SPI1_RadioUnit_ADR_Write()
*PARAMETER:    None.
*RETURNVALUE:  0-Success/>0 on Failure.
*TEST CASE:    TU_OEDT_SPI1_021.  
*DESCRIPTION:  Write to SPI device synchronously. 
*HISTORY:      Created by Tinoy Mathews.
               Modified by Kishore Kumar.R  
        Updated By Rakesh Dhanya on 7 feb, 2007. 
        Updated By Tinoy Mathews(RBIN/EDI3) on May 10,2007
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_Write(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tU32 BytesWritten = 0;
  tU8 DataToWrite[] = { 0x01 , 0x06 , 0x12, 0x32, 0x44, 0x66} ;  //6 bytes

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 3;
  }
  else
  {
    /*Write synchronously to device*/
    BytesWritten = (tU32)OSAL_s32IOWrite (
                                         hDevice,
                                         (tPS8)DataToWrite,
                                         sizeof(DataToWrite));
    if(BytesWritten != sizeof(DataToWrite))
    {
      u32Ret = 10;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret = 100;
    }
  }

  return u32Ret;
} 

/******************************************************************************
*FUNCTION:     u32SPI1_RadioUnit_ADR_Read()
*PARAMETER:    None.
*RETURNVALUE:  0-Success/>0 on Failure.
*TEST CASE:    TU_OEDT_SPI1_022.  
*DESCRIPTION:  Read from SPI device synchronously. (Asynchronous read is not supported, refer mms 125833-Kishore).

*HISTORY:      Created by Tinoy Mathews.
               Updated By Rakesh Dhanya on 7 feb, 2007.
         Updated By Tinoy Mathews(RBIN/EDI3) on May 10,2007
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_Read(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tU32 BytesToRead = 6;
  tU32 BytesRead;    
  tU8 msgBuff[256];    


  /*Initialize the buffer*/
  memset(msgBuff,0x00,sizeof( msgBuff ) );

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess ); 
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Read synchronously*/
    BytesRead =  (tU32)OSAL_s32IORead (
                                      hDevice,
                                      (tPS8)msgBuff,
                                      BytesToRead); 
    if(BytesRead != (BytesToRead+4))  /* 4 byte header is read along with the data in an async read */
    {
      u32Ret = 3;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret = 100;
    }
  }
  return u32Ret;
}

/******************************************************************************
*FUNCTION:     u32SPI1_RadioUnit_ADR_WriteGreat()
*PARAMETER:    None.
*RETURNVALUE:  0-Success/>0 on Failure.
*TEST CASE:    TU_OEDT_SPI1_023.  
*DESCRIPTION:  Write more than 256 bytes to SPI device synchronously. (According to MMS TICKET - 125832,
                Synchronous write of more than 256 bytes is possible.)
*HISTORY:      Created by Kishore Kumar.R
               Updated by Kishore Kumar.R on 12.03.07
               Updated by Anoop Chandran on 27.01.09

******************************************************************************/

tU32 u32SPI1_RadioUnit_ADR_WriteGreat(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trSpiTxDataBuffer rTxBuffer;
  tU8 DataToWrite[260] ={0};    
  tU32 BufferLength = 260;       
  //tU32 loopcount=0;
  tU32 value=0;


  /*for(loopcount=0;loopcount<=259;loopcount++)
{
      DataToWrite[loopcount]= 10;
}*/
  OSAL_pvMemorySet(DataToWrite,'\0',BufferLength);
  OSAL_pvMemorySet(DataToWrite,58,BufferLength-1);



  /*Reserving memory block in heap memory*/
  rTxBuffer.pu8DataBuffer =(tU8 *) OSAL_pvMemoryAllocate  
                           (
                           BufferLength 
                           );//rTxBuffer.u8BufferLength );		//changed



  if(rTxBuffer.pu8DataBuffer == OSAL_NULL)
  {
    u32Ret = 1; 
  }


  OSAL_pvMemorySet(rTxBuffer.pu8DataBuffer,'\0',BufferLength);
  /*Strcpy*/
  (tVoid)OSAL_szStringCopy(  rTxBuffer.pu8DataBuffer,DataToWrite ); //only 256 bytes are copied.

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 3;
  }
  else
  {


    value=(tU32)OSAL_s32IOWrite
          (
          hDevice,
          (tPS8)&(rTxBuffer.pu8DataBuffer),
          BufferLength
          );//== OSAL_ERROR)

    if(value < BufferLength)
    {
      u32Ret = 10;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret = 100;
    }
  }

  if(rTxBuffer.pu8DataBuffer != OSAL_NULL)
  {
    /*Release memory block from heap*/
    OSAL_vMemoryFree ( rTxBuffer.pu8DataBuffer );
  }
  return u32Ret;
}


/******************************************************************************
*FUNCTION:     u32SPI1_RadioUnit_ADR_ReadGreat()
*PARAMETER:    None.
*RETURNVALUE:  0-Success/>0 on Failure.
*TEST CASE:    TU_OEDT_SPI1_024.  
*DESCRIPTION:  Try to read more than 256 bytes from spi device synchronously(should fail).
               ( According to MMS TICKET 125833 , Synchronous read of data larger than 256 
                will never happen, because ADR does not send packets larger than 255 bytes.) 
*HISTORY:      Created by Kishore Kumar.R
               Updated by Anoop Chandran on 27.01.09
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_ReadGreat(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  tU32 u32Read = 0;    
  OSAL_trSpiRxDataBuffer rRxBuffer;    
  tU32 BufferLength=260;  

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {



    /*Read synchronously*/
    if((u32Read = (tU32)OSAL_s32IORead(hDevice,
                                       (tPS8)(rRxBuffer.pu8DataBuffer),
                                       BufferLength))== (tU32)OSAL_ERROR)      //try to read more than  256 bytes
    {
      u32Ret = 3;
    }
    /*check read return more than 255*/
    if(u32Read > 255)
    {
      u32Ret += 50;
    }
    else
    {
      /*Does nothing*/
    }


    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret = 100;
    }

  }
  return u32Ret;
}

#if 0 

/*The following test cases TU_OEDT_SPI1_026- 029 correspond to read and write
  operations.( makes use of the macro OSAL_C_S32_IOCTRL_SPI_SPI_SYNC_START, 
  which is defined in osioctrl.h).This is not yet implemented in driver code.
  The default mode of read and write operations is SYNCHRONOUS according to
  MMS TICKET NO- 125833, 125822. ASYNCHRONOUS read and write are not supported.)
  The test cases no-  TU_OEDT_SPI1_026 ,TU_OEDT_SPI1_027,TU_OEDT_SPI1_028,
  TU_OEDT_SPI1_029 have to be removed. As per Mr.Koechling Christian (CM-DI/PJ-GM35) 
  (ref MMS Ticketno - 132705).But it is not deleted as test case TAG's are unique 
  and these test case tags are used for test process rollout. */

/******************************************************************************
* FUNCTION:    u32SPI1_RadioUnit_ADR_ReadSync_InvalDev()
* PARAMETER:   None 
* RETURNVALUE: 0-Success/>0 on Failure.
* TEST CASE:   TU_OEDT_SPI1_025.    
* DESCRIPTION: Synchronously reads the specified number of bytes. 
* HISTORY:     Created by Tinoy Mathews	  
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_ReadSync_InvalDev(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trIOCtrlSpiTransferData rTransferBuffer;

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess ); 
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose ( hDevice ) == OSAL_ERROR)
    {
      u32Ret = 2;
    }
    else
    {

      if(OSAL_s32IOControl(
                          hDevice,
                          OSAL_C_S32_IOCTRL_SPI_SPI_SYNC_START,   // not present in driver code // kishore
                          (tS32)&rTransferBuffer
                          ) != OSAL_ERROR)
      {
        u32Ret = 3; 
      }
    }
  }
  return u32Ret;
} 


/******************************************************************************
*FUNCTION:     u32SPI1_RadioUnit_ADR_WriteSync_InvalDev()
*PARAMETER:    None
*RETURNVALUE:  0-Success/>0 on Failure.
*TEST CASE:    TU_OEDT_SPI1_026.  
*DESCRIPTION:  Synchronously writes the specified number of bytes
*              to an non existent device 
*HISTORY:      Created by Tinoy Mathews
               Modified by Kishore Kumar.R  	  
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_WriteSync_InvalDev(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trIOCtrlSpiTransferData rTransferBuffer;

  rTransferBuffer.rTxData.u8BufferLength = 128;

  /*Reserving memory block in the heap*/
  rTransferBuffer.rTxData.pu8DataBuffer = 
  (tU8 *) OSAL_pvMemoryAllocate ( rTransferBuffer.rTxData.u8BufferLength );

  if(rTransferBuffer.rTxData.pu8DataBuffer == OSAL_NULL)
  {
    return 1;
  }

  /*Strcpy*/
  if(OSAL_szStringNCopy(
                       rTransferBuffer.rTxData.pu8DataBuffer,
                       as8SPI1MesgToWrite,
                       rTransferBuffer.rTxData.u8BufferLength
                       ) == OSAL_ERROR)
  {
    return 2;
  }

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 10;
  }
  else
  {
    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret = 100;
    }
    else
    {
      /*Call the control function after closing the device*/
      if(OSAL_s32IOControl(
                          hDevice,
                          OSAL_C_S32_IOCTRL_SPI_SPI_SYNC_START,
                          (tS32)&rTransferBuffer
                          )!= OSAL_ERROR)
      {
        u32Ret = 1000;
      }

    }
  }

  if(rTransferBuffer.rTxData.pu8DataBuffer != OSAL_NULL)
  {
    /*Release memory block from heap*/
    OSAL_vMemoryFree ( rTransferBuffer.rTxData.pu8DataBuffer );
  }
  return u32Ret;
}   



/******************************************************************************
*FUNCTION:     u32SPI1_RadioUnit_ADR_ReadSync()
*PARAMETER:    None.
*RETURNVALUE:  0-Success/>0 on Failure.
*TEST CASE:    TU_OEDT_SPI1_027.  
*DESCRIPTION:  Read from SPI device synchronously. 
*HISTORY:      Created by Tinoy Mathews.
         Modified  by Kishore Kumar.R    
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_ReadSync(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trIOCtrlSpiTransferData rTransferBuffer;

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess ); 
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 1;
  }
  else
  {
    /*Read synchronously*/
    if(OSAL_s32IOControl(
                        hDevice,
                        OSAL_C_S32_IOCTRL_SPI_SPI_SYNC_START,
                        (tS32)&(rTransferBuffer.rRxData) ) == OSAL_ERROR)     // change			                  	 		   	{
    {
      u32Ret = 2; 
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret = 100;
    }

  }
  return u32Ret;
}

/******************************************************************************
*FUNCTION:     u32SPI1_RadioUnit_ADR_WriteSync()
*PARAMETER:    None.
*RETURNVALUE:  0-Success/>0 on Failure.
*TEST CASE:    TU_OEDT_SPI1_028.  
*DESCRIPTION:  Write to SPI device synchronously. 
*HISTORY:      Created by Tinoy Mathews.
         Modified by Kishore Kumar.R 
******************************************************************************/
tU32 u32SPI1_RadioUnit_ADR_WriteSync(void)
{
  OSAL_tIODescriptor hDevice = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;
  OSAL_trIOCtrlSpiTransferData rTransferBuffer;

  rTransferBuffer.rTxData.u8BufferLength = 128 ;      //changed

  /*Reserving memory block in the heap*/
  rTransferBuffer.rTxData.pu8DataBuffer = 
  (tU8 *) OSAL_pvMemoryAllocate ( rTransferBuffer.rTxData.u8BufferLength );

  if(rTransferBuffer.rTxData.pu8DataBuffer == OSAL_NULL)
  {
    return 1;
  }

  /*Strcpy*/
  if(OSAL_szStringNCopy(
                       rTransferBuffer.rTxData.pu8DataBuffer,
                       as8SPI1MesgToWrite,
                       rTransferBuffer.rTxData.u8BufferLength
                       ) == OSAL_ERROR)
  {
    return 2;
  }

  /*Open the SPI1 device*/
  hDevice = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR, enAccess );
  if(hDevice == OSAL_ERROR)
  {
    u32Ret = 10;
  }
  else
  {
    /*Call the control function*/
    if(OSAL_s32IOControl(
                        hDevice,
                        OSAL_C_S32_IOCTRL_SPI_SPI_SYNC_START,
                        (tS32)&rTransferBuffer
                        ) == OSAL_ERROR)
    {
      u32Ret = 1000;
    }

    /*Restore SPI1 device state*/
    if(OSAL_s32IOClose(hDevice) == OSAL_ERROR)
    {
      u32Ret = 100;
    }
  }


  if(rTransferBuffer.rTxData.pu8DataBuffer != OSAL_NULL)
  {
    /*Release memory block from heap*/
    OSAL_vMemoryFree ( rTransferBuffer.rTxData.pu8DataBuffer );
  }
  return u32Ret;
}   

#endif	//End of commented code


#if OEDT_SPI1_KERNEL_MODE
/******************************************************************************
*FUNCTION:    u32_Set_CS_Reset_Request_to_IO_mode_ADR()
*PARAMETER:   none.
*RETURNVALUE: 0-Success/>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_29. 
*DESCRIPTION: Set Chip Select pin of ADR and Reset pin to output mode.
        and request pin of ADR to input. 
*HISTORY:     Created by Tinoy Mathews(RBIN/ECM1).              
******************************************************************************/
tU32 u32_Set_CS_Reset_Request_to_IO_mode_ADR( tVoid )
{
  OSAL_tIODescriptor hDeviceGPIO = 0;
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  tU32 u32Ret = 0;

  /*Create the GPIO Device*/
  if(GPIO_DevCreate())
    return 1;

  /*Open the GPIO device*/
  hDeviceGPIO = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPIO,enAccess );

  /*Check if GPIO device opened successfully*/
  if(hDeviceGPIO == OSAL_ERROR)
    return 2;

  /*Chip select to output*/
  if(CS_to_Output( &hDeviceGPIO ))
    u32Ret = 10;

  /*Reset to output*/
  if(Reset_to_Output( &hDeviceGPIO ))
    u32Ret += 100;

  /*Request to input*/
  if(Request_to_Input( &hDeviceGPIO ))
    u32Ret += 1000;

  /*Restore device state*/
  if(OSAL_s32IOClose( hDeviceGPIO ) == OSAL_ERROR)
    u32Ret += 10000;

  return u32Ret;
}   

/******************************************************************************
*FUNCTION:    u32Open_Connection2ADR()
*PARAMETER:   none.
*RETURNVALUE: 0-Success/<>0 on Failure.
*TEST CASE:   TU_OEDT_SPI1_030. 
*DESCRIPTION: Open connection with ADR , read and write data. 
*HISTORY:     Created by Tinoy Mathews(RBIN/ECM1).              
******************************************************************************/
tU32 u32Open_Connection2ADR(tVoid)
{
  /*Function return value*/
  tU32 u32Ret = 0;
  /*Buffer for OSAL_s32IOControl and OSAL_IORead and OSAL_IOWrite*/
  tChar Buffer [10] = {0};
  tU8  u8DataBuffer_ADRMsg[256];
  /*Device descriptors*/
  OSAL_tIODescriptor hDeviceSPI1 = 0;
  OSAL_tIODescriptor hDeviceGPIO = 0;
  /*Access modes*/
  OSAL_tenAccess enAccess = OSAL_EN_READWRITE;
  /*Pin information*/
  OSAL_trGPIOData pinData;
  /*Callback*/
  OSAL_trGPIOCallbackData tCBData;
  /*Device ID*/
  OSAL_tGPIODevID idADRRequestGPIO;
  tU32 dtt_adr_resetGPIO;
  /*Return value after a read from ADR or write to ADR*/
  tS32 s32RetVal;


  /*Initialize the buffer*/
  memset( &u8DataBuffer_ADRMsg,0x00,sizeof( u8DataBuffer_ADRMsg ) );


  /*Create the GPIO Device*/
  if(GPIO_DevCreate())
    return -4;

  /*Open the GPIO device*/
  hDeviceGPIO = OSAL_IOOpen( OSAL_C_STRING_DEVICE_GPIO,enAccess );
  if(hDeviceGPIO == OSAL_ERROR)
  {
    return -3;
  }

  /*Open the SPI1 device*/
  hDeviceSPI1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR,enAccess );
  if(hDeviceSPI1 == OSAL_ERROR)
  {
    if(OSAL_s32IOClose( hDeviceGPIO ) == OSAL_ERROR)
      return -2;
    else
      return -1;
  }

  /*Set SPI1 to advanced mode*/
  if(OSAL_s32IOControl (
                       hDeviceSPI1,
                       OSAL_C_S32_IOCTRL_SPI_SET_ADVANCED_MODE,
                       (tS32)Buffer
                       ) == OSAL_ERROR 
    )
  {
    u32Ret = 1;  
  }

  /*Chip Select pin of the ADR,pin set to output is set to default low*/
  if(CS_to_Output( &hDeviceGPIO ))
  {
    u32Ret += 2;
  }


  /*Reset pin of ADR set to output*/
  if(Reset_to_Output( &hDeviceGPIO ))
  {
    u32Ret += 3;    
  }

  /*Create Event*/
  if(bCreEvent( &tHandleEvent_ADRRequestLine_GLOB, "ADR_TEST" ) == FALSE)
  {
    u32Ret += 4;    
  }

  /*Request pin of ADR set to input*/
  if(Request_to_Input( &hDeviceGPIO ))
  {
    u32Ret += 5;  
  }


  /*Request line of ADR*/
  idADRRequestGPIO = DTT_GET_SPI_CS_ID_FROM_DEVICEID(
                                                    DTT_getDeviceId( DTT_EMBEDDEDRADIO_GPIO_ADR_REQ )
                                                    );

  /*Set trigger edge low for 
  DTT_EMBEDDEDRADIO_GPIO_ADR_REQ*/
  pinData.tId = (OSAL_tGPIODevID)idADRRequestGPIO; 
  pinData.unData.u16Edge = OSAL_GPIO_EDGE_LOW;
  if(OSAL_s32IOControl(
                      hDeviceGPIO,
                      OSAL_C_32_IOCTRL_GPIO_SET_EDGE,
                      (tS32) &pinData
                      ) == OSAL_ERROR
    )
  {
    u32Ret += 6;
  }

  /*Install callback function*/
  tCBData.rData.tId = (OSAL_tGPIODevID)idADRRequestGPIO;
  tCBData.rData.unData.pfCallback = (OSAL_tpfGPIOCallback)Callback;
  tCBData.pvArg = NULL;
  if(OSAL_s32IOControl(
                      hDeviceGPIO,
                      OSAL_C_32_IOCTRL_GPIO_SET_CALLBACK,
                      (tS32) &tCBData ) == OSAL_ERROR)
  {
    u32Ret += 7;
  }

  /*Enable interrupt*/
  tCBData.rData.unData.bState = TRUE;
  if(OSAL_s32IOControl(
                      hDeviceGPIO,
                      OSAL_C_32_IOCTRL_GPIO_ENABLE_INT,
                      (tS32) &tCBData
                      ) == OSAL_ERROR
    )
  {
    u32Ret += 8;    
  }

  /*Reset line of ADR*/
  dtt_adr_resetGPIO = DTT_GET_SPI_CS_ID_FROM_DEVICEID(
                                                     DTT_getDeviceId(DTT_EMBEDDEDRADIO_GPIO_ADR_RESET));

  /*Set GPIO state to low for 
  DTT_EMBEDDEDRADIO_GPIO_ADR_RESET*/
  pinData.tId  = (OSAL_tGPIODevID)dtt_adr_resetGPIO;
  pinData.unData.bState = FALSE; /*STATE_LOW*/

  if(OSAL_s32IOControl(
                      hDeviceGPIO,
                      OSAL_C_32_IOCTRL_GPIO_SET_STATE,
                      (tS32) &pinData
                      ) == OSAL_ERROR
    )
  {
    u32Ret += 9;
  }

  /*50 milliseconds wait*/
  OSAL_s32ThreadWait(50);

  /*Set GPIO state to high for 
  DTT_EMBEDDEDRADIO_GPIO_ADR_RESET*/
  pinData.unData.bState = TRUE; /*STATE_HIGH*/
  if(OSAL_s32IOControl(
                      hDeviceGPIO,
                      OSAL_C_32_IOCTRL_GPIO_SET_STATE,
                      (tS32) &pinData
                      ) == OSAL_ERROR
    )
  {
    u32Ret += 10;
  }

  /*200 milliseconds wait*/
  OSAL_s32ThreadWait(200);

  /*Check if Callback function was called*/
  if(CallbackCheck(
                  tHandleEvent_ADRRequestLine_GLOB, 
                  tEventMask_ADRRequestLine_GLOB,
                  2000 ) == FALSE
    )
  {
    u32Ret += 11;
  }


  /*Read message from ADR which is send by it because of reset*/
  s32RetVal = OSAL_s32IORead(hDeviceSPI1, (tPS8) u8DataBuffer_ADRMsg, 256);
  if(s32RetVal == OSAL_ERROR)
  {
    u32Ret += 12;
  }


  printf("Values from ADR  because of a reset: %d\n",s32RetVal);
  printf("The values is : \n%x\n",u8DataBuffer_ADRMsg[0]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[1]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[2]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[3]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[4]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[5]);

  /*Analyse message*/
  if(
    (s32RetVal              !=     6)     ||
    (u8DataBuffer_ADRMsg[0] !=   0x00)    ||
    (u8DataBuffer_ADRMsg[1] !=   0x01)    ||
    (u8DataBuffer_ADRMsg[2] !=   0x00)    ||
    (u8DataBuffer_ADRMsg[3] !=   0x00)    ||
    (u8DataBuffer_ADRMsg[4] !=   0x30)    ||  
    (u8DataBuffer_ADRMsg[5] !=   0x00)  
    )
  {
    u32Ret += 13;
  }


  /*Initialize the buffer*/
  memset( &u8DataBuffer_ADRMsg,0x00,sizeof( u8DataBuffer_ADRMsg ) );

  /*Write 2 bytes to ADR*/
  u8DataBuffer_ADRMsg[0] = 0x10;u8DataBuffer_ADRMsg[1] = 0x06;
  s32RetVal = OSAL_s32IOWrite(hDeviceSPI1, (tPS8) u8DataBuffer_ADRMsg, 2);
  if(s32RetVal == OSAL_ERROR)
  {
    u32Ret += 14;
  }

  OSAL_s32ThreadWait(200);       //change


  /*Check if Callback function was called*/
  if(CallbackCheck(
                  tHandleEvent_ADRRequestLine_GLOB, 
                  tEventMask_ADRRequestLine_GLOB,
                  2000 ) == FALSE
    )
  {
    u32Ret += 15;
  }

  /*read message from ADR*/
  s32RetVal = OSAL_s32IORead(hDeviceSPI1,(tPS8) u8DataBuffer_ADRMsg,256);
  if(s32RetVal == OSAL_ERROR)
  {
    u32Ret += 16;
  }


  printf("Values from ADR  because of a version request: %d\n",s32RetVal);
  printf("The values is : \n%x\n",u8DataBuffer_ADRMsg[0]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[1]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[2]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[3]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[4]);
  printf("The values is : %x\n",u8DataBuffer_ADRMsg[5]);

  /*Analyse the response*/
  if(!(
      (s32RetVal              ==     10)     &&
      (u8DataBuffer_ADRMsg[0] ==   0x00)     &&
      (u8DataBuffer_ADRMsg[1] ==   0x01)     &&
      (u8DataBuffer_ADRMsg[2] ==   0x22)     &&
      (u8DataBuffer_ADRMsg[3] ==   0x03)     &&
      (u8DataBuffer_ADRMsg[4] ==   0x20)     &&
      (u8DataBuffer_ADRMsg[5] ==   0x05)     
      ))
  {
    u32Ret += 17; 
  }

  /*Close the SPI1 device*/
  if(OSAL_s32IOClose(hDeviceSPI1) == OSAL_ERROR)
  {
    if(OSAL_s32IOClose(hDeviceGPIO) == OSAL_ERROR)
      u32Ret += 18;
    else
      u32Ret += 19;

  }

  /*Open the SPI1 device*/
  hDeviceSPI1 = OSAL_IOOpen( OSAL_C_STRING_DEVICE_SPI1_ADR,enAccess );
  if(hDeviceSPI1 == OSAL_ERROR)
  {
    if(OSAL_s32IOClose(hDeviceGPIO) == OSAL_ERROR)
      u32Ret += 20;
    else
      u32Ret += 21;

  }

  /*Initialize the buffer*/
  memset(&u8DataBuffer_ADRMsg,0x00,sizeof(u8DataBuffer_ADRMsg));

  /*Feed the command into u8DataBuffer_ADRMsg buffer*/
  u8DataBuffer_ADRMsg[0] = 0x90;u8DataBuffer_ADRMsg[1] = 0x03;
  u8DataBuffer_ADRMsg[2] = 0x00;u8DataBuffer_ADRMsg[3] = 0x01;
  u8DataBuffer_ADRMsg[4] = 0x04;u8DataBuffer_ADRMsg[5] = 0x16;
  u8DataBuffer_ADRMsg[6] = 0x10;u8DataBuffer_ADRMsg[7] = 0x06;

  /*Write*/
  s32RetVal = OSAL_s32IOWrite(hDeviceSPI1, (tPS8) u8DataBuffer_ADRMsg, 8);
  if(s32RetVal == OSAL_ERROR)
  {
    u32Ret += 22;
  }

  /*Check if Callback function was called*/
  if(CallbackCheck(
                  tHandleEvent_ADRRequestLine_GLOB, 
                  tEventMask_ADRRequestLine_GLOB,
                  2000 ) == FALSE
    )
  {
    u32Ret += 23;
  }

  /*Read response from ADR*/
  s32RetVal = OSAL_s32IORead(hDeviceSPI1, (tPS8) u8DataBuffer_ADRMsg, 256);
  if(s32RetVal == OSAL_ERROR)
  {
    u32Ret += 24;
  }

  /*Close the SPI1 and GPIO devices*/
  u32Ret = u32Ret+CloseDeviceGPIOSPI1( &hDeviceGPIO,&hDeviceSPI1 );

  /*Remove the GPIO device*/
  if(GPIO_DevRemove())
    u32Ret += 99;

  return u32Ret;
}
#endif
/*----------------------------------------------------------------------------*/
/*---------------------TEST for CAP ------------------------------------------*/
/*----------------------------------------------------------------------------*/
#define CAP_GPIO_RESET_CAP_PIN  JV_IRQ_GPIO1_11
/*events*/
#define CAP_EVENT_REQUEST_OCCURED          0x0001
#define CAP_EVENT_SEND_DATA                0x0002
#define CAP_EVENT_EXIT_THREAD              0x0004

static tU32 vu32RetThread;
static tU32 vu32CntTransceive;
/*****************************************************************************
* FUNCTION:		vSpiCap_RequestCallback()
* PARAMETER:    none
* RETURNVALUE:  none
* TEST CASE:    
* DESCRIPTION:  callback for SPI CAP
* HISTORY:		Created Andrea B�ter (TMS)  26.06.2009 
******************************************************************************/
void vSpiCap_RequestCallback(void)
{
  OSAL_s32EventPost(tHandleEvent_CAPRequestLine,CAP_EVENT_REQUEST_OCCURED,OSAL_EN_EVENTMASK_OR);
}/*end function*/

/*****************************************************************************
* FUNCTION:		vSpiCap_Thread()
* PARAMETER:    none
* RETURNVALUE:  none
* TEST CASE:    
* DESCRIPTION:  thread for transcieve
* HISTORY:		Created Andrea B�ter (TMS)  29.06.2009 
******************************************************************************/
void  vSpiCap_Thread(tVoid *pvData)
{
  tS32                           Vs32Status=OSAL_E_NOERROR;
  BOOL                           VbEndLoop=TRUE;
  OSAL_tEventMask                VrResultEventMask;
  OSAL_tenAccess                 VenAccess = OSAL_EN_READWRITE;
  OSAL_tIODescriptor             VhDevice = 0;
  tS32                           Vs32ErrorCode=OSAL_ERROR;
  tU8                            Vu8Retry;
  OSAL_trSpiTransceive_TunerICs  VtrCapTranceive;
  tU8                            Vpu8TxMsg[256]; // TX buffer
  tU8                            Vpu8RxMsg[256]; // RX buffer   

  OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(pvData);
  vu32RetThread=0;
  vu32CntTransceive=0;
  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    vu32RetThread= 100;
  }
  else
  {/*set callback */
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_SPI_INSTALL_REQCALLBACK,(tS32)vSpiCap_RequestCallback)==OSAL_ERROR)
    {
      vu32RetThread= 200; 
    }
    else
    {/* while event exit thread*/
      while(VbEndLoop==TRUE)
      {/* wait for event to start read action or to exit the thread*/
        OSAL_s32EventWait(tHandleEvent_CAPRequestLine,CAP_EVENT_REQUEST_OCCURED|CAP_EVENT_EXIT_THREAD|CAP_EVENT_SEND_DATA,OSAL_EN_EVENTMASK_OR,20,&VrResultEventMask);
        Vu8Retry=0xff;
        /* if event call read*/
        if((VrResultEventMask & CAP_EVENT_REQUEST_OCCURED)==CAP_EVENT_REQUEST_OCCURED)
        { /*clear event*/
          OSAL_s32EventPost(tHandleEvent_CAPRequestLine,~(CAP_EVENT_REQUEST_OCCURED),OSAL_EN_EVENTMASK_AND);
          /* idle*/
          Vpu8TxMsg[0] = 0xff;
          Vpu8TxMsg[1] = 0xff;             
          VtrCapTranceive.u32Length = 0xff;   
          Vu8Retry=0;
        }/*end if*/
        if((VrResultEventMask & CAP_EVENT_SEND_DATA)==CAP_EVENT_SEND_DATA)
        { /*clear event*/
          OSAL_s32EventPost(tHandleEvent_CAPRequestLine,~(CAP_EVENT_SEND_DATA),OSAL_EN_EVENTMASK_AND);
          /* init cp*/
          Vpu8TxMsg[0] = 0xc1;
          Vpu8TxMsg[1] = 0xc0;             
          VtrCapTranceive.u32Length = 0x02;   
          Vu8Retry=0;         
        }/*end if*/
        if(Vu8Retry==0)
        {/*transceive data*/
          VtrCapTranceive.pu8RxData = Vpu8RxMsg;
          VtrCapTranceive.pu8TxData = Vpu8TxMsg;
          VtrCapTranceive.bDriverUsesChipSelect = TRUE; 
          /*loop until valid  transcieve*/
          while(Vu8Retry < 5)
          {
            Vs32ErrorCode=OSAL_s32IOControl(VhDevice, OSAL_C_S32_IOCTRL_SPI_TRANSCEIVE_TUNERIC,(tS32)&VtrCapTranceive);
            if(Vs32ErrorCode==OSAL_ERROR)
            {/* Error status returned */
              Vs32Status = (tS32) OSAL_u32ErrorCode();
              if(Vs32Status==(tS32) OSAL_E_IOERROR)
              { /*retry*/
                Vu8Retry++;
                /*wait time T9  = 4ms before retry */
                OSAL_s32ThreadWait(4);
              }
              else
              {/*other error break loop*/
                Vu8Retry=10;
                vu32RetThread+= 40;
              }
            }/*end if else*/
            else
            {/*break loop => OK*/
              Vu8Retry=10;
              vu32CntTransceive++;
            }
#if 0
            /*if get valid data (RX) and idle send*/
            if((VtrCapTranceive.pu8RxData[0]!=0xff)&&(VtrCapTranceive.u32Length!=0xff)&&(VtrCapTranceive.pu8TxData[0]==0xff))
            {/*break loop => OK*/
              Vu8Retry=10;
              vu32CntTransceive++;
            }
#endif
          }/*end while*/
          if((Vs32Status==(tS32)OSAL_E_IOERROR)&&(Vs32ErrorCode==(tS32)OSAL_ERROR))
          {
            vu32RetThread+= 80;
          }
        }/*end transceive*/
        /*if event call exit thread*/
        if((VrResultEventMask & CAP_EVENT_EXIT_THREAD)==CAP_EVENT_EXIT_THREAD)
        {
          VbEndLoop=FALSE;
        }/*end if*/
      }/*end while*/
    }
    /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == (tS32)OSAL_ERROR)
    {
      vu32RetThread += 800;       
    }
  }/*end else*/
  OSAL_vThreadExit();
}/*end function*/
/*****************************************************************************
* FUNCTION:		vStartCapProcessor()
* PARAMETER:    none
* RETURNVALUE:  none
* TEST CASE:    
* DESCRIPTION:  start CAP
* HISTORY:		Created Andrea B�ter (TMS)  26.06.2009 
******************************************************************************/
tS32 s32SpiCap_StartCapProcessor(OSAL_tIODescriptor* VphOsalDeviceGpio)
{
  tS32             Vs32OsalError;
  OSAL_trGPIOData  VpinData;

  *VphOsalDeviceGpio = OSAL_IOOpen(OSAL_C_STRING_DEVICE_GPIO, OSAL_EN_READWRITE);
  if(*VphOsalDeviceGpio!= OSAL_ERROR)
  {
    Vs32OsalError = OSAL_s32IOControl(*VphOsalDeviceGpio, OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32)CAP_GPIO_RESET_CAP_PIN);
    if(Vs32OsalError != (tS32)OSAL_ERROR)
    {
      VpinData.tId   = (OSAL_tGPIODevID)CAP_GPIO_RESET_CAP_PIN;
      VpinData.unData.bState = TRUE;
      Vs32OsalError=OSAL_s32IOControl(*VphOsalDeviceGpio, OSAL_C_32_IOCTRL_GPIO_SET_STATE, (tS32) &VpinData);      
    }/*end if*/
  }/*end if*/
  else Vs32OsalError=OSAL_E_IOERROR;
  return(Vs32OsalError);
}

/*****************************************************************************
* FUNCTION:		vStopCapProcessor()
* PARAMETER:    none
* RETURNVALUE:  none
* TEST CASE:    
* DESCRIPTION:  stop CAP
* HISTORY:		Created Andrea B�ter (TMS)  29.06.2009 
******************************************************************************/
tS32 s32SpiCap_StopCapProcessor(OSAL_tIODescriptor* VphOsalDeviceGpio)
{
  tS32             Vs32OsalError;
  OSAL_trGPIOData  VpinData;

  Vs32OsalError = OSAL_s32IOControl(*VphOsalDeviceGpio, OSAL_C_32_IOCTRL_GPIO_SET_OUTPUT, (tS32)CAP_GPIO_RESET_CAP_PIN);
  if(Vs32OsalError != OSAL_ERROR)
  {
    VpinData.tId  = (OSAL_tGPIODevID)CAP_GPIO_RESET_CAP_PIN;
    VpinData.unData.bState = FALSE;
    Vs32OsalError=OSAL_s32IOControl(*VphOsalDeviceGpio, OSAL_C_32_IOCTRL_GPIO_SET_STATE, (tS32) &VpinData);
    if(Vs32OsalError!= (tS32)OSAL_ERROR)
    {/*Close the gpio device*/
      Vs32OsalError=OSAL_s32IOClose(*VphOsalDeviceGpio);  
      *VphOsalDeviceGpio = OSAL_ERROR;
    }
  }
  return(Vs32OsalError);
}
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvOpenClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Open and close the spi driver with CAP ID
* HISTORY:		Created Andrea B�ter (TMS)  24.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvOpenClose(void)
{/*Function return value*/
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  { /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
  }
  return(Vu32Ret);
}/*end function*/

/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvOpenInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Open the spi driver with invalid parameter
* HISTORY:		Created Andrea B�ter (TMS)  24.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvOpenInvalParam(void)
{
  tU32 Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = (OSAL_tenAccess)OEDT_SPI1_INVAL_PARAM;;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice != OSAL_ERROR)
  {
    Vu32Ret= 1;
    /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
  }
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvOpenNullParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Open the spi driver with parameter NULL
* HISTORY:		Created Andrea B�ter (TMS)  24.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvOpenNullParam(void)
{
  tU32 Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(0,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice != OSAL_ERROR)
  {
    Vu32Ret= 1;
    /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
  }
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvCloseInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  close the spi driver with invalid parameter 
* HISTORY:		Created Andrea B�ter (TMS)  24.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvCloseInvalParam(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  { /* close SPI1 */
    if(OSAL_s32IOClose(-1) != OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
    else
    {/*close device*/
      if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
      {
        Vu32Ret= 3;       
      }
    }
  }
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvReOpen()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  open the SPI driver with CAP- ID several times
* HISTORY:		Created Andrea B�ter (TMS)  24.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvReOpen(void)
{
  tU32 Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;
  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  { /*reopen driver*/
    if(OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess)!=OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
    /*close device*/
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret+= 10;       
    }
  }
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvReClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  close the SPI driver with CAP- ID several times
* HISTORY:		Created Andrea B�ter (TMS)  24.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvReClose(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  { /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
    /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) != OSAL_ERROR)
    {
      Vu32Ret+= 10;       
    }
  }
  return(Vu32Ret);
}/*end function*/
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvSetCallback()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  set the callback function for spi cap
* HISTORY:		Created Andrea B�ter (TMS)  26.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvSetCallback(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  { /*set callback */
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_SPI_INSTALL_REQCALLBACK,(tS32)vSpiCap_RequestCallback)==OSAL_ERROR)
    {
      Vu32Ret= 2; 
    }
    /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret+= 10;       
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvSetCallbackInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  set the callback function for spi cap with invalid param
* HISTORY:		Created Andrea B�ter (TMS)  26.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvSetCallbackInvalParam(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  { /*set callback Null Pointer*/
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_SPI_INSTALL_REQCALLBACK,(tS32)NULL)!=OSAL_ERROR)
    {
      Vu32Ret+= 10; 
    }
    /*set callback wrong id*/
    if(OSAL_s32IOControl(-1,OSAL_C_S32_IOCTRL_SPI_INSTALL_REQCALLBACK,(tS32)vSpiCap_RequestCallback)!=OSAL_ERROR)
    {
      Vu32Ret+= 100; 
    }
    /*set callback 2 x*/
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_SPI_INSTALL_REQCALLBACK,(tS32)vSpiCap_RequestCallback)==OSAL_ERROR)
    {
      Vu32Ret+= 1000; 
    }
    else
    {/*set callback 2 x*/
      if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_SPI_INSTALL_REQCALLBACK,(tS32)vSpiCap_RequestCallback)!=OSAL_ERROR)
      {
        Vu32Ret+= 2000; 
      }
    }
    /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret+= 2;       
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvSetCallbackAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  set the callback function for spi cap after dev close
* HISTORY:		Created Andrea B�ter (TMS)  26.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvSetCallbackAfterDevClose(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  { /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
    /*set callback */
    if(OSAL_s32IOControl(VhDevice,OSAL_C_S32_IOCTRL_SPI_INSTALL_REQCALLBACK,(tS32)vSpiCap_RequestCallback)!=OSAL_ERROR)
    {
      Vu32Ret+= 10; 
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvTransceiveNoCallback()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Transceive data to CAP with no callback
* HISTORY:		Created Andrea B�ter (TMS)  26.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvTransceiveNoCallback(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  {
    OSAL_tIODescriptor             VhOsalDeviceGpio;
    OSAL_trSpiTransceive_TunerICs  VtrCapTranceive;
    tU8                            Vu8Retry=0;
    tS32                           Vs32ErrorCode=OSAL_ERROR;
    tU8                            Vpu8TxMsg[256]; // TX buffer
    tU8                            Vpu8RxMsg[256]; // RX buffer    
    tS32                           Vs32Status=OSAL_E_NOERROR;

    if(s32SpiCap_StartCapProcessor(&VhOsalDeviceGpio)==(tS32)OSAL_E_NOERROR)
    {
      Vu32Ret+= 10; 
    }
    else
    {/*wait 250 ms */
      OSAL_s32ThreadWait(250); 
      /*init CP*/
      Vpu8TxMsg[0] = 0xC1;
      Vpu8TxMsg[1] = 0xC0;/*1100 0000*/   
      VtrCapTranceive.pu8RxData = Vpu8RxMsg;
      VtrCapTranceive.pu8TxData = Vpu8TxMsg;
      VtrCapTranceive.u32Length = 2;
      VtrCapTranceive.bDriverUsesChipSelect = TRUE; 
      /*loop until valid  transcieve*/
      while(Vu8Retry < 10)
      {
        Vs32ErrorCode=OSAL_s32IOControl(VhDevice, OSAL_C_S32_IOCTRL_SPI_TRANSCEIVE_TUNERIC,(tS32)&VtrCapTranceive);
        if(Vs32ErrorCode==OSAL_ERROR)
        {/* Error status returned */
          Vs32Status = (tS32) OSAL_u32ErrorCode();
          if(Vs32Status==(tS32)OSAL_E_IOERROR)
          {/*retry*/
            Vu8Retry++;
            /*wait time T9  = 4ms before retry */
            OSAL_s32ThreadWait(4);
          }
          else
          {/*other error break loop*/
            Vu8Retry=10;
            Vu32Ret+= 100;
          }
        }
        else
        {/*break loop => OK*/
          Vu8Retry=10;
        }
      }     
      if((Vs32Status==(tS32)OSAL_E_IOERROR)&&(Vs32ErrorCode==(tS32)OSAL_ERROR))
      {
        Vu32Ret+= 200;
      }
      /*stop cap prozessor*/
      if(s32SpiCap_StopCapProcessor(&VhOsalDeviceGpio))
      {
        Vu32Ret+= 1000;
      }
    }/*end else*/
    /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvTransceiveNoCallbackInvalParam()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Transceive data to CAP with no callback with invalid parameter
* HISTORY:		Created Andrea B�ter (TMS)  26.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvTransceiveNoCallbackInvalParam(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  {
    OSAL_tIODescriptor             VhOsalDeviceGpio;
    OSAL_trSpiTransceive_TunerICs  VtrCapTranceive;
    tU8                            Vpu8TxMsg[256]; // TX buffer
    tU8                            Vpu8RxMsg[256]; // RX buffer                                                    

    if(s32SpiCap_StartCapProcessor(&VhOsalDeviceGpio)==(tS32)OSAL_E_NOERROR)
    {
      Vu32Ret+= 10; 
    }
    else
    {/*wait 250 ms */
      OSAL_s32ThreadWait(250); 
      /*init CP*/
      Vpu8TxMsg[0] = 0xC1;
      Vpu8TxMsg[1] = 0xC0;/*1100 0000*/   
      VtrCapTranceive.bDriverUsesChipSelect = TRUE; 
      /*1. Len wrong*/
      VtrCapTranceive.pu8RxData = Vpu8RxMsg;
      VtrCapTranceive.pu8TxData = Vpu8TxMsg;
      VtrCapTranceive.u32Length = 1000;                       
      if(OSAL_s32IOControl(VhDevice, OSAL_C_S32_IOCTRL_SPI_TRANSCEIVE_TUNERIC,(tS32)&VtrCapTranceive)!= OSAL_ERROR)
      {
        Vu32Ret+= 100; 
      }/*end if*/
      /*2. RX Pointer NULL */
      VtrCapTranceive.pu8RxData = NULL;
      VtrCapTranceive.pu8TxData = Vpu8TxMsg;
      VtrCapTranceive.u32Length = 2;                       
      if(OSAL_s32IOControl(VhDevice, OSAL_C_S32_IOCTRL_SPI_TRANSCEIVE_TUNERIC,(tS32)&VtrCapTranceive)!= OSAL_ERROR)
      {
        Vu32Ret+= 200; 
      }/*end if*/
      /*3. TX Pointer NULL*/
      VtrCapTranceive.pu8RxData = Vpu8RxMsg;
      VtrCapTranceive.pu8TxData = NULL;
      VtrCapTranceive.u32Length = 2;                       
      if(OSAL_s32IOControl(VhDevice, OSAL_C_S32_IOCTRL_SPI_TRANSCEIVE_TUNERIC,(tS32)&VtrCapTranceive)!= OSAL_ERROR)
      {
        Vu32Ret+= 400; 
      }/*end if*/
      /*4. IO-Control wrong*/
      VtrCapTranceive.pu8RxData = Vpu8RxMsg;
      VtrCapTranceive.pu8TxData = Vpu8TxMsg;
      VtrCapTranceive.u32Length = 2;                       
      if(OSAL_s32IOControl(VhDevice, 100,(tS32)&VtrCapTranceive)!= OSAL_ERROR)
      {
        Vu32Ret+= 800; 
      }/*end if*/
      /*5. Pointer param NULL*/
      if(OSAL_s32IOControl(VhDevice, OSAL_C_S32_IOCTRL_SPI_TRANSCEIVE_TUNERIC,(tS32)NULL)!= OSAL_ERROR)
      {
        Vu32Ret+= 1000; 
      }/*end if*/
      if(s32SpiCap_StopCapProcessor(&VhOsalDeviceGpio))
      {
        Vu32Ret+= 2000;
      }
    }/*end else*/
    /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvTransceiveNoCallbackAfterDevClose()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Transceive data to CAP with no callback after dev closed
* HISTORY:		Created Andrea B�ter (TMS)  26.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvTransceiveNoCallbackAfterDevClose(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  {/* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
    else
    {
      OSAL_tIODescriptor             VhOsalDeviceGpio;
      OSAL_trSpiTransceive_TunerICs  VtrCapTranceive;
      tU8                            Vpu8TxMsg[256]; // TX buffer
      tU8                            Vpu8RxMsg[256]; // RX buffer    
      if(s32SpiCap_StartCapProcessor(&VhOsalDeviceGpio)==(tS32)OSAL_E_NOERROR)
      {
        Vu32Ret+= 10; 
      }
      /*wait 250 ms */
      OSAL_s32ThreadWait(250); 
      /*init CP*/
      Vpu8TxMsg[0] = 0xC1;
      Vpu8TxMsg[1] = 0xC0;/*1100 0000*/   
      VtrCapTranceive.pu8RxData = Vpu8RxMsg;
      VtrCapTranceive.pu8TxData = Vpu8TxMsg;
      VtrCapTranceive.u32Length = 2;
      VtrCapTranceive.bDriverUsesChipSelect = TRUE; 
      if(OSAL_s32IOControl(VhDevice, OSAL_C_S32_IOCTRL_SPI_TRANSCEIVE_TUNERIC,(tS32)&VtrCapTranceive)!= OSAL_ERROR)
      {
        Vu32Ret+= 100; 
      }/*end if*/
      if(s32SpiCap_StopCapProcessor(&VhOsalDeviceGpio))
      {
        Vu32Ret+= 1000;
      }
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvTransceiveNoCallbackCheckRetry()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Transceive data to CAP with no callback; 
*               wait time for send data is to small => check retry
* HISTORY:		Created Andrea B�ter (TMS)  29.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvTransceiveNoCallbackCheckRetry(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhDevice = 0;
  OSAL_tenAccess        VenAccess = OSAL_EN_READWRITE;

  /*Open the SPI1 device*/
  VhDevice = OSAL_IOOpen(OSAL_C_STRING_DEVICE_SPI_CAP,VenAccess);
  /*Check if opening has been successful*/
  if(VhDevice == OSAL_ERROR)
  {
    Vu32Ret= 1;
  }
  else
  {
    OSAL_tIODescriptor             VhOsalDeviceGpio;
    OSAL_trSpiTransceive_TunerICs  VtrCapTranceive;
    tU8                            Vu8Retry=0;
    tS32                           Vs32ErrorCode=OSAL_ERROR;
    tU8                            Vpu8TxMsg[256]; // TX buffer
    tU8                            Vpu8RxMsg[256]; // RX buffer                                                    

    if(s32SpiCap_StartCapProcessor(&VhOsalDeviceGpio)==(tS32)OSAL_E_NOERROR)
    {
      Vu32Ret+= 10; 
    }
    else
    {/*wait 50 ms => that is to small for the cap error must be occured (normal 250 ms)*/
      OSAL_s32ThreadWait(50); 
      /*init CP*/
      Vpu8TxMsg[0] = 0xC1;
      Vpu8TxMsg[1] = 0xC0;/*1100 0000*/   
      VtrCapTranceive.pu8RxData = Vpu8RxMsg;
      VtrCapTranceive.pu8TxData = Vpu8TxMsg;
      VtrCapTranceive.u32Length = 2;
      VtrCapTranceive.bDriverUsesChipSelect = TRUE; 
      /*loop until valid  transceive*/
      while((Vu8Retry < 10)&&(Vs32ErrorCode==OSAL_ERROR))
      {
        Vs32ErrorCode=OSAL_s32IOControl(VhDevice, OSAL_C_S32_IOCTRL_SPI_TRANSCEIVE_TUNERIC,(tS32)&VtrCapTranceive);
        Vu8Retry++;
        /*wait time T9  = 4ms before retry */
        OSAL_s32ThreadWait(10);
        /* Error status returned */
        tS32 Vs32Status = (tS32) OSAL_u32ErrorCode();
        if(Vs32Status!=(tS32)OSAL_E_IOERROR)
        {/*break loop*/
          Vu8Retry=10;
        }
      }/*end while*/
      if(Vu8Retry < 10)
      {/*increment counter*/
        Vu32Ret+= 100; 
      }
      /*stop cap prozessor*/
      if(s32SpiCap_StopCapProcessor(&VhOsalDeviceGpio))
      {
        Vu32Ret+= 1000;
      }
    }/*end else*/
    /* close SPI1 */
    if(OSAL_s32IOClose(VhDevice) == OSAL_ERROR)
    {
      Vu32Ret= 2;       
    }
  }
  return(Vu32Ret);
}
/*****************************************************************************
* FUNCTION:		u32SpiCap_DrvTransceiveCallback()
* PARAMETER:    none
* RETURNVALUE:  tU32, "0" on success  or "non-zero" value in case of error
* TEST CASE:    
* DESCRIPTION:  Transceive data to CAP with callback 
* HISTORY:		Created Andrea B�ter (TMS)  26.06.2009 
******************************************************************************/
tU32 u32SpiCap_DrvTransceiveCallback(void)
{
  tU32                  Vu32Ret = 0;
  OSAL_tIODescriptor    VhOsalDeviceGpio;

  /*create thread */
  /*Create Event*/
  if(bCreEvent(&tHandleEvent_CAPRequestLine, "CAP_TEST" ) == FALSE)
  {
    Vu32Ret += 1;    
  }
  else
  { /* start read thread */
    tThreadAttributeReadKey_CAP.szName       = "OEDT_CAP";
    tThreadAttributeReadKey_CAP.pfEntry      = vSpiCap_Thread;   
    if(bOpenThread(tThreadAttributeReadKey_CAP,&tIDThreadReadKey_CAP)==FALSE)
    {
      Vu32Ret += 2;
    }
    else
    {/*start cap processor */
      if(s32SpiCap_StartCapProcessor(&VhOsalDeviceGpio)==(tS32)OSAL_E_NOERROR)
      {
        Vu32Ret+= 4; 
      }
      else
      {
        OSAL_s32ThreadWait(250);      
        OSAL_s32EventPost(tHandleEvent_CAPRequestLine,CAP_EVENT_SEND_DATA,OSAL_EN_EVENTMASK_OR);     
        OSAL_s32ThreadWait(250);      
        OSAL_s32EventPost(tHandleEvent_CAPRequestLine,CAP_EVENT_SEND_DATA,OSAL_EN_EVENTMASK_OR);     
        OSAL_s32ThreadWait(1000); 
        /* get number of transaction*/
        if(vu32CntTransceive==0)
        {/*no transceive*/
          Vu32Ret+=10;
        }
        /* get error thread*/
        Vu32Ret+=vu32RetThread;
        /* event for exit the thread */
        OSAL_s32EventPost(tHandleEvent_CAPRequestLine,CAP_EVENT_EXIT_THREAD,OSAL_EN_EVENTMASK_OR);  
        /*stop cap processor*/
        if(s32SpiCap_StopCapProcessor(&VhOsalDeviceGpio))
        {
          Vu32Ret+= 1000;
        }
      }
      /* delete thread*/
      if(bCloseThread(tIDThreadReadKey_CAP)==FALSE)
      {
        Vu32Ret += 2000; 
      }
    }     
    /* delete event*/
    if(bDelEvent(&tHandleEvent_CAPRequestLine, "CAP_TEST")==FALSE)
    {
      Vu32Ret += 4000;    
    }
  }
  return(Vu32Ret);
}















